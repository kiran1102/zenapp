/*! Rappid v2.2.0 - HTML5 Diagramming Framework

Copyright (c) 2015 client IO

 2017-10-23


This Source Code Form is subject to the terms of the Rappid License
, v. 2.0. If a copy of the Rappid License was not distributed with this
file, You can obtain one at http://jointjs.com/license/rappid_v2.txt
 or from the Rappid archive as was distributed by client IO. See the LICENSE file.*/

import "images/LineageIcons/data-subjects.svg";
import "images/LineageIcons/ds-employee.svg";
import "images/LineageIcons/ds-contractor.svg";
import "images/LineageIcons/ds-customer.svg";
import "images/LineageIcons/related.svg";
import "images/LineageIcons/ds-potential-employee.svg";
import "images/LineageIcons/asset-application.svg";
import "images/LineageIcons/asset-database.svg";
import "images/LineageIcons/asset-physical.svg";
import "images/LineageIcons/asset-server.svg";
import "images/LineageIcons/asset-website.svg";
import "images/LineageIcons/parties.svg";
import "images/LineageIcons/party-external.svg";
import "images/LineageIcons/party-internal.svg";
import "images/LineageIcons/party-regulatory.svg";
import "images/LineageIcons/vendor-operations.svg";
import "images/LineageIcons/vendor-contractor.svg";
import "images/LineageIcons/vendor-regulatory.svg";
import "images/LineageIcons/vendor-strategy.svg";
import "images/LineageIcons/vendor-technology.svg";
import "images/LineageIcons/vendor-custom.svg";
import "images/LineageIcons/archive-server.svg";
import "images/LineageIcons/data-delete.svg";
import "images/LineageIcons/physical-archive.svg";
import "images/LineageIcons/physical-destroy.svg";
import "images/LineageIcons/delete.svg";

import {ui} from './rappid';
const imageHeight: number = 12;
const imagewidth: number = 18;
const fontSize: number = 10;

export const stencil = {
    customGroups: <{ [key: string]: ui.Stencil.Group }>{
        dataSubjects: { index: 1, label: 'DataSubjects', closed: false },
        assets: { index: 2, label: 'Assets', closed: true },
        vendors: { index: 3, label: 'Vendors', closed: true },
        parties: { index: 3, label: 'PartiesWithAccess', closed: true },
        misc: { index: 4, label: 'Misc.', closed: true },
    },
    groups: <{ [key: string]: ui.Stencil.Group }>{
        basic: { index: 1, label: 'Basic shapes' },
        fsa: { index: 2, label: 'State machine' },
        pn: { index: 3, label: 'Petri nets' },
        erd: { index: 4, label: 'Entity-relationship' },
        uml: { index: 5, label: 'UML' },
        org: { index: 6, label: 'ORG' }
    },
    shapes: {
        dataSubjects: [
            {
                type: 'basic.DataSubjects',
                size: { width: imagewidth, height: imageHeight },
                attrs: {
                    '.': {
                        'data-tooltip': 'DataSubject',
                        'data-tooltip-position': 'left',
                        'data-tooltip-position-selector': '.joint-stencil'
                    },
                    image: {
                        width: imagewidth,
                        height: imageHeight,
                        'xlink:href': 'images/LineageIcons/data-subjects.svg'
                    },
                    text: {
                        text: 'DataSubject',
                        'font-family': 'Open Sans',
                        'font-weight': 'Normal',
                        'font-size': fontSize,
                        display: '',
                        stroke: '#000000',
                        'stroke-width': 0,
                        'fill': '#000000'
                    }
                },
            },
            {
                type: 'basic.DataSubjectEmployee',
                size: { width: imagewidth, height: imageHeight },
                attrs: {
                    '.': {
                        'data-tooltip': 'Employee',
                        'data-tooltip-position': 'left',
                        'data-tooltip-position-selector': '.joint-stencil'
                    },
                    image: {
                        width: imagewidth,
                        height: imageHeight,
                        'xlink:href': 'images/LineageIcons/ds-employee.svg'
                    },
                    text: {
                        text: 'Employee',
                        'font-family': 'Open Sans',
                        'font-weight': 'Normal',
                        'font-size': fontSize,
                        display: '',
                        stroke: '#000000',
                        'stroke-width': 0,
                        'fill': '#000000'
                    }
                }
            },
            {
                type: 'basic.DataSubjectContractor',
                size: { width: imagewidth, height: imageHeight },
                attrs: {
                    '.': {
                        'data-tooltip': 'Contractor',
                        'data-tooltip-position': 'left',
                        'data-tooltip-position-selector': '.joint-stencil'
                    },
                    image: {
                        width: imagewidth,
                        height: imageHeight,
                        'xlink:href': 'images/LineageIcons/ds-contractor.svg'
                    },
                    text: {
                        text: 'Contractor',
                        'font-family': 'Open Sans',
                        'font-weight': 'Normal',
                        'font-size': fontSize,
                        display: '',
                        stroke: '#000000',
                        'stroke-width': 0,
                        'fill': '#000000'
                    }
                }
            },
            {
                type: 'basic.DataSubjectCustomer',
                size: { width: imagewidth, height: imageHeight },
                attrs: {
                    '.': {
                        'data-tooltip': 'Customer',
                        'data-tooltip-position': 'left',
                        'data-tooltip-position-selector': '.joint-stencil'
                    },
                    image: {
                        width: imagewidth,
                        height: imageHeight,
                        'xlink:href': 'images/LineageIcons/ds-customer.svg'
                    },
                    text: {
                        text: 'Customer',
                        'font-family': 'Open Sans',
                        'font-weight': 'Normal',
                        'font-size': fontSize,
                        display: '',
                        stroke: '#000000',
                        'stroke-width': 0,
                        'fill': '#000000'
                    }
                }
            },
            {
                type: 'basic.DataSubjectProspectiveEmployee',
                size: { width: imagewidth, height: imageHeight },
                attrs: {
                    '.': {
                        'data-tooltip': 'ProspectiveEmployee',
                        'data-tooltip-position': 'left',
                        'data-tooltip-position-selector': '.joint-stencil'
                    },
                    image: {
                        width: imagewidth,
                        height: imageHeight,
                        'xlink:href': 'images/LineageIcons/ds-potential-employee.svg'
                    },
                    text: {
                        text: 'ProspectiveEmployee',
                        'font-family': 'Open Sans',
                        'font-weight': 'Normal',
                        'font-size': fontSize,
                        display: '',
                        stroke: '#000000',
                        'stroke-width': 0,
                        'fill': '#000000'
                    }
                }
            },
        ],
        assets: [
            {
                type: 'basic.Assets',
                size: { width: imagewidth, height: imageHeight },
                attrs: {
                    '.': {
                        'data-tooltip': 'Application',
                        'data-tooltip-position': 'left',
                        'data-tooltip-position-selector': '.joint-stencil'
                    },
                    image: {
                        width: imagewidth,
                        height: imageHeight,
                        'xlink:href': 'images/LineageIcons/asset-application.svg'
                    },
                    text: {
                        text: 'Application',
                        'font-family': 'Open Sans',
                        'font-weight': 'Normal',
                        'font-size': fontSize,
                        display: '',
                        stroke: '#000000',
                        'stroke-width': 0,
                        'fill': '#000000'
                    }
                }
            },
            {
                type: 'basic.Assets',
                size: { width: imagewidth, height: imageHeight },
                attrs: {
                    '.': {
                        'data-tooltip': 'Database',
                        'data-tooltip-position': 'left',
                        'data-tooltip-position-selector': '.joint-stencil'
                    },
                    image: {
                        width: imagewidth,
                        height: imageHeight,
                        'xlink:href': 'images/LineageIcons/asset-database.svg'
                    },
                    text: {
                        text: 'Database',
                        'font-family': 'Open Sans',
                        'font-weight': 'Normal',
                        'font-size': fontSize,
                        display: '',
                        stroke: '#000000',
                        'stroke-width': 0,
                        'fill': '#000000'
                    }
                }
            },
            {
                type: 'basic.Assets',
                size: { width: imagewidth, height: imageHeight },
                attrs: {
                    '.': {
                        'data-tooltip': 'Physical',
                        'data-tooltip-position': 'left',
                        'data-tooltip-position-selector': '.joint-stencil'
                    },
                    image: {
                        width: imagewidth,
                        height: imageHeight,
                        'xlink:href': 'images/LineageIcons/asset-physical.svg'
                    },
                    text: {
                        text: 'Physical',
                        'font-family': 'Open Sans',
                        'font-weight': 'Normal',
                        'font-size': fontSize,
                        display: '',
                        stroke: '#000000',
                        'stroke-width': 0,
                        'fill': '#000000'
                    }
                }
            },
            {
                type: 'basic.Assets',
                size: { width: imagewidth, height: imageHeight },
                attrs: {
                    '.': {
                        'data-tooltip': 'Server',
                        'data-tooltip-position': 'left',
                        'data-tooltip-position-selector': '.joint-stencil'
                    },
                    image: {
                        width: imagewidth,
                        height: imageHeight,
                        'xlink:href': 'images/LineageIcons/asset-server.svg'
                    },
                    text: {
                        text: 'Server',
                        'font-family': 'Open Sans',
                        'font-weight': 'Normal',
                        'font-size': fontSize,
                        display: '',
                        stroke: '#000000',
                        'stroke-width': 0,
                        'fill': '#000000'
                    }
                }
            },
            {
                type: 'basic.Assets',
                size: { width: imagewidth, height: imageHeight },
                attrs: {
                    '.': {
                        'data-tooltip': 'Website',
                        'data-tooltip-position': 'left',
                        'data-tooltip-position-selector': '.joint-stencil'
                    },
                    image: {
                        width: imagewidth,
                        height: imageHeight,
                        'xlink:href': 'images/LineageIcons/asset-website.svg'
                    },
                    text: {
                        text: 'Website',
                        'font-family': 'Open Sans',
                        'font-weight': 'Normal',
                        'font-size': fontSize,
                        display: '',
                        stroke: '#000000',
                        'stroke-width': 0,
                        'fill': '#000000'
                    }
                }
            },
            {
                type: 'basic.RelatedAsset',
                size: { width: imagewidth, height: imageHeight },
                attrs: {
                    '.': {
                        'data-tooltip': 'RelatedAsset',
                        'data-tooltip-position': 'left',
                        'data-tooltip-position-selector': '.joint-stencil'
                    },
                    image: {
                        width: imagewidth,
                        height: imageHeight,
                        'xlink:href': 'images/LineageIcons/related.svg'
                    },
                    text: {
                        text: 'RelatedAsset',
                        'font-family': 'Open Sans',
                        'font-weight': 'Normal',
                        'font-size': fontSize,
                        display: '',
                        stroke: '#000000',
                        'stroke-width': 0,
                        'fill': '#000000'
                    }
                }
            },
        ],
        vendors: [
            {
                type: 'basic.Vendors',
                size: { width: imagewidth, height: imageHeight },
                attrs: {
                    '.': {
                        'data-tooltip': 'Operations',
                        'data-tooltip-position': 'left',
                        'data-tooltip-position-selector': '.joint-stencil'
                    },
                    image: {
                        width: imagewidth,
                        height: imageHeight,
                        'xlink:href': 'images/LineageIcons/vendor-operations.svg'
                    },
                    text: {
                        text: 'Operations',
                        'font-family': 'Open Sans',
                        'font-weight': 'Normal',
                        'font-size': fontSize,
                        display: '',
                        stroke: '#000000',
                        'stroke-width': 0,
                        'fill': '#000000'
                    }
                }
            },
            {
                type: 'basic.Vendors',
                size: { width: imagewidth, height: imageHeight },
                attrs: {
                    '.': {
                        'data-tooltip': 'Regulatory',
                        'data-tooltip-position': 'left',
                        'data-tooltip-position-selector': '.joint-stencil'
                    },
                    image: {
                        width: imagewidth,
                        height: imageHeight,
                        'xlink:href': 'images/LineageIcons/vendor-regulatory.svg'
                    },
                    text: {
                        text: 'Regulatory',
                        'font-family': 'Open Sans',
                        'font-weight': 'Normal',
                        'font-size': fontSize,
                        display: '',
                        stroke: '#000000',
                        'stroke-width': 0,
                        'fill': '#000000'
                    }
                }
            },
            {
                type: 'basic.Vendors',
                size: { width: imagewidth, height: imageHeight },
                attrs: {
                    '.': {
                        'data-tooltip': 'Strategy',
                        'data-tooltip-position': 'left',
                        'data-tooltip-position-selector': '.joint-stencil'
                    },
                    image: {
                        width: imagewidth,
                        height: imageHeight,
                        'xlink:href': 'images/LineageIcons/vendor-strategy.svg'
                    },
                    text: {
                        text: 'Strategy',
                        'font-family': 'Open Sans',
                        'font-weight': 'Normal',
                        'font-size': fontSize,
                        display: '',
                        stroke: '#000000',
                        'stroke-width': 0,
                        'fill': '#000000'
                    }
                }
            },
            {
                type: 'basic.Vendors',
                size: { width: imagewidth, height: imageHeight },
                attrs: {
                    '.': {
                        'data-tooltip': 'Technology',
                        'data-tooltip-position': 'left',
                        'data-tooltip-position-selector': '.joint-stencil'
                    },
                    image: {
                        width: imagewidth,
                        height: imageHeight,
                        'xlink:href': 'images/LineageIcons/vendor-technology.svg'
                    },
                    text: {
                        text: 'Technology',
                        'font-family': 'Open Sans',
                        'font-weight': 'Normal',
                        'font-size': fontSize,
                        display: '',
                        stroke: '#000000',
                        'stroke-width': 0,
                        'fill': '#000000'
                    }
                }
            },
            {
                type: 'basic.Vendors',
                size: { width: imagewidth, height: imageHeight },
                attrs: {
                    '.': {
                        'data-tooltip': 'Custom',
                        'data-tooltip-position': 'left',
                        'data-tooltip-position-selector': '.joint-stencil'
                    },
                    image: {
                        width: imagewidth,
                        height: imageHeight,
                        'xlink:href': 'images/LineageIcons/related.svg'
                    },
                    text: {
                        text: 'Custom',
                        'font-family': 'Open Sans',
                        'font-weight': 'Normal',
                        'font-size': fontSize,
                        display: '',
                        stroke: '#000000',
                        'stroke-width': 0,
                        'fill': '#000000'
                    }
                }
            },
        ],
        parties: [
            {
                type: 'basic.Parties',
                size: { width: imagewidth, height: imageHeight },
                attrs: {
                    '.': {
                        'data-tooltip': 'Party',
                        'data-tooltip-position': 'left',
                        'data-tooltip-position-selector': '.joint-stencil'
                    },
                    image: {
                        width: imagewidth,
                        height: imageHeight,
                        'xlink:href': 'images/LineageIcons/parties.svg'
                    },
                    text: {
                        text: 'Party',
                        'font-family': 'Open Sans',
                        'font-weight': 'Normal',
                        'font-size': fontSize,
                        display: '',
                        stroke: '#000000',
                        'stroke-width': 0,
                        'fill': '#000000'
                    }
                }
            },
            {
                type: 'basic.Parties',
                size: { width: imagewidth, height: imageHeight },
                attrs: {
                    '.': {
                        'data-tooltip': 'External',
                        'data-tooltip-position': 'left',
                        'data-tooltip-position-selector': '.joint-stencil'
                    },
                    image: {
                        width: imagewidth,
                        height: imageHeight,
                        'xlink:href': 'images/LineageIcons/party-external.svg'
                    },
                    text: {
                        text: 'External',
                        'font-family': 'Open Sans',
                        'font-weight': 'Normal',
                        'font-size': fontSize,
                        display: '',
                        stroke: '#000000',
                        'stroke-width': 0,
                        'fill': '#000000'
                    }
                }
            },
            {
                type: 'basic.Parties',
                size: { width: imagewidth, height: imageHeight },
                attrs: {
                    '.': {
                        'data-tooltip': 'Internal',
                        'data-tooltip-position': 'left',
                        'data-tooltip-position-selector': '.joint-stencil'
                    },
                    image: {
                        width: imagewidth,
                        height: imageHeight,
                        'xlink:href': 'images/LineageIcons/party-internal.svg'
                    },
                    text: {
                        text: 'Internal',
                        'font-family': 'Open Sans',
                        'font-weight': 'Normal',
                        'font-size': fontSize,
                        display: '',
                        stroke: '#000000',
                        'stroke-width': 0,
                        'fill': '#000000'
                    }
                }
            },
            {
                type: 'basic.Parties',
                size: { width: imagewidth, height: imageHeight },
                attrs: {
                    '.': {
                        'data-tooltip': 'Regulatory',
                        'data-tooltip-position': 'left',
                        'data-tooltip-position-selector': '.joint-stencil'
                    },
                    image: {
                        width: imagewidth,
                        height: imageHeight,
                        'xlink:href': 'images/LineageIcons/party-regulatory.svg'
                    },
                    text: {
                        text: 'Regulatory',
                        'font-family': 'Open Sans',
                        'font-weight': 'Normal',
                        'font-size': fontSize,
                        display: '',
                        stroke: '#000000',
                        'stroke-width': 0,
                        'fill': '#000000'
                    }
                }
            },
            {
                type: 'basic.RelatedParty',
                size: { width: imagewidth, height: imageHeight },
                attrs: {
                    '.': {
                        'data-tooltip': 'RelatedParty',
                        'data-tooltip-position': 'left',
                        'data-tooltip-position-selector': '.joint-stencil'
                    },
                    image: {
                        width: imagewidth,
                        height: imageHeight,
                        'xlink:href': 'images/LineageIcons/related.svg'
                    },
                    text: {
                        text: 'RelatedParty',
                        'font-family': 'Open Sans',
                        'font-weight': 'Normal',
                        'font-size': fontSize,
                        display: '',
                        stroke: '#000000',
                        'stroke-width': 0,
                        'fill': '#000000'
                    }
                }
            },
        ],
        misc: [
            {
                type: 'basic.Label',
                size: { width: 5, height: 3 },
                attrs: {
                    '.': {
                        'data-tooltip': 'Label',
                        'data-tooltip-position': 'left',
                        'data-tooltip-position-selector': '.joint-stencil'
                    },
                    rect: {
                        rx: 2,
                        ry: 2,
                        width: 50,
                        height: 30,
                        fill: 'transparent',
                        stroke: 'transparent',
                        'stroke-width': 2,
                        'stroke-dasharray': '0'
                    },
                    text: {
                        text: 'Label',
                        fill: '#000000',
                        'font-family': 'Open Sans',
                        'font-weight': 'Bold',
                        'font-size': 14,
                        'stroke-width': 0
                    }
                }
            },
            {
                type: 'basic.Label',
                size: { width: 5, height: 3 },
                attrs: {
                    '.': {
                        'data-tooltip': 'Text',
                        'data-tooltip-position': 'left',
                        'data-tooltip-position-selector': '.joint-stencil'
                    },
                    rect: {
                        rx: 2,
                        ry: 2,
                        width: 50,
                        height: 30,
                        fill: 'transparent',
                        stroke: 'transparent',
                        'stroke-width': 2,
                        'stroke-dasharray': '0'
                    },
                    text: {
                        text: 'Text',
                        fill: '#000000',
                        'font-family': 'Open Sans',
                        'font-weight': 'Normal',
                        'font-size': 12,
                        'stroke-width': 0
                    }
                }
            },
            {
                type: 'basic.Misc',
                size: { width: imagewidth, height: imageHeight },
                attrs: {
                    '.': {
                        'data-tooltip': 'Delete',
                        'data-tooltip-position': 'left',
                        'data-tooltip-position-selector': '.joint-stencil'
                    },
                    image: {
                        width: imagewidth,
                        height: imageHeight,
                        'xlink:href': 'images/LineageIcons/delete.svg'
                    },
                    text: {
                        text: 'Delete',
                        'font-family': 'Open Sans',
                        'font-weight': 'Normal',
                        'font-size': fontSize,
                        display: '',
                        stroke: '#000000',
                        'stroke-width': 0,
                        'fill': '#000000'
                    }
                }
            },
            {
                type: 'basic.Circle',
                size: { width: 1, height: 1 },
                attrs: {
                    '.': {
                        'data-tooltip': 'Circle',
                        'data-tooltip-position': 'left',
                        'data-tooltip-position-selector': '.joint-stencil'
                    },
                    circle: {
                        width: 1,
                        height: 1,
                        fill: '#1F96DB',
                        stroke: 'transparent',
                        'stroke-width': 2,
                        'stroke-dasharray': '0'
                    },
                    text: {
                        text: '',
                        fill: '#FFFFFF',
                        'font-family': 'Open Sans',
                        'font-weight': 'Bold',
                        'font-size': 12,
                        'stroke-width': 0
                    }
                }
            },
            {
                type: 'basic.Rect',
                size: { width: 5, height: 3 },
                attrs: {
                    '.': {
                        'data-tooltip': 'Rectangle',
                        'data-tooltip-position': 'left',
                        'data-tooltip-position-selector': '.joint-stencil'
                    },
                    rect: {
                        rx: 2,
                        ry: 2,
                        width: 50,
                        height: 30,
                        fill: '#1F96DB',
                        stroke: 'transparent',
                        'stroke-width': 2,
                        'stroke-dasharray': '0'
                    },
                    text: {
                        text: '',
                        fill: '#FFFFFF',
                        'font-family': 'Open Sans',
                        'font-weight': 'Bold',
                        'font-size': 12,
                        'stroke-width': 0
                    }
                }
            },
            {
                type: 'erd.Relationship',
                size: { width: 5, height: 3 },
                attrs: {
                    '.': {
                        'data-tooltip': 'Diamond',
                        'data-tooltip-position': 'left',
                        'data-tooltip-position-selector': '.joint-stencil'
                    },
                    '.outer': {
                        fill: '#1F96DB',
                        stroke: 'transparent',
                        'stroke-width': 2,
                        'stroke-dasharray': '0'
                    },
                    text: {
                        text: '',
                        'font-size': 12,
                        'font-family': 'Open Sans',
                        'font-weight': 'Bold',
                        fill: '#FFFFFF',
                        'stroke-width': 0
                    }
                }
            },
            {
                type: 'erd.ISA',
                attrs: {
                    '.': {
                        'data-tooltip': 'Triangle',
                        'data-tooltip-position': 'left',
                        'data-tooltip-position-selector': '.joint-stencil'
                    },
                    text: {
                        text: '',
                        fill: '#FFFFFF',
                        'letter-spacing': 0,
                        'font-family': 'Open Sans',
                        'font-weight': 'Bold',
                        'font-size': 12
                    },
                    polygon: {
                        fill: '#1F96DB',
                        stroke: 'transparent',
                        'stroke-dasharray': '0'
                    }
                }
            },
        ],
        basic: [
            {
                type: 'basic.Rect',
                size: { width: 5, height: 3 },
                attrs: {
                    '.': {
                        'data-tooltip': 'Rectangle',
                        'data-tooltip-position': 'left',
                        'data-tooltip-position-selector': '.joint-stencil'
                    },
                    rect: {
                        rx: 2,
                        ry: 2,
                        width: 50,
                        height: 30,
                        fill: 'transparent',
                        stroke: '#31d0c6',
                        'stroke-width': 2,
                        'stroke-dasharray': '0'
                    },
                    text: {
                        text: 'rect',
                        fill: '#c6c7e2',
                        'font-family': 'Roboto Condensed',
                        'font-weight': 'Normal',
                        'font-size': 11,
                        'stroke-width': 0
                    }
                }
            },
            {
                type: 'basic.Circle',
                size: { width: 5, height: 3 },
                attrs: {
                    '.': {
                        'data-tooltip': 'Ellipse',
                        'data-tooltip-position': 'left',
                        'data-tooltip-position-selector': '.joint-stencil'
                    },
                    circle: {
                        width: 50,
                        height: 30,
                        fill: 'transparent',
                        stroke: '#31d0c6',
                        'stroke-width': 2,
                        'stroke-dasharray': '0'
                    },
                    text: {
                        text: 'ellipse',
                        fill: '#c6c7e2',
                        'font-family': 'Roboto Condensed',
                        'font-weight': 'Normal',
                        'font-size': 11,
                        'stroke-width': 0
                    }
                }
            },
            {
                type: 'app.RectangularModel',
                size: { width: 40, height: 30 },
                inPorts: ['in1', 'in2'],
                outPorts: ['out'],
                allowOrthogonalResize: false,
                attrs: {
                    '.': {
                        'data-tooltip': 'Rectangle with ports',
                        'data-tooltip-position': 'left',
                        'data-tooltip-position-selector': '.joint-stencil'
                    },
                    '.body': {
                        fill: 'transparent',
                        rx: 2,
                        ry: 2,
                        stroke: '#31d0c6',
                        'stroke-width': 2,
                        'stroke-dasharray': '0'
                    },
                    '.label': {
                        text: 'rect',
                        fill: '#c6c7e2',
                        'font-family': 'Roboto Condensed',
                        'font-weight': 'Normal',
                        'font-size': 11,
                        'stroke-width': 0,
                        'ref-y': 0.5,
                        'y-alignment': 'middle'
                    }
                }
            },
            {
                type: 'app.CircularModel',
                size: { width: 5, height: 3 },
                inPorts: ['in1', 'in2'],
                outPorts: ['out'],
                allowOrthogonalResize: false,
                attrs: {
                    '.': {
                        'data-tooltip': 'Ellipse with ports',
                        'data-tooltip-position': 'left',
                        'data-tooltip-position-selector': '.joint-stencil'
                    },
                    '.body': {
                        fill: 'transparent',
                        stroke: '#31d0c6',
                        'stroke-width': 2,
                        'stroke-dasharray': '0',
                        'rx': '50%',
                        'ry': '50%'
                    },
                    '.label': {
                        text: 'ellipse',
                        fill: '#c6c7e2',
                        'font-family': 'Roboto Condensed',
                        'font-weight': 'Normal',
                        'font-size': 11,
                        'stroke-width': 0,
                        'ref-y': 0.5,
                        'y-alignment': 'middle'
                    }
                }
            },
            {
                type: 'basic.Image',
                size: { width: 53, height: 42 },
                attrs: {
                    '.': {
                        'data-tooltip': 'Image',
                        'data-tooltip-position': 'left',
                        'data-tooltip-position-selector': '.joint-stencil'
                    },
                    image: {
                        width: 53,
                        height: 42,
                        'xlink:href': './assets/image-icon1.svg'
                    },
                    text: {
                        text: 'image',
                        'font-family': 'Roboto Condensed',
                        'font-weight': 'Normal',
                        'font-size': 9,
                        display: '',
                        stroke: '#000000',
                        'stroke-width': 0,
                        'fill': '#222138'
                    }
                }
            }
        ],
        fsa: [

            {
                type: 'fsa.StartState',
                preserveAspectRatio: true,
                attrs: {
                    '.': {
                        'data-tooltip': 'Start State',
                        'data-tooltip-position': 'left',
                        'data-tooltip-position-selector': '.joint-stencil'
                    },
                    circle: {
                        width: 50,
                        height: 30,
                        fill: '#61549C',
                        'stroke-width': 0
                    },
                    text: {
                        text: 'startState',
                        fill: '#c6c7e2',
                        'font-family': 'Roboto Condensed',
                        'font-weight': 'Normal',
                        'font-size': 11,
                        'stroke-width': 0
                    }
                }
            },
            {
                type: 'fsa.EndState',
                preserveAspectRatio: true,
                attrs: {
                    '.': {
                        'data-tooltip': 'End State',
                        'data-tooltip-position': 'left',
                        'data-tooltip-position-selector': '.joint-stencil'
                    },
                    '.inner': {
                        fill: '#6a6c8a',
                        stroke: 'transparent'
                    },
                    '.outer': {
                        fill: 'transparent',
                        stroke: '#61549C',
                        'stroke-width': 2,
                        'stroke-dasharray': '0'
                    },
                    text: {
                        text: 'endState',
                        fill: '#c6c7e2',
                        'font-family': 'Roboto Condensed',
                        'font-weight': 'Normal',
                        'font-size': 11,
                        'stroke-width': 0
                    }
                }
            },
            {
                type: 'fsa.State',
                preserveAspectRatio: true,
                attrs: {
                    '.': {
                        'data-tooltip': 'State',
                        'data-tooltip-position': 'left',
                        'data-tooltip-position-selector': '.joint-stencil'
                    },
                    circle: {
                        fill: '#6a6c8a',
                        stroke: '#61549C',
                        'stroke-width': 2,
                        'stroke-dasharray': '0'
                    },
                    text: {
                        text: 'state',
                        fill: '#c6c7e2',
                        'font-family': 'Roboto Condensed',
                        'font-weight': 'Normal',
                        'font-size': 11,
                        'stroke-width': 0
                    }
                }
            }
        ],
        pn: [

            {
                type: 'pn.Place',
                preserveAspectRatio: true,
                tokens: 3,
                attrs: {
                    '.': {
                        'data-tooltip': 'Place',
                        'data-tooltip-position': 'left',
                        'data-tooltip-position-selector': '.joint-stencil'
                    },
                    '.root': {
                        fill: 'transparent',
                        stroke: '#61549C',
                        'stroke-width': 2,
                        'stroke-dasharray': '0'
                    },
                    '.tokens circle': {
                        fill: '#6a6c8a',
                        stroke: '#000000',
                        'stroke-width': 0
                    },
                    '.label': {
                        text: '',
                        'font-family': 'Roboto Condensed',
                        'font-weight': 'Normal'
                    }
                }
            },
            {
                type: 'pn.Transition',
                preserveAspectRatio: true,
                attrs: {
                    '.': {
                        'data-tooltip': 'Transition',
                        'data-tooltip-position': 'left',
                        'data-tooltip-position-selector': '.joint-stencil'
                    },
                    rect: {
                        rx: 3,
                        ry: 3,
                        width: 12,
                        height: 50,
                        fill: '#61549C',
                        stroke: '#7c68fc',
                        'stroke-width': 0,
                        'stroke-dasharray': '0'
                    },
                    '.label': {
                        text: 'transition',
                        'font-family': 'Roboto Condensed',
                        'font-weight': 'Normal',
                        'stroke-width': 0,
                        'fill': '#222138'
                    }
                }
            }
        ],
        erd: [

            {
                type: 'erd.Entity',
                attrs: {
                    '.': {
                        'data-tooltip': 'Entity',
                        'data-tooltip-position': 'left',
                        'data-tooltip-position-selector': '.joint-stencil'
                    },
                    '.outer': {
                        rx: 3,
                        ry: 3,
                        fill: '#31d0c6',
                        'stroke-width': 2,
                        stroke: 'transparent',
                        'stroke-dasharray': '0'
                    },
                    text: {
                        text: 'Entity',
                        'font-size': 11,
                        'font-family': 'Roboto Condensed',
                        'font-weight': 'Normal',
                        fill: '#f6f6f6',
                        'stroke-width': 0
                    }
                }
            },
            {
                type: 'erd.WeakEntity',
                attrs: {
                    '.': {
                        'data-tooltip': 'Weak Entity',
                        'data-tooltip-position': 'left',
                        'data-tooltip-position-selector': '.joint-stencil'
                    },
                    '.outer': {
                        fill: 'transparent',
                        stroke: '#feb663',
                        'stroke-width': 2,
                        points: '100,0 100,60 0,60 0,0',
                        'stroke-dasharray': '0'
                    },
                    '.inner': {
                        fill: '#feb663',
                        stroke: 'transparent',
                        points: '97,5 97,55 3,55 3,5',
                        'stroke-dasharray': '0'
                    },
                    text: {
                        text: 'Weak entity',
                        'font-size': 11,
                        'font-family': 'Roboto Condensed',
                        'font-weight': 'Normal',
                        fill: '#f6f6f6',
                        'stroke-width': 0
                    }
                }
            },
            {
                type: 'erd.Relationship',
                attrs: {
                    '.': {
                        'data-tooltip': 'Relationship',
                        'data-tooltip-position': 'left',
                        'data-tooltip-position-selector': '.joint-stencil'
                    },
                    '.outer': {
                        fill: '#61549C',
                        stroke: 'transparent',
                        'stroke-width': 2,
                        'stroke-dasharray': '0'
                    },
                    text: {
                        text: 'Relation',
                        'font-size': 11,
                        'font-family': 'Roboto Condensed',
                        'font-weight': 'Normal',
                        fill: '#f6f6f6',
                        'stroke-width': 0
                    }
                }
            },
            {
                type: 'erd.IdentifyingRelationship',
                attrs: {
                    '.': {
                        'data-tooltip': 'Identifying Relationship',
                        'data-tooltip-position': 'left',
                        'data-tooltip-position-selector': '.joint-stencil'
                    },
                    '.outer': {
                        fill: 'transparent',
                        stroke: '#6a6c8a',
                        'stroke-dasharray': '0'
                    },
                    '.inner': {
                        fill: '#6a6c8a',
                        stroke: 'transparent',
                        'stroke-dasharray': '0'
                    },
                    text: {
                        text: 'Relation',
                        'font-size': 11,
                        'font-family': 'Roboto Condensed',
                        'font-weight': 'Normal',
                        fill: '#f6f6f6',
                        'stroke-width': 0
                    }
                }
            },
            {
                type: 'erd.ISA',
                attrs: {
                    '.': {
                        'data-tooltip': 'ISA',
                        'data-tooltip-position': 'left',
                        'data-tooltip-position-selector': '.joint-stencil'
                    },
                    text: {
                        text: 'ISA',
                        fill: '#f6f6f6',
                        'letter-spacing': 0,
                        'font-family': 'Roboto Condensed',
                        'font-weight': 'Normal',
                        'font-size': 11
                    },
                    polygon: {
                        fill: '#feb663',
                        stroke: 'transparent',
                        'stroke-dasharray': '0'
                    }
                }
            },
            {
                type: 'erd.Key',
                attrs: {
                    '.': {
                        'data-tooltip': 'Key',
                        'data-tooltip-position': 'left',
                        'data-tooltip-position-selector': '.joint-stencil'
                    },
                    '.outer': {
                        fill: 'transparent',
                        stroke: '#fe854f',
                        'stroke-dasharray': '0'
                    },
                    '.inner': {
                        fill: '#fe854f',
                        stroke: 'transparent',
                        display: 'block',
                        'stroke-dasharray': '0'
                    },
                    text: {
                        text: 'Key',
                        'font-size': 11,
                        'font-family': 'Roboto Condensed',
                        'font-weight': 'Normal',
                        fill: '#f6f6f6',
                        'stroke-width': 0
                    }
                }
            },
            {
                type: 'erd.Normal',
                attrs: {
                    '.': {
                        'data-tooltip': 'Normal',
                        'data-tooltip-position': 'left',
                        'data-tooltip-position-selector': '.joint-stencil'
                    },
                    '.outer': {
                        fill: '#feb663',
                        stroke: 'transparent',
                        'stroke-dasharray': '0'
                    },
                    text: {
                        text: 'Normal',
                        'font-size': 11,
                        'font-family': 'Roboto Condensed',
                        'font-weight': 'Normal',
                        fill: '#f6f6f6',
                        'stroke-width': 0
                    }
                }
            },
            {
                type: 'erd.Multivalued',
                attrs: {
                    '.': {
                        'data-tooltip': 'Mutltivalued',
                        'data-tooltip-position': 'left',
                        'data-tooltip-position-selector': '.joint-stencil'
                    },
                    '.outer': {
                        fill: 'transparent',
                        stroke: '#fe854f',
                        'stroke-dasharray': '0'
                    },
                    '.inner': {
                        fill: '#fe854f',
                        stroke: 'transparent',
                        rx: 33,
                        ry: 21,
                        'stroke-dasharray': '0'
                    },
                    text: {
                        text: 'MultiValued',
                        'font-size': 11,
                        'font-family': 'Roboto Condensed',
                        'font-weight': 'Normal',
                        fill: '#f6f6f6',
                        'stroke-width': 0
                    }
                }
            },
            {
                type: 'erd.Derived',
                attrs: {
                    '.': {
                        'data-tooltip': 'Derived',
                        'data-tooltip-position': 'left',
                        'data-tooltip-position-selector': '.joint-stencil'
                    },
                    '.outer': {
                        fill: 'transparent',
                        stroke: '#fe854f',
                        'stroke-dasharray': '0'
                    },
                    '.inner': {
                        fill: '#feb663',
                        stroke: 'transparent',
                        'display': 'block',
                        'stroke-dasharray': '0'
                    },
                    text: {
                        text: 'Derived',
                        fill: '#f6f6f6',
                        'font-family': 'Roboto Condensed',
                        'font-weight': 'Normal',
                        'font-size': 11,
                        'stroke-width': 0
                    }
                }
            }
        ],
        uml: [

            {
                type: 'uml.Class',
                name: 'Class',
                attributes: ['+attr1'],
                methods: ['-setAttr1()'],
                size: {
                    width: 150,
                    height: 100
                },
                attrs: {
                    '.': {
                        'data-tooltip': 'Class',
                        'data-tooltip-position': 'left',
                        'data-tooltip-position-selector': '.joint-stencil'
                    },
                    '.uml-class-name-rect': {
                        top: 2,
                        fill: '#61549C',
                        stroke: '#f6f6f6',
                        'stroke-width': 1,
                        rx: 8,
                        ry: 8
                    },
                    '.uml-class-attrs-rect': {
                        top: 2,
                        fill: '#61549C',
                        stroke: '#f6f6f6',
                        'stroke-width': 1,
                        rx: 8,
                        ry: 8
                    },
                    '.uml-class-methods-rect': {
                        top: 2,
                        fill: '#61549C',
                        stroke: '#f6f6f6',
                        'stroke-width': 1,
                        rx: 8,
                        ry: 8
                    },
                    '.uml-class-name-text': {
                        ref: '.uml-class-name-rect',
                        'ref-y': 0.5,
                        'y-alignment': 'middle',
                        fill: '#f6f6f6',
                        'font-family': 'Roboto Condensed',
                        'font-weight': 'Normal',
                        'font-size': 11
                    },
                    '.uml-class-attrs-text': {
                        ref: '.uml-class-attrs-rect',
                        'ref-y': 0.5,
                        'y-alignment': 'middle',
                        fill: '#f6f6f6',
                        'font-family': 'Roboto Condensed',
                        'font-weight': 'Normal',
                        'font-size': 11
                    },
                    '.uml-class-methods-text': {
                        ref: '.uml-class-methods-rect',
                        'ref-y': 0.5,
                        'y-alignment': 'middle',
                        fill: '#f6f6f6',
                        'font-family': 'Roboto Condensed',
                        'font-weight': 'Normal',
                        'font-size': 11
                    }
                }
            },
            {
                type: 'uml.Interface',
                name: 'Interface',
                attributes: ['+attr1'],
                methods: ['-setAttr1()'],
                size: {
                    width: 150,
                    height: 100
                },
                attrs: {
                    '.': {
                        'data-tooltip': 'Interface',
                        'data-tooltip-position': 'left',
                        'data-tooltip-position-selector': '.joint-stencil'
                    },
                    '.uml-class-name-rect': {
                        fill: '#fe854f',
                        stroke: '#f6f6f6',
                        'stroke-width': 1,
                        rx: 8,
                        ry: 8
                    },
                    '.uml-class-attrs-rect': {
                        fill: '#fe854f',
                        stroke: '#f6f6f6',
                        'stroke-width': 1,
                        rx: 8,
                        ry: 8
                    },
                    '.uml-class-methods-rect': {
                        fill: '#fe854f',
                        stroke: '#f6f6f6',
                        'stroke-width': 1,
                        rx: 8,
                        ry: 8
                    },
                    '.uml-class-name-text': {
                        ref: '.uml-class-name-rect',
                        'ref-y': 0.5,
                        'y-alignment': 'middle',
                        fill: '#f6f6f6',
                        'font-family': 'Roboto Condensed',
                        'font-weight': 'Normal',
                        'font-size': 11
                    },
                    '.uml-class-attrs-text': {
                        ref: '.uml-class-attrs-rect',
                        'ref-y': 0.5,
                        'y-alignment': 'middle',
                        fill: '#f6f6f6',
                        'font-family': 'Roboto Condensed',
                        'font-size': 11
                    },
                    '.uml-class-methods-text': {
                        ref: '.uml-class-methods-rect',
                        'ref-y': 0.5,
                        'y-alignment': 'middle',
                        fill: '#f6f6f6',
                        'font-family': 'Roboto Condensed',
                        'font-weight': 'Normal',
                        'font-size': 11
                    }
                }
            },
            {
                type: 'uml.Abstract',
                name: 'Abstract',
                attributes: ['+attr1'],
                methods: ['-setAttr1()'],
                size: {
                    width: 150,
                    height: 100
                },
                attrs: {
                    '.': {
                        'data-tooltip': 'Abstract',
                        'data-tooltip-position': 'left',
                        'data-tooltip-position-selector': '.joint-stencil'
                    },
                    '.uml-class-name-rect': {
                        fill: '#6a6c8a',
                        stroke: '#f6f6f6',
                        'stroke-width': 1,
                        rx: 8,
                        ry: 8
                    },
                    '.uml-class-attrs-rect': {
                        fill: '#6a6c8a',
                        stroke: '#f6f6f6',
                        'stroke-width': 1,
                        rx: 8,
                        ry: 8
                    },
                    '.uml-class-methods-rect': {
                        fill: '#6a6c8a',
                        stroke: '#f6f6f6',
                        'stroke-width': 1,
                        rx: 8,
                        ry: 8
                    },
                    '.uml-class-name-text': {
                        ref: '.uml-class-name-rect',
                        'ref-y': 0.5,
                        'y-alignment': 'middle',
                        fill: '#f6f6f6',
                        'font-family': 'Roboto Condensed',
                        'font-weight': 'Normal',
                        'font-size': 11
                    },
                    '.uml-class-attrs-text': {
                        ref: '.uml-class-attrs-rect',
                        'ref-y': 0.5,
                        'y-alignment': 'middle',
                        fill: '#f6f6f6',
                        'font-family': 'Roboto Condensed',
                        'font-weight': 'Normal',
                        'font-size': 11
                    },
                    '.uml-class-methods-text': {
                        ref: '.uml-class-methods-rect',
                        'ref-y': 0.5,
                        'y-alignment': 'middle',
                        fill: '#f6f6f6',
                        'font-family': 'Roboto Condensed',
                        'font-weight': 'Normal',
                        'font-size': 11
                    }
                }
            },

            {
                type: 'uml.State',
                name: 'State',
                events: ['entry/', 'create()'],
                size: {
                    width: 150,
                    height: 100
                },
                attrs: {
                    '.': {
                        'data-tooltip': 'State',
                        'data-tooltip-position': 'left',
                        'data-tooltip-position-selector': '.joint-stencil'
                    },
                    '.uml-state-body': {
                        fill: '#feb663',
                        stroke: '#f6f6f6',
                        'stroke-width': 1,
                        rx: 8,
                        ry: 8,
                        'stroke-dasharray': '0'
                    },
                    '.uml-state-separator': {
                        stroke: '#f6f6f6',
                        'stroke-width': 1,
                        'stroke-dasharray': '0'
                    },
                    '.uml-state-name': {
                        fill: '#f6f6f6',
                        'font-size': 11,
                        'font-family': 'Roboto Condensed',
                        'font-weight': 'Normal'
                    },
                    '.uml-state-events': {
                        fill: '#f6f6f6',
                        'font-size': 11,
                        'font-family': 'Roboto Condensed',
                        'font-weight': 'Normal'
                    }
                }
            }
        ],
        org: [

            {
                type: 'org.Member',
                attrs: {
                    '.': {
                        'data-tooltip': 'Member',
                        'data-tooltip-position': 'left',
                        'data-tooltip-position-selector': '.joint-stencil'
                    },
                    '.rank': {
                        text: 'Rank',
                        fill: '#f6f6f6',
                        'font-family': 'Roboto Condensed',
                        'font-size': 12,
                        'font-weight': 'Bold',
                        'text-decoration': 'none'
                    },
                    '.name': {
                        text: 'Person',
                        fill: '#f6f6f6',
                        'font-family': 'Roboto Condensed',
                        'font-weight': 'Normal',
                        'font-size': 10
                    },
                    '.card': {
                        fill: '#31d0c6',
                        stroke: 'transparent',
                        'stroke-width': 0,
                        'stroke-dasharray': '0'
                    },
                    image: {
                        width: 22,
                        height: 22,
                        x: 16,
                        y: 13,
                        'xlink:href': './assets/member-male.png'
                    }
                }
            }
        ]
    }
}


