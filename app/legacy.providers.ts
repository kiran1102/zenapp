export function getAngularJsHttp(injector) {
    return injector.get("$http");
}

export function getAngularJsScope(injector) {
    return injector.get("$rootScope");
}

export function getAngularJsQ(injector) {
    return injector.get("$q");
}
