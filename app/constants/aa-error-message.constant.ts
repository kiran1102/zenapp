import { AAErrorCode } from "enums/aa-error-code.enum";

export const AAErrorMessage = {
    [AAErrorCode.UNAUTHORIZED_INVENTORY_ACTION]:  "ApprovalPermissionDenied",
    [AAErrorCode.INVITED_USER_CREATE_FAILED]:  "InvitedUserCreationFailed",
    [AAErrorCode.INVALID_SEARCH_CRITERIA]: "InvalidSearchCriteria",
    [AAErrorCode.INVALID_APPROVER_RESPONDENT_CHANGE_AFTER_DEADLINE]: "InvalidUserOperationOnApproversOrRespondents",
};
