export const AssessmentCreationActions = {
    COMMENT_CHANGED: "COMMENT_CHANGED",
    DEADLINE_CHANGED: "DEADLINE_CHANGED",
    DESCRIPTION_CHANGED: "DESCRIPTION_CHANGED",
    ORG_SELECTED: "ORG_SELECTED",
    PROJECT_APPROVER_SELECTED: "PROJECT_APPROVER_SELECTED",
    PROJECT_RESPONDENT_SELECTED: "PROJECT_RESPONDENT_SELECTED",
    TOGGLE_SHOW_MORE_DETAILS: "TOGGLE_SHOW_MORE_DETAILS",
    NAME_CHANGED: "NAME_CHANGED",
    SELECT_TEMPLATE_VERSION: "SELECT_TEMPLATE_VERSION",
    EXPLICIT_CHANGE_REQUIRED: "EXPLICIT_CHANGE_REQUIRED",
};
