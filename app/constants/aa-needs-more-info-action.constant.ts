export const NeedsMoreInfoActions = {
    CancelReOpen: "CANCEL_REOPEN",
    CancelRespond: "CANCEL_RESPOND",
    CloseRequest: "CLOSE_REQUEST",
    Closed: "CLOSED",
    Delete: "DELETE",
    Open: "OPEN",
    ReOpen: "REOPEN",
    ReOpenSend: "REOPEN_SEND",
    Respond: "RESPOND",
    ResponseSend: "RESPONSE_SEND",
};
