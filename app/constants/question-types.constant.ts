export const QuestionTypeNames = {
    Date: "DATE",
    MultiChoice: "MULTICHOICE",
    Statement: "STATEMENT",
    TextBox: "TEXTBOX",
    YesNo: "YESNO",
    Inventory: "INVENTORY",
    Attribute: "ATTRIBUTE",
    PersonalData: "PERSONAL_DATA",
    Incident: "INCIDENT",
};

export const QuestionTypes = {
    Date: { name: QuestionTypeNames.Date, icon: "ot ot-calendar", label: "Date" },
    MultiChoice: { name: QuestionTypeNames.MultiChoice, icon: "ot ot-check", label: "MultiChoice" },
    Statement: { name: QuestionTypeNames.Statement, icon: "ot ot-quote-right", label: "Statement" },
    TextBox: { name: QuestionTypeNames.TextBox, icon: "ot ot-text", label: "Text" },
    YesNo: { name: QuestionTypeNames.YesNo, icon: "ot ot-yes-no", label: "YesOrNo" },
    Inventory: { name: QuestionTypeNames.Inventory, icon: "ot ot-list-ul", label: "Inventory" },
    Attribute: { name: QuestionTypeNames.Attribute, icon: "ot ot-attribute", label: "Attribute" },
    PersonalData: { name: QuestionTypeNames.PersonalData, icon: "ot ot-personal-data", label: "PersonalData" },
    Incident: { name: QuestionTypeNames.Incident, icon: "ot ot-exclamation-circle", label: "Incident" },
};

export const QuestionResponseType = {
    Justification: "JUSTIFICATION",
    NotApplicable: "NOT_APPLICABLE",
    NotSure: "NOT_SURE",
    Others: "OTHERS",
    Default: "DEFAULT", // YesNo, Multichoice
};

export const AttributeQuestionResponseType = {
    TEXT: "text",
    SINGLE_SELECT: "single-select",
    MULTI_SELECT: "multi-select",
    DATE: "date",
    DATE_TIME: "datetime",
    SELECT_TABLE: "select-table",
};

export const AttributeQuestionNameKey = {
    JURISDICTIONS: "Jurisdictions",
};

export const AttributeType = {
    NONE: "none",
    USERS: "users",
    LOCATIONS: "locations",
};

export const AAOtherOptionConfig = {
    isEditable: true,
    isButton: true,
    inputType: 10,
    clearOnSelect: false,
};

export const QuestionConfigurations = {
    IS_ANSWER_REQUIRED: "isAnswerRequired",
    IS_DESCRIPTION_ENABLED: "isDescriptionEnabled",
    IS_QUESTION_HINT_ENABLED: "isQuestionHintEnabled",
};
