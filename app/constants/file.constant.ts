export const BulkImportFileTypes = {
    CSV: "csv",
    XLSX: "xlsx",
};

export const RolesImportFileTypes = {
    XLSX: ".xlsx",
};

export const FileSizeLimit = {
    FILE_SIZE_LIMIT: 67108864,
};

export const FileConstraints = {
    FILE_SIZE_LIMIT: 67108864,
    FILE_NAME_MAX_LENGTH: 100,
};
