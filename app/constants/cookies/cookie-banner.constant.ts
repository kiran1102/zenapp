export const CookieBannerAccordions = {
    Layout: "Layout",
    Colors: "Colors",
    Content: "Content",
    Behavior: "Behavior",
};

export const CookieBannerColorType = {
    ButtonBgcolor: "ButtonBgcolor",
    TextColor: "TextColor",
    BannerBgColor: "BannerBgColor",
};

export const EDITABLE_BANNER_ITEMS = {
    SHOW_ADD_POLICY_LINK: "showAddPolicyLink",
    SHOW_COOKIE_SETTINGS: "showCookieSettings",
    SHOW_ACCEPT_COOKIES: "showAcceptCookies",
    CLOSE_ACCEPTS_ALL_COOKIES: "closeAcceptsAllCookies",
    SCROLL_ACCEPTS_ALL_COOKIES: "scrollAcceptsAllCookies",
    ON_CLICK_ACCEPT_ALL_COOKIES: "onClickAcceptAllCookies",
    SCROLL_ACCEPTS_COOKIES: "scrollAcceptsCookies",
    SHOW_ONLY_IN_EU: "showOnlyInEU",
    HIDE_FOR_ALL: "hideForAll",
    FORCE_CONSENT: "forceConsent",
    BANNER_PUSHES_DOWN: "bannerPushesDown",
    SHOW_BANNER_CLOSE_BUTTON: "showBannerCloseButton",
};
