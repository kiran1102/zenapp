export const ScriptArchiveColumnTypes = {
    Action: "action",
    ConsentModel: "consentModel",
    Language: "language",
    PublishedBy: "publisherName",
    PublishedDate: "publishedDate",
    ReConsentRequired: "reConsentRequired",
};
