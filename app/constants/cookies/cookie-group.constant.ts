export const CookieGroup = {
    PerformanceCookies: "PerformanceCookies",
    FunctionalityCookies: "FunctionalityCookies",
    TargetingCookies: "TargetingCookies",
    SocialMediaCookies: "SocialMediaCookies",
    StrictlyNecessaryCookies: "StrictlyNecessaryCookies",
};
