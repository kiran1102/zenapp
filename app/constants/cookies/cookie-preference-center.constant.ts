export const PrefCenterColorType = {
    PrimaryColor: "PrimaryColor",
    ButtonColor: "ButtonColor",
    MenuColor: "MenuColor",
    MenuHighlightColor: "MenuHighlightColor",
};

export const PreferenceCenterFormDataTypes = {
    INPUT: "INPUT",
    TEXTAREA: "TEXTAREA",
};

export const PreferenceStyles = {
    MODERN: "MODERN",
    CLASSIC: "CLASSIC",
};
