export const CoookieConsentPolicyRuleControls = {
    ConsentModel: "consentModel",
    EnableIAB: "enableIAB",
    EnableLogging: "enabledConsentLogging",
    ImpliedBehaviors: "impliedBehaviors",
    ShowImpliedBehaviors: "showImpliedBehaviors",
    RuleName: "name",
    ShowBanner: "showBanner",
    SiteVisitorId: "siteVisitorId",
    VendorVersion: "vendorVersion",
    ThirdPartyEuConsentCookie: "thirdPartyEuConsentCookie",
    StatusModal: "statusModal",
};

export const CookieConsentPolicyDetailTabs = {
    RegionRules: "RegionRules",
    AssignedDomains: "AssignedDomains",
};

export const CookieConsentPolicyStatus = {
    Draft: "Draft",
    Published: "Published",
};

export const CookieConsentPolicyDetailGroupColumns = {
    GroupName: "GroupName",
    Status: "Status",
    DoNotTrack: "DoNotTrack",
};

export const ImpliedBehaviorOptionKeys = {
    ScrollAcceptAll: "ScrollAcceptAll",
    NextPage: "NextPage",
    ExcludePages: "ExcludePages",
    ClickAcceptAll: "ClickAcceptAll",
};
