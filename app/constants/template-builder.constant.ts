export const BuilderDraggableTypes = {
    BuilderSection: "BuilderSection",
    BuilderQuestion: "BuilderQuestion",
    BuilderQuestionIcon: "BuilderQuestionIcon",
    None: "None",
};
