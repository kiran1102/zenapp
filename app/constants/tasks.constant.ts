export const TASK_STATES = {
    COMPLETE: "TaskCompleted",
    OPEN: "TaskOpen",
    FAILED: "TaskFailed",
};

export const TASK_STATE_CLASSES = {
    COMPLETE: "success",
    OPEN: "primary",
    FAILED: "destructive",
    UNKNOWN: "",
};

export const TaskStateKeys = {
    10: TASK_STATES.OPEN,
    20: TASK_STATES.COMPLETE,
    30: TASK_STATES.FAILED,
};

export const TaskStateIds = {
    TaskOpen: 10,
    TaskCompleted: 20,
    TaskFailed: 30,
};

export const TaskTypes = {
    User: "user",
    System: "system",
};

export const TaskDetailType = {
    FILE_DOWNLOAD: "file-download",
    LINK: "link",
};
