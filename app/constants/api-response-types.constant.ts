export const APIResponseTypes = {
    ARRAY_BUFFER: "arraybuffer",
    BLOB: "blob",
    JSON: "json",
    TEXT: "text",
};
