import { InventoryTableIds } from "enums/inventory.enum";

export const InventorySchemaIds = {
    Elements: "data-elements",
    Assets: "assets",
    Processes: "processing-activities",
    Vendors: "vendors",
    Entities: "entities",
};

export const NewInventorySchemaIds = {
    [InventoryTableIds.Elements]: InventorySchemaIds.Elements,
    [InventoryTableIds.Assets]: InventorySchemaIds.Assets,
    [InventoryTableIds.Processes]: InventorySchemaIds.Processes,
    [InventoryTableIds.Vendors]: InventorySchemaIds.Vendors,
    [InventoryTableIds.Entities]: InventorySchemaIds.Entities,
};

export const LegacyInventorySchemaIds = {
    [InventorySchemaIds.Elements]: InventoryTableIds.Elements,
    [InventorySchemaIds.Assets]: InventoryTableIds.Assets,
    [InventorySchemaIds.Processes]: InventoryTableIds.Processes,
    [InventorySchemaIds.Vendors]: InventoryTableIds.Vendors,
    [InventorySchemaIds.Entities]: InventoryTableIds.Entities,
};

export const InventoryTypeTranslations = {
    [InventoryTableIds.Elements]: "Elements",
    [InventoryTableIds.Assets]: "Assets",
    [InventoryTableIds.Processes]: "ProcessingActivities",
    [InventoryTableIds.Vendors]: "Vendors",
    [InventoryTableIds.Entities]: "LegalEntities",
};
