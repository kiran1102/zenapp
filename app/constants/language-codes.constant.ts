export const LanguageCodes = {
    Danish: "da",
    Dutch: "nl",
    English: "en-us",
    French: "fr",
    German: "de",
    Italian: "it",
    Japanese: "ja",
    Portuguese: "pt",
    Romanian: "ro",
    Spanish: "es",
    Swedish: "sv",
};
