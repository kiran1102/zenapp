import { InventoryTableIds, InventoryContexts } from "enums/inventory.enum";
import { RiskPermissions } from "constants/risk.constants";
import { GridViewStorageNames } from "sharedModules/enums/grid-view-storage.enum";
import { InventorySchemaIds } from "constants/inventory-config.constant";
import { BulkImportCards } from "modules/admin/enums/bulk-import.enums";

export const STATUS = {
    New: "New",
    InDiscovery: "InDiscovery",
    Complete: "Discovered",
};

export const ModuleContext = {
    Inventory: "inventory",
    Vendor: "vendor",
};

export const InventoryListTypes = {
    DataSubjectTypes: "data-subjects",
    DataElements: "data-elements",
    DataCategories: "data-categories",
    DataClassifications: "data-classifications",
};

export const InventoryListActionContext = {
    List: "list",
    Details: "details",
};

export const AttributeFieldNames = {
    Name: "name",
    Org: "organization",
    Location: "location",
    Risk: "riskLevel",
    RiskScore: "riskScore",
    Description: "description",
    Type: "type",
    Status: "status",
    ExternalId: "externalId",
    Number: "number",
};

// TODO remove when vendor-controls merge into link-controls
export const ShowVendorControlList = {
    [InventoryTableIds.Vendors]: true,
    [InventoryTableIds.Assets]: false,
    [InventoryTableIds.Processes]: false,
};

export const InventoryTypeGetOrgById: Array<InventoryTableIds | string> = [
    InventoryTableIds.Incidents,
    InventoryTableIds.Assets,
    InventoryTableIds.Processes,
    InventoryTableIds.Vendors,
    InventoryTableIds.Entities,
    InventoryTableIds.Incidents.toString(),
    InventoryTableIds.Assets.toString(),
    InventoryTableIds.Processes.toString(),
    InventoryTableIds.Vendors.toString(),
    InventoryTableIds.Entities.toString(),
];

// TODO move to inventory-config.constant
export const InventoryAttributeCodes: any = {
    [InventoryTableIds.Elements]: {
        Data_Subjects: "_ATT_DEDA01",
        Data_Categories: "_ATT_DEDA02",
        Data_Elements: "_ATT_DEDA03",
        Organization_Group: "_ATT_DEOR04",
        Data_Classification: "_ATT_DEDA05",
        Status: "_ATT_DES",
    },
    [InventoryTableIds.Assets]: {
        Asset: "_ATT_AAS01",
        Managing_Organization: "_ATT_AMA02",
        Hosting_Location: "_ATT_AHO03",
        Type: "_ATT_ATY04",
        Description: "_ATT_ADE05",
        Internal_or_3rd_Party_Storage: "_ATT_AIN06",
        Hosting_Provider: "_ATT_AHO07",
        Storage_Format: "_ATT_AST08",
        Technical_Security_Measures: "_ATT_ATE09",
        Organizational_Security_Measures: "_ATT_AOR10",
        Other_Security_Measures: "_ATT_AOT11",
        Volume_of_Data_Subjects: "_ATT_AVO12",
        Data_Retention: "_ATT_ADA13",
        IT_Owner: "_ATT_AIT14",
        Business_Owner: "_ATT_ABU15",
        Privacy_Owner: "_ATT_APR16",
        Categories_of_Data: "_ATT_ACA17",
        Processing_Activities: "_ATT_APR18",
        Status: "_ATT_AAS",
        Risk: "_ATT_ARL",
    },
    [InventoryTableIds.Processes]: {
        Processing_Activity: "_ATT_PAPR01",
        Managing_Organization: "_ATT_PAMA02",
        Description: "_ATT_PADE03",
        Type: "_ATT_PATY04",
        Categories_of_Data_Subjects: "_ATT_PACA05",
        Data_Elements: "_ATT_PADA06",
        Data_Subjects_Region: "_ATT_PADA07",
        Purpose_of_Processing: "_ATT_PAPU08",
        Data_Subjects_Volume: "_ATT_PADA09",
        Data_Source: "_ATT_PADA10",
        Source_Asset: "_ATT_PASO11",
        Business_Process_Owner: "_ATT_PABU12",
        Primary_Storage_Asset: "_ATT_PAPR13",
        Transfer_Method_From_Sources: "_ATT_PATR14",
        Destination_Asset: "_ATT_PADE15",
        Transfer_Method_To_Destinations: "_ATT_PATR16",
        Parties_who_Access_Use_Data: "_ATT_PAPA17",
        Locations_of_Parties_Access_Use: "_ATT_PALO18",
        Categories_of_Data: "_ATT_PACA19",
        Status: "_ATT_PAAS",
        Risk: "_ATT_PARL",
    },
    [InventoryTableIds.Vendors]: {
        Name: "name",
        Location: "location",
        Description: "description",
        ProductService: "productService",
        Organization: "organization",
        Owner: "owner",
        Rating: "vendorRating",
        Criticality: "vendorCriticality",
        VendorId: "vendorId",
        Classification: "vendorClassification",
        PrimaryContact: "primaryContact",
        Risk: "riskLevel",
        VendorType: "type",
    },
    [InventoryTableIds.Entities]: {
    },
};

// TODO move to inventory-config.constant
export const InventoryTranslationKeys: any = {
    [InventoryTableIds.Assets]: {
        linkAssessmentText: "AssociateAssessmentsToAsset",
        relatedInventoryHeader: "RelatedAssets",
        addRelatedInventoryHeader: "AddRelatedAsset",
        addRelatedInventoryButton: "AddAsset",
        howIsInventoryRelatedText: "HowIsAssetRelated",
        chooseInventoryItem: "ChooseAnAsset",
        copyInventoryItem: "CopyAsset",
        itemName: "Asset",
        exportV2: "ExportMultitab",
    },
    [InventoryTableIds.Processes]: {
        linkAssessmentText: "AssociateAssessmentsToPA",
        relatedInventoryHeader: "RelatedProcessingActivities",
        addRelatedInventoryHeader: "AddRelatedProcessingActivities",
        addRelatedInventoryButton: "AddProcessingActivities",
        howIsInventoryRelatedText: "HowIsProcessingActivityRelated",
        chooseInventoryItem: "ChooseProcessingActivities",
        copyInventoryItem: "CopyProcessingActivity",
        itemName: "ProcessingActivity",
        exportV2: "ExportMultitab",
    },
    [InventoryTableIds.Elements]: {
        relatedInventoryHeader: "RelatedDataElements",
        addRelatedInventoryHeader: "ManageDataElements",
        addRelatedInventoryButton: "ManageDataElements",
    },
    [InventoryTableIds.Vendors]: {
        linkAssessmentText: "AssociateAssessmentsToVendor",
        relatedInventoryHeader: "RelatedVendors",
        addRelatedInventoryHeader: "AddRelatedVendor",
        addRelatedInventoryButton: "AddVendor",
        chooseInventoryItem: "ChooseVendor",
        copyInventoryItem: "CopyVendor",
        howIsInventoryRelatedText: "HowIsVendorRelated",
        itemName: "Vendor",
        exportV2: "Export",
    },
    [InventoryTableIds.Entities]: {
        relatedInventoryHeader: "RelatedEntities",
        addRelatedInventoryHeader: "AddRelatedEntity",
        addRelatedInventoryButton: "AddEntity",
        chooseInventoryItem: "ChooseEntity",
        copyInventoryItem: "CopyLegalEntity",
        howIsInventoryRelatedText: "HowIsEntityRelated",
    },
    // V2 Ids
    [InventorySchemaIds.Elements]: {
        inventoryName: "Elements",
        attributes: "ElementAttributes",
        addModalTitle: "AddElement",
    },
    [InventorySchemaIds.Assets]: {
        inventoryName: "Assets",
        attributes: "AssetAttributes",
        addModalTitle: "AddAsset",
        linkPersonalDataHelpText: "LinkPersonalDataAssetHelpText",
    },
    [InventorySchemaIds.Processes]: {
        inventoryName: "ProcessingActivities",
        attributes: "ProcessAttributes",
        addModalTitle: "AddProcessingActivity",
        linkPersonalDataHelpText: "LinkPersonalDataProcessingHelpText",
    },
    [InventorySchemaIds.Vendors]: {
        inventoryName: "Vendors",
        attributes: "VendorAttributes",
        addModalTitle: "AddVendor",
        linkPersonalDataHelpText: "LinkPersonalDataAssetHelpText",
    },
    [InventorySchemaIds.Entities]: {
        inventoryName: "Entities",
        attributes: "EntityAttributes",
        addModalTitle: "AddLegalEntity",
    },

    // List Types
    [InventoryListTypes.DataSubjectTypes]: {
        listTitle: "DataSubjectTypes",
        listDetailsTitle: "DataSubjectTypesDetails",
        addModalTitle: "CreateNewDataSubjectType",
        editModalTitle: "EditDataSubjectType",
    },
    [InventoryListTypes.DataElements]: {
        listTitle: "DataElements",
        listDetailsTitle: "DataElementsDetails",
        addModalTitle: "CreateNewDataElement",
        editModalTitle: "EditDataElement",
    },
    [InventoryListTypes.DataCategories]: {
        listTitle: "DataCategories",
        listDetailsTitle: "DataCategoriesDetails",
        addModalTitle: "CreateNewDataCategory",
        editModalTitle: "EditDataCategory",
    },
    [InventoryListTypes.DataClassifications]: {
        listTitle: "DataClassifications",
        listDetailsTitle: "DataClassificationsDetails",
        addModalTitle: "CreateNewDataClassification",
        editModalTitle: "EditDataClassification",
    },
};

// TODO move to inventory-config.constants
export const InventoryPermissions = {
    [InventoryTableIds.Entities]: {
        add: "DataMappingAddEntities",
        edit: "DataMappingEditEntities",
        delete: "DataMappingDeleteEntities",
        details: "DataMappingEntitiesInventory",
        launch: "DataMappingEntitiesInventory",
        inventoryLinkingV2: "InventoryLinkingEnabled",
        attachments: "FileRead",
        createAttachments: "FileCreate",
        deleteAttachments: "FileDelete",
        viewRelated: "InventoryViewRelated",
        viewActivity: "InventoryViewAuditHistory",
        viewRisks: "DataMappingEntitiesInventory",
        viewRiskLevel: "DataMappingEntitiesInventory",
        editRiskLevel: "DataMappingEntitiesInventory",
        riskSummaryView: "DataMappingEntitiesInventory",
        riskSummaryEdit: "DataMappingEntitiesInventory",
        riskRecommendationView: "DataMappingEntitiesInventory",
        riskRecommendationEdit: "DataMappingEntitiesInventory",
        deleteRisk: "DataMappingEntitiesInventory",
        addRisk: "DataMappingEntitiesInventory",
        bulkEdit: "DataMappingEditEntities",
        export: "DataMappingExportEntities",
        exportV2: "DataMappingExportEntities",
        copy: "CopyInventory",
        updateRelatedInventory: "DataMappingInventoryManageRelated",
        viewAssessmentLinks: "DataMappingAssessmentsList",
        viewDMAssessmentLinks: "DataMappingAssessmentsList",
        launchV2Assessment: "DMAssessmentV2",
        addAssessmentLink: "ProjectsInventoryAssociate",
        viewPIAAssessmentLinks: "ProjectsInventoryRead",
        removeAssessmentLinks: "ProjectsInventoryDelete",
        changeInventoryStatus: "DataMappingInventoryChangeStatus",
    },
    [InventoryTableIds.Elements]: {
        add: "DataMappingAttributesDataElementsAdd",
        edit: "DataMappingAttributesDataElementsEdit",
        delete: "DataMappingAttributesDataElementsDelete",
        export: "DataMappingInventoryElementExport",
        updateRelatedInventory: "DataMappingInventoryManageRelated",
    },
    [InventoryTableIds.Assets]: {
        add: "DataMappingInventoryAssetsAdd",
        edit: "DataMappingInventoryAssetsEdit",
        delete: "DataMappingInventoryAssetsDelete",
        details: "DataMappingInventoryAssetsDetails",
        export: "DataMappingInventoryAssetsExport",
        exportV2: "InventoryLinkingEnabled",
        launch: "DataMappingInventoryAssetsLaunchAssessment",
        copy: "CopyInventory",
        viewRisks: RiskPermissions.InventoryAssetRiskView,
        viewRiskLevel: RiskPermissions.InventoryAssetRiskLevelView,
        editRiskLevel: RiskPermissions.InventoryAssetRiskLevelEdit,
        riskSummaryView: RiskPermissions.InventoryAssetRiskSummaryView,
        riskSummaryEdit: RiskPermissions.InventoryAssetRiskSummaryEdit,
        riskRecommendationView: RiskPermissions.InventoryAssetRiskRecommendationView,
        riskRecommendationEdit: RiskPermissions.InventoryAssetRiskRecommendationEdit,
        deleteRisk: RiskPermissions.InventoryAssetRiskDelete,
        addRisk: RiskPermissions.InventoryAssetRiskCreate,
        addAssessmentLink: "ProjectsInventoryAssociate",
        viewAssessmentLinks: "DataMappingAssessmentsList",
        viewDMAssessmentLinks: "DataMappingAssessmentsList",
        viewPIAAssessmentLinks: "ProjectsInventoryRead",
        removeAssessmentLinks: "ProjectsInventoryDelete",
        attachments: "FileRead",
        createAttachments: "FileCreate",
        deleteAttachments: "FileDelete",
        changeStatus: "InventoryChangeStatus",
        viewActivity: "InventoryViewAuditHistory",
        bulkEdit: "InventoryBulkEdit",
        inventoryLinkingV2: "InventoryLinkingEnabled",
        launchV2Assessment: "DMAssessmentV2",
        contracts: "FileRead",
        viewRelated: "InventoryViewRelated",
        riskDeadlineView: "riskDeadlineView",
        riskDeadlineEdit: "riskDeadlineEdit",
        riskOwnerView: "riskOwnerView",
        riskOwnerEdit: "riskOwnerEdit",
        changeInventoryStatus: "DataMappingInventoryChangeStatus",
        vendorContractInventoryLink: "VendorContractInventoryLink",
        vendorContractInventoryLinkAdd: "VendorContractInventoryLinkAdd",
        vendorContractInventoryLinkRemove: "VendorContractInventoryLinkRemove",
        certificatesPanel: "VRMCertificates",
        updateRelatedInventory: "DataMappingInventoryManageRelated",
        vendorDocumentTab: "VendorDocumentTab",
    },
    [InventoryTableIds.Processes]: {
        add: "DataMappingInventoryPAAdd",
        edit: "DataMappingInventoryPAEdit",
        delete: "DataMappingInventoryPADelete",
        details: "DataMappingInventoryPADetails",
        export: "DataMappingInventoryPAExport",
        exportV2: "InventoryLinkingEnabled",
        launch: "DataMappingInventoryPALaunch",
        copy: "CopyInventory",
        exportArt30: "DataMappingInventoryArticle30",
        viewRisks: RiskPermissions.InventoryProcessRiskView,
        viewRiskLevel: RiskPermissions.InventoryProcessRiskLevelView,
        editRiskLevel: RiskPermissions.InventoryProcessRiskLevelEdit,
        riskSummaryView: RiskPermissions.InventoryProcessRiskSummaryView,
        riskSummaryEdit: RiskPermissions.InventoryProcessRiskSummaryEdit,
        riskRecommendationView: RiskPermissions.InventoryProcessRiskRecommendationView,
        riskRecommendationEdit: RiskPermissions.InventoryProcessRiskRecommendationEdit,
        deleteRisk: RiskPermissions.InventoryProcessRiskDelete,
        addRisk: RiskPermissions.InventoryProcessRiskCreate,
        addAssessmentLink: "ProjectsInventoryAssociate",
        viewAssessmentLinks: "DataMappingAssessmentsList",
        viewDMAssessmentLinks: "DataMappingAssessmentsList",
        viewPIAAssessmentLinks: "ProjectsInventoryRead",
        removeAssessmentLinks: "ProjectsInventoryDelete",
        attachments: "FileRead",
        createAttachments: "FileCreate",
        deleteAttachments: "FileDelete",
        viewActivity: "InventoryViewAuditHistory",
        viewLineage: "DataMappingDataMapsLineage",
        bulkEdit: "InventoryBulkEdit",
        exportPdf: "DataMappingInventoryPdfExport",
        exportDefaultPdf: "ExportDefaultPAPDF",
        setDefaultPdf: "SetDefaultPAPDF",
        inventoryLinkingV2: "InventoryLinkingEnabled",
        launchV2Assessment: "DMAssessmentV2",
        viewRelated: "InventoryViewRelated",
        riskDeadlineView: "riskDeadlineView",
        riskDeadlineEdit: "riskDeadlineEdit",
        riskOwnerView: "riskOwnerView",
        riskOwnerEdit: "riskOwnerEdit",
        changeInventoryStatus: "DataMappingInventoryChangeStatus",
        vendorContractInventoryLink: "VendorContractInventoryLink",
        vendorContractInventoryLinkAdd: "VendorContractInventoryLinkAdd",
        vendorContractInventoryLinkRemove: "VendorContractInventoryLinkRemove",
        contracts: "FileRead",
        certificatesPanel: null, // Permission Turned Off for now: "VRMCertificates"
        updateRelatedInventory: "DataMappingInventoryManageRelated",
        vendorDocumentTab: "VendorDocumentTab",
    },
    [InventoryTableIds.Vendors]: {
        add: "DataMappingInventoryVRMAdd",
        edit: "DataMappingInventoryVRMEdit",
        delete: "DataMappingInventoryVRMDelete",
        details: "DataMappingInventoryVRMDetails",
        copy: "CopyInventory",
        export: "ExportVendorInventory",
        exportV2: "ExportVendorInventory",
        launchVendorAssessment: "LaunchVendorAssessment",
        viewAssessmentLinks: "VendorRiskManagement",
        viewAssessmentV2Links: "VendorRiskManagement",
        viewRisks: "VendorRiskManagement",
        viewRiskLevel: "VendorRiskManagement",
        editRiskLevel: "VendorRiskManagement",
        riskSummaryView: "VendorRiskManagement",
        riskSummaryEdit: "VendorRiskManagement",
        riskRecommendationView: "VendorRiskManagement",
        riskRecommendationEdit: "VendorRiskManagement",
        deleteRisk: "VendorRiskManagement",
        addRisk: "VendorRiskManagement",
        addAssessmentLink: "ProjectsInventoryAssociate",
        removeAssessmentLinks: "ProjectsInventoryDelete",
        attachments: "FileRead",
        createAttachments: "FileCreate",
        deleteAttachments: "FileDelete",
        viewActivity: "InventoryViewAuditHistory",
        deleteAssessments: "AssessmentDelete",
        bulkEdit: "InventoryBulkEdit",
        launchV2Assessment: "DMAssessmentV2",
        inventoryLinkingV2: "InventoryLinkingEnabled",
        vendorDocumentTab: "VendorDocumentTab",
        contracts: "FileRead",
        createContracts: "FileCreate",
        deleteContracts: "FileDelete",
        downloadContracts: "FileCreate",
        viewRelated: "VendorRiskManagement",
        riskDeadlineView: "riskDeadlineView",
        riskDeadlineEdit: "riskDeadlineEdit",
        riskOwnerView: "riskOwnerView",
        riskOwnerEdit: "riskOwnerEdit",
        changeInventoryStatus: "DataMappingInventoryChangeStatus",
        certificatesPanel: "VRMCertificates",
        updateRelatedInventory: "DataMappingInventoryVRMEdit",
    },
    // V2 Ids
    [InventorySchemaIds.Elements]: {
        viewAttributes: "InventoryViewAttributes",
        addAttributes: "InventoryAddAttributes",
        enableAttributeOptions: "InventoryAddAttributes",
        disableAttributeOptions: "InventoryAddAttributes",
        editAttributeOptions: "InventoryAddAttributes",
    },
    [InventorySchemaIds.Assets]: {
        viewAttributes: "InventoryViewAttributes",
        addAttributes: "InventoryAddAttributes",
        enableAttributeOptions: "InventoryAddAttributes",
        disableAttributeOptions: "InventoryAddAttributes",
        editAttributeOptions: "InventoryAddAttributes",
        bulkEdit: "InventoryBulkEdit",
        attributeGrouping: "ConfigureAttributeGroups",
        viewDetails: "DataMappingInventoryAssetsDetails",
        viewRisks: RiskPermissions.InventoryProcessRiskView,
        viewRiskLevel: RiskPermissions.InventoryProcessRiskLevelView,
        editRiskLevel: RiskPermissions.InventoryProcessRiskLevelEdit,
        riskSummaryView: RiskPermissions.InventoryProcessRiskSummaryView,
        riskSummaryEdit: RiskPermissions.InventoryProcessRiskSummaryEdit,
        riskRecommendationView: RiskPermissions.InventoryProcessRiskRecommendationView,
        riskRecommendationEdit: RiskPermissions.InventoryProcessRiskRecommendationEdit,
        deleteRisk: RiskPermissions.InventoryProcessRiskDelete,
        addRisk: RiskPermissions.InventoryProcessRiskCreate,
        changeInventoryStatus: "DataMappingInventoryChangeStatus",
        inventoryLinkingV2: "InventoryLinkingEnabled",
        bulkImportCreate: "DataMappingAssetBulkCreate",
        bulkImportUpdate: "DataMappingAssetBulkUpdate",
        bulkImportAddControls: null,
    },
    [InventorySchemaIds.Processes]: {
        viewAttributes: "InventoryViewAttributes",
        addAttributes: "InventoryAddAttributes",
        enableAttributeOptions: "InventoryAddAttributes",
        disableAttributeOptions: "InventoryAddAttributes",
        editAttributeOptions: "InventoryAddAttributes",
        bulkEdit: "InventoryBulkEdit",
        attributeGrouping: "ConfigureAttributeGroups",
        viewDetails: "DataMappingInventoryPADetails",
        viewRisks: RiskPermissions.InventoryProcessRiskView,
        viewRiskLevel: RiskPermissions.InventoryProcessRiskLevelView,
        editRiskLevel: RiskPermissions.InventoryProcessRiskLevelEdit,
        riskSummaryView: RiskPermissions.InventoryProcessRiskSummaryView,
        riskSummaryEdit: RiskPermissions.InventoryProcessRiskSummaryEdit,
        riskRecommendationView: RiskPermissions.InventoryProcessRiskRecommendationView,
        riskRecommendationEdit: RiskPermissions.InventoryProcessRiskRecommendationEdit,
        deleteRisk: RiskPermissions.InventoryProcessRiskDelete,
        addRisk: RiskPermissions.InventoryProcessRiskCreate,
        changeInventoryStatus: "DataMappingInventoryChangeStatus",
        inventoryLinkingV2: "InventoryLinkingEnabled",
        bulkImportCreate: "DataMappingPABulkCreate",
        bulkImportUpdate: "DataMappingPABulkUpdate",
        bulkImportAddControls: null,
    },
    [InventorySchemaIds.Vendors]: {
        viewAttributes: "VendorRiskManagement",
        addAttributes: "InventoryAddAttributes",
        enableAttributeOptions: "InventoryAddAttributes",
        disableAttributeOptions: "InventoryAddAttributes",
        editAttributeOptions: "InventoryAddAttributes",
        bulkEdit: "InventoryBulkEdit",
        attributeGrouping: "ConfigureAttributeGroups",
        viewDetails: "DataMappingInventoryVRMDetails",
        viewRisks: RiskPermissions.InventoryProcessRiskView,
        viewRiskLevel: RiskPermissions.InventoryProcessRiskLevelView,
        editRiskLevel: RiskPermissions.InventoryProcessRiskLevelEdit,
        riskSummaryView: RiskPermissions.InventoryProcessRiskSummaryView,
        riskSummaryEdit: RiskPermissions.InventoryProcessRiskSummaryEdit,
        riskRecommendationView: RiskPermissions.InventoryProcessRiskRecommendationView,
        riskRecommendationEdit: RiskPermissions.InventoryProcessRiskRecommendationEdit,
        deleteRisk: RiskPermissions.InventoryProcessRiskDelete,
        addRisk: RiskPermissions.InventoryProcessRiskCreate,
        viewLineage: "DataMappingDataMapsLineage",
        changeInventoryStatus: "DataMappingInventoryChangeStatus",
        inventoryLinkingV2: "InventoryLinkingEnabled",
        bulkImportCreate: "VendorBulkImport",
        bulkImportUpdate: "VendorBulkImport",
        bulkImportAddControls: "VRMBulkImportControlsOnVendor",
    },
    [InventorySchemaIds.Entities]: {
        inventoryLinkingV2: "InventoryLinkingEnabled",
        bulkImportCreate: "DataMappingEntitiesBulkCreate",
        bulkImportUpdate: "DataMappingEntitiesBulkUpdate",
        bulkImportAddControls: null, // Permission Turned Off for now: "VRMBulkImportControlsOnVendor"
        viewDetails: "DataMappingEntitiesInventory",
        copy: "CopyInventory",
        viewRisks: "DataMappingEntitiesInventory",
        viewRiskLevel: "DataMappingEntitiesInventory",
        editRiskLevel: "DataMappingEntitiesInventory",
        riskSummaryView: "DataMappingEntitiesInventory",
        riskSummaryEdit: "DataMappingEntitiesInventory",
        riskRecommendationView: "DataMappingEntitiesInventory",
        riskRecommendationEdit: "DataMappingEntitiesInventory",
        deleteRisk: "DataMappingEntitiesInventory",
        addRisk: "DataMappingEntitiesInventory",
        addAttributes: "InventoryAddAttributes",
        attributeGrouping: "ConfigureAttributeGroups",
        viewAttributes: "InventoryViewAttributes",
        export: "DataMappingExportEntities",
        bulkEdit: "DataMappingEditEntities",
        enableAttributeOptions: "InventoryAddAttributes",
        disableAttributeOptions: "InventoryAddAttributes",
        editAttributeOptions: "InventoryAddAttributes",
        viewAssessmentLinks: "DataMappingAssessmentsList",
        viewDMAssessmentLinks: "DataMappingAssessmentsList",
        changeInventoryStatus: "DataMappingInventoryChangeStatus",
        launchV2Assessment: "DMAssessmentV2",
        addAssessmentLink: "ProjectsInventoryAssociate",
        viewPIAAssessmentLinks: "ProjectsInventoryRead",
        removeAssessmentLinks: "ProjectsInventoryDelete",
    },
};

const elementAttribute: any = InventoryAttributeCodes[InventoryTableIds.Elements];
const assetAttribute: any = InventoryAttributeCodes[InventoryTableIds.Assets];
const processAttribute: any = InventoryAttributeCodes[InventoryTableIds.Processes];
const vendorAttribute: any = InventoryAttributeCodes[InventoryTableIds.Vendors];

export const AttributeAllowedContexts: any = {
    [InventoryContexts.List]: {
        [elementAttribute.Data_Subjects]: true,
        [elementAttribute.Data_Categories]: true,
        [elementAttribute.Data_Elements]: true,
        [elementAttribute.Organization_Group]: true,
        [elementAttribute.Data_Classification]: true,

        [assetAttribute.Asset]: true,
        [assetAttribute.Managing_Organization]: true,
        [assetAttribute.Hosting_Location]: true,
        [assetAttribute.Type]: true,
        [assetAttribute.IT_Owner]: true,
        [assetAttribute.Status]: true,
        [assetAttribute.Risk]: true,

        [processAttribute.Processing_Activity]: true,
        [processAttribute.Managing_Organization]: true,
        [processAttribute.Business_Process_Owner]: true,
        [processAttribute.Status]: true,
        [processAttribute.Risk]: true,

        [vendorAttribute.Name]: true,
        [vendorAttribute.Organization]: true,
        [vendorAttribute.Owner]: true,
        [vendorAttribute.Rating]: true,
        [vendorAttribute.Criticality]: true,
        [vendorAttribute.VendorId]: true,
        [vendorAttribute.Classification]: true,
        [vendorAttribute.PrimaryContact]: true,
        [vendorAttribute.Risk]: true,
        [vendorAttribute.VendorType]: true,
    },
    [InventoryContexts.Add]: {
        [elementAttribute.Data_Subjects]: true,
        [elementAttribute.Data_Categories]: true,
        [elementAttribute.Data_Elements]: true,
        [elementAttribute.Organization_Group]: true,
        [elementAttribute.Data_Classification]: true,

        [assetAttribute.Asset]: true,
        [assetAttribute.Managing_Organization]: true,
        [assetAttribute.Hosting_Location]: true,
        [assetAttribute.Type]: true,
        [assetAttribute.IT_Owner]: true,

        [processAttribute.Processing_Activity]: true,
        [processAttribute.Managing_Organization]: true,
        [processAttribute.Business_Process_Owner]: true,

        [vendorAttribute.Name]: true,
        [vendorAttribute.VendorType]: true,
        [vendorAttribute.Organization]: true,
    },
    [InventoryContexts.LineageSidebar]: {
        [processAttribute.Locations_of_Parties_Access_Use]: true,
    },
};

export const AttributeHiddenContexts = {
    [InventoryContexts.Details]: {
        [processAttribute.Categories_of_Data_Subjects]: true,
        [processAttribute.Categories_of_Data]: true,
        [processAttribute.Data_Elements]: true,
        [processAttribute.Source_Asset]: true,
        [processAttribute.Primary_Storage_Asset]: true,
        [processAttribute.Destination_Asset]: true,

        [assetAttribute.Processing_Activities]: true,
    },
    [InventoryContexts.EditSidebar]: {
        [processAttribute.Categories_of_Data_Subjects]: true,
        [processAttribute.Categories_of_Data]: true,
        [processAttribute.Data_Elements]: true,
        [processAttribute.Source_Asset]: true,
        [processAttribute.Primary_Storage_Asset]: true,
        [processAttribute.Destination_Asset]: true,

        [assetAttribute.Processing_Activities]: true,
    },
};

// TODO move to inventory-config file
export const AttributeV2AllowedContexts = {
    [InventoryContexts.Add]: {
        [InventorySchemaIds.Assets]: {
            name: true,
            organization: true,
            location: true,
            type: true,
            technicalOwner: true,
        },
        [InventorySchemaIds.Processes]: {
            name: true,
            organization: true,
            businessOwner: true,
        },
        [InventorySchemaIds.Vendors]: {
            name: true,
            organization: true,
            type: true,
        },
        [InventorySchemaIds.Entities]: {
            name: true,
            organization: true,
            type: true,
            primaryOperatingLocation: true,
        },
    },
};

export const DetailsLinkAttributes = {
    [InventoryTableIds.Assets]: assetAttribute.Asset,
    [InventoryTableIds.Processes]: processAttribute.Processing_Activity,
    [InventoryTableIds.Vendors]: vendorAttribute.Name,
};

export const StatusLinkAttributes = {
    [InventoryTableIds.Assets]: assetAttribute.Status,
    [InventoryTableIds.Processes]: processAttribute.Status,
    [InventoryTableIds.Elements]: elementAttribute.Status,
};

export const ManagingOrgAttributes = {
    [InventoryTableIds.Assets]: assetAttribute.Managing_Organization,
    [InventoryTableIds.Processes]: processAttribute.Managing_Organization,
    [InventoryTableIds.Elements]: elementAttribute.Organization_Group,
    [InventoryTableIds.Vendors]: vendorAttribute.Organization,
};

export const TypesAttributes = {
    [InventoryTableIds.Assets]: assetAttribute.Type,
    [InventoryTableIds.Processes]: processAttribute.Type,
    [InventoryTableIds.Elements]: elementAttribute.Data_Categories,
    [InventoryTableIds.Vendors]: vendorAttribute.VendorType,
};

export const LocationAttributes = {
    [InventoryTableIds.Assets]: assetAttribute.Hosting_Location,
    [InventoryTableIds.Processes]: processAttribute.Locations_of_Parties_Access_Use,
    [InventoryTableIds.Elements]: elementAttribute.Data_Categories,
    [InventoryTableIds.Vendors]: vendorAttribute.Location,
};

export const NamesAttributes = {
    [InventoryTableIds.Assets]: assetAttribute.Asset,
    [InventoryTableIds.Processes]: processAttribute.Processing_Activity,
    [InventoryTableIds.Elements]: elementAttribute.Data_Elements,
    [InventoryTableIds.Vendors]: vendorAttribute.Name,
};

export const CopyAttributes: any = {
    [InventoryTableIds.Assets]: {
        Name: NamesAttributes[InventoryTableIds.Assets],
        Org: InventoryAttributeCodes[InventoryTableIds.Assets].Managing_Organization,
        Location: InventoryAttributeCodes[InventoryTableIds.Assets].Hosting_Location,
    },
    [InventoryTableIds.Processes]: {
        Name: NamesAttributes[InventoryTableIds.Processes],
        Org: InventoryAttributeCodes[InventoryTableIds.Processes].Managing_Organization,
    },
    [InventoryTableIds.Vendors]: {
        Name: NamesAttributes[InventoryTableIds.Vendors],
        Org: InventoryAttributeCodes[InventoryTableIds.Vendors].Organization,
        Type: InventoryAttributeCodes[InventoryTableIds.Vendors].VendorType,
    },
};

export const LocationFieldNames = {
    [InventoryTableIds.Entities]: "primaryOperatingLocation",
    [InventorySchemaIds.Entities]: "primaryOperatingLocation",
};

// TODO move to inventory-config file
export const RequiredFieldNames = {
    [InventoryTableIds.Assets]: {
        Name: AttributeFieldNames.Name,
        Org: AttributeFieldNames.Org,
        Location: AttributeFieldNames.Location,
    },
    [InventoryTableIds.Processes]: {
        Name: AttributeFieldNames.Name,
        Org: AttributeFieldNames.Org,
    },
    [InventoryTableIds.Vendors]: {
        Name: AttributeFieldNames.Name,
        Org: AttributeFieldNames.Org,
        Type: AttributeFieldNames.Type,
    },
    [InventoryTableIds.Entities]: {
        Name: AttributeFieldNames.Name,
        Org: AttributeFieldNames.Org,
        Location: LocationFieldNames[InventoryTableIds.Entities],
    },
};

// TODO move to inventory-config file
export const InventoryMandatoryColumnNames = {
    [InventorySchemaIds.Assets]: [
        RequiredFieldNames[InventoryTableIds.Assets].Name,
        RequiredFieldNames[InventoryTableIds.Assets].Org,
        RequiredFieldNames[InventoryTableIds.Assets].Location,
    ],
    [InventorySchemaIds.Processes]: [
        RequiredFieldNames[InventoryTableIds.Processes].Name,
        RequiredFieldNames[InventoryTableIds.Processes].Org,
    ],
    [InventorySchemaIds.Vendors]: [
        RequiredFieldNames[InventoryTableIds.Vendors].Name,
        RequiredFieldNames[InventoryTableIds.Vendors].Org,
        RequiredFieldNames[InventoryTableIds.Vendors].Type,
    ],
    [InventorySchemaIds.Entities]: [
        RequiredFieldNames[InventoryTableIds.Entities].Name,
        RequiredFieldNames[InventoryTableIds.Entities].Org,
        RequiredFieldNames[InventoryTableIds.Entities].Location,
    ],
};

export const OwnerAttributes = {
    [InventoryTableIds.Assets]: assetAttribute.IT_Owner,
    [InventoryTableIds.Processes]: processAttribute.Business_Process_Owner,
    [InventoryTableIds.Vendors]: vendorAttribute.PrimaryContact,
};

export const OwnerFieldNames = {
    [InventoryTableIds.Assets]: "technicalOwner",
    [InventoryTableIds.Processes]: "businessOwner",
    [InventoryTableIds.Vendors]: "primaryContact",
};

export const DefaultRespondentFieldNames = {
    [InventoryTableIds.Assets]: "technicalOwner",
    [InventoryTableIds.Processes]: "businessOwner",
    [InventoryTableIds.Vendors]: "primaryContact",
};

export const DisabledStatuses = {
    [STATUS.InDiscovery]: true,
};

export const RiskAttributes = {
    [InventoryTableIds.Assets]: assetAttribute.Risk,
    [InventoryTableIds.Processes]: processAttribute.Risk,
    [InventoryTableIds.Vendors]: vendorAttribute.Risk,
};

export const InventoryDetailsTabs = {
    Details: "details",
    Activity: "activity",
    Assessments: "assessments",
    Risks: "risks",
    Attachments: "attachments",
    Documents: "documents",
    Related: "related",
    Lineage: "lineage",
    Controls: "controls",
    Certificates: "certificates",
};

export const InventoryListDetailsTabs = {
    Details: "details",
    List: "list",
};

export const AttributeManagerTabs = {
    Attributes: "attributes",
    Groups: "groups",
};

export const InventoryTableNames = {
    Default: "Inventory",
    Singular: {
        [InventoryTableIds.Elements]: "Element",
        [InventoryTableIds.Assets]: "Asset",
        [InventoryTableIds.Processes]: "ProcessingActivity",
        [InventoryTableIds.Vendors]: "Vendor",
        [InventorySchemaIds.Assets]: "Asset",
        [InventorySchemaIds.Processes]: "ProcessingActivity",
        [InventorySchemaIds.Vendors]: "Vendor",
    },
    Plural: {
        [InventoryTableIds.Elements]: "Elements",
        [InventoryTableIds.Assets]: "Assets",
        [InventoryTableIds.Processes]: "ProcessingActivities",
        [InventoryTableIds.Vendors]: "Vendors",
        [InventorySchemaIds.Assets]: "Assets",
        [InventorySchemaIds.Processes]: "ProcessingActivities",
        [InventorySchemaIds.Vendors]: "Vendors",
    },
};

export const DMAssessmentTemplateTypeNames = {
    [InventoryTableIds.Assets]: "Asset",
    [InventoryTableIds.Processes]: "Processing Activity",
};

// TODO move to inventory-config
export const InventoryDetailRoutes = {
    [InventoryTableIds.Elements]: "zen.app.pia.module.datamap.views.inventory.details",
    [InventoryTableIds.Assets]: "zen.app.pia.module.datamap.views.inventory.details",
    [InventoryTableIds.Processes]: "zen.app.pia.module.datamap.views.inventory.details",
    [InventoryTableIds.Entities]: "zen.app.pia.module.datamap.views.inventory.details",
    [InventoryTableIds.Vendors]: "zen.app.pia.module.vendor.inventory.details",
};

// TODO move to inventory-config
export const InventoryListRoutes = {
    [InventoryTableIds.Elements]: "zen.app.pia.module.datamap.views.inventory.list",
    [InventoryTableIds.Assets]: "zen.app.pia.module.datamap.views.inventory.list",
    [InventoryTableIds.Processes]: "zen.app.pia.module.datamap.views.inventory.list",
    [InventoryTableIds.Vendors]: "zen.app.pia.module.vendor.inventory.list",
    [InventoryTableIds.Entities]: "zen.app.pia.module.datamap.views.inventory.list",
};

export const InventoryRiskDetailsRoutes = {
    [InventoryTableIds.Elements]: "zen.app.pia.module.datamap.views.inventory.risk-details",
    [InventoryTableIds.Assets]: "zen.app.pia.module.datamap.views.inventory.risk-details",
    [InventoryTableIds.Processes]: "zen.app.pia.module.datamap.views.inventory.risk-details",
    [InventoryTableIds.Vendors]: "zen.app.pia.module.vendor.inventory.risk-details",
    [InventoryTableIds.Entities]: "zen.app.pia.module.datamap.views.inventory.risk-details",
};

export const InventoryBulkEditRoutes = {
    [InventoryTableIds.Assets]: "zen.app.pia.module.datamap.views.bulk-edit",
    [InventoryTableIds.Processes]: "zen.app.pia.module.datamap.views.bulk-edit",
    [InventoryTableIds.Entities]: "zen.app.pia.module.datamap.views.bulk-edit",
    [InventoryTableIds.Vendors]: "zen.app.pia.module.vendor.bulk-edit",
};

export const InventoryListManagerRoutes = {
    Entry: "zen.app.pia.module.datamap.views.inventory-manager.home",
    List: "zen.app.pia.module.datamap.views.inventory-manager.list",
    Details: "zen.app.pia.module.datamap.views.inventory-manager.details",
    ManageVisibility: "zen.app.pia.module.settings.manage-visibility",
};

export const RelatedInventories = {
    [InventoryTableIds.Assets]: [InventoryTableIds.Processes],
    [InventoryTableIds.Processes]: [InventoryTableIds.Assets, InventoryTableIds.Elements],
};

export const LinkedInventories = {
    [InventoryTableIds.Assets]: [
        InventoryTableIds.Assets,
        InventoryTableIds.Processes,
        InventoryTableIds.Elements,
        InventoryTableIds.Vendors,
        InventoryTableIds.Entities,
    ],
    [InventoryTableIds.Processes]: [
        InventoryTableIds.Assets,
        InventoryTableIds.Processes,
        InventoryTableIds.Elements,
        InventoryTableIds.Vendors,
        InventoryTableIds.Entities,
    ],
    [InventoryTableIds.Vendors]: [
        InventoryTableIds.Assets,
        InventoryTableIds.Processes,
        InventoryTableIds.Elements,
        InventoryTableIds.Vendors,
    ],
    [InventoryTableIds.Entities]: [
        InventoryTableIds.Assets,
        InventoryTableIds.Processes,
        InventoryTableIds.Entities,
    ],
};

export const RelatedInventoryAttributes = {
    [InventoryTableIds.Assets]: [
        InventoryAttributeCodes[InventoryTableIds.Processes].Source_Asset,
        InventoryAttributeCodes[InventoryTableIds.Processes].Primary_Storage_Asset,
        InventoryAttributeCodes[InventoryTableIds.Processes].Destination_Asset,
    ],
    [InventoryTableIds.Processes]: InventoryAttributeCodes[InventoryTableIds.Assets].Processing_Activities,
};

export const RelatedRecordAttributes = {
    [InventoryTableIds.Assets]: [
        InventoryAttributeCodes[InventoryTableIds.Assets].Processing_Activities,
    ],
    [InventoryTableIds.Processes]: [
        InventoryAttributeCodes[InventoryTableIds.Processes].Source_Asset,
        InventoryAttributeCodes[InventoryTableIds.Processes].Primary_Storage_Asset,
        InventoryAttributeCodes[InventoryTableIds.Processes].Destination_Asset,
        InventoryAttributeCodes[InventoryTableIds.Processes].Categories_of_Data_Subjects,
        InventoryAttributeCodes[InventoryTableIds.Processes].Data_Elements,
        InventoryAttributeCodes[InventoryTableIds.Processes].Categories_of_Data,
    ],
};

export const PrimaryKeyAttributes = {
    [InventoryTableIds.Assets]: [
        InventoryAttributeCodes[InventoryTableIds.Assets].Asset,
        InventoryAttributeCodes[InventoryTableIds.Assets].Managing_Organization,
        InventoryAttributeCodes[InventoryTableIds.Assets].Hosting_Location,
    ],
    [InventoryTableIds.Processes]: [
        InventoryAttributeCodes[InventoryTableIds.Processes].Processing_Activity,
        InventoryAttributeCodes[InventoryTableIds.Processes].Managing_Organization,
    ],
    [InventoryTableIds.Vendors]: [
        InventoryAttributeCodes[InventoryTableIds.Vendors].Name,
        InventoryAttributeCodes[InventoryTableIds.Vendors].Organization,
        InventoryAttributeCodes[InventoryTableIds.Vendors].VendorType,
    ],
};

export const UniqueAttributes = {
    [InventoryTableIds.Assets]: {
        SameOrg: [
            InventoryAttributeCodes[InventoryTableIds.Assets].Risk,
            InventoryAttributeCodes[InventoryTableIds.Assets].Status,
        ],
        DifferentOrg: [
            InventoryAttributeCodes[InventoryTableIds.Assets].Risk,
            InventoryAttributeCodes[InventoryTableIds.Assets].Status,
            InventoryAttributeCodes[InventoryTableIds.Assets].Processing_Activities,
            InventoryAttributeCodes[InventoryTableIds.Assets].IT_Owner,
        ],
    },
    [InventoryTableIds.Processes]: {
        SameOrg: [
            InventoryAttributeCodes[InventoryTableIds.Processes].Risk,
            InventoryAttributeCodes[InventoryTableIds.Processes].Status,
        ],
        DifferentOrg: [
            InventoryAttributeCodes[InventoryTableIds.Processes].Risk,
            InventoryAttributeCodes[InventoryTableIds.Processes].Status,
            InventoryAttributeCodes[InventoryTableIds.Processes].Business_Process_Owner,
            InventoryAttributeCodes[InventoryTableIds.Processes].Source_Asset,
            InventoryAttributeCodes[InventoryTableIds.Processes].Primary_Storage_Asset,
            InventoryAttributeCodes[InventoryTableIds.Processes].Destination_Asset,
            InventoryAttributeCodes[InventoryTableIds.Processes].Data_Elements,
            InventoryAttributeCodes[InventoryTableIds.Processes].Categories_of_Data_Subjects,
            InventoryAttributeCodes[InventoryTableIds.Processes].Categories_of_Data,
        ],
    },
    [InventoryTableIds.Vendors]: {
        SameOrg: [
            InventoryAttributeCodes[InventoryTableIds.Vendors].Risk,
        ],
        DifferentOrg: [
            InventoryAttributeCodes[InventoryTableIds.Vendors].Risk,
            InventoryAttributeCodes[InventoryTableIds.Vendors].Owner,
            InventoryAttributeCodes[InventoryTableIds.Vendors].PrimaryContact,
        ],
    },
};

// TODO move to inventory-config.constant
export const InventoryListV2Columns = {
    [InventoryTableIds.Assets]: [
        AttributeFieldNames.Number,
        AttributeFieldNames.Name,
        AttributeFieldNames.Org,
        AttributeFieldNames.Location,
        AttributeFieldNames.Type,
        OwnerFieldNames[InventoryTableIds.Assets],
        AttributeFieldNames.Risk,
        AttributeFieldNames.Status,
    ],
    [InventoryTableIds.Processes]: [
        AttributeFieldNames.Number,
        AttributeFieldNames.Name,
        AttributeFieldNames.Org,
        OwnerFieldNames[InventoryTableIds.Processes],
        AttributeFieldNames.Risk,
        AttributeFieldNames.Status,
    ],
    [InventoryTableIds.Vendors]: [
        AttributeFieldNames.Number,
        AttributeFieldNames.Name,
        AttributeFieldNames.Org,
        AttributeFieldNames.Type,
        InventoryAttributeCodes[InventoryTableIds.Vendors].VendorId,
        InventoryAttributeCodes[InventoryTableIds.Vendors].Classification,
        InventoryAttributeCodes[InventoryTableIds.Vendors].Criticality,
        InventoryAttributeCodes[InventoryTableIds.Vendors].Rating,
        InventoryAttributeCodes[InventoryTableIds.Vendors].PrimaryContact,
        InventoryAttributeCodes[InventoryTableIds.Vendors].Owner,
        AttributeFieldNames.Risk,
        AttributeFieldNames.Status,
    ],
    [InventoryTableIds.Entities]: [
        AttributeFieldNames.Number,
        AttributeFieldNames.Name,
        AttributeFieldNames.Org,
        LocationFieldNames[InventoryTableIds.Entities],
        AttributeFieldNames.Type,
    ],
};

// TODO move to inventory-config.constant
export const InventoryBrowserStorageNames: any = {
    [InventorySchemaIds.Assets]: GridViewStorageNames.ASSET_INVENTORY_LIST,
    [InventorySchemaIds.Processes]: GridViewStorageNames.PA_INVENTORY_LIST,
    [InventorySchemaIds.Vendors]: GridViewStorageNames.VENDOR_INVENTORY_LIST,
    [InventorySchemaIds.Entities]: GridViewStorageNames.ENTITY_INVENTORY_LIST,
};

export const InventoryBulkImportActiveCards: { [key: string]: { [key: string]: BulkImportCards } } = {
    [InventorySchemaIds.Assets]: {
        Create: BulkImportCards.CREATE_ASSETS,
        Update: BulkImportCards.UPDATE_ASSETS,
    },
    [InventorySchemaIds.Processes]: {
        Create: BulkImportCards.CREATE_PROCESSING_ACTIVITIES,
        Update: BulkImportCards.UPDATE_PROCESSING_ACTIVITIES,
    },
    [InventorySchemaIds.Entities]: {
        Create: BulkImportCards.CREATE_ENTITIES,
        Update: BulkImportCards.UPDATE_ENTITIES,
    },
    [InventorySchemaIds.Vendors]: {
        Create: BulkImportCards.CREATE_VENDORS,
        Update: BulkImportCards.UPDATE_VENDORS,
        AddControls: BulkImportCards.ADD_CONTROLS_TO_VENDOR,
    },
};

export const AttributeResponseTypes = {
    MultiSelect: "multi-select",
    SingleSelect: "single-select",
    Text: "text",
    Date: "date",
};

export const ResponseTypeTranslationKeys = {
    "multi-select": "MultiSpaceSelect",
    "single-select": "SingleSelect",
    "text": "Text",
    "date": "Date",
};

export const InventoryOptionStatusKeys = {
    enabled: "Active",
    disabled: "InActive",
};

export const InventoryOptionStatuses = {
    Enabled: "enabled",
    Disabled: "disabled",
};

export const AttributeModalTypes = {
    Add: "add",
    Edit: "edit",
    Options: "options",
};

export const AttributeTypes = {
    Locations: "locations",
    Orgs: "organizations",
    Users: "users",
    None: "none",
};

export const AttributeStatuses = {
    Enabled: "enabled",
    Disabled: "disabled",
};

export const LineageElementTypes = {
    RelatedAsset: "basic.RelatedAsset",
    RelatedParty: "basic.RelatedParty",
    RelatedVendor: "basic.Vendors",
    Grid: "basic.Grid",
    GridLabel: "basic.GridLabel",
    DataDisposal: "basic.DataDisposal",
    DataSubjects: {
        Custom: "basic.DataSubjects",
        Employee: "basic.DataSubjectEmployee",
        Contractor: "basic.DataSubjectContractor",
        Customer: "basic.DataSubjectCustomer",
        ProspectiveEmployee: "basic.DataSubjectProspectiveEmployee",
    },
};

export const InventoryLineageAssociationTypeKeys = {
    SourceCollection: "SourceCollectionLinkType",
    StorageProcessing: "StorageProcessingLinkType",
    DestinationAccess: "DestinationAccessLinkType",
    Related: "RelatedLinkType",
    RelatedParty: "RelatedPartyLinkType",
};

// These are based on translation keys and are subject to change. DO NOT USE!
export const PartiesWithAccessValueKeys = {
    CustomerServiceReps: "Customer Service Reps",
    ExecutiveBoardOfDirectors: "Executive/Board of Directors",
    FinanceAccountingTeams: "Finance/Accounting Teams",
    InternalAuditors: "Internal Auditors",
    InternalEmployeesOnNeedToKnowBasis: "Internal Employees on need to know basis",
    LegalTeams: "Legal Teams",
    OperationsMaintenanceTeams: "Operations/Maintenance teams",
    SalesMarketingTeams: "Sales/Marketing Teams",
    ExternalAgencies: "External Agencies",
    ExternalAuditors: "External Auditors",
    PublicAuthoritiesGovernmentBodies: "Public Authorities/Government Bodies",
    TradeUnionsWorkCouncils: "Trade Unions/Work Councils",
};

export const PartiesWithAccessAssociationTypeKeys = {
    Internal: "Internal",
    External: "External",
    Regulatory: "Regulatory",
};

export const InventoryPartiesWithAccessTypes = {
    [PartiesWithAccessValueKeys.CustomerServiceReps]: PartiesWithAccessAssociationTypeKeys.Internal,
    [PartiesWithAccessValueKeys.ExecutiveBoardOfDirectors]: PartiesWithAccessAssociationTypeKeys.Internal,
    [PartiesWithAccessValueKeys.FinanceAccountingTeams]: PartiesWithAccessAssociationTypeKeys.Internal,
    [PartiesWithAccessValueKeys.InternalAuditors]: PartiesWithAccessAssociationTypeKeys.Internal,
    [PartiesWithAccessValueKeys.InternalEmployeesOnNeedToKnowBasis]: PartiesWithAccessAssociationTypeKeys.Internal,
    [PartiesWithAccessValueKeys.LegalTeams]: PartiesWithAccessAssociationTypeKeys.Internal,
    [PartiesWithAccessValueKeys.OperationsMaintenanceTeams]: PartiesWithAccessAssociationTypeKeys.Internal,
    [PartiesWithAccessValueKeys.SalesMarketingTeams]: PartiesWithAccessAssociationTypeKeys.Internal,
    [PartiesWithAccessValueKeys.ExternalAgencies]: PartiesWithAccessAssociationTypeKeys.External,
    [PartiesWithAccessValueKeys.ExternalAuditors]: PartiesWithAccessAssociationTypeKeys.External,
    [PartiesWithAccessValueKeys.PublicAuthoritiesGovernmentBodies]: PartiesWithAccessAssociationTypeKeys.Regulatory,
    [PartiesWithAccessValueKeys.TradeUnionsWorkCouncils]: PartiesWithAccessAssociationTypeKeys.Regulatory,
};

export const LineageImageTypes = {
    [InventoryLineageAssociationTypeKeys.Related]: {
        Employees: "images/LineageIcons/ds-employee.svg",
        Contractors: "images/LineageIcons/ds-contractor.svg",
        Customers: "images/LineageIcons/ds-customer.svg",
        ProspectiveEmployees: "images/LineageIcons/ds-potential-employee.svg",
        Generic: "images/LineageIcons/data-subjects.svg",
    },
    // TODO: Update keys to something other than english string values
    [InventorySchemaIds.Assets]: {
        "Website": "images/LineageIcons/asset-website.svg",
        "Database": "images/LineageIcons/asset-database.svg",
        "Physical Storage": "images/LineageIcons/asset-physical.svg",
        "Application": "images/LineageIcons/asset-application.svg",
    },
    // TODO: Update keys to something other than english string values
    [InventorySchemaIds.Vendors]: {
        Consulting: "images/LineageIcons/vendor-contractor.svg",
        Operations: "images/LineageIcons/vendor-operations.svg",
        Regulatory: "images/LineageIcons/vendor-regulatory.svg",
        Strategic: "images/LineageIcons/vendor-strategy.svg",
        Technology: "images/LineageIcons/vendor-technology.svg",
        Generic: "images/LineageIcons/vendor-custom.svg",
    },
    PartiesWithAccess: {
        [PartiesWithAccessAssociationTypeKeys.Internal]: "images/LineageIcons/party-internal.svg",
        [PartiesWithAccessAssociationTypeKeys.External]: "images/LineageIcons/party-external.svg",
        [PartiesWithAccessAssociationTypeKeys.Regulatory]: "images/LineageIcons/party-regulatory.svg",
        Generic: "images/LineageIcons/parties.svg",
    },
    // TODO: Update keys to something other than english string values
    Disposal: {
        "Archive Server": "images/LineageIcons/archive-server.svg",
        "Data Deletion": "images/LineageIcons/data-delete.svg",
        "Physical Archive": "images/LineageIcons/physical-archive.svg",
        "Physically Destroyed": "images/LineageIcons/physical-destroy.svg",
        "Generic": "images/LineageIcons/delete.svg",
    },
};

export const AllowV1Details = {
    [InventoryTableIds.Elements]: true,
};

export const InventorySettings = {
    AssessmentAccessControlOverride: "AssessmentAccessControlOverride",
    AssessmentDefaultInventoryStatus: "AssessmentDefaultInventoryStatus",
    InventoryAccessControlOverrideInventorySelection: "InventoryAccessControlOverrideInventorySelection",
};

export const InventorySettingsDetails = {
    AssessmentAccessControlOverride: {
        titleKey: "AccessControlOverrideSettingTitle",
        descriptionKey: "AccessControlOverrideSettingDescription",
        otAutoId: "settingsAccessControlOverrideToggle",
    },
    AssessmentDefaultInventoryStatus: {
        titleKey: "InventorySettingDefaultStatusTitle",
        descriptionKey: "InventorySettingDefaultStatusText",
        otAutoId: "defaultStatusDropdown",
    },
    InventoryAccessControlOverrideInventorySelection: {
        titleKey: "AccessControlOverrideInventorySelectionTitle",
        descriptionKey: "AccessControlOverrideInventorySelectionDescription",
        otAutoId: "accessControlOverrideInventorySelectionToggle",
    },
};

export const InventoryStatusTranslationKeys = {
    Pending: "InventoryPendingStatus",
    pending: "InventoryPendingStatus",
    Archived: "InventoryArchivedStatus",
    archived: "InventoryArchivedStatus",
    Active: "InventoryActiveStatus",
    active: "InventoryActiveStatus",
};

export const InventoryStatusDescTranslationKeys = {
    pending: "InventoryPendingStatusDescription",
    archived: "InventoryArchivedStatusDescription",
    active: "InventoryActiveStatusDescription",
};

export const ControlLibraryDetailsTabs = {
    Details: "details",
    RelatedRisk: "related-risk",
};

export const ButtonTypes = {
    Brand: "brand",
    Neutral: "neutral",
    Context: "context",
};

export const RiskControlsStatusColorMap = {
    Implemented: "success",
    Pending: "primary",
    NotDoing: "disabled",
};
