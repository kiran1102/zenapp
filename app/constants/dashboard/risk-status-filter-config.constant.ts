import { RiskV2States } from "enums/riskV2.enum";

export const RiskV2StatusFilterConfig = {
    IDENTIFIED: { numericValue: RiskV2States.IDENTIFIED, label : "Identified"},
    RECOMMENDATION_ADDED: { numericValue: RiskV2States.RECOMMENDATION_ADDED, label : "RecommendationAdded"},
    RECOMMENDATION_SENT: { numericValue: RiskV2States.RECOMMENDATION_SENT, label : "RecommendationSent"},
    REMEDIATION_PROPOSED: { numericValue: RiskV2States.REMEDIATION_PROPOSED, label : "RemediationProposed"},
    EXCEPTION_REQUESTED: { numericValue: RiskV2States.EXCEPTION_REQUESTED, label :  "ExceptionRequested"},
    REDUCED: { numericValue: RiskV2States.REDUCED, label : "Reduced"},
    RETAINED: { numericValue: RiskV2States.RETAINED, label : "Retained"},
    ARCHIVED_IN_VERSION: { numericValue: RiskV2States.RETAINED, label: "ArchivedInVersion"},
};
