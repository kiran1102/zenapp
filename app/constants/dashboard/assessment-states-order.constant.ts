export const AssessmentStatesOrder = {
    NotStarted: 1,
    InProgress: 2,
    UnderReview: 3,
    Completed: 4,
};

export const AssessmentStatesCode = {
    All: 0,
    NeedsAttention: 1,
    NotStarted: 10,
    InProgress: 20,
    Completed: 30,
    Archived: 40,
    UnderReview: 50,
};

export const AssessmentRiskCountStatus = {
    Open: "OPEN",
    Closed: "CLOSED",
};
