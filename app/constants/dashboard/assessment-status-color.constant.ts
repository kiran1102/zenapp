export const AssessmentStatusColors = {
    NotStarted: "#696969",  // $slate
    InProgress: "#008ACC",  // $blue
    UnderReview: "#FDA028", // $orange
    Completed: "#6CC04A",   // $green
};

export const AssessmentStateClasses = {
    NotStarted: "not-started",
    InProgress: "in-progress",
    Completed: "completed",
    UnderReview: "under-review",
};
