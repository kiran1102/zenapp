export const ProjectStates = {
    NotStarted: "Not Started",
    InProgress: "In Progress",
    UnderReview: "Under Review",
    NeedInfo: "More Info Needed",
    Completed: "Completed",
    GapAnalysis: "Risk Tracking", // deprecated
    RiskTracking: "Risk Tracking", // deprecated
    InMitigation: "In Mitigation", // deprecated
    RiskAssessment: "Risk Assessment",
    RiskTreatment: "Risk Treatment",
};
