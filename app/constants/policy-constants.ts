export const PolicyConstants = {
    PrivacyNoticeStatus: {
        Draft: "DRAFT",
        Active: "ACTIVE",
        Retired: "RETIRED",
    },
    PrivacyNoticeTabs: {
        Builder: "builder",
        Integrations: "integrations",
        Settings: "settings",
    },
};

export default PolicyConstants;
