export const AppMaintenanceAlertStates = {
    MAINTENANCE: "10",
};

export const MaintenanceModalMoreInfo = {
    MoreInfoLink: "https://support.onetrust.com/hc/en-us/articles/360000677297",
};
