export const ProjectStateTranslationKeys = {
    "Not Started": "NotStarted",
    "In Progress": "InProgress",
    "Under Review": "UnderReview",
    "More Info Needed": "MoreInfoNeeded",
    "Risk Assessment": "RiskAssessment",
    "Risk Treatment": "RiskTreatment",
    "Completed": "Completed",
};
