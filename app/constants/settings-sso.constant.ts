export const SsoSetupTabs = {
    CONFIGURATION: "Configuration",
    ATTRIBUTES: "Attributes",
    ORG_ASGMT: "Org_Asgmt",
};

export const SamlOrgMappingActions = {
    EDIT_ATTRIBUTE: "EDIT_ATTRIBUTE",
    DELETE_ATTRIBUTE: "DELETE_ATTRIBUTE",
};
