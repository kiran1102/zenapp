export const ActivityColor = {
    Created: "#D7D7D7",
    Add: "#2ACCBA",
    Status: "#5BC0DE",
    Edit: "#768CA3",
    Linked: "#2ACCBA",
    Unlinked: "#FD5B65",
    Delete: "#FD5B65",
};

export const ActivityIcon = {
    Created: "ot ot-plus",
    Status: "ot ot-check",
    Edit: "ot ot-edit",
    Linked: "ot ot-list-ul",
    Unlinked: "ot ot-list-ul",
    Delete: "ot ot-trash",
    Failed: "ot ot-close",
    OrderAscend: "ot ot-sort-amount-asc",
    OrderDescend: "ot ot-sort-amount-desc",
};
