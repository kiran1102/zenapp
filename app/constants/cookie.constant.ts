export const CookieScanStatus = {
    Pending: "Pending",
    InProgress: "InProgress",
    Complete: "Complete",
    Cancelled: "Cancelled",
};

export const CookieParts = {
    Dashboard: "0",
    Cookies: "1",
    Tags: "2",
    Forms: "3",
    Pages: "4",
    LocalStorage: "5",
};

export const ScanBehindLoginStatus = {
    NotAvailable: "NOT_AVAILABLE",
    NotVerified: "NOT_VERIFIED",
    Verified: "VERIFIED",
    Failed: "FAILED",
};

export const CookiePurposes = {
    StrictlyNecessary: "Strictly Necessary",
    Performance: "Performance",
    Functionality: "Functionality",
    Targeting: "Targeting/Advertising",
    Unknown: "Unknown",
};

export const ScanBehindLoginError = {
    INVALID_USERNAMEFEILD: "INVALID_USERNAMEFEILD",
    INVALID_PASSWORDFIELD: "INVALID_PASSWORDFIELD",
    INVALID_SUBMITBUTTON: "INVALID_SUBMITBUTTON",
    INVALID_LOGININFO: "INVALID_LOGININFO",
};

export const TemplateTabOptions = {
    Banner: "Banner",
    PreferenceCenter: "PreferenceCenter",
};

export const TemplateStatus = {
    Published_Drafted: "Published_Drafted",
    Published: "Published",
    Drafted: "Drafted",
    Draft: "Draft",
};

export const CookieType = {
    Session: "session",
    Persistent: "Persistent",
};

export const CookiePartyTypes = {
    FirstParty: "FirstParty",
    ThirdParty: "ThirdParty",
};
