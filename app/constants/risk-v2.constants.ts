// Interface
import { InventoryRiskFilterTypes } from "enums/riskV2.enum";
import { FilterValueTypes } from "modules/filter/enums/filter.enum";

export const RiskV2Permissions = {
    RiskCreate: "RiskCreate",
    RiskView: "RiskView",
    RiskEdit: "RiskEdit",
    RiskRecommendation: "RiskRecommendation",
    RiskRemediation: "RiskRemediation",
    RiskException: "RiskException",
    RiskRemediationApprove: "RiskRemediationApprove",
    AssessmentRiskDelete: "AssessmentRiskDelete",
    RiskEditCategory: "RiskEditCategory",
};

export const RiskV2StateTranslationKeys = {
    IDENTIFIED: "Identified",
    RECOMMENDATION_ADDED: "RecommendationAdded",
    RECOMMENDATION_SENT: "RecommendationSent",
    REMEDIATION_PROPOSED: "RemediationProposed",
    EXCEPTION_REQUESTED: "ExceptionRequested",
    REDUCED: "Reduced",
    RETAINED: "Retained",
};

export const InventoryRiskFilterValueTypes = {
    [InventoryRiskFilterTypes.RiskLevel]: FilterValueTypes.SingleSelect,
    [InventoryRiskFilterTypes.RiskState]: FilterValueTypes.SingleSelect,
    [InventoryRiskFilterTypes.Type]: FilterValueTypes.SingleSelect,
    [InventoryRiskFilterTypes.Organization]: FilterValueTypes.Org,
    [InventoryRiskFilterTypes.RiskOwner]: FilterValueTypes.User,
    [InventoryRiskFilterTypes.Category]: FilterValueTypes.MultiSelect,
};
