import { Color } from "./color.constant";
export const ONETRUST_BACKGROUND_COLOR = Color.Green;
export const COOKIEPRO_BACKGROUND_COLOR = "#1B8FEB";
export const DEFAULT_BACKGROUND_COLOR = ONETRUST_BACKGROUND_COLOR;
export const DEFAULT_BRAND_LOGO = "images/logo.png";
export const DEFAULT_HEADER_LOGO = "images/logo_white.svg";
export const DEFAULT_TEXT_COLOR = "#FFFFFF";
