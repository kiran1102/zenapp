export const AssignmentTypes = {
    approver: "approver",
    riskOwner: "risk owner",
    respondent: "respondent",
};
