export const Regex = {
    GUID: /^[0-9A-Fa-f]{8}[-]([0-9A-Fa-f]{4}[-]){3}[0-9A-Fa-f]{12}$/,
    EMAIL: /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
    IP: /^(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)[.]){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/,
    IP_MULTI: /^((?:(?:25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9][0-9]|[0-9]?)[.]){3}(?:25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9][0-9]|[0-9]?)(,(?:(?:25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9][0-9]|[0-9]?)[.]){3}(?:25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9][0-9]|[0-9]?))*)|((?:(?:[0-3][0-7][0-7]|[0-7][0-7]|[0-7]?)[.]){3}(?:[0-3][0-7][0-7]|[0-7][0-7]|[0-7]?)(,(?:(?:[0-3][0-7][0-7]|[0-7][0-7]|[0-7]?)[.]){3}(?:[0-3][0-7][0-7]|[0-7][0-7]|[0-7]?))*)$/,
    CIDR: /^(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)[.]){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)(\/(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?))?$/,
    CIDR_MULTI: /^((?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)[.]){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)[\/](([1][0-9])|([2][0-9])|([3][0-2])|([1-9]))?(,(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)[.]){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)[\/](([1][0-9])|([2][0-9])|([3][0-2])|([1-9])))*)$/,
    REMOVE_SPACE_BETWEEN_TAGS: /(^|>)[ \n\t]+/g,
    LINK: /^(http|https?):\/\/[^ "]+$/,
    HTTPS_LINK: /^https:\/\/[^ "]+$/,
    MULTI_SPACES: /\s{2,}/g,
    ALPHA_NUMERIC: /[^a-zA-Z0-9]/g,
    HEXA_COLOR_CODE: /^#[0-9A-F]{6}$/i,
    NON_ASCII: /[^A-Za-z 0-9 \.,\?""!@#\$%\^&\*\(\)-_=\+;:<>\/\\\|\}\{\[\]`~]*/g,
    REMOVE_HTML_TAGS: /<[^>]*>/g,
    REMOVE_NON_BREAKING_SPACES: /&nbsp;/gi,
    NO_BLANK_SPACE: /(?!^ +$)^.+$/,
    NON_NUMBER: /\D/,
    NEGATIVE_NUMBER: /^-\d+/,
    POSITIVE_NUMBER_WITH_ZERO: /^[0-9]+$/,
    NEGATIVE_NUMBER_PERIOD: /^[^-]\d+\./,
    ISO_OR_UTC_DATE: /(\d\d\sUTC)$|(\dZ)$/,
    FILE_EXTENSION: /(?:\.([^.]+))?$/,
    URL: /https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/,
    API_PREFIX_PATTERN: /\.([0-9a-z]+)(?:[\?#]|$)/i,
    HOST_NAME: /:\/\/(.[^/:]+)/,
    NO_WHITESPACES: /^\S*$/,
    SPECIAL_CHARACTERS: /[!=+@#$%^&*()[,.?\\[:;/~_|\]-]/,
};
export const {
    GUID,
    EMAIL,
    IP,
    IP_MULTI,
    CIDR,
    CIDR_MULTI,
    REMOVE_SPACE_BETWEEN_TAGS,
    LINK,
    MULTI_SPACES,
    ALPHA_NUMERIC,
    HEXA_COLOR_CODE,
    NON_ASCII,
    REMOVE_HTML_TAGS,
    REMOVE_NON_BREAKING_SPACES,
    NO_BLANK_SPACE,
    NON_NUMBER,
    NEGATIVE_NUMBER,
    NEGATIVE_NUMBER_PERIOD,
    POSITIVE_NUMBER_WITH_ZERO,
    ISO_OR_UTC_DATE,
    FILE_EXTENSION,
    API_PREFIX_PATTERN,
    HOST_NAME,
    NO_WHITESPACES,
    SPECIAL_CHARACTERS,
} = Regex;
