export const TemplateHeaderActionButtons = {
    Cancel: "cancel",
    CreateNewVersion: "createNewVersion",
    EditThisVersion: "editThisVersion",
    Publish: "publish",
    SaveUpdates: "saveUpdates",
    DiscardDraft: "discardDraft",
    UnlockTemplate: "unlockTemplate",
};
