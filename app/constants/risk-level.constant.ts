export const RiskLevelTags = {
    NONE: "None",
    LOW: "Low",
    MEDIUM: "Medium",
    HIGH: "High",
    VERY_HIGH: "VeryHigh",
};

export const RiskLevelOrder = {
    None: 0,
    Low: 1,
    Medium: 2,
    High: 3,
    VeryHigh: 4,
};
