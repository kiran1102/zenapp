// Enums
import {
    FilterValueTypes,
    FilterOperators,
    FilterOperatorSymbol,
} from "modules/filter/enums/filter.enum";
import { AAFilterTypes } from "modules/assessment/enums/aa-filter.enum";
import { AssessmentStatus } from "enums/assessment/assessment-status.enum";

// Constants
import { BadgeColorTypes } from "incidentModule/shared/constants/incident-badge-color.constants";

export const AssessmentStates = {
    NotStarted: "Not Started",
    InProgress: "In Progress",
    UnderReview: "Under Review",
    InfoNeeded: "More Info Needed",
    RiskAssessment: "Risk Assessment",
    RiskTreatment: "Risk Treatment",
    Completed: "Completed",
};

export const AssessmentStatusKeys = {
    COMPLETED: "Completed",
    IN_PROGRESS: "InProgress",
    NOT_STARTED: "NotStarted",
    UNDER_REVIEW: "UnderReview",
};

export const AssessmentStateKeys = {
    NotStarted: "NotStarted",
    InProgress: "InProgress",
    UnderReview: "UnderReview",
    InfoNeeded: "MoreInfoNeeded",
    RiskAssessment: "RiskAssessment",
    RiskTreatment: "RiskTreatment",
    Completed: "Completed",
};

export const AssessmentPermissions = {
    AssessmentCreate: "AssessmentCreate",
    AssessmentDelete: "AssessmentDelete",
    AssessmentSubmit: "AssessmentSubmit",
    AssessmentEditDetails: "AssessmentEditDetails",
    AssessmentRequestInfo: "AssessmentRequestInfo",
    AssessmentProvideMoreInfo: "AssessmentProvideMoreInfo",
    AssessmentNextSteps: "AssessmentNextSteps",
    AssessmentRiskManagement: "AssessmentRiskManagement",
    AssessmentApprove: "AssessmentApprove",
    AssessmentViewAddedInfo: "AssessmentViewAddedInfo",
    AssessmentQuestionApprove: "AssessmentQuestionApprove",
    AssessmentQuestionView: "AssessmentQuestionView",
    AssessmentQuestionEdit: "AssessmentQuestionEdit",
    AssesmentRiskCountView: "AssesmentRiskCountView",
    AssessmentDetailsView: "AssessmentDetailsView",
    AssessmentNameEdit: "AssessmentNameEdit",
    AssessmentOrgView: "AssessmentOrgView",
    AssessmentDeadlineEdit: "AssessmentDeadlineEdit",
    AssessmentDescriptionEdit: "AssessmentDescriptionEdit",
    AssessmentChangeAssessmentOrg: "AssessmentChangeAssessmentOrg",
    AssessmentEditResponses: "AssessmentEditResponse",
    AssessmentApproverReassign: "AssessmentApproverReassign",
    AssessmentRespondentReassign: "AssessmentRespondentReassign",
    AssessmentCanBeApprover: "AssessmentCanBeApprover",
    AssessmentCanBeRespondent: "AssessmentCanBeRespondent",
    AssessmentExports: "AssessmentExports",
    AssessmentSavedViews: "AssessmentSavedViews",
    CommentsQuestionRead: "CommentsQuestionRead",
    CommentsQuestionCreate: "CommentsQuestionCreate",
    AttachmentsRead: "AttachmentsRead",
    AttachmentsCreate: "AttachmentsCreate",
    AttachmentsDelete: "AttachmentsDelete",
    QuestionHistory: "QuestionHistory",
    CanBeRiskOwner: "CanBeRiskOwner",
    CreateAndEditNote: "CreateAndEditNote",
    ViewNote: "ViewNote",
    DeleteNote: "DeleteNote",
    AssessmentDeleteInfoRequest: "AssessmentDeleteInfoRequest",
    AssessmentRespondtoInfoRequest: "AssessmentRespondtoInfoRequest",
    AssessmentCloseInfoRequest: "AssessmentCloseInfoRequest",
    CreateQuestionComment: "CreateQuestionComment",
    ViewQuestionComment: "ViewQuestionComment",
    TemplateUpgradeUIv2: "TemplateUpgradeUIv2",
    FileRead: "FileRead",
    ReportsExportNew: "ReportsExportNew",
};

export const AssessmentQuestionButtonKeys = {
    NotSure: "NotSure",
    NotApplicable: "NotApplicable",
    Yes: "Yes",
    No: "No",
};

export const AssessmentWelcomeSectionText = "<p class=\"p1\"><span class=\"s1\"><b>Welcome to our data privacy portal!</b></span></p><p class=\"p1\"><span class=\"s1\">Our goal is to help maximize your project's success while protecting the privacy of the users and customers of your project.<span class=\"Apple-converted-space\"> </span></span></p><p class=\"p1\"><span class=\"s1\">The following wizard will help us collaborate with you and your team to quickly learn about your project and provide relevant, specific advice for your situation.</span></p><p class=\"p1\"><span class=\"s1\">Privacy of both our customers and our employees is critical to our organization in order to protect us from regulatory violations, prevent negative publicity, as well as show respect to our customers and peers.<span class=\"Apple-converted-space\"> </span></span></p><p class=\"p1\"><span class=\"s1\"> </span></p><p class=\"p1\"><span class=\"s1\"><b>Below are some answers to common questions:</b></span></p><p class=\"p2\"><span class=\"s1\"> </span></p><p class=\"p1\"><span class=\"s1\"><b>FAQ</b></span></p><p class=\"p1\"><span class=\"s1\"><i>1.</i><b><i> </i></b><i>How do I get help?</i></span></p><ul><li class=\"li1\"><span class=\"s2\"></span><span class=\"s1\">Commenting capabilities are built into this portal. You can flag certain answers with a &#34;not sure&#34; response, add comments, and our team will provide help directly within this tool.</span></li><li class=\"li1\"><span class=\"s2\"></span><span class=\"s1\">You can re-assign questions to be answered by different members on your team in order to get the most accurate answers possible from the right people.</span></li> <li class=\"li1\"><span class=\"s2\"></span><span class=\"s1\">At any time if you need additional help, please contact us at PRIVACY@OURCOMPANY.COM</span></li> </ul><p class=\"p3\"><span class=\"s1\"> </span></p><p class=\"p1\"><span class=\"s1\">2. <i>What is OneTrust?</i></span></p><p class=\"p1\"><span class=\"s1\">OneTrust is a tool that we use to make it easier and faster for us to provide timely and relevant guidance for your projects. It also makes it easy for you to collaborate with us in the privacy office, as well as with your peers to get to closure on your projects.</span></p>";

export const AssessmentTypes = {
    PIA: {
        Name: "pia",
        TemplateType: 10,
        BaseRoute: "zen.app.pia",
        DefaultRoute: "zen.app.pia.module.projects.list",
    },
    DM: {
        Name: "datamap",
        TemplateType: 40,
        BaseRoute: "zen.app.pia.module.datamap",
        DefaultRoute: "zen.app.pia.module.datamap.views.tables(tableId: 10)",
    },
};

export const AssessmentThankYouStates = {
    Complete: "complete",
    Incomplete: "incomplete",
};

export const DMAssessmentStateLabelKeys = {
    10: "NotStarted",
    20: "InProgress",
    30: "Completed",
    40: "Archived",
    50: "UnderReview",
};

export const PIAStateDescTranslationKeys = {
    "Not Started": "NotStarted",
    "In Progress": "InProgress",
    "Under Review": "UnderReview",
    "More Info Needed": "MoreInfoNeeded",
    "Risk Assessment": "RiskAssessment",
    "Risk Treatment": "RiskTreatment",
    "Completed": "Completed",
};

export const AssessmentActivityKeys = {
    ALL: "All",
    STATE_CHANGED: "StateChanged",
    NAME_CHANGED: "NameChanged",
    DEADLINE_CHANGED: "DeadlineChanged",
    APPROVER_CHANGED: "ApproverChanged",
    RESPONDENT_CHANGED: "RespondentChanged",
    ASSESSMENT_SHARED: "AssessmentShared",
    TEMPLATE_CHANGED: "TemplateChanged",
    STAGE_CHANGED: "StageChanged",
};

export const AssessmentStatusBadgeColorMap = {
    NOT_STARTED: BadgeColorTypes.DEFAULT,
    IN_PROGRESS: BadgeColorTypes.PRIMARY,
    UNDER_REVIEW: BadgeColorTypes.WARNING,
    COMPLETED: BadgeColorTypes.SUCCESS,
};

export const AssessmentReviewUpdatesStatus = {
    NEW_QUESTION_ADDED: {
        text: "NewQuestionAdded",
        status: BadgeColorTypes.ADD,
    },
    NEW_SECTION_ADDED: {
        text: "NewSectionAdded",
        status: BadgeColorTypes.ADD,
    },
    QUESTION_DELETED: {
        text: "QuestionDeleted",
        status: BadgeColorTypes.DESTRUCTIVE,
    },
    SECTION_DELETED: {
        text: "SectionDeleted",
        status: BadgeColorTypes.DESTRUCTIVE,
    },
    ALLOW_NOT_SURE_ENABLED: {
        text: "AllowNotSureEnabled",
        status: BadgeColorTypes.EDIT,
    },
    ALLOW_NOT_SURE_DISABLED: {
        text: "AllowNotSureDisabled",
        status: BadgeColorTypes.EDIT,
    },
    ALLOW_NOT_APPLICABLE_ENABLED: {
        text: "AllowNotApplicableEnabled",
        status: BadgeColorTypes.EDIT,
    },
    ALLOW_NOT_APPLICABLE_DISABLED: {
        text: "AllowNotApplicableDisabled",
        status: BadgeColorTypes.EDIT,
    },
    ALLOW_OTHER_ENABLED: {
        text: "AllowOtherEnabled",
        status: BadgeColorTypes.EDIT,
    },
    ALLOW_OTHER_DISABLED: {
        text: "AllowOtherDisabled",
        status: BadgeColorTypes.EDIT,
    },
    ALLOW_MULTIPLE_SELECTIONS_ENABLED: {
        text: "AllowMultipleSelectionsEnabled",
        status: BadgeColorTypes.EDIT,
    },
    ALLOW_MULTIPLE_SELECTIONS_DISABLED: {
        text: "AllowMultipleSelectionsDisabled",
        status: BadgeColorTypes.EDIT,
    },
    ALLOW_JUSTIFICATION_ENABLED: {
        text: "AllowJustificationEnabled",
        status: BadgeColorTypes.EDIT,
    },
    ALLOW_JUSTIFICATION_DISABLED: {
        text: "AllowJustificationDisabled",
        status: BadgeColorTypes.EDIT,
    },
    NEW_OPTION_ADDED: {
        text: "NewOptionAdded",
        status: BadgeColorTypes.EDIT,
    },
    OPTION_DELETED: {
        text: "OptionDeleted",
        status: BadgeColorTypes.EDIT,
    },
};

export const AAColumnFilterValueTypes = {
    [AAFilterTypes.State]: FilterValueTypes.MultiSelect,
    [AAFilterTypes.RiskLevel]: FilterValueTypes.MultiSelect,
    [AAFilterTypes.Approver]: FilterValueTypes.MultiSelect,
    [AAFilterTypes.Respondent]: FilterValueTypes.MultiSelect,
    [AAFilterTypes.Creator]: FilterValueTypes.MultiSelect,
    [AAFilterTypes.Template]: FilterValueTypes.MultiSelect,
    [AAFilterTypes.Tags]: FilterValueTypes.MultiSelect,
    [AAFilterTypes.Deadline]: FilterValueTypes.DateRange,
    [AAFilterTypes.DateCreated]: FilterValueTypes.DateRange,
    [AAFilterTypes.Organization]: FilterValueTypes.MultiSelect,
    [AAFilterTypes.OpenInfoRequest]: FilterValueTypes.Number,
    [AAFilterTypes.OpenRiskCount]: FilterValueTypes.Number,
    [AAFilterTypes.Result]: FilterValueTypes.MultiSelect,
    [AAFilterTypes.Tags]: FilterValueTypes.MultiSelect,
};

export const AAColumnFilterOperators = {
    [FilterOperatorSymbol.EqualTo] : {
        key: FilterOperators.Equals,
        value: FilterOperatorSymbol.EqualTo,
        translationKey: "EqualTo",
    },
    [FilterOperatorSymbol.GreaterThan] : {
        key: FilterOperators.GreaterThan,
        value: FilterOperatorSymbol.GreaterThan,
        translationKey: "GreaterThan",
        },
    [FilterOperatorSymbol.LessThan] : {
        key: FilterOperators.LessThan,
        value: FilterOperatorSymbol.LessThan,
        translationKey: "LessThan",
    },
};

export const AssessmentFinishReviewActions = {
    APPROVED: "Approved",
    REJECTED: "Rejected",
};

export const AssessmentResultKeys = {
    Approved: "Approved",
    Rejected: "Rejected",
    AutoClosed: "AutoClosed",
};

export const AssessmentColumnData = {
    Approver: "approver",
    Creator: "creator",
    DateCreated: "createDT",
    Deadline: "deadline",
    Id: "number",
    Name: "name",
    OpenInfoRequest: "openInfoRequestCount",
    OpenRiskCount: "openRiskCount",
    Organization: "orgGroupName",
    Respondent: "respondent",
    Result: "result",
    RiskLevel: "assessmentRiskLevelId",
    RiskScore: "assessmentRiskScore",
    State: "status",
    Template: "templateName",
    Tags: "tags",
};

export const AssessmentExportColumnMetaData = {
    [AssessmentColumnData.Approver]: "approvers",
    [AssessmentColumnData.Creator]: "creator",
    [AssessmentColumnData.DateCreated]: "createDT",
    [AssessmentColumnData.Deadline]: "deadline",
    [AssessmentColumnData.Id]: "number",
    [AssessmentColumnData.Name]: "name",
    [AssessmentColumnData.OpenInfoRequest]: "openInfoRequestCount",
    [AssessmentColumnData.OpenRiskCount]: "openRiskCount",
    [AssessmentColumnData.Organization]: "orgGroup",
    [AssessmentColumnData.Respondent]: "respondents",
    [AssessmentColumnData.Result]: "result",
    [AssessmentColumnData.RiskLevel]: "riskSummary",
    [AssessmentColumnData.State]: "state",
    [AssessmentColumnData.Template]: "templateName",
    RiskScore: "riskScore",
    Tags: "tags",
};

export const AssessmentStatusKeysToExport = {
    [AssessmentStatus.Not_Started]: "Not Started",
    [AssessmentStatus.In_Progress]: "In Progress",
    [AssessmentStatus.Under_Review]: "Under Review",
    [AssessmentStatus.Completed]: "Completed",
};

export const AssessmentResultKeysToExport = {
    [AssessmentResultKeys.Approved]: "Approved",
    [AssessmentResultKeys.Rejected]: "Rejected",
    [AssessmentResultKeys.AutoClosed]: "Auto-Closed",
};

export const AssessmentSectionNavFilters = {
    AllQuestions: "ALL_QUESTIONS",
    RequiredAndUnansweredQuestions: "REQUIRED_UNANSWERED_QUESTIONS",
    RequiredQuestions: "REQUIRED_QUESTIONS",
    UnansweredQuestions: "UNANSWERED_QUESTIONS",
};
