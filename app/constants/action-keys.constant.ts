export const ActionKeys = {
    Delete: "delete",
    Edit: "edit",
    Conditions: "conditions",
    Enable: "enable",
    Disable: "disable",
};
