export const TemplateTypes = {
    PIA: "PIA",
    VENDOR: "VENDOR",
    RA: "RA",
    GRA: "GRA",
    DINA: "DINA",
};

export type TemplateType = "PIA" | "VENDOR" | "RA" | "GRA" | "DINA";
