export const UserGroupActions = {
    CreateUserGroup: "Create",
    DeleteUserGroup: "Delete",
    EditUserGroup: "Edit",
    ListUserGroups: "List",
    SearchUserGroup: "Search",
};