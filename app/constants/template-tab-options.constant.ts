export const TemplateTabOptions = {
    Details: "Details",
    Builder: "Builder",
    Preview: "Preview",
    Rules: "Rules",
};

export const TemplatesTypeTabOptions = {
    Templates: "templates",
    Archive: "archive",
};
