export const TemplateStates = {
    ARCHIVE: "Archive",
    DRAFT: "Draft",
    LOCKED: "Locked",
    PUBLISHED: "Published",
};

export const TemplateStateProperty = {
    Draft: "draft",
    Published: "published",
    Both: "both",
};

export const TemplateCriteria = {
    Active: "ACTIVE",
    Both: "BOTH",
};
