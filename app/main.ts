// Angular
import { UpgradeModule } from "@angular/upgrade/static";
import { platformBrowserDynamic } from "@angular/platform-browser-dynamic";
import { enableProdMode } from "@angular/core";

// Router
import { Router } from "@angular/router";
import {
    UrlService,
    UIRouter,
    Trace,
} from "@uirouter/core";

// Modules
import { Ng1Module } from "./ng1.module";
import { Ng2Module } from "./ng2.module";
import { downgrade } from "./downgrade";

// Interfaces
import { INodeProcess } from "modules/core/interfaces/process.interface";

declare const process: INodeProcess;

Ng1Module.config(["$urlServiceProvider", ($urlService: UrlService) => $urlService.deferIntercept()]);

if (process.env.NODE_ENV === "production") {
    enableProdMode();
}

if (process.env.NODE_ENV === "development") {
    // Ng1Module.run(["$trace", ($trace: Trace) => { $trace.enable(1); }]);
}

platformBrowserDynamic().bootstrapModule(Ng2Module).then((platformRef) => {

    downgrade();

    const injector = platformRef.injector;
    const upgrade = injector.get(UpgradeModule) as UpgradeModule;
    const router = injector.get(Router);

    upgrade.bootstrap(document.body, [Ng1Module.name], { strictDi: true });

    const url: UrlService = injector.get(UIRouter).urlService;
    url.listen();
    url.sync();
    router.initialNavigation();

}).catch((err) => {
    if (process.env.NODE_ENV === "development") {
        // tslint:disable-next-line:no-console
        console.log(err);
    }
});
