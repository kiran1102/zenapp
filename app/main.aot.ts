import { UpgradeModule } from "@angular/upgrade/static";
import { platformBrowser } from "@angular/platform-browser";
import { Injector, enableProdMode } from "@angular/core";
import { Router } from "@angular/router";
import { UrlService, UIRouter } from "@uirouter/core";
import { Ng1Module } from "./ng1.module";
import { downgrade } from "./downgrade";

import { Ng2ModuleNgFactory } from "./ng2.module.ngfactory";

Ng1Module.config(["$urlServiceProvider", ($urlService: UrlService) => $urlService.deferIntercept()]);

enableProdMode();

platformBrowser().bootstrapModuleFactory(Ng2ModuleNgFactory).then((platformRef) => {

    downgrade();

    const injector: Injector = platformRef.injector;
    const upgrade = injector.get(UpgradeModule) as UpgradeModule;
    const router = injector.get(Router);

    upgrade.bootstrap(document.body, [Ng1Module.name], { strictDi: true });

    const url: UrlService = injector.get(UIRouter).urlService;
    url.listen();
    url.sync();
    router.initialNavigation();

    // tslint:disable-next-line:no-console
}).catch((err) => {
    if (process.env.NODE_ENV === "development") {
        console.log(err);
    }
});
