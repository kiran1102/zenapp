import { UrlHandlingStrategy } from "@angular/router";

const legacyRoutes = [
    "/pia",
    "/login",
    "/forgot-password",
    "/confirm",
    "/createpassword",
    "/changepassword",
    "/invite",
    "/saml",
    "/webform",
    "/consent",
];

export class CustomUrlHandlingStrategy implements UrlHandlingStrategy {
    shouldProcessUrl(url) {
        const urlString: string = url.toString();
        if (!urlString) return true;
        if (urlString.indexOf(".") > -1 || urlString.indexOf("undefined") > -1) return false;
        for (const r of legacyRoutes) {
            if (urlString.startsWith(r)) return false;
        }
        return true;
    }

    extract(url) {
        return url;
    }

    merge(url, whole) {
        return url;
    }
}
