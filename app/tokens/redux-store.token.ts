import { IStore } from "interfaces/redux.interface";
import { InjectionToken } from "@angular/core";
export const StoreToken = new InjectionToken<IStore>("ReduxStore");
