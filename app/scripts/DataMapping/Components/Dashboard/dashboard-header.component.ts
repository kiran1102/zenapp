declare var angular: any;

class DashboardHeaderController {

    private showFilters: boolean;

    constructor() {
        this.showFilters = false;
    }

    private toggleFilters(): void {
        this.showFilters = !this.showFilters;
    }

}

export default {
    controller: DashboardHeaderController,
    template: `
        <one-header class="dashboard-header">
            <header-title class="dashboard-header__title row-flex-start align-center text-nowrap">
                <span>{{::$root.t("Dashboard")}}</span>
            </header-title>
            <div class="dashboard-header__actions row-flex-end align-center">
                <ul class="dashboard-header__actions--list row-horizontal-vertical-center margin-none">
                    <li class="dashboard-header__actions--item">
                        <one-button class="one-header__button" icon="fa fa-filter" button-click="$ctrl.toggleFilters()" text="{{::$root.t('Filters')}}"></one-button>
                    </li>
                </ul>
            </div>
            <header-dropdown
                ng-if="$ctrl.showFilters"
                class="dashboard-header__dropdown full-width row-flex-start align-center padding-all-1"
                ng-transclude="dropdown">
            </header-dropdown>
        </one-header>
    `,
    transclude: {
        dropdown: "?dropdown",
    },
};
