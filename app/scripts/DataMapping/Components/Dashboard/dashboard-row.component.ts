
const dashboardRow = {
    transclude: {
        cards: "cards",
    },
    bindings: {},
    template: (): string => {
        return `
                <div class="dashboard-row" ng-transclude="cards"></div>
                `;
    },
};

export default dashboardRow;
