class DashboardCardController implements ng.IComponentController {

    private cardSizeClass: string;
    private cardSize: string;

    constructor() {
        this.cardSizeClass = `dashboard-card--${this.cardSize}`;
    }
}

export default {
    transclude: {
        content: "cardContent",
    },
    bindings: {
        cardSize: "<",
        cardTitle: "@?",
        loadingText: "@?",
        ready: "<?",
        empty: "<",
        emptyText: "@",
    },
    controller: DashboardCardController,
    template:  `
        <div
            class="dashboard-card position-relative overflow-hidden"
            ng-class="{
                'padding-all-0 margin-all-0 height-100': $ctrl.cardSize === 'full-size',
                '{{$ctrl.cardSizeClass}}': $ctrl.cardSizeClass,
            }"
            >
            <dashboard-card-header ng-if="$ctrl.cardTitle" class="dashboard-card__header" card-title="$ctrl.cardTitle"></dashboard-card-header>
            <loading ng-if="!$ctrl.ready" class="dashboard-card__loading" text="{{$ctrl.loadingText}}"></loading>
            <div
                ng-if="$ctrl.ready && !$ctrl.empty"
                class="dashboard-card__content"
                ng-class="{'padding-all-0': $ctrl.cardSize === 'full-size'}"
                ng-transclude="content"
                >
            </div>
            <div ng-if="$ctrl.ready && $ctrl.empty" class="dashboard-card__empty">
                <h2>{{::$root.t($ctrl.emptyText)}}</h2>
            <div>
        </div>
    `,
};
