
class DashboardCardHeaderController {

    private showingTable: boolean;

    private toggleTable(): void {
        this.showingTable = !this.showingTable;
    }
}

const dashboardCardHeader = {
    bindings: {
        cardTitle: "<",
        hasPopOut: "<",
        hasTable: "<",
    },
    controller: DashboardCardHeaderController,
    template: (): string => {
        return `
                <div class="dashboard-card-header">
                    <h4 class="dashboard-card-header__title">{{$ctrl.cardTitle}}</h4>
                    <div class="dashboard-card-header__actions">
                        <i ng-if="$ctrl.hasTable && !$ctrl.showingTable" class="fa fa-table fa-fw dashboard-card-header__icon" ng-click="$ctrl.toggleTable()"></i>
                        <i ng-if="$ctrl.hasTable && $ctrl.showingTable" class="fa fa-bar-chart fa-fw dashboard-card-header__icon" ng-click="$ctrl.toggleTable()"></i>
                        <i ng-if="$ctrl.hasPopOut" class="fa fa-external-link fa-fw dashboard-card-header__icon"></i>
                    </div>
                </div>
                `;
    },
};

export default dashboardCardHeader;
