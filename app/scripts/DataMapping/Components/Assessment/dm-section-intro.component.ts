
const dmSectionIntro = {
    bindings: {
        question: "<",
    },
    template: (): string => {
        return `<section class="dm-section-intro centered-max-70">
                    <question-content question="$ctrl.question"></question-content>
                </section>`;
    },
};

export default dmSectionIntro;
