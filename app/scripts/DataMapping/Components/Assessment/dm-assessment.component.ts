// 3rd party
import {
    find,
    filter,
    isArray,
    isUndefined,
    forEach,
} from "lodash";

// Services
import TreeMap from "TreeMap";

// Constants
import { AssessmentThankYouStates } from "constants/assessment.constants";

// Redux
import { getCurrentUser } from "oneRedux/reducers/user.reducer";

// Interfaces
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import { IDMAssessmentSection } from "interfaces/section.interface";
import { IDMSendBackModal } from "interfaces/dm-assessment.interface";
import { IStore } from "interfaces/redux.interface";

class DmAssessment {

    static $inject: string[] = [
        "$scope",
        "$rootScope",
        "$q",
        "$filter",
        "$timeout",
        "ENUMS",
        "DMAssessments",
        "DMAssessmentNavigation",
        "DMAnswer",
        "UserStore",
        "OrgGroups",
        "$state",
        "AssessmentModals",
        "ModalService",
        "Permissions",
        "OneAssessment",
        "OrgGroupStoreNew",
        "NotifyService",
        "store",
    ];

    public assessmentId: string;
    public assessment: any;
    public ready = false;
    private translate: any;
    private AssessmentStates: any;
    private ThankYouStates: any;
    private TemplateTypes: any;
    private dmAssessment: any;
    private context: number;
    private treeMap: any;
    private bodyElement: Element;
    private parentTreeFilter: any;
    private RoleNames: any;

    constructor(
        private $scope: any,
        private $rootScope: IExtendedRootScopeService,
        private $q: ng.IQService,
        private $filter: ng.IFilterService,
        private $timeout: ng.ITimeoutService,
        ENUMS: any,
        private DMAssessments: any,
        private DMAssessmentNavigation: any,
        private DMAnswer: any,
        private UserStore: any,
        private OrgGroups: any,
        private $state: ng.ui.IStateService,
        private AssessmentModals: any,
        private ModalService: any,
        private Permissions: any,
        private OneAssessment: any,
        private OrgGroupStoreNew: any,
        private NotifyService: any,
        private store: IStore,
    ) {
        this.context = ENUMS.ContextTypes.DMAssessment;
        this.AssessmentStates = ENUMS.DMAssessmentStates;
        this.TemplateTypes = ENUMS.TemplateTypes;
        this.RoleNames = ENUMS.RoleNames;
        this.translate = $rootScope.t;
        this.dmAssessment = OneAssessment;
        this.assessmentId = this.$state.params.Id;
        this.parentTreeFilter = $filter("IsInParentTree");
    }

    public $onInit(): void {
        this.startEventWatchers();
        this.getAssessment(this.assessmentId).then( (response: any): any => {
            if (response && response.result) {
                this.dmAssessment.init(response.data, this.context);
                this.dmAssessment.setCurrentUser(getCurrentUser(this.store.getState()));
                this.treeMap = new TreeMap("Id", "ParentId", "Children", this.OrgGroupStoreNew.orgTree);
                const team: any[] = filter(this.getUsers(), (user: any): boolean => user.IsActive);
                this.dmAssessment.setTeam(team);
                this.setUserDetails();
                this.dmAssessment.setTeam(this.filterUsersByOrg());
                this.formatAssessment();
                this.ready = true;
            }
        });
    }

    public getAssessment(id: string): ng.IPromise<any> {
        return this.DMAssessments.getAssessment(id);
    }

    public formatAssessment(): void {
        const sections: any = this.dmAssessment.getSections();
        const activeSectionId: string = this.dmAssessment.getActiveSection().id;
        forEach(sections, (section: any): void => {
            section.isActive = (section.id === activeSectionId);
        });
        this.dmAssessment.setSections(sections);
        this.setAssessmentApprovalState();
        this.setReadOnly();
    }

    public goToSection(sectionId: string, sections?: any): ng.IPromise<any> {
        const sectionList: any = sections || this.dmAssessment.getSections();
        return this.DMAssessmentNavigation.getSection(this.dmAssessment.getId(), sectionId).then( (response: any): any => {
            if (!response.result) return { result: false };
            this.dmAssessment.setActiveSection(response.data);
            this.dmAssessment.setActiveQuestions(response.data.nextQuestions);
            const updateSections: any[] = this.DMAssessmentNavigation.updateActiveSection(sectionList, response.data);
            this.dmAssessment.setSections(updateSections);
            this.setSectionPostionToTop();
            this.setAssessmentApprovalState();
            this.$scope.$broadcast("STATUS_UPDATED");
            return { result: true };
        });
    }

    public setSectionPostionToTop(): void {
        this.bodyElement = this.bodyElement || document.getElementsByClassName("dm-assessment__body")[0];
        if (this.bodyElement) {
            this.$timeout((): void => {
                this.bodyElement.scrollTop = 0;
            }, 0, false);
        }
    }

    public completeSection(question: any): ng.IPromise<any> {
        return this.DMAnswer.sendEmptyResponse(question).then( (response: any): any => {
            if (!response.result) return { result: false, data: null };
            const nextSectionId: string = response.data.activeSection.id;
            const status: number = this.dmAssessment.getStatus();
            if (status === this.AssessmentStates.NotStarted) {
                return this.updateDetails("status", this.AssessmentStates.InProgress, null, true).then( (): any => {
                    this.$scope.$broadcast("STATUS_UPDATED");
                    return this.goToSection(nextSectionId, response.data.sections);
                });
            }
            return this.goToSection(nextSectionId, response.data.sections);
        });
    }

    public updateAnswer(question: any, params?: any): ng.IPromise<any> {
        this.$scope.$broadcast("ANSWER_STARTED");
        return this.DMAnswer.answerQuestion(question, params).then( (response: any): any => {
            if (!response.result) return { result: false };
            const status: number = this.dmAssessment.getStatus();
            const activeSection: any = this.OneAssessment.getActiveSection();
            if (!response.pendingResponses) {
                const readyForApproval: boolean = response.data.activeSection ? response.data.activeSection.isReadyForApproval : false;
                this.setAssessmentApprovalState(readyForApproval);
                this.$scope.$broadcast("ANSWER_CHANGE_FINISHED");
            }
            this.dmAssessment.setSections(this.DMAssessmentNavigation.setSectionAsActive(response.data.sections, activeSection.id));
            if (status === this.AssessmentStates.NotStarted) {
                this.updateDetails("status", this.AssessmentStates.InProgress, null, true).then((): void => {
                    this.$scope.$broadcast("STATUS_UPDATED");
                });
            }
            return { result: true };
        });
    }

    public setAssessmentApprovalState(canApprove?: boolean): void {
        if (isUndefined(canApprove)) {
            const activeSection: IDMAssessmentSection = this.dmAssessment.getActiveSection();
            const readyForApproval: boolean = activeSection.isReadyForApproval;
            this.dmAssessment.setIsReadyForApproval(readyForApproval);
        } else {
            this.dmAssessment.setIsReadyForApproval(canApprove);
        }
    }

    public setAssessmentAction(): void {
        const actions: any = {};
        const id: string = this.dmAssessment.getId();
        const status: number = this.dmAssessment.getStatus();
        const isUnderReview: boolean = status === this.AssessmentStates.UnderReview;
        actions.success = isUnderReview ? this.translate("Approved") : this.translate("submitted");
        actions.error = isUnderReview ? this.translate("approving") : this.translate("submitting");
        if (isUnderReview) {
            this.approveAssessment(id, actions);
        } else {
            this.submitAssessment(id, actions).then((): void => {
                this.$scope.$broadcast("APPROVE_CANCELLED");
            });
        }
    }

    public submitAssessment(id: string, actions: any): ng.IPromise<any> {
        return this.DMAssessments.submitAssessment(id, actions).then((response: any): any | void => {
            if (!response.result || (isArray(response.data) && !response.data.length)) return { result: false, data: null };
            if (this.Permissions.canShow("DataMappingAssessmentsList")) {
                this.$state.go("zen.app.pia.module.datamap.views.assessment.list");
            } else {
                this.$state.go("zen.app.pia.module.thanks", {
                    options: {
                        state: AssessmentThankYouStates.Complete,
                        projectName: this.dmAssessment.getName(),
                        reviewerId: this.dmAssessment.getApprover().Id,
                    },
                    projectId: id,
                    templateType: this.TemplateTypes.NewDatamapping,
                    projectVersion: 1,
                });
            }
        });
    }

    public exitAssessment(): void {
        if (this.Permissions.canShow("DataMappingAssessments")) {
            this.$state.go("zen.app.pia.module.datamap.views.assessment.list");
        } else {
            this.$state.go("zen.app.pia.module.thanks", {
                options: {
                    state: AssessmentThankYouStates.Incomplete,
                    projectName: this.dmAssessment.getName(),
                    reviewerId: this.dmAssessment.getApprover().Id,
                },
                projectId: this.dmAssessment.getId(),
                templateType: this.TemplateTypes.NewDatamapping,
                projectVersion: 1,
            });
        }
    }

    public updateDetails(key: string, value: any, comment?: string, onlyUpdate?: boolean): ng.IPromise<{result: boolean}> {
        this.dmAssessment.setDetail(key, value);
        const request: any = {
            assignedUserId: this.dmAssessment.getRespondent().Id || this.dmAssessment.getRespondent(),
            name: this.dmAssessment.getName(),
            statusId: this.dmAssessment.getStatus(),
            orgGroupId: this.dmAssessment.getOrgGroup().id,
            assetId: this.dmAssessment.getAsset().id,
            approverId: this.dmAssessment.getApprover().Id,
            deadlineDate: this.dmAssessment.getDeadline(),
            comment: comment || "",
            onlyUpdate,
        };
        return this.DMAssessments.updateAssessment(this.dmAssessment.getId(), request).then( (response: any): any => {
            if (!response.result) return { result: false };
            return { result: true };
        });
    }

    public launchReassignmentModal(model: any, options: any, config: any): void {
        const respondent: any = this.dmAssessment.getRespondent();
        let currentUser: string;
        if (respondent) {
            currentUser = respondent;
            if (respondent.FullName || respondent.Email) {
                currentUser = (respondent.FullName || respondent.Email);
            }
        } else {
            currentUser = this.translate("DisabledUser");
        }
        const modalData: any = {
            model,
            config,
            currentUser,
            orgId: this.dmAssessment.getOrgGroup().id,
            assessmentName: this.dmAssessment.getName(),
            dropdownTypeKey: "RespondentDm",
            headlineTranslationKey: "ReassignRespondent",
            updateCallback: (cbModel: any): ng.IPromise<any> => {
                return this.updateDetails("respondent", cbModel);
            },
            callback: (response) => {
                if (!response.result) return;
                this.$scope.$broadcast("RESPONDENT_CHANGED");
            },
        };
        this.ModalService.setModalData(modalData);
        this.ModalService.openModal("reassignmentModal");
    }

    public launchApproverModal(model: any, options: any, config: any): void {
        const approver: any = this.dmAssessment.getApprover();
        let currentUser: string;
        if (approver) {
            currentUser = approver;
            if (approver.FullName || approver.Email) {
                currentUser = (approver.FullName || approver.Email);
            }
        } else {
            currentUser = this.translate("DisabledUser");
        }
        const modalData: any = {
            model,
            config,
            currentUser,
            orgId: this.dmAssessment.getOrgGroup().id,
            assessmentName: this.dmAssessment.getName(),
            dropdownTypeKey: "ApproverDm",
            headlineTranslationKey: "AssignApprover",
            updateCallback: (odel: any): ng.IPromise<any> => {
                return this.updateDetails("approver", odel);
            },
            callback: (response: any): void => {
                if (!response.result) return;
                this.$scope.$broadcast("APPROVER_CHANGED");
            },
        };
        this.ModalService.setModalData(modalData);
        this.ModalService.openModal("reassignmentModal");
    }

    public launchSendBackModal(): void {
        const data: IDMSendBackModal = {
            updateCallback: (comment: string): ng.IPromise<{result: boolean}> => {
                return this.updateDetails("status", this.AssessmentStates.InProgress, comment);
            },
            callback: (result: boolean) => {
                if (!result) return;
                this.$scope.$broadcast("STATUS_UPDATED");
                this.$state.go("zen.app.pia.module.datamap.views.assessment.list");
            },
        };
        this.ModalService.setModalData(data);
        this.ModalService.openModal("sendBackModalComponent");
    }

    private startEventWatchers(): void {
        this.$scope.$on("GO_TO_SECTION", (event: ng.IAngularEvent, sectionId: string): void => {
            this.goToSection(sectionId).then((response: any): void | undefined => {
                if (!response.result) return;
                this.$scope.$broadcast("SECTION_CHANGED");
            });
        });
        this.$scope.$on("COMPLETE_SECTION", (event: ng.IAngularEvent, question: any): void | undefined => {
            this.completeSection(question).then((response: any): void => {
                if (!response.result) return;
                this.$scope.$broadcast("SECTION_CHANGED");
            });
        });
        this.$scope.$on("ANSWER_QUESTION", (event: ng.IAngularEvent, question: any, params: any): void | undefined => {
            this.updateAnswer(question, params).then((response: any): void => {
                if (!response.result) return;
                this.$scope.$broadcast("ANSWER_CHANGED");
            });
        });
        this.$scope.$on("UPDATE_ASSESSMENT_DETAIL", (event: ng.IAngularEvent, key: string, value: any): void => {
            this.updateDetails(key, value).then((response: any): void => {
                this.$scope.$broadcast("DETAILS_CHANGED");
            });
        });
        this.$scope.$on("SUBMIT_ASSESSMENT", (): void => {
            this.setAssessmentAction();
        });
        this.$scope.$on("LAUNCH_SEND_BACK", (event: ng.IAngularEvent): void => {
            this.launchSendBackModal();
        });
        this.$scope.$on("LAUNCH_REASSIGN_RESPONDENT",
            (event: ng.IAngularEvent, model: any, options: any, config: any): void => {
                this.launchReassignmentModal(model, options, config);
            },
        );
        this.$scope.$on("LAUNCH_ASSIGN_APPROVER",
            (event: ng.IAngularEvent, model: any, options: any, config: any): void => {
                this.launchApproverModal(model, options, config);
            },
        );
        this.$scope.$on("EXIT", (): void => {
            this.exitAssessment();
        });
    }

    private getUsers(): ng.IPromise<any> {
        return this.UserStore.getUserList();
    }

    private approveAssessment = (id: string, actions: any): ng.IDeferred<any> => {
        const deferred: any = this.$q.defer();
        this.NotifyService.confirm(this.translate("Confirm"), this.translate("InventoryValuesUpdate"), "", "", true, this.translate("Confirm"), this.translate("Cancel")).then(
            (result: any): void => {
                if (result) {
                    deferred.resolve(this.submitAssessment(id, actions).then((): void => {
                        this.$scope.$broadcast("APPROVE_CANCELLED");
                    }));
                } else {
                    this.$scope.$broadcast("APPROVE_CANCELLED");
                    deferred.resolve({ result });
                }
            },
        );
        return deferred.promise;
    }

    private setUserDetails(): void {
        const allUsers: any = this.getUsers();
        const team: any[] = this.dmAssessment.getTeam();
        const assessment: any = this.dmAssessment.getAssessment();
        const respondentDefault: string = (assessment.respondent && assessment.respondent.fullName) ? assessment.respondent.fullName : "";
        function findTeamMember(members: any[], id: string): any {
            return find(members, { Id: id } as any);
        }
        this.dmAssessment.setRespondent(findTeamMember(allUsers, assessment.respondent.id) || respondentDefault);
        this.dmAssessment.setApprover(findTeamMember(team, assessment.approver.id));
        this.dmAssessment.setCreator(findTeamMember(team, assessment.createdBy.id));
        this.dmAssessment.setDeadline(assessment.deadlineDate);
    }

    private filterUsersByOrg(): any {
        const team: any[] = this.dmAssessment.getTeam();
        const orgId: string = this.dmAssessment.getOrgGroup().id;
        return this.parentTreeFilter(team, this.treeMap, orgId, "OrgGroupId");
    }

    private setReadOnly(): void {
        const status: number = this.dmAssessment.getStatus();
        if ((status === this.AssessmentStates.UnderReview && !this.Permissions.canShow("DataMappingAssessmentsApprove")) || status === this.AssessmentStates.Completed) {
            this.dmAssessment.setReadOnly(true);
        } else {
            this.dmAssessment.setReadOnly(false);
        }
    }

    private getParentId(currentOrgId: string): string {
        return this.treeMap.Parent(currentOrgId);
    }

}

const dmAssessment: ng.IComponentOptions = {
    controller: DmAssessment,
    template: `
        <loading ng-if="!$ctrl.ready" loading-class="dm-assessment__loading", show-text="false"></loading>
        <section
            class="dm-assessment"
            ng-if="$ctrl.ready">
            <assessment-header
                class="dm-assessment__header"
                context="::$ctrl.context"
                >
            </assessment-header>
            <section class="dm-assessment__body flex-full-height">
                <question-list
                    class="dm-assessment__list flex-full-height"
                    context="::$ctrl.context"
                    >
                </question-list>
            </section>
            <assessment-footer></assessment-footer>
        </section>
    `,
};

export default dmAssessment;
