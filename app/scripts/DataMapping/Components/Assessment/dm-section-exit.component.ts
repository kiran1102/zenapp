class DMSectionExit implements ng.IComponentController {

    static $inject: string[] = ["$scope"];

    constructor(readonly $scope: any) {
    }
}

export default {
    bindings: {
        question: "<",
        readOnly: "<?",
    },
    controller: DMSectionExit,
    template: `
        <section class="dm-section-exit centered-max-70">
            <question-content question="$ctrl.question"></question-content>
        </section>
    `,
};
