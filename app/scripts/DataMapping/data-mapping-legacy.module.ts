﻿import * as angular from "angular";

import "diagram-js/assets/diagram-js.css";
import TableColumnProperties from "./Services/table-helpers/table-column-properties.service";
import TableHelper from "./Services/table-helpers/table-helper.service";
import TableKeys from "./Services/table-helpers/table-keys.service";
import TableResources from "./Services/table-helpers/table-resources.service";
import TableSettings from "./Services/table-helpers/table-settings.service";
import DataMapping from "./Services/DataMappingService";
import TableHelpers from "./Services/TableHelpersService";
import Tables from "./Services/TablesService";

import AssessmentModalService from "datamappingservice/Assessment/assessment-modals.service.ts";
import DMAnswer from "datamappingservice/Assessment/dm-answer.service.ts";
import DMAssessmentNavigation from "datamappingservice/Assessment/dm-assessment-navigation.service.ts";
import { DMAssessmentsService } from "datamappingservice/Assessment/dm-assessments.service.ts";
import OneAssessmentListFactory from "datamappingservice/Assessment/one-assessment-list.factory.ts";
import OneAssessmentTable from "datamappingservice/Assessment/one-assessment-table-config.service.ts";
import OneAssessmentFactory from "datamappingservice/Assessment/one-assessment.factory.ts";
import OneTemplateFactory from "datamappingservice/Template/one-template.factory.ts";
import TemplateModalService from "datamappingservice/Template/template-modals.service.ts";
import AssessmentDetailsConfig from "datamappingservice/Assessment/assessment-details-config.service.ts";

import dashboardCard from "datamappingcomponent/Dashboard/dashboard-card.component.ts";
import dashboardCardHeader from "datamappingcomponent/Dashboard/dashboard-card-header.component.ts";
import DashboardHeaderComponent from "datamappingcomponent/Dashboard/dashboard-header.component.ts";
import dashboardRow from "datamappingcomponent/Dashboard/dashboard-row.component.ts";

export const dataMappingLegacyModule = angular.module("zen.datamapping", [
    "ui.router",
])
    .service("TableColumnProperties", TableColumnProperties)
    .service("TableHelper", TableHelper)
    .service("TableKeys", TableKeys)
    .service("TableResources", TableResources)
    .service("TableSettings", TableSettings)
    .factory("DataMapping", DataMapping)
    .factory("TableHelpers", TableHelpers)
    .factory("Tables", Tables)

    .service("AssessmentModals", AssessmentModalService)
    .service("DMAnswer", DMAnswer)
    .service("DMAssessments", DMAssessmentsService)
    .service("OneAssessmentList", OneAssessmentListFactory)
    .service("OneAssessmentTableConfig", OneAssessmentTable)
    .service("OneAssessment", OneAssessmentFactory)
    .service("OneTemplate", OneTemplateFactory)
    .service("TemplateModals", TemplateModalService)
    .service("AssessmentDetailsConfig", AssessmentDetailsConfig)

    .component("dashboardCard", dashboardCard)
    .component("dashboardCardHeader", dashboardCardHeader)
    .component("dashboardHeader", DashboardHeaderComponent)
    .component("dashboardRow", dashboardRow)

    .service("DMAssessmentNavigation", DMAssessmentNavigation)
    ;
