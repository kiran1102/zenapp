

export default function Tables(OneProtocol, $rootScope) {
    /**
     * @name list
     * @returns {Promise} Promise_Tables Returns a promise that will
     * contain a response object with a data property
     * which contains a list of Tables.
     * @description Get a list of Tables.
     * @example Tables.list()
     **/
    function getTablesList() {
        var config = OneProtocol.config("GET", "/DataMapping/GetInventoryType");
        var messages = { "Error": { object: $rootScope.t("Inventories"), action: "get" } };
        return OneProtocol.http(config, messages);
    }

    /**
     * @name get
     * @returns {Promise} Promise_Table Returns a promise that will
     * contain a response object with a data property
     * which contains a Table object.
     * @description Get a Table by Id.
     * @example Tables.get(Id)
     **/
    function getTableById(Id) {
        var config = OneProtocol.config("GET", "/DataMapping/GetInventoryTable/" + Id);
        var messages = { "Error": { object: $rootScope.t("Inventory"), action: "get" } };
        return OneProtocol.http(config, messages);
    }

    function getPage(Id, page, size, filter, search) {
        var params = {
            "pagination.pageSize": size || 20,
            "pagination.pageNumber": page || 1,
            "filter[0].key": filter || null,
            "filter[0].value": search || null,
        };
        var config = OneProtocol.config("GET", "/DataMapping/GetInventoryPage/" + Id, params);
        var messages = { "Error": { object: $rootScope.t("Inventory"), action: "get" } };
        return OneProtocol.http(config, messages);
    }

    /**
     * @name getInventoryRecord
     * @param {string} InventoryId
     * @returns {Promise|Inventory_Record} Promise_Table Returns a promise that will
     * contain a response object with a data property
     * which contains an Inventory Record object.
     * @description Get Inventory by Inventory Id.
     * @example Tables.getRecord(InventoryId)
     **/
    function getInventoryRecord(InventoryId) {
        var config = OneProtocol.config("GET", "/DataMapping/GetInventoryItem/" + InventoryId);
        var messages = { "Error": { Hide: true } };
        return OneProtocol.http(config, messages);
    }

    /**
    @name create
    @param {Object} tableCreateRequest A table create request.
    @returns {Promise} Promise_CreateResult Returns a promise that will
    contain a response object with a data property
    which contains the result of the create request.
    @description Create Table.
    @example Tables.create(tableCreateRequest)
        {
            "TableTypeId": 10,
            "Dictionary": {
                // Column Based Inputs
            }
        }
    **/
    function createInventory(tableCreateRequest) {
        var config = OneProtocol.config("POST", "/DataMapping/AddInventory", [], tableCreateRequest);
        var messages = { "Error": { object: $rootScope.t("InventoryItem"), action: "create" } };
        return OneProtocol.http(config, messages);
    }

    /**
    @name update
    @param {Object} tableUpdateRequest A Table update request.
    @returns {Promise} Promise_UpdateResult Returns a promise that will
    contain a response object with a data property
    which contains the result of the update request.
    @description Update Table.
    @example Tables.update(tableUpdateRequest)
        {
            "TableTypeId": 10,
            "Dictionary": {
                // Column Based Inputs
            }
        }
    **/
    function updateInventory(tableUpdateRequest) {
        var config = OneProtocol.config("PUT", "/DataMapping/UpdateInventory", [], tableUpdateRequest);
        var messages = { "Error": { object: $rootScope.t("InventoryItem"), action: "update" } };
        return OneProtocol.http(config, messages);
    }

    /**
    @name delete
    @param {String} deleteRequestId
    @param {String} tableId
    @returns {Promise} Promise_DeleteResult Returns a promise that will
    contain a response object with a data property
    which contains the result of the delete request.
    @description Remove inventory item.
    @example Tables.delete(deleteRequestId, tableId)
    **/
    function deleteInventory(deleteRequestId, tableId) {
        var uri = _.template("/DataMapping/DeleteInventory?id=${id}&tableTypeId=${tableId}");
        var config = OneProtocol.config("DELETE", uri({ "id": deleteRequestId, "tableId": tableId }));
        var messages = { "Error": { object: $rootScope.t("InventoryItem"), action: "Delete" } };
        return OneProtocol.http(config, messages);
    }

    /**
    @name archive
    @param {Object} archiveRequest archiveRequest including item ID and status of archive
    @returns {Promise} Promise_ArchiveResult Returns a promise that will
    contain a response object with a data property
    which contains the result of the archive request.
    @description archive items
    @example Tables.archive(archiveRequest)
    **/
    function archiveInventory(archiveRequest) {
        // TODO: Replace this API with archive API.
        var config = OneProtocol.config("PUT", "/DataMapping/ApproveInventory/", [], archiveRequest);
        var messages = { "Error": { object: $rootScope.t("InventoryItem"), action: "Archive" } };
        return OneProtocol.http(config, messages);
    }

    /**
    @name approve
    @param {Object} approvalRequest approval request including item ID and status of approved
    @returns {Promise} Promise_ApproveResult Returns a promise that will
    contain a response object with a data property
    which contains the result of the approval request.
    @description Approve items.
    @example Tables.approve(approvalRequest)
    **/
    function approveInventory(approvalRequest) {
        var config = OneProtocol.config("PUT", "/DataMapping/ApproveInventory/", [], approvalRequest);
        var messages = { "Error": { object: $rootScope.t("InventoryItem"), action: "Approve" } };
        return OneProtocol.http(config, messages);
    }

    var service = {
        list: getTablesList,
        get: getTableById,
        getPage: getPage,
        getRecord: getInventoryRecord,
        create: createInventory,
        update: updateInventory,
        delete: deleteInventory,
        archive: archiveInventory,
        approve: approveInventory
    };

    return service;

}

Tables.$inject = ["OneProtocol", "$rootScope"];
