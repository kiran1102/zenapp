
'use strict';

export default function DataMapping($rootScope, $http, NotificationService) {
    var successMessage = _.template('Your ${object} has been ${action}.');
    var errorMessage = _.template('Sorry, there was a problem ${action} your ${object}.');
    var progressMessage = _.template('Please wait as we ${action} your ${object}.');

    function createSuccessCallback(messageParams) {
        if (messageParams) {
            NotificationService.alertSuccess($rootScope.t('Success'), successMessage(messageParams));
        }

        return function (response) {
            return {
                result: true,
                status: response.status,
                data: response.data
            };
        };
    }

    function createProgressCallback() {
        return function (progress) {
            return progress;
        };
    }

    function createErrorCallback(messageParams, hideMessage) {

        return function (error) {
            if (!hideMessage) {
                if (error.data && error.data.Message) {
                    NotificationService.alertWarning($rootScope.t('Error'), error.data.Message);
                } else if (messageParams) {
                    NotificationService.alertError($rootScope.t('Error'), errorMessage(messageParams));
                }
            }

            return {
                result: false,
                status: error.status,
                data: (error.data ? error.data.Message : ''),
            };
        };
    }

    function getGraphCrossBorder() {
        var objectName = 'graph';
        var errorCallbackParams = {
            action: 'loading',
            object: objectName
        };
        var progressCallbackParams = {
            action: 'load',
            object: objectName
        };

        return $http.get('images/cross_border_graph.svg').then(
            createSuccessCallback(),
            createErrorCallback(errorCallbackParams),
            createProgressCallback(progressCallbackParams)
        );
    }

    function getGraphDataFlow() {
        return $http.get('images/data_flow_graph.svg').then(
            function successCallback(response) {
                var packet = {
                    result: true,
                    data: response.data
                };
                return packet;
            },
            function errorCallback(response) {
                NotificationService.alertError($rootScope.t('Error'), $rootScope.t("SorryThereWasAProblemRetrievingGraph"));
                return {
                    result: false,
                    data: response.data
                };
            }
        );
    }

    // service call for retrieving Transfer Data
    function getDataInventoryTransfer() {
        var objectName = 'transfer data';
        var errorCallbackParams = {
            action: 'loading',
            object: objectName
        };
        var progressCallbackParams = {
            action: 'load',
            object: objectName
        };

        return $http.get('/DataInventory/GetTransfers').then(
            createSuccessCallback(),
            createErrorCallback(errorCallbackParams),
            createProgressCallback(progressCallbackParams)
        );
    }

    function getDataInventoryElements() {
        var objectName = 'element data';
        var errorCallbackParams = {
            action: 'loading',
            object: objectName
        };
        var progressCallbackParams = {
            action: 'load',
            object: objectName
        };

        return $http.get('/DataInventory/GetElements').then(
            createSuccessCallback(),
            createErrorCallback(errorCallbackParams),
            createProgressCallback(progressCallbackParams)
        );
    }

    function getDataInventoryAssets() {
        var objectName = 'system data';
        var errorCallbackParams = {
            action: 'loading',
            object: objectName
        };
        var progressCallbackParams = {
            action: 'load',
            object: objectName
        };

        return $http.get('/DataInventory/GetSystems').then(
            createSuccessCallback(),
            createErrorCallback(errorCallbackParams),
            createProgressCallback(progressCallbackParams)
        );
    }

    function getDataMapCrossBorder() {
        var alertText = $rootScope.t("ErrorRetreivingCrossBorderGraph");

        return $http.post('/Export/DataMapCrossBorder').then(
            function successCallback(response) {
                var packet = {
                    result: true,
                    data: response.data
                };
                response.time = (new Date()).getTime();
                $rootScope.APICache.put('DataMapping.getDataMapCrossBorder', response);
                return packet;
            },
            function errorCallback() {
                var packet = {
                    result: false,
                    data: alertText
                };
                return packet;
            }
        );
    }

    // service call for adding transfer data
    function setTransfer(element) {
        var objectName = 'transfer data';

        var params = {
            Value: element.Value
        };

        var errorCallbackParams = {
            action: 'saving',
            object: objectName
        };
        var progressCallbackParams = {
            action: 'save',
            object: objectName
        };

        return $http.post('/DataInventory/AddTransfer', params).then(
            createSuccessCallback(),
            createErrorCallback(errorCallbackParams),
            createProgressCallback(progressCallbackParams)
        );
    }

    function setDataElement(element) {
        var objectName = 'data element';

        var params = {
            ClassificationId: element.ClassificationId,
            CategoryId: element.CategoryId,
            Value: element.Value,
            Approved: true,
            ResponseId: (element.ResponseId || null)
        };

        var errorCallbackParams = {
            action: 'saving',
            object: objectName
        };

        var progressCallbackParams = {
            action: 'save',
            object: objectName
        };

        return $http.post('/DataInventory/AddElement', params).then(
            createSuccessCallback(),
            createErrorCallback(errorCallbackParams),
            createProgressCallback(progressCallbackParams)
        );
    }

    //service call for updating the transfer data
    function updateTransfer(element) {
        var objectName = 'transfer data';

        var params = {
            Value: element.Value,
            Id: element.Id
        };
        var errorCallbackParams = {
            action: 'saving',
            object: objectName
        };
        var progressCallbackParams = {
            action: 'save',
            object: objectName
        };

        return $http.put('/DataInventory/UpdateTransfer', params).then(
            createSuccessCallback(),
            createErrorCallback(errorCallbackParams),
            createProgressCallback(progressCallbackParams)
        );
    }

    function updateDataElement(element) {
        var objectName = 'data element';

        var params = {
            ClassificationId: element.ClassificationId,
            CategoryId: element.CategoryId,
            Value: element.Value,
            Id: element.Id
        };
        var errorCallbackParams = {
            action: 'saving',
            object: objectName
        };
        var progressCallbackParams = {
            action: 'save',
            object: objectName
        };

        return $http.put('/DataInventory/UpdateElement', params).then(
            createSuccessCallback(),
            createErrorCallback(errorCallbackParams),
            createProgressCallback(progressCallbackParams)
        );
    }

    function updateCategory(category) {
        var objectName = 'category';

        var params = {
            CategoryId: category.CategoryId,
            CategoryName: category.CategoryName,
            ElementId: category.ElementId
        };
        var errorCallbackParams = {
            action: 'saving',
            object: objectName
        };
        var progressCallbackParams = {
            action: 'save',
            object: objectName
        };

        return $http.put('/DataInventory/UpdateCategory', params).then(
            createSuccessCallback(),
            createErrorCallback(errorCallbackParams),
            createProgressCallback(progressCallbackParams)
        );
    }

    function updateClassification(classification) {
        var objectName = 'classification';

        var params = {
            ClassificationId: classification.ClassificationId,
            ClassificationName: classification.ClassificationName,
            ElementId: classification.ElementId
        };
        var errorCallbackParams = {
            action: 'saving',
            object: objectName
        };
        var progressCallbackParams = {
            action: 'save',
            object: objectName
        };

        return $http.put('/DataInventory/UpdateClassification', params).then(
            createSuccessCallback(),
            createErrorCallback(errorCallbackParams),
            createProgressCallback(progressCallbackParams)
        );
    }

    function setAsset(asset) {
        var objectName = 'asset';

        // var Reminder = Project.Reminder.Show ? Project.Reminder.Value : null;
        var params = {
            LocationId: asset.locationId,
            Value: asset.Value
        };
        var errorCallbackParams = {
            action: 'saving',
            object: objectName
        };
        var progressCallbackParams = {
            action: 'save',
            object: objectName
        };

        return $http.post('/DataInventory/AddSystem', params).then(
            createSuccessCallback(),
            createErrorCallback(errorCallbackParams),
            createProgressCallback(progressCallbackParams)
        );
    }

    function updateAsset(asset) {
        var objectName = 'asset';

        var params = {
            LocationId: asset.locationId,
            Value: asset.Value,
            Id: asset.Id
        };
        var errorCallbackParams = {
            action: 'saving',
            object: objectName
        };
        var progressCallbackParams = {
            action: 'save',
            object: objectName
        };

        return $http.put('/DataInventory/UpdateSystem', params).then(
            createSuccessCallback(),
            createErrorCallback(errorCallbackParams),
            createProgressCallback(progressCallbackParams)
        );
    }

    function getSystemLocations() {
        var objectName = 'system locations';
        var errorCallbackParams = {
            action: 'loading',
            object: objectName
        };
        var progressCallbackParams = {
            action: 'load',
            object: objectName
        };

        return $http.get('/DataInventory/GetSystemLocations').then(
            createSuccessCallback(),
            createErrorCallback(errorCallbackParams),
            createProgressCallback(progressCallbackParams)
        );
    }

    function getElementClassifications() {
        var objectName = 'element classifications';
        var errorCallbackParams = {
            action: 'loading',
            object: objectName
        };
        var progressCallbackParams = {
            action: 'load',
            object: objectName
        };

        return $http.get('/DataInventory/GetElementClassifications').then(
            createSuccessCallback(),
            createErrorCallback(errorCallbackParams),
            createProgressCallback(progressCallbackParams)
        );
    }

    function getElementCategories() {
        var objectName = 'element categories';
        var errorCallbackParams = {
            action: 'loading',
            object: objectName
        };
        var progressCallbackParams = {
            action: 'load',
            object: objectName
        };

        return $http.get('/DataInventory/GetElementCategories').then(
            createSuccessCallback(),
            createErrorCallback(errorCallbackParams),
            createProgressCallback(progressCallbackParams)
        );
    }

    // delete service call for deleting a transfer data
    function deleteTransfer(Id) {
        var objectName = 'transfer data';
        var errorCallbackParams = {
            action: 'deleting',
            object: objectName
        };
        var progressCallbackParams = {
            action: 'delete',
            object: objectName
        };

        return $http.delete('/DataInventory/DeleteTransfer/' + Id).then(
            createSuccessCallback(),
            createErrorCallback(errorCallbackParams),
            createProgressCallback(progressCallbackParams)
        );
    }

    function deleteDataElement(Id) {
        var objectName = 'data element';
        var errorCallbackParams = {
            action: 'deleting',
            object: objectName
        };
        var progressCallbackParams = {
            action: 'delete',
            object: objectName
        };

        return $http.delete('/DataInventory/DeleteElement/' + Id).then(
            createSuccessCallback(),
            createErrorCallback(errorCallbackParams, true),
            createProgressCallback(progressCallbackParams)
        );
    }

    function deleteAsset(Id) {
        var objectName = 'asset';
        var errorCallbackParams = {
            action: 'deleting',
            object: objectName
        };
        var progressCallbackParams = {
            action: 'delete',
            object: objectName
        };

        return $http.delete('/DataInventory/DeleteSystem/' + Id).then(
            createSuccessCallback(),
            createErrorCallback(errorCallbackParams),
            createProgressCallback(progressCallbackParams)
        );
    }

    function getDataFlows() {
        var objectName = 'data flows';
        var errorCallbackParams = {
            action: 'loading',
            object: objectName
        };
        var progressCallbackParams = {
            action: 'load',
            object: objectName
        };

        return $http.get('/DataFlow').then(
            createSuccessCallback(),
            createErrorCallback(errorCallbackParams),
            createProgressCallback(progressCallbackParams)
        );
    }

    function getElementRisk(elementId) {
        var objectName = 'element risk';
        var errorCallbackParams = {
            action: 'loading',
            object: objectName
        };
        var progressCallbackParams = {
            action: 'load',
            object: objectName
        };

        return $http.get('/DataInventory/GetRisk/' + elementId).then(
            createSuccessCallback(),
            createErrorCallback(errorCallbackParams),
            createProgressCallback(progressCallbackParams)
        );
    }

    function addElementRisk(options) {
        var objectName = 'risk';

        var params = {
            InventoryId: options.InventoryId,
            RiskLevelId: options.RiskLevelId,
            Description: options.Description,
            Recommendation: options.Recommendation
        };
        var successCallbackParams = {
            action: 'added',
            object: objectName
        };
        var errorCallbackParams = {
            action: 'adding',
            object: objectName
        };
        var progressCallbackParams = {
            action: 'add',
            object: objectName
        };

        return $http.post('/DataInventory/AddRisk', params).then(
            createSuccessCallback(successCallbackParams),
            createErrorCallback(errorCallbackParams),
            createProgressCallback(progressCallbackParams)
        );
    }

    function updateElementRisk(options) {
        var objectName = 'risk';

        var params = {
            InventoryRiskId: options.InventoryRiskId,
            RiskLevelId: options.RiskLevelId,
            Description: options.Description,
            Recommendation: options.Recommendation
        };
        var successCallbackParams = {
            action: 'saved',
            object: objectName
        };
        var errorCallbackParams = {
            action: 'saving',
            object: objectName
        };
        var progressCallbackParams = {
            action: 'save',
            object: objectName
        };

        return $http.put('/DataInventory/UpdateRisk', params).then(
            createSuccessCallback(successCallbackParams),
            createErrorCallback(errorCallbackParams),
            createProgressCallback(progressCallbackParams)
        );
    }

    function deleteElementRisk(Id) {
        var objectName = 'risk';
        var successCallbackParams = {
            action: 'deleted',
            object: objectName
        };
        var errorCallbackParams = {
            action: 'deleting',
            object: objectName
        };
        var progressCallbackParams = {
            action: 'delete',
            object: objectName
        };

        return $http.delete('/DataInventory/DeleteRisk/' + Id).then(
            createSuccessCallback(successCallbackParams),
            createErrorCallback(errorCallbackParams),
            createProgressCallback(progressCallbackParams)
        );
    }

    function setDataMapCrossBorder() {
        var objectName = 'graph';
        var errorCallbackParams = {
            action: 'loading',
            object: objectName
        };
        var progressCallbackParams = {
            action: 'load',
            object: objectName
        };

        return $http.get('images/cross_border_graph.svg').then(
            createSuccessCallback(),
            createErrorCallback(errorCallbackParams),
            createProgressCallback(progressCallbackParams)
        );
    }

    function getProjectElements() {
        var objectName = 'project elements';
        var errorCallbackParams = {
            action: 'loading',
            object: objectName
        };
        var progressCallbackParams = {
            action: 'load',
            object: objectName
        };
        return $http.get('/dataflow/GetProjectElements').then(
            createSuccessCallback(),
            createErrorCallback(errorCallbackParams),
            createProgressCallback(progressCallbackParams)
        );
    }


    return {
        getGraphCrossBorder: getGraphCrossBorder,
        getDataInventoryElements: getDataInventoryElements,
        getDataInventoryAssets: getDataInventoryAssets,
        getDataMapCrossBorder: getDataMapCrossBorder,
        setDataElement: setDataElement,
        updateDataElement: updateDataElement,
        updateClassification: updateClassification,
        updateCategory: updateCategory,
        setAsset: setAsset,
        updateAsset: updateAsset,
        getSystemLocations: getSystemLocations,
        getElementClassifications: getElementClassifications,
        getElementCategories: getElementCategories,
        deleteDataElement: deleteDataElement,
        deleteAsset: deleteAsset,
        setTransfer: setTransfer,
        updateTransfer: updateTransfer,
        getDataInventoryTransfer: getDataInventoryTransfer,
        deleteTransfer: deleteTransfer,
        getDataFlows: getDataFlows,
        getElementRisk: getElementRisk,
        addElementRisk: addElementRisk,
        updateElementRisk: updateElementRisk,
        deleteElementRisk: deleteElementRisk,
        setDataMapCrossBorder: setDataMapCrossBorder,
        getGraphDataFlow: getGraphDataFlow,
        getProjectElements: getProjectElements
    };
}

DataMapping.$inject = ['$rootScope', '$http', 'NotificationService'];
