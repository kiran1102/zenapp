import { AssessmentStates } from "constants/assessment.constants";
import { InventoryStatuses, TableInputTypes } from "enums/inventory.enum";
import { EmptyGuid } from "constants/empty-guid.constant";
import { Regex } from "constants/regex.constant";

export default function TableHelpers($q, $filter, $state, $rootScope, Tables, Users, OrgGroups, $uibModal, Permissions, TableHelper, ENUMS) {
    var mocks = {
        Events: {
            Defaults: {
                getDefaultItem: function (keys, columns) {
                    var defaultItem = {};
                    // Foreach column build out default row by cell based on column input type.
                    if (_.isArray(columns)) {
                        _.each(columns, function (column, index) {
                            // Get the key of the column or assume it based on (index + 1) * 10 parsed as a string.
                            var key = (column[keys.Column_ModelKey] || ((index + 1) * 10)).toString();
                            defaultItem[key] = getColumnDefaultCellByInput(keys, key);
                        });
                    }
                    return defaultItem;
                },
                isEqual: function (keys, item1, item2) {
                    return !!(item1 && item2 &&
                        (item1[keys.Item_Id] === item2[keys.Item_Id]) &&
                        (item1[keys.Item_Name] === item2[keys.Item_Name])
                    );
                },
                getPage: getPage,
                formatPaginationConfig: formatPaginationConfig,
            },
            Elements: {
                getItemId: function (keys, item) {
                    return (item && item["10"]) ? item["10"][keys.Item_Id] : "";
                },
                setItemId: function (keys, item, id) {
                    if (item) {
                        item["10"][keys.Item_Id] = id;
                    }
                    return item;
                },
                isEqual: function (keys, item1, item2) {
                    return !!(item1 && item2 &&
                        (item1[keys.Item_Id] === item2[keys.Item_Id]) &&
                        (item1[keys.Item_Name] === item2[keys.Item_Name])
                    );
                },
                getDefaultItem: function (keys, columns) {
                    var defaultItem = {
                        "10": getColumnDefaultCellByInput(keys, "10", columns),
                        "20": getColumnDefaultCellByValue(keys, "20", columns),
                        "30": getColumnDefaultCellByInput(keys, "30", columns),
                        "40": getColumnDefaultCellByInput(keys, "40", columns),
                        "50": getColumnDefaultCellByInput(keys, "50", columns),
                        "60": { "ValueId": InventoryStatuses.Approved, "Value": "Approved" }
                    };
                    return defaultItem;
                },
                searchFilter: searchFilter,
                isDisabled: function (keys, item) {
                    // If item does not exist, or item exists but is pending, then disable.
                    return (item ? !!(item["60"] ? (item["60"][keys.Item_Name] === "Pending") : true) : true);
                },
                add: function (keys, item) {
                    var request = {
                        TableTypeId: ENUMS.InventoryTableIds.Elements,
                        Dictionary: {
                            "10": item["10"] ? item["10"][keys.Item_Name] : "",
                            "20": item["20"] ? item["20"][keys.Item_Id] : "",
                            "30": item["30"] ? item["30"][keys.Item_Id] : ""
                        }
                    };
                    return Tables.create(request);
                },
                update: function (keys, item) {
                    var request = {
                        TableTypeId: ENUMS.InventoryTableIds.Elements,
                        Id: item["10"] ? item["10"][keys.Item_Id] : "",
                        Dictionary: {
                            "10": item["10"] ? item["10"][keys.Item_Name] : "",
                            "20": item["20"] ? item["20"][keys.Item_Id] : "",
                            "30": item["30"] ? item["30"][keys.Item_Id] : ""
                        }
                    };
                    return Tables.update(request);
                },
                afterSave: function (item, data, columns, keys) {
                    return tableAfterSave(item, data, columns, keys);
                },
                approve: function (keys, item) {
                    var request = {
                        ResponseId: item["10"] ? item["10"][keys.Item_Id] : "",
                        Status: InventoryStatuses.Approved
                    };
                    return Tables.approve(request);
                },
                delete: function (keys, item, tableId) {
                    var InventoryId = item["10"] ? item["10"][keys.Item_Id] : "";
                    return Tables.delete(InventoryId, tableId);
                },
                options: [
                    {
                        Name: "Approve",
                        Show: showApproveElement,
                        Action: approveElementAction,
                        Description: function (name, count) { return getOptionDescription(name, "Approve", count); },
                        DisabledDescription: function (name, count) { return getOptionDisabledDescription(name, "approved", count); }
                    },
                    {
                        Name: "Delete",
                        Show: showDeleteElement,
                        Action: deleteAction,
                        Description: function (name, count) { return getOptionDescription(name, "Delete", count); },
                        DisabledDescription: function (name, count) { return getOptionDisabledDescription(name, "deleted", count); }
                    }
                ],
                getPage: getPage,
                formatPaginationConfig: formatPaginationConfig,
            },
            Applications: {
                getItemId: function (keys, item) {
                    return item ? item["10"][keys.Item_Id] : "";
                },
                setItemId: function (keys, item, id) {
                    if (item) {
                        item["10"][keys.Item_Id] = id;
                    }
                    return item;
                },
                isEqual: function (keys, item1, item2) {
                    return !!(item1 && item2 &&
                        (item1[keys.Item_Id] === item2[keys.Item_Id]) &&
                        (item1[keys.Item_Name] === item2[keys.Item_Name])
                    );
                },
                getDefaultItem: function (keys, columns) {
                    var defaultItem = {
                        "10": getColumnDefaultCellByInput(keys, "10", columns),
                        "20": getColumnDefaultCellByInput(keys, "20", columns),
                        "30": getColumnDefaultCellByInput(keys, "30", columns),
                        "40": getColumnDefaultCellByInput(keys, "40", columns),
                        "50": getColumnDefaultCellByValue(keys, "50", columns),
                        "60": getColumnDefaultCellByValue(keys, "60", columns),
                        "90": { "ValueId": InventoryStatuses.Approved, "Value": "Approved" },
                        "100": getColumnDefaultCellByValue(keys, "100", columns),
                        "110": getColumnDefaultCellByValue(keys, "110", columns),
                        "120": getColumnDefaultCellByValue(keys, "120", columns)
                    };
                    return defaultItem;
                },
                searchFilter: searchFilter,
                add: function (keys, item) {
                    var request = {
                        TableTypeId: ENUMS.InventoryTableIds.Assets,
                        Dictionary: {
                            "10": item["10"] ? item["10"][keys.Item_Name] : "",
                            // "20": item["20"] ? item["20"][keys.Item_Name] : "",
                            // "30": item["30"] ? item["30"][keys.Item_Name] : "",
                            "40": item["40"] ? item["40"][keys.Item_Id] : "",
                            "50": item["50"] ? item["50"][keys.Item_Id] : "",
                            "60": item["60"] ? item["60"][keys.Item_Id] : "",
                            "100": item["100"] ? item["100"][keys.Item_Id] : "",
                            "110": item["110"] ? item["110"][keys.Item_Id] : "",
                            "120": item["120"] ? item["120"][keys.Item_Id] : ""
                        }
                    };
                    return Tables.create(request);
                },
                update: function (keys, item) {
                    var request = {
                        TableTypeId: ENUMS.InventoryTableIds.Assets,
                        Id: item["10"] ? item["10"][keys.Item_Id] : "",
                        Dictionary: {
                            "10": item["10"] ? item["10"][keys.Item_Name] : "",
                            // "20": item["20"] ? item["20"][keys.Item_Name] : "",
                            // "30": item["30"] ? item["30"][keys.Item_Name] : "",
                            "40": item["40"] ? item["40"][keys.Item_Id] : "",
                            "50": item["50"] ? item["50"][keys.Item_Id] : "",
                            "60": item["60"] ? item["60"][keys.Item_Id] : "",
                            "100": item["100"] ? item["100"][keys.Item_Id] : "",
                            "110": item["110"] ? item["110"][keys.Item_Id] : "",
                            "120": item["120"] ? item["120"][keys.Item_Id] : ""
                        }
                    };
                    return Tables.update(request);
                },
                afterSave: function (item, data, columns, keys) {
                    tableAfterSave(item, data, columns, keys);
                },
                delete: function (keys, item, tableId) {
                    var InventoryId = item["10"] ? item["10"][keys.Item_Id] : "";
                    return Tables.delete(InventoryId, tableId);
                },
                options: [
                    {
                        Name: "Delete",
                        Show: showDelete,
                        Action: deleteAction,
                        Description: function (name, count) { return getOptionDescription(name, "Delete", count); },
                        DisabledDescription: function (name, count) { return getOptionDisabledDescription(name, "deleted", count); }
                    }
                ],
                getPage: getPage,
                formatPaginationConfig: formatPaginationConfig,
            },
            Processes: {
                getItemId: function (keys, item) {
                    return item ? item["10"][keys.Item_Id] : "";
                },
                setItemId: function (keys, item, id) {
                    if (item) {
                        item["10"][keys.Item_Id] = id;
                    }
                    return item;
                },
                isEqual: function (keys, item1, item2) {
                    return !!(item1 && item2 &&
                        (item1[keys.Item_Id] === item2[keys.Item_Id]) &&
                        (item1[keys.Item_Name] === item2[keys.Item_Name])
                    );
                },
                getDefaultItem: function (keys, columns) {
                    var defaultItem = {
                        "10": getColumnDefaultCellByInput(keys, "10", columns),
                        "20": getColumnDefaultCellByInput(keys, "20", columns),
                        "30": getColumnDefaultCellByValue(keys, "30", columns),
                        "40": getColumnDefaultCellByInput(keys, "40", columns),
                        "50": getColumnDefaultCellByInput(keys, "50", columns),
                        "60": getColumnDefaultCellByInput(keys, "60", columns),
                        "70": { "ValueId": EmptyGuid, "Value": AssessmentStates.NotStarted }
                    };
                    return defaultItem;
                },
                searchFilter: searchFilter,
                add: function (keys, item) {
                    var request = {
                        TableTypeId: ENUMS.InventoryTableIds.Processes,
                        Dictionary: {
                            "10": item["10"] ? item["10"][keys.Item_Name] : "",
                            "20": item["20"] ? item["20"][keys.Item_Name] : "",
                            "30": item["30"] ? item["30"][keys.Item_Id] : "",
                            "40": item["40"] ? item["40"][keys.Item_Id] : ""
                        }
                    };
                    return Tables.create(request);
                },
                update: function (keys, item) {
                    var request = {
                        TableTypeId: ENUMS.InventoryTableIds.Processes,
                        Id: item["10"] ? item["10"][keys.Item_Id] : "",
                        Dictionary: {
                            "10": item["10"] ? item["10"][keys.Item_Name] : "",
                            "20": item["20"] ? item["20"][keys.Item_Name] : "",
                            "30": item["30"] ? item["30"][keys.Item_Id] : "",
                            "40": item["40"] ? item["40"][keys.Item_Id] : ""
                        }
                    };
                    return Tables.update(request);
                },
                afterSave: function (item, data, columns, keys) {
                    tableAfterSave(item, data, columns, keys);
                },
                archive: function (keys, item) {
                    var deferred = $q.defer();
                    deferred.resolve({ result: false, data: "" });
                    return deferred.promise;
                    // TODO: Use the real request below once the API exists.
                    // var request = {
                    //     ResponseId: item["10"] ? item["10"][keys.Item_Id] : "",
                    //     Status: InventoryStatuses.Archived
                    // };
                    // return Tables.archive(request);
                },
                delete: function (keys, item, tableId) {
                    var InventoryId = item["10"] ? item["10"][keys.Item_Id] : "";
                    return Tables.delete(InventoryId, tableId);
                },
                hasUpdates: function (rows, keys, resourceKeys) {
                    if (_.isArray(rows) && Permissions.canShow(resourceKeys.Save)) {
                        var idComparator = {
                            Key: keys.Item_Id,
                            Cell: "10",
                            CheckTruthy: true
                        };
                        var mainComparator = {
                            Key: "hasUpdates",
                            Cell: "",
                            CheckFalsey: true
                        };
                        var comparators = [idComparator, mainComparator];
                        var canDo = $filter("TableComparator")(rows, comparators, true);
                        var allowed = (canDo.length && canDo.length === rows.length);
                        return (allowed ? canDo : false);
                    }
                    return false;
                },
                canLaunch: function (rows, keys, resourceKeys) {
                    if (_.isArray(rows) && Permissions.canShow(resourceKeys.Launch)) {
                        var selectedRows = $filter("filter")(rows, { "selected": true });
                        if (selectedRows.length) {
                            var mainComparator = {
                                Key: keys.Item_Name,
                                Cell: "70",
                                Value: AssessmentStates.NotStarted
                            };
                            var comparators = [mainComparator];
                            var canDo = $filter("TableComparator")(selectedRows, comparators, true);
                            var allowed = (canDo.length && canDo.length === selectedRows.length);
                            return (allowed ? canDo : false);
                        }
                    }
                    return false;
                },
                launch: function (items, columns, keys, scope, callback) {
                    var modalInstance = $uibModal.open({
                        templateUrl: "AssignmentModal.html",
                        controller: "AssignDMAssessmentsCtrl",
                        size: "lg",
                        scope: scope,
                        backdrop: "static",
                        keyboard: false,
                        resolve: {
                            Processes: function () {
                                return _.map(items,
                                    function (item) {
                                        return {
                                            Name: (item["10"] ? item["10"].Value.substring(0, 100) : ""),
                                            Description: (item["20"] ? item["20"].Value : ""),
                                            OrgGroupId: (item["30"] ? item["30"].ValueId : ""),
                                            LeadId: (item["40"] ? item["40"].ValueId : ""),
                                            InventoryId: (item["10"] ? item["10"].ValueId : ""),
                                        };
                                    }
                                );
                            },
                            Columns: function () {
                                return columns;
                            },
                            Keys: function () {
                                return keys;
                            },
                            NewInventory: false,
                            TemplateType: null,
                            TableName: function () {
                                return $rootScope.t("ProcessingActivities");
                            },
                        }
                    });
                    modalInstance.result.then(function (response) {
                        callback(response);
                    });
                },
                openProject: function (item, cellKey, keys) {
                    if (item && cellKey && item[cellKey] && item[cellKey][keys.Item_Id]) {
                        var route = "zen.app.pia.module.datamap.views.assessment-old.single.module.questions";
                        var id = item[cellKey][keys.Item_Id];
                        var version = item[cellKey][keys.Item_Version] || 1;
                        if (id && id !== EmptyGuid && version) {
                            $state.go(route, {
                                "Id": id,
                                "Version": version
                            });
                        }
                    }
                },
                options: [
                    // TODO: Add this back once the Archive Process feature is complete.
                    // {
                    //     Name: "Archive",
                    //     Show: showArchiveProcesses,
                    //     Action: archiveAction,
                    //     Description: function(name, count){ return getOptionDescription(name, "Archive", count); },
                    //     DisabledDescription: function(name, count){ return getOptionDisabledDescription(name, "archived", count); }
                    // },
                    {
                        Name: "Delete",
                        Show: showDeleteProcesses,
                        Action: deleteAction,
                        Description: function (name, count) { return getOptionDescription(name, "Delete", count); },
                        DisabledDescription: function (name, count) { return getOptionDisabledDescription(name, "deleted", count); }
                    }
                ],
                getPage: getPage,
                formatPaginationConfig: formatPaginationConfig,
            }
        },
        DefaultCells: {
            "Unknown": {
                "Value": "Unknown",
                "ValueId": EmptyGuid
            },
            "Empty": {
                "Value": "",
                "ValueId": ""
            }
        }
    };

    /**
    * @name searchFilter
    * @param {Array} items
    * @param {string} string
    * @description Filter array of items by string.
    * @return {Array}
    **/
    function searchFilter(items, string) {
        return ((_.isArray(items) && string) ? $filter("filter")(items, string) : items);
    }

    /**
     * @name getOptionDescription
     * @param {string} name
     * @param {string} action
     * @param {number} count
     * @description return a string interpolated description of an option.
     */
    function getOptionDescription(name, action, count) {
        return _.template("${action} ${name}")({ name: name, action: action });
    }

    /**
     * @name getOptionDisabledDescription
     * @param {string} name
     * @param {string} action
     * @param {number} count
     * @description return a string interpolated description of a disabled option.
     */
    function getOptionDisabledDescription(name, action, count) {
        return _.template("Some selected ${name} cannot be ${action}")({ name: name, action: action });
    }

    /**
    * @name showApproveElement
    * @param {Array} rows
    * @param {Dictionary} keys
    * @param {Dictionary} resourceKeys
    * @param {Boolean} returnArr
    * @description Given a set of selected rows, return whether the approve option be shown.
    * @return {Boolean_or_Array}
    **/
    function showApproveElement(rows, keys, resourceKeys, returnArr) {
        if (_.isArray(rows) && Permissions.canShow(resourceKeys.Approve)) {
            var selectedRows = $filter("filter")(rows, { "selected": true });
            if (selectedRows.length) {
                var mainComparator = {
                    Key: keys.Item_Name,
                    Cell: "60",
                    Value: "Pending"
                };
                var comparators = [mainComparator];
                var pending = $filter("TableComparator")(selectedRows, comparators);
                var allowed = (pending.length && pending.length === selectedRows.length);
                return (returnArr ? (allowed ? pending : []) : allowed);
            }
        }
        return (returnArr ? [] : false);
    }

    /**
    * @name approveElementAction
    * @param {String} tableId
    * @param {Array} rows
    * @param {Array} filteredRows
    * @param {Array} columns
    * @param {Dictionary} keys
    * @param {Dictionary} resourceKeys
    * @param {Function} callback
    * @description Approve selected items from rows.
    **/
    function approveElementAction(tableId, rows, filteredRows, columns, keys, resourceKeys, callback) {
        var deferred = $q.defer();
        var selectedRows = showApproveElement(rows, keys, resourceKeys, true);
        var events = getTableEvents(tableId);
        if (tableId && keys && selectedRows && _.isFunction(events.approve)) {
            var promiseArray = [];
            _.each(selectedRows, function (item) {
                var id = item["10"] ? item["10"][keys.Item_Id] : "";
                if (id) {
                    var promise = events.approve(keys, item).then(
                        function (response) {
                            if (response.result) {
                                item["60"][keys.Item_Name] = "Approved";
                                item = events.afterSave(item, response.data, columns, keys);
                                item.selected = false;
                            }
                            return response;
                        }
                    );
                    promiseArray.push(promise);
                }
                else {
                    var deferred = $q.defer();
                    deferred.resolve({
                        result: true,
                        data: ""
                    });
                    promiseArray.push(deferred.promise);
                }
            });
            $q.all(promiseArray).then(
                function (responses) {
                    if (_.isFunction(callback)) {
                        callback(rows);
                    }
                    deferred.resolve({ result: true, data: responses });
                }
            );
        }
        else {
            if (_.isFunction(callback)) {
                callback(false);
            }
            deferred.resolve({ result: false, data: "" });
        }
        return deferred.promise;
    }

    /**
    * @name showDelete
    * @param {Array} rows
    * @param {Dictionary} keys
    * @param {Dictionary} resourceKeys
    * @param {Boolean} returnArr
    * @description Given a set of selected rows, return whether the delete option be shown.
    * @return {Boolean}
    **/
    function showDelete(rows, keys, resourceKeys, returnArr) {
        if (_.isArray(rows) && Permissions.canShow(resourceKeys.Delete)) {
            var selectedRows = $filter("filter")(rows, { "selected": true });
            return selectedRows.length;
        }
        return false;
    }

    /**
    * @name showDeleteElement
    * @param {Array} rows
    * @param {Dictionary} keys
    * @param {Dictionary} resourceKeys
    * @param {Boolean} returnArr
    * @description Given a set of selected rows, return whether the delete option be shown for Elements.
    * @return {Boolean}
    **/
    function showDeleteElement(rows, keys, resourceKeys, returnArr) {
        if (_.isArray(rows) && Permissions.canShow(resourceKeys.Delete)) {
            var selectedRows = $filter("filter")(rows, { "selected": true });
            if (selectedRows.length) {
                var idComparator = {
                    Key: keys.Item_Id,
                    Cell: "10",
                    CheckFalsey: true
                };
                var mainComparator = {
                    Key: keys.Item_Name,
                    Cell: "60",
                    Value: "Approved"
                };
                var comparators = [idComparator, mainComparator];
                var canDo = $filter("TableComparator")(selectedRows, comparators);
                var allowed = (canDo.length && canDo.length === selectedRows.length);
                return (returnArr ? (allowed ? canDo : []) : allowed);
            }
        }
        return (returnArr ? [] : false);
    }

    /**
    * @name showDeleteProcesses
    * @param {Array} rows
    * @param {Dictionary} keys
    * @param {Dictionary} resourceKeys
    * @param {Boolean} returnArr
    * @description Given a set of selected rows, return whether the delete option be shown for Processes.
    * @return {Boolean}
    **/
    function showDeleteProcesses(rows, keys, resourceKeys, returnArr) {
        if (_.isArray(rows) && Permissions.canShow(resourceKeys.Delete)) {
            var selectedRows = $filter("filter")(rows, { "selected": true });
            if (selectedRows.length) {
                var emptyComparator = {
                    Key: keys.Item_Id,
                    Cell: "10",
                    CheckFalsey: true
                };
                var mainComparator = {
                    Key: keys.Item_Name,
                    Cell: "70",
                    Value: AssessmentStates.NotStarted
                };
                var comparators = [emptyComparator, mainComparator];
                var canDo = $filter("TableComparator")(selectedRows, comparators);
                var allowed = (canDo.length && canDo.length === selectedRows.length);
                return (returnArr ? (allowed ? canDo : []) : allowed);
            }
        }
        return (returnArr ? [] : false);
    }

    /**
    * @name deleteAction
    * @param {String} tableId
    * @param {Array} rows
    * @param {Array} filteredRows
    * @param {Array} columns
    * @param {Dictionary} keys
    * @param {Dictionary} resourceKeys
    * @param {Function} callback
    * @description Delete selected items from rows.
    **/
    function deleteAction(tableId, rows, filteredRows, columns, keys, resourceKeys, callback) {
        var deferred = $q.defer();
        var selectedRows = $filter("filter")(filteredRows, { "selected": true });
        var events = getTableEvents(tableId);
        if (tableId && keys && selectedRows && _.isFunction(events.delete)) {
            var promiseArray = [];
            _.each(selectedRows, function (item) {
                var id = item["10"] ? item["10"][keys.Item_Id] : "";
                if (id) {
                    var promise = events.delete(keys, item, tableId).then(
                        function (response) {
                            if (response.result) {
                                item.MarkedForDeletion = true;
                            }
                            return response;
                        }
                    );
                    promiseArray.push(promise);
                }
                else {
                    var deferred = $q.defer();
                    item.MarkedForDeletion = true;
                    deferred.resolve({
                        result: true,
                        data: "",
                        MarkedForDeletion: true
                    });
                    promiseArray.push(deferred.promise);
                }
            });
            $q.all(promiseArray).then(
                function (responses) {
                    // Remove all items that are marked for delete;
                    rows = $filter("filter")(rows, {
                        MarkedForDeletion: "!true"
                    });

                    if (_.isFunction(callback)) {
                        callback(rows);
                    }
                    deferred.resolve({ result: true, data: responses });
                }
            );
        }
        else {
            if (_.isFunction(callback)) {
                callback(false);
            }
            deferred.resolve({ result: false, data: "" });
        }
        return deferred.promise;
    }

    /**
    * @name showArchiveProcess
    * @param {Array} rows
    * @param {Dictionary} keys
    * @param {Dictionary} resourceKeys
    * @param {Boolean} returnArr
    * @description Given a set of selected rows, return whether the delete option be shown for Processes.
    * @return {Boolean}
    **/
    function showArchiveProcesses(rows, keys, resourceKeys, returnArr) {
        if (_.isArray(rows) && Permissions.canShow(resourceKeys.Archive)) {
            var selectedRows = $filter("filter")(rows, { "selected": true });
            if (selectedRows.length) {
                var mainComparator = {
                    Key: keys.Item_Name,
                    Cell: "70",
                    Value: AssessmentStates.Completed
                };
                var canDo = $filter("TableComparator")(selectedRows, [mainComparator]);
                var allowed = (canDo.length && canDo.length === selectedRows.length);
                return (returnArr ? (allowed ? canDo : []) : allowed);
            }
        }
        return (returnArr ? [] : false);
    }

    /**
    * @name archiveAction
    * @param {String} tableId
    * @param {Array} rows
    * @param {Array} filteredRows
    * @param {Array} columns
    * @param {Dictionary} keys
    * @param {Dictionary} resourceKeys
    * @param {Function} callback
    * @description Archive selected items from rows.
    **/
    function archiveAction(tableId, rows, filteredRows, columns, keys, resourceKeys, callback) {
        var deferred = $q.defer();
        var selectedRows = $filter("filter")(filteredRows, { "selected": true });
        var events = getTableEvents(tableId);
        if (tableId && keys && selectedRows && _.isFunction(events.archive)) {
            var promiseArray = [];
            _.each(selectedRows, function (item) {
                var id = item["10"] ? item["10"][keys.Item_Id] : "";
                if (id) {
                    var promise = events.archive(keys, item).then(
                        function (response) {
                            if (response.result) {
                                item.selected = false;
                                item["70"].Value = "Archived";
                                item = events.afterSave(item, response.data, columns);
                            }
                            return response;
                        }
                    );
                    promiseArray.push(promise);
                }
                else {
                    var deferred = $q.defer();
                    deferred.resolve({
                        result: true,
                        data: ""
                    });
                    promiseArray.push(deferred.promise);
                }
            });
            $q.all(promiseArray).then(
                function (responses) {
                    if (_.isFunction(callback)) {
                        callback(rows);
                    }
                    deferred.resolve({ result: true, data: responses });
                }
            );
        }
        else {
            if (_.isFunction(callback)) {
                callback(false);
            }
            deferred.resolve({ result: false, data: "" });
        }
        return deferred.promise;
    }

    /**
    * @name getColumnDefaultCellByInput
    * @param {Dictionary} keys
    * @param {String} columnId
    * @param {Array} columns
    * @param {Boolean} parseIdAsString
    * @returns {Object} Default Cells by Column Input Type
    * @description Retrieve Default Cell by Column Input Type.
    * @example TableHelpers.getColumnDefaultCellByInput(keys, columnId, columns, parseIdAsString)
    **/
    function getColumnDefaultCellByInput(keys, columnId, columns, parseIdAsString) {
        var defaultCells = getDefaultCells();
        var customUnknown = {};

        // Find the column associated with the cell by ColumnId via comparator.
        var comparator = {};
        comparator[keys.Column_ModelKey] = parseIdAsString ? columnId.toString() : parseInt(columnId);
        var columnsById = $filter("filter")(columns, comparator, true);
        var column = (_.isArray(columnsById) && columnsById.length ? columnsById[0] : null);

        // If the column has list values, find the Unknown value and set that as the default cell.
        if (column) {
            if (column[keys.Column_List] && column[keys.Column_List].length) {
                var unknown = $filter("filter")(column[keys.Column_List], "Unknown", true);
                if (_.isArray(unknown) && unknown.length) {
                    customUnknown[keys.Item_Name] = unknown[0][keys.Column_List_Value];
                    customUnknown[keys.Item_Id] = unknown[0][keys.Column_List_Id];
                }
            }

            switch (column.Input) {
                case TableInputTypes.Typeahead:
                    return _.cloneDeep(defaultCells.Unknown);
                case TableInputTypes.User:
                    return Object.keys(customUnknown).length ? customUnknown : _.cloneDeep(defaultCells.Unknown);
                case TableInputTypes.Text:
                    return _.cloneDeep(defaultCells.Empty);
                default:
                    return _.cloneDeep(defaultCells.Empty);
            }
        }
    }

    /**
    * @name getColumnDefaultCellByValue
    * @param {Dictionary} keys
    * @param {String} columnId
    * @param {Array} columns
    * @param {Boolean} parseIdAsString
    * @returns {Object} Default Cell by first Column List Value.
    * @description Retrieve Default Cell by first Column List Value.
    * @example TableHelpers.getColumnDefaultCellByValue(keys, columnId, columns, parseIdAsString)
    **/
    function getColumnDefaultCellByValue(keys, columnId, columns, parseIdAsString) {
        // Create generic cell object.
        var cell = {
            "Value": "",
            "ValueId": ""
        };

        // Find the column associated with the cell by ColumnId via comparator.
        var comparator = {};
        comparator[keys.Column_ModelKey] = parseIdAsString ? columnId.toString() : parseInt(columnId);
        var columnsById = $filter("filter")(columns, comparator, true);
        var column = (_.isArray(columnsById) && columnsById.length ? columnsById[0] : null);

        // If the column has list values, find the first value and set cell properties using it.
        if (column && column[keys.Column_List] && column[keys.Column_List].length) {
            var list = column[keys.Column_List] || [];
            var value = (_.isArray(list) && list.length ? list[0] : null);
            if (value) {
                cell.ValueId = value[keys.Column_List_Id];
                cell.Value = value[keys.Column_List_Value];
            }
        }

        return cell;
    }

    /**
     * @name tableAfterSave
     * @param  {Object} item
     * @param  {Object} data
     * @param  {Array} columns
     * @param  {Dictionary} keys
     * @returns {Object}
     * @description Normalize response objects after saving and updated Column Lists if necessary.
     * @example TableHelpers.tableAfterSave(item, data, columns, keys)
     */
    function tableAfterSave(item, data, columns, keys) {
        if (data) {
            // Convert data object to match format of item object.
            for (var key in data) {
                var value = data[key];
                data[key] = {};
                if (Utilities.matchRegex(value, Regex.GUID)) {
                    data[key][keys.Item_Id] = value;
                }
                else {
                    data[key][keys.Item_Name] = value;
                }
            }

            if (_.isArray(columns)) {
                // Find all columns with Lists
                var hasColumnComparator = {
                    Key: keys.Column_List,
                    CheckTruthy: true
                };
                var listColumns = $filter("TableComparator")(columns, [hasColumnComparator]);

                // If Column has List, detect if list needs to be updated, and update it.
                if (_.isArray(listColumns) && listColumns.length) {
                    _.each(listColumns, function (column) {
                        var modelKey = column[keys.Column_ModelKey].toString();
                        // Column Property exists on the data object.
                        if (data[modelKey] && data[modelKey][keys.Item_Id]) {

                            var hasValueComparator = {
                                Key: keys.Column_List_Id,
                                Value: data[modelKey][keys.Item_Id]
                            };
                            var hasValue = $filter("TableComparator")(column[keys.Column_List], [hasValueComparator]);

                            // Update our values wherever possible.
                            if (hasValue.length) {
                                data[modelKey][keys.Item_Name] = hasValue[0][keys.Column_List_Value];
                            }
                            // If item needs to be added to columns, do so.
                            else {
                                if (item[modelKey] &&
                                    (!Utilities.matchRegex(item[modelKey][keys.Item_Id], Regex.GUID)) &&
                                    (Utilities.matchRegex(data[modelKey][keys.Item_Id], Regex.GUID))) {
                                    var newObject = {};
                                    newObject[keys.Column_List_Id] = data[modelKey][keys.Item_Id];
                                    newObject[keys.Column_List_Value] = item[modelKey][keys.Item_Name];
                                    column[keys.Column_List].push(newObject);
                                }
                            }
                        }
                        // Run our Filter counts at then end in case new items were added, to fix the Filter display.
                        if (_.isFunction(column.RunFilter)) {
                            column.RunFilter(column);
                        }
                    });
                }
            }

            // Merge item and data objects.
            item = _.merge(item, data);
            item[keys.Item_Id] = item["10"][keys.Item_Id];
        }

        return item;
    }

    /**
    * @name getDefaultCells
    * @returns {Dictionary} Default Cells Dictionary
    * @description Retrieve Dictionary of Default Cells.
    * @example TableHelpers.getDefaultCells()
    **/
    function getDefaultCells() {
        if (mocks && mocks.DefaultCells) {
            return mocks.DefaultCells;
        }
        return {};
    }

    /**
    * @name getEvents
    * @param {String} tableId
    * @returns {Dictionary} Events Dictionary
    * @description Retrieve Dictionary of Table Events by Table Id
    * @example TableHelpers.getEvents(tableId)
    **/
    function getTableEvents(tableId) {
        if (mocks && mocks.Events) {
            switch (tableId) {
                case ENUMS.InventoryTableIds.Elements:
                    return mocks.Events.Elements;
                case ENUMS.InventoryTableIds.Assets:
                    return mocks.Events.Applications;
                case ENUMS.InventoryTableIds.Processes:
                    return mocks.Events.Processes;
                default:
                    return mocks.Events.Defaults;
            }
        }
        return {};
    }

    function getPage(tableId, page, size, filter, search) {
        return Tables.getPage(tableId, page, size, filter, search);
    }

    function formatPaginationConfig(metadata) {
        return {
            first: metadata.PageNumber === 1,
            last: metadata.PageNumber === metadata.TotalPageCount,
            number: metadata.PageNumber - 1,
            numberOfElements: metadata.ItemCount,
            size: metadata.PageSize,
            totalElements: metadata.TotalItemCount,
            totalPages: metadata.TotalPageCount,
        };
    }

    var service = {
        getEvents: getTableEvents
    };

    return service;

}

TableHelpers.$inject = ["$q", "$filter", "$state", "$rootScope", "Tables", "Users", "OrgGroups", "$uibModal", "Permissions", "TableHelper", "ENUMS"];
