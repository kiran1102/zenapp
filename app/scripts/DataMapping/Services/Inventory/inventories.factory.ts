// Interface
import { IInventorySchemaV2 } from "interfaces/inventory.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";

// Constants
import { InventorySchemaIds } from "constants/inventory-config.constant";

class OneInventories {

    static $inject: string[] = ["Inventory", "Permissions"];
    private inventories: IInventorySchemaV2[];
    private inventorySchemaOrder = [InventorySchemaIds.Assets, InventorySchemaIds.Processes, InventorySchemaIds.Entities];

    constructor(
        readonly Inventory: any,
        private Permissions: any,
    ) {}

    public getAPI(): Promise<IInventorySchemaV2[]> {
        return this.Inventory.getSchemasV2()
            .then((res: IProtocolResponse<IInventorySchemaV2[]>): IInventorySchemaV2[] => {
                if (res.data) {
                    this.inventories = this.getAllowedInventories(this.formatInventorySchemaList(res.data));
                    return this.inventories;
                }
                return [];
            });
    }

    public getList(): IInventorySchemaV2[] {
        return this.inventories || [];
    }

    private getAllowedInventories(data: IInventorySchemaV2[]): IInventorySchemaV2[] {
        return data.filter((inventory: IInventorySchemaV2): boolean => {
            if (inventory.id === InventorySchemaIds.Elements) {
                return this.Permissions.canShow("DataMappingAttributesDataElements");
            } else if (inventory.id === InventorySchemaIds.Assets) {
                return this.Permissions.canShow("DataMappingInventoryAssets");
            } else if (inventory.id === InventorySchemaIds.Processes) {
                return this.Permissions.canShow("DataMappingInventoryProcessingActivity");
            } else if (inventory.id === InventorySchemaIds.Entities) {
                return this.Permissions.canShow("DataMappingEntitiesInventory");
            }
            return false;
        });
    }

    private formatInventorySchemaList(data: IInventorySchemaV2[]): IInventorySchemaV2[] {
        const orderedInventoryList: IInventorySchemaV2[] = this.inventorySchemaOrder.map((inventorySchemaName: string): IInventorySchemaV2 => {
            const matchedInventorySchemaIndex: number = data.findIndex((item) => item.id === inventorySchemaName);
            const matchedInventorySchema: IInventorySchemaV2 = data[matchedInventorySchemaIndex] || null;
            if (matchedInventorySchema) data.splice(matchedInventorySchemaIndex, 1);
            return matchedInventorySchema;
        }).filter((item) => item);
        return orderedInventoryList.concat(data);
    }
}

export default OneInventories;
