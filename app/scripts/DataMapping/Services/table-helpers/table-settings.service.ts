import * as _ from "lodash";
declare var angular: any;
import Utilities from "Utilities";

export default class TableSettings {
    static $inject: string[] = ["ENUMS"];

    private Inventories: any;
    private Settings: any;
    private InventorySettings: any;

    constructor(ENUMS: any) {
        this.Inventories = ENUMS.InventoryTableIds;
        this.Settings = this.getDefaultSettings();
        this.InventorySettings = this.getInventorySettings(this.Settings);
    }

    public getSettings(tableId: string): any {
        const setting: any = this.InventorySettings;
        switch (tableId) {
            case this.Inventories.Elements:
                setting["ResourceKey"] = "InventoryElements";
                return setting;
            case this.Inventories.Applications:
                setting["ResourceKey"] = "InventoryApplications";
                return setting;
            case this.Inventories.Processes:
                setting["ResourceKey"] = "InventoryProcesses";
                return setting;
            default:
                return this.Settings;
        }
    }

    public setSettings(table: any, idKey: string): any {
        if (table && idKey) {
            table = _.merge(table, this.getSettings(table[idKey].toString()));
        }
        return table;
    }

    public applySettings = (tables: any[], idKey: string): any[] => {
        if (_.isArray(tables) && idKey) {
            _.each(tables, (table: any): void => {
                table = this.setSettings(table, idKey);
            });
        }
        return tables;
    }

    private getDefaultSettings(): any {
        const defaultSettings: any = {};
        return defaultSettings;
    }

    private getInventorySettings(defaultSettings: any): any {
        const inventorySettings: any = defaultSettings;
        inventorySettings["SaveOnLeave"] = true;
        return inventorySettings;
    }

}
