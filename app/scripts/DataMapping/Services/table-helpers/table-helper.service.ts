declare var angular: any;

export default class TableHelper {

    static $inject: string[] = ["TableKeys", "TableResources", "TableSettings", "TableColumnProperties"];

    constructor(private readonly TableKeys: any, private readonly TableResources: any, private readonly TableSettings: any, private readonly TableColumnProperties: any) { }

    public getKeys = (tableId: string): any => {
        return this.TableKeys.getKeys(tableId);
    }

    public getResourceKeys = (tableId: string): any => {
        return this.TableResources.getResources(tableId);
    }

    public getSettings = (tableId: string): any => {
        return this.TableSettings.getSettings(tableId);
    }

    public applySettings = (tables: any[], idKey: string): any[] => {
        return this.TableSettings.applySettings(tables, idKey);
    }

    public getColumnProperties = (tableId: string): any => {
        return this.TableColumnProperties.getColumnProperties(tableId);
    }

    public applyColumnProperties = (tableId: string, column: any, keys: any): any => {
        return this.TableColumnProperties.applyColumnProperties(tableId, column, keys);
    }

}
