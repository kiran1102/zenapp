import * as _ from "lodash";
declare var angular: any;

export default class TableKeys {
    static $inject: string[] = ["ENUMS"];

    private Inventories: any;
    private Keys: any;
    private InventoryKeys: any;

    constructor(ENUMS: any) {
        this.Inventories = ENUMS.InventoryTableIds;
        this.Keys = this.getDefaultKeys();
        this.InventoryKeys = this.getInventoryKeys(this.Keys);
    }

    public getKeys(tableId: string): any {
        switch (tableId) {
            case this.Inventories.Elements:
                return this.InventoryKeys;
            case this.Inventories.Applications:
                return this.InventoryKeys;
            case this.Inventories.Processes:
                return this.InventoryKeys;
            default:
                return this.Keys;
        }
    }

    private getDefaultKeys(): any {
        return {
            Column_Id: "Id",
            Column_List: "List",
            Column_List_Email: "Email",
            Column_List_Id: "Id",
            Column_List_OrgGroupId: "OrgGroupId",
            Column_List_Value: "Value",
            Column_List_Version: "Version",
            Column_ModelKey: "Order",
            Column_Name: "Name",
            Column_Order: "Order",
            Column_Required: "Required",
            Column_TreeMap: "TreeMap",
            Columns: "Columns",
            Id: "Id",
            Item_Column: "Name",
            Item_Id: "Id",
            Item_Name: "Value",
            Items: "Items",
            Name: "Name",
            Rows: "Rows",
        };
    }

    private getInventoryKeys(defaultKeys: any): any {
        const inventoryKeys: any = {
            Column_Id: "ColumnId",
            Column_List: "ColumnValues",
            Column_List_Email: "ColumnEmail",
            Column_List_Id: "ColumnValueId",
            Column_List_OrgGroupId: "ColumnOrgGroupId",
            Column_List_Value: "ColumnValue",
            Column_List_Version: "ColumnVersion",
            Column_Name: "ColumnName",
            Columns: "InventoryColumns",
            Id: "InventoryTypeId",
            Item_Column: "ColumnName",
            Item_Id: "ValueId",
            Item_Name: "Value",
            Items: "Records",
            Name: "InventoryTypeName",
            Rows: "Records",
            // Following are inherited from default.
            // Leaving them here in case they need to be changed.
            // Column_ModelKey: "Order",
            // Column_Order: "Order",
            // Column_Required: "Required",
            // Column_TreeMap: "TreeMap",
        };
        return _.merge(defaultKeys, inventoryKeys);
    }

}
