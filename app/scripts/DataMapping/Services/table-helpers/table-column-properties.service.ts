import * as _ from "lodash";
import Utilities from "Utilities";
import TreeMap from "TreeMap";
import { getActiveUsers } from "oneRedux/reducers/user.reducer";
import { IStore } from "interfaces/redux.interface";
import { IUser } from "interfaces/user.interface";

export default class TableColumnProperties {
    static $inject: string[] = ["ENUMS", "$q", "$filter", "OrgGroups", "store"];

    private Inventories: any;
    private EmptyGuid: string;
    private TableWidths: any;
    private TableInputTypes: any;
    private OrganizationService: any;
    private filter: any;
    private pick: any;
    private orderBy: any;

    constructor(ENUMS: any, private readonly $q: ng.IQService, $filter: ng.IFilterService, OrgGroups: any, private readonly store: IStore) {
        this.Inventories = ENUMS.InventoryTableIds;
        this.EmptyGuid = ENUMS.EmptyGuid;
        this.TableWidths = ENUMS.TableWidths;
        this.TableInputTypes = ENUMS.TableInputTypes;
        this.OrganizationService = OrgGroups;
        this.filter = $filter("filter");
        this.pick = $filter("pick");
        this.orderBy = $filter("orderBy");
    }

    public getColumnProperties(tableId: string): any {
        const settings = this.defaultColumnSettings();
        switch (tableId) {
            case this.Inventories.Elements:
                return _.merge(settings, this.elementsColumnSettings());
            case this.Inventories.Applications:
                return _.merge(settings, this.applicationsColumnsSettings());
            case this.Inventories.Processes:
                return _.merge(settings, this.processesColumnsSettings());
            default:
                return settings;
        }
    }

    public applyColumnProperties = (tableId: string, column: any, keys: any): any => {
        const deferred: any = this.$q.defer();
        // Apply Column Settings (Input, Width, Hidden, etc.)
        column = this.applySingleColumnSettings(tableId, column, keys);
        this.applyColumnInputBasedProperties(tableId, column, keys).then(
            (tableColumn: any) => {
                // Based on Column Settings and Column Input Properties, set the Column List Properties.
                tableColumn = this.applyColumnListProperties(tableId, tableColumn, keys);
                // Return the Column wih all settings and properties.
                deferred.resolve(tableColumn);
            },
        );
        return deferred.promise;
    }

    private defaultColumnSettings = (): any => {
        return {
            Default: {
                Width: this.TableWidths.Medium,
            },
        };
    }

    private elementsColumnSettings = (): any => {
        return {
            Default: {
                Width: this.TableWidths.Normal,
            },
            10: {
                Width: this.TableWidths.Large,
                Input: this.TableInputTypes.Text,
            },
            20: {
                Width: this.TableWidths.Large,
                Input: this.TableInputTypes.Typeahead,
            },
            30: {
                Input: this.TableInputTypes.Typeahead,
            },
            40: {
                Hide: true,
            },
            50: {
                Hide: true,
            },
        };
    }

    private applicationsColumnsSettings = (): any => {
        return {
            Default: {
                Width: this.TableWidths.Medium,
            },
            10: {
                Input: this.TableInputTypes.Text,
            },
            20: {
                Hide: true,
                Input: this.TableInputTypes.Text,
            },
            30: {
                Hide: true,
                Input: this.TableInputTypes.Text,
            },
            40: {
                Input: this.TableInputTypes.User,
                FilterComparator: this.genericFilterComparator,
                FilterComparatorId: "120",
            },
            50: {
                Input: this.TableInputTypes.Typeahead,
            },
            60: {
                Input: this.TableInputTypes.Typeahead,
            },
            70: {
                Hide: true,
            },
            80: {
                Hide: true,
            },
            90: {
                Hide: true,
            },
            100: {
                Input: this.TableInputTypes.Typeahead,
                DisableComparator: this.shouldDisableComparator("External"),
                DisableComparatorId: "110",
            },
            110: {
                Input: this.TableInputTypes.Typeahead,
                InvalidateSibling: "100",
            },
            120: {
                Input: this.TableInputTypes.Typeahead,
                InvalidateSibling: "40",
                OrderBy: false,
            },
        };
    }

    private processesColumnsSettings = (): any => {
        return {
            Default: {
                Width: this.TableWidths.Normal,
            },
            10: {
                Width: this.TableWidths.Normal,
                Input: this.TableInputTypes.Text,
            },
            20: {
                Width: this.TableWidths.Large,
                Input: this.TableInputTypes.Text,
            },
            30: {
                Input: this.TableInputTypes.Typeahead,
                InvalidateSibling: "40",
                OrderBy: false,
            },
            40: {
                Width: this.TableWidths.Large,
                Input: this.TableInputTypes.User,
                FilterComparator: this.genericFilterComparator,
                FilterComparatorId: "30",
            },
            50: {
                Hide: true,
            },
            60: {
                Hide: true,
            },
            70: {
                Width: this.TableWidths.Medium,
                Input: this.TableInputTypes.ProjectState,
                Prefilter: [
                    "Archived",
                ],
                OrderBy: false,
            },
        };
    }

    private applySingleColumnSettings = (tableId: string, column: any, keys: any): any => {
        const columnId: string = column[keys.Column_ModelKey].toString();
        const tableColumnSettings: any = this.getColumnProperties(tableId) || {};
        const defaultProps: any = { Default: tableColumnSettings.Default };
        const settings: any = Object.keys(tableColumnSettings).length ? (tableColumnSettings[columnId] || {}) : {};
        column = _.merge(column, settings, defaultProps);
        return column;
    }

    private applyColumnInputBasedProperties = (tableId: string, column: any, keys: any): any => {
        const deferred: any = this.$q.defer();
        if (column.Input) {
            switch (column.Input) {
                case this.TableInputTypes.User:
                    const activeUsers: IUser[] = getActiveUsers(this.store.getState());
                    const list: any[] = [];
                    const unknown: any = {};
                    unknown[keys.Column_List_Id] = this.EmptyGuid;
                    unknown[keys.Column_List_Value] = "Unknown";
                    unknown[keys.Column_List_Email] = "";
                    unknown[keys.Column_List_OrgGroupId] = "";

                    list.push(unknown);
                    _.each(activeUsers, (user: IUser): void => {
                        const item: any = {};
                        item[keys.Column_List_Id] = user.Id;
                        item[keys.Column_List_Value] = user.FullName || user.Email;
                        item[keys.Column_List_Email] = user.Email;
                        item[keys.Column_List_OrgGroupId] = user.OrgGroupId;
                        list.push(item);
                    });
                    column[keys.Column_List] = list;

                    const orgPromise: any = this.OrganizationService.rootTree().then(
                        (res) => {
                            if (res.result) {
                                const rootOrganizationTree = res.data;
                                column[keys.Column_TreeMap] = new TreeMap("Id", "ParentId", "Children", rootOrganizationTree[0]);
                            }
                        },
                    );
                    const columnListPromises: any[] = [orgPromise];
                    this.$q.all(columnListPromises).then((responses) => {
                        deferred.resolve(column);
                    });
                    break;
                default:
                    deferred.resolve(column);
            }
        } else {
            deferred.resolve(column);
        }
        return deferred.promise;
    }

    private applyColumnListProperties = (tableId: string, column: any, keys: any): any => {
        if (_.isArray(column[keys.Column_List])) {
            column.compare = this.getGenericCompareFunction();
            // If settings is not defined, or is defined and truthy, then set OrderBy.
            if (_.isUndefined(column.OrderBy) || column.OrderBy) {
                if (_.isUndefined(column.OrderBy)) {
                    // Default Ordering set if the setting is undefined.
                    column.OrderBy = keys.Column_List_Value;
                }
                column[keys.Column_List] = this.orderBy(column[keys.Column_List], column.OrderBy);
            }
            const newColumnList = [];
            let unknownItem = null;
            let notApplicableItem = null;

            _.each(column[keys.Column_List], (item: any, index: any): void => {
                item.Filtered = !!(_.isArray(column.prefilter) && (column.prefilter.indexOf(item[keys.Column_List_Value]) > -1));

                switch (item[keys.Column_List_Value]) {
                    case "Unknown":
                        unknownItem = item;
                        break;
                    case "Not Applicable":
                        item.HideInput = true;
                        notApplicableItem = item;
                        break;
                    default:
                        newColumnList.push(item);
                }
            });

            if (unknownItem) newColumnList.unshift(unknownItem);
            if (notApplicableItem) newColumnList.unshift(notApplicableItem);

            column[keys.Column_List] = newColumnList;
        }
        return column;
    }

    private getGenericCompareFunction = (): any => {
        return (columnItem: any, rowItem: any, column: any, props: any): boolean => {
            const rowCell: any = rowItem[column[props.Column_ModelKey]];
            if (columnItem[props.Column_List_Id] && columnItem[props.Column_List_Id] !== this.EmptyGuid) {
                return ((columnItem && rowCell) ? (rowCell[props.Item_Id] ? (rowCell[props.Item_Id] === columnItem[props.Column_List_Id]) : false) : false);
            } else {
                if (columnItem[props.Column_List_Value] !== "- - - -" || columnItem[props.Column_List_Value] !== "Unknown") {
                    return ((columnItem && rowCell) ? (rowCell[props.Item_Name] ? (rowCell[props.Item_Name] === columnItem[props.Column_List_Value]) : false) : false);
                }
                return (rowCell && rowCell[props.Item_Name] ? false : true);
            }
        };
    }

    private genericFilterComparator = (items: any[], data: any, comparatorId: string): any[] => {
        const itemOrg: string = data.item[comparatorId].ValueId;
        const treeMap: any = data.column.TreeMap || null;
        const itemsInTree: any[] = [];
        if (_.isArray(items) && itemOrg && treeMap) {
            _.each(items, (item: any): void => {
                if ((item.ColumnValue === "Unknown") || treeMap.IsInParentTree(item.ColumnOrgGroupId, itemOrg)) {
                    itemsInTree.push(item);
                }
            });
            return itemsInTree;
        }
        return items;
    }

    private shouldDisableComparator = (comparatorValue: string): any => {

        return (comparatorId: string, data: any, columns: any[]): boolean => {
            const columnFinder = (col: any): boolean =>
                col.Order === Number(comparatorId);

            const columnItemFinder = (item: any): boolean =>
                item.ColumnValue === comparatorValue;

            let isDisabled = false;
            const compareItem: any = data.item[comparatorId];
            const compareColumn: any = (this.pick(columns, columnFinder)[0] || null);
            if (compareItem && compareColumn && compareColumn.ColumnValues) {
                const enableOnItem: any = (this.pick(compareColumn.ColumnValues, columnItemFinder)[0] || null);
                if (compareItem && enableOnItem && (compareItem.ValueId !== enableOnItem.ColumnValueId)) {
                    isDisabled = true;
                }
            }
            return isDisabled;
        };
    }

}
