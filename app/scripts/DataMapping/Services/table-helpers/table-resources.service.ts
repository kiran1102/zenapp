import * as _ from "lodash";
declare var angular: any;

export default class TableResources {
    static $inject: string[] = ["ENUMS"];

    private Inventories: any;
    private Resources: any;
    private InventoryResources: any;

    constructor(ENUMS: any) {
        this.Inventories = ENUMS.InventoryTableIds;
        this.Resources = this.getDefaultResources();
        this.InventoryResources = this.getInventoryResources(this.Resources);
    }

    public getResources(tableId: string): any {
        switch (tableId) {
            case this.Inventories.Elements:
                return this.InventoryResources;
            case this.Inventories.Applications:
                return this.InventoryResources;
            case this.Inventories.Processes:
                return this.InventoryResources;
            default:
                return this.Resources;
        }
    }

    private getDefaultResources(): any {
        return {
            Approve: "InventoryTablesApprove",
            Archive: "InventoryTablesArchive",
            Delete: "InventoryTablesDelete",
            Editable: "InventoryTablesEditable",
            Export: "InventoryTablesExport",
            Filter: "InventoryTablesFilter",
            Launch: "InventoryTablesLaunch",
            Options: "InventoryTablesOptions",
            Save: "InventoryTablesSave",
            Search: "InventoryTablesSearch",
        };
    }

    private getInventoryResources(defaultResources: any): any {
        const inventoryResources: any = {
            // Following are inherited from default.
            // Leaving them here in case they need to be changed.
            // "Approve": "InventoryTablesApprove",
            // "Archive": "InventoryTablesArchive",
            // "Delete": "InventoryTablesDelete",
            // "Editable": "InventoryTablesEditable",
            // "Export": "InventoryTablesExport",
            // "Filter": "InventoryTablesFilter",
            // "Launch": "InventoryTablesLaunch",
            // "Options": "InventoryTablesExport",
            // "Save": "InventoryTablesSave",
            // "Search": "InventoryTablesSearch",
        };

        return _.merge(defaultResources, inventoryResources);
    }

}
