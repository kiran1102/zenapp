
export default class AssessmentModalService {

    public getReassignmentTemplate(): string {
        return `
                <reassignment-modal
                    scope="controller"
                    >
                </reassignment-modal>
            `;
    }

    public getSendBackTemplate(): string {
        return `
                <send-back-modal
                    scope="controller"
                    >
                </send-back-modal>
            `;
    }
}
