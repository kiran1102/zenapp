import * as _ from "lodash";
import Utilities from "Utilities";

// Interface
import { IAssessmentParams } from "./dm-assessments.service";
import { IStore } from "interfaces/redux.interface";

// Redux
import { getCurrentUser } from "oneRedux/reducers/user.reducer";

class OneAssessmentListFactory {

    static $inject: string[] = ["$q", "DMAssessments", "OneAssessmentTableConfig", "store"];

    public assessmentList: any = {};
    private Protocol: any;

    constructor(
        private $q: ng.IQService,
        private DMAssessments: any,
        private OneAssessmentTableConfig: any,
        private store: IStore,
        ) {

    }

    public init(params: IAssessmentParams): ng.IPromise<any> {
        this.clearAssessmentList();
        return this.getAssessments(params).then((response: any): any => {
            if (!response.result) return { result: false, data: null };
            this.getTableConfig();
            return { ...response, data: _.assign(this.assessmentList, { ...response.data, currentUser: getCurrentUser(this.store.getState()) }) };
        });
    }

    public getAssessments(params?: IAssessmentParams): ng.IPromise<any> {
        return this.DMAssessments.getAssessments(_.cloneDeep(params));
    }

    public deleteAssessment(assessmentId: string): ng.IPromise<any> {
        return this.DMAssessments.deleteAssessment(assessmentId);
    }

    private getTableConfig(): void {
        this.assessmentList.tableConfig = this.OneAssessmentTableConfig.getDMAssessmentTable();
    }

    private clearAssessmentList(): void {
        this.assessmentList = {};
    }

}

export default OneAssessmentListFactory;
