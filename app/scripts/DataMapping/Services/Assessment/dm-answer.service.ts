import * as _ from "lodash";
import { ProtocolService } from "modules/core/services/protocol.service";
import { IProtocolPacket, IProtocolConfig, IProtocolMessages} from "interfaces/protocol.interface";
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";

interface IAsset {
    assetId: any;
}

interface IAttribute {
    attributeId: any;
}

class DMAnswer {

    static $inject: string[] = ["$rootScope", "OneProtocol", "ENUMS", "$q", "OneAssessment"];

    private QuestionTypes: any;
    private DisplayTypes: any;
    private AttributeTypes: any;
    private OptionTypes: any;
    private translate: any;
    private answerPromiseQueue: any[] = [];
    private resolvingAnswerQueue = false;
    private outstandingResponses = 0;
    private responseQueue: any[] = [];

    constructor(
        $rootScope: IExtendedRootScopeService,
        readonly OneProtocol: ProtocolService,
        ENUMS: any,
        readonly $q: ng.IQService,
        readonly OneAssessment: any,
    ) {
        this.QuestionTypes = ENUMS.DMQuestionTypes;
        this.AttributeTypes = ENUMS.QuestionAttributeTypes;
        this.DisplayTypes = ENUMS.DMQuestionDisplayTypes;
        this.OptionTypes = ENUMS.OptionTypes;
        this.translate = $rootScope.t;
    }

    public updateAnswer(question: any, params?: any): ng.IPromise<IProtocolPacket> {
        if (question.responseType === this.OptionTypes.Assets || question.responseType === this.OptionTypes.Processes) {
            question.response = this.updateAssetResponses(params);
        } else {
            switch (question.questionType) {
                case this.QuestionTypes.AppInfo:
                    question.response = this.updateAppInfoResponses(question.response, params);
                    break;
                case this.QuestionTypes.MultiSelect:
                    question.response = this.updateMultiSelectResponses(params);
                    break;
                case this.QuestionTypes.MultiSelectLoop:
                    question.response = this.updateMultiSelectLoopResponses(question, params);
                    break;
                case this.QuestionTypes.Welcome:
                case this.QuestionTypes.Info:
                    question.response = "";
                    break;
                case this.QuestionTypes.SingleSelect:
                    question.response = this.updateSingleSelectResponses(question, params);
                    break;
                case this.QuestionTypes.SingleSelectLoop:
                    question.response = this.updateSingleSelectLoopResponses(question, params);
                    break;
                case this.QuestionTypes.TypeaheadMultiselectLoop:
                    question.response = this.updateTypeaheadMultiSelectLoopResponses(question, params);
                    break;
                case this.QuestionTypes.Text:
                    question.response = this.updateTextResponses(params);
                    break;
                case this.QuestionTypes.Asset:
                case this.QuestionTypes.AssetSingleSelect:
                case this.QuestionTypes.Process:
                case this.QuestionTypes.ProcessSingleSelect:
                    question.response = this.updateAssetResponses(params);
                    break;
                default:
            }
        }
        return this.sendResponse(question, question.response);
    }

    public resolveAnswerQueue(): ng.IPromise<IProtocolPacket> | void {
        if (this.answerPromiseQueue.length) {
            this.resolvingAnswerQueue = true;
            this.answerPromiseQueue[0]().then((response: IProtocolPacket): IProtocolPacket => {
                this.answerPromiseQueue.splice(0, 1);
                this.resolveAnswerQueue();
                return response;
            });
        } else {
            this.resolvingAnswerQueue = false;
        }
    }

    public sendResponse(question: any, response: any): ng.IPromise<IProtocolPacket> {
        const queryParams: any = {
            assessmentId: question.assessmentId,
            sectionId: question.sectionId,
            questionId: question.id,
        };
        const responseString: string = _.isString(response) ? response : JSON.stringify(response);
        const queryString = "/Assessment/QuestionResponse";
        const config: IProtocolConfig = this.OneProtocol.config("PUT", queryString, queryParams, { response: responseString });
        const messages: IProtocolMessages = { Error: { object: this.translate("question"), action: this.translate("answer") } };
        const deferred: ng.IDeferred<IProtocolPacket> = this.$q.defer();
        const answerPromiseFactory = (): ng.IPromise<void> => {
            return this.OneProtocol.http(config, messages).then((res: IProtocolPacket): void => {
                deferred.resolve(res);
            });
        };
        this.answerPromiseQueue.push(answerPromiseFactory);
        if (!this.resolvingAnswerQueue) {
            this.resolveAnswerQueue();
        }
        return deferred.promise;
    }

    public sendEmptyResponse(question: any): ng.IPromise<IProtocolPacket> {
        return this.sendResponse(question, "");
    }

    public answerQuestion = (question: any, params?: any): ng.IPromise<IProtocolPacket> => {
        this.outstandingResponses++;
        return this.updateAnswer(question, params).then( (response: any): any => {
            this.outstandingResponses--;
            if (!response.result) return { result: false, data: [] };
            const activeSection: any = this.OneAssessment.getActiveSection();
            const status: number = this.OneAssessment.getStatus();
            const questionList: any[] = this.OneAssessment.getActiveQuestions();
            const initialSectionId: string = this.OneAssessment.getActiveSection().id;
            const identifier: string = question.loopQuestionId ? "loopQuestionId" : "id";
            const answeredQuestionIndex: number = _.findIndex(questionList, [identifier, question[identifier]]);
            this.responseQueue.push({
                question,
                questionId: question[identifier],
                identifier,
                questionIndex: answeredQuestionIndex,
                nextQuestions: response.data.activeSection.nextQuestions,
            });
            if (activeSection.id === initialSectionId && !this.outstandingResponses) {
                let sliceIndex = -1;
                let aggregateResponses: any[] = [];
                _.forEach(this.responseQueue, (res: any): void | undefined => {
                    if (sliceIndex === -1 || !aggregateResponses.length || res.questionIndex < sliceIndex) {
                        sliceIndex = res.questionIndex;
                        aggregateResponses = [res.question, ...res.nextQuestions];
                        return;
                    }
                    const overlapIndex: number = _.findIndex(aggregateResponses, (aggRes: any): boolean => aggRes[res.identifier] === res.questionId);
                    if (overlapIndex > -1) {
                        const originalQuestions: any[] = aggregateResponses.slice(0, overlapIndex);
                        aggregateResponses = [...originalQuestions, res.question, ...res.nextQuestions];
                    }
                });
                const previousQuestions: any[] = questionList.slice(0, sliceIndex);
                if (activeSection.id === response.data.activeSection.id && aggregateResponses) {
                    this.OneAssessment.setActiveQuestions([...previousQuestions, ...aggregateResponses]);
                }
            }
            if (!this.outstandingResponses) {
                this.responseQueue = [];
            }
            response.pendingResponses = this.outstandingResponses;
            return response;
        });
    }

    private updateAppInfoResponses(response: any, params: any): any {
        const valueKeys = {
            selection: "selection",
            value: "value",
        };
        let valueKey = "";
        switch (params.attrType) {
            case this.AttributeTypes.Select:
            case this.AttributeTypes.MultiSelect:
                valueKey = valueKeys.selection;
                break;
            case this.AttributeTypes.Typeahead:
                valueKey = valueKeys.selection;
                break;
            case this.AttributeTypes.Text:
            case this.AttributeTypes.Boolean:
                valueKey = valueKeys.value;
                break;
            default:
        }
        const newResponse = {
           assetResponses: [
              {
                 assetId: params.appId,
                 attributeResponses: [
                    {
                       attributeId: params.attrId,
                       value: params[valueKey],
                   },
                ],
            },
          ],
        };
        if (!response) return newResponse;

        const currentAssets: IAsset[] = response.assetResponses;
        if (!(_.isArray(currentAssets) && currentAssets.length)) {
            response.assetResponses = newResponse.assetResponses;
            return response;
        }
        let matchingApp = null;
        let matchingAppIndex = 0;
        const currentAssetsLength: number = currentAssets.length;
        for (let i = 0; i < currentAssetsLength; i++) {
            if (currentAssets[i].assetId === params.appId) {
                matchingApp = currentAssets[i];
                matchingAppIndex = i;
                break;
            }
        }
        if (!matchingApp) {
            response.assetResponses.push(newResponse.assetResponses[0]);
            return response;
        }

        const currentAttrs: IAttribute[] = matchingApp.attributeResponses;
        if (!(_.isArray(currentAttrs) && currentAttrs.length)) {
            response.assetResponses[matchingAppIndex].attributeResponses = [newResponse.assetResponses[0].attributeResponses];
            return response;
        }
        let matchingAttr = null;
        let matchingAttrIndex = 0;
        const currentAttrsLength: number = currentAttrs.length;
        for (let i = 0; i < currentAttrsLength; i++) {
            if (currentAttrs[i].attributeId === params.attrId) {
                matchingAttr = currentAttrs[i];
                matchingAttrIndex = i;
                break;
            }
        }
        if (!matchingAttr) {
            response.assetResponses[matchingAppIndex].attributeResponses.push(newResponse.assetResponses[0].attributeResponses[0]);
            return response;
        }
        if (!params[valueKey]) {
            response.assetResponses[matchingAppIndex].attributeResponses.splice(matchingAttrIndex, 1);
            return response;
        }
        response.assetResponses[matchingAppIndex].attributeResponses.splice(matchingAttrIndex, 1, newResponse.assetResponses[0].attributeResponses[0]);
        return response;
    }

    private updateSingleSelectResponses(question: any, params: any): any {
        if (question.displayType === this.DisplayTypes.Typeahead) {
            if (params.notSure) return { notSure: true };
            if (params.key) return { singleSelectResponse: params.key };
            if (params.value) return { otherResponses: [params.value] };
        }
        if (question.displayType === this.DisplayTypes.Buttons) {
            const newResponse: any = {
                singleSelectResponse: params.response,
                otherResponses: params.otherResponse ? [params.otherResponse] : [],
                notSure: params.notSure,
            };
            return newResponse;
        }
    }

    private updateMultiSelectResponses(params: any): any {
        const newResponse: any = {
            multiSelectResponse: params.responses,
            otherResponses: params.otherResponses,
            notSure: params.notSure,
        };
        return newResponse;
    }

    private updateMultiSelectLoopResponses(question: any, params: any): any {
        const newResponse: any = {
           loopMultiSelectResponse: {
               loopQuestionId: question.loopQuestionId,
               selectedOptions: params.responses,
               otherResponse: params.otherResponses,
           },
        };
        return newResponse;
    }

    private updateSingleSelectLoopResponses(question: any, params: any): any {
        const newResponse: any = {
           loopSingleSelectResponse: {
               loopQuestionId: question.loopQuestionId,
               selectedOption: params.response,
               otherResponse: params.otherResponse,
               notSure: params.notSure,
           },
        };
        return newResponse;
    }

    private updateTypeaheadMultiSelectLoopResponses(question: any, params: any): any {
        const newResponse: any = {
           loopMultiSelectTypeAheadResponse: {
               loopQuestionId: question.loopQuestionId,
               selectedOptions: params.responses,
           },
        };
        return newResponse;
    }

    private updateTextResponses(params: any): any {
        const newResponse: any = {
            textResponse: params.response,
            notSure: params.notSure,
        };
        return newResponse;
    }

    private updateAssetResponses(params: any): any {
        return params.notSure ? { notSure: true } : params.assetResponses;
    }

}

export default DMAnswer;
