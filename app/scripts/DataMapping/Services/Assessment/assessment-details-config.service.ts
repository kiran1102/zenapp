import { Permissions } from "modules/shared/services/helper/permissions.service";
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import { IPermissions } from "modules/shared/interfaces/permissions.interface";

export default class AssessmentDetailsConfig {
    static $inject: string[] = ["$rootScope", "ENUMS", "Permissions"];

    private inputTypes: any;
    private contexts: any;
    private translate: any;
    private permissions: IPermissions;

    constructor(
        $rootScope: IExtendedRootScopeService,
        ENUMS: any,
        permissions: Permissions,
    ) {
        this.inputTypes = ENUMS.InputTypes;
        this.contexts = ENUMS.ContextTypes;
        this.translate = $rootScope.t;
        this.permissions = {
            DataMappingAssessmentsEditOrg: permissions.canShow("DataMappingAssessmentsEditOrg"),
            DataMappingAssessmentsEditCreator: permissions.canShow("DataMappingAssessmentsEditCreator"),
            DataMappingAssessmentsEditRespondent: permissions.canShow("DataMappingAssessmentsEditRespondent"),
            DataMappingAssessmentsEditApprover: permissions.canShow("DataMappingAssessmentsEditApprover"),
            DataMappingAssessmentsEditDeadline: permissions.canShow("DataMappingAssessmentsEditDeadline"),
        };
    }

    public getConfig(context: number): any[] {
        if (context === this.contexts.DMAssessment) {
            return [
                {
                    label: this.translate("Organization"),
                    key: "orgGroup",
                    labelKey: "name",
                    inputType: this.inputTypes.Text,
                    isEditable: this.permissions.DataMappingAssessmentsEditOrg,
                },
                {
                    label: this.translate("Creator"),
                    key: "createdBy",
                    labelKey: "FullName",
                    inputType: this.inputTypes.Text,
                    isEditable: this.permissions.DataMappingAssessmentsEditCreator,
                },
                {
                    label: this.translate("Respondent"),
                    key: "respondent",
                    labelKey: "FullName",
                    inputType: this.inputTypes.Modal,
                    isEditable: this.permissions.DataMappingAssessmentsEditRespondent,
                    optionsKey: "team",
                    optionLabelKey: "FullName",
                    modalEvent: "LAUNCH_REASSIGN_RESPONDENT",
                },
                {
                    label: this.translate("Approver"),
                    key: "approver",
                    labelKey: "FullName",
                    inputType: this.inputTypes.Modal,
                    isEditable: this.permissions.DataMappingAssessmentsEditApprover,
                    optionsKey: "team",
                    optionLabelKey: "FullName",
                    modalEvent: "LAUNCH_ASSIGN_APPROVER",
                },
                {
                    label: this.translate("Deadline"),
                    key: "deadline",
                    labelKey: "",
                    inputType: this.inputTypes.Date,
                    isEditable: this.permissions.DataMappingAssessmentsEditDeadline,
                    minDate: new Date(),
                },
            ];
        }
    }

}
