import Utilities from "Utilities";
import * as _ from "lodash";

import { ProtocolService } from "modules/core/services/protocol.service";
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";

export default class DMAssessmentNavigation {

    static $inject: string[] = ["$rootScope", "OneProtocol", "ENUMS"];

    private Protocol: any;
    private HTTP: any;
    private sectionStates: any;
    private translate: any;

    constructor(
        $rootScope: IExtendedRootScopeService,
        OneProtocol: ProtocolService,
        ENUMS: any,
        DMAnswer: any,
    ) {
        this.Protocol = OneProtocol;
        this.sectionStates = ENUMS.SectionStates;
        this.translate = $rootScope.t;
    }

    public setSectionAsActive(sections: any, activeSectionId: string): void {
        _.forEach(sections, (section: any, index: number) => {
            if (section.id === activeSectionId) {
                section.isActive = true;
                section.statusId = (section.statusId === this.sectionStates.NotStarted) ? this.sectionStates.InProgress : section.statusId;
            } else {
                section.isActive = false;
            }
        });
        return sections;
    }

    public updateActiveSection(sections: any, activeSection: any): void {
        _.forEach(sections, (section: any, index: number) => {
            if (section.id === activeSection.id) {
                activeSection.isActive = true;
                activeSection.statusId = (activeSection.statusId === this.sectionStates.NotStarted) ? this.sectionStates.InProgress : activeSection.statusId;
                sections[index] = activeSection;
            } else {
                section.isActive = false;
            }
        });
        return sections;
    }

    public getSection(assessmentId: string, sectionId: string): any {
        const params = {
            assessmentId,
            sectionId,
        };
        const config = this.Protocol.config("GET", "/Assessment/ReadSection", params);
        const messages = { Error: { object: "Section", action: this.translate("reading") } };
        return this.Protocol.http(config, messages);
    }

}
