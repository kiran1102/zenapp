
export default class OneAssessmentFactory {

    static $inject: string[] = ["ENUMS"];

    public assessment: any = {};
    public sections: any = {};
    public currentUser: any;
    public respondent: any;
    public approver: any;
    public creator: any;
    public orgGroup: any;
    public status: number;
    public readOnly: boolean;
    public isReadyForApproval: boolean;
    public activeQuestions: any[];
    public activeSection: any;
    public deadline: Date;
    public team: any[];
    public name: string;
    public asset: any;
    public id: string;

    private Contexts: any;

    constructor(ENUMS: any) {
        this.Contexts = ENUMS.ContextTypes;
    }

    public init(assessment: any, context: number): void {
        this.destroy();
        this.setAssessment(assessment);
        if (context === this.Contexts.DMAssessment) {
            this.formatDataMappingAssessment();
        }
    }

    public getActiveQuestions = (): any[] => {
        return this.activeQuestions;
    }

    public setActiveQuestions(questions: any[]): void {
        this.activeQuestions = questions;
    }

    public getActiveSection(): any[] {
        return this.activeSection;
    }

    public setActiveSection(section: any): void {
        this.activeSection = section;
    }

    public getApprover(): any {
        return this.approver;
    }

    public setApprover(user: any): void {
        this.approver = user;
    }

    public getAssessment(): any {
        return this.assessment;
    }

    public setAssessment(assessment: any): void {
        this.assessment = assessment;
    }

    public getAsset(): any {
        return this.asset;
    }

    public setAsset(asset: any): void {
        this.asset = asset;
    }

    public getCreator(): any {
        return this.creator;
    }

    public setCreator(user: any): void {
        this.creator = user;
    }

    public getCurrentUser(): any {
        return this.currentUser;
    }

    public setCurrentUser(user: any): void {
        this.currentUser = user;
    }

    public getDeadline(): Date {
        return this.deadline;
    }

    public setDeadline(date: Date): void {
        this.deadline = date;
    }

    public setDetail(key: string, value: any): void {
        this[key] = value;
    }

    public getId(): string {
        return this.id;
    }

    public setId(id: string): void {
        this.id = id;
    }

    public getIsReadyForApproval(): boolean {
        return this.isReadyForApproval;
    }

    public setIsReadyForApproval(isReady: boolean): void {
        this.isReadyForApproval = isReady;
    }

    public getName(): string {
        return this.name;
    }

    public setName(name: string): void {
        this.name = name;
    }

    public getOrgGroup(): string {
        return this.orgGroup;
    }

    public setOrgGroup(org: any): void {
        this.orgGroup = org;
    }

    public getReadOnly(): boolean {
        return this.readOnly;
    }

    public setReadOnly(readOnly: boolean): void {
        this.readOnly = readOnly;
    }

    public getRespondent(): any {
        return this.respondent;
    }

    public setRespondent(user: any): void {
        this.respondent = user;
    }

    public getSections(): any {
        return this.sections;
    }

    public setSections(sections: any): void {
        this.sections = sections;
    }

    public getStatus(): number {
        return this.status;
    }

    public setStatus(status: number): void {
        this.status = status;
    }

    public setTeam(users: any[]): void {
        this.team = users;
    }

    public getTeam(): any[] {
        return this.team;
    }

    private formatDataMappingAssessment(): void {
        this.setSections(this.assessment.sections);
        this.setOrgGroup(this.assessment.orgGroup);
        this.setStatus(this.assessment.statusId);
        this.setActiveSection(this.assessment.activeSection);
        this.setActiveQuestions(this.assessment.activeSection.nextQuestions);
        this.setAsset(this.assessment.asset);
        this.setId(this.assessment.assessmentId);
        this.setName(this.assessment.name);
    }

    private destroy(): void {
        this.assessment = null;
        this.sections = null;
        this.currentUser = null;
        this.respondent = null;
        this.approver = null;
        this.creator = null;
        this.orgGroup = null;
        this.status = null;
        this.readOnly = null;
        this.isReadyForApproval = null;
        this.activeQuestions = null;
        this.activeSection = null;
        this.deadline = null;
        this.team = null;
        this.name = null;
        this.asset = null;
        this.id = null;
    }

}
