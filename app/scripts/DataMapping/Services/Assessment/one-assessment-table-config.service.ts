// Interfaces
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";

// Enums
import { TableInputTypes } from "enums/inventory.enum";
import { AssesmentTemplateTypes } from "enums/assesment-template-types.enum.ts";
import { DMAssessmentStates } from "enums/assessment.enum.ts";

// Constants
import { DMAssessmentStateLabelKeys } from "constants/assessment.constants";
import { AssessmentStateClasses } from "constants/dashboard/assessment-status-color.constant";

export default class OneAssessmentTable {
    static $inject: string[] = ["$rootScope"];

    private translate: any;

    constructor(readonly $rootScope: IExtendedRootScopeService) {
        this.translate = this.$rootScope.t;
    }

    public getDMAssessmentTable(): any {
        return {
            cells: [
                {
                    text: "ID",
                    type: TableInputTypes.Text,
                    getLabel: (assessment: any): string => {
                        return assessment.assessmentRefId;
                    },
                    size: { width: "7rem" },
                    sortable: true,
                    sortKey: "assessmentRefId",
                    required: false,
                },
                {
                    text: this.translate("Name"),
                    type: TableInputTypes.Link,
                    link: "zen.app.pia.module.datamap.views.assessment.single",
                    linkParams: [
                        {
                            routeKey: "Id",
                            dataKey: "assessmentId",
                        },
                    ],
                    getLabel: (assessment: any): string => {
                        return assessment.name;
                    },
                    size: { width: "15rem" },
                    classes: "text-left",
                    sortable: true,
                    sortKey: "name",
                    required: true,
                },
                {
                    text: this.translate("Type"),
                    type: TableInputTypes.Text,
                    getLabel: (assessment: any): string => {
                        return this.translate(AssesmentTemplateTypes[assessment.templateType]);
                    },
                    size: { width: "15rem" },
                },
                {
                    text: this.translate("Status"),
                    type: TableInputTypes.ProjectState,
                    getLabel: (assessment: any): string => {
                        return this.translate(DMAssessmentStateLabelKeys[assessment.statusId]);
                    },
                    getColor: (assessment: any): string => {
                        return AssessmentStateClasses[DMAssessmentStates[assessment.statusId]];
                    },
                    size: { width: "11rem" },
                    sortable: false,
                    sortKey: "status",
                    required: false,
                },
                {
                    text: this.translate("Organization"),
                    type: TableInputTypes.Text,
                    getLabel: (assessment: any): string => {
                        return (assessment.orgGroup) ? assessment.orgGroup.name : "";
                    },
                    size: { width: "15rem" },
                    sortable: false,
                    sortKey: "orgGroup",
                },
                {
                    text: this.translate("CreatedBy"),
                    type: TableInputTypes.Text,
                    getLabel: (assessment: any): string => {
                        if (assessment.createdBy.fullName) {
                            return assessment.createdBy.fullName;
                        } else if (assessment.createdBy.firstName && assessment.createdBy.lastName) {
                            return `${assessment.createdBy.firstName} ${assessment.createdBy.lastName}`;
                        }
                        return assessment.createdBy.email;
                    },
                    size: { width: "15rem" },
                    sortable: false,
                    sortKey: "createdBy",
                    required: false,
                },
                {
                    text: this.translate("Respondent"),
                    type: TableInputTypes.Text,
                    getLabel: (assessment: any): string => {
                        if (assessment.respondent.fullName) {
                            return assessment.respondent.fullName;
                        } else if (assessment.respondent.firstName && assessment.respondent.lastName) {
                            return `${assessment.respondent.firstName} ${assessment.respondent.lastName}`;
                        }
                        return assessment.respondent.email;
                    },
                    size: { width: "15rem" },
                    sortable: false,
                    sortKey: "respondent",
                    required: false,
                },
                {
                    text: this.translate("Approver"),
                    type: TableInputTypes.Text,
                    getLabel: (assessment: any): string => {
                        if (assessment.approver.fullName) {
                            return assessment.approver.fullName;
                        } else if (assessment.approver.firstName && assessment.approver.lastName) {
                            return `${assessment.approver.firstName} ${assessment.approver.lastName}`;
                        }
                        return assessment.approver.email;
                    },
                    size: { width: "15rem" },
                    sortable: false,
                    sortKey: "approver",
                    required: false,
                },
                {
                    text: this.translate("DateCreated"),
                    type: TableInputTypes.Date,
                    getLabel: (assessment: any): string => {
                        return assessment.createdDate;
                    },
                    size: { width: "11rem" },
                    sortable: true,
                    sortKey: "createdDate",
                    required: true,
                },
                {
                    text: this.translate("Deadline"),
                    type: TableInputTypes.Date,
                    getLabel: (assessment: any): string => {
                        return assessment.deadlineDate;
                    },
                    size: { width: "10rem" },
                    sortable: true,
                    sortKey: "deadlineDate",
                    required: true,
                },
                {
                    text: this.translate("Actions"),
                    type: TableInputTypes.Actions,
                    size: { width: "8rem" },
                },
            ],
        };
    }

}
