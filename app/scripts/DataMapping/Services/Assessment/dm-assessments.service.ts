import * as _ from "lodash";
import Utilities from "Utilities";

import { ProtocolService } from "modules/core/services/protocol.service";
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import { ModalService } from "sharedServices/modal.service";
import { TaskPollingService } from "sharedServices/task-polling.service";

import { ITaskConfirmationModalResolve } from "interfaces/task-confirmation-modal-resolve.interface";

// Interfaces
import {
    IProtocolConfig,
    IProtocolMessages,
    IProtocolResponse,
} from "interfaces/protocol.interface";
import { IStore } from "interfaces/redux.interface";

// Redux
import { getCurrentUser } from "oneRedux/reducers/user.reducer";

export interface IAssessmentFilters {
    types?: number[];
    statuses?: number[];
    templates?: string[];
    name?: string;
    orgGroupId?: string;
}

export interface IAssessmentParams {
    page?: number;
    size?: number;
    sort?: string;
    filter?: IAssessmentFilters;
}

export class DMAssessmentsService {
    static $inject: string[] = [
        "$rootScope",
        "$q",
        "OneProtocol",
        "ModalService",
        "TaskPollingService",
        "store",
    ];

    private HTTP: any;
    private translate: any;

    private defaultAssessmentParams: IAssessmentParams = {
        page: 1,
        size: 10,
        sort: "assessmentRefId,DESC",
        filter: {
            types: [],
            statuses: [],
            templates: [],
            name: "",
            orgGroupId: "",
        },
    };

    constructor(
        $rootScope: IExtendedRootScopeService,
        private $q: ng.IQService,
        private OneProtocol: ProtocolService,
        private modalService: ModalService,
        private taskPollingService: TaskPollingService,
        private store: IStore,
    ) {
        this.translate = $rootScope.t;
    }

    public bulkCreate(bulkRequest: any): any {
        const config: any = this.OneProtocol.config("POST", "/DataMapping/AddBulk", [], bulkRequest);
        const messages: any = { Error: { object: this.translate("Assessments"), action: this.translate("creating") } };
        return this.OneProtocol.http(config, messages);
    }

    public dataMappingBulkCreate(bulkRequest: any): any {
        const config: any = this.OneProtocol.config("POST", "/Assessment/BulkCreate", [], bulkRequest);
        const messages: any = { Error: { object: this.translate("Assessments"), action: this.translate("creating") } };
        return this.OneProtocol.http(config, messages);
    }

    public getAssessments(params: IAssessmentParams = this.defaultAssessmentParams ): any {
        params.page = params.page ? --params.page : 0;
        const config: any = this.OneProtocol.config("GET", "/Assessment/ReadAssessments", params);
        const messages: any = { Error: { object: this.translate("Assessments"), action: this.translate("getting") } };
        return this.OneProtocol.http(config, messages);
        // return this.mockGetAssessments(params);
    }

    public getAssessment = (id: string): any => {
        const params: any = { assessmentId: id };
        const config: any = this.OneProtocol.config("GET", "/Assessment/ReadAssessment", params);
        const messages: any = { Error: { object: this.translate("Assessments"), action: this.translate("reading") } };
        return this.OneProtocol.http(config, messages);
    }

    public updateAssessment(id: string, request: any): any {
        const params: any = { assessmentId: id };
        const config: any = this.OneProtocol.config("PUT", "/Assessment/UpdateAssessment", params, request);
        const messages: any = { Error: { object: this.translate("Assessment"), action: this.translate("Updating") } };
        return this.OneProtocol.http(config, messages);
    }

    public deleteAssessment(id: string): any {
        const params: any = { assessmentId: id };
        const config: any = this.OneProtocol.config("DELETE", "/Assessment/DeleteAssessment", params);
        const messages: any = { Error: { object: this.translate("Assessment"), action: this.translate("deleting") } };
        return this.OneProtocol.http(config, messages);
    }

    public submitAssessment(id: string, actions: any): any {
        const params: any = { assessmentId: id };
        const config: any = this.OneProtocol.config("GET", "/Assessment/SubmitAssessment", params);
        const messages: any = {
            Success: { object: this.translate("Assessment"), action: actions.success },
            Error: { object: this.translate("Assessment"), action: actions.error },
        };
        return this.OneProtocol.http(config, messages);
    }

    // TODO: add param "id: number" to the method once the BE accepts it
    public getDataMappingStatuses = (): any => {
        // const params: any = { inventoryId: id }; Keep for BE to update the API call
        const config: any = this.OneProtocol.config("GET", "/Assessment/getAssetStatus");
        const messages: any = { Error: { Hide: true } };
        return this.OneProtocol.http(config, messages);
    }

    public getElementsStatuses = (): any => {
        const config: any = this.OneProtocol.config("GET", "/Assessment/GetAssessmentsElementsInUse");
        const messages: any = { Error: { Hide: true } };
        return this.OneProtocol.http(config, messages);
    }

    public exportV2Report(templateType: string): ng.IPromise<void> {
       return this.assessmentV2Export(templateType).then((): void => {
            const modalData: ITaskConfirmationModalResolve = {
                modalTitle: this.translate("ReportDownload"),
                confirmationText: this.translate("ReportIsGeneratedCheckTasksBar"),
            };
            this.modalService.setModalData(modalData);
            this.modalService.openModal("oneNotificationModalComponent");
        });
    }

    public assessmentV2Export = (templateType: string): ng.IPromise<IProtocolResponse<null>> => {
        this.taskPollingService.startPolling(5000);
        const config: IProtocolConfig = this.OneProtocol.config("GET", `/Assessment/ExportAssessments/${templateType}`);
        const messages: IProtocolMessages = { Error: { custom: this.translate("ExportFailed") } };
        return this.OneProtocol.http(config, messages);
    }

    // MOCK ========================================

    private mockGetAssessments(params: IAssessmentParams): ng.IPromise<any> {
        const deferred: ng.IDeferred<any> = this.$q.defer();
        const list: any[] = this.mockAssessmentsList(params.size);
        const sortArr: string[] = params.sort.split(",") || ["name", "asc"];
        sortArr[1] = sortArr[1].toLowerCase();
        const page: any = {
            content: _.orderBy(list, sortArr),
            first: !params.page,
            last: params.page === (params.size * 10 - 1),
            number: params.page,
            numberOfElements: params.size,
            size: params.size,
            sort: [{
                ascending: sortArr[1] === "asc",
                direction: sortArr[1] === "asc" ? "ASC" : "DESC",
                ignoreCase: false,
                nullHandling: "NATIVE",
                property: sortArr[0],
            }],
            totalElements: params.size * 10,
            totalPages: params.size,
        };
        deferred.resolve({
            result: true,
            data: page,
        });
        return deferred.promise;
    }

    private mockAssessmentsList(num: number): any[] {
        const records: any[] = [];
        for (let i = 0; i < num; i++) {
            records.push(this.mockAssessmentSingle(i));
        }
        return records;
    }

    private mockAssessmentSingle(id: number): any {
        const user: any = getCurrentUser(this.store.getState());
        const assessmentUser: any = {
            firstName: user.FirstName,
            lastName: user.LastName,
            email: user.Email,
        };
        const assessmentOrg: any = {
            name: `${assessmentUser.firstName} ${assessmentUser.lastName} Group`,
            id: user.OrgGroupId,
        };
        const randomDate: any = (start: Date, end: Date): Date => {
            return new Date(start.getTime() + Math.random() * (end.getTime() - start.getTime()));
        };
        return {
            assessmentRefId: id,
            name: Utilities.uuid(),
            templateType: 20,
            status: "InProgress",
            statusId: 20,
            orgGroup: assessmentOrg,
            createdBy: assessmentUser,
            respondent: assessmentUser,
            approver: assessmentUser,
            createdDate: randomDate(new Date(2016, 0, 1), new Date()),
            deadlineDate: randomDate(new Date(), new Date(2018, 12, 30)),
        };
    }

}
