
export default class TemplateModalService {

    public getEditSectionTemplate(): string {
        return `
                <section-edit-modal
                    scope="controller"
                    >
                </section-edit-modal>
            `;
    }

    public getQuestionTemplate(): string {
        return `
                <question-modal
                    scope="controller"
                    >
                </question-modal>
            `;
    }

    public getConditionsTemplate(): string {
        return `
                <conditions-modal
                    scope="controller"
                    >
                </conditions-modal>
            `;
    }
}
