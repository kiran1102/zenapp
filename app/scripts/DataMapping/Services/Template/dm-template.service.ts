import { Injectable } from "@angular/core";

import { ProtocolService } from "modules/core/services/protocol.service";

import { TranslatePipe } from "pipes/translate.pipe";

@Injectable()
export class DMTemplateService {

    constructor(
        private OneProtocol: ProtocolService,
        private translatePipe: TranslatePipe,
    ) { }

    public list(templateType?: string): ng.IPromise<any> {
        const params: any = { templateType };
        const config: any = this.OneProtocol.config("GET", "/TemplateAssessment/List", params);
        const messages: any = { Error: { object: this.translatePipe.transform("Templates"), action: this.translatePipe.transform("Getting") } };
        return this.OneProtocol.http(config, messages);
    }

    public read(templateId: string): ng.IPromise<any> {
        const params: any = { templateId };
        const config: any = this.OneProtocol.config("GET", "/TemplateAssessment/ReadByTemplateIdAndCurrentOrg", params);
        const messages: any = { Hide: true };
        return this.OneProtocol.http(config, messages);
    }

    public readOrgLatest(templateType?: string): ng.IPromise<any> {
        const params: any = { templateType };
        const config: any = this.OneProtocol.config("GET", "/TemplateAssessment/ReadTemplateByCurrentOrg", params);
        const messages: any = { Error: { object: this.translatePipe.transform("Template"), action: this.translatePipe.transform("Reading") } };
        return this.OneProtocol.http(config, messages);
    }

    public addSection(templateId: string, request: any): ng.IPromise<any> {
        const params: any = { templateId };
        const config: any = this.OneProtocol.config("POST", "/TemplateAssessment/CreateSection", params, request);
        const messages: any = { Error: { object: this.translatePipe.transform("Section"), action: this.translatePipe.transform("Adding") } };
        return this.OneProtocol.http(config, messages);
    }

    public updateSection(templateId: string, sectionId: string, request: any): ng.IPromise<any> {
        const params: any = { templateId, sectionId };
        const config: any = this.OneProtocol.config("PUT", "/TemplateAssessment/UpdateSection", params, request);
        const messages: any = { Error: { object: this.translatePipe.transform("Section"), action: this.translatePipe.transform("Editing") } };
        return this.OneProtocol.http(config, messages);
    }

    public addQuestion(templateId: string, sectionId: string, request: any): ng.IPromise<any> {
        const params: any = { templateId, sectionId };
        const config: any = this.OneProtocol.config("POST", "/TemplateAssessment/CreateQuestion", params, request);
        const messages: any = { Hide: true };
        return this.OneProtocol.http(config, messages);
    }

    public updateQuestion(templateId: string, sectionId: string, questionId: string, request: any): ng.IPromise<any> {
        const params: any = { templateId, sectionId, questionId };
        const config: any = this.OneProtocol.config("PUT", "/TemplateAssessment/UpdateQuestion", params, request);
        const messages: any = { Hide: true };
        return this.OneProtocol.http(config, messages);
    }

    public create(templateId?: string): ng.IPromise<any> {
        const params: any = { templateId };
        const config: any = this.OneProtocol.config("POST", "/TemplateAssessment/Create", params);
        const messages: any = { Error: { object: this.translatePipe.transform("ThisVersion"), action: this.translatePipe.transform("Getting") } };
        return this.OneProtocol.http(config, messages);
    }

    public deleteSection(templateId: string, sectionId: string): ng.IPromise<any> {
        const params: any = { templateId, sectionId };
        const config: any = this.OneProtocol.config("DELETE", "/TemplateAssessment/DeleteSection", params);
        const messages: any = { Error: { object: this.translatePipe.transform("Section"), action: this.translatePipe.transform("Deleting") } };
        return this.OneProtocol.http(config, messages);
    }

    public deleteQuestion(templateId: string, sectionId: string, questionId: string): ng.IPromise<any> {
        const params: any = { templateId, sectionId, questionId };
        const config: any = this.OneProtocol.config("DELETE", "/TemplateAssessment/DeleteQuestion", params);
        const messages: any = { Error: { object: this.translatePipe.transform("Question"), action: this.translatePipe.transform("Deleting") } };
        return this.OneProtocol.http(config, messages);
    }

    public publish(templateId: string): ng.IPromise<any> {
        const params: any = { templateId };
        const config: any = this.OneProtocol.config("PUT", "/TemplateAssessment/Publish", params);
        const messages: any = { Error: { object: this.translatePipe.transform("Template"), action: this.translatePipe.transform("Publishing") } };
        return this.OneProtocol.http(config, messages);
    }

    public deleteVersion(templateId: string): ng.IPromise<any> {
        const params: any = { templateId };
        const config: any = this.OneProtocol.config("DELETE", "/TemplateAssessment/Delete", params);
        const messages: any = { Error: { object: this.translatePipe.transform("Template"), action: this.translatePipe.transform("Deleting") } };
        return this.OneProtocol.http(config, messages);
    }

    public updatePublished(templateId: string): ng.IPromise<any> {
        const params: any = { templateId };
        const config: any = this.OneProtocol.config("PUT", "/TemplateAssessment/UpdatePublish", params);
        const messages: any = { Error: { object: this.translatePipe.transform("Template"), action: this.translatePipe.transform("Updating") } };
        return this.OneProtocol.http(config, messages);
    }
}
