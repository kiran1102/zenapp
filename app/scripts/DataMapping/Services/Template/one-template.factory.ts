import { assign, isArray, find, forEach, isUndefined, sortBy, filter } from "lodash";

import { IProtocolPacket } from "interfaces/protocol.interface";
import { IDMAssessmentTemplate } from "modules/template/interfaces/template.interface";
import { IDMAssessmentQuestion } from "interfaces/question.interface";
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import { ISectionEditModalResolve } from "interfaces/inventory.interface";
import { DMTemplateService } from "datamappingservice/Template/dm-template.service";

class OneTemplateFactory {

    static $inject: string[] = [
        "$rootScope",
        "$state",
        "$q",
        "$templateCache",
        "$stateParams",
        "ENUMS",
        "DMTemplate",
        "ModalService",
        "TemplateModals",
        "NotifyService",
        "DefaultQuestion",
        "OrgGroupStoreNew",
    ];

    public template: any = {};
    private dictionary: any = { sections: {}, questions: {} };
    private QuestionTypes: any;
    private TemplateTypes: any;
    private TemplateStates: any;
    private translate: any;
    private templateType: string;

    constructor(
        readonly $rootScope: IExtendedRootScopeService,
        readonly $state: ng.ui.IStateService,
        readonly $q: ng.IQService,
        readonly $templateCache: any,
        readonly $stateParams: ng.ui.IStateParamsService,
        ENUMS: any,
        readonly DMTemplate: DMTemplateService,
        readonly ModalService: any,
        readonly TemplateModals: any,
        readonly NotifyService: any,
        readonly DefaultQuestion: any,
        readonly OrgGroupStoreNew: any,
    ) {
        this.QuestionTypes = ENUMS.DMQuestionTypes;
        this.TemplateTypes = ENUMS.TemplateTypes;
        this.TemplateStates = ENUMS.DMTemplateStates;
        this.translate = $rootScope.t;
    }

    public init(id: string, config: any = {}, data?: any): ng.IPromise<any> {
        this.destroy();
        this.templateType = this.$stateParams.templateType;
        const templatePromises: any[] = [
            this.getTemplateVersions(this.templateType),
            this.getCurrentOrg(),
        ];
        if (data) {
            this.template = assign(this.template, data);
        } else {
            templatePromises.push(this.getTemplate(id, this.templateType));
        }
        return this.$q.all(templatePromises).then( (responses: any[]): any => {
            if (isArray(responses) && responses.length) {
                const failedResponses: any[] = filter(responses, {result: false} as any);
                if (failedResponses.length) return {result: false, data: null};
                this.getModalTemplates();
                this.template = this.formatTemplate(this.template, true);
                this.template.isLatestPublishedVersion = this.setIsLatestPublishedVersion();
                this.template = assign(this.template, config);
                return {result: true, data: this.template};
            }
        });
    }

    public getModalTemplates(): void {
        this.$templateCache.put("sectionEditModal.html", this.TemplateModals.getEditSectionTemplate());
        this.$templateCache.put("questionModal.html", this.TemplateModals.getQuestionTemplate());
        this.$templateCache.put("conditionsModal.html", this.TemplateModals.getConditionsTemplate());
    }

    public getTemplate(id: string, templateType: string): ng.IPromise<any> {
        return this.DMTemplate.read(id).then( (response: any): void|any => {
            if (!response.result) {
                return this.DMTemplate.readOrgLatest(templateType).then( (orgLatestResponse: any): void|any => {
                    if (!orgLatestResponse.result) {
                        this.$state.go("zen.app.pia.module.datamap.views.assessment.list");
                    } else {
                        this.template = assign(this.template, orgLatestResponse.data);
                        return { result: true, data: this.template };
                    }
                });
            } else {
                this.template = assign(this.template, response.data);
                return { result: true, data: this.template };
            }
        });
    }

    public getTemplateVersions(templateType: string): ng.IPromise<any> {
        return this.DMTemplate.list(templateType).then( (response: any): any => {
            if (!response.result) return { result: false, data: null };
            const versionList: any[] = this.formatVersions(response.data);
            this.template = assign(this.template, { versionList });
            return { result: true, data: this.template };
        });
    }

    public getCurrentOrg(): void {
        const currentOrg = this.OrgGroupStoreNew.selectedOrg;
        this.template.isRootOrg = currentOrg && !currentOrg.level;
    }

    public setIsLatestPublishedVersion(): boolean {
        if (!isArray(this.template.versionList) || !isArray(this.template.versionList[0].versions)) {
            return false;
        }
        const latestPublishedTemplate: any =  find(this.template.versionList[0].versions, {statusId: this.TemplateStates.Published});
        if (latestPublishedTemplate) {
            return this.template.templateVersion === latestPublishedTemplate.templateVersion;
        }
        return false;
    }

    public formatVersions(list: any[]): any {
        const sortedList: any[] = sortBy(list, "templateVersion").reverse();
        let splitter = 1;
        if (sortedList[0].statusId === this.TemplateStates.Draft) {
            splitter = 2;
        }
        const versionGroups: any[] = [
            { title: this.translate("CurrentVersions"), versions: sortedList.slice(0, splitter) },
            { title: this.translate("PreviousVersions"), versions: sortedList.slice(splitter) },
        ];
        return versionGroups;
    }

    public formatTemplate(templateData: any, initial: boolean = false): any {
        let questionNumber = 0;
        templateData.disabledDraggableTypes = {};
        if (!initial) {
            assign(this.template, templateData);
            templateData = this.template;
        }
        forEach(templateData.sections, (section: any, sectionIndex: number): void => {
            if (!initial && this.dictionary.sections[section.templateSectionUniqueId]) {
                section.isOpen = this.dictionary.sections[section.templateSectionUniqueId].isOpen;
            }
            this.dictionary.sections[section.templateSectionUniqueId] = section;
            section.number = sectionIndex;
            forEach(section.questions, (question: any): void => {
                if (!initial && this.dictionary.questions[question.templateQuestionUniqueId]) {
                    question.isOpen = this.dictionary.questions[question.templateQuestionUniqueId].isOpen;
                }
                if (question.questionType === this.QuestionTypes.AppInfo || question.questionType === this.QuestionTypes.DataSubject) {
                    templateData.disabledDraggableTypes[question.questionType] = true;
                }
                if (question.questionType !== this.QuestionTypes.Bumper) question.number = questionNumber++;
                this.dictionary.questions[question.templateQuestionUniqueId] = question;
                question.sectionId = section.templateSectionUniqueId;
            });
        });
        return templateData;
    }

    public toggleEditMode(): any {
        this.template.editMode = !this.template.editMode;
        if (!this.template.editMode) {
            this.updatePublished(this.template.id);
        }
        return this.template;
    }

    public toggleVersionHistory(): any {
        this.template.showingVersionHistory = !this.template.showingVersionHistory;
        return this.template;
    }

    public launchSectionModal(callback: (any) => any, sectionId?: string): void {
        const isNewSection: boolean = !Boolean(sectionId);
        const modalData: ISectionEditModalResolve = {
            modalTitle: isNewSection ? "AddSection" : "SubmitChanges",
            sectionId,
            templateId: this.template.id,
            isNewSection,
            sectionName: isNewSection ? "" : this.dictionary.sections[sectionId].name,
            section: isNewSection ? "" : this.dictionary.sections[sectionId],
            previousSectionId: isNewSection ? this.template.sections[this.template.sections.length - 1].templateSectionUniqueId : null,
            callback: (response) => {
                callback(response);
                this.template = this.formatTemplate(response.data);
            },
        };

        this.ModalService.setModalData(modalData);
        this.ModalService.openModal("sectionEditModal");
    }

    public launchQuestionModal(sectionId: string, question?: any, config?: any): Promise<any> {
        const deferred: any = this.$q.defer();
        const isNewQuestion: boolean = !Boolean(question);
        const section: any = this.dictionary.sections[sectionId];
        const filteredQuestions: any[] = filter(section.questions, (sectionQuestion: any): boolean => sectionQuestion.questionType !== this.QuestionTypes.Bumper);
        const sectionLastQuestionId: string = filteredQuestions.length ? filteredQuestions[filteredQuestions.length - 1].templateQuestionUniqueId : "";
        const previousQuestionId: string = isNewQuestion ? (config ? config.previousQuestionId : sectionLastQuestionId) : null;
        const data: any = {
            templateId: this.template.id,
            templateStatusId: this.template.statusId,
            templateType: this.template.templateType,
            sectionId,
            question: isNewQuestion ? "" : question,
            previousQuestionId,
            config,
        };
        const cb: any = (response: any): void => {
            if (response.result) {
                this.template = this.formatTemplate(response.data);
                deferred.resolve({result: true, data: this.template});
            } else {
                deferred.resolve({result: false, data: null});
            }
        };
        this.ModalService.renderModal(data, "QuestionModalCtrl", "questionModal.html", "questionModalCb", "lg");
        const destroyQuestionModalCb: any = this.$rootScope.$on("questionModalCb",
            (event: ng.IAngularEvent, args: any): void => {
                cb(args.response);
                destroyQuestionModalCb();
            },
        );
        return deferred.promise;
    }

    public launchConditionsModal(sectionId: string, question: any): Promise<any> {
        const deferred: any = this.$q.defer();
        const data: any = {
            templateId: this.template.id,
            templateStatusId: this.template.statusId,
            section: this.dictionary.sections[sectionId],
            question,
        };
        const cb: any = (response: any): void => {
            if (response.result) {
                this.template = this.formatTemplate(response.data);
                deferred.resolve({result: true, data: this.template});
            } else {
                deferred.resolve({result: false, data: null});
            }
        };
        this.ModalService.renderModal(data, "ConditionsModalCtrl", "conditionsModal.html", "conditionsModalCb", "md");
        const destroyConditionsModalCb: any = this.$rootScope.$on("conditionsModalCb",
            (event: ng.IAngularEvent, args: any): void => {
                cb(args.response);
                destroyConditionsModalCb();
            },
        );
        return deferred.promise;
    }

    public goToNewVersion(): any|Promise<void> {
        return this.DMTemplate.create(this.template.id).then( (response: any): any => {
            if (!response.result) return { result: false, data: null };
            this.$state.go("zen.app.pia.module.datamap.views.template", {templateId: response.data.id}, { notify: false });
            return this.init(response.data.id, {showingVersionHistory: this.template.showingVersionHistory});
        });
    }

    public goToVersion(templateId: string): any|Promise<void> {
        this.$state.go("zen.app.pia.module.datamap.views.template", { templateId }, { notify: false });
        return this.init(templateId, {showingVersionHistory: this.template.showingVersionHistory});
    }

    public publishTemplate(): any|Promise<any> {
        return this.NotifyService.confirm("", this.translate("AreYouSureToPublishTemplateName", { TemplateName: this.template.name }), "", "Cancel").then( (result: boolean): any => {
            if (!result) return { result: false, data: null };
            this.$rootScope.$broadcast("DM_TEMPLATE_START_LOADING");
            return this.DMTemplate.publish(this.template.id).then( (response: any): any => {
                if (!response.result) return { result: false, data: null };
                return this.init(response.data.id, {showingVersionHistory: this.template.showingVersionHistory});
            });
        });
    }

    public deleteSection(section: any): any|Promise<any> {
        return this.NotifyService.confirm("", this.translate("AreYouSureToDeleteSectionName", { SectionName: section.name}), "", "Cancel").then( (result: boolean): any => {
            if (!result) return { result: false, data: null };
            return this.DMTemplate.deleteSection(this.template.id, section.templateSectionUniqueId).then( (response: any): any => {
                if (!response.result) return { result: false, data: null };
                this.template = this.formatTemplate(response.data);
                return { result: true, data: this.template };
            });
        });
    }

    public deleteQuestion(question: any): Promise<any> {
        return this.NotifyService.confirm("", this.translate("SureToDeleteThisQuestion"), "", "Cancel").then( (result: boolean): any => {
            if (!result) return { result: false, data: null };
            return this.DMTemplate.deleteQuestion(this.template.id, question.sectionId, question.templateQuestionUniqueId).then( (response: any): any => {
                if (!response.result) return { result: false, data: null };
                this.template = this.formatTemplate(response.data);
                return { result: true, data: this.template };
            });
        });
    }

    public toggleSection(sectionId: string): any {
        const sectionDefinition: any = this.dictionary.sections[sectionId];
        sectionDefinition.isOpen = !sectionDefinition.isOpen;
        return this.template;
    }

    public toggleQuestion(questionId: string): any {
        const questionDefinition: any = this.dictionary.questions[questionId];
        if (!isUndefined(questionDefinition)) {
            questionDefinition.isOpen = !questionDefinition.isOpen;
        }
        return this.template;
    }

    public toggleHideQuestion(sectionId: string, questionId: string): ng.IPromise<any> {
        let questionDefinition: IDMAssessmentQuestion | undefined = this.dictionary.questions[questionId];
        if (isUndefined(questionDefinition)) {
            const dataSubjectQuestion: IDMAssessmentQuestion = find(this.dictionary.questions, (question: IDMAssessmentQuestion) => question.questionType === this.QuestionTypes.DataSubject );
            questionDefinition = find(dataSubjectQuestion.followUpQuestions as IDMAssessmentQuestion[], { templateQuestionUniqueId: questionId });
            sectionId = dataSubjectQuestion.sectionId;
            questionId = questionDefinition.templateQuestionUniqueId;
            questionDefinition.disabled = !questionDefinition.disabled;
        } else {
            if (questionDefinition.isOpen) {
                questionDefinition.isOpen = !questionDefinition.isOpen;
            }
            questionDefinition.disabled = !questionDefinition.disabled;
        }
        return this.DMTemplate.updateQuestion(this.template.id, sectionId, questionId, questionDefinition).then((response: IProtocolPacket): IDMAssessmentTemplate | { result: boolean, data: null } => {
            if (!response.result) return { result: false, data: null };
            this.template = this.formatTemplate(response.data);
            return this.template;
        });
    }

    public addPlaceholderQuestion(sectionId: string, config: any): ng.IPromise<any>  {
        const placeholderRequest: any = this.DefaultQuestion.getDefaultQuestion(this.TemplateTypes.Datamapping, config.questionType, config);
        return this.DMTemplate.addQuestion(this.template.id, sectionId, placeholderRequest).then( (response: any): any => {
            if (response.result) {
                this.template = this.formatTemplate(response.data);
                return { result: true, data: this.template };
            } else {
                this.dictionary.sections[sectionId].questions.splice(config.index, 1);
                return { result: true, data: this.template };
            }
        });
    }

    public deleteVersion(templateId: string, isCurrentVersion: boolean): Promise<any> {
        const confirmMessage: string = this.translate("SureToDiscardQuestionnaireTemplateDraft", { TemplateDraftName: this.template.name });
        return this.NotifyService.confirm("", confirmMessage, "", "Cancel").then( (result: boolean): any|Promise<any> => {
            const currentVersions: any = this.template.versionList[0];
            if (!result || !currentVersions || !currentVersions.versions || !currentVersions.versions[1]) {
                return { result: false, data: null };
            }
            return this.DMTemplate.deleteVersion(templateId).then( (response: any): any|Promise<any> => {
                if (!response.result) return { result: false, data: null };
                if (isCurrentVersion) {
                    const previousTemplateId: string = currentVersions.versions[1].id;
                    this.$state.go("zen.app.pia.module.datamap.views.template", {templateId: previousTemplateId}, { notify: false });
                    return this.init(previousTemplateId, {showingVersionHistory: this.template.showingVersionHistory});
                }
                currentVersions.versions = currentVersions.versions.slice(1);
                return { result: true, data: this.template };
            });
        });
    }

    public moveQuestion(config: any): ng.IPromise<undefined | any> {
        const question: any = assign(this.dictionary.questions[config.questionId], {sequenceId: config.index + 1});
        return this.DMTemplate.updateQuestion(this.template.id, config.sectionId, config.questionId, question).then( (response: any): undefined | any => {
            if (!response.result) return;
            this.template = this.formatTemplate(response.data);
            return this.template;
        });
    }

    public updatePublished(templateId: string): void {
        this.DMTemplate.updatePublished(templateId);
    }

    private destroy(): void {
        this.templateType = "";
        this.template = {};
        this.dictionary = { sections: {}, questions: {} };
    }

}

export default OneTemplateFactory;
