declare var angular: angular.IAngularStatic;

import routes from "./inventory.routes";

import OneInventories from "datamappingservice/Inventory/inventories.factory";
import { InventoryAdapter } from "oneServices/adapters/inventory-adapter.service";
import { DrawerActionService } from "oneServices/actions/drawer-action.service";

export const inventoryLegacyModule = angular.module("zen.inventory", [
    "ui.router",
])
    .config(routes)
    .service("OneInventories", OneInventories)
    .service("InventoryAdapter", InventoryAdapter)
    .service("DrawerActionService", DrawerActionService)
    ;
