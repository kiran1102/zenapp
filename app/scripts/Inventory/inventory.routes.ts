// Services
import { Permissions } from "modules/shared/services/helper/permissions.service";

// Enums
import { InventoryTableIds } from "enums/inventory.enum";
import { ModuleTypes } from "enums/module-types.enum";

// Constants
import {
    InventoryDetailsTabs,
    InventoryPermissions,
    InventoryListDetailsTabs,
} from "constants/inventory.constant";
import {
    InventorySchemaIds,
    NewInventorySchemaIds,
} from "constants/inventory-config.constant";
import { ControlPermissions } from "linkControlsAPIModule/constants/link-controls.constant";

export default function routes($stateProvider: ng.ui.IStateProvider, $urlRouterProvider: ng.ui.IUrlRouterProvider) {

    $urlRouterProvider.when("/pia/datamap/attribute-manager", "/pia/datamap/attribute-manager/");
    $urlRouterProvider.when("/pia/datamap/inventory-manager", "/pia/datamap/inventory-manager/");

    $stateProvider
        .state("zen.app.pia.module.datamap.views.inventory", {
            url: "inventory",
            template: "<ui-view></ui-view>",
            resolve: {
                DataMappingViewsPermission: [
                    "DataMappingViewsPermission",
                    (DataMappingViewsPermission: boolean): boolean => {
                        return DataMappingViewsPermission;
                    },
                ],
            },
        })
        .state("zen.app.pia.module.datamap.views.inventory.list", {
            url: "-list/:Id?size&page",
            params: { time: null, Filters: {}, Records: [], openDataDiscoveryModal: null },
            template: "<inventory-list-wrapper></inventory-list-wrapper>",
            resolve: {
                DataMappingInventoryNewPermission: [
                    "DataMappingViewsPermission",
                    "Permissions",
                    "$stateParams",
                    (
                        DataMappingViewsPermission: boolean,
                        permissions: Permissions,
                        $stateParams: ng.ui.IStateParamsService,
                    ): boolean => {
                        const inventoryId = $stateParams.inventoryId;
                        if (
                            (permissions.canShow("DataMappingInventoryNew") && inventoryId !== InventorySchemaIds.Vendors) &&
                            ((permissions.canShow("EnableDataSubjectsElementsV2") && inventoryId !== InventorySchemaIds.Elements) ||
                                !permissions.canShow("EnableDataSubjectsElementsV2"))
                        ) {
                            return true;
                        } else {
                            permissions.goToFallback();
                        }
                        return false;
                    },
                ],
            },
        })
        .state("zen.app.pia.module.datamap.views.inventory.details", {
            url: "/details/:inventoryId/:recordId/:tabId",
            params: {
                time: null,
                tabId: { dynamic: true, value: InventoryDetailsTabs.Details },
            },
            template: "<inventory-details></inventory-details>",
            resolve: {
                DataMappingPermission: [
                    "Permissions",
                    "$stateParams",
                    (
                        permissions: Permissions,
                        $stateParams: ng.ui.IStateParamsService,
                    ): boolean => {
                        const inventoryId: number = parseInt($stateParams.inventoryId, 10);
                        if (
                            permissions.canShow(InventoryPermissions[inventoryId].details) ||
                            permissions.canShow(InventoryPermissions[inventoryId].viewAssessmentLinks) ||
                            permissions.canShow(InventoryPermissions[inventoryId].viewRisks) ||
                            permissions.canShow(InventoryPermissions[inventoryId].viewRelated) ||
                            permissions.canShow(InventoryPermissions[inventoryId].viewLineage) ||
                            permissions.canShow(ControlPermissions[NewInventorySchemaIds[inventoryId]].viewControlTab) ||
                            permissions.canShow(InventoryPermissions[inventoryId].viewActivity) ||
                            permissions.canShow(InventoryPermissions[inventoryId].attachments) ||
                            permissions.canShow(InventoryPermissions[inventoryId].vendorContractInventoryLink) ||
                            permissions.canShow(InventoryPermissions[inventoryId].vendorDocumentTab)
                        ) {
                            return true;
                        } else {
                            permissions.goToFallback();
                        }
                        return false;
                    },
                ],
            },
        })
        .state("zen.app.pia.module.datamap.views.inventory.risk-details", {
            url: "/details/:inventoryId/:recordId/:inventoryName/risk/:riskId/:tabId",
            params: {
                time: null,
            },
            template: "<downgrade-inventory-risk-details></downgrade-inventory-risk-details>",
            resolve: {
                DataMappingPermission: [
                    "DataMappingViewsPermission",
                    "Permissions",
                    "$stateParams",
                    (
                        DataMappingViewsPermission: boolean,
                        permissions: Permissions,
                        $stateParams: ng.ui.IStateParamsService,
                    ): boolean => {
                        const inventoryId: number = parseInt($stateParams.inventoryId, 10);
                        if (
                            (inventoryId === InventoryTableIds.Assets && permissions.canShow("DataMappingInventoryAssetsDetails")) ||
                            (inventoryId === InventoryTableIds.Processes && permissions.canShow("DataMappingInventoryPADetails")) ||
                            (inventoryId === InventoryTableIds.Entities)
                        ) {
                            return true;
                        } else {
                            permissions.goToFallback();
                        }
                        return false;
                    },
                ],
            },
        })
        .state("zen.app.pia.module.datamap.views.attribute-manager", {
            url: "attribute-manager",
            template: "<ui-view></ui-view>",
            resolve: {
                DataMappingInventoryNewPermission: [
                    "DataMappingViewsPermission",
                    "Permissions",
                    "$stateParams",
                    (
                        DataMappingViewsPermission: boolean,
                        permissions: Permissions,
                        $stateParams: ng.ui.IStateParamsService,
                    ): boolean => {
                        if (permissions.canShow("InventoryViewAttributes")) return true;
                        permissions.goToFallback();
                        return false;
                    },
                ],
            },
        })
        .state("zen.app.pia.module.datamap.views.attribute-manager.home", {
            url: "/",
            template: "<attribute-manager-entry></attribute-manager-entry>",
            resolve: {
                DataMappingAttributeManagerPermission: [
                    "DataMappingInventoryNewPermission",
                    "Permissions",
                    "$stateParams",
                    (
                        DataMappingInventoryNewPermission: boolean,
                        permissions: Permissions,
                        $stateParams: ng.ui.IStateParamsService,
                    ): boolean => {
                        if (permissions.canShow("InventoryViewAttributes") && !permissions.canShow("EnableDataSubjectsElementsV2")) return true;
                        permissions.goToFallback();
                        return false;
                    },
                ],
            },
        })
        .state("zen.app.pia.module.datamap.views.attribute-manager.list", {
            url: "/:id",
            template: "<attribute-manager></attribute-manager>",
            resolve: {
                DataMappingAttributeManagerPermission: [
                    "DataMappingInventoryNewPermission",
                    "Permissions",
                    "$stateParams",
                    (
                        DataMappingInventoryNewPermission: boolean,
                        permissions: Permissions,
                        $stateParams: ng.ui.IStateParamsService,
                    ): boolean => {
                        if (permissions.canShow(InventoryPermissions[$stateParams.id].viewAttributes)) return true;
                        permissions.goToFallback();
                        return false;
                    },
                ],
            },
        })
        .state("zen.app.pia.module.datamap.views.attribute-manager.details", {
            url: "/:id/:attributeId/:tabId",
            params: {
                time: null,
                tabId: { dynamic: true, value: "details" },
            },
            template: "<attribute-manager-details></attribute-manager-details>",
            resolve: {
                DataMappingAttributeManagerPermission: [
                    "DataMappingInventoryNewPermission",
                    "Permissions",
                    "$stateParams",
                    (
                        DataMappingInventoryNewPermission: boolean,
                        permissions: Permissions,
                        $stateParams: ng.ui.IStateParamsService,
                    ): boolean => {
                        if (permissions.canShow(InventoryPermissions[$stateParams.id].viewAttributes)) return true;
                        permissions.goToFallback();
                        return false;
                    },
                ],
            },
        })
        .state("zen.app.pia.module.datamap.views.link-controls", {
            url: "link-controls",
            template: "<ui-view></ui-view>",
            resolve: {
                DataMappingInventoryNewPermission: [
                    "DataMappingViewsPermission",
                    "Permissions",
                    (
                        DataMappingViewsPermission: boolean,
                        permissions: Permissions,
                    ): boolean => {
                        if (permissions.canShow("InventoryControlsTab")) return true;
                        permissions.goToFallback();
                        return false;
                    },
                ],
            },
        })
        .state("zen.app.pia.module.datamap.views.link-controls.details", {
            url: "/:recordType/:recordId/:controlId/:editMode",
            template: "<link-controls-list-detail></link-controls-list-detail>",
            resolve: {
                DataMappingLinkControlsPermission: [
                    "DataMappingInventoryNewPermission",
                    "Permissions",
                    (
                        DataMappingInventoryNewPermission: boolean,
                        permissions: Permissions,
                    ): boolean => {
                        return true;
                    },
                ],
            },
        })
        .state("zen.app.pia.module.datamap.views.bulk-edit", {
            url: "bulk-edit/:schemaId",
            params: {
                recordIds: [],
                returnRoute: "",
            },
            template: "<inventory-bulk-edit></inventory-bulk-edit>",
            resolve: {
                DataMappingInventoryNewPermission: [
                    "DataMappingViewsPermission",
                    "Permissions",
                    "$stateParams",
                    (
                        DataMappingViewsPermission: boolean,
                        permissions: Permissions,
                        $stateParams: ng.ui.IStateParamsService,
                    ): boolean => {
                        if (
                            permissions.canShow(InventoryPermissions[$stateParams.schemaId].bulkEdit)
                            && $stateParams.recordIds && $stateParams.recordIds.length
                            && $stateParams.returnRoute
                        ) return true;
                        permissions.goToFallback("zen.app.pia.module.datamap.views.inventory.list", { Id: $stateParams.schemaId });
                        return false;
                    },
                ],
            },
        })
        .state("zen.app.pia.module.datamap.views.inventory-manager", {
            url: "inventory-manager",
            template: "<ui-view></ui-view>",
            resolve: {
                DataMappingListManagerPermission: [
                    "Permissions",
                    (
                        permissions: Permissions,
                    ): boolean => {
                        if (permissions.canShow("EnableDataSubjectsElementsV2")) return true;
                        permissions.goToFallback();
                        return false;
                    },
                ],
            },
        })
        .state("zen.app.pia.module.datamap.views.inventory-manager.home", {
            url: "/",
            template: "<inventory-list-manager-entry></inventory-list-manager-entry>",
        })
        .state("zen.app.pia.module.datamap.views.inventory-manager.list", {
            url: "/:listId",
            template: "<inventory-list-manager></inventory-list-manager>",
            resolve: {
                DataMappingListManagerPermission: [
                    "Permissions",
                    (
                        permissions: Permissions,
                    ): boolean => {
                        if (permissions.canShow("EnableDataSubjectsElementsV2")) return true;
                        permissions.goToFallback();
                        return false;
                    },
                ],
            },
        })
        .state("zen.app.pia.module.datamap.views.inventory-manager.details", {
            url: "/:listId/:itemId/:tabId",
            params: {
                time: null,
                tabId: { dynamic: true, value: InventoryListDetailsTabs.List },
            },
            template: "<inventory-list-manager-details></inventory-list-manager-details>",
            resolve: {
                DataMappingListManagerPermission: [
                    "Permissions",
                    (
                        permissions: Permissions,
                    ): boolean => {
                        if (permissions.canShow("EnableDataSubjectsElementsV2")) return true;
                        permissions.goToFallback();
                        return false;
                    },
                ],
            },
        })
        .state("zen.app.pia.module.datamap.views.data-discovery", {
            abstract: true,
            url: "data-discovery",
            template: "<ui-view></ui-view>",
            resolve: {
                DataDiscoveryPermission: [
                    "Permissions",
                    (
                        permissions: Permissions,
                    ): boolean => {
                        if (permissions.canShow("IntegrationsDataDiscovery")) return true;
                        permissions.goToFallback();
                        return false;
                    },
                ],
            },
        })
        .state("zen.app.pia.module.datamap.views.data-discovery.home", {
            url: "",
            template: "<downgrade-data-discovery></downgrade-data-discovery>",
            resolve: {
                DataMappingDataDiscoveryPermission: [
                    "Permissions",
                    (
                        permissions: Permissions,
                    ): boolean => {
                        if (permissions.canShow("IntegrationsDataDiscovery")) return true;
                        permissions.goToFallback();
                        return false;
                    },
                ],
            },
        })
        .state("zen.app.pia.module.datamap.views.custom-dashboards", {
            url: "custom-dashboards",
            params: { time: null },
            template: "<dashboard-switcher [module-type]='moduleType' class='flex-full-height full-size'></dashboard-switcher>",
            controller: ["$scope", ($scope) => $scope.moduleType = ModuleTypes.INV],
            resolve: {
                DataMappingCustomDashboardPermission: ["Permissions",
                    (permissions: Permissions): boolean => {
                        if (permissions.canShow("CustomDashboards") && permissions.canShow("ViewDMCustomDashboards")) return true;
                        permissions.goToFallback();
                        return false;
                    },
                ],
            },
        });

}

routes.$inject = ["$stateProvider", "$urlRouterProvider"];
