import * as _ from "lodash";

export default function TemplateStudio(): any {
    return {
        isTypeDisabled: ($scope: any, Permissions: any, questionType: number): boolean => {
            const isArray: boolean = _.isArray($scope.LockedQuestionTypes);
            const questionTypeExists: boolean = $scope.LockedQuestionTypes.indexOf(questionType) > -1;
            const permissions: any = {
                TemplatesQuestionAdd: Permissions.canShow("TemplatesQuestionAdd"),
            };
            if (permissions.TemplatesQuestionAdd) {
                switch (questionType) {
                    case 1:
                        return isArray && questionTypeExists;
                    case 2:
                        return isArray && questionTypeExists;
                    case 4:
                        return isArray && questionTypeExists;
                    case 5:
                        return isArray && questionTypeExists;
                    default:
                        return isArray && questionTypeExists;
                }
            } else {
                return false;
            }
        },
    };
}
