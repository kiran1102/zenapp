import * as _ from "lodash";

export default function GenFactory(): any {
    return {
        isEven: (num: number): boolean => {
            return (num % 2 === 0) ? true : false;
        },
        objLen: (obj: {}): number => {
            return Object.keys(obj).length;
        },
        removeNulls: (array: any[]): any[] => {
            if (_.isArray(array)) {
                let hasNulls = true;
                while (hasNulls) {
                    const nIndex = array.indexOf(null);
                    if (nIndex > -1) {
                        array.splice(nIndex, 1);
                    } else {
                        hasNulls = false;
                        break;
                    }
                }
            } else {
                return [];
            }
            return array;
        },
        sortOrder: (a: number, b: number, sortReverse: boolean): number => {
            if (sortReverse) return a > b ? 1 : -1;
            return a > b ? -1 : 1;
        },
    };
}
