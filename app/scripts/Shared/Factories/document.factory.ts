export default function DocumentFactory(): any {
    return {
        fontPicker: (extension: string): string => {
            switch (extension) {
                case "TXT":
                    return "fa fa-file-text-o";
                case "ZIP":
                    return "fa fa-file-zip-o";
                case "CODE":
                case "HTML":
                case "JS":
                case "CSS":
                case "JSON":
                case "JAVA":
                case "XML":
                case "CSHTML":
                case "CSPROJ":
                case "SLN":
                case "CMD":
                case "SH":
                case "SWIFT":
                    return "fa fa-file-code-o";
                case "PDF":
                    return "fa fa-file-pdf-o";
                case "AUD":
                    return "fa fa-file-audio-o";
                case "VID":
                    return "fa fa-file-video-o";
                case "GIF":
                case "JPG":
                case "JPEG":
                case "PNG":
                    return "fa fa-file-image-o";
                case "DOC":
                case "DOCX":
                    return "fa fa-file-word-o";
                case "PPT":
                case "PPTX":
                    return "fa fa-file-powerpoint-o";
                case "XSL":
                case "XSLX":
                    return "fa fa-file-excel-o";
                default:
                    return "fa fa-file-o";
            }
        },
    };
}
