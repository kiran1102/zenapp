import * as _ from "lodash";
import { ProtocolService } from "modules/core/services/protocol.service";

import { IRiskSettings } from "interfaces/setting.interface";
import { IProtocolPacket, IProtocolConfig, IProtocolMessages } from "interfaces/protocol.interface";
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";

export default class HeatmapSettingsService {

    static $inject: string[] = ["$rootScope", "OneProtocol"];
    private translations: any;

    constructor(
        private readonly $rootScope: IExtendedRootScopeService,
        private readonly OneProtocol: ProtocolService,
    ) {}

    public getDefault(): IRiskSettings {
        return {
            Enabled: true,
            GraphSettings: {
                Axes: [
                    {
                        Label: "Probability",
                        Points: [
                            {
                                Label: "Low",
                                Value: 1,
                            },
                            {
                                Label: "Medium",
                                Value: 2,
                            },
                            {
                                Label: "High",
                                Value: 3,
                            },
                            {
                                Label: "Very High",
                                Value: 4,
                            },
                        ],
                    },
                    {
                        Label: "Impact",
                        Points: [
                            {
                                Label: "Low",
                                Value: 1,
                            },
                            {
                                Label: "Medium",
                                Value: 2,
                            },
                            {
                                Label: "High",
                                Value: 3,
                            },
                            {
                                Label: "Very High",
                                Value: 4,
                            },
                        ],
                    },
                ],
            },
            RiskMap: [
                {
                    Axis1Value: 1,
                    Axis2Value: 4,
                    RiskLevel: 3,
                }, {
                    Axis1Value: 2,
                    Axis2Value: 4,
                    RiskLevel: 3,
                }, {
                    Axis1Value: 3,
                    Axis2Value: 4,
                    RiskLevel: 4,
                }, {
                    Axis1Value: 4,
                    Axis2Value: 4,
                    RiskLevel: 4,
                }, {
                    Axis1Value: 1,
                    Axis2Value: 3,
                    RiskLevel: 2,
                }, {
                    Axis1Value: 2,
                    Axis2Value: 3,
                    RiskLevel: 3,
                }, {
                    Axis1Value: 3,
                    Axis2Value: 3,
                    RiskLevel: 3,
                }, {
                    Axis1Value: 4,
                    Axis2Value: 3,
                    RiskLevel: 4,
                }, {
                    Axis1Value: 1,
                    Axis2Value: 2,
                    RiskLevel: 2,
                }, {
                    Axis1Value: 2,
                    Axis2Value: 2,
                    RiskLevel: 2,
                }, {
                    Axis1Value: 3,
                    Axis2Value: 2,
                    RiskLevel: 3,
                }, {
                    Axis1Value: 4,
                    Axis2Value: 2,
                    RiskLevel: 3,
                }, {
                    Axis1Value: 1,
                    Axis2Value: 1,
                    RiskLevel: 1,
                }, {
                    Axis1Value: 2,
                    Axis2Value: 1,
                    RiskLevel: 2,
                }, {
                    Axis1Value: 3,
                    Axis2Value: 1,
                    RiskLevel: 2,
                }, {
                    Axis1Value: 4,
                    Axis2Value: 1,
                    RiskLevel: 3,
                },
            ],
        };
    }

    public setHeatmap(heatmap: IRiskSettings): ng.IPromise<IProtocolPacket> {
        const config: IProtocolConfig = this.OneProtocol.config("POST", `/HeatMapSettings/Update`, [], heatmap);
        const messages: IProtocolMessages = {
            Success: { custom: this.$rootScope.t("SettingsHaveBeenSaved") },
            Error: { custom: this.$rootScope.t("SorryThereWasProblemSavingSettings") },
        };
        return this.OneProtocol.http(config, messages);
    }

    public getHeatmap(): ng.IPromise<IProtocolPacket> {
        const config: IProtocolConfig = this.OneProtocol.config("GET", `/HeatMapSettings/Get`);
        const messages: IProtocolMessages = {Error: { custom: this.$rootScope.t("SorryThereWasProblemGettingHeatmap") }};
        return this.OneProtocol.http(config, messages).then((response: IProtocolPacket): IProtocolPacket => {
            const defaultSettings: IRiskSettings = this.getDefault();
            if (response.data !== null && response.data !== "") {
                if (!(_.isArray(response.data.RiskMap) && response.data.RiskMap.length)) {
                    response.data.RiskMap = defaultSettings.RiskMap;
                }
                return {
                    result: true,
                    data: response.data,
                };
            } else {
                return {
                    result: true,
                    data: defaultSettings,
                };
            }
        }, () => {
            return {
                result: true,
                data: this.getDefault(),
            };
        });
    }
}
