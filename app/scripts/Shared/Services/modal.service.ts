// 3rd Party
import {
    keys,
    isUndefined,
    endsWith,
    assign,
} from "lodash";

// Reducer
import { getModalStatus, getModalName, getModalData, ModalActions } from "oneRedux/reducers/modal.reducer";

// Interfaces
import { IStore } from "interfaces/redux.interface";
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import { IStringMap } from "interfaces/generic.interface";
import { IModalConfig } from "interfaces/modal.interface";

export class ModalService {

    static $inject: string[] = ["store", "$uibModal", "$uibModalStack", "$rootScope", "GenFactory"];

    private modalConfigurations: IStringMap<IModalConfig> = {
        default: { size: "md", backdrop: "static", keyboard: false },
        downgradeEditTemplateRuleModal: { size: "lg", backdrop: "static", keyboard: false, windowClass: "template-rule-modal" },
        downgradeAAAttachmentsModal: { size: "lg", backdrop: "static", keyboard: false, windowClass: "aa-attachments-modal" },
        downgradeAssessmentDetailRelatedDataSubjectModal: { size: "full-height", backdrop: "static", keyboard: false, windowClass: "relate-data-subject-modal" },
        downgradeTemplateConditionsModal: { size: "lg", backdrop: "static", keyboard: false, windowClass: "template-condition-modal" },
        downgradeTemplateRelatedInventoryModal: { size: "md", backdrop: "static", keyboard: false, windowClass: "template-related-inventory-modal" },
        downgradeTemplateQuestionBuilderModal: { size: "lg", backdrop: "static", keyboard: false, windowClass: "question-builder-modal" },
        templateAttributeQuestionBuilderModal: { size: "lg", backdrop: "static", keyboard: false, windowClass: "attribute-question-modal" },
        scanBehindTutorial: { size: "lg", backdrop: "static", keyboard: false, windowClass: "scan-behind-tutorial" },
        relateDataSubjectModal: { size: "full-height", backdrop: "static", keyboard: false, windowClass: "relate-data-subject-modal" },
        linkDataSubjectModal: { size: "full-height", backdrop: "static", keyboard: false, windowClass: "link-data-subject-modal" },
        attributeManagerModal: { size: "lg", backdrop: "static", keyboard: false, windowClass: "attribute-manager-modal" },
        downgradeAaNeedsMoreInfoModal: { size: "md", backdrop: "static", keyboard: false, windowClass: "aa-needs-more-info-modal" },
        removeCookieModal: { size: "md", backdrop: "static", keyboard: false, windowClass: "remove-cookie-modal" },
        cookieDeleteConfirmationComponent: { size: "md", backdrop: "static", keyboard: false, windowClass: "cookie-delete-confirmation-modal" },
        addCustomCookieModal: { size: "md", backdrop: "static", keyboard: false, windowClass: "add-custom-cookie-modal" },
        downgradeEditTemplateWelcomeMessage: { size: "lg", backdrop: "static", keyboard: false, windowClass: "edit-template-welcome-message-modal" },
        createPreferenceCenterModal: { size: "md", backdrop: "static", keyboard: false, windowClass: "cr-create-preference-center-modal" },
        downgradeAssessmentActivityModal: { size: "lg", backdrop: "static", keyboard: false, windowClass: "assessment-activity-modal" },
        downgradeAAQuestionActivityStream: { size: "lg", backdrop: "static", keyboard: false, windowClass: "aa-question-activity" },
        columnSelectorModal: { size: "lg", backdrop: "static", keyboard: false, windowClass: "column-selector-modal"},
        attributePickerModal: { size: "lg", backdrop: "static", keyboard: false, windowClass: "attribute-picker-modal"},
        eulaModal: { size: "lg", backdrop: "static", keyboard: false, windowClass: "eula-modal"},
        copyTemplateModal: { size: "md copy-template-modal", backdrop: "static", keyboard: false },
        userGroupsAddUsersModal: { size: "lg", backdrop: "static", keyboard: false, windowClass: "user-groups-add-users-modal"},
        downgradeCookiePublishScriptModal: { size: "lg", backdrop: "static", keyboard: false, windowClass: "cookie-publish-script-modal"},
        columnPickerModal: { size: "lg", backdrop: "static", keyboard: false, windowClass: "column-picker-modal" },
        scheduleAuditModal: { size: "md", backdrop: "static", keyboard: false, windowClass: "cc-schedule-audit-modal" },
        OrgGroupReAssignModal: { size: "md", backdrop: "static", keyboard: false, windowClass: "org-group-re-assign-modal" },
        addVendorListModal: { size: "md", backdrop: "static", keyboard: false, windowClass: "cc-add-vendor-list-modal" },
        downgradeRestoreScriptModal: { size: "lg", backdrop: "static", keyboard: false, windowClass: "cookie-restore-script-modal"},
        downgradeAAMetadataModal: { size: "lg", backdrop: "static", keyboard: false, windowClass: "aa-metadata-modal" },
        downgradeConsentPolicyStatusModal: { size: "md", backdrop: "static", keyboard: false, windowClass: "consent-status-modal" },
        downgradeShareAssessmentModal: { size: "lg", backdrop: "static", keyboard: false, windowClass: "user-groups-add-users-modal"},
        downgradeAAUnapprovalModal: { size: "md", backdrop: "static", keyboard: false, windowClass: "aa-unapproval-modal" },
        cookieLimitModal: { size: "md", backdrop: "static", keyboard: false, windowClass: "cookie-limit-modal" },
    };

    constructor(
        private store: IStore,
        private $uibModal: any,
        private $uibModalStack: any,
        private $rootScope: IExtendedRootScopeService,
        private GenFactory: any,
    ) {
        this.store.subscribe(() => this.handleStoreUpdate());
    }

    public renderModal(data: any, controller: string, template: string, modalType?: string, modalSize?: string, modalBackdrop?: string) {
        const resolve = {};
        for (const key in data) {
            if (data.hasOwnProperty(key)) {
                resolve[key] = (): any => {
                    return data[key];
                };
            }
        }

        const modalConfig: IModalConfig = {
            controller,
            size: !isUndefined(modalSize) ? modalSize : "",
            backdrop: !isUndefined(modalBackdrop) ? modalBackdrop : "static",
            keyboard: false,
            resolve,
            windowClass: "one-modal__wrapper",
        };
        modalConfig[(endsWith(template, ".html") ? "templateUrl" : "template")] = template;

        return this.$uibModal.open(modalConfig).result.then((res: any): void | any => {
            if (!isUndefined(res)) {
                if (res.result || this.GenFactory.objLen(res)) {
                    switch (modalType) {
                        case "analysisModal":
                            this.$rootScope.$broadcast("analysisModalCb", { cbInitiated: true });
                            break;
                        case "commentsModal":
                            this.$rootScope.$broadcast("commentsModalCb", { response: res });
                            break;
                        case "attachmentModal":
                            this.$rootScope.$broadcast("attachmentModalCb", { response: res });
                            break;
                        case "deadlinesModal":
                            this.$rootScope.$broadcast("deadlinesModalCb", { cbInitiated: true, response: res });
                            break;
                        case "nextStepsModal":
                            this.$rootScope.$broadcast("nextStepsModalCb", { response: res });
                            break;
                        case "projectReviewerModal":
                            this.$rootScope.$broadcast("projectReviewerModalCb", { cbInitiated: true, response: res });
                            break;
                        case "sendBackModal":
                            this.$rootScope.$broadcast("sendBackModalCb", { response: res });
                            break;
                        case "selectIconModal":
                            this.$rootScope.$broadcast("selectIconModalCb", { response: res });
                            break;
                        case "userPreferencesModal":
                            this.$rootScope.$broadcast("userPreferencesModalCb", { cbInitiated: true });
                            break;
                        case "welcomeTextModal":
                            this.$rootScope.$broadcast("welcomeTextModalCb", { response: res });
                            break;
                        case "projectAssignmentModal":
                            this.$rootScope.$broadcast("projectAssignmentModalCb", { response: res });
                            break;
                        case "certificateUploadModal":
                            this.$rootScope.$broadcast("certificateUploadCb", { response: res });
                            break;
                        case "bulkCreateAssetsModalCb":
                            this.$rootScope.$broadcast("bulkCreateAssetsModalCb", { response: res });
                            break;
                        case "questionModalCb":
                            this.$rootScope.$broadcast("questionModalCb", { response: res });
                            break;
                        case "conditionsModalCb":
                            this.$rootScope.$broadcast("conditionsModalCb", { response: res });
                            break;
                        case "reassignmentModalCb":
                            this.$rootScope.$broadcast("reassignmentModalCb", { response: res });
                            break;
                        case "approverModalCb":
                            this.$rootScope.$broadcast("approverModalCb", { response: res });
                            break;
                        case "shareProjectModal":
                            this.$rootScope.$broadcast("shareProjectModalCb", { cbInitiated: true });
                            break;
                        case "ccLanguageModalCb":
                            this.$rootScope.$broadcast("ccLanguageModalCb", { response: res });
                            break;
                        case "ccRemoveOldCookiesModalCb":
                            this.$rootScope.$broadcast("ccRemoveOldCookiesModalCb", { response: res });
                            break;
                        default:
                            return res;
                    }
                }
            }
        }, () => {
            // Need this to remove Possibly unhandled rejection: cancel error log
        });
    }

    public renderUibModal(modalName: string, modalData) {
        return this.$uibModal.open(
            this.buildModalConfig(modalName, modalData),
        ).result.then(
            (res: any): any => res,
            (): void => {/* swallow error */},
        );
    }

    public openModal(modalName: string): void {
        this.store.dispatch({ type: ModalActions.OPEN_MODAL, modalName });
    }

    public closeModal(): void {
        this.store.dispatch({ type: ModalActions.CLOSE_MODAL });
    }

    public setModalData(modalData: any): void {
        this.store.dispatch({ type: ModalActions.SET_MODAL_DATA, modalData });
    }

    public updateModalData(modalDataToMerge: any): void {
        this.store.dispatch({ type: ModalActions.UPDATE_MODAL_DATA, modalDataToMerge });
    }

    public clearModalData(): void {
        this.store.dispatch({ type: ModalActions.CLEAR_MODAL_DATA });
    }

    public handleModalCallback(...args: any[]): void | undefined {
        const modalData = getModalData(this.store.getState());
        const callback = modalData ? modalData.callback : null;
        if (!callback) return;
        else if (args.length) {
            callback.apply(null, args);
        } else {
            callback();
        }
    }

    private getDefaultUibModalConfig(modalName: string) {
        return this.modalConfigurations[modalName] || this.modalConfigurations.default;
    }

    private wrapModalDataWithFunctions(modalData) {
        return {
            resolve: keys(modalData)
                .reduce((acc, curr) => {
                    acc[curr] = () => modalData[curr];
                    return acc;
                }, {}),
        };
    }

    private buildModalConfig(modalName: string, modalData) {
        return assign({ component: modalName }, this.getDefaultUibModalConfig(modalName), this.wrapModalDataWithFunctions(modalData));
    }

    private getTopModalInstance() {
        return this.$uibModalStack.getTop() ? this.$uibModalStack.getTop().key : null;
    }

    private handleStoreUpdate(): void {
        const modalKey = this.getTopModalInstance();
        const modalStatus: string = getModalStatus(this.store.getState());

        if (modalStatus === "opened") {
            if (!modalKey) {
                this.renderUibModal(getModalName(this.store.getState()), getModalData(this.store.getState()));
            }
        } else if (modalKey && modalStatus === "closed") {
            this.$uibModalStack.close(modalKey);
        }
    }
}

export default ModalService;
