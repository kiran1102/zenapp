
function TreeMap(id, parentId, nodes, rootNode) {
    if (_.isString(id) && _.isString(parentId) && _.isString(nodes)) {
        var self = this;

        // Define Keys
        this.Id = id;
        this.ParentId = parentId;
        this.Nodes = nodes;

        // Define Root and Dictionary
        this.Root = rootNode[this.Id];
        this._tree = [];
        this._tree[this.Root] = null;
        this._treeIndex = [this.Root];
        this._tree.length++;

        this._nodeTree = [];
        this._nodeTree[this.Root] = rootNode;
        this._nodeTreeIndex = [this.Root];
        this._nodeTree.length++;
        this.GetTree = function() {
            return self._tree;
        };

        this.GetRoot = function() {
            return self.Root;
        };

        this.GetRootArr = function() {
            return [self.Node(self.Root)];
        };

        this.Add = function(id, parentId) {
            if (_.isUndefined(self._tree[id])) {
                self._tree[id] = parentId;
            } else {
                self._tree.length++;
            }
        };

        this.AddNode = function(id, node) {
            if (!_.isUndefined(self._nodeTree[id])) {
                self._nodeTree[id] = node;
            } else {
                self._nodeTree[id] = node;
                self._nodeTree.length++;
            }
        };

        this.PrintTree = function() {
            var string = "";
            _.each(self._tree, function(n, i) {
                string += (n+", ");
            });

            return "[ " + string + " ]";
        };

        this.PrintNodeTree = function() {
            var string = "";
            _.each(self._nodeTree, function(n, i) {
                string += (angular.toJson(n) + ", \n");
            });

            return "[ \n" + string + "\n ]";
        };

        this.Size = function() {
            return self._tree.length;
        };

        this.Parent = function(id) {
            return self._tree[id];
        };

        this.Node = function(id) {
            return self._nodeTree[id];
        };

        this.RecursiveAdd = function(arr) {
            var nodesAdded = 0;
            if (_.isArray(arr)) {
                _.each(arr, function(n, i) {
                    self.Add(n[self.Id], n[self.ParentId]);
                    self.AddNode(n[id], n);
                    nodesAdded += self.RecursiveAdd(n[self.Nodes]);
                });
            }
            return nodesAdded + 1;
        };

        this.IsInParentTree = function(parentId, childId) {
            if (parentId === childId) {
                return true;
            }

            var currentId = childId;
            var currentParentId = null;

            var result = false;

            do {
                currentParentId = self.Parent(currentId);

                if (currentParentId == parentId) {
                    return true;
                } else if (!_.isString(currentParentId)) {
                    return false;
                } else {
                    currentId = currentParentId;
                }
                result = (currentParentId !== null);
            }
            while (result);
        };

        this.flatten = function() {
            return this.recursivelyFlatten(this.Node(this.Root));
        };

        this.recursivelyFlatten = function(node) {
            var flattenedTree = [_.omit(node, this.Nodes)];
            if (node[this.Nodes]) {
                var self = this;
                _.each(node[this.Nodes], function(node) {
                    flattenedTree = flattenedTree.concat(self.recursivelyFlatten(node));
                });
            }
            return flattenedTree;
        };

        // Add Nodes to the Dictionary Recursively
        var nodesAdded = this.RecursiveAdd(rootNode[this.Nodes]);
    }
}

export default TreeMap;
