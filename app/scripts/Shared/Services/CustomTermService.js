
"use strict";

export default function CustomTermService($http, NotificationService, $rootScope, $localStorage, CONSTANTS, ProtocolService) {

    function _setCustomTermsToStorage(customTerms) {
        $localStorage.customTerms = customTerms;
        return true;
    }

    function _addDefaultCustomTermsToRootScope() {
        var compiledCustomTerms = {};

        _.each(Object.keys(CONSTANTS.TERMS), function(term) {
            compiledCustomTerms[term] = term;
        });

        $rootScope.customTerms = compiledCustomTerms;
        _setCustomTermsToStorage(compiledCustomTerms);
    }

    function getCustomTermByTenatId(tenantId, language) {
        const config = ProtocolService.customConfig("GET", "/api/globalization/v1/customTerms/" + language);
        const messages = { Error: { custom: this.$rootScope.t("SorryProblemGettingTranslations") }};
        return ProtocolService.http(config, messages);
    }

    //todo remove this method once we have the localization framework set up
    function addCustomTermsToRootScope(tenantId, language) {

        if (!tenantId) {
            _addDefaultCustomTermsToRootScope();
        } else {
            this.getCustomTermByTenatId(tenantId, language).then(
                function (response) {
                    var compiledCustomTerms = null;
                    if (response.result && response.data) {
                        compiledCustomTerms = {};
                        _.forEach(response.data, function (term) {
                            compiledCustomTerms[term.TermKey] = term.CustomTermName || term.defaultTermName;
                        });
                    }
                    $rootScope.customTerms = compiledCustomTerms;
                    _setCustomTermsToStorage(compiledCustomTerms);
                },
                function () {
                    _addDefaultCustomTermsToRootScope();
                }
            );

        }
    }

    function saveCustomTerm(term, reset) {
        var action = reset ? "reset" : "saved";
        const config = ProtocolService.customConfig("PUT", "/api/globalization/v1/customTerms", null, term);
        const messages = {
            Error: { custom: this.$rootScope.t("SorryProblemGettingTranslations") },
            Success: { custom: "Your custom term has been " + action + "."},
         };
        return ProtocolService.http(config, messages);

    }

    function getCustomTermsFromStorage() {
        var customTerms = $localStorage.customTerms ? $localStorage.customTerms : null;
        return customTerms;
    }

    return {
        getCustomTermByTenatId: getCustomTermByTenatId,
        addCustomTermsToRootScope: addCustomTermsToRootScope,
        saveCustomTerm: saveCustomTerm
    };

}

CustomTermService.$inject = ["$http", "NotificationService", "$rootScope", "$localStorage", "CONSTANTS", "ProtocolService"];
