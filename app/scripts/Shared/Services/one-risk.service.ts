import { AssessmentStates, AssessmentStateKeys } from "constants/assessment.constants";
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import * as _ from "lodash";
import { RiskLevels } from "enums/risk.enum";
import { IPiaAssessmentSelfService } from "interfaces/assessment.interface";

export class OneRisk {
    static $inject: string[] = ["$rootScope"];

    public description: string;
    public recommendation: string;
    public translate: any;
    public customTerms: any;

    constructor($rootScope: IExtendedRootScopeService) {
        this.description = undefined;
        this.recommendation = undefined;
        this.translate = $rootScope.t;
        this.customTerms = $rootScope.customTerms;
    }

    public riskCounter(project: any): any {
        project.lowRiskCount = 0;
        project.mediumRiskCount = 0;
        project.highRiskCount = 0;
        project.veryHighRiskCount = 0;
        project.totalRiskCount = 0;

        _.each(project.Sections, (section: any) => {
            _.each(section.Questions, (question: any) => {
                if (!question.Risk) return;

                const risk: any = question.Risk;

                switch (risk.Level) {
                    case RiskLevels.Low:
                        project.lowRiskCount += 1;
                        project.totalRiskCount += 1;
                        break;
                    case RiskLevels.Medium:
                        project.mediumRiskCount += 1;
                        project.totalRiskCount += 1;
                        break;
                    case RiskLevels.High:
                        project.highRiskCount += 1;
                        project.totalRiskCount += 1;
                        break;
                    case RiskLevels.VeryHigh:
                        project.veryHighRiskCount += 1;
                        project.totalRiskCount += 1;
                        break;
                }
            });
        });

        return project;
    }

    public applyStateLabels(project: IPiaAssessmentSelfService, projectState: string, scope?: any): IPiaAssessmentSelfService {
        const { InProgress, UnderReview, InfoNeeded, RiskTreatment, RiskAssessment, Completed } = AssessmentStateKeys;
        switch (projectState) {
            case AssessmentStates.InProgress:
                project.StateLabel = "in-progress";
                project.StateName = this.translate(InProgress);
                return project;
            case AssessmentStates.UnderReview:
                project.StateLabel = "under-review";
                project.StateName = this.translate(UnderReview);
                return project;
            case AssessmentStates.InfoNeeded:
                project.StateLabel = "needs-info";
                project.StateName = this.translate(InfoNeeded);
                return project;
            case AssessmentStates.RiskTreatment:
                project.StateLabel = "risk-treatment";
                project.StateName = this.translate(RiskTreatment);
                return project;
            case AssessmentStates.RiskAssessment:
                project.StateLabel = "risk-assessment";
                project.StateName = this.translate(RiskAssessment);
                return project;
            case AssessmentStates.Completed:
                project.StateLabel = "completed";
                project.StateName = this.translate(Completed);
                return project;
        }
    }

    set riskDescription(riskDescription: string) {
        this.description = riskDescription;
    }

    get riskDescription(): string {
        return this.description;
    }

    set riskRecommendation(riskRecommendation: string) {
        this.recommendation = riskRecommendation;
    }

    get riskRecommendation(): string {
        return this.recommendation;
    }

}
