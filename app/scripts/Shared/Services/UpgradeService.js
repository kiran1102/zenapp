
export default function Upgrade($uibModal, UserActionService, Content, SupportService) {

    function GetContentData() {
        return {
            // Module_Key: {
            //     Name: 'Module_Name',
            //     UpgradeVideo: 'dQw4w9WgXcQ',
            // },
            Projects: {
                Name: "Projects",
                UpgradeVideo: Content.GetKey("TutorialProjects")
            },
            Templates: {
                Name: "Templates",
                UpgradeVideo: Content.GetKey("TutorialQuestionnaire")
            },
            Dashboard: {
                Name: "Dashboard",
                UpgradeVideo: Content.GetKey("TutorialDashboard")
            },
            Risks: {
                Name: "Risks",
                UpgradeVideo: Content.GetKey("TutorialRisks")
            },
            DataMapping: {
                Name: "Data Mapping",
                UpgradeVideo: Content.GetKey("TutorialDataMapping")
            },
            DSAR: {
                Name: "Data Subject Access Request",
                UpgradeVideo: Content.GetKey("TutorialDSAR")
            },
            Users: {
                Name: "Users",
                UpgradeVideo: Content.GetKey("TutorialUsers")
            },
            Organizations: {
                Name: "Organizations",
                UpgradeVideo: Content.GetKey("TutorialOrganizations")
            },
            Settings: {
                Name: "Settings",
                UpgradeVideo: Content.GetKey("TutorialSettings")
            },
            General: {
                Name: "OneTrust",
                UpgradeVideo: Content.GetKey("Tutorials")
            }
        };
    }

    return {
        ModuleStates: {
            Message: 1,
            Video: 2,
            Form: 3,
            Submitted: 4
        },

        GenericModule: function (name) {
            return {
                Name: name ? name : "",
                UpgradeVideo: ""
            };
        },

        ModuleData: null,

        GetModule: function (module) {
            if (!this.ModuleData) {
                this.ModuleData = GetContentData();
            }

            if (module && this.ModuleData.hasOwnProperty(module)) {
                return this.ModuleData[module];
            }
            return this.GenericModule(module);
        },

        showModal: function (options) {
            var modalInstance = $uibModal.open({
                templateUrl: "UpgradeModal.html",
                controller: "UpgradeModalCtrl",
                keyboard: false,
                resolve: {
                    Options: options,
                    currentUser: UserActionService.fetchCurrentUser().then(function (result) {
                        return result.data;
                    }),
                    OneContent: function () {
                        return Content;
                    }
                }
            });
        },

        sendUpgradeForm: function (data) {
            return SupportService.upgrade(data);
        }
    };
}

Upgrade.$inject = ["$uibModal", "UserActionService", "Content", "SupportService"];
