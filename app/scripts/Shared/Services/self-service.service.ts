import { ProtocolService } from "modules/core/services/protocol.service";
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import { ISelfServiceConfig } from "interfaces/self-service.interface";
import {
    IProtocolConfig,
    IProtocolMessages,
    IProtocolPacket,
} from "interfaces/protocol.interface";

export default class SelfService {

    static $inject: string[] = ["$rootScope", "OneProtocol"];

    constructor(
        private readonly $rootScope: IExtendedRootScopeService,
        private readonly OneProtocol: ProtocolService,
    ) { }

    public readConfiguration = (params: ISelfServiceConfig): ng.IPromise<IProtocolPacket> => {
        const id: string = params.TemplateId;
        const version: number = params.TemplateVersion;
        params.method = "GET";
        params.url = `/template/getselfserviceregistration/${id}/${version}`;
        return this.crudConfiguration(params);
    }

    public updateConfiguration = (params: ISelfServiceConfig): ng.IPromise<IProtocolPacket> => {
        params.method = "POST";
        params.url = "/template/registerselfservice";
        return this.crudConfiguration(params);
    }

    public deleteConfiguration = (params: ISelfServiceConfig): ng.IPromise<IProtocolPacket> => {
        const id: string = params.TemplateId;
        const version: number = params.TemplateVersion;
        params.method = "DELETE";
        params.url = `/template/deregisterselfservice/${id}/${version}`;
        return this.crudConfiguration(params);
    }

    private crudConfiguration = (params: ISelfServiceConfig): ng.IPromise<IProtocolPacket> => {
        const config: IProtocolConfig = this.OneProtocol.config(
            params.method,
            params.url,
            [],
            params,
        );
        const messages: IProtocolMessages = {
            Error: {
                object: this.$rootScope.t("SelfService"),
                action: params.method.toLowerCase(),
            },
        };
        return this.OneProtocol.http(config, messages);
    }
}
