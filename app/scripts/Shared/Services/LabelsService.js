import { AssessmentStateKeys } from "constants/assessment.constants";

export default function Labels($rootScope, ENUMS) {

    var getLabels = function (module, type, properties) {
        properties = properties || {};

        var terms = {
            default: {
                singular: $rootScope.customTerms.Project,
                plural: $rootScope.customTerms.Projects
            },
            40: {
                singular: $rootScope.t("Assessment"),
                plural: $rootScope.t("Assessments")
            }
        };
        var term = (type && terms[type]) ? terms[type] : terms.default;

        var modules = {
            projectList: {
                project: term.singular,
                projects: term.plural,
                breadcrumb: term.plural,
                addButton: $rootScope.t("AddProjectsDuplicate", {ProjectTerm: term.plural}),
                filter: $rootScope.t("AllProjectsDuplicate", {ProjectTerm: term.plural}),
                loading: term.plural,
                noExisting: $rootScope.t("NoExistingProjectsDuplicate", {ProjectTerm: term.plural}),
                noExistingShared: $rootScope.t("NoProjectsHaveBeenShared", {ProjectTerm: term.plural}),
                createNew: $rootScope.t("CreateNewProjectUsingAddProjectButtonDuplicate", {ProjectTerm: term.singular}),
                noneNeedAttention: $rootScope.t("NoProjectsNeedYourAttentionDuplicate", {ProjectTerm: term.plural}),
                changeFilterOrAdd: $rootScope.t("ChangeFilterOrAddProjectDuplicate", {ProjectTerm: term.singular}),
                noneCompleted: $rootScope.t("NoCompletedProjectsDuplicate", {ProjectTerm: term.plural}),
                assignTooltip: $rootScope.t("AssignProjectDuplicate", {ProjectTerm: term.singular}),
                historyTooltip: $rootScope.t("ViewProjectHistoryDuplicate", {ProjectTerm: term.singular}),
                tagsTooltip: $rootScope.t("ModifyProjectTags", {ProjectTerm: term.singular}),
                pdfTooltip: $rootScope.t("ProjectDetailsPDFDuplicate", {ProjectTerm: term.singular}),
                deleteTooltip: $rootScope.t("DeleteProjectDuplicate", {ProjectTerm: term.singular}),
                versionSubmitTooltip: $rootScope.t("SubmitProjectBeforeCreatingNewVersionDuplicate", {ProjectTerm: term.singular}),
                filteringRecords: $rootScope.t("FilteringRecords", {ProjectTerm: term.plural})
            },
            project: {
                loadingMessage: term.singular,
                breadcrumb: term.singular,
                projectApprover: $rootScope.t("ProjectApproverDuplicate", {ProjectTerm: term.singular}),
                riskHeadline: $rootScope.t("ProjectRisksDuplicate", {ProjectTerm: term.singular}),
                approvedMessage: $rootScope.t("YouApprovedProjectEmailSentToTeamDuplicate", {ProjectTerm: term.singular}),
                readOnlyMessage: $rootScope.t("ThisProjectIsInReadOnlyModeDuplicate", {ProjectTerm: term.singular}),
                versionWillLock: $rootScope.t("NewProjectVersionWillLockCurrentDuplicate", {ProjectTerm: term.singular}),
                deadlinesHeadline: $rootScope.t("ProjectDeadlineDuplicate", {ProjectTerm: term.singular}),
                successModalMessage: function (reviewer, reviewerEmail, projectName) {
                    return $rootScope.t("ProjectSubmittedToReviewerEmailApproverDuplicate", {
                        ProjectTerm: term.singular,
                        Reviewer: "<strong>" + reviewer + "</strong>",
                        ReviewerEmail: "<a href='mailto:" + reviewerEmail + "?Subject=" + encodeURI(projectName) + "'>" + reviewerEmail + "</a>"
                    });
                },
                submitForReviewTooltip: $rootScope.t("SubmitProjectForReviewDuplicate", {ProjectTerm: term.singular}),
                sendBackTooltip: $rootScope.t("SendProjectBackToQuestionRespondentDuplicate", {ProjectTerm: term.singular}),
                noLongerAvailable: $rootScope.t("ProjectNoLongerAvailable", {ProjectTerm: term.singular})
            },
            projectWizard: {
                breadcrumb: term.plural,
                createHeadline: $rootScope.t("Create") + " " + term.singular,
                createVersionHeadline: $rootScope.t("Create") + " " + term.singular + " " + $rootScope.t("Version"),
                templateAssociation: $rootScope.t("ProjectAssociatedToLatestTemplateDuplicate", { ProjectTerm: term.singular, TemplateName : properties.templateName }),
                assignmentHeadline: $rootScope.t("ProjectAssignmentDuplicate", { ProjectTerm: term.singular }),
                cancelButtonTooltip: $rootScope.t("CancelAndReturnProjects", { ProjectTerm: term.plural }),
                previousButtonTooltip: $rootScope.t("ReturnToProjectInfo", { ProjectTerm: term.singular }),
                respondentTooltip: $rootScope.t("RespondentAssignmentTooltip", { ProjectTerm: term.singular }),
                assignProjectRespondents: $rootScope.t("AssignProjectRespondents", { ProjectTerm: term.singular }),
                enterProjectName:  $rootScope.t("EnterProjectNameDuplicate", { ProjectTerm: term.singular }),
                errorEnterProjectName: $rootScope.t("ErrorEnterProjectNameDuplicate", { ProjectTerm: term.singular }),
                exceedCharacterLengthName: $rootScope.t("ExceedCharacterLengthProjectNameDuplicate", { ProjectTerm: term.singular }),
                enterDescription: $rootScope.t("EnterProjectDescriptionDuplicate", { ProjectTerm: term.singular }),
                officerWillReview: $rootScope.t("PrivacyOfficerWillReviewProjectDuplicate", { ProjectTerm: term.singular, PrivacyOfficerName: properties.privacyOfficerName }),
                saveAndAssign: $rootScope.t("SaveAndAssignOnTheNextPage", { ProjectsTerm: term.plural }),
                projectTerm: term.singular
            },
            reports: {
                project: term.singular,
                projects: term.plural,
                noExisting: $rootScope.t("NoExistingProjectsDuplicate", {ProjectTerm: term.plural})
            },
            sendBackModal: {
                sendBackText: function (lead) {
                    return $rootScope.t("SendProjectNeedMoreInfoEmailToLeadDuplicate", {
                        ProjectTerm: term.singular,
                        Lead: lead
                    });
                }
            }

        };
        return modules[module];
    };

    var setStateLabel = function(assessmentStates, stateObj) {
        if (!assessmentStates || !stateObj) return;
        switch (stateObj.StateDesc) {
            case assessmentStates.InProgress:
                stateObj.StateLabel = "in-progress";
                stateObj.StateName = $rootScope.t(AssessmentStateKeys.InProgress);
                break;
            case assessmentStates.UnderReview:
                stateObj.StateLabel = "under-review";
                stateObj.StateName = $rootScope.t(AssessmentStateKeys.UnderReview);
                break;
            case assessmentStates.InfoNeeded:
                stateObj.StateLabel = "needs-info";
                stateObj.StateName = $rootScope.t(AssessmentStateKeys.InfoNeeded);
                break;
            case assessmentStates.RiskAssessment:
                stateObj.StateLabel = "risk-assessment";
                stateObj.StateName = $rootScope.t(AssessmentStateKeys.RiskAssessment);
                break;
            case assessmentStates.RiskTreatment:
                stateObj.StateLabel = "risk-treatment";
                stateObj.StateName = $rootScope.t(AssessmentStateKeys.RiskTreatment);
                break;
            case assessmentStates.Completed:
                stateObj.StateLabel = "completed";
                stateObj.StateName = $rootScope.t(AssessmentStateKeys.Completed);
                break;
            default:
                stateObj.StateLabel = "default";
                stateObj.StateName = stateObj.StateDesc;
        }
    };

    var service = {
        getLabels: getLabels,
        setStateLabel: setStateLabel
    };
    return service;
}

Labels.$inject = ["$rootScope", "ENUMS"];
