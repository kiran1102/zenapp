// 3rd Party
import { find } from "lodash";

// Interfaces
import { IProtocolPacket } from "interfaces/protocol.interface";
import { ILanguage } from "interfaces/localization.interface";
import {
    IProtocolResponse,
    IProtocolMessages,
    IProtocolConfig,
} from "interfaces/protocol.interface";
import {
    ITranslation,
    ITenantTranslations,
} from "interfaces/localization.interface";
import { IStringMap } from "interfaces/generic.interface";

// Constants
import { TranslationModules } from "constants/translation.constant";

// Rxjs
import { Observable } from "rxjs/Observable";

// Services
import { LocalizationApiService } from "settingsServices/apis/localization-api.service";
import { NotificationService } from "modules/shared/services/provider/notification.service";
import { ProtocolService } from "modules/core/services/protocol.service";
export class TenantTranslationService {

    static $inject: string[] = [
        "$http",
        "$rootScope",
        "$localStorage",
        "CONSTANTS",
        "$q",
        "LocalizationApiService",
        "NotificationService",
        "OneProtocol",
    ];

    private _loadedModules: IStringMap<boolean> = {};

    constructor(
        private readonly $http: ng.IHttpService,
        private readonly $rootScope: any,
        private readonly $localStorage: any,
        private readonly CONSTANTS: any,
        private readonly $q: ng.IQService,
        private readonly localizationApiService: LocalizationApiService,
        private notificationService: NotificationService,
        private readonly OneProtocol: ProtocolService,
    ) {
        this.$rootScope.translations = {};
        this.$rootScope.customTerms = {};
    }

    getLanguages(tenantId?: number): ILanguage[] {
        return this.$rootScope.languages;
    }

    setLanguages(languages: ILanguage[]) {
        this.$rootScope.languages = languages;
    }

    addLanguagesToRootScope(): ng.IPromise<{}> {
        const deferred = this.$q.defer();
        this._getLanguages()
            .then(
                (response: IProtocolPacket) => {
                if (response.result && response.data) {
                    this.setLanguages(response.data);
                    deferred.resolve({
                        result: true,
                    });
                }
            },
            (response: any) => {
                this.notificationService.alertError(this.$rootScope.t("Error"),
                    this.$rootScope.translations && this.$rootScope.translations["SorryProblemGettingTranslations"] ?
                        this.$rootScope.t("SorryProblemGettingTranslations")
                        : "Sorry, there was a problem getting translations.");
                deferred.resolve({
                    result: false,
                });
            });
        return deferred.promise;
    }

    getCurrentLanguage(): string {
        return JSON.parse(localStorage.getItem("OneTrust.languageSelected")) || "en-us";
    }

    setTenantLanguageToRootScope(language: string): void {
        /* App will be using tenant Language for fetching translations for user*/
        this.$rootScope.tenantLanguage = language;
    }

    addSystemTranslations(tenantId: string, language: string): ng.IPromise<{}> {
        const deferred = this.$q.defer();
        if (!tenantId) {
            this._addDefaultCustomTermsToRootScope(language);
            deferred.resolve({ result: true });
        } else {
            this.getSystemTranslations(tenantId, language).then((response: IProtocolResponse<ITenantTranslations>) => {
                deferred.resolve(response);
            });
        }
        return deferred.promise;
    }

    addModuleTranslations(module: string): ng.IPromise<boolean> {
        return this.localizationApiService.getTranslationsByModule(module, this.getCurrentLanguage()).then((response: IProtocolResponse<ITenantTranslations>): boolean => {
            if (!response.result || !response.data) return false;
            this.addIndividualTranslations(response.data.Translations);
            this._loadedModules[module] = true;
            return true;
        });
    }

    getCustomTerms(tenantId: string, language: string): any {
        const config: IProtocolConfig = this.OneProtocol.customConfig("GET", `/api/globalization/v1/customTerms/${language}`);
        const messages: IProtocolMessages = { Error: { custom: this.$rootScope.t("SorryProblemGettingTranslations") } };
        return this.OneProtocol.http(config, messages);
    }

    saveCustomTerm(term: any, reset: any): any {
        const alertText = reset
            ? this.$rootScope.t("YourCustomTermHasBeenReset")
            : this.$rootScope.t("YourCustomTermHasBeenSaved");
        const config: IProtocolConfig = this.OneProtocol.customConfig("PUT", "/api/globalization/v1/customTerms", null, term);
        const messages: IProtocolMessages = {
            Error: { custom: this.$rootScope.t("SorryProblemGettingTranslations") },
            Success: { custom: alertText},
         };
        return this.OneProtocol.http(config, messages);
    }

    addIndividualTranslations(translationMap: IStringMap<string>): void {
        this.$rootScope.translations = { ...this.$rootScope.translations, ...translationMap };
        this.$rootScope.customTerms = { ...this.$rootScope.customTerms, ...translationMap };
    }

    refreshTermCurrentLanguage(termKey: string): ng.IPromise<IProtocolResponse<ITranslation[]>> {
        const currentLanguage: string = this.getCurrentLanguage();
        return this.localizationApiService.getTranslationsByKey(termKey).then((response: IProtocolResponse<ITranslation[]>): IProtocolResponse<ITranslation[]> => {
            if (!response.result) return;
            const currentTranslation = find(response.data, (translation: ITranslation): boolean => translation.languageCode === currentLanguage);
            if (!currentTranslation) return;
            const newValue: string = currentTranslation.customizedValue ? (currentTranslation.value || currentTranslation.defaultValue) : currentTranslation.defaultValue;
            this.addIndividualTranslations({ [termKey]: newValue });
            return response;
        });
    }

    setSystemTranslations(translations) {
        this.$rootScope.translations = translations;
        this.$rootScope.customTerms = translations;
        this._loadedModules = { [TranslationModules.System]: true };
    }

    get loadedModuleTranslations(): IStringMap<boolean> {
        return this._loadedModules;
    }

    private _getLanguages(tenantId?: string): any {
        const config: IProtocolConfig = this.OneProtocol.customConfig("GET", "/api/globalization/v1/languages");
        const messages: IProtocolMessages = { Error: { custom: this.$rootScope.t("SorryProblemGettingTranslations") } };
        return this.OneProtocol.http(config, messages);
    }

    /* Required for Login page*/
    private _addDefaultCustomTermsToRootScope(language: string): void {
        const compiledCustomTerms = {};
        if (!this.CONSTANTS[language])
            language = "en-us";

        this.$rootScope.customTerms = this.CONSTANTS[language];
        this.$rootScope.translations = this.CONSTANTS[language];
    }

    private getSystemTranslations(tenantId: string, language: string): ng.IPromise<{}> {
        const deferred = this.$q.defer();
        this.localizationApiService.getSystemTranslations({tenantId, language}).subscribe((response: IProtocolResponse<ITenantTranslations>) => {
            if (response.result) {
                this.extractTranslationData(response);
                deferred.resolve(response);
            } else {
                this.getSystemTranslationsLegacy(tenantId, language).subscribe((legacyresponse:  IProtocolResponse<ITenantTranslations>) => {
                    this.extractTranslationData(legacyresponse);
                    deferred.resolve(legacyresponse);
                });
            }
        });
        return deferred.promise;
    }

    private getSystemTranslationsLegacy(tenantId: string, language: string): Observable<{}> {
        return this.localizationApiService.getSystemTranslationsLegacy({tenantId, language});
    }

    private extractTranslationData(response: IProtocolResponse<ITenantTranslations>) {
        if (response.result && response.data) {
            this.setSystemTranslations(response.data.Translations);
        }
    }
}
