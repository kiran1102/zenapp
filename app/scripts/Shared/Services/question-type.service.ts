import * as _ from "lodash";
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import { IQuestionTypes } from "enums/question-types.enum";

class QuestionTypes {
    static $inject: string[] = ["$rootScope", "ENUMS"];

    private DragTypes: any;
    private Types: any;
    private DMTypes: any;
    private Attributes: any;
    private TemplateTypes: any;
    private DMAssessmentTemplateTypes: any;
    private translate: any;

    private DefaultAttributes: any;
    private BasicAttributes: any;
    private AllowableNotAttributes: any;
    private JustificationAttributes: any;
    private AllowableAttributes: any;
    private StandardAttributesSet: any;
    private TextboxAttributes: any;
    private MultichoiceAttributes: any;
    private CustomMultichoiceAttributes: any;

    private QuestionAttributes: any;

    constructor(
        $rootScope: IExtendedRootScopeService,
        readonly ENUMS: any,
    ) {
        this.DragTypes = ENUMS.DragTypes;
        this.Types = IQuestionTypes;
        this.DMTypes = ENUMS.DMQuestionTypes;
        this.Attributes = ENUMS.QuestionAttributes;
        this.TemplateTypes = ENUMS.TemplateTypes;
        this.DMAssessmentTemplateTypes = ENUMS.DMAssessmentTemplateTypes;
        this.translate = $rootScope.t;
        this.DefaultAttributes = this.buildDefaultAttributes();
        this.BasicAttributes = this.buildBasicAttributes();
        this.AllowableNotAttributes = this.buildAllowableNotAttributes();
        this.JustificationAttributes = this.buildJustificationAttributes();
        this.AllowableAttributes = this.buildAllowableAttributes();
        this.StandardAttributesSet = this.buildStandardAttributesSet();
        this.TextboxAttributes = this.buildTextboxAttributes();
        this.MultichoiceAttributes = this.buildMultichoiceAttributes();
        this.CustomMultichoiceAttributes = this.buildCustomMultichoiceAttributes();
        this.QuestionAttributes = this.buildQuestionAttributes();
    }

    public getDraggableQuestionTypes(templateType: number, typeId: number): any[] {
        const questionTypeArray = [{
                AllowedContexts: [this.TemplateTypes.Custom, this.TemplateTypes.Datamapping],
                DragType: this.DragTypes.StatementAdd,
                Type: this.Types.Content,
                Permission: {
                    Key: "",
                    Upgrade: "",
                },
                TypeName: this.translate("Statement"),
            },
            {
                AllowedContexts: [this.TemplateTypes.Custom, this.TemplateTypes.Datamapping],
                DragType: this.DragTypes.QuestionAdd,
                Type: this.Types.TextBox,
                Permission: {
                    Key: "",
                    Upgrade: "",
                },
                TypeName: this.translate("Textbox"),
            },
            {
                AllowedContexts: [this.TemplateTypes.Custom, this.TemplateTypes.Datamapping],
                DragType: this.DragTypes.QuestionAdd,
                Type: this.Types.Multichoice,
                Permission: {
                    Key: "",
                    Upgrade: "",
                },
                TypeName: this.translate("MultiChoice"),
            },
            {
                AllowedContexts: [this.TemplateTypes.Custom, this.TemplateTypes.Datamapping],
                DragType: this.DragTypes.QuestionAdd,
                Type: this.Types.YesNo,
                Permission: {
                    Key: "",
                    Upgrade: "",
                },
                TypeName: this.translate("YesOrNo"),
            },
            {
                AllowedContexts: [this.TemplateTypes.Custom, this.TemplateTypes.Datamapping],
                DragType: this.DragTypes.QuestionAdd,
                Type: this.Types.DateTime,
                Permission: {
                    Key: "",
                    Upgrade: "",
                },
                TypeName: this.translate("Date"),
            },
            {
                AllowedContexts: [],
                DragType: this.DragTypes.QuestionAdd,
                Type: this.Types.User,
                Permission: {
                    Key: "",
                    Upgrade: "",
                },
                TypeName: this.translate("User"),
            },
            {
                AllowedContexts: [],
                DragType: this.DragTypes.QuestionAdd,
                Type: this.Types.Location,
                Permission: {
                    Key: "",
                    Upgrade: "",
                },
                TypeName: this.translate("Location"),
            },
            {
                AllowedContexts: [this.TemplateTypes.Custom, this.TemplateTypes.Datamapping],
                DragType: this.DragTypes.QuestionAdd,
                Type: this.Types.DataElement,
                Permission: {
                    Key: "TemplatesQuestionTypeDataElement",
                    Upgrade: "DataMappingUpgrade",
                },
                TypeName: this.translate("DataElements"),
            },
            {
                AllowedContexts: [],
                DragType: this.DragTypes.QuestionAdd,
                Type: this.Types.Application,
                Permission: {
                    Key: "DataMapping",
                    Upgrade: "DataMappingUpgrade",
                },
                TypeName: this.translate("Application"),
            },
            {
                AllowedContexts: [],
                DragType: this.DragTypes.QuestionAdd,
                Type: this.Types.Provider,
                Permission: {
                    Key: "",
                    Upgrade: "",
                },
                TypeName: this.translate("Provider"),
            },
            {
                AllowedContexts: [this.TemplateTypes.Custom, this.TemplateTypes.Datamapping],
                DragType: this.DragTypes.QuestionAddGroup,
                Type: this.Types.ApplicationGroup,
                Permission: {
                    Key: "ApplicationQuestionGroup",
                    Upgrade: "DataMappingUpgrade",
                },
                TypeName: this.translate("ApplicationDiscovery"),
            },
            {
                AllowedContexts: [this.TemplateTypes.NewDatamapping],
                AllowedDMTemplateTypes: [this.DMAssessmentTemplateTypes.ProcessingActivity, this.DMAssessmentTemplateTypes.Asset],
                DragType: this.DragTypes.QuestionAdd,
                Type: this.DMTypes.Text,
                Permission: {
                    Key: "",
                    Upgrade: "",
                },
                TypeName: this.translate("Textbox"),
            },
            {
                AllowedContexts: [this.TemplateTypes.NewDatamapping],
                AllowedDMTemplateTypes: [this.DMAssessmentTemplateTypes.ProcessingActivity, this.DMAssessmentTemplateTypes.Asset],
                DragType: this.DragTypes.QuestionAdd,
                Type: this.DMTypes.SingleSelect,
                Permission: {
                    Key: "",
                    Upgrade: "",
                },
                TypeName: this.translate("MultiChoice"),
            },
            {
                AllowedContexts: [this.TemplateTypes.NewDatamapping, this.DMAssessmentTemplateTypes.ProcessingActivity],
                AllowedDMTemplateTypes: [this.DMAssessmentTemplateTypes.ProcessingActivity],
                DragType: this.DragTypes.QuestionAddGroup,
                Type: this.DMTypes.DataSubject,
                Permission: {
                    Key: "",
                    Upgrade: "",
                },
                TypeName: this.translate("DataSubjects"),
            },
            {
                AllowedContexts: [this.TemplateTypes.Custom, this.TemplateTypes.Datamapping],
                DragType: this.DragTypes.QuestionAdd,
                Type: this.Types.AssetDiscovery,
                Permission: {
                    Key: "DataMappingSmartQuestionsAssets",
                    Upgrade: "",
                },
                TypeName: this.translate("AssetDiscovery"),
            },
        ];
        if (templateType) {
            return _.filter(questionTypeArray, (questionType: any): boolean => {
                if (typeId) {
                    return (questionType.AllowedContexts.indexOf(templateType) > -1) && (questionType.AllowedDMTemplateTypes.indexOf(typeId) > -1);
                } else {
                    return (questionType.AllowedContexts.indexOf(templateType) > -1);
                }
            });
        } else {
            return questionTypeArray;
        }
    }

    public getSelectableQuestionTypes(): any[] {
        return [{
                Type: this.Types.TextBox,
                TypeName: this.translate("Textbox"),
                Permission: {
                    Key: "",
                    Upgrade: "",
                },
            },
            {
                Type: this.Types.Multichoice,
                TypeName: this.translate("MultiChoice"),
                Permission: {
                    Key: "",
                    Upgrade: "",
                },
            },
            {
                Type: this.Types.YesNo,
                TypeName: this.translate("YesOrNo"),
                Permission: {
                    Key: "",
                    Upgrade: "",
                },
            },
            {
                Type: this.Types.DateTime,
                TypeName: this.translate("Date"),
                Permission: {
                    Key: "",
                    Upgrade: "",
                },
            },
            {
                Type: this.Types.AssetDiscovery,
                TypeName: this.translate("AssetDiscovery"),
                Permission: {
                    Key: "DataMappingSmartQuestionsAssets",
                    Upgrade: "",
                },
            },
            {
                Type: this.Types.DataElement,
                TypeName: this.translate("DataElement"),
                Permission: {
                    Key: "TemplatesQuestionTypeDataElement",
                    Upgrade: "DataMappingUpgrade",
                },
            },
        ];
    }

    public questionHasAttribute(type: number, property: string, hasGroup: boolean): boolean {
        if (type && property) {
            if (this.QuestionAttributes[type]) {
                const prop: { BlockGroup: any } = this.QuestionAttributes[type][property];
                if (_.isObject(prop) && Object.keys(prop).length) {
                    return !(prop.BlockGroup && hasGroup);
                }
                return Boolean(prop);
            } else {
                return Boolean(this.QuestionAttributes["Default"][property]);
            }
        }
        return false;
    }

    public propIsAllowedForEditing(property: string, question: any): boolean {
        const allowedPropsForEditQuestion: string[] = ["Name", "Description", "Hint"];
        if (question.Id) {
            return allowedPropsForEditQuestion.indexOf(property) > -1;
        }
        return true;
    }

    public getAttributes(): any {
        return this.Attributes;
    }

    public getQuestionAttributes(): any {
        return this.QuestionAttributes;
    }

    private objectify(property: string, value?: any): any {
        const obj: any = {};
        if (property) {
            obj[property] = (value || true);
        }
        return obj;
    }

    private merge(objects: any[]): any {
        objects.unshift({});
        return _.reduce(objects, _.merge);
    }

    private buildDefaultAttributes(): any {
        return this.merge([
            this.objectify(this.Attributes.Name),
            this.objectify(this.Attributes.Description),
        ]);
    }

    private buildBasicAttributes(): any {
        return this.merge([
            this.objectify(this.Attributes.Hint),
            this.objectify(this.Attributes.ReportFriendlyName),
            this.objectify(this.Attributes.Required),
        ]);
    }

    private buildAllowableNotAttributes(): any {
        return this.merge([
            this.objectify(this.Attributes.AllowNotSure),
            this.objectify(this.Attributes.AllowNotApplicable),
        ]);
    }

    private buildJustificationAttributes(): any {
        return this.merge([
            this.objectify(this.Attributes.AllowJustification),
            this.objectify(this.Attributes.RequireJustification),
        ]);
    }

    private buildAllowableAttributes(): any {
        return this.merge([
            this.AllowableNotAttributes,
            this.JustificationAttributes,
        ]);
    }

    private buildStandardAttributesSet(): any {
        return this.merge([
            this.DefaultAttributes,
            this.BasicAttributes,
            this.AllowableAttributes,
        ]);
    }

    private buildTextboxAttributes(): any {
        return this.merge([
            this.objectify(this.Attributes.Multiline),
        ]);
    }

    private buildMultichoiceAttributes(): any {
        return this.merge([
            this.objectify(this.Attributes.AllowOther),
        ]);
    }

    private buildCustomMultichoiceAttributes(): any {
        return this.merge([
            this.objectify(this.Attributes.Options, {
                BlockGroup: true,
            }),
            this.objectify(this.Attributes.MultiSelect),
        ]);
    }

    private buildQuestionAttributes(): any {
        return this.merge([
            this.objectify(
                "Default",
                this.merge([
                    this.StandardAttributesSet,
                ]),
            ),
            this.objectify(
                this.Types.Content,
                this.merge([
                    this.DefaultAttributes,
                    this.TextboxAttributes,
                ]),
            ),
            this.objectify(
                this.Types.TextBox,
                this.merge([
                    this.DefaultAttributes,
                    this.BasicAttributes,
                    this.TextboxAttributes,
                    this.AllowableNotAttributes,
                ]),
            ),
            this.objectify(
                this.Types.Multichoice,
                this.merge([
                    this.StandardAttributesSet,
                    this.MultichoiceAttributes,
                    this.CustomMultichoiceAttributes,
                ]),
            ),
            this.objectify(
                this.Types.AssetDiscovery,
                this.merge([
                    this.StandardAttributesSet,
                    this.objectify(this.Attributes.AllowOther),
                ]),
            ),
            this.objectify(
                this.Types.YesNo,
                this.merge([
                    this.StandardAttributesSet,
                ]),
            ),
            this.objectify(
                this.Types.DateTime,
                this.merge([
                    this.DefaultAttributes,
                    this.BasicAttributes,
                    this.AllowableNotAttributes,
                ]),
            ),
            this.objectify(
                this.Types.Location,
                this.merge([
                    this.StandardAttributesSet,
                    this.MultichoiceAttributes,
                ]),
            ),
            this.objectify(
                this.Types.User,
                this.merge([
                    this.StandardAttributesSet,
                    this.MultichoiceAttributes,
                ]),
            ),
            this.objectify(
                this.Types.DataElement,
                this.merge([
                    this.DefaultAttributes,
                    this.BasicAttributes,
                    this.MultichoiceAttributes,
                    this.objectify(this.Attributes.GroupFilter),
                ]),
            ),
            this.objectify(
                this.Types.Application,
                this.merge([
                    this.DefaultAttributes,
                    this.BasicAttributes,
                    this.JustificationAttributes,
                    this.MultichoiceAttributes,
                ]),
            ),
            this.objectify(
                this.Types.Provider,
                this.merge([
                    this.StandardAttributesSet,
                    this.MultichoiceAttributes,
                ]),
            ),
            this.objectify(
                this.Types.PlaceholderGroup,
                this.merge([
                    // No Attributes
                ]),
            ),
        ]);
    }

}

export default QuestionTypes;
