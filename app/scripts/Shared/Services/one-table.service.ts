import * as _ from "lodash";

export default class OneTable {

    public columnMoved(columnList: any, droppedIndex: number, column: any): any[] {
        const columnIndex: number = _.findIndex(columnList, {key: column.key});
        if (columnIndex > -1) {
            const newColumn: any = columnList[columnIndex];

            if (droppedIndex > columnIndex) {
                droppedIndex -= 1;
            }
            if (droppedIndex !== columnIndex) {
                columnList.splice(columnIndex, 1);
                columnList.splice(droppedIndex, 0, newColumn);
            }
        }
        return columnList;
    }

}
