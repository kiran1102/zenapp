import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import { IBreadcrumb } from "interfaces/breadcrumb.interface";
import { IStateService } from "angular-ui-router";
import { ui } from "angular";
import { Transition } from "@uirouter/core";
import { isString, uniqBy } from "lodash";

export default class OneBreadcrumbService {
    static $inject: string[] = ["$state", "$transitions"];

    private breadcrumbs: IBreadcrumb[] = [];

    constructor(readonly $state: IStateService, $transitions: Transition) {
        this.breadcrumbs = this.refresh(this.$state);

        $transitions.onSuccess({}, (): void => {
            this.breadcrumbs = this.refresh(this.$state);
        });
    }

    public getBreadcrumbs(): IBreadcrumb[] {
        this.breadcrumbs = this.refresh(this.$state);
        return this.breadcrumbs;
    }

    private refresh($state: IStateService): IBreadcrumb[] {
        const breadcrumbs: IBreadcrumb[] = [];
        let currentState: ui.IState = $state.$current as ui.IState;
        while (currentState.parent) {
            const breadcrumb: IBreadcrumb = this.compile(currentState);
            if (breadcrumb) {
                breadcrumbs.unshift(breadcrumb);
            }
            currentState = currentState.parent as ui.IState;
        }
        return uniqBy(breadcrumbs, (crumb: IBreadcrumb): string => crumb.text);
    }

    private compile(currentState: ui.IState): IBreadcrumb {
        if (!currentState.data || !currentState.data.breadcrumb) return null;
        if (isString(currentState.data.breadcrumb)) {
            return {
                text: currentState.data.breadcrumb,
                stateName: currentState.name,
            };
        } else {
            return currentState.data.breadcrumb;
        }
    }
}
