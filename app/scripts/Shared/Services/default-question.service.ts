class DefaultQuestion {
    static $inject: string[] = ["ENUMS"];

    private TemplateTypes: any;
    private questionTypes: any = {};

    constructor(ENUMS: any) {
        this.TemplateTypes = ENUMS.TemplateTypes;
        this.questionTypes[this.TemplateTypes.Custom] = ENUMS.QuestionTypes;
        this.questionTypes[this.TemplateTypes.Datamapping] = ENUMS.DMQuestionTypes;
    }

    public getDefaultQuestion(templateType: number, questionType: number, config?: any): any {
        switch (templateType) {
            case this.TemplateTypes.Custom:
                break;
            case this.TemplateTypes.Datamapping:
                switch (questionType) {
                    case this.questionTypes[templateType].AppInfo:
                        return this.getDMAppInfoDefault(this.questionTypes[templateType].AppInfo, config);
                    case this.questionTypes[templateType].DataSubject:
                        return this.getDMDataSubjectDefault(this.questionTypes[templateType].DataSubject, config);
                    default:
                        break;
                }
                break;
            default:
                break;
        }
        return null;
    }

    public getDataSubjectPreview(): any[] {
        return [
            {
                name: "Data Subjects",
                description: "What type of Data Subjects are used for this Processing Activity?",
                questionType: 40,
                displayType: 10,
                required: false,
            },
            {
                name: "Data Elements",
                description: "What Data Elements are processed for this Data Subject?",
                questionType: 80,
                required: false,
            },
            {
                name: "Data Subject Region",
                description: "Specifically for this Processing Activity, where are the primary places that Data Subjects reside?",
                questionType: 80,
                required: false,
            },
            {
                name: "Data Volume",
                description: "Roughly, how many Data Subjects data is involved?",
                questionType: 80,
                required: false,
            },
        ];
    }

    public getDefaultAssetQuestionOptions(): any[] {
        return [
            {
                key: "OneTrust",
                value: "OneTrust",
            },
            {
                key: "Legacy System",
                value: "Legacy System",
            },
        ];
    }

    public getDefaultProcessQuestionOptions(): any[] {
        return [
            {
                key: "Email Marketing",
                value: "Email Marketing",
            },
            {
                key: "Lead Generation",
                value: "Lead Generation",
            },
        ];
    }

    public getDefaultFollowupOptions(): any[] {
        return [
            {
                key: "John Smith",
                value: "John Smith",
            },
            {
                key: "Jane Doe",
                value: "Jane Doe",
            },
            {
                key: "example_user@onetrust.com",
                value: "example_user@onetrust.com",
            },
        ];
    }

    private getDMAppInfoDefault(questionType: number, config?: any): any {
        return {
            name: "Asset Information",
            description: "Answer the following questions about the asset.",
            questionType,
            required: false,
            previousQuestionId: config ? config.previousQuestionId : null,
        };
    }

    private getDMDataSubjectDefault(questionType: number, config?: any): any {
        return {
            name: "Data Subjects",
            description: "Answer the following questions about data subjects.",
            questionType,
            required: false,
            previousQuestionId: config ? config.previousQuestionId : null,
        };
    }

}

export default DefaultQuestion;
