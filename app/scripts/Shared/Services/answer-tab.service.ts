import * as _ from "lodash";
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";

export default class AnswerTab {

    static $inject: string[] = ["$rootScope", "ENUMS"];

    private translate: any;
    private QuestionTypes: any;

    constructor(
        $rootScope: IExtendedRootScopeService,
        ENUMS: any,
    ) {
        this.translate = $rootScope.t;
        this.QuestionTypes = ENUMS.QuestionTypes;
    }

    public findQuestionAnswer(scope: any, project: any, sectionIndex: number, questionIndex: number, answerIndex: number, questionData: any, responseType: any): undefined|any {
        if (_.isUndefined(project.Sections[sectionIndex])) return;

        let question: any;
        if (questionData) {
            _.each(project.Sections, (section: any): void => {
                const sectionQuestionsLen = section.Questions.length;
                for (let i = 0; i < sectionQuestionsLen; i += 1) {
                    if (section.Questions[i].TemplateQuestionId === questionData.Id) {
                    question = section.Questions[i];
                    break;
                    }
                }
            });
        }

        const questionType: any = question ? question.Type : {};
        let questionAnswered = false;
        scope.processedValue = {};

        switch (questionType) {
            case this.QuestionTypes.TextBox:
            case this.QuestionTypes.DateTime:
                questionAnswered = this.getTextBoxOrDateAnswer(scope, question, answerIndex);
                break;
            case this.QuestionTypes.Multichoice:
                if (question.MultiSelect) {
                    questionAnswered = this.getMultiSelectAnswer(scope, question, answerIndex, responseType);
                } else {
                    questionAnswered = this.getMultiChoiceAnswer(scope, question, answerIndex);
                }
                break;
            case this.QuestionTypes.YesNo:
                questionAnswered = this.getBooleanAnswer(scope, question, answerIndex);
                break;
            default:
            }
        return questionAnswered;
        }

    private getTextBoxOrDateAnswer(scope: any, question: any, answerIndex: number): boolean {
        if (answerIndex === -1) { // If Clicked Not Answered
            if (question.Responses && question.Responses.length) {
                scope.processedValue = {
                    text: this.translate("Unanswered"),
                    value: null,
                };
                return true;
            }
        }
        const response: any = question.Responses ? question.Responses[0] : {};

        if (response.Value && answerIndex === 0) {
            scope.processedValue = {
                text: response.Value,
                value: this.translate("Answered"),
            };
            return true;
        } else if (question.AllowNotSure && (response.Value === null) && (response.Type === scope.responseTypes.NotSure)) {
            scope.processedValue = {
                text: "Not Sure",
                value: this.translate("NotSure"),
            };
            return true;
        } else if (question.AllowNotApplicable && (response.Value === null) && (response.Type === scope.responseTypes.NotApplicable)) {
            scope.processedValue = {
                text: "Not Applicable",
                value: this.translate("NotApplicable"),
            };
            return true;
        } else {
            scope.processedValue = {
                text: "Not Answered",
                value: this.translate("Unanswered"),
            };
            return false;
        }
    }

    private getMultiSelectAnswer(scope: any, question: any, answerIndex: number, responseType: any): any {
        const getResponseByType = () => {
            for (const questionResponse of question.Responses.length) {
                if (questionResponse.Type === responseType) {
                    return questionResponse;
                }
            }
        };
        const response = question.Responses ? question.Responses[answerIndex] ? question.Responses[answerIndex] : getResponseByType() || {} : {};
        const isStandardAnswerType: boolean = response.Type === scope.responseTypes.Reference || response.Type === scope.responseTypes.Value;
        if (response.Value !== null && !_.isUndefined(response.Value) && isStandardAnswerType) {
            scope.processedValue = {
                text: question.Options[response.Value].Name,
                value: response.Value,
            };
            return true;
        } else if (response.Value !== null && !_.isUndefined(response.Value) && response.Type === scope.responseTypes.Other) {
            scope.processedValue = {
                text: response.Value,
                value: this.translate("Other"),
            };
            return true;
        } else if (question.AllowNotSure && (response.Value === null || _.isUndefined(response.Value)) && (response.Type === scope.responseTypes.NotSure)) {
            scope.processedValue = {
                text: "Not Sure",
                value: this.translate("NotSure"),
            };
            return true;
        } else if (question.AllowNotApplicable && (response.Value === null || _.isUndefined(response.Value)) && (response.Type === scope.responseTypes.NotApplicable)) {
            scope.processedValue = {
                text: "Not Applicable",
                value: this.translate("NotApplicable"),
            };
            return true;
        } else {
            scope.processedValue = {
                text: "Not Answered",
                value: this.translate("Unanswered"),
            };
            return false;
        }
    }

    private getMultiChoiceAnswer(scope: any, question: any, answerIndex: number): boolean {
        const response: any = question.Responses ? question.Responses[0] : {};
        const isStandardAnswerType: boolean = response.Type === scope.responseTypes.Reference || response.Type === scope.responseTypes.Value;
        if (response.Value !== null && !_.isUndefined(response.Value) && response.Type !== scope.responseTypes.Other) {
            scope.processedValue = {
                text: question.Options[response.Value].Name,
                value: question.Options[response.Value].Name,
            };
            return true;
        } else if (response.Value !== null && response.Type === scope.responseTypes.Other) {
            scope.processedValue = {
                text: response.Value,
                value: this.translate("Other"),
            };
            return true;
        } else if (question.AllowNotSure && (response.Value === null) && (response.Type === scope.responseTypes.NotSure)) {
            scope.processedValue = {
                text: "Not Sure",
                value: this.translate("NotSure"),
            };
            return true;
        } else if (question.AllowNotApplicable && (response.Value === null) && (response.Type === scope.responseTypes.NotApplicable)) {
            scope.processedValue = {
                text: "Not Applicable",
                value: this.translate("NotApplicable"),
            };
            return true;
        } else {
            scope.processedValue = {
                text: "Not Answered",
                value: this.translate("Unanswered"),
            };
            return false;
        }
    }

    private getBooleanAnswer(scope: any, question: any, answerIndex: number): boolean {
        const response: any = question.Responses ? question.Responses[0] : {};

        if (response.Value) {
            scope.processedValue = {
                text: "Yes",
                value: this.translate("Yes"),
            };
            return true;
        } else if (response.Value === false) {
            scope.processedValue = {
                text: "No",
                value: this.translate("No"),
            };
            return true;
        } else if (question.AllowNotSure && (response.Value === null) && (response.Type === scope.responseTypes.NotSure)) {
            scope.processedValue = {
                text: "Not Sure",
                value: this.translate("NotSure"),
            };
            return true;
        } else if (question.AllowNotApplicable && (response.Value === null) && (response.Type === scope.responseTypes.NotApplicable)) {
            scope.processedValue = {
                text: "Not Applicable",
                value: this.translate("NotApplicable"),
            };
            return true;
        } else {
            scope.processedValue = {
                text: "Not Answered",
                value: this.translate("Unanswered"),
            };
            return false;
        }
    }

}
