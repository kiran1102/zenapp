
export function serverInterceptor($rootScope) {
    return {
        request: function (config) {
            return $rootScope.handleConfig(config);
        }
    };
}

serverInterceptor.$inject = ["$rootScope"];

export function registerInterceptor($httpProvider) {
    $httpProvider.defaults.useXDomain = true;
    //$httpProvider.defaults.withCredentials = true;

    $httpProvider.interceptors.push("serverInterceptor");
}

registerInterceptor.$inject = ["$httpProvider"];
