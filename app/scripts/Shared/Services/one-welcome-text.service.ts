import { IExtendedRootScopeService } from "interfaces/root-scope.interface";

export default class OneWelcomeTextService {

    static $inject: string[] = ["$rootScope"];
    private translate: any;
    private defaultText: any;
    private oneWelcomeTextDefault: string;

    constructor($rootScope: IExtendedRootScopeService) {
        this.translate = $rootScope.t;

        this.defaultText = {
            Section1Title: this.translate("WelcomeText.Section1.Title"),
            Section1Line1: this.translate("WelcomeText.Section1.Line1"),
            Section1Line2: this.translate("WelcomeText.Section1.Line2"),
            Section1Line3: this.translate("WelcomeText.Section1.Line3"),
            Section2Title: this.translate("WelcomeText.Section2.Title"),
            Section3Title: this.translate("WelcomeText.faq.Title"),
            Section3Q1: this.translate("WelcomeText.faq.Q1"),
            Section3Q1A1: this.translate("WelcomeText.faq.Q1.A1"),
            Section3Q2: this.translate("WelcomeText.faq.Q2"),
            Section3Q2A1: this.translate("WelcomeText.faq.Q2.A1"),
            Section4Line1: this.translate("WelcomeText.disclaimer"),
        };

        this.oneWelcomeTextDefault = // DO NOT use template literals here. you will be introducing unwanted spaces
            "<p><strong>" +          this.defaultText.Section1Title  + "</strong></p>" +
            "<p>" +                  this.defaultText.Section1Line1  + "</p>" +
            "<p>" +                  this.defaultText.Section1Line2  + "</p>" +
            "<p>" +                  this.defaultText.Section1Line3  + "</p>" +
            "<p><br></p>" +
            "<p><strong>" +          this.defaultText.Section2Title  + "</strong></p>" +
            "<p><br></p>" +
            "<p><strong>" +          this.defaultText.Section3Title  + "</strong></p>" +
            "<p><em>1.&nbsp;</em>" + this.defaultText.Section3Q1     + "</p>" +
            "<p>" +                  this.defaultText.Section3Q1A1   + "</span></p>" +
            "<p><em>2.&nbsp;</em>" + this.defaultText.Section3Q2     + "</p>" +
            "<p>" +                  this.defaultText.Section4Line1  + "</p>";
    }

    public getWelcomeText(): string { return this.oneWelcomeTextDefault; }
}
