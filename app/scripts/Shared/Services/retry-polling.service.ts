import * as _ from "lodash";

export default class RetryPollingService {
    static $inject: string[] = ["$timeout"];

    // retry rate in active mode
    private activePollRate: number;

    // retry rate in broken mode
    private brokenPollRate: number;

    // circuit status
    private isLoopActive: boolean;

    // number of retries before breaking the circuit
    private failureThreshold: number;

    // number of miliseconds to wait until triggering failure
    private lastResetThresholdMs: number;

    // action to be protected by circuit
    private onCircuitAction: () => void;

    // action to trigger when breaking the circuit
    private onCircuitBroken: () => void;

    // action to trigger when opening the circuit
    private onCircuitOpen: () => void;

    // set manual mode to trigger failures an resets from the client
    private manualMode: boolean;

    private failureCount: number;
    private lastFailureReset: Date;
    private timeoutPromise: ng.IPromise<boolean>;
    private isCircuitJustOpen: boolean;

    constructor(private readonly $timeout: ng.ITimeoutService) {}

    public initialize(
        circuitAction: () => void,
        failureThreshold: number = 5,
        lastResetThresholdMs: number = 30000,
        activePollRate: number = 10000,
        brokenPollRate: number = 30000,
        manualMode: boolean = false,
        circuitBrokenAction?: () => void,
        circuitOpenAction?: () => void,
    ): void {
        this.onCircuitAction = circuitAction;
        this.failureThreshold = failureThreshold;
        this.activePollRate = activePollRate;
        this.brokenPollRate = brokenPollRate;
        this.manualMode = manualMode;
        this.onCircuitBroken = circuitBrokenAction;
        this.onCircuitOpen = circuitOpenAction;
        this.lastResetThresholdMs = lastResetThresholdMs;

        this.triggerSuccess();
        this.setLoopStatus(false);
    }

    public setLoopStatus(isActive: boolean): void {
        const isActivated: boolean = !this.isLoopActive && isActive;
        this.isLoopActive = isActive;
        if (isActivated) {
            this.circuitLoop();
        }

        if (!isActive) {
            this.$timeout.cancel(this.timeoutPromise);
        }
    }

    public setPollRate(pollRate: number): void {
        this.activePollRate = pollRate;
    }

    public triggerFailure(): void {
        this.failureCount++;
        if (this.isCircuitJustBroken && _.isFunction(this.onCircuitBroken)) {
            this.onCircuitBroken();
        }
    }

    public triggerSuccess(): void {
        this.isCircuitJustOpen = this.isCircuitBroken;
        this.failureCount = 0;
        this.lastFailureReset = new Date();

        if (this.isCircuitJustOpen && _.isFunction(this.onCircuitOpen)) {
            this.onCircuitOpen();
        }
        this.failureCount = 0;
        this.isCircuitJustOpen = false;
    }

    private circuitLoop(): boolean {
        if (!this.isLoopActive) {
            return false;
        }

        let actionSuccessful = false;
        try {
            this.onCircuitAction();
            actionSuccessful = true;
        } catch (ex) {
            if (!this.manualMode) {
                this.triggerFailure();
            }
        }

        // handle timeout failures
        if (this.lastResetThresholdMs > 0 && this.isCircuitTimeout()) {
            this.triggerFailure();
        } else if (!this.manualMode && actionSuccessful) {
            this.triggerSuccess();
        }

        this.scheduleNextLoop();
        return true;
    }

    private get isCircuitBroken(): boolean {
        return this.failureCount >= this.failureThreshold;
    }

    private get isCircuitJustBroken(): boolean {
        return this.failureCount === this.failureThreshold;
    }

    private isCircuitTimeout(): boolean {
        const currentTimeMs: number = new Date().getTime();
        const lastTimeMs: number = this.lastFailureReset.getTime();
        return currentTimeMs - lastTimeMs > this.lastResetThresholdMs;
    }

    private scheduleNextLoop(): void {
        const pollRate: number = this.isCircuitBroken ? this.brokenPollRate : this.activePollRate;
        this.timeoutPromise = this.$timeout(() => this.circuitLoop(), pollRate);
    }
}
