// 3rd Party
import { filter } from "lodash";

// Constants
import {
    TaskStateIds,
    TaskTypes,
} from "constants/tasks.constant";

// Services
import { ProtocolService } from "modules/core/services/protocol.service";

// Interfaces
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import { IProtocolConfig, IProtocolMessages, IProtocolResponse } from "interfaces/protocol.interface";
import { IPageable } from "interfaces/pagination.interface";
import { ITaskListRequest, ITask } from "interfaces/tasks.interface";

export class TasksService {
    static $inject: string[] = [
        "$rootScope",
        "OneProtocol",
    ];

    constructor(
        private readonly $rootScope: IExtendedRootScopeService,
        private readonly OneProtocol: ProtocolService,
    ) { }

    createTask(taskCreateRequest: ITask): ng.IPromise<IProtocolResponse<ITask>> {
        const object = this.$rootScope.t("Tasks.Task");
        const config: IProtocolConfig = this.OneProtocol.customConfig("POST", "/api/task/v1/tasks", null, taskCreateRequest);
        const messages: IProtocolMessages = {
            Error: {
                action: this.$rootScope.t("Creating").toString().toLowerCase(),
                object,
            },
            Success: {
                action: this.$rootScope.t("Created").toString().toLowerCase(),
                object,
            },
        };
        return this.OneProtocol.http(config, messages);
    }

    listTasks(tasksListRequest: ITaskListRequest): ng.IPromise<IProtocolResponse<IPageable>> {
        const object = this.$rootScope.t("Tasks.Tasks");
        const url = `/api/task/v1/tasks/byuser/${tasksListRequest.user}`;
        const config: IProtocolConfig = this.OneProtocol.customConfig(
            "GET",
            url,
            {
                page: tasksListRequest.pageNumber,
                size: tasksListRequest.pageSize,
                sort: tasksListRequest.sortOptions,
            },
            tasksListRequest,
        );
        const messages: IProtocolMessages = {
            Error: {
                action: this.$rootScope.t("Retrieving").toString().toLowerCase(),
                object,
            },
        };
        return this.OneProtocol.http(config, messages);
    }

    updateTask(taskUpdateRequest: ITask): ng.IPromise<IProtocolResponse<ITask>> {
        const object = this.$rootScope.t("Tasks.Task");
        const config: IProtocolConfig = this.OneProtocol.customConfig("PUT", "/api/task/v1/tasks", null, taskUpdateRequest);
        const messages: IProtocolMessages = {
            Error: {
                action: this.$rootScope.t("Updating").toString().toLowerCase(),
                object,
            },
            Success: {
                action: this.$rootScope.t("Updated").toString().toLowerCase(),
                object,
            },
        };
        return this.OneProtocol.http(config, messages);
    }

    deleteTask(taskDeleteRequest: ITask): ng.IPromise<IProtocolResponse<void>> {
        const object = this.$rootScope.t("Tasks.Task");
        const url = `/api/task/v1/tasks/${taskDeleteRequest.id}`;
        const config: IProtocolConfig = this.OneProtocol.customConfig("DELETE", url, null, taskDeleteRequest);
        const messages: IProtocolMessages = {
            Error: {
                action: this.$rootScope.t("Deleting").toString().toLowerCase(),
                object,
            },
            Success: {
                action: this.$rootScope.t("Deleted").toString().toLowerCase(),
                object,
            },
        };
        return this.OneProtocol.http(config, messages);
    }

    filterTaskCount(tasks: ITask[]): number {
        return tasks && tasks.length ? filter(tasks, (task: ITask): boolean => {
            return (task.type === TaskTypes.User && task.taskStatusId === TaskStateIds.TaskOpen)
                || (task.type === TaskTypes.System && (task.taskStatusId === TaskStateIds.TaskCompleted || task.taskStatusId === TaskStateIds.TaskFailed));
        }).length : 0;
    }
}
