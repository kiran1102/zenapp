// Services
import { SupportService } from "oneServices/support.service";
import { Content } from "sharedModules/services/provider/content.service";
import { McmModalService } from "modules/mcm/mcm-modal.service";

// Enums
import { UpgradeModuleState } from "sharedModules/enums/upgrade.enum";

// Interfaces
import { IStringMap } from "interfaces/generic.interface";
import { IUser } from "interfaces/user.interface";
import { IProtocolPacket, IProtocolResponse } from "interfaces/protocol.interface";
import {
    IUpgradeModule,
    IModuleStates,
    IShowModalData,
    IUpgradeModalOptions,
} from "interfaces/upgrade.interface";
import { IStore } from "interfaces/redux.interface";

// Redux
import { getCurrentUser } from "oneRedux/reducers/user.reducer";

export class UpgradeService {

    static $inject = ["$uibModal", "PRINCIPAL", "Content", "SupportService", "store", "McmModalService"];

    private ModuleData: IStringMap<IUpgradeModule> | null;
    private ModuleStates: IModuleStates;

    constructor(
        private readonly $uibModal: any,
        private readonly PRINCIPAL: any,
        private readonly content: Content,
        private readonly supportService: SupportService,
        private store: IStore,
        private mcmModalService: McmModalService,
    ) {}

    public getContentData(): IStringMap<IUpgradeModule> {
        return {
            // Module_Key: {
            //     Name: 'Module_Name',
            //     UpgradeVideo: 'dQw4w9WgXcQ',
            // },
            Projects: {
                Name: "Projects",
                UpgradeVideo: this.content.GetKey("TutorialProjects"),
            },
            Templates: {
                Name: "Templates",
                UpgradeVideo: this.content.GetKey("TutorialQuestionnaire"),
            },
            Dashboard: {
                Name: "Dashboard",
                UpgradeVideo: this.content.GetKey("TutorialDashboard"),
            },
            Risks: {
                Name: "Risks",
                UpgradeVideo: this.content.GetKey("TutorialRisks"),
            },
            DataMapping: {
                Name: "Data Mapping",
                UpgradeVideo: this.content.GetKey("TutorialDataMapping"),
            },
            DSAR: {
                Name: "Data Subject Access Request",
                UpgradeVideo: this.content.GetKey("TutorialDSAR"),
            },
            Users: {
                Name: "Users",
                UpgradeVideo: this.content.GetKey("TutorialUsers"),
            },
            Organizations: {
                Name: "Organizations",
                UpgradeVideo: this.content.GetKey("TutorialOrganizations"),
            },
            Settings: {
                Name: "Settings",
                UpgradeVideo: this.content.GetKey("TutorialSettings"),
            },
            General: {
                Name: "OneTrust",
                UpgradeVideo: this.content.GetKey("Tutorials"),
            },
        };
    }

    public genericModule(name: string): IUpgradeModule {
        return {
            Name: name ? name : "",
            UpgradeVideo: "",
        };
    }

    public getModule(module: string): IUpgradeModule {
        if (!this.ModuleData) {
            this.ModuleData = this.getContentData();
        }

        if (module && this.ModuleData.hasOwnProperty(module)) {
            return this.ModuleData[module];
        }
        return this.genericModule(module);
    }

    public showModal(options: IShowModalData): void {
        const url = "UpgradeModal.html";
        const modalInstance: ng.IPromise<IProtocolPacket> = this.$uibModal.open({
            templateUrl: url,
            controller: "UpgradeModalCtrl",
            keyboard: false,
            resolve: {
                Options: options,
                currentUser: getCurrentUser(this.store.getState()),
                OneContent: (): Content => {
                    return this.content;
                },
            },
        });
    }

    public sendUpgradeForm(data: any): ng.IPromise<IProtocolPacket> {
        return this.supportService.upgrade(data);
    }

    public upgradeModule(module: string = "", upgradeState?: UpgradeModuleState): void {
        // TODO: Pass relevant key based on module when the content is ready
        this.mcmModalService.openMcmModal("Upgrade");
    }

    public contactSalesModule(module: string = "", upgradeState?: UpgradeModuleState, modalOptions?: IUpgradeModalOptions): void {
        module = module ? module : "";
        const ModuleData: IUpgradeModule = this.getModule(module);
        const modalData: IShowModalData = {
            name: ModuleData.Name,
            video: ModuleData.UpgradeVideo,
            state: upgradeState,
            contactSales: true,
        };
        if (modalOptions) {
            modalData.modalOptions = modalOptions;
        }
        this.showModal(modalData);
    }
}
