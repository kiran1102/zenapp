import  naturalSort from "natural-sort";

function Utilities() {
    return {
        findById: findById,
        findQuestionById: findQuestionById,
        findByIdReturnBoolean: findByIdReturnBoolean,
        getParameterByName: getParameterByName,
        isArrayWithLength: isArrayWithLength,
        removeURLParameter: removeURLParameter,
        objectify: objectify,
        openWindow: openWindow,
        validateLength: validateLength,
        validateSpecial: validateSpecial,
        validateMatch: validateMatch,
        validatePwdAndEmail: validatePwdAndEmail,
        validateUppercase: validateUppercase,
        validateLowercase: validateLowercase,
        validateNumber: validateNumber,
        validateReminder: validateReminder,
        matchRegex: matchRegex,
        naturalSort: naturalSort,
        round: round,
        schemaNameWithParenthesis: schemaNameWithParenthesis,
        uuid: uuid,
        copyToClipboard: copyToClipboard,
    };

    function findById(arr, id) {
        if (_.isArray(arr) && !_.isUndefined(id)) {
            return _.find(arr, ["Id", id]);
        }
        return false;
    }

    function findQuestionById(arr, questionId) {
        var questionsLen = arr.length;
        for (var i = 0; i < questionsLen; i++) {
            var question = arr[i];
            if (question.Id === questionId) return i;
        }
        return -1;
    }

    function findByIdReturnBoolean(arr, id, idKey) {
        var key = idKey || "Id";
        var length = arr.length;
        for (var i = 0; i < length; i++) {
            if (arr[i][key] === id) {
                return true;
            }
        }
        return false;
    }

    function getParameterByName(url, name) {
        if (!url) url = window.location.hash;
        name = name.replace(/[\[\]]/g, "\\$&");
        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return "";
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    }

    function removeURLParameter(url, parameter) {
        //prefer to use l.search if you have a location/link object
        var urlparts = url.split("?");
        if (urlparts.length >= 2) {

            var prefix = encodeURIComponent(parameter) + "=";
            var pars = urlparts[1].split(/[&;]/g);

            //reverse iteration as may be destructive
            for (var i = pars.length; i-- > 0;) {
                //idiom for string.startsWith
                if (pars[i].lastIndexOf(prefix, 0) !== -1) {
                    pars.splice(i, 1);
                }
            }

            url = urlparts[0] + (pars.length > 0 ? "?" + pars.join("&") : "");
            return url;
        }
        return url;
    }

    // Arguments :
    //  verb : 'GET'|'POST'
    //  target : an optional opening target (a name, or "_blank"), defaults to "_self"
    function openWindow(verb, url, data, target) {
        var form = document.createElement("form");
        form.action = url;
        form.method = verb;
        form.target = target || "_self";
        if (data) {
            for (var key in data) {
                if (data.hasOwnProperty(key)) {
                    var input = document.createElement("textarea");
                    input.name = key;
                    input.value = typeof data[key] === "object" ? JSON.stringify(data[key]) : data[key];
                    form.appendChild(input);
                }
            }
        }
        form.style.display = "none";
        document.body.appendChild(form);
        form.submit();
    }

    function validateLength(password) {
        if (!_.isUndefined(password)) {
            return password.length >= 8;
        }
        return 0;
    }

    function validateSpecial(password) {
        return password.match(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).+$/g);
    }

    function validateMatch(password, confirmPassword) {
        return (!_.isUndefined(password) && (password === confirmPassword) && (password.length > 0));
    }

    function validatePwdAndEmail(password, email) {
        return (password === email);
    }

    function validateUppercase(string) {
        return (!_.isUndefined(string) && /[A-Z]+/.test(string));
    }

    function validateLowercase(string) {
        return (!_.isUndefined(string) && /[a-z]+/.test(string));
    }

    function validateNumber(string) {
        return (!_.isUndefined(string) && /[0-9]+/.test(string));
    }

    function matchRegex(str, re){
        if (_.isString(str) && (re instanceof RegExp) && _.isFunction(re.test)) {
            return re.test(str);
        }
        return false;
    }

    function objectify(property, value, allowUndefined) {
        var obj = {};
        if (property) {
            obj[property] = (_.isUndefined(value) && allowUndefined) ? value : (value || true);
        }
        return obj;
    }

    function round(value, exp) {
        if (typeof exp === 'undefined' || +exp === 0)
            return Math.round(value);

        value = +value;
        exp = +exp;

        if (isNaN(value) || !(typeof exp === 'number' && exp % 1 === 0))
            return NaN;

        // Shift
        value = value.toString().split('e');
        value = Math.round(+(value[0] + 'e' + (value[1] ? (+value[1] + exp) : exp)));

        // Shift back
        value = value.toString().split('e');
        return +(value[0] + 'e' + (value[1] ? (+value[1] - exp) : -exp));
    }

    function schemaNameWithParenthesis(schemaName) {
        if (!schemaName || schemaName === "") {
            return "";
        } else if (endsWith(schemaName, "ies")) {
            return schemaName.replace(new RegExp("ies$"), "y(ies)");
        }
        return schemaName.replace(new RegExp("s$"), "(s)");
    }

    function endsWith(str, suffix) {
        return str.indexOf(suffix, str.length - suffix.length) !== -1;
    }

    function uuid() {
        return Math.random().toString(36).substr(2, 9);
    }

    function isArrayWithLength(arr) {
        return _.isArray(arr) && arr.length > 0;
    }

    function copyToClipboard(text) {
        var element = document.createElement("textarea");
        element.value = text;
        // Add it to the document so that it can be focused.
        document.body.appendChild(element);
        // Focus on the element so that it can be copied.
        element.focus();
        element.setSelectionRange(0, element.value.length);
        // Execute the copy command.
        document.execCommand("copy");
        // Remove the element to keep the document clear.
        document.body.removeChild(element);
    }

    function validateReminder(deadline, reminderDays) {
        if (deadline && reminderDays) {
            var reminderDate = new Date(deadline);
            var today = new Date();
            var reminderDateString = reminderDate.toLocaleDateString();
            var todayString = today.toLocaleDateString();
            reminderDate.setTime(reminderDate.getTime() - 1000 * 60 * 60 * 24 * reminderDays);
            return (todayString !== reminderDateString) && (reminderDate > today);
        } else {
            return true;
        }
    }
}

window.Utilities = new Utilities();
export default new Utilities();
