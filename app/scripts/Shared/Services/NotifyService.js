﻿
export default function NotifyService($uibModal) {

    var NotifyModalCtrl = function ($rootScope, $uibModalInstance, $document, Title, Content, Type, DefaultAction, ActionText, CancelText, ShowCloseButton) {
        var translate = $rootScope.t;
        var $ctrl = this;
        $ctrl.DefaultAction = DefaultAction;

        $ctrl.$onInit = function () {
            if (Type === "Alert") {
                $ctrl.Title = Title || translate("Alert");
            }
            if (Type === "Confirm") {
                $ctrl.Title = Title || translate("Confirm");
                $ctrl.ShowCancel = true;
            }
            $ctrl.ShowOk = true;
            $ctrl.Content = Content || "";

            $ctrl.ActionText = ActionText || "";
            $ctrl.CancelText = CancelText || "";
            $ctrl.ShowCloseButton = ShowCloseButton || false;
        };

        $ctrl.okType = "primary";

        /**
        * @name ok
        * @return {Boolean} Returns false (ok button clicked).
        **/
        $ctrl.ok = function () {
            $uibModalInstance.close(true);
            $document.unbind(EVENTS);
        };

        /**
        * @name cancel
        * @return {Boolean} Returns false (cancel button clicked).
        **/
        $ctrl.cancel = function () {
            $uibModalInstance.close(false);
            $document.unbind(EVENTS);
        };

        /**
        * @name close
        * @return {Boolean} Returns null (notification modal closed, signifies aborting action).
        **/
        $ctrl.close = function () {
            $uibModalInstance.close(null);
            $document.unbind(EVENTS);
        };

        var EVENTS = "keyup";
        var Keys = {
            "Enter": 13,
            "Escape": 27
        };

        function keyClose(event) {
            if (Type === "Alert") {
                switch (event.keyCode) {
                    case Keys.Enter:
                        $ctrl.ok();
                        break;
                    default:
                }
            } else if (Type === "Confirm") {
                switch (event.keyCode) {
                    case Keys.Enter:
                        ($ctrl.DefaultAction === "Ok") ? $ctrl.ok() : $ctrl.cancel();
                        break;
                    case Keys.Escape:
                        $ctrl.cancel();
                        break;
                    default:
                }
            }
        }

        $document.bind(EVENTS, keyClose);
    };


    NotifyModalCtrl.$inject = ["$rootScope", "$uibModalInstance", "$document", "Title", "Content", "Type", "DefaultAction", "ActionText", "CancelText", "ShowCloseButton"];

    var showNotify = function (data) {
        var notify = $uibModal.open({
            controller: NotifyModalCtrl,
            controllerAs: "$ctrl",
            template: require("views/Notification/notificationModal.jade"),
            windowClass: "notificationModal",
            openedClass: "notify__modal",
            backdrop: "static",
            keyboard: false,
            size: data.Size || "sm",
            resolve: {
                /**
                 * @return {String}
                 */
                Title: function () {
                    return data.Title;
                },
                /**
                 * @return {String}
                 */
                Content: function () {
                    return data.Content;
                },
                /**
                 * @return {String}
                 */
                Type: function () {
                    return data.Type;
                },
                /**
                 * @return {String}
                 */
                DefaultAction: function () {
                    return data.DefaultAction;
                },
                /**
                 * @return {String}
                 */
                ActionText: function () {
                    return (data.ActionText || "");
                },
                /**
                 * @return {String}
                 */
                CancelText: function () {
                    return (data.CancelText || "");
                },
                /**
                * @return {Boolean}
                */
                ShowCloseButton: function () {
                    return (data.ShowCloseButton || false);
                }
            }
        });

        return notify.result;
    };

    return {
        /**
        * @name OneAlert
        * @param {String} title
        * @param {String} content
        * @param {String} type (Special Notify type. Defaults to Alert)
        * @param {String} defaultAction
        * @param {Boolean} showCloseButton
        * @param {String} actionText
        * @return {Promise} Returns a promise of a user decision.
        **/
        alert: function (title, content, type, defaultAction, showCloseButton, actionText) {
            return showNotify({
                Title: title,
                Content: content,
                Type: ((!type) ? "Alert" : type),
                DefaultAction: ((!defaultAction) ? "Ok" : defaultAction),
                ShowCloseButton: (showCloseButton || false),
                ActionText: (actionText || ""),
            });
        },
        /**
        * @name OneConfirm
        * @param {String} title
        * @param {String} content
        * @param {String} type (Special Notify type. Defaults to Alert)
        * @param {String} defaultAction
        * @param {Boolean} showCloseButton
        * @param {String} actionText
        * @param {String} cancelText
        * @return {Promise} Returns a promise of a user decision.
        **/
        confirm: function (title, content, type, defaultAction, showCloseButton, actionText, cancelText, size) {
            return showNotify({
                Title: title,
                Content: content,
                Type: ((!type) ? "Confirm" : type),
                DefaultAction: ((!defaultAction) ? "Ok" : defaultAction),
                ShowCloseButton: (showCloseButton || false),
                ActionText: (actionText || ""),
                CancelText: (cancelText || ""),
                Size: size
            });
        }
    };
}

NotifyService.$inject = ["$uibModal"];
