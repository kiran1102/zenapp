import { assign } from "lodash";

import { ProtocolService } from "modules/core/services/protocol.service";
import { Principal } from "sharedModules/services/helper/principal.service";
import { NotificationService } from "modules/shared/services/provider/notification.service";

import { BrandingActions } from "oneRedux/reducers/branding.reducer";

import {
    DEFAULT_BACKGROUND_COLOR,
    DEFAULT_TEXT_COLOR,
} from "constants/branding.constant";
import { UserRoles } from "constants/user-roles.constant";
import { StatusCodes } from "enums/status-codes.enum";
import { SamlAttributeType } from "enums/settings-sso.enum";

import { IStore } from "interfaces/redux.interface";
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import {
    IProtocolPacket,
    IProtocolMessages,
    IProtocolConfig,
    IProtocolResponse,
} from "interfaces/protocol.interface";
import {
    IBrandingModel,
    IBrandingData,
    ICustomBranding,
    IBrandingHeader,
    IBranding,
    ILogo,
} from "interfaces/branding.interface";
import {
    IProjectSettings,
    ISettingsUpdateBrandingContract,
    ISettingsUpdateTagContract,
    ISamlSettings,
    IHelpSettings,
} from "interfaces/setting.interface";

import {
    IAddSamlSettings,
    ISamlResponse,
    ISamlOrgGroupMapping,
    ISamlOrgMappingResponse,
    ISamlOrgGroupMappingTable,
} from "interfaces/settings-sso.interface";
import { ICertificateFileUpload } from "interfaces/settings-sso.interface";

export class Settings {

    static $inject: string[] = ["$rootScope", "OneProtocol", "$q", "store", "NotificationService", "PRINCIPAL"];

    public defaultBranding: IBrandingData = {
        InheritsFrom: null,
        IsInherited: false,
        branding: {
            logo: {
                url: "",
                name: "",
            },
            header: {
                icon: {
                    url: "",
                    name: "",
                },
                backgroundColor: DEFAULT_BACKGROUND_COLOR,
                textColor: DEFAULT_TEXT_COLOR,
                title: "",
            },
        },
    };

    private isSelfServiceEnabled = false;

    constructor(
        readonly $rootScope: IExtendedRootScopeService,
        readonly OneProtocol: ProtocolService,
        readonly $q: ng.IQService,
        readonly store: IStore,
        private notificationService: NotificationService,
        private principal: Principal,
    ) {}

    public getIsSelfServiceEnabled = (): boolean => {
        return this.isSelfServiceEnabled;
    }

    public structureBranding = (data: IBrandingModel): IBrandingData => {
        return {
            InheritsFrom: data.InheritsFrom,
            IsInherited: data.IsInherited,
            branding: {
                logo: {
                    url: data.LogoData,
                    name: data.LogoFileName,
                },
                header: {
                    icon: {
                        url: data.IconData,
                        name: data.IconFileName,
                    },
                    backgroundColor: data.PrimaryColor,
                    textColor: data.SecondaryColor,
                    title: data.Title,
                },
            },
        };
    }

    public structureProject = (data: IProjectSettings): IProjectSettings => {
        return {
            Reminder: {
                Show: (data.DefaultReminderDays > 0),
                Value: data.DefaultReminderDays,
            },
            ProjectLevelApproval: data.ProjectLevelApproval,
            IsVersioningEnabled: data.IsVersioningEnabled,
            EnableThresholdTemplates: data.EnableThresholdTemplates,
            EnableRiskAnalysis: data.EnableRiskAnalysis,
            DefaultReviewer: data.DefaultReviewer,
            HasSeeded: data.HasSeeded,
            SectionAssignment: data.SectionAssignment,
            TaggingEnabled: data.TaggingEnabled,
            SelfServiceEnabled: data.SelfServiceEnabled,
        };
    }

    public defaultProject = (): IProjectSettings => {
        return {
            Reminder: {
                Show: true,
                Value: 3,
            },
            ProjectLevelApproval: false,
            IsVersioningEnabled: false,
            EnableThresholdTemplates: false,
            EnableRiskAnalysis: false,
            HasSeeded: false,
            SectionAssignment: false,
            TaggingEnabled: false,
        };
    }

    public defaultTask = (): any => {
        return {
            Enabled: false,
        };
    }

    public setBranding(custom: ICustomBranding): ng.IPromise<IProtocolPacket> {
        const brandData: ISettingsUpdateBrandingContract = this.structureUpdateBrandingContract(custom);
        const config: IProtocolConfig = this.OneProtocol.config("POST", `/settings/updateBranding`, [], brandData);
        const messages: IProtocolMessages = {
            Success: { custom: this.$rootScope.t("CustomSettingsSaved") },
            Error: { custom: this.$rootScope.t("SorryProblemSavingCustomSettings") },
         };
        return this.OneProtocol.http(config, messages).then((response) => {
            if (response.result && this.principal.getRoleName() !== UserRoles.Invited) {
                localStorage.setItem("OneTrust.ZenBrands", JSON.stringify(brandData));
            }
            return response;
        });
    }

    public getBranding(): ng.IPromise<IProtocolResponse<IBrandingData>> {
        this.store.dispatch({ type: BrandingActions.FETCHING_BRANDING });
        const config: IProtocolConfig = this.OneProtocol.config("GET", `/settings/getBranding`);
        const messages: IProtocolMessages = {
            Error: { custom: this.$rootScope.t("SorryProblemGettingCustomSettings") },
        };
        return this.OneProtocol.http(config, messages).then((res: IProtocolResponse<IBrandingModel>): IProtocolResponse<IBrandingData> => {
            if (res.result && this.principal.getRoleName() !== UserRoles.Invited) {
                localStorage.setItem("OneTrust.ZenBrands", JSON.stringify(res.data));
            }
            const brandingResult: IBrandingData = (res.result && res.data) ? this.structureBranding(res.data) : null;
            this.store.dispatch({
                type: BrandingActions.SET_BRANDING_DATA_AND_FETCHED,
                branding: brandingResult ? { ...brandingResult.branding } : null,
            });
            return { ...res, result: true, data: brandingResult };
        });
    }

    public deleteBranding(): ng.IPromise<IProtocolPacket> {
        const config: IProtocolConfig = this.OneProtocol.config("DELETE", `/settings/deleteBranding`);
        const messages: IProtocolMessages = {
            Error: { custom: this.$rootScope.t("SorryProblemResettingCustomSettings") },
        };
        return this.OneProtocol.http(config, messages);
    }

    public setProject(data: IProjectSettings): ng.IPromise<IProtocolPacket> {
        const reminderDays: number = data.Reminder && data.Reminder.Show ? data.Reminder.Value : null;
        const settings: IProjectSettings = {
            DefaultReminderDays: (reminderDays > 0) ? reminderDays : null,
            DefaultReviewer: data.DefaultReviewer,
            EnableRiskAnalysis: data.EnableRiskAnalysis,
            EnableThresholdTemplates: data.EnableThresholdTemplates,
            IsVersioningEnabled: data.IsVersioningEnabled,
            ProjectLevelApproval: data.ProjectLevelApproval,
            SectionAssignment: data.SectionAssignment,
            SelfServiceEnabled: data.SelfServiceEnabled,
            TaggingEnabled: data.TaggingEnabled,
        };
        const config: IProtocolConfig = this.OneProtocol.config("POST", `/settings/updateProject`, [], settings);
        const messages: IProtocolMessages = {
            Success: { custom: this.$rootScope.t("CustomSettingsSaved") },
            Error: { custom: this.$rootScope.t("SorryProblemSavingCustomSettings") },
        };
        return this.OneProtocol.http(config, messages);
    }

    public updateTagging(data: IProjectSettings): ng.IPromise<IProtocolPacket>  {
        const settings: ISettingsUpdateTagContract = {
            QuestionTaggingEnabled: data.QuestionTaggingEnabled,
            ProjectTaggingEnabled: data.ProjectTaggingEnabled,
        };
        const config: IProtocolConfig = this.OneProtocol.config("POST", `/settings/updateTag`, [], settings);
        const messages: IProtocolMessages = {
            Success: { custom: this.$rootScope.t("CustomSettingsSaved") },
            Error: { custom: this.$rootScope.t("SorryProblemSavingCustomSettings") },
        };
        return this.OneProtocol.http(config, messages);
    }

    public getProject(): ng.IPromise<IProtocolPacket> {
        const config: IProtocolConfig = this.OneProtocol.config("GET", `/settings/getProject`);
        const messages: IProtocolMessages = {
            Error: { custom: this.$rootScope.t("SorryProblemGettingCustomSettings") },
        };
        return this.OneProtocol.http(config, messages).then((res: IProtocolPacket): IProtocolPacket => {
            if (res.result) {
                return assign({}, res, { data: this.structureProject(res.data) });
            }
            return assign({}, res, { result: true, data: this.defaultProject() });
        });
    }

    public getTagging(): ng.IPromise<IProtocolPacket> {
        const config: IProtocolConfig = this.OneProtocol.config("GET", `/settings/getTag`);
        const messages: IProtocolMessages = {
            Error: { custom: this.$rootScope.t("SorryProblemGettingCustomSettings") },
        };
        return this.OneProtocol.http(config, messages).then((res: IProtocolPacket): IProtocolPacket => {
            if (res.result) {
                return res;
            }
            assign({}, res, { result: true, data: this.defaultProject() });
        });
    }

    public getAPI(tenantId: string): ng.IPromise<IProtocolPacket> {
        const config: IProtocolConfig = this.OneProtocol.config("GET", `/tenantapi/get/${tenantId}`);
        const messages: IProtocolMessages = {
            Error: { custom: this.$rootScope.t("SorryProblemGettingCustomSettings") },
        };
        return this.OneProtocol.http(config, messages);
    }

    public activateAPI(tenantId: string): ng.IPromise<IProtocolPacket> {
        const config: IProtocolConfig = this.OneProtocol.config("POST", `/tenantapi/activate/${tenantId}`);
        const messages: IProtocolMessages = {
            Success: { custom: this.$rootScope.t("EnabledApi") },
            Error: { custom: this.$rootScope.t("CouldNotEnableApi") },
        };
        return this.OneProtocol.http(config, messages);
    }

    public deactivateAPI(tenantId: string): ng.IPromise<IProtocolPacket> {
        const config: IProtocolConfig = this.OneProtocol.config("POST", `/tenantapi/deactivate/${tenantId}`);
        const messages: IProtocolMessages = {
            Success: { custom: this.$rootScope.t("DisabledApi") },
            Error: { custom: this.$rootScope.t("CouldNotDisableApi") },
        };
        return this.OneProtocol.http(config, messages);
    }

    public getDirectory(): ng.IPromise<IProtocolResponse<any>> {
        const config: IProtocolConfig = this.OneProtocol.config("GET", `/saml/getTenantSamlSettings`);
        const messages: IProtocolMessages = {
            Error: { custom: this.$rootScope.t("SorryProblemGettingCustomSettings") },
        };
        return this.OneProtocol.http(config, messages).then((response: IProtocolResponse<ISamlResponse>): IProtocolResponse<IAddSamlSettings> => {
            const samlSettingsResponse: IAddSamlSettings = response.data.Content;
            return { ...response, data: samlSettingsResponse };
        });
    }

    public setSamlSettings(settings: IAddSamlSettings): ng.IPromise<IProtocolResponse<IAddSamlSettings>> {
        if (settings.Id === null && !settings.IsEnabled) {
            this.notificationService.alertSuccess(this.$rootScope.t("Success"), this.$rootScope.t("SingleSignOnSettingsSaved"));
            const deferred: any = this.$q.defer();
            deferred.resolve({
                result: true,
                data: null,
            });
            return deferred.promise;
        }
        const url: string = settings.Id === null ? "/saml/addSamlSettings" : "/saml/updateSamlSettings";
        const config: IProtocolConfig = this.OneProtocol.config("POST", url, null, settings);
        const messages: IProtocolMessages = {
            Success: { custom: this.$rootScope.t("SingleSignOnSettingsSaved") },
            Error: { custom: this.$rootScope.t("ProblemSavingYourSingleSignOnSettings") },
        };
        return this.OneProtocol.http(config, messages);
    }

    public setSSOSettings(settings: IAddSamlSettings): ng.IPromise<IProtocolResponse<IAddSamlSettings>> {
        if (settings.Id === null && !settings.IsEnabled) {
            this.notificationService.alertSuccess(this.$rootScope.t("Success"), this.$rootScope.t("SingleSignOnSettingsSaved"));
            const deferred: any = this.$q.defer();
            deferred.resolve({
                result: true,
                data: null,
            });
            return deferred.promise;
        }
        const url: string = settings.Id === null ? "/saml/addSamlSettings" : "/saml/updateSamlSettings";
        const config: IProtocolConfig = this.OneProtocol.config("POST", url, null, settings);
        const messages: IProtocolMessages = {
            Success: { custom: this.$rootScope.t("SingleSignOnSettingsSaved") },
            Error: { custom: this.$rootScope.t("ProblemSavingYourSingleSignOnSettings") },
        };
        return this.OneProtocol.http(config, messages);
    }

    public resetSSOSettings(settings: IAddSamlSettings): ng.IPromise<IProtocolResponse<IAddSamlSettings>> {
        const config: IProtocolConfig = this.OneProtocol.config("DELETE", `/saml/ResetSamlSettings`);
        const messages: IProtocolMessages = {
            Success: { custom: this.$rootScope.t("SingleSignOnSettingsSaved") },
            Error: { custom: this.$rootScope.t("ProblemresettingYourSingleSignOnSettings") },
        };
        return this.OneProtocol.http(config, messages);
    }

    public saveAttributeMapping(attribute: IAddSamlSettings): ng.IPromise<IProtocolResponse<IAddSamlSettings>> {
        const config: IProtocolConfig = this.OneProtocol.config("POST", `/saml/addAttributeMapping`, [], attribute);
        const messages: IProtocolMessages = {
            Success: { custom: this.$rootScope.t("AttributeSaved") },
            Error: { custom: this.$rootScope.t("SorryThereWasAProblemSavingYourAttribute") },
        };
        return this.OneProtocol.http(config, messages);
    }

    public addCertificate(attachment: ICertificateFileUpload): ng.IPromise<IProtocolPacket> {
        const config: IProtocolConfig = this.OneProtocol.config("POST", `/certificate/add`, [], attachment);
        const messages: IProtocolMessages = {
            Success: { custom: this.$rootScope.t("CertificateSaved") },
            Error: { custom: this.$rootScope.t("SorryThereWasAProblemSavingYourCertificate") },
        };
        return this.OneProtocol.http(config, messages).then((response: IProtocolResponse<ISamlResponse>): IProtocolResponse<ISamlSettings> => {
            const samlSettingsResponse: any = response.data.Content;
            return { ...response, data: samlSettingsResponse };
        });
    }

    public deleteDirectory(): ng.IPromise<IProtocolPacket> {
        const config: IProtocolConfig = this.OneProtocol.config("DELETE", `/settings/deleteSaml`);
        const messages: IProtocolMessages = {
            Success: { custom: this.$rootScope.t("SingleSignOnSettingsHaveBeenDeleted") },
            Error: { custom: this.$rootScope.t("ProblemResettingSingleSignOnSettings") },
        };
        return this.OneProtocol.http(config, messages);
    }

    public getTranslation(language: string): ng.IPromise<IProtocolPacket> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("GET", `/api/globalization/v1/translations/seed/${language}`);
        const messages: IProtocolMessages = {
            Error: { custom: this.$rootScope.t("ProblemGettingYourCustomTranslations") },
        };
        return this.OneProtocol.http(config, messages);
    }

    public updateTranslation(parameters: any): ng.IPromise<IProtocolPacket> {
        const config: IProtocolConfig = this.OneProtocol.config("PUT", `/settings/updateTranslation`, [], parameters);
        const messages: IProtocolMessages = {
            Success: { custom: this.$rootScope.t("CustomTranslationsHaveBeenSaved") },
            Error: { custom: this.$rootScope.t("ProblemGettingYourCustomTranslations") },
        };
        return this.OneProtocol.http(config, messages);
    }

    public getTimeFormat(): ng.IPromise<IProtocolPacket> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("GET", `/api/globalization/v1/ui-settings?key=timeFormat`);
        const messages: IProtocolMessages = {
            Error: { custom: this.$rootScope.t("ErrorRetrievingTimeFormat") },
        };
        return this.OneProtocol.http(config, messages);
    }

    public getDateFormat(): ng.IPromise<IProtocolPacket> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("GET", `/api/globalization/v1/ui-settings?key=dateFormat`);
        const messages: IProtocolMessages = {
            Error: { custom: this.$rootScope.t("ErrorRetrievingDateFormat") },
        };
        return this.OneProtocol.http(config, messages);
    }

    public updateDateTimeFormat(params: any): ng.IPromise<IProtocolPacket> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("PUT", `/api/globalization/v1/ui-settings`, [], params);
        const messages: IProtocolMessages = {
            Success: { custom: this.$rootScope.t("DateTimeUpdated") },
            Error: { custom: this.$rootScope.t("ErrorUpdatingDateTimeFormats") },
        };
        return this.OneProtocol.http(config, messages);
    }

    public getHelp(): ng.IPromise<IProtocolResponse<IHelpSettings>> {
        const config: IProtocolConfig = this.OneProtocol.config("GET", `/support/settings`);
        const messages: IProtocolMessages = { Error: { Hide: true }};
        return this.OneProtocol.http(config, messages);
    }

    public saveHelp(request: IHelpSettings): ng.IPromise<IProtocolResponse<IHelpSettings>> {
        const config: IProtocolConfig = this.OneProtocol.config("PUT", `/support/settings`, null, request);
        const messages: IProtocolMessages = {
            Success: { custom: this.$rootScope.t("SettingsSavedSuccessfully") },
            Error: { custom: this.$rootScope.t("ErrorSavingSettings") },
        };
        return this.OneProtocol.http(config, messages);
    }

    public addOrgMappingAttribute(request: ISamlOrgGroupMapping): ng.IPromise<IProtocolResponse<ISamlOrgMappingResponse>> {
        const config: IProtocolConfig = this.OneProtocol.config("POST", `/saml/AttributeLookups`, null, request);
        const messages: IProtocolMessages = {
            Success: { custom: this.$rootScope.t("OrgAttributeSavedSuccessfully") },
            Error: { custom: this.$rootScope.t("ErrorAddingOrgAttribute") },
        };
        return this.OneProtocol.http(config, messages);
    }

    public getMappingAttribute(params: ISamlOrgGroupMappingTable, key?: string): ng.IPromise<IProtocolResponse<ISamlOrgMappingResponse>> {
        const config: IProtocolConfig = params.attributeValue
            ? this.OneProtocol.config("GET", `/saml/AttributeLookups/${SamlAttributeType.Organization}/search`, params)
            : this.OneProtocol.config("GET", `/saml/AttributeLookups`, params);
        const messages: IProtocolMessages = {
            Error: { custom: this.$rootScope.t("ErrorRetrivingOrgAttributes") },
        };
        return this.OneProtocol.http(config, messages);
    }

    public updateMappingAttribute(params: ISamlOrgGroupMapping, attrId: string): ng.IPromise<IProtocolResponse<ISamlOrgGroupMapping>> {
        const config: IProtocolConfig = this.OneProtocol.config("PUT", `/saml/AttributeLookups/${attrId}`, null, params);
        const messages: IProtocolMessages = {
            Success: { custom: this.$rootScope.t("UpdatedOrgAttributeSuccessfully") },
        };
        return this.OneProtocol.http(config, messages)
            .then((response: IProtocolResponse<ISamlOrgGroupMapping>): IProtocolResponse<ISamlOrgGroupMapping> => {
                if (!response.result) {
                    if (response.status === StatusCodes.Conflict) {
                        this.notificationService.alertError(this.$rootScope.t("Error"), this.$rootScope.t("AttributeOrOrgExists"));
                    } else {
                        this.notificationService.alertError(this.$rootScope.t("Error"), this.$rootScope.t("ErrorUpdatingOrgAttributes"));
                    }
                }
                return response;
            });
    }

    public deleteMappingAttribute(attrId: string): ng.IPromise<IProtocolResponse<ISamlOrgGroupMapping>> {
        const config: IProtocolConfig = this.OneProtocol.config("DELETE", `/saml/AttributeLookups/${attrId}`);
        const messages: IProtocolMessages = {
            Success: { custom: this.$rootScope.t("DeletedOrgAttributeSuccessfully") },
            Error: { custom: this.$rootScope.t("ErrorDeletingOrgAttributes") },
        };
        return this.OneProtocol.http(config, messages);
    }

    public structureUpdateBrandingContract(custom: ICustomBranding): ISettingsUpdateBrandingContract {
        const header: IBrandingHeader = custom.branding.header || this.defaultBranding.branding.header;
        const backgroundColor: string = header.backgroundColor || "";
        const textColor: string = header.textColor ? header.textColor : "";

        const logo: ILogo = custom.branding.logo || this.defaultBranding.branding.logo;
        let logoUrl: string = logo ? (logo.url || "") : "";
        const logoName: string = logo ? (logo.name || "") : "";
        const title: string = header.title || "";
        if (logoUrl.indexOf("data:") === -1) {
            logoUrl = "";
        }

        const icon: ILogo = header.icon || null;
        let iconUrl: string = icon ? (icon.url || "") : "";
        const iconName: string = icon ? (icon.name || "") : "";

        if (iconUrl.indexOf("data:") === -1) {
            iconUrl = "";
        }

        return {
            LogoData: logoUrl || "",
            LogoFileName: logoName || "",
            IconData: iconUrl || "",
            IconFileName: iconName || "",
            PrimaryColor: backgroundColor || "",
            SecondaryColor: textColor || "",
            TertiaryColor: "",
            Title: title ? title : "",
        };
    }

}

export default Settings;
