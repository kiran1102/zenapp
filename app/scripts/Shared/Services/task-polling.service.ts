// Rxjs
import {
    Observable,
    Subject,
    from,
    interval,
    merge,
    timer,
} from "rxjs";
import {
    switchMap,
    take,
    takeUntil,
} from "rxjs/operators";

// Interfaces
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import { IPageable } from "interfaces/pagination.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";
import { ITaskListRequest } from "interfaces/tasks.interface";
import { IStringMap } from "interfaces/generic.interface";

// Redux
import { IStore } from "interfaces/redux.interface";
import { getCurrentUser } from "oneRedux/reducers/user.reducer";

// Services
import { TasksService } from "sharedServices/tasks.service";

export class TaskPollingService {

    static $inject: string[] = [
        "$rootScope",
        "store",
        "TASKS",
    ];

    private pollingObservable: Observable<IProtocolResponse<IPageable>> = null;
    private tasksCount: number;

    private stopPollingInterval = 300000;
    private resetPolling: Subject<string> = new Subject();
    private stopPolling: Observable<number> = interval(this.stopPollingInterval).pipe(
        take(1),
    );

    private defaultConfig: IStringMap<number> = {
        initialDelay: 30000,
        interval: 30000,
    };

    constructor(
        private readonly $rootScope: IExtendedRootScopeService,
        private readonly store: IStore,
        private readonly TasksService: TasksService,
    ) { }

    getTasksCount(): ng.IPromise<IProtocolResponse<IPageable>> {

        const currentUser = getCurrentUser(this.store.getState());

        const tasksRequest: ITaskListRequest = {
            filterOptions: [],
            pageNumber: 0,
            pageSize: 10000,
            sortOptions: [],
            user: currentUser.Id,
        };

        return this.TasksService.listTasks(tasksRequest).then(
            (response: IProtocolResponse<IPageable>) => {
                this.tasksCount = response.result && response.data && response.data.content ? this.TasksService.filterTaskCount(response.data.content) : 0;
                this.$rootScope.$broadcast("tasks-updated", this.tasksCount);
                return response;
            },
        );

    }

    startPolling(initialDelay: number = this.defaultConfig.initialDelay, interval: number = this.defaultConfig.interval): void {
        if (this.pollingObservable && this.hasDefaultConfig(initialDelay, interval)) {
            this.resetPolling.next();
        } else {
            this.pollingObservable = this.getPollingObservable(initialDelay, interval);
        }
        this.pollingObservable.subscribe();
    }

    private hasDefaultConfig(initialDelay: number, interval: number): boolean {
        return initialDelay === this.defaultConfig.initialDelay && interval === this.defaultConfig.interval;
    }

    private getPollingObservable(initialDelay: number = this.defaultConfig.initialDelay, interval: number = this.defaultConfig.interval): Observable<IProtocolResponse<IPageable>> {
        return timer(initialDelay, interval).pipe(
            switchMap(() => from(this.getTasksCount())),
            takeUntil(merge(this.stopPolling, this.resetPolling)),
        );
    }

}
