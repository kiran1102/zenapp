import i18next from 'i18next';

"use strict";

export default function Translations($q, $localStorage, Settings, CONSTANTS) {

    var library = {};
    var _myTranslations = initTranslateFromStorage();
    // if(_myTranslations.length > 0) {
    initLibrary(_myTranslations);
    // }

    function initLibrary(translations) {
        library["LanguageCode"] = translations.LanguageCode;
        if (_.isArray(translations.TranslationDictionary)) {
            _.each(translations.TranslationDictionary, function(item) {
                library[item.Key] = item.Value;
            });
        }

        i18next.init({
            lng: library.LanguageCode,
            resources: {
                en: {
                    translation: library
                }
            }
        });
    }

    return {
        getTranslateFromStorage: getTranslateFromStorage,
        setTranslateToStorage: setTranslateToStorage,
        removeTranslateFromStorage: removeTranslateFromStorage,
        getTranslate: getTranslate,
        setTranslate: setTranslate,
        sendToServer: sendToServer,
        getFromServer: getFromServer,
        deleteTranslate: deleteFromServer,
        getItemTranslation: getItemTranslation,
        compileTranslations: compileTranslations
    };

    function compileTranslations() {
        var translations = {};

        _.each(Object.keys(CONSTANTS.TERMS), function(term) {
            translations[term] = i18next.t(term);
        });

        return translations;
    }

    function getItemTranslation() {
        return i18next;
    }

    function getTranslateFromStorage() {
        _myTranslations = $localStorage.ZenTranslate ? $localStorage.ZenTranslate : getTranslate(true);
        $q.when(_myTranslations).then(function (translations) {
            initLibrary(translations);
        });
        return _myTranslations;
    }

    function removeTranslateFromStorage() {
        $localStorage.ZenTranslate = null;
    }

    function initTranslateFromStorage() {
        _myTranslations = $localStorage.ZenTranslate ? $localStorage.ZenTranslate : {};
        return _myTranslations;
    }

    function setTranslateToStorage(appTranslate) {
        $localStorage.ZenTranslate = appTranslate;
        return true;
    }

    function getTranslate(force) {
        var deferred = $q.defer();
        if (force) {
            getFromServer()
                .then(function (translations) {
                    deferred.resolve(translations);
                });
            return deferred.promise;
        } else {
            // Check if our Translate is loaded.
            if (!_.isUndefined(_myTranslations) && _myTranslations !== null && _myTranslations !== "") {
                deferred.resolve(_myTranslations);
                return deferred.promise;
            }
            // Check if our Translate exists on $localStorage
            else if (!_.isUndefined(getTranslateFromStorage()) && getTranslateFromStorage() !== null && getTranslateFromStorage() !== "") {
                _myTranslations = getTranslateFromStorage();
                deferred.resolve(_myTranslations);
                return deferred.promise;
            }
            // Finally, just get from the server, set $localStorage and _myTranslations, and return the new Translate.
            else {
                getFromServer()
                    .then(function (translations) {
                        deferred.resolve(translations);
                    });
                return deferred.promise;
            }
        }
    }

    function setTranslate(customTranslate) {
        var deferred = $q.defer();
        if (!_.isUndefined(customTranslate) && customTranslate !== null) {
            sendToServer(customTranslate)
                .then(function (translations) {
                    setTranslateToStorage(translations);
                    _myTranslations = getTranslateFromStorage();
                    deferred.resolve(_myTranslations);
                });
            return deferred.promise;
        } else {
            deferred.resolve(_myTranslations);
            return deferred.promise;
        }
    }

    function sendToServer(translations) {
        var deferred = $q.defer();
        Settings.updateTranslation(translations)
            .then(function (response) {
                if (response.result) {
                    setTranslateToStorage(translations);
                    _myTranslations = getTranslateFromStorage();
                    _myTranslations.result = true;
                } else {
                    // Replace with data from server.
                    _myTranslations.result = false;
                }
                deferred.resolve(_myTranslations);
            });
        return deferred.promise;
    }

    function getFromServer() {
        var deferred = $q.defer();
        Settings.getTranslation("en-us")
            .then(function (response) {
                if (response.result) {
                    setTranslateToStorage(response.data);
                    _myTranslations = getTranslateFromStorage();
                    // _myTranslations.result = true;
                    deferred.resolve(_myTranslations);
                } else {
                    _myTranslations = getTranslateFromStorage();
                    // _myTranslations.result = false;
                    deferred.resolve(_myTranslations);
                }
            });
        return deferred.promise;
    }

    function deleteFromServer() {
        var deferred = $q.defer();
        Settings.deleteTranslate()
            .then(function (response) {
                deferred.resolve(response);
            });
        return deferred.promise;
    }
}

Translations.$inject = ["$q", "$localStorage", "Settings", "CONSTANTS"];
