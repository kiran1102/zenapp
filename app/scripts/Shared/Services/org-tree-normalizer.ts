
import {
    IOrg,
    IOrgList,
    IOrgTable,
    IOrganization,
} from "interfaces/org.interface";

export default class OrgTreeNormalizer {
    private _orgTree: any;
    private _orgList: IOrgList;
    private _orgTable: IOrgTable;

    constructor(orgTree: any) {
        this._orgTree = orgTree;
    }

    private normalizeToArray(orgNode: any, hierarchyLevel: number, acc: IOrgList): any {
        const level: number = hierarchyLevel;
        const childrenIds = (org: IOrganization): string[] => org.Children ? org.Children.map((child: any): string => child.Id) : [];
        const generateOrg = (org: IOrganization): IOrg => ({
            id: (org) ? org.Id : null,
            name: (org) ? org.Name : null,
            privacyOfficerId: (org) ? org.PrivacyOfficerId : null,
            privacyOfficerName: (org) ? org.PrivacyOfficerName : null,
            canDelete: (org) ? org.CanDelete : null,
            parentId: (org) ? org.ParentId : null,
            childrenIds: (org) ? childrenIds(org) : null,
            level,
        });

        acc.push(generateOrg(orgNode));

        if (orgNode.Children && orgNode.Children.length) {
            for (const child of orgNode.Children) {
                this.normalizeToArray(child, level + 1, acc);
            }
        }
        return acc;
    }

    private normalizeToTable(parent: any, orgNode: any, hierarchyLevel: number, acc: any): any {
        const level: number = hierarchyLevel;
        const childrenIds = (org: IOrganization): string[] => org.Children ? org.Children.map((child: any): string => child.Id) : [];
        const generateOrg = (org: IOrganization): IOrg => ({
            id: (org) ? org.Id : null,
            name: (org) ? org.Name : null,
            privacyOfficerId: (org) ? org.PrivacyOfficerId : null,
            privacyOfficerName: (org) ? org.PrivacyOfficerName : null,
            canDelete: (org) ? org.CanDelete : null,
            parentId: (org) ? org.ParentId : null,
            childrenIds: (org) ? childrenIds(org) : null,
            level,
        });

        acc[orgNode.Id] = generateOrg(orgNode);

        if (orgNode.Children && orgNode.Children.length) {
            for (const child of orgNode.Children) {
                this.normalizeToTable(orgNode, child, level + 1, acc);
            }
        }
        return acc;
    }

    get orgList(): IOrgList {
        if (!this._orgList) {
            const accumulator: IOrgList = [];
            this._orgList = this.normalizeToArray(this._orgTree, 0, accumulator);
        }
        return this._orgList;
    }

    set orgList(orgList: IOrgList) {
        throw new Error("overwriting organization this way is forbidden!");
    }

    get orgTable(): IOrgTable {
        if (!this._orgTable) {
            const accumulator: IOrgTable = { rootId: this._orgTree.Id };
            this._orgTable = this.normalizeToTable(null, this._orgTree, 0, accumulator);
        }
        return this._orgTable;
    }

    set orgTable(orgTable: IOrgTable) {
        throw new Error("overwriting organization this way is forbidden!");
    }

    get orgTree(): any {
        return this._orgTree;
    }

    set orgTree(orgTree: any) {
        throw new Error("overwriting organization this way is forbidden!");
    }
}
