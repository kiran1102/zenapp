import { IExtendedRootScopeService } from "interfaces/root-scope.interface";

export default class TagService {
    static $inject: string[] = ["$rootScope", "OneProtocol"];

    private HTTP: any;
    private translations: any;
    private tagAddPending = false;

    constructor(
        private $rootScope: IExtendedRootScopeService,
        readonly OneProtocol: any) {
        this.translations = {
            errorGettingTags: $rootScope.t("ErrorGettingTags"),
            errorAddingTag: $rootScope.t("ErrorAddingTag"),
        };
    }

    public getTags(filter: string, count: number): ng.IPromise<any> {
        const params: any = { filter, count };
        const config: any = this.OneProtocol.config("GET", `/tag/get/`, params);
        const messages: any = { Error: { custom: this.translations.errorGettingTags } };
        return this.OneProtocol.http(config, messages);
    }

    public addTag(createRequest: any): any {
        this.tagAddPending = true;
        const config: any = this.OneProtocol.config("POST", `/tag/add/`, [], createRequest);
        const messages: any = { Error: { custom: this.translations.errorAddingTag } };
        return this.OneProtocol.http(config, messages);
    }

    public updateProject(updateProject: any): ng.IPromise<any> {
        const config: any = this.OneProtocol.config("POST", `/tag/UpdateProject/`, [], updateProject);
        const messages: any = { Error: { custom: this.translations.errorAddingTag } };
        return this.OneProtocol.http(config, messages);
    }

    public updateQuestion(updateQuestion: any): ng.IPromise<any> {
        const config: any = this.OneProtocol.config("POST", `/tag/UpdateQuestion/`, [], updateQuestion);
        const messages: any = { Error: { custom: this.translations.errorAddingTag } };
        return this.OneProtocol.http(config, messages);
    }

    private isTagAddPending = (): boolean => this.tagAddPending;

}
