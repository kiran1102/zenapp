
"use strict";

export default function ReadinessAssessment($http, $q, $rootScope, NotificationService, ENUMS) {

    var templateTypes = ENUMS.TemplateTypes;
    var projectTranslation = $rootScope.customTerms.Project;
    var projectsTranslation = $rootScope.customTerms.Projects;
    var errorMessages = {
        show: "Error retrieving " + projectTranslation + ".",
        create: "Error creating " + projectTranslation + ".",
        copy: "Error copying " + projectTranslation + ".",
        update: "Error updating " + projectTranslation + ".",
        delete: "Error deleting " + projectTranslation + ".",
        complete: "Error completing " + projectTranslation + ".",
        submit: "Error submitting " + projectTranslation + ".",
        info: "Error requesting for more information for this " + projectTranslation + ".",
        start: "Error starting" + projectTranslation + ".",
        approve: "Error getting " + projectTranslation + " approval status.",
        removeSeed: "Error deleting sample " + projectsTranslation + "."
    };

    var errorCallback = function (alertText) {
        return function (response) {
            var packet = {
                result: false,
                data: alertText
            };
            NotificationService.alertError("Error", alertText);
            return packet;
        };
    };

    var list = function () {
        return $http.get("/project/getReadiness")
            .then(function successCallback(response) {
                var packet = {
                    result: true,
                    data: response.data
                };
                return packet;
            }, errorCallback(errorMessages.show));
    };

    var create = function () {
        return $http.post("/gallery/sample/" + templateTypes.Readiness)
            .then(function successCallback(response) {
                var packet = {
                    result: true,
                    data: response.data
                };
                return packet;
            }, errorCallback(errorMessages.show));
    };

    var service = {
        list: list,
        create: create
    };
    return service;
}

ReadinessAssessment.$inject = ["$http", "$q", "$rootScope", "NotificationService", "ENUMS"];
