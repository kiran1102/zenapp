import { forEach } from "lodash";

export function getCommaSeparatedList(arr: any[], transform?: (any) => string): string {
    let newString = "";
    const recordValueLength: number = arr.length;
    forEach(arr, (option: any, index: number): void => {
        if (index === recordValueLength - 1) {
            newString += transform ? transform(option) : option;
        } else {
            newString += `${transform ? transform(option) : option}, `;
        }
    });
    return newString;
}
