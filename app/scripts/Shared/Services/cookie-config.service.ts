
export default class CcConfigService {
    static $inject: string[] = ["$http", "$q", "OPTANON_URL"];

    private optanonUrl: string;

    constructor(readonly $http: ng.IHttpService, readonly $q: ng.IQService, OPTANON_URL: string) {
        this.optanonUrl = OPTANON_URL;
        this.$http.get("/system/optanonurl").then((response: any): void => {
            this.optanonUrl = response.data || OPTANON_URL;
        });
    }

    public getOptanonUrl = (): string => {
        return this.optanonUrl;
    }
}
