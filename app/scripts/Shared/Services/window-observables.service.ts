import { isFunction } from "lodash";
import { Observable, fromEvent } from "rxjs";
import { share } from "rxjs/operators";
import { IWindowObservablesService } from "interfaces/window-observables.interface";

interface IObservableEventListenerMap {
    [index: string]: Observable<EventListener>;
}

export default class WindowObservablesService implements IWindowObservablesService {

    private observers: IObservableEventListenerMap = {};

    private observerSingletonConstructors: any = {
        RESIZE: (): Observable<EventListener> => {
            this.observers["RESIZE"] = fromEvent<EventListener>(window, "resize").pipe(share());
            return this.observers["RESIZE"];
        },
    };

    public get(observerKey: string): Observable<EventListener> {
        if (this.observers[observerKey]) {
            return this.observers[observerKey];
        } else if (isFunction(this.observerSingletonConstructors[observerKey])) {
           return this.observerSingletonConstructors[observerKey]();
        }
        return null;
    }

}
