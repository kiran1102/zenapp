// Services
import { Content } from "sharedModules/services/provider/content.service";

// Interfaces
import { IStringMap } from "interfaces/generic.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";
import { IMarketingDefault } from "interfaces/marketing.interface";

export class MarketingService {
    static $inject: string[] = ["$q", "Content"];

    constructor(
        private $q: ng.IQService,
        private content: Content,
    ) {}

    public getMarketing(): ng.IPromise<IProtocolResponse<IStringMap<string>>> {
        return this.content.GetContent().then((data: IStringMap<string>) => {
            return {
                result: Boolean(data),
                data,
            };
        });
    }

    public getMarketingDefaults(): IMarketingDefault {
        return {
            backgroundImage: "",
            buttonText: "",
            copy: "",
            headline: "",
            loginLogo: "images/logo.png",
            marketingLogo: "",
            marketingUrl: "",
            registerUrl: "",
        };
    }

    public checkImage(src: string): ng.IPromise<any> {
        const deferred = this.$q.defer();
        const image: HTMLImageElement = new Image();
        image.onerror = (): void => {
            deferred.resolve(false);
        };
        image.onload = (): void => {
            deferred.resolve(true);
        };
        image.src = src;
        return deferred.promise;
    }
}

export default MarketingService;
