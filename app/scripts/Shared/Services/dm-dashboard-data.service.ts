import * as _ from "lodash";
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import { IAssessmentParams } from "app/scripts/DataMapping/Services/Assessment/dm-assessments.service";

export default class DmDashboardData {

    static $inject: string[] = ["$rootScope", "$filter", "DMAssessments", "ENUMS"];

    private translate: any;
    private AssessmentStates: any;
    private AssessmentStateLabels: any;
    private AssessmentStateOrder: any;
    private parentTreeFilter: any;
    private graphData: any;
    private dataContent: any;

    constructor(
        $rootScope: IExtendedRootScopeService,
        readonly $filter: ng.IFilterService,
        readonly DMAssessments: any,
        ENUMS: any,
    ) {
        this.translate = $rootScope.t;
        this.AssessmentStates = ENUMS.DMAssessmentStates;
        this.AssessmentStateLabels = ENUMS.DMAssessmentStateLabels;
        this.AssessmentStateOrder = ENUMS.DMAssessmentStatesOrder;
        this.parentTreeFilter = $filter("IsInParentTree");
    }

    public formatAssessmentsData(orgTree: any, orgId?: string): any {

        const params: IAssessmentParams = {
            page: 0,
            size: 100000,
            sort: "createdDate,DESC",
        };
        return this.DMAssessments.getAssessments(params).then((response: any): any[] => {
            this.dataContent = response.data.content;
            const statusData: any[] = [];
            const assessmentsWithOrgs: any[] = this.formatAssessmentOrg(this.dataContent);
            const filteredAssessments: any[] = this.parentTreeFilter(assessmentsWithOrgs, orgTree, orgId, "OrgGroupId", "down");
            const totalNumAssesments: number = filteredAssessments.length;

            _.forEach(this.AssessmentStates, (status: string, translation: string): void => {
                const numAssessments: number = _.filter(filteredAssessments, {statusId: status} as any).length;
                const assessmentData: any = {
                    statusId: status,
                    translation,
                    legendLabel: this.translate(this.AssessmentStateLabels[status]),
                    magnitude: status === this.AssessmentStates.All ? totalNumAssesments : numAssessments,
                    percentage: Math.round((numAssessments / totalNumAssesments) * 100),
                    show: !(status === this.AssessmentStates.Archived || status === this.AssessmentStates.NeedsAttention),
                };
                statusData.push(assessmentData);
            });
            return _.filter( _.sortBy(statusData, (status: any): number => this.AssessmentStateOrder[status.translation]), "show");
        });
    }

    private formatAssessmentOrg(assessments: any[]): any[] {
        const assessmentsWithOrgs: any[] = _.filter(assessments, "orgGroup");
        _.forEach(assessmentsWithOrgs, (assessment: any): void => {
            assessment.OrgGroupId = assessment.orgGroup.id;
        });
        return assessmentsWithOrgs;
    }
}
