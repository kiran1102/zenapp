import { Content } from "sharedModules/services/provider/content.service";
import { Permissions } from "modules/shared/services/helper/permissions.service";
import { McmModalService } from "modules/mcm/mcm-modal.service";

export class AppController {
    static $inject: string[] = [
        "$scope",
        "$state",
        "$q",
        "Permissions",
        "Content",
        "Upgrade",
        "McmModalService",
    ];

    constructor(
        $scope: any,
        $state: ng.ui.IStateService,
        $q: ng.IQService,
        permissions: Permissions,
        content: Content,
        upgrade: any,
        mcmModalService: McmModalService,
    ) {
        $scope.Permissions = permissions;
        $scope.OneContent = content;
        $scope.Upgrade = upgrade;

        $scope.UpgradeModule = (module, upgradeState) => {
            // TODO: Pass relevant key when the content is ready
            mcmModalService.openMcmModal("Upgrade");
        };
    }

}
