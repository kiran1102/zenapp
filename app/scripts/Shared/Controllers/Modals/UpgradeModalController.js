
export function UpgradeModalCtrl($state, $scope, $uibModalInstance, Options, Upgrade, currentUser, $sce, Content) {

    $scope.loading = false;
    $scope.Options = Options;
    $scope.returnView = null;

    $scope.customModalOptions = Boolean(Options.modalOptions);
    if (Options.modalOptions) {
        $scope.customModalTitle = Options.modalOptions.modalTitle;
        $scope.customModalBodyText = Options.modalOptions.modalText;
    }

    $scope.upgradeViews = Upgrade.ModuleStates;

    $scope.currentView = Options.state || (Options.video ? Upgrade.ModuleStates.Video : Upgrade.ModuleStates.Message);
    Options.name = Options.name.replace(/([a-z])([A-Z])/g, "$1 $2");
    $scope.upgradeForm = {
        firstName: currentUser.FirstName || "",
        lastName: currentUser.LastName || "",
        email: currentUser.Email || "",
        phone: "",
        module: Options.name,
    };

    $scope.errors = {
        email: false,
        firstName: false,
        lastName: false
    };

    $scope.currentViewIs = function (view) {
        return $scope.currentView === view;
    };

    $scope.changeView = function (view, returnView) {
        $scope.returnView = returnView || null;
        $scope.currentView = view;
    };

    var validateForm = function (form) {
        var emailValidated = validateEmail(form);
        var nameValidated = validateName();
        return emailValidated && nameValidated;
    };

    var validateEmail = function (form) {
        if (form.email.$invalid) {
            $scope.errors.email = true;
            $scope.loading = false;
        } else {
            $scope.errors.email = false;
        }
        return !$scope.errors.email;
    };

    var validateName = function () {
        if ($scope.upgradeForm.firstName) {
            $scope.errors.firstName = false;
        } else {
            $scope.errors.firstName = true;
        }

        if ($scope.upgradeForm.lastName) {
            $scope.errors.lastName = false;
        } else {
            $scope.errors.lastName = true;
        }

        return !$scope.errors.firstName && !$scope.errors.lastName;
    };

    $scope.sendUpgradeForm = function (form) {
        if (validateForm(form)) {
            $scope.loading = true;
            Upgrade.sendUpgradeForm($scope.upgradeForm)
                .then(function () {
                    $scope.changeView(Upgrade.ModuleStates.Submitted);
                    $scope.loading = false;
                });
        }
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss("cancel");
    };

    var init = function () {
        if (Options.contactSales) {
            $scope.changeView($scope.upgradeViews.Form)
        }
    };

    init();

    $scope.UpgradeModalFeatureList = $sce.trustAsHtml(Content.GetKey("UpgradeModalFeatureList"));
    $scope.UpgradeModalSalesLink = Content.GetKey("UpgradeModalSalesLink");
}

UpgradeModalCtrl.$inject = ["$state", "$scope", "$uibModalInstance", "Options", "Upgrade", "currentUser", "$sce", "Content"];
