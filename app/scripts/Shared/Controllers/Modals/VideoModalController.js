
export default function WelcomeCtrl($scope, VideoId, Title, $uibModalInstance) {

    $scope.videoId = VideoId;
    $scope.title = Title;

    $scope.cancel = function () {
        $uibModalInstance.dismiss("cancel");
    }
}

WelcomeCtrl.$inject = ["$scope", "VideoId", "Title", "$uibModalInstance"];
