
function templateQuestionsController(ENUMS, Permissions) {
	var vm = this;
    vm.questionTypes = ENUMS.QuestionTypes;
    vm.permissions = {
        TemplatesQuestionAdd: Permissions.canShow("TemplatesQuestionAdd"),
        TemplatesQuestionAddUpgrade: Permissions.canShow("TemplatesQuestionAddUpgrade"),
        TemplatesQuestionItemEdit: Permissions.canShow("TemplatesQuestionItemEdit"),
        TemplatesQuestionItemEditUpgrade: Permissions.canShow("TemplatesQuestionItemEditUpgrade"),
        TemplatesQuestionItemDelete: Permissions.canShow("TemplatesQuestionItemDelete"),
        TemplatesQuestionItemDeleteUpgrade: Permissions.canShow("TemplatesQuestionItemDeleteUpgrade"),
        TemplatesQuestionItemPreview: Permissions.canShow("TemplatesQuestionItemPreview"),
        TemplatesQuestionItemConditionUpgrade: Permissions.canShow("TemplatesQuestionItemConditionUpgrade"),
    }
}

templateQuestionsController.$inject = ["ENUMS", "Permissions"];

export default function templateQuestions() {
	var directive = {
		template: require("views/Shared/Directives/templateQuestions.jade"),
		restrict: "E",
		controller: templateQuestionsController,
		controllerAs: "vm",
		bindToController: true,
		scope: {
			questionGroups: "=",
			section: "=",
			template: "=",
			reorderOptions: "=",
			readOnly: "=",
			editMode: "=",
			events: "=",
		}
	};

	return directive;
}