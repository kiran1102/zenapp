
export default function fileOnChange(): any {
    return {
        require: "ngModel",
        restrict: "A",
        link: (scope: ng.IScope, elem: any, attrs: any, ngModel: any): void => {
            elem.bind("change", (event: any): void => {
                const files: any[] = event.target.files;
                const file: any = files[0];
                ngModel.$setViewValue(file);
            });
        },
    };
}
