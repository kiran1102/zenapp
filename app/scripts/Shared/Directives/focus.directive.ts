export default function focusMe(): any {
    return {
        link: ($scope: any, elem: any, attrs: any): void => {
            elem.bind("keydown", (e: any): void => {
                const code = e.keyCode || e.which;

                if (code === 13) {
                    e.preventDefault();
                    elem.next().focus();
                }
            });
        },
        restrict: "A",
    };
}
