
function questionChangeController(Response, ENUMS, $rootScope, store) {
    var vm = this;

    vm.versionStates = ENUMS.QuestionVersionStates;

    var getState = function () {
        if (vm.question.IsFromCurrentVersion) {
            return vm.versionStates.New;
        } else if (vm.question.HasVersionResponseChanged) {
            return vm.versionStates.Change;
        } else if (vm.question.HasVersionResponseChanged === false) {
            return vm.versionStates.DontChange;
        }
        return vm.versionStates.Undecided;
    };
    vm.changeQuestion = function () {
        OneConfirm("", $rootScope.t("ChangeAnswerUnableToMarkUnchanged")).then(
            function (result) {
                if (result) {
                    vm.question.versionState.isDisabled = false;
                    vm.question.HasVersionResponseChanged = true;
                    vm.question.versionState.state = vm.versionStates.Change;
                    Response.create({
                        QuestionId: vm.question.Id,
                        Responses: vm.question.Responses ? vm.question.Responses : [],
                        HasVersionResponseChanged: true,
                        IsVersionResponseReviewed: false
                    }).then(function (response) {
                        if (!response.result) {
                            vm.question.versionState.isDisabled = true;
                            vm.question.HasVersionResponseChanged = null;
                            vm.question.versionState.state = vm.versionStates.undecided;
                        }
                        vm.checkSubmitState();
                        store.dispatch({ type: "UNHANDLED_ACTION: questionChangeToggleDirective" });
                    });
                }
            }
        );
    };

    vm.dontChangeQuestion = function () {
        if (vm.question.versionState.state !== vm.versionStates.DontChange) {
            vm.question.versionState.state = vm.versionStates.DontChange;
            vm.question.HasVersionResponseChanged = false;
            Response.create({
                QuestionId: vm.question.Id,
                Responses: vm.question.Responses ? vm.question.Responses : [],
                HasVersionResponseChanged: false,
                IsVersionResponseReviewed: false
            }).then(function (response) {
                if (!response.result) {
                    vm.question.versionState.state = vm.versionStates.Undecided;
                    vm.question.HasVersionResponseChanged = null;
                }
                if (vm.project.StateDesc === vm.projectStates.InProgress ||
                    vm.project.StateDesc === vm.projectStates.InfoNeeded) {
                    vm.getProjectProgress(vm.project.Id, vm.project.Version, vm.question.Id);
                }
                vm.checkSubmitState();
                store.dispatch({ type: "UNHANDLED_ACTION: questionChangeToggleDirective" });
            });
        }
    };

    vm.showButtons = function () {
        return (vm.question.versionState.state === vm.versionStates.DontChange) ||
            (vm.question.versionState.state === vm.versionStates.Undecided);
    };

    vm.$onInit = function () {
        vm.question.versionState = {
            isDisabled: !(vm.question.IsFromCurrentVersion || vm.question.HasVersionResponseChanged),
            state: getState()
        };
        vm.checkSubmitState();
    };
}

questionChangeController.$inject = ["Response", "ENUMS", "$rootScope", "store"];

export default function questionChangeToggle() {
    var directive = {
        template: require("views/Shared/Directives/questionChangeToggle.jade"),
        restrict: "E",
        controller: questionChangeController,
        controllerAs: "vm",
        bindToController: true,
        replace: true,
        scope: {
            question: "=",
            changeAnswer: "=",
            checkSubmitState: "=",
            project: "=",
            projectStates: "=",
            getProjectProgress: "="
        }
    };

    return directive;
}
