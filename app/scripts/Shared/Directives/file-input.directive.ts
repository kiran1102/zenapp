import { getModalName } from "oneRedux/reducers/modal.reducer";
import * as _ from "lodash";
import $ from "jquery";

interface IFileInput {
    Data: string;
    FileName: string;
    Extension: string;
    Blob?: Blob;
}

export default function fileInput(): any {
    return {
        link: (scope: any, elem: any, attrs: any): void => {

            const getFileExtension = (fileName: string): string => {
                // Return only file extension
                return (fileName.split(".").pop() || "");
            };

            const getFileName = (fileName: string): string => {
                const ext = getFileExtension(fileName);
                // Return file name without extension
                return _.replace(fileName, `.${ext}`, "");
            };

            const resetWarning = (): any => {
                return {
                    MaxSize: false,
                };
            };

            const defaultMax = 64;

            scope.files = [];
            scope.duplicateFiles = [];
            scope.maxSize = (scope.maxSize || defaultMax);
            scope.maxFileSize = ((scope.maxSize) * 1024 * 1024);
            scope.accept = (scope.accept || "");
            scope.showFileName = (scope.showFileName || false);
            scope.isDisabled = scope.isDisabled || false;
            scope.Warn = {
                MaxSize: false,
            };
            scope.removeFile = (file: IFileInput, type: string): void => {
                if (type === "size") {
                    scope.Warn.MaxSize = true;
                }
                scope.files.splice(scope.files.indexOf(file), 1);
                scope.duplicateFiles.splice(scope.duplicateFiles.indexOf(`${file.FileName}.${file.Extension}`), 1);
                scope.filesCount = scope.files.length;
                scope.done({ files: scope.files });
            };

            scope.clearFiles = (): void  => {
                $("input[type='file']").val(null);
            };

            elem.bind("change", (): void => {
                const files: any[] = elem[0].querySelector(".upload-input").files;
                scope.$apply((): void => {
                    scope.Warn = resetWarning();
                });

                _.each(files, (file: File): void => {
                    if (file.size < scope.maxFileSize) {
                        const reader: FileReader = new FileReader();
                        const fileObj: IFileInput = {
                            Data: "",
                            FileName: getFileName(file.name),
                            Extension: getFileExtension(file.name),
                            Blob: file,
                        };
                        reader.readAsDataURL(file);
                        reader.onload = (): void => {
                            scope.$apply((): void => {
                                if (!_.includes(scope.duplicateFiles, `${fileObj.FileName}.${fileObj.Extension}`)) {
                                    scope.duplicateFiles.push(`${fileObj.FileName}.${fileObj.Extension}`);
                                    (fileObj.Data as any) = reader.result;
                                    if (scope.allowMultiple) {
                                        scope.files.push(fileObj);
                                    }
                                }
                                if (!scope.allowMultiple) {
                                    scope.files = [fileObj];
                                }
                                scope.filesCount = scope.files.length;
                                scope.done({ files: scope.files });
                            });
                        };
                    } else {
                        scope.removeFile(file, "size");
                    }
                });
            });
        },
        replace: true,
        template: require("views/Shared/Directives/fileInput.jade"),
        restrict: "E",
        scope: {
            allowMultiple: "<?",
            accept: "@",
            done: "&",
            inputClass: "@",
            maxSize: "<?",
            showFileName: "<?",
            selectText: "<?",
            showFileList: "<?",
            isDisabled: "<?",
            maxSizeText: "@?",
            inputStyle: "<?",
            isRequired: "<?",
        },
    };
}
