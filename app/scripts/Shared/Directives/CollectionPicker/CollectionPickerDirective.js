import TreeMap from "TreeMap";

export default function CollectionPicker() {

    function link(scope) {
        scope.status = {
            isopen: false
        };

        scope.DisplayTypes = {
            input: 10,
            tree: 20
        };

        scope.DisplayType = scope.type ? scope.DisplayTypes[scope.type] : scope.DisplayTypes.input;

        scope.cancel = (!_.isUndefined(scope.cancel)) ? scope.cancel : true;

        scope.showClear = false;
        scope.touched = false;

        scope.Node = {
            Nodes: scope.collectionKeyNodes ? scope.collectionKeyNodes : "Children",
            Name: scope.collectionKeyName ? scope.collectionKeyName : "Name",
            Subname: scope.collectionKeySubname ? scope.collectionKeySubname : "SubName",
            Icon: scope.collectionKeyIcon ? scope.collectionKeyIcon : "",
            ShowChildren: scope.collectionKeyShowChildren ? scope.collectionKeyShowChildren : "",
            replacement: scope.replacement ? scope.replacement : ""
        };

        function updateAllowOpen() {
            // Allow open if the picker is an input, there is no model,
            // or if there is a model and the list has more than 1 item or if the list's only item has children.
            scope.AllowOpen = !!(scope.cancel || !scope.ngModel || (scope.ngModel && (scope.list.length > 1 || scope.list[0][scope.Node.Nodes].length)));
        }
        updateAllowOpen();

        scope.isPlaceholder = true;

        scope.$watch("status.isopen", function () {
            if (!scope.touched && !_.isUndefined(scope.SelectedNode)) {
                scope.touched = true;
            }
        });

        scope.select = function (selectedItem) {

            scope.setNode(selectedItem);

            if (_.isFunction(scope.onSelect)) {
                scope.onSelect({ "item": selectedItem });
            }
        };

        scope.caretClick = function (selectedItem) {
            if (_.isFunction(scope.onCaretClick)) {
                scope.onCaretClick({ "item": selectedItem });
            }
        };

        scope.setNode = function (item) {
            scope.isPlaceholder = false;
            scope.SelectedNode = item;
            scope.ngModel = item.Id;
            scope.status.isopen = false;
            updateAllowOpen();
        };

        scope.Clear = function ($event) {
            $event.preventDefault();
            $event.stopPropagation();

            scope.isPlaceholder = true;
            scope.SelectedNode = null;
            scope.ngModel = "";
            updateAllowOpen();
        };

        scope.initSearch = function (value, rootArr) {

            scope.TreeMap = new TreeMap("Id", "ParentId", scope.Node.Nodes, rootArr[0]);
            var node = scope.TreeMap.Node(value);

            if (!_.isUndefined(node) && node !== null) {
                scope.setNode(node);
            }

        };

        scope.$watch("ngModel", function (newValue, oldValue) {
            if (((_.isUndefined(oldValue) || oldValue === "") &&
                (!_.isUndefined(newValue) && newValue !== "") &&
                _.isUndefined(scope.SelectedNode)) ||
                (!_.isUndefined(oldValue) &&
                    oldValue === newValue &&
                    _.isUndefined(scope.SelectedNode))) {

                if (!_.isUndefined(scope.list)) {
                    scope.initSearch(newValue, scope.list);
                }

                else {
                    scope.$watch("list", function (newList, oldList) {
                        if ((_.isUndefined(oldList) || oldList === "") &&
                            (!_.isUndefined(newList) && newList !== "") &&
                            _.isUndefined(scope.SelectedNode)) {
                            if (!_.isUndefined(newList)) {
                                scope.initSearch(newValue, newList);
                            }
                        }
                    });
                }

            }
        });

        scope.$watch("list", function (newList, oldList) {
            if ((!_.isUndefined(oldList) && (!_.isUndefined(newList) && newList !== "") &&
                !_.isUndefined(scope.SelectedNode) && (!_.isUndefined(scope.ngModel) && scope.ngModel !== ""))) {
                if (!_.isUndefined(newList)) {
                    scope.initSearch(scope.ngModel, newList);
                }
            }
        });

    }

    var collectionPicker = {
        restrict: "E",
        replace: true,
        scope: {
            list: "=",
            ngDisabled: "=",
            ngModel: "=",
            filter: "=",
            type: "@",
            touched: "=?",
            cancel: "=?",
            onSelect: "&",
            onCaretClick: "&",
            replacement: "=",
            trackBy: "<?",
            collectionPlaceholder: "@",
            collectionKeyNodes: "@",
            collectionKeyName: "@",
            collectionKeySubname: "@",
            collectionKeyIcon: "@",
            collectionKeyShowChildren: "@"
        },
        template: require("views/Shared/Directives/CollectionPicker/collection-picker.jade"),
        link: link
    };

    return collectionPicker;
}

CollectionPicker.$inject = [];
