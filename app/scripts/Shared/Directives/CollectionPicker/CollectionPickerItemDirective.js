﻿
"use strict";

export default function PickerItem($compile) {
    // Usage:
    //     <collection-picker-item></collection-picker-item>
    // Creates:
    //
    //

    function link(scope, element) {

        element.find(".collection-picker-item").bind("click", function ($event) {
            scope.thisSelect(scope.item, $event);
            scope.$apply();
        });

        if (_.isUndefined(scope.num)) {
            scope.num = 0;
        }

        scope.thisCaretClick = function (selectedItem, event) {
            if (scope.caretClick) {
                scope.caretClick({
                    "item": selectedItem
                });
                if (!_.isUndefined(event)) {
                    event.stopPropagation();
                    event.preventDefault();
                    event.stopNextHandler = true;
                }
            } else {
                selectedItem[scope.collectionKeyShowChildren] = !selectedItem[scope.collectionKeyShowChildren];
                if (!_.isUndefined(event)) {
                    event.stopPropagation();
                    event.preventDefault();
                    event.stopNextHandler = true;
                }
            }
        };

        scope.thisSelect = function (selectedItem, $event) {
            if (!_.isUndefined($event)) {
                $event.stopPropagation();
                $event.preventDefault();
                $event.stopNextHandler = true;
            }
            scope.select({
                "item": selectedItem
            });

            scope.$root.$broadcast("selectedOrgParentId", { selectedOrgParentId: selectedItem.ParentId });
        };

        scope.Setup = function () {
            scope.collectionKeyNodes = scope.collectionKeyNodes ? scope.collectionKeyNodes : "Children";

            scope.Node = {
                Id: scope.collectionKeyModel ? scope.item[scope.collectionKeyModel] : scope.item["Id"],
                Nodes: scope.collectionKeyNodes ? scope.item[scope.collectionKeyNodes] : scope.item["Children"],
                Name: scope.collectionKeyName ? scope.item[scope.collectionKeyName] : scope.item["Name"],
                Subname: scope.collectionKeySubname ? scope.item[scope.collectionKeySubname] : scope.item["Subname"],
                Icon: scope.collectionKeyIcon ? scope.item[scope.collectionKeyIcon] : ""
            };

            scope.collectionKeyShowChildren = (!_.isUndefined(scope.collectionKeyShowChildren) ? scope.collectionKeyShowChildren : "");
            scope.showChildrenString = scope.collectionKeyShowChildren ? "item." + scope.collectionKeyShowChildren : "item." + scope.collectionKeyNodes + ".length";
        };

        scope.Setup();

        if (_.isArray(scope.Node.Nodes)) {
            var collectionPickerTemplate = "<collection-picker-list ng-if=\"" + scope.showChildrenString + "\"" +
                "ng-model=\"ngModel\" " +
                "filter=\"filter\" " +
                "select=\"thisSelect(item)\" " +
                "caret-click=\"thisCaretClick(item)\" " +
                "num=\"" + (scope.num + 20) + "\" " +
                (scope.trackBy ? "track-by=\"'" + (scope.trackBy) +"'\" " : "") +
                "list=\"item." + scope.collectionKeyNodes + "\" " +
                "collection-key-nodes=\"'" + scope.collectionKeyNodes + "'\" " +
                "collection-key-name=\"'" + scope.collectionKeyName + "'\" " +
                "collection-key-subname=\"'" + scope.collectionKeySubname + "'\"" +
                "collection-key-icon=\"'" + scope.collectionKeyIcon + "'\"" +
                "collection-key-show-children=\"'" + scope.collectionKeyShowChildren + "'\" >" +
                "</collection-picker-list>";
            element.append(collectionPickerTemplate);
            $compile(element.contents())(scope);
        }

    }

    var collectionPickerItemDirective = {
        restrict: "E",
        replace: true,
        scope: {
            item: "=",
            filter: "=",
            select: "&",
            caretClick: "&",
            ngModel: "=",
            num: "=",
            trackBy: "<?",
            collectionKeyNodes: "=",
            collectionKeyName: "=",
            collectionKeySubname: "=",
            collectionKeyIcon: "=",
            collectionKeyShowChildren: "="
        },
        template: require("views/Shared/Directives/CollectionPicker/collection-picker-item.jade"),
        link: link
    };

    return collectionPickerItemDirective;
}

PickerItem.$inject = ["$compile"];
