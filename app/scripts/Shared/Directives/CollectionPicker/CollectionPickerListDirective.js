
"use strict";

export default function CollectionPickerList() {
    // Usage:
    //     <collection-picker-list></collection-picker-list>
    // Creates:
    //
    //

    function link(scope) {

        scope.Node = {
            Nodes: scope.collectionKeyNodes,
            Name: scope.collectionKeyName,
            SubName: scope.collectionKeySubname,
            Icon: scope.collectionKeyIcon,
            ShowChildren: scope.collectionKeyShowChildren
        };

        scope.thisSelect = function (selectedItem) {
            scope.select({
                "item": selectedItem
            });
        };

        scope.thisCaretClick = function (selectedItem) {
            scope.caretClick({
                "item": selectedItem
            });
        };

        scope.trackById = function (item, index) {
            return scope.trackBy ? item[scope.trackBy] : index;
        };

    }

    var collection = {
        restrict: "E",
        replace: true,
        scope: {
            list: "=",
            filter: "=",
            ngModel: "=",
            select: "&",
            caretClick: "&",
            num: "=",
            trackBy: "<?",
            collectionKeyNodes: "=",
            collectionKeyName: "=",
            collectionKeySubname: "=",
            collectionKeyIcon: "=",
            collectionKeyShowChildren: "="
        },
        template: require("views/Shared/Directives/CollectionPicker/collection-picker-list.jade"),
        link: link
    };

    return collection;

}

CollectionPickerList.$inject = [];
