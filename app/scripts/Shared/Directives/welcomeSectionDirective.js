import TreeMap from "TreeMap";
import { getOrgTreeById } from "oneRedux/reducers/org.reducer";
import Utilities from "Utilities";

function welcomeController($scope, ENUMS, Projects, OrgGroups, Users, $q, $uibModal, PRINCIPAL, UserStore, store) {
	var vm = this;
	vm.team = [];
	vm.sessionOrgTree = getOrgTreeById(PRINCIPAL.getOrggroup())(store.getState());
	vm.orgs = (vm.sessionOrgTree) ? vm.sessionOrgTree : [];
	vm.rootOrganizationTree = [];
	vm.currentUser = PRINCIPAL.getUser();
	$scope.usersList = UserStore.getUserList();

	var init = function () {
		if (vm.showHeader) {
			var welcomePromises = [
				getUsers(),
				getRootTree()
			];
			$q.all(welcomePromises);
		}
	};

	var getRootTree = function () {
		return OrgGroups.rootTree()
			.then(function (response) {
				if (response.data) {
					vm.rootOrganizationTree = response.data;
					vm.treeMap = new TreeMap("Id", "ParentId", "Children", vm.rootOrganizationTree[0]);
				}
			});
	};

	var getUsers = function () {
		if ($scope.usersList) {
			vm.team = $scope.usersList;
			vm.team = vm.team.filter(function (user) {
				return user.IsActive;
			});
			return;
		}
	};

	vm.orgSaveProject = function (item) {
		vm.project.OrgGroupId = item.Id ? item.Id : vm.project.OrgGroupId;
		vm.project.OrgGroup = item.Name ? item.Name : vm.project.OrgGroup;

		if (_.isString(vm.project.OrgGroupId)) {
			vm.saveProject();
		}
	};

	vm.saveProject = function () {
		var project = {
			Id: vm.project.Id,
			ProjectVersion: vm.project.Version,
			Name: vm.project.Name,
			Description: vm.project.Description,
			OrgGroupId: vm.project.OrgGroupId,
			OwnerId: vm.project.OwnerId,
			LeadId: vm.project.LeadId,
			TemplateId: vm.project.TemplateId
		};

		// Set the View Values of the Updated Typeaheads.
		project.OrgGroup = formatGroupLabel(project.OrgGroupId);
		project.Owner = formatTeamLabel(project.OwnerId);
		project.Lead = formatTeamLabel(project.LeadId);

		if (Utilities.matchRegex(project.OrgGroupId, ENUMS.Regex.Guid)) {
			if (Utilities.matchRegex(project.LeadId, ENUMS.Regex.Guid)) {
				Projects.update(project);
			}
		}
	};

	var formatGroupLabel = function (model) {
		if (model && Utilities.matchRegex(model, ENUMS.Regex.Guid)) {
			return OrgGroups.formatGroupLabel(vm.orgs, model);
		}
	};

	var formatTeamLabel = function (model) {
		if (model && Utilities.matchRegex(model, ENUMS.Regex.Guid)) {
			return Users.formatTeamLabel(vm.team, model);
		}
		else return "";
	};

	vm.openProjectAssignmentsModal = function () {
		var modalInstance = $uibModal.open({
			templateUrl: "AssignmentsModal.html",
			controller: "ProjectAssignmentsCtrl",
			scope: $scope,
			backdrop: "static",
			keyboard: false,
			resolve: {
				project: function () {
					return vm.project;
				},
				Team: function () {
					return vm.team;
				},
			}
		});
		modalInstance.result.then(function (response) {
			if (response.result) {
				var assignment = response.data;
				vm.project.Assignment = assignment;
				vm.project.Lead = assignment.Assignee;
				vm.project.LeadId = assignment.AssigneeId;
			}
		});
	};

	vm.openDeadlinesModal = function () {
		var modalInstance = $uibModal.open({
			templateUrl: "DeadlinesModal.html",
			controller: "DeadlinesModalCtrl",
			scope: $scope,
			backdrop: "static",
			keyboard: false,
			resolve: {
				Project: function () {
					return vm.project;
				}
			}
		});
		modalInstance.result.then(function (response) {
			if (response.result) {
				vm.project.Deadline = response.data.Deadline;
				vm.project.ReminderDays = response.data.ReminderDays;
			}
		});
	};

	vm.openProjectReviewerModal = function () {
		var modalInstance = $uibModal.open({
			templateUrl: "ReviewerModal.html",
			controller: "ProjectReviewerCtrl",
			scope: $scope,
			backdrop: "static",
			keyboard: false,
			resolve: {
				project: function () {
					return vm.project;
				},
				CurrentUser: function () {
					return vm.currentUser;
				},
				Team: function () {
					return vm.team;
				},
				Tree: function () {
					return vm.orgs;
				},
				TreeMap: function () {
					return vm.treeMap;
				}
			}
		});
		modalInstance.result.then(function (response) {
			if (response.result) {
				var assignment = response.data;
				vm.project.Assignment = assignment;
				vm.project.Reviewer = assignment.Assignee;
				vm.project.ReviewerId = assignment.AssigneeId;
			}
		});
	};

	init();
};

welcomeController.$inject = ["$scope", "ENUMS", "Projects", "OrgGroups", "Users", "$q", "$uibModal", "PRINCIPAL", "UserStore", "store"];

export default function welcomeSection() {
	var directive = {
		template: require("views/Shared/Directives/welcomeSection.jade"),
		restrict: "E",
		controller: welcomeController,
		controllerAs: "vm",
		bindToController: true,
		scope: {
			project: "=",
			section: "=",
			editable: "&",
			resources: "=",
			showHeader: "="
		}
	};

	return directive;
}
