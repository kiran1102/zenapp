"use strict";

export default function expand($document, $timeout) {
    return {
        restrict: "A",
        link: function ($scope, element, attributes) {
            // Set transition
            var transition = attributes.transition ? parseInt(attributes.transition) : 300;
            var initialHeight = attributes.startExpanded ? "auto" : 0;
            element.css({
                "height": initialHeight,
                "transition": "height " + transition + "ms",
            });

            function getElementCurrentHeight() {
                return element[0].offsetHeight;
            }

            function getElementAutoHeight() {
                var currentHeight = getElementCurrentHeight();

                element.css("height", "auto");
                var autoHeight = getElementCurrentHeight();

                element.css("height", currentHeight);
                // Force the browser to recalc height after moving it back to normal
                getElementCurrentHeight();

                return autoHeight;
            }

            function setTransition() {
                element.css("transition", "height " + transition + "ms");
            }

            function removeTransition() {
                element.css("transition", "none");
            }

            $scope.$watch(attributes.expand, function (expand) {
                var maxHeight = getElementAutoHeight();
                // If expanding, transition to the max height then set to auto
                if (expand) {
                    element.css("height", maxHeight + "px");
                    $timeout(function () {
                        removeTransition();
                        element.css({
                            "height":"auto",
                            "overflow-y": "visible",
                        });
                    }, transition);
                    // Otherwise, change to max height and then transition to zero
                } else {
                    setTransition();
                    element.css({
                        "height": 0,
                        "overflow-y": "hidden",
                    });
                }
                element.toggleClass("expanded", expand);
            });

        }
    };
}

expand.$inject = ["$document", "$timeout"];
