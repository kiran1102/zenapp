
function VersionSelectorController(Projects) {
	var vm = this;
	vm.show = false;
	vm.options = [];

	var setVersionOptions = function () {
		var latestVersion = vm.project.Version;
		if (latestVersion > 1) {
			for (var i = latestVersion; i > 0; i--) {
				vm.options.push({
					label: "v" + i,
					value: i
				});
			}
			vm.selectedVersion = vm.options[0];
			vm.show = true;
		}
	};

	vm.changeVersion = function () {
		vm.project.versionChanging = true;
		Projects.read(vm.project.Id, vm.selectedVersion.value)
			.then(function (response) {
				if (response.result) {
					vm.formatData(response.data);
					vm.project = response.data;
					vm.project.versionChanging = false;
				}
			});
	};

	function init() {
		setVersionOptions();
	}

	init();
}

VersionSelectorController.$inject = ["Projects"];

export default function projectVersionSelector() {
	var directive = {
		template: require("views/Shared/Directives/projectVersionSelector.jade"),
		restrict: "E",
		controller: VersionSelectorController,
		controllerAs: "vm",
		bindToController: true,
		scope: {
			project: "=",
			formatData: "="
		}
	};

	return directive;
}
