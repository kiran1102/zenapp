
"use strict";

export default function Collection() {
    // Usage:
    //     <collection></collection>
    // Creates:
    //
    //

    var template = "<div class=\"collection\">" +
        "<div ui-tree=\"options\" ui-tree-nodes=\"\" ng-model=\"collection\">" +
        "<member ui-tree-node member=\"member\" filter=\"filter\" options=\"options\" member-nodes=\"meta.Nodes\" " +
        "member-name=\"meta.Name\" member-subname=\"meta.SubName\" add-node=\"add(Id)\" edit-node=\"edit(Id)\" edit-modal-node=\"editModal(Id)\" delete-node=\"delete(Id)\" " +
        "ng-repeat=\"member in collection | filter:filter\"></member>" +
        "</div>" +
        "</div>";

    function link(scope) {

        scope.add = function (id) {
            scope.collectionAddNode({ "Id": id });
        };

        scope.edit = function (id) {
            scope.collectionEditNode({ "Id": id });
        };

        scope.editModal = function (id) {
            scope.collectionEditModalNode({ "Id": id });
        };

        scope.delete = function (id) {
            scope.collectionDeleteNode({ "Id": id });
        };

        scope.meta = {
            Nodes: scope.collectionKeyNodes,
            Name: scope.collectionKeyName,
            SubName: scope.collectionKeySubname
        };

    }

    var collection = {
        restrict: "E",
        replace: true,
        scope: {
            collection: "=",
            filter: "=",
            options: "=",
            collectionAddNode: "&",
            collectionEditNode: "&",
            collectionDeleteNode: "&",
            collectionEditModalNode: "&",
            collectionKeyNodes: "=",
            collectionKeyName: "=",
            collectionKeySubname: "="
        },
        template: template,
        link: link
    };

    return collection;
}

Collection.$inject = [];
