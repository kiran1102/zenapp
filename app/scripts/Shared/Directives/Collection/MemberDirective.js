﻿
export default function Member($compile, Permissions) {

    var template = "<div class=\"member\">" +
        "<div class=\"member-box\">" +
        "<span class=\"member-box-content\">" +
        "<span ng-if=\"CanDrag\" ui-tree-handle class=\"member-box-content-handle\">" +
        "<i class=\"fa fa-arrows\"></i>" +
        "</span>" +
        "<span ng-if=\"CanCollapse\" ng-click=\"Collapse()\" class=\"member-box-content-collapse\">" +
        "<i class=\"fa\" ng-class=\"{'fa-caret-right': IsCollapsed, 'fa-caret-down': !IsCollapsed }\"></i>" +
        "</span>" +
        "<span ng-if=\"Node.Name\" ng-click=\"edit(member.Id)\" class=\"member-box-content-name\">{{Node.Name}}</span>" +
        "<span ng-if=\"Node.SubName\" ng-click=\"edit(member.Id)\" class=\"member-box-content-subname\">{{Node.SubName}}</span>" +
        "</span>" +
        "<span class=\"member-box-actions\">" +
        "<span ng-if=\"CanAdd\" class=\"member-box-actions-item\" uib-tooltip=\"{{::$root.t('Add')}}\" tooltip-placement=\"top\" tooltip-trigger=\"mouseenter\">" +
        "<span class=\"btn\" ng-click=\"add(member.Id)\">" +
        "<i class=\"fa fa-plus\"></i>" +
        "</span>" +
        "</span>" +
        "<span ng-if=\"CanEdit\" class=\"member-box-actions-item\" uib-tooltip=\"{{::$root.t('Edit')}}\" tooltip-placement=\"top\" tooltip-trigger=\"mouseenter\">" +
        "<span class=\"btn\" ng-click=\"editModal(member.Id)\">" +
        "<i class=\"fa fa-pencil\"></i>" +
        "</span>" +
        "</span>" +
        "<span ng-if=\"canDelete && AllowDelete\" class=\"member-box-actions-item\" uib-tooltip=\"{{::$root.t('Delete')}}\" " +
        "tooltip-placement=\"top\" tooltip-trigger=\"mouseenter\">" +
        "<span class=\"btn\" ng-click=\"delete(member.Id)\">" +
        "<i class=\"fa fa-trash\"></i>" +
        "</span>" +
        "</span>" +
        "</span>" +
        "</div>" +
        "</div>";

    function link(scope, element) {

        scope.Setup = function () {
            scope.memberNodes = scope.memberNodes ? scope.memberNodes : "Children";

            scope.Node = {
                Nodes: scope.memberNodes ? scope.member[scope.memberNodes] : scope.member.Children,
                Name: scope.memberName ? scope.member[scope.memberName] : scope.member.Name,
                SubName: scope.memberSubname ? scope.member[scope.memberSubname] : scope.member.SubName,
                canDelete: scope.member.CanDelete,
            };

            scope.CanDrag = (scope.member.ParentId ? ((scope.member.ParentId !== null) ? scope.member.ParentId : false) : false);
            scope.CanAdd = Boolean(!_.isUndefined(scope.addNode));
            scope.CanEdit = Boolean(!_.isUndefined(scope.editNode));
            scope.CanEdit = Boolean(!_.isUndefined(scope.editModalNode));
            scope.canDelete = Boolean(!_.isUndefined(scope.deleteNode) && scope.Node.canDelete);
            scope.CanCollapse = Boolean(!_.isUndefined(scope.Node.Nodes) && scope.Node.Nodes.length);
            scope.AllowDelete = Boolean(scope.CanDrag && !scope.CanCollapse && Permissions.canShow("OrgGroupsDelete"));
            scope.IsCollapsed = scope.$parent.collapsed;
        };

        scope.Setup();

        scope.Collapse = function () {
            scope.$parent.toggle();
            scope.IsCollapsed = scope.$parent.collapsed;
        };

        scope.add = function (id) {
            scope.addNode({ "Id": id });
        };

        scope.edit = function (id) {
            scope.editNode({ "Id": id });
        };

        scope.delete = function (id) {
            scope.deleteNode({ "Id": id });
        };

        scope.editModal = function (id) {
            scope.editModalNode({ "Id": id });
        };

        if (_.isArray(scope.Node.Nodes)) {
            var collectionTemplate = "<collection ng-hide=\"$parent.collapsed\" class=\"member-children\" " +
                "filter=\"filter\" " +
                "options=\"options\" " +
                "collection=\"member." + scope.memberNodes + "\" " +
                "collection-add-node=\"add(Id)\" " +
                "collection-edit-node=\"edit(Id)\" " +
                "collection-edit-modal-node=\"editModal(Id)\" " +
                "collection-delete-node=\"delete(Id)\" " +
                "collection-key-nodes=\"'" + scope.memberNodes + "'\" " +
                "collection-key-name=\"'" + scope.memberName + "'\" " +
                "collection-key-subname=\"'" + scope.memberSubname + "'\" >" +
                "</collection>";
            element.append(collectionTemplate);
            $compile(element.contents())(scope);
        }

    }

    var member = {
        restrict: "E",
        replace: true,
        scope: {
            member: "=",
            filter: "=",
            options: "=",
            addNode: "&",
            editNode: "&",
            editModalNode: "&",
            deleteNode: "&",
            memberNodes: "=",
            memberName: "=",
            memberSubname: "="
        },
        template: template,
        link: link
    };

    return member;
}

Member.$inject = ["$compile", "Permissions"];
