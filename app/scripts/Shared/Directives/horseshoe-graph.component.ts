import * as _ from "lodash";

class HorseshoeGraphCtrl  implements ng.IComponentController {
    private circumference: number;
    private graphRadius: number;
    private list: any;
    private shortCirc: number;
    private size: number;
    private stroke: number;

    public $onInit(): void {
        this.size = Number(this.size);
        this.stroke = Number(this.stroke);
        this.calcGraph();
        this.setOffset();
    }

    private calcGraph(): void {
        this.graphRadius = (this.size / 2) - (this.stroke / 2);
        this.circumference = 2 * Math.PI * this.graphRadius;
        this.shortCirc = this.circumference * .75;
    }

    private setOffset(): void {
        const graphList: any[] = this.list;
        let start = 0;

        _.each(graphList, (item: any): void => {
            const itemLen: number = this.shortCirc * item.value;
            item.dashoffset = start;
            item.dasharray = `${itemLen}  ${this.circumference}`;
            start -= itemLen;
        });
    }
}

const horseshoeGraph: ng.IComponentOptions = {
    bindings: {
        size: "@",
        stroke: "@",
        list: "=",
        title: "@",
        titleClass: "@?",
        subtitle: "@",
        countText: "@",
    },
    controller: HorseshoeGraphCtrl,
    template: (): string => {
        return `
				<div class="horseshoe">
					<h3 class="horseshoe__title {{$ctrl.titleClass}}">
						{{::$ctrl.title}}
						<span class="horseshoe__title-sub" data-ng-if="$ctrl.subtitle">
							{{$ctrl.subtitle}}
						</span>
						<span class="horseshoe__count" data-ng-if="$ctrl.countText">
							{{$ctrl.countText}}
						</span>
                    </h3>
                    <svg class="horseshoe__graph"
                        data-ng-attr-height="{{::$ctrl.size}}"
                        data-ng-attr-width="{{::$ctrl.size}}">
                        <circle class="horseshoe__graph-item"
                            data-ng-repeat="item in $ctrl.list track by $index",
                            data-ng-attr-r="{{$ctrl.graphRadius}}"
                            data-ng-attr-cx="{{::$ctrl.size / 2}}"
                            data-ng-attr-cy="{{::$ctrl.size / 2}}"
                            data-ng-class="item.graphClass ? item.graphClass : item.class"
                            data-ng-style="{
                                'stroke-width': $ctrl.stroke,
                                'stroke-dasharray': item.dasharray,
                                'stroke-dashoffset': item.dashoffset,
                            }">
                            <title ng-bind="item.count" ng-if="item.count"></title>
                        </circle>
                    </svg>
				</div>
			`;
    },
};

export default horseshoeGraph;
