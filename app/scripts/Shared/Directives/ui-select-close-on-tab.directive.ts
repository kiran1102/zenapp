import { IStringMap } from "interfaces/generic.interface";
import { KeyCodes } from "enums/key-codes.enum";

export default function uiSelectCloseOnTab(): any {
    return {
        require: "uiSelect",
        link: (
            scope: any,
            element: any,
            attrs: any,
            $select: any,
        ): void => {
            const searchInput: JQuery = element.querySelectorAll("input.ui-select-search");

            searchInput.on("keydown", { select: $select }, (event: JQueryEventObject): void => {
                if (event.keyCode === KeyCodes.Tab) {
                    event.data.select.close();
                    event.data.select.setFocus();
                }
            });
        },
    };
}
