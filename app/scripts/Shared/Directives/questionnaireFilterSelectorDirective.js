
"use strict";

function QuestionnaireFilterSelectorController() {
    var vm = this;

    vm.activeQuestionId = null;

    vm.toggleSection = function (section) {
        section.isOpen = !section.isOpen;
    };

    vm.clearFilters = function () {
        _.each(vm.questionnaire.Sections, function(section, sectionIndex) {
            _.each(section.TemplateQuestions, function(question, questionIndex) {
                // Search the Question for matching Response and set selected
                _.each(question.Responses, function(questionResponse, questionResponseIndex) {
                    if (questionResponse.selected) {
                        questionResponse.selected = false;
                        vm.toggleFilter(question, questionResponse, sectionIndex, questionIndex, questionResponseIndex);
                    }
                });
            });
        });
    };

    vm.toggleFilter = function (Question, QuestionResponse, selectedSection, selectedQuestion, selectedAnswer) {
        var QuestionIndex = -1;
        var filterQuestion = null;

        // Traverse the Filters to find the Question.
        for (var i = 0; i < vm.filters.length; i++) {
            filterQuestion = vm.filters[i];
            // Note the index if we have found the question.
            if (filterQuestion.QuestionId === Question.Id) {
                filterQuestion.Name = Question.Name;
                QuestionIndex = i;
                break;
            }
        }

        // Question Exists in filters
        if (QuestionIndex > -1) {
            filterQuestion = vm.filters[QuestionIndex];
            // If this question has responses, search them for a match;
            if (_.isArray(filterQuestion.Responses) && filterQuestion.Responses.length > 0) {
                var ResponseFound = false;
                for (var j = 0; j < filterQuestion.Responses.length; j++) {
                    var filterResponse = filterQuestion.Responses[j];
                    if (_.isObject(filterResponse) &&
                        (filterResponse.Type === QuestionResponse.Type) &&
                        (filterResponse.Value === QuestionResponse.Value)) {
                        // This response already exists. Remove this response.
                        vm.filters[QuestionIndex].Responses.splice(j, 1);
                        // If this question has no responses, remove the question from the filter.
                        if (vm.filters[QuestionIndex].Responses.length === 0) {
                            vm.filters.splice(QuestionIndex, 1);
                        }
                        ResponseFound = true;
                    }
                }

                // If we have not found a response, add a new one.
                if (!ResponseFound) {
                    vm.filters[QuestionIndex].Responses.push({
                        Type: QuestionResponse.Type,
                        Value: QuestionResponse.Value
                    });
                }
            }
            // Init the responses for this question
            else {
                vm.filters[QuestionIndex].Responses = [{
                    Type: QuestionResponse.Type,
                    Value: QuestionResponse.Value
                }];
            }
        }
        // Question does not exist. Just add the new response.
        else {
            vm.filters.push({
                QuestionId: Question.Id,
                Responses: [{
                    Type: QuestionResponse.Type,
                    Value: QuestionResponse.Value
                }]
            });
        }

        // Traverse the Filters to find the Question with data in the project list.
        for (var k = 0; k < $scope.report.projects.length; k++) {
            var filterQuestion = $scope.report.projects[k];
            // Note the index if we have found the question.
            if (filterQuestion.QuestionId === Question.Id) {
                filterQuestion.Name = Question.Name;
                QuestionIndex = k;
                break;
            }
        }
        var isNoAnswer = vm.questionnaire.Sections[selectedSection].TemplateQuestions[selectedQuestion].Responses[selectedAnswer].Name;
        if (isNoAnswer === "No Answer") {
            selectedAnswer = -1;
        }

        // updateFilteredList(selectedSection, selectedQuestion, selectedAnswer, Question, QuestionResponse.selected);
    };

    vm.setActiveQuestion = function (questionId) {
        vm.activeQuestionId = questionId;
    };

    vm.clearActiveQuestion = function () {
        vm.activeQuestionId = null;
    };

    vm.sectionFilterActive = function (section) {
        section.HasFilter = false;

        for (var i = 0; i < section.TemplateQuestions.length; i++) {
            if (vm.questionFilterActive(section.TemplateQuestions[i])) {
                section.HasFilter = true;
                break;
            }
        }

        return section.HasFilter;
    };

    vm.questionFilterActive = function (question) {
        question.HasFilter = false;

        for (var i = 0; i < question.Responses.length; i++) {
            if (!!question.Responses[i]) {
                question.HasFilter = true;
                break;
            }
        }

        return question.HasFilter;
    };
}

export default function QuestionnaireFilterSelectorDirective() {
    return {
        template: require("views/Shared/Directives/questionnaireFilterSelector.jade"),
        restrict: "E",
        controller: QuestionnaireFilterSelectorController,
        controllerAs: "vm",
        bindToController: true,
        replace: true,
        scope: {
            questionnaire: "=",
            filters: "=",
            onFilterSelected: "&"
        }
    };
}
