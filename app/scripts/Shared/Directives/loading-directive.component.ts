import * as _ from "lodash";

class Loading {
    iconClass: string;
    image: string;
    imageClass: string;
    loadingClass: string;
    showEllipsis: boolean;
    showIcon: boolean;
    showImage: boolean;
    showLoadingSuffix: boolean;
    showLoadingPrefix: boolean;
    showText: boolean;
    text: string;

    private DEFAULT: any = {
        iconClass: "fa fa-spinner fa-pulse fa-fw",
        image: "",
        imageClass: "",
        loadingClass: "",
        showEllipsis: true,
        showIcon: true,
        showImage: false,
        showLoadingPrefix: true,
        showLoadingSuffix: false,
        showText: true,
        text: "",
    };

    $onInit() {
        this.iconClass = !_.isUndefined(this.iconClass) ? this.iconClass : this.DEFAULT.iconClass;
        this.image = !_.isUndefined(this.image) ? this.image : this.DEFAULT.image;
        this.imageClass = !_.isUndefined(this.imageClass) ? this.imageClass : this.DEFAULT.imageClass;
        this.loadingClass = !_.isUndefined(this.loadingClass) ? this.loadingClass : this.DEFAULT.loadingClass;
        this.showEllipsis = !_.isUndefined(this.showEllipsis) ? this.showEllipsis : this.DEFAULT.showEllipsis;
        this.showIcon = !_.isUndefined(this.showIcon) ? this.showIcon : this.DEFAULT.showIcon;
        this.showImage = !_.isUndefined(this.showImage) ? this.showImage : this.DEFAULT.showImage;
        this.showLoadingSuffix = !_.isUndefined(this.showLoadingSuffix) ? this.showLoadingSuffix : this.DEFAULT.showLoadingSuffix;
        this.showLoadingPrefix = !_.isUndefined(this.showLoadingPrefix) ? this.showLoadingPrefix : this.DEFAULT.showLoadingPrefix;
        this.showText = !_.isUndefined(this.showText) ? this.showText : this.DEFAULT.showText;
        this.text = !_.isUndefined(this.text) ? this.text : this.DEFAULT.text;
    }
}

export default {
    bindings: {
        iconClass: "@",
        image: "@",
        imageClass: "@",
        loadingClass: "@",
        showEllipsis: "=?",
        showIcon: "=?",
        showImage: "=?",
        showLoadingPrefix: "=?",
        showLoadingSuffix: "=?",
        showText: "=?",
        text: "@",
        identifier: "@?",
    },
    controller: Loading,
    template:
        `<span class="one__loading {{$ctrl.loadingClass}}" ot-auto-id="{{$ctrl.identifier}}">
            <img ng-if="$ctrl.showImage" class="one__loading__image {{$ctrl.imageClass}}" ng-src="{{$ctrl.image}}" />
            <i ng-if="$ctrl.showIcon" class="one__loading__icon {{$ctrl.iconClass}}"></i>
            <p ng-if="$ctrl.showText" class="one__loading__text margin-top-1 margin-bottom-0">
                <span ng-if="$ctrl.showLoadingPrefix">{{::$root.t("Loading")}}&nbsp;</span><span ng-if="$ctrl.text && $ctrl.text.length > 0", class="one__loading__subject">{{$ctrl.text}}</span><span ng-if="$ctrl.showLoadingSuffix">&nbsp;{{::$root.t("Loading")}}</span><span ng-if="$ctrl.showEllipsis" class="one__loading__ellipses" >...</span>
            </p>
        </span>`,
};
