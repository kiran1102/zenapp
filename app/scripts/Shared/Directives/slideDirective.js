
export default function pageslide($document, $timeout) {

    function link($scope, el, attrs) {

        var param = {};

                    param.side = $scope.psSide || "right";
                    param.speed = $scope.psSpeed || "0.5";
                    param.size = $scope.psSize || "320px";
                    param.zindex = $scope.psZindex || 1000;
                    param.className = $scope.psClass || "ng-pageslide";
                    param.squeeze = $scope.psSqueeze === "true";
                    param.squeezeContent = $scope.psSqueezecontent === "true";
                    param.push = $scope.psPush === "true";
                    param.container = $scope.psContainer || false;
                    param.keyListener = $scope.psKeyListener === "true";
                    param.bodyClass = $scope.psBodyClass || false;
                    param.clickOutside = $scope.psClickoutside !== "false";

        var mobileWidth = 768;

        el.addClass(param.className);

        /* DOM manipulation */

        var content, slider, body;

        body = param.container ? document.getElementById(param.container) : document.body;
        if (body === null) {
            body = document.getElementsByClassName(param.container);
        }
        var isOpen = false;

        function onBodyClick(e) {
            if (isOpen && !slider.contains(e.target)) {
                isOpen = false;
                $scope.psOpen = false;
                $scope.$apply();
            }

            if ($scope.psOpen) {
                isOpen = true;
            }
        }

        function setBodyClass(value) {
            if (param.bodyClass) {
                var bodyClass = param.className + "-body";
                var bodyClassRe = new RegExp(" " + bodyClass + "-closed| " + bodyClass + "-open");
                body.className = body.className.replace(bodyClassRe, "");
                body.className += " " + bodyClass + "-" + value;
            }
        }

        setBodyClass("closed");

        slider = el[0];

        if (slider.tagName.toLowerCase() !== "div" &&
            slider.tagName.toLowerCase() !== "pageslide") {
            throw new Error("Pageslide can only be applied to <div> or <pageslide> elements");
        }

        if (slider.children.length === 0) {
            throw new Error("You need to have content inside the <pageslide>");
        }

        content = angular.element(slider.children);

        /* Append */
        body.appendChild(slider);

        /* Style setup */
        slider.style.zIndex = param.zindex;
        slider.style.position = param.container !== false ? "absolute" : "fixed";
        slider.style.width = 0;
        slider.style.height = 0;
        slider.style.transitionDuration = param.speed + "s";
        slider.style.webkitTransitionDuration = param.speed + "s";
        slider.style.transitionProperty = "width, height";

        if (param.squeeze || param.push) {
            body.style.position = "absolute";
            body.style.transitionDuration = param.speed + "s";
            body.style.webkitTransitionDuration = param.speed + "s";
            body.style.transitionProperty = "top, bottom, left, right";
        }

        switch (param.side) {
            case "right":
                slider.style.height = attrs.psCustomheight || "100%";
                slider.style.top = attrs.psCustomtop || "0px";
                slider.style.bottom = attrs.psCustomBottom || "0px";
                slider.style.right = attrs.psCustomRight || "0px";
                break;
            case "left":
                slider.style.height = attrs.psCustomHeight || "100%";
                slider.style.top = attrs.psCustomTop || "0px";
                slider.style.bottom = attrs.psCustomBottom || "0px";
                slider.style.left = attrs.psCustomLeft || "0px";
                break;
            case "top":
                slider.style.width = attrs.psCustomWidth || "100%";
                slider.style.left = attrs.psCustomLeft || "0px";
                slider.style.top = attrs.psCustomTop || "0px";
                slider.style.right = attrs.psCustomRight || "0px";
                break;
            case "bottom":
                slider.style.width = attrs.psCustomWidth || "100%";
                slider.style.bottom = attrs.psCustomBottom || "0px";
                slider.style.left = attrs.psCustomLeft || "0px";
                slider.style.right = attrs.psCustomRight || "0px";
                break;
        }


        /* Closed */
        function psClose(slider, param) {
            if (slider && slider.style.width !== 0) {
                if (param.squeezeContent) {
                    angular.element("#pageslide-container").css("flex", "none");
                }
                content.map(function (index, item) {
                    if (!angular.element(item).hasClass("closeSlide")) {
                        angular.element(item).addClass("hidden");
                    }
                });

                content.addClass("hidden");
                switch (param.side) {
                    case "right":
                        slider.style.width = "0px";
                        if (param.squeeze) {
                            body.style.right = "0px";
                        }
                        if (param.push) {
                            body.style.right = "0px";
                            body.style.left = "0px";
                        }
                        break;
                    case "left":
                        slider.style.width = "0px";
                        if (param.squeeze) {
                            body.style.left = "0px";
                        }
                        if (param.push) {
                            body.style.left = "0px";
                            body.style.right = "0px";
                        }
                        break;
                    case "top":
                        slider.style.height = "0px";
                        if (param.squeeze) {
                            body.style.top = "0px";
                        }
                        if (param.push) {
                            body.style.top = "0px";
                            body.style.bottom = "0px";
                        }
                        break;
                    case "bottom":
                        slider.style.height = "0px";
                        if (param.squeeze) {
                            body.style.bottom = "0px";
                        }
                        if (param.push) {
                            body.style.bottom = "0px";
                            body.style.top = "0px";
                        }
                        break;
                }
            }
            if (param.keyListener) {
                $document.off("keydown", handleKeyDown);
            }

            if (param.clickOutside) {
                $document.off("click", onBodyClick);
            }
            isOpen = false;
            setBodyClass("closed");
            $scope.psOpen = false;
        }

        /* Open */
        function psOpen(slider, param) {
            var width = angular.element("body").width();
            if (slider.style.width !== 0) {
                switch (param.side) {
                    case "right":
                        slider.style.width = param.size;
                        if (param.squeeze) {
                            body.style.right = param.size;
                        }
                        if (param.push) {
                            body.style.right = param.size;
                            body.style.left = "-" + param.size;
                        }
                        break;
                    case "left":
                        slider.style.width = param.size;
                        if (param.squeeze) {
                            body.style.left = param.size;
                        }
                        if (param.push) {
                            body.style.left = param.size;
                            body.style.right = "-" + param.size;
                        }
                        break;
                    case "top":
                        slider.style.height = param.size;
                        if (param.squeeze) {
                            body.style.top = param.size;
                        }
                        if (param.push) {
                            body.style.top = param.size;
                            body.style.bottom = "-" + param.size;
                        }
                        break;
                    case "bottom":
                        slider.style.height = param.size;
                        if (param.squeeze) {
                            body.style.bottom = param.size;
                        }
                        if (param.push) {
                            body.style.bottom = param.size;
                            body.style.top = "-" + param.size;
                        }
                        break;
                }

                $timeout(function () {
                    if (param.squeezeContent && (width > mobileWidth)) {
                        angular.element("#pageslide-container").css("flex", "0 0 " + param.size);
                        slider.style.top = "inherit";
                    }
                    content.removeClass("hidden");
                }, (param.speed * 1000));

                $scope.psOpen = true;

                if (param.keyListener) {
                    $document.on("keydown", handleKeyDown);
                }

                if (param.clickOutside) {
                    $document.on("click", onBodyClick);
                }
                setBodyClass("open");
            }
        }

        function handleKeyDown(e) {
            var ESC_KEY = 27;
            var key = e.keyCode || e.which;

            if (key === ESC_KEY) {
                psClose(slider, param);
            }
        }

        /*
         * Watchers
         * */

        $scope.$watch("psOpen", function (value) {
            if (!!value) {
                psOpen(slider, param);
            } else {
                psClose(slider, param);
            }
        });

        $scope.$watch("psSize", function (newValue, oldValue) {
            if (oldValue !== newValue) {
                param.size = newValue;
                if ($scope.psOpen) {
                    psOpen(slider, param);
                }
            }
        });

        /*
         * Events
         * */

        $scope.$on("$destroy", function () {
            if (slider.parentNode === body) {
                if (param.clickOutside) {
                    $document.off("click", onBodyClick);
                }
                body.removeChild(slider);
            }
        });

        if ($scope.psAutoClose) {
            $scope.$on("$locationChangeStart", function () {
                psClose(slider, param);
            });
            $scope.$on("$stateChangeStart", function () {
                psClose(slider, param);
            });
        }

    }

    var directive = {
        restrict: "EAC",
        transclude: false,
        scope: {
            psOpen: "=?",
            psAutoClose: "@",
            psSide: "@",
            psSpeed: "@",
            psClass: "@",
            psSize: "@",
            psZindex: "@",
            psSqueeze: "@",
            psSqueezecontent: "@",
            psCloak: "@",
            psPush: "@",
            psContainer: "@",
            psKeyListener: "@",
            psBodyClass: "@",
            psClickoutside: "@"
        },
        link: link
    };

    return directive;

}

pageslide.$inject = ["$document", "$timeout"];
