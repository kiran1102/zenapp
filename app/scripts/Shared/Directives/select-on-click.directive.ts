
export default function selectOnClick($window: ng.IWindowService): any {
    return {
        restrict: "A",
        link(
            scope: any,
            element: any,
            attrs: any,
        ): void {
            element.on("click", (): void => {
                if (!$window.getSelection().toString()) {
                    if (!this.value) return;
                    this.setSelectionRange(0, this.value.length);
                }
            });
        },
    };
}

selectOnClick.$inject = ["$window"];
