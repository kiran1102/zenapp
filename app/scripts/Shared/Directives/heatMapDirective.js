
export default function heatmap($rootScope, ENUMS) {

    function link(scope) {
        var translate = $rootScope.t;

        // Default Axis
        var defaultHorizontal = [{
            label: translate("Low"),
            value: 1
        }, {
            label: translate("Medium"),
            value: 2
        }, {
            label: translate("High"),
            value: 3
        }, {
            label: translate("VeryHigh"),
            value: 4
        }];
        var defaultVertical = [{
            label: translate("Low"),
            value: 1
        }, {
            label: translate("Medium"),
            value: 2
        }, {
            label: translate("High"),
            value: 3
        }, {
            label: translate("VeryHigh"),
            value: 4
        }];

        scope.horizontalAxis = scope.horizontalAxis || defaultHorizontal;
        scope.verticalAxis = scope.verticalAxis || defaultVertical;
        scope.horizontalLabel = scope.horizontalLabel || translate("Impact");
        scope.verticalLabel = scope.verticalLabel || translate("Probability");
        scope.tiles = scope.tiles || [];
        scope.mode = scope.mode || "content";
        scope.contentKey = scope.contentKey || "risk";
        scope.callback = _.isFunction(scope.callback) ? scope.callback : null;

        scope.riskLabels = ENUMS.RiskLabels;
        scope.riskNames = ENUMS.RiskPrettyNames;
        scope.graphWeight = 0;

        scope.RiskLevels = [{
            Label: translate("LowRisk"),
            Value: 1
        }, {
            Label: translate("MediumRisk"),
            Value: 2
        }, {
            Label: translate("HighRisk"),
            Value: 3
        }, {
            Label: translate("VeryHighRisk"),
            Value: 4
        }];

        var drawGraphAxis = function () {
            // Get tile percentage width and height based on number of columns and rows
            scope.itemWidth = 100 / scope.horizontalAxis.length + "%";
            scope.itemHeight = 100 / scope.verticalAxis.length + "%";
            // Calculate overall basis for risks
            scope.graphWeight = scope.horizontalAxis.length + scope.verticalAxis.length;
        };

        var createTiles = function () {
            scope.tiles = [];
            // Iterate through rows (y)
            _.each(scope.verticalAxis, function(row, yIndex) {
                // Iterate through columns (x)
                _.each(scope.horizontalAxis, function(column, xIndex) {
                    // Create tile styles and content
                    var xCoord = xIndex + 1;
                    var yCoord = -(yIndex - scope.verticalAxis.length);
                    var tileWeight = xCoord + yCoord;
                    var risk = Math.round((tileWeight / scope.graphWeight) * 4);
                    risk = risk === 0 ? 1 : risk;
                    scope.tiles.push({
                        Axis1Value: xCoord,
                        Axis2Value: yCoord,
                        RiskLevel: risk,
                        selected: false
                    });
                });
            });
        };

        // Remove all plotted points on graph
        var clearPoints = function () {
            _.each(scope.tiles, function(tile) {
                tile.selected = false;
            });
        };

        // Get index of tile by its x and y coordinates
        var getTileIndex = function (x, y) {
            for (var i = 0; i < scope.tiles.length; i++) {
                if (scope.tiles[i].Axis1Value === x && scope.tiles[i].Axis2Value === y) {
                    return i;
                }
            }
        };

        // Update graph with points
        var plotContent = function (points) {
            // If multiple points
            if (_.isArray(points) && points.length) {
                _.each(points, function(point) {
                    if (point.x && point.y) {
                        var tileIndex = getTileIndex(point.x, point.y);
                        scope.tiles[tileIndex].selected = true;
                    }
                });
                // If single point
            } else if (points && points.x && points.y) {
                var tileIndex = getTileIndex(points.x, points.y);
                scope.tiles[tileIndex].selected = true;
                if (scope.callback) {
                    scope.callback(scope.tiles[tileIndex]);
                }
            } else {
                if (scope.callback) {
                    scope.callback();
                }
            }
        };

        var init = function () {
            drawGraphAxis();
            if (!scope.tiles.length) {
                createTiles();
                if (scope.points) {
                    plotContent(scope.points);
                }
            }
            // Watch for changes on points expression and update
            scope.$watch("points", function (newPoints) {
                if (newPoints) {
                    clearPoints();
                    plotContent(newPoints);
                }
            }, true);

            // Watch for changes on x axis
            scope.$watch("horizontalAxis", function (newAxis, oldAxis) {
                if (newAxis.length !== oldAxis.length) {
                    drawGraphAxis();
                    createTiles();
                }
            }, true);

            // Watch for changes on y axis
            scope.$watch("verticalAxis", function (newAxis, oldAxis) {
                if (newAxis.length !== oldAxis.length) {
                    drawGraphAxis();
                    createTiles();
                }
            }, true);
        };

        init();
    }

    return {
        restrict: "EA",
        replace: true,
        template: require("views/Shared/Directives/heatmap.jade"),
        scope: {
            horizontalAxis: "=",
            verticalAxis: "=",
            horizontalLabel: "=",
            verticalLabel: "=",
            points: "=",
            tiles: "=",
            mode: "@",
            contentKey: "@",
            callback: "="
        },
        link: link
    };
}

heatmap.$inject = ["$rootScope", "ENUMS"];
