
function navController($scope, ENUMS) {
    var vm = this;
    vm.questionTypes = ENUMS.QuestionTypes;

    vm.$onInit = function () {
        setInitialState();
        vm.sections[vm.sectionIndex].isActive = true;
        $scope.$watch("vm.sectionIndex", function (value) {
            deactivateAllSections();
            vm.sections[value].isActive = true;
        });
    };

    function setInitialState() {
        _.each(vm.sections, function(section) {
            if (section.Id === "00000000-0000-0000-0000-000000000000") {
                section.isWelcomeSection = true;
            }
            section.expanded = false;
        });
        vm.ready = true;
    };

    function deactivateAllSections() {
        _.each(vm.sections, function(section) {
            section.isActive = false;
        });
    };

    vm.toggleExpand = function (section) {
        section.expanded = !section.expanded;
    };

    vm.goToSection = function (index, section) {
        if (!section.IsSkipped) {
            vm.sectionIndex = index;
            vm.sectionCallback(section);
        }
    };
};

navController.$inject = ["$scope", "ENUMS"];

export default function sectionNav() {
    var directive = {
        template: require("views/Shared/Directives/sectionNav.jade"),
        restrict: "E",
        controller: navController,
        controllerAs: "vm",
        bindToController: true,
        scope: {
            sections: "=",
            sectionCallback: "=",
            sectionIndex: "=",
            showNumbers: "=",
            showCount: "="
        }
    };

    return directive;
}
