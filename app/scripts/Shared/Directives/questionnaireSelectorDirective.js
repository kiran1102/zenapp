
"use strict";

function QuestionnaireSelectorController() {
    var vm = this;
}

export default function QuestionnaireSelectorDirective() {
    return {
        template: require("views/Shared/Directives/questionnaireSelector.jade"),
        restrict: "E",
        controller: QuestionnaireSelectorController,
        controllerAs: "vm",
        bindToController: true,
        replace: true,
        scope: {
            availableQuestionnaires: "=",
            onQuestionnaireSelected: "&"
        }
    }
}
