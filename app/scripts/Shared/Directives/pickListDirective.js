
function pickListController($filter) {
	var vm = this;

	var init = function () {
		vm.hasEvents = !(_.isEmpty(vm.events));
		vm.nameKey = vm.nameKey || "Group";
		vm.idKey = vm.idKey || "GroupId";
		vm.filter = _.isArray(vm.filter) ? vm.filter : [];
		vm.getGroupName = (vm.hasEvents && vm.events.getGroupName && _.isFunction(vm.events.getGroupName) ? vm.events.getGroupName : vm.defaultGetGroupName);
	};

	vm.defaultGetGroupName = function (id) {
		var filterObject = {};
		filterObject[vm.idKey] = id;

		var groupItems = $filter("filter")(vm.list, filterObject);
		if (groupItems && groupItems.length) {
			return groupItems[0][vm.nameKey];
		}
		return "";
	};

	vm.toggleGroup = function (id) {
		if (_.isArray(vm.filter)) {
			var index = vm.filter.indexOf(id);
			if (index > -1) {
				vm.filter.splice(index, 1);
				if (vm.hasEvents && vm.events.onRemove) {
					vm.events.onRemove(id);
				}
			}
			else {
				vm.filter.push(id);
				if (vm.hasEvents && vm.events.onAdd) {
					vm.events.onAdd(id);
				}
			}
		}
	};

	vm.isFiltered = function (id) {
		return (_.isArray(vm.filter) && vm.filter.indexOf(id) > -1);
	};

	init();
};

pickListController.$inject = ["$filter"];

export default function pickList() {
	var directive = {
		template: require("views/Shared/Directives/pickList.jade"),
		restrict: "E",
		controller: pickListController,
		controllerAs: "vm",
		bindToController: true,
		scope: {
			list: "=",
			nameKey: "@",
			idKey: "@",
			filter: "=",
			events: "="
		},
	};

	return directive;
}
