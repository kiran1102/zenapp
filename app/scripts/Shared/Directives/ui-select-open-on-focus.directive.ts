declare var angular: any;

export default function uiSelectOpenOnFocus($timeout: ng.ITimeoutService): any {
    return {
        require: "uiSelect",
        restrict: "A",
        link: (
            $scope: any,
            el: any,
            attrs: any,
            uiSelect: any,
        ): void => {
            let autoopen = true;

            angular.element(uiSelect.focusser || uiSelect.focusInput).on("focus", (): void => {
                if (autoopen) {
                    uiSelect.activate();
                }
            });

            // Disable the auto open when this select element has been activated.
            $scope.$on("uis:activate", (): void => {
                autoopen = false;
            });

            // Re-enable the auto open after the select element has been closed
            $scope.$on("uis:close", (): void => {
                autoopen = false;
                $timeout((): void => {
                    autoopen = true;
                }, 250);
            });
        },
    };
}

uiSelectOpenOnFocus.$inject = ["$timeout"];
