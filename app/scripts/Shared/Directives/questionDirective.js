import { AssessmentStates } from "constants/assessment.constants";
import { ProjectModes } from "enums/assessment.enum";
import { IQuestionTypes } from "enums/question-types.enum";

function QuestionController($rootScope, $q, $filter, ENUMS) {
    var vm = this;

    vm.responseTypes = ENUMS.ResponseTypes;
    vm.questionTypes = IQuestionTypes;
    vm.projectModes = ProjectModes;
    vm.projectStates = AssessmentStates;
    vm.templateStates = ENUMS.TemplateStates;
    vm.inventoryIds = ENUMS.InventoryTableIds;
    vm.multiChoiceTypes = ENUMS.MultiChoiceTypes;

    vm.inputPlaceholders = {};
    vm.inputPlaceholders[vm.questionTypes.Application] = $rootScope.t("EnterAnApplication");
    vm.inputPlaceholders[vm.questionTypes.User] = $rootScope.t("EnterAUser");
    vm.inputPlaceholders[vm.questionTypes.Location] = $rootScope.t("EnterALocation");
    vm.inputPlaceholders[vm.questionTypes.Provider] = $rootScope.t("EnterAProvider");

    var init = function() {

        if (vm.events) return;

        vm.events = {
            textAreaChange: vm.textAreaSave,
            textAreaSave: vm.textAreaSave,
            yesNoChange: vm.yesNoChange,
            multiChoiceSelected: vm.multiChoiceSelected,
            multiChoiceGroupSelected: vm.multiChoiceGroupSelected,
            multiChoiceChange: vm.multiChoiceChange,
            multiSelectChange: vm.multiSelectChange,
            multiSelectGroupChange: vm.multiSelectGroupChange,
            toggleNotSure: vm.toggleNotSure,
            toggleNotApplicable: vm.toggleNotApplicable,
            otherOptionSet: vm.otherOptionSet,
            otherOptionSave: null,
            justificationSave: null,
        };
    };

    vm.answerQuestion = function() {
        vm.question.ResponseType = vm.responseTypes.Value;
        if (!_.isUndefined(vm.question.AllowOther)) {
            vm.question.OtherAnswer = null;
        }
        vm.question.NotApplicable = false;
        vm.question.isAnswered = true;
    };

    vm.textAreaSave = function() {
        vm.question.ResponseType = vm.responseTypes.Value;
        if (!_.isUndefined(vm.question.AllowOther)) {
            vm.question.OtherAnswer = null;
        }
        vm.question.NotApplicable = false;
        if (vm.question.Answer) {
            vm.question.isAnswered = true;
        } else {
            vm.question.isAnswered = false;
        }
    };

    vm.yesNoChange = function() {
        vm.question.ResponseType = vm.responseTypes.Value;
        if (!_.isUndefined(vm.question.AllowOther)) {
            vm.question.OtherAnswer = null;
        }
        vm.question.NotApplicable = false;
        if ((vm.question.Answer !== null) && (vm.question.Answer !== "") && (vm.question.Answer !== undefined)) {
            vm.question.isAnswered = true;
        } else {
            vm.question.isAnswered = false;
        }
    };

    vm.multiChoiceSelected = function(x, y, oIndex) {
        if (oIndex >= 0) {
            if (_.isArray(vm.question.Answer)) {
                return vm.question.Answer.indexOf(vm.question.Options[oIndex].Value.toString()) >= 0;
            } else if (_.isString(vm.question.Answer)) {
                return (vm.question.Answer === vm.question.Options[oIndex].Value.toString());
            }
            return false;
        }
    };

    vm.inventorySelect = function (sectionId, question, response) {};

    vm.multiChoiceGroupSelected = function(x, y, z, value) {
        if (!_.isUndefined(value) && _.isArray(vm.question.Answer)) {
            var filteredAnswers = $filter("filter")(vm.question.Answer, value.toString(), true);
            return Boolean(filteredAnswers && filteredAnswers.length);
        }
        return false;
    };

    vm.multiChoiceChange = function(x, y, value) {
        vm.question.ResponseType = vm.responseTypes.Reference;
        if (vm.question.Answer === value.toString()) {
            vm.question.Answer = "";
            vm.question.isAnswered = false;
        } else {
            vm.question.Answer = value.toString();
            vm.question.isAnswered = true;
        }
        if (!_.isUndefined(vm.question.AllowOther) && vm.question.AllowOther) {
            vm.question.OtherAnswer = null;
        }
        vm.question.NotApplicable = false;
    };

    vm.multiSelectChange = function(x, y, value) {
        vm.question.ResponseType = vm.responseTypes.Reference;
        if (_.isArray(vm.question.Answer) && vm.question.Answer.length > 0) {
            var valueIndex = vm.question.Answer.indexOf(value.toString());
            if (valueIndex < 0) {
                vm.question.Answer.push(value.toString());
            } else {
                vm.question.Answer.splice(valueIndex, 1);
            }
        } else {
            vm.question.Answer = [value.toString()];
        }
        vm.question.NotApplicable = false;
        vm.question.isAnswered = (vm.question.Answer && vm.question.Answer.length) || (vm.question.OtherAnswer && vm.question.OtherAnswer.length);
        vm.question.Justification = vm.question.isAnswered ? vm.question.Justification : null;
    };

    vm.multiSelectGroupChange = function(x, y, z, value) {
        vm.question.ResponseType = vm.responseTypes.Reference;
        if (_.isArray(vm.question.Answer) && vm.question.Answer.length > 0) {
            var valueIndex = vm.question.Answer.indexOf(value.toString());
            if (valueIndex < 0) {
                vm.question.Answer.push(value.toString());
            } else {
                vm.question.Answer.splice(valueIndex, 1);
            }
        } else {
            vm.question.Answer = [value.toString()];
        }
        vm.question.NotApplicable = false;
        vm.question.isAnswered = (vm.question.Answer && vm.question.Answer.length) || (vm.question.OtherAnswer && vm.question.OtherAnswer.length);
    };

    vm.resetOther = function() {
        vm.question.OtherAnswer = null;
    };

    vm.toggleNotSure = function() {
        if (vm.question.ResponseType === vm.responseTypes.NotSure) {
            vm.question.ResponseType = null;
        } else {
            vm.question.ResponseType = vm.responseTypes.NotSure;
        }
        vm.question.Answer = null;
        vm.question.NotApplicable = false;
        vm.question.OtherAnswer = null;
        vm.question.isAnswered = true;
    };

    vm.toggleNotApplicable = function() {
        vm.question.ResponseType = vm.responseTypes.NotApplicable;
        vm.question.Answer = null;
        vm.question.OtherAnswer = null;
        if (vm.question.NotApplicable) {
            vm.question.isAnswered = false;
            vm.question.NotApplicable = false;
        } else {
            vm.question.isAnswered = true;
            vm.question.NotApplicable = true;
        }
    };

    vm.otherOptionSet = function() {
        vm.question.ResponseType = vm.responseTypes.Other;
        if (!vm.question.MultiSelect) {
            vm.question.Answer = null;
        }
    };

    vm.hideGroup = function(list, id) {
        return (_.isArray(list) && list.indexOf(id) > -1);
    };

    vm.getGroupName = function (question, groupId) {
        if (question && question.Options) {
            var groupItems = $filter("filter")(question.Options, { "GroupId": groupId });
            if (groupItems && groupItems.length) {
                return groupItems[0].Group;
            }
        }
        return "";
    };

    vm.QuestionStringKeys = {
        Header: 10,
        Subheader: 20
    };

    var QuestionStrings = {
        DataElement: function (stringKey) {
            if (stringKey === vm.QuestionStringKeys.Header) {
                return _.template("Please select the data elements groups being processed.");
            } else if (stringKey === vm.QuestionStringKeys.Subheader) {
                return _.template("Which data elements are processed by ${name}?");
            }
        },
        default: function (stringKey) {
            if (stringKey === QuestionStringKeys.Header) {
                return _.template($rootScope.t("SelectAsManyAsYouWant"));
            } else if (stringKey === QuestionStringKeys.Subheader) {
                return _.template("Which options are processed by the ${name}?");
            }
        }
    };

    vm.getQuestionString = function (stringType, name) {
        if (vm.question.Type === vm.questionTypes.DataElement) {
            return QuestionStrings.DataElement(stringType)({ "name": name });
        }
        return QuestionStrings.default(stringType)({ "name": name });
    };

    vm.handleInventorySelect = function(sectionId, question, response) {
        if (response && response.IsPlaceholder) {
            question.Answer = response.InputValue;
        }

        if (response && response.ExistingMatch) {
             OneConfirm($rootScope.t("CreatingNewApplication"), $rootScope.t("CreateNewAppWithSameName"), "Confirm", "Cancel", true, $rootScope.t("CreateNew"), $rootScope.t("SelectExisting"))
                .then(function(result) {
                    if (_.isUndefined(result) || result === null) {
                        question.Answer = null;
                    } else if (!result) {
                        question.Answer = response.ExistingMatch;
                    }
                    vm.events.inventorySelect(sectionId, question, question.Answer);
                });
         } else {
             vm.events.inventorySelect(sectionId, question, question.Answer);
         }
    };

    vm.toggleInventorySubmitBtn = function(question, bool) {
        if (question.isShowingSubmitBtn !== bool) {
            question.isShowingSubmitBtn = bool;
        }
    };

    vm.isInventorySubmitBtnShowing = function(question) {
        if (question.AllowOther) {
            return question.Answer && !question.Answer.Value && question.isShowingSubmitBtn;
        }
        return false;
    };

    init();
}

QuestionController.$inject = ["$rootScope", "$q", "$filter", "ENUMS"];

export default function Question() {
    var directive = {
        template: require("views/Shared/Directives/question.jade"),
        restrict: "E",
        controller: QuestionController,
        controllerAs: "vm",
        bindToController: true,
        replace: true,
        scope: {
            question: "=",
            section: "=",
            project: "=",
            template: "=",
            events: "=?",
            isDisabled: "=",
            projectMode: "=",
            editMode: "=",
            readOnly: "=",
            questionUniqId: "<?",
            questionTextModifier: "<?"
        }
    };
    return directive;
}
