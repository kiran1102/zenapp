
interface IFileObject {
    name: string;
    url: string;
}

export default function fileUpload($parse: any): any {
    return {
        link: (scope: any, elem: any, attrs: any): any => {
            elem.bind("change", (): void => {
                const file: File = elem[0].files[0];
                const fileObj: IFileObject = {
                    name: file.name,
                    url: "",
                };
                const reader: FileReader = new FileReader();
                reader.readAsDataURL(file);
                reader.onload = (): void => {
                    (fileObj.url as any) = reader.result;

                    switch (attrs.id) {
                        case "logo-upload":
                            // located in BrandingSettingsController.js
                            scope.app.branding.logo = fileObj;
                            break;
                        case "brand-upload":
                            // located in BrandingSettingsController.js
                            scope.app.branding.header.icon = fileObj;
                            break;
                        case "attachment-upload":
                            // located in AttachmentController.js
                            scope.updateAttachment(elem, file);
                            break;
                        default:
                            break;
                    }

                    $parse(attrs.fileUpload).assign(scope, fileObj);
                    scope.$apply();
                };
            });
        },
        restrict: "A",
    };
}

fileUpload.$inject = ["$parse"];
