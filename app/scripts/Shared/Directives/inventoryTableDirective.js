import { AssessmentStates } from "constants/assessment.constants";

function TableController($rootScope, $scope, $state, $q, $filter, ENUMS, $timeout, NotificationService, Permissions) {
    var vm = this;

    vm.loading = false;
    vm.changesMade = false;
    vm.InputTypes = ENUMS.TableInputTypes;
    vm.permissions = Permissions;
    vm.elementCountOffset = 0;

    var saveOnLeave = false,
        contextSwitchHandled = false,
        requiredColumns,
        requiredColumnKeys,
        userColumns,
        statusColumns;

    var scroller = document.getElementById("scroll-list");
    var targets = {
        addButton: "smart-table__new-row"
    };
    var isTarget = function (string, target) {
        return string.indexOf(target) > -1;
    };

    vm.filter = {
        elementFilter: "",
        isOpen: false,
        toggleFilter: function () {
            vm.filter.isOpen = !vm.filter.isOpen;
        }
    };

    vm.sortComparator = function (record) {
        if (vm.sortColumn && record[vm.sortColumn]) {
            return record[vm.sortColumn][vm.keys.Item_Name];
        }
        return "";
    };

    // Table Sorting
    vm.sortBy = function (column) {
        if (!column) {
            // Check for previously saved sort column
            var foundItem = false;
            for (var i; i < vm.columns.length; i++) {
                var item = vm.columns[i];
                if (item.sorted) {
                    foundItem = true;
                    vm.sortBy(item);
                    break;
                }
            }
            // If no previously saved sort column, default to Name
            if (!foundItem) {
                vm.sortColumn = column[vm.keys.Column_ModelKey];
            }
        } else {
            // Reset sorted flag to false for all
            _.each(vm.columns, function(item) {
                item.sorted = false;
            });

            vm.sortColumn = column[vm.keys.Column_ModelKey];

            // Set sorted flag on sorted column
            column.sorted = true;
        }
        // Use lodash sortBy to use comparator
        var newOrderedList = _.sortBy(vm.rows, vm.sortComparator);
        // Reverse from previous
        vm.sortReverse = !vm.sortReverse;
        // Reverse based on boolean
        if (vm.sortReverse) {
            newOrderedList.reverse();
        }
        vm.rows = newOrderedList;
    };

    function setColumnFilterNumber(column) {
        if (vm.keys && vm.keys.Column_List && column[vm.keys.Column_List]) {
            column.ItemsFiltered = $filter("filter")(column[vm.keys.Column_List], {
                Filtered: false
            }).length;
        }
    }

    function setAllColumnFilterCounts() {
        if (vm.columns) {
            _.each(vm.columns, function(column) {
                column.RunFilter = setColumnFilterNumber;
                column.RunFilter(column);
            });
        }
    }

    function scrollToBottom() {
        $timeout(function () {
            scroller.scrollTop = scroller.scrollHeight;
        }, 0, false);
    }

    vm.getColumnWidth = function (column) {
        return ((column.Width || column.Default.Width) || "normal");
    };

    vm.onPrimaryCancel = function (item, form) {
        if (!item[vm.keys.Item_Id] && !form.$data) {
            for (var i = vm.rows.length - 1; i !== 0; i--) {
                if (_.isEqual(item, vm.rows[i])) {
                    vm.rows.splice(i, 1);
                    return;
                }
            }
        }
    };

    vm.toggleColumnFilterItem = function (column, item) {
        if (item) {
            item.Filtered = !item.Filtered;
            if (column) {
                setColumnFilterNumber(column);
            }
        }
    };

    vm.toggleAllColumnFilterItems = function (column) {
        if (column && vm.keys && vm.keys.Column_List && column[vm.keys.Column_List]) {
            // If there are any items being shown, then stop showing everything
            if (column.ItemsFiltered) {
                _.each(column[vm.keys.Column_List], function(item) {
                    item.Filtered = true;
                });
            } else {
                _.each(column[vm.keys.Column_List], function(item) {
                    item.Filtered = false;
                });
            }
            setColumnFilterNumber(column);
        }
    };

    vm.setAsChanged = function (column, item) {
        item.hasUpdates = true;
        vm.changesMade = true;
        if (column[vm.keys.Column_Required]) {
            validateEmpty(column, item);
        }
        if (column.InvalidateSibling) {
            resetValue(item, column.InvalidateSibling);
        }
    };

    function resetValue(item, columnKey) {
        item[columnKey] = _.cloneDeep(vm.defaultItem[columnKey]);
    }

    function saveItem(item) {
        var deferred = $q.defer();
        // If user column exists, validate this item's user before saving
        var readyToSave = (userColumns && userColumns.length) ? validateUsers(item) : true;
        if (readyToSave) {
            var itemId = vm.events.getItemId(vm.keys, item);
            if (!itemId) {
                vm.events.add(vm.keys, item).then(function (response) {
                    if (response.result) {
                        saveSuccessCb(item, response);
                    } else {
                        saveFailCb(item, response);
                    }
                    deferred.resolve(response);
                });
            } else {
                vm.events.update(vm.keys, item).then(function (response) {
                    if (response.result) {
                        saveSuccessCb(item, response);
                    } else {
                        saveFailCb(item, response);
                    }
                    deferred.resolve(response);
                });
            }
        }
        // If user validation fails
        else {
            NotificationService.alertWarning($rootScope.t("Error"), $rootScope.t("InvalidUserWarning"));
            deferred.resolve({ result: false });
        }
        return deferred.promise;
    }

    vm.setItem = function (item, key, model) {
        item[key] = item[key] || {};
        item[key][vm.keys.Item_Name] = model[vm.keys.Column_List_Value];
        item[key][vm.keys.Item_Id] = model[vm.keys.Column_List_Id];
    };

    vm.setUser = function (item, key, model) {
        vm.setItem(item, key, model);
        item[key].hasBeenSelected = true;
    };

    vm.checkUserValue = function (item, key, data, column) {
        // If the name has changed
        if (item[key][vm.keys.Item_Name] !== data) {
            vm.setAsChanged(column, item);
            // item was NOT explicitly selected
            if (!item[key].hasBeenSelected) {
                item[key][vm.keys.Item_Name] = data;
                item[key][vm.keys.Item_Id] = data;
            }
        }
    };

    vm.removeSelectedBool = function (item, key) {
        item[key].hasBeenSelected = false;
    };

    vm.isDisabled = function (item) {
        if (vm.events && _.isFunction(vm.events.isDisabled)) {
            return vm.events.isDisabled(vm.keys, item);
        }
        return false;
    };

    vm.searchFilter = function (elements, string) {
        return (vm.events && _.isFunction(vm.events.searchFilter) ? vm.events.searchFilter(elements, string) : elements);
    };

    vm.saveChanges = function () {
        vm.saveInProgress = true;
        var savePromises = [];
        _.each(vm.rows, function(item) {
            if (item.hasUpdates) {
                savePromises.push(saveItem(item));
            }
        });

        return $q.all(savePromises).then(function (responses) {
            var hasError = $filter("filter")(responses, {
                result: false
            });
            if (hasError.length) {
                vm.changesMade = true;
            } else {
                vm.changesMade = false;
                removeAllErrors();
                NotificationService.alertSuccess($rootScope.t("Success"), $rootScope.t("AllChangesWereSaved"));
            }
            vm.saveInProgress = false;
            return !vm.changesMade;
        });
    };

    function saveSuccessCb(item, response) {
        if (_.isFunction(vm.events.afterSave)) {
            var newItem = vm.events.afterSave(item, response.data, vm.columns, vm.keys);
            if (newItem) {
                item = newItem;
            }
        }

        item.hasUpdates = false;
        removeColumnErrors(item);
    }

    function saveFailCb(item, response) {
        switch (response.status) {
            case ENUMS.StatusCodes.BadRequest:
                validateEmptyRow(item);
                break;
            case ENUMS.StatusCodes.Conflict:
                validateDuplicate(item);
                break;
            default:
        }
    }

    function validateEmptyRow(item) {
        _.each(requiredColumns, function(column) {
            validateEmpty(column, item);
        });
    }

    function validateEmpty(column, item) {
        item.emptyErrors = item.emptyErrors || {};
        if (itemIsEmpty(column, item)) {
            item.emptyErrors[column[vm.keys.Column_ModelKey]] = true;
        } else {
            item.emptyErrors[column[vm.keys.Column_ModelKey]] = null;
        }
    }

    function validateDuplicate(item) {
        // Start by clearing all errors
        removeColumnErrors(item, "duplicateErrors");
        var comparators = [];
        // Get Comparators of required fields on a single item
        _.each(requiredColumns, function(column) {
            var columnKey = column[vm.keys.Column_ModelKey];
            var cellKey = column.input === ENUMS.TableInputTypes.Typeahead ? vm.keys.Item_Id : vm.keys.Item_Name;
            var comparator = {
                Cell: columnKey,
                Key: cellKey,
                Value: item[columnKey][cellKey]
            };
            comparators.push(comparator);
        });
        // Filter to all rows that match that single item
        var matchingRows = $filter("TableComparator")(vm.rows, comparators, true);
        // For each required field (primary key) on these items, mark with error
        // Mark with errors if more than one found (duplicates)
        if (matchingRows.length > 1) {
            _.each(matchingRows, function(row) {
                // Mark with errors if more than one found (duplicates)
                row.duplicateErrors = row.duplicateErrors || {};
                _.each(requiredColumnKeys, function(key) {
                    row.duplicateErrors[key] = true;
                });
            });
        }
    }

    function validateUsers(item) {
        var hasInvalidUsers = false;
        _.each(userColumns, function(column) {
            item.userErrors = item.userErrors || {};
            var user = item[column[vm.keys.Column_ModelKey]];
            var isValidUser = false;
            if (user && user[vm.keys.Item_Id]) {
                // Does user have a valid Guid or email address for an ID?
                var isValidGuid = Utilities.matchRegex(user[vm.keys.Item_Id], ENUMS.Regex.Guid);
                var isValidEmail = Utilities.matchRegex(user[vm.keys.Item_Name], ENUMS.Regex.Email);
                isValidUser = isValidGuid || isValidEmail;
            }
            item.userErrors[column[vm.keys.Column_ModelKey]] = !isValidUser;
            if (!isValidUser) {
                hasInvalidUsers = true;
            } else {
                removeErrors(item, "userErrors", column);
            }
        });
        return !hasInvalidUsers;
    }

    function removeErrors(item, typeKey, column) {
        if (typeKey) {
            if (item[typeKey]) {
                item[typeKey][column[vm.keys.Column_ModelKey]] = false;
            }
        } else {
            if (item.duplicateErrors) {
                item.duplicateErrors[column[vm.keys.Column_ModelKey]] = false;
            }
            if (item.emptyErrors) {
                item.emptyErrors[column[vm.keys.Column_ModelKey]] = false;
            }
            if (item.userErrors) {
                item.userErrors[column[vm.keys.Column_ModelKey]] = false;
            }
        }
    }

    function removeColumnErrors(item, typeKey) {
        _.each(vm.columns, function(column) {
            removeErrors(item, typeKey, column);
        });
    }

    function removeAllErrors(type) {
        _.each(vm.rows, function(item) {
            removeColumnErrors(item, type);
        });
    }

    function setColumnProps() {
        requiredColumnKeys = [];
        requiredColumns = $filter("filter")(vm.columns, { "Required": true });
        _.each(requiredColumns, function(column) {
            requiredColumnKeys.push(column[vm.keys.Column_ModelKey]);
        });
        userColumns = $filter("filter")(vm.columns, { "Input": vm.InputTypes.User });
        statusColumns = $filter("filter")(vm.columns, { "Input": vm.InputTypes.ProjectState });
    }

    function itemIsEmpty(column, item) {
        // No object
        return !item[column[vm.keys.Column_ModelKey]] ||
            // Text type and no Value
            (column.input === ENUMS.TableInputTypes.Text && !item[column[vm.keys.Column_ModelKey]].Value) ||
            // Typeahead type and no Id
            (column.input === ENUMS.TableInputTypes.Typeahead && !item[column[vm.keys.Column_ModelKey]].ValueId);
    }

    vm.selectText = function (item) {
        if (item) {
            $timeout(function () {
                item.$form.$editables[0].inputEl[0].select();
            }, 0);
        }
    };

    vm.canEdit = function (element, cellKey, item) {
        var deferred = $q.defer();
        if (element[cellKey][vm.keys.Item_Name] === "Not Applicable") {
            element[cellKey][vm.keys.Item_Name] = "";
            element[cellKey][vm.keys.Item_Id] = "";
        }
        vm.selectText(item);
        deferred.resolve();
        return deferred.promise;
    };

    // add data element
    vm.addNewItem = function () {
        var newItem = _.cloneDeep(vm.defaultItem) || { "10": "" };
        newItem[vm.keys.Item_Id] = "";
        if (statusColumns && statusColumns.length) {
            var statusCol = statusColumns[0];
            newItem = handleRecordLoaded(newItem, statusCol);
        }
        newItem.hasUpdates = true;
        newItem.isNew = true;
        vm.rows.push(newItem);
        vm.changesMade = true;
        vm.pages.totalElements++;
        scrollToBottom();
    };

    vm.cellInit = function (col, el, item) {
        if (el.isNew && col[vm.keys.Column_ModelKey] === 10) {
            $timeout(function () {
                item.$form.$show();
            }, 0);
        }
        el.isNew = false;
    };

    vm.cellFocus = function (item) {
        $timeout(function () {
            item.$form.$show();
        }, 0);
    };

    vm.cellBlur = function (item, suppressSubmit) {
        $timeout(function () {
            if (!suppressSubmit) {
                item.$form.$submit();
            }
            item.$form.$hide();
        }, 0);
    };

    vm.setSelected = function (element) {
        if (element) {
            element.selected = !element.selected;
        }
        vm.SelectedItems = $filter("filter")(vm.elements, {
            "selected": true
        }).length;
    };

    vm.toggleAllSelected = function (rows) {
        var toggle = (vm.SelectedItems === vm.elements.length) ? false : true;
        if (rows) {
            _.each(rows, function(element) {
                element.selected = toggle;
            });
        }
        vm.SelectedItems = $filter("filter")(vm.elements, {
            "selected": true
        }).length;
    };

    vm.filterItemThen = function (item, result) {
        // For all items that were filtered out, remove them from the selec count.
        if (!result) {
            item.selected = false;
        }
    };

    vm.filterListThen = function (items) {
        // Recalculate Selected Items.
        if (items) {
            vm.SelectedItems = $filter("filter")(items, {
                "selected": true
            }).length;
        }
        return items;
    };

    vm.closeEditOptions = function () {
        vm.editOptionsShowing = false;
    };

    vm.toggleEditOptions = function () {
        vm.editOptionsShowing = !vm.editOptionsShowing;
    };

    vm.optionsCallback = function (newRows) {
        if (_.isArray(newRows)) {
            var rowDifference = newRows.length - vm.rows.length;
            vm.pages.totalElements += rowDifference;
            vm.rows = newRows;
        }
        vm.loading = false;
    };

    vm.optionSelect = function () {
        vm.loading = true;
        vm.closeEditOptions();
    };

    vm.evaluateKeypress = function (event) {
        event.preventDefault();
        var Keys = {
            "Enter": 13
        };
        switch (event.keyCode) {
            case Keys.Enter:
                if (isTarget(event.target.className, targets.addButton)) {
                    vm.addNewItem();
                }
                break;
            default:
        }
    };

    vm.getStatusClass = function (status) {
        switch (status) {
            case AssessmentStates.NotStarted:
                return "default";
            case AssessmentStates.InProgress:
                return "in-progress";
            case AssessmentStates.UnderReview:
                return "under-review";
            case AssessmentStates.InfoNeeded:
                return "needs-info";
            case AssessmentStates.RiskAssessment:
                return "risk-assessment";
            case AssessmentStates.RiskTreatment:
                return "risk-treatment";
            case AssessmentStates.Completed:
                return "completed";
            default:
                return "default";
        }
    };

    vm.launchAssessments = function () {
        var selected = $filter("filter")(vm.elements, {
            "selected": true
        });

        // Check to make sure all are saved
        var AllItemsUptoDate = (_.isFunction(vm.events.hasUpdates) ? vm.events.hasUpdates(selected, vm.keys, vm.resourceKeys) : false);

        if (!AllItemsUptoDate) {
            OneAlert("", $rootScope.t("SaveBeforeSending"));
        } else {
            var cb = function (response) {
                if (response.result) {
                    _.forEach(selected,
                        function (item) {
                            var itemId = vm.events.getItemId(vm.keys, item) || "";
                            item.selected = false;
                            if (_.isArray(response.launched) && response.launched.length && _.includes(response.launched, itemId) && statusColumns && statusColumns.length) {
                                _.each(statusColumns, function(column) {
                                    var cell = item[column[vm.keys.Column_ModelKey]];
                                    cell[vm.keys.Item_Name] = AssessmentStates.InProgress;
                                    cell.Class = vm.getStatusClass(AssessmentStates.InProgress);
                                    cell[vm.keys.Item_Id] = response.data[itemId];
                                });
                            }
                        }
                    );
                }
                else {
                    _.forEach(selected,
                        function (item) {
                            item.selected = false;
                        }
                    );
                }
            };
            vm.events.launch(selected, vm.columns, vm.keys, $scope, cb);
        }
    };

    function stateChangeLeaveEvent(event, toState, toParams) {
        if (!contextSwitchHandled) {
            var stateChange = {
                event: event,
                toState: toState,
                toParams: toParams
            };
            handleLeaveEvent(false, null, stateChange);
        }
    }

    function contextSwitchLeaveEvent(event, callbacks) {
        handleLeaveEvent(true, callbacks, null);
    }

    function handleLeaveEvent(isContextSwitch, contextSwitchCallbacks, stateChange) {
        if (saveOnLeave && vm.changesMade) {
            if (isContextSwitch) {
                contextSwitchCallbacks.cancel();
            } else {
                stateChange.event.preventDefault();
            }

            launchLeavingConfirmation().then(
                function (result) {
                    if (!_.isUndefined(result) && result !== null) {
                        if (result) {
                            vm.saveChanges().then(
                                function (saveResult) {
                                    if (saveResult) {
                                        saveOnLeave = false;
                                        if (isContextSwitch) {
                                            contextSwitchHandled = true;
                                            contextSwitchCallbacks.continueSwitch();
                                        }
                                        else $state.go(stateChange.toState.name, stateChange.toParams);
                                    }
                                }
                            );
                        }
                        else {
                            saveOnLeave = false;
                            if (isContextSwitch) {
                                contextSwitchHandled = true;
                                contextSwitchCallbacks.continueSwitch();
                            }
                            else $state.go(stateChange.toState.name, stateChange.toParams);
                        }
                    }
                }
            );
        }
    }

    function launchLeavingConfirmation() {
        return OneConfirm("", $rootScope.t("UnsavedChangesSaveBeforeContinuing"), "", "", true, $rootScope.t("SaveChanges"), $rootScope.t("DontSave"));
    }

    function handlePageChange(tableId, page, size, filter, search) {
        vm.loading = true;
        var params = {
            page: page,
            size: size,
            filter: filter,
            search: search,
        };
        $state.go(".", params, {notify: false});
        vm.events.getPage(tableId, page, size, filter, search).then( function (response) {
            vm.loading = false;
            if (!response.result) return;
            vm.rows = handleRecordsLoaded(response.data.Records);
            vm.pages = vm.events.formatPaginationConfig(response.data.PageMetadata);
        });
    }

    function handleRecordLoaded(row, statusCol) {
        var cell = row[statusCol[vm.keys.Column_ModelKey]];
        row[statusCol[vm.keys.Column_ModelKey]].Class = vm.getStatusClass(row[statusCol[vm.keys.Column_ModelKey]][vm.keys.Item_Name]);
        return row;
    }

    function handleRecordsLoaded(rows) {
        if (statusColumns && statusColumns.length) {
            var statusCol = statusColumns[0];
            _.each(rows, function (row) {
                row = handleRecordLoaded(row, statusCol);
            });
        }
        return rows;
    }

    function saveBeforePageChange(page, size, filter, search) {
        var changePage = function () { handlePageChange(vm.tableId, page, size, filter, search) };
        if (!vm.changesMade) {
            changePage();
            return;
        }
        launchLeavingConfirmation().then( function(result) {
            if (!result) {
                vm.changesMade = false;
                changePage();
                return;
            }
            vm.saveChanges().then(
                function (saveResult) {
                    if (!saveResult) {
                        vm.pages = _.clone(vm.pages);
                        return;
                    }
                    changePage();
                }
            );
        });
    }

    function setSearchConfig() {
        vm.searchConfig = {
            searchTerm: vm.pages.search || "",
            fieldLabel: $rootScope.t("SearchSelectedColumn"),
            placeholder: $rootScope.t("Search"),
            required: true,
        };
        var selectedColumn;
        if (vm.pages.filter) {
            selectedColumn = _.find(vm.columns, function(column) {
                return column.ColumnName === vm.pages.filter;
            });
        }
        var shownColumns = _.filter(vm.columns, function(column) {
            return !column.Hide;
        });
        vm.selectConfig = {
            list: shownColumns,
            fieldLabel: $rootScope.t("SelectAColumn"),
            labelKey: "ColumnName",
            selection: selectedColumn || shownColumns[0],
            required: true,
        };
        vm.buttonConfig = {
            buttonText: $rootScope.t("Search"),
            handleClick: handleSearch,
            closeOnClick: true,
        }
        vm.clearConfig = {
            buttonText: $rootScope.t("Clear"),
            handleClick: handleSearchClear,
            closeOnClick: true,
        }
    }

    function handleSearchClear() {
        vm.selectConfig.selection = vm.columns[0];
        saveBeforePageChange(1, vm.pages.size);
    }

    function handleSearch(data) {
        if (data.selection && data.search) {
            saveBeforePageChange(1, vm.pages.size, data.selection.ColumnName, data.search);
        }
    }

    vm.getPageRange = function() {
        if (!vm.pages || !vm.pages.totalElements) return "0";
        var firstNum = vm.pages.first ? 1 : vm.pages.number * vm.pages.size + 1;
        var secondNum = firstNum + vm.elements.length - 1;
        return firstNum + " - " + secondNum;
    };

    var destroyStateChangeListener = $scope.$on("$stateChangeStart", stateChangeLeaveEvent);
    var destroyContextSwitchListener = $scope.$on("event:context-switch", contextSwitchLeaveEvent);
    var destroyListener = $scope.$on("$destroy", function () {
        destroyStateChangeListener();
        destroyContextSwitchListener();
        destroyListener();
    });

    function init() {
        setAllColumnFilterCounts();
        setColumnProps();
        setSearchConfig();

        vm.HasEvents = (Object.keys(vm.events).length);
        vm.HasOptions = (vm.HasEvents && _.isArray(vm.events.options) && vm.events.options.length);
        vm.rows = handleRecordsLoaded(vm.rows);

        saveOnLeave = _.cloneDeep(vm.saveOnLeave);

        $scope.$on("CHANGE_PAGE", function(event, page) {
            saveBeforePageChange(page + 1, vm.pages.size, $state.params.filter, $state.params.search);
        });
    };

    init();

}

TableController.$inject = ["$rootScope", "$scope", "$state", "$q", "$filter", "ENUMS", "$timeout", "NotificationService", "Permissions"];

export default function InventoryTable() {
    var directive = {
        template: require("views/Shared/Directives/inventoryTable.jade"),
        restrict: "E",
        controller: TableController,
        controllerAs: "vm",
        bindToController: true,
        scope: {
            columns: "=",
            rows: "=",
            keys: "=",
            resourceKeys: "=",
            events: "=",
            defaultItem: "=",
            permissions: "=",
            tableId: "=",
            tableName: "@",
            saveOnLeave: "=",
            pages: "=",
        }
    };

    return directive;
}
