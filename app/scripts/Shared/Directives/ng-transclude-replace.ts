export default function ngTranscludeReplace($compile: ng.ICompileService): any {
    return {
        terminal: true,
        restrict: "EAC",
        compile: (tElement: any): any => {

            const fallbackLinkFn: Function = $compile(tElement.contents());
            tElement.empty();

            return (
                $scope: any,
                $element: ng.IAugmentedJQuery,
                $attrs: any,
                controller: any,
                $transclude: any,
            ): void | undefined => {
                if (!$transclude) return;

                if ($attrs.ngTranscludeReplace === $attrs.$attr.ngTranscludeReplace) {
                    $attrs.ngTranscludeReplace = "";
                }
                const slotName: string = $attrs.ngTranscludeReplace || $attrs.ngTranscludeReplaceSlot;

                $transclude(ngTranscludeCloneAttachFn, null, slotName);

                if (slotName && !$transclude.isSlotFilled(slotName)) {
                    useFallbackContent();
                }

                function ngTranscludeCloneAttachFn(clone: Element[], transcludedScope: ng.IScope): void {
                    if (clone.length) {
                        $element.replaceWith(clone);
                    } else {
                        useFallbackContent();
                        transcludedScope.$destroy();
                    }
                }

                function useFallbackContent() {
                    fallbackLinkFn($scope, (clone: Element) => {
                        $element.replaceWith(clone);
                    });
                }
            };
        },
    };
}

ngTranscludeReplace.$inject = ["$compile"];
