export default function oneClickOutside($document: JQuery): any {

    function linkClickOutside(
        scope: any,
        el: any,
        attr: any,
    ): void {
        const CanTouch = checkCanTouch();

        function eventHandler(e: any): void {
            if (scope.oneClickOutside && (el !== e.target && !el[0].contains(e.target))) {
                scope.$apply(() => {
                    scope.$eval(scope.oneClickAction);
                });
            }
        }

        if (CanTouch) {
            $document.on("touchstart", eventHandler);
        }

        $document.on("click", eventHandler);

        scope.$on("$destroy", (): void => {
            if (CanTouch) {
                $document.off("touchstart", eventHandler);
            }

            $document.off("click", eventHandler);
        });

    }

    function checkCanTouch(): boolean {
        return (!!("ontouchstart" in window) || !!(navigator.maxTouchPoints));
    }

    return {
        link: linkClickOutside,
        restrict: "A",
        scope: {
            oneClickAction: "&",
            oneClickOutside: "=",
        },
    };
}

oneClickOutside.$inject = ["$document"];
