
export default function autoOpen() {
  var ENTER = 13;

  function compile(tElement) {
    tElement.find("input").attr("ng-click", "ctrl.onClick($event)").next().remove();

    return function (scope, element, attrs, datePicker) {

      datePicker.onClick = function (event) {
        datePicker.openCalendarPane(event);
      };

      datePicker.onFocus = function (event) {
        datePicker.openCalendarPane(event);
      };

      datePicker.ngInputElement.on("keypress", function (event) {
        if (event.keyCode === ENTER) {
          datePicker.openCalendarPane(event);
        }
      });
    };
  }

  return {
    compile: compile,
    priority: -1,
    require: "mdDatepicker",
    restrict: "A"
  };
}
