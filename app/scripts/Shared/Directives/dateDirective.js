﻿"use strict";

export default function date($compile, amDateFormatFilter, amCalendarFilter, $rootScope) {
	// Usage:
	//     <date model='dateString'></date>
	// Creates:
	//

	var template = "<div></div>";
	var dateTemplate = "<span uib-tooltip=\"{{formattedDate}}\" tooltip-append-to-body=\"true\" " +
		"tooltip-placement=\"top\" tooltip-popup-delay=\"500\" tooltip-trigger=\"mouseenter\">" + "{{calendarDate}}</span>";
	var emptyTemplate = "<div>{{::emptyText}}</div>";

	function getTemplate(scope) {
		return scope.date === null ? emptyTemplate : dateTemplate;
	}

	function link(scope, element) {
		scope.date = null;
		scope.$watch("model", function (newValue) {
			if (_.isString(newValue) && newValue !== null) {
				scope.date = new Date(newValue.toLocaleString());
			}
			else if (_.isDate(newValue) && newValue !== null) {
				scope.date = newValue;
			}

			scope.formattedDate = amDateFormatFilter(scope.date, "MMMM Do YYYY, h:mm a (Z)");
			scope.calendarDate = amCalendarFilter(scope.date, $rootScope.referenceTime, $rootScope.formats);

			element.replaceWith($compile(getTemplate(scope))(scope));
		});
	}

	var date = {
		link: link,
		scope: {
			model: "=",
			emptyText: "@"
		},
		restrict: "EA",
		replace: false,
		template: template
	};
	return date;

}

date.$inject = ["$compile", "amDateFormatFilter", "amCalendarFilter", "$rootScope"];
