
export default function focusIf($timeout: ng.ITimeoutService, $document: JQuery): any {
    return {
        link: (scope: any, element: any, attrs: {}): void => {
            if (scope.focusIf) {
                $timeout(() => {
                    if (element && element[0]) {
                        element.focus();
                    }
                });
            }
        },
        restrict: "A",
        scope: {
            focusIf: "<",
        },
    };
}

focusIf.$inject = ["$timeout", "$document"];
