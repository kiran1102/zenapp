export class SharedConfig {

    static $inject: string[] = [
        "$localStorageProvider",
        "$httpProvider",
        "$compileProvider",
        "$logProvider",
        "$locationProvider",
    ];

    constructor(
        $localStorageProvider: any,
        $httpProvider: any,
        $compileProvider: any,
        $logProvider: any,
        $locationProvider: any,
    ) {

        if (location.host.indexOf("localhost") === -1) {
            $compileProvider.debugInfoEnabled(false);
            $logProvider.debugEnabled(true);
        }

        $localStorageProvider.setKeyPrefix("OneTrust.");
        $httpProvider.useApplyAsync(true);
        $compileProvider.preAssignBindingsEnabled(true);
        $locationProvider.hashPrefix("");
    }
}
