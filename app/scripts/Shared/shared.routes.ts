// rxjs
import { forkJoin, from } from "rxjs";

// util
import Utilities from "Utilities";

// Services
import { AuthHandlerService } from "modules/shared/services/helper/auth-handler.service";
import { Content } from "sharedModules/services/provider/content.service";
import { LoadingService } from "modules/core/services/loading.service";
import { Permissions } from "modules/shared/services/helper/permissions.service";
import { TenantTranslationService } from "sharedServices/tenant-translation.service";

// Constants
import { EmptyGuid } from "constants/empty-guid.constant";
import { Regex } from "constants/regex.constant";
import { inviteModules } from "modules/shared/constants/invite-modules.constant";

// Interfaces
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";
import { IStringMap } from "interfaces/generic.interface";

export function routes(
    $stateProvider: ng.ui.IStateProvider,
    $urlRouterProvider: ng.ui.IUrlRouterProvider,
) {
    const ad_location = "/auth/login";

    $urlRouterProvider.when("/invite/pia/:module/:auth/:id?openPanel&filterByStatus", [
        "$state",
        "$location",
        "$stateParams",
        ($state: ng.ui.IStateService, $location: ng.ILocationService, $stateParams: ng.ui.IStateParamsService) => {
            const urlParts = $location.path().split("/");
            const module: any = inviteModules[$stateParams.module] || null;
            $state.go("zen.app.invite", {
                module: urlParts[3],
                auth: urlParts[4],
                id: urlParts[5],
                version: module && module.VersionParam ? module.VersionParam : null,
                ...$location.search(),
            });
            return true;
        }]);

    $urlRouterProvider.when("/webform", "");

    $stateProvider
        .state("zen", { abstract: true })
        .state("zen.app", {
            abstract: true,
            url: "",
            controller: "AppController",
        })

        ///////////////
        //   AUTH    //
        ///////////////
        .state("zen.app.auth", {
            abstract: true,
            url: "",
            template: `<ui-view></ui-view>`,
            resolve: {
                BasicTranslations: [
                    "TenantTranslationService",
                    (
                        tenantTranslationService: TenantTranslationService,
                    ): Promise<any> => {
                        const lang = JSON.parse(localStorage.getItem("OneTrust.languageSelected")) || "en-us";
                        tenantTranslationService.setTenantLanguageToRootScope(lang);
                        return forkJoin(
                            from(tenantTranslationService.addLanguagesToRootScope()),
                            from(tenantTranslationService.addSystemTranslations(EmptyGuid, lang)),
                        ).toPromise().then(([languageRes, transRes]) => {
                            return transRes;
                        });
                    },
                ],
                AuthResolves: [
                    "AuthHandlerService",
                    "Authorization",
                    "Permissions",
                    "BasicTranslations",
                    (
                        authHandlerService: AuthHandlerService,
                        Authorization: any,
                        permissions: Permissions,
                        BasicTranslations: IProtocolResponse<any>,
                    ): boolean => {
                        authHandlerService.closeAll();
                        permissions.logout();
                        return Authorization.authorize();
                    },
                ],
            },
        })
        .state("zen.app.auth.login", {
            url: "/login",
            params: { error: null },
            resolve: {
                redirect: [
                    "$rootScope",
                    "$stateParams",
                    "$q",
                    "local",
                    "Session",
                    "AuthResolves",
                    (
                        $rootScope: IExtendedRootScopeService,
                        $stateParams: ng.ui.IStateParamsService,
                        $q: ng.IQService,
                        local: boolean,
                        Session: any,
                        AuthResolves: boolean,
                    ): ng.IPromise<any> => {
                        const deferred: ng.IDeferred<any> = $q.defer();
                        if ($rootScope.SaveReturnUrl) {
                            $rootScope.origin = (window.location.href && window.location.href.indexOf("pia") > -1) ? window.location.href : "";
                        } else {
                            $rootScope.origin = "";
                            $rootScope.SaveReturnUrl = true;
                        }
                        const returnUrl: string = $rootScope.origin || "";
                        Session.setReturnUrl(returnUrl);
                        if (!local) {
                            if ($stateParams.error) Session.setAuthError($stateParams.error);
                            window.location.replace(ad_location);
                        } else {
                            deferred.resolve();
                        }
                        return deferred.promise;
                    },
                ],
            },
            controller: "LoginController",
            template: require("views/Identity/Auth/login.jade"),
            onEnter: ["LoadingService", (loadingService: LoadingService): void => {
                loadingService.set(false);
            }],
        })
        .state("zen.app.auth.forgotPassword", {
            url: "/forgot-password",
            controller: "ForgotPasswordController",
            template: require("views/Identity/Auth/forgot_password.jade"),
            resolve: {
                ForgotPasswordResolves: ["AuthResolves", (AuthResolves: boolean): boolean => {
                    return AuthResolves;
                }],
            },
            onEnter: ["LoadingService", (loadingService: LoadingService): void => {
                loadingService.set(false);
            }],
        })
        .state("zen.app.auth.confirm", {
            url: "/confirm/:auth_code",
            controller: "ConfirmAccountController",
            template: require("views/Identity/Auth/confirm-account.jade"),
            resolve: {
                BasicAuth: [
                    "AuthResolves",
                    "$stateParams",
                    "$q",
                    "Permissions",
                    "PRINCIPAL",
                    "Accounts",
                    "Session",
                    "local",
                    "BasicTranslations",
                    (
                        AuthResolves: boolean,
                        $stateParams: ng.ui.IStateParamsService,
                        $q: ng.IQService,
                        permissions: Permissions,
                        PRINCIPAL: any,
                        Accounts: any,
                        Session: any,
                        local: boolean,
                        BasicTranslations: IProtocolResponse<any>,
                    ): boolean | IStringMap<any> | ng.IPromise<any> => {
                        const deferred: ng.IDeferred<any> = $q.defer();
                        const auth_code: string = $stateParams.auth_code || "";
                        const identity: IStringMap<any> = PRINCIPAL.getIdentity();
                        if (auth_code) {
                            Accounts.basicRefresh(auth_code, true).then((response: IProtocolResponse<any>): any => {
                                location.href = Utilities.removeURLParameter(location.href, "auth_code");
                                if (response.result) {
                                    deferred.resolve(response.data);
                                } else {
                                    const defaultErrorText = "Your link has expired. Please contact your system administrator.";
                                    const errorMessage: string = BasicTranslations.data.LoginLinkExpiredContactAdministrator || defaultErrorText;
                                    Session.setAuthError(errorMessage);
                                    if (!local) {
                                        window.location.replace(ad_location);
                                    } else {
                                        permissions.goToFallback("zen.app.auth.login");
                                    }
                                }
                            });
                        } else if (identity) {
                            deferred.resolve(identity);
                        } else {
                            if (!local) {
                                window.location.replace(ad_location);
                            } else {
                                permissions.goToFallback("zen.app.auth.login");
                            }
                        }
                        return deferred.promise;
                    },
                ],
            },
            onEnter: ["LoadingService", (loadingService: LoadingService): void => {
                loadingService.set(false);
            }],
        })
        .state("zen.app.auth.createPassword", {
            url: "/createpassword/:auth_code",
            controller: "CreatePasswordController",
            template: require("views/Identity/Auth/create_password.jade"),
            resolve: {
                BasicAuth: [
                    "AuthResolves",
                    "$stateParams",
                    "$q",
                    "Permissions",
                    "PRINCIPAL",
                    "Accounts",
                    "Session",
                    "local",
                    "BasicTranslations",
                    (
                        AuthResolves: boolean,
                        $stateParams: ng.ui.IStateParamsService,
                        $q: ng.IQService,
                        permissions: Permissions,
                        PRINCIPAL: any,
                        Accounts: any,
                        Session: any,
                        local: boolean,
                        BasicTranslations: IProtocolResponse<any>,
                    ): boolean | IStringMap<any> | ng.IPromise<any> => {
                        const deferred: ng.IDeferred<any> = $q.defer();
                        const auth_code: string = $stateParams.auth_code || "";
                        const identity: IStringMap<any> = PRINCIPAL.getIdentity();
                        if (auth_code) {
                            Accounts.basicRefresh(auth_code, true).then((response: IProtocolResponse<any>): any => {
                                location.href = Utilities.removeURLParameter(location.href, "auth_code");
                                if (response.result) {
                                    deferred.resolve(response.data);
                                } else {
                                    const defaultErrorText = "Your link has expired. Please contact your system administrator.";
                                    const errorMessage: string = BasicTranslations.data.LoginLinkExpiredContactAdministrator || defaultErrorText;
                                    Session.setAuthError(errorMessage);
                                    if (!local) {
                                        window.location.replace(ad_location);
                                    } else {
                                        permissions.goToFallback("zen.app.auth.login");
                                    }
                                }
                            });
                        } else if (identity) {
                            deferred.resolve(identity);
                        } else {
                            if (!local) {
                                window.location.replace(ad_location);
                            } else {
                                permissions.goToFallback("zen.app.auth.login");
                            }
                        }
                        return deferred.promise;
                    },
                ],
            },
            onEnter: ["LoadingService", (loadingService: LoadingService): void => {
                loadingService.set(false);
            }],
        })
        .state("zen.app.auth.changePassword", {
            url: "/changepassword/:auth_code",
            controller: "ChangePasswordController",
            template: require("views/Identity/Auth/change_password.jade"),
            resolve: {
                BasicAuth: [
                    "AuthResolves",
                    "$stateParams",
                    "$q",
                    "Permissions",
                    "PRINCIPAL",
                    "Accounts",
                    "Session",
                    "local",
                    "BasicTranslations",
                    (
                        AuthResolves: boolean,
                        $stateParams: ng.ui.IStateParamsService,
                        $q: ng.IQService,
                        permissions: Permissions,
                        PRINCIPAL: any,
                        Accounts: any,
                        Session: any,
                        local: boolean,
                        BasicTranslations: IProtocolResponse<any>,
                    ): boolean | IStringMap<any> | ng.IPromise<any> => {
                        const deferred: ng.IDeferred<any> = $q.defer();
                        const auth_code: string = $stateParams.auth_code || "";
                        const identity: IStringMap<any> = PRINCIPAL.getIdentity();
                        if (auth_code) {
                            Accounts.basicRefresh(auth_code, true).then((response: IProtocolResponse<any>): any => {
                                location.href = Utilities.removeURLParameter(location.href, "auth_code");
                                if (response.result) {
                                    deferred.resolve(response.data);
                                } else {
                                    const defaultErrorText = "Your link has expired. Please contact your system administrator.";
                                    const errorMessage: string = BasicTranslations.data.LoginLinkExpiredContactAdministrator || defaultErrorText;
                                    Session.setAuthError(errorMessage);
                                    if (!local) {
                                        window.location.replace(ad_location);
                                    } else {
                                        permissions.goToFallback("zen.app.auth.login");
                                    }
                                }
                            });
                        } else if (identity) {
                            deferred.resolve(identity);
                        } else {
                            if (!local) {
                                window.location.replace(ad_location);
                            } else {
                                permissions.goToFallback("zen.app.auth.login");
                            }
                        }
                        return deferred.promise;
                    },
                ],
            },
            onEnter: ["LoadingService", (loadingService: LoadingService): void => {
                loadingService.set(false);
            }],
        })

        /////////////////////////
        // Shadow Invite View  //
        /////////////////////////
        .state("zen.app.invite", {
            url: "/invite/pia/:module/:auth/:id/:version?openPanel&filterByStatus",
            resolve: {
                invite: [
                    "$state",
                    "$stateParams",
                    "$timeout",
                    "$localStorage",
                    "local",
                    "Permissions",
                    "PRINCIPAL",
                    "Authorization",
                    "Accounts",
                    "Session",
                    "TenantTranslationService",
                    (
                        $state: ng.ui.IStateService,
                        $stateParams: ng.ui.IStateParamsService,
                        $timeout: ng.ITimeoutService,
                        $localStorage: any,
                        local: boolean,
                        permissions: Permissions,
                        PRINCIPAL: any,
                        Authorization: any,
                        Accounts: any,
                        Session: any,
                        tenantTranslationService: TenantTranslationService,
                    ): boolean => {
                        const module: any = inviteModules[$stateParams.module] || null;
                        const auth: any = $stateParams.auth;
                        const id: string = $stateParams.id;
                        const version: string = $stateParams.version;
                        if (module && auth) {
                            Accounts.basicRefresh(auth).then((response: IProtocolResponse<any>): void => {
                                if (response.result) {
                                    Authorization.authorize();
                                    const params: IStringMap<any> = {};
                                    if (module.IdParam) params[module.IdParam] = id ? id : null;
                                    if (module.VersionParam) params[module.VersionParam] = version ? version : null;
                                    if (module.searchParam) {
                                        params["openPanel"] = $state.params.openPanel;
                                        params["filterByStatus"] = $state.params.filterByStatus;
                                    }
                                    params.time = new Date();
                                    $timeout((): void => {
                                        if (id && module.IdState) {
                                            $state.go(module.IdState, params, { reload: true });
                                        } else {
                                            $state.go(module.State, {}, { reload: true });
                                        }
                                    });
                                } else {
                                    tenantTranslationService.addLanguagesToRootScope();
                                    tenantTranslationService.setTenantLanguageToRootScope($localStorage.languageSelected || "en-us");
                                    const tenantId = PRINCIPAL.getTenant() || EmptyGuid;
                                    tenantTranslationService.addSystemTranslations(tenantId, tenantTranslationService.getCurrentLanguage()).then((translationResponse: IProtocolResponse<any>) => {
                                        const defaultErrorText = "Your link has expired. Please contact your system administrator.";
                                        const errorMessage = translationResponse.data.LoginTokenExpiredContactAdministrator || defaultErrorText;
                                        Session.setAuthError(errorMessage);
                                        if (!local) {
                                            window.location.replace(ad_location);
                                        } else {
                                            permissions.goToFallback("zen.app.auth.login");
                                        }
                                    });
                                }
                            },
                                (): void => {
                                    permissions.goToFallback();
                                });
                        } else {
                            permissions.goToFallback("zen.app.auth.login");
                        }
                        return true;
                    },
                ],
            },
        })

        ////////////////////////////////////
        // Shadow SAML Authorization View  //
        ////////////////////////////////////
        .state("zen.app.saml", {
            url: "/saml/:auth",
            template: "<loading show-text='false'></loading>",
            resolve: {
                saml: [
                    "$stateParams",
                    "Permissions",
                    "Authorization",
                    "Accounts",
                    "$localStorage",
                    "Session",
                    (
                        $stateParams: ng.ui.IStateParamsService,
                        permissions: Permissions,
                        Authorization: any,
                        Accounts: any,
                        $localStorage: any,
                        Session: any,
                    ): boolean => {
                        const auth: string = $stateParams.auth;
                        if (auth) {
                            Accounts.basicRefresh(auth).then((response: IProtocolResponse<any>): void => {
                                if (response.result) {
                                    Authorization.authorize();
                                    if ($localStorage.Session) {
                                        window.location.href = $localStorage.Session;
                                        Session.removeReturnUrl();
                                    } else {
                                        permissions.goToFallback("landing", null, { reload: true });
                                    }
                                } else {
                                    permissions.goToFallback("zen.app.auth.login");
                                }
                            },
                                (): void => {
                                    permissions.goToFallback("zen.app.auth.login");
                                });
                        } else {
                            permissions.goToFallback("zen.app.auth.login");
                        }
                        return true;
                    },
                ],
            },
        })

        ///////////////
        //   Webform //
        ///////////////
        .state("zen.app.webform", {
            url: "/webform/:webFormId",
            template: "<webform-live></webform-live>",
            resolve: {
                WebformPermission: [
                    "$stateParams",
                    "Permissions",
                    (
                        $stateParams: ng.ui.IStateParamsService,
                        permissions: Permissions,
                    ): boolean => {
                        const webFormId: string = $stateParams.webFormId;
                        if (!webFormId || !Utilities.matchRegex(webFormId, Regex.GUID)) {
                            permissions.goToFallback("landing",  null, { reload: true });
                            return false;
                        }
                        return true;
                    },
                ],
                BasicContent: [
                    "Content",
                    "WebformPermission",
                    (
                        content: Content,
                        WebformPermission: ng.IPromise<any>,
                    ): ng.IPromise<IStringMap<string>> => {
                        return content.GetContent().then((response: IStringMap<string>): IStringMap<string> => {
                            return response;
                        });
                    },
                ],
            },
            onEnter: ["LoadingService", (loadingService: LoadingService): void => {
                loadingService.set(false);
            }],
        })

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //   NG+ Routes   ////////////////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        .state("landing", {
            template: "",
            url: "/landing",
        })

        .state("module", {
            template: "",
            url: "/module/{path:.*}",
            params: { path: { value: null, squash: true } },
        })

        .state("404", {
            template: "",
            url: "/404/{path:.*}",
            params: { path: { value: null, squash: true } },
        })

        .state("error", {
            template: "",
            url: "/error/{path:.*}",
            params: { path: { value: null, squash: true } },
        })

    ;
}

routes.$inject = ["$stateProvider", "$urlRouterProvider"];
