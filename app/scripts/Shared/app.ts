import "images/assessment.svg";
import "images/certification.svg";
import "images/checkered-blend.png";
import "images/cookies.svg";
import "images/data-mapping.svg";
import "images/data_flow_graph.svg";
import "images/default-green.jpg";
import "images/favicon.ico";
import "images/favicon.png";
import "images/cookie_favicon.png";
import "images/favicon_touch.png";
import "images/logo.png";
import "images/logo.svg";
import "images/logo_green_transparent.svg";
import "images/logo_notext.svg";
import "images/logo_white.svg";
import "images/module-cc.svg";
import "images/module-cr.svg";
import "images/module-dm.svg";
import "images/module-ibm.svg";
import "images/module-iis.svg";
import "images/module-pia.svg";
import "images/module-ra.svg";
import "images/module-srs.svg";
import "images/module-vrm.svg";
import "images/pia.svg";
import "images/readiness.svg";
import "images/screenshot-banner.jpg";
import "images/screenshot-policy.jpg";
import "images/screenshot-preferencecentre.jpg";
import "images/screenshot-scan.jpg";
import "images/consent/bottom-dark-2btn.svg";
import "images/consent/bottom-dark.svg";
import "images/consent/bottom-light-2btn.svg";
import "images/consent/bottom-light.svg";
import "images/consent/preference-center.png";
import "images/consent/Thumbs.db";
import "images/consent/top-dark-2btn.svg";
import "images/consent/top-dark.svg";
import "images/consent/top-light-2btn.svg";
import "images/consent/top-light.svg";
import "images/consent/cookie-banner-center.svg";
import "images/consent/webpage-placeholder.svg";
import "images/consent/bottom-dark-2btn.svg";
import "images/consent/bottom-dark.svg";
import "images/consent/bottom-light-2btn.svg";
import "images/consent/bottom-light.svg";
import "images/consent/preference-center.png";
import "images/powered-by-ot.svg";
import "images/screenshot-preferences.png";
import "images/screenshot-ds-types.png";
import "images/screenshot-prefcentres.png";
import "images/task-notification.png";
import "images/empty_state_icon.svg";
import "images/thankyou-icon.svg";
import "images/vendor_generic.svg";
import "images/custom-report.svg";
import "images/custom-table.svg";
import "images/standard-report.svg";
import "images/standard-table.svg";
import "images/attribute.svg";
import "images/meta.svg";
import "images/notes.svg";
import "images/question.svg";
import "images/rich-text.svg";

import "angular-material/angular-material.css";
import "jquery-minicolors/jquery.minicolors.css";
import "angular-ui-tree/dist/angular-ui-tree.min.css";
import "angular-xeditable/dist/css/xeditable.css";
import "angularjs-toaster/toaster.css";
import "font-awesome/css/font-awesome.css";
import "ng-tags-input/build/ng-tags-input.css";
import "ng-tags-input/build/ng-tags-input.bootstrap.css";
import "ui-select/dist/select.css";
import "google-code-prettify/bin/prettify.min.css";
import "quill/dist/quill.snow.css";

import "../../styles/main.scss";

import "jquery";
import * as angular from "angular";

import "angularjs-captcha";
import "angular-animate";
import "angular-aria";
import "angular-ui-bootstrap";
import "angular-cookies";
import "angular-drag-and-drop-lists";
import "angular-file-upload";
import "angular-material";
import "angular-messages";
import "angular-minicolors";
import "angular-resource";
import "angular-sanitize";
import "angularjs-scroll-glue";
import "angular-touch";
import "angular-ui-tree";
import "angular-xeditable";
import "angularjs-toaster";
import "diagram-js";
import "d3";
import "file-saver";
import "i18next";
import "moment-timezone";
import "ngstorage";
import "angular-tek-progress-bar";
import "ng-tags-input";
import "tiny-svg";
import "angular-ui-tinymce";
import "angular-filter";
import "angular-moment";
import "natural-sort";
import "inherits";
import "ui-select";
import "quill";
import "angular-recaptcha";
import "topojson";
import "textures";
import "iso-currency";

/*App angular modules*/
import "../Identity/identity";
import { questionnaireModule } from "../Questionnaire/questionnaire";
import "../PIA/pia";
import "../Template/template.module";
import "../PIA/template.module";

/*Shared*/
import { SharedConfig } from "./shared.config";
import { routes } from "./shared.routes";
import { Run } from "./run";

/*Shared-> assets*/
import "./assets/constants";
import "./assets/envConfig";
import enums from "./assets/enums";

/*Shared-> component*/
import OneConfigurableItemComponent from "../../Components/one-configurable-item/one-configurable-item.component";
import OnePopupCardComponent from "../../Components/one-popup/one-popup-card.component";
import OnePopupListComponent from "../../Components/one-popup/one-popup-list.component";
import OnePopupToggleComponent from "../../Components/one-popup/one-popup-toggle.component";
import OnePopupTableComponent from "../../Components/one-popup/one-popup-table.component";
import OnePopupComponent from "../../Components/one-popup/one-popup.component";
import OnePopupSearch from "../../Components/one-popup/one-popup-search.component";
import OneTableCellComponent from "../../Components/one-table/one-table-cell.component";
import OneTableHeaderCellComponent from "../../Components/one-table/one-table-header-cell.component";
import OneTableRowGroupComponent from "../../Components/one-table/one-table-row-group.component";
import OneTableRowComponent from "../../Components/one-table/one-table-row.component";
import OneTableComponent from "../../Components/one-table/one-table.component";
import OneTitledSectionHeaderComponent from "../../Components/one-titled-section/one-titled-section-header.component";
import OneSectionListComponent from "../../Components/one-titled-section/one-titled-section-list.component";
import OneTitledSectionComponent from "../../Components/one-titled-section/one-titled-section.component";
import OneCheckboxComponent from "../../Components/one-checkbox/one-checkbox.component";
import { DropdownComponent } from "../../Components/one-dropdown/one-dropdown.component";
import OneTabsNav from "../../Components/one-tabs-nav/one-tabs-nav.component";
import ReactiveDropdownComponent from "../../Components/reactive-dropdown/reactive-dropdown.component";
import ReactiveDropdownMenuComponent from "../../Components/reactive-dropdown-menu/reactive-dropdown-menu.component";
import oneDonutGraph from "../../Components/one-donut-graph/one-donut-graph.component";
import OneHeaderComponent from "../../Components/one-header/one-header.component";
import oneNavbar from "../../Components/one-navbar/one-navbar.component";
import oneRespondentAssign from "../../Components/one-respondent-assign/one-respondent-assign.component";
import { orgUserDropdown } from "generalcomponent/org-user-dropdown/org-user-dropdown.component";
import { OrgUserDropdownService } from "generalcomponent/org-user-dropdown/org-user-dropdown.service";
import oneSelect from "../../Components/one-select/one-select.component";
import pillList from "../../Components/pill-list/pill-list.component";
import sectionNavigatorToggle from "../../Components/section-navigator-toggle/section-navigator-toggle.component";
import statCard from "../../Components/stat-card/stat-card.component";
import typeahead from "../../Components/typeahead/typeahead.component";
import changeindicator from "../../Components/change-indicator/change-indicator.component";
import checkmark from "../../Components/check-mark/check-mark.component";
import ccToggleSwitch from "../../Components/Cookies/cc-toggle-switch.component";
import OneDatepickerComponent from "../../Components/one-datepicker/one-datepicker.component";
import OneDateComponent from "generalcomponent/one-date/one-date.component";
import OneExpandablePanel from "generalcomponent/one-expandable-panel/one-expandable-panel.component";

/*Shared-> Controllers*/
import { UpgradeModalCtrl } from "./Controllers/Modals/UpgradeModalController";
import WelcomeCtrl from "./Controllers/Modals/VideoModalController";
import { AppController } from "./Controllers/app.controller";

/*Shared-> Directives*/
import Collection from "./Directives/Collection/CollectionDirective";
import Member from "./Directives/Collection/MemberDirective";
import CollectionPicker from "./Directives/CollectionPicker/CollectionPickerDirective";
import PickerItem from "./Directives/CollectionPicker/CollectionPickerItemDirective";
import CollectionPickerList from "./Directives/CollectionPicker/CollectionPickerListDirective";
import autoOpen from "./Directives/auto-open-datepicker";
import typeaheadAutoOpen from "./Directives/autoOpenTypeaheadDirective";
import date from "./Directives/dateDirective";
import expand from "./Directives/expandDirective";
import fileInput from "./Directives/file-input.directive";
import focusMe from "./Directives/focus.directive";
import focusIf from "./Directives/focusIfDirective";
import heatmap from "./Directives/heatMapDirective";
import horseshoeGraph from "./Directives/horseshoe-graph.component";
import InventoryTable from "./Directives/inventoryTableDirective";
import loading from "./Directives/loading-directive.component";
import oneClickOutside from "./Directives/oneClickOutsideDirective";
import uiSelectCloseOnTab from "./Directives/ui-select-close-on-tab.directive";
import uiSelectOpenOnFocus from "./Directives/ui-select-open-on-focus.directive";
import pickList from "./Directives/pickListDirective";
import projectVersionSelector from "./Directives/projectVersionSelectorDirective";
import questionChangeToggle from "./Directives/questionChangeToggleDirective";
import QuestionnaireFilterSelectorDirective from "./Directives/questionnaireFilterSelectorDirective";
import QuestionnaireSelectorDirective from "./Directives/questionnaireSelectorDirective";
import sectionNav from "./Directives/sectionNavDirective";
import pageslide from "./Directives/slideDirective";
import templateQuestions from "./Directives/templateQuestionsDirective";
import welcomeSection from "./Directives/welcomeSectionDirective";
import fileUpload from "./Directives/file-upload.directive";
import selectOnClick from "./Directives/select-on-click.directive";
import fileOnChange from "./Directives/file-on-change.directive";

/*Shared-> Factories*/
import DocumentFactory from "./Factories/document.factory";
import GenFactory from "./Factories/gen.factory";
import TemplateStudio from "./Factories/template-studio.factory";

/*Shared-> Filters*/
import {
    ResponseFilter,
    dateToISO,
    filterUsersForGroup,
    capitalize,
    uncapitalize,
    getByKey,
    unique,
    absolute,
} from "./Filters/Filters";
import fromNow from "./Filters/fromNowFilter";
import InventoryOptionsFilter from "./Filters/InventoryOptionsFilter";
import ItemsNotIn from "./Filters/ItemsNotInFilter";
import OneSortFilter from "./Filters/one-sort.filter";
import passThrough from "./Filters/PassThroughFilter";
import smartTable from "./Filters/SmartTableFilter";
import tableItemsComparator from "./Filters/TableComparatorFilter";
import IsInParentTree from "./Filters/TreeParentFilter";
import TypeaheadInventoryFilter from "./Filters/TypeaheadInventoryFilter";

/*Shared-> Model*/
import ConditionFactory from "./Models/Conditions/Condition.js";
import TermFactory from "./Models/Conditions/ConditionTerm.js";
import Risk from "./Models/Risk.js";
import FilterFactory from "./Models/FilterFactory";
import RiskStateFilterModel from "./Models/RiskStateFilter";

/*Shared-> Services*/
import AnswerTab from "./Services/answer-tab.service";
import AppInsightService from "./Services/AppInsightService";
import CustomTermService from "./Services/CustomTermService";
import HeatmapSettingsService from "./Services/heatmap-settings.service";
import "./Services/http-auth-interceptor"; // Module defined
import { registerInterceptor, serverInterceptor } from "./Services/HttpService";
import Labels from "./Services/LabelsService";
import MarketingService from "./Services/marketing.service";
import { ModalService } from "./Services/modal.service";
import NotifyService from "./Services/NotifyService";
import { OneRisk } from "./Services/one-risk.service";
import OneTable from "./Services/one-table.service";
import { Accounts } from "modules/shared/services/api/accounts.service";
import QuestionTypes from "./Services/question-type.service";
import ReadinessAssessment from "./Services/ReadinessAssessmentService";
import { AttachmentService } from "oneServices/attachment.service";
import { AttachmentActionService } from "../services/actions/attachment-action.service";
import RetryPollingService from "./Services/retry-polling.service";
import SelfService from "./Services/self-service.service";
import { Settings } from "./Services/settings.service";
import TagService from "./Services/tag.service";
import { TasksService } from "./Services/tasks.service";
import { TaskPollingService } from "./Services/task-polling.service";
import { TenantTranslationService } from "./Services/tenant-translation.service";
import Translations from "./Services/TranslateService";
import "./Services/TreeMap"; // Global Objects
import Upgrade from "./Services/UpgradeService";
import "./Services/Utilities"; // Global Objects
import DefaultQuestion from "./Services/default-question.service";
import DmDashboardData from "./Services/dm-dashboard-data.service";
import OneBreadcrumbService from "./Services/one-breadcrumb.service";
import "./Services/http-auth-interceptor.js";
import { UpgradeService } from "./Services/upgrade.service";
import { Branding } from "sharedModules/services/provider/branding.service";
import { UserActionService } from "oneServices/actions/user-action.service";
import { AssessmentBulkLaunchActionService } from "sharedModules/services/actions/bulk-launch-assessment-action.service";

import OneWelcomeTextService from "./Services/one-welcome-text.service";
import AssessmentService from "../services/assessment.service";
import OrgGroupStoreNew from "../services/org-group.store";
import OrgGroupStoreOld from "../services/org-store.service";
import OrgApi from "../services/api/org-api.service";
import OrgAdapter from "../services/adapters/org-adapter.service";
import UserStore from "../services/user-store.service";
import { DrawerActionService } from "oneServices/actions/drawer-action.service";
import SettingStore from "../services/setting-store.service";
import CcConfigService from "./Services/cookie-config.service";
import WindowObservablesService from "./Services/window-observables.service";
import { PaginationService } from "oneServices/pagination.service";
import { TimeStamp } from "../services/time-stamp.service";

// import "./vendors/bootstrap/dropdown.js";

// Data mapping component

import dmAssessment from "datamappingcomponent/Assessment/dm-assessment.component.ts";
// import dmQuestion from "datamappingcomponent/Assessment/dm-question.component.ts";
import dmSectionExit from "datamappingcomponent/Assessment/dm-section-exit.component.ts";
import dmSectionIntro from "datamappingcomponent/Assessment/dm-section-intro.component.ts";

import actionIconsComponent from "generalcomponent/action-icons/action-icons.component.ts";
import assessmentDetailsBanner from "generalcomponent/Assessment/assessment-details-banner/assessment-details-banner.component.ts";
import assessmentFooterComponent from "generalcomponent/Assessment/assessment-footer/assessment-footer.component.ts";
import assessmentHeaderComponent from "generalcomponent/Assessment/assessment-header/assessment-header.component.ts";
import dmAssessmentListComponent from "generalcomponent/Assessment/dm-assessment-list/dm-assessment-list.component.ts";
import dmAssessmentListActionsComponent from "generalcomponent/Assessment/dm-assessment-list-actions/dm-assessment-list-actions.component.ts";
import dmAssessmentTableComponent from "generalcomponent/Assessment/dm-assessment-table/dm-assessment-table.component.ts";
import dmAssessmentTableCellComponent from "generalcomponent/Assessment/dm-assessment-table-cell/dm-assessment-table-cell.component.ts";
import dmAssessmentsHeaderComponent from "generalcomponent/Assessment/dm-assessments-header/dm-assessments-header.component.ts";
import assessmentsAging from "generalcomponent/Dashboard/DataMapping/assessments-aging/assessments-aging.component.ts";
import assessmentsAgingGraph from "generalcomponent/Dashboard/DataMapping/assessments-aging-graph/assessments-aging-graph.component.ts";
import assessmentsStatusCounts from "generalcomponent/Dashboard/DataMapping/assessments-status-counts/assessments-status-counts.component.ts";
import assessmentsStatusDonut from "generalcomponent/Dashboard/DataMapping/assessments-status-donut/assessments-status-donut.component.ts";
import dmDashboard from "generalcomponent/Dashboard/DataMapping/dm-dashboard/dm-dashboard.component.ts";
import dmDashboardWrapper from "generalcomponent/Dashboard/DataMapping/dm-dashboard-wrapper/dm-dashboard-wrapper.component.ts";
import { reassignmentComponent } from "generalcomponent/Modals/assignment-modal/reassignment-modal.component.ts";
import conditionsComponent from "generalcomponent/Modals/conditions-modal/conditions-modal.component.ts";
import modalComponent from "generalcomponent/Modals/modal/modal.component.ts";
import questionComponent from "generalcomponent/Modals/question-modal/question-modal.component.ts";
import newItemFieldComponent from "generalcomponent/new-item-field/new-item-field.component.ts";
import oneButton from "generalcomponent/one-button/one-button.component.ts";
import oneSplitDropdown from "generalcomponent/one-split-dropdown/one-split-dropdown.component.ts";
import { OneNavHeader } from "generalcomponent/one-nav-header/one-nav-header.component.ts";
import oneNumberedCircle from "generalcomponent/one-numbered-circle/one-numbered-circle.component.ts";
import EditableComponent from "generalcomponent/one-editable/one-editable.component.ts";
import editableTextComponent from "generalcomponent/one-editable-text/one-editable-text.component.ts";
import editableTypesComponent from "generalcomponent/one-editable-types/one-editable-types.component.ts";
import { OneLabelContentComponent } from "generalcomponent/one-label-content/one-label-content.component.ts";
import OneLinkComponent from "generalcomponent/one-link/one-link.component.ts";
import paginationComponent from "generalcomponent/one-pagination/one-pagination.component.ts";
import { OnePopupFooterComponent } from "generalcomponent/one-popup-footer/one-popup-footer.component.ts";
import oneSearchComponent from "generalcomponent/one-search/one-search.component.ts";
import SearchableDropdownComponent from "generalcomponent/one-searchable-dropdown/one-searchable-dropdown.component.ts";
import oneTagComponent from "generalcomponent/one-tag/one-tag.component.ts";
import OneWelcomeText from "generalcomponent/one-welcome-text/one-welcome-text.ts";
import SideDrawerComponent from "generalcomponent/side-drawer/side-drawer.component.ts";
import piaProjectButtons from "generalcomponent/table-columns/pia-project/pia-project-buttons/pia-project-buttons.component.ts";
import piaProjectDeadline from "generalcomponent/table-columns/pia-project/pia-project-deadline/pia-project-deadline.component.ts";
import piaProjectName from "generalcomponent/table-columns/pia-project/pia-project-name/pia-project-name.component.ts";
import piaProjectRiskState from "generalcomponent/table-columns/pia-project/pia-project-risk-state/pia-project-risk-state.component.ts";
import piaProjectStateLabel from "generalcomponent/table-columns/pia-project/pia-project-state-label/pia-project-state-label.component.ts";
import piaProjectTable from "generalcomponent/tables/pia-project-table/pia-project-table.component.ts";
import dmTemplate from "generalcomponent/Template/dm-template/dm-template.component.ts";
import dmTemplateHeader from "generalcomponent/Template/dm-template-header/dm-template-header.component.ts";
import questionBank from "generalcomponent/Template/question-bank/question-bank.component.ts";
import templateItem from "generalcomponent/Template/template-item/template-item.component.ts";
import templateQuestionItem from "generalcomponent/Template/template-question-item/template-question-item.component.ts";
import templateQuestionList from "generalcomponent/Template/template-question-list/template-question-list.component.ts";
import templateSectionItem from "generalcomponent/Template/template-section-item/template-section-item.component.ts";
import templateSectionList from "generalcomponent/Template/template-section-list/template-section-list.component.ts";
import templateSidebar from "generalcomponent/Template/template-sidebar/template-sidebar.component.ts";
import templateVersionList from "generalcomponent/Template/template-version-list/template-version-list.component.ts";
import oneAccordionComponent from "generalcomponent/one-accordion/one-accordion.component.ts";
import oneBackgroundMessage from "generalcomponent/one-background-message/one-background-message.component.ts";
import sectionNavigatorButton from "generalcomponent/Assessment/section-navigator-button/section-navigator-button.component.ts";
import sectionNavigator from "generalcomponent/Assessment/section-navigator/section-navigator.component.ts";
import CommaSeparatedListComponent from "generalcomponent/one-comma-separated-list/one-comma-separated-list.component.ts";
import MultiselectComponent from "generalcomponent/one-multiselect/one-multiselect.component.ts";
import oneOrgDropdownComponent from "generalcomponent/one-org-dropdown/one-org-dropdown.component.ts";
import oneOrgHeaderButtonComponent from "generalcomponent/one-org-header-button/one-org-header-button.component.ts";
import oneOrgInputComponent from "generalcomponent/one-org-input/one-org-input.component.ts";
import OneReviewNotesComponent from "generalcomponent/one-review-notes/one-review-notes.component.ts";
import pwValidationAlertComponent from "generalcomponent/pw-validation-alert/pw-validation-alert.component.ts";
import questionAssetComponent from "generalcomponent/Questions/asset/question-asset.component.ts";
import oneQuestionComponent from "generalcomponent/Questions/one-question.component.ts";
import questionListComponent from "generalcomponent/Questions/question-list/question-list.component.ts";
import questionSetComponent from "generalcomponent/Questions/question-set/question-set.component.ts";
import questionTypeaheadComponent from "generalcomponent/Questions/typeahead/question-typeahead.component.ts";
import questionTypeaheadMultiselect from "generalcomponent/Questions/typeahead/question-typeahead-multiselect.component.ts";
import { AssessmentQuestionMultiselectDropdown } from "generalcomponent/Questions/typeahead/assessment-question-multiselect-dropdown.ts";
import QuestionTextComponent from "generalcomponent/Questions/question-text.component.ts";
import questionWelcome from "generalcomponent/Questions/welcome/question-welcome.component.ts";
import questionAppInfo from "generalcomponent/Questions/app-info/question-app-info.component.ts";
import questionContent from "generalcomponent/Questions/question-content.component.ts";
import questionMultichoiceComponent from "generalcomponent/Questions/buttons/question-multichoice.component.ts";
import questionSingleSelectButtonsComponent from "generalcomponent/Questions/buttons/question-single-select-buttons.component.ts";
import OneRichTextEditorComponent from "generalcomponent/one-rich-text-editor/one-rich-text-editor.component.ts";
import OneDraggableCard from "generalcomponent/one-draggable-card/one-draggable-card.component.ts";
import OneDraggableElement from "generalcomponent/one-draggable-card/one-draggable-element.component.ts";
import OneDropList from "generalcomponent/one-draggable-card/one-drop-list.component.ts";
import OneDropTarget from "generalcomponent/one-draggable-card/one-drop-target.component.ts";
import prettify from "generalcomponent/prettyprint/prettyprint.component.ts";
import oneTextarea from "generalcomponent/one-textarea/one-textarea.component.ts";
import inventoryComponent from "generalcomponent/inventory-card/inventory-card.component.ts";
import riskSummaryComponent from "generalcomponent/risk-summary/risk-summary.component.ts";
import riskSummaryContentComponent from "generalcomponent/risk-summary/risk-summary-content.component.ts";
import riskModalComponent from "generalcomponent/Modals/risk-modal/risk-modal.component.ts";
import oneRiskActionButtons from "generalcomponent/one-risk-action-buttons/one-risk-action-buttons.component.ts";
import oneRiskLevelDropdown from "generalcomponent/one-risk-level-dropdown/one-risk-level-dropdown.component.ts";
import oneRiskTileButton from "generalcomponent/one-risk-tile-button/one-risk-tile-button.component.ts";
import oneRiskTileDropdown from "generalcomponent/one-risk-tile-dropdown/one-risk-tile-dropdown.component.ts";
import ExpandableTileComponent from "generalcomponent/expandable-tile/expandable-tile.component.ts";
import dropdownChecklist from "generalcomponent/dropdown-checklist/dropdown-checklist.component.ts";
import createProjectFormComponent from "generalcomponent/create-project-form/create-project-form.component.ts";
import OneSidebarMenu from "generalcomponent/one-sidebar-menu/one-sidebar-menu.component.ts";
import OneSidebarMenuGroup from "generalcomponent/one-sidebar-menu/one-sidebar-menu-group.component.ts";
import OneSidebarMenuItem from "generalcomponent/one-sidebar-menu/one-sidebar-menu-item.component.ts";
import OneTreePicklist from "generalcomponent/one-tree-picklist/one-tree-picklist.component.ts";
import { deleteConfirmationModal } from "generalcomponent/Modals/delete-confirmation-modal/delete-confirmation-modal.component.ts";
import { confirmationModal } from "generalcomponent/Modals/confirmation-modal/confirmation-modal.component.ts";
import { saveChangesModal } from "generalcomponent/Modals/save-changes-modal/save-changes-modal.component.ts";
import { attachmentModal } from "generalcomponent/Modals/attachment-modal/attachment-modal.component.ts";
import { editAttachmentModal } from "generalcomponent/Modals/attachment-modal/edit-attachment-modal.component.ts";
import notifyUserModal from "generalcomponent/Modals/notify-user-modal/notify-user-modal.component.ts";
import { ReduxStoreFactory } from "../../redux/redux-store.factory";

import listTable from "generalcomponent/tables/list-table/list-table.component.ts";
import listTableHeaderCell from "generalcomponent/table-columns/list-table-cell-types/list-table-header-cell/list-table-header-cell.component.ts";
import listTableRowCell from "generalcomponent/table-columns/list-table-cell-types/list-table-row-cell/list-table-row-cell.component.ts";
import ngTranscludeReplace from "./Directives/ng-transclude-replace";
import { OneInput } from "generalcomponent/one-input/one-input.component.ts";

import OneSearchInput from "generalcomponent/one-search-input/one-search-input.component.ts";
import expandingCell from "generalcomponent/expanding-cell/expanding-cell.component.ts";
import overflowTooltip from "generalcomponent/overflow-tooltip/overflow-tooltip.component.ts";
import scrollableCell from "generalcomponent/scrollable-cell/scrollable-cell.component.ts";

import RiskSingleEditModal from "generalcomponent/Modals/risk-single-edit-modal/risk-single-edit-modal.component.ts";
import { flagRiskModal } from "generalcomponent/Modals/flag-risk-modal/flag-risk-modal.component.ts";
import { contactOnetrustModal } from "generalcomponent/Modals/contact-onetrust-modal/contact-onetrust-modal.component.ts";
import { installAPIKeyModal } from "generalcomponent/Modals/install-api-key-modal/install-api-key-modal.component.ts";

import { MetaService } from "modules/shared/services/provider/meta.service.ts";

// Correct way of importing AngularJS modules
import { settingsLegacyModule } from "modules/settings/settings-legacy.module";
import { adminModule } from "modules/admin/admin-legacy.module";
import { vendorLegacyModule } from "modules/vendor/vendor-legacy.module";
import { inventoryLegacyModule } from "../Inventory/inventory-legacy.module";
import { dsarModule } from "dsarModule/dsar-legacy.module";
import { assessmentLegacyModule } from "modules/assessment/assessment-legacy.module";
import { dashboardLegacyModule } from "modules/dashboard/dashboard-legacy.module";
import { intgLegacyModule } from "modules/intg/intg-legacy.module";
import { consentReceiptLegacyModule } from "modules/consent/consentreceipt-legacy.module";
import { risksLegacyModule } from "modules/risks/risks-legacy.module";
import { sspLegacyModule } from "modules/self-service-portal/ssp-legacy.module";
import { incidentLegacyModule } from "incidentModule/incident-legacy.module";
import { eulaLegacyModule } from "modules/eula/eula-legacy.module";
import { globalReadinessLegacyModule } from "modules/global-readiness/global-readiness-legacy.module";
import { reportsLegacyModule } from "modules/reports/reports-legacy.module";
import { controlsLegacyModule } from "modules/controls/controls-legacy.module";
import "../DataMapping/data-mapping-legacy.module";
import { cookiesLegacyModule } from "modules/cookies/cookies-legacy.module";
import { preferencesLegacyModule } from "modules/preferences/preferences-legacy.module";
import { policyLegacyModule } from "modules/policy/policy-legacy.module";
import { reportScheduleLegacyModule } from "modules/reports/report-schedule/report-schedule-legacy.module";

import uiRouter from "@uirouter/angularjs";
import { upgradeModule } from "@uirouter/angular-hybrid";

// npm run serve:dev6 | npm run serve:intg
if (process.env.NODE_ENV === "development") {
    angular.module("envConfig", [])
        .constant("API_URL", process.env.ONETRUST_DEV_ENV + "/api/v1/private")
        .constant("MOCK", false)
        .constant("APP_SSL", true)
        .constant("local", true)
        .constant("OPTANON_URL", "");
} else if (process.env.NODE_ENV === "production") {
    angular.module("envConfig", [])
        .constant("API_URL", "/api/v1/private")
        .constant("MOCK", false)
        .constant("APP_SSL", true)
        .constant("local", false)
        .constant("OPTANON_URL", "");
}

export const LegacyModule = angular.module("zen", [
        // Build Configuration
        "envConfig",
        "constants",

        // Angular modules
        "http-auth-interceptor",
        uiRouter,
        upgradeModule.name,
        "ui.bootstrap",
        "ui.tree",
        "ui.select",
        "ngAnimate",
        "ngMaterial",
        "ngSanitize",
        "ngStorage",
        "angular.filter",
        "ui.tinymce",

        // Custom modules
        "zen.identity",
        "zen.datamapping",
        "zen.cookies",
        "zen.intg",
        "zen.pia",
        "zen.template",
        "zen.templatesmodule",

        questionnaireModule.name,
        adminModule.name,
        vendorLegacyModule.name,
        intgLegacyModule.name,
        dashboardLegacyModule.name,
        eulaLegacyModule.name,
        globalReadinessLegacyModule.name,
        incidentLegacyModule.name,
        inventoryLegacyModule.name,
        settingsLegacyModule.name,
        assessmentLegacyModule.name,
        dsarModule.name,
        consentReceiptLegacyModule.name,
        sspLegacyModule.name,
        preferencesLegacyModule.name,
        policyLegacyModule.name,
        risksLegacyModule.name,
        reportsLegacyModule.name,
        controlsLegacyModule.name,
        cookiesLegacyModule.name,
        reportScheduleLegacyModule.name,

        // 3rd Party Modules
        "luegg.directives", // scroll-glue
        "dndLists", // Drag and Drop List
        "minicolors", // Color Picker
        "angularFileUpload", // Angular File
        "vcRecaptcha", // Angular Recaptcha for DSAR webform
    ]).config(SharedConfig)
    .config(routes)
    .run(Run)
    .factory("store", ReduxStoreFactory)
    .service("ENUMS", enums)
    .component("oneConfigurableItem", OneConfigurableItemComponent)
    .component("onePopupCard", OnePopupCardComponent)
    .component("onePopupList", OnePopupListComponent)
    .component("onePopupToggle", OnePopupToggleComponent)
    .component("onePopupTable", OnePopupTableComponent)
    .component("onePopup", OnePopupComponent)
    .component("onePopupSearch", OnePopupSearch)
    .component("oneTableCell", OneTableCellComponent)
    .component("oneTableHeaderCell", OneTableHeaderCellComponent)
    .component("oneTableRowGroup", OneTableRowGroupComponent)
    .component("oneTableRow", OneTableRowComponent)
    .component("oneTable", OneTableComponent)
    .component("oneTitledSectionHeader", OneTitledSectionHeaderComponent)
    .component("oneTitledSectionList", OneSectionListComponent)
    .component("oneTitledSection", OneTitledSectionComponent)
    .component("oneCheckbox", OneCheckboxComponent)
    .component("oneDropdown", DropdownComponent)
    .component("oneTabsNav", OneTabsNav)
    .component("oneExpandablePanel", OneExpandablePanel)
    .component("reactiveDropdown", ReactiveDropdownComponent)
    .component("reactiveDropdownMenu", ReactiveDropdownMenuComponent)
    .component("oneDatepicker", OneDatepickerComponent)
    .controller("UpgradeModalCtrl", UpgradeModalCtrl)
    .controller("VideoModalCtrl", WelcomeCtrl)
    .controller("AppController", AppController)
    .directive("collection", Collection)
    .directive("member", Member)
    .directive("collectionPicker", CollectionPicker)
    .directive("collectionPickerItem", PickerItem)
    .directive("collectionPickerList", CollectionPickerList)
    .directive("autoOpen", autoOpen)
    .directive("typeaheadAutoOpen", typeaheadAutoOpen)
    .component("changeindicator", changeindicator)
    .component("checkmark", checkmark)
    .directive("date", date)
    .directive("expand", expand)
    .directive("fileInput", fileInput)
    .directive("focusMe", focusMe)
    .directive("focusIf", focusIf)
    .directive("heatmap", heatmap)
    .component("horseshoeGraph", horseshoeGraph)
    .directive("inventoryTableOld", InventoryTable)
    .component("loading", loading)
    .directive("oneClickOutside", oneClickOutside)
    .directive("pickList", pickList)
    .directive("projectVersionSelector", projectVersionSelector)
    .directive("questionChangeToggle", questionChangeToggle)
    .directive("questionnaireFilterSelector", QuestionnaireFilterSelectorDirective)
    .directive("questionnaireSelector", QuestionnaireSelectorDirective)
    .directive("sectionNav", sectionNav)
    .directive("pageslide", pageslide)
    .directive("templateQuestions", templateQuestions)
    .directive("welcomeSection", welcomeSection)
    .factory("DocumentFactory", DocumentFactory)
    .factory("GenFactory", GenFactory)
    .factory("TemplateStudio", TemplateStudio)
    .filter("ResponseFilter", ResponseFilter)
    .filter("dateToISO", dateToISO)
    .filter("filterUsersForGroup", filterUsersForGroup)
    .filter("capitalize", capitalize)
    .filter("uncapitalize", uncapitalize)
    .filter("getByKey", getByKey)
    .filter("unique", unique)
    .filter("absolute", absolute)
    .filter("fromNow", fromNow)
    .filter("InventoryOptions", InventoryOptionsFilter)
    .filter("ItemsNotIn", ItemsNotIn)
    .filter("OneSort", OneSortFilter as any)
    .filter("PassThrough", passThrough)
    .filter("smartTable", smartTable)
    .filter("TableComparator", tableItemsComparator)
    .filter("IsInParentTree", IsInParentTree)
    .filter("TypeaheadInventoryFilter", TypeaheadInventoryFilter)
    .factory("Condition", ConditionFactory)
    .factory("Term", TermFactory)
    .factory("Risk", Risk)
    .service("AnswerTab", AnswerTab)
    .service("PaginationService", PaginationService)
    .factory("AppInsightService", AppInsightService)
    .service("Branding", Branding)
    .factory("CustomTermService", CustomTermService)
    .service("HeatmapSettings", HeatmapSettingsService)
    .config(registerInterceptor)
    .factory("serverInterceptor", serverInterceptor)
    .factory("Labels", Labels)
    .service("Marketing", MarketingService)
    .service("ModalService", ModalService)
    .service("AttachmentService", AttachmentService)
    .service("AttachmentAction", AttachmentActionService)
    .factory("NotifyService", NotifyService)
    .service("OneRisk", OneRisk)
    .service("OneTable", OneTable)
    .service("Accounts", Accounts)
    .service("QuestionTypes", QuestionTypes)
    .factory("ReadinessAssessment", ReadinessAssessment)
    .service("RetryPollingService", RetryPollingService)
    .service("SelfService", SelfService)
    .service("Settings", Settings)
    .service("Tags", TagService)
    .service("TASKS", TasksService)
    .service("TaskPollingService", TaskPollingService)
    .service("TenantTranslationService", TenantTranslationService)
    .factory("Translations", Translations)
    .factory("Upgrade", Upgrade)
    .service("FilterFactory", FilterFactory)
    .service("RiskStateFilter", RiskStateFilterModel)
    .component("oneDonutGraph", oneDonutGraph)
    .component("oneHeader", OneHeaderComponent)
    .component("oneNavbar", oneNavbar)
    .component("oneNavHeader", OneNavHeader)
    .component("oneRespondentAssign", oneRespondentAssign)
    .component("orgUserDropdown", orgUserDropdown)
    .service("OrgUserDropdownService", OrgUserDropdownService)
    .component("oneSelect", oneSelect)
    .component("pillList", pillList)
    .component("questionAppInfo", questionAppInfo)
    .component("questionContent", questionContent)
    .component("questionMultichoice", questionMultichoiceComponent)
    .component("questionSingleSelectButtons", questionSingleSelectButtonsComponent)
    .component("questionText", QuestionTextComponent)
    .component("questionTypeaheadMultiselect", questionTypeaheadMultiselect)
    .component("assessmentQuestionMultiselectDropdown", AssessmentQuestionMultiselectDropdown)
    .component("questionTypeahead", questionTypeaheadComponent)
    .component("questionWelcome", questionWelcome)
    .component("sectionNavigatorToggle", sectionNavigatorToggle)
    .component("statCard", statCard)
    .component("typeahead", typeahead)
    .directive("fileUpload", fileUpload)
    .directive("selectOnClick", selectOnClick)
    .service("DefaultQuestion", DefaultQuestion)
    .service("DmDashboardData", DmDashboardData)
    .service("UpgradeService", UpgradeService)
    .service("UserActionService", UserActionService)
    .service("AssessmentBulkLaunchActionService", AssessmentBulkLaunchActionService)

    .component("dmAssessment", dmAssessment)
    .component("dmSectionExit", dmSectionExit)
    .component("dmSectionIntro", dmSectionIntro)
    .component("sectionNavigatorButton", sectionNavigatorButton)

    .component("actionIcons", actionIconsComponent)
    .component("assessmentDetailsBanner", assessmentDetailsBanner)
    .component("assessmentFooter", assessmentFooterComponent)
    .component("assessmentHeader", assessmentHeaderComponent)
    .component("dmAssessmentList", dmAssessmentListComponent)
    .component("dmAssessmentListActions", dmAssessmentListActionsComponent)
    .component("dmAssessmentTable", dmAssessmentTableComponent)
    .component("dmAssessmentTableCell", dmAssessmentTableCellComponent)
    .component("dmAssessmentsHeader", dmAssessmentsHeaderComponent)
    .component("assessmentsAging", assessmentsAging)
    .component("assessmentsAgingGraph", assessmentsAgingGraph)
    .component("assessmentsStatusCounts", assessmentsStatusCounts)
    .component("assessmentsStatusDonut", assessmentsStatusDonut)
    .component("dmDashboard", dmDashboard)
    .component("dmDashboardWrapper", dmDashboardWrapper)
    .component("reassignmentModal", reassignmentComponent)
    .component("conditionsModal", conditionsComponent)
    .component("oneModal", modalComponent)
    .component("questionModal", questionComponent)
    .component("newItemField", newItemFieldComponent)
    .component("oneButton", oneButton)
    .component("oneSplitDropdown", oneSplitDropdown)
    .component("oneNumberedCircle", oneNumberedCircle)
    .component("oneEditable", EditableComponent)
    .component("oneEditableText", editableTextComponent)
    .component("oneEditableTypes", editableTypesComponent)
    .component("oneLabelContent", OneLabelContentComponent)
    .component("oneLink", OneLinkComponent)
    .component("onePagination", paginationComponent)
    .component("onePopupFooter", OnePopupFooterComponent)
    .component("oneSearch", oneSearchComponent)
    .component("oneSearchableDropdown", SearchableDropdownComponent)
    .component("oneTag", oneTagComponent)
    .component("oneWelcomeText", OneWelcomeText)
    .component("sideDrawer", SideDrawerComponent)
    .component("piaProjectButtons", piaProjectButtons)
    .component("piaProjectDeadline", piaProjectDeadline)
    .component("piaProjectName", piaProjectName)
    .component("piaProjectRiskState", piaProjectRiskState)
    .component("piaProjectStateLabel", piaProjectStateLabel)
    .component("piaProjectTable", piaProjectTable)
    .component("dmTemplate", dmTemplate)
    .component("dmTemplateHeader", dmTemplateHeader)
    .component("questionBank", questionBank)
    .component("templateItem", templateItem)
    .component("templateQuestionItem", templateQuestionItem)
    .component("templateQuestionList", templateQuestionList)
    .component("templateSectionItem", templateSectionItem)
    .component("templateSectionList", templateSectionList)
    .component("templateSidebar", templateSidebar)
    .component("templateVersionList", templateVersionList)
    .component("oneDraggableCard", OneDraggableCard)
    .component("oneDraggableElement", OneDraggableElement)
    .component("oneDropList", OneDropList)
    .component("oneDropTarget", OneDropTarget)
    .component("prettify", prettify)
    .component("inventoryCard", inventoryComponent)
    .component("createProjectForm", createProjectFormComponent)

    .service("Assessment", AssessmentService)
    .service("OrgGroupStore", OrgGroupStoreOld)
    .service("OrgGroupStoreNew", OrgGroupStoreNew)

    .service("OneWelcomeTextService", OneWelcomeTextService)
    .component("ccToggleSwitch", ccToggleSwitch)
    .component("oneAccordion", oneAccordionComponent)
    .component("oneBackgroundMessage", oneBackgroundMessage)
    .component("sectionNavigator", sectionNavigator)
    .component("oneCommaSeparatedList", CommaSeparatedListComponent)
    .component("oneMultiselect", MultiselectComponent)
    .directive("uiSelectCloseOnTab", uiSelectCloseOnTab)
    .directive("uiSelectOpenOnFocus", uiSelectOpenOnFocus)
    .component("oneOrgDropdown", oneOrgDropdownComponent)
    .component("oneOrgHeaderButton", oneOrgHeaderButtonComponent)
    .component("oneOrgInput", oneOrgInputComponent)
    .component("oneReviewNotes", OneReviewNotesComponent)
    .component("pwValidationAlert", pwValidationAlertComponent)
    .component("questionAsset", questionAssetComponent)
    .component("oneQuestion", oneQuestionComponent)
    .component("questionList", questionListComponent)
    .component("questionSet", questionSetComponent)

    .service("UserStore", UserStore)
    .service("TimeStamp", TimeStamp)
    .service("SettingStore", SettingStore)
    .directive("fileOnChange", fileOnChange)
    .service("CcConfigService", CcConfigService)
    .component("oneDate", OneDateComponent)
    .service("WindowObservables", WindowObservablesService)
    .component("oneRichTextEditor", OneRichTextEditorComponent)
    .component("riskSummary", riskSummaryComponent)
    .component("riskSummaryContent", riskSummaryContentComponent)
    .component("riskModal", riskModalComponent)
    .component("oneRiskActionButtons", oneRiskActionButtons)
    .component("oneRiskLevelDropdown", oneRiskLevelDropdown)
    .component("oneRiskTileButton", oneRiskTileButton)
    .component("oneRiskTileDropdown", oneRiskTileDropdown)
    .component("expandableTile", ExpandableTileComponent)
    .component("oneTextarea", oneTextarea)
    .service("OrgApi", OrgApi)
    .service("OrgAdapter", OrgAdapter)
    .service("DrawerAction", DrawerActionService)
    .component("deleteConfirmationModal", deleteConfirmationModal)
    .component("confirmationModal", confirmationModal)
    .component("saveChangesModal", saveChangesModal)
    .component("attachmentModal", attachmentModal)
    .component("editAttachmentModal", editAttachmentModal)
    .component("notifyUserModal", notifyUserModal)
    .service("WindowObservables", WindowObservablesService)
    .component("dropdownChecklist", dropdownChecklist)
    .component("listTable", listTable)
    .component("listTableHeaderCell", listTableHeaderCell)
    .component("listTableRowCell", listTableRowCell)
    .directive("ngTranscludeReplace", ngTranscludeReplace)
    .component("oneSidebarMenu", OneSidebarMenu)
    .component("oneSidebarMenuGroup", OneSidebarMenuGroup)
    .component("oneSidebarMenuItem", OneSidebarMenuItem)
    .component("oneInput", OneInput)
    .component("expandingCell", expandingCell)
    .component("oneSearchInput", OneSearchInput)
    .component("oneTreePicklist", OneTreePicklist)
    .component("overflowTooltip", overflowTooltip)
    .component("scrollableCell", scrollableCell)
    .component("riskSingleEditModal", RiskSingleEditModal)
    .service("OneBreadcrumbService", OneBreadcrumbService)
    .component("flagRiskModal", flagRiskModal)
    .component("contactOnetrustModal", contactOnetrustModal)
    .component("installAPIKeyModal", installAPIKeyModal)
    .service("MetaService", MetaService)
;
