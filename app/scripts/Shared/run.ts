declare const window: Window;
import {
    each,
    includes,
    isObject,
} from "lodash";
import {
    StateDeclaration,
    Transition,
    TransitionService,
} from "@uirouter/core";

// Services
import { AuthHandlerService } from "modules/shared/services/helper/auth-handler.service";
import { StorageProxy } from "modules/shared/services/helper/storage.service";

// Interfaces
import { IStringMap } from "interfaces/generic.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";

export interface ICustomCacheObject extends ng.ICacheObject {
    promise: IStringMap<any>;
    getData: (key: string, callback, value: any, expiration: number, cancel) => any;
    clearData: (key: string) => void;
    clearAll: () => void;
}

// TODO: Remove this once legacy usages have been removed.
const apiCacheCreator = ($cacheFactory: ng.ICacheFactoryService, $q: ng.IQService) => {

    const name = Math.random() * 10000;
    const cache = $cacheFactory(name.toString()) as ICustomCacheObject;
    cache.promise = {};

    // Override some functionality in the cache.get method.
    // Accept a number of parameters. Key is the actual AJAX call
    // and will later be used to create a unique promise name.
    // Check for an AJAX call in callback as well for an object sent
    // in the call 'value'. Optional expiration can be set to a specific ms.
    // And lastly 'cancel' will set an abort method
    // to stop the AJAX call mid response.
    cache.getData = (key: string, callback, value: any, expiration: number, cancel) => {

        // Set up some deferred to handle incoming calls and create an expiration
        // so that data is refreshed at an interval
        const d = $q.defer();
        const response: IProtocolResponse<any> = {
            result: true,
            data: null,
        };
        const dataExpiration = expiration || 300000; // To turn off caching use 0 ms
        const now = (new Date()).getTime();
        let cacheKey = "";

        // Check to see if the AJAX call has an object. If it does use a different
        // scheme to create the key. Else just create the key using the call in a string.
        if (typeof value === "object") {
            const keys = Object.keys(value);
            let objectValue = "";
            each(keys, (vKey, index) => {
                if (index > 1) {
                    return;
                }
                objectValue = objectValue + value[vKey];
            });
            cacheKey = objectValue ? key + "" + objectValue : key;
        } else {
            cacheKey = value ? key + "" + value : key;
        }

        $q.when(response.data = cache.get(cacheKey)).then(() => {
            d.resolve(response);
        });

        // Before acting look to see if the call has been made if not make the call. If it
        // has been made send a promise to be filled later, but first check to see if the expiration
        // has been reached before sending promise. Don't want to send stale data.
        if (response.data && (response.data.time + dataExpiration) > now) {
            if (typeof cache.promise[cacheKey] !== "undefined") {
                return cache.promise[cacheKey];
            } else {
                return d.promise;
            }
        } else if (callback) {
            cache.promise[cacheKey] = callback(value, cancel);
            $q.all(cache.promise[cacheKey]).then(() => {
                if (cache) {
                    cache.promise[cacheKey] = {};
                }
            });
            return cache.promise[cacheKey];
        }
    };

    // Just clear the data in the cache per key
    cache.clearData = (key) => {
        cache.remove(key);
    };

    cache.clearAll = () => {
        cache.removeAll();
    };

    // Return the object cache that we customized to the address created on rootscope. This
    // will allow all controllers and services to access the cache.
    return cache;
};

const pattern = /\.([0-9a-z]+)(?:[\?#]|$)/i;
let authCode = location.hash.match("confirm") || location.hash.match("createpassword");

export function Run(
    $rootScope,
    $animate,
    $cacheFactory,
    $localStorage,
    $q,
    $sce,
    $state,
    $timeout,
    $transitions: TransitionService,
    Accounts,
    API_URL,
    AppInsightService,
    Authorization,
    authService,
    CONSTANTS,
    router,
    NotifyService,
    Permissions,
    PRINCIPAL,
    Session,
    SettingAction,
    store,
    TenantTranslationService,
    Translations,
    authHandlerService: AuthHandlerService,
    // Instantiate But Do Not Use
    storageProxy: StorageProxy,
) {

    store.subscribe(() => {
        $timeout(() => { $rootScope.$apply(() => {}); }, 100);
    });

    $animate.enabled(document.body, false);
    $rootScope.SaveReturnUrl = true;
    $rootScope.terms = CONSTANTS.TERMS;
    $rootScope.i18n = Translations.getItemTranslation();
    $rootScope.compiledTranslations = Translations.compileTranslations();
    $rootScope.$state = $state;
    (window as any).OneAlert = NotifyService.alert;
    (window as any).OneConfirm = NotifyService.confirm;
    SettingAction.setIfBrowserIsIE11();

    // If the landing route is an Invite url, then clear all previous session information.
    if (location.hash.toLowerCase().includes("/invite/pia/")) {
        Session.removeReturnUrl();
    }

    $rootScope.enableAnimation = (identifier) => {
        if (!identifier) return;
        const element = $(identifier);
        if (element) $animate.enabled(element, true);
    };

    $rootScope.t = (key: string, paramValues: any) => {
        if (!key) return;
        let message = $rootScope.translations[key];

        if (!message) {
            return key + " NOT FOUND";
        }

        if (!(paramValues instanceof Object)) {
            return message;
        }

        for (const param in paramValues) {
            if (paramValues.hasOwnProperty(param)) {
                const currentRegexString = "\{{1,2}" + param + "\}{1,2}";
                const tPattern = new RegExp(currentRegexString, "g");
                message = message.replace(tPattern, paramValues[param]);
            }
        }

        return $sce.valueOf(message);
    };

    $rootScope.changeTenantLanguage = (language) => {
        $localStorage.languageSelected = language;
        TenantTranslationService.setTenantLanguageToRootScope(language);
        TenantTranslationService.addSystemTranslations(PRINCIPAL.getTenant(), TenantTranslationService.getCurrentLanguage());
        $state.go($state.current, {}, { reload: true });
    };

    $rootScope.CheckUrlHasCode = () => {
        authCode = location.hash.match("confirm") || location.hash.match("createpassword");
    };

    function handlePreviousStateUrl(toState: StateDeclaration): boolean {
        const ignoreUrls = [
            "/404",
            "/landing",
            "/invite",
            "/webform",
            "/consent",
            "/saml",
            "/error",
        ];
        const ignoreStates = [
            "zen.app.auth",
        ];
        const url = Session.getReturnUrl();
        if (!url || shouldIgnoreSession(toState.url.toString(), ignoreUrls) || (shouldIgnoreSession(toState.name, ignoreStates))) return true;

        Session.removeReturnUrl();
        window.location = url;
        return false;
    }

    function syncNgRouter(transition: Transition) {
        const toState = transition.to();
        const toStateParams = transition.params();
        const options = transition.options();
        const stateService = transition.router.stateService;
        const transitionHref = stateService.href(toState, toStateParams, options);
        // NOTE: This is under the assumption that the stateService href method converts "%" to "~".
        const transitionHrefNormalized = transitionHref
            .substring(1)
            .replace("~", "%");
        const transitionUrl = decodeURIComponent(transitionHrefNormalized);
        if (router.url !== "/" && router.url !== transitionUrl) {
            router.navigateByUrl(transitionUrl);
        }
    }

    function shouldIgnoreSession(toState: string, urls: string[]): boolean {
        for (const u of urls) {
            if (toState.startsWith(u)) {
                return true;
            }
        }
        return false;
    }

    $rootScope.DoAuthInit = () => {

        $rootScope.CheckUrlHasCode();

        if (authCode && PRINCIPAL.isAuthenticated()) {
            PRINCIPAL.logOff();
            PRINCIPAL.identity();
        }
    };

    $rootScope.handleConfig = (config) => {
        config.headers.Expires = 86400;
        // disable IE ajax request caching by asserting arbitrary date.
        config.headers["If-Modified-Since"] = "Mon, 26 Jul 1997 05:00:00 GMT";
        // extra
        config.headers["Cache-Control"] = "no-cache";
        config.headers.Pragma = "no-cache";
        config.headers["Accept-Language"] = $rootScope.tenantLanguage;
        config.legacy = true;

        if (config.url.match("botdetect")) {
            return config;
        } else if (config.url === "/token") {
            config.url = API_URL + config.url;

            if (config.data.grant_type === "password") {
                config.ignoreAuthModule = true;
            }

            // Use x-www-form-urlencoded Content-Type
            config.headers["Content-Type"] = "application/x-www-form-urlencoded;charset=utf-8";

            // Override $http service's default transformRequest
            config.transformRequest = [ (data) => {
                /**
                 * The workhorse; converts an object to x-www-form-urlencoded serialization.
                 * @param {Object} obj
                 * @return {String}
                 */
                const param =  (obj: object): string => {
                    let query = "";
                    let value;
                    let fullSubName;
                    let subValue;
                    let innerObj;
                    let i;
                    for (const name in obj) {
                        if (obj.hasOwnProperty(name)) {
                            value = obj[name];
                            if (value instanceof Array) {
                                for (i = 0; i < value.length; ++i) {
                                    subValue = value[i];
                                    fullSubName = name + "[" + i + "]";
                                    innerObj = {};
                                    innerObj[fullSubName] = subValue;
                                    query += param(innerObj) + "&";
                                }
                            } else if (value instanceof Object) {
                                for (const subName in value) {
                                    if (obj.hasOwnProperty(name)) {
                                        subValue = value[subName];
                                        fullSubName = name + "[" + subName + "]";
                                        innerObj = {};
                                        innerObj[fullSubName] = subValue;
                                        query += param(innerObj) + "&";
                                    }
                                }
                            } else if (value !== undefined && value !== null) {
                                query += encodeURIComponent(name) + "=" + encodeURIComponent(value) + "&";
                            }
                        }
                    }
                    return query.length ? query.substr(0, query.length - 1) : query;
                };
                return isObject(data) && String(data) !== "[object File]" ? param(data) : data;
            }];

        } else if (!config.url.match(pattern)) {
            // config url is local url and doesn't have API_URL already.
            if (config.url.indexOf("https://") === -1 && config.url.indexOf(API_URL) === -1) {
                const modifiedAPIUrl = config.removeURI ? API_URL.slice(0, API_URL.indexOf("/api/")) : API_URL;
                // config.removeURI === true            ==> `https://env.onetrust.com${config.url}`
                // config.removeURI === false [default] ==> `https://env.onetrust.com/api/v1/private${config.url}`
                config.url = modifiedAPIUrl + config.url;
            }

            config.headers["Content-Type"] = config.multipartFormData ? undefined : "application/json";

            if (!config.tokenNotRequired) {
                const access_token = localStorage.getItem("access_token");
                if (access_token) {
                    config.headers.Authorization = "Bearer " + access_token;
                }
            }
        }

        return config;
    };

    // Create an app wide cache system
    $rootScope.APICache = apiCacheCreator($cacheFactory, $q);

    function checkIfEulaModalShouldTrigger(toState: StateDeclaration) {
        return ($rootScope.eulaModalOpen
            && (toState.name !== "module"
                && toState.params
                && toState.params.path
                && toState.params.path !== "welcome"
            )
            && !includes(toState.name, "zen.app.auth")
        );
    }

    // On before state transition authentication guard.
    $transitions.onBefore({}, (transition: Transition) => {
        const toState = transition.to();
        const toStateParams = transition.params();
        const options = transition.options();
        const stateService = transition.router.stateService;
        $rootScope.toState = toState;
        $rootScope.toStateParams = toStateParams;
        if (checkIfEulaModalShouldTrigger(toState)) {
            return stateService.target("module", { path: "welcome" });
        }
        return handlePreviousStateUrl(toState);
    // Set as High Priority on order to resolve first.
    }, { priority: 999 });

    $transitions.onSuccess({},  (transition: Transition) => {
        // On state changes, scroll to the top of the document.
        document.body.scrollTop = document.documentElement.scrollTop = 0;
        syncNgRouter(transition);
    }, { priority: 999 });

    $rootScope.$on("event:auth-loginRequired",  () => {
        authHandlerService.refresh();
    });

    $rootScope.$on("event:auth-forbidden",  () => {
        // Do Nothing. Drop everything.
        authService.loginCancelled();
    });

    $rootScope.contextSwitch = (callback) => {
        let cancel = false;
        const callbacks = {
            cancel: () => {
                cancel = true;
            },
            continueSwitch: callback,
        };
        $rootScope.$broadcast("event:context-switch", callbacks);
        $timeout(() => {
            if (!cancel) callback();
        }, 0);
    };

    // Kick Off Splash Screen
    Permissions.logout();
    PRINCIPAL.identity();
    $rootScope.DoAuthInit();
    Authorization.authorize();
    AppInsightService.init();
}

Run.$inject = [
    "$rootScope",
    "$animate",
    "$cacheFactory",
    "$localStorage",
    "$q",
    "$sce",
    "$state",
    "$timeout",
    "$transitions",
    "Accounts",
    "API_URL",
    "AppInsightService",
    "Authorization",
    "authService",
    "CONSTANTS",
    "HybridRouter",
    "NotifyService",
    "Permissions",
    "PRINCIPAL",
    "Session",
    "SettingAction",
    "store",
    "TenantTranslationService",
    "Translations",
    "AuthHandlerService",
    "StorageProxy",
];
