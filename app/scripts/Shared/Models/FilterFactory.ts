
interface IFilter {
    key: string;
    values: Array<(number | string)>;
}

type IFilters = IFilter[];

class Filter {

    private filters: IFilters;

    constructor() {
        this.filters = [];
    }

    // save one filter value, overwrites the previous one
    public set(key: string, value: any): void {
        let matchFound = false;

        for (const filter of this.filters) {
            if (filter.key === key) {
                if (Array.isArray(value)) {
                    filter.values = value;
                } else {
                    filter.values = [value];
                }
                matchFound = true;
            }
        }
        if (!matchFound) this.filters = [...this.filters, this.createNewFilter(key, value)];
    }

    // save multiple filter values, append to previous ones
    public push(key: string, value: any): void {
        let matchFound = false;

        for (const filter of this.filters) {
            if (filter.key === key) {
                if (Array.isArray(value)) {
                    filter.values.concat(value);
                } else {
                    filter.values.push(value);
                }
                matchFound = true;
                this.removeDuplicates(filter.values);
            }
        }
        if (!matchFound) this.filters = [...this.filters, this.createNewFilter(key, value)];
    }

    // remove all keys and values
    public clearAll(): void {
        this.filters = [];
    }

    // clear values for a key
    public clearKey(key: string): void {
        for (const filter of this.filters) {
            if (filter.key === key) {
                filter.values = [];
            }
        }
    }

    // remove specific value from multiple values that are saved
    public remove(key: string, value: any): IFilters {
        let filterToHandle: IFilter;

        for (const filter of this.filters) {
            if (filter.key === key) {
                filterToHandle = filter;
            }
        }

        if (filterToHandle) {
            const index: number = this.filters.indexOf(filterToHandle);
            filterToHandle.values = filterToHandle.values.filter((val: (number | string)): boolean => val !== value);

            return [
                ...this.filters.slice(0, index),
                filterToHandle,
                ...this.filters.slice(index + 1),
            ];
        }
        return [...this.filters];
    }

    // remove a specific key and its values
    public removeKey(key: string): void {
        this.filters = this.filters.filter((filter: IFilter): boolean => filter.key !== key);
    }

    public createNewFilter(key: string, value: any): IFilter {
        if (Array.isArray(value)) {
            return {
                key,
                values: value,
            };
        } else {
            return {
                key,
                values: [value],
            };
        }
    }

    private removeDuplicates(arr: Array<(number | string)>): Array<(number | string)> {
        const hashTable: any = {};

        for (const item of arr) {
            hashTable[item] = item;
        }

        return Object.keys(hashTable);
    }

    // returns a filtered collection
    // conditions:
    // if this.filters array is empty, all collection items will be returned
    // if ANY filter exists in this.filters array, it will always be used for filtering
    // if ANY filter in this.filters array has empty values array, nothing will be returned
    private applyFilter(collection: any): any[] {
        let result: any[] = [];

        result = collection.filter((project: any): boolean => {
            let allFiltersPassed = true;
            if (!this.filters.length) {
                return true;
            }
            for (const filter of this.filters) {
                let foundMatch = false;

                if (filter.values.length) {
                    for (const value of filter.values) {
                        if (project[filter.key] === value) {
                            foundMatch = true;
                        }
                    }
                }
                allFiltersPassed = allFiltersPassed && foundMatch;
            }
            return allFiltersPassed;
        });
        return result;
    }
}

export default class FilterFactory {
    public createFilter(): Filter {
        return new Filter();
    }
}
