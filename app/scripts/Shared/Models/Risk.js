import { AssessmentStates } from "constants/assessment.constants";
import { ProjectModes } from "enums/assessment.enum";

export default function Risk($q, ENUMS, RiskAPI, ProjectQuestions) {
    function Risk(question, project, section) {
        _.assign(this, question.Risk);

        this.Question = _.assign({}, question);
        this.Section = section.Name;
        this.RiskLabel = ENUMS.RiskLabels[this.Level];
        this.NeedsAttention = this.needsAttention();

        this._projectState = project.StateDesc;
        this._projectMode = project.Mode;

        // Normalize the risk based on state
        if (this.State === ENUMS.RiskState.Exception) {
            this.Mitigation = null;
        } else if (this.State === ENUMS.RiskState.Addressed) {
            this.RequestedException = null;
        }
    }

    function revertReviewMode() {
        if (this.State === ENUMS.RiskState.Accepted ||
            this.State === ENUMS.RiskState.Analysis) {
            if (this.Mitigation) {
                this.State = ENUMS.RiskState.Addressed;
            } else if (this.RequestedException) {
                this.State = ENUMS.RiskState.Exception;
            } else if (this.State === ENUMS.RiskState.Accepted) {
                this.State = ENUMS.RiskState.Identified;
            }
        }
    }

    function revertAssessmentMode() {
        this.State = this.Recommendation ?
            ENUMS.RiskState.Analysis : ENUMS.RiskState.Identified;
    }

    Risk.prototype.accept = function () {
        this.State = ENUMS.RiskState.Accepted;
        return RiskAPI.acceptRisk(this.Id);
    };

    Risk.prototype.unaccept = function () {
        this.revert(this._projectMode);
        return RiskAPI.unacceptRisk(this.Id);
    };

    Risk.prototype.reject = function () {
        if (this.State === ENUMS.RiskState.Analysis) {
            this.revert(this._projectMode);

            var comment;
            if (this.State === ENUMS.RiskState.Addressed) {
                comment = this.Mitigation;
            } else if (this.State === ENUMS.RiskState.Exception) {
                comment = this.RequestedException;
            }

            return this.update(comment);
        }

        this.State = ENUMS.RiskState.Analysis;

        return $q.resolve();
    };

    Risk.prototype.revert = function () {
        // Reviewer
        if (this._projectMode === ProjectModes.ReviewMode) {
            revertReviewMode.call(this);
            // Lead
        } else if (this._projectMode === ProjectModes.AssessmentMode) {
            revertAssessmentMode.call(this);
        }
    };

    Risk.prototype.isAccepted = function () {
        return !(this.Recommendation &&
            this.State !== ENUMS.RiskState.Accepted);
    };

    Risk.prototype.isResolved = function () {
        return !((this.Recommendation &&
            !this.Mitigation &&
            !this.RequestedException) ||
            this.State === ENUMS.RiskState.Analysis);
    };

    Risk.prototype.hasRecommendation = function () {
        return this.State === ENUMS.RiskState.Analysis;
    };

    Risk.prototype.needsMitigation = function () {
        return !this.Mitigation && !this.RequestedException;
    };

    Risk.prototype.addRecommendation = function () {
        this.State =
            this.Recommendation ?
                ENUMS.RiskState.Analysis :
                ENUMS.RiskState.Identified;

        var afterRecommendationAdded = (function () {
            this.Mitigation = null;
            this.RequestedException = null;
        }).bind(this);

        var promise = this.update(this.Recommendation).then(afterRecommendationAdded);

        return promise;
    };

    Risk.prototype.addMitigation = function () {
        this.State = ENUMS.RiskState.Addressed;

        var afterMitigationAdded = (function () {
            this.RequestedException = null;
        }).bind(this);

        var promise = this.update(this.Mitigation)
            .then(afterMitigationAdded);

        return promise;
    };

    Risk.prototype.addException = function () {
        this.State = ENUMS.RiskState.Exception;

        var afterExceptionAdded = (function () {
            this.Mitigation = null;
        }).bind(this);

        var promise = this.update(this.RequestedException).then(afterExceptionAdded);

        return promise;
    };

    Risk.prototype.update = function (comment) {
        var riskObj = {
            Id: this.Id,
            State: this.State,
            Comment: comment || null
        };

        return RiskAPI.updateRisk(riskObj);
    };

    Risk.prototype.updateDescription = function () {
        var newState = {
            QuestionId: this.QuestionId,
            RiskLevel: this.Level,
            Comment: this.Description
        };

        return ProjectQuestions.updateRisk(newState);
    };

    Risk.prototype.delete = function () {
        return RiskAPI.deleteRisk(this.QuestionId);
    };

    Risk.prototype.needsAttention = function () {
        if (this.State === ENUMS.RiskState.Analysis ||
            this.State === ENUMS.RiskState.Addressed ||
            this.State === ENUMS.RiskState.Exception) {
            return true;
        }

        if (this.State === ENUMS.RiskState.Identified &&
            this._projectState === AssessmentStates.RiskAssessment) {
            return true;
        }

        return false;
    };

    Risk.createAllFromProject = function (project) {
        var risks = [];

        _.each(project.Sections, function(section) {
            _.each(section.Questions, function(question) {
                risks.push(new Risk(question, project, section));
            });
        });

        return risks;
    };

    return Risk;
}

Risk.$inject = ["$q", "ENUMS", "RiskAPI", "ProjectQuestions"];
