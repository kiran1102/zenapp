import { IExtendedRootScopeService } from "interfaces/root-scope.interface";

interface IRiskStatusFilter {
    Filtered: boolean;
    value: string;
    label: any;
    lookupKey: number;
}

export default class RiskStateFilterModel {
    static $inject: string[] = ["$rootScope", "ENUMS"];

    private filters: IRiskStatusFilter[];
    private allStatesEnabled: boolean;

    constructor(
        public readonly $rootScope: IExtendedRootScopeService,
        private readonly ENUMS: any,
        recommendation?: string,
    ) {
        this.filters = this.buildFilters(recommendation);
        this.allStatesEnabled = true;
    }

    public getFilters(): IRiskStatusFilter[] {
        return this.filters;
    }

    public toggleAll(enabled: boolean): void {
        this.filters = this.filters.map((filter: IRiskStatusFilter): IRiskStatusFilter => {
            filter.Filtered = enabled;
            return filter;
        });
        this.allStatesEnabled = this.checkIfAllEnabled();
    }

    // toggle a specific risk state
    public toggleRiskState(state: number): boolean {
        let enabled: boolean;

        this.filters = this.filters.map((filter: IRiskStatusFilter): IRiskStatusFilter => {
            if (filter.lookupKey === state) {
                filter.Filtered = !filter.Filtered;
                enabled = filter.Filtered;
            }
            return filter;
        });
        this.allStatesEnabled = this.checkIfAllEnabled();

        return enabled;
    }

    public isAllEnabled(): boolean {
        return this.allStatesEnabled;
    }

    // returns all risk states
    public getEnabledStates(): number[] {
        const result: number[] = [];

        for (const filter of this.filters) {
            if (filter.Filtered) {
                result.push(filter.lookupKey);
            }
        }
        return result;
    }

    public checkIfAllEnabled(): boolean {
        let allEnabled = true;
        allEnabled = this.filters.reduce((acc: boolean, filter: IRiskStatusFilter): boolean => acc && filter.Filtered, allEnabled);
        return allEnabled;
    }

    private buildFilters(recommendation?: string): IRiskStatusFilter[] {
        const riskStatesMap = {
            10: this.$rootScope.t("Identified"),
            20: this.$rootScope.t("RecommendationAdded", { Recommendation: recommendation || this.$rootScope.customTerms.Recommendation }),
            30: this.$rootScope.t("RemediationProposed"),
            40: this.$rootScope.t("ExceptionRequested"),
            45: this.$rootScope.t("Reduced"),
            55: this.$rootScope.t("Retained"),
        };

        const filters = Object.keys(riskStatesMap).map((key: string): IRiskStatusFilter => ({
            Filtered: true,
            value: this.ENUMS.RiskStates[key],
            label: riskStatesMap[key],
            lookupKey: Number(key),
        }));

        return filters;
    }
}
