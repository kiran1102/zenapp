"use strict";
import { RiskLevels } from "enums/risk.enum";
import Utilities from "Utilities";

export default function ConditionFactory(ENUMS, ConditionGroups, Term) {

    var ConditionTypes = ENUMS.ConditionTypes;

    var RiskNames = ENUMS.RiskPrettyNames;
    var RiskLabels = ENUMS.RiskLabels;

    var RiskStatesList = [
        {
            Name: RiskNames[RiskLevels.None],
            Class: "",
            FlagClass: "",
            Value: RiskLevels.None
        },
        {
            Name: RiskNames[RiskLevels.Low],
            Class: RiskLabels[RiskLevels.Low],
            FlagClass: "risk-" + RiskLabels[RiskLevels.Low],
            Value: RiskLevels.Low
        },
        {
            Name: RiskNames[RiskLevels.Medium],
            Class: RiskLabels[RiskLevels.Medium],
            FlagClass: "risk-" + RiskLabels[RiskLevels.Medium],
            Value: RiskLevels.Medium
        },
        {
            Name: RiskNames[RiskLevels.High],
            Class: RiskLabels[RiskLevels.High],
            FlagClass: "risk-" + RiskLabels[RiskLevels.High],
            Value: RiskLevels.High
        },
        {
            Name: RiskNames[RiskLevels.VeryHigh],
            Class: RiskLabels[RiskLevels.VeryHigh],
            FlagClass: "risk-" + RiskLabels[RiskLevels.VeryHigh],
            Value: RiskLevels.VeryHigh
        }
    ];

    function Condition(cond, questionType, operators, comparators, operands, conditionOptions, sectionList) {
        _.assign(this, Condition.deserialize(cond, questionType, operators, comparators, operands, sectionList));

        // List Resources
        this.ConditionTypes = conditionOptions;
        this.RiskStatesList = RiskStatesList;

        this.Operators = operators;
        this.Comparators = comparators;
        this.Operands = operands;

        this.SectionList = sectionList;
        this.setQuestionsBySectionId(this.TargetSectionId);

        this.deleteQueue = [];

    }

    Condition.deserialize = function (cond, questionType, operators, comparators, operands, sectionList) {
        var deserial = {};
        deserial.ReadOnly = {};

        deserial.Id = cond.Id;
        deserial.TemplateQuestionId = cond.TemplateQuestionId,
            deserial.Version = cond.Version,
            deserial.NextConditionGroupId = cond.NextConditionGroupId;
        deserial.QuestionType = questionType;

        // Target Action
        deserial.Type = cond.Type;

        deserial.RiskLevelId = cond.RiskResult ? cond.RiskResult.RiskLevelId : RiskLevels.None,
            deserial.ReadOnly.Risk = RiskNames[deserial.RiskLevelId] || RiskNames[RiskLevels.None],
            deserial.Recommendation = cond.RiskResult ? cond.RiskResult.Recommendation : "",
            deserial.Description = cond.RiskResult ? cond.RiskResult.Description : "";

        var targetQuestionId = cond.SkipResult ? cond.SkipResult.TargetQuestionId : "";
        var section = Condition.getTargetSection(targetQuestionId, sectionList);
        var question = Condition.getTargetQuestion(targetQuestionId, section);
        deserial.TargetSectionId = (section ? section.Id : "");
        deserial.ReadOnly.Section = (section ? section.Name : "");
        deserial.TargetQuestionId = targetQuestionId || Condition.getFirstQuestionId(section) || "";
        deserial.ReadOnly.Question = (question ? question.Name : "");

        deserial.Operator = cond.Operator || operators[0].Value;

        // Read Only Term Data
        deserial.ReadOnly.Operator = Condition.getHumanizedOperator(cond.Operator, operators);

        deserial.Terms = this.createTerms(cond, questionType, operators, comparators, operands);

        return deserial;
    };

    Condition.prototype.serialize = function (version, groupOnly) {
        var serial = {
            "TemplateQuestionId": this.TemplateQuestionId,
            "Version": !_.isUndefined(version) ? version : (this.Version || ""),
            "Id": this.Id ? this.Id : "",
            "NextConditionGroupId": this.NextConditionGroupId || "",
            "Operator": this.Operator,
            "Conditions": []
        };

        if (!groupOnly) {
            _.each(this.Terms, function(term) {
                serial.Conditions.push(term.serialize(serial.ConditionGroupId, serial.Version));
            });
        }

        switch (this.Type) {
            case ConditionTypes.Skip:
                serial.SkipResult = {
                    TargetQuestionId: this.TargetQuestionId
                };
                break;
            case ConditionTypes.Risk:
                serial.RiskResult = {
                    RiskLevelId: this.RiskLevelId,
                    Description: this.Description,
                    Recommendation: this.Recommendation
                };
                break;
            default:
        }

        return serial;
    };

    Condition.prototype.setQuestionsBySectionId = function (sectionId, resetQuestionId) {
        if (this.SectionList) {
            var sectionIndex = _.findIndex(this.SectionList, ["Id", sectionId]);
            if (sectionIndex !== false && sectionIndex > -1) {
                this.QuestionList = this.SectionList[sectionIndex].Questions;
                if (resetQuestionId) {
                    this.TargetQuestionId = Condition.getFirstQuestionId(this.SectionList[sectionIndex]);
                }
            }
            else {
                this.QuestionList = Condition.getQuestions(this.SectionList[0]);
                this.TargetQuestionId = Condition.getFirstQuestionId(this.SectionList[sectionIndex]);
            }
        }
    };

    Condition.prototype.setQuestionsBySectionIndex = function (sectionIndex) {
        if (!_.isUndefined(sectionIndex) && !_.isUndefined(this.SectionList.indexOf(sectionIndex))) {
            this.QuestionList = Condition.getQuestions(this.SectionList[sectionIndex]);
            this.TargetQuestionId = Condition.getFirstQuestionId(this.SectionList[sectionIndex]);
        }
    };

    Condition.prototype.validate = function () {
        this.hasError = false;

        // Check if we have terms. We should always have atleast 1.
        if (_.isArray(this.Terms) && this.Terms.length) {
            _.each(this.Terms, function(term) {
                term.validate();
                if (term.hasError) {
                    this.hasError = true;
                }
            });

            if (this.Terms.length > 1 && !this.Operator) {
                // Multiple terms but no operator.
                this.hasError = true;
            }
        }
        else {
            // No terms, error!
            this.hasError = true;
        }

        // Check Type and Target Action based on Type.
        if (this.Type) {
            switch (this.Type) {
                case ConditionTypes.Skip:
                    if (!this.TargetSectionId || !this.TargetQuestionId) {
                        this.hasError = true;
                    }
                    break;
                case ConditionTypes.Risk:
                    if (!this.RiskLevelId) {
                        this.hasError = true;
                    }
                    break;
                default:
                    // Invalid condition type, error!
                    this.hasError = true;
            }
        }
        else {
            // No valid Type, error!
            this.hasError = true;
        }

    };

    Condition.prototype.getOperatorName = function () {
        return (this.Operator && this.Operators) ? (Condition.getHumanizedOperator(this.Operator, this.Operators) || "") : "";
    };

    Condition.prototype.addTerm = function (index) {
        var term = {
            ConditionGroupId: this.Id,
            Operator: this.Comparators[0].Id,
            Value: this.Operands[0].Value
        };
        this.Terms.splice(index + 1, 0, new Term(term, this.QuestionType, this.Operators, this.Comparators, this.Operands));
    };

    Condition.prototype.removeTerm = function (index) {
        if (this.Terms[index]) {
            if (this.Terms[index].Id) {
                this.queueTermDelete(index);
            }
            else {
                this.Terms.splice(index, 1);
            }
        }
    };

    Condition.prototype.queueTermDelete = function (index) {
        var itemArr = this.Terms.splice(index, 1);
        if (_.isArray(itemArr) && itemArr.length) {
            this.deleteQueue = this.deleteQueue.concat(itemArr);
        }
    };

    Condition.prototype.save = function (index, nextConditionId, templateVersion) {
        var serialized = this.serialize(templateVersion, nextConditionId);

        if (!_.isUndefined(this.ConditionGroupId) && (this.ConditionGroupId !== "")) {
            return this.saveAsUpdateCondition(serialized, index, nextConditionId, templateVersion);
        }
        return this.saveAsNewCondition(serialized, index, nextConditionId, templateVersion);
    };

    Condition.createAllFromQuestion = function (conditionsList, questionType, operators, comparators, operands, conditionOptions, SectionsList) {
        var list = [];
        _.each(conditionsList, function(cond) {
            if (!_.isUndefined(cond)) {
                list.push(new Condition(cond, questionType, operators, comparators, operands, conditionOptions, SectionsList));
            }
        });
        return list;
    };

    Condition.getHumanizedOperator = function (operator, operators) {
        for (var i = 0; i < operators.length; i++) {
            if (operators[i].Value === operator) {
                return operators[i].Name;
            }
        }
        return operators[0].Name;
    };

    Condition.createTerms = function (condition, questionType, operators, comparators, operands) {
        return condition.Terms.map(
            function (term) {
                return new Term(term, questionType, operators, comparators, operands);
            }
        );
    };

    Condition.getTargetSection = function (targetQuestionId, sectionList) {
        if (_.isArray(sectionList) && sectionList.length) {
            for (var i = 0; i < sectionList.length; i++) {
                var sectionIndex = _.findIndex(sectionList[i].Questions, ["Id", targetQuestionId]);
                if (sectionIndex > -1) {
                    return (sectionList[i] || sectionList[0]);
                }
            }
            return sectionList[0];
        }
        return [];
    };

    Condition.getTargetSectionId = function (targetQuestionId, sectionList) {
        var s = Condition.getTargetSection(targetQuestionId, sectionList);
        return s ? s.Id : "";
    };

    Condition.getTargetQuestion = function (targetQuestionId, section) {
        if (section && _.isArray(section.Questions) && section.Questions.length) {
            var question = Utilities.findById(section.Questions, targetQuestionId);
            return question ? question : Condition.getFirstQuestion(section);
        }
        return Condition.getFirstQuestion(section);
    };

    Condition.getQuestions = function (section) {
        return (section && section.Questions && section.Questions.length ? section.Questions : []);
    };

    Condition.getFirstQuestion = function (section) {
        var questions = Condition.getQuestions(section);
        return ((questions && questions.length && questions[0]) ? questions[0] : "");
    };

    Condition.getFirstQuestionId = function (section) {
        var q = Condition.getFirstQuestion(section);
        return (q ? q.Id : "");
    };

    return Condition;

}

ConditionFactory.$inject = ["ENUMS", "ConditionGroups", "Term"];
