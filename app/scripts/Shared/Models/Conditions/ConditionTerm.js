"use strict";
import Utilities from "Utilities";

export default function TermFactory($q, ENUMS, $filter) {

    var QuestionTypes = ENUMS.QuestionTypes;
    var ConditionTypes = ENUMS.ConditionTypes;
    var ComparatorTypes = ENUMS.ConditionComparatorTypes;
    var ResponseTypes = ENUMS.ResponseTypes;

    function Term(inputTerm, questionType, Operators, Comparators, Operands) {

        _.assign(this, inputTerm);

        // Id's
        this.ConditionGroupId = inputTerm.ConditionGroupId;
        this.Version = inputTerm.Version;

        // Term Data
        this.Comparator = inputTerm.Operator;
        this.Operand = this.normalizeOperand(inputTerm.Value, questionType);

        // Read Only Term Data
        this.ReadOnly = {
            Comparator: this.getHumanizedComparator(inputTerm.Operator, Comparators),
            Operand: this.getHumanizedOperand(inputTerm.Value, Operands, questionType)
        };

        // Lists
        this.Comparators = Comparators;
        this.Operands = Operands;

        this.QuestionType = questionType;

    }

    Term.prototype.serialize = function (groupId, version) {
        var serial = {
            "ConditionGroupId": !_.isUndefined(groupId) ? groupId : (this.ConditionGroupId || ""),
            "Id": this.Id,
            "Version": !_.isUndefined(version) ? version : (this.Version || ""),
            "Operator": this.Comparator,
            "Value": this.Operand,
            "ResponseType": this.getResponseType()
        };
        return serial;
    };

    Term.prototype.normalizeOperand = function (operand, questionType) {
        var normalized;
        switch (questionType) {
            case QuestionTypes.YesNo:
                switch (operand) {
                    case "True":
                    case "true":
                    case "1":
                    case 1:
                    case true:
                        normalized = true;
                        break;
                    case "False":
                    case "false":
                    case "0":
                    case 0:
                    case false:
                        normalized = false;
                        break;
                }
                break;
            case QuestionTypes.Multichoice:
                normalized = operand;
                break;
            default:
                normalized = operand;
        }
        return normalized;
    };

    Term.prototype.getHumanizedOperand = function (operand, operands, questionType) {
        var humanized = operand;
        switch (questionType) {
            case QuestionTypes.YesNo:
                switch (operand) {
                    case "True":
                    case "true":
                    case "1":
                    case 1:
                    case true:
                        humanized = "YES";
                        break;
                    case "False":
                    case "false":
                    case "0":
                    case 0:
                    case false:
                        humanized = "NO";
                        break;
                }
                break;
            case QuestionTypes.Multichoice:
                var expression = function (op) { return op.Value === operand; };
                var temp = $filter("pick")(operands, expression);
                humanized = (_.isArray(temp) && temp.length) ? temp[0].Name : operand;
                break;
            default:
                humanized = operand;
        }
        return humanized;
    };

    Term.prototype.getHumanizedOperator = function (operator, operators) {
        var op = operator;
        var operatorResult = Utilities.findById(operators, operator);
        if (operatorResult !== null && operatorResult) {
            op = operatorResult.Name;
        }
        return op;
    };

    Term.prototype.getHumanizedComparator = function (comparator, comparators) {
        var comp = comparator;
        var comparatorResult = Utilities.findById(comparators, comparator);
        if (comparatorResult !== null && comparatorResult) {
            comp = comparatorResult.Description;
        }
        return comp;
    };

    Term.prototype.opShow = function () {
        switch (this.Comparator) {
            case ComparatorTypes.NotSure:
            case ComparatorTypes.Other:
            case ComparatorTypes.NotApplicable:
                return false;
            default:
                return true;
        }
    };

    Term.prototype.getResponseType = function () {
        switch (this.Comparator) {
            case ComparatorTypes.NotApplicable:
                return ResponseTypes.NotApplicable;
            case ComparatorTypes.NotSure:
                return ResponseTypes.NotSure;
            case ComparatorTypes.Other:
                return ResponseTypes.Other;
            default:
                if (this.QuestionType === QuestionTypes.Multichoice)
                    return ResponseTypes.Reference;
                else
                    return ResponseTypes.Value;
        }
    };

    Term.prototype.validate = function () {
        this.hasError = false;
        if (!this.Comparator) {
            // No Comparator, error.
            this.hasError = true;
        }
        else if (this.opShow() && (this.Operand < 0)) {
            // Invalid Operand, error
            this.hasError = true;
        }

    };

    return Term;

}

TermFactory.$inject = ["$q", "ENUMS", "$filter"];
