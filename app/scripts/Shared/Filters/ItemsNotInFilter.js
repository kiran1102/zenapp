"use strict";

export default function ItemsNotIn() {
    return function (items, key, comparatorList) {
        if (_.isArray(items) && _.isArray(comparatorList)) {
            var filtered = [];
            _.each(items, function(item) {
                // if item property does not exist in comparatorList, then push.
                if (comparatorList.indexOf(item[key]) < 0) {
                    filtered.push(item);
                }
            });
            return filtered;
        }
        else {
            return items;
        }
    };
}

ItemsNotIn.$inject = [];
