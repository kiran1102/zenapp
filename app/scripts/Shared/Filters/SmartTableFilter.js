    "use strict";

    export default function smartTable($filter) {

        function listContains(items, column, keys, props) {
            var arr = [];
            _.each(items, function(item) {
                item.invalid = false;
                if (!item.IgnoreFilter) {
                    for (var i = 0; i < keys.length; i++) {
                        var key = keys[i];
                        if (column.compare(key, item, column, props)) {
                            item.invalid = true;
                            break;
                        }
                    }
                }
                if (!item.invalid) {
                    arr.push(item);
                }
            });
            return arr;
        }

        return function(items, columns, props, searchFilter, optionalString, final, itemFinal) {
            var originalList = items;
            if (items && columns) {
                _.each(columns, function(col) {
                    // for each valid column with a compare function
                    if (col && _.isFunction(col.compare)) {
                        // find the active filtered key
                        var activeKeys = $filter("filter")(col[props.Column_List], {
                            Filtered: true
                        });
                        // Apply filter to check whether items contain any activeKey
                        if (activeKeys && activeKeys.length) {
                            items = listContains(items, col, activeKeys, props);
                        }
                    }
                });
            }

            if (items.length && _.isFunction(searchFilter) && optionalString) {
                items = searchFilter(items, optionalString);
            }

            // For all filtered out items, run itemFinal function.
            var diff = _.difference(originalList, items);
            if (_.isFunction(itemFinal)) {
                _.each(diff, function(item) {
                    itemFinal(item, false);
                });
            }

            if (_.isFunction(final)) {
                items = final(items);
            }

            return items;
        };
    }

    smartTable.$inject = ["$filter"];
