
export default function IsInParentTree() {
    return function (list, treeMap, id, comparatorKey, direction) {
        if (_.isUndefined(direction)) {
            direction = "both";
        }

        var filtered = [];
        if (id && comparatorKey && treeMap && _.isArray(list)) {
            _.each(list, function(item) {
                switch (direction) {
                    case "down":
                        if (treeMap.IsInParentTree(id, item[comparatorKey])) {
                            filtered.push(item);
                        }
                        break;
                    case "up":
                        if (treeMap.IsInParentTree(item[comparatorKey], id)) {
                            filtered.push(item);
                        }
                        break;
                    case "both":
                        if (treeMap.IsInParentTree(item[comparatorKey], id) || treeMap.IsInParentTree(id, item[comparatorKey])) {
                            filtered.push(item);
                        }
                        break;
                }
            });
            return filtered;
        }
        else {
            return list;
        }
    };
}

IsInParentTree.$inject = [];
