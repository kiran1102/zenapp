"use strict";

export default function tableItemsComparator() {

    function doCheck(compare, item) {
        if (compare && compare.Key) {
            var value = (compare.Cell ? (item[compare.Cell] ? item[compare.Cell][compare.Key] : item[compare.Cell]) : item[compare.Key]);
            if (compare.CheckTruthy) {
                return !!(value);
            } else if (compare.CheckFalsey) {
                return !(value);
            } else {
                return !!(value === compare.Value);
            }
        }
        return false;
    }

    /**
    @name TableComparator
    @param {Array} row
    @param {Object} comparator
    @returns {Array} filtered
    @description Filter out all items in row that match comparator
    @example TableComparator(row, comparator)
    **/
    return function (rows, comparators, requireAll) {
        if (_.isArray(rows) && _.isArray(comparators)) {
            var filtered = [];
            _.each(rows, function(item) {
                var passes = 0;
                _.each(comparators, function(compare) {
                    if (doCheck(compare, item)) {
                        passes++;
                    }
                });

                if (requireAll) {
                    if (passes === comparators.length) {
                        filtered.push(item);
                    }
                } else if (passes) {
                    filtered.push(item);
                }
            });
            return filtered;
        } else return rows;
    };
}
