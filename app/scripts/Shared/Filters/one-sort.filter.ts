import * as _ from "lodash";

export default class OneSortFilter {
    static $inject: string[] = ["ENUMS"];

    constructor(ENUMS: any) {
        const SortTypes = ENUMS.SortTypes;

        return (rows: any[], sortType?: boolean, sortValueFn?: (arg: any) => any, omitFn?: (arg: any) => any): any[] => {
            let sortedRows: any[] = [];
            const sortOmitRows: { sort: any[], omit: any[] } = _.isFunction(omitFn) ? omitFn(rows) : { sort: rows, omit: [] };

            if (_.isFunction(sortValueFn)) {
                sortedRows = _.sortBy(
                    sortOmitRows.sort,
                    (row: any): any => {
                        return sortValueFn(row);
                    },
                );
            } else {
                sortedRows = _.sortBy(
                    sortOmitRows.sort,
                    (row: any): any => {
                        return row;
                    },
                );
            }

            if (!_.isUndefined(sortType) && sortType === SortTypes.Ascending) {
                sortedRows = _.reverse(sortedRows);
            }

            return sortedRows.concat(sortOmitRows.omit);
        };
    }
}
