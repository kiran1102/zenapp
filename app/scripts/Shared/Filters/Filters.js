export function ResponseFilter() {
    var text;
    return function (delimeter, response) {
        if (delimeter === undefined || delimeter === null || delimeter === "") {
            text = response;
        } else {
            var start = delimeter;
            var end = delimeter.substring(0, 1) + "/" + delimeter.substring(1, delimeter.length);
            var startIndex = response.indexOf(start) + start.length;
            var endIndex = response.indexOf(end);
            text = response.substring(startIndex, endIndex);
        }

        var textElement = document.createElement("P");
        textElement.innerHTML = text;
        return textElement.textContent || textElement.innerText || "Error";
    };
}

export function dateToISO() {
    return function (date) {
        date = new Date(parseInt(date.substring(6)));
        return date;
    };
}

export function filterUsersForGroup() {
    return function (users, groupId) {
        var filteredUsers = [];

        // Catch if groupId is empty or undefined.
        if (groupId === undefined || groupId === "") {
            return users;
        }
        _.each(users, function(user) {
            if (user.GroupId === groupId) filteredUsers.push(user);
        });
        return filteredUsers;
    };
}

export function capitalize() {
    return function (input) {
        return (!!input) ? input.charAt(0).toUpperCase() + input.substr(1).toLowerCase() : "";
    };
}

export function uncapitalize() {
    return function (input) {
        return (!!input) ? input.charAt(0).toLowerCase() + input.substr(1).toLowerCase() : "";
    };
}

export function getByKey() {
    return function (input, id) {
        var i = 0,
            len = input.length;
        for (; i < len; i++) {
            if (input[i].Key === id) {
                return input[i];
            }
        }
        return null;
    }
}

export function unique() {
    return function (collection, keyname) {
        var output = [],
            keys = [];
        _.each(collection, function(item) {
            var key = item[keyname];
            if (keys.indexOf(key) === -1) {
                keys.push(key);
                output.push(item);
            }
        });

        return output;
    }
}

export function absolute() {
    return function (value) {
        return Math.abs(value);
    }
}
