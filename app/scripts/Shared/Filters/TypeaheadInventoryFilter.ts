import { IExtendedRootScopeService } from "interfaces/root-scope.interface";

export default function TypeaheadInventoryFilter(
    ENUMS: any,
    $filter: ng.IFilterService,
    $rootScope: IExtendedRootScopeService): any {

    return (items: any[], inputValue: string, allowOther: boolean, type: number): any[] => {
        const filteredList: any = $filter("filter")(items, { $: inputValue });
        const questionTypes: any = ENUMS.QuestionTypes;
        const dmQuestionTypes: any = ENUMS.DMQuestionTypes;
        const tableInputTypes: any = ENUMS.TableInputTypes;

        function showNoAnswer(): any {
            const placeholder: any = {
                InputValue: "",
                IsPlaceholder: true,
                Name: `(${$rootScope.t("NoAnswer")})`,
            };
            filteredList.unshift(placeholder);

        }
        function showInputWithHint(hintWord: string, existingMatch?: any): any {
            if (inputValue) {
                const nameWithHint: string = hintWord ? `${hintWord} "${inputValue}"` : null;
                const placeholder: any = {
                    ExistingMatch: existingMatch,
                    InputValue: inputValue,
                    IsPlaceholder: true,
                    Name: nameWithHint,
                };
                filteredList.unshift(placeholder);
            }
        }
        // Create placeholder based on type
        switch (type) {
            case questionTypes.Provider:
                if (allowOther && inputValue && filteredList) {
                    const hasMatch: boolean = Boolean(filteredList.length && filteredList[0].Name.toLowerCase() === inputValue.toLowerCase());
                    if (!hasMatch) {
                        showInputWithHint($rootScope.t("Add"));
                    }
                } else if (!inputValue || (filteredList && !filteredList.length)) {
                    showNoAnswer();
                }
                break;
            case questionTypes.User:
                if (allowOther && inputValue && filteredList && !filteredList.length) {
                    showInputWithHint($rootScope.t("EnterEmail"));
                } else if (!inputValue || (filteredList && !filteredList.length)) {
                    showNoAnswer();
                }
                break;
            case questionTypes.Application:
                if (allowOther && inputValue && filteredList) {
                    const existingMatch: any = (filteredList.length && filteredList[0].Name.toLowerCase() === inputValue.toLowerCase()) ? filteredList[0] : null;
                    showInputWithHint($rootScope.t("Create"), existingMatch);
                } else if (!inputValue || (filteredList && !filteredList.length)) {
                    showNoAnswer();
                }
                break;
            case questionTypes.Location:
                if (allowOther && inputValue && filteredList) {
                    showInputWithHint($rootScope.t("Add"));
                } else if (!inputValue || (filteredList && !filteredList.length)) {
                    showNoAnswer();
                }
                break;
            case dmQuestionTypes.TypeaheadMultiselectLoop:
                break;
            default:
                if (!inputValue || (filteredList && !filteredList.length)) {
                    showNoAnswer();
                }
        }
        return filteredList;
    };
}

TypeaheadInventoryFilter.$inject = ["ENUMS", "$filter", "$rootScope"];
