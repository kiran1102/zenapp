declare var angular: any;
import * as _ from "lodash";

export default function InventoryOptionsFilter(): any {

    return (options: any[], items: any[], keys: any, resourceKeys: any): any[] => {
        if (options) {
            _.each(options, (option: any): void => {
                option.isDisabled = !(option.Show(items, keys, resourceKeys));
            });
        }
        return options;
    };
}
