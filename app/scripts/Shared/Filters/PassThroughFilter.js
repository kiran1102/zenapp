"use strict";

export default function passThrough() {
    /**
    @name PassThroughFilter
    @param {Array} values
    @param {Object} item
    @param {Object} comparator
    @returns {Array} Filtered array of values by comparator for item.
    @description Filtered an array of values by a comparator for a specified item.
    @example PassThroughFilter(values, comparator, item)
    **/
    return function (values, data, comparator, comparatorId) {
        if (_.isArray(values) && _.isFunction(comparator) && !_.isUndefined(data)) {
            return comparator(values, data, comparatorId);
        }
        return values;
    };
}
