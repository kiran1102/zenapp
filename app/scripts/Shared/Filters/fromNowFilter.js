export default function fromNow() {
    return function (date) {
        return moment(date).fromNow();
    };
}
