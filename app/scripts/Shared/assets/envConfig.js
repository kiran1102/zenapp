"use strict";

angular.module("envConfig", [])
    .constant("API_URL", (function() {
        var origin = window.location.host || window.location.href;
        var regex = "app";
        var found = origin.match(regex);

        if (found) {
            // Use regex to replace 'app' for 'api'
            origin = origin.replace(regex, "api");
        } else {
            origin = "https://api-" + window.location.host;
        }

        return origin + "/api/v1/private";
    })())

.constant("MOCK", false);
