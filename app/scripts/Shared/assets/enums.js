export default function enums(CONSTANTS) {
    return {
        QuestionSliderPositions: {
            Intro: 10,
            Question: 20,
            Exit: 30,
        },
		TransitionTimes: {
			AssessmentSlide: 1000,
			SectionNavFade: 300,
			PopupDelay: 1000,
			TooltipDelay: 500,
		},
        ProjectStateColors: {
            InProgress: "#008ACC",  //$blue
            UnderReview: "#FDA028", //$orange
            NeedInfo: "#16BDB7",    //$teal
            Completed: "#6CC04A",   //$green
            GapAnalysis: "#FED03F", //$yellow
            InMitigation: "#55788C" //$steel
        },
        EmptyGuid: "00000000-0000-0000-0000-000000000000",
        Regex: {
            Guid: /^[0-9A-Fa-f]{8}[-]([0-9A-Fa-f]{4}[-]){3}[0-9A-Fa-f]{12}$/,
            Email: /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
            IP: /^(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)[.]){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/,
            IP_MULTI: /^((?:(?:25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9][0-9]|[0-9]?)[.]){3}(?:25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9][0-9]|[0-9]?)(,(?:(?:25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9][0-9]|[0-9]?)[.]){3}(?:25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9][0-9]|[0-9]?))*)|((?:(?:[0-3][0-7][0-7]|[0-7][0-7]|[0-7]?)[.]){3}(?:[0-3][0-7][0-7]|[0-7][0-7]|[0-7]?)(,(?:(?:[0-3][0-7][0-7]|[0-7][0-7]|[0-7]?)[.]){3}(?:[0-3][0-7][0-7]|[0-7][0-7]|[0-7]?))*)$/,
            CIDR: /^(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)[.]){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)(\/(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?))?$/,
            CIDR_MULTI: /^((?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)[.]){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)[\/](([1][0-9])|([2][0-9])|([3][0-2])|([1-9]))?(,(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)[.]){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)[\/](([1][0-9])|([2][0-9])|([3][0-2])|([1-9])))*)$/,
        },
        bindingTypes: {
            Post: 1,
            Redirect: 2,
            Artifact: 3,
        },
        certificateProviders: {
		    ServiceProvider: 0,
		    IdentityProvider: 1,
		},
        directoryProviders: {
            Basic: 0,
            SAML: 1,
            LDAP: 2,
        },
        StatusCodes: {
            Success: 200,
            BadRequest: 400,
            NotFound: 404,
            Conflict: 409,
            ServerError: 500,
        },
        TemplateStates: {
            Stock: 10,
            Draft: 20,
            Published: 30,
            Archived: 40,
            Locked: 50,
        },
        DMTemplateStates: {
            Draft: 10,
            Published: 20,
        },
        DragTypes: {
            QuestionAdd: 10,
            QuestionAddGroup: 15,
            QuestionReorder: 20,
            QuestionGroupReorder: 25,
            SectionAdd: 100,
            SectionReorder: 130,
            StatementAdd: 30,
        },
        SectionStates: {
            NotStarted: 10,
            InProgress: 20,
            Completed: 30,
        },
        ProjectStatesOrder: {
            InProgress: 1,
            UnderReview: 2,
            NeedInfo: 3,
            GapAnalysis: 4,
            RiskTracking: 4,
            InMitigation: 5,
            Completed: 6,
        },
        FilterDisplayTypes: {
            CollectionPicker: 1,
            Select: 2,
            Toggle: 3,
        },
        TemplateTypes: {
            Custom: 10,
            Readiness: 20,
            RequestForm: 30,
            Datamapping: 40,
            SelfService: 50,
            NewDatamapping: 60,
        },
        QuestionVersionStates: {
            Undecided: 1,
            Change: 2,
            DontChange: 3,
            New: 4,
        },
        Regions: {
            USA: "United States",
            EEARegion: "EEA",
            AdequateProtection: "Adequate Protection",
        },
        RiskLabels: {
            0: "none",
            1: "low",
            2: "medium",
            3: "high",
            4: "very-high",
        },
        RiskLabelClasses: {
            1: "risk-low",
            2: "risk-medium",
            3: "risk-high",
            4: "risk-very-high",
        },
        RiskLabelBgClasses: {
            1: "risk-low--bg",
            2: "risk-medium--bg",
            3: "risk-high--bg",
            4: "risk-very-high--bg",
        },
        RiskPrettyNames: {
            0: "None",
            1: "Low",
            2: "Medium",
            3: "High",
            4: "Very High",
        },
        RiskState: {
            Identified: 10,
            Analysis: 20,
            Addressed: 30,
            Exception: 40,
            Reduced: 45,
            Accepted: 50,
            Retained: 55,
            Archived: 60,
        },
        RiskStateText: {
            10: "Identified",
            20: "RequirementAdded",
            30: "Resolved",
            40: "ExceptionRequested",
            45: "Reduced",
            50: "Accepted",
            55: "Retained",
            60: "Archived"
        },
        RiskStates: {
            10: "Identified",
            20: "Analysis",
            30: "Addressed",
            40: "Exception",
            45: "Reduced",
            50: "Accepted",
            55: "Retained",
            60: "Archived"
        },
        CheckboxTypes: {
            Primary: 10,
            Danger: 20,
        },
        QuestionTypes: {
            Content: 1,
            TextBox: 2,
            Multichoice: 3,
            YesNo: 4,
            DateTime: 5,
            DataElement: 110,
            Application: 1000,
            Location: 1010,
            User: 1020,
            Provider: 1030,
            ApplicationGroup: 1040,
            PlaceholderGroup: 1050,
        },
        DMAssessmentStates: {
            All: 0,
            NeedsAttention: 1,
            NotStarted: 10,
            InProgress: 20,
            Completed: 30,
            Archived: 40,
            UnderReview: 50,
        },
        DMAssessmentStateLabels: {
            10: "NotStarted",
            20: "InProgress",
            30: "Completed",
            40: "Archived",
            50: "UnderReview",
        },
        DMAssessmentTemplateTypes: {
            Asset: 20,
            ProcessingActivity: 30,
        },
        DMAssessmentTemplateTypeNames: {
            20: "Asset",
            30: "Processing Activity",
        },
        DMAssessmentStatesOrder: {
            NotStarted: 1,
            InProgress: 2,
            UnderReview: 3,
            Completed: 4,
        },
        DMAssessmentStateColors: {
            NotStarted: "#696969",  //$slate
            InProgress: "#008ACC",  //$blue
            UnderReview: "#FDA028", //$orange
            Completed: "#6CC04A",   //$green
        },
        DMAssessmentStateClasses: {
            10: "not-started",
            20: "in-progress",
            30: "completed",
            40: "archived",
            50: "under-review",
        },
        DMQuestionTypes: {
            Welcome: 10,
            Info: 20,
            AppInfo: 30,
            MultiSelect: 40,
            SingleSelect: 50,
            Text: 60,
            Bumper: 70,
            MultiSelectLoop: 80,
            TypeaheadMultiselectLoop: 90,
            DataSubject: 100,
            Asset: 110,
            AssetSingleSelect: 115,
            Process: 120,
            ProcessSingleSelect: 125,
            SingleSelectLoop: 130,
        },
        DMQuestionDisplayTypes: {
            Buttons: 10,
            Typeahead: 20,
            Dropdown: 30,
            CheckList: 40,
        },
        QuestionAttributeTypes: {
            Text: 10,
            Select: 20,
            MultiSelect: 30,
        },
        QuestionAttributes: {
            // Basic Attributes
            Name: "Name",
            Description: "Description",
            Hint: "Hint",
            ReportFriendlyName: "ReportFriendlyName",
            Required: "Required",
            // Allowable Attributes
            AllowOther: "AllowOther",
            AllowNotSure: "AllowNotSure",
            AllowNotApplicable: "AllowNotApplicable",
            AllowJustification: "AllowJustification",
            RequireJustification: "RequireJustification",
            // Question Specific Attributes
            Multiline: "Multiline",
            Options: "Options",
            MultiSelect: "MultiSelect",
            GroupFilter: "GroupFilter",
        },
        ResponseTypes: {
            Value: 1,
            Reference: 2,
            Other: 3,
            NotSure: 4,
            NotApplicable: 5,
            Justification: 6,
        },
        OptionTypes: {
            Custom: 0,
            DataElements: 10,
            Assets: 20,
            Processes: 30,
            Users: 300,
            Orgs: 310,
            Locations: 400,
            Subjects: 410,
            Categories: 420,
        },
        QuestionCommentTypes: {
            Question: 10,
            MoreInfo: 20,
            InfoAdded: 25,
            RiskLevel: 30,
        },
        SubmitStates: {
            Submit: 10,
            SendBack: 20,
            Approve: 30,
            GoToAnalsis: 40,
            NextSteps: 50,
            SendRecommendations: 60,
            SendMitigations: 70,
            CompleteAnalysis: 80,
        },
        ConditionTypes: {
            Skip: "Skip",
            Risk: "Risk"
        },
        ConditionOperatorsTypes: {
            And: 1,
            Or: 2
        },
        ConditionOperators: [{
            Name: "AND",
            Value: 1
        }, {
            Name: "OR",
            Value: 2
        }],
        ConditionComparatorTypes: {
            NotSure: 1,
            Other: 2,
            Equals: 3,
            NotEquals: 4,
            NotApplicable: 5
        },
        DMRelationalOperators: {
            Equals: 1,
            NotEquals: 2,
            NotSure: 3,
        },
        DataMapsIds: {
            AssetsByLocation: 1,
            Crossborder: 2,
            Lineage: 3,
        },
        InventoryStatuses: {
            Approved: 10,
            Pending: 20,
            Disabled: 30,
            Archived: 40,
        },
        InventoryTableIds: {
            Elements: "10",
            Applications: "20",
            Assets: "20",
            Processes: "30",
        },
        InventoryTableNames: {
            Default: "Inventory",
            Singular: {
                10: "Element",
                20: "Asset",
                30: "ProcessingActivity",
            },
            Plural: {
                10: "Elements",
                20: "Assets",
                30: "ProcessingActivities",
            },
        },
        AttributeTableNames: {
            Default: "Attribute",
            Singular: {
                10: "Element",
            },
            Plural: {
                10: "Elements",
            },
        },
        AttributeValueStatuses: {
            Enabled: 10,
            Disabled: 20,
        },
        TableInputTypes: {
            Text: 10,
            Typeahead: 20,
            List: 30,
            User: 40,
            Date: 50,
            Link: 60,
            Popup: 70,
            PopupList: 71,
            PopupTable: 72,
            PopupCard: 73,
            ProjectState: 80,
            Actions: 90,
            Tooltip: 100,
        },
        TableWidths: {
            Small: "small",
            Medium: "medium",
            Normal: "normal",
            Large: "large"
        },
        RiskColumnTypes: {
            ProjectRisk: 1,
            ProjectName: 2,
            ProjectState: 3,
            Description: 4,
            Recommendation: 5,
            Status: 6,
            SectionName: 7,
            Question: 8,
            QuestionFriendlyName: 9,
            QuestionAnswer: 10,
            QuestionJustification: 11,
            RiskDeadline: 12,
            MitigatedDt: 13,
            RiskOwner: 14,
        },
        ReportColumnTypes: {
            Name: 1,
            StateDesc: 2,
            OrgGroup: 3,
            Lead: 4,
            Reviewer: 5,
            Creator: 6,
            Deadline: 7,
            Question: 8,
            ProjectRisk: 9,
            VeryHighRisk: 14,
            HighRisk: 10,
            MediumRisk: 11,
            LowRisk: 12,
            TotalRisk: 13,
            ProjectVersion: 14,
            Description: 15,
            Modified: 16,
            CreatedDate: 17,
            CompletedDate: 18,
        },
        DataSubjectRequestQueueColumnTypes: {
            Id: 1,
            Name: 2,
            Status: 3,
            Purpose: 4,
            DaysLeftToRespond: 5,
            Extension: 6,
            DateCreated: 7,
            DSRType: 8,
            Respondent: 9
        },
        ReportFilterColumnTypes: {
            Name: 1,
            Description: 2,
            CreatedBy: 3,
            ModifiedDate: 4,
        },
        roles: {
            "Invited User": 10,
            "Project Owner": 20,
            "Privacy Officer": 30,
            "Site Admin": 40,
        },
        RoleNames: {
            PrivacyOfficer: "Privacy Officer",
            SiteAdmin: "Site Admin",
            InvitedUser: "Invited",
            ProjectOwner: "Project Owner",
            ProjectViewer: "Project Viewer",
        },
        taskStateKeys: {
            10: CONSTANTS.TASK_STATES.OPEN,
            20: CONSTANTS.TASK_STATES.COMPLETE
        },
        taskStateIds: {
            TaskOpen:      10,
            TaskCompleted: 20,
            TaskFailed:    30,
        },
        SortTypes: {
            None: 0,
            Ascending: 1,
            Descending: 2,
        },
        ContextTypes: {
            Inventory: 10,
            Assessment: 20,
            DataMapping: 30,
            DMAssessment: 40,
            DMTemplate: 50,
        },
        InputTypes: {
            Text: 10,
            Select: 20,
            List: 25,
            Multiselect: 30,
            Date: 40,
            Modal: 50,
        },
        YesNoText: {
            Yes: "Yes",
            No: "No",
            NotSure: "Not Sure",
        },
        InventoryTypes: {
            Inventory: "Inventory",
            Attribute: "Attribute",
        },
        ActionKeys: {
            Delete: "delete",
            Edit: "edit",
            Conditions: "conditions",
        },
        Pagination: {
            Inventory: {
                size: 20,
                page: 0,
            },
        },
        Status: {
            New: "New",
            InDiscovery: "InDiscovery",
            Complete: "Discovered",
        },
        BulkImportFileTypes: {
            CSV: "csv",
            XLSX: "xlsx",
        },
        ScanStatus: {
            Pending: "Pending",
            InProgress: "InProgress",
            Complete: "Complete",
            Cancelled: "Cancelled",
        },
        MultiChoiceTypes: {
            Button: 10,
            Dropdown: 20,
        },
        BulkImportStatus: {
            Pending: 1,
            InProgress: 2,
            Error: 3,
            Completed: 4,
        },
        CardStyle: {
            Cards: 10,
            List: 20,
        },
        ProjectSectionDirections: {
            Backwards: -1,
            NotMoving: 0,
            Forwards: 1,
        },
        IntegrationSystems: {
            JIRA: "a4b4442e-0a5f-49c8-99df-8722d03c89f2",
            OneTrust: "6eb78f1a-56ac-4a89-a655-8f5a870fa054",
        },
        IntegrationSystemsTab: {
            AUTHENTICATION: 1,
            API_DEFINITION: 2,
        }
    };
}

enums.$inject = ["CONSTANTS"];
