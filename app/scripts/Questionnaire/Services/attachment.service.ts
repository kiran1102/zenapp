import { ProtocolService } from "modules/core/services/protocol.service";
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import { IProtocolPacket, IProtocolMessages, IProtocolConfig } from "interfaces/protocol.interface";

export default class AttachmentService {
    static $inject: string[] = ["$rootScope", "OneProtocol"];

    private translate: any;

    constructor(
        public readonly $rootScope: IExtendedRootScopeService,
        private readonly OneProtocol: ProtocolService) {
            this.translate = $rootScope.t;
        }

    public list(questionId: string): ng.IPromise<IProtocolPacket> {
        const config: IProtocolConfig = this.OneProtocol.config("GET", `/attachment/get/${questionId}`);
        const messages: IProtocolMessages = { Error: { custom: this.translate("ErrorRetrievingResponses") } };
        return this.OneProtocol.http(config, messages);
    }

    public createAttachment(attachment: any): ng.IPromise<IProtocolPacket> {
        const config: IProtocolConfig = this.OneProtocol.config("POST", "/attachment/add", [], attachment);
        const messages: IProtocolMessages = {Error: { custom: this.translate("ErrorCreatingAttachment")}, Success: { custom: this.translate("AttachmentSaved") } };
        return this.OneProtocol.http(config, messages);
    }

    public delete(attachmentId: string): ng.IPromise<IProtocolPacket> {
        const config: IProtocolConfig = this.OneProtocol.config("DELETE", `/attachment/delete/${attachmentId}`);
        const messages: IProtocolMessages = {Error: { custom: this.translate("ErrorDeletingAttachment")}, Success: { custom: this.translate("AttachmentDeleted") } };
        return this.OneProtocol.http(config, messages);
    }
}
