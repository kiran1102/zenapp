
"use strict";
import { saveAs } from "file-saver";

export default function Documents($http, $rootScope, NotificationService, OneProtocol) {
    var service = {
        tree: getTree,
        list: getList,
        get: getItem,
        add: addItem,
        update: updateItem,
        delete: deleteItem,
        download: download
    };

    return service;

    function getTree() {
        return $http.get("/document/tree").then(
            function successCallback(response) {
                var packet = {
                    result: true,
                    status: response.status,
                    data: response.data ? response.data : "",
                    time: (new Date()).getTime()
                };
                return packet;
            },
            function errorCallback(response) {
                var packet = {
                    result: false,
                    status: response.status,
                    data: response.data.Message ? response.data.Message : "Error Retrieving Documents.",
                    time: (new Date()).getTime()
                };
                // NotificationService.alertWarning("Warning", packet.data);
                return packet;
            });
    }

    function getList(id) {
        return $http.get("/document/list/" + id).then(
            function successCallback(response) {
                var packet = {
                    result: true,
                    status: response.status,
                    data: response.data ? response.data : "",
                    time: (new Date()).getTime()
                };
                return packet;
            },
            function errorCallback(response) {
                var packet = {
                    result: false,
                    status: response.status,
                    data: response.data.Message ? response.data.Message : "Error Retrieving Documents.",
                    time: (new Date()).getTime()
                };
                // NotificationService.alertWarning("Warning", packet.data);
                return packet;
            });
    }

    function getItem(id) {
        return $http.get("/document/get/" + id).then(
            function successCallback(response) {
                var packet = {
                    result: true,
                    status: response.status,
                    data: response.data ? response.data : "",
                    time: (new Date()).getTime()
                };
                return packet;
            },
            function errorCallback(response) {
                var packet = {
                    result: false,
                    status: response.status,
                    data: response.data.Message ? response.data.Message : "Error Retrieving Document.",
                    time: (new Date()).getTime()
                };
                // NotificationService.alertWarning("Warning", packet.data);
                return packet;
            });
    }

    function addItem(item) {

        var fd = new FormData();
        fd.append('File', item.Blob);
        fd.append('FileName', item.FileName);
        fd.append('Extension', item.Extension);
        fd.append('IsDirectory', item.IsDirectory);
        fd.append('ParentDirectoryId', item.ParentDirectoryId);

        var config = {
            method: "POST",
            url: "/document/add",
            body: fd,
            multipartFormData: true,
        };

        var messages = { Error: { custom: $rootScope.t("ErrorAddingFile") } };

        return OneProtocol.http(config, messages);

    }

    function updateItem(item) {
        return $http.put("/document/update", item).then(
            function successCallback(response) {
                var packet = {
                    result: true,
                    status: response.status,
                    data: response.data ? response.data : "",
                    time: (new Date()).getTime()
                };
                return packet;
            },
            function errorCallback(response) {
                var packet = {
                    result: false,
                    status: response.status,
                    data: response.data.Message ? response.data.Message : $rootScope.t("ErrorUpdatingFile"),
                    time: (new Date()).getTime()
                };
                NotificationService.alertWarning("", packet.data);
                return packet;
            });
    }

    function deleteItem(id) {
        return $http.delete("/document/delete?id=" + id).then(
            function successCallback(response) {
                var packet = {
                    result: true,
                    status: response.status,
                    data: response.data ? response.data : "",
                    time: (new Date()).getTime()
                };
                return packet;
            },
            function errorCallback(response) {
                var packet = {
                    result: false,
                    status: response.status,
                    data: response.data.Message ? response.data.Message : $rootScope.t("ErrorDeletingDocument"),
                    time: (new Date()).getTime()
                };
                NotificationService.alertWarning("", packet.data);
                return packet;
            });
    }

    function download(id, fileName) {
        var config = {
            method: "GET",
            url: "/document/v1/document/" + id + "/download",
            responseType: "arraybuffer",
            removeURI: true,
         };

        return $http(config).then(
            function successCallback(response) {
                var blob = new Blob([response.data], {
                    type: "application/octet-stream",
                });
                saveAs(blob, fileName);
            },
            function errorCallback(response) {
                var packet = {
                    result: false,
                    status: response.status,
                    data: response.data.Message ? response.data.Message : $rootScope.t("ErrorDownloadingFile"),
                    time: (new Date()).getTime()
                };
                NotificationService.alertWarning("", packet.data);
                return packet;
            }
        );
    }

}

Documents.$inject = ["$http", "$rootScope", "NotificationService", "OneProtocol"];
