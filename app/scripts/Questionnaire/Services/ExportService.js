import { saveAs } from "file-saver";
import { endsWith } from "lodash";

import { TemplateTypes } from "enums/template-type.enum";
import { ExportTypes } from "enums/export-types.enum";

import { ModalService } from "sharedServices/modal.service";

export default function Export($rootScope, $http, NotificationService, $document, $window, $log, ModalService, TaskPollingService) {

    var blobHelper = function (response, type, baseName) {
        var httpFileName = getFileNameFromHttpResponse(response);
        var fileName;
        if (baseName || !httpFileName) {
            fileName = baseName + "_" + new Date().toLocaleString().replace(/ /g, "");
        } else {
            fileName = httpFileName;
        }

        if (type === ExportTypes.XLSX) {
            if (!endsWith(fileName, ".xlsx")) {
                fileName += ".xlsx";
            }
            return buildAndSaveFile(response.data, ExportTypes.XLSX, fileName);
        } else if (type === ExportTypes.CSV) {
            if (!endsWith(fileName, ".csv")) {
                fileName += ".csv";
            }
            return buildAndSaveFile(response.data, ExportTypes.CSV, fileName);
        } else if (type === ExportTypes.XLS) {
            if (!endsWith(fileName, ".xls")) {
                fileName += ".xls";
            }
            return buildAndSaveFile(response.data, ExportTypes.XLS, fileName);
        } else if (type === ExportTypes.ZIP) {
            var blob = new Blob([response.data], {
                type: "application/zip",
            });
            saveAs(blob, fileName + ".zip");
            return;
        } else if (type === ExportTypes.SVG) {
            return buildAndSaveFile(response, ExportTypes.SVG, fileName + ".svg");
        }

        var packet = { result: true, data: response.data };
        var uriPrefix = "data:attachment/csv;charset=utf-8,",
            blobType =  "application/octet-stream",
            linkType = "text/csv",
            data = response.data,
            fileExtension = ".csv";

		if (type ===  ExportTypes.JSON) {
			uriPrefix = "data:text/json;charset=utf-8,";
			blobType = "application/json";
            data = JSON.stringify(response.data);
            response.data = data;
			fileExtension = ".json";
		}

        var encodedUri = encodeURI(data),
            fileNameWithExtension = fileName + fileExtension,
            fullURI = uriPrefix + encodedUri,
            link = $document[0].createElement("a"),
            msCheck = $window.navigator.msSaveOrOpenBlob;

        var blob = new Blob([response.data], {
            type: blobType,
        });

        if (msCheck) {
            $window.navigator.msSaveOrOpenBlob(blob, fileNameWithExtension);
        }

		if (type !== ExportTypes.JSON) {
			link.setAttribute("type", linkType);
		}
        if (!msCheck && !_.isUndefined(link.download)) {
            link.setAttribute("href", fullURI);
            link.setAttribute("target", "_blank");
            link.setAttribute("download", fileNameWithExtension);
            document.body.appendChild(link);
            link.click();
            document.body.removeChild(link);
        } else {
            saveAs(blob, fileNameWithExtension);
        }

        return packet;
    };

    var CreateTable = function () {
        var push = function (arr, item) {
            if (_.isString(item)) {
                item = item.replace("\n", " ");
                arr.push("\"" + item + "\"");
            } else {
                arr.push(item);
            }
            return arr;
        };

        var headers = [],
            table_headers = angular.element("th");
        table_headers.each(function (cell_index, cell) {
            if (!(cell.classList.contains("ng-hide") || cell.innerText === $rootScope.t("Actions").toString())) {
                headers = push(headers, cell.innerText);
            }
        });

        var rows = [],
            table_rows = angular.element("tbody tr");
        table_rows.each(function (row_index, row) {
            var row_cells = [],
                cells = [].slice.call(row.cells);

            if (!row.classList.contains("ng-hide")) {
                _.each(cells, function (cell, cell_index) {
                    if (!_.isEmpty(cell.children) && !_.isEmpty(cell.children[0].children) && !_.isEmpty(cell.children[0].children[0].children)) {
                        var currentCell = cell.children[0].children[0].children[0],
                        cellIsDefined = !_.isUndefined(currentCell);
                    } else {
                        cellIsDefined = false;
                    }

                    if (cell.classList.contains("table-actions")) {
                        return;
                    }

                    if (cellIsDefined && currentCell.classList.contains("date")) {
                        row_cells = push(row_cells, (currentCell.attributes["uib-tooltip"]) ? currentCell.attributes["uib-tooltip"].value : cell.innerText);
                    } else if (cellIsDefined && currentCell.classList.contains("risk")) {
                        if (currentCell.classList.contains("very-high")) {
                            row_cells = push(row_cells, "Very High");
                        } else if (currentCell.classList.contains("high")) {
                            row_cells = push(row_cells, "High");
                        } else if (currentCell.classList.contains("medium")) {
                            row_cells = push(row_cells, "Medium");
                        } else if (currentCell.classList.contains("low")) {
                            row_cells = push(row_cells, "Low");
                        } else {
                            row_cells = push(row_cells, currentCell.innerText);
                        }
                    } else {
                        var text = (
                            (cellIsDefined && currentCell.attributes.getNamedItem("cell-text") && currentCell.attributes.getNamedItem("cell-text").value) ||
                            (!_.isEmpty(cell.children) && !_.isEmpty(cell.children[0].children) && cell.children[0].children[0].innerText)
                        );
                        row_cells = push(row_cells, text);
                    }
                });

                rows.push({
                    Records: row_cells
                });
            }
        });

        return {
            Headers: headers,
            Values: rows
        };
    };

    var buildAndSaveFile = function (data, filetype, filename) {
        if (filetype) {
            var type;

            if (filetype === ExportTypes.CSV) {
                type = "text/csv";
            } else if (filetype === ExportTypes.XLSX) {
                type = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            } else if (filetype === ExportTypes.XLS) {
                type = "application/vnd.ms-excel";
            } else if (filetype === ExportTypes.SVG) {
                type = "image/svg+xml";
            }

            var blob = new Blob([data], {
                type: type
            });
            saveAs(blob, filename);
        }
    };

    var buildFilename = function (filetype, filename) {
        var name;
        if (typeof filename == "undefined" || !filename) {
            name = "Report_" + moment().format("YYYY_MM_DD_HH_mm");
        } else {
            name = filename + "_" + moment().format("YYYY_MM_DD_HH_mm");;
        }
        if (filetype === ExportTypes.CSV) {
            name += ".csv";
        } else if (filetype === ExportTypes.XLSX) {
            name += ".xlsx";
        }
        return name;
    };

    var buildRequest = function (filetype, filename) {
        return _.assign({}, CreateTable(), {
            ExportType: filetype,
            ReportName: filename
        });
    };

    var buildEndpoint = function (reportType) {
        return "/report/export" + reportType;
    };

    var blobExport = function (endpoint, exportType, errorMessage, templateType, fileName, removeURI) {
        var httpConfig = {};
        if (templateType) {
            httpConfig.params = { types: templateType ? buildTemplateParams(templateType) : null };
        }
        httpConfig.removeURI = removeURI;
        if (exportType) {
            httpConfig.responseType = (exportType === ExportTypes.XLSX || exportType === ExportTypes.CSV || exportType === ExportTypes.XLS || exportType === ExportTypes.ZIP) ? "arraybuffer" : null;
        }
        return $http.get(endpoint, httpConfig).then(function successCallback(response) {
            blobHelper(response, exportType, fileName);
        }, function (errorResponse) {
            return errorCallback(errorResponse, errorMessage);
        });
    }

    function getFileNameFromHttpResponse(httpResponse) {
        if (!httpResponse || !httpResponse.headers) return "";
        var contentDispositionHeader = httpResponse.headers("Content-Disposition");
        if (!contentDispositionHeader) return "";
        var result = contentDispositionHeader
            .split(";")[1]
            .trim()
            .split("=")[1];
        return result.replace(/"/g, "");
    }

    function buildTemplateParams(templateType) {
        var defaultArray = [TemplateTypes.Custom, TemplateTypes.SelfService];
        var types = templateType ? [templateType] : defaultArray;

        switch (templateType) {
            case TemplateTypes.Custom:
            case TemplateTypes.SelfService:
                types = defaultArray;
                break;
            default:
                break;
        }

        return types;
    }

    function errorCallback(response, errorMessage) {
        var errorMessage = _.isUndefined(errorMessage) ? $rootScope.t("ErrorCreatingReportXlsx") : errorMessage;
        var packet = { result: false, data: errorMessage };
        if (errorMessage) {
            NotificationService.alertError($rootScope.t("Error"), errorMessage);
        }
        return packet;
    }

    return {
        exportReport: function (reportType, filetype, filename) {
            var excelTabname = filename || "Report_" + moment().format("YYYY_MM_DD_HH_mm");
            var data = buildRequest(filetype, excelTabname);
            var config = {
                responseType: "arraybuffer",
            };
            var endpoint = buildEndpoint(reportType);
            return $http.post(endpoint, data, config)
                .then(function successCallback(response) {
                    var extendedFilename = buildFilename(filetype, filename);
                    buildAndSaveFile(response.data, filetype, extendedFilename);
                },
                errorCallback);
        },
        xlsxProjectExport: function (name) {
            return blobExport(
                "/Assessment/DownloadAssessmentReports",
                ExportTypes.XLSX,
                $rootScope.t("ErrorDownloadingAssessmentReport", {
                    Assessment: $rootScope.t("Assessment")
                }),
                null,
                name
            );
        },
        pdfProjectExport: function (Id, Version, Name) {
            const exportModalData = {
                modalTitle: $rootScope.t("ReportDownload"),
                confirmationText: $rootScope.t("ReportIsGeneratedCheckTasksBar"),
            };
            ModalService.setModalData(exportModalData);
            ModalService.openModal("oneNotificationModalComponent");
            TaskPollingService.startPolling();
            return $http.get("/export/projectdetail?id=" + Id + "&version=" + Version + "&type=1")
                .then(
                //success
                function (response) {
                    $log.log("pdf export process executed.");
                },
                //error
                function (response) {
                    $log.log("pdf export request failed with status: " + response.status);
                });
        },
        CreateTable: CreateTable,
        xlsxImportTemplateExport: function (name, importType) {
            return blobExport(
                "/JobType/DownloadTemplate?jobType=" + importType,
                ExportTypes.XLSX,
                $rootScope.t("ErrorDownloadingImportTemplate"),
                null,
                name
            );
        },
        xlsxImportStatusExport: function (name, importId, type) {
            return blobExport(
                "/Job/DownloadError?guid=" + importId,
                type,
                $rootScope.t("ErrorDownloadingImportStatus"),
                null,
                name
            );
        },
        downloadExcel: function (url, name, removeUri) {
            return blobExport(
                url,
                ExportTypes.XLSX,
                "",
                null,
                name,
                removeUri
            );
        },
        downloadSVG: function (data, name) {
            return blobHelper(
                data,
                ExportTypes.SVG,
                name
            );
        },
        downloadFile: function (url, type, name, errorMessage) {
            return blobExport(url, type, errorMessage, null, name);
        },
        exportWithParams: function(url, type, name, params) {
            var excelTabname = buildFilename(type, name);
            var data = {
                ReportName: excelTabname,
                ExportType: type,
            };
            data = _.assign(data, params);
            var config = {
                responseType: "arraybuffer",
            };
            return $http.post(url, data, config)
                .then(function successCallback(response) {
                    buildAndSaveFile(response.data, type, excelTabname);
                },
                errorCallback);
        },
        basicFileDownload: function(data, fileName)  {

            var blob = new Blob([data], {
                type: "application/octet-stream",
            });
            saveAs(blob, fileName);
        },
    };
}

Export.$inject = ["$rootScope", "$http", "NotificationService", "$document", "$window", "$log", "ModalService", "TaskPollingService"];
