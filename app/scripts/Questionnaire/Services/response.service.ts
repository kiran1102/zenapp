import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import { IProtocolPacket, IProtocolMessages, IProtocolConfig } from "interfaces/protocol.interface";

export default class ResponseService {
    static $inject: string[] = ["$rootScope", "OneProtocol"];

    private translate: any;

    constructor(
        readonly $rootScope: IExtendedRootScopeService,
        readonly OneProtocol: any) {
            this.translate = $rootScope.t;
        }

    public list(projectId: string): ng.IPromise<IProtocolPacket> {
        const config: IProtocolConfig = this.OneProtocol.config("GET", `/response/get/${projectId}`);
        const messages: IProtocolMessages = { Error: { custom: this.translate("ErrorRetrievingResponses") } };
        return this.OneProtocol.http(config, messages);
    }

    public create(response: any): ng.IPromise<IProtocolPacket> {
        const config: IProtocolConfig = this.OneProtocol.config("POST", "/response/post", [], response);
        const messages: IProtocolMessages = { Error: { custom: this.translate("ErrorCreatingResponse") } };
        return this.OneProtocol.http(config, messages);
    }

    public read(id: string): ng.IPromise<IProtocolPacket> {
        const config: IProtocolConfig = this.OneProtocol.config("GET", `/response/getresponse/${id}`);
        const messages: IProtocolMessages = { Error: { custom: this.translate("ErrorRetrievingResponse") } };
        return this.OneProtocol.http(config, messages);
    }

    public update(response: any): ng.IPromise<IProtocolPacket> {
        const config: IProtocolConfig = this.OneProtocol.config("PUT", "/response/put", [], response);
        const messages: IProtocolMessages = { Error: { custom: this.translate("ErrorUpdatingResponse") } };
        return this.OneProtocol.http(config, messages);
    }

    public delete(id: string): ng.IPromise<IProtocolPacket> {
        const config: IProtocolConfig = this.OneProtocol.config("DELETE", `/response/delete/${id}`);
        const messages: IProtocolMessages = { Error: { custom: this.translate("ErrorDeletingResponse") }, Success: { custom: this.translate("ResponseDeleted") } };
        return this.OneProtocol.http(config, messages);
    }

    public createDataSystem(response: any): ng.IPromise<IProtocolPacket> {
        const config: IProtocolConfig = this.OneProtocol.config("POST", "/response/datasystem", [], response);
        const messages: IProtocolMessages = { Error: { custom: this.translate("ErrorCreatingResponse") }, Success: { custom: this.translate("ResponseSaved") } };
        return this.OneProtocol.http(config, messages);
    }

    public dataElementResponse(response: any): ng.IPromise<IProtocolPacket> {
        const config: IProtocolConfig = this.OneProtocol.config("POST", "/response/dataelement", [], response);
        const messages: IProtocolMessages = { Error: { custom: this.translate("ErrorCreatingResponse") }};
        return this.OneProtocol.http(config, messages);
    }

    public inventoryResponse(response: any): ng.IPromise<IProtocolPacket> {
        const config: IProtocolConfig = this.OneProtocol.config("POST", "/response/inventoryresponse", [], response);
        const messages: IProtocolMessages = { Error: { custom: this.translate("ErrorCreatingResponse") } };
        return this.OneProtocol.http(config, messages);
    }
}
