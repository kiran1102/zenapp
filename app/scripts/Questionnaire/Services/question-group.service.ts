declare var angular: any;
import Utilities from "Utilities";
import * as _ from "lodash";
import { ProtocolService } from "modules/core/services/protocol.service";
import { IProtocolPacket, IProtocolMessages, IProtocolConfig } from "interfaces/protocol.interface";
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";

export default class QuestionGroup {
    static $inject: string[] = ["Sections", "ENUMS", "$q", "$filter", "OneProtocol", "$rootScope"];

    private addQuestionToSection: any;
    private getQuestion: any;
    private q: any;
    private filter: any;
    private questionTypes: any;
    private protocol: any;
    private root: any;
    private httpActions: any;

    constructor(
        Sections: any,
        ENUMS: any,
        $q: ng.IQService,
        $filter: ng.IFilterService,
        OneProtocol: ProtocolService,
        $rootScope: IExtendedRootScopeService,
    ) {
        this.addQuestionToSection = Sections.addQuestion;
        this.getQuestion = Sections.getQuestion;
        this.q = $q;
        this.filter = $filter;
        this.questionTypes = ENUMS.QuestionTypes;
        this.protocol = OneProtocol;
        this.createGroup = this.createGroup.bind(this);
        this.addQuestionsToSection = this.addQuestionsToSection.bind(this);
        this.root = $rootScope;
    }

    public addQuestionsToSection = (version: number, questions: any, section: any, groupIndex: number): any => {
        const deferred = this.q.defer();
        const nextQuestionId = (_.isArray(section.QuestionGroups) && !_.isUndefined(section.QuestionGroups[groupIndex])) ? section.QuestionGroups[groupIndex].Questions[0].Id : "";
        let questionIndex = questions.length - 1;
        const questionIdArray = [];
        const recursivelyAddQuestion = (question: any, nextQId: any): any => {
            const data = {
                SectionId: section.Id,
                QuestionId: question.Id,
                NextQuestionId: nextQId,
                ReportFriendlyName: question.ReportFriendlyName,
                Version: version,
            };
            this.addQuestionToSection(data).then((res) => {
                if (res.result) {
                    questionIdArray.push(res.data);
                    if (questionIndex === 0) {
                        deferred.resolve({ result: true, data: questionIdArray.reverse() });
                    } else {
                        questionIndex--;
                        recursivelyAddQuestion(questions[questionIndex], res.data);
                    }
                } else {
                    deferred.reject();
                }
            });
        };
        recursivelyAddQuestion(questions[questionIndex], nextQuestionId);
        return deferred.promise;
    }

    public renderQuestionGroup(version: any, section: any, index: number, questionData: string[]): ng.IDeferred<IProtocolPacket> {
        const deferred = this.q.defer();
        const getQuestionPromises: Array<ng.IPromise<IProtocolPacket>> = _.map(questionData, (questionId: string): ng.IPromise<IProtocolPacket> => {
            return this.getQuestion(questionId, version).then((res: any): ng.IPromise<IProtocolPacket> => {
                if (res.data) { return res; }
            });
        });
        this.q.all(getQuestionPromises).then((r: IProtocolPacket): void => {
            const trueResults: IProtocolPacket[] = this.filter("filter")(r, { result: true });
            const createdQuestions: any[] = _.map(trueResults, (obj: any): void => obj.data);
            if (_.isArray(section.QuestionGroups) && section.QuestionGroups.length) {
                section.QuestionGroups.splice(index, 0, this.newGroupFromQuestionArray(createdQuestions, section.Id, createdQuestions[0].GroupId, this.questionTypes.ApplicationGroup));
            } else {
                section.QuestionGroups = [this.newGroupFromQuestionArray(createdQuestions, section.Id, createdQuestions[0].GroupId, this.questionTypes.ApplicationGroup)];
            }
            deferred.resolve({ result: true, data: createdQuestions });
        });
        return deferred.promise;
    }

    public newGroupFromQuestionArray(questionArray: any[], sectionId: any, id?: any, type?: any): any {
        return {
            GroupId: id,
            Type: type,
            SectionId: sectionId,
            Questions: questionArray,
        };
    }

    public arrangeQuestionsByGroup(sections: any[]): void {
        _.each(sections, (section: any): void => {
            if (_.isArray(section.Questions) && section.Questions.length) {
                const questionGroups = [];
                const foundGroupIds = [];
                const questionsCount: number = section.Questions.length;
                const question = section.Questions;
                for (let i = 0; i < questionsCount; i++) {
                    if (question[i].GroupId) {
                        const comparatorId: string = question[i].GroupId;
                        if ((foundGroupIds.indexOf(comparatorId) < 0)) {
                            // Store id for comparison to prevent creating duplicate groups
                            foundGroupIds.push(comparatorId);
                            const group = this.filter("filter")(section.Questions, { GroupId: comparatorId });
                            questionGroups.push(this.newGroupFromQuestionArray(group, section.Id, question[i].GroupId, this.questionTypes.ApplicationGroup));
                        }
                    } else {
                        questionGroups.push(this.newGroupFromQuestionArray([question[i]], section.Id));
                    }
                }
                section.QuestionGroups = questionGroups;
            }
            delete section.Questions;
        });
    }

    public createGroup = (name: string): any => {
        const groupData: any = {
            Name: name,
        };
        const config: IProtocolConfig = this.protocol.config("POST", "/questiongroup/add/", [], groupData);
        const messages: IProtocolMessages = { Error: { object: this.root.t("Group"), action: "create" } };
        return this.protocol.http(config, messages);
    }

}
