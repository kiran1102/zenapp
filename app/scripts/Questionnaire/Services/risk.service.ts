import _ from "lodash";
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import { NotificationService } from "modules/shared/services/provider/notification.service";

interface IRiskStatusFilter {
    Filtered: boolean;
    value: string;
    label: any;
    lookupKey: number;
}

interface IRiskStatusFilters {
    filters: IRiskStatusFilter[];
    allStatesEnabled: boolean;
}

export default class RiskService {

    static $inject: string[] = ["$rootScope", "$localStorage", "$http", "ENUMS", "NotificationService"];

    constructor(
        private readonly $rootScope: IExtendedRootScopeService,
        private readonly $localStorage: any,
        private readonly $http: any,
        private readonly ENUMS: any,
        private notificationService: NotificationService,
        ) {
    }

    // call to get Project by Id.
    public getPage(pagination: any): any {
        const params: any = {
            "pagination.pageNumber": pagination.page || 1,
            "pagination.pageSize": pagination.size || 10,
        };
        if (pagination.filters) {
            _.forEach(pagination.filters, (filter: any, index: number): void => {
                params[`filter[${index}].key`] = filter.name;
                params[`filter[${index}].value`] = filter.value;
            });
        }
        return this.$http.get("/risk/get/", {params}).then((response: any): any => {
            const formattedMetaData: any = this.formatPaginationConfig(response.data.Metadata);
            const data: any = {
                Content: response.data.Content,
                Metadata: formattedMetaData,
            };
            return {
                result: true,
                data,
            };
        },
        (response: any): any => {
            const alertText = this.$rootScope.t("CouldNotRetrieveRisk");
            this.notificationService.alertError(this.$rootScope.t("Error"), alertText);
            return {
                result: false,
                data: response.data.message,
            };
        });
    }

    // Updates the risk of a Project Question
    public updateRisk(risk: any): any {
        return this.$http.put("/risk/updatestate", risk)
            .then(
            (response: any): any => {
                return {
                    result: true,
                    data: response.data,
                };
            },
            (response: any): any => {
                const alertText = this.$rootScope.t("CouldNotUpdateRisk");
                this.notificationService.alertError(this.$rootScope.t("Error"), alertText);
                return {
                    result: false,
                    data: response.data.message,
                };
            });
    }

    // Accepts a risk
    public acceptRisk(Id: string): any {
        return this.$http.put("/risk/accept/" + Id)
            .then(
            (response: any): any => {
                return {
                    result: true,
                    data: response.data,
                };
            },
            (response: any): any => {
                const alertText = this.$rootScope.t("CouldNotAcceptRisk");
                this.notificationService.alertError(this.$rootScope.t("Error"), alertText);
                return {
                    result: false,
                    data: response.data.message,
                };
            });
    }

    // Unaccepts a risk
    public unacceptRisk(Id: string): any {
        return this.$http.put("/risk/unaccept/" + Id)
            .then(
            (response: any): any => {
                return {
                    result: true,
                    data: response.data,
                };
            },
            (response: any): any => {
                const alertText = this.$rootScope.t("CouldNotUnacceptRisk");
                this.notificationService.alertError(this.$rootScope.t("Error"), alertText);
                return {
                    result: false,
                    data: response.data.message,
                };
            });
    }

    public buildRiskStatesMap(recommendation: string): any {
        return {
            10: this.$rootScope.t("Identified"),
            20: this.$rootScope.t("RecommendationAdded", { Recommendation: recommendation }),
            30: this.$rootScope.t("Resolved"),
            40: this.$rootScope.t("ExceptionRequested"),
            50: this.$rootScope.t("Accepted"),
        };
    }

    public getRiskStateFilters(): IRiskStatusFilters {
        const riskStatesMap = this.buildRiskStatesMap(this.$rootScope.customTerms.Recommendation);

        if (this.$localStorage.riskStateFilters) {
            const localFilters = this.$localStorage.riskStateFilters.filters;

            const filters = localFilters.map((item: any) => {
                return {
                    Filtered: item.Filtered,
                    value: item.value,
                    label: riskStatesMap[item.lookupKey],
                    lookupKey: parseInt(item.lookupKey, 10),
                };
            });

            return {
                filters,
                allStatesEnabled: this.$localStorage.riskStateFilters.allStatesEnabled,
            };
        } else {
            const lookupKeys = Object.keys(riskStatesMap);

            const filters = lookupKeys.map((key: string): any => {
                return {
                    Filtered: true,
                    value: this.ENUMS.RiskStates[key],
                    label: riskStatesMap[key],
                    lookupKey: parseInt(key, 10),
                };
            });

            return {
                filters,
                allStatesEnabled: true,
            };
        }
    }

    public saveFiltersLocally(riskStateFilters: any): void {
        const riskFilters = riskStateFilters.filters;

        const filters = riskFilters.map((item: any): any => {
            return {
                Filtered: item.Filtered,
                value: item.value,
                lookupKey: parseInt(item.lookupKey, 10),
            };
        });
        this.$localStorage.riskStateFilters = {
            filters,
            allStatesEnabled: riskStateFilters.allStatesEnabled,
        };
    }

    public clearLocalFilters(): void {
        if (this.$localStorage.riskStateFilters) {
            delete this.$localStorage.riskStateFilters;
            const text = this.$rootScope.t("FiltersCleared");
            this.notificationService.alertSuccess(this.$rootScope.t("Success"), text);
        }
    }

    private formatPaginationConfig(metadata: any): any {
        return {
            first: metadata.PageNumber === 1,
            last: metadata.PageNumber === metadata.TotalPageCount,
            number: metadata.PageNumber - 1,
            numberOfElements: metadata.ItemCount,
            size: metadata.PageSize,
            totalElements: metadata.TotalItemCount,
            totalPages: metadata.TotalPageCount,
        };
    }

}
