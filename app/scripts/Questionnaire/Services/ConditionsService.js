
"use strict";

export default function Conditions($http, NotificationService, $rootScope) {

    return {
        list: listConditions,
        create: createCondition,
        read: readCondition,
        update: updateCondition,
        reorder: reorderCondition,
        delete: deleteCondition,
        batchDelete: batchDeleteConditions,
        operators: getOperators,
        createRiskCondition: createRiskCondition,
        readRiskCondition: readRiskCondition,
        updateRiskCondition: updateRiskCondition
    };

    // call to get all Conditions
    function listConditions(questionId, version) {
        var version = version ? version : "";
        return $http.get("/condition/getquestion/" + questionId + "/" + version)
            .then(function successCallback(response) {
                return {
                    result: true,
                    data: response.data
                };
            },
            function errorCallback(response) {
                var alertText = $rootScope.t("CouldNotRetrieveConditions");
                NotificationService.alertError($rootScope.t("Error"), alertText);
                return {
                    result: false,
                    data: alertText
                };
            },
            function progressCallback(progress) {
                return progress;
            });
    }

    // call to POST and create a new Condition
    function createCondition(condition) {
        return $http.post("/condition/addquestion/", condition)
            .then(function successCallback(response) {
                return {
                    result: true,
                    data: response.data
                };
            },
            function errorCallback(response) {
                var alertText = $rootScope.t("CouldNotCreateCondition");
                NotificationService.alertError($rootScope.t("Error"), alertText);
                return {
                    result: false,
                    data: alertText
                };
            },
            function progressCallback(progress) {
                return progress;
            });
    }

    // call to get Condition by id.
    function readCondition(id) {
        return $http.get("/condition/get/" + id)
            .then(function successCallback(response) {
                return {
                    result: true,
                    data: response.data
                };
            },
            function errorCallback(response) {
                var alertText = $rootScope.t("CouldNotGetCondition");
                NotificationService.alertError($rootScope.t("Error"), alertText);
                return {
                    result: false,
                    data: alertText
                };
            },
            function progressCallback(progress) {
                return progress;
            });
    }

    // call to UPDATE a Condition
    function updateCondition(condition) {
        return $http.put("/condition/put", condition)
            .then(function successCallback(response) {
                return {
                    result: true,
                    data: response.data
                };
            },
            function errorCallback(response) {
                var alertText = $rootScope.t("CouldNotUpdateCondition");
                NotificationService.alertError($rootScope.t("Error"), alertText);
                return {
                    result: false,
                    data: alertText
                };
            },
            function progressCallback(progress) {
                return progress;
            });
    }

    // call to Reorder a Condition
    function reorderCondition(condition) {
        return $http.put("/condition/reorder", condition)
            .then(function successCallback(response) {
                return {
                    result: true,
                    data: response.data
                };
            },
            function errorCallback(response) {
                var alertText = $rootScope.t("CouldNotReorderCondition");
                NotificationService.alertError($rootScope.t("Error"), alertText);
                return {
                    result: false,
                    data: alertText
                };
            },
            function progressCallback(progress) {
                return progress;
            });
    }

    // call to DELETE a Condition
    function deleteCondition(id, version) {
        var version = version ? version : "";
        return $http.delete("/condition/delete/" + id + "/" + version)
            .then(function successCallback(response) {
                return {
                    result: true,
                    data: response.data
                };
            },
            function errorCallback(response) {
                var alertText = $rootScope.t("CouldNotDeleteCondition");
                NotificationService.alertError($rootScope.t("Error"), alertText);
                return {
                    result: false,
                    data: alertText
                };
            },
            function progressCallback(progress) {
                return progress;
            });
    }

    // call to BATCH DELETE a Condition
    function batchDeleteConditions(arr, version) {
        var version = version ? version : "";
        return $http.post("/condition/delete/" + version, arr)
            .then(function successCallback(response) {
                return {
                    result: true,
                    data: response.data
                };
            },
            function errorCallback(response) {
                var alertText = $rootScope.t("CouldNotDeleteConditions");
                NotificationService.alertError($rootScope.t("Error"), alertText);
                return {
                    result: false,
                    data: alertText
                };
            },
            function progressCallback(progress) {
                return progress;
            });
    }

    function getOperators(questionTypeId) {
        var version = version ? version : "";
        return $http.get("/condition/getoperators/" + questionTypeId + "/" + version)
            .then(function successCallback(response) {
                return {
                    result: true,
                    data: response.data
                };
            },
            function errorCallback(response) {
                var alertText = $rootScope.t("CouldNotRetrieveConditionsOperators");
                NotificationService.alertError($rootScope.t("Error"), alertText);
                return {
                    result: false,
                    data: alertText
                };
            },
            function progressCallback(progress) {
                return progress;
            });
    }

    function createRiskCondition(condition) {
        return $http.post("/condition/addrisk/", condition)
            .then(function successCallback(response) {
                return {
                    result: true,
                    data: response.data
                };
            },
            function errorCallback(response) {
                var alertText = $rootScope.t("CouldNotCreateRiskCondition");
                NotificationService.alertError($rootScope.t("Error"), alertText);
                return {
                    result: false,
                    data: alertText
                };
            },
            function progressCallback(progress) {
                return progress;
            });
    }

    function readRiskCondition(id, version) {
        return $http.get("/condition/getrisk/" + id + "/" + version)
            .then(function successCallback(response) {
                return {
                    result: true,
                    data: response.data
                };
            },
            function errorCallback(response) {
                var alertText = $rootScope.t("CouldNotGetRiskCondition");
                NotificationService.alertError($rootScope.t("Error"), alertText);
                return {
                    result: false,
                    data: alertText
                };
            },
            function progressCallback(progress) {
                return progress;
            });
    }

    function updateRiskCondition(condition) {
        return $http.put("/condition/updaterisk/", condition)
            .then(function successCallback(response) {
                return {
                    result: true,
                    data: response.data
                };
            },
            function errorCallback(response) {
                var alertText = $rootScope.t("CouldNotUpdateRiskCondition");
                NotificationService.alertError($rootScope.t("Error"), alertText);
                return {
                    result: false,
                    data: alertText
                };
            },
            function progressCallback(progress) {
                return progress;
            });
    }

}

Conditions.$inject = ["$http", "NotificationService", "$rootScope"];
