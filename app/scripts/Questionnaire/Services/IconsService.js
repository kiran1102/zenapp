
export default function Icons() {

	var topIcons = ["fa-archive", "fa-ban", "fa-child", "fa-code", "fa-cog", "fa-copyright", "fa-desktop", "fa-envelope", "fa-exclamation", "fa-gavel", "fa-home", "fa-info", "fa-lock", "fa-unlock", "fa-mobile", "fa-shopping-cart", "ot-star"];

	return {
		topList: function () {
			return topIcons;
		}
	};
}
