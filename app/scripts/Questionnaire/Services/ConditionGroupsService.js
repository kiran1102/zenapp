"use strict";

export default function ConditionGroups($rootScope, $http, NotificationService, $q) {

    /**
    @name successCallback
    @param {Response} response A success response.
    @returns {ResponsePacket} packet Formatted response.
    @description Takes in a success response and return a packet containing the response result,
    response status, response data, and the response time.
    @example successCallback(response)
    **/
    function successCallback() {
        return function (response) {
            var packet = {
                result: true,
                status: response.status,
                data: response.data ? response.data : [],
                time: (new Date()).getTime()
            };
            return packet;
        };
    }

    /**
    @name errorCallback
    @param {Response} response An error response.
    @param {String} errorMsg Optional Error message.
    @returns {ResponsePacket} packet Formatted response.
    @description Takes in an error response and return a packet containing the response result,
    response status, response data / error message, and the response time.
    @example errorCallback(response)
    **/
    function errorCallback(errorMsg) {
        return function (response) {
            var packet = {
                result: false,
                status: response.status,
                data: (response.data ? response.data.Message : (errorMsg || "")) || (errorMsg || ""),
                time: (new Date()).getTime()
            };
            if (packet.data) {
                NotificationService.alertWarning($rootScope.t("Warning"), packet.data);
            }
            return packet;
        };
    }

    /**
    @name errorCallback
    @param {Progress} progress Progress of promise.
    @returns {Progress} progress Progress of promise.
    @description Takes the progress of the action and returns it.
    @example progressCallback(progress)
    **/
    function progressCallback() {
        return function (progress) {
            return progress;
        };
    }

    /**
    * @name list
    * @param {String} templateQuestionId The Id of the Template question.
    * @param {String} templateVersion The Version of the Template.
    * @returns {Promise} Promise_ConditionGroups Returns a promise that will
    * contain a response object with a data property
    * which contains a list of Condition Groups.
    * @description Get a list of condition groups.
    * @example ConditionGroups.list(templateQuestionId)
    **/
    function getListByQuestion(templateQuestionId, templateVersion) {
        return $http.get("/ConditionGroup/Get?templateQuestionId=" + templateQuestionId + "&version=" + templateVersion).then(// + '?version='+templateVersion).then(
            successCallback(),
            errorCallback("Could not retrieve conditions.")
        );
    }

    /**
    @name create
    @param {Array} conditionGroupCreateRequests An array of condition group create requests.
    @returns {Promise} Promise_CreateResult Returns a promise that will
    contain a response object with a data property
    which contains the result of the bulk create request.
    @description Create condition groups in bulk.
    @example ConditionGroups.create(conditionGroupCreateRequests)
        [
            {
                'Id': '00000000-0000-0000-0000-000000000000',
                'Version': 1,
                'TemplateQuestionId': '00000000-0000-0000-0000-000000000000'
                'NextConditionGroupId': '00000000-0000-0000-0000-000000000000'
                'Operator': 1, // AND = 1, OR = 2
                'Conditions': [
                    // An array of condition create requests.
                ],
            },
        ]
    **/
    function createGroupsOnQuestion(conditionGroupCreateRequests) {
        return $http.post("/ConditionGroup/Add", conditionGroupCreateRequests).then(
            successCallback(),
            errorCallback("Could not create conditions.")
        );
    }

    /**
    @name update
    @param {Array} conditionGroupUpdateRequests An array of condition group create requests.
    @returns {Promise} Promise_CreateResult Returns a promise that will
    contain a response object with a data property
    which contains the result of the bulk create request.
    @description Create condition groups in bulk.
    @example ConditionGroups.create(conditionGroupCreateRequests)
        [
            {
                'Id': '00000000-0000-0000-0000-000000000000',
                'TemplateQuestionId': '00000000-0000-0000-0000-000000000000'
                'NextConditionGroupId': '00000000-0000-0000-0000-000000000000'
                'Operator': 1, // AND = 1, OR = 2
            },
        ]
    **/
    function updateGroupsOnQuestion(conditionGroupUpdateRequests) {
        return $http.put("/ConditionGroup/Put", conditionGroupUpdateRequests).then(
            successCallback(),
            errorCallback()
        );
    }

    /**
    @name addConditions
    @param {Array} conditionCreateRequests
    @returns {Promise} Promise_CreateResult Returns a promise that will
    contain a response object with a data property
    which contains the result of the bulk create request.
    @description Add condition to an existing group in bulk.
    @example ConditionGroups.addConditions(conditionCreateRequests)
        [
            {
                'ConditionGroupId': '00000000-0000-0000-0000-000000000000',
                'Conditions': [
                    // An array of normal condition create requests.
                ]
            }
        ]
    **/
    function addConditionsToGroup(conditionCreateRequests) {
        return $http.put("/ConditionGroup/AddConditions", conditionCreateRequests).then(
            successCallback(),
            errorCallback("Could not create condition terms.")
        );
    }

    /**
    @name updateConditions
    @param {Array} conditionUpdateRequests
    @returns {Promise} Promise_UpdateResult Returns a promise that will
    contain a response object with a data property
    which contains the result of the bulk update request.
    @description Update condition on an existing group in bulk.
    @example ConditionGroups.updateConditions(conditionUpdateRequests)
        [
            {
                'ConditionGroupId': '00000000-0000-0000-0000-000000000000',
                'Conditions': [
                    // An array of normal condition update requests.
                ]
            }
        ]
    **/
    function updateConditionsOnGroup(conditionUpdateRequests) {
        return $q.all(conditionUpdateRequests.map(function (updateRequest) {
            return $http.put("/condition/put", updateRequest);
        }))
            .then(
            successCallback(),
            errorCallback("Could not update condition terms"),
            progressCallback());
    }

    /**
    @name removeConditions
    @param {Array} conditionsDeleteRequests
    @returns {Promise} Promise_DeleteResult Returns a promise that will
    contain a response object with a data property
    which contains the result of the bulk delete request.
    @description Remove conditions from an existing group in bulk.
    @example ConditionGroups.removeConditions(conditionsDeleteRequests)
        [
            {
                'ConditionGroupId': '00000000-0000-0000-0000-000000000000',
                'Conditions': [
                    '00000000-0000-0000-0000-000000000000',
                    '00000000-0000-0000-0000-000000000000'
                ]
            }
        ]
    **/
    function removeConditionsFromGroup(conditionsDeleteRequests) {
        return $http.put("/ConditionGroup/RemoveConditions", conditionsDeleteRequests).then(
            successCallback(),
            errorCallback("Could not remove condition terms.")
        );
    }

    /**
    @name delete
    @param {Array} conditionGroupIds
    @returns {Promise} Promise_DeleteResult Returns a promise that will
    contain a response object with a data property
    which contains the result of the bulk delete request.
    @description Remove condition groups in bulk.
    @example ConditionGroups.delete(conditionGroupIds)
        [
            '00000000-0000-0000-0000-000000000000',
            '00000000-0000-0000-0000-000000000000'
        ]
    **/
    function deleteGroupsFromQuestion(conditionGroupIds) {
        return $http.post("/ConditionGroup/Delete", conditionGroupIds).then(
            successCallback(),
            errorCallback("Could not remove conditions.")
        );
    }

    var service = {
        list: getListByQuestion,
        create: createGroupsOnQuestion,
        update: updateGroupsOnQuestion,
        addConditions: addConditionsToGroup,
        updateConditions: updateConditionsOnGroup,
        removeConditions: removeConditionsFromGroup,
        delete: deleteGroupsFromQuestion
    };

    return service;

}

ConditionGroups.$inject = ["$rootScope", "$http", "NotificationService", "$q"];
