declare var angular: any;

export default class ApplicationQuestions {
    static $inject: string[] = ["ENUMS"];

    private questionTypes: any;

    constructor(ENUMS: any) {
        this.questionTypes = ENUMS.QuestionTypes;
    }

    public getQuestions = (): any[] => {
        return [
            {
                AllowNotApplicable: false,
                AllowNotSure: false,
                AllowOther: true,
                Hint: "Data elements are collected at the application level",
                Multiline: true,
                Name: "Enter the application name that supports the processing activity",
                ReportFriendlyName: "Application Name",
                Required: true,
                Text: null,
                Type: this.questionTypes.Application,
            },
            {
                AllowJustification: true,
                AllowNotApplicable: false,
                AllowNotSure: true,
                AllowOther: true,
                Hint: "",
                MultiSelect: false,
                Name: "Is it an in-house or 3rd party application?",
                Options: [
                    {
                        Description: "",
                        Name: "In-House",
                        Value: 0,
                    },
                    {
                        Description: "",
                        Name: "3rd Party",
                        Value: 1,
                    },
                ],
                ReportFriendlyName: "In House vs 3rd Party",
                RequireJustification: false,
                Required: false,
                Text: null,
                Type: this.questionTypes.Multichoice,
            },
            {
                AllowNotApplicable: false,
                AllowNotSure: true,
                Hint: "",
                Multiline: true,
                Name: "Name the country where the application is hosted",
                ReportFriendlyName: "Application Host Country",
                Required: true,
                Text: null,
                Type: this.questionTypes.Location,
            },
            {
                AllowJustification: false,
                AllowNotApplicable: false,
                AllowNotSure: true,
                Hint: "",
                Name: "Is the application hosted on cloud?",
                ReportFriendlyName: "Hosted on Cloud?",
                RequireJustification: false,
                Required: false,
                Text: null,
                Type: this.questionTypes.YesNo,
            },
            {
                AllowNotApplicable: false,
                AllowNotSure: true,
                AllowOther: true,
                Hint: "",
                Multiline: true,
                Name: "Name the Cloud Provider hosting the application",
                ReportFriendlyName: "Cloud Provider",
                Required: false,
                Text: null,
                Type: this.questionTypes.Provider,
            },
            {
                AllowNotApplicable: false,
                AllowNotSure: true,
                AllowOther: true,
                Hint: "",
                Multiline: true,
                Name: "Who knows about this application?",
                ReportFriendlyName: "App IT Owner",
                Required: false,
                Text: "Select a user from the list or enter a valid email address.",
                Type: this.questionTypes.User,
            },
        ];
    }

    public getQuestionsIndex(): any {
        return {
            Application: 0,
            Cloud: 3,
            Location: 2,
            Owner: 5,
            Provider: 4,
            Type: 1,
        };
    }

}
