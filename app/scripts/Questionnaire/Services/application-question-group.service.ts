declare var angular: any;
import Utilities from "Utilities";
import * as _ from "lodash";

export default class ApplicationQuestionGroup {
    static $inject: string[] = ["$q", "ENUMS", "Questions", "QuestionGroup", "ApplicationQuestions", "ConditionGroups"];

    private q: ng.IQService;

    private ResponseTypes: any;
    private ConditionOperators: any;
    private ConditionComparators: any;

    private createQuestion: any;
    private createGroup: any;
    private addToSection: any;

    private getQuestionsData: any;
    private getQuestionsIndex: any;

    private createConditions: any;

    constructor($q: ng.IQService, ENUMS: any, Questions: any, QuestionGroup: any, ApplicationQuestions: any, ConditionGroups: any) {
        this.q = $q;

        this.ResponseTypes = ENUMS.ResponseTypes;
        this.ConditionOperators = ENUMS.ConditionOperatorsTypes;
        this.ConditionComparators = ENUMS.ConditionComparatorTypes;

        this.createQuestion = Questions.create;
        this.createGroup = QuestionGroup.createGroup;
        this.addToSection = QuestionGroup.addQuestionsToSection;

        this.getQuestionsData = ApplicationQuestions.getQuestions;
        this.getQuestionsIndex = ApplicationQuestions.getQuestionsIndex;

        this.createConditions = ConditionGroups.create;

        this.createApplicationConditions = this.createApplicationConditions.bind(this);
    }

    public createApplicationGroup(version: number, section: any, type: number, index: number): any {
        const deferred = this.q.defer();
        const nextQuestionId = (_.isArray(section.QuestionGroups) && !_.isUndefined(section.QuestionGroups[index])) ? section.QuestionGroups[index].Questions[0].Id : "";
        this.createGroup("Application").then((groupResponse) => {
            if (groupResponse.result) {
                const questionData = this.getQuestionsData();
                const createQuestionPromises = [];
                _.each(questionData, (question: any): void => {
                    question.QuestionGroupId = groupResponse.data;
                    createQuestionPromises.push(this.createQuestion(question));
                });
                this.q.all(createQuestionPromises).then((responses) => {
                    let promiseSuccess = true;
                    if (_.isArray(responses) && responses.length) {
                        _.each(responses, (response: any, responseIndex: number): void => {
                            if (response.result) {
                                questionData[responseIndex].Id = response.data;
                            } else {
                                promiseSuccess = false;
                            }
                        });
                    }
                    if (promiseSuccess) {
                        this.addToSection(version, questionData, section, index).then(
                            (questionResponse) => {
                                if (questionResponse.result) {

                                    this.createApplicationConditions(version, questionResponse.data).then(() => {
                                        deferred.resolve(questionResponse);
                                    });
                                } else {
                                    deferred.resolve(questionResponse);
                                }
                            },
                        );
                    } else {
                        deferred.reject();
                    }
                });
            } else {
                deferred.reject();
            }
        });
        return deferred.promise;
    }

    private createApplicationConditions(version: number, questionData: string[]): any {
        const deferred = this.q.defer();
        if (questionData) {
            const questionIndexes = this.getQuestionsIndex();
            const cloudId = questionData[questionIndexes.Cloud];
            const providerId = questionData[questionIndexes.Provider];
            const ownerId = questionData[questionIndexes.Owner];

            // Term: is Equal To No
            const termEqualNo = this.createConditionTerm(version, this.ConditionComparators.Equals, false, this.ResponseTypes.Value);
            // Term: is Not Sure
            const termNotSure = this.createConditionTerm(version, this.ConditionComparators.NotSure, null, this.ResponseTypes.NotSure);

            // If [Cloud Question Response] is [Equal To] [No] [OR] is [Not Sure], then [Skip To] [Owner Question].
            const cloudSkipCondition = this.createCondition(version, cloudId, ownerId, this.ConditionOperators.Or, [termEqualNo, termNotSure]);

            this.createConditions([cloudSkipCondition]).then((cloudResponse) => {
                deferred.resolve();
            });
        } else {
            deferred.resolve();
        }
        return deferred.promise;
    }

    private createCondition(version: number, qId: string, targetId: string, operator: any, terms: any[]): any {
        return {
            Id: "",
            NextConditionGroupId: "",
            Version: version,
            TemplateQuestionId: qId,
            SkipResult: {
                TargetQuestionId: targetId,
            },
            Operator: operator,
            Conditions: terms,
        };
    }

    private createConditionTerm(version: number, comparator: any, operand: any, responseType: number): any {
        return {
            ConditionGroupId: "",
            Id: "",
            Version: version,
            Operator: comparator,
            Value: operand,
            ResponseType: responseType,
        };
    }

}
