﻿import { find, isArray, isUndefined, isNumber, forEach, isEqual, assign } from "lodash";
import { RiskStates } from "enums/risk-states.enum";
import { QuestionStates } from "enums/assessment-question.enum";
import { RiskLevels } from "enums/risk.enum";
import { MultiChoiceTypes } from "enums/question-types.enum";

export default function Answer($rootScope, ENUMS, Response, $filter, GenFactory, Assessment) {

	var questionTypes = ENUMS.QuestionTypes,
		responseTypes = ENUMS.ResponseTypes,
		riskLabels = ENUMS.RiskLabels,
        multichoiceTypes = ENUMS.MultiChoiceTypes, // button and dropdown
        multiChoiceAnswerTypes = MultiChoiceTypes,
	    translate = $rootScope.t;

	return {
        createResponse: function (projectId, Question) {
            var responsePromise = {},
                vm = this;

            var newResponse = {
                HasVersionResponseChanged: true,
                QuestionId: Question.Id,
                Responses: []
            };

            var addResponse = function (value, type) {
                newResponse.Responses.push({
                    Type: type,
                    Value: value
                });
            };

            var isValidQuestionType = (Question.ResponseType === responseTypes.Other && Question.OtherAnswer !== (null || undefined || "")) ||
                (Question.ResponseType === responseTypes.NotSure) ||
                (Question.ResponseType === responseTypes.Justification) ||
                (Question.ResponseType === responseTypes.NotApplicable);

            var isValidAnswer = (Question.Answer !== null) && (!isUndefined(Question.Answer)) && (Question.Answer !== "");

            if (isValidAnswer || isValidQuestionType) {

                if ((Question.ResponseType !== responseTypes.NotApplicable) && (Question.ResponseType !== responseTypes.Justification)) {
                    Question.NotApplicable = false;
                }

                if ((Question.ResponseType !== responseTypes.NotSure) && (Question.ResponseType !== responseTypes.NotApplicable) && !Question.NotApplicable) {
                    switch (Question.Type) {
                        case questionTypes.Content:
                            break;
                        case questionTypes.TextBox:
                            if (Question.Answer) addResponse(Question.Answer, responseTypes.Value);
                            break;
                        case questionTypes.Multichoice:
                            // MutliSelect Answers (Not including 'Other')
                            if (Question.MultiSelect) {
                                var multiValues = GenFactory.removeNulls(Question.Answer);
                                forEach(multiValues, function(value) {
                                    addResponse(value, responseTypes.Reference);
                                });
                            } else {
                                if (Question.Answer) addResponse(Question.Answer, responseTypes.Reference);
                            }
                            break;
                        case questionTypes.YesNo:
                            if (Question.Answer !== null) addResponse(Question.Answer, responseTypes.Value);
                            break;
                        case questionTypes.DateTime:
                            if (Question.Answer !== null) addResponse(Question.Answer, responseTypes.Value);
                            break;
                        case questionTypes.Application:
                        case questionTypes.User:
                        case questionTypes.Location:
                        case questionTypes.Provider:
                            if (Question.Answer !== null) {
                                if (Question.Answer.Value) {
                                    addResponse(Question.Answer.Value, $scope.ResponseTypes.Reference);
                                } else {
                                    addResponse(Question.Answer, $scope.ResponseTypes.Other);
                                }
                            }
                            break;
                    };

                    if (Question.ResponseType === responseTypes.Other || Question.MultiSelect) {
                        if (Question.OtherAnswer) addResponse(Question.OtherAnswer, responseTypes.Other);
                    }
                } else if (Question.ResponseType === responseTypes.NotSure) {
                    addResponse("", responseTypes.NotSure);
                } else {
                    addResponse("", responseTypes.NotApplicable);
                }
            }

            if (Question.Justification && newResponse.Responses.length) {
                addResponse(Question.Justification, responseTypes.Justification);
            } else {
                Question.Justification = null;
            }

            responsePromise = Response.create(newResponse);

            return {
                promise: responsePromise,
                response: newResponse
            };
        },
        fixStates: function (scope, section) {
            if (!section) return;
            var sectionQuestionsLen = section.Questions.length;
            for (var i = 0; i < sectionQuestionsLen; i++) {
                if (!section.Questions[i].IsSkipped) {
                    if (section.Questions[i].State === QuestionStates.Skipped) {
                        if (section.Questions[i].ResponseType !== responseTypes.NotSure && (section.Questions[i].Answer === "" || isUndefined(section.Questions[i].Answer) || section.Questions[i].Answer === null)) {
                            section.Questions[i].State = QuestionStates.Unanswered;
                        } else {
                            section.Questions[i].State = QuestionStates.Answered;
                        }
                    }
                } else {
                    if (section.Questions[i].State !== QuestionStates.Skipped) {
                        section.Questions[i].State = QuestionStates.Skipped;
                    }
                }
            }
        },
        handleQuestionResponse: function (scope, Dictionary, questionRef, SectionQuestionProps) {
            var vm = this;
            var assessmentRef = scope.isReadinessTemplate ? scope.Assessment : scope.Project;
            if (isArray(SectionQuestionProps)) {
                forEach(SectionQuestionProps, function(sectionStates) {
                    var sectionRef = find(assessmentRef.Sections, { Id: sectionStates.Id });
                    if (sectionStates.Questions) {
                        forEach(sectionStates.Questions, function(questionProps) {
                            var questionName = questionProps.Name,
                                questionState = questionProps.State,
                                questionResponses = questionProps.Responses;

                            var question = find(sectionRef.Questions, { Id: questionProps.Id });

                            if (question) {
                                if (questionName) {
                                    question.Name = questionName;
                                }

                                if (!isUndefined(questionState)) {
                                    question.State = questionState;
                                    if (questionState === QuestionStates.Skipped) {
                                        question.IsSkipped = true;
                                        question.Responses = [];
                                        question.Answer = null;
                                    } else {
                                        question.IsSkipped = false;
                                    }
                                }

                                if (isNumber(questionProps.Risk)) {
                                    question.Risk = assign({}, question.Risk, { Level: questionProps.Risk, State: RiskStates.Identified });
                                } else {
                                    question.Risk = null;
                                }

                                if (question.Id !== questionRef.Id && isArray(questionResponses)) {
                                    vm.handleResponses(scope, vm.removeAnswers(scope, question), questionResponses);
                                }
                            }
                        });
                    }
                });
            }
            forEach(assessmentRef.Sections, function(section) {
                if (!section) return;

                if (section.Questions) {
                    var skippedQuestions = $filter("filter")(section.Questions, {
                        IsSkipped: true
                    }).length;
                    section.IsSkipped = !!(skippedQuestions && skippedQuestions === section.Questions.length);
                } else {
                    section.IsSkipped = true;
                }
            });
        },
        getAnswerString: function (scope, question) {
            var answerArray = [],
                vm = this;
            if (question.Responses) {
                switch (question.Type) {
                    case questionTypes.Content:
                        break;
                    case questionTypes.TextBox:
                        vm.renderSingleAnswer(question, scope, "textArea");
                        answerArray.push(question.Answer);
                        break;
                    case questionTypes.Multichoice:
                        question.MultiSelect ? vm.renderMultiSelectAnswers(scope, question) : vm.renderMultiChoiceAnswers(scope, question);
                        if (question.Answer) {
                            answerArray.push(vm.getMultichoiceAnswerString(question, scope));
                        }
                        break;
                    case questionTypes.YesNo:
                        vm.renderBooleanAnswers(scope, question);
                        if (question.Answer) {
                            answerArray.push(translate("Yes"));
                        } else {
                            answerArray.push(translate("No"));
                        }
                        break;
                    case questionTypes.DateTime:
                        vm.renderSingleAnswer(scope, question, "date");
                        answerArray.push(question.Answer);
                        break;
                    case questionTypes.DataElement:
                        answerArray.push(vm.getInventoryAnswerString(question, scope));
                        break;
                    case questionTypes.Application:
                    case questionTypes.Location:
                    case questionTypes.User:
                    case questionTypes.Provider:
                        answerArray.push(vm.getMultichoiceAnswerString(question, scope));
                        break;
                    default:
                }

                if (question.AllowNotSure && question.ResponseType === responseTypes.NotSure) {
                    answerArray = [translate("NotSure")];
                }

                if (question.AllowNotApplicable && question.ResponseType === responseTypes.NotApplicable) {
                    answerArray = [translate("NotApplicable")];
                }

                if (question.AllowOther && question.OtherAnswer) {
                    answerArray.push(question.OtherAnswer);
                }
            } else if (question.State === QuestionStates.Skipped) {
                answerArray = [translate("Skipped")];
            }
            else {
                answerArray = [translate("Unanswered")];
            }
            var answerString = answerArray.join(", ");
            return answerString;
        },
        getMultichoiceAnswerString: function (question) {
            var answerArray = [];

            if (isArray(question.Options)) {
                forEach(question.Options, function(option) {
                    if (option.Answer) answerArray.push(option.Name);
                });
            }
            return answerArray.join(", ");
        },
        getQuestions: function (scope, Section, Dictionary) {
            var vm = this;
            forEach(Section.Questions, function (question) {
                Dictionary.Questions[question.Id] = question;
                question.IsSkipped = (question.State === QuestionStates.Skipped);

                // Set Default Response Type Based on Type (?)
                switch (question.Type) {
                    case null:
                        question.Type = questionTypes.Content;
                        question.DefaultResponseType = responseTypes.Value;
                        break;
                    case questionTypes.Multichoice:
                    case questionTypes.Application:
                    case questionTypes.Location:
                    case questionTypes.User:
                    case questionTypes.Provider:
                        question.DefaultResponseType = responseTypes.Reference;
                        break;
                    default:
                        question.DefaultResponseType = responseTypes.Value;
                }

                vm.handleResponses(scope, question);
            });
        },
        getSections: function (scope, sections, Dictionary) {
            var vm = this;
            forEach(sections, function(Section) {
                Dictionary.Sections[Section.Id] = Section;
                if (!isUndefined(Section.Questions && Section.Questions.length > 0)) {
                    // Loop through Questions
                    vm.getQuestions(scope, Section, Dictionary);

                    var hide = true;
                    // If section has atleast 1 unskipped question, then show.
                    for (var i = 0; i < Section.Questions.length; i++) {
                        if (!Section.Questions[i].IsSkipped) {
                            hide = false;
                        }
                    }
                    Section.IsSkipped = hide;
                    vm.fixStates(scope, Section);
                    Section.FilteredQuestions = vm.setFilters(scope, Section);
                }
            });
        },
        handleResponses: function (scope, Question, responses) {
            var vm = this;
            Question.Responses = (isArray(responses) ? responses : (Question.Responses || []));

            // Render answers based on Question Type
            switch (Question.Type) {
                case questionTypes.YesNo:
                    vm.renderBooleanAnswers(scope, Question);
                    break;
                case questionTypes.Multichoice:
                    Question.MultiSelect ? vm.renderMultiSelectAnswers(scope, Question) : vm.renderMultiChoiceAnswers(scope, Question);
                    break;
                case questionTypes.TextBox:
                    vm.renderSingleAnswer(scope, Question, "textArea");
                    break;
                case questionTypes.DateTime:
                    vm.renderSingleAnswer(scope, Question, "date");
                    break;
                case questionTypes.Application:
                case questionTypes.Location:
                case questionTypes.User:
                case questionTypes.Provider:
                    vm.renderInventoryAnswers(scope, Question);
                    break;
                default: // Default : 0
            }
            return Question;
        },
        removeAnswers: function (scope, Question) {
            Question.Responses = [];
            Question.Answer = null;
            if (Question.AllowOther) {
                Question.OtherAnswer = null;
            }
            if (Question.AllowJustification) {
                Question.Justification = null;
            }
            Question.ResponseType = Question.DefaultResponseType;
            return Question;
        },
        renderBooleanAnswers: function (scope, Question) {
            forEach(Question.Responses, function(response) {
                switch (response.Type) {
                    case responseTypes.NotSure:
                        Question.Answer = null;
                        Question.ResponseType = responseTypes.NotSure;
                        break;
                    case responseTypes.NotApplicable:
                        Question.Answer = null;
                        Question.NotApplicable = true;
                        Question.ResponseType = responseTypes.NotApplicable;
                        break;
                    case responseTypes.Justification:
                        Question.Justification = response.Value;
                        break;
                    default:
                        Question.DefaultResponseType = responseTypes.Value;
                        if (response.Value === null) {
                            Question.Answer = null;
                        } else {
                            if (typeof response.Value === "boolean") {
                                Question.Answer = response.Value;
                            } else {
                                Question.Answer = (response.Value === ("true" || "True"));
                            }
                        }
                }
            });
        },
        renderMultiChoiceAnswers: function (scope, Question) {
            forEach(Question.Responses, function (response) {
                switch (response.Type) {
                    case responseTypes.NotSure:
                        Question.Answer = null;
                        Question.ResponseType = responseTypes.NotSure;
                        break;
                    case responseTypes.NotApplicable:
                        Question.Answer = null;
                        Question.NotApplicable = true;
                        Question.ResponseType = responseTypes.NotApplicable;
                        break;
                    case responseTypes.Other:
                        if (Question.OptionListTypeId === multiChoiceAnswerTypes.AssetDiscovery){
                            Question.AnswerName = response.Value;
                        } else {
                            Question.Answer = null;
                            Question.OtherAnswer = response.Value;
                        }
                        break;
                    case responseTypes.Justification:
                        Question.Justification = response.Value;
                        break;
                    default:
                        Question.DefaultResponseType = responseTypes.Reference;
                        Question.Answer = null;
                        if (response.Value !== null && Question.OptionListTypeId !== multiChoiceAnswerTypes.AssetDiscovery) {
                            forEach(Question.Options, function (option) {
                                if (String(option.Value) === String(response.Value)) {
                                    Question.Answer = String(option.Value);
                                    return false;
                                }
                            });
                        }
                        if (response.Value !== null && Question.OptionListTypeId === multiChoiceAnswerTypes.AssetDiscovery) {
                            forEach(Question.Options, function (option) {
                                if (String(option.Value) === String(response.Value)) {
                                    Question.AnswerName = String(option.Name);
                                    return false;
                                }
                            });
                        }
                }
            });
        },
        renderMultiSelectAnswers: function (scope, Question) {
            forEach(Question.Responses, function(response) {
                switch (response.Type) {
                    case responseTypes.NotSure:
                        Question.Answer = null;
                        Question.ResponseType = responseTypes.NotSure;
                        break;
                    case responseTypes.NotApplicable:
                        Question.Answer = null;
                        Question.NotApplicable = true;
                        Question.ResponseType = responseTypes.NotApplicable;
                        break;
                    case responseTypes.Other:
                        Question.OtherAnswer = response.Value;
                        break;
                    case responseTypes.Justification:
                        Question.Justification = response.Value;
                        break;
                    default:
                        Question.DefaultResponseType = responseTypes.Reference;
                        Question.Answer = Question.Answer || [];
                        if ((response.Value !== null) && (response.Value !== [null])) {
                            Question.Answer.push(response.Value.toString());
                            forEach(Question.Options, function (option, index) {
                                if (response.Value === option.Value) option.Answer = true;
                            });
                        }
                }
            });
        },
        renderSingleAnswer: function (scope, Question, type) {
            if (isArray(Question.Responses) && Question.Responses.length) {
                var response = Question.Responses[0];
                if (isEqual(response.Type, responseTypes.NotSure)) {
                    Question.Answer = null;
                    Question.ResponseType = responseTypes.NotSure;
                } else if (isEqual(response.Type, responseTypes.NotApplicable)) {
                    Question.Answer = null;
                    Question.NotApplicable = true;
                    Question.ResponseType = responseTypes.NotApplicable;
                } else {
                    Question.DefaultResponseType = responseTypes.Value;
                    if (isEqual(type, "textArea")) {
                        Question.Answer = response.Value ? response.Value : "";
                    } else if (isEqual(type, "date")) {
                        Question.Answer = response.Value ? new Date(response.Value) : null;
                    }
                }
            }
        },
        renderInventoryAnswers: function (scope, Question) {
            forEach(Question.Responses, function(response) {
                switch (response.Type) {
                    case responseTypes.NotSure:
                        Question.Answer = null;
                        Question.ResponseType = responseTypes.NotSure;
                        break;
                    // Not Applicable
                    case responseTypes.NotApplicable:
                        Question.Answer = null;
                        Question.NotApplicable = true;
                        Question.ResponseType = responseTypes.NotApplicable;
                        break;
                    // Other
                    case responseTypes.Other:
                        Question.Answer = response.Value;
                        break;
                    // Justification
                    case responseTypes.Justification:
                        Question.Justification = response.Value;
                        break;
                    // Reference
                    default:
                        Question.DefaultResponseType = responseTypes.Reference;
                        if (response.Value !== null) {
                            // Filter out our option.
                            var option = $filter("filter")(Question.Options, { Value: response.Value });
                            // We only need the first match, even though each option Value is unique.
                            Question.Answer = option.length ? option[0] : { Name: "", Value: response.Value };
                        } else {
                            // If Value is null, set answer to null.
                            Question.Answer = null;
                        }
                }
            });
        },
        setFilters: function (scope, Section) {
            if (Section.Questions && QuestionStates) {
                var riskLevel = "",
                    risks = [];

                var riskFilter = function () {
                    var highestRiskNum = 0;
                    forEach(Section.Questions, function(item) {
                        // If Risk, Risk Level is not null
                        if (item.Risk && item.Risk.Level !== null && item.Risk.Level > RiskLevels.None && !item.IsSkipped) {
                            if (item.Risk.Level > highestRiskNum) {
                                highestRiskNum = item.Risk.Level;
                            }
                            risks.push(item);
                        }
                    });
                    riskLevel = riskLabels[highestRiskNum.toString()];
                }();

                var FilteredQuestions = {
                    Unanswered: $filter("filter")(Section.Questions, {
                        State: QuestionStates.Unanswered
                    }),
                    Answered: $filter("filter")(Section.Questions, {
                        State: QuestionStates.Answered
                    }),
                    Accepted: $filter("filter")(Section.Questions, {
                        State: QuestionStates.Accepted
                    }),
                    InfoRequested: $filter("filter")(Section.Questions, {
                        State: QuestionStates.InfoRequested
                    }),
                    InfoAdded: $filter("filter")(Section.Questions, {
                        State: QuestionStates.InfoAdded
                    }),
                    Skipped: $filter("filter")(Section.Questions, {
                        IsSkipped: true
                    }),
                    Risks: risks.length,
                    RiskLevel: riskLevel
                };

                FilteredQuestions.AllAccepted = FilteredQuestions.Accepted.concat(FilteredQuestions.Exception);
                FilteredQuestions.NeedsReview = FilteredQuestions.Unanswered.concat(FilteredQuestions.Answered);

                return FilteredQuestions;
            }
        }
    };
}

Answer.$inject = ["$rootScope", "ENUMS", "Response", "$filter", "GenFactory", "Assessment"];
