
import { ProtocolService } from "modules/core/services/protocol.service";
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import { IProtocolPacket, IProtocolMessages, IProtocolConfig } from "interfaces/protocol.interface";

export default class CommentService {
    static $inject: string[] = ["$rootScope", "OneProtocol", "ENUMS"];

    private translate: any;

    constructor(
        public readonly $rootScope: IExtendedRootScopeService,
        private readonly OneProtocol: ProtocolService) {
            this.translate = $rootScope.t;
    }

    public getQuestionComments(questionId: string): ng.IPromise<IProtocolPacket> {
        const config: IProtocolConfig = this.OneProtocol.config("GET", `/comment/getquestion/${questionId}`);
        const messages: IProtocolMessages = { Error: { custom: this.translate("CouldNotGetComment") } };
        return this.OneProtocol.http(config, messages);
    }

    public getComment(id: string): ng.IPromise<IProtocolPacket> {
        const config: IProtocolConfig = this.OneProtocol.config("GET", `/comment/get/${id}`);
        const messages: IProtocolMessages = { Error: { custom: this.translate("CouldNotGetComment") } };
        return this.OneProtocol.http(config, messages);
    }

    public addProject(projectComment: any): ng.IPromise<IProtocolPacket> {
        const config: IProtocolConfig = this.OneProtocol.config("POST", "/comment/addproject", [], projectComment);
        const messages: IProtocolMessages = { Error: { custom: this.translate("CouldNotCommentOnProject") } };
        return this.OneProtocol.http(config, messages);
    }

    public addQuestion(questionComment: any): ng.IPromise<IProtocolPacket> {
        const config: IProtocolConfig = this.OneProtocol.config("POST", "/comment/addquestion", [], questionComment);
        const messages: IProtocolMessages = { Error: { custom: this.translate("CouldNotCommentOnQuestion") } };
        return this.OneProtocol.http(config, messages);
    }

    public delete(id: string): ng.IPromise<IProtocolPacket> {
        const config: IProtocolConfig = this.OneProtocol.config("DELETE", `/comment/delete/${id}`);
        const messages: IProtocolMessages = { Error: { custom: this.translate("CouldNotDeleteComment") }, Success: { custom: this.translate("CommentDeleted") } };
        return this.OneProtocol.http(config, messages);
    }
}
