﻿declare var angular: angular.IAngularStatic;

import Answer from "./Services/AnswerService";
import ApplicationQuestionGroup from "./Services/application-question-group.service";
import ApplicationQuestions from "./Services/application-questions.service";
import Assignments from "oneServices/assignments.service";
import AttachmentService from "./Services/attachment.service";
import CommentService from "./Services/comment.service";
import Documents from "./Services/DocumentsService";
import Export from "./Services/ExportService";
import ProjectQuestions from "oneServices/project-question.service";
import { ProjectService } from "oneServices/project.service";
import QuestionGroup from "./Services/question-group.service";
import ResponseService from "./Services/response.service";
import RiskService from "./Services/risk.service";
import RiskBusinessLogic from "oneServices/logic/risk-business-logic.service";
import RiskSummaryContentViewLogicService from "oneServices/view-logic/risk-summary-content-view-logic.service";
import RiskPermissionService from "oneServices/permissions/risk-permission.service";
import RiskModalViewLogicService from "oneServices/view-logic/risk-modal-view-logic.service";

export const questionnaireModule = angular.module("zen.questionnaire", [
    // Angular modules

    // 3rd Party Modules

])
    .factory("Answer", Answer)
    .service("ApplicationQuestionGroup", ApplicationQuestionGroup)
    .service("ApplicationQuestions", ApplicationQuestions)
    .service("Assignments", Assignments)
    .service("Attachment", AttachmentService)
    .service("Comment", CommentService)
    .factory("Documents", Documents)
    .factory("Export", Export)
    .service("ProjectQuestions", ProjectQuestions)
    .service("Projects", ProjectService)
    .service("QuestionGroup", QuestionGroup)
    .service("Response", ResponseService)
    .service("Risks", RiskService)
    .service("RiskBusinessLogic", RiskBusinessLogic)
    .service("RiskSummaryContentViewLogic", RiskSummaryContentViewLogicService)
    .service("RiskPermission", RiskPermissionService)
    .service("RiskModalViewLogic", RiskModalViewLogicService)
;
