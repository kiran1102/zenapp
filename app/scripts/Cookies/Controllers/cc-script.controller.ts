declare var angular: any;

export default class CookieComplianceScriptCtrl {
    constructor(
        $scope: any,
        WebAuditService: any,
        CookieComplianceStore: any,
    ) {
        $scope.PageReady = false;
        $scope.ScriptUnavailable = true;

        $scope.loadConsentDomain = (): void | undefined => {
            if (!$scope.model.selectedWebsite) { return; }

            CookieComplianceStore.setSelectedWebsite($scope.model.selectedWebsite);
            const domainId: number = CookieComplianceStore.getSelectedDomainId();

            if (domainId) {
                WebAuditService.getDomainDetails(domainId).then((data: any): void => {
                    if (data !== null) {
                        $scope.domainModel = data;
                        $scope.ScriptUnavailable = false;
                        $scope.PageReady = true;
                    } else {
                        $scope.domainModel = undefined;
                        $scope.ScriptUnavailable = true;
                        $scope.PageReady = true;
                    }
                });
            } else {
                $scope.ScriptUnavailable = true;
                $scope.domainModel = undefined;
                $scope.PageReady = true;
            }
        };

        $scope.loadReport = (): void => {
            $scope.PageReady = false;
            $scope.init();
        };

        $scope.init = (): void => {
            if ($scope.model.selectedWebsite) {
                $scope.loadConsentDomain();
            } else {
                $scope.$watch("model.domainList", (): void => {
                    $scope.loadConsentDomain();
                });
            }
        };

        $scope.init();
    }
}

CookieComplianceScriptCtrl.$inject = ["$scope", "WebAuditService", "CookieComplianceStore"];
