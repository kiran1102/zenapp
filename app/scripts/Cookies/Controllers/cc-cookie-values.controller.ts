class CookieComplianceCookieValuesCtrl {
    static $inject: string[] = [
        "$q",
        "$scope",
        "WebAuditService",
        "$stateParams",
    ];

    constructor(
        $q: ng.IQService,
        $scope: any,
        WebAuditService: any,
        $stateParams: ng.ui.IStateParamsService,
    ) {
        $scope.reportReady = false;
        $scope.cookieValuesReport = {};
        $scope.host = "";
        $scope.pageURL = "";
        $scope.cookieName = "";
        $scope.parentDomain = "";

        $scope.loadReportCookieValues = (): void => {
            const promises: Array<ng.IPromise<any>> = [];
            $scope.reportReady = false;

            if (!$scope.parentDomain.length && $scope.model.selectedWebsite.length) {
                $scope.parentDomain = $scope.model.selectedWebsite;
            } else if ($scope.parentDomain !== $scope.model.selectedWebsite || !$scope.model.selectedWebsite.length) {
                $scope.goTo("zen.app.pia.module.cookiecompliance.views.reports");
            }
            if (!$scope.model.summaryReport || $scope.model.summaryReport.domain !== $scope.model.selectedWebsite) {
                // Get scanning status of current domain
                $scope.updateProgress();
                promises.push($scope.getSummaryReport($scope.model.selectedWebsite).then((data: any): void => {
                    $scope.model.summaryReport = data;
                }));
            }
            if ($stateParams.cookie) {
                if ($stateParams.url) {
                    $scope.host = $stateParams.url;
                } else {
                    $scope.host = $scope.model.selectedWebsite;
                }
                $scope.cookieName = $stateParams.cookie;
                promises.push(WebAuditService.getCookieValuesByName($scope.model.selectedWebsite, $stateParams.cookie).then((data: any): void => {
                    $scope.cookieValuesReport = {
                        reportType: "cookie",
                        reportData: data,
                    };
                }));
            } else if ($stateParams.url) {
                $scope.pageURL = $stateParams.url;
                promises.push(WebAuditService.getCookieValuesByUrl($scope.model.selectedWebsite, $stateParams.url).then((data: any): void => {
                    $scope.cookieValuesReport = {
                        reportType: "url",
                        reportData: data,
                    };
                }));
            }

            $q.all(promises).then((data: any): void => {
                $scope.reportReady = true;
                $scope.debounceRefresh = false;
            });
        };

        $scope.init = (): void => {
            if ($scope.model.selectedWebsite) {
                $scope.loadReportCookieValues();
            } else {
                $scope.$watch("model.domainList", (): void => {
                    $scope.loadReportCookieValues();
                });
            }
        };

        $scope.$on("LOAD_REPORT", (event: ng.IAngularEvent): void => {
            $scope.loadReportCookieValues();
        });

        $scope.reloadReport = (): void => {
            $scope.debounceRefresh = true;
            $scope.reportReady = false;
            $scope.loadReportMain().then((): void => {
                $scope.loadReportCookieValues();
            });
        };

        $scope.init();
    }
}

export default CookieComplianceCookieValuesCtrl;
