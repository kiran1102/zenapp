import * as _ from "lodash";
import { WebAuditService } from "cookiesService/web-audit.service";

import { CookieParts } from "constants/cookie.constant";

import { IExtendedRootScopeService } from "interfaces/root-scope.interface";

export default class CookieComplianceCookiesCtrl {
    constructor(
        $rootScope: IExtendedRootScopeService,
        $q: ng.IQService,
        $scope: any,
        WebAuditService: WebAuditService,
    ) {
        const translate = $rootScope.t;
        $scope.reportReady = false;
        $scope.CookieDetailReport = {};
        $scope.PurposeList = [];
        $scope.model.selectedPart = CookieParts.Cookies;

        function getCookieBreakdownByType(data: any): any {
            const firstPartySession: number = _.filter(data, (cookie: any): boolean => cookie.change !== "old" && cookie.firstParty && cookie.isSession).length;
            const firstPartyPersistent: number = _.filter(data, (cookie: any): boolean => cookie.change !== "old" && cookie.firstParty && !cookie.isSession).length;
            const thirdPartySession: number = _.filter(data, (cookie: any): boolean => cookie.change !== "old" && !cookie.firstParty && cookie.isSession).length;
            const thirdPartyPersistent: number = _.filter(data, (cookie: any): boolean => cookie.change !== "old" && !cookie.firstParty && !cookie.isSession).length;
            const total: number = _.filter(data, (cookie: any): boolean => cookie.change !== "old").length;

            return total ? [
                {
                    class: "graph-container__segment--first-party-session",
                    count: firstPartySession,
                    label: translate("FirstPartySession"),
                    value: firstPartySession / total,
                },
                {
                    class: "graph-container__segment--first-party-persistent",
                    count: firstPartyPersistent,
                    label: translate("FirstPartyPersistent"),
                    value: firstPartyPersistent / total,
                },
                {
                    class: "graph-container__segment--third-party-session",
                    count: thirdPartySession,
                    label: translate("ThirdPartySession"),
                    value: thirdPartySession / total,
                },
                {
                    class: "graph-container__segment--third-party-persistent",
                    count: thirdPartyPersistent,
                    label: translate("ThirdPartyPersistent"),
                    value: thirdPartyPersistent / total,
                },
            ] : [{
                class: "remainder",
                label: "",
                value: 1,
            }];
        }

        $scope.loadReportCookies = (reloadSummaryReport: boolean = false): void => {
            if (!$scope.model.selectedWebsite) return;

            const promises: Array<ng.IPromise<any>> = [];

            $scope.reportReady = false;

            if (!$scope.model.summaryReport || $scope.model.summaryReport.domain !== $scope.model.selectedWebsite || reloadSummaryReport) {
                // Get scanning status of current domain
                $scope.updateProgress();
                promises.push($scope.getSummaryReport($scope.model.selectedWebsite).then((data: any): void => {
                    $scope.model.summaryReport = data;
                }));
            }

            promises.push(WebAuditService.getCookieDetailReport($scope.model.selectedWebsite, $scope.model.selectedRowKey).then((data: any): void => {
                $scope.CookieDetailReport = data;
                $scope.PurposeList = _.sortBy(_.uniq(_.map(data, "purpose")), purposeOrder);
                $scope.model.cookiesByType = getCookieBreakdownByType(data);
            }));

            $q.all(promises).then((data: any) => {
                $scope.reportReady = true;
            });
        };

        $scope.init = (): void => {
            if ($scope.model.selectedWebsite) {
                $scope.loadReportCookies(false);
            } else {
                $scope.$watch("model.domainList", () => {
                    $scope.loadReportCookies(false);
                });
            }
        };

        $scope.hasFirstParty = (purpose: string): boolean => {
            return _.some($scope.CookieDetailReport, (cookie: any): boolean => cookie.purpose === purpose && cookie.firstParty);
        };

        $scope.hasThirdParty = (purpose: string): boolean => {
            return _.some($scope.CookieDetailReport, (cookie: any): boolean => cookie.purpose === purpose && !cookie.firstParty);
        };

        $scope.filterFirstPartyBy = (purpose: string): any => {
            return _.filter($scope.CookieDetailReport, (cookie: any): boolean => cookie.purpose === purpose && cookie.firstParty);
        };

        $scope.filterThirdPartyBy = (purpose: string): any => {
            return _.sortBy(_.filter($scope.CookieDetailReport, (cookie: any): boolean => cookie.purpose === purpose && !cookie.firstParty), "host");
        };

        $scope.getClassNameFor = (purpose: string): string => {
            return WebAuditService.getCookieClass(purpose, "graph-legend__");
        };

        $scope.init();

        function purposeOrder(purpose: string): number {
            const labelOrder = ["Strictly Necessary", "Performance", "Functionality", "Targeting/Advertising", "Unknown"];
            return labelOrder.indexOf(purpose);
        }

        $scope.$on("LOAD_REPORT", (event: ng.IAngularEvent, reloadSummaryReport: boolean = false): void => {
            $scope.loadReportCookies(reloadSummaryReport);
        });

        $scope.reloadReport = (): void => {
            $scope.debounceRefresh = true;
            $scope.reportReady = false;
            $scope.loadReportMain().then((): void => {
                $scope.loadReportCookies();
            });
        };
    }
}

CookieComplianceCookiesCtrl.$inject = ["$rootScope", "$q", "$scope", "WebAuditService"];
