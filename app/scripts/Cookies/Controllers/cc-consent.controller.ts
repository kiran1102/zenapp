
declare var angular: any;
import * as _ from "lodash";
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import { IBannerLayoutProperties } from "interfaces/cookies/cc-banner-template.interface";

// Constants
import { CookieScanStatus } from "constants/cookie.constant";

import { CookieConsentService } from "cookiesService/cookie-consent.service";

export default function CookieComplianceConsentCtrl(
    $scope: any,
    $rootScope: IExtendedRootScopeService,
    WebAuditService: any,
    Permissions: any,
    CookieComplianceStore: any,
    cookieConsentService: CookieConsentService,
) {
    $scope.PageReady = false;
    $scope.ScriptUnavailable = false;
    $scope.consentLoadingMessage = "";
    $scope.showCookieAndPrivacyPolicy = false;
    $scope.LayoutOptions = [
        {
            imageSrc: "images/consent/bottom-dark.svg",
            label: $rootScope.t("BottomBarDarkTheme"),
            position: "bottom",
            selected: true,
            theme: "dark",
        },
        {
            imageSrc: "images/consent/bottom-light.svg",
            label: $rootScope.t("BottomBarLightTheme"),
            position: "bottom",
            theme: "light",
        },
        {
            imageSrc: "images/consent/top-dark.svg",
            label: $rootScope.t("TopBarDarkTheme"),
            position: "top",
            theme: "dark",
        },
        {
            imageSrc: "images/consent/top-light.svg",
            label: $rootScope.t("TopBarLightTheme"),
            position: "top",
            theme: "light",
        },
        {
            imageSrc: "images/consent/cookie-banner-center.svg",
            label: $rootScope.t("CenterTileLightTheme"),
            position: "center",
            theme: "light",
        },
    ];

    $scope.showLoadingPrefix = {
        value: true,
    };

    $scope.loadNotice = (): void => {
        $scope.PageReady = true;
        $scope.ScriptUnavailable = _.isUndefined($scope.domainModel);
    };

    $scope.selectLayout = (index: number): void => {
        _.each($scope.LayoutOptions, (layout: IBannerLayoutProperties): void => { layout.selected = false; });
        $scope.LayoutOptions[index].selected = true;
    };

    $scope.SetLayout = (skinName: string): void => {
        const skinPosition: string = cookieConsentService.getSkinPosition(skinName, Permissions.canShow("CookieCenterTileLayout"));
        const skinTheme = _.includes(skinName, "white") ? "light" : "dark";
        _.each($scope.LayoutOptions, (layout: IBannerLayoutProperties, index: number): void => {
            if (skinPosition === layout.position && skinTheme === layout.theme) {
                $scope.selectLayout(index);
            }
        });
    };

    $scope.saveConsentBanner = (): void => {
        $scope.PageReady = false;
        _.each($scope.LayoutOptions, (element: any): void => {
            if (element.selected) {
                $scope.consentNotice.Layout = element.position;
                $scope.consentNotice.Theme = element.theme;
            }
        });

        let promise: Promise<any>;
        $scope.showLoadingPrefix.value = false;

        if ($scope.consentNotice.DomainId === 0) {
            $scope.consentLoadingMessage = $rootScope.t("ConsentNotice.LoadingText.Creating");
            promise = WebAuditService.createConsentNotice($scope.consentNotice).then((domain: any): void => {
                if (domain != null) {
                    _.each($scope.model.domainList, (domains: any, index: number): void => {
                        if (domains.auditDomain === $scope.consentNotice.DomainName) {
                            $scope.consentNotice.DomainId = domain.domainId;
                            $scope.model.domainList[index].domainId = domain.domainId;
                            CookieComplianceStore.setWebsiteList($scope.model.domainList);
                        }
                    });
                }
                $scope.loadNotice();
                $scope.$emit("ccSetArchiveFlagTrue");
            });
        } else {
            $scope.consentLoadingMessage = $rootScope.t("UpdatingUpper");
            promise = WebAuditService.updateConsentNotice($scope.consentNotice).then((): void => {
                $scope.loadNotice();
                $scope.$emit("ccSetArchiveFlagTrue");
            });
        }

        promise.then((): void => {
            $scope.consentLoadingMessage = "";
            $scope.showLoadingPrefix.value = true;

            if (Permissions.canShow("CookieBannerEditor")) {
                $scope.goTo("zen.app.pia.module.cookiecompliance.views.banner");
            }
        });
    };

    $scope.loadInitialNotice = (): void | undefined => {
        if (!$scope.model.selectedWebsite) { return; }
        let selectedDomain;
        _.each($scope.model.domainList, (domains: any): void => {
            if (domains.auditDomain === $scope.model.selectedWebsite) {
                selectedDomain = domains;
                $scope.consentNotice.DomainId = domains.domainId;
                $scope.consentNotice.DomainName = domains.auditDomain;
            }
        });

        if ($scope.consentNotice.DomainId !== 0) {
            if (Permissions.canShow("CookieBannerEditor")) {
                $scope.goTo("zen.app.pia.module.cookiecompliance.views.banner");
            }

            WebAuditService.getDomainDetails($scope.consentNotice.DomainId).then((data: any): void => {
                if (data !== null) {
                    $scope.domainModel = data;
                    $scope.SetLayout($scope.domainModel.LayoutModel.SkinDirectoryName);
                    $scope.loadNotice();
                } else {
                    $scope.domainModel = undefined;
                    $scope.selectLayout(0);
                    $scope.loadNotice(false);
                }
            });
        } else {
            if (!selectedDomain.DomainId && selectedDomain.status === CookieScanStatus.InProgress || selectedDomain.status === CookieScanStatus.Pending) {
                $scope.goTo("zen.app.pia.module.cookiecompliance.views.cookieEmptyState", { pageName: $scope.pageNames.cookieBanner, isDomainCreated: false });
                return;
            }
            $scope.domainModel = undefined;
            $scope.selectLayout(0);
            $scope.loadNotice(false);
        }
    };

    $scope.loadReport = (selection: any): void => {
        $scope.model.selectedWebsite = selection;
        $scope.pageReady = false;
        $scope.init();
    };

    $scope.init = (): void => {
        if (!Permissions.canShow("CookieCenterTileLayout")) {
            _.each($scope.LayoutOptions, (layout: IBannerLayoutProperties, index: number): void => {
                if (layout.position === "center") {
                    $scope.LayoutOptions.splice(index, 1);
                }
            });
        }

        if ($scope.model.selectedWebsite) {
            $scope.loadInitialNotice();
        } else {
            $scope.$watch("model.domainList", (): void => {
                $scope.loadInitialNotice();
            });
        }
    };

    $scope.init();
}

CookieComplianceConsentCtrl.$inject = ["$scope", "$rootScope", "WebAuditService", "Permissions", "CookieComplianceStore", "CookieConsentService"];
