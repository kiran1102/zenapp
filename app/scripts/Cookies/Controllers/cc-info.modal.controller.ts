import { CookieParts } from "constants/cookie.constant";

import { IExtendedRootScopeService } from "interfaces/root-scope.interface";

export default class InfoMdCtrl {

    static $inject: string[] = [
        "$scope",
        "$rootScope",
        "$uibModalInstance",
        "$sce",
        "ModalType",
    ];

    constructor(
        readonly $scope: any,
        readonly $rootScope: IExtendedRootScopeService,
        readonly $uibModalInstance: any,
        readonly $sce: ng.ISCEService,
        readonly ModalType: any,
    ) {
        $scope.cancel = this.cancel;
        this.getPopupContent(this.ModalType);
    }

    private cancel = (): void => {
        this.$uibModalInstance.close({ result: false, data: null });
    }

    private getPopupContent = (popupType: string): void => {
        switch (popupType) {
            case CookieParts.Cookies:
                this.$scope.PopupHeader = this.$rootScope.t("CookiePurposesExplained");
                this.$scope.PopupContent = this.$sce.trustAsHtml(`<p class="info-modal__title">${this.$rootScope.t("WeCategorizeCookiesInReport")}&nbsp;
						<a href="http://cookiepedia.co.uk/classify-cookies", target="__blank">${this.$rootScope.t("Cookiepedia")}</a></p>
						<p class='info-modal__text text-bold'>${this.$rootScope.t("AsASummary")}</p>
						<p class='info-modal__text'><span class='text-bold'>${this.$rootScope.t("StrictlyNecessary")}</span>&nbsp;${this.$rootScope.t("StrictlyNecessaryDescription")}</p>
						<p class='info-modal__text'><span class='text-bold'>${this.$rootScope.t("PerformanceCookies")}</span>&nbsp;${this.$rootScope.t("PerformanceCookiesDescription")}</p>
						<p class='info-modal__text'><span class='text-bold'>${this.$rootScope.t("FunctionalityCookies")}</span>&nbsp;${this.$rootScope.t("FunctionalityCookiesDescription")}</p>
						<p class='info-modal__text'><span class='text-bold'>${this.$rootScope.t("TargetingOrAdvertisingCookies")}</span>&nbsp;${this.$rootScope.t("TargetingOrAdvertisingCookiesDescription")}</p>`);
                break;
            case CookieParts.Tags:
                this.$scope.PopupHeader = this.$rootScope.t("TagsExplained");
                this.$scope.PopupContent = this.$sce.trustAsHtml(`<p class='info-modal__text'>${this.$rootScope.t("TagsExplainedParagraph1")}</p>
						<p class='info-modal__text'>${this.$rootScope.t("TagsExplainedParagraph2")}</p>
						<p class='info-modal__text'>${this.$rootScope.t("TagsExplainedParagraph3")}</p>`);
                break;
            case CookieParts.Forms:
                this.$scope.PopupHeader = this.$rootScope.t("FormsExplained");
                this.$scope.PopupContent = this.$sce.trustAsHtml(`<p class='info-modal__text'>${this.$rootScope.t("FormsExplainedParagraph1")}</p>
						<p class='info-modal__text'>${this.$rootScope.t("FormsExplainedParagraph2")}</p>`);
                break;
            default:
                this.$scope.PopupHeader = "";
                this.$scope.PopupContent = "";
                break;
        }
    }

}
