declare var OneConfirm: any;
import { WebAuditService } from "cookiesService/web-audit.service";
import RetryPollingService from "sharedServices/retry-polling.service";

import { CookieScanStatus } from "constants/cookie.constant";

import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import { NotificationService } from "modules/shared/services/provider/notification.service";

export default class CookieComplianceWebsitesCtrl {
    constructor(
        $scope: any,
        $rootScope: IExtendedRootScopeService,
        $uibModal: any,
        webAuditService: WebAuditService,
        RetryService: RetryPollingService,
        private notificationService: NotificationService,
    ) {
        const quickPollRate = 10000;
        const slowPollRate = 30000;

        $scope.refreshAction = (): void => {
        if ($scope.isLoading) {
            return;
        }
        $scope.isLoading = true;
        $scope.loadWebsitesList(true, false).then((): void => {
            $scope.isLoading = false;
            RetryService.triggerSuccess();
        },
            (): void => {
                $scope.isLoading = false;
                RetryService.triggerFailure();
            });
    };
        $scope.successAction = (): void => notificationService.alertSuccess($rootScope.t("Success"), $rootScope.t("CcWebsitesReconnected"));
        $scope.failureAction = (): void => notificationService.alertError($rootScope.t("Error"), $rootScope.t("CcWebsitesDisconnected"));

        RetryService.initialize($scope.refreshAction, 2, slowPollRate + 2000, quickPollRate, slowPollRate, true,
            $scope.failureAction, $scope.successAction);

        $scope.$on("$destroy", (): void => {
            RetryService.setLoopStatus(false);
        });

        $scope.addWebsite = ($event: ng.IAngularEvent, domain: string): void => {
            const url = "addWebsiteModal.html";
            const modalInstance = $uibModal.open({
                backdrop: "static",
                controller: "AddWebsiteCtrl",
                keyboard: false,
                resolve: {
                    DataProvider: (): { Domain: string } => {
                        return {
                            Domain: domain,
                        };
                    },
                },
                scope: $scope,
                size: "md",
                templateUrl: url,
            });

            modalInstance.result.then((): void => {
                $scope.loadWebsitesList();
            });
        };

        $scope.deleteWebsite = (domain: string): void => {
            $scope.loadWebsitesList(true, true, true).then((): void => {
                if ($scope.domainListCount === 0) {
                    $scope.model.selectedWebsite = "";
                } else if (domain === $scope.model.selectedWebsite) {
                    $scope.model.selectedWebsite = $scope.model.domainList[0].auditDomain;
                }
            });
        };

        $scope.cancelAudit = (domain: string): void => {
            OneConfirm("", $rootScope.t("ConfirmYouWantToStopTheScanFor", { Domain: domain }), "", "", "", $rootScope.t("Confirm"), $rootScope.t("Cancel")).then((result: boolean): void => {
                if (result) {
                    webAuditService.cancelAudit(domain).then((): void => {
                        $scope.loadWebsitesList().then((): void => {
                            if ($scope.domainListCount === 0) {
                                $scope.model.selectedWebsite = "";
                            } else if (domain === $scope.model.selectedWebsite) {
                                $scope.model.selectedWebsite = $scope.model.domainList[0].auditDomain;
                            }
                        });
                    });
                }
            });
        };

        $scope.isComplete = (domain: any): boolean => (domain.status === CookieScanStatus.Complete || domain.status === CookieScanStatus.Cancelled);

        $scope.isNewDomain = (domain: any): boolean => (domain.status === CookieScanStatus.Complete && domain.isNew);

        $scope.isStoppable = (domain: any): boolean => (domain.status === CookieScanStatus.Pending || domain.status === CookieScanStatus.InProgress);

    }
}

CookieComplianceWebsitesCtrl.$inject = ["$scope", "$rootScope", "$uibModal",
    "WebAuditService", "RetryPollingService", "NotificationService"];
