declare var angular: any;
import * as _ from "lodash";
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import { WebAuditService } from "cookiesService/web-audit.service";

import { CookieParts } from "constants/cookie.constant";

export default class CookieComplianceFormsCtrl {
    constructor(
        $q: ng.IQService,
        $scope: any,
        $rootScope: IExtendedRootScopeService,
        WebAuditService: WebAuditService,
        $compile: ng.ICompileService,
    ) {
        $scope.reportReady = false;
        $scope.FormDetailReport = {};
        $scope.model.selectedPart = CookieParts.Forms;

        $scope.loadReportForms = (): void => {
            if (!$scope.model.selectedWebsite) return;

            const promises: Array<ng.IPromise<any>> = [];

            $scope.reportReady = false;

            if (!$scope.model.summaryReport || $scope.model.summaryReport.domain !== $scope.model.selectedWebsite) {
                // Get scanning status of current domain
                $scope.updateProgress();
                promises.push($scope.getSummaryReport($scope.model.selectedWebsite).then((data: any): void => {
                    $scope.model.summaryReport = data;
                }));
            }

            promises.push(WebAuditService.getFormDetailReport($scope.model.selectedWebsite).then((data: any): void => {
                $scope.FormDetailReport = data;
            }));

            $q.all(promises).then(() => {
                $scope.reportReady = true;
                $scope.debounceRefresh = false;
            });
        };

        $scope.getFormLink = (elementId: string, count: number): string => {
            const element: any = angular.element(document.getElementById(elementId));
            const linktext = `<div>(${$rootScope.t("FoundOnPages", { Count: count })})</div>`;
            let linkTag = `<a ng-click=goToFormPages('${encodeURIComponent(elementId)}')><b><u>${count.toString()}</u></b></a>`;
            linkTag = _.replace(linktext, new RegExp(count.toString()), linkTag.toString());
            element.empty();
            element.prepend($compile(linkTag.toString())($scope));
            return "";
        };

        $scope.goToFormPages = (formName: string): void => {
            $scope.goTo("zen.app.pia.module.cookiecompliance.views.formpages", { formName: decodeURIComponent(formName) });
        };

        $scope.init = (): void => {
            if ($scope.model.selectedWebsite) {
                $scope.loadReportForms();
            } else {
                $scope.$watch("model.domainList", () => {
                    $scope.loadReportForms();
                });
            }
        };

        $scope.$on("LOAD_REPORT", (event: ng.IAngularEvent): void => {
            $scope.loadReportForms();
        });

        $scope.reloadReport = (): void => {
            $scope.debounceRefresh = true;
            $scope.reportReady = false;
            $scope.loadReportMain().then((): void => {
                $scope.loadReportForms();
            });
        };

        $scope.init();
    }
}

CookieComplianceFormsCtrl.$inject = ["$q", "$scope", "$rootScope", "WebAuditService", "$compile"];
