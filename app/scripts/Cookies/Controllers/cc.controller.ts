import { find, filter, orderBy, some } from "lodash";
declare var Utilities: any;
import { Permissions } from "modules/shared/services/helper/permissions.service";
import { CookieComplianceStore } from "cookiesService/cookie-compliance-store.service";
import { ModalService } from "sharedServices/modal.service";
import { CookieAuditScheduleService } from "app/scripts/Cookies/Services/cookie-audit-schedule.service";
import { WebAuditService } from "cookiesService/web-audit.service";
import { CookieSideMenuHelperService } from "modules/cookies/services/helper/cookie-side-menu-helper.service";
import { UserActionService } from "oneServices/actions/user-action.service";
import { Wootric } from "sharedModules/services/provider/wootric.service";

import { CookieScanStatus, CookieParts, ScanBehindLoginStatus } from "constants/cookie.constant";
import { WootricModuleMap } from "modules/shared/constants/wootric.constants";
import { CookiesStorageService } from "modules/cookies/services/helper/cookies-storage.service";
import { CookiesModel } from "interfaces/cookies/cookies-model.interface";
import { CcConsentNotice } from "interfaces/cookies/cc-consent-notice.interface";

import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import { IDomain, IDomainSort } from "interfaces/cookies/cc-domain.interface";

import { Subscription } from "rxjs/Subscription";

export default function CookieComplianceCtrl(
    $scope: any,
    $rootScope: IExtendedRootScopeService,
    $state: ng.ui.IStateService,
    $q: ng.IQService,
    $window: ng.IWindowService,
    webAuditService: WebAuditService,
    permissions: Permissions,
    GenFactory: any,
    $uibModal: any,
    $timeout: ng.ITimeoutService,
    cookieComplianceStore: CookieComplianceStore,
    modalService: ModalService,
    cookieAuditScheduleService: CookieAuditScheduleService,
    userActionService: UserActionService,
    cookieSideMenuHelperService: CookieSideMenuHelperService,
    wootric: Wootric,
    cookiesStorageService: CookiesStorageService,
): void {
    let timeout: any = null;
    let destroyed = false;
    let scanNowTrigger = false;
    let domainHistoryList: any = [];
    const subscriptions: Subscription[] = [];

    $scope.title = $rootScope.t("CookieConsent");
    $scope.Ready = false;
    $scope.domainListCount = 0;
    $scope.LaunchDisabled = false;
    $scope.FirstDomain = true;
    $scope.CanAddWebsites = permissions.canShow("CookieAuditUnlimited");
    $scope.canShowIABSettings = permissions.canShow("CookieIABIntegration");
    $scope.ShowCookieAuditQueryString = permissions.canShow("CookieAuditQueryString");
    $scope.ShowCookieAuditStartPages = permissions.canShow("CookieAuditStartPages");
    $scope.canAuditSchedule = permissions.canShow("CookieAuditScheduling");
    $scope.canShowScriptArchive = permissions.canShow("ScriptArchive");
    $scope.canShowScanBehindLogin = permissions.canShow("CookieScanBehindLogin");
    $scope.canTenantLevelScheduledScan = permissions.canShow("CookieTenantLevelScheduledScan");
    $scope.showNewWebsiteList = permissions.canShow("CookieNewWebsiteList");
    $scope.showCookieBannerTemplate = permissions.canShow("CookieReadBannerTemplates");
    $scope.canShowSiteMapUris = permissions.canShow("CookieSiteMapUris");
    $scope.canShowConsentPolicy = permissions.canShow("CookieConsentPolicy");
    $scope.canShowAddWebsite = permissions.canShow("CookieAddWebsite");
    $scope.sortType = "domain"; // set the default sort type
    $scope.sortReverse = true; // set the default sort order
    $scope.scanInProgress = false;
    $scope.scanPending = false;
    $scope.debounceRefresh = false;
    $scope.domainReadyForBanner = false;
    $scope.domainHasScriptArchive = false;
    $scope.domainHasTemplateAssigned = false;
    $scope.hasSetArchiveFlag = false;
    $scope.sortKey = "auditDomain";
    $scope.sortOrder = "asc";
    $scope.searchString = "";
    $scope.page = 0;
    $scope.size = 20;
    $scope.isMenuListUpdated = false;

    cookiesStorageService.model.next(new CookiesModel());
    subscriptions.push(cookiesStorageService.model.subscribe((model: CookiesModel) => {
        $scope.model = model;
    }));
    if (!cookiesStorageService.consentNotice) {
        cookiesStorageService.consentNotice.next(new CcConsentNotice());
    }
    subscriptions.push(cookiesStorageService.consentNotice.subscribe((consentNotice: CcConsentNotice) => {
        $scope.consentNotice = consentNotice;
    }));
    $scope.domainModel = null;
    $scope.ScanStatus = CookieScanStatus;
    $scope.cookieParts = CookieParts;

    $scope.reportParts = [
        { label: $rootScope.t("Dashboard"), value: CookieParts.Dashboard },
        { label: $rootScope.t("Cookies"), value: CookieParts.Cookies },
        { label: $rootScope.t("Tags"), value: CookieParts.Tags },
        { label: $rootScope.t("Forms"), value: CookieParts.Forms },
        { label: $rootScope.t("Pages"), value: CookieParts.Pages },
        { label: $rootScope.t("Storage"), value: CookieParts.LocalStorage },
    ];

    $scope.pageNames = {
        cookieBanner:  $rootScope.t("CookieBanner"),
        cookieDisclosure:  $rootScope.t("CookieDisclosure"),
        preferenceCenter:  $rootScope.t("PreferenceCenter"),
        scriptArchive:  $rootScope.t("ScriptArchive"),
    };

    $scope.loadReportPart = (selection: string): void => {
        $scope.model.selectedPart = selection;
        $scope.model.domainHistorySelectionList = validDomainHistoryListFilter(domainHistoryList);
        switch (selection) {
            case CookieParts.Dashboard:
                $scope.goTo("zen.app.pia.module.cookiecompliance.views.reports");
                break;
            case CookieParts.Cookies:
                $scope.goTo("zen.app.pia.module.cookiecompliance.views.cookies");
                break;
            case CookieParts.Tags:
                $scope.goTo("zen.app.pia.module.cookiecompliance.views.tags");
                break;
            case CookieParts.Forms:
                $scope.goTo("zen.app.pia.module.cookiecompliance.views.forms");
                break;
            case CookieParts.Pages:
                $scope.goTo("zen.app.pia.module.cookiecompliance.views.pages");
                break;
            case CookieParts.LocalStorage:
                $scope.goTo("zen.app.pia.module.cookiecompliance.views.localStorage");
                break;
            default:
                $scope.goTo("zen.app.pia.module.cookiecompliance.views.cookies");
                break;
        }
    };

    function applySort(domainsList: IDomain[]): IDomain[] {
        return domainsList.sort((a: IDomain, b: IDomain) => {
            // keep in progress scans at the top
            if (a.status !== CookieScanStatus.Complete) {
                return -1;
            }
            if (b.status !== CookieScanStatus.Complete) {
                return 1;
            }

            // sort by column definitions
            switch ($scope.sortType) {
                case "domain":
                    return GenFactory.sortOrder(a.auditDomain, b.auditDomain, $scope.sortReverse);
                case "lastAudit":
                    return GenFactory.sortOrder(a.completedTime, b.completedTime, $scope.sortReverse);
                case "pages":
                    return GenFactory.sortOrder(a.auditedPages, b.auditedPages, $scope.sortReverse);
                case "cookies":
                    return GenFactory.sortOrder(a.cookiesFound, b.cookiesFound, $scope.sortReverse);
                case "privacypolicy":
                    return GenFactory.sortOrder(a.hasPrivacyPolicy, b.hasPrivacyPolicy, $scope.sortReverse);
                case "banner":
                case "policy":
                case "preferences":
                    return GenFactory.sortOrder(a.hasBanner, b.hasBanner, $scope.sortReverse);
                default:
                    return GenFactory.sortOrder(a.auditDomain, b.auditDomain, $scope.sortReverse);
            }
        });
    }
    $rootScope.$on("ccSetSelectedwebsite", (event: ng.IAngularEvent, data: string): void => {
        $scope.model.selectedWebsite = data;
    });

    $rootScope.$on("ccSetArchiveFlagTrue", (event: ng.IAngularEvent): void => {
        if (!$scope.domainHasScriptArchive) {
            $scope.loadWebsitesList().then((): void => {
                $scope.domainHasScriptArchive = true;
                $scope.hasSetArchiveFlag = true;
            });
        }
    });

    $rootScope.$on("ccLoadWebsitesList", () => {
        $scope.loadWebsitesList();
    });

    $rootScope.$on("ccInitCookieData", (event: ng.IAngularEvent, data: string[]): void => {
        $scope.model.selectedWebsite = data[0];
        const currentPage: string = data[1];
        if (!$scope.model.domainList || $scope.model.domainList.length <= 0 || currentPage === "ccTemplatePublish") {
            $scope.loadWebsitesList().then((): void => {
                if (!$scope.model.selectedWebsite) {
                    $scope.model.selectedWebsite = $scope.model.domainSelectionList[0].auditDomain;
                    cookieComplianceStore.setSelectedWebsite($scope.model.selectedWebsite);
                }
                cookieComplianceStore.setWebsiteList($scope.model.domainList);
                if (currentPage === "ccPreferenceCenter") {
                    $scope.switchPreferenceCenterEditor();
                } else if (currentPage === "ccPolicyPage") {
                    $scope.switchCookiePolicyEditor();
                }
                if (cookieComplianceStore.getSelectedDomainId()) {
                    if (currentPage === "ccPreferenceCenter") {
                        $rootScope.$broadcast("ccResumePreferenceCenter", null);
                    } else if (currentPage === "ccPolicyPage") {
                        $rootScope.$broadcast("ccResumeCookiePolicy", null);
                    }
                }
                if (currentPage === "ccScriptIntegration") {
                    $rootScope.$broadcast("ccResumeScriptintegration", null);
                } else if (currentPage === "ccScriptArchive") {
                    $rootScope.$broadcast("ccResumeScriptArchive", null);
                }
                cookiesStorageService.model.next($scope.model);
            });
        } else {
            if (!$scope.model.selectedWebsite) {
                $scope.model.selectedWebsite = $scope.model.domainSelectionList[0].auditDomain;
                cookieComplianceStore.setSelectedWebsite($scope.model.selectedWebsite);
            }
            if (currentPage === "ccPreferenceCenter") {
                $scope.switchPreferenceCenterEditor();
            } else if (currentPage === "ccPolicyPage") {
                $scope.switchCookiePolicyEditor();
            } else if (currentPage === "ccScriptIntegration") {
                $rootScope.$broadcast("ccResumeScriptintegration", null);
            } else if (currentPage === "ccScriptArchive") {
                $rootScope.$broadcast("ccResumeScriptArchive", null);
            }
        }
    });

    $rootScope.$on("ccSetdomainHasTemplateAssigned", (event: ng.IAngularEvent, data: boolean): void => {
        $scope.domainHasTemplateAssigned = data;
        cookieSideMenuHelperService.updateSideMenuItems($scope);
    });

    $scope.handleSearch = (model: string = ""): void => {
        $scope.searchString = model;
        $scope.page = 0;
        $scope.handleSortSearch({ sortKey: $scope.sortKey, sortOrder: $scope.sortOrder });
    };

    $scope.handleSortSearch = (event: IDomainSort): void => {
        $scope.Ready = false;
        $scope.sortKey = event.sortKey;
        $scope.sortOrder = event.sortOrder;
        $scope.loadWebsitesList(true, false);
    };

    $scope.handlePageChange = (page: number): void => {
        $scope.page = page;
        $scope.handleSortSearch({ sortKey: $scope.sortKey, sortOrder: $scope.sortOrder });
    };

    $scope.$watch("model", () => {
        $scope.scanInProgress = find($scope.model.domainList, (domain: any): boolean => {
            return (domain.status === CookieScanStatus.InProgress || domain.status === CookieScanStatus.Pending);
        }) != null;
        cookiesStorageService.model.next($scope.model);
    });

    $scope.$watch("consentNotice", () => {
        cookiesStorageService.consentNotice.next($scope.consentNotice);
    });

    $scope.loadWebsitesListCallback = (showLoading: boolean = false): void => {
        if (showLoading) { $scope.Ready = false; }
        $scope.loadWebsitesList(true, true);
    };

    $scope.updateBannerWarningClosedStatus = (): void => {
        $scope.model.summaryReport.isBannerWarningClosed = true;
    };

    $scope.loadWebsitesList = (reloadData: boolean = true, showErrors: boolean = true, deleteWebsite: boolean = false): ng.IPromise<void> => {
        cookiesStorageService.loadingWebsites.next(true);
        if (permissions.canShow("CookieAuditSingle")) {
            $scope.CanAddWebsites = false;
        }
        if (deleteWebsite && $scope.model.domainList.length === 1) {
            $scope.page --;
        }
        if (reloadData) {
            return webAuditService.getWebsiteList(showErrors, $scope.showNewWebsiteList, $scope.sortKey, $scope.sortOrder, $scope.searchString, $scope.page, $scope.size).then((data: any): void => {
                $scope.model.websiteListLoaded = true;
                if ($scope.showNewWebsiteList) {
                    $scope.model.domainList = data.content;
                    $scope.model.pagination = data;
                } else {
                    $scope.model.domainList = applySort(data);
                }
                $scope.model.domainSelectionList = webAuditService.validDomainFilter($scope.model.domainList);
                $scope.Ready = true;
                $scope.domainListCount = $scope.model.domainList.length;
                $scope.scanInProgress = find($scope.model.domainList, (domain: any): boolean => {
                    return (domain.status === CookieScanStatus.InProgress || domain.status === CookieScanStatus.Pending);
                }) != null;
                if (permissions.canShow("CookieAuditSingle")) {
                    $scope.CanAddWebsites = $scope.domainListCount === 0;
                }
                cookiesStorageService.loadingWebsites.next(false);
            });
        } else {
            $scope.model.domainList = applySort($scope.model.domainList);
            $scope.model.domainSelectionList = webAuditService.validDomainFilter($scope.model.domainList);
            $scope.Ready = true;
            $scope.domainListCount = $scope.model.domainList.length;
            cookiesStorageService.loadingWebsites.next(false);
        }
    };

    $scope.updateSort = (sortType: string, sortReverse: boolean): void => {
        $scope.sortReverse = sortReverse;
        $scope.sortType = sortType;

        $scope.loadWebsitesList(false);
    };

    $scope.init = (): void => {
        $scope.loadWebsitesList().then(() => {
            $scope.$on("$destroy", $scope.$watch("model.domainList", (): void => {
                if ($scope.domainListCount > 0 && (!$scope.model.selectedWebsite || !some($scope.model.domainList, { auditDomain: $scope.model.selectedWebsite }))) {
                    $scope.model.selectedWebsite = $scope.model.domainSelectionList[0].auditDomain;
                } else if ($scope.domainListCount === 0 && $scope.model.selectedWebsite) {
                    $scope.model.selectedWebsite = "";
                }
                cookieComplianceStore.setWebsiteList($scope.model.domainList);
                $scope.domainReadyForBanner = checkDomainReadyForBannerAccess($scope.model.selectedWebsite);
                if (!$scope.hasSetArchiveFlag) {
                    $scope.domainHasScriptArchive = checkDomainHasScriptArchive($scope.model.selectedWebsite);
                }
                $scope.domainHasTemplateAssigned = checkDomainAssignedToTemplate($scope.model.selectedWebsite);
            }));
            $scope.$on("$destroy", $scope.$watch("model.selectedWebsite", (): void => {
                cookieComplianceStore.setSelectedWebsite($scope.model.selectedWebsite);
                $scope.domainReadyForBanner = checkDomainReadyForBannerAccess($scope.model.selectedWebsite);
                $scope.domainHasScriptArchive = checkDomainHasScriptArchive($scope.model.selectedWebsite);
                const isDomainAssignedToTemplate = $scope.domainHasTemplateAssigned;
                $scope.domainHasTemplateAssigned = checkDomainAssignedToTemplate($scope.model.selectedWebsite);
                if ($scope.model.selectedWebsite) {
                    $scope.loadScanHistoryList($scope.model.selectedWebsite);
                    $scope.model.selectedRowKey = "";
                    $scope.model.reportPartsSelectionList = validReportPartsFilter($scope.reportParts);
                }
                if ($scope.isMenuListUpdated !== !!$scope.model.selectedWebsite || isDomainAssignedToTemplate !== $scope.domainHasTemplateAssigned) {
                    $scope.isMenuListUpdated = !!$scope.model.selectedWebsite;
                    cookieSideMenuHelperService.updateSideMenuItems($scope);
                }
            }));
        });
        $scope.$on("SCAN_NOW_STATUS", (): void => {
            scanNowTrigger = true;
        });
        cookieSideMenuHelperService.updateSideMenuItems($scope);
        wootric.run(WootricModuleMap.CookieConsent);
    };

    function checkDomainReadyForBannerAccess(domain: string): boolean {
        if (!domain || !$scope.model.domainList) { return false; }
        const domainDetails: IDomain = webAuditService.getSelectedDomainDetails(
            domain, $scope.model.domainList);
        return domainDetails.status === CookieScanStatus.Complete || domainDetails.lastSuccessfulScanDate !== null;
    }

    function checkDomainHasScriptArchive(domain: string): boolean {
        if (!domain || !$scope.model.domainList) { return false; }
        const domainDetails: IDomain = webAuditService.getSelectedDomainDetails(
            domain, $scope.model.domainList);
        return domainDetails.hasScriptArchive;
    }

    function checkDomainAssignedToTemplate(domain: string): boolean {
        if (!domain || !$scope.model.domainList) { return false; }
        const domainDetails: IDomain = webAuditService.getSelectedDomainDetails(
            domain, $scope.model.domainList);
        return domainDetails.templateId !== 0;
    }

    $scope.launchCookieCompliance = (showAdvancedSettings: boolean, isLivePreview: boolean): void => {
        webAuditService.launchCookieCompliance(cookieComplianceStore.getSelectedDomainId(), showAdvancedSettings, isLivePreview);
    };

    $scope.goTo = (route: string, toParams: any): void => {
        $state.go(route, toParams);
    };

    $scope.createOrManageBanner = (): void => {
        const domainDetails = webAuditService.getSelectedDomainDetails(
            $scope.model.selectedWebsite,
            $scope.model.domainList);
        if (!domainDetails.domainId && (domainDetails.status === CookieScanStatus.InProgress || domainDetails.status === CookieScanStatus.Pending)) {
            $scope.goTo("zen.app.pia.module.cookiecompliance.views.cookieEmptyState", {pageName: $scope.pageNames.cookieBanner, isDomainCreated: false});
        } else if (domainDetails.domainId && permissions.canShow("CookieBannerEditor")) {
            $scope.goTo("zen.app.pia.module.cookiecompliance.views.banner");
        } else {
            $scope.goTo("zen.app.pia.module.cookiecompliance.views.layout");
        }
    };

    $scope.goToReport = (domain: any, selectedRowKey: string = "", reScanTrigger: boolean = false): void => {
         if (domain && domain.domain) {
            scanNowTrigger = domain.reScanTrigger;
            $scope.model.selectedWebsite = domain.domain;
            $scope.model.selectedRowKey = domain.selectedRowKey;
         } else {
            scanNowTrigger = reScanTrigger;
            $scope.model.selectedWebsite = domain;
            $scope.model.selectedRowKey = selectedRowKey;
         }
         $scope.goTo("zen.app.pia.module.cookiecompliance.views.reports");
    };

    $scope.manageConsentBanner = (domain: string): void => {
        $scope.model.selectedWebsite = domain;
        if (permissions.canShow("CookieBannerEditor")) {
            $scope.goTo("zen.app.pia.module.cookiecompliance.views.banner");
        } else {
            $scope.goTo("zen.app.pia.module.cookiecompliance.views.layout");
        }
    };

    $scope.createConsentBanner = (domain: string): void => {
        $scope.model.selectedWebsite = domain;
        if (checkDomainReadyForBannerAccess(domain)) {
            $scope.goTo("zen.app.pia.module.cookiecompliance.views.layout");
        } else {
            $scope.goTo("zen.app.pia.module.cookiecompliance.views.reports");
        }
    };

    subscriptions.push(cookiesStorageService.bannerDomain.subscribe((domain: string) => {
        if (domain) {
            $scope.model.selectedWebsite = domain;
        }
    }));

    $scope.switchCookiePolicyEditor = (): void => {
        const domainDetails: IDomain = webAuditService.getSelectedDomainDetails(
            $scope.model.selectedWebsite,
            $scope.model.domainList);
        if (!domainDetails.domainId && domainDetails.status === CookieScanStatus.InProgress || domainDetails.status === CookieScanStatus.Pending) {
            $scope.goTo("zen.app.pia.module.cookiecompliance.views.cookieEmptyState", {pageName: $scope.pageNames.cookieDisclosure, isDomainCreated: false });
        } else if (!domainDetails.domainId && domainDetails.status === CookieScanStatus.Complete) {
            $scope.goTo("zen.app.pia.module.cookiecompliance.views.cookieEmptyState", {pageName: $scope.pageNames.cookieDisclosure, isDomainCreated: true });
        } else if (domainDetails.domainId && permissions.canShow("CookiePolicyEditor")) {
            $scope.goTo("zen.app.pia.module.cookiecompliance.views.policyeditor");
        } else if (!permissions.canShow("CookiePolicyEditor")) {
            $scope.goTo("zen.app.pia.module.cookiecompliance.views.policy");
        } else if (checkDomainReadyForBannerAccess($scope.model.selectedWebsite)) {
            $scope.goTo("zen.app.pia.module.cookiecompliance.views.layout");
        } else {
            $scope.goTo("zen.app.pia.module.cookiecompliance.views.reports");
        }
    };

    $scope.switchPreferenceCenterEditor = (): void => {
        const domainDetails: IDomain = webAuditService.getSelectedDomainDetails(
            $scope.model.selectedWebsite,
            $scope.model.domainList);
        if (!domainDetails.domainId && domainDetails.status === CookieScanStatus.InProgress || domainDetails.status === CookieScanStatus.Pending) {
            $scope.goTo("zen.app.pia.module.cookiecompliance.views.cookieEmptyState", {pageName: $scope.pageNames.preferenceCenter, isDomainCreated: false });
        }  else if (!domainDetails.domainId && domainDetails.status === CookieScanStatus.Complete) {
            $scope.goTo("zen.app.pia.module.cookiecompliance.views.cookieEmptyState", {pageName: $scope.pageNames.preferenceCenter, isDomainCreated: true });
        }  else if ($scope.domainHasTemplateAssigned) {
            $scope.goTo("zen.app.pia.module.cookiecompliance.views.cookieEmptyState", {pageName: $scope.pageNames.preferenceCenter, isDomainCreated: true, isTemplateAssigned: true });
        } else if (domainDetails.domainId && permissions.canShow("PreferenceCenterEditor")) {
            $scope.goTo("zen.app.pia.module.cookiecompliance.views.prefcentereditor");
        } else if (!permissions.canShow("PreferenceCenterEditor")) {
            $scope.goTo("zen.app.pia.module.cookiecompliance.views.prefcenter");
        } else if (checkDomainReadyForBannerAccess($scope.model.selectedWebsite)) {
            $scope.goTo("zen.app.pia.module.cookiecompliance.views.layout");
        } else {
            $scope.goTo("zen.app.pia.module.cookiecompliance.views.reports");
        }
    };

    $scope.switchIABVendorsListPage = (): void => {
        $scope.goTo("zen.app.pia.module.cookiecompliance.views.iabvendorslist");
    };

    $scope.switchScriptArchivePage = (): void => {
        const domainDetails: IDomain = webAuditService.getSelectedDomainDetails(
            $scope.model.selectedWebsite,
            $scope.model.domainList);
        if (!domainDetails.domainId && domainDetails.status === CookieScanStatus.InProgress || domainDetails.status === CookieScanStatus.Pending) {
            $scope.goTo("zen.app.pia.module.cookiecompliance.views.cookieEmptyState", {pageName: $scope.pageNames.scriptArchive, isDomainCreated: false });
        } else if (!domainDetails.domainId && domainDetails.status === CookieScanStatus.Complete) {
            $scope.goTo("zen.app.pia.module.cookiecompliance.views.cookieEmptyState", {pageName: $scope.pageNames.scriptArchive, isDomainCreated: true });
        } else {
            $scope.goTo("zen.app.pia.module.cookiecompliance.views.archive");
        }
    };

    $scope.manageCookiePolicy = (domain: string): void => {
        $scope.model.selectedWebsite = domain;
        cookieComplianceStore.setSelectedWebsite($scope.model.selectedWebsite);
        $scope.switchCookiePolicyEditor();
    };

    $scope.managePreferenceCenter = (domain: string): void => {
        $scope.model.selectedWebsite = domain;
        cookieComplianceStore.setSelectedWebsite($scope.model.selectedWebsite);
        $scope.switchPreferenceCenterEditor();
    };

    $scope.calculateProgress = (domain: IDomain): string => {
        const upperLimit: number = domain.auditedPages < 50 ? Math.min(100, domain.pageLimit)
            : domain.auditedPages < 250 ? Math.min(500, domain.pageLimit)
                : domain.pageLimit;
        return (domain.auditedPages / upperLimit * 100) + "%";
    };

    $scope.$on("$destroy", (): void => {
        destroyed = true;
        $timeout.cancel(timeout);
        subscriptions.forEach((sub: Subscription) => {
            sub.unsubscribe();
        });
    });

    $scope.updateProgress = (): void => {
        $timeout.cancel(timeout);
        webAuditService.getSingleWebsiteAudit($scope.model.selectedWebsite).then((data: any): void => {
            $scope.CurrentDomain = data;
            $scope.scanInProgress = $scope.CurrentDomain && $scope.CurrentDomain.status === CookieScanStatus.InProgress;
            $scope.scanPending = $scope.CurrentDomain && $scope.CurrentDomain.status === CookieScanStatus.Pending;
            if ($scope.CurrentDomain && $scope.CurrentDomain.status === CookieScanStatus.Complete && scanNowTrigger) {
                scanNowTrigger = false;
                $scope.loadReport($scope.model.selectedWebsite);
            }
            if (($scope.scanInProgress || $scope.scanPending) && !destroyed) {
                timeout = $timeout($scope.updateProgress, 10000);
            }
        });
    };

    $scope.reauditWebsite = (domain: string, loadWebsiteList: boolean = true): void => {
        const url = "reauditWebsiteModal.html";
        const modalInstance = $uibModal.open({
            backdrop: "static",
            controller: "AddWebsiteCtrl",
            keyboard: false,
            resolve: {
                DataProvider: () => {
                    return {
                        Domain: domain,
                    };
                },
            },
            scope: $scope,
            size: "md",
            templateUrl: url,
        });
        modalInstance.result.then((response: any) => {
            if (loadWebsiteList) {
                $scope.loadWebsitesList();
            } else {
                $scope.updateProgress();
            }
        });
    };

    $scope.setOrUpdateNextScan = (domain: string): void => {
        $q.all([userActionService.fetchUsers(), cookieAuditScheduleService.getScheduledScan(domain, $scope.canTenantLevelScheduledScan)])
            .then((response): void => {
                let scheduleData = null;
                if (response[1]) {
                    scheduleData = { ...response[1], domain, canTenantLevelScheduledScan: $scope.canTenantLevelScheduledScan };
                }

                modalService.setModalData({
                    scheduleData,
                    userList: response[0].data,
                    promiseToResolve: (): void => $scope.loadWebsitesList(true),
                });
                modalService.openModal("ScheduleAuditModal");

            });
    };

    $scope.loadReportMain = (reload: boolean = true): Promise<boolean> => {
        // Don't try to load report until website selected
        if (!$scope.model.selectedWebsite) {
            $scope.reportReady = false;
            return;
        }

        $scope.model.reportPartsSelectionList = validReportPartsFilter($scope.reportParts);

        // Get scanning status of current domain
        $scope.updateProgress();

        // If we are already viewing the domain and it isn't in progress then no need to refresh
        if ((!$scope.scanInProgress && !$scope.scanPending) && $scope.model.summaryReport && $scope.model.summaryReport.domain === $scope.model.selectedWebsite && !reload) {
            $scope.reportReady = true;
            return;
        }

        // Clear previous report
        $scope.reportReady = false;
        $scope.model.PrivacyPolicyNotes = "";
        $scope.model.CookiePolicyNotes = "";
        $scope.model.CookieNoticeNotes = "";

        return $scope.getSummaryReport($scope.model.selectedWebsite).then((data: any): void => {
            if (data) {
                $scope.model.summaryReport = data;
                $scope.reportReady = true;

                if ($scope.model.summaryReport.privacyPolicy && $scope.model.summaryReport.privacyPolicyLinkCount > 0.0) {
                    $scope.model.PrivacyPolicyNotes = $rootScope.t("DetectedPrivacyPolicyInSeveralPages");
                } else if ($scope.model.summaryReport.privacyPolicy) {
                    $scope.model.PrivacyPolicyNotes = $rootScope.t("DetectedPrivacyPolicyOnThisSite");
                } else {
                    $scope.model.PrivacyPolicyNotes = $rootScope.t("NotDetectedPrivacyPolicy");
                }

                if (!$scope.model.summaryReport.cookieDetectVersion) {
                    $scope.model.CookiePolicyNotes = $rootScope.t("NotYetScannedForCookiePolicy");
                } else if ($scope.model.summaryReport.cookiePolicy) {
                    $scope.model.CookiePolicyNotes = $rootScope.t("DetectedDedicatedCookiePolicy");
                } else {
                    $scope.model.CookiePolicyNotes = $rootScope.t("NotDetectedCookiePolicy");
                }

                if (!$scope.model.summaryReport.cookieDetectVersion) {
                    $scope.model.CookieNoticeNotes = $rootScope.t("NotYetScannedForCookieBanner");
                } else if ($scope.model.summaryReport.cookieNotice) {
                    $scope.model.CookieNoticeNotes = $rootScope.t("DetectedCookieBanner");
                } else {
                    $scope.model.CookieNoticeNotes = $rootScope.t("NotDetectedCookieBanner");
                }
            }
        });
    };

    $scope.getSummaryReport = (domain: string): any => {
        return webAuditService.getSummaryReport($scope.model.selectedWebsite, $scope.model.selectedRowKey);
    };

    function validReportPartsFilter(reportParts: any): any[] {
        return filter(reportParts, (reportPart: any): boolean => (reportPart.value !== CookieParts.Forms && reportPart.value !== CookieParts.Pages && reportPart.value !== CookieParts.LocalStorage) || !$scope.model.selectedRowKey);
    }

    function validDomainHistoryListFilter(historyList: any): any[] {
        return filter(historyList, (historyItem: any): boolean => historyItem.rowKey === "" || ($scope.model.selectedPart === CookieParts.Dashboard || $scope.model.selectedPart === CookieParts.Cookies || $scope.model.selectedPart === CookieParts.Tags));
    }

    $scope.loadReport = (selection: any): void => {
        $scope.model.selectedWebsite = selection;
        $scope.loadScanHistoryList(selection);
        $scope.model.selectedRowKey = "";
        $scope.$broadcast("LOAD_REPORT");
    };

    $scope.loadScanHistoryList = (selectedDomain: string): any => {
        return webAuditService.getScanHistoryList(selectedDomain).then((data: any): any => {
            webAuditService.getSingleWebsiteAudit($scope.model.selectedWebsite).then((domainDetails: any): void => {
                data.push({
                    auditDomain: selectedDomain,
                    status: domainDetails.status,
                    completedTime: domainDetails.completedTime,
                    scanDateTimeInUserTimezone: webAuditService.getDateTimeInUserTimezone(domainDetails.completedTime),
                    rowKey: "",
                });
                domainHistoryList = orderBy(data, ["completedTime"], "desc").filter((e: any): any => e.status !== CookieScanStatus.Cancelled);
                $scope.model.domainHistorySelectionList = validDomainHistoryListFilter(domainHistoryList);
            });
        });
    };

    $scope.loadScanHistoryReport = (selection: string): void => {
        $scope.model.selectedRowKey = selection;
        $scope.model.reportPartsSelectionList = validReportPartsFilter($scope.reportParts);
        $scope.$broadcast("LOAD_REPORT", true);
    };

    $scope.reassignOrgGroup = (domain: IDomain): void => {
        modalService.setModalData({
            domainName: domain.auditDomain,
            organizationUUID: domain.organizationUUID,
            promiseToResolve: (): void => $scope.loadWebsitesList(true),
        });

        modalService.openModal("OrgGroupReAssignModal");
    };

    $scope.scanLoginDetails = (domain: IDomain): void => {
        const isExisting: boolean = (domain.scanLoginInfoStatus === ScanBehindLoginStatus.Verified || domain.scanLoginInfoStatus === ScanBehindLoginStatus.NotVerified || domain.scanLoginInfoStatus === ScanBehindLoginStatus.Failed);
        $scope.goTo("zen.app.pia.module.cookiecompliance.views.scanlogin", { domain: decodeURIComponent(domain.auditDomain), isnew: !isExisting });
    };
    $scope.canShowViewResults = (domain: IDomain): boolean => {
        return ((domain.status === CookieScanStatus.Complete || domain.status === CookieScanStatus.Cancelled) && (!domain.scanError && domain.scanLoginInfoStatus !== ScanBehindLoginStatus.Failed));
    };

    $scope.canShowScanErrorMessage = (domain: IDomain): boolean => {
        return ((domain.status === CookieScanStatus.Complete || domain.status === CookieScanStatus.Cancelled) && domain.scanError !== "");
    };

    $scope.canShowLoginFailedMessage = (domain: IDomain): boolean => {
        return ((domain.status === CookieScanStatus.Complete || domain.status === CookieScanStatus.Cancelled) && (!domain.scanError && domain.scanLoginInfoStatus === ScanBehindLoginStatus.Failed));
    };

    if (permissions.canShow("UniversalMigratedCookieModule")) {
      $window.location.href = "/cookies/index.html";
    } else {
      $scope.init();
    }
}

CookieComplianceCtrl.$inject = [
    "$scope",
    "$rootScope",
    "$state",
    "$q",
    "$window",
    "WebAuditService",
    "Permissions",
    "GenFactory",
    "$uibModal",
    "$timeout",
    "CookieComplianceStore",
    "ModalService",
    "CookieAuditScheduleService",
    "UserActionService",
    "CookieSideMenuHelperService",
    "Wootric",
    "CookiesStorageService",
];
