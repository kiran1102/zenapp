import * as _ from "lodash";
import { WebAuditService } from "cookiesService/web-audit.service";
import { ModalService } from "sharedServices/modal.service";
import { Content } from "sharedModules/services/provider/content.service";

import { CookieParts } from "constants/cookie.constant";

import { IExtendedRootScopeService } from "interfaces/root-scope.interface";

export default class CookieComplianceReportsCtrl {
    constructor(
        $scope: any,
        $rootScope: IExtendedRootScopeService,
        $timeout: ng.ITimeoutService,
        WebAuditService: WebAuditService,
        Content: Content,
        ModalService: ModalService,
    ) {
        const timeout: any = null;
        const destroyed = false;
        $scope.reportExplanationUrl = Content.GetKey("CookiesReportExplanation");
        $scope.model.selectedPart = CookieParts.Dashboard;

        function addhttp(url: string): string {
            if (!/^(?:ht)tps?\:\/\//.test(url)) {
                url = "http://" + url;
            }
            return url;
        }

        $scope.goToCookieDetailsReport = (): void => {
            $scope.loadReportPart(CookieParts.Cookies);
        };

        $scope.goToTagDetailsReport = (): void => {
            $scope.loadReportPart(CookieParts.Tags);
        };

        $scope.goToFormDetailsReport = (): void => {
            $scope.loadReportPart(CookieParts.Forms);
        };

        $scope.goToPageDetailsReport = (): void => {
            $scope.loadReportPart(CookieParts.Pages);
        };

        $scope.checkForReportDataChanges = (): boolean => {
            const domain = WebAuditService.getSelectedDomainDetails($scope.model.selectedWebsite, $scope.model.domainList);
            if (!_.isUndefined($scope.model.summaryReport) && !_.isUndefined(domain) && $scope.model.summaryReport.pagesScanned === domain.auditedPages && $scope.model.summaryReport.cookies === domain.cookiesFound) {
                return false;
            }
            return true;
        };

        $scope.init = (): void => {
            if ($scope.model.selectedWebsite) {
                $scope.loadScanHistoryList($scope.model.selectedWebsite);
                $scope.loadReportMain($scope.checkForReportDataChanges());
            } else {
                $scope.$watch("model.domainList", (): void => {
                    $scope.loadReportMain();
                });
            }
        };

        $scope.$on("LOAD_REPORT", (event: ng.IAngularEvent): void => {
            $scope.loadReportMain();
        });

        $scope.reloadReport = (): void => {
            $scope.debounceRefresh = true;
            $scope.loadReportMain().then((): void => {
                $scope.debounceRefresh = false;
            });
        };

        $scope.showInfoModal = (modalType: string): void => {
            let modalInstance;
            const data: any = {
                ModalType: modalType,
            };

            modalInstance = ModalService.renderModal(data, "InfoModalCtrl", "category_details_modal.html", "InfoModal");
        };

        $scope.init();
    }
}

CookieComplianceReportsCtrl.$inject = ["$scope", "$rootScope", "$timeout", "WebAuditService", "Content", "ModalService"];
