import { WebAuditService } from "cookiesService/web-audit.service";

import { CookieParts } from "constants/cookie.constant";

export default class CookieCompliancePagesCtrl {
    constructor(
        $q: ng.IQService,
        $scope: any,
        WebAuditService: WebAuditService,
    ) {
        $scope.reportReady = false;
        $scope.PagesReport = [];
        $scope.model.selectedPart = CookieParts.Pages;

        $scope.loadReportPage = (): void => {
            if (!$scope.model.selectedWebsite) return;

            const promises: Array<ng.IPromise<any>> = [];

            $scope.reportReady = false;

            if (!$scope.model.summaryReport || $scope.model.summaryReport.domain !== $scope.model.selectedWebsite) {
                // Get scanning status of current domain
                $scope.updateProgress();
                promises.push($scope.getSummaryReport($scope.model.selectedWebsite).then((data: any): void => {
                    $scope.model.summaryReport = data;
                }));
            }

            promises.push(WebAuditService.getPagesReport($scope.model.selectedWebsite).then((data: any): void => {
                $scope.PagesReport = data;
            }));

            $q.all(promises).then((): void => {
                $scope.reportReady = true;
                $scope.debounceRefresh = false;
            });
        };

        $scope.init = (): void => {
            if ($scope.model.selectedWebsite) {
                $scope.loadReportPage();
            } else {
                $scope.$watch("model.domainList", (): void => {
                    $scope.loadReportPage();
                });
            }
        };

        $scope.$on("LOAD_REPORT", (event: ng.IAngularEvent): void => {
            $scope.loadReportPage();
        });

        $scope.reloadReport = (): void => {
            $scope.debounceRefresh = true;
            $scope.reportReady = false;
            $scope.loadReportMain().then((): void => {
                $scope.loadReportPage();
            });
        };

        $scope.init();
    }
}

CookieCompliancePagesCtrl.$inject = ["$q", "$scope", "WebAuditService"];
