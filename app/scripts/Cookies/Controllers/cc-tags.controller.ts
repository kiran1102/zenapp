import { WebAuditService } from "cookiesService/web-audit.service";

import { CookieParts } from "constants/cookie.constant";

export default class CookieComplianceTagsCtrl {
    constructor(
        $q: ng.IQService,
        $scope: any,
        WebAuditService: WebAuditService,
    ) {
        $scope.reportReady = false;
        $scope.CookieDetailReport = {};
        $scope.PurposeList = [];
        $scope.model.selectedPart = CookieParts.Tags;

        $scope.loadReportTags = (reloadSummaryReport: boolean = false): void | undefined => {
            if (!$scope.model.selectedWebsite) return;

            const promises: Array<ng.IPromise<any>> = [];

            $scope.reportReady = false;

            if (!$scope.model.summaryReport || $scope.model.summaryReport.domain !== $scope.model.selectedWebsite || reloadSummaryReport) {
                // Get scanning status of current domain
                $scope.updateProgress();
                promises.push($scope.getSummaryReport($scope.model.selectedWebsite).then((data: any): void => {
                    $scope.model.summaryReport = data;
                }));
            }

            promises.push(WebAuditService.getTagDetailReport($scope.model.selectedWebsite, $scope.model.selectedRowKey).then((data: any): void => {
                $scope.TagDetailReport = data;
            }));

            $q.all(promises).then((data: any) => {
                $scope.reportReady = true;
                $scope.debounceRefresh = false;
            });
        };

        $scope.init = (): void => {
            if ($scope.model.selectedWebsite) {
                $scope.loadReportTags();
            } else {
                $scope.$watch("model.domainList", () => {
                    $scope.loadReportTags();
                });
            }
        };

        $scope.$on("LOAD_REPORT", (event: ng.IAngularEvent, reloadSummaryReport: boolean = false): void => {
            $scope.loadReportTags(reloadSummaryReport);
        });

        $scope.reloadReport = (): void => {
            $scope.debounceRefresh = true;
            $scope.reportReady = false;
            $scope.loadReportMain().then((): void => {
                $scope.loadReportTags();
            });
        };

        $scope.init();
    }
}

CookieComplianceTagsCtrl.$inject = ["$q", "$scope", "WebAuditService"];
