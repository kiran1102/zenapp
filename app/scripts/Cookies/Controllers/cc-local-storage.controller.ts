import { WebAuditService } from "cookiesService/web-audit.service";
import { CookieParts } from "constants/cookie.constant";

export default class CookieComplianceLocalStorageCtrl {
    constructor(
        $q: ng.IQService,
        $scope: any,
        webAuditService: WebAuditService,
    ) {
        $scope.reportReady = false;
        $scope.PagesReport = [];
        $scope.model.selectedPart = CookieParts.LocalStorage;
        $scope.sessionStorageReport = [];
        $scope.localStorageReport = [];

        $scope.loadReportLocalStorage = (): void => {
            if (!$scope.model.selectedWebsite) return;

            const promises: Array<ng.IPromise<any>> = [];

            $scope.reportReady = false;

            if (!$scope.model.summaryReport || $scope.model.summaryReport.domain !== $scope.model.selectedWebsite) {
                // Get scanning status of current domain
                $scope.updateProgress();
                promises.push($scope.getSummaryReport($scope.model.selectedWebsite).then((data: any): void => {
                    $scope.model.summaryReport = data;
                }));
            }

            promises.push(webAuditService.getLocalStorageReport($scope.model.selectedWebsite).then((data: any): void => {
                $scope.localStorageReport  = data.storageEntries ? data.storageEntries.filter((storage: any) => (!storage.storageType || storage.storageType === "local")) : data.storageEntries;
                $scope.sessionStorageReport = data.storageEntries ? data.storageEntries.filter((storage: any) => storage.storageType === "session") : data.storageEntries;
            }));

            $q.all(promises).then((): void => {
                $scope.reportReady = true;
                $scope.debounceRefresh = false;
            });
        };

        $scope.init = (): void => {
            if ($scope.model.selectedWebsite) {
                $scope.loadReportLocalStorage();
            } else {
                $scope.$watch("model.domainList", (): void => {
                    $scope.loadReportLocalStorage();
                });
            }
        };

        $scope.$on("LOAD_REPORT", (event: ng.IAngularEvent): void => {
            $scope.loadReportLocalStorage();
        });

        $scope.reloadReport = (): void => {
            $scope.debounceRefresh = true;
            $scope.reportReady = false;
            $scope.loadReportMain().then((): void => {
                $scope.loadReportLocalStorage();
            });
        };

        $scope.init();
    }
}

CookieComplianceLocalStorageCtrl.$inject = ["$q", "$scope", "WebAuditService"];
