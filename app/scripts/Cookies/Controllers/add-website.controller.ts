import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import OrgGroupStoreNew from "oneServices/org-group.store";
import { CookieComplianceStore } from "cookiesService/cookie-compliance-store.service";
import { find } from "lodash";

export default function AddWebsiteCtrl(
    $scope: any,
    $rootScope: IExtendedRootScopeService,
    $uibModalInstance: any,
    WebAuditService: any,
    DataProvider: any,
    $sce: ng.ISCEService,
    orgGroupStoreNew: OrgGroupStoreNew,
    cookieComplianceStore: CookieComplianceStore,
): void {
    $scope.addWebsiteModel = {
        ClearPreviousHistory: false,
        Domain: DataProvider.Domain || "",
        IndependentPaths: false,
        PageLimit: 1000,
        DebounceSubmit: false,
        AdvancedSettingsExpanded: false,
        TogglePageLimit: false,
        IncludedQueryParams: "",
        SecondaryUris: "",
        OrgList: orgGroupStoreNew.orgList,
        selectedOrg: orgGroupStoreNew.orgList[0],
        SitemapUris: "",
    };

    $scope.init = (): void => {
        const maxPageLimit = 10000;
        if (DataProvider.Domain) {
            const domain: any = cookieComplianceStore.getDomainByName(DataProvider.Domain);
            $scope.addWebsiteModel.Domain = domain.auditDomain;
            if (domain.pageLimit < maxPageLimit) {
                $scope.addWebsiteModel.PageLimit = domain.pageLimit;
                $scope.addWebsiteModel.TogglePageLimit = true;
            }
            $scope.addWebsiteModel.IncludedQueryParams = domain.includedQueryParams;
            $scope.addWebsiteModel.SecondaryUris = domain.secondaryUris;
            $scope.addWebsiteModel.SitemapUris = domain.siteMapsUris;
            $scope.addWebsiteModel.selectedOrg = find(orgGroupStoreNew.orgList, (orgnization: any): boolean => orgnization.id ===  domain.organizationUUID);
        }
    };

    $scope.orgSelected = (payload: any): void => {
        $scope.addWebsiteModel.selectedOrg = payload.org;
    };

    $scope.PageLimitValidation = "";
    $scope.EnterWebSiteToolTip = $sce.trustAsHtml(`<div class='text-left'>${$rootScope.t("EnterWebSiteToolTipSentence1")} <br>
		${$rootScope.t("EnterWebSiteToolTipSentence2")} <br> ${$rootScope.t("EnterWebSiteToolTipSentence3")}</div>`);
    $scope.LimitScanToolTip = $sce.trustAsHtml(`<div class='text-left'>${$rootScope.t("LimitScanToolTip")}</div>`);
    $scope.LimitToThisPathToolTip = $sce.trustAsHtml(`<div class='text-left'>${$rootScope.t("LimitToThisPathToolTip")}</div>`);
    $scope.ClearPreviousScanToolTip = $sce.trustAsHtml(`<div class='text-left'>${$rootScope.t("ClearPreviousScanToolTip")}</div>`);
    $scope.IncludedQueryParamsToolTip = $sce.trustAsHtml(`<div class='text-left'>${$rootScope.t("IncludedQueryParamsToolTip")}</div>`);
    $scope.SecondaryUrisToolTip = $sce.trustAsHtml(`<div class='text-left'>${$rootScope.t("SecondaryUrisToolTip")}</div>`);
    $scope.SiteMapUrisToolTip = $sce.trustAsHtml(`<div class='text-left'>${$rootScope.t("SiteMapUrisToolTip")}</div>`);
    $scope.InvalidDomainName = false;
    $scope.domainExistOrg = false;

    $scope.done = (): void => {
        $uibModalInstance.close();
    };

    $scope.submitWebsite = (reaudit: boolean = false): void => {
        if (WebAuditService.validateDomainName($scope.addWebsiteModel.Domain) ||
            WebAuditService.validateIPAddress($scope.addWebsiteModel.Domain)) {
            $scope.InvalidDomainName = false;
            $scope.addWebsiteModel.DebounceSubmit = true;

            if (reaudit) {
                $scope.addWebsiteModel.IndependentPaths = true;
            }

            const pageLimit: number = $scope.addWebsiteModel.TogglePageLimit ? $scope.addWebsiteModel.PageLimit : 0;
            $scope.scanInProgress = true;
            WebAuditService.addWebsite($scope.addWebsiteModel.Domain, pageLimit, !$scope.addWebsiteModel.ClearPreviousHistory,
                $scope.addWebsiteModel.IndependentPaths, $scope.addWebsiteModel.selectedOrg.id,
                $scope.addWebsiteModel.IncludedQueryParams, $scope.addWebsiteModel.SecondaryUris, reaudit, $scope.addWebsiteModel.SitemapUris)
                .then((response: any): void => {
                    if (response.ok) {
                        $scope.done();
                        if (reaudit) {
                            $scope.$emit("SCAN_NOW_STATUS");
                        }
                    } else {
                        $scope.addWebsiteModel.DebounceSubmit = false;
                        $scope.scanInProgress = false;
                        $scope.domainExistOrg = true;
                    }
                });
        } else {
            $scope.InvalidDomainName = true;
        }
    };

    $scope.toggleAdvancedSettings = (): void => {
        if (!$scope.enterKeyPressed) {
            $scope.addWebsiteModel.AdvancedSettingsExpanded = !$scope.addWebsiteModel.AdvancedSettingsExpanded;
        }
        $scope.enterKeyPressed = false;
    };

    $scope.doSubmitIfEnterKey = ($event: any, reaudit: boolean = false): void => {
        const keyCode: number = $event.which || $event.keyCode;
        if (keyCode === 13) {
            $scope.enterKeyPressed = true;
            if (!$scope.addForm.$invalid && !$scope.addWebsiteModel.DebounceSubmit) {
                $scope.submitWebsite(reaudit);
            }
        }
    };
    $scope.init();
}

AddWebsiteCtrl.$inject = [
    "$scope",
    "$rootScope",
    "$uibModalInstance",
    "WebAuditService",
    "DataProvider",
    "$sce",
    "OrgGroupStoreNew",
    "CookieComplianceStore",
];
