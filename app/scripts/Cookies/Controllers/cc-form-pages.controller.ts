export default class CookieComplianceFormPagesCtrl {
    static $inject: string[] = [
        "$q",
        "$scope",
        "WebAuditService",
        "$stateParams",
    ];

    constructor(
        $q: ng.IQService,
        $scope: any,
        WebAuditService: any,
        $stateParams: ng.ui.IStateParamsService,
    ) {
        $scope.reportReady = false;
        $scope.formPages = {};
        $scope.parentDomain = "";
        $scope.formName = "";

        $scope.loadReportFormPages = (): void => {
            const promises: Array<ng.IPromise<any>> = [];
            $scope.reportReady = false;

            if (!$scope.parentDomain.length && $scope.model.selectedWebsite.length) {
                $scope.parentDomain = $scope.model.selectedWebsite;
            } else if ($scope.parentDomain !== $scope.model.selectedWebsite || !$scope.model.selectedWebsite.length) {
                $scope.goTo("zen.app.pia.module.cookiecompliance.views.reports");
            }
            if (!$scope.model.summaryReport || $scope.model.summaryReport.domain !== $scope.model.selectedWebsite) {
                // Get scanning status of current domain
                $scope.updateProgress();
                promises.push($scope.getSummaryReport($scope.model.selectedWebsite).then((data: any): void => {
                    $scope.model.summaryReport = data;
                }));
            }
            if ($stateParams.formName) {
                $scope.formName = $stateParams.formName;
                promises.push(WebAuditService.getFormPagesByFormName($scope.model.selectedWebsite, $scope.formName).then((data: any): void => {
                    $scope.formPages = data;
                }));
            }

            $q.all(promises).then((data: any): void => {
                $scope.reportReady = true;
                $scope.debounceRefresh = false;
            });
        };

        $scope.init = (): void => {
            if ($scope.model.selectedWebsite) {
                $scope.loadReportFormPages();
            } else {
                $scope.$watch("model.domainList", (): void => {
                    $scope.loadReportFormPages();
                });
            }
        };

        $scope.$on("LOAD_REPORT", (event: ng.IAngularEvent): void => {
            $scope.loadReportFormPages();
        });

        $scope.reloadReport = (): void => {
            $scope.debounceRefresh = true;
            $scope.reportReady = false;
            $scope.loadReportMain().then((): void => {
                $scope.loadReportFormPages();
            });
        };

        $scope.init();
    }
}
