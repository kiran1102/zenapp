import * as angular from "angular";
import * as _ from "lodash";
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import { IProtocolPacket, IProtocolConfig, IProtocolMessages, IProtocolResponse} from "interfaces/protocol.interface";
import { ProtocolService } from "modules/core/services/protocol.service";
import { IDomainScanSchedule } from "interfaces/cookies/cc-domain-scan-schedule.interface";

export class CookieAuditScheduleService {
    static $inject: string[] = ["OneProtocol", "$rootScope"];
    private Protocol: any;

    constructor(private readonly OneProtocol: ProtocolService, private readonly $rootScope: IExtendedRootScopeService) {
        this.Protocol = OneProtocol;
    }

    public apiRequest(requestMethod: any, requestUrl: string, requestObject: any, object: string, action: string, timeout?: number): ng.IPromise<IProtocolPacket> {
        const config: IProtocolConfig = this.Protocol.config(requestMethod, requestUrl, [], requestObject, undefined, undefined, undefined, undefined, timeout);
        const messages: IProtocolMessages = { Error: { object, action } };
        return this.Protocol.http(config, messages);
    }
    public getScheduledScan(domain: string, canTenantLevelScheduledScan: boolean): ng.IPromise<IProtocolResponse<IDomainScanSchedule>> {
        return this.apiRequest("GET", canTenantLevelScheduledScan ? `/audit/GetScheduledScan` : `/audit/getdomainscheduledscan?domain=${this.encodeDomain(domain)}`, null, this.$rootScope.t("ScheduleScan"), this.$rootScope.t("getting")).then((response: IProtocolPacket): ng.IPromise<IProtocolResponse<IDomainScanSchedule>> => {
            return response.result ? response.data : null;
        });
    }
    public updateSchedule(schedule: IDomainScanSchedule): ng.IPromise<IProtocolResponse<IDomainScanSchedule>> {
        return this.apiRequest("PUT", schedule.canTenantLevelScheduledScan ? `/audit/UpdateSchedule` : `/audit/updatedomainschedulescan`, schedule, this.$rootScope.t("ScheduleScan"), this.$rootScope.t("updating")).then((response: IProtocolPacket): ng.IPromise<IProtocolResponse<IDomainScanSchedule>> => {
            return response.result ? response.data : null;
        });
    }
    public cancelSchedule(schedule: IDomainScanSchedule, canTenantLevelScheduledScan: boolean): ng.IPromise<IProtocolResponse<IDomainScanSchedule>> {
        return this.apiRequest("PUT", canTenantLevelScheduledScan ? `/audit/CancelSchedule` : `/audit/canceldomainschedule?domain=${this.encodeDomain(schedule.domain)}`, null, this.$rootScope.t("ScheduleScan"), this.$rootScope.t("cancelling")).then((response: IProtocolPacket): ng.IPromise<IProtocolResponse<IDomainScanSchedule>> => {
            return response.result ? response.data : null;
        });
    }
    private encodeDomain(domain: string): string {
        return encodeURIComponent(domain).replace(/\./g, "%2e");
    }

}
