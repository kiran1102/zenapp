import { HttpRequest } from "@angular/common/http";

import {
    sortBy,
} from "lodash";

// Services
import { CookieComplianceStore } from "cookiesService/cookie-compliance-store.service";
import { ProtocolService } from "modules/core/services/protocol.service";

// Enums
import { BannerLanguageStatus } from "enums/cookies/cookie-list.enum";

// Interfaces
import {
    IProtocolPacket,
    IProtocolConfig,
    IProtocolMessages,
    IProtocolResponse,
    IProtocolMessage,
} from "interfaces/protocol.interface";
import {
    IBannerLanguage,
    IBannerLanguageRequestData,
    IBannerLanguageResponse,
    IBannerLangReponse,
} from "interfaces/cookies/cc-banner-languages.interface";

export class CookieMultiLanguageService {
    static $inject: string[] = [
        "OneProtocol",
        "$rootScope",
        "CookieComplianceStore",
    ];

    private Protocol: any;

    constructor(
        private readonly OneProtocol: ProtocolService,
        private readonly $rootScope: any,
        private readonly cookieComplianceStore: CookieComplianceStore) {
        this.Protocol = OneProtocol;
    }

    public apiRequest(requestMethod: any, requestUrl: string, requestObject: any, errorMessage: string): ng.IPromise<IProtocolPacket> {
        const config: IProtocolConfig = this.Protocol.config(requestMethod, requestUrl, [], requestObject, undefined, undefined, undefined, undefined, undefined);
        const messages: IProtocolMessages = { Error: { custom: errorMessage } };
        return this.Protocol.http(config, messages);
    }

    public getBannerLanguages(domainId: number): ng.IPromise<IBannerLangReponse> {
        return this.apiRequest("GET", `/consentNotice/BannerLanguages?domainId=${domainId}`, null, this.$rootScope.t("CouldNotGetLanguages"))
            .then((response: IProtocolResponse<IBannerLangReponse>): IBannerLangReponse => {
                const refreshNeeded = response.result ? response.data.refreshNeeded : false;
                let languages: IBannerLanguage[] = [];
                let failedLanguges: IBannerLanguage[] = [];
                if (response.result) {
                    languages = response.data.languages.filter((language: IBannerLanguage) => language.status === BannerLanguageStatus.COMPLETED && Boolean(language.domainId));
                    languages = sortBy(languages.map((item: IBannerLanguage): IBannerLanguage => { item.englishName = item.language.englishName; return item; }), "name");
                    failedLanguges = response.data.languages.filter((language: IBannerLanguage) => language.status === BannerLanguageStatus.FAILED);
                }
                this.cookieComplianceStore.setBannerLanguages(languages);
                return { languages, failedLanguges, refreshNeeded };
            });
    }

    public getLanguages(domainId: number): ng.IPromise<IBannerLanguageResponse[]> {
        return this.apiRequest("GET", `/consentNotice/Languages?domainId=${domainId}`, null, this.$rootScope.t("CouldNotGetLanguages")).then((response: IProtocolResponse<IBannerLanguageResponse[]>): IBannerLanguageResponse[] => {
            return response.result ? sortBy(response.data, "name") : null;
        });
    }
    public createDomainLanguage(domainId: number, languageId: number): ng.IPromise<IProtocolPacket> {
        return this.apiRequest("POST", `/consentNotice/Languages?domainId=${domainId}&&languageId=${languageId}`, null, this.$rootScope.t("ErrorAddingNewLanguage")).then((response: IProtocolPacket): ng.IPromise<IProtocolPacket> => {
            return response.result ? response.data : null;
        });
    }

    public addDomainLanguage(domainId: number, data: IBannerLanguageRequestData[]): ng.IPromise<boolean> {
        const config = new HttpRequest("POST", `/consentnotice/domains/${domainId}/languages/management`, { requests: data }, {responseType: "text"});
        const messages: IProtocolMessages | IProtocolMessage  = { Error: { custom: this.$rootScope.t("ErrorAddingNewLanguage") } };
        return this.Protocol.http(config, messages);
    }

}
