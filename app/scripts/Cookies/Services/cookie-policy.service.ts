// Interfaces
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import {
    IProtocolPacket,
    IProtocolConfig,
    IProtocolResponse,
    IProtocolMessages,
} from "interfaces/protocol.interface";
import { ICustomCookieModal } from "interfaces/cookies/custom-cookie-modal.interface";
import { ICookies } from "interfaces/cookies/cc-cookie-category.interface";
import {
    ICookieGroupDetail,
    ICookieGroupRequest,
} from "interfaces/cookies/cookie-group-detail.interface";
import {
    ICookieListConsentDetail,
    IConsentSettingDetail,
} from "interfaces/cookies/cookie-list-consent-detail.interface";
import { ICookieListConfigurationDetail } from "interfaces/cookies/cookie-list-configuration-detail.interface";

// Services
import { ProtocolService } from "modules/core/services/protocol.service";

export class CookiePolicyService {
    static $inject: string[] = [
        "OneProtocol",
        "$rootScope",
    ];
    private Protocol: ProtocolService;
    private translate = this.$rootScope.t;

    constructor(
        private readonly OneProtocol: ProtocolService,
        private readonly $rootScope: IExtendedRootScopeService) {
        this.Protocol = OneProtocol;
    }

    public apiRequest(requestMethod: any, requestUrl: string, requestObject: any, object: string, action: string): any {
        const config: IProtocolConfig = this.Protocol.config(requestMethod, requestUrl, [], requestObject);
        const messages: IProtocolMessages = { Error: { object, action } };
        return this.Protocol.http(config, messages);
    }

    public getAssignedGroupByDomainId(domainId: number): any {
        return this.apiRequest("GET", `/cookiepolicy/assignedcookies?domainId=${domainId}`, null, this.$rootScope.t("CookiePolicy"), this.$rootScope.t("getting")).then((response: any): any => {
            return response.result ? response.data : null;
        });
    }

    public getUnAssignedGroupByDomainId(domainId: number): any {
        return this.apiRequest("GET", `/cookiepolicy/unassignedcookies?domainId=${domainId}`, null, this.$rootScope.t("CookiePolicy"), this.$rootScope.t("getting")).then((response: any): any => {
            return response.result ? response.data : null;
        });
    }

    public updateCookiesForRescan(domainId: number): Promise<any> {
        return this.apiRequest("POST", `/cookiepolicy/UpdateCookiesForRescan?domainId=${domainId}`, null, this.$rootScope.t("CookiePolicy"), this.$rootScope.t("updating")).then((response: any): any => {
            return response.result ? response.data : null;
        });
    }

    public moveCookie(domainId: number, cookieId: number, groupId: number): any {
        const cookies: number[] = [];
        cookies.push(cookieId);
        return this.apiRequest("PUT", `/cookiepolicy/MoveCookies?domainid=${domainId}&&groupid=${groupId}`, cookies, this.$rootScope.t("CookiePolicy"), this.$rootScope.t("updating")).then((response: any): any => {
            return response.result ? response.data : null;
        });
    }

    public UpdateSubgroup(domainId: number, groupId: number, subgroupId: number): any {
        return this.apiRequest("PUT", `/cookiepolicy/EditSubGroup?domainId=${domainId}&&groupid=${groupId}&&subgroupId=${subgroupId}`, null, this.$rootScope.t("CookiePolicy"), this.$rootScope.t("updating")).then((response: any): any => {
            return response.result ? response.data : null;
        });
    }

    public getGroupCookies(domainId: number, groupId: number): any {
        return this.apiRequest("GET", `/cookiepolicy/GetGroupCookies?domainId=${domainId}&&groupid=${groupId}`, null, this.$rootScope.t("CookiePolicy"), this.$rootScope.t("getting")).then((response: any): any => {
            return response.result ? response.data : null;
        });
    }

    public autoAssignCookies(domainId: number): any {
        return this.apiRequest("PUT", `/cookiepolicy/AutoAssignCookies?domainId=${domainId}`, null, this.$rootScope.t("CookiePolicy"), this.$rootScope.t("updating")).then((response: any): any => {
            return response.result ? response.data : null;
        });
    }

    public moveHosts(domainId: number, hostName: string, currentGroupId: number, targetGroupId: number): any {
        const host: any = {
            currentGroupId,
            hostName,
        };

        return this.apiRequest("PUT", `/cookiepolicy/MoveHosts?domainid=${domainId}&&groupid=${targetGroupId}`, host, this.$rootScope.t("CookiePolicy"), this.$rootScope.t("updating")).then((response: any): any => {
            return response.result ? response.data : null;
        });
    }

    public createGroup = (payload: ICookieGroupRequest, parentGroupId, domainId: number): ng.IPromise<IProtocolResponse<ICookieGroupDetail>> => {
        const config: IProtocolConfig = this.OneProtocol.config("POST", "/cookiepolicy/CreateGroup", { domainId, parentGroupId }, payload);
        const messages: IProtocolMessages = { Error: { Hide: true } };
        return this.OneProtocol.http(config, messages);
    }

    public updateGroup = (payload: ICookieGroupRequest, groupId: number, domainId: number): ng.IPromise<IProtocolResponse<void>> => {
        const config: IProtocolConfig = this.OneProtocol.config("PUT", "/cookiepolicy/EditGroup", { domainId, groupId }, payload);
        const messages: IProtocolMessages = { Error: { Hide: true } };
        return this.OneProtocol.http(config, messages);
    }

    public deleteGroup(targetGroupId: number): Promise<any> {
        return this.apiRequest("DELETE", `/cookiepolicy/DeleteGroup?groupId=${targetGroupId}`, null, this.$rootScope.t("CookiePolicy"), this.$rootScope.t("Deleting"));
    }
    public getPublishedStatus = (domainId: number): any => {
        return this.apiRequest("GET", `/cookiepolicy/isUnpublished?domainid=${domainId}`, null, this.$rootScope.t("CookiePolicy"), this.$rootScope.t("getting")).then((response: any): any => {
            return response.result ? response : null;
        });
    }

    public getOldcookies = (domainId: number): any => {
        return this.apiRequest("GET", `/cookiepolicy/GetOldCookies?domainid=${domainId}`, null, this.$rootScope.t("CookiePolicy"), this.$rootScope.t("getting")).then((response: any): any => {
            return response.result ? response.data : null;
        });
    }
    public deleteOldcookies = (cookieIds): any => {
        return this.apiRequest("PUT", `/cookiepolicy/DeleteOldCookies`, cookieIds, this.$rootScope.t("CookiePolicy"), this.$rootScope.t("updating")).then((response: any): any => {
            return response.result ? response.data : null;
        });
    }
    public changeGroupOrder = (groupIds: number[]): void | undefined => {
        const requestObject: any = { groupIds };
        return this.apiRequest("PUT", `/cookiepolicy/GroupOrder`, requestObject, this.$rootScope.t("CookiePolicy"), this.$rootScope.t("updating")).then((response: any): any => {
            return response.result ? response.data : null;
        });
    }
    public getCookiePolicyText(domainId: number): ng.IPromise<IProtocolPacket> {
        return this.apiRequest("GET", `/consentNotice/GetCookiePolicyText?domainId=${domainId}`, null, this.$rootScope.t("ErrorGettingCookieLabels"), this.$rootScope.t("getting")).then((response: IProtocolPacket): ng.IPromise<IProtocolPacket> => {
            return response.result ? response.data : null;
        });
    }
    public updatePolicyLabels(domainId: number, policyLabels: any): ng.IPromise<IProtocolPacket> {
        return this.apiRequest("PUT", `/consentNotice/UpdateCookiePolicyText?domainId=${domainId}`, policyLabels, this.$rootScope.t("ErrorUpdatingCookieLabels"), this.$rootScope.t("updating")).then((response: IProtocolPacket): any => {
            return response.result ? response.data : null;
        });
    }

    getConsentSettings(domainId: number): ng.IPromise<IProtocolResponse<ICookieListConsentDetail>> {
        const config: IProtocolConfig = this.OneProtocol.config("GET", `/CookiePolicy/domains/${domainId}/cookieconsentset`);
        const messages: IProtocolMessages = { Error: { custom: this.translate("CouldNotGetConsentSettings") } };
        return this.OneProtocol.http(config, messages);
    }

    updateConsentSettings(domainId: number, consentSettings: ICookieListConsentDetail): ng.IPromise<IProtocolResponse<void>> {
        const config: IProtocolConfig = this.OneProtocol.config("POST", `/CookiePolicy/domains/${domainId}/cookieconsentset`, [], consentSettings);
        const messages: IProtocolMessages = { Error: { custom: this.translate("CouldNotUpdate") } };
        return this.OneProtocol.http(config, messages);
    }

    createCustomCookie(payload: ICustomCookieModal, domainId: number, groupId: number): ng.IPromise<IProtocolResponse<ICookies | string>> {
        const config: IProtocolConfig = this.OneProtocol.config("POST", `/ConsentNotice/domains/${domainId}/groups/${groupId}/cookies`, [], payload);
        const messages: IProtocolMessages = { Error: { Hide: true } };
        return this.OneProtocol.http(config, messages);
    }

    updateDntToggle(domainId: number, isDntEnabled: boolean): ng.IPromise<IProtocolResponse<{ groups: number[] }>> {
        const config: IProtocolConfig = this.OneProtocol.config("PUT", `/CookiePolicy/domains/${domainId}/dntsetting`, [], { isDntEnabled });
        const messages: IProtocolMessages = { Error: { custom: this.translate("CouldNotUpdate") } };
        return this.OneProtocol.http(config, messages);
    }

    updateConsentConfiguration(domainId: number, payload: IConsentSettingDetail): ng.IPromise<IProtocolResponse<{ groups: number[] }>> {
        const config: IProtocolConfig = this.OneProtocol.config("PUT", `/CookiePolicy/domains/${domainId}/consentsetting`, [], payload);
        const messages: IProtocolMessages = { Error: { custom: this.translate("CouldNotUpdate") } };
        return this.OneProtocol.http(config, messages);
    }

    updateCustomLogging(domainId: number, organizationId: string, IsConsentLoggingEnabled: boolean): ng.IPromise<IProtocolResponse<string>> {
        const config: IProtocolConfig = this.OneProtocol.config("POST", `/consentNotice/domains/${domainId}/logging`, null, { organizationId, IsConsentLoggingEnabled }, null, null, null, "text");
        const messages: IProtocolMessages = { Error: { custom: this.translate("ErrorEnableLogging") } };
        return this.OneProtocol.http(config, messages);
    }

    updateIabConfiguration(domainId: number, payload: { IsIABEnabled: boolean, SelectedList: number, IsIabThirdPartyCookieEnabled: boolean }): ng.IPromise<IProtocolResponse<void>> {
        const config: IProtocolConfig = this.OneProtocol.config("PUT", `/CookiePolicy/domains/${domainId}/iabconsentsetting`, [], payload);
        const messages: IProtocolMessages = { Error: { custom: this.translate("CouldNotUpdate") } };
        return this.OneProtocol.http(config, messages);
    }

    updateCookiePolicyLabels(domainId: number, payload: ICookieListConfigurationDetail): ng.IPromise<IProtocolResponse<void>> {
        const config: IProtocolConfig = this.OneProtocol.config("PUT", `/cookiePolicy/domains/${domainId}/cookiepolicysetting`, [], payload);
        const messages: IProtocolMessages = { Error: { custom: this.translate("CouldNotUpdate") } };
        return this.OneProtocol.http(config, messages);
    }

}
