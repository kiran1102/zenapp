import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import { IProtocolPacket, IProtocolConfig, IProtocolMessages, IProtocolResponse } from "interfaces/protocol.interface";
import {
    IPurposes,
    IVendors,
    IVersion,
    IVersionData,
    IAddVendorRequest,
    IAddVendorRequestHeader,
    IfilterMeta,
    IcustomFilter,
    IVendorsResponse,
    IVendorMobile,
} from "interfaces/cookies/cc-iab-purpose.interface";
import { ProtocolService } from "modules/core/services/protocol.service";
import { IPageableOf } from "interfaces/pagination.interface";

export class CookieIABIntegrationService {
    static $inject: string[] = [
        "OneProtocol",
        "$rootScope",
        "$timeout",
    ];

    constructor(
        private readonly OneProtocol: ProtocolService,
        private readonly $rootScope: IExtendedRootScopeService,
        private readonly $timeout: ng.ITimeoutService,
    ) { }

    public apiRequest(requestMethod: string, requestUrl: string, requestObject: any, errorMessage: string, headers: any = null): ng.IPromise<IProtocolPacket> {
        const config: IProtocolConfig = this.OneProtocol.config(requestMethod, requestUrl, [], requestObject, headers);
        const messages: IProtocolMessages = { Error: { custom: errorMessage } };
        return this.OneProtocol.http(config, messages);
    }

    public getPurposes(): ng.IPromise<IPurposes> {
        return this.apiRequest("GET", `/ConsentNotice/purposes`, null, this.$rootScope.t("CouldNotGetPurposes"))
            .then((response: IProtocolResponse<IPurposes>): IPurposes => {
                return response.data;
            });
    }

    public getVendorsForPurpose(purposeIds: string): ng.IPromise<IVendors> {
        return this.apiRequest("GET", `/ConsentNotice/vendors?purposeIds=${purposeIds}`, null, this.$rootScope.t("CouldNotGetVendors"))
            .then((response: IProtocolPacket): ng.IPromise<IVendors> => {
                return response.result ? response.data : null;
            });
    }

    public addVendorVersions(reqBody, headers): ng.IPromise<boolean> {
        return this.apiRequest("POST", `/vendor/member-vendorlist`, reqBody, this.$rootScope.t("CouldNotAddVersions"), headers).then((response: IProtocolResponse<boolean>): boolean => {
            return response.result ? response.data : false;
        });
    }

    public refreshVendors(vendorId): ng.IPromise<IVendorsResponse> {
        return this.apiRequest("GET", `/vendor/vendorlist/${vendorId}/vendors/refresh`, null, this.$rootScope.t("CouldNotRefreshVendors")).then((response: IProtocolResponse<IVendorsResponse>): IVendorsResponse => {
            return response.result ? response.data : null;
        });
    }

    public updateVendorVersions(listId: number, reqBody: IAddVendorRequest, headers: IAddVendorRequestHeader): ng.IPromise<boolean> {
        return this.apiRequest("PATCH", `/vendor/vendorlist/${listId}`, reqBody, this.$rootScope.t("CouldNotAddVersions"), headers).then((response: IProtocolResponse<boolean>): boolean => {
            return response.result ? response.data : null;
        });
    }

    public deleteVendorVersions(listId: number): ng.IPromise<boolean> {
        return this.apiRequest("DELETE", `/vendor/vendorlist/${listId}`, null, this.$rootScope.t("CouldNotAddVersions")).then((response: IProtocolResponse<boolean>): boolean => {
            return response.result ? response.data : null;
        });
    }

    public updateVendors(vendorIds: number[], versionId: number): ng.IPromise<boolean> {
        return this.apiRequest("PUT", `/vendor/vendorlist/${versionId}/status`, vendorIds, this.$rootScope.t("CouldNotUpdateVendors"))
            .then((response: IProtocolResponse<boolean>): boolean => {
                return response.result ? response.data : null;
            });
    }

    public updateWhitelistVendors(vendorIds: number[], versionId: number): ng.IPromise<boolean> {
        return this.apiRequest("PATCH", `/vendor/whitelist/${versionId}`, vendorIds, this.$rootScope.t("CouldNotUpdateWhitelistVendors"))
            .then((response: IProtocolResponse<boolean>): boolean => {
                return response.result ? response.data : false;
            });
    }

    public getVendorDetails(versionId: number = 17, activePage: number = 0, activePageSize: number = 20, inactivePage: number = 0, inactivePageSize: number = 20, activeName: string, inactiveName: string, filterData: IcustomFilter | IcustomFilter[]): ng.IPromise<IVendorsResponse> {
        const method = filterData || activeName || inactiveName ? "POST" : "GET";
        const methodName = filterData || activeName || inactiveName ? "filter" : "vendors";
        const customFilter = filterData ? { filters: filterData } : filterData;
        let URL = `/vendor/vendorlist/${versionId}/${methodName}?`;

        const options = {
            active_page: activePage,
            active_size: activePageSize,
            inactive_page: inactivePage,
            inactive_size: inactivePageSize,
        };

        if (activeName) {
            options["activeName"] = activeName;
        }
        if (inactiveName) {
            options["inactiveName"] = inactiveName;
        }
        const optionsList = [];
        for (const opt in options) {
            if (opt) {
                optionsList.push(`${opt}=${options[opt]}`);
            }
        }
        URL += optionsList.join("&");
        return this.apiRequest(method, URL, customFilter, this.$rootScope.t("CouldNotGetVendors"))
            .then((response: IProtocolResponse<IVendorsResponse>): IVendorsResponse => {
                return response.result ? response.data : null;
            });
    }

    public getListNames(page: number, size: number = 20): ng.IPromise<IVersionData> {
        return this.apiRequest("GET", `/vendor/vendorLists?page=${page}&size=${size}`, null, this.$rootScope.t("CouldNotGetVersions"))
            .then((response: IProtocolResponse<IVersionData>): IVersionData => {
                return response.result ? response.data : null;
            });
    }

    public getPurposesAndFeatures(): ng.IPromise<IfilterMeta[]> {
        return this.apiRequest("GET", `/vendor/filterMetadata`, null, this.$rootScope.t("CouldNotGetPurposesAndFeatures"))
            .then((response: IProtocolResponse<IfilterMeta[]>): IfilterMeta[] => {
                return response.result ? response.data : null;
            });
    }

    public movePurposes(domainId: number, groupId: number, purposes: number[]): ng.IPromise<boolean> {
        return this.apiRequest("PUT", `/ConsentNotice/domains/${domainId}/groups/${groupId}/purposes`, purposes, this.$rootScope.t("ErrorMovingPurposes"))
            .then((response: IProtocolResponse<boolean>): boolean => {
                return response.data;
            });
    }

    public deletePurposes(domainId: number, groupId: number, purposes: number): ng.IPromise<boolean> {
        return this.apiRequest("DELETE", `/ConsentNotice/domains/${domainId}/groups/${groupId}/purposes/${purposes}`, null, this.$rootScope.t("ErrorDeletingPurposes"))
            .then((response: IProtocolResponse<boolean>): boolean => {
                return response.data;
            });
    }

    public moveVendors(domainId: number, groupId: number, vendors: number[]): ng.IPromise<boolean> {
        return this.apiRequest("PUT", `/ConsentNotice/domains/${domainId}/groups/${groupId}/vendors`, vendors, this.$rootScope.t("ErrorMovingVendors"))
            .then((response: IProtocolResponse<boolean>): boolean => {
                return response.data;
            });
    }

    public getVendorsForMobile(page: number = 0, size: number = 2000): ng.IPromise<IPageableOf<IVendorMobile>> {
        return this.apiRequest("GET", `/vendor/vendorLists?page=${page}&size=${size}`, null, this.$rootScope.t("CouldNotGetVendors"))
            .then((response: IProtocolResponse<IPageableOf<IVendorMobile>>): IPageableOf<IVendorMobile> => {
                return response.data;
            });
    }

}
