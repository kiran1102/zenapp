export enum ConsentModels {
    InformationOnly = 1,
    ImpliedConsent = 2,
    ExplicitConsent = 3,
    OwnerDefined = 4,
    SoftOptIn = 5,
}
export enum DefaultStatuses {
    Active = 1,
    Inactive = 2,
    AlwaysActive = 3,
    DoNotTrack = 4,
    InactiveLandingPage = 5,
}
export enum PurposeGroupId {
    StrictlyNecessaryCookies = 1,
}
