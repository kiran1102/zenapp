import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import { IProtocolPacket, IProtocolResponse, IProtocolConfig, IProtocolMessages } from "interfaces/protocol.interface";
import { ProtocolService } from "modules/core/services/protocol.service";
import { IScanLoginDetails } from "interfaces/cookies/cc-scan-login-details.interface";

export class CookieScanLoginService {
    static $inject: string[] = ["OneProtocol", "$rootScope"];

    constructor(
        private readonly OneProtocol: ProtocolService,
        private readonly $rootScope: IExtendedRootScopeService,
    ) { }

    public apiRequest(requestMethod: string, requestUrl: string, requestParams: any, requestObject: any, errorMessage: string): ng.IPromise<IProtocolPacket> {
        const config: IProtocolConfig = this.OneProtocol.config(requestMethod, requestUrl, requestParams, requestObject);
        const messages: IProtocolMessages = { Error: { custom: errorMessage } };
        return this.OneProtocol.http(config, messages);
    }

    public getScanLoginDetails(domain: string): ng.IPromise<IScanLoginDetails> {
        return this.apiRequest("GET", "/audit/GetLoginInfo", { domain }, null, this.$rootScope.t("ErrorGettingScanLoginDetails")).then((response: IProtocolPacket): ng.IPromise<IScanLoginDetails> => {
            return response.result ? response.data : null;
        });
    }

    public updateScanLoginDetails(loginDetails: IScanLoginDetails): ng.IPromise<IScanLoginDetails | null> {
        return this.apiRequest("PUT", "/audit/CreateOrUpdateLoginInfo", null, loginDetails, this.$rootScope.t("ErrorUpdatingScanLoginDetails")).then((response: IProtocolPacket): ng.IPromise<IScanLoginDetails | null> => {
            return response.result ? response.data : null;
        });
    }
    public deleteScanLoginInfo(loginDetails: IScanLoginDetails, domain: string): ng.IPromise<IScanLoginDetails | null> {
        return this.apiRequest("DELETE", "/audit/DeleteLoginInfo", { domain }, loginDetails, this.$rootScope.t("ErrorDeletingScanLoginDetails")).then((response: IProtocolPacket): ng.IPromise<IScanLoginDetails | null> => {
            return response.result ? response.data : null;
        });
    }

}
