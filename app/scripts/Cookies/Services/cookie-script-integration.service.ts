// Services
import { ProtocolService } from "modules/core/services/protocol.service";

// Enums
import { ExportTypes } from "enums/export-types.enum";

// Interfaces
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import {
    IProtocolPacket,
    IProtocolConfig,
    IProtocolMessages,
    IProtocolResponse,
} from "interfaces/protocol.interface";
import { IPreviewScript } from "interfaces/cookies/cc-preview-script.interface";
import { IDomainScript } from "interfaces/cookies/cc-script-integration.interface";

export class CookieScriptIntegrationService {
    static $inject: string[] = [
        "OneProtocol",
        "$rootScope",
        "Export",
    ];

    constructor(
        private readonly OneProtocol: ProtocolService,
        private readonly $rootScope: IExtendedRootScopeService,
        readonly Export: any,
    ) { }

    public apiRequest(requestMethod: any, requestUrl: string, requestObject: any, errorMessage: string): ng.IPromise<IProtocolPacket> {
        const config: IProtocolConfig = this.OneProtocol.config(requestMethod, requestUrl, [], requestObject, undefined, undefined, undefined, undefined, undefined);
        const messages: IProtocolMessages = { Error: { custom: errorMessage } };
        return this.OneProtocol.http(config, messages);
    }

    public getScriptIntegration(domainId: number): ng.IPromise<IDomainScript> {
        return this.apiRequest("GET", `/consentNotice/GetScriptIntegration?domainId=${domainId}`, null, this.$rootScope.t("ErrorGettingScriptTag")).then((response: IProtocolPacket): ng.IPromise<IDomainScript> => {
            return response.result ? response.data : null;
        });
    }

    public updateScriptIntegrationToggleAction(domainId: number, noJquery: boolean): ng.IPromise<IProtocolPacket> {
        return this.apiRequest("PUT", `/consentNotice/UpdateScriptIntegrationToggleAction?domainId=${domainId}&&noJquery=${noJquery}`, null, this.$rootScope.t("ErrorUpdatingScriptTag")).then((response: IProtocolPacket): ng.IPromise<IProtocolPacket> => {
            return response.result ? response.data : null;
        });
    }

    public publishTestScript(domainId: number): ng.IPromise<IProtocolPacket> {
        return this.apiRequest("POST", `/consentNotice/PublishTestScript?domainId=${domainId}`, null, this.$rootScope.t("ErrorPublishingScriptTag"));
    }

    public downloadCookieScript = (domainId: number): ng.IPromise<IProtocolPacket> => {
        return this.Export.downloadFile(`/consentNotice/DownloadCookieScripts?domainId=${domainId}`, ExportTypes.ZIP, "OneTrustCookiesScript");
    }

    public getPreviewScriptHistory = (archiveScriptId: number, domainCctId: string): ng.IPromise<IPreviewScript | null> => {
        return this.apiRequest("GET", `/consentNotice/GetScriptHistoryContent?archiveScriptId=${archiveScriptId}&domainCctId=${domainCctId}`, null, this.$rootScope.t("ErrorGettingPreviewScript"))
            .then((response: IProtocolPacket): ng.IPromise<IPreviewScript | null> => {
                return response.result ? response.data : null;
            });
    }

    saveCustomJs(payload: string, domainId: number): ng.IPromise<IProtocolResponse<void>> {
        const config: IProtocolConfig = this.OneProtocol.config("PUT", `/scriptintegration/domains/${domainId}/customjs`, [], { customJsContent: payload });
        const messages: IProtocolMessages = { Error: { custom: this.$rootScope.t("CouldNotSaveCustomJavascript") } };
        return this.OneProtocol.http(config, messages);
    }

    fetchCustomJs(domainId: number): ng.IPromise<IProtocolResponse<string>> {
        const config: IProtocolConfig = this.OneProtocol.config("GET", `/scriptintegration/domains/${domainId}/customjs`);
        const messages: IProtocolMessages = { Error: { custom: this.$rootScope.t("CouldNotGetCustomJavascript") } };
        return this.OneProtocol.http(config, messages);
    }

}
