declare var angular: any;
declare var _: any;
import { IPromise } from "angular";

import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import {
    IProtocolPacket,
    IProtocolConfig,
    IProtocolMessages,
    IProtocolResponse,
} from "interfaces/protocol.interface";
import { ProtocolService } from "modules/core/services/protocol.service";
import { IPrefCenterData } from "interfaces/cookies/cc-pref-center.interface";

export class CookiePreferenceCenterService {
    static $inject: string[] = ["OneProtocol", "$rootScope", "Content"];
    private Protocol: any;

    constructor(private readonly OneProtocol: ProtocolService, private readonly $rootScope: IExtendedRootScopeService, readonly Content: any) {
        this.Protocol = OneProtocol;
    }

    public apiRequest(requestMethod: any, requestUrl: string, requestObject: any, object: string, action: string, timeout?: number): ng.IPromise<IProtocolPacket> {
        const config: IProtocolConfig = this.Protocol.config(requestMethod, requestUrl, [], requestObject, undefined, undefined, undefined, undefined, timeout);
        const messages: IProtocolMessages = { Error: { object, action } };
        return this.Protocol.http(config, messages);
    }

    public getPreferenceCenter(domainId: number): ng.IPromise<IPrefCenterData> {
        return this.apiRequest("GET", `/consentNotice/PreferenceCenter?domainId=${domainId}`, null, this.$rootScope.t("PreferenceCenter"), this.$rootScope.t("getting")).then((response: IProtocolPacket): ng.IPromise<IPrefCenterData> => {
            return response.result ? response.data : null;
        });
    }

    public updatePreferenceCenter(domainId: number, preferenceCenterData: any): ng.IPromise<IProtocolPacket> {
        return this.apiRequest("POST", `/consentNotice/PreferenceCenter?domainId=${domainId}`, preferenceCenterData, this.$rootScope.t("PreferenceCenter"), this.$rootScope.t("updating")).then((response: IProtocolPacket): ng.IPromise<IProtocolPacket> => {
            return response.result ? response.data : null;
        });
    }

    public enableLogging(domainId: number, organizationId: string): ng.IPromise<string> {
        return this.apiRequest("POST", `/consentNotice/domains/${domainId}/logging`, { organizationId }, this.$rootScope.t("EnableLogging"), this.$rootScope.t("updating")).then((response: IProtocolResponse<string>): string => {
            return response.result ? response.data : null;
        });
    }

    getCollectionPoint(domainId: number): IPromise<IProtocolResponse<string>> {
        const config: IProtocolConfig = this.OneProtocol.config("GET", `/consentNotice/domains/${domainId}/consenttrans`, null, null, null, null, null, "text");
        const messages: IProtocolMessages = { Error: { custom: this.$rootScope.t("FetchingCollectionPoint") } };
        return this.OneProtocol.http(config, messages);
    }

    public getPreferencePreviewCssUrl(styleType: string): string {
        let previewCssUrl = "scripts/optanon-modern.css";
        if (styleType.toUpperCase() === "CLASSIC") {
            previewCssUrl = "scripts/optanon-classic.css";
        }
        return previewCssUrl;
    }

    public getPreferencePreviewLogo(styleType: string, logoFileData: string): string {
        if (logoFileData) {
            return `'${logoFileData}'`;
        } else if (styleType.toUpperCase() === "CLASSIC") {
            return "images/cookie-collective-top-logo.png";
        } else {
            return "";
        }
    }
}
