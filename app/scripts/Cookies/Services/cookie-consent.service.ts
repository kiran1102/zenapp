declare var angular: any;

// 3rd party
import { includes } from "lodash";

// Interfaces
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import {
    IProtocolPacket,
    IProtocolConfig,
    IProtocolMessages,
    IProtocolResponse,
} from "interfaces/protocol.interface";
import { IBannerModel } from "interfaces/cookies/cc-banner-template.interface";
import {
    ITemplateJson,
    ITemplateBannerPrefCenter,
    ICookieTemplateDetail,
    ICookieTemplate,
} from "interfaces/cookies/cc-template-json.interface";
import { IColorOptions } from "interfaces/color-picker.interface.ts";
import { IBannerLayoutProperties } from "interfaces/cookies/cc-banner-template.interface";
import { IMinicolorOptions } from "interfaces/email-template.interface";

// Services
import { ProtocolService } from "modules/core/services/protocol.service";
import { CookiePreferenceCenterService } from "app/scripts/Cookies/Services/cookie-preference-center.service";

export class CookieConsentService {
    static $inject: string[] = [
        "OneProtocol",
        "$rootScope",
    ];
    private Protocol: any;

    constructor(
        private readonly OneProtocol: ProtocolService,
        private readonly $rootScope: IExtendedRootScopeService,
        readonly cookiePreferenceCenterService: CookiePreferenceCenterService,
    ) {
        this.Protocol = OneProtocol;
    }

    public getLayoutOptions(): IBannerLayoutProperties[] {
        return [
            {
                imageSrc: "images/consent/bottom-dark.svg",
                label: this.$rootScope.t("BottomBarDarkTheme"),
                position: "bottom",
                selected: true,
                theme: "dark",
                bannerColor: "#000000",
                textColor: "#ffffff",
                top: "inherit",
                bottom: "0",
                left: "0",
                right: "0",
                width: "",
                height: "",
                transform: "scale(0.75)",
                transformOrigin: "bottom left",
            },
            {
                imageSrc: "images/consent/bottom-light.svg",
                label: this.$rootScope.t("BottomBarLightTheme"),
                position: "bottom",
                selected: false,
                theme: "light",
                bannerColor: "#ffffff",
                textColor: "#000000",
                top: "inherit",
                bottom: "0",
                left: "0",
                right: "0",
                width: "",
                height: "",
                transform: "scale(0.75)",
                transformOrigin: "bottom left",
            },
            {
                imageSrc: "images/consent/top-dark.svg",
                label: this.$rootScope.t("TopBarDarkTheme"),
                position: "top",
                selected: false,
                theme: "dark",
                bannerColor: "#000000",
                textColor: "#ffffff",
                top: "0",
                bottom: "inherit",
                left: "0",
                right: "0",
                width: "",
                height: "",
                transform: "scale(0.75)",
                transformOrigin: "top left",
            },
            {
                imageSrc: "images/consent/top-light.svg",
                label: this.$rootScope.t("TopBarLightTheme"),
                position: "top",
                selected: false,
                theme: "light",
                bannerColor: "#ffffff",
                textColor: "#000000",
                top: "0",
                bottom: "inherit",
                left: "0",
                right: "0",
                width: "",
                height: "",
                transform: "scale(0.75)",
                transformOrigin: "top left",
            },
            {
                imageSrc: "images/consent/cookie-banner-center.svg",
                label: this.$rootScope.t("CenterTileLightTheme"),
                position: "center",
                selected: false,
                theme: "light",
                bannerColor: "#ffffff",
                textColor: "#000000",
                top: "",
                bottom: "",
                left: "",
                right: "",
                width: "",
                height: "",
                transform: "",
                transformOrigin: "",
            },
        ];
    }

    public getDomainModel(id: any): ng.IPromise<IBannerModel> {
        const config: IProtocolConfig = this.OneProtocol.config("GET", `/consentNotice/GetCookieBanner?domainId=${id}`);
        const messages: IProtocolMessages = { Error: { custom: this.$rootScope.t("CookieBanner") } };
        return this.OneProtocol.http(config, messages).then(
            (response: IProtocolPacket): ng.IPromise<IBannerModel> => {
                const editCookieBannerRequest: IBannerModel = response.data;
                return this.mapBannerModel(editCookieBannerRequest);
            },
        );
    }

    public getDomainTemplateModel(id: number, languageId: number): ng.IPromise<ITemplateJson> {
        const config: IProtocolConfig = this.OneProtocol.config("GET", `/cookietemplate/templates/${id}/languages/${languageId}/templateJson`);
        const messages: IProtocolMessages = { Error: { custom: this.$rootScope.t("TemplateDetails") } };
        return this.OneProtocol.http(config, messages)
            .then((response: IProtocolResponse<ITemplateBannerPrefCenter>): ITemplateJson => {
                const templateJsonData: ITemplateBannerPrefCenter = response.data;
                return {
                    bannerModel: this.mapBannerModel(templateJsonData.cookieBanner ? templateJsonData.cookieBanner : templateJsonData.bannerModel),
                    prefCenterData: templateJsonData.allTabs ? templateJsonData.allTabs : templateJsonData.prefCenterData,
                };
            });
    }

    public getTemplateById(templateId: number): ng.IPromise<ICookieTemplate | null> {
        const config: IProtocolConfig = this.OneProtocol.config("GET", `/cookietemplate/templates/${templateId}`);
        const messages: IProtocolMessages = { Error: { custom: this.$rootScope.t("TemplateDetails") } };
        return this.OneProtocol.http(config, messages)
            .then((response: IProtocolResponse<ICookieTemplateDetail>): ICookieTemplate | null => {
                if (!response.result) {
                    return null;
                }
                const { templateJson, ...data } = response.data;
                const templateJsonData = JSON.parse(templateJson);
                return {
                    ...data,
                    bannerModel: this.mapBannerModel(templateJsonData.cookieBanner ? templateJsonData.cookieBanner : templateJsonData.bannerModel),
                    prefCenterData: templateJsonData.allTabs ? templateJsonData.allTabs : templateJsonData.prefCenterData,
                };
            });
    }
    public mapBannerModel(editCookieBannerRequest: any): any {
        const bannerModel: IBannerModel = this.getDefaultModel();
        bannerModel.domainId = editCookieBannerRequest.DomainId;
        bannerModel.showAddPolicyLink = editCookieBannerRequest.AddCookiePolicyLink;
        bannerModel.cookiePolicyLinkText = editCookieBannerRequest.CookiePolicyLink;
        bannerModel.cookiePolicyUrl = editCookieBannerRequest.CookiePolicyURL;
        bannerModel.noticeText = editCookieBannerRequest.NoticeDescription;
        bannerModel.showCookieSettings = editCookieBannerRequest.ShowCookieSettingsButton;
        bannerModel.cookieSettings = editCookieBannerRequest.CookieSettingsButtonText;
        bannerModel.showAcceptCookies = editCookieBannerRequest.ShowAcceptCookiesButton;
        bannerModel.acceptCookies = editCookieBannerRequest.AcceptCookiesButtonText;
        bannerModel.hideForAll = editCookieBannerRequest.HideForAll;
        bannerModel.closeAcceptsAllCookies = editCookieBannerRequest.CloseAccepstAllCookies;
        bannerModel.onClickAcceptAllCookies = editCookieBannerRequest.OnClickAcceptAllCookies;
        bannerModel.scrollAcceptsCookies = editCookieBannerRequest.ScrollingAcceptsAllCookies;
        bannerModel.scrollAcceptsAllCookies = editCookieBannerRequest.ScrollAcceptsAllCookiesAndClosesBanner;
        bannerModel.showOnlyInEU = editCookieBannerRequest.ShowOnlyInEU;
        bannerModel.skinName = editCookieBannerRequest.Layout;
        bannerModel.isPublished = editCookieBannerRequest.IsPublished;
        bannerModel.containsUnpublishedChanges = editCookieBannerRequest.ContainsUnpublishedChanges;
        bannerModel.bannerTitle = editCookieBannerRequest.BannerTitle;
        bannerModel.forceConsent = editCookieBannerRequest.ForceConsent;
        bannerModel.bannerPushesDown = editCookieBannerRequest.BannerPushesDown;
        bannerModel.showBannerCloseButton = editCookieBannerRequest.ShowBannerCloseButton;
        bannerModel.footerDescriptionText = editCookieBannerRequest.FooterDescriptionText;
        bannerModel.externalCssBaseUrl = editCookieBannerRequest.ExternalCssBaseUrl;
        bannerModel.bannerCloseButtonText = editCookieBannerRequest.BannerCloseButtonText;
        if (editCookieBannerRequest.ButtonColor) {
            bannerModel.buttonStyle.backgroundColor = editCookieBannerRequest.ButtonColor;
        }
        if (editCookieBannerRequest.BackgroundColor) {
            bannerModel.bannerStyle.backgroundColor = editCookieBannerRequest.BackgroundColor;
        }
        if (editCookieBannerRequest.TextColor) {
            bannerModel.textStyle.color = editCookieBannerRequest.TextColor;
        }
        return bannerModel;
    }

    public getEditCookieBannerRequestByBannerModel(bannerModel: any): any {
        return {
            DomainId: bannerModel.domainId,
            AddCookiePolicyLink: bannerModel.showAddPolicyLink,
            CookiePolicyLink: bannerModel.cookiePolicyLinkText,
            CookiePolicyURL: bannerModel.cookiePolicyUrl,
            NoticeDescription: bannerModel.noticeText,
            ShowCookieSettingsButton: bannerModel.showCookieSettings,
            CookieSettingsButtonText: bannerModel.cookieSettings,
            ShowAcceptCookiesButton: bannerModel.showAcceptCookies,
            AcceptCookiesButtonText: bannerModel.acceptCookies,
            HideForAll: bannerModel.hideForAll,
            CloseAccepstAllCookies: bannerModel.closeAcceptsAllCookies,
            OnClickAcceptAllCookies: bannerModel.onClickAcceptAllCookies,
            ScrollingAcceptsAllCookies: bannerModel.scrollAcceptsCookies,
            ScrollAcceptsAllCookiesAndClosesBanner: bannerModel.scrollAcceptsAllCookies,
            ShowOnlyInEU: bannerModel.showOnlyInEU,
            Layout: bannerModel.skinName,
            IsPublished: bannerModel.isPublished,
            ContainsUnpublishedChanges: bannerModel.containsUnpublishedChanges,
            ButtonColor: bannerModel.buttonStyle.backgroundColor,
            BackgroundColor: bannerModel.bannerStyle.backgroundColor,
            TextColor: bannerModel.textStyle.color,
            BannerTitle: bannerModel.bannerTitle,
            ForceConsent: bannerModel.forceConsent,
            BannerPushesDown: bannerModel.bannerPushesDown,
            ShowBannerCloseButton: bannerModel.showBannerCloseButton,
            FooterDescriptionText: bannerModel.footerDescriptionText,
            BannerCloseButtonText: bannerModel.bannerCloseButtonText,
        };
    }

    public getDefaultBannerModel(languageId: number): ng.IPromise<IBannerModel> {
        const config: IProtocolConfig = this.OneProtocol.config("GET", `/consentNotice/GetDefaultBanner?languageId=${languageId}`);
        const messages: IProtocolMessages = { Error: { object: this.$rootScope.t("CookieBanner") } };
        return this.OneProtocol.http(config, messages).then((response: IProtocolPacket): ng.IPromise<IBannerModel> => {
            return response.result ? this.mapBannerModel(response.data) : null;
        });
    }

    public changeDefaultGroups(domainId: number): ng.IPromise<IProtocolResponse<void>> {
        const config: IProtocolConfig = this.OneProtocol.config("POST", `/consentNotice/domains/${domainId}/defaultGroups`);
        const messages: IProtocolMessages = { Error: { object: this.$rootScope.t("CookieDefaultGroups") } };
        return this.OneProtocol.http(config, messages);
    }

    // TODO BKEL 11/30/18 - Make this object a class, then add in translations in this function (using translatePipe)

    public getDefaultModel(): any {
        return {
            domainId: 0,
            noticeText: this.$rootScope.t("DefaultCookieNotice").toString(),
            showAddPolicyLink: false,
            cookiePolicyLinkText: "",
            footerDescriptionText: "",
            cookiePolicyUrl: "",
            showCookieSettings: false,
            cookieSettings: this.$rootScope.t("CookieSettings").toString(),
            showAcceptCookies: true,
            acceptCookies: this.$rootScope.t("AcceptCookies").toString(),
            bannerStyle: {
                backgroundColor: "",
                transform: "scale(0.75)",
                transformOrigin: "bottom left",
            },
            textStyle: {
                color: "",
            },
            buttonStyle: {
                backgroundColor: "",
                border: "none",
            },
            closeAcceptsAllCookies: false,
            scrollAcceptsCookies: false,
            scrollAcceptsAllCookies: false,
            showOnlyInEU: false,
            hideForAll: false,
            skinName: "default_flat_bottom_one_button_black",
            isPublished: false,
            containsUnpublishedChanges: false,
            forceConsent: false,
            bannerPushesDown: false,
            showBannerCloseButton: true,
            onClickAcceptAllCookies: false,
        };
    }

    public getSkinPosition = (skinName: string, canShowCenterTile): string => {
        if (includes(skinName, "top")) {
            return "top";
        } else if (includes(skinName, "center") && canShowCenterTile) {
            return "center";
        } else {
            return "bottom";
        }
    }

    public getMinicolorSettings(): IMinicolorOptions {
        return {
            position: "bottom right",
            defaultValue: "",
            animationSpeed: 50,
            animationEasing: "swing",
            change: null,
            changeDelay: 0,
            control: "hue",
            hide: null,
            hideSpeed: 100,
            inline: false,
            letterCase: "uppercase",
            opacity: false,
            show: null,
            showSpeed: 100,
        };
    }

    public getColorCode(): IColorOptions[] {
        return [
            {
                rgb: "rgb(227, 171, 236)",
                code: "#e3abec",
            },
            {
                rgb: "rgb(194, 219, 247)",
                code: "#c2dbf7",
            },
            {
                rgb: "rgb(159, 214, 255)",
                code: "#9fd6ff",
            },
            {
                rgb: "rgb(157, 231, 218)",
                code: "#9de7da",
            },
            {
                rgb: "rgb(157, 240, 192)",
                code: "#9df0c0",
            },
            {
                rgb: "rgb(255, 240, 153)",
                code: "#fff099",
            },
            {
                rgb: "rgb(254, 212, 154)",
                code: "#fed49a",
            },
            {
                rgb: "rgb(208, 115, 224)",
                code: "#d073e0",
            },
            {
                rgb: "rgb(134, 186, 243)",
                code: "#86baf3",
            },
            {
                rgb: "rgb(94, 187, 255)",
                code: "#5ebbff",
            },
            {
                rgb: "rgb(68, 216, 190)",
                code: "#44d8be",
            },
            {
                rgb: "rgb(59, 226, 130)",
                code: "#3be282",
            },
            {
                rgb: "rgb(255, 230, 84)",
                code: "#ffe654",
            },
            {
                rgb: "rgb(255, 183, 88)",
                code: "#ffb758",
            },
            {
                rgb: "rgb(189, 53, 189)",
                code: "#bd35bd",
            },
            {
                rgb: "rgb(87, 121, 193)",
                code: "#5779c1",
            },
            {
                rgb: "rgb(94, 187, 255)",
                code: "#5ebbff",
            },
            {
                rgb: "rgb(0, 174, 169)",
                code: "#00aea9",
            },
            {
                rgb: "rgb(60, 186, 76)",
                code: "#3cba4c",
            },
            {
                rgb: "rgb(245, 188, 37)",
                code: "#f5bc25",
            },
            {
                rgb: "rgb(249, 146, 33)",
                code: "#f99221",
            },
            {
                rgb: "rgb(88, 13, 140)",
                code: "#580d8c",
            },
            {
                rgb: "rgb(0, 25, 112)",
                code: "#001970",
            },
            {
                rgb: "rgb(10, 35, 153)",
                code: "#0a2399",
            },
            {
                rgb: "rgb(11, 116, 119)",
                code: "#0b7477",
            },
            {
                rgb: "rgb(11, 107, 80)",
                code: "#0b6b50",
            },
            {
                rgb: "rgb(182, 126, 17)",
                code: "#b67e11",
            },
            {
                rgb: "rgb(184, 93, 13)",
                code: "#b85d0d",
            },
        ];
    }

    public getContrastTextColor(hex: string): string {
        const color: any = this.hexToRgb(hex);
        const textColor: string = color ? (((color.r * 0.299 + color.g * 0.587 + color.b * 0.114) > 186) ? "#000000" : "#ffffff") : "";
        return textColor;
    }

    private hexToRgb(hex: string): any {
        const result: RegExpExecArray = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
        return result ? {
            r: parseInt(result[1], 16),
            g: parseInt(result[2], 16),
            b: parseInt(result[3], 16),
        } : null;
    }
}
