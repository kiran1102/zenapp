import { sortBy } from "lodash";
import { INumberMap } from "interfaces/generic.interface";
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import {
    IProtocolPacket,
    IProtocolConfig,
    IProtocolMessages,
    IProtocolResponse,
} from "interfaces/protocol.interface";
import { IBannerTemplate, IBannerTemplateCreate } from "interfaces/cookies/cc-banner-template.interface";
import { ILanguage } from "interfaces/cookies/cc-banner-languages.interface";
import { ITemplateJson, ITemplateBannerPrefCenter } from "interfaces/cookies/cc-template-json.interface";
import { IPreviewScript } from "interfaces/cookies/cc-preview-script.interface";

import { CookieConsentService } from "cookiesService/cookie-consent.service";
import { ProtocolService } from "modules/core/services/protocol.service";

export class CookieTemplateService {
    static $inject: string[] = [
        "OneProtocol",
        "$rootScope",
        "CookieConsentService",
    ];

    constructor(
        private OneProtocol: ProtocolService,
        private $rootScope: IExtendedRootScopeService,
        private cookieConsentService: CookieConsentService) {}

    public apiRequest(requestMethod: string, requestUrl: string, requestObject: any, errorMessage: string): ng.IPromise<IProtocolPacket> {
        const config: IProtocolConfig = this.OneProtocol.config(requestMethod, requestUrl, [], requestObject);
        const messages: IProtocolMessages = { Error: { custom: errorMessage } };
        return this.OneProtocol.http(config, messages);
    }

    public getAllLanguagesForTemplate(): ng.IPromise<ILanguage[]> {
        return this.apiRequest("GET", `/cookietemplate/templates/languages`, null, this.$rootScope.t("CouldNotGetLanguages"))
            .then((response: IProtocolResponse<ILanguage[]>): ILanguage[] => {
                return response.data;
            });
    }

    public getAllMappedLanguagesForTemplate(templateId: number): ng.IPromise<ILanguage[]> {
        return this.apiRequest("GET", `/cookietemplate/templates/${templateId}/languages/mapped`, null, this.$rootScope.t("CouldNotGetLanguages"))
            .then((response: IProtocolResponse<ILanguage[]>): ILanguage[] => {
                return sortBy(response.data, "name");
            });
    }

    public getNotMappedLanguagesByTemplateId(templateId: number): ng.IPromise<ILanguage[]> {
        return this.apiRequest("GET", `/cookietemplate/templates/${templateId}/languages`, null, this.$rootScope.t("CouldNotGetLanguages"))
            .then((response: IProtocolResponse<ILanguage[]>): ILanguage[] => {
                return sortBy(response.data, "name");
            });
    }

    public addTemplate(templateData: IBannerTemplateCreate): ng.IPromise<IBannerTemplate> {
        return this.apiRequest("POST", `/cookietemplate/templates`, templateData, this.$rootScope.t("ErrorAddingTemplate"))
            .then((response: IProtocolResponse<IBannerTemplate>): IBannerTemplate => {
                return response.data;
            });
    }

    public getAllTemplates(): ng.IPromise<IBannerTemplate[]> {
        return this.apiRequest("GET", `/cookietemplate/templates`, null, this.$rootScope.t("ErrorGettingTemplates"))
            .then((response: IProtocolResponse<IBannerTemplate[]>): IBannerTemplate[] => {
                return response.data;
            });
    }

    public assignDomainToTemplate(templateId: number, domainId: number): ng.IPromise<boolean> {
        return this.apiRequest("POST", `/cookietemplate/templates/${templateId}/domains/${domainId}`, null, this.$rootScope.t("ErrorAssigningTemplate"))
            .then((response: IProtocolResponse<boolean>): boolean => {
                return response.data;
            });
    }

    public publishTemplate(templateId: number, publishData: INumberMap<boolean>): ng.IPromise<boolean> {
        return this.apiRequest("POST", `/cookietemplate/templates/${templateId}/domains/script`, publishData, this.$rootScope.t("ErrorPublishingTemplate"))
            .then((response: IProtocolResponse<boolean>): boolean => {
                return response.data;
            });
    }

    public deleteTemplate(templateId: number): ng.IPromise<boolean> {
        return this.apiRequest("DELETE", `/cookietemplate/templates/${templateId}`, null, this.$rootScope.t("ErrorDeletingTemplate"))
            .then((response: IProtocolResponse<boolean>): boolean => {
                return response.data;
            });
    }

    public updateTemplate(templateData: IBannerTemplate): ng.IPromise<boolean> {
        return this.apiRequest("PUT", `/cookietemplate/templates/`, {templateData}, this.$rootScope.t("ErrorUpdatingTemplate"))
            .then((response: IProtocolResponse<boolean>): boolean => {
                return response.data;
            });
    }

    public addNewLanguageTemplate(templateId: number, languageId: number): ng.IPromise<boolean> {
        return this.apiRequest("POST", `/cookietemplate/templates/${templateId}/languages/${languageId}`, null, this.$rootScope.t("ErrorAddingNewLanguageToTemplate")).then(
            (response: IProtocolResponse<boolean>): boolean => {
                return response.data;
            });
    }

    public updateBannerPreferenceCenterDetails(templateId: number, languageId: number, templateJson: ITemplateJson): ng.IPromise<ITemplateJson> {
        const templateJsonData: ITemplateBannerPrefCenter = {
            allTabs: templateJson.prefCenterData,
            cookieBanner: this.cookieConsentService.getEditCookieBannerRequestByBannerModel(templateJson.bannerModel),
        };
        return this.apiRequest("PUT", `/cookietemplate/templates/${templateId}/languages/${languageId}/templateJson`, templateJsonData, this.$rootScope.t("ErrorUpdatingTemplate"))
            .then((response: IProtocolResponse<ITemplateJson>): ITemplateJson => {
                return response.data;
            });
    }

    public editTemplate(templateId: number, templateData: IBannerTemplateCreate): ng.IPromise<boolean> {
        return this.apiRequest("PUT", `/cookietemplate/templates/${templateId}`, templateData, this.$rootScope.t("ErrorUpdatingTemplate"))
            .then((response: IProtocolResponse<boolean>): boolean => {
                return response.data;
            });
    }

    public unLinkTemplate = (templateId: number, domainId: number): ng.IPromise<boolean> => {
        return this.apiRequest("DELETE", `/cookietemplate/templates/${templateId}/domains/${domainId}`, null, this.$rootScope.t("ErrorUpdatingTemplate"))
            .then((response: IProtocolResponse<boolean>): boolean => {
                return response.data;
            });
    }

    public getAssignDomainByTemplateId(templateId: number): ng.IPromise<INumberMap<string>> {
        return this.apiRequest("GET", `/cookietemplate/templates/${templateId}/domains`, null, this.$rootScope.t("ErrorUpdatingTemplate"))
            .then((response: IProtocolResponse<INumberMap<string>>): INumberMap<string> => {
                return response.data;
            });
    }

    public getTemplatePreviewScript(templateId: number, languageId: number): ng.IPromise<IPreviewScript|null> {
        return this.apiRequest("GET", `/cookietemplate/templates/${templateId}/languages/${languageId}/scripts/preview`, null, this.$rootScope.t("ErrorGettingPreviewScript"))
            .then((response: IProtocolPacket): ng.IPromise<IPreviewScript|null> => {
                return response.result ? response.data : null;
            });
    }
}
