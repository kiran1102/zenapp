import * as _ from "lodash";
import Utilities from "Utilities";
import { ProtocolService } from "modules/core/services/protocol.service";
import { CookieComplianceStore } from "cookiesService/cookie-compliance-store.service";
import { TimeStamp } from "oneServices/time-stamp.service";
import { CookieScanStatus } from "constants/cookie.constant";
import { ExportTypes } from "enums/export-types.enum";
import {
    IProtocolPacket,
    IProtocolConfig,
    IProtocolMessages,
    IProtocolMessage,
    IProtocolResponse,
} from "interfaces/protocol.interface";
import { IDomain } from "interfaces/cookies/cc-domain.interface";
import { ITemplateDomainDetails } from "interfaces/cookies/publish-template-modal.interface";
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import { NotificationService } from "modules/shared/services/provider/notification.service";
import { HttpRequest } from "@angular/common/http";

export class WebAuditService {
    static $inject: string[] = [
        "$q",
        "$rootScope",
        "moment",
        "OneProtocol",
        "PRINCIPAL",
        "Accounts",
        "CcConfigService",
        "Export",
        "CookieComplianceStore",
        "TimeStamp",
        "NotificationService",
    ];

    constructor(
        private readonly $q: ng.IQService,
        private readonly $rootScope: IExtendedRootScopeService,
        private readonly moment: any,
        private readonly OneProtocol: ProtocolService,
        readonly PRINCIPAL: any,
        readonly Accounts: any,
        readonly CcConfigService: any,
        readonly Export: any,
        readonly cookieComplianceStore: CookieComplianceStore,
        private readonly timeStamp: TimeStamp,
        private notificationService: NotificationService,
    ) {}

    public apiRequest(requestMethod: any, requestUrl: string, requestObject: any, errorMessage: string, timeout?: number): ng.IPromise<IProtocolPacket> {
        const config: IProtocolConfig = this.OneProtocol.config(requestMethod, requestUrl, [], requestObject, undefined, undefined, undefined, undefined, timeout);
        const messages: IProtocolMessages | IProtocolMessage  = errorMessage ? { Error: { custom: errorMessage } } : { Hide: true };
        return this.OneProtocol.http(config, messages);
    }

    public getWebsiteAudits(showErrors: boolean = true, allowNewSearchSort: boolean, sortKey: string, sortOrder: string, searchString: string, page: number, size: number): ng.IPromise<IProtocolPacket | any[]> {
        let searchApiPath = "";
        if (allowNewSearchSort) {
            searchApiPath = `/audit/AuditList?page=${page}&size=${size}&sortOrder=${sortOrder.toUpperCase()}&sortBy=${sortKey}&searchStr=${this.encodeURI(searchString)}`;
        }
        return this.apiRequest("GET", allowNewSearchSort ? searchApiPath : "/audit/list", null, showErrors ? this.$rootScope.t("ErrorListingAudits") : undefined)
            .then((response: IProtocolPacket): IProtocolPacket | any[] => {
                if (!response.result) {
                    throw response;
                }
                const auditData: any = response.data;
                const domainList: IDomain = response.data.content.map((domain: IDomain): IDomain => {
                    domain.cookiesChangedBy = domain.cookiesFound - domain.lastCookiesFound;
                    if (domain.status === CookieScanStatus.Pending || domain.status === CookieScanStatus.InProgress) {
                        if (domain.lastSuccessfulScanDate !== null && domain.lastSuccessfulScanRowKey !== null) {
                            domain.lastScanDate = domain.lastSuccessfulScanDate;
                        } else if (domain.status === CookieScanStatus.InProgress) {
                            domain.lastSuccessfulScanRowKey = "";
                            domain.lastScanDate = domain.completedTime ? domain.completedTime : null;
                        } else {
                            domain.lastSuccessfulScanRowKey = "";
                            domain.lastScanDate = null;
                        }
                    } else {
                        domain.lastSuccessfulScanRowKey = "";
                        domain.lastScanDate = domain.completedTime ? domain.completedTime : null;
                    }
                    return domain;
                });
                auditData.content = domainList;
                return auditData;
            });
    }

    public getScanHistoryList(domainName: string): ng.IPromise<IProtocolPacket> {
        return this.apiRequest("GET", "/audit/historylist?domain=" + this.encodeURI(domainName), null, this.$rootScope.t("ErrorRetrievingScanHistory")).then((response: IProtocolPacket): any => {
            if (response.result) {
                return response.data.map((domain: any): any => {
                    domain.completedTime = domain.completedTime;
                    domain.scanDateTimeInUserTimezone = this.getDateTimeInUserTimezone(domain.completedTime);
                    domain.cookiesChangedBy = domain.cookiesFound - domain.lastCookiesFound;
                    return domain;
                });
            }
            return [];
        });
    }

    public getSingleWebsiteAudit(domain: string): ng.IPromise<IProtocolPacket> {
        return this.apiRequest("GET", "/audit/get?domain=" + this.encodeURI(domain), null, this.$rootScope.t("ErrorGettingAudit")).then((response: IProtocolPacket): any => {
            if (response.result) {
                response.data.cookiesChangedBy = response.data.cookiesFound - response.data.lastCookiesFound;
                return response.data;
            }
            return null;
        });
    }

    public getWebsiteBanners(showErrors: boolean = true): ng.IPromise<IProtocolPacket | {domains: ITemplateDomainDetails[]}> {
        return this.apiRequest("GET", "/consentNotice/domains", null, showErrors ? this.$rootScope.t("ErrorGettingBanners") : undefined, 30000)
            .then((response: IProtocolPacket): any => {
                if (response.result) {
                    return response.data;
                }
                return response;
            });
    }

    public createConsentNotice(consentNotice: any): ng.IPromise<IProtocolPacket> {
        return this.apiRequest("POST", "/consentNotice/Request", consentNotice, this.$rootScope.t("ErrorCreatingConsentNotice")).then((response: IProtocolPacket): any => {
            return response.result ? response.data : null;
        });
    }

    public updateBannerNotice(consentNotice: any): ng.IPromise<IProtocolPacket> {
        return this.apiRequest("PUT", "/consentNotice/UpdateCookieBanner", consentNotice, this.$rootScope.t("ErrorUpdatingConsentNotice")).then((response: IProtocolPacket): any => {
            return response.result ? response.data : null;
        });
    }

    public updateConsentNotice(consentNotice: any): ng.IPromise<IProtocolPacket> {
        return this.apiRequest("PUT", "/consentNotice/Put", consentNotice, this.$rootScope.t("ErrorUpdatingConsentNotice")).then((response: IProtocolPacket): any => {
            return response.result ? response.data : null;
        });
    }

    public publish(publishObj: any): ng.IPromise<IProtocolPacket> {
        const config = new HttpRequest("POST", "/consentNotice/Publish", publishObj, {responseType: "text"});
        const messages: IProtocolMessages | IProtocolMessage  = { Error: { custom: this.$rootScope.t("ErrorPublishingConsentNotice") } };
        return this.OneProtocol.http(config, messages);
    }

    public getDomainDetails(id: any): ng.IPromise<IProtocolPacket> {
        return this.apiRequest("GET", "/consentNotice/Get?id=" + id, null, this.$rootScope.t("ErrorGettingConsentNotice")).then((response: IProtocolPacket): any => {
            return response.result ? response.data : null;
        });
    }

    public getWebsiteList(showErrors: boolean = true, allowNewSearchSort: boolean = false, sortKey: string = "auditDomain", sortOrder: string = "ASC", searchString: string = "", page: number = 0, size: number = 10): ng.IPromise<IDomain[]> {
        return this.$q.all({
            audits: this.getWebsiteAudits(showErrors, allowNewSearchSort, sortKey, sortOrder, searchString, page, size),
            banners: this.getWebsiteBanners(showErrors),
        }).then((data: any): any => {
            data.audits.content.forEach((website: IDomain): any => {
                const banner = _.find(data.banners.domains, (b: any): boolean => b.DomainName === website.auditDomain);
                website.hasBanner = !_.isUndefined(banner);
                website.domainId = !_.isUndefined(banner) ? banner.DomainId : 0;
                website.domainReadyForBanner = website.status === CookieScanStatus.Complete || website.lastSuccessfulScanDate !== null;
                website.hasScriptArchive = !_.isUndefined(banner) ? banner.HasScriptArchive : false;
                website.templateId = !_.isUndefined(banner) ? banner.TemplateId : 0;
                website.templateName = !_.isUndefined(banner) ? banner.TemplateName : "";
                website.showThirdPartyBannerWarning = website.isThirdPartyBannerEnabled && !website.isBannerWarningClicked && website.scanError === "";
            });
            return data.audits;
        });
    }

    public addWebsite(domain: string, pageLimit: number, keepExistingData: boolean = true, independentPaths: boolean = false,
                      orgGroupId: string, includedQueryParams: string = "", secondaryUris: string = "", isRescan: boolean = false, siteMapsUris: string = ""): ng.IPromise<IProtocolPacket> {
            return this.apiRequest("POST", `/audit/request?orgGroupId=${orgGroupId}`,
            {
                domain,
                independentPaths,
                keepExistingData,
                numberOfPages: pageLimit,
                includedQueryParams,
                secondaryUris,
                isRescan,
                siteMapsUris,
            }, this.$rootScope.t("ErrorRequestingAudit"), 40000).then((response: IProtocolPacket): any => {
                return response.result ? response.data : null;
            });
    }

    public deleteWebsite(domain: string): ng.IPromise<IProtocolPacket> {
        return this.apiRequest("DELETE", "/audit/delete?domain=" + this.encodeURI(domain), null, this.$rootScope.t("ErrorDeletingAudit"));
    }

    public deleteCompleteWebsite(domainId: number): ng.IPromise<IProtocolResponse<boolean>> {
        const config: IProtocolConfig = this.OneProtocol.config("DELETE", `/consentnotice/domains/${domainId}`);
        const messages: IProtocolMessages = { Error: { custom: this.$rootScope.t("ErrorDeletingDomain") } };
        return this.OneProtocol.http(config, messages);
    }

    public getCookieClass(label: string, prefix: string = "graph-container__segment--"): string {
        if (!label) { return ""; }
        if (label === "Targeting/Advertising") {
            return prefix + "targeting";
        }
        return prefix + label.replace(" ", "-").toLowerCase();
    }

    public getSummaryReport(domain: string, rowKey: string = ""): ng.IPromise<IProtocolPacket | void> {
        if (!domain) { return this.$q.when(); }
        return this.apiRequest("GET", !rowKey ? `/audit/report?domain=${this.encodeURI(domain)}` : `/audit/historyreport?domain=${this.encodeURI(domain)}&rowKey=${rowKey}`, null, this.$rootScope.t("ErrorGettingReport")).then((response: IProtocolPacket): any => {
            if (response.result) {
                response.data.domain = domain;
                response.data.cookiesChangedBy = response.data.cookies - response.data.lastCookies;
                response.data.tagsChangedBy = response.data.tags - response.data.lastTags;
                if (_.isUndefined(response.data.newCookiesCount)) {
                    response.data.newCookiesCount = response.data.cookiesChangedBy;
                }
                if (_.isUndefined(response.data.newTagsCount)) {
                    response.data.newTagsCount = response.data.tagsChangedBy;
                }
                response.data.cookiesBreakdown = (response.data.cookieBreakDown && response.data.cookieBreakDown.length > 0) ? response.data.cookieBreakDown.map((cookie: any): any => {
                    return {
                        class: this.getCookieClass(cookie.label),
                        count: cookie.count,
                        label: cookie.label,
                        value: cookie.percentage,
                    };
                }) : [{
                    class: "remainder",
                    label: "",
                    value: 1,
                }];

                response.data.tagsBreakdown = (response.data.tagsBreakDown && response.data.tagsBreakDown.length > 0) ? response.data.tagsBreakDown.map((tag: any): any => {
                    return {
                        class: "graph-container__segment--" + tag.label.replace(" ", "-").toLowerCase(),
                        label: tag.label,
                        value: tag.percentage,
                        count: tag.count,
                    };
                }) : [{
                    class: "remainder",
                    label: "",
                    value: 1,
                }];

                response.data.formsBreakdown = (response.data.formBreakDown && response.data.formBreakDown.length > 0) ? response.data.formBreakDown.map((form: any): any => {
                    return {
                        class: "graph-container__segment--" + form.label.replace(" ", "-").toLowerCase(),
                        label: form.label,
                        value: form.percentage,
                        count: form.count,
                    };
                }) : [{
                    class: "remainder",
                    label: "",
                    value: 1,
                }];
                if (!response.data.behindLoginCookies) {
                    response.data.behindLoginCookies = 0;
                }
                if (!response.data.behindLoginPagesScanned) {
                    response.data.behindLoginPagesScanned = 0;
                }
                // Sorting cookie, tag and form breakdowns
                response.data.cookiesBreakdown = _.sortBy(response.data.cookiesBreakdown, this.cookiePurposeOrder);
                response.data.tagsBreakdown = _.sortBy(response.data.tagsBreakdown, this.tagTypeOrder);
                response.data.formsBreakdown = _.sortBy(response.data.formsBreakdown, this.formFieldOrder);
                return response.data;
            }
            return null;
        });
    }

    public getCookieDetailReport(domain: string, partitionKey: string = ""): ng.IPromise<IProtocolPacket> {
        return this.apiRequest("GET", !partitionKey ? `/audit/cookiereport?domain=${this.encodeURI(domain)}` : `/audit/CookieHistoryReport?domain=${this.encodeURI(domain)}&partitionKey=${partitionKey}`, null, this.$rootScope.t("ErrorGettingReport")).then((response: IProtocolPacket): any => {
            if (response.result) {
                return response.data.map((cookie: any): any => {
                    cookie.combinedDescription = [cookie.hostDescription, cookie.description].join(" ").trim();
                    if (!cookie.combinedDescription) { cookie.combinedDescription = "-"; }
                    if (cookie.isSession) { cookie.expiry = "Session"; } else if (!cookie.expiry) { cookie.expiry = "-"; } else { cookie.expiry = this.moment.duration(cookie.expiry, "seconds").humanize(); }
                    if (!cookie.isBehindLogin) {
                        cookie.isBehindLogin = false;
                    }
                    return cookie;
                });
            }
        });
    }

    public getCookieValuesByName(domain: string, cookieName: string): ng.IPromise<IProtocolPacket> {
        return this.apiRequest("GET", `/audit/cookiebyname?domain=${this.encodeURI(domain)}&cookie=${this.encodeURI(cookieName)}`, null, this.$rootScope.t("ErrorGettingReport")).then((response: IProtocolPacket): any => {
            return response.result ? response.data : null;
        });
    }

    public cancelAudit(domainName: any): ng.IPromise<IProtocolPacket> {
        return this.apiRequest("PUT", `/audit/CancelAudit?domain=${this.encodeURI(domainName)}`, "", this.$rootScope.t("ErrorCancellingAudit")).then((response: IProtocolPacket): any => {
            return response.result ? response.data : null;
        });
    }

    public getCookieValuesByUrl(domain: string, pageUrl: string): ng.IPromise<IProtocolPacket> {
        return this.apiRequest("GET", `/audit/cookiebypage?domain=${this.encodeURI(domain)}&url=${this.encodeURI(pageUrl)}`, null, this.$rootScope.t("ErrorGettingReport")).then((response: IProtocolPacket): any => {
            return response.result ? response.data : null;
        });
    }

    public getTagDetailReport(domain: string, historyKey: string = ""): ng.IPromise<IProtocolPacket> {
        return this.apiRequest("GET", !historyKey ? `/audit/tagsreport?domain=${this.encodeURI(domain)}` : `/audit/taghistoryreport?historyKey=${historyKey}`, null, this.$rootScope.t("ErrorGettingReport")).then((response: IProtocolPacket): any => {
            if (response.result) {
                return response.data.map((host: any): any => {
                    if (host.hasOwnProperty("tags")) {
                        host.tags = host.tags.map((tag: any): any => {
                            if (!tag.age) { tag.age = "-"; } else { tag.age = this.moment.duration(tag.age, "seconds").humanize(); }
                            tag.className = tag.tagType.replace(" ", "-").toLowerCase();
                            return tag;
                        });
                    }
                    return host;
                });
            }
            return null;
        });
    }

    public getFormDetailReport(domain: string): ng.IPromise<IProtocolPacket> {
        return this.apiRequest("GET", "/audit/formsreport?domain=" + this.encodeURI(domain), null, this.$rootScope.t("ErrorGettingReport")).then((response: IProtocolPacket): any => {
            return response.result ? response.data : null;
        });
    }

    public getPagesReport(domain: string): ng.IPromise<IProtocolPacket> {
        return this.apiRequest("GET", "/audit/pagesreport?domain=" + this.encodeURI(domain), null, this.$rootScope.t("ErrorGettingReport")).then((response: IProtocolPacket): any => {
            if (!response.result) {
                return null;
            }
            if (!response.data.isBehindLogin) {
                response.data.isBehindLogin = false;
            }
            return response.data;
        });
    }

    public getLocalStorageReport(domain: string): ng.IPromise<IProtocolPacket> {
        return this.apiRequest("GET", `/audit/localstoragereport?domain=${this.encodeURI(domain)}`, null, this.$rootScope.t("ErrorGettingReport")).then((response: IProtocolPacket): any => {
            return response.result ? response.data : null;
        });
    }

    public getFormPagesByFormName(domain: string, formName: string): ng.IPromise<IProtocolPacket> {
        return this.apiRequest("GET", "/audit/getformpagesbyformname?domain=" + this.encodeURI(domain) + "&&formname=" + formName, null, this.$rootScope.t("ErrorGettingReport")).then((response: IProtocolPacket): any => {
            return response.result ? response.data : null;
        });
    }

    public exportScanResultsByScannedDate = (domain: string, rowKey: string = ""): any => {
        return this.Export.downloadFile(`/audit/ExportScanResult?domain=${this.encodeURI(domain)}&rowKey=${rowKey}`, ExportTypes.XLS, this.$rootScope.t("WebScanFor", {domain}));
    }

    public updateThirdPrtyBannerNotificationStatus(domainName: string, isBannerWarningClicked: boolean, isBannerWarningClosed: boolean): ng.IPromise<IProtocolPacket> {
        return this.apiRequest("PUT", `/audit/banner-warning?domain=${this.encodeURI(domainName)}`,
        {
            isBannerWarningClicked,
            isBannerWarningClosed,
        }, null);
    }

    public validateDomainName = (domain: string): boolean => {
        const re = /(http(s)?:\/\/.)?(www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-z]{2,256}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/g;
        return re.test(domain.toLowerCase());
    }

    public validateIPAddress = (domainIP: string): boolean => {
        const re = /^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/;
        return re.test((domainIP.toLowerCase().replace("http://", "").replace("https://", "")));
    }

    public getDateTimeInUserTimezone = (dt: string): string => {
        return this.timeStamp.formatDate(dt);
    }

    public getSelectedDomainDetails = (selectedWebsite: string, domainList: any): any => {
        return _.find(domainList, (b: any): boolean => b.auditDomain === selectedWebsite);
    }

    public launchCookieCompliance(domainId: number, showAdvancedSettings: boolean, isLivePreview: boolean): void {
        const identity: any = this.PRINCIPAL.getIdentity();
        const expires: any = this.moment.utc(identity[".expires"], "ddd, DD MMM YYYY HH:mm:ss");

        if (this.moment.utc().isAfter(expires)) {
            this.Accounts.refresh().then((): void => {
                this.notificationService.alertWarning(this.$rootScope.t("Warning"), this.$rootScope.t("SessionWasRefreshed"));
            });
        } else {
            const accessToken: string = identity ? identity.access_token : null;
            const optanonUrl: string = this.CcConfigService.getOptanonUrl();
            if (showAdvancedSettings) {
                Utilities.openWindow("POST", optanonUrl, { access_token: accessToken }, "_blank");
            } else {
                const data: any = {
                    id: domainId,
                    access_token: accessToken,
                    LivePreview: isLivePreview,
                };
                let previewUrl: any = optanonUrl.match(/^https?:\/\/([^/]+)/i);
                previewUrl = `http://${previewUrl[1]}/CookieManager/Domain/PreviewLive`;
                Utilities.openWindow("POST", previewUrl, data, "_blank");
            }
        }
    }

    public validDomainFilter(domainList: IDomain[], doScriptArchiveFilterForDomain: boolean = false, selectedWebsite: string = ""): IDomain[] {
        if (!doScriptArchiveFilterForDomain) {
            return _.filter(domainList, (domain: IDomain): boolean => (domain.scanError === "" || domain.status !== CookieScanStatus.Complete) && domain.auditDomain !== null);
        } else {
            domainList = _.filter(domainList, (domain: IDomain): boolean => (domain.scanError === "" || domain.status !== CookieScanStatus.Complete) && domain.auditDomain !== null);
            return _.filter(domainList, (domain: IDomain): boolean => domain.hasScriptArchive || domain.auditDomain === selectedWebsite);
        }
    }

    private encodeURI(queryStringValue: string): string {
        return encodeURIComponent(queryStringValue).replace(/\./g, "%2e");
    }

    private cookiePurposeOrder(cookie): number {
        return ["Strictly Necessary", "Performance", "Functionality", "Targeting/Advertising", "Unknown"].indexOf(cookie.label);
    }

    private tagTypeOrder(tag): number {
        return ["Script", "Image", "Object", "Embed", "Iframe", "WebBeacon"].indexOf(tag.label);
    }

    private formFieldOrder(form): number {
        return ["Personal", "Not personal"].indexOf(form.label);
    }
}
