import * as angular from "angular";
import * as _ from "lodash";
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import { IProtocolPacket, IProtocolConfig, IProtocolMessages} from "interfaces/protocol.interface";
import { ProtocolService } from "modules/core/services/protocol.service";

export class CookieOrgGroupService {
    static $inject: string[] = ["OneProtocol", "$rootScope"];
    private Protocol: any;

    constructor(private readonly OneProtocol: ProtocolService, private readonly $rootScope: IExtendedRootScopeService) {
        this.Protocol = OneProtocol;
    }

    public apiRequest(requestMethod: any, requestUrl: string, requestObject: any, object: string, action: string, timeout?: number): ng.IPromise<IProtocolPacket> {
        const config: IProtocolConfig = this.Protocol.config(requestMethod, requestUrl, [], requestObject, undefined, undefined, undefined, undefined, timeout);
        const messages: IProtocolMessages = { Error: { object, action } };
        return this.Protocol.http(config, messages);
    }
    public reassignOrgGroup(domain: string, orgGroupId: string): ng.IPromise<IProtocolPacket> {
        return this.apiRequest("PUT", `/audit/ReassignOrgGroup?domain=${domain}&organization=${orgGroupId}`, null, this.$rootScope.t("Organization"), this.$rootScope.t("Reassign")).then((response: IProtocolPacket): ng.IPromise<IProtocolPacket> => {
            return response.result ? response.data : null;
        });
    }
}
