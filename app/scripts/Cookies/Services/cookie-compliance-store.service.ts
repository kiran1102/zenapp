import {find, includes, each } from "lodash";

export class CookieComplianceStore {

    static $inject: string[] = ["$q", "$templateCache", "$timeout", "$state"];
    private selectedWebsite: string;
    private websiteList: any[];
    private bannerLanguageDomainId: number;
    private bannerLanguages: any[];

    constructor(
        private readonly $q: ng.IQService,
        readonly $templateCache: ng.ITemplateCacheService,
        readonly $timeout: ng.ITimeoutService,
        readonly $state: ng.ui.IStateService) { }

    public setSelectedWebsite(selectedWebsite: string): void {
        if (this.selectedWebsite !== selectedWebsite) {
            this.selectedWebsite = selectedWebsite;
            this.setSelectedLanguageDomainId(undefined);
        }
    }
    public getSelectedWebsite(): string {
        return this.selectedWebsite;
    }
    public setWebsiteList(websiteList: any): void {
        this.websiteList = websiteList;
    }
    public getWebsiteList(): any {
        return this.websiteList;
    }
    public getSelectedDomainId = (): any => {
        if (!this.bannerLanguageDomainId) {
            const websiteList: any = this.getWebsiteList();
            const selectedWebsite: string = this.getSelectedWebsite();
            const domain: any = find(websiteList, (site: any): boolean => site.auditDomain === selectedWebsite);
            return domain.domainId;
        }
        return this.bannerLanguageDomainId;
    }
    public getDomainByName = (domainName: string): any => {
        return find(this.websiteList, (domain: any): boolean => domain.auditDomain === domainName);
    }

    public registerVendorModal = (): void => {
        if (!this.$templateCache.get("ccGroupVendorModal.html")) {
            this.$templateCache.put("ccGroupVendorModal.html", "<cc-group-vendor-modal scope='controller'></cc-group-vendor-modal>");
        }
    }

    public registerLanguageModal = (): void => {
        if (!this.$templateCache.get("ccLanguageModal.html")) {
            this.$templateCache.put("ccLanguageModal.html", "<cc-language-modal scope='controller'></cc-language-modal>");
        }
    }
    public setSelectedLanguageDomainId = (domainId): void | undefined => {
        this.bannerLanguageDomainId = domainId;
    }

    public getBannerLanguages = (): any[] => {
        return this.bannerLanguages;
    }
    public setBannerLanguages = (bannerLanguages: any[]): void => {
        this.bannerLanguages = bannerLanguages;
    }
}
