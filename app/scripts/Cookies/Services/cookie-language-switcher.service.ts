// Services
import { ProtocolService } from "modules/core/services/protocol.service";
import { NotificationService } from "modules/shared/services/provider/notification.service";

// Interfaces
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import {
    IProtocolPacket,
    IProtocolConfig,
    IProtocolMessages,
    IProtocolResponse,
} from "interfaces/protocol.interface";
import { ILanguageSwitcher } from "interfaces/cookies/cc-language-switcher.interface";
import {
    IGeoLocationSwitcher,
    IGeoLocationSwitcherRequest,
} from "interfaces/cookies/geo-location-switcher.interface";

export class CookieLanguageSwitcherService {
    static $inject: string[] = ["OneProtocol", "$rootScope", "NotificationService" ];

    constructor(private readonly OneProtocol: ProtocolService, private readonly $rootScope: IExtendedRootScopeService, private notificationService: NotificationService) { }

    public apiRequest(requestMethod: any, requestUrl: string, requestObject: any, errorMessage: string): ng.IPromise<IProtocolPacket> {
        const config: IProtocolConfig = this.OneProtocol.config(requestMethod, requestUrl, [], requestObject);
        const messages: IProtocolMessages = { Error: { custom: errorMessage } };
        return this.OneProtocol.http(config, messages);
    }

    public getLanguageSwitcherStatus = (domainId: number): ng.IPromise<ILanguageSwitcher | null> => {
        return this.apiRequest("GET", `/cookiepolicy/GetLanguageSwitcher?domainId=${domainId}`, null, this.$rootScope.t("ErrorGettingLanguageSwitcherStatus"))
            .then((response: IProtocolPacket): ng.IPromise<ILanguageSwitcher> => {
                return response.result ? response.data : null;
            });
    }

    public updateLanguageDetection(domainId: number, isEnabled: boolean, defaultLanguage: string): ng.IPromise<ILanguageSwitcher | null> {
        const languageSwitcherRequest: ILanguageSwitcher = {
            isEnabled,
            defaultLanguage,
            domainId,
        };
        return this.apiRequest("PUT", `/cookiepolicy/CreateOrUpdate`, languageSwitcherRequest, this.$rootScope.t("ErrorUpdatingLanguageDetection"))
            .then((response: IProtocolPacket): ng.IPromise<ILanguageSwitcher | null> => {
                return response.result ? response.data : null;
            });
    }

    public publishLanguageLoader(domainId: number): ng.IPromise<ILanguageSwitcher | null> {
        return this.apiRequest("POST", `/cookiepolicy/PublishLanguageSwitcher?domainId=${domainId}`, null, this.$rootScope.t("ErrorPublishingLanguageSwitcher"))
            .then((response: IProtocolPacket): ng.IPromise<ILanguageSwitcher | null> => {
                return response.result ? response.data : null;
            });
    }

    public checkAndNotifyLanguageSwitcher(domainId: number): void {
        this.getLanguageSwitcherStatus(domainId).then((languageSwitcher: ILanguageSwitcher): void => {
            if (languageSwitcher) {
                if (languageSwitcher.isEnabled) {
                    this.notificationService.alertNote(this.$rootScope.t("NeedAttention"), this.$rootScope.t("PublishLanguageSwitcherNotification"));
                }
            }
        });
    }

    getGeoLocationSwitcher(domainId: number): ng.IPromise<IProtocolResponse<IGeoLocationSwitcher | null>> {
        const config: IProtocolConfig = this.OneProtocol.config("GET", `/geolocationswitcher/domains/${domainId}`);
        const messages: IProtocolMessages = { Error: { custom: this.$rootScope.t("ErrorGettingGeoLocationSwitcherStatus") } };
        return this.OneProtocol.http(config, messages);
    }

    updateGeoLocationSwitcher(domainId: number, payload: IGeoLocationSwitcherRequest): ng.IPromise<IProtocolResponse<IGeoLocationSwitcher | null>> {
        const config: IProtocolConfig = this.OneProtocol.config("PUT", `/geolocationswitcher/domains/${domainId}`, [], payload);
        const messages: IProtocolMessages = { Error: { custom: this.$rootScope.t("ErrorUptingGeoLocationSwitcher") } };
        return this.OneProtocol.http(config, messages);
    }

    publishGeoLocationSwitcherChanges(domainId: number): ng.IPromise<IProtocolResponse<IGeoLocationSwitcher | null>> {
        const config: IProtocolConfig = this.OneProtocol.config("POST", `/geolocationswitcher/domains/${domainId}/script`);
        const messages: IProtocolMessages = { Error: { custom: this.$rootScope.t("ErrorPublishingGeoLocationSwitcher") } };
        return this.OneProtocol.http(config, messages);
    }
}
