// Rxjs
import {
    Observable,
    Subject,
    from,
    timer,
    interval,
} from "rxjs";
import {
    map,
    switchMap,
    take,
    takeUntil,
    takeWhile,
} from "rxjs/operators";

// Interfaces
import { IStringMap } from "interfaces/generic.interface";

// Services
import { CookieMultiLanguageService } from "cookiesService/cookie-multi-language.service";

// Constants
import { IBannerLangReponse } from "interfaces/cookies/cc-banner-languages.interface";

export class LanguagePollingService {

    static $inject: string[] = [
        "CookieMultiLanguageService",
    ];

    private pollingObservable: Observable<IBannerLangReponse> = null;
    private stopPollingInterval = 45000;
    private resetPolling: Subject<string> = new Subject();
    private stopPolling: Observable<number> = interval(this.stopPollingInterval).pipe(
        take(1),
    );
    private defaultConfig: IStringMap<number> = {
        initialDelay: 0,
        interval: 10000,
    };

    constructor(
        private readonly CookieMultiLanguageService: CookieMultiLanguageService,
    ) {}

    startPolling(initialDelay: number = this.defaultConfig.initialDelay, interval: number = this.defaultConfig.interval, domainId: number, languagePollingObservable: Subject<IBannerLangReponse>): void {
        if (this.pollingObservable && this.hasDefaultConfig(initialDelay, interval)) {
            this.resetPolling.next();
        } else {
            this.pollingObservable = this.getPollingObservable(initialDelay, interval, domainId, languagePollingObservable);
        }
        this.stopPolling.subscribe(() => {
            languagePollingObservable.next({languages: [], failedLanguges: [], refreshNeeded: false});
        });
        this.pollingObservable.subscribe();
    }

    private hasDefaultConfig(initialDelay: number, interval: number): boolean {
        return initialDelay === this.defaultConfig.initialDelay && interval === this.defaultConfig.interval;
    }

    private getPollingObservable(initialDelay: number = this.defaultConfig.initialDelay, intervalDelay: number = this.defaultConfig.interval, domainId: number, languagePollingObservable: Subject<IBannerLangReponse>): Observable<IBannerLangReponse> {
        return timer(initialDelay, intervalDelay).pipe(
            switchMap(() => from(this.CookieMultiLanguageService.getBannerLanguages(domainId))),
            map((data) => {
                languagePollingObservable.next(data);
                return data;
            }),
            takeWhile((data) => data.refreshNeeded),
            takeUntil(this.stopPolling),
        );
    }

}
