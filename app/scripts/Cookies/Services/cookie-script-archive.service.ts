import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import {
    IProtocolPacket,
    IProtocolConfig,
    IProtocolMessages,
} from "interfaces/protocol.interface";
import { ProtocolService } from "modules/core/services/protocol.service";
import { ExportTypes } from "enums/export-types.enum";

export default class CookieScriptArchiveService {
    static $inject: string[] = ["OneProtocol", "$rootScope", "moment", "Export"];
    private translate: any;

    constructor(
        private readonly OneProtocol: ProtocolService,
        private readonly $rootScope: IExtendedRootScopeService,
        private readonly moment: any,
        private readonly Export: any,
    ) {
        this.translate = this.$rootScope.t;
    }

    public apiRequest(requestMethod: any, requestUrl: string, requestObject: any, errorMessage: string): ng.IPromise<IProtocolPacket> {
        const config: IProtocolConfig = this.OneProtocol.config(requestMethod, requestUrl, [], requestObject);
        const messages: IProtocolMessages = { Error: { custom: errorMessage } };
        return this.OneProtocol.http(config, messages);
    }

    public exportScriptArchiveList = (domainId: number): ng.IPromise<any> => {
        return this.Export.downloadFile(`/cookiepolicy/DownloadArchiveScriptReports?domainId=${domainId}`, ExportTypes.XLSX, this.$rootScope.t("ScriptArchive"));
    }

    public exportPubVendors = (domainId: number): ng.IPromise<any> => {
        return this.Export.downloadFile(`/consentnotice/domains/${domainId}/pubvendors`, ExportTypes.JSON, this.$rootScope.t("PubVendors"));
    }
}
