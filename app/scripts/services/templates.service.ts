import * as _ from "lodash";
import { ProtocolService } from "modules/core/services/protocol.service";
import { IProtocolPacket, IProtocolMessages, IProtocolConfig, IProtocolResponse } from "interfaces/protocol.interface";
import { ISection } from "interfaces/section.interface";
import {
    ITemplate,
    IGalleryType,
    ITemplateAPIParams,
    ITemplateTypesParams,
    ITemplateReorderData,
    ITemplateDropdownItem,
    IProjectDropdownItem,
} from "modules/template/interfaces/template.interface";
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";

export default class TemplateService {
    static $inject: string[] = ["$rootScope", "OneProtocol", "ENUMS"];

    private translations: any;

    constructor(
        readonly $rootScope: IExtendedRootScopeService,
        readonly OneProtocol: ProtocolService,
        readonly ENUMS: any,
    ) {
        this.translations = {
            CouldNotRetrieveQuestionnairesGallery: $rootScope.t("CouldNotRetrieveQuestionnairesGallery"),
            CouldNotReadQuestionnaire: $rootScope.t("CouldNotReadQuestionnaire"),
            QuestionnaireCopied: $rootScope.t("QuestionnaireCopied"),
            CouldNotCopyQuestionnaire: $rootScope.t("CouldNotCopyQuestionnaire"),
            CouldNotRetrieveQuestionnaires: $rootScope.t("CouldNotRetrieveQuestionnaires"),
            CouldNotRetrievePublishedQuestionnaires: $rootScope.t("CouldNotRetrievePublishedQuestionnaires"),
            QuestionnaireCreated: $rootScope.t("QuestionnaireCreated"),
            CouldNotCreateQuestionnaire: $rootScope.t("CouldNotCreateQuestionnaire"),
            UpdatedQuestionnaire: $rootScope.t("UpdatedQuestionnaire"),
            CouldNotUpdateQuestionnaire: $rootScope.t("CouldNotUpdateQuestionnaire"),
            QuestionnaireDeleted: $rootScope.t("QuestionnaireDeleted"),
            CouldNotDeleteQuestionnaire: $rootScope.t("CouldNotDeleteQuestionnaire"),
            QuestionnairePublished: $rootScope.t("QuestionnairePublished"),
            CouldNotPublishQuestionnaire: $rootScope.t("CouldNotPublishQuestionnaire"),
            QuestionnaireActivated: $rootScope.t("QuestionnaireActivated"),
            CouldNotActivateQuestionnaire: $rootScope.t("CouldNotActivateQuestionnaire"),
            QuestionnaireArchived: $rootScope.t("QuestionnaireArchived"),
            CouldNotArchiveQuestionnaire: $rootScope.t("CouldNotArchiveQuestionnaire"),
            CouldNotRetrieveArchive: $rootScope.t("CouldNotRetrieveArchive"),
            SectionAddedToTheQuestionnaire: $rootScope.t("SectionAddedToTheQuestionnaire"),
            CouldNotAddTheSectionToQuestionnaire: $rootScope.t("CouldNotAddTheSectionToQuestionnaire"),
            CouldNotRemoveSectionFromQuestionnaire: $rootScope.t("CouldNotRemoveSectionFromQuestionnaire"),
            QuestionnaireVersionCreated: $rootScope.t("QuestionnaireVersionCreated"),
            CouldNotCreateNewVersionForQuestionnaire: $rootScope.t("CouldNotCreateNewVersionForQuestionnaire"),
            CouldNotGetVersionHistoryForQuestionnaire: $rootScope.t("CouldNotGetVersionHistoryForQuestionnaire"),
            SectionReordered: $rootScope.t("SectionReordered"),
            CouldNotReorderSection: $rootScope.t("CouldNotReorderSection"),
        };
    }

    public gallery(type: IGalleryType): ng.IPromise<IProtocolPacket> {
        const config: IProtocolConfig = this.OneProtocol.config("GET", `/gallery/getgallery/`);
        const messages: IProtocolMessages = { Error: { custom: this.translations.CouldNotRetrieveQuestionnairesGallery } };
        return this.OneProtocol.http(config, messages).then((res: IProtocolPacket): IProtocolPacket => {
            this.$rootScope.APICache.put("Templates.gallery", res);
            return res;
        });
    }

    public readTemplateGallery(params: ITemplateAPIParams): ng.IPromise<IProtocolPacket> {
        const version: string = (!_.isUndefined(params.version)) ? params.version : "";
        const config: IProtocolConfig = this.OneProtocol.config("GET", `/gallery/get/${params.id}/${version}`);
        const messages: IProtocolMessages = { Error: { custom: this.translations.CouldNotReadQuestionnaire } };
        return this.OneProtocol.http(config, messages);
    }

    public cloneTemplateGallery(template: ITemplate): ng.IPromise<IProtocolPacket> {
        const config: IProtocolConfig = this.OneProtocol.config("POST", `/gallery/clone`, [], template);
        const messages: IProtocolMessages = {
            Success: { custom: this.translations.QuestionnaireCopied },
            Error: { custom: this.translations.CouldNotCopyQuestionnaire },
        };
        return this.OneProtocol.http(config, messages);
    }

    public buildParams(templateType: string): ITemplateTypesParams {
        const defaultArray: string[] = [this.ENUMS.TemplateTypes.Custom, this.ENUMS.TemplateTypes.SelfService];
        if (templateType === this.ENUMS.TemplateTypes.Custom || templateType === this.ENUMS.TemplateTypes.Custom.toString()) {
            return { types: defaultArray };
        }
        return templateType ? { types: [templateType] } : { types: defaultArray };
    }

    public list(templateType: string): ng.IPromise<IProtocolPacket> {
        const params: ITemplateTypesParams = this.buildParams(templateType);
        const config: IProtocolConfig = this.OneProtocol.config("GET", `/template/getactive/`, params);
        const messages: IProtocolMessages = {
            Error: { custom: this.translations.CouldNotRetrieveQuestionnaires },
        };
        return this.OneProtocol.http(config, messages).then((res: IProtocolPacket): IProtocolPacket => {
            this.$rootScope.APICache.put("Templates.list", res);
            return res;
        });
    }

    public publishedList(templateType: string): ng.IPromise<IProtocolPacket> {
        const params: ITemplateTypesParams = this.buildParams(templateType);
        const config: IProtocolConfig = this.OneProtocol.config("GET", `/template/getpublished/`, params);
        const messages: IProtocolMessages = {
            Error: { custom: this.translations.CouldNotRetrievePublishedQuestionnaires },
        };
        return this.OneProtocol.http(config, messages).then((res: IProtocolPacket): IProtocolPacket => {
            this.$rootScope.APICache.put("Templates.publishedList", res);
            return res;
        });
    }

    public getTemplateDropdownList(): ng.IPromise<IProtocolResponse<ITemplateDropdownItem[]>> {
        const config: IProtocolConfig = this.OneProtocol.config("GET", `/template/templatesdropdown/`);
        const messages: IProtocolMessages = { Error: { custom: this.translations.CouldNotRetrievePublishedQuestionnaires } };
        return this.OneProtocol.http(config, messages);
    }

    public getProjectDropdownList = (templateId: string, recordId: string): ng.IPromise<IProtocolResponse<IProjectDropdownItem[]>> => {
        const config: IProtocolConfig = this.OneProtocol.config("GET", `/template/projectsdropdown/${templateId}?inventoryId=${recordId}`);
        const messages: IProtocolMessages = { Error: { custom: this.translations.CouldNotRetrieveQuestionnaires } };
        return this.OneProtocol.http(config, messages);
    }

    public latestPublishedList(templateType: string): ng.IPromise<IProtocolPacket> {
        const params: ITemplateTypesParams = this.buildParams(templateType);
        const config: IProtocolConfig = this.OneProtocol.config("GET", `/template/getlatestpublished/`, params);
        const messages: IProtocolMessages = {
            Error: { custom: this.translations.CouldNotRetrievePublishedQuestionnaires },
        };
        return this.OneProtocol.http(config, messages).then((res: IProtocolPacket): IProtocolPacket => {
            this.$rootScope.APICache.put("Templates.publishedList", res);
            return res;
        });
    }

    public cloneTemplate(template: ITemplate): ng.IPromise<IProtocolPacket> {
        const config: IProtocolConfig = this.OneProtocol.config("POST", `/template/clone/`, [], template);
        const messages: IProtocolMessages = {
            Success: { custom: this.translations.QuestionnaireCopied },
            Error: { custom: this.translations.CouldNotCopyQuestionnaire },
        };
        return this.OneProtocol.http(config, messages);
    }

    public createTemplate(template: ITemplate): ng.IPromise<IProtocolPacket> {
        const config: IProtocolConfig = this.OneProtocol.config("POST", `/template/post/`, [], template);
        const messages: IProtocolMessages = {
            Success: { custom: this.translations.QuestionnaireCreated },
            Error: { custom: this.translations.CouldNotCreateQuestionnaire },
        };
        return this.OneProtocol.http(config, messages).then((res: IProtocolPacket): IProtocolPacket => {
            const templateData: any = res.data ? (res.data.Id ? res.data.Id : res.data) : res.data;
            return _.assign({}, res, templateData);
        });
    }

    public readTemplate(params: ITemplateAPIParams): ng.IPromise<IProtocolPacket> {
        const version: string = (!_.isUndefined(params.version)) ? params.version : "";
        const config: IProtocolConfig = this.OneProtocol.config("GET", `/template/get/${params.id}/${version}`);
        const messages: IProtocolMessages = { Error: { custom: this.translations.CouldNotReadQuestionnaire } };
        return this.OneProtocol.http(config, messages);
    }

    public updateTemplate(template: ITemplate): ng.IPromise<IProtocolPacket> {
        const config: IProtocolConfig = this.OneProtocol.config("PUT", `/template/put`, [], template);
        const messages: IProtocolMessages = {
            Success: { custom: this.translations.UpdatedQuestionnaire },
            Error: { custom: this.translations.CouldNotUpdateQuestionnaire },
        };
        return this.OneProtocol.http(config, messages);
    }

    public deleteTemplate(params: ITemplateAPIParams, hideNotification?: boolean): ng.IPromise<IProtocolPacket> {
        const version: string = (!_.isUndefined(params.version)) ? params.version : "";
        const config: IProtocolConfig = this.OneProtocol.config("DELETE", `/template/delete/${params.id}/${params.version}`);
        const messages: IProtocolMessages = {
            Success: { custom: this.translations.QuestionnaireDeleted },
            Error: { custom: this.translations.CouldNotDeleteQuestionnaire },
        };
        return this.OneProtocol.http(config, messages);
    }

    public publishTemplate(id: string): ng.IPromise<IProtocolPacket> {
        const config: IProtocolConfig = this.OneProtocol.config("PUT", `/template/publish/${id}`);
        const messages: IProtocolMessages = {
            Success: { custom: this.translations.QuestionnairePublished },
            Error: { custom: this.translations.CouldNotPublishQuestionnaire },
        };
        return this.OneProtocol.http(config, messages).then((res: IProtocolPacket): IProtocolPacket => {
            this.$rootScope.APICache.clearAll();
            return res;
        });
    }

    public activateTemplate(id: string): ng.IPromise<IProtocolPacket> {
        const config: IProtocolConfig = this.OneProtocol.config("PUT", `/template/activate/${id}`);
        const messages: IProtocolMessages = {
            Success: { custom: this.translations.QuestionnaireActivated },
            Error: { custom: this.translations.CouldNotActivateQuestionnaire },
        };
        return this.OneProtocol.http(config, messages);
    }

    public archiveTemplate(params: ITemplateAPIParams): ng.IPromise<IProtocolPacket> {
        const version: string = (!_.isUndefined(params.version)) ? params.version : "";
        const config: IProtocolConfig = this.OneProtocol.config("PUT", `/template/archive/${params.id}/${version}`);
        const messages: IProtocolMessages = {
            Success: { custom: this.translations.QuestionnaireArchived },
            Error: { custom: this.translations.CouldNotArchiveQuestionnaire },
        };
        return this.OneProtocol.http(config, messages);
    }

    public readArchive(): ng.IPromise<IProtocolPacket> {
        const config: IProtocolConfig = this.OneProtocol.config("GET", `/template/archive`);
        const messages: IProtocolMessages = {
            Error: { custom: this.translations.CouldNotRetrieveArchive },
        };
        return this.OneProtocol.http(config, messages);
    }

    public addSection(section: ISection): ng.IPromise<IProtocolPacket> {
        const config: IProtocolConfig = this.OneProtocol.config("POST", `/template/addsection/`, [], section);
        const messages: IProtocolMessages = {
            Success: { custom: this.translations.SectionAddedToTheQuestionnaire },
            Error: { custom: this.translations.CouldNotAddTheSectionToQuestionnaire },
        };
        return this.OneProtocol.http(config, messages);
    }

    public reorderSection(sectionReorderData: ITemplateReorderData): ng.IPromise<IProtocolPacket> {
        const config: IProtocolConfig = this.OneProtocol.config("PUT", `/template/reordersection/`, [], sectionReorderData);
        const messages: IProtocolMessages = {
            Success: { custom: this.translations.SectionReordered },
            Error: { custom: this.translations.CouldNotReorderSection },
        };
        return this.OneProtocol.http(config, messages);
    }

    public deleteSection(templateId: string, sectionId: string): ng.IPromise<IProtocolPacket> {
        const params: any = { templateId, sectionId };
        const config: IProtocolConfig = this.OneProtocol.config("DELETE", `/template/removesection/`, params);
        const messages: IProtocolMessages = {
            Error: { custom: this.translations.CouldNotRemoveSectionFromQuestionnaire },
        };
        return this.OneProtocol.http(config, messages);
    }

    public createVersion(id: string): ng.IPromise<IProtocolPacket> {
        const config: IProtocolConfig = this.OneProtocol.config("POST", `/template/version/${id}`);
        const messages: IProtocolMessages = {
            Success: { custom: this.translations.QuestionnaireVersionCreated },
            Error: { custom: this.translations.CouldNotCreateNewVersionForQuestionnaire },
        };
        return this.OneProtocol.http(config, messages);
    }

    public getVersions(params: ITemplateAPIParams): ng.IPromise<IProtocolPacket> {
        const version: string = params.version ? params.version : "";
        const config: IProtocolConfig = this.OneProtocol.config("GET", `/template/getversions/${params.id}/${version}`);
        const messages: IProtocolMessages = {
            Error: { custom: this.translations.CouldNotGetVersionHistoryForQuestionnaire },
        };
        return this.OneProtocol.http(config, messages);
    }

    public excludeThresholdTemplates(template: ITemplate): boolean {
        return !template.HasParents;
    }

}
