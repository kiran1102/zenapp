
import * as _ from "lodash";

import { IObserver, IUnsubscribeCallback, IAction } from "interfaces/redux.interface";
import { IOrgGroupState, IOrganization } from "interfaces/org.interface";

import OrgTreeNormalizer from "../Shared/Services/org-tree-normalizer";

// TODO deprecate this entire service and subscribe to store: IStore instead
export default class OrgGroupStoreNew {

    static $inject: string[] = ["OrgGroups"];
    private orgGroupState: IOrgGroupState;
    private observers: IObserver[] = [];

    constructor(readonly OrgGroups) {
        this.orgGroupState = {
            tenantNormalizer: null,
            rootNormalizer: null,
            normalizer: null,
            tenantOrgTree: null,
            orgTree: null,
            selectedOrg: null,
        };
    }

    public dispatch(action: IAction): void {
        this.orgGroupState = this.reducer(this.orgGroupState, action);
        const obsLength: number = this.observers.length;
        for (let i = 0; i < obsLength; i++) {
            this.observers[i]();
        }
    }

    public handleLegacyStoreFetch = (rootOrgId: string, selectedOrgId: string, rootOrgTree: IOrganization) => {
        this.dispatch({ type: "TENANT_ORG_TREE_UPDATED", payload: { tenantOrgTree: rootOrgTree } });

        const orgTree: IOrganization = rootOrgId ? this.recursivelyFindTreeOrgById(rootOrgTree, rootOrgId) : null;
        let selectedOrg: any = null;

        if (orgTree) {
            this.dispatch({ type: "ORG_TREE_UPDATED", payload: { orgTree } });
        }
        if (selectedOrgId) {
            selectedOrg = this.rootOrgTable[selectedOrgId];
        }
        if (selectedOrg) {
            this.dispatch({ type: "ORG_SELECTED", payload: { org: selectedOrg } });
        }
    }

    public fetchOrg(rootOrgId: string, selectedOrgId: string): ng.IPromise<void|undefined> {
        return this.OrgGroups.rootTree().then((res: any): void|undefined => {
            if (!res.result) {
              return;
            }
            this.handleLegacyStoreFetch(rootOrgId, selectedOrgId, res.data ? res.data[0] : null);
        });
    }

    public subscribe(observer: IObserver): IUnsubscribeCallback {
        observer();
        this.observers.push(observer);
        return (): void => {
            this.observers = this.observers.filter((obs) => obs !== observer);
        };
    }

    public saveOrgToCache(orgTree: any[]): void {
        this.orgGroupState.orgTree = orgTree;
    }

    private reducer(state: IOrgGroupState, action: IAction): IOrgGroupState {
        switch (action.type) {
            case "ORG_SELECTED":
                return this.handleSelectedNormalizer({ ...state, selectedOrg: action.payload.org });
            case "ORG_TREE_UPDATED":
                this.saveOrgToCache(action.payload.orgTree);
                return this.handleRootNormalizer({ ...state, orgTree: action.payload.orgTree });
            case "TENANT_ORG_TREE_UPDATED":
                return this.handleTenantNormalizer({ ...state, tenantOrgTree: action.payload.tenantOrgTree });
            default:
                return state;
        }
    }

    private handleTenantNormalizer(state: IOrgGroupState): IOrgGroupState {
        state.tenantNormalizer = new OrgTreeNormalizer(state.tenantOrgTree);
        return state;
    }

    private handleRootNormalizer(state: IOrgGroupState): IOrgGroupState {
        state.rootNormalizer = new OrgTreeNormalizer(state.orgTree);
        return state;
    }

    private handleSelectedNormalizer(state: IOrgGroupState): IOrgGroupState {
        // NOTE: The following is necessary since the normalizer only accepts denormalized org trees.
        // We must find the selectedOrg in a denormalized tree and then normalize it afterwards.
        // TODO: Simplify below based on requirements of above comment.
        if (!state.selectedOrg) {
          state.selectedOrg = state.rootNormalizer.orgList[0];
        }
        const selectedOrg: any = this.recursivelyFindTreeOrgById(state.orgTree, state.selectedOrg.id);
        if (selectedOrg) {
          state.normalizer = new OrgTreeNormalizer(selectedOrg);
        }
        return state;
    }

    private handleAllNormalizers(state: IOrgGroupState): IOrgGroupState {
        return this.handleSelectedNormalizer(this.handleRootNormalizer(this.handleTenantNormalizer(state)));
    }

    private recursivelyFindTreeOrgById(org: IOrganization, orgId: string): IOrganization {
        if (!orgId || !org) {
          return null;
        } else if (org.Id === orgId) {
          return org;
        } else if (_.isArray(org.Children) && org.Children.length) {
            let childOrg: any = null;
            const childrenCount = org.Children.length;
            for (let i = 0; i < childrenCount; i++) {
                childOrg = this.recursivelyFindTreeOrgById(org.Children[i], orgId);
                if (childOrg) {
                  return childOrg;
                }
            }
        }
        return null;
    }

    get selectedOrg(): any {
        return this.orgGroupState.selectedOrg;
    }

    set selectedOrg(selectedOrg: any) {
        this.orgGroupState.selectedOrg = selectedOrg;
    }

    get tenantOrgList(): any|null {
        if (this.orgGroupState.orgTree) {
            if (!this.orgGroupState.tenantNormalizer) this.orgGroupState = this.handleAllNormalizers(this.orgGroupState);
            return this.orgGroupState.tenantNormalizer.orgList;
        }
        return null;
    }

    set tenantOrgList(orgList: any) {
        throw new Error("setting tenantOrgList is forbidden!");
    }

    get tenantOrgTable(): any|null {
        if (this.orgGroupState.orgTree) {
            if (!this.orgGroupState.tenantNormalizer) this.orgGroupState = this.handleAllNormalizers(this.orgGroupState);
            return this.orgGroupState.tenantNormalizer.orgTable;
        }
        return null;
    }

    set tenantOrgTable(orgTable: any) {
        throw new Error("setting tenantOrgTree is forbidden!");
    }

    get tenantOrgTree(): any|null {
        if (this.orgGroupState.orgTree) {
            if (!this.orgGroupState.tenantNormalizer) this.orgGroupState = this.handleAllNormalizers(this.orgGroupState);
            return this.orgGroupState.tenantNormalizer.orgTree;
        }
        return null;
    }

    set tenantOrgTree(orgTree: any) {
        throw new Error("setting tenantOrgTree is forbidden!");
    }

    get rootOrgList(): any|null {
        if (this.orgGroupState.orgTree) {
            if (!this.orgGroupState.rootNormalizer) this.orgGroupState = this.handleAllNormalizers(this.orgGroupState);
            return this.orgGroupState.rootNormalizer.orgList;
        }
        return null;
    }

    set rootOrgList(orgList: any) {
        throw new Error("setting rootOrgList is forbidden!");
    }

    get rootOrgTable(): any|null {
        if (this.orgGroupState.orgTree) {
            if (!this.orgGroupState.rootNormalizer) this.orgGroupState = this.handleAllNormalizers(this.orgGroupState);
            return this.orgGroupState.rootNormalizer.orgTable;
        }
        return null;
    }

    set rootOrgTable(orgTable: any) {
        throw new Error("setting rootOrgTree is forbidden!");
    }

    get rootOrgTree(): any|null {
        if (this.orgGroupState.orgTree) {
            if (!this.orgGroupState.rootNormalizer) this.orgGroupState = this.handleAllNormalizers(this.orgGroupState);
            return this.orgGroupState.rootNormalizer.orgTree;
        }
        return null;
    }

    set rootOrgTree(orgTree: any) {
        throw new Error("setting rootOrgTree is forbidden!");
    }

    get orgList(): any|null {
        if (this.orgGroupState.orgTree) {
            if (!this.orgGroupState.normalizer) this.orgGroupState = this.handleAllNormalizers(this.orgGroupState);
            return this.orgGroupState.normalizer.orgList;
        }
        return null;
    }

    set orgList(orgList: any) {
        throw new Error("setting orgList is forbidden!");
    }

    get orgTable(): any|null {
        if (this.orgGroupState.orgTree) {
            if (!this.orgGroupState.normalizer) this.orgGroupState = this.handleAllNormalizers(this.orgGroupState);
            return this.orgGroupState.normalizer.orgTable;
        }
        return null;
    }

    set orgTable(orgTable: any) {
        throw new Error("setting orgTree is forbidden!");
    }

    get orgTree(): any|null {
        if (this.orgGroupState.orgTree) {
            if (!this.orgGroupState.normalizer) this.orgGroupState = this.handleAllNormalizers(this.orgGroupState);
            return this.orgGroupState.normalizer.orgTree;
        }
        return null;
    }

    set orgTree(orgTree: any) {
        throw new Error("setting orgTree is forbidden!");
    }

}
