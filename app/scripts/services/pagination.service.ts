import { chunk } from "lodash";
import {
    IPageable,
    IPageableResponse,
    IPageableMeta,
} from "interfaces/pagination.interface";

export class PaginationService {

    private pages: any[];
    private size: number;
    private itemCount: number;
    private emptyPage: IPageable = {
        content: [],
        first: true,
        last: true,
        number: 0,
        numberOfElements: 0,
        size: 0,
        totalElements: 0,
        totalPages: 1,
    };

    public convertListToPages(list: any[], size: number, page: number = 1): IPageable {
        this.destroy();
        this.itemCount = list.length;
        this.size = size;
        this.pages = chunk(list, size);
        return this.getPage(page);
    }

    public getPage(page: number): IPageable {
        const pageIndex: number = page - 1;
        if (this.pages.length) {
            return {
                content: this.pages[pageIndex],
                first: page === 1,
                last: page === this.pages.length,
                number: pageIndex,
                numberOfElements: this.pages[pageIndex].length,
                size: this.size,
                totalElements: this.itemCount,
                totalPages: this.pages.length,
            };
        }
        return this.emptyPage;
    }

    public formatPaginationMetadata(metadata: IPageableResponse): IPageable {
        return {
            content: [],
            first: metadata.PageNumber === 0,
            last: metadata.PageNumber === metadata.TotalPageCount - 1,
            number: metadata.PageNumber,
            numberOfElements: metadata.ItemCount,
            size: metadata.PageSize,
            totalElements: metadata.TotalItemCount,
            totalPages: metadata.TotalPageCount,
        };
    }

    private destroy(): void {
        this.pages = null;
        this.size = null;
        this.itemCount = null;
    }
}
