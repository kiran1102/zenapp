import { IProjectSettings } from "interfaces/setting.interface";
import { getProjectSettings } from "oneRedux/reducers/setting.reducer";
import { SettingActionService } from "settingsModule/services/actions/setting-action.service";
declare var OneConfirm: any;
import * as _ from "lodash";
import Utilities from "Utilities";
import { ICreateProjectNotePayload, IProject } from "interfaces/project.interface";
import { ISection } from "interfaces/section.interface";
import { IHeaderOption } from "interfaces/assessment.interface";
import { IProtocolPacket } from "interfaces/protocol.interface";
import { IGeneralModalData } from "interfaces/general-modal.interface";
import { ModalService } from "app/scripts/Shared/Services/modal.service";
import { IStore } from "interfaces/redux.interface";

class AssessmentService {
    static $inject: string[] = [
        "store",
        "$rootScope",
        "SettingStore",
        "Permissions",
        "HeatmapSettings",
        "GenFactory",
        "$timeout",
        "SettingAction",
    ];

    public ctrlInstance: any;

    constructor(
        private readonly store: IStore,
        private readonly $rootScope,
        private readonly SettingStore: any,
        private readonly Permissions: any,
        private readonly HeatmapSettings: any,
        private readonly GenFactory: any,
        private readonly $timeout: any,
        private readonly SettingAction: SettingActionService,
    ) {}

    public setCtrlInstance(scope: any): void {
        this.ctrlInstance = scope;
    }

    public getCtrlInstance(): any {
        return this.ctrlInstance;
    }

    public getApprovalLevel(): any {
        return this.SettingAction.fetchProjectSettings().then((response: boolean): undefined|void => {
            if (!response) return;

            const projectSettings: IProjectSettings = getProjectSettings(this.store.getState());

            this.ctrlInstance.riskReminderDays = projectSettings.Reminder.Value;
            this.ctrlInstance.riskMinDeadline.setDate(this.ctrlInstance.riskMinDeadline.getDate() + this.ctrlInstance.riskReminderDays + 1);
            this.ctrlInstance.projectLevelApproval = projectSettings.ProjectLevelApproval;
            this.ctrlInstance.EnableRiskAnalysis = projectSettings.EnableRiskAnalysis;
            this.ctrlInstance.DefaultReviewer = projectSettings.DefaultReviewer;
            this.ctrlInstance.allowVersioning = projectSettings.IsVersioningEnabled;
            this.ctrlInstance.selfServiceEnabled = projectSettings.SelfServiceEnabled;
        });
    }

    public getHeatmapSettings(): any {
        if (this.Permissions.canShow("SettingsRisk")) {
            return this.HeatmapSettings.getHeatmap().then((response: any): void => {
                if (response.result) this.SettingAction.addRiskSettings(response.data);
                if (response.result && response.data.Enabled) {
                    this.ctrlInstance.heatmap = response.data;
                }
            });
        }
    }

    public goToQuestion(sectionId: string, questionId: string): void {
        const sectionIndex: number = _.findIndex(this.ctrlInstance.Project.Sections, ["Id", sectionId]);

        if (sectionIndex > -1) {
            this.goToSection(sectionIndex, {});

            this.$timeout((): void => {
                const scrollTarget: HTMLElement = document.getElementById(questionId);
                const sectionContainer: HTMLElement = document.getElementById(sectionId);
                const scrollContainer: HTMLElement = sectionContainer.scrollHeight > sectionContainer.clientHeight ? sectionContainer : document.getElementById("questions-container");

                if (scrollTarget && scrollContainer) {
                    scrollContainer.scrollTop = scrollTarget.offsetTop - 100;
                }
            }, 250, true);
        }
    }

    public goToTopOfSection = (): void => {
        const el: Element | null = document.getElementById("questions-container");
        if (el) el.scrollTop = 0;
    }

    public goToBottomOfSection(): void {
        const htmlContainer: Element = document.getElementById("questions-container");
        htmlContainer.scrollTop = htmlContainer.scrollHeight;
    }

    public goToSection(index: number, section: any): void {
        const sectionCount = Object.keys(section);

        if (this.ctrlInstance.SelectedIndex !== index) {
            this.ctrlInstance.SelectedIndex = index;
        }

        if (this.ctrlInstance.SelectedIndex > this.ctrlInstance.previousIndex) {
            this.ctrlInstance.slideDirection = "right";
        }

        if (this.ctrlInstance.SelectedIndex < this.ctrlInstance.previousIndex) {
            this.ctrlInstance.slideDirection = "left";
        }

        this.ctrlInstance.previousIndex = this.ctrlInstance.SelectedIndex;

        if (sectionCount) this.setActiveSection(section);
    }

    public openSidebarSection(index: number): void {
        if (this.ctrlInstance.projectPhases && this.ctrlInstance.projectPhases.length > 1) {
            const firstLength: number = this.ctrlInstance.projectPhases[0].Sections.length;
            const secondLength: number = this.ctrlInstance.projectPhases[1].Sections.length;

            if (index < firstLength) {
                this.ctrlInstance.projectPhases[0].expanded = true;
            } else if (index < (firstLength + secondLength)) {
                this.ctrlInstance.projectPhases[1].expanded = true;
            }
        }
    }

    public setActiveSection(section: ISection): void {
        this.ctrlInstance.activeSection = section;
    }

    public setSectionsFromPhases(phases: any[]): any[] {
        const newArray: any[] = [];
        _.each(phases, (phase: any): void => {
            _.each(phase.Sections, (section: ISection): void => {
                newArray.push(section);
            });
        });
        return newArray;
    }

    public anyAdditionalSections(index: number, sections: ISection[]): boolean {
        const sectionsLen = sections.length;
        for (let i = index + 1; i < sectionsLen; i++) {
            if (sections[i].Total > 0) return true;
        }
        return false;
    }

    public getHeaderOptions(): IHeaderOption[] {
        const options: IHeaderOption[] = [];
        if (this.ctrlInstance.permissions.notesApprovalView && this.ctrlInstance.Project.StateDesc !== this.ctrlInstance.ProjectStates.InProgress) {
            options.push({ text: this.$rootScope.t("ShowHideReviewNotes"), action: this.ctrlInstance.togglePageSlide });
        }
        if (this.ctrlInstance.permissions.canDuplicateProject && !this.ctrlInstance.Project.IsLinked) {
            options.push({ text: this.$rootScope.t("DuplicateProject", { Project: this.$rootScope.customTerms.Project }), action: this.ctrlInstance.duplicateProject });
        }
        if (!this.ctrlInstance.isDataMapping && this.ctrlInstance.canCreateNewVersion()) {
            options.unshift({ text: this.$rootScope.t("CreateNewVersion"), action: this.ctrlInstance.createNewVersion });
        }
        if (!this.ctrlInstance.isDataMapping && this.ctrlInstance.permissions.projectsShare) {
            options.unshift({ text: this.$rootScope.t("ShareProject"), action: this.ctrlInstance.openShareProjectModal });
        }
        return options;
    }
}

export default AssessmentService;
