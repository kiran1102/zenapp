import invariant from "invariant";
import { includes, isNil } from "lodash";
import { isHeatMapEnabled, isRiskSummaryEnabled } from "oneRedux/reducers/setting.reducer";
import { RiskPermissions } from "constants/risk.constants";

import { RiskStates } from "enums/risk-states.enum";
import { AssessmentStates } from "constants/assessment.constants";
import { AssignmentTypes } from "constants/assignment-types.constant";

import { IUser } from "interfaces/user.interface";
import { IProject } from "interfaces/project.interface";
import { IRisk } from "interfaces/risk.interface";
import { IStore } from "interfaces/redux.interface";
import { Permissions } from "modules/shared/services/helper/permissions.service";

export default class RiskPermissionService {
    static $inject: string[] = ["store", "Permissions"];

    private riskPermissions;

    constructor(
        private store: IStore,
        private permissions: Permissions,
    ) {
        const { Ignored, Identified, Analysis, Addressed, Exception, Retained, Reduced, Archived } = RiskStates;
        const { UnderReview, InfoNeeded, RiskAssessment, RiskTreatment, Completed } = AssessmentStates;
        const { approver, riskOwner } = AssignmentTypes;

        this.riskPermissions = {
            canShowRiskPill: {
                allowedPermission:    RiskPermissions.AssesmentRiskCountView,
                allowedAssignments:   [approver, riskOwner],
                allowedProjectStates: [UnderReview, InfoNeeded, RiskAssessment, RiskTreatment, Completed],
                allowedRiskStates:    [Identified, Analysis, Addressed, Exception, Reduced, Retained, Archived],
            },
            canShowSingleRisk: {
                allowedPermission:    RiskPermissions.AssessmentRiskView,
                allowedAssignments:   [approver, riskOwner],
                allowedProjectStates: [UnderReview, InfoNeeded, RiskAssessment, RiskTreatment, Completed],
                allowedRiskStates:    [Identified, Analysis, Addressed, Exception, Reduced, Retained, Archived],
            },
            canCreateRisk: {
                allowedPermission:    RiskPermissions.AssessmentRiskCreate,
                allowedAssignments:   [approver],
                allowedProjectStates: [UnderReview, RiskAssessment, RiskTreatment],
                allowedRiskStates:    [Ignored],
                requiresLatestVersion: true,
            },
            canShowRiskLevelInModal: {
                heatmapShouldBeOn:    false,
                allowedPermission:    RiskPermissions.AssessmentRiskLevelView,
                allowedAssignments:   [approver, riskOwner],
                allowedProjectStates: [UnderReview, InfoNeeded, RiskAssessment, RiskTreatment, Completed],
                allowedRiskStates:    [Identified, Analysis, Addressed, Exception, Reduced, Retained, Archived],
            },
            canEditRiskLevelInModal: {
                heatmapShouldBeOn:    false,
                allowedPermission:    RiskPermissions.AssessmentRiskLevelEdit,
                allowedAssignments:   [approver],
                allowedProjectStates: [UnderReview, RiskAssessment, RiskTreatment],
                allowedRiskStates:    [Identified, Analysis, Addressed, Exception],
                requiresLatestVersion: true,
            },
            canEditRiskLevelInSummary: {
                heatmapShouldBeOn:    false,
                allowedPermission:    RiskPermissions.AssessmentRiskLevelEdit,
                allowedAssignments:   [approver],
                allowedProjectStates: [UnderReview, RiskAssessment, RiskTreatment],
                allowedRiskStates:    [Identified, Analysis, Addressed, Exception],
                requiresLatestVersion: true,
            },
            canShowRiskHeatmapInModal: {
                heatmapShouldBeOn:    true,
                allowedPermission:    RiskPermissions.AssessmentRiskLevelView,
                allowedAssignments:   [approver, riskOwner],
                allowedProjectStates: [UnderReview, InfoNeeded, RiskAssessment, RiskTreatment, Completed],
                allowedRiskStates:    [Identified, Analysis, Addressed, Exception, Reduced, Retained, Archived],
            },
            canEditRiskHeatmapInModal: {
                heatmapShouldBeOn:    true,
                allowedPermission:    RiskPermissions.AssessmentRiskLevelEdit,
                allowedAssignments:   [approver],
                allowedProjectStates: [UnderReview, RiskAssessment, RiskTreatment],
                allowedRiskStates:    [Identified, Analysis, Addressed, Exception],
                requiresLatestVersion: true,
            },
            canEditRiskHeatmapInSummary: {
                heatmapShouldBeOn:    true,
                allowedPermission:    RiskPermissions.AssessmentRiskLevelEdit,
                allowedAssignments:   [approver],
                allowedProjectStates: [UnderReview, RiskAssessment, RiskTreatment],
                allowedRiskStates:    [Identified, Analysis, Addressed, Exception],
                requiresLatestVersion: true,
            },
            canDeleteRiskInModal: {
                allowedPermission:    RiskPermissions.AssessmentRiskDelete,
                allowedAssignments:   [approver],
                allowedProjectStates: [UnderReview, RiskAssessment, RiskTreatment, Completed],
                allowedRiskStates:    [Identified, Analysis, Addressed, Exception],
                requiresLatestVersion: true,
            },
            canDeleteRiskInSummary: {
                allowedPermission:    RiskPermissions.AssessmentRiskDelete,
                allowedAssignments:   [approver],
                allowedProjectStates: [UnderReview, RiskAssessment, RiskTreatment, Completed],
                allowedRiskStates:    [Identified, Analysis, Addressed, Exception],
                requiresLatestVersion: true,
            },
            canShowRiskDeadlineInModal: {
                allowedPermission:    RiskPermissions.AssessmentRiskDeadlineView,
                allowedAssignments:   [approver, riskOwner],
                allowedProjectStates: [UnderReview, InfoNeeded, RiskAssessment, RiskTreatment, Completed],
                allowedRiskStates:    [Identified, Analysis, Addressed, Exception, Reduced, Retained, Archived],
            },
            canShowRiskDeadlineInSummary: {
                allowedPermission:    RiskPermissions.AssessmentRiskDeadlineView,
                allowedAssignments:   [approver, riskOwner],
                allowedProjectStates: [UnderReview, InfoNeeded, RiskAssessment, RiskTreatment, Completed],
                allowedRiskStates:    [Identified, Analysis, Addressed, Exception, Reduced, Retained, Archived],
            },
            canEditRiskDeadlineInModal: {
                allowedPermission:    RiskPermissions.AssessmentRiskDeadlineEdit,
                allowedAssignments:   [approver],
                allowedProjectStates: [UnderReview, RiskAssessment, RiskTreatment],
                allowedRiskStates:    [Identified, Analysis, Addressed, Exception],
                requiresLatestVersion: true,
            },
            canShowRiskOwnerInModal: {
                allowedPermission:    RiskPermissions.AssessmentRiskOwnerView,
                allowedAssignments:   [approver, riskOwner],
                allowedProjectStates: [UnderReview, InfoNeeded, RiskAssessment, RiskTreatment, Completed],
                allowedRiskStates:    [Identified, Analysis, Addressed, Exception, Reduced, Retained, Archived],
            },
            canEditRiskOwnerInModal: {
                allowedPermission:    RiskPermissions.AssessmentRiskOwnerEdit,
                allowedAssignments:   [approver, riskOwner],
                allowedProjectStates: [UnderReview, RiskAssessment, RiskTreatment],
                allowedRiskStates:    [Identified, Analysis, Addressed, Exception],
                requiresLatestVersion: true,
            },
            canShowRiskSummaryInModal: {
                allowedPermission:    RiskPermissions.AssessmentRiskSummaryView,
                allowedAssignments:   [approver, riskOwner],
                allowedProjectStates: [UnderReview, InfoNeeded, RiskAssessment, RiskTreatment, Completed],
                allowedRiskStates:    [Identified, Analysis, Addressed, Exception, Reduced, Retained, Archived],
            },
            canEditSummaryInModal: {
                allowedPermission:    RiskPermissions.AssessmentRiskSummaryEdit,
                allowedAssignments:   [approver],
                allowedProjectStates: [UnderReview, RiskAssessment, RiskTreatment],
                allowedRiskStates:    [Identified, Analysis, Addressed, Exception],
                requiresLatestVersion: true,
            },
            canShowRiskRecommendationInModal: {
                allowedPermission:    RiskPermissions.AssessmentRiskRecommendationView,
                allowedAssignments:   [approver, riskOwner],
                allowedProjectStates: [UnderReview, InfoNeeded, RiskAssessment, RiskTreatment, Completed],
                allowedRiskStates:    [Identified, Analysis, Addressed, Exception, Reduced, Retained, Archived],
            },
            canEditRiskRecommendationInModal: {
                allowedPermission:    RiskPermissions.AssessmentRiskRecommendationEdit,
                allowedAssignments:   [approver],
                allowedProjectStates: [UnderReview, RiskAssessment, RiskTreatment],
                allowedRiskStates:    [Identified, Analysis],
                requiresLatestVersion: true,
            },
            canAddRiskRecommendationInSummary: {
                allowedPermission:    RiskPermissions.AssessmentRiskRecommendationEdit,
                allowedAssignments:   [approver],
                allowedProjectStates: [UnderReview, RiskAssessment, RiskTreatment],
                allowedRiskStates:    [Identified],
                requiresLatestVersion: true,
            },
            canEditRiskRecommendationInSummary: {
                allowedPermission:    RiskPermissions.AssessmentRiskRecommendationEdit,
                allowedAssignments:   [approver],
                allowedProjectStates: [UnderReview, RiskAssessment],
                allowedRiskStates:    [Analysis],
                requiresLatestVersion: true,
            },
            canDeleteRiskRecommendationInSummary: {
                allowedPermission:    RiskPermissions.AssessmentRiskRecommendationEdit,
                allowedAssignments:   [approver],
                allowedProjectStates: [UnderReview, RiskAssessment, RiskTreatment],
                allowedRiskStates:    [Analysis],
                requiresLatestVersion: true,
            },
            canShowRiskRemediationInModal: {
                allowedPermission:    RiskPermissions.AssessmentRiskRemediationProposalView,
                allowedAssignments:   [approver, riskOwner],
                allowedProjectStates: [UnderReview, InfoNeeded, RiskAssessment, RiskTreatment, Completed],
                allowedRiskStates:    [Addressed],
            },
            canEditRiskRemediationInModal: {
                riskSummaryShouldBeOn: true,
                allowedPermission:     RiskPermissions.AssessmentRiskRemediationProposalEdit,
                allowedAssignments:    [approver, riskOwner],
                allowedProjectStates:  [RiskTreatment],
                allowedRiskStates:     [Addressed],
                requiresLatestVersion: true,
            },
            canClickAddRiskRemediationInSummary: {
                riskSummaryShouldBeOn: true,
                allowedPermission:     RiskPermissions.AssessmentRiskProposeRemediation,
                allowedAssignments:    [approver, riskOwner],
                allowedProjectStates:  [RiskTreatment],
                allowedRiskStates:     [Analysis],
                requiresLatestVersion: true,
            },
            canEditRiskRemediationInSummary: {
                riskSummaryShouldBeOn: true,
                allowedPermission:     RiskPermissions.AssessmentRiskRemediationProposalEdit,
                allowedAssignments:    [riskOwner],
                disallowedAssignments: [approver],
                allowedProjectStates:  [RiskTreatment],
                allowedRiskStates:     [Addressed, Exception],
                requiresLatestVersion: true,
            },
            canDeleteRiskRemediationInSummary: {
                riskSummaryShouldBeOn: true,
                allowedPermission:     RiskPermissions.AssessmentRiskRemoveRemediationProposal,
                allowedAssignments:    [approver, riskOwner],
                allowedProjectStates:  [RiskTreatment],
                allowedRiskStates:     [Addressed],
                requiresLatestVersion: true,
            },
            canShowRiskExceptionInModal: {
                allowedPermission:    RiskPermissions.AssessmentRiskExceptionRequestView,
                allowedAssignments:   [approver, riskOwner],
                allowedProjectStates: [UnderReview, InfoNeeded, RiskAssessment, RiskTreatment, Completed],
                allowedRiskStates:    [Exception],
            },
            canEditRiskExceptionInModal: {
                riskSummaryShouldBeOn: true,
                allowedPermission:     RiskPermissions.AssessmentRiskExceptionRequestEdit,
                allowedAssignments:    [approver, riskOwner],
                allowedProjectStates:  [RiskTreatment],
                allowedRiskStates:     [Exception],
                requiresLatestVersion: true,
            },
            canClickAddRiskExceptionInSummary: {
                riskSummaryShouldBeOn: true,
                allowedPermission:     RiskPermissions.AssessmentRiskRequestException,
                allowedAssignments:    [approver, riskOwner],
                allowedProjectStates:  [RiskTreatment],
                allowedRiskStates:     [Analysis],
                requiresLatestVersion: true,
            },
            canEditRiskExceptionInSummary: {
                riskSummaryShouldBeOn: true,
                allowedPermission:     RiskPermissions.AssessmentRiskExceptionRequestEdit,
                allowedAssignments:    [riskOwner],
                disallowedAssignments: [approver],
                allowedProjectStates:  [RiskTreatment],
                allowedRiskStates:     [Addressed, Exception],
                requiresLatestVersion: true,
            },
            canDeleteRiskExceptionInSummary: {
                riskSummaryShouldBeOn: true,
                allowedPermission:     RiskPermissions.AssessmentRiskRemoveExceptionRequest,
                allowedAssignments:    [approver, riskOwner],
                allowedProjectStates:  [RiskTreatment],
                allowedRiskStates:     [Exception],
                requiresLatestVersion: true,
            },
            canApproveRemediationInSummary: {
                riskSummaryShouldBeOn: true,
                allowedPermission:     RiskPermissions.AssessmentRiskApproveRemediation,
                allowedAssignments:    [approver],
                allowedProjectStates:  [RiskTreatment],
                allowedRiskStates:     [Addressed],
                requiresLatestVersion: true,
            },
            canRejectRemediationInSummary: {
                riskSummaryShouldBeOn: true,
                allowedPermission:     RiskPermissions.AssessmentRiskRejectRemediation,
                allowedAssignments:    [approver],
                allowedProjectStates:  [RiskTreatment],
                allowedRiskStates:     [Addressed],
                requiresLatestVersion: true,
            },
            canApproveExceptionInSummary: {
                riskSummaryShouldBeOn: true,
                allowedPermission:     RiskPermissions.AssessmentRiskGrantException,
                allowedAssignments:    [approver],
                allowedProjectStates:  [RiskTreatment],
                allowedRiskStates:     [Exception],
                requiresLatestVersion: true,
            },
            canRejectExceptionInSummary: {
                riskSummaryShouldBeOn: true,
                allowedPermission:     RiskPermissions.AssessmentRiskRejectException,
                allowedAssignments:    [approver],
                allowedProjectStates:  [RiskTreatment],
                allowedRiskStates:     [Exception],
                requiresLatestVersion: true,
            },
        };
    }

    public hasRiskPermission(permissionToCheck: string, currentUser: IUser, projectModel: IProject, riskModel: IRisk): boolean {
        const config = this.riskPermissions[permissionToCheck];
        invariant(Boolean(config), `Permission: ${permissionToCheck || "undefined"}, is not available.`);

        return (this.hasPermission(config.allowedPermission)
            || this.isAllowedUser(config.allowedAssignments, currentUser, projectModel, riskModel))
            && this.isNotDisallowedUser(config.disallowedAssignments, currentUser, projectModel, riskModel)
            && includes(config.allowedProjectStates, projectModel.StateDesc)
            && this.isAllowedRiskState(config.allowedRiskStates, riskModel)
            && this.isHeatmapSettingMatching(config.heatmapShouldBeOn, this.isHeatMapEnabled())
            && this.isRiskSummarySettingMatching(config.riskSummaryShouldBeOn, this.isRiskSummaryEnabled())
            && this.isLatestVersionRequired(config.requiresLatestVersion, projectModel);
    }

    private isHeatMapEnabled(): boolean {
        return isHeatMapEnabled(this.store.getState());
    }

    private isHeatmapSettingMatching(permissionToCompare: boolean | undefined, currentSetting: boolean): boolean {
        if (permissionToCompare === undefined) {
            return true;
        }
        return permissionToCompare === currentSetting;
    }

    private isAllowedRiskState(permissionArray: number[], riskModel: IRisk | undefined): boolean {
        if (isNil(riskModel) && includes(permissionArray, RiskStates.Ignored)) {
            return true;
        }
        return !isNil(riskModel) && includes(permissionArray, riskModel.State);
    }

    private isRiskSummaryEnabled(): boolean {
        return isRiskSummaryEnabled(this.store.getState());
    }

    private isRiskSummarySettingMatching(permissionToCompare: boolean | undefined, currentSetting: boolean): boolean {
        if (permissionToCompare === undefined) {
            return true;
        }
        return permissionToCompare === currentSetting;
    }

    private hasPermission(permissionToCheck: string): boolean {
        return this.permissions.canShow(permissionToCheck);
    }

    private isAllowedUser(permissionArray: string[], user: IUser, projectModel: IProject, riskModel: IRisk): boolean {
        let isApprover: boolean;
        let isRiskOwner: boolean;
        for (const permission of permissionArray) {
            if (permission === AssignmentTypes.approver) {
                isApprover = projectModel.ReviewerId === user.Id;
            } else if (permission === AssignmentTypes.riskOwner && riskModel) {
                isRiskOwner = riskModel.RiskOwnerId === user.Id;
            }
        }
        return isApprover || isRiskOwner;
    }

    private isNotDisallowedUser(permissionArray: string[], user: IUser, projectModel: IProject, riskModel: IRisk): boolean {
        if (!permissionArray) return true;

        let isApprover: boolean;
        let isRiskOwner: boolean;
        for (const permission of permissionArray) {
            if (permission === AssignmentTypes.approver) {
                isApprover = projectModel.ReviewerId !== user.Id;
            } else if (permission === AssignmentTypes.riskOwner) {
                isRiskOwner = riskModel.RiskOwnerId !== user.Id;
            }
        }
        return isApprover || isRiskOwner;
    }

    private isLatestVersionRequired(requiresLatestVersion: boolean, projectModel: IProject): boolean {
        return !requiresLatestVersion || projectModel.IsLatestVersion;
    }

}
