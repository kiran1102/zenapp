import * as _ from "lodash";
import OrgGroupStoreNew from "oneServices/org-group.store";

// TODO deprecate this entire class and subscribe to store: IStore instead
export default class OrgGroupStoreOld {

    static $inject: string[] = ["OrgGroups", "$q", "OrgGroupStoreNew"];

    constructor(readonly OrgGroups, readonly $q: ng.IQService, readonly OrgGroupStoreNew: OrgGroupStoreNew) {}

    public getOrgTree(force: boolean = false, leavesOnly: boolean = false): Promise<any> {
        if (force) return this.getOrgTreeAndSet();
        else if (leavesOnly) return this.getOrgTreeLeavesAndSet();
        const deferred: any = this.$q.defer();
        deferred.resolve(this.OrgGroupStoreNew.orgList);
        return deferred.promise;
    }

    public setOrgTree(treeMod?: any): Promise<any> {
        if (treeMod) {
            const deferred: any = this.$q.defer();
            deferred.resolve();
            return deferred.promise;
        }
        return this.getOrgTreeAndSet();
    }

    public addToTree(org: any, parentId?: string): Promise<any> {
        return this.getOrgAndCB(org, parentId, this.addTreeCB(org));
    }

    public updateInTree(org: any, id?: string): Promise<any> {
        return this.getOrgAndCB(org, id, this.updateTreeCB(org));
    }

    public removeFromTree(id: string) {
        return this.getOrgTree().then((tree: any[]): any[] => {
            tree = this.recursivelyRemoveFromTree(tree, id);
            return tree;
        });
    }

    private getOrgTreeAndSet(): any {
        return this.OrgGroups.tree().then((res: any): undefined | any => {
            if (!res.data) return;
            return res.data;
        });
    }

    private getOrgTreeLeavesAndSet(): any {
        // TODO: Flush this out to refresh all leaf nodes and set them in the store.
        return this.getOrgTreeAndSet();
    }

    private getOrgAndCB(org: any, id: string, cb: any): Promise<any> {
        return this.getOrgTree().then((tree: any[]): void => {
            if (org && tree && tree[0] && id) {
                tree[0] = this.findTreeRecursivelyAndCB(tree[0], id, cb);
            }
        });
    }

    private findTreeRecursivelyAndCB(treeItem: any, id: string, cb: any): undefined | any {
        if (!treeItem || !id) return treeItem;
        else if (treeItem.Id === id) {
            treeItem = cb(treeItem);
        } else if (_.isArray(treeItem.Children) && treeItem.Children.length) {
            const childrenCount: number = treeItem.Children.length;
            for (let i = 0; i < childrenCount; i++) {
                treeItem.Children[i] = this.findTreeRecursivelyAndCB(treeItem.Children[i], id, cb);
            }
        }
        return treeItem;
    }

    private addTreeCB(org: any): any {
        return (treeItem: any): any => {
            treeItem.Children.push(org);
            return treeItem;
        };
    }

    private updateTreeCB(org: any): any {
        return (treeItem: any): any => {
            treeItem = _.merge(treeItem, org);
            return treeItem;
        };
    }

    private recursivelyRemoveFromTree(tree: any[], id: string): any {
        if (tree) {
            const treeLength: number = tree.length;
            for (let i = 0; i < treeLength; i++) {
                if (tree[i].Id === id) {
                    tree.splice(i, 1);
                    break;
                } else {
                    tree[i].Children = this.recursivelyRemoveFromTree(tree[i].Children, id);
                }
            }
        }
        return tree;
    }

}
