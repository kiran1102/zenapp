declare var angular: any;
import * as _ from "lodash";
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import { NotificationService } from "modules/shared/services/provider/notification.service";

export default class OrgApi {

    static $inject: string[] = ["$rootScope", "OneProtocol", "NotificationService"];

    private translate: any;
    private apiCache: any;

    constructor(
        private $rootScope: IExtendedRootScopeService,
        private readonly OneProtocol: any,
        private ENUMS: any,
        private notificationService: NotificationService,
    ) {
        this.translate = $rootScope.t;
        this.apiCache = $rootScope.APICache;
    }

    private rootTree = (): ng.IPromise<any | boolean> => {
        const config: any = this.OneProtocol.config("GET", `/orggroup/roottree/`);
        const messages: any = { Error: { Hide: true } };
        return this.OneProtocol.http(config, messages).then((response: any): any | boolean => {
            if (response.result) {
                return response.data[0];
            }
            this.notificationService.alertWarning(this.translate("Warning"), this.translate("ErrorRetrievingOrganizationGroups"));
            return false;
        });
    }
}
