import { isArray, map, filter, flatMapDeep, each } from "lodash";
import { ProtocolService } from "modules/core/services/protocol.service";
import { NotificationService } from "modules/shared/services/provider/notification.service";

import { getCurrentUser } from "oneRedux/reducers/user.reducer";

import { OrgUserTraversal } from "enums/org-user-dropdown.enum";
import { UserRoles } from "constants/user-roles.constant";

import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import { IProtocolPacket, IProtocolConfig, IProtocolMessages, IProtocolResponse } from "interfaces/protocol.interface";
import { IOrganization } from "interfaces/org.interface";
import { IOrgUser, IOrgUserAdapted, IUserDropdownValidation } from "interfaces/org-user.interface";
import { IUser } from "interfaces/user.interface";
import { IStore } from "interfaces/redux.interface";
import { IStringMap } from "interfaces/generic.interface";

export class OrgGroupApiService {

    static $inject: string[] = [
        "store",
        "$rootScope",
        "OneProtocol",
        "NotificationService",
    ];

    constructor(
        private store: IStore,
        private $rootScope: IExtendedRootScopeService,
        private readonly OneProtocol: ProtocolService,
        private notificationService: NotificationService,
    ) { }

    public list(): ng.IPromise<IProtocolResponse<IOrganization[]>> {
        const config: IProtocolConfig = this.OneProtocol.config("GET", `/orggroup/get`);
        const messages: IProtocolMessages = { Error: { custom: this.$rootScope.t("ErrorRetrievingOrganizationGroups") } };
        return this.OneProtocol.http(config, messages);
    }

    public tree = (id: string = ""): ng.IPromise<IProtocolResponse<IOrganization[]>> => {
        const config: IProtocolConfig = this.OneProtocol.config("GET", `/orggroup/tree/${id}`);
        const messages: IProtocolMessages = { Error: { custom: this.$rootScope.t("ErrorRetrievingOrganizationGroups") } };
        return this.OneProtocol.http(config, messages);
    }

    public rootTree(): ng.IPromise<IProtocolResponse<IOrganization[]>> {
        const config: IProtocolConfig = this.OneProtocol.config("GET", `/orggroup/roottree/`);
        const messages: IProtocolMessages = { Error: { custom: this.$rootScope.t("ErrorRetrievingOrganizationGroups") } };
        return this.OneProtocol.http(config, messages).then((response: IProtocolResponse<IOrganization>): ng.IPromise<IProtocolResponse<IOrganization[]>>  => {
            if (response.result) {
                return response.data[0];
            }
            this.notificationService.alertWarning(this.$rootScope.t("Warning"), this.$rootScope.t("ErrorRetrievingOrganizationGroups"));
        });
    }

    public read(id: string): ng.IPromise<IProtocolResponse<IOrganization>> {
        const config: IProtocolConfig = this.OneProtocol.config("GET", `/orggroup/get/${id}`);
        const messages: IProtocolMessages = { Error: { custom: this.$rootScope.t("ErrorRetrievingOrganizationGroup") } };
        return this.OneProtocol.http(config, messages);
    }

    public create(group: any): ng.IPromise<IProtocolPacket> {
        const config: IProtocolConfig = this.OneProtocol.config("POST", `/orggroup/post`, [], group);
        const messages: IProtocolMessages = {
            Error: { custom: this.$rootScope.t("ErrorCreatingOrganizationGroup") },
            Success: { custom: this.$rootScope.t("OrganizationGroupCreated") },
        };
        return this.OneProtocol.http(config, messages);
    }

    public update(group: any): ng.IPromise<IProtocolPacket> {
        const config: IProtocolConfig = this.OneProtocol.config("PUT", `/orggroup/put`, [], group);
        const messages: IProtocolMessages = {
            Success: { custom : this.$rootScope.t("OrganizationGroupUpdated") },
            Error: { custom: this.$rootScope.t("ErrorUpdatingOrganizationGroup") },
        };
        return this.OneProtocol.http(config, messages);
    }

    public reorder(data: IStringMap<string>): ng.IPromise<IProtocolPacket> {
        const config: IProtocolConfig = this.OneProtocol.config("PUT", `/orggroup/move`, [], data);
        const messages: IProtocolMessages = { Error: { custom: this.$rootScope.t("ErrorReorderingOrganizationGroup") } };
        return this.OneProtocol.http(config, messages);
    }

    public delete(id: string): ng.IPromise<IProtocolPacket> {
        const config: IProtocolConfig = this.OneProtocol.config("DELETE", `/orggroup/delete/${id}`);
        const messages: IProtocolMessages = {
            Error: { custom: this.$rootScope.t("ErrorDeletingOrganizationGroup") },
            Success: { custom: this.$rootScope.t("OrganizationGroupDeleted") },
        };
        return this.OneProtocol.http(config, messages);
    }

    public formatGroupLabel(groups: IOrganization[], model: string): undefined | string {
        if (!model) return;
        else if (isArray(groups)) {
            for (const group of groups) {
                if (model === group.Id) {
                    return group.Name || this.$rootScope.t("GroupNameNotFound");
                }
            }
        }
        return "";
    }

    public getOrgUsersWithPermission(organizationId?: string, traversal?: OrgUserTraversal, filterCurrentUser: boolean = false, permissionKeys: string[] = [], excludedPermissionKeys: string[] = []): ng.IPromise<IProtocolResponse<IOrgUserAdapted[]>> {
        const permissions: string = permissionKeys.join(",");
        const excludedPermissions: string = excludedPermissionKeys.join(",");
        const config: IProtocolConfig = this.OneProtocol.config("GET", `/orggroupuser/DropDownUsers`, { organizationId, traversal, permissions, excludedPermissions });
        const messages: IProtocolMessages = { Error: { custom: this.$rootScope.t("ErrorRetrievingOrganizationGroupsWithPermission") } };
        return this.OneProtocol.http(config, messages).then((res: IProtocolResponse<IOrgUser[]>): IProtocolResponse<IOrgUserAdapted[]> => {
            // For filtering Invited Users... include the "Login" permissionKey in the permissionKeys array
            const currentUserId: string = getCurrentUser(this.store.getState()).Id;
            const data: IOrgUser[] = filterCurrentUser ? filter(res.data, ((user) => user.id !== currentUserId)) : res.data;
            return {
                ...res,
                data: this.orgUsersAdapter(data),
            };
        });
    }

    public orgUsersAdapter(data: IOrgUser[]): IOrgUserAdapted[] {
        return map(data, (obj: IOrgUser): IOrgUserAdapted => {
            return {
                Id: obj.id,
                FullName: obj.fullName,
            };
        });
    }
}
