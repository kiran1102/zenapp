import { map } from "lodash";

// Rxjs
import {
    from,
    Observable,
} from "rxjs";

import { ProtocolService } from "modules/core/services/protocol.service";

import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import {
    IProtocolPacket,
    IProtocolConfig,
    IProtocolMessages,
    IProtocolResponse,
} from "interfaces/protocol.interface";
import {
    IUserManageContract,
    IUserActivateContract,
    IUser,
    IUserType,
    IUserDetail,
} from "interfaces/user.interface";
import { IStringMap } from "interfaces/generic.interface";
import { IPageableOf } from "interfaces/pagination.interface";
import { IUserPaginationParams } from "interfaces/user.interface";

export class UserApiService {
    static $inject: string[] = ["$rootScope", "OneProtocol", "Export"];
    constructor(
        private $rootScope: IExtendedRootScopeService,
        private OneProtocol: ProtocolService,
    ) { }

    getUsersDetails(payload: string[]): Observable<IProtocolResponse<IUserDetail[]>> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("POST", `/access/v1/tenants/current/users`, null, payload);
        return from(this.OneProtocol.http(config));
    }

    public getUsers(): ng.IPromise<IProtocolResponse<IUser[]>> {
        const config: IProtocolConfig = this.OneProtocol.config("GET", `/orggroupuser/get`);
        const messages: IProtocolMessages = { Error: { custom: this.$rootScope.t("ErrorRetreivingUsers") } };
        return this.OneProtocol.http(config, messages);
    }

    public getUsersByPage(params: IUserPaginationParams): ng.IPromise<IProtocolResponse<IPageableOf<IUser>>> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("GET", `/api/access/v1/orgGroupUsers/details` , this.serialize(params));
        const messages: IProtocolMessages = { Error: { custom: this.$rootScope.t("ErrorRetreivingUsers") } };
        return this.OneProtocol.http(config, messages).then(
            (response: IProtocolResponse<IPageableOf<IUser>> ) => {
                if (response.result) {
                    return {
                        ...response,
                        data: this.modifyData(response.data),
                    };
                } else {
                    return null;
                }
            },
        );
    }

    public modifyData(pageable: IPageableOf<IUser>): IPageableOf<IUser> {
        const roleMap = this.translateSeededRoles();
        return {
            ...pageable,
            content: map(pageable.content, (item) => {
                item["RoleName"] = item["RoleName"] in roleMap ? roleMap[item["RoleName"]] : item["RoleName"];
                return item;
            }),
        };
    }

    public translateSeededRoles(): IStringMap<string> {
        return {
            "Site Admin": this.$rootScope.t("SiteAdmin"),
            "Privacy Officer": this.$rootScope.t("PrivacyOfficer"),
            "Project Owner": this.$rootScope.t("ProjectOwner"),
            "Project Viewer": this.$rootScope.t("ProjectViewer"),
            "Invited User": this.$rootScope.t("InvitedUser"),
        };
    }

    public getUserById = (id: string, details: boolean = false): ng.IPromise<IProtocolResponse<IUser>> => {
        const config: IProtocolConfig = this.OneProtocol.config("GET", `/orggroupuser/${details ? "getdetails" : "get"}/${id}`);
        const messages: IProtocolMessages = { Error: { custom: this.$rootScope.t("ErrorRetreivingUser") } };
        return this.OneProtocol.http(config, messages);
    }

    public createUser = (user: IUserManageContract): ng.IPromise<IProtocolPacket> => {
        const config: IProtocolConfig = this.OneProtocol.config("POST", `/orggroupuser/post`, [], user);
        const messages: IProtocolMessages = { Success: { custom: this.$rootScope.t("UserCreated") }, Error: { custom: this.$rootScope.t("ErrorCreatingUser") } };
        return this.OneProtocol.http(config, messages);
    }

    public updateUser = (user: IUserManageContract): ng.IPromise<IProtocolPacket> => {
        const config: IProtocolConfig = this.OneProtocol.config("PUT", `/orggroupuser/put`, [], user);
        const messages: IProtocolMessages = { Success: { custom: this.$rootScope.t("UserUpdated") }, Error: { custom: this.$rootScope.t("ErrorUpdatingUser") } };
        return this.OneProtocol.http(config, messages);
    }

    public getCurrentUser = (): ng.IPromise<IProtocolResponse<IUser>> => {
        const config: IProtocolConfig = this.OneProtocol.config("GET", `/user/info`);
        const messages: IProtocolMessages = { Error: { Hide: true } };
        return this.OneProtocol.http(config, messages);
    }

    public getBasicUserInfoList = (usersIds: string[]): ng.IPromise<IProtocolPacket> => {
        const config: IProtocolConfig = this.OneProtocol.config("POST", `/user/basicinfo`, null, usersIds);
        const messages: IProtocolMessages = { Error: { custom: this.$rootScope.t("ErrorRetrievingUsers") } };
        return this.OneProtocol.http(config, messages);
    }

    public deactivateUser = (id: string): ng.IPromise<IProtocolPacket> => {
        const config: IProtocolConfig = this.OneProtocol.config("PUT", `/user/deactivate/${id}`);
        const messages: IProtocolMessages = { Success: { custom: this.$rootScope.t("UserDisabled") }, Error: { custom: this.$rootScope.t("ErrorDisablingUser") } };
        return this.OneProtocol.http(config, messages);
    }

    public activateUser = (activateRequest: IUserActivateContract): ng.IPromise<IProtocolPacket> => {
        if (activateRequest.ExpirationDT) activateRequest.ExpirationDT.setHours(23, 55);
        const config: IProtocolConfig = this.OneProtocol.config("PUT", `/user/activate`, null, activateRequest);
        const messages: IProtocolMessages = { Success: { custom: this.$rootScope.t("UserEnabled") }, Error: { custom: this.$rootScope.t("ErrorEnablingUser") } };
        return this.OneProtocol.http(config, messages);
    }

    public deleteUser = (id: string): ng.IPromise<IProtocolPacket> => {
        const config: IProtocolConfig = this.OneProtocol.config("DELETE", `/user/delete/${id}`);
        const messages: IProtocolMessages = { Success: { custom: this.$rootScope.t("UserDelete") }, Error: { custom: this.$rootScope.t("ErrorDeletingUser") } };
        return this.OneProtocol.http(config, messages);
    }

    public resendRegistrationEmail = (id: string): ng.IPromise<IProtocolPacket> => {
        const config: IProtocolConfig = this.OneProtocol.config("GET", `/user/resendregistrationemail/${id}`);
        const messages: IProtocolMessages = { Success: { custom: this.$rootScope.t("EmailSent") }, Error: { custom: this.$rootScope.t("ErrorResendingRegistrationEmail") } };
        return this.OneProtocol.http(config, messages);
    }

    public onboardUser = (): ng.IPromise<IProtocolPacket> => {
        const config: IProtocolConfig = this.OneProtocol.config("POST", `/user/onboard`);
        const messages: IProtocolMessages = { Error: { Hide: true } };
        // Below error message was unused, migrated just in case.
        // const messages: IProtocolMessages = { Error: { custom: this.$rootScope.t("CouldNotOnboardUser") } };
        return this.OneProtocol.http(config, messages);
    }

    public getUserType = (userTypeRequest: IUserType): ng.IPromise<IProtocolPacket> => {
        const config: IProtocolConfig = this.OneProtocol.customConfig("POST", `/auth/UserType`, null, userTypeRequest);
        const messages: IProtocolMessages = { Error: { Hide: true } };
        return this.OneProtocol.http(config, messages);
    }

    private serialize(params: IUserPaginationParams) {
        if (!params.seedFilters) return params;
        const paramQueryEmailId = params.seedFilters.search ? `email=~=${params.seedFilters.search},` : "";
        const paramQueryFullName = params.seedFilters.search ? `fullname=~=${params.seedFilters.search};` : "";
        const paramQueryEnabled = `(enabled==${params.seedFilters.enabled});`;
        const paramQueryInternalExternal =
            !params.seedFilters.internal && !params.seedFilters.external ? "" :
            params.seedFilters.internal && params.seedFilters.external ? "" :
            params.seedFilters.internal && !params.seedFilters.external ? "internal==true;" : "internal==false;";
        const beginDate = params.seedFilters.beginDate;
        const endDate = params.seedFilters.endDate;
        const expirationDate = !params.seedFilters.beginDate || !params.seedFilters.beginDate ? "" : `expirationDate=lt=${endDate};expirationDate=gt=${beginDate}`;
        if (!params.tenantFilters) return params;
        const paramQueryOrgName = !params.tenantFilters.organizationId ? "" : `OrganizationId==${params.tenantFilters.organizationId};`;
        const paramQueryRoleNames = !params.tenantFilters.roleId.length ? "" : `roleId=in=[${params.tenantFilters.roleId}];`;
        return {
            ...params,
            seedFilters: `${paramQueryEmailId}${paramQueryFullName}${paramQueryEnabled}${paramQueryInternalExternal}${expirationDate}`,
            tenantFilters: `${paramQueryOrgName}${paramQueryRoleNames}`,
        };
    }
}
