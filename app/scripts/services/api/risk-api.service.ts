// Angular
import {
    Injectable,
    Inject,
} from "@angular/core";

// Rxjs
import {
    Observable,
    Subject,
    from,
} from "rxjs";
import {
    shareReplay,
    map as rxMap,
} from "rxjs/operators";

// 3rd party
import {
    assign,
    forIn,
    forEach,
    map,
    sortBy,
} from "lodash";

// Services
import { ProtocolService } from "modules/core/services/protocol.service";
import { PaginationService } from "oneServices/pagination.service";
import { RiskAdapterService } from "oneServices/adapters/risk-adapter.service";

// Interfaces
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import { IGridPaginationParams } from "interfaces/grid.interface";
import {
    IRisk,
    IFetchingRisksResponse,
    IRiskSerialized,
    IRiskFilter,
    IRiskDetails,
    IRiskSerializedV2,
    IAssessmentRiskUnlinkRequest,
    IRiskCategoryDetail,
    IRiskCategoryBasicInfo,
    IRiskCreateRequest,
    IRiskUpdateRequest,
} from "interfaces/risk.interface";
import {
    IProtocolPacket,
    IProtocolConfig,
    IProtocolMessages,
    IProtocolResponse,
} from "interfaces/protocol.interface";
import {
    IPageable,
    IPaginationParams,
    IPaginationFilter,
    IPageableOf,
} from "interfaces/pagination.interface";
import { IStringMap } from "interfaces/generic.interface";
import { RiskReferenceTypes } from "enums/risk.enum";
import {
    IRiskSetting,
    IRiskLevel,
} from "interfaces/risks-settings.interface";
import {
    IIndividualRiskSetting,
    IRiskHeatmapSettings,
    ISettingsRiskImpact,
    ISettingsRiskProbability,
} from "modules/settings/settings-risks/shared/settings-risks.interface";
import {
    IInventoryControlsTableRow,
    IInventoryRiskControlsFilter,
} from "interfaces/inventory.interface";
import { IRiskNewCategoryModel } from "modules/risks/risk-category/risk-category-add-modal/risk-category-add-modal.interface";
import { IRiskCategoryFilter } from "modules/risks/risk-category/risk-category.interface";

// enums/constants
import { RiskV2Levels } from "enums/riskV2.enum";
import { APIResponseTypes } from "constants/api-response-types.constant";
import { RiskCategoryStatus } from "modules/risks/risk-category/risk-category.constants";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

@Injectable({
    providedIn: "root",
})
export class RiskAPIService {

    private heatmapSettings = new Subject<IRiskHeatmapSettings>();
    private heatmapSettings$: Observable<IRiskHeatmapSettings>;
    private riskSettings = new Subject<IRiskSetting>();
    private riskSettings$: Observable<IRiskSetting>;
    private individualRiskSettings = new Subject<IIndividualRiskSetting>();
    private individualRiskSettings$: Observable<IIndividualRiskSetting>;
    private individualRiskScore = new Subject<IRiskLevel[]>();
    private individualRiskScore$: Observable<IRiskLevel[]>;
    private riskLevels$: Observable<IRiskLevel[]>;
    private riskLevels = new Subject<IRiskLevel[]>();

    constructor(
        @Inject("$rootScope") private rootScope: IExtendedRootScopeService,
        private oneProtocol: ProtocolService,
        private riskAdapter: RiskAdapterService,
        private pagination: PaginationService,
        private translatePipe: TranslatePipe,
    ) {
        this.clearRisksCache();
    }

    public getRiskLevel(forceLoad = false): Observable<IRiskLevel[]> {

        if (!this.riskLevels$) {
            forceLoad = true;
            this.riskLevels$ = this.riskLevels.asObservable().pipe(shareReplay(1));
        }

        if (forceLoad) {
            const config: IProtocolConfig = this.oneProtocol.customConfig("GET", "/api/risk-v2/v2/risks/levels");
            const messages: IProtocolMessages = {
                Error: { custom: this.translatePipe.transform("CouldNotRetrieveRisk") },
            };
            from(this.oneProtocol.http(config, messages)
                .then((response: IProtocolResponse<IRiskLevel[]>) => this.riskLevels.next(response.result ? response.data : null)));
        }

        return this.riskLevels$;
    }

    public fetchRiskById(riskId: string): ng.IPromise<any> {
        const config: IProtocolConfig = this.oneProtocol.config("GET", `/risk/getquestion/${riskId}`);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("CouldNotRetrieveRisk") } };
        return this.oneProtocol.http(config, messages)
            .then((response: IProtocolPacket): any => response.result ? response.data : false);
    }

    public fetchRisksByProjectId(projectId: string, projectVersion: number, pageMetadata: IPaginationParams<IPaginationFilter[]>): ng.IPromise<IFetchingRisksResponse> {
        const params: any = this.buildParamForPagination(pageMetadata);
        assign(params, { id: projectId, version: projectVersion });

        const config: IProtocolConfig = this.oneProtocol.config("GET", `/project/risks`, params);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("CouldNotRetrieveRisks") } };
        return this.oneProtocol.http(config, messages)
            .then((response: IProtocolPacket): IFetchingRisksResponse => response.result ? response.data : false);
    }

    public fetchRiskByQuestionId(questionId: string): ng.IPromise<any> {
        const config: IProtocolConfig = this.oneProtocol.config("GET", `/projectquestion/get/${questionId}`);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("CouldNotRetrieveQuestion") } };
        return this.oneProtocol.http(config, messages)
            .then((response: IProtocolPacket): any => response.data.Risk);
    }

    // TODO: Deprecate once assessment risk modal migrated to shared risk modal
    public saveQuestionRisk(riskModel: IRisk): ng.IPromise<any> {
        const serializedRisk: IRiskSerialized = this.riskAdapter.serializeRisk(riskModel);
        const config: IProtocolConfig = this.oneProtocol.config("POST", `/risk/addquestion`, [], serializedRisk);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("CouldNotUpdateRisk") } };
        return this.oneProtocol.http(config, messages)
            .then((response: IProtocolPacket): any => response.result ? response.data : false);
    }

    public saveRiskV2(riskModel: IRiskCreateRequest): ng.IPromise<IRiskDetails | boolean> {
        const config: IProtocolConfig = this.oneProtocol.customConfig("POST", `/api/risk-v2/v2/risks`, [], riskModel);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("CouldNotUpdateRisk") } };
        return this.oneProtocol.http(config, messages)
            .then((response: IProtocolPacket): IRiskDetails | boolean => response.result ? response.data : false);
    }

    public updateRiskV2(riskModel: IRiskDetails): ng.IPromise<IRiskDetails> {
        const serializedRisk: IRiskSerializedV2 = this.riskAdapter.serializeRiskV2(riskModel);
        serializedRisk.impactLevel = RiskV2Levels[serializedRisk.impactLevel];
        serializedRisk.probabilityLevel = RiskV2Levels[serializedRisk.probabilityLevel];
        const config: IProtocolConfig = this.oneProtocol.customConfig("PUT", `/api/risk-v2/v2/risks/${riskModel.id}`, [], serializedRisk);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("CouldNotUpdateRisk") } };
        return this.oneProtocol.http(config, messages)
            .then((response: IProtocolPacket): IRiskDetails => response.result ? response.data : null);
    }

    public updateRiskDetailsV2New(riskModel: IRiskUpdateRequest): Observable<IProtocolResponse<void>> {
        const config: IProtocolConfig = this.oneProtocol.customConfig("PUT", `/api/risk-v2/v2/risks/${riskModel.id}`, [], riskModel);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("CouldNotUpdateRisk") } };
        return from(this.oneProtocol.http(config, messages));
    }

    public updateRiskTreatment(riskModel: IRisk): ng.IPromise<IProtocolPacket> {
        const config: IProtocolConfig = this.oneProtocol.config("PUT", `/risk/updatestate`, [], riskModel);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("CouldNotUpdateRisk") } };
        return this.oneProtocol.http(config, messages)
            .then((response: IProtocolPacket): any => response.result ? response.data : false);
    }

    public read(): ng.IPromise<IProtocolPacket> {
        const config: IProtocolConfig = this.oneProtocol.config("GET", `/risk/get/`);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("CouldNotRetrieveRisk") } };
        return this.oneProtocol.http(config, messages)
            .then((response: IProtocolPacket): any => response.result ? response.data : false);
    }

    public getRiskSetting(forceLoad = false): Observable<IRiskSetting> {
        if (!this.riskSettings$) {
            forceLoad = true;
            this.riskSettings$ = this.riskSettings.asObservable().pipe(shareReplay(1));
        }

        if (forceLoad) {
            const config: IProtocolConfig = this.oneProtocol.customConfig("Get", "/api/risk-v2/v2/risks/setting/old");
            const messages: IProtocolMessages = {
                Error: { custom: this.translatePipe.transform("CouldNotRetrieveRisk") },
            };
            from(this.oneProtocol.http(config, messages)
                .then((response: IProtocolResponse<IRiskSetting>) => this.riskSettings.next(response.result ? response.data : null)));
        }

        return this.riskSettings$;
    }

    public updateRiskSetting(riskSetting: IRiskSetting): Observable<IRiskSetting> {
        const config: IProtocolConfig = this.oneProtocol.customConfig("POST", `/api/risk-v2/v2/risks/setting/old`, [], riskSetting);
        const messages: IProtocolMessages = {
            Success: { custom: this.translatePipe.transform("CustomSettingsHaveBeenSaved") },
            Error: { custom: this.translatePipe.transform("CouldNotUpdateRiskSettings") },
        };
        return from(this.oneProtocol.http(config, messages)
            .then((response: IProtocolResponse<IRiskSetting>): IRiskSetting => {
                if (response.result) {
                    this.riskSettings.next(response.data);
                    return response.data;
                }
                return null;
            }));
    }

    public getIndividualRiskSetting(forceLoad = false): Observable<IIndividualRiskSetting> {
        if (!this.individualRiskSettings$) {
            forceLoad = true;
            this.individualRiskSettings$ = this.individualRiskSettings.asObservable().pipe(shareReplay(1));
        }

        if (forceLoad) {
            const config: IProtocolConfig = this.oneProtocol.customConfig("Get", "/api/risk-v2/v2/risks/setting");
            const messages: IProtocolMessages = {
                Error: { custom: this.translatePipe.transform("CouldNotRetrieveRisk") },
            };
            from(this.oneProtocol.http(config, messages)
                .then((response: IProtocolResponse<IIndividualRiskSetting>) => this.individualRiskSettings.next(response.result ? response.data : null)));
        }

        return this.individualRiskSettings$;
    }

    public updateIndividualRiskSetting(individualRiskSetting: IIndividualRiskSetting): Observable<IIndividualRiskSetting> {
        const config: IProtocolConfig = this.oneProtocol.customConfig("POST", `/api/risk-v2/v2/risks/setting`, [], individualRiskSetting);
        const messages: IProtocolMessages = {
            Success: { custom: this.translatePipe.transform("CustomSettingsHaveBeenSaved") },
            Error: { custom: this.translatePipe.transform("CouldNotUpdateRiskSettings") },
        };
        return from(this.oneProtocol.http(config, messages)
            .then((response: IProtocolResponse<IIndividualRiskSetting>): IIndividualRiskSetting => {
                if (response.result) {
                    this.individualRiskSettings.next(response.data);
                    return response.data;
                }
                return null;
            }));
    }

    public getIndividualRiskScoreByValuationMethod(methodName: string, forceLoad = false): Observable<IRiskLevel[]> {
        if (!this.individualRiskScore$) {
            forceLoad = true;
            this.individualRiskScore$ = this.individualRiskScore.asObservable().pipe(shareReplay(1));
        }
        if (forceLoad) {
            const config: IProtocolConfig = this.oneProtocol.customConfig("GET", "/api/risk-v2/v2/risks/setting/riskLevelScore/byValuationMethod/" + methodName);
            const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("CouldNotRetrieveIndividualRiskScore") } };
            this.oneProtocol.http(config, messages)
                .then((response: IProtocolResponse<IRiskLevel[]>) => this.individualRiskScore.next(response.result ? response.data : []));
        }
        return this.individualRiskScore$;
    }

    public updateRisk(requestBody: any): ng.IPromise<boolean> {
        const config: IProtocolConfig = this.oneProtocol.config("PUT", `/risk/updatestate`, [], requestBody);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("CouldNotUpdateRisk") } };
        return this.oneProtocol.http(config, messages)
            .then((response: IProtocolPacket): boolean => response.result);
    }

    public acceptRisk(id: string): ng.IPromise<boolean> {
        const config: IProtocolConfig = this.oneProtocol.config("PUT", `/risk/accept/${id}`, [], {});
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("CouldNotAcceptRisk") } };
        return this.oneProtocol.http(config, messages)
            .then((response: IProtocolPacket): boolean => response.result);
    }

    public unacceptRisk(id: string): ng.IPromise<IProtocolPacket> {
        const config: IProtocolConfig = this.oneProtocol.config("PUT", `/risk/unaccept/${id}`, [], {});
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("CouldNotUnacceptRisk") } };
        return this.oneProtocol.http(config, messages);
    }

    public deleteRisk(riskId: string): ng.IPromise<IProtocolPacket> {
        const config: IProtocolConfig = this.oneProtocol.config("DELETE", `/risk/deletequestion/${riskId}`);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("CouldNotDeleteRisk") } };
        return this.oneProtocol.http(config, messages);
    }

    public buildParamForLegacyPagination_deprecated(pagination: IPaginationParams<IPaginationFilter[]>): any {
        const params: any = {
            "pagination.pageNumber": pagination.page || 0,
            "pagination.pageSize": pagination.size || 10,
        };
        if (pagination.filters) {
            pagination.filters = map(pagination.filters, (filter: IPaginationFilter, index: number): IPaginationFilter => {
                params[`filter[${index}].key`] = filter.name;
                params[`filter[${index}].value`] = filter.value;
                return filter;
            });
        }
        return params;
    }

    public getPage(pagination: IPaginationParams<IPaginationFilter[]>): ng.IPromise<IProtocolPacket> {
        const params: any = this.buildParamForLegacyPagination_deprecated(pagination);
        const config: IProtocolConfig = this.oneProtocol.config("GET", `/risk/get/`, params);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("CouldNotRetrieveRisks") } };
        return this.oneProtocol.http(config, messages).then((response: IProtocolPacket) => {
            if (response.result) {
                const formattedMetaData: IPageable = this.pagination.formatPaginationMetadata(response.data.Metadata);
                const data: any = {
                    Content: response.data.Content,
                    Metadata: formattedMetaData,
                };
                return {
                    result: true,
                    data,
                };
            } else {
                return response;
            }
        });
    }

    public getRisks(filters?: IRiskFilter[]): ng.IPromise<IProtocolResponse<IRiskDetails[]>> {
        let filterQueryString = "";
        if (filters && filters.length) {
            forEach(filters, (filter: IRiskFilter, index: number): void => {
                filterQueryString += `${index > 0 ? "&" : "?"}filter[${index}].key=${filter.name}`;
                forEach(filter.values, (value: string): void => {
                    filterQueryString += `&filter[${index}].value=${value}`;
                });
            });
        }
        const config: IProtocolConfig = this.oneProtocol.customConfig("GET", `/api/v2/private/risk/get/${filterQueryString}`);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("CouldNotRetrieveRisks") } };
        return this.oneProtocol.http(config, messages);
    }

    public exportRisks(assessmentId?: string, paginationParams?: IGridPaginationParams, filter?: IStringMap<string>, fullTextSearch?: string): ng.IPromise<IProtocolResponse<any>> {
        const requestBody = {
            references: [{id: assessmentId}],
            filters: filter,
            fullTextSearch,
        };

        if (assessmentId) {
            assign(requestBody.references[0], { type: RiskReferenceTypes[RiskReferenceTypes.Assessment].toUpperCase() });
        } else {
            assign(requestBody.references[0], { type: "GENERIC" });
        }
        const config: IProtocolConfig = this.oneProtocol.customConfig("POST", `/api/risk-v2/v2/risks/export`, [], requestBody, null, null, null, APIResponseTypes.TEXT);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("CouldNotRetrieveRisks") } };
        return this.oneProtocol.http(config, messages);
    }

    public getRisksByReference(referenceId: string, type: string): ng.IPromise<IProtocolResponse<any>> {
        const config: IProtocolConfig =  this.oneProtocol.customConfig("GET", `/api/risk-v2/v2/risks/references/${referenceId}/types/${type}`);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("CouldNotRetrieveRisks") } };
        return this.oneProtocol.http(config, messages);
    }

    public deleteRiskV2(riskId: string): ng.IPromise<boolean> {
        const config: IProtocolConfig = this.oneProtocol.customConfig("DELETE", `/api/risk-v2/v2/risks/${riskId}`);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("CouldNotDeleteRisk") } };
        return this.oneProtocol.http(config, messages).then((response: IProtocolResponse<null>): boolean => response.result);
    }

    fetchRiskV2ById(riskId: string): ng.IPromise<IProtocolResponse<IRiskDetails | IRiskDetails>> {
        const config: IProtocolConfig = this.oneProtocol.customConfig("GET", `/api/risk-v2/v2/risks/${riskId}`);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("CouldNotRetrieveRisk") } };
        return this.oneProtocol.http(config, messages);
    }

    fetchInventoryRiskControl(riskId: string, paginationParams: IGridPaginationParams, fullText?: string, filters?: IInventoryRiskControlsFilter[]): Observable<IProtocolResponse<IPageableOf<IInventoryControlsTableRow>>> {
        const requestBody = {
            fullText,
            filters,
        };
        const config: IProtocolConfig = this.oneProtocol.customConfig("POST", `/api/risk-v2/v2/risks/${riskId}/controls/pages`, paginationParams, requestBody);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("CouldNotFetchRiskControl") } };
        return from(this.oneProtocol.http(config, messages));
    }

    // Risk Category API's
    fetchRiskCategory(paginationParams: IGridPaginationParams, fullText?: string, filters?: IRiskCategoryFilter[]): Observable<IProtocolResponse<IPageableOf<IRiskCategoryDetail>>> {
        const requestBody = {
            fullText,
            filters,
        };
        const config: IProtocolConfig = this.oneProtocol.customConfig("POST", `/api/risk-v2/v2/risks/categories/pages`, paginationParams, requestBody);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("CouldNotFetchRiskCategory") } };
        return from(this.oneProtocol.http(config, messages));
    }

    fetchRiskCategoryListByStatus(categoryStatus: RiskCategoryStatus): Observable<IProtocolResponse<IRiskCategoryBasicInfo[]>> {
        const config: IProtocolConfig = this.oneProtocol.customConfig("GET", `/api/risk-v2/v2/risks/categories/statuses/${categoryStatus}/categories`);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("CouldNotFetchRiskCategories") } };
        return from(this.oneProtocol.http(config, messages));
    }

    addNewCategory(riskCategory: IRiskNewCategoryModel): Observable<IProtocolResponse<void>> {
        const config: IProtocolConfig = this.oneProtocol.customConfig("POST", `/api/risk-v2/v2/risks/categories`, null, riskCategory);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("CouldNotAddRiskCategory") } };
        return from(this.oneProtocol.http(config, messages));
    }

    updateRiskCategory(categoryId: string, riskCategory: IRiskNewCategoryModel): Observable<IProtocolResponse<void>> {
        const config: IProtocolConfig = this.oneProtocol.customConfig("PUT", `/api/risk-v2/v2/risks/categories/${categoryId}`, null, riskCategory);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("CouldNotUpdateRiskCategory") } };
        return from(this.oneProtocol.http(config, messages));
    }

    archiveRiskCategory(categoryId: string): Observable<IProtocolResponse<void>> {
        const config: IProtocolConfig = this.oneProtocol.customConfig("PUT", `/api/risk-v2/v2/risks/categories/${categoryId}/archive`);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("CouldNotArchiveRiskCategories") } };
        return from(this.oneProtocol.http(config, messages));
    }

    unarchiveRiskCategory(categoryId: string): Observable<IProtocolResponse<void>> {
        const config: IProtocolConfig = this.oneProtocol.customConfig("PUT", `/api/risk-v2/v2/risks/categories/${categoryId}/unarchive`);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("CouldNotUnArchiveRiskCategories") } };
        return from(this.oneProtocol.http(config, messages));
    }

    public fetchRiskSummary(filters: IStringMap<string>, paginationParams: IGridPaginationParams): ng.IPromise<IProtocolResponse<IPageable>> {
        const params = {
            "pagination.pageSize": paginationParams.size,
            "pagination.pageNumber": paginationParams.page,
            "filter[0].key": "refId",
            "filter[0].value": filters.refId,
            "filter[1].key": "riskOwnerId",
            "filter[1].value": filters.userId,
        };
        const config: IProtocolConfig = this.oneProtocol.customConfig("GET", "/api/v2/private/risk/page/", params);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("CouldNotRetrieveRisk") } };
        return this.oneProtocol.http(config, messages);
    }

    public fetchRiskV2Summary(assessmentId?: string, paginationParams?: IGridPaginationParams, filter?: IStringMap<string>, fullTextSearch?: string): ng.IPromise<IProtocolResponse<IPageable>> {
        const params = {
            page: paginationParams.page,
            size: paginationParams.size,
            sort: paginationParams.sort ? paginationParams.sort : "",
        };
        const requestBody = {
            references: [],
            filters: filter,
            fullTextSearch,
        };

        if (assessmentId) {
            assign(requestBody.filters, { assessmentId });
        }

        const config: IProtocolConfig = this.oneProtocol.customConfig("POST", "/api/risk-v2/v2/risks/page", params, requestBody);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("CouldNotRetrieveRisk") } };
        return this.oneProtocol.http(config, messages);
    }

    public getHeatmapSettings(forceLoad = false): Observable<IRiskHeatmapSettings> {
        if (!this.heatmapSettings$) {
            forceLoad = true;
            this.heatmapSettings$ = this.heatmapSettings.asObservable().pipe(shareReplay(1));
        }
        if (forceLoad) {
            const config: IProtocolConfig = this.oneProtocol.customConfig("GET", "/api/risk-v2/v2/risks/heatmap");
            const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("CouldNotRetrieveRiskHeatmapSettings") } };
            this.oneProtocol.http(config, messages)
                    .then((response: IProtocolResponse<IRiskHeatmapSettings>) => this.heatmapSettings.next(response.result ? response.data : null));
        }
        return this.heatmapSettings$
                .pipe(
                    rxMap((settings: IRiskHeatmapSettings) => this.sortHeatmapLevels(settings)),
                );
    }

    public saveHeatmapSettings(settings: IRiskHeatmapSettings): Observable<IRiskHeatmapSettings> {
        const config: IProtocolConfig = this.oneProtocol.customConfig("POST", "/api/risk-v2/v2/risks/heatmap", null, settings);
        const messages: IProtocolMessages = {
            Success: { custom: this.translatePipe.transform("CustomSettingsHaveBeenSaved") },
            Error: { custom: this.translatePipe.transform("CouldNotSaveRiskHeatmapSettings") },
        };
        return from(this.oneProtocol.http(config, messages)
            .then((response: IProtocolResponse<IRiskHeatmapSettings>) => {
                if (response.result) {
                    this.heatmapSettings.next(response.data);
                    return response.data;
                } else {
                    return null;
                }
            }));
    }

    public resetHeatmapSettings(): ng.IPromise<IRiskHeatmapSettings> {
        const config: IProtocolConfig = this.oneProtocol.customConfig("POST", "/api/risk-v2/v2/risks/heatmap/reset");
        const messages: IProtocolMessages = {
            Success: { custom: this.translatePipe.transform("RiskSettingReset") },
            Error: { custom: this.translatePipe.transform("CouldNotResetRiskSetting") },
        };
        return this.oneProtocol.http(config, messages).then((response: IProtocolResponse<IRiskHeatmapSettings>) => {
            if (response.result) {
                this.heatmapSettings.next(response.data);
                return response.data;
            }
            return null;
        });
    }

    // Unlink Assessment Risk from inventory
    unlinkAssessmentRisk(unlinkAssessmentRiskRequest: IAssessmentRiskUnlinkRequest): Observable<IProtocolResponse<boolean>> {
        const config: IProtocolConfig = this.oneProtocol.customConfig("POST", `/api/risk-v2/v2/risks/inheritance/unlink`, null, unlinkAssessmentRiskRequest);
        const messages: IProtocolMessages = {
            Success: { custom: this.translatePipe.transform("AssessmentRiskUnlinkedSuccessfully") },
            Error: { custom: this.translatePipe.transform("CouldNotUnlinkAssessmentRisk") },
        };
        return from(this.oneProtocol.http(config, messages));
    }

    private buildParamForPagination(pageMetadata: IPaginationParams<IPaginationFilter[]>): any {
        const params: any = {
            "pagination.pageNumber": pageMetadata.page || 0,
            "pagination.pageSize": pageMetadata.size || 10,
        };
        if (pageMetadata.filters) {
            forEach(pageMetadata.filters, (filter: IPaginationFilter, index: number): void => {
                params[`filter[${index}].key`] = filter.name;
                params[`filter[${index}].value`] = filter.value;
            });
        }
        if (pageMetadata.sort) {
            let index = 0;
            forIn(pageMetadata.sort, (value: string, key: string): void => {
                params[`sort[${index}].key`] = key;
                params[`sort[${index}].value`] = value;
                index++;
            });
        }
        return params;
    }

    private sortHeatmapLevels(settings: IRiskHeatmapSettings): IRiskHeatmapSettings {
        return {
            ...settings,
            impactLevels: sortBy(settings.impactLevels, (level: ISettingsRiskImpact) => level.position),
            probabilityLevels: sortBy(settings.probabilityLevels, (level: ISettingsRiskProbability) => level.position),
        };
    }

    private clearRisksCache() {
        this.rootScope.$on("event:auth-logout", () => {
            this.riskSettings$ = null;
            this.riskSettings = new Subject<IRiskSetting>();
            this.riskLevels$ = null;
            this.riskLevels = new Subject<IRiskLevel[]>();
            this.individualRiskSettings$ = null;
            this.individualRiskSettings = new Subject<IIndividualRiskSetting>();
            this.individualRiskScore$ = null;
            this.individualRiskScore = new Subject<IRiskLevel[]>();
        });
    }
}
