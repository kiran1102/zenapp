import moment, { Moment } from "moment";
import "moment-timezone";
import { find } from "lodash";
import { Settings } from "sharedServices/settings.service";
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import { IProtocolPacket } from "interfaces/protocol.interface";
import { IKeyValue, ILabelValue } from "interfaces/generic.interface";

// Constants
import { DateTimeFormats } from "constants/date-time.constant";

export class TimeStamp {
    static $inject: string[] = ["$rootScope", "Settings", "$q"];

    private timezone: string;
    private currentDateFormat: string;
    private currentTimeFormat: string;
    private defaultFormat: string;
    private defaultDateFormat: string;
    private defaultTimeFormat: string;

    constructor(
        private readonly $rootScope: IExtendedRootScopeService,
        private readonly settings: Settings,
        private readonly $q: ng.IQService,
    ) {
        this.timezone = moment.tz.guess();
        this.defaultDateFormat = "MM/DD/YYYY";
        this.defaultTimeFormat = "hh:mm A";
        this.defaultFormat = `${this.defaultDateFormat} ${this.defaultTimeFormat}`;
    }

    get dateFormats(): Array<ILabelValue<string>> {
        return [
            {
                label: this.$rootScope.t("YYYY/MM/DD"),
                value: "YYYY/MM/DD",
            },
            {
                label: this.$rootScope.t("DD/MM/YYYY"),
                value: "DD/MM/YYYY",
            },
            {
                label: this.$rootScope.t("MM/DD/YYYY"),
                value: "MM/DD/YYYY",
            },
            {
                label: this.$rootScope.t("DD/MM/YY"),
                value: "DD/MM/YY",
            },
            {
                label: this.$rootScope.t("MM/DD/YY"),
                value: "MM/DD/YY",
            },
            {
                label: this.$rootScope.t("DoNotDisplay"),
                value: " ",
            },
        ];
    }

    get timeFormats(): Array<ILabelValue<string>> {
        return [
            {
                label: this.$rootScope.t("12Hour"),
                value: "hh:mm A",
            },
            {
                label: this.$rootScope.t("24Hour"),
                value: "HH:mm",
            },
            {
                label: this.$rootScope.t("DoNotDisplay"),
                value: " ",
            },
        ];
    }

    get backupDateTimeFormat(): string {
        return this.defaultFormat;
    }

    get currentDateFormatting(): string {
        return this.currentDateFormat || this.defaultDateFormat;
    }

    get currentTimeFormatting(): string {
        return this.currentTimeFormat || this.defaultTimeFormat;
    }

    get combiningDateTimeFormat(): string {
        return `${this.currentDateFormat} ${this.currentTimeFormat}`;
    }

    get currentDate(): Moment {
        return moment.utc();
    }

    get dateFormatWithoutTime(): string {
        return this.currentDateFormat
            ? this.currentDateFormat
            : this.defaultDateFormat;
    }

    getReminderDate(days: number, reminderDate: string): string {
        const reminderDateTemp = moment.utc(reminderDate);
        reminderDateTemp.millisecond(reminderDateTemp.millisecond() - 1000 * 60 * 60 * 24 * days);
        return reminderDateTemp.format();
    }

    // Used in Incident Activity Stream Only for 3.23
    formatJavaDate(dateToFormat: string, forceUseBackupDateTimeFormat?: boolean): string {
        return ((this.combiningDateTimeFormat !== `${null} ${null}`) && !forceUseBackupDateTimeFormat)
            ? moment(dateToFormat).utc().tz(this.timezone).format(this.combiningDateTimeFormat)
            : moment(dateToFormat).utc().tz(this.timezone).format(this.backupDateTimeFormat);
    }

    formatDate(dateToFormat: string, forceUseBackupDateTimeFormat?: boolean): string {
        return ((this.combiningDateTimeFormat !== `${null} ${null}`) && !forceUseBackupDateTimeFormat)
            ? moment.utc(dateToFormat).tz(this.timezone).format(this.combiningDateTimeFormat)
            : moment.utc(dateToFormat).tz(this.timezone).format(this.backupDateTimeFormat);
    }

    // Use this when prepopulating DatePicker.
    formatDateWithoutTime(dateToFormat: string, forceUseBackupDateFormat?: boolean): string {
        const currentFormat = this.dateFormatWithoutTime.replace(/Y{1,4}/i, "YYYY");
        return ((this.combiningDateTimeFormat !== `${null} ${null}`) && !forceUseBackupDateFormat)
            ? moment.utc(dateToFormat).tz(this.timezone).format(currentFormat)
            : moment.utc(dateToFormat).tz(this.timezone).format(this.defaultDateFormat);
    }

    formatDateWithoutTimezoneForDatepicker(dateToFormat: string, forceUseBackupDateFormat?: boolean): string {
        const currentFormat = this.dateFormatWithoutTime.replace(/Y{1,4}/i, "YYYY");
        return ((this.combiningDateTimeFormat !== `${null} ${null}`) && !forceUseBackupDateFormat)
            ? moment.utc(dateToFormat).format(currentFormat).trim()
            : moment.utc(dateToFormat).format(this.defaultDateFormat).trim();
    }

    formatDateWithoutTimezone(dateToFormat: string, forceUseBackupDateFormat?: boolean): string {
        return ((this.combiningDateTimeFormat !== `${null} ${null}`) && !forceUseBackupDateFormat)
            ? moment.utc(dateToFormat).format(this.currentDateFormat).trim()
            : moment.utc(dateToFormat).format(this.defaultDateFormat).trim();
    }

    addDaysToDate(dateToFormat: string, daysToAdd: number): string {
        return (this.combiningDateTimeFormat !== `${null} ${null}`) ? moment.utc(dateToFormat).tz(this.timezone).add(daysToAdd, "d").format(this.combiningDateTimeFormat) : moment.utc(dateToFormat).tz(this.timezone).add(daysToAdd, "d").format(this.backupDateTimeFormat);
    }

    // converts the ISO-8601 format string in Zero UTC format to local time format ISO string
    getLocalDate(date: string) {
        if (date) {
            try {
                const localDate = new Date(date);
                return new Date(localDate.getTime() - (localDate.getTimezoneOffset() * 60000)).toISOString();
            } catch (ex) {
                return "";
            }
        }
    }

    // converts date object to ISO-8601 format
    formatDateToIso(date: {day: number, month: number, year: number}): string {
        let day = date.day.toString();
        let month = date.month.toString();
        let year = date.year.toString();
        if (day.length === 1) day = `0${day}`;
        if (month.length === 1) month = `0${month}`;
        if (year.length === 2) {
            const fullYear: number = new Date().getFullYear();
            const yearPrefix: string = fullYear.toString().slice(0, 2);
            year = `${yearPrefix}${year}`;
        }
        return `${year}-${month}-${day}`;
    }

    isValidDate(dateToFormat: string): boolean {
        return moment.utc(dateToFormat).tz(this.timezone).isValid();
    }

    setCurrentTimeFormat(): ng.IPromise<IProtocolPacket> {
        return this.settings.getTimeFormat().then((res: IProtocolPacket): IProtocolPacket => {
            if (res.result) {
                this.currentTimeFormat = res.data.value;
            }
            return res;
        });
    }

    setCurrentDateFormat(): ng.IPromise<IProtocolPacket> {
        return this.settings.getDateFormat().then((res: IProtocolPacket): IProtocolPacket => {
            if (res.result) {
                this.currentDateFormat = res.data.value;
            }
            return res;
        });
    }

    dateTimeCombinedFormats(): ng.IPromise<string[]> {
        const dateTimePromise: Array<ng.IPromise<IProtocolPacket>> = [this.setCurrentDateFormat(), this.setCurrentTimeFormat()];
        return this.$q.all(dateTimePromise).then(() => [this.currentDateFormat, this.currentTimeFormat] );
    }

    updateTimeStamp(params: Array<IKeyValue<string>>): ng.IPromise<IProtocolPacket> {
        return this.settings.updateDateTimeFormat(params).then((res: IProtocolPacket): IProtocolPacket => {
            if (res.result) {
                const tempDate: IKeyValue<string> = find(params, (option: IKeyValue<string>): boolean => {
                    return option.key === DateTimeFormats.Date;
                });
                if (tempDate) {
                    this.currentDateFormat = tempDate.value;
                }

                const tempTime: IKeyValue<string> = find(params, (option: IKeyValue<string>): boolean => {
                    return option.key === DateTimeFormats.Time;
                });
                if (tempTime) {
                    this.currentTimeFormat = tempTime.value;
                }
            }
            return res;
        });
    }

    getMillisecondsSince(createdDate: string): number {
        return Math.abs(moment.utc(createdDate).diff(this.currentDate));
    }

    returnDayTime(createdDate: string): string {
        const minuteMS: number = 60 * 1000;
        const hourMS: number = 60 * minuteMS;
        const dayMS: number = 24 * hourMS;

        const daysSince: number = this.getMillisecondsSince(createdDate);
        const days: number = Math.floor(daysSince / dayMS);
        const hours: number = Math.floor((daysSince - (days * dayMS)) / hourMS);
        const minutes: number = Math.floor((daysSince - (days * dayMS) - (hours * hourMS)) / minuteMS);

        const daysString: string = (days > 1) ? (days + " days") : (days + " day");
        const hoursString: string = (hours > 1) ? (hours + " hours") : (hours + " hour");
        const minutesString: string = (minutes > 1) ? (minutes + " minutes") : (minutes + " minute");

        let timeString = "";

        if (this.currentTimeFormatting === " ") {
            timeString += daysString;
        } else {
            if (days && hours) {
                timeString += (daysString + " " + hoursString);
            } else if (days) {
                timeString += daysString;
            } else if (hours) {
                timeString += hoursString;
            } else if (minutes) {
                timeString += minutesString;
            } else {
                timeString += this.$rootScope.t("JustNow");
            }
        }
        return timeString;
    }

    getFormattedCurrentDate(): string {
        return moment.utc().tz(this.timezone).format(this.currentDateFormat || this.defaultDateFormat);
    }

    // reason: Jackson parser (on BE) throws error when ISO Format (YYYY-MM-DDTHH:mm:ss.sssZ) has milliseconds === .000Z
    incrementISODateMillisecond(isoDate: string): string {
        return new Date(Date.parse(isoDate) + 1).toISOString();
    }

    getIs24Hour(): boolean {
        const timeFormat: string = this.currentTimeFormat
                            ? this.currentTimeFormat
                            : this.defaultTimeFormat;
        return timeFormat[timeFormat.length - 1] !== "A";
    }

    removeTimeFromISOString(ISOString: string): string {
        return ISOString ? ISOString.split("T")[0] : "";
    }

    createJSDateFromISO(ISOString: string): Date {
        const dateParts: string[] = ISOString.split("-");
        return new Date(parseInt(dateParts[0], 10), parseInt(dateParts[1], 10) - 1, parseInt(dateParts[2], 10));
    }

    getDatePickerDateFormat(): string {
        const currentFormat = this.dateFormatWithoutTime.toLowerCase();
        return (currentFormat === " ") ? this.defaultDateFormat.toLowerCase() : currentFormat;
    }

    displayDate(): boolean {
        return !(this.currentDateFormatting === " ");
    }

    displayTime(): boolean {
        return !(this.currentTimeFormatting === " ");
    }

    // used for ot-datepicker and ot-date-time-picker's dateDisableUntil (forces user to select a >= today)
    getDisableUntilDate(date?: Date) {
        if (!date) {
            return {
                day: moment().date() - 1,
                month: moment().month() + 1,
                year: moment().year(),
            };
        } else {
            return {
                day: moment(date).date() - 1,
                month: moment(date).month() + 1,
                year: moment(date).year(),
            };
        }
    }

    getDisableSinceDate(date?: Date) {
        const currentFormat = this.dateFormatWithoutTime.replace(/Y{1,4}/i, "YYYY");
        if (!date) {
            return {
                day: 0,
                month: 0,
                year: 0,
            };
        } else {
            return {
                day: moment(date, currentFormat).date(),
                month: moment(date, currentFormat).month() + 1,
                year: moment(date, currentFormat).year(),
            };
        }
    }

    getRangePickerModelFromISO(beginISODate: string, endISODate: string): string {
        if (!beginISODate || !endISODate) return "";
        return `${this.formatDateWithoutTime(beginISODate)} - ${this.formatDateWithoutTime(endISODate)}`;
    }

    getEndOfDayISO(ISOString: string): string {
        return moment.utc(ISOString).add(1, "days").subtract(1, "seconds").toISOString();
    }

    revertEndOfDayISO(ISOString: string): string {
        return moment.utc(ISOString).subtract(1, "days").add(1, "seconds").toISOString();
    }
}
