import _ from "lodash";
import moment, { Moment } from "moment";
import "moment-timezone";
import Assignments from "oneServices/assignments.service.ts";
import { ProtocolService } from "modules/core/services/protocol.service";
import { NotificationService } from "modules/shared/services/provider/notification.service";

import { AssignmentTypes } from "enums/assignment-types.enum";
import { RiskLevels } from "enums/risk.enum";

import { AssessmentStates, AssessmentStateKeys } from "constants/assessment.constants";

import {
    IProtocolPacket,
    IProtocolMessages,
    IProtocolConfig,
    IProtocolResponse,
} from "interfaces/protocol.interface";
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import {
    IProject,
    ICreateNextProjectParams,
    ICreateProjectNotePayload,
    IProjectStatusCount,
    IDuplicateProjectContract,
    IProjectForm,
    IProjectCreateParams,
} from "interfaces/project.interface";
import { IUser } from "interfaces/user.interface";
import { IProjectSettings } from "interfaces/setting.interface";
import { ISection } from "interfaces/section.interface";
import { IQuestion } from "interfaces/question.interface";
import {
    IProjectPageResponse,
    IProjectGetPageResponse,
    IProjectGetResponse,
    IProjectGetPageResponseNormalized,
} from "interfaces/assessment.interface";
import { IStore } from "interfaces/redux.interface";

// Redux
import { getCurrentUser } from "oneRedux/reducers/user.reducer";

export class ProjectService {
    static $inject: string[] = [
        "$rootScope",
        "OneProtocol",
        "ENUMS",
        "Settings",
        "Permissions",
        "Sections",
        "Assignments",
        "NotificationService",
        "store",
    ];

    private translations: any;
    private projectCustomTerm: any;
    private projectsCustomTerm: any;
    private riskTrackingCustomTerm: any;
    private recommendationsCustomTerm: any;

    constructor(
        public $rootScope: IExtendedRootScopeService,
        private OneProtocol: ProtocolService,
        private ENUMS: any,
        private Settings: any,
        private Permissions: any,
        private Sections: any,
        private assignments: Assignments,
        private notificationService: NotificationService,
        private store: IStore,
    ) {
        this.projectCustomTerm = { Project: $rootScope.customTerms.Project };
        this.projectsCustomTerm = { Projects: $rootScope.customTerms.Projects };
        this.riskTrackingCustomTerm = { RiskTracking: $rootScope.customTerms.RiskTracking };
        this.recommendationsCustomTerm = { Recommendations: $rootScope.customTerms.Recommendations };
        this.translations = {
            duplicateProject: this.$rootScope.t("DuplicateProject", this.projectCustomTerm),
            errorRetrievingProject: $rootScope.t("ErrorRetrievingProject", this.projectCustomTerm),
            errorCreatingProject: $rootScope.t("ErrorCreatingProject", this.projectCustomTerm),
            errorCopyingProject: $rootScope.t("ErrorCopyingProject", this.projectCustomTerm),
            errorDuplicatingProject: $rootScope.t("ErrorDuplicatingProject", this.projectCustomTerm),
            errorUpdatingProject: $rootScope.t("ErrorUpdatingProject", this.projectCustomTerm),
            errorDeletingProject: $rootScope.t("ErrorDeletingProject", this.projectCustomTerm),
            errorCompletingProject: $rootScope.t("ErrorCompletingProject", this.projectCustomTerm),
            errorSubmittingProject: $rootScope.t("ErrorSubmittingProject", this.projectCustomTerm),
            errorStartingProject: $rootScope.t("ErrorStartingProject", this.projectCustomTerm),
            errorGettingProjectApprovalStatus: $rootScope.t("ErrorGettingProjectApprovalStatus", this.projectCustomTerm),
            errorRequestingForMoreInformation: $rootScope.t("ErrorRequestingForMoreInformation", this.projectCustomTerm),
            organizationDeniedAccessToProject: $rootScope.t("OrganizationDeniedAccessToProject", this.projectCustomTerm),
            errorDeletingSampleProjects: $rootScope.t("ErrorDeletingSampleProjects", this.projectsCustomTerm),
            sampleProjectsDeleted: $rootScope.t("SampleProjectsDeleted", this.projectsCustomTerm),
            errorRetrievingNextQuestion: $rootScope.t("ErrorRetrievingNextQuestion"),
            errorCheckingRemainingRequiredQuestions: $rootScope.t("ErrorCheckingRemainingRequiredQuestions"),
            errorCreatingNote: $rootScope.t("ErrorCreatingNote"),
            errorProceedingToRiskTracking: $rootScope.t("ErrorProceedingToRiskTracking", this.riskTrackingCustomTerm),
            errorSendingRecommendations: $rootScope.t("ErrorSendingRecommendations", this.recommendationsCustomTerm),
            errorLinkingProject: $rootScope.t("ErrorLinkingProject"),
            errorRemovingProjectLink: $rootScope.t("ErrorRemovingProjectLink"),
            successfullyLinkedProjects: $rootScope.t("AssessmentsSuccessfullyLinked"),
        };
    }

    public list = (type: string): ng.IPromise<IProtocolPacket> => {
        const params: any = { types: this.buildParams(type) };
        const config: IProtocolConfig = this.OneProtocol.config("GET", `/project/getproject`, params);
        const messages: IProtocolMessages = { Error: { custom: this.translations.errorRetrievingProject } };
        return this.OneProtocol.http(config, messages).then((res: IProtocolPacket) => {
            res.data.time = (new Date()).getTime();
            this.$rootScope.APICache.put("Projects.list", res.data);
            return res;
        });
    }

    public getPage = (templateType: string, paginationOptions: any, filterOptions: any, sortOptions: any): ng.IPromise<IProtocolResponse<IProjectGetPageResponseNormalized>> => {
        paginationOptions = paginationOptions || { pageSize: 10, pageNumber: 1 };
        const params: any = {
            "types": this.buildParams(templateType),
            "pagination.pageSize": paginationOptions.pageSize,
            "pagination.pageNumber": paginationOptions.pageNumber,
        };
        if (filterOptions) {
            _.each(filterOptions, (option: any, index: number): void => {
                params["filter[" + index + "].key"] = option.filterType;
                params["filter[" + index + "].value"] = option.filterValue || "";
            });
        }
        if (sortOptions) {
            params["sort[0].key"] = sortOptions.sortType;
            params["sort[0].value"] = sortOptions.sortReverse ? "desc" : "asc";
        }
        const config: IProtocolConfig = this.OneProtocol.config("GET", `/project/getpage`, params);
        const messages: IProtocolMessages = { Error: { custom: this.translations.errorRetrievingProject } };
        return this.OneProtocol.http(config, messages).then((res: IProtocolResponse<IProjectGetPageResponse>): IProtocolResponse<IProjectGetPageResponseNormalized> => {
            return {
                result: true,
                data: {
                    page: res.data.Content,
                    metadata: res.data.Metadata,
                },
            };
        });
    }

    public linkProjects = (recordId: string, projectIds: string[]): ng.IPromise<IProtocolResponse<null>> => {
        const config: IProtocolConfig = this.OneProtocol.config("POST", `/project/inventory/${recordId}`, [], projectIds);
        const messages: IProtocolMessages = {
            Error: { custom: this.translations.errorLinkingProject },
            Success: { custom: this.translations.successfullyLinkedProjects },
        };
        return this.OneProtocol.http(config, messages);
    }

    public read = (id: string, version: string): ng.IPromise<IProtocolResponse<IProjectGetResponse>> => {
        const params: any = version ? { id, version } : { id };
        const config: IProtocolConfig = this.OneProtocol.config("GET", `/project/get`, params);
        const messages: IProtocolMessages = { Error: { custom: this.translations.errorRetrievingProject } };
        return this.OneProtocol.http(config, messages).then((res: IProtocolPacket): IProtocolPacket => {
            if (res.result) {
                res.data.time = (new Date()).getTime();
                this.$rootScope.APICache.put("Projects.read" + id, res.data);
                return res;
            } else if (res.status === 403) {
                const alertText: string = this.translations.organizationDeniedAccessToProject;
                const packet = {
                    result: false,
                    data: alertText,
                };
                this.notificationService.alertNote("Note: ", alertText);
                return res;
            } else {
                return res;
            }
        });
    }

    public getVersion = (id: string, version: string): ng.IPromise<IProtocolPacket> => {
        const config: IProtocolConfig = this.OneProtocol.config("GET", `/project/get/${id}/?version=${version}`);
        const messages: IProtocolMessages = { Error: { custom: this.translations.errorRetrievingProject } };
        return this.OneProtocol.http(config, messages).then((res: IProtocolPacket): IProtocolPacket => {
            if (res.result) {
                res.data.time = (new Date()).getTime();
                this.$rootScope.APICache.put("Projects.read" + id, res.data);
                return res;
            } else {
                if (res.status === 403) {
                    const alertText: string = this.translations.organizationDeniedAccessToProject;
                    const packet = {
                        result: false,
                        data: alertText,
                    };
                    this.notificationService.alertNote("Note: ", alertText);
                    return res;
                }
            }
        });
    }

    public getHistory = (params: any): ng.IPromise<IProtocolPacket> => {
        const _params: any = params && _.isObject(params) ? params : {};
        const config: IProtocolConfig = this.OneProtocol.config("GET", `/project/gethistory/`, _params);
        const messages: IProtocolMessages = { Error: { custom: this.translations.errorRetrievingProject } };
        return this.OneProtocol.http(config, messages).then((res: IProtocolPacket): IProtocolPacket => {
            res.data.time = (new Date()).getTime();
            this.$rootScope.APICache.put("Projects.getHistory" + params.Id + params.Version, res.data);
            return res;
        });
    }

    public create = (project: IProjectCreateParams): ng.IPromise<IProtocolPacket> => {
        this.$rootScope.APICache.clearData("Projects.list");
        const config: IProtocolConfig = this.OneProtocol.config("POST", `/project/post/`, [], project);
        const messages: IProtocolMessages = { Error: { custom: this.translations.errorCreatingProject } };
        return this.OneProtocol.http(config, messages);
    }

    public createNextProject = (projectInfo: ICreateNextProjectParams): ng.IPromise<IProtocolPacket> => {
        this.$rootScope.APICache.clearData("Projects.list");
        const config: IProtocolConfig = this.OneProtocol.config("POST", `/project/createnextproject/`, [], projectInfo);
        const messages: IProtocolMessages = { Error: { custom: this.translations.errorCreatingProject } };
        return this.OneProtocol.http(config, messages);
    }

    public cloneProject = (id: string): ng.IPromise<IProtocolPacket> => {
        const config: IProtocolConfig = this.OneProtocol.config("POST", `/project/clone/${id}`);
        const messages: IProtocolMessages = { Error: { custom: this.translations.errorCopyingProject } };
        return this.OneProtocol.http(config, messages);
    }

    public createVersion = (project: IProject): ng.IPromise<IProtocolPacket> => {
        this.$rootScope.APICache.clearData("Projects.list");
        const config: IProtocolConfig = this.OneProtocol.config("POST", `/project/AddVersion/`, [], project);
        const messages: IProtocolMessages = { Error: { custom: this.translations.errorCreatingProject } };
        return this.OneProtocol.http(config, messages);
    }

    public update = (project: IProject): ng.IPromise<IProtocolPacket> => {
        this.$rootScope.APICache.clearData("Projects.list");
        const config: IProtocolConfig = this.OneProtocol.config("PUT", `/project/put/`, [], project);
        const messages: IProtocolMessages = { Error: { custom: this.translations.errorUpdatingProject } };
        return this.OneProtocol.http(config, messages);
    }

    public getProgress = (id: string, version: string): ng.IPromise<IProtocolPacket> => {
        const params: any = version ? { version } : "";
        const config: IProtocolConfig = this.OneProtocol.config("GET", `/project/progress/${id}`, params);
        const messages: IProtocolMessages = { Error: { custom: this.translations.errorRetrievingProject } };
        return this.OneProtocol.http(config, messages);
    }

    public getQuestion = (questionId: string): ng.IPromise<IProtocolPacket> => {
        const config: IProtocolConfig = this.OneProtocol.config("GET", `/project/getquestion/${questionId}`);
        const messages: IProtocolMessages = { Error: { custom: this.translations.errorRetrievingNextQuestion } };
        return this.OneProtocol.http(config, messages);
    }

    public delete = (id: string): ng.IPromise<IProtocolPacket> => {
        this.$rootScope.APICache.clearData("Projects.list");
        const config: IProtocolConfig = this.OneProtocol.config("DELETE", `/project/delete/${id}`);
        const messages: IProtocolMessages = { Error: { custom: this.translations.errorDeletingProject } };
        return this.OneProtocol.http(config, messages);
    }

    public remainingQuestions = (id: string): ng.IPromise<IProtocolPacket> => {
        const config: IProtocolConfig = this.OneProtocol.config("GET", `/project/remainingquestions/${id}`);
        const messages: IProtocolMessages = { Error: { custom: this.translations.errorCheckingRemainingRequiredQuestions } };
        return this.OneProtocol.http(config, messages);
    }

    public submitProject = (id: string, version: number): ng.IPromise<IProtocolPacket> => {
        const params: any = version ? { version } : "";
        const config: IProtocolConfig = this.OneProtocol.config("PUT", `/project/submit/${id}`, params);
        const messages: IProtocolMessages = { Error: { custom: this.translations.errorSubmittingProject } };
        return this.OneProtocol.http(config, messages);
    }

    public completeProject = (payload: any): ng.IPromise<IProtocolPacket> => {
        const config: IProtocolConfig = this.OneProtocol.config("PUT", `/project/complete/`, [], payload);
        const messages: IProtocolMessages = { Error: { custom: this.translations.errorCompletingProject } };
        return this.OneProtocol.http(config, messages);
    }

    public requestProjectInfo = (payload: any): ng.IPromise<IProtocolPacket> => {
        const config: IProtocolConfig = this.OneProtocol.config("PUT", `/project/requestinfo/`, [], payload);
        const messages: IProtocolMessages = { Error: { custom: this.translations.errorRequestingForMoreInformation } };
        return this.OneProtocol.http(config, messages);
    }

    public analyzeProject = (id: string, version: number): ng.IPromise<IProtocolPacket> => {
        const params: any = version ? { version } : {};
        const config: IProtocolConfig = this.OneProtocol.config("PUT", `/project/analyzerisks/${id}`, params);
        const messages: IProtocolMessages = { Error: { custom: this.translations.errorProceedingToRiskTracking } };
        return this.OneProtocol.http(config, messages);
    }

    public mitigateProject = (id: string, version: number): ng.IPromise<IProtocolPacket> => {
        const params: any = version ? { version } : "";
        const config: IProtocolConfig = this.OneProtocol.config("PUT", `/project/mitigaterisks/${id}`, params);
        const messages: IProtocolMessages = { Error: { custom: this.translations.errorSendingRecommendations } };
        return this.OneProtocol.http(config, messages);
    }

    public startProject = (id: string): ng.IPromise<IProtocolPacket> => {
        const config: IProtocolConfig = this.OneProtocol.config("PUT", `/project/start/${id}`);
        const messages: IProtocolMessages = { Error: { custom: this.translations.errorStartingProject } };
        return this.OneProtocol.http(config, messages);
    }

    public createNote = (params: ICreateProjectNotePayload): ng.IPromise<IProtocolPacket> => {
        const config: IProtocolConfig = this.OneProtocol.config("POST", `/comment/addnote/`, [], params);
        const messages: IProtocolMessages = { Error: { custom: this.translations.errorCreatingNote } };
        return this.OneProtocol.http(config, messages);
    }

    public removeSeededData = (): ng.IPromise<IProtocolPacket> => {
        const config: IProtocolConfig = this.OneProtocol.config("DELETE", `/project/deleteseeded/`);
        const messages: IProtocolMessages = {
            Error: { custom: this.translations.errorDeletingSampleProjects },
            Success: { custom: this.translations.sampleProjectsDeleted },
        };
        return this.OneProtocol.http(config, messages);
    }

    public duplicateProject = (contract: IDuplicateProjectContract): ng.IPromise<IProtocolPacket> => {
        const config: IProtocolConfig = this.OneProtocol.config("POST", `/project/copy/`, [], contract);
        const messages: IProtocolMessages = {
            Error: { custom: this.translations.errorDuplicatingProject },
        };
        return this.OneProtocol.http(config, messages);
    }

    public countEachProjectStates(projects: IProject[]): IProjectStatusCount {
        const status: IProjectStatusCount = {
            projectsTotal: 0,
            projectsInProgress: 0,
            projectsUnderReview: 0,
            projectsRiskAssessment: 0,
            projectsCompleted: 0,
            projectsRiskTreatment: 0,
            projectsInfoNeeded: 0,
            projectsNotStarted: 0,
        };

        _.each(projects, (project: IProject): void => {
            switch (project.StateDesc) {
                case AssessmentStates.NotStarted:
                    status.projectsNotStarted++;
                    break;
                case AssessmentStates.InProgress:
                    status.projectsInProgress++;
                    break;
                case AssessmentStates.UnderReview:
                    status.projectsUnderReview++;
                    break;
                case AssessmentStates.RiskAssessment:
                    status.projectsRiskAssessment++;
                    break;
                case AssessmentStates.RiskTreatment:
                    status.projectsRiskTreatment++;
                    break;
                case AssessmentStates.Completed:
                    status.projectsCompleted++;
                    break;
                case AssessmentStates.InfoNeeded:
                    status.projectsInfoNeeded++;
                    break;
                default:
                    break;
            }
            status.projectsTotal++;
        });
        return status;
    }

    public getProjectFilterStatus(user: IUser): string | null {
        if (!this.Settings.getIsSelfServiceEnabled()) {
            return "needs attention";  // no translations
        }
        return this.Permissions.canShow("SelfServiceExecute") ? null : "needs attention";
    }

    public computeProjectProgress(answered: number, outstanding: number): number {
        return Math.ceil((answered / outstanding) * 100);
    }

    public addMissingJustifications(project: IProject, outstanding: number): number {
        let justificationCount = 0;
        _.each(project.Sections, (section: ISection) => {
            _.each(section.Questions, (question: IQuestion) => {
                if (question.AllowJustification && question.RequireJustification && !question.Justification) {
                    justificationCount++;
                }
            });
        });
        return justificationCount + outstanding;
    }

    public divideProjectByLocked(project: IProject): any {
        const phases: any[] = [];
        const sectionsObjectByTemplate: any = _(project.Sections)
            .map(this.Sections.setWelcomeSection)
            .map(this.Sections.setDefaultWelcomeText)
            .groupBy("TemplateName")
            .value();
        const lockedPhases: any[] = this.Sections.buildLockedPhases(sectionsObjectByTemplate);
        const unlockedPhases: any[] = this.Sections.buildUnlockedPhases(sectionsObjectByTemplate);

        if (lockedPhases.length) {
            _.each(lockedPhases, (phase: any): void => {
                phase.TemplateName = this.Sections.setPhaseTemplateName(phase.Sections[0], project);
                phases.push(phase);
            });
        }

        if (unlockedPhases.length) {
            _.each(unlockedPhases, (phase: any): void => {
                phase.TemplateName = this.Sections.setPhaseTemplateName(phase.Sections[0], project);
                phases.push(phase);
            });
        }

        return this.Sections.setTotalIndices(phases);
    }

    public isProjectReviewer(project: IProject): boolean {
        const currentUserId: string = getCurrentUser(this.store.getState()).Id;
        return currentUserId === project.ReviewerId;
    }

    public isProjectLead(project: IProject): boolean {
        const currentUserId: string = getCurrentUser(this.store.getState()).Id;
        return currentUserId === project.LeadId;
    }

    public isProjectReviewerOrLead(project: IProject): boolean {
        const currentUserId: string = getCurrentUser(this.store.getState()).Id;
        return currentUserId === project.ReviewerId || currentUserId === project.LeadId;
    }

    public composeDuplicateProjectContract(projectForm: IProjectForm, projectSource: IProject): IDuplicateProjectContract {
        return {
            Name: projectForm.projectName,
            Description: projectForm.comments,
            OrgGroupId: projectForm.orgGroupId,
            LeadId: projectForm.respondentId,
            Deadline: projectForm.deadline,
            ReminderDays: projectForm.reminderDays,
            Tags: null,  // TODO: include tagging
            ProjectId: projectSource.Id,
            ProjectVersion: projectSource.Version,
            TemplateType: projectSource.TemplateId,
            SectionAssignments: projectForm.sectionAssignments,
        };
    }

    public setDuplicateProjectForm(project: IProject, settings: IProjectSettings): IProjectForm {
        const currentUserId: string = getCurrentUser(this.store.getState()).Id;
        const hasMultipleRespondentSettingOn: boolean = settings.SectionAssignment;
        const hasMoreThanOneSection: boolean = project.Sections.length > 1;
        const isNotDataMappingTemplateType: boolean = project.TemplateType !== this.ENUMS.TemplateTypes.Datamapping;
        const hasCrossSectionSkipCondition: boolean = project.hasCrossSectionConditions;

        return {
            sourceAssessment: project.Name,
            projectName: project.Name,
            comments: "",
            hasReminder: false,
            reminderDays: project.ReminderDays || 1,
            deadline: null,
            minDate: new Date(),
            formName: this.translations.duplicateProject,
            orgGroupId: project.OrgGroupId || null,
            respondentId: currentUserId,
            allowMultipleRespondent: hasMultipleRespondentSettingOn && hasMoreThanOneSection && isNotDataMappingTemplateType && !hasCrossSectionSkipCondition,
            isMultiRespondent: false,
            isShowingDetails: false,
            isValidReminder: true,
            isRespondentValid: false,
            sectionAssignments: this.assignments.composeSectionAssignments(project),
        };
    }

    public formatCreatedDate(project: IProject): IProject {
        const timezone: string = moment.tz.guess();
        const date: Moment = moment.utc(project.CreatedDT);
        project.localeCreatedDT = date.tz(timezone).format("L");
        project.CreatedDTtooltip = date.tz(timezone).format("lll z");
        return project;
    }

    public formatLocaleDeadline(project: IProject): IProject {
        if (!project.Deadline) return project;

        const timezone: string = moment.tz.guess();
        const deadline: Moment = moment.utc(project.Deadline);
        project.localeDeadline = deadline.tz(timezone).format("L");
        project.Deadlinetooltip = deadline.tz(timezone).format("lll z");
        return project;
    }

    public setProjectStateNameAndLabels = (project: IProject): IProject => {
        switch (project.StateDesc) {
            case AssessmentStates.NotStarted:
                project.StateLabel = "default";
                project.StateName = this.$rootScope.t(AssessmentStateKeys.NotStarted);
                return project;
            case AssessmentStates.InProgress:
                project.StateLabel = "in-progress";
                project.StateName = this.$rootScope.t(AssessmentStateKeys.InProgress);
                return project;
            case AssessmentStates.UnderReview:
                project.StateLabel = "under-review";
                project.StateName = this.$rootScope.t(AssessmentStateKeys.UnderReview);
                return project;
            case AssessmentStates.InfoNeeded:
                project.StateLabel = "needs-info";
                project.StateName = this.$rootScope.t(AssessmentStateKeys.InfoNeeded);
                return project;
            case AssessmentStates.RiskAssessment:
                project.StateLabel = "risk-assessment";
                project.StateName = this.$rootScope.t(AssessmentStateKeys.RiskAssessment);
                return project;
            case AssessmentStates.RiskTreatment:
                project.StateLabel = "risk-treatment";
                project.StateName = this.$rootScope.t(AssessmentStateKeys.RiskTreatment);
                return project;
            case AssessmentStates.Completed:
                project.StateLabel = "completed";
                project.StateName = this.$rootScope.t(AssessmentStateKeys.Completed);
                return project;
            default:
                project.StateLabel = "default";
                project.StateName = project.StateDesc;
                return project;
        }
    }

    public setProjectIsShared(project: IProject): IProject {
        project.isShared = _.some(project.Assignments, { Type: AssignmentTypes.ProjectViewer });
        return project;
    }

    public setProjectRiskStates = (project: IProject): IProject => {
        switch (project.ProjectRisk) {
            case RiskLevels.Low:
                project.riskClass = "fa fa-caret-down risk-low";
                project.riskWord = this.$rootScope.t("Low");
                return project;
            case RiskLevels.Medium:
                project.riskClass = "fa fa-caret-up risk-medium";
                project.riskWord = this.$rootScope.t("Medium");
                return project;
            case RiskLevels.High:
                project.riskClass = "fa fa-caret-up risk-high";
                project.riskWord = this.$rootScope.t("High");
                return project;
            case RiskLevels.VeryHigh:
                project.riskClass = "fa fa-caret-up risk-very-high";
                project.riskWord = this.$rootScope.t("VeryHigh");
                return project;
            default:
                project.riskClass = "";
                project.riskWord = null;
                project.ProjectRisk = 0;
                return project;
        }
    }

    public setProjectCanBeDuplicated = (project: IProject): IProject => {
        const projectIsNotArchived: boolean = project.TemplateState !== "Archived";
        const projectIsNotThreshold: boolean = !project.IsLinked;
        project.canBeDuplicated = projectIsNotArchived && projectIsNotThreshold;
        return project;
    }

    public getInventoryProjects(recordId: string): ng.IPromise<IProtocolResponse<IProjectPageResponse[]>> {
        const config: IProtocolConfig = this.OneProtocol.config("GET", `/project/inventory/${recordId}`);
        const messages: IProtocolMessages = { Error: { custom: this.translations.errorRetrievingProject } };
        return this.OneProtocol.http(config, messages);
    }

    public removeInventoryProject(recordId: string, projectId: string): ng.IPromise<IProtocolResponse<null>> {
        const config: IProtocolConfig = this.OneProtocol.config("DELETE", `/project/${projectId}/inventory/${recordId}`);
        const messages: IProtocolMessages = { Error: { custom: this.translations.errorRemovingProjectLink } };
        return this.OneProtocol.http(config, messages);
    }

    private buildParams(templateType: string): string[] {
        const defaultArray: string[] = [this.ENUMS.TemplateTypes.Custom, this.ENUMS.TemplateTypes.SelfService];
        switch (templateType) {
            case this.ENUMS.TemplateTypes.Custom:
            case this.ENUMS.TemplateTypes.SelfService:
                return defaultArray;
            default:
                return templateType ? [templateType] : defaultArray;
        }
    }
}
