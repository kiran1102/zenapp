import * as _ from "lodash";
import { ProtocolService } from "modules/core/services/protocol.service";
import QuestionService from "oneServices/questions.service";

import { EmptyGuid } from "constants/empty-guid.constant";
import { AssessmentWelcomeSectionText } from "constants/assessment.constants";

import { IProtocolPacket, IProtocolMessages, IProtocolConfig } from "interfaces/protocol.interface";
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import { ISection, IPhase } from "interfaces/section.interface";
import { IQuestion } from "interfaces/question.interface";
import { IProject } from "interfaces/project.interface";
import { Permissions } from "modules/shared/services/helper/permissions.service";

export default class Sections {

    static $inject: string[] = [
        "$rootScope",
        "OneProtocol",
        "PRINCIPAL",
        "Questions",
        "Permissions",
    ];

    private translations = {
        couldNotUpdateSection: this.$rootScope.t("CouldNotUpdateSection"),
        couldNotDeleteTheSection: this.$rootScope.t("CouldNotDeleteTheSection"),
        couldNotAddQuestion: this.$rootScope.t("CouldNotAddQuestion"),
        couldNotUpdateQuestion: this.$rootScope.t("CouldNotUpdateQuestion"),
        couldNotGetQuestion: this.$rootScope.t("CouldNotGetQuestion"),
        couldNotReorderQuestion: this.$rootScope.t("CouldNotReorderQuestionOnTheSection"),
        couldNotRemoveQuestionFromSection: this.$rootScope.t("CouldNotRemoveQuestionFromSection"),
        error: this.$rootScope.t("Error"),
        unassignedSections: this.$rootScope.t("UnassignedSections"),
        assignedSections: this.$rootScope.t("AssignedSections"),
        other: this.$rootScope.t("Other"),
        needsAttention: this.$rootScope.t("NeedsAttention"),
    };

    constructor(
        private $rootScope: IExtendedRootScopeService,
        private OneProtocol: ProtocolService,
        private PRINCIPAL: any,
        private Questions: QuestionService,
        private permissions: Permissions,
    ) {}

    // Updates a Section
    public putSection(section): ng.IPromise<IProtocolPacket> {
        const config: IProtocolConfig = this.OneProtocol.config("PUT", `/section/put`, [], section);
        const messages: IProtocolMessages = {Error: {custom: this.translations.couldNotUpdateSection}};
        return this.OneProtocol.http(config, messages);
    }

    // Deletes a Section
    public deleteSection(id: string, version: string): ng.IPromise<IProtocolPacket> {
        const _version: string = version ? version : "";
        const config: IProtocolConfig = this.OneProtocol.config("DELETE", `/section/delete/${id}/${_version}`);
        const messages: IProtocolMessages = {Error: {custom: this.translations.couldNotDeleteTheSection}};
        return this.OneProtocol.http(config, messages);
    }

    // Adds a Question to a Section
    public addQuestion = (question: any): ng.IPromise<IProtocolPacket> => {
        const config: IProtocolConfig = this.OneProtocol.config("POST", `/section/addquestion`, [], question);
        const messages: IProtocolMessages = {Error: {custom: this.translations.couldNotAddQuestion}};
        return this.OneProtocol.http(config, messages);
    }

    // Update a Question on a Section
    public updateQuestion(question: any): ng.IPromise<IProtocolPacket> {
        const config: IProtocolConfig = this.OneProtocol.config("PUT", `/section/updatequestion`, [], question);
        const messages: IProtocolMessages = {Error: {custom: this.translations.couldNotUpdateQuestion}};
        return this.OneProtocol.http(config, messages);
    }

    // Gets a Question from a Section
    public getQuestion = (questionId: string, version: string): ng.IPromise<any> => {
        const config: IProtocolConfig = this.OneProtocol.config("GET", `/section/getquestion/${questionId}/${version}`);
        const messages: IProtocolMessages = {Error: {custom: this.translations.couldNotGetQuestion}};
        return this.OneProtocol.http(config, messages);
    }

    // Reorders a Question on a Section
    public reorderQuestion(data: any): ng.IPromise<IProtocolPacket> {
        const config: IProtocolConfig = this.OneProtocol.config("PUT", `/section/reorderquestion`, [], data);
        const messages: IProtocolMessages = {Error: {custom: this.translations.couldNotReorderQuestion}};
        return this.OneProtocol.http(config, messages);
    }

    // Removes a Question from a Section
    public deleteQuestion(questionId: string, version: string): ng.IPromise<IProtocolPacket> {
        const _version: string = version ? version : "";
        const config: IProtocolConfig = this.OneProtocol.config("DELETE", `/section/deletequestion/${questionId}/${_version}`);
        const messages: IProtocolMessages = {Error: {custom: this.translations.couldNotRemoveQuestionFromSection}};
        return this.OneProtocol.http(config, messages);
    }

    public excludeWelcomeSection = (section: ISection): boolean => {
        return section.Id !== EmptyGuid;
    }

    public currentUserIsAssignedToSection = (section: ISection): boolean => {
        return this.PRINCIPAL.getCurrentUserId() === section.Assignment.AssigneeId;
    }

    public getNoAdditionalChangesAllSections = (sections: ISection[]): ISection[] => {
        return _(sections)
            .filter(this.excludeWelcomeSection)
            .value();
    }

    public getNoAdditionalChangesAssignedSections = (sections: ISection[]): ISection[] => {
        return _(sections)
            .filter(this.excludeWelcomeSection)
            .filter(this.currentUserIsAssignedToSection)
            .value();
    }

    public getQuestionsWithoutChanges = (section: ISection): IQuestion[] => {
        return _(section.Questions)
            .filter(this.Questions.excludeNewQuestion)
            .filter(this.Questions.excludeSkippedQuestion)
            .filter(this.Questions.excludeChangedQuestion)
            .value();
    }

    public getSectionsCountWithoutChanges = (sections: ISection[]): number => {
        return _(sections)
        .map(this.getQuestionsWithoutChanges)
        .filter((questions: IQuestion[]): boolean => Boolean(questions.length))
        .size();
    }

    public getUpdatedSectionsOnQuestionNoChange = (sections: ISection[], questionId: string): ISection[] => {
        return _.map(sections, (section: ISection): any => {
            section.Questions = this.Questions.updateQuestionsNoChange(section.Questions, questionId);
            return section;
        });
    }

    public setDefaultWelcomeText = (section: ISection): ISection => {
        if (this.welcomeSectionHasContent(section)) {
            section.Questions[0].Content = AssessmentWelcomeSectionText;
        }
        return section;
    }

    public setWelcomeSection = (section: ISection): ISection => {
        section.isWelcomeSection = section.Id === EmptyGuid;
        return section;
    }

    public includeLockedSection = (section: ISection): boolean => {
        return section.IsLocked;
    }

    public excludeLockedSection = (section: ISection): boolean => {
        return !section.IsLocked;
    }

    public isWelcomeSection = (section: ISection): boolean => {
        return section.isWelcomeSection;
    }

    public setPhaseTemplateName = (section: ISection, project: IProject): string => {
        const projectAssignmentIsNull: boolean = project.Assignment === null;
        if (!projectAssignmentIsNull || this.permissions.canShow("ProjectsFullAccess")) {
            return section.TemplateName;
        } else {
            return section.IsLocked ? this.translations.other : this.translations.needsAttention;
        }
    }

    public setTotalIndices = (phases: IPhase[]): IPhase[] => {
        let index = 0;
        return _.map(phases, (phase: IPhase): any => {
            phase.Sections = _.map(phase.Sections, (section: ISection): any => {
                section.totalIndex = index++;
                return section;
            });
            return phase;
        });
    }

    public setTotalSections = (phases: IPhase[]): number => {
        const lastPhase: any = _.last(phases);
        const lastSection: any = _.last(lastPhase.Sections);
        return lastSection.totalIndex + 1;
    }

    public buildLockedPhases = (obj: any): IPhase[] => {
        const lockedPhases: IPhase[] = [];
        let i = 1;
        _.forIn(obj, (sections: ISection[]) => {
            const phase: IPhase = {
                Id: `locked-${i}`,
                locked: true,
                expanded: false,
                Sections: _(sections)
                    .filter(this.includeLockedSection)
                    .value(),
            };
            if (phase.Sections.length) { lockedPhases.push(phase); }
            i++;
        });
        return lockedPhases;
    }

    public buildUnlockedPhases = (obj: any): IPhase[] => {
        const unlockedPhases: IPhase[] = [];
        let i = 1;
        _.forIn(obj, (sections: ISection[]) => {
            const phase: IPhase = {
                Id: `unlocked-${i}`,
                locked: false,
                expanded: true,
                Sections: _(sections)
                    .filter(this.excludeLockedSection)
                    .value(),
            };
            if (phase.Sections.length) { unlockedPhases.push(phase); }
            i++;
        });
        return unlockedPhases;
    }

    public phaseIndexBackward(selectedIndex: number): number {
        if (selectedIndex > 0) {
            return --selectedIndex;
        }
    }

    public phaseIndexForward(len: number, selectedIndex: number): number {
        if (selectedIndex < len) {
            return ++selectedIndex;
        }
    }

    private welcomeSectionHasContent = (section: ISection): boolean => {
        return section.Questions && !section.Questions[0].Content;
    }

}
