import { ProtocolService } from "modules/core/services/protocol.service";
import { IProtocolPacket, IProtocolMessages, IProtocolConfig } from "interfaces/protocol.interface";
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import { IQuestionSerialized } from "interfaces/question.interface";
import { IRiskSerialized } from "interfaces/risk.interface";

export default class ProjectQuestionService {
    static $inject: string[] = ["$rootScope", "OneProtocol"];

    private HTTP: any;
    private translations: any;

    constructor(
        $rootScope: IExtendedRootScopeService,
        readonly OneProtocol: ProtocolService,
        ENUMS: any,
    ) {
        this.translations = {
            couldNotUpdateQuestion: $rootScope.t("CouldNotUpdateQuestion"),
            couldNotRetrieveQuestion: $rootScope.t("CouldNotRetrieveQuestion"),
            couldNotUpdateRisk: $rootScope.t("CouldNotUpdateRisk"),
            error: $rootScope.t("Error"),
        };
    }

    public updateState(questionState: IQuestionSerialized): ng.IPromise<IProtocolPacket> {
        const config: IProtocolConfig = this.OneProtocol.config("PUT", `/projectquestion/updatestate`, [], questionState);
        const messages: IProtocolMessages = {Error: {custom: this.translations.couldNotUpdateQuestion}};
        return this.OneProtocol.http(config, messages);
    }

    public noAdditionalChanges(id: string, version: number): ng.IPromise<IProtocolPacket> {
        const params: any = { id, version };
        const config: IProtocolConfig = this.OneProtocol.config("PUT", `/project/updatehasversionresponsechange/`, params);
        const messages: IProtocolMessages = {Error: {custom: this.translations.couldNotUpdateQuestion}};
        return this.OneProtocol.http(config, messages);
    }

    public getState(questionId: string): ng.IPromise<IProtocolPacket> {
        const config: IProtocolConfig = this.OneProtocol.config("GET", `/projectquestion/get/${questionId}`);
        const messages: IProtocolMessages = {Error: {custom: this.translations.couldNotRetrieveQuestion}};
        return this.OneProtocol.http(config, messages);
    }

    public updateRisk(riskPayload: IRiskSerialized): ng.IPromise<IProtocolPacket> {
        const config: IProtocolConfig = this.OneProtocol.config("POST", `/risk/addquestion`, [], riskPayload);
        const messages: IProtocolMessages = {Error: {custom: this.translations.couldNotUpdateRisk}};
        return this.OneProtocol.http(config, messages);
    }

    public getHistory(id: string): ng.IPromise<IProtocolPacket> {
        const config: IProtocolConfig = this.OneProtocol.config("GET", `/projectquestion/gethistory/${id}`);
        const messages: IProtocolMessages = {Error: {custom: this.translations.errorRetrievingQuestion}};
        return this.OneProtocol.http(config, messages);
    }
}
