// Angular
import { Injectable } from "@angular/core";

// Services
import { ProtocolService } from "modules/core/services/protocol.service";

// Interfaces
import { IProtocolResponse, IProtocolConfig, IProtocolMessages } from "interfaces/protocol.interface";
import { IStringMap } from "interfaces/generic.interface";
import { ISupportTicket, ISupportUpgrade } from "interfaces/support.interface";

@Injectable()
export class SupportService {

    constructor(
        private oneProtocol: ProtocolService,
    ) {}

    upgrade(upgradeDetails: ISupportUpgrade): ng.IPromise<IProtocolResponse<void>> {
        return this.basicApiRequest("PUT", "/support/upgrade", null, upgradeDetails);
    }

    ticket(ticketDetails: ISupportTicket): ng.IPromise<IProtocolResponse<void>> {
        return this.basicApiRequest("PUT", "/support/ticket", null, ticketDetails);
    }

    basicApiRequest(method: string, endpoint: string, params: IStringMap<any>, data: IStringMap<any>): ng.IPromise<IProtocolResponse<void>> {
        const config: IProtocolConfig = this.oneProtocol.config(method, endpoint, params, data);
        const messages: IProtocolMessages = { Error: { Hide: true } };
        return this.oneProtocol.http(config, messages);
    }
}
