// External
import invariant from "invariant";
import { compose } from "lodash/fp";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";

// Services
import { Permissions } from "modules/shared/services/helper/permissions.service";
import { RiskAPIService } from "oneServices/api/risk-api.service";

// Redux
import { getTempRiskById } from "oneRedux/reducers/risk.reducer";
import { getRiskSettings } from "oneRedux/reducers/setting.reducer";

// Interfaces
import { IDropdownOption } from "interfaces/one-dropdown.interface";
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import { IStore } from "interfaces/redux.interface";
import {
    IRisk,
    IRiskDetails,
    IRiskStatusFilters,
    IHeatMapTile,
    IRiskHeatMapLabels,
    IRiskSummaryPermissionConfig,
} from "interfaces/risk.interface";
import { IRiskLevel } from "interfaces/risks-settings.interface";

// Enums
import { RiskStates } from "enums/risk-states.enum";
import { RiskLevels, RiskLabelBgClasses } from "enums/risk.enum";
import { RiskV2Levels } from "enums/riskV2.enum";

export default class RiskBusinessLogic {
    static $inject: string[] = ["store", "$rootScope", "$localStorage", "Permissions", "RiskAPI"];

    private ctrlInstance: any;

    private riskStatesMap: any;

    constructor(
        private store: IStore,
        private $rootScope: IExtendedRootScopeService,
        private $localStorage: any,
        private permissions: Permissions,
        private riskApi: RiskAPIService,
    ) {}

    public allRisksAreReducedOrRetained(riskModels: IRisk[]): boolean {
        const { Reduced, Retained } = RiskStates;
        const riskLength = riskModels.length;
        for (let i = 0; i < riskLength; i++) {
            const riskState = riskModels[i].State;
            if (riskState !== Reduced
                && riskState !== Retained) {
                return false;
            }
        }
        return true;
    }

    public allRisksHaveRecommendation(riskModels: IRisk[]): boolean {
        const { Analysis } = RiskStates;
        const riskLength = riskModels.length;
        for (let i = 0; i < riskLength; i++) {
            const riskModel = riskModels[i];
            if (riskModel.State !== Analysis) {
                return false;
            }
        }
        return true;
    }

    public getRiskSortByOptions(): IDropdownOption[] {
        return [
            { text: this.$rootScope.t("ClosestDeadline"), value: { deadline: "asc" } },
            { text: this.$rootScope.t("NeedsAttention"), value: "pendingAssessment" },
            { text: this.$rootScope.t("RiskLevelHigh"), value: { level: "desc" } },
            { text: this.$rootScope.t("RiskLevelLow"), value: { level: "asc" } },
        ];
    }

    // TODO uncomment when group by option is enabled
    // public getRiskGroupByOptions(): IDropdownOption[] {
    //     return [
    //         { text: this.$rootScope.t("All") },
    //         { text: this.$rootScope.t("RiskLevel") },
    //         { text: this.$rootScope.t("RiskOwner") },
    //         { text: this.$rootScope.t("Status") },
    //     ];
    // }

    public getRiskModalTranslations(riskId: string, allowHeatmap: boolean = true): any {
        const isHeatMapEnabled: boolean = getRiskSettings(this.store.getState()).Enabled;
        const riskModel: IRisk | IRiskDetails = getTempRiskById(riskId)(this.store.getState());
        const isNewRisk: boolean = riskId.indexOf("temp") > -1;
        invariant(isHeatMapEnabled !== undefined, "HeatMap setting cannot be undefined when building translations.");
        invariant(Boolean(riskModel), "Risk Model must be defined when building translations.");

        return {
            cancel: this.$rootScope.t("Cancel"),
            deadline: this.$rootScope.t("Deadline"),
            description: this.$rootScope.t("Summary"),
            groupBy: this.$rootScope.t("GroupBy"),
            mitigation: this.$rootScope.t("RemediationProposal"),
            modalTitle: isNewRisk ? this.$rootScope.t("FlagRisk") : this.$rootScope.t("EditRisk"),
            owner: this.$rootScope.t("Owner"),
            ownerInputPlaceholder: this.$rootScope.t("PleaseSelectOwner"),
            recommendation: this.$rootScope.customTerms.Recommendation,
            requestedException: this.$rootScope.t("ExceptionRequest"),
            riskLevel: isHeatMapEnabled && allowHeatmap ? this.$rootScope.t("CalculatedRiskLevel") : this.$rootScope.t("RiskLevel"),
            save: this.$rootScope.t("Save"),
            sortBy: this.$rootScope.t("SortBy"),
            submit: isNewRisk ? this.$rootScope.t("Add") : this.$rootScope.t("Update"),
        };
    }

    public getRiskSummaryPermissions(): IRiskSummaryPermissionConfig {
        return {
            heatmap: this.permissions.canShow("SettingsRiskHeatmap") && getRiskSettings(this.store.getState()).Enabled,
            canViewProjectList: this.permissions.canShow("ProjectsList"),
        };
    }

    public getRiskSummaryTranslations(): any {
        return {
            associatedQuestion: this.$rootScope.t("AssociatedQuestion"),
            backToProject: this.$rootScope.t("BackToProject", { Project: this.$rootScope.customTerms.Project }),
            cancel: this.$rootScope.t("Cancel"),
            close: this.$rootScope.t("Close").toUpperCase(),
            deadline: this.$rootScope.t("Deadline"),
            pendingAssessment: this.$rootScope.t("PendingAssessment"),
            noDeadline: this.$rootScope.t("NoDeadline"),
            noRiskOwner: this.$rootScope.t("NoRiskOwner"),
            open: this.$rootScope.t("Tasks.Open").toUpperCase(),
            pendingResponseFromRiskOwner: this.$rootScope.t("PendingResponseFromRiskOwner"),
            project: this.$rootScope.t("Project"),
            projects: this.$rootScope.t("Projects"),
            recommendation: this.$rootScope.customTerms.Recommendation,
            riskOwner: this.$rootScope.t("RiskOwner"),
            riskTreatment: this.$rootScope.t("RiskTreatment"),
            save: this.$rootScope.t("Save"),
            selected: this.$rootScope.t("Selected"),
            status: this.$rootScope.t("Status"),
            summary: this.$rootScope.t("Summary"),
            totalRisks: this.$rootScope.t("TotalRisks"),
            submit: this.$rootScope.t("Submit"),
            recommendationSent: this.$rootScope.t("RecommendationSent", { Recommendation: this.$rootScope.customTerms.Recommendation }),
            recommendationSentToRespondent: this.$rootScope.t("RecommendationsSentToRespondent", { Recommendations: this.$rootScope.customTerms.Recommendations }),
            mitigationPlanSent: this.$rootScope.t("MitigationPlanSent"),
            mitigationPlanSentToApprover: this.$rootScope.t("MitigationPlanSentToApprover"),
            couldNotGetAssociatedQuestion: this.$rootScope.t("CouldNotGetTheAssociatedQuestion"),
            youHaveNoRisks: this.$rootScope.t("YouHaveNoRisks"),
            doYouWantToCompleteProject: this.$rootScope.t("SureToEndAnalysisAndCompleteProject"),
            approved: this.$rootScope.t("Approved"),
            approvedMessage: this.$rootScope.t("YouApprovedProjectEmailSentToTeamDuplicate", { ProjectTerm: this.$rootScope.customTerms.Project }),
            unassigned: this.$rootScope.t("Unassigned"),
        };
    }

    public buildRiskTileMap(xAxisLen: number, yAxisLen: number): { axisLabels: IRiskHeatMapLabels, tileMap: IHeatMapTile[][] } {
        const heatMapSettings: any = getRiskSettings(this.store.getState());
        invariant(heatMapSettings !== undefined && heatMapSettings !== null, "HeatMap setting cannot be undefined.");

        const xAxis = 0;
        const yAxis = 1;

        const xAxisLabel: string = heatMapSettings.GraphSettings.Axes[xAxis].Label;
        const xAxisPoints: any[] = heatMapSettings.GraphSettings.Axes[xAxis].Points;

        const yAxisLabel: string = heatMapSettings.GraphSettings.Axes[yAxis].Label;
        const yAxisPoints: any[] = heatMapSettings.GraphSettings.Axes[yAxis].Points;

        const getOnlyLabels = (points: any[]): string[] => points.map((point: any): string => point.Label);
        const appendLabel = (labelToAppend: string) => (points: any[]): string[] => points.map((point: string): string => `${point} ${labelToAppend}`); // might be a problem for German translation

        const axisLabels: IRiskHeatMapLabels = {
            xAxis: compose(appendLabel(xAxisLabel), getOnlyLabels)(xAxisPoints),
            xAxisLabel,
            yAxis: compose(appendLabel(yAxisLabel), getOnlyLabels)(yAxisPoints),
            yAxisLabel,
        };

        const srcArr: any[] = heatMapSettings.RiskMap;
        invariant(xAxisLen * yAxisLen <= srcArr.length, "Specified HeatMap parameters cannot exceed HeatMap tile length.");

        const riskHtmlClasses = RiskLabelBgClasses;
        const tileMap: IHeatMapTile[][] = [];
        let srcInd = 0;

        const xAxisKey = "Axis1Value";
        const yAxisKey = "Axis2Value";

        for (let y = 0; y < yAxisLen; y++) {
            tileMap[y] = [];
            for (let x = 0; x < xAxisLen; x++) {
                tileMap[y][x] = {
                    xValue: srcArr[srcInd][xAxisKey],
                    yValue: srcArr[srcInd][yAxisKey],
                    riskLevel: srcArr[srcInd].RiskLevel,
                    htmlClass: riskHtmlClasses[srcArr[srcInd].RiskLevel],
                };
                srcInd++;
            }
        }

        return { axisLabels, tileMap };
    }

    public buildRiskStatesMap(recommendation: string): any {
        return {
            10: this.$rootScope.t("RiskIdentified"),
            20: this.$rootScope.t("RecommendationAdded", { Recommendation: recommendation }),
            30: this.$rootScope.t("RemediationProposed"),
            40: this.$rootScope.t("ExceptionRequested"),
            45: this.$rootScope.t("Reduced"),
            55: this.$rootScope.t("Retained"),
        };
    }

    public buildRiskStatesColorMap(): any {
        return {
            10: "color-accent-blue",
            20: "color-green",
            30: "color-accent-blue",
            40: "color-accent-blue",
            50: "color-accent-blue",
        };
    }

    public getRiskStatusLabelColor(state: number): string {
        return this.buildRiskStatesColorMap()[state];
    }

    public buildRiskLevelClassMap(): any {
        return {
            0: "risk-summary__risk-level-symbol risk-none--bg border",
            1: "risk-summary__risk-level-symbol risk-low--bg",
            2: "risk-summary__risk-level-symbol risk-medium--bg",
            3: "risk-summary__risk-level-symbol risk-high--bg",
            4: "risk-summary__risk-level-symbol risk-very-high--bg",
        };
    }

    public getRiskLevelClass(level: number): string {
        return this.buildRiskLevelClassMap()[level];
    }

    public getRiskLevelOptions(): Observable<IDropdownOption[]> {
        return this.riskApi.getRiskLevel().pipe(map((riskLevels: IRiskLevel[]) => {
            return [
                {
                    icon: "risk-summary__risk-level-symbol risk-none--bg border",
                    text: this.$rootScope.t("None"),
                    value: RiskLevels.None,
                },
                {
                    icon: "risk-summary__risk-level-symbol risk-low--bg",
                    text: this.$rootScope.t("Low"),
                    value: riskLevels.find((level: IRiskLevel) => level.name === RiskV2Levels[RiskV2Levels.LOW]).id,
                },
                {
                    icon: "risk-summary__risk-level-symbol risk-medium--bg",
                    text: this.$rootScope.t("Medium"),
                    value: riskLevels.find((level: IRiskLevel) => level.name === RiskV2Levels[RiskV2Levels.MEDIUM]).id,
                },
                {
                    icon: "risk-summary__risk-level-symbol risk-high--bg",
                    text: this.$rootScope.t("High"),
                    value: riskLevels.find((level: IRiskLevel) => level.name === RiskV2Levels[RiskV2Levels.HIGH]).id,
                },
                {
                    icon: "risk-summary__risk-level-symbol risk-very-high--bg",
                    text: this.$rootScope.t("VeryHigh"),
                    value: riskLevels.find((level: IRiskLevel) => level.name === RiskV2Levels[RiskV2Levels.VERY_HIGH]).id,
                },
            ];
        }));
    }

    public getRiskStateFilters(): IRiskStatusFilters {
        if (!this.riskStatesMap) {
            this.riskStatesMap = this.buildRiskStatesMap(this.$rootScope.customTerms.Recommendation);
        }
        let filters: any;

        if (this.$localStorage.riskStateFilters) {
            const localFilters = this.$localStorage.riskStateFilters.filters;

            filters = localFilters.map((item: any): any => {
                return {
                    Filtered: item.Filtered,
                    value: item.value,
                    label: this.riskStatesMap[item.lookupKey],
                    lookupKey: parseInt(item.lookupKey, 10),
                };
            });

            return {
                filters,
                allStatesEnabled: this.$localStorage.riskStateFilters.allStatesEnabled,
            };
        }
        const lookupKeys = Object.keys(this.riskStatesMap);

        filters = lookupKeys.map((key: string): any => {
            return {
                Filtered: true,
                value: RiskStates[key],
                label: this.riskStatesMap[key],
                lookupKey: parseInt(key, 10),
            };
        });

        return {
            filters,
            allStatesEnabled: true,
        };
    }

    public saveFiltersLocally(riskStateFilters: any): void {
        const riskFilters = riskStateFilters.filters;

        const filters = riskFilters.map((item: any): any => {
            return {
                Filtered: item.Filtered,
                value: item.value,
                lookupKey: parseInt(item.lookupKey, 10),
            };
        });
        this.$localStorage.riskStateFilters = {
            filters,
            allStatesEnabled: riskStateFilters.allStatesEnabled,
        };
    }
}
