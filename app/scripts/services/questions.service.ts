import { isNil, map, isArray, isUndefined, isFunction, findIndex, isString, debounce, each } from "lodash";
import Utilities from "Utilities";
import { ProtocolService } from "modules/core/services/protocol.service";
import { IProtocolPacket, IProtocolMessages, IProtocolConfig } from "interfaces/protocol.interface";
import { IQuestion, ICreateQuestionResponseContract, IQuestionResponse } from "interfaces/question.interface";
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import { QuestionResponseTypes, QuestionTypes, QuestionStates } from "enums/assessment-question.enum";
import { IStore } from "interfaces/redux.interface";
import { AssessmentStates } from "constants/assessment.constants";
import { ProjectModes } from "enums/assessment.enum";
import { getCurrentUser } from "oneRedux/reducers/user.reducer";

export default class QuestionService {
    static $inject: string[] = ["store", "$rootScope", "OneProtocol", "ENUMS", "$q", "Answer", "Response", "GenFactory", "ProjectQuestions"];

    private questionTypes: any;
    private responseTypes: any;
    private projectStates: any;
    private questionStates: any;
    private translations: any;
    private ctrlInstance: any;

    private saveQuestionResponseDebounced = debounce((project: any, question: IQuestion): any => {
        const { InProgress, InfoNeeded } = AssessmentStates;
        const currentCtrl: any = this.getCtrlInstance();
        const createResponseContract: ICreateQuestionResponseContract = {
            HasVersionResponseChanged: true,
            QuestionId: question.Id,
            Responses: this.composeResponses(question),
        };
        return this.Response.create(createResponseContract).then((res: IProtocolPacket): undefined | void => {
            if (!res.result) return;
            question.Responses = createResponseContract.Responses;
            question.HasVersionResponseChanged = true;
            question.State = this.changeQuestionStateLabelRefactored(question, question.Responses);

            this.Answer.handleQuestionResponse(currentCtrl, currentCtrl.Dictionary, question, res.data);

            if (project.StateDesc !== InProgress && project.StateDesc !== InfoNeeded) {
                this.getRemainingQuestionsRefactored();
            } else {
                currentCtrl.getProjectProgress(project.Id, project.Version);
            }
        });
    }, 650);

    private addInventoryResponseDebounced = debounce((newResponse: any, question: IQuestion, callback: (IQuestion, string) => void, groupId: string, currentCtrl: any): void => {
        const { InProgress, InfoNeeded } = AssessmentStates;
        this.Response.inventoryResponse(newResponse).then((res: IProtocolPacket): undefined | void => {
            if (!res.result) return;

            question.Responses = newResponse.Responses;
            if (isFunction(callback)) {
                callback(question, groupId);
            }
            question.HasVersionResponseChanged = true;
            question.State = this.changeQuestionStateLabelRefactored(question, question.Responses);
            this.Answer.handleQuestionResponse(currentCtrl, currentCtrl.Dictionary, question, res.data);

            if (currentCtrl.Project.StateDesc !== InProgress && currentCtrl.Project.StateDesc !== InfoNeeded) {
                this.getRemainingQuestionsRefactored();
            } else {
                currentCtrl.getProjectProgress(currentCtrl.Project.Id, currentCtrl.Project.Version);
            }
        });
    }, 650);

    constructor(
        readonly store: IStore,
        readonly $rootScope: IExtendedRootScopeService,
        readonly OneProtocol: ProtocolService,
        readonly ENUMS: any,
        readonly $q: ng.IQService,
        readonly Answer: any,
        readonly Response: any,
        readonly GenFactory: any,
        readonly ProjectQuestions: any) {
        this.questionTypes = ENUMS.QuestionTypes;
        this.responseTypes = ENUMS.ResponseTypes;
        this.projectStates = AssessmentStates;
        this.translations = {
            couldNotRetrieveQuestions: $rootScope.t("CouldNotRetrieveQuestions"),
            couldNotCreateQuestion: $rootScope.t("CouldNotCreateQuestion"),
            couldNotReadQuestion: $rootScope.t("CouldNotReadQuestion"),
            couldNotUpdateQuestion: $rootScope.t("CouldNotUpdateQuestion"),
            couldNotDeleteQuestion: $rootScope.t("CouldNotDeleteQuestion"),
            errorRetrievingOptions: $rootScope.t("ErrorRetrievingOptions"),
            errorTogglingCategory: $rootScope.t("ErrorTogglingCategory"),
        };
    }

    public list(): ng.IPromise<IProtocolPacket> {
        const config: IProtocolConfig = this.OneProtocol.config("GET", `/question`);
        const messages: IProtocolMessages = {Error: {custom: this.translations.couldNotRetrieveQuestions}};
        return this.OneProtocol.http(config, messages);
    }

    public create = (question: IQuestion): ng.IPromise<any> => {
        switch (question.Type) {
            case this.questionTypes.Content:
                return this.addContentQuestion(question);
            case this.questionTypes.Multichoice:
            case this.questionTypes.DataElement:
            case this.questionTypes.Application:
            case this.questionTypes.Location:
            case this.questionTypes.User:
            case this.questionTypes.Provider:
                question.QuestionType = question.Type;
                return this.addMultichoiceQuestion(question);
            case this.questionTypes.YesNo:
                return this.addYesNoQuestion(question);
            case this.questionTypes.DateTime:
                return this.addDateTimeQuestion(question);
            case this.questionTypes.TextBox:
            default:
                return this.addTextBoxQuestion(question);
        }
    }

    public addContentQuestion(question: any): ng.IPromise<IProtocolPacket> {
        const config: IProtocolConfig = this.OneProtocol.config("POST", `/question/addcontent/`, [], question);
        const messages: IProtocolMessages = {Error: {custom: this.translations.couldNotCreateQuestion}};
        return this.OneProtocol.http(config, messages);
    }

    public addMultichoiceQuestion(question: any): ng.IPromise<IProtocolPacket> {
        const config: IProtocolConfig = this.OneProtocol.config("POST", `/question/addmultichoice/`, [], question);
        const messages: IProtocolMessages = {Error: {custom: this.translations.couldNotCreateQuestion}};
        return this.OneProtocol.http(config, messages);
    }

    public addYesNoQuestion(question: any): ng.IPromise<IProtocolPacket> {
        const config: IProtocolConfig = this.OneProtocol.config("POST", `/question/addyesno/`, [], question);
        const messages: IProtocolMessages = {Error: {custom: this.translations.couldNotCreateQuestion}};
        return this.OneProtocol.http(config, messages);
    }

    public addDateTimeQuestion(question: any): ng.IPromise<IProtocolPacket> {
        const config: IProtocolConfig = this.OneProtocol.config("POST", `/question/adddatetime/`, [], question);
        const messages: IProtocolMessages = {Error: {custom: this.translations.couldNotCreateQuestion}};
        return this.OneProtocol.http(config, messages);
    }

    public addTextBoxQuestion(question: any): ng.IPromise<IProtocolPacket> {
        const config: IProtocolConfig = this.OneProtocol.config("POST", `/question/addtextbox/`, [], question);
        const messages: IProtocolMessages = {Error: {custom: this.translations.couldNotCreateQuestion}};
        return this.OneProtocol.http(config, messages);
    }

    public read(id: string): ng.IPromise<IProtocolPacket> {
        const config: IProtocolConfig = this.OneProtocol.config("GET", `/question/${id}`);
        const messages: IProtocolMessages = {Error: {custom: this.translations.couldNotReadQuestion}};
        return this.OneProtocol.http(config, messages);
    }

    public update(question: any): ng.IPromise<any> {
        switch (question.Type) {
            case this.questionTypes.Content:
                return this.updateContentQuestion(question);
            case this.questionTypes.Multichoice:
            case this.questionTypes.DataElement:
            case this.questionTypes.Application:
            case this.questionTypes.Location:
            case this.questionTypes.User:
            case this.questionTypes.Provider:
                question.QuestionType = question.Type;
                return this.updateMultichoiceQuestion(question);
            case this.questionTypes.YesNo:
                return this.updateYesNoQuestion(question);
            case this.questionTypes.DateTime:
                return this.updateDateTimeQuestion(question);
            case this.questionTypes.TextBox:
            default:
                return this.updateTextBoxQuestion(question);
        }
    }

    public updateContentQuestion(question: any): ng.IPromise<IProtocolPacket> {
        const config: IProtocolConfig = this.OneProtocol.config("PUT", `/question/updatecontent/`, [], question);
        const messages: IProtocolMessages = {Error: {custom: this.translations.couldNotUpdateQuestion}};
        return this.OneProtocol.http(config, messages);
    }

    public updateMultichoiceQuestion(question: any): ng.IPromise<IProtocolPacket> {
        const config: IProtocolConfig = this.OneProtocol.config("PUT", `/question/updatemultichoice/`, [], question);
        const messages: IProtocolMessages = {Error: {custom: this.translations.couldNotUpdateQuestion}};
        return this.OneProtocol.http(config, messages);
    }

    public updateYesNoQuestion(question: any): ng.IPromise<IProtocolPacket> {
        const config: IProtocolConfig = this.OneProtocol.config("PUT", `/question/updateyesno/`, [], question);
        const messages: IProtocolMessages = {Error: {custom: this.translations.couldNotUpdateQuestion}};
        return this.OneProtocol.http(config, messages);
    }

    public updateDateTimeQuestion(question: any): ng.IPromise<IProtocolPacket> {
        const config: IProtocolConfig = this.OneProtocol.config("PUT", `/question/updatedatetime/`, [], question);
        const messages: IProtocolMessages = {Error: {custom: this.translations.couldNotUpdateQuestion}};
        return this.OneProtocol.http(config, messages);
    }

    public updateTextBoxQuestion(question: any): ng.IPromise<IProtocolPacket> {
        const config: IProtocolConfig = this.OneProtocol.config("PUT", `/question/updatetextbox/`, [], question);
        const messages: IProtocolMessages = {Error: {custom: this.translations.couldNotUpdateQuestion}};
        return this.OneProtocol.http(config, messages);
    }

    public delete(id: string): ng.IPromise<IProtocolPacket> {
        const config: IProtocolConfig = this.OneProtocol.config("DELETE", `/question/${id}`);
        const messages: IProtocolMessages = {Error: {custom: this.translations.couldNotDeleteQuestion}};
        return this.OneProtocol.http(config, messages);
    }

    public getInventoryOptions(tableId: string): ng.IPromise<IProtocolPacket> {
        const config: IProtocolConfig = this.OneProtocol.config("GET", `/question/GetInventoryOption/${tableId}`);
        const messages: IProtocolMessages = {Error: {custom: this.translations.errorRetrievingOptions}};
        return this.OneProtocol.http(config, messages);
    }

    public getQuestionCategory(questionId: string, version: string): ng.IPromise<IProtocolPacket> {
        const config: IProtocolConfig = this.OneProtocol.config("GET", `/question/GetQuestionCategory/${questionId}/${version}`);
        const messages: IProtocolMessages = {Error: {custom: this.translations.errorTogglingCategory}};
        return this.OneProtocol.http(config, messages);
    }

    public updateQuestionCategory(data: any): ng.IPromise<IProtocolPacket> {
        const config: IProtocolConfig = this.OneProtocol.config("PUT", `/question/UpdateQuestionCategory/`, [], data);
        const messages: IProtocolMessages = {Error: {custom: this.translations.errorTogglingCategory}};
        return this.OneProtocol.http(config, messages);
    }

    public excludeChangedQuestion(question: IQuestion): boolean {
        return isNil(question.HasVersionResponseChanged);
    }

    public excludeNewQuestion(question: IQuestion): boolean {
        return !question.IsFromCurrentVersion;
    }

    public excludeSkippedQuestion(question: IQuestion): boolean {
        return !question.IsSkipped;
    }

    public updateQuestionsNoChange(questions: any[], questionId: string): any[] {
        return map(questions, (question: IQuestion): any => this.updateQuestionNoChange(question, questionId));
    }

    public updateQuestionNoChange(question: IQuestion, questionId: string): IQuestion {
        if (question.Id === questionId) question.HasVersionResponseChanged = false;
        return question;
    }

    public changeQuestionStateLabel(scope: any, question: IQuestion, responses: IQuestionResponse[]): void|undefined {
        if (responses && responses.length) {
            question.State = QuestionStates.Answered;
            return;
        }
        question.State = QuestionStates.Unanswered;
    }

    public changeQuestionStateLabelRefactored(question: IQuestion, responses: IQuestionResponse[]): QuestionStates {
        const responsesLength: number = responses.length;
        for (let i = 0; i < responsesLength; i++) {
            if (responses[i].Type !== QuestionResponseTypes.Justification && !isNil(responses[i].Value)) {
                return QuestionStates.Answered;
            }
        }
        return QuestionStates.Unanswered;
    }

    public dmIsQuestionSelected = (question: IQuestion, option: any): boolean => {
        if (!isArray(question.Responses)) return false;
        const questionResponsesCount: number = question.Responses.length;
        for (let i = 0; i < questionResponsesCount; i++) {
            if (question.Responses[i] && question.Responses[i].InventoryId !== null && (option.Value.toString() === question.Responses[i].InventoryId.toString())) {
                return true;
            }
        }
    }

    public dmSaveQuestionResponse = (question: IQuestion, type: any, option: any, groupId: string, callback: (question: IQuestion, groupId: string) => any): void => {
        const currentCtrl: any = this.getCtrlInstance();
        let itemIndex = -1;
        currentCtrl.callsInProgress++;

        const newResponse: any = {
            HasVersionResponseChanged: true,
            QuestionId: question.Id,
            Responses: question.Responses || [],
        };

        const actions: any = {
            Ignore: -1,
            Delete: 0,
            Add: 1,
            Update: 2,
        };

        let currentAction = actions.Ignore;

        if (type === this.responseTypes.Reference) {
            const newResponseResponsesLength: number = newResponse.Responses.length;
            for (let i = 0; i < newResponseResponsesLength; i++) {
                if (newResponse.Responses[i].Type === this.responseTypes.Reference && newResponse.Responses[i].InventoryId === option.Value) {
                    itemIndex = i;
                    currentAction = actions.Delete;
                    break;
                }
            }

            if (itemIndex < 0) {
                currentAction = actions.Add;
            }
            if (currentAction === actions.Add) {
                this.addDmResponse(this.responseTypes.Reference, "", groupId, option.Value, newResponse);
            } else if (currentAction === actions.Delete && itemIndex > -1) {
                newResponse.Responses.splice(itemIndex, 1);
            }
        } else if (type === this.responseTypes.Other) {
            if (isUndefined(option.Value)) {
                currentAction = actions.Update;
            } else if (option.Value) {
                if (option.FieldId) {
                    currentAction = actions.Update;
                } else {
                    currentAction = actions.Add;
                }
            } else {
                currentAction = actions.Delete;
            }

            if (currentAction === actions.Update || currentAction === actions.Delete) {
                const newResponseResponsesLength: number = newResponse.Responses.length;
                for (let i = 0; i < newResponseResponsesLength; i++) {
                    if (newResponse.Responses[i].Type === this.responseTypes.Other && newResponse.Responses[i].Value === option.Value) {
                        itemIndex = i;
                        break;
                    }
                }
            }
            if (currentAction === actions.Add) {
                this.addDmResponse(this.responseTypes.Other, option.Value, groupId, null, newResponse);
            } else if (currentAction === actions.Update && itemIndex > -1) {
                newResponse.Responses[itemIndex].Value = option.Value;
            } else if (currentAction === actions.Delete && itemIndex > -1) {
                newResponse.Responses.splice(itemIndex, 1);
            }
        }

        this.Response.inventoryResponse(newResponse).then((res: IProtocolPacket): undefined|void => {
            if (!res.result) return;
            question.Responses = newResponse.Responses;
            if (isFunction(callback)) {
                callback(question, groupId);
            }

            question.HasVersionResponseChanged = true;
            this.changeQuestionStateLabel(currentCtrl, question, newResponse.Responses);
            this.Answer.handleQuestionResponse(currentCtrl, currentCtrl.Dictionary, question, res.data);
            this.getRemainingQuestions();
            currentCtrl.SetSectionFooterStates();
            if (currentCtrl.Project.StateDesc === this.projectStates.InProgress ||
                currentCtrl.Project.StateDesc === this.projectStates.InfoNeeded) {
                currentCtrl.getProjectProgress(currentCtrl.Project.Id, currentCtrl.Project.Version);
            }
            currentCtrl.callsInProgress--;
        });
    }

    public setQuestionResponseAndSave = (question: IQuestion): void => {
        const currentCtrl: any = this.getCtrlInstance();
        question.ResponseType = question.DefaultResponseType;
        if (!isUndefined(question.AllowOther) && !question.MultiSelect) {
            question.OtherAnswer = null;
        }
        question.NotApplicable = false;
        if (question.Answer) {
            question.isAnswered = true;
        } else {
            question.isAnswered = false;
        }
        this.saveQuestionResponse(currentCtrl.Project, question);
    }

    public isQuestionSelected = (sectionId: string, questionId: string, oIndex: number, subIndex: number): boolean => {
        const currentCtrl: any = this.getCtrlInstance();
        const sectionIndex: number = findIndex(currentCtrl.Project.Sections, ["Id", sectionId]);
        const boolean = false;

        if (sectionIndex >= 0) {
            const questionIndex: number = findIndex(currentCtrl.Project.Sections[sectionIndex].Questions, ["Id", questionId]);
            if (questionIndex >= 0) {
                if (oIndex >= 0) {
                    const answer: any = currentCtrl.Project.Sections[sectionIndex].Questions[questionIndex].Answer;

                    if (isArray(answer)) {
                        return answer.indexOf(currentCtrl.Project.Sections[sectionIndex].Questions[questionIndex].Options[oIndex].Value.toString()) >= 0;
                    } else if (isString(answer)) {
                        return (answer === currentCtrl.Project.Sections[sectionIndex].Questions[questionIndex].Options[oIndex].Value.toString());
                    }
                    return boolean;
                }
            }
        }
        return boolean;
    }

    // Deprecated when: AssessmentPermissionsRefactor: true
    public toggleNotApplicable = (question: IQuestion): void => {
        const currentCtrl: any = this.getCtrlInstance();
        if (question.ResponseType === this.responseTypes.NotApplicable) {
            question.ResponseType = question.DefaultResponseType;
            question.NotApplicable = false;
            this.saveQuestionResponse(currentCtrl.Project, question);
        } else {
            question.ResponseType = this.responseTypes.NotApplicable;
            if (!isUndefined(question.AllowOther)) {
                question.OtherAnswer = null;
            }
            question.Answer = null;
            question.NotApplicable = true;
            this.saveQuestionResponse(currentCtrl.Project, question);
        }
        if (question.Type === this.questionTypes.User) question.inventoryError = "";
    }

    public toggleNotSure = (sectionId: string, questionId: string): void => {
        const currentCtrl: any = this.getCtrlInstance();
        const sectionIndex: number = findIndex(currentCtrl.Project.Sections, ["Id", sectionId]);
        if (sectionIndex >= 0) {
            const questionIndex: number = findIndex(currentCtrl.Project.Sections[sectionIndex].Questions, ["Id", questionId]);
            if (questionIndex >= 0) {
                const question: any = currentCtrl.Project.Sections[sectionIndex].Questions[questionIndex];
                if (question.ResponseType === this.responseTypes.NotSure) {
                    question.ResponseType = question.DefaultResponseType;
                    this.saveQuestionResponse(currentCtrl.Project, question);
                } else {
                    question.ResponseType = this.responseTypes.NotSure;
                    if (!isUndefined(question.AllowOther)) {
                        question.OtherAnswer = null;
                    }
                    question.Answer = null;
                    this.saveQuestionResponse(currentCtrl.Project, question);
                }
                if (question.Type === this.questionTypes.User) question.inventoryError = "";
            }
        }
    }

    public updateQuestionState = (sectionId: string, question: IQuestion, stateId: number): void => {
        const currentCtrl: any = this.getCtrlInstance();
        const newState: any = {
            Id: question.Id,
            State: stateId,
        };
        this.ProjectQuestions.updateState(newState).then((response: IProtocolPacket): void => {
            if (!response.result) return;
            question.State = stateId;
            const sectionIndex: number = findIndex(currentCtrl.Project.Sections, ["Id", sectionId]);
            const projectId: string = currentCtrl.Project.Id;
            const projectVersion: string = currentCtrl.Project.Version;
            this.getRemainingQuestionsRefactored();
            currentCtrl.Project.Sections[sectionIndex].FilteredQuestions = this.Answer.setFilters(currentCtrl, currentCtrl.Project.Sections[sectionIndex]);
        });
    }

    public toggleQuestionAcceptance(sectionId: string, question: IQuestion): void {
        if (question.State === QuestionStates.Accepted) {
            if (question.Responses !== null && question.Responses.length) {
                this.updateQuestionState(sectionId, question, QuestionStates.Answered);
            } else {
                this.updateQuestionState(sectionId, question, QuestionStates.Unanswered);
            }
        } else {
            this.updateQuestionState(sectionId, question, QuestionStates.Accepted);
        }
    }

    public questionInventorySelect = (sectionId: string, question: IQuestion, answer: any): void | undefined => {
        const currentCtrl: any = this.getCtrlInstance();
        const application = this.questionTypes.Application;
        const location = this.questionTypes.Location;
        const provider = this.questionTypes.Provider;
        const user = this.questionTypes.User;
        if (question.Type === application  || question.Type === location || question.Type === provider) {
            if (!answer) {
                question.Answer = null;
                this.setQuestionResponseAndSave(question);
                return;
            }
            if (answer.Value) {
                this.setQuestionResponseAndSave(question);
                return;
            }
            this.setOtherOption(sectionId, question.Id);
            this.saveQuestionResponse(currentCtrl.Project, question);
            return;
        }
        if (question.Type === user) {
            if (!answer) {
                question.Answer = null;
                this.setQuestionResponseAndSave(question);
                question.inventoryError = null;
                return;
            }
            if (answer.Value) {
                this.setQuestionResponseAndSave(question);
                question.inventoryError = null;
                return;
            }
            if (Utilities.matchRegex(answer, this.ENUMS.Regex.Email)) {
                this.setOtherOption(sectionId, question.Id);
                this.saveQuestionResponse(currentCtrl.Project, question);
                question.inventoryError = null;
                return;
            }
            question.inventoryError = this.$rootScope.t("SelectUserOrEnterEmail");
        }
    }

    public saveJustification = (question: IQuestion): void => {
        const currentCtrl: any = this.getCtrlInstance();
        this.saveQuestionResponse(currentCtrl.Project, question);
    }

    public multiSelectToggle = (sectionId: string, questionId: string, value: any, subValue: any): any => {
        const currentCtrl: any = this.getCtrlInstance();
        const sectionIndex: number = findIndex(currentCtrl.Project.Sections, ["Id", sectionId]);
        let stringCompare = "";
        if (isUndefined(subValue)) {
            stringCompare = value.toString();
        } else {
            stringCompare = value.toString() + "/" + subValue.toString();
        }

        if (sectionIndex >= 0) {
            const questionRef: IQuestion[] = currentCtrl.Project.Sections[sectionIndex].Questions;
            const questionIndex: number = findIndex(questionRef, ["Id", questionId]);

            if (questionIndex >= 0) {
                const answer: any = questionRef[questionIndex].Answer;
                if (isArray(answer) && answer.length > 0) {
                    const valueIndex: number = answer.indexOf(stringCompare);
                    if (valueIndex < 0) {
                        questionRef[questionIndex].Answer.push(stringCompare);
                    } else {
                        const orphanAssetCheck: number = answer.indexOf(value.toString());
                        if (orphanAssetCheck < 0) {
                            questionRef[questionIndex].Answer.push(value.toString());
                        }
                        questionRef[questionIndex].Answer.splice(valueIndex, 1);
                    }
                } else {
                    questionRef[questionIndex].Answer = [stringCompare];
                }
                this.setQuestionResponseAsValue(questionRef[questionIndex]);
                return this.saveQuestionResponseDebounced(currentCtrl.Project, questionRef[questionIndex]);
            }
        }
    }

    public multiChoiceToggle = (sectionId: string, questionId: string, value: any): void => {
        const currentCtrl: any = this.getCtrlInstance();
        const sectionIndex: number = findIndex(currentCtrl.Project.Sections, ["Id", sectionId]);

        if (sectionIndex >= 0) {
            const questionIndex: number = findIndex(currentCtrl.Project.Sections[sectionIndex].Questions, ["Id", questionId]);
            if (questionIndex >= 0) {
                const answer: any = currentCtrl.Project.Sections[sectionIndex].Questions[questionIndex].Answer;
                if (answer === value.toString()) {
                    currentCtrl.Project.Sections[sectionIndex].Questions[questionIndex].Answer = "";
                } else {
                    currentCtrl.Project.Sections[sectionIndex].Questions[questionIndex].Answer = value.toString();
                }

                if (!isUndefined(currentCtrl.Project.Sections[sectionIndex].Questions[questionIndex].AllowOther) &&
                    currentCtrl.Project.Sections[sectionIndex].Questions[questionIndex].AllowOther) {
                    currentCtrl.Project.Sections[sectionIndex].Questions[questionIndex].OtherAnswer = null;
                }

                this.setQuestionResponseAsValue(currentCtrl.Project.Sections[sectionIndex].Questions[questionIndex]);
                this.saveQuestionResponse(currentCtrl.Project, currentCtrl.Project.Sections[sectionIndex].Questions[questionIndex]);
            }
        }
    }

    public setOtherOption = (sectionId: string, questionId: string): void => {
        const currentCtrl: any = this.getCtrlInstance();
        const question = currentCtrl.Dictionary.Questions[questionId];
        if (!question) return;

        question.ResponseType = this.responseTypes.Other;

        if (!question.MultiSelect && question.Type === this.questionTypes.Multichoice) {
            question.Answer = null;
        }
    }

    public setQuestionResponseAsValue(question: any): void {
        question.ResponseType = question.DefaultResponseType;
        if (!isUndefined(question.AllowOther) && !question.MultiSelect) {
            question.OtherAnswer = null;
        }
        question.NotApplicable = false;
        if (question.Answer) {
            question.isAnswered = true;
        } else {
            question.isAnswered = false;
        }
    }

    public saveQuestionResponse = (project: any, question: IQuestion): ng.IPromise<IProtocolPacket> => {
        const deferred: any = this.$q.defer();
        const promise: ng.IPromise<IProtocolPacket> = deferred.promise;
        const currentCtrl: any = this.getCtrlInstance();
        const that: any = this;

        const newResponse: any = {
            HasVersionResponseChanged: true,
            QuestionId: question.Id,
            Responses: [],
        };
        currentCtrl.callsInProgress++;

        const addResponse = (value, type): void => {
            newResponse.Responses.push({
                Type: type,
                Value: value,
            });
        };

        const isValidQuestionType: boolean = (question.ResponseType === this.responseTypes.Other && question.OtherAnswer) ||
            (question.ResponseType === this.responseTypes.NotSure) ||
            (question.ResponseType === this.responseTypes.NotApplicable);

        const isValidAnswer: boolean = !isNil(question.Answer);

        if (isValidAnswer || isValidQuestionType) {

            if ((question.ResponseType !== this.responseTypes.NotApplicable) && (question.ResponseType !== this.responseTypes.Justification)) {
                question.NotApplicable = false;
            }

            if ((question.ResponseType !== this.responseTypes.NotSure) && (question.ResponseType !== this.responseTypes.NotApplicable) && !question.NotApplicable) {
                switch (question.Type) {
                    case this.questionTypes.Content:
                        break;
                    case this.questionTypes.TextBox:
                        if (question.Answer) {
                            addResponse(question.Answer, this.responseTypes.Value);
                        }
                        break;
                    case this.questionTypes.Multichoice: // MutliSelect Answers (Not including 'Other')
                        if (question.MultiSelect) {
                            const multiValues = this.GenFactory.removeNulls(question.Answer);
                            each(multiValues, (value: any): void => {
                                addResponse(value, this.responseTypes.Reference);
                            });
                        } else {
                            if (question.Answer) addResponse(question.Answer, this.responseTypes.Reference);
                        }
                        if (question.OtherAnswer) addResponse(question.OtherAnswer, this.responseTypes.Other);
                        break;
                    case this.questionTypes.YesNo:
                        if (question.Answer !== null) addResponse(question.Answer, this.responseTypes.Value);
                        break;
                    case this.questionTypes.DateTime:
                        if (question.Answer !== null) addResponse(question.Answer, this.responseTypes.Value);
                        break;
                    case this.questionTypes.Application:
                    case this.questionTypes.User:
                    case this.questionTypes.Location:
                    case this.questionTypes.Provider:
                        if (question.Answer !== null) {
                            if (question.Answer.Value) {
                                addResponse(question.Answer.Value, this.responseTypes.Reference);
                            } else {
                                addResponse(question.Answer, this.responseTypes.Other);
                            }
                        }
                        break;
                }

            } else if (question.ResponseType === this.responseTypes.NotSure) {
                addResponse("", this.responseTypes.NotSure);
            } else {
                addResponse("", this.responseTypes.NotApplicable);
            }
        }

        if (question.Justification) addResponse(question.Justification, this.responseTypes.Justification);

        this.Response.create(newResponse).then((res: IProtocolPacket): undefined|void => {
            deferred.resolve(res);
            if (!res.result) return;
            question.Responses = newResponse.Responses;
            question.HasVersionResponseChanged = true;
            that.changeQuestionStateLabel(currentCtrl, question, question.Responses);
            that.Answer.handleQuestionResponse(currentCtrl, currentCtrl.Dictionary, question, res.data);

            this.getRemainingQuestions();
            if (project.StateDesc === this.projectStates.InProgress ||
                project.StateDesc === this.projectStates.InfoNeeded) {
                currentCtrl.getProjectProgress(project.Id, project.Version);
            }
            currentCtrl.callsInProgress--;
        });

        return promise;
    }

    public getRemainingQuestions = (): void => {
        const currentCtrl: any = this.getCtrlInstance();
        const currentUserId: string = currentCtrl.CurrentUser.Id;
        const ProjectRemaining: any[] = [];
        let questionNumber = 1;
        let NeedInfoCount = 0;
        let HasRisks = false;
        let undecidedCount = 0;

        each(currentCtrl.Project.Sections, (section: any, sIndex: number): void => {
            const SectionRequiredRemaining: any[] = [];
            let SectionTotal = 0;
            let SectionTotalRemaining = 0;
            if (section.Assignment && section.Assignment.AssigneeId === currentUserId) {
                each(section.Questions, (question: IQuestion, qIndex: number): void => {
                    if (!question.IsSkipped && !section.isWelcomeSection) {
                        question.Number = (questionNumber++).toString();
                        SectionTotal++;
                        if (question.State === QuestionStates.InfoRequested) {
                            if (section.Assignment && section.Assignment.AssigneeId === currentUserId) {
                                NeedInfoCount++;
                            } else if (section.Assignment === null) {
                                NeedInfoCount++;
                            }
                        }
                        if (question.Risk && question.Risk.Level) {
                            HasRisks = true;
                        }
                        if (question.versionState && question.versionState.state === this.ENUMS.QuestionVersionStates.Undecided) {
                            undecidedCount++;
                        }
                    }
                    if (!question.IsSkipped) {
                        const questionsFiltered: boolean[] = map(question.Responses, (questionResponse: IQuestion): boolean => questionResponse.Type !== QuestionResponseTypes.Justification);
                        const questionJustified: boolean = question.AllowJustification && Boolean(question.Justification);
                        const questionAnswered: boolean = isArray(question.Responses) && questionsFiltered.length > 0;
                        if (!questionAnswered || !questionJustified) {
                            SectionTotalRemaining++;
                            if (!section.IsLocked && (question.Required && !questionAnswered) || (question.RequireJustification && !questionJustified)) {
                                SectionRequiredRemaining.push(question.Id);
                            }
                        }
                    }
                });
            }

            if (currentCtrl.ProjectMode === ProjectModes.ReviewMode) {
                this.Answer.fixStates(currentCtrl, section);
                section.FilteredQuestions = this.Answer.setFilters(currentCtrl, section);
            }

            section.Remaining = SectionRequiredRemaining;
            section.totalRemaining = SectionTotalRemaining;
            section.Total = SectionTotal;
            section.Completed = SectionTotal - SectionTotalRemaining;

            if (SectionRequiredRemaining.length) {
                ProjectRemaining.push({
                    Id: section.Id,
                    QuestionIds: SectionRequiredRemaining,
                });
            }
        });
        currentCtrl.NeedInfoCount = NeedInfoCount;
        currentCtrl.Project.HasRisks = HasRisks;
        currentCtrl.Project.Remaining = ProjectRemaining;
        currentCtrl.UndecidedRemaining = undecidedCount;
        currentCtrl.responseIsUpdating = false;
        this.store.dispatch({ type: "UNHANDLED_ACTION: questions.service: saveQuestionResponseDebounced()" });
    }

    // New Methods : Only used by new features toggled with AssessmentPermissionsRefactoring
    public composeResponses = (question: IQuestion): IQuestionResponse[] => {
        const questionResponses: IQuestionResponse[] = [];
        if (question.ResponseType === QuestionResponseTypes.NotSure ||
            question.ResponseType === QuestionResponseTypes.NotApplicable) {
            questionResponses.push({ Type: question.ResponseType, Value: "" });
        }
        if (question.Justification) {
            questionResponses.push({ Type: QuestionResponseTypes.Justification, Value: question.Justification });
        }
        if (question.ResponseType !== QuestionResponseTypes.NotSure &&
            question.ResponseType !== QuestionResponseTypes.NotApplicable &&
            !question.NotApplicable
        ) {
            switch (question.Type) {
                case QuestionTypes.Content:
                    break;
                case QuestionTypes.TextBox:
                    if (question.Answer) {
                        questionResponses.push({ Type: QuestionResponseTypes.Value, Value: question.Answer});
                    }
                    break;
                case QuestionTypes.Multichoice: // MutliSelect Answers (Not including 'Other')
                    if (question.MultiSelect) {
                        each(question.Answer, (value: string): void => {
                            questionResponses.push({ Type: QuestionResponseTypes.Reference, Value: value });
                        });
                    } else if (question.Answer) {
                        questionResponses.push({ Type: QuestionResponseTypes.Reference, Value: question.Answer });
                    }
                    if (question.OtherAnswer) {
                        questionResponses.push({ Type: QuestionResponseTypes.Other, Value: question.OtherAnswer });
                    }
                    break;
                case QuestionTypes.YesNo:
                case QuestionTypes.DateTime:
                    if (question.Answer !== null) {
                        questionResponses.push({ Type: QuestionResponseTypes.Value, Value: question.Answer });
                    }
                    break;
                case QuestionTypes.Application:
                case QuestionTypes.User:
                case QuestionTypes.Location:
                case QuestionTypes.Provider:
                    if (question.Answer !== null) {
                        if (question.Answer.Value) {
                            questionResponses.push({ Type: QuestionResponseTypes.Reference, Value: question.Answer.Value });
                        } else {
                            questionResponses.push({ Type: QuestionResponseTypes.Other, Value: question.Answer });
                        }
                    }
                    break;
            }
        }
        return questionResponses;
    }

    public setQuestionResponseAndSaveRefactored = (question: IQuestion): void => {
        const currentCtrl: any = this.getCtrlInstance();
        currentCtrl.responseIsUpdating = true;
        question.ResponseType = question.DefaultResponseType;
        if (!isUndefined(question.AllowOther) && !question.MultiSelect) {
            question.OtherAnswer = null;
        }
        question.NotApplicable = false;
        if (question.Answer) {
            question.isAnswered = true;
        } else {
            question.isAnswered = false;
        }
        this.saveQuestionResponseDebounced(currentCtrl.Project, question);
    }

    public toggleNotSureNotApplicableRefactored = (question: IQuestion): void => {
        const currentCtrl: any = this.getCtrlInstance();
        currentCtrl.responseIsUpdating = true;
        if (question.Type === this.ENUMS.QuestionTypes.User) question.inventoryError = "";
        this.saveQuestionResponseDebounced(currentCtrl.Project, question);
    }

    public saveQuestionResponseRefactored = (question: IQuestion): void => {
        const currentCtrl: any = this.getCtrlInstance();
        currentCtrl.responseIsUpdating = true;
        this.saveQuestionResponseDebounced(currentCtrl.Project, question);
    }

    public multiChoiceToggleRefactored = (question: IQuestion): void => {
        const currentCtrl: any = this.getCtrlInstance();
        currentCtrl.responseIsUpdating = true;
        this.setQuestionResponseAsValue(question);
        this.saveQuestionResponseDebounced(currentCtrl.Project, question);
    }

    public isRequiredAndUnanswered = (question: IQuestion): boolean => {
        return question.Required && !question.IsSkipped && (question.State === QuestionStates.Unanswered);
    }

    public isRequiredAndUnasnweredJustification = (question: IQuestion): boolean => {
        return question.AllowJustification && question.RequireJustification && !question.Justification;
    }

    public dmSaveQuestionResponseRefactored = (question: IQuestion, type: any, option: any, groupId: string, callback: any): void => {
        const currentCtrl: any = this.getCtrlInstance();
        currentCtrl.responseIsUpdating = true;
        let itemIndex = -1;

        const newResponse: any = {
            HasVersionResponseChanged: true,
            QuestionId: question.Id,
            Responses: question.Responses || [],
        };

        const actions: any = {
            Ignore: -1,
            Delete: 0,
            Add: 1,
            Update: 2,
        };

        let currentAction = actions.Ignore;

        if (type === this.responseTypes.Reference) {
            const newResponseResponsesLength: number = newResponse.Responses.length;
            for (let i = 0; i < newResponseResponsesLength; i++) {
                if (newResponse.Responses[i].Type === this.responseTypes.Reference && newResponse.Responses[i].InventoryId === option.Value) {
                    itemIndex = i;
                    currentAction = actions.Delete;
                    break;
                }
            }

            if (itemIndex < 0) {
                currentAction = actions.Add;
            }
            if (currentAction === actions.Add) {
                this.addDmResponse(this.responseTypes.Reference, "", groupId, option.Value, newResponse);
            } else if (currentAction === actions.Delete && itemIndex > -1) {
                newResponse.Responses.splice(itemIndex, 1);
            }
        } else if (type === this.responseTypes.Other) {
            if (isUndefined(option.Value)) {
                currentAction = actions.Update;
            } else if (option.Value) {
                if (option.FieldId) {
                    currentAction = actions.Update;
                } else {
                    currentAction = actions.Add;
                }
            } else {
                currentAction = actions.Delete;
            }

            if (currentAction === actions.Update || currentAction === actions.Delete) {
                const newResponseResponsesLength: number = newResponse.Responses.length;
                for (let i = 0; i < newResponseResponsesLength; i++) {
                    if (newResponse.Responses[i].Type === this.responseTypes.Other && newResponse.Responses[i].Value === option.Value) {
                        itemIndex = i;
                        break;
                    }
                }
            }
            if (currentAction === actions.Add) {
                this.addDmResponse(this.responseTypes.Other, option.Value, groupId, null, newResponse);
            } else if (currentAction === actions.Update && itemIndex > -1) {
                newResponse.Responses[itemIndex].Value = option.Value;
            } else if (currentAction === actions.Delete && itemIndex > -1) {
                newResponse.Responses.splice(itemIndex, 1);
            }
        }

        this.addInventoryResponseDebounced(newResponse, question, callback, groupId, currentCtrl);
    }

    public getRemainingQuestionsRefactored = (): void => {
        const currentCtrl: any = this.getCtrlInstance();
        const currentUserId: string = getCurrentUser(this.store.getState()).Id;
        const ProjectRemaining: any[] = [];
        let questionNumber = 1;
        let NeedInfoCount = 0;
        let HasRisks = false;
        let undecidedCount = 0;

        each(currentCtrl.Project.Sections, (section: any, sIndex: number): void => {
            const SectionRequiredRemaining: any[] = [];
            let SectionTotal = 0;
            let SectionTotalRemaining = 0;
            each(section.Questions, (question: IQuestion, qIndex: number): void => {
                if (!question.IsSkipped && !section.isWelcomeSection) {
                    question.Number = (questionNumber++).toString();
                    SectionTotal++;
                    if (question.State === QuestionStates.InfoRequested) {
                        NeedInfoCount++;
                    }
                    if (question.Risk && question.Risk.Level) {
                        HasRisks = true;
                    }
                    if (question.versionState && question.versionState.state === this.ENUMS.QuestionVersionStates.Undecided) {
                        undecidedCount++;
                    }
                }
                if (!question.IsSkipped) {
                    if (this.isRequiredAndUnanswered(question)) {
                        SectionTotalRemaining++;
                        SectionRequiredRemaining.push(question.Id);
                    }
                    if (this.isRequiredAndUnasnweredJustification(question)) {
                        SectionTotalRemaining++;
                        SectionRequiredRemaining.push(question.Id);
                    }
                }
            });

            if (currentCtrl.ProjectMode === ProjectModes.ReviewMode) {
                this.Answer.fixStates(currentCtrl, section);
                section.FilteredQuestions = this.Answer.setFilters(currentCtrl, section);
            }

            section.Remaining = SectionRequiredRemaining;
            section.totalRemaining = SectionTotalRemaining;
            section.Total = SectionTotal;
            section.Completed = SectionTotal - SectionTotalRemaining;

            if (SectionRequiredRemaining.length) {
                ProjectRemaining.push({
                    Id: section.Id,
                    QuestionIds: SectionRequiredRemaining,
                });
            }
        });
        currentCtrl.NeedInfoCount = NeedInfoCount;
        currentCtrl.Project.HasRisks = HasRisks;
        currentCtrl.Project.Remaining = ProjectRemaining;
        currentCtrl.UndecidedRemaining = undecidedCount;
        currentCtrl.responseIsUpdating = false;
        this.store.dispatch({ type: "UNHANDLED_ACTION: questions.service: getRemainingQuestionsRefactored()" });
    }

    private addDmResponse(type: any, name: string, groupId: string, value: any, newResponse: any): void {
        newResponse.Responses.push({
            Type: type,
            Value: name,
            FieldId: groupId,
            InventoryId: value,
        });
    }

    private setCtrlInstance(scope: any): void {
        this.ctrlInstance = scope;
    }

    private getCtrlInstance(): any {
        return this.ctrlInstance;
    }
}
