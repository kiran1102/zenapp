// Angular
import { Injectable } from "@angular/core";

// Interfaces
import {
    IRisk,
    IRiskSerialized,
    IRiskDetails,
    IRiskSerializedV2,
    IRiskSerializedV2New,
} from "interfaces/risk.interface";

@Injectable({
    providedIn: "root",
})
export class RiskAdapterService {

    public serializeRisk(riskModel: IRisk): IRiskSerialized {
        return {
            Deadline: riskModel.Deadline,
            Description: riskModel.Description,
            QuestionId: riskModel.QuestionId,
            Recommendation: riskModel.Recommendation,
            ReminderDays: riskModel.ReminderDays,
            RiskImpactLevel: riskModel.RiskImpactLevel,
            RiskLevel: riskModel.Level,
            RiskOwnerId: riskModel.RiskOwnerId,
            RiskProbabilityLevel: riskModel.RiskProbabilityLevel,
            Mitigation: riskModel.Mitigation,
            RequestedException: riskModel.RequestedException,
        };
    }

    public serializeRiskV2(riskModel: IRiskDetails): IRiskSerializedV2 {
        return {
            action: riskModel.action,
            deadline: riskModel.deadline,
            description: riskModel.description,
            recommendation: riskModel.recommendation,
            reminderDays: riskModel.reminderDays,
            levelId: riskModel.levelId,
            probabilityLevel: riskModel.probabilityLevel,
            impactLevel: riskModel.impactLevel,
            riskOwnerId: riskModel.riskOwnerId,
            mitigation: riskModel.mitigation,
            requestedException: riskModel.requestedException,
            typeId: riskModel.typeId,
            type: riskModel.type,
            sourceTypeId: riskModel.sourceTypeId,
            sourceType: riskModel.sourceType,
            orgGroupId: riskModel.orgGroupId,
            references: riskModel.references,
        };
    }
}
