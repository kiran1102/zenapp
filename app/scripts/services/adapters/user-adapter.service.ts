import { map, mapKeys, lowerFirst, upperFirst } from "lodash";

import { IUser, IUserManageContract } from "interfaces/user.interface";
import { IAccessLevel, IAccessLevelLegacy } from "interfaces/user-access-level.interface";

export class UserAdapter {

    deserializeUser(user: IUser): IUser {
        if (user.AccessLevels) {
            user.AccessLevels = map(
                user.AccessLevels as IAccessLevelLegacy[],
                (aLevel: IAccessLevelLegacy): IAccessLevel => mapKeys(aLevel, (value, key) => {
                    return lowerFirst(key);
                }) as any as IAccessLevel,
            );
        } else {
            user.AccessLevels = [{
                id: user.Id,
                organizationId: user.OrgGroupId,
                roleId: user.RoleId,
                isDefault: true,
            }];
        }
        return user;
    }

    serializeUser(user: IUser | IUserManageContract): IUser | IUserManageContract {
        user.AccessLevels = map(
            user.AccessLevels as IAccessLevel[],
            (aLevel: IAccessLevel): IAccessLevelLegacy => mapKeys(aLevel, (value, key) => {
                return upperFirst(key);
            }) as any as IAccessLevelLegacy,
        );
        return user;
    }
}
