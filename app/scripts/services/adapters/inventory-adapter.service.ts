// 3rd Party
import {
    sortBy,
    forEach,
    forIn,
    map,
    filter,
    find,
    remove,
    flatten,
    uniq,
    intersectionWith,
} from "lodash";
import { Injectable, Inject } from "@angular/core";

// Redux
import {
    getAttributeMap,
    getStatusMap,
    InventoryActions,
    getAttributeList,
} from "oneRedux/reducers/inventory.reducer";
import {
    getCurrentUser,
    getAllUserModels,
} from "oneRedux/reducers/user.reducer";

// Services
import { DMAssessmentsService } from "datamappingservice/Assessment/dm-assessments.service";
import { InventoryService } from "inventorySharedModule/shared/services/inventory-api.service";
import { getCommaSeparatedList } from "sharedServices/string-helper.service";
import { UserActionService } from "oneServices/actions/user-action.service";
import { Permissions } from "modules/shared/services/helper/permissions.service";
import { InventoryRiskService } from "modules/inventory/services/adapter/inventory-risk.service";

// Interfaces
import { IStoreState, IStore } from "interfaces/redux.interface";
import {
    IInventorySchema,
    IRecordsResponse,
    IRecordResponse,
    IInventoryDetails,
    IInventoryTable,
    IAttribute,
    IInventoryRecord,
    IInventoryCell,
    IInventoryCellValue,
    IAttributeValue,
    IAssessmentStatusMap,
    IInventoryTableView,
    IRecordAssignment,
} from "interfaces/inventory.interface";
import { IAttributeMap, IRecordMap } from "interfaces/inventory-reducer.interface";
import { IStringMap } from "interfaces/generic.interface";
import { IUser } from "interfaces/user.interface";
import { IUserMap } from "interfaces/user-reducer.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";
import { IFilterTree } from "interfaces/filter.interface";
import { IFilterColumnOption } from "interfaces/filter.interface";
import { IOrgUserAdapted } from "interfaces/org-user.interface";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

// Enums & Constants
import {
    OptionTypes,
    InventoryContexts,
    InventoryTableIds,
    InputTypes,
} from "enums/inventory.enum";
import {
    ManagingOrgAttributes,
    DetailsLinkAttributes,
    StatusLinkAttributes,
    AttributeAllowedContexts,
    NamesAttributes,
    OwnerAttributes,
    DisabledStatuses,
    RiskAttributes,
    InventoryDetailRoutes,
    InventoryAttributeCodes,
} from "constants/inventory.constant";

// Tokens
import { StoreToken } from "tokens/redux-store.token";

@Injectable()
export class InventoryAdapter {

    private inventoryId: string;
    private currentUser: IUser;

    constructor(
        @Inject(StoreToken) readonly store: IStore,
        @Inject("$q") private readonly $q: ng.IQService,
        private readonly inventoryRiskService: InventoryRiskService,
        private readonly dmAssessmentsService: DMAssessmentsService,
        private readonly inventoryService: InventoryService,
        private readonly userActionService: UserActionService,
        readonly permissions: Permissions,
        readonly translatePipe: TranslatePipe,
    ) { }

    getInventoryListView(inventoryId: string, page: number = 0, size: number = 20, searchTerm: string = "", filterData: IFilterTree): ng.IPromise<IProtocolResponse<IInventoryTableView>> {
        const inventoryPromises: Array<ng.IPromise<IProtocolResponse<IRecordsResponse>>> = [this.inventoryService.getSchema(inventoryId, { slim: true })];
        if (!searchTerm && filterData) {
            inventoryPromises.push(this.inventoryService.getFilteredRecords(inventoryId, { filterCriteria: filterData }, { page, size, filter: { name: searchTerm } }));
        } else {
            inventoryPromises.push(this.inventoryService.getRecords(inventoryId, { page, size, filter: { name: searchTerm } }));
        }
        if (this.permissions.canShow("DataMappingAssessmentsList")) inventoryPromises.push(this.dmAssessmentsService.getDataMappingStatuses());
        return this.$q.all(inventoryPromises).then((responses: Array<IProtocolResponse<IInventorySchema | IOrgUserAdapted[] | IRecordsResponse | IAssessmentStatusMap>>): ng.IPromise<IProtocolResponse<IInventoryTableView | null>> | IProtocolResponse<null> => {
            if (!responses || !responses.length || responses.filter((response) => !response.result).length) return { result: false, data: null };
            const statusMap = responses[2] ? responses[2].data as IAssessmentStatusMap : {};
            this.store.dispatch({
                type: InventoryActions.SET_INVENTORY_DEPENDENCIES,
                statusMap,
            });
            return this.setListUsers(responses[1].data as IRecordsResponse).then((): IProtocolResponse<IInventoryTableView | null> => {
                return {
                    result: true,
                    data: this.createInventoryViewResponse(responses[0].data as IInventorySchema, responses[1].data as IRecordsResponse, statusMap as IAssessmentStatusMap),
                };
            });
        });
    }

    getInventoryPage(inventoryId: string, page: number = 0, size: number = 20, searchTerm: string = ""): ng.IPromise<IProtocolResponse<IInventoryTable | null>> {
        return this.inventoryService.getRecords(inventoryId, { page, size, filter: { name: searchTerm } }).then((response: IProtocolResponse<IRecordsResponse>): ng.IPromise<IProtocolResponse<IInventoryTable | null>> | IProtocolResponse<null> => {
            if (!response.result) return { result: false, data: null };
            return this.setListUsers(response.data as IRecordsResponse).then((): IProtocolResponse<IInventoryTable | null> => {
                return {
                    result: true,
                    data: this.createInventoryPageResponse(response.data),
                };
            });
        });
    }

    getFilteredInventoryPage(inventoryId: string, filterData: IFilterTree, page: number = 0, size: number = 20, searchTerm: string = ""): ng.IPromise<IProtocolResponse<IInventoryTable | null>> {
        return this.inventoryService.getFilteredRecords(
            inventoryId,
            { filterCriteria: this.inventoryRiskService.formatRiskFilterForSave(filterData) },
            { page, size, filter: { name: searchTerm } },
        ).then((response: IProtocolResponse<IRecordsResponse>): ng.IPromise<IProtocolResponse<IInventoryTable | null>> | IProtocolResponse<null> => {
            if (!response.result) return { result: false, data: null };
            return this.setListUsers(response.data as IRecordsResponse).then((): IProtocolResponse<IInventoryTable | null> => {
                return {
                    result: true,
                    data: this.createInventoryPageResponse(response.data),
                };
            });
        });
    }

    getInventoryDetailsView(inventoryId: string, recordId: string): ng.IPromise<IProtocolResponse<IInventoryDetails | null>> {
        this.init(inventoryId);
        const detailPromises: Array<ng.IPromise<IProtocolResponse<IInventorySchema | IRecordResponse | IAssessmentStatusMap>>> = [
            this.inventoryService.getSchema(inventoryId, { slim: true }),
            this.inventoryService.getRecord(inventoryId, recordId),
        ];
        if (this.permissions.canShow("DataMappingAssessmentsList")) detailPromises.push(this.dmAssessmentsService.getDataMappingStatuses());
        return this.$q.all(detailPromises).then((responses: Array<IProtocolResponse<IInventorySchema | IRecordResponse | IAssessmentStatusMap>>): ng.IPromise<IProtocolResponse<IInventoryDetails | null>> | IProtocolResponse<null> => {
            if (!responses || !responses.length || responses.filter((response) => !response.result).length) return { result: false, data: null };
            const statusMap: IAssessmentStatusMap = responses[2] ? responses[2].data as IAssessmentStatusMap : {};
            return this.setRecordUsers(responses[1].data as IRecordResponse).then((): IProtocolResponse<IInventoryDetails | null> => {
                return {
                    result: true,
                    data: this.createInventoryDetailsResponse(responses[0].data as IInventorySchema, responses[1].data as IRecordResponse, statusMap),
                };
            });
        });
    }

    upsertFormattedRecord(inventoryType: string, record: IInventoryRecord, recordId?: string): ng.IPromise<IProtocolResponse<IRecordResponse>> {
        const recordBody: IStringMap<IInventoryCellValue[]> = this.formatRecordForSave(record);
        return this.inventoryService.upsertRecord(inventoryType, recordBody, recordId);
    }

    getRecordCells(attributeMap: IStringMap<IAttribute>, statusMap: IAssessmentStatusMap, record?: IRecordResponse, context?: number): IInventoryCell[] {
        const cells: IInventoryCell[] = [];
        forIn(attributeMap, (attribute: IAttribute, attributeId: string): void | undefined => {
            if ((context && !AttributeAllowedContexts[context][attribute.fieldAttributeCode])
                || (this.permissions.canShow("DMAssessmentV2")
                    && (attribute.fieldAttributeCode === InventoryAttributeCodes[this.inventoryId].Status))) {
                return;
            }
            let cell: IInventoryCell = {
                values: record && record.map ? record.map[attributeId] : [],
                attributeId,
                attributeCode: attribute.fieldAttributeCode,
                displayValue: "",
                fieldResponseTypeId: attribute.fieldResponseTypeId,
                fieldAttributeTypeId: attribute.fieldAttributeTypeId,
            };
            if (cell.values && cell.values.length) {
                let matchingOptions;
                if (attribute.fieldAttributeTypeId === OptionTypes.Users) {
                    cell.values = this.getUserSelections(cell.values);
                }
                if (attribute.fieldAttributeTypeId === OptionTypes.Users || attribute.fieldResponseTypeId === InputTypes.Text) {
                    matchingOptions = map(cell.values, (value) => {
                        return { name: value.value };
                    });
                } else if (attribute.fieldAttributeTypeId === OptionTypes.Orgs) {
                    matchingOptions = cell.values;
                } else {
                    matchingOptions = intersectionWith(attribute.values, cell.values, (attrValue, cellValue) => {
                        return attrValue.id === cellValue.valueId;
                    });
                }
                cell.displayValue = getCommaSeparatedList(matchingOptions, (item: IAttributeValue): string => {
                    return item.nameKey ? this.translatePipe.transform(item.nameKey) : item.name || item.value;
                });
            }
            if (attribute.fieldAttributeCode === StatusLinkAttributes[this.inventoryId] && record) {
                cell = this.getStatusCell(cell, statusMap, record.id);
            }
            if (attribute.fieldAttributeCode === DetailsLinkAttributes[this.inventoryId] && record) {
                cell.route = InventoryDetailRoutes[this.inventoryId];
                cell.routeParams = { inventoryId: this.inventoryId, recordId: record.id };
            }
            if (attribute.fieldAttributeCode === RiskAttributes[this.inventoryId] && record) {
                cell = this.inventoryRiskService.getRiskCell(cell);
            }
            cells.push(cell);
        });
        return cells;
    }

    getRecordAssignments(records: IInventoryRecord[], inventoryId: number): IRecordAssignment[] {
        return map(records, (record: IInventoryRecord): IRecordAssignment => {
            return {
                InventoryId: record.id,
                Name: this.getCellLabelByAttributeCode(record, NamesAttributes[inventoryId]),
                OrgGroupId: this.getCellValuePropByAttributeCode(record, ManagingOrgAttributes[inventoryId], "valueId"),
                defaultRespondent: this.getCellValuePropByAttributeCode(record, OwnerAttributes[inventoryId], "valueId") || this.getCellValuePropByAttributeCode(record, OwnerAttributes[inventoryId], "value"),
            };
        });
    }

    getRecordIsDisabledByStatus(recordId: string, statusMap: IAssessmentStatusMap): boolean {
        if (this.permissions.canShow("DMAssessmentV2")) {
            return false;
        }
        const assessmentStatus: string = statusMap[recordId] ? statusMap[recordId].status : "";
        return DisabledStatuses[assessmentStatus];
    }

    setRecordStatuses(recordMap: IRecordMap, statusMap: IAssessmentStatusMap): IRecordMap {
        forIn(recordMap, (record: IInventoryRecord): void => {
            let statusCell: IInventoryCell = find(record.cells, (cell: IInventoryCell): boolean => cell.attributeCode === StatusLinkAttributes[this.inventoryId]);
            statusCell = this.getStatusCell(statusCell, statusMap, record.id);
            record.assessmentOutstanding = record.assessmentOutstanding || this.getRecordIsDisabledByStatus(record.id, statusMap);
        });
        return recordMap;
    }

    formatColumnData(columnData: IFilterColumnOption[]): IFilterColumnOption[] {
        const formattedData: IFilterColumnOption[] = map(columnData, (column: IFilterColumnOption): IFilterColumnOption => {
            if (column.translationKey) {
                column.name = this.translatePipe.transform(column.translationKey);
            }
            return column;
        });
        return this.inventoryRiskService.formatRiskColumns(formattedData);
    }

    formatRecordForSave(record: IInventoryRecord): IStringMap<IInventoryCellValue[]> {
        const cellMap: IStringMap<IInventoryCellValue[]> = {};
        forEach(record.cells, (cell: IInventoryCell): void => {
            cellMap[cell.attributeId] = cell.values;
        });
        return cellMap;
    }

    formatOptions(options: IAttributeValue[]): IInventoryCellValue[] {
        const formattedOptions = map(options, (option: IAttributeValue): IInventoryCellValue => {
            const formattedOption: IInventoryCellValue = {
                value: "",
                valueId: "",
            };
            formattedOption.value = option.name;
            formattedOption.valueId = option.id;
            return formattedOption;
        });
        return formattedOptions;
    }

    getCellValuePropByAttributeCode(record: IInventoryRecord, attributeCode: string, property: string): string {
        const cell: IInventoryCell = find(record.cells, (recordCell: IInventoryCell): boolean => recordCell.attributeCode === attributeCode);
        return cell && cell.values && cell.values[0] ? cell.values[0][property] : "";
    }

    getCellLabelByAttributeCode(record: IInventoryRecord, attributeCode: string): string {
        const cell: IInventoryCell = find(record.cells, (recordCell: IInventoryCell): boolean => recordCell.attributeCode === attributeCode);
        return cell ? cell.displayValue : "";
    }

    getValueIdFromCell(cell: IInventoryCell): string {
        return cell && cell.values && cell.values.length && cell.values[0].valueId ? cell.values[0].valueId : "";
    }

    getCurrentRecordOrgId(record: IInventoryRecord): string {
        if (!record.cells) return "";
        const orgCell: IInventoryCell = find(record.cells, (cell: IInventoryCell): boolean => cell.attributeCode === ManagingOrgAttributes[this.inventoryId]);
        return this.getValueIdFromCell(orgCell);
    }

    setListUsers(recordList: IRecordsResponse): ng.IPromise<IUserMap> {
        const unknownIds: string[] = uniq(flatten(map(recordList.content, (recordResponse: IRecordResponse): string[] => {
            return this.getUnknownUsersFromRecord(recordResponse);
        })));
        if (!unknownIds.length) return this.$q.resolve(true);
        return this.userActionService.fetchUsersById(unknownIds);
    }

    setRecordUsers(record: IRecordResponse): ng.IPromise<IUserMap> {
        const unknownIds: string[] = this.getUnknownUsersFromRecord(record);
        if (!unknownIds.length) return this.$q.resolve(true);
        return this.userActionService.fetchUsersById(unknownIds);
    }

    private getUnknownUsersFromRecord(record: IRecordResponse): string[] {
        const userMap: IUserMap = getAllUserModels(this.store.getState());
        const attributes: IAttribute[] = getAttributeList(this.store.getState());
        const userAttributes = filter(attributes, (attribute: IAttribute): boolean => attribute.fieldAttributeTypeId === OptionTypes.Users);
        const recordUserIds = map(userAttributes, (attr: IAttribute): string => record.map[attr.id] ? record.map[attr.id][0].valueId : "");
        return filter(recordUserIds, (id: string): boolean => id && !userMap[id]);
    }

    private createInventoryViewResponse(schema: IInventorySchema, recordResponse: IRecordsResponse, statusMap: IAssessmentStatusMap): IInventoryTableView | undefined {
        this.init(schema.id);
        const recordList: IInventoryRecord[] = [];
        if (parseInt(schema.id, 10) === InventoryTableIds.Elements) { // If DE inventory, remove status column
            remove(schema.attributes, (attribute: IAttribute): boolean => attribute.fieldAttributeCode === StatusLinkAttributes[schema.id]);
        }
        const attributeMap: IAttributeMap = this.getAttributeMap(schema);
        forEach(recordResponse.content, (record: IRecordResponse): void => {
            const row: IInventoryRecord = this.getEmptyRecordModel(record, statusMap);
            row.cells = this.getRecordCells(attributeMap, statusMap, record, InventoryContexts.List);
            recordList.push(row);
        });
        return {
            id: schema.id,
            name: schema.name,
            nameKey: schema.nameKey,
            description: schema.description,
            first: recordResponse.first,
            last: recordResponse.last,
            number: recordResponse.number,
            numberOfElements: recordResponse.numberOfElements,
            size: recordResponse.size,
            sort: recordResponse.sort,
            totalElements: recordResponse.totalElements,
            totalPages: recordResponse.totalPages,
            attributes: schema.attributes,
            records: recordList,
            statusMap,
        };
    }

    private createInventoryPageResponse(records: IRecordsResponse): IInventoryTable {
        const recordList: IInventoryRecord[] = [];
        const assessmentStatusMap: IAssessmentStatusMap = getStatusMap(this.store.getState());
        const attributeMap: IAttributeMap = getAttributeMap(this.store.getState());
        forEach(records.content, (record: IRecordResponse): void => {
            const row: IInventoryRecord = this.getEmptyRecordModel(record, assessmentStatusMap);
            row.cells = this.getRecordCells(attributeMap, assessmentStatusMap, record, InventoryContexts.List);
            recordList.push(row);
        });
        return {
            first: records.first,
            last: records.last,
            number: records.number,
            numberOfElements: records.numberOfElements,
            size: records.size,
            sort: records.sort,
            totalElements: records.totalElements,
            totalPages: records.totalPages,
            records: recordList,
        };
    }

    private createInventoryDetailsResponse(schema: IInventorySchema, record: IRecordResponse, assessmentStatuses: IAssessmentStatusMap): IInventoryDetails {
        if (parseInt(schema.id, 10) === InventoryTableIds.Elements) { // If DE inventory, remove status column
            remove(schema.attributes, (attribute: IAttribute): boolean => attribute.fieldAttributeCode === StatusLinkAttributes[schema.id]);
        }
        const recordDetails: IInventoryRecord = this.getEmptyRecordModel(record, assessmentStatuses);
        const attributeMap: IAttributeMap = this.getAttributeMap(schema);
        recordDetails.cells = this.getRecordCells(attributeMap, assessmentStatuses, record);
        const currentRecordOrgId: string = this.getCurrentRecordOrgId(recordDetails);
        return {
            id: schema.id,
            name: schema.name,
            nameKey: schema.nameKey,
            description: schema.description,
            attributes: schema.attributes,
            record: recordDetails,
            statusMap: assessmentStatuses,
            currentRecordOrgId,
        };
    }

    private getAttributeMap(schema: IInventorySchema): IStringMap<IAttribute> {
        const columnMap = {};
        schema.attributes = sortBy(schema.attributes, ["order"]);
        forEach(schema.attributes, (attribute): void => {
            columnMap[attribute.id] = attribute;
        });
        return columnMap;
    }

    private getUserSelections(options: IInventoryCellValue[]): IInventoryCellValue[] {
        return map(options, (option: IInventoryCellValue): IInventoryCellValue => {
            if (!option.valueId && !option.value) return { value: null, valueId: null };
            if (!option.valueId) return { value: option.value, valueId: null };
            const userMap: IUserMap = getAllUserModels(this.store.getState());
            const user: IUser = userMap[option.valueId];
            if (user && !user.IsActive) return { value: user.FullName || user.Email, valueId: null };
            if (user) return { value: user.FullName || user.Email, valueId: user.Id || option.valueId };
            return { value: this.translatePipe.transform("UserNotFound"), valueId: null };
        });
    }

    private getStatusCell(cell: IInventoryCell, statusMap: IAssessmentStatusMap, recordId?: string): IInventoryCell {
        if (statusMap[recordId] && statusMap[recordId].assessmentId) {
            cell.route = "zen.app.pia.module.datamap.views.assessment.single";
            cell.routeParams = { Id: statusMap[recordId].assessmentId };
            cell.displayValue = this.translatePipe.transform(statusMap[recordId].status);
        } else {
            cell.route = "";
            cell.routeParams = { Id: "" };
            cell.displayValue = this.translatePipe.transform("New");
        }
        return cell;
    }

    private getEmptyRecordModel(record: IRecordResponse, statusMap: IAssessmentStatusMap): IInventoryRecord {
        return {
            id: record.id,
            cells: [],
            canEdit: record.canEdit,
            assessmentOutstanding: this.getRecordIsDisabledByStatus(record.id, statusMap),
        };
    }

    private init(inventoryId: string): void {
        this.destroy();
        this.inventoryId = inventoryId;
        const state: IStoreState = this.store.getState();
        this.currentUser = getCurrentUser(state);
    }

    private destroy(): void {
        this.inventoryId = null;
    }
}
