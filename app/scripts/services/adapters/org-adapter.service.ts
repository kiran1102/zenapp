import _ from "lodash";
import { IOrganization, IOrganizationMap, IOrg, IOrgList, IOrgTable } from "interfaces/org.interface";

export default class OrgAdapter {

    static $inject: string[] = ["store"];

    private index = 0;

    constructor(
        private readonly store: any,
    ) {}

    public normalizeOrg(orgNode) {
        if (!orgNode) throw new Error("org is not defined.");
        this.index = 0;
        return this.normalizeHelper(orgNode, 0, [], { rootId: null });
    }

    private normalizeHelper(
        orgNode: IOrganization,
        hierarchyLevel: number,
        listAcc: IOrganization[],
        tableAcc: IOrganizationMap,
    ) {
        const level: number = hierarchyLevel;
        const childrenIds = (org): string[] => org.Children.map((child: any): string => child.Id);
        const generateOrg = (org): any => ({
            Id: org.Id,
            Name: org.Name,
            PrivacyOfficerId: org.PrivacyOfficerId,
            PrivacyOfficerName: org.PrivacyOfficerName,
            CanDelete: org.CanDelete,
            ParentId: org.ParentId,
            ChildrenIds: childrenIds(org),
            LanguageId: org.LanguageId,
            Level: level,
            Index: this.index,
        });

        listAcc.push(generateOrg(orgNode));

        if (!tableAcc.rootId) {
            tableAcc.rootId = orgNode.Id;
        }
        tableAcc[orgNode.Id] = generateOrg(orgNode);
        this.index++;

        if (orgNode.Children && orgNode.Children.length) {
            for (const child of orgNode.Children) {
                this.normalizeHelper(child, level + 1, listAcc, tableAcc);
            }
        }

        return { orgList: listAcc, orgTable: tableAcc };
    }
}
