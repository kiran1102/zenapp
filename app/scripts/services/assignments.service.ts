import { filter, map } from "lodash";
import { ProtocolService } from "modules/core/services/protocol.service";
import Sections from "oneServices/section.service";

import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import { IProject } from "interfaces/project.interface";
import { ISection } from "interfaces/section.interface";
import { IAssignment, IShareProjectParams } from "interfaces/assignment.interface";
import { IProtocolResponse, IProtocolMessages } from "interfaces/protocol.interface";

export default class Assignments {

    static $inject: string[] = ["$rootScope", "OneProtocol", "Sections"];

    constructor(
        private $rootScope: IExtendedRootScopeService,
        private OneProtocol: ProtocolService,
        private sections: Sections,
    ) {}

    public list(): ng.IPromise<IProtocolResponse<any>> {
        const config = this.OneProtocol.config("GET", "/assignment");
        const messages: IProtocolMessages = { Error: { custom: this.$rootScope.t("CouldNotRetrieveAssigments") } };
        return this.OneProtocol.http(config, messages);
    }

    public get(id: string): ng.IPromise<IProtocolResponse<any>> {
        const config = this.OneProtocol.config("GET", `/assignment/get/${id}`);
        const messages: IProtocolMessages = { Error: { custom: this.$rootScope.t("CouldNotGetAssignment") } };
        return this.OneProtocol.http(config, messages);
    }

    public assignProject(assignment: IAssignment): ng.IPromise<IProtocolResponse<any>> {
        const config = this.OneProtocol.config("POST", `/assignment/addproject`, [], assignment);
        const messages: IProtocolMessages = { Error: { custom: this.$rootScope.t("CouldNotAssignProject", { ProjectTerm: this.$rootScope.customTerms.Project })}};
        return this.OneProtocol.http(config, messages);
    }

    public assignSection(assignment: IAssignment): ng.IPromise<IProtocolResponse<any>> {
        const config = this.OneProtocol.config("POST", `/assignment/addsection`, [], assignment);
        const messages: IProtocolMessages = { Error: { custom: this.$rootScope.t("CouldNotAssignSection") } };
        return this.OneProtocol.http(config, messages);
    }

    public assignProjectEmail(assignment: IAssignment): ng.IPromise<IProtocolResponse<any>> {
        const config = this.OneProtocol.config("POST", `/assignment/addprojectemail`, [], assignment);
        const messages: IProtocolMessages = { Error: { custom: this.$rootScope.t("CouldNotAssignProject", { ProjectTerm: this.$rootScope.customTerms.Project })}};
        return this.OneProtocol.http(config, messages);
    }

    public assignSectionEmail(assignment: IAssignment): ng.IPromise<IProtocolResponse<any>> {
        const config = this.OneProtocol.config("POST", `/assignment/addsectionemail`, [], assignment);
        const messages: IProtocolMessages = { Error: { custom: this.$rootScope.t("CouldNotAssignSection") } };
        return this.OneProtocol.http(config, messages);
    }

    public assignQuestion(assignment: IAssignment): ng.IPromise<IProtocolResponse<any>> {
        const config = this.OneProtocol.config("POST", `/assignment/addquestion`, [], assignment);
        const messages: IProtocolMessages = { Error: { custom: this.$rootScope.t("CouldNotAssignQuestion") } };
        return this.OneProtocol.http(config, messages);
    }

    public delete(id: string): ng.IPromise<IProtocolResponse<any>> {
        const config = this.OneProtocol.config("DELETE", `/assignment/delete/${id}`);
        const messages: IProtocolMessages = { Error: { custom: this.$rootScope.t("CouldNotDeleteAssignment") } };
        return this.OneProtocol.http(config, messages);
    }

    public shareProject(params: IShareProjectParams): ng.IPromise<IProtocolResponse<any>> {
        const config = this.OneProtocol.config("POST", `/assignment/addprojectbulk`, [], params);
        const messages: IProtocolMessages = {
            Error: { custom: this.$rootScope.t("Error.SharingProject") },
            Success: { custom: this.$rootScope.t("Success.SharingProject") },
        };
        return this.OneProtocol.http(config, messages);
    }

    public composeSectionAssignments(project: IProject): IAssignment[] {
        const sectionsNoWelcome: ISection[] = filter(project.Sections, this.sections.excludeWelcomeSection);
        return map(sectionsNoWelcome, (section: ISection): IAssignment => {
            return {
                AssigneeId: null,
                SectionId: section.TemplateSectionId,
                ProjectVersion: project.Version,
            };
        });
    }
}
