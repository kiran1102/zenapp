import RiskPermissionService from "oneServices/permissions/risk-permission.service";

import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import { IUser } from "interfaces/user.interface";
import { IProject } from "interfaces/project.interface";
import { IRisk, IRiskModalViewConfig } from "interfaces/risk.interface";

import { RiskStates } from "enums/risk-states.enum";

export default class RiskModalViewLogicService {
    static $inject: string[] = ["$rootScope", "RiskPermission"];

    constructor(
        private readonly $rootScope: IExtendedRootScopeService,
        private readonly RiskPermission: RiskPermissionService,
    ) {}

    public calculateRiskModalViewState(currentUser: IUser, projectModel: IProject, riskModel: IRisk, tempRiskModel: IRisk): IRiskModalViewConfig {
        return {
            riskLevelShown: this.getRiskLevelShowState(currentUser, projectModel, riskModel),
            riskLevelEnabled: this.getRiskLevelEnableState(currentUser, projectModel, riskModel),
            riskHeatmapShown: this.getRiskHeatmapShowState(currentUser, projectModel, riskModel),
            riskHeatmapEnabled: this.getRiskHeatmapEnableState(currentUser, projectModel, riskModel),
            riskDeadlineShown: this.getRiskDeadlineShowState(currentUser, projectModel, riskModel),
            riskDeadlineEnabled: this.getRiskDeadlineEnableState(currentUser, projectModel, riskModel),
            riskOwnerShown: this.getRiskOwnerShowState(currentUser, projectModel, riskModel),
            riskOwnerEnabled: this.getRiskOwnerEnableState(currentUser, projectModel, riskModel),
            riskSummaryShown: this.getRiskSummaryShowState(currentUser, projectModel, riskModel),
            riskSummaryEnabled: this.getRiskSummaryEnableState(currentUser, projectModel, riskModel),
            riskRecommendationShown: this.getRiskRecommendationShowState(currentUser, projectModel, riskModel),
            riskRecommendationEnabled: this.getRiskRecommendationEnableState(currentUser, projectModel, riskModel),
            riskRemediationShown: this.getRiskRemediationShowState(currentUser, projectModel, riskModel),
            riskRemediationEnabled: this.getRiskRemediationEnableState(currentUser, projectModel, riskModel),
            riskExceptionShown: this.getRiskExceptionShowState(currentUser, projectModel, riskModel),
            riskExceptionEnabled: this.getRiskExceptionEnableState(currentUser, projectModel, riskModel),
        };
    }

    private getRiskLevelShowState(currentUser: IUser, projectModel: IProject, riskModel: IRisk): boolean {
        return this.RiskPermission.hasRiskPermission("canShowRiskLevelInModal", currentUser, projectModel, riskModel);
    }

    private getRiskLevelEnableState(currentUser: IUser, projectModel: IProject, riskModel: IRisk): boolean {
        return this.RiskPermission.hasRiskPermission("canEditRiskLevelInModal", currentUser, projectModel, riskModel);
    }

    private getRiskHeatmapShowState(currentUser: IUser, projectModel: IProject, riskModel: IRisk): boolean {
        return this.RiskPermission.hasRiskPermission("canShowRiskHeatmapInModal", currentUser, projectModel, riskModel);
    }

    private getRiskHeatmapEnableState(currentUser: IUser, projectModel: IProject, riskModel: IRisk): boolean {
        return this.RiskPermission.hasRiskPermission("canEditRiskHeatmapInModal", currentUser, projectModel, riskModel);
    }

    private getRiskDeadlineShowState(currentUser: IUser, projectModel: IProject, riskModel: IRisk): boolean {
        return this.RiskPermission.hasRiskPermission("canShowRiskDeadlineInModal", currentUser, projectModel, riskModel);
    }

    private getRiskDeadlineEnableState(currentUser: IUser, projectModel: IProject, riskModel: IRisk): boolean {
        return this.RiskPermission.hasRiskPermission("canEditRiskDeadlineInModal", currentUser, projectModel, riskModel);
    }

    private getRiskOwnerShowState(currentUser: IUser, projectModel: IProject, riskModel: IRisk): boolean {
        return this.RiskPermission.hasRiskPermission("canShowRiskOwnerInModal", currentUser, projectModel, riskModel);
    }

    private getRiskOwnerEnableState(currentUser: IUser, projectModel: IProject, riskModel: IRisk): boolean {
        return this.RiskPermission.hasRiskPermission("canEditRiskOwnerInModal", currentUser, projectModel, riskModel);
    }

    private getRiskSummaryShowState(currentUser: IUser, projectModel: IProject, riskModel: IRisk): boolean {
        return this.RiskPermission.hasRiskPermission("canShowRiskSummaryInModal", currentUser, projectModel, riskModel);
    }

    private getRiskSummaryEnableState(currentUser: IUser, projectModel: IProject, riskModel: IRisk): boolean {
        return this.RiskPermission.hasRiskPermission("canEditSummaryInModal", currentUser, projectModel, riskModel);
    }

    private getRiskRecommendationShowState(currentUser: IUser, projectModel: IProject, riskModel: IRisk): boolean {
        return this.RiskPermission.hasRiskPermission("canShowRiskRecommendationInModal", currentUser, projectModel, riskModel);
    }

    private getRiskRecommendationEnableState(currentUser: IUser, projectModel: IProject, riskModel: IRisk): boolean {
        return this.RiskPermission.hasRiskPermission("canEditRiskRecommendationInModal", currentUser, projectModel, riskModel);
    }

    private getRiskRemediationShowState(currentUser: IUser, projectModel: IProject, riskModel: IRisk): boolean {
        return riskModel.State === RiskStates.Reduced
            || this.RiskPermission.hasRiskPermission("canShowRiskRemediationInModal", currentUser, projectModel, riskModel);
    }

    private getRiskRemediationEnableState(currentUser: IUser, projectModel: IProject, riskModel: IRisk): boolean {
        return this.RiskPermission.hasRiskPermission("canEditRiskRemediationInModal", currentUser, projectModel, riskModel);
    }

    private getRiskExceptionShowState(currentUser: IUser, projectModel: IProject, riskModel: IRisk): boolean {
        return riskModel.State === RiskStates.Retained
            || this.RiskPermission.hasRiskPermission("canShowRiskExceptionInModal", currentUser, projectModel, riskModel);
    }

    private getRiskExceptionEnableState(currentUser: IUser, projectModel: IProject, riskModel: IRisk): boolean {
        return this.RiskPermission.hasRiskPermission("canEditRiskExceptionInModal", currentUser, projectModel, riskModel);
    }

}
