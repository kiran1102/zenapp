import { IDropdownOption } from "interfaces/one-dropdown.interface";
import invariant from "invariant";
import { IRiskSummaryContentViewConfig, IRisk } from "interfaces/risk.interface";
import { RiskStates } from "enums/risk-states.enum";
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import { IUser } from "interfaces/user.interface";
import { IProject } from "interfaces/project.interface";
import RiskPermissionService from "oneServices/permissions/risk-permission.service";
import { RiskLevels } from "enums/risk.enum";

export const RiskSummaryContentActions = {
    OPEN_RISK_MODAL: "OPEN_RISK_MODAL",
    ADD_RISK_RECOMMENDATION: "ADD_RISK_RECOMMENDATION",
    ADD_RISK_REMEDIATION: "ADD_RISK_REMEDIATION",
    ADD_RISK_EXCEPTION: "ADD_RISK_EXCEPTION",
    SAVE_RISK_RECOMMENDATION: "SAVE_RISK_RECOMMENDATION",
    SAVE_RISK_REMEDIATION: "SAVE_RISK_REMEDIATION",
    SAVE_RISK_EXCEPTION: "SAVE_RISK_EXCEPTION",
    ACCEPT_RISK_REMEDIATION: "ACCEPT_RISK_REMEDIATION",
    REJECT_RISK_REMEDIATION: "REJECT_RISK_REMEDIATION",
    ACCEPT_RISK_EXCEPTION: "ACCEPT_RISK_EXCEPTION",
    REJECT_RISK_EXCEPTION: "REJECT_RISK_EXCEPTION",
    DELETE_RISK_RECOMMENDATION: "DELETE_RISK_RECOMMENDATION",
    DELETE_RISK_REMEDIATION: "DELETE_RISK_REMEDIATION",
    DELETE_RISK_EXCEPTION: "DELETE_RISK_EXCEPTION",
    DELETE_RISK_MODEL: "DELETE_RISK_MODEL",
};

export default class RiskSummaryContentViewService {

    static $inject: string[] = ["$rootScope", "RiskPermission"];

    private riskPermissions;

    constructor(
        private readonly $rootScope: IExtendedRootScopeService,
        private readonly RiskPermission: RiskPermissionService,
    ) {}

    public getRiskStatusText(riskModel: IRisk): string {
        const { Identified, Analysis, Addressed, Exception, Reduced, Retained } = RiskStates;
        const recommendation = this.$rootScope.customTerms.Recommendation;
        const state = riskModel.State;

        switch (state) {
            case Identified:
                return this.$rootScope.t("RiskIdentified");
            case Analysis:
                return this.$rootScope.t("RecommendationAdded", { Recommendation: recommendation });
            case Addressed:
                return this.$rootScope.t("RemediationProposed");
            case Exception:
                return this.$rootScope.t("ExceptionRequested");
            case Reduced:
                return this.$rootScope.t("RiskReduced");
            case Retained:
                return this.$rootScope.t("RiskRetained");
            default:
                return "Something went wrong.";
        }
    }

    public getRiskStatusIconClass(riskModel: IRisk): string {
        const { Identified, Analysis, Addressed, Exception, Reduced, Retained } = RiskStates;
        const state = riskModel.State;

        switch (state) {
            case Identified:
                return "fa fa-flag-o color-cherry";
            case Analysis:
                return "fa fa-book color-accent-blue";
            case Addressed:
                return "fa fa-asterisk color-green";
            case Exception:
                return "fa fa-window-close-o color-accent-blue";
            case Reduced:
                return "fa fa-arrow-down color-green";
            case Retained:
                return "ot ot-flag color-green";
            default:
                return "fa fa-exclamation-circle color-cherry";
        }
    }

    public calculateNewViewState(currentUser: IUser, projectModel: IProject, riskModel: IRisk, tempRiskModel: IRisk | undefined): IRiskSummaryContentViewConfig {
        return {
            riskStatusIconClass: this.getRiskStatusIconClass(riskModel),
            riskStatusText: this.getRiskStatusText(riskModel),
            riskLevelSymbolClass: this.getRiskLevelClass(riskModel),
            riskLevelText: this.getRiskLevelText(riskModel),
            flagRiskButtonEnabled: this.getFlagRiskButtonEnableState(currentUser, projectModel, riskModel),
            openModalButtonEnabled: this.getOpenModalButtonEnableState(currentUser, projectModel, riskModel),
            deleteRiskButtonEnabled: this.getDeleteRiskButtonEnableState(currentUser, projectModel, riskModel),
            canShowDetails: true,
            button1Enabled: this.getButton1EnableState(currentUser, projectModel, riskModel),
            button1Text: this.getButton1Text(currentUser, projectModel, riskModel),
            button1Action: this.getButton1Action(currentUser, projectModel, riskModel),
            button2Enabled: this.getButton2EnableState(currentUser, projectModel, riskModel),
            button2Text: this.getButton2Text(currentUser, projectModel, riskModel),
            button2Action: this.getButton2Action(currentUser, projectModel, riskModel),
            button2Type: this.getButton2Type(currentUser, projectModel, riskModel),
            treatmentLabelText: this.getTreatmentLabelText(riskModel),
            treatmentModelProp: this.getTreatmentModelProp(riskModel),
            textareaLabel: this.getTextareaLabel(tempRiskModel),
            textareaPlaceholder: this.getTextareaPlaceholder(tempRiskModel),
            textareaModelProp: this.getTextareaModelProp(tempRiskModel),
            textareaSaveButtonText: this.getTextareaSaveButtonText(tempRiskModel),
            textareaSubmitAction: this.getTextareaSubmitAction(tempRiskModel),
            contextMenuEnabled: this.getContextMenuEnableState(currentUser, projectModel, riskModel),
            contextMenuOptions: this.getContextMenuOption(riskModel),
        };
    }

    private getRiskLevelClass(riskModel: IRisk): string {
        const { None, Low, Medium, High, VeryHigh } = RiskLevels;
        const riskLevel = riskModel.Level;

        switch (riskLevel) {
            case None:
                return "risk-none--bg border";
            case Low:
                return "risk-low--bg";
            case Medium:
                return "risk-medium--bg";
            case High:
                return "risk-high--bg";
            case VeryHigh:
                return "risk-very-high--bg";
        }
    }

    private getRiskLevelText(riskModel: IRisk): string {
        const { None, Low, Medium, High, VeryHigh } = RiskLevels;
        const riskLevel = riskModel.Level;

        switch (riskLevel) {
            case None:
                return this.$rootScope.t("None");
            case Low:
                return this.$rootScope.t("Low");
            case Medium:
                return this.$rootScope.t("Medium");
            case High:
                return this.$rootScope.t("High");
            case VeryHigh:
                return this.$rootScope.t("VeryHigh");
        }
    }

    private getFlagRiskButtonEnableState(currentUser: IUser, projectModel: IProject, riskModel: IRisk): boolean {
        return this.RiskPermission.hasRiskPermission("canEditRiskLevelInSummary", currentUser, projectModel, riskModel)
            || this.RiskPermission.hasRiskPermission("canEditRiskHeatmapInSummary", currentUser, projectModel, riskModel);
    }

    private getOpenModalButtonEnableState(currentUser: IUser, projectModel: IProject, riskModel: IRisk): boolean {
        const { Retained, Reduced, Archived } = RiskStates;
        // this first check was done to hide the open modal button when risk is completed.
        return (riskModel.State !== Retained
            && riskModel.State !== Reduced
            && riskModel.State !== Archived)
            && (this.RiskPermission.hasRiskPermission("canShowRiskLevelInModal", currentUser, projectModel, riskModel)
                || this.RiskPermission.hasRiskPermission("canShowRiskHeatmapInModal", currentUser, projectModel, riskModel)
                || this.RiskPermission.hasRiskPermission("canShowRiskDeadlineInModal", currentUser, projectModel, riskModel)
                || this.RiskPermission.hasRiskPermission("canShowRiskOwnerInModal", currentUser, projectModel, riskModel)
                || this.RiskPermission.hasRiskPermission("canShowRiskSummaryInModal", currentUser, projectModel, riskModel)
                || this.RiskPermission.hasRiskPermission("canShowRiskRecommendationInModal", currentUser, projectModel, riskModel)
                || this.RiskPermission.hasRiskPermission("canShowRiskRemediationInModal", currentUser, projectModel, riskModel)
                || this.RiskPermission.hasRiskPermission("canShowRiskExceptionInModal", currentUser, projectModel, riskModel)
                || this.RiskPermission.hasRiskPermission("canEditRiskLevelInModal", currentUser, projectModel, riskModel)
                || this.RiskPermission.hasRiskPermission("canDeleteRiskInModal", currentUser, projectModel, riskModel)
                || this.RiskPermission.hasRiskPermission("canEditSummaryInModal", currentUser, projectModel, riskModel)
                || this.RiskPermission.hasRiskPermission("canEditRiskOwnerInModal", currentUser, projectModel, riskModel)
                || this.RiskPermission.hasRiskPermission("canEditRiskRecommendationInModal", currentUser, projectModel, riskModel)
                || this.RiskPermission.hasRiskPermission("canEditRiskRemediationInModal", currentUser, projectModel, riskModel)
                || this.RiskPermission.hasRiskPermission("canEditRiskExceptionInModal", currentUser, projectModel, riskModel)
            );
    }

    private getDeleteRiskButtonEnableState(currentUser: IUser, projectModel: IProject, riskModel: IRisk): boolean {
        return this.RiskPermission.hasRiskPermission("canDeleteRiskInSummary", currentUser, projectModel, riskModel);
    }

    private getButton1EnableState(currentUser: IUser, projectModel: IProject, riskModel: IRisk): boolean {
        return this.RiskPermission.hasRiskPermission("canAddRiskRecommendationInSummary", currentUser, projectModel, riskModel)
            || this.RiskPermission.hasRiskPermission("canClickAddRiskExceptionInSummary", currentUser, projectModel, riskModel)
            || this.RiskPermission.hasRiskPermission("canRejectRemediationInSummary", currentUser, projectModel, riskModel)
            || this.RiskPermission.hasRiskPermission("canRejectExceptionInSummary", currentUser, projectModel, riskModel);
    }

    private getButton1Text(currentUser: IUser, projectModel: IProject, riskModel: IRisk): string {
        if (this.RiskPermission.hasRiskPermission("canAddRiskRecommendationInSummary", currentUser, projectModel, riskModel)) {
            return this.$rootScope.t("AddRecommendation", { Recommendation: this.$rootScope.customTerms.Recommendation });
        } else if (this.RiskPermission.hasRiskPermission("canClickAddRiskExceptionInSummary", currentUser, projectModel, riskModel)) {
            return this.$rootScope.t("RequestException");
        } else if (this.RiskPermission.hasRiskPermission("canRejectRemediationInSummary", currentUser, projectModel, riskModel)) {
            return this.$rootScope.t("RejectRemediation");
        } else if (this.RiskPermission.hasRiskPermission("canRejectExceptionInSummary", currentUser, projectModel, riskModel)) {
            return this.$rootScope.t("RejectException");
        }
        return "Something went wrong";
    }

    private getButton1Action(currentUser: IUser, projectModel: IProject, riskModel: IRisk) {
        if (this.RiskPermission.hasRiskPermission("canAddRiskRecommendationInSummary", currentUser, projectModel, riskModel)) {
            return RiskSummaryContentActions.ADD_RISK_RECOMMENDATION;
        } else if (this.RiskPermission.hasRiskPermission("canClickAddRiskExceptionInSummary", currentUser, projectModel, riskModel)) {
            return RiskSummaryContentActions.ADD_RISK_EXCEPTION;
        } else if (this.RiskPermission.hasRiskPermission("canRejectRemediationInSummary", currentUser, projectModel, riskModel)) {
            return RiskSummaryContentActions.REJECT_RISK_REMEDIATION;
        } else if (this.RiskPermission.hasRiskPermission("canRejectExceptionInSummary", currentUser, projectModel, riskModel)) {
            return RiskSummaryContentActions.REJECT_RISK_EXCEPTION;
        }
    }

    private getButton2EnableState(currentUser: IUser, projectModel: IProject, riskModel: IRisk): boolean {
        return this.RiskPermission.hasRiskPermission("canClickAddRiskRemediationInSummary", currentUser, projectModel, riskModel)
            || this.RiskPermission.hasRiskPermission("canApproveRemediationInSummary", currentUser, projectModel, riskModel)
            || this.RiskPermission.hasRiskPermission("canApproveExceptionInSummary", currentUser, projectModel, riskModel);
    }

    private getButton2Text(currentUser: IUser, projectModel: IProject, riskModel: IRisk): string {
        if (this.RiskPermission.hasRiskPermission("canClickAddRiskRemediationInSummary", currentUser, projectModel, riskModel)) {
            return this.$rootScope.t("ProposeRemediation");
        } else if (this.RiskPermission.hasRiskPermission("canApproveRemediationInSummary", currentUser, projectModel, riskModel)) {
            return this.$rootScope.t("ApproveRemediation");
        } else if (this.RiskPermission.hasRiskPermission("canApproveExceptionInSummary", currentUser, projectModel, riskModel)) {
            return this.$rootScope.t("GrantException");
        }
        return "Something went wrong";
    }

    private getButton2Action(currentUser: IUser, projectModel: IProject, riskModel: IRisk): string {
        if (this.RiskPermission.hasRiskPermission("canClickAddRiskRemediationInSummary", currentUser, projectModel, riskModel)) {
            return RiskSummaryContentActions.ADD_RISK_REMEDIATION;
        } else if (this.RiskPermission.hasRiskPermission("canApproveRemediationInSummary", currentUser, projectModel, riskModel)) {
            return RiskSummaryContentActions.ACCEPT_RISK_REMEDIATION;
        } else if (this.RiskPermission.hasRiskPermission("canApproveExceptionInSummary", currentUser, projectModel, riskModel)) {
            return RiskSummaryContentActions.ACCEPT_RISK_EXCEPTION;
        }
    }

    private getButton2Type(currentUser: IUser, projectModel: IProject, riskModel: IRisk): string {
        if (this.RiskPermission.hasRiskPermission("canApproveRemediationInSummary", currentUser, projectModel, riskModel)) {
            return "primary";
        } else if (this.RiskPermission.hasRiskPermission("canApproveExceptionInSummary", currentUser, projectModel, riskModel)) {
            return "primary";
        }
        return "";
    }

    private getTreatmentLabelText(riskModel: IRisk): string {
        const { Identified, Analysis, Addressed, Exception, Reduced, Retained } = RiskStates;

        switch (riskModel.State) {
            case Reduced:
                return this.$rootScope.t("Remediation");
            case Retained:
                return this.$rootScope.t("Exception");
            case Identified:
            case Analysis:
                return `${this.$rootScope.t("Exception")} / ${this.$rootScope.t("Remediation")}`;
            case Addressed:
                return this.$rootScope.t("Remediation");
            case Exception:
                return this.$rootScope.t("Exception");
            default:
                return "";
        }
    }

    private getTreatmentModelProp(riskModel: IRisk): string {
        const { Identified, Analysis, Addressed, Exception, Reduced, Retained } = RiskStates;

        switch (riskModel.State) {
            case Reduced:
                return "Mitigation";
            case Retained:
                return "RequestedException";
            case Identified:
            case Analysis:
                return "";
            case Addressed:
                return "Mitigation";
            case Exception:
                return "RequestedException";
            default:
                return "";
        }
    }

    private getTextareaLabel(riskModel: IRisk | undefined): string {
        if (!riskModel) return "";
        const { Identified, Analysis, Addressed, Exception, Reduced, Retained } = RiskStates;

        switch (riskModel.State) {
            case Reduced:
            case Retained:
                return "";
            case Identified:
            case Analysis:
                return "";
            case Addressed:
                return this.$rootScope.t("ProposalRequest");
            case Exception:
                return this.$rootScope.t("Exception");
            default:
                return "";
        }
    }

    private getTextareaPlaceholder(riskModel: IRisk | undefined): string {
        if (!riskModel) return "";
        const { Identified, Analysis, Addressed, Exception, Reduced, Retained } = RiskStates;

        switch (riskModel.State) {
            case Reduced:
            case Retained:
                return "";
            case Identified:
            case Analysis:
                return this.$rootScope.t("PleaseProvideRecommendation", { Recommendation: this.$rootScope.customTerms.Recommendation });
            case Addressed:
                return this.$rootScope.t("PleaseProvideRemediation");
            case Exception:
                return this.$rootScope.t("PleaseProvideJustification");
            default:
                return "";
        }
    }

    private getTextareaModelProp(riskModel: IRisk | undefined): string {
        if (!riskModel) return "";
        const { Identified, Analysis, Addressed, Exception, Reduced, Retained } = RiskStates;

        switch (riskModel.State) {
            case Reduced:
            case Retained:
                return "";
            case Identified:
            case Analysis:
                return "Recommendation";
            case Addressed:
                return "Mitigation";
            case Exception:
                return "RequestedException";
            default:
                return "";
        }
    }

    private getTextareaSaveButtonText(riskModel: IRisk | undefined): string {
        if (!riskModel) return "";
        const { Identified, Analysis, Addressed, Exception, Reduced, Retained } = RiskStates;

        switch (riskModel.State) {
            case Reduced:
            case Retained:
                return "";
            case Identified:
            case Analysis:
                return this.$rootScope.t("SaveRecommendation", { Recommendation: this.$rootScope.customTerms.Recommendation });
            case Addressed:
                return this.$rootScope.t("SaveRemediation");
            case Exception:
                return this.$rootScope.t("SaveException");
            default:
                return "";
        }
    }

    private getTextareaSubmitAction(riskModel: IRisk | undefined): string {
        if (!riskModel) return "";
        const { Identified, Analysis, Addressed, Exception, Reduced, Retained } = RiskStates;

        switch (riskModel.State) {
            case Reduced:
            case Retained:
                return "";
            case Identified:
            case Analysis:
                return RiskSummaryContentActions.SAVE_RISK_RECOMMENDATION;
            case Addressed:
                return RiskSummaryContentActions.SAVE_RISK_REMEDIATION;
            case Exception:
                return RiskSummaryContentActions.SAVE_RISK_EXCEPTION;
            default:
                return "";
        }
    }

    private getContextMenuEnableState(currentUser: IUser, projectModel: IProject, riskModel: IRisk): boolean {
        return this.RiskPermission.hasRiskPermission("canDeleteRiskRecommendationInSummary", currentUser, projectModel, riskModel)
            || this.RiskPermission.hasRiskPermission("canDeleteRiskRemediationInSummary", currentUser, projectModel, riskModel)
            || this.RiskPermission.hasRiskPermission("canDeleteRiskExceptionInSummary", currentUser, projectModel, riskModel);
    }

    private getContextMenuOption(riskModel: IRisk): IDropdownOption[] {
        if (!riskModel) return [];
        const { Identified, Analysis, Addressed, Exception, Reduced, Retained } = RiskStates;

        switch (riskModel.State) {
            case Reduced:
            case Retained:
            case Identified:
                return [];
            case Analysis:
                return [
                    {
                        text: this.$rootScope.t("ClearRecommendation", { Recommendation: this.$rootScope.customTerms.Recommendation }),
                        value: RiskSummaryContentActions.DELETE_RISK_RECOMMENDATION,
                    },
                ];
            case Addressed:
                return [
                    {
                        text: this.$rootScope.t("ClearRemediation"),
                        value: RiskSummaryContentActions.DELETE_RISK_REMEDIATION,
                    },
                ];
            case Exception:
                return [
                    {
                        text: this.$rootScope.t("ClearException"),
                        value: RiskSummaryContentActions.DELETE_RISK_EXCEPTION,
                    },
                ];
            default:
                return [];
        }
    }

}
