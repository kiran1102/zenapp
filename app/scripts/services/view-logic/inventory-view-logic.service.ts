// 3rd party
import { Injectable } from "@angular/core";
import { find } from "lodash";

// Interfaces
import { IInventoryRecord, IInventoryCell } from "interfaces/inventory.interface";
import { IStringMap } from "interfaces/generic.interface";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

// Enums and Constants
import { InventoryTableIds } from "enums/inventory.enum";
import { NamesAttributes } from "constants/inventory.constant";
import { InventorySchemaIds } from "constants/inventory-config.constant";

@Injectable()
export class InventoryViewLogic {

    private inventoryLabels: IStringMap<IStringMap<string>> = {
        [InventoryTableIds.Assets]: {
            addModalTitle: "AddAsset",
            attributeDrawerHeader: "Inventory.Asset.Attributes",
        },
        [InventoryTableIds.Elements]: {
            addModalTitle: "AddElement",
            attributeDrawerHeader: "Inventory.Element.Attributes",
        },
        [InventoryTableIds.Processes]: {
            addModalTitle: "AddProcessingActivity",
            attributeDrawerHeader: "Inventory.PA.Attributes",
        },
        [InventoryTableIds.Vendors]: {
            addModalTitle: "AddVendor",
            attributeDrawerHeader: "Inventory.Vendor.Attributes",
        },
        [InventoryTableIds.Entities]: {
            addModalTitle: "AddLegalEntity",
            attributeDrawerHeader: "Inventory.Entity.Attributes",
        },
    };

    constructor(
        readonly translatePipe: TranslatePipe,
    ) {}

    getLabel(inventoryId: number, labelKey: string): string {
        return this.translatePipe.transform(this.inventoryLabels[inventoryId][labelKey]);
    }

    getRecordName(record: IInventoryRecord, inventoryId: string): string {
        return find(record.cells, (attributes: IInventoryCell): boolean => attributes.attributeCode === NamesAttributes[inventoryId]).displayValue;
    }
}
