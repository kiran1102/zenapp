// 3rd party
import { Injectable, Inject } from "@angular/core";

// redux
import { getInventoryId } from "oneRedux/reducers/inventory.reducer";
import { getCurrentInventoryId } from "oneRedux/reducers/inventory-record.reducer";

// Interfaces
import { IUser } from "interfaces/user.interface";
import { IProject } from "interfaces/project.interface";
import { IRisk, IRiskModalViewConfig } from "interfaces/risk.interface";
import { IStore } from "interfaces/redux.interface";

// Services
import { Permissions } from "modules/shared/services/helper/permissions.service";

// Enums and Constants
import { InventoryPermissions } from "constants/inventory.constant";

// Tokens
import { StoreToken } from "tokens/redux-store.token";

@Injectable()
export class InventoryRiskModalViewLogicService {

    constructor(
        @Inject(StoreToken) private store: IStore,
        readonly permissions: Permissions,
    ) {}

    public calculateRiskModalViewState = (currentUser: IUser, riskModel: IRisk, tempRiskModel: IRisk): IRiskModalViewConfig => {
        return {
            riskLevelShown: this.getRiskLevelShowState(currentUser, riskModel),
            riskLevelEnabled: this.getRiskLevelEnableState(currentUser, riskModel),
            riskHeatmapShown: this.getRiskHeatmapShowState(currentUser, riskModel),
            riskHeatmapEnabled: this.getRiskHeatmapEnableState(currentUser, riskModel),
            riskDeadlineShown: this.getRiskDeadlineShowState(currentUser, riskModel),
            riskDeadlineEnabled: this.getRiskDeadlineEnableState(currentUser, riskModel),
            riskOwnerShown: this.getRiskOwnerShowState(currentUser, riskModel),
            riskOwnerEnabled: this.getRiskOwnerEnableState(currentUser, riskModel),
            riskSummaryShown: this.getRiskSummaryShowState(currentUser, riskModel),
            riskSummaryEnabled: this.getRiskSummaryEnableState(currentUser, riskModel),
            riskRecommendationShown: this.getRiskRecommendationShowState(currentUser, riskModel),
            riskRecommendationEnabled: this.getRiskRecommendationEnableState(currentUser, riskModel),
            riskRemediationShown: this.getRiskRemediationShowState(currentUser, riskModel),
            riskRemediationEnabled: this.getRiskRemediationEnableState(currentUser, riskModel),
            riskExceptionShown: this.getRiskExceptionShowState(currentUser, riskModel),
            riskExceptionEnabled: this.getRiskExceptionEnableState(currentUser, riskModel),
        };
    }

    private getRiskLevelShowState(currentUser: IUser, riskModel: IRisk): boolean {
        // TODO uncomment when using heatmap
        // return this.hasInventoryPermission("riskLevelView") && !getRiskSettings(this.store.getState()).Enabled;
        return this.hasInventoryPermission("riskLevelView");
    }

    private getRiskLevelEnableState(currentUser: IUser, riskModel: IRisk): boolean {
        // TODO uncomment when using heatmap
        // return this.hasInventoryPermission("editRiskLevel") && !getRiskSettings(this.store.getState()).Enabled;
        return this.hasInventoryPermission("editRiskLevel");
    }

    private getRiskHeatmapShowState(currentUser: IUser, riskModel: IRisk): boolean {
        // TODO uncomment when using heatmap
        // return this.hasInventoryPermission("riskLevelView") && getRiskSettings(this.store.getState()).Enabled;
        return false;
    }

    private getRiskHeatmapEnableState(currentUser: IUser, riskModel: IRisk): boolean {
        // TODO uncomment when using heatmap
        // return this.hasInventoryPermission("riskLevelView") && getRiskSettings(this.store.getState()).Enabled;
        return false;
    }

    private getRiskDeadlineShowState(currentUser: IUser, riskModel: IRisk): boolean {
        return this.hasInventoryPermission("riskDeadlineView");
    }

    private getRiskDeadlineEnableState(currentUser: IUser, riskModel: IRisk): boolean {
        return this.hasInventoryPermission("riskDeadlineEdit");
    }

    private getRiskOwnerShowState(currentUser: IUser, riskModel: IRisk): boolean {
        return this.hasInventoryPermission("riskOwnerView");
    }

    private getRiskOwnerEnableState(currentUser: IUser, riskModel: IRisk): boolean {
        return this.hasInventoryPermission("riskOwnerEdit");
    }

    private getRiskSummaryShowState(currentUser: IUser, riskModel: IRisk): boolean {
        return this.hasInventoryPermission("riskSummaryView");
    }

    private getRiskSummaryEnableState(currentUser: IUser, riskModel: IRisk): boolean {
        return this.hasInventoryPermission("riskSummaryEdit");
    }

    private getRiskRecommendationShowState(currentUser: IUser, riskModel: IRisk): boolean {
        return this.hasInventoryPermission("riskRecommendationView");
    }

    private getRiskRecommendationEnableState(currentUser: IUser, riskModel: IRisk): boolean {
        return this.hasInventoryPermission("riskRecommendationEdit");
    }

    private getRiskRemediationShowState(currentUser: IUser, riskModel: IRisk): boolean {
        return this.hasInventoryPermission("riskRemediationView");
    }

    private getRiskRemediationEnableState(currentUser: IUser, riskModel: IRisk): boolean {
        return this.hasInventoryPermission("riskRemediationEdit");
    }

    private getRiskExceptionShowState(currentUser: IUser, riskModel: IRisk): boolean {
        return this.hasInventoryPermission("riskExceptionRequestView");
    }

    private getRiskExceptionEnableState(currentUser: IUser, riskModel: IRisk): boolean {
        return this.hasInventoryPermission("riskExceptionRequestEdit");
    }

    private hasInventoryPermission(permission: string): boolean {
        const currentInventoryId: number | string = this.permissions.canShow("InventoryLinkingEnabled") ? getCurrentInventoryId(this.store.getState()) : getInventoryId(this.store.getState());
        return this.permissions.canShow(InventoryPermissions[currentInventoryId][permission]);
    }
}
