// 3rd Party
import {
    forEach,
    filter,
    isArray,
} from "lodash";

// Services
import { ProtocolService } from "modules/core/services/protocol.service";

// Interfaces
import { ISamlSettingsResponse } from "interfaces/setting.interface";
import { IPageableOf } from "interfaces/pagination.interface";
import {
    IProtocolResponse,
    IProtocolPacket,
    IProtocolConfig,
    IProtocolMessages,
} from "interfaces/protocol.interface";
import {
    IAttachmentFile,
    IAttachmentFileResponse,
    IAttachmentFileType,
} from "interfaces/attachment-file.interface";
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import { IStringMap } from "interfaces/generic.interface";
import { IFetchDetailsParams } from "interfaces/inventory.interface";

export class AttachmentService {

    static $inject: string[] = [
        "$rootScope",
        "OneProtocol",
        "$q",
    ];

    constructor(
        readonly $rootScope: IExtendedRootScopeService,
        readonly OneProtocol: ProtocolService,
        readonly $q: ng.IQService,
    ) { }

    public getAttachmentFileTypes(): ng.IPromise<IProtocolResponse<IAttachmentFileType[]>> {
        const config: IProtocolConfig = this.OneProtocol.config("GET", `/settings/getfile`);
        const messages: IProtocolMessages = {Error: { Hide: true }};
        return this.OneProtocol.http(config, messages);
    }

    public getAttachmentList(refId: string, params: IStringMap<number>): ng.IPromise<IProtocolResponse<IAttachmentFileResponse[]>> {
        const config: IProtocolConfig = this.OneProtocol.config("GET", `/file/attachments/${refId}`, params);
        const messages: IProtocolMessages = { Error: { custom: this.$rootScope.t("ErrorRetrievingResponses") } };
        return this.OneProtocol.http(config, messages);
    }

    public getPageableAttachmentList(refId: string, params: IStringMap<number> | IFetchDetailsParams): ng.IPromise<IProtocolResponse<IPageableOf<IAttachmentFileResponse>>> {
        const config: IProtocolConfig = this.OneProtocol.config("GET", `/file/attachments/pages/${refId}`, params);
        const messages: IProtocolMessages = { Error: { custom: this.$rootScope.t("ErrorRetrievingResponses") } };
        return this.OneProtocol.http(config, messages);
    }

    public addAttachment(file: IAttachmentFile): ng.IPromise<IProtocolResponse<IAttachmentFile>> {
        const fileData: FormData = new FormData();
        fileData.append("File", file.Blob);
        fileData.append("FileName", file.FileName);
        fileData.append("Name", file.Name);
        fileData.append("Type", `${file.Type}`);
        fileData.append("Encrypt", "true");
        fileData.append("IsInternal", `${file.IsInternal}`);
        fileData.append("FileRefId", file.RequestId);
        fileData.append("RefIds", file.RequestId);
        fileData.append("Comments", file.Comments);

        const config: IProtocolConfig = {
            method: "POST",
            url: "/file/addFile",
            body: fileData,
            multipartFormData: true,
        };
        const messages: IProtocolMessages = { Error: { custom: this.$rootScope.t("ErrorUploadingFiles") } };
        return this.OneProtocol.http(config, messages);
    }

    public downloadAttachment(attachmentId: string): ng.IPromise<IProtocolResponse<any>> {
        const config: IProtocolConfig = {
            method: "GET",
            url: `/file/downloadFile/${attachmentId}`,
            responseType: "arraybuffer",
        };
        const messages: IProtocolMessages = { Error: { custom: this.$rootScope.t("ErrorDownloadingFile") } };
        return this.OneProtocol.http(config, messages);
    }

    public deleteAttachment(attachmentId: string): ng.IPromise<IProtocolResponse<ISamlSettingsResponse>> {
        const config: IProtocolConfig = this.OneProtocol.config("DELETE", `/file/deleteFile/${attachmentId}`);
        const messages: IProtocolMessages = { Error: { custom: this.$rootScope.t("ErrorDeletingFile") } };
        return this.OneProtocol.http(config, messages);
    }

    public editAttachment(attachmentId: string, editData: IStringMap<string>): ng.IPromise<IProtocolResponse<IAttachmentFileResponse[]>> {
        const config: IProtocolConfig = this.OneProtocol.config("PUT", `/file/attachments/${attachmentId}`, null, editData);
        const messages: IProtocolMessages = { Error: { custom: this.$rootScope.t("ErrorEditingAttachment") } };
        return this.OneProtocol.http(config, messages);
    }

    public handleAttachmentUpload(
        attachments: IAttachmentFile[],
        requestId: string,
        isInternal: boolean,
        attachmentType: number,
        attachmentName?: string,
        attachmentComment: string = "",
    ): ng.IPromise<IProtocolResponse<IAttachmentFileResponse[]>> {
        const promiseArr: Array<ng.IPromise<IProtocolResponse<IAttachmentFile>>> = [];
        forEach(attachments, (file: IAttachmentFile) => {
            const fileData: IAttachmentFile = {
                FileName: `${file.FileName}.${file.Extension}`,
                Extension: file.Extension,
                Blob: file.Blob,
                IsInternal: isInternal,
                Type: attachmentType,
                Name: attachmentName || file.FileName,
                RequestId: requestId,
                Comments: attachmentComment,
            };
            promiseArr.push(this.addAttachment(fileData));
        });
        return this.$q.all(promiseArr).then((responses: Array<IProtocolResponse<IAttachmentFile>>): IProtocolResponse<IAttachmentFileResponse[]> => {
            const attachmentResponse: IAttachmentFileResponse[] = [];
            if (isArray(responses) && responses.length) {
                const failedResponses: Array<IProtocolResponse<IAttachmentFile>> = filter(responses, {
                    result: false,
                });
                if (!failedResponses.length) {
                    forEach(responses, (res: IProtocolResponse<IAttachmentFile>): void => {
                        const attachmentObjectForComment: IAttachmentFileResponse = {
                            Id: res.data.Id,
                            FileName: res.data.FileName,
                        };
                        attachmentResponse.push(attachmentObjectForComment);
                    });
                    return { result: true, data: attachmentResponse };
                } else {
                    return { result: false, data: attachmentResponse };
                }
            }
        });
    }
}
