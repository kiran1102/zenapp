// 3rd Party
import { isString } from "lodash";

// Services
import OrgGroupStoreNew from "oneServices/org-group.store";
import OrgTreeNormalizer from "../../Shared/Services/org-tree-normalizer";

// Reducers
import {
    getOrgById,
    getOrgList,
    getParentOrgs,
    getCurrentOrgId,
    getCurrentRelatedOrgIds,
    OrgActions,
} from "oneRedux/reducers/org.reducer";

// Interfaces
import { IStore } from "interfaces/redux.interface";
import { IOrganization } from "interfaces/org.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";
import { IStringMap } from "interfaces/generic.interface";

export class OrgActionService {

    static $inject: string[] = ["store", "OrgGroups", "OrgAdapter", "OrgGroupStoreNew"];

    constructor(
        private readonly store: IStore,
        private readonly OrgGroups: any,
        private readonly OrgAdapter: any,
        private readonly orgGroupStoreNew: OrgGroupStoreNew,
    ) {}

    public fetchOrg(rootOrgId: string, selectedOrgId: string): ng.IPromise<IOrganization> {
        return this.OrgGroups.rootTree().then((rootOrgTree: IOrganization): IOrganization => {
            if (!rootOrgTree) {
                return null;
            }

            const { orgList, orgTable } = this.OrgAdapter.normalizeOrg(rootOrgTree);
            this.store.dispatch({ type: OrgActions.SET_ROOT_ORG_TREE, rootOrgTreeToSet: rootOrgTree });
            this.store.dispatch({ type: OrgActions.SET_ROOT_ORG_LIST, rootOrgListToSet: orgList });
            this.store.dispatch({ type: OrgActions.SET_ROOT_ORG_TABLE, rootOrgTableToSet: orgTable });

            if (selectedOrgId) {
                this.setCurrentOrg(selectedOrgId);
            }
            // TODO: Remove this once all Org Store references are consolidated.
            this.orgGroupStoreNew.handleLegacyStoreFetch(rootOrgId, selectedOrgId, rootOrgTree);
            return rootOrgTree;
        });
    }

    public setCurrentOrg(orgId: string): void {
        if (!isString(orgId)) throw new Error("id must be a string.");
        this.store.dispatch({ type: OrgActions.SET_CURRENT_ORG, currentOrgId: orgId });
    }

    public getOrgList() {
        const selectedOrgId = getCurrentOrgId(this.store.getState());
        const selectedOrg = getOrgById(selectedOrgId)(this.store.getState());
        const orgList = getOrgList(selectedOrgId)(this.store.getState());
    }

    public getParentOrgs() {
        const parentOrgs = getCurrentRelatedOrgIds(this.store.getState());
    }

    public updateOrgGroup(params: any): ng.IPromise<IProtocolResponse<void>> {
        return this.OrgGroups.update(params);
    }

    public deleteOrgGroup(id: string): ng.IPromise<IProtocolResponse<void>> {
        return this.OrgGroups.delete(id);
    }

    public addOrgGroup(params: any): ng.IPromise<IProtocolResponse<void>> {
        return this.OrgGroups.create(params);
    }

    public reorderOrg(params: IStringMap<string>): ng.IPromise<IProtocolResponse<void>> {
        return this.OrgGroups.reorder(params);
    }
}
