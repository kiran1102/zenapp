// Angular
import { Injectable, Inject } from "@angular/core";

// External
import { uniqueId } from "lodash";
import invariant from "invariant";

// Redux
import { getRiskById, RiskActions } from "oneRedux/reducers/risk.reducer";

// Services
import { RiskAPIService } from "oneServices/api/risk-api.service";
import { ModalService } from "sharedServices/modal.service";

// Interfaces
import {
    IRisk,
    IFetchingRisksResponse,
    IRiskDetails,
    IRiskModalData,
    IRiskCreateRequest,
} from "interfaces/risk.interface";
import { IProtocolPacket } from "interfaces/protocol.interface";
import { IStore } from "interfaces/redux.interface";
import { IConfirmationModalResolve } from "interfaces/confirmation-modal-resolve.interface";
import { IDeleteConfirmationModalResolve } from "interfaces/delete-confirmation-modal-resolve.interface";
import { IStringMap } from "interfaces/generic.interface";

// Enums
import { RiskStates } from "enums/risk-states.enum";
import { RiskV2States } from "enums/riskV2.enum";
import { RiskTypes } from "enums/risk.enum";
import {
    RiskV2SourceTypes,
    RiskV2ReferenceTypes,
} from "enums/riskV2.enum";
import { InventoryTableIds } from "enums/inventory.enum";
import { RiskStateTransitionTypes } from "enums/risk-state-transition-types.enum";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";
import { StoreToken } from "tokens/redux-store.token";

@Injectable()
export class RiskActionService {

    constructor(
        @Inject(StoreToken) private store: IStore,
        private riskAPI: RiskAPIService,
        private modalService: ModalService,
        private translatePipe: TranslatePipe,
    ) { }

    addRisk(riskModel: IRisk | IRiskDetails, idKey: string = "Id"): void {
        this.store.dispatch({ type: RiskActions.ADD_RISK, riskToAdd: riskModel, idKey });
    }

    addTempRisk(riskId: string, idKey: string = "Id"): void {
        const riskModel: IRisk | IRiskDetails = getRiskById(riskId)(this.store.getState());
        this.store.dispatch({ type: RiskActions.ADD_TEMP_RISK, tempRiskToAdd: riskModel, idKey });
    }

    // TODO deprecate when V2 api used by PIA
    addBlankRiskModel(questionId: string): string {
        const tempId = `temp-risk-${uniqueId()}`;
        const blankRiskModel: IRisk = {
            ConditionGroupId: null,
            CurrentProjectVersion: null,
            Deadline: null,
            Description: "",
            Id: tempId,
            Level: null,
            MitigatedDt: null,
            Mitigation: null,
            OriginalProjectVersion: null,
            ProjectId: "",
            QuestionId: questionId,
            Recommendation: "",
            ReminderDays: null,
            RiskImpactLevel: null,
            RiskOwnerId: null,
            RiskProbabilityLevel: null,
            PreviousState: 10,
            State: 10,
            isSaving: false,
        };
        this.store.dispatch({ type: RiskActions.ADD_BLANK_RISK, tempRiskToAdd: blankRiskModel });
        return tempId;
    }

    getBlankRiskModel(subjectId: string, type: number, sourceType: number, inventoryName: string, inventoryType: InventoryTableIds, inventoryOrgId: string): IRiskCreateRequest {
        return {
            id: null,
            level: "",
            conditionGroupUuid: null,
            deadline: null,
            description: "",
            levelId: null,
            mitigatedDate: null,
            mitigation: null,
            recommendation: "",
            reminderDays: null,
            impactLevel: null,
            riskOwnerId: null,
            probabilityLevel: null,
            previousState: null,
            state: RiskV2States.IDENTIFIED,
            typeId: type,
            type: RiskTypes[type].toUpperCase(),
            sourceTypeId: sourceType,
            sourceType: RiskV2SourceTypes[sourceType],
            requestedException: null,
            orgGroup: null,
            orgGroupId: inventoryOrgId,
            references: [{
                id: subjectId,
                typeId: type,
                name: inventoryName,
                version: 1,
                type: RiskV2ReferenceTypes[type],
                additionalAttributes: {
                    inventoryType,
                },
            }],
            source : {
                id: subjectId,
                name: inventoryName,
                version: 0,
                type: RiskTypes[type].toUpperCase(),
                additionalAttributes: {
                    inventoryType,
                },
            },
            categoryIds: null,
        };
    }

    addBlankRiskModelV2(subjectId: string, type: number, sourceType: number, inventoryName: string, inventoryType: InventoryTableIds, inventoryOrgId: string): string {
        const tempId = `temp-risk-${uniqueId()}`;
        const blankRiskModel = {
            ...this.getBlankRiskModel(subjectId, type, sourceType, inventoryName, inventoryType, inventoryOrgId),
            id: tempId,
        };
        this.store.dispatch({ type: RiskActions.ADD_BLANK_RISK, tempRiskToAdd: blankRiskModel, idKey: "id" });
        return tempId;
    }

    fetchRisksByProjectId(projectId: string, projectVersion: number, pageMetadata: any): ng.IPromise<IFetchingRisksResponse> {
        return this.riskAPI.fetchRisksByProjectId(projectId, projectVersion, pageMetadata).then((res: IFetchingRisksResponse): IFetchingRisksResponse => {
            const allIds: string[] = res.AllIds;
            const byId: Map<string, IRisk> = res.ById;
            this.store.dispatch({ type: RiskActions.RISKS_FETCHED, allIds, byId });
            return res;
        });
    }

    updateRisk(riskId: string, keyValueToMerge: any): void {
        this.store.dispatch({ type: RiskActions.UPDATE_RISK, riskId, keyValueToMerge: { ...keyValueToMerge } });
    }

    updateTempRisk(riskIdTemp: string, keyValueToMergeTemp: any): void {
        this.store.dispatch({ type: RiskActions.UPDATE_TEMP_RISK, riskIdTemp, keyValueToMergeTemp: { ...keyValueToMergeTemp } });
    }

    updateRiskWithTempProp(riskId: string, keyValueToMerge: any): void {
        this.store.dispatch({ type: RiskActions.UPDATE_RISK_WITH_TEMP_PROP, riskId, keyValueToMerge: { ...keyValueToMerge } });
    }

    // TODO: Deprecate once assessment risk modal migrated to shared risk modal
    saveQuestionRisk(riskModel: IRisk): ng.IPromise<string> {
        return this.riskAPI.saveQuestionRisk(riskModel);
    }

    public saveRiskV2(riskModel: IRiskCreateRequest): ng.IPromise<IRiskDetails | boolean> {
        return this.riskAPI.saveRiskV2(riskModel);
    }

    updateRiskV2(riskModel: IRiskDetails, keyValue?: IStringMap<any>): ng.IPromise<IRiskDetails> {
        if (keyValue) riskModel = { ...riskModel, ...keyValue };
        riskModel.references = riskModel.references || [{ ...riskModel.source, version: null }];
        return this.riskAPI.updateRiskV2(riskModel).then((response: IRiskDetails): IRiskDetails => {
            if (response) {
                this.deleteTempRisk(riskModel.id);
                this.store.dispatch({ type: RiskActions.UPDATE_RISK, riskId: riskModel.id, keyValueToMerge: response });
            }
            return response;
        });
    }

    deleteRisk(riskId: string, questionId: string): ng.IPromise<boolean> {
        if (!riskId) throw new Error("Risk Id cannot be undefined");
        return this.riskAPI.deleteRisk(questionId).then((res: IProtocolPacket): boolean => {
            if (res.result) {
                this.store.dispatch({ type: RiskActions.DELETE_RISK, riskToDelete: riskId });
                this.unSelectRisk(riskId);
            }
            return res.result;
        });
    }

    deleteRiskV2(riskId: string): ng.IPromise<boolean> {
        if (!riskId) throw new Error("Risk Id cannot be undefined");
        return this.riskAPI.deleteRiskV2(riskId).then((result: boolean): boolean => {
            if (result) this.store.dispatch({ type: RiskActions.DELETE_RISK, riskToDelete: riskId });
            return result;
        });
    }

    // TODO: Deprecate once assessment risk modal migrated to shared risk modal
    openCreateNewRiskModal(questionId: string, modalData: any): void {
        const tempRiskId: string = this.addBlankRiskModel(questionId);
        this.modalService.setModalData({ ...modalData, selectedModelId: tempRiskId });
        this.modalService.openModal("riskModal");
    }

    // TODO: Remove V2 when all modals use shared modal
    openCreateNewRiskModalV2(modalData: IRiskModalData): void {
        this.modalService.setModalData(modalData);
        this.modalService.openModal("flagRiskModal");
    }

    // TODO: Deprecate once assessment risk modal migrated to shared risk modal
    openEditRiskModal(riskId: string, modalData?: any): void {
        this.addTempRisk(riskId);
        this.modalService.setModalData({ ...modalData, selectedModelId: riskId });
        this.modalService.openModal("riskModal");
    }

    // TODO: Remove V2 when all modals use shared modal
    openEditRiskModalV2(riskId: string, modalData?: any): void {
        this.addTempRisk(riskId, "id");
        this.modalService.setModalData({ ...modalData, selectedModelId: riskId });
        this.modalService.openModal("flagRiskModal");
    }

    openSingleEditRiskModal(riskId: string, modalData?: any): void {
        this.addTempRisk(riskId);
        this.modalService.setModalData({ ...modalData, selectedModelId: riskId });
        this.modalService.openModal("riskSingleEditModal");
    }

    openDeleteRiskModal(riskId: string, questionId: string): void {
        const modalData: IDeleteConfirmationModalResolve = {
            modalTitle: this.translatePipe.transform("DeleteRisk"),
            confirmationText: this.translatePipe.transform("SureToDeleteThisRisk"),
            cancelButtonText: this.translatePipe.transform("Cancel"),
            submitButtonText: this.translatePipe.transform("Submit"),
            promiseToResolve: () => this.deleteRisk(riskId, questionId),
        };

        this.modalService.setModalData(modalData);
        this.modalService.openModal("deleteConfirmationModal");
    }

    openAcceptRiskModal(riskId: string, riskStateToAccept: number): void {
        const { Addressed, Exception } = RiskStates;
        const riskModel: IRisk | IRiskDetails = getRiskById(riskId)(this.store.getState());

        let modalData: IConfirmationModalResolve;

        if (riskStateToAccept === Addressed) {
            modalData = {
                modalTitle: this.translatePipe.transform("ApproveRemediation"),
                confirmationText: this.translatePipe.transform("SureToAcceptThisRiskAsRiskReduced"),
                cancelButtonText: this.translatePipe.transform("Cancel"),
                submitButtonText: this.translatePipe.transform("Approve"),
                promiseToResolve: () => this.acceptRisk(riskId),
            };
        } else if (riskStateToAccept === Exception) {
            modalData = {
                modalTitle: this.translatePipe.transform("GrantException"),
                confirmationText: this.translatePipe.transform("SureToGrantExceptionAsRiskRetained"),
                cancelButtonText: this.translatePipe.transform("Cancel"),
                submitButtonText: this.translatePipe.transform("Grant"),
                promiseToResolve: () => this.acceptRisk(riskId),
            };
        }

        this.modalService.setModalData(modalData);
        this.modalService.openModal("confirmationModal");
    }

    openRejectRiskModal(riskId: string, riskStateToReject: number): void {
        const { Addressed, Exception } = RiskStates;
        let modalData: IConfirmationModalResolve;

        if (riskStateToReject === Addressed) {
            modalData = {
                modalTitle: this.translatePipe.transform("RejectRisk"),
                confirmationText: this.translatePipe.transform("SureToRejectThisRemediation", {
                    State: this.translatePipe.transform("RecommendationAdded"),
                }),
                cancelButtonText: this.translatePipe.transform("Cancel"),
                submitButtonText: this.translatePipe.transform("Reject"),
                promiseToResolve: () => this.revertRiskToState(riskId, RiskStates.Analysis, RiskStateTransitionTypes.Reject),
            };
        } else if (riskStateToReject === Exception) {
            modalData = {
                modalTitle: this.translatePipe.transform("RejectRisk"),
                confirmationText: this.translatePipe.transform("SureToRejectThisWillChangeRiskState", {
                    State: this.translatePipe.transform("RecommendationAdded"),
                }),
                cancelButtonText: this.translatePipe.transform("Cancel"),
                submitButtonText: this.translatePipe.transform("Reject"),
                promiseToResolve: () => this.revertRiskToState(riskId, RiskStates.Analysis, RiskStateTransitionTypes.Reject),
            };
        }

        this.modalService.setModalData(modalData);
        this.modalService.openModal("confirmationModal");
    }

    reloadRiskModel(riskId: string): ng.IPromise<any> {
        invariant(riskId, "Risk Id must be defined.");
        return this.riskAPI.fetchRiskById(riskId).then((res: any): any => {
            if (res) {
                this.store.dispatch({ type: RiskActions.RISK_RELOADED, riskModelToReload: res });
                return res;
            }
            return res;
        });
    }

    deleteTempRisk(riskId: string): void {
        if (!riskId) throw new Error("Risk Id cannot be undefined");
        this.store.dispatch({ type: RiskActions.DELETE_TEMP_RISK, tempRiskToDelete: riskId });
    }

    selectRisk(riskId: string): void {
        if (!riskId) throw new Error("Risk Id cannot be undefined.");
        this.store.dispatch({ type: RiskActions.SELECT_RISK, riskIdToSelect: riskId });
    }

    unSelectRisk(riskId: string): void {
        this.store.dispatch({ type: RiskActions.UNSELECT_RISK, riskIdToUnSelect: riskId });
    }

    selectAllRisks(): void {
        this.store.dispatch({ type: RiskActions.SELECT_ALL_RISKS });
    }

    unSelectAllRisks(): void {
        this.store.dispatch({ type: RiskActions.UNSELECT_ALL_RISKS });
    }

    saveRiskLevel(risk: IRisk): void {
        this.store.dispatch({ type: RiskActions.RISK_SAVING, riskIdSaving: risk.Id });
        this.riskAPI.saveQuestionRisk(risk).then((res: string): void => {
            if (res) {
                this.updateRisk(risk.Id, { Level: risk.Level });
            }
            this.store.dispatch({ type: RiskActions.RISK_SAVED, riskIdSaved: risk.Id });
        });
    }

    saveRiskTile(risk: IRisk): void {
        this.store.dispatch({ type: RiskActions.RISK_SAVING, riskIdSaving: risk.Id });
        this.riskAPI.saveQuestionRisk(risk).then((res: string): void => {
            if (res) {
                this.updateRisk(risk.Id, {
                    Level: risk.Level,
                    RiskImpactLevel: risk.RiskImpactLevel,
                    RiskProbabilityLevel: risk.RiskProbabilityLevel,
                });
            }
            this.store.dispatch({ type: RiskActions.RISK_SAVED, riskIdSaved: risk.Id });
        });
    }

    saveRiskComment(tempRiskModel: IRisk, stateToProceed: number): any {
        invariant(tempRiskModel, "Risk Model must be defined.");

        const { Analysis, Addressed, Exception, Reduced, Retained } = RiskStates;
        const { Id: riskId } = tempRiskModel;
        const requestBody = {
            Comment: "",
            Id: riskId,
            State: stateToProceed,
        };

        // this is done to preserve the comment data
        // if a risk was changed back to 20 (Analysis/Recommendation Added), we need to re-send what we had for recommendation otherwise the recommendation field will be empty
        if (stateToProceed === Analysis) {
            requestBody.Comment = tempRiskModel.Recommendation;
        } else if (stateToProceed === Addressed) {
            requestBody.Comment = tempRiskModel.Mitigation;
        } else if (stateToProceed === Exception) {
            requestBody.Comment = tempRiskModel.RequestedException;
        }

        return this.riskAPI.updateRisk(requestBody).then((success: boolean): ng.IPromise<any> | boolean => {
            if (success) {
                return this.reloadRiskModel(riskId);
            }
            return success;
        });
    }

    acceptRisk(riskId: string): ng.IPromise<boolean> {
        const riskModel = getRiskById(riskId)(this.store.getState()) as IRisk;

        const requestBody = {
            Comment: "",
            Id: riskId,
            State: null,
        };

        if (riskModel.State === RiskStates.Addressed) {
            requestBody.State = RiskStates.Reduced;
        } else if (riskModel.State === RiskStates.Exception) {
            requestBody.State = RiskStates.Retained;
        }

        return this.riskAPI.updateRisk(requestBody).then((success: boolean): ng.IPromise<any> | boolean => {
            if (success) {
                return this.reloadRiskModel(riskId);
            }
            return success;
        });
    }

    openDeletePropConfirmModal(riskId: string, propToDelete: string): void {
        let modalData: IDeleteConfirmationModalResolve;

        switch (propToDelete) {
            case "Recommendation":
                modalData = {
                    modalTitle: this.translatePipe.transform("ClearRecommendation"),
                    confirmationText: this.translatePipe.transform("ClearingThisWillChangeRiskState", {
                        Attribute: this.translatePipe.transform("Recommendation"),
                        State: this.translatePipe.transform("Identified"),
                    }),
                    cancelButtonText: this.translatePipe.transform("Cancel"),
                    submitButtonText: this.translatePipe.transform("Clear"),
                    promiseToResolve: () => this.revertRiskToState(riskId, RiskStates.Identified),
                };
                break;
            case "Remediation":
                modalData = {
                    modalTitle: this.translatePipe.transform("ClearRemediation"),
                    confirmationText: this.translatePipe.transform("ClearingThisWillChangeRiskState", {
                        Attribute: this.translatePipe.transform("RemediationProposal"),
                        State: this.translatePipe.transform("RecommendationAdded"),
                    }),
                    cancelButtonText: this.translatePipe.transform("Cancel"),
                    submitButtonText: this.translatePipe.transform("Clear"),
                    promiseToResolve: () => this.revertRiskToState(riskId, RiskStates.Analysis),
                };
                break;
            case "RequestedException":
                modalData = {
                    modalTitle: this.translatePipe.transform("ClearException"),
                    confirmationText: this.translatePipe.transform("ClearingThisWillChangeRiskState", {
                        Attribute: this.translatePipe.transform("ExceptionRequest"),
                        State: this.translatePipe.transform("RecommendationAdded"),
                    }),
                    cancelButtonText: this.translatePipe.transform("Cancel"),
                    submitButtonText: this.translatePipe.transform("Clear"),
                    promiseToResolve: () => this.revertRiskToState(riskId, RiskStates.Analysis),
                };
                break;
            default:
                invariant(false, "The property to delete must be defined.");
        }

        this.modalService.setModalData(modalData);
        this.modalService.openModal("deleteConfirmationModal");
    }

    updateModalDataWithExistingRisk(riskId: string, modalData: IRiskModalData): void {
        this.addTempRisk(riskId, "id");
        this.modalService.setModalData({ ...modalData, selectedModelId: riskId });
    }

    updateModalDataWithNewRisk(risk: IRiskDetails, modalData: IRiskModalData): void {
        this.addRisk(risk, "id");
        this.addTempRisk(risk.id, "id");
        this.modalService.setModalData({ ...modalData, selectedModelId: risk.id });
    }

    private revertRiskToState(riskId: string, stateToRevertTo: number, stateTransitionAction?: number): ng.IPromise<boolean> {
        invariant(riskId, "Risk id must be defined.");

        const riskModel: IRisk = getRiskById(riskId)(this.store.getState()) as IRisk;
        invariant(riskModel, "Risk Model must be defined.");

        const { Identified, Analysis } = RiskStates;

        const requestBody = {
            Comment: stateToRevertTo === Analysis ? riskModel.Recommendation : "",
            Id: riskModel.Id,
            State: stateToRevertTo,
            Action: stateTransitionAction,
        };

        return this.riskAPI.updateRisk(requestBody).then((success: boolean): ng.IPromise<any> | boolean => {
            if (success) {
                return this.reloadRiskModel(riskId);
            }
            return success;
        });
    }
}
