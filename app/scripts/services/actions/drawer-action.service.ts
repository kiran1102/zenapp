import {
    DrawerActions,
    getDrawerState,
} from "oneRedux/reducers/drawer.reducer";
import { IStore } from "interfaces/redux.interface";

export class DrawerActionService {

    static $inject: string[] = ["store"];

    constructor(
        private readonly store: IStore,
    ) {}

    public openDrawer(): void {
        this.store.dispatch({ type: DrawerActions.OPEN_DRAWER });
    }

    public closeDrawer(): void {
        this.store.dispatch({ type: DrawerActions.CLOSE_DRAWER });
    }
}
