// 3rd Party
import {
    each,
    findIndex,
} from "lodash";

// Services
import { UserApiService } from "oneServices/api/user-api.service";
import { UserAdapter } from "oneServices/adapters/user-adapter.service";
import { Principal } from "modules/shared/services/helper/principal.service";

// Redux
import {
    getCurrentUser,
    UserActions,
    getAllUsers,
} from "oneRedux/reducers/user.reducer";

// Interfaces
import { IStore } from "interfaces/redux.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";
import { IUserMap } from "interfaces/user-reducer.interface";
import {
    IUser,
    IUserManageContract,
    IUserPaginationParams,
    IUserDetail,
} from "interfaces/user.interface";
import { IPageableOf } from "interfaces/pagination.interface";

export class UserActionService {
    static $inject: string[] = ["store", "UserApi", "UserAdapter", "PRINCIPAL"];

    currentUser: IUser;

    constructor(
        private store: IStore,
        private userApi: UserApiService,
        private userAdapter: UserAdapter,
        private principal: Principal,
    ) {}

    public fetchUsers(): ng.IPromise<IProtocolResponse<IUser[]>> {
        return this.userApi.getUsers()
        .then((response: IProtocolResponse<IUser[]>): IProtocolResponse<IUser[]> => {
                if (response.result) {
                    this.saveUserListToCache(response.data);
                }
                return response;
            },
        );
    }

    public fetchUsersByPage(params: IUserPaginationParams): ng.IPromise<IPageableOf<IUser>> {
        return this.userApi.getUsersByPage(params)
            .then((response: IProtocolResponse<IPageableOf<IUser>>): IPageableOf<IUser> => {
                return response && response.result && response.data ? response.data : null;
            },
        );
    }

    public fetchUsersById(userIds: string[]): ng.IPromise<IUserMap> {
        return this.userApi.getBasicUserInfoList(userIds).then((response: IProtocolResponse<IUserMap>): IUserMap => {
            if (!response.result || !response.data) return null;
            const userMap: IUserMap = response.data;
            const users: IUser[] = [];
            for (const userId in userMap) {
                if (userMap.hasOwnProperty(userId)) {
                    users.push({ Id: userId, ...userMap[userId] });
                }
            }
            this.updateMultipleUsersInList(users, true);
            return userMap;
        });
    }

    public fetchUsersDetailsById(userIds: string[]): Promise<boolean> {
        if (!userIds || !userIds.length) return Promise.resolve(true);
        return this.userApi.getUsersDetails(userIds).toPromise().then((response: IProtocolResponse<IUserDetail[]>): boolean => {
            if (!response.result || !response.data) return null;
            const userMap: IUserDetail[] = response.data;
            const users: IUser[] = [];
            userMap.map((userDetail: IUserDetail) => {
                users.push({
                    Id: userDetail.userId,
                    IsActive: !userDetail.disabled,
                    IsInternal: userDetail.internal,
                    FirstNameLastName: userDetail.fullName,
                    Email: userDetail.email,
                    FullName: userDetail.fullName,
                });
            });
            this.updateMultipleUsersInList(users, true);
            return true;
        });
    }

    public getUser(id: string, details: boolean = false, updateList: boolean = true): ng.IPromise<IProtocolResponse<IUser>> {
        return this.userApi.getUserById(id, details)
            .then((response: IProtocolResponse<IUser>): IProtocolResponse<IUser> => {
                if (response.result && updateList) {
                    this.updateUserInList(
                        id,
                        this.userAdapter.deserializeUser(response.data),
                    );
                }
                return response;
            },
        );
    }

    public fetchCurrentUser(): ng.IPromise<IProtocolResponse<IUser>> {
        return this.userApi.getCurrentUser().then(
            (response: IProtocolResponse<IUser>): IProtocolResponse<IUser> => {
                if (response.result) {
                    const currentUser = this.userAdapter.deserializeUser(response.data);
                    this.setCurrentUser(currentUser);
                    this.principal.setUser(currentUser);
                }
                return response;
            },
        );
    }

    public upsertUser(user: IUserManageContract): ng.IPromise<IProtocolResponse<void>> {
        user = this.userAdapter.serializeUser(user) as IUserManageContract;
        if (user.Id) {
            return this.userApi.updateUser(user);
        } else {
            return this.userApi.createUser(user);
        }
    }

    public saveUserListToCache(list: IUser[]): void {
        this.addUserList(list);
    }

    public addUserList(userList: IUser[]): void {
        const allIds: string[] = userList.map((user: IUser): string => user.Id);
        const byId: IUserMap = userList.reduce((prev, curr) => {
            prev[curr.Id] = curr;
            return prev;
        }, {});
        this.store.dispatch({
            type: UserActions.ADD_USER_LIST,
            allIds,
            byId,
        });
    }

    public getUserList(): IUser[] {
        return getAllUsers(this.store.getState());
    }

    public setCurrentUser(user: IUser): void {
        this.store.dispatch({
            type: UserActions.SET_CURRENT_USER,
            currentUser: user,
        });
    }

    private updateMultipleUsersInList(users: IUser[], merge = false): void {
        let list: IUser[] = getAllUsers(this.store.getState());
        each(users, (user: IUser) => {
           list = this.updateListWithUser(list, user, merge);
        });
        this.saveUserListToCache(list);
    }

    private updateUserInList(id: string, user: IUser): void {
        const list: IUser[] = this.updateListWithUser(getAllUsers(this.store.getState()), user);
        const currentUser = getCurrentUser(this.store.getState());
        if (currentUser && currentUser.Id === id) {
            this.setCurrentUser(user);
        }
        this.saveUserListToCache(list);
    }

    private updateListWithUser(list: IUser[], user: IUser, merge = false): IUser[] {
        const userIndex: number = findIndex(list, { Id: user.Id });
        if (userIndex > -1) {
            list[userIndex] = merge ? { ...list[userIndex], ...user } : user;
        } else {
            list.push(user);
        }
        return list;
    }

}
