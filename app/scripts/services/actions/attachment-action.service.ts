import { startWith } from "rxjs/operators";
// Redux
import observableFromStore from "oneRedux/utils/observable-from-store";
import {
    AttachmentActions,
    getRefId,
} from "oneRedux/reducers/attachment.reducer";
import { Subscription } from "rxjs";

// Services
import { AttachmentService } from "oneServices/attachment.service";
import { ModalService } from "sharedServices/modal.service";

// Interfaces
import { ISamlSettingsResponse } from "interfaces/setting.interface";
import { IAttachmentFileResponse } from "interfaces/attachment-file.interface";
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import { IAttachmentModalResolve } from "interfaces/attachment-modal-resolve.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";
import {
    IStoreState,
    IStore,
} from "interfaces/redux.interface";
import { IDeleteConfirmationModalResolve } from "interfaces/delete-confirmation-modal-resolve.interface";
import { IPageableOf } from "interfaces/pagination.interface";
import { IFetchDetailsParams } from "interfaces/inventory.interface";

// Enums
import { InventoryTableIds } from "enums/inventory.enum";

export class AttachmentActionService {

    static $inject: string[] = [
        "$rootScope",
        "store",
        "AttachmentService",
        "Export",
        "ModalService",
    ];

    private subscription: Subscription;
    private refId: string;

    constructor(
        private readonly $rootScope: IExtendedRootScopeService,
        private readonly store: IStore,
        private readonly attachmentService: AttachmentService,
        private readonly Export: any,
        private readonly modalService: ModalService,
    ) {
        this.subscription = observableFromStore(this.store).pipe(
            startWith(this.store.getState()),
        ).subscribe((state: IStoreState): void => this.componentWillReceiveState(state));
    }

    public fetchAttachmentList(inventoryId: number, page: number, size: number): void {
        const params: IFetchDetailsParams = {
            type: inventoryId === InventoryTableIds.Vendors ? InventoryTableIds.Vendors : null,
            page,
            size,
            sort: "createdDate,desc",
        };
        this.isFetchingAttachments();
        this.attachmentService.getPageableAttachmentList(this.refId, params).then((res: IProtocolResponse<IPageableOf<IAttachmentFileResponse>>): void => {
            if (res.data && res.result) {
                this.setAttachmentList(res.data);
            }
            this.isDoneFetchingAttachments();
        });
    }

    public deleteAttachment(attachmentId: string, inventoryId: number, page: number, size: number): void {
        const resolvePromise: () => ng.IPromise<IProtocolResponse<ISamlSettingsResponse>> = (): ng.IPromise<IProtocolResponse<ISamlSettingsResponse>> => {
            this.isDeletingAttachment();
            return this.attachmentService.deleteAttachment(attachmentId).then((res: IProtocolResponse<ISamlSettingsResponse>): IProtocolResponse<ISamlSettingsResponse> => {
                this.isDoneDeletingAttachment();
                if (this.refId) {
                    this.fetchAttachmentList(inventoryId, page, size);
                }
                return res;
            });
        };
        this.launchDeleteConfirmation(resolvePromise, this.$rootScope.t("DeleteAttachment"), this.$rootScope.t("SureToDeleteThisAttachment"));
    }

    public downloadAttachment(attachment: IAttachmentFileResponse): void {
        this.isDownloadingAttachment();
        this.attachmentService.downloadAttachment(attachment.Id).then((res: IProtocolResponse<any>): void => {
            if (res.result) {
                this.Export.basicFileDownload(res.data, `${attachment.Name}.${attachment.FileType}`);
            }
            this.isDoneDownloadingAttachment();
        });
    }

    public addAttachment(modalData: IAttachmentModalResolve): void {
        this.modalService.setModalData(modalData);
        this.modalService.openModal("attachmentModal");
    }

    public editAttachment(modalData): void {
        this.modalService.setModalData(modalData);
        this.modalService.openModal("editAttachmentModal");
    }

    public isFetchingAttachments(): void {
        this.store.dispatch({ type: AttachmentActions.FETCHING_ATTACHMENT_LIST });
    }

    public setAttachmentList(attachmentListData: IPageableOf<IAttachmentFileResponse>): void {
        this.store.dispatch({ type: AttachmentActions.RECEIVE_ATTACHMENT_LIST, attachmentListData });
    }

    public isDoneFetchingAttachments(): void {
        this.store.dispatch({ type: AttachmentActions.ATTACHMENT_LIST_FETCHED });
    }

    public isDeletingAttachment(): void {
        this.store.dispatch({ type: AttachmentActions.DELETING_ATTACHMENT });
    }

    public isDoneDeletingAttachment(): void {
        this.store.dispatch({ type: AttachmentActions.DONE_DELETING_ATTACHMENT });
    }

    public isDownloadingAttachment(): void {
        this.store.dispatch({ type: AttachmentActions.DOWNLOADING_ATTACHMENT });
    }

    public isDoneDownloadingAttachment(): void {
        this.store.dispatch({ type: AttachmentActions.DONE_DOWNLOADING_ATTACHMENT });
    }

    public setCurrentRefId(currentRefId: string): void {
        this.store.dispatch({ type: AttachmentActions.SET_REF_ID, currentRefId });
    }

    public setDefaultList(): void {
        this.store.dispatch({ type: AttachmentActions.SET_DEFAULT_LIST });
    }

    private componentWillReceiveState(state: IStoreState): void {
        this.refId = getRefId(state);
    }

    private launchDeleteConfirmation(resolvePromise: () => ng.IPromise<any>, title: string, text: string): void {
        const modalData: IDeleteConfirmationModalResolve = {
            modalTitle: title,
            confirmationText: text,
            cancelButtonText: this.$rootScope.t("Cancel"),
            submitButtonText: this.$rootScope.t("Delete"),
            promiseToResolve: resolvePromise,
        };
        this.modalService.setModalData(modalData);
        this.modalService.openModal("deleteConfirmationModal");
    }

}
