import * as _ from "lodash";
import { IUser } from "interfaces/user.interface";

// TODO: DEPRECATE THIS FILE ENTIRELY
export default class UserStore {

    static $inject: string[] = ["UserActionService"];

    constructor(private readonly UserActionService: any) {}

    public getUserList(): IUser[] {
        return this.UserActionService.getUserList();
    }

    public setUserList(list: IUser[]): void {
        this.UserActionService.addUserList(list);
    }

    public updateUser(user: IUser): void {
        const list: IUser[] = this.getUserList();
        const userIndex: number = _.findIndex(list, {Id: user.Id});
        list[userIndex] = user;
        this.setUserList(list);
    }

}
