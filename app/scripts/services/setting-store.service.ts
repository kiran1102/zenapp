import * as _ from "lodash";
import { Settings } from "../Shared/Services/settings.service";
import { SettingActionService } from "settingsModule/services/actions/setting-action.service";

import { IProtocolPacket } from "interfaces/protocol.interface";
import { IProjectSettings } from "interfaces/setting.interface";

interface ITagSetting {
    ProjectTaggingEnabled: boolean;
    QuestionTaggingEnabled: boolean;
    FromCache: boolean;
}

interface IRiskSetting {
    Enabled: boolean;
    GraphSettings: { Axes: [{ Label: string }, { Label: string }] };
    RiskMap: Array<{ RiskLevel: number, Axis1Value: number, Axis2Value: number }>;
}

export default class SettingStore {
    static $inject: string[] = ["Settings", "$q", "SettingAction"];

    private projectSettings: IProjectSettings = {} as IProjectSettings;
    private fetchingData = false;
    private cachedProjectPromise: ng.IPromise<any>;

    constructor(
        private readonly Settings: Settings,
        private readonly $q: ng.IQService,
        private readonly SettingAction: SettingActionService,
    ) {
        this.clearSessionStorage();
        this.handleProjectResponse = this.handleProjectResponse.bind(this);
    }

    public fetchProjectSettings(forceFetch?: boolean): ng.IPromise<any | boolean> {
        if (forceFetch) {
            this.fetchingData = true;
            this.cachedProjectPromise = this.Settings.getProject().then(this.handleProjectResponse);
            return this.cachedProjectPromise;
        }

        this.projectSettings = this.loadProjectSettingsFromSession();

        if (_.isEmpty(this.projectSettings)) {
            if (this.fetchingData) {
                return this.cachedProjectPromise;
            }
            this.fetchingData = true;
            this.cachedProjectPromise = this.Settings.getProject().then(this.handleProjectResponse);
            return this.cachedProjectPromise;
        }
        return this.resolve(this.projectSettings);
    }

    public getProjectSettings(): IProjectSettings {
        return this.projectSettings;
    }

    public setProjectSettings(projectSetting: IProjectSettings): void {
        this.projectSettings = projectSetting;
    }

    public saveSettings(key: string, value: any): ng.IPromise<any | boolean> | undefined {
        switch (key) {
            case "ProjectSettings":
                return this.Settings.setProject(value as IProjectSettings).then((res: IProtocolPacket): IProtocolPacket => {
                    this.projectSettings = value;
                    this.SettingAction.addProjectSettings(value);
                    return res;
                });
            default:
                return;
        }
    }

    private clearSessionStorage(): void {
        sessionStorage.removeItem("OneTrust.ProjectSettings");
        // TODO add more lines here to clear out other settings
    }

    private resolve(val: any): ng.IPromise<any> {
        return this.$q.when(val);
    }

    private loadProjectSettingsFromSession(): IProjectSettings {
        if (_.isEmpty(this.projectSettings)) {
            return JSON.parse(sessionStorage.getItem("OneTrust.ProjectSettings")) || {} as IProjectSettings;
        }
        return this.projectSettings;
    }

    private saveToSession(key: string, value: IProjectSettings | IRiskSetting | ITagSetting): void {
        sessionStorage.setItem(`OneTrust.${key}`, JSON.stringify(value));
    }

    private handleProjectResponse = (res: any): any | boolean => {
        if (res.result) {
            this.setProjectSettings(res.data);
            // this.saveToSession("ProjectSettings", res.data); // TODO uncomment this to commence the session caching
            this.cachedProjectPromise = null;
            return res.data;
        }
        this.fetchingData = false;
        this.cachedProjectPromise = null;
        return false;
    }

}
