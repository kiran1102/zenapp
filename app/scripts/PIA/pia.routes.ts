// UI Router
import { Transition } from "@uirouter/core";

// Library
import TreeMap from "TreeMap";

// Redux
import { getCurrentUser } from "oneRedux/reducers/user.reducer";

// Services
import { OmniService } from "modules/core/services/omni.service";
import { LoadingService } from "modules/core/services/loading.service";
import { AuthEntryService } from "modules/core/services/auth-entry.service";
import { GlobalSidebarService } from "modules/global-sidebar/services/one-global-sidebar.service";
import { AuthHandlerService } from "modules/shared/services/helper/auth-handler.service";
import { Permissions } from "modules/shared/services/helper/permissions.service";
import { TimeStamp } from "oneServices/time-stamp.service";

// Enums / Constants
import { Regex } from "constants/regex.constant";

// Interfaces
import { IPermissions } from "modules/shared/interfaces/permissions.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";
import { IStore } from "interfaces/redux.interface";
import { IUser } from "interfaces/user.interface";
import { IStringMap } from "interfaces/generic.interface";

export interface IGeneralData {
    OnePermissions: IPermissions;
    OneContent: IStringMap<any>;
    OneTranslations: IStringMap<string>;
    OneBranding: any;
    Organizations: any;
    OneUsers: any;
    OneTimeStamp: TimeStamp;
}

export function routes($stateProvider: ng.ui.IStateProvider, $urlRouterProvider: ng.ui.IUrlRouterProvider): void {

    // Default PIA redirects/corrections.

    $urlRouterProvider.when("/pia", "/landing");
    $urlRouterProvider.when("/pia/landing", "/landing");
    $urlRouterProvider.when("/pia/welcome", "/module/welcome");

    $urlRouterProvider.when("/pia/projects", "/pia/projects/");

    $urlRouterProvider.when("/pia/datamap/assessments/:Id", "/pia/datamap/assessments/:Id/");
    $urlRouterProvider.when("/pia/datamap/assessment/:Id", "/pia/datamap/assessment/:Id/");

    $urlRouterProvider.when("/pia/settings", "/pia/settings/");
    $urlRouterProvider.when("/pia/settings/", "/pia/settings/project");

    $urlRouterProvider.when("/pia/thanks", "/pia/thanks/");

    $urlRouterProvider.when("/pia/admin", "/pia/admin/");
    $urlRouterProvider.when("/pia/admin/", "/pia/admin/users");

    $stateProvider
        ///////////////
        //  Modules  //
        ///////////////
        .state("zen.app.pia", {
            url: "/pia",
            abstract: true,
            resolve: {
                isAuthenticated: [
                    "AuthHandlerService",
                    (
                        authHandlerService: AuthHandlerService,
                    ): Promise<boolean> => {
                        return authHandlerService.handleAuthGuard() ? Promise.resolve(true) : Promise.reject(false);
                    },
                ],
                omniData: ["isAuthenticated", "OmniService", (isAuthenticated: boolean, omni: OmniService) => {
                    if (isAuthenticated) {
                        return omni.fetch().toPromise();
                    }
                }],
            },
        })
        .state("zen.app.pia.module", {
            url: "/",
            abstract: true,
            resolve: {
                currentUser: ["omniData", "store", (omniData: IGeneralData, store: IStore): IUser => {
                    return getCurrentUser(store.getState());
                }],
                GeneralData: [
                    "omniData", (omniData: IGeneralData) => {
                        return omniData;
                    },
                ],
            },
            template: require("views/PIA/pia.jade"),
            onEnter: ["LoadingService", "AuthEntryService", (loadingService: LoadingService, authEntry: AuthEntryService): void => {
                authEntry.init();
                loadingService.set(false);
            }],
        })

        .state("zen.app.pia.module.readiness", {
            abstract: true,
            url: "readiness",
            resolve: {
                ReadinessPermission: ["GeneralData", "Permissions",
                    (GeneralData: IGeneralData, permissions: Permissions): boolean => {
                        if (permissions.canShow("ReadinessAssessmentV2")) {
                            return true;
                        }
                        permissions.goToFallback();
                        return false;
                    },
                ],
            },
            params: { time: null },
            views: {
                module: {
                    template: "<ui-view></ui-view>",
                    controller: [
                        "GlobalSidebarService",
                        (globalSidebarService: GlobalSidebarService) => {
                            globalSidebarService.set();
                        },
                    ],
                },
            },
        })
        .state("zen.app.pia.module.readiness.dashboard", {
            url: "/dashboard",
            template: require("piaviews/Readiness/readiness_dashboard.jade"),
            controller: "ReadinessDashboardCtrl",
        })
        .state("zen.app.pia.module.readiness.assessment", {
            url: "/assessment/:Id",
            template: require("piaviews/Readiness/readiness_assessment.jade"),
            controller: "ReadinessAssessmentCtrl",
        })
        .state("zen.app.pia.module.readiness.report", {
            url: "/report/:Id",
            template: require("piaviews/Readiness/readiness_report.jade"),
            controller: "ReadinessReportCtrl",
        })
        .state("zen.app.pia.module.projects", {
            abstract: true,
            url: "projects",
            views: {
                module: {
                    template: require("piaviews/Projects/pia_projects-wrapper.jade"),
                    controller: [
                        "GlobalSidebarService",
                        (globalSidebarService: GlobalSidebarService) => {
                            globalSidebarService.setAAMenu();
                        },
                    ],
                },
            },
            resolve: {
                ProjectsPermission: [
                    "Permissions",
                    "$transition$",
                    "$stateParams",
                    (permissions: Permissions, transition: Transition, $stateParams: ng.ui.IStateParamsService): boolean => {
                        if (permissions.canShow("Projects")) {
                            return true;
                        } else if (permissions.canShow("Assessments")) {
                            const url = transition.router.locationService.url();
                            const urlParts = url.split("/");
                            if (urlParts.length >= 3 && ((urlParts[1] === "pia" && urlParts[2] === "projects" && urlParts[3].match(Regex.GUID) && parseInt(urlParts[4], 10))  ||
                                (urlParts[1] === "invite" && urlParts[2] === "pia" && urlParts[3] === "projects" && urlParts[5].match(Regex.GUID) && parseInt(urlParts[6], 10)))) {
                                if (permissions.canShow("Assessments")) {
                                    permissions.goToFallback("zen.app.pia.module.assessment.detail", { assessmentId: $stateParams.Id });
                                    return false;
                                }
                            }
                        }
                        permissions.goToFallback();
                        return false;
                    },
                ],
            },
        })
        .state("zen.app.pia.module.projects.list", {
            url: "/",
            params: { time: null },
            template: require("piaviews/Projects/pia_projects.jade"),
            controller: "ProjectsCtrl",
        })
        .state("zen.app.pia.module.selfservice", {
            url: "selfservice/",
            abstract: true,
            views: {
                module: {
                    template: "<ui-view></ui-view>",
                    controller: [
                        "GlobalSidebarService",
                        (globalSidebarService: GlobalSidebarService) => {
                            globalSidebarService.set();
                        },
                    ],
                },
            },
            resolve: {
                SelfServicePermission: ["GeneralData", "Permissions", (GeneralData: IGeneralData, permissions: Permissions): IGeneralData => {
                    if (permissions.canShow("SelfServicePortal")) {
                        permissions.goToFallback("zen.app.pia.module.ssp.list", {}, { reload: true });
                    }
                    return GeneralData;
                }],
            },
        })
        .state("zen.app.pia.module.projects.risk_summary", {
            url: "/risksummary/:ProjectId/:Version",
            template: "<risk-summary></risk-summary>",
        })
        .state("zen.app.pia.module.selfservice.list", {
            url: "list",
            template: require("piaviews/SelfService/portal.jade"),
            controller: "SelfServicePortalCtrl",
        })
        .state("zen.app.pia.module.projects.wizard", {
            url: "wizard/",
            abstract: true,
            template: require("piaviews/Projects/ProjectWizard/pia_projects_wizard.jade"),
        })
        .state("zen.app.pia.module.projects.wizard.project_type", {
            url: "select",
            template: require("piaviews/Projects/ProjectWizard/project_type.jade"),
            controller: "ProjectsWizardSelectCtrl",
        })
        .state("zen.app.pia.module.projects.wizard.project_info", {
            url: "info/:mode/:id/:version",
            template: require("piaviews/Projects/ProjectWizard/project_info.jade"),
            controller: "ProjectsWizardFormCtrl",
            resolve: {
                ProjectsPermission: ["GeneralData", (GeneralData: IGeneralData): IGeneralData => {
                    return GeneralData;
                }],
            },
        })
        .state("zen.app.pia.module.projects.wizard.project_assignment", {
            url: "info/:mode/:id/:version/assignment",
            template: require("piaviews/Projects/ProjectWizard/project_assignment.jade"),
            controller: "ProjectsWizardFormCtrl",
            resolve: {
                ProjectsPermission: ["GeneralData", (GeneralData: IGeneralData): IGeneralData => {
                    return GeneralData;
                }],
            },
        })
        .state("zen.app.pia.module.projects.assessment", {
            url: "/",
            abstract: true,
            template: require("piaviews/Projects/Assessment/assessment_wrapper.jade"),
        })
        .state("zen.app.pia.module.projects.assessment.module", {
            url: "",
            abstract: true,
            controller: "AssessmentCtrl",
            template: require("piaviews/Projects/Assessment/assessment_list.jade"),
        })
        .state("zen.app.pia.module.projects.duplicate", {
            params: { id: null, version: null, project: null },
            url: "/duplicate/:id/:version",
            template: "<create-project-form></create-project-form>",
        })
        .state("zen.app.pia.module.projects.assessment.module.questions", {
            url: ":Id/:Version",
            views: {
                assessment_questions: {
                    template: require("piaviews/Projects/Assessment/assessment_questions.jade"),
                },
            },
        })
        .state("zen.app.pia.module.documents", {
            abstract: true,
            url: "documents",
            resolve: {
                DocumentsPermission: ["GeneralData", "Permissions",
                    (GeneralData: IGeneralData, permissions: Permissions): boolean => {
                        if (permissions.canShow("Documents")) {
                            return true;
                        }
                        permissions.goToFallback();
                        return false;
                    },
                ],
            },
            views: {
                module: {
                    template: require("piaviews/Documents/documents.jade"),
                    controller: [
                        "GlobalSidebarService",
                        (globalSidebarService: GlobalSidebarService) => {
                            globalSidebarService.set();
                        },
                    ],
                },
            },
        })
        .state("zen.app.pia.module.documents.repository", {
            url: "",
            abstract: true,
            resolve: {
                DocumentsDirectory: ["Documents", "DocumentsPermission",
                    (Documents: any, DocumentsPermission: boolean): ng.IPromise<any> => {
                        return Documents.tree().then((response: IProtocolResponse<any>): any => {
                            const Directory: any = new TreeMap("Id", "ParentDirectoryId", "Children", response.data[0]);
                            return Directory;
                        });
                    },
                ],
            },
            views: {
                menu: {
                    template: require("piaviews/Documents/repository/menu.jade"),
                    controller: "DocumentsMenuCtrl",
                    controllerAs: "vm",
                },
                main: {
                    template: require("piaviews/Documents/repository/main.jade"),
                    controller: "DocumentsMainCtrl",
                    controllerAs: "vm",
                },
            },
        })
        .state("zen.app.pia.module.documents.repository.main", {
            url: "/:path",
            params: { path: "" },
            views: {
                body: {
                    template: require("piaviews/Documents/repository/body.jade"),
                },
                details: {
                    template: require("piaviews/Documents/repository/details.jade"),
                },
            },
        })
        .state("zen.app.pia.module.dashboard", {
            abstract: true,
            url: "dashboard",
            resolve: {
                DashboardPermission: ["GeneralData", "Permissions",
                    (GeneralData: IGeneralData, permissions: Permissions): boolean => {
                        if (permissions.canShow("Dashboard")) {
                            return true;
                        }
                        permissions.goToFallback();
                        return false;
                    },
                ],
            },
            views: {
                module: {
                    template: "<ui-view></ui-view>",
                    controller: "DashboardCtrl",
                },
            },
        })
        .state("zen.app.pia.module.dashboard.list", {
            url: "",
            params: { time: null },
            template: require("piaviews/Dashboard/pia_dashboard.jade"),
        })
        .state("zen.app.pia.module.datamap", {
            url: "datamap",
            resolve: {
                DataMappingPermission: ["GeneralData", "Permissions",
                    (GeneralData: IGeneralData, permissions: Permissions): boolean => {
                        if (permissions.canShow("DataMapping") || permissions.canShow("DataMappingNew")) {
                            return true;
                        }
                        permissions.goToFallback();
                        return false;
                    },
                ],
            },
            abstract: true,
            views: {
                module: {
                    template: require("piaviews/DataMapping/data_mapping_wrapper.jade"),
                    controller: "DataMappingCtrl",
                },
            },
        })
        .state("zen.app.pia.module.datamap.views", {
            url: "/",
            params: { time: null },
            abstract: true,
            views: {
                // data_mapping_menu: {
                //     template: require("piaviews/DataMapping/data_mapping_menu.jade"),
                // },
                data_mapping_body: {
                    template: "<ui-view></ui-view>",
                },
            },
            resolve: {
                DataMappingViewsPermission: ["DataMappingPermission",
                    (DataMappingPermission: boolean): boolean => {
                        return DataMappingPermission;
                    },
                ],
            },
        })
        .state("zen.app.pia.module.datamap.views.asset", {
            url: "asset",
            params: { id: 1, time: null },
            template: "<dm-data-maps></dm-data-maps>",
            resolve: {
                DataMappingDataMaps: ["DataMappingViewsPermission", "Permissions",
                    (DataMappingViewsPermission: boolean, permissions: Permissions): boolean => {
                        if (permissions.canShow("DataMappingDataMaps") && permissions.canShow("DataMappingDataMapsAssetsByLocation")) {
                            return true;
                        }
                        permissions.goToFallback();
                        return false;
                    },
                ],
            },
        })
        .state("zen.app.pia.module.datamap.views.crossborder", {
            url: "crossborder",
            params: { id: 2, time: null },
            template: "<dm-data-maps></dm-data-maps>",
            resolve: {
                DataMappingDataMaps: ["DataMappingViewsPermission", "Permissions",
                    (DataMappingViewsPermission: boolean, permissions: Permissions): boolean => {
                        if (permissions.canShow("DataMappingDataMaps") && permissions.canShow("DataMappingCrossborder")) {
                            return true;
                        }
                        permissions.goToFallback();
                        return false;
                    },
                ],
            },
        })
        .state("zen.app.pia.module.datamap.views.tables", {
            url: "tables/:tableId?page&size&filter&search",
            params: { time: null },
            resolve: {
                DataMappingInventoryPermission: ["DataMappingViewsPermission", "Permissions",
                    (DataMappingViewsPermission: boolean, permissions: Permissions): boolean => {
                        if (permissions.canShow("DataMappingInventories")) {
                            return true;
                        } else if (permissions.canShow("DataMappingInventory")) {
                            permissions.goToFallback("zen.app.pia.module.datamap.views.inventory.list");
                        } else {
                            permissions.goToFallback();
                        }
                        return false;
                    },
                ],
            },
            template: require("piaviews/DataMapping/Tables/data_table.jade"),
            controllerAs: "vm",
            controller: "DataTableCtrl",
        })
        .state("zen.app.pia.module.datamap.views.questionnaire-list", {
            url: "questionnaires",
            params: { time: null },
            template: require("piaviews/Templates/pia_templates.jade"),
            controller: "TemplatesCtrl",
            resolve: {
                DataMappingQuestionnairePermission: ["DataMappingViewsPermission",
                    (DataMappingViewsPermission: boolean): boolean => {
                        return DataMappingViewsPermission;
                    },
                ],
            },
        })
        .state("zen.app.pia.module.datamap.views.questionnaire-archive", {
            url: "archive",
            template: require("piaviews/Templates/pia_templates_archive.jade"),
            controller: "TemplatesArchiveCtrl",
            resolve: {
                DataMappingQuestionnairePermission: ["DataMappingViewsPermission",
                    (DataMappingViewsPermission: boolean): boolean => {
                        return DataMappingViewsPermission;
                    },
                ],
            },
        })
        .state("zen.app.pia.module.datamap.views.questionnaire-gallery", {
            url: "gallery",
            template: require("piaviews/Templates/pia_template_studio.jade"),
            controller: "TemplatesStudioGalleryCtrl",
            resolve: {
                DataMappingQuestionnairePermission: ["DataMappingViewsPermission",
                    (DataMappingViewsPermission: boolean): boolean => {
                        return DataMappingViewsPermission;
                    },
                ],
            },
        })
        .state("zen.app.pia.module.datamap.views.questionnaire", {
            url: "questionnaire/:templateId/:version",
            template: require("piaviews/Templates/pia_template_manage.jade"),
            controller: "TemplateStudioManageCtrl",
            params: { time: null, version: null },
            resolve: {
                DataMappingQuestionnairePermission: [
                    "DataMappingViewsPermission",
                    "$stateParams",
                    "Permissions",
                    (
                        DataMappingViewsPermission: boolean,
                        $stateParams: ng.ui.IStateParamsService,
                        permissions: Permissions,
                    ): boolean => {
                        if (!$stateParams.version) {
                            $stateParams.version = 1;
                        }
                        if (permissions.canShow("DataMappingQuestionnaire")) {
                            return true;
                        } else {
                            permissions.goToFallback();
                        }
                        return false;
                    },
                ],
            },
        })
        .state("zen.app.pia.module.datamap.views.template-redirect", {
            url: "template-redirect",
            template: "<loading show-text='false'></loading>",
            params: { templateId: null, templateType: null },
            resolve: {
                DataMappingQuestionnairePermission: [
                    "DataMappingViewsPermission",
                    "$state",
                    "$stateParams",
                    "Permissions",
                    "Templates",
                    "DMTemplate",
                    "ENUMS",
                    (
                        DataMappingViewsPermission: boolean,
                        $state: ng.ui.IStateService,
                        $stateParams: ng.ui.IStateParamsService,
                        permissions: Permissions,
                        Templates: any,
                        DMTemplate: any,
                        ENUMS: any,
                    ): boolean => {
                        if (permissions.canShow("DataMappingQuestionnaire")) {
                            Templates.list(ENUMS.TemplateTypes.Datamapping).then((response: IProtocolResponse<any[]>): void => {
                                if ($state.includes("zen.app.pia.module.datamap.views.template-redirect")) {
                                    if (response.result && response.data.length) {
                                        const dmTemplate = response.data[0].Published || response.data[0].Draft;
                                        permissions.goToFallback("zen.app.pia.module.datamap.views.questionnaire", {
                                            templateId: dmTemplate.Id,
                                            version: dmTemplate.Version,
                                            time: (new Date()).getTime(),
                                        });
                                    } else {
                                        permissions.goToFallback("zen.app.pia.module.datamap.views.assessment.list");
                                    }
                                }
                            });
                        } else if (permissions.canShow("DataMappingTemplate")) {
                            if ($stateParams.templateId) {
                                permissions.goToFallback("zen.app.pia.module.datamap.views.template", {
                                    templateId: $stateParams.templateId,
                                    templateType: $stateParams.templateType,
                                });
                            } else {
                                DMTemplate.readOrgLatest($stateParams.templateType).then((res: IProtocolResponse<any>): void => {
                                    if ($state.includes("zen.app.pia.module.datamap.views.template-redirect")) {
                                        if (!res.result) {
                                            permissions.goToFallback("zen.app.pia.module.datamap.views.assessment.list");
                                        } else {
                                            permissions.goToFallback("zen.app.pia.module.datamap.views.template", {
                                                templateId: res.data.id,
                                                templateType: $stateParams.templateType,
                                                templateData: res.data,
                                            });
                                        }
                                    }
                                });
                            }
                        }
                        return true;
                    },
                ],
            },
        })
        .state("zen.app.pia.module.datamap.views.template", {
            url: "template/:templateId/:version?templateType",
            params: { time: null, templateData: null, version: null },
            template: "<dm-template></dm-template>",
            resolve: {
                DataMappingQuestionnairePermission: [
                    "DataMappingViewsPermission",
                    "$stateParams",
                    "Permissions",
                    "ENUMS",
                    (
                        DataMappingViewsPermission: boolean,
                        $stateParams: ng.ui.IStateParamsService,
                        permissions: Permissions,
                        ENUMS: any,
                    ): boolean => {
                        const canViewTemplate: boolean = $stateParams.templateType === ENUMS.InventoryTableIds.Processes
                            ? permissions.canShow("DataMappingProcessingTemplate")
                            : permissions.canShow("DataMappingAssetTemplate");
                        if (canViewTemplate) {
                            return true;
                        }
                        permissions.goToFallback();
                        return false;
                    },
                ],
            },
        })
        .state("zen.app.pia.module.datamap.views.assessment", {
            abstract: true,
            url: "assessment",
            template: "<ui-view></ui-view>",
            resolve: {
                DataMappingAssessmentPermission: ["DataMappingViewsPermission", (DataMappingViewsPermission: boolean): boolean => {
                    return DataMappingViewsPermission;
                }],
            },
        })
        .state("zen.app.pia.module.datamap.views.assessmentV2", {
            url: "assessment-list",
            template: "<downgrade-aa-list></downgrade-aa-list>",
        })
        .state("zen.app.pia.module.datamap.views.assessment-old", {
            abstract: true,
            url: "assessments",
            template: require("piaviews/Projects/pia_projects-wrapper.jade"),
            resolve: {
                DataMappingAssessmentPermission: [
                    "DataMappingViewsPermission",
                    "Permissions",
                    "$stateParams", (DataMappingViewsPermission: boolean, permissions: Permissions, $stateParams: ng.ui.IStateParamsService): boolean => {
                        if (permissions.canShow("Assessments")) {
                            permissions.goToFallback("zen.app.pia.module.assessment.detail", { assessmentId: $stateParams.Id });
                            return false;
                        }

                        return DataMappingViewsPermission;
                    }],
            },
        })
        .state("zen.app.pia.module.datamap.views.assessment.list", {
            url: "s?page&size&sort&filter",
            params: {
                time: null,
                filter: { dynamic: true },
                page: { dynamic: true },
                size: { dynamic: true },
                sort: { dynamic: true },
            },
            template: "<dm-assessment-list></dm-assessment-list>",
            resolve: {
                AssessmentsListPermission: ["DataMappingAssessmentPermission", "Permissions",
                    (DataMappingAssessmentPermission: boolean, permissions: Permissions): boolean => {
                        if (permissions.canShow("DataMappingAssessmentsList")) {
                            return true;
                        } else if (permissions.canShow("DataMappingAssessments")) {
                            permissions.goToFallback("zen.app.pia.module.datamap.views.assessment-old.list");
                        } else {
                            permissions.goToFallback();
                        }
                        return false;
                    },
                ],
            },
        })
        .state("zen.app.pia.module.datamap.views.assessment-old.list", {
            url: "",
            params: { time: null },
            template: require("piaviews/Projects/pia_projects.jade"),
            controller: "ProjectsCtrl",
            resolve: {
                OldAssessmentsListPermission: ["DataMappingAssessmentPermission", "Permissions",
                    (DataMappingAssessmentPermission: boolean, permissions: Permissions): boolean => {
                        if (permissions.canShow("DataMappingAssessments")) {
                            return true;
                        } else if (permissions.canShow("DMAssessmentV2")) {
                            permissions.goToFallback("zen.app.pia.module.assessment.list");
                        } else if (permissions.canShow("DataMappingAssessmentsList")) {
                            permissions.goToFallback("zen.app.pia.module.datamap.views.assessment.list");
                        } else {
                            permissions.goToFallback();
                        }
                        return false;
                    },
                ],
            },
        })
        .state("zen.app.pia.module.datamap.views.assessment.single", {
            url: "/:Id/:Version",
            params: { time: null, Version: null },
            templateProvider: [
                "Permissions",
                (permissions: Permissions) => {
                    if (permissions.canShow("DMAssessmentV2")) return;
                    return require("views/DataMapping/Assessment/assessment_wrapper.html");
                },
            ],
            resolve: {
                AssessmentSinglePermission: ["DataMappingAssessmentPermission", "Permissions", "$stateParams",
                    (DataMappingAssessmentPermission: boolean, permissions: Permissions, $stateParams: ng.ui.IStateParamsService): boolean => {
                        if (permissions.canShow("DMAssessmentV2")) {
                            permissions.goToFallback("zen.app.pia.module.assessment.detail", { assessmentId: $stateParams.Id });
                        } else if (permissions.canShow("DataMappingAssessmentView")) {
                            return true;
                        } else if (permissions.canShow("DataMappingAssessmentsSingle")) {
                            permissions.goToFallback("zen.app.pia.module.datamap.views.assessment-old.single.module.questions");
                        } else {
                            permissions.goToFallback();
                        }
                        return false;
                    },
                ],
            },
        })
        .state("zen.app.pia.module.datamap.views.assessment-old.single", {
            url: "",
            params: { time: null },
            abstract: true,
            template: require("piaviews/Projects/Assessment/assessment_wrapper.jade"),
            resolve: {
                DataMappingAssessmentSinglePermission: ["DataMappingAssessmentPermission", (DataMappingAssessmentPermission: boolean): boolean => {
                    return DataMappingAssessmentPermission;
                }],
            },
        })
        .state("zen.app.pia.module.datamap.views.assessment-old.single.module", {
            url: "",
            abstract: true,
            controller: "AssessmentCtrl",
            template: require("piaviews/Projects/Assessment/assessment_list.jade"),
        })
        .state("zen.app.pia.module.datamap.views.assessment-old.single.module.questions", {
            url: "/:Id/:Version",
            params: { Version: null },
            views: {
                assessment_questions: {
                    template: require("piaviews/Projects/Assessment/assessment_questions.jade"),
                },
            },
            resolve: {
                OldAssessmentSinglePermission: ["DataMappingAssessmentSinglePermission", "Permissions",
                    (DataMappingAssessmentSinglePermission: boolean, permissions: Permissions): boolean => {
                        if (permissions.canShow("DataMappingAssessmentsSingle")) {
                            return true;
                        } else if (permissions.canShow("DataMappingAssessmentView")) {
                            permissions.goToFallback("zen.app.pia.module.datamap.views.assessment.single");
                        } else {
                            permissions.goToFallback();
                        }
                        return false;
                    },
                ],
            },
        })
        .state("zen.app.pia.module.datamap.views.welcome", {
            url: "welcome?tab",
            params: { time: null },
            template: "<dm-welcome></dm-welcome>",
            resolve: {
                DataMappingWelcomePermission: ["DataMappingViewsPermission", "Permissions",
                    (DataMappingViewsPermission: boolean, permissions: Permissions): boolean => {
                        if (permissions.canShow("DataMappingWelcomeHomePage")) {
                            return true;
                        }
                        permissions.goToFallback();
                        return false;
                    },
                ],
            },
        })
        .state("zen.app.pia.module.datamap.views.dashboard", {
            url: "dashboard",
            params: { time: null },
            template: "<dm-dashboard></dm-dashboard>",
            resolve: {
                DataMappingPermission: ["DataMappingViewsPermission", "Permissions",
                    (DataMappingViewsPermission: boolean, permissions: Permissions): boolean => {
                        if (permissions.canShow("DataMappingAssessmentsDashboard")) {
                            return true;
                        }
                        permissions.goToFallback();
                        return false;
                    },
                ],
            },
        })
        .state("zen.app.pia.module.risk", {
            abstract: true,
            url: "risk",
            resolve: {
                RiskPermission: ["GeneralData", "Permissions",
                    (GeneralData: IGeneralData, permissions: Permissions): boolean => {
                        if (permissions.canShow("Risks")) {
                            return true;
                        }
                        permissions.goToFallback();
                        return false;
                    },
                ],
            },
            views: {
                module: {
                    template: "<ui-view></ui-view>",
                    controller: "RiskCtrl",
                },
            },
        })
        .state("zen.app.pia.module.risk.list", {
            url: "?page&status",
            params: { time: null },
            template: require("piaviews/Risk/pia_risk.jade"),
        })
        .state("zen.app.pia.module.dsar", {
            abstract: true,
            url: "dsar",
            resolve: {
                DSARPermission: ["GeneralData", "Permissions",
                    (GeneralData: IGeneralData, permissions: Permissions): boolean => {
                        if (permissions.canShow("DSARView")) {
                            return true;
                        }
                        permissions.goToFallback();
                        return false;
                    },
                ],
            },
            views: {
                module: {
                    template: "<dsar-wrapper></dsar-wrapper>",
                },
            },
        })
        .state("zen.app.pia.module.cookiecompliance", {
            abstract: true,
            url: "cookiecompliance",
            resolve: {
                CookiesPermission: ["GeneralData", "Permissions",
                    (GeneralData: IGeneralData, permissions: Permissions): boolean => {
                        if (permissions.canShow("Cookie")) {
                            return true;
                        }
                        permissions.goToFallback();
                        return false;
                    },
                ],
            },
            views: {
                module: {
                    template: require("views/Cookies/cookiecompliance_wrapper.jade"),
                    controller: "CookieComplianceCtrl",
                },
            },
        })

        .state("zen.app.pia.module.thanks", {
            url: "thanks/:projectId/:templateType/:projectVersion",
            params: {
                options: null,
                saveAndExit: null,
            },
            views: {
                module: {
                    template: require("piaviews/Thanks/pia_thanks.jade"),
                    controller: "ThanksCtrl",
                },
            },
            resolve: {
                ThanksPermission: ["GeneralData", (GeneralData: IGeneralData): IGeneralData => {
                    return GeneralData;
                }],
            },
        })
        .state("zen.app.pia.module.intg", {
            abstract: true,
            url: "integrations",
            resolve: {
                ISPermission: ["GeneralData", "Permissions",
                    (GeneralData: IGeneralData, permissions: Permissions): boolean => {
                        if (permissions.canShow("IntegrationPageView")) {
                            return true;
                        }
                        permissions.goToFallback();
                        return false;
                    },
                ],
            },
            views: {
                module: {
                    template: "<intg-wrapper></intg-wrapper>",
                },
            },
        })

        ////////////////////
        //   New Reports  //
        ////////////////////
        .state("zen.app.pia.module.reports-new", {
            abstract: true,
            url: "reporting",
            resolve: {
                ReportsPermission: ["GeneralData", "Permissions",
                    (GeneralData: IGeneralData, permissions: Permissions): boolean => {
                        if (permissions.canShow("ReportsNew")) {
                            return true;
                        }
                        permissions.goToFallback();
                        return false;
                    },
                ],
            },
            views: {
                module: {
                    template: "<reports-wrapper class='full-size flex-full-height'></reports-wrapper>",
                },
            },
        })

        .state("zen.app.pia.module.preview", {
            abstract: true,
            url: "consent/",
            views: {
                module: {
                    template: "<div class='launch'><ui-view></ui-view></div>",
                    controller: [
                        "GlobalSidebarService",
                        (globalSidebarService: GlobalSidebarService) => {
                            globalSidebarService.set();
                        },
                    ],
                },
            },
            onEnter: ["LoadingService", (loadingService: LoadingService): void => {
                loadingService.set(false);
            }],
        })
        .state("zen.app.pia.module.download", {
            url: "download/:documentId",
            params: { time: null },
            views: {
                module: {
                    template: "<download-landing class='full-size flex-full-height'></download-landing>",
                },
            },
        })
        ;
}

routes.$inject = ["$stateProvider", "$urlRouterProvider"];
