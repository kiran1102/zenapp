
"use strict";

export default function run($rootScope, editableOptions) {

    //Set global PIA variables
    editableOptions.theme = "bs3";

}

run.$inject = ["$rootScope", "editableOptions"];
