import * as angular from "angular";

import { routes } from "./template.routes";

import TemplatesCtrl from "./Controllers/Templates/TemplatesController";
import AddSectionCtrl from "./Controllers/Templates/AddSectionController";
import ConditionalController from "./Controllers/Templates/ConditionalController";
import CopyTemplateCtrl from "./Controllers/Templates/CopyTemplateController";
import CreateTemplateCtrl from "./Controllers/Templates/CreateTemplateController";
import EditSectionCtrl from "./Controllers/Templates/EditSectionController";
import EditTemplatesCtrl from "./Controllers/Templates/EditTemplateController";
import SaveQuestionCtrl from "./Controllers/Templates/SaveQuestionController";
import SaveStatementCtrl from "./Controllers/Templates/SaveStatementController";
import SelectIconCtrl from "./Controllers/Templates/SelectIconController";
import TemplatesArchiveCtrl from "./Controllers/Templates/TemplatesArchiveController";
import TemplatesStudioGalleryCtrl from "./Controllers/Templates/TemplateStudioGalleryController";
import TemplateStudioManageCtrl from "./Controllers/Templates/TemplateStudioManageController";
import WelcomeTextCtrl from "./Controllers/Templates/welcome-text.controller";

import TemplateService from "oneServices/templates.service.ts";
import ConditionGroups from "../Questionnaire/Services/ConditionGroupsService";
import Conditions from "../Questionnaire/Services/ConditionsService";
import Icons from "../Questionnaire/Services/IconsService";
import Questions from "oneServices/questions.service";
import Sections from "oneServices/section.service";

import Question from "../Shared/Directives/questionDirective";

angular.module("zen.template", [
	// Angular modules
	"ui.bootstrap",
	"ui.router",
	"ui.tree",
	"ngMaterial",
	"ngSanitize",
	"ngStorage",

	// Custom modules
	"zen.identity",

	// 3rd Party Modules
	"luegg.directives", // scroll-glue
	"dndLists", // Drag and Drop List'
	"xeditable", // Xeditable Goodness
	"ngTagsInput", // TagsInput req. for tagging
	"minicolors", // Color Picker
	"angularFileUpload", // Angular File Upload
	"angularMoment", // Angular Moment
])
	.config(routes)

	.directive("question", Question)

	.controller("TemplatesCtrl", TemplatesCtrl)
	.controller("AddSectionCtrl", AddSectionCtrl)
	.controller("ConditionalController", ConditionalController)
	.controller("CopyTemplateCtrl", CopyTemplateCtrl)
	.controller("CreateTemplateCtrl", CreateTemplateCtrl)
	.controller("EditSectionCtrl", EditSectionCtrl)
	.controller("EditTemplatesCtrl", EditTemplatesCtrl)
	.controller("SaveQuestionCtrl", SaveQuestionCtrl)
	.controller("SaveStatementCtrl", SaveStatementCtrl)
	.controller("SelectIconCtrl", SelectIconCtrl)
	.controller("TemplatesArchiveCtrl", TemplatesArchiveCtrl)
	.controller("TemplatesStudioGalleryCtrl", TemplatesStudioGalleryCtrl)
	.controller("TemplateStudioManageCtrl", TemplateStudioManageCtrl)
	.controller("WelcomeTextCtrl", WelcomeTextCtrl)

	.service("Templates", TemplateService)
	.factory("ConditionGroups", ConditionGroups)
	.factory("Conditions", Conditions)
	.factory("Icons", Icons)
	.service("Questions", Questions)
	.service("Sections", Sections)
	;
