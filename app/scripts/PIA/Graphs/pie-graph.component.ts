declare var d3: any;
import * as _ from "lodash";
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import { IPieGraphData } from "interfaces/pie-graph-data.interface";
import { AssessmentStates, AssessmentStateKeys } from "constants/assessment.constants";

class PieGraph implements ng.IComponentController {
    static $inject: string[] = ["$rootScope", "$q", "$filter"];

    private graphData: any;
    private projectSettings: any;
    private pieGraphData: IPieGraphData[];
    private originalPieGraphData: IPieGraphData[];
    private translations: any;
    private customTerms: any;
    private allCountsEmpty: boolean;

    constructor(
        private readonly $rootScope: IExtendedRootScopeService,
        private readonly $q: ng.IQService,
        private readonly $filter: ng.IFilterService,
    ) {}

    public $onInit(): void {
        this.translations = {
            inProgressState: this.$rootScope.t(AssessmentStateKeys.InProgress).toString(),
            underReviewState: this.$rootScope.t(AssessmentStateKeys.UnderReview).toString(),
            needInfoState: this.$rootScope.t(AssessmentStateKeys.InfoNeeded).toString(),
            riskTreatmentState: this.$rootScope.t(AssessmentStateKeys.RiskTreatment).toString(),
            completedState: this.$rootScope.t(AssessmentStateKeys.Completed).toString(),
        };

        this.customTerms = {
            riskAssessmentState: this.$rootScope.t(AssessmentStateKeys.RiskAssessment).toString(),
        };

        if (this.graphData && this.projectSettings) {
            this.pieGraphData = this.preparePieGraphData(this.graphData);
            this.originalPieGraphData = _.cloneDeep(this.pieGraphData);
            this.renderSvg();
        }
    }

    private preparePieGraphData(projectsArr: any[]): IPieGraphData[] {
        const tempPieGraphData: any = [];
        const counts: any = {};
        counts[AssessmentStates.InProgress] = this.$filter("filter")(projectsArr, {
            StateDesc: AssessmentStates.InProgress,
        }).length;
        counts[AssessmentStates.UnderReview] = this.$filter("filter")(projectsArr, {
            StateDesc: AssessmentStates.UnderReview,
        }).length;
        counts[AssessmentStates.InfoNeeded] = this.$filter("filter")(projectsArr, {
            StateDesc: AssessmentStates.InfoNeeded,
        }).length;
        counts[AssessmentStates.RiskAssessment] = this.$filter("filter")(projectsArr, {
            StateDesc: AssessmentStates.RiskAssessment,
        }).length;
        counts[AssessmentStates.RiskTreatment] = this.$filter("filter")(projectsArr, {
            StateDesc: AssessmentStates.RiskTreatment,
        }).length;
        counts[AssessmentStates.Completed] = this.$filter("filter")(projectsArr, {
            StateDesc: AssessmentStates.Completed,
        }).length;
        this.allCountsEmpty = true;
        for (const i in counts) {
            if (counts.hasOwnProperty(i)) {
                if (counts[i] > 0) {
                    this.allCountsEmpty = false;
                    break;
                }
            }
        }
        if (!this.allCountsEmpty) {
            const temp: any = [{
                legendLabel: this.translations.inProgressState,
                magnitude: counts[AssessmentStates.InProgress],
                percentage: ((counts[AssessmentStates.InProgress] / projectsArr.length) * 100).toFixed(1),
                color: "#008ACC",
                show: true,
            }, {
                legendLabel: this.translations.underReviewState,
                magnitude: counts[AssessmentStates.UnderReview],
                percentage: ((counts[AssessmentStates.UnderReview] / projectsArr.length) * 100).toFixed(1),
                color: "#FDA028",
                show: true,
            }, {
                legendLabel: this.translations.needInfoState,
                magnitude: counts[AssessmentStates.InfoNeeded],
                percentage: ((counts[AssessmentStates.InfoNeeded] / projectsArr.length) * 100).toFixed(1),
                color: "#16BDB7",
                show: true,
            }, {
                legendLabel: this.customTerms.riskAssessmentState,
                magnitude: counts[AssessmentStates.RiskAssessment],
                percentage: ((counts[AssessmentStates.RiskAssessment] / projectsArr.length) * 100).toFixed(1),
                color: "#FED03F",
                show: (counts[AssessmentStates.RiskAssessment] || counts[AssessmentStates.RiskTreatment] || this.projectSettings.EnableRiskAnalysis),
            }, {
                legendLabel: this.translations.riskTreatmentState,
                magnitude: counts[AssessmentStates.RiskTreatment],
                percentage: ((counts[AssessmentStates.RiskTreatment] / projectsArr.length) * 100).toFixed(1),
                color: "#55788C",
                show: (counts[AssessmentStates.RiskAssessment] || counts[AssessmentStates.RiskTreatment] || this.projectSettings.EnableRiskAnalysis),
            }, {
                legendLabel: this.translations.completedState,
                magnitude: counts[AssessmentStates.Completed],
                percentage: ((counts[AssessmentStates.Completed] / projectsArr.length) * 100).toFixed(1),
                color: "#6CC04A",
                show: true,
            }];
            _.each(temp, (data: IPieGraphData): void => {
                if (data.show) tempPieGraphData.push(data);
            });
            return tempPieGraphData;
        }
    }

    private renderSvg(): void {
        this.drawPie("PieGraph", this.pieGraphData, "#piegraph", 20, 175, 75, 1);
        this.$rootScope.$on("updatePieGraph", (event: ng.IAngularEvent, pieOrgFilter: string): void => {
            this.redrawSvg(pieOrgFilter);
        });
    }

    private redrawSvg(pieOrgFilter: string): void {
        d3.select("#piegraph svg").remove();
        if (pieOrgFilter !== "") {
            const filteredArray: any[] = [];
            _.each(this.graphData, (project: any): void => {
                if (project.OrgGroupId === pieOrgFilter) {
                    filteredArray.push(project);
                }
            });
            this.pieGraphData = this.preparePieGraphData(filteredArray);
            this.drawPie("PieGraph", this.pieGraphData, "#piegraph", 20, 175, 75, 1);
        } else {
            this.drawPie("PieGraph", this.originalPieGraphData, "#piegraph", 20, 175, 75, 1);
        }
    }

    private drawPie(pieName: string, dataSet: any, selectString: string, margin: number, outerRadius: number, innerRadius: number, sortArcs: number): void {
        // pieName => A unique drawing identifier that has no spaces, no "." and no "#" characters.
        // dataSet => Input Data for the chart, itself.
        // selectString => String that allows you to pass in a D3 select string.
        // margin => Integer margin offset value.
        // outerRadius => Integer outer radius value.
        // innerRadius => Integer inner radius value.
        // sortArcs => Controls sorting of Arcs by value.
        // false = No Sort. Maintain original order.
        // true = Sort by arc value size.
        const translations: any = this.translations;
        const customTerms = this.customTerms;
        const canvasWidth = 550;
        const pieWidthTotal: number = outerRadius * 1.1;
        const pieCenterX: number = outerRadius + margin / 2;
        const pieCenterY: number = outerRadius + margin / 2;
        const legendBulletOffset = 10;
        const legendVerticalOffset: number = outerRadius - margin;
        const legendTextOffset = 20;
        const textVerticalSpace = 20;
        let canvasHeight = 0;
        const pieDrivenHeight: number = outerRadius * 2 + margin * 2;
        const legendTextDrivenHeight: number = (dataSet.length * textVerticalSpace) + margin * 2;
        // Autoadjust Canvas Height
        if (pieDrivenHeight >= legendTextDrivenHeight) {
            canvasHeight = pieDrivenHeight;
        } else {
            canvasHeight = legendTextDrivenHeight;
        }
        const x: any = d3.scale.linear().domain([0, d3.max(dataSet, (d: any): any => {
            return d.magnitude;
        })]).rangeRound([0, pieWidthTotal]);
        const y: any = d3.scale.linear().domain([0, dataSet.length]).range([0, (dataSet.length * 20)]);
        const synchronizedMouseOver: Function = (d: any, i: number): void => {
            const arc: any = d3.select(this);
            const indexValue: any = arc.attr("index_value");
            const arcSelector = `.pie-${pieName}-arc-${indexValue}`;
            const selectedArc: any = d3.selectAll(arcSelector);
            const arcOver: any = d3.svg.arc()
                .innerRadius(innerRadius * 1.2) // Causes center of pie to be hollow
                .outerRadius(outerRadius);
            selectedArc.transition()
                .duration(300)
                .ease("elastic")
                .attr("d", arcOver);
        };
        const synchronizedMouseOut: Function = (d: any, i: number): void => {
            const arc: any = d3.select(this);
            const indexValue: any = arc.attr("index_value");
            const arcSelector: string = "." + "pie-" + pieName + "-arc-" + indexValue;
            const selectedArc: any = d3.selectAll(arcSelector);
            const arcOver: any = d3.svg.arc()
                .innerRadius(innerRadius) // Causes center of pie to be hollow
                .outerRadius(outerRadius * 0.9);
            selectedArc.transition()
                .duration(200)
                .ease("exp")
                .attr("d", arcOver);
        };
        // Define an arc generator. This will create <path> elements for using arc data.
        const arc: any = d3.svg.arc()
            .innerRadius(innerRadius) // Causes center of pie to be hollow
            .outerRadius(outerRadius * 0.9);
        const tweenPie: Function = (b: any): any => {
            b.innerRadius = 0;
            const i: any = d3.interpolate({
                startAngle: 0,
                endAngle: 0,
            }, b);
            return (t: any): any => {
                return arc(i(t));
            };
        };
        // Create a drawing canvas...
        const canvas: any = d3.select(selectString)
            .append("svg:svg") // create the SVG element inside the <body>
            .data([dataSet]) // associate our data with the document
            .attr("width", canvasWidth) // set the width of the canvas
            .attr("height", canvasHeight) // set the height of the canvas
            .attr("align", "center")
            .append("svg:g") // make a group to hold our pie chart
            .attr("transform", "translate(" + pieCenterX + "," + pieCenterY + ")"); // Set center of pie

        // Define a pie layout: the pie angle encodes the value of dataSet.
        // Since our data is in the form of a post-parsed CSV string, the
        // values are Strings which we coerce to Numbers.
        const pie: any = d3.layout.pie()
            .value((d: any): any => {
                return d.magnitude;
            })
            .sort((a: any, b: any): any => {
                if (sortArcs === 1) {
                    return b.magnitude - a.magnitude;
                } else {
                    return null;
                }
            });
        // Select all <g> elements with class slice (there aren't any yet)
        const arcs: any = canvas.selectAll("g.slice")
            // Associate the generated pie data (an array of arcs, each having startAngle,
            // endAngle and value properties)
            .data(pie)
            // This will create <g> elements for every "extra" data element that should be associated
            // with a selection. The result is creating a <g> for every object in the data array
            // Create a group to hold each slice (we will have a <path> and a <text> // element associated with each slice)
            .enter()
            .append("svg:g")
            .attr("class", "slice") // allow us to style things in the slices (like text)
            // Set the color for each slice to be chosen from the color function defined above
            // This creates the actual SVG path using the associated data (pie) with the arc drawing function
            .style("stroke", "White")
            .attr("d", arc);
        arcs.append("svg:path")
            // Set the color for each slice to be chosen from the color function defined above
            // This creates the actual SVG path using the associated data (pie) with the arc drawing function
            .attr("fill", (d: any, i: number): any => {
                return d.data.color;
            })
            .attr("color_value", (d: any): any => {
                return d.data.color;
            }) // Bar fill color...
            .attr("index_value", (d: any, i: number): any => {
                return "index-" + i;
            })
            .attr("class", (d: any, i: number): any => {
                return "pie-" + pieName + "-arc-index-" + i;
            })
            .style("stroke", "White")
            .attr("d", arc)
            .on("mouseover", synchronizedMouseOver)
            .on("mouseout", synchronizedMouseOut)
            .transition()
            .ease("exp")
            .duration(700)
            .attrTween("d", tweenPie);
        // Add a magnitude value to the larger arcs, translated to the arc centroid and rotated.
        arcs.filter((d: any): any => {
            return d.endAngle - d.startAngle > .2;
        }).append("svg:text")
            .attr("dy", ".35em")
            .attr("text-anchor", "middle")
            .attr("transform", (d: any): any => { // set the label's origin to the center of the ar=> c
                // we have to make sure to set these before calling arc.centroid
                d.outerRadius = outerRadius; // Set Outer Coordinate
                d.innerRadius = innerRadius; // Set Inner Coordinate
                const c: any = arc.centroid(d);
                return `translate(${c[0] * 1.1},${c[1] * 1.1})`;
            })
            .style("fill", "White")
            .text((d: any): any => {
                return (d.data.percentage + "%");
            });
        // Computes the angle of an arc, converting from radians to degrees.
        function angle(d: any): number {
            const a: number = (d.startAngle + d.endAngle) * 90 / Math.PI - 90;
            return a > 90 ? a - 180 : a;
        }
        // Plot the bullet circles...
        canvas.selectAll("circle")
            .data(dataSet).enter().append("svg:circle") // Append circle elements
            .attr("cx", pieWidthTotal + legendBulletOffset)
            .attr("cy", (d: any, i: number): any => {
                return i * textVerticalSpace - legendVerticalOffset;
            })
            .attr("stroke-width", ".5")
            .style("fill", (d: any, i: number): any => {
                return d.color;
            }) // Bullet fill color
            .attr("r", 5)
            .attr("color_value", (d: any): any => {
                return d.color;
            }) // Bar fill color...
            .attr("index_value", (d: any, i: number): any => {
                return `index-${i}`;
            })
            .attr("class", (d: any, i: number): any => {
                return `pie-${pieName}-legendBullet-index-${i}`;
            })
            .on("mouseover", synchronizedMouseOver)
            .on("mouseout", synchronizedMouseOut);
        // Create hyper linked text at right that acts as label key...
        canvas.selectAll(".legend_link")
            .data(dataSet) // Instruct to bind dataSet to text elements
            .enter() // Append legend elements
            .append("text")
            .attr("text-anchor", "center")
            .attr("x", pieWidthTotal + legendBulletOffset + legendTextOffset)
            .attr("y", (d: any, i: number): any => {
                return i * textVerticalSpace - legendVerticalOffset;
            })
            .attr("dx", 0)
            .attr("dy", "5px") // Controls padding to place text in alignment with bullets
            .text((d: any): any => {
                return d.legendLabel.toUpperCase();
            })
            .attr("color_value", (d: any): any => {
                return d.color;
            }) // Bar fill color...
            .attr("index_value", (d: any, i: number): any => {
                return `index-${i}`;
            })
            .attr("class", (d: any, i: number): any => {
                return `pie-${pieName}-legendText-index-${i}`;
            })
            .on("mouseover", synchronizedMouseOver)
            .on("mouseout", synchronizedMouseOut);
    }
}

const pieGraph: ng.IComponentOptions = {
    bindings: {
        graphData: "<",
        projectSettings: "<",
    },
    controller: PieGraph,
    template: `
        <div ng-if="!$ctrl.allCountsEmpty" id="piegraph" ng-init="$ctrl.renderSvg()"></div>
        <div ng-if="$ctrl.allCountsEmpty" class="no-data">{{::$root.t("NoProjects")}}</div>
    `,
};

export default pieGraph;
