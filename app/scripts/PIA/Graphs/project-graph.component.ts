declare var d3: any;
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";

// Constants
import { Color } from "constants/color.constant";

class ProjectGraph  implements ng.IComponentController {

    static $inject: string[] = [
        "$rootScope",
        "$scope",
        "$q",
        "$filter",
        "$state",
        "$element",
        "Projects",
        "ENUMS",
    ];

    private graphData;

    constructor(
        private readonly $rootScope: IExtendedRootScopeService,
        private readonly $scope: any,
        private readonly $q: ng.IQService,
        private readonly $filter: ng.IFilterService,
        private readonly $state: ng.ui.IStateService,
        private readonly $element: any,
        private readonly Projects: any,
        private readonly ENUMS: any,
    ) {
    }

    public $onInit(): void {
        this.$scope.graphData = this.graphData;
        this.renderSvg();
    }

    public $onChanges(changesObj: any): void {
        this.$scope.graphData = changesObj.graphData.currentValue;
        if (this.$scope.svg) {
            this.update();
        }
    }

    public $onDestroy() {
        this.destroySvg();
    }

    private redrawSvg(): void {
        if (this.$scope.svg) {
            this.destroySvg();
            this.renderSvg();
        } else {
            this.renderSvg();
        }
    }

    private renderSvg(): void {
        const margins = {
            left: 100,
            right: 50,
            top: 0,
            bottom: 0,
        };

        this.$scope.colorMap = {
            0: Color.White,
            1: Color.RiskLow,
            2: Color.RiskMedium,
            3: Color.RiskHigh,
            4: Color.RiskVeryHigh,
        };

        const width = 300;
        const height = 350;

        this.$scope.radius = Math.min(width, height) / 2;

        this.$scope.riskLabelMap = [
            "No Risk",
            "Low",
            "Medium",
            "High",
            "Very High",
        ];

        this.$scope.svg = d3.select("#projectgraph")
            .append("svg")
            .append("g");

        this.$scope.svg.attr("transform", "translate(" + (margins.left + (width / 2)) + "," + (margins.top + (height / 2)) + ")");

        this.$scope.sliceGroup = this.$scope.svg.append("g").attr("class", "slices");
        this.$scope.labelGroup = this.$scope.svg.append("g").attr("class", "labels");
        this.$scope.lineGroup = this.$scope.svg.append("g").attr("class", "lines");

        this.$scope.pie = d3.layout.pie()
            .sort(null)
            .value((d) => d.count);

        this.$scope.arc = d3.svg.arc()
            .outerRadius(this.$scope.radius * 0.8)
            .innerRadius(this.$scope.radius * 0.4);

        this.$scope.outerArc = d3.svg.arc()
            .innerRadius(this.$scope.radius * 0.9)
            .outerRadius(this.$scope.radius * 0.9);

        this.update();
    }

    private update(): void {
        const slices = this.$scope.sliceGroup.selectAll("path")
            .data(this.$scope.pie(this.$scope.graphData))
            .attr("d", this.$scope.arc)
            .attr("fill", (d, i) => this.$scope.colorMap[d.data.id]);

        slices
            .enter()
            .append("path")
            .attr("d", this.$scope.arc)
            .attr("fill", (d, i) => this.$scope.colorMap[d.data.id]);

        slices.exit().remove();

        this.$scope.labelGroup.selectAll("text").remove();
        this.$scope.lineGroup.selectAll("polyline").remove();

        const texts = this.$scope.labelGroup.selectAll("text")
            .data(this.$scope.pie(this.$scope.graphData))
            .attr("dy", ".35em")
            .text((d) => {
                const pieSliceLabel = d.data.label + ", " + d.data.percentage + "%";
                return pieSliceLabel;
            });

        texts
            .enter()
            .append("text")
            .attr("dy", ".35em")
            .text((d) => {
                const pieSliceLabel = d.data.label + ", " + d.data.percentage + "%";
                return pieSliceLabel;
            })
            .attr("transform", (d) => {
                const pos = this.$scope.outerArc.centroid(d);
                pos[0] = this.$scope.radius * (this.midAngle(d) < Math.PI ? 1 : -1);
                return "translate(" + pos + ")";
            })
            .style("text-anchor", (d) => this.midAngle(d) < Math.PI ? "start" : "end");

        const lines = this.$scope.lineGroup.selectAll("polyline")
            .data(this.$scope.pie(this.$scope.graphData));

        lines
            .enter()
            .append("polyline")
            .attr("points", (d) => {
                const pos = this.$scope.outerArc.centroid(d);
                pos[0] = this.$scope.radius * 0.95 * (this.midAngle(d) < Math.PI ? 1 : -1);
                return [this.$scope.arc.centroid(d), this.$scope.outerArc.centroid(d), pos];
            });
    }

    private midAngle(d) {
        return d.startAngle + (d.endAngle - d.startAngle) / 2;
    }

    private destroySvg() {
        this.$scope.svg.remove();
    }
}

const projectGraph: ng.IComponentOptions = {
    controller: ProjectGraph,
    template: () => `<div id="projectgraph"></div>`,
    bindings: {
        graphData: "<",
    },
};

export default projectGraph;
