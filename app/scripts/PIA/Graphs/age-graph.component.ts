declare var d3: any;
import * as _ from "lodash";
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import { IAgeDate } from "interfaces/age-graph.interface";
import { LanguageCodes } from "constants/language-codes.constant";
import { AssessmentStates, AssessmentStateKeys } from "constants/assessment.constants";
import { TimeStamp } from "oneServices/time-stamp.service";

class AgeGraph implements ng.IComponentController {
    static $inject: string[] = ["$rootScope", "CONSTANTS", "$state", "TimeStamp"];

    private graphData: any;

    private translations: any;
    private terms: any;
    private userLanguage: string;

    private dayMS: number = 24 * 60 * 60 * 1000;

    private ageGraphData: IAgeDate[] = [];
    private statusMap: any = {
        "In Progress": "1",
        "Under Review": "2",
        "More Info Needed": "3",
        "Risk Assessment": "4",
        "Risk Treatment": "5",
    };

    private statusMapArray: any[] = [];

    constructor(
        private readonly $rootScope: IExtendedRootScopeService,
        readonly CONSTANTS: any,
        readonly $state: ng.ui.IStateService,
        private readonly TimeStamp: TimeStamp,
    ) {
        this.userLanguage = this.$rootScope.tenantLanguage;
        this.translations = {
            inProgressState: $rootScope.t(AssessmentStateKeys.InProgress).toString(),
            underReviewState: $rootScope.t(AssessmentStateKeys.UnderReview).toString(),
            needInfoState: $rootScope.t(AssessmentStateKeys.InfoNeeded).toString(),
            riskAssessmentState: $rootScope.t(AssessmentStateKeys.RiskAssessment).toString(),
            riskTreatmentState: $rootScope.t(AssessmentStateKeys.RiskTreatment).toString(),
            completedState: $rootScope.t(AssessmentStateKeys.Completed).toString(),
        };
        this.terms = CONSTANTS.TERMS;
        this.statusMapArray = [
            this.translations.inProgressState,
            this.translations.underReviewState,
            this.translations.needInfoState,
            this.translations.riskAssessmentState,
            this.translations.riskTreatmentState,
        ];
    }

    public $onInit(): void {
        if (this.graphData) {
            this.prepareAgeGraphData(this.graphData);
            this.renderSvg();
        }
    }

    private prepareAgeGraphData(projectsArr: any): void {
        _.each(projectsArr, (project: any): void => {
            if (project.StateDesc === "Completed" || project.StateDesc === "Sample") {
                return;
            }

            const chartObject: IAgeDate = {
                Name: project.Name,
                OrgGroup: project.OrgGroup,
                StateDesc: project.StateDesc,
                StateName: this.getName(project.StateDesc),
                Time: this.TimeStamp.getMillisecondsSince(project.CreatedDT),
                Day: this.TimeStamp.getMillisecondsSince(project.CreatedDT) / this.dayMS,
                TimeSinceCreated: this.TimeStamp.returnDayTime(project.CreatedDT),
                Id: project.Id,
                Version: project.Version,
            };
            this.ageGraphData.push(chartObject);
        });
    }

    private renderSvg(): void {
        this.drawAgeGraph(this.ageGraphData, this.CalcYRange(this.ageGraphData)); // sets graph to default show all
        this.$rootScope.$on("updateAgingProjectGraph", (event: ng.IAngularEvent, projectAgeFilter: string): void => {
            this.redrawSvg(projectAgeFilter);
        });
    }

    private redrawSvg(projectAgeFilter: string): void {
        d3.select("#agegraph svg").remove();
        if (projectAgeFilter !== "") {
            const filteredArray: any[] = [];
            _.each(this.ageGraphData, (project: any): void => {
                if (project && project.DaysAgo <= projectAgeFilter) {
                    filteredArray.push(project);
                }
            });
            const yAxisRange: any[] = [this.CalcYRange(filteredArray)[0], projectAgeFilter];
            this.drawAgeGraph(filteredArray, yAxisRange);
        } else {
            this.drawAgeGraph(this.ageGraphData, this.CalcYRange(this.ageGraphData));
        }
    }

    private getName: Function = (state: any): void => {
        switch (state) {
            case AssessmentStates.InProgress:
                return this.translations.inProgressState;
            case AssessmentStates.UnderReview:
                return this.translations.underReviewState;
            case AssessmentStates.InfoNeeded:
                return this.translations.needInfoState;
            case AssessmentStates.RiskAssessment:
                return this.translations.riskAssessmentState;
            case AssessmentStates.RiskTreatment:
                return this.translations.riskTreatmentState;
            case AssessmentStates.Completed:
                return this.translations.completedState;
            default:
                return state;
        }
    }

    private goToProject: Function = (Id: string, Version: number): void => {
        this.$state.go("zen.app.pia.module.projects.assessment.module.questions", {
            Id,
            Version,
        });
    }

    private CalcXRange(data: any): any[] {
        return [this.statusMap["In Progress"], this.statusMap["Risk Treatment"]];
    }

    private CalcYRange(data: any): any[] {
        const arr: any[] = d3.extent(data, (d: any): any => d.Day);
        arr[0] = Math.floor(arr[0]);
        arr[1] = Math.ceil(arr[1]);
        if (arr[1] < 4) {
            arr[0] = 0;
            arr[1] = 4;
        }
        return arr;
    }

    private drawAgeGraph(data: IAgeDate[], yAxisRange: any): void {
        d3.selectAll(".tooltip-info").remove();
        const width = 440;
        const height = 250;
        const timeFormat: number = d3.time.format("%x");

        _.each(data, (d: IAgeDate): void => {
                d.DaysAgo = Math.floor(d.Time / this.dayMS);
        });

        // just to have some space around items.
        const margins: any = {
            left: 50,
            right: 120,
            top: 20,
            bottom: 50,
        };

        const colorMap: any = {
            "In Progress": "#008ACC",
            "Under Review": "#FDA028",
            "More Info Needed": "#16BDB7",
            "Risk Assessment": "#FED03F",
            "Risk Treatment": "#55788C",
            "Completed": "#6CC04A",
        };

        // this will be our colour scale. An Ordinal scale.
        const colors: any = d3.scale.category10();
        // we add the SVG component to the scatter-load div
        const svg: any = d3.select("#agegraph").append("svg").attr("width", width).attr("height", height).append("g")
            .attr("transform", `translate(${margins.left},${margins.top})`);

        // this sets the scale that we're using for the X axis.
        // the domain define the min and max constiables to show. In this case, it's the series of states.
        const x: any = d3.scale.linear()
            .domain(this.CalcXRange(data))
            // the range maps the domain to values from 0 to the width minus the left and right margins (used to space out the visualization)
            .range([0, width - margins.left - margins.right]);

        // this does the same as for the y axis but maps from the rating constiable to the height to 0.
        const y: any = d3.scale.linear()
            .domain(yAxisRange)
            // Note that height goes first due to the weird SVG coordinate system
            .range([height - margins.top - margins.bottom, 0]);

        // we add the axes SVG component. At this point, this is just a placeholder. The actual axis will be added in a bit
        svg.append("g").attr("class", "x axis").attr("transform", "translate(0," + y.range()[0] + ")");
        svg.append("g").attr("class", "y axis");

        // this is our Y axis label. Nothing too special to see here.
        svg.append("text")
            .attr("fill", "#414241")
            .attr("text-anchor", "end")
            .attr("y", -10)
            .attr("x", -10)
            .text("(days)");

        // this is the actual definition of our x and y axes. The orientation refers to where the labels appear - for the x axis, below or above the line, and for the y axis, left or right of the line. Tick padding refers to how much space between the tick and the label. There are other parameters too - see https://github.com/mbostock/d3/wiki/SVG-Axes for more information
        const xAxis: any = d3.svg.axis()
            .scale(x)
            .orient("bottom")
            .tickPadding(2)
            .tickFormat((d: any): any => this.statusMapArray[d - 1]);
        const yAxis: any = d3.svg.axis()
            .scale(y)
            .orient("left")
            .ticks(5)
            .tickFormat((d: any): any => d.toFixed(0));

        // this is where we select the axis we created a few lines earlier. See how we select the axis item. in our svg we appended a g element with a x/y and axis class. To pull that back up, we do this svg select, then 'call' the appropriate axis object for rendering.
        svg.selectAll("g.y.axis").call(yAxis);
        svg.selectAll("g.x.axis").call(xAxis).selectAll("text")
            .attr("y", 10)
            .attr("x", 0)
            .attr("transform", "rotate(15)")
            .style("text-anchor", "start");

        // now, we can get down to the data part, and drawing stuff. We are telling D3 that all nodes (g elements with class node) will have data attached to them. The 'key' we use (to let D3 know the uniqueness of items) will be the name. Not usually a great key, but fine for this example.
        const chocolate: any = svg.selectAll("g.node").data(data, (d: IAgeDate): any => {
            return d.Day;
        });

        // we 'enter' the data, making the SVG group (to contain a circle and text) with a class node. This corresponds with what we told the data it should be above.
        const chocolateGroup: any = chocolate.enter().append("g").attr("class", "node")
            // this is how we set the position of the items. Translate is an incredibly useful function for rotating and positioning items
            .attr("transform", (d: IAgeDate): any => {
                return `translate(${(x(this.statusMap[d.StateDesc]) + 5)},${y(d.Day)})`;
            });

        // add the tooltip area to the webpage
        const tooltip = d3.select(".text").append("div")
            .attr("class", "tooltip-info")
            .style("opacity", .9)
            .html("<h6>" + this.$rootScope.t("RollOver") + " " + this.$rootScope.customTerms.Projects + " " + this.$rootScope.t("ForInformation") + "</h6>");

        // we add our first graphics element! A circle!
        chocolateGroup.append("circle")
            .attr("r", 5)
            .attr("class", "dot grow")
            .style("fill", (d: IAgeDate): any => {
                // remember the ordinal scales? We use the colors scale to get a colour for our manufacturer. Now each node will be coloured
                // by who makes the chocolate.
                return colorMap[d.StateDesc];
            })
            .on("mouseover", (d: IAgeDate): any => {
                tooltip.transition()
                    .duration(200)
                    .style("opacity", .9);
                tooltip.html("<h6>" + this.$rootScope.customTerms.Project + " Name</h6> " + d.Name +
                    "<h6>" + this.$rootScope.t("DaysCurrentStatus") + "</h6> " + d.TimeSinceCreated +
                    "</br><h6>" + this.$rootScope.t("OrganizationGroup") + "</h6> " + d.OrgGroup +
                    "</br><h6>" + this.$rootScope.customTerms.Project + " " + this.$rootScope.t("Status") + "</h6> " + d.StateName)
                    .style("left", 0 + "px")
                    .style("top", 0 + "px");
            })
            .on("mouseout", (d: IAgeDate): any => {
                tooltip.html("<h6>" + this.$rootScope.t("RollOver") + " " + this.$rootScope.customTerms.Projects + " " + this.$rootScope.t("ForInformation") + "</h6>");
            })
            .on("click", (d: IAgeDate): any => {
                this.goToProject(d.Id, d.Version);
                d3.event.stopPropagation();
            });

    }

}

const ageGraph: ng.IComponentOptions = {
    bindings: {
        graphData: "<",
    },
    controller: AgeGraph,
    template: `
        <div id="agegraph"></div>`,
};

export default ageGraph;
