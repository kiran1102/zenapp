// Services
import { GlobalSidebarService } from "modules/global-sidebar/services/one-global-sidebar.service";
import { Permissions } from "modules/shared/services/helper/permissions.service";

// Interfaces
import { IStringMap } from "interfaces/generic.interface";

export function routes($stateProvider: ng.ui.IStateProvider, $urlRouterProvider: ng.ui.IUrlRouterProvider) {

    $urlRouterProvider.when("/pia/questionnaire", "/pia/questionnaire/");
    $urlRouterProvider.when("/pia/questionnaire/archive", "/pia/questionnaire/archive/");

    $stateProvider
        .state("zen.app.pia.module.templates", {
            abstract: true,
            url: "questionnaire/",
            resolve: {
                TemplatePermission: [
                    "GeneralData", "Permissions",
                    (GeneralData: IStringMap<any>, permissions: Permissions): boolean => {
                        if (permissions.canShow("Templates") && !permissions.canShow("TemplatesV2")) {
                            return true;
                        } else {
                            permissions.goToFallback();
                        }
                        return false;
                    },
                ],
            },
            views: {
                module: {
                    template: "<ui-view></ui-view>",
                    controller: [
                        "GlobalSidebarService",
                        (globalSidebarService: GlobalSidebarService) => {
                            globalSidebarService.setAAMenu();
                        },
                    ],
                },
            },
        })
        .state("zen.app.pia.module.templates.list", {
            url: "",
            params: { time: null },
            template: require("piaviews/Templates/pia_templates.jade"),
            controller: "TemplatesCtrl",
        })
        .state("zen.app.pia.module.templates.archive", {
            url: "archive/",
            template: require("piaviews/Templates/pia_templates_archive.jade"),
            controller: "TemplatesArchiveCtrl",
        })
        .state("zen.app.pia.module.templates.studio", {
            url: "studio/",
            abstract: true,
            template: "<ui-view></ui-view>",
        })
        .state("zen.app.pia.module.templates.studio.gallery", {
            url: "gallery",
            template: require("piaviews/Templates/pia_template_studio.jade"),
            controller: "TemplatesStudioGalleryCtrl",
        })
        .state("zen.app.pia.module.templates.studio.manage", {
            url: "manage/:templateId/:version",
            template: require("piaviews/Templates/pia_template_manage.jade"),
            controller: "TemplateStudioManageCtrl",
            resolve: {
                param: ["$stateParams", ($stateParams: ng.ui.IStateParamsService): void => {
                    if (!$stateParams.version) {
                        $stateParams.version = 1;
                    }
                }],
            },
        });
}

routes.$inject = ["$stateProvider", "$urlRouterProvider"];
