declare var angular: angular.IAngularStatic;

// import config from "./config";
import { routes } from "./pia.routes";
import run from "./run";

import DashboardController from "./Controllers/Dashboard/dashboard.controller";
import AssignDMCtrl from "./Controllers/DataMapping/Modals/assignment.controller";
import DataTableCtrl from "./Controllers/DataMapping/Tables/DataTableController";
import GlobalFlowGraphController from "./Controllers/DataMapping/Visuals/GlobalFlowGraphController";
import DataMappingCtrl from "./Controllers/DataMapping/datamapping.controller";
import AddDocumentCtrl from "./Controllers/Documents/AddDocumentController";
import DocumentsMainCtrl from "./Controllers/Documents/DocumentsMainController";
import DocumentsMenuCtrl from "./Controllers/Documents/DocumentsMenuController";
import DataFlowGraphController from "./Controllers/old_datamapping/DataFlowGraphController";
// import from "./Controllers/old_datamapping/GlobalFlowGraphController";
import OrganizationCreateCtrl from "./Controllers/Organizations/OrgGroupsCreateController";
import OrganizationEditCtrl from "./Controllers/Organizations/OrgGroupsEditController";
import AnalysisModalCtrl from "./Controllers/Projects/AnalysisModalController";
import AssessmentCtrl from "./Controllers/Projects/assessment.controller";
import assessmentsPageSlideCtrl from "./Controllers/Projects/AssessmentsPageSlideController";
import AttachmentController from "./Controllers/Projects/AttachmentController";
import DeadlinesModalCtrl from "./Controllers/Projects/DeadlinesController";
import NextStepsController from "./Controllers/Projects/NextStepsController";
import ProjectAssignmentsCtrl from "./Controllers/Projects/ProjectAssignmentsController";
import ProjectHistoryCtrl from "./Controllers/Projects/ProjectHistoryController";
import ProjectReviewerCtrl from "./Controllers/Projects/ProjectReviewerController";
import ProjectsCtrl from "./Controllers/Projects/ProjectsController";
import ProjectsWizardFormCtrl from "./Controllers/Projects/ProjectsWizardFormController";
import ProjectTagsCtrl from "./Controllers/Projects/ProjectTagsController";
import ProjectsWizardSelectCtrl from "./Controllers/Projects/ProjectsWizardSelectController";
import QuestionCommentsCtrl from "./Controllers/Projects/QuestionCommentsController";
import QuestionHistoryCtrl from "./Controllers/Projects/QuestionHistoryController";
import SendBackCtrl from "./Controllers/Projects/SendBackController";
import SuspendModalController from "./Controllers/Projects/suspend-modal.controller";
import ReadinessAssessmentCtrl from "./Controllers/Readiness/ReadinessAssessmentController";
import ReadinessDashboardCtrl from "./Controllers/Readiness/ReadinessDashboardController";
import ReadinessReportCtrl from "./Controllers/Readiness/ReadinessReportController";
import RiskController from "./Controllers/Risk/risk.controller";
import PortalModalCtrl from "./Controllers/SelfService/ss-portal-modal.controller";
import SelfServicePortalCtrl from "./Controllers/SelfService/ss-portal.controller";
import APISettingsController from "./Controllers/Settings/APISettingsController";
import BrandingSettingsController from "./Controllers/Settings/BrandingSettingsController";
import CertificateUploadController from "./Controllers/Settings/CertificateUploadController";
import CustomTermSettingsController from "./Controllers/Settings/CustomTermSettingsController";
import DirectorySettingsController from "./Controllers/Settings/directory-settings.controller";
import GeneralSettingsController from "./Controllers/Settings/general-settings.controller";
import ProjectSettingsController from "./Controllers/Settings/ProjectSettingsController";
import RisksSettingsController from "./Controllers/Settings/risk-settings.controller";
import TaggingSettingsController from "./Controllers/Settings/TaggingSettingsController";
import ThanksCtrl from "./Controllers/Thanks/ThanksController";
import projectGraph from "./Graphs/project-graph.component";
import ShareProjectModalCtrl from "./Controllers/Projects/share-project-modal.controller";
import { settingsEmailTemplate } from "settingsComponents/settings-email-template/settings-email-template.component";

import AssessmentSectionHeader from "generalcomponent/Assessment/assessment-section-header.component";
import AssessmentMenu from "generalcomponent/Assessment/assessment-menu.component";
import AssessmentQuestion from "generalcomponent/Assessment/assessment-question.component";
import AssessmentFooterNew from "generalcomponent/Assessment/assessment-footer-new/assessment-footer-new.component";
import AssessmentQuestionContent from "generalcomponent/Assessment/assessment-question-content/assessment-question-content.component";
import AssessmentQuestionSideButtons from "generalcomponent/Assessment/assessment-question-side-buttons/assessment-question-side-buttons.component";
import AssessmentQuestionAddons from "generalcomponent/Assessment/assessment-question-addons/assessment-question-addons.component";
import AssessmentQuestionNumber from "generalcomponent/Assessment/assessment-question-number/assessment-question-number.component";
import AssessmentQuestionYesNo from "generalcomponent/Assessment/assessment-question-yes-no/assessment-question-yes-no.component";
import AssessmentQuestionDate from "generalcomponent/Assessment/assessment-question-date/assessment-question-date.component";
import AssessmentQuestionMultichoice from "generalcomponent/Assessment/assessment-question-multichoice/assessment-question-multichoice.component";
import AssessmentQuestionMultiselect from "generalcomponent/Assessment/assessment-question-multiselect/assessment-question-multiselect.component";
import AssessmentQuestionTextbox from "generalcomponent/Assessment/assessment-question-textbox/assessment-question-textbox.component";
import AssessmentQuestionInventory from "generalcomponent/Assessment/assessment-question-inventory/assessment-question-inventory.component";
import AssessmentQuestionDataElement from "generalcomponent/Assessment/assessment-question-data-element/assessment-question-data-element.component";
import AssessmentQuestionHeader from "generalcomponent/Assessment/assessment-question-header/assessment-question-header.component";
import AssessmentQuestionJustification from "generalcomponent/Assessment/assessment-question-justification/assessment-question-justification.component";
import {
    AssessmentQuestionNotSureButton,
    AssessmentQuestionNotApplicableButton,
    AssessmentQuestionOtherOptionButton,
} from "generalcomponent/Assessment/assessment-question-buttons/assessment-question-buttons.component";
import AssessmentBusinessLogicService from "generalcomponent/Assessment/assessment-business-logic.service";
import AssessmentQuestionValidationService from "generalcomponent/Assessment/assessment-question-validation.service";
import AssessmentQuestionViewLogicService from "generalcomponent/Assessment/assessment-question-view-logic.service";
import AssessmentSideButtonsViewLogicService from "generalcomponent/Assessment/assessment-question-side-buttons/assessment-side-buttons-view-logic.service";
import AssessmentFooterPermissionService from "generalcomponent/Assessment/assessment-footer-permission.service";
import AssessmentFooterViewLogicService from "generalcomponent/Assessment/assessment-footer-view-logic.service";
import AssessmentMenuPermissionService from "generalcomponent/Assessment/assessment-menu-permission.service";
import AssessmentMenuViewLogicService from "generalcomponent/Assessment/assessment-menu-view-logic.service";
import AssessmentSectionHeaderViewLogicService from "generalcomponent/Assessment/assessment-section-header-view-logic.service";
import AssessmentSectionHeaderPermissionService from "generalcomponent/Assessment/assessment-section-header-permission.service";

import AssessmentQuestionPermissionService from "generalcomponent/Assessment/assessment-question-permission.service";
import NeedsMoreInfoPermissionService from "generalcomponent/Assessment/assessment-nmi-permission.service";

import NeedsMoreInfoModal from "generalcomponent/Modals/needs-more-info-modal/needs-more-info-modal.component";
import ViewInfoAddedModal from "generalcomponent/Modals/view-info-added-modal/view-info-added-modal.component";
import AddInfoModal from "generalcomponent/Modals/add-info-modal/add-info-modal.component";

import ConditionsModalCtrl from "generalcomponent/Modals/conditions-modal/conditions-modal.controller";
import QuestionModalCtrl from "generalcomponent/Modals/question-modal/question-modal.controller";

import DashboardService from "./Controllers/Dashboard/dashboard.service";
import pieGraph from "./Graphs/pie-graph.component";
import ageGraph from "./Graphs/age-graph.component";

angular.module("zen.pia", [
    // Angular modules
    "ui.bootstrap",
    "ui.router",
    "ui.tree",
    "ngMaterial",
    "ngSanitize",
    "ngStorage",

    // Custom modules
    "zen.identity",
    "zen.template",

    // 3rd Party Modules
    "luegg.directives", // scroll-glue
    "dndLists", // Drag and Drop List'
    "xeditable", // Xeditable Goodness
    "ngTagsInput", // TagsInput req. for tagging
    "minicolors", // Color Picker
    "angularFileUpload", // Angular File Upload
    "angularMoment", // Angular Momen

])

    // .config(config)
    .config(routes)
    .run(run)
    .controller("DashboardCtrl", DashboardController)
    .controller("AssignDMAssessmentsCtrl", AssignDMCtrl)
    .controller("DataTableCtrl", DataTableCtrl)
    .controller("GlobalFlowGraphController", GlobalFlowGraphController)
    .controller("DataMappingCtrl", DataMappingCtrl)
    .controller("AddDocumentCtrl", AddDocumentCtrl)
    .controller("DocumentsMainCtrl", DocumentsMainCtrl)
    .controller("DocumentsMenuCtrl", DocumentsMenuCtrl)
    .controller("DataFlowGraphController", DataFlowGraphController)
    .controller("OrganizationCreateCtrl", OrganizationCreateCtrl)
    .controller("OrganizationEditCtrl", OrganizationEditCtrl)
    .controller("AnalysisModalCtrl", AnalysisModalCtrl)
    .controller("AssessmentCtrl", AssessmentCtrl)
    .controller("assessmentsPageSlideCtrl", assessmentsPageSlideCtrl)
    .controller("AttachmentController", AttachmentController)
    .controller("DeadlinesModalCtrl", DeadlinesModalCtrl)
    .controller("NextStepsController", NextStepsController)
    .controller("ProjectAssignmentsCtrl", ProjectAssignmentsCtrl)
    .controller("ProjectHistoryCtrl", ProjectHistoryCtrl)
    .controller("ProjectReviewerCtrl", ProjectReviewerCtrl)
    .controller("ProjectsCtrl", ProjectsCtrl)
    .controller("ProjectsWizardFormCtrl", ProjectsWizardFormCtrl)
    .controller("ProjectsWizardSelectCtrl", ProjectsWizardSelectCtrl)
    .controller("ProjectTagsCtrl", ProjectTagsCtrl)
    .controller("QuestionCommentsCtrl", QuestionCommentsCtrl)
    .controller("QuestionHistoryCtrl", QuestionHistoryCtrl)
    .controller("SendBackCtrl", SendBackCtrl)
    .controller("ReadinessAssessmentCtrl", ReadinessAssessmentCtrl)
    .controller("ReadinessDashboardCtrl", ReadinessDashboardCtrl)
    .controller("ReadinessReportCtrl", ReadinessReportCtrl)
    .controller("RiskCtrl", RiskController)
    .controller("PortalModalCtrl", PortalModalCtrl)
    .controller("SelfServicePortalCtrl", SelfServicePortalCtrl)
    .controller("APISettingsController", APISettingsController)
    .controller("BrandingSettingsController", BrandingSettingsController)
    .controller("CertificateUploadController", CertificateUploadController)
    .controller("CustomTermSettingsController", CustomTermSettingsController)
    .controller("DirectorySettingsController", DirectorySettingsController as any)
    .controller("GeneralSettingsController", GeneralSettingsController)
    .controller("ProjectSettingsController", ProjectSettingsController)
    .controller("RisksSettingsController", RisksSettingsController)
    .controller("TaggingSettingsController", TaggingSettingsController)
    .controller("ThanksCtrl", ThanksCtrl)
    .component("projectGraph", projectGraph)
    .component("pieGraph", pieGraph)
    .component("ageGraph", ageGraph)
    .component("assessmentSectionHeader", AssessmentSectionHeader)
    .component("assessmentMenu", AssessmentMenu)
    .component("assessmentQuestion", AssessmentQuestion)
    .component("assessmentFooterNew", AssessmentFooterNew)
    .component("assessmentQuestionContent", AssessmentQuestionContent)
    .component("assessmentQuestionSideButtons", AssessmentQuestionSideButtons)
    .component("assessmentQuestionAddons", AssessmentQuestionAddons)
    .component("assessmentQuestionNumber", AssessmentQuestionNumber)
    .component("assessmentQuestionYesNo", AssessmentQuestionYesNo)
    .component("assessmentQuestionDate", AssessmentQuestionDate)
    .component("assessmentQuestionMultichoice", AssessmentQuestionMultichoice)
    .component("assessmentQuestionMultiselect", AssessmentQuestionMultiselect)
    .component("assessmentQuestionTextbox", AssessmentQuestionTextbox)
    .component("assessmentQuestionInventory", AssessmentQuestionInventory)
    .component("assessmentQuestionDataElement", AssessmentQuestionDataElement)
    .component("assessmentQuestionHeader", AssessmentQuestionHeader)
    .component("assessmentQuestionJustification", AssessmentQuestionJustification)
    .component("assessmentQuestionNotSureButton", AssessmentQuestionNotSureButton)
    .component("assessmentQuestionNotApplicableButton", AssessmentQuestionNotApplicableButton)
    .component("assessmentQuestionOtherOptionButton", AssessmentQuestionOtherOptionButton)
    .component("settingsEmailTemplateComponent", settingsEmailTemplate)

    .service("AssessmentBusinessLogic", AssessmentBusinessLogicService)
    .service("AssessmentQuestionValidation", AssessmentQuestionValidationService)
    .service("AssessmentQuestionViewLogic", AssessmentQuestionViewLogicService)
    .service("AssessmentSideButtonsViewLogic", AssessmentSideButtonsViewLogicService)
    .service("AssessmentFooterPermission", AssessmentFooterPermissionService)
    .service("AssessmentFooterViewLogic", AssessmentFooterViewLogicService)
    .service("AssessmentMenuPermission", AssessmentMenuPermissionService)
    .service("AssessmentMenuViewLogic", AssessmentMenuViewLogicService)
    .service("AssessmentSectionHeaderViewLogic", AssessmentSectionHeaderViewLogicService)
    .service("AssessmentSectionHeaderPermission", AssessmentSectionHeaderPermissionService)

    .service("AssessmentQuestionPermission", AssessmentQuestionPermissionService)
    .service("NeedsMoreInfoPermission", NeedsMoreInfoPermissionService)

    .component("needsMoreInfoModal", NeedsMoreInfoModal)
    .component("viewInfoAddedModal", ViewInfoAddedModal)
    .component("addInfoModal", AddInfoModal)

    .controller("ConditionsModalCtrl", ConditionsModalCtrl)
    .controller("QuestionModalCtrl", QuestionModalCtrl)
    .controller("ShareProjectModalCtrl", ShareProjectModalCtrl)
    .controller("SuspendModalController", SuspendModalController)
    .service("DashboardService", DashboardService)
    ;
