import * as _ from "lodash";
import RiskBusinessLogicService from "oneServices/logic/risk-business-logic.service";
import { GlobalSidebarService } from "modules/global-sidebar/services/one-global-sidebar.service";
import { Wootric } from "sharedModules/services/provider/wootric.service";
import { Content } from "sharedModules/services/provider/content.service";
import { OnboardingService } from "modules/onboarding/services/onboarding.service";
import { NotificationService } from "modules/shared/services/provider/notification.service";

import { RiskStates } from "enums/risk-states.enum";
import { ExportTypes } from "enums/export-types.enum";
import { WootricModuleMap } from "modules/shared/constants/wootric.constants";

import { IRiskStates, IRisk, IRiskGetPageParams, IRiskStateFilter, IRiskGetPageParamsFilter } from "interfaces/risk.interface";
import { IProtocolPacket } from "interfaces/protocol.interface";
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import { INavMenuItem } from "modules/global-sidebar/interfaces/nav-menu-item.interface";

export default class RiskController {
    static $inject: string[] = [
        "$rootScope",
        "$scope",
        "$q",
        "$sessionStorage",
        "$filter",
        "$state",
        "$timeout",
        "$http",
        "Content",
        "Export",
        "RiskBusinessLogic",
        "currentUser",
        "Permissions",
        "ENUMS",
        "Tags",
        "Settings",
        "OneTable",
        "RiskAPI",
        "OnboardingService",
        "GlobalSidebarService",
        "Wootric",
        "NotificationService",
    ];

    private riskStates: IRiskStates;
    private translate: any;

    constructor(
        public readonly $rootScope: IExtendedRootScopeService,
        private readonly $scope: any,
        private readonly $q: ng.IQService,
        private readonly $sessionStorage: any,
        private readonly $filter: ng.IFilterService,
        private readonly $state: ng.ui.IStateService,
        private readonly $timeout: ng.ITimeoutService,
        private $http: ng.IHttpService,
        private readonly content: Content,
        private readonly Export: any,
        private readonly RiskBusinessLogic: RiskBusinessLogicService,
        private readonly currentUser: any,
        private readonly Permissions: any,
        private readonly ENUMS: any,
        private readonly Tags: any,
        private readonly Settings: any,
        private readonly OneTable: any,
        private readonly RiskAPI: any,
        private onboardingService: OnboardingService,
        private globalSidebarService: GlobalSidebarService,
        private wootric: Wootric,
        private notificationService: NotificationService,
    ) {
        this.translate = $rootScope.t;
        this.riskStates = ENUMS.RiskState;

        this.init();
    }

    $onInit() {
        this.globalSidebarService.set(this.getMenuRoutes(), this.translate("Risks"));
        this.wootric.run(WootricModuleMap.RiskRegister);
    }

    private init(): void {
        this.$scope.RiskStateENUM = RiskStates;
        this.$scope.currentUser = this.currentUser;
        this.$scope.Ready = false;
        this.$scope.hasFiltersApplied = false;
        this.$scope.page = this.$state.params.page || 0;
        this.$scope.Metadata = {};
        this.$scope.riskStates = {
            allStatesEnabled: true,
            filters: [],
        };

        this.onboardingService.onboard(
            this.translate("WelcomeToRisks"),
            this.content.GetKey("OnBoardingRisks"),
        );

        this.$scope.filtersClosed = true;
        this.$scope.ProjectRisks = [];
        this.$scope.filterOpen = false;
        this.$scope.filterOverflow = false;
        this.$scope.risksFilter = { QuestionTags: [] };
        this.$scope.questionTaggingEnabled = false;
        this.$scope.report = {
            template: "",
            projects: [],
            view: "",
            columns: [],
            filters: [],
            search: "",
        };

        const getRisks = (): any => {
            const filterOptions: any[] = this.$scope.riskStates.filters;
            const statusFilters: number[] = this.getStatusFilterParams();
            this.$scope.hasFiltersApplied = statusFilters.length !== filterOptions.length;
            const filters: IRiskGetPageParamsFilter[] = [{
                name: "state",
                value: statusFilters,
            }];
            const params: IRiskGetPageParams = {
                page: this.$scope.page,
                filters,
            };
            const stateParams: any = this.getStateParams(params);
            this.$state.go(".", stateParams, { notify: false });
            return this.RiskAPI.getPage(params).then((response: IProtocolPacket): IRisk[] => {
                if (response.result) {
                    this.$scope.Risks = !_.isUndefined(response.data) ? response.data.Content : [];
                    this.$scope.Metadata = response.data.Metadata;
                }
                return this.$scope.Risks;
            });
        };

        this.$scope.$on("CHANGE_PAGE", (event: ng.IAngularEvent, page: number): void => {
            this.$scope.Ready = false;
            this.$scope.page = page;
            getRisks().then((response: any): void => {
                this.renderPage();
            });
        });

        const RiskStateMap: any = this.RiskBusinessLogic.buildRiskStatesMap(this.$scope.$root.customTerms.Recommendation);

        const defaultProps = [
            {
                key: "Level",
                friendlyName: this.$rootScope.t("Risk"),
                isActive: false,
                Type: this.ENUMS.RiskColumnTypes.ProjectRisk,
                sorted: true,
                QuestionId: "",
                resource: true,
            },
            {
                key: "ProjectName",
                friendlyName: this.$rootScope.t("ProjectName"),
                isActive: false,
                Type: this.ENUMS.RiskColumnTypes.ProjectName,
                sorted: true,
                QuestionId: "",
                resource: true,
            },
            {
                key: "Description",
                friendlyName: this.$rootScope.t("RiskDescription"),
                isActive: false,
                Type: this.ENUMS.RiskColumnTypes.Description,
                sorted: false,
                QuestionId: "",
                resource: true,
            },
            {
                key: "Recommendation",
                friendlyName: this.$rootScope.t("RiskRecommendation", { Recommendation: this.$scope.$root.customTerms.Recommendation }),
                isActive: false,
                Type: this.ENUMS.RiskColumnTypes.Recommendation,
                sorted: false,
                QuestionId: "",
                resource: true,
            },
            {
                key: "Status",
                friendlyName: this.$rootScope.t("RiskStatus"),
                isActive: false,
                Type: this.ENUMS.RiskColumnTypes.Status,
                sorted: false,
                QuestionId: "",
                resource: true,
            },
            {
                key: "Justification",
                friendlyName: this.$rootScope.t("Justification"),
                isActive: false,
                Type: this.ENUMS.RiskColumnTypes.QuestionJustification,
                sorted: false,
                QuestionId: "",
                resource: true,
            },
            {
                key: "Deadline",
                friendlyName: this.$rootScope.t("RiskDeadline"),
                isActive: false,
                Type: this.ENUMS.RiskColumnTypes.RiskDeadline,
                sorted: false,
                QuestionId: "",
                resource: this.Permissions.canShow("RiskDeadline"),
            },
            {
                key: "MitigatedDt",
                friendlyName: this.$rootScope.t("MitigatedDate"),
                isActive: false,
                Type: this.ENUMS.RiskColumnTypes.MitigatedDt,
                sorted: false,
                QuestionId: "",
                resource: true,
            },
            {
                key: "RiskOwner",
                friendlyName: this.$rootScope.t("RiskOwner"),
                isActive: false,
                Type: this.ENUMS.RiskColumnTypes.RiskOwner,
                sorted: false,
                QuestionId: "",
                resource: true,
            },
        ];

        const getTaggingSettings = (): any => {
            this.Settings.getTagging().then((response: any): void => {
                if (response.result) {
                    this.$scope.questionTaggingEnabled = response.data.QuestionTaggingEnabled;
                }
            });
        };

        const setupDefaultColumns = (): void => {
            this.$scope.report.columns = _.filter(defaultProps, (prop) => prop.resource === true);
        };

        const setupColumns = (): void => {
            if (this.$scope.report.view !== null) {
                let found = false;
                const reverseCopyArray = _.clone(this.$scope.report.view.Columns).reverse();
                _.each(reverseCopyArray, (column: any, columnIndex: number): void => {
                    const reportColumnLength = this.$scope.report.columns.length;

                    for (let i = 0; i < reportColumnLength; i++) {
                        const reportColumns = this.$scope.report.columns[i];

                        if (reportColumns.Type === column.Type) {
                            if (column.Type === this.ENUMS.RiskColumnTypes.Question) {
                                if (column.QuestionId === reportColumns.QuestionId) {
                                    reportColumns.Sorted = column.Sorted;
                                    reportColumns.isActive = true;
                                    found = true;
                                }
                            } else {
                                reportColumns.Sorted = column.Sorted;
                                reportColumns.isActive = true;
                                found = true;
                            }

                            if (found) {
                                const itemCopy = _.clone(this.$scope.report.columns[i]);
                                this.$scope.report.columns.splice(i, 1);
                                this.$scope.report.columns.unshift(itemCopy);
                                found = false;
                                break;
                            }
                        }
                    }
                });
            } else {
                for (const column of this.$scope.report.columns) {
                    if (column.Type <= 14) {
                        column.isActive = true;
                    }
                }
            }
        };

        const initialize = (): void => {
            const statusFilters: string[] = this.$state.params.status ? this.$state.params.status.split(",") : [];
            this.$scope.riskStates = this.RiskBusinessLogic.getRiskStateFilters();
            const initPromiseArray = [
                getTaggingSettings(),
                this.$scope.changeView(),
            ];
            this.$q.all(initPromiseArray).then((): void => {
                setupDefaultColumns();
                setupColumns();
                this.$scope.Ready = true;
            });
        };

        const getReportView = (): ng.IPromise<void> => {
            return this.$http.get(`/report/getrisks/${this.currentUser.Id}`).then((response) => {
                this.$scope.reportView = response.data;
            });
        };

        this.$scope.changeView = (templateId: number): any => {
            this.$scope.report = {
                template: "",
                projects: [],
                view: "",
                columns: [],
                filters: [],
                search: "",
            };

            this.$scope.reportView = null;

            return this.$q.all([getReportView(), getRisks()]).then(() => {
                this.renderPage();
            });
        };

        this.$scope.getTagsList = (filter: any, count: number): any =>
        this.Tags.getTags(filter, count).then((response: any) => response.data);

        const applyRiskStyle = (risk: any) => {
            switch (risk.Level) {
                case 1:
                    risk.riskClass = "fa fa-caret-down low";
                    risk.riskWord = this.translate("Low");
                    break;
                case 2:
                    risk.riskClass = "fa fa-caret-up medium";
                    risk.riskWord = this.translate("Medium");
                    break;
                case 3:
                    risk.riskClass = "fa fa-caret-up high";
                    risk.riskWord = this.translate("High");
                    break;
                case 4:
                    risk.riskClass = "fa fa-caret-up very-high";
                    risk.riskWord = this.translate("VeryHigh");
                    break;
                default:
                    risk.riskClass = "";
                    risk.riskWord = "";
            }
            return risk;
        };

        const setupProjectQuestions = (): void => {
            _.each(this.$scope.report.risks, (project: any, projectIndex: number): void => {
                project = applyRiskStyle(project);
            });
        };

        const setupRisks = (): void => {
            _.each(this.$scope.report.risks, (risk: any, riskIndex: number): void => {
                risk.Status = RiskStateMap[risk.State];
            });
        };

        this.$scope.setupViewFromReport = (): void => {
            setupProjectQuestions();
            setupRisks();
        };

        const transposeViewForSave = () => {
            const view = {
                Filters: [],
                Columns: [],
                TemplateId: this.$scope.report.template.Id,
            };

            const activeColumns = this.$filter("filter")(this.$scope.report.columns, {
                isActive: true,
            });

            _.each(activeColumns, (column: any): void => {
                const Column = {
                    Type: column.Type,
                    QuestionId: "",
                    Sorted: column.sorted,
                };

                if (column.Type === 8) {
                    Column.QuestionId = column.QuestionId;
                }

                view.Columns.push(Column);
            });

            _.each(this.$scope.report.filters, (filter: any): void => {
                const newResponse = [];
                filter.Responses.map((response: any) => {
                    if (response.Value !== "") {
                        newResponse.push(response);
                    }
                });
                filter.Responses = newResponse;
            });

            view.Filters = this.$scope.report.filters;

            return view;
        };

        this.$scope.saveReportView = (): void => {
            const params = transposeViewForSave();
            params.TemplateId = this.currentUser.Id;
            this.$http.post("/report/postrisks", params).then(
                () => {
                    this.$rootScope.APICache.clearData("Reports.getRisks");
                    const alertText = this.$rootScope.t("RisksViewCreated");
                    this.notificationService.alertSuccess(this.$rootScope.t("Success"), alertText);
                },
                () => {
                    const alertText = this.$rootScope.t("ErrorCreatingRisksView");
                    this.notificationService.alertError(this.$rootScope.t("Error"), alertText);
                },
            );
            this.RiskBusinessLogic.saveFiltersLocally(this.$scope.riskStates);
        };

        this.$scope.toggleFilter = (): void => {
            if (!this.$scope.filterOpen) {
                this.$scope.openFilter();
                this.$timeout(() => {
                    this.$scope.filterOverflow = true;
                }, 500);
            } else {
                this.$scope.filterOverflow = false;
                this.$scope.closeFilter();
            }
        };

        this.$scope.openFilter = (): void => {
            this.$scope.filterOpen = true;
        };

        this.$scope.closeFilter = (): void => {
            this.$scope.filterOpen = false;
        };

        this.$scope.clearFilters = (): void => {
            this.$scope.risksFilter.QuestionTags = [];
            this.$scope.riskStates.allStatesEnabled = true;
            this.$scope.setAllRiskStateFilterItems(true);
        };

        this.$scope.columnMoved = (index: number, column: any): void => {
            this.$scope.report.columns = this.OneTable.columnMoved(this.$scope.report.columns, index, column);
        };

        this.$scope.goToProject = (Id: number, Version: any): void => {
            this.$sessionStorage.goToAnalysis = true;
            this.$state.go("zen.app.pia.module.projects.assessment.module.questions", {
                Id,
                Version,
            });
        };

        this.$scope.generateCsv = (): void => {
            const exportUrl = "/report/exportRisks";
            const params: any = {
                Headers: this.getColumnsInUse(),
                State: this.getStatusFilterParams(),
            };
            this.Export.exportWithParams(exportUrl, ExportTypes.XLSX, this.translate("Risks"), params);
        };

        this.$scope.setAllRiskStateFilterItems = (enabled: boolean): void => {
            const riskStates = this.$scope.riskStates.filters;
            for (const state of riskStates) {
                state.Filtered = enabled;
            }
        };

        // VIEW INTERACTION
        this.$scope.toggleAllRiskFilterItems = (): void => {
            this.$scope.Ready = false;
            this.$scope.riskStates.allStatesEnabled = !this.$scope.riskStates.allStatesEnabled;
            this.$scope.setAllRiskStateFilterItems(this.$scope.riskStates.allStatesEnabled);
            this.$scope.page = 0;
            getRisks().then((response: any): void => {
                this.renderPage();
            });
        };

        this.$scope.toggleStatusFilterItem = (column): void => {
            this.$scope.Ready = false;
            let allChecked = true;
            const riskState = column.value;
            const riskStates = this.$scope.riskStates.filters;
            let lookupKey; // will keep track of 10, 20, or 30 .... etc
            let enabled; // will remember whether to enable or disable a risk
            for (const state of riskStates) {
                if (state.value === riskState) {
                    state.Filtered = !state.Filtered;
                    enabled = state.Filtered;
                    lookupKey = state.lookupKey;
                }
                allChecked = allChecked && state.Filtered;
            }
            this.$scope.riskStates.allStatesEnabled = allChecked;

            // do this here because if you do it in the loop above, allChecked checkbox will change after the risk filter is complete. Looks bad if so.
            for (const risk of this.$scope.report.risks) {
                if (risk.State === lookupKey) {
                    risk.isShowing = enabled;
                }
            }
            this.$scope.page = 0;
            getRisks().then((response: any): void => {
                this.renderPage();
            });
        };

        initialize();

    }

    private renderPage(): void {
        this.$scope.report.risks = _.clone(this.$scope.Risks);
        this.$scope.report.view = this.$scope.reportView ? _.clone(this.$scope.reportView) : null;
        this.$scope.report.filters = this.$scope.report.view ? (_.isArray(this.$scope.report.view.Filters) ? _.clone(this.$scope.report.view.Filters) : []) : [];
        this.$scope.setupViewFromReport();
        this.$scope.Ready = true;
    }

    private getStatusFilterParams(): number[] {
        const riskStates: IRiskStateFilter[] = this.$scope.riskStates.filters;
        const filteredStates: IRiskStateFilter[] = _.filter(riskStates, (state: IRiskStateFilter): boolean => state.Filtered);
        if (filteredStates.length) {
            return _.map(filteredStates, (state: IRiskStateFilter): number => state.lookupKey);
        }
        return [0];
    }

    private getColumnsInUse(): number[] {
        const filteredColumns: any[] = _.filter(this.$scope.report.columns, (column: any): boolean => column.isActive);
        return _.map(filteredColumns, (column: any): any => {
            return {id: column.Type, name: column.friendlyName};
        });
    }

    private getStateParams(params: IRiskGetPageParams): any {
        const newState: any = {};
        newState.page = params.page;
        _.each(params.filters, (filter: IRiskGetPageParamsFilter): void => {
            newState[filter.name] = _.isArray(filter.value) ? filter.value.join(",") : filter.value;
        });
        return newState;
    }

    private getMenuRoutes(): INavMenuItem[] {
        const menuGroup: INavMenuItem[] = [];
        // TODO: When Risk has menu items, populate here.
        return menuGroup;
    }

}
