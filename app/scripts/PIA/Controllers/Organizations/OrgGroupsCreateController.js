import { Regex } from "constants/regex.constant";
import { OrgUserDropdownTypes } from "enums/org-user-dropdown.enum";
import Utilities from "Utilities";

export default function OrganizationCreateCtrl(
    $scope,
    $state,
    OrgGroups,
    PRINCIPAL
) {
    $scope.dropdownType = OrgUserDropdownTypes.OrgGroupDefaultApprover;

    function init() {
        $scope.submitInProgress = false;
        $scope.assignedToMe = true;
        $scope.ParentId = $state.params["ParentId"] || "";
        $scope.CurrentUser = PRINCIPAL.getUser();
        $scope.Organization = {
            Name: "",
            PrivacyOfficer: $scope.CurrentUser.Id,
        };

        $scope.selectedUser = $scope.assignedToMe ? $scope.CurrentUser : "";
    }

    $scope.toggleApprover = function(value) {
        $scope.selectedUser = value ? $scope.CurrentUser : "";
        $scope.assignedToMe = value;
    };

    $scope.orgGroupSelected = function(selection) {
        $scope.selectedUser = selection;
    };

    $scope.createOrganization = function() {
        var organization = {
            ParentId: $scope.ParentId,
            Name: $scope.Organization.Name,
            PrivacyOfficerId: $scope.selectedUser.Id,
        };
        if (
            Utilities.matchRegex(
                organization.PrivacyOfficerId,
                Regex.GUID
            )
        ) {
            $scope.submitInProgress = true;
            OrgGroups.create(organization).then(function(response) {
                if (response.result) {
                    $state.go("zen.app.pia.module.admin.organizations.list", {
                        reload: true,
                    });
                }
                $scope.submitInProgress = false;
            });
        } else {
            $scope.Organization.PrivacyOfficer = "";
        }
    };

    init();
}

OrganizationCreateCtrl.$inject = [
    "$scope",
    "$state",
    "OrgGroups",
    "PRINCIPAL"
];
