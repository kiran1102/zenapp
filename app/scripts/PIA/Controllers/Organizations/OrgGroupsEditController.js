import { cloneDeep } from "lodash";
import { Regex } from "constants/regex.constant";
import { getOrgById } from "oneRedux/reducers/org.reducer";
import { OrgUserDropdownTypes } from "enums/org-user-dropdown.enum";
import Utilities from "Utilities";

export default function OrganizationEditCtrl(
    $scope,
    $state,
    OrgGroups,
    PRINCIPAL,
    store
) {
    $scope.dropdownType = OrgUserDropdownTypes.OrgGroupDefaultApprover;

    function init() {
        $scope.ready = false;
        $scope.submitInProgress = false;
        $scope.CurrentUser = PRINCIPAL.getUser();
        $scope.OrganizationId = $state.params["Id"] || "";
        $scope.Organization = $scope.OrganizationId ? cloneDeep(getOrgById($scope.OrganizationId)(store.getState())) : null;
        $scope.toggle = $scope.Organization.PrivacyOfficerId === $scope.CurrentUser.Id;
        $scope.defaultApprover = {
            Id: $scope.Organization.PrivacyOfficerId,
            FullName: $scope.Organization.PrivacyOfficerName,
        };
        $scope.showApprover = $scope.Organization.PrivacyOfficerId ? $scope.defaultApprover : "";
        $scope.selectedUser = $scope.Organization.PrivacyOfficerId === $scope.CurrentUser.Id ? $scope.CurrentUser : $scope.showApprover;
        $scope.ready = true;
    }

    $scope.toggleApprover = function(value) {
        $scope.selectedUser = value ? $scope.CurrentUser : "";
        $scope.toggle = value;
    };

    $scope.orgGroupSelected = function(selection) {
        $scope.selectedUser = selection;
    };

    $scope.updateOrganization = function() {
        var organization = {
            Id: $scope.Organization.Id,
            Name: $scope.Organization.Name,
            PrivacyOfficerId: $scope.selectedUser.Id,
        };
        if (
            Utilities.matchRegex(
                organization.PrivacyOfficerId,
                Regex.GUID
            )
        ) {
            $scope.submitInProgress = true;
            OrgGroups.update(organization).then(function(response) {
                if (response.result) {
                    $state.go("zen.app.pia.module.admin.organizations.list");
                }
                $scope.submitInProgress = false;
            });
        } else {
            $scope.selectedUser.Id = "";
        }
    };

    init();
}

OrganizationEditCtrl.$inject = [
    "$scope",
    "$state",
    "OrgGroups",
    "PRINCIPAL",
    "store"
];
