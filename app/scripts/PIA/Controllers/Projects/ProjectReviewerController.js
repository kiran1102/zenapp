import { find, filter } from "lodash";
import { Regex } from "constants/regex.constant";
import { IProtocolPacket } from "interfaces/protocol.interface";
import { AssessmentPermissions } from "constants/assessment.constants";

export default function ProjectReviewerCtrl($scope, $uibModalInstance, PRINCIPAL, Users, Assignments, project, Team) {

    $scope.AssignToMe = function () {
        $scope.Assignment.AssigneeId = $scope.currentUser.Id;
    };

    $scope.ClearAssignee = function () {
        $scope.Assignment.AssigneeId = "";
    };

    $scope.init = function () {
        $scope.Project = project;
        $scope.currentUser = PRINCIPAL.getUser();
        $scope.Team = Team.slice();

        $scope.currentAssignee = find($scope.Team, function(user) {
            return user.Id === project.ReviewerId;
        });

        $scope.TeamPicker = filter($scope.Team, function(user) {
            return user.Id !== $scope.currentUser.Id;
        });

        $scope.Assignment = {
            ProjectId: project.Id,
            ProjectVersion: project.Version,
            AssigneeId: "",
            Comment: "",
            Type: 20
        };

        if (_.isUndefined($scope.currentAssignee) || $scope.currentAssignee.Id !== $scope.currentUser.Id) {
            $scope.AssignToMe();
            $scope.ShowMeButton = true;
            $scope.ShowSomeoneElseButton = true;
        }
    };

    $scope.init();

    $scope.formatTeamLabel = function (model) {
        if (model) {
            if (Utilities.matchRegex(model, Regex.GUID)) {
                return Users.formatTeamLabel($scope.Team, model);
            }
            if (Utilities.matchRegex(model, Regex.EMAIL)) {
                return model;
            }
        }
        return "";
    };

    $scope.AssignProject = function() {
        $scope.submitInProgress = true;
        Assignments.assignProject($scope.Assignment).then(function(response) {
            if (!response.result) {
                $scope.done();
                return;
            }
            Assignments.get(response.data).then(function(res) {
                var closeData = {
                    result: res.result,
                    data: (res.data) ? res.data : null
                };
                $uibModalInstance.close(closeData);
                $scope.submitInProgress = false;
            });
        });
    };

    $scope.done = function () {
        $uibModalInstance.close({ result: false, data: null });
        $scope.submitInProgress = false;
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss("cancel");
    };
}

ProjectReviewerCtrl.$inject = ["$scope", "$uibModalInstance", "PRINCIPAL", "Users", "Assignments", "project", "Team"];
