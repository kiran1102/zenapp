import { AssessmentTypes } from "constants/assessment.constants";

export default function ProjectsWizardSelectCtrl($scope, $state, $sessionStorage, Templates, ENUMS, Permissions) {

    $scope.templateType = $state.includes(AssessmentTypes.DM.BaseRoute) ? ENUMS.TemplateTypes.Datamapping : ENUMS.TemplateTypes.Custom;
    $scope.routes = {
        projects: ($scope.templateType === ENUMS.TemplateTypes.Datamapping) ? "zen.app.pia.module.datamap.views.assessment.list" : "zen.app.pia.module.projects.list",
        templates: ($scope.templateType === ENUMS.TemplateTypes.Datamapping) ? "zen.app.pia.module.datamap.views.questionnaire-list" : "zen.app.pia.module.templates.list",
        createForm: ($scope.templateType === ENUMS.TemplateTypes.Datamapping) ? "zen.app.pia.module.datamap.views.assessment.wizard.project_info" : "zen.app.pia.module.projects.wizard.project_info",
    };

    $scope.title = "PIA Project Wizard";
    $scope.loadingTemplate = true;
    $scope.spacers = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
    $scope.templates = [];
    $scope.templateIdsList = [];
    $scope.loading = true;

    $scope.goToProjectInfo = function (_id, version) {
        if (_id) {
            $state.go($scope.routes.createForm, {
                mode: "new",
                id: _id,
                version: version
            });
        }
    };

    $scope.toggleDetails = function (template) {
        template.detailsOpen = !template.detailsOpen;
    };

    function init() {
        // Checking Auths
        $scope.canCreateTemplate = Permissions.canShow("TemplatesCreate");
        getTemplatesList();
        clearSession();
    };

    function getTemplatesList() {
        return Templates.publishedList($scope.templateType)
            .then(function (response) {
                if (response.result) {
                    filterPublishedList(response.data);
                    $scope.loading = false;
                } else {
                    filterPublishedList(response.data);
                    $scope.loading = false;
                }
            });
    };

    // Clear the saved project session object
    function clearSession() {
        if ($sessionStorage.wizard) {
            delete $sessionStorage.wizard;
        }
    };

    function filterPublishedList(list) {
        $scope.templates = _.filter(list, function (item) {
            if (!_.isNil(item) && item.HasParents === false) {
                return item;
            }
        });
    };

    init();
}

ProjectsWizardSelectCtrl.$inject = ["$scope", "$state", "$sessionStorage", "Templates", "ENUMS", "Permissions"];
