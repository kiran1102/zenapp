
export default function AttachmentController($scope, $uibModalInstance, FileUploader, Attachment, PRINCIPAL, AssessmentQuestionViewLogic, Project, Question) {

    var init = function () {
        var currentUser = PRINCIPAL.getUser();
        $scope.canAddQuestionAttachments = AssessmentQuestionViewLogic.getCanAddQuestionAttachments(currentUser, Project, Question);
        $scope.canDeleteQuestionAttachments = AssessmentQuestionViewLogic.getCanDeleteQuestionAttachments(currentUser, Project, Question);
        LoadData();
    };

    $scope.inputsShowing = false;
    $scope.errorShowing = false;
    $scope.attachmentLoading = false;
    $scope.fileNameError = false;
    $scope.Extension = "";
    $scope.Question = Question;
    $scope.attachmentList = [];
    $scope.attachmentFilter = {
        All: ""
    };

    $scope.updateAttachment = function (elem, attachmentFile) {
        $scope.inputElem = elem;
        if (!_.isUndefined(attachmentFile)) {
            if (attachmentFile.size < 67108864) {
                var reader = new FileReader();
                $scope.attachment = null;
                reader.readAsDataURL(attachmentFile);
                $scope.$apply();
                reader.onload = function (e) {
                    $scope.attachment = {
                        file: attachmentFile,
                        name: attachmentFile.name,
                        url: reader.result
                    };
                    $scope.fileName = attachmentFile.name;
                    $scope.Extension = attachmentFile.type;
                    if ($scope.fileName.length > 99) {
                        $scope.fileNameError = true;
                    }
                    $scope.$apply();
                };
                $scope.attachment = null;
                $scope.inputsShowing = true;
                $scope.errorShowing = false;
                $scope.fileNameError = false;
                $scope.$apply();
            }
            else {
                $scope.inputsShowing = false;
                $scope.errorShowing = true;
                $scope.fileNameError = false;
                $scope.$apply();
            }
        }

    };

    function LoadData() {
        Attachment.list(Question.Id).then(function (response) {
            if (response.result) {
                $scope.attachmentList = response.data;
                for (var i = 0; i < $scope.attachmentList.length; i++) {
                    $scope.attachmentList[i].FileSize = FileSize($scope.attachmentList[i].FileSize);
                }
            } else {
                $scope.attachmentList = [];
            }
        });
    };

    function FileSize(value) {
        if (value >= 1048576) { return round_number(value / 1048576, 2) + " MB"; }
        if (value >= 1024) { return round_number(value / 1024, 0) + " KB"; }
        return value + " B";
    };

    function round_number(num, dec) {
        return Math.round(num * Math.pow(10, dec)) / Math.pow(10, dec);
    };

    $scope.DeleteAttachment = function (attachmentId) {
        if (_.isString(attachmentId)) {
            Attachment.delete(attachmentId).then(function (response) {
                if (response.result) {
                    LoadData();
                } else {
                }
            });
        }
    };

    $scope.uploadAttachment = function () {
        $scope.attachmentLoading = true;
        var attachment = {
            "Name": $scope.fileName,
            "FileName": $scope.fileName,
            "Comments": $scope.fileComments,
            "Extension": $scope.Extension,
            "Location": "",
            "FileSize": $scope.attachment.file.size,
            "QuestionId": Question.Id,
            "FileData": $scope.attachment.url
        };
        Attachment.createAttachment(attachment).then(function (response) {
            $scope.attachmentLoading = false;
            if (response.result) {
                $scope.clearAttachment();
                LoadData();
            }
        });
    };

    $scope.clearAttachment = function () {
        $scope.inputsShowing = false;
        $scope.fileNameError = false;
        $scope.attachment = null;
        angular.element($scope.inputElem).val(null);
        $scope.fileName = null;
        $scope.fileComments = null;
    };

    $scope.closeModal = function () {
        $uibModalInstance.close({ result: $scope.attachmentList.length });
    };

    $scope.isLongValue = function (attachment) {
        var value = attachment.Comments;
        if (value) {
            var length = value.split("").length;
            if (length > 100) {
                addShortenedValue(attachment);
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    };

    $scope.validateFileName = function () {
        $scope.fileNameError = $scope.fileName.length > 99;
    };

    function addShortenedValue(attachment) {
        attachment.CommentsShort = attachment.Comments.substring(0, 76) + "...";
    };

    init();

}

AttachmentController.$inject = ["$scope", "$uibModalInstance", "FileUploader", "Attachment", "PRINCIPAL", "AssessmentQuestionViewLogic", "Project", "Question"];
