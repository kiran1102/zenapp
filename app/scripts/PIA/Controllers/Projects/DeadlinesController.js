
export default function DeadlinesModalCtrl($scope, $uibModalInstance, SettingStore, Projects, Project, Labels) {

    $scope.labels = Labels;

    $scope.IsDefault = false;
    $scope.validReminder = true;

    $scope.Project = _.cloneDeep(Project);

    $scope.Project.ReminderShow = ($scope.Project.ReminderDays !== null && $scope.Project.ReminderDays > 0) ? ($scope.Project.ReminderDays !== null) : false;

    SettingStore.fetchProjectSettings()
        .then(function (response) {
            if (response) {
                $scope.Settings = response;

            if (_.isUndefined($scope.Project.Deadline) || $scope.Project.Deadline === null || $scope.Project.Deadline === "") {
                $scope.Project.Deadline = new Date();
                $scope.Project.ReminderDays = $scope.Settings.Reminder.Value;
                $scope.Project.ReminderShow = $scope.Settings.Reminder.Show;
            }

            if (_.isUndefined($scope.Project.ReminderDays) || $scope.Project.ReminderDays < 0) {
                $scope.Project.ReminderDays = $scope.Settings.Reminder.Value;
                $scope.Project.ReminderShow = $scope.Settings.Reminder.Show;
            }

            $scope.$watch("Project.ReminderDays", function (newValue, oldValue) {
                $scope.IsDefault = newValue === $scope.Settings.Reminder.Value;
            });

        }
        $scope.validateReminder();

    });

    $scope.MinDate = new Date();

    $scope.updateDeadline = function(deadline) {
        $scope.Project.Deadline = deadline.jsdate;
        $scope.validateReminder();
    }

    $scope.validateReminder = function (date) {
        if ($scope.Project.ReminderShow) {
            var reminderDate = date ? new Date(date) : new Date($scope.Project.Deadline);
            reminderDate.setTime(reminderDate.getTime() - 1000 * 60 * 60 * 24 * $scope.Project.ReminderDays);
            var today = new Date();
            $scope.validReminder = (today.toLocaleDateString() !== reminderDate.toLocaleDateString()) && (reminderDate > today);
        } else {
            $scope.validReminder = true;
        }
    };

    $scope.Save = function () {
        $scope.isMakingRequest = true;
        var project = {
            Id: $scope.Project.Id,
            ProjectVersion: $scope.Project.Version,
            Name: $scope.Project.Name,
            Description: $scope.Project.Description,
            OrgGroupId: $scope.Project.OrgGroupId,
            OwnerId: $scope.Project.OwnerId,
            LeadId: $scope.Project.Sections[1].Assignment.AssigneeId,
            TemplateId: $scope.Project.TemplateId
        };

        if ($scope.Project.Deadline === "") {
            project.Deadline = null;
            $scope.Project.ReminderShow = false;
        }
        else {
            project.Deadline = $scope.Project.Deadline;
        }

        if ($scope.Project.ReminderShow) {
            project.ReminderDays = $scope.Project.ReminderDays;
        }
        else {
            project.ReminderDays = null;
        }

        Projects.update(project).then(function (response) {
            if (response.result) {
                var data = {
                    Deadline: project.Deadline,
                    ReminderDays: project.ReminderDays
                };
                $uibModalInstance.close({ result: true, data: data });
            }
            $scope.isMakingRequest = false;
        });
	};

    $scope.done = function () {
        $uibModalInstance.close({ status: false, data: null });
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss("cancel");
    };
}

DeadlinesModalCtrl.$inject = ["$scope", "$uibModalInstance", "SettingStore", "Projects", "Project", "Labels"];
