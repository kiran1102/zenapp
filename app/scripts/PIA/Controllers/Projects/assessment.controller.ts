declare var OneAlert: any;
declare var OneConfirm: any;
import { find, map, forEach, isArray, isEqual, findIndex, isUndefined, isString, isNil, cloneDeep, flatMap, filter } from "lodash";
import { getOrgNameById } from "oneRedux/reducers/org.reducer";
import { getUserById } from "oneRedux/reducers/user.reducer";
import { getRiskSettings, getProjectSettings } from "oneRedux/reducers/setting.reducer";
import { getRiskById } from "oneRedux/reducers/risk.reducer";
import { getModalData } from "oneRedux/reducers/modal.reducer";

import { RiskActionService } from "oneServices/actions/risk-action.service";
import { NotificationService } from "modules/shared/services/provider/notification.service";
import AssessmentBusinessLogic from "generalcomponent/Assessment/assessment-business-logic.service";
import { SettingActionService } from "settingsModule/services/actions/setting-action.service";
import Utilities from "Utilities";
import TreeMap from "TreeMap";

import * as ACTIONS from "generalcomponent/Assessment/assessment-actions.constants";
import { AssessmentPermissions, AssessmentStates, AssessmentWelcomeSectionText, AssessmentTypes, AssessmentThankYouStates } from "constants/assessment.constants";
import { Regex } from "constants/regex.constant";

import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import { IStore } from "interfaces/redux.interface";
import { IProtocolPacket } from "interfaces/protocol.interface";
import { IProjectSectionDirections, IViewInfoAddedModalData, INeedMoreInfoModalData, IAssessmentModalData, IPiaAssessment } from "interfaces/assessment.interface";
import { IOrganization } from "interfaces/organization.interface";
import { IUser } from "interfaces/user.interface";
import { IProject, IUpdateProjectPayload, ICreateProjectNotePayload } from "interfaces/project.interface";
import { INote } from "interfaces/note.interface";
import { ISection, IPhase } from "interfaces/section.interface";
import { IQuestion, IAssessmentQuestionAction } from "interfaces/question.interface";
import { IRisk, IAssessmentRiskModalData } from "interfaces/risk.interface";
import { IAssessmentQuestionModalData } from "interfaces/assessment-question-modal.interface";
import { IProjectDeadlinesModalData } from "interfaces/project-deadlines-modal.interface";
import { IProjectReviewerModalData } from "interfaces/project-reviewer-modal.interface";
import { ISendBackModalData } from "interfaces/send-back-modal.interface";
import { INextStepsModalData } from "interfaces/next-steps-modal.interface";
import { IGeneralModalData } from "interfaces/general-modal.interface";
import { IHelpDialogModal } from "interfaces/help-dialog-modal.interface";

import { RiskStates } from "enums/risk-states.enum";
import { TemplateTypes } from "enums/template-type.enum";
import { QuestionStates, QuestionTypes, QuestionResponseTypes } from "enums/assessment-question.enum";
import { NoteTypes } from "enums/notes.enum";
import { OrgUserTraversal } from "enums/org-user-dropdown.enum";

export default class AssessmentCtrl implements ng.IController {

    static $inject: string[] = [
        "$scope",
        "$rootScope",
        "$state",
        "$uibModal",
        "$q",
        "Permissions",
        "ENUMS",
        "Projects",
        "PRINCIPAL",
        "Users",
        "OrgGroups",
        "OrgGroupStore",
        "ModalService",
        "ProjectQuestions",
        "$location",
        "Labels",
        "Answer",
        "Assessment",
        "UserStore",
        "Sections",
        "Questions",
        "store",
        "RiskAction",
        "AssessmentBusinessLogic",
        "SettingAction",
        "NotificationService",
    ];

    constructor(
        private readonly $scope: any,
        private readonly $rootScope: IExtendedRootScopeService,
        private readonly $state: ng.ui.IStateService,
        private readonly $uibModal: any,
        private readonly $q: ng.IQService,
        private readonly Permissions: any,
        private readonly ENUMS: any,
        private readonly Projects: any,
        private readonly PRINCIPAL: any,
        private readonly Users: any,
        private readonly OrgGroups: any,
        private readonly OrgGroupStore: any,
        private readonly ModalService: any,
        private readonly ProjectQuestions: any,
        private readonly $location: ng.ILocationService,
        private readonly Labels: any,
        private readonly Answer: any,
        private readonly Assessment: any,
        private readonly UserStore: any,
        private readonly Sections: any,
        private readonly Questions: any,
        private readonly store: IStore,
        private readonly RiskAction: RiskActionService,
        private readonly AssessmentBusinessLogic: AssessmentBusinessLogic,
        private readonly SettingAction: SettingActionService,
        private notificationService: NotificationService,
    ) {
        $scope.Projects = Projects;
        Assessment.setCtrlInstance($scope);
        Questions.setCtrlInstance($scope);
        $scope.loading = true;
        $scope.loadingMessage = $scope.$root.customTerms.Projects;
        $scope.SelectedIndex = 0;
        $scope.NeedInfoCount = 0;
        $scope.ShowNextSection = null;
        $scope.riskReminderDays = 0;
        $scope.activeSection = "welcome";
        $scope.showAnalysis = false;
        $scope.riskMinDeadline = new Date();
        $scope.allowVersioning = false;
        $scope.slideDirection = null;
        $scope.previousIndex = 0;
        $scope.isSelectedSectionInit = false;
        $scope.TotalSections = 0;
        $scope.requestInProgress = false;
        $scope.responseIsUpdating = false;
        $scope.CurrentUser = PRINCIPAL.getUser();
        $scope.usersList = UserStore.getUserList();
        $scope.Dictionary = {
            Sections: {},
            Questions: {},
        };
        $scope.ProjectProgress = {
            value: 0,
        };
        $scope.UndecidedRemaining = 0;
        $scope.isDataMapping = $state.includes(AssessmentTypes.DM.BaseRoute);
        $scope.templateType = $scope.isDataMapping ? TemplateTypes.Datamapping : TemplateTypes.Custom;
        $scope.routes = {
            projects: ($scope.isDataMapping) ? "zen.app.pia.module.datamap.views.assessment-old.list" : "zen.app.pia.module.projects.list",
            createForm: ($scope.isDataMapping) ? "zen.app.pia.module.datamap.views.assessment.wizard.project_info" : "zen.app.pia.module.projects.wizard.project_info",
            selfServiceList: "zen.app.pia.module.selfservice.list",
        };
        $scope.labels = Labels.getLabels("project", $scope.templateType);
        $scope.projectLevelApproval = false;
        $scope.approve = {
            status: false,
        };
        $scope.SkipActive = "";
        $scope.ProjectStates = AssessmentStates;
        $scope.RoleNames = ENUMS.RoleNames;
        $scope.QuestionTypes = QuestionTypes;
        $scope.ResponseTypes = QuestionResponseTypes;
        $scope.QuestionStates = QuestionStates;
        $scope.showingQuestions = false;
        $scope.projectPhases = [];
        $scope.singlePhase = true;
        $scope.permissions = {
            canViewProjects: Permissions.canShow("ProjectsList"),
            canDuplicateProject: Permissions.canShow("ProjectDuplicate"),
            selfServiceExecute: Permissions.canShow("SelfServiceExecute"),
            notesApprovalView: Permissions.canShow("NotesApprovalView"),
            projectsShare: Permissions.canShow("ProjectsShare"),
            projectVersioning: Permissions.canShow("ProjectVersioning"),
            noAdditionalChanges: Permissions.canShow("ProjectVersioningNoAdditionalChanges"),
            projectsReassignApprover: Permissions.canShow("ProjectsReassignApprover"),
            projectsReassignProjectRespondent: Permissions.canShow("ProjectsReassignProjectRespondent"),
            projectsReassignSectionRespondent: Permissions.canShow("ProjectsReassignSectionRespondent"),
        };

        const getStateParams = (param: string): string => {
            return $state.params[param] ? $state.params[param] : "";
        };

        $scope.goToSection = (index: number, section: ISection): void => {
            $scope.currentSection = section;
            Assessment.goToSection(index, section);
            Assessment.goToTopOfSection();
        };

        $scope.goToQuestion = (sectionId: string, questionId: string): void => {
            Assessment.goToQuestion(sectionId, questionId);
        };

        $scope.getProjectProgress = (id: string, version: number, questionId: string): void => {
            Projects.getProgress(id, version).then((response: IProtocolPacket): void => {
                const totalOutstanding: number = Projects.addMissingJustifications($scope.Project, response.data.Outstanding);
                $scope.ProjectProgress = { value: Projects.computeProjectProgress(response.data.Answered, totalOutstanding) };

                if (questionId) {
                    const updatedSections: any = Sections.getUpdatedSectionsOnQuestionNoChange($scope.Project.Sections, questionId);
                    $scope.Project.Sections = map(updatedSections, (section: ISection): any => {
                        section.canShowNoAdditionalChangesButton = canShowNoAdditionalChangesButton(section);
                        return section;
                    });
                }
                Questions.getRemainingQuestionsRefactored();
            });
        };

        $scope.GetRemainingQuestions = (): void => {
            Questions.getRemainingQuestionsRefactored();
        };

        $scope.generateNote = (val: string, oldVal: string, type: string): void => {
            if (!isUndefined(oldVal) && !isUndefined(val) && (val !== oldVal)) {
                const params: ICreateProjectNotePayload = {
                    ProjectId: $scope.Project.Id,
                    ProjectVersion: $scope.Project.Version,
                    Value: val,
                    Type: type,
                };
                this.Projects.createNote(params);
            }
        };

        $scope.GetProject = (id: string, version: number): ng.IPromise<void> => {
            if (this.Permissions.canShow("Assessments")) {
                return;
            } else {
                return Projects.read(id, version).then((response: IProtocolPacket): void => {
                    if (response.result) {
                        $scope.Project = response.data;

                        $scope.Project = $scope.SetStateLabel(response.data);
                        forEach($scope.Project.Sections, (section: any): void => { section = $scope.SetStateLabel(section); });

                        $scope.projectPhases = Projects.divideProjectByLocked($scope.Project);
                        if ($scope.projectPhases.length === 1) { $scope.singlePhase = true; }
                        $scope.TotalSections = Sections.setTotalSections($scope.projectPhases);

                        forEach($scope.projectPhases, (phase: IPhase): void => {
                            if (phase.Sections && !phase.Sections[0].Questions[0].Content) {
                                phase.Sections[0].Questions[0].Content = AssessmentWelcomeSectionText;
                            }
                        });

                        if (!isUndefined($scope.Project.Sections) && isArray($scope.Project.Sections) && $scope.Project.Sections.length > 0) {
                            Answer.getSections($scope, $scope.Project.Sections, $scope.Dictionary);
                        }

                        $scope.pageSlide = {
                            internalNotes: "",
                            approvalNotes: "",
                            checked: true,
                        };

                        if (isArray($scope.Project.Notes)) {
                            forEach($scope.Project.Notes, (note: INote): void => {
                                if (note.Type === NoteTypes.Internal) {
                                    $scope.pageSlide.internalNotes = note.Value;
                                } else {
                                    $scope.pageSlide.approvalNotes = note.Value;
                                }
                            });
                        }

                        $scope.$watch("pageSlide.internalNotes", (value: string, oldValue: string): void => {
                            $scope.generateNote(value, oldValue, NoteTypes.Internal);
                        });

                        $scope.$watch("pageSlide.approvalNotes", (value: string, oldValue: string): void => {
                            $scope.generateNote(value, oldValue, NoteTypes.Approval);
                        });

                        if ($scope.Project.StateDesc === AssessmentStates.InProgress ||
                            $scope.Project.StateDesc === AssessmentStates.InfoNeeded) {
                            $scope.getProjectProgress(id, version);
                        }
                    } else {
                        $scope.projectNotAvailable = true;
                        if (!$scope.permissions.canViewProjects) OneAlert("", $scope.labels.noLongerAvailable);
                        if ($scope.permissions.canViewProjects) $state.go($scope.routes.projects);
                    }
                });
            }
        };

        $scope.SetStateLabel = (statScope: any): any => {
            Labels.setStateLabel(AssessmentStates, statScope);
            return statScope;
        };

        function canShowNoAdditionalChangesButton(section: ISection): boolean {
            const isNewSection: boolean = Sections.getQuestionsWithoutChanges(section).length === 0;
            const isWelcomeSection: boolean = section.isWelcomeSection;
            const stateDescIsNotInProgress: boolean = $scope.Project.StateDesc !== AssessmentStates.InProgress;
            if (isWelcomeSection || !$scope.permissions.noAdditionalChanges || stateDescIsNotInProgress || $scope.Project.Version <= 1 || !$scope.Project.IsLatestVersion || isNewSection) {
                return false;
            }
            return Permissions.canShow("ProjectsFullAccess") || Projects.isProjectReviewerOrLead($scope.Project) || Sections.currentUserIsAssignedToSection(section);
        }

        $scope.formatTeamLabel = (model: string): string => {
            if (model && Utilities.matchRegex(model, Regex.GUID)) {
                return Users.formatTeamLabel($scope.Team, model);
            }
            return "";
        };

        const approveModal = (): void => {
            const modalData: IHelpDialogModal = {
                modalTitle: this.$rootScope.t("Approved"),
                content: $scope.labels.approvedMessage,
            };
            this.ModalService.setModalData(modalData);
            this.ModalService.openModal("helpDialogModal");
        };

        const suspendModal = (): void => {
            const url = "SuspendModal.html";
            const modalInstance: any = $uibModal.open({
                templateUrl: url,
                controller: "SuspendModalController",
                scope: $scope,
                backdrop: "static",
                keyboard: false,
                resolve: {
                    Project: (): any => $scope.Project,
                    EnableRiskAnalysis: (): boolean => $scope.EnableRiskAnalysis && $scope.Project.HasRisks,
                },
            });

            modalInstance.result.then((response: any): undefined | void => {
                if (!response.result) return;

                if (response.data === 1) {
                    approveModal();
                } else if (response.data === 2) {
                    $scope.init();
                }
            });

        };

        $scope.init = (): ng.IPromise<undefined | void> => {
            $scope.loading = true;
            $scope.projectId = getStateParams("Id");
            $scope.projectVersion = getStateParams("Version");
            $scope.Project = null;

            if ($scope.projectId === "") {
                $state.go($scope.routes.projects);
                return;
            }

            $scope.questionEvents = {
                textAreaChange: Questions.setQuestionResponseAndSave,
                textAreaSave: Questions.saveQuestionResponse,
                yesNoChange: Questions.setQuestionResponseAndSave,
                multiChoiceSelected: Questions.isQuestionSelected,
                multiChoiceChange: Questions.multiChoiceToggle,
                multiSelectChange: Questions.multiSelectToggle,
                dateChange: Questions.setQuestionResponseAndSave,
                toggleNotSure: Questions.toggleNotSure,
                toggleNotApplicable: Questions.toggleNotApplicable,
                otherOptionSet: Questions.setOtherOption,
                otherOptionSave: Questions.saveQuestionResponse,
                multiChoiceGroupSelected: Questions.dmIsQuestionSelected,
                multiSelectGroupChange: Questions.dmSaveQuestionResponseRefactored,
                justificationSave: Questions.saveJustification,
                inventorySelect: Questions.questionInventorySelect,
            };

            const initPromiseArray: any[] = [
                $scope.GetProject($scope.projectId, $scope.projectVersion),
                Assessment.getApprovalLevel(),
                Assessment.getHeatmapSettings(),
            ];

            $scope.Team = filter($scope.usersList, (user: IUser): boolean => user.IsActive);

            OrgGroups.rootTree().then((res: any): void => {
                OrgGroupStore.getOrgTree().then((organizations: IOrganization[]): void => {
                    $scope.OrganizationTree = organizations;
                    if (res.data) {
                        $scope.RootOrganizationTree = res.data;
                        $scope.TreeMap = new TreeMap("Id", "ParentId", "Children", $scope.RootOrganizationTree[0]);
                    } else {
                        $scope.RootOrganizationTree = [];
                    }
                });
            });

            return $q.all(initPromiseArray).then((): undefined | void => {
                $scope.loading = false;
                if (!$scope.Project) return;
                Questions.getRemainingQuestionsRefactored();

                // Show if CurrentUser has Read Only Access
                $scope.isReadOnly = !AssessmentBusinessLogic.isApproverOrHasFullAccess($scope.Project) && $scope.Project.StateDesc !== AssessmentStates.InProgress && !this.Permissions.canShow("TemplatesV2");
                if ($scope.isReadOnly) {
                    const modalData: IHelpDialogModal = {
                        modalTitle: $rootScope.t("ReadOnly"),
                        content: $scope.labels.readOnlyMessage,
                    };
                    this.ModalService.setModalData(modalData);
                    this.ModalService.openModal("helpDialogModal");
                }

                if ($scope.projectPhases) {
                    const newSections: ISection[] = [];
                    let count = 0;
                    forEach($scope.projectPhases, (phase: IPhase): void => {
                        forEach(phase.Sections, (section: ISection): void => {
                            if (section.isWelcomeSection) {
                                $scope.SelectedIndex = count;
                                $scope.currentSection = section;
                            }
                            section.canShowNoAdditionalChangesButton = canShowNoAdditionalChangesButton(section);
                            newSections.push(section);
                            count++;
                        });
                    });
                    $scope.Project.Sections = newSections;
                }
                $scope.headerOptions = Assessment.getHeaderOptions();
            });
        };

        $scope.checkSidebarActive = (section: ISection): boolean => {
            return $scope.SelectedIndex === section.totalIndex;
        };

        $scope.questionsOpen = (section: any): any => isEqual($scope.activeSection, section) && section.questionToggle;

        $scope.indexForward = (): void => {
            $scope.slideDirection = "right";
            $scope.SelectedIndex = Sections.phaseIndexForward($scope.TotalSections, $scope.SelectedIndex);
            const section: ISection = ($scope.Project.Sections[$scope.SelectedIndex]);

            if (section.IsSkipped) $scope.indexForward();
            $scope.goToSection($scope.SelectedIndex, section);
        };

        $scope.indexBackwards = (): void => {
            $scope.slideDirection = "left";
            $scope.SelectedIndex = Sections.phaseIndexBackward($scope.SelectedIndex);
            const section: ISection = ($scope.Project.Sections[$scope.SelectedIndex]);

            if (section.IsSkipped) $scope.indexBackwards();
            $scope.goToSection($scope.SelectedIndex, section);
        };

        $scope.toggleOpenQuestions = (id: string): void => {
            const sections: ISection[] = $scope.Project.Sections;
            const index: number = findIndex(sections, ["Id", id]);
            sections[index].questionToggle = !sections[index].questionToggle;
            Assessment.setActiveSection(sections[index]);
        };

        $scope.toggleQuestionAcceptance = (sectionId: string, question: IQuestion): void => {
            Questions.toggleQuestionAcceptance(sectionId, question);
        };

        $scope.OrgSaveProject = (org: IOrganization): void => {
            $scope.Project.OrgGroupId = org.id ? org.id : $scope.Project.OrgGroupId;
            $scope.Project.OrgGroup = org.name ? org.name : $scope.Project.OrgGroup;

            if (isString($scope.Project.OrgGroupId)) {
                $scope.SaveProject();
            }
        };

        $scope.SaveProject = (): void => {
            const project: IUpdateProjectPayload = {
                Id: $scope.Project.Id,
                ProjectVersion: $scope.Project.Version,
                Name: $scope.Project.Name,
                Description: $scope.Project.Description,
                OrgGroupId: $scope.Project.OrgGroupId,
                OwnerId: $scope.Project.OwnerId,
                LeadId: $scope.Project.LeadId,
                TemplateId: $scope.Project.TemplateId,
                Deadline: $scope.Project.Deadline,
            };

            $scope.Project.OrgGroup = getOrgNameById($scope.Project.OrgGroupId)(this.store.getState());
            $scope.Project.Lead = $scope.formatTeamLabel(project.LeadId);
            $scope.Project.Owner = $scope.formatTeamLabel(project.OwnerId);

            if (Utilities.matchRegex(project.OrgGroupId, Regex.GUID)) {
                if (Utilities.matchRegex(project.LeadId, Regex.GUID)) {
                    Projects.update(project);
                } else {
                    $scope.wizard.Project.LeadId = "";
                    notificationService.alert($rootScope.t("Warning"), $rootScope.t("PleaseSelectValidProjectRespondent"));
                }
            } else {
                $scope.wizard.Project.OrgGroupId = "";
                notificationService.alert($rootScope.t("Warning"), $rootScope.t("PleaseSelectValidOrganizationGroup"));
            }
        };

        const successModal = (): void | undefined => {
            const modalData: IHelpDialogModal = {
                modalTitle: $rootScope.t("Success"),
                content: $rootScope.t("ProjectSubmittedApproversNotified"),
                hideClose: !$scope.permissions.canViewProjects,
            };

            if ($scope.Project.ReviewerId) {
                const Reviewer: IUser = getUserById($scope.Project.ReviewerId)(this.store.getState()) || null;
                if (!Reviewer) return;
                modalData.content = $scope.labels.successModalMessage($scope.Project.Reviewer, Reviewer.Email, $scope.Project.Name);
            }
            this.ModalService.setModalData(modalData);
            this.ModalService.openModal("helpDialogModal");
            if ($scope.permissions.canViewProjects) $state.go($scope.routes.projects);
        };

        $scope.SubmitAssessment = (): undefined | void => {
            OneConfirm("", $rootScope.t("AreYouSureSubmitAssessment")).then((result: boolean): undefined | void => {
                if (!result) return;

                $scope.requestInProgress = true;
                Projects.submitProject($scope.Project.Id, $scope.Project.Version).then((response: IProtocolPacket): undefined | void => {
                    if (response.result) {
                        if (!$scope.permissions.canViewProjects) {
                            $state.go("zen.app.pia.module.thanks", {
                                options: {
                                    state: AssessmentThankYouStates.Complete,
                                    projectName: $scope.Project.Name,
                                    reviewerId: $scope.Project.ReviewerId,
                                    canViewProjects: $scope.permissions.canViewProjects,
                                },
                                projectId: $scope.Project.Id,
                                templateType: $scope.templateType,
                                projectVersion: $scope.Project.Version,
                            });
                            return;
                        } else {
                            successModal();
                        }
                    } else {
                        $scope.requestInProgress = false;
                    }
                });
            });
        };

        $scope.CompleteAssessment = (): void => {
            OneConfirm("", $rootScope.t("SureToCompleteThisReview")).then((result: boolean): undefined | void => {
                if (!result) return;

                Projects.completeProject({ Id: $scope.Project.Id }).then((response: IProtocolPacket): void => {
                    if (response.result && $scope.permissions.canViewProjects) $state.go($scope.routes.projects);
                });
            });
        };

        $scope.cancel = (): void => {
            if ($scope.permissions.canViewProjects) $state.go($scope.routes.projects);
        };

        $scope.AnalysisModal = (): void => {
            const data: Object = {
                Project: $scope.Project,
            };
            const modalInstance: any = ModalService.renderModal(data, "AnalysisModalCtrl", "AnalysisModal.html", "analysisModal");
            const destroyAnalysisModalCb: any = $scope.$root.$on("analysisModalCb", (event: ng.IAngularEvent, args: any): void => {
                if (args.cbInitiated) {
                    $scope.transitionToRiskSummary();
                }
                destroyAnalysisModalCb();
            });
        };

        $scope.NextStepsModal = (): void => {
            const data: INextStepsModalData = {
                Project: $scope.Project,
                EnableRiskAnalysis: $scope.EnableRiskAnalysis && this.AssessmentBusinessLogic.flaggedRiskExists($scope.Project),
            };
            const modalInstance: any = ModalService.renderModal(data, "NextStepsController", "NextStepsModal.html", "nextStepsModal");

            const destroyNextStepsModalCb: any = $scope.$root.$on("nextStepsModalCb", (e: any, args: any): void => {
                if (args.response) {
                    if (args.response.data === 1 && $scope.EnableRiskAnalysis && this.AssessmentBusinessLogic.flaggedRiskExists($scope.Project)) {
                        $scope.AnalysisModal();
                    }
                    if (args.response.data === 1 && (!$scope.EnableRiskAnalysis || !this.AssessmentBusinessLogic.flaggedRiskExists($scope.Project))) {
                        $scope.Project.IsLinked = false;
                        this.store.dispatch({ type: "UNHANDLED_ACTION: assessment-refactored controller" }); // done here to trigger the footer change
                    }
                }
                destroyNextStepsModalCb();
            });
        };

        $scope.ApproveAssessment = (): void => {
            OneConfirm("", $rootScope.t("AreYouSureYouWantToApproveThisAssessment")).then((result: boolean): undefined | void => {
                if (!result) return;
                // TODO: Check for (if defaultApprover off && !projectModel.Assignment) ==> Insert CurrentUser as the approver
                $scope.requestInProgress = true;
                Projects.completeProject({ Id: $scope.Project.Id, ProjectVersion: $scope.Project.Version }).then((response: IProtocolPacket): undefined | void => {
                    if (!response.result) {
                        $scope.requestInProgress = false;
                        return;
                    }
                    if ($scope.permissions.canViewProjects) $state.go($scope.routes.projects);
                    approveModal();
                });
            });
        };

        const recommendationModal = (): void => {
            const modalData: IHelpDialogModal = {
                modalTitle: $rootScope.t("RecommendationSent", { Recommendation: $scope.$root.customTerms.Recommendation }),
                content: $rootScope.t("RecommendationsSentToRespondent", { Recommendations: $scope.$root.customTerms.Recommendations }),
            };
            this.ModalService.setModalData(modalData);
            this.ModalService.openModal("helpDialogModal");
        };

        $scope.sendBackModal = (): void => {
            const data: ISendBackModalData = {
                Project: $scope.Project,
                Labels,
                TemplateType: $scope.templateType,
            };
            const modalInstance: any = ModalService.renderModal(data, "SendBackCtrl", "SendBackModal.html", "sendBackModal");

            const destroySendBackModalCb: any = $scope.$root.$on("sendBackModalCb", (event: ng.IAngularEvent, args: any): void => {
                if (args.response && $scope.permissions.canViewProjects) $state.go($scope.routes.projects);
                destroySendBackModalCb();
            });
        };

        $scope.OpenProjectAssignmentsModal = (sectionId: string, scopeContext: any, section: ISection): void => {
            if (($scope.permissions.projectsReassignProjectRespondent && $scope.permissions.projectsReassignSectionRespondent) ||
                Projects.isProjectReviewerOrLead($scope.Project) ||
                Sections.currentUserIsAssignedToSection(section)
            ) {
                $scope.Project.assignText = $rootScope.t("ReAssign");
                const data: any = {
                    project: $scope.Project,
                    Team: $scope.Team,
                    SectionId: sectionId,
                    ScopeContext: scopeContext,
                    Section: section,
                    SectionAssignment: {},
                    Initial: false,
                };

                ModalService.renderModal(data, "ProjectAssignmentsCtrl", "AssignmentsModal.html", "projectAssignmentModal");

                const destroyProjectAssignmentModalCb: any = $scope.$root.$on("projectAssignmentModalCb", (event: ng.IAngularEvent, args: any): void => {
                    const assignmentType: string = args.response.assignmentScope;

                    if ($scope.Project) {
                        if (this.AssessmentBusinessLogic.isApproverOrHasFullAccess($scope.Project)) {
                            $scope.init();
                        } else if (assignmentType === "project" && !this.AssessmentBusinessLogic.isApproverOrHasFullAccess($scope.Project)) {
                            $state.go($scope.routes.projects);
                        } else if (assignmentType === "section") {
                            // deepCloning $scope.Project prevents negatively affecting lower-level components
                            const project: IPiaAssessment = cloneDeep($scope.Project);
                            // create sections array excluding recently updated section
                            project.Sections = filter($scope.Project.Sections, (projectSection) => projectSection.Id !== sectionId);
                            const { assignedSectionsLength } = this.AssessmentBusinessLogic.getSections(project);
                            // refresh the page if current user has other assigned sections, or send to Assessment List
                            assignedSectionsLength > 0 ? $scope.init() : $state.go($scope.routes.projects);
                        }
                    }
                });
            }
        };

        $scope.OpenProjectReviewerModal = (): undefined | void => {
            if (!$scope.permissions.projectsReassignApprover) return;

            OrgGroups.getOrgUsersWithPermission(
                $scope.Project.OrgGroupId,
                OrgUserTraversal.Up,
                false,
                [AssessmentPermissions.AssessmentCanBeApprover],
            ).then((res: IProtocolPacket): void => {
                if (res.result) {
                    const data: IProjectReviewerModalData = {
                        project: $scope.Project,
                        Team: res.data,
                    };
                    ModalService.renderModal(data, "ProjectReviewerCtrl", "ReviewerModal.html", "projectReviewerModal");
                }
            });

            const destroyProjectReviewerModalCb: any = $scope.$root.$on("projectReviewerModalCb", (event: ng.IAngularEvent, args: any): void => {
                $scope.init();
            });
        };

        $scope.OpenCommentsModal = (question: IQuestion): void => {
            const data: IAssessmentQuestionModalData = {
                Project: $scope.Project,
                Question: question,
            };

            const modalInstance: any = ModalService.renderModal(data, "QuestionCommentsCtrl", "CommentsModal.html", "commentsModal");

            const destroyCommentsModalCb: any = $scope.$root.$on("commentsModalCb", (event: ng.IAngularEvent, args: any): void => {
                question.HasComments = args.response.result > 0;
                destroyCommentsModalCb();
            });
        };

        $scope.openNeedInfoModal = (sectionId: string, question: IQuestion, isFromViewInfoAddedModal: boolean): void => {
            const callback: any = (): undefined | void => {
                const sectionIndex: number = findIndex($scope.Project.Sections, ["Id", sectionId]);
                Questions.getRemainingQuestionsRefactored();
                $scope.Project.Sections[sectionIndex].FilteredQuestions = Answer.setFilters($scope, $scope.Project.Sections[sectionIndex]);
            };
            const modalData: INeedMoreInfoModalData = {
                currentUser: $scope.CurrentUser,
                project: $scope.Project,
                question,
                callback,
                isFromViewInfoAddedModal,
            };
            this.ModalService.setModalData(modalData);
            this.ModalService.openModal("needsMoreInfoModal");
        };

        $scope.openAddInfoModal = (sectionId: string, question: IQuestion): void => {
            const callback: any = (): undefined | void => {
                question.State = QuestionStates.InfoAdded;
                const sectionIndex: number = findIndex($scope.Project.Sections, ["Id", sectionId]);
                Questions.getRemainingQuestionsRefactored();
                $scope.Project.Sections[sectionIndex].FilteredQuestions = Answer.setFilters($scope, $scope.Project.Sections[sectionIndex]);
            };
            const modalData: IAssessmentModalData = {
                currentUser: $scope.CurrentUser,
                project: $scope.Project,
                question,
                callback,
            };
            this.ModalService.setModalData(modalData);
            this.ModalService.openModal("addInfoModal");
        };

        $scope.openViewInfoAddedModal = (sectionId: string, question: IQuestion): void => {
            const callback: any = (...args): undefined | void => {
                if (args[0].selectedQuestionState === QuestionStates.Accepted) {
                    $scope.toggleQuestionAcceptance(sectionId, question);
                } else if (args[0].isFromViewInfoAddedModal) {
                    $scope.openNeedInfoModal(sectionId, question, args[0].isFromViewInfoAddedModal);
                }
            };
            const modalData: IViewInfoAddedModalData = {
                reviewerName: $scope.Project.Reviewer,
                currentUser: $scope.CurrentUser,
                project: $scope.Project,
                question,
                callback,
            };
            this.ModalService.setModalData(modalData);
            this.ModalService.openModal("viewInfoAddedModal");
        };

        $scope.openRiskModal = (sectionId: string, question: any): void => {
            const section: any = find($scope.Project.Sections, (projectSection) => projectSection.Id === sectionId);
            const riskId: string | null = question.Risk ? question.Risk.Id : null;
            const callback = () => {
                const selectedRiskId: string = getModalData(this.store.getState()).selectedModelId;
                const riskModelCopy: any = { ...getRiskById(selectedRiskId)(this.store.getState()) };

                riskModelCopy.State = riskModelCopy.Recommendation ? RiskStates.Analysis : RiskStates.Identified;
                question.Risk = riskModelCopy;
                section.FilteredQuestions = Answer.setFilters($scope, section);
                Questions.getRemainingQuestionsRefactored();

                // riskModelCopy.QuestionId = question.Id;  TODO: switch back once Ruben updates endpoint
                riskModelCopy.ProjectQuestionGuid = question.Id;
            };

            const isCompleted: boolean = $scope.Project.StateDesc === AssessmentStates.Completed;
            const modalData: IAssessmentRiskModalData = {
                reminderDays: $scope.riskReminderDays,
                projectModel: cloneDeep($scope.Project), // cloneDeep because redux will complain. until all of assessment is migrated, this has to be done
                section: cloneDeep(section), // cloneDeep because redux will complain. until all of assessment is migrated, this has to be done
                callback,
            };

            if (riskId) {
                this.RiskAction.addRisk(question.Risk);
                this.RiskAction.addTempRisk(question.Risk.Id);
                this.RiskAction.openEditRiskModal(riskId, modalData);
            } else {
                this.RiskAction.openCreateNewRiskModal(question.Id, modalData);
            }
        };

        $scope.OpenDeadlinesModal = (): void => {
            const data: IProjectDeadlinesModalData = {
                Project: $scope.Project,
                Labels: $scope.labels,
            };
            const modalInstance: any = ModalService.renderModal(data, "DeadlinesModalCtrl", "DeadlinesModal.html", "deadlinesModal");

            const destroyDeadlinesModalCb: any = $scope.$root.$on("deadlinesModalCb", (event: ng.IAngularEvent, args: any): void => {
                $scope.init();
            });
        };

        $scope.OpenHistoryModal = (question: IQuestion): void => {
            const data: IAssessmentQuestionModalData = {
                Project: $scope.Project,
                Question: question,
            };
            const modalInstance: any = ModalService.renderModal(data, "QuestionHistoryCtrl", "QuestionHistoryModal.html", "historyModal", "lg");
        };

        $scope.OpenAttachmentModal = (question: IQuestion): void => {
            const data: IAssessmentQuestionModalData = {
                Project: $scope.Project,
                Question: question,
            };
            const modalInstance: any = ModalService.renderModal(data, "AttachmentController", "AttachmentModal.html", "attachmentModal", "lg");

            const destroyAttachmentModalCb: any = $scope.$root.$on("attachmentModalCb", (event: ng.IAngularEvent, args: any): void => {
                question.HasAttachments = args.response.result > 0;
                destroyAttachmentModalCb();
            });
        };

        $scope.pageSlide = {
            internalNotes: "",
            approvalNotes: "",
            checked: true,
            size: "100px",
            toggle: (): void => {
                $scope.pageSlide.checked = !$scope.pageSlide.checked;
            },
        };

        $scope.togglePhaseExpand = (phase: IPhase): void => {
            phase.expanded = !phase.expanded;
        };

        $scope.SendRecommendations = (): void => {
            $scope.requestInProgress = true;
            Projects.mitigateProject($scope.Project.Id, $scope.Project.Version).then((response: IProtocolPacket): undefined | void => {
                if (!response.result) {
                    $scope.requestInProgress = false;
                    return;
                }
                if ($scope.permissions.canViewProjects) $state.go($scope.routes.projects);
                recommendationModal();
            });
        };

        $scope.CompleteAnalysis = (): void => {
            OneConfirm("", $rootScope.t("SureToEndAnalysisAndCompleteProject")).then((result: boolean): undefined | void => {
                if (!result) return;
                Projects.completeProject({ Id: $scope.Project.Id, ProjectVersion: $scope.Project.Version }).then((response: IProtocolPacket): undefined | void => {
                    if (!response.result) return;
                    approveModal();
                    if ($scope.permissions.canViewProjects) $state.go($scope.routes.projects);
                });
            });
        };

        $scope.createNewVersion = (): void | undefined => {
            OneConfirm("", $scope.labels.versionWillLock).then((result: boolean): void | undefined => {
                if (!result) return;
                $state.go($scope.routes.createForm, {
                    mode: "version",
                    id: $scope.Project.Id,
                    version: $scope.Project.Version,
                });
            });
        };

        $scope.duplicateProject = (): void => {
            $state.go("zen.app.pia.module.projects.duplicate", { id: $scope.Project.Id, version: $scope.Project.Version, project: $scope.Project });
        };

        /* this check is done with a function because $scope.Project may not be available when all the variables are instantiated.
        can optionally create this boolean in the .then() block in a promise. */
        $scope.canCreateNewVersion = (): boolean => {
            return $scope.Project && $scope.permissions.projectVersioning &&
                $scope.Project.StateDesc !== AssessmentStates.InProgress &&
                $scope.Project.IsLatestVersion && $scope.allowVersioning &&
                !$scope.Project.IsLinked && !$scope.isDataMapping &&
                $scope.Project.TemplateState !== "Archived";
        };

        $scope.openShareProjectModal = (): void => {
            const data: any = {
                Project: $scope.Project,
            };
            const modalInstance: any = ModalService.renderModal(data, "ShareProjectModalCtrl", "ShareProjectModal.html", "shareProjectModal");

            const destroyShareProjectModalCb: any = $scope.$root.$on("shareProjectModalCb", (event: ng.IAngularEvent, args: any): void => {
                if (args.cbInitiated) {
                    $scope.init();
                }
                destroyShareProjectModalCb();
            });
        };

        $scope.togglePageSlide = (): void => { $scope.pageSlide.checked = !$scope.pageSlide.checked; };

        $scope.noAdditionalChanges = (): void => {
            const sectionsRef: ISection[] = cloneDeep($scope.Project.Sections);
            let sections: ISection[];
            if (Permissions.canShow("ProjectsFullAccess") || Projects.isProjectReviewerOrLead($scope.Project)) {
                sections = Sections.getNoAdditionalChangesAllSections(sectionsRef);
            } else {
                sections = Sections.getNoAdditionalChangesAssignedSections(sectionsRef);
            }
            const sectionsCount: number = Sections.getSectionsCountWithoutChanges(sections);
            const questions: any = flatMap(sections, Sections.getQuestionsWithoutChanges);
            const message: string = $rootScope.t("NoAdditionalChangesConfirmation", { QuestionsCount: questions.length, SectionsCount: sectionsCount, NoChange: $rootScope.t("NoChange") });

            OneConfirm("", message, "", "", false, $rootScope.t("Continue"), $rootScope.t("Cancel")).then((result: boolean): undefined | void => {
                if (!result) return;
                $scope.isMakingNoAdditionalChangesRequest = true;
                ProjectQuestions.noAdditionalChanges($scope.Project.Id, $scope.Project.Version).then((response: IProtocolPacket): void => {
                    if (response.result) {
                        $scope.isMakingNoAdditionalChangesRequest = false;
                        $scope.init();
                    } else {
                        $scope.Project.Sections = sectionsRef;
                        $scope.isMakingNoAdditionalChangesRequest = false;
                    }
                });
            });
        };

        $scope.transitionToRiskSummary = (): void => {
            this.$state.go("zen.app.pia.module.projects.risk_summary", {
                ProjectId: this.$scope.Project.Id,
                Version: this.$scope.Project.Version,
            });
        };

        // Methods only used by FT AssessmentPermissionsRefactoring new features
        $scope.handleQuestionAction = (action: IAssessmentQuestionAction): undefined => {
            switch (action.type) {
                // Question Response Actions
                case ACTIONS.DATE_SELECTED:
                case ACTIONS.NO_SELECTED:
                case ACTIONS.YES_SELECTED:
                    Questions.setQuestionResponseAndSaveRefactored(action.question);
                    return;
                case ACTIONS.NOT_SURE_SELECTED:
                case ACTIONS.NOT_APPLICABLE_SELECTED:
                    Questions.toggleNotSureNotApplicableRefactored(action.question);
                    return;
                case ACTIONS.TEXTBOX_UPDATED:
                case ACTIONS.MULTISELECT_OPTION_SELECTED:
                case ACTIONS.OTHER_OPTION_UPDATED:
                case ACTIONS.JUSTIFICATION_UPDATED:
                    Questions.saveQuestionResponseRefactored(action.question);
                    return;
                case ACTIONS.MULTICHOICE_OPTION_SELECTED:
                    Questions.multiChoiceToggleRefactored(action.question);
                    return;
                // Question Addon Actions
                case ACTIONS.OPEN_COMMENTS_MODAL:
                    $scope.OpenCommentsModal(action.question);
                    return;
                case ACTIONS.OPEN_ATTACHMENT_MODAL:
                    $scope.OpenAttachmentModal(action.question);
                    return;
                case ACTIONS.OPEN_HISTORY_MODAL:
                    $scope.OpenHistoryModal(action.question);
                    return;
                // Question Side Button Actions
                case ACTIONS.OPEN_EXCEPTIONS_MODAL:
                    $scope.openRiskModal(action.question.SectionId, action.question);
                    return;
                case ACTIONS.TOGGLE_QUESTION_ACCEPTANCE:
                    $scope.toggleQuestionAcceptance(action.question.SectionId, action.question);
                    return;
                case ACTIONS.OPEN_NEED_INFO_MODAL:
                    $scope.openNeedInfoModal(action.question.SectionId, action.question);
                    return;
                case ACTIONS.OPEN_VIEW_INFO_ADDED_MODAL:
                    $scope.openViewInfoAddedModal(action.question.SectionId, action.question);
                    return;
                case ACTIONS.OPEN_ADD_INFO_MODAL:
                    $scope.openAddInfoModal(action.question.SectionId, action.question);
                    return;
            }
        };

        $scope.handleFooterButtonActions = (action: any): undefined => {
            switch (action.type) {
                case ACTIONS.INDEX_BACKWARD:
                    $scope.indexBackwards();
                    return;
                case ACTIONS.INDEX_FORWARD:
                    $scope.indexForward();
                    return;
                case ACTIONS.SUBMIT_ASSESSMENT:
                    $scope.SubmitAssessment();
                    return;
                case ACTIONS.SEND_BACK_MODAL:
                    $scope.sendBackModal();
                    return;
                case ACTIONS.APPROVE_ASSESSMENT:
                    $scope.ApproveAssessment();
                    return;
                case ACTIONS.OPEN_NEXT_STEPS_MODAL:
                    $scope.NextStepsModal();
                    return;
                case ACTIONS.OPEN_ANALYSIS_MODAL:
                    $scope.AnalysisModal();
                    return;
                case ACTIONS.SEND_RECOMMENDATIONS:
                    $scope.SendRecommendations();
                    return;
                case ACTIONS.COMPLETE_ANALYSIS:
                    $scope.CompleteAnalysis();
                    return;
            }
        };

        $scope.headerActions = {
            saveProject: $scope.SaveProject,
            openProjectAssignmentsModal: $scope.OpenProjectAssignmentsModal,
            openProjectReviewerModal: $scope.OpenProjectReviewerModal,
            openDeadlinesModal: $scope.OpenDeadlinesModal,
            orgSaveProject: $scope.OrgSaveProject,
        };

        $scope.init();
    }

    public $onInit(): void {
        const promises = [];

        if (!getRiskSettings(this.store.getState())) {
            if (this.Permissions.canShow("Assessments")) {
                return;
            } else {
                promises.push(this.SettingAction.fetchRiskSettings());
            }
        }

        if (!getProjectSettings(this.store.getState())) {
            if (this.Permissions.canShow("Assessments")) {
                return;
            } else {
                promises.push(this.SettingAction.fetchProjectSettings());
            }
        }

        this.$q.all(promises);
    }
}
