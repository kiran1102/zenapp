import { assign, map, filter, includes } from "lodash";
import {
    getAllUsers,
    getMultiFilteredUsers,
    getFilteredUsers,
    isActive,
    isNotProjectViewer,
    isProjectViewer,
} from "oneRedux/reducers/user.reducer";
import { IStore } from "interfaces/redux.interface";
import { IUser } from "interfaces/user.interface";
import { ICreateNextProjectParams } from "interfaces/project.interface";
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import { NotificationService } from "modules/shared/services/provider/notification.service";

// Redux
import { getCurrentUser } from "oneRedux/reducers/user.reducer";

export default class NextStepsController {

    static $inject: string[] = ["$rootScope", "$state", "$scope", "$uibModalInstance", "Projects", "Project", "EnableRiskAnalysis", "store", "NotificationService"];

    private error = false;
    private commentTouched = false;
    private translate: any;

    constructor(
        readonly $rootScope: IExtendedRootScopeService,
        readonly $state: ng.ui.IStateService,
        readonly $scope: any,
        readonly $uibModalInstance: any,
        readonly Projects: any,
        readonly Project: any,
        readonly EnableRiskAnalysis: any,
        readonly store: IStore,
        private notificationService: NotificationService,
        ) {

        $scope.Ready = false;

        $scope.init = (): void => {
            this.translate = this.$rootScope.t;
            $scope.currentUser = getCurrentUser(this.store.getState());
            $scope.enableRiskAnalysis = this.EnableRiskAnalysis;
            $scope.LeadName = this.Project.Lead;
            $scope.submitButtonText = $scope.getSubmitButtonText(this.Project.LeadId);
            $scope.TemplateName = this.Project.NextTemplateName ? this.Project.NextTemplateName : "Another Template";
            $scope.respondentSelected = Boolean(this.Project.LeadId);
            $scope.isAssigningAdditional = false;
            $scope.isSubmitted = false;
            $scope.loadingAssign = false;
            $scope.comment = "";
            $scope.project = assign({}, this.Project);

            $scope.allUsers = getAllUsers(this.store.getState());
            const activeNotProjectViewerUsers: IUser[] = getMultiFilteredUsers([isActive, isNotProjectViewer])(this.store.getState());
            $scope.projectViewerEmails = map(getFilteredUsers(isProjectViewer)(this.store.getState()), (user: IUser): string => user.Email);
            $scope.activeUsers = filter(activeNotProjectViewerUsers, (user: IUser): boolean => user.Id !== $scope.currentUser.Id);
            $scope.Ready = true;
        };

        $scope.noAdditional = (): void => {
            $scope.isSubmitted = true;
            this.$uibModalInstance.close({
                result: true,
                data: 1,
            });
        };

        $scope.assignAdditional = (): void => {
            $scope.isAssigningAdditional = true;
        };

        $scope.previous = (): void => {
            $scope.isAssigningAdditional = false;
        };

        $scope.respondentAssignUpdated = (): void => {
            $scope.LeadName = $scope.project.Lead || `[ ${this.translate("AssignProjectRespondent")} ]`;
            $scope.respondentSelected = Boolean($scope.project.LeadId);
            $scope.submitButtonText = $scope.getSubmitButtonText($scope.project.LeadId);
        };

        $scope.getSubmitButtonText = (leadId: string): string => {
            const isCurrentUser: boolean = leadId === $scope.currentUser.Id;
            return isCurrentUser ? this.translate("CreateAndBeginProject") : this.translate("Assign");
        };

        $scope.updateComment = (comment: string): void => {
            $scope.comment = comment;
        };

        $scope.assignQuestionnaire = (): void => {
            $scope.loadingAssign = true;
            $scope.isSubmitted = true;

            if (includes($scope.projectViewerEmails, $scope.project.Lead)) {
                this.notificationService.alertError($rootScope.t("Error"), this.translate("Error.RespondentEmailIsProjectViewer"));
                $scope.loadingAssign = false;
                $scope.isSubmitted = false;
                return;
            }

            const projectInfo: ICreateNextProjectParams = {
                Id: $scope.project.Id,
                ProjectVersion: $scope.project.Version,
                Comment: $scope.comment || "",
                LeadId: $scope.project.LeadId,
            };

            this.Projects.createNextProject(projectInfo).then((response: any): void => {
                if (!response.result) {
                    $scope.isSubmitted = false;
                    $scope.loadingAssign = false;
                    return;
                }

                $scope.loadingAssign = false;
                this.$state.go("zen.app.pia.module.projects.assessment.module.questions", { Id: projectInfo.Id }, { reload: true });
                this.$uibModalInstance.close({
                    result: true,
                    data: 2,
                });
            });
        };

        $scope.done = (): void => {
            this.$uibModalInstance.close({
                result: false,
                data: 0,
            });
        };

        $scope.cancel = (): void => {
            this.$uibModalInstance.dismiss("cancel");
        };

        $scope.init();
    }
}
