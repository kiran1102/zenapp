
export default function assessmentsPageSlideCtrl($scope) {

    $scope.checked = false;
    $scope.size = "100px";
    $scope.toggle = function () {
        $scope.checked = !$scope.checked;
    };
    $scope.mockRouteChange = function () {
        $scope.$broadcast("$locationChangeStart");
    };
}

assessmentsPageSlideCtrl.$inject = ["$scope"];
