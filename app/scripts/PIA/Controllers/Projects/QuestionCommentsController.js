
export default function QuestionCommentsCtrl($scope, $uibModalInstance, $interval, PRINCIPAL, Comment, AssessmentQuestionViewLogic, Project, Question) {
    $scope.Ready = false;

    $scope.CommentTypes = {
        Question: 10,
        MoreInfo: 20,
        InfoAdded: 25,
        RiskLevel: 30
    };

    $scope.init = function () {
        $scope.HasComments = true;
        $scope.currentUser = PRINCIPAL.getUser();
        $scope.canAddQuestionComments = AssessmentQuestionViewLogic.getCanAddQuestionComments($scope.currentUser, Project, Question);
        $scope.GetComments();
        if (!Question.HasComments) {
            $scope.HasComments = false;
        }
    };

    // Function Get Comments
    $scope.GetComments = function () {
        Comment.getQuestionComments(Question.Id)
            .then(function (response) {
                if (response.result) {
                    $scope.questionComments = response.data;
                    $scope.HasComments = $scope.questionComments.length !== 0;
                    _.each($scope.questionComments, function(element, index, array) {
                        element.dateObj = new Date(element.CreatedDT);
                        element.CreatedDT = element.CreatedDT;
                        element.Value = _.unescape(element.Value).replace(/&#34;/g, '"');
                        element.Value = _.unescape(element.Value).replace(/&#61;/g, '=');
                        element.Value = _.unescape(element.Value).replace(/&#43;/g, '+');
                        element.Value = _.unescape(element.Value).replace(/&#64;/g, '@');
                        element.Value = _.unescape(element.Value).replace(/&#96;/g, '`');
                    });
                } else {
                    if (!$scope.questionComments) {
                        $scope.questionComments = [];
                        $scope.HasComments = false;
                    }
                }
                $scope.Ready = true;
            });
    };

    // Function to get a Single Comment
    $scope.GetComment = function (id, callback) {
        Comment.getComment(id)
            .then(function (response) {
                if (response.result) {
                    response.data.CreatedDT = response.data.CreatedDT;
                    callback(response.data);
                } else {
                    callback(null);
                }
            });
    };

    $scope.newComment = {
        ProjectId: Project.Id,
        Version: Project.Version,
        Type: $scope.CommentTypes.Question,
        QuestionId: Question.Id,
        Value: ""
    };

    $scope.init();
    $scope.processing = false;

    var comments = $interval(function () {
        $scope.GetComments();
    }, 60000);

    $scope.AddComment = function () {
        $scope.processing = true;
        if ($scope.newComment.Value != "") {
            var newCommentClone = _.cloneDeep($scope.newComment);
            newCommentClone.Value = _.escape(newCommentClone.Value);
            Comment.addQuestion(newCommentClone)
                .then(function (response) {
                    if (response.result) {
                        $scope.newComment.Value = "";
                        $scope.GetComment(response.data, function (comment) {
                            if (comment) {
                                $scope.HasComments = true;
                                comment.Value = _.unescape(comment.Value).replace(/&#34;/g, '"');
                                comment.Value = _.unescape(comment.Value).replace(/&#61;/g, '=');
                                comment.Value = _.unescape(comment.Value).replace(/&#43;/g, '+');
                                comment.Value = _.unescape(comment.Value).replace(/&#64;/g, '@');
                                comment.Value = _.unescape(comment.Value).replace(/&#96;/g, '`');
                                $scope.questionComments.push(comment);
                            }
                        });
                    }
                    $scope.processing = false;
                });
        }
    };

    $scope.DeleteComment = function (index) {
        Comment.delete($scope.questionComments[index].Id)
            .then(function (response) {
                if (response.result) {
                    $scope.questionComments.splice(index, 1);
                }
            });
    };

    $scope.closeModal = function () {
        $uibModalInstance.close({
            result: $scope.questionComments.length,
        });
    };

    $scope.$on("$destroy", function () {
        // Make sure that the interval is destroyed too
        $interval.cancel(comments);
    });


}

QuestionCommentsCtrl.$inject = ["$scope", "$uibModalInstance", "$interval", "PRINCIPAL", "Comment", "AssessmentQuestionViewLogic", "Project", "Question"];
