import { QuestionStates } from "enums/assessment-question.enum";

export default function QuestionHistoryCtrl($rootScope, $scope, $uibModalInstance, $interval, Users, Projects, Questions, Project, Question, ProjectQuestions) {

	var init = function () {
		getHistory();
	};
	var translate = $rootScope.t;
	$scope.Project = Project;
	$scope.Question = Question;

	$scope.questionHistory = {
		CreateDT: "----",
		CreatedBy: "----",
		Entries: [],
		Name: "----"
	};

	$scope.projectStatuses = [
		{
			Name: translate("All"),
			Value: ""
		},
		{
			Name: translate("Unanswered"),
			Value: QuestionStates.Unanswered
		},
		{
			Name: translate("Answered"),
			Value: QuestionStates.Answered
		},
		{
			Name: translate("Accepted"),
			Value: QuestionStates.Accepted
		},
		{
			Name: translate("InfoRequested"),
			Value: QuestionStates.InfoRequested
		},
		{
			Name: translate("InfoAdded"),
			Value: QuestionStates.InfoAdded
		},
		{
			Name: translate("Skipped"),
			Value: QuestionStates.Skipped
		}
	];

	$scope.historyFilter = {
		All: "",
		State: $scope.projectStatuses[0].Value
	};

	var QuestionStateEnums = function getQuestionStateEnums(states) {
		var stateEnums = {};
		for (var state in QuestionStates) {
			if (QuestionStates.hasOwnProperty(state) && (_.isNumber(QuestionStates[state]) || _.isString(QuestionStates[state]))) {
				stateEnums[QuestionStates[state]] = state;
			}
		}
		return stateEnums;
	}(QuestionStates);

	$scope.questionStateFilter = function (item) {
		if (!$scope.historyFilter.State) return true;

		var state = QuestionStateEnums[$scope.historyFilter.State];
		return (state && (item.StateDesc === state));
	};

	$scope.isLongValue = function (event) {
		var value = event.UpdateDesc;
		var length = value.split("").length;
		if (length > 106) {
			event.UpdateDescShort = event.UpdateDesc.substring(0, 85) + "...";
			return true;
		}
		else {
			return false;
		}
	};

	$scope.closeModal = function () {
		$uibModalInstance.close({
			result: false,
			data: ""
		});
	};

	$scope.cancel = function () {
		$uibModalInstance.dismiss("cancel");
	};

	function getHistory() {
		ProjectQuestions.getHistory(Question.Id, Project.Version).then(
			function (response) {
				if (response.result) {
                    $scope.questionHistory = response.data;
                    _.each($scope.questionHistory.Entries, function(event, index) {
                        // Set State Labels
                        switch (event.StateDesc) {
                            case "Unanswered":
                                event.StateLabel = "default";
                                event.StateLabelText = translate("Unanswered");
                                break;
                            case "Answered":
                                event.StateLabel = "primary";
                                event.StateLabelText = translate("Answered");
                                break;
                            case "Accepted":
                                event.StateLabel = "success";
                                event.StateLabelText = translate("Accepted");
                                break;
                            case "Info Requested":
                                event.StateLabel = "warning";
                                event.StateLabelText = translate("InfoRequested");
                                break;
                            case "Info Added":
                                event.StateLabel = "info";
                                event.StateLabelText = translate("InfoAdded");
                                break;
                            case "Skipped":
                                event.StateLabel = "default";
                                event.StateLabelText = translate("Skipped");
                                break;
                            default:
                                return null;
                        }
                    });
				}
				else {
					if (!$scope.questionHistory) {
						$scope.questionHistory = [];
					}
				}
			}
		);
	}

	init();

}

QuestionHistoryCtrl.$inject = ["$rootScope", "$scope", "$uibModalInstance", "$interval", "Users", "Projects", "Questions", "Project", "Question", "ProjectQuestions"];
