import { KeyCodes } from "enums/key-codes.enum";
import { TemplateTypes } from "enums/template-type.enum";
import { ExportTypes } from "enums/export-types.enum";
import { getProjectSettings, isRiskSummaryEnabled } from "oneRedux/reducers/setting.reducer.ts";
import { AssessmentStates, AssessmentStateKeys, AssessmentTypes } from "constants/assessment.constants";

export default function ProjectsCtrl(
    store,
    $rootScope,
    $timeout,
    $scope,
    $state,
    $q,
    currentUser,
    Content,
    Projects,
    Users,
    UserActionService,
    Permissions,
    OnboardingService,
    $uibModal,
    Export,
    $localStorage,
    ENUMS,
    Settings,
    NotificationService,
    Templates,
    Labels,
    Upgrade,
    ModalService,
    SettingAction,
    McmModalService
) {
    $scope.translate = $rootScope.t;
    $scope.currentUser = currentUser;
    $scope.RoleNames = ENUMS.RoleNames;
    $scope.users = UserActionService.getUserList();
    $scope.templateTypes = TemplateTypes;
    $scope.sortTypes = ENUMS.SortTypes;
    $scope.isDataMapping = $state.includes(AssessmentTypes.DM.BaseRoute);
    $scope.templateType = $scope.isDataMapping ? TemplateTypes.Datamapping : TemplateTypes.Custom;
    $scope.routes = {
        single: ($scope.isDataMapping) ? "zen.app.pia.module.datamap.views.assessment-old.single.module.questions" : "zen.app.pia.module.projects.assessment.module.questions",
        projects: ($scope.isDataMapping) ? "zen.app.pia.module.datamap.views.assessment-old.list" : "zen.app.pia.module.projects.list",
        createForm: ($scope.isDataMapping) ? "zen.app.pia.module.datamap.views.assessment.wizard.project_info" : "zen.app.pia.module.projects.wizard.project_info",
    };
    $scope.labels = Labels.getLabels("projectList", $scope.templateType);
    $scope.defaultProjectStates = [
        { label: $scope.labels.filter, value: null },
        { label: $rootScope.t("NeedsAttention"), value: "needs attention" },
        { label: $rootScope.t(AssessmentStateKeys.InProgress), value: AssessmentStates.InProgress },
        { label: $rootScope.t(AssessmentStateKeys.UnderReview), value: AssessmentStates.UnderReview },
        { label: $rootScope.t(AssessmentStateKeys.InfoNeeded), value: AssessmentStates.InfoNeeded },
        { label: $rootScope.t(AssessmentStateKeys.RiskAssessment), value: AssessmentStates.RiskAssessment },
        { label: $rootScope.t(AssessmentStateKeys.RiskTreatment), value: AssessmentStates.RiskTreatment },
        { label: $rootScope.t(AssessmentStateKeys.Completed), value: AssessmentStates.Completed }
    ];
    $scope.sortType = "created"; // set the default sort type
    $scope.sortReverse = true; // set the default sort order
    $scope.fade = !$scope.fade;
    $scope.projectsCompleted = 0;
    $scope.projectsLoading = true;
    $scope.allowVersioning = false;
    $scope.taggingEnabled = false;

    OnboardingService.onboard(
        $rootScope.t("WelcomeToProjects"),
        Content.GetKey("OnBoardingProjects")
    );

    $scope.filtered = false;
    $scope.filterOpen = true;

    $scope.maxSize = 3;
    $scope.pageSize = 10;
    $scope.pageNumber = 1;
    $scope.pageMetadata = {
        TotalItemCount: 0,
        ItemCount: 0,
        TotalPageCount: 1
    };

    $scope.pageRange = function () {
        var firstNum = $scope.pageNumber === 1 ? 1 : ($scope.pageNumber - 1) * $scope.pageSize + 1;
        var secondNum = $scope.pageNumber === $scope.pageMetadata.TotalPageCount ? $scope.pageMetadata.TotalItemCount : (firstNum - 1) + $scope.pageSize;
        return firstNum + " - " + secondNum;
    };

    $scope.projectStates = AssessmentStates;
    $scope.projectStateKeys = AssessmentStateKeys;

    $scope.RiskStates = {
        None: 0,
        Low: 1,
        Medium: 2,
        High: 3,
        VeryHigh: 4
    };
    $scope.SelfServiceEnabled = false;
    $scope.Ready = false;

    $scope.exportInProgressProjectIds = [];

    $scope.templateFilters = [
        { label: $rootScope.t("AllTemplates"), value: null }
    ];

    $scope.filter = {
        stateFilter: !_.isUndefined($localStorage.projectsStateFilter) ? $localStorage.projectsStateFilter : Projects.getProjectFilterStatus($scope.currentUser),
        searchFilter: "",
        templateFilter: !_.isUndefined($localStorage.projectsTemplateFilter) ? $localStorage.projectsTemplateFilter : null
    };

    $scope.showTemplateFilter = !($scope.isDataMapping && $scope.templateFilters.length < 2);

    $scope.toggleFilter = function () {
        $scope.filterOpen = !$scope.filterOpen;
    };
    $scope.openFilter = function () {
        $scope.filterOpen = true;
    };
    $scope.closeFilter = function () {
        $scope.filterOpen = false;
    };

    $scope.searchDelayActive = false;
    $scope.getProjectPage = function (pageSize, pageNumber, filters, sortType, sortReverse) {
        var paginationOptions = {
            pageSize: pageSize,
            pageNumber: pageNumber
        };

        var filterOptions = [];
        if (filters.stateFilter) {
            if (filters.stateFilter === "needs attention") {
                filterOptions.push({
                    filterType: "needsAttention",
                    filterValue: "",
                });
            } else {
                filterOptions.push({
                    filterType: "state",
                    filterValue: filters.stateFilter,
                });
            }
        }
        if (filters.searchFilter) {
            filterOptions.push({
                filterType: "search",
                filterValue: filters.searchFilter
            });
        }
        if (filters.templateFilter) {
            filterOptions.push({
                filterType: "template",
                filterValue: filters.templateFilter
            });
        }

        var sortOptions = {
            sortType: sortType,
            sortReverse: sortReverse
        };

        $scope.projectsLoading = true;

        Projects.getPage($scope.templateType, paginationOptions, filterOptions, sortOptions).then(function (response) {
            if (response.result) {
                $scope.pageMetadata = response.data.metadata;
                $scope.projects = _(response.data.page)
                    .map(Projects.formatCreatedDate)
                    .map(Projects.formatLocaleDeadline)
                    .map(Projects.setProjectStateNameAndLabels)
                    .map(Projects.setProjectIsShared)
                    .map(Projects.setProjectRiskStates)
                    .map(Projects.setProjectCanBeDuplicated)
                    .value();
            } else {
                $scope.projects = [];
            }
            $scope.Ready = true;
            $scope.projectsLoading = false;
        });
    };

    $scope.updateFilter = function (key, filter) {
        $scope.filter[key] = filter;
        $scope.updatePage();
    }

    $scope.updatePage = function () {
        $localStorage.projectsStateFilter = $scope.filter.stateFilter;
        $localStorage.projectsTemplateFilter = $scope.filter.templateFilter;
        $scope.filtered = !_.isNull($scope.filter.stateFilter) ||
            !_.isNull($scope.filter.templateFilter) ||
            Boolean($scope.filter.searchFilter);

        $scope.getProjectPage($scope.pageSize, $scope.pageNumber, $scope.filter, $scope.sortType, $scope.sortReverse);
    };

    $scope.search = function (e) {
        if (e.keyCode === KeyCodes.Enter && !$scope.searchDelayActive) {
            $scope.searchDelayActive = true;
            $timeout($scope.updatePage, 600).then(function () {
                $scope.searchDelayActive = false;
            });
        }
    };

    $scope.updateSort = function (sortColumn, sortDirection) {
        $scope.sortType = sortColumn;
        $scope.sortReverse = sortDirection === $scope.sortTypes.Descending;
        $scope.sortColumn = sortColumn;
        $scope.sortDirection = sortDirection;

        $scope.updatePage();
    };

    $scope.changeProjectVersion = function (project, version) {
        $scope.projectVersionLoading = true;
        $scope.loadingRowIndex = _.findIndex($scope.projects, ["Id", project.Id]);
        $timeout(function () {
            return Projects.read(project.Id, version).then(function (response) {
                if (response.result) {
                    var formattedProject = _.last(_([response.data])
                        .map(Projects.formatCreatedDate)
                        .map(Projects.formatLocaleDeadline)
                        .map(Projects.setProjectStateNameAndLabels)
                        .map(Projects.setProjectIsShared)
                        .map(Projects.setProjectRiskStates)
                        .map(Projects.setProjectCanBeDuplicated)
                        .value());
                    $scope.projects[$scope.loadingRowIndex] = formattedProject;
                }
                $scope.projectVersionLoading = false;
                $scope.loadingRowIndex = null;
            });
        }, 200);
    };

    var fetchProjectSettings = function () {
        return SettingAction.fetchProjectSettings().then(function (response) {
            if (response) {
                var projectSettings = getProjectSettings(store.getState());
                $scope.allowVersioning = projectSettings.IsVersioningEnabled;
                $scope.SelfServiceEnabled = projectSettings.SelfServiceEnabled;
                $scope.buttonsReady = true;
            }
        });
    };

    var fetchHeatmapSettings = function () {
        return SettingAction.fetchRiskSettings();
    }

    var getTaggingSettings = function () {
        Settings.getTagging().then(function (response) {
            if (response.result) {
                $scope.taggingEnabled = response.data.ProjectTaggingEnabled;
            }
        });
    };

    var getTemplates = function () {
        return Templates.publishedList($scope.templateType).then(function (response) {
            if (response.result) {
                _.each(response.data, function (template) {
                    if (template !== null) {
                        $scope.templateFilters.push({
                            label: template.Name,
                            value: template.Id
                        });
                    }
                });
            }
        });
    };

    var zipPermissions = function () {
        return {
            taggingEnabled: $scope.taggingEnabled && Permissions.canShow("Tagging"),
            versioningEnabled: $scope.allowVersioning && Permissions.canShow("ProjectVersioning") && !$scope.isDataMapping,
            canShowProjectsDeadline: Permissions.canShow("ProjectsDeadline"),
            canDuplicateProject: Permissions.canShow("ProjectDuplicate"),
            canShowProjectsViewHistory: Permissions.canShow("ProjectsViewHistory"),
            canShowProjectsViewHistoryUpgrade: Permissions.canShow("ProjectsViewHistoryUpgrade"),
            canShowTagging: Permissions.canShow("Tagging"),
            canShowTaggingUpgrade: Permissions.canShow("TaggingUpgrade"),
            canShowProjectsExportPdf: Permissions.canShow("ProjectsExportPDF"),
            canShowProjectsExportPdfUpgrade: Permissions.canShow("ProjectsExportPDFUpgrade"),
            canShowProjectsDelete: Permissions.canShow("ProjectsDelete"),
            canShowProjectsDeleteUpgrade: Permissions.canShow("ProjectsDeleteUpgrade"),
            canShowShareProject: Permissions.canShow("ProjectsShare") && !$scope.isDataMapping,
            canShowShareProjectUpgrade: Permissions.canShow("ProjectsShareUpgrade") && !$scope.isDataMapping,
        };
    };

    $scope.init = function () {
        var projectsPromises = [
            fetchProjectSettings(),
            fetchHeatmapSettings(),
            getTaggingSettings(),
            getTemplates(),
        ];

        $q.all(projectsPromises).then(function () {
            // Once all Promises are resolved check whether we are allowed to show the template filter.
            // Note that the template filter always has atleast 1 option.
            $scope.showTemplateFilter = !($scope.templateType === $scope.templateTypes.Datamapping && $scope.templateFilters.length < 3);
            $scope.permissions = zipPermissions();
            $scope.updatePage();
        });
    };

    $scope.init();

    $scope.goToProject = function (rowData) {
        if (!rowData.loadingVersion) {

            if (!$scope.isDataMapping &&
                isRiskSummaryEnabled(store.getState())
                && (rowData.StateDesc === AssessmentStates.RiskAssessment
                    || rowData.StateDesc === AssessmentStates.RiskTreatment
                    || rowData.StateDesc === AssessmentStates.Completed)
                && rowData.ProjectRisk
            ) {
                $state.go("zen.app.pia.module.projects.risk_summary", {
                    ProjectId: rowData.Id,
                    Version: rowData.Version,
                });
            } else {
                $state.go($scope.routes.single, {
                    Id: rowData.Id,
                    Version: rowData.Version,
                });
            }
        }
    };

    $scope.GenerateCsv = function () {
        var params = {
            templateTypes: $scope.isDataMapping ? [ENUMS.TemplateTypes.Datamapping] : [ENUMS.TemplateTypes.Custom, ENUMS.TemplateTypes.SelfService],
            state: $scope.filter.stateFilter,
            template: $scope.filter.templateFilter || undefined,
            search: $scope.filter.searchFilter,
            needsAttention: $scope.filter.stateFilter === "needs attention",
            sort: {
                column: $scope.sortColumn || "created",
                direction: $scope.sortDirection === $scope.sortTypes.Descending ? 1 : 0,
            },
        };
        Export.exportWithParams("/project/getprojectcsv", ExportTypes.CSV, $scope.labels.projects, params);
    };

    $scope.generatePdf = function (id, version, name) {
        var projectIndex = _.findIndex($scope.projects, ["Id", id]);
        if (projectIndex > -1) {
            $scope.projectIdInProgress = id;
            const isBeingExported = $scope.exportInProgressProjectIds.indexOf(id) !== -1;
            if (isBeingExported) {
                NotificationService.alertWait("", $rootScope.t("PleaseWaitExportProjectProgress"));
                return;
            }
            $scope.projects[projectIndex].loadingAction = true;
            $scope.exportInProgressProjectIds.push(id);
            Export.pdfProjectExport(id, version, name).then(function (response) {
                $scope.projectIdInProgress = "";
                $scope.projects[projectIndex].loadingAction = false;
                const index = $scope.exportInProgressProjectIds.indexOf(id);
                $scope.exportInProgressProjectIds.splice(index, 1);
            });
        }
    };

    $scope.goToSelfService = function () {
        $state.go("zen.app.pia.module.selfservice.list");
    };

    $scope.addProject = function () {
        $state.go("zen.app.pia.module.projects.wizard.project_type");
    };

    $scope.duplicateProject = function (project) {
        $state.go("zen.app.pia.module.projects.duplicate", { id: project.Id, version: project.Version });
    };

    $scope.deleteProject = function (Id) {
        var projectIndex = _.findIndex($scope.projects, ["Id", Id]);
        if (projectIndex > -1) {
            if (Permissions.canShow("ProjectsDelete")) {
                var message = $rootScope.t("SureYouWantToDeleteThisProject");
                if ($scope.allowVersioning) {
                    message += " " + $rootScope.t("AllVersionsWillBeDeleted");
                }
                OneConfirm("", message, "Confirm", "Cancel").then(function (result) {
                    if (result) {
                        $scope.projectIdInProgress = Id;
                        $scope.projects[projectIndex].loadingAction = true;
                        Projects.delete(Id).then(function (response) {
                            if (response.result) {
                                $scope.updatePage();
                            } else {
                                $scope.projects[projectIndex].loadingAction = false;
                            }
                            $scope.projectIdInProgress = "";
                        });
                    }
                });
            }
        }
    };

    $scope.CloneProject = function (Id) {
        Projects.cloneProject(Id).then(function (response) {
            if (response.result) {
                Projects.read(response.data).then(function (response) {
                    if (response.data) {
                        if (response.data.StateDesc === "In Progress") {
                            response.data.StateLabel = "primary";
                        }

                        $scope.projects.push(response.data);
                    }
                });
            }
        });
    };

    $scope.refreshProjects = function () {
        $scope.projectsLoading = true;
        $scope.init();
    };

    $scope.openHistoryModal = function (Id) {
        var index = _.findIndex($scope.projects, ["Id", Id]);

        var modalInstance = $uibModal.open({
            templateUrl: "HistoryModal.html",
            controller: "ProjectHistoryCtrl",
            scope: $scope,
            size: "lg",
            backdrop: "static",
            keyboard: false,
            resolve: {
                Project: function () {
                    return $scope.projects[index];
                }
            }
        });
    };

    $scope.openTagsModal = function (Id) {
        var index = _.findIndex($scope.projects, ["Id", Id]);

        var modalInstance = $uibModal.open({
            templateUrl: "TagsModal.html",
            controller: "ProjectTagsCtrl",
            scope: $scope,
            size: "lg",
            backdrop: "static",
            keyboard: false,
            resolve: {
                Project: function () {
                    return $scope.projects[index];
                }
            }
        });

        modalInstance.result.then(function (response) {
            if (response.result) {
                NotificationService.alertSuccess($rootScope.t("Success"), $rootScope.t("ProjectTagAdded", { ProjectTerm: $rootScope.customTerms.Project }));
            }
        });
    };

    $scope.createNewVersion = function (project) {
        $state.go($scope.routes.createForm, {
            mode: "version",
            id: project.Id,
            version: project.Version
        });
    };

    $scope.ToggleUtilityMenu = function () {
        $scope.utilityMenuOpen = !$scope.utilityMenuOpen;
    };

    $scope.clearFilters = function () {
        $scope.filter.stateFilter = null;
        $scope.filter.templateFilter = null;
        $scope.filter.searchFilter = "";
        $scope.updatePage();
    };

    $scope.upgradeModule = function (module, upgradeState) {
        // TODO: Pass related key when content is ready
        McmModalService.openMcmModal("Upgrade");
    };

    $scope.openShareProjectModal = function (project) {
        var users = Users.getActiveProjectViewers($scope.users);
        var data = {
            Users: users,
            Project: project,
        };
        var modalInstance = ModalService.renderModal(data, "ShareProjectModalCtrl", "ShareProjectModal.html", "shareProjectModal");

        var destroyShareProjectModalCb = $scope.$root.$on("shareProjectModalCb", function (event, args) {
            if (args.cbInitiated) {
                $scope.init();
            }
            destroyShareProjectModalCb();
        });
    };

    $scope.receiveAction = function (action, payload) {
        switch (action) {
            case "EXPORT_PDF":
                return $scope.generatePdf(payload.rowData.Id, payload.rowData.Version, payload.rowData.Name);
            case "SHOW_EXPORT_PDF_UPGRADE_MODAL":
                return $scope.upgradeModule("ProjectsExportPDF");
            case "SHOW_PROJECT_HISTORY":
                return $scope.openHistoryModal(payload.rowData.Id, payload.rowData.Version);
            case "SHOW_PROJECT_HISTORY_UPGRADE_MODAL":
                return $scope.upgradeModule("ProjectsViewHistory");
            case "SHARE_PROJECT":
                return $scope.openShareProjectModal(payload.rowData);
            case "DELETE_PROJECT":
                return $scope.deleteProject(payload.rowData.Id);
            case "SHOW_DELETE_PROJECT_UPGRADE_MODAL":
                return $scope.upgradeModule("ProjectsDelete");
            case "TAG_PROJECT":
                return $scope.openTagsModal(payload.rowData.Id);
            case "SHOW_TAG_PROJECT_UPGRADE_MODAL":
                return $scope.upgradeModule("Tagging");
            case "CREATE_NEW_PROJECT_VERSION":
                return $scope.createNewVersion(payload.rowData);
            case "GO_TO_PROJECT":
                if (!$scope.projectVersionLoading) return $scope.goToProject(payload.rowData);
                return;
            case "SORT_COLUMN":
                return $scope.updateSort(payload.sortColumn, payload.sortDirection);
            case "VERSION_CHANGED":
                return $scope.changeProjectVersion(payload.rowData, payload.version);
            case "DUPLICATE_PROJECT":
                return $scope.duplicateProject(payload.rowData);
        }
    };
}

ProjectsCtrl.$inject = [
    "store",
    "$rootScope",
    "$timeout",
    "$scope",
    "$state",
    "$q",
    "currentUser",
    "Content",
    "Projects",
    "Users",
    "UserActionService",
    "Permissions",
    "OnboardingService",
    "$uibModal",
    "Export",
    "$localStorage",
    "ENUMS",
    "Settings",
    "NotificationService",
    "Templates",
    "Labels",
    "Upgrade",
    "ModalService",
    "SettingAction",
    "McmModalService"
];
