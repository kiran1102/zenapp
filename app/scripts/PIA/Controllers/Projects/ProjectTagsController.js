export default function ProjectTagsCtrl($rootScope, $scope, $uibModalInstance, $interval, Users, Projects, Project, ENUMS, Tags, $timeout) {

    $scope.init = function () {
        $scope.Project = Project;
        $scope.tagIsSaving = false;
        $scope.tagInputPlaceholder = $rootScope.t("AddTags").toString();
    };

    // Update Project
    $scope.updateProject = function () {
        $scope.projectUpdating = true;
        var project = {
            ProjectId: $scope.Project.Id,
            ProjectVersion: $scope.Project.Version,
            Tags: $scope.Project.Tags
        };

        Tags.updateProject(project).then(function (response) {
            if (response.result) {
                $scope.projectUpdating = false;
                $uibModalInstance.close(response);
            }
        }, function() {
            $scope.projectUpdating = false;
        });
    };

    // Add project tag
    $scope.addTag = function (tag) {
        $scope.tagIsSaving = true;
        Tags.addTag(tag).then(function (response) {
            if (response.result) {
                tag.Id = response.data;
            } else {
                tag = null;
            }
            $scope.tagIsSaving = false;
        }, function() {
            $scope.tagIsSaving = false;
        });
    };

    $scope.getTagsList = function (filter, count) {
        return Tags.getTags(filter, count).then(function (response) {
            return response.data;
        });
    };

    $scope.init();

    $scope.closeModal = function () {
        $uibModalInstance.close({
            result: false,
            data: ""
        });
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss("cancel");
    };
}

ProjectTagsCtrl.$inject = ["$rootScope", "$scope", "$uibModalInstance", "$interval", "Users", "Projects", "Project", "ENUMS", "Tags", "$timeout"];
