export default function AnalysisModalCtrl($state, $scope, $uibModalInstance, Projects, Project) {

    $scope.Ready = true;
    $scope.Error = false;
    $scope.isLoading = false;

    $scope.continueToRiskSummary = function () {
        $scope.isLoading = true;
        Projects.analyzeProject(Project.Id, Project.Version)
            .then(function (response) {
                if (response.result) {
                    $uibModalInstance.close({
                        result: true
                    });
                }
            }, function() {
                $scope.isLoading = false;
            });
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss("cancel");
    };
}

AnalysisModalCtrl.$inject = ["$state", "$scope", "$uibModalInstance", "Projects", "Project"];
