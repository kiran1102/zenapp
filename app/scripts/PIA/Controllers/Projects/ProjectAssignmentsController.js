﻿import { getCurrentUser } from "oneRedux/reducers/user.reducer";

import { OrgUserDropdownTypes } from "enums/org-user-dropdown.enum";

import { EmptyGuid } from "constants/empty-guid.constant"
import { Regex } from "constants/regex.constant";

export default function ProjectAssignmentsCtrl(store, $rootScope, $scope, $uibModalInstance, Assignments, project, Team, SectionId, ScopeContext, Section, SectionAssignment, Initial, UserActionService) {

    $scope.stage = 0;
    $scope.Ready = false;
    $scope.assignmentScope = "";

    $scope.dropdownLabel = $rootScope.t("AssignRespondent");
    $scope.dropdownType = OrgUserDropdownTypes.RespondentPia;
    $scope.respondentSelected = function(selection, isValid) {
        $scope.Assignment.AssigneeId = selection.Id || selection;
        $scope.isSelectionValid = isValid;
    }

    $scope.init = function () {
        $scope.Project = project;
        $scope.currentUser = getCurrentUser(store.getState());

        if (_.isUndefined(SectionAssignment.AssigneeId)) {
            if (Section.Assignment) {
                SectionAssignment = Section.Assignment;
            } else if (Section.Id === EmptyGuid) {
                SectionAssignment = { AssigneeId: $scope.Project.LeadId };
            } else {
                SectionAssignment = { AssigneeId: "" };
            }
        }

        $scope.currentAssignee = _.find(Team, function (user) {
            return user.Id === SectionAssignment.AssigneeId;
        });

        if (ScopeContext) {
            $scope.Assignment = {
                ProjectId: ScopeContext.Id,
                ProjectVersion: ScopeContext.Version,
                AssigneeId: "",
                Comment: "",
                Type: 10
            };
        } else {
            $scope.Assignment = {
                ProjectId: "",
                ProjectVersion: "",
                AssigneeId: "",
                Comment: "",
                Type: 10
            };
        }

        $scope.Ready = true;
    };

    $scope.init();

    $scope.AssignProject = function () {
        $scope.submitInProgress = true;
        var promise = {};
        if (Initial) {
            var projectVersion = ($scope.wizard.SelectedVersion && $scope.mode === "version") ? $scope.wizard.SelectedVersion : "1";
            var sendObject = {
                AssigneeId: $scope.Assignment.AssigneeId,
                SectionId: SectionId,
                ProjectVersion: projectVersion,
                Comment: $scope.Assignment.Comment,
            };
            var closeData = {
                result: true,
                data: sendObject
            };
            $uibModalInstance.close(closeData);
            $scope.submitInProgress = false;
        } else {
            if ($scope.Assignment && $scope.Assignment.AssigneeId) {
                $scope.stage = 1; // Processing
                var params = {
                    AssigneeId: $scope.Assignment.AssigneeId,
                    SectionId: Section.Id,
                    ProjectVersion: $scope.Project.Version,
                    Comment: $scope.Assignment.Comment
                };
                if (Utilities.matchRegex($scope.Assignment.AssigneeId, Regex.GUID)) {
                    if (project.Assignment) {
                        $scope.assignmentScope = "project";
                        promise = Assignments.assignProject($scope.Assignment);
                    } else {
                        $scope.assignmentScope = "section";
                        // Re-assigning a section respondent creates new section on the backend
                        promise = Assignments.assignSection(params);
                    }
                    promise.then(function (response) {
                        $scope.handleAssignmentPromiseResponse(response, true);
                    });
                } else if ($scope.Assignment.AssigneeId.match($scope.$root.emailRegex)) {
                    if (project.Assignment) {
                        $scope.assignmentScope = "project";
                        promise = Assignments.assignProjectEmail($scope.Assignment);
                    } else {
                        $scope.assignmentScope = "section";
                        promise = Assignments.assignSectionEmail(params);
                    }
                    promise.then(function (response) {
                        $scope.handleAssignmentPromiseResponse(response, true);
                    });
                }
            }
        }
    };

    $scope.handleAssignmentPromiseResponse = function (response, needsAssignmentScope) {
        if (!response.result) {
            $scope.done();
            return;
        }
        Assignments.get(response.data).then(function (res) {
            // res.data: {Id: string, AssigneeId: string, Assignee: null}
            var closeData = {
                result: res.result,
                data: res.data || null,
            };

            if (res.result && needsAssignmentScope) {
                closeData.assignmentScope = $scope.assignmentScope;
            }

            if (res.result && $scope.isAssigningNewInvitedUser) {
                UserActionService.fetchUsers();
            }

            $uibModalInstance.close(closeData);
            $scope.submitInProgress = false;
            $scope.stage = 0;
        });
    };

    $scope.done = function () {
        $uibModalInstance.close({
            result: false,
            data: null
        });
        $scope.submitInProgress = false;
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss("cancel");
    };
}

ProjectAssignmentsCtrl.$inject = ["store", "$rootScope", "$scope", "$uibModalInstance", "Assignments", "project",
    "Team", "SectionId", "ScopeContext", "Section", "SectionAssignment", "Initial", "UserActionService"];
