import { Permissions } from "modules/shared/services/helper/permissions.service";

export default class SuspendModalController {

    static $inject: string[] = ["$scope", "Permissions"];

    constructor(
        private readonly $scope: any,
        private permissions: Permissions,
    ) {
        $scope.hasProjectFullAccess = permissions.canShow("ProjectsFullAccess");
        $scope.Ready = true;
    }
}
