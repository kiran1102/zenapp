import TreeMap from "TreeMap";
import {
    each,
    filter,
    find,
    includes,
    isArray,
    isUndefined,
    map,
    toLower
} from "lodash";
import {
    getDisabledUsers,
    getFilteredUsers,
    isProjectViewer,
    getCurrentUser,
} from "oneRedux/reducers/user.reducer";
import { AssessmentTypes, AssessmentPermissions } from "constants/assessment.constants";
import { OrgUserTraversal } from "enums/org-user-dropdown.enum";

export default function ProjectsWizardFormCtrl($scope, $state, $q, $uibModal,
    $sessionStorage, $rootScope, Permissions, PRINCIPAL, NotificationService, Settings,
    Projects, Templates, Users, OrgGroups, OrgGroupStoreNew, Tags, ENUMS, Labels,
    store, SettingStore, UserActionService
) {
    $scope.permissions = {
        ConditionsAdd: Permissions.canShow("ConditionsAdd"),
        TemplatesCopy: Permissions.canShow("TemplatesCopy"),
        TemplatesCustomWelcomeText: Permissions.canShow("TemplatesCustomWelcomeText"),
        TemplatesDetailsEditDescription: Permissions.canShow("TemplatesDetailsEditDescription"),
        TemplatesDetailsEditIcon: Permissions.canShow("TemplatesDetailsEditIcon"),
        TemplatesDetailsEditName: Permissions.canShow("TemplatesDetailsEditName"),
        TemplatesPublish: Permissions.canShow("TemplatesPublish"),
        TemplatesPublishedEdit: Permissions.canShow("TemplatesPublishedEdit"),
        TemplatesQuestionItemEdit: Permissions.canShow("TemplatesQuestionItemEdit"),
        TemplatesQuestionItemPreview: Permissions.canShow("TemplatesQuestionItemPreview"),
        TemplatesSectionAdd: Permissions.canShow("TemplatesSectionAdd"),
        TemplatesSectionDelete: Permissions.canShow("TemplatesSectionDelete"),
        TemplatesSectionDeleteUpgrade: Permissions.canShow("TemplatesSectionDeleteUpgrade"),
        TemplatesShowVersions: Permissions.canShow("TemplatesShowVersions"),
        TemplatesLock: Permissions.canShow("TemplatesLock"),
    };
    $scope.templateType = $state.includes(AssessmentTypes.DM.BaseRoute) ? ENUMS.TemplateTypes.Datamapping : ENUMS.TemplateTypes.Custom;
    $scope.routes = {
        single: ($scope.templateType === ENUMS.TemplateTypes.Datamapping) ? "zen.app.pia.module.datamap.views.assessment-old.single.module.questions" : "zen.app.pia.module.projects.assessment.module.questions",
        projects: ($scope.templateType === ENUMS.TemplateTypes.Datamapping) ? "zen.app.pia.module.datamap.views.assessment.list" : "zen.app.pia.module.projects.list",
        createForm: ($scope.templateType === ENUMS.TemplateTypes.Datamapping) ? "zen.app.pia.module.datamap.views.assessment.wizard.project_info" : "zen.app.pia.module.projects.wizard.project_info",
        selectForm: ($scope.templateType === ENUMS.TemplateTypes.Datamapping) ? "zen.app.pia.module.datamap.views.assessment.wizard.project_type" : "zen.app.pia.module.projects.wizard.project_type",
        assignment: ($scope.templateType === ENUMS.TemplateTypes.Datamapping) ? "zen.app.pia.module.datamap.views.assessment.wizard.project_assignment" : "zen.app.pia.module.projects.wizard.project_assignment",
    };

    $scope.saveTooltip = "";
    $scope.wizard = {};
    $scope.project = {};
    $scope.SectionAssignments = [];
    $scope.isSubmitted = false;
    $scope.isInvitedUser = false;
    $scope.SaveSettingProjectLeadId = false;
    $scope.mode = $state.params.mode;
    $scope.isTypedEmailDisabledUser = false;

    $scope.placeholder = {
        name: $rootScope.t("EnterProjectName"),
        description: $rootScope.t("EnterProjectDescription"),
        lead: $rootScope.t("AssignProjectRespondent")
    };

    $scope.error = {
        lead: $rootScope.t("PleaseAssignValidProjectRespondent")
    };

    if (Permissions.canShow("AssessmentRespondentReassign") && Permissions.canShow("ProjectsAssignEmails")) {
        $scope.placeholder.lead = $rootScope.t("AssignADefaultRespondentOrInviteViaEmail");
        $scope.error.lead = $rootScope.t("PleaseAssignADefaultRespondent");
    }
    else if (Permissions.canShow("AssessmentRespondentReassign")) {
        $scope.placeholder.lead = $rootScope.t("AssignProjectRespondent");
        $scope.error.lead = $rootScope.t("PleaseAssignValidProjectRespondent");
    } else if (Permissions.canShow("ProjectsAssignEmails")) {
        $scope.placeholder.lead = $rootScope.t("InviteProjectRespondentViaEmail");
        $scope.error.lead = $rootScope.t("ErrorEnterValidEmailAddress");
    }

    $scope.tooltip = {
        lead: $rootScope.t("ProjectRespondentDescription")
    };

    var getProjectSettings = function () {
        Settings.getTagging().then(function (response) {
            if (response.result) {
                $scope.Tagging = response.data;
            }
        });
    };

    var init = function () {
        $scope.loading = true;
        $scope.hasDefaultApproverSettingsPermission = Permissions.canShow("SettingsProjectsReviewer");
        $scope.hasMultiRespondentSettingsPermission = Permissions.canShow("SettingsProjectsSectionAssignment");
        $scope.hasOrgGroupsListPermission = Permissions.canShow("OrgGroupsList");

        $scope.isCreatingProject = false;

        $scope.validReminder = true;
        $scope.saveTooltip = "";
        $scope.orgList = OrgGroupStoreNew.orgList;
        $scope.orgTable = OrgGroupStoreNew.orgTable;
        if (OrgGroupStoreNew.orgList.length === 1) $scope.selectedOrg = OrgGroupStoreNew.selectedOrg;
        $scope.OrganizationTree = OrgGroupStoreNew.orgTree;
        $scope.currentUser = getCurrentUser(store.getState());

        $q.all([
            getSettings(),
            getProjectSettings()
        ]).then(function() {
            if ($scope.mode === "version") {
                Projects.read($state.params.id).then(function(response) {
                    if (response.result) {
                        $scope.project = response.data;
                        secondaryPromise(response.data, true);
                    } else {
                        $scope.loading = false;
                    }
                });
            } else {
                secondaryPromise();
            }
        });
    };

    var setWizardDefaults = function (projectData) {
        var reminder = $scope.Settings.Reminder ? $scope.Settings.Reminder : {
            Show: false,
            Value: 1
        };
        var defaultReviewer = $scope.Settings.DefaultReviewer;
        if ($sessionStorage.wizard) {
            $scope.wizard = $sessionStorage.wizard;
        } else {
            $scope.wizard = {
                ProjectType: $scope.mode,
                SelectedId: $state.params.id || "", // ID of either the Template or the Project
                SelectedVersion: ($scope.mode === "version" ? +$state.params.version + 1 : $state.params.version) || "", // If creating new version, increment by 1
                Project: {
                    Id: "",
                    StateDesc: "Not Started",
                    Name: projectData ? projectData.Name : "",
                    Description: projectData ? projectData.Description : "",
                    OrgGroupId: "",
                    LeadId: $scope.currentUser.Id, // Project Respondent
                    Deadline: null,
                    Reminder: reminder.Value,
                    TemplateId: "",
                    TemplateName: projectData ? projectData.TemplateName : "",
                    Tags: []
                },
                MinDate: new Date(),
                ReminderShow: reminder.Show,
                MultipleRespondentShow: false,
                IsDefault: true
            };
        }

        $scope.$watch("wizard.Project.Reminder", function (newValue, oldValue) {
            $scope.wizard.IsDefault = (newValue === $scope.Settings.Reminder.Value);
        });

        var privacyOfficerName = null;

        if ($scope.wizard.Project.OrgGroupId.length) {
            $scope.selectedOrg = $scope.orgTable[$scope.wizard.Project.OrgGroupId];
        } else {
            if (OrgGroupStoreNew.orgList.length === 1) {
                $scope.wizard.Project.OrgGroupId = $scope.orgList[0].id;
                privacyOfficerName = $scope.orgTable[$scope.orgList[0].id].privacyOfficerName;
            }
        }

        $scope.labels = Labels.getLabels("projectWizard", $scope.templateType, {
            templateName: $scope.wizard.Project.TemplateName,
            privacyOfficerName: privacyOfficerName,
        });

        return true;
    };

    $scope.formatLeadLabel = function (model) {
        if (model) {
            if ($scope.currentUser && $scope.currentUser.Id && ($scope.currentUser.Id === model)) {
                return Users.formatTeamLabel($scope.Team, model);
            }
            if (Utilities.matchRegex(model, ENUMS.Regex.Guid)) {
                return Users.formatTeamLabel($scope.Team, model);
            }
            if (Utilities.matchRegex(model, ENUMS.Regex.Email)) {
                return model;
            }
        }
        return "";
    };

    var secondaryPromise = function (data, boolean) {
        $q.all([
            setWizardDefaults(data),
            getTemplate(boolean),
            getUsers(),
        ]).then(function() {
            setCreateProjectText();
            $scope.loading = false;
        });
    };

    var getTemplate = function (isProjectTemplate, params) {
        if (isUndefined(params)) {
            var template = $scope.project.TemplateId ? $scope.project.TemplateId : $scope.wizard.SelectedId;
            var version = isProjectTemplate ? "" : $scope.wizard.SelectedVersion;
            var params = {
                id: template,
                version: version
            };
        }
        return Templates.readTemplate(params).then(function (response) {
            if (isProjectTemplate) {
                var params = {
                    id: response.data.Published.Id,
                    version: response.data.Published.Version
                };
                getTemplate(null, params);
            } else {
                $scope.template = response.data;
                var assignee = $scope.wizard.Project.LeadId || "";
                updateBulkSectionAssignments(assignee);
            }
        });
    };

    $scope.getRootTree = function() {
        OrgGroups.rootTree().then(function(response) {
            if (response.data) $scope.TreeMap = new TreeMap("Id", "ParentId", "Children", response.data[0]);
        });
    };

    var getUsers = function () {
        if ($scope.wizard && $scope.wizard.Project && $scope.wizard.Project.OrgGroupId) {
            var orgId = $scope.wizard.Project.OrgGroupId;
        } else {
            var orgId = $scope.currentUser.OrgGroupId;
        }

        OrgGroups.getOrgUsersWithPermission(orgId, OrgUserTraversal.Branch, false, [AssessmentPermissions.AssessmentCanBeRespondent]).then(function(res) {
            $scope.disabledUserList = getDisabledUsers(store.getState());
            $scope.projectViewerEmails = map(getFilteredUsers(isProjectViewer)(store.getState()), function(user) { return user.Email; });
            $scope.Team = res.data;
            $scope.TeamPicker = filter($scope.Team, function (user) { return user.Id !== $scope.currentUser.Id; });
            return $q.when();
        });
    };

    var getSettings = function () {
        return SettingStore.fetchProjectSettings().then(function (response) {
            if (response) $scope.Settings = response;
        });
    };

    var isSingleOrg = function (orgTree) {
        if (isArray(orgTree) && orgTree.length) {
            var treeMap = new TreeMap("Id", "ParentId", "Children", orgTree[0]);
            if (treeMap.Size() === 1) {
                return orgTree[0].Id;
            }
        }
        return "";
    };

    $scope.assignToMe = function () {
        $scope.wizard.MultipleRespondentShow = false;
        $scope.wizard.Project.LeadId = $scope.currentUser.Id;
        $sessionStorage.wizard = $scope.wizard;
        $scope.saveTooltip = "and Begin Assessment";
    };

    var setCreateProjectText = function () {
        if ($scope.wizard.MultipleRespondentShow) {
            $scope.createProjectText = $rootScope.t("CreateProject");
        } else if (!$scope.wizard.MultipleRespondentShow) {
            if ($scope.wizard.Project.LeadId === $scope.currentUser.Id) {
                $scope.createProjectText = $rootScope.t("CreateAndBeginProject");
            } else {
                $scope.createProjectText = $rootScope.t("CreateProject");
            }
        }
    };

    $scope.$watch("wizard.Project.LeadId", function (newValue, oldValue) {
        if ($scope.template && !$scope.SaveSettingProjectLeadId && !$scope.wizard.MultipleRespondentShow) {
            var sendArray = [];
            var projectVersion = ($scope.wizard.SelectedVersion && $scope.mode === "version") ? $scope.wizard.SelectedVersion : "1";
            each($scope.template.Sections, function (section, index) {
                var assignment = {
                    AssigneeId: newValue,
                    SectionId: section.Id,
                    ProjectVersion: projectVersion,
                    Comment: $scope.wizard.Project.Comment
                };
                sendArray.push(assignment);
            });
            $scope.SectionAssignments = sendArray;
            setCreateProjectText();
        }
    });

    var updateBulkSectionAssignments = function (newValue) {
        var sections = $scope.template ? $scope.template.Sections : {};
        var projectVersion = ($scope.wizard.SelectedVersion && $scope.mode === "version") ? $scope.wizard.SelectedVersion : "1";
        if (sections) {
            var sendArray = [];
            each(sections, function (section, index) {
                var assignment = {
                    AssigneeId: newValue || "",
                    SectionId: section.Id,
                    ProjectVersion: projectVersion,
                    Comment: $scope.wizard.Project.Comment || ""
                };
                sendArray.push(assignment);
            });
            $scope.SectionAssignments = sendArray;
        }
    };

    $scope.assignToSomeoneElse = function () {
        $scope.wizard.Project.LeadId = "";
        $scope.saveTooltip = "";
        $sessionStorage.wizard = $scope.wizard;
        $scope.isInvitedUser = true;
    };

    $scope.updateDeadline = function(deadline) {
        $scope.wizard.Project.Deadline = deadline;
        $scope.validateReminder();
    };

    $scope.toggleReminder = function () {
        $scope.wizard.ReminderShow = !$scope.wizard.ReminderShow;
        if ($scope.wizard.ReminderShow) {
            $scope.validateReminder();
        } else {
            $scope.validReminder = true;
        }
    };

    $scope.validateReminder = function () {
        if ($scope.wizard.ReminderShow && $scope.wizard.Project.Deadline) {
            var reminderDate = new Date($scope.wizard.Project.Deadline);
            reminderDate.setTime(reminderDate.getTime() - 1000 * 60 * 60 * 24 * $scope.wizard.Project.Reminder);
            var today = new Date();
            $scope.validReminder = (today.toLocaleDateString() !== reminderDate.toLocaleDateString()) && (reminderDate > today);
        } else {
            $scope.validReminder = true;
        }
    };

    $scope.toggleMultipleRespondent = function () {
        $scope.wizard.MultipleRespondentShow = !$scope.wizard.MultipleRespondentShow;

        updateBulkSectionAssignments($scope.wizard.Project.LeadId);
        if (($scope.template.HasCrossSectionConditions || $scope.template.NextTemplateId || $scope.template.Sections.length < 2) && $scope.wizard.MultipleRespondentShow) {
            var msg = $rootScope.t("MultiRespondentIsNotAllowed");
            OneAlert("", msg);
            $scope.wizard.MultipleRespondentShow = false;
        }
        setCreateProjectText();
        $scope.isAssigned = respondentCheck();
    };

    var createProject = function (project, createMethod) {
        var deadline = $scope.wizard.Project.Deadline;
        if (deadline === null || deadline === "") {
            project.Deadline = null;
            $scope.wizard.ReminderShow = false;
        }

        if (!$scope.wizard.ReminderShow) {
            project.ReminderDays = null;
        }

        if (Utilities.matchRegex(project.OrgGroupId, ENUMS.Regex.Guid)) {
            var idIsGuidType = Utilities.matchRegex(project.LeadId, ENUMS.Regex.Guid);
            var idIsEmailAddress = Utilities.matchRegex(project.LeadId, ENUMS.Regex.Email);

            if (idIsGuidType || idIsEmailAddress) {
                project.SectionAssignments = $scope.SectionAssignments;

                createMethod(project).then(function(response) {
                    if (response.result) {
                        delete $sessionStorage.wizard;
                        $scope.wizard.Project.Id = response.data;

                        // Assigning a new invited user creates a new user on backend, therefore we update users reducer with updated user list
                        if (idIsEmailAddress) { UserActionService.fetchUsers(); }

                        if ($scope.wizard.Project.LeadId !== $scope.currentUser.Id || $scope.wizard.MultipleRespondentShow) {
                            $state.go($scope.routes.projects);
                        } else {
                            $state.go($scope.routes.single, {
                                Id: response.data,
                                Version: $scope.mode === "version" ? $scope.wizard.SelectedVersion : 1
                            });
                        }
                    } else {
                        $scope.isCreatingProject = false;
                    }
                });
            } else {
                $scope.wizard.Project.LeadId = "";
                NotificationService.alert($rootScope.t("Warning"), $rootScope.t("PleaseSelectValidProjectRespondent"));
                $scope.isCreatingProject = false;
            }
        } else {
            $scope.wizard.Project.OrgGroupId = "";
            NotificationService.alert($rootScope.t("Warning"), $rootScope.t("PleaseSelectValidOrganizationGroup"));
            $scope.isCreatingProject = false;
        }
    };

    $scope.Save = function () {
        $scope.SaveSettingProjectLeadId = true;
        $scope.isAssigned = respondentCheck();
        $scope.isSubmitted = true;

        // Reject if Respondent email entered is already a Project Viewer's email
        if (includes($scope.projectViewerEmails, $scope.wizard.Project.LeadId)) {
            NotificationService.alertError($rootScope.t("Error"), $rootScope.t("Error.RespondentEmailIsProjectViewer"));
            $scope.isSubmitted = false;
            return;
        }
        // Check if we should save
        if ($scope.isCreatingProject || !$scope.isAssigned) {
            NotificationService.alertError($rootScope.t("Error"), $rootScope.t("CouldNotGetAssignment"));
            $scope.isSubmitted = false;
            return;
        }
        if ($scope.wizard.SelectedId || $scope.wizard.Project.Id) {
            $scope.isCreatingProject = true;
            var project = {
                Name: $scope.wizard.Project.Name,
                Description: $scope.wizard.Project.Description,
                OrgGroupId: $scope.wizard.Project.OrgGroupId,
                LeadId: $scope.wizard.Project.LeadId,
                Deadline: $scope.wizard.Project.Deadline,
                ReminderDays: $scope.wizard.Project.Reminder,
                Comment: $scope.wizard.Project.Comment,
                Tags: $scope.wizard.Project.Tags
            };
            // Check mode to determine whether this is a new project or a new version of old project
            if ($scope.mode === "version") {
                project.ProjectGuid = $scope.wizard.SelectedId;
                createProject(project, Projects.createVersion);
            } else {
                project.TemplateId = $scope.wizard.SelectedId;
                project.TemplateVersion = $scope.wizard.SelectedVersion;
                project.TemplateType = $scope.templateType;
                createProject(project, Projects.create);
            }
        } else {
            $state.go($scope.routes.selectForm);
            NotificationService.alert($rootScope.t("Warning"), $rootScope.t("NoTemplateWasSelected"));
        }
    };

    var respondentCheck = function () {
        var isValid = true;
        each($scope.SectionAssignments, function (assignment) {
            if (assignment.AssigneeId === "" || isUndefined(assignment.AssigneeId)) {
                isValid = false;
            }
        });

        // If lead is empty just set it to the first section respondent. Its a placeholder property because of section assigments.
        if (isValid) {
            if ($scope.wizard.Project.LeadId === "" || isUndefined($scope.wizard.Project.LeadId)) {
                $scope.wizard.Project.LeadId = $scope.SectionAssignments[0].AssigneeId;
            }
        }
        return isValid;
    };

    $scope.cancel = function () {
        $state.go($scope.routes.projects);
    };

    $scope.next = function () {
        $sessionStorage.wizard = $scope.wizard;
        $state.go($scope.routes.assignment, {
            mode: $scope.mode,
            id: $scope.wizard.SelectedId,
            version: $scope.wizard.SelectedVersion
        });
    };

    $scope.previous = function () {
        $state.go($scope.routes.createForm, {
            mode: $scope.mode,
            id: $scope.wizard.SelectedId,
            version: $scope.wizard.SelectedVersion
        });
    };

    $scope.OpenProjectAssignmentsModal = function (sectionId, index) {
        $scope.getRootTree();
        $scope.wizard.Project.assignText = $rootScope.t("Assign");
        var initial = true;
        var modalInstance = $uibModal.open({
            templateUrl: "AssignmentsModal.html",
            controller: "ProjectAssignmentsCtrl",
            scope: $scope,
            backdrop: "static",
            keyboard: false,
            resolve: {
                project: function () {
                    return $scope.wizard.Project;
                },
                Team: function () {
                    return $scope.Team;
                },
                SectionId: function () {
                    return sectionId;
                },
                ScopeContext: function () {
                    return $scope.ScopeContext;
                },
                Section: function () {
                    return {};
                },
                SectionAssignment: function () {
                    return $scope.SectionAssignments[index];
                },
                Initial: function () {
                    return initial;
                }
            }
        });

        modalInstance.result.then(function (response) {
            if (response.result) {
                var index = findSectionAssignment(response.data.SectionId);
                $scope.SectionAssignments[index] = response.data;
                $scope.isAssigned = respondentCheck();
            }
        });
    };

    var findSectionAssignment = function (sectionId) {
        function findWithAttr(array, attr, value) {
            var arrayLength = array.length;
            for (var i = 0; i < arrayLength; i += 1) {
                if (array[i][attr] === value) {
                    return i;
                }
            }
            return -1;
        }
        return findWithAttr($scope.SectionAssignments, "SectionId", sectionId);
    };

    $scope.addTag = function (tag) {
        Tags.addTag(tag).then(function (response) {
            tag.Id = response.data;
        });
    };

    $scope.getTagsList = function (filter, count) {
        return Tags.getTags(filter, count).then(function (response) {
            if (response.result && response.data && response.data.filter) {
                return response.data.filter(filterSelectedTags);
            }
        });
    };

    var filterSelectedTags = function (value) {
        if (getTagIndexByLabel($scope.wizard.Project.Tags, value.Label) > -1) return false;
        return true;
    };

    function getTagIndexByLabel(arr, label) {
        for (var i = 0; i < arr.length; i++) {
            if (arr[i].Label === label) return i;
        }
        return -1;
    };

    $scope.getEmailLeadText = function () {
        return $scope.wizard.Project.LeadId !== $scope.currentUser.Id && !$scope.wizard.Project.Deadline ?
            $rootScope.t("EmailLeadWithCommentToCompleteProject", { ProjectLeadId: $scope.formatLeadLabel($scope.wizard.Project.LeadId), ProjectName: $scope.wizard.Project.Name }) :
            $scope.wizard.Project.Deadline ?
                $rootScope.t("EmailLeadWithCalendarToCompleteProject", { ProjectLeadId: $scope.formatLeadLabel($scope.wizard.Project.LeadId), ProjectName: $scope.wizard.Project.Name }) :
                $rootScope.t("EmailLeadToCompleteProject", { ProjectLeadId: $scope.formatLeadLabel($scope.wizard.Project.LeadId), ProjectName: $scope.wizard.Project.Name });
    };

    $scope.orgSelected = function (action, payload) {
        if (action === "ORG_SELECTED") {
            $scope.selectedOrg = payload.org;
            $scope.wizard.Project.OrgGroupId = payload.org.id;
            return;
        }
    };

    $scope.isTypedUserEmailDisabled = function (typedEmail) {
        var typedDisabledUser = find($scope.disabledUserList, function(specificUser) {
            return (toLower(specificUser.Email) === toLower(typedEmail));
        });
        $scope.isTypedEmailDisabledUser = Boolean(typedDisabledUser);
    };

    init();
}

ProjectsWizardFormCtrl.$inject = ["$scope", "$state", "$q", "$uibModal",
    "$sessionStorage", "$rootScope", "Permissions", "PRINCIPAL", "NotificationService","Settings",
    "Projects", "Templates", "Users", "OrgGroups", "OrgGroupStoreNew", "Tags", "ENUMS", "Labels",
    "store", "SettingStore", "UserActionService"
];
