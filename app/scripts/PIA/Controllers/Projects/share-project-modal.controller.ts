declare var angular: any;
import * as _ from "lodash";
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import { getAllRelatedUsersThatAreViewer } from "oneRedux/reducers/user.reducer";
import { IUser } from "interfaces/user.interface";
import { IStore } from "interfaces/redux.interface";
import { IShareProjectParams } from "interfaces/assignment.interface";

export default class ShareProjectModalCtrl {

    static $inject: string[] = ["store", "$rootScope", "$state", "$scope", "$uibModalInstance", "Project", "Assignments"];

    private translate: any;

    constructor(
        readonly store: IStore,
        readonly $rootScope: IExtendedRootScopeService,
        readonly $state: ng.ui.IStateService,
        readonly $scope: any,
        readonly $uibModalInstance: any,
        readonly Project: any,
        readonly Assignments: any) {

        const NotSharedWithUser = "NOT_SHARED_WITH_USER";
        const SharedWithUser = "SHARED_WITH_USER";

        $scope.translations = {
            advanced: $rootScope.t("Advanced").toString(),
            cancel: $rootScope.t("Cancel").toString(),
            emptyPlaceholder: $rootScope.t("NoAvailableProjectViewersToShareWith").toString(),
            enterNames: $rootScope.t("EnterNames").toString(),
            saveChanges: $rootScope.t("SaveChanges").toString(),
            shareAReadOnlyCopy: $rootScope.t("ShareAReadOnlyCopy").toString(),
            shareProject: $rootScope.t("ShareProject", { ProjectTerm: $rootScope.customTerms.Project }).toString(),
            sharedWith: $rootScope.t("SharedWith").toString(),
        };
        $scope.project = _.clone(Project); // Project object from parent

        // Helpers to build SharedWith and NotSharedWith User arrays
        const sharedWithUserIds: string[] = $scope.project.Assignments.length ?
            $scope.project.Assignments.map((assignment: any): string => assignment.AssigneeId) : [];
        const createSharedWithUsers = (ids: string[], users: any[]): any[] => {
            return _.filter(users, (user: any): boolean => _.includes(ids, user.Id));
        };
        const createNotYetSharedWithUsers = (ids: string[], users: any[]): any[] => {
            return _.filter(users, (user: any): boolean => !_.includes(ids, user.Id));
        };
        const getActiveUsers = (users: IUser[]): IUser[] => {
            return _.filter(users, (user: any): boolean => user.IsActive);
        };
        const userList: IUser[] = getAllRelatedUsersThatAreViewer(this.store.getState());
        // User Lists For Multiselect Dropdowns
        $scope.sharedWithUsers = createSharedWithUsers(sharedWithUserIds, _.clone(userList));
        $scope.notSharedWithUsers = createNotYetSharedWithUsers(sharedWithUserIds, _.clone(userList));
        $scope.notDisabledUsers = getActiveUsers(_.clone($scope.notSharedWithUsers));

        // Models for Multiselect Dropdowns
        $scope.sharedWithUserModel = $scope.sharedWithUsers;
        $scope.notSharedWithUserModel = [];

        $scope.removedSharedWithUsers = []; // Array of Users whose Share access was removed
        $scope.Ready = true; // Display modal content when true
        $scope.multiple = true; // sets one-multiselect component to manage multiple selections
        $scope.isProjectShared = Boolean($scope.sharedWithUserModel.length);
        $scope.isAdvancedButtonClicked = false; // state management for the Advanced button
        $scope.shareButtonDisabled = false;

        const removeShareToUser = (item: any): void => {
            $scope.notSharedWithUserModel = _.remove($scope.notSharedWithUserModel,
                (user: any): boolean => Boolean(user) && user.Id !== item.Id);
        };

        const updateSharedWithUserModel = (item: any): void => {
            $scope.sharedWithUserModel = _.filter($scope.sharedWithUserModel,
                (user: any): boolean => {
                    const userHasSameId: boolean = Boolean(user) && user.Id === item.Id;
                    if (userHasSameId) {
                        $scope.removedSharedWithUsers.push(user);
                        return false;
                    }
                    return true;
                },
            );
        };

        const updateRemovedUsers = (item: any): void => {
            $scope.removedSharedWithUsers = _.filter($scope.removedSharedWithUsers,
                (user: any): boolean => Boolean(user) && user.Id !== item.Id);
            $scope.sharedWithUserModel.push(item);
        };

        const getNewAssigneeIds = (): Object[] => {
            const merged: any[] = _.concat([], $scope.sharedWithUserModel, $scope.notSharedWithUserModel);
            return _.map(merged, "Id");
        };

        $scope.handleSelect = (type: string, $item: any): any => {
            if (type === NotSharedWithUser) {
                $scope.notSharedWithUserModel.push($item);
            } else {
                updateRemovedUsers($item);
            }
        };

        $scope.handleRemove = (type: string, $item: any): any => {
            if (type === NotSharedWithUser) {
                removeShareToUser($item);
            } else {
                updateSharedWithUserModel($item);
            }
        };

        $scope.advancedButtonClicked = (): void => {
            $scope.isAdvancedButtonClicked = true;
        };

        $scope.shareProject = (): void => {
            $scope.isLoading = true;
            const params: IShareProjectParams = {
                ProjectId: $scope.project.Id,
                ProjectVersion: $scope.project.Version,
                Type: 30,
                AssigneeIds: getNewAssigneeIds(),
            };
            Assignments.shareProject(params).then((res: any): void => {
                if (res.result) {
                    $scope.done();
                } else {
                    $scope.isLoading = false;
                }
            });
        };

        $scope.done = (): void => {
            this.$uibModalInstance.close({
                result: false,
                data: 0,
            });
        };

        $scope.cancel = (): void => {
            this.$uibModalInstance.dismiss("cancel");
        };
    }
}
