import { AssessmentStates, AssessmentStateKeys } from "constants/assessment.constants";

export default function ProjectHistoryCtrl($rootScope, $scope, $uibModalInstance, $interval, Projects, Project, ENUMS) {

    $scope.init = function () {
        $scope.GetHistory();
    };

    $scope.Project = Project;
    $scope.isLoading = false;

    $scope.projectStatuses = [{
        Name: $rootScope.t("All"),
        Value: ""
    }, {
        Name: $rootScope.t("InProgress"),
        Value: "In Progress"
    }, {
        Name: $rootScope.t("UnderReview"),
        Value: "Under Review"
    }, {
        Name: $rootScope.t("MoreInfoNeeded"),
        Value: "More Info Needed"
    }, {
        Name: $rootScope.t("RiskAssessment"),
        Value: "Risk Assessment"
    }, {
        Name: $rootScope.t("RiskTreatment"),
        Value: "Risk Treatment"
    }, {
        Name: $rootScope.t("Completed"),
        Value: "Completed"
    }];

    $scope.historyFilter = {
        All: "",
        eventType: $scope.projectStatuses[0].Value
    };

    // Function Get Comments
    $scope.GetHistory = function () {
        $scope.isLoading = true;

        Projects.getHistory({
            Id: Project.Id,
            Version: Project.Version
        }).then(function (response) {
            if (response.result) {
                $scope.projectHistory = response.data;
                _.each($scope.projectHistory.Entries, function (event, index) {
                    // State Labels
                    switch (event.StateDesc) {
                        case AssessmentStates.InProgress:
                            event.StateLabel = "in-progress";
                            event.StateName = $rootScope.t(AssessmentStateKeys.InProgress);
                            break;
                        case AssessmentStates.UnderReview:
                            event.StateLabel = "under-review";
                            event.StateName = $rootScope.t(AssessmentStateKeys.UnderReview);
                            break;
                        case AssessmentStates.InfoNeeded:
                            event.StateLabel = "needs-info";
                            event.StateName = $rootScope.t(AssessmentStateKeys.InfoNeeded);
                            break;
                        case $scope.projectStates.RiskAssessment:
                            event.StateLabel = "risk-assessment";
                            event.StateName = $rootScope.t(AssessmentStateKeys.RiskAssessment);
                            break;
                        case $scope.projectStates.RiskTreatment:
                            event.StateLabel = "risk-treatment";
                            event.StateName = $rootScope.t(AssessmentStateKeys.RiskTreatment);
                            break;
                        case AssessmentStates.Completed:
                            event.StateLabel = "completed";
                            event.StateName = $rootScope.t(AssessmentStateKeys.Completed);
                            break;
                        default:
                            event.StateLabel = "default";
                            event.StateName = event.StateDesc;
                    }
                });
            } else {
                if ($scope.projectHistory) {
                    // If it is already defined, let's not mess with it.
                } else {
                    $scope.projectHistory = [];
                }
            }

            $scope.isLoading = false;
        });
    };

    $scope.init();

    $scope.closeModal = function () {
        $uibModalInstance.close({
            result: false,
            data: ""
        });
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss("cancel");
    };

}

ProjectHistoryCtrl.$inject = ["$rootScope", "$scope", "$uibModalInstance", "$interval", "Projects", "Project", "ENUMS"];
