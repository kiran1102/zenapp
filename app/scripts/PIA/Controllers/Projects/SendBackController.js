
export default function SendBackCtrl($rootScope, $state, $scope, $uibModalInstance, Users, Projects, Project, Labels, TemplateType, Permissions) {

    $scope.Lead = Project.Lead || "the Respondent";
    $scope.labels = Labels.getLabels("sendBackModal", TemplateType);
    $scope.canRespondentEditAnswer = Permissions.canShow("ProjectSendBackToInProgress");
    $scope.AllowRespondentEditing = false;

    $scope.toggleAllowRespondentEditing = function() {
        $scope.AllowRespondentEditing = !$scope.AllowRespondentEditing;
    }

    $scope.sendBack = function (comments) {
        $scope.requestInProgress = true;
        var data = {
            Id: Project.Id,
            ProjectVersion: Project.Version,
            Comments: comments,
            AllowRespondentEditing: $scope.AllowRespondentEditing
        };
        Projects.requestProjectInfo(data).then(
            function (response) {
                if (response.result) {
                    $uibModalInstance.close(response);
                }
                $scope.requestInProgress = false;
            }
        );
    };

    $scope.done = function () {
        $uibModalInstance.close({ result: false, data: "" });
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss("cancel");
    };
}

SendBackCtrl.$inject = ["$rootScope", "$state", "$scope", "$uibModalInstance", "Users", "Projects", "Project", "Labels", "TemplateType", "Permissions"];
