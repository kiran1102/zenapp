import { BrandingActions } from "oneRedux/reducers/branding.reducer";
export default function BrandingSettingsController($scope, $rootScope, Permissions, Branding, PRINCIPAL, store, NotifyService) {
    $scope.Permissions = Permissions;

    $scope.rootOrg = PRINCIPAL.getRootOrg();
    $scope.originalOrgGroup = PRINCIPAL.getOrggroup();

    $scope.InheritTypes = {
        Inherit: true,
        Override: false
    };

    $scope.Ready = false;

    $scope.GetBranding = function () {
        // init our branding
        return Branding.getBranding(true).then(function(brands) {
            $scope.app = _.cloneDeep(brands);
            $scope.originalBranding = _.cloneDeep(brands);
            $scope.suborgInheritText = $rootScope.t("BrandingsuborgInheritText", { Parentorg: $scope.app.InheritsFrom });
            $scope.Ready = true;
        });
    };

    $scope.GetBranding();

    $scope.logoLoading = false;
    $scope.iconLoading = false;

    $scope.showBranding = function (app) {
        // Update branding in the header.
        store.dispatch({type: BrandingActions.SET_BRANDING_DATA, branding: _.cloneDeep(app.branding)});

        // Make sure our image previews are updated, if the views are available. #jquery
        if (!_.isUndefined($("#logo-preview")) && $("#logo-preview").length) {
            $("#logo-preview")[0].src = _.cloneDeep(app.branding.logo.url ? app.branding.logo.url : "");
        }
        if (!_.isUndefined($("#icon-preview")) && $("#icon-preview").length) {
            $("#icon-preview")[0].src = _.cloneDeep(app.branding.header.icon.url ? app.branding.header.icon.url : "");
        }
    };

    // Reset our branding when we leave the page.
    $scope.$on("$destroy", function () {
        if (!_.isEqual($scope.app, $scope.originalBranding) && _.isEqual($scope.originalOrgGroup, PRINCIPAL.getOrggroup())) {
            NotifyService.confirm('', $rootScope.t('UnsavedChangesWouldYouLikeToSave')).then(function(result) {
                if (result) {
                    $scope.saveBranding();
                } else {
                    $scope.showBranding($scope.originalBranding);
                }
            });
        }
    });

    var testForPrefix = function (str, pref) {
        if (!_.isUndefined(str) && str.indexOf(pref) === 0) {
            return str;
        }
        return pref + str;
    };

    $scope.checkLength = function (string) {
        if (!_.isUndefined(string) && string.length > 0) {
            string = string.replace(/^#/, "").toUpperCase();
            if (string.length === 3) {
                string += string;
            }
        }
        string = testForPrefix(string, "#");
        return string;
    };

    var testBrandingPrefixes = function (app) {
        if (!_.isUndefined(app.branding.header)) {
            app.branding.header.backgroundColor = testForPrefix(app.branding.header.backgroundColor, "#");
            app.branding.header.textColor = testForPrefix(app.branding.header.textColor, "#");
        }
        return app;
    };

    $scope.defaultBranding = function() {
        $scope.app = _.cloneDeep(Branding.getZenDefault());
        $scope.showBranding($scope.app);
    };

    $scope.resetBranding = function() {
        $scope.app = _.cloneDeep($scope.originalBranding);
        $scope.showBranding($scope.app);
    };

    $scope.testBranding = function() {
        $scope.app = testBrandingPrefixes($scope.app);
        $scope.showBranding($scope.app);
    };

    var inheritBranding = function() {
        if ($scope.app.IsInherited !== $scope.originalBranding.IsInherited) {
            Branding.deleteFromServer().then(function (response) {
                if (!response.result) return;
                $scope.GetBranding().then(function () {
                    $scope.showBranding($scope.app);
                });
            });
        }
    };

    var saveNewBranding = function () {
        $scope.app = testBrandingPrefixes($scope.app);
        Branding.setBranding($scope.app).then(function(brands) {
            $scope.originalBranding = _.cloneDeep($scope.app);
            $scope.showBranding($scope.app);
        });
    };

    $scope.saveBranding = function () {
        if ($scope.app.IsInherited) {
            inheritBranding();
            return;
        }
        saveNewBranding();
    };

    $scope.settings = {
        branding: {
            header: {
                backgroundColor: {
                    position: "bottom right",
                    defaultValue: "",
                    animationSpeed: 50,
                    animationEasing: "swing",
                    change: null,
                    changeDelay: 0,
                    control: "hue",
                    hide: null,
                    hideSpeed: 100,
                    inline: false,
                    letterCase: "uppercase",
                    opacity: false,
                    show: null,
                    showSpeed: 100
                },
                textColor: {
                    position: "bottom right",
                    defaultValue: "",
                    animationSpeed: 50,
                    animationEasing: "swing",
                    change: null,
                    changeDelay: 0,
                    control: "hue",
                    hide: null,
                    hideSpeed: 100,
                    inline: false,
                    letterCase: "uppercase",
                    opacity: false,
                    show: null,
                    showSpeed: 100
                }
            }
        }
    };
}

BrandingSettingsController.$inject = ["$scope", "$rootScope", "Permissions", "Branding", "PRINCIPAL", "store", "NotifyService"];
