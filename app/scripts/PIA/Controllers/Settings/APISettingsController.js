
"use strict";

export default function APISettingsController($scope, Permissions, Settings, PRINCIPAL, $rootScope, NotifyService) {
    $scope.tenantId = PRINCIPAL.getTenant();
    $scope.Permissions = Permissions;
    $scope.API = { enable: false };

    $scope.Ready = false;

    var init = function () {
        Settings.getAPI($scope.tenantId)
            .then(function (response) {
                if (response.result && response.data !== null) {
                    $scope.API.key = response.data;
                    $scope.API.enable = !!(response.data);
                }
                $scope.Ready = true;
            });
    }();

    var activateAPI = function (tenantId) {
        Settings.activateAPI(tenantId)
            .then(function (response) {
                if (response.result) {
                    $scope.API.key = response.data;
                    $scope.API.enable = true;
                }
            });
    };

    var deactivateAPI = function (tenantId) {
        NotifyService.confirm('', $rootScope.t('SureToDisableYourApi')).then(
            function (result) {
                if (result) {
                    Settings.deactivateAPI(tenantId).then(
                        function (response) {
                            if (response.result) {
                                $scope.API.key = "";
                                $scope.API.enable = false;
                            }
                        }
                    );
                }
            }
        );
    };

    $scope.toggleAPI = function () {
        if (!$scope.API.enable) {
            activateAPI($scope.tenantId);
        } else {
            deactivateAPI($scope.tenantId);
        }
    };


}

APISettingsController.$inject = ["$scope", "Permissions", "Settings", "PRINCIPAL", "$rootScope", "NotifyService"];
