
"use strict";

export default function TaggingSettingsController($scope, Permissions, Settings, $rootScope) {
    $scope.disableSave = true;
    $scope.Permissions = Permissions;
    $scope.Ready = false;

    $scope.GetTaggingSettings = function () {
        Settings.getTagging().then(
            function (response) {
                if (response.result) {
                    $scope.Tagging = response.data;
                    $scope.BackupTagging = _.cloneDeep(response.data);
                }
                $scope.CanSave();
                $scope.Ready = true;
            }
        );
    };

    $scope.CanSave = function () {
        if ($scope.Tagging && !(_.isEqual($scope.Tagging, $scope.BackupTagging))) {
            $scope.disableSave = false;
        } else {
            $scope.disableSave = true;
        }
    };

    $scope.GetTaggingSettings();

    $scope.SaveTaggingSettings = function () {
        $scope.disableSave = true;
        Settings.updateTagging($scope.Tagging)
            .then(function (response) {
                if (response.result) {
                    $scope.BackupTagging = _.cloneDeep($scope.Tagging);
                }
                $scope.disableSave = false;
                $scope.CanSave();
            });
    };

    $scope.toggleTagging = function (bool) {
        $scope.Tagging.ProjectTaggingEnabled = bool;
        $scope.CanSave();
    };

    $scope.toggleQuestionTagging = function (bool) {
        $scope.Tagging.QuestionTaggingEnabled = bool;
        $scope.CanSave();
    };

}

TaggingSettingsController.$inject = ["$scope", "Permissions", "Settings", "$rootScope"];
