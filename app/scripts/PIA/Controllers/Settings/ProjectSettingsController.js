import * as _ from "lodash";

export default function ProjectSettingsController($scope, Permissions, Settings, Projects, $rootScope, SettingStore, NotifyService) {

    $scope.Ready = false;
    $scope.Permissions = Permissions;

    SettingStore.fetchProjectSettings(true).then(function(response) {
        $scope.Project = response;
        $scope.BackupProject = _.cloneDeep(response);
        $scope.disableSave = true;
        $scope.Ready = true;
    });

    $scope.saveProjectSettings = function () {
        $scope.disableSave = true;
        SettingStore.saveSettings("ProjectSettings", $scope.Project).then(function (res) {
            if (res) {
                $scope.BackupProject = _.assign({}, $scope.Project);
                $scope.disableSave = true;
            } else {
                $scope.disableSave = false;
            }
        });
    };

    $scope.changeReminderDays = function (numOfDays) {
        _.assign($scope.Project.Reminder, { Value: numOfDays });
        $scope.disableSave = _.isEqual($scope.Project, $scope.BackupProject);
    };

    $scope.toggleReminder = function (bool) {
        _.assign($scope.Project.Reminder, { Show: bool });
        $scope.disableSave = _.isEqual($scope.Project, $scope.BackupProject);
    };

    $scope.toggleReviewer = function (bool) {
        _.assign($scope.Project, { DefaultReviewer: bool });
        $scope.disableSave = _.isEqual($scope.Project, $scope.BackupProject);
    };

    $scope.toggleApproval = function (bool) {
        _.assign($scope.Project, { ProjectLevelApproval: bool });
        $scope.disableSave = _.isEqual($scope.Project, $scope.BackupProject);
    };

    $scope.toggleVersionEnabled = function (bool) {
        _.assign($scope.Project, { IsVersioningEnabled: bool });
        $scope.disableSave = _.isEqual($scope.Project, $scope.BackupProject);
    };

    $scope.toggleMultiRespondEnabled = function (bool) {
        _.assign($scope.Project, { SectionAssignment: bool });
        $scope.disableSave = _.isEqual($scope.Project, $scope.BackupProject);
    };

    $scope.toggleThresholds = function (bool) {
        _.assign($scope.Project, { EnableThresholdTemplates: bool });
        $scope.disableSave = _.isEqual($scope.Project, $scope.BackupProject);
    };

    $scope.toggleTagging = function (bool) {
        _.assign($scope.Project, { TaggingEnabled: bool });
        $scope.disableSave = _.isEqual($scope.Project, $scope.BackupProject);
    };

    $scope.toggleQuestionTagging = function (bool) {
        _.assign($scope.Project, { QuestionTaggingEnabled: bool });
        $scope.disableSave = _.isEqual($scope.Project, $scope.BackupProject);
    };

    $scope.toggleSelfService = function (bool) {
        _.assign($scope.Project, { SelfServiceEnabled: bool });
        $scope.disableSave = _.isEqual($scope.Project, $scope.BackupProject);
    };

    $scope.RemoveStockData = function () {
        var confirmMessage = $rootScope.t("AreYouSureToDeleteAllSampleProjects");
        NotifyService.confirm('', confirmMessage).then(function (result) {
            if (result) {
                $scope.DeletingStock = true;
                $scope.disableSave = true;
                Projects.removeSeededData().then(function (response) {
                    if (response.result) {
                        $scope.Project.HasSeeded = false;
                    }
                    $scope.disableSave = false;
                    $scope.DeletingStock = false;
                });
            }
        });
    };
}

ProjectSettingsController.$inject = ["$scope", "Permissions", "Settings", "Projects", "$rootScope", "SettingStore", "NotifyService"];
