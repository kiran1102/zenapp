import Utilities from "Utilities";
import * as _ from "lodash";
import * as angular from "angular";

import { Permissions } from "modules/shared/services/helper/permissions.service";
import { IProjectSettings, IRiskSettings } from "interfaces/setting.interface";
import { IProtocolPacket } from "interfaces/protocol.interface";
import { IRiskPrettyNames } from "interfaces/risk.interface";
import { SettingActionService } from "settingsModule/services/actions/setting-action.service";

export default class RisksSettingsController {
    static $inject: string[] = ["$scope", "Settings", "SettingStore", "Projects", "HeatmapSettings", "ENUMS", "Permissions", "SettingAction"];

    private RiskPrettyNames: IRiskPrettyNames;
    private disableSave = true;

    constructor(
        private readonly $scope: any,
        private readonly Settings: any,
        private readonly SettingStore: any,
        private readonly Projects: any,
        private readonly HeatmapSettings: any,
        ENUMS: any,
        private permissions: Permissions,
        private readonly SettingAction: SettingActionService,
    ) {
        this.RiskPrettyNames = ENUMS.RiskPrettyNames;
        this.$scope.Permissions = permissions;

        this.init();
    }

    protected GetRiskSettings(): any {
        this.$scope.Ready = false;
        this.SettingStore.fetchProjectSettings().then((response: IProjectSettings): void => {
            if (response) {
                this.SettingAction.addProjectSettings(response);
                this.$scope.Project = response;
                this.$scope.BackupProject = _.cloneDeep(response);
            }
            this.CanSave();
        });

        if (this.permissions.canShow("RiskHeatMapSettings")) {
            this.HeatmapSettings.getHeatmap().then((response: IProtocolPacket): void => {
                if (response.result) {
                    this.SettingAction.addRiskSettings(response.data);
                    this.$scope.heatmap = response.data;
                    this.applyRiskLabels();
                }
                this.$scope.Ready = true;
                this.disableSave = false;
            });
        } else {
            this.$scope.Ready = true;
        }
    }

    private init(): any {
        this.GetRiskSettings();

        this.$scope.toggleAnalysis = (bool: boolean): void => {
            this.$scope.Project.EnableRiskAnalysis = bool;
            this.CanSave();
        };

        this.$scope.SaveRiskSettings = (): void => {
            this.disableSave = true;
            this.SettingStore.saveSettings("ProjectSettings", this.$scope.Project).then((res: IProtocolPacket): void => {
                if (res.result) {
                    this.$scope.BackupProject = _.cloneDeep(this.$scope.Project);
                }
                this.disableSave = false;
                this.CanSave();
            });

            if (this.permissions.canShow("RiskHeatMapSettings")) {
                this.HeatmapSettings.setHeatmap(this.$scope.heatmap).then((res: IProtocolPacket): void => {
                    if (res.result) {
                        this.SettingAction.addRiskSettings(this.$scope.heatmap);
                    }
                    this.disableSave = false;
                });
            }
        };

        this.$scope.ResetRiskSettings = (): void => {
            this.$scope.heatmap = this.HeatmapSettings.getDefault();
            this.applyRiskLabels();
        };

        this.$scope.addToAxis = (axis: any): void => {
            this.$scope.heatmap.GraphSettings.Axes[axis].Points.push({
                Label: "",
                Value: (this.$scope.heatmap.GraphSettings.Axes[axis].Points.length + 1),
            });
        };

        this.$scope.removeFromAxis = (axis: any, index: number): void => {
            this.$scope.heatmap.GraphSettings.Axes[axis].Points.splice(index, 1);
        };

        this.$scope.editLabel = (axis: any): void => {
            this.$scope.heatmap.GraphSettings.Axes[axis].editing = true;
        };

        this.$scope.saveLabel = (axis: any): void => {
            this.$scope.heatmap.GraphSettings.Axes[axis].editing = false;
        };

    }

    private CanSave(): any {
        if (this.$scope.Project && !(_.isEqual(this.$scope.Project, this.$scope.BackupProject))) {
            this.disableSave = false;
        } else {
            this.disableSave = true;
        }
    }

    private applyRiskLabels(): any {
        const isAnArray = (_.isArray(this.$scope.heatmap.RiskMap) && this.$scope.heatmap.RiskMap.length);
        if (isAnArray) {
            _.each(this.$scope.heatmap.RiskMap, (tile: any): void => {
                tile.RiskLabel = `${this.RiskPrettyNames[tile.RiskLevel]} Risk`;
            });
        }
    }

}
