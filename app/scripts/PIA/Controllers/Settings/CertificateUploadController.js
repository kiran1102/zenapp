
"use strict";

export default function CertificateUploadController($scope, $uibModalInstance, Settings) {

    $scope.Disabled = false;

    $scope.certificateFile = {
        FileName: "",
        Extension: "",
        Data: "",
        Password: ""
    };

    $scope.certificateSelected = function (files) {
        if (_.isArray(files) && files.length) {
            $scope.certificateFile.FileName = files[0].FileName;
            $scope.certificateFile.Extension = files[0].Extension;
            $scope.certificateFile.Data = files[0].Data;
            $scope.certificateFile.Password = "";
        }
    };

    $scope.uploadCertificate = function () {
        $scope.Disabled = true;
        Settings.addCertificate($scope.certificateFile).then(function (response) {
            if (response.result) {
                $uibModalInstance.close(response);
            }
            $scope.Disabled = false;
        });
    };

    $scope.cancelCertificateModal = function () {
        $uibModalInstance.dismiss("cancel");
    };

}

CertificateUploadController.$inject = ["$scope", "$uibModalInstance", "Settings"];
