import * as _ from "lodash";

export default class GeneralSettingsController {

    static $inject: string[] = ["$scope", "Tenants"];

    constructor(private readonly $scope: any, private readonly Tenants: any) {
        this.init();
    }

    private init(): void {
        this.$scope.disableSave = false;
        this.$scope.Ready = false;

        this.Tenants.getCurrent().then((tenant: any): void => {
            this.$scope.tenant = tenant.data;
            this.$scope.Ready = true;
        });

        this.$scope.updateTenant = (tenant: any): void => {
            this.$scope.disableSave = true;

            if (!_.isUndefined(tenant)) {
                this.Tenants.updateTenant(tenant).then((response: any): void => {
                    this.$scope.disableSave = false;
                });
            }
        };
    }

}
