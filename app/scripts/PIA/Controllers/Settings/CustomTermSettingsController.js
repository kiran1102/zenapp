
"use strict";

export default function CustomTermSettingsController($scope, Permissions, TenantTranslationService, PRINCIPAL) {
    $scope.Permissions = Permissions;

    var tenantId = PRINCIPAL.getTenant();
    var language = TenantTranslationService.getCurrentLanguage();

    function _GetCustomeTerms(tenantId, language) {
        TenantTranslationService.getCustomTerms(tenantId, language).then(function (response) {
            $scope.customTerms = _.map(response.data, function (t) {
                return {
                    TermKey: t.TermKey,
                    DefaultTermName: t.DefaultTermName,
                    CustomTermName: t.CustomTermName || t.DefaultTermName
                };
            });
        });
    }

    $scope.changeLanguage = function (languageSelected) {
        _GetCustomeTerms(tenantId, languageSelected);
    };

    $scope.saveTerm = function (term, reset) {
        if (!term.CustomTermName) {
            return;
        }
        term.TenantId = tenantId;
        term.Language = $scope.languageSelected || language;
        TenantTranslationService.saveCustomTerm(term, reset);
    };

    $scope.setToDefault = function (term) {
        term.CustomTermName = term.DefaultTermName;
        $scope.saveTerm(term, true);
    };

    // Get all data required for the UI.
    function init() {
        $scope.languages = TenantTranslationService.getLanguages();
        $scope.languageSelected = TenantTranslationService.getCurrentLanguage();
        $scope.customTerms = [];
        _GetCustomeTerms(tenantId, language);
    }

    init();

}

CustomTermSettingsController.$inject = ["$scope", "Permissions", "TenantTranslationService", "PRINCIPAL"];
