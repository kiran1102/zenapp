import { clone } from "lodash";

import { ExportTypes } from "enums/export-types.enum";

import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";
import { ISamlSettings } from "interfaces/setting.interface";

import { Permissions } from "modules/shared/services/helper/permissions.service";

export default class DirectorySettingsController {

    static $inject: string[] = ["Permissions", "$scope", "$rootScope", "ModalService", "Settings", "ENUMS", "NotifyService", "Export"];

    constructor(
        permissions: Permissions,
        private $scope: any,
        private $rootScope: IExtendedRootScopeService,
        private ModalService: any,
        private Settings: any,
        private ENUMS: any,
        private NotifyService: any,
        private Export: any,
    ) {
        this.$scope.Permissions = permissions;
    }

    private $onInit(): void {
        this.$scope.Ready = false;

        this.$scope.BindingTypes = this.ENUMS.bindingTypes;
        this.$scope.CertificateProviders = this.ENUMS.certificateProviders;
        this.$scope.DirectoryProviders = this.ENUMS.directoryProviders;

        this.$scope.Directory = {
            Id: "",
            IsEnabled: false,
            Provider: this.$scope.DirectoryProviders.SAML,
            SericeProviderName: "",
            IdentityProviderName: "",
            RequestBindingTypeId: "",
            ResponseBindingTypeId: "",
            IdentityProviderSignOnUrl: "",
            IdentityProviderSignOutUrl: "",
            IdpCertificate: "",
            SpCertificate: "",
            AttributeMappings: "",
            Hosts: [],
        };

        this.$scope.original = clone(this.$scope.Directory);

        this.$scope.getDirectory = (): IProtocolResponse<ISamlSettings> =>
            this.Settings.getDirectory().then((response: IProtocolResponse<ISamlSettings>): void => {
                if (response.result && response.data) {
                    const samlSettigns: ISamlSettings = response.data;

                    this.$scope.Directory = {
                        Provider: this.$scope.DirectoryProviders.SAML,
                        Id: samlSettigns.Id,
                        IsEnabled: samlSettigns.IsEnabled,
                        SericeProviderName: samlSettigns.SericeProviderName || "",
                        IdentityProviderName: samlSettigns.IdentityProviderName || "",
                        RequestBindingTypeId: samlSettigns.RequestBindingTypeId || "",
                        IdentityProviderSignOnUrl: samlSettigns.IdentityProviderSignOnUrl || "",
                        IdentityProviderSignOutUrl: samlSettigns.IdentityProviderSignOutUrl || "",
                        ResponseBindingTypeId: samlSettigns.ResponseBindingTypeId || "",
                        SpAssertionUrl: samlSettigns.SpAssertionUrl || "",
                        IdpCertificate: samlSettigns.IdpCertificate,
                        SpCertificate: samlSettigns.SpCertificate,
                        AttributeMappings: samlSettigns.AttributeMappings,
                        Hosts: samlSettigns.Hosts || [],
                    };

                    this.$scope.original = clone(this.$scope.Directory);

                }

                this.$scope.Ready = true;
            });

        this.$scope.getDirectory();

        const createSettings = (): ISamlSettings | any | null => {
            switch (this.$scope.Directory.Provider) {
                case this.$scope.DirectoryProviders.Basic:
                case this.$scope.DirectoryProviders.LDAP:
                    return {};
                case this.$scope.DirectoryProviders.SAML:
                    return {
                        Id: this.$scope.Directory.Id,
                        IsEnabled: this.$scope.Directory.IsEnabled,
                        IdentityProviderName: this.$scope.Directory.IdentityProviderName,
                        RequestBindingTypeId: this.$scope.Directory.RequestBindingTypeId,
                        IdentityProviderSignOnUrl: this.$scope.Directory.IdentityProviderSignOnUrl,
                        IdentityProviderSignOutUrl: this.$scope.Directory.IdentityProviderSignOutUrl,
                        ResponseBindingTypeId: this.$scope.Directory.ResponseBindingTypeId,
                        IdpCertificateId: this.$scope.Directory.IdpCertificate ? this.$scope.Directory.IdpCertificate.Id : "",
                        SpCertificateId: this.$scope.Directory.SpCertificate ? this.$scope.Directory.SpCertificate.Id : "",
                        Hosts: this.$scope.Directory.Hosts || null,
                        AttributeMappings: this.$scope.Directory.AttributeMappings || null,
                    };
                default:
                    return null;
            }
        };

        this.$scope.addSamlSettings = (): void => {
            if (!this.$scope.Directory.IsEnabled && (this.$scope.Directory.IsEnabled === this.$scope.original.IsEnabled)) {
                // Based on the Provider, set the data we need, and reset any other data to original.
                const settings = createSettings();

                if (settings) {
                    return this.Settings.setSamlSettings(settings).then((response: any): void => {
                        if (response.result) this.$scope.original = clone(this.$scope.Directory);
                    });
                }
            } else {
                const should_continue_message: string = this.$scope.Directory.IsEnabled ? this.$rootScope.t("ToContinueConvertingLocalUserToSSO") : this.$rootScope.t("ToContinueConvertingSSOUserToLocal");

                this.NotifyService.confirm("", should_continue_message).then((result: any): any => {
                    if (result) {
                        const settings = createSettings();

                        if (settings) {
                            return this.Settings.setSamlSettings(settings).then((response: any): void => {
                                if (response.result) {
                                    if (this.$scope.Directory.Id === null) {
                                        this.$scope.getDirectory();
                                    } else {
                                        this.$scope.original = clone(this.$scope.Directory);
                                    }
                                }
                            });
                        }
                    }
                });
            }
        };

        this.$scope.resetDirectory = (): void => {
            this.$scope.Directory = clone(this.$scope.original);
        };

        this.$scope.setEnable = (enable: boolean): void => {
            this.$scope.Directory.IsEnabled = enable;
        };

        this.$scope.setBindingType = (bindingTypeId: number, isRequest: boolean): void => {
            (isRequest) ? this.$scope.Directory.RequestBindingTypeId = bindingTypeId : this.$scope.Directory.ResponseBindingTypeId = bindingTypeId;
        };

        this.$scope.downloadLogs = (): ng.IPromise<void> => {
            return this.Export.downloadFile("/Report/ExportSamlLogs", ExportTypes.XLSX, null, this.$rootScope.t("SorryErrorWhileRetrievingLogs"));
        };

        this.$scope.UploadedCertificateProvider = "";

        this.$scope.certificateFile = {
            FileName: "",
            Extension: "",
            Data: "",
            Password: "",
        };

        this.$scope.openCertificateUploadtModal = (uploadedCertificateProvider: any): void => {
            this.$scope.UploadedCertificateProvider = uploadedCertificateProvider;

            const modalInstance = this.ModalService.renderModal({}, "CertificateUploadController", "CertificateUploadModal.html", "certificateUploadModal", "lg");

            const destroyCertificateUploadModal = this.$scope.$root.$on("certificateUploadCb", (event: ng.IAngularEvent, args: any): void => {
                if (args.response) {
                    if (this.$scope.UploadedCertificateProvider === this.$scope.CertificateProviders.ServiceProvider) {
                        this.$scope.Directory.SpCertificate = args.response.data.Content;
                    } else if (this.$scope.UploadedCertificateProvider === this.$scope.CertificateProviders.IdentityProvider) {
                        this.$scope.Directory.IdpCertificate = args.response.data.Content;
                    }
                }
            });
        };

        this.$scope.clearCertificate = (uploadedCertificateProvider: any): void => {
            if (uploadedCertificateProvider === this.$scope.CertificateProviders.ServiceProvider) {
                this.$scope.Directory.SpCertificate = "";
            } else if (uploadedCertificateProvider === this.$scope.CertificateProviders.IdentityProvider) {
                this.$scope.Directory.IdpCertificate = "";
            }
        };

        this.$scope.addHost = (): void => {
            this.$scope.newHost = {
                Id: null,
                Name: null,
                Description: null,
            };
            this.$scope.Directory.Hosts.push(this.$scope.newHost);
        };

        this.$scope.removeHost = (index: number): void => {
            this.$scope.Directory.Hosts.splice(index, 1);
        };

    }
}
