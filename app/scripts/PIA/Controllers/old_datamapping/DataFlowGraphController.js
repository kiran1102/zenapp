import * as d3 from "d3";

export default function DataFlowGraphController($scope, Settings, Projects, DataMapping) {
    $scope.disableSave = true;
    $scope.Ready = false;

    // $scope.DataFlowGraph = function() {
    //     DataMapping.getGraphDataFlow().then(
    //         function(response) {
    //             if (response.result) {
    //                 // $scope.crossBorderGraphData = response.data;
    //                 $("#data-flow-graph").append(response.data);
    //                 $("#data-flow-graph").html($("#data-flow-graph").html());
    //                 $scope.Ready = true;

    //                 // var getInventories = [
    //                 //     GetDataInventoryElements(),
    //                 //     GetDataInventoryAssets()
    //                 // ]

    //                 // $q.all(getInventories).then(function() {
    //                 //     var data = {
    //                 //         DataElements: $scope.DataElements,
    //                 //         Assets: $scope.Assets
    //                 //     }
    //                 //     createCrossBorderControls(data);
    //                 // })
    //             }
    //         }
    //     );
    // };

    // $scope.DataFlowGraph();


    $scope.DataFlowGraph = function () {
        var jsonData = {
            "name": "Customer",
            "children": [{
                "name": "Company Marking Website",
                "AssetRisk": true,
                "DataElementRisk": "3",
                "description": "Interactive authoring tools",
                "children": [{
                    "name": "Salesforce",
                    "description": "Web-based 'cloud' applications for authoring data visualisations",
                    "AssetRisk": true,
                    "DataElementRisk": "3",
                    "children": [{
                        "name": "(Host Gator)",
                        "description": "An open-source platform for publishing charts on the web. Cloud-based or self-hosted.",
                        "url": "#",
                        "AssetRisk": true,
                        "DataElementRisk": true
                    }, {
                        "name": "MailChimp",
                        "description": "Spreadsheet in the cloud with charting",
                        "AssetRisk": true,
                        "DataElementRisk": true
                    }, {
                        "name": "(Azure (EU))",
                        "description": "Cloud-based interactive tool for creating data visualisations",
                        "url": "#",
                        "AssetRisk": "3",
                        "DataElementRisk": "3"
                    }, {
                        "name": "Constant Contact",
                        "description": "Open-source interactive tool for creating and exporting D3-like charts",
                        "url": "#",
                        "AssetRisk": true,
                        "DataElementRisk": true
                    }]
                }, {
                    "name": "Host Gator",
                    "children": [{
                        "name": "Google Analytics",
                        "description": "Powerful tool for data analytics and visualisation",
                        "url": "#"
                    }, {
                        "name": "Office 365",
                        "description": "Free version of Tableau Desktop where charts are public",
                        "url": "#",
                        "AssetRisk": true,
                        "DataElementRisk": true
                    }]
                }]
            }, {
                "name": "Company Application",
                "description": "Code-based data visualisation creation",
                "AssetRisk": "1",
                "DataElementRisk": "3",
                "children": [{
                    "name": "Azure (EU)",
                    "description": "The language behind most (all?) browser-based data visualisations",
                    "AssetRisk": "3",
                    "DataElementRisk": "0",
                    "children": [{
                        "name": "New Relic",
                        "description": "Off-the-shelf pre-designed charts. Easy to use but less flexible.",
                        "AssetRisk": true,
                        "DataElementRisk": "0",
                        "children": [{
                            "name": "Heroku",
                            "description": "A good selection of charts including bar, line, scatter, geo, pie, donut, org etc.",
                            "url": "#",
                            "AssetRisk": true,
                            "DataElementRisk": true
                        }, {
                            "name": "Github Pages",
                            "description": "A well maintained commercial library of commonly used chart types",
                            "url": "#"
                        }, {
                            "name": "Jot Form",
                            "description": "A lovely selection of charts including bar, pie, sunburst, icicle, network, trees etc.",
                            "url": "#",
                            "AssetRisk": true,
                            "DataElementRisk": true
                        }, {
                            "name": "Wordpress Blog",
                            "description": "Libraries for visualising geographic data",
                            "AssetRisk": "0",
                            "DataElementRisk": "0"
                        }]
                    }, {
                        "name": "Browser Cookie",
                        "description": "For maximum flexibility, custom coding is the way to go. These libraries will lend a hand.",
                        "AssetRisk": "3",
                        "DataElementRisk": "3",
                        "children": [{
                            "name": "(Company Application)",
                            "description": "A general purpose drawing library with good browser support",
                            "url": "#",
                            "AssetRisk": "1",
                            "DataElementRisk": "3"
                        }]
                    }]
                }, {
                    "name": "Azure (US)",
                    "description": "Non-JavaScript languages for producing web-based data visualisations",
                    "AssetRisk": "1",
                    "DataElementRisk": "1",
                    "children": [{
                        "name": "New Relic",
                        "description": "Python's a very popular language in data science and is a pleasant language to learn and use",
                        "AssetRisk": true,
                        "DataElementRisk": true
                    }, {
                        "name": "Browser Cookie",
                        "description": "Very popular language for data science",
                        "AssetRisk": "3",
                        "DataElementRisk": "1",
                        "children": [{
                            "name": "(Company Application)",
                            "description": "A platform for producing web applications using R",
                            "url": "#",
                            "AssetRisk": "1",
                            "DataElementRisk": "1"
                        }]
                    }]
                }]
            }]
        };

        var m = [20, 20, 20, 120],
            w = 1280 - m[1] - m[3],
            h = 800 - m[0] - m[2],
            i = 0,
            root;

        var tree = d3.layout.tree()
            .size([h, w]);

        var diagonal = d3.svg.diagonal()
            .projection(function (d) {
                return [d.y, d.x];
            });

        var vis = d3.select("#data-flow-graph").append("svg:svg")
            // .attr("width", w + m[1] + m[3])
            // .attr("height", h + m[0] + m[2])
            .attr("viewBox", "0 0 " + w + " " + h + "")
            .append("svg:g")
            .attr("transform", "translate(" + m[3] + "," + m[0] + ")");

        root = jsonData;
        root.x0 = h / 2;
        root.y0 = 0;

        function toggleAll(d) {
            if (d.children) {
                _.each(d.children, toggleAll);
                toggle(d);
            }
        }

        // Initialize the display to show a few nodes.
        // root.children.forEach(toggleAll);
        // toggle(root.children[1]);
        // toggle(root.children[1].children[2]);
        // toggle(root.children[9]);
        // toggle(root.children[9].children[0]);

        update(root);

        function update(source) {
            var duration = d3.event && d3.event.altKey ? 5000 : 500;

            // Compute the new tree layout.
            var nodes = tree.nodes(root).reverse();

            // Normalize for fixed-depth.
            _.each(nodes, function(d) {
                d.y = d.depth * 180;
            });

            // Update the nodes…
            var node = vis.selectAll("g.node")
                .data(nodes, function (d) {
                    return d.id || (d.id = ++i);
                });

            // Enter any new nodes at the parent's previous position.
            var nodeEnter = node.enter().append("svg:g")
                .attr("class", "node")
                .attr("transform", function (d) {
                    return "translate(" + source.y0 + "," + source.x0 + ")";
                })
                .on("click", function (d) {
                    toggle(d);
                    update(d);
                });

            nodeEnter.append("svg:circle")
                .attr("r", 1e-6)
                .style("fill", function (d) {
                    return d._children ? "lightsteelblue" : "#fff";
                });

            nodeEnter.append("a")
                .attr("xlink:href", function (d) {
                    return d.url;
                })
                .append("svg:text")
                .attr("x", function (d) {
                    return d.children || d._children ? -10 : 10;
                })
                .attr("dy", ".35em")
                .attr("text-anchor", function (d) {
                    return d.children || d._children ? "end" : "start";
                })
                .text(function (d) {
                    return d.name;
                })
                .style("fill", function (d) {
                    var color = "#ccc";
                    switch (d.AssetRisk) {
                        case "0":
                            color = "#5bc0de";
                            break;
                        case "1":
                            color = "#f0ad4e";
                            break;
                        case "2":
                            color = "#d9534f";
                            break;
                        case "3":
                            color = "#961512";
                            break;
                        default:
                    }
                    return color;
                })
                .style("fill-opacity", 1e-6);

            nodeEnter.append("svg:title")
                .text(function (d) {
                    return d.description;
                });

            // Transition nodes to their new position.
            var nodeUpdate = node.transition()
                .duration(duration)
                .attr("transform", function (d) {
                    return "translate(" + d.y + "," + d.x + ")";
                });

            nodeUpdate.select("circle")
                .attr("r", 6)
                .style("fill", function (d) {
                    var color = "";
                    switch (d.AssetRisk) {
                        case "0":
                            color = "#5bc0de";
                            break;
                        case "1":
                            color = "#f0ad4e";
                            break;
                        case "2":
                            color = "#d9534f";
                            break;
                        case "3":
                            color = "#961512";
                            break;
                        default:
                    }
                    return color;
                })
                .style("stroke", function (d) {
                    var color = "#ccc";
                    switch (d.AssetRisk) {
                        case "0":
                            color = "#5bc0de";
                            break;
                        case "1":
                            color = "#f0ad4e";
                            break;
                        case "2":
                            color = "#d9534f";
                            break;
                        case "3":
                            color = "#961512";
                            break;
                        default:
                    }
                    return color;
                });

            nodeUpdate.select("text")
                .style("fill-opacity", 1);

            // Transition exiting nodes to the parent's new position.
            var nodeExit = node.exit().transition()
                .duration(duration)
                .attr("transform", function (d) {
                    return "translate(" + source.y + "," + source.x + ")";
                })
                .remove();

            nodeExit.select("circle")
                .attr("r", 1e-6);

            nodeExit.select("text")
                .style("fill-opacity", 1e-6);

            // Update the links…
            var link = vis.selectAll("path.link")
                .data(tree.links(nodes), function (d) {
                    return d.target.id;
                });

            // Enter any new links at the parent's previous position.
            link.enter().insert("svg:path", "g")
                .attr("class", "link")
                .style("stroke", function (d) {
                    var color = "#ccc";
                    switch (d.target.DataElementRisk) {
                        case "0":
                            color = "#5bc0de";
                            break;
                        case "1":
                            color = "#f0ad4e";
                            break;
                        case "2":
                            color = "#d9534f";
                            break;
                        case "3":
                            color = "#961512";
                            break;
                        default:
                    }
                    return color;
                })
                .attr("d", function (d) {
                    var o = {
                        x: source.x0,
                        y: source.y0
                    };
                    return diagonal({
                        source: o,
                        target: o
                    });
                })
                .transition()
                .duration(duration)
                .attr("d", diagonal);

            // Transition links to their new position.
            link.transition()
                .duration(duration)
                .attr("d", diagonal);

            // Transition exiting nodes to the parent's new position.
            link.exit().transition()
                .duration(duration)
                .attr("d", function (d) {
                    var o = {
                        x: source.x,
                        y: source.y
                    };
                    return diagonal({
                        source: o,
                        target: o
                    });
                })
                .remove();

            // Stash the old positions for transition.
            _.each(nodes, function(d) {
                d.x0 = d.x;
                d.y0 = d.y;
            });

            $scope.Ready = true;
        }

        // Toggle children.
        function toggle(d) {
            if (d.children) {
                d._children = d.children;
                d.children = null;
            } else {
                d.children = d._children;
                d._children = null;
            }
        }
    };

    $scope.DataFlowGraph();




}

DataFlowGraphController.$inject = ["$scope", "Settings", "Projects", "DataMapping"];
