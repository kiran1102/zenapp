// 3rd Party
import {
    isArray,
    remove,
} from "lodash";

// Services
import { StateService } from "@uirouter/core";
import { Permissions } from "modules/shared/services/helper/permissions.service";
import { TenantTranslationService } from "sharedServices/tenant-translation.service";
import { McmModalService } from "modules/mcm/mcm-modal.service";
import { GlobalSidebarService } from "modules/global-sidebar/services/one-global-sidebar.service";
import { Wootric } from "sharedModules/services/provider/wootric.service";
import { ReportAction } from "reportsModule/reports-list/services/report-action.service";

// Constants
import {
    InventorySchemaIds,
    LegacyInventorySchemaIds,
} from "constants/inventory-config.constant";
import { TranslationModules } from "constants/translation.constant";
import { WootricModuleMap } from "modules/shared/constants/wootric.constants";

// Enums
import { InventoryTableIds } from "enums/inventory.enum";
import { UpgradeModuleState } from "sharedModules/enums/upgrade.enum";
import { ReportListColumnProps } from "reportsModule/reports-shared/enums/reports.enum";
import { ModuleTypes } from "enums/module-types.enum";

// Interfaces
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import { INavMenuItem } from "modules/global-sidebar/interfaces/nav-menu-item.interface";
import { IInventorySchemaV2 } from "interfaces/inventory.interface";

class DataMappingCtrl implements ng.IComponentController {

    static $inject: string[] = [
        "$rootScope",
        "$scope",
        "$state",
        "$q",
        "ENUMS",
        "Tables",
        "TableHelper",
        "Templates",
        "Permissions",
        "OneInventories",
        "dmOneDataMaps",
        "DMTemplate",
        "TenantTranslationService",
        "McmModalService",
        "GlobalSidebarService",
        "Wootric",
        "ReportAction",
    ];

    constructor(
        private $rootScope: IExtendedRootScopeService,
        private $scope: any,
        private $state: ng.ui.IStateService,
        $q: ng.IQService,
        ENUMS: any,
        Tables: any,
        TableHelper: any,
        Templates: any,
        private permissions: Permissions,
        private OneInventories: any,
        private dmOneDataMaps: any,
        private DMTemplate: any,
        private tenantTranslationService: TenantTranslationService,
        private mcmModalService: McmModalService,
        private globalSidebarService: GlobalSidebarService,
        private wootric: Wootric,
        private reportAction: ReportAction,
    ) {
        $scope.Ready = false;
        $scope.Tables = [];
        $scope.Inventories = [];
        $scope.DataMaps = [];
        $scope.InventoryTableIds = ENUMS.InventoryTableIds;
        $scope.templateType = $state.params.templateType;
        $scope.hasReportingPermission = this.permissions.canShow("DataMappingDataMapsAssetsByLocation") ||
                                        this.permissions.canShow("DataMappingCrossborder") ||
                                        this.permissions.canShow("DataMappingAssetMapUpgrade") ||
                                        this.permissions.canShow("DataMappingCrossborderUpgrade") ||
                                        this.permissions.canShow("DataMappingScanUpgrade") ||
                                        this.permissions.canShow("ReportsDMInventory");

        function deserializeTables(inventories?: any[], attributes?: any[]): {Inventories: any[], Attributes: any[]} {
            return {
                Inventories: (isArray(inventories) ? TableHelper.applySettings(inventories, "InventoryId") : []),
                Attributes: (isArray(attributes) ? TableHelper.applySettings(attributes, "InventoryId") : []),
            };
        }

        function getTables(): any {
            return Tables.list().then(
                (response: any): any => {
                    $scope.Tables = response.result ? deserializeTables(response.data) : $scope.Tables = deserializeTables();
                    return $scope.Tables;
                },
            );
        }

        function getInventoryTypes(): IInventorySchemaV2[] {
            return OneInventories.getAPI()
                .then((inventories: IInventorySchemaV2[]): void => {
                    const deInventories: IInventorySchemaV2[] = remove(inventories, (inventory: IInventorySchemaV2) => inventory.id === InventorySchemaIds.Elements) || [];
                    $scope.Inventories = inventories || [];
                    if (deInventories.length) {
                        $scope.DEInventory = deInventories[0];
                    }
                });
        }

        function getDataMaps(): void {
            $scope.DataMaps = dmOneDataMaps.getDataMaps() || [];
        }

        const getInventoryTranslations = (): ng.IPromise<boolean> => {
            return this.tenantTranslationService.addModuleTranslations(TranslationModules.Inventory);
        };

        $scope.goToTemplate = (templateType: string): void => {
            if ($scope.LoadingTemplate !== templateType) {
                $scope.LoadingTemplate = templateType;
                $scope.templateType = templateType;
                $state.go("zen.app.pia.module.datamap.views.template-redirect", {templateType}).then((): void => {
                    $scope.LoadingTemplate = "";
                }).catch((): void => {
                    $scope.LoadingTemplate = "";
                });
            }
        };

        $scope.goTo = (route: string, params: any): void => {
            if (params) {
                params.time = (new Date().getTime());
            } else {
                params = {
                    time: (new Date().getTime()),
                };
            }
            $state.go(route, params);
        };

        $scope.isActiveInventory = (inventoryId: string): boolean => {
            return $state.includes("zen.app.pia.module.datamap.views.inventory.list", { Id: inventoryId })
                || $state.includes("zen.app.pia.module.datamap.views.inventory.details", { inventoryId: LegacyInventorySchemaIds[inventoryId] })
                || $state.includes("zen.app.pia.module.datamap.views.bulk-edit", { schemaId: inventoryId });
        };

        const init = (): void => {
            const promiseArray: any[] = [];
            if (this.permissions.canShow("DataMappingDataMaps")) promiseArray.push(getDataMaps());
            if (this.permissions.canShow("DataMappingInventory") || this.permissions.canShow("DataMappingInventoryNew")) promiseArray.push(getInventoryTypes());
            if (this.permissions.canShow("DataMappingInventories")) promiseArray.push(getTables());
            if (!this.tenantTranslationService.loadedModuleTranslations[TranslationModules.Inventory]) promiseArray.push(getInventoryTranslations());

            $q.all(promiseArray).then(
                (): void => {
                    this.globalSidebarService.set(
                        this.getMenuRoutes(),
                        this.$rootScope.t("DataMapping"),
                    );
                    $scope.Ready = true;
                },
            );
        };

        init();

    }

    $onInit() {
        this.wootric.run(WootricModuleMap.DataMapping);
    }

    $onDestroy() {
        this.globalSidebarService.set();
    }

    private getMenuRoutes(): INavMenuItem[] {
        if (!this.permissions.canShow("DataMappingSidebar") && !this.permissions.canShow("DataMappingSidebarNew")) return null;
        const menuGroup: INavMenuItem[] = [];

        // Welcome
        if (this.permissions.canShow("DataMappingWelcomeHomePage")) {
            menuGroup.push({
                id: "DataMappingWelcomeSideMenuItem",
                title: this.$rootScope.t("Welcome"),
                route: "zen.app.pia.module.datamap.views.welcome",
                activeRoute: "zen.app.pia.module.datamap.views.welcome",
                icon: "ot ot-globe",
            });
        }

        const canViewDMCustomDashboards: boolean = this.permissions.canShow("CustomDashboards") && this.permissions.canShow("ViewDMCustomDashboards");

        // Dashboard
        if (this.permissions.canShow("DataMappingAssessmentsDashboard") && !this.permissions.canShow("DMAssessmentV2") && !canViewDMCustomDashboards) {
            menuGroup.push({
                id: "DataMappingDashboardSideMenuItem",
                title: this.$rootScope.t("Dashboard"),
                route: "zen.app.pia.module.datamap.views.dashboard",
                activeRoute: "zen.app.pia.module.datamap.views.dashboard",
                icon: "ot ot-dashboard",
            });
        }
        // DM V2 Dashboard
        if (this.permissions.canShow("ViewDashboards") && this.permissions.canShow("DMAssessmentV2") && !canViewDMCustomDashboards) {
            menuGroup.push({
                id: "DataMappingDashboardSideMenuItem",
                title: this.$rootScope.t("Dashboard"),
                route: "zen.app.pia.module.datamap.views.dashboardv2",
                activeRoute: "zen.app.pia.module.datamap.views.dashboardv2",
                icon: "ot ot-dashboard",
            });
        }

        // DM Reporting Dashboard
        if (canViewDMCustomDashboards) {
            menuGroup.push({
                id: "DataMappingReportsDashboardSideMenuItem",
                title: this.$rootScope.t("Dashboard"),
                route: "zen.app.pia.module.datamap.views.custom-dashboards",
                activeRoute: "zen.app.pia.module.datamap.custom-dashboards",
                icon: "ot ot-dashboard",
            });
        }

        // New Assessments
        if (this.permissions.canShow("DataMappingAssessmentsList") && !this.permissions.canShow("DMAssessmentV2")) {
            menuGroup.push({
                id: "DataMappingNewAssessmentSideMenuItem",
                title: this.$rootScope.t("Assessments"),
                route: "zen.app.pia.module.datamap.views.assessment.list",
                activeRoute: "zen.app.pia.module.datamap.views.assessment",
                icon: "ot ot-file-text-o",
            });
        }

        // DM V2 Assessment
        if (this.permissions.canShow("DMAssessmentV2")) {
            menuGroup.push({
                id: "DataMappingV2AssessmentSideMenuItem",
                title: this.$rootScope.t("Assessments"),
                route: "zen.app.pia.module.assessment.list",
                activeRoute: "zen.app.pia.module.assessment",
                icon: "ot ot-file-text-o",
            });
        }

        const inventoryChildren: INavMenuItem[] = [];

        // New Inventories
        if (this.permissions.canShow("DataMappingInventoryNew")) {
            for (const inventory of this.$scope.Inventories) {
                inventoryChildren.push({
                    id: "DataMappingInventoryChildrenSideMenuItem",
                    title: inventory.nameKey ? this.$rootScope.t(inventory.nameKey) : inventory.name,
                    route: "zen.app.pia.module.datamap.views.inventory.list",
                    params: { Id: inventory.id, page: null, size: null },
                    activeRouteFn: (stateService: StateService) => {
                        return this.$scope.isActiveInventory(inventory.id);
                    },
                });
            }
        }

        if (this.permissions.canShow("DataMappingNew") && (this.permissions.canShow("DataMappingInventoryAssets") || this.permissions.canShow("DataMappingInventoryProcessingActivity"))) {
            menuGroup.push({
                id: "DataMappingInventorySideMenuItem",
                title: this.$rootScope.t("Inventory"),
                children: inventoryChildren,
                open: true,
                icon: "ot ot-folder-open-o",
            });
        }

        // Reportings -> Asset Map
        const reportingsChildren: INavMenuItem[] = [];
        if (this.permissions.canShow("DataMappingDataMapsAssetsByLocation")) {
            reportingsChildren.push({
                id: "DataMappingReportingChildrenAssetMapSideMenuItem",
                title: this.$rootScope.t("AssetMap"),
                route: "zen.app.pia.module.datamap.views.asset",
                activeRoute: "zen.app.pia.module.datamap.views.asset",
            });
        }
        if (this.permissions.canShow("DataMappingAssetMapUpgrade")) {
            reportingsChildren.push({
                id: "DataMappingAssetReportingChildrenMapUpgradeSideMenuItem",
                title: this.$rootScope.t("AssetMap"),
                upgrade: true,
                action: () => {
                    this.mcmModalService.openMcmModal("featureModalContent");
                },
            });
        }

        // Reportings -> Cross Border
        if (this.permissions.canShow("DataMappingCrossborder")) {
            reportingsChildren.push({
                id: "DataMappingReportingChildrenCrossBorderSideMenuItem",
                title: this.$rootScope.t("Crossborder"),
                route: "zen.app.pia.module.datamap.views.crossborder",
                activeRoute: "zen.app.pia.module.datamap.views.crossborder",
            });
        }

        if (this.permissions.canShow("DataMappingCrossborderUpgrade")) {
            reportingsChildren.push({
                id: "DataMappingReportingChildrenCrossBorderUpgradeSideMenuItem",
                title: this.$rootScope.t("Crossborder"),
                upgrade: true,
                action: () => {
                    this.mcmModalService.openMcmModal("DataMappingCrossborderUpgrade");
                },
            });
        }

        if (this.permissions.canShow("ReportsDMInventory")) {
            // TODO enable when able to filter by template
            // reportingsChildren.push({
            //     id: "DataMappingReportingChildrenArticle30ReportsSideMenuItem",
            //     title: this.$rootScope.t("Article30Reports"),
            //     route: "zen.app.pia.module.reports-new.list",
            //     activeRoute: "zen.app.pia.module.reports-new",
            // });
            reportingsChildren.push({
                id: "DataMappingReportingChildrenCustomInventoryReportsSideMenuItem",
                title: this.$rootScope.t("CustomInventoryReports"),
                action: () => {
                    this.reportAction.setReportListFilter({ [ReportListColumnProps.Type]: [ModuleTypes.INV] });
                    this.$state.go("zen.app.pia.module.reports-new.list");
                },
            });
        }

        // New Reporting
        if (this.permissions.canShow("DataMappingNew") && this.$scope.hasReportingPermission) {
            menuGroup.push({
                id: "DataMappingReportingSideMenuItem",
                title: this.$rootScope.t("Reporting"),
                children: reportingsChildren,
                open: true,
                icon: "fa fa-area-chart",
            });
        }

        // Lineage Upgrade
        if (this.permissions.canShow("DataLineageUpgrade")) {
            reportingsChildren.push({
                id: "DataMappingLinageUpgradeSideMenuItem",
                title: this.$rootScope.t("DataLineage"),
                upgrade: true,
                action: () => {
                    this.mcmModalService.openMcmModal("DataLineageUpgrade");
                },
            });
        }

        // Scan Upgrade
        if (this.permissions.canShow("DataMappingScanUpgrade")) {
            reportingsChildren.push({
                id: "DataMappingScanUpgradeSideMenuItem",
                title: this.$rootScope.t("ScanResults"),
                upgrade: true,
                action: () => {
                    this.mcmModalService.openMcmModal("DataMappingScanUpgrade");
                },
            });
        }

        // Setup -> Templates
        const setupChildren: INavMenuItem[] = [];

        // New Asset Template
        if (this.permissions.canShow("DataMappingAssetTemplate") && !this.permissions.canShow("DataMappingOld") && !this.permissions.canShow("DMAssessmentV2")) {
            setupChildren.push({
                id: "DataMappingNewAssetTemplateSideMenuItem",
                title: this.$rootScope.t("AssetTemplate"),
                route: "zen.app.pia.module.datamap.views.template-redirect",
                params: { templateType: InventoryTableIds.Assets },
                activeRouteFn: (stateService: StateService) => {
                    return stateService.includes("zen.app.pia.module.datamap.views.template", { templateType: InventoryTableIds.Assets })
                        || stateService.includes("zen.app.pia.module.datamap.views.template-redirect", { templateType: InventoryTableIds.Assets });
                },
            });
        }
        // New Processing Template
        if (this.permissions.canShow("DataMappingProcessingTemplate") && !this.permissions.canShow("DataMappingOld") && !this.permissions.canShow("DMAssessmentV2")) {
            setupChildren.push({
                id: "DataMappingProcessingTemplateSideMenuItem",
                title: this.$rootScope.t("ProcessingTemplate"),
                route: "zen.app.pia.module.datamap.views.template-redirect",
                params: { templateType: InventoryTableIds.Processes },
                activeRouteFn: (stateService: StateService) => {
                    return stateService.includes("zen.app.pia.module.datamap.views.template", { templateType: InventoryTableIds.Processes })
                        || stateService.includes("zen.app.pia.module.datamap.views.template-redirect", { templateType: InventoryTableIds.Processes });
                },
            });
        }
        // DM V2 Templates
        if (this.permissions.canShow("DMAssessmentV2")) {
            setupChildren.push({
                id: "DataMappingDMVAssessmentV2SideMenuItem",
                title: this.$rootScope.t("Templates"),
                route: "zen.app.pia.module.templatesv2.list",
                activeRoute: "zen.app.pia.module.templatesv2",
            });
        }

        // Data Elements Inventory
        if (this.permissions.canShow("DataMappingInventoryNew") && this.$scope.DEInventory) {
            if (this.permissions.canShow("EnableDataSubjectsElementsV2")) {
                setupChildren.push({
                    id: "DataMappingListsMenuItem",
                    title: this.$rootScope.t("InventoryManager"),
                    route: "zen.app.pia.module.datamap.views.inventory-manager.home",
                    activeRoute: "zen.app.pia.module.datamap.views.inventory-manager",
                });
            } else {
                setupChildren.push({
                    id: `DataMappingSetupChildrenInventorySideMenuItem-${this.$scope.DEInventory.id}`,
                    title: this.$scope.DEInventory.nameKey ? this.$rootScope.t(this.$scope.DEInventory.nameKey) : this.$scope.DEInventory.name,
                    route: "zen.app.pia.module.datamap.views.inventory.list",
                    params: { Id: this.$scope.DEInventory.id },
                });
            }
        }

        // Old Template
        if (this.permissions.canShow("DataMappingQuestionnaire")) {
            setupChildren.push({
                id: "DataMappingOldTemplateSideMenuItem",
                title: this.$rootScope.t("Template"),
                route: "zen.app.pia.module.datamap.views.template-redirect",
                params: { templateId: InventoryTableIds.Processes },
                activeRouteFn: (stateService: StateService) => {
                    return stateService.includes("zen.app.pia.module.datamap.views.questionnaire")
                        || stateService.includes("zen.app.pia.module.datamap.views.template-redirect");
                },
            });
        }

        // Old Assessments
        if (this.permissions.canShow("DataMappingAssessments")) {
            menuGroup.push({
                id: "DataMappingOldAssessmentsSideMenuItem",
                title: this.$rootScope.t("Assessments"),
                route: "zen.app.pia.module.datamap.views.assessment-old.list",
                activeRoute: "zen.app.pia.module.datamap.views.assessment-old",
                icon: "ot ot-file-text-o",
            });
        }

        // Old Inventories
        const oldInventoryChildren: INavMenuItem[] = [];

        if (this.permissions.canShow("DataMappingInventories") && this.$scope.Tables.Inventories) {
            for (const table of this.$scope.Tables.Inventories) {
                if (table.ResourceKey && this.permissions.canShow(table.ResourceKey)) {
                    oldInventoryChildren.push({
                        id: "DataMappingOldInventoriesSideMenuItem",
                        title: table.InventoryName || "Inventory Table",
                        route: "zen.app.pia.module.datamap.views.tables",
                        params: { tableId: table.InventoryId, page: null, size: null, filter: null, search: null },
                    });
                }
            }
        }

        if (oldInventoryChildren.length) {
            menuGroup.push({
                id: "DataMappingSetupSideMenuItem",
                title: this.$rootScope.t("Inventory"),
                children: oldInventoryChildren,
                open: true,
                icon: "ot ot-folder-open-o",
            });
        }

        // Maps Upgrade
        for (const map of this.$scope.DataMaps) {
            if (map.resourceUpgradeKey && this.permissions.canShow(map.resourceUpgradeKey)) {
                menuGroup.push({
                    id: `DataMappingMapsUpgradeSideMenuItem-${map.name}`,
                    title: this.$rootScope.t(map.name),
                    upgrade: true,
                    action: () => {
                        if (map.name === "AssetMap") {
                            this.mcmModalService.openMcmModal("DataMappingAssetMapUpgrade");
                        } else if (map.name === "Crossborder") {
                            this.mcmModalService.openMcmModal("DataMappingCrossborderUpgrade");
                        }
                    },
                });
            }
        }

        // Attribute Manager
        if (this.permissions.canShow("InventoryViewAttributes") && !this.permissions.canShow("EnableDataSubjectsElementsV2")) {
            setupChildren.push({
                id: "DataMappingSetupChildrenAttributeManagerSideMenuItem",
                title: this.$rootScope.t("AttributeManager"),
                route: "zen.app.pia.module.datamap.views.attribute-manager.home",
                activeRoute: "zen.app.pia.module.datamap.views.attribute-manager",
            });
        }

        // DataDiscovery Template
        if (this.permissions.canShow("IntegrationsDataDiscovery")) {
            setupChildren.push({
                id: "DataMappingSetupChildrenDataDiscoverySideMenuItem",
                title: this.$rootScope.t("DataDiscovery"),
                route: "zen.app.pia.module.datamap.views.data-discovery.home",
                activeRoute: "zen.app.pia.module.datamap.views.data-discovery",
            });
        }

        // Control Library
        if (this.permissions.canShow("ControlsLibrary") && this.permissions.canShow("InventoryLinkingEnabled")) {
            setupChildren.push({
                id: "DataMappingSetupChildrenSettingsControlsLibraryMenuItem",
                title: this.$rootScope.t("ControlsLibrary"),
                route: "zen.app.pia.module.risks.views.controls.library",
                activeRoute: "zen.app.pia.module.risks.views.controls",
            });
        }

        // Inventory Setting
        if (this.permissions.canShow("DataMappingInventorySettings") && this.permissions.canShow("InventoryLinkingEnabled") && this.permissions.canShow("Settings")) {
            setupChildren.push({
                id: "DataMappingSetupChildrenInventorySettingSideMenuItem",
                title: this.$rootScope.t("Settings"),
                route: "zen.app.pia.module.settings.inventory-settings",
                activeRoute: "zen.app.pia.module.settings.inventory-settings",
            });
        }

        if (setupChildren.length) {
            menuGroup.push({
                id: "DataMappingSetupSideMenuItem",
                title: this.$rootScope.t("Setup"),
                children: setupChildren,
                open: true,
                icon: "ot ot-wrench",
            });
        }
        return menuGroup;
    }

}

export default DataMappingCtrl;
