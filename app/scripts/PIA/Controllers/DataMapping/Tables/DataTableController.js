
export default function DataTableCtrl($scope, $q, $state, ENUMS, DataMapping, Tables, TableHelpers, TableHelper, Users, UserStore) {
    var vm = this;
    vm.Table;
    vm.TableId = $state.params ? ($state.params.tableId ? $state.params.tableId : "") : "";
    vm.UserStore = UserStore;
    vm.Users = Users;

    var inventoryPromises = [
        Tables.getPage(vm.TableId, $state.params.page, $state.params.size, $state.params.filter, $state.params.search),
    ];

    function setSingleColumnProps(column) {
        var deferred = $q.defer();
        TableHelper.applyColumnProperties(vm.TableId, column, vm.Table.Keys).then(
            function (column) {
                deferred.resolve(column);
            }
        );
        return deferred.promise;
    }

    function setColumnsProperties(columns) {
        var deferred = $q.defer();
        if (_.isArray(columns)) {
            var columnPromises = [];

            _.each(columns, function(column) {
                columnPromises.push(setSingleColumnProps(column));
            });
            $q.all(columnPromises).then(
                function (columns) {
                    deferred.resolve(columns);
                }
            );
        }
        else {
            deferred.resolve(columns);
        }
        return deferred.promise;
    }

    function setSingleRowProps(row) {
        var deferred = $q.defer();
        deferred.resolve(row);
        return deferred.promise;
    }

    function setRowsProperties(rows) {
        var deferred = $q.defer();
        if (_.isArray(rows)) {
            var rowPromises = [];
            _.each(rows, function(row) {
                rowPromises.push(setSingleRowProps(row));
            });
            $q.all(rowPromises).then(
                function (rows) {
                    deferred.resolve(rows);
                }
            );
        }
        else {
            deferred.resolve(rows);
        }
        return deferred.promise;
    }

    var init = function () {
        vm.loadingTable = true;
        $q.all(inventoryPromises).then(
            function (response) {
                // Table Response
                if (response) {
                    var tableResponse = response[0];
                    if (tableResponse.result) {
                        vm.Table = tableResponse.data;
                        vm.ValidTable = !!(vm.Table && Object.keys(vm.Table).length);
                        vm.Table.Keys = TableHelper.getKeys(vm.TableId);
                        vm.Table.ResourceKeys = TableHelper.getResourceKeys(vm.TableId);
                        vm.Table.Settings = TableHelper.getSettings(vm.TableId);
                        vm.Table.Events = TableHelpers.getEvents(vm.TableId);
                        vm.Table.PageMetadata = vm.Table.Events.formatPaginationConfig(vm.Table.PageMetadata);
                        vm.Table.PageMetadata.search = $state.params.search;
                        vm.Table.PageMetadata.filter = $state.params.filter;

                        var tablePropPromises = [
                            setColumnsProperties(vm.Table[vm.Table.Keys.Columns]),
                            setRowsProperties(vm.Table[vm.Table.Keys.Rows])
                        ];

                        $q.all(tablePropPromises).then(
                            function (tableProps) {
                                vm.Table[vm.Table.Keys.Columns] = tableProps[0];
                                vm.Table[vm.Table.Keys.Rows] = tableProps[1];

                                // Set Default Item after all Column and Row data has been set.
                                vm.Table.DefaultItem = _.isFunction(vm.Table.Events.getDefaultItem) ? vm.Table.Events.getDefaultItem(vm.Table.Keys, vm.Table[vm.Table.Keys.Columns]) : {};

                                // The Owner column doesn't include Inactives or Project Viewers
                                var users = vm.Users.getActiveNonProjectViewers(vm.UserStore.getUserList());
                                var userIds = _.map(users, vm.Users.userIdsOnly);
                                var owners = vm.Table.InventoryColumns[3].ColumnValues;
                                var userIdIncluded = function (user) {
                                    return _.includes(userIds, user.ColumnValueId);
                                };
                                // Resets Column Values without Project Viewers
                                vm.Table.InventoryColumns[3].ColumnValues = _.filter(owners, userIdIncluded);

                                vm.loadingTable = false;
                            }
                        );

                    }
                    else {
                        vm.loadingTable = false;
                    }
                }
                else {
                    vm.loadingTable = false;
                }
            }
        );
    };
    init();
}

DataTableCtrl.$inject = ["$scope", "$q", "$state", "ENUMS", "DataMapping", "Tables", "TableHelpers", "TableHelper", "Users", "UserStore"];
