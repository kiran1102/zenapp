import * as _ from "lodash";
import TreeMap from "TreeMap";
import Utilities from "Utilities";
import { getAllUsers } from "oneRedux/reducers/user.reducer";
import { IStore } from "interfaces/redux.interface";
import { Permissions } from "modules/shared/services/helper/permissions.service";
import { IDateTimeOutputModel } from "@onetrust/vitreus";

interface IAssignDmCtrlIScope extends ng.IScope {
    ApplyToAll: any;
    cancel: any;
    canSubmit: boolean;
    deleteItem: any;
    formatTypeahead: any;
    Items: string[];
    NewInventory: boolean;
    onDeadlineSelect: any;
    Organizations: any[];
    ProjectSettings: any;
    Ready: boolean;
    Saving: boolean;
    selectInputText: any;
    SendAssessments: any;
    setUpToggles: any;
    showingSuccessMessage: boolean;
    tableName: any;
    toggleDetails: any;
    toggleReminder: any;
    togglePrepopulate: any;
    TreeMap: any;
    Users: any[];
    validateIfHasErrors: any;
    canPrepopulate: boolean;
}

export interface IInventoryColumn {
    ColumnId: string | null;
    ColumnName: string;
    ColumnValues: IInventoryColumnValue[];
    Default: IInventoryColumnDefault;
    Input: number;
    Order: number;
    OrderBy: string;
    Required: boolean;
}

export interface IInventoryColumnDefault {
    Width: string;
}

export interface IInventoryColumnValue {
    ColumnEmail: string;
    ColumnOrgGroupId: string;
    ColumnOrgGroup: string;
    ColumnValue: string;
    ColumnValueId: string;
    Filtered?: boolean;
}

export default class AssignDMCtrl {

    static $inject: string[] = [
        "$scope",
        "$uibModalInstance",
        "$q",
        "$filter",
        "$timeout",
        "store",
        "ENUMS",
        "Settings",
        "Templates",
        "Projects",
        "Users",
        "OrgGroups",
        "DMAssessments",
        "Processes",
        "Columns",
        "NewInventory",
        "TemplateType",
        "TableName",
        "Permissions",
    ];

    private dmTemplate: any;
    private dmTemplateSections: any;
    private lastFocusedInput: any;
    private RootOrganizationTree: any[] = [];
    private truncate: any;

    constructor(
        readonly $scope: IAssignDmCtrlIScope,
        readonly $uibModalInstance: any,
        readonly $q: ng.IQService,
        $filter: ng.IFilterService,
        readonly $timeout: ng.ITimeoutService,
        readonly store: IStore,
        readonly ENUMS: any,
        readonly Settings: any,
        readonly Templates: any,
        readonly Projects: any,
        readonly Users: any,
        readonly OrgGroups: any,
        readonly DMAssessments: any,
        readonly Processes: any,
        readonly Columns: IInventoryColumn[],
        readonly NewInventory: boolean,
        readonly TemplateType: number,
        TableName: string,
        private permissions: Permissions,
    ) {

        $scope.Ready = false;
        $scope.Saving = false;
        $scope.Items = [];
        $scope.Users = getAllUsers(this.store.getState());
        $scope.Organizations = [];
        $scope.ProjectSettings = [];
        $scope.canSubmit = true;
        $scope.formatTypeahead = this.formatTypeahead;
        $scope.deleteItem = this.deleteItem;
        $scope.toggleDetails = this.toggleDetails;
        $scope.toggleReminder = this.toggleReminder;
        $scope.togglePrepopulate = this.togglePrepopulate;
        $scope.setUpToggles = this.setUpToggles;
        $scope.ApplyToAll = this.ApplyToAll;
        $scope.SendAssessments = this.SendAssessments;
        $scope.cancel = this.cancel;
        $scope.validateIfHasErrors = this.validateIfHasErrors;
        $scope.onDeadlineSelect = this.onDeadlineSelect;
        $scope.selectInputText = this.selectInputText;
        $scope.tableName = TableName;
        $scope.NewInventory = NewInventory;
        $scope.canPrepopulate = permissions.canShow("DataMappingAssessmentPrepopulate");
        this.truncate = $filter("truncate");
        this.init();
    }

    private init(): void {
        const promiseArray: any[] = [
            this.getSettings(),
            this.getOrganizationGroups(),
        ];

        if (!this.NewInventory) {
            promiseArray.push(this.getTemplate());
        }

        this.$q.all(promiseArray).then(
            (response: any): void => {
                this.getItems();
                this.getColumns();
                _.forEach(
                    this.$scope.Items,
                    (item: any): void => {
                        this.$scope.setUpToggles(item);
                    },
                );
                this.$scope.Ready = true;
            },
        );
    }

    private getColumns(): void {
        this.$scope.Organizations = (_.isArray(this.Columns) && this.Columns[2] && this.Columns[2].ColumnValues) ? this.Columns[2].ColumnValues : [];
        if (_.isArray(this.Columns) && this.NewInventory) {
            this.$scope.Users = this.mapUserForSendAssessments(this.Columns);
        } else if (_.isArray(this.Columns) && this.Columns[3] && this.Columns[3].ColumnValues) {
            this.$scope.Users = _.cloneDeep(this.Columns[3].ColumnValues);
        } else this.$scope.Users = [];
    }

    private getItems(): void {
        const item: any = {
            Name: "",
            Description: "",
            OrgGroupId: "",
            LeadId: "",
            Deadline: "",
            ReminderDays: 0,
            Comment: "",
            Tags: [],
            InventoryId: "",
            Prepopulate: true,
        };
        const items: any[] = [];
        if (_.isArray(this.Processes) && this.Processes.length) {
            _.forEach(
                this.Processes,
                (proc: any): void => {
                    const defaultItem: any = _.cloneDeep(item);
                    if (proc.defaultRespondent) {
                        defaultItem.LeadId = proc.defaultRespondent;
                    }
                    items.push(_.merge({}, defaultItem, proc));
                },
            );
        }
        this.$scope.Items = items;
    }

    private mapUserForSendAssessments = (attributes: any): any[] => {
        const filterUsers: any[] = _.filter(this.$scope.Users,
            (user: any): boolean => user.IsActive && user.RoleName !== this.ENUMS.RoleNames.ProjectViewer,
        );
        return _.map(filterUsers,
            (item: any): IInventoryColumnValue => {
                return {
                    ColumnValue: item.FullName.substring(0, 100) || "",
                    ColumnValueId: item.Id || "",
                    ColumnEmail: item.Email || "",
                    ColumnOrgGroup: item.OrgGroup || "",
                    ColumnOrgGroupId: item.OrgGroupId || "",
                };
            },
        );
    }

    private getTemplate(): any {
        const deferred: any = this.$q.defer();
        this.Templates.list(this.ENUMS.TemplateTypes.Datamapping).then(
            (response: any): void => {
                if (response.result && response.data.length) {
                    this.dmTemplate = response.data[0].Published;
                    // Get Template Sections
                    this.Templates.readTemplate({ id: this.dmTemplate.Id, version: this.dmTemplate.Version }).then(
                        (res: any): void => {
                            if (res.result) this.dmTemplateSections = res.data.Sections;
                            deferred.resolve(res);
                        },
                    );
                } else {
                    deferred.resolve({ result: false });
                }
            },
        );
        return deferred.promise;
    }

    private getSettings(): any {
        return this.Settings.getProject().then(
            (response: any): any => {
                if (response.result) {
                    this.$scope.ProjectSettings = response.data;
                    this.$scope.ProjectSettings.MinDate = new Date();
                }
                return response;
            },
        );
    }

    private getOrganizationGroups(): any {
        return this.OrgGroups.rootTree().then((response: any): void => {
            if (response.result) {
                this.RootOrganizationTree = response.data;
                this.$scope.TreeMap = new TreeMap("Id", "ParentId", "Children", this.RootOrganizationTree[0]);
            }
        });
    }

    private formatTypeahead = (list: IInventoryColumnValue[], id: any): string => {
        if (_.isArray(list)) {
            for (const item of list) {
                if (item.ColumnValueId === id) {
                    return (item.ColumnValue);
                }
            }
        }
        return Utilities.matchRegex(id, this.ENUMS.Regex.Email) ? id : "";
    }

    private formatLeadLabel(model: any): any {
        if (model) {
            if (Utilities.matchRegex(model, this.ENUMS.Regex.Guid)) {
                return this.Users.formatTeamLabel(this.$scope.Users, model);
            }
            if (Utilities.matchRegex(model, this.ENUMS.Regex.Email)) {
                return model;
            }
        }
        return "";
    }

    private deleteItem = (selected: any): any => {
        _.remove(this.$scope.Items,
            (item: any): boolean => {
                return selected === item;
            },
        );

        // If deleting the last item in the list, close modal
        if (!this.$scope.Items.length) {
            this.cancel();
        } else {
            this.validateItems();
        }
    }

    private toggleDetails = (item: any): void => {
        item.showingDetails = !item.showingDetails;
    }

    private toggleReminder = (item: any): void => {
        item.showingReminder = !item.showingReminder;
        this.validateReminderItem(item);
    }

    private togglePrepopulate = (item: any): void => {
        item.Prepopulate = !item.Prepopulate;
    }

    private setUpToggles = (item: any): void => {
        if (!item.Deadline) {
            item.showingReminder = false;
            item.ReminderDays = (this.$scope.ProjectSettings && this.$scope.ProjectSettings.Reminder && this.$scope.ProjectSettings.Reminder.Show) ? this.$scope.ProjectSettings.Reminder.Value : null;
        }
    }

    private assignSections(request: any): void {
        request.SectionAssignments = [];
        _.forEach(
            this.dmTemplateSections,
            (section: any): void => {
                const assignment: any = {
                    AssigneeId: request.LeadId,
                    SectionId: section.Id,
                    ProjectVersion: 1, // Will only ever launch version 1 at this time
                };
                request.SectionAssignments.push(assignment);
            },
        );
    }

    private ApplyToAll = (keys: any, baseItem: any): void => {
        if (_.isArray(keys) && baseItem) {
            _.forEach(this.$scope.Items, (item: any): void => {
                if (!_.isEqual(item, baseItem)) {
                    _.forEach(keys, (key: string): void => {
                        item[key] = _.cloneDeep(baseItem[key]);
                    });
                }
            });
            this.validateItems();
        }
    }

    private serializeRequests(items: any): any[] {
        const requests: any[] = [];
        if (_.isArray(items)) {
            _.forEach(
                items,
                (item: any): void => {
                    const createRequest: any = {
                        Name: item.Name,
                        Description: this.truncate(item.Description, 500),
                        OrgGroupId: item.OrgGroupId,
                        LeadId: item.LeadId,
                        Deadline: item.Deadline,
                        ReminderDays: item.ReminderDays,
                        Comment: item.Comment,
                        TemplateId: this.dmTemplate.Id,
                        TemplateVersion: this.dmTemplate.Version,
                        TemplateType: this.ENUMS.TemplateTypes.Datamapping,
                        Tags: [],
                        InventoryId: item.InventoryId,
                    };
                    // Assign Sections
                    this.assignSections(createRequest);
                    requests.push(createRequest);
                },
            );
        }
        return requests;
    }

    private dataMappingSerializeRequests(items: any): any[] {
        const requests: any[] = [];
        if (_.isArray(items)) {
            _.forEach(
                items,
                (item: any): void => {
                    const createRequest: any = {
                        approver: item.LeadId,
                        inventoryId: item.InventoryId,
                        assignedUser: item.LeadId,
                        name: item.Name,
                        orgGroupId: item.OrgGroupId,
                        templateType: this.TemplateType,
                        prepopulate: item.Prepopulate,
                    };
                    if (item.Deadline) {
                        createRequest.deadlineDate = item.Deadline;
                    }
                    if (item.Comment) {
                        createRequest.comment = item.Comment;
                    }
                    requests.push(createRequest);
                },
            );
        }
        return requests;
    }

    private SendAssessments = (): void => {
        this.validateItems();
        if (this.$scope.canSubmit) {
            this.$scope.Saving = true;
            const requests: any[] = (this.NewInventory ? this.dataMappingSerializeRequests(this.$scope.Items) : this.serializeRequests(this.$scope.Items));
            if (this.NewInventory) {
                this.newBulkCreate(requests);
            } else {
                this.oldBulkCreate(requests);
            }
        }
    }

    private newBulkCreate = (requests: any): void => {
        this.DMAssessments.dataMappingBulkCreate(requests).then(this.handleModalResponse(requests));
    }

    private oldBulkCreate = (requests: any): void => {
        this.DMAssessments.bulkCreate(requests).then(this.handleModalResponse(requests));
    }

    private handleModalResponse = (requests: any[]): any => {
        return (response: any): void => {
            this.$scope.Saving = false;
            if (response.result) {
                this.$scope.showingSuccessMessage = true;
                const processesLaunched: any = [];
                _.forEach(
                    requests,
                    (request: any): void => {
                        if (this.NewInventory) {
                            const recordResponse: any = _.find(response.data, { name: request.name });
                            processesLaunched.push(request.inventoryId);
                        } else {
                            processesLaunched.push(request.InventoryId);
                        }
                    },
                );
                this.$timeout(() => {
                    this.$uibModalInstance.close({ result: true, data: response.data, launched: processesLaunched });
                }, 2000);
            }
        };
    }

    private cancel = (): void => {
        this.$uibModalInstance.close({ result: false, data: null });
    }

    private validateItems(): void {
        this.$scope.canSubmit = true;
        _.forEach(
            this.$scope.Items,
            (item: any): void => {
                item.errors = item.errors || {};
                item.errors.lead = this.validateLead(item);
                item.errors.reminder = this.validateReminder(item);
                if (item.errors.lead || item.errors.reminder) {
                    this.$scope.canSubmit = false;
                }
            },
        );
    }

    private validateReminderItem(item: any): void {
        this.$scope.canSubmit = true;
        item.errors = item.errors || {};
        item.errors.reminder = this.validateReminder(item);
        if (item.errors.lead || item.errors.reminder) {
            this.$scope.canSubmit = false;
        }
    }

    private validateLead(item: any): boolean {
        const isGuidOrEmail: any = Utilities.matchRegex(item.LeadId, this.ENUMS.Regex.Guid) || Utilities.matchRegex(item.LeadId, this.ENUMS.Regex.Email);
        const invalidLead: boolean = !item.LeadId || item.LeadId === this.ENUMS.EmptyGuid || !isGuidOrEmail;
        return invalidLead;
    }

    private validateReminder(item: any): boolean {
        let invalidReminder = false;
        if (item.showingReminder) {
            if (item.ReminderDays) {
                const reminderDate: any = new Date(item.Deadline);
                reminderDate.setTime(reminderDate.getTime() - 1000 * 60 * 60 * 24 * item.ReminderDays);
                const today: any = new Date();
                invalidReminder = (today.toLocaleDateString() === reminderDate.toLocaleDateString()) || (reminderDate < today);
            } else {
                invalidReminder = true;
            }
        }
        return invalidReminder;
    }

    private onDeadlineSelect = (date: IDateTimeOutputModel, item: any) => {
        item.Deadline = date;

        if (!date) {
            item.showingReminder = false;
            item.ReminderDays = 0;
        }

        this.validateItems();
    }

    private validateIfHasErrors = (item: any, errorKey: any): void => {
        if (item.errors && item.errors[errorKey]) {
            this.validateItems();
        }
    }

    private selectInputText = (event: any): void => {
        if (event.target && (this.lastFocusedInput !== event.target)) {
            event.target.setSelectionRange(0, event.target.value.length);
        }
        this.lastFocusedInput = event.target;
    }

    private unsetFocusedElement(event: any): void {
        if (event.target === this.lastFocusedInput) {
            this.lastFocusedInput = null;
        }
    }
}
