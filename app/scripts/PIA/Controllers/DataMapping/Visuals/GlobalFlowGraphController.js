import * as d3 from "d3";

export default function GlobalFlowGraphController($rootScope, $scope, $q, DataMapping) {
    $scope.disableSave = true;
    $scope.Ready = false;
    var active = d3.select(null);
    var width = 800;
    var height = 600;
    var containerEl = document.getElementById('cross-border-graph');

    $scope.CrossBorderGraph = function () {
        DataMapping.getGraphCrossBorder().then(
            function (response) {
                if (response.result) {
                    $scope.crossBorderGraphData = response.data;
                    $("#cross-border-graph").append(response.data);
                    $("#cross-border-graph").html($("#cross-border-graph").html());

                    var getInventories = [
                        // GetDataInventoryElements(),
                        // GetDataInventoryAssets()
                    ];

                    $q.all(getInventories).then(function () {
                        var data = {
                            DataElements: $scope.DataElements,
                            Assets: $scope.Assets
                        }
                        $('#arrowhead-none').remove();
                        createCrossBorderControls(data);
                        $scope.Ready = true;
                        pollVisibility();
                    })
                } else {
                    $scope.Ready = true;
                }
            }
        );
    };

    var GetDataInventoryElements = function () {
        return DataMapping.getDataInventoryElements()
            .then(function (response) {
                if (response.result) {
                    $scope.DataElements = response.data;
                }
            });
    };

    var GetDataInventoryAssets = function () {
        return DataMapping.getDataInventoryAssets()
            .then(function (response) {
                if (response.result) {
                    $scope.Assets = response.data;
                }
            });
    };

    var createCrossBorderControls = function (data) {
        var DataFlows = $scope.DataFlows || [];
        var Locations = data.Assets || [];

        var svg = d3.select("#cross-border-graph");

        // Hover event container
        var tooltip = svg.append("div")
            .attr("class", "tooltip")
            .html("<h6>Roll over graph for information</h6>");

        // Transfers
        var transfers = svg.selectAll(".transfer")
            .data(DataFlows);

        transfers
            .on("click", clicked)
            .on("mouseover", function (d) {
                d3.select(this).classed("transfer-grow", true);
                tooltip.transition()
                    .duration(200);
                tooltip.html(
                    "<h5> Data Flow Information </h5>" +
                    "<div>" +
                    "<h6>Source: " + d.Source + "</h6>" +
                    "<h6>Target: " + d.Destination + "</h6>" +
                    "<h6>Element: " + d.Element + "</h6>" +
                    "</div>");
            })
            .on("mouseout", function () {
                d3.select(this).classed("transfer-grow", false);
            });

        // Locations
        var locations = svg.selectAll(".location")
            .data(Locations);

        locations
            .on("click", clicked)
            .on("mouseover", function (d) {
                d3.select(this).classed("location-grow", true);
                var name = $(this).data("name");
                var thisAsset = findAsset(name);
                tooltip.transition()
                    .duration(200);
                tooltip.html("<h5> " + (($scope.$root.customTerms && $scope.$root.customTerms.System) || "System") + " Information </h5>" +
                    "<h6> Name " + thisAsset.Value + "</h6><h6> Location " + thisAsset.Location.Name + "</h6>");
            })
            .on("mouseout", function () {
                d3.select(this).classed("location-grow", false);
            });
    };

    var findAsset = function (compareAsset) {
        var sendAsset;
        _.each($scope.Assets, function(asset) {
            if (asset.Location.Name === compareAsset) {
                sendAsset = asset;
            }
        });
        return sendAsset;
    };

    var projection = d3.geo.albersUsa()
        .scale(1000)
        .translate([width / 2, height / 2]);

    var path = d3.geo.path()
        .projection(projection);

    function clicked(d) {
        if (active.node() === this) return reset();
        active.classed("active", false);
        active = d3.select(this).classed("active", true);

        var bounds = path.bounds(d),
            dx = bounds[1][0] - bounds[0][0],
            dy = bounds[1][1] - bounds[0][1],
            x = (bounds[0][0] + bounds[1][0]) / 2,
            y = (bounds[0][1] + bounds[1][1]) / 2,
            scale = .9 / Math.max(dx / width, dy / height),
            translate = [width / 2 - scale * x, height / 2 - scale * y];

        rect.transition()
            .duration(750)
            .style("stroke-width", 1.5 / scale + "px")
            .attr("transform", "translate(" + translate + ")scale(" + scale + ")");
    }

    function reset() {
        active.classed("active", false);
        active = d3.select(null);

        g.transition()
            .duration(750)
            .style("stroke-width", "1.5px")
            .attr("transform", "");
    }

    function initZoom() {
        width = containerEl.offsetWidth;
        height = containerEl.offsetHeight;
        var svgWidth = $("svg").width();
        var svgHeight = $("svg").height();
        var path = d3.selectAll("path");
        var circle = d3.selectAll("circle");

        var locationArray = $(".location");
        var rectArray = [];

        locationArray.map(function (index) {
            rectArray.push(locationArray[index].getBBox());
        });


        var xMax = Math.max.apply(Math, rectArray.map(function (location) {
            return location.x;
        }));
        var xMin = Math.min.apply(Math, rectArray.map(function (location) {
            return location.x;
        }));
        var yMax = Math.max.apply(Math, rectArray.map(function (location) {
            return location.y;
        }));
        var yMin = Math.min.apply(Math, rectArray.map(function (location) {
            return location.y;
        }));

        var dx = xMax - xMin,
            dy = yMax - yMin,
            x = (xMin + xMax) / 2,
            y = (yMin + yMax) / 2,

            scale = .9 / Math.max(dx / width, dy / height),
            translate = [width / 2 - scale * x, height / 2 - scale * y];

        path.transition()
            .duration(750)
            .style("stroke-width", 1.5 / scale + "px")
            .attr("transform", "translate(" + translate + ")scale(" + scale + ")");

        circle.transition()
            .duration(750)
            .style("stroke-width", 1.5 / scale + "px")
            .attr("transform", "translate(" + translate + ")scale(" + scale + ")");
    }

    function pollVisibility() {
        if (!$(".fa-spinner").is(":visible")) {
            initZoom();
        } else {
            setTimeout(pollVisibility, 500);
        }
    }

    $scope.CrossBorderGraph();
}

GlobalFlowGraphController.$inject = ["$rootScope", "$scope", "$q", "DataMapping"];
