// Redux
import { getUserById } from "oneRedux/reducers/user.reducer";

// Constants
import { AssessmentThankYouStates } from "constants/assessment.constants";
import { TemplateTypes as AssessmentTemplateTypes } from "constants/template-types.constant";

// Services
import { AuthHandlerService } from "sharedModules/services/helper/auth-handler.service";

export default function ThanksCtrl(
    $scope,
    $rootScope,
    $stateParams,
    $state,
    $q,
    $sce,
    store,
    ENUMS,
    Projects,
    DMAssessments,
    AssessmentDetailApi,
    authHandlerService
) {

    $scope.Ready = false;
    $scope.description = "";
    var TemplateTypes = ENUMS.TemplateTypes;
    var projectId = $stateParams.projectId || "";
    var projectVersion = $stateParams.projectVersion || "";
    var templateType = parseInt($stateParams.templateType) || null;
    var options = $stateParams.options || null;
    var saveAndExit = $stateParams.saveAndExit || false;
    var getAssessment = templateType !== TemplateTypes.NewDatamapping ? Projects.read : DMAssessments.getAssessment;
    if ($stateParams.templateType === AssessmentTemplateTypes.PIA) {
        templateType = AssessmentTemplateTypes.PIA;
        getAssessment = AssessmentDetailApi.fetchFirstSection;
    }
    var returnRoute = setRoute(templateType);
    var reviewer, subject;
    var projectWord = templateType === TemplateTypes.Custom ? $rootScope.customTerms.Project : $rootScope.t("Assessment");

    var init = function () {
        if (options) {
            $scope.currentState = options ? (options.state ? options.state : AssessmentThankYouStates.Complete) : AssessmentThankYouStates.Complete;
            subject = options.projectName ? "?Subject=" + encodeURI(options.projectName) : "";
            reviewer = options.reviewerId ? getUserById(options.reviewerId)(store.getState()) : null;
        } else if (projectId && templateType) {
            return getAssessment(projectId, projectVersion).then(
                function (response) {
                    var project;
                    if (templateType == AssessmentTemplateTypes.PIA && response){
                        project = response;
                        project.approver = response.approvers[0];
                    } else if (response.data) {
                        project = response.data;
                    } else if (!response.data) return;

                    options = {
                        state: AssessmentThankYouStates.Complete,
                        projectName: templateType === TemplateTypes.Custom ? project.Name : project.name,
                    };

                    if (project.approver) {
                        options.reviewerId = (templateType === TemplateTypes.Custom) ? project.ReviewerId : project.approver.id;
                    }

                    $scope.currentState = options ? (options.state ? options.state : AssessmentThankYouStates.Complete) : AssessmentThankYouStates.Complete;
                    subject = options.projectName ? "?Subject=" + encodeURI(options.projectName) : "";
                    reviewer = options.reviewerId ? getUserById(options.reviewerId)(store.getState()) : null;
                }
            );
        }
        return $q.when();
    };

    function setRoute(templateType) {
        switch(templateType) {
            case TemplateTypes.Datamapping:
                return "zen.app.pia.module.datamap.views.assessment-old.single.module.questions";
            case TemplateTypes.NewDatamapping:
                return "zen.app.pia.module.datamap.views.assessment.single";
            case AssessmentTemplateTypes.PIA:
                return "zen.app.pia.module.assessment.detail";
            default:
                return "zen.app.pia.module.projects.assessment.module.questions";
        }
    };

    var setText = function () {
        // Approver
        $scope.description += reviewer ? $rootScope.t("EmailSentToApprover", { approver: reviewer.FullName }) + " " : $rootScope.t("EmailSentToAllApprovers") + " ";
        // Approver email
        $scope.description += reviewer ? $rootScope.t("ContactApproverAtEmail", { approver: reviewer.FullName, projectWord: projectWord, emailLink: "<a href='mailto:" + reviewer.Email + subject + "'>" + reviewer.Email + "</a>" }) + " " : "";
        // End
        $scope.description += $rootScope.t("WillReceiveEmailIfRequired");
        $scope.description = $sce.trustAsHtml($scope.description);
    };

    var setMessages = function () {
        $scope.messages = _.merge({},
            Utilities.objectify(AssessmentThankYouStates.Complete,
                {
                    subhead: saveAndExit ? $rootScope.t("YourResponsesHaveBeenSaved") : $rootScope.t("ProjectSubmitted", { projectWord: projectWord }),
                    description: saveAndExit ? "" : $scope.description,
                    buttons: [{
                        text: $rootScope.t("ViewProject", { projectWord: projectWord }),
                        click: returnToProject,
                        show: (options && projectId)
                    },
                    {
                        text: $rootScope.t("ExitApplication"),
                        click: logout,
                        show: true
                    }]
                }
            ),
            Utilities.objectify(AssessmentThankYouStates.Incomplete,
                {
                    subhead: $rootScope.t("YourProjectHasBeenSaved", { projectWord: projectWord }),
                    buttons: [{
                        text: $rootScope.t("ReturnToProject", { projectWord: projectWord }),
                        click: returnToProject,
                        show: (options && projectId)
                    },
                    {
                        text: $rootScope.t("ExitApplication"),
                        click: logout,
                        show: true
                    }]
                }
            )
        );
    };

    var logout = function () {
        authHandlerService.logout();
    };

    var returnToProject = function () {
        if (options && projectId) {
            if (templateType === AssessmentTemplateTypes.PIA){
                $state.go(returnRoute, {
                    assessmentId: projectId,
                });
            } else {
                $state.go(returnRoute, {
                    Id: projectId,
                    Version: projectVersion
                });
            }
        }
    };

    init().then(function () {
        setText();
        setMessages();
        $scope.Ready = true;
    });

}

ThanksCtrl.$inject = [
    "$scope",
    "$rootScope",
    "$stateParams",
    "$state",
    "$q",
    "$sce",
    "store",
    "ENUMS",
    "Projects",
    "DMAssessments",
    "AssessmentDetailApi",
    "AuthHandlerService"
];
