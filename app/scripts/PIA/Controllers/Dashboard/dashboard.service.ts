import { IExtendedRootScopeService } from "interfaces/root-scope.interface";

class DashboardService {

    static $inject: string[] = ["$q", "$rootScope", "OneProtocol"];

    constructor(readonly $q: ng.IQService, readonly $rootScope: IExtendedRootScopeService, readonly OneProtocol: any) {
    }

    public getDashboardData(): Promise<any> {
        const config: any = this.OneProtocol.config("GET", "/report/dashboard");
        const messages: any = { Error: { object: this.$rootScope.t("Dashboard"), action: this.$rootScope.t("Getting") } };
        return this.OneProtocol.http(config, messages);
    }
}

export default DashboardService;
