import * as _ from "lodash";
import Utilities from "Utilities";
import { Content } from "sharedModules/services/provider/content.service";

import { OnboardingService } from "modules/onboarding/services/onboarding.service";
import { GlobalSidebarService } from "modules/global-sidebar/services/one-global-sidebar.service";

import { getOrgList, getCurrentOrgId } from "oneRedux/reducers/org.reducer";

import { IStore } from "interfaces/redux.interface";
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";

interface IFilter {
    key: string;
    values: any[];
}

export default class DashboardController {
    static $inject: string[] = [
        "$rootScope",
        "$scope",
        "$q",
        "$cacheFactory",
        "Projects",
        "store",
        "Content",
        "currentUser",
        "ENUMS",
        "RiskAPI",
        "FilterFactory",
        "RiskStateFilter",
        "SettingStore",
        "DashboardService",
        "OnboardingService",
        "GlobalSidebarService",
    ];

    private ColumnTypes: any;
    private RiskStates: any;
    private translate: any;
    private apiCache: any;

    constructor(
        $rootScope: IExtendedRootScopeService,
        private readonly $scope: any,
        private readonly $q: ng.IQService,
        private readonly $cacheFactory: any,
        private readonly Projects: any,
        private readonly store: IStore,
        private readonly content: Content,
        private readonly currentUser: any,
        private readonly ENUMS: any,
        private readonly RiskAPI: any,
        private FilterFactory: any,
        private RiskStateFilter: any,
        private readonly SettingStore: any,
        readonly DashboardService: any,
        private onboardingService: OnboardingService,
        private globalSidebarService: GlobalSidebarService,
    ) {
        this.translate = $rootScope.t;
        this.apiCache = $rootScope.APICache;
        this.ColumnTypes = ENUMS.RiskColumnTypes;
        this.$scope.projectCustomTerm = $rootScope.customTerms.Project;
        this.init();
    }

    $onInit() {
        this.globalSidebarService.setAAMenu();
    }

    private init(): void {
        this.$scope.ready = false;
        this.$scope.riskGraphReady = false;
        this.$scope.projectRiskGraphReady = false;

        this.$scope.status = {
            projectsTotal: 0,
            projectsInProgress: 0,
            projectsUnderReview: 0,
            projectsRiskAssessment: 0,
            projectsCompleted: 0,
            projectsRiskTreatment: 0,
            projectsInfoNeeded: 0,
        };

        this.$scope.currentUser = this.currentUser;

        this.onboardingService.onboard(
            this.translate("WelcomeToTheDashboard"),
            this.content.GetKey("OnBoardingDashboard"),
        );

        this.$scope.selectedOrgGroupId = "";
        this.$scope.selectedRiskStatus = "";
        this.$scope.selectedTemplate = this.translate("NoTemplateSelected");

        this.$scope.pieOrgFilter = "";
        this.$scope.projectAgeFilter = "";

        this.$scope.filteredRiskCount = 0;

        this.$scope.getProjectHistory = (Id: string, Version: any): any => {
            const deferred: any = this.$q.defer();

            this.$scope.projectHistory = [];

            this.apiCache.getData("Projects.getHistory", this.Projects.getHistory, { Id, Version })
                .then((res: any): any => {
                    this.$scope.projectHistory = res.data;
                    deferred.resolve(this.$scope.projectHistory);
                });
            return deferred.promise;
        };

        this.$scope.updateAgingProjectGraph = (selected: any): void => {
            this.$scope.$root.$broadcast("updateAgingProjectGraph", selected.day);
        };

        this.$scope.updatePieGraph = (orgGroup: any): void => {
            if (orgGroup.Id === "" || _.isNil(orgGroup.Id)) {
                this.$scope.status = this.Projects.countEachProjectStates(this.$scope.projects);
            } else {
                const filteredProjects: any[] = [];
                const projectsLength: number = this.$scope.projects.length;

                for (let i = 0; i < projectsLength; i++) {
                    if (this.$scope.projects[i].OrgGroupId === orgGroup.Id) {
                        filteredProjects.push(this.$scope.projects[i]);
                    }
                }
                this.$scope.status = this.Projects.countEachProjectStates(filteredProjects);
            }
            this.$scope.$root.$broadcast("updatePieGraph", orgGroup.Id);
        };

        this.$scope.riskStateFilter = this.RiskStateFilter;
        this.$scope.allRiskStatesEnabled = this.RiskStateFilter.isAllEnabled();

        this.$scope.riskFilter = this.FilterFactory.createFilter();
        this.$scope.projectRiskFilter = this.FilterFactory.createFilter();
        this.$scope.projectRiskFilter.set("TemplateId", "");

        this.$scope.updateRiskOrgFilter = (orgGroup: any): void => {
            if (orgGroup.Id === "" || _.isNil(orgGroup.Id)) {
                this.$scope.riskFilter.removeKey("OrgGroupId");
            } else {
                this.$scope.riskFilter.set("OrgGroupId", orgGroup.Id);
            }
            this.prepareRiskGraphData();
        };

        this.$scope.toggleAllRiskStates = (): void => {
            this.RiskStateFilter.toggleAll(!this.RiskStateFilter.isAllEnabled());
            this.$scope.allRiskStatesEnabled = this.RiskStateFilter.isAllEnabled();

            if (this.$scope.allRiskStatesEnabled) {
                this.$scope.riskFilter.removeKey("State");
            } else {
                this.$scope.riskFilter.set("State", this.RiskStateFilter.getEnabledStates());
            }

            this.prepareRiskGraphData();
        };

        this.$scope.toggleRiskStateFilter = (column: any): void => {
            const state: number = Number(column.lookupKey);
            const enabled: boolean = this.RiskStateFilter.toggleRiskState(state);
            this.$scope.allRiskStatesEnabled = this.RiskStateFilter.isAllEnabled();

            if (enabled) {
                this.$scope.riskFilter.set("State", state);
            } else {
                this.$scope.riskFilter.remove("State", state);
            }

            if (this.$scope.allRiskStatesEnabled) {
                this.$scope.riskFilter.removeKey("State");
            } else {
                this.$scope.riskFilter.set("State", this.RiskStateFilter.getEnabledStates());
            }

            this.prepareRiskGraphData();
        };

        this.$scope.updateProjectRiskOrgFilter = (orgGroup: any): void => {
            if (orgGroup.Id === "" || _.isNil(orgGroup.Id)) {
                this.$scope.projectRiskFilter.removeKey("OrgGroupId");
            } else {
                this.$scope.projectRiskFilter.set("OrgGroupId", orgGroup.Id);
            }
            this.prepareProjectRiskGraphData();
        };

        this.$scope.updateTemplateFilter = (template: any): void => {
            if (template.id === "" || _.isNil(template.id)) {
                this.$scope.projectRiskFilter.clearKey("TemplateId");
                this.$scope.selectedTemplate = this.translate("NoTemplateSelected");
            } else {
                this.$scope.projectRiskFilter.set("TemplateId", template.id);
                this.$scope.selectedTemplate = template.text;
            }
            this.prepareProjectRiskGraphData();
        };

        this.$scope.projectAgeFilters = [
            {
                text: this.translate("ShowAll"),
                day: "",
                action: this.$scope.updateAgingProjectGraph,
            },
            {
                text: this.translate("Last15Days"),
                day: 15,
                action: this.$scope.updateAgingProjectGraph,
            },
            {
                text: this.translate("Last30Days"),
                day: 30,
                action: this.$scope.updateAgingProjectGraph,
            },
            {
                text: this.translate("Last60Days"),
                day: 60,
                action: this.$scope.updateAgingProjectGraph,
            },
            {
                text: this.translate("Last90Days"),
                day: 90,
                action: this.$scope.updateAgingProjectGraph,
            },
        ];

        const promiseArray: any[] = [
            this.getProjectSettings(),
            this.getDashboardData(),
            this.getOrganizations(),
        ];

        this.$q.all(promiseArray).then((): void => {
            this.$scope.ready = true;
            this.prepareRiskGraphData();
            this.prepareProjectRiskGraphData();
        });
    }

    private getProjectSettings(): ng.IPromise<any> {
        return this.SettingStore.fetchProjectSettings().then((res: any): void => {
            this.$scope.ProjectSettings = res;
        });
    }

    private getDashboardData(): any {
        return this.DashboardService.getDashboardData().then((response: any): void => {
            if (response.result) {
                this.$scope.projects = response.data.Projects;
                this.$scope.risks = response.data.Risks;
                this.$scope.status = this.Projects.countEachProjectStates(this.$scope.projects);
                const templateHashTable: any = {};
                for (const project of this.$scope.projects) {
                    templateHashTable[project.TemplateId] = project.TemplateName;
                }
                this.$scope.templateTypes = [{ text: this.translate("NoFilter"), id: "", action: this.$scope.updateTemplateFilter }];

                const templateIds: string[] = Object.keys(templateHashTable);
                for (const templateId of templateIds) {
                    this.$scope.templateTypes.push({
                        id: templateId,
                        text: templateHashTable[templateId],
                        action: this.$scope.updateTemplateFilter,
                    });
                }
            } else {
                this.$scope.projects = [];
                this.$scope.templateTypes = [{ text: this.translate("NoFilter"), id: "", action: this.$scope.updateTemplateFilter }];
            }
        });
    }

    private getOrganizations(): any {
        const currentOrgs = getOrgList(getCurrentOrgId(this.store.getState()))(this.store.getState());
        if (currentOrgs) {
            this.$scope.OrganizationGroups = currentOrgs;

            this.$scope.orgGroupsForProject = [{ text: this.translate("NoFilter"), Id: "", action: this.$scope.updatePieGraph }];
            const orgGroupsForProject: any[] = currentOrgs.map((org: any): any => ({
                text: org.Name,
                Id: org.Id,
                action: this.$scope.updatePieGraph,
            }));
            this.$scope.orgGroupsForProject = this.$scope.orgGroupsForProject.concat(orgGroupsForProject);

            this.$scope.orgGroupsForRisk = [{ text: this.translate("NoFilter"), Id: "", action: this.$scope.updateRiskOrgFilter }];
            const orgGroupsForRisk: any[] = currentOrgs.map((org: any): any => ({
                text: org.Name,
                Id: org.Id,
                action: this.$scope.updateRiskOrgFilter,
            }));
            this.$scope.orgGroupsForRisk = this.$scope.orgGroupsForRisk.concat(orgGroupsForRisk);

            this.$scope.orgGroupsForProjectRisk = [{ text: this.translate("NoFilter"), Id: "", action: this.$scope.updateProjectRiskOrgFilter }];
            const orgGroupsForProjectRisk: any[] = currentOrgs.map((org: any): any => ({
                text: org.Name,
                Id: org.Id,
                action: this.$scope.updateProjectRiskOrgFilter,
            }));
            this.$scope.orgGroupsForProjectRisk = this.$scope.orgGroupsForProjectRisk.concat(orgGroupsForProjectRisk);
        } else {
            this.$scope.OrganizationGroups = [];
            this.$scope.orgGroupsForProject = [{ text: this.translate("NoFilter"), Id: "", action: this.$scope.updatePieGraph }];
            this.$scope.orgGroupsForRisk = [{ text: this.translate("NoFilter"), Id: "", action: this.$scope.updateRiskOrgFilter }];
            this.$scope.orgGroupsForProjectRisk = [{ text: this.translate("NoFilter"), Id: "", action: this.$scope.updateProjectRiskOrgFilter }];
        }
    }

    private prepareRiskGraphData(): void {
        let riskGraphData: any[] = [
            { id: 0, label: this.translate("NoRisk").toString(), count: 0, percentage: 0 },
            { id: 1, label: this.translate("Low").toString(), count: 0, percentage: 0 },
            { id: 2, label: this.translate("Medium").toString(), count: 0, percentage: 0 },
            { id: 3, label: this.translate("High").toString(), count: 0, percentage: 0 },
            { id: 4, label: this.translate("VeryHigh").toString(), count: 0, percentage: 0 },
        ];
        const totalCount = 0;
        const risks: any[] = this.$scope.riskFilter.applyFilter(this.$scope.risks);
        const risksLength: number = risks.length;
        this.$scope.filteredRiskCount = risksLength;

        for (const risk of risks) {
            if (risk.Level) {
                riskGraphData[risk.Level].count++;
            } else {
                riskGraphData[0].count++;
            }
        }

        // percentage error calculation to avoid the sum to be 99 or 101.
        let max = 0;
        let error = 0;
        let maxInd = 0;
        riskGraphData = riskGraphData
            .filter((data: any): boolean => data.count > 0)
            .map((data: any, i: number): any => {
                const dirtyPercentage: number = data.count / risksLength * 100;
                const cleanPercentage: number = Utilities.round(dirtyPercentage, 0);
                const diff: number = dirtyPercentage - cleanPercentage;
                error += diff;
                if (cleanPercentage > max) {
                    max = cleanPercentage;
                    maxInd = i;
                }
                data.percentage = cleanPercentage;
                return data;
            });

        if (riskGraphData.length) {
            riskGraphData[maxInd].percentage += Utilities.round(error, 0);
        }

        this.$scope.riskGraphData = riskGraphData;
        this.$scope.riskGraphReady = true;
    }

    private prepareProjectRiskGraphData(): void {
        let projectRiskGraphData: any = [
            { label: this.translate("OpenRisk").toString(), legendLabel: this.translate("OpenRisk").toString(), color: "#BE2F2B", count: 0, percentage: 0, magnitude: 0 },
            { label: this.translate("ClosedNoRisk").toString(), legendLabel: this.translate("ClosedNoRisk").toString(), color: "#6CC04A", count: 0, percentage: 0, magnitude: 0 },
        ];
        const totalCount = 0;
        const resolvedRiskState = 50;
        const projects: any[] = this.$scope.projectRiskFilter.applyFilter(this.$scope.projects);
        const projectsLength: number = projects.length;
        this.$scope.filteredProjectRiskCount = projectsLength;

        for (const project of projects) {
            if (project.StateDesc === "Completed") {
                projectRiskGraphData[1].count++;
            } else if (project.ProjectRisk) {
                if (project.ProjectRiskState < resolvedRiskState) {
                    projectRiskGraphData[0].count++;
                } else {
                    projectRiskGraphData[1].count++;
                }
            } else {
                projectRiskGraphData[1].count++;
            }
        }

        // percentage error calculation to avoid the sum to be 99 or 101.
        let max = 0;
        let error = 0;
        let maxInd = 0;
        projectRiskGraphData = projectRiskGraphData
            .map((data: any, i: number): any => {
                const dirtyPercentage: number = data.count / projectsLength * 100;
                const cleanPercentage: number = Utilities.round(dirtyPercentage, 0);
                const diff: number = dirtyPercentage - cleanPercentage;
                error += diff;
                if (cleanPercentage > max) {
                    max = cleanPercentage;
                    maxInd = i;
                }
                data.percentage = cleanPercentage;
                data.magnitude = cleanPercentage;
                return data;
            });

        if (projectRiskGraphData.length) {
            projectRiskGraphData[maxInd].percentage += Utilities.round(error, 0);
            projectRiskGraphData[maxInd].magnitude += Utilities.round(error, 0);
        }
        this.$scope.projectRiskGraphData = projectRiskGraphData;
        this.$scope.projectRiskGraphReady = true;
    }
}
