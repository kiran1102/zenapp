
export default function DocumentsMenuCtrl($rootScope, $state, $q, $timeout, currentUser, $uibModal, Documents, DocumentsDirectory, DocumentFactory, Permissions) {
    /* jshint validthis:true */

    var vm = this;
    var params = $state.params;

    vm.permissions = {
        DocumentsCreate: Permissions.canShow("DocumentsCreate"),
        DocumentsDelete: Permissions.canShow("DocumentsDelete"),
        DocumentsDetails: Permissions.canShow("DocumentsDetails"),
        DocumentsDownload: Permissions.canShow("DocumentsDownload"),
        DocumentsList: Permissions.canShow("DocumentsList"),
        DocumentsTree: Permissions.canShow("DocumentsTree"),
    };
    vm.Ready = false;
    vm.Library = [];
    vm.Directory = null;
    vm.CurrentDirectory = null;
    vm.CurrentDirectoryId = "";
    vm.TreeFilter = {
        IsDirectory: true
    };
    vm.ShowMenu = true;

    function init() {
        vm.Ready = false;
        vm.Directory = DocumentsDirectory;
        vm.Library = DocumentsDirectory.GetRootArr() || [];
        vm.Library = recursivelySetIcon(vm.Library);
        if (_.isArray(vm.Library) && vm.Library.length) {
            // Open Root
            GetItem().Open = true;

            var dirId = params.path || "";
            vm.SelectItem(GetItem(dirId));
        }
        vm.Ready = true;
    };

    /* =============== Controller Section =============== */

    vm.ToggleMenu = function () {
        vm.ShowMenu = !vm.ShowMenu;
    };

    vm.SelectItem = function (item) {
        var itemId = item ? (item.Id || "") : "";
        // if item has valid Id and is not selected, then select it.
        if (!vm.CurrentDirectoryId || (vm.CurrentDirectoryId && vm.CurrentDirectoryId !== itemId)) {
            SelectDirectory(itemId);
        }
    };

    vm.SelectItemCaret = function (item, force) {
        // If item is a directory and has a parent
        if (item && item.IsDirectory && item.ParentDirectoryId) {
            // If the Open property is defined, toggle it and set folder Icon.
            if (_.isUndefined(item.Open) || force) {
                item.Open = true;
                item.Icon = "fa fa-folder-open-o";
            } else {
                item.Open = !item.Open;
                item.Icon = item.Open ? "fa fa-folder-open-o" : "fa fa-folder-o";
            }
        } else {
            item.Open = true;
            item.Icon = "fa fa-home";
        }

        return item;
    };

    $rootScope.$on("Directory:Path",
        function (event, dirId) {
            if (dirId === "") {
                dirId = "00000000-0000-0000-0000-000000000000";
            }
            recursivelyOpenParents(dirId);

            vm.CurrentDirectory = GetItem(dirId);
            vm.CurrentDirectoryId = dirId;

        }
    );

    function GetItem(id) {
        if (id) {
            var item = vm.Directory.Node(id);
            if (item) item.Icon = GetIcon(item);
            return item;
        } else {
            var item = vm.Directory.Node(vm.Directory.GetRoot());
            if (item) item.Icon = GetIcon(item);
            return item;
        }
    };

    function SelectDirectory(dirId) {
        vm.CurrentDirectory = GetItem(dirId);
        vm.CurrentDirectoryId = dirId;
        if (!dirId || dirId !== "00000000-0000-0000-0000-000000000000") {
            $state.go("zen.app.pia.module.documents.repository.main", { path: dirId }, { location: true, notify: false });
            $rootScope.$broadcast("Directory:Path", dirId);
        } else {
            $state.go("zen.app.pia.module.documents.repository.main", { path: "" }, { location: true, notify: false });
            $rootScope.$broadcast("Directory:Path", "");
        }
    };

    function recursivelyOpenParents(Id) {
        var item = GetItem(Id);
        if (item.FileName === "Home") {
            item.FileName = $rootScope.t(item.FileName);
        }
        if (item.IsDirectory) {
            item = vm.SelectItemCaret(item, true);
            if (item.ParentDirectoryId) {
                recursivelyOpenParents(item.ParentDirectoryId);
            }
        }
    };

    function recursivelySetIcon(arr) {
        if (_.isArray(arr) && arr.length) {
            _.each(arr, function (item) {
                item.Icon = GetIcon(item);
                if (_.isArray(item.Children) && item.Children.length) {
                    recursivelySetIcon(item.Children);
                }
            });
        }
        return arr;
    };

    function GetIcon(item) {
        var icon = item.Icon || "fa fa-folder-o";
        if (item.IsDirectory) {
            // Set Root Icon
            if (!item.ParentDirectoryId) return "fa fa-home";
            // Set Directory Icon based on if it is open.
            if (item.Open) {
                return "fa fa-folder-open-o";
            } else {
                return "fa fa-folder-o";
            }
        } else {
            DocumentFactory.fontPicker(item.FileExtension);
        }
        return icon;
    };

    init();
}

DocumentsMenuCtrl.$inject = ["$rootScope", "$state", "$q", "$timeout", "currentUser", "$uibModal", "Documents", "DocumentsDirectory", "DocumentFactory", "Permissions"];

