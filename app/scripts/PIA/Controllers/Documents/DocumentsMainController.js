
"use strict";

export default function DocumentsMainCtrl($rootScope, $state, $q, $timeout, $filter, $uibModal, Documents, DocumentsDirectory, DocumentFactory, ENUMS, Permissions, Wootric, NotifyService) {
    /* jshint validthis:true */
    var vm = this;
    var enums = ENUMS;
    var PromiseArray = [];
    var UploadTypes = {
        Folder: 10,
        File: 20
    };
    vm.permissions = {
        DocumentsCreate: Permissions.canShow("DocumentsCreate"),
        DocumentsDelete: Permissions.canShow("DocumentsDelete"),
        DocumentsDetails: Permissions.canShow("DocumentsDetails"),
        DocumentsDownload: Permissions.canShow("DocumentsDownload"),
        DocumentsList: Permissions.canShow("DocumentsList"),
        DocumentsTree: Permissions.canShow("DocumentsTree"),
    };
    vm.Ready = false;
    vm.Directory = DocumentsDirectory;
    vm.CurrentDirectory = null;
    vm.CurrentDirectoryId = "";
    vm.ShowDetails = false;

    function init(p) {
        vm.Ready = false;
        var path = p;
        Documents.list(path).then(
            function (response) {
                vm.CurrentDirectory = GetItem(path);
                if (vm.CurrentDirectory) {
                    vm.CurrentDirectory.Children = recursivelySetIcon(UpsertFiles((response.result ? response.data : vm.CurrentDirectory.Children)));
                    vm.Directory.RecursiveAdd([vm.CurrentDirectory]);
                    vm.CurrentDirectoryId = vm.CurrentDirectory.Id;
                    vm.Selected = GetItem(vm.CurrentDirectory.Id);
                    GetDetails();
                }
                vm.Ready = true;
            }
        );
        Wootric.run("Documents");

    }

    $rootScope.$on("Directory:Path",
        function (event, dirId) {
            vm.CurrentDirectory = GetItem(dirId);
            vm.CurrentDirectoryId = dirId;
            init(dirId);
        }
    );

    /* =============== Controller Section =============== */

    vm.ToggleMenu = function () {
        vm.ShowMenu = !vm.ShowMenu;
    };

    vm.ToggleDetails = function () {
        vm.ShowDetails = !vm.ShowDetails;
    };

    vm.AddNewFolder = function () {
        var modalInstance = $uibModal.open({
            templateUrl: "AddDocumentModal.html",
            controller: "AddDocumentCtrl as vm",
            backdrop: "static",
            keyboard: false,
            resolve: {
                UploadTypes: function () {
                    return UploadTypes;
                },
                Type: function () {
                    return UploadTypes.Folder;
                },
                ParentDirectoryId: function () {
                    return vm.CurrentDirectory.Id;
                }
            }
        });

        modalInstance.result.then(function (response) {
            if (response.result) {
                var doc = response.data;
                doc.Icon = GetIcon(doc);
                doc.Children = [];
                var parent = GetItem(vm.CurrentDirectory.Id);
                if (parent.Children) {
                    parent.Children.push(doc);
                } else {
                    parent.Children = [doc];
                }
                vm.CurrentDirectory = parent;
                vm.Directory.RecursiveAdd([doc]);
            }
        });
    };

    vm.AddNewFile = function () {
        var modalInstance = $uibModal.open({
            templateUrl: "AddDocumentModal.html",
            controller: "AddDocumentCtrl as vm",
            backdrop: "static",
            keyboard: false,
            resolve: {
                UploadTypes: function () {
                    return UploadTypes;
                },
                Type: function () {
                    return UploadTypes.File;
                },
                ParentDirectoryId: function () {
                    return vm.CurrentDirectory.Id;
                }
            }
        });

        modalInstance.result.then(function (response) {
            if (response.result) {
                var doc = response.data;
                doc.Icon = GetIcon(doc);
                var parent = GetItem(vm.CurrentDirectory.Id);
                if (parent.Children) {
                    parent.Children.push(doc);
                } else {
                    parent.Children = [doc];
                }
                vm.CurrentDirectory = parent;
                vm.Directory.RecursiveAdd([doc]);
            }
        });
    };

    vm.DeleteCurrentDirectory = function () {
        if (vm.CurrentDirectory && !vm.CurrentDirectory.isRoot) {
            vm.DeleteDirectory(vm.CurrentDirectory.Id, vm.CurrentDirectory.ParentDirectoryId);
        }
    };

    vm.DeleteDirectory = function (Id, ParentDirectoryId) {
        var deferred = $q.defer();
        if (Id) {
            vm.DeleteItem(Id, ParentDirectoryId).then(
                function (response) {
                    deferred.resolve(response);
                }
            );
        }
        else {
            deferred.resolve({ result: false, data: "" });
        }
        return deferred.promise;
    };

    vm.DeleteItem = function (Id, ParentDirectoryId) {
        var deferred = $q.defer();
        if (Id) {
            var item = GetItem(Id);
            var msg = item.FileName ? $rootScope.t("AreYouSureToDeleteFilename", { FileName: item.FileName })
                : $rootScope.t("AreYouSureToDeleteThisFile");

            NotifyService.confirm(' ', msg).then(
                function (result) {
                    if (result) {
                        Documents.delete(Id).then(
                            function (response) {
                                if (response.result) {
                                    if (ParentDirectoryId) {
                                        var item = GetItem(Id);
                                        var parent = GetItem(ParentDirectoryId);

                                        var index = _.findIndex(parent.Children, ["Id", Id]);
                                        if (index > -1) {
                                            parent.Children.splice(index, 1);
                                        }
                                        if (vm.Selected && vm.Selected.Id === Id) {
                                            vm.Selected = GetItem(ParentDirectoryId);
                                            GetDetails();
                                        }

                                        if (item && item.IsDirectory && vm.CurrentDirectory.Id === Id) {
                                            vm.GoBack();
                                        }
                                    }
                                }
                                deferred.resolve(response);
                            }
                        );
                    }
                }
            );
        }
        else {
            deferred.resolve({ result: false, data: "" });
        }
        return deferred.promise;
    };

    vm.SelectItem = function (item, details, move) {
        var ChangeDir = (!_.isUndefined(move) ? move : true);
        var OpenDetails = (!_.isUndefined(details) ? details : true);
        if (item && item.Id) {
            if (ChangeDir) {
                SelectDirectory(item.Id);
                vm.Selected = GetItem(item.Id);
            } else if (vm.Selected.Id && vm.Selected.Id === item.Id) {
                vm.Selected = GetItem(item.ParentDirectoryId);
            } else {
                vm.Selected = GetItem(item.Id);
            }
            GetDetails();
        }
        if (OpenDetails) vm.ShowDetails = true;
    };

    vm.GoBack = function () {
        SelectDirectory((vm.CurrentDirectory.ParentDirectoryId || ""));
    };

    function SelectDirectory(dirId) {
        if (!dirId || dirId !== enums.EmptyGuid) {
            $state.go("zen.app.pia.module.documents.repository.main", { path: dirId }, { location: true, notify: false });
            $rootScope.$broadcast("Directory:Path", dirId);
        }
        else {
            $state.go("zen.app.pia.module.documents.repository.main", { path: "" }, { location: true, notify: false });
            $rootScope.$broadcast("Directory:Path", "");
        }
    }

    function GetDetails() {
        if (vm.Selected && vm.Selected.Id && !vm.GettingDetails && vm.Selected.Id !== enums.EmptyGuid) {
            vm.GettingDetails = true;
            return Documents.get(vm.Selected.Id).then(
                function (response) {
                    if (response.result) {
                        var temp = response.data;
                        temp.Children = vm.Selected ? (vm.Selected.Children || []) : [];
                        temp.Icon = vm.Selected.Icon;
                        temp.Open = vm.Selected.Open;
                        temp.$$hashKey = vm.Selected.$$hashKey;
                        vm.Selected = temp;
                    }
                    vm.GettingDetails = false;
                }
            );
        }
    }

    function GetItem(id) {
        var item;
        if (id) {
            item = vm.Directory.Node(id);
            if (item) item.Icon = GetIcon(item);
            return item;
        } else {
            item = vm.Directory.Node(vm.Directory.GetRoot());
            if (item) item.Icon = GetIcon(item);
            return item;
        }
    }

    function recursivelySetIcon(arr) {
        if (_.isArray(arr) && arr.length) {
            _.each(arr, function(item) {
                item.Icon = GetIcon(item);
                if (_.isArray(item.Children) && item.Children.length) {
                    recursivelySetIcon(item.Children);
                }
            });
        }
        return arr;
    }

    function recursivelyOpenParents(Id) {
        var item = GetItem(Id);
        if (item.IsDirectory) {
            item = vm.SelectItemCaret(item, true);
            if (item.ParentDirectoryId) {
                recursivelyOpenParents(item.ParentDirectoryId);
            }
        }
    }

    function GetIcon(item) {
        var icon = item.Icon || "fa fa-folder-o";
        if (item.IsDirectory) {
            if (!item.ParentDirectoryId) return "fa fa-home"; // Set Root Icon

            if (item.Open) {
                return "fa fa-folder-open-o";
            } else {
                return "fa fa-folder-o";
            }
        } else {
            DocumentFactory.fontPicker(item.FileExtension);
        }
        return icon;
    }

    function UpsertFiles(updated) {
        var merge = [];
        if (_.isArray(updated)) {
            _.each(updated, function(item, index) {
                // find matching original
                var existing = GetItem(item.Id);
                if (!_.isUndefined(existing)) {
                    // if original found, then copy over Children.
                    item.Children = existing.Children;
                    merge.push(item);
                } else {
                    merge.push(item);
                }
            });
            _.each(merge, function (item) {
                item.Icon = GetIcon(item);
            });
        } else {
            merge = (updated);
        }

        return merge;
    }

    vm.createDocumentOptions = [
        {
            text: $rootScope.t("File"),
            action: vm.AddNewFile,
        },
        {
            text: $rootScope.t("Folder"),
            action: vm.AddNewFolder,
        }
    ];

    vm.download = function(event, documentId, fileName) {
        if (event) {
            event.stopPropagation();
        }
        Documents.download(documentId, fileName);
    }

    init($state.params.path);

}

DocumentsMainCtrl.$inject = ["$rootScope", "$state", "$q", "$timeout", "$filter", "$uibModal", "Documents", "DocumentsDirectory", "DocumentFactory", "ENUMS", "Permissions", "Wootric", "NotifyService"];
