
"use strict";

export default function AddDocumentCtrl($rootScope, $scope, $uibModalInstance, Documents, UploadTypes, Type, ParentDirectoryId) {
    /* jshint validthis:true */

    var vm = this;

    vm.Loading = false;
    vm.Disabled = true;
    vm.UploadTypes = UploadTypes;
    vm.UploadType = Type;

    switch (vm.UploadType) {
        case vm.UploadTypes.Folder:
            vm.TypeText = $rootScope.t("CreateFolder");
            break;
        case vm.UploadTypes.File:
            vm.TypeText = $rootScope.t("UploadFile");
            break;
        default:
            vm.TypeText = $rootScope.t("UploadFile");
    }

    vm.AllowMultiple = false;
    vm.Accept = "";

    vm.file = {
        ParentDirectoryId: ParentDirectoryId,
        FileName: "",
        Extension: "",
        IsDirectory: (vm.UploadType === vm.UploadTypes.Folder),
        Data: ""
    };

    vm.InputChanged = function (file) {
        if (!_.isNil(file.FileName)) {
            vm.Disabled = false;
        } else {
            vm.Disabled = true;
        }
    };

    function hasSuffix(file) {
        return _.includes(file.FileName, file.Extension);
    }

    vm.CreateDocument = function () {
        vm.Disabled = true;
        vm.Loading = true;
        Documents.add(vm.file).then(
            function (response) {
                if (response.result) {
                    $uibModalInstance.close(response);
                }
                vm.Disabled = false;
                vm.Loading = false;
            }
        );
    };

    vm.cancel = function () {
        $uibModalInstance.dismiss("cancel");
    };

    vm.UploadFinished = function (files) {
        vm.Disabled = true;
        vm.Loading = true;
        if (_.isArray(files) && files.length) {
            vm.file.FileName = files[0].FileName + "." + files[0].Extension;
            vm.file.Data = files[0].Data;
            vm.file.Extension = files[0].Extension;
            vm.Disabled = false;
            vm.Loading = false;
            vm.file.Blob = files[0].Blob;
        }
    };

}

AddDocumentCtrl.$inject = ["$rootScope", "$scope", "$uibModalInstance", "Documents", "UploadTypes", "Type", "ParentDirectoryId"];
