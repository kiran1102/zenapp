declare var moment: any;
import Utilities from "Utilities";
import { each, find, map, filter, includes, toLower } from "lodash";
import TreeMap from "TreeMap";
import SelfService from "sharedServices/self-service.service";
import {
    getAllUsers,
    getFilteredUsers,
    getMultiFilteredUsers,
    getDisabledUsers,
    isActive,
    isNotProjectViewer,
    isProjectViewer,
    getCurrentUser,
} from "oneRedux/reducers/user.reducer";
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import { IStringMap } from "interfaces/generic.interface";
import { IProtocolPacket } from "interfaces/protocol.interface";
import { IStore } from "interfaces/redux.interface";
import { IUser } from "interfaces/user.interface.ts";
import { IAssignment } from "interfaces/assignment.interface";
import { ISelfServiceConfig } from "interfaces/self-service.interface";
import { IProjectCreateParams } from "interfaces/project.interface";
import { ISectionZenApi } from "interfaces/section.interface";
import { AssessmentTypes } from "constants/assessment.constants";
import { NotificationService } from "modules/shared/services/provider/notification.service";

export default class PortalModalCtrl {

    static $inject: string[] = ["$scope", "$state", "$q", "$uibModal", "$rootScope", "Permissions", "SettingStore", "store", "Projects", "Templates", "Users", "OrgGroupStore", "Tags", "ENUMS", "Labels", "$uibModalInstance", "TemplateId", "TemplateVersion", "SelfService", "OrgGroupStoreNew", "OrgGroups", "NotificationService"];

    private sectionAssignments: IAssignment[];
    private currentUser: IUser;
    private saveSettingProjectLeadId: boolean;
    private dataMappingEnum: number;
    private templateType: number;
    private routes: IStringMap<string>;

    constructor(
        private readonly $scope: any,
        private readonly $state: ng.ui.IStateService,
        private readonly $q: ng.IQService,
        private readonly $uibModal: any,
        private readonly $rootScope: IExtendedRootScopeService,
        private readonly Permissions: any,
        private readonly SettingStore: any,
        private readonly store: IStore,
        private readonly Projects: any,
        private readonly Templates: any,
        private readonly Users: any,
        private readonly OrgGroupStore: any,
        private readonly Tags: any,
        private readonly ENUMS: any,
        private readonly Labels: any,
        private readonly $uibModalInstance: any,
        private readonly TemplateId: any,
        private readonly TemplateVersion: any,
        private readonly selfService: SelfService,
        private readonly OrgGroupStoreNew: any,
        private readonly OrgGroups: any,
        private notificationService: NotificationService,
    ) {
        this.sectionAssignments = [];
        this.saveSettingProjectLeadId = false;
        this.dataMappingEnum = ENUMS.TemplateTypes.Datamapping;
        this.templateType = $state.includes(AssessmentTypes.DM.BaseRoute) ? this.dataMappingEnum : ENUMS.TemplateTypes.Custom;

        this.routes = {
            project: (this.templateType === this.dataMappingEnum) ? "zen.app.pia.module.datamap.views.assessment-old.single.module.questions" : "zen.app.pia.module.projects.assessment.module.questions",
            projects: (this.templateType === this.dataMappingEnum) ? "zen.app.pia.module.datamap.views.assessment.list" : "zen.app.pia.module.projects.list",
            createForm: (this.templateType === this.dataMappingEnum) ? "zen.app.pia.module.datamap.views.assessment.wizard.project_info" : "zen.app.pia.module.projects.wizard.project_info",
            selectForm: (this.templateType === this.dataMappingEnum) ? "zen.app.pia.module.datamap.views.assessment.wizard.project_type" : "zen.app.pia.module.projects.wizard.project_type",
            assignment: (this.templateType === this.dataMappingEnum) ? "zen.app.pia.module.datamap.views.assessment.wizard.project_assignment" : "zen.app.pia.module.projects.wizard.project_assignment",
        };

        $scope.currentUser = getCurrentUser(store.getState());
        $scope.isTypedEmailDisabledUser = false;
        $scope.showDeadline = Permissions.canShow("ProjectsDeadline");
        $scope.selectAssignee = false;
        $scope.selectOrgGroup = false;
        $scope.wizard = {
            Project: {},
        };
        $scope.placeholder = {
            name: $rootScope.t("EnterProjectName"),
            description: $rootScope.t("EnterProjectDescription"),
            lead: $rootScope.t("AssignProjectRespondent"),
        };
        $scope.error = {
            lead: $rootScope.t("PleaseAssignValidProjectRespondent"),
        };

        if (Permissions.canShow("AssessmentRespondentReassign") && Permissions.canShow("ProjectsAssignEmails")) {
            $scope.placeholder.lead = $rootScope.t("AssignADefaultRespondentOrInviteViaEmail");
            $scope.error.lead = $rootScope.t("PleaseAssignADefaultRespondent");
        } else if (Permissions.canShow("AssessmentRespondentReassign")) {
            $scope.placeholder.lead = $rootScope.t("AssignProjectRespondent");
            $scope.error.lead = $rootScope.t("PleaseAssignValidProjectRespondent");
        } else if (Permissions.canShow("ProjectsAssignEmails")) {
            $scope.placeholder.lead = $rootScope.t("InviteProjectRespondentViaEmail");
            $scope.error.lead = $rootScope.t("ErrorEnterValidEmailAddress");
        }

        $scope.cancel = (): void => {
            $uibModalInstance.dismiss("cancel");
        };

        $scope.formatLeadLabel = (model): string => {
            if (model) {
                if ($scope.currentUser && $scope.currentUser.Id && ($scope.currentUser.Id === model) || Utilities.matchRegex(model, ENUMS.Regex.Guid)) {
                    return Users.formatTeamLabel($scope.Team, model);
                }

                if (Utilities.matchRegex(model, ENUMS.Regex.Email)) {
                    return model;
                }
            }
            return "";
        };

        $scope.assignToMe = (): void => {
            $scope.wizard.MultipleRespondentShow = false;
            $scope.wizard.Project.LeadId = $scope.currentUser.Id;
            $scope.saveTooltip = "and Begin Assessment";
        };

        $scope.$watch("wizard.Project.LeadId", (newValue: string, oldValue: string): void => {
            if ($scope.template && !this.saveSettingProjectLeadId && !$scope.wizard.MultipleRespondentShow && newValue) {
                const sendArray = [];
                const projectVersion = ($scope.wizard.SelectedVersion && $scope.mode === "version") ? $scope.wizard.SelectedVersion : "1";
                each($scope.template.Sections, (section: ISectionZenApi): void => {
                    const assignment: IAssignment = {
                        AssigneeId: newValue,
                        SectionId: section.Id,
                        ProjectVersion: projectVersion,
                        Comment: $scope.wizard.Project.Comment,
                    };
                    sendArray.push(assignment);
                });
                this.sectionAssignments = sendArray;
            }
        });

        $scope.assignToSomeoneElse = (): void => {
            $scope.wizard.Project.LeadId = "";
            $scope.saveTooltip = "";
        };

        $scope.updateDeadline = (deadline: Date) => {
            $scope.wizard.Project.Deadline = deadline;
            $scope.validateReminder();
        };

        $scope.validateReminder = (): void => {
            if ($scope.wizard.ReminderShow) {
                if ($scope.wizard.Project.Deadline) {
                    const dayDiff = Math.abs(moment($scope.wizard.Project.Deadline).diff(moment(), "days")) + 1;
                    const deadlineCheck = dayDiff > 1;
                    const reminderCheck = $scope.wizard.Project.Reminder > 0 && dayDiff > $scope.wizard.Project.Reminder;
                    $scope.validReminder = deadlineCheck && reminderCheck;
                } else {
                    $scope.validReminder = false;
                }
            } else {
                $scope.validReminder = true;
            }
        };

        $scope.Save = (): void => {
            $scope.saveInProgress = true;
            this.saveSettingProjectLeadId = true;

            if (!this.$scope.selectAssignee) {
                this.$scope.wizard.Project.LeadId = this.$scope.currentUser.Id;
            }
            if (!this.$scope.selectOrgGroup) {
                this.$scope.wizard.Project.OrgGroupId = this.$scope.currentUser.OrgGroupId;
            }

            if (this.validationSuccess()) {
                const [ project, createMethod ]: any[] = this.buildProjectParams();
                this.createProject(project, createMethod).then(() => {
                    $scope.saveInProgress = false;
                });
            }
        };

        $scope.orgSelected = (action, payload) => {
            if (action === "ORG_SELECTED") {
                $scope.selectedOrg = payload.org;
                $scope.wizard.Project.OrgGroupId = payload.org.id;
                return;
            }
        };

        $scope.isTypedUserEmailDisabled = (typedEmail: string): void => {
            const typedDisabledUser: IUser = find($scope.disabledUserList, (specificUser: IUser): boolean => {
                return (toLower(specificUser.Email) === toLower(typedEmail));
            });
            $scope.isTypedEmailDisabledUser = Boolean(typedDisabledUser);
        };

        this.init();
    }

    private init(): void {
        this.$scope.loading = true;
        this.$scope.saveInProgress = false;

        this.$scope.validReminder = true;
        this.$scope.saveTooltip = "";

        this.$scope.orgList = this.OrgGroupStoreNew.orgList;
        this.$scope.orgTable = this.OrgGroupStoreNew.orgTable;
        if (this.OrgGroupStoreNew.orgList.length === 1) this.$scope.selectedOrg = this.OrgGroupStoreNew.selectedOrg;

        const promises: any[] = [
            this.getUsers(),
            this.getSettings(),
            this.getOrganizations(),
            this.getTemplateData(),
            this.getTemplateSelfServiceSettings(),
        ];

        if (this.$scope.mode === "version") {
            promises.push(this.getProjectInfo(this.$state.params.id));
        }

        this.$q.all(promises).then((res: any[]): void => {
            const projectData: any = res[5];
            this.setWizardDefaults(projectData);

            const templateSettings: any = res[3];
            if (!templateSettings.manualAssigneeEnabled) {
                this.$scope.wizard.Project.LeadId = this.$scope.currentUser.Id;
                this.updateBulkSectionAssignments(this.$scope.currentUser.Id);
            }
            if (!templateSettings.manualOrgGroupEnabled) {
                this.$scope.wizard.Project.OrgGroupId = this.$scope.currentUser.OrgGroupId;
            }
            this.$scope.loading = false;
        });
    }

    private getUsers(): ng.IPromise<void> {
        const users: IUser[] = getAllUsers(this.store.getState());
        this.$scope.disabledUserList = getDisabledUsers(this.store.getState());
        this.$scope.Team = getMultiFilteredUsers([isActive, isNotProjectViewer])(this.store.getState());
        this.$scope.projectViewerEmails = map(getFilteredUsers(isProjectViewer)(this.store.getState()), (user: IUser): string => user.Email);
        this.$scope.TeamPicker = filter(this.$scope.Team, (user: IUser): boolean => user.Id !== this.$scope.currentUser.Id);
        return this.$q.when();
    }

    private getSettings(): ng.IPromise<void> {
        return this.SettingStore.fetchProjectSettings().then((response: any): any | boolean => {
            if (response) {
                this.$scope.Settings = response;
                return this.$scope.Settings;
            }
            return false;
        });
    }

    private getOrganizations(): ng.IPromise<void> {
        return this.OrgGroups.rootTree().then((response: any): any => {
            if (response.data) {
                this.$scope.TreeMap = new TreeMap("Id", "ParentId", "Children", response.data[0]);
                return this.$scope.TreeMap;
            }
            this.$scope.OrganizationTree = this.OrgGroupStoreNew.orgTree;
            return this.$scope.OrganizationTree;
        });
    }

    private getProjectInfo(id: string): ng.IPromise<any | boolean> {
        return this.Projects.read(id).then((response: any): any | boolean => {
            if (response.result) {
                return response.data;
            }
            return false;
        });
    }

    private setWizardDefaults(projectData: any): void {
        const reminder = this.$scope.Settings.Reminder ? this.$scope.Settings.Reminder : {
            Show: false,
            Value: 1,
        };

        this.$scope.wizard = {
            ProjectType: this.$scope.mode,
            SelectedId: this.$state.params.id || "", // ID of either the Template or the Project
            SelectedVersion: (this.$scope.mode === "version" ? + this.$state.params.version + 1 : this.$state.params.version) || "", // If creating new version, increment by 1
            // Our Project Object
            Project: {
                Id: "",
                StateDesc: "Not Started",
                Name: projectData ? projectData.Name : "", // Project Name
                Description: projectData ? projectData.Description : "", // Project Description
                OrgGroupId: "", // Organization
                LeadId: this.$scope.currentUser.Id, // Project Respondent
                Deadline: null, // Project Deadline
                Reminder: reminder.Value, // Project Reminder
                TemplateId: "", // Project TemplateId
                TemplateName: projectData ? projectData.TemplateName : "",
                Tags: [],
            },
            MinDate: new Date(),
            ReminderShow: reminder.Show,
            MultipleRespondentShow: false,
            IsDefault: true,
        };

        this.$scope.$watch("wizard.Project.Reminder", (newValue: any, oldValue: any): void => {
            this.$scope.wizard.IsDefault = (newValue === this.$scope.Settings.Reminder.Value);
        });

        if (this.$scope.wizard.Project.OrgGroupId) {
            this.$scope.selectedOrg = this.$scope.orgTable[this.$scope.wizard.Project.OrgGroupId];
        } else if (this.OrgGroupStoreNew.orgList.length === 1) {
            this.$scope.wizard.Project.OrgGroupId = this.$scope.orgList[0].id;
        }
    }

    private getTemplateSelfServiceSettings(): ng.IPromise<any | boolean> {
        const selfServiceParams: ISelfServiceConfig = {
            TemplateId: this.TemplateId,
            TemplateVersion: this.TemplateVersion,
        };

        return this.selfService.readConfiguration(selfServiceParams).then((response: IProtocolPacket): any | boolean => {
            if (response.result) {
                return response.data;
            }
            return false;
        }).then(this.saveTemplateSelfServiceDataToScope.bind(this));
    }

    private saveTemplateSelfServiceDataToScope(templateData: any): any | boolean {
        if (templateData) {
            const manualAssigneeEnabled: boolean = templateData.SelectAssignee || false;
            const manualOrgGroupEnabled: boolean = templateData.SelectOrgGroup || false;

            this.$scope.selectAssignee = manualAssigneeEnabled;
            this.$scope.selectOrgGroup = manualOrgGroupEnabled;

            return { manualAssigneeEnabled, manualOrgGroupEnabled };
        }
        return false;
    }

    private getTemplateData(): ng.IPromise<void> {
        const templateParams: any = {
            id: this.TemplateId,
            version: this.TemplateVersion,
        };

        return this.Templates.readTemplate(templateParams).then((response: any): any | boolean => {
            if (response.result) {
                this.$scope.template = response.data;
                return response.data;
            }
            return false;
        });
    }

    private updateBulkSectionAssignments(newValue: string): void {
        const projectVersion = (this.$scope.wizard.SelectedVersion && this.$scope.mode === "version") ? this.$scope.wizard.SelectedVersion : "1";
        const sections = this.$scope.template ? this.$scope.template.Sections : {};
        if (sections) {
            this.sectionAssignments = map(sections, (section: ISectionZenApi): IAssignment => {
                return {
                    AssigneeId: newValue || "",
                    SectionId: section.Id,
                    ProjectVersion: projectVersion,
                    Comment: this.$scope.wizard.Project.Comment || "",
                };
            });
        }
    }

    private buildProjectParams(): any[] {
        let createMethod: any;
        const project: IProjectCreateParams = {
            Name: this.$scope.wizard.Project.Name,
            Description: this.$scope.wizard.Project.Description,
            OrgGroupId: this.$scope.wizard.Project.OrgGroupId,
            LeadId: this.$scope.wizard.Project.LeadId,
            Comment: this.$scope.wizard.Project.Comment,
            Tags: this.$scope.wizard.Project.Tags,
            Deadline: this.$scope.wizard.Project.Deadline,
        };

        if (!this.$scope.wizard.Project.Deadline) {
            project.Deadline = null;
            project.ReminderDays = null;
            this.$scope.wizard.ReminderShow = false;
        }

        if (this.$scope.mode === "version") {
            project.ProjectGuid = this.$scope.wizard.SelectedId;
            createMethod = this.Projects.createVersion;
        } else {
            project.TemplateId = this.TemplateId;
            project.TemplateVersion = this.TemplateVersion;
            project.TemplateType = this.templateType;
            createMethod = this.Projects.create;
        }

        project.SectionAssignments = this.sectionAssignments;

        return [ project, createMethod ];
    }

    private createProject(project: IProjectCreateParams, createMethod: any): ng.IPromise<undefined | void> {
        return createMethod(project).then((response: any): undefined | void => {
            if (response.result) {
                this.$scope.wizard.Project.Id = response.data;

                if (this.$scope.wizard.Project.LeadId !== this.$scope.currentUser.Id) {
                    this.notificationService.alertSuccess(this.$rootScope.t("Success"), this.$rootScope.t("Notification.Success.ProjectCreatedAssigned"));
                } else {
                    this.$state.go(this.routes.project, {
                        Id: response.data,
                        Version: this.$scope.mode === "version" ? this.$scope.wizard.SelectedVersion : 1,
                    });
                }
            }
            this.$uibModalInstance.close();
            return;
        });
    }

    private validationSuccess(): boolean {
        if (includes(this.$scope.projectViewerEmails, this.$scope.wizard.Project.LeadId)) {
            this.notificationService.alertError(this.$rootScope.t("Error"), this.$rootScope.t("Error.RespondentEmailIsProjectViewer"));
            this.$scope.isSubmitted = false;
            return false;
        }
        if (!Utilities.matchRegex(this.$scope.wizard.Project.OrgGroupId, this.ENUMS.Regex.Guid)) {
            this.$scope.wizard.Project.OrgGroupId = "";
            this.notificationService.alert(this.$rootScope.t("Warning"), this.$rootScope.t("PleaseSelectValidOrganizationGroup"));
            this.$scope.saveInProgress = false;
            return false;
        }
        if (!(Utilities.matchRegex(this.$scope.wizard.Project.LeadId, this.ENUMS.Regex.Guid) ||
            Utilities.matchRegex(this.$scope.wizard.Project.LeadId, this.ENUMS.Regex.Email))) {
            this.$scope.wizard.Project.LeadId = "";
            this.notificationService.alert(this.$rootScope.t("Warning"), this.$rootScope.t("PleaseSelectValidProjectRespondent"));
            this.$scope.saveInProgress = false;
            return false;
        }
        return this.respondentCheck();
    }

    private respondentCheck(): boolean {
        let isValid = true;
        each(this.sectionAssignments, (assignment: IAssignment): void => {
            if (!assignment.AssigneeId) {
                isValid = false;
            }
        });

        // If lead is empty just set it to the first section respondent. Its a placeholder property because of section assigments.
        if (isValid && !this.$scope.wizard.Project.LeadId) {
            this.$scope.wizard.Project.LeadId = this.sectionAssignments[0].AssigneeId;
        }
        return isValid;
    }
}
