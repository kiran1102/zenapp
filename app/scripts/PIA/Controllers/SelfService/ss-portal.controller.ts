import Utilities from "Utilities";
import moment, { Moment } from "moment";
import "moment-timezone";
import { isUndefined, filter, each, map } from "lodash";
import {
    isRiskSummaryEnabled,
    getProjectSettings,
} from "oneRedux/reducers/setting.reducer";

import { ProjectService } from "oneServices/project.service";
import { SettingActionService } from "settingsModule/services/actions/setting-action.service";
import TemplateService from "oneServices/templates.service";
import { OneRisk } from "sharedServices/one-risk.service";
import { ModalService } from "sharedServices/modal.service";

import {
    AssessmentStates,
    AssessmentTypes,
 } from "constants/assessment.constants";

import {
    IProjectGetPageResponseNormalized,
    IProjectPageResponse,
    IPiaAssessmentSelfService,
} from "interfaces/assessment.interface";
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import { IStore } from "interfaces/redux.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";
import { Permissions } from "modules/shared/services/helper/permissions.service";

import { TemplateTypes } from "enums/template-type.enum";
import { SortTypes } from "enums/general.enum";
import { IProjectCreateParams } from "interfaces/project.interface";

export default class SelfServicePortalCtrl {
    constructor(
        $rootScope: IExtendedRootScopeService,
        $scope: any,
        $state: ng.ui.IStateService,
        $q: ng.IQService,
        Projects: ProjectService,
        $uibModal: any,
        $localStorage: any,
        Templates: TemplateService,
        Labels: any,
        oneRisk: OneRisk,
        modalService: ModalService,
        permissions: Permissions,
        PRINCIPAL: any,
        store: IStore,
        SettingAction: SettingActionService,
    ) {
        $scope.templateTypes = TemplateTypes;
        $scope.templateType = TemplateTypes.SelfService;
        $scope.sortTypes = SortTypes;
        $scope.routes = {
            single: ($scope.templateType === TemplateTypes.Datamapping) ? "zen.app.pia.module.datamap.views.assessment-old.single.module.questions" : "zen.app.pia.module.projects.assessment.module.questions",
            projects: ($scope.templateType === TemplateTypes.Datamapping) ? "zen.app.pia.module.datamap.views.assessment.list" : "zen.app.pia.module.projects.list",
            createForm: ($scope.templateType === TemplateTypes.Datamapping) ? "zen.app.pia.module.datamap.views.assessment.wizard.project_info" : "zen.app.pia.module.projects.wizard.project_info",
        };
        $scope.labels = Labels.getLabels("projectList", $scope.templateType);

        $scope.sortType = "created";    // set the default sort type
        $scope.sortReverse = true;      // set the default sort order
        $scope.fade = !$scope.fade;
        $scope.projectsLoading = true;
        $scope.filtered = false;
        $scope.filterOpen = false;

        $scope.maxSize = 3;
        $scope.pageSize = 5;
        $scope.pageNumber = 1;
        $scope.pageMetadata = {
            TotalItemCount: 0,
            ItemCount: 0,
            TotalPageCount: 1,
        };

        $scope.Ready = false;
        $scope.templateCount = 0;
        $scope.templateFilters = [
            { label: $rootScope.t("AllTemplates"), value: null },
        ];

        $scope.filter = {
            stateFilter: !isUndefined($localStorage.projectsStateFilter) ? $localStorage.projectsStateFilter : null,
            searchFilter: "",
            templateFilter: !isUndefined($localStorage.projectsTemplateFilter) ? $localStorage.projectsTemplateFilter : null,
        };

        $scope.currentUser = PRINCIPAL.getUser();
        $scope.projectLaunching = false;
        $scope.loadingId = "";

        const templateType: any = $state.includes(AssessmentTypes.DM.BaseRoute) ? TemplateTypes.Datamapping : TemplateTypes.Custom;
        const routes: any = {
            project: (templateType === TemplateTypes.Datamapping) ? "zen.app.pia.module.datamap.views.assessment-old.single.module.questions" : "zen.app.pia.module.projects.assessment.module.questions",
        };

        const zipPermissions = (): any => ({ canShowProjectsDeadline: permissions.canShow("ProjectsDeadline") });

        function getTemplatesList(): ng.IPromise<boolean> {
            return Templates.latestPublishedList($scope.templateType).then((response: any): boolean => {
                if (response.result) {
                    $scope.templates = filter(response.data, Templates.excludeThresholdTemplates);
                    $scope.templateCount = $scope.templates.length;
                    return true;
                }
                return false;
            });
        }

        getTemplatesList().then((): void => {
            $scope.permissions = zipPermissions();
            $scope.updatePage();
        });

        $scope.pageRange = (): string => {
            const firstNum = $scope.pageNumber === 1 ? 1 : ($scope.pageNumber - 1) * $scope.pageSize + 1;
            const secondNum = $scope.pageNumber === $scope.pageMetadata.TotalPageCount ? $scope.pageMetadata.TotalItemCount : (firstNum - 1) + $scope.pageSize;
            return firstNum + " - " + secondNum;
        };

        $scope.getProjectPage = (pageSize: number, pageNumber: number, filters: any, sortType: string, sortReverse: boolean): void => {
            const paginationOptions: any = {
                pageSize,
                pageNumber,
            };

            const filterOptions: any[] = [
                {
                    filterType: "state",
                    filterValue: [
                        AssessmentStates.InProgress,
                        AssessmentStates.UnderReview,
                        AssessmentStates.InfoNeeded,
                        AssessmentStates.RiskTreatment,
                    ],
                },
            ];

            if (filters.templateFilter) {
                filterOptions.push({
                    filterType: "template",
                    filterValue: filters.templateFilter,
                });
            }

            const sortOptions: any = {
                sortType,
                sortReverse,
            };

            $scope.projectsLoading = true;

            const promises = [];

            if (!getProjectSettings(store.getState())) {
                promises.push(SettingAction.fetchProjectSettings());
            }

            const getProjectPromise = Projects.getPage($scope.templateType, paginationOptions, filterOptions, sortOptions).then((response: IProtocolResponse<IProjectGetPageResponseNormalized>) => {
                $scope.projectsLoading = false;
                if (response.result) {
                    $scope.pageMetadata = response.data.metadata;
                    $scope.projects = map(response.data.page, (p: IProjectPageResponse): IPiaAssessmentSelfService => $scope.formatProject(p));
                } else {
                    $scope.projects = [];
                }
            });

            promises.push(getProjectPromise);

            $q.all(promises).then(() => {
                $scope.Ready = true;
            });
        };

        $scope.formatProject = (project: IProjectPageResponse): IPiaAssessmentSelfService => {
            const timezone: string = moment.tz.guess();
            const dt: Moment = moment.utc(project.CreatedDT);
            let formattedProject: IPiaAssessmentSelfService = {
                ...project,
                localeCreatedDT: dt.tz(timezone).format("L"),
                CreatedDTtooltip: dt.tz(timezone).format("lll z"),
            };

            if (project.Deadline) {
                const dl = moment.utc(project.Deadline);
                formattedProject = {
                    ...formattedProject,
                    localeDeadline: dl.tz(timezone).format("L"),
                    Deadlinetooltip: dl.tz(timezone).format("lll z"),
                };
            }

            return oneRisk.applyStateLabels(formattedProject, formattedProject.StateDesc);
        };

        $scope.updatePage = (): void => {
            $scope.filter.stateFilter = null;
            $localStorage.projectsStateFilter = $scope.filter.stateFilter;
            $localStorage.projectsTemplateFilter = $scope.filter.templateFilter;
            if ($scope.filter.stateFilter !== null || $scope.filter.templateFilter !== null || $scope.filter.searchFilter) {
                $scope.filtered = true;
            } else {
                $scope.filtered = false;
            }
            $scope.getProjectPage($scope.pageSize, $scope.pageNumber, $scope.filter, $scope.sortType, $scope.sortReverse);
        };

        $scope.updateSort = (sortColumn: string, sortDirection: number): void => {
            $scope.sortType = sortColumn;
            $scope.sortReverse = sortDirection === $scope.sortTypes.Descending;
            $scope.sortColumn = sortColumn;
            $scope.sortDirection = sortDirection;

            $scope.updatePage();
        };

        $scope.goToProject = (project: any): void => {
            if (!project.loadingVersion) {
                if (!$scope.isDataMapping &&
                    isRiskSummaryEnabled(store.getState())
                    && (project.StateDesc === AssessmentStates.RiskAssessment
                        || project.StateDesc === AssessmentStates.RiskTreatment
                        || project.StateDesc === AssessmentStates.Completed)
                    && project.ProjectRisk
                ) {
                    $state.go("zen.app.pia.module.projects.risk_summary", {
                        ProjectId: project.Id,
                        Version: project.Version,
                    });
                } else {
                    $state.go($scope.routes.single, {
                        Id: project.Id,
                        Version: project.Version,
                    });
                }
            }
        };

        $scope.CreateProjectModal = (template: any): void => {
            const data: any = {
                TemplateId: template.Id,
                TemplateVersion: template.Version,
            };
            const modalInstance: any = modalService.renderModal(data, "PortalModalCtrl", "portalModal.html", "portalModal", "lg");
        };

        $scope.directLaunchProject = (template: any): ng.IPromise<void> => {
            const project: IProjectCreateParams = {
                Comment: null,
                Deadline: null,
                Description: "",
                LeadId: $scope.currentUser.Id,
                Name: `${$scope.currentUser.FirstName}-${$scope.currentUser.LastName}-${template.Name}-${moment.utc().format("MM-DD-YYYY")}`,
                OrgGroupId: $scope.currentUser.OrgGroupId,
                ReminderDays: null,
                Tags: [],
                TemplateId: template.Id,
                TemplateType: templateType,
                TemplateVersion: template.Version,
            };

            $scope.projectLaunching = true;
            $scope.loadingId = template.Id;

            return Projects.create(project).then((response: any): void => {
                if (response.result) {
                    $state.go(routes.project, {
                        Id: response.data,
                        Version: 1,
                    });
                } else {
                    $scope.projectLaunching = false;
                    $scope.loadingId = "";
                }
            });
        };

        $scope.receiveAction = (action: string, payload: any): void => {
            switch (action) {
                case "GO_TO_PROJECT":
                    if (!$scope.projectVersionLoading) return $scope.goToProject(payload.rowData);
                    return;
                case "SORT_COLUMN":
                    return $scope.updateSort(payload.sortColumn, payload.sortDirection);
            }
        };
    }
}

SelfServicePortalCtrl.$inject = [
    "$rootScope",
    "$scope",
    "$state",
    "$q",
    "Projects",
    "$uibModal",
    "$localStorage",
    "Templates",
    "Labels",
    "OneRisk",
    "ModalService",
    "Permissions",
    "PRINCIPAL",
    "store",
    "SettingAction",
];
