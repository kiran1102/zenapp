
export default function ConditionalController($rootScope, $scope, $uibModalInstance, $q, ENUMS, ConditionGroups, Conditions, Condition, TemplateSections,
    Question, QuestionIndex, ReadOnly, Template, EnableSkip, Permissions) {

    /********** Scope Variables **********/
    var translate = $rootScope.t;
    $scope.permissions = {
        ConditionsAdd: Permissions.canShow("ConditionsAdd"),
        ConditionsDelete: Permissions.canShow("ConditionsDelete"),
    };
    $scope.Ready = false;
    $scope.Error = false;
    $scope.CanSave = true;
    $scope.DisableRisk = false;

    var ConditionTypes = ENUMS.ConditionTypes;
    var QuestionTypes = ENUMS.QuestionTypes;
    var ConditionTypeOptions = [{
        text: "-- " + translate("SelectType") + " --",
        value: "",
        show: EnableSkip,
        disable: false
    }, {
        text: translate("SkipTo"),
        value: "Skip",
        show: EnableSkip,
        disable: false
    }, {
        text: translate("AddRisk"),
        value: "Risk",
        show: $scope.permissions.ConditionsAdd,
        disable: false
    }];

    var SaveType = {
        Add: 20,
        Update: 10,
        Delete: 0,
        DeleteTerm: 5
    };

    $scope.ReadOnly = ReadOnly;

    /********** Local Variables **********/

    var saves = [];
    var deleteQueue = [];

    /********** Data Functions **********/

    function getConditionsGroups() {
        return ConditionGroups.list($scope.Question.Id, Template.Version).then(
            function (response) {
                if (response.result) {
                    $scope.Question.Conditions = response.data || [];
                    _.each($scope.Question.Conditions, function(group) {
                        group.Terms = group.Conditions;
                        if (group.RiskResult) {
                            group.Type = ConditionTypes.Risk;
                        }
                        else {
                            group.Type = ConditionTypes.Skip;
                        }
                    });
                } else {
                    $scope.Question.Conditions = [];
                }
            }
        );
    }

    function getOperators(questionTypeId) {
        var deferred = $q.defer();
        $scope.Operators = ENUMS.ConditionOperators;

        //Filter AND if not Multichoice
        if (questionTypeId !== ENUMS.QuestionTypes.Multichoice)
            $scope.Operators = ENUMS.ConditionOperators.filter(function (element) {
                return !(element.Value == ENUMS.ConditionOperatorsTypes.And);
            });

        deferred.resolve($scope.Operators);
        return deferred.promise;
    }

    function getComparators(questionTypeId) {
        return Conditions.operators(questionTypeId).then(
            function (response) {
                var filteredResponse = response.data || [];
                filteredResponse = filteredResponse.filter(function (element) {
                    if (element.Id == ENUMS.ConditionComparatorTypes.NotApplicable)
                        return $scope.Question.AllowNotApplicable || false;
                    else if (element.Id == ENUMS.ConditionComparatorTypes.NotSure)
                        return $scope.Question.AllowNotSure || false;
                    else if (element.Id == ENUMS.ConditionComparatorTypes.Other)
                        return $scope.Question.AllowOther || false;
                    else return true;
                });
                $scope.Comparators = filteredResponse;
            }
        );
    }

    function getOperands(question) {
        var deferred = $q.defer();
        $scope.Operands = null;
        switch (question.Type) {
            case QuestionTypes.Content:
                // No Options
                $scope.Operands = null;
                break;
            case QuestionTypes.TextBox:
                $scope.Operands = null;
                break;
            case QuestionTypes.Multichoice:
                $scope.Operands = question.Options;
                break;
            case QuestionTypes.YesNo:
                $scope.Operands = [{
                    Description: "YES",
                    Value: true
                }, {
                    Description: "NO",
                    Value: false
                }];
                break;
            default:
        }
        deferred.resolve($scope.Operands);
        return deferred.promise;
    }

    /********** Helper Functions **********/

    function doDeletes() {
        var deferred = $q.defer();
        var deleteList = [];

        _.each(deleteQueue, function(item) {
            if (item.Id) {
                deleteList.push(item.Id);
            }
        });
        if (deleteList.length) {
            ConditionGroups.delete(deleteList).then(
                function (response) {
                    if (response.result) {
                        deferred.resolve({
                            SaveType: SaveType.Delete,
                            Status: false
                        });
                    } else {
                        deferred.resolve({
                            SaveType: SaveType.Delete,
                            Status: deleteQueue
                        });
                    }
                }
            );
        } else {
            deferred.resolve({
                SaveType: SaveType.Delete,
                Status: false
            });
        }

        saves.push(deferred.promise);

        return deferred.promise;
    }

    function batchTermCreate(arr) {
        var deferred = $q.defer();
        ConditionGroups.addConditions(arr).then(
            function (response) {
                if (response.result) {
                    deferred.resolve({ SaveType: SaveType.Add, Status: true });
                } else {
                    deferred.resolve({ SaveType: SaveType.Add, Status: -1 });
                }
            }
        );
        return deferred.promise;
    }

    function batchTermUpdate(arr) {
        var deferred = $q.defer();
        ConditionGroups.updateConditions(arr).then(
            function (response) {
                if (response.result) {
                    deferred.resolve({ SaveType: SaveType.Update, Status: true });
                } else {
                    deferred.resolve({ SaveType: SaveType.Update, Status: -1 });
                }
            }
        );
        return deferred.promise;
    }

    function batchTermDelete(arr) {
        var deferred = $q.defer();
        ConditionGroups.removeConditions(arr).then(
            function (response) {
                if (response.result) {
                    deferred.resolve({ SaveType: SaveType.DeleteTerm, Status: true });
                } else {
                    // There was an issue, roll back deleting terms.
                    _.each(arr, function(termDelete) {
                        var condition = Utilities.findById($scope.Question.Conditions, termDelete.ConditionGroupId);
                        if (condition && _.isArray(condition.deleteQueue)) {
                            condition.Terms = condition.Terms.concat(condition.deleteQueue);
                            condition.deleteQueue = [];
                        }
                    });
                    deferred.resolve({ SaveType: SaveType.DeleteTerm, Status: false });
                }
            }
        );
        return deferred.promise;
    }

    function batchCreate(arr) {
        var deferred = $q.defer();
        ConditionGroups.create(arr).then(
            function (response) {
                if (response.result) {
                    deferred.resolve({ SaveType: SaveType.Add, Status: true });
                } else {
                    deferred.resolve({ SaveType: SaveType.Add, Status: -1 });
                }
            }
        );
        return deferred.promise;
    }

    function batchUpdate(arr) {
        var deferred = $q.defer();
        ConditionGroups.update(arr).then(
            function (response) {
                if (response.result) {
                    deferred.resolve({ SaveType: SaveType.Update, Status: true });
                } else {
                    deferred.resolve({ SaveType: SaveType.Update, Status: -1 });
                }
            }
        );
        return deferred.promise;
    }

    function afterSave() {
        $q.all(saves).then(
            function (results) {
                var errors = [];
                for (var i = 0; i < results.length; i++) {
                    switch (results[i].SaveType) {
                        case SaveType.Add:
                            if (results[i].Status === -1) {
                                errors.push(results[i]);
                            }
                            break;
                        case SaveType.Update:
                            if (results[i].Status === -1) {
                                errors.push(results[i]);
                            }
                            break;
                        case SaveType.DeleteTerm:
                            if (!results[i].Status) {
                                errors.push(results[i]);
                            }
                            break;
                        case SaveType.Delete:
                            // !== false, means needs attention, something happened.
                            if (results[i].Status !== false) {
                                // Something happened. Undelete and log.
                                $scope.Question.Conditions = $scope.Question.Conditions.concat(deleteQueue);
                                errors.push(results[i]);
                            }
                            // Else, Success, deed is done.
                            break;
                    }
                }

                if (!errors.length) {
                    // Requests went well.
                    $uibModalInstance.close({
                        result: true,
                        data: $scope.Question.Conditions.length
                    });
                } else {
                    // Something happened.
                    deleteQueue = [];
                    saves = [];
                    $scope.Ready = true;
                }
            }
        );
    }

    function batchSave() {
        var termDeleteArray = [];
        var termCreateArray = [];
        var termUpdateArray = [];
        var createArray = [];
        var updateArray = [];
        if ($scope.Question.Conditions && $scope.Question.Conditions.length) {
            _.each($scope.Question.Conditions, function(condition) {
                    if (condition.Id) {
                        // Find the terms that need to be deleted
                        if (condition.deleteQueue.length) {
                            var termDelete = {
                                ConditionGroupId: condition.Id,
                                Conditions: []
                            };
                            _.each(condition.deleteQueue, function(term) {
                                termDelete.Conditions.push(term.Id);
                            });
                            termDeleteArray.push(termDelete);
                        }

                        var termUpdate = {
                            ConditionGroupId: condition.Id,
                            Conditions: []
                        };

                        var termCreate = {
                            ConditionGroupId: condition.Id,
                            Conditions: []
                        };

                        _.each(condition.Terms, function(term) {
                            var serialized = term.serialize(condition.Id, Template.Version);
                            (term.Id ? termUpdate : termCreate).Conditions.push(serialized);
                        });

                        if (termUpdate.Conditions.length) {
                            termUpdateArray = termUpdateArray.concat(termUpdate.Conditions);
                        }

                        if (termCreate.Conditions.length) {
                            termCreateArray = termCreateArray.concat(termCreate);
                        }

                        // Update the Condition Group
                        updateArray.push(condition.serialize(Template.Version, true));
                    }
                    // Create a new Condition Group with new terms.
                    else {
                        createArray.push(condition.serialize(Template.Version));
                    }
                });
            if (termDeleteArray.length) {
                saves.push(batchTermDelete(termDeleteArray));
            }
            if (termCreateArray.length) {
                saves.push(batchTermCreate(termCreateArray));
            }
            if (termUpdateArray.length) {
                saves.push(batchTermUpdate(termUpdateArray));
            }
            if (createArray.length) {
                saves.push(batchCreate(createArray));
            }
            if (updateArray.length) {
                saves.push(batchUpdate(updateArray));
            }
        }
        afterSave();
    }

    function canSaveConditions() {
        // Find any reason to not save.
        var hasError = false;

        // Check if we have any Conditions
        if (_.isArray($scope.Question.Conditions) && $scope.Question.Conditions.length) {
            // Iterate through all our conditions and ask if we can save them.
            _.each($scope.Question.Conditions, function(condition) {
                condition.validate();
                if (condition.hasError) {
                    hasError = true;
                }
            });
        }
        return !hasError;
    }

    function convertYesNo() {
        _.each($scope.Question.Conditions, function(condition) {
            _.each(condition.Terms, function(term) {
                if (term.Value) {
                    switch (term.Value) {
                        case "True":
                            term.Operand = true;
                            break;
                        case "False":
                            term.Operand = false;
                            break;
                    }
                }
            });
        });
    }

    /********** Close Modal **********/
    // Close modal with no changes.
    $scope.cancel = function () {
        $uibModalInstance.close({
            result: false,
            data: $scope.Question.Conditions
        });
    };

    // Close modal and update conditions.
    $scope.Close = function () {
        $uibModalInstance.close({
            result: false,
            data: $scope.Question.Conditions
        });
    };

    /********** Control Functions **********/

    $scope.addCondition = function () {
        var condition = {
            "Id": "",
            "TemplateQuestionId": Question.Id,
            "Version": Template.Version,
            "Type": "",
            "Terms": [{
                "Operator": $scope.Comparators[0].Id,
                "Value": $scope.Operands[0].Value
            }]
        };

        if (EnableSkip) {
            condition.Type = ConditionTypes.Skip;
        }
        else {
            condition.Type = ConditionTypes.Risk;
        }
        condition = new Condition(condition, Question.Type, $scope.Operators, $scope.Comparators, $scope.Operands, ConditionTypeOptions, TemplateSections);
        if ($scope.Question.Conditions) {
            $scope.Question.Conditions.push(condition);
        } else {
            $scope.Question.Conditions = [condition];
        }
    };

    $scope.saveAll = function () {
        if (canSaveConditions()) {
            $scope.Ready = false;

            doDeletes().then(
                function () {
                    batchSave();
                }
            );
        }
    };

    $scope.queueDelete = function (index) {
        var condition = $scope.Question.Conditions[index];
        if (!_.isUndefined(condition.Id) && condition.Id !== "") {
            deleteQueue.push(condition);
            $scope.Question.Conditions.splice(index, 1);
        } else {
            // Local Delete
            $scope.Question.Conditions.splice(index, 1);
        }
    };

    $scope.init = function () {
        $scope.QuestionIndex = QuestionIndex;
        $scope.Question = _.cloneDeep(Question);

        var PromiseArray = [
            getOperators(Question.Type),
            getComparators(Question.Type),
            getOperands(Question),
            getConditionsGroups()
        ];

        $q.all(PromiseArray).then(
            function () {
                // Convert Conditions into Condition Objects
                $scope.Question.Conditions = Condition.createAllFromQuestion($scope.Question.Conditions, Question.Type,
                    $scope.Operators, $scope.Comparators, $scope.Operands, ConditionTypeOptions, TemplateSections);
                // Convert the term.Operand for Yes No
                if ($scope.Question.Type === QuestionTypes.YesNo) {
                    convertYesNo();
                }

                // Flag Ready
                $scope.Ready = true;
            }
        );
    };

    /********** Call Init **********/
    $scope.init();
}

ConditionalController.$inject = ["$rootScope", "$scope", "$uibModalInstance", "$q", "ENUMS", "ConditionGroups", "Conditions", "Condition",
    "TemplateSections", "Question", "QuestionIndex", "ReadOnly", "Template", "EnableSkip", "Permissions"];
