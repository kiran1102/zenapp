import { AssessmentTypes } from "constants/assessment.constants";

export default function TemplatesArchiveCtrl($rootScope, $scope, $state, $uibModal, Users, Templates, $q, ENUMS, Permissions) {
    $scope.permissions = {
        TemplatesUnarchive: Permissions.canShow("TemplatesUnarchive"),
    };
    $scope.Ready = false;

    $scope.TemplateStates = ENUMS.TemplateStates;
    $scope.templateType = $state.includes(AssessmentTypes.DM.BaseRoute) ? ENUMS.TemplateTypes.Datamapping : ENUMS.TemplateTypes.Custom;
    $scope.routes = {
        single: ($scope.templateType === ENUMS.TemplateTypes.Datamapping) ? "zen.app.pia.module.datamap.views.questionnaire" : "zen.app.pia.module.templates.studio.manage",
        gallery: ($scope.templateType === ENUMS.TemplateTypes.Datamapping) ? "zen.app.pia.module.datamap.views.questionnaire-gallery" : "zen.app.pia.module.templates.studio.gallery",
        list: ($scope.templateType === ENUMS.TemplateTypes.Datamapping) ? "zen.app.pia.module.datamap.views.questionnaire-list" : "zen.app.pia.module.templates.list",
    };

    $scope.init = function () {
        var promiseArray = [
            getArchived()
        ];

        $q.all(promiseArray).then(
            function () {
                $scope.Ready = true;
            }
        );
    };

    var getArchived = function () {
        return Templates.readArchive().then(
            function (response) {
                $scope.templates = ((response.result && _.isArray(response.data)) ? response.data : []);
                return $scope.templates;
            }
        );
    };

    $scope.goToActive = function () {
        $state.go($scope.routes.list);
    };

    $scope.openStudio = function () {
        $state.go($scope.routes.gallery);
    };

    $scope.goToTemplate = function (Id, Version) {
        $state.go($scope.routes.single, { templateId: Id, version: Version });
    };

    $scope.activate = function (Id) {
        var index = _.findIndex($scope.templates, ["Id", Id]);
        var confirmMessage = $rootScope.t("AreYouSureToActivateTemplateName", { TemplateName: $scope.templates[index].Name });
        OneConfirm("", confirmMessage).then(function(result) {
            if (!result) return;
            Templates.activateTemplate(Id).then(function(response) {
                if (response.result) $scope.templates.splice(index, 1);
            });
        });
    };

    $scope.init();

}

TemplatesArchiveCtrl.$inject = ["$rootScope", "$scope", "$state", "$uibModal", "Users", "Templates", "$q", "ENUMS", "Permissions"];
