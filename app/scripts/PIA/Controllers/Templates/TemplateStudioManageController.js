import { AssessmentTypes } from "constants/assessment.constants";
import { IQuestionTypes } from "enums/question-types.enum";

export default function TemplateStudioManageCtrl(
    $scope, $state, $stateParams, $q, $filter, $timeout,
    $uibModal, Permissions, NotificationService, Templates,
    Sections, Questions, Icons, Settings,
    ENUMS, DataMapping, $rootScope, QuestionTypes, ApplicationQuestionGroup,
    QuestionGroup, SelfService, TemplateStudio, OneWelcomeTextService, SettingStore
) {
    $scope.permissions = {
        ConditionsAdd: Permissions.canShow("ConditionsAdd"),
        TemplatesCopy: Permissions.canShow("TemplatesCopy"),
        TemplatesCustomWelcomeText: Permissions.canShow("TemplatesCustomWelcomeText"),
        TemplatesDetailsEditDescription: Permissions.canShow("TemplatesDetailsEditDescription"),
        TemplatesDetailsEditIcon: Permissions.canShow("TemplatesDetailsEditIcon"),
        TemplatesDetailsEditName: Permissions.canShow("TemplatesDetailsEditName"),
        TemplatesPublish: Permissions.canShow("TemplatesPublish"),
        TemplatesPublishedEdit: Permissions.canShow("TemplatesPublishedEdit"),
        TemplatesQuestionItemEdit: Permissions.canShow("TemplatesQuestionItemEdit"),
        TemplatesQuestionItemPreview: Permissions.canShow("TemplatesQuestionItemPreview"),
        TemplatesSectionAdd: Permissions.canShow("TemplatesSectionAdd"),
        TemplatesSectionDelete: Permissions.canShow("TemplatesSectionDelete"),
        TemplatesSectionDeleteUpgrade: Permissions.canShow("TemplatesSectionDeleteUpgrade"),
        TemplatesShowVersions: Permissions.canShow("TemplatesShowVersions"),
        TemplatesLock: Permissions.canShow("TemplatesLock"),
    };
    $scope.Ready = false;
    $scope.LoadingNewVersion = false;
    $scope.ReorderInProgress = false;
    $scope.TemplateStates = ENUMS.TemplateStates;
    $scope.templateTypes = ENUMS.TemplateTypes;
    $scope.DragTypes = ENUMS.DragTypes;
    $scope.QuestionTypes = IQuestionTypes;
    $scope.templateType = $state.includes(AssessmentTypes.DM.BaseRoute) ? ENUMS.TemplateTypes.Datamapping : ENUMS.TemplateTypes.Custom;
    $scope.routes = {
        list: ($scope.templateType === ENUMS.TemplateTypes.Datamapping) ? "zen.app.pia.module.datamap.views.questionnaire-list" : "zen.app.pia.module.templates.list",
    };
    $scope.template = null;
    $scope.showTopIcons = false;
    $scope.state = false;
    $scope.editMode = false;
    $scope.readOnly = false;
    $scope.templateIsEditable = false;
    $scope.hasLaterVersions = false;
    $scope.isHistoricalVersion = false;
    $scope.versionsRetrieved = false;
    $scope.selectedLink = null;
    $scope.LinkingEnabled = false;
    $scope.AssignOrganizationEnabled = false;
    $scope.AssignProjectEnabled = false;
    $scope.allowThresholds = false;
    $scope.allowDataMapping = true;
    $scope.CreatingQuestionGroup = false;
    $scope.timer = 0;
    $scope.TopIcons = Icons.topList();
    $scope.QuestionTypesArray = QuestionTypes.getDraggableQuestionTypes($scope.templateType);
    $scope.allowDataMapping = Permissions.canShow("DataMapping");
    $scope.selfServiceEnabled = false;
    $scope.selfServiceLinked = false;
    $scope.directLaunchEnabled = false;
    $scope.lockTemplateEnabled = false;

    var Dictionary = { Sections: {}, Questions: {} };

    $scope.IsTypeDisabled = function (questionType) {
        return TemplateStudio.isTypeDisabled($scope, Permissions, questionType);
    };

    function lockQuestionType(questionType) {
        if (!_.isArray($scope.LockedQuestionTypes)) {
            $scope.LockedQuestionTypes = [];
        }
        if ($scope.LockedQuestionTypes.indexOf(questionType) < 0) {
            $scope.LockedQuestionTypes.push(questionType);
        }
    }

    var setQuestionsData = function (Questions, questionNumber) {
        if (_.isArray(Questions)) {
            _.each(Questions, function (question) {
                Dictionary.Questions[question.Id] = question;
                if (question.Type !== $scope.QuestionTypes.Content) {
                    question.Number = questionNumber++;
                }

                if (question.Type === $scope.QuestionTypes.DataElement) {
                    lockQuestionType($scope.QuestionTypes.DataElement);
                }

                question.EnableConditions = true;
                question.CanEdit = true;

                question.DragType = $scope.DragTypes.QuestionReorder;
            });
        } else {
            Questions = [];
        }

        return {
            Questions: Questions,
            QuestionNumber: questionNumber
        };
    };

    var setGroupData = function (Groups, questionNumber) {
        if (_.isArray(Groups)) {
            _.each(Groups, function (group, index) {
                var questionData = setQuestionsData(group.Questions, questionNumber);
                group.DragType = $scope.DragTypes.QuestionGroupReorder;
                group.Questions = questionData.Questions;
                group.GroupIndex = index;
                questionNumber = questionData.QuestionNumber;

                if (group.Type === $scope.QuestionTypes.ApplicationGroup) {
                    lockQuestionType($scope.QuestionTypes.ApplicationGroup);
                }
            });
        } else {
            Groups = [];
        }
        return {
            Groups: Groups,
            QuestionNumber: questionNumber
        };
    };

    var setSectionData = function (Sections) {
        var questionNumber = 1;
        $scope.LockedQuestionTypes = [];

        _.each(Sections, function (section) {
            Dictionary.Sections[section.Id] = section;
            var groupData = setGroupData(section.QuestionGroups, questionNumber);
            section.DragType = $scope.DragTypes.SectionReorder;
            section.QuestionGroups = groupData.Groups;
            questionNumber = groupData.QuestionNumber;
        });
        return Sections;
    };

    var getPublishedTemplates = function () {
        return Templates.publishedList($scope.templateType).then(function (response) {
            $scope.publishedList = ((response.data && _.isArray(response.data)) ? response.data : []);
            return $scope.publishedList;
        });
    };

    var FilterList = function (list, currentLinkedTemplateId) {
        var filtered = [];
        _.each(list, function (item) {
            if ((item && !item.HasParents && !item.NextTemplateId && (item.Id !== $scope.templateId || (currentLinkedTemplateId && item.Id === currentLinkedTemplateId)))) {
                filtered.push(item);
            }
        });
        return filtered;
    };

    var readTemplate = function () {
        var params = {
            id: $scope.templateId,
            version: $scope.version
        };
        return Templates.readTemplate(params).then(function (response) {
            if (response.result) {
                $scope.template = response.data;
                $state.go(".", { templateId: $scope.templateId, version: $scope.template.Version }, { notify: false });

                if (!$scope.template.HasCustomWelcomeText) {
                    $scope.template.WelcomeText = OneWelcomeTextService.getWelcomeText();
                }

                $scope.TemplateLink = $scope.template.NextTemplateId;
                $scope.selectedLink = $scope.template.NextTemplateId;

                switch ($scope.template.State) {
                    case $scope.TemplateStates.Stock:
                    case $scope.TemplateStates.Draft:
                        $scope.template.isPublished = false;
                        break;
                    case $scope.TemplateStates.Published:
                    case $scope.TemplateStates.Archived:
                    case $scope.TemplateStates.Locked:
                        $scope.template.isPublished = true;
                        break;
                }

                $scope.template.hasProjects = $scope.template.VersionProjectCount === 0 ? false : true;
                $scope.editMode = false;
                $scope.readOnly = $scope.template.isPublished || $scope.template.State === $scope.TemplateStates.Archived ? true : false;

                if (!_.isUndefined($scope.template.Sections) && $scope.template.Sections !== null) {
                    QuestionGroup.arrangeQuestionsByGroup($scope.template.Sections);
                    $scope.template.Sections = setSectionData($scope.template.Sections);
                    $scope.template.Sections[0].Show = true;
                }

                $scope.lockTemplateEnabled = response.data.IsLocked;
            } else {
                $state.go($scope.routes.list);
            }
        });
    };

    $scope.toggleLinkEnabled = function () {
        $scope.LinkingEnabled = !$scope.LinkingEnabled;
        if (!$scope.LinkingEnabled) {
            $scope.LinkedTemplateId = null;
            $scope.LinkedTemplate = null;
            $scope.saveLinkedTemplate();
        }
    };

    var debouncedSetSelfService = _.debounce(function () {
        var params = {
            TemplateId: $scope.template.Id,
            TemplateVersion: $scope.template.Version,
            SelectOrgGroup: $scope.SelectOrgGroup,
            SelectAssignee: $scope.SelectAssignee,
            DirectLaunch: $scope.directLaunchEnabled,
        };

        if ($scope.selfServiceLinked) {
            $q.defer().resolve(SelfService.updateConfiguration(params));
        } else {
            $q.defer().resolve(SelfService.deleteConfiguration(params));
        }
    }, 500);

    $scope.toggleSelfService = function () {
        $scope.selfServiceLinked = !$scope.selfServiceLinked;
        if (!$scope.selfServiceLinked) {
            $scope.directLaunchEnabled = false;
            $scope.SelectOrgGroup = false;
            $scope.SelectAssignee = false;
        }
        debouncedSetSelfService();
    };

    $scope.toggleDirectLaunch = function () {
        $scope.directLaunchEnabled = !$scope.directLaunchEnabled;
        if ($scope.directLaunchEnabled) {
            $scope.SelectOrgGroup = false;
            $scope.SelectAssignee = false;
        }
        debouncedSetSelfService();
    };

    var debouncedSetLockTemplate = _.debounce(function () {
        $scope.Save();
    }, 500);

    $scope.toggleLockTemplate = function () {
        $scope.lockTemplateEnabled = !$scope.lockTemplateEnabled;
        debouncedSetLockTemplate();
    }

    $scope.toggleAssignOrganizationEnabled = function () {
        $scope.SelectOrgGroup = !$scope.SelectOrgGroup;
        debouncedSetSelfService();
    };

    $scope.toggleAssignProjectEnabled = function () {
        $scope.SelectAssignee = !$scope.SelectAssignee;
        debouncedSetSelfService();
    };

    $scope.setLinkedTemplate = function (template) {
        $scope.LinkedTemplateId = template ? template.Id : null;
    };

    $scope.saveLinkedTemplate = function () {
        $scope.template.NextTemplateId = $scope.LinkedTemplateId;
        $scope.Save();
    };

    $scope.Save = function () {
        if (!_.isUndefined($scope.template)) {
            var template = {
                Id: $scope.template.Id,
                Name: $scope.template.Name,
                Description: $scope.template.Description,
                Version: $scope.template.Version,
                Icon: $scope.template.Icon,
                NextTemplateId: $scope.template.NextTemplateId,
                IsLocked: $scope.lockTemplateEnabled,
            };

            if ($scope.template.HasCustomWelcomeText) {
                template.WelcomeText = $scope.template.WelcomeText.trim();
            } else {
                template.WelcomeText = "";
            }

            return Templates.updateTemplate(template);
        }
    };

    $scope.Cancel = function () {
        $state.go($scope.routes.list);
    };

    function getQuestionCategoryFilter(question) {
        return Questions.getQuestionCategory(question.Id, $scope.template.Version);
    }

    function categoryFilterToggle(question, category) {
        var deferred = $q.defer();
        if (question && question.Id && category && category.Id) {
            var toggleCategory = {
                TemplateQuestionId: question.Id,
                CategoryId: category.Id,
                Version: $scope.template.Version,
            };
            Questions.updateQuestionCategory(toggleCategory).then(function (response) {
                deferred.resolve(response);
            });
        } else {
            return deferred.resolve({ result: false, data: false });
        }

        return deferred.promise;
    }

    $scope.init = function () {
        $scope.questionEvents = {
            categoryFilterToggle: categoryFilterToggle,
            getQuestionCategoryFilter: getQuestionCategoryFilter,
            editMode: function () {
                return $scope.editMode;
            },
            readOnly: function () {
                return $scope.readOnly;
            },
            canShowAddConditionButton: $scope.canShowAddConditionButton,
            openConditionalModal: $scope.OpenConditionalModal,
            deleteQuestion: $scope.deleteQuestion,
            deleteQuestionGroup: $scope.deleteQuestionGroup,
            openSaveItemModal: $scope.OpenSaveItemModal
        };

        $scope.templateId = $stateParams.templateId;
        $scope.version = $scope.version || parseInt($stateParams.version);

        if (!$scope.templateId) {
            $state.go($scope.routes.list);
        } else {
            var promiseArray = [
                readTemplate(),
                getPublishedTemplates(),
                getVersionHistory(),
                getSettings()
            ];

            $q.all(promiseArray).then(function () {
                if ($scope.template) {
                    checkLaterVersions();
                    $scope.LinkedTemplateId = $scope.template.NextTemplateId;
                    $scope.LinkingEnabled = !!($scope.template.NextTemplateId);
                    $scope.LinkedTemplate = ($scope.LinkedTemplateId ? $filter("filter")($scope.publishedList, { Id: $scope.LinkedTemplateId })[0] : null);
                    $scope.LinkableTemplates = FilterList($scope.publishedList);

                    if ($scope.LinkedTemplate) {
                        $scope.LinkableTemplates.push($scope.LinkedTemplate);
                    }

                    $scope.Ready = true;
                } else {
                    $state.go($scope.routes.list);
                }
            });
        }
    };

    $scope.SetIcon = function (index) {
        $scope.template.Icon = $scope.TopIcons[index];
        $scope.showTopIcons = false;
    };

    $scope.publishTemplate = function () {
        if (!$scope.template.IsPublished) {
            var confirmMessage = $rootScope.t("AreYouSureToPublishTemplateName", { TemplateName: $scope.template.Name });
            OneConfirm("", confirmMessage).then(function(result) {
                if (!result) return;
                Templates.publishTemplate($scope.template.Id).then(function(response) {
                    if (!response.result) return;
                    if ($scope.templateType === ENUMS.TemplateTypes.Datamapping) {
                        $scope.Ready = false;
                        $scope.init();
                    } else {
                        $state.go($scope.routes.list);
                    }
                });
            });
        }
    };

    $scope.editSection = function (Section) {
        var modalInstance = $uibModal.open({
            size: "lg",
            templateUrl: "EditSectionModal.html",
            controller: "EditSectionCtrl",
            scope: $scope,
            backdrop: "static",
            keyboard: false,
            resolve: {
                Section: function () {
                    return Section;
                },
                Template: function () {
                    return $scope.template;
                },
                EditMode: function () {
                    return $scope.editMode;
                }
            }
        });

        modalInstance.result.then(function (response) {
            if (response.result) Section = response.data;
        }, function () { });
    };

    $scope.deleteSection = function (version, Section) {
        var confirmMessage = $rootScope.t("AreYouSureToDeleteSectionName", { SectionName: Section.Name });
        OneConfirm("", confirmMessage).then(function (result) {
            if (!result) return;
            Section.isDeleting = true;
            Sections.deleteSection(Section.Id, version).then(function (response) {
                if (response.result) {
                    $scope.template.Sections = $filter("filter")($scope.template.Sections, { Id: "!" + Section.Id }, true);
                    $timeout(function () {
                        if (_.isArray(response.data)) {
                            _.each(response.data, function (section) {
                                applySectiontoQuestionConditionUpdates(section);
                                $scope.template.Sections = setSectionData($scope.template.Sections);
                            });
                        }
                    }, 0);
                }
                Section.isDeleting = false;
            });
        });
    };

    $scope.OpenSelectIconModal = function () {
        var SelectedIcon = $scope.template.Icon;

        var modalInstance = $uibModal.open({
            templateUrl: "SelectIconModal.html",
            controller: "SelectIconCtrl",
            scope: $scope,
            backdrop: "static",
            keyboard: false,
            resolve: {
                SelectedIcon: function () {
                    return SelectedIcon;
                }
            }
        });

        modalInstance.result.then(function (response) {
            if (!response.result) return false;
            $scope.template.Icon = response.data;
            $scope.Save();
        });
    };

    $scope.SetWelcomeText = function (isCustom) {
        var resetResponse = true;
        if (!isCustom) {
            if ($scope.template.HasCustomWelcomeText) {
                OneConfirm("", $rootScope.t("SureResetToDefaultWelcomeMessage")).then(function (result) {
                    if (!result) return;
                    $scope.template.HasCustomWelcomeText = false;
                    $scope.template.WelcomeText = OneWelcomeTextService.getWelcomeText();
                    $scope.Save();
                });
            }
        }
    };

    $scope.OpenWelcomeTextModal = function () {
        var WelcomeText = $scope.template.WelcomeText;
        var modalInstance = $uibModal.open({
            templateUrl: "WelcomeTextModal.html",
            controller: "WelcomeTextCtrl as ctrl",
            scope: $scope,
            backdrop: "static",
            keyboard: false,
            resolve: {
                WelcomeText: function () {
                    return WelcomeText;
                }
            }
        });

        modalInstance.result.then(function (response) {
            if (!response.result) return;
            $scope.template.WelcomeText = response.data;
            $scope.template.HasCustomWelcomeText = true;
            $scope.Save();
        }, function (cancel) {
            return;
        });
    };

    $scope.OpenAddSectionModal = function () {
        var templateId = $scope.template.Id;
        var version = $scope.template.Version;
        var nextSectionId = "";

        var modalInstance = $uibModal.open({
            size: "lg",
            templateUrl: "AddSectionModal.html",
            controller: "AddSectionCtrl",
            scope: $scope,
            backdrop: "static",
            keyboard: false,
            resolve: {
                templateId: function () {
                    return templateId;
                },
                version: function () {
                    return version;
                },
                Next: function () {
                    return nextSectionId;
                }
            }
        });

        modalInstance.result.then(function (response) {
            if (response.result) {
                if (_.isArray($scope.template.Sections) && $scope.template.Sections.length > 0) {
                    $scope.template.Sections[$scope.template.Sections.length - 1].NextSectionId = response.data.Id;
                    $scope.template.Sections.push(response.data);
                    $scope.template.Sections = setSectionData($scope.template.Sections);
                } else {
                    $scope.template.Sections = [response.data];
                    $scope.template.Sections = setSectionData($scope.template.Sections);
                    $scope.template.Sections[0].Show = true;
                }
            }
        });
    };

    function applySectiontoQuestionConditionUpdates(conditionSection) {
        var sectionIndex = _.findIndex($scope.template.Sections, ["Id", conditionSection.Id]);
        if (conditionSection && conditionSection.QuestionConditionCounts && (sectionIndex > -1)) {
            for (var questionId in conditionSection.QuestionConditionCounts) {
                if (conditionSection.QuestionConditionCounts.hasOwnProperty(questionId)) {
                    var question = Dictionary.Questions[questionId];
                    if (question) {
                        question.ConditionCount = conditionSection.QuestionConditionCounts[questionId];
                    }
                }
            }
        }
    }

    function applySectionConditionUpdates(sectionData) {
        if (_.isArray(sectionData) && sectionData.length) {
            _.each(sectionData, function (conditionSection) {
                applySectiontoQuestionConditionUpdates(conditionSection);
            });
        }
    }

    var accept = function (source, dest, index) {
        var allowDrop = false;

        if (source.$element[0].className.indexOf("sections") === -1) {
            allowDrop = true;
        }
        return allowDrop;
    };

    var dropped = function (event) {
        $scope.template.Sections = setSectionData($scope.template.Sections);
    };

    var beforeDrop = function (event) {
        var deferred = $q.defer();
        if ((event.dest.nodesScope.$element[0].className.indexOf("template-questions-group") < 0)) {
            deferred.reject();
        } else if (event.source.nodeScope.$modelValue) {
            var section;
            if (event.dest.nodesScope.$parent && event.dest.nodesScope.$parent.$parent && event.dest.nodesScope.$parent.$parent.vm) {
                section = event.dest.nodesScope.$parent.$parent.vm.section ? event.dest.nodesScope.$parent.$parent.vm.section : null;
            }
            switch (event.source.nodeScope.$modelValue.DragType) {
                case $scope.DragTypes.StatementAdd:
                    $scope.OpenSaveItemModal(false, section, event.dest.index, null, 0, event.source.nodeScope.$modelValue.Type);
                    deferred.reject();
                    break;
                case $scope.DragTypes.QuestionAdd:
                    $scope.OpenSaveItemModal(false, section, event.dest.index, null, 0, event.source.nodeScope.$modelValue.Type);
                    deferred.reject();
                    break;
                case $scope.DragTypes.QuestionAddGroup:
                    createLoadingPlaceholder(section, event.dest.index);
                    ApplicationQuestionGroup.createApplicationGroup($scope.template.Version, section, event.source.nodeScope.$modelValue.Type, event.dest.index + 1)
                        .then(function (response) {
                            if (response.result && _.isArray(response.data)) {
                                QuestionGroup.renderQuestionGroup($scope.template.Version, section, event.dest.index + 1, response.data)
                                    .then(function (res) {
                                        deleteLoadingPlaceholder(section, event.dest.index);
                                        $scope.template.Sections = setSectionData($scope.template.Sections);
                                    });
                            } else {
                                deleteLoadingPlaceholder(section, event.dest.index);
                            }
                        });
                    deferred.reject();
                    break;
                case $scope.DragTypes.QuestionGroupReorder:
                    if (!_.isEqual(event.source.nodesScope, event.dest.nodesScope) ||
                        (_.isEqual(event.source.nodesScope, event.dest.nodesScope) && event.source.index === event.dest.index)) {
                        deferred.reject();
                        break;
                    }
                    $scope.QuestionGroupMoved(event.source.index, event.dest.index, event.source.nodeScope.$modelValue).then(function (response) {
                        if (response.result) {
                            deferred.resolve(response);
                            applySectiontoQuestionConditionUpdates(response.data);
                        } else {
                            deferred.reject();
                        }
                    });
                    return true;
                default:
                    deferred.reject();
            }
        }
        return deferred.promise;
    };

    $scope.QuestionOptions = {
        accept: accept,
        dropped: dropped,
        beforeDrop: beforeDrop
    };

    var sectionAccept = function (source, dest, index) {
        var allowDrop = false;

        if (source.$element[0].className.indexOf("sections") !== -1) {
            allowDrop = true;
        }

        return allowDrop;
    };

    var sectionDropped = function (event) {
        $scope.template.Sections = setSectionData($scope.template.Sections);
    };

    var sectionBeforeDrop = function (event) {
        var deferred = $q.defer();
        if ((event.dest.nodesScope.$element[0].className.indexOf("template-sections-group") < 0)) {
            deferred.reject();
        } else if (event.source.nodeScope.$modelValue) {
            switch (event.source.nodeScope.$modelValue.DragType) {
                case $scope.DragTypes.SectionReorder:
                    if (_.isEqual(event.source.nodesScope, event.dest.nodesScope) && event.source.index === event.dest.index) {
                        deferred.reject();
                        break;
                    }
                    $scope.SectionMoved(event, event.dest.index, event.source.nodeScope.$modelValue).then(function (response) {
                        if (response.result) {
                            deferred.reject();
                            $timeout(function () {
                                if ((_.isInteger(response.FromIndex) && response.FromIndex > -1) && (_.isInteger(response.ToIndex) && response.ToIndex > -1)) {
                                    $scope.template.Sections.splice(response.ToIndex, 0, $scope.template.Sections.splice(response.FromIndex, 1)[0]);
                                }
                                applySectionConditionUpdates(response.data);
                                sectionDropped(event);
                            }, 0);
                        } else {
                            deferred.reject();
                        }
                    });
                    break;
                default:
                    deferred.reject();
            }
        } else {
            deferred.reject();
        }
        return deferred.promise;
    };

    $scope.SectionOptions = {
        accept: sectionAccept,
        dropped: sectionDropped,
        beforeDrop: sectionBeforeDrop
    };

    $scope.canShowAddConditionButton = function (type) {
        return $scope.permissions.ConditionsAdd
            && (type === $scope.QuestionTypes.Multichoice
            || type === $scope.QuestionTypes.YesNo);
    };

    var getLastQuestionInSection = function (section) {
        var questionGroupIndex = section.QuestionGroups.length ? section.QuestionGroups.length - 1 : 0;
        var questionIndex = section.QuestionGroups[questionGroupIndex].Questions.length ? section.QuestionGroups[questionGroupIndex].Questions.length - 1 : 0;
        return section ? section.QuestionGroups[questionGroupIndex].Questions[questionIndex] : {};
    };

    var getLastSectionWithQuestions = function (index) {
        var sectionNum = (!index ? $scope.template.Sections.length : index) - 1;
        var section = $scope.template.Sections[sectionNum];
        if (section) {
            if (section.QuestionGroups && section.QuestionGroups.length) {
                return section;
            }
            return getLastSectionWithQuestions(sectionNum--);
        }
    };

    $scope.OpenConditionalModal = function (currentSectionId, groupIndex, question) {
        var sectionIndex = _.findIndex($scope.template.Sections, ["Id", currentSectionId]);
        var enableSkip = false;
        if (sectionIndex >= 0) {
            var questionIndex = _.findIndex($scope.template.Sections[sectionIndex].QuestionGroups[groupIndex].Questions, ["Id", question.Id]);
            if (questionIndex >= 0) {
                var TemplateSections = _.cloneDeep($scope.template.Sections).slice(sectionIndex, $scope.template.Sections.length);
                _.each(TemplateSections, function (section) {
                    if (_.isArray(section.QuestionGroups)) {
                        _.each(section.QuestionGroups, function (group) {
                            if (group.GroupId) {
                                group.Questions = [group.Questions[0]];
                            }
                        });
                    }
                });

                var firstSectionGroups = TemplateSections[0].QuestionGroups;
                var GroupQuestions = firstSectionGroups[groupIndex].Questions;
                if ((groupIndex === firstSectionGroups.length - 1) && (questionIndex === GroupQuestions.length - 1)) {
                    TemplateSections.splice(0, 1);
                } else if (questionIndex < GroupQuestions.length - 1) {
                    var groupSliceIndex = questionIndex === firstSectionGroups[groupIndex].Questions.length - 1 ? groupIndex + 1 : groupIndex;
                    firstSectionGroups = firstSectionGroups.slice(groupSliceIndex, firstSectionGroups.length);
                    GroupQuestions = GroupQuestions.slice(questionIndex + 1, GroupQuestions.length);
                } else {
                    firstSectionGroups.splice(0, 1);
                }

                var CompleteTemplateSections = _.map(TemplateSections, function (section, i) {
                    if (Utilities.isArrayWithLength(section.QuestionGroups)) {
                        section.Questions = getSkipConditions(section, currentSectionId, groupIndex);
                        delete section.QuestionGroups;
                        return section;
                    }
                });

                CompleteTemplateSections = _.filter(CompleteTemplateSections, function (section) {
                    return !_.isUndefined(section);
                });

                enableSkip = CompleteTemplateSections.length && $scope.permissions.ConditionsAdd;
                var modalInstance = $uibModal.open({
                    templateUrl: "ConditionalModal.html",
                    controller: "ConditionalController",
                    scope: $scope,
                    backdrop: "static",
                    keyboard: false,
                    resolve: {
                        TemplateId: function () {
                            return $scope.template.Id;
                        },
                        TemplateSections: function () {
                            return CompleteTemplateSections;
                        },
                        Template: function () {
                            return $scope.template;
                        },
                        Question: function () {
                            return question;
                        },
                        QuestionIndex: function () {
                            return questionIndex;
                        },
                        ReadOnly: function () {
                            return ($scope.readOnly || !!question.GroupId);
                        },
                        EnableSkip: function () {
                            return enableSkip;
                        }
                    }
                });

                modalInstance.result.then(function (response) {
                    if (response.result) question.ConditionCount = response.data;
                });
            }
        }
    };

    function getSkipConditions(section, currentSectionId, groupIndex) {
        var sectionQuestions = _.flatMap(section.QuestionGroups, function (group) {
            return group.Questions;
        });
        return _.filter(sectionQuestions, function (question, i) {
            return section.Id === currentSectionId ? i + 1 > groupIndex : Boolean(question.Type);
        });
    }

    function openSaveQuestionModal(isUpdate, section, groupIndex, question, questionIndex, nextQuestionId, questionType) {

        var modalInstance = $uibModal.open({
            templateUrl: "SaveQuestionModal.html",
            controller: "SaveQuestionCtrl",
            scope: $scope,
            size: "lg",
            backdrop: "static",
            keyboard: false,
            resolve: {
                SectionId: function () {
                    return section.Id;
                },
                Template: function () {
                    return $scope.template;
                },
                LoadedQuestion: function () {
                    return question || null;
                },
                NextQuestionId: function () {
                    return isUpdate ? question.NextQuestionId : nextQuestionId;
                },
                EditMode: function () {
                    return $scope.editMode;
                },
                ReadOnly: function () {
                    return $scope.readOnly;
                },
                LockedQuestionTypes: function () {
                    return $scope.LockedQuestionTypes;
                },
                PreSelected: function () {
                    return questionType || $scope.QuestionTypesArray[1].Type;
                },
            }
        });

        modalInstance.result.then(function (response) {
            if (!response.result) return;
            var questionId = isUpdate ? question.Id : response.data;
            updateSectionsAfterAdd(isUpdate, section, groupIndex, questionId, questionIndex);
        }, function cancelHandler() { });
    };

    function updateSectionsAfterAdd(isUpdate, section, groupIndex, questionId, questionIndex) {
        Sections.getQuestion(questionId, $scope.template.Version).then(
            function (res) {
                if (res.result) {
                    if (isUpdate) {
                        section.QuestionGroups[groupIndex].Questions[questionIndex] = res.data;
                    } else {
                        if (_.isArray(section.QuestionGroups) && section.QuestionGroups.length) {
                            section.QuestionGroups.splice(groupIndex, 0, QuestionGroup.newGroupFromQuestionArray([res.data], section.Id));
                        } else {
                            section.QuestionGroups = [QuestionGroup.newGroupFromQuestionArray([res.data], section.Id)];
                        }
                    }
                }
                $scope.template.Sections = setSectionData($scope.template.Sections);
            }
        );
    };

    function openSaveStatementModal(isUpdate, section, groupIndex, question, questionIndex, nextQuestionId) {
        var modalInstance = $uibModal.open({
            templateUrl: "StatementModal.html",
            controller: "SaveStatementCtrl",
            scope: $scope,
            size: "md",
            backdrop: "static",
            keyboard: false,
            resolve: {
                SectionId: function () {
                    return section.Id;
                },
                Template: function () {
                    return $scope.template;
                },
                Question: function () {
                    return question;
                },
                NextQuestionId: function () {
                    return isUpdate ? question.NextQuestionId : nextQuestionId;
                },
                EditMode: function () {
                    return $scope.editMode;
                },
                ReadOnly: function () {
                    return $scope.readOnly;
                }
            }
        });

        modalInstance.result.then(function (response) {
            if (response.result) {
                var questionId = isUpdate ? question.Id : response.data;
                updateSectionsAfterAdd(isUpdate, section, groupIndex, questionId, questionIndex);
            }
        });
    };

    $scope.OpenSaveItemModal = function (isUpdate, section, groupIndex, question, questionIndex, questionType) {
        var isContentQuestion = question ? question.Type === $scope.QuestionTypes.Content : !_.isUndefined(questionType) && questionType === $scope.QuestionTypes.Content;
        var hasGroupIndex = !_.isUndefined(groupIndex) && groupIndex !== null;
        var sectionHasGroups = section.QuestionGroups && _.isArray(section.QuestionGroups) && section.QuestionGroups.length;
        groupIndex = hasGroupIndex ? groupIndex : sectionHasGroups ? section.QuestionGroups.length : 0;

        var hasQuestionIndex = !_.isUndefined(questionIndex) && questionIndex !== null;
        var groupExists = sectionHasGroups && hasGroupIndex && !_.isUndefined(section.QuestionGroups[groupIndex]);
        var groupHasQuestions = sectionHasGroups && groupExists && _.isArray(section.QuestionGroups[groupIndex].Questions) && section.QuestionGroups[groupIndex].Questions.length;
        questionIndex = hasQuestionIndex ? questionIndex : groupHasQuestions ? section.QuestionGroups[groupIndex].Questions.length - 1 : 0;

        var nextQuestionExists = groupHasQuestions && !_.isUndefined(section.QuestionGroups[groupIndex].Questions[questionIndex]);
        var nextQuestionId = nextQuestionExists ? section.QuestionGroups[groupIndex].Questions[questionIndex].Id : "";

        if (section) {
            if (isContentQuestion) {
                openSaveStatementModal(isUpdate, section, groupIndex, question, questionIndex, nextQuestionId);
            } else {
                openSaveQuestionModal(isUpdate, section, groupIndex, question, questionIndex, nextQuestionId, questionType);
            }
        }
    };

    $scope.QuestionGroupMoved = function (sourceGroupIndex, destGroupIndex, questionGroup) {
        var deferred = $q.defer();
        var sectionIndex = _.findIndex($scope.template.Sections, ["Id", questionGroup.SectionId]);

        if (sectionIndex > -1) {
            $scope.ReorderInProgress = true;
            var section = $scope.template.Sections[sectionIndex];
            if (isNaN(destGroupIndex)) {
                destGroupIndex = 0;
            }
            var questionReorderList = [];
            var nextQuestionId;
            _.each(questionGroup.Questions, function (question, questionIndex) {
                if (sourceGroupIndex < destGroupIndex) {
                    if ((destGroupIndex === section.QuestionGroups.length - 1) && (questionIndex === questionGroup.Questions.length - 1)) {
                        nextQuestionId = "";
                    } else if (questionIndex === questionGroup.Questions.length - 1) {
                        nextQuestionId = section.QuestionGroups[destGroupIndex + 1].Questions[0].Id;
                    } else {
                        nextQuestionId = questionGroup.Questions[questionIndex + 1].Id;
                    }
                } else {
                    nextQuestionId = section.QuestionGroups[destGroupIndex].Questions[0].Id;
                }
                questionReorderList.push({ questionId: question.Id, nextQuestionId: nextQuestionId });
            });

            if (sourceGroupIndex < destGroupIndex) {
                questionReorderList.reverse();
            }

            recursivelyReorderQuestions(questionReorderList).then(function (response) {
                $scope.ReorderInProgress = false;
                if (response.result) {
                    deferred.resolve(response);
                } else {
                    NotificationService.alertError($rootScope.t("Failed"), $rootScope.t("CouldNotReorderQuestionOnTheSection"))
                    deferred.reject();
                }
            });
        } else {
            deferred.reject();
        }
        return deferred.promise;
    };

    function recursivelyReorderQuestions(questionReorderList) {
        var deferred = $q.defer();
        var promiseIndex = 0;
        var reorderFn = function (params) {
            reorderQuestion(params.questionId, params.nextQuestionId).then(function (res) {
                if (res.result) {
                    if (promiseIndex === questionReorderList.length - 1) {
                        deferred.resolve(res);
                    } else {
                        promiseIndex++;
                        reorderFn(questionReorderList[promiseIndex]);
                    }
                } else {
                    deferred.reject();
                }
            });
        }
        reorderFn(questionReorderList[promiseIndex]);
        return deferred.promise;
    }

    $scope.SectionMoved = function (event, index, section) {
        var deferred = $q.defer();
        if (event.dest.index === -1 || event.source.index === -1) {
            deferred.reject();
        } else if (index === $scope.template.Sections.length) {
            if (section.Id !== $scope.template.Sections[index - 1].Id) {
                $scope.reorderSection(section.Id, null).then(function (response) {
                    if (response.result) {
                        response.FromIndex = event.source.index;
                        response.ToIndex = index - 1;
                        deferred.resolve(response);
                    } else {
                        deferred.reject();
                    }
                });
            } else {
                deferred.reject();
            }
        } else if ((index > -1 && index < $scope.template.Sections.length) && section.Id !== $scope.template.Sections[index].Id) {
            $scope.reorderSection(section.Id, $scope.template.Sections[index].Id).then(function (response) {
                if (response.result) {
                    response.FromIndex = event.source.index;
                    response.ToIndex = (event.dest.index > event.source.index) ? (index - 1) : index;
                    deferred.resolve(response);
                } else {
                    deferred.reject();
                }
            });
        } else {
            deferred.reject();
        }
        return deferred.promise;
    };

    function reorderQuestion(questionId, nextQuestionId) {
        var reorderData = {
            QuestionId: questionId,
            NextQuestionId: nextQuestionId,
            Version: $scope.template.Version
        };

        return Sections.reorderQuestion(reorderData);
    };

    $scope.reorderSection = function (sectionId, nextSectionId) {
        $scope.ReorderInProgress = true;
        var reorderData = {
            SectionId: sectionId,
            NextSectionId: nextSectionId,
            Version: $scope.template.Version
        };

        return Templates.reorderSection(reorderData).then(function (response) {
            $scope.ReorderInProgress = false;
            if (!response.result) {
                NotificationService.alertError($rootScope.t("Failed"), $rootScope.t("CouldNotReorderSection"));
            }
            return response;
        });
    };

    $scope.deleteQuestion = function (section, groupIndex, questionId, version) {
        OneConfirm("", $rootScope.t("SureToDeleteThisQuestion")).then(function (result) {
            if (!result) return;

            Sections.deleteQuestion(questionId, version).then(function (response) {
                if (!response.result) return;
                section.QuestionGroups.splice(groupIndex, 1);
                $scope.template.Sections = setSectionData($scope.template.Sections);
                applySectionConditionUpdates(response.data);
            });
        });
    };

    $scope.deleteQuestionGroup = function (section, groupIndex, version) {
        OneConfirm("", $rootScope.t("SureToDeleteThisQuestion")).then(function (result) {
            if (!result) return
            section.QuestionGroups[groupIndex].deleteInProgress = true;
            recursivelyDeleteQuestions(section.QuestionGroups[groupIndex].Questions, 0, version).then(function (response) {
                if (response.result) {
                    section.QuestionGroups.splice(groupIndex, 1);
                    $scope.template.Sections = setSectionData($scope.template.Sections);
                    applySectionConditionUpdates(response.data);
                } else {
                    section.QuestionGroups[groupIndex].deleteInProgress = false;
                }
            });
        });
    };

    function recursivelyDeleteQuestions(questionsArray, questionIndex, version) {
        var deferred = $q.defer();
        function deleteFn(qArr, qIndex) {
            Sections.deleteQuestion(qArr[qIndex].Id, version).then(function (response) {
                if (response.result) {
                    if (qIndex === qArr.length - 1) {
                        deferred.resolve(response);
                    } else {
                        deleteFn(qArr, qIndex + 1);
                    }
                } else {
                    deferred.reject();
                }
            });
        }
        deleteFn(questionsArray, questionIndex);
        return deferred.promise;
    }

    $scope.copy = function () {
        var original = $scope.template;

        if (_.isObject(original)) {
            var copy = {
                Id: original.Id,
                Name: original.Name + " (Copy)",
                Description: original.Description,
                Icon: original.Icon,
                Version: original.Version,
                WelcomeText: original.WelcomeText,
            };

            Templates.cloneTemplate(copy).then(function (response) {
                if (response.result) $state.go($scope.routes.list);
            });
        }
    };

    $scope.toggleEditMode = function () {
        $scope.editMode = !$scope.editMode;
    };

    $scope.createNewVersion = function (Id) {
        if ($scope.templateIsEditable && $scope.hasLaterVersions) {
            $scope.changeVersion($scope.versionHistoryCurrent[0]);
        } else {
            $scope.LoadingNewVersion = true;
            Templates.createVersion(Id).then(function (response) {
                if (!response.result) return;

                if ($scope.templateType === ENUMS.TemplateTypes.Datamapping) {
                    $state.go("zen.app.pia.module.datamap.views.questionnaire", {
                        templateId: $scope.templateId,
                        version: $scope.version + 1
                    });
                } else {
                    $state.go($scope.routes.list);
                }
            });
        }
    };

    function checkLaterVersions() {
        if ($scope.permissions.TemplatesShowVersions) {
            var isCurrent = false;
            if ($scope.versionHistoryCurrent.length) {
                _.each($scope.versionHistoryCurrent, function (item) {
                    if (item.Version === $scope.version) {
                        isCurrent = true;
                    }
                });
                $scope.isHistoricalVersion = isCurrent ? false : true;
                $scope.hasLaterVersions = $scope.versionHistoryCurrent[0].Version === $scope.version ? false : true;
            } else {
                $scope.isHistoricalVersion = true;
                $scope.hasLaterVersions = true;
            }
        }
        $scope.templateIsEditable = $scope.readOnly && !$scope.isHistoricalVersion && ($scope.template.State !== $scope.TemplateStates.Archived);
    };

    var getVersionHistory = function () {
        if ($scope.permissions.TemplatesShowVersions) {
            $scope.versionHistoryCurrent = [];
            $scope.versionHistoryPrevious = [];
            $scope.versionHistoryArchived = [];
            var params = {
                id: $scope.templateId
            };
            return Templates.getVersions(params).then(function (response) {
                if (response.result) {
                    var versionHistory = response.data;
                    var foundPublished = false;
                    var foundArchived = false;
                    _.each(response.data, function (version) {
                        if (version.Version === $scope.version) {
                            version.isSelected = true;
                        }

                        if (!foundArchived) {
                            if (version.State === $scope.TemplateStates.Archived) {
                                $scope.versionHistoryArchived.push(version);
                                foundArchived = true;
                            } else if (!foundPublished) {
                                $scope.versionHistoryCurrent.push(version);
                                if (version.State === $scope.TemplateStates.Published) {
                                    foundPublished = true;
                                }
                            } else {
                                $scope.versionHistoryPrevious.push(version);
                            }
                        } else {
                            $scope.versionHistoryArchived.push(version);
                        }
                    });
                    $scope.versionsRetrieved = true;
                } else {
                    $scope.versionsRetrieved = false;
                }
            });
        } else {
            $scope.versionsRetrieved = false;
        }
    };

    $scope.toggleVersionMenu = function () {
        $scope.versionMenuShowing = !$scope.versionMenuShowing;
    };

    $scope.changeVersion = function (version) {
        $scope.Ready = false;
        _.each($scope.versionHistoryCurrent, function (version) {
            version.isSelected = false;
        });
        _.each($scope.versionHistoryPrevious, function (version) {
            version.isSelected = false;
        });
        _.each($scope.versionHistoryArchived, function (version) {
            version.isSelected = false;
        });
        version.isSelected = true;
        $scope.version = version.Version;

        readTemplate().then(function (response) {
            checkLaterVersions();
            $scope.Ready = true;
        });
    };

    var getSettings = function () {
        return SettingStore.fetchProjectSettings()
            .then(saveSettingsResponseToScope)
            .then(createSelfServiceParamsIfEnabled)
            .then(fetchSelfServiceInfoIfEnabled)
            .then(saveSelfServiceResponseToScope);
    };

    var saveSettingsResponseToScope = function (response) {
        if (response) {
            $scope.allowThresholds = response.EnableThresholdTemplates;
            $scope.selfServiceEnabled = response.SelfServiceEnabled;
            return response.SelfServiceEnabled;
        }
        return false;
    };

    var createSelfServiceParamsIfEnabled = function (selfServiceEnabled) {
        if (selfServiceEnabled) {
            return {
                TemplateId: $stateParams.templateId,
                TemplateVersion: $scope.version || parseInt($stateParams.version)
            };
        }
        return false;
    };

    var fetchSelfServiceInfoIfEnabled = function (params) {
        if (params) {
            return SelfService.readConfiguration(params).then(function (response) {
                if (response.result) {
                    return response.data;
                }
                return false;
            });
        }
        return false;
    };

    var saveSelfServiceResponseToScope = function (selfServiceData) {
        if (selfServiceData) {
            $scope.selfServiceLinked = !_.isUndefined(selfServiceData.TemplateId);
            $scope.SelectAssignee = selfServiceData.SelectAssignee;
            $scope.SelectOrgGroup = selfServiceData.SelectOrgGroup;
            $scope.directLaunchEnabled = selfServiceData.DirectLaunch;
        }
        return false;
    };

    $scope.discardDraft = function (versionDetails) {
        var params = {
            id: $scope.template.Id,
            version: versionDetails.Version
        };

        var confirmMessage = $rootScope.t("SureToDiscardQuestionnaireTemplateDraft", { TemplateDraftName: versionDetails.Name });
        OneConfirm("", confirmMessage).then(function (result) {
            if (result) {
                versionDetails.Loading = true;
                var hideNotification = true;
                Templates.deleteTemplate(params, hideNotification).then(function (response) {
                    if (response.result) {
                        var expression = "Version === " + params.version;
                        $scope.versionHistoryCurrent = $filter("omit")($scope.versionHistoryCurrent, expression);

                        if (params.version === $scope.template.Version) {
                            if (params.version > 1) {
                                OneAlert("", $rootScope.t("DraftDiscardedReturnToPrevious")).then(function (result) {
                                    var stateParams = { templateId: params.id, version: (params.version - 1) };
                                    $state.go(".", stateParams);
                                });
                            } else {
                                OneAlert("", $rootScope.t("DraftDiscardedReturnToList")).then(function (result) {
                                    $state.go($scope.routes.list);
                                });
                            }
                        }
                    }
                    versionDetails.Loading = false;
                });
            }
        });
    };

    function createLoadingPlaceholder(section, groupIndex) {
        var placeholderGroup = {
            Type: $scope.QuestionTypes.PlaceholderGroup,
            SectionId: section.Id
        };

        if (_.isArray(section.QuestionGroups) && section.QuestionGroups.length) {
            section.QuestionGroups.splice(groupIndex, 0, placeholderGroup);
        } else {
            section.QuestionGroups = [placeholderGroup];
        }
    }

    function deleteLoadingPlaceholder(section, groupIndex) {
        section.QuestionGroups.splice(groupIndex, 1);
    }

    $scope.init();

}

TemplateStudioManageCtrl.$inject = ["$scope", "$state", "$stateParams", "$q", "$filter", "$timeout", "$uibModal", "Permissions",
    "NotificationService", "Templates", "Sections", "Questions", "Icons", "Settings", "ENUMS", "DataMapping", "$rootScope",
    "QuestionTypes", "ApplicationQuestionGroup", "QuestionGroup", "SelfService", "TemplateStudio", "OneWelcomeTextService", "SettingStore"];
