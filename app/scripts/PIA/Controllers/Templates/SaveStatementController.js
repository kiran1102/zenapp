
export default function SaveStatementCtrl($rootScope, $scope, $uibModalInstance, Sections, Questions, ENUMS,
    Template, SectionId, Question, NextQuestionId) {
    $scope.translate = $rootScope.t;
    $scope.Ready = false;
    $scope.Disabled = false;
    $scope.tryAgain = false;
    $scope.Template = Template;
    $scope.questionTypeMap = ENUMS.QuestionTypes;
    $scope.translate = $rootScope.t;

    var defaultQuestion = {
        Id: "",
        Name: "",
        Type: ENUMS.QuestionTypes.Content,
        Content: ""
    };

    function init() {
        $scope.editorClassName = ".statement__container";
        $scope.editorConfig = {
            placeholder: $rootScope.t("EnterRequiredStatementHere")
        }
        $scope.Question = Question ? Question : defaultQuestion;

        if (Question && Question.Id) {
            Sections.getQuestion(Question.Id, Template.Version).then(function(response) {
                if (response.result) {
                    $scope.Question = response.data;
                }
                $scope.Ready = true;
            });
        } else {
            $scope.Ready = true;
        }
    }

    $scope.SaveStatement = function () {
        var question = {
            Id: $scope.Question.Id,
            Name: $scope.Question.Name,
            Type: $scope.Question.Type,
            Content: $scope.Question.Content
        };

        if (question.Id) {
            Questions.update(question).then(function (response) {
                if (response.result) {
                    var data = {
                        SectionId: SectionId,
                        QuestionId: Question.Id,
                        NextQuestionId: NextQuestionId,
                        Version: Template.Version
                    };
                    Sections.updateQuestion(data).then(function (res) {
                        $uibModalInstance.close(res);
                    });
                } else {
                    $scope.tryAgain = true;
                }
                $scope.Disabled = false;
            });
        } else {
            Questions.create(question).then(function (response) {
                if (response.result) {
                    var questionId = response.data;
                    var data = {
                        SectionId: SectionId,
                        QuestionId: questionId,
                        NextQuestionId: NextQuestionId,
                        Version: Template.Version
                    };
                    Sections.addQuestion(data).then(function (res) {
                        $uibModalInstance.close(res);
                    });
                } else {
                    $scope.tryAgain = true;
                }
                $scope.Disabled = false;
            });
        }
    };

    $scope.done = function (done) {
        $uibModalInstance.close(done);
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss("cancel");
    };

    $scope.updateQuestionContent = function (data) {
        $scope.$apply(function () {
            $scope.Question.Content = data;
        });
    }

    init();
}

SaveStatementCtrl.$inject = ["$rootScope", "$scope", "$uibModalInstance", "Sections", "Questions", "ENUMS",
    "Template", "SectionId", "Question", "NextQuestionId"
];
