﻿import * as _ from "lodash";
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";

interface IEditorConfig {
    placeholder: string;
}

export default class WelcomeTextCtrl {
    static $inject: string[] = ["$rootScope", "$scope", "$uibModalInstance", "WelcomeText"];

    private translations: any;
    private editorClassName: string;
    private editorConfig: IEditorConfig;

    constructor(
        private readonly $rootScope: IExtendedRootScopeService,
        private readonly $scope: any,
        private readonly $uibModalInstance: any,
        private WelcomeText: any,
    ) {
        this.translations = {
            cancel: $rootScope.t("Cancel"),
            placeholder: $rootScope.t("EnterQuestionnaireWelcomeText"),
            save: $rootScope.t("Save"),
            welcomeText: $rootScope.t("WelcomeText"),
        };
        this.editorConfig = {
            placeholder: this.translations.placeholder,
        };

        this.setWelcomeText(WelcomeText);
    }

    public setWelcomeText(welcomeText: string): void {
        if (!_.isUndefined(welcomeText) && _.isString(welcomeText)) {
            this.WelcomeText = welcomeText;
        } else {
            this.WelcomeText = "";
        }
    }

    public save(): void {
        this.done({
            result: true,
            data: this.WelcomeText,
        });
    }

    public done(results: any): void {
        this.$uibModalInstance.close(results);
    }

    public cancel(): void {
        this.$uibModalInstance.dismiss("cancel");
    }

    public updateWelcomeText(data: string): void {
        this.WelcomeText = data;
    }
}
