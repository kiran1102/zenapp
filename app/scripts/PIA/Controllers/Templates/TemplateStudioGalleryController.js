import Utilities from "Utilities";
import { AssessmentWelcomeSectionText, AssessmentTypes } from "constants/assessment.constants";

export default function TemplatesStudioGalleryCtrl($scope, $state, $uibModal, Templates, ENUMS, CONSTANTS, Permissions) {
    $scope.permissions = {
        TemplatesGalleryImport: Permissions.canShow("TemplatesGalleryImport"),
        TemplatesQuestionItemPreview: Permissions.canShow("TemplatesQuestionItemPreview"),
    };
    $scope.templateType = $state.includes(AssessmentTypes.DM.BaseRoute) ? ENUMS.TemplateTypes.Datamapping : ENUMS.TemplateTypes.Custom;

    $scope.routes = {
		single: ($scope.templateType === ENUMS.TemplateTypes.Datamapping) ? "zen.app.pia.module.datamap.views.questionnaire" : "zen.app.pia.module.templates.studio.manage",
		gallery: ($scope.templateType === ENUMS.TemplateTypes.Datamapping) ? "zen.app.pia.module.datamap.views.questionnaire-gallery" : "zen.app.pia.module.templates.studio.gallery",
		list: ($scope.templateType === ENUMS.TemplateTypes.Datamapping) ? "zen.app.pia.module.datamap.views.questionnaire-list" : "zen.app.pia.module.templates.list",
	};

	$scope.isCollapsed = true;
	$scope.pageSlide = {};
	$scope.QuestionTypes = ENUMS.QuestionTypes;
	$scope.TemplateStates = ENUMS.TemplateStates;

	$scope.SelectedId = "";
	$scope.Version = "";
	$scope.SelectedIndex = 0;
	$scope.Open = true;

	$scope.template = null;
	$scope.WelcomeText = "";
	$scope.ShowWelcomeText = true;


	$scope.toggleWelcome = function () {
		$scope.ShowWelcomeText = !$scope.ShowWelcomeText;
	};

	$scope.init = function () {
		$scope.Ready = false;
		$scope.template = null;
		$scope.getGallery().then(
			function () {
				$scope.Ready = true;
			}
		);
    };

    function overwriteFaWithOt(templates) {
        return templates.map(function (template) {
            template.Icon = template.Icon.replace(/fa-/, "ot-");
            return template;
        });
    }

	$scope.getGallery = function () {
		return Templates.gallery().then(
			function (response) {
                $scope.templates = ((response.result && _.isArray(response.data)) ? response.data : []);

                // fa icons break on ChromeOS. this function overwrites fa with ot.
                $scope.templates = overwriteFaWithOt($scope.templates);

				return $scope.templates;
			}
		);
	};

	$scope.slideToggle = function (template) {
		if ($scope.SelectedId === template.Id) return;

		$scope.SelectedId = template.Id;
		$scope.ShowWelcomeText = false;

		if (template.State !== $scope.TemplateStates.Locked) {
			$scope.readTemplate();
		} else {
			$scope.showPreview(template);
		}
	};

	// function to read our template
	$scope.readTemplate = function () {
		$scope.loadingTemplate = true;
		$scope.template = null;
		$scope.showingUpgradeMessage = false;
		var params = {
			id: $scope.SelectedId,
			version: 0
		};
        Templates.readTemplateGallery(params).then(
			function (response) {
				if (response.result) {
					$scope.template = response.data;

					// Check for Welcome Text.
					$scope.template.HasWelcomeText = _.isString($scope.template.WelcomeText) && ($scope.template.WelcomeText.trim() !== "");

					if (!$scope.template.HasWelcomeText) {
						$scope.template.WelcomeText = AssessmentWelcomeSectionText;
						$scope.template.HasWelcomeText = true;
					}

					$scope.WelcomeText = $scope.template.WelcomeText;
					$scope.ShowWelcomeText = true;

					if ($scope.template.Sections) {
                        var questionCount = 1;
                        _.each($scope.template.Sections, function(section, sectionIndex) {
                            if (section.Questions) {
                                _.each(section.Questions, function(question, questionIndex) {
                                    if (question.Type != $scope.QuestionTypes.Content) {
                                        question.Number = questionCount++;
                                    }
                                    question = $scope.setQuestionDataByType(question);
                                });
                            }
                        });
					}

				}
				$scope.SelectedIndex = 0;
				$scope.loadingTemplate = false;
			}
		);
	};

	$scope.showPreview = function (template) {
		$scope.template = template;
		$scope.showingUpgradeMessage = true;
	};

	$scope.setQuestionDataByType = function (question) {
		switch (question.Type) {
			case $scope.QuestionTypes.YesNo: // Yes/No
				if (_.isUndefined(question.Value) || question.Value === null) {
					question.Answer = null;
				}
				else {
					// question.Answer = question.Value === (true || "true" || "True");
					question.Answer = null;
				}
				break;
			case $scope.QuestionTypes.Multichoice:
				if (question.MultiSelect) {
					if (_.isUndefined(question.Value) || question.Value === null) {
						question.Answer = [];
					}
					else {
						question.Answer = question.Value;
						_.each(question.Options, function(option, oIndex) {
							question.Options[oIndex].Answer = question.Answer.indexOf(option.Value) >= 0;
						});
					}
				} else {
					// If the value is a string, lets convert it to an int.
					// This relates to the index of the Option in the Options Array
					// Else, catch by default.
					if (_.isUndefined(question.Value) || question.Value === null) {
						if (_.isString(question.Value)) {
							question.Answer = parseInt(question.Value);
						}
					} else {
						question.Answer = 1;
					}
				}
				break;
			case $scope.QuestionTypes.TextBox: // Textbox
			default: // Default : 0
				question.Answer = question.Value;
				break;
		}
		return question;
	};

	// Opens the Create Template Modal
	$scope.openCopyTemplateModal = function () {
		var modalInstance = $uibModal.open({
			templateUrl: "copyTemplateModal.html",
			controller: "CopyTemplateCtrl",
			scope: $scope,
			size: "lg",
			backdrop: "static",
			keyboard: false,
			resolve: {
				CopyTemplate: function () {
					return $scope.template;
				}
			}
		});

		modalInstance.result.then(function (response) {
			// Push the new template to our list if results were true (success)
			if (response.result) {
				$state.go($scope.routes.single, { templateId: response.data, version: 1 });
			}
		});
	};

	$scope.isSelected = function (sectionId, questionId, oIndex) {
		var sectionIndex = _.findIndex($scope.template.Sections, ["Id", sectionId]);
		if (sectionIndex >= 0) {
			var questionIndex = _.findIndex($scope.template.Sections[sectionIndex].Questions, ["Id", questionId]);
			if (questionIndex >= 0) {
				if (oIndex >= 0) {
					var answer = $scope.template.Sections[sectionIndex].Questions[questionIndex].Answer;

					if (_.isArray(answer)) {
						return answer.indexOf($scope.template.Sections[sectionIndex].Questions[questionIndex].Options[oIndex].Value.toString()) >= 0;
					}
					else if (_.isString(answer)) {
						return (answer === $scope.template.Sections[sectionIndex].Questions[questionIndex].Options[oIndex].Value.toString());
					}
					return false;
				}
			}
		}
		return false;
	};

	$scope.galleryIsCollapsed = function () {
		return $scope.template || $scope.showingUpgradeMessage || $scope.loadingTemplate;
	};

	$scope.unCollapseGallery = function () {
		$scope.template = null;
		$scope.showingUpgradeMessage = false;
		$scope.loadingTemplate = false;
		$scope.SelectedId = null;
	};

	$scope.Cancel = function () {
		$state.go($scope.routes.list);
	};

	$scope.init();

}

TemplatesStudioGalleryCtrl.$inject = ["$scope", "$state", "$uibModal", "Templates", "ENUMS", "CONSTANTS", "Permissions"];
