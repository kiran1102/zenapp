
export default function AddSectionCtrl($scope, $uibModalInstance, templateId, version, Next, Templates, $rootScope) {

  $scope.Section = {
    TemplateId: templateId,
    Name: "",
    NextSectionId: Next,
    Version: version
  };

  $scope.tryAgain = false;
  $scope.stage = 0; // Entering information

  // Submit request to Create a Question
  $scope.AddSection = function () {
    $scope.stage = 1; // Processing

    // Associating the newly created Question to our template.
    Templates.addSection($scope.Section)
      .then(function (response) {
        $scope.type = response.type;
        if (response.result) {
          $scope.results = $rootScope.t("SectionCreatedAndAddedToTemplate");
          $scope.Section.Id = response.data;
          var data = {
            result: response.result,
            data: $scope.Section
          };
          $scope.stage = 2;
          $uibModalInstance.close(data);
        }
        else {
          $scope.results = $rootScope.t("CouldNotAddSectionToTemplate");
          var data = {
            results: response.result,
            response: response.data
          };
          $scope.stage = 2;
          $uibModalInstance.close(data);
        }
      });
  };

  $scope.done = function () {
    $uibModalInstance.close($scope.results);
  };

  $scope.cancel = function () {
    $uibModalInstance.dismiss("cancel");
  };
}

AddSectionCtrl.$inject = ["$scope", "$uibModalInstance", "templateId", "version", "Next", "Templates", "$rootScope"];
