﻿
export default function SelectIconCtrl($scope, $uibModalInstance, SelectedIcon, Icons) {

  $scope.TopIcons = Icons.topList();

  $scope.SelectedIcon = SelectedIcon;

  $scope.SetIcon = function (index) {
    var icon = $scope.TopIcons[index];
    if (icon !== $scope.SelectedIcon) {
      $scope.SelectedIcon = icon;
      $scope.done(
        {
          result: true,
          data: $scope.SelectedIcon
        }
      );
    }
  };

  $scope.done = function (results) {
    $uibModalInstance.close(results);
  };

  $scope.cancel = function () {
    $uibModalInstance.dismiss("cancel");
  };
}

SelectIconCtrl.$inject = ["$scope", "$uibModalInstance", "SelectedIcon", "Icons",];
