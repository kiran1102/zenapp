
export default function EditSectionCtrl($scope, $uibModalInstance, Section, Template, Sections, EditMode, $rootScope, Permissions) {
    $scope.permissions = {
        TemplatesSectionEdit: Permissions.canShow("TemplatesSectionEdit"),
    }
    var backupSectionName = Section.Name;
    $scope.Section = Section;
    $scope.editMode = EditMode;

    $scope.tryAgain = false;
    $scope.stage = 0; // Entering information

    // Submit request to Create a Question
    $scope.EditSection = function () {
        $scope.stage = 1; // Processing

        var sectionData = {
            "Id": $scope.Section.Id,
            "Name": $scope.Section.Name,
            "Version": Template.Version
        };

        Sections.putSection(sectionData)
            .then(function (response) {
                $scope.type = response.type;
                if (response.result) {
                    $scope.results = $rootScope.t("SectionUpdated");

                    var data = {
                        result: response.result,
                        data: $scope.Section
                    };
                    $scope.stage = 2;
                    $uibModalInstance.close(data);
                } else {
                    $scope.results = $rootScope.t("TheSectionCouldNotBeUpdated");
                    var data = {
                        results: response.result,
                        response: response.data
                    };
                    $scope.stage = 2;
                    $uibModalInstance.close(data);
                }
            });
    };

    $scope.DeleteSection = function () {
        var version = Template.Version;
        var Section = $scope.Section;
        var confirmMessage = $rootScope.t("AreYouSureToDeleteSectionName", { SectionName: Section.Name });
        OneConfirm("", confirmMessage).then(
            function (result) {
                if (result) {
                    Sections.deleteSection(Section.Id, version).then(
                        function (response) {
                            if (response.result) {
                                Template.Sections.splice(Template.SelectedIndex - 1, 1);
                                if (Template.SelectedIndex > 1) Template.SelectedIndex--;
                                $uibModalInstance.close(response.data);
                            }
                        }
                    );
                }
            }
        );
    };

    $scope.done = function () {
        $uibModalInstance.close($scope.results);
    };

    $scope.cancel = function () {
        Section.Name = backupSectionName;
        $uibModalInstance.dismiss("cancel");
    };

}

EditSectionCtrl.$inject = ["$scope", "$uibModalInstance", "Section", "Template", "Sections", "EditMode", "$rootScope", "Permissions"];
