
export default function EditTemplatesCtrl($scope, $uibModalInstance, Icons, Templates, ID, $rootScope) {

  $scope.EditWelcomeText = false;

  Templates.readTemplate(ID)
    .then(function successCallback(response) {
      $scope.editTemplate = response.data;

      if (_.isString($scope.editTemplate.WelcomeText)
        && ($scope.editTemplate.WelcomeText !== "")) {
        $scope.editTemplate.AddWelcomeText = true;
      }
      else {
        $scope.editTemplate.AddWelcomeText = false;
      }

    }, function errorCallback(response) {
      $scope.editTemplate = {
        "Name": "",
        "Description": "",
        "Icon": "fa-file-text-o",
        "WelcomeText": "",
        "AddWelcomeText": false
      };

    });

  $scope.EditWelcomeText = false;

  $scope.topIconsList = Icons.topList();

  $scope.stage = 0; // Entering information

  // Submit a request to update template.
  $scope.updateTemplate = function () {
    $scope.stage = 1; // Processing

    var template = {
      "Id": $scope.editTemplate.Id,
      "Name": $scope.editTemplate.Name,
      "Description": $scope.editTemplate.Description,
      "Version": $scope.editTemplate.Version,
      "Icon": $scope.editTemplate.Icon
    };

    if ($scope.editTemplate.AddWelcomeText) {
      template.WelcomeText = $scope.editTemplate.WelcomeText;
    }
    else {
      template.WelcomeText = "";
    }

    Templates.updateTemplate(template)
      .then(function (response) {
        $scope.type = response.type;

        // Return data to be displayed if result is true
        if (response.result) {
            Templates.readTemplate(template.Id)
            .then(function (res) {
              if (res.result) {
                $uibModalInstance.close(res);
              }
              else {
                $scope.results = $rootScope.t("CouldNotReadUpdatedTemplate");
              }
            });
          $scope.results = $rootScope.t("TemplateUpdated");
        }
        else {
          $scope.results = response.data;
        }

        $scope.tryAgain = !response.result;
        $scope.stage = 2;
      });
  };

  $scope.done = function () {
    var data = {
      results: $scope.results,
      response: $scope.response.data
    };
    $uibModalInstance.close(data);
  };

  $scope.cancel = function () {

    OneConfirm("", $rootScope.t("SureToCancelYouWillLoseYourChanges")).then(
      function (result) {
        if (result) {
          if (!$scope.result) {
            $scope.done();
          }
          else {
            $uibModalInstance.dismiss("cancel");
          }
        }
      }
    );
  };
}

EditTemplatesCtrl.$inject = ["$scope", "$uibModalInstance", "Icons", "Templates", "ID", "$rootScope"];
