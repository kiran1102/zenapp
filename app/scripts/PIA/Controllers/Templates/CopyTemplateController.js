
export default function CopyTemplateCtrl($scope, $uibModalInstance, Icons, Templates, CopyTemplate, $rootScope, Permissions) {
    $scope.permissions = {
        TemplatesGalleryImport: Permissions.canShow("TemplatesGalleryImport"),
    };
    $scope.topIconsList = Icons.topList();

    $scope.newTemplate = {
        Id: CopyTemplate.Id,
        Name: CopyTemplate.Name,
        Description: CopyTemplate.Description,
        Icon: CopyTemplate.Icon,
        Version: CopyTemplate.Version,
        WelcomeText: CopyTemplate.WelcomeText
    };

    $scope.newTemplate.AddWelcomeText = $scope.newTemplate.WelcomeText !== ("" && null && undefined);

    $scope.tryAgain = false;
    $scope.stage = 0; // Entering information

    // Submit request to Create a template
    $scope.copyTemplate = function () {
        $scope.stage = 1; // Processing
        if (_.isObject($scope.newTemplate)) {
            var template = {
                Id: $scope.newTemplate.Id,
                Name: $scope.newTemplate.Name,
                Description: $scope.newTemplate.Description,
                Icon: $scope.newTemplate.Icon,
                Version: $scope.newTemplate.Version,
                WelcomeText: $scope.newTemplate.WelcomeText
            };

            Templates.cloneTemplateGallery(template).then(function (response) {
                // Return data to be displayed if result is true
                if (response.result) {
                    $uibModalInstance.close(response);
                    $scope.results = $rootScope.t("TemplateCopied");
                }
                else {
                    $scope.results = response.data;
                }

                $scope.tryAgain = !response.result;
                $scope.stage = 2;
            });
        } else {
            $scope.results = $rootScope.t("CouldNotCopyTemplate");
            $scope.tryAgain = true;
            $scope.stage = 2;
        }
    };

    $scope.retry = function () {
        $scope.stage = 0;
        $scope.tryAgain = !$scope.tryAgain;
    };

    $scope.done = function (done) {
        var result = done ? done : { result: false, data: false };
        $uibModalInstance.close(result);
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss("cancel");
    };
}

CopyTemplateCtrl.$inject = ["$scope", "$uibModalInstance", "Icons", "Templates", "CopyTemplate", "$rootScope", "Permissions"];
