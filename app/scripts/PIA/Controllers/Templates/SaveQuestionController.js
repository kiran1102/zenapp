import * as _ from "lodash";
import { IQuestionTypes, MultiChoiceTypes } from "enums/question-types.enum";

export default function SaveQuestionCtrl($rootScope, $scope, $q, $uibModalInstance, $timeout, Sections, Questions, ENUMS,
    Template, SectionId, NextQuestionId, LoadedQuestion, EditMode, ReadOnly, LockedQuestionTypes,
    PreSelected, Tags, QuestionTypes, Settings, Permissions) {
    $scope.permissions = {
        TemplatesQuestionAdd: Permissions.canShow("TemplatesQuestionAdd"),
        TemplatesQuestionTypeDataElement: Permissions.canShow("TemplatesQuestionTypeDataElement"),
    };
    $scope.Ready = false;
    $scope.Disabled = false;
    $scope.SaveForm = null;
    $scope.tryAgain = false;
    $scope.tagPending = false;
    $scope.editMode = ReadOnly ? EditMode : false;
    $scope.Template = Template;
    $scope.LockedQuestionTypes = LockedQuestionTypes;
    $scope.questionTypeMap = IQuestionTypes;
    $scope.questionMultichoiceType = MultiChoiceTypes;
    $scope.MultiChoiceTypes = ENUMS.MultiChoiceTypes;
    $scope.questionTaggingEnabled = false;
    $scope.BackupOptions = [{
        Name: "",
        Order: 0,
        Value: 0,
        Description: ""
    }];
    $scope.tagInputPlaceholder = $rootScope.t("AddTags").toString();
    $scope.QuestionTypes = QuestionTypes.getSelectableQuestionTypes();
    $scope.DataInventoryOptions = [];
    $scope.tagsList = [];
    $scope.unselectedTagsList = [];

    var defaultQuestion = {
        Name: "",
        Number: 1,
        ReportFriendlyName: "",
        Text: "",
        Hint: "",
        Required: false,
        Type: PreSelected ? PreSelected : $scope.questionTypeMap.TextBox,
        Tags: [],
        Multiline: true,
        MultiSelect: false,
        AllowOther: false,
        AllowNotSure: false,
        AllowNotApplicable: false,
        AllowJustification: false,
        RequireJustification: false,
        Options: _.cloneDeep($scope.BackupOptions),
        Filters: [],
        Answer: "",
        DisplayTypeId: $scope.MultiChoiceTypes.Button,
        OptionHintEnabled: false,
    };
    $scope.Question = LoadedQuestion ? LoadedQuestion : defaultQuestion;
    $scope.bindingUniqId = _.uniqueId();

    var promiseArray = [getTaggingSettings()];
    if (LoadedQuestion && LoadedQuestion.Id) {
        promiseArray.push(getQuestion(LoadedQuestion.Id, $scope.Template.Version));
    } else if ($scope.permissions.TemplatesQuestionTypeDataElement) {
        promiseArray.push(getInventoryOptions(ENUMS.InventoryTableIds.Elements));
    }

    $q.all(promiseArray).then(_.spread(function (tagsList, question, inventoryOptions) {
        if (!$scope.Question.Id) {
            $scope.$watch("Question.Type", function (value, oldValue) {
                if (oldValue === $scope.questionTypeMap.Multichoice) {
                    $scope.BackupOptions = $scope.Question.Options;
                }

                if (value === $scope.questionTypeMap.DataElement) {
                    $scope.Question.Options = $scope.DataInventoryOptions;
                } else if (value === $scope.questionTypeMap.Multichoice) {
                    $scope.Question.Options = $scope.BackupOptions;
                }
            });
        }

        if (question) {
            $scope.unselectedTagsList = _.xorBy(tagsList, question.Tags, "Label")
        } else {
            $scope.unselectedTagsList = tagsList;
        }

        $scope.BackupQuestion = _.cloneDeep($scope.Question);
        $scope.Ready = true;
    }));

    $scope.SetForm = function (form) {
        $scope.SaveForm = form;
    };

    function getTaggingSettings() {
        return Settings.getTagging()
            .then(saveTagSettingToScope)
            .then(fetchTagDataIfEnabled)
            .then(sortTagData)
            .then(saveTagDataToScope);
    }

    function saveTagSettingToScope(response) {
        var questionTaggingEnabled = response.data.QuestionTaggingEnabled || false;
        if (response.result) {
            $scope.questionTaggingEnabled = questionTaggingEnabled;
        }
        return questionTaggingEnabled;
    }

    function fetchTagDataIfEnabled(taggingEnabled) {
        if (taggingEnabled) {
            return Tags.getTags("", 50).then(function (response) {
                if (response.result) {
                    return response.data;
                }
                return false;
            });
        }
        return false;
    }

    function sortTagData(tagData) {
        if (tagData) {
            return _.sortBy(tagData, [function (i) { return i.Label; }]);
        }
        return false;
    }

    function saveTagDataToScope(tagData) {
        var tagsList = [];
        if (tagData) {
            tagsList = tagData;
            $scope.tagsList = tagData;
        }
        return tagsList;
    }

    function getQuestion(id, version) {
        return Sections.getQuestion(id, version).then(function (res) {
            if (res.result) {
                return res.data;
            }
            return false;
        })
            .then(formatQuestionResponse(LoadedQuestion))
            .then(saveQuestionToScope);
    }

    function formatQuestionResponse(loadedQuestion) {
        return function (question) {
            if (question) {
                question.Number = loadedQuestion.Number;
                if (question.Options && question.Options.length < 1) {
                    question.Options = [{
                        Name: "",
                        Order: 0,
                        Value: 0,
                        Description: "",
                        Hint: "",
                    }];
                }
                if (question.OptionHintEnabled) {
                    _.each(question.Options, function (option) {
                        option.tooltipHintEnabled = true;
                    });
                }
                return question;
            }
            return false;
        }
    }

    function saveQuestionToScope(question) {
        if (question) {
            $scope.Question = question;
            return question;
        }
        return false;
    }

    function getInventoryOptions(tableId) {
        return Questions.getInventoryOptions(tableId).then(function (response) {
            if (response.result && _.isArray(response.data)) {
                $scope.DataInventoryOptions = response.data;
            } else {
                $scope.DataInventoryOptions = [];
            }
        });
    }

    $scope.showAttribute = function (property) {
        if ($scope.Question.OptionListTypeId === $scope.questionMultichoiceType.AssetDiscovery) {
            $scope.Question.Type = $scope.questionTypeMap.AssetDiscovery;
        }
        return (Template.State === ENUMS.TemplateStates.Draft || QuestionTypes.propIsAllowedForEditing(property, $scope.Question)) &&
            QuestionTypes.questionHasAttribute($scope.Question.Type, property, Boolean($scope.Question.GroupId));
    };

    $scope.typeHasPermission = function (type) {
        return type.Permission.Key ? Permissions.canShow(type.Permission.Key) : true;
    };

    $scope.clearOption = function (index) {
        $scope.Question.Options[0].Name = "";
        if ($scope.Question.OptionHintEnabled) {
            delete $scope.Question.Options[0].Hint;
        }
    };

    $scope.addOption = function () {
        $scope.Question.Options.push({
            Value: $scope.Question.Options.length,
            Name: "",
            Description: "",
            Hint: "",
            tooltipHintEnabled: $scope.Question.OptionHintEnabled,
        });
    };

    $scope.removeOption = function (index) {
        $scope.Question.Answer = null;
        if ($scope.Question.Options.length < 2) {
            $scope.clearOption(index);
        } else {
            $scope.Question.Options.splice(index, 1);
        }
    };

    $scope.toggleCheckbox = function (property) {
        if (property === "MultiSelect" && !$scope.Question.MultiSelect && $scope.Question.DisplayTypeId === $scope.MultiChoiceTypes.Dropdown) {
            $scope.Question.AllowOther = false;
        }
        $scope.Question[property] = !$scope.Question[property];
    };

    $scope.SaveQuestion = function () {
        $scope.Disabled = true;
        $scope.Ready = false;
        if (!$scope.Question.Text) {
            $scope.Question.Text = null;
        }
        $scope.Question.RequireJustification = $scope.Question.Required ? $scope.Question.RequireJustification : false;

        var question = {
            Id: $scope.Question.Id || "",
            Name: $scope.Question.Name,
            Text: $scope.Question.Text,
            Hint: $scope.Question.Hint,
            Required: $scope.Question.Required,
            Type: $scope.Question.Type,
            Tags: $scope.Question.Tags,
            AllowNotSure: $scope.Question.AllowNotSure,
            AllowNotApplicable: $scope.Question.AllowNotApplicable,
            AllowJustification: $scope.Question.AllowJustification,
            RequireJustification: $scope.Question.RequireJustification && $scope.Question.AllowJustification,
            DisplayTypeId: $scope.Question.DisplayTypeId,
            OptionHintEnabled: $scope.Question.OptionHintEnabled,
        };

        switch (question.Type) {
            case $scope.questionTypeMap.TextBox:
                question.Multiline = true;
                break;
            case $scope.questionTypeMap.Multichoice:
                question.AllowOther = $scope.Question.AllowOther;
                question.MultiSelect = $scope.Question.MultiSelect;
                question.OptionListTypeId = $scope.questionMultichoiceType.Multichoice;
                question.Options = _.map($scope.Question.Options, function (option, index) {
                    var payload = {
                        Name: option.Name,
                        Value: index,
                        Description: "",
                    };
                    if ($scope.Question.OptionHintEnabled) {
                        payload["Hint"] = option.Hint;
                    }
                    return payload;
                });
                break;
            case $scope.questionTypeMap.AssetDiscovery:
                question.AllowOther = $scope.Question.AllowOther;
                question.MultiSelect = $scope.Question.MultiSelect;
                question.OptionListTypeId = $scope.questionMultichoiceType.AssetDiscovery;
                question.Type = $scope.questionTypeMap.Multichoice;
                question.QuestionType = $scope.questionTypeMap.Multichoice;
                break;
            case $scope.questionTypeMap.YesNo:
                question.Value = $scope.Question.Value;
                break;
            case $scope.questionTypeMap.Date:
                break;
            case $scope.questionTypeMap.DataElement:
                question.MultiSelect = true;
                question.AllowOther = $scope.Question.AllowOther;
                break;
            case $scope.questionTypeMap.Application:
            case $scope.questionTypeMap.Location:
            case $scope.questionTypeMap.User:
            case $scope.questionTypeMap.Provider:
                question.MultiSelect = false;
                question.AllowOther = $scope.Question.AllowOther;
                break;
            default:
        }

        if (question.Id) {
            Questions.update(question).then(function (response) {
                if (response.result) {
                    var params = buildSectionParams(SectionId, LoadedQuestion.Id, NextQuestionId, $scope.Question.ReportFriendlyName, Template.Version);

                    Sections.updateQuestion(params).then(function (res) {
                        if (res.result) {
                            saveFilters(LoadedQuestion.Id).then(function () {
                                $scope.results = $rootScope.t("QuestionUpdated");
                                $uibModalInstance.close(res);
                            });
                        } else {
                            $scope.results = $rootScope.t("CouldNotUpdateQuestion");
                            $uibModalInstance.close(res);
                        }
                    });
                } else {
                    $scope.tryAgain = true;
                    $scope.Disabled = false;
                    $scope.Ready = true;
                }
            });
        } else {
            Questions.create(question).then(function (response) {
                $scope.type = response.type;
                if (response.result) {
                    $scope.results = $rootScope.t("QuestionCreated");
                    var params = buildSectionParams(SectionId, response.data, NextQuestionId, $scope.Question.ReportFriendlyName, Template.Version);

                    Sections.addQuestion(params).then(function (res) {
                        if (res.result) {
                            saveFilters(res.data).then(function () {
                                $scope.results = $rootScope.t("QuestionCreatedAndAddedToTemplate");
                                $uibModalInstance.close(res);
                            });
                        } else {
                            $scope.results = $rootScope.t("CouldNotAddQuestionToTemplate");
                            $uibModalInstance.close(res);
                        }
                    });
                } else {
                    $scope.results = response.data;
                    $scope.tryAgain = true;
                    $scope.Disabled = false;
                    $scope.Ready = false;
                }
            });
        }
    };

    function buildSectionParams(sectionId, loadedQuestionId, nextQuestionId, reportFriendlyName, templateVersion) {
        return {
            SectionId: sectionId,
            QuestionId: loadedQuestionId,
            NextQuestionId: nextQuestionId,
            ReportFriendlyName: reportFriendlyName,
            Version: templateVersion,
        };
    }

    function saveFilters(questionId) {
        var oldFilters = $scope.BackupQuestion.Filters || [];
        var newFilters = $scope.Question.Filters || [];

        var toggleArray = _.xor(oldFilters, newFilters);
        var promises = _.map(toggleArray, function (id) {
            var toggleCategory = {
                TemplateQuestionId: questionId,
                FieldId: id,
                Version: $scope.template.Version
            };
            return Questions.updateQuestionCategory(toggleCategory);
        });

        return $q.all(promises);
    }

    $scope.done = function (done) {
        $uibModalInstance.close(done);
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss("cancel");
    };

    $scope.tagSelectCallback = function (tag) {
        if (!tagAlreadyExists($scope.tagsList, tag)) {
            $scope.tagsList = addTagToList($scope.tagsList, tag);
            Tags.addTag(tag).then(function (response) {
                tag.Id = response.data;
            });
        } else {
            tag.Id = addTagId($scope.unselectedTagsList, tag);
            $scope.unselectedTagsList = removeTagFromList($scope.unselectedTagsList, tag);
        }
        $scope.Question.Tags = addTagToList($scope.Question.Tags, tag);
    }

    $scope.tagRemoveCallback = function (tag) {
        $scope.unselectedTagsList = addTagToList($scope.unselectedTagsList, tag);
        $scope.Question.Tags = removeTagFromList($scope.Question.Tags, tag);
    }

    function addTagId(tagsList, tag) {
        var index = _.findIndex(tagsList, function (i) { return i.Label === tag.Label; });
        tag.Id = tagsList[index].Id;
        return tag.Id;
    }

    function tagAlreadyExists(tagsList, tag) {
        return tagsList.length && -1 !== _.findIndex(tagsList, function (item) {
            return item.Label === tag.Label;
        });
    }

    function addTagToList(tagsList, tag) {
        var index = _.sortedIndexBy(tagsList, tag, function (item) {
            return item.Label;
        });
        return _.concat(tagsList.slice(0, index), [tag], tagsList.slice(index));
    }

    function removeTagFromList(tagsList, tag) {
        var index = _.findIndex(tagsList, function (i) { return i.Label === tag.Label; });
        return _.concat(tagsList.slice(0, index), tagsList.slice(index + 1));
    }

    $scope.updateOptionHintSetting = function () {
        $scope.Question.OptionHintEnabled = !$scope.Question.OptionHintEnabled;
        _.each($scope.Question.Options, function (option) {
            option.tooltipHintEnabled = $scope.Question.OptionHintEnabled;;
        });
    };


    $scope.updateDropdownSetting = function () {
        var dropdownId = $scope.MultiChoiceTypes.Dropdown;
        var buttonId = $scope.MultiChoiceTypes.Button;
        var isDropdown = $scope.Question.DisplayTypeId === dropdownId;
        $scope.Question.Answer = null;
        if ($scope.Question.MultiSelect && $scope.Question.DisplayTypeId === $scope.MultiChoiceTypes.Dropdown) {
            $scope.Question.AllowOther = false;
        }

        if (isDropdown) {
            $scope.Question.DisplayTypeId = buttonId;
        } else {
            $scope.Question.DisplayTypeId = dropdownId;
        }
    };

    $scope.updateOptionHintHeight = function (e) {
        var element = _.isObject(e) ? e.target : document.getElementById(e);
        if (e.target.value) {
            element.style.height = "1px";
            element.style.height = element.scrollHeight + 20 + "px";
        } else {
            element.style.height = "3.6rem"; // when text is deleted, reset height
        }
    };

    $scope.resetOptionHintHeight = function (e) {
        var element = _.isObject(e) ? e.target : document.getElementById(e);
        element.style.height = "3.6rem";
    };

    $scope.updateQuestionText = function (data) {
        $scope.Question.Text = data;
    }
}

SaveQuestionCtrl.$inject = ["$rootScope", "$scope", "$q", "$uibModalInstance", "$timeout", "Sections", "Questions", "ENUMS",
    "Template", "SectionId", "NextQuestionId", "LoadedQuestion", "EditMode", "ReadOnly", "LockedQuestionTypes",
    "PreSelected", "Tags", "QuestionTypes", "Settings", "Permissions"
];
