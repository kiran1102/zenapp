import { AssessmentTypes } from "constants/assessment.constants";

export default function TemplatesCtrl($rootScope, $scope, $state, $uibModal, OnboardingService, Content, Users, Templates, ModalService, $timeout, ENUMS, currentUser, DataMapping, Permissions) {
    $scope.permissions = {
        TemplatesArchive: Permissions.canShow("TemplatesArchive"),
        TemplatesCopy: Permissions.canShow("TemplatesCopy"),
        TemplatesCreate: Permissions.canShow("TemplatesCreate"),
        TemplatesDiscard: Permissions.canShow("TemplatesDiscard"),
        TemplatesGallery: Permissions.canShow("TemplatesGallery"),
        TemplatesImport: Permissions.canShow("TemplatesImport"),
        TemplatesPublish: Permissions.canShow("TemplatesPublish"),
        TemplatesViewArchive: Permissions.canShow("TemplatesViewArchive"),
    };
    $scope.Ready = false;
    $scope.loadingTemplate = true;
    $scope.currentUser = currentUser;
    $scope.createTemplateClass = "";
    OnboardingService.onboard(
        $rootScope.t("Welcome to Questionnaire Templates"),
        Content.GetKey("OnBoardingQuestionnaire")
    );
    $scope.templateType = $state.includes(AssessmentTypes.DM.BaseRoute) ? ENUMS.TemplateTypes.Datamapping : ENUMS.TemplateTypes.Custom;
    $scope.TemplateStates = ENUMS.TemplateStates;
    $scope.routes = {
        single: ($scope.templateType === ENUMS.TemplateTypes.Datamapping) ? "zen.app.pia.module.datamap.views.questionnaire" : "zen.app.pia.module.templates.studio.manage",
        projects: ($scope.templateType === ENUMS.TemplateTypes.Datamapping) ? "zen.app.pia.module.datamap.views.assessment.list" : "zen.app.pia.module.projects.list",
        gallery: ($scope.templateType === ENUMS.TemplateTypes.Datamapping) ? "zen.app.pia.module.datamap.views.questionnaire-gallery" : "zen.app.pia.module.templates.studio.gallery",
        archive: ($scope.templateType === ENUMS.TemplateTypes.Datamapping) ? "zen.app.pia.module.datamap.views.questionnaire-archive" : "zen.app.pia.module.templates.archive",
        list: ($scope.templateType === ENUMS.TemplateTypes.Datamapping) ? "zen.app.pia.module.datamap.views.questionnaire-list" : "zen.app.pia.module.templates.list",
    };

    var getTemplates = function () {
        Templates.list($scope.templateType).then(
            function (response) {
                if (response.result) {
                    $scope.templates = response.data;
                    // Add front end states
                    _.each($scope.templates, function(template) {
                        formatTemplate(template);
                    });
                    linkTemplates();
                    $scope.Ready = true;
                    $scope.loadingTemplate = false;
                } else {
                    $scope.templates = [];
                    $scope.Ready = true;
                    $scope.loadingTemplate = false;
                }
            }
        );
    };

    var formatTemplate = function (template) {
        // Set active state and active version
        if (template.Published) {
            template.activeState = "Published";
            template.activeVersion = template.Published.Version;
        } else if (template.Draft) {
            template.activeState = "Draft";
            template.activeVersion = template.Draft.Version;
        }
        // Show template info by default
        template.showInfo = true;
        template.Ready = true;
    };

    var linkTemplates = function () {
        var linkedList = [];
        _.each($scope.templates, function(template) {
            if (template.Published && template.Published.NextTemplateId) {
                var childIndex = _.findIndex($scope.templates, ["Id", template.Published.NextTemplateId]);
                var child = $scope.templates[childIndex];
                var linkedTemplate = {
                    isLinked: true,
                    parent: template,
                    child: child
                };
                linkedList.push(linkedTemplate);
            } else if (template.Published && template.Published.HasParents) {
                return;
            } else {
                linkedList.push(template);
            }
        });
        $scope.templates = linkedList;
    };

    $scope.init = function () {
        getTemplates();
    };

    $scope.openStudio = function () {
        $state.go($scope.routes.gallery);
    };

    $scope.openCreateTemplateModal = function () {
        var modalInstance = $uibModal.open({
            templateUrl: "createTemplateModal.html",
            controller: "CreateTemplateCtrl",
            scope: $scope,
            size: "lg",
            backdrop: "static",
            keyboard: false
        });

        modalInstance.result.then(
            function (response) {
                if (response.result) {
                    $scope.templates.push(response.data);
                }
            }
        );
    };
    $scope.openUploadTemplateModal = function () {
        const modalData = {
            modalTitle: $rootScope.t("ImportQuestionnaire"),
            content: $rootScope.t("SendImportQuestionnaireToEmailInfo", {
                EmailInfo: "<a href='mailto:support@onetrust.com?subject=Import Template'>support@onetrust.com</a>"
            }),
        };
        ModalService.setModalData(modalData);
        ModalService.openModal("helpDialogModal");
    };

    $scope.copy = function (template, version) {
        var originalParams = {
            id: template.Id,
            version: version
        };
        // Get selected version of template for copy
        Templates.readTemplate(originalParams).then(
            function (response) {
                if (response.data) {
                    if (_.isObject(response.data)) {
                        var copy = {
                            "Id": response.data.Id,
                            "Name": response.data.Name + " (Copy)",
                            "Description": response.data.Description,
                            "Icon": response.data.Icon,
                            "Version": response.data.Version,
                            "WelcomeText": response.data.WelcomeText
                        };

                        Templates.cloneTemplate(copy).then(
                            function (response) {
                                // Return data to be displayed if result is true
                                if (response.result) {
                                    var newParams = {
                                        id: response.data
                                    };
                                    // Get new template and add to grid
                                    Templates.readTemplate(newParams).then(
                                        function (response) {
                                            if (response.result) {
                                                var newTemplate = response.data;
                                                formatTemplate(newTemplate);
                                                $scope.templates.push(newTemplate);
                                                $scope.results = $rootScope.t("TemplateCloned");
                                            }
                                        }
                                    );
                                } else {
                                    $scope.results = response.data;
                                }
                            }
                        );
                    } else {
                        $scope.results = $rootScope.t("CouldNotCloneTemplate");
                        $scope.tryAgain = true;
                        $scope.stage = 2;
                    }
                } else {
                    $scope.results = $rootScope.t("CouldNotCopyTemplate");
                }
            }
        );
    };

    $scope.goToTemplate = function (Id, version) {
        $state.go($scope.routes.single, { templateId: Id, version: version });
    };

    // Asks the user to confirm that they would like to publish a template.
    $scope.publishTemplate = function (template) {
        if (template.activeState == "Draft") {
            var confirmMessage = $rootScope.t("AreYouSureToPublishTemplateDraft", { TemplateDraftName: template.Draft.Name });
            OneConfirm("", confirmMessage).then(
                function (result) {
                    if (result) {
                        Templates.publishTemplate(template.Id).then(
                            function (response) {
                                if (response.result) {
                                    fadeInfoSection(template, function () {
                                        template.Published = template.Draft;
                                        template.Draft = null;
                                        template.activeState = "Published";
                                        template.activeVersion = template.Published.Version;
                                    });
                                }
                            }
                        );
                    }
                }
            );
        }
    };

    // Asks the user to confirm that they would like to archive a template.
    $scope.archiveTemplate = function (template) {
        var templateName = template[template.activeState].Name;
        if (template) {
            var confirmMessage = $rootScope.t("AreYouSureToArchiveTemplateName", { TemplateName: templateName });

            OneConfirm("", confirmMessage).then(
                function (result) {
                    if (result) {
                        template.Ready = false;
                        var params = {
                            id: template.Id
                        };

                        Templates.archiveTemplate(params).then(
                            function (response) {
                                if (response.result) {
                                    var index = _.findIndex($scope.templates, ["Id", template.Id]);
                                    if (index !== false && index > -1) {
                                        $scope.templates.splice(index, 1);
                                    }
                                }
                                else {
                                    template.Ready = true;
                                }
                            }
                        );
                    }
                }
            );
        }
    };

    // Asks the user to confirm that they would like to delete a template.
    $scope.deleteTemplate = function (template, version) {
        var params = {
            id: template.Id,
            version: version
        };

        var confirmMessage = $rootScope.t("SureToDiscardQuestionnaireTemplateDraft", { TemplateDraftName: template.Draft.Name });
        OneConfirm("", confirmMessage).then(
            function (result) {
                if (!result) return;
                template.Ready = false;
                Templates.deleteTemplate(params).then(function(response) {
                    if (!response.result) return;
                    if (template.Published) {
                        fadeInfoSection(template, function() {
                            template.activeState = "Published";
                            template.activeVersion = template.Published.Version;
                            template.Draft = null;
                            template.Ready = true;
                        });
                    } else {
                        var index = _.findIndex($scope.templates, ["Id", params.id]);
                        $scope.templates.splice(index, 1);
                    }
                });
            }
        );
    };

    $scope.archive = function () {
        $state.go($scope.routes.archive);
    };

    $scope.createTemplateAddClass = function () {
        if ($scope.createTemplateClass === "") {
            $scope.createTemplateClass = "create-item--animate";
        } else {
            $scope.createTemplateClass = "";
        }
    };

    $scope.toggleTemplateState = function (template, state) {
        fadeInfoSection(template, function () {
            template.activeState = state;
        });
        if (state === "Published") {
            template.activeVersion = template.Published.Version;
        } else if (state === "Draft") {
            template.activeVersion = template.Draft.Version;
        }
    };

    var fadeInfoSection = function (template, fn) {
        template.showInfo = false;
        $timeout(fn, 200);
        $timeout(function () {
            template.showInfo = true;
        }, 400);
    };


    $scope.setDropdownDirection = function (template) {
        var heightWindow = window.innerHeight;
        var templateCard = document.getElementById(template.Id);
        var scrollContainer = document.getElementById("template-container");
        var heightScroll = scrollContainer.scrollTop;
        var currentHeight = templateCard.offsetTop;
        var temp = currentHeight - heightScroll;
        var res = temp / heightWindow;
        if (heightWindow >= 850) {
            if (res > 0.75) {
                template.hasInverted = true;
            } else {
                template.hasInverted = false;
            }
        } else if (heightWindow >= 500) {
            if (res > 0.5) {
                template.hasInverted = true;
            } else {
                template.hasInverted = false;
            }
        } else {
            template.hasInverted = false;
        }
    };

    $scope.init();

}

TemplatesCtrl.$inject = ["$rootScope", "$scope", "$state", "$uibModal", "OnboardingService", "Content", "Users", "Templates", "ModalService", "$timeout", "ENUMS", "currentUser", "DataMapping", "Permissions"];
