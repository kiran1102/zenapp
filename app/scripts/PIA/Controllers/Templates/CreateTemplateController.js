import { AssessmentTypes } from "constants/assessment.constants";

export default function CreateTemplateCtrl($scope, $state, $uibModalInstance, Icons, Templates, $rootScope, ENUMS) {
    $scope.templateType = $state.includes(AssessmentTypes.DM.BaseRoute) ? ENUMS.TemplateTypes.Datamapping : ENUMS.TemplateTypes.Custom;

    $scope.routes = {
        single: ($scope.templateType === ENUMS.TemplateTypes.Datamapping) ? "zen.app.pia.module.datamap.views.questionnaire" : "zen.app.pia.module.templates.studio.manage",
    };

    $scope.createType = "new";
    $scope.tryAgain = false;
    $scope.stage = 0; // Entering information

    $scope.selectItem = function (Id) {
        if ($scope.SelectedId === Id) {
            $scope.SelectedId = "";
        } else {
            $scope.SelectedId = Id;
        }
    };

    $scope.newTemplateTab = function () {
        $scope.createType = "new";
        $scope.SelectedId = "";
    };

    $scope.cloneTemplateTab = function () {
        $scope.createType = "clone";
        $scope.SelectedId = "";
    };

    $scope.topIconsList = Icons.topList();

    Templates.publishedList().then(function (response) {
        if (response.result) {
            $scope.publishedTemplates = response.data;
        } else {
            $scope.publishedTemplates = [];
        }
    });

    $scope.newTemplate = {
        "Name": "",
        "Description": "",
        "Icon": $scope.topIconsList[0], // Default Icon is top Icon
        "WelcomeText": "",
        "AddWelcomeText": false
    };

    // Submit request to Create a template
    $scope.createTemplate = function () {
        $scope.stage = 1; // Processing

        if (!$scope.newTemplate.AddWelcomeText) {
            $scope.newTemplate.AddWelcomeText = "";
        }

        var template = {
            "Name": $scope.newTemplate.Name,
            "Description": $scope.newTemplate.Description,
            "Icon": $scope.newTemplate.Icon,
            "WelcomeText": $scope.newTemplate.WelcomeText
        };

        Templates.createTemplate(template).then(function (response) {
            $scope.type = response.type;

            // Return data to be displayed if result is true
            if (response.result) {
                var params = {
                    id: response.data,
                    version: 1
                };
                Templates.readTemplate(params).then(function (res) {
                    if (res.result) {
                        $uibModalInstance.close(res);
                        $scope.goToTemplate(res.data.Id, res.data.Version);
                    } else {
                        $scope.results = $rootScope.t("CouldNotReadCreatedTemplate");
                    }
                });
                $scope.results = $rootScope.t("TemplateCreated");
            } else {
                $scope.results = response.data;
                $scope.stage = 0;
                $scope.newTemplate = {
                    "Name": "",
                    "Description": "",
                    "Icon": $scope.topIconsList[0], // Default Icon is top Icon
                    "WelcomeText": "",
                    "AddWelcomeText": false
                };
            }
        });
    };

    // Submit request to Create a template
    $scope.cloneTemplate = function () {
        $scope.stage = 1; // Processing
        var clone = {};
        for (var i = 0; i < $scope.publishedTemplates.length; i++) {
            if ($scope.publishedTemplates[i].Id === $scope.SelectedId) {
                clone = $scope.publishedTemplates[i];
                break;
            }
        }
        if (_.isObject(clone)) {
            var template = {
                Id: clone.Id,
                Name: clone.Name,
                Description: clone.Description,
                Icon: clone.Icon,
                Version: clone.Version,
                WelcomeText: clone.WelcomeText
            };

            Templates.cloneTemplate(template).then(function (response) {
                $scope.type = response.type;

                // Return data to be displayed if result is true
                if (response.result) {
                    Templates.readTemplate(response.data).then(function (res) {
                        if (res.result) {
                            $uibModalInstance.close(res);
                        } else {
                            $scope.results = $rootScope.t("CouldNotReadClonedTemplate");
                        }
                    });
                    $scope.results = $rootScope.t("TemplateCloned");
                }
                else {
                    $scope.results = response.data;
                }
            });
        } else {
            $scope.results = $rootScope.t("CouldNotCloneTemplate");
            $scope.tryAgain = true;
            $scope.stage = 2;
        }
    };

    $scope.done = function (done) {
        var result = done ? done : { result: false, data: false };
        $uibModalInstance.close(result);
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss("cancel");
    };

    // Opens the Edit Template Modal
    $scope.goToTemplate = function (Id, Version) {
        $state.go($scope.routes.single, { templateId: Id, version: Version });
    };
}

CreateTemplateCtrl.$inject = ["$scope", "$state", "$uibModalInstance", "Icons", "Templates", "$rootScope", "ENUMS"];
