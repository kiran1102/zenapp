import { debounce } from "lodash";
import { RiskLevels } from "enums/risk.enum";

export default function ReadinessAssessmentCtrl($scope, ENUMS, ReadinessAssessment, $state, Answer, Projects, $q, ModalService) {

    $scope.isReadinessTemplate = true;
    $scope.Ready = false;
    $scope.riskLevels = RiskLevels;
    $scope.responseTypes = ENUMS.ResponseTypes;
    $scope.questionTypes = ENUMS.QuestionTypes;
    $scope.selectedSectionIndex = 0;

    $scope.Dictionary = {
        Sections: {},
        Questions: {}
    };

    var init = function () {
        var assessmentPromises = [
            getProject()
        ];
        $q.all(assessmentPromises)
            .then(function () {
                $scope.Ready = true;
                $scope.updateProjectStates($scope.selectedSectionIndex);
            });
        $scope.questionEvents = {
            textAreaChange: setResponseAndSave,
            textAreaSave: answerQuestion,
            yesNoChange: setResponseAndSave,
            multiChoiceSelected: isSelected,
            multiChoiceChange: multiChoiceToggle,
            multiSelectChange: multiSelectToggle,
            dateChange: setResponseAndSave,
            toggleNotSure: toggleNotSure,
            toggleNotApplicable: toggleNotApplicable,
            otherOptionSet: setOtherOption,
            otherOptionSave: answerQuestion,
            justificationSave: saveJustification
        };
    };

    var getProject = function () {
        return Projects.read($state.params.Id, 1).then(
            function (response) {
                if (response.result) {
                    $scope.Assessment = response.data;
                    Answer.getSections($scope, $scope.Assessment.Sections, $scope.Dictionary);
                }
            }
        );
    };

    $scope.changeSection = function (section) {
        $scope.currentlySelectedSection = section;
        $scope.selectedSectionIndex = _.findIndex($scope.Assessment.Sections, ["Id", section.Id]);
        $scope.goToTopOfSection();
    };

    $scope.goToReport = function () {
        $state.go("zen.app.pia.module.readiness.report", {
            "Id": $scope.Assessment.Id
        });
    };

    $scope.goToDashboard = function () {
        $state.go("zen.app.pia.module.readiness.dashboard");
    };

    var answerQuestion = debounce(function (id, question) {
        var answer = Answer.createResponse($scope.Assessment.Id, question);
        answer.promise.then(function (response) {
            if (response.result) {
                question.Responses = answer.response.Responses;
                Answer.handleQuestionResponse($scope, $scope.Dictionary, question, response.data);
                $scope.updateProjectStates($scope.selectedSectionIndex);
            }
        });
    }, 650);

    var setResponseAndSave = function (question) {
        question.ResponseType = question.DefaultResponseType;
        answerQuestion(null, question);
    };

    var isSelected = function (sectionId, questionId, oIndex, subIndex) {
        var sectionIndex = _.findIndex($scope.Assessment.Sections, ["Id", sectionId]);
        var boolean = false;
        if (sectionIndex >= 0) {
            var questionIndex = _.findIndex($scope.Assessment.Sections[sectionIndex].Questions, ["Id", questionId]);
            if (questionIndex >= 0) {
                if (oIndex >= 0) {
                    var answer = $scope.Assessment.Sections[sectionIndex].Questions[questionIndex].Answer;
                    if (_.isArray(answer)) {
                        if (subIndex) {
                            var topQuestionValue = $scope.Assessment.Sections[sectionIndex].Questions[questionIndex].Options[oIndex].Value.toString();
                            var parsedAnswers = [];
                            _.each(answer, function(Answer) {
                                var subQuestion = Answer.search("/");
                                var valuePair = Answer.split("/");
                                if (subQuestion > 0) {
                                    if ((valuePair[0] == topQuestionValue) && (valuePair[1] == subIndex)) {
                                        boolean = true;
                                    }
                                }
                            });
                        } else {
                            return answer.indexOf($scope.Assessment.Sections[sectionIndex].Questions[questionIndex].Options[oIndex].Value.toString()) >= 0;
                        }
                    } else if (_.isString(answer)) {
                        return (answer === $scope.Assessment.Sections[sectionIndex].Questions[questionIndex].Options[oIndex].Value.toString());
                    }
                    return boolean;
                }
            }
        }
        return boolean;
    };

    var multiChoiceToggle = function (sectionId, questionId, value) {
        var sectionIndex = _.findIndex($scope.Assessment.Sections, ["Id", sectionId]);
        if (sectionIndex >= 0) {
            var questionIndex = _.findIndex($scope.Assessment.Sections[sectionIndex].Questions, ["Id", questionId]);
            if (questionIndex >= 0) {
                var question = $scope.Assessment.Sections[sectionIndex].Questions[questionIndex];
                var answer = question.Answer;
                if (answer === value.toString()) {
                    question.Answer = "";
                } else {
                    question.Answer = value.toString();
                }

                // Remove the other answer if there is one.
                if (!_.isUndefined(question.AllowOther) &&
                    question.AllowOther) {
                    question.OtherAnswer = null;
                }
                setResponseAndSave(question);
            }
        }
    };

    var multiSelectToggle = function (sectionId, questionId, value, subValue) {
        var sectionIndex = _.findIndex($scope.Assessment.Sections, ["Id", sectionId]);
        var stringCompare = "";
        if (_.isUndefined(subValue)) {
            stringCompare = value.toString();
        } else {
            stringCompare = value.toString() + "/" + subValue.toString();
        }

        if (sectionIndex >= 0) {
            var questionIndex = _.findIndex($scope.Assessment.Sections[sectionIndex].Questions, ["Id", questionId]);
            if (questionIndex >= 0) {
                var question = $scope.Assessment.Sections[sectionIndex].Questions[questionIndex];
                // var answer = question.Answer;
                if (_.isArray($scope.Assessment.Sections[sectionIndex].Questions[questionIndex].Answer) && $scope.Assessment.Sections[sectionIndex].Questions[questionIndex].Answer.length > 0) {
                    var valueIndex = $scope.Assessment.Sections[sectionIndex].Questions[questionIndex].Answer.indexOf(stringCompare);
                    if (valueIndex < 0) {
                        $scope.Assessment.Sections[sectionIndex].Questions[questionIndex].Answer.push(stringCompare);
                    } else {
                        var orphanAssetCheck = $scope.Assessment.Sections[sectionIndex].Questions[questionIndex].Answer.indexOf(value.toString());
                        if (orphanAssetCheck < 0) {
                            $scope.Assessment.Sections[sectionIndex].Questions[questionIndex].Answer.push(value.toString());
                        }
                        $scope.Assessment.Sections[sectionIndex].Questions[questionIndex].Answer.splice(valueIndex, 1);
                    }
                } else {
                    $scope.Assessment.Sections[sectionIndex].Questions[questionIndex].Answer = [stringCompare];
                }
                setResponseAndSave(question);
            }
        }
    };

    var toggleNotSure = function (sectionId, questionId) {
        var sectionIndex = _.findIndex($scope.Assessment.Sections, ["Id", sectionId]);
        if (sectionIndex >= 0) {
            var questionIndex = _.findIndex($scope.Assessment.Sections[sectionIndex].Questions, ["Id", questionId]);
            if (questionIndex >= 0) {
                var question = $scope.Assessment.Sections[sectionIndex].Questions[questionIndex];
                if (question.ResponseType === $scope.responseTypes.NotSure) {
                    question.ResponseType = question.DefaultResponseType;
                    question.Justification = null;
                    answerQuestion($scope.Assessment.Id, question);
                } else {
                    question.ResponseType = $scope.responseTypes.NotSure;
                    if (!_.isUndefined(question.AllowOther)) {
                        question.OtherAnswer = null;
                    }
                    question.Answer = null;
                    question.Justification = null;
                    answerQuestion($scope.Assessment.Id, question);
                }
            }
        }
    };

    var toggleNotApplicable = function (question) {
        if (question.ResponseType === $scope.responseTypes.NotApplicable) {
            question.ResponseType = question.DefaultResponseType;
            question.NotApplicable = false;
            answerQuestion($scope.Assessment.Id, question);
        } else {
            question.ResponseType = $scope.responseTypes.NotApplicable;
            if (!_.isUndefined(question.AllowOther)) {
                question.OtherAnswer = null;
            }
            question.Answer = null;
            question.NotApplicable = true;
            answerQuestion($scope.Assessment.Id, question);
        }
    };

    var setOtherOption = function (sectionId, questionId) {
        var sectionIndex = _.findIndex($scope.Assessment.Sections, ["Id", sectionId]);
        if (sectionIndex >= 0) {
            var questionIndex = _.findIndex($scope.Assessment.Sections[sectionIndex].Questions, ["Id", questionId]);
            if (questionIndex >= 0) {
                $scope.Assessment.Sections[sectionIndex].Questions[questionIndex].ResponseType = $scope.responseTypes.Other;
                if (!$scope.Assessment.Sections[sectionIndex].Questions[questionIndex].MultiSelect) {
                    $scope.Assessment.Sections[sectionIndex].Questions[questionIndex].Answer = null;
                }
            }
        }
    };

    var saveJustification = function (question) {
        answerQuestion($scope.Assessment.Id, question);
    };

    $scope.nextSection = function (index) {
        var nextSectionIndex = index ? index : $scope.selectedSectionIndex + 1;
        var section = $scope.Assessment.Sections[nextSectionIndex];
        if (section && !section.IsSkipped) {
            $scope.selectedSectionIndex = nextSectionIndex;
            $scope.currentlySelectedSection = $scope.Assessment.Sections[nextSectionIndex];
            $scope.goToTopOfSection();
        } else if (section) {
            $scope.nextSection(nextSectionIndex + 1);
        }
    };

    $scope.previousSection = function (index) {
        var previousSectionIndex = index ? index : $scope.selectedSectionIndex - 1;
        var section = $scope.Assessment.Sections[previousSectionIndex];
        if (section && !section.IsSkipped) {
            $scope.selectedSectionIndex = previousSectionIndex;
            $scope.currentlySelectedSection = $scope.Assessment.Sections[previousSectionIndex];
            $scope.goToTopOfSection();
        } else if (section) {
            $scope.previousSection(previousSectionIndex - 1);
        }
    };

    $scope.nextSectionDisabled = function (index) {
        var nextIndex = index ? index : $scope.selectedSectionIndex + 1;
        var nextSection = $scope.Assessment.Sections[nextIndex];
        if (nextSection && !nextSection.IsSkipped) {
            return false;
        } else if (nextSection) {
            $scope.nextSectionDisabled(nextIndex + 1);
        } else {
            return true;
        }
    };

    $scope.previousSectionDisabled = function (index) {
        var previousIndex = index ? index : $scope.selectedSectionIndex - 1;
        var previousSection = $scope.Assessment.Sections[previousIndex];
        if (previousSection && !previousSection.IsSkipped) {
            return false;
        } else if (previousSection) {
            $scope.previousSectionDisabled(previousIndex - 1);
        } else {
            return true;
        }
    };

    $scope.updateProjectStates = function (currentIndex) {
        var lastSectionIndex;
        var sectionNumber = 0;
        _.each($scope.Assessment.Sections, function(section, index) {
            var questionCount = 0;
            var answeredCount = 0;
            _.each(section.Questions, function(question) {
                if (question.IsSkipped && answeredCount === 0) {
                    questionCount++;
                }
                if (!question.IsSkipped) {
                    questionCount++;
                    question.Number = questionCount;
                    if (question.Responses && question.Responses.length) {
                        answeredCount++;
                    }
                } else {
                    clearAnswers(question);
                }
            });
            if (!section.IsSkipped) {
                section.Number = sectionNumber;
                sectionNumber++;
            }
            section.QuestionCount = questionCount;
            section.AnsweredCount = answeredCount;
        });
        $scope.lastSectionIndex = lastSectionIndex;
        $scope.currentlySelectedSection = $scope.Assessment.Sections[currentIndex];
    };

    var clearAnswers = function (question) {
        question.OtherAnswer = null;
        question.Answer = null;
        question.Justification = null;
        question.Responses = null;
        question.NotApplicable = null;
        question.ResponseType = null;
    };

    $scope.OpenAttachmentModal = function (question) {
        var data = {
            Project: $scope.Assessment,
            Question: question,
            ReadOnly: false
        };
        var modalInstance = ModalService.renderModal(data, "AttachmentController", "AttachmentModal.html", "attachmentModal", "lg");

        var destroyAttachmentModalCb = $scope.$root.$on("attachmentModalCb", function (event, args) {
            question.HasAttachments = Boolean(args.response.result > 0);
            destroyAttachmentModalCb();
        });
    };

    $scope.goToTopOfSection = function() {
        const el = document.getElementById("readiness-section-scroller");
        if (el) el.scrollTop = 0;
    }

    init();

}

ReadinessAssessmentCtrl.$inject = ["$scope", "ENUMS", "ReadinessAssessment", "$state", "Answer", "Projects", "$q", "ModalService"];
