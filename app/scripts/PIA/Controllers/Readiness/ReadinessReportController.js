import { QuestionStates } from "enums/assessment-question.enum";
import { RiskLevels } from "enums/risk.enum";

export default function ReadinessReportCtrl($scope, ENUMS, ReadinessAssessment, $state, Projects, Export, ModalService, Answer) {

    $scope.Ready = false;
    $scope.loadingPDF = false;
    $scope.riskLevels = RiskLevels;
    $scope.questionStates = QuestionStates;
    $scope.QuestionTypes = ENUMS.QuestionTypes;
    $scope.ResponseTypes = ENUMS.ResponseTypes;

    var init = function () {
        getProject($state.params.Id)
            .then(function (response) {
                if (response.result) {
                    $scope.Assessment = response.data;
                    formatAssessment();
                }
                $scope.Ready = true;
            });
    };

    var getProject = function (Id) {
        return Projects.read(Id, 1);
    };

    var formatAssessment = function () {
        var sectionCount = 0;
        _.each($scope.Assessment.Sections, function(section) {
            section.Risk = { Level: 0 };
            section.UnansweredCount = 0;
            section.NoRiskCount = 0;
            section.LowRiskCount = 0;
            section.MediumRiskCount = 0;
            section.HighRiskCount = 0;
            section.VeryHighRiskCount = 0;
            section.IsSkipped = true;
            section.unskippedQuestionsCount = 0;
            _.each(section.Questions, function(question) {
                // Get Skipped state
                if (question.State === $scope.questionStates.Skipped) {
                    question.IsSkipped = true;
                } else {
                    section.unskippedQuestionsCount++;
                    question.Number = section.unskippedQuestionsCount;
                    section.IsSkipped = false;

                    if (question.Risk) {
                        // Set section risk level
                        if (question.Risk.Level > section.Risk.Level) {
                            section.Risk.Level = question.Risk.Level;
                        }
                        // Count different risks for section
                        switch (question.Risk.Level) {
                            case $scope.riskLevels.Low:
                                section.LowRiskCount++;
                                break;
                            case $scope.riskLevels.Medium:
                                section.MediumRiskCount++;
                                break;
                            case $scope.riskLevels.High:
                                section.HighRiskCount++;
                                break;
                            case $scope.riskLevels.VeryHigh:
                                section.VeryHighRiskCount++;
                                break;
                        }
                    } else if (question.Responses && question.Responses.length) {
                        // Set section no risk count
                        section.NoRiskCount++;
                    } else {
                        section.UnansweredCount++;
                    }
                }
                applyRiskStyle(question);
                getAnswers(question);
            });
            if (!section.IsSkipped) {
                section.Number = sectionCount;
                sectionCount++;
            }
            setSectionReadiness(section);
            applyRiskStyle(section);
        });
    };

    var applyRiskStyle = function (item) {
        var risk = item.Risk ? item.Risk.Level : null;
        switch (risk) {
            case $scope.riskLevels.Low:
                item.riskClass = "fa fa-flag low";
                item.riskWord = "Low";
                break;
            case $scope.riskLevels.Medium:
                item.riskClass = "fa fa-flag medium";
                item.riskWord = "Medium";
                break;
            case $scope.riskLevels.High:
                item.riskClass = "fa fa-flag high";
                item.riskWord = "High";
                break;
            case $scope.riskLevels.VeryHigh:
                item.riskClass = "fa fa-flag very-high";
                item.riskWord = "Very High";
                break;
            default:
                item.riskClass = "";
                item.riskWord = null;
        }
    };

    var getAnswers = function (question) {
        question.AnswerString = Answer.getAnswerString($scope, question);
    };

    var setSectionReadiness = function (section) {
        var totalRiskCount = section.LowRiskCount + section.MediumRiskCount + section.HighRiskCount + section.VeryHighRiskCount;
        var getPercentage = function (count) { return Math.floor((count / section.unskippedQuestionsCount) * 100); };
        section.UnansweredPercentage = getPercentage(section.UnansweredCount);
        section.LowRiskPercentage = getPercentage(section.LowRiskCount);
        section.MediumRiskPercentage = getPercentage(section.MediumRiskCount);
        section.HighRiskPercentage = getPercentage(section.HighRiskCount);
        section.VeryHighRiskPercentage = getPercentage(section.VeryHighRiskCount);
        section.TotalRiskPercentage = getPercentage(totalRiskCount);
        section.NoRiskPercentage = 100 - (section.UnansweredPercentage + section.LowRiskPercentage + section.MediumRiskPercentage + section.HighRiskPercentage + section.VeryHighRiskPercentage);
    };

    $scope.toggleSection = function (section) {
        section.isActive = !section.isActive;
    };

    $scope.goToAssessment = function () {
        $state.go("zen.app.pia.module.readiness.assessment", {
            "Id": $scope.Assessment.Id
        });
    };

    $scope.goToDashboard = function () {
        $state.go("zen.app.pia.module.readiness.dashboard");
    };

    $scope.exportPDF = function () {
        $scope.loadingPDF = true;
        Export.pdfProjectExport($scope.Assessment.Id, 1, $scope.Assessment.Name).then(
            function () {
                $scope.loadingPDF = false;
            }
        );
    };

    $scope.OpenAttachmentModal = function (question) {
        var data = {
            Project: $scope.Assessment,
            Question: question,
            ReadOnly: false
        };
        var modalInstance = ModalService.renderModal(data, "AttachmentController", "AttachmentModal.html", "attachmentModal", "lg");

        var destroyAttachmentModalCb = $scope.$root.$on("attachmentModalCb", function (event, args) {
            question.HasAttachments = Boolean(args.response.result > 0);
            destroyAttachmentModalCb();
        });
    };

    init();

}

ReadinessReportCtrl.$inject = ["$scope", "ENUMS", "ReadinessAssessment", "$state", "Projects", "Export", "ModalService", "Answer"];
