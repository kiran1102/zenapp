import { RiskLevels } from "enums/risk.enum";

export default function ReadinessDashboardCtrl($rootScope, $scope, ENUMS, $state, OnboardingService, ReadinessAssessment, Export, currentUser, Content, $q, Projects) {
	$scope.translate = $rootScope.t;
	$scope.Ready = false;
	$scope.CreatingAssessment = false;
	var riskNames = ENUMS.RiskPrettyNames;
	var riskLabels = ENUMS.RiskLabels;
	$scope.spacers = [0, 1, 2, 3, 4, 5, 6];
	$scope.currentUser = currentUser;

	$scope.graphKey = [{
		class: riskLabels[RiskLevels.None],
		text: $scope.translate("Ready"),
	}, {
		class: riskLabels[RiskLevels.Medium],
		text: $scope.translate("GapIdentified"),
	}, {
		class: "unanswered",
		text: $scope.translate("Unanswered"),
	}];

	var init = function () {
		var readinessPromises = [
			getList()
		];
		$q.all(readinessPromises).then(function () {
			OnboardingService.onboard(
				Content.GetKey("OnBoardingReadinessTitle") || "Welcome to Readiness Assessments",
				Content.GetKey("readinessOnboardModalContent") || "<p><b>Readiness Assessments consist of a set of pre-built templates for organizations to assess their readiness in order to meet the requirements of the GDPR, Privacy Shield, and Binding Corporate Rules for Processors and Controllers (BCR).</b></p><p>In this view, users are able to:</p><ul><li>Access readiness assessments composed by a team of legal experts with a deep knowledge of GDPR, Privacy Shield, and BCR compliance</li><li>Complete readiness assessments through an easy-to-use workflow</li><li>Review responses in a gap analysis report with automatically flagged gaps and recommendations</li><li>Export PDFs for each readiness assessment</li></ul><p></p>"
			);
			$scope.Ready = true;
		});
	};

	var getList = function () {
		return ReadinessAssessment.list()
			.then(function (response) {
				if (response.result) {
					if (response.data.length) {
                        $scope.assessmentData = response.data;
                        _.each($scope.assessmentData, function(item) {
                            item.notStarted = item.IsTemplate || item.RemainingPercentage === 1;
                            setGraphData(item);
                        });
					}
				}
			});
	};

	function buildCompletedRatio(ratio) {
		return ratio + " " + $rootScope.t("Answered");
	}

	var setGraphData = function (assessment) {
		if (!assessment.IsTemplate) {
			assessment.graphData = [{
				label: riskNames[RiskLevels.None],
				class: riskLabels[RiskLevels.None],
				value: assessment.ReadyPercentage
			}, {
				label: riskNames[RiskLevels.Medium],
				class: riskLabels[RiskLevels.Medium],
				value: assessment.GapPercentage
			}, {
				label: "",
				class: "remaining",
				value: assessment.RemainingPercentage
			}];
			assessment.readyPercentage = Math.floor(assessment.ReadyPercentage * 100);
			assessment.completedRatio = assessment.CompletedRatio ? buildCompletedRatio(assessment.CompletedRatio) : null;
		} else {
			assessment.graphData = [{
				label: "",
				class: "remaining",
				value: 1
			}];
			assessment.readyPercentage = 0;
		}
	};

	$scope.goToAssessment = function (assessment, id) {
		if (assessment.IsTemplate) {
			createAssessment(assessment);
		} else {
			$state.go("zen.app.pia.module.readiness.assessment", {
				"Id": assessment.ProjectId
			});
		}
	};

    var createAssessment = function (assessment) {
        $scope.CreatingAssessment = true;
        var assessmentData = {
            Description: assessment.Name,
            LeadId: assessment.LeadId,
            Name: assessment.Name,
            OrgGroupId: assessment.OrgGroupId,
            TemplateId: assessment.TemplateId,
            TemplateVersion: assessment.TemplateVersion,
        };
        Projects.create(assessmentData).then(function (response) {
            if (response.result) {
                assessment.IsTemplate = false;
                assessment.ProjectId = response.data;
                $scope.goToAssessment(assessment);
            }
        });
    };

	$scope.goToReport = function (Id) {
		$state.go("zen.app.pia.module.readiness.report", {
			"Id": Id
		});
	};

	$scope.exportPDF = function (assessment) {
		assessment.loadingPDF = true;
		Export.pdfProjectExport(assessment.ProjectId, 1, assessment.Name).then(
			function () {
				assessment.loadingPDF = false;
			}
		);
	};

	init();
}

ReadinessDashboardCtrl.$inject = ["$rootScope", "$scope", "ENUMS", "$state", "OnboardingService", "ReadinessAssessment", "Export", "currentUser", "Content", "$q", "Projects"];
