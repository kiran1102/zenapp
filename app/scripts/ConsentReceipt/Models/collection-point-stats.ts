import { TransactionSummary } from "crmodel/dashboard-data";

export class CollectionPointStats {
    TransactionCount: number;
    ReceiptCount: number;
    Purposes: CollectionPointPurposeStats[];
    Transactions: TransactionSummary;
}

export class CollectionPointPurposeStats {
    Label: string;
    TransactionCount: number;
    NoConsentTransactionCount: number;
}
