﻿import angular from "angular";

/*Controllers*/
import ChangePasswordController from "./Controllers/change-password.controller";
import ConfirmAccountController from "./Controllers/ConfirmAccountController";
import CreatePasswordController from "./Controllers/CreatePasswordController";
import ForgotPasswordController from "./Controllers/ForgotPasswordController";
import LoginController from "./Controllers/LoginController";

/* Services */
import Authorization from "./Services/AuthorizeService";
import { OrgGroupApiService } from "oneServices/api/org-group-api.service.ts";
import Tenants from "./Services/TenantService";
import Users from "./Services/UsersService";

angular.module("zen.identity", [
    // Angular modules
    "ngCookies",
    "ui.router",

    // 3rd Party Modules

])
    .controller("ChangePasswordController", ChangePasswordController)
    .controller("ConfirmAccountController", ConfirmAccountController)
    .controller("CreatePasswordController", CreatePasswordController)
    .controller("ForgotPasswordController", ForgotPasswordController)
    .controller("LoginController", LoginController)
    .factory("Authorization", Authorization)
    .service("OrgGroups", OrgGroupApiService)
    .factory("Tenants", Tenants)
    .factory("Users", Users);
