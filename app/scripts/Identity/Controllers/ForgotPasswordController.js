
export default function ForgotPasswordController(Accounts, $scope, $state, Marketing, $sce, Branding) {
    $scope.fade = !$scope.fade;
    $scope.app = Branding.getBrandingFromStorage();
    $scope.loading = false;
    $scope.Ready = false;
    $scope.emailSentSuccess = false;

    $scope.forgotData = {
        "Email": sessionStorage.getItem("userEmail") || ""
    };

    $scope.errors = {
        Email: false
    };

    $scope.marketingContent = {};

    $scope.init = function () {
        Marketing.getMarketing()
            .then(function (response) {
                if (response.result) {
                    $scope.marketingContent = response.data;
                    $scope.marketingContent.headline = $sce.trustAsHtml($scope.marketingContent.headline);
                    $scope.marketingContent.copy = $sce.trustAsHtml($scope.marketingContent.copy);
                    $scope.marketingContent.backgroundImage = $scope.marketingContent.backgroundImage || "/images/default-green.jpg";
                    checkForMarketingImage();
                }
            });
    };

    var validateEmail = function (form) {
        if (form.Email.$invalid) {
            $scope.errors.Email = true;
            $scope.loading = false;
        } else {
            $scope.errors.Email = false;
        }
    };


    $scope.forgotPassword = function (form) {
        $scope.loading = true;

        validateEmail(form);
        if (!$scope.errors.Email) {
            var data = {
                Email: $scope.forgotData.Email
            };

            Accounts.forgotPassword(data)
                .then(function (response) {
                    if (response.result) {
                        sessionStorage.removeItem("userEmail");
                    }
					$scope.emailSentSuccess = true;
					$scope.loading = false;
                });
        }
    };

    var checkForMarketingImage = function () {
        if ($scope.marketingContent.backgroundImage) {
            Marketing.checkImage($scope.marketingContent.backgroundImage)
                .then(function (isImage) {
                    if (!isImage) {
                        $scope.marketingContent = Marketing.getMarketingDefaults();
                    }
                    $scope.Ready = true;
                });
        } else {
            $scope.Ready = true;
        }
    };

    $scope.init();

}

ForgotPasswordController.$inject = ["Accounts", "$scope", "$state", "Marketing", "$sce", "Branding"];
