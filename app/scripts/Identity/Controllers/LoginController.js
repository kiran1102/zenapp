export default function LoginController(Accounts, $scope, $state, $localStorage, $rootScope, authService, Branding, local, Marketing, $sce, TenantTranslationService, $stateParams, Session) {
    $scope.local = local;
    $scope.app = Branding.getBrandingFromStorage();
    $scope.basicChecked = false;
    $scope.isBasic = false;
    $scope.marketingContent = {};
    $scope.Ready = false;
    $scope.invalidPassword = false;
    $scope.errorMessage = Session.getAuthError() || "";
    Session.removeAuthError();

    $scope.languages = $rootScope.languages || [{ Name: "English", Code: "en-us" }];
    $scope.languageSelected = JSON.parse(localStorage.getItem("OneTrust.languageSelected")) || "en-us";

    $scope.changeLanguage = function (languageSelected) {
        localStorage.setItem("OneTrust.languageSelected", JSON.stringify(languageSelected));
        $state.go($state.current, {}, { reload: true });
    };

    var lastLogin = $localStorage.LastLogin || "";

    $scope.init = function () {
        Marketing.getMarketing().then(function(response) {
            if (response.result) {
                $scope.marketingContent = response.data;
                $scope.marketingContent.headline = $sce.trustAsHtml($scope.marketingContent.headline);
                $scope.marketingContent.copy = $sce.trustAsHtml($scope.marketingContent.copy);
                $scope.marketingContent.backgroundImage = $scope.marketingContent.backgroundImage || "/images/default-green.jpg";
                checkForMarketingImage();
            } else {
                $scope.Ready = true;
            }
        });
    };

    $scope.disable = false;

    $scope.loginData = {
        "Email": lastLogin || "",
        "Password": "",
        "RememberMe": false
    };

    $scope.errors = {
        Email: false,
        Password: false
    };

    $scope.$on('event:auth-expiredPassword', function (event, args) {
        $state.go('zen.app.auth.changePassword', {
            'auth_code': args
        });
    });

    var validateEmail = function (form) {
        if (form.Email.$invalid) {
            $scope.errors.Email = true;
            $scope.loading = false;
        } else {
            $scope.errors.Email = false;
        }
    };

    var validatePassword = function (form) {
        if (form.Password.$invalid) {
            $scope.errors.Password = true;
            $scope.loading = false;
        } else {
            $scope.errors.Password = false;
        }
    };

    $scope.checkBasic = function (form) {
        validateEmail(form);
        if (!$scope.errors.Email) {
            $scope.isBasic = true;
            $scope.basicChecked = true;
        }
        $scope.loading = false;
    };

    $scope.login = function (form) {
        $scope.loading = true;
        $scope.invalidPassword = false;
        $scope.errorMessage = null;

        if (!$scope.basicChecked) {
            $scope.checkBasic(form);

        } else {

            validatePassword(form);
            if (!$scope.errors.Password) {
                var data = {
                    Email: $scope.loginData.Email,
                    Password: $scope.loginData.Password
                };
                Accounts.login(data)
                    .then(function (response) {
                        if (response.result) {
                            $localStorage.LastLogin = (data.Email ? data.Email : "");
                            // Redirect to where they were attempting to go before.
                            if ($rootScope.origin && $rootScope.origin.indexOf("pia") > -1 && $rootScope.origin.indexOf("landing") < 0) {
                                window.location.assign($rootScope.origin);
                                window.location.reload();
                            } else {
                                $state.go("landing",  null, { notify: true, reload: true });
                            }
                            authService.loginConfirmed();
                        } else if (response.data && !response.data.expired) {
                            $scope.invalidPassword = Boolean(response.data.invalidPassword)
                            authService.loginCancelled();
                            $scope.loading = false;
                        }
                    });
            }
        }
    };

    $scope.goBack = function () {
        $scope.basicChecked = false;
        $scope.isBasic = false;
        $scope.errors = {
            Email: false,
            Password: false
        };
    };

    $scope.helpForgotContactModal = function () {
        sessionStorage.setItem("userEmail", $scope.loginData.Email);
        $state.go("zen.app.auth.forgotPassword");
    };

    var checkForMarketingImage = function () {
        if ($scope.marketingContent.backgroundImage) {
            Marketing.checkImage($scope.marketingContent.backgroundImage)
                .then(function (isImage) {
                    if (!isImage) {
                        $scope.marketingContent = Marketing.getMarketingDefaults();
                    }
                    $scope.Ready = true;
                });
        } else {
            $scope.Ready = true;
        }
    };

    $scope.getDev = function () {
        localStorage.clear();
        location.reload(true);
    };

    $scope.init();

}

LoginController.$inject = ["Accounts", "$scope", "$state", "$localStorage", "$rootScope", "authService", "Branding", "local", "Marketing", "$sce", "TenantTranslationService", "$stateParams", "Session"];
