import Utilities from "Utilities";
import { Regex } from "constants/regex.constant";

export default function ConfirmAccountController($rootScope, $scope, $state, Accounts, ModalService, Users, Branding, Marketing, $q, BasicAuth, $sce) {

    $scope.app = Branding.getBrandingFromStorage();

    $scope.loading = false;
    $scope.Ready = false;
    $scope.initState = true;

    $scope.type1 = "password";
    $scope.type2 = "password";

    $scope.confirmData = {
        Email: "",
        FirstName: "",
        LastName: "",
        ConfirmPassword: "",
        Password: ""
    };

    $scope.errors = {
        Email: false,
        PasswordSpecial: false,
        PasswordLength: false,
        PasswordMatch: false,
        FirstName: false,
        LastName: false
    };

    $scope.marketingContent = {};

    var getMarketing = function () {
        return Marketing.getMarketing().then(function(response) {
            if (!response.result) return;
                $scope.marketingContent = response.data;
                $scope.marketingContent.headline = $sce.trustAsHtml($scope.marketingContent.headline);
                $scope.marketingContent.copy = $sce.trustAsHtml($scope.marketingContent.copy);
                $scope.marketingContent.backgroundImage = $scope.marketingContent.backgroundImage || "/images/default-green.jpg";
            });
    };

    var getInfo = function () {
        if (BasicAuth) {
            $scope.user = BasicAuth.user ? BasicAuth.user : false;
            if ($scope.user) {
                $scope.confirmData.Email = $scope.user.Email ? $scope.user.Email : "";
                $scope.confirmData.FirstName = $scope.user.FirstName ? $scope.user.FirstName : "";
                $scope.confirmData.LastName = $scope.user.LastName ? $scope.user.LastName : "";
            } else {
                $scope.user = false;
            }
        }
    };

    var validatePassword = function () {
        $scope.initState = false;
        $scope.errors.PasswordLength = !Utilities.validateLength($scope.confirmData.Password);
        $scope.errors.Uppercase = !Utilities.validateUppercase($scope.confirmData.Password);
        $scope.errors.Lowercase = !Utilities.validateLowercase($scope.confirmData.Password);
        $scope.errors.Number = !Utilities.validateNumber($scope.confirmData.Password);
        $scope.errors.PasswordMatch = !Utilities.validateMatch($scope.confirmData.Password, $scope.confirmData.ConfirmPassword);
        $scope.errors.emailEqualToPassword = !_.isUndefined($scope.confirmData.Password) && (
            $scope.confirmData.Email.toLowerCase() === $scope.confirmData.Password.toLowerCase()
        );
        $scope.errors.mustContainOneSpecialChar = !_.isUndefined($scope.confirmData.Password) && (Regex.SPECIAL_CHARACTERS.test($scope.confirmData.Password));
        return !$scope.errors.PasswordLength &&
            !$scope.errors.Uppercase &&
            !$scope.errors.Lowercase &&
            !$scope.errors.Number &&
            !$scope.errors.PasswordMatch &&
            !$scope.errors.emailEqualToPassword &&
            $scope.errors.mustContainOneSpecialChar;
    };

    $scope.checkValidation = function () {
        $scope.confirmData.Password = !_.isUndefined($scope.confirmData.Password) ? $scope.confirmData.Password.replace(/\s/g, "") : "";
        $scope.confirmData.ConfirmPassword = !_.isUndefined($scope.confirmData.ConfirmPassword) ? $scope.confirmData.ConfirmPassword.replace(/\s/g, "") : "";
        validatePassword();
    }

    var validateEmail = function (form) {
        if (!$scope.user || !$scope.user.Email) {
            if (form.Email.$invalid) {
                $scope.errors.Email = true;
                return false;
            } else {
                $scope.errors.Email = false;
                return true;
            }
        }
        return true;
    };

    var validateName = function (form) {
        if ($scope.user && $scope.user.FirstName && $scope.user.LastName) {
            return true;
        }

        $scope.errors.FirstName = form.FirstName.$invalid ? true : false;
        $scope.errors.LastName = form.LastName.$invalid ? true : false;
        return !$scope.errors.LastName && !$scope.errors.FirstName;
    };

    var validateForm = function (form) {
        var passwordValid = validatePassword();
        var emailValid = validateEmail(form);
        var nameValid = validateName(form);


        return (passwordValid && emailValid && nameValid);
    };


    $scope.confirm = function (form) {
        $scope.loading = true;
        if (validateForm(form)) {
            var data = {
                Email: $scope.confirmData.Email,
                Password: $scope.confirmData.Password,
                ConfirmPassword: $scope.confirmData.ConfirmPassword,
                FirstName: $scope.confirmData.FirstName,
                LastName: $scope.confirmData.LastName
            };

            Accounts.confirm(data).then(
                function (response) {
                    if (response.result) {
                        var loginData = {
                            Email: $scope.confirmData.Email,
                            Password: $scope.confirmData.Password
                        };

                        Accounts.login(loginData)
                            .then(function (response) {
                                if (response.result) {
                                    $state.go("landing",  null, { reload: true });
                                } else {
                                    $scope.loading = false;
                                }
                            });
                    } else {
                        $scope.loading = false;
                    }
                }
            );
        } else {
            $scope.loading = false;
        }
    };

    $scope.helpConfirmContactModal = function () {
        const modalData = {
            modalTitle: $rootScope.t("ConfirmAccount"),
            content: $rootScope.t("TroubleConfirmingYourAccount", {
                Email: "<a href='mailto:support@onetrust.com?subject=" + $rootScope.t("ConfirmAccount") + "'>support@onetrust.com</a>",
                Phone: "<a href='tel:+1 (844) 900-0472'>+1 (844) 900-0472</a>",
            }),
        };
        ModalService.setModalData(modalData);
        ModalService.openModal("helpDialogModal");
    };

    var checkForMarketingImage = function () {
        if ($scope.marketingContent.backgroundImage) {
            Marketing.checkImage($scope.marketingContent.backgroundImage)
                .then(function (isImage) {
                    if (!isImage) {
                        $scope.marketingContent = Marketing.getMarketingDefaults();
                    }
                    $scope.Ready = true;
                });
        } else {
            $scope.Ready = true;
        }
    };

    $scope.init = function () {
        var promiseArray = [
            getMarketing()
        ];

        $q.all(promiseArray).then(function() {
            getInfo();
            checkForMarketingImage();
        });
    };

    $scope.init();

}

ConfirmAccountController.$inject = ["$rootScope", "$scope", "$state", "Accounts", "ModalService", "Users", "Branding", "Marketing", "$q", "BasicAuth", "$sce"];
