import Utilities from "Utilities";

import { Regex } from "constants/regex.constant";

import * as _ from "lodash";
import { IHelpDialogModal } from "interfaces/help-dialog-modal.interface";

export default function ChangePasswordController($rootScope, $scope, $state, Accounts, ModalService, Users, Branding, Marketing, $q, BasicAuth, $sce) {

    $scope.app = Branding.getBrandingFromStorage();
    $scope.loading = false;
    $scope.Ready = false;
    $scope.initState = true;

    $scope.type1 = "password";
    $scope.type2 = "password";

    $scope.changePasswordData = {
        Email: "",
        NewPassword: "",
        ConfirmNewPassword: "",
    };

    $scope.errors = {
        Email: false,
        PasswordMatch: false,
        PasswordSpecial: false,
        PasswordLength: false,
    };

    $scope.marketingContent = {};

    const getMarketing = () => {
        return Marketing.getMarketing()
            .then((response: any) => {
                if (response.result) {
                    $scope.marketingContent = response.data;
                    $scope.marketingContent.headline = $sce.trustAsHtml($scope.marketingContent.headline);
                    $scope.marketingContent.copy = $sce.trustAsHtml($scope.marketingContent.copy);
                    $scope.marketingContent.backgroundImage = $scope.marketingContent.backgroundImage || "/images/default-green.jpg";
                }
            });
    };

    const getInfo = () => {
        if (BasicAuth) {
            $scope.user = BasicAuth.user ? BasicAuth.user : false;
            if ($scope.user) {
                $scope.changePasswordData.Email = $scope.user.Email ? $scope.user.Email : "";
                $scope.changePasswordData.FirstName = $scope.user.FirstName ? $scope.user.FirstName : "";
                $scope.changePasswordData.LastName = $scope.user.LastName ? $scope.user.LastName : "";
            } else {
                $scope.user = false;
            }
        }
    };

    const checkForMarketingImage = () => {
        if ($scope.marketingContent.backgroundImage) {
            Marketing.checkImage($scope.marketingContent.backgroundImage)
                .then((isImage: any) => {
                    if (!isImage) {
                        $scope.marketingContent = Marketing.getMarketingDefaults();
                    }
                    $scope.Ready = true;
                });
        } else {
            $scope.Ready = true;
        }
    };

    $scope.init = () => {
        const promiseArray = [
            getMarketing(),
        ];

        $q.all(promiseArray)
            .then(() => {
                getInfo();
                checkForMarketingImage();
            });
    };

    $scope.changeForgotPassword = () => {
        $scope.loading = true;
        if ($scope.validatePassword()) {
            const data = {
                NewPassword: $scope.changePasswordData.NewPassword,
                ConfirmNewPassword: $scope.changePasswordData.ConfirmNewPassword,
            };
            Accounts.changeForgottenPassword(data)
                .then((response: any) => {
                    if (response.result) {
                        const loginData = {
                            Email: $scope.changePasswordData.Email,
                            Password: $scope.changePasswordData.NewPassword,
                        };

                        Accounts.login(loginData)
                            .then((res: any) => {
                                if (res.result) {
                                    $state.go("landing",  null, { reload: true });
                                } else {
                                    $scope.loading = false;
                                }
                            });
                    } else {
                        $scope.loading = false;
                    }
                });
        } else {
            $scope.loading = false;
        }
    };

    $scope.validatePassword = () => {
        $scope.initState = false;
        $scope.changePasswordData.NewPassword = !_.isUndefined($scope.changePasswordData.NewPassword) ? $scope.changePasswordData.NewPassword.replace(/\s/g, "") : "";
        $scope.changePasswordData.ConfirmNewPassword = !_.isUndefined($scope.changePasswordData.ConfirmNewPassword) ? $scope.changePasswordData.ConfirmNewPassword.replace(/\s/g, "") : "";
        $scope.errors.PasswordLength = !Utilities.validateLength($scope.changePasswordData.NewPassword);
        $scope.errors.Uppercase = !Utilities.validateUppercase($scope.changePasswordData.NewPassword);
        $scope.errors.Lowercase = !Utilities.validateLowercase($scope.changePasswordData.NewPassword);
        $scope.errors.Number = !Utilities.validateNumber($scope.changePasswordData.NewPassword);
        $scope.errors.PasswordMatch = !Utilities.validateMatch($scope.changePasswordData.NewPassword, $scope.changePasswordData.ConfirmNewPassword);
        $scope.errors.emailEqualToPassword = !_.isUndefined($scope.changePasswordData.NewPassword) &&
        (
            $scope.changePasswordData.Email.toLowerCase() === $scope.changePasswordData.NewPassword.toLowerCase()
        );
        $scope.errors.mustContainOneSpecialChar = !_.isUndefined($scope.changePasswordData.NewPassword) && (Regex.SPECIAL_CHARACTERS.test($scope.changePasswordData.NewPassword));
        return !$scope.errors.PasswordLength &&
            !$scope.errors.Uppercase &&
            !$scope.errors.Lowercase &&
            !$scope.errors.Number &&
            !$scope.errors.PasswordMatch &&
            !$scope.errors.emailEqualToPassword &&
            $scope.errors.mustContainOneSpecialChar;
    };

    $scope.helpChangePasswordContactModal = () => {

        const modalData: IHelpDialogModal = {
            modalTitle: $rootScope.t("ResetPassword"),
            content: $rootScope.t("TroubleResettingPassword", {
                Email: "<a href='mailto:support@onetrust.com?subject=" + $rootScope.t("ForgotPasswordSubject")
                    + "'>support@onetrust.com</a>",
                Phone: "<a href='tel:+1 (844) 900-0472'>+1 (844) 900-0472</a>",
            }),
        };
        ModalService.setModalData(modalData);
        ModalService.openModal("helpDialogModal");
    };

    $scope.init();

}

ChangePasswordController.$inject = ["$rootScope", "$scope", "$state", "Accounts", "ModalService", "Users", "Branding", "Marketing", "$q", "BasicAuth", "$sce"];
