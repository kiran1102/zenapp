import { UserRoles } from "constants/user-roles.constant";

export default function Users($http, $rootScope, PRINCIPAL) {

    return {
        formatTeamLabel: function (team, model) {
            if (model !== undefined && model !== "") {
                for (var index in team) {
                    if (team.hasOwnProperty(index)) {
                        if (model === team[index].Id) {
                            return team[index].FullName ? team[index].FullName : "User Name Not Found";
                        }
                    }
                }
                return "";
            }
        },

        translateRoleNames: function (usersArray) {
            if (_.isArray(usersArray)) {
                _.each(usersArray, function (user) {
                    switch (user.RoleName) {
                        case UserRoles.SiteAdmin:
                            return user.roleNameTranslated = $rootScope.t("SiteAdmin");
                        case UserRoles.PrivacyOfficer:
                            return user.roleNameTranslated = $rootScope.t("PrivacyOfficer");
                        case UserRoles.ProjectOwner:
                            return user.roleNameTranslated = $rootScope.t("ProjectOwner");
                        case UserRoles.InvitedUser:
                            return user.roleNameTranslated = $rootScope.t("InvitedUser");
                        case UserRoles.ProjectViewer:
                            return user.roleNameTranslated = $rootScope.t("ProjectViewer");
                        default:
                            return (user.roleNameTranslated = user.RoleName);
                    }
                });
                return usersArray;
            }
        },

        isActiveUser: function (user) {
            return user.IsActive;
        },
        isDisabledUser: function (user) {
            return !user.IsActive;
        },
        isInvitedUser: function (user) {
            return user.RoleName === UserRoles.InvitedUser;
        },
        isProjectViewer: function (user) {
            return user.RoleName === UserRoles.ProjectViewer;
        },
        excludeProjectViewer: function (user) {
            return user.RoleName !== UserRoles.ProjectViewer;
        },
        excludeCurrentUser: function (user) {
            return user.Id !== PRINCIPAL.getUser().Id;
        },
        emailsOnly: function (user) {
            return user.Email;
        },
        userIdsOnly: function (user) {
            return user.Id;
        },
        getSomeoneElseAssignments: function (users) {
            return _(users)
                .filter(this.isActiveUser)
                .filter(this.excludeCurrentUser)
                .filter(this.excludeProjectViewer)
                .value();
        },
        getProjectViewerEmails: function (users) {
            return _(users)
                .filter(this.isProjectViewer)
                .map(this.emailsOnly)
                .value();
        },
        getActiveNonProjectViewers: function (users) {
            return _(users)
                .filter(this.isActiveUser)
                .filter(this.excludeProjectViewer)
                .value();
        },
        getActiveProjectViewers: function (users) {
            return _(users)
                .filter(this.isActiveUser)
                .filter(this.isProjectViewer)
                .value();
        },
        getDisabledUsers: function (users) {
            return _.filter(users, this.isDisabledUser);
        }
    };
}

Users.$inject = ["$http", "$rootScope", "PRINCIPAL"];
