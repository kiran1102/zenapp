
"use strict";

export default function Tenants($http, $q, $localStorage, NotificationService, $rootScope) {
    return {
        getCurrent: getCurrent,
        getTenants: getTenants,
        updateTenant: updateTenant,
    };

    /** Allows a user to get their tenants. **/
    function getTenants() {
        return $http.get("/tenant/GetUserTenants")
            .then(function successCallback(response) {
                return {
                    result: true,
                    data: response.data
                };
            },
            function errorCallback(response) {
                NotificationService.alertError($rootScope.t("Error"), $rootScope.t("SorryCouldntGetTenantsList"));
                return {
                    result: false,
                    data: response.data
                };
            });
    };

    function getCurrent() {
        return $http.get("/tenant/getcurrent")
            .then(function successCallback(response) {
                return {
                    result: true,
                    data: response.data
                };
            },
            function errorCallback(response) {
                NotificationService.alertError($rootScope.t("Error"), $rootScope.t("SorryCouldntRetrieveTheTenant"));
                return {
                    result: false,
                    data: response.data
                };
            });
    };

}

Tenants.$inject = ["$http", "$q", "$localStorage", "NotificationService", "$rootScope"];
