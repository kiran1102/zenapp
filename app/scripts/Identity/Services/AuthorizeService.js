export default function Authorization($rootScope, $state, PRINCIPAL) {
    return {
        authorize: function () {
            PRINCIPAL.identity();
            var isAuthenticated = PRINCIPAL.isAuthenticated();
            if (isAuthenticated
                && ($rootScope.toState && $rootScope.toState.name.indexOf("zen.app.auth") > -1)
                && ($rootScope.toState && $rootScope.toState.name.indexOf("zen.app.invite") > -1)) {
                $state.go("landing", null, { reload: true });
            }
            return isAuthenticated;
        },
    };
}

Authorization.$inject = ["$rootScope", "$state", "PRINCIPAL"];
