// Rxjs
import {
    Observable,
    from,
} from "rxjs";

// Constants
import { AssessmentTemplateErrorCodes } from "constants/assessment-template-error-codes.constant";

// Services
import { ProtocolService } from "modules/core/services/protocol.service";
import { NotificationService } from "modules/shared/services/provider/notification.service";

// Interfaces
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import {
    IProtocolConfig,
    IProtocolMessages,
    IProtocolPacket,
    IProtocolResponse,
} from "interfaces/protocol.interface";
import {
    ICreateQuestionParams,
    IQuestionDetails,
    ITemplateQuestionReorderRequest,
    IAssessmentTemplateAttribute,
    IQuestionOptionDetails,
} from "interfaces/assessment-template-question.interface";
import {
    ITemplateWelcomeTextUpdateParams,
    ICreateTemplateVersionResponse,
    IPublishTemplatePayload,
} from "interfaces/assessment-template-detail.interface";
import { IAssessmentTemplateDetail } from "interfaces/assessment-template-detail.interface";
import {
    IAssessmentTemplateSectionCreateParam,
    IAssessmentTemplateSection,
} from "interfaces/assessment-template-section.interface";
import { IGalleryTemplateResponse } from "interfaces/gallery-templates.interface";
import { ICustomTemplate } from "modules/template/interfaces/custom-templates.interface";
import {
    ITemplateVersion,
    ITemplateNameValue,
} from "modules/template/interfaces/template.interface";
import { IAssessmentNavigation } from "interfaces/assessment/assessment-navigation.interface";
import { ITemplateBuilderState } from "interfaces/template-reducer.interface";
import { ITemplateRule } from "modules/template/components/template-page/template-rule-list/template-rule/template-rule.interface";
import { IStringMap } from "interfaces/generic.interface";
import {
    IInventoryRelationshipPayload,
    IGetInventoryRelationshipPayload,
} from "modules/template/interfaces/template-related-inventory.interface";
import { IGRATemplateCardsResponse } from "modules/global-readiness/interfaces/gra-welcome.interface";

export class TemplateAPI {
    public static $inject: string[] = ["$rootScope", "OneProtocol", "NotificationService"];

    private translations: IStringMap<string>;

    constructor(
        private readonly $rootScope: IExtendedRootScopeService,
        private readonly OneProtocol: ProtocolService,
        private notificationService: NotificationService,
    ) {
        this.translations = {
            CannotCreateNewVersionOfThisTemplate: $rootScope.t("CannotCreateNewVersionOfThisTemplate"),
            CouldNotAddSectionToTemplate: $rootScope.t("CouldNotAddSectionToTemplate"),
            CouldNotCreateQuestionnaire: $rootScope.t("CouldNotCreateQuestionnaire"),
            CouldNotDeleteTheQuestion: $rootScope.t("CouldNotDeleteTheQuestion"),
            CouldNotDeleteTheQuestionnaire: $rootScope.t("CouldNotDeleteTheQuestionnaire "),
            CouldNotDeleteTheRule: $rootScope.t("CouldNotDeleteTheRule"),
            CouldNotDeleteTheSection: $rootScope.t("CouldNotDeleteTheSection"),
            CouldNotPublishQuestionnaire: $rootScope.t("CouldNotPublishQuestionnaire"),
            CouldNotReadQuestionnaire: $rootScope.t("CouldNotReadQuestionnaire"),
            CouldNotReorderQuestionOnTheSection: $rootScope.t("CouldNotReorderQuestionOnTheSection"),
            CouldNotReorderSection: $rootScope.t("CouldNotReorderSection"),
            CouldNotRetrieveQuestionnaires: $rootScope.t("CouldNotRetrieveQuestionnaires"),
            CouldNotUpdateQuestionnaire: $rootScope.t("CouldNotUpdateQuestionnaire"),
            NoQuestionUnderSection: $rootScope.t("NoQuestionUnderSection"),
            NoSectionUnderTemplate: $rootScope.t("NoSectionUnderTemplate"),
            QuestionnaireCreated: $rootScope.t("QuestionnaireCreated"),
            QuestionnairePublished: $rootScope.t("QuestionnairePublished"),
            SectionCreatedAndAddedToTemplate: $rootScope.t("SectionCreatedAndAddedToTemplate"),
            SectionUpdated: $rootScope.t("SectionUpdated"),
            TheSectionCouldNotBeUpdated: $rootScope.t("TheSectionCouldNotBeUpdated"),
            UpdatedQuestionnaire: $rootScope.t("UpdatedQuestionnaire"),
            TemplateArchiveSuccessMessage: $rootScope.t("TemplateArchiveSuccessMessage"),
            CouldNotArchiveTemplate: $rootScope.t("CouldNotArchiveTemplate"),
            TemplateUnarchiveSuccessMessage: $rootScope.t("TemplateUnarchiveSuccessMessage"),
            CouldNotUnarchiveTemplate: $rootScope.t("CouldNotUnarchiveTemplate"),
        };
    }

    public createNewTemplateVersion(templateId: string): ng.IPromise<IProtocolResponse<ICreateTemplateVersionResponse>> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("POST", `/api/template/v1/templates/version/${templateId}`);
        const messages: IProtocolMessages = {
            Success: { custom: this.translations.QuestionnaireCreated },
        };
        return this.OneProtocol.http(config, messages);
    }

    public createQuestion(sectionId: string, templateId: string, question: ICreateQuestionParams): ng.IPromise<IQuestionDetails | null> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("POST", `/api/template/v1/templates/${templateId}/sections/${sectionId}/questions`, null, question);
        const messages: IProtocolMessages = {
            Success: { custom: this.translations.UpdatedQuestionnaire },
            Error: { custom: this.translations.CannotCreateNewVersionOfThisTemplate },
        };
        return this.OneProtocol.http(config, messages)
            .then((response: IProtocolPacket): IQuestionDetails => response.result ? response.data : null);
    }

    public createTemplateSection(templateId: string, section: IAssessmentTemplateSectionCreateParam): ng.IPromise<IProtocolPacket> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("POST", `/api/template/v1/templates/${templateId}/sections`, null, section);
        const messages: IProtocolMessages = {
            Success: { custom: this.translations.SectionCreatedAndAddedToTemplate },
            Error: { custom: this.translations.CouldNotAddSectionToTemplate },
        };
        return this.OneProtocol.http(config, messages);
    }

    public deleteTemplateSection(templateId: string, sectionId: string): ng.IPromise<IProtocolResponse<ITemplateBuilderState>> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("DELETE", `/api/template/v1/templates/${templateId}/sections/${sectionId}`);
        const messages: IProtocolMessages = {
            Error: { custom: this.translations.CouldNotDeleteTheSection },
        };
        return this.OneProtocol.http(config, messages);
    }

    public getTemplateMetadata(templateId: string): ng.IPromise<IProtocolResponse<IGRATemplateCardsResponse>> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("GET", `/api/template/v1/templates/${templateId}/export`);
        const messages: IProtocolMessages = {
            Error: { Hide: true },
        };
        return this.OneProtocol.http(config, messages);
    }

    public getAttributesByQuestion(templateId: string, questionId: string): ng.IPromise<IProtocolResponse<IAssessmentTemplateAttribute[]>> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("GET", `/api/template/v1/templates/${templateId}/questions/${questionId}/attributes`);
        const messages: IProtocolMessages = { Error: { custom: this.translations.NoRemainingAttributes } };
        return this.OneProtocol.http(config, messages);
    }

    public getDetailByVersion(id: string, version: number): Observable<IProtocolResponse<IAssessmentTemplateDetail>> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("GET", `/api/template/v1/templates/rootversion/${id}?version=${version}`);
        const messages: IProtocolMessages = { Error: { custom: this.translations.CouldNotReadQuestionnaire } };
        return from(this.OneProtocol.http(config, messages));
    }

    public getQuestionById(templateId: string, sectionId: string, questionId: string): ng.IPromise<IQuestionDetails | null> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("GET", `/api/template/v1/templates/${templateId}/sections/${sectionId}/questions/${questionId}`);
        const messages: IProtocolMessages = { Error: { custom: this.translations.CouldNotReadQuestionnaire } };
        return this.OneProtocol.http(config, messages)
            .then((response: IProtocolPacket): IQuestionDetails => response.result ? response.data : null);
    }

    public getTemplateSectionDetails(templateId: string): ng.IPromise<ITemplateBuilderState | null> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("GET", `/api/template/v1/templates/${templateId}/details`);
        const messages: IProtocolMessages = { Error: { custom: this.translations.CouldNotReadQuestionnaire } };
        return this.OneProtocol.http(config, messages)
            .then((response: IProtocolPacket): ITemplateBuilderState => response.result ? response.data : null);
    }

    public getTemplateVersionList(templateId: string): ng.IPromise<ITemplateVersion[] | null> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("GET", `/api/template/v1/templates/versions/${templateId}`);
        const messages: IProtocolMessages = { Error: { custom: this.translations.CouldNotReadQuestionnaire } };
        return this.OneProtocol.http(config, messages)
            .then((response: IProtocolPacket): ITemplateVersion[] => response.result ? response.data : null);
    }

    public list(templateType: string): ng.IPromise<IProtocolResponse<ICustomTemplate[]>> {
        const params: { type: string } = { type: templateType };
        const config: IProtocolConfig = this.OneProtocol.customConfig("GET", "/api/template/v1/templates/details", params);
        const messages: IProtocolMessages = {
            Error: { custom: this.translations.CouldNotRetrieveQuestionnaires },
        };
        return this.OneProtocol.http(config, messages);
    }

    public getArchiveTemplates(templateType: string): ng.IPromise<IProtocolResponse<ICustomTemplate[]>> {
        const params: { type: string, active: false } = { type: templateType, active: false };
        const config: IProtocolConfig = this.OneProtocol.customConfig("GET", `/api/template/v1/templates/details`, params);
        const messages: IProtocolMessages = {
            Error: { custom: this.translations.CouldNotRetrieveQuestionnaires },
        };
        return this.OneProtocol.http(config, messages);
    }

    public updateQuestion = (templateId: string, sectionId: string, questionId: string, question: IQuestionDetails): ng.IPromise<IProtocolResponse<ITemplateBuilderState>> => {
        const config: IProtocolConfig = this.OneProtocol.customConfig("PUT", `/api/template/v1/templates/${templateId}/sections/${sectionId}/questions/${questionId}`, null, question);
        const messages: IProtocolMessages = {
            Error: { custom: this.translations.CouldNotUpdateQuestionnaire },
        };
        return this.OneProtocol.http(config, messages);
    }

    public updateTemplateSection(templateId: string, sectionId: string, section: { name: string }): ng.IPromise<IAssessmentTemplateSection | null> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("PUT", `/api/template/v1/templates/${templateId}/sections/${sectionId}`, null, section);
        const messages: IProtocolMessages = {
            Success: { custom: this.translations.SectionUpdated },
            Error: { custom: this.translations.TheSectionCouldNotBeUpdated },
        };
        return this.OneProtocol.http(config, messages)
            .then((response: IProtocolPacket): IAssessmentTemplateSection => response.result ? response.data : null);
    }

    public updatePublishedTemplateSection(templateId: string, sectionId: string, section: IAssessmentTemplateSection): ng.IPromise<IAssessmentTemplateSection | null> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("PUT", `/api/template/v1/templates/published/${templateId}/sections/${sectionId}`, null, section);
        const messages: IProtocolMessages = {
            Success: { custom: this.translations.SectionUpdated },
            Error: { custom: this.translations.TheSectionCouldNotBeUpdated },
        };
        return this.OneProtocol.http(config, messages)
            .then((response: IProtocolResponse<IAssessmentTemplateSection>): IAssessmentTemplateSection => response.result ? response.data : null);
    }

    public updateWelcomeText(templateId: string, welcomeText: ITemplateWelcomeTextUpdateParams): ng.IPromise<IProtocolPacket> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("PUT", `/api/template/v1/templates/${templateId}/welcometext`, null, welcomeText);
        const messages: IProtocolMessages = {
            Success: { custom: this.translations.UpdatedQuestionnaire },
            Error: { custom: this.translations.CouldNotUpdateQuestionnaire },
        };
        return this.OneProtocol.http(config, messages);
    }

    public reorderSection(sectionId: string, nextSectionId: string, templateId: string): ng.IPromise<IProtocolResponse<ITemplateBuilderState>> {
        const data: { sectionId: string, nextSectionId: string } = { sectionId, nextSectionId };
        const config: IProtocolConfig = this.OneProtocol.customConfig("POST", `/api/template/v1/templates/${templateId}/reorder/section`, null, data);
        const messages: IProtocolMessages = { Error: { custom: this.translations.CouldNotReorderSection } };
        return this.OneProtocol.http(config, messages);
    }

    public reorderQuestion(templateId: string, requestParams: ITemplateQuestionReorderRequest): ng.IPromise<IProtocolResponse<ITemplateBuilderState>> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("POST", `/api/template/v1/templates/${templateId}/reorder/question`, null, requestParams);
        const messages: IProtocolMessages = { Error: { custom: this.translations.CouldNotReorderQuestionOnTheSection } };
        return this.OneProtocol.http(config, messages);
    }

    public resolveInvalidQuestion(templateId: string, sectionId: string, questionId: string, data: ICreateQuestionParams): ng.IPromise<IProtocolResponse<ITemplateBuilderState>> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("PUT", `/api/template/v1/templates/${templateId}/sections/${sectionId}/questions/${questionId}/resolve`, null, data);
        const messages: IProtocolMessages = { Error: { custom: this.$rootScope.t("ErrorUpdatingTemplate") } };
        return this.OneProtocol.http(config, messages);
    }

    public deleteTemplateQuestion(templateId: string, sectionId: string, questionId: string): ng.IPromise<IProtocolResponse<ITemplateBuilderState>> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("DELETE", `/api/template/v1/templates/${templateId}/sections/${sectionId}/questions/${questionId}`);
        const messages: IProtocolMessages = {
            Error: { custom: this.translations.CouldNotDeleteTheQuestion },
        };
        return this.OneProtocol.http(config, messages);
    }

    public deleteTemplateDraft(templateId: string): ng.IPromise<boolean> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("DELETE", `/api/template/v1/templates/${templateId}`);
        const messages: IProtocolMessages = {
            Error: { custom: this.translations.CouldNotDeleteTheQuestionnaire },
        };
        return this.OneProtocol.http(config, messages)
            .then((response: IProtocolPacket): boolean => response.result);
    }

    public publishTemplate(templateId: string, payload: IPublishTemplatePayload = null): ng.IPromise<IProtocolResponse<void>> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("PUT", `/api/template/v1/templates/${templateId}/publish`, null, payload);
        const messages: IProtocolMessages = {
            Success: { custom: this.translations.QuestionnairePublished },
        };
        return this.OneProtocol.http(config, messages)
            .then((response: IProtocolResponse<void>): IProtocolResponse<void> => {
                if (!response.result) {
                    if (response.errorCode === AssessmentTemplateErrorCodes.TEMPLATE_DO_NOT_HAVE_SECTION) {
                        this.notificationService.alertError(this.$rootScope.t("Error"), this.translations.NoSectionUnderTemplate);
                    }
                    if (response.errorCode === AssessmentTemplateErrorCodes.SECTION_DO_NOT_HAVE_QUESTION) {
                        this.notificationService.alertError(this.$rootScope.t("Error"), this.translations.NoQuestionUnderSection);
                    }
                }
                return response;
            });
    }

    public getGalleryTemplateByTemplateId(templateId: string): ng.IPromise<IProtocolResponse<IGalleryTemplateResponse>> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("GET", `/api/gallery/v1/gallery/templates/${templateId}`);
        const messages: IProtocolMessages = { Error: { custom: this.translations.CouldNotReadQuestionnaire } };
        return this.OneProtocol.http(config, messages);
    }

    public updateNavigations(templateId: string, sectionId: string, questionId: string, navigations: IAssessmentNavigation[]): ng.IPromise<IProtocolResponse<IAssessmentNavigation[]>> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("POST", `/api/template/v1/templates/${templateId}/sections/${sectionId}/questions/${questionId}/navigations`, null, navigations);
        const messages: IProtocolMessages = { Error: { custom: this.$rootScope.t("CouldNotUpdateCondition") } };
        return this.OneProtocol.http(config, messages);
    }

    public getAssessmentPublishedTemplates(templateType: string): ng.IPromise<ITemplateNameValue[] | boolean> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("GET", `/api/template/v1/templates/published/identity`, { type: templateType });
        const messages: IProtocolMessages = { Error: { custom: this.$rootScope.t("ErrorGettingTemplates") } };
        return this.OneProtocol.http(config, messages).then((res: IProtocolResponse<ITemplateNameValue[] | boolean>): ITemplateNameValue[] | boolean => {
            if (!res.result) return false;
            return res.data;
        });
    }

    public saveTemplateRule(templateId: string, rules: ITemplateRule[]): Observable<ITemplateRule[]> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("POST", `/api/template/v1/templates/${templateId}/actions`, null, rules);
        const messages: IProtocolMessages = {
            Success: { custom: this.$rootScope.t("RuleSavedSuccessfully") },
            Error: { custom: this.$rootScope.t("ErrorUpdatingTemplateRules") },
        };
        return from(this.OneProtocol.http(config, messages)
            .then((response: IProtocolResponse<ITemplateRule[]>): ITemplateRule[] => {
                if (response.result) {
                    return response.data;
                }
                return [];
            }));
    }

    public deleteTemplateActionRule(templateId: string, conditionId: string): ng.IPromise<boolean> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("DELETE", `/api/template/v1/templates/${templateId}/action/${conditionId}`);
        const messages: IProtocolMessages = { Error: { custom: this.translations.CouldNotDeleteTheRule } };
        return this.OneProtocol.http(config, messages)
            .then((response: IProtocolPacket): boolean => response.result);
    }

    public getQuestionOptions(questionId: string, sectionId: string, templateId: string): Observable<IProtocolResponse<IQuestionOptionDetails[]>> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("GET", `/api/template/v1/templates/${templateId}/sections/${sectionId}/questions/${questionId}/options`);
        const messages: IProtocolMessages = { Error: { custom: this.$rootScope.t("CouldNotFetchOptionsForQuestion") } };
        return from(this.OneProtocol.http(config, messages));
    }

    public updateInventoryRelationship(payload: IInventoryRelationshipPayload): ng.IPromise<IProtocolResponse<boolean>> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("POST", `/api/template/v1/templates/${payload.templateId}/inventoryrelationship`, null, payload.contract);
        const messages: IProtocolMessages = { Error: { Hide: true } };
        return this.OneProtocol.http(config, messages);
    }

    public getInventoryRelationship(templateId: string, sectionId: string, questionId: string): ng.IPromise<IProtocolResponse<IGetInventoryRelationshipPayload[]>> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("GET", `/api/template/v1/templates/${templateId}/sections/${sectionId}/questions/${questionId}/inventoryrelationship`);
        const messages: IProtocolMessages = { Error: { custom: this.$rootScope.t("Error.GetInventoryRelationship") } };
        return this.OneProtocol.http(config, messages);
    }

    public getOpenAssessmentCount(rootVersionId: string): ng.IPromise<number> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("GET", `/assessment-v2/v2/assessments/templates/rootVersion/${rootVersionId}/assessments/count?statusList=NOT_STARTED,IN_PROGRESS`);
        const messages: IProtocolMessages = { Error: { custom: this.$rootScope.t("Error.GetInflightAssessmentCount") } };
        return this.OneProtocol.http(config, messages).then((response: IProtocolResponse<number>): number => response.data);
    }

    public archiveTemplate(rootVersionId: string): ng.IPromise<IProtocolResponse<void>> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("POST", `/api/template/v1/templates/rootversion/${rootVersionId}/inactivate`);
        const messages: IProtocolMessages = {
            Success: { custom: this.translations.TemplateArchiveSuccessMessage },
            Error: { custom: this.translations.CouldNotArchiveTemplate },
        };
        return this.OneProtocol.http(config, messages);
    }

    public unarchiveTemplate(rootVersionId: string): ng.IPromise<IProtocolResponse<void>> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("POST", `/api/template/v1/templates/rootversion/${rootVersionId}/activate`);
        const messages: IProtocolMessages = {
            Success: { custom: this.translations.TemplateUnarchiveSuccessMessage },
            Error: { custom: this.translations.CouldNotUnarchiveTemplate },
        };
        return this.OneProtocol.http(config, messages);
    }
}
