// 3rd Party
import { forEach, sortBy } from "lodash";

// Redux
import {
    getTemplateState,
    getSectionById,
    getQuestionById,
    sectionHasInvalidQuestions,
} from "oneRedux/reducers/template.reducer";

// Interfaces
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import {
    IAssessmentTemplateQuestion,
    IAssessmentTemplateAttribute,
    IQuestionDetails,
    IInventoryQuestionOption,
} from "interfaces/assessment-template-question.interface";
import { IStringMap } from "interfaces/generic.interface";
import { IStore } from "interfaces/redux.interface";
import { IAssessmentTemplateSection } from "interfaces/assessment-template-section.interface";

// Enums & Constants
import { AttributeType, AttributeQuestionResponseType, QuestionTypeNames } from "constants/question-types.constant";
import { InventorySchemaIds } from "constants/inventory-config.constant";
import { RelatedInventoryDestination } from "modules/template/enums/template-related-inventory.enum";
import { QuestionAnswerTypes } from "enums/question-types.enum";

// Services
import TemplateAction from "app/scripts/Template/Services/actions/template-action.service";

export class TemplateBuilderService {

    public static $inject: string[] = [
        "$rootScope",
        "store",
        "TemplateAction",
    ];

    constructor(
        private $rootScope: IExtendedRootScopeService,
        private store: IStore,
        private templateAction: TemplateAction,
    ) { }

    getTranslations(): IStringMap<string> {
        return {
            asset: this.$rootScope.t("Asset"),
            entity: this.$rootScope.t("Entity"),
            attributeSingleSelectInventory: this.$rootScope.t("AttributeSingleSelectInventory"),
            attributeWhichInventoryQuestion: this.$rootScope.t("AttributeWhichInventoryQuestion"),
            cancel: this.$rootScope.t("Cancel"),
            confirm: this.$rootScope.t("Confirm"),
            deleteQuestion: this.$rootScope.t("DeleteQuestion"),
            loading: this.$rootScope.t("Loading"),
            noRemainingAttributes: this.$rootScope.t("NoRemainingAttributes"),
            processingActivity: this.$rootScope.t("ProcessingActivity"),
            resolve: this.$rootScope.t("Resolve"),
            resolveInvalidQuestionModalHelpText: this.$rootScope.t("ResolveInvalidQuestionModalHelpText"),
            save: this.$rootScope.t("Save"),
            saveAndAddNew: this.$rootScope.t("SaveAndAddNew"),
            selectInventoryQuestion: this.$rootScope.t("SelectInventoryQuestion"),
            selectAttribute: this.$rootScope.t("SelectAttribute"),
            sureToDeleteThisQuestion: this.$rootScope.t("SureToDeleteThisQuestion"),
            vendor: this.$rootScope.t("Vendor"),
            whichAttribute: this.$rootScope.t("WhichAttribute"),
            whichInventoryQuestionFollowUpFor: this.$rootScope.t("WhichInventoryQuestionFollowUpFor"),
        };
    }

    getInventoryTypes(): IStringMap<string> {
        const translations = this.getTranslations();
        return {
            [InventorySchemaIds.Assets]: translations.asset,
            [InventorySchemaIds.Processes]: translations.processingActivity,
            [InventorySchemaIds.Vendors]: translations.vendor,
            [InventorySchemaIds.Entities]: translations.entity,
        };
    }

    applyInventoryQuestionFormatting(question: IAssessmentTemplateQuestion, sectionSequence: number): IAssessmentTemplateQuestion {
        const inventoryTypes = this.getInventoryTypes();
        const inventoryType = question.attributeJson ? question.attributeJson.inventoryType : JSON.parse(question.attributes).inventoryType;
        const questionName = question.friendlyName || question.content;
        const content: string = question.questionType === QuestionTypeNames.Inventory ? `${sectionSequence}.${question.sequence}) ${inventoryTypes[inventoryType]}: ${questionName}`
                                                                      : `${sectionSequence}.${question.sequence}) ${questionName}`;
        return {
            ...question,
            sectionSequence,
            questionSequence: question.sequence,
            content,
        };
    }

    applyIncidentQuestionFormatting(question: IAssessmentTemplateQuestion, sectionSequence: number): IAssessmentTemplateQuestion {
        const questionName = question.friendlyName || question.content;
        return {
            ...question,
            sectionSequence,
            questionSequence: question.sequence,
            content: `${sectionSequence}.${question.sequence}) ${this.$rootScope.t("Incident")}: ${questionName}`,
        };
    }

    fillRelatedInventoryQuestionsDropdown(currentQuestionId: string, type: RelatedInventoryDestination): IAssessmentTemplateQuestion[] {
        const state = this.store.getState();
        const builder = getTemplateState(state).builder;
        const inventoryQuestions = [];

        forEach(builder.sectionById, (section: IAssessmentTemplateSection): void => {
            forEach(builder.sectionIdQuestionIdsMap[section.id], (questionId: string): void => {
                const question = getQuestionById(questionId)(state);
                if (question.id !== currentQuestionId && question.attributeJson && !question.attributeJson.isMultiSelect) {
                    // when RelatedInventoryDestination is Asset or Personal Data ==> Processing Activity Inventory questions
                    if ((type === RelatedInventoryDestination.Asset || type === RelatedInventoryDestination.PersonalData)
                        && question.attributeJson.inventoryType === InventorySchemaIds.Processes) {
                        inventoryQuestions.push(this.applyInventoryQuestionFormatting(question, section.sequence));
                        // when RelatedInventoryDestination is Processing Activity ==> Asset Inventory questions
                    } else if (type === RelatedInventoryDestination.ProcessingActivity && question.attributeJson.inventoryType === InventorySchemaIds.Assets) {
                        inventoryQuestions.push(this.applyInventoryQuestionFormatting(question, section.sequence));
                    }
                }
            });
        });

        return sortBy(inventoryQuestions, ["sectionSequence", "questionSequence"]);
    }

    fillRelatedInventoryAndPersonalQuestionsDropdown(currentQuestionId: string, currentQuestionType: string): IAssessmentTemplateQuestion[] {
        const builder = getTemplateState(this.store.getState()).builder;
        const inventoryQuestions = [];

        forEach(builder.sectionById, (section: IAssessmentTemplateSection): void => {
            forEach(builder.sectionIdQuestionIdsMap[section.id], (questionId: string): void => {
                const question: IAssessmentTemplateQuestion = getQuestionById(questionId)(this.store.getState());
                if (question.id !== currentQuestionId) {
                    if (currentQuestionType === QuestionTypeNames.Inventory
                        && (question.questionType === QuestionTypeNames.Inventory
                        || question.questionType === QuestionTypeNames.PersonalData)) {
                        inventoryQuestions.push(this.applyInventoryQuestionFormatting(question, section.sequence));
                    } else if (currentQuestionType === QuestionTypeNames.PersonalData && question.questionType === QuestionTypeNames.Inventory) {
                        inventoryQuestions.push(this.applyInventoryQuestionFormatting(question, section.sequence));
                    }
                }
            });
        });

        return sortBy(inventoryQuestions, ["sectionSequence", "questionSequence"]);
    }

    fillInventoryQuestionsDropdown(sectionId: string, nextQuestionId?: string): IAssessmentTemplateQuestion[] {
        const state = this.store.getState();
        const builder = getTemplateState(state).builder;
        const selectedSectionSequence = (getSectionById(state, sectionId)).sequence;
        const selectedNextQuestionIdSequence = (getQuestionById(nextQuestionId)(state)).sequence;
        const inventoryQuestions = [];

        // Filter inventory questions based on sequence
        forEach(builder.sectionById, (section: IAssessmentTemplateSection): void => {
            forEach(builder.sectionIdQuestionIdsMap[section.id], (questionId: string): void => {
                const question = getQuestionById(questionId)(state);
                if (section.sequence < selectedSectionSequence) {
                    if (question.questionType === QuestionTypeNames.Inventory) {
                        inventoryQuestions.push(this.applyInventoryQuestionFormatting(question, section.sequence));
                    } else if (question.questionType === QuestionTypeNames.Incident) {
                        inventoryQuestions.push(this.applyIncidentQuestionFormatting(question, section.sequence));
                    }
                } else if (section.id === sectionId
                    && (!selectedNextQuestionIdSequence || question.sequence < selectedNextQuestionIdSequence)) {
                    if (question.questionType === QuestionTypeNames.Inventory) {
                        inventoryQuestions.push(this.applyInventoryQuestionFormatting(question, section.sequence));
                    } else if (question.questionType === QuestionTypeNames.Incident) {
                        inventoryQuestions.push(this.applyIncidentQuestionFormatting(question, section.sequence));
                    }
                }
            });
        });

        return sortBy(inventoryQuestions, ["sectionSequence", "questionSequence"]);
    }

    getAllInventoryQuestions(inventoryType: string): IInventoryQuestionOption[] {
        const state = this.store.getState();
        const builder = getTemplateState(state).builder;
        const inventoryQuestions = [];
        forEach(builder.sectionById, (section: IAssessmentTemplateSection): void => {
            forEach(builder.sectionIdQuestionIdsMap[section.id], (questionId: string): void => {
                const question = getQuestionById(questionId)(state);
                question.attributeJson = this.templateAction.parseAttributes(question.attributes);
                if (question.questionType === QuestionTypeNames.Inventory && question.attributeJson.inventoryType === inventoryType) {
                    inventoryQuestions.push(this.applyFriendlyNameAndContentFormatting(question, section.sequence));
                }
            });
        });
        return sortBy(inventoryQuestions, ["sectionSequence", "questionSequence"]);
    }

    applyFriendlyNameAndContentFormatting(question: IAssessmentTemplateQuestion, sectionSequence: number): IInventoryQuestionOption {
        let questionContent: string;
        if (question.friendlyName) {
            questionContent = `${sectionSequence}.${question.sequence}: ${question.friendlyName}`;
        } else {
            questionContent = `${sectionSequence}.${question.sequence}: ${question.content}`;
        }
        return {
            id: question.id,
            content: questionContent,
            sectionSequence,
            questionSequence: question.sequence,
        };
    }

    handleInventoryQuestionChangeAction(attributesList: IAssessmentTemplateAttribute[]): { isAttributeDropdownDisabled: boolean, attributePlaceholderText: string } {
        const isValidAttributesList = attributesList && attributesList.length > 0;
        const translations = this.getTranslations();
        return {
            isAttributeDropdownDisabled: !isValidAttributesList,
            attributePlaceholderText: isValidAttributesList ? "SelectAttribute" : "NoRemainingAttributes",
        };
    }

    buildQuestionForSelectedAttribute(attribute: IAssessmentTemplateAttribute, selectedInventoryQuestionId: string): IQuestionDetails {
        let newQuestion = null;
        if (attribute) {
            newQuestion = ({
                content: attribute.name,
                questionType: "ATTRIBUTE",
                required: false,
                options: [],
                attributeJson: [],
                parentQuestionId: selectedInventoryQuestionId,
            }) as Partial<IQuestionDetails>;
            newQuestion.attributeJson.attributeId = attribute.id;
            newQuestion.attributeJson.fieldName = attribute.fieldName;
            newQuestion.attributeJson.name = attribute.name;
            newQuestion.attributeJson.nameKey = attribute.nameKey;
            newQuestion.attributeJson.attributeType = attribute.attributeType;
            newQuestion.attributeJson.responseType = attribute.responseType;
            newQuestion.attributeJson.displayAnswerType = this.initializeDisplayAsDropdown(attribute);
        }
        return newQuestion;
    }

    buildQuestionForResolveInvalidQuestion(attribute: IAssessmentTemplateAttribute, selectedInventoryQuestionId: string): IQuestionDetails {
        let newQuestion = null;
        if (attribute) {
            newQuestion = ({
                content: attribute.name,
                questionType: "ATTRIBUTE",
                required: false,
                options: [],
                attributes: JSON.stringify({
                    attributeId: attribute.id,
                    fieldName: attribute.fieldName,
                    name: attribute.name,
                    nameKey: attribute.nameKey,
                    attributeType: attribute.attributeType,
                    responseType: attribute.responseType,
                }),
                parentQuestionId: selectedInventoryQuestionId,
            }) as IQuestionDetails;
        }
        return newQuestion;
    }

    isQuestionInvalid(sectionId: string, questionId: string): boolean {
        const question = getQuestionById(questionId)(this.store.getState());
        return sectionHasInvalidQuestions(this.store.getState(), sectionId)
            && question.valid === false;
    }

    initializeDisplayAsDropdown(attribute: IAssessmentTemplateAttribute): QuestionAnswerTypes {
        // Not setting display as dropdown option for users , location and text type of attributes.
        if (!(attribute.attributeType === AttributeType.USERS || attribute.attributeType === AttributeType.LOCATIONS
            || attribute.responseType === AttributeQuestionResponseType.TEXT)) {
            return QuestionAnswerTypes.Button;
        }
    }
}
