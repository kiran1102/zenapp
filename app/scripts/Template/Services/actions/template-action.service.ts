// Rxjs
import { Observable } from "rxjs";
import {
    filter,
    first,
    map,
    tap,
    shareReplay,
    startWith,
} from "rxjs/operators";

// Third Party
import {
    forEach,
    forIn,
} from "lodash";

// Constants
import { TemplateStates } from "constants/template-states.constant";
import {
    QuestionTypes,
    QuestionResponseType,
    QuestionTypeNames,
} from "constants/question-types.constant";
import { AssessmentTemplateErrorCodes } from "constants/assessment-template-error-codes.constant";

// Interfaces
import {
    IAssessmentTemplateDetail,
    IPublishTemplatePayload,
} from "interfaces/assessment-template-detail.interface";
import {
    IAssessmentTemplateSection,
} from "interfaces/assessment-template-section.interface";
import {
    ICreateQuestionParams,
    IQuestionDetails,
    IQuestionBuilderModalData,
    IAssessmentTemplateQuestion,
    ITemplateQuestionReorderRequest,
    IQuestionOptionDetails,
    IAssessmentTemplateAttribute,
} from "interfaces/assessment-template-question.interface";
import {
    IProtocolPacket,
    IProtocolResponse,
} from "interfaces/protocol.interface";
import {
    IStore,
    IStoreState,
} from "interfaces/redux.interface";
import { IStringMap } from "interfaces/generic.interface";
import { ITemplateWelcomeTextUpdateParams } from "interfaces/assessment-template-detail.interface";
import {
    getCurrentTemplate,
    getTempQuestionById,
    TemplateActions,
    getQuestionById,
    getTemplateQuestionOptions,
    getTempPrePopulateResponseQuestionIdByInventoryType,
} from "oneRedux/reducers/template.reducer";
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import { ICustomTemplate } from "modules/template/interfaces/custom-templates.interface";
import { IInventorySchemaV2 } from "interfaces/inventory.interface";
import { IAssessmentNavigation } from "interfaces/assessment/assessment-navigation.interface";
import { ITemplateVersion } from "modules/template/interfaces/template.interface";
import { ITemplateBuilderState } from "interfaces/template-reducer.interface";
import {
    IInventoryRelationshipPayload,
    IGetInventoryRelationshipPayload,
    IInventoryRelationshipContract,
} from "modules/template/interfaces/template-related-inventory.interface";
import { IIncidentSchema } from "modules/template/interfaces/incident-schema.interface";
import { IGRATemplateCardsResponse } from "modules/global-readiness/interfaces/gra-welcome.interface";

// Services
import { ModalService } from "sharedServices/modal.service";
import { TemplateAPI } from "templateServices/api/template-api.service";
import { InventoryService } from "inventorySharedModule/shared/services/inventory-api.service";
import { IncidentSchemaApiService } from "modules/template/services/incident-schema-api.service";
import observableFromStore from "oneRedux/utils/observable-from-store";
import { NotificationService } from "modules/shared/services/provider/notification.service";

export default class TemplateAction {

    public static $inject: string[] = [
        "$rootScope",
        "$q",
        "store",
        "ModalService",
        "TemplateAPI",
        "Inventory",
        "IncidentSchemas",
        "NotificationService",
    ];
    private translations: IStringMap<string>;
    private loadingOptions$: IStringMap<Observable<IQuestionOptionDetails[]>> = {};

    constructor(
        private readonly $rootScope: IExtendedRootScopeService,
        private readonly $q: ng.IQService,
        private readonly store: IStore,
        private readonly modalService: ModalService,
        private readonly templateAPI: TemplateAPI,
        private readonly Inventory: InventoryService,
        private readonly IncidentSchemas: IncidentSchemaApiService,
        private notificationService: NotificationService,
    ) {
        this.translations = {
            InProgress: $rootScope.t("InProgress"),
            NotApplicable: this.$rootScope.t("NotApplicable"),
            CouldNotReorderQuestionnaire: $rootScope.t("CouldNotReorderQuestionnaire"),
            CouldNotReorderSection: $rootScope.t("CouldNotReorderSection"),
            AttributeQuestionBelowAssociatedQuestion: this.$rootScope.t("AttributeQuestionBelowAssociatedQuestion"),
            Success: this.$rootScope.t("Success"),
        };
    }

    public extractAttributes = (attributes: IStringMap<any>, removeAttributes: string[] = []): string => {
        const attributesCopy = { ...attributes };
        removeAttributes.forEach((attr: string): void => {
            delete attributesCopy[attr];
        });
        return JSON.stringify(attributesCopy);
    }

    public generateOptionUUID = (): string => {
        function s4(): string {
            return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
        }
        return s4() + s4() + "-" + s4() + "-" + s4() + "-" + s4() + "-" + s4() + s4() + s4();
    }

    public openTemplateEditorModal = (data: IQuestionBuilderModalData): void => {
        this.modalService.setModalData(data);
        this.modalService.openModal("downgradeTemplateQuestionBuilderModal");
    }

    public openTemplateAttributeQuestionModal = (data: IQuestionBuilderModalData): void => {
        this.modalService.setModalData(data);
        this.modalService.openModal("questionAttributeModal");
    }

    public openResolveInvalidQuestionModal = (data: IQuestionBuilderModalData): void => {
        this.modalService.setModalData(data);
        this.modalService.openModal("resolveInvalidQuestionModal");
    }

    public openTemplateWelcomeMessageModal = (templateId: string): void => {
        this.modalService.setModalData({ templateId });
        this.modalService.openModal("downgradeEditTemplateWelcomeMessage");
    }

    public createQuestion = (nextQuestionId: string, sectionId: string, templateId: string, question: IQuestionDetails): ng.IPromise<boolean> => {
        const questionObject: IQuestionDetails = this.getQuestionRequest(question);
        const reqObject: ICreateQuestionParams = {
            question: questionObject,
            nextQuestionId: nextQuestionId ? nextQuestionId : null,
        };
        return this.templateAPI.createQuestion(sectionId, templateId, reqObject)
            .then((response: IQuestionDetails): boolean => {
                if (response) {
                    const templateQuestion: IAssessmentTemplateQuestion = {
                        id: response.id,
                        attributeJson: response.attributeJson || null,
                        attributes: response.attributes || null,
                        content: response.content,
                        friendlyName: response.friendlyName,
                        destinationInventoryQuestion: response.destinationInventoryQuestion,
                        sourceInventoryQuestion: question.sourceInventoryQuestion,
                        linkAssessmentToInventory: response.linkAssessmentToInventory,
                        options: response.options ? [...response.options] : [],
                        parentQuestionId: response.parentQuestionId || null,
                        prePopulateResponse: response.prePopulateResponse,
                        questionType: response.questionType,
                        required: response.required,
                        sequence: response.sequence,
                    };
                    this.store.dispatch({ type: TemplateActions.ADD_QUESTION, sectionId, question: templateQuestion, questionId: response.id });
                    return true;
                }
                return false;
            });
    }

    public getAttributesByInventory(templateId: string, questionId: string): ng.IPromise<IAssessmentTemplateAttribute[]> {
        return this.templateAPI.getAttributesByQuestion(templateId, questionId)
            .then((response: IProtocolResponse<IAssessmentTemplateAttribute[] | null>): IAssessmentTemplateAttribute[] | null => {
                return response.data;
            });
    }

    public getTemplateMetadata(templateId: string): ng.IPromise<IGRATemplateCardsResponse> {
        return this.templateAPI.getTemplateMetadata(templateId)
            .then((response: IProtocolResponse<IGRATemplateCardsResponse>): IGRATemplateCardsResponse => {
                return response.data;
            });
    }

    public getDetailByVersion(rootVersionId: string, version: number): Observable<IProtocolResponse<IAssessmentTemplateDetail>> {
        this.store.dispatch({ type: TemplateActions.FETCH_TEMPLATE });
        return this.templateAPI
            .getDetailByVersion(rootVersionId, version)
            .pipe(
                tap((response: IProtocolResponse<IAssessmentTemplateDetail>) => {
                    this.store.dispatch({ type: TemplateActions.TEMPLATE_FETCHED, current: response.data });
                }),
            );
    }

    public getTemplateVersionList(templateId: string): ng.IPromise<ITemplateVersion[] | null> {
        return this.templateAPI
            .getTemplateVersionList(templateId)
            .then((response: any): ng.IPromise<ITemplateVersion[] | null> => {
                return response;
            });
    }

    public getQuestion = (templateId: string, sectionId: string, questionId: string): ng.IPromise<IQuestionDetails> => {
        return this.templateAPI.getQuestionById(templateId, sectionId, questionId)
            .then((response: IQuestionDetails): IQuestionDetails => {
                if (response) {
                    const question: IAssessmentTemplateQuestion = {
                        attributeJson: JSON.parse(response.attributes) || null,
                        content: response.content,
                        friendlyName: response.friendlyName,
                        destinationInventoryQuestion: response.destinationInventoryQuestion,
                        sourceInventoryQuestion: response.sourceInventoryQuestion,
                        id: response.id,
                        options: [...response.options || []],
                        parentQuestionId: response.parentQuestionId || null,
                        prePopulateResponse: response.prePopulateResponse,
                        questionType: response.questionType,
                        required: response.required,
                        valid: response.valid,
                        linkAssessmentToInventory: response.linkAssessmentToInventory,
                    };
                    this.store.dispatch({ type: TemplateActions.UPDATE_QUESTION, question });
                    const updatedQuestion: IQuestionDetails = this.getQuestionResponse(response, sectionId);
                    return updatedQuestion;
                }
            });
    }

    public parseAttributes = (attributes: string): IStringMap<any> => {
        return attributes ? JSON.parse(attributes) : null;
    }

    public parseQuestionById(questionById: IStringMap<IAssessmentTemplateQuestion>): IStringMap<IAssessmentTemplateQuestion> {
        forIn(questionById, (question, key) => {
            question.attributeJson = this.parseAttributes(question.attributes);
        });
        return questionById;
    }

    public setTemplateBuilderState(templateId: string): void {
        this.store.dispatch({ type: TemplateActions.RESET_BUILDER_VIEW });
        this.store.dispatch({ type: TemplateActions.FETCH_BUILDER, isFetching: true });
        this.loadingOptions$ = {};

        this.templateAPI
            .getTemplateSectionDetails(templateId)
            .then((response: ITemplateBuilderState) => {
                // TODO: This adapter is Tech debt - BE should send question.attributes as parsed JSON, not string for FE to parse
                return {
                    ...response,
                    questionById: this.parseQuestionById(response.questionById),
                };
            })
            .then((response: ITemplateBuilderState) => {
                if (response.sectionAllIds) {
                    this.store.dispatch({ type: TemplateActions.SET_BUILDER_VIEW, builderStructure: response });
                }
                this.store.dispatch({ type: TemplateActions.FETCH_BUILDER, isFetching: false });
            });
    }

    public setTemplateList(type: string): ng.IPromise<boolean> {
        return this.templateAPI.list(type).then((response: IProtocolResponse<ICustomTemplate[]>): boolean => {
            if (response.result) {
                this.store.dispatch({ type: TemplateActions.SET_TEMPLATES, templates: response.data });
            }
            return response.result;
        });
    }

    public getArchiveTemplates(type: string): ng.IPromise<boolean> {
        return this.templateAPI.getArchiveTemplates(type).then((response: IProtocolResponse<ICustomTemplate[]>): boolean => {
            if (response.result) {
                this.store.dispatch({ type: TemplateActions.SET_TEMPLATES, templates: response.data });
            }
            return response.result;
        });
    }

    public reorderQuestion(templateId: string, requestParams: ITemplateQuestionReorderRequest): ng.IPromise<boolean> {
        this.store.dispatch({ type: TemplateActions.TEMPLATE_BUILDER_LOADER_SHOW, text: this.translations.InProgress });
        return this.templateAPI.reorderQuestion(templateId, requestParams)
            .then((response: IProtocolResponse<ITemplateBuilderState>): boolean => {
                if (response.result) {
                    this.store.dispatch({ type: TemplateActions.SET_BUILDER_VIEW, builderStructure: response.data });
                } else {
                    if (response.errorCode === AssessmentTemplateErrorCodes.ATTRIBUTE_QUESTION_OUT_OF_ORDER) {
                        this.notificationService.alertWarning(this.$rootScope.t("Warning"), this.translations.AttributeQuestionBelowAssociatedQuestion);
                    } else {
                        this.notificationService.alertError(this.$rootScope.t("Error"), this.translations.CouldNotReorderQuestionnaire);
                    }
                    this.store.dispatch({ type: TemplateActions.TEMPLATE_BUILDER_LOADER_HIDE });
                }
                return response.result;
            });
    }

    public reorderSection(sectionId: string, nextSectionId: string, templateId: string): ng.IPromise<boolean> {
        this.store.dispatch({ type: TemplateActions.TEMPLATE_BUILDER_LOADER_SHOW, text: this.translations.InProgress });
        return this.templateAPI.reorderSection(sectionId, nextSectionId, templateId)
            .then((response: IProtocolResponse<ITemplateBuilderState>): boolean => {
                if (response.result) {
                    this.store.dispatch({ type: TemplateActions.SET_BUILDER_VIEW, builderStructure: response.data });
                } else {
                    if (response.errorCode === AssessmentTemplateErrorCodes.ATTRIBUTE_QUESTION_OUT_OF_ORDER) {
                        this.notificationService.alertWarning(this.$rootScope.t("Warning"), this.translations.AttributeQuestionBelowAssociatedQuestion);
                    } else {
                        this.notificationService.alertError(this.$rootScope.t("Error"), this.translations.CouldNotReorderSection);
                    }
                    this.store.dispatch({ type: TemplateActions.TEMPLATE_BUILDER_LOADER_HIDE });
                }
                return response.result;
            });
    }

    public saveWelcomeMessage = (templateId: string, welcomeText: ITemplateWelcomeTextUpdateParams): ng.IPromise<boolean> => {
        const templateStatus = getCurrentTemplate(this.store.getState()).status;

        if (templateStatus === TemplateStates.DRAFT.toUpperCase()) {
            return this.templateAPI.updateWelcomeText(templateId, welcomeText).then((response: IProtocolPacket): boolean => {
                if (response.result) {
                    this.store.dispatch({ type: TemplateActions.UPDATE_TEMPLATE_WELCOME_TEXT, welcomeText });
                }
                return response.result;
            });
        } else if (templateStatus === TemplateStates.PUBLISHED.toUpperCase()) {
            this.store.dispatch({ type: TemplateActions.UPDATE_TEMP_MODEL_WELCOME_TEXT, welcomeText });
            return this.$q.resolve(true);
        }
    }

    public updateQuestion = (templateId: string, sectionId: string, question: IQuestionDetails): ng.IPromise<boolean> => {
        const templateStatus = getCurrentTemplate(this.store.getState()).status;
        if (templateStatus === TemplateStates.DRAFT.toUpperCase()) {
            const reqObject = this.getQuestionRequest(question);
            return this.templateAPI.updateQuestion(templateId, sectionId, question.id, reqObject)
                .then((response: IProtocolResponse<ITemplateBuilderState>): boolean => {
                    if (response.result) {
                        if (question.questionType === QuestionTypes.Attribute.name) {
                            this.store.dispatch({ type: TemplateActions.DELETE_OPTIONS_BY_QUESTION_ID, questionId: question.id });
                        }
                        this.store.dispatch({ type: TemplateActions.SET_BUILDER_VIEW, builderStructure: response.data });
                        return true;
                    }
                    return false;
                });
        } else if (templateStatus === TemplateStates.PUBLISHED.toUpperCase()) {
            this.store.dispatch({
                type: TemplateActions.UPDATE_TEMP_MODEL_QUESTION,
                sectionId,
                question: {
                    questionId: question.id,
                    content: question.content,
                    friendlyName: question.friendlyName,
                    hint: question.hint,
                    description: question.description,
                    prePopulateResponse: question.prePopulateResponse,
                    linkAssessmentToInventory: question.linkAssessmentToInventory,
                },
                inventoryType: question.attributeJson.inventoryType,
            });
            return this.$q.resolve(true);
        }
    }

    public resolveInvalidQuestion = (templateId: string, sectionId: string, questionId: string, contract: ICreateQuestionParams): ng.IPromise<boolean> => {
        return this.templateAPI.resolveInvalidQuestion(templateId, sectionId, questionId, contract)
            .then((response: IProtocolResponse<ITemplateBuilderState>): boolean => {
                if (response && response.data) {
                    this.store.dispatch({ type: TemplateActions.SET_BUILDER_VIEW, builderStructure: response.data });
                }
                return response.result;
            });
    }

    public deleteTemplateSection = (templateId, sectionId): ng.IPromise<boolean> => {
        return this.templateAPI.deleteTemplateSection(templateId, sectionId)
            .then((response: IProtocolResponse<ITemplateBuilderState>): boolean => {
                if (response.data && response.data.sectionAllIds) {
                    this.store.dispatch({ type: TemplateActions.SET_BUILDER_VIEW, builderStructure: response.data });
                } else {
                    this.store.dispatch({ type: TemplateActions.RESET_BUILDER_VIEW });
                }
                return response.result;
            });
    }

    public deleteTemplateQuestion = (templateId, sectionId, questionId): ng.IPromise<boolean> => {
        return this.templateAPI.deleteTemplateQuestion(templateId, sectionId, questionId)
            .then((response: IProtocolResponse<ITemplateBuilderState>): boolean => {
                this.store.dispatch({ type: TemplateActions.SET_BUILDER_VIEW, builderStructure: response.data });
                return response.result;
            });
    }

    public discardDraftedTemplate = (templateId: string, templateRootId: string): ng.IPromise<boolean> => {
        return this.templateAPI.deleteTemplateDraft(templateId)
            .then((result: boolean): boolean => {
                if (result) {
                    this.store.dispatch({ type: TemplateActions.DISCARD_DRAFT, templateRootId });
                }
                return result;
            });
    }

    public publishTemplate = (templateId: string, templateRootId: string, payload: IPublishTemplatePayload = null): ng.IPromise<IProtocolResponse<void>> => {
        return this.templateAPI.publishTemplate(templateId, payload).then((response: IProtocolResponse<void>): IProtocolResponse<void> => {
            if (response.result) {
                this.store.dispatch({ type: TemplateActions.PUBLISH_DRAFT, templateId, templateRootId });
            }
            return response;
        });
    }

    public updateSection = (templateId: string, sectionId: string, section: { name: string }): ng.IPromise<boolean> => {
        const templateStatus = getCurrentTemplate(this.store.getState()).status;
        if (templateStatus === TemplateStates.PUBLISHED.toUpperCase()) {
            this.store.dispatch({ type: TemplateActions.UPDATE_TEMP_MODEL_SECTION, sectionId, sectionName: section.name });
            return this.$q.resolve(true);
        } else if (templateStatus === TemplateStates.DRAFT.toUpperCase()) {
            return this.templateAPI.updateTemplateSection(templateId, sectionId, section)
                .then((response: IAssessmentTemplateSection | null): boolean => {
                    if (response) {
                        this.store.dispatch({ type: TemplateActions.UPDATE_SECTION, sectionId, sectionName: section.name });
                        return true;
                    }
                    return false;
                });
        }
    }

    public updateNavigations(templateId: string, sectionId: string, questionId: string, navigations: IAssessmentNavigation[]): ng.IPromise<boolean> {
        return this.templateAPI.updateNavigations(templateId, sectionId, questionId, navigations)
            .then((response: IProtocolResponse<IAssessmentNavigation[]>) => {
                if (response.result) {
                    this.store.dispatch({ type: TemplateActions.UPDATE_NAVIGATIONS, questionId, navigations: response.data });
                }
                return response.result;
            });
    }

    public getInventorySchema = (): ng.IPromise<IProtocolResponse<IInventorySchemaV2[]>> => {
        return this.Inventory.getSchemasV2();
    }

    public fetchIncidentSchemas = (): ng.IPromise<IProtocolResponse<IIncidentSchema[]>> => {
        return this.IncidentSchemas.fetchSchemas();
    }

    public loadQuestionOptions(questionId: string, sectionId: string, templateId: string): Observable<IQuestionOptionDetails[]> {
        if (this.loadingOptions$[questionId]) {
            return this.loadingOptions$[questionId];
        }
        this.loadingOptions$[questionId] = this.templateAPI.getQuestionOptions(questionId, sectionId, templateId)
            .pipe(
                map((response: IProtocolResponse<IQuestionOptionDetails[]>) => {
                    if (response.result) {
                        return response.data;
                    }
                    return [];
                }),
                tap((questionOptions: IQuestionOptionDetails[]) => this.store.dispatch({ type: TemplateActions.SET_OPTIONS_BY_QUESTION_ID, questionId, options: questionOptions })),
                shareReplay(1),
            );
        return this.loadingOptions$[questionId];

    }

    public getQuestionOptions(questionId: string, sectionId: string, templateId: string): Observable<IQuestionOptionDetails[]> {
        return observableFromStore(this.store)
            .pipe(
                startWith(this.store.getState()),
                map((state: IStoreState) => getTemplateQuestionOptions(questionId)(state)),
                filter((questionOptions: IQuestionOptionDetails[]) => {
                    if (!questionOptions) {
                        this.loadQuestionOptions(questionId, sectionId, templateId).pipe(first()).subscribe();
                        return false;
                    }
                    return true;
                }),
            );
    }

    public updateQuestionRelationshipConfig(questionId: string, destinationInventoryQuestion: boolean): void {
        this.store.dispatch({ type: TemplateActions.UPDATE_QUESTION_RELATIONSHIP_CONFIGURATION, payload: { questionId, destinationInventoryQuestion } });
    }

    public updateInventoryRelationshipDraft(payload: IInventoryRelationshipPayload, messages: { success: string, error: string }): ng.IPromise<boolean> {
        return this.templateAPI.updateInventoryRelationship(payload).then((response: IProtocolResponse<boolean>): boolean => {
            if (response.result) {
                this.notificationService.alertSuccess(this.$rootScope.t("Success"), this.$rootScope.t(messages.success));
            } else {
                this.notificationService.alertError(this.$rootScope.t("Error"), this.$rootScope.t(messages.error));
            }
            return response.result;
        });
    }

    updateInventoryRelationshipPublished(payload: IInventoryRelationshipContract[]): void {
        this.store.dispatch({ type: TemplateActions.UPDATE_TEMP_MODEL_INVENTORY_RELATIONS, payload });
    }

    public getInventoryRelationship(templateId: string, sectionId: string, questionId: string): ng.IPromise<IProtocolResponse<IGetInventoryRelationshipPayload[]>> {
        return this.templateAPI.getInventoryRelationship(templateId, sectionId, questionId);
    }

    public updateTemplatesActiveState(templateRootId: string, activeState: string): void {
        this.store.dispatch({ type: TemplateActions.UPDATE_TEMPLATE_CARD_ACTIVE_STATE, templateRootId, activeState });
    }

    public archiveTemplate(templateRootId: string): ng.IPromise<void> {
        return this.templateAPI.archiveTemplate(templateRootId)
            .then((response: IProtocolResponse<void>): void => {
                if (response.result) {
                    this.store.dispatch({ type: TemplateActions.DISCARD_TEMPLATE, templateRootId });
                }
            });
    }

    public unarchiveTemplate(templateRootId: string): ng.IPromise<void> {
        return this.templateAPI.unarchiveTemplate(templateRootId)
            .then((response: IProtocolResponse<void>): void => {
                if (response.result) {
                    this.store.dispatch({ type: TemplateActions.DISCARD_TEMPLATE, templateRootId });
                }
            });
    }

    private getQuestionRequest(question: IQuestionDetails): IQuestionDetails {
        if (question.questionType === QuestionTypes.YesNo.name) {
            question.options = [
                { option: "Yes", optionType: QuestionResponseType.Default },
                { option: "No", optionType: QuestionResponseType.Default },
            ];
        }

        const questionReqObject: IQuestionDetails = {
            content: question.content,
            description: question.description,
            friendlyName: question.friendlyName,
            destinationInventoryQuestion: question.destinationInventoryQuestion,
            sourceInventoryQuestion: question.sourceInventoryQuestion,
            hint: question.hint,
            prePopulateResponse: question.prePopulateResponse,
            questionType: question.questionType,
            required: question.required || false,
            parentQuestionId: question.parentQuestionId || null,
            options: question.options
                ? this.parseQuestionOptions(question.attributeJson.optionHintEnabled, [...question.options])
                : [],
            linkAssessmentToInventory: question.linkAssessmentToInventory,
        };

        questionReqObject.options = questionReqObject.options ? questionReqObject.options : [];

        // Incident Question needs allowOther = true for prepopulation in Assessment
        if (question.questionType === QuestionTypeNames.Incident) {
            question.attributeJson.allowOther = true;
        }

        if (question.attributeJson.allowNotSure) {
            questionReqObject.options.push({
                option: "NotSure",
                optionType: QuestionResponseType.NotSure,
            });
        }

        if (question.attributeJson.allowNotApplicable) {
            questionReqObject.options.push({
                option: "NotApplicable",
                optionType: QuestionResponseType.NotApplicable,
            });
        }

        if (question.attributeJson.allowOther) {
            questionReqObject.options.push({
                option: "Other",
                optionType: QuestionResponseType.Others,
            });
        }

        questionReqObject["attributes"] = this.extractAttributes(question.attributeJson, ["required", "allowNotSure", "allowNotApplicable"]);

        if (question.id) {
            const previousQuestionState = getQuestionById(question.id)(this.store.getState());
            if (previousQuestionState.questionType === questionReqObject.questionType) {
                questionReqObject.options = this.remapQuestionOptions(questionReqObject.options, previousQuestionState.options);
            }
        }

        return questionReqObject;
    }

    // parsing question response object to match with the UI attributes
    private getQuestionResponse(question: IQuestionDetails, sectionId: string): IQuestionDetails {
        let updatedQuestion: IQuestionDetails = {
            attributeJson: this.parseAttributes(question.attributes),
            content: question.content,
            description: question.description,
            friendlyName: question.friendlyName,
            destinationInventoryQuestion: question.destinationInventoryQuestion,
            sourceInventoryQuestion: question.sourceInventoryQuestion,
            hint: question.hint,
            id: question.id,
            prePopulateResponse: question.prePopulateResponse,
            options: question.options,
            questionType: question.questionType,
            required: question.required,
            sequence: question.sequence,
            parentQuestionId: question.parentQuestionId,
            linkAssessmentToInventory: question.linkAssessmentToInventory,
        };

        const tempQuestion = getTempQuestionById(this.store.getState(), sectionId, question.id);
        if (tempQuestion) {
            updatedQuestion = {
                ...updatedQuestion,
                content: tempQuestion.content,
                description: tempQuestion.description,
                friendlyName: tempQuestion.friendlyName,
                hint: tempQuestion.hint,
            };
        }

        if (updatedQuestion.attributeJson.inventoryType) {
            const tempPrePopulateResponseQuestionId = getTempPrePopulateResponseQuestionIdByInventoryType(updatedQuestion.attributeJson.inventoryType)(this.store.getState());
            if (tempPrePopulateResponseQuestionId) {
                updatedQuestion.prePopulateResponse = tempPrePopulateResponseQuestionId === updatedQuestion.id;
            }
        }

        if (question.options) {
            for (let index = question.options.length - 1; index >= 0; index -= 1) {
                let isMatching = false;
                switch (question.options[index].optionType) {
                    case QuestionResponseType.NotApplicable:
                        updatedQuestion.attributeJson.allowNotApplicable = true;
                        isMatching = true;
                        break;
                    case QuestionResponseType.NotSure:
                        updatedQuestion.attributeJson.allowNotSure = true;
                        isMatching = true;
                        break;
                    case QuestionResponseType.Others:
                        updatedQuestion.attributeJson.allowOther = true;
                        isMatching = true;
                        break;
                }
                if (isMatching) {
                    question.options.splice(index, 1);
                }
            }
        }
        return updatedQuestion;
    }

    private parseQuestionOptions(optionHintEnabled: boolean, options: IQuestionOptionDetails[]): IQuestionOptionDetails[] | undefined {
        if (options && options.length) {
            forEach(options, (option: IQuestionOptionDetails) => {
                option.attributes = optionHintEnabled ? this.extractAttributes(option.attributeJson) : null;
            });
            return options;
        }
        return;
    }

    private remapQuestionOptions(newOptions: IQuestionOptionDetails[], previousOptions: IQuestionOptionDetails[]): IQuestionOptionDetails[] {
        return newOptions.map((option: IQuestionOptionDetails) => {
            if (option.option === "Yes") {
                const previousYes = previousOptions.find((prevOption: IQuestionOptionDetails) => prevOption.option === "Yes");
                return previousYes ? previousYes : option;
            }
            if (option.option === "No") {
                const previousNo = previousOptions.find((prevOption: IQuestionOptionDetails) => prevOption.option === "No");
                return previousNo ? previousNo : option;
            }
            if (option.optionType === QuestionResponseType.Default) {
                const previousOption = previousOptions.find((prevOption: IQuestionOptionDetails) => prevOption.option === option.option);
                return previousOption ? { ...option, id: previousOption.id } : { ...option, id: null };
            }
            if (option.optionType === QuestionResponseType.NotApplicable) {
                const previousNotApplicable = previousOptions.find((prevOption: IQuestionOptionDetails) => prevOption.optionType === QuestionResponseType.NotApplicable);
                return previousNotApplicable ? previousNotApplicable : option;
            }
            if (option.optionType === QuestionResponseType.NotSure) {
                const previousNotSure = previousOptions.find((prevOption: IQuestionOptionDetails) => prevOption.optionType === QuestionResponseType.NotSure);
                return previousNotSure ? previousNotSure : option;
            }
            if (option.optionType === QuestionResponseType.Others) {
                const previousOther = previousOptions.find((prevOption: IQuestionOptionDetails) => prevOption.optionType === QuestionResponseType.Others);
                return previousOther ? previousOther : option;
            }
            return option;
        });
    }
}
