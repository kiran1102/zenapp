// External Libraries
import {
    Injectable,
} from "@angular/core";
import {
    find,
    forEach,
} from "lodash";

// Constants
import { InventorySchemaIds } from "constants/inventory-config.constant";
import {
    QuestionResponseType,
    QuestionTypes,
    AttributeQuestionResponseType,
    QuestionTypeNames,
} from "constants/question-types.constant";

// Enums
import { QuestionAnswerTypes } from "enums/question-types.enum";

// Interfaces
import {
    IQuestionTypeAttributes,
    IQuestionTypeList,
    IQuestionTypeAttributesList,
} from "interfaces/question-type-attribute.interface";
import { IInventorySchemaV2 } from "interfaces/inventory.interface";
import {
    IQuestionOptionDetails,
    IAdditionalAttribute,
} from "interfaces/assessment-template-question.interface";
import { ILabelValue } from "interfaces/generic.interface";

// Services
import { Permissions } from "modules/shared/services/helper/permissions.service";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

@Injectable()
export class QuestionBuilderService {

    constructor(
        private readonly permissions: Permissions,
        private translatePipe: TranslatePipe,
    ) { }

    public getEnabledAttributes = (): IQuestionTypeAttributes => {
        return {
            DATE: {
                allowNotApplicable: true,
                allowNotSure: true,
            },
            MULTICHOICE: {
                allowJustification: true,
                allowNotApplicable: true,
                allowNotSure: true,
                allowOther: true,
                isMultiSelect: true,
                optionHintEnabled: true,
                requireJustification: false,
            },
            TEXTBOX: {
                allowNotApplicable: true,
                allowNotSure: true,
            },
            YESNO: {
                allowJustification: true,
                allowNotApplicable: true,
                allowNotSure: true,
                requireJustification: false,
            },
            INVENTORY: {
                allowNotApplicable: true,
                allowNotSure: true,
                allowOther: true,
                isMultiSelect: true,
            },
            PERSONALDATA: {
                allowNotApplicable: false,
                allowNotSure: false,
            },
            INCIDENT: {
                allowJustification: false,
                allowNotApplicable: false,
                allowNotSure: false,
                allowOther: false,
                isMultiSelect: false,
                optionHintEnabled: false,
                required: false,
                requireJustification: false,
            },
        };
    }

    initializeQuestionTypeList(responseType: string, hasInventoryQuestion: boolean, hasIncidentQuestion: boolean): IQuestionTypeList {
        const enabledAttributes = this.getEnabledAttributes();
        const questionTypeList: IQuestionTypeList = {
            MULTICHOICE: {
                label: this.translatePipe.transform(QuestionTypes.MultiChoice.label),
                value: QuestionTypes.MultiChoice.name,
                attributes: enabledAttributes.MULTICHOICE,
            },
            YESNO: {
                label: this.translatePipe.transform(QuestionTypes.YesNo.label),
                value: QuestionTypes.YesNo.name,
                attributes: enabledAttributes.YESNO,
            },
            TEXTBOX: {
                label: this.translatePipe.transform(QuestionTypes.TextBox.label),
                value: QuestionTypes.TextBox.name,
                attributes: enabledAttributes.TEXTBOX,
            },
            DATE: {
                label: this.translatePipe.transform(QuestionTypes.Date.label),
                value: QuestionTypes.Date.name,
                attributes: enabledAttributes.DATE,
            },
            STATEMENT: {
                label: this.translatePipe.transform(QuestionTypes.Statement.label),
                value: QuestionTypes.Statement.name,
            },
        };

        if (this.permissions.canShow("TemplateInventoryQuestion")) {
            questionTypeList[QuestionTypeNames.Inventory] = {
                label: this.translatePipe.transform(QuestionTypes.Inventory.label),
                value: QuestionTypes.Inventory.name,
                attributes: enabledAttributes.INVENTORY,
            };
        }

        if (hasInventoryQuestion || hasIncidentQuestion) {
            questionTypeList[QuestionTypeNames.Attribute] = {
                label: this.translatePipe.transform(QuestionTypes.Attribute.label),
                value: QuestionTypes.Attribute.name,
                attributes: this.getEnabledAttributesForAttributeQuestion(responseType),
            };
        }

        if (this.permissions.canShow("TemplatePersonalDataQuestion")) {
            questionTypeList[QuestionTypeNames.PersonalData] = {
                label: this.translatePipe.transform(QuestionTypes.PersonalData.label),
                value: QuestionTypes.PersonalData.name,
                attributes: enabledAttributes.PERSONALDATA,
            };
        }

        if (this.permissions.canShow("IncidentBreachManagement")) {
            questionTypeList[QuestionTypeNames.Incident] = {
                label: this.translatePipe.transform(QuestionTypes.Incident.label),
                value: QuestionTypes.Incident.name,
                attributes: enabledAttributes.INCIDENT,
            };
        }

        return questionTypeList;
    }

    getAdditionalAttributes(): IAdditionalAttribute[] {
        return [
            { label: this.translatePipe.transform("AllowOther"), value: "allowOther", tooltip: null },
            { label: this.translatePipe.transform("AllowNotSure"), value: "allowNotSure", tooltip: null },
            { label: this.translatePipe.transform("AllowNotApplicable"), value: "allowNotApplicable", tooltip: null },
            { label: this.translatePipe.transform("AllowMultiSelections"), value: "isMultiSelect", tooltip: this.translatePipe.transform("Template.DisabledMultiSelectTooltip") },
            { label: this.translatePipe.transform("AllowJustification", { Justification: this.translatePipe.transform("Justification") }), value: "allowJustification", tooltip: null },
            { label: this.translatePipe.transform("JustificationRequired"), value: "requireJustification", tooltip: null },
        ];
    }

    getDisplayAnwerTypes(): Array<ILabelValue<string>> {
        return [
            { label: this.translatePipe.transform("Button"), value: QuestionAnswerTypes.Button },
            { label: this.translatePipe.transform("Dropdown"), value: QuestionAnswerTypes.Dropdown },
        ];
    }

    acceptedInventoryTypes(inventoryType: IInventorySchemaV2): boolean {
        return inventoryType.id === InventorySchemaIds.Assets
            || inventoryType.id === InventorySchemaIds.Processes
            || inventoryType.id === InventorySchemaIds.Vendors
            || inventoryType.id === InventorySchemaIds.Entities;
    }

    getEmptyOption = (): IQuestionOptionDetails => {
        return {
            attributeJson: {},
            id: "",
            option: "",
            optionType: QuestionResponseType.Default,
            focusEnabled: true,
        };
    }

    preSelectInventoryType(inventoryTypes: IInventorySchemaV2[], questionInventoryType: string): IInventorySchemaV2 {
        return find<IInventorySchemaV2>(inventoryTypes, (type: IInventorySchemaV2) => type.id === questionInventoryType);
    }

    extractQuestionOptions = (optionHintEnabled: boolean, options: IQuestionOptionDetails[]): IQuestionOptionDetails[] | undefined => {
        forEach(options, (option: IQuestionOptionDetails): void => {
            if (optionHintEnabled) {
                option.attributeJson = JSON.parse(option.attributes);
                option.Hint = option.attributeJson ? option.attributeJson["hint"] : "";
            }
        });
        return options;
    }

    getEnabledAttributesForAttributeQuestion(responseType?: string): IQuestionTypeAttributesList {
        switch (responseType) {
            case AttributeQuestionResponseType.TEXT:
                return {
                    allowNotApplicable: true,
                    allowNotSure: true,
                };
            case AttributeQuestionResponseType.SINGLE_SELECT:
                return {
                    allowJustification: true,
                    allowNotApplicable: true,
                    allowNotSure: true,
                    requireJustification: false,
                };
            case AttributeQuestionResponseType.MULTI_SELECT:
                return {
                    allowJustification: true,
                    allowNotApplicable: true,
                    allowNotSure: true,
                    requireJustification: false,
                };
            default:
                return {
                    allowNotApplicable: true,
                    allowNotSure: true,
                };
        }
    }
}
