export default class OTIconService {
    private readonly iconList = [
        "ot-archive",
        "ot-ban",
        "ot-child",
        "ot-code",
        "ot-cog",
        "ot-copyright",
        "ot-desktop",
        "ot-envelope",
        "ot-exclamation",
        "ot-gavel",
        "ot-home",
        "ot-info",
        "ot-lock",
        "ot-unlock",
        "ot-mobile",
        "ot-shopping-cart",
        "ot-star",
    ];

    public getIconList = (): string[] => {
        return this.iconList;
    }
}
