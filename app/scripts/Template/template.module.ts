
declare var angular: angular.IAngularStatic;

import { routes } from "./template.routes";

import { MultiselectButtonQuestion } from "generalcomponent/assessment-templates/multichoice-question/multiselect-button-question.component";
import { MultiselectDropdownQuestion } from "generalcomponent/assessment-templates/multichoice-question/multiselect-dropdown-question.component";
import { QuestionDateComponent } from "generalcomponent/assessment-templates/question-date/question-date.component";
import { QuestionTextBoxComponent } from "generalcomponent/assessment-templates/question-textbox/question-textbox.component";
import { QuestionYesNoComponent } from "generalcomponent/assessment-templates/question-yes-no/question-yes-no.component";
import { SingleSelectButtonQuestion } from "generalcomponent/assessment-templates/multichoice-question/single-select-button-question.component";
import { SingleSelectDropdownQuestion } from "generalcomponent/assessment-templates/multichoice-question/single-select-dropdown.component";
import { TemplateQuestionPreviewComponent } from "generalcomponent/assessment-templates/question-preview/template-question-preview.component";
import { ResolveInvalidQuestionModal } from "generalcomponent/assessment-templates/resolve-invalid-question-modal/resolve-invalid-question-modal.component";

import OTIconService from "./Services/icon.service";
import TemplateAction from "./Services/actions/template-action.service";
import { TemplateAPI } from "./Services/api/template-api.service";
import { TemplateService } from "./Services/templates.service";
import { TemplateBuilderService } from "./Services/template-builder.service";

angular
    .module("zen.templatesmodule", [
        // Angular modules
        "ui.bootstrap",
        "ui.router",
        "ui.tree",
        "ngMaterial",
        "ngSanitize",
        "ngStorage",

        // Custom modules
        "zen.identity",

        // External Modules
        "dndLists",
    ])
    .config(routes)

    .component("multiselectButtonQuestion", MultiselectButtonQuestion)
    .component("multiselectDropdownQuestion", MultiselectDropdownQuestion)
    .component("questionDate", QuestionDateComponent)
    .component("questionTextbox", QuestionTextBoxComponent)
    .component("questionYesNo", QuestionYesNoComponent)
    .component("resolveInvalidQuestionModal", ResolveInvalidQuestionModal)
    .component("singleSelectButtonQuestion", SingleSelectButtonQuestion)
    .component("singleSelectDropdownQuestion", SingleSelectDropdownQuestion)
    .component("templateQuestionPreview", TemplateQuestionPreviewComponent)

    .service("OTIconService", OTIconService)
    .service("TemplateAction", TemplateAction)
    .service("TemplateAPI", TemplateAPI)
    .service("TemplateService", TemplateService)
    .service("TemplateBuilderService", TemplateBuilderService)
    ;
