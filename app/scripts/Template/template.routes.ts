// Services
import { Permissions } from "modules/shared/services/helper/permissions.service";
import { GlobalSidebarService } from "modules/global-sidebar/services/one-global-sidebar.service";

// Interfaces
import { IStringMap } from "interfaces/generic.interface";

export function routes($stateProvider: ng.ui.IStateProvider, $urlRouterProvider: ng.ui.IUrlRouterProvider) {

    $urlRouterProvider.when("/pia/templates", "/pia/templates/");
    $urlRouterProvider.when("/pia/templates/archive", "/pia/templates/archive/");

    $stateProvider
        .state("zen.app.pia.module.templatesv2", {
            abstract: true,
            url: "templates/",
            resolve: {
                TemplatePermission: [
                    "GeneralData", "Permissions",
                    (GeneralData: IStringMap<any>, permissions: Permissions): boolean => {
                        if ((permissions.canShow("TemplatesV2") && permissions.canShow("TemplatesView"))
                            || permissions.canShow("DMAssessmentV2")
                            || permissions.canShow("VendorRiskManagement")
                        ) {
                            return true;
                        } else {
                            permissions.goToFallback();
                        }
                        return false;
                    },
                ],
            },
            views: {
                module: {
                    template: "<ui-view></ui-view>",
                    controller: [
                        "GlobalSidebarService",
                        (globalSidebarService: GlobalSidebarService) => {
                            globalSidebarService.setAAMenu();
                        },
                    ],
                },
            },
        })
        .state("zen.app.pia.module.templatesv2.list", {
            url: "",
            template: "<downgrade-custom-templates-component></downgrade-custom-templates-component>",
        })
        .state("zen.app.pia.module.templatesv2.archive", {
            url: "archive/",
            template: require("piaviews/Templates/pia_templates_archive.jade"),
            controller: "TemplatesArchiveCtrl",
        })
        .state("zen.app.pia.module.templatesv2.studio", {
            url: "studio/",
            abstract: true,
            template: "<ui-view></ui-view>",
        })
        .state("zen.app.pia.module.templatesv2.studio.gallery", {
            url: "gallery",
            template: "<downgrade-gallery-templates-component></downgrade-gallery-templates-component>",
        })
        .state("zen.app.pia.module.templatesv2.studio.manage", {
            url: "manage/:templateId/:version",
            template: "<downgrade-template-page></downgrade-template-page>",
        });
}

routes.$inject = ["$stateProvider", "$urlRouterProvider"];
