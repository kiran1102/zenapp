import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { ReactiveFormsModule } from "@angular/forms";
import { FormsModule } from "@angular/forms";

// Modules
import { HttpClientModule } from "@angular/common/http";
import { SharedModule } from "sharedModules/shared.module";
import { TranslateModule } from "@ngx-translate/core";
import { VitreusModule } from "@onetrust/vitreus";
import { OTPipesModule } from "modules/pipes/pipes.module";
import { UpgradedLegacyModule } from "sharedModules/upgrade/upgraded-legacy.module";

// Services
import { SystemTrayModalService } from "sharedModules/modules/system-tray/system-tray-modal/system-tray-modal.service";
import { SystemSubtasksApiService } from "./system-tray-subtasks-api.service";
import { SystemSubTasksService } from "./system-tray-subtasks.service";
import { SystemTraySubtasksDetailsModule } from "./system-tray-subtasks-details/system-tray-subtasks-details.module";

// Components
import { SystemTraySubtasksComponent } from "./system-tray-subtasks.component";
import { SystemTraySubtasksTableComponent } from "./system-tray-subtasks-table/system-tray-subtasks-table.component";

@NgModule({
    imports: [
        HttpClientModule,
        SharedModule,
        CommonModule,
        UpgradedLegacyModule,
        FormsModule,
        SystemTraySubtasksDetailsModule,
        ReactiveFormsModule,
        TranslateModule,
        VitreusModule,
        OTPipesModule,
    ],
    declarations: [
        SystemTraySubtasksComponent,
        SystemTraySubtasksTableComponent,
    ],
    exports: [
        SystemTraySubtasksComponent,
        SystemTraySubtasksTableComponent,
    ],
    providers: [
        SystemTrayModalService,
        SystemSubTasksService,
        SystemSubtasksApiService,
    ],
})
export class SystemTraySubtasksModule {}
