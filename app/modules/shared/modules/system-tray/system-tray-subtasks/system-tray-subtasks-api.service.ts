import { Injectable } from "@angular/core";

// Rxjs
import { Observable } from "rxjs";

// Services
import { HttpClient, HttpParams } from "@angular/common/http";

// Interfaces
import { IPageableOf, IPaginationParams } from "interfaces/pagination.interface";
import { IIncidentSubTasksRecord } from "incidentModule/shared/interfaces/incident-subtasks.interface";
import { IIncidentAddEditSubTasksUpdateRequest } from "incidentModule/shared/interfaces/incident-modal.interface";

@Injectable()
export class SystemSubtasksApiService {
    constructor(
        private http: HttpClient,
    ) {}

    fetchSystemSubtasks(assigneeId: string, paginationParams: IPaginationParams<any>): Observable<IPageableOf<IIncidentSubTasksRecord>> {
        const params = new HttpParams()
            .set("page", paginationParams.page.toString())
            .set("size", paginationParams.size.toString());
        return this.http.post<IPageableOf<IIncidentSubTasksRecord>>(`/api/incident/v1/incidents/subtasks/search`, {
                filters: { assigneeId },
            }, { params });
    }

    fetchSystemSubtaskDetails(subtaskId: string): Observable<IIncidentSubTasksRecord> {
        return this.http.get<IIncidentSubTasksRecord>(`/api/incident/v1/incidents/subtasks/${subtaskId}/subtasks`);
    }

    changeSubtaskStatus(subtaskId: string, status: string): Observable<IIncidentSubTasksRecord> {
        return this.http.post<IIncidentSubTasksRecord>(
            `/api/incident/v1/incidents/subtasks/${subtaskId}/status`,
            { status },
        );
    }

    changeSubtaskDetails(subtaskId: string, subtaskDetails: IIncidentAddEditSubTasksUpdateRequest): Observable<IIncidentSubTasksRecord> {
        return this.http.put<IIncidentSubTasksRecord>(
            `/api/incident/v1/incidents/subtasks/${subtaskId}/subtasks`,
            { ...subtaskDetails },
        );
    }
}
