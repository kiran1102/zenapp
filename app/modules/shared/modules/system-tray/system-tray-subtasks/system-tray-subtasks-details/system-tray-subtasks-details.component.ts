import {
    Component,
    Output,
    EventEmitter,
    Input,
} from "@angular/core";
import {
    FormGroup,
    FormBuilder,
    Validators,
} from "@angular/forms";

// 3rd Party
import { Subject, Subscription } from "rxjs";
import { take, takeUntil } from "rxjs/operators";

// Services
import { SystemSubTasksService } from "../system-tray-subtasks.service";
import { Permissions } from "sharedModules/services/helper/permissions.service";
import { Principal } from "sharedModules/services/helper/principal.service";
import { TranslateService } from "@ngx-translate/core";
import { TimeStamp } from "oneServices/time-stamp.service";
import { SystemTrayModalService } from "sharedModules/modules/system-tray/system-tray-modal/system-tray-modal.service";

// Interfaces
import { IIncidentSubTasksRecord } from "incidentModule/shared/interfaces/incident-subtasks.interface";
import { ISystemSubtaskInfo } from "sharedModules/interfaces/system-tray.interface";
import { IOtOrgUserOutput } from "sharedModules/components/ot-org-user/ot-org-user.interface";

// Const / Enums
import { IncidentSubTasksStateKeys } from "incidentModule/shared/enums/incident-translation-keys.enums";
import { OrgUserTraversal } from "enums/org-user-dropdown.enum";
import { IDateTimeOutputModel } from "@onetrust/vitreus";

@Component({
    selector: "system-tray-subtasks-details",
    templateUrl: "./system-tray-subtasks-details.component.html",
})
export class SystemTraySubtasksDetailsComponent {
    @Input() set subtaskInfo(info: ISystemSubtaskInfo) {
        this.loadingDetails = true;
        this.pageInfo = { current: info.index, count: info.count };
        this.systemSubTasksService.getSystemSubTasksDetails(info.id)
            .pipe(take(1))
            .subscribe((subtaskDetails: IIncidentSubTasksRecord): void => {
                this.details = subtaskDetails;
                this.buildForm();
                this.loadingDetails = false;
            });
    }

    @Output() returnToList = new EventEmitter<void>();
    @Output() nextSubtaskEvent = new EventEmitter<void>();
    @Output() previousSubtaskEvent = new EventEmitter<void>();

    user = this.principal.getUser();
    canEdit = this.permissions.canShow("IncidentSubtaskEdit");
    editing = false;
    hasEdits = false;
    loadingDetails = true;
    details: IIncidentSubTasksRecord;
    pageInfo: { current: number, count: number };
    incidentSubTasksStateKeys = IncidentSubTasksStateKeys;
    commentInput = "";
    form: FormGroup;
    is24Hour = this.timeStamp.getIs24Hour();
    orgUserTraversal = OrgUserTraversal;
    disableUntilDate = this.timeStamp.getDisableUntilDate();
    format = this.timeStamp.getDatePickerDateFormat();
    customModules = [
        [
            "bold",
            "italic",
            "underline",
            "strike",
            "blockquote",
            "code-block",
            "link",
            "image",
        ],
        [
            {
                list: "ordered",
            },
            {
                list: "bullet",
            },
        ],
        [
            {
                indent: "-1",
            },
            {
                indent: "+1",
            },
        ],
    ];

    private formSubscription$: Subscription;
    private translations = {};
    private keys = [
        "MarkAsComplete",
        "SubtaskName",
        "SubtaskDescription",
        "Assignee",
        "DueDate",
        "Of",
        "SubTasks",
        "Comments",
        "Comment",
        "BackToList",
        "Cancel",
        "Save",
        "DeadlineDate",
        "Hours",
        "Minutes",
        "DeadlineTime",
    ];
    private destroy$ = new Subject();

    constructor(
        private formBuilder: FormBuilder,
        private principal: Principal,
        public permissions: Permissions,
        private systemSubTasksService: SystemSubTasksService,
        private systemTrayModalService: SystemTrayModalService,
        private translate: TranslateService,
        private timeStamp: TimeStamp,
    ) {
        this.translate
            .stream(this.keys)
            .subscribe((values) => (this.translations = values));
    }

    ngOnDestroy() {
        this.formSubscription$.unsubscribe();
        this.systemTrayModalService.disableClose(false);
        this.destroy$.next(true);
        this.destroy$.complete();
    }

    buildForm() {
        this.form = this.formBuilder.group({
            name: [ this.details.name, [ Validators.required ]],
            description: [ this.details.description, [ Validators.required ] ],
            assigneeId: [ this.details.assigneeId ],
            deadline: [ this.details.deadline ],
        });

        this.formSubscription$ = this.form.valueChanges.subscribe((val) => {
            this.hasEdits = true;
        });
    }

    toggleEdit() {
        if (this.permissions.canShow("IncidentSubtaskEdit")) {
            this.buildForm();
            this.editing = !this.editing;
            this.systemTrayModalService.disableClose(true);
        }
    }

    backToList() {
        this.returnToList.emit();
    }

    previousSubtask() {
        if (this.pageInfo.current > 0) {
            this.cancelEdits();
            this.previousSubtaskEvent.emit();
        }
    }

    nextSubtask() {
        if (this.pageInfo.current < this.pageInfo.count) {
            this.cancelEdits();
            this.nextSubtaskEvent.emit();
        }
    }

    toggleMarkAsComplete(complete: boolean) {
        const status = complete ? IncidentSubTasksStateKeys.Closed : IncidentSubTasksStateKeys.Open;
        this.systemSubTasksService.markAsComplete(this.details.id, status)
            .pipe(take(1))
            .subscribe((subtask: IIncidentSubTasksRecord) => {
                this.details = subtask;
            });
    }

    subtaskDescriptionInputChanged(description: string) {
        this.form.get("description").setValue(description);
        this.hasEdits = true;
    }

    selectAssignee(selectedOption: IOtOrgUserOutput) {
        selectedOption.selection
            ? this.form.get("assigneeId").setValue(selectedOption.selection.Id)
            : this.form.get("assigneeId").setValue(null);
        this.hasEdits = true;
    }

    selectDeadline(dateTimeValue: IDateTimeOutputModel): void {
        this.form.get("deadline").setValue(dateTimeValue.dateTime);
    }

    canSave(): boolean {
        return this.hasEdits && this.form.get("name").value && this.form.get("description").value;
    }

    cancelEdits() {
        this.hasEdits = false;
        this.systemTrayModalService.disableClose(false);
        this.editing = false;
        this.formSubscription$.unsubscribe();
        this.loadingDetails = false;
    }

    saveEdits() {
        const subtaskDetails = {
            name: this.form.get("name").value ? this.form.get("name").value : null,
            assigneeId: this.form.get("assigneeId").value ? this.form.get("assigneeId").value : null,
            description: this.form.get("description").value,
            incidentId: this.details.incidentId,
            status: this.details.status,
            deadline: "",
        };
        const deadline = this.form.get("deadline").value;
        if (deadline) {
            subtaskDetails.deadline = this.timeStamp.incrementISODateMillisecond(deadline);
        }

        this.loadingDetails = true;
        this.systemSubTasksService.updateSubtaskDetails(this.details.id, subtaskDetails)
            .pipe(takeUntil(this.destroy$))
            .subscribe((subtask: IIncidentSubTasksRecord) => {
                this.details = subtask;
                this.cancelEdits();
            });
    }

    commentInputChanged(comment: string) {
        this.commentInput = comment;
    }

    submitComment() {

    }
}
