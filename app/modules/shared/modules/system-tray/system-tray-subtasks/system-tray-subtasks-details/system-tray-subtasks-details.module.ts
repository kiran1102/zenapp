import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { ReactiveFormsModule } from "@angular/forms";
import { FormsModule } from "@angular/forms";

// Modules
import { SharedModule } from "sharedModules/shared.module";
import { TranslateModule } from "@ngx-translate/core";
import { VitreusModule } from "@onetrust/vitreus";
import { OTPipesModule } from "modules/pipes/pipes.module";
import { UpgradedLegacyModule } from "sharedModules/upgrade/upgraded-legacy.module";

// Services
import { SystemTrayModalService } from "sharedModules/modules/system-tray/system-tray-modal/system-tray-modal.service";
import { SystemSubtasksApiService } from "../system-tray-subtasks-api.service";
import { SystemSubTasksService } from "../system-tray-subtasks.service";

// Components
import { SystemTraySubtasksDetailsComponent } from "./system-tray-subtasks-details.component";

@NgModule({
    imports: [
        SharedModule,
        CommonModule,
        UpgradedLegacyModule,
        FormsModule,
        ReactiveFormsModule,
        TranslateModule,
        VitreusModule,
        OTPipesModule,
    ],
    declarations: [
        SystemTraySubtasksDetailsComponent,
    ],
    exports: [
        SystemTraySubtasksDetailsComponent,
    ],
    providers: [
        SystemTrayModalService,
        SystemSubTasksService,
        SystemSubtasksApiService,
    ],
})
export class SystemTraySubtasksDetailsModule {}
