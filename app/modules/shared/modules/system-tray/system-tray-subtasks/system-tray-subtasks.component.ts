// Angular
import { Component } from "@angular/core";

// Services
import { SystemTrayModalService } from "sharedModules/modules/system-tray/system-tray-modal/system-tray-modal.service";
import { Permissions } from "sharedModules/services/helper/permissions.service";

// Interfaces
import { ISystemSubtaskInfo } from "sharedModules/interfaces/system-tray.interface";
import { SystemSubTasksService } from "./system-tray-subtasks.service";

@Component({
    selector: "system-tray-subtasks",
    templateUrl: "./system-tray-subtasks.component.html",
})
export class SystemTraySubtasksComponent {
    selectedSubtaskInfo: ISystemSubtaskInfo;

    constructor(
        public permissions: Permissions,
        public systemSubTasksService: SystemSubTasksService,
        public systemTrayModalService: SystemTrayModalService,
    ) { }

    selectedSubtask(subtaskInfo: ISystemSubtaskInfo) {
        this.selectedSubtaskInfo = subtaskInfo;
        this.systemTrayModalService.toggleNav(true, "SubtaskDetail");
    }

    returnToList() {
        this.selectedSubtaskInfo = null;
        this.systemTrayModalService.toggleNav();
    }

    nextSubtask() {
        const subtask = this.systemSubTasksService.systemSubTasksList$.value[this.selectedSubtaskInfo.index + 1];
        this.selectedSubtaskInfo = {
            id: subtask.id,
            index: this.selectedSubtaskInfo.index + 1,
            count: this.systemSubTasksService.systemSubTasksMetaData$.value.numberOfElements,
        };
    }

    previousSubtask() {
        const subtask = this.systemSubTasksService.systemSubTasksList$.value[this.selectedSubtaskInfo.index - 1];
        this.selectedSubtaskInfo = {
            id: subtask.id,
            index: this.selectedSubtaskInfo.index - 1,
            count: this.systemSubTasksService.systemSubTasksMetaData$.value.numberOfElements,
        };
    }
}
