import {
    Component,
    Output,
    EventEmitter,
} from "@angular/core";

// Interfaces
import { IIncidentSubTasksRecord, IIncidentSubTasksRecordTable } from "incidentModule/shared/interfaces/incident-subtasks.interface";

// Services
import { TimeStamp } from "oneServices/time-stamp.service";
import { SystemSubTasksService } from "../system-tray-subtasks.service";
import { Permissions } from "sharedModules/services/helper/permissions.service";
import { Principal } from "modules/shared/services/helper/principal.service";
import { TranslateService } from "@ngx-translate/core";

// 3rd Party
import { Subject } from "rxjs";
import { takeUntil, filter } from "rxjs/operators";

// Interfaces
import { IPageableMeta } from "interfaces/pagination.interface";
import { ISystemSubtaskInfo } from "sharedModules/interfaces/system-tray.interface";

// Enums
import { TableColumnTypes } from "enums/data-table.enum";
import { IncidentTableColumnNames } from "incidentModule/shared/enums/incident-actions.enums";
import { IDataTableColumn } from "interfaces/tables.interface";
import { IncidentSubTasksStatusColorMap } from "incidentModule/shared/constants/incident-badge-color.constants";
import { IncidentSubTasksStateKeys } from "incidentModule/shared/enums/incident-translation-keys.enums";

@Component({
    selector: "system-tray-subtasks-table",
    templateUrl: "./system-tray-subtasks-table.component.html",
})
export class SystemTraySubtasksTableComponent {
    @Output() selectedSubtask = new EventEmitter<ISystemSubtaskInfo>();

    user = this.principal.getUser();
    loadingSubTasks = true;
    table: IIncidentSubTasksRecordTable;
    tableMetaData: IPageableMeta;
    colBordered = false;
    resizableCol = false;
    responsiveTable = true;
    rowBordered = true;
    rowNoHover = false;
    rowStriped = false;
    sortKey = "";
    sortOrder = "";
    menuClass: string;
    sortable = false;
    truncateCellContent = true;
    wrapCellContent = false;
    disabledRowMap = {};
    selectedRowMap = {};
    tableColumnTypes = TableColumnTypes;
    showDeadline = this.timeStamp.displayDate() && this.timeStamp.displayTime();
    incidentSubTasksStatusColorMap = IncidentSubTasksStatusColorMap;
    incidentSubTasksStateKeys = IncidentSubTasksStateKeys;
    defaultPagination = { page: 0, size: 10};
    paginationParams = this.defaultPagination;
    destroy$ = new Subject();

    private translations = {};
    private keys = [
        IncidentSubTasksStateKeys.Open,
        IncidentSubTasksStateKeys.Closed,
        "Name",
        "Description",
        "Status",
        "DueDate",
        "NoSubtasksAssigned",
    ];

    constructor(
        private principal: Principal,
        public permissions: Permissions,
        private systemSubTasksService: SystemSubTasksService,
        private translate: TranslateService,
        private readonly timeStamp: TimeStamp,
    ) {
        this.translate
            .stream(this.keys)
            .subscribe((values) => (this.translations = values));
    }

    ngOnInit(): void {
        this.systemSubTasksService.getSystemSubTasks(this.user.Id, this.paginationParams);
        this.systemSubTasksService.systemSubTasksList$
            .pipe(
                filter((subtaskList) => !!subtaskList),
                takeUntil(this.destroy$),
            ).subscribe((subtaskList: IIncidentSubTasksRecord[]) => {
                this.tableMetaData = this.systemSubTasksService.systemSubTasksMetaData$.value;
                this.configureTable(subtaskList);
                this.loadingSubTasks = false;
            });
    }

    ngOnDestroy(): void {
        this.destroy$.next(true);
        this.destroy$.complete();
    }

    columnTrackBy(index: number, column: IDataTableColumn): string {
        return column.id;
    }

    configureTable(records: IIncidentSubTasksRecord[]) {
        this.table = {
            rows: records,
            columns: [
                {
                    name: this.translations["Name"],
                    sortKey: IncidentTableColumnNames.INCIDENT_NAME_COLUMN,
                    type: TableColumnTypes.Link,
                    cellValueKey: IncidentTableColumnNames.INCIDENT_NAME_COLUMN,
                },
                {
                    name: this.translations["Status"],
                    sortKey: IncidentTableColumnNames.INCIDENT_STATUS_NAME,
                    type: TableColumnTypes.Status,
                    cellValueKey: IncidentTableColumnNames.INCIDENT_STATUS_NAME,
                },
                {
                    name: this.translations["DueDate"],
                    sortKey: IncidentTableColumnNames.INCIDENT_DEADLINE_COLUMN,
                    type: TableColumnTypes.Deadline,
                    cellValueKey: IncidentTableColumnNames.INCIDENT_DEADLINE_COLUMN,
                },
                {
                    name: "",
                    sortKey: IncidentTableColumnNames.INCIDENT_SUBTASK_ACTION,
                    type: TableColumnTypes.Action,
                    cellValueKey: IncidentTableColumnNames.INCIDENT_SUBTASK_ACTION,
                },
            ],
        };
    }

    onPaginationChange(event: number = 0): void {
        this.paginationParams = { ...this.paginationParams, page: event };
        this.systemSubTasksService.getSystemSubTasks(this.user.Id, this.paginationParams);
        this.loadingSubTasks = true;
    }

    checkIfRowIsDisabled = (row: IIncidentSubTasksRecord): boolean => !!this.disabledRowMap[row.id];
    checkIfRowIsSelected = (row: IIncidentSubTasksRecord): boolean => !!this.selectedRowMap[row.id];

    editSubtask(index: number, row: IIncidentSubTasksRecord): void {
        this.selectedSubtask.emit({ id: row.id, index, count: this.tableMetaData.numberOfElements });
    }

    subTasksStatusName(name: string): string {
        switch (name) {
            case IncidentSubTasksStateKeys.Open:
                return this.translations[IncidentSubTasksStateKeys.Open];
            case IncidentSubTasksStateKeys.Closed:
                return this.translations[IncidentSubTasksStateKeys.Closed];
            default:
                return name;
        }
    }
}
