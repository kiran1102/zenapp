import { Injectable } from "@angular/core";

// Rxjs
import { take } from "rxjs/operators";

// Services
import { SystemSubtasksApiService } from "./system-tray-subtasks-api.service";

// Interfaces
import { IIncidentSubTasksRecord } from "incidentModule/shared/interfaces/incident-subtasks.interface";
import { IPageableMeta,
    IPaginationParams,
    IPageableOf,
} from "interfaces/pagination.interface";
import { IIncidentAddEditSubTasksUpdateRequest } from "incidentModule/shared/interfaces/incident-modal.interface";

// Const / Enums
import { IncidentSubTasksStateKeys } from "incidentModule/shared/enums/incident-translation-keys.enums";

// 3rd Party
import { BehaviorSubject, Observable } from "rxjs";

@Injectable()
export class SystemSubTasksService {
    systemSubTasksList$ = new BehaviorSubject<IIncidentSubTasksRecord[]>(null);
    systemSubTasksMetaData$ = new BehaviorSubject<IPageableMeta>(null);

    constructor(
        private systemSubtasksApiService: SystemSubtasksApiService,
    ) { }

    reset(): void {
        this.systemSubTasksList$.next(null);
        this.systemSubTasksMetaData$.next(null);
    }

    getSystemSubTasks(assigneeId: string, paginationParams: IPaginationParams<any>): void {
        this.systemSubtasksApiService.fetchSystemSubtasks(assigneeId, paginationParams)
            .pipe(take(1))
            .subscribe((data: IPageableOf<IIncidentSubTasksRecord>): void => {
                if (data) {
                    const subtasksListData = { ...data };
                    this.systemSubTasksMetaData$.next(
                        {
                            first: data.first,
                            last: data.last,
                            number: data.number,
                            numberOfElements: data.numberOfElements,
                            size: data.size,
                            sort: [...data.sort],
                            totalElements: data.totalElements,
                            totalPages: data.totalPages,
                        },
                    );
                    this.systemSubTasksList$.next(subtasksListData.content);
                }
            });
    }

    getSystemSubTasksDetails(subtaskId: string): Observable<IIncidentSubTasksRecord> {
        return this.systemSubtasksApiService.fetchSystemSubtaskDetails(subtaskId);
    }

    markAsComplete(subtaskId: string, status: IncidentSubTasksStateKeys): Observable<IIncidentSubTasksRecord> {
        return this.systemSubtasksApiService.changeSubtaskStatus(subtaskId, status);
    }

    updateSubtaskDetails(subtaskId: string, subtaskDetails: IIncidentAddEditSubTasksUpdateRequest) {
        return this.systemSubtasksApiService.changeSubtaskDetails(subtaskId, subtaskDetails);
    }
}
