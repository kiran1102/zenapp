// Angular
import {
    Component,
    Inject,
    OnInit,
} from "@angular/core";
import {
    FormGroup,
    FormBuilder,
    Validators,
    FormControl,
} from "@angular/forms";

// 3rd party
import { merge, map, findIndex } from "lodash";
import { IMyDateModel, IMyDate } from "mydatepicker";

// Redux / Tokens
import { StoreToken } from "tokens/redux-store.token";
import { getCurrentUser } from "oneRedux/reducers/user.reducer";

// Services
import { TranslateService } from "@ngx-translate/core";
import { TasksService } from "sharedServices/tasks.service";
import { TimeStamp } from "oneServices/time-stamp.service";
import { AttachmentService } from "oneServices/attachment.service";
import { StateService } from "@uirouter/core";

// Interfaces
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import { IStore } from "interfaces/redux.interface";
import {
    ITask,
    ITaskDetail,
    ITaskFilterOption,
} from "interfaces/tasks.interface";
import { IDropdownOption } from "interfaces/one-dropdown.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";
import { IPageableMeta } from "interfaces/pagination.interface";

// Enums and Constants
import { TasksViews } from "enums/tasks-view.enum";
import {
    TaskStateIds,
    TASK_STATE_CLASSES,
    TASK_STATES,
    TaskStateKeys,
    TaskTypes,
    TaskDetailType,
} from "constants/tasks.constant";

// Rxjs
import { Subject } from "rxjs";

@Component({
    selector: "system-tray-notifications",
    templateUrl: "./system-tray-notifications.component.html",
})
export class SystemTrayNotificationsComponent implements OnInit {
    task = this.getEmptyTask();
    disabledUntil = this.getDisableUntilDate();
    disabled = false;

    taskView: TasksViews;

    dueDateModel: string;
    reminderDateModel: string;
    validReminder = false;
    isDateDisabledCheck = true;
    taskManageForm: FormGroup;

    format = this.timeStamp.getDatePickerDateFormat();

    tasks: ITask[] = [];

    taskColumns = [
        {
            columnName: "Tasks.Name",
            cellValueKey: "name",
            cellClass: "text-left",
        },
        {
            columnName: "Tasks.TaskDescription",
            cellValueKey: "description",
            cellClass: "text-left",
        },
        {
            columnName: "Tasks.Status",
            cellValueKey: "stateName",
            cellClass: "text-center",
        },
        {
            columnName: "Tasks.DueDate",
            cellValueKey: "dueDate",
            cellClass: "text-center",
        },
    ];

    ready = false;
    errorMessage: string;
    hasTaskInfoError = false;

    pageSize = 10000;
    pageNumber = 0; // Zero-based for API (1-based on user presentation)

    taskStateIds = TaskStateIds;
    taskStateKeys = TaskStateKeys;

    otModalCloseEvent: Subject<boolean>;

    pageMetadata: IPageableMeta = {
        first: false,
        last: false,
        number: 0,
        numberOfElements: 0,
        size: 10000,
        sort: [],
        totalElements: 0,
        totalPages: 1,
        itemCount: 0,
    };
    translations = {};
    translateKeys = [
        "ControlExport",
        "Tasks.Complete",
        "Tasks.DeleteTask",
        "Tasks.Failed",
        "Tasks.MarkTaskComplete",
        "Tasks.Open",
        "Tasks.PleaseEnterANameForThisTask",
        "Tasks.ReopenTask",
        "Tasks.TaskDescriptionExceedsMaximumLengthAllowed",
        "Tasks.TaskNameExceedsMaximumLength",
        "Tasks.Unknown",
    ];

    constructor(
        @Inject("$rootScope") private $rootScope: IExtendedRootScopeService,
        @Inject(StoreToken) private store: IStore,
        @Inject("Export") readonly Export: any,
        private stateService: StateService,
        private formBuilder: FormBuilder,
        private Tasks: TasksService,
        private timeStamp: TimeStamp,
        private attachmentService: AttachmentService,
        private translate: TranslateService,
    ) { }

    ngOnInit(): void {
        this.translate.stream(this.translateKeys).subscribe((t) => {
            this.translations = t;
        });

        this.getTasksPage();
        // This is required, since ot-popover(slds-popover) has 7000 z-index and our ot-modal has 50000 z-index
        // And the ot-popover is appended at body level I had to go reverse and set the z-index of modal to lesser value
        (document.querySelector(".ot-modal-base") as HTMLElement).style.zIndex = "6999";
    }

    trackByFn(index: number, task: ITask) {
        return task.id;
    }

    closeModal() {
        this.otModalCloseEvent.next(false);
    }

    deleteTask(task) {
        this.disabled = true;
        return this.Tasks.deleteTask(task).then(() => {
            this.getTasksPage();
        });
    }

    completeTask(task) {
        this.disabled = true;
        task.taskStatusId = TaskStateIds[TASK_STATES.COMPLETE];
        task.reminderDate = null;
        return this.Tasks.updateTask(task).then(() => {
            this.getTasksPage();
        });
    }

    reopenTask(task) {
        this.disabled = true;
        task.taskStatusId = TaskStateIds[TASK_STATES.OPEN];
        task.reminderDate = null;
        return this.Tasks.updateTask(task).then(() => {
            this.getTasksPage();
        });
    }

    downloadFile(task: ITask) {
        if (task.downloadId) {
            task.linkPressed = true;
            this.attachmentService.downloadAttachment(task.downloadId).then(
                (res: IProtocolResponse<any>): void => {
                    if (res.result) {
                        const contentDisposition: string = res.headers
                            ? res.headers["content-disposition"] || res.headers["Content-Disposition"]
                            : "";
                        const splitHeader: string[] = contentDisposition
                            ? contentDisposition.split(".")
                            : [];
                        const fileExtension: string =
                            splitHeader.length && splitHeader.length > 1
                                ? splitHeader[splitHeader.length - 1].replace(/"/g, "") : "";
                        if (contentDisposition) {
                            this.Export.basicFileDownload(res.data, `${task.description || "file"}.${fileExtension}`);
                        } else {
                            this.Export.basicFileDownload(res.data, task.description);
                        }
                    }
                    task.linkPressed = false;
                },
            );
        }
    }

    loadCreateModal() {
        this.task = this.getEmptyTask();
        this.initForm();
        this.taskView = TasksViews.Create;
    }

    onDatepickerChanged(selectedValues: IMyDateModel, type: string) {
        if (type === "dueDate") {
            this.dueDateModel = selectedValues.formatted;
            this.task.dueDate = this.dueDateModel ? selectedValues.jsdate.toISOString() : null;
        } else if (type === "reminderDate") {
            this.reminderDateModel = selectedValues.formatted;
            this.task.reminderDate = this.reminderDateModel ? selectedValues.jsdate.toISOString() : null;
        }
        this.validateReminder();
    }

    loadListModal() {
        this.taskView = TasksViews.List;
        this.disabled = false;
        return this.getTasksPage();
    }

    createTask() {
        this.disabled = true;
        const userId = getCurrentUser(this.store.getState()).Id;
        this.task.name = this.taskManageForm.get("name").value;
        this.task.description = this.taskManageForm.get("description").value;
        this.task.assignee = userId;
        this.task.creator = userId;
        return this.Tasks.createTask(this.task).then(() => {
            this.clearTask(true);
            this.disabled = false;
            this.validReminder = false;
            this.loadListModal();
        });
    }

    clearTask(clearForm) {
        this.task = this.getEmptyTask();
    }

    validateReminder() {
        this.getDisableUntilDate();
        this.validReminder = false;
        if (this.task.dueDate && this.task.reminderDate) {
            const today = new Date();
            const reminderDate = new Date(this.task.reminderDate);
            this.validReminder = reminderDate >= today;
        } else {
            this.validReminder = Boolean(this.task.dueDate);
        }
    }

    getDisableUntilDate(): IMyDate {
        const currentDate = new Date();
        return {
            year: currentDate.getFullYear(),
            month: currentDate.getMonth() + 1,
            day: currentDate.getDate() - 1,
        };
    }

    getActions(task: ITask): IDropdownOption[] {
        const dropDownMenu: IDropdownOption[] = [];
        if (task.canDelete) {
            dropDownMenu.push(this.getDeleteAction(task));
        }
        if (task.canComplete) {
            dropDownMenu.push(this.getCompleteAction(task));
        }
        if (task.canReopen) {
            dropDownMenu.push(this.getReopenAction(task));
        }
        return dropDownMenu;
    }

    getError(field: string): boolean {
        if (field === "name") {
            const name = this.taskManageForm.get(field);
            if (!name || !name.errors) {
                return false;
            } else if (name.dirty && name.touched && name.errors.required) {
                this.errorMessage = this.translations["Tasks.PleaseEnterANameForThisTask"];
                return true;
            } else if (name.dirty && name.touched && name.errors.maxlength) {
                this.errorMessage = this.translations["Tasks.TaskNameExceedsMaximumLength"];
                return true;
            }
        } else if (field === "description") {
            const description = this.taskManageForm.get(field);
            if (!description || !description.errors) {
                return false;
            } else if (description && description.errors.maxlength) {
                this.errorMessage = this.translations["Tasks.TaskDescriptionExceedsMaximumLengthAllowed"];
                return true;
            }
        }
        return false;
    }

    goToAssessment(task: ITask) {
        this.closeModal();
        this.stateService.go("zen.app.pia.module.assessment.detail", { assessmentId: task.assessmentId });
    }

    goToAssets(task: ITask) {
        this.closeModal();
        this.stateService.go("zen.app.pia.module.datamap.views.inventory.list", { Id: "assets", openDataDiscoveryModal: true });
    }

    private getTasksPage(
        pageSize = this.pageSize,
        pageNumber = this.pageNumber,
        filters?,
    ) {
        this.taskView = TasksViews.List;
        this.ready = false;
        this.disabled = false;
        this.validReminder = false;
        const user: string = getCurrentUser(this.store.getState()).Id;
        const filterOptions: ITaskFilterOption[] = [];
        const sort = "createdDate,desc";
        const sortOptions: any[] = [sort];

        if (filters) {
            if (filters.stateFilter) {
                filterOptions.push({
                    filterType: filters.stateFilter,
                    filterValue: null,
                });
            }

            if (filters.searchFilter) {
                filterOptions.push({
                    filterType: "search",
                    filterValue: filters.searchFilter,
                });
            }
        }

        return this.Tasks.listTasks({
            filterOptions,
            pageNumber,
            pageSize,
            sortOptions,
            user,
        }).then((response) => {
            if (response) {
                this.pageMetadata = merge(this.pageMetadata, response.data);
                this.tasks = map(response.data.content, (task: ITask) => {
                    return this.formatTask(task);
                });
                const taskCount: number =
                    response.data && response.data.content
                        ? this.Tasks.filterTaskCount(response.data.content)
                        : 0;
                this.$rootScope.$broadcast("tasks-updated", taskCount);
            }
            this.ready = true;
        });
    }

    private formatTask(task: ITask) {
        const taskDetailType = TaskDetailType.FILE_DOWNLOAD;
        task.linkPressed = false;

        switch (TaskStateKeys[task.taskStatusId]) {
            case TASK_STATES.COMPLETE:
                task.stateLabel = TASK_STATE_CLASSES.COMPLETE;
                task.stateName = this.translations["Tasks.Complete"];
                task.canComplete = false;
                task.canDelete = true;
                task.canReopen = task.type !== TaskTypes.System;
                task.canModify = false;
                break;
            case TASK_STATES.OPEN:
                task.stateLabel = TASK_STATE_CLASSES.OPEN;
                task.stateName = this.translations["Tasks.Open"];
                task.canComplete = task.type !== TaskTypes.System;
                task.canDelete = task.type !== TaskTypes.System;
                task.canReopen = false;
                task.canModify = false;
                if (task.details) {
                    task.details.forEach((detail: ITaskDetail) => {
                        if (detail.type === TaskDetailType.LINK) {
                            task.isDataDiscoveryLink = true;
                        }
                    });
                }
                break;
            case TASK_STATES.FAILED:
                task.stateLabel = TASK_STATE_CLASSES.FAILED;
                task.stateName = this.translations["Tasks.Failed"];
                task.canComplete = task.type !== TaskTypes.System;
                task.canDelete = true;
                task.canReopen = false;
                task.canModify = false;
                if (task.details) {
                    task.details.forEach((detail: ITaskDetail) => {
                        if (detail.type === "assessment-id") {
                            task.isAssessment = true;
                            task.assessmentId = detail.value;
                        }
                    });
                }
                break;
            default:
                task.stateLabel = TASK_STATE_CLASSES.UNKNOWN;
                task.stateName = this.translations["Tasks.Unknown"];
                task.canComplete = false;
                task.canDelete = false;
                task.canReopen = false;
                task.canModify = false;
                break;
        }
        const fileDownloadIndex: number = findIndex(
            task.details,
            (taskdetails: ITaskDetail) => {
                return taskdetails.type === taskDetailType;
            },
        );

        if (fileDownloadIndex > -1 && task.details[0].type !== TaskDetailType.LINK) {
            task.isFileDownload = true;
            task.isFailed = task.taskStatusId === TaskStateIds.TaskFailed;
            task.downloadId = task.details[fileDownloadIndex].value;
        }
        if (task.name === "ControlExport") {
            task.name = this.translations["ControlExport"];
        }
        task.dropDownMenu = this.getActions(task);
        return task;
    }

    private getDeleteAction(task: ITask): IDropdownOption {
        return {
            rowRefId: "TasksModalDeleteButton",
            text: this.translations["Tasks.DeleteTask"],
            action: (): void => {
                this.deleteTask(task);
            },
        };
    }

    private getCompleteAction(task: ITask): IDropdownOption {
        return {
            rowRefId: "TasksModalMarkCompleteButton",
            text: this.translations["Tasks.MarkTaskComplete"],
            action: (): void => {
                this.completeTask(task);
            },
        };
    }

    private getReopenAction(task: ITask): IDropdownOption {
        return {
            rowRefId: "TasksModalReopenTaskButton",
            text: this.translations["Tasks.ReopenTask"],
            action: (): void => {
                this.reopenTask(task);
            },
        };
    }

    private getEmptyTask(): ITask {
        return {
            description: "",
            dueDate: "",
            reminderDate: "",
            name: "",
        };
    }

    private initForm() {
        this.taskManageForm = this.formBuilder.group({
            name: new FormControl("", [
                Validators.required,
                Validators.maxLength(200),
            ]),
            description: new FormControl("", Validators.maxLength(4000)),
        });
    }
}
