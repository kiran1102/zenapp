import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { ReactiveFormsModule } from "@angular/forms";

// Modules
import { TranslateModule } from "@ngx-translate/core";
import { VitreusModule } from "@onetrust/vitreus";
import { OTPipesModule } from "modules/pipes/pipes.module";
import { FormsModule } from "@angular/forms";

// Components
import { SystemTrayNotificationsComponent } from "./system-tray-notifications.component";

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        TranslateModule,
        VitreusModule,
        OTPipesModule,
    ],
    declarations: [
        SystemTrayNotificationsComponent,
    ],
    exports: [
        SystemTrayNotificationsComponent,
    ],
})
export class SystemTrayNotificationsModule {}
