import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

// Modules
import { TranslateModule } from "@ngx-translate/core";
import { VitreusModule } from "@onetrust/vitreus";
import { OTPipesModule } from "modules/pipes/pipes.module";
import { SystemTrayNotificationsModule } from "./system-tray-notifications/system-tray-notifications.module";
import { SystemTraySubtasksModule } from "./system-tray-subtasks/system-tray-subtasks.module";

// Services
import { SystemTrayModalService } from "./system-tray-modal/system-tray-modal.service";

// Components
import { SystemTrayModalComponent } from "./system-tray-modal/system-tray-modal.component";

@NgModule({
    imports: [
        CommonModule,
        TranslateModule,
        VitreusModule,
        OTPipesModule,
        SystemTrayNotificationsModule,
        SystemTraySubtasksModule,
    ],
    declarations: [
        SystemTrayModalComponent,
    ],
    entryComponents: [
        SystemTrayModalComponent,
    ],
    providers: [
        SystemTrayModalService,
    ],
})
export class SystemTrayModule {}
