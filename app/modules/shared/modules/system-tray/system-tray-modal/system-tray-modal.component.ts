import { Component } from "@angular/core";
import { Subject } from "rxjs";
import { SystemTrayOptions } from "sharedModules/enums/system-tray.enum";

// Services
import { TranslateService } from "@ngx-translate/core";
import { SystemTrayModalService } from "./system-tray-modal.service";

@Component({
    selector: "system-tray-modal",
    templateUrl: "./system-tray-modal.component.html",
})
export class SystemTrayModalComponent {
    otModalCloseEvent: Subject<string>;
    currentTab = SystemTrayOptions.SYSTEM;
    systemTrayOptions = SystemTrayOptions;
    tabOptions = [];

    private keys = ["SystemNotificationsAndSubtasks", "System", "SubTasks", "Close"];
    private translations = {};

    constructor(
        public systemTrayModalService: SystemTrayModalService,
        private translate: TranslateService,
    ) {
        this.translate
            .stream(this.keys)
            .subscribe((values) => (this.translations = values));
    }

    ngOnInit() {
        this.tabOptions.push({
            text: this.translations["System"],
            id: SystemTrayOptions.SYSTEM,
            otAutoId: "SystemTrayTabSystem",
        });

        this.tabOptions.push({
            text: this.translations["SubTasks"],
            id: SystemTrayOptions.SUBTASKS,
            otAutoId: "SystemTrayTabSubtasks",
        });
    }

    ngOnDestroy(): void {
        this.systemTrayModalService.reset();
    }

    closeModal(): void {
        this.systemTrayModalService.reset();
        this.otModalCloseEvent.next();
    }

    changeTab(option: SystemTrayOptions): void {
        this.currentTab = option;
    }
}
