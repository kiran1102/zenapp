import { Injectable } from "@angular/core";

// 3rd Party
import { BehaviorSubject } from "rxjs";

@Injectable()
export class SystemTrayModalService {
    hideNav$ = new BehaviorSubject<boolean>(false);
    modalTitle$ = new BehaviorSubject<string>("SystemNotificationsAndSubtasks");
    closeDisabled$ = new BehaviorSubject<boolean>(false);

    reset(): void {
        this.hideNav$.next(false);
        this.modalTitle$.next("SystemNotificationsAndSubtasks");
    }

    toggleNav(hide = false, modalTitle = "SystemNotificationsAndSubtasks") {
        this.hideNav$.next(hide);
        this.modalTitle$.next(modalTitle);
    }

    disableClose(disable = true) {
        this.closeDisabled$.next(disable);
    }

}
