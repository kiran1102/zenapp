export enum VideoTypes {
    OTHER = 0,
    VIMEO = 10,
    WISTIA = 20,
    YOUTUBE = 30,
}