export enum UpgradeModuleState {
    Message = 1,
    Video = 2,
    Form = 3,
    Submitted = 4,
};
