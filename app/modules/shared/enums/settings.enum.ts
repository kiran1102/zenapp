export enum SettingsType {
    TOGGLE,
    NUMBER,
    PICKLIST,
    TEXTAREA,
    INPUT,
    ORGANIZATION,
    APPROVER,
    CHECKBOX,
    LOOKUP,
}
