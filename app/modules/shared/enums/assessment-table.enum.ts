export enum AssessmentTableColumns {
    Id = "assessmentNumber",
    AssessmentName = "assessmentName",
    TemplateName = "templateName",
    AssessmentStatus = "assessmentStatus",
    Status = "status",
    ParentAssessmentName = "parentAssessmentName",
    Relationship = "relationship",
}
