// Each grid view gets it's own Local/SessionStorage name for persisting
// Filters, Columns, Search Text, and Pagination details
export enum GridViewStorageNames {
    ASSET_INVENTORY_LIST = "OneTrust.AssetInventoryList",
    PA_INVENTORY_LIST = "OneTrust.PAInventoryList",
    VENDOR_INVENTORY_LIST = "OneTrust.VendorInventoryList",
    ENTITY_INVENTORY_LIST = "OneTrust.EntityInventoryList",
    CONTROLS_LIBRARY_LIST = "OneTrust.ControlsLibraryList",
    VENDORPEDIA_EXCHANGE_LIST = "OneTrust.VendorpediaExchangeList",
    INVENTORY_RISK_CONTROLS_FILTER_LIST = "OneTrust.InventoryRiskControlsFilterList",
    DASHBOARDS_LIST = "OneTrust.DashboardsList",
    SCHEDULER_LIST = "OneTrust.SchedulerList",
    CONTRACT_LIST = "OneTrust.ContractList",
    ENGAGEMENT_LIST = "OneTrust.EngagementList",
    ENGAGEMENT_CONTRACTS = "OneTrust.EngagementContractList",
}
