// 3rd Party
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { HttpClientModule } from "@angular/common/http";
import { TranslateModule } from "@ngx-translate/core";
import { FormsModule } from "@angular/forms";
import { ReactiveFormsModule } from "@angular/forms";
import { VitreusModule } from "@onetrust/vitreus";
import { DndListModule } from "ngx-drag-and-drop-lists";
import { OTPipesModule, PIPES } from "modules/pipes/pipes.module";
import { VirtualScrollModule } from "od-virtualscroll";

// Component Modules
import { OneVideoModule } from "modules/video/video.module";

// Upgraded Legacy
import { UpgradedLegacyModule } from "./upgrade/upgraded-legacy.module";

// Components
import { AssessmentStateLabelComponent } from "sharedModules/components/assessment-state-label/assessment-state-label.component";
import { OrgSelect } from "sharedModules/components/org-select/org-select.component";
import { OneModalBase } from "sharedModules/components/one-modal-base/one-modal-base.component";
import { OneHorseshoeGraph } from "sharedModules/components/one-horseshoe-graph/one-horseshoe-graph.component";
import { AboutOnetrustModal } from "sharedModules/components/about-onetrust-modal/about-onetrust-modal.component";
import { SwitchTenantModal } from "sharedModules/components/switch-tenant-modal/switch-tenant-modal.component";
import { RiskSummaryCardComponent } from "./components/risk-summary-card/risk-summary-card.component";
import { ColorPickerComponent } from "sharedModules/components/color-picker/color-picker.component";
import { LanguageSelectionModal } from "sharedModules/components/language-selection-modal/language-selection-modal.component";
import { GridColumnSelectorComponent } from "sharedModules/components/grid-column-selector/grid-column-selector.component";
import { OneNotificationModalComponent } from "sharedModules/components/one-notification-modal/one-notification-modal.component";
import { OneEmptyState } from "sharedModules/components/one-empty-state/one-empty-state.component";
import { OneButtonToggleComponent } from "sharedModules/components/one-button-toggle/one-button-toggle.component";
import { OtOrgUserComponent } from "sharedModules/components/ot-org-user/ot-org-user.component";
import { OtOrgUserMultiSelectComponent } from "sharedModules/components/ot-org-user/ot-org-user-multi-select.component";
import { OtTemplateSelect } from "sharedModules/components/ot-template-select/ot-template-select.component";
import { RiskSelectorComponent } from "sharedModules/components/risk-selector/risk-selector.component";
import { ColumnSelectorModalComponent } from "sharedModules/components/column-selector/column-selector-modal.component";
import { OtDeleteConfirmationModal } from "modals/ot-delete-confirmation-modal/ot-delete-confirmation-modal.component";
import { UnsavedChangesConfirmationModal } from "sharedModules/components/unsaved-changes-confirmation-modal/unsaved-changes-confirmation-modal.component";
import { AppMaintenanceAlertsModal } from "sharedModules/components/app-maintenance-alerts-modal/app-maintenance-alerts-modal.component";
import { OneDeadlineLabel } from "sharedModules/components/one-deadline-label/one-deadline-label.component";
import { OneAssigneeLabel } from "sharedModules/components/one-assignee-label/one-assignee-label.component";

import { LoginHistoryComponent } from "adminComponent/login-history/login-history.component";
import { HelpDialogModal } from "sharedModules/components/help-dialog-modal/help-dialog-modal.component";
import { ChangeLanguageModal } from "sharedModules/components/change-language-modal/change-language-modal.component";
import { HelpModal } from "sharedModules/components/help-modal/help-modal.component";
import { OneAnchorButton } from "sharedModules/components/one-anchor-button/one-anchor-button.component.ts";
import { OneStaticContainerListComponent } from "sharedModules/components/one-static-container-list/one-static-container-list.component.ts";
import { OneExpandableLinks } from "sharedModules/components/one-expandable-links/one-expandable-links.component.ts";
import { OneEditableButton } from "sharedModules/components/one-editable-button/one-editable-button.component";
import { SwitchToggleComponent } from "sharedModules/components/switch-toggle/switch-toggle.component";
import { OneDateTimePickerComponent } from "sharedModules/components/one-date-time-picker/one-date-time-picker.component";
import { SettingsGeneratorComponent } from "sharedModules/components/settings-generator/settings-generator.component";
import { GeneralSettingsGeneratorComponent } from "sharedModules/components/general-settings-generator/general-settings-generator.component";
import { ShareAssessmentModal } from "./components/share-assessment-modal/share-assessment-modal.component";
import { OneCommentsActivityStreamComponent } from "sharedModules/components/one-comments-activity-stream/one-comments-activity-stream.component";
import { ListGroupContainer } from "sharedModules/components/list-group/list-group-container.component";
import { ListGroupItems } from "sharedModules/components/list-group/list-group-items.component";
import { ListGroupAdd } from "sharedModules/components/list-group/list-group-add.component";
import { ViewRelatedAssessmentModalComponent } from "sharedModules/components/view-related-assessment-modal/view-related-assessment-modal.component";
import { AssessmentTable } from "sharedModules/components/view-related-assessment-modal/assessment-table/assessment-table.component";
import { OneAgingGraph } from "sharedModules/components/one-aging-graph/one-aging-graph.component";
import { OTNotificationModalComponent } from "sharedModules/components/ot-notification-modal/ot-notification-modal.component";
import { DownloadLandingComponent } from "sharedModules/components/download-landing/download-landing.component";

// Directives
import { EmptyStateImageDirective } from "./directives/empty-state-image.directive";
import { OtClipTextDirective } from "./directives/ot-clip-text.directive";
import { OtTextAreaPlaceholderDirective } from "./directives/ot-text-area-placeholder.directive";

// Legacy Services
import { StoreToken } from "tokens/redux-store.token";
import { Permissions } from "modules/shared/services/helper/permissions.service";
import { ModalService } from "sharedServices/modal.service";
import { TasksService } from "sharedServices/tasks.service";
import { Settings } from "sharedServices/settings.service";
import { SettingActionService } from "settingsModule/services/actions/setting-action.service";
import { PaginationService } from "oneServices/pagination.service.ts";
import { SupportService } from "oneServices/support.service.ts";
import { UpgradeService } from "sharedServices/upgrade.service.ts";
import { UserActionService } from "oneServices/actions/user-action.service";

// API
import { Accounts } from "./services/api/accounts.service";
import { AccessLevelsService } from "./services/api/access-levels.service";
import { ConfigurationApi } from "./services/api/configuration-api.service";
import { AppMaintenanceAlertsService } from "sharedModules/services/api/app-maintenance-alerts.service";
import { BannerAlertService } from "sharedModules/services/api/banner-alert.service";
import { LoginHistoryService } from "adminService/login-history.service";
import { AuthHandlerService } from "modules/shared/services/helper/auth-handler.service";
import { Principal } from "./services/helper/principal.service";
import { SessionService } from "./services/helper/session.service";
import { StorageProxy } from "./services/helper/storage.service";
import { ViewRelatedAssessmentModalApiService } from "sharedModules/services/api/view-related-assessment-modal-api.service";
import { AssessmentBulkLaunchApiService } from "sharedModules/services/api/bulk-launch-assessment-api.service";

// Actions
import { ConfigurationAction } from "./services/actions/configuration-action.service";
import { AssessmentBulkLaunchActionService } from "sharedModules/services/actions/bulk-launch-assessment-action.service";

// Providers
import { AuthEntryService } from "../core/services/auth-entry.service";
import { Branding } from "./services/provider/branding.service";
import { Content } from "sharedModules/services/provider/content.service";
import { DefaultContentService } from "./services/provider/default-content.service";
import { HelpCenter } from "./services/provider/help-center.service";
import {
    Favicons,
    BrowserFavicons,
    BROWSER_FAVICONS_CONFIG,
    BROWSER_FAVICONS_CONFIG_VALUE,
} from "./services/provider/favicon.service";
import { HybridRouter } from "sharedModules/services/helper/hybrid-router.service";
import { LookupService } from "sharedModules/services/helper/lookup.service";
import { GridViewStorageService } from "sharedModules/services/helper/grid-view-storage.service";
import { MetaService } from "./services/provider/meta.service";
import { NotificationService } from "sharedModules/services/provider/notification.service";
import { OtOrgUserService } from "sharedModules/components/ot-org-user/ot-org-user.service";
import { RiskHelperService } from "sharedModules/services/helper/risk-helper.service";
import { ScriptLoader } from "./services/provider/script-loader.service";
import { Userlane } from "./services/provider/userlane.service";
import { Wootric } from "./services/provider/wootric.service";

export function getLegacyStoreFactory(injector) {
    return injector.get("store");
}

export function getLegacyStorageFactory(injector) {
    return injector.get("$localStorage");
}

export function getLegacySessionStorageFactory(injector) {
    return injector.get("$sessionStorage");
}

export function getLegacyExportService(injector) {
    return injector.get("Export");
}

export function getLegacyAuthService(injector) {
    return injector.get("authService");
}

export function getLegacyModalService(injector) {
    return injector.get("ModalService");
}

export function getLegacyTasksService(injector) {
    return injector.get("TasksService");
}

export function getLegacySettingsService(injector) {
    return injector.get("Settings");
}

export function getLegacySettingActionService(injector) {
    return injector.get("SettingAction");
}

export function getLegacyPaginationService(injector) {
    return injector.get("PaginationService");
}

export function getLegacySettingConsentService(injector) {
    return injector.get("SettingConsentAction");
}

export function getLegacyAccountsService(injector) {
    return injector.get("Accounts");
}

export function getLegacyTenantTranslatiosService(injector) {
    return injector.get("TenantTranslationService");
}

export function getLegacyBrandingService(injector) {
    return injector.get("Branding");
}

export function getLegacyUpgradeService(injector) {
    return injector.get("UpgradeService");
}

export function getLegacyUserActionService(injector) {
    return injector.get("UserActionService");
}

export function getAssessmentBulkLaunchActionService(injector) {
    return injector.get("AssessmentBulkLaunchActionService");
}

export function getOrgGroupStoreNewService(injector) {
    return injector.get("OrgGroupStoreNew");
}

export function getRiskBusinessLogicService(injector) {
    return injector.get("RiskBusinessLogic");
}

const SHARED_COMPONENTS_TO_BE_DOWNGRADED = [
    AboutOnetrustModal,
    LanguageSelectionModal,
    GridColumnSelectorComponent,
    OneEmptyState,
    OneHorseshoeGraph,
    OneNotificationModalComponent,
    OrgSelect,
    SwitchTenantModal,
    ColumnSelectorModalComponent,
    OtDeleteConfirmationModal,
    UnsavedChangesConfirmationModal,
    RiskSelectorComponent,
    AppMaintenanceAlertsModal,
    LoginHistoryComponent,
    HelpDialogModal,
    ChangeLanguageModal,
    HelpModal,
    OneAnchorButton,
    OneExpandableLinks,
    SwitchToggleComponent,
    OneDateTimePickerComponent,
    ShareAssessmentModal,
    ViewRelatedAssessmentModalComponent,
    AssessmentTable,
    OTNotificationModalComponent,
    DownloadLandingComponent,
];

const SHARED_COMPONENTS = [
    AssessmentStateLabelComponent,
    ViewRelatedAssessmentModalComponent,
    AssessmentTable,
    ColorPickerComponent,
    OneModalBase,
    OneHorseshoeGraph,
    OneButtonToggleComponent,
    OtOrgUserComponent,
    OtOrgUserMultiSelectComponent,
    OtTemplateSelect,
    OneDeadlineLabel,
    OneAssigneeLabel,
    OneAnchorButton,
    OneExpandableLinks,
    OneEditableButton,
    EmptyStateImageDirective,
    OtClipTextDirective,
    OtTextAreaPlaceholderDirective,
    // TODO: Below components need to be moved to respective modules.
    RiskSummaryCardComponent,
    OneNotificationModalComponent,
    ColumnSelectorModalComponent,
    OneStaticContainerListComponent,
    SettingsGeneratorComponent,
    GeneralSettingsGeneratorComponent,
    ShareAssessmentModal,
    OneAgingGraph,
    OTNotificationModalComponent,
    RiskSelectorComponent,
    OneCommentsActivityStreamComponent,
    ListGroupContainer,
    ListGroupItems,
    ListGroupAdd,
];

const GLOBAL_TOKENS = [
    { provide: StoreToken, deps: ["$injector"], useFactory: getLegacyStoreFactory },
    { provide: Favicons, useClass: BrowserFavicons },
    { provide: BROWSER_FAVICONS_CONFIG, useValue: BROWSER_FAVICONS_CONFIG_VALUE },
];

const GLOBAL_SERVICES = [
    { provide: ModalService, deps: ["$injector"], useFactory: getLegacyModalService },
    { provide: TasksService, deps: ["$injector"], useFactory: getLegacyTasksService },
    { provide: UpgradeService, deps: ["$injector"], useFactory: getLegacyUpgradeService },
    { provide: UserActionService, deps: ["$injector"], useFactory: getLegacyUserActionService },
    { provide: AssessmentBulkLaunchActionService, deps: ["$injector"], useFactory: getAssessmentBulkLaunchActionService },
    { provide: "OrgGroupStoreNewService", deps: ["$injector"], useFactory: getOrgGroupStoreNewService },
];

// TODO: The below services should be moved out as they are migrated and deprecated.
const GLOBAL_SERVICES_TO_BE_DEPRECATED = [
    { provide: "$localStorage", deps: ["$injector"], useFactory: getLegacyStorageFactory },
    { provide: "$sessionStorage", deps: ["$injector"], useFactory: getLegacySessionStorageFactory },
    { provide: "authService", deps: ["$injector"], useFactory: getLegacyAuthService },
    { provide: "Export", deps: ["$injector"], useFactory: getLegacyExportService },
    { provide: Settings, deps: ["$injector"], useFactory: getLegacySettingsService },
    { provide: SettingActionService, deps: ["$injector"], useFactory: getLegacySettingActionService },
    { provide: PaginationService, deps: ["$injector"], useFactory: getLegacyPaginationService },
    { provide: Accounts, deps: ["$injector"], useFactory: getLegacyAccountsService },
    { provide: "TenantTranslationService", deps: ["$injector"], useFactory: getLegacyTenantTranslatiosService },
    { provide: Branding, deps: ["$injector"], useFactory: getLegacyBrandingService },
    { provide: "RiskBusinessLogic", deps: ["$injector"], useFactory: getRiskBusinessLogicService },
];

const SERVICES = [
    AccessLevelsService,
    AppMaintenanceAlertsService,
    AuthEntryService,
    AssessmentBulkLaunchApiService,
    AuthHandlerService,
    BannerAlertService,
    ConfigurationAction,
    ConfigurationApi,
    Content,
    DefaultContentService,
    GridViewStorageService,
    HelpCenter,
    HybridRouter,
    LoginHistoryService,
    LookupService,
    MetaService,
    NotificationService,
    OtOrgUserService,
    Permissions,
    Principal,
    RiskHelperService,
    ScriptLoader,
    SessionService,
    StorageProxy,
    SupportService,
    Userlane,
    ViewRelatedAssessmentModalApiService,
    Wootric,
];

@NgModule({
    imports: [
        CommonModule,
        HttpClientModule,
        FormsModule,
        VitreusModule,
        TranslateModule,
        ReactiveFormsModule,
        DndListModule,
        OTPipesModule,
        VirtualScrollModule,
        OneVideoModule,
        UpgradedLegacyModule,
    ],
    providers: [
        ...GLOBAL_TOKENS,
        ...GLOBAL_SERVICES,
        ...GLOBAL_SERVICES_TO_BE_DEPRECATED,
        ...SERVICES,
        ...PIPES, // TODO: Remove from providers when ot-common is implemented. Only necessary if you are using pipe in TS file, else just module is fine
    ],
    exports: [
        ...SHARED_COMPONENTS,
        ...SHARED_COMPONENTS_TO_BE_DOWNGRADED,
    ],
    declarations: [
        ...SHARED_COMPONENTS,
        ...SHARED_COMPONENTS_TO_BE_DOWNGRADED,
    ],
    entryComponents: [
        ...SHARED_COMPONENTS_TO_BE_DOWNGRADED,
    ],
})
export class SharedModule { }
