export const inviteModules = {
    "projects": {
        Name: "projects",
        State: "zen.app.pia.module.projects.list",
        IdState: "zen.app.pia.module.projects.assessment.module.questions",
        IdParam: "Id",
        VersionParam: "Version",
    },
    "datamap": {
        Name: "datamap",
        State: "zen.app.pia.module.datamap.views.assessment-old.list",
        IdState: "zen.app.pia.module.datamap.views.assessment-old.single.module.questions",
        IdParam: "Id",
        VersionParam: "Version",
    },
    "datamap-assessments": {
        Name: "datamap-assessments",
        State: "zen.app.pia.module.datamap.views.assessment.list",
        IdState: "zen.app.pia.module.datamap.views.assessment.single",
        IdParam: "Id",
        VersionParam: "Version",
    },
    "dsar": {
        Name: "dsar",
        State: "zen.app.pia.module.dsar.views.queue",
        IdState: "zen.app.pia.module.dsar.views.queue-details",
        IdParam: "id",
    },
    "assessment": {
        Name: "assessment",
        State: "zen.app.pia.module.assessment.list",
        IdState: "zen.app.pia.module.assessment.detail",
        IdParam: "assessmentId",
        searchParam: "openPanel",
    },
};
