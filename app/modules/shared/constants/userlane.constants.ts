export const UserlaneActions = {
    assistant: "openAssistant",
    hide     : "hide",
    init     : "init",
    lang     : "lang",
    start    : "start",
    tag      : "tag",
    untag    : "untag",
    user     : "user",
    identify: "identify",
};

export const PermissionToModuleNameMap = {
    Readiness: "Readiness",
    ReadinessAssessmentV2: "Global Readiness",
    Projects: "Projects",
    Assessments: "Assessments",
    DataMapping: "Data Mapping",
    DataMappingNew: "Data Mapping",
    Cookie: "Cookie Consent",
    ConsentReceipt: "Consent",
    DSARView: "Data Subject Requests",
    VendorRiskManagement: "Vendor Management",
    IncidentBreachManagement: "Incident Response",
};
