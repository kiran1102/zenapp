import {
    Frequency,
    DaysOfTheWeek,
} from "sharedModules/enums/cron.enum";
import { ICronObject } from "sharedModules/interfaces/cron.interface";

export const TimeOfDayOptions = [
    { label: "12:00 AM", value: 0 },
    { label: "1:00 AM", value: 1 },
    { label: "2:00 AM", value: 2 },
    { label: "3:00 AM", value: 3 },
    { label: "4:00 AM", value: 4 },
    { label: "5:00 AM", value: 5 },
    { label: "6:00 AM", value: 6 },
    { label: "7:00 AM", value: 7 },
    { label: "8:00 AM", value: 8 },
    { label: "9:00 AM", value: 9 },
    { label: "10:00 AM", value: 10 },
    { label: "11:00 AM", value: 11 },
    { label: "12:00 PM", value: 12 },
    { label: "1:00 PM", value: 13 },
    { label: "2:00 PM", value: 14 },
    { label: "3:00 PM", value: 15 },
    { label: "4:00 PM", value: 16 },
    { label: "5:00 PM", value: 17 },
    { label: "6:00 PM", value: 18 },
    { label: "7:00 PM", value: 19 },
    { label: "8:00 PM", value: 20 },
    { label: "9:00 PM", value: 21 },
    { label: "10:00 PM", value: 22 },
    { label: "11:00 PM", value: 23 },
];

export const DayOfWeekTranslations = {
    [DaysOfTheWeek.Sunday]: "Sunday",
    [DaysOfTheWeek.Monday]: "Monday",
    [DaysOfTheWeek.Tuesday]: "Tuesday",
    [DaysOfTheWeek.Wednesday]: "Wednesday",
    [DaysOfTheWeek.Thursday]: "Thursday",
    [DaysOfTheWeek.Friday]: "Friday",
    [DaysOfTheWeek.Saturday]: "Saturday",
};

export const DayOfMonthLabels = {
    1: "1st",
    2: "2nd",
    3: "3rd",
    4: "4th",
    5: "5th",
    6: "6th",
    7: "7th",
    8: "8th",
    9: "9th",
    10: "10th",
    11: "11th",
    12: "12th",
    13: "13th",
    14: "14th",
    15: "15th",
    16: "16th",
    17: "17th",
    18: "18th",
    19: "19th",
    20: "20th",
    21: "21st",
    22: "22nd",
    23: "23rd",
    24: "24th",
    25: "25th",
    26: "26th",
    27: "27th",
    28: "28th",
    29: "29th",
    30: "30th",
    31: "31st",
};

export const DayOfTheWeekOptions = [
    { labelKey: DayOfWeekTranslations[DaysOfTheWeek.Sunday], value: DaysOfTheWeek.Sunday },
    { labelKey: DayOfWeekTranslations[DaysOfTheWeek.Monday], value: DaysOfTheWeek.Monday },
    { labelKey: DayOfWeekTranslations[DaysOfTheWeek.Tuesday], value: DaysOfTheWeek.Tuesday },
    { labelKey: DayOfWeekTranslations[DaysOfTheWeek.Wednesday], value: DaysOfTheWeek.Wednesday },
    { labelKey: DayOfWeekTranslations[DaysOfTheWeek.Thursday], value: DaysOfTheWeek.Thursday },
    { labelKey: DayOfWeekTranslations[DaysOfTheWeek.Friday], value: DaysOfTheWeek.Friday },
    { labelKey: DayOfWeekTranslations[DaysOfTheWeek.Saturday], value: DaysOfTheWeek.Saturday },
];

export const FrequencyOptions = [
    { labelKey: "Daily", value: Frequency.Daily },
    { labelKey: "Weekly", value: Frequency.Weekly },
    { labelKey: "Monthly", value: Frequency.Monthly },
];

export const DayOfTheMonthOptions = [
    { label: DayOfMonthLabels[1], value: 1 },
    { label: DayOfMonthLabels[2], value: 2 },
    { label: DayOfMonthLabels[3], value: 3 },
    { label: DayOfMonthLabels[4], value: 4 },
    { label: DayOfMonthLabels[5], value: 5 },
    { label: DayOfMonthLabels[6], value: 6 },
    { label: DayOfMonthLabels[7], value: 7 },
    { label: DayOfMonthLabels[8], value: 8 },
    { label: DayOfMonthLabels[9], value: 9 },
    { label: DayOfMonthLabels[10], value: 10 },
    { label: DayOfMonthLabels[11], value: 11 },
    { label: DayOfMonthLabels[12], value: 12 },
    { label: DayOfMonthLabels[13], value: 13 },
    { label: DayOfMonthLabels[14], value: 14 },
    { label: DayOfMonthLabels[15], value: 15 },
    { label: DayOfMonthLabels[16], value: 16 },
    { label: DayOfMonthLabels[17], value: 17 },
    { label: DayOfMonthLabels[18], value: 18 },
    { label: DayOfMonthLabels[19], value: 19 },
    { label: DayOfMonthLabels[20], value: 20 },
    { label: DayOfMonthLabels[21], value: 21 },
    { label: DayOfMonthLabels[22], value: 22 },
    { label: DayOfMonthLabels[23], value: 23 },
    { label: DayOfMonthLabels[24], value: 24 },
    { label: DayOfMonthLabels[25], value: 25 },
    { label: DayOfMonthLabels[26], value: 26 },
    { label: DayOfMonthLabels[27], value: 27 },
    { label: DayOfMonthLabels[28], value: 28 },
    { label: DayOfMonthLabels[29], value: 29 },
    { label: DayOfMonthLabels[30], value: 30 },
    { label: DayOfMonthLabels[31], value: 31 },
];

export const FrequencyTranslations = {
    [Frequency.Daily]: "Daily",
    [Frequency.Weekly]: "Weekly",
    [Frequency.Monthly]: "Monthly",
};

export const EveryDayValues = [1, 2, 3, 4, 5, 6, 7];

export const WeekdayOnlyValues = [2, 3, 4, 5, 6];

export const DefaultCronObject: ICronObject = {
    minute: 0,
    hour: 0,
    daysOfTheWeek: EveryDayValues,
    dayOfMonth: null,
};
