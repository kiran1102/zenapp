export interface IHelpResource {
    otAutoId: string;
    show: boolean;
    title: string;
    type?: string;
    click?: () => void;
}
