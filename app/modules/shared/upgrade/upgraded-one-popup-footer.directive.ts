import { Directive, ElementRef, Injector, Input } from "@angular/core";
import { UpgradeComponent } from "@angular/upgrade/static";

@Directive({
    selector: "upgraded-one-popup-footer",
})

export class UpgradedOnePopupFooter extends UpgradeComponent {

    constructor(elementRef: ElementRef, injector: Injector) {
        super("onePopupFooter", elementRef, injector);
    }

    @Input() visible;
    @Input() footerClass;
    @Input() fullCenter;
    @Input() footerMainClass;
}
