import { Directive, ElementRef, Injector, Input, Output, EventEmitter } from "@angular/core";
import { UpgradeComponent } from "@angular/upgrade/static";

@Directive({
    selector: "upgraded-one-pagination",
})
export class UpgradedOnePagination extends UpgradeComponent {

    constructor(elementRef: ElementRef, injector: Injector) {
        super("onePagination", elementRef, injector);
    }

    @Input() identifier;
    @Input() list;
    @Input() hidePaginationNumber;
    @Input() paginationClass;
    @Input() hideCount;
    @Output() onPageChange: EventEmitter<any>;
}
