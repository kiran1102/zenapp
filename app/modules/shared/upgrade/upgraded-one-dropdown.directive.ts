import { Directive, ElementRef, Injector, Input } from "@angular/core";
import { UpgradeComponent } from "@angular/upgrade/static";

@Directive({
    selector: "upgraded-one-dropdown",
})
export class UpgradedOneDropdown extends UpgradeComponent {

    @Input() alignRight;
    @Input() appendToBody;
    @Input() buttonClass;
    @Input() defaultIndex;
    @Input() roundButton;
    @Input() getOptions;
    @Input() iconClass;
    @Input() flatButton;
    @Input() onToggle;
    @Input() isLoading;
    @Input() isDisabled;
    @Input() isLarge;
    @Input() dropdownClass;
    @Input() enableBorder;
    @Input() hideCaret;
    @Input() icon;
    @Input() identifier;
    @Input() itemClass;
    @Input() listClass;
    @Input() options;
    @Input() text;
    @Input() textUpdate;

    constructor(elementRef: ElementRef, injector: Injector) {
        super("oneDropdown", elementRef, injector);
    }
}
