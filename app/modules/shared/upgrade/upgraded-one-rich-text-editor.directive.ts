import {
    Directive,
    ElementRef,
    Injector,
    Input,
    Output,
} from "@angular/core";
import { UpgradeComponent } from "@angular/upgrade/static";

@Directive({ selector: "upgraded-one-rich-text-editor" })
export class UpgradedOneRichTextEditorDirective extends UpgradeComponent {
    @Input() editorClassName;
    @Input() content;
    @Input() contentObservable;
    @Input() config;
    @Output() onContentChange;

    constructor(elementRef: ElementRef, injector: Injector) {
        super("oneRichTextEditor", elementRef, injector);
    }
}
