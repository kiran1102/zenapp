import { Directive, ElementRef, Injector, Input, Output } from "@angular/core";
import { UpgradeComponent } from "@angular/upgrade/static";

@Directive({ selector: "upgraded-pill-list" })
export class UpgradedPillList extends UpgradeComponent {
    constructor(elementRef: ElementRef, injector: Injector) {
        super("pillList", elementRef, injector);
    }

    @Input() list;
    @Input() nameKey;
    @Input() descriptionKey;
    @Input() tooltipKey;
    @Input() isDisabled;
    @Output() handleRemove;
}
