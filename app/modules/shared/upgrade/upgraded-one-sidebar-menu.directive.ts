import { Directive, ElementRef, Injector, Input } from "@angular/core";
import { UpgradeComponent } from "@angular/upgrade/static";

@Directive({ selector: "upgraded-one-sidebar-menu" })
export class UpgradedOneSidebarMenuDirective extends UpgradeComponent {
    @Input() config;
    @Input() menu;
    @Input() pinnedMenu;
    @Input() menuTitle;

    constructor(elementRef: ElementRef, injector: Injector) {
        super("oneSidebarMenu", elementRef, injector);
    }
}
