import { Directive, ElementRef, Injector, Input } from "@angular/core";
import { UpgradeComponent } from "@angular/upgrade/static";

@Directive({
    selector: "upgraded-one-label-content",
})

export class UpgradedOneLabelContent extends UpgradeComponent {
    @Input() labelName;
    @Input() description;
    @Input() descriptionDelay;
    @Input() hideDescriptionStyle;
    @Input() hideName;
    @Input() labelClass;
    @Input() labelStyle;
    @Input() bodyClass;
    @Input() isRequired;
    @Input() requiredFlagBeforeLabel;
    @Input() inline;
    @Input() split;
    @Input() icon;
    @Input() iconClass;
    @Input() wrapperClass;
    @Input() legendColor;
    @Input() nameDescription;

    constructor(elementRef: ElementRef, injector: Injector) {
        super("oneLabelContent", elementRef, injector);
    }
}
