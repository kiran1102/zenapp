import { Directive, ElementRef, Injector, Input, Output } from "@angular/core";
import { UpgradeComponent } from "@angular/upgrade/static";

@Directive({ selector: "upgraded-reactive-dropdown" })
export class UpgradedReactiveDropdown extends UpgradeComponent {
    constructor(elementRef: ElementRef, injector: Injector) {
        super("reactiveDropdown", elementRef, injector);
    }

    @Input() identifier;
    @Input() isDisabled;
    @Input() placeholder;
    @Input() selectedModel;
    @Input() roundButton;
    @Input() buttonClass;
    @Input() rightIconClass;
    @Input() options;
    @Input() listClass;
    @Input() leftIconClass;
    @Input() buttonTextClass;
    @Input() defaultOption;
    @Input() itemClass;
    @Input() flatButton;
    @Input() dropdownClass;
    @Input() alignRight;
    @Output() onChange;

}
