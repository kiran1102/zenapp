import { Directive, ElementRef, Injector, Input, Output } from "@angular/core";
import { UpgradeComponent } from "@angular/upgrade/static";

@Directive({
    selector: "upgraded-one-popup-toggle",
})
export class UpgradedOnePopupToggle extends UpgradeComponent {

    @Input() public popupAppendToBody;
    @Input() public popupClass;
    @Input() public popupDelay;
    @Input() public popupPlacement;
    @Input() public popupTrigger;
    @Input() public popupTitle;
    @Input() public popupContent;
    @Input() public popupContentPromise;
    @Input() public options;
    @Output() public callback;

    constructor(elementRef: ElementRef, injector: Injector) {
        super("onePopupToggle", elementRef, injector);
    }
}