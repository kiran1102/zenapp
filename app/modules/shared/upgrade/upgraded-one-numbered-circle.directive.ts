import { Directive, ElementRef, Injector, Input } from "@angular/core";
import { UpgradeComponent } from "@angular/upgrade/static";

@Directive({
    selector: "upgraded-one-numbered-circle",
})
export class UpgradedOneNumberedCircle extends UpgradeComponent {

    constructor(elementRef: ElementRef, injector: Injector) {
        super("oneNumberedCircle", elementRef, injector);
    }

    @Input() public number;
    @Input() public size;
    @Input() public type;
    @Input() public circleClass;
    @Input() public icon;
}
