import { Directive, ElementRef, Injector, Input, Output, EventEmitter } from "@angular/core";
import { UpgradeComponent } from "@angular/upgrade/static";

@Directive({ selector: "upgraded-one-select" })
export class UpgradedOneSelect extends UpgradeComponent {

    @Input() model;
    @Input() disablePlaceholderOption;
    @Input() isDisabled;
    @Input() isRequired;
    @Input() options;
    @Input() labelKey;
    @Input() translationKey;
    @Input() valueKey;
    @Input() placeholderText;
    @Input() selectClass;
    @Input() resetToFirstSelection;
    @Input() identifier;
    @Input() modifierClass;
    @Output() onChange: EventEmitter<any>;

    constructor(elementRef: ElementRef, injector: Injector) {
        super("oneSelect", elementRef, injector);
    }
}
