import { Directive, ElementRef, Injector, Input } from "@angular/core";
import { UpgradeComponent } from "@angular/upgrade/static";

@Directive({
    selector: "upgraded-one-background-message",
})
export class UpgradedOneBackgroundMessage extends UpgradeComponent {
    @Input() showImage;
    @Input() imageSrc;
    @Input() showEmptyStateImg;
    @Input() rowStyled;
    @Input() row1;
    @Input() row2;

    constructor(elementRef: ElementRef, injector: Injector) {
        super("oneBackgroundMessage", elementRef, injector);
    }
}
