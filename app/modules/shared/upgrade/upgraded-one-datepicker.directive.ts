import { Directive, ElementRef, Injector, Input, Output } from "@angular/core";
import { UpgradeComponent } from "@angular/upgrade/static";

@Directive({
    selector: "upgraded-one-datepicker",
})
export class UpgradedOneDatePicker extends UpgradeComponent {

    constructor(elementRef: ElementRef, injector: Injector) {
        super("oneDatepicker", elementRef, injector);
    }

    @Input() dateModel;
    @Input() isDisabled;
    @Input() label;
    @Input() minDate;
    @Input() placeholder;
    @Input() useMinDate;
    @Output() onChange;
}
