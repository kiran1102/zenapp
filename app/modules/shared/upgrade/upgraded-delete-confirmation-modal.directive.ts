import { Directive, ElementRef, Injector, Input, Output } from "@angular/core";
import { UpgradeComponent } from "@angular/upgrade/static";

@Directive({ selector: "upgraded-delete-confirmation-modal" })
export class UpgradedDeleteConfirmationModal extends UpgradeComponent {
    @Input() resolve;

    constructor(elementRef: ElementRef, injector: Injector) {
        super("deleteConfirmationModal", elementRef, injector);
    }
}
