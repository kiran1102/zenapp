import { Directive, ElementRef, Injector, Input, Output, EventEmitter } from "@angular/core";
import { UpgradeComponent } from "@angular/upgrade/static";

@Directive({
    selector: "upgraded-one-editable-text",
})
export class UpgradedOneEditableText extends UpgradeComponent {

    @Input() blockClass;
    @Input() disabled;
    @Input() inputClass;
    @Input() inputSmall;
    @Input() isDisabled;
    @Input() text;
    @Input() textArea;

    @Output() handleCancel: EventEmitter<any>;
    @Output() handleChange: EventEmitter<any>;
    @Output() handleSubmit: EventEmitter<any>;

    constructor(elementRef: ElementRef, injector: Injector) {
        super("oneEditableText", elementRef, injector);
    }
}
