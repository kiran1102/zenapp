import { Directive, ElementRef, Injector, Input, Output } from "@angular/core";
import { UpgradeComponent } from "@angular/upgrade/static";

@Directive({
    selector: "upgraded-side-drawer",
})
export class UpgradedSideDrawer extends UpgradeComponent {

    constructor(elementRef: ElementRef, injector: Injector) {
        super("sideDrawer", elementRef, injector);
    }

    @Input() offscreenHide;
    @Input() showingDrawer;
    @Input() defaultOpen;
    @Input() toggleCb;
}
