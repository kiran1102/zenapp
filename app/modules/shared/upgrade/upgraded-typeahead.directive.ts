import { Directive, ElementRef, Injector, Input, Output, EventEmitter } from "@angular/core";
import { UpgradeComponent } from "@angular/upgrade/static";

@Directive({
    selector: "upgraded-typeahead",
})
export class UpgradedTypeahead extends UpgradeComponent {

    constructor(elementRef: ElementRef, injector: Injector) {
        super("typeahead", elementRef, injector);
    }

    @Input() options;
    @Input() loadOnClick;
    @Input() asyncOptionsFunction;
    @Input() model;
    @Input() readOnly;
    @Input() labelKey;
    @Input() hasArrow;
    @Input() clearOnSelect;
    @Input() disableIfNoOptions;
    @Input() identifier;
    @Input() isRequired;
    @Input() isDisabled;
    @Input() hasError;
    @Input() placeholderText;
    @Input() allowOther;
    @Input() otherOptionPrepend;
    @Input() allowEmpty;
    @Input() emptyText;
    @Input() emptyOptionLast;
    @Input() formClass;
    @Input() appendToBody;
    @Input() scrollParent;
    @Input() orderByLabel;
    @Input() focusFirst;
    @Input() isParentModal;
    @Input() translationKey;
    @Input() showArrowByDefault;
    @Input() optionsLimit;
    @Input() inputLimit;
    @Input() useVirtualScroll;
    @Output() onSelect: EventEmitter<any>;
    @Output() onBlur: EventEmitter<any>;
    @Output() onFocus: EventEmitter<any>;
    @Output() onChange: EventEmitter<any>;
}
