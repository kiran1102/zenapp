import { Directive, ElementRef, Injector, Input } from "@angular/core";
import { UpgradeComponent } from "@angular/upgrade/static";

@Directive({
    selector: "upgraded-action-icons",
})
export class UpgradedActionIcons extends UpgradeComponent {
    @Input() actions;
    @Input() target;
    constructor(elementRef: ElementRef, injector: Injector) {
        super("actionIcons", elementRef, injector);
    }
}
