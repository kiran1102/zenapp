import { Directive, ElementRef, Injector, Input, Output } from "@angular/core";
import { UpgradeComponent } from "@angular/upgrade/static";

@Directive({
    selector: "upgraded-org-user-dropdown",
})
export class UpgradedOrgUserDropdown extends UpgradeComponent {

    @Input() allowEmpty;
    @Input() allowOther;
    @Input() bodyClass;
    @Input() defaultCurrentUser;
    @Input() doValidate;
    @Input() emptyText;
    @Input() errorText;
    @Input() hideLabel;
    @Input() inputClass;
    @Input() isDisabled;
    @Input() isRequired;
    @Input() requiredFlagBeforeLabel;
    @Input() label;
    @Input() scrollParent;
    @Input() labelClass;
    @Input() labelInline;
    @Input() labelKey;
    @Input() labelDescription;
    @Input() labelStyle;
    @Input() loadExternalUserOptions;
    @Input() loadUserOptionsOnClick;
    @Input() userOptions;
    @Input() orgId;
    @Input() otherOptionPrependText;
    @Input() model;
    @Input() permissionKeys;
    @Input() placeholder;
    @Input() removeCurrentUser;
    @Input() traversal;
    @Input() tooltipPosition;
    @Input() type;
    @Input() wrapperClass;
    @Input() validations;
    @Input() identifier;
    @Input() filterCurrentUser;
    @Input() allowInvalidPrepopulated;

    @Output() onSelect;

    constructor(elementRef: ElementRef, injector: Injector) {
        super("orgUserDropdown", elementRef, injector);
    }
}
