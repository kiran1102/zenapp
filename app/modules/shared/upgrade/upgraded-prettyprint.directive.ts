import { Directive, ElementRef, Injector, Input } from "@angular/core";
import { UpgradeComponent } from "@angular/upgrade/static";

@Directive({
    selector: "upgraded-prettify",
})
export class UpgradedPrettify extends UpgradeComponent {

    constructor(elementRef: ElementRef, injector: Injector) {
        super("prettify", elementRef, injector);
    }
}
