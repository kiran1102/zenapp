import { Directive, ElementRef, Injector, Input, Output } from "@angular/core";
import { UpgradeComponent } from "@angular/upgrade/static";

@Directive({ selector: "upgraded-one-nav-header" })
export class UpgradedOneNavHeader extends UpgradeComponent {

    @Input() body;
    @Input() breadcrumbClass;
    @Input() breadcrumbRoute;
    @Input() breadcrumbText;
    @Input() headerClass;
    @Input() isDisabled;
    @Input() title;
    @Input() titleClass;
    @Input() useBreadcrumbService;

    constructor(elementRef: ElementRef, injector: Injector) {
        super("oneNavHeader", elementRef, injector);
    }
}
