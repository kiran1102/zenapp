import { Directive, ElementRef, Injector, Input, Output } from "@angular/core";
import { UpgradeComponent } from "@angular/upgrade/static";

@Directive({ selector: "upgraded-one-checkbox" })
export class UpgradedOneCheckboxDirective extends UpgradeComponent {
    constructor(elementRef: ElementRef, injector: Injector) {
        super("oneCheckbox", elementRef, injector);
    }

    @Input() isSelected;
    @Input() isDisabled;
    @Input() selectType;
    @Input() type;
    @Input() identifier;
    @Input() text;
    @Output() toggle;
}
