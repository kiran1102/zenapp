import { Directive, ElementRef, Injector, Input } from "@angular/core";
import { UpgradeComponent } from "@angular/upgrade/static";

@Directive({ selector: "upgraded-one-tabs-nav" })
export class UpgradedOneTabsNav extends UpgradeComponent {

    @Input() selectedTab;
    @Input() options;
    @Input() customClass;

    constructor(elementRef: ElementRef, injector: Injector) {
        super("oneTabsNav", elementRef, injector);
    }
}
