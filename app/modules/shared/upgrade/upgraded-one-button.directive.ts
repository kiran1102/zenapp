import { Directive, ElementRef, Injector, Input, Output } from "@angular/core";
import { UpgradeComponent } from "@angular/upgrade/static";

@Directive({ selector: "upgraded-one-button" })
export class UpgradedOneButtonDirective extends UpgradeComponent {
    constructor(elementRef: ElementRef, injector: Injector) {
        super("oneButton", elementRef, injector);
    }

    @Input() buttonClass;
    @Input() buttonTextClass;
    @Input() disableTabbing;
    @Input() enableLoading;
    @Input() icon;
    @Input() iconClass;
    @Input() rightSideIcon;
    @Input() rightSideIconClass;
    @Input() hasNoBorder;
    @Input() identifier;
    @Input() isDisabled;
    @Input() isSelected;
    @Input() isLoading;
    @Input() isCircular;
    @Input() isRounded;
    @Input() isFloating;
    @Input() text;
    @Input() type;
    @Input() wrapText;
    @Input() focus;
    @Input() buttonStyle;
    @Output() buttonClick;
}
