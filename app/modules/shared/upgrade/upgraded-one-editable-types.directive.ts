import { Directive, ElementRef, Injector, Input, Output, EventEmitter } from "@angular/core";
import { UpgradeComponent } from "@angular/upgrade/static";

@Directive({
    selector: "upgraded-one-editable-types",
})
export class UpgradedOneEditableTypes extends UpgradeComponent {

    @Input() buttonType;
    @Input() config;
    @Input() identifier;
    @Input() isButtonRounded;
    @Input() isDisabled;
    @Input() model;
    @Input() options;
    @Input() buttonClass;

    @Output() handleEdit: EventEmitter<any>;

    constructor(elementRef: ElementRef, injector: Injector) {
        super("oneEditableTypes", elementRef, injector);
    }
}
