
import { Directive, ElementRef, Injector, Input, Output } from "@angular/core";
import { UpgradeComponent } from "@angular/upgrade/static";

@Directive({ selector: "upgraded-project-graph" })
export class UpgradedProjectGraph extends UpgradeComponent {
    @Input() graphData;

    constructor(elementRef: ElementRef, injector: Injector) {
        super("projectGraph", elementRef, injector);
    }
}
