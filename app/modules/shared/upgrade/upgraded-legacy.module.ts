import { NgModule } from "@angular/core";

// Upgraded Legacy
import { UpgradedOneDatePicker } from "./upgraded-one-datepicker.directive";
import { UpgradedOnePagination } from "./upgraded-one-pagination.directive";
import { UpgradedOneDropdown } from "./upgraded-one-dropdown.directive";
import { UpgradedOneDate } from "./upgraded-one-date.directive";
import { UpgradedLoading } from "./upgraded-loading.directive";
import { UpgradedOnePopupToggle } from "./upgraded-one-popup-toggle.directive";
import { UpgradedOrgUserDropdown } from "./upgraded-org-user-dropdown.directive";
import { UpgradedOneTag } from "./upgraded-one-tag.directive";
import { UpgradedOneButtonDirective } from "./upgraded-one-button.directive";
import { UpgradedOneRichTextEditorDirective } from "./upgraded-one-rich-text-editor.directive";
import { UpgradedOneCheckboxDirective } from "./upgraded-one-checkbox.directive";
import { UpgradedSingleSelectDropdown } from "./upgraded-single-select-dropdown.directive";
import { UpgradedOneEditableTypes } from "./upgraded-one-editable-types.directive";
import { UpgradedOneEditableText } from "./upgraded-one-editable-text.directive";
import { UpgradedRiskActionButtons } from "./upgraded-risk-action-buttons.directive";
import { UpgradedOneNavHeader } from "./upgraded-one-nav-header.directive";
import { UpgradedOneTabsNav } from "./upgraded-one-tabs-nav.directive";
import { UpgradedTypeahead } from "./upgraded-typeahead.directive";
import { UpgradedOneMultiselect } from "./upgraded-one-multiselect.directive";
import { UpgradedPillList } from "./upgraded-pill-list.directive";
import { UpgradedOneLabelContent } from "./upgraded-one-label-content.directive";
import { UpgradedPrettify } from "./upgraded-prettyprint.directive";
import { UpgradedOneNumberedCircle } from "./upgraded-one-numbered-circle.directive";
import { UpgradedReactiveDropdown } from "./upgraded-reactive-dropdown.directive";
import { UpgradedOneDonutGraph } from "./upgraded-one-donut-graph.directive";
import { UpgradedProjectGraph } from "./upgraded-project-graph.directive";
import { UpgradedOneBackgroundMessage } from "./upgraded-one-background-message.directive";
import { UpgradedOnePopupFooter } from "./upgraded-one-popup-footer.directive";
import { UpgradedDeleteConfirmationModal } from "./upgraded-delete-confirmation-modal.directive";
import { UpgradedSideDrawer } from "./upgraded-side-drawer.directive";
import { UpgradedActionIcons } from "./upgraded-action-icons.directive";
import { UpgradedOneSelect } from "./upgraded-one-select.directive";
import { UpgradedOneSidebarMenuDirective } from "./upgraded-one-sidebar-menu.directive";

@NgModule({
    declarations: [
        UpgradedLoading,
        UpgradedOneBackgroundMessage,
        UpgradedOneButtonDirective,
        UpgradedOneRichTextEditorDirective,
        UpgradedOneCheckboxDirective,
        UpgradedOneDate,
        UpgradedOneDatePicker,
        UpgradedOneDonutGraph,
        UpgradedOneDropdown,
        UpgradedOneEditableTypes,
        UpgradedOneEditableText,
        UpgradedOneLabelContent,
        UpgradedOneMultiselect,
        UpgradedOneNavHeader,
        UpgradedOneNumberedCircle,
        UpgradedOnePagination,
        UpgradedOneTabsNav,
        UpgradedOneTag,
        UpgradedOrgUserDropdown,
        UpgradedPillList,
        UpgradedPrettify,
        UpgradedProjectGraph,
        UpgradedReactiveDropdown,
        UpgradedRiskActionButtons,
        UpgradedSingleSelectDropdown,
        UpgradedTypeahead,
        UpgradedOnePopupFooter,
        UpgradedDeleteConfirmationModal,
        UpgradedSideDrawer,
        UpgradedActionIcons,
        UpgradedOnePopupToggle,
        UpgradedOneSidebarMenuDirective,
        UpgradedOneSelect,
    ],
    exports: [
        UpgradedLoading,
        UpgradedOneBackgroundMessage,
        UpgradedOneButtonDirective,
        UpgradedOneRichTextEditorDirective,
        UpgradedOneCheckboxDirective,
        UpgradedOneDate,
        UpgradedOneDatePicker,
        UpgradedOneDonutGraph,
        UpgradedOneDropdown,
        UpgradedOneEditableTypes,
        UpgradedOneEditableText,
        UpgradedOneLabelContent,
        UpgradedOneMultiselect,
        UpgradedOneNavHeader,
        UpgradedOneNumberedCircle,
        UpgradedOnePagination,
        UpgradedOneTabsNav,
        UpgradedOneTag,
        UpgradedOrgUserDropdown,
        UpgradedPillList,
        UpgradedPrettify,
        UpgradedProjectGraph,
        UpgradedReactiveDropdown,
        UpgradedRiskActionButtons,
        UpgradedSingleSelectDropdown,
        UpgradedTypeahead,
        UpgradedOnePopupFooter,
        UpgradedDeleteConfirmationModal,
        UpgradedSideDrawer,
        UpgradedActionIcons,
        UpgradedOnePopupToggle,
        UpgradedOneSidebarMenuDirective,
        UpgradedOneSelect,
    ],
})
export class UpgradedLegacyModule { }
