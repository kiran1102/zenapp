import { Directive, ElementRef, Injector, Input } from "@angular/core";
import { UpgradeComponent } from "@angular/upgrade/static";

@Directive({
    selector: "upgraded-loading",
})
export class UpgradedLoading extends UpgradeComponent {

    @Input() public iconClass;
    @Input() public image;
    @Input() public imageClass;
    @Input() public loadingClass;
    @Input() public showEllipsis;
    @Input() public showIcon;
    @Input() public showImage;
    @Input() public showLoadingPrefix;
    @Input() public showLoadingSuffix;
    @Input() public showText;
    @Input() public text;

    constructor(elementRef: ElementRef, injector: Injector) {
        super("loading", elementRef, injector);
    }
}
