import { Directive, ElementRef, Injector, Input, Output, EventEmitter } from "@angular/core";
import { UpgradeComponent } from "@angular/upgrade/static";

@Directive({
    selector: "upgraded-single-select-dropdown",
})
export class UpgradedSingleSelectDropdown extends UpgradeComponent {

    constructor(elementRef: ElementRef, injector: Injector) {
        super("singleSelectDropdownQuestion", elementRef, injector);
    }

    @Input() appendToBody;
    @Input() question;
    @Input() readOnly;
    @Output() onChange: EventEmitter<any> = new EventEmitter<any>();
}
