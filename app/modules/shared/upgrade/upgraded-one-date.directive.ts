import { Directive, ElementRef, Injector, Input } from "@angular/core";
import { UpgradeComponent } from "@angular/upgrade/static";

@Directive({
    selector: "upgraded-one-date",
})
export class UpgradedOneDate extends UpgradeComponent {

    @Input() dateToFormat;
    @Input() dateTooltipPlacement;
    @Input() enableTooltip;
    @Input() fallBackToValue;

    constructor(elementRef: ElementRef, injector: Injector) {
        super("oneDate", elementRef, injector);
    }
}
