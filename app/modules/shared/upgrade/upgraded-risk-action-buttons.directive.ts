import { Directive, ElementRef, Injector, Input } from "@angular/core";
import { UpgradeComponent } from "@angular/upgrade/static";

@Directive({
    selector: "upgraded-risk-action-buttons",
})
export class UpgradedRiskActionButtons extends UpgradeComponent {

    constructor(elementRef: ElementRef, injector: Injector) {
        super("oneRiskActionButtons", elementRef, injector);
    }

    @Input() onOpenRiskModal;
    @Input() onOpenDeleteModal;
    @Input() onRiskLevelChange;
    @Input() onRiskDropdownToggle;
    @Input() onRiskTileChange;
    @Input() heatmapEnabled;
    @Input() axisLabels;
    @Input() tileMap;
    @Input() enableLevelChange;
    @Input() enableRiskEdit;
    @Input() enableRiskDelete;
    @Input() alignRight;
}
