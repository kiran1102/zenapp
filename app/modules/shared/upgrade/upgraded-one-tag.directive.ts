import { Directive, ElementRef, Injector, Input } from "@angular/core";
import { UpgradeComponent } from "@angular/upgrade/static";

@Directive({
    selector: "upgraded-one-tag",
})
export class UpgradedOneTag extends UpgradeComponent {

    constructor(elementRef: ElementRef, injector: Injector) {
        super("oneTag", elementRef, injector);
    }

    @Input() color;
    @Input() labelClass;
    @Input() plainText;
    @Input() size;
    @Input() translatedText;
    @Input() iconClass;
    @Input() icon;
    @Input() wrapperClass;
    @Input() backgroundColor;
}
