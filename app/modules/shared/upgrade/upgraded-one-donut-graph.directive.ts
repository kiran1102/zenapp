import { Directive, ElementRef, Injector, Input, Output } from "@angular/core";
import { UpgradeComponent } from "@angular/upgrade/static";

@Directive({
    selector: "upgraded-one-donut-graph",
})
export class UpgradedOneDonutGraph extends UpgradeComponent {
    @Input() centerLabel;
    @Input() disablePercentage;
    @Input() donutData;
    @Input() donutName;
    @Input() graphId;
    @Input() innerRadius;
    @Input() margin;
    @Input() outerRadius;
    @Input() sortArcs;
    @Output() donutClickCallback;

    constructor(elementRef: ElementRef, injector: Injector) {
        super("oneDonutGraph", elementRef, injector);
    }
}
