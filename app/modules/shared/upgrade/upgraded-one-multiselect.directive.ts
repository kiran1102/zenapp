import { Directive, ElementRef, Injector, Input, Output, EventEmitter } from "@angular/core";
import { UpgradeComponent } from "@angular/upgrade/static";

@Directive({
    selector: "upgraded-one-multiselect",
})
export class UpgradedOneMultiselect extends UpgradeComponent {

    constructor(elementRef: ElementRef, injector: Injector) {
        super("oneMultiselect", elementRef, injector);
    }

    @Input() allowEmpty;
    @Input() allowOther;
    @Input() allowOtherLabel;
    @Input() allowOtherLabelAppend;
    @Input() allowSorting;
    @Input() appendToBody;
    @Input() containerClass;
    @Input() emptyOptionText;
    @Input() emptyOptionLast;
    @Input() identifier;
    @Input() isDisabled;
    @Input() labelKey;
    @Input() list;
    @Input() model;
    @Input() openOnInit;
    @Input() orderByLabel;
    @Input() placeholder;
    @Input() pillTabbing;
    @Input() isParentModal;
    @Input() throttleValue;
    @Output() onBlur: EventEmitter<any>;
    @Output() handleRemove: EventEmitter<any>;
    @Output() handleSelect: EventEmitter<any>;
}
