// 3rd party
import {
    Component,
    Input,
    Output,
    EventEmitter,
} from "@angular/core";

// Interfaces
import { IAction } from "interfaces/redux.interface";
import { ISettingsMeta } from "sharedModules/interfaces/settings.interface";

// Enums and Constants
import { SettingsType } from "sharedModules/enums/settings.enum";

@Component({
    selector: "settings-generator",
    templateUrl: "./settings-generator.component.html",
})
export class SettingsGeneratorComponent {

    @Input() settingMeta;
    @Input() settings;
    @Input() isDisabled = false;
    @Output() handleSettingAction = new EventEmitter<IAction>();

    settingsType = SettingsType;

    handleAction(event: any, type: string, minValue?: number, maxValue?: number) {
        if (minValue || maxValue) {
            event.minValue = minValue;
            event.maxValue = maxValue;
        }
        if (event.payload) {
            this.handleSettingAction.emit({
                payload: {
                    event: event.payload.event,
                },
                type: event.type,
            });
        } else {
            this.handleSettingAction.emit({
                payload: {
                    event,
                },
                type,
            });
        }
    }

    trackByFn(index: number, setting: ISettingsMeta): string {
        return setting.title;
    }
}
