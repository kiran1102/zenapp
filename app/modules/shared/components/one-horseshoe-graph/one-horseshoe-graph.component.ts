import { Component, Input, Output, OnInit, EventEmitter } from "@angular/core";
import { each } from "lodash";

// Interfaces
import { IHorseshoeGraphData, IHorseShoeSeries } from "interfaces/one-horseshoe-graph.interface";

@Component({
    selector: "one-horseshoe-graph",
    templateUrl: "./one-horseshoe-graph.component.html",
})
export class OneHorseshoeGraph implements OnInit {
    @Input() title: string;
    @Input() titleClass: string;
    @Input() subTitle: string;
    @Input() subTitleClass: string;
    @Input() countText: string;
    @Input() countClass: string;
    @Input() legendClass: string;
    @Input() size = 200;
    @Input() stroke = 30;
    @Input() detailsText: string;
    @Input() showDetailsButton = true;
    @Output() onDetailsClick: EventEmitter<void> = new EventEmitter<void>();

    circumference: number;
    graphRadius: number;
    shortCirc: number;

    private _graphData: IHorseshoeGraphData;

    get graphData(): IHorseshoeGraphData {
        return this._graphData;
    }

    @Input()
    set graphData(graphData: IHorseshoeGraphData) {
        this._graphData = graphData;
        this.setOffset();
    }

    ngOnInit(): void {
        this.calcGraph();
        this.setOffset();
    }

    detailsClicked(): void {
        this.onDetailsClick.emit();
    }

    private calcGraph(): void {
        this.graphRadius = (this.size / 2) - (this.stroke / 2);
        this.circumference = 2 * Math.PI * this.graphRadius;
        this.shortCirc = this.circumference * .75;
    }

    private setOffset(): void {
        const graphList: IHorseShoeSeries[] = this.graphData.series;
        let start = 0;

        each(graphList, (item: IHorseShoeSeries): void => {
            const itemLen: number = this.shortCirc * item.value;
            item.dashOffset = start;
            item.dashArray = `${itemLen}  ${this.circumference}`;
            start -= itemLen;
        });
    }
}
