// 3rd party
import {
    Component,
    Input,
    Output,
    EventEmitter,
} from "@angular/core";

// Interfaces
import { IAction } from "interfaces/redux.interface";
import { ISettingsMeta } from "sharedModules/interfaces/settings.interface";

// Enums and Constants
import { SettingsType } from "sharedModules/enums/settings.enum";
import { KeyCodes } from "enums/key-codes.enum";

@Component({
    selector: "general-settings-generator",
    templateUrl: "./general-settings-generator.component.html",
})
export class GeneralSettingsGeneratorComponent {

    @Input() settingMeta;
    @Input() settings;
    @Output() handleSettingAction = new EventEmitter<IAction>();

    settingsType = SettingsType;
    fieldSearchTerm = "";

    handleAction(event: any, type: string, minValue?: number, maxValue?: number) {
        if (minValue || maxValue) {
            event.minValue = minValue;
            event.maxValue = maxValue;
        }
        this.handleSettingAction.emit({
            payload: event,
            type,
        });
    }

    fieldSearch({ keyCode, value }) {
        if (keyCode === KeyCodes.Enter) return;
        this.fieldSearchTerm = value || "";
    }

    trackByFn(index: number, setting: ISettingsMeta): string {
        return setting.title;
    }
}
