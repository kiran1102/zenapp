import {
    Component,
    OnInit,
    Inject,
} from "@angular/core";

// Services
import { ModalService } from "sharedServices/modal.service";
import { AppMaintenanceAlertsService } from "sharedModules/services/api/app-maintenance-alerts.service";

// Interfaces
import { IBannerAlertData } from "interfaces/app-maintenance-alerts.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";

// Constants
import { MaintenanceModalMoreInfo } from "constants/app-maintenance-alerts.constant";

// Redux
import { IStore } from "interfaces/redux.interface";
import { StoreToken } from "tokens/redux-store.token";
import { getModalData } from "oneRedux/reducers/modal.reducer";

@Component({
    selector: "app-maintenance-alerts-modal",
    templateUrl: "./app-maintenance-alerts-modal.component.html",
})

export class AppMaintenanceAlertsModal implements OnInit {
    maintenanceData: IBannerAlertData;
    isLoading = false;
    modalIcon = "ot ot-cloud-upload";
    modalIconTextClass = "color-orange";
    fromDate: Date;
    toDate: Date;
    scheduleId: string;
    moreInfoLink = MaintenanceModalMoreInfo.MoreInfoLink;
    private doNotShowCheckboxValue = false;

    constructor(
        @Inject(StoreToken) private store: IStore,
        private modalService: ModalService,
        private appMaintenanceAlertsService: AppMaintenanceAlertsService,
    ) {}

    ngOnInit(): void {
        const bannerAlertData: IBannerAlertData = getModalData(this.store.getState());
        if (bannerAlertData) {
            this.handleNotificationData(bannerAlertData);
            this.modalService.clearModalData();
        } else {
            this.appMaintenanceAlertsService.getAlertNotifications().then((response: IProtocolResponse<IBannerAlertData>): void => {
                if (response.data) {
                    this.handleNotificationData(response.data);
                }
            });
        }
        this.modalIcon = this.modalIcon;
        this.modalIconTextClass = this.modalIconTextClass;
    }

    closeModal(): void {
        if (this.doNotShowCheckboxValue) {
            this.appMaintenanceAlertsService.updateAlertNotifications(this.scheduleId);
        }
        this.modalService.closeModal();
        this.modalService.clearModalData();
    }

    doNotShowCheckBox(event: boolean): void {
        this.doNotShowCheckboxValue = event;
    }

    private handleNotificationData(notificationData: IBannerAlertData) {
        this.maintenanceData = notificationData;
        this.fromDate = notificationData.maintenanceScheduleAttributes.MaintenanceStartTime;
        this.toDate = notificationData.maintenanceScheduleAttributes.MaintenanceEndTime;
        this.scheduleId = notificationData.alertScheduleId;
        this.isLoading = true;
    }
}
