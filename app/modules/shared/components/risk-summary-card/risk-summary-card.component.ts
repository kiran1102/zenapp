// Rxjs
import { Subject, fromEvent } from "rxjs";
import {
    debounceTime,
    takeUntil,
} from "rxjs/operators";

// 3rd party
import {
    Component,
    Inject,
    Output,
    EventEmitter,
    Input,
    ElementRef,
} from "@angular/core";

// Interfaces
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import { IRiskDetails } from "interfaces/risk.interface";
import { INumberMap } from "interfaces/generic.interface";

// Constants + Enums
import {
    RiskLabelBgClasses,
    RiskLevels,
    RiskTranslations,
    RiskLabelBgClassesV2,
} from "enums/risk.enum";

@Component({
    selector: "risk-summary-card",
    templateUrl: "./risk-summary-card.component.html",
})
export class RiskSummaryCardComponent {

    @Input() risk;
    @Input() canDeleteRisk: boolean;
    @Input() canChangeRiskLevel: boolean;
    @Input() canEditRisk: boolean;

    @Output() public openInModal: EventEmitter<IRiskDetails> = new EventEmitter<IRiskDetails>();
    @Output() public delete: EventEmitter<IRiskDetails> = new EventEmitter<IRiskDetails>();
    @Output() public changeLevel: EventEmitter<{risk: IRiskDetails, levelId: number}> = new EventEmitter<{risk: IRiskDetails, levelId: number}>();

    RiskClasses = RiskLabelBgClasses;
    RiskLabelBgClassesV2 = RiskLabelBgClassesV2;
    RiskLevels = RiskLevels;
    RiskTranslations = RiskTranslations;
    hasOverflow: boolean;
    showingOverflow: boolean;

    private destroy$ = new Subject();
    private expandContainer: HTMLElement;

    constructor(
        private element: ElementRef,
    ) {
        fromEvent(window, "resize").pipe(
            debounceTime(300),
            takeUntil(this.destroy$),
        ).subscribe(() => this.setOverflowClass());
    }

    ngOnInit(): void {
        this.expandContainer = this.element.nativeElement.querySelectorAll(".risk-summary__expandable-content")[0];
        this.setOverflowClass();
    }

    ngOnChanges(): void {
        this.setOverflowClass();
    }

    openRiskModal = (risk: IRiskDetails): void => {
        this.openInModal.emit(this.risk);
    }

    openDeleteModal = (risk: IRiskDetails): void => {
        this.delete.emit(this.risk);
    }

    changeRiskLevel = (): (param: { value: number }) => void => {
        return (param: { value: number }): void => {
            this.changeLevel.emit({risk: this.risk, levelId: param.value});
        };
    }

    toggleOverflow(): void {
        this.showingOverflow = !this.showingOverflow;
    }

    ngOnDestroy(): void {
        this.destroy$.next();
        this.destroy$.complete();
    }

    private setOverflowClass(): void {
        setTimeout((): void => {
            this.hasOverflow = this.expandContainer.offsetHeight < this.expandContainer.scrollHeight;
            this.showingOverflow = this.hasOverflow ? this.showingOverflow : false;
        }, 0);
    }
}
