// Deprecated - use "ot-notification-modal"

// 3rd party
import {
    Component,
    Inject,
} from "@angular/core";

// Redux
import { getModalData } from "oneRedux/reducers/modal.reducer";

// Services
import { ModalService } from "sharedServices/modal.service";

// Tokens
import { StoreToken } from "tokens/redux-store.token";

import { IStore} from "interfaces/redux.interface";
import { ITaskConfirmationModalResolve } from "interfaces/task-confirmation-modal-resolve.interface";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

@Component({
    selector: "one-notification-modal",
    templateUrl: "./one-notification-modal.component.html",
})
export class OneNotificationModalComponent {
    modalData: ITaskConfirmationModalResolve;
    modalBody: string;
    modalTitle: string;
    closeText: string;
    imgSrc: string;

    constructor(
        @Inject(StoreToken) private store: IStore,
        private translatePipe: TranslatePipe,
        private modalService: ModalService,
    ) {}

    ngOnInit(): void {
        this.modalData = getModalData(this.store.getState());
        this.modalTitle = this.modalData.modalTitle;
        this.modalBody  = this.modalData.confirmationText;
        this.closeText = this.modalData.closeButtonText || this.translatePipe.transform("Close");
        this.imgSrc = this.modalData.imgSrc || "images/task-notification.png";
    }

    closeModal(): void {
        this.modalService.closeModal();
        this.modalService.clearModalData();
    }
}
