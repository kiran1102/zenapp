// Angular
import { Component, OnInit } from "@angular/core";

// Vitreus
import { IOtModalContent } from "@onetrust/vitreus";

// Rxjs
import { Subject } from "rxjs";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

// Interfaces
import { IModalData } from "interfaces/modal.interface";

@Component({
    selector: "unsaved-changes-confirmation-modal",
    templateUrl: "./unsaved-changes-confirmation-modal.component.html",
})
export class UnsavedChangesConfirmationModal implements IOtModalContent, OnInit {

    otModalCloseEvent: Subject<boolean>;
    otModalData: IModalData;
    isSaving = false;
    modalTitle: string;
    descText: string;
    submitButtonText: string;
    cancelButtonText: string;

    constructor(
        private translatePipe: TranslatePipe,
    ) { }

    ngOnInit() {
        this.modalTitle = this.otModalData.modalTitle || this.translatePipe.transform("SaveChanges");
        this.descText = this.otModalData.descText || this.translatePipe.transform("UnsavedChangesSaveBeforeContinuing");
        this.submitButtonText = this.otModalData.saveButton || this.translatePipe.transform("SaveAndContinue");
        this.cancelButtonText = this.otModalData.cancelButtonText || this.translatePipe.transform("DiscardChanges");
    }

    onSaveAndContinue() {
        this.isSaving = true;
        this.otModalData.promiseToResolve();
        this.cancel(true);
        this.isSaving = false;
    }

    onDiscard() {
        this.isSaving = true;
        this.otModalData.discardCallback();
        this.cancel(true);
        this.isSaving = false;
    }

    cancel(success: boolean) {
        this.otModalCloseEvent.next(success);
        this.otModalCloseEvent.complete();
      }
}
