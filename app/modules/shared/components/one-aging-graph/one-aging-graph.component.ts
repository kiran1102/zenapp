declare var d3: any;

// Angular
import {
    Component,
    Input,
    Output,
    OnInit,
    OnChanges,
    EventEmitter,
    SimpleChanges,
} from "@angular/core";

// 3rd Party
import {
    isArray,
    each,
} from "lodash";

// Constants
import { Color } from "constants/color.constant";
import { AssessmentStateKeys } from "constants/assessment.constants";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

// Interfaces
import { IGraphDatum } from "../../interfaces/dashboard.interface";

@Component({
    selector: "one-aging-graph",
    template: `<div id="one-aging-graph"></div>`,
})
export class OneAgingGraph implements OnInit, OnChanges {
    @Input() graphData;
    @Input() graphId;
    @Input() projectTerm;
    @Input() width = 640;
    @Input() xAxisRange: string[] = [];
    @Output() assessmentDetailsChanged: EventEmitter<{ value: Array<{ title: string, value: string }> }> = new EventEmitter<{ value: Array<{ title: string, value: string }> }>();
    @Output() assessmentNavigationCallback: EventEmitter<{ id: string, version: number }> = new EventEmitter<{ id: string, version: number }>();

    private height = 270;
    private margins = {
        left: 50,
        right: 100,
        top: 40,
        bottom: 50,
    };
    private colorMap = {
        [AssessmentStateKeys.NotStarted]: Color.Grey6,
        [AssessmentStateKeys.InProgress]: Color.Blue,
        [AssessmentStateKeys.UnderReview]: Color.Orange,
        [AssessmentStateKeys.InfoNeeded]: Color.Teal,
        [AssessmentStateKeys.RiskAssessment]: Color.Yellow,
        [AssessmentStateKeys.RiskTreatment]: Color.Steel,
    };
    private statusMap = {
        [AssessmentStateKeys.NotStarted]: "1",
        [AssessmentStateKeys.InProgress]: "2",
        [AssessmentStateKeys.UnderReview]: "3",
        [AssessmentStateKeys.InfoNeeded]: "4",
        [AssessmentStateKeys.RiskAssessment]: "5",
        [AssessmentStateKeys.RiskTreatment]: "6",
    };
    private statusMapArray = [
        AssessmentStateKeys.NotStarted,
        AssessmentStateKeys.InProgress,
        AssessmentStateKeys.UnderReview,
        AssessmentStateKeys.InfoNeeded,
        AssessmentStateKeys.RiskAssessment,
        AssessmentStateKeys.RiskTreatment,
    ];

    constructor(private translatePipe: TranslatePipe) {}

    ngOnInit() {
        this.updateAgingGraph();
    }

    ngOnChanges(changes: SimpleChanges) {
        if (changes.graphData && changes.graphData.currentValue) {
            this.graphData = changes.graphData.currentValue;
        }
        if (isArray(this.graphData) && this.graphData.length > 0) {
            this.initializeGraphData();
            this.drawGraph(this.graphData, this.CalcYRange(this.graphData));
        }
    }

    updateAgingGraph(filter: number | "" = "") {
        // remove the current aging project graph
        d3.select(`#${this.graphId} svg`).remove();

        if (filter !== "") {
            const filteredArray = [];
            this.initializeGraphData();
            each(this.graphData, (project: any): void => {
                if (project.DaysAgo <= filter) {
                    filteredArray.push(project);
                }
            });

            const yAxisRange = [this.CalcYRange(filteredArray)[0], filter];

            this.drawGraph(filteredArray, yAxisRange);

        } else {
            this.initializeGraphData();
            this.drawGraph(this.graphData, this.CalcYRange(this.graphData));

        }
    }

    private initializeGraphData() {
        const minuteMS = 60 * 1000;
        const hourMS = 60 * minuteMS;
        const dayMS: any = 24 * hourMS;
        each(this.graphData, (d: IGraphDatum): void => {
            d.DaysAgo = Math.floor(d.Time / dayMS);
            d.HoursAgo = Math.floor(((d.Time - (d.DaysAgo * dayMS)) / hourMS));
            d.MinutesAgo = Math.floor(((d.Time - (d.DaysAgo * dayMS) - (d.HoursAgo * hourMS)) / minuteMS));
        });
    }

    private drawGraph(data, yAxisRange) {
        d3.select(`#${this.graphId} > *`).remove();

        // this will be our colour scale. An Ordinal scale.
        const colors = d3.scale.category10();

        // we add the SVG component to the scatter-load div
        const svg = d3.select(`#${this.graphId}`)
            .append("svg")
            .attr("width", this.width)
            .attr("height", this.height)
            .append("g")
            .attr("transform", "translate(" + this.margins.left + "," + this.margins.top + ")");

        // this sets the scale that we're using for the X axis.
        // the domain define the min and max variables to show. In this case, it's the series of states.
        const x = d3.scale.linear()
            .domain(this.CalcXRange())
            // the range maps the domain to values from 0 to the width minus the left and right margins (used to space out the visualization)
            .range([0, this.width - this.margins.left - this.margins.right]);

        // this does the same as for the y axis but maps from the rating variable to the height to 0.
        const y = d3.scale.linear()
            .domain(yAxisRange)
            // Note that height goes first due to the weird SVG coordinate system
            .range([this.height - this.margins.top - this.margins.bottom, 0]);

        // we add the axes SVG component. At this point, this is just a placeholder. The actual axis will be added in a bit
        svg.append("g").attr("class", "x axis").attr("transform", "translate(0," + y.range()[0] + ")");
        svg.append("g").attr("class", "y axis");

        // this is our Y axis label. Nothing too special to see here.
        svg.append("text")
            .attr("fill", "#696969")
            .style("font-weight", "bold")
            .attr("text-anchor", "end")
            .attr("y", -20)
            .attr("x", -10)
            .text("(" + this.translatePipe.transform("Days") + ")");

        // this is the actual definition of our x and y axes. The orientation refers to where the labels appear - for the x axis, below or above the line, and for the y axis, left or right of the line. Tick padding refers to how much space between the tick and the label. There are other parameters too - see https://github.com/mbostock/d3/wiki/SVG-Axes for more information
        const xAxis = d3.svg.axis()
            .scale(x)
            .orient("bottom")
            .tickPadding(2)
            .tickFormat((d) => this.translatePipe.transform(this.statusMapArray[d - 1]));

        const yAxis = d3.svg.axis()
            .scale(y)
            .orient("left")
            .ticks(5);

        // this is where we select the axis we created a few lines earlier. See how we select the axis item. in our svg we appended a g element with a x/y and axis class. To pull that back up, we do this svg select, then 'call' the appropriate axis object for rendering.
        svg.selectAll("g.y.axis").call(yAxis)
            .attr("fill", "#696969");
        svg.selectAll("g.x.axis").call(xAxis)
            .attr("fill", "#696969");
        svg.selectAll("g.x.axis").call(xAxis).selectAll("text")
            .attr("y", 10)
            .attr("x", 0)
            .attr("transform", "rotate(15)")
            .style("text-anchor", "start")
            .style("font-weight", "bold");
        svg.selectAll("g.y.axis").call(yAxis).selectAll("text")
            .style("font-weight", "bold");

        // now, we can get down to the data part, and drawing stuff. We are telling D3 that all nodes (g elements with class node) will have data attached to them. The 'key' we use (to let D3 know the uniqueness of items) will be the name. Not usually a great key, but fine for this example.
        const chocolate = svg.selectAll("g.node").data(data, (d) => {
            return d.Day;
        });

        // we 'enter' the data, making the SVG group (to contain a circle and text) with a class node. This corresponds with what we told the data it should be above.

        const chocolateGroup = chocolate.enter().append("g").attr("class", "node")
            // this is how we set the position of the items. Translate is an incredibly useful function for rotating and positioning items
            .attr("transform", (d) => {
                return "translate(" + (x(this.statusMap[d.StateDesc]) + 5) + "," + y(d.Day) + ")";
            });

        // we add our first graphics element! A circle!
        chocolateGroup.append("circle")
            .attr("r", 5)
            .attr("class", "dot grow")
            .style("fill", (d) => {
                // remember the ordinal scales? We use the colors scale to get a colour for our manufacturer. Now each node will be coloured
                // by who makes the chocolate.
                return this.colorMap[d.StateDesc];
            })
            .on("mouseover", (d) => {
                const projectDetails: any[] = [];
                projectDetails.push({ title: `${this.projectTerm}  ${this.translatePipe.transform("Name")}`, value: d.Name });
                projectDetails.push({ title: `${this.projectTerm}  ${this.translatePipe.transform("Status")}`, value: d.StateName });
                projectDetails.push({ title: this.translatePipe.transform("StatusDuration"), value: this.ReturnDayTime(d) });
                projectDetails.push({ title: this.translatePipe.transform("OrganizationGroup"), value: d.OrgGroup });
                this.assessmentDetailsChanged.emit({ value: projectDetails });
            })
            .on("mouseout", (d) => {
                const projectDetails: any[] = [];
                this.assessmentDetailsChanged.emit({ value: projectDetails });
            })
            .on("click", (d) => {
                this.assessmentNavigationCallback.emit({ id: d.Id, version: d.Version });
                d3.event.stopPropagation();
            });
    }

    private CalcXRange(): string[] {
        if (this.xAxisRange && this.xAxisRange.length) {
            return [this.statusMap[this.xAxisRange[0]], this.statusMap[this.xAxisRange[1]]];
        } else {
            return [this.statusMap[AssessmentStateKeys.InProgress], this.statusMap[AssessmentStateKeys.RiskTreatment]];
        }
    }

    private CalcYRange(data) {
        const arr = d3.extent(data, (d) => {
            return d.Day;
        });
        arr[0] = 0;
        arr[1] = Math.ceil(arr[1]);
        if (arr[1] < 4) {
            arr[0] = 0;
            arr[1] = 4;
        }

        return arr;
    }

    private ReturnDayTime(obj) {
        const days = obj.DaysAgo;
        const hours = obj.HoursAgo;
        const minutes = obj.MinutesAgo;
        const daysString = (days > 1) ? (days + " " + this.translatePipe.transform("Days")) : (days + " " + this.translatePipe.transform("Day"));
        const hoursString = (hours > 1) ? (hours + " " + this.translatePipe.transform("Hours")) : (hours + " " + this.translatePipe.transform("Hour"));
        const minutesString = (minutes > 1) ? (minutes + " " + this.translatePipe.transform("Minutes")) : (minutes + " " + this.translatePipe.transform("Minute"));

        let dayTimeString = "";

        if (days && hours) {
            dayTimeString += (daysString + " " + hoursString);
        } else if (days) {
            dayTimeString += daysString;
        } else if (hours) {
            dayTimeString += hoursString;
        } else if (minutes) {
            dayTimeString += minutesString;
        } else {
            dayTimeString += this.translatePipe.transform("JustNow");
        }

        return dayTimeString;
    }
}
