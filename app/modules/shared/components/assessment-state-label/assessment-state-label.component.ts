import { Component, Input, OnChanges } from "@angular/core";

import { AssessmentStateKeys } from "constants/assessment.constants";

import { IStringMap } from "interfaces/generic.interface";
import { AssessmentStatus } from "enums/assessment/assessment-status.enum";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

@Component({
    selector: "assessment-state-label",
    templateUrl: "./assessment-state-label.component.html",
})
export class AssessmentStateLabelComponent implements OnChanges {
    translations: IStringMap<string>;
    stateLabel: string;
    stateName: string;
    @Input() stateDescription: string;

    constructor(
        private translatePipe: TranslatePipe,
    ) { }

    ngOnChanges(): void {
        this.setStateNameAndLabels(this.stateDescription);
    }

    public setStateNameAndLabels = (stateDesc: string): void => {
        switch (stateDesc) {
            case AssessmentStatus.Not_Started:
                this.stateLabel = "default";
                this.stateName = this.translatePipe.transform(AssessmentStateKeys.NotStarted);
                break;
            case AssessmentStatus.In_Progress:
                this.stateLabel = "in-progress";
                this.stateName = this.translatePipe.transform(AssessmentStateKeys.InProgress);
                break;
            case AssessmentStatus.Under_Review:
                this.stateLabel = "under-review";
                this.stateName = this.translatePipe.transform(AssessmentStateKeys.UnderReview);
                break;
            case AssessmentStatus.Completed:
                this.stateLabel = "completed";
                this.stateName = this.translatePipe.transform(AssessmentStateKeys.Completed);
                break;
            default:
                this.stateLabel = "default";
                this.stateName = stateDesc;
                break;
        }
    }
}
