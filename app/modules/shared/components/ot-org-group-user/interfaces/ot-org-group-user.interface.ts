export interface IOtLookupSelection<T> {
    previousValue: T;
    currentValue: T;
    change: T;
}

export interface IOtOrgGroupUserOutput {
    selection: IOtOrgGroupUserAdapted;
    valid: boolean;
}

export interface IOtOrgGroupUserAdapted {
    id: string;
    fullName: string;
    type: string;
    isLabel?: boolean;
    errorMessage?: string;
}
