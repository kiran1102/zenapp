export interface IOtOrgGroupUserResponse {
    id: string;
    type: string;
    name: string;
    description: string;
}
