import { IPaginationParams } from "interfaces/pagination.interface";

export interface IOtOrgGroupUserRequest {
    params: IPaginationParams<string>;
    excludeGroups?: boolean;
    search?: string;
    organizationId?: string;
    traversal?: string;
    permissions?: string[];
    excludedPermissions?: string[];
}
