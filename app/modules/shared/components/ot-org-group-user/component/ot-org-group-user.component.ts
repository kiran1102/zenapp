// Angular
import {
    Component,
    Input,
    OnInit,
    OnDestroy,
    Output,
    EventEmitter,
    SimpleChanges,
} from "@angular/core";

// Rxjs
import {
    Subject,
    from,
    BehaviorSubject,
} from "rxjs";
import {
    takeUntil,
    debounceTime,
    tap,
} from "rxjs/operators";

// Services
import { Principal } from "sharedModules/services/helper/principal.service";
import { matchRegex } from "modules/utils/match-regex";
import { OtOrgGroupUserHelperService } from "sharedModules/components/ot-org-group-user/services/ot-org-group-user-helper.service";
import { OtOrgGroupUserApiService } from "sharedModules/components/ot-org-group-user/services/ot-org-group-user-api.service";
import { TranslateService } from "@ngx-translate/core";

// Interfaces
import { IKeyValue, IStringMap } from "interfaces/generic.interface";
import { IUser } from "interfaces/user.interface";
import {
    IOtOrgGroupUserOutput,
    IOtLookupSelection,
    IOtOrgGroupUserAdapted,
} from "sharedModules/components/ot-org-group-user/interfaces/ot-org-group-user.interface";
import { IOtOrgGroupUserResponse } from "sharedModules/components/ot-org-group-user/interfaces/ot-org-group-user-response.interface";
import { IOtOrgGroupUserRequest } from "../interfaces/ot-org-group-user-request.interface";
import { HttpResponse, HttpErrorResponse } from "@angular/common/http";
import { IUserGroupDisplay } from "interfaces/user-groups.interface";
import { IPageableOf } from "interfaces/pagination.interface";

// Constants & Enums
import { Regex } from "constants/regex.constant";
import { GroupUserTypes } from "sharedModules/components/ot-org-group-user/constants & enums/ot-org-group-user-types.constants";
import { OrgUserTraversal } from "enums/org-user-dropdown.enum";

@Component({
    selector: "ot-org-group-user",
    templateUrl: "./ot-org-group-user.component.html",
})
export class OtOrgGroupUserComponent implements OnInit, OnDestroy {

    /**
     * Provides an option to add email in directly
     */
    @Input() allowEmail = true;

    /**
     * Translation key to display when typing for an email
     */
    @Input() allowEmailPrependTextKey = "AssignTo";

    /**
     * Provides the ability to add project viewer's email
     */
    @Input() allowEmailProjectViewer = false;

    /**
     * Display an option that says 'Assign to Me'
     */
    @Input() allowAssignToMe = true;

    /**
     * Enables/Disables the component
     */
    @Input() disabled = false;

    /**
     * Exclude user/user groups from the list
     */
    @Input() excludedOptions: IOtOrgGroupUserAdapted[] = [];

    /**
     * TO DO
     */
    @Input() excludedPermissions: string[];

    /**
     * Value to control if the options need to pre-fetched or fetch when clicked
     */
    @Input() fetchOptionsOnClick = false;

    /**
     * Removed current user from the list
     */
    @Input() filterCurrentUser = false;

    /**
     * Pass the userId or userGroupId to the component
     */
    @Input() model: string;

    /**
     * Pass the organizationId to the component
     */
    @Input() orgId: string;

    /**
     * Filter users/user groups based on permissions
     */
    @Input() permissions: string[];

    /**
     * Placeholder to the lookup component
     */
    @Input() placeholderKey = "SelectAssignee";

    /**
     * The order of traversal for organization users
     */
    @Input() traversal = OrgUserTraversal.All;

    /**
     * TO DO
     */
    @Input() retainErrorMessage = false;

    /**
     * Option to exclude groups from the list then this becomes a ot-org-user component
     */
    @Input() excludeGroups = false;

    /**
     * Returns the selected value to the consuming component
     */
    @Output() onSelect = new EventEmitter<IOtOrgGroupUserOutput>();

    allowEmailInput: string;
    allOptions: IOtOrgGroupUserAdapted[] = [];
    cachedOptions: IOtOrgGroupUserAdapted[] = [];
    error: string;
    inputValue: string;
    labelKey = "fullName";
    valueKey = "id";
    groupLabelKey = "isLabel";
    lookupPlaceholderKey: string;
    modelForLookup$ = new BehaviorSubject<IOtOrgGroupUserAdapted>(null);
    orgChanged = false;
    pageNumber = 0; // used for paginated api call
    isLastPage = false; // used to fetch the next set of users/user groups
    isApiCallDone = true; // used to prevent from making multiple api calls
    searchStr = "";
    previousList = [] as IOtOrgGroupUserResponse[];

    inputChange$ = new Subject<string>();
    loading$ = new Subject<boolean>();
    multichoiceChange$ = new Subject<IOtOrgGroupUserOutput>();
    options$ = new Subject<IOtOrgGroupUserAdapted[]>();

    translationKeys = [
        "AssignToMe",
        "UserGroups",
        "Users",
    ];
    translations: IStringMap<string> = {};

    private destroy$ = new Subject();

    constructor(
        private orgGroupsApiService: OtOrgGroupUserApiService,
        private otOrgGroupUserHelperService: OtOrgGroupUserHelperService,
        private translateService: TranslateService,
        private principal: Principal,
    ) {}

    ngOnInit(): void {
        this.lookupPlaceholderKey = this.placeholderKey;
        this.configureSelectionListener();
        this.configureInputListener();

        this.setModelForLookup(this.model);
        this.decideToFetchOptions();

        this.translateService
            .stream(this.translationKeys)
            .subscribe((values) => {
                this.translations = values;
        });
    }

    ngOnChanges(changes: SimpleChanges): void {
        if (changes.model && !changes.model.firstChange) {
            this.setModelForLookup(changes.model.currentValue);
        }

        if (changes.orgId && !changes.orgId.firstChange) {
            this.orgChanged = true;
            this.cachedOptions = [];
            this.options$.next(null);
            this.onComponentClick();
        }

        if (changes.disabled && !changes.disabled.firstChange) {
            this.decideToFetchOptions();
        }

        if (changes.excludedOptions) {
            this.cachedOptions = this.formatOptions(this.allOptions, this.excludedOptions);
            this.options$.next(this.cachedOptions);
        }
    }

    onComponentClick(): void {
        if (this.error && !this.retainErrorMessage) {
            this.resetError();
            this.onSelect.emit({ selection: null, valid: true });
        }
        if (this.preventFetchOptionsOnClick()) return;
        this.orgChanged = false;

        this.fetchUsers(this.configureOrgId(this.orgId));
    }

    formatOptions(userOptions: IOtOrgGroupUserAdapted[], excludedOptions: IOtOrgGroupUserAdapted[]): IOtOrgGroupUserAdapted[] {
        let options = userOptions;
        const usersStartingIndex = options.findIndex((value) => value.type === GroupUserTypes.USER);
        // applying excluded options
        if (this.otOrgGroupUserHelperService.shouldHandleExcludedOptions(excludedOptions)) {
            options = options.filter((option) => !(excludedOptions.some((excludedOpt) => excludedOpt && excludedOpt.id === option.id)));
        }

        // adding Assign To Me option only if the user is not searching and the list has users
        if (this.allowAssignToMe && !this.otOrgGroupUserHelperService.isCurrentUserAnOption(options, this.translations["AssignToMe"]) && usersStartingIndex > -1 && !Boolean(this.searchStr)) {
            options.splice(usersStartingIndex, 0, { fullName: this.translations["AssignToMe"], id: this.principal.getCurrentUserId(), type: GroupUserTypes.USER });
        }

        return options.slice();
    }

    decideToFetchOptions(): void {
        // Validate the OrgId
        this.orgId = this.configureOrgId(this.orgId);
        // Fetch options on render
        if (!this.fetchOptionsOnClick && !this.disabled) {
            this.fetchUsers(this.orgId);
        }
    }

    preventFetchOptionsOnClick(): boolean {
        if (this.orgChanged) {
            return false;
        } else {
            return Boolean(!this.fetchOptionsOnClick || this.disabled);
        }
    }

    configureSelectionListener(): void {
        this.multichoiceChange$.pipe(
            takeUntil(this.destroy$),
        ).subscribe((val: IOtOrgGroupUserOutput) => {
            this.onSelect.emit(val);
            this.cachedOptions = this.formatOptions(this.allOptions, this.excludedOptions);
            this.options$.next(this.cachedOptions);
            this.resetAllowEmailOption();
        });
    }

    configureInputListener(): void {
        this.inputChange$.pipe(
            takeUntil(this.destroy$),
            tap((val: string) => {
                this.searchStr = val;
                this.options$.next([] as IOtOrgGroupUserAdapted[]);
            }),
            debounceTime(150),
        ).subscribe((val: string) => {
            this.loading$.next(false);
            this.fetchUsers(this.configureOrgId(this.orgId), 0, val, true);
        });
    }

    ngOnDestroy(): void {
        this.destroy$.next();
        this.destroy$.complete();
    }

    configureOrgId(orgId: string = null): string {
        if (orgId) return orgId;
        return this.principal.getOrggroup();
    }

    /**
     *  Fetches the next paginated list of users/user groups if the
     *  current page is not the last page and an api call is not in progress.
     */
    updateOptions(userScrolledDown: boolean): void {
        if (userScrolledDown && !this.isLastPage && this.isApiCallDone) {
            this.fetchUsers(this.configureOrgId(this.orgId), ++this.pageNumber);
        }
    }

    /**
     * gets a list of user/user groups, adds group labels, formats them &
     * send them to lookup component for display.
     *
     * @param {string} orgId
     * @param {number} [pageToFetch = 0]
     * @param {string} [search = null]
     * @param {boolean} [resetList = false]
     *
     *  @returns {void}
     */
    fetchUsers(orgId: string, pageToFetch = 0, search = null, resetList = false): void {

        this.isApiCallDone = false;

        const searchString = Boolean(search) ? search : (Boolean(this.searchStr) ? this.searchStr : null);

        this.loading$.next(true);

        const request: IOtOrgGroupUserRequest = {
            params: {
                page: pageToFetch,
                size: 20,
            },
            excludeGroups: this.excludeGroups,
            excludedPermissions: this.excludedPermissions,
            organizationId: orgId,
            search: searchString,
            traversal: this.traversal,
            permissions: this.permissions,
        };

        this.orgGroupsApiService.getUsersAndGroups(request).pipe(
            takeUntil(this.destroy$),
            ).subscribe((res: HttpResponse<IPageableOf<IOtOrgGroupUserResponse>>) => {

                if (res.ok) {
                    if (!res.body.content.length) {
                        this.lookupPlaceholderKey = "NoOptionsAvailable";
                    } else {
                        this.lookupPlaceholderKey = this.placeholderKey;
                        this.disabled = false;
                    }

                    // get the page number and last page information for the latest api call
                    this.pageNumber = res.body.number;
                    this.isLastPage = res.body.last;

                    this.previousList = resetList ? res.body.content : this.previousList.concat(res.body.content);

                    this.allOptions = this.formatOptions(this.addGroupLabels(this.previousList), this.excludedOptions);

                    this.cachedOptions = this.allOptions;
                    this.options$.next(this.allOptions);

                    if (this.modelForLookup$.value) {
                        from(this.otOrgGroupUserHelperService.shouldClearSelectionAfterFetchingOptions(
                            this.retainErrorMessage,
                            this.allowEmail,
                            this.modelForLookup$.value,
                            this.allOptions,
                        )).pipe(takeUntil(this.destroy$)).subscribe((shouldClearSelection: boolean) => {
                            if (shouldClearSelection) {
                                this.onSelect.emit({
                                    selection: null,
                                    valid: true,
                                });
                            }
                        });
                    }
                }
                this.isApiCallDone = true;
                this.loading$.next(false);
            });
    }

    /**
     * Formats the raw list from API to required format and
     * add group labels to it (User or User Groups).
     * @param {Array<IOtOrgGroupUserResponse>} list
     */
    addGroupLabels(list: IOtOrgGroupUserResponse[]): IOtOrgGroupUserAdapted[] {

        const formattedOptions: IOtOrgGroupUserAdapted[] = list.map((value) => {
            return { id: value.id, fullName: value.name, type: value.type };
        });

        const groupStartingIndex = formattedOptions.findIndex((value) => value.type === GroupUserTypes.GROUP);
        if (groupStartingIndex > -1) formattedOptions.splice(groupStartingIndex, 0, {id: "", fullName: this.translations["UserGroups"], isLabel: true} as IOtOrgGroupUserAdapted);

        const userStartingIndex = formattedOptions.findIndex((value) => value.type === GroupUserTypes.USER);
        if (userStartingIndex > -1 ) formattedOptions.splice(userStartingIndex, 0, {id: "", fullName: this.translations["Users"], isLabel: true} as IOtOrgGroupUserAdapted);

        return formattedOptions.slice();
    }

    updateFilteredOptions(input: string): IOtOrgGroupUserAdapted[] {
        const regex = new RegExp(input.replace(/[.*+?^${}()|[\]\\]/g, "\\$&"), "gi");
        return this.cachedOptions.filter((option: IOtOrgGroupUserAdapted): boolean => {
            return Boolean(option[this.labelKey].match(regex));
        });
    }

    onModelChange(selection: IOtLookupSelection<IOtOrgGroupUserAdapted>): void {
        if (this.error && !this.retainErrorMessage) {
            this.resetError();
        }
        if (!selection.change) return;

        if (this.allowAssignToMe && selection.currentValue && selection.currentValue.fullName === this.translations["AssignToMe"]) {
            selection.currentValue.fullName = this.principal.getUser().FullName;
        }

        this.multichoiceChange$.next({
            selection: selection.currentValue,
            valid: true,
        });
    }

    onInputChange(input: IKeyValue<string>): void {
        this.inputValue = input.value;

        if (!input.key || !input.value) {
            this.inputChange$.next("");
            this.resetAllowEmailOption();
            return;
        }

        this.allowEmailInput = input.value;
        if (this.error && !this.retainErrorMessage) {
            this.resetError();
        }

        // When Backspace removes all characters from input field - Clear the selection
        if (input.key === "Backspace" && input.value === "") {
            this.onSelect.emit({
                selection: null,
                valid: true,
            });
        }

        // When Enter makes a selection
        if (this.allowEmail && input.key === "Enter") {
           this.addEmail(input.value.trim());
           return;
        }

        // Filter options
        if (input.value && input.value.length > 1) {
            this.loading$.next(true);
            this.inputChange$.next(input.value);
        }
    }

    onAllowEmailSelect(): void {
        if (this.inputValue) {
            this.addEmail(this.inputValue.trim());
        }
    }

    private addEmail(input: string) {
        if (input && input.length && matchRegex(input, Regex.EMAIL)) {
            // Check email against disabled user (mandatory)
            if (this.otOrgGroupUserHelperService.emailIsDisabled(input)) {
                this.handleError("Error.EmailBelongsToDisabledUser");
                return;
            }

            // Check email against project viewer (optional)
            if (!this.allowEmailProjectViewer && this.otOrgGroupUserHelperService.emailIsProjectViewer(input)) {
                this.handleError("Error.EmailBelongsToProjectViewer");
                return;
            }

            this.multichoiceChange$.next({
                selection: { id: input, fullName: input, type: GroupUserTypes.USER },
                valid: true,
            });
        } else {
            this.handleError("PleaseEnterValidEmail");
            return;
        }
    }

    private setModelForLookup(modelFromParent: string): void {
        // check for valid GUID
        if (matchRegex(modelFromParent, Regex.GUID)) {
            // check from cached details
            const item = this.previousList.find((value) => value.id === modelFromParent);

            if (item && item.type && (item.type === GroupUserTypes.USER || item.type === GroupUserTypes.GROUP)) {
                // build model for ot-lookup
                this.modelForLookup$.next({
                    id: item.id,
                    fullName: item.name,
                    type: item.type,
                });
            } else {
                this.orgGroupsApiService.getUserOrGroupById(modelFromParent).pipe(
                    takeUntil(this.destroy$),
                ).subscribe((userOrGroupDetails: HttpResponse<IOtOrgGroupUserResponse>) => {
                    if (userOrGroupDetails && userOrGroupDetails.ok) {
                        this.modelForLookup$.next({
                            id: userOrGroupDetails.body.id,
                            fullName: userOrGroupDetails.body.name,
                            type: userOrGroupDetails.body.type === GroupUserTypes.GROUP ? GroupUserTypes.GROUP : GroupUserTypes.USER,
                        });
                    } else {
                        this.handleError("UserOrGroupNotFound");
                    }
                }, (err) => {
                    this.handleError("UserOrGroupNotFound");
                });
            }
        } else if (matchRegex(modelFromParent, Regex.EMAIL)) {
            // check for valid Email
            this.modelForLookup$.next({
                id: modelFromParent,
                fullName: modelFromParent,
                type: GroupUserTypes.USER,
            });
        } else {
            this.modelForLookup$.next(null);
        }
    }

    private resetAllowEmailOption(): void {
        this.allowEmailInput = "";
    }

    private resetError() {
        this.error = null;
    }

    private handleError(errorText: string) {
        this.error = errorText;
        this.resetAllowEmailOption();
        this.onSelect.emit({
            selection: null,
            valid: false,
        });
    }
}
