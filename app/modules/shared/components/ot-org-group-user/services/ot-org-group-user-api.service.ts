// Angular
import { Injectable } from "@angular/core";
import { HttpClient, HttpResponse } from "@angular/common/http";

// Rxjs
import { Observable } from "rxjs";

// Interfaces
import { IPageableOf } from "interfaces/pagination.interface";
import { IOtOrgGroupUserResponse } from "../interfaces/ot-org-group-user-response.interface";
import { IUser } from "interfaces/user.interface.ts";
import { IUserGroupDisplay } from "interfaces/user-groups.interface";
import { IOtOrgGroupUserRequest } from "./../interfaces/ot-org-group-user-request.interface";

@Injectable()
export class OtOrgGroupUserApiService {
    constructor(private httpClient: HttpClient) {}

    getUsersAndGroups(request: IOtOrgGroupUserRequest): Observable<HttpResponse<IPageableOf<IOtOrgGroupUserResponse>>> {

        let url = `/api/access/v1/drop-downs/assignees?page=${request.params.page}&size=${request.params.size}`;
        url = Boolean(request.search) ? `${url}&search=${request.search}` : url;
        url = Boolean(request.organizationId) ? `${url}&organizationId=${request.organizationId}` : url;
        url = Boolean(request.traversal) ? `${url}&traversal=${request.traversal}` : url;
        url = Boolean(request.permissions) ? `${url}&permissions=${request.permissions}` : url;
        url = Boolean(request.excludedPermissions) ? `${url}&excludedPermissions=${request.excludedPermissions}` : url;
        url = Boolean(request.excludeGroups) ? `${url}&excludeGroups=${request.excludeGroups}` : url;

        return this.httpClient.get<IPageableOf<IOtOrgGroupUserResponse>>(url, {observe: "response"});
    }

    getUserOrGroupById(id: string): Observable<HttpResponse<IOtOrgGroupUserResponse>> {
        return this.httpClient.get<IOtOrgGroupUserResponse>(`/api/access/v1/drop-downs/assignees/${id}`, {observe: "response"});
    }

}
