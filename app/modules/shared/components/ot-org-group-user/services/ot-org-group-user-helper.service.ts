// Angular
import { Injectable, Inject } from "@angular/core";

// Rxjs
import { take } from "rxjs/operators";

// Redux
import { StoreToken } from "tokens/redux-store.token";
import {
    getDisabledUsers,
    getProjectViewers,
} from "oneRedux/reducers/user.reducer";

// Interfaces
import { IStore } from "interfaces/redux.interface";
import { IOtOrgGroupUserAdapted } from "../interfaces/ot-org-group-user.interface";

// Constants & Enums
import { Regex } from "constants/regex.constant";

// Services
import { matchRegex } from "modules/utils/match-regex";
import { Principal } from "sharedModules/services/helper/principal.service";
import { OtOrgGroupUserApiService } from "./ot-org-group-user-api.service";

@Injectable()
export class OtOrgGroupUserHelperService {

    constructor(
        @Inject(StoreToken) private store: IStore,
        private principal: Principal,
        private orgGroupUserApiService: OtOrgGroupUserApiService,
    ) { }

    emailIsDisabled(email: string): boolean {
        return getDisabledUsers(this.store.getState()).some((user) => user.Email === email);
    }

    emailIsProjectViewer(email: string): boolean {
        return getProjectViewers(this.store.getState()).some((user) => user.Email === email);
    }

    isCurrentUserAnOption(options: IOtOrgGroupUserAdapted[], assignToMeValue: string): boolean {
        const currentUserId = this.principal.getCurrentUserId();
        if (!currentUserId) return false;
        return options.some((opt) => opt.id === currentUserId && opt.fullName === assignToMeValue);
    }

    shouldHandleExcludedOptions(excludedOptions: IOtOrgGroupUserAdapted[]): boolean {
        return excludedOptions
            && excludedOptions.length
            && excludedOptions.filter((option) => option != null).length > 0;
    }

    async shouldClearSelectionAfterFetchingOptions(retainError: boolean, allowEmail: boolean, model: IOtOrgGroupUserAdapted, options: IOtOrgGroupUserAdapted[]): Promise<boolean> {
        if (retainError) return false;
        const selectedUserIsAnOption = options.find((opt) => opt.id === model.id);
        if (selectedUserIsAnOption) {
            return false;
        } else {
            const userOrGroupResponse =  await this.orgGroupUserApiService.getUserOrGroupById(model.id).pipe(take(1)).toPromise();

            if (userOrGroupResponse.ok && userOrGroupResponse.body.id === model.id) return false;
        }
        const userEmailIsValid = matchRegex(model.id, Regex.EMAIL) && allowEmail;
        if (userEmailIsValid) return false;
        return true;
    }

}
