// Angular
import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { HttpClientModule } from "@angular/common/http";

// Components
import { OtOrgGroupUserComponent } from "./component/ot-org-group-user.component";

// Services
import { OtOrgGroupUserApiService } from "./services/ot-org-group-user-api.service";
import { OtOrgGroupUserHelperService } from "./services/ot-org-group-user-helper.service";

// Modules
import { OtFormModule, OtLookupModule } from "@onetrust/vitreus";
import { TranslateModule } from "@ngx-translate/core";

@NgModule({
    imports: [
        CommonModule,
        OtLookupModule,
        OtFormModule,
        TranslateModule,
        HttpClientModule,
    ],
    exports: [
        OtOrgGroupUserComponent,
    ],
    declarations: [
        OtOrgGroupUserComponent,
    ],
    providers: [
        OtOrgGroupUserHelperService,
        OtOrgGroupUserApiService,
    ],
})
export class OtOrgGroupUserModule { }
