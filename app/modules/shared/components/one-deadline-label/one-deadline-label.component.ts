// Angular
import {
    Component,
    Input,
} from "@angular/core";

import moment from "moment";
import { TimeStamp } from "oneServices/time-stamp.service";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

@Component({
    selector: "one-deadline-label",
    templateUrl: "one-deadline-label.component.html",
})
export class OneDeadlineLabel {

    @Input() set deadline(value: string) {
        this.deadlineStateLabel = this.setDeadlineStateLabel(value);
    }
    @Input() showDeadline = true;

    isAfterDeadline = false;
    deadlineStateLabel: string;

    constructor(
        private translatePipe: TranslatePipe,
        private readonly timeStamp: TimeStamp,
    ) { }

    setDeadlineStateLabel(deadline: string): string {
        if (!deadline) return "- - - -";

        this.isAfterDeadline = moment().isAfter(deadline);
        if (this.isAfterDeadline) {
            return this.translatePipe.transform("PastDue");
        }

        const milliseconds: number = this.timeStamp.getMillisecondsSince(deadline);
        const hourMS = 3600000;
        const hours = milliseconds / hourMS;
        const hoursRemaining = Math.floor(hours % 24);
        const daysRemaining = Math.floor(hours / 24);
        const hoursText = hoursRemaining === 1 ? this.translatePipe.transform("Hour") : this.translatePipe.transform("Hours");
        const daysText = daysRemaining === 1 ? this.translatePipe.transform("Day") : this.translatePipe.transform("Days");
        return `${daysRemaining} ${daysText} ${hoursRemaining} ${hoursText}`;
    }
}
