// Angular
import {
    Component,
    Input,
    Output,
    EventEmitter,
} from "@angular/core";
import { StateService } from "@uirouter/core";

// Interfaces
import {
    IViewRelatedAssessmentModalData,
    IViewRelatedAssessmentColumns,
} from "sharedModules/interfaces/view-related-assessment-modal.interface";
import { IStringMap } from "interfaces/generic.interface";

// Enum
import { AssessmentTableColumns } from "sharedModules/enums/assessment-table.enum";

@Component({
    selector: "assessment-table",
    templateUrl: "./assessment-table.component.html",
})
export class AssessmentTable {
    @Input() tableData: IViewRelatedAssessmentModalData;
    @Input() columns: IViewRelatedAssessmentColumns[];
    @Input() badgeColorMap: IStringMap<string>;
    @Output() goToAssessment = new EventEmitter<string>();
    @Output() closeRelatedAssessmentsModal = new EventEmitter<boolean>();

    tableColumnKeys = AssessmentTableColumns;

    constructor(
        public stateService: StateService,
    ) { }

    routeToAssessment(assessmentId: string) {
        this.goToAssessment.emit(assessmentId);
    }

    routeToAssessmentLink(assessmentId: string): string {
        if (assessmentId) {
            return this.stateService.href("zen.app.pia.module.assessment.detail", { assessmentId }, {absolute: true});
        }
    }

    trackByFn(index: number): number {
        return index;
    }

    closeRelatedAssessmentModal() {
        this.closeRelatedAssessmentsModal.emit(true);
    }
}
