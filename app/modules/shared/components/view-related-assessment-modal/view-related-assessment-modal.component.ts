// Angular
import {
    Component,
    Inject,
} from "@angular/core";
import { StateService } from "@uirouter/core";

// RxJS
import {
    Subject,
    BehaviorSubject,
} from "rxjs";

// Vitreus
import { IOtModalContent } from "@onetrust/vitreus";

// Services
import { ViewRelatedAssessmentModalApiService } from "sharedModules/services/api/view-related-assessment-modal-api.service";

// Interface
import {
    IViewRelatedAssessmentModalData,
    IViewRelatedAssessmentChildren,
    IViewRelatedAssessmentColumns,
    IViewRelatedCurrentAssessmentDetails,
} from "sharedModules/interfaces/view-related-assessment-modal.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";

// Constants
import { AssessmentStatusBadgeColorMap } from "constants/assessment.constants";

@Component({
    selector: "view-related-assessment-modal",
    templateUrl: "./view-related-assessment-modal.component.html",
    styles: ["assessment-table { height: 24.2rem; }"],
})
export class ViewRelatedAssessmentModalComponent implements IOtModalContent {
    otModalData: string;
    otModalCloseEvent: Subject<string>;
    assessmentDetails: IViewRelatedAssessmentModalData;
    relatedAssessments: IViewRelatedAssessmentChildren[] = [];
    currentAssessmentDetails: IViewRelatedCurrentAssessmentDetails;
    isLoading$ = new BehaviorSubject<boolean> (true);
    assessmentBadgeColorMap = AssessmentStatusBadgeColorMap;

    relatedAssessmentColumns: IViewRelatedAssessmentColumns[] = [
        { columnName: "ID", cellValueKey: "assessmentNumber"},
        { columnName: "Name", cellValueKey: "assessmentName" },
        { columnName: "Relationship", cellValueKey: "relationship" },
        { columnName: "Stage", cellValueKey: "status" },
        { columnName: "Template", cellValueKey: "templateName" },
    ];

    constructor(
        private stateService: StateService,
        private viewRelatedAssessmentModalApiService: ViewRelatedAssessmentModalApiService,
        ) {}

    ngOnInit() {
        this.viewRelatedAssessmentModalApiService.getRelatedAssessments(this.otModalData).then((res: IProtocolResponse<IViewRelatedAssessmentModalData>): void => {
            if (!res.result) return;
            this.currentAssessmentDetails = this.getCurrentAssessmentDetails(res.data);

            if (res.data.relatedAssessments && res.data.relatedAssessments.length) {
                this.relatedAssessments = res.data.relatedAssessments.map((relatedChildren: IViewRelatedAssessmentChildren) => relatedChildren);
            }
            this.isLoading$.next(false);
        });
    }

    goToAssessment(assessmentId: string) {
        this.stateService.go("zen.app.pia.module.assessment.detail", { assessmentId });
        this.closeModal();
    }

    closeRelatedAssessmentModal(closeRelatedAssessmentsModal: boolean) {
        if (closeRelatedAssessmentsModal) {
            this.closeModal();
        }
    }

    trackByFn(index: number): number {
        return index;
    }

    closeModal() {
        this.otModalCloseEvent.next();
    }

    private getCurrentAssessmentDetails(modalData: IViewRelatedAssessmentModalData): IViewRelatedCurrentAssessmentDetails {
        return {
            assessmentId: modalData.assessmentId,
            assessmentName: modalData.assessmentName,
            assessmentStatus: modalData.assessmentStatus,
        };
    }
}
