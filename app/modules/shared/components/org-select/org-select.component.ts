// Rxjs
import { Subject } from "rxjs";
import {
    startWith,
    takeUntil,
} from "rxjs/operators";

// 3rd party
import {
    Component,
    EventEmitter,
    Inject,
    Input,
    Output,
    OnInit,
    OnDestroy,
} from "@angular/core";
import { isEqual } from "lodash";

// Services
import { Principal } from "modules/shared/services/helper/principal.service";

// Reducers
import observableFromStore from "oneRedux/utils/observable-from-store";
import {
    getCurrentOrgTree,
    getOrgTreeById,
} from "oneRedux/reducers/org.reducer";

// Tokens
import { StoreToken } from "tokens/redux-store.token";

// Interfaces
import { IStore, IStoreState } from "interfaces/redux.interface";
import { IOrganization } from "interfaces/org.interface";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

@Component({
    selector: "org-select",
    templateUrl: "./org-select.component.html",
})
export class OrgSelect implements OnInit, OnDestroy {

    @Input() identifier: string;
    @Input() describedBy: string;
    @Input() placeholder: string;
    @Input() searchPlaceholder: string = this.translatePipe.transform("Search");
    @Input() noOptionsText: string = this.translatePipe.transform("NoOptionsAvailable");
    @Input() expandText: string = this.translatePipe.transform("Expand");
    @Input() collapseText: string = this.translatePipe.transform("Collapse");
    @Input() required: boolean;
    @Input() disabled: boolean;
    @Input() hideClearAction: boolean;
    @Input() rootTree = false;
    @Input() triggerButton = false;
    @Input() searchDelimiter = 2;
    @Input() labelDepth = 3;
    @Input() model: string;

    // Organizations passed here will take priority as options.
    @Input() options: IOrganization[];

    @Output() onSelect = new EventEmitter();

    organizations: IOrganization[] = [];
    private destroy$ = new Subject();

    constructor(
        @Inject(StoreToken) private store: IStore,
        private translatePipe: TranslatePipe,
        private principal: Principal,
    ) {}

    ngOnInit() {
        observableFromStore(this.store).pipe(
            startWith(this.store.getState()),
            takeUntil(this.destroy$),
        ).subscribe((state: IStoreState) => this.componentWillReceiveState(state));
    }

    ngOnDestroy(): void {
        this.destroy$.next();
        this.destroy$.complete();
    }

    componentWillReceiveState(state: IStoreState) {
        const rootOrgId = this.principal.getRootOrg();
        const temp = this.options || this.rootTree ? [getOrgTreeById(rootOrgId)(state)] : [getCurrentOrgTree(state)];
        if (!isEqual(this.organizations, temp)) {
            this.organizations = temp;
        }
    }

    handleSelect(orgId: string) {
        if (this.onSelect.observers.length) {
            this.onSelect.emit(orgId);
        }
    }
}
