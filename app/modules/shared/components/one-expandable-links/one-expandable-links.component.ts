// 3rd party
import { Component, Input } from "@angular/core";

@Component({
    selector: "one-expandable-links",
    template: `
        <ul class="text-small">
            <li
                class="margin-bottom-half"
                *ngFor="let link of list"
                >
                <a
                    class="text-link text-decoration-none"
                    (click)="link.expand = !link.expand">
                    <i
                        class="fa"
                        [ngClass]="{
                            'fa-angle-right': !link.expand,
                            'fa-angle-down': link.expand
                        }"
                        >
                    </i>
                    {{link.title || (link.titleKey | otTranslate)}}
                </a>
                <ul
                    class="margin-bottom-1 margin-left-3"
                    *ngIf="link.items && link.expand">
                    <li
                        class="margin-bottom-half"
                        *ngFor="let item of link.items"
                        [ngClass]="{'bullet-list': item.bullet }"
                        >
                        {{ item.text || (item.textKey | otTranslate) }}
                    </li>
                </ul>
            </li>
        </ul>
    `,
})

export class OneExpandableLinks {
    @Input() list: any;
};

