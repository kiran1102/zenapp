// External library
import { includes } from "lodash";

// Rxjs
import {
    Observable,
    Subject,
    of,
} from "rxjs";
import {
    filter,
    takeUntil,
} from "rxjs/operators";

// Angular
import {
    Component,
    Inject,
    OnInit,
} from "@angular/core";

// 3rd Party
import {
    IVirtualScrollOptions,
    ScrollObservableService,
    SetScrollTopCmd,
} from "od-virtualscroll";

// Vitreus
import { ToastService } from "@onetrust/vitreus";

// Redux
import { getModalData } from "oneRedux/reducers/modal.reducer";
import { StoreToken } from "tokens/redux-store.token";

// Services
import { ModalService } from "sharedServices/modal.service";
import { UserApiService } from "oneServices/api/user-api.service";
import { ShareAssessmentService } from "modules/assessment/services/api/share-assessment-api.service";

// Interfaces
import { IProtocolResponse } from "interfaces/protocol.interface";
import {
    IPageableOf,
    IPaginationParams,
} from "interfaces/pagination.interface";
import {
    IUserPaginationParams,
    IUserSeedFilters,
    IUserTenantFilters,
    IUser,
    IModifiedUserMetaData,
    IUserRequestParams,
    IUserMetaData,
    ISharedAssessmentUsersContract,
} from "interfaces/user.interface";
import { IStore } from "interfaces/redux.interface";
import { IAssessmentTableRow } from "modules/global-readiness/interfaces/global-readiness-assessments.interface";
import { IModifiedUser } from "interfaces/user-groups-users.interface";
import { IShareAssessmentModalData } from "interfaces/assessment/assessment.interface";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

@Component({
    selector: "share-assessment-modal",
    templateUrl: "./share-assessment-modal.component.html",
    providers: [ScrollObservableService],
})

export class ShareAssessmentModal implements OnInit {

    isReady: boolean;
    disabled: boolean;
    noRecordFound: boolean;
    isSelectedUserRecordEmpty: boolean;
    search: string;
    selectedUsers: IUserMetaData[] = [];

    scrollTop$: Subject<SetScrollTopCmd> = new Subject();
    options$: Observable<IVirtualScrollOptions> = of({
        itemWidth: 400,
        itemHeight: 50,
        numAdditionalRows: 1,
        numLimitColumns: 1,
    });
    data$: Observable<IModifiedUserMetaData[]>;
    private modalData: IShareAssessmentModalData;
    private modifiedUsersList: IModifiedUserMetaData[];
    private previousSearch: string;
    private selectedUserIds: string[] = [];
    private virtualData$: Subject<IModifiedUserMetaData[]>;
    private params = { page: 0, size: 20 };
    private users: IPageableOf<IUser>;
    private destroy$ = new Subject();
    private scrollEnd$ = this.scrollObs.scrollWin$.pipe(filter(
        ([scrollWin]) =>
            scrollWin.visibleEndRow !== -1 &&
            scrollWin.visibleEndRow === scrollWin.numVirtualRows - 1,
    ));

    constructor(
        @Inject(UserApiService) private UserApi: UserApiService,
        @Inject(StoreToken) private store: IStore,
        private modalService: ModalService,
        private scrollObs: ScrollObservableService,
        private shareAssessmentService: ShareAssessmentService,
        private toastService: ToastService,
        private translatePipe: TranslatePipe,
    ) {
        this.virtualData$ = new Subject();
        this.data$ = this.virtualData$.asObservable();
    }

    ngOnInit() {
        this.modalData = getModalData(this.store.getState());
        this.scrollEnd$.pipe(takeUntil(this.destroy$))
            .subscribe(([scrollWin]) => {
                const { number, totalPages } = this.users;
                let nextPage = 0;
                if (number + 1 < totalPages) {
                    nextPage = number + 1;
                }
                if (nextPage !== this.params.page) {
                    this.getInternalUsersMetaData(nextPage);
                }
            });
        this.selectedUserIds = Object.keys(this.modalData.usersToWhichAssessmentsShared);
        for (const userById in this.modalData.usersToWhichAssessmentsShared) {
            if (this.modalData.usersToWhichAssessmentsShared.hasOwnProperty(userById)) {
                this.selectedUsers.push(
                    {
                        userId: this.modalData.usersToWhichAssessmentsShared[userById].UserId,
                        userName: this.modalData.usersToWhichAssessmentsShared[userById].UserFullName,
                        email: this.modalData.usersToWhichAssessmentsShared[userById].Email,
                    },
                );
            }
        }
        this.getInternalUsersMetaData(this.params.page, this.params.size);
    }

    trackByFn(index: number, userEntry: IUserMetaData) {
        return userEntry.userId;
    }

    closeModal() {
        this.modalService.closeModal();
        this.modalService.clearModalData();
    }

    shareAssessmentToUsers(): void {
        this.disabled = true;
        let userRequestParamsList: IUserRequestParams[] = [];
        userRequestParamsList = this.selectedUsers.map((selectedUser: IUserMetaData): IUserRequestParams => {
            return {
                userId: selectedUser.userId,
                userName: selectedUser.userName,
            };
        });
        this.shareAssessmentService.shareAssessmentToUsers(this.modalData.assessmentId, userRequestParamsList)
            .then((response: IProtocolResponse<null>): void => {
                if (response.result) {
                    this.toastService.show(this.translatePipe.transform("Success"), this.translatePipe.transform("AssessmentShared"), "success");
                    this.disabled = false;
                    this.closeModal();
                }
            });
    }

    onToggle(isSelected: boolean, userEntry: IModifiedUserMetaData): void {
        if (isSelected) {
            this.modifiedUsersList.find(
                (uncheckUser: IModifiedUserMetaData) =>
                    uncheckUser.user["userId"] === userEntry.user["userId"],
            ).checked = true;
            this.selectedUsers.push(userEntry.user);
            this.selectedUserIds.push(userEntry.user.userId);
        } else {
            this.removeItem(userEntry["user"]);
        }
    }

    removeItem(user: IUserMetaData) {
        const removeIndex: number = this.selectedUsers.findIndex(
            (removeUser: IUserMetaData) =>
                removeUser["userId"] === user.userId,
        );
        if (removeIndex > -1) {
            this.selectedUsers.splice(removeIndex, 1);
            this.selectedUserIds.splice(removeIndex, 1);
            this.uncheckModifiedUser(user);
        }
    }

    onSearch(search = "") {
        this.isReady = false;
        this.search = search.trim();
        if (this.search !== this.previousSearch) {
            this.getInternalUsersMetaData();
        } else {
            this.isReady = true;
        }
    }

    private getInternalUsersMetaData(
        page: number = 0,
        size: number = 20,
    ): void {
        const seedFilters: IUserSeedFilters = {
            enabled: true,
            internal: true,
            external: true,
        };
        if (this.search) {
            seedFilters.search = this.search;
        }
        const tenantFilters: IUserTenantFilters = {
            roleId: [],
        };
        const userParams: IUserPaginationParams = {
            page,
            size,
            seedFilters,
            tenantFilters,
        };
        this.UserApi.getUsersByPage(userParams).then((response: IProtocolResponse<IPageableOf<IUser>>): void => {
            if (response === null || !response.result) {
                this.noRecordFound = true;
            } else {
                this.users = response.data;
                this.noRecordFound = !this.users.totalElements;
                if (!this.modifiedUsersList || this.search !== this.previousSearch) {
                    this.modifiedUsersList = this.modifyArray(this.users.content);
                } else {
                    this.modifiedUsersList.push(...this.modifyArray(this.users.content));
                }
            }
            setTimeout(() => {
                this.updateVirtualData(this.modifiedUsersList);
            }, 0);
            this.isReady = true;
            this.previousSearch = this.search;
            // To cover a bug from od-virtualscroll, where the height is not calculated properly.
            setTimeout(() =>  {
                window.dispatchEvent(new Event("resize"));
            }, 300);
        });
    }

    private modifyArray(userList: IUser[]): IModifiedUserMetaData[] {
        return userList.map((user: IUser): IModifiedUserMetaData => {
            return {
                user: {
                    userId: user.Id,
                    userName: user.FullName,
                    email: user.Email,
                },
                checked: includes(this.selectedUserIds, user.Id),
            };
        });
    }

    private updateVirtualData(users: IModifiedUserMetaData[], reset?: boolean): void {
        this.virtualData$.next(users);
    }

    private uncheckModifiedUser(user: IUserMetaData): void {
        const removeIndexModifiedList: number = this.modifiedUsersList.findIndex(
            (removeUser: IModifiedUserMetaData) =>
                removeUser.user["userId"] === user.userId,
        );
        if (removeIndexModifiedList > -1) {
            this.modifiedUsersList[removeIndexModifiedList].checked = false;
        }
    }
}
