import { IOrgUserAdapted } from "interfaces/org-user.interface";

export interface IOTLookupSelection<T> {
    previousValue: T;
    currentValue: T;
    change: T;
}

export interface IOtOrgUserOutput {
    selection: IOrgUserAdapted;
    valid: boolean;
}
