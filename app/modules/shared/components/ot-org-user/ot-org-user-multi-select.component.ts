// Angular
import {
    Component,
    OnInit,
    Input,
    Output,
    EventEmitter,
    OnChanges,
    SimpleChanges,
} from "@angular/core";

// Interfaces
import { IOrgUserAdapted } from "interfaces/org-user.interface";
import { IOtOrgUserOutput } from "sharedModules/components/ot-org-user/ot-org-user.interface";

@Component({
    selector: "ot-org-user-multi-select",
    templateUrl: "ot-org-user-multi-select.component.html",
})
export class OtOrgUserMultiSelectComponent implements OnChanges {
    @Input() allowEmail;
    @Input() disabled = false;
    @Input() fetchOptionsOnClick;
    @Input() filterCurrentUser = false;
    @Input() userList: IOrgUserAdapted[];
    @Input() permissions;
    @Input() orgId: string;
    @Input() traversal;
    @Input() returnEmptyEntriesOnSelect = false;
    @Input() hideButtons = false;
    @Input() placeholder = "SelectAssignee";
    @Input() retainErrorMessage = false;
    @Input() wrapperClass = "row-space-between";
    @Input() userSelectClass = "width-90-percent flex-grow-1";

    @Output() onSelect = new EventEmitter<IOrgUserAdapted[]>();

    selectedList: IOrgUserAdapted[] = [];

    ngOnChanges(changes: SimpleChanges): void {
        if (changes.userList) {
            this.selectedList = changes.userList.currentValue;
            if (!changes.userList.currentValue || !changes.userList.currentValue.length) {
                this.selectedList.push(null);
            }
        }
    }

    trackByOptionId(user: IOrgUserAdapted): string {
        return user.Id;
    }

    onRemoveOptionClick(index: number): void {
        if (this.selectedList.length < 2) {
            this.selectedList[0] = null;
        } else {
            this.selectedList.splice(index, 1);
        }
        this.updateSelection();
    }

    onAddOptionClick(index: number = 0): void {
        if (!this.selectedList) {
            this.selectedList = [];
        }
        if (this.selectedList.length) {
            this.selectedList.splice(index + 1, 0, null);
        } else {
            this.selectedList.push(null);
        }
        if (this.returnEmptyEntriesOnSelect) {
            this.onSelect.emit(this.selectedList);
        }
    }

    selectAssignee(selectedOption: IOtOrgUserOutput, index: number = 0) {
        if (this.selectedList.some((user: IOrgUserAdapted): boolean => user && selectedOption.selection && user.FullName === selectedOption.selection.FullName)) return;
        this.selectedList[index] = selectedOption.selection || null;
        this.updateSelection();
    }

    updateSelection() {
        this.selectedList = [...this.selectedList];
        this.userList = this.selectedList.filter((user) => user != null);
        if (this.returnEmptyEntriesOnSelect) {
            this.onSelect.emit(this.selectedList);
        } else {
            this.onSelect.emit(this.selectedList.filter((user) => user != null));
        }
    }
}
