import { Injectable, Inject } from "@angular/core";

// 3rd Party
import { find } from "lodash";

// Redux
import { StoreToken } from "tokens/redux-store.token";
import {
    getDisabledUsers,
    getProjectViewers,
} from "oneRedux/reducers/user.reducer";

// Interfaces
import { IStore } from "interfaces/redux.interface";
import { IOrgUserAdapted } from "interfaces/org-user.interface";

// Constants & Enums
import { Regex } from "constants/regex.constant";

// Services
import { Principal } from "sharedModules/services/helper/principal.service";
import { matchRegex } from "modules/utils/match-regex";

@Injectable()
export class OtOrgUserService {

    constructor(
        @Inject(StoreToken) private store: IStore,
        private principal: Principal,
    ) { }

    emailIsDisabled(email: string): boolean {
        return Boolean(find(getDisabledUsers(this.store.getState()), (user) => user.Email === email));
    }

    emailIsProjectViewer(email: string): boolean {
        return Boolean(find(getProjectViewers(this.store.getState()), (user) => user.Email === email));
    }

    isCurrentUserAnOption(options: IOrgUserAdapted[]): boolean {
        const currentUserId = this.principal.getCurrentUserId();
        if (!currentUserId) return false;
        return options.some((opt) => opt && opt.Id === currentUserId);
    }

    shouldHandleExcludedOptions(excludedOptions: IOrgUserAdapted[]): boolean {
        return excludedOptions
            && excludedOptions.length
            && excludedOptions.filter((option) => option != null).length > 0;
    }

    shouldClearSelectionAfterFetchingOptions(retainError: boolean, allowEmail: boolean, model: IOrgUserAdapted, options: IOrgUserAdapted[]): boolean {
        if (retainError) return false;
        const selectedUserIsAnOption = options.find((opt) => opt.Id === model.Id);
        if (selectedUserIsAnOption) return false;
        const userEmailIsValid = matchRegex(model.Id, Regex.EMAIL) && allowEmail;
        if (userEmailIsValid) return false;
        return true;
    }

}
