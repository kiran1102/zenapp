// Rxjs
import {
    Subject,
    BehaviorSubject,
} from "rxjs";
import {
    takeUntil,
    debounceTime,
    map,
} from "rxjs/operators";

// 3rd party
import {
    Component,
    Input,
    OnInit,
    OnDestroy,
    Output,
    EventEmitter,
    SimpleChanges,
} from "@angular/core";
import { IPromise } from "angular";

// Services
import { Principal } from "sharedModules/services/helper/principal.service";
import { OrgGroupApiService } from "oneServices/api/org-group-api.service";
import { UserActionService } from "oneServices/actions/user-action.service";
import { matchRegex } from "modules/utils/match-regex";
import { OtOrgUserService } from "sharedModules/components/ot-org-user/ot-org-user.service";

// Interfaces
import { OrgUserTraversal } from "enums/org-user-dropdown.enum";
import { IOrgUserAdapted } from "interfaces/org-user.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";
import { IKeyValue } from "interfaces/generic.interface";
import { IUser } from "interfaces/user.interface";
import {
    IOtOrgUserOutput,
    IOTLookupSelection,
} from "sharedModules/components/ot-org-user/ot-org-user.interface";

// Constants & Enums
import { Regex } from "constants/regex.constant";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

@Component({
    selector: "ot-org-user",
    templateUrl: "./ot-org-user.component.html",
})
export class OtOrgUserComponent implements OnInit, OnDestroy {

    @Input() allowEmail = true;
    @Input() allowEmailPrependText = "AssignTo";
    @Input() allowEmailProjectViewer = false;
    @Input() allowAssignToMe = true;
    @Input() disabled = false;
    @Input() excludedOptions: IOrgUserAdapted[] = [];
    @Input() excludedPermissions: string[];
    @Input() fetchOptionsOnClick = false;
    @Input() filterCurrentUser = false;
    @Input() model: string;
    @Input() orgId: string;
    @Input() permissions: string[];
    @Input() placeholder = "SelectAssignee";
    @Input() traversal = OrgUserTraversal.All;
    @Input() error: string;
    @Input() retainErrorMessage = false;

    @Output() onSelect = new EventEmitter<IOtOrgUserOutput>();

    allowEmailInput: string;
    allOptions: IOrgUserAdapted[] = [];
    cachedOptions: IOrgUserAdapted[] = [];
    inputValue: string;
    labelKey = "FullName";
    lookupPlaceholder: string;
    modelForLookup$ = new BehaviorSubject<IOrgUserAdapted>(null);
    valueKey = "Id";
    orgChanged = false;

    input$ = new Subject();
    inputChange$ = new Subject<string>();
    loading$ = new Subject<boolean>();
    multichoiceChange$ = new Subject();
    multiselectChange$ = new Subject();
    options$ = new Subject<IOrgUserAdapted[]>();

    private destroy$ = new Subject();

    constructor(
        readonly orgGroups: OrgGroupApiService,
        private userActionService: UserActionService,
        private otOrgUserService: OtOrgUserService,
        private translatePipe: TranslatePipe,
        private principal: Principal,
    ) {}

    ngOnInit(): void {
        this.lookupPlaceholder = this.placeholder;
        this.configureSelectionListener();
        this.configureInputListener();

        this.setModelForLookup(this.model);
        this.decideToFetchOptions();
    }

    ngOnChanges(changes: SimpleChanges): void {
        if (changes.model && !changes.model.firstChange) {
            this.setModelForLookup(changes.model.currentValue);
        }

        if (changes.orgId && !changes.orgId.firstChange) {
            this.orgChanged = true;
            this.cachedOptions = [];
            this.options$.next(null);
            this.onClick();
        }

        if (changes.disabled && !changes.disabled.firstChange) {
            this.decideToFetchOptions();
        }

        if (changes.excludedOptions) {
            this.cachedOptions = this.formatOptions(this.allOptions, this.excludedOptions);
            this.options$.next(this.cachedOptions);
        }
    }

    onClick(): void {
        if (this.error && !this.retainErrorMessage) {
            this.resetError();
            this.onSelect.emit({ selection: null, valid: true });
        }
        if (this.preventFetchOptionsOnClick()) return;
        this.orgChanged = false;

        this.fetchUsers(this.configureOrgId(this.orgId));
    }

    formatOptions(userOptions: IOrgUserAdapted[], excludedOptions: IOrgUserAdapted[]): IOrgUserAdapted[] {
        let options = userOptions;

        // applying excluded options
        if (this.otOrgUserService.shouldHandleExcludedOptions(excludedOptions)) {
            options = options.filter((option) => !(excludedOptions.some((excludedOpt) => excludedOpt && excludedOpt.Id === option.Id)));
        }

        // adding Assign To Me option
        if (this.allowAssignToMe && this.otOrgUserService.isCurrentUserAnOption(options)) {
            options = [{ FullName: this.translatePipe.transform("AssignToMe"), Id: this.principal.getCurrentUserId() }, ...options];
        }

        return options;
    }

    onDocumentClick(): void {
        this.onInputChange({
            key: "",
            value: "",
        });
    }

    decideToFetchOptions(): void {
        // Validate the OrgId
        this.orgId = this.configureOrgId(this.orgId);
        // Fetch options on render
        if (!this.fetchOptionsOnClick && !this.disabled) {
            this.fetchUsers(this.orgId);
        }
    }

    preventFetchOptionsOnClick(): boolean {
        if (this.orgChanged && !this.disabled) {
            return false;
        } else {
            return Boolean(!this.fetchOptionsOnClick || this.disabled);
        }
    }

    configureSelectionListener(): void {
        this.multichoiceChange$
            .asObservable().pipe(
                takeUntil(this.destroy$),
            ).subscribe((val: IOtOrgUserOutput) => {
                this.onSelect.emit(val);
                this.cachedOptions = this.formatOptions(this.allOptions, this.excludedOptions);
                this.options$.next(this.cachedOptions);
                this.resetAllowEmailOption();
            });
    }

    configureInputListener(): void {
        this.inputChange$
            .asObservable().pipe(
                takeUntil(this.destroy$),
                debounceTime(300),
                map((val: string) => this.updateFilteredOptions(val)),
            ).subscribe((options: IOrgUserAdapted[]) => {
                this.loading$.next(false);
                this.options$.next(options);
            });
    }

    ngOnDestroy(): void {
        this.destroy$.next();
        this.destroy$.complete();
    }

    configureOrgId(orgId: string = null): string {
        if (orgId) return orgId;
        return this.principal.getOrggroup();
    }

    fetchUsers(orgId: string): IPromise<void> {
        this.loading$.next(true);
        return this.orgGroups.getOrgUsersWithPermission(
            orgId,
            (this.traversal || OrgUserTraversal.All),
            (this.filterCurrentUser || false),
            (this.permissions || []),
            (this.excludedPermissions || []),
        ).then((res: IProtocolResponse<IOrgUserAdapted[]>) => {
            if (res.result) {
                if (!res.data.length) {
                    this.lookupPlaceholder = "NoOptionsAvailable";
                } else {
                    this.lookupPlaceholder = this.placeholder;
                    this.disabled = false;
                }
                this.allOptions = res.data;
                res.data = this.formatOptions(this.allOptions, this.excludedOptions);
                this.cachedOptions = res.data;
                this.options$.next(res.data);
                if (this.modelForLookup$.value) {
                    const shouldClearSelection = this.otOrgUserService.shouldClearSelectionAfterFetchingOptions(
                        this.retainErrorMessage,
                        this.allowEmail,
                        this.modelForLookup$.value,
                        this.allOptions,
                    );

                    if (shouldClearSelection) {
                        this.onSelect.emit({
                            selection: null,
                            valid: true,
                        });
                    }
                }
            }
            this.loading$.next(false);
        });
    }

    updateFilteredOptions(input: string): IOrgUserAdapted[] {
        const regex = new RegExp(input.replace(/[.*+?^${}()|[\]\\]/g, "\\$&"), "gi");
        return this.cachedOptions.filter((option: IOrgUserAdapted): boolean => {
            return Boolean(option[this.labelKey].match(regex));
        });
    }

    onModelChange(selection: IOTLookupSelection<IOrgUserAdapted>): void {
        if (this.error && !this.retainErrorMessage) {
            this.resetError();
        }
        if (!selection.change) return;

        if (this.allowAssignToMe && selection.currentValue && selection.currentValue.FullName === this.translatePipe.transform("AssignToMe")) {
            selection.currentValue.FullName = this.principal.getUser().FullName;
        }

        this.multichoiceChange$.next({
            selection: selection.currentValue,
            valid: true,
        });
    }

    onInputChange(input: IKeyValue<string>): void {
        this.inputValue = input.value;

        if (!input.key || !input.value) {
            this.inputChange$.next("");
            this.resetAllowEmailOption();
            return;
        }

        this.allowEmailInput = input.value;
        if (this.error && !this.retainErrorMessage) {
            this.resetError();
        }

        // When Backspace removes all characters from input field - Clear the selection
        if (input.key === "Backspace" && input.value === "") {
            this.onSelect.emit({
                selection: null,
                valid: true,
            });
        }

        // When Enter makes a selection
        if (this.allowEmail && input.key === "Enter") {
           this.addEmail(input.value.trim());
           return;
        }

        // Filter options
        if (input.value && input.value.length > 1) {
            this.loading$.next(true);
            this.inputChange$.next(input.value);
        }
    }

    onAllowEmailSelect(): void {
        if (this.inputValue) {
            this.addEmail(this.inputValue.trim());
        }
    }

    private addEmail(input: string) {
        if (input && input.length && matchRegex(input, Regex.EMAIL)) {
            // TODO: These checks require UserStore (redux) and won't work when (AppInitAllowAllUsersCall: false)
            if (this.otOrgUserService.emailIsDisabled(input)) {
                this.handleError("Error.EmailBelongsToDisabledUser");
                return;
            }
            if (!this.allowEmailProjectViewer && this.otOrgUserService.emailIsProjectViewer(input)) {
                this.handleError("Error.EmailBelongsToProjectViewer");
                return;
            }
            this.multichoiceChange$.next({
                selection: { Id: input, FullName: input },
                valid: true,
            });
        } else {
            this.handleError("PleaseEnterValidEmail");
            return;
        }
    }

    private setModelForLookup(model: string): void {
        if (matchRegex(model, Regex.GUID)) {
            this.getUserById(model).then((user) => {
                if (user) {
                    this.modelForLookup$.next(user);
                } else {
                    this.handleError("UserNotFound");
                    this.modelForLookup$.next(null);
                }
            });
        } else if (matchRegex(model, Regex.EMAIL)) {
            this.modelForLookup$.next({
                Id: model,
                FullName: model,
            });
        } else {
            this.modelForLookup$.next(null);
        }
    }

    private async getUserById(id: string) {
        if (this.allOptions && this.allOptions.length) {
            return this.allOptions.find((user) => user.Id === id) || null;
        } else {
            return this.userActionService.getUser(id, true, false).then((res: IProtocolResponse<IUser>) => {
                return res.result ? { Id: res.data.Id, FullName: res.data.FullName } : null;
            });
        }
    }

    private resetAllowEmailOption(): void {
        this.allowEmailInput = "";
    }

    private resetError() {
        this.error = null;
    }

    private handleError(errorText: string) {
        this.error = errorText;
        this.resetAllowEmailOption();
        this.onSelect.emit({
            selection: null,
            valid: false,
        });
    }
}
