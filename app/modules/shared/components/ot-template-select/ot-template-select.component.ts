// Rxjs
import { Subject } from "rxjs";
import {
    takeUntil,
    debounceTime,
    map,
} from "rxjs/operators";

// 3rd party
import {
    Component,
    Inject,
    Input,
    OnInit,
    OnDestroy,
    Output,
    EventEmitter,
    SimpleChanges,
} from "@angular/core";

// Reducers
import { getIsIE11 } from "oneRedux/reducers/setting.reducer";

// Tokens
import { StoreToken } from "tokens/redux-store.token";
import { IStore } from "interfaces/redux.interface";

// Services
import { AssessmentLinkingAPIService } from "modules/assessment/services/api/assessment-linking-api.service";
import { ProtocolService } from "modules/core/services/protocol.service";

// Interfaces
import {
    IProtocolResponse,
    IProtocolConfig,
    IProtocolMessages,
} from "interfaces/protocol.interface";
import { IKeyValue, INameId } from "interfaces/generic.interface";
import { ICustomTemplateDetails } from "modules/template/interfaces/custom-templates.interface";

// Constants & Enums
import { TemplateTypes } from "constants/template-types.constant";
import { TemplateStates } from "enums/template-states.enum";
import { TemplateCriteria } from "constants/template-states.constant";
import { GUID } from "constants/regex.constant";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

export interface IOTLookupSingleSelection<T> {
    previousValue: T;
    currentValue: T;
    change: T;
}

@Component({
    selector: "ot-template-select",
    templateUrl: "./ot-template-select.component.html",
})
export class OtTemplateSelect implements OnInit, OnDestroy {

    @Input() disabled = true;
    @Input() fetchOptionsOnClick = false;
    @Input() model: string;
    @Input() placeholder = this.translatePipe.transform("SelectATemplate");
    @Input() autoClose = true;

    @Output() onSelect = new EventEmitter<INameId<string>>();

    cachedOptions: Array<INameId<string>> = [];
    labelKey = "name";
    showError = false;
    valueKey = "id";

    input$ = new Subject();
    inputChange$ = new Subject<string>();
    loading$ = new Subject<boolean>();
    model$ = new Subject();
    multichoiceChange$ = new Subject();
    options$ = new Subject<Array<INameId<string>>>();
    optionsChange$ = new Subject();

    private destroy$ = new Subject();

    constructor(
        @Inject(StoreToken) private store: IStore,
        private assessmentLinkingAPIService: AssessmentLinkingAPIService,
        private translatePipe: TranslatePipe,
        private OneProtocol: ProtocolService,
    ) {}

    ngOnInit(): void {
        this.placeholder = getIsIE11(this.store.getState()) ? "" : this.placeholder;
        this.configureSelectionListeners();
        this.configureInputListener();
        this.decideToFetchOptions();
        this.setModelForLookup(this.model);
    }

    ngOnChanges(changes: SimpleChanges): void {
        if (changes.model && !changes.model.firstChange) {
            this.setModelForLookup(changes.model.currentValue);
        }
    }

    ngOnDestroy(): void {
        this.destroy$.next();
        this.destroy$.complete();
    }

    onClick(): void {
        if (!this.fetchOptionsOnClick || this.disabled) return;
        if (this.cachedOptions && this.cachedOptions.length) return;
        this.fetchTemplateTypes();
    }

    decideToFetchOptions(): void {
        // Fetch options on render
        if (!this.fetchOptionsOnClick) {
            this.fetchTemplateTypes();
        // Fetch options when dropdown is clicked
        } else {
            this.placeholder = getIsIE11(this.store.getState()) ? "" : this.translatePipe.transform("SelectATemplate");
            return;
        }
    }

    configureSelectionListeners(): void {
        this.multichoiceChange$
            .asObservable().pipe(
                takeUntil(this.destroy$),
                map((selection: IOTLookupSingleSelection<INameId<string>>) => this.validateMultichoice(selection)),
            ).subscribe((val: INameId<string>) => {
                this.model$.next(val);
                this.onSelect.emit(val);
                this.options$.next(this.cachedOptions);
            });
    }

    configureInputListener(): void {
        this.inputChange$
            .asObservable().pipe(
                takeUntil(this.destroy$),
                debounceTime(300),
                map((val: string) => this.updateFilteredOptions(val)),
            ).subscribe((options: Array<INameId<string>>) => {
                this.loading$.next(false);
                this.options$.next(options);
            });
    }

    validateMultichoice(selection: IOTLookupSingleSelection<INameId<string>>): INameId<string> {
        if (selection.change && selection.currentValue) {
            return selection.currentValue;
        } else {
            return null;
        }
    }

    fetchTemplateTypes(): void {
        this.loading$.next(true);
        this.assessmentLinkingAPIService.getAssessmentTemplates(`${TemplateTypes.PIA},${TemplateTypes.VENDOR}`, TemplateStates.Published, TemplateCriteria.Both)
            .then((res: IProtocolResponse<ICustomTemplateDetails[]>): void => {
                if (res.result) {
                    const showNoOptions = this.disabled = !res.data.length;
                    const templates = res.data.map((opt: ICustomTemplateDetails): INameId<string> => {
                        const templateName: string = opt.isActive ? opt.name : `${opt.name} (${this.translatePipe.transform("Archived")})`;
                        return {
                            name: templateName,
                            id: opt.rootVersionId,
                        };
                    });
                    this.placeholder = this.setPlaceholder(showNoOptions);
                    this.cachedOptions = templates;
                    this.options$.next(templates);
                }
                this.loading$.next(false);
            });
    }

    onModelChange(selection: IOTLookupSingleSelection<INameId<string>>): void {
        if (!selection.change) return;
        this.multichoiceChange$.next(selection as IOTLookupSingleSelection<INameId<string>>);
    }

    onInputChange(input: IKeyValue<string>): void {
        if (!input.key || !input.value) {
            this.inputChange$.next("");
            return;
        }

        if (input.value && input.value.length > 1) {
            this.loading$.next(true);
            this.inputChange$.next(input.value);
        }
    }

    private updateFilteredOptions(input: string): Array<INameId<string>> {
        const regex = new RegExp(input, "gi");
        return this.cachedOptions.filter((option: INameId<string>): boolean => {
            return Boolean(option[this.labelKey].match(regex));
        });
    }

    private setPlaceholder(showNoOptions: boolean = true): string {
        return getIsIE11(this.store.getState()) ? ""
            : showNoOptions
                ? this.translatePipe.transform("NoOptionsAvailable")
                : this.translatePipe.transform("SelectATemplate");
    }

    private setModelForLookup(model: string): void {
        if (model && model.match(GUID)) {
            this.getTemplateMetadata(model).then((res) => {
                if (res.result) {
                    this.model$.next({ id: model, name: res.data.name });
                }
            });
        } else {
            this.model = null;
            this.model$.next(null);
        }
    }

    private getTemplateMetadata(templateId: string): ng.IPromise<IProtocolResponse<any>> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("GET", `/api/template/v1/templates/${templateId}/export`);
        const messages: IProtocolMessages = { Error: { Hide: true } };
        return this.OneProtocol.http(config, messages);
    }

}
