declare var VERSION: string;

// 3rd party
import {
    Component,
    OnInit,
} from "@angular/core";

// Constants
import { Patents } from "constants/about-onetrust.constant";
import { DEFAULT_BRAND_LOGO } from "constants/branding.constant";

// Rxjs
import { Subject } from "rxjs";

// Services
import { Content } from "modules/shared/services/provider/content.service";

@Component({
    selector: "about-onetrust-modal",
    templateUrl: "./about-onetrust-modal.component.html",
})
export class AboutOnetrustModal implements OnInit {

    version = VERSION;
    patentList = Patents.join("; ");
    copyright = "";
    supportPortalLink = "";
    privacyPolicyLink = "";
    brandingLogoUrl = "";

    otModalCloseEvent: Subject<boolean>;

    constructor(
        private content: Content,
    ) { }

    ngOnInit() {
        this.brandingLogoUrl = this.content.GetKey("loginLogo") || DEFAULT_BRAND_LOGO;
        this.supportPortalLink = this.content.GetKey("SupportPortalLink");
        this.privacyPolicyLink = this.content.GetKey("privacyPolicy");
        this.copyright = this.content.GetKey("copyright");
    }

    closeModal(): void {
        this.otModalCloseEvent.next(false);
    }
}
