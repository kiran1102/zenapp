import {
    Component,
    Input,
    Output,
    EventEmitter,
    HostListener,
    ElementRef,
    ViewChild,
} from "@angular/core";

@Component({
    selector: "one-editable-button",
    templateUrl: "one-editable-button.component.html",
})

export class OneEditableButton {

    @Input() buttonClass: string;
    @Input() disabled = false;
    @Input() model: string;
    @Input() modelFromParent = false;

    @Output() onChange = new EventEmitter();

    @ViewChild("inputEl") set inputElement(inputElement: ElementRef) { this.inputEl = inputElement; }
    @ViewChild("buttonEl") set buttonElement(buttonElement: ElementRef) { this.buttonEl = buttonElement; }

    inputModel: string;
    isEditing = false;
    prevModel: string;

    private inputEl: ElementRef;
    private buttonEl: ElementRef;

    @HostListener("keyup.esc")
    cancelClicked() {
        this.isEditing = false;
        this.focusButton();
    }

    acceptClicked() {
        this.onInputEnter();
    }

    buttonClicked() {
        this.prevModel = this.model;
        this.inputModel = this.model;
        this.isEditing = true;
        this.focusInput();
    }

    onInputEnter() {
        this.model = this.inputModel ? this.inputModel.trim() : this.inputModel;
        this.isEditing = false;
        this.focusButton();
        this.onChange.emit(this.model);
        if (this.modelFromParent && this.prevModel !== this.model) {
            this.model = null;
        }
    }

    onDocumentClick() {
        this.cancelClicked();
    }

    focusButton() {
        setTimeout(() => { this.buttonEl.nativeElement.focus(); });
    }

    focusInput() {
        setTimeout(() => { this.inputEl.nativeElement.focus(); });
    }
}
