import { Component, Input, Output, EventEmitter } from "@angular/core"

@Component({
    selector: "one-button-toggle",
    templateUrl: "./one-button-toggle.component.html",
})
export class OneButtonToggleComponent {
    @Input() leftButtonText: string;
    @Input() rightButtonText: string;
    @Input() buttonClass = "";
    @Input() primaryButtonActive = true;
    @Output() toggle = new EventEmitter();

    buttonTypes = {
        primary: "primary",
        secondary: "secondary",
    }

    toggleButtons(buttonType: string) {
        if (
            (buttonType === this.buttonTypes.primary && !this.primaryButtonActive) ||
            (buttonType === this.buttonTypes.secondary && this.primaryButtonActive)
        ) {
            this.primaryButtonActive = !this.primaryButtonActive;
            this.toggle.emit();
        }
    }
}