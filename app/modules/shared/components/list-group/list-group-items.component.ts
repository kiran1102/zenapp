import {
    Component,
    Input,
} from "@angular/core";

@Component({
    selector: "list-group-items",
    template: `
        <ul
            class="row-wrap-space-between"
            *ngIf="items?.length"
            >
            <ng-container *ngFor="let item of items; trackBy: trackByIndex">
                <li
                    class="
                        list-group__sub-item
                        static-horizontal
                        margin-bottom-1
                        padding-all-1
                        border
                        border-radius
                        text-center
                        overflow-ellipsis-nowrap
                        "
                    [ngClass]="{'list-group__sub-item--single-column': singleColumn}"
                    [innerText]="item | getLabelBy:getItemLabel"
                    >
                </li>
            </ng-container>
        </ul>
    `,
})
export class ListGroupItems {
    @Input() items: any[];
    @Input() getItemLabel: (any) => string;
    @Input() singleColumn: boolean;

    trackByIndex(index: number): number {
        return index;
    }
}
