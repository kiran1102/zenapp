import {
    Component,
    Input,
    Output,
    EventEmitter,
    TemplateRef,
    ContentChild,
} from "@angular/core";

@Component({
    selector: "list-group-container",
    template: `
        <header class="list-group__item border row-vertical-center background-grey border-radius padding-left-right-2">
            <div
                *ngIf="headerIcon"
                class="list-group__icon border border-round background-white position-relative row-horizontal-vertical-center"
                >
                <ng-container *ngTemplateOutlet="headerIcon"></ng-container>
            </div>
            <h5
                class="margin-top-bottom-0 margin-right-auto text-bold"
                [innerText]="text || ''"
                >
            </h5>
            <ng-container *ngTemplateOutlet="headerActions"></ng-container>
            <i
                class="ot cursor-pointer"
                ot-auto-id="ListGroupHeaderToggle"
                [ngClass]="[(isOpen ? 'ot-chevron-down' : 'ot-chevron-right'), (toggleEnabled ? '' : 'pointer-events-none')]"
                (click)="toggled.emit(!isOpen)"
                >
            </i>
        </header>
        <section
            class="margin-left-3 padding-left-4 padding-top-1 border-left"
            *ngIf="isOpen"
            >
            <ng-container *ngTemplateOutlet="groupContent"></ng-container>
        </section>
    `,
})
export class ListGroupContainer {
    @Input() text: string;
    @Input() toggleEnabled = true;
    @Input() isOpen = false;

    @ContentChild("headerActions") headerActions: TemplateRef<any>;
    @ContentChild("groupContent") groupContent: TemplateRef<any>;
    @ContentChild("headerIcon") headerIcon: TemplateRef<any>;

    @Output() toggled = new EventEmitter<boolean>();
}
