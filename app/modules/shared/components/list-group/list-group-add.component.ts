import {
    Component,
    Input,
    Output,
    EventEmitter,
} from "@angular/core";

@Component({
    selector: "list-group-add",
    template: `
        <div class="list-group__item list-group__item--dashed text-center padding-all-1">
            <button
                otButton
                neutral
                class="margin-0-auto"
                [attr.ot-auto-id]="otAutoId || 'ListGroupAddButton'"
                [innerText]="text"
                (click)="handleClick.emit()"
                >
            </button>
        </div>
    `,
})
export class ListGroupAdd {
    @Input() text: string;
    @Input() otAutoId: string;
    @Output() handleClick = new EventEmitter<boolean>();
}
