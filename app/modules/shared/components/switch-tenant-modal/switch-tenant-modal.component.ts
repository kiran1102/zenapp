import {
    Component,
    OnInit,
    Inject,
} from "@angular/core";
import { filter, orderBy } from "lodash";

// Rxjs
import { Subject } from "rxjs";

// Redux / Tokens
import { IStore } from "interfaces/redux.interface";
import { StoreToken } from "tokens/redux-store.token";
import { getCurrentUser } from "oneRedux/reducers/user.reducer";

// Services
import { UserActionService } from "oneServices/actions/user-action.service";
import { NotificationService } from "modules/shared/services/provider/notification.service";

// Interface
import { ITenant } from "interfaces/tenant.interface";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

@Component({
    selector: "switch-tenant-modal",
    templateUrl: "./switch-tenant-modal.component.html",
})

export class SwitchTenantModal implements OnInit {
    ready = false;

    tenants: ITenant[] = [];
    filteredTenants: ITenant[] = [];
    search: string;

    otModalCloseEvent: Subject<string | boolean>;

    constructor(
        @Inject(StoreToken) private store: IStore,
        private translatePipe: TranslatePipe,
        private userActionService: UserActionService,
        private notificationService: NotificationService,
    ) { }

    ngOnInit(): void {
        this.userActionService.fetchCurrentUser().then(() => {
            this.tenants = orderBy(getCurrentUser(this.store.getState()).Tenants, "Name");
            this.filteredTenants = [...this.tenants];
            this.ready = true;
        });
    }

    onSearch(search?: string) {
        this.filteredTenants = search ? filter(this.tenants, (tenant: ITenant): boolean => {
            return tenant.Name.toLowerCase().indexOf(search.toLowerCase()) > -1;
        }) : [...this.tenants];
    }

    selectTenant(id: string, offline: boolean) {
        if (offline) {
            this.notificationService.alertError(this.translatePipe.transform("errorOfflineTenant"), null);
        } else {
            this.otModalCloseEvent.next(id);
        }
        this.closeModal();
    }

    trackByFn(index: number, tenant: ITenant) {
        return tenant.Id;
    }

    closeModal() {
        this.otModalCloseEvent.next(false);
    }
}
