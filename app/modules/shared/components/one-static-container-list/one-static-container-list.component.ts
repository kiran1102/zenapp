// 3rd party
import { Component, Input } from "@angular/core";
import { each } from "lodash";

// Interfaces
import { IStaticContainer } from "interfaces/one-static-container.interface";

// Services
import { Permissions } from "modules/shared/services/helper/permissions.service";

@Component({
    selector: "one-static-container-list",
    template: `
        <ol
            *ngFor="let item of list"
            class="row-flex-start margin-top-bottom-2"
            >
            <upgraded-one-numbered-circle
                *ngIf="item.index && canShow(item)"
                [number]="item.index"
                [circleClass]="'margin-right-2'"
                >
            </upgraded-one-numbered-circle>
            <div
                *ngIf="canShow(item)"
                class="
                    padding-bottom-2
                    border-bottom
                    stretch-vertical
                    row-horizontal-vertical-center
                    responsive-wrap
                ">
                <div class="stretch-horizontal margin-bottom-1 margin-right-2">
                    <p class="text-bold text-medium" *ngIf="item.content.title || item.content.titleKey">{{item.content.title || (item.content.titleKey | otTranslate)}}</p>
                    <p class="text-thin-2" *ngIf="item.content.copy || item.content.copyKey">{{item.content.copy || (item.content.copyKey | otTranslate)}}</p>
                    <one-expandable-links *ngIf="item.content.links" [list]="item.content.links"></one-expandable-links>
                </div>
                <div class="flex-full-33 max-25">
                    <one-anchor-button
                        *ngIf="item.action"
                        [anchorLink]="item.action.link"
                        [anchorRoute]="item.action.route"
                        [anchorRouteParams]="item.action.routeParams"
                        [buttonClass]="getButtonClass(item)"
                        [icon]="item.action.icon"
                        [iconClass]="item.action.iconClass"
                        [identifier]="item.action.identifier"
                        [newTab]="item.action.newTab"
                        [rightSideIcon]="item.action.rightSideIcon"
                        [rightSideIconClass]="item.action.rightSideIconClass"
                        [text]="item.action.text"
                        [textKey]="item.action.textKey"
                        [type]="item.action.type"
                        [wrapText]="item.action.wrapText || true"
                        >
                    </one-anchor-button>
                </div>
            </div>
        </ol>
    `,
})
export class OneStaticContainerListComponent {

    @Input() list: IStaticContainer[];

    constructor(private readonly permissions: Permissions) { }

    canShow(item: IStaticContainer): boolean {
        let canShowItem = true;
        each(item.permissionKeys, (permission: string): void => {
            if (!this.permissions.canShow(permission)) {
                canShowItem = false;
            }
        });
        each(item.negativePermissionKeys, (permission: string): void => {
            if (this.permissions.canShow(permission)) {
                canShowItem = false;
            }
        });
        return canShowItem;
    }

    getButtonClass(item: IStaticContainer): string {
        return `width-100-percent text-center padding-all-half ${item.action.buttonClass || ""}`;
    }
}
