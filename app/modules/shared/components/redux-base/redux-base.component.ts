// Rxjs
import { Subject } from "rxjs";
import {
    startWith,
    takeUntil,
} from "rxjs/operators";

// Angular
import { OnInit, OnDestroy } from "@angular/core";

// Redux
import observableFromStore from "oneRedux/utils/observable-from-store";
import { IStore, IStoreState } from "interfaces/redux.interface";

export abstract class ReduxBaseComponent implements OnInit, OnDestroy {
    componentDestroy = new Subject();

    constructor(
        private readonly baseStore: IStore,
    ) { }

    ngOnInit(): void {
        observableFromStore(this.baseStore).pipe(
            startWith(this.baseStore.getState()),
            takeUntil(this.componentDestroy),
        ).subscribe((state: IStoreState): void => this.onStateUpdate(state));
    }

    ngOnDestroy(): void {
        this.componentDestroy.next();
    }

    protected abstract onStateUpdate(state: IStoreState): void;

}
