// Angular
import {
    Component,
    Inject,
    OnInit,
} from "@angular/core";

// Redux / Tokens
import { StoreToken } from "tokens/redux-store.token";
import { getModalData } from "oneRedux/reducers/modal.reducer";

// Services
import { ModalService } from "sharedServices/modal.service";

// Interfaces
import { IHelpDialogModal } from "interfaces/help-dialog-modal.interface";
import { IStore } from "interfaces/redux.interface";

@Component({
    selector: "help-dialog-modal",
    templateUrl: "help-dialog-modal.component.html",
})
export class HelpDialogModal implements OnInit {
    disabled = false;
    modalData: IHelpDialogModal;
    constructor(
        @Inject(StoreToken) private store: IStore,
        private ModalService: ModalService,
    ) {}

    ngOnInit() {
        this.modalData = getModalData(this.store.getState());
    }

    closeModal() {
        this.ModalService.closeModal();
        this.ModalService.clearModalData();
    }
}
