import { Component, Input, Output, EventEmitter } from "@angular/core";

@Component({
    selector: "switch-toggle",
    template: `
        <div class="flex {{switchClass}}">
            <ot-toggle
                [attr.aria-label]="label"
                [attr.ot-auto-id]="identifier"
                [checked]="toggleState"
                [disabled]="isDisabled"
                (toggle)="callback.emit($event)"
            ></ot-toggle>
            <ng-container *ngIf="label">
                <span class="margin-left-half" [ngClass]="{'text-bold': toggleState}">{{label}}</span>
            </ng-container>
        </div>
    `,
})

export class SwitchToggleComponent {
    @Input() identifier: string;
    @Input() isDisabled: boolean;
    @Input() label: string;
    @Input() switchClass: string;
    @Input() toggleState: boolean;

    @Output() callback = new EventEmitter<boolean>();
}
