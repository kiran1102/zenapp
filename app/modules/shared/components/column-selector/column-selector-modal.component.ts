// 3rd party
import {
    Component,
    Inject,
    ViewChild,
    OnInit,
    OnDestroy,
} from "@angular/core";
import { isFunction } from "lodash";
import { IListItem, IListState } from "@onetrust/vitreus";
import { OtDuelingPicklistComponent } from "interfaces/ot-dueling-picklist.interface";

// Redux
import { getModalData } from "oneRedux/reducers/modal.reducer";

// Services
import { ModalService } from "sharedServices/modal.service";

// Tokens
import { StoreToken } from "tokens/redux-store.token";

// Interfaces
import { IStore } from "interfaces/redux.interface";
import { IColumnSelectorModalResolve } from "interfaces/column-selector-modal.interface";

@Component({
    selector: "column-selector-modal",
    templateUrl: "./column-selector-modal.component.html",
})

// DEPRECATING - please use the grid-column-selector.component.ts instead
// - This uses Redux store
// - This doesn't use the latest Vitreus modal

export class ColumnSelectorModalComponent implements OnInit {

    modalData: IColumnSelectorModalResolve;
    promiseToResolve: (columnSelectionMap: IListState) => Promise<boolean>;
    modalTitle: string;
    cancelText: string;
    saveText: string;
    leftList: IListItem[];
    rightList: IListItem[];
    labelLeft: string;
    labelRight: string;
    labelKey: string;
    isSaving = false;

    @ViewChild("duelingPicklist")
    private duelingListComponent: OtDuelingPicklistComponent;

    constructor(
        @Inject(StoreToken) private store: IStore,
        private modalService: ModalService,
    ) {}

    ngOnInit(): void {
        this.modalData = getModalData(this.store.getState());
        this.modalTitle = this.modalData.modalTitle;
        this.cancelText = this.modalData.cancelButtonText || "Close";
        this.saveText = this.modalData.saveButtonText || "Save";
        this.labelKey = this.modalData.labelKey || "key";
        this.leftList = this.modalData.leftList || [];
        this.rightList = this.modalData.rightList || [];
        this.labelLeft = this.modalData.leftHeaderLabel || "Disabled";
        this.labelRight = this.modalData.rightHeaderLabel || "Active";
        this.promiseToResolve = this.modalData.promiseToResolve;

    }

    saveAndClose(): void {
        if (isFunction(this.promiseToResolve)) {
            this.isSaving = true;
            this.promiseToResolve(this.duelingListComponent.getListState()).then((success: boolean) => {
                if (success) {
                    this.modalService.closeModal();
                    this.modalService.clearModalData();
                }
                this.isSaving = false;
            });
        }
    }

    closeModal(): void {
        this.modalService.closeModal();
        this.modalService.clearModalData();
    }
}
