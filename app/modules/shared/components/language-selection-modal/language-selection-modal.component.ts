import {
    Component,
    Inject,
    OnInit,
} from "@angular/core";
import {
    map,
    every,
    findIndex,
    filter,
    orderBy,
    some,
} from "lodash";

// Redux
import { IStore } from "interfaces/redux.interface";
import { StoreToken } from "tokens/redux-store.token";

// Interfaces
import {
    IGetAllLanguageResponse,
    ILanguageSelection,
} from "interfaces/dsar/dsar-edit-language.interface";
import { ILanguageSelectionModalResolve } from "interfaces/language-selection-modal-resolve.interface";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

// Rxjs
import { Subject } from "rxjs";

@Component({
    selector: "language-selection-modal",
    templateUrl: "./language-selection-modal.component.html",
})
export class LanguageSelectionModal implements OnInit {
    columns = [
        {
            columnName: this.translatePipe.transform("SelectRow"),
            columnType: "checkbox",
        },
        {
            columnName: this.translatePipe.transform("SelectAll"),
            cellValueKey: "Name",
        },
        {
            columnName: this.translatePipe.transform("Default"),
            columnType: "radio",
        },
    ];
    isLoading = false;
    headerTitleKey: string;
    bodyTextKey: string;
    saveLoading = false;
    defaultLanguage: string;
    currentSearchTerm = "";
    showAdvancedLanguages = false;

    otModalData: ILanguageSelectionModalResolve;
    otModalCloseEvent: Subject<{languages: string[], defaultLanguage: string}>;

    private modalData: ILanguageSelectionModalResolve;
    private _languageTableData: ILanguageSelection[];

    constructor(
        @Inject(StoreToken) private store: IStore,
        private translatePipe: TranslatePipe,
    ) {
    }

    ngOnInit() {
        this.initModalData();
        this.initLanguageTable();
    }

    initModalData() {
        this.isLoading = true;
        this.bodyTextKey = this.otModalData.bodyTextKey;
    }

    initLanguageTable() {
        this.languageTableData = this.buildLanguageData(this.otModalData.allLanguages, this.otModalData.selectedLanguages, this.otModalData.defaultLanguage);
        this.defaultLanguage = this.otModalData.defaultLanguage;
        this.isLoading = false;
        this.showAdvancedLanguages = this.isAnyAdvancedLanguageSelected();
    }

    buildLanguageData(allLanguages: IGetAllLanguageResponse[], selectedLanguages: string[], defaultLanguage: string): ILanguageSelection[] {
        const languageData = map(allLanguages, (language: IGetAllLanguageResponse): ILanguageSelection => {
            const index = selectedLanguages.findIndex((selection: string): boolean => selection === language.Code);
            return {
                ...language,
                enabled: index >= 0,
                isDefault: language.Code === defaultLanguage,
            };
        });
        return orderBy(languageData, "enabled", "desc");
    }

    handleSelectDefaultLanguage(language: string) {
        this.defaultLanguage = language;
        this.languageTableData.forEach((elem: ILanguageSelection) => {
            elem.isDefault = false;
        });
        const selectedIndex = findIndex(this.languageTableData, ["Code", this.defaultLanguage]);
        this.languageTableData[selectedIndex].enabled = true;
        this.languageTableData[selectedIndex].isDefault = true;
    }

    handleSelectLanguage(language: string): boolean {
        const defaultIndex = findIndex(this.languageTableData, ["Code", this.defaultLanguage]);
        const selectedIndex = findIndex(this.languageTableData, ["Code", language]);
        if (selectedIndex === defaultIndex) {
            return false;
        }
        if (!this.hasLanguagesEnabled) {
            this.languageTableData[selectedIndex].isDefault = true;
            this.defaultLanguage = language;
        }
        this.languageTableData[selectedIndex].enabled = !this.languageTableData[selectedIndex].enabled;
        return true;
    }

    handleSelectAll(): boolean {
        const defaultIndex = findIndex(this.languageTableData, ["Code", this.defaultLanguage]);
        const selection = !this.allLanguagesEnabled;
        this.languageTableData.forEach((elem: ILanguageSelection, index: number): void => {
            if (index !== defaultIndex) {
                elem.enabled = selection;
            }
        });
        return true;
    }

    handleSearch(searchTerm) {
        this.currentSearchTerm = searchTerm;
    }

    toggleShowAdvancedLanguages() {
        this.showAdvancedLanguages = !this.showAdvancedLanguages;
    }

    onSave() {
        const selectedLanguages = filter(this.languageTableData, (row: ILanguageSelection) => row.enabled);
        const selectedLanguageCodes = map(selectedLanguages, (obj: ILanguageSelection) => obj.Code);
        this.otModalCloseEvent.next({ languages: selectedLanguageCodes, defaultLanguage: this.defaultLanguage });
    }

    closeModal() {
        this.otModalCloseEvent.next();
    }

    get singleLanguageEnabled(): boolean {
        return this.languageTableData && filter(this.languageTableData, ["enabled", true]).length === 1;
    }

    get allLanguagesEnabled(): boolean {
        return this.languageTableData && every(this.languageTableData, "enabled");
    }

    get hasLanguagesEnabled(): boolean {
        return this.languageTableData && some(this.languageTableData, "enabled");
    }

    get languageTableData(): ILanguageSelection[] {
        return this._languageTableData;
    }

    set languageTableData(languageTableData: ILanguageSelection[]) {
        this._languageTableData = languageTableData;
    }

    get filteredLanguageTableData(): ILanguageSelection[] {
        const regex = new RegExp(this.currentSearchTerm, "i");
        return filter(this._languageTableData, (s) => s.Name.search(regex) >= 0 &&
            (this.showAdvancedLanguages || !this.isAdvancedLanguage(s)));
    }

    private isAnyAdvancedLanguageSelected() {
        return some(this._languageTableData, (s) => s.enabled && this.isAdvancedLanguage(s));
    }

    private isAdvancedLanguage(language: IGetAllLanguageResponse) {
        if (language.Name.startsWith("English (U.S.)")) {
            return false;
        }
        return language.Name.indexOf("(") >= 0;
    }
}
