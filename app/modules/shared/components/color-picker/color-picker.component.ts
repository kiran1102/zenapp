
import {
    Component,
    EventEmitter,
    ElementRef,
    Input,
    Output,
    OnInit,
    HostListener,
    ViewChild,
} from "@angular/core";

import { IColorOptions } from "interfaces/color-picker.interface.ts";
import { HEXA_COLOR_CODE } from "constants/regex.constant";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

@Component({
    selector: "color-picker",
    templateUrl: "./color-picker.component.html",
})

export class ColorPickerComponent implements OnInit {
    public pickerColor: string;
    public colorSwatchClass = "";
    public inputHasError = false;
    public readonly defaultPickerColor = "rgb(87, 123, 193)";

    @Input() id: string;
    @Input() title?: string;
    @Input() colorOptions: IColorOptions[];
    @Input() selectedColor: string;
    @Input() placeholder: string;
    @Input() defaultText: string;
    @Input() inputErrorMessage: string;
    @Output() selectionChange = new EventEmitter();
    @Output() inputValidationCallback = new EventEmitter();
    @ViewChild("colorPickerInput") inputEl: ElementRef;

    constructor(
        private translatePipe: TranslatePipe,
        private el: ElementRef,
    ) {}

    ngOnInit(): void {
        if (!this.selectedColor) { this.selectedColor = this.defaultText; }
        this.colorSwatchClass = "slds-hide";
        this.pickerColor = this.selectedColor || this.defaultPickerColor;
        this.onInputBlur();
    }

    @HostListener("document:click", ["$event"])
    onDocumentClick(evt: Event): void {
        if (this.el.nativeElement.contains(event.target)) {
            this.inputEl.nativeElement.focus();
        } else {
            this.hideColorSwatch();
        }
    }

    public onInputBlur(): void {
        const valid: boolean = this.isValidHexColor(this.selectedColor);
        this.inputHasError = !valid;
        this.inputValidationCallback.emit(valid);
    }

    public triggerColorChange(inputValue: string): void {
        this.selectColor(inputValue);
    }

    public selectColor(color: string): void {
        this.selectedColor = color;
        this.pickerColor = this.selectedColor;
        this.selectionChange.emit(this.selectedColor);
    }

    public showColorSwatch(): void {
        this.colorSwatchClass = this.colorSwatchClass === "slds-show" ? "slds-hide" : "slds-show";
    }

    public hideColorSwatch(): void {
        this.colorSwatchClass = "slds-hide";
    }

    public isValidHexColor(color: string): boolean {
        if (!color || color === this.defaultText) { return true; }
        if (HEXA_COLOR_CODE.test(color)) { return true; }
        return false;
    }

}
