import {
    Component,
    OnInit,
    Inject,
} from "@angular/core";

// Services
import { TenantTranslationService } from "sharedServices/tenant-translation.service";

// Interfaces
import { ILanguage } from "interfaces/localization.interface";

// Rxjs
import { Subject } from "rxjs";

@Component({
    selector: "change-language-modal",
    templateUrl: "change-language-modal.component.html",
})
export class ChangeLanguageModal implements OnInit {
    disabled = false;
    isReady = false;
    errorMessage: string;
    languages: ILanguage[];
    currentLanguage: ILanguage;
    selectedLanguage: ILanguage;
    selectedOption: ILanguage;

    otModalCloseEvent: Subject<boolean>;

    constructor(
        @Inject("TenantTranslationService") private tenantTranslationService: TenantTranslationService,
    ) { }

    ngOnInit() {
        this.languages = this.tenantTranslationService.getLanguages();
        const currentLangValue = this.tenantTranslationService.getCurrentLanguage();
        this.currentLanguage = this.languages.find( (lang) => lang.Code === currentLangValue);
        this.selectedOption = this.currentLanguage;
        this.isReady = true;
    }

    selectOption(selectedLang: ILanguage) {
        this.selectedLanguage = selectedLang;
        this.selectedOption = selectedLang;
    }

    changeLanguage() {
        this.isReady = true;
        this.disabled = true;
        localStorage.setItem("OneTrust.languageSelected", JSON.stringify(this.selectedLanguage.Code));
        this.otModalCloseEvent.next(true);
        this.disabled = false;
        this.isReady = false;
    }

    closeModal() {
        this.otModalCloseEvent.next(false);
    }
}
