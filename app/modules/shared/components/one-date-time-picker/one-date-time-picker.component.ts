import { Component, Input, Output, EventEmitter, OnChanges, SimpleChanges } from "@angular/core";

import { TimeStamp } from "oneServices/time-stamp.service";

import { IDateTimeOutputModel } from "@onetrust/vitreus";
import { IMyDate } from "mydatepicker";

/*
    ATTENTION: This is NOT meant for reuse.
    It only serves as a downgradeable wrapper for ot-date-time-picker so we can remove md-datepicker from legacy files.
    Please use ot-date-time-picker for new Angular components
*/

@Component({
    selector: "one-date-time-picker",
    templateUrl: "./one-date-time-picker.component.html",
})

export class OneDateTimePickerComponent implements OnChanges {

    @Input() disabled: string;
    @Input() dateDisableUntil: Date;
    @Input() dateDisableSince: Date;
    @Input() datePlaceholder: string;
    @Input() dateTimeModel: string | Date;
    @Input() identifier: string;

    @Output() onChange = new EventEmitter<IDateTimeOutputModel>();

    _dateDisableUntil: IMyDate;
    _dateDisableSince: IMyDate;
    dateFormat = this.timeStamp.getDatePickerDateFormat();
    is24Hour = this.timeStamp.getIs24Hour();

    constructor(private readonly timeStamp: TimeStamp) {
    }

    ngOnChanges() {
        this._dateDisableUntil = this.dateDisableUntil
            ? this.timeStamp.getDisableUntilDate(this.dateDisableUntil)
            : { year: 0, month: 0, day: 0 };
        this._dateDisableSince = this.dateDisableSince
            ? this.timeStamp.getDisableSinceDate(this.dateDisableSince)
            : { year: 0, month: 0, day: 0};
    }

    onDateChange(dateOutput: IDateTimeOutputModel) {
        this.onChange.emit(dateOutput);
    }
}
