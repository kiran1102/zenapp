import {
    Component,
    Input,
} from "@angular/core";
import { StateService } from "@uirouter/core";

@Component({
    selector: "one-anchor-button",
    template: `
        <a
            *ngIf="anchorRoute"
            class="one-button inline-block text-decoration-none cursor-pointer"
            [ngClass]="[
                (type === 'primary' ? 'one-button--primary' : ''),
                (type === 'cta' ? 'one-button--cta' : ''),
                (type === 'warning' ? 'one-button--warning' : ''),
                (isRounded ? 'one-button--rounded' : ''),
                (isFloating ? 'one-button--floating' : ''),
                (wrapText ? 'text-wrap' : 'text-nowrap'),
                (buttonClass ? buttonClass : '')
            ]"
            data-id="{{identifier}}"
            (click)="goToRoute()"
            >
            <span class="one-button__container">
                <i
                    *ngIf="icon"
                    [ngClass]="[icon, iconClass, (isLoading ? 'invisible' : '')]"
                    >
                </i>
                <span
                    class="one-button__text"
                    [ngClass]="{'invisible': isLoading, 'margin-left-right-half': (text || textKey) && !isLoading}"
                    >
                    {{text || (textKey | otTranslate)}}
                </span>
                <i
                    *ngIf="rightSideIcon"
                    [ngClass]="[rightSideIcon, rightSideIconClass, (isLoading ? 'invisible' : '')]"
                    >
                </i>
                <span
                    *ngIf="enableLoading && isLoading"
                    class="one-button__loading fa fa-spinner fa-pulse fa-fw">
                </span>
            </span>
        </a>
        <a
            *ngIf="anchorLink"
            class="one-button inline-block text-decoration-none cursor-pointer"
            [ngClass]="[
                (type === 'primary' ? 'one-button--primary' : ''),
                (type === 'cta' ? 'one-button--cta' : ''),
                (type === 'warning' ? 'one-button--warning' : ''),
                (isRounded ? 'one-button--rounded' : ''),
                (isFloating ? 'one-button--floating' : ''),
                (wrapText ? 'text-wrap' : 'text-nowrap'),
                (buttonClass ? buttonClass : '')
            ]"
            [href]="anchorLink"
            target="{{newTab ? '_blank' : ''}}"
            data-id="{{identifier}}"
            >
            <span class="one-button__container">
                <i
                    *ngIf="icon"
                    [ngClass]="[icon, iconClass]"
                    >
                </i>
                <span
                    class="one-button__text"
                    [ngClass]="{'margin-left-right-half': (text || textKey)}"
                    >
                    {{text || (textKey | otTranslate)}}
                </span>
                <i
                    *ngIf="rightSideIcon"
                    [ngClass]="[rightSideIcon, rightSideIconClass]"
                    >
                </i>
            </span>
        </a>
    `,
})
export class OneAnchorButton {
    @Input() anchorRoute: string;
    @Input() anchorRouteParams: any;
    @Input() anchorLink: string;
    @Input() buttonClass = "";
    @Input() icon = "";
    @Input() iconClass = "";
    @Input() identifier = "";
    @Input() newTab: boolean;
    @Input() rightSideIcon = "";
    @Input() rightSideIconClass = "";
    @Input() text: string;
    @Input() textKey: string;
    @Input() type: string;
    @Input() wrapText: boolean;
    @Input() isLoading: boolean;
    @Input() isRounded: boolean;

    constructor(private stateService: StateService) {}

    goToRoute() {
        if (this.anchorRoute && this.anchorRouteParams) {
            this.stateService.go(this.anchorRoute, this.anchorRouteParams);
        } else {
            this.stateService.go(this.anchorRoute);
        }
    }
}
