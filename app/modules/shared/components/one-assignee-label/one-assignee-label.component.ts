// Angular
import {
    Component,
    Input,
} from "@angular/core";

@Component({
    selector: "one-assignee-label",
    templateUrl: "one-assignee-label.component.html",
})

export class OneAssigneeLabel {

    assigneeStateLabel: string;

    @Input() set assignee(value: string) {
        this.assigneeStateLabel = this.setAssigneeStateLabel(value);
    }

    setAssigneeStateLabel(assignee: string): string {
        return assignee ? assignee : "- - - -";
    }
}
