// Angular
import {
    Component,
    Input,
    Output,
    EventEmitter,
} from "@angular/core";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

// Images
// import "images/empty_state_icon.svg"; // TODO uncomment when removed from app.js
import "images/blank.svg";
import "images/search.svg";
import "images/add_user.svg";
import "images/add_usergroup.svg";

@Component({
    selector: "one-empty-state",
    templateUrl: "one-empty-state.component.html",
})
export class OneEmptyState {
    @Input() emptyStateMessage = this.TranslatePipe.transform("NoItemsToDisplay");
    @Input() hideMessage = false;
    @Input() imagePath = "images/empty_state_icon.svg";
    @Input() actionText: string;
    @Input() actionButtonId: string;
    @Input() showImage = true;
    @Input() showButton = true;
    @Input() isActionEnabled = true;
    @Output() actionCallback = new EventEmitter();

    constructor(
        private readonly TranslatePipe: TranslatePipe,
    ) { }
}
