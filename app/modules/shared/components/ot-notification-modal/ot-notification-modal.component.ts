// 3rd party
import { Component } from "@angular/core";
import { Subject } from "rxjs";

// Interfaces
import { ITaskConfirmationModalResolve } from "interfaces/task-confirmation-modal-resolve.interface";
import { IOtModalContent } from "@onetrust/vitreus";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

@Component({
    selector: "ot-notification-modal",
    templateUrl: "./ot-notification-modal.component.html",
})
export class OTNotificationModalComponent implements IOtModalContent {
    modalTitle: string;
    modalBody: string;
    closeText: string;
    imgSrc: string;

    otModalData: ITaskConfirmationModalResolve;
    otModalCloseEvent: Subject<null>;

    constructor(
        private translatePipe: TranslatePipe,
    ) {}

    ngOnInit(): void {
        this.modalTitle = this.otModalData.modalTitle;
        this.modalBody = this.otModalData.confirmationText;
        this.closeText = this.otModalData.closeButtonText || this.translatePipe.transform("Close");
        this.imgSrc = this.otModalData.imgSrc || "images/task-notification.png";
    }

    closeModal(): void {
        this.otModalCloseEvent.next();
        this.otModalCloseEvent.complete();
    }
}
