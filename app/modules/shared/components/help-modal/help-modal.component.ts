// Angular
import {
    Component,
    OnInit,
    Inject,
} from "@angular/core";
import {
    FormGroup,
    FormBuilder,
    Validators,
    FormControl,
} from "@angular/forms";

// Services
import { SupportService } from "oneServices/support.service";
import { SettingActionService } from "settingsModule/services/actions/setting-action.service";
import { Content } from "sharedModules/services/provider/content.service";
import { Permissions } from "modules/shared/services/helper/permissions.service";

// Tokens
import { StoreToken } from "tokens/redux-store.token";

// Rxjs
import {
    Subject,
    Subscription,
} from "rxjs";
import { startWith } from "rxjs/operators";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

// Redux
import observableFromStore from "oneRedux/utils/observable-from-store";
import {
    getHelpSettingsFetchStatus,
    getHelpSettings,
} from "oneRedux/reducers/setting.reducer";

// Interfaces
import {
    IStore,
    IStoreState,
} from "interfaces/redux.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";
import { ISupportTicket } from "interfaces/support.interface";
import { IHelpSettings } from "interfaces/setting.interface";
import { IHelpResource } from "sharedModules/constants/help-modal.interface";

// Constants
import { NO_BLANK_SPACE } from "constants/regex.constant";

@Component({
    selector: "help-modal",
    templateUrl: "./help-modal.component.html",
})
export class HelpModal implements OnInit {

    loading: boolean;
    customHelpText: string;
    sendingMessage = false;
    messageSubmitted = false;
    helpModalForm: FormGroup;
    selectedItem = null;
    showOnlyForm: boolean = !(
        this.permissions.canShow("HelpGettingStarted")
        || this.permissions.canShow("HelpDocumentation")
        || this.permissions.canShow("HelpTutorials")
    );
    helpResources: IHelpResource[] = [{
        otAutoId: "GettingStartedButton",
        show: this.permissions.canShow("HelpGettingStarted"),
        title: this.content.GetKey("helpModalButtonTxt")[0],
        type: "",
        click: (): void => this.openLink(this.content.GetKey("GetStartedGuide")),
    }, {
        otAutoId: "ProductDocumentationButton",
        show: this.permissions.canShow("HelpDocumentation"),
        title: this.content.GetKey("helpModalButtonTxt")[1],
        click: (): void => this.openLink(this.content.GetKey("ProdDoc")),
        type: "",
    }, {
        otAutoId: "TutorialsButton",
        show: this.permissions.canShow("HelpTutorials"),
        title: this.content.GetKey("helpModalButtonTxt")[2],
        type: "",
        click: (): void => this.openLink(this.content.GetKey("GetStartedVideo")),
    }, {
        otAutoId: "ContactButton",
        show: true,
        // show: Permissions.canShow("HelpContact"),
        title: this.content.GetKey("helpModalButtonTxt")[3],
        type: "select",
        click: (): void => this.toggleContactUs(),
    }];

    otModalCloseEvent: Subject<boolean>;
    private helpSettings: IHelpSettings;
    private subscription: Subscription;
    private ticket: ISupportTicket = { comment: "" };

    constructor(
        @Inject(StoreToken) private store: IStore,
        private settingAction: SettingActionService,
        private supportService: SupportService,
        private translatePipe: TranslatePipe,
        private formBuilder: FormBuilder,
        private permissions: Permissions,
        private content: Content,
    ) { }

    ngOnInit() {
        this.initForm();
        this.subscription = observableFromStore(this.store).pipe(
            startWith(this.store.getState()),
        ).subscribe((state: IStoreState) => this.componentWillReceiveState());
        this.settingAction.fetchHelpSettings();
    }

    componentWillReceiveState() {
        this.loading = getHelpSettingsFetchStatus(this.store.getState());
        this.helpSettings = getHelpSettings(this.store.getState());
        this.customHelpText = this.helpSettings && this.helpSettings.Enabled && this.helpSettings.DefaultComment
            ? this.helpSettings.DefaultComment
            : this.translatePipe.transform("NeedHelpSendMessage");
    }

    sendMessage() {
        this.ticket.comment = this.helpModalForm.get("ticketComment").value;
        this.sendingMessage = true;
        this.supportService.ticket(this.ticket).then((response: IProtocolResponse<void>): void => {
            if (response.result) {
                this.messageSubmitted = true;
                setTimeout(() => {
                    this.otModalCloseEvent.next(false);
                }, 2000);
            } else {
                this.sendingMessage = false;
            }
        });
    }

    closeModal() {
        this.otModalCloseEvent.next(false);
    }

    toggleContactUs() {
        if (this.selectedItem === this.helpResources[3]) {
            this.selectedItem = null;
        } else {
            this.selectedItem = this.helpResources[3];
            this.messageSubmitted = false;
        }
    }

    private initForm() {
        this.helpModalForm = this.formBuilder.group({
            ticketComment: new FormControl("", [
                Validators.required,
            ]),
        });
    }

    private openLink(link: string): void {
        if (link) window.open(link, "_blank");
    }
}
