// Rxjs
import { Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";

// Angular
import {
    Component,
    OnInit,
    Input,
    OnChanges,
    Output,
    EventEmitter,
    SimpleChanges,
} from "@angular/core";

// 3rd party
import { forEach } from "lodash";

// Interfaces
import {
    IRiskHeatmap,
    IRiskSetting,
    IRiskLevel,
} from "interfaces/risks-settings.interface";

import {
    IIndividualRiskSetting,
    ISettingsRiskHeatmapMatrix,
    ISettingsRiskImpact,
    ISettingsRiskProbability,
    IRiskHeatmapSettings,

} from "modules/settings/settings-risks/shared/settings-risks.interface";

// Enums
import { RiskTranslations } from "enums/risk.enum";
import { RiskV2Levels } from "enums/riskV2.enum";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

// Services
import { RiskHelperService } from "sharedModules/services/helper/risk-helper.service";
import { RiskAPIService } from "oneServices/api/risk-api.service";
import { Permissions } from "modules/shared/services/helper/permissions.service";

@Component({
    selector: "risk-selector",
    templateUrl: "./risk-selector.component.html",
})
export class RiskSelectorComponent implements OnInit, OnChanges {

    _impactLevelName: string;
    get impactLevelName(): string {
        return this._impactLevelName;
    }
    @Input("impact") set impactLevelName(value: string) {
        this._impactLevelName = this.helper.getLabelValue(value);
    }
    _probabilityLevelName: string;
    get probabilityLevelName(): string {
        return this._probabilityLevelName;
    }
    @Input("probability") set probabilityLevelName(value: string) {
        this._probabilityLevelName = this.helper.getLabelValue(value);
    }
    @Input("level") riskLevelId: number;
    @Input("disabled") isDisabled = false;
    @Input() showClickableWhenDisabled = false;
    @Input() score: number;

    @Output() onChange: EventEmitter<IRiskHeatmap> = new EventEmitter();

    color: string;
    dropdownIconClass = "ot ot-chevron-down";
    isHeatmapOpen = false;
    hoveredImpactLevelName: string;
    hoveredProbabilityLevelName: string;
    heatmapEnabled: boolean;
    selectedHeatmap: ISettingsRiskHeatmapMatrix;
    impactLevels: ISettingsRiskImpact[];
    probabilityLevels: ISettingsRiskProbability[];
    heatmapMatrix: ISettingsRiskHeatmapMatrix[][];
    loadingSettings: boolean;
    riskLevels: IRiskLevel[] = [];
    riskLevelName: string;
    riskLevel: RiskV2Levels;
    riskV2Levels: typeof RiskV2Levels = RiskV2Levels;
    riskTranslations: typeof RiskTranslations = RiskTranslations;

    private destroy$ = new Subject();

    constructor(
        private readonly helper: RiskHelperService,
        private readonly api: RiskAPIService,
        private readonly permission: Permissions,
        private readonly translatePipe: TranslatePipe,
    ) {
    }

    ngOnInit(): void {
        this.loadingSettings = true;
        let riskSettings$;
        if (this.permission.canShow("AssessmentRiskScore")) {
            riskSettings$ = this.api.getIndividualRiskSetting();
            this.heatmapEnabled = true;
        } else {
            riskSettings$ = this.api.getRiskSetting();
        }
        riskSettings$.pipe(
            takeUntil(this.destroy$),
        ).subscribe((settings: IRiskSetting | IIndividualRiskSetting) => {
            this.heatmapEnabled = false;
            if ((settings as IRiskSetting).enableRiskHeatMap) {
                this.heatmapEnabled = true;
                this.getRiskHeatmapSettings();
                this.loadingSettings = false;
            } else {
                this.getRiskLevels();
                if ((settings as IIndividualRiskSetting).currentScoringMethod) {
                    const scoringMethod = (settings as IIndividualRiskSetting).currentScoringMethod;
                    if (scoringMethod.id === 2) {
                        this.getRiskHeatmapSettings();
                        this.heatmapEnabled = true;
                    }
                }
            }
        });
        }

    ngOnChanges(changes: SimpleChanges): void {
        if (this.riskLevelId) {
            this.riskLevel = RiskV2Levels[RiskV2Levels[this.riskLevelId]];
            this.color = this.helper.getRiskColorByLevel(this.riskLevel);
            this.riskLevelName = this.helper.getRiskLevelLabel(this.riskLevel);
        }
    }

    ngOnDestroy(): void {
        this.destroy$.next();
        this.destroy$.complete();
    }

    onHeatmapButtonToggle(isOpen: boolean): void {
        this.dropdownIconClass = isOpen ? "ot ot-chevron-up" : "ot ot-chevron-down";
        this.isHeatmapOpen = isOpen;
    }

    onHeatmapButtonClick(): void {
        if (!this.isDisabled) {
            this.isHeatmapOpen = !this.isHeatmapOpen;
            this.hoveredImpactLevelName = "";
            this.hoveredProbabilityLevelName = "";
        }
    }

    onHeatmapTileMouseOver(impactRow: ISettingsRiskHeatmapMatrix): void {
        this.hoveredImpactLevelName = `${this.helper.getLabelValue(impactRow.impact.name)} ${this.translatePipe.transform("Impact")}`;
        this.hoveredProbabilityLevelName = `${this.helper.getLabelValue(impactRow.probability.name)} ${this.translatePipe.transform("Probability")}`;
    }

    onHeatmapTileMouseLeave(): void {
        this.hoveredImpactLevelName = "";
        this.hoveredProbabilityLevelName = "";
    }

    onHeatMapTileClick(heatmapCell: ISettingsRiskHeatmapMatrix): void {
        this.impactLevelName = heatmapCell.impact.name;
        this.probabilityLevelName = heatmapCell.probability.name;
        this.score = heatmapCell.score;
        this.color = heatmapCell.color;
        const levelId = this.helper.getRiskLevel(heatmapCell.score);
        const heatmap: IRiskHeatmap = {
            impactLevel: heatmapCell.impact.name,
            impactLevelId: heatmapCell.impact.id,
            probabilityLevel: heatmapCell.probability.name,
            probabilityLevelId: heatmapCell.probability.id,
            riskScore: heatmapCell.score,
            levelId,
        };
        this.onChange.emit(heatmap);
    }

    onRiskLevelSelected(level: IRiskLevel) {
        this.riskLevel = RiskV2Levels[level.name];
        this.riskLevelName = this.helper.getRiskLevelLabel(RiskV2Levels[level.name]);
        this.color = this.helper.getRiskColorByLevel(RiskV2Levels[level.name]);
        this.impactLevelName = "";
        this.probabilityLevelName = "";
        this.score = level.score;

        const risk: IRiskHeatmap = {
            levelId: level.id,
            impactLevel: null,
            impactLevelId: null,
            probabilityLevel: null,
            probabilityLevelId: null,
            riskScore: level.score,
        };
        this.onChange.emit(risk);
    }

    getRiskLevels() {
        this.api.getRiskLevel()
            .pipe(
                takeUntil(this.destroy$),
            )
            .subscribe((riskLevels: IRiskLevel[]) => {
                if (riskLevels) {
                    this.riskLevels = riskLevels;
                    this.loadingSettings = false;
                }
            });
    }

    private getRiskHeatmapSettings() {
        this.api.getHeatmapSettings().pipe(
            takeUntil(this.destroy$),
        ).subscribe((settings: IRiskHeatmapSettings) => {
            if (settings) {
                this.impactLevels = settings.impactLevels;
                this.probabilityLevels = settings.probabilityLevels;
                this.initializeMatrix();
                this.loadingSettings = false;
            }
        });
    }

    private initializeMatrix(): void {
        this.heatmapMatrix = [];
        forEach(this.impactLevels, (impact: ISettingsRiskImpact) => {
            const impactRow: ISettingsRiskHeatmapMatrix[] = [];
            forEach(this.probabilityLevels, (probability: ISettingsRiskProbability) => {
                const score = this.helper.getScore(impact.value, probability.value);
                impactRow.push({
                    impact,
                    probability,
                    score,
                    color: this.helper.getRiskColor(score),
                });
            });
            this.heatmapMatrix.push(impactRow);
        });
    }
}
