import {
    Component,
    ViewChild,
    OnInit,
} from "@angular/core";

// 3rd Party
import { Subject } from "rxjs";

// Interfaces
import { IListItem, IListState } from "@onetrust/vitreus";
import { OtDuelingPicklistComponent } from "interfaces/ot-dueling-picklist.interface";

export interface IListColumnSelectorData {
    title?: string;
    left: IListItem[];
    right: IListItem[];
    labelKey?: string;
    leftHeaderLabel?: string;
    rightHeaderLabel?: string;
    resetToDefault?: () => IListState;
}

@Component({
    selector: "grid-column-selector",
    templateUrl: "./grid-column-selector.component.html",
})
export class GridColumnSelectorComponent implements OnInit {

    otModalCloseEvent: Subject<IListState>;
    otModalData: IListColumnSelectorData;

    @ViewChild("duelingPicklist")
    duelingListComponent: OtDuelingPicklistComponent;

    modalTitle: string;
    leftList: IListItem[];
    rightList: IListItem[];
    labelKey: string;
    labelLeft: string;
    labelRight: string;
    noChanges = true;

    constructor() {}

    ngOnInit(): void {
        this.modalTitle = this.otModalData.title || "ColumnSelector";
        this.labelKey = this.otModalData.labelKey || "key";
        this.leftList = this.otModalData.left || [];
        this.rightList = this.otModalData.right || [];
        this.labelLeft = this.otModalData.leftHeaderLabel || "AvailableFields";
        this.labelRight = this.otModalData.rightHeaderLabel || "VisibleFields";
    }

    resetToDefault() {
        const defaultState = this.otModalData.resetToDefault();
        if (!this.doArraysHaveSameLength(defaultState.left, this.leftList)
            || !this.doArraysHaveSameLength(defaultState.right, this.rightList)
            || !this.doArraysMatch(defaultState.left, this.leftList)
            || !this.doArraysMatch(defaultState.right, this.rightList)
        ) {
            this.leftList = defaultState.left;
            this.rightList = defaultState.right;
            this.onChanged();
        }
    }

    doArraysHaveSameLength(reset: IListItem[], current: IListItem[]): boolean {
        return reset.length === current.length;
    }

    doArraysMatch(reset: IListItem[], current: IListItem[]): boolean {
        let doMatch = true;
        for (let i = 0; i < current.length; i++) {
            if (reset[i][this.labelKey] !== current[i][this.labelKey]) {
                doMatch = false;
                break;
            }
        }
        return doMatch;
    }

    saveAndClose() {
        const listState = this.duelingListComponent.getListState();
        this.closeModal(listState);
    }

    closeModal(data: IListState = null) {
        this.otModalCloseEvent.next(data);
        this.otModalCloseEvent.complete();
    }

    onChanged() {
        this.noChanges = false;
    }
}
