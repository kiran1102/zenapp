import {
    Component,
    OnInit,
    Inject,
} from "@angular/core";

// Services
import { StateService } from "@uirouter/core";
import { AttachmentService } from "oneServices/attachment.service";

// Interfaces
import { IProtocolResponse } from "interfaces/protocol.interface";

@Component({
    selector: "download-landing",
    template: `
        <ot-loading
            otPageLoading
            class="app-loading flex-full-height"
            >
            {{ "DownloadingFile" | otTranslate }}
        </ot-loading>
    `,
})
export class DownloadLandingComponent implements OnInit {

    constructor(
        private stateService: StateService,
        private attachmentService: AttachmentService,
        @Inject("Export") private Export: any,
    ) {}

    ngOnInit() {
        const documentId: string = this.stateService.params.documentId;
        if (!documentId) {
            this.stateService.go("module", { path: "welcome" });
            return;
        }
        this.attachmentService.downloadAttachment(documentId).then(
            (res: IProtocolResponse<unknown>) => {
                if (res.result) {
                    const contentDisposition: string = res.headers
                        ? res.headers["content-disposition"] || res.headers["Content-Disposition"]
                        : "";
                    const splitHeader: string[] = contentDisposition
                        ? contentDisposition.split(".")
                        : [];
                    const fileExtension: string =
                        splitHeader.length && splitHeader.length > 1
                            ? splitHeader[splitHeader.length - 1].replace(/"/g, "") : "";
                    this.Export.basicFileDownload(
                        res.data,
                        `download_${documentId}.${fileExtension}`,
                    );
                }
                this.stateService.go("module", { path: "welcome" });
            },
        );
    }
}
