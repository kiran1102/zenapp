// 3rd party
import { Component, Input, Output, EventEmitter } from "@angular/core";

// TODO : Deperecate and remove this once `ot-modal-base` is defined and exists.
@Component({
    selector: "one-modal-base",
    templateUrl: "./one-modal-base.component.html",
    host: { class: "full-size" },
})
export class OneModalBase {
    @Input() modalTitle?: string;
    @Input() identifier?: string;
    @Input() closeIsDisabled?: boolean;
    @Input() hideClose?: boolean;

    @Output() close = new EventEmitter();

    modalClose() {
        if (this.close.observers.length) {
            this.close.emit();
        }
    }
}
