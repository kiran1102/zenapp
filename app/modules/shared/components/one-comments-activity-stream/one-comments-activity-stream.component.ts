// Angular
import {
    Component,
    Input,
    Output,
    EventEmitter,
} from "@angular/core";

// Enums and Constants
import { CommentsActivitiesEvents } from "sharedModules/enums/one-comments-activities.enum";

// Interfaces
import { IAction } from "interfaces/redux.interface";
import { IRequestQueueComment } from "modules/dsar/interfaces/request-queue-comment.interface";
import { IAttachmentFileResponse } from "interfaces/attachment-file.interface";

@Component({
    selector: "one-comments-activity-stream",
    templateUrl: "./one-comments-activity-stream.component.html",
})
export class OneCommentsActivityStreamComponent {
    @Input() activities: IRequestQueueComment[];
    @Output() actionCallback: EventEmitter<IAction> = new EventEmitter<IAction>();

    commentsActivitiesEvents = CommentsActivitiesEvents;
    constructor() { }

    handleAction(type, payload) {
        switch (type) {
            case CommentsActivitiesEvents.DELETE_FILE_FROM_COMMENT:
                this.actionCallback.emit({ type, payload });
                break;
            case CommentsActivitiesEvents.DOWNLOAD_FILE:
                this.actionCallback.emit({ type, payload });
                break;
        }
    }

    trackByFn(index: number, activity: any): number {
        return activity.id;
    }

    trackByAttachmentName(index: number, attachment: IAttachmentFileResponse): string {
        return attachment.name;
    }

}
