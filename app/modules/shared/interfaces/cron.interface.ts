export interface ICronObject {
    minute: number;
    hour: number;
    daysOfTheWeek: number[];
    dayOfMonth: number;
}
