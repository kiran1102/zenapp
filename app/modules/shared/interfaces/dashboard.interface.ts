export interface IGraphDatum {
    DaysAgo: number;
    HoursAgo: number;
    MinutesAgo: number;
    Time: number;
}
