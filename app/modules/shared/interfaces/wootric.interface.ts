export interface IWootricWindow extends Window, IWootricWindowProperties {
    customMessages: IWootricCustomMessages;
    wootricSettings: IWootricSettings;
    WootricSurvey: IWootricSurvey;
    Wootric?: IWootricThankYou;
}

export interface IWootricWindowProperties {
    wootric_event_queue: any[];
    wootric_no_surveyed_cookie: boolean;
    wootric_segment_integration: boolean;
    wootric_show_logs: boolean;
    wootric_survey_immediately: boolean;
    wootric_survey_running: boolean;
    wootric: (...params: Array<string | number>) => void;
}

export interface IWootricSurvey {
    run: (wootricSettings?: IWootricSettings) => void;
    stop: () => void;
    version: () => string;
}

export interface IWootricConfig {
    messages?: IWootricCustomMessages;
    settings: IWootricSettings;
    wootric?: IWootricThankYou;
}

export interface IWootricSettings extends IWootricModalSettings {
    account_token: string;
    created_at?: number;
    email: string;
    language?: string;
    product_name?: string;
    properties: IWootricCustomAttributes;
}

export interface IWootricCustomMessages {
    followup_question?: string;
    followup_questions_list?: {
        detractor_question?: string;
        passive_question?: string;
        promoter_question?: string;
    };
    placeholder_text?: string;
    placeholder_texts_list?: {
        detractor_text?: string;
        passive_text?: string;
        promoter_text?: string;
    };
}

export interface IWootricModalSettings {
    wootric_recommend_target?: string;
    modal_theme?: "light" | "dark";
    modal_footprint?: "normal" | "compact" | "spacious";
    modal_position?: "top" | "bottom";
    aria: boolean;
}

export interface IWootricSocial {
    facebook_page?: string;
    twitter_account?: string;
}

export interface IWootricThankYou {
    add_score_param_to_url: boolean;
    add_comment_param_to_url: boolean;
    thankYouMessages: {
        thank_you_setup: string;
        thank_you_setup_list: {
            detractor_thank_you_setup: string;
            passive_thank_you_setup: string;
            promoter_thank_you_setup: string;
        },
        thank_you_main: string;
        thank_you_main_list: {
            detractor_thank_you_main: string;
            passive_thank_you_main: string;
            promoter_thank_you_main: string;
        }
    };
    thankYouLinks: {
        thank_you_link_text: string;
        thank_you_link_url: string;
        thank_you_link_text_list: {
            detractor_thank_you_link_text: string;
            passive_thank_you_link_text: string;
            promoter_thank_you_link_text: string;
        },
        thank_you_link_url_list: {
            detractor_thank_you_link_url: string;
            passive_thank_you_link_url: string;
            promoter_thank_you_link_url: string;
        }
    };
}

export interface IWootricCustomAttributes {
    role: string;
    module: string;
    release: string;
    email: string;
    tenant: string;
}
