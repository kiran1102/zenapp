import { AssessmentStatus } from "enums/assessment/assessment-status.enum";
import { IStringMap } from "interfaces/generic.interface";

export interface IViewRelatedAssessmentModalData {
    assessmentId: string;
    assessmentName: string;
    assessmentStatus: AssessmentStatus;
    relatedAssessments: IViewRelatedAssessmentChildren[];
    templateName: string;
}

export interface IViewRelatedAssessmentChildren {
    id: string;
    assessmentName: string;
    templateName: string;
    status: AssessmentStatus;
    relationship: string;
}

export interface IViewRelatedAssessmentColumns {
    columnName: string;
    cellValueKey: string;
}

export interface IViewRelatedCurrentAssessmentDetails {
    assessmentId: string;
    assessmentName: string;
    assessmentStatus: string;
}
