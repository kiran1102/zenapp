export interface ISystemSubtaskInfo {
    id: string;
    index: number;
    count: number;
}
