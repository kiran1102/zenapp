import { IStringMap } from "interfaces/generic.interface";
export interface IUserlaneWindow extends Window {
    Userlane: (...params: IUserlaneParams) => void;
    UserlaneCommandObject: string;
    userlaneFacade: IUserlaneFacade;
    userlaneWebpackJsonp: any[];
}

export interface IUserlaneFacade {
    getEditorModule: (e, t) => any;
    getModule: (e) => any;
    showDiagnostics: () => any;
    start: (e) => any;
}

export type IUserlaneParams = Array<string|number|IUserlaneAttributes>;
export type IUserlaneAttributes = IStringMap<string|number|boolean|string[]>;
