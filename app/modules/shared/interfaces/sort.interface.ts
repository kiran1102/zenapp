export interface ISortEvent {
    sortKey: string;
    sortOrder: string;
}
