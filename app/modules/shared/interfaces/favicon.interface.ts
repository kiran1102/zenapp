export interface IFaviconsConfig {
    icons: Map<string, IFavicon[] | boolean>;
    cacheBusting?: boolean;
}

export interface IFavicon {
    href: string;
    rel: string;
    type: string;
    isDefault?: boolean;
}
