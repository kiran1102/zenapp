export interface IPermissions {
    [index: string]: boolean;
}
