import { Directive } from "@angular/core";

@Directive({
    selector: "[emptyStateImg]",
    host: {
        "[src]": "imgSrc",
    },
})
export class EmptyStateImageDirective {
    imgSrc = require("../../../images/empty_state_icon.svg");
}
