// Core
import {
    Directive,
    ElementRef,
    Input,
} from "@angular/core";

// Forms
import { NgControl } from "@angular/forms";

/* This directive is used to fix the below issue in IE
** Angular2 setting <textarea> as dirty when using placeholder text on IE 11
** Usage:
    <textarea
        otTextarea
        [otTextAreaPlaceholder]="'Placeholder Text here'| otTranslate"
    ></textarea>
*/
@Directive({
    selector: "[otTextAreaPlaceholder]",
})
export class OtTextAreaPlaceholderDirective {
    constructor(private el: ElementRef, private control: NgControl) { }
    @Input("otTextAreaPlaceholder") set defineInputType(pattern: string) {
        this.el.nativeElement.placeholder = pattern;
        setTimeout(() => {
            this.control.control.markAsPristine();
        }, 0);
    }
}
