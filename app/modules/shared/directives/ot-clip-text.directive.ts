import {
    Directive,
    ElementRef,
    AfterViewInit,
    Renderer2,
} from "@angular/core";

@Directive({
    selector: "[otClipText]",
})

export class OtClipTextDirective implements AfterViewInit {
    constructor(
        private el: ElementRef,
        private renderer: Renderer2,
    ) { }

    ngAfterViewInit() {
        const element = this.el.nativeElement;
        if (element.offsetHeight < element.scrollHeight) {
            this.renderer.addClass(element, "clip-text");
        }
    }
}
