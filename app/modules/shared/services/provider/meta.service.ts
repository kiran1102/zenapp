// Angular
import { Injectable, Inject } from "@angular/core";

// Service
import { Favicons, IconSet } from "sharedModules/services/provider/favicon.service";

// Redux
import { IStore } from "interfaces/redux.interface";
import { StoreToken } from "tokens/redux-store.token";

// Constants
import {
    COOKIEPRO_BACKGROUND_COLOR,
    ONETRUST_BACKGROUND_COLOR,
} from "constants/branding.constant";
import { BrandingActions } from "oneRedux/reducers/branding.reducer";

// Interfaces
import { IStringMap } from "interfaces/generic.interface";

@Injectable()
export class MetaService {

    metaId = `ot-meta`;
    styleId = `ot-style`;
    year = (new Date()).getFullYear();
    copyright = `©${this.year} OneTrust LLC`;
    privacyPolicy = `https://www.onetrust.com/privacy-notice/`;

    constructor(
        @Inject(StoreToken) private store: IStore,
        private favicons: Favicons,
    ) {
        this.appendToHead(`<title class="${this.metaId}">${window.location.host}</title>`);
    }

    // TODO: Upgrade this service to not be so specific to CookiePro.
    applyMetaTags(content: IStringMap<string>) {
        const isCookiePro = content.License === "CookiePro";
        const company = isCookiePro ? "CookiePro" : "OneTrust";
        this.copyright = `©${this.year} ${company}${isCookiePro ? "" : " LLC"}`;
        this.privacyPolicy = isCookiePro ? "https://www.cookiepro.com/privacy-policy" : "https://www.onetrust.com/privacy-notice";
        const support = `support@${isCookiePro ? "cookiepro" : "onetrust"}.com`;
        const title = `${company}${isCookiePro ? "" : " | Privacy Management Software"}`;
        const favicon = isCookiePro ? IconSet.cookiepro : IconSet.onetrust;
        const brandColor = isCookiePro ? COOKIEPRO_BACKGROUND_COLOR : ONETRUST_BACKGROUND_COLOR;
        const language = localStorage.selectedLanguage || "en-us";
        const defaultBranding = { header: { backgroundColor: brandColor } };
        this.store.dispatch({
            type: BrandingActions.SET_DEFAULT_BRANDING_DATA,
            defaultBranding,
        });

        this.removeBySelector(`.${this.metaId}`);
        this.removeBySelector(`#${this.styleId}`);

        this.appendToHead(`
            <title class="${this.metaId}" >${title}</title>

            <meta class="${this.metaId}" name="description" content="${title}">
            <meta class="${this.metaId}" name="robots" content="none">
            <meta class="${this.metaId}" name="copyright" content="${this.copyright}">
            <meta class="${this.metaId}" name="language" content="${language}">
            <meta class="${this.metaId}" name="HandheldFriendly" content="True">
            <meta class="${this.metaId}" name="MobileOptimized" content="320">

            <link class="${this.metaId}" rel="help" title="${company} Support" href="mailto:${support}">
        `);

        this.useFavicon(favicon);

    }

    public useFavicon(iconSet: IconSet): void {
        this.favicons.activate(iconSet);
    }

    private appendToHead(htmlEl: string) {
        const div = document.createElement("div");
        div.innerHTML = htmlEl.trim();

        for (let i = 0; i < div.childNodes.length; i++) {
            document.head.appendChild(div.childNodes.item(i));
        }
    }

    private removeBySelector(selector: string) {
        const elements = document.head.querySelectorAll(selector);
        if (!elements.length) return;
        else if (elements.length === 1) {
            this.removeEl(elements.item(0));
            return;
        } else {
            for (let i = 0; i < elements.length; i++) {
                this.removeEl(elements.item(i));
            }
        }
    }

    private removeEl(el: Element) {
        if (el.parentElement) {
            el.parentElement.removeChild(el);
        }
    }

}
