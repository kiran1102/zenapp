﻿// 3rd Party
import { merge, isUndefined } from "lodash";

// Services
import { Settings } from "sharedServices/settings.service";
import { Principal } from "modules/shared/services/helper/principal.service";

// Constant
import {
    DEFAULT_BACKGROUND_COLOR,
    DEFAULT_TEXT_COLOR,
} from "constants/branding.constant";
import { UserRoles } from "constants/user-roles.constant";

// Interfaces
import { IBrandingData } from "interfaces/branding.interface";
export class Branding {

    myBrands = this.getBrandingFromStorage();

    constructor(
        private $q: ng.IQService,
        private $localStorage: any,
        private settings: Settings,
        private principal: Principal,
    ) {}

    /**
     * @name getZenDefault
     * @description Return OneTrust Default Branding
     * @return Object app.branding
     */
    getZenDefault() {
        // set defaults
        this.myBrands = {
            InheritsFrom: null,
            IsInherited: false,
            branding: {
                logo: {
                    url: "",
                    name: "",
                },
                header: {
                    icon: {
                        url: "",
                        name: "",
                    },
                    backgroundColor: DEFAULT_BACKGROUND_COLOR,
                    textColor: DEFAULT_TEXT_COLOR,
                    title: "",
                },
            },
        };
        return this.myBrands;
    }

    /**
     * @name getBrandingFromStorage
     * @description Get our Branding from Storage
     * @return Object app.branding
     */
    getBrandingFromStorage() {
        if (this.principal.getRoleName() !== UserRoles.Invited) {
            let localBrands = this.$localStorage.ZenBrands;
            if (localBrands && !localBrands.branding) {
                localBrands = this.settings.structureBranding(localBrands);
            }
            this.myBrands = merge(this.getZenDefault(), localBrands || {});
        }
        return this.myBrands;
    }

    /**
     * @name sendToServer
     * @description Send Branding Settings to the Server
     * @param branding
     * @return Object serverBranding
     */
    sendToServer(branding) {
        const deferred = this.$q.defer();
        this.settings.setBranding(branding)
            .then((response) => {
                if (response.result) {
                    this.myBrands = branding;
                }
                deferred.resolve(this.myBrands);
            });
        return deferred.promise;
    }

    /**
     * @name  Server
     * @description Get Branding Settings from the Server
     * @return Object serverBranding
     */
    getFromServer() {
        const deferred = this.$q.defer();
        this.settings.getBranding()
            .then((response) => {
                if (response && response.data !== null) {
                    this.myBrands = response.data;
                }
                deferred.resolve(this.myBrands);
            });
        return deferred.promise;
    }

    /**
     * @name deleteFromServer
     * @description Delete Branding Settings from the Server
     * @return response
     */
    deleteFromServer() {
        const deferred = this.$q.defer();
        this.settings.deleteBranding()
            .then((response) => {
                deferred.resolve(response);
            });
        return deferred.promise;
    }

    /**
     * @name getBranding
     * @description Get User's Branding
     * @return Object app.branding
     */
    getBranding(force): ng.IPromise<IBrandingData> {
        const deferred: ng.IDeferred<IBrandingData> = this.$q.defer();
        if (force) {
            this.getFromServer()
                .then((brands: IBrandingData) => {
                    deferred.resolve(brands);
                });
            return deferred.promise;
        } else {
            // Check if our branding is loaded.
            const storageBranding = this.getBrandingFromStorage();
            if (this.myBrands) {
                deferred.resolve(this.myBrands);
                return deferred.promise;
            } else if (storageBranding) {
                // Check if our branding exists on $localStorage
                this.myBrands = storageBranding;
                deferred.resolve(this.myBrands);
                return deferred.promise;
            } else {
                // Finally, just get from the server, set $localStorage and this.myBrands, and return the new branding.
                this.getFromServer()
                    .then((brands: IBrandingData) => {
                        deferred.resolve(brands);
                    });
                return deferred.promise;
            }
        }
    }

    /**
     * @name setBranding
     * @description Set User Custom Branding
     * @param customBranding
     */
    setBranding(customBranding) {
        return this.sendToServer(customBranding);
    }
}

Branding.$inject = ["$q", "$localStorage", "Settings", "PRINCIPAL"];
