// Angular
import { Injectable, Inject } from "@angular/core";
import { DOCUMENT } from "@angular/common";

@Injectable()
export class ScriptLoader {
    constructor(@Inject(DOCUMENT) private _document: Document) {}

    load(name: string, src: string) {
        return new Promise((resolve, reject) => {
            const scriptEl: HTMLScriptElement = this._document.createElement("script");
            scriptEl.type = "text/javascript";
            scriptEl.id = name;
            scriptEl.src = src;
            scriptEl.async = true;
            scriptEl.onload = (event) => resolve(event);
            scriptEl.onerror = (event) => reject(event);
            this._document.head.appendChild(scriptEl);
        });
    }
}
