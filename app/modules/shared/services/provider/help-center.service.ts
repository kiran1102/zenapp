// Rxjs
import { Subject } from "rxjs";
import {
    startWith,
    takeUntil,
} from "rxjs/operators";

// 3rd Party
import {
    Injectable,
    Inject,
} from "@angular/core";

// Redux
import { getConfigurationByType } from "oneRedux/reducers/configuration.reducer";
import {
    getCurrentUser,
    getUserDisplayName,
} from "oneRedux/reducers/user.reducer";
import { getHeaderBranding } from "oneRedux/reducers/branding.reducer";
import { StoreToken } from "tokens/redux-store.token";
import observableFromStore from "oneRedux/utils/observable-from-store";

// Services
import { ConfigurationAction } from "modules/shared/services/actions/configuration-action.service";
import { ScriptLoader } from "./../provider/script-loader.service";
import { Content } from "sharedModules/services/provider/content.service";

// Interfaces
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import { IHelpCenterConfiguration } from "interfaces/configuration.interface";
import {
    IStore,
    IStoreState,
} from "interfaces/redux.interface";
import {
    IHelpCenterWindow,
    IZendeskWidget,
    IZendeskSettingsWebWidget,
} from "interfaces/help-center.interface";

// Constants
import { Color } from "constants/color.constant";
declare var window: IHelpCenterWindow;

@Injectable()
export class HelpCenter {
    private helpCenterType = this.configurationAction.types.HelpCenter;

    private config: IHelpCenterConfiguration;
    private currentUserName: string;
    private currentUserEmail: string;
    private currentTheme: string;
    private destroy$ = new Subject();
    private isActive = false;

    constructor(
        @Inject(StoreToken) private store: IStore,
        @Inject("$rootScope") private $rootScope: IExtendedRootScopeService,
        private configurationAction: ConfigurationAction,
        private scriptLoader: ScriptLoader,
        private content: Content,
    ) {}

    initialize(closeCallback?: () => void) {
        observableFromStore(this.store).pipe(
            startWith(this.store.getState()),
            takeUntil(this.destroy$),
        ).subscribe((state: IStoreState) => this.serviceWillReceiveState(state));

        return this.getConfigurations().then(() => {
            if (!this.config || !this.config.token) {
                return;
            }
            this.updateSettings(this.config.token);
            this.apply((zE: any) => {
                zE.hide();
                zE.identify({
                    name: this.currentUserName,
                    email: this.currentUserEmail,
                    organization: this.config.organization,
                });
                zE.setLocale(this.$rootScope.tenantLanguage);
                if (closeCallback) {
                    zE("webWidget:on", "close", () => closeCallback());
                }
                setTimeout(() => {
                    this.isActive = true;
                }, 1000);
            });
        });
    }

    serviceWillReceiveState(state: IStoreState) {
        this.config = getConfigurationByType(this.helpCenterType)(state) as IHelpCenterConfiguration;
        const jwt = this.config ? this.config.token : "";
        if (!jwt) return;
        const currentUser = getCurrentUser(state);
        this.currentUserName = getUserDisplayName(currentUser);
        this.currentUserEmail = currentUser.Email;
        const branding = getHeaderBranding(state);
        this.currentTheme = branding ? branding.backgroundColor : Color.Green;
        this.updateSettings(jwt);
    }

    open(search?: string) {
        if (!this.helpCenterIsLive) return;
        this.updateSettings(this.config.token);
        this.apply((zE: IZendeskWidget) => {
            const suggesstions = search ? { search } : {};
            zE.setHelpCenterSuggestions(suggesstions);
            zE.activate({ hideOnClose: true });
        });
    }

    close() {
        if (!this.helpCenterIsLive) return;
        this.apply((zE: IZendeskWidget) => {
            zE.hide();
        });
    }

    logout() {
        this.destroy$.next();
        this.destroy$.complete();
        if (!this.helpCenterIsLive) return;
        this.apply((zE: IZendeskWidget) => {
            this.isActive = false;
            this.removeFromStorage(localStorage, "ZD-");
            this.removeFromStorage(sessionStorage, "ZD-");
            this.removeFromWindow();
            this.removeElements();
        });
    }

    get helpCenterIsLive() {
        return Boolean(this.isActive && window.zEACLoaded && window.zEmbed && window.zE);
    }

    private updateSettings(jwt: string) {
        window.zESettings = {
            authenticate: { jwt },
            webWidget: this.getWebWidgetCustomizations(jwt),
        };
    }

    private getConfigurations(): ng.IPromise<Promise<any>> {
        const prm = this.configurationAction.getConfigurationByType(this.helpCenterType) as ng.IPromise<any>;
        return prm.then((config: IHelpCenterConfiguration) => {
            if (!config.token) return null;
            const configType = this.configurationAction.config[this.helpCenterType];
            const src = configType.getSrc ? configType.getSrc(config) : configType.src;
            return this.scriptLoader.load(configType.id, src).then(
                (script) => {
                return script;
            }, (error) => {
                return error;
            });
        });
    }

    private apply(cb: (zE: IZendeskWidget) => void) {
        window.zE(() => {
            const zE = (window as any).zE as IZendeskWidget;
            cb(zE);
        });
    }

    private getWebWidgetCustomizations(jwt: string): IZendeskSettingsWebWidget {
        const overrides = this.content.GetKey("HelpCenterSettings") || {};
        return {
            authenticate: { jwt },
            color: {
                theme: this.currentTheme,
                articleLinks: Color.Blue,
                resultLists: Color.Blue,
            },
            helpCenter: { title: { "*": "Support" } },
            contactOptions: { enabled: false },
            contactForm: { suppress: true },
            position: { horizontal: "right", vertical: "top" },
            offset: { horizontal: "-5px", vertical: "60px" },
            ...overrides,
        };
    }

    private removeFromWindow() {
        for (const key in window) {
            if (window.hasOwnProperty(key) && key.startsWith("zE")) {
                window[key] = undefined;
            }
        }
    }

    private removeFromStorage(storage: Storage, prefix: string) {
        const keys = [];
        for (let i = 0; i < storage.length; i++) {
            if (storage.key(i).substring(0, prefix.length) === prefix) {
                keys.push(storage.key(i));
            }
        }
        for (const key of keys) {
            storage.removeItem(key);
        }
    }

    private removeElements() {
        const selectors = [
            "head > *[id*='ze-snippet']",
            "head > *[src*='zdassets']",
            "body > *[data-product*='web_widget']",
        ];

        const helpCenterElements: HTMLElement[] = Array.prototype.slice.call(document.querySelectorAll(selectors.join(", ")));
        helpCenterElements.forEach((el: HTMLElement) => {
            el.parentElement.removeChild(el);
        });

        const zeScript = document.getElementById("ze-snippet");
        if (zeScript) {
            document.head.removeChild(zeScript);
        }
        const webWidgetEl = document.getElementById("webWidget");
        const launcherEl = document.getElementById("launcher");
        if (webWidgetEl) {
            document.body.removeChild(webWidgetEl.parentElement);
            document.body.removeChild(launcherEl.parentElement);
        }
    }
}
