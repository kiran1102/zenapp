declare var VERSION: string;
// Angular
import {
    Injectable,
    Inject,
} from "@angular/core";
import { DOCUMENT } from "@angular/common";

// rxjs
import { Subject } from "rxjs";
import { startWith, takeUntil } from "rxjs/operators";

// Redux
import { getConfigurationByType } from "oneRedux/reducers/configuration.reducer";
import { getCurrentUser } from "oneRedux/reducers/user.reducer";
import { StoreToken } from "tokens/redux-store.token";
import observableFromStore from "oneRedux/utils/observable-from-store";

// Services
import { ConfigurationAction } from "modules/shared/services/actions/configuration-action.service";
import { ScriptLoader } from "./script-loader.service";
import { Content } from "sharedModules/services/provider/content.service";
import { SettingsLearningAndFeedbackService } from "settingsServices/apis/settings-learning-and-feedback-api.service";
import { Permissions } from "modules/shared/services/helper/permissions.service";

// Interfaces
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import { IWootricConfiguration } from "interfaces/configuration.interface";
import { IStore, IStoreState } from "interfaces/redux.interface";
import {
    IWootricConfig,
    IWootricWindow,
} from "../../interfaces/wootric.interface";
import { ILearningAndFeedbackSetting } from "interfaces/settings-learning-and-feedback-tool.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

// Constants
import { ToolNames } from "constants/learning-and-feedback-tool.constant";

@Injectable()
export class Wootric {
    isActive = false;
    version = VERSION;

    private _window = window as IWootricWindow;
    private widgetType = this.configurationAction.types.Wootric;
    private config: IWootricConfiguration;
    private currentUserId: string;
    private destroy$ = new Subject();

    constructor(
        @Inject(DOCUMENT) private _document: Document,
        @Inject(StoreToken) private store: IStore,
        @Inject("$rootScope") private $rootScope: IExtendedRootScopeService,
        public permissions: Permissions,
        private configurationAction: ConfigurationAction,
        private scriptLoader: ScriptLoader,
        private content: Content,
        private translatePipe: TranslatePipe,
        private settingsLearningAndFeedbackService: SettingsLearningAndFeedbackService,
    ) {}

    initialize() {
        observableFromStore(this.store).pipe(
            startWith(this.store.getState()),
            takeUntil(this.destroy$),
        ).subscribe((state: IStoreState) => this.serviceWillReceiveState(state));

        return this.getConfigurations().then(() => {
            if (!this.config || !this.config.token) {
                return;
            }
            if (this._window.WootricSurvey) {
                this.isActive = true;
                this.applyStyles();
            }
        });
    }

    run(moduleName: string) {
        if (this.permissions.canShow("UserToolSetting")) {
            this.settingsLearningAndFeedbackService.getFeedbackSettings()
                .then(
                    (response: IProtocolResponse<ILearningAndFeedbackSetting>) => {
                        if (response.result) {
                            const settings = response.data;
                            const wootriceEnabled = settings.enabled;
                            if (wootriceEnabled && this.isActive && this.permissions.canShow("UserFeedBack")) {
                                this._window.wootric_no_surveyed_cookie = true;
                                this._window.wootric_survey_immediately = this.content.GetKey("WootricSurveyImmediately") === "true";
                                this.updateSettings(moduleName);
                                this._window.WootricSurvey.run();
                            }
                        }
                    },
                );
        }
    }

    updateSettings(moduleName: string) {
        const customizations = this.getWootricCustomizations(moduleName);
        this._window.wootricSettings = customizations.settings;
        this._window.customMessages = customizations.messages;
        this._window.Wootric = customizations.wootric;
    }

    serviceWillReceiveState(state: IStoreState) {
        this.config = getConfigurationByType(this.widgetType)(state) as IWootricConfiguration;
        const token = this.config ? this.config.token : "";
        if (!token) return;
        const currentUser = getCurrentUser(state);
        this.currentUserId = currentUser.Id;
    }

    logout() {
        this.destroy$.next();
        this.destroy$.complete();
        this.isActive = false;
        this._window.customMessages = undefined;

        if (this._window.WootricSurvey) {
            this._window.WootricSurvey.stop();
        }

        for (const key in this._window) {
            if (this._window.hasOwnProperty(key) && key.toLowerCase().startsWith("wootric")) {
                this._window[key] = undefined;
            }
        }

        const selectors = [
            "head > *[href*='wootric']",
            "head > *[id*='wootric']",
            "body > *[id*='wootric']",
        ];

        const userlaneElements: HTMLElement[] = Array.prototype.slice.call(document.querySelectorAll(selectors.join(", ")));
        userlaneElements.forEach((el: HTMLElement) => {
            el.parentElement.removeChild(el);
        });
    }

    private getConfigurations(): Promise<{}> {
        const prm = this.configurationAction.getConfigurationByType(this.widgetType, true) as Promise<{}>;
        return prm.then((config: IWootricConfiguration) => {
            if (!config.token) return null;
            const configType = this.configurationAction.config[this.widgetType];
            const src = configType.getSrc ? configType.getSrc(config) : configType.src;
            return this.scriptLoader.load(configType.id, src);
        });
    }

    private getWootricCustomizations(moduleName: string): IWootricConfig {
        const overrides = JSON.parse(this.content.GetKey("WootricSettings")) || {};
        const currentUser = getCurrentUser(this.store.getState());
        return {
            settings: {
                account_token: this.content.GetKey("OneTrustWootricAccountNumber"),
                created_at: Math.round(new Date().getTime() / 1000), // to get Unix timestamp
                email: currentUser.EmailHash,
                language: this.$rootScope.tenantLanguage.substring(0, 2).toUpperCase(),
                disclaimer: {
                    text: this.translatePipe.transform("WootricFeedbackDisclaimer"),
                    link: this.content.GetKey("privacyPolicy"),
                    link_word: this.translatePipe.transform("PrivacyNotice"),
                },
                properties: {
                    role: currentUser.RoleName,
                    module: moduleName,
                    release: this.version,
                    tenant: currentUser.CurrentTenant,
                },
                product_name: this.translatePipe.transform("WootricPrimaryFeedbackPrompt"),
            },
            messages: {
                followup_question: this.translatePipe.transform(
                    "WootricCustomFeedbackPrompt",
                ),
            },
            wootric: {},
            ...overrides,
        };
    }

    private applyStyles() {
        const styles: HTMLStyleElement = this._document.createElement("style");
        styles.type = "text/css";
        styles.id = "wootric-custom-styles";
        styles.textContent = `
            #wootric-form {
                max-width: 100%;
            }
            .wootric-separator, .wootric-powered-by {
                display: none !important;
            }
        `;
        this._document.body.appendChild(styles);
    }
}
