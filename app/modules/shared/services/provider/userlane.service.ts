declare var VERSION: string;

// Angular
import {
    Injectable,
    Inject,
} from "@angular/core";
import { DOCUMENT } from "@angular/common";

// rxjs
import { Subject } from "rxjs";
import { startWith, takeUntil } from "rxjs/operators";

// Redux
import { getConfigurationByType } from "oneRedux/reducers/configuration.reducer";
import { getCurrentUser } from "oneRedux/reducers/user.reducer";
import { StoreToken } from "tokens/redux-store.token";
import observableFromStore from "oneRedux/utils/observable-from-store";

// Services
import { NgxPermissionsService } from "ngx-permissions";
import { ConfigurationAction } from "modules/shared/services/actions/configuration-action.service";
import { ScriptLoader } from "./script-loader.service";
import { Content } from "sharedModules/services/provider/content.service";
import { SettingsLearningAndFeedbackService } from "settingsServices/apis/settings-learning-and-feedback-api.service";

// Interfaces
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import { IUserlaneConfiguration } from "interfaces/configuration.interface";
import { IStore, IStoreState } from "interfaces/redux.interface";
import {
    IUserlaneWindow,
    IUserlaneParams,
} from "../../interfaces/userlane.interface";
import { ILearningAndFeedbackSetting } from "interfaces/settings-learning-and-feedback-tool.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";
import { IUser } from "interfaces/user.interface";

// Constants
import {
    UserlaneActions,
    PermissionToModuleNameMap,
} from "../../constants/userlane.constants";

@Injectable()
export class Userlane {
    isActive = false;
    version = VERSION;

    private config: IUserlaneConfiguration;
    private _window = window as IUserlaneWindow;
    private widgetType = this.configurationAction.types.Userlane;
    private destroy$ = new Subject();
    private currentUser: IUser;

    constructor(
        @Inject(DOCUMENT) private _document: Document,
        @Inject(StoreToken) private store: IStore,
        @Inject("$rootScope") private $rootScope: IExtendedRootScopeService,
        private permissions: NgxPermissionsService,
        private configurationAction: ConfigurationAction,
        private scriptLoader: ScriptLoader,
        private content: Content,
        private settingsLearningAndFeedbackService: SettingsLearningAndFeedbackService,
    ) {}

    initialize() {
        observableFromStore(this.store).pipe(
            startWith(this.store.getState()),
            takeUntil(this.destroy$),
        ).subscribe((state: IStoreState) => this.serviceWillReceiveState(state));

        return this.getConfigurations().then(() => {
            if (!this.config || !this.config.token) {
                return;
            }
            if (this._window.Userlane) {
                this.isActive = true;
                this.applyStyles();
                this.setCustomData();
                this.invoke(UserlaneActions.lang, this.$rootScope.tenantLanguage);
                this.setTags();
                this.validateAdminSettingAndRun();
            }
        });
    }

    validateAdminSettingAndRun() {
        this.settingsLearningAndFeedbackService.getLearningSettings().then(
            (response: IProtocolResponse<ILearningAndFeedbackSetting>) => {
                if (response.result) {
                    const settings = response.data;
                    const userlaneEnabled = settings.enabled;
                    if (userlaneEnabled && this.permissions.getPermission("UserGuidance")) {
                        this.invoke(UserlaneActions.init, this.config.token);
                    }
                }
            });
    }

    invoke(...params: IUserlaneParams) {
        if (this._window.Userlane) {
            this._window.Userlane(...params);
        }
    }

    serviceWillReceiveState(state: IStoreState) {
        this.config = getConfigurationByType(this.widgetType)(state) as IUserlaneConfiguration;
        const token = this.config ? this.config.token : "";
        if (!token) return;
        this.currentUser = getCurrentUser(state);
    }

    logout() {
        this.destroy$.next();
        this.destroy$.complete();
        this.isActive = false;

        for (const key in this._window) {
            if (this._window.hasOwnProperty(key) && key.toLowerCase().startsWith("userlane")) {
                this._window[key] = undefined;
            }
        }

        const selectors = [
            "head > *[href*='userlane']",
            "head > *[id*='userlane']",
            "head > *[id*='Userlane']",
            "body > *[id*='userlane']",
        ];

        const userlaneElements: HTMLElement[] = Array.prototype.slice.call(document.querySelectorAll(selectors.join(", ")));
        userlaneElements.forEach((el: HTMLElement) => {
            el.parentElement.removeChild(el);
        });

    }

    private getConfigurations(): Promise<{}> {
        const prm = this.configurationAction.getConfigurationByType(this.widgetType, true) as Promise<{}>;
        return prm.then((config: IUserlaneConfiguration) => {
            if (!config.token) return null;
            const configType = this.configurationAction.config[this.widgetType];
            const src = configType.getSrc ? configType.getSrc(config) : configType.src;
            return this.scriptLoader.load(configType.id, src);
        });
    }

    private getUserlaneCustomizations() {
        const overrides = JSON.parse(this.content.GetKey("UserlaneSettings")) || [];
        return [...overrides];
    }

    private setCustomData() {
        const currentLicenseType = this.getLicenseType();
        this.invoke(UserlaneActions.identify, this.currentUser.Id, {
            email: this.currentUser.EmailHash,
            release: this.version,
            role: this.currentUser.RoleName,
            tenantId: this.currentUser.CurrentTenant,
            licenseType: currentLicenseType,
            primaryUrl: window.location.hostname,
            userlaneDevelopment: this.content.GetKey("UserLaneDevelopment") ? this.content.GetKey("UserLaneDevelopment") : "No",
            showOnlyInEU: Boolean(this.permissions.getPermission("CookieShowOnlyInEU")),
        });
    }

    private setTags() {
        const tags = [];
        for (const permission in PermissionToModuleNameMap) {
            if (this.permissions.getPermission(permission)) {
                tags.push(PermissionToModuleNameMap[permission]);
            }
        }
        this.invoke(UserlaneActions.tag, ...tags, ...this.getUserlaneCustomizations());
    }

    private getLicenseType() {
        return this.currentUser.Tenants.find((tenant) => tenant.Id === this.currentUser.CurrentTenant).LicenseType;
    }

    private applyStyles() {
        const styles: HTMLStyleElement = this._document.createElement("style");
        styles.type = "text/css";
        styles.id = "userlane-custom-styles";
        styles.textContent = `
            .userlane-assistant-branding {
                display: none;
            }
        `;
        this._document.body.appendChild(styles);
    }
}
