// NOTE: Implenentation based on https://www.bennadel.com/blog/3408-creating-a-dynamic-favicon-service-in-angular-5-2-4.htm

import { Inject, InjectionToken } from "@angular/core";

import { IFaviconsConfig, IFavicon } from "../../interfaces/favicon.interface";

export const BROWSER_FAVICONS_CONFIG = new InjectionToken<IFaviconsConfig>("Favicons Configuration");

export enum IconSet {
    onetrust,
    cookiepro,
}

export const BROWSER_FAVICONS_CONFIG_VALUE = {
    icons: {
        [IconSet.onetrust]: getCommonIconSets("images/favicon.png", "image/png"),
        [IconSet.cookiepro]: getCommonIconSets("images/cookie_favicon.png", "image/png"),
    },
    cacheBusting: false,
};

export abstract class Favicons {
    abstract activate(iconSet: IconSet);
    abstract reset();
}

export class BrowserFavicons implements Favicons {
    private elementClass = "ot-favicon";
    private icons: Map<string, IFavicon[]>;
    private useCacheBusting: boolean;

    constructor(@Inject(BROWSER_FAVICONS_CONFIG) config: IFaviconsConfig) {
        this.icons = Object.assign(Object.create(null), config.icons);
        this.useCacheBusting = config.cacheBusting || false;
        this.removeExternalLinkElements();
    }

    public activate(iconSet: IconSet) {
        if (!this.icons[iconSet]) {
            throw new Error(`Favicons for [ ${iconSet} ] not found.`);
        }

        this.setNodes(this.icons[iconSet]);
    }

    public reset() {
        this.setNodes(this.icons[IconSet.onetrust]);
    }

    private addNode(icon: IFavicon) {
        const linkElement = document.createElement("link");
        linkElement.setAttribute("class", this.elementClass);
        linkElement.setAttribute("rel", icon.rel || "icon");
        linkElement.setAttribute("type", icon.type);
        linkElement.setAttribute("href", icon.href);
        document.head.appendChild(linkElement);
    }

    private cacheBustHref(href: string): string {
        const augmentedHref =
            href.indexOf("?") === -1
                ? `${href}?cache=${Date.now()}`
                : `${href}&cache=${Date.now()}`;

        return augmentedHref;
    }

    private removeExternalLinkElements() {
        const linkElements = document.querySelectorAll("link[ rel ~= 'icon']");

        for (const linkElement of Array.from(linkElements)) {
            linkElement.parentNode.removeChild(linkElement);
        }
    }

    private removeNode() {
        const linkElements = document.head.querySelectorAll("." + this.elementClass);

        if (linkElements.length) {
            for (let i = 0; i < linkElements.length; i++) {
                document.head.removeChild(linkElements.item(i));
            }
        }
    }

    private setNodes(icons: IFavicon[]) {
        this.removeNode();
        for (const i of icons) {
            this.setNode(i);
        }
    }

    private setNode(icon: IFavicon) {
        icon.href = this.useCacheBusting
            ? this.cacheBustHref(icon.href)
            : icon.href;

        this.addNode(icon);
    }
}

export function getCommonIconSets(href: string, type: string, touchHref = href, touchType = type): IFavicon[] {
    return [
        {
            href,
            rel: "shortcut icon mask-icon fluid-icon",
            type,
        },
        {
            href: touchHref,
            rel: "apple-touch-icon apple-touch-icon-precomposed",
            type: touchType,
        },
    ];
}
