// 3rd party
import { Injectable, Inject } from "@angular/core";

// Interfaces
import { IStringMap } from "interfaces/generic.interface";
import { IStaticContainer } from "interfaces/one-static-container.interface";
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";

// Constants
import { InventorySchemaIds } from "constants/inventory-config.constant";

@Injectable()
export class DefaultContentService {

    constructor(@Inject("$rootScope") public $rootScope: IExtendedRootScopeService) { }

    getDefaults(): IStringMap<any> {
        return {
            ProdDoc: "https://onetrust.com/product-documentation/user_guide.pdf",
            DataMappingDocumentationLink: "https://onetrust.com/product-documentation/user_guide.pdf",
            TermsOfService: "https://onetrust.com/",
            PrivacyPolicy: "https://www.onetrust.com/privacy-notice",
            SupportPortalLink: "https://support.onetrust.com",
            Forum: "https://onetrust.com",
            GetStartedGuide: "https://onetrust.com",
            GetStartedVideo: "",
            tutorials: "",
            TutorialQuestionnaire: "",
            TutorialProjects: "",
            TutorialDashboard: "",
            TutorialRisks: "",
            tutorialCookieScanning: "",
            tutorialDataMapping: "",
            tutorialReadiness: "",
            tutorialAssessment: "",
            tutorialSubjectRights: "",
            tutorialIncidentBreach: "",
            tutorialVendorRisk: "",
            TutorialUsers: "",
            TutorialOrganizations: "",
            TutorialSettings: "",
            TutorialSettingsProjects: "",
            TutorialSettingsAPI: "",
            TutorialSettingsSSO: "",
            TutorialSettingsBranding: "",
            TutorialSettingsTerminology: "",
            OnBoardingQuestionnaire: `
                <p><b>Questionnaire templates are the starting point for creating ${this.$rootScope.customTerms.Projects}.</b></p>
                <p>
                    In this view, users are able to:
                    <ul>
                        <li>See a list of all available templates, both published and unpublished.</li>
                        <li>Perform high-level template actions by clicking the gear icon on any template card.</li>
                        <li>Access an individual template by clicking on the template card. </li>
                        <li>Navigate to a list of archived templates by click the "View Archived" at the top right corner of the screen.</li>
                        <li>Add new templates. You can create one from scratch, use a template from our template gallery as a starting point, or request to import your templates that may already exist outside of our the OneTrust system.</li>
                    </ul>
                </p>
            `,
            OnBoardingProjects: `
                <p><b>${this.$rootScope.customTerms.Projects} are created by privacy officers and assigned to individuals in order to gather information and identify risks pertaining to a particular initiative.</b></p>
                <p>In this view, users are able to:
                    <ul>
                        <li>View a list of available ${this.$rootScope.customTerms.Projects} and their high-level details</li>
                        <li>Access an individual ${this.$rootScope.customTerms.Project} by clicking a row in the table</li>
                        <li>Add new ${this.$rootScope.customTerms.Projects}</li>
                        <li>Export a CSV of a filtered ${this.$rootScope.customTerms.Project} list</li>
                        <li>Export a PDF for individual ${this.$rootScope.customTerms.Project} details</li>
                        <li>View ${this.$rootScope.customTerms.Project} History</li>
                    </ul>
                </p>
            `,
            OnBoardingDashboard: `
                <p><b>The Dashboard gives users an overview of all of the data they've collected through ${this.$rootScope.customTerms.Projects}.</b></p>
                <p>In this view, users are able to:
                    <ul>
                        <li>View a breakdown of all ${this.$rootScope.customTerms.Project} states</li>
                        <li>Compare ${this.$rootScope.customTerms.Projects} by age</li>
                        <li>See an overview of total ${this.$rootScope.customTerms.Project} risk levels</li>
                    </ul>
                </p>
            `,
            OnBoardingRisks: `
                <p><b>The Risks view provides an overview of risks flagged across all ${this.$rootScope.customTerms.Projects}.</b></p>
                <p>In this view, users are able to:
                    <ul>
                        <li>View a list of all active risks and their details</li>
                        <li>Access individual risks at their origin by clicking their ${this.$rootScope.customTerms.Project} name</li>
                        <li>Export a CSV of the risk table, which can be modified, filtered and saved to the users preference</li>
                    </ul>
                </p>
            `,
            OnBoardingDataMapping: "",
            OnBoardingUsers: "",
            OnBoardingOrganizations: "",
            OnBoardingSettings: "",
            helpModalButtonTxt: ["Getting Started Guide", "Product Documentation", "Video Tutorials", "Contact Us"],
            WelcomeUpgradeContent: "<li class=\"welcome__upgrade-bullet\">On-Premise Deployment</li><li class=\"welcome__upgrade-bullet\">Advanced Assessment Automation Capabilities</li><li class=\"welcome__upgrade-bullet\">Data Mapping Automation</li><li class=\"welcome__upgrade-bullet\">Website Monitoring</li><li class=\"welcome__upgrade-bullet\">Privacy Shield Verification Seal</li><li class=\"welcome__upgrade-bullet\">APEC CBPR Certification </li>",
            UpgradeModalFeatureList: "<ul class=\"upgrade-modal__list\"><li class=\"upgrade-modal__list-item\">On-Premise Deployment</li><li class=\"upgrade-modal__list-item\">Advanced Assessment Automation Capabilities</li><li class=\"upgrade-modal__list-item\">Data Mapping Automation</li></ul><ul class=\"upgrade-modal__list\"><li class=\"upgrade-modal__list-item\">Website Monitoring</li><li class=\"upgrade-modal__list-item\">Privacy Shield Verification Seal</li><li class=\"upgrade-modal__list-item\">APEC CBPR Certification </li></ul>",
            DataMappingRestrictedFeature: "<div>Please contact OneTrust at <a href=\"mailto:info@onetrust.com\">info@onetrust.com</a> or call <a class=\"text-nowrap\" href=\"tel:1-844-900-0472\">1 844 900 0472</a> for additional information about this feature</div>",
            CookiesReportExplanation: "https://onetrust.com/product-documentation/cookies/OneTrust%20-%20Cookie%20Compliance%20Quick%20Start%20Guide.pdf",
            dataMappingWelcomeGettingStartedData: [
                {
                    index: 1,
                    content: {
                        titleKey: "ListYourOrganizationsAssets",
                        copyKey: "ListYourOrganizationsAssetsCopy",
                        links: [
                            {
                                titleKey: "WhatIsAnAsset",
                                items: [
                                    {
                                        textKey: "WhatIsAnAssetResponse",
                                        bullet: true,
                                    },
                                ],
                            },
                            {
                                titleKey: "HowDoIPopulateMyList",
                                items: [
                                    {
                                        textKey: "HostWorkshopsITOwners",
                                        bullet: true,
                                    },
                                    {
                                        textKey: "BulkImportListOfAssets",
                                        bullet: true,
                                    },
                                    {
                                        textKey: "UtilizeOurScanningToolToPopulate",
                                        bullet: true,
                                    },
                                    {
                                        textKey: "DiscoverYourAssets",
                                        bullet: true,
                                    },
                                ],
                            },
                        ],
                    },
                    action: {
                        textKey: "GoToAssets",
                        route: "zen.app.pia.module.datamap.views.inventory.list",
                        routeParams: { Id: InventorySchemaIds.Assets , page: null, size: null },
                        type: "primary",
                        identifier: "DMWelcomeGettingStartedGoToAssetBtn",
                    },
                } as IStaticContainer,
                {
                    index: 2,
                    content: {
                        titleKey: "ConfigureYourAssetAttributes",
                        copyKey: "ConfigureYourAssetAttributesCopy",
                    },
                    action: {
                        textKey: "GoToAssetTemplate",
                        route: "zen.app.pia.module.datamap.views.template-redirect",
                        routeParams: {templateType: 20},
                        type: "primary",
                        identifier: "DMWelcomeGettingStartedGoToAssetTemplateBtn",
                    },
                    negativePermissionKeys: ["InventoryViewAttributes", "DMAssessmentV2"],
                } as IStaticContainer,
                {
                    index: 2,
                    content: {
                        titleKey: "ConfigureYourAssetAttributes",
                        copyKey: "ConfigureYourAssetAttributesManagerCopy",
                    },
                    action: {
                        textKey: "GoToAttributeManager",
                        route: "zen.app.pia.module.datamap.views.attribute-manager.list",
                        routeParams: {id: "assets"},
                        type: "primary",
                        identifier: "DMWelcomeGettingStartedGoToAssetAttributeManagerBtn",
                    },
                    permissionKeys: ["InventoryViewAttributes"],
                } as IStaticContainer,
                {
                    index: 3,
                    content: {
                        titleKey: "ListYourOrganizationsPAs",
                        copyKey: "ListYourOrganizationsPAsCopy",
                        links: [
                            {
                                titleKey: "WhatIsAPA",
                                items: [
                                    {
                                        textKey: "WhatIsAPAResponse",
                                        bullet: true,
                                    },
                                ],
                            },
                            {
                                titleKey: "HowDoIPopulateMyList",
                                items: [
                                    {
                                        textKey: "HostWorkshopsBizOwners",
                                        bullet: true,
                                    },
                                    {
                                        textKey: "BulkImportListOfPAs",
                                        bullet: true,
                                    },
                                    {
                                        textKey: "DiscoverYourPAs",
                                        bullet: true,
                                    },
                                ],
                            },
                        ],
                    },
                    action: {
                        textKey: "GoToPAs",
                        route: "zen.app.pia.module.datamap.views.inventory.list",
                        routeParams: { Id: InventorySchemaIds.Processes, page: null, size: null},
                        type: "primary",
                        identifier: "DMWelcomeGettingStartedGoToPABtn",
                    },
                } as IStaticContainer,
                {
                    index: 4,
                    content: {
                        titleKey: "ConfigureYourPAAttributes",
                        copyKey: "ConfigureYourPAAttributesCopy",
                    },
                    action: {
                        textKey: "GoToPATemplate",
                        route: "zen.app.pia.module.datamap.views.template-redirect",
                        routeParams: {templateType: 30},
                        type: "primary",
                        identifier: "DMWelcomeGettingStartedGoToPATemplateBtn",
                    },
                    negativePermissionKeys: ["InventoryViewAttributes", "DMAssessmentV2"],
                } as IStaticContainer,
                {
                    index: 4,
                    content: {
                        titleKey: "ConfigureYourPAAttributes",
                        copyKey: "ConfigureYourPAAttributesManagerCopy",
                    },
                    action: {
                        textKey: "GoToAttributeManager",
                        route: "zen.app.pia.module.datamap.views.attribute-manager.list",
                        routeParams: {id: "processing-activities"},
                        type: "primary",
                        identifier: "DMWelcomeGettingStartedGoToPAAttributeManagerBtn",
                    },
                    permissionKeys: ["InventoryViewAttributes"],
                } as IStaticContainer,
                {
                    index: 5,
                    content: {
                        titleKey: "ConfigureYourTemplateAttributes",
                        copyKey: "ConfigureYourTemplateAttributesCopy",
                    },
                    action: {
                        textKey: "GoToTemplates",
                        route: "zen.app.pia.module.templatesv2.list",
                        type: "primary",
                        identifier: "DMWelcomeGettingStartedGoToTemplateBtn",
                    },
                    permissionKeys: ["DMAssessmentV2", "TemplatesView"],
                } as IStaticContainer,
                {
                    index: 5,
                    content: {
                        titleKey: "ConfigureYourTemplateAttributes",
                        copyKey: "ConfigureYourTemplateAttributesCopy",
                    },
                    permissionKeys: ["DMAssessmentV2"],
                    negativePermissionKeys: ["TemplatesView"],
                } as IStaticContainer,
                {
                    index: 6,
                    content: {
                        titleKey: "LaunchAssessments",
                        copyKey: "LaunchAssessmentsCopy",
                    },
                    permissionKeys: ["DMAssessmentV2"],
                } as IStaticContainer,
                {
                    index: 5,
                    content: {
                        titleKey: "LaunchAssessments",
                        copyKey: "LaunchAssessmentsCopy",
                    },
                    negativePermissionKeys: ["DMAssessmentV2"],
                } as IStaticContainer,
                {
                    index: 6,
                    content: {
                        titleKey: "MonitorAndTrackProgress",
                        copyKey: "MonitorAndTrackProgressCopy",
                    },
                    action: {
                        textKey: "GoToAssessments",
                        route: "zen.app.pia.module.datamap.views.assessment.list",
                        routeParams: {size: null, page: null},
                        type: "primary",
                        identifier: "DMWelcomeGettingStartedGoToAssessmentsBtn",
                    },
                    negativePermissionKeys: ["DMAssessmentV2"],
                } as IStaticContainer,
                {
                    index: 7,
                    content: {
                        titleKey: "MonitorAndTrackProgress",
                        copyKey: "MonitorAndTrackProgressCopy",
                    },
                    action: {
                        textKey: "GoToAssessments",
                        route: "zen.app.pia.module.assessment.list",
                        type: "primary",
                        identifier: "DMWelcomeGettingStartedGoToAssessmentsV2Btn",
                    },
                    permissionKeys: ["DMAssessmentV2"],
                } as IStaticContainer,
                {
                    index: 7,
                    content: {
                        titleKey: "VisualizeAndReport",
                        copyKey: "VisualizeAndReportCopy",
                    },
                    negativePermissionKeys: ["DMAssessmentV2"],
                } as IStaticContainer,
                {
                    index: 8,
                    content: {
                        titleKey: "VisualizeAndReport",
                        copyKey: "VisualizeAndReportCopy",
                    },
                    permissionKeys: ["DMAssessmentV2"],
                } as IStaticContainer,
            ],
        };
    }
}
