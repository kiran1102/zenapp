// Angular
import { Injectable } from "@angular/core";

// Services
import { ToastService } from "@onetrust/vitreus";
import {
    ToastType,
    ToastIconColor,
} from "@onetrust/vitreus/src/app/vitreus/services/toast/toast-config";

@Injectable()
export class NotificationService {
    constructor(private toastService: ToastService) {}

    alert(alertTitle: string, alertMessage: string): void {
        this.toastService.info(alertTitle, alertMessage);
    }
    alertSuccess(alertTitle: string, alertMessage: string): void {
        this.toastService.success(alertTitle, alertMessage);
    }
    alertError(alertTitle: string, alertMessage: string): void {
        this.toastService.error(alertTitle, alertMessage);
    }
    alertWarning(alertTitle: string, alertMessage: string): void {
        this.toastService.warning(alertTitle, alertMessage);
    }
    alertWait(alertTitle: string, alertMessage: string): void {
        this.toastService.info(alertTitle, alertMessage);
    }
    alertNote(alertTitle: string, alertMessage: string): void {
        this.toastService.info(alertTitle, alertMessage);
    }
    alertCustomizedNote(alertTitle: string, alertMessage: string , toastType: ToastType, timeout?: number, icon?: string, iconColor?: ToastIconColor, iconBackgroundColor?: ToastIconColor): void {
        this.toastService.show(alertTitle, alertMessage, toastType, icon, iconColor, iconBackgroundColor, timeout);
    }
}
