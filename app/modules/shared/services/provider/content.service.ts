// 3rd Party
import { Injectable } from "@angular/core";

// Services
import { ProtocolService } from "modules/core/services/protocol.service";
import { DefaultContentService } from "sharedModules/services/provider/default-content.service";
import { MetaService } from "sharedModules/services/provider/meta.service";

// Interfaces
import { IStringMap } from "interfaces/generic.interface";
import { IProtocolMessages, IProtocolConfig, IProtocolResponse } from "interfaces/protocol.interface";

@Injectable()
export class Content {

    private defaultContent: IStringMap<string>;
    private content: IStringMap<string>;

    constructor(
        private oneProtocol: ProtocolService,
        private defaultContentService: DefaultContentService,
        private metaService: MetaService,
    ) { }

    GetContent(): ng.IPromise<IStringMap<string>> {
        if (!this.defaultContent) {
            this.defaultContent = this.defaultContentService.getDefaults();
            this.content = this.defaultContent;
        }
        const config: IProtocolConfig = this.oneProtocol.config("GET", "/content/list");
        const messages: IProtocolMessages = { Error: { Hide: true } };
        return this.oneProtocol.http(config, messages).then((response: IProtocolResponse<IStringMap<string>>): IStringMap<string> => {
            if (response.result) {
                this.metaService.applyMetaTags(response.data);
                this.content = {
                    ...this.defaultContent,
                    copyright: this.metaService.copyright,
                    privacyPolicy: this.metaService.privacyPolicy,
                    ...response.data,
                };
            }
            return this.content;
        });
    }

    GetKey(key: string): string {
        return this.content[key] || null;
    }

}
