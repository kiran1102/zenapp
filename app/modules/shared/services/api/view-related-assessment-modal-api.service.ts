// Angular
import { Injectable } from "@angular/core";

// Interfaces
import {
    IProtocolConfig,
    IProtocolResponse,
    IProtocolMessages,
} from "interfaces/protocol.interface";
import { IViewRelatedAssessmentModalData } from "sharedModules/interfaces/view-related-assessment-modal.interface";

// Services
import { ProtocolService } from "modules/core/services/protocol.service";
import { TranslatePipe } from "pipes/translate.pipe";

@Injectable()
export class ViewRelatedAssessmentModalApiService {
    constructor(
        private oneProtocol: ProtocolService,
        private translatePipe: TranslatePipe,
    ) { }

    getRelatedAssessments(assessmentId: string): ng.IPromise<IProtocolResponse<IViewRelatedAssessmentModalData>> {
        const config: IProtocolConfig = this.oneProtocol.customConfig("GET", `/assessment-v2/v2/assessments/${assessmentId}/related`);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("CouldNotRetrieveRelatedAssessments") } };
        return this.oneProtocol.http(config, messages);
    }
}
