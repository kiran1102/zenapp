// 3rd Party
import { Injectable } from "@angular/core";

// Services
import { ProtocolService } from "modules/core/services/protocol.service";

// Interfaces
import { IProtocolMessages, IProtocolResponse } from "interfaces/protocol.interface";
import { IAccessLevel } from "interfaces/user-access-level.interface";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

@Injectable()
export class AccessLevelsService {

    constructor(
        private translatePipe: TranslatePipe,
        private OneProtocol: ProtocolService,
    ) { }

    getCurrentAccess(): ng.IPromise<IProtocolResponse<IAccessLevel[]>> {
        const config = this.OneProtocol.customConfig("GET", `/access/v1/access-levels/current`);
        const messages: IProtocolMessages = {
            Error: { custom: this.translatePipe.transform("ErrorRetrievingRoles") },
        };
        return this.OneProtocol.http(config, messages);
    }

}
