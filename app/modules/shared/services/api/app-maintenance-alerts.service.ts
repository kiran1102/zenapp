// 3rd Party
import { Injectable } from "@angular/core";

// Services
import { ProtocolService } from "modules/core/services/protocol.service";

// Interfaces
import {
    IProtocolConfig,
    IProtocolMessages,
    IProtocolResponse,
} from "interfaces/protocol.interface";
import { IBannerAlertData } from "interfaces/app-maintenance-alerts.interface";

@Injectable()
export class AppMaintenanceAlertsService {

    constructor(
        private OneProtocol: ProtocolService,
    ) { }

    getAlertNotifications(): ng.IPromise<IProtocolResponse<IBannerAlertData>> {
        const config: IProtocolConfig = this.OneProtocol.config("GET", "/api/alert/v1/alerts/maintenance/immediate");
        const messages: IProtocolMessages = { Error: { Hide: true } };
        return this.OneProtocol.http(config, messages);
    }

    updateAlertNotifications(alertId: string): ng.IPromise<IProtocolResponse<null>> {
        const config: IProtocolConfig = this.OneProtocol.config("POST", `/api/alert/v1/schedules/${alertId}/acknowledge`);
        const messages: IProtocolMessages = { Error: { Hide: true } };
        return this.OneProtocol.http(config, messages);
    }

}
