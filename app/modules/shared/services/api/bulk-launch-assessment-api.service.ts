// Angular
import { Injectable } from "@angular/core";

// Interfaces
import {
    IProtocolConfig,
    IProtocolResponse,
    IProtocolMessages,
} from "interfaces/protocol.interface";

// Services
import { ProtocolService } from "modules/core/services/protocol.service";
import { TranslatePipe } from "pipes/translate.pipe";

// Rxjs
import {
    Observable,
    from,
} from "rxjs";

@Injectable()
export class AssessmentBulkLaunchApiService {

    constructor(
        private oneProtocol: ProtocolService,
        private translatePipe: TranslatePipe,
    ) { }

    getDuplicateAssessmentWarnings(params): Observable<IProtocolResponse<string[]>> {
        const config: IProtocolConfig = this.oneProtocol.customConfig("POST", "/assessment-v2/v2/assessments/inventory/duplicate-assessment-validation", [], params);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("CouldNotRetrieveDuplicateWarnings") } };
        return from(this.oneProtocol.http(config, messages));
    }
}
