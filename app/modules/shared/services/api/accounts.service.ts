// Services
import { ProtocolService } from "modules/core/services/protocol.service";
import { Principal } from "modules/shared/services/helper/principal.service";
import { UserActionService } from "oneServices/actions/user-action.service";
import { NotificationService } from "modules/shared/services/provider/notification.service";

// Interfaces
import { IUser } from "interfaces/user.interface.ts";
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import { IProtocolResponse, IProtocolMessages } from "interfaces/protocol.interface";
import {
    ILoginRequest,
    IToken,
    ITokenError,
    ITokenIdentity,
    ITokenPasswordRequest,
    ITokenRefreshRequest,
} from "interfaces/token.interface";

export class Accounts {
    static $inject: string[] = [
        "$rootScope",
        "$http",
        "$q",
        "OneProtocol",
        "PRINCIPAL",
        "$localStorage",
        "Session",
        "UserActionService",
        "NotificationService",
    ];

    constructor(
        private $rootScope: IExtendedRootScopeService,
        private $http: ng.IHttpService,
        private $q: ng.IQService,
        private OneProtocol: ProtocolService,
        private PRINCIPAL: Principal,
        private $localStorage,
        private Session,
        private userActionService: UserActionService,
        private notificationService: NotificationService,
    ) {}

    login(data: ILoginRequest): ng.IPromise<IProtocolResponse<ITokenIdentity|ITokenError>> {
        const deferred: ng.IDeferred<IProtocolResponse<ITokenIdentity|ITokenError>> = this.$q.defer();
        const loginRequest: ITokenPasswordRequest = {
            client_id: "onetrust",
            grant_type: "password",
            username: data.Email,
            password: data.Password,
            tenant_id: this.$localStorage.ZenTenant || "",
        };
        this.$http.post("/token", loginRequest).then(
            (tokenResponse: ng.IHttpResponse<IToken>) => {
                const identity: ITokenIdentity = tokenResponse.data;
                if (identity.expiredPassword.toLowerCase() === "true") {
                    this.$rootScope.$broadcast("event:auth-expiredPassword", identity.refresh_token);
                    deferred.resolve({
                        result: false,
                        data: { expired: true } as ITokenError,
                    });
                } else {
                    this.$rootScope.origin = this.Session.getReturnUrl();
                    this.PRINCIPAL.authenticate(identity);
                    deferred.resolve({
                        result: true,
                        data: identity,
                    });
                }
                return deferred.promise;
            },
            (response: ng.IHttpResponse<ITokenError>) => {
                if (response.status === 400) {
                    if (response.data.error === "disabled") {
                        response.data.disabled = true;
                        this.notificationService.alertError(
                            this.$rootScope.t("Error"),
                            response.data.error_description,
                        );
                    } else {
                        response.data.invalidPassword = true;
                    }
                } else {
                    this.notificationService.alertError(
                        this.$rootScope.t("Error"),
                        this.$rootScope.t("SorryProblemLoggingIn"),
                    );
                }
                deferred.resolve({
                    result: false,
                    data: response.data,
                });
            },
        );
        return deferred.promise;
    }

    refresh(tenantId: string, orgGroupId?: string, roleId?: string): ng.IPromise<IProtocolResponse<ITokenIdentity>> {
        const deferred: ng.IDeferred<IProtocolResponse<ITokenIdentity>> = this.$q.defer();
        const tokenString = localStorage.getItem("OneTrust.ZenAuth");
        const originalIdentity = tokenString ? JSON.parse(tokenString) : null;

        if (originalIdentity && originalIdentity.refresh_token) {
            const tenant_id = tenantId || originalIdentity.tenant_id;
            const isSameTenant = tenant_id === originalIdentity.tenant_id;
            const orggroup_id = orgGroupId || (isSameTenant ? originalIdentity.orggroup_id : "");
            const role_id = roleId || (isSameTenant ? originalIdentity.role_id : "");

            const refreshRequest: ITokenRefreshRequest = {
                grant_type: "refresh_token",
                client_id: "onetrust",
                refresh_token: originalIdentity.refresh_token,
                tenant_id,
                orggroup_id,
                organization_id: orggroup_id,
                role_id,
            };
            this.executeRefresh(refreshRequest).then((response: IProtocolResponse<ITokenIdentity>) => {
                deferred.resolve(response);
            });
        } else {
            deferred.resolve({
                result: false,
                data: null,
            });
        }
        return deferred.promise;
    }

    basicRefresh(auth_code: string, fetchUser: boolean = false): ng.IPromise<IProtocolResponse<ITokenIdentity>> {
        const refreshRequest: ITokenRefreshRequest = {
            grant_type: "refresh_token",
            client_id: "onetrust",
            refresh_token: auth_code,
        };
        return this.executeRefresh(refreshRequest, fetchUser);
    }

    confirm(data): ng.IPromise<IProtocolResponse<any>> {
        const config = this.OneProtocol.config("POST", "/account/confirm", null, data);
        const messages: IProtocolMessages = {
            Error: { custom: this.$rootScope.t("SorryProblemConfirmingAccount") },
        };
        return this.OneProtocol.http(config, messages);
    }

    register(data): ng.IPromise<IProtocolResponse<any>> {
        const config = this.OneProtocol.config("POST", "/account/register", null, data);
        const messages: IProtocolMessages = {
            Success: { custom: this.$rootScope.t("YourAccountHasBeenRegistered") },
            Error: { custom: this.$rootScope.t("SorryProblemRegisteringAccount") },
        };
        return this.OneProtocol.http(config, messages);
    }

    resetPassword(data): ng.IPromise<IProtocolResponse<any>> {
        const config = this.OneProtocol.config("POST", "/account/resetpassword", null, data);
        const messages: IProtocolMessages = {
            Success: { custom: this.$rootScope.t("YourPasswordHasBeenReset") },
            Error: { custom: this.$rootScope.t("SorryProblemResettingPassword") },
        };
        return this.OneProtocol.http(config, messages);
    }

    changeForgottenPassword(data): ng.IPromise<IProtocolResponse<any>> {
        const config = this.OneProtocol.config("PUT", "/account/changeforgottenpassword", null, data);
        const messages: IProtocolMessages = {
            Success: { custom: this.$rootScope.t("YourPasswordHasBeenChanged") },
            Error: { custom: this.$rootScope.t("SorryProblemChangingPassword") },
        };
        return this.OneProtocol.http(config, messages);
    }

    changePassword(data): ng.IPromise<IProtocolResponse<any>> {
        const config = this.OneProtocol.config("PUT", "/account/changepassword", null, data);
        const messages: IProtocolMessages = {
            Success: { custom: this.$rootScope.t("YourPasswordHasBeenChanged") },
            Error: { custom: this.$rootScope.t("SorryProblemChangingPassword") },
        };
        return this.OneProtocol.http(config, messages);
    }

    forgotPassword(data): ng.IPromise<IProtocolResponse<any>> {
        const config = this.OneProtocol.config("PUT", "/account/forgotpassword", null, data);
        const messages: IProtocolMessages = {
            Error: { Hide: true },
        };
        return this.OneProtocol.http(config, messages);
    }

    changeForgotPassword(data, id: string): ng.IPromise<IProtocolResponse<any>> {
        const config = this.OneProtocol.config("PUT", "/account/changeforgotpassword", { id }, data);
        const messages: IProtocolMessages = {
            Success: { custom: this.$rootScope.t("YourPasswordHasBeenReset") },
            Error: { custom: this.$rootScope.t("SorryProblemResettingPassword") },
        };
        return this.OneProtocol.http(config, messages);
    }

    hasTenant(): ng.IPromise<IProtocolResponse<any>> {
        const config = this.OneProtocol.config("GET", "/account/TenantDatabaseIsInitialized");
        const messages: IProtocolMessages = {
            Error: { custom: this.$rootScope.t("SorryCouldntGetTenant") },
        };
        return this.OneProtocol.http(config, messages);
    }

    private executeRefresh(refreshRequest: ITokenRefreshRequest, fetchUser: boolean = false): ng.IPromise<IProtocolResponse<ITokenIdentity>> {
        return this.$http.post("/token", refreshRequest).then(
            (refreshResponse: ng.IHttpResponse<IToken>) => {
                const identity: ITokenIdentity = refreshResponse.data;
                this.PRINCIPAL.authenticate(identity);
                if (fetchUser) {
                    return this.userActionService.fetchCurrentUser().then(
                        (userResponse: IProtocolResponse<IUser>) => {
                            if (userResponse.result && userResponse.data) {
                                identity.user = userResponse.data;
                                this.PRINCIPAL.setUser(userResponse.data);
                            }
                            return {
                                result: userResponse.result,
                                data: userResponse.result ? identity : null,
                            };
                        },
                    );
                } else {
                    return {
                        result: true,
                        data: identity,
                    };
                }
            },
            (response: ng.IHttpResponse<any>): IProtocolResponse<any> => {
                return {
                    result: false,
                    data: response.data,
                };
            },
        );
    }
}
