// Angular
import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";

// rxjs
import { Observable } from "rxjs";
import { tap } from "rxjs/operators";

// Services
import { AlertBannerService } from "@onetrust/vitreus";

// Interfaces
import { IBannerAlertData } from "interfaces/app-maintenance-alerts.interface";

@Injectable()
export class BannerAlertService {

    private persistDismissedAlerts = true;

    constructor(
        private http: HttpClient,
        private alertBannerService: AlertBannerService,
    ) {}

    getAlertBanners(): Observable<IBannerAlertData> {
        const currentLanguage = JSON.parse(localStorage.getItem("OneTrust.languageSelected")) || "en-us";
        const params = { languageCode: currentLanguage };
        return this.http.get<IBannerAlertData>("/api/alert/v1/alerts/banner/immediate", { params });
    }

    handleMaintenantsAlerts() {
        return this.getAlertBanners().pipe(
            tap((bannerMessageData: IBannerAlertData) => {
                this.closeAll();
                if (!bannerMessageData) return;
                const dismissedAlerts: string[] = JSON.parse(localStorage.getItem("OneTrust.Alerts")) || [];
                if (dismissedAlerts && dismissedAlerts.indexOf(bannerMessageData.alertScheduleId) > -1) return;
                if (bannerMessageData.displayAttributes.attributes) {
                    this.alertBannerService.updateConfig({
                        bannerIcon: bannerMessageData.displayAttributes.attributes.IconName,
                        bannerType: bannerMessageData.alertType,
                        callback: () => {
                            if (!this.persistDismissedAlerts) return;
                            dismissedAlerts.push(bannerMessageData.alertScheduleId);
                            localStorage.setItem("OneTrust.Alerts", JSON.stringify(dismissedAlerts));
                        },
                    });
                }
                this.alertBannerService.show(bannerMessageData.bannerMessage.message);
            }),
        );
    }

    closeAll() {
        this.persistDismissedAlerts = false;
        this.alertBannerService.closeAll();
        this.persistDismissedAlerts = true;
    }
}
