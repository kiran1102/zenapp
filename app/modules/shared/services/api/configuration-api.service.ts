// 3rd Party
import { Injectable } from "@angular/core";

// Services
import { ProtocolService } from "modules/core/services/protocol.service";

// Interfaces
import { IProtocolMessages, IProtocolResponse } from "interfaces/protocol.interface";
import { IStringMap } from "interfaces/generic.interface";

@Injectable()
export class ConfigurationApi {

    constructor(
        private OneProtocol: ProtocolService,
    ) { }

    getConfigurationByType(type: string): ng.IPromise<IProtocolResponse<IStringMap<string>>> {
        const config = this.OneProtocol.customConfig("GET", `/api/access/v1/${type}/configuration`);
        const messages: IProtocolMessages = { Error: { Hide: true } };
        return this.OneProtocol.http(config, messages);
    }

}
