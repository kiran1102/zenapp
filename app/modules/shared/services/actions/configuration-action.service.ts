// 3rd Party
import { Injectable, Inject } from "@angular/core";

// Services
import { ConfigurationApi } from "./../api/configuration-api.service";
import { Content } from "sharedModules/services/provider/content.service";

// Token / Interfaces
import { StoreToken } from "tokens/redux-store.token";
import { IStore } from "interfaces/redux.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";
import { IStringMap } from "interfaces/generic.interface";
import {
    IConfigurationSource,
    IHelpCenterConfiguration,
} from "interfaces/configuration.interface";

@Injectable()
export class ConfigurationAction {

    types = {
        HelpCenter: "help-center",
        Userlane: "userlane",
        Wootric: "wootric",
    };
    config: IStringMap<IConfigurationSource> = {
        [this.types.HelpCenter]: {
            id: "ze-snippet",
            getSrc: (config: IHelpCenterConfiguration) => `https://static.zdassets.com/ekr/snippet.js${config.widgetId ? "?key=" + config.widgetId : ""}`,
        },
        [this.types.Userlane]: {
            id: "Userlane",
            src: `https://cdn.userlane.com/userlane.js`,
        },
        [this.types.Wootric]: {
            id: "wootric-survey",
            src: `https://cdn.wootric.com/wootric-sdk.js`,
        },
    };

    constructor(
        @Inject(StoreToken) private store: IStore,
        private configurationApi: ConfigurationApi,
        private content: Content,
    ) {}

    getConfigurationByType(type: string, ignoreApi = false): ng.IPromise<IStringMap<any>> | Promise<IStringMap<any>> {
        if (ignoreApi) {
            return new Promise((resolve) => {
                resolve(this.handleConfig(type));
            });
        }
        return this.configurationApi.getConfigurationByType(type).then((response: IProtocolResponse<IStringMap<string|number>>) => {
            const config = response.result ? response.data : {};
            return this.handleConfig(type, config);
        });
    }

    private handleConfig(type: string, config: IStringMap<string|number> = {}) {
        if (type === this.types.Userlane) {
            config.token = this.content.GetKey("OneTrustUserLaneAccountNumber");
        }

        if (type === this.types.Wootric) {
            config.token = this.content.GetKey("OneTrustWootricAccountNumber");
        }

        const payload = { type, config };
        this.store.dispatch({ type: "SET_CONFIGURATION", payload });
        return config;
    }

}
