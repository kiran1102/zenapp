import {
    forEach,
    cloneDeep,
} from "lodash";

// Interfaces
import { IStore } from "interfaces/redux.interface";
import {
    IRecordAssignment,
} from "interfaces/inventory.interface";
import { IAssessmentCreationDetails } from "interfaces/assessment/assessment-creation-details.interface";

// Redux
import { getOrgById } from "oneRedux/reducers/org.reducer";
import { AssessmentDetailAction } from "modules/assessment/reducers/assessment-detail.reducer";
import { getCurrentInventoryOrgGroupId } from "oneRedux/reducers/inventory-record.reducer";

// Constants & Enums
import { InventoryTableIds } from "enums/inventory.enum";
import { InventoryTypeGetOrgById } from "constants/inventory.constant";

export class AssessmentBulkLaunchActionService {

    static $inject: string[] = ["store"];

    constructor(
        private readonly store: IStore,
    ) { }

    public setBulkLaunchAssessments(bulkAssessmentDetails: IAssessmentCreationDetails[]): void {
        this.store.dispatch({ type: AssessmentDetailAction.SET_BULK_LAUNCH_ASSESSMENTS, bulkAssessmentDetailsList: bulkAssessmentDetails });
    }

    public getAssessmentCreationItems(bulkInventoryList: IRecordAssignment[], inventoryId: InventoryTableIds): IAssessmentCreationDetails[] {
        const item: IAssessmentCreationDetails = {
            approverId: "",
            approverName: "",
            comments: "",
            deadline: "",
            description: "",
            name: "",
            orgGroupId: "",
            orgGroupName: "",
            reminder: null,
            isValidReminder: true,
            respondents: [],
            respondentModel: "",
            respondentInvitedUser: "",
            showDetails: false,
            templateId: "",
            templateName: "",
        };
        const assessmentCreationItems: IAssessmentCreationDetails[] = [];

        if (bulkInventoryList && bulkInventoryList.length) {
            forEach(bulkInventoryList, (record: IRecordAssignment): void => {
                const orgDetails = InventoryTypeGetOrgById.includes(inventoryId) && record.OrgGroupId ?
                        getOrgById(record.OrgGroupId)(this.store.getState()) : getOrgById(getCurrentInventoryOrgGroupId(this.store.getState()))(this.store.getState());
                const newCloneItem: IAssessmentCreationDetails = cloneDeep(item);

                if (inventoryId === InventoryTableIds.Incidents) {
                    newCloneItem.incidentDetails = {
                        incidentId: record.InventoryId,
                        incidentName: record.Name,
                    };
                } else {
                    newCloneItem.inventoryDetails = {
                        inventoryId: record.InventoryId,
                        inventoryTypeId: inventoryId,
                        inventoryName: record.Name,
                        inventoryNumber: record.inventoryNumber,
                    };
                }

                newCloneItem.respondentModel = record.defaultRespondent;
                newCloneItem.name = record.Name;
                newCloneItem.orgGroupId = orgDetails.Id;
                newCloneItem.orgGroupName = orgDetails.Name;
                assessmentCreationItems.push(newCloneItem);
            });
        }

        return assessmentCreationItems;
    }
}
