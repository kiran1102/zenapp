// Core
import { Injectable } from "@angular/core";

import * as _ from "lodash";

// Interfaces
import { IUser } from "interfaces/user.interface";
import { IToken, IAccessToken } from "interfaces/token.interface";

// Constants
import { UserRoles } from "constants/user-roles.constant";

@Injectable()
export class Principal {
    private _identity: IToken;
    private _authenticated = false;
    private _tenantId: string = null;
    private _orggroupId: string = null;
    private _rootOrgId: string = null;
    private _accessToken: IAccessToken = null;

    private _roleId: string = null;
    private _roleName: string = null;
    private _user: IUser = null;

    isIdentityResolved(): boolean {
        return Boolean(this._identity);
    }

    isAuthenticated(): boolean {
        this._authenticated = false;
        if (
            this._identity &&
            this._identity.refresh_token &&
            this._identity.access_token
        ) {
            const token = this.getDecodedToken(true);
            this._roleName = token.role || null;
            this._authenticated =
                token &&
                this._roleName &&
                this._roleName !== UserRoles.Register;
        }
        return this._authenticated;
    }

    authenticate(identity: IToken) {
        this._identity = identity;
        this._authenticated = this.isAuthenticated();
        if (identity) {
            this.setIdentityToStorage(identity, localStorage);
        } else {
            localStorage.removeItem("OneTrust.ZenAuth");
        }
        if (this._authenticated) {
            this._tenantId = identity.tenant_id || null;
            if (this._tenantId) {
                localStorage.setItem(
                    "OneTrust.ZenTenant",
                    JSON.stringify(this._tenantId),
                );
            }
            this._rootOrgId = this.getRootOrg(true);
            this._orggroupId = identity.orggroup_id || null;
            this._roleId = identity.role_id || null;
        }
    }

    identity(force?: boolean): IToken {
        if (force) this._identity = undefined;

        if (_.isUndefined(this._identity)) {
            // Get Identity from local instead
            this._identity = JSON.parse(
                localStorage.getItem("OneTrust.ZenAuth"),
            );
            this.authenticate(this._identity);
        }

        return this._identity;
    }

    getIdentity(): IToken {
        return this._identity ? this._identity : null;
    }

    setUser(user: IUser) {
        this._user = user;
    }

    getUser(): IUser {
        return this._user || null;
    }

    getCurrentUserId(): string {
        const token = this.getDecodedToken();
        return token ? token.guid : "";
    }

    getTenant(): string {
        return this._tenantId || "";
    }

    getOrggroup(): string {
        return this._orggroupId || "";
    }

    getRoleName(): string {
        return this._roleName || "";
    }

    getRole(): string {
        return this._roleId || "";
    }

    getRootOrg(force?: boolean): string {
        if (!force && this._rootOrgId) return this._rootOrgId;
        const token = this.getDecodedToken();
        if (token && token.root) {
            this._rootOrgId = token.root;
        }
        return this._rootOrgId;
    }

    getDecodedToken(force?: boolean): IAccessToken {
        if (!this._identity) return null;
        else if (!force && this._accessToken) return this._accessToken;
        const token = this._identity.access_token;
        const base64Url = token.split(".")[1];
        const base64 = base64Url.replace("-", "+").replace("_", "/");
        this._accessToken = JSON.parse(window.atob(base64));
        return this._accessToken;
    }

    logOff() {
        this.clearSessionStorage();
        this._identity = null;
        this._authenticated = null;
        this._accessToken = null;
        this._tenantId = null;
        this._orggroupId = null;
        this._roleId = null;
        this._roleName = null;
    }

    private setIdentityToStorage(identity: IToken, storage: Storage = localStorage) {
        storage.setItem("OneTrust.ZenAuth", JSON.stringify(identity));
        storage.setItem("refresh_token", identity.refresh_token);
        storage.setItem("access_token", identity.access_token);
        storage.setItem("access_token_stored_at", this.convertTimeForOidc(identity[".issued"]));
        storage.setItem("expires_at", this.convertTimeForOidc(identity[".expires"]));
    }

    private clearSessionStorage(storage: Storage = localStorage) {
        sessionStorage.clear();
        storage.removeItem("OneTrust.ZenAuth");
        storage.removeItem("OneTrust.AutoRefreshId");
        storage.removeItem("refresh_token");
        storage.removeItem("access_token");
        storage.removeItem("access_token_stored_at");
        storage.removeItem("expires_at");
    }

    private convertTimeForOidc(dateStr: string) {
        return JSON.stringify(Date.parse(dateStr));
    }
}
