// Angular
import { Injectable } from "@angular/core";

// Enums and Constants
import { Color } from "constants/color.constant";
import { RiskV2Levels } from "enums/riskV2.enum";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";
import { ILabelValue } from "interfaces/generic.interface";

const RISK_LABELS = {
    LOW: "Low",
    MEDIUM: "Medium",
    HIGH: "High",
    VERY_HIGH: "VeryHigh",
};

@Injectable()
export class RiskHelperService {

    constructor(
        private readonly translatePipe: TranslatePipe,
    ) { }

    getScore(impactLevel: number, probabilityLevel: number): number {
        return impactLevel + probabilityLevel;
    }

    getRiskColor(score: number): string {
        if (score <= 2) {
            return Color.RiskLow;
        } else if (score <= 4) {
            return Color.RiskMedium;
        } else if (score <= 6) {
            return Color.RiskHigh;
        } else if (score <= 8) {
            return Color.RiskVeryHigh;
        }
    }

    getRiskLevel(score: number): number {
        if (score <= 2) {
            return RiskV2Levels.LOW;
        } else if (score <= 4) {
            return RiskV2Levels.MEDIUM;
        } else if (score <= 6) {
            return RiskV2Levels.HIGH;
        } else if (score <= 8) {
            return RiskV2Levels.VERY_HIGH;
        }
    }

    getRiskColorByLevel(riskLevel: RiskV2Levels): string {
        let color: string;
        switch (riskLevel) {
            case RiskV2Levels.LOW:
                color = Color.RiskLow;
                break;
            case RiskV2Levels.MEDIUM:
                color = Color.RiskMedium;
                break;
            case RiskV2Levels.HIGH:
                color = Color.RiskHigh;
                break;
            case RiskV2Levels.VERY_HIGH:
                color = Color.RiskVeryHigh;
                break;
        }
        return color;
    }

    getLabelValue(label: string): string {
        const translatedValue = this.translatePipe.transform(label);
        return translatedValue.indexOf("NOT FOUND") > -1 ? label : translatedValue;
    }

    getRiskLevelLabel(riskLevel: RiskV2Levels): string {
        let label: string;
        switch (riskLevel) {
            case RiskV2Levels.LOW:
                label = RISK_LABELS.LOW;
                break;
            case RiskV2Levels.MEDIUM:
                label = RISK_LABELS.MEDIUM;
                break;
            case RiskV2Levels.HIGH:
                label = RISK_LABELS.HIGH;
                break;
            case RiskV2Levels.VERY_HIGH:
                label = RISK_LABELS.VERY_HIGH;
                break;
        }
        return label;
    }
}
