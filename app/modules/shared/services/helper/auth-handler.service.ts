// Angular
import { Injectable, Inject } from "@angular/core";
import { StateService } from "@uirouter/core";

// 3rd Party
import { OAuthEvent } from "angular-oauth2-oidc";

// rxjs
import { Subscription } from "rxjs";

// Common
import { OAuthenticationService } from "@onetrust/common";

// Vitreus
import { OtModalService } from "@onetrust/vitreus";

// Services
import { Principal } from "./principal.service";
import { OmniService } from "modules/core/services/omni.service";
import { LoadingService } from "modules/core/services/loading.service";
import { BannerAlertService } from "modules/shared/services/api/banner-alert.service";
import { UserApiService } from "oneServices/api/user-api.service";
import { ModalService } from "sharedServices/modal.service";

// Interfaces
import { INodeProcess } from "modules/core/interfaces/process.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";
import { IUserType } from "interfaces/user.interface";
import { ISamlSettings } from "interfaces/setting.interface";

declare const process: INodeProcess;

@Injectable()
export class AuthHandlerService {

    private refreshSub: Subscription;

    constructor(
        private stateService: StateService,
        private omniService: OmniService,
        private loadingService: LoadingService,
        private principal: Principal,
        private userApi: UserApiService,
        private oauthService: OAuthenticationService,
        private bannerAlertService: BannerAlertService,
        private modalService: ModalService,
        private otModalService: OtModalService,
        @Inject("$rootScope") private $rootScope,
        @Inject("authService") private authService,
    ) {}

    handleAuthGuard(): boolean {
        this.principal.identity(true);
        if (this.principal.isAuthenticated()) return true;
        this.loadingService.set(true);
        setTimeout(() => {
            this.logout();
        });
        return false;
    }

    refresh() {
        if (this.refreshSub && !this.refreshSub.closed) return;
        this.refreshSub = this.oauthService.oauthService.events.subscribe((event: OAuthEvent) => {
            switch (event.type) {
                case "token_refreshed":
                    this.refreshSub.unsubscribe();
                    // Wait for the event queue to empty and for the token to be persisted.
                    // Then inform the legacy API that the token has been refreshed.
                    setTimeout(() => {
                        this.persistTokenPostRefresh();
                    });
                    break;
                case "token_refresh_error":
                case "logout":
                    this.refreshSub.unsubscribe();
                    this.logout();
                    break;
                default:
                    break;
            }
        });
    }

    closeAll() {
        this.modalService.closeModal();
        this.otModalService.destroyAll();
        this.omniService.reset();
        this.bannerAlertService.closeAll();
    }

    logout() {
        const token = this.principal.getDecodedToken();
        const email = token ? token.email : "";
        const userTypeRequest: IUserType = { Email: email };

        localStorage.removeItem("OneTrust.Session");
        this.closeAll();
        this.principal.logOff();
        this.authService.loginCancelled();

        if (!email) {
            this.stateService.go("zen.app.auth.login", null, { reload: true });
        } else {
            this.userApi.getUserType(userTypeRequest).then((userTypeResponse: IProtocolResponse<ISamlSettings>) => {
                if (userTypeResponse.data && userTypeResponse.data.IsEnabled) {
                    const redirectUrl = (!userTypeResponse.data.IsLegacy && userTypeResponse.data.IdentityProviderSignOutUrl)
                        ? `/access/v1/saml/logout`
                        : `/auth/logout?email=${encodeURIComponent(email)}`;
                    window.location.href = `${window.location.protocol}//${process.env.ONETRUST_DEV_ENV || window.location.host}${redirectUrl}`;
                } else {
                    this.stateService.go("zen.app.auth.login", null, { reload: true });
                }
            });
        }

    }

    private persistTokenPostRefresh() {
        const refresh_token = localStorage.getItem("refresh_token");
        const access_token = localStorage.getItem("access_token");
        const access_token_stored_at = localStorage.getItem("access_token_stored_at");
        const expires_at = localStorage.getItem("expires_at");

        this.principal.authenticate({
            ...this.principal.getIdentity(),
            refresh_token,
            access_token,
            ".issued": (new Date(JSON.parse(access_token_stored_at))).toUTCString(),
            ".expires": (new Date(JSON.parse(expires_at))).toUTCString(),
        });

        this.authService.loginConfirmed("success", (config) => {
            return this.$rootScope.handleConfig(config);
        });
    }

}
