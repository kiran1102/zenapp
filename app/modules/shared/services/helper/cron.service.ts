import {
    DefaultCronObject,
    WeekdayOnlyValues,
    EveryDayValues,
} from "sharedModules/constants/cron.constant";
import { Frequency } from "sharedModules/enums/cron.enum";
import { ICronObject } from "sharedModules/interfaces/cron.interface";

export class CronBuilder {

    private _cronObject: ICronObject;

    constructor(initialValue: ICronObject = DefaultCronObject) {
        this._cronObject = initialValue;
    }

    setAll(cronObj: ICronObject) {
        if (!cronObj) return;
        this._cronObject = cronObj;
    }

    setMinute(minute: number) {
        if (!(minute || minute === 0)) return;
        this._cronObject.minute = minute;
    }

    setHour(hour: number) {
        if (!(hour || hour === 0)) return;
        this._cronObject.hour = hour;
    }

    setDayOfTheMonth(day: number) {
        if (!(day || day === 0)) return;
        this._cronObject.dayOfMonth = day;
        this._cronObject.daysOfTheWeek = null;
    }

    setDayOfTheWeek(day: number) {
        if (!day) return;
        this._cronObject.daysOfTheWeek = [day];
        this._cronObject.dayOfMonth = null;
    }

    setDaysOfTheWeek(days: number[]) {
        if (!(days && days.length)) return;
        this._cronObject.daysOfTheWeek = days;
        this._cronObject.dayOfMonth = null;
    }

    setWeekdayOnly(bool = true) {
       this._cronObject.daysOfTheWeek = bool ? WeekdayOnlyValues : EveryDayValues;
       this._cronObject.dayOfMonth = null;
    }

    setToDaily(weekdayOnly?: boolean) {
        this._cronObject.daysOfTheWeek = weekdayOnly ? WeekdayOnlyValues : EveryDayValues;
        this._cronObject.dayOfMonth = null;
    }

    getValue(): ICronObject {
        return this._cronObject;
    }
}

export function getFrequency(cronObj: ICronObject): Frequency {
    if (!cronObj) return;
    if (!cronObj.daysOfTheWeek && cronObj.dayOfMonth) return Frequency.Monthly;
    if (cronObj.daysOfTheWeek.length > 1) return Frequency.Daily;
    if (cronObj.daysOfTheWeek.length === 1) return Frequency.Weekly;
    return Frequency.Daily;
}
