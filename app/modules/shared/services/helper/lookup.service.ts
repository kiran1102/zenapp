// 3rd Party
import { Injectable } from "@angular/core";
import { filter } from "lodash";

@Injectable()
export class LookupService {

    filterOptionsBySelections(selections: any[], options: any[], idPropertyName?: string): any[] {
        if (!selections || selections.length === 0 || !selections[0]) return options;
        return filter(options, (option: any) => {
            if (idPropertyName && !option[idPropertyName]) return false;
            if (idPropertyName) {
                return selections.map((selection) => selection[idPropertyName]).indexOf(option[idPropertyName]) === -1;
            } else {
                return selections.indexOf(option) === -1;
            }
        });
    }

    filterOptionsByInput(selections: any[], options: any[], searchTerm: string, idPropertyName?: string, namePropertyName?: string): any[] {
        let filteredOptions = [...options];
        if (searchTerm) {
            filteredOptions = filter(options, (option: any): boolean => {
                if (namePropertyName && !option[namePropertyName]) return false;
                if (namePropertyName) {
                    return (String(option[namePropertyName])).toLowerCase().indexOf(searchTerm.toLowerCase()) !== -1;
                } else {
                    return (String(option)).toLowerCase().indexOf(searchTerm.toLowerCase()) !== -1;
                }
            });
        }
        return this.filterOptionsBySelections(selections, filteredOptions, idPropertyName);
    }
}
