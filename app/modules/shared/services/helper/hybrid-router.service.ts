import { Injectable } from "@angular/core";
import {
    NavigationExtras,
    Router,
    UrlTree,
} from "@angular/router";

@Injectable()
export class HybridRouter {
    constructor(
        public router: Router,
    ) {}

    navigate(commands: any[], extras?: NavigationExtras): Promise<boolean> {
        return this.router.navigate(commands, extras);
    }

    navigateByUrl(url: string | UrlTree, extras?: NavigationExtras): Promise<boolean> {
        return this.router.navigateByUrl(url, extras);
    }

    get url() {
        return this.router.url;
    }
}
