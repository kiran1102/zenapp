import { Injectable } from "@angular/core";

@Injectable()
export class SessionService {
    private blacklist = ["invite", "landing", "welcome"];
    private whitelist = [location.host];

    getReturnUrl() {
        const session = localStorage.getItem("OneTrust.Session");
        const sessionUrl = session ? JSON.parse(session) : "";
        return this.sanitizeURL(sessionUrl) || "";
    }

    setReturnUrl(url: string) {
        const sanitizedUrl = this.sanitizeURL(url);
        if (sanitizedUrl) {
            localStorage.setItem("OneTrust.Session", JSON.stringify(sanitizedUrl));
        } else {
            this.removeReturnUrl();
        }
        return url;
    }

    removeReturnUrl() {
        localStorage.removeItem("OneTrust.Session");
    }

    setAuthError(message) {
        if (message) {
            sessionStorage.setItem("OneTrust.authError", JSON.stringify(message));
        } else {
            this.removeAuthError();
        }
    }

    getAuthError() {
        const authErr = sessionStorage.getItem("OneTrust.authError");
        return authErr ? JSON.parse(authErr) : "";
    }

    removeAuthError() {
        sessionStorage.removeItem("OneTrust.authError");
    }

    private sanitizeList(list: string[], url: string) {
        if (!url) return "";
        for (let i = 0; i < list.length; i++) {
            if (url.indexOf(list[i]) > -1) {
                return (list[i]);
            }
        }
    }

    private sanitizeURL(url: string) {
        const hasWhitelistedItem = this.sanitizeList(this.whitelist, url);
        const hasBlacklistedItem = this.sanitizeList(this.blacklist, url);
        return hasWhitelistedItem && !hasBlacklistedItem ? url : "";
    }

}
