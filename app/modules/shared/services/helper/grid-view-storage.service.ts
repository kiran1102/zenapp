// 3rd Party
import { Injectable } from "@angular/core";
import { GridViewStorageNames } from "sharedModules/enums/grid-view-storage.enum";
import { ISortEvent } from "sharedModules/interfaces/sort.interface";

// Why have this service?
// - PM's and UX agree each grid view should exhibit consistent behavior
// - Having a single place to get/set grid view data for all grid views makes this request easier to achieve for all teams

// What is the most commonly peristed grid view data?
// - Columns, Filters, Search Text, and Pagination

// What goes in Local Storage? (clears only when browser cache is cleared, unique to the machine)
// - Columns
// - Filters

// What goes in Session Storage? (clears when browser tab closes)
// - Search Text
// - Pagination

export interface IGridViewStorage<T> {
    columns: string[];  // always saved as a string[], each string is a column name/nameKey, intended to work with ListColumnSelectorComponent
    filters: T[];       // teams can supply their own filter models, but this is intended to work with the FilterComponent<T, U, V>
    searchText: string; // always saved as a string, intended to work with ot-search-input
    pagination: any;    // todo - currently not in scope
    sort: ISortEvent;   // TODO: We could have a standard sort object model, could make generic if there are objections
}
interface ILocalStorageList {
    columns: string[];
    filters: any[];
}
interface ISessionStorageList {
    searchText: string;
    pagination: any;
    sort: ISortEvent;
}

@Injectable()
export class GridViewStorageService {

    getBrowserStorageList(name: GridViewStorageNames): IGridViewStorage<any> {
        const local = this.getLocalStorageList(name);
        const session = this.getSessionStorageList(name);
        return { ...local, ...session };
    }

    setBrowserStorageList(name: GridViewStorageNames, items: IGridViewStorage<any>) {
        const { columns, filters, searchText, pagination } = items;
        this.saveLocalStorageColumns(name, columns);
        this.saveLocalStorageFilters(name, filters);
        this.saveSessionStorageSearch(name, searchText);
        this.saveSessionStoragePagination(name, pagination);
    }

    saveLocalStorageColumns(name: GridViewStorageNames, columns: string[]) {
        const ls = this.getLocalStorageList(name) || { filters: null };
        this.setLocalStorageList(name, {...ls, columns});
    }

    saveLocalStorageFilters(name: GridViewStorageNames, filters: any[]) {
        const ls = this.getLocalStorageList(name) || { columns: null };
        this.setLocalStorageList(name, {...ls, filters});
    }

    saveSessionStorageSearch(name: GridViewStorageNames, searchText: string) {
        const ss = this.getSessionStorageList(name) || { pagination: null, sort: null };
        this.setSessionStorageList(name, {...ss, searchText});
    }

    saveSessionStoragePagination(name: GridViewStorageNames, pagination: any) {
        const ss = this.getSessionStorageList(name) || { searchText: null, sort: null };
        this.setSessionStorageList(name, {...ss, pagination});
    }

    saveSessionStorageSort(name: GridViewStorageNames, sort: ISortEvent) {
        const ss = this.getSessionStorageList(name) || { searchText: null, pagination: null };
        this.setSessionStorageList(name, {...ss, sort});
    }

    private getLocalStorageList(name: GridViewStorageNames): ILocalStorageList  {
        return JSON.parse(localStorage.getItem(name));
    }

    private getSessionStorageList(name: GridViewStorageNames): ISessionStorageList  {
        return JSON.parse(sessionStorage.getItem(name));
    }

    private setLocalStorageList(name: GridViewStorageNames, payload: ILocalStorageList) {
        localStorage.setItem(name, JSON.stringify(payload));
    }

    private setSessionStorageList(name: GridViewStorageNames, payload: ISessionStorageList) {
        sessionStorage.setItem(name, JSON.stringify(payload));
    }

}
