import { Injectable, Inject } from "@angular/core";

// DO NOT USE THIS SERVICE!
// THE PURPOSE OF THIS SERVICE IS TO KEEP THE
// LEGACY PROVIDERS IN SYNC WITH DIRECT WEB_STORAGE USAGE.
@Injectable()
export class StorageProxy {

    constructor(
        @Inject("$localStorage") private $localStorage,
        @Inject("$sessionStorage") private $sessionStorage,
    ) {
        const self = this;
        const _setItem = Storage.prototype.setItem;
        const _removeItem = Storage.prototype.removeItem;

        Storage.prototype.setItem = function(key, value) {
            _setItem.apply(this, arguments);
            self.cleanSync(this, key, value);
        };

        Storage.prototype.removeItem = function(key) {
            _removeItem.apply(this, arguments);
            self.cleanSync(this, key);
        };
    }

    private cleanSync(context: Storage, key: string, value?: string) {
        let storage;
        if (context === window.localStorage) {
            storage = this.$localStorage;
        } else if (context === window.sessionStorage) {
            storage = this.$sessionStorage;
        } else {
            return;
        }
        if (key && key.startsWith("OneTrust.") && key.indexOf("$") <= -1) {
            const localKey = key.substr(9);
            storage[localKey] = value ? JSON.parse(value) : undefined;
        }
    }

}
