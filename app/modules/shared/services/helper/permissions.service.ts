// Core
import { Injectable } from "@angular/core";
import {
    RawParams,
    StateOrName,
    TransitionOptions,
} from "@uirouter/core";

// Services
import { StateService } from "@uirouter/core";

// Services
import { PermissionsService } from "@onetrust/common";

// Interfaces
import { NgxPermissionsObject } from "ngx-permissions";

@Injectable()
export class Permissions {

    private permissionsMap: NgxPermissionsObject = this.permissionsService.permService.getPermissions();

    constructor(
        private permissionsService: PermissionsService,
        private stateService: StateService,
    ) {
        this.permissionsService.permService.permissions$.subscribe((map: NgxPermissionsObject) => {
            this.permissionsMap = map;
        });
    }

    canShow(...keys: string[]): boolean {
        if (keys.length === 1) {
            return this.checkPermissionsStatic(keys[0]);
        } else {
            for (const key of keys) {
                if (!this.checkPermissionsStatic(key)) {
                    return false;
                }
            }
        }
        return true;
    }

    goToFallback(fallbackRoute?: StateOrName, fallbackParams?: RawParams, fallbackOptions?: TransitionOptions) {
        fallbackOptions = { location: "replace", ...fallbackOptions };
        return setTimeout((): Promise<any> | ng.IPromise<any> => {
            return this.stateService.go(fallbackRoute || "404", fallbackParams, fallbackOptions);
        });
    }

    logout() {
        this.permissionsService.permService.flushPermissions();
    }

    private checkPermissionsStatic(key: string) {
        return Boolean(this.permissionsMap[key]);
    }
}
