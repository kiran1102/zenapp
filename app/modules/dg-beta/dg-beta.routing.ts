import { ModuleWithProviders } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { DGBetaLandingComponent } from "./components/dg-beta-landing/dg-beta-landing.component";

const appRoutes: Routes = [
    { path: "", component: DGBetaLandingComponent },
];

export const routing: ModuleWithProviders = RouterModule.forChild(appRoutes);
