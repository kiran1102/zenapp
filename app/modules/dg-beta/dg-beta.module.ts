import {
    NgModule,
} from "@angular/core";

// Modules
import { CommonModule } from "@angular/common";
import { OtButtonModule } from "@onetrust/vitreus";
import { OTPipesModule, PIPES } from "modules/pipes/pipes.module";

// Components
import { DGBetaLandingComponent } from "./components/dg-beta-landing/dg-beta-landing.component";

// Routing
import { routing } from "./dg-beta.routing";

@NgModule({
    imports: [
        CommonModule,
        OtButtonModule,
        OTPipesModule,
        routing,
    ],
    entryComponents: [
        DGBetaLandingComponent,
    ],
    declarations: [
        DGBetaLandingComponent,
    ],
    providers: [
        PIPES,
    ],
})
export class DGBetaModule { }
