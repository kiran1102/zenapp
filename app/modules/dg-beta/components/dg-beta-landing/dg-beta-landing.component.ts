// Angular
import {
    Component,
    OnInit,
} from "@angular/core";

// Services
import { StateService } from "@uirouter/core";
import { GlobalSidebarService } from "modules/global-sidebar/services/one-global-sidebar.service";

import "images/dg_logo.png";
import "images/dg_devices.png";
import "images/dg_hero_image.png";

@Component({
    selector: "dg-beta-landing",
    templateUrl: "./dg-beta-landing.component.html",
    host: { class: "full-size flex-full-height overflow-y-auto background-eggshell" },
})
export class DGBetaLandingComponent implements OnInit {

    constructor(
        private stateService: StateService,
        private globalSidebarService: GlobalSidebarService,
    ) {}

    ngOnInit() {
        this.globalSidebarService.set();
    }

    goToReadiness() {
        this.stateService.go("zen.app.pia.module.globalreadiness.views.welcome");
    }
}
