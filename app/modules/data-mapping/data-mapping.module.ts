// 3rd Party
import { NgModule } from "@angular/core";

// Modules
import { SharedModule } from "sharedModules/shared.module";
import { UpgradedLegacyModule } from "sharedModules/upgrade/upgraded-legacy.module";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";
import { VitreusModule } from "@onetrust/vitreus";
import { OTPipesModule, PIPES } from "modules/pipes/pipes.module";
import { OneVideoModule } from "modules/video/video.module";

// Components
import { DMCrossBorderMap } from "modules/data-mapping/components/cross-border-map/dm-cross-border-map.component";
import { DMCrossBorderCard } from "modules/data-mapping/components/cross-border-card/dm-cross-border-card.component";
import { DMDataMaps } from "modules/data-mapping/components/data-maps/dm-data-maps.component";
import { DMOneMapControls } from "modules/data-mapping/components/one-map-controls/dm-one-map-controls.component";
import { DMAssetLocationCard } from "modules/data-mapping/components/asset-location-card/dm-asset-location-card.component";
import { DMGlobalAssetMap } from "modules/data-mapping/components/global-asset-map/dm-global-asset-map.component";
import { SendBackModalComponent } from "modules/data-mapping/components/send-back-modal/send-back-modal.component";
import { DMWelcome } from "modules/data-mapping/components/welcome/dm-welcome.component";

// Services
import { DMCrossBorderConfigService } from "modules/data-mapping/services/dm-cross-border-config.service";
import { DMCrossBorderService } from "modules/data-mapping/services/dm-cross-border-map.service";
import { DMOneDataMaps } from "modules/data-mapping/services/dm-one-data-maps.service";
import { DMGlobalMapService } from "modules/data-mapping/services/dm-global-map.service";

@NgModule({
    imports: [
        SharedModule,
        UpgradedLegacyModule,
        CommonModule,
        FormsModule,
        VitreusModule,
        OTPipesModule,
        OneVideoModule,
    ],
    declarations: [
        DMCrossBorderMap,
        DMCrossBorderCard,
        DMDataMaps,
        DMOneMapControls,
        DMAssetLocationCard,
        DMGlobalAssetMap,
        DMWelcome,
        SendBackModalComponent,
    ],
    entryComponents: [
        DMCrossBorderMap,
        DMCrossBorderCard,
        DMDataMaps,
        DMOneMapControls,
        DMAssetLocationCard,
        DMGlobalAssetMap,
        DMWelcome,
        SendBackModalComponent,
    ],
    providers: [
        DMCrossBorderConfigService,
        DMCrossBorderService,
        DMOneDataMaps,
        DMGlobalMapService,
        PIPES, // TODO: Remove from providers when ot-common is implemented. Only necessary if you are using pipe in TS file, else just module is fine
    ],
})
export class DataMappingModule {}
