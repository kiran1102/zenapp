// 3rd party
import { Injectable } from "@angular/core";
import { find } from "lodash";

// Enums
import { DataMapsIds } from "modules/data-mapping/enums/dm-data-maps-ids.enums";

// Interfaces
interface IMapResource {
    route: string,
    name: string,
    resourceKey: string,
    resourceUpgradeKey: string,
    id: number,
}

@Injectable()
export class DMOneDataMaps {

    readonly dataMaps: IMapResource[] = [
        {
            route: "asset-map",
            name: "AssetMap",
            resourceKey: "DataMappingDataMapsAssetsByLocation",
            resourceUpgradeKey: "DataMappingDataMapsAssetsByLocationUpgrade",
            id: DataMapsIds.AssetsByLocation,
        },
        {
            route: "crossborder-map",
            name: "Crossborder",
            resourceKey: "DataMappingCrossborder",
            resourceUpgradeKey: "DataMappingCrossborderUpgrade",
            id: DataMapsIds.Crossborder,
        },
    ];
    readonly StateRoute = "zen.app.pia.module.datamap.views.datamaps";

    public getDataMaps(): IMapResource[] {
        return this.dataMaps;
    }

    public getStateRoute(): string {
        return this.StateRoute;
    }

    public getDataMapById(id: number): IMapResource {
        return find(this.dataMaps, (map: any): boolean => map.id === id);
    }

}
