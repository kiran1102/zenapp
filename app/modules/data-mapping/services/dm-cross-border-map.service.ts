// Rxjs
import { BehaviorSubject } from "rxjs";

// 3rd party
import { Injectable } from "@angular/core";
import * as _ from "lodash";
import * as d3 from "d3";

// Interfaces
import {
    IRegions,
    ITransfer,
    IFormattedTransfer,
    IPATransfer,
    ICrossBorderStateData,
} from "modules/data-mapping/interfaces/dm-cross-border.interface";
import {  IProtocolResponse } from "interfaces/protocol.interface";

// Services
import { DMCrossBorderConfigService } from "modules/data-mapping/services/dm-cross-border-config.service";
import { InventoryService } from "inventorySharedModule/shared/services/inventory-api.service";
import { Permissions } from "modules/shared/services/helper/permissions.service";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

// Enums and Constants
import { Regions } from "modules/data-mapping/enums/dm-cross-border.enums";
import { EmptyGuid } from "constants/empty-guid.constant";

@Injectable()
export class DMCrossBorderService {

    processingActivities: IPATransfer[] = [];
    toggleStateData = new BehaviorSubject({drawerOpen: false});
    toggleStateData$ = this.toggleStateData.asObservable();
    crossBorderMap: any;
    hasLinking = this.permissions.canShow("InventoryLinkingEnabled");

    private defaultActivity: IPATransfer = {
        destinationAssetsCount: 0,
        id: EmptyGuid,
        name: "",
        organization: "",
        originAssetsCount: 0,
    };
    private currentSelection = this.defaultActivity.id;
    private regions: IRegions = this.dmCrossBorderConfigService.getRegionConfig();
    private map: any;
    private mapData: IFormattedTransfer[];
    private filteredMapData: IFormattedTransfer[];

    constructor(
        readonly dmCrossBorderConfigService: DMCrossBorderConfigService,
        readonly inventoryService: InventoryService,
        readonly translatePipe: TranslatePipe,
        readonly permissions: Permissions,
    ) {}

    initMap(containerId: string, transferData: ITransfer[]) {
        import(/* webpackChunkName: "datamaps" */ "datamaps").then((m) => {
            this.crossBorderMap = m.default;
            this.mapData = this.formatMapData(transferData);
            this.map = this.drawMap(this.mapData, containerId);
            this.setMapZoom(this.map);
            this.processingActivities = this.getProcesingActivities(transferData);
            this.setArcEvent();
        });
    }

    highlightRegion(regions: string[]) {
        this.map.updateChoropleth(null, { reset: true });
        if (_.includes(regions, Regions.USA)) this.map.updateChoropleth(this.regions[Regions.USA]);
        if (_.includes(regions, Regions.EEARegion)) this.map.updateChoropleth(this.regions[Regions.EEARegion]);
        if (_.includes(regions, Regions.AdequateProtection)) this.map.updateChoropleth(this.regions[Regions.AdequateProtection]);
    }

    applyArcFilter(selection: string) {
        this.currentSelection = selection;
        this.clearArcData();
        if (selection === EmptyGuid) {
            setTimeout(() => {
                this.map.arc(this.mapData);
                this.setArcEvent();
            }, 500);
        } else if (this.hasLinking) {
            this.inventoryService.getPATransfer(selection).then((response: IProtocolResponse<ITransfer[]>) => {
                if (response.result) {
                    this.filteredMapData = this.formatMapData(response.data);
                    setTimeout(() => {
                        this.map.arc(this.filteredMapData);
                        this.setArcEvent();
                    }, 250);
                }
            });
        } else {
            const filteredTransfers: ITransfer[] = _.filter(this.mapData, (transfer: ITransfer): boolean => _.includes(_.flatten(transfer.processingActivities.map(_.values)), selection));
            this.filteredMapData = _.map(filteredTransfers, (transfer: ITransfer): IFormattedTransfer => {
                const processingData: IPATransfer = _.find(transfer.processingActivities, { id: selection });
                return {
                    ...transfer,
                    sources: processingData.sourcesCount,
                    reverseSources: processingData.reverseSourcesCount,
                    reverseDestinations: processingData.reverseDestinationsCount,
                    destinations: processingData.destinationsCount,
                    options: this.dmCrossBorderConfigService.getLineOptions(processingData, true, false),
                };
            });
            setTimeout(() => {
                this.map.arc(this.filteredMapData);
                this.setArcEvent();
            }, 500);
        }
    }

    getDefaultActivity(): IPATransfer {
        return this.defaultActivity;
    }

    setArcEvent() {
        if (this.hasLinking) {
            this.map.svg.selectAll("path").on("click", (activityData: ITransfer) => {
                if (!_.isUndefined(activityData.greatArc)) {
                    const stateData: ICrossBorderStateData = {
                        ...activityData,
                        drawerOpen: true,
                    };
                    this.toggleStateData.next(stateData);
                }
            });
        }
    }

    formatMapData(data: ITransfer[]): IFormattedTransfer[] {
        return _.map(data, (transfer: ITransfer): IFormattedTransfer => {
            return {
                sources: transfer.originAssetsCount || transfer.sources,
                destinations: transfer.destinationAssetsCount || transfer.destinations,
                origin: {
                    latitude: transfer.origin.latitude,
                    longitude: transfer.origin.longitude,
                    country: transfer.origin.locationKey,
                },
                destination: {
                    latitude: transfer.destination.latitude,
                    longitude: transfer.destination.longitude,
                    country: transfer.destination.locationKey,
                },
                options: this.dmCrossBorderConfigService.getLineOptions(transfer, false, this.hasLinking),
                reverseDestinations: transfer.reverseDestinations,
                reverseSources: transfer.reverseSources,
                processingActivities: transfer.processingActivities,
                processingActivitiesCount: transfer.processingActivitiesCount,
            };
        });
    }

    getProcesingActivities(transferData: ITransfer[]) {
        const processingActivities = _.sortBy(_.uniqBy(_.flatten(_.map(transferData, "processingActivities")), "id"), [(itemPA: IPATransfer) => itemPA.name.toLowerCase()]);
        return processingActivities;
    }

    private drawMap(data: ITransfer[], containerId: string): any {
        const mapConfig: any = this.getMapConfig(containerId);
        mapConfig.arc(data);
        return mapConfig;
    }

    private getMapConfig(containerId: string): any {
        return new this.crossBorderMap({
            element: document.getElementById(containerId),
            geographyConfig: {
                dataUrl: null,
                popupOnHover: false,
                highlightOnHover: true,
                highlightFillColor: "#eae9e9",
                highlightBorderColor: "#c4c4c4",
                highlightBorderWidth: 1,
            },
            responsive: true,
            fills: {
                defaultFill: "#dadada",
                blue: "#9cc6f0",
                green: "#7fe3a9",
                pink: "#fdd2ce",
                purple: "#daa8dd",
            },
            arcConfig: {
                arcSharpness: 1,
                animationSpeed: 600,
                popupOnHover: true,
                popupTemplate: (geography: ITransfer, data: ITransfer): string => {
                    if (this.hasLinking) {
                        if (this.currentSelection === EmptyGuid) {
                            return `
                                <div class="dm-cross-border-card__popup dm-cross-border-card__popup--full padding-all-half text-nowrap full-size position-relative shadow6 color-slate background-white border-grey border-radius">
                                    <div class="text-bold margin-top-1 border-bottom">
                                        ${this.translatePipe.transform(data.origin.country)} - ${this.translatePipe.transform(data.destination.country)}
                                    </div>
                                    <div class="text-small margin-top-bottom-half">
                                        <span class="text-bold">${this.translatePipe.transform("ProcessingActivities")}</span>: ${data.processingActivitiesCount}
                                    </div>
                                    <div class="text-small margin-top-bottom-half">
                                        <span class="text-bold">${this.translatePipe.transform("Assets")}</span> (${this.translatePipe.transform(data.origin.country)}): ${data.sources}
                                    </div>
                                    <div class="text-small margin-bottom-1">
                                        <span class="text-bold">${this.translatePipe.transform("Assets")}</span> (${this.translatePipe.transform(data.destination.country)}): ${data.destinations}
                                    </div>
                                    <div class="dm-cross-border-card__popup-arrow position-absolute"></div>
                                </div>
                            `;
                        } else {
                            return `<div class="dm-cross-border-card__popup dm-cross-border-card__popup--partial padding-all-half text-nowrap full-size position-relative shadow6 color-slate background-white border-grey border-radius">
                                    <div class="text-bold margin-top-1 border-bottom">
                                        ${this.translatePipe.transform(data.origin.country)} - ${this.translatePipe.transform(data.destination.country)}
                                    </div>
                                    <div class="text-small margin-top-bottom-half">
                                        <span class="text-bold">${this.translatePipe.transform("Assets")}</span> (${this.translatePipe.transform(data.origin.country)}): ${data.sources}
                                    </div>
                                    <div class="text-small margin-bottom-1">
                                        <span class="text-bold">${this.translatePipe.transform("Assets")}</span> (${this.translatePipe.transform(data.destination.country)}): ${data.destinations}
                                    </div>
                                    <div class="dm-cross-border-card__popup-arrow position-absolute"></div>
                                </div>
                            `;
                        }
                    } else {
                        return `
                            <div class="dm-cross-border-card__popup dm-cross-border-card__popup--v1 padding-all-half text-nowrap full-size position-relative shadow6 color-slate background-white border-grey border-radius">
                                <ul>
                                    <li class="border-bottom">
                                        <div class="text-bold margin-top-1">
                                            ${this.translatePipe.transform(data.origin.country)} ${this.translatePipe.transform("To")} ${this.translatePipe.transform(data.destination.country)}
                                        </div>
                                        <div class="text-small">
                                            <span class="text-bold">${this.translatePipe.transform("Source")}</span>: <span>${data.sources}</span>
                                        </div>
                                        <div class="text-small margin-bottom-1">
                                            <span class="text-bold">${this.translatePipe.transform("Destination")}</span>: <span>${data.destinations}</span>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="text-bold margin-top-1">
                                            ${this.translatePipe.transform(data.destination.country)} ${this.translatePipe.transform("To")} ${this.translatePipe.transform(data.origin.country)}
                                        </div>
                                        <div class="text-small">
                                            <span class="text-bold">${this.translatePipe.transform("Source")}</span>: <span>${data.reverseSources}</span>
                                        </div>
                                        <div class="text-small">
                                            <span class="text-bold">${this.translatePipe.transform("Destination")}</span>: <span>${data.reverseDestinations}</span>
                                        </div>
                                    </li>
                                </ul>
                                <div class="dm-cross-border-card__popup-arrow position-absolute"></div>
                            </div>
                        `;
                    }
                },
            },
        });
    }

    private setMapZoom(map: any) {
        map.svg.call(d3.behavior.zoom()
            .scaleExtent([1, 5])
            .on("zoom", () => {
            map.svg.selectAll("g").attr("transform", "translate(" + d3.event.translate + ")scale(" + d3.event.scale + ")");
        }));
    }

    private clearArcData() {
        this.map.arc([]);
    }
}
