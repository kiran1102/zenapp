import {
    IRegions,
    ITransfer,
    ILineOptions,
    IPATransfer,
} from "modules/data-mapping/interfaces/dm-cross-border.interface";

import { Regions } from "modules/data-mapping/enums/dm-cross-border.enums";

export class DMCrossBorderConfigService {

    getRegionConfig(): IRegions {
        return {
            [Regions.USA]: {
                USA: { fillKey: "blue" },
            },
            [Regions.EEARegion]: {
                AUT: { fillKey: "purple" },
                BEL: { fillKey: "purple" },
                BGR: { fillKey: "purple" },
                CZE: { fillKey: "purple" },
                CYP: { fillKey: "purple" },
                DNK: { fillKey: "purple" },
                EST: { fillKey: "purple" },
                FIN: { fillKey: "purple" },
                FRA: { fillKey: "purple" },
                DEU: { fillKey: "purple" },
                GRC: { fillKey: "purple" },
                HUN: { fillKey: "purple" },
                ISL: { fillKey: "purple" },
                IRL: { fillKey: "purple" },
                ITA: { fillKey: "purple" },
                LVA: { fillKey: "purple" },
                LIE: { fillKey: "purple" },
                LTU: { fillKey: "purple" },
                LUX: { fillKey: "purple" },
                MLT: { fillKey: "purple" },
                NLD: { fillKey: "purple" },
                NOR: { fillKey: "purple" },
                POL: { fillKey: "purple" },
                PRT: { fillKey: "purple" },
                ROU: { fillKey: "purple" },
                SVK: { fillKey: "purple" },
                SVN: { fillKey: "purple" },
                ESP: { fillKey: "purple" },
                SWE: { fillKey: "purple" },
                GBR: { fillKey: "purple" },
            },
            [Regions.AdequateProtection]: {
                AND: { fillKey: "pink" },
                ARG: { fillKey: "pink" },
                CAN: { fillKey: "pink" },
                CHE: { fillKey: "pink" },
                FRO: { fillKey: "pink" },
                GGY: { fillKey: "pink" },
                ISR: { fillKey: "pink" },
                IMN: { fillKey: "pink" },
                JEY: { fillKey: "pink" },
                NZL: { fillKey: "pink" },
                URY: { fillKey: "pink" },
            },
        };
    }

    getLineOptions(transfer: ITransfer | IPATransfer, isUpdate: boolean, isV2: boolean): ILineOptions {
        let strokeWidth: number;
        let strokeColor: string;
        let assetCount: number;

        if (isV2) {
            assetCount = transfer.originAssetsCount * transfer.destinationAssetsCount;
        } else {
            assetCount = isUpdate
                ? ((transfer as IPATransfer).sourcesCount * (transfer as IPATransfer).destinationsCount) + ((transfer as IPATransfer).reverseSourcesCount * (transfer as IPATransfer).reverseDestinationsCount)
                : ((transfer as ITransfer).sources * (transfer as ITransfer).destinations) + ((transfer as ITransfer).reverseSources * (transfer as ITransfer).reverseDestinations);
        }

        if (assetCount < 51) {
            strokeWidth = 2;
            strokeColor = "#F82D52";
        } else if (assetCount > 50 && assetCount < 101) {
            strokeWidth = 3;
            strokeColor = "#B5233E";
        } else  {
            strokeWidth = 5;
            strokeColor = "#810E25";
        }

        return {
            greatArc: true,
            strokeWidth,
            strokeColor,
        };
    }
}
