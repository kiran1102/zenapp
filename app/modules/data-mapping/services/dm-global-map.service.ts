import { Injectable, EventEmitter } from "@angular/core";

// 3rd Party
import {
    isUndefined,
    toString,
    maxBy,
    minBy,
    map,
} from "lodash";
import * as d3 from "d3";
import * as topojson from "topojson";
import textures from "textures";

import { worldJson } from "modules/data-mapping/constants/dm-world.constant";

// Services
import { ProtocolService } from "modules/core/services/protocol.service";

// Interfaces
import {
    IProtocolPacket,
    IProtocolConfig,
    IProtocolMessages,
} from "interfaces/protocol.interface";
import { IMapDataPoint } from "modules/data-mapping/interfaces/dm-map-data-point.interface";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

@Injectable()
export class DMGlobalMapService {

    private selectString: string;
    private container: Element;
    private width: number;
    private height: number;
    private projection: any;
    private mapId: string;
    private pointData: IMapDataPoint[];
    private scale = 1;
    private translate: any;
    private path: any;
    private svg: any;
    private g: any;
    private worldTopoJson = worldJson;
    private popUpId: string;
    private popUpElement: any;
    private dataGroups: any;
    private zoom: any;
    private popUpTemplate: string;
    private assetsTotal = 0;
    private assets3rdParty = 0;
    private assetsInternal = 0;
    private assetsUnknown = 0;
    private assetsTotalLabel: string;
    private assets3rdPartyLabel: string;
    private assetsInternalLabel: string;
    private assetsUnknownLabel: string;
    private assetsGeography = "";
    private rectWidth = 30;
    private locationClick = new EventEmitter();

    constructor(
        readonly OneProtocol: ProtocolService,
        private translatePipe: TranslatePipe,
    ) { }

    initMap(mapId: string, pointData: IMapDataPoint[], popUpId: string = "", locationClick: any) {
        this.locationClick = locationClick;
        this.selectString = `#${mapId}`;
        this.mapId = mapId;
        this.pointData = pointData;
        this.popUpId = popUpId;
        this.setupPopUp();
        this.popUpElement = d3.select(`#${popUpId}`);
        this.drawMap();
    }

    drawMap() {
        this.destroyMap();
        this.setDimensions();
        this.setProjection();
        this.drawWorld();
        if (!isUndefined(this.pointData)) {
            this.drawData(this.pointData);
            this.initZoom();
        }
    }

    destroyMap() {
        d3.select("#global-assets").selectAll("svg").remove();
    }

    setDimensions() {
        this.container = document.getElementById(this.mapId);
        const size = this.container.getBoundingClientRect();
        this.width = size.width;
        this.height = size.height;
    }

    setProjection() {
        this.projection = d3.geo.mercator()
            .translate([(this.width / 2), (this.height / 2)])
            .center([0, 30]); // Adjust center to focus on northern hemisphere
        if ((this.width / this.height) > 1.7) {
            this.projection.scale(this.width / 2.5 / Math.PI);
        } else {
            this.projection.scale(this.width / 2 / Math.PI);
        }

        this.path = d3.geo.path()
            .projection(this.projection);
    }

    drawWorld() {
        const texture = textures.lines()
            .orientation("vertical", "horizontal")
            .size(12)
            .strokeWidth(1)
            .stroke("#e6e6e6");

        this.zoom = d3.behavior.zoom()
            .scaleExtent([1, 9])
            .on("zoom", this.zoomMap);

        this.svg = d3.select(this.selectString).append("svg")
            .attr("class", "ocean")
            .attr("width", this.width)
            .attr("height", this.height)
            .call(this.zoom)
            .call(texture)
            .append("g");

        this.svg.append("rect")
            .attr("class", "ocean-texture")
            .attr("width", this.width)
            .attr("height", this.height)
            .style("fill", texture.url());

        this.g = this.svg.append("g");

        this.g.append("path")
            .datum(topojson.feature(this.worldTopoJson, this.worldTopoJson.objects.countries))
            .attr("d", this.path)
            .attr("class", "land");
    }

    drawData(pointData: IMapDataPoint[]) {

        const popUp = document.getElementById(this.popUpId);

        this.g.selectAll(".data-group").remove();

        this.dataGroups = this.g.selectAll(".data-group")
            .data(pointData).enter()
            .append("g")
            .attr("class", "data-group")
            .attr("transform", (d: IMapDataPoint): string => {
                const x = this.projection([d.location.longitude, d.location.latitude])[0];
                const y = this.projection([d.location.longitude, d.location.latitude])[1];
                return `translate(${x}, ${y})`;
            })
            .on("mouseover", (d: IMapDataPoint) => {
                this.assetsTotal = d.totalCount;
                this.assets3rdParty = d.thirdPartyCount;
                this.assetsInternal = d.internalCount;
                this.assetsUnknown = d.unAnsweredCount;
                this.assetsGeography = d.location ?
                    d.location.locationKey ? this.translatePipe.transform(d.location.locationKey) : d.location.location
                    : "";
                this.assetsTotalLabel = (d.totalCount > 1) ? this.translatePipe.transform("Assets") : this.translatePipe.transform("Asset");
                this.assets3rdPartyLabel = this.translatePipe.transform("3rdParty");
                this.assetsInternalLabel = this.translatePipe.transform("Internal");
                this.assetsUnknownLabel = this.translatePipe.transform("Unanswered");
                this.updatePopUp();
                this.popUpElement.style("visibility", "visible");
            })
            .on("mousemove", () => {
                const yCoord: number = d3.event.clientY;
                const xCoord: number = d3.event.clientX;

                if (((yCoord + popUp.offsetHeight) > this.height) && ((yCoord - this.height) < popUp.offsetHeight)) {
                    this.popUpElement.style("top", `${yCoord - popUp.offsetHeight - 25}px`);
                } else {
                    this.popUpElement.style("top", `${yCoord + 25}px`);
                }

                if (((xCoord + popUp.offsetWidth - 200) > this.width) && ((xCoord - this.width - 200) < popUp.offsetWidth)) {
                    this.popUpElement.style("left", `${xCoord - popUp.offsetWidth - 25}px`);
                } else {
                    this.popUpElement.style("left", `${xCoord + 25}px`);
                }
            })
            .on("mouseout", () => {
                this.popUpElement.style("visibility", "hidden");
            })
            .on("click", (d: IMapDataPoint) => {
                this.locationClick.emit(d.location.location);
            });

        this.dataGroups.append("rect")
            .attr("class", "data-circle")
            .attr("height", Math.max(25 / this.scale, 2))
            .attr("width", (d: IMapDataPoint): number => {
                const countLength = toString(d.totalCount).length;
                this.updateWidth(countLength);
                return Math.max(this.rectWidth / this.scale, 2);
            })
            .attr("x", (d: IMapDataPoint): number => {
                const countLength = toString(d.totalCount).length;
                this.updateWidth(countLength);
                return -(Math.max(this.rectWidth / this.scale, 2) / 2);
            })
            .attr("y", -(Math.max(25 / this.scale, 2) / 2))
            .attr("rx", Math.max(this.rectWidth / this.scale, 2) / 10)
            .attr("ry", Math.max(this.rectWidth / this.scale, 2) / 10);

        this.dataGroups.append("text")
            .attr("class", "data-text")
            .attr("text-anchor", "middle")
            .attr("dy", ".4em")
            .attr("transform", `translate(0, 0)scale(${Math.max(1 / this.scale, .1)})`)
            .text((d: IMapDataPoint): number => d.totalCount);
    }

    initZoom() {
        const svgRect: any = this.svg[0][0].getBoundingClientRect();

        const locationArray: any[] = d3.selectAll(".data-circle");

        const rectArray: any[] = map(locationArray[0], (loc: any): any => {
            return loc.getBoundingClientRect();
        });

        if (!rectArray.length) return;

        let xMax: number = maxBy(rectArray, (rect): number => rect.right).right - svgRect.left;
        let xMin: number = minBy(rectArray, (rect): number => rect.left).left - svgRect.left;
        let yMax: number = maxBy(rectArray, (rect): number => rect.bottom).bottom - svgRect.top;
        let yMin: number = minBy(rectArray, (rect): number => rect.top).top - svgRect.top;

        const checkWidth: boolean = (xMax - xMin < 100);
        const checkHeight: boolean = (yMax - yMin < 100);

        if (checkWidth || checkHeight) {
            xMax = xMax + 50;
            xMin = xMin - 50;
            yMax = yMax + 50;
            yMin = yMin - 50;
        }

        const extentWidth: number = xMax - xMin;
        const extentHeight: number = yMax - yMin;
        const extentXTranslate: number = (xMin + xMax) / 2;
        const extentYTranslate: number = (yMin + yMax) / 2;

        const scale: number = .8 / Math.max(extentWidth / this.width, extentHeight / this.height);
        const translate: number[] = [this.width / 2 - scale * extentXTranslate, this.height / 2 - scale * extentYTranslate ];

        if ((this.width / this.height) > 1.5) {
            translate[1] = translate[1] + 100;
        }
        if ((this.width / this.height) > 1.8) {
            translate[1] = translate[1] + 200;
        }

        this.g.transition()
            .duration(750)
            .call(this.zoom.translate(translate).scale(scale).event);
    }

    zoomMap = () => {
        this.translate = d3.event.translate;
        this.scale = d3.event.scale;
        this.translate[0] = Math.min((this.width / this.height)  * (this.scale - 1) + 500, Math.max( this.width * (1 - this.scale) - 500 * this.scale, this.translate[0]));
        this.translate[1] = Math.min(this.height / 2 * (this.scale - 1) + 500 * this.scale, Math.max(this.height / 2 * (1 - this.scale) - 500 * this.scale, this.translate[1]));
        d3.selectAll(".data-circle")
            .attr("height", Math.max(25 / this.scale, 2))
            .attr("width", (d: IMapDataPoint): number => {
                const countLength = toString(d.totalCount).length;
                this.updateWidth(countLength);
                return Math.max(this.rectWidth / this.scale, 2);
            })
            .attr("x", (d: IMapDataPoint): number => {
                const countLength = toString(d.totalCount).length;
                this.updateWidth(countLength);
                return -(Math.max(this.rectWidth / this.scale, 2) / 2);
            })
            .attr("y", -(Math.max(25 / this.scale, 2) / 2))
            .attr("rx", Math.max(this.rectWidth / this.scale, 2) / 10)
            .attr("ry", Math.max(this.rectWidth / this.scale, 2) / 10);
        d3.selectAll(".data-text")
            .attr("transform", `translate(0, 0)scale(${Math.max(1 / this.scale, .1)})`);
        this.g.attr("transform", `translate(${this.translate})scale(${this.scale})`);
    }

    updateWidth(countLength: number) {
        this.rectWidth = 30;
        if (countLength > 2) {
            this.rectWidth = this.rectWidth + ((countLength - 2) * 10);
        }
    }

    setupPopUp() {
        d3.select(this.selectString).append("div")
            .attr("id", this.popUpId)
            .attr("class", "dm-asset-popup");
    }

    updatePopUp() {
        this.popUpTemplate = `
                <div class="dm-asset-popup__header">
                    <div class="dm-asset-popup__icon-circle">
                        <i class="fa fa-fw fa-desktop"></i>
                    </div>
                    <span class="dm-asset-popup__count">${this.assetsTotal}</span>&nbsp
                    <span class="text-bold">${this.assetsTotalLabel}</span>
                </div>
                <div class="dm-asset-popup__content">
                    <div class="dm-asset-popup__item">
                        <label>${this.assets3rdParty}</label>
                        <label class="text-italic">${this.assets3rdPartyLabel}</label>
                    </div>
                    <div class="dm-asset-popup__item">
                        <label>${this.assetsInternal}</label>
                        <label class="text-italic">${this.assetsInternalLabel}</label>
                    </div>
                    <div class="dm-asset-popup__item">
                        <label>${this.assetsUnknown}</label>
                        <label class="text-italic">${this.assetsUnknownLabel}</label>
                    </div>
                </div>
                <div class="dm-asset-popup__footer text-center text-italic">
                    ${this.assetsGeography}
                </div>`;

        this.popUpElement.html(this.popUpTemplate);
    }

}
