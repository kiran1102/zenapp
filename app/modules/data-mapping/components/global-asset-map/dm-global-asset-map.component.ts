import {
    Component,
    OnInit,
    Input,
    Output,
    EventEmitter,
    OnChanges,
} from "@angular/core";

// Services
import { DMGlobalMapService } from "modules/data-mapping/services/dm-global-map.service";

// Interfaces
import { IMapDataPoint } from "modules/data-mapping/interfaces/dm-map-data-point.interface";

@Component({
    selector: "dm-global-asset-map",
    template: `<div class="full-size-block" id="dm-global-assets"></div>`,
})
export class DMGlobalAssetMap implements OnInit, OnChanges {

    @Input() mapData: IMapDataPoint[];
    @Output() locationClick = new EventEmitter();

    private throttleTimer: any;

    constructor(readonly dmGlobalMapService: DMGlobalMapService) {}

    ngOnInit(){
        setTimeout(() => {
            this.drawMap();
            window.addEventListener("resize", this.throttleMapResize);
        }, 0);
    }

    ngOnChanges(changes: ng.IOnChangesObject){
        if (changes.mapData && !changes.mapData.isFirstChange()) {
            this.redrawAssets();
        }
    }

    private drawMap(){
        this.dmGlobalMapService.initMap("dm-global-assets", this.mapData, "dm-asset-popup", this.locationClick);
    }

    private redrawAssets(){
        this.dmGlobalMapService.drawData(this.mapData);
    }

    private throttleMapResize = () => {
        clearTimeout(this.throttleTimer);
        this.throttleTimer = setTimeout(() => {
            this.drawMap();
        }, 200);
    }
}
