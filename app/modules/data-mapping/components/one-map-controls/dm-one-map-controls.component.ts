
import { Component, Input } from "@angular/core";

import { IMapControlOption } from "modules/data-mapping/interfaces/dm-map-controls.interface";

@Component({
    selector: "dm-one-map-controls",
    template: `
        <div class="dm-one-map-controls position-absolute left-bottom-2">
            <upgraded-one-popup-toggle
                class="shadow6"
                [popupTrigger]="'click'"
                [popupPlacement]="'top-left'"
                [options]="options"
                (callback)="toggleSelect($event)"
                >
                <upgraded-one-button
                    class="margin-top-1 block"
                    identifier="toggleMapFiltersPanel"
                    type="select"
                    [isCircular]="true"
                    [isFloating]="true"
                    [isSelected]="selected"
                    [icon]="'ot ot-layers'"
                    (buttonClick)="toggleSelect()"
                ></upgraded-one-button>
            </upgraded-one-popup-toggle>
        </div>`,
})
export class DMOneMapControls {

    selected = false;

    @Input() options: IMapControlOption[];

    toggleSelect(selected?: boolean) {
        this.selected = selected || !this.selected;
    }
}
