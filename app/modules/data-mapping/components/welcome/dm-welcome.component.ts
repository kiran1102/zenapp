// 3rd party
import {
    Component,
    Inject,
    OnInit,
} from "@angular/core";
import { StateService } from "@uirouter/core";

// Services
import { Content } from "sharedModules/services/provider/content.service";

// Constant
import { DMWelcomeTabs } from "constants/dm-welcome.constant";

// Interfaces
import { ITab } from "interfaces/ot-tabs.interface";
import { IStringMap } from "interfaces/generic.interface";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

@Component({
    selector: "dm-welcome",
    templateUrl: "./dm-welcome.component.html",
})
export class DMWelcome implements OnInit {

    tabs: ITab[];
    currentTab: string;
    tabIds: IStringMap<string> = DMWelcomeTabs;

    constructor(
        private stateService: StateService,
        private translatePipe: TranslatePipe,
        private content: Content,
    ) { }

    ngOnInit() {
        this.tabs = this.getTabs();
        this.currentTab = this.stateService.params.tab || this.tabIds.Overview;
    }

    onTabClick(option: ITab) {
        this.currentTab = option.id;
        this.stateService.go(".", { tab: option.id }, { notify: false });
    }

    private getTabs(): ITab[] {
        return [
            {
                id: this.tabIds.Overview,
                text: this.translatePipe.transform("Overview"),
                otAutoId: "dmWelcomeOverview",
            },
            {
                id: this.tabIds.GettingStartedChecklist,
                text: this.translatePipe.transform("GettingStartedChecklist"),
                otAutoId: "dmWelcomeGettingStartedChecklist",
            },
        ];
    }
}
