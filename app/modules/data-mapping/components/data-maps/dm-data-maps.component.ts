import { Component, Inject, OnInit } from "@angular/core";
import { StateService } from "@uirouter/core";

// Services
import { DMOneDataMaps } from "modules/data-mapping/services/dm-one-data-maps.service";

// Enums
import { DataMapsIds } from "modules/data-mapping/enums/dm-data-maps-ids.enums";

// Interfaces
import { IMapResource } from "modules/data-mapping/interfaces/dm-map-resource.interface";

@Component({
    selector: "dm-data-maps",
    template: `
        <div class="dm-data-maps full-size column-vertical-center">
            <ot-page-header [title]="map?.name | otTranslate"></ot-page-header>
            <div
                class="dm-data-maps__wrapper overflow-y-auto height-100 padding-all-0"
                *ngIf="mapId === dataMapsIds.AssetsByLocation"
                >
                <dm-asset-location-card class="flex height-100"></dm-asset-location-card>
            </div>
            <div
                class="dm-data-maps__wrapper overflow-hidden height-100 padding-all-0"
                *ngIf="mapId === dataMapsIds.Crossborder"
                >
                <dm-cross-border-card class="flex height-100"></dm-cross-border-card>
            </div>
        </div>
    `,
})

export class DMDataMaps implements OnInit {

    mapId: number;
    header: string;
    map: IMapResource;
    dataMapsIds = DataMapsIds;

    constructor(
        private stateService: StateService,
        readonly dmOneDataMaps: DMOneDataMaps,
    ) { }

    ngOnInit() {
        this.mapId = this.stateService.params.id ? parseInt(this.stateService.params.id, 10) : DataMapsIds.AssetsByLocation;
        this.map = this.dmOneDataMaps.getDataMapById(this.mapId);
    }

}
