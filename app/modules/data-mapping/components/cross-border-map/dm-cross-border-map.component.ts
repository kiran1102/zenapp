import {
    Component,
    Input,
    OnInit,
} from "@angular/core";

// Services
import { DMCrossBorderService } from "modules/data-mapping/services/dm-cross-border-map.service";

// Interfaces
import { ITransfer } from "modules/data-mapping/interfaces/dm-cross-border.interface";

@Component({
    selector: "dm-cross-border-map",
    template: `<div class="full-size-block" id="dm-cross-border-map"></div>`,
})

export class DMCrossBorderMap implements OnInit {

    @Input() mapId: string;
    @Input() mapData: ITransfer[];

    constructor(
        readonly dmCrossBorderService: DMCrossBorderService,
    ) {}

    ngOnInit() {
        setTimeout((): void => {
            this.drawMap();
        }, 0);
    }

    private drawMap(): void {
        this.dmCrossBorderService.initMap(this.mapId, this.mapData);
    }
}
