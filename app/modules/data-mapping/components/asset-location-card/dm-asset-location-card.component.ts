import { Component, Inject, OnInit } from "@angular/core";
import { StateService } from "@uirouter/core";

// 3rd Party
import {
    forEach,
    find,
    map,
    remove,
} from "lodash";
import {
    Subscription,
    Observable,
} from "rxjs";
import { tap } from "rxjs/operators";

// Redux
import { StoreToken } from "tokens/redux-store.token";
import { getCurrentOrgId, getCurrentOrgTree, getOrgList } from "oneRedux/reducers/org.reducer";

// Services
import TreeMap from "TreeMap";
import { Permissions } from "modules/shared/services/helper/permissions.service";
import { InventoryActionService } from "inventoryModule/services/action/inventory-action.service";
import { InventoryService } from "inventorySharedModule/shared/services/inventory-api.service";

// Constants + Enums
import { InventoryPermissions } from "constants/inventory.constant";
import { InventorySchemaIds } from "constants/inventory-config.constant";
import { InventoryTableIds } from "enums/inventory.enum";
import { FilterColumnTypes} from "enums/filter-column-types.enum";

// Interfaces
import { IMapDataPoint } from "modules/data-mapping/interfaces/dm-map-data-point.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";
import { IOrganization } from "interfaces/org.interface";
import {
    IFilter,
    IFilterTree,
} from "interfaces/filter.interface";
import { IAttributeV2 } from "interfaces/inventory.interface";
import { IStore } from "interfaces/redux.interface";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

interface IFilterToggle {
    name: string;
    values: IFilterValue[];
}

interface IFilterValue {
    name: string;
    value: string;
    toggleState: boolean;
}

@Component({
    selector: "dm-asset-location-card",
    templateUrl: "./dm-asset-location-card.component.html",
})
export class DMAssetLocationCard implements OnInit {

    subscription: Subscription;
    assetsReady = false;
    hasSubOrgs: boolean;
    unknownLocations = 0;
    globalLocations = 0;
    globalLocationsText: string;
    unknownLocationsText: string;

    private viewReady = false;
    private filterToggles: IFilterToggle;
    private mapData: IMapDataPoint[];
    private assetLocations: IMapDataPoint[];
    private orgData: IOrganization[];
    private currentOrgTree: IOrganization;
    private orgFilter: IOrganization;
    private currentOrgId: string;
    private orgTreeMap: any;
    private orgAttribute: IFilter;
    private locationAttribute: IFilter;

    constructor(
        private stateService: StateService,
        @Inject(StoreToken) private store: IStore,
        private inventoryAction: InventoryActionService,
        private inventory: InventoryService,
        private translatePipe: TranslatePipe,
        private permissions: Permissions,
    ) {
        this.filterToggles = {
            name: this.translatePipe.transform("AssetsInternalOrThirdParty"),
            values: [{
                name: this.translatePipe.transform("3rdParty"),
                value: "3rd Party",
                toggleState: true,
            },
            {
                name: this.translatePipe.transform("Internal"),
                value: "Internal",
                toggleState: true,
            }],
        };
    }

    ngOnInit() {
        this.currentOrgId = getCurrentOrgId(this.store.getState());
        this.currentOrgTree = getCurrentOrgTree(this.store.getState());
        this.getOrganizations();
        Promise.all([this.getAttributes(), this.getAssets(this.currentOrgId).toPromise()]).then(() => this.viewReady = true);
    }

    goToFilteredInventory = (locationName: string, defaultProp?: string) => {
        const orgList: IOrganization[] = getOrgList(this.currentOrgId)(this.store.getState());
        const orgNames: string[] = map(orgList, (org: IOrganization): string => {
            return org.Name;
        });

        this.locationAttribute.values = [locationName || defaultProp];
        this.orgAttribute.values = orgNames;

        const sessionFilter: IFilterTree = { AND: [this.locationAttribute, this.orgAttribute]};
        sessionStorage.removeItem("DMInventorySearchTerm20");
        sessionStorage.setItem("DMInventoryFilterSettings20", JSON.stringify(sessionFilter));
        sessionStorage.setItem(`DMInventoryPageNumber20`, JSON.stringify(0));
        this.stateService.go("zen.app.pia.module.datamap.views.inventory.list", { Id: InventorySchemaIds.Assets });
    }

    applyOrgFilter(item: string) {
        this.assetsReady = false;
        this.currentOrgId = item;
        this.getAssets(item).toPromise().then(() => this.viewReady = true);
    }

    toggleSwitch = (filter: IFilterToggle, valueObj: IFilterValue) => {
        const toggledValue: IFilterValue = find(filter.values, {value: valueObj.value});
        toggledValue.toggleState = !toggledValue.toggleState;
        this.applyToggleFilter();
    }

    private getOrganizations() {
        this.orgData = [this.currentOrgTree];
        this.orgFilter = this.orgData[0];
        this.orgTreeMap = new TreeMap("Id", "ParentId", "Children", this.orgFilter);
        this.hasSubOrgs = Boolean(this.orgTreeMap.Size());
    }

    private getAssets(orgGroupId: string): Observable<IProtocolResponse<{ data: IMapDataPoint[] }>> {
        return this.inventoryAction.getAssetMapData(orgGroupId).pipe(tap((response: IProtocolResponse<{ data: IMapDataPoint[] }>) => {
                if (response.result) {
                    this.setToggleStates();
                    this.mapData = response.data.data;
                    this.unknownLocations = 0;
                    this.globalLocations = 0;
                    remove(this.mapData, (locationData: IMapDataPoint): boolean => {
                        if (locationData.location.location === "Unknown") {
                            this.unknownLocations = locationData.totalCount;
                            this.unknownLocationsText = this.translatePipe.transform("AssetsMissingLocation", { numAssets: this.unknownLocations });
                            return true;
                        } else if (locationData.location.location === "Global") {
                            this.globalLocations = locationData.totalCount;
                            this.globalLocationsText = this.translatePipe.transform("GlobalAssetsCount", { numAssets: this.globalLocations });
                            return true;
                        }
                        return false;
                    });
                    this.assetLocations = this.mapData;
                    this.assetsReady = true;
                }
            }));
    }

    private setToggleStates() {
        forEach(this.filterToggles.values, (value: IFilterValue) => {
            value.toggleState = true;
        });
    }

    private applyToggleFilter() {
        this.assetLocations = [];
        forEach(this.mapData, (location: IMapDataPoint) => {
            const filteredLocationData: IMapDataPoint = this.filterData(location);
            if (filteredLocationData.totalCount) {
                this.assetLocations.push(this.filterData(location));
            }
        });
    }

    private filterData(location: IMapDataPoint): IMapDataPoint {
        const locationInfo: IMapDataPoint = {
            location: location.location,
            totalCount: location.totalCount,
            internalCount: location.internalCount,
            thirdPartyCount: location.thirdPartyCount,
            unAnsweredCount: location.unAnsweredCount,
        };
        forEach(this.filterToggles.values, (filter: IFilterValue) => {
            switch (filter.value) {
                case "3rd Party":
                    if (!filter.toggleState) {
                        locationInfo.totalCount = locationInfo.totalCount -= locationInfo.thirdPartyCount;
                        locationInfo.thirdPartyCount = 0;
                    }
                    break;
                case "Internal":
                    if (!filter.toggleState) {
                        locationInfo.totalCount = locationInfo.totalCount -= locationInfo.internalCount;
                        locationInfo.internalCount = 0;
                    }
                    break;
            }
        });
        return locationInfo;
    }

    private getAttributes(): ng.IPromise<void> {
        const hasV2InventoryUi = this.permissions.canShow(InventoryPermissions[InventoryTableIds.Assets].inventoryLinkingV2);
        return this.inventory.getAttributes(InventorySchemaIds.Assets).then((response: IProtocolResponse<IAttributeV2[]>) => {
            if (!response.result) return;

            const location: IAttributeV2 = find(response.data, (attribute) => {
                return attribute.fieldName === "location";
            });

            this.locationAttribute = {
                columnId: hasV2InventoryUi ? location.fieldName : location.id,
                attributeKey: hasV2InventoryUi ? location.fieldName : location.id,
                dataType: FilterColumnTypes.Multi,
                operator: "EQ",
                values: [],
            };

            const org: IAttributeV2 = find(response.data, (attribute) => {
                return attribute.fieldName === "organization";
            });

            this.orgAttribute = {
                columnId: hasV2InventoryUi ? org.fieldName : org.id,
                attributeKey: hasV2InventoryUi ? org.fieldName : org.id,
                dataType: FilterColumnTypes.Multi,
                operator: "EQ",
                values: [],
            };
        });
    }
}
