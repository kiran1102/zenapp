import {
    Component,
    OnInit,
    Inject,
} from "@angular/core";
import { StateService } from "@uirouter/core";
import {
    includes,
    remove,
    toString,
    map,
    forEach,
    filter,
} from "lodash";

// Services
import { DMCrossBorderService } from "modules/data-mapping/services/dm-cross-border-map.service";
import { InventoryService } from "inventorySharedModule/shared/services/inventory-api.service";
import { Permissions } from "modules/shared/services/helper/permissions.service";

// Enums and Constants
import { Regions } from "modules/data-mapping/enums/dm-cross-border.enums";
import { InventoryTableIds } from "enums/inventory.enum";
import { InventoryDetailRoutes } from "constants/inventory.constant";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

// Interfaces
import {
    ITransfer,
    IPATransfer,
    ICrossBorderStateData,
} from "modules/data-mapping/interfaces/dm-cross-border.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";
import { IMapControlOption } from "modules/data-mapping/interfaces/dm-map-controls.interface";

@Component({
    selector: "dm-cross-border-card",
    templateUrl: "./dm-cross-border-card.component.html",
})
export class DMCrossBorderCard implements OnInit {

    ready = false;
    drawerOpen = false;
    currentStateData: ICrossBorderStateData;
    mapData: ITransfer[] = [];
    controlOptions: IMapControlOption[];
    selectedPA: IPATransfer;
    hasPADetailsPermission: boolean;
    activityList: IPATransfer[] = [];
    activityListLookup: IPATransfer[] = [];
    showDefaultList = true;
    private toggledRegions: string[] = [];

    constructor(
        private stateService: StateService,
        private inventoryService: InventoryService,
        private dmCrossBorderService: DMCrossBorderService,
        private translatePipe: TranslatePipe,
        private permissions: Permissions,
    ) { }

    ngOnInit() {
        const hasLinking = this.permissions.canShow("InventoryLinkingEnabled");
        this.hasPADetailsPermission = this.permissions.canShow("DataMappingInventoryPADetails");
        this.dmCrossBorderService.toggleStateData.next({drawerOpen: false});
        this.inventoryService.getTransfers(hasLinking).then((response: IProtocolResponse<ITransfer[]>) => {
            if (!response.result) return;
            this.mapData = hasLinking ? response.data : this.transformTransferData(response.data);
            this.activityList = this.dmCrossBorderService.getProcesingActivities(this.dmCrossBorderService.formatMapData(this.mapData));
            this.ready = true;
        });
        this.controlOptions = [
            {
                legendColor: "map-accent-blue",
                title: this.translatePipe.transform(Regions.USA),
                key: Regions.USA,
                action: this.filterAction,
                value: false,
            },
            {
                legendColor: "map-accent-purple",
                title: this.translatePipe.transform(Regions.EEARegion),
                key: Regions.EEARegion,
                action: this.filterAction,
                value: false,
            },
            {
                legendColor: "map-accent-pink",
                title: this.translatePipe.transform(Regions.AdequateProtection),
                key: Regions.AdequateProtection,
                action: this.filterAction,
                value: false,
            },
        ];
        this.dmCrossBorderService.toggleStateData.subscribe((stateData: ICrossBorderStateData) => {
            this.currentStateData = stateData;
            this.drawerOpen = this.currentStateData.drawerOpen;
        });
    }

    trackByActivity(index: number, activity: IPATransfer): string {
        return activity.id;
    }

    trackByIndex(index: number, activity: IPATransfer): number {
        return index;
    }

    onInputChange({ key, value }) {
        if (key === "Enter") { return; }
        this.showDefaultList = false;
        this.filterList(this.activityList, value);
    }

    applyFilters({ currentValue }) {
        this.dmCrossBorderService.toggleStateData.next({drawerOpen: false});
        this.showDefaultList = true;
        if (currentValue) {
            this.selectedPA = currentValue;
        } else {
            this.selectedPA = undefined;
        }
        this.dmCrossBorderService.applyArcFilter(toString(this.selectedPA ? this.selectedPA.id : this.dmCrossBorderService.getDefaultActivity().id));
    }

    toggleDrawer(toggleState: boolean) {
        this.dmCrossBorderService.toggleStateData.next({drawerOpen: toggleState});
    }

    goToProcessingActivity(inventoryId: string) {
        this.stateService.go(InventoryDetailRoutes[InventoryTableIds.Processes], { inventoryId: InventoryTableIds.Processes, recordId: inventoryId, tabId: "related" });
    }

    private transformTransferData(transfers: ITransfer[]): ITransfer[] {
        const transformedTransfers: ITransfer[] = [];
        forEach(transfers, (transfer: ITransfer) => {
            transformedTransfers.push({
                processingActivities: map(transfer.pas, (processingActivity: IPATransfer): IPATransfer => {
                    return {
                        id: processingActivity.id,
                        name: processingActivity.name,
                        sourcesCount: processingActivity.sourcesCount,
                        reverseDestinationsCount: processingActivity.reverseDestinationsCount,
                        destinationsCount: processingActivity.destinationsCount,
                        reverseSourcesCount: processingActivity.reverseSourcesCount,
                        organization: "",
                    };
                }),
                destinations: transfer.destinations + transfer.reverseSources,
                reverseSources: transfer.reverseSources,
                origin: transfer.source,
                destination: transfer.destination,
                sources: transfer.sources,
                reverseDestinations: transfer.reverseDestinations,
                processingActivitiesCount: transfer.pas.length,
            });
        });
        return transformedTransfers;
    }

    private filterAction = (option: IMapControlOption) => {
        option.value = !option.value;
        if (includes(this.toggledRegions, option.key)) {
            remove(this.toggledRegions, (region: string): boolean => region === option.key);
        } else {
            this.toggledRegions.push(option.key);
        }
        this.dmCrossBorderService.highlightRegion(this.toggledRegions);
    }

    private filterList(activityList: IPATransfer[], stringSearch: string) {
        this.activityListLookup = filter(activityList, (activity: IPATransfer): boolean => (activity.name.search(new RegExp(stringSearch, "i")) !== -1));
    }
}
