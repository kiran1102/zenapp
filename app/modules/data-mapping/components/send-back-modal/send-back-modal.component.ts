// 3rd Party
import { Component, Inject } from "@angular/core";

// Services
import { ModalService } from "sharedServices/modal.service";

// Redux
import { getModalData } from "oneRedux/reducers/modal.reducer";

// Tokens
import { StoreToken } from "tokens/redux-store.token";

// Interfaces
import { IStore } from "interfaces/redux.interface";
import { IDMSendBackModal } from "interfaces/dm-assessment.interface";

@Component({
    selector: "send-back-modal",
    templateUrl: "./send-back-modal.component.html",
})
export class SendBackModalComponent {

    comment = "";
    sendBackInProgress = false;
    modalData: IDMSendBackModal;

    constructor(
        @Inject(StoreToken) private store: IStore,
        private ModalService: ModalService,
    ) {}

    ngOnInit() {
        this.modalData = getModalData(this.store.getState());
    }

    closeModal(result: boolean): void {
        this.ModalService.handleModalCallback(result);
        this.ModalService.closeModal();
        this.ModalService.clearModalData();
    }

    sendBack(): void {
        this.sendBackInProgress = true;
        this.modalData.updateCallback(this.comment).then((response: { result: boolean }): void => {
            this.sendBackInProgress = false;
            this.closeModal(response && response.result);
        });
    }

}
