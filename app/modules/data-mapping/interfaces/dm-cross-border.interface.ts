// Interfaces
import { IStringMap } from "interfaces/generic.interface";

// Enums
import { Regions } from "modules/data-mapping/enums/dm-cross-border.enums";

export interface IRegions {
    [Regions.USA]: IStringMap<{fillKey: string}>;
    [Regions.EEARegion]: IStringMap<{fillKey: string}>;
    [Regions.AdequateProtection]: IStringMap<{fillKey: string}>;
}

export interface ILineOptions {
    greatArc: boolean;
    strokeWidth: number;
    strokeColor: string;
}

export interface ITransfer {
    destination?: ILocation;
    destinations?: number;
    pas?: IPATransfer[];
    processingActivities?: IPATransfer[];
    reverseDestinations?: number;
    reverseSources?: number;
    originAssetsCount?: number;
    destinationAssetsCount?: number;
    origin?: ILocation;
    source?: ILocation;
    sources?: number;
    strokeColor?: string;
    strokeWidth?: number;
    processingActivitiesCount?: number;
    greatArc?: boolean;
}

export interface IFormattedTransfer extends ITransfer {
    options: ILineOptions;
    origin?: ILocation;
}

export interface IPATransfer {
    destination?: number;
    id: string;
    name: string;
    reverseDestinationsCount?: number;
    reverseSourcesCount?: number;
    sourcesCount?: number;
    destinationsCount?: number;
    organization?: string;
    originAssetsCount?: number;
    destinationAssetsCount?: number;
}

export interface ICoordinates {
    latitude: number;
    longitude: number;
}

export interface ILocation extends ICoordinates {
    location?: string;
    locationId?: string;
    country?: string;
    locationKey?: string;
}

export interface ICrossBorderStateData extends ITransfer {
    drawerOpen: boolean;
}
