export interface IMapControlOption {
    legendColor: string;
    title: string;
    key: string;
    action: (option: IMapControlOption) => void;
    value: boolean;
}
