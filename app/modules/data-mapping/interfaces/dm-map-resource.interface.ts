export interface IMapResource {
    route: string,
    name: string,
    resourceKey: string,
    resourceUpgradeKey: string,
    id: number,
}