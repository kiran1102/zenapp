export interface IMapDataPoint {
    totalCount: number;
    internalCount: number;
    location: IMapLocation;
    thirdPartyCount: number;
    unAnsweredCount: number;
}

export interface IMapLocation {
    latitude: number;
    longitude: number;
    locationId: string;
    location: string;
    locationKey: string;
}
