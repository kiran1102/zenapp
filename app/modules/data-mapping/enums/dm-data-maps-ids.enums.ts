export enum DataMapsIds {
    AssetsByLocation = 1,
    Crossborder = 2,
    Lineage = 3,
}