export interface IOnboardingModalContent {
    title: string;
    content: string;
}
