// 3rd party
import { Component, Inject } from "@angular/core";

// Redux / Tokens
import { IStore } from "interfaces/redux.interface";
import { StoreToken } from "tokens/redux-store.token";
import { getModalData } from "oneRedux/reducers/modal.reducer";

// Services
import { OnboardingService } from "../../services/onboarding.service";

// Interfaces
import { IOnboardingModalContent } from "../../interfaces/onboarding.interface";

@Component({
    selector: "onboarding-modal",
    templateUrl: "./onboarding-modal.component.html",
})
export class OnboardingModal {
    modalData: IOnboardingModalContent = getModalData(this.store.getState());
    modalTitle = this.modalData.title;
    content = this.modalData.content;
    disabled = false;
    doNotShow = false;

    constructor(
        @Inject(StoreToken) private store: IStore,
        private onboardingService: OnboardingService,
    ) { }

    onToggle(show = false) {
        this.doNotShow = show;
    }

    closeModal(submit = false) {
        this.disabled = true;
        this.onboardingService.closeOnboardingModal(submit && this.doNotShow);
    }
}
