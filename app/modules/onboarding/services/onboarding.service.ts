// 3rd Party
import {
    Injectable,
    Inject,
} from "@angular/core";

// Services
import { Permissions } from "modules/shared/services/helper/permissions.service";
import { ModalService } from "sharedServices/modal.service";
import { UserActionService } from "oneServices/actions/user-action.service";
import { UserApiService } from "oneServices/api/user-api.service";

// rxjs
import { StoreToken } from "tokens/redux-store.token";
import { getCurrentUser } from "oneRedux/reducers/user.reducer";

// Interfaces
import { IStore } from "interfaces/redux.interface";
import { IUser } from "interfaces/user.interface.ts";

@Injectable()
export class OnboardingService {

    constructor(
        @Inject(StoreToken) private store: IStore,
        private permissions: Permissions,
        private modalService: ModalService,
        private userActionService: UserActionService,
        private userApiService: UserApiService,
    ) { }

    onboard(title: string, content: string) {
        const currentUser: IUser = getCurrentUser(this.store.getState());
        if (currentUser.Onboarded || !this.permissions.canShow("OnBoarding")) return;

        this.modalService.setModalData({ title, content });
        this.modalService.openModal("onboardingModal");
    }

    closeOnboardingModal(doNotShow = false) {
        if (doNotShow) {
            this.userApiService.onboardUser().then(() => {
                const currentUser: IUser = getCurrentUser(this.store.getState());
                this.userActionService.setCurrentUser({ ...currentUser, Onboarded: true });
                this.closeModal();
            });
        } else {
            this.closeModal();
        }
    }

    private closeModal() {
        this.modalService.closeModal();
        this.modalService.clearModalData();
    }

}
