// Angular
import { Injectable } from "@angular/core";

// Rxjs
import {
    Observable,
    from,
} from "rxjs";

// Interfaces
import {
    IProtocolConfig,
    IProtocolMessages,
    IProtocolResponse,
} from "interfaces/protocol.interface";
import { IAssessmentStatusCountResponse } from "interfaces/dashboard/assessment-status-count.interface";
import { IAssessmentRiskCountResponse } from "interfaces/dashboard/assessment-risk-count-graph.interface";
import { IAgingAssessmentDetailResponse } from "interfaces/dashboard/aging-assessment.interface";
import { IDashboardRiskSummaryResponse } from "interfaces/dashboard/dashboard-risk-summary.interface";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

// Services
import { ProtocolService } from "modules/core/services/protocol.service";

@Injectable()
export class DashboardAPI {

    constructor(
        private translatePipe: TranslatePipe,
        private readonly oneProtocol: ProtocolService,
    ) { }

    getAssessmentStatusCount(orgGroupId: string, templateId: string): Observable<IAssessmentStatusCountResponse | null> {
        const config: IProtocolConfig = this.oneProtocol.customConfig("GET", "/assessment-v2/v2/assessments/dashboard/status", { orgGroupId, templateId });
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("CouldNotGetAssessmentStatusCount") } };
        return from(this.oneProtocol.http(config, messages)
            .then((response: IProtocolResponse<IAssessmentStatusCountResponse>): IAssessmentStatusCountResponse | null => {
                return response.result ? response.data : null;
            }),
        );
    }

    getAgingAssessments(templateId: string): Observable<IAgingAssessmentDetailResponse> {
        const config: IProtocolConfig = this.oneProtocol.customConfig("GET", "/assessment-v2/v2/assessments/dashboard/age", { templateId });
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("CouldNotGetAgingAssessments") } };
        return from(this.oneProtocol.http(config, messages)
            .then((response: IProtocolResponse<IAgingAssessmentDetailResponse>): IAgingAssessmentDetailResponse | null => {
                return response.result ? response.data : null;
            }),
        );
    }

    getAssessmentRiskCounts(orgGroupId: string, templateId: string): Observable<IAssessmentStatusCountResponse | null> {
        const config: IProtocolConfig = this.oneProtocol.customConfig("GET", "/assessment-v2/v2/assessments/dashboard/openClosedRisk", { orgGroupId, templateId });
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("CouldNotGetAssessmentRiskCount") } };
        return from(this.oneProtocol.http(config, messages)
            .then((response: IProtocolResponse<IAssessmentRiskCountResponse>): IAssessmentRiskCountResponse | null => {
                return response.result ? response.data : null;
            }),
        );
    }

    getRiskSummaryCounts(orgFilterCriteria: string, riskStateFilter: string[], templateFilterCriteria: string): Observable<IDashboardRiskSummaryResponse | null> {
        const config: IProtocolConfig = this.oneProtocol.customConfig("POST", `/assessment-v2/v2/assessments/dashboard/risk`,
            null, { orgFilterCriteria, riskStateFilter, templateFilterCriteria });
        const messages: IProtocolMessages = {
                Error: { custom: this.translatePipe.transform("CouldNotGetRiskCount") },
            };
        return from(this.oneProtocol.http(config, messages)
            .then((response: IProtocolResponse<IDashboardRiskSummaryResponse>): IDashboardRiskSummaryResponse | null => {
                return response.result ? response.data : null;
            }),
        );
    }

    getFilterOptions(): Observable<IProtocolResponse<string[]>> {
        const config: IProtocolConfig = this.oneProtocol.customConfig("GET", "/api/risk-v2/v2/risks/status");
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("CouldNotLoadRiskStatus") } };
        return from(this.oneProtocol.http(config, messages));
    }

}
