// Angular
import { Injectable } from "@angular/core";

// 3rd party
import {
    each,
    sortBy,
} from "lodash";

// Interfaces
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import { IStringMap } from "interfaces/generic.interface";
import { IAssessmentStatusCount } from "interfaces/dashboard/assessment-status-count.interface";
import { IAssessmentRiskCountResponse } from "interfaces/dashboard/assessment-risk-count-graph.interface";
import {
    IAgingAssessmentDetail,
    IAssessmentAgingGraphData,
} from "interfaces/dashboard/aging-assessment.interface";
import { IOrganization } from "interfaces/org.interface";
import { IOneDonutGraphData } from "interfaces/one-donut-graph.inetrface";

// Constants
import {
    AssessmentStatusKeys,
} from "constants/assessment.constants";
import { AssessmentStatusColors } from "constants/dashboard/assessment-status-color.constant";
import { AssessmentRiskCountStatus } from "constants/dashboard/assessment-states-order.constant";
import {
    IDashboardRiskSummaryResponse,
    IAssessmentRiskSummaryDetails,
    IRiskSummaryGraphData,
} from "interfaces/dashboard/dashboard-risk-summary.interface";

// Enums
import { AssessmentStatus } from "enums/assessment/assessment-status.enum";
import {
    RiskLevelTags,
    RiskLevelOrder,
} from "constants/risk-level.constant";

// Services
import { TimeStamp } from "oneServices/time-stamp.service";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

@Injectable()
export class DashboardDataService {

    constructor(
        private translatePipe: TranslatePipe,
        private readonly timeStamp: TimeStamp,
    ) { }

    getAssessmentStatusChartData(statusData: IAssessmentStatusCount[], totalCount: number): IOneDonutGraphData[] {
        const statusGraphData: IOneDonutGraphData[] = [];
        each(statusData, (data: IAssessmentStatusCount): void => {
            statusGraphData.push({
                status: data.status,
                count: data.count,
                percentage: 0,
                magnitude: 0,
                color: AssessmentStatusColors[AssessmentStatusKeys[data.status]],
                legendLabel: this.translatePipe.transform(AssessmentStatusKeys[data.status]),
            });
        });
        return this.calculatePercentage(statusGraphData, "count", totalCount, true);
    }

    getAssessmentRiskGraphCounts(data: IAssessmentRiskCountResponse): IOneDonutGraphData[] {
        const riskCountsData: IOneDonutGraphData[] = [];
        each(data.assessments, (project: IAssessmentStatusCount): void => {
            switch (project.status) {
                case AssessmentRiskCountStatus.Open:
                    riskCountsData.push({
                        legendLabel: this.translatePipe.transform("OpenRisk").toString(),
                        color: "#BE2F2B",
                        count: project.count,
                        percentage: 0,
                        magnitude: 0,
                    });
                    break;
                case AssessmentRiskCountStatus.Closed:
                    riskCountsData.push({
                        legendLabel: this.translatePipe.transform("ClosedNoRisk").toString(),
                        color: "#6CC04A",
                        count: project.count,
                        percentage: 0,
                        magnitude: 0,
                    });
                    break;
            }
        });
        return this.calculatePercentage(riskCountsData, "count", data.totalCount, false);
    }

    populateAgingGraphData(orgGroups: IStringMap<IOrganization>, assessments: IAgingAssessmentDetail[]): IAssessmentAgingGraphData[] {
        const dayMS: number = 24 * 60 * 60 * 1000;
        const graphData: IAssessmentAgingGraphData[] = [];
        each(assessments, (assessment: IAgingAssessmentDetail): void => {
            if (assessment.status && assessment.status !== AssessmentStatus.Completed) {
                const duration = this.timeStamp.getMillisecondsSince(assessment.createDate);
                graphData.push({
                    Day: duration / dayMS,
                    Id: assessment.id,
                    Name: assessment.name,
                    OrgGroup: orgGroups[assessment.orgGroupId].Name,
                    StateDesc: AssessmentStatusKeys[assessment.status],
                    StateName: this.translatePipe.transform(AssessmentStatusKeys[assessment.status]),
                    Time: duration,
                });
            }
        });
        return graphData;
    }

    populateRiskSummaryGraphData(riskData: IDashboardRiskSummaryResponse): any {
        let riskGraphData: IRiskSummaryGraphData[] = [];
        each(riskData.risks, (risk: IAssessmentRiskSummaryDetails, index: number): void => {
            if (risk.count > 0) {
                const riskIndex = riskGraphData.findIndex((option) => option.level.toLowerCase() === RiskLevelTags[risk.level].toLowerCase());
                if (riskIndex > -1) {
                    riskGraphData.splice(riskIndex, 1, {
                        id: RiskLevelOrder[RiskLevelTags[risk.level]],
                        level: RiskLevelTags[risk.level],
                        label: this.translatePipe.transform(RiskLevelTags[risk.level]),
                        count: riskGraphData[riskIndex].count + risk.count,
                        percentage: 0,
                        magnitude: 0,
                    });
                } else {
                    riskGraphData.push({
                        id: RiskLevelOrder[RiskLevelTags[risk.level]],
                        level: RiskLevelTags[risk.level],
                        label: this.translatePipe.transform(RiskLevelTags[risk.level]),
                        count: risk.count,
                        percentage: 0,
                        magnitude: 0,
                    });
                }
            }
        });
        riskGraphData = sortBy(riskGraphData, (data: any): number => RiskLevelOrder[data.level]);
        return this.calculatePercentage(riskGraphData, "count", riskData.totalCount, true);
    }

    private calculatePercentage(graphData: any[], keyName: string, totalCount: number, decimalRequired: boolean): any[] {
        let maxPercentage = 0;
        let maxIndex = 0;
        let totalPercentage = 0;
        each(graphData, (data: IOneDonutGraphData, index: number): void => {
            if (decimalRequired) {
                data.percentage = Math.round((data[keyName] / totalCount * 100) * 10) / 10;
            } else {
                data.percentage = Math.round(data[keyName] / totalCount * 100);
            }
            data.magnitude = data.percentage;
            if (data.percentage > maxPercentage) {
                maxPercentage = data.percentage;
                maxIndex = index;
            }
            totalPercentage += data.percentage;
        });
        if (decimalRequired) {
            graphData[maxIndex].percentage = Math.round((graphData[maxIndex].percentage + (100 - totalPercentage)) * 10) / 10;
        } else {
            graphData[maxIndex].percentage += 100 - totalPercentage;
        }
        graphData[maxIndex].magnitude = graphData[maxIndex].percentage;
        return graphData;
    }
}
