// Services
import { Permissions } from "modules/shared/services/helper/permissions.service";
import { GlobalSidebarService } from "modules/global-sidebar/services/one-global-sidebar.service";

// Interfaces
import { IStringMap } from "interfaces/generic.interface";

export function dashboardLegacyRoutes($stateProvider: ng.ui.IStateProvider): void {
    $stateProvider
        .state("zen.app.pia.module.dashboardV2", {
            abstract: true,
            url: "dashboard-v2",
            resolve: {
                DashboardPermission: ["GeneralData", "Permissions",
                    (GeneralData: IStringMap<any>, permissions: Permissions): boolean => {
                        if (permissions.canShow("ViewDashboards")) {
                            return true;
                        }
                        permissions.goToFallback();
                        return false;
                    },
                ],
            },
            views: {
                module: {
                    template: "<ui-view></ui-view>",
                    controller: [
                        "GlobalSidebarService",
                        (globalSidebarService: GlobalSidebarService) => {
                            globalSidebarService.setAAMenu();
                        },
                    ],
                },
            },
        })
        // AA Dashboard
        .state("zen.app.pia.module.dashboardV2.list", {
            url: "",
            template: "<downgrade-dashboard-wrapper></downgrade-dashboard-wrapper>",
        })
        // DM dashboard(copy of AA dashboard)
        .state("zen.app.pia.module.datamap.views.dashboardv2", {
            url: "dashboard-v2",
            template: "<downgrade-dashboard-wrapper></downgrade-dashboard-wrapper>",
        });
}

dashboardLegacyRoutes.$inject = ["$stateProvider"];
