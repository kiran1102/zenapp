import { NgModule } from "@angular/core";
import { SharedModule } from "sharedModules/shared.module";
import { UpgradedLegacyModule } from "sharedModules/upgrade/upgraded-legacy.module";
import { CommonModule } from "@angular/common";
import { HttpClientModule } from "@angular/common/http";
import { FormsModule } from "@angular/forms";
import { ReactiveFormsModule } from "@angular/forms";
import { VitreusModule } from "@onetrust/vitreus";
import { DndListModule } from "ngx-drag-and-drop-lists";
import { OTPipesModule, PIPES } from "modules/pipes/pipes.module";
import { VirtualScrollModule } from "od-virtualscroll";
import { OneVideoModule } from "modules/video/video.module";

// Components
import { DashboardWrapperComponent } from "modules/dashboard/components/dashboard-wrapper/dashboard-wrapper.component";
import { AssessmentStatusCountComponent } from "modules/dashboard/components/assessment-status-count/assessment-status-count.component";
import { AssessmentStatusDonutGraphComponent } from "modules/dashboard/components/assessment-status-donut-graph/assessment-status-donut-graph.component";
import { AssessmentRiskCountGraphComponent } from "modules/dashboard/components/assessment-risk-count-graph/assessment-risk-count-graph.component";
import { AssessmentAgingGraphComponent } from "modules/dashboard/components/assessment-aging-graph/assessment-aging-graph.component";
import { AssessmentStatusWrapperComponent } from "modules/dashboard/components/assessment-status-wrapper/assessment-status-wrapper.component";
import { AssessmentRiskCountWrapperComponent } from "modules/dashboard/components/assessment-risk-count-graph/assessment-risk-count-graph-wrapper.component";
import { AssessmentAgingGraphWrapperComponent } from "modules/dashboard/components/assessment-aging-graph/assessment-aging-graph-wrapper.component";
import { RiskSummaryGraphWrapperComponent } from "modules/dashboard/components/risk-summary-graph/risk-summary-graph-wrapper.component";
import { RiskSummaryGraphComponent } from "modules/dashboard/components/risk-summary-graph/risk-summary-graph.component";

// Services
import { DashboardDataService } from "modules/dashboard/services/action/dashboard-data.service";
import { DashboardAPI } from "modules/dashboard/services/api/dashboard-api.service";

@NgModule({
    imports: [
        SharedModule,
        UpgradedLegacyModule,
        CommonModule,
        HttpClientModule,
        FormsModule,
        ReactiveFormsModule,
        VitreusModule,
        DndListModule,
        OTPipesModule,
        VirtualScrollModule,
        OneVideoModule,
    ],
    declarations: [
        DashboardWrapperComponent,
        AssessmentStatusCountComponent,
        AssessmentStatusDonutGraphComponent,
        AssessmentRiskCountGraphComponent,
        AssessmentRiskCountGraphComponent,
        AssessmentAgingGraphComponent,
        AssessmentStatusWrapperComponent,
        AssessmentRiskCountWrapperComponent,
        AssessmentAgingGraphWrapperComponent,
        RiskSummaryGraphComponent,
        RiskSummaryGraphWrapperComponent,
    ],
    entryComponents: [
        DashboardWrapperComponent,
    ],
    providers: [
        DashboardDataService,
        DashboardAPI,
    ],
})

export class DashboardModule { }
