declare const angular: any;

// Routes
import { dashboardLegacyRoutes } from "./dashboard.routes";

export const dashboardLegacyModule = angular
    .module("dashboard-module", ["ui.router"])
    .config(dashboardLegacyRoutes);
