// Angular
import {
    Component,
    Input,
    Inject,
    Output,
    EventEmitter,
    OnInit,
} from "@angular/core";

// Constants / Enums
import { TemplateTypes } from "constants/template-types.constant";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

// Interfaces
import { IOneDonutGraphData } from "interfaces/one-donut-graph.inetrface";
import { IOrganization } from "interfaces/org.interface";
import { ICustomTemplateDetails } from "modules/template/interfaces/custom-templates.interface";
import { ITemplateNameValue } from "modules/template/interfaces/template.interface";

// Services
import { TemplateAPI } from "templateServices/api/template-api.service";

@Component({
    selector: "assessment-status-donut-graph",
    templateUrl: "./assessment-status-donut-graph.component.html",
})

export class AssessmentStatusDonutGraphComponent implements OnInit {
    @Input() canLoadPage = false;
    @Input() isEmptyData = false;
    @Input() graphData: IOneDonutGraphData[] = [];
    @Input() orgList: IOrganization[];

    @Output() donutClickCallback = new EventEmitter();
    @Output() onOrgChange = new EventEmitter();
    @Output() onTemplateChange = new EventEmitter();

    selectedOrg: IOrganization;
    templateList: ITemplateNameValue[];
    selectedTemplate: ITemplateNameValue;

    constructor(
        @Inject("TemplateAPI") private templateAPI: TemplateAPI,
        private translate: TranslatePipe,
    ) { }

    ngOnInit(): void {
        this.selectedOrg = this.orgList[0];
        this.templateAPI.getAssessmentPublishedTemplates(`${TemplateTypes.PIA},${TemplateTypes.VENDOR}`)
            .then((response: ITemplateNameValue[]): void => {
                this.templateList = response;
                this.templateList.unshift({ id: "", name: this.translate.transform("NoFilter") });
                this.selectedTemplate = this.templateList[0];
            });
    }

    onOrgselection(selection: IOrganization): void {
        this.selectedOrg = selection;
        this.onOrgChange.emit(selection);
    }

    onTemplateSelection(template: ICustomTemplateDetails) {
        this.selectedTemplate = template;
        this.onTemplateChange.emit(template);
    }
}
