// Angular
import {
    Component,
    Input,
    Inject,
    Output,
    EventEmitter,
    OnInit,
    OnChanges,
    SimpleChanges,
} from "@angular/core";

// 3rd party
import { each } from "lodash";

// Interfaces
import { IOrganization } from "interfaces/org.interface";
import { IOneDonutGraphData } from "interfaces/one-donut-graph.inetrface";
import { ITemplateNameValue } from "modules/template/interfaces/template.interface";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

@Component({
    selector: "assessment-risk-count-graph",
    templateUrl: "./assessment-risk-count-graph.component.html",
})

export class AssessmentRiskCountGraphComponent implements OnInit, OnChanges {
    @Input() canLoadPage = false;
    @Input() graphData: IOneDonutGraphData[] = [];
    @Input() orgList: IOrganization[];
    @Input() templateList: ITemplateNameValue[];
    @Input() totalRisks = 0;
    @Output() onFilterUpdate = new EventEmitter();
    selectedOrg: IOrganization;
    selectedTemplate: ITemplateNameValue;

    constructor(
        private translatePipe: TranslatePipe,
    ) { }

    ngOnInit(): void {
        this.selectedOrg = this.orgList[0];
    }

    ngOnChanges(changes: SimpleChanges): void {
        if (changes.templateList && changes.templateList.currentValue) {
            this.templateList = changes.templateList.currentValue;
            this.templateList.unshift({ id: "", name: this.translatePipe.transform("NoFilter") });
            this.selectedTemplate = this.templateList[0];
        }
    }

    onTemplateChange(currentValue: ITemplateNameValue): void {
        this.selectedTemplate = currentValue;
        this.onFilterUpdate.emit({
            orgId: this.selectedOrg.Id || null,
            templateId: this.selectedTemplate ? this.selectedTemplate.id : null,
        });
    }

    onOrgChange(currentValue: IOrganization): void {
        this.selectedOrg = currentValue;
        this.onFilterUpdate.emit({
            orgId: this.selectedOrg.Id || null,
            templateId: this.selectedTemplate.id || null,
        });
    }

}
