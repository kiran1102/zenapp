// Angular
import {
    Component,
    Inject,
    OnInit,
    Input,
    OnDestroy,
} from "@angular/core";

// 3rd party
import {
    each,
    sortBy,
} from "lodash";

// Interfaces
import {
    IAssessmentStatusCountResponse,
    IAssessmentStatusCount,
} from "interfaces/dashboard/assessment-status-count.interface";
import { IOrganization } from "interfaces/org.interface";
import { IStringMap } from "interfaces/generic.interface";
import { ITemplateNameValue } from "modules/template/interfaces/template.interface";
import { IAssessmentRiskCountResponse } from "interfaces/dashboard/assessment-risk-count-graph.interface";
import { IOneDonutGraphData } from "interfaces/one-donut-graph.inetrface";
import { IStore } from "interfaces/redux.interface";

// Constants
import { AssessmentStatusKeys } from "constants/assessment.constants";
import { AssessmentStatesOrder } from "constants/dashboard/assessment-states-order.constant";
import { TemplateTypes } from "constants/template-types.constant";
import { StoreToken } from "tokens/redux-store.token";

// Enums
import { AssessmentStatus } from "enums/assessment/assessment-status.enum";

// Redux
import { getCurrentOrgId } from "oneRedux/reducers/org.reducer";

// Rxjs
import { Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";

// Services
import { DashboardDataService } from "modules/dashboard/services/action/dashboard-data.service";
import { DashboardAPI } from "modules/dashboard/services/api/dashboard-api.service";
import { TemplateAPI } from "templateServices/api/template-api.service";

@Component({
    selector: "assessment-risk-count-wrapper",
    template: `
            <assessment-risk-count-graph
                class="flex-full-100 row-nowrap"
                [graphData]="assessmentRiskCountGraphData"
                [orgList]="orgList"
                [templateList]="templateList"
                [canLoadPage]="canLoadPage"
                [totalRisks]="totalRisks"
                (onFilterUpdate)="onRiskCountFilterUpdate($event)"
            ></assessment-risk-count-graph>
            `,
})

export class AssessmentRiskCountWrapperComponent implements OnInit {
    @Input() orgList: IOrganization[];
    assessmentRiskCountGraphData: IOneDonutGraphData[];
    canLoadPage = false;
    templateList: ITemplateNameValue[];
    totalRisks: number;
    destroy$ = new Subject();

    constructor(
        @Inject(StoreToken) private store: IStore,
        @Inject("TemplateAPI") private templateAPI: TemplateAPI,
        private dashboardAPI: DashboardAPI,
        private dashboardDataService: DashboardDataService,
    ) { }

    ngOnInit(): void {
        this.getAssessmentRiskCounts(getCurrentOrgId(this.store.getState()));
        this.populateRiskTemplates();
    }

    onRiskCountFilterUpdate(selection: { ["orgId"]: string, ["templateId"]: string }): void {
        this.getAssessmentRiskCounts(selection.orgId, selection.templateId);
    }

    private getAssessmentRiskCounts(orgGroupId: string, templateId: string = null): void {
        this.canLoadPage = false;
        this.dashboardAPI.getAssessmentRiskCounts(orgGroupId, templateId)
            .pipe(
                takeUntil(this.destroy$),
            ).subscribe((response: IAssessmentRiskCountResponse | null): void => {
                if (response && response.totalCount > 0) {
                    this.totalRisks = response.totalCount;
                    this.assessmentRiskCountGraphData = this.dashboardDataService.getAssessmentRiskGraphCounts(response);
                } else {
                    this.totalRisks = 0;
                    this.assessmentRiskCountGraphData = [];
                }
                this.canLoadPage = true;
            });
    }

    private populateRiskTemplates(): void {
        this.templateAPI.getAssessmentPublishedTemplates(`${TemplateTypes.PIA},${TemplateTypes.VENDOR}`)
            .then((response: ITemplateNameValue[]): void => {
                this.templateList = response;
            });
    }
}
