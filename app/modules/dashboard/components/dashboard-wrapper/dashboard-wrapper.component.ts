// Angular
import {
    Component,
    Inject,
    OnInit,
} from "@angular/core";

// Constants
import { StoreToken } from "tokens/redux-store.token";

// Redux
import {
    getOrgList,
    getCurrentOrgId,
} from "oneRedux/reducers/org.reducer";

// Interfaces
import { IStore } from "interfaces/redux.interface";
import { IOrganization } from "interfaces/org.interface";

// Service
import { Permissions } from "modules/shared/services/helper/permissions.service";

@Component({
    selector: "dashboard-wrapper",
    templateUrl: "./dashboard-wrapper.component.html",
})

export class DashboardWrapperComponent implements OnInit {
    orgList: IOrganization[];

    constructor(
        @Inject(StoreToken) private store: IStore,
        private permissions: Permissions,

    ) { }

    ngOnInit(): void {
        this.orgList = getOrgList(getCurrentOrgId(this.store.getState()))(this.store.getState());
    }
}
