// Angular
import {
    Component,
    Input,
    Inject,
    OnInit,
} from "@angular/core";
import { StateService } from "@uirouter/core";
import { keyBy } from "lodash";

// Interfaces
import {
    ILabelValue,
    IStringMap,
} from "interfaces/generic.interface";
import {
    IAgingAssessmentDetailResponse,
    IAssessmentAgingGraphData,
} from "interfaces/dashboard/aging-assessment.interface";
import { IOrganization } from "interfaces/org.interface";
import { ITemplateNameValue } from "modules/template/interfaces/template.interface";

// Rxjs
import { Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";

// Services
import { DashboardDataService } from "modules/dashboard/services/action/dashboard-data.service";
import { DashboardAPI } from "modules/dashboard/services/api/dashboard-api.service";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

@Component({
    selector: "assessment-aging-graph-wrapper",
    template: `
        <assessment-aging-graph
            class="flex-full-100 row-nowrap"
            [graphData]="ageGraphData"
            [canLoadPage]="canLoadPage"
            [ageCriteriaList]="ageCriteriaList"
            (assessmentCallback)="goToAssessment($event)"
            (onTemplateChange)="onTemplateChange($event)"
        ></assessment-aging-graph>
    `,
})

export class AssessmentAgingGraphWrapperComponent implements OnInit {
    @Input() orgList: IOrganization[] = [];
    ageGraphData: IAssessmentAgingGraphData[] = [];
    ageCriteriaList: Array<ILabelValue<number>> = [];
    canLoadPage = false;
    selectedTemplate: ITemplateNameValue;
    destroy$ = new Subject();

    constructor(
        private stateService: StateService,
        private translatePipe: TranslatePipe,
        private dashboardAPI: DashboardAPI,
        private dashboardDataService: DashboardDataService,
    ) { }

    ngOnInit(): void {
        this.ageCriteriaList = [
            {
                label: this.translatePipe.transform("ShowAll"),
                value: null,
            }, {
                label: this.translatePipe.transform("Last15Days"),
                value: 15,
            }, {
                label: this.translatePipe.transform("Last30Days"),
                value: 30,
            }, {
                label: this.translatePipe.transform("Last60Days"),
                value: 60,
            }, {
                label: this.translatePipe.transform("Last90Days"),
                value: 90,
            },
        ];
        this.getAgingAssessments(this.selectedTemplate);
    }

    goToAssessment(assessmentData: { ["id"]: string }): void {
        this.stateService.go("zen.app.pia.module.assessment.detail", {
            assessmentId: assessmentData.id,
        });
    }

    onTemplateChange($event) {
        this.selectedTemplate = $event;
        this.getAgingAssessments(this.selectedTemplate);
    }

    private getAgingAssessments(selectedTemplate: ITemplateNameValue): void {
        this.canLoadPage = false;
        this.dashboardAPI.getAgingAssessments(selectedTemplate ? selectedTemplate.id || null : null)
            .pipe(
                takeUntil(this.destroy$),
            ).subscribe((response: IAgingAssessmentDetailResponse): void => {
                if (response) {
                    const orgGroups: IStringMap<IOrganization> = keyBy(this.orgList, "Id");
                    this.ageGraphData = this.dashboardDataService.populateAgingGraphData(orgGroups, response.content);
                }
                this.canLoadPage = true;
            });
    }
}
