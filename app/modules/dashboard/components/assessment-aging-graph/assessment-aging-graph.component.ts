// Angular
import {
    Component,
    EventEmitter,
    Inject,
    Input,
    OnInit,
    Output,
    ViewChild,
} from "@angular/core";

// Interfaces
import { ILabelValue } from "interfaces/generic.interface";
import { IAssessmentAgingGraphData } from "interfaces/dashboard/aging-assessment.interface";
import { ITemplateNameValue } from "modules/template/interfaces/template.interface";

// Constants
import { AssessmentStateKeys } from "constants/assessment.constants";
import { TemplateTypes } from "constants/template-types.constant";

// Services
import { TemplateAPI } from "templateServices/api/template-api.service";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

// Components
import { OneAgingGraph } from "sharedModules/components/one-aging-graph/one-aging-graph.component";

@Component({
    selector: "assessment-aging-graph",
    templateUrl: "./assessment-aging-graph.component.html",
})

export class AssessmentAgingGraphComponent implements OnInit {
    @Input() graphData: IAssessmentAgingGraphData[] = [];
    @Input() canLoadPage = false;
    @Input() ageCriteriaList: Array<ILabelValue<number>> = [];
    @Output() assessmentCallback = new EventEmitter();
    @Output() onTemplateChange = new EventEmitter();
    currentAssessmentDetails: Array<{ ["title"]: string, ["value"]: string }> = null;
    assessmentStates: typeof AssessmentStateKeys;
    selectedAge: ILabelValue<number>;
    templateList: ITemplateNameValue[];
    selectedTemplate: ITemplateNameValue;

    @ViewChild("assessmentAgingGraph")
    private oneAgingGraphComponent: OneAgingGraph;

    constructor(
        @Inject("TemplateAPI") private templateAPI: TemplateAPI,
        private translate: TranslatePipe,
    ) { }

    ngOnInit(): void {
        this.selectedAge = this.ageCriteriaList[0];
        this.assessmentStates = AssessmentStateKeys;
        this.templateAPI.getAssessmentPublishedTemplates(`${TemplateTypes.PIA},${TemplateTypes.VENDOR}`)
            .then((response: ITemplateNameValue[]): void => {
                this.templateList = response;
                this.templateList.unshift({ id: "", name: this.translate.transform("NoFilter") });
                this.selectedTemplate = this.templateList[0];
            });
    }

    ngOnChanges(): void {
        this.onAgeCriteriaChange(this.selectedAge);
    }

    onTemplateSelection(template: ITemplateNameValue) {
        this.selectedTemplate = template;
        this.onTemplateChange.emit(this.selectedTemplate);
    }

    showAssessmentDetails(assessmentData: { value: Array<{ title: string, value: string }> }): void {
        this.currentAssessmentDetails = assessmentData.value.length ? assessmentData.value : null;
    }

    onAgeCriteriaChange(selection: ILabelValue<number>): void {
        this.selectedAge = selection;
        const value = selection && selection.value ? selection.value : "";

        if (!this.oneAgingGraphComponent) return;
        // Re-renders the aging graph on criteria update
        this.oneAgingGraphComponent.updateAgingGraph(value);
    }
}
