// Angular
import {
    Component,
    EventEmitter,
    Inject,
    Input,
    OnInit,
    Output,
} from "@angular/core";
import { each } from "lodash";

// Interfaces
import { IOrganization } from "interfaces/org.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";
import { IRiskStatusFilter } from "interfaces/risk.interface";
import { IRiskSummaryGraphData } from "interfaces/dashboard/dashboard-risk-summary.interface";
import { ITemplateNameValue } from "modules/template/interfaces/template.interface";

// Constants
import { RiskV2StatusFilterConfig } from "constants/dashboard/risk-status-filter-config.constant";
import { TemplateTypes } from "constants/template-types.constant";

// Services
import { TemplateAPI } from "templateServices/api/template-api.service";
import { DashboardAPI } from "modules/dashboard/services/api/dashboard-api.service";

// Rxjs
import { Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

@Component({
    selector: "risk-summary-graph",
    templateUrl: "./risk-summary-graph.component.html",
})

export class RiskSummaryGraphComponent implements OnInit {
    @Input() canLoadPage = false;
    @Input() graphData: IRiskSummaryGraphData[] = [];
    @Input() orgList: IOrganization[];
    @Output() onFilterUpdate = new EventEmitter();

    allRiskStatesEnabled = false;
    canShowStatusDropdown: boolean;
    riskFilters: IRiskStatusFilter[] = [];
    selectedOrg: IOrganization;
    selectedRiskStates: string[] = [];
    selectedTemplate: ITemplateNameValue;
    riskV2Status = RiskV2StatusFilterConfig;
    availableRiskV2Status: string[] = [];
    templateList: ITemplateNameValue[];
    destroy$ = new Subject();

    constructor(
        private translatePipe: TranslatePipe,
        private dashboardAPI: DashboardAPI,
        @Inject("TemplateAPI") private templateAPI: TemplateAPI,
    ) { }

    ngOnInit(): void {
        this.selectedOrg = this.orgList[0];
        this.populateRiskFilterOptions();
        this.populateTemplatesFilter();
    }

    onOrgChange(selection: IOrganization): void {
        this.selectedOrg = selection;
        this.onFilterUpdate.emit({
            orgGroupId: this.selectedOrg.Id,
            riskStates: this.selectedRiskStates,
            templateId: this.selectedTemplate ? this.selectedTemplate.id : null,
        });
    }

    onTemplateChange(currentValue: ITemplateNameValue): void {
        this.selectedTemplate = currentValue;
        this.onFilterUpdate.emit({
            orgGroupId: this.selectedOrg.Id,
            riskStates: this.selectedRiskStates,
            templateId: this.selectedTemplate ? this.selectedTemplate.id : null,
        });
    }

    toggleStatusDropdown(value: boolean): void {
        this.canShowStatusDropdown = value;
    }

    toggleAllRiskStateFilters(): void {
        this.riskFilters = this.riskFilters.map((filter: IRiskStatusFilter): IRiskStatusFilter => {
            filter.Filtered = !this.allRiskStatesEnabled;
            return filter;
        });
        this.allRiskStatesEnabled = this.checkIfAllEnabled();
        this.updateGraphData();
    }

    toggleRiskStateFilter(column: IRiskStatusFilter): void {
        this.riskFilters = this.riskFilters.map((filter: IRiskStatusFilter): IRiskStatusFilter => {
            if (filter.lookupKey === column.lookupKey) {
                filter.Filtered = !filter.Filtered;
            }
            return filter;
        });
        this.allRiskStatesEnabled = this.checkIfAllEnabled();
        this.updateGraphData();
    }

    private populateTemplatesFilter() {
        this.templateAPI.getAssessmentPublishedTemplates(`${TemplateTypes.PIA},${TemplateTypes.VENDOR}`)
            .then((response: ITemplateNameValue[]): void => {
                this.templateList = response;
                this.templateList.unshift({ id: null, name: this.translatePipe.transform("NoFilter") });
                this.selectedTemplate = this.templateList[0];
            });
    }

    private populateRiskFilterOptions() {
        this.dashboardAPI.getFilterOptions()
            .pipe(
                takeUntil(this.destroy$),
            ).subscribe((response: IProtocolResponse<string[]>): boolean => {
                if (response.result) {
                    this.availableRiskV2Status = response.data;
                    this.buildFilters();
                    return;
                }
                return false;
            });
    }

    private buildFilters(): void {
        each(this.availableRiskV2Status, (status: string) => {
            if (status !== "ARCHIVED_IN_VERSION") {
                this.riskFilters.push({
                Filtered: true,
                value: status,
                label: this.translatePipe.transform(this.riskV2Status[status].label),
                lookupKey: this.riskV2Status[status].numericValue,
                });
            }
        });
        this.allRiskStatesEnabled = true;
    }

    private checkIfAllEnabled(): boolean {
        return this.riskFilters.reduce((allEnabled: boolean, filter: IRiskStatusFilter): boolean => allEnabled && filter.Filtered, true);
    }

    private updateGraphData(): void {
        this.selectedRiskStates = [];
        each(this.riskFilters, (filter: IRiskStatusFilter): void => {
            if (filter.Filtered) {
                this.selectedRiskStates.push(filter.value);
            }
        });
        if (!this.selectedRiskStates.length) {
            this.graphData = [];
            return;
        }
        this.onFilterUpdate.emit({
            orgGroupId: this.selectedOrg.Id,
            riskStates: this.selectedRiskStates,
            templateId: this.selectedTemplate ? this.selectedTemplate.id : null,
        });
    }
}
