// Angular
import {
    Component,
    Inject,
    OnInit,
    Input,
} from "@angular/core";

// Interfaces
import { IOrganization } from "interfaces/org.interface";
import {
    IDashboardRiskSummaryResponse,
    IRiskSummaryGraphData,
} from "interfaces/dashboard/dashboard-risk-summary.interface";
import { IStore } from "interfaces/redux.interface";

// Constants
import { StoreToken } from "tokens/redux-store.token";

// Enums
import { RiskV2States } from "enums/riskV2.enum";

// Redux
import { getCurrentOrgId } from "oneRedux/reducers/org.reducer";

// Rxjs
import { Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";

// Services
import { DashboardDataService } from "modules/dashboard/services/action/dashboard-data.service";
import { DashboardAPI } from "modules/dashboard/services/api/dashboard-api.service";

@Component({
    selector: "risk-summary-graph-wrapper",
    template: `
        <risk-summary-graph
            class="flex-full-100 row-nowrap"
            [canLoadPage]="canLoadPage"
            [graphData]="riskGraphData"
            [orgList]="orgList"
            (onFilterUpdate)="onRiskFiltersUpdate($event)"
        >
        </risk-summary-graph>
    `,
})

export class RiskSummaryGraphWrapperComponent implements OnInit {
    @Input() orgList: IOrganization[];
    canLoadPage = false;
    riskGraphData: IRiskSummaryGraphData[] = [];
    destroy$ = new Subject();

    constructor(
        @Inject(StoreToken) private store: IStore,
        private dashboardAPI: DashboardAPI,
        private dashboardDataService: DashboardDataService,
    ) { }

    ngOnInit(): void {
        this.getRiskSummaryCount(getCurrentOrgId(this.store.getState()));
    }

    public onRiskFiltersUpdate(selection: { orgGroupId: string, riskStates: string[], templateId: string }): void {
        const riskStates: string = selection.riskStates.length ? selection.riskStates.join() : null;
        this.getRiskSummaryCount(selection.orgGroupId, riskStates, selection.templateId);
    }

    private getRiskSummaryCount(orgGroupId: string, riskStates: string = null, templateId: string = ""): void {
        if (!riskStates) {
            riskStates = `${RiskV2States.IDENTIFIED},${RiskV2States.RECOMMENDATION_ADDED},` +
                `${[RiskV2States.RECOMMENDATION_SENT]},${[RiskV2States.REMEDIATION_PROPOSED]},` +
                `${[RiskV2States.EXCEPTION_REQUESTED]},${[RiskV2States.REDUCED]},${[RiskV2States.RETAINED]}`;
        }
        this.canLoadPage = false;
        this.dashboardAPI.getRiskSummaryCounts(orgGroupId, riskStates.split(","), templateId)
            .pipe(
                takeUntil(this.destroy$),
            ).subscribe((response: IDashboardRiskSummaryResponse): void => {
                if (response && response.totalCount > 0) {
                    this.riskGraphData = this.dashboardDataService.populateRiskSummaryGraphData(response);
                } else {
                    this.riskGraphData = [];
                }
                this.canLoadPage = true;
            });
    }
}
