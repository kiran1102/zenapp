// Angular
import {
    Component,
    Inject,
    OnInit,
    Input,
    OnDestroy,
} from "@angular/core";
import { StateService } from "@uirouter/core";

import { Subject } from "rxjs";

import { takeUntil } from "rxjs/operators";

// 3rd party
import {
    each,
    sortBy,
} from "lodash";

// Interfaces
import { ICustomTemplateDetails } from "modules/template/interfaces/custom-templates.interface";
import {
    IAssessmentStatusCountResponse,
    IAssessmentStatusCount,
} from "interfaces/dashboard/assessment-status-count.interface";
import { IOrganization } from "interfaces/org.interface";
import { IStringMap } from "interfaces/generic.interface";
import { IOneDonutGraphData } from "interfaces/one-donut-graph.inetrface";
import { IStore } from "interfaces/redux.interface";

// Constants
import { AssessmentStatusKeys } from "constants/assessment.constants";
import { AssessmentStatesOrder } from "constants/dashboard/assessment-states-order.constant";
import { StoreToken } from "tokens/redux-store.token";

// Enums
import { AssessmentStatus } from "enums/assessment/assessment-status.enum";

// Redux
import { getCurrentOrgId } from "oneRedux/reducers/org.reducer";

// Services
import { DashboardDataService } from "modules/dashboard/services/action/dashboard-data.service";
import { DashboardAPI } from "modules/dashboard/services/api/dashboard-api.service";

@Component({
    selector: "assessment-status-wrapper",
    templateUrl: "./assessment-status-wrapper.component.html",
})

export class AssessmentStatusWrapperComponent implements OnInit {
    @Input() orgList: IOrganization[] = [];
    assessmentStatusCount: IAssessmentStatusCount[] = [];
    assessmentStatusGraphData: IOneDonutGraphData[] = [];
    canLoadPage = false;
    selectedOrgGroupId: string;
    selectedTemplateId: string;
    totalCount = 0;
    destroy$ = new Subject();

    constructor(
        private stateService: StateService,
        @Inject(StoreToken) private store: IStore,
        private dashboardAPI: DashboardAPI,
        private dashboardDataService: DashboardDataService,
    ) { }

    ngOnInit(): void {
        this.populateAssessmentStatusData(getCurrentOrgId(this.store.getState()));
        this.selectedOrgGroupId = this.orgList[0].Id;
    }

    onDonutClickCallback(data: { ["value"]: IOneDonutGraphData, index: number }): void {
        this.stateService.go("zen.app.pia.module.assessment.list",
            {
                statusFilter: data.value.status || "",
                orgGroupId: this.selectedOrgGroupId || null,
                templateId: this.selectedTemplateId || null,
            });
    }

    onStatusOrgChange(data: IOrganization): void {
        this.selectedOrgGroupId = data.Id;
        this.populateAssessmentStatusData(data.Id, this.selectedTemplateId);
    }

    onStatusTemplateChange(template: ICustomTemplateDetails): void {
        this.selectedTemplateId = template ? template.id : null;
        this.populateAssessmentStatusData(this.selectedOrgGroupId, this.selectedTemplateId);
    }

    private populateAssessmentStatusData(orgGroupId?: string, templateId?: string): void {
        this.canLoadPage = false;
        this.dashboardAPI.getAssessmentStatusCount(orgGroupId, templateId || null)
            .pipe(
                takeUntil(this.destroy$),
            ).subscribe((statusCountData: IAssessmentStatusCountResponse | null): void => {
                this.assessmentStatusCount = this.getAssessmentStatusCount(statusCountData);
                this.totalCount = statusCountData ? statusCountData.totalCount : 0;
                // Populate graph data
                this.assessmentStatusGraphData = this.dashboardDataService.getAssessmentStatusChartData(this.assessmentStatusCount, this.totalCount);
                this.canLoadPage = true;
            });
    }

    private getAssessmentStatusCount(data: IAssessmentStatusCountResponse | null): IAssessmentStatusCount[] {
        const statusData: IStringMap<IAssessmentStatusCount> = {
            [AssessmentStatus.Not_Started]: {
                count: 0,
                status: AssessmentStatus.Not_Started,
            }, [AssessmentStatus.In_Progress]: {
                count: 0,
                status: AssessmentStatus.In_Progress,
            }, [AssessmentStatus.Under_Review]: {
                count: 0,
                status: AssessmentStatus.Under_Review,
            }, [AssessmentStatus.Completed]: {
                count: 0,
                status: AssessmentStatus.Completed,
            },
        };
        if (data && data.assessments.length) {
            each(data.assessments, (assessment: IAssessmentStatusCount): void => {
                if (assessment.status) {
                    statusData[assessment.status].count = assessment.count;
                }
            });
        }
        return sortBy(statusData, (countData: IAssessmentStatusCount): number => AssessmentStatesOrder[AssessmentStatusKeys[countData.status]]);
    }

}
