// Angular
import {
    Component,
    Input,
    OnInit,
} from "@angular/core";

// Constants
import { AssessmentStateClasses } from "constants/dashboard/assessment-status-color.constant";
import { AssessmentStatusKeys } from "constants/assessment.constants";

@Component({
    selector: "assessment-status-count",
    templateUrl: "./assessment-status-count.component.html",
})

export class AssessmentStatusCountComponent implements OnInit {
    @Input() totalCount: number;
    @Input() assessmentStatusCount;

    assessmentStatusClasses: typeof AssessmentStateClasses;
    assessmentStatusKeys: typeof AssessmentStatusKeys;

    ngOnInit(): void {
        this.assessmentStatusClasses = AssessmentStateClasses;
        this.assessmentStatusKeys = AssessmentStatusKeys;
    }

}
