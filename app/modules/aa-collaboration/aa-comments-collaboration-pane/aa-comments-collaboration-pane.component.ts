// 3rd party
import {
    Component,
    OnInit,
    Output,
    EventEmitter,
    Inject,
    ViewChild,
    ElementRef,
} from "@angular/core";

import {
    filter,
    find,
    uniqBy,
 } from "lodash";

// redux
import { StoreToken } from "tokens/redux-store.token";
import { getAssessmentQuestionSequence, getAssessmentDetailAssessmentId } from "modules/assessment/reducers/assessment-detail.reducer";
import {
    getPreSelectedCommentQuestionId,
    AssessmentAction,
} from "modules/assessment/reducers/assessment.reducer";

// Interfaces
import { IStore } from "interfaces/redux.interface";
import { IAssessmentAllComments, ICommentCollaborationPaneResponse } from "interfaces/aa-question-comment.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";
import { IStringMap } from "interfaces/generic.interface";

// services
import { AACommentApiService } from "modules/assessment/services/api/aa-comment-api.service";

@Component({
    selector: "aa-comments-collaboration-pane",
    templateUrl: "aa-comments-collaboration-pane.component.html",
})
export class AACommentsCollaborationPaneComponent implements OnInit {

    @Output() toggleDrawer = new EventEmitter();
    @Output() hide = new EventEmitter<{questionId: string, scrollToQuestion: boolean}>();
    @ViewChild("questionList") questionListElement: ElementRef;

    assessmentId: string = getAssessmentDetailAssessmentId(this.store.getState());
    commentsList: IAssessmentAllComments[];
    isLoading: boolean;
    questionsList: IAssessmentAllComments[];
    selectedComment: IAssessmentAllComments;
    showSubPane = false;
    loadMore = false;
    pageCount = 0;
    isLoadingMore = false;

    constructor(
        @Inject(StoreToken) private store: IStore,
        private assessmentComment: AACommentApiService,
    ) {}

    ngOnInit() {
        this.getComments();
    }

    getComments(newPage?: number) {
        this.isLoading = !Boolean(newPage);
        this.showSubPane = Boolean(newPage);
        const currentPage: number = newPage ? newPage : 0;
        const params: IStringMap<number> = {
            page: currentPage,
            size: 50,
        };
        this.assessmentComment.getAllAssessmentComments(this.assessmentId, params).then((response: IProtocolResponse<ICommentCollaborationPaneResponse>) => {
            if (!response) return;
            this.commentsList = response.data.content;
            this.loadMore = (response.data.totalElements > response.data.numberOfElements) && !response.data.last;
            this.questionsList = currentPage ?
                this.questionsList.concat(this.getQuestionsFromComments(response.data.content)) :
                this.getQuestionsFromComments(response.data.content);
            this.setSelectedComment();
            this.isLoading = false;
            this.isLoadingMore = false;
        });
    }

    setSelectedComment(): void {
        if (this.commentsList && this.commentsList.length > 0) {
            const questionId = getPreSelectedCommentQuestionId(this.store.getState());
            if (questionId) {
                this.selectedComment = this.commentsList.find((comment) => comment.questionId === questionId);
                if (this.selectedComment) {
                    this.store.dispatch({ type: AssessmentAction.SET_PRESELECTED_COMMENT_QUESTION_ID, preSelectedCommentQuestionId: null });
                } else if (this.loadMore) {
                    this.loadMoreComments();
                }
            } else {
                this.selectedComment = this.questionsList[0];
            }
            this.onCommentSelected(this.selectedComment);
        }
    }

    getQuestionsFromComments(comments: IAssessmentAllComments[]): IAssessmentAllComments[] {
        return uniqBy(comments, "questionId");
    }

    getQuestionSequence(questionId: string): string {
        return getAssessmentQuestionSequence(questionId)(this.store.getState());
    }

    onCommentSelected(question: IAssessmentAllComments): void {
        this.selectedComment = question;
        this.showSubPane = Boolean(question);
    }

    showPane(): void {
        this.showSubPane = true;
    }

    loadMoreComments(): void {
        this.isLoadingMore = true;
        this.pageCount++;
        this.getComments(this.pageCount);
        setTimeout(() => {
            this.questionListElement.nativeElement.scrollTop = this.questionListElement.nativeElement.scrollHeight;
        });
    }
}
