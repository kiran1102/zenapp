// Modules
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { VitreusModule } from "@onetrust/vitreus";
import { SharedModule } from "./../../shared/shared.module";
import { OTPipesModule } from "modules/pipes/pipes.module";

// Components
import { AACommentsCollaborationPaneComponent } from "./aa-comments-collaboration-pane.component";
import { AACommentsDrawerComponent } from "./aa-comments-drawer/aa-comments-drawer.component";

@NgModule({
    imports: [
        CommonModule,
        VitreusModule,
        SharedModule,
        OTPipesModule,
    ],
    exports: [
        AACommentsCollaborationPaneComponent,
    ],
    entryComponents: [
        AACommentsCollaborationPaneComponent,
    ],
    declarations: [
        AACommentsDrawerComponent,
        AACommentsCollaborationPaneComponent,
    ],
})
export class AACommentsCollaborationModule { }
