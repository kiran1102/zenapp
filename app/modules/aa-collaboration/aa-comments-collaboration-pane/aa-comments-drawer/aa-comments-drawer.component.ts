import {
    Component,
    EventEmitter,
    Inject,
    Input,
    OnChanges,
    Output,
} from "@angular/core";

// Redux
import { StoreToken } from "tokens/redux-store.token";
import {
    getAssessmentQuestionTitle,
    getAssessmentQuestionSequence,
    AssessmentDetailAction,
    getAssessmentDetailSelectedSectionId,
} from "modules/assessment/reducers/assessment-detail.reducer";

// Services
import { AAQuestionCommentViewLogicService } from "modules/assessment/services/view-logic/aa-question-comment-view-logic.service";
import { AACommentApiService } from "modules/assessment/services/api/aa-comment-api.service";
import { NotificationService } from "modules/shared/services/provider/notification.service";
import { AssessmentDetailService } from "modules/assessment/services/view-logic/assessment-detail.service";
import { AaCollaborationPaneCountsService } from "modules/assessment/services/messaging/aa-section-detail-header-helper.service";

// Interfaces
import { IStore } from "interfaces/redux.interface";
import {
    IQuestionCommentCreateRequest,
    IAssessmentAllComments,
    IQuestionComment,
    IAssessmentQuestionComments,
} from "interfaces/aa-question-comment.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";

// Constants
import { EmptyGuid } from "constants/empty-guid.constant";
import { AAQuillEditor } from "modules/aa-collaboration/constants/aa-quill-editor.constant";

// Pipes
import { PermitPipe } from "pipes/permit.pipe";
import { TranslatePipe } from "pipes/translate.pipe";

// Enums
import { AACollaborationCountsTypes } from "modules/assessment/enums/aa-detail-header.enum";

@Component({
    selector: "aa-comments-drawer",
    templateUrl: "./aa-comments-drawer.component.html",
})
export class AACommentsDrawerComponent implements OnChanges {

    questionName: string;
    questionSequence: string;
    questionId: string;
    commentText = "";
    commentList: IQuestionComment[];
    customModules: any[] = AAQuillEditor;
    isLoading: boolean;
    viewMode: boolean;
    isAdding: boolean;
    isReadOnly = false;

    @Input() currentComment: IAssessmentAllComments;
    @Output() hide = new EventEmitter<{ questionId: string, scrollToQuestion: boolean }>();

    constructor(
        @Inject(StoreToken) private store: IStore,
        private translatePipe: TranslatePipe,
        private assessmentComment: AACommentApiService,
        private permitPipe: PermitPipe,
        private aaQuestionCommentViewLogic: AAQuestionCommentViewLogicService,
        private notificationService: NotificationService,
        private assessmentDetailLogic: AssessmentDetailService,
        private aaCollaborationPaneCountsService: AaCollaborationPaneCountsService,
    ) { }
    ngOnChanges(): void {
        this.questionId = this.currentComment.questionId;
        this.questionName = getAssessmentQuestionTitle(this.questionId)(this.store.getState());
        this.questionSequence = getAssessmentQuestionSequence(this.questionId)(this.store.getState());
        this.onCommentSelected(this.currentComment);
        this.viewMode = this.aaQuestionCommentViewLogic.checkForRespondent() || this.permitPipe.transform("CreateQuestionComment");
        this.assessmentDetailLogic.fetchAssessmentReadOnlyState().subscribe((readonly: boolean) => {
            this.isReadOnly = readonly;
        });
    }

    hidePane(scrollToQuestion: boolean): void {
        this.hide.emit({ questionId: this.questionId, scrollToQuestion });
    }

    commentTrackBy(index: number): number {
        return index;
    }

    cancelComment() {
        this.commentText = "";
    }

    sendComment() {
        this.isAdding = true;
        const commentModel: IQuestionCommentCreateRequest = {
            assessmentId: this.currentComment.assessmentId,
            questionId: this.currentComment.questionId,
            sectionId: this.currentComment.sectionId,
            comment: this.commentText,
        };
        this.commentText = "";

        this.assessmentComment.addcomment(commentModel).then(() => {
            const comment = {
                assessmentId: commentModel.assessmentId,
                questionId: commentModel.questionId,
                sectionId: commentModel.sectionId,
            } as IAssessmentAllComments;
            this.notificationService.alertSuccess(
                this.translatePipe.transform("Success"),
                this.translatePipe.transform("CommentSuccessMessage"),
            );
            this.aaCollaborationPaneCountsService.getCollaborationPaneCounts(commentModel.assessmentId, AACollaborationCountsTypes.Comments);
            this.onCommentSelected(comment);
            this.isAdding = false;
        });
    }

    handleContentChange(comment: string) {
        this.commentText = comment;
    }

    onCommentSelected(question: IAssessmentAllComments): void {
        this.isLoading = true;
        this.assessmentComment.getCommentsByQuestionId(question.assessmentId, question.sectionId, question.questionId)
            .then((response: IProtocolResponse<IAssessmentQuestionComments>): void => {
                if (!response) return;
                this.commentList = response.data.commentDetails;

                if (getAssessmentDetailSelectedSectionId(this.store.getState()) !== EmptyGuid) {
                    this.store.dispatch({ type: AssessmentDetailAction.UPDATE_QUESTION_COMMENT_COUNT, questionId: question.questionId, totalComments: this.commentList.length });
                }

                this.isLoading = false;
            });
    }
}
