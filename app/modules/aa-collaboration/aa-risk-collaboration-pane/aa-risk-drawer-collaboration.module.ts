// Modules
import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { CommonModule } from "@angular/common";
import { VitreusModule } from "@onetrust/vitreus";
import { SharedModule } from "./../../shared/shared.module";
import { OTPipesModule } from "modules/pipes/pipes.module";

// Components
import { AssessmentRiskDrawer } from "./assessment-risk-drawer/assessment-risk-drawer.component";
import { AssessmentRiskCollaborationPaneComponent } from "./aa-risk-collaboration-pane.component";
import { AaRiskControlUpdateModalComponent } from "./assessment-risk-drawer/aa-risk-control/aa-risk-control-update-modal/aa-risk-control-update-modal.component";
import { AARiskControlComponent } from "./assessment-risk-drawer/aa-risk-control/aa-risk-control.component";

@NgModule({
    imports: [
        CommonModule,
        VitreusModule,
        SharedModule,
        OTPipesModule,
        FormsModule,
    ],
    exports: [
        AssessmentRiskCollaborationPaneComponent,
        AaRiskControlUpdateModalComponent,
    ],
    entryComponents: [
        AaRiskControlUpdateModalComponent,
    ],
    declarations: [
        AaRiskControlUpdateModalComponent,
        AssessmentRiskDrawer,
        AARiskControlComponent,
        AssessmentRiskCollaborationPaneComponent,
    ],
})
export class AARiskCollaborationModule { }
