// Rxjs
import { Subject } from "rxjs";
import {
    startWith,
    takeUntil,
} from "rxjs/operators";

// Angular
import {
    Component,
    OnInit,
    Inject,
    Output,
    EventEmitter,
    ElementRef,
    ViewChild,
} from "@angular/core";

// External
import {
    map,
    findIndex,
    find,
    some,
} from "lodash";

// Redux
import { StoreToken } from "tokens/redux-store.token";
import observableFromStore from "oneRedux/utils/observable-from-store";
import {
    getAssessmentDetailAssessmentId,
    getAssessmentQuestionSequence,
    getAssessmentDetailModel,
} from "modules/assessment/reducers/assessment-detail.reducer";
import { getCurrentUser } from "oneRedux/reducers/user.reducer";
import {
    AssessmentAction,
    getPreSelectedRiskId,
} from "modules/assessment/reducers/assessment.reducer";
import {
    getRiskFilter,
    RiskActions,
} from "oneRedux/reducers/risk.reducer";

// Interfaces
import {
    IStore,
    IStoreState,
} from "interfaces/redux.interface";
import {
    IStringMap,
    IValueId,
    INameId,
} from "interfaces/generic.interface";
import {
    IRiskDetails,
    IRiskReference,
} from "interfaces/risk.interface";
import { IPageable } from "interfaces/pagination.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";
import { ILabelValue } from "interfaces/generic.interface";
import { IUser } from "interfaces/user.interface";
import { IAssessmentModel } from "interfaces/assessment/assessment-model.interface";
import { IRiskSetting } from "interfaces/risks-settings.interface";

// Service
import { RiskAPIService } from "oneServices/api/risk-api.service";
import { Permissions } from "modules/shared/services/helper/permissions.service";

// Enums and constant
import {
    RiskV2States,
    RiskV2ReferenceTypes,
} from "enums/riskV2.enum";
import { UserRoles } from "constants/user-roles.constant";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

@Component({
    selector: "aa-risk-collaboration-pane",
    templateUrl: "./aa-risk-collaboration-pane.component.html",
})

export class AssessmentRiskCollaborationPaneComponent implements OnInit {

    @Output() toggleDrawer = new EventEmitter();
    @Output() hide = new EventEmitter<{questionId: string, scrollToQuestion: boolean}>();

    translations: IStringMap<string>;
    riskFilterOptions: Array<ILabelValue<string>>;
    riskModels: IRiskDetails[];
    selectedFilterValue: any;
    selectedRisk: IRiskDetails;
    showSubPane = false;
    loadMore = false;
    pagecount = 0;
    noRisk = false;
    isLoading = false;
    isLoadingMore = false;
    riskSettings: IRiskSetting;

    private assessmentId: string;
    private currentUserId: string;
    private destroy$ = new Subject();

    @ViewChild("riskList")
    private riskListElement: ElementRef;

    constructor(
        @Inject(StoreToken) private store: IStore,
        private RiskAPI: RiskAPIService,
        private readonly permissions: Permissions,
        private translatePipe: TranslatePipe,
    ) {
    }

    ngOnInit() {
        observableFromStore(this.store).pipe(
            startWith(this.store.getState()),
            takeUntil(this.destroy$),
        ).subscribe((state: IStoreState) => this.componentWillReceiveState(state));

        this.translations = {
            show: this.translatePipe.transform("Show"),
            hide: this.translatePipe.transform("Hide"),
            risks: this.translatePipe.transform("Risks"),
            notes: this.translatePipe.transform("Notes"),
            comments: this.translatePipe.transform("Comments"),
        };
        this.riskFilterOptions = [
            { label: this.translatePipe.transform("AllRisks"), value: RiskV2States.NONE },
            { label: this.translatePipe.transform("Identified"), value: RiskV2States.IDENTIFIED },
            { label: this.translatePipe.transform("RecommendationAdded"), value: RiskV2States.RECOMMENDATION_ADDED },
            { label: this.translatePipe.transform("RecommendationSent"), value: RiskV2States.RECOMMENDATION_SENT },
            { label: this.translatePipe.transform("RemediationProposed"), value: RiskV2States.REMEDIATION_PROPOSED },
            { label: this.translatePipe.transform("ExceptionRequested"), value: RiskV2States.EXCEPTION_REQUESTED },
            { label: this.translatePipe.transform("Reduced"), value: RiskV2States.REDUCED },
            { label: this.translatePipe.transform("Retained"), value: RiskV2States.RETAINED },
        ];
        this.selectedFilterValue = getRiskFilter(this.store.getState()) || this.riskFilterOptions[0];
        this.selectRiskFilterOption(this.selectedFilterValue);
    }

    ngOnDestroy() {
        this.destroy$.next();
        this.destroy$.complete();
    }

    fetchRiskSummary(filteroption?: string, newPage?: number) {
        this.isLoading = newPage ? false : true;
        this.showSubPane = newPage ? true : false;
        let filters: IStringMap<string> = { orgGroupId: getAssessmentDetailModel(this.store.getState()).orgGroup.id };
        if (filteroption !== RiskV2States.NONE) {
            filters = {...filters, riskState: RiskV2States[filteroption]};
        }
        const page: number = newPage ? newPage : 0;
        const size = 50;
        if (this.shouldFetchCurrentUserRiskOnly()) {
            filters.riskOwnerId = this.currentUserId;
        }
        this.RiskAPI.fetchRiskV2Summary(this.assessmentId, { page, size }, filters).then((response: IProtocolResponse<IPageable>): void => {
            if (response.result) {
                this.noRisk = response.data.totalElements === 0;
                this.loadMore = (response.data.totalElements > response.data.numberOfElements) && !response.data.last;
                this.riskModels = page
                    ? this.riskModels.concat(response.data.content)
                    : response.data.content;
                this.setSelectedRisk();
            }
            this.isLoading = false;
            this.isLoadingMore = false;
        });
    }

    setSelectedRisk() {
        if (this.riskModels && this.riskModels.length > 0) {
            const preSelectedRiskId: string = getPreSelectedRiskId(this.store.getState());
            if (preSelectedRiskId) {
                this.selectedRisk = find(this.riskModels, { id: preSelectedRiskId });
                if (this.selectedRisk) {
                    this.store.dispatch({ type: AssessmentAction.SET_PRESELECTED_RISKID, preSelectedRiskId: null });
                } else if (this.loadMore) {
                    this.loadMoreRisk();
                }
            } else {
                this.selectedRisk = this.riskModels[0];
            }
            this.onRiskSelected(this.selectedRisk);
        }
    }

    selectRiskFilterOption(selection: ILabelValue<RiskV2States>) {
        this.store.dispatch({ type: RiskActions.SET_RISK_FILTER, riskFilter: selection });
        this.selectedFilterValue = selection;
        this.fetchRiskSummary(selection.value);
    }

    loadMoreRisk(): void {
        this.isLoadingMore = true;
        this.pagecount++;
        this.fetchRiskSummary(this.selectedFilterValue.label, this.pagecount);
        setTimeout(() => {
            this.riskListElement.nativeElement.scrollTop = this.riskListElement.nativeElement.scrollHeight;
        });
    }

    getQuestionSequence(risk: IRiskDetails): string {
        const questionId = (risk as any as IRiskDetails).typeRefId || risk.references.find((reference: IRiskReference) => reference.type === "QUESTION" || reference.typeId === RiskV2ReferenceTypes.QUESTION).id;
        return getAssessmentQuestionSequence(questionId)(this.store.getState());
    }

    onRiskSelected(risk: IRiskDetails) {
        this.selectedRisk = risk;
        this.showSubPane = Boolean(risk);
    }

    updateRisk(risk: IRiskDetails) {
        const riskToUpdateIndex = findIndex(this.riskModels, (oldRisk: IRiskDetails) => oldRisk.id === risk.id);
        if (riskToUpdateIndex >= 0) {
            this.riskModels[riskToUpdateIndex] = { ...risk };
            // when risk state is changed and the selection is not 'All risks' update the filter object
            // 'All risks' value is 0
            if (this.selectedFilterValue.value > 0 && this.selectedFilterValue.value !== risk.state) {
                this.riskModels.splice(riskToUpdateIndex, 1);
                if (!this.riskModels.length) {
                    this.showSubPane = false;
                    this.noRisk = true;
                    return;
                }
                this.selectedRisk = this.riskModels[0];
            } else {
                this.selectedRisk = this.riskModels[riskToUpdateIndex];
            }
        }
    }

    deleteRisk(risk: IRiskDetails) {
        const riskToDeleteIndex = findIndex(this.riskModels, (oldRisk) => oldRisk.id === risk.id);
        if (riskToDeleteIndex >= 0) {
            this.riskModels.splice(riskToDeleteIndex, 1);
            if (this.riskModels.length) {
                this.onRiskSelected(this.riskModels[0]);
                this.noRisk = false;
            } else {
                this.selectedRisk = null;
                this.showSubPane = false;
                this.noRisk = true;
            }
        }
    }

    showPane() {
        this.showSubPane = true;
    }

    private componentWillReceiveState(state: IStoreState) {
        this.currentUserId = getCurrentUser(state).Id;
        this.assessmentId = getAssessmentDetailAssessmentId(state);
    }

    private shouldFetchCurrentUserRiskOnly(): boolean {
        let fetchCurrentUserRiskOnly = false;
        const user: IUser = getCurrentUser(this.store.getState());
        const assessmentModel: IAssessmentModel = getAssessmentDetailModel(this.store.getState());
        if (user) {
            const isApprover = find(assessmentModel.approvers, (ap: IValueId<string>) => ap.id === user.Id);
            const isRespondent = some(assessmentModel.respondents, (respondent: INameId<string>) => respondent.id === user.Id);
            if (!(isApprover || isRespondent)) {
                switch (user.RoleName) {
                    case UserRoles.ProjectOwner:
                    case UserRoles.Invited:
                        fetchCurrentUserRiskOnly = true;
                        break;
                }
            }
        }
        return fetchCurrentUserRiskOnly;
    }

}
