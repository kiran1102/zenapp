// Angular
import {
    Component,
    OnInit,
    OnDestroy,
} from "@angular/core";

// RxJs
import { Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";

// Services
import { AARiskControlApiService } from "modules/assessment/services/api/aa-risk-control-api.service";

// Interfaces
import { IOtModalContent } from "@onetrust/vitreus";
import { ILabelValue } from "@onetrust/vitreus";
import { IRiskControlUpdateModalData } from "modules/assessment/interfaces/aa-risk-control.interface";

// Enums
import { RiskControlStatusNames } from "modules/assessment/enums/aa-risk-control.enum";

@Component({
    selector: "aa-risk-control-update-modal",
    templateUrl: "aa-risk-control-update-modal.component.html",
})

export class AaRiskControlUpdateModalComponent implements OnInit, IOtModalContent, OnDestroy {

    otModalData: IRiskControlUpdateModalData;
    otModalCloseEvent: Subject<RiskControlStatusNames>;
    riskControlList: Array<ILabelValue<string>>;
    selectedRiskControlStatus: ILabelValue<string>;
    riskControlId: string;
    isUpdating: boolean;
    isDisabled: boolean;

    private destroy$ = new Subject();

    constructor(
        private aaRiskControlApiService: AARiskControlApiService,
    ) {}

    ngOnInit() {
        this.isDisabled = true;
        this.riskControlList = this.otModalData.riskControlStatusList;
        this.riskControlId = this.otModalData.riskControl.id;
        this.selectedRiskControlStatus = this.riskControlList.find((control: ILabelValue<string>) => control.value === this.otModalData.riskControl.status);
    }

    ngOnDestroy() {
        this.destroy$.next();
        this.destroy$.complete();
    }

    completeModalEvent(status?: RiskControlStatusNames): void {
        this.otModalCloseEvent.next(status);
        this.otModalCloseEvent.complete();
    }

    onConfirm() {
        this.isUpdating = true;
        this.aaRiskControlApiService.updateRiskControlStatus(this.riskControlId, this.selectedRiskControlStatus.value as RiskControlStatusNames)
            .pipe(takeUntil(this.destroy$))
            .subscribe((response: boolean) => {
                if (response) {
                    this.completeModalEvent(this.selectedRiskControlStatus.value as RiskControlStatusNames);
                }
                this.isUpdating = false;
            });
    }

    selectingRiskStatus(riskControlStatus: ILabelValue<string>) {
        this.selectedRiskControlStatus = this.riskControlList.find((riskControl: ILabelValue<string>) => riskControl.value === riskControlStatus.value);
        this.isDisabled = this.selectedRiskControlStatus.value === this.otModalData.riskControl.status;
    }

}
