// Angular
import {
    Component,
    Input,
    Output,
    EventEmitter,
    OnDestroy,
    OnInit,
} from "@angular/core";

// RxJs
import { Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";

// Vitreus
import {
    OtModalService,
    ConfirmModalType,
} from "@onetrust/vitreus";

// Enums
import {
    RiskControlStatusNames,
    RiskControlActionNames,
} from "modules/assessment/enums/aa-risk-control.enum";

// Interface
import {
    IRiskControlInformation,
    IRiskControlUpdateModalData,
} from "modules/assessment/interfaces/aa-risk-control.interface";
import { ILabelValue } from "interfaces/generic.interface";

// Services
import { AARiskControlApiService } from "modules/assessment/services/api/aa-risk-control-api.service";
import { NotificationService } from "sharedModules/services/provider/notification.service";
import { Permissions } from "sharedModules/services/helper/permissions.service";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

// Components
import { AaRiskControlUpdateModalComponent } from "./aa-risk-control-update-modal/aa-risk-control-update-modal.component";

@Component({
    selector: "aa-risk-control",
    templateUrl: "./aa-risk-control.component.html",
})

export class AARiskControlComponent implements OnInit, OnDestroy {

    @Input() riskControl: IRiskControlInformation;
    @Input() removeRiskControlOption: boolean;

    @Output() onRiskControlRemove = new EventEmitter<string>();
    @Output() onRiskControlUpdate = new EventEmitter<IRiskControlInformation>();

    emptyText = "----";
    riskControlStatusNames = RiskControlStatusNames;
    isControlExpanded = false;
    riskControlMenuOptions = [
        {
            text: this.translatePipe.transform("UpdateStatus"),
            otAutoId: "AssessmentRiskControlUpdateStatusButton",
            value: RiskControlActionNames.UPDATE_STATUS,
        },
        {
            text: this.translatePipe.transform("RemoveControl"),
            otAutoId: "AssessmentRiskControlRemoveControlButton",
            value: RiskControlActionNames.REMOVE_CONTROL,
        },
    ];
    riskControlStatusList: Array<ILabelValue<string>> = [
        {
            label: this.translatePipe.transform(RiskControlStatusNames.PENDING),
            value: RiskControlStatusNames.PENDING,
        },
        {
            label: this.translatePipe.transform(RiskControlStatusNames.IMPLEMENTED),
            value: RiskControlStatusNames.IMPLEMENTED,
        },
        {
            label: this.translatePipe.transform(RiskControlStatusNames.NOT_DOING),
            value: RiskControlStatusNames.NOT_DOING,
        },
    ];
    selectedRiskControlStatus = this.riskControlStatusList[0];

    private destroy$ = new Subject();

    constructor(
        private permissions: Permissions,
        private otModalService: OtModalService,
        private aaRiskControlApiService: AARiskControlApiService,
        private notificationService: NotificationService,
        private translatePipe: TranslatePipe,
    ) { }

    ngOnInit() {
        this.resetContextMenuOptions();
        if (!(this.permissions.canShow("RiskRemoveControl") && this.removeRiskControlOption)) {
            this.riskControlMenuOptions = this.riskControlMenuOptions.filter((option) =>
                option.value !== RiskControlActionNames.REMOVE_CONTROL);
        }
    }

    ngOnDestroy() {
        this.destroy$.next();
        this.destroy$.complete();
    }

    onContextMenuOptionClick(optionId: RiskControlActionNames, riskControl: IRiskControlInformation) {
        const riskControlId = this.riskControl.id;
        const modalData: IRiskControlUpdateModalData = {riskControl, riskControlStatusList: this.riskControlStatusList };

        switch (optionId) {
            case RiskControlActionNames.UPDATE_STATUS:
                this.otModalService.create(
                    AaRiskControlUpdateModalComponent,
                    {
                        isComponent: true,
                        hideClose: true,
                    },
                    modalData,
                ).pipe(takeUntil(this.destroy$))
                .subscribe((result: RiskControlStatusNames) => {
                    if (result) {
                        riskControl.status = result || riskControl.status;
                        this.onRiskControlUpdate.emit(riskControl);
                        this.notificationService.alertSuccess(
                            this.translatePipe.transform("Success"),
                            this.translatePipe.transform("RiskControlStatusUpdatedSuccessfully"));
                    }
                } );

                break;
            case RiskControlActionNames.REMOVE_CONTROL:
                this.otModalService.confirm({
                    type: ConfirmModalType.DELETE,
                    translations: {
                        title: this.translatePipe.transform("RemoveControl"),
                        desc: this.translatePipe.transform("RemoveRiskControlConfirmationMessage"),
                        confirm: this.translatePipe.transform("DoYouWantToContinue"),
                        cancel: this.translatePipe.transform("Cancel"),
                        submit: this.translatePipe.transform("Confirm"),
                    },
                }).pipe(takeUntil(this.destroy$))
                .subscribe((data: boolean): boolean => {
                    if (!data) return;
                    this.aaRiskControlApiService.removeRiskControl(riskControlId)
                        .subscribe((response: boolean): boolean => {
                            if (response) {
                                this.notificationService.alertSuccess(
                                    this.translatePipe.transform("Success"),
                                    this.translatePipe.transform("RiskControlRemovedSuccessfully"));
                                this.onRiskControlRemove.emit(riskControlId);
                            }
                            return response;
                        });
                });
                break;
        }
    }

    selectingRiskStatus(riskStatus: ILabelValue<string>) {
        this.selectedRiskControlStatus = this.riskControlStatusList
            .find((riskControl: ILabelValue<string>): boolean => riskControl.value === riskStatus.value);
    }

    private resetContextMenuOptions() {
        this.riskControlMenuOptions = [
            {
                text: this.translatePipe.transform("UpdateStatus"),
                otAutoId: "AssessmentRiskControlUpdateStatusButton",
                value: RiskControlActionNames.UPDATE_STATUS,
            },
            {
                text: this.translatePipe.transform("RemoveControl"),
                otAutoId: "AssessmentRiskControlRemoveControlButton",
                value: RiskControlActionNames.REMOVE_CONTROL,
            },
        ];
    }
}
