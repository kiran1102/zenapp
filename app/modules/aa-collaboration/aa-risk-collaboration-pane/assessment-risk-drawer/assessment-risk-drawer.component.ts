//#region IMPORTS
import {
    Component,
    Inject,
    Input,
    Output,
    EventEmitter,
    OnChanges,
    OnDestroy,
    SimpleChanges,
} from "@angular/core";

// External
import { isObject } from "lodash";

// RxJs
import { takeUntil } from "rxjs/operators";
import { Subject } from "rxjs";

// Redux
import { StoreToken } from "tokens/redux-store.token";
import { IStore } from "interfaces/redux.interface";
import { getAssessmentDetailModel } from "modules/assessment/reducers/assessment-detail.reducer";

// Selectors
import {
    getAssessmentQuestionSequence,
    getAssessmentQuestionTitle,
    getAssessmentDetailAssessmentId,
    getAssessmentSectionByQuestionId,
    getAssessmentDetailSectionModel,
} from "modules/assessment/reducers/assessment-detail.reducer";

// Enums/Constants
import { RiskV2States, RiskV2ReferenceTypes } from "enums/riskV2.enum";
import { RiskCharacterLimit } from "enums/risk.enum";
import { RiskV2Actions } from "constants/assessment-riskV2-action.constant";
import { RiskV2StateTranslationKeys } from "constants/risk-v2.constants";
import { OrgUserDropdownTypes } from "enums/org-user-dropdown.enum";
import { AACollaborationCountsTypes } from "modules/assessment/enums/aa-detail-header.enum";
import { RiskCategoryStatus } from "modules/risks/risk-category/risk-category.constants";

// Types
import { RiskV2UpdateActions, RiskV2Fields } from "modules/assessment/types/risk-v2.types";

// Services
import { AssessmentRiskActionService } from "modules/assessment/services/action/assessment-risk-action.service";
import { AssessmentRiskApiService } from "modules/assessment/services/api/assessment-risk-api.service";
import { RiskTrackingActionConfig } from "modules/assessment/services/configuration/risk-tracking-action-config.service";
import { ModalService } from "sharedServices/modal.service";
import { TimeStamp } from "oneServices/time-stamp.service";
import { NotificationService } from "modules/shared/services/provider/notification.service";
import { AARiskControlApiService } from "modules/assessment/services/api/aa-risk-control-api.service";
import { AaRiskControlActionService } from "modules/assessment/services/action/aa-risk-control-link-controls-action.service";
import { Permissions } from "modules/shared/services/helper/permissions.service";
import { AaCollaborationPaneCountsService } from "modules/assessment/services/messaging/aa-section-detail-header-helper.service";
import { LookupService } from "sharedModules/services/helper/lookup.service.ts";
import { RiskAPIService } from "oneServices/api/risk-api.service";

// Interfaces
import {
    IRiskMetadata,
    IRiskDetails,
    IRiskUpdateRequest,
    IRiskCategoryBasicInfo,
} from "interfaces/risk.interface";
import { IAssessmentTemplateSection } from "modules/template/interfaces/template.interface";
import { IRiskActionButtonConfig } from "interfaces/assessment-risk-action-button-config.interface";
import { INameId } from "interfaces/generic.interface";
import { IAssessmentModel } from "interfaces/assessment/assessment-model.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";
import { IRiskHeatmap } from "interfaces/risks-settings.interface";
import { IOrgUserAdapted } from "interfaces/org-user.interface";
import { IOtOrgUserOutput } from "sharedModules/components/ot-org-user/ot-org-user.interface";
import { IRiskControlInformation } from "modules/assessment/interfaces/aa-risk-control.interface";
import { IGridPaginationParams } from "interfaces/grid.interface";
import { IPageableOf } from "interfaces/pagination.interface";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

//#endregion IMPORTS

@Component({
    selector: "assessment-risk-drawer",
    templateUrl: "assessment-risk-drawer.component.html",
})

export class AssessmentRiskDrawer implements OnChanges, OnDestroy {

    @Input() risk: IRiskDetails;

    @Output() hide = new EventEmitter<{questionId: string, scrollToQuestion: boolean}>();
    @Output() updateRisk = new EventEmitter<IRiskDetails>();
    @Output() deleteRisk = new EventEmitter<IRiskDetails>();

    assessmentId: string;
    assessmentModel: IAssessmentModel;
    canAddRecommendation = false;
    canEditCategory = false;
    canEditRecommendation = false;
    canEditRiskDetails = false;
    canEditRiskOwner = false;
    canShowContextMenu = false;
    canShowContextMenuOptions = false;
    canShowException: boolean;
    canShowRemediation: boolean;
    canShowRiskStateChangeForm = false;
    contextMenuOptions: Array<INameId<string>>;
    dateDisableUntil = this.timeStamp.getDisableUntilDate();
    dateFormat = this.timeStamp.getDatePickerDateFormat();
    dropdownType = OrgUserDropdownTypes.RiskOwner;
    is24Hour = this.timeStamp.getIs24Hour();
    isDeleting = false;
    isPaneLoading = false;
    maxCharLimit: number = RiskCharacterLimit.Limit;
    model: IRiskUpdateRequest;
    previewMode = true;
    primaryButtonObj: IRiskActionButtonConfig;
    questionId: string;
    questionSequence: string;
    questionTitle: string;
    riskOwnerList: IOrgUserAdapted[];
    riskOwnerSelected: IOrgUserAdapted | string;
    riskStateChangeFieldLabel: string;
    riskStateChangeInput: string;
    riskStates: typeof RiskV2States;
    riskV2Actions: typeof RiskV2Actions;
    riskV2StateTranslationKeys = RiskV2StateTranslationKeys;
    secondaryButtonObj: IRiskActionButtonConfig;
    showRiskEditIcon = false;
    retainedOrReduced = false;
    riskControls: IRiskControlInformation[];
    riskNumber: number;
    riskControlParams: IGridPaginationParams = { page: 0, size: 20, sort: ["createdDate,desc", "controlIdentifier,asc"] };
    showMoreButton: boolean;
    showAddRiskControlButton: boolean;
    isRiskControlLoading: boolean;
    isMoreRiskControlsLoading: boolean;
    pageCount = 0;
    categoryList: IRiskCategoryBasicInfo[];
    filteredCategoryOptions: IRiskCategoryBasicInfo[];
    selectedCategories: IRiskCategoryBasicInfo[] = [];
    selectedCategoriesCSV: string;

    private destroy$ = new Subject();

    constructor(
        @Inject(StoreToken) private store: IStore,
        @Inject(ModalService) private modalService: ModalService,
        private timeStamp: TimeStamp,
        private assessmentRiskAction: AssessmentRiskActionService,
        private assessmentRiskApi: AssessmentRiskApiService,
        private aaCollaborationPaneCountsService: AaCollaborationPaneCountsService,
        private riskTrackingActionConfig: RiskTrackingActionConfig,
        private aaRiskControlApiService: AARiskControlApiService,
        private aaRiskControlActionService: AaRiskControlActionService,
        private notificationService: NotificationService,
        private translatePipe: TranslatePipe,
        private permissions: Permissions,
        private lookupService: LookupService,
        private riskAPI: RiskAPIService,
    ) { }

    ngOnInit(): void {
        const state = this.store.getState();
        this.assessmentId = getAssessmentDetailAssessmentId(state);
        this.assessmentModel = getAssessmentDetailModel(state);
        this.assessmentRiskAction.getRiskOwners(this.risk).then((users: IOrgUserAdapted[]): void => {
            this.riskOwnerList = users;
            this.riskOwnerSelected = this.riskOwnerList.find((user: IOrgUserAdapted) => user.Id === this.risk.riskOwnerId);
        });
        this.riskStates = RiskV2States;
        this.riskV2Actions = RiskV2Actions;
        this.canShowException = true;
        this.canShowRemediation = true;
        this.initialize();
        if (this.permissions.canShow("RiskViewCategory")) {
            this.initializeCategories();
        }
    }

    ngOnChanges(changes: SimpleChanges): void {
        if (changes.risk && !changes.risk.firstChange) {
            this.resetStateChangeForm();
            this.resetRiskControls();
            this.resetCategory();
            this.initialize();
            if (this.riskOwnerList && this.riskOwnerList.length > 0) {
                this.riskOwnerSelected = this.riskOwnerList.find((user: IOrgUserAdapted) => user.Id === this.risk.riskOwnerId);
            }
        }
        this.selectedCategoriesCSV = this.getSelectCategoriesCSV();
    }

    ngOnDestroy() {
        this.destroy$.next();
        this.destroy$.complete();
    }

    hidePane(scrollToQuestion: boolean): void {
        this.hide.emit({ questionId: this.questionId, scrollToQuestion });
    }

    onRiskChange(value: IRiskHeatmap) {
        this.model.probabilityLevelId = value.probabilityLevelId;
        this.model.impactLevelId = value.impactLevelId;
        this.model.riskScore = value.riskScore;
        this.model.levelId = value.levelId;
    }

    onRiskOwnerChange(selectedRiskOwner: IOtOrgUserOutput): void {
        if (selectedRiskOwner.valid) {
            if (selectedRiskOwner.selection && typeof selectedRiskOwner === "object") {
                this.model.riskOwnerId = (selectedRiskOwner.selection.Id === selectedRiskOwner.selection.FullName)
                    ? null
                    : selectedRiskOwner.selection.Id;
                this.model.riskOwner = selectedRiskOwner.selection.FullName;
            } else {
                this.model.riskOwnerId = null;
                this.model.riskOwner = selectedRiskOwner.selection ? selectedRiskOwner.selection.FullName : null;
            }
        }
        this.riskOwnerSelected = (selectedRiskOwner.selection && typeof selectedRiskOwner.selection === "object")
            ? selectedRiskOwner.selection.FullName
            : selectedRiskOwner.selection;
    }

    resetModel(): void {
        this.model = this.convertRiskDetailsToUpdateModel(this.risk);
        this.riskOwnerSelected = this.riskOwnerList.find((user: IOrgUserAdapted) => user.Id === this.model.riskOwnerId);
        this.previewMode = true;
        this.resetCategory();
    }

    toggleContextMenu(value: boolean): void {
        this.canShowContextMenuOptions = value;
    }

    resetStateChangeForm(): void {
        this.previewMode = true;
        this.canShowException = true;
        this.canShowRemediation = true;
        this.canShowRiskStateChangeForm = false;
        this.riskStateChangeInput = null;
        this.retainedOrReduced = false;
    }

    initializeStateChangeForm(): void {
        this.canShowRiskStateChangeForm = true;
        this.primaryButtonObj.isDisabled = true;
    }

    onRiskStateInputChange(value: string): void {
        this.riskStateChangeInput = value;
        this.primaryButtonObj.isDisabled = this.riskStateChangeInput ? false : true;
    }

    setActionButtons(buttonConfig): void {
        if (this.retainedOrReduced) return;
        this.primaryButtonObj = buttonConfig.primaryButtonObj;
        this.secondaryButtonObj = buttonConfig.secondaryButtonObj;
    }

    onPrimaryActionClick(buttonId: string): void {
        switch (buttonId) {
            case RiskV2Actions.SaveRisk:
                this.save("SAVE");
                break;
            case RiskV2Actions.AddRecommendation:
                if (this.retainedOrReduced) return;
                this.setActionButtons(this.riskTrackingActionConfig.getAddRecommendationStateActions());
                this.riskStateChangeFieldLabel = this.translatePipe.transform("Recommendation");
                this.initializeStateChangeForm();
                this.riskStateChangeInput = this.model.recommendation ? this.model.recommendation : null;
                break;
            case RiskV2Actions.SaveRecommendation:
                this.save("SAVE", "recommendation", this.riskStateChangeInput);
                break;
            case RiskV2Actions.SendRecommendation:
                this.performRiskAction("RECOMMENDATION_SEND", "PRIMARY");
                break;
            case RiskV2Actions.ProposeRemediation:
                this.setActionButtons(this.riskTrackingActionConfig.getAddRemediaitonStateActions());
                this.riskStateChangeFieldLabel = this.translatePipe.transform("Remediation");
                this.canShowRemediation = false;
                this.canShowException = false;
                this.initializeStateChangeForm();
                this.riskStateChangeInput = this.model.mitigation ? this.model.mitigation : null;
                break;
            case RiskV2Actions.SendRemediation:
                this.save("SAVE", "mitigation", this.riskStateChangeInput);
                break;
            case RiskV2Actions.ApproveRemediation:
                this.performRiskAction("REMEDIATION_APPROVED", "PRIMARY");
                break;
            case RiskV2Actions.SendException:
                this.save("SAVE", "requestedException", this.riskStateChangeInput);
                break;
            case RiskV2Actions.GrantException:
                this.performRiskAction("EXCEPTION_GRANTED", "PRIMARY");
                break;
        }
    }

    onSecondaryActionClick(buttonId: string): void {
        switch (buttonId) {
            case RiskV2Actions.CancelEdit:
                this.resetModel();
                this.updateActionButtonsConfig(this.risk.state);
                break;
            case RiskV2Actions.CancelRecommendation:
            case RiskV2Actions.CancelRemediation:
            case RiskV2Actions.CancelException:
                this.resetStateChangeForm();
                this.updateActionButtonsConfig(this.risk.state);
                break;
            case RiskV2Actions.RequestException:
                this.setActionButtons(this.riskTrackingActionConfig.getAddExceptionStateActions());
                this.riskStateChangeFieldLabel = this.translatePipe.transform("Exception");
                this.canShowException = false;
                this.canShowRemediation = false;
                this.initializeStateChangeForm();
                this.riskStateChangeInput = this.model.requestedException ? this.model.requestedException : null;
                break;
            case RiskV2Actions.RejectRemediation:
                this.secondaryButtonObj.isLoading = true;
                this.performRiskAction("REMEDIATION_REJECTED", "SECONDARY");
                break;
            case RiskV2Actions.RejectException:
                this.secondaryButtonObj.isLoading = true;
                this.performRiskAction("EXCEPTION_REJECTED", "SECONDARY");
                break;
        }
    }

    onContextMenuOptionClick(optionId: string): void {
        switch (optionId) {
            case RiskV2Actions.EditRisk:
                this.previewMode = false;
                this.setActionButtons(this.riskTrackingActionConfig.getEditRiskDetailActions());
                break;
            case RiskV2Actions.DeleteRisk:
                this.delete();
                break;
            case RiskV2Actions.ClearRecommendation:
                this.save("CLEAR", "recommendation", null);
                break;
            case RiskV2Actions.ClearRemediation:
                this.save("CLEAR", "mitigation", null);
                break;
            case RiskV2Actions.ClearException:
                this.save("CLEAR", "requestedException", null);
                break;
        }
    }

    trackByOptionId(index: number, option: INameId<string>): string {
        return option.id;
    }

    onAddRiskControl() {
        this.aaRiskControlActionService.openAddRiskControlsModal(this.risk.id)
            .pipe(takeUntil(this.destroy$))
            .subscribe((added: boolean) => {
                if (added) {
                    this.notificationService.alertSuccess(
                        this.translatePipe.transform("Success"),
                        this.translatePipe.transform("RiskControlAddedSuccessfully"));
                    this.fetchRiskControl(this.risk.id);
                }
            });
    }

    riskControlRemove(riskControlId: string) {
        const index = this.riskControls.findIndex((control: IRiskControlInformation) => control.id === riskControlId);
        this.riskControls.splice(index, 1);
    }

    riskControlUpdate(riskControl: IRiskControlInformation) {
        if (riskControl.status) {
            const index = this.riskControls.findIndex((control: IRiskControlInformation) => control.id === riskControl.id);
            this.riskControls[index].status = riskControl.status;
        }
    }

    fetchRiskControl(riskId: string, page?: number) {

        if (!this.permissions.canShow("RiskControl")) return;

        this.isRiskControlLoading = page ? false : true;
        this.riskControlParams.page = page || 0;
        this.aaRiskControlApiService.fetchRiskControlByRiskId(riskId, this.riskControlParams).subscribe((response: IProtocolResponse<IPageableOf<IRiskControlInformation>>) => {
            if (response.result) {
                if (this.riskControlParams.page === 0) {
                    this.riskControls = response.data.content;
                } else {
                    this.riskControls = this.riskControls.concat(response.data.content);
                }
                this.showMoreButton = (response.data.totalElements > response.data.numberOfElements) && !response.data.last;
            }
            this.isRiskControlLoading = false;
            this.isMoreRiskControlsLoading = false;
        });
    }

    showMoreClick() {
        ++this.pageCount;
        this.isMoreRiskControlsLoading = true;
        this.fetchRiskControl(this.risk.id, this.pageCount);
    }

    initializeCategories() {
        this.riskAPI.fetchRiskCategoryListByStatus(RiskCategoryStatus.UNARCHIVED)
            .pipe(takeUntil(this.destroy$))
            .subscribe((response: IProtocolResponse<IRiskCategoryBasicInfo[]>) => {
                if (response.result) {
                    this.categoryList = response.data;
                    this.selectedCategories = [...this.risk.categories];
                    this.selectedCategoriesCSV = this.getSelectCategoriesCSV();
                    this.filteredCategoryOptions = this.lookupService.filterOptionsBySelections(this.selectedCategories, this.categoryList, "id");
                }
            });
    }

    getSelectCategoriesCSV(): string {
        if (this.selectedCategories && this.selectedCategories.length) {
            return this.selectedCategories.map((category) => category.name).join(", ");
        } else {
            return null;
        }
    }

    onCategoryLookupInputChange({ key, value }) {
        if (key === "Enter") {
            this.selectedCategories.push(this.filteredCategoryOptions[0]);
            this.onModelValueChange({ currentValue: this.selectedCategories });
            return;
        }
        this.filteredCategoryOptions = this.lookupService.filterOptionsByInput(this.selectedCategories, this.categoryList, value, "id", "name");
    }

    public onModelValueChange({currentValue}): void {
        this.selectedCategories = currentValue;
        this.filteredCategoryOptions = this.lookupService.filterOptionsBySelections(this.selectedCategories, this.categoryList, "id");
        this.model.categoryIds = this.selectedCategories && this.selectedCategories.length ? this.selectedCategories.map((category) => category.id) : null;
    }

    private save(type: "SAVE" | "CLEAR", field?: RiskV2Fields, value?: string): void {
        const model = { ... this.model };
        if (type === "SAVE") {
            this.setButtonLoading("PRIMARY", true);
            switch (field) {
                case "mitigation":
                    model.action = RiskV2States.REMEDIATION_PROPOSED as RiskV2UpdateActions;
                    break;
                case "requestedException":
                    model.action = RiskV2States.EXCEPTION_REQUESTED as RiskV2UpdateActions;
                    break;
                case "recommendation":
                    model.action = RiskV2States.RECOMMENDATION_ADDED as RiskV2UpdateActions;
                    break;
                default:
                    if (model.state === RiskV2States.RECOMMENDATION_ADDED) {
                        model.action = (model.recommendation === null || model.recommendation === "")
                            ? RiskV2States.RECOMMENDATION_REMOVED as RiskV2UpdateActions
                            : RiskV2States.RECOMMENDATION_ADDED as RiskV2UpdateActions;
                    }
                    if (model.recommendation !== null && model.recommendation !== ""
                        && (model.state === RiskV2States.IDENTIFIED
                        || model.state === RiskV2States.IDENTIFIED)) {
                        model.action = RiskV2States.RECOMMENDATION_ADDED as RiskV2UpdateActions;
                    }
            }
        }
        if (type === "CLEAR") {
            switch (field) {
                case "mitigation":
                    model.action = RiskV2States.REMEDIATION_REMOVED as RiskV2UpdateActions;
                    break;
                case "requestedException":
                    model.action = RiskV2States.EXCEPTION_REMOVED as RiskV2UpdateActions;
                    break;
                case "recommendation":
                    model.action = RiskV2States.RECOMMENDATION_REMOVED as RiskV2UpdateActions;
                    break;
            }
        }
        this.isPaneLoading = type === "CLEAR";
        if (field) {
            model[field] = value ? value : null;
        }
        this.assessmentRiskAction.updateRisk(model, model.id, this.assessmentId)
            .then((risk: IRiskMetadata): void => {
                if (type === "SAVE") {
                    this.setButtonLoading("PRIMARY", false);
                }
                this.isPaneLoading = false;
                if (isObject(risk) && risk.riskId) {
                    this.previewMode = true;
                    if (this.risk.levelId !== risk.levelId) {
                        this.updateAssessmentRiskState("EDIT", risk);
                    }
                    this.updateCurrentModel({ ...risk.riskResponse });
                }
            });
    }

    private performRiskAction(action: RiskV2UpdateActions, type: "PRIMARY" | "SECONDARY"): void {
        this.setButtonLoading(type, true);
        this.assessmentRiskAction.performRiskAction(this.model.id, action)
            .then((risk: IRiskDetails): void => {
                this.setButtonLoading(type, false);
                if (isObject(risk) && risk.id) {
                    this.updateCurrentModel({ ...risk });
                }
            });
    }

    private setButtonLoading(type: "PRIMARY" | "SECONDARY", value: boolean) {
        if (type === "PRIMARY") {
            this.primaryButtonObj.isLoading = value;
            if (this.secondaryButtonObj) {
                this.secondaryButtonObj.isDisabled = value;
            }
        } else if (type === "SECONDARY") {
            this.secondaryButtonObj.isLoading = value;
            this.primaryButtonObj.isDisabled = value;
        }
    }

    private deleteRiskModal(riskId: string, assessmentId: string): void {
        this.isDeleting = true;
        this.modalService.setModalData({
            modalTitle: this.translatePipe.transform("DeleteRisk"),
            confirmationText: this.translatePipe.transform("DeleteRiskWarning"),
            cancelButtonText: this.translatePipe.transform("Cancel"),
            submitButtonText: this.translatePipe.transform("Delete"),
            promiseToResolve: (): ng.IPromise<boolean> =>
                this.assessmentRiskApi.deleteAssessmentRisk(riskId, assessmentId)
                    .then((response: IProtocolResponse<void>): boolean => {
                        if (response.result) {
                            this.notificationService.alertSuccess(
                                this.translatePipe.transform("Success"),
                                this.translatePipe.transform("RiskDeleted"),
                            );
                            this.aaCollaborationPaneCountsService.getCollaborationPaneCounts(this.assessmentId, AACollaborationCountsTypes.Risks);
                            this.updateAssessmentRiskState("DELETE");
                            this.deleteRisk.emit({ ...this.risk });
                        }
                        this.isDeleting = false;
                        return response.result;
                    }),
        });
        this.modalService.openModal("deleteConfirmationModal");
    }

    private delete(): void {
        this.deleteRiskModal(this.risk.id, this.assessmentId);
    }

    private updateActionButtonsConfig(state: number | string): void {
        this.resetStateChangeForm();
        switch (state) {
            case RiskV2States.IDENTIFIED:
                this.setActionButtons(this.riskTrackingActionConfig.getRiskIdentifiedStateActions(this.assessmentModel, this.risk));
                break;
            case RiskV2States.RECOMMENDATION_ADDED:
                this.setActionButtons(this.riskTrackingActionConfig.getRecommendationAddedStateActions(this.assessmentModel, this.risk));
                if (!this.risk.riskOwnerId) {
                    this.primaryButtonObj.isDisabled = true;
                }
                break;
            case RiskV2States.RECOMMENDATION_SENT:
                this.setActionButtons(this.riskTrackingActionConfig.getRecommendationSentStateActions(this.assessmentModel, this.risk));
                break;
            case RiskV2States.REMEDIATION_PROPOSED:
                this.setActionButtons(this.riskTrackingActionConfig.getRemediationAddedStateActions(this.assessmentModel, this.risk));
                break;
            case RiskV2States.EXCEPTION_REQUESTED:
                this.setActionButtons(this.riskTrackingActionConfig.getExceptionAddedActions(this.assessmentModel, this.risk));
                break;
            case RiskV2States.REDUCED:
            case RiskV2States.RETAINED:
                this.primaryButtonObj = null;
                this.secondaryButtonObj = null;
                this.retainedOrReduced = true;
                break;
        }
    }

    private updateCurrentModel(risk: (IRiskDetails)): void {
        this.risk = { ...risk };
        this.model = this.convertRiskDetailsToUpdateModel(risk);
        this.updateRisk.emit({ ...risk });
        this.updateActions();
    }

    private updateActions(): void {
        this.setContextMenuOptions();
        this.updateActionButtonsConfig(this.risk.state);
    }

    private setContextMenuOptions(): void {
        this.contextMenuOptions = this.riskTrackingActionConfig.getContextMenuOptions(this.assessmentModel, this.risk);
        this.canShowContextMenu = this.contextMenuOptions.length ? true : false;
        this.setEditRiskDetailPermissions();
    }

    private setEditRiskDetailPermissions(): void {
        this.canEditRiskDetails = this.riskTrackingActionConfig.hasEditRiskPermission(this.assessmentModel, this.risk) && !this.retainedOrReduced;
        this.canEditRiskOwner = this.riskTrackingActionConfig.hasEditRiskOwnerPermission(this.assessmentModel, this.risk) && !this.retainedOrReduced;
        this.canAddRecommendation = this.riskTrackingActionConfig.hasAddRecommendationPermission(this.assessmentModel, this.risk);
        this.canEditRecommendation = this.riskTrackingActionConfig.hasEditRecommendationPermission(this.assessmentModel, this.risk);
        this.canEditCategory = this.riskTrackingActionConfig.hadEditCategoryPermission(this.assessmentModel, this.risk);
    }

    private updateAssessmentRiskState(action: "DELETE" | "EDIT", risk?: IRiskMetadata): void {
        const section: IAssessmentTemplateSection = getAssessmentSectionByQuestionId(this.questionId)(this.store.getState());
        const currentSection = getAssessmentDetailSectionModel(this.store.getState());
        if (currentSection && section.id === currentSection.sectionId) {
            if (action === "EDIT") {
                this.assessmentRiskAction.updateRiskCallback(risk, risk.riskResponse, section.id);
            } else {
                this.assessmentRiskAction.deleteRiskCallback([this.risk.id], this.questionId, section.id, this.assessmentId);
            }
        } else {
            this.assessmentRiskAction.updateRiskStatistics(this.assessmentId, section.id);
        }
    }

    private initialize(): void {
        this.riskControls = [];
        this.fetchRiskControl(this.risk.id);
        this.questionId = this.risk.typeRefId;
        this.questionSequence = getAssessmentQuestionSequence(this.questionId)(this.store.getState());
        this.questionTitle = getAssessmentQuestionTitle(this.questionId)(this.store.getState());
        this.model = this.convertRiskDetailsToUpdateModel(this.risk);
        this.riskNumber = this.risk.number;
        this.updateActions();
        this.showAddRiskControlButton = !(this.risk.state === RiskV2States.REDUCED
            || this.risk.state === RiskV2States.RETAINED);
    }

    private resetRiskControls() {
        this.riskControls = null;
        this.pageCount = 0;
        this.showMoreButton = false;
    }

    private resetCategory() {
        this.selectedCategories = [...this.risk.categories];
    }

    private convertRiskDetailsToUpdateModel(risk: IRiskDetails): IRiskUpdateRequest {
        return {
            action: risk.action,
            deadline: risk.deadline,
            description: risk.description,
            id: risk.id,
            impactLevelId: risk.impactLevelId,
            levelId: risk.levelId,
            mitigation: risk.mitigation,
            orgGroupId: risk.orgGroup.id,
            probabilityLevelId: risk.probabilityLevelId,
            recommendation: risk.recommendation,
            references: [{
                type: RiskV2ReferenceTypes[RiskV2ReferenceTypes.ASSESSMENT],
                id: risk.source.id,
                name: risk.source.name,
                version: risk.source.version,
            }],
            requestedException: risk.requestedException,
            riskApproversId: risk.riskApproversId,
            riskOwner: risk.riskOwner,
            riskOwnerId: risk.riskOwnerId,
            riskScore: risk.riskScore,
            state: risk.state as string,
            categoryIds: risk.categories && risk.categories.length ? risk.categories.map((category) => category.id) : null,
        };
    }
}
