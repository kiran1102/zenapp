// Modules
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { VitreusModule } from "@onetrust/vitreus";
import { SharedModule } from "./../../shared/shared.module";
import { OTPipesModule } from "modules/pipes/pipes.module";

// Components
import { AANotesDrawer } from "./aa-notes-drawer/aa-notes-drawer.component";
import { AANotesCollaborationPaneComponent } from "./aa-notes-collaboration-pane.component";

@NgModule({
    imports: [
        CommonModule,
        VitreusModule,
        SharedModule,
        OTPipesModule,
    ],
    exports: [
        AANotesCollaborationPaneComponent,
    ],
    entryComponents: [
    ],
    declarations: [
        AANotesDrawer,
        AANotesCollaborationPaneComponent,
    ],
})
export class AANotesCollaborationModule { }
