// Rxjs
import { Subject } from "rxjs";
import {
    startWith,
    takeUntil,
} from "rxjs/operators";

// Angular
import {
    Component,
    OnInit,
    Inject,
    Output,
    EventEmitter,
    OnDestroy,
} from "@angular/core";

// External
import {
    filter,
    find,
} from "lodash";

// Redux
import { StoreToken } from "tokens/redux-store.token";
import observableFromStore from "oneRedux/utils/observable-from-store";
import {
    getAssessmentDetailAssessmentId,
} from "modules/assessment/reducers/assessment-detail.reducer";
import { getCurrentUser } from "oneRedux/reducers/user.reducer";
import { getAssessmentCommentState } from "modules/assessment/reducers/assessment-comment.reducer";
import {
    getNotesFilterState,
    AANoteActions,
} from "modules/assessment/reducers/aa-notes-filter.reducer.ts";

// Interfaces
import {
    IStore,
    IStoreState,
} from "interfaces/redux.interface";
import { IStringMap } from "interfaces/generic.interface";
import { ILabelValue } from "interfaces/generic.interface";
import {
    IAssessmentCommentModel,
    IAssessmentCommentModaldata,
} from "interfaces/assessment/assessment-comment-model.interface";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

// Service
import { Permissions } from "modules/shared/services/helper/permissions.service";
import { AaNotesActionService } from "modules/assessment/services/action/aa-notes-action.service";
import { AssessmentDetailService } from "modules/assessment/services/view-logic/assessment-detail.service";

// Enums
import { AANotesTypes } from "enums/assessment/assessment-comment-types.enum";
import { AssessmentPermissions } from "constants/assessment.constants";

@Component({
    selector: "aa-notes-collaboration-pane",
    templateUrl: "aa-notes-collaboration-pane.component.html",
})

export class AANotesCollaborationPaneComponent implements OnInit, OnDestroy {

    @Output() toggleDrawer = new EventEmitter();
    @Output() hide = new EventEmitter<null>();

    commentList: IAssessmentCommentModel[];
    commentType = AANotesTypes;
    filteredCommentList: IAssessmentCommentModel[];
    isEditMode = false;
    notesFilterOptions: Array<ILabelValue<AANotesTypes>>;
    notesPermissions: IStringMap<boolean>;
    selectedFilterValue: ILabelValue<AANotesTypes>;
    selectedNote: IAssessmentCommentModel;
    showSubPane = false;
    isLoading: boolean;
    currentUserId: string;
    isReadOnly = false;

    private assessmentId: string;
    private destroy$ = new Subject();

    constructor(
        @Inject(StoreToken) private store: IStore,
        private translatePipe: TranslatePipe,
        private permissions: Permissions,
        private assessmentNotesAction: AaNotesActionService,
        private assessmentDetailLogic: AssessmentDetailService,
    ) {
    }

    ngOnInit(): void {
        this.notesFilterOptions = [
            { label: this.translatePipe.transform("AllNotes"), value: AANotesTypes.AllNote },
            { label: this.translatePipe.transform("OnlyMe"), value: AANotesTypes.OnlyMeNote },
            { label: this.translatePipe.transform("ApproversOnly"), value: AANotesTypes.ApproversNote },
            { label: this.translatePipe.transform("Everyone"), value: AANotesTypes.EveryoneNote },
        ];

        this.notesPermissions = {
            createAndEditNote: this.permissions.canShow(AssessmentPermissions.CreateAndEditNote),
            assessmentCanBeApprover: this.permissions.canShow(AssessmentPermissions.AssessmentCanBeApprover),
        };

        observableFromStore(this.store).pipe(
            startWith(this.store.getState()),
            takeUntil(this.destroy$),
        ).subscribe((state: IStoreState) => this.componentWillReceiveState(state));
        this.selectedFilterValue = getNotesFilterState(this.store.getState()) || this.notesFilterOptions[0];
        this.selectNotesFilterOption(this.selectedFilterValue);

        this.fetchComments();
        this.assessmentDetailLogic.fetchAssessmentReadOnlyState().subscribe((readonly: boolean) => {
            this.isReadOnly = readonly;
        });
    }

    fetchComments() {
        this.isLoading = true;
        this.assessmentNotesAction.fetchNotes(this.assessmentId)
            .then((response: IAssessmentCommentModel[]): void => {
                if (response.length) {
                    this.commentList = response;
                    this.onNoteSelected(this.commentList[0]);
                }
                this.isLoading = false;
            });
    }

    ngOnDestroy(): void {
        this.destroy$.next();
        this.destroy$.complete();
    }

    onNoteSelected(comment: IAssessmentCommentModel) {
        this.selectedNote = { ...comment };
        this.showSubPane = true;
    }

    selectNotesFilterOption(selection: ILabelValue<AANotesTypes>): void {
        this.store.dispatch({ type: AANoteActions.SET_NOTES_FILTER, filterState: selection });
        this.filterNotesList(selection);
    }

    filterNotesList(selection: ILabelValue<AANotesTypes>): void {
        this.selectedFilterValue = selection;
        if (selection.value === AANotesTypes.AllNote) {
            this.filteredCommentList = this.commentList;
        } else {
            this.filteredCommentList = filter(this.commentList, { type: this.selectedFilterValue.value });
        }
        if (this.selectedNote && this.selectedNote.id && !find(this.commentList, (comment: IAssessmentCommentModel) => comment.id === this.selectedNote.id)) {
            if (this.filteredCommentList.length) {
                this.onNoteSelected(this.filteredCommentList[0]);
            } else {
                this.hideSubPane();
            }
        }
    }

    hideSubPane() {
        this.hide.emit();
    }

    updateNote(commentModal: IAssessmentCommentModel): void {
        const modalData: IAssessmentCommentModaldata = {
            id: commentModal ? commentModal.id : null,
            type: commentModal ? commentModal.type : AANotesTypes.OnlyMeNote,
            value: commentModal ? commentModal.value : "",
            isReadOnly: commentModal && commentModal.id ? true : false,
            editMode: true,
            commenterId: commentModal ? commentModal.commenterId : null,
        };
        this.isEditMode = false;
        this.assessmentNotesAction.updateNote(this.assessmentId, modalData)
            .then((note: IAssessmentCommentModel) => this.selectedNote = note || this.selectedNote);
    }

    trackByFn(index: number, item: IAssessmentCommentModel): string {
        return item.id;
    }

    addNote() {
        this.selectedNote = { type: AANotesTypes.OnlyMeNote } as IAssessmentCommentModel;
        this.isEditMode = true;
        this.showSubPane = true;
    }

    showPane(): void {
        this.showSubPane = true;
    }

    private componentWillReceiveState(state: IStoreState): void {
        this.commentList = getAssessmentCommentState(state).commentModel;
        this.currentUserId = getCurrentUser(state).Id;
        this.assessmentId = getAssessmentDetailAssessmentId(state);
        if (this.commentList) {
            this.selectedFilterValue = getNotesFilterState(this.store.getState()) || this.notesFilterOptions[0];
            this.filterNotesList(this.selectedFilterValue);
        }
    }
}
