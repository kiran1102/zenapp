//#region IMPORTS
import {
    Component,
    Inject,
    Input,
    Output,
    EventEmitter,
    OnChanges,
    OnInit,
} from "@angular/core";

// External
import {
    find,
    isNil,
} from "lodash";
import {
    Observable,
    BehaviorSubject,
} from "rxjs";

// Enums
import { AANotesTypes } from "enums/assessment/assessment-comment-types.enum";

// Constants
import { AAQuillEditor } from "../../constants/aa-quill-editor.constant";
import { AssessmentPermissions } from "constants/assessment.constants";

// Interfaces
import { IAssessmentCommentModel } from "interfaces/assessment/assessment-comment-model.interface";
import {
    ILabelValue,
    IStringMap,
} from "interfaces/generic.interface";

// Services
import { Permissions } from "modules/shared/services/helper/permissions.service";
import { AaNotesActionService } from "modules/assessment/services/action/aa-notes-action.service";
import { AssessmentDetailService } from "modules/assessment/services/view-logic/assessment-detail.service";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

enum NotesActions {
    EDIT_NOTE = "Edit",
    DELETE_NOTE = "Delete",
}

@Component({
    selector: "aa-notes-drawer",
    templateUrl: "aa-notes-drawer.component.html",
})

export class AANotesDrawer implements OnChanges, OnInit {

    @Input() comment: IAssessmentCommentModel;
    @Input() assessmentId: string;
    @Input() currentUserId: string;
    @Input() isEditMode: boolean;
    @Output() hide = new EventEmitter();
    @Output() updateNote = new EventEmitter();
    @Output() resetNotesPaneEditMode = new EventEmitter<null>();

    isCommenter = false;
    model: IAssessmentCommentModel;
    selectedOption: ILabelValue<AANotesTypes>;
    contextMenuOptions: Array<ILabelValue<NotesActions>>;
    canShowContextMenuOptions = false;
    isDisabled = true;
    accessOptions: Array<ILabelValue<AANotesTypes>> = [
        { label: this.translatePipe.transform("OnlyMe"), value: AANotesTypes.OnlyMeNote },
        { label: this.translatePipe.transform("ApproversOnly"), value: AANotesTypes.ApproversNote },
        { label: this.translatePipe.transform("Everyone"), value: AANotesTypes.EveryoneNote },
    ];
    isReadOnly = false;
    createAndEditPermission = false;
    permissions: IStringMap<boolean> = {
        assessmentCanBeApprover: this.permission.canShow(AssessmentPermissions.AssessmentCanBeApprover),
        createAndEditNote: this.permission.canShow(AssessmentPermissions.CreateAndEditNote),
        deleteNote: this.permission.canShow(AssessmentPermissions.DeleteNote),
        viewNote: this.permission.canShow(AssessmentPermissions.ViewNote),
    };
    contentSource = new BehaviorSubject("");
    contentObservable: Observable<string> = this.contentSource.asObservable();
    customModules: any[] = AAQuillEditor;

    constructor(
        private translatePipe: TranslatePipe,
        private permission: Permissions,
        private assessmentNotesAction: AaNotesActionService,
        private assessmentDetailLogic: AssessmentDetailService,
    ) { }

    ngOnInit(): void {
        this.model = { ...this.comment };
        this.setContextMenuOptions();
        this.assessmentDetailLogic.fetchAssessmentReadOnlyState().subscribe((readonly: boolean) => {
            this.isReadOnly = readonly;
        });
        this.contentSource.next(this.comment.value);
    }

    ngOnChanges(): void {
        this.model = { ...this.comment };
        if (this.model.id) {
            this.selectedOption = find(this.accessOptions, { value: this.model.type });
            this.isCommenter = !isNil(this.model.id) && this.currentUserId === this.model.commenterId;
        } else {
            this.selectedOption = this.accessOptions[0];
            this.model.value = "";
            this.model.type = this.selectedOption.value;
        }
        this.setContextMenuOptions();
        this.contentSource.next(this.comment.value);
    }

    setContextMenuOptions() {
        this.createAndEditPermission = this.permissions && (this.permissions.createAndEditNote || this.isCommenter || this.permissions.assessmentCanBeApprover);
        this.contextMenuOptions = [];

        if (this.createAndEditPermission) {
            this.contextMenuOptions.push({
                label: NotesActions.EDIT_NOTE,
                value: NotesActions.EDIT_NOTE,
            });
        }
        if (this.hasPermissionToDeleteNote()) {
            this.contextMenuOptions.push({
                label: NotesActions.DELETE_NOTE,
                value: NotesActions.DELETE_NOTE,
            });
        }
    }

    hasPermissionToDeleteNote(): boolean {
        return this.permissions && (this.permissions.deleteNote || this.permissions.assessmentCanBeApprover || this.isCommenter)
            && Boolean(this.model.id);
    }

    hidePane(): void {
        this.hide.emit();
    }

    editNote(): void {
        this.isEditMode = true;
    }

    handleInputChange(value: string): void {
        this.model.value = value;
        this.isDisabled = !Boolean(value && value.length);
    }

    openDeleteNoteModal(comment: IAssessmentCommentModel): void {
        this.assessmentNotesAction.deleteNotesModal(this.assessmentId, comment);
    }

    toggleContextMenu(value: boolean): void {
        this.canShowContextMenuOptions = value;
    }

    onContextMenuOptionClick(option: NotesActions): void {
        switch (option) {
            case NotesActions.DELETE_NOTE:
                this.openDeleteNoteModal(this.comment);
                break;
            case NotesActions.EDIT_NOTE:
                this.editNote();
        }
    }

    resetModel(): void {
        this.model = { ...this.comment };
        if (this.model.id) {
            this.selectedOption = find(this.accessOptions, { value: this.model.type });
            this.isEditMode = false;
            this.resetNotesPaneEditMode.emit();
        } else {
            this.hidePane();
        }
    }

    selectNotesAccessOption(selection: ILabelValue<AANotesTypes>): void {
        this.selectedOption = selection;
        this.model.type = this.selectedOption.value;
        this.isDisabled = !selection;
    }

    updateComment() {
        this.updateNote.emit({ ...this.model });
        this.isEditMode = false;
    }

}
