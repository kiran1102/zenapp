// Modules
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { VitreusModule } from "@onetrust/vitreus";
import { SharedModule } from "./../shared/shared.module";
import { OTPipesModule } from "modules/pipes/pipes.module";
import { AAPipesModule } from "modules/assessment/pipes/aa-pipes.module";
import { AACommentsCollaborationModule } from "./aa-comments-collaboration-pane/aa-comments-collaboration.module";
import { AANotesCollaborationModule } from "./aa-notes-collaboration-pane/aa-notes-collaboration.module";
import { AANeedsMoreInfoCollaborationModule } from "./aa-needs-more-info-collaboration-pane/aa-needs-more-info-collaboration.module";
import { AARiskCollaborationModule } from "./aa-risk-collaboration-pane/aa-risk-drawer-collaboration.module";

// Components
import { AACommentsCollaborationPaneComponent } from "./aa-comments-collaboration-pane/aa-comments-collaboration-pane.component";
import { AaNeedsMoreInfoCollaborationPaneComponent } from "./aa-needs-more-info-collaboration-pane/aa-needs-more-info-collaboration-pane.component";
import { AANotesCollaborationPaneComponent } from "./aa-notes-collaboration-pane/aa-notes-collaboration-pane.component";
import { AssessmentRiskCollaborationPaneComponent } from "./aa-risk-collaboration-pane/aa-risk-collaboration-pane.component";

@NgModule({
    imports: [
        CommonModule,
        VitreusModule,
        SharedModule,
        OTPipesModule,
        AAPipesModule,
        AACommentsCollaborationModule,
        AANotesCollaborationModule,
        AANeedsMoreInfoCollaborationModule,
        AARiskCollaborationModule,
    ],
    exports: [
        AACommentsCollaborationPaneComponent,
        AaNeedsMoreInfoCollaborationPaneComponent,
        AANotesCollaborationPaneComponent,
        AssessmentRiskCollaborationPaneComponent,
    ],
})
export class AACollaborationModule { }
