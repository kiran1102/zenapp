export const AAQuillEditor = [
    [
        "bold",
        "italic",
        "underline",
        "strike",
        "clean",
        "blockquote",
        "code-block",
        "link",
        "image",
        { list: "ordered" },
        { list: "bullet" },
    ],
    [
        { align: "" },
        { align: "right" },
        { align: "center" },
        { align: "justify" },
    ],
    [
        {
            indent: "-1",
        },
        {
            indent: "+1",
        },
    ],
];
