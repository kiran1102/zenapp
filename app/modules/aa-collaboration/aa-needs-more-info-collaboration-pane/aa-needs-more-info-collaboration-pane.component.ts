// Angular
import {
    Component,
    OnInit,
    Inject,
    Output,
    EventEmitter,
} from "@angular/core";

// Rxjs
import {
    startWith,
    takeUntil,
} from "rxjs/operators";
import { Subject } from "rxjs";

// External
import {
    findIndex,
    find,
    remove,
    filter,
    isNil,
} from "lodash";

// Redux
import { StoreToken } from "tokens/redux-store.token";
import observableFromStore from "oneRedux/utils/observable-from-store";
import {
    getAssessmentDetailAssessmentId,
    getAssessmentQuestionSequence,
} from "modules/assessment/reducers/assessment-detail.reducer";
import { getCurrentUser } from "oneRedux/reducers/user.reducer";
import {
    getPreSelectedRootRequestId,
    AssessmentAction,
} from "modules/assessment/reducers/assessment.reducer";
import {
    getNeedsMoreInfoFilterState,
    AANeedsMoreInfoReducerAction,
} from "modules/assessment/reducers/aa-needs-more-info.reducer";

// Interfaces
import {
    IStore,
    IStoreState,
} from "interfaces/redux.interface";
import { IStringMap } from "interfaces/generic.interface";
import { ILabelValue } from "interfaces/generic.interface";
import {
    INeedsMoreInfo,
    INeedsMoreInfoModel,
} from "interfaces/assessment/needs-more-info.interface";
import { IAssessmentTemplateSection } from "modules/template/interfaces/template.interface";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

// Service
import { AANeedsMoreInfoActionService } from "modules/assessment/services/action/aa-needs-more-info-action.service";

// Enums
import {
    NeedsMoreInfoFilterState,
    NeedsMoreInfoResponseStateKeys,
} from "enums/aa-needs-more-info.enum";

@Component({
    selector: "aa-needs-more-info-collaboration-pane",
    templateUrl: "aa-needs-more-info-collaboration-pane.component.html",
})

export class AaNeedsMoreInfoCollaborationPaneComponent implements OnInit {

    @Output() toggleDrawer = new EventEmitter();
    @Output() hide = new EventEmitter<{ questionId: string, scrollToQuestion: boolean }>();

    translations: IStringMap<string>;
    needsMoreInfoFilterOptions: Array<ILabelValue<string>>;
    needsMoreInfoModels: INeedsMoreInfo[];
    filteredNeedsMoreInfoModels: INeedsMoreInfo[];
    selectedFilterValue: any;
    selectedRequest: INeedsMoreInfo;
    showSubPane = false;
    loadMore = false;
    pagecount = 0;
    noRequests = false;
    isLoading = false;
    needsMoreInfoOpenState = NeedsMoreInfoFilterState.Open;
    currentSection: IAssessmentTemplateSection;
    questionContentLimit = 60;
    responseStateTransalationKeys = NeedsMoreInfoResponseStateKeys;

    private assessmentId: string;
    private destroy$ = new Subject();

    constructor(
        @Inject(StoreToken) private store: IStore,
        private translatePipe: TranslatePipe,
        private aaNeedsMoreInfoActionService: AANeedsMoreInfoActionService,
    ) {
    }

    ngOnInit(): void {
        observableFromStore(this.store).pipe(
            startWith(this.store.getState()),
            takeUntil(this.destroy$),
        ).subscribe((state: IStoreState) => this.componentWillReceiveState(state));

        this.translations = {
            show: this.translatePipe.transform("Show"),
            hide: this.translatePipe.transform("Hide"),
            flaggedquestions: this.translatePipe.transform("FlaggedQuestions"),
            notes: this.translatePipe.transform("Notes"),
            comments: this.translatePipe.transform("Comments"),
        };
        this.needsMoreInfoFilterOptions = [
            { label: this.translatePipe.transform("All"), value: NeedsMoreInfoFilterState.All },
            { label: this.translatePipe.transform("Open"), value: NeedsMoreInfoFilterState.Open },
            { label: this.translatePipe.transform("ClosedInfo"), value: NeedsMoreInfoFilterState.Close },
        ];
        this.getAllNeedsMoreInfoDetails();
    }

    ngOnDestroy(): void {
        this.destroy$.next();
        this.destroy$.complete();
        this.store.dispatch({
            type: AANeedsMoreInfoReducerAction.SET_FILTER_STATE,
            filterState: { label: this.translatePipe.transform("All"), value: NeedsMoreInfoFilterState.All },
        });
    }

    getAllNeedsMoreInfoDetails(): void {
        this.isLoading = true;
        this.showSubPane = false;

        this.aaNeedsMoreInfoActionService.getAllAssessmentRequestDetails(this.assessmentId)
            .then((response: INeedsMoreInfoModel): void => {
                if (response) {
                    this.needsMoreInfoModels = this.filteredNeedsMoreInfoModels = response.rootRequestInformationResponses;
                    this.noRequests = this.needsMoreInfoModels.length === 0;
                    if (this.needsMoreInfoModels && this.needsMoreInfoModels.length > 0) {
                        const preSelectedRootRequestId: string = getPreSelectedRootRequestId(this.store.getState());
                        const defaultFilter: boolean = preSelectedRootRequestId ? true : false;
                        this.applyFilter(defaultFilter);

                        // If - Open preselection
                        // ElseIf - Pane open from header
                        // Else - Empty check
                        if (preSelectedRootRequestId) {
                            this.selectedRequest = find(this.needsMoreInfoModels, { rootRequestId: preSelectedRootRequestId });
                            this.store.dispatch({ type: AssessmentAction.SET_PRESELECTED_ROOTREQUESTID, preSelectedRootRequestId: "" });
                            this.showSubPane = true;
                        } else if (this.filteredNeedsMoreInfoModels.length > 0) {
                            this.selectedRequest = this.filteredNeedsMoreInfoModels[0];
                            this.showSubPane = true;
                        } else {
                            this.subPaneEmptyCheck();
                        }
                    }
                    this.isLoading = false;
                }
            });
    }

    applyFilter(defaultFilter: boolean): void {
        const preSetFilter = getNeedsMoreInfoFilterState(this.store.getState());
        if (!isNil(preSetFilter) && !defaultFilter) {
            this.selectedFilterValue = preSetFilter;
        } else {
            this.selectedFilterValue = this.needsMoreInfoFilterOptions[0];
        }
        this.filterList();
    }

    selectFilterOption(selection: ILabelValue<string>): void {
        this.selectedFilterValue = selection;
        this.store.dispatch({ type: AANeedsMoreInfoReducerAction.SET_FILTER_STATE, filterState: selection });
        this.filterList();
        this.selectedRequest = this.filteredNeedsMoreInfoModels[0];
        this.subPaneEmptyCheck();
    }

    filterList(): void {
        if (this.selectedFilterValue.value !== NeedsMoreInfoFilterState.All) {
            this.filteredNeedsMoreInfoModels = filter(this.needsMoreInfoModels, (item) => item.responseState === this.selectedFilterValue.value);
        } else {
            this.filteredNeedsMoreInfoModels = this.needsMoreInfoModels;
        }
        this.subPaneEmptyCheck();
    }

    subPaneEmptyCheck(): void {
        if (this.filteredNeedsMoreInfoModels.length === 0) {
            this.showSubPane = false;
            this.noRequests = true;
        } else {
            this.showSubPane = true;
            this.noRequests = false;
        }
    }

    getQuestionSequence(questionId: string): string {
        return getAssessmentQuestionSequence(questionId)(this.store.getState());
    }

    onSelection(selection: INeedsMoreInfo): void {
        this.selectedRequest = selection;
        this.showSubPane = true;
    }

    updateRequest(needsMoreInfo: INeedsMoreInfo): void {
        const modelIndex = findIndex(this.needsMoreInfoModels, { rootRequestId: needsMoreInfo.rootRequestId });
        this.needsMoreInfoModels.splice(modelIndex, 1, needsMoreInfo);
        this.filterList();
        if (this.selectedFilterValue.value !== NeedsMoreInfoFilterState.All) {
            this.selectedRequest = this.filteredNeedsMoreInfoModels[0];
        } else {
            this.selectedRequest = needsMoreInfo;
        }
    }

    deleteInfo(rootRequestId: string): void {
        remove(this.needsMoreInfoModels, (needMoreInfoModel: INeedsMoreInfo): boolean => needMoreInfoModel.rootRequestId === rootRequestId);
        this.filterList();
        this.selectedRequest = this.filteredNeedsMoreInfoModels[0];
    }

    showPane(): void {
        this.showSubPane = true;
    }

    private componentWillReceiveState(state: IStoreState): void {
        this.assessmentId = getAssessmentDetailAssessmentId(state);
        this.selectedFilterValue = getNeedsMoreInfoFilterState(this.store.getState()) || this.needsMoreInfoFilterOptions[0];
    }

}
