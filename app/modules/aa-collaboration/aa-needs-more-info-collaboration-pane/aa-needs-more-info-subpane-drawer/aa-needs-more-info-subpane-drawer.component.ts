//#region IMPORTS
import {
    Component,
    Inject,
    Input,
    Output,
    EventEmitter,
    OnChanges,
    SimpleChanges,
} from "@angular/core";

// Interfaces
import {
    INeedsMoreInfo,
    INeedsMoreInfoResponse,
    INeedsMoreInfoModel,
} from "interfaces/assessment/needs-more-info.interface";
import { IStore } from "interfaces/redux.interface";
import { IUser } from "interfaces/user.interface";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

// Redux
import { StoreToken } from "tokens/redux-store.token";

// Selectors
import { getCurrentUser } from "oneRedux/reducers/user.reducer";
import { getAssessmentDetailModel } from "modules/assessment/reducers/assessment-detail.reducer";
import {
    getAssessmentQuestionSequence,
    getAssessmentQuestionTitle,
    getAssessmentDetailAssessmentId,
} from "modules/assessment/reducers/assessment-detail.reducer";

// Services
import { ModalService } from "sharedServices/modal.service";
import { Permissions } from "modules/shared/services/helper/permissions.service";
import { AANeedsMoreInfoActionService } from "modules/assessment/services/action/aa-needs-more-info-action.service";
import { AANeedsMoreInfoApiService } from "modules/assessment/services/api/aa-needs-more-info-api.service";
import { AssessmentDetailService } from "modules/assessment/services/view-logic/assessment-detail.service";
import { AaCollaborationPaneCountsService } from "modules/assessment/services/messaging/aa-section-detail-header-helper.service";

// Interfaces
import {
    INameId,
    IStringMap,
} from "interfaces/generic.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";
import { INeedsMoreInfoButtonConfig } from "modules/assessment/interfaces/aa-needs-more-info.interface";

// Constants
import { AssessmentPermissions } from "constants/assessment.constants";
import { NeedsMoreInfoActions } from "constants/aa-needs-more-info-action.constant";
import { AAQuillEditor } from "modules/aa-collaboration/constants/aa-quill-editor.constant";

// Enum
import { AACollaborationCountsTypes } from "modules/assessment/enums/aa-detail-header.enum";

//#endregion IMPORTS

@Component({
    selector: "aa-needs-more-info-subpane-drawer",
    templateUrl: "./aa-needs-more-info-subpane-drawer.component.html",
})

export class AANeedsMoreInfoSubpaneDrawerComponent implements OnChanges {

    @Input() needsMoreInfoRequest: INeedsMoreInfo;
    @Output() hide = new EventEmitter();
    @Output() updateRequestEvent = new EventEmitter<INeedsMoreInfo>();
    @Output() deleteInfo = new EventEmitter();

    state = this.store.getState();
    assessmentId = getAssessmentDetailAssessmentId(this.state);
    assessmentModel = getAssessmentDetailModel(this.state);
    canAddRecommendation = false;
    canEditRecommendation = false;
    canShowContextMenu = false;
    canShowContextMenuOptions = false;
    canShowNeedsMoreInfoStateChangeForm = false;
    canShowException = true;
    canShowRemediation = true;
    contextMenuOptions: Array<INameId<string>>;
    isDeleting = false;
    isPaneLoading = false;
    needsMoreInfoStateChangeInput: string;
    previewMode = true;
    primaryButtonObj: INeedsMoreInfoButtonConfig;
    permissionNMI: IStringMap<boolean>;
    questionId: string;
    questionSequence: string;
    questionTitle: string;
    rootRequestId: string;
    needsMoreInfoResponseList: INeedsMoreInfoResponse[];
    sectionId: string;
    secondaryButtonObj: INeedsMoreInfoButtonConfig;
    defaultDropdownDelays = [100, 100];
    showQuestionEditMessage = false;
    showAllowQuestionEditingToggle = false;
    questionEditEnabled = false;
    isReadOnly = false;
    cancelButtonConfig = {
        identifier: "AssessmentNeedsMoreInfoCancelRespondButton",
        isDisabled: false,
        isLoading: false,
        id: NeedsMoreInfoActions.CancelRespond,
    };
    customModules: any[] = AAQuillEditor;

    constructor(
        @Inject(StoreToken) private store: IStore,
        @Inject(ModalService) private modalService: ModalService,
        private translatePipe: TranslatePipe,
        private permissions: Permissions,
        private aaNeedsMoreInfoActionService: AANeedsMoreInfoActionService,
        private NeedsMoreInfoApi: AANeedsMoreInfoApiService,
        private assessmentDetailLogic: AssessmentDetailService,
        private aaCollaborationPaneCountsService: AaCollaborationPaneCountsService,
    ) { }

    ngOnChanges(changes: SimpleChanges): void {
        if (changes.needsMoreInfoRequest) {
            this.initialize();
            this.resetStateChangeForm();
            this.resetQuestionEditMessageOptions();
            this.sectionId = changes.needsMoreInfoRequest.currentValue.sectionId;
            this.questionId = changes.needsMoreInfoRequest.currentValue.questionId;
            this.rootRequestId = changes.needsMoreInfoRequest.currentValue.rootRequestId;
            this.assessmentId = changes.needsMoreInfoRequest.currentValue.assessmentId;
            this.questionSequence = getAssessmentQuestionSequence(changes.needsMoreInfoRequest.currentValue.questionId)(this.store.getState());
            this.questionTitle = getAssessmentQuestionTitle(changes.needsMoreInfoRequest.currentValue.questionId)(this.store.getState());
            this.updateResponses(this.assessmentId, this.rootRequestId);
            this.updateActionButtonsConfig(changes.needsMoreInfoRequest.currentValue.responseState);
        }
    }

    initialize(): void {
        const currentUser: IUser = getCurrentUser(this.store.getState());
        const isRespondent: boolean = getAssessmentDetailModel(this.store.getState()).respondents.some((respondent: INameId<string>) => respondent.id === currentUser.Id);
        this.permissionNMI = {
            canShowContextMenu: this.permissions.canShow(AssessmentPermissions.AssessmentDeleteInfoRequest),
            deleteRequest: this.permissions.canShow(AssessmentPermissions.AssessmentDeleteInfoRequest),
            canRespondToInfoRequest: this.permissions.canShow(AssessmentPermissions.AssessmentRespondtoInfoRequest) || isRespondent,
            canCloseInfoRequest: this.permissions.canShow(AssessmentPermissions.AssessmentCloseInfoRequest),
        };
        this.setContextMenuOptions();
        this.assessmentDetailLogic.fetchAssessmentReadOnlyState().subscribe((readonly: boolean) => {
            this.isReadOnly = readonly;
        });
    }

    hidePane(scrollToQuestion: boolean): void {
        this.hide.emit({ questionId: this.questionId, scrollToQuestion });
    }

    toggleContextMenu(value: boolean): void {
        this.canShowContextMenuOptions = value;
    }

    resetStateChangeForm(): void {
        this.previewMode = true;
    }

    initializeStateChangeForm(): void {
        this.canShowNeedsMoreInfoStateChangeForm = true;
        this.primaryButtonObj.isDisabled = true;
        this.needsMoreInfoStateChangeInput = "";
    }

    resetQuestionEditMessageOptions(): void {
        this.showAllowQuestionEditingToggle = false;
        this.showQuestionEditMessage = false;
        this.questionEditEnabled = false;
    }

    updateResponses(assessmentId: string, rootRequestId: string) {
        this.aaNeedsMoreInfoActionService.getAllAssessmentRequestResponses(assessmentId, rootRequestId)
            .then((res: INeedsMoreInfoResponse[]): void => {
                this.needsMoreInfoResponseList = res;
            });
    }

    onPrimaryActionClick(buttonId: string): void {
        switch (buttonId) {
            case NeedsMoreInfoActions.Respond:
                this.setRespondClickedStateActionButtons();
                this.initializeStateChangeForm();
                break;
            case NeedsMoreInfoActions.ResponseSend:
                this.saveResponse();
                this.resetStateChangeForm();
                break;
            case NeedsMoreInfoActions.ReOpenSend:
                this.reOpenRequest();
                this.resetStateChangeForm();
                this.resetQuestionEditMessageOptions();
                break;
        }
    }

    onSecondaryActionClick(buttonId: string): void {
        switch (buttonId) {
            case NeedsMoreInfoActions.CancelRespond:
                this.resetStateChangeForm();
                this.updateActionButtonsConfig(buttonId);
                break;
            case NeedsMoreInfoActions.CancelReOpen:
                this.resetStateChangeForm();
                this.updateActionButtonsConfig(buttonId);
                this.resetQuestionEditMessageOptions();
                break;
            case NeedsMoreInfoActions.ReOpen:
                this.setReOpenClickedStateActionButtons();
                this.initializeStateChangeForm();
                break;
            case NeedsMoreInfoActions.CloseRequest:
                this.closeRequest();
                break;
        }
    }

    onNeedsMoreInfoStateInputChange(value: string): void {
        this.needsMoreInfoStateChangeInput = value;
        this.primaryButtonObj.isDisabled = this.needsMoreInfoStateChangeInput ? false : true;
    }

    onContextMenuOptionClick(optionId: string): void {
        switch (optionId) {
            case NeedsMoreInfoActions.Delete:
                this.openDeleteNeedsMoreInfoModal();
                break;
            default:
                break;
        }
    }

    saveResponse(): void {
        this.setButtonLoading("PRIMARY", true);
        this.aaNeedsMoreInfoActionService.addNeedsMoreInfoResponse(
            this.assessmentId,
            this.sectionId,
            this.questionId,
            this.needsMoreInfoRequest.rootRequestId,
            this.needsMoreInfoStateChangeInput).then((needsMoreInfoResponse: INeedsMoreInfoModel): void => {
                this.setButtonLoading("PRIMARY", false);
                const selectedNeedsMoreInfo = needsMoreInfoResponse.rootRequestInformationResponses.find((nmiRequest) => nmiRequest.rootRequestId === this.needsMoreInfoRequest.rootRequestId);
                this.updateResponses(this.assessmentId, this.rootRequestId);
                this.updateActionButtonsConfig(selectedNeedsMoreInfo.responseState);
                this.needsMoreInfoStateChangeInput = "";
            });
    }

    trackByOptionId(index: number, option: INameId<string>): string {
        return option.id;
    }

    onToggle(questionEditEnabled: boolean) {
        this.questionEditEnabled = questionEditEnabled;
    }

    private closeRequest(): void {
        this.setButtonLoading("SECONDARY", true);
        this.aaNeedsMoreInfoActionService.closeNeedsMoreInfoRequest(
            this.assessmentId,
            this.needsMoreInfoRequest.rootRequestId)
            .then((needsMoreInfoResponse: INeedsMoreInfoModel): void => {
                this.setButtonLoading("SECONDARY", false);
                this.updateActionButtonsConfig(NeedsMoreInfoActions.Closed);
                const selectedNeedsMoreInfo = needsMoreInfoResponse.rootRequestInformationResponses.find((nmiRequest) => nmiRequest.rootRequestId === this.needsMoreInfoRequest.rootRequestId);
                this.updateRequestEvent.emit({ ...selectedNeedsMoreInfo });
                this.canShowNeedsMoreInfoStateChangeForm = false;
            });
    }

    private reOpenRequest(): void {
        this.setButtonLoading("PRIMARY", true);
        this.aaNeedsMoreInfoActionService.reOpenNeedsMoreInfoRequest(
            this.assessmentId,
            this.sectionId,
            this.questionId,
            this.needsMoreInfoRequest.rootRequestId,
            this.needsMoreInfoStateChangeInput,
            this.questionEditEnabled)
            .then((needsMoreInfoResponse: INeedsMoreInfoModel): void => {
                this.setButtonLoading("PRIMARY", false);
                this.updateActionButtonsConfig(NeedsMoreInfoActions.Open);
                const selectedNeedsMoreInfo = needsMoreInfoResponse.rootRequestInformationResponses.find((nmiRequest) => nmiRequest.rootRequestId === this.needsMoreInfoRequest.rootRequestId);
                this.updateResponses(this.assessmentId, this.rootRequestId);
                this.updateRequestEvent.emit({ ...selectedNeedsMoreInfo });
                this.needsMoreInfoStateChangeInput = "";
            });
    }

    private setButtonLoading(type: "PRIMARY" | "SECONDARY", value: boolean) {
        if (type === "PRIMARY") {
            this.primaryButtonObj.isLoading = value;
            this.primaryButtonObj.isDisabled = value;
            if (this.secondaryButtonObj) {
                this.secondaryButtonObj.isDisabled = value;
            }
        } else if (type === "SECONDARY") {
            this.secondaryButtonObj.isLoading = value;
            this.secondaryButtonObj.isDisabled = value;
            if (this.secondaryButtonObj) {
                this.secondaryButtonObj.isDisabled = value;
            }
        }
    }

    private updateActionButtonsConfig(state: string): void {
        switch (state) {
            case NeedsMoreInfoActions.Open:
                this.setRequestOpenStateActionButtons();
                break;
            case NeedsMoreInfoActions.CancelReOpen:
            case NeedsMoreInfoActions.Closed:
                this.setRequestCloseStateAction();
                break;
            case NeedsMoreInfoActions.Respond:
                this.setRespondClickedStateActionButtons();
                break;
            case NeedsMoreInfoActions.CancelRespond:
                this.setCancelClickedStateActionButtons();
                break;
        }
    }

    private setRequestOpenStateActionButtons(): void {
        this.primaryButtonObj = null;
        this.secondaryButtonObj = null;
        if (this.permissionNMI.canRespondToInfoRequest) {
            this.primaryButtonObj = {
                name: this.translatePipe.transform("Respond"),
                identifier: "AssessmentNeedsMoreInfoRespondButton",
                isDisabled: false,
                isLoading: false,
                id: NeedsMoreInfoActions.ResponseSend,
            };
            this.cancelButtonConfig.identifier = "AssessmentNeedsMoreInfoCancelRespondButton";
            this.cancelButtonConfig.id = NeedsMoreInfoActions.CancelRespond;
            this.canShowNeedsMoreInfoStateChangeForm = true;
        }
        if (this.permissionNMI.canCloseInfoRequest) {
            this.secondaryButtonObj = {
                name: this.translatePipe.transform("CloseRequestNeedsMoreInfo"),
                identifier: "AssessmentNeedsMoreInfoCloseRequestButton",
                isDisabled: false,
                isLoading: false,
                id: NeedsMoreInfoActions.CloseRequest,
            };
        }
    }

    private setRequestCloseStateAction(): void {
        this.primaryButtonObj = null;
        this.secondaryButtonObj = null;
        if (this.permissionNMI.canCloseInfoRequest) {
            this.secondaryButtonObj = {
                name: this.translatePipe.transform("ReOpenRequest"),
                identifier: "AssessmentNeedsMoreInfoReOpenButton",
                isDisabled: false,
                isLoading: false,
                id: NeedsMoreInfoActions.ReOpen,
            };
            this.canShowNeedsMoreInfoStateChangeForm = false;
        }
    }

    private setRespondClickedStateActionButtons(): void {
        this.primaryButtonObj = null;
        this.secondaryButtonObj = null;
        if (this.permissionNMI.canRespondToInfoRequest) {
            this.primaryButtonObj = {
                name: this.translatePipe.transform("Send"),
                identifier: "AssessmentNeedsMoreInfoSendButton",
                isDisabled: false,
                isLoading: false,
                id: NeedsMoreInfoActions.ResponseSend,
            };
            this.canShowNeedsMoreInfoStateChangeForm = true;
        }

        if (this.permissionNMI.canCloseInfoRequest) {
            this.cancelButtonConfig.identifier = "AssessmentNeedsMoreInfoCancelRespondButton";
            this.cancelButtonConfig.id = NeedsMoreInfoActions.CancelRespond;
        }
    }

    private setReOpenClickedStateActionButtons(): void {
        this.primaryButtonObj = null;
        this.secondaryButtonObj = null;
        if (this.permissionNMI.canRespondToInfoRequest) {
            this.primaryButtonObj = {
                name: this.translatePipe.transform("Save"),
                identifier: "AssessmentNeedsMoreInfoReOpenSaveButton",
                isDisabled: false,
                isLoading: false,
                id: NeedsMoreInfoActions.ReOpenSend,
            };

            if (this.questionId) {
                this.showQuestionEditMessage = true;
                this.showAllowQuestionEditingToggle = this.needsMoreInfoRequest.canReopenWithAllowEditOption;
            }

            this.canShowNeedsMoreInfoStateChangeForm = true;
        }

        if (this.permissionNMI.canCloseInfoRequest) {
            this.cancelButtonConfig.identifier = "AssessmentNeedsMoreInfoReOpenCancelButton";
            this.cancelButtonConfig.id = NeedsMoreInfoActions.CancelReOpen;
        }
    }

    private setCancelClickedStateActionButtons(): void {
        this.setRequestOpenStateActionButtons();
        this.needsMoreInfoStateChangeInput = "";
    }

    private setContextMenuOptions(): void {
        this.contextMenuOptions = [];
        if (this.permissionNMI.canShowContextMenu) {
            this.contextMenuOptions.push({
                name: this.translatePipe.transform("Delete"),
                id: NeedsMoreInfoActions.Delete,
            });
        }
    }

    private openDeleteNeedsMoreInfoModal(): void {
        this.modalService.setModalData({
            modalTitle: this.translatePipe.transform("DeleteNeedsMoreInfo"),
            confirmationText: this.translatePipe.transform("DeleteNeedsMoreInfoWarning"),
            cancelButtonText: this.translatePipe.transform("Cancel"),
            submitButtonText: this.translatePipe.transform("Delete"),
            promiseToResolve: (): ng.IPromise<boolean> =>
                this.NeedsMoreInfoApi.deleteNeedsMoreInfoRequest(this.assessmentId, this.needsMoreInfoRequest.rootRequestId)
                    .then((response: IProtocolResponse<void>): boolean => {
                        if (response.result) {
                            this.deleteInfo.emit(this.needsMoreInfoRequest.rootRequestId);
                            this.aaNeedsMoreInfoActionService.getAllAssessmentRequestDetails(this.assessmentId);
                            this.aaCollaborationPaneCountsService.getCollaborationPaneCounts(this.assessmentId, AACollaborationCountsTypes.Nmi);
                            this.resetStateChangeForm();
                        }
                        return response.result;
                    }),
        });
        this.modalService.openModal("deleteConfirmationModal");
    }
}
