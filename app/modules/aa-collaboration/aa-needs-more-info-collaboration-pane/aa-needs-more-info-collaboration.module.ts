// Modules
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { VitreusModule } from "@onetrust/vitreus";
import { SharedModule } from "./../../shared/shared.module";
import { OTPipesModule } from "modules/pipes/pipes.module";

// Components
import { AANeedsMoreInfoSubpaneDrawerComponent } from "./aa-needs-more-info-subpane-drawer/aa-needs-more-info-subpane-drawer.component";
import { AaNeedsMoreInfoCollaborationPaneComponent } from "./aa-needs-more-info-collaboration-pane.component";

@NgModule({
    imports: [
        CommonModule,
        VitreusModule,
        SharedModule,
        OTPipesModule,
    ],
    exports: [
        AaNeedsMoreInfoCollaborationPaneComponent,
    ],
    entryComponents: [
        AaNeedsMoreInfoCollaborationPaneComponent,
    ],
    declarations: [
        AANeedsMoreInfoSubpaneDrawerComponent,
        AaNeedsMoreInfoCollaborationPaneComponent,
    ],
})
export class AANeedsMoreInfoCollaborationModule { }
