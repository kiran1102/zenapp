import { EmptyGuid } from "constants/empty-guid.constant";
import { Regex } from "constants/regex.constant";
import { matchRegex } from "modules/utils/match-regex";

export function isValidGuidOrEmail(item, idKey: string = null): boolean {
    return item && ((matchRegex(item[idKey], Regex.GUID) && item[idKey] !== EmptyGuid) || matchRegex(item, Regex.EMAIL));
}
