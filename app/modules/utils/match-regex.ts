import { isString, isFunction } from "lodash";

export function matchRegex(str: string, re: RegExp): boolean {
    return isString(str) && re && isFunction(re.test) && re.test(str);
}
