import { ISmtpSettingsResponse } from "interfaces/smtp-settings-response.interface";
import { ISmtpSetting } from "interfaces/smtp-settings.interface";
import { ISmtpSettingRequest } from "interfaces/smtp-settings-request.interface";

export class SettingSmtpAdapterService {

    static $inject: string[] = [];

    private smtpSettingKeys = {
        smtpEnabled: "SMTPENABLED",
        smtpHostname: "SMTPHOSTNAME",
        smtpInstalled: "SMTPINSTALLED",
        smtpPassword: "SMTPPASSWORD",
        smtpPort: "SMTPPORT",
        smtpSenderEmail: "SMTPSENDEREMAIL",
        smtpSenderName: "SMTPSENDERNAME",
        smtpSslTls: "SMTPSSL",
        smtpTested: "SMTPTESTED",
        smtpUsername: "SMTPUSERNAME",
        htmlButtonBgColor: "HTMLBUTTONBGCOLOR",
        htmlButtonFgColor: "HTMLBUTTONFGCOLOR",
    };

    convertSettingResponseStructure(res: ISmtpSettingsResponse): ISmtpSetting {
        const {
            smtpEnabled,
            smtpHostname,
            smtpInstalled,
            smtpPassword,
            smtpPort,
            smtpSenderEmail,
            smtpSenderName,
            smtpSslTls,
            smtpTested,
            smtpUsername,
            htmlButtonBgColor,
            htmlButtonFgColor,
        } = this.smtpSettingKeys;

        const smtpSetting = {} as ISmtpSetting;

        for (const item of res.content) {
            switch (item.name) {
                case (smtpEnabled):
                    smtpSetting.smtpEnabled = Boolean(Number(item.value));
                    continue;
                case (smtpHostname):
                    smtpSetting.smtpHostname = item.value as string;
                    continue;
                case (smtpInstalled):
                    smtpSetting.smtpInstalled = Boolean(Number(item.value)); // backend sends "0" or "1" as strings
                    continue;
                case (smtpPassword):
                    smtpSetting.smtpPassword = item.value as string;
                    continue;
                case (smtpPort):
                    smtpSetting.smtpPort = item.value as number;
                    continue;
                case (smtpSenderEmail):
                    smtpSetting.smtpSenderEmail = item.value as string;
                    continue;
                case (smtpSenderName):
                    smtpSetting.smtpSenderName = item.value as string;
                    continue;
                case (smtpSslTls):
                    smtpSetting.smtpSslTls = Boolean(Number(item.value)); // backend sends "0" or "1" as strings
                    continue;
                case (smtpTested):
                    smtpSetting.smtpTested = Boolean(Number(item.value)); // backend sends "0" or "1" as strings
                    continue;
                case (smtpUsername):
                    smtpSetting.smtpUsername = item.value as string;
                    continue;
                case (htmlButtonBgColor):
                    smtpSetting.htmlButtonBgColor = item.value as string;
                    continue;
                case (htmlButtonFgColor):
                    smtpSetting.htmlButtonFgColor = item.value as string;
                    continue;
            }
        }

        return smtpSetting;
    }

    convertSettingRequestStructure(smtpSetting: ISmtpSetting): ISmtpSettingRequest {
        const {
            smtpEnabled,
            smtpHostname,
            smtpInstalled,
            smtpPassword,
            smtpPort,
            smtpSenderEmail,
            smtpSenderName,
            smtpSslTls,
            smtpTested,
            smtpUsername,
        } = this.smtpSettingKeys;
        const request = {} as ISmtpSettingRequest;

        request.validate = !!smtpSetting.validate;
        request.settings = {
            [smtpHostname]: smtpSetting.smtpHostname,
            [smtpPort]: smtpSetting.smtpPort,
            [smtpSslTls]: smtpSetting.smtpSslTls ? 1 : 0,
            [smtpUsername]: smtpSetting.smtpUsername,
            [smtpPassword]: smtpSetting.smtpPassword,
            [smtpSenderName]: smtpSetting.smtpSenderName,
            [smtpSenderEmail]: smtpSetting.smtpSenderEmail,
            [smtpInstalled]: smtpSetting.smtpEnabled ? 1 : 0,
        };

        return request;
    }

}
