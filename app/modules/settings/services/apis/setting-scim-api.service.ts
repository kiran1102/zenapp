// Angular
import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
// rxjs
import { Observable } from "rxjs";

// Service
import { TranslateService } from "@ngx-translate/core";

// Interface
import {
    IScimSettingGenerateCreds,
    IScimSettingGenerateCredsResponse,
    IScimSettings,
 } from "../../components/setting-scim/setting-scim.interface";
import { IRole } from "interfaces/role.interface";

// constants
import { UserRoles } from "constants/user-roles.constant";

// lodash
import {
    isArray,
    map,
 } from "lodash";
@Injectable()
export class SettingScimApiService {

    translations = {};
    keys = [
        "SiteAdmin",
        "SiteAdminHasAccessToApplicationSettings",
        "PrivacyOfficer",
        "APrivacyOfficerHasAccessToAllLevel",
        "ProjectOwner",
        "AProjectOwnerCanCreateAndCompleteProject",
        "InvitedUser",
        "AnInvitedUserIsInvitedToTheApplication",
        "ProjectViewer",
        "AProjectViewerHasViewOnlyAccess",
        "AssessmentsManager",
        "AnAssessmentsManagerCanAccessAssessmentsModule",
        "ConsentManager",
        "AConsentManagerCanAccessConsentModule",
        "CookieManager",
        "ACookieManagerCanAccessCookieModule",
        "DataMappingManager",
        "ADataMappingManagerCanAccessDMModule",
        "DataSubjectRequestsManager",
        "ADSARManagerCanAccessDSARModule",
        "IncidentsManager",
        "AnIncidentsManagerCanAccessIncidentsModule",
        "VendorManager",
        "AVendorManagerCanAccessVendorModule",
        "AGenericUser",
    ];

    constructor(
        private http: HttpClient,
        private translateService: TranslateService,
    ) {
        this.translateService
            .stream(this.keys)
            .subscribe((values) => (this.translations = values));
    }

    listRoles(assignable: boolean, orgId: string): Observable<IRole[]> {
        return this.http.get<IRole[]>(`/role?isAssignable=${assignable}&organizationId=${orgId}`);
    }

    translateRoleNamesAndDescription(roles: IRole[]): IRole[] {
        if (!isArray(roles)) return roles;
        return map(roles, (role: IRole): IRole => {
            return this.translateRoleProperties(role);
        });
    }

    translateRoleProperties(role: IRole): IRole {
        if (!role) return role;
        switch (role.Name) {
            case UserRoles.SiteAdmin:
                role.Name = this.translations["SiteAdmin"];
                role.Description = this.translations["SiteAdminHasAccessToApplicationSettings"];
                break;
            case UserRoles.PrivacyOfficer:
                role.Name = this.translations["PrivacyOfficer"];
                role.Description = this.translations["APrivacyOfficerHasAccessToAllLevel"];
                break;
            case UserRoles.ProjectOwner:
                role.Name = this.translations["ProjectOwner"];
                role.Description = this.translations["AProjectOwnerCanCreateAndCompleteProject"];
                break;
            case UserRoles.Invited:
                role.Name = this.translations["InvitedUser"];
                role.Description = this.translations["AnInvitedUserIsInvitedToTheApplication"];
                break;
            case UserRoles.ProjectViewer:
                role.Name = this.translations["ProjectViewer"];
                role.Description = this.translations["AProjectViewerHasViewOnlyAccess"];
                break;
            case UserRoles.AssessmentsManager:
                role.Name = this.translations["AssessmentsManager"];
                role.Description = this.translations["AnAssessmentsManagerCanAccessAssessmentsModule"];
                break;
            case UserRoles.ConsentManager:
                role.Name = this.translations["ConsentManager"];
                role.Description = this.translations["AConsentManagerCanAccessConsentModule"];
                break;
            case UserRoles.CookieManager:
                role.Name = this.translations["CookieManager"];
                role.Description = this.translations["ACookieManagerCanAccessCookieModule"];
                break;
            case UserRoles.DataMappingManager:
                role.Name = this.translations["DataMappingManager"];
                role.Description = this.translations["ADataMappingManagerCanAccessDMModule"];
                break;
            case UserRoles.DataSubjectRequestsManager:
                role.Name = this.translations["DataSubjectRequestsManager"];
                role.Description = this.translations["ADSARManagerCanAccessDSARModule"];
                break;
            case UserRoles.IncidentsManager:
                role.Name = this.translations["IncidentsManager"];
                role.Description = this.translations["AnIncidentsManagerCanAccessIncidentsModule"];
                break;
            case UserRoles.VendorManager:
                role.Name = this.translations["VendorManager"];
                role.Description = this.translations["AVendorManagerCanAccessVendorModule"];
                break;
            default:
                if (!role.Description) {
                    role.Description = this.translations["AGenericUser"];
                }
                break;
        }
        return role;
    }

    getScimSettings(): Observable<IScimSettings[]> {
        return this.http.get<IScimSettings[]>(`/api/scim/v1/Settings`);
    }

    saveScimSettings(requestBody: IScimSettings[], hasId?: boolean) {
        const method = hasId ? "PUT" : "POST";
        return this.http.request(method, `/api/scim/v1/Settings`, { body: requestBody });
    }

    generateScimCreds(config: IScimSettingGenerateCreds): Observable<IScimSettingGenerateCredsResponse> {
        return this.http.post<IScimSettingGenerateCredsResponse>(`/api/access/v1/client-credentials/generate`, config);
    }

    getCredentials(): Observable<IScimSettingGenerateCreds> {
        return this.http.get<IScimSettingGenerateCreds>(`/api/access/v1/client-credentials/scopes/SCIM`);
    }

    updateClientCredentials(clientDetails: IScimSettingGenerateCreds): Observable<IScimSettingGenerateCreds> {
        return this.http.put<IScimSettingGenerateCreds>(`/api/access/v1/client-credentials/details/${clientDetails.clientDetailId}`, clientDetails);
    }

}
