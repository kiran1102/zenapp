// Angular
import { Injectable } from "@angular/core";

// 3rd party
import {
    Observable,
    from,
} from "rxjs";

// Services
import { ProtocolService } from "modules/core/services/protocol.service";

// Interfaces
import {
    IProtocolConfig,
    IProtocolMessages,
    IProtocolResponse,
} from "interfaces/protocol.interface";
import { ITemplateSettings } from "modules/template/interfaces/template.interface";
import { ISspTemplatesRestrictView } from "interfaces/self-service-portal/ssp.interface";

// Pipe
import { TranslatePipe } from "pipes/translate.pipe";

@Injectable()
export class SettingsAAApiService {

    constructor(
        private protocolService: ProtocolService,
        private translatePipe: TranslatePipe,
    ) { }

    getAssessmentTemplateSettings(): Observable<IProtocolResponse<ITemplateSettings>> {
        const config: IProtocolConfig = this.protocolService.customConfig("GET", `/api/template/v1/settings`);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("CouldNotRetrieveTemplateSettings") } };
        return from(this.protocolService.http(config, messages)
            .then((response: IProtocolResponse<ITemplateSettings>): any => response));
    }

    saveTemplateAccessControlSettings(templateSettings: ITemplateSettings): Observable<IProtocolResponse<boolean>> {
        const config: IProtocolConfig = this.protocolService.customConfig("POST", `/api/template/v1/settings`, null, { ...templateSettings });
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("CouldNotSaveTemplateAccessSettings") } };
        return from(this.protocolService.http(config, messages)
            .then((response: IProtocolResponse<ITemplateSettings>): any => response));
    }

    getAssessmentSettings(): Observable<IProtocolResponse<ISspTemplatesRestrictView>> {
        const config: IProtocolConfig = this.protocolService.customConfig("GET", `/assessment-v2/v2/assessments/setting`);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("CouldNotRetrieveSetting") } };
        return from(this.protocolService.http(config, messages));
    }

    saveAssessmentSettings(infoRequestBulkEmailAllowed: boolean): Observable<IProtocolResponse<ISspTemplatesRestrictView>> {
        const config: IProtocolConfig = this.protocolService.customConfig("POST", `/assessment-v2/v2/assessments/setting/assessments`, null, { infoRequestBulkEmailAllowed });
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("CouldNotSaveAssessmentSettings") } };
        return from(this.protocolService.http(config, messages));
    }
}
