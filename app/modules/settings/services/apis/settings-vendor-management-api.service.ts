// Angular
import { Injectable } from "@angular/core";

// Services
import { ProtocolService } from "modules/core/services/protocol.service";

// Interfaces
import {
    IProtocolConfig,
    IProtocolMessages,
    IProtocolResponse,
} from "interfaces/protocol.interface";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

// Rxjs
import {
    from,
    Observable,
} from "rxjs";

// interfaces
import { INameValue } from "modules/settings/interfaces/settings-vendor-management.interface";

@Injectable()
export class SettingsVendorManagementService {

    constructor(
        private protocolService: ProtocolService,
        private translatePipe: TranslatePipe,
    ) { }

    fetchVendorSettings(): Observable<IProtocolResponse<Array<INameValue<string>>>> {
        const config: IProtocolConfig = this.protocolService.customConfig("GET", `/api/vendor/v1/vendors/settings`);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("ErrorFetchingVendorSettings") } };
        return from(this.protocolService.http(config, messages));
    }

    saveSettingsVendorManagement(paylod: Array<INameValue<string>>): Observable<IProtocolResponse<Array<INameValue<string>>>> {
        const config: IProtocolConfig = this.protocolService.customConfig("PUT", `/api/vendor/v1/vendors/settings`, null, paylod);
        const messages: IProtocolMessages = {
            Error: { custom: this.translatePipe.transform("ErrorSavingSettingsVendorManagement") },
            Success: { custom: this.translatePipe.transform("SettingsVendorManagementSavedSuccessfully") },
        };
        return from(this.protocolService.http(config, messages));
    }
}
