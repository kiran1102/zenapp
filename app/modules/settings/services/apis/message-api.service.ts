// Angular
import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";

// Vitreus
import { ToastService } from "@onetrust/vitreus";

// Service
import { TranslateService } from "@ngx-translate/core";

// rxjs
import { of } from "rxjs";
import { map, catchError } from "rxjs/operators";

// Interfaces
import {
    IBindToTemplateScopeResponse,
    IMessageTemplateData,
} from "interfaces/email-template.interface";

export interface ISendMessageRequest {
    messageTypeId: number;
    email: string;
    languageId?: number;
}

@Injectable()
export class MessageApiService {

    BODY_CONTENT = "<!--BodyContent-->";
    error = {
        result: false,
        data: null,
    };
    translations = {};
    keys = [
        "Error",
        "Success",
        "CouldNotRetrieveTerms",
        "SorryTemplateIsInvalid",
        "SorryTheSavedTemplateIsInvalid",
        "TemplateSaved",
        "SorryThereWasAProblemSavingTheTemplate",
        "EmailContentWillBeInsertedHereDoNotEditThisSection",
        "TestEmailSent",
        "SorryThereWasAProblemSendingTheTestEmail",
    ];

    constructor(
        private http: HttpClient,
        private translateService: TranslateService,
        private toastService: ToastService,
    ) {
        this.translateService
            .stream(this.keys)
            .subscribe((values) => (this.translations = values));
    }

    showSuccessMsg(key: string) {
        this.toastService.success(this.translations["Success"],
            this.translations[key],
        );
    }
    showErrorMessage(key: string) {
        this.toastService.error(this.translations["Error"],
            this.translations[key],
        );
    }

    getMessageTemplate(requestParams): Promise<IMessageTemplateData> {
        const config = {
            params: requestParams,
        };
        return this.http.get<IMessageTemplateData>(`/api/notification/v1/messageTemplates`, config)
        .pipe(
            map((res: IMessageTemplateData) => {
                return res;
            }),
            catchError((err) => {
                this.showErrorMessage("CouldNotRetrieveTerms");
                return of(err);
            }),
        )
        .toPromise();
    }

    saveMessageTemplate(template: IBindToTemplateScopeResponse, editTrue: boolean) {
        if (!editTrue && !this.processSubmittedTemplate(template)) {
            return new Promise((resolve) => {
                this.showErrorMessage("SorryTemplateIsInvalid");
                resolve(this.error);
            });
        }
        const params = { params: { version: "2"}};
        return this.http.post(`/api/notification/v1/messageTemplates`, template, params)
        .pipe(
            map((res: IBindToTemplateScopeResponse) => {
                if (!this.processRetrievedTemplate(res)) {
                    this.showErrorMessage("SorryTheSavedTemplateIsInvalid");
                    return this.error;
                }
                this.showSuccessMsg("TemplateSaved");
                return res;
            }),
            catchError((err) => {
                this.showErrorMessage("SorryThereWasAProblemSavingTheTemplate");
                return of(err);
            }),
        )
        .toPromise();
    }

    isBodyTextValid(bodyText: string) {
        // Check 1: Ensure bodyText contains BODY_CONTENT
        return bodyText && bodyText.indexOf(this.BODY_CONTENT) !== -1;
    }

    convertContentTag(bodyText: string, uiMode: boolean) {
        const contentMessage = this.translations["EmailContentWillBeInsertedHereDoNotEditThisSection"];
        if (uiMode) {
            // Switch for UI
            // BODY_CONTENT => contentMessage
            return bodyText.replace(this.BODY_CONTENT, contentMessage);
        }
        if (uiMode === false) {
            // Switch for Backend
            // contentMessage => BODY_CONTENT
            return bodyText.replace(contentMessage, this.BODY_CONTENT);
        }
        // Just return the template
        return bodyText;
    }

    processSubmittedTemplate(submission: IBindToTemplateScopeResponse) {
        submission.bodyText = this.convertContentTag(submission.bodyText, false);
        // Validate bodyText
        return this.isBodyTextValid(submission.bodyText);
    }

    processRetrievedTemplate(template: IBindToTemplateScopeResponse) {
        // Validate bodyText
        if (template.messageTypeId === 0 && !this.isBodyTextValid(template.bodyText)) {
            return false;
        }
        template.bodyText = this.convertContentTag(template.bodyText, true);
        return true;
    }

    sendTestMessage(testEmail: ISendMessageRequest) {
        return this.http.post(`/api/notification/v1/notifications/test`, testEmail)
        .pipe(
            map((res: boolean) => {
                this.showSuccessMsg("TestEmailSent");
                return res;
            }),
            catchError((err) => {
                this.showErrorMessage("SorryThereWasAProblemSendingTheTestEmail");
                return of(err);
            }),
        )
        .toPromise();
    }

    sendPreviewMessage(previewEmailSettings: ISendMessageRequest) {
        return this.http.post(`/api/notification/v1/notifications/preview`, previewEmailSettings)
        .pipe(
            map((res: boolean) => {
                this.showSuccessMsg("TestEmailSent");
                return res;
            }),
            catchError((err) => {
                this.showErrorMessage("SorryThereWasAProblemSendingTheTestEmail");
                return of(err);
            }),
        )
        .toPromise();
    }
}
