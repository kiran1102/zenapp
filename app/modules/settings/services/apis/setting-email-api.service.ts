import { ProtocolService } from "modules/core/services/protocol.service";
import { SettingSmtpAdapterService } from "./../adapters/setting-smtp-adapter.service";

import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";
import { ISmtpSettingsResponse } from "interfaces/smtp-settings-response.interface";
import { ISmtpSetting } from "interfaces/smtp-settings.interface";
import { Channel } from "enums/channel.enum";
import { ISmtpSettingRequest } from "interfaces/smtp-settings-request.interface";
import { NotificationService } from "modules/shared/services/provider/notification.service";

export class SettingEmailApiService {

    static $inject: string[] = [
        "$q",
        "$rootScope",
        "OneProtocol",
        "NotificationService",
    ];

    private translate: any;

    constructor(
        private $q: ng.IQService,
        $rootScope: IExtendedRootScopeService,
        private OneProtocol: ProtocolService,
        private notificationService: NotificationService,
    ) {
        this.translate = $rootScope.t;
    }

    saveEmailButtonColor(buttonBgColor: string, buttonFgColor: string): ng.IPromise<boolean> {
        const request: ISmtpSettingRequest = {
            validate: false,
            settings: {
                HTMLBUTTONFGCOLOR: buttonFgColor,
                HTMLBUTTONBGCOLOR: buttonBgColor,
            },
        };

        const config: any = this.OneProtocol.config("POST", `/api/notification/v1/channels/${Channel.Smtp}/settings`, null, request);
        const messages: any = { Error: { Hide: true } };

        return this.OneProtocol.http(config, messages)
            .then((res: any): boolean => {
                if (!res.result) {
                    this.notificationService.alertError(this.translate("Error"), this.translate("ErrorSavingButtonBranding"));
                    return false;
                }
                this.notificationService.alertSuccess(this.translate("Success"), this.translate("ButtonBrandingSaved"));
                return true;
            });
    }

}
