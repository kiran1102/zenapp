// Angular
import { Injectable } from "@angular/core";

// Interfaces
import {
    IProtocolResponse,
    IProtocolMessages,
    IProtocolConfig,
} from "interfaces/protocol.interface";
import {
    ILocalizationParams,
    ITranslationsPage,
    ITranslation,
    ITenantTranslations,
} from "interfaces/localization.interface";

// Constants
import { LanguageCodes } from "constants/language-codes.constant";
import { TranslationModules } from "constants/translation.constant";
import { EmptyGuid } from "constants/empty-guid.constant";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

// Rxjs
import {
    from,
    Observable,
} from "rxjs";

// Services
import { ProtocolService } from "modules/core/services/protocol.service";

@Injectable()
export class LocalizationApiService {

    constructor(
        private protocolService: ProtocolService,
        private translatePipe: TranslatePipe,
    ) { }

    public getSystemTranslationsLegacy(params: ILocalizationParams): Observable<IProtocolResponse<ITenantTranslations>> {
        const config: IProtocolConfig = this.protocolService.customConfig("GET", `/api/globalization/v1/translations/${params.language}`, params);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("CouldNotRetrieveTerms") } };
        return from(this.protocolService.http(config, messages));
    }

    public getSystemTranslations(params: ILocalizationParams): Observable<IProtocolResponse<ITenantTranslations>> {
        let url;
        if (params) {
            url = params.tenantId === EmptyGuid ? `/api/globalization/v1/translations/seed/${params.language}` : `/api/globalization/v1/translations/${params.language}`;
        }
        const messages: IProtocolMessages = { Error: { Hide: true } };
        const config: IProtocolConfig = this.protocolService.customConfig("GET", url, params);
        return from(this.protocolService.http(config, messages));
    }

    public getTranslationsPage(params: ILocalizationParams = { page: 0, size: 20 }, language: string = LanguageCodes.English, module: string = TranslationModules.System): ng.IPromise<IProtocolResponse<ITranslationsPage>> {
        let config;
        if (module === TranslationModules.System) {
            params = { language, ...params };
            config = this.protocolService.customConfig("GET", `/api/globalization/v1/translations/${language}`, params);
        } else {
            // TODO replace "custom" path variable with module when available
            config = this.protocolService.customConfig("GET", `/api/globalization/v1/translations/${language}/custom`, params);
        }
        const messages: any = { Error: { custom: this.translatePipe.transform("CouldNotRetrieveTerms") } };
        return this.protocolService.http(config, messages);
    }

    public getTranslationsByKey(key: string): ng.IPromise<IProtocolResponse<ITranslation[]>> {
        const config: any = this.protocolService.customConfig("GET", `/api/globalization/v1/translations`, { key });
        const messages: any = { Error: { custom: this.translatePipe.transform("CouldNotRetrieveTerms") } };
        return this.protocolService.http(config, messages);
    }

    public updateTranslations(terms: ITranslation[]): ng.IPromise<IProtocolResponse<void>> {
        const config: any = this.protocolService.customConfig("PUT", `/api/globalization/v1/translations`, null, terms);
        const messages: IProtocolMessages = {
            Success: { custom: this.translatePipe.transform("TermUpdatedSuccessfully")},
            Error: { custom: this.translatePipe.transform("CouldNotUpdateTerms") },
        };
        return this.protocolService.http(config, messages);
    }

    public restoreSingleTranslations(key: string): ng.IPromise<IProtocolResponse<void>> {
        const config: IProtocolConfig = this.protocolService.customConfig("DELETE", `/api/globalization/v1/translations/key/${encodeURI(key)}`);
        const messages: IProtocolMessages = {
            Success: { custom: this.translatePipe.transform("SuccessfullyRestoredValue")},
            Error: { custom: this.translatePipe.transform("ErrorInRestoringValues") },
        };
        return this.protocolService.http(config, messages);
    }

    public restoreAllTranslations(): ng.IPromise<IProtocolResponse<void>> {
        const config: IProtocolConfig = this.protocolService.customConfig("DELETE", `/api/globalization/v1/translations`);
        const messages: IProtocolMessages = {
            Success: { custom: this.translatePipe.transform("SuccessfullyRestoredAllValue") },
            Error: { custom: this.translatePipe.transform("ErrorInRestoringValues") },
        };
        return this.protocolService.http(config, messages);
    }

    public getTranslationsByModule(module: string, language: string): ng.IPromise<IProtocolResponse<ITenantTranslations>> {
        const config: any = this.protocolService.customConfig("GET", `/globalization/v1/translations/${language}/${module}`);
        const messages: any = { Error: { custom: this.translatePipe.transform("CouldNotRetrieveTerms") } };
        return this.protocolService.http(config, messages);
    }

}
