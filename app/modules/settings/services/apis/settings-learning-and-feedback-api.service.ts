// Angular
import { Injectable } from "@angular/core";

// Services
import { ProtocolService } from "modules/core/services/protocol.service";

// Interfaces
import {
    ILearningAndFeedbackSetting,
    ILearningAndFeedbackUserSetting,
} from "interfaces/settings-learning-and-feedback-tool.interface";
import {
    IProtocolConfig,
    IProtocolMessages,
    IProtocolResponse,
} from "interfaces/protocol.interface";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

@Injectable()
export class SettingsLearningAndFeedbackService {

    constructor(
        private oneProtocol: ProtocolService,
        private readonly translatePipe: TranslatePipe,
    ) { }

    getLearningAndFeedbackSettings(): ng.IPromise<IProtocolResponse<ILearningAndFeedbackSetting[]>> {
        const url = `/api/access/v1/tool-settings`;
        const config: IProtocolConfig = this.oneProtocol.customConfig("GET", url);
        const messages: IProtocolMessages = {};
        return this.oneProtocol.http(config, messages);
    }

    saveLearningAndFeedbackSettings(settings: ILearningAndFeedbackSetting[]): ng.IPromise<IProtocolResponse<ILearningAndFeedbackSetting[]>> {
        const url = `/api/access/v1/tool-settings`;
        const config: IProtocolConfig = this.oneProtocol.customConfig("PUT", url, null, settings);
        const messages: IProtocolMessages = {};
        return this.oneProtocol.http(config, messages);
    }

    // User Settings
    getLearningAndFeedbackUserSettings(): ng.IPromise<IProtocolResponse<ILearningAndFeedbackUserSetting[]>> {
        const url = `/api/access/v1/tool-settings/user-preferences`;
        const config: IProtocolConfig = this.oneProtocol.customConfig("GET", url);
        const messages: IProtocolMessages = {
            Error: { custom: this.translatePipe.transform("CouldNotUpdateSettings") },
        };
        return this.oneProtocol.http(config, messages);
    }

    saveLearningAndFeedbackUserSettings(settings: ILearningAndFeedbackUserSetting[]): ng.IPromise<IProtocolResponse<ILearningAndFeedbackUserSetting[]>> {
        const url = `/api/access/v1/tool-settings/user-preferences`;
        const config: IProtocolConfig = this.oneProtocol.customConfig("PUT", url, null, settings);
        const messages: IProtocolMessages = {
            Error: { custom: this.translatePipe.transform("CouldNotUpdateSettings") },
        };
        return this.oneProtocol.http(config, messages);
    }

    getFeedbackSettings(): ng.IPromise<IProtocolResponse<ILearningAndFeedbackSetting>> {
        const url = `/api/access/v1/tool-settings/user-preferences/UserFeedBack`;
        const config: IProtocolConfig = this.oneProtocol.customConfig("GET", url);
        const messages: IProtocolMessages = {};
        return this.oneProtocol.http(config, messages);
    }

    getLearningSettings(): ng.IPromise<IProtocolResponse<ILearningAndFeedbackSetting>> {
        const url = `/api/access/v1/tool-settings/user-preferences/UserGuidance`;
        const config: IProtocolConfig = this.oneProtocol.customConfig("GET", url);
        const messages: IProtocolMessages = {};
        return this.oneProtocol.http(config, messages);
    }
}
