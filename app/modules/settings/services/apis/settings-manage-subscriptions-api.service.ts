// Angular
import { Injectable } from "@angular/core";

// Services
import { Content } from "sharedModules/services/provider/content.service";
import { ProtocolService } from "modules/core/services/protocol.service";

// Interfaces
import { IStringMap } from "interfaces/generic.interface";
import {
    IProtocolConfig,
    IProtocolMessages,
    IProtocolResponse,
} from "interfaces/protocol.interface";
import { ISubscriptionManager } from "interfaces/settings-manage-subscriptions.interface";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

@Injectable()
export class SettingsManageSubscriptionsService {

    constructor(
        private content: Content,
        private oneProtocol: ProtocolService,
        private translatePipe: TranslatePipe,
    ) {}

    getRedirectionDetails(): ng.IPromise<IStringMap<string>> {
        return this.content.GetContent();
    }

    getCurrentAdmin(): ng.IPromise<IProtocolResponse<ISubscriptionManager>> {
        const url = `/api/access/v1/subscription-manager`;
        const config: IProtocolConfig = this.oneProtocol.customConfig("GET", url);
        const messages: IProtocolMessages = {};
        return this.oneProtocol.http(config, messages);
    }

    getAdminsList(): ng.IPromise<IProtocolResponse<ISubscriptionManager[]>> {
        const url = `/api/v1/private/orggroupuser/admins`;
        const config: IProtocolConfig = this.oneProtocol.customConfig("GET", url);
        const messages: IProtocolMessages = {};
        return this.oneProtocol.http(config, messages);
    }

    setSubscriptionManager(userId: string): ng.IPromise<IProtocolResponse<ISubscriptionManager>> {
        const url = `/api/access/v1/subscription-manager/${userId}`;
        const config: IProtocolConfig = this.oneProtocol.customConfig("PUT", url);
        const messages: IProtocolMessages = {
            Success: { custom: this.translatePipe.transform("OneTrustSubscriptionAdministratorSuccess") },
            Error: { custom: this.translatePipe.transform("OneTrustSubscriptionAdministratorFailure") },
        };
        return this.oneProtocol.http(config, messages);
    }
}
