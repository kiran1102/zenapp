// Angular
import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";

// Service
import { SettingSmtpAdapterService } from "./../adapters/setting-smtp-adapter.service";
import { TranslateService } from "@ngx-translate/core";

// Interfaces
import { ISmtpSettingsResponse } from "interfaces/smtp-settings-response.interface";
import { ISmtpSetting } from "interfaces/smtp-settings.interface";
import { ISmtpSettingRequest } from "interfaces/smtp-settings-request.interface";

// Vitreus
import { ToastService } from "@onetrust/vitreus";

// Rxjs
import { catchError, map } from "rxjs/operators";
import { of } from "rxjs";

// Enums
import { Channel } from "enums/channel.enum";

@Injectable()
export class SettingSmtpApiService {

    translations = {};
    keys = [
        "Error",
        "Success",
        "ErrorEnablingSMTPConnection",
        "ErrorDisablingSMTPConnection",
        "ErrorRetrievingSMTPSettings",
        "SMTPConnectionHasBeenInstalledAndTestedSuccessfully",
        "SMTPConnectionHasBeenInstalledSuccessfully",
        "SMTPConnectionHasBeenInstalledButTestConnectionFailed",
        "ErrorInstallingSMTPConnection",
    ];
    constructor(
        private settingSmtpAdapterService: SettingSmtpAdapterService,
        private translateService: TranslateService,
        private toastService: ToastService,
        private http: HttpClient,
    ) {
        this.translateService
            .stream(this.keys)
            .subscribe((values) => (this.translations = values));
    }

    showSuccessMsg(key: string) {
        this.toastService.success(this.translations["Success"],
            this.translations[key],
        );
    }
    showErrorMessage(key: string) {
        this.toastService.error(this.translations["Error"],
            this.translations[key],
        );
    }

    toggleSmtpConnection(enable: boolean): Promise<boolean> {
        const request: ISmtpSettingRequest = {
            validate: false,
            settings: {
                SMTPENABLED: enable ? 1 : 0,
            },
        };
        return this.http.post(`/api/notification/v1/channels/${Channel.Smtp}/settings`, request)
        .pipe(
            map(() => {
                return true;
            }),
            catchError((err) => {
                enable
                ? this.showErrorMessage("ErrorEnablingSMTPConnection")
                : this.showErrorMessage("ErrorDisablingSMTPConnection");
                return of(false);
            }),
        )
        .toPromise();
    }

    fetchSmtpSetting(): Promise<ISmtpSetting | boolean> {
        return this.http.get(`/api/notification/v1/settings`)
        .pipe(
            map((res) => {
                return this.settingSmtpAdapterService.convertSettingResponseStructure(res as ISmtpSettingsResponse);
            }),
            catchError((err) => {
                this.showErrorMessage("ErrorRetrievingSMTPSettings");
                return of(false);
            }),
        )
        .toPromise();
    }

    installSmtpConnection(smtpSetting: ISmtpSetting): Promise< boolean> {
        const request = this.settingSmtpAdapterService.convertSettingRequestStructure(smtpSetting);
        const testConnWhenInstalling = !!request.validate;
        return this.http.post(`/api/notification/v1/channels/10/settings`, request)
        .pipe(
            map(() => {
                testConnWhenInstalling
                ? this.showSuccessMsg("SMTPConnectionHasBeenInstalledAndTestedSuccessfully")
                : this.showSuccessMsg("SMTPConnectionHasBeenInstalledSuccessfully");
                return true;
            }),
            catchError((err) => {
                if (testConnWhenInstalling) {
                    this.showErrorMessage("SMTPConnectionHasBeenInstalledButTestConnectionFailed");
                }
                this.showErrorMessage("ErrorInstallingSMTPConnection");
                return of(false);
            }),
        )
        .toPromise();
    }

}
