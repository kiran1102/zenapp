import { ISmtpSettingsResponse } from "interfaces/smtp-settings-response.interface";
import {
    getSmtpSetting,
    getSmtpSettingFetchStatus,
    isSmtpConnectionEnabled,
    SettingActions,
    getTempSmtpSetting,
} from "oneRedux/reducers/setting.reducer";
import { IProtocolPacket } from "interfaces/protocol.interface";
import { IStore } from "interfaces/redux.interface";
import { SettingSmtpApiService } from "../apis/setting-smtp-api.service";
import { ISmtpSetting } from "interfaces/smtp-settings.interface";

export class SettingSmtpActionService {

    static $inject: string[] = ["store", "$q", "SettingSmtpApi"];

    constructor(
        private readonly store: IStore,
        private readonly $q: ng.IQService,
        private SettingSmtpApi: SettingSmtpApiService,
    ) { }

    addTempSmtpSetting(tempSmtpSetting: ISmtpSetting): void {
        this.store.dispatch({ type: SettingActions.ADD_TEMP_SMTP_SETTING, tempSmtpSetting });
    }

    addBlankSmtpSetting(): void {
        const blankSetting = {
            smtpEnabled: null,
            smtpHostname: null,
            smtpInstalled: null,
            smtpPassword: null,
            smtpSenderEmail: null,
            smtpSenderName: null,
            smtpTested: null,
            smtpUsername: null,
            validate: null,
        };

        this.addTempSmtpSetting(blankSetting);
    }

    updateTempSmtpSetting(keyValueToMerge: any): void {
        this.store.dispatch({ type: SettingActions.UPDATE_TEMP_SMTP_SETTING, keyValueToMerge: { ...keyValueToMerge } });
    }

    toggleSmtpConnection(): Promise<void> {
        const smtpSetting = getSmtpSetting(this.store.getState());

        if (!getSmtpSetting(this.store.getState())) {
            this.addBlankSmtpSetting();
        }

        const isSmtpEnabled = isSmtpConnectionEnabled(this.store.getState());
        const enable = !isSmtpEnabled;

        this.store.dispatch({ type: SettingActions.TOGGLE_SMTP_CONNECTION_IN_PROGRESS });

        return this.SettingSmtpApi.toggleSmtpConnection(enable).then((success: boolean): void => {
            if (success) {
                this.store.dispatch({ type: SettingActions.TOGGLE_SMTP_CONNECTION, smtpEnabled: enable });
            }
            this.store.dispatch({ type: SettingActions.TOGGLE_SMTP_CONNECTION_COMPLETE });
        });
    }

    addSmtpSetting(smtpSetting: ISmtpSetting): void {
        this.store.dispatch({ type: SettingActions.ADD_SMTP_SETTING, smtpSetting });
    }

    fetchSmtpSetting(): Promise<ISmtpSetting | boolean> {
        if (!getSmtpSettingFetchStatus(this.store.getState())) {
            this.store.dispatch({ type: SettingActions.FETCHING_SMTP_SETTING });

            return this.SettingSmtpApi.fetchSmtpSetting().then((res: ISmtpSetting | boolean): ISmtpSetting | boolean => {
                if (res) {
                    this.addSmtpSetting(res as ISmtpSetting);
                    this.addTempSmtpSetting(res as ISmtpSetting);

                    this.store.dispatch({ type: SettingActions.FETCHED_SMTP_SETTING });
                    return res;
                }
                return false;
            });
        }
        return new Promise((resolve) => {
            resolve(true);
        });
    }

    installSmtpConnection(): Promise<ISmtpSetting | boolean> {
        const tempSmtpSetting = getTempSmtpSetting(this.store.getState());
        return this.SettingSmtpApi.installSmtpConnection(tempSmtpSetting)
            .then((response: ISmtpSetting | boolean) => {
                if (response) {
                    return this.fetchSmtpSetting().then((res: ISmtpSetting | boolean) => {
                        if (res) {
                            this.store.dispatch({ type: SettingActions.SET_SMTP_SETTING_PENDING_CHANGES_TO_FALSE });
                        }
                        return res;
                    });
                }
                return response;
            });
    }
}
