// Angular
import { Injectable } from "@angular/core";

// 3rd party
import { Observable } from "rxjs";

// Interfaces
import { IProtocolResponse } from "interfaces/protocol.interface";
import { ITemplateSettings } from "modules/template/interfaces/template.interface";
import { ISspTemplatesRestrictView } from "interfaces/self-service-portal/ssp.interface";

// Services
import { SettingsAAApiService } from "settingsServices/apis/settings-assessment-api.service";

@Injectable()
export class SettingsAAActionService {

    constructor(
        private settingsAssessmentApiService: SettingsAAApiService,
    ) {}

    getAssessmentTemplateSettings(): Observable<IProtocolResponse<ITemplateSettings>> {
        return this.settingsAssessmentApiService.getAssessmentTemplateSettings();
    }

    saveTemplateAccessControlSettings(templateSettings: ITemplateSettings): Observable<IProtocolResponse<boolean>> {
        return this.settingsAssessmentApiService.saveTemplateAccessControlSettings(templateSettings);
    }

    getAssessmentSettings(): Observable<IProtocolResponse<ISspTemplatesRestrictView>> {
        return this.settingsAssessmentApiService.getAssessmentSettings();
    }

    saveAssessmentSettings(infoRequestBulkEmailAllowed: boolean): Observable<IProtocolResponse<ISspTemplatesRestrictView>> {
        return this.settingsAssessmentApiService.saveAssessmentSettings(infoRequestBulkEmailAllowed);
    }
}
