// 3rd Party
import { find } from "lodash";

// Redux
import { LocalizationActions } from "settingsReducers/localization.reducer";

// Services
import { LocalizationApiService } from "settingsServices/apis/localization-api.service";
import { TenantTranslationService } from "sharedServices/tenant-translation.service";
import { ProtocolService } from "modules/core/services/protocol.service";

// Interfaces
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import { IStore } from "interfaces/redux.interface";
import {
    IProtocolResponse,
    IProtocolConfig,
    IProtocolMessages,
 } from "interfaces/protocol.interface";
import {
    ITranslationsPage,
    ILocalizationParams,
    ITranslation,
} from "interfaces/localization.interface";

// rxjs
import {
    Observable,
    from,
} from "rxjs";

export class LocalizationActionService {
    static $inject: string[] = [
        "$rootScope",
        "$state",
        "store",
        "LocalizationApiService",
        "TenantTranslationService",
        "OneProtocol",
    ];

    private params: ILocalizationParams;

    private defaultParams: ILocalizationParams = {
        page: 1,
        size: 20,
        searchKey: "",
    };

    constructor(
        private $rootScope: IExtendedRootScopeService,
        private $state: ng.ui.IStateService,
        private store: IStore,
        private localizationApiService: LocalizationApiService,
        private tenantTranslationService: TenantTranslationService,
        private protocolService: ProtocolService,
    ) {}

    public fetchTranslations(params: ILocalizationParams, module: string): ng.IPromise<IProtocolResponse<ITranslationsPage>> {
        return this.localizationApiService.getTranslationsPage(params, this.getCurrentLanguage(), module);
    }

    public fetchTranslationsPage(params: ILocalizationParams, tabId: string): ng.IPromise<boolean> {
        params = { ... params, searchKey: params.searchKey || "" };
        this.store.dispatch({
            type: LocalizationActions.FETCHING_LOCALIZATION_PAGE,
        });
        return this.localizationApiService.getTranslationsPage(params, this.getCurrentLanguage(), tabId).then((response: IProtocolResponse<ITranslationsPage>): boolean => {
            if (response.result) {
                if (!response.data && !params.searchKey && params.page > 0) {
                    this.updateQueryParamsAndFetchTranslations({...this.params, page: 0}).then((): boolean => {
                        return response.result;
                    });
                } else {
                    this.setLocalizationPage(response.data);
                    return response.result;
                }
            } else {
                this.store.dispatch({
                    type: LocalizationActions.FETCHED_LOCALIZATION_PAGE,
                });
                return response.result;
            }
        });
    }

    public fetchQueryParams(): ILocalizationParams {
        this.params = {
            page: (parseInt(this.$state.params.page, 10) || this.defaultParams.page) - 1 || 0,
            size: parseInt(this.$state.params.size, 10) || this.defaultParams.size,
            searchKey: this.$state.params.search,
        };
        return this.params;
    }

    public updateQueryParamsAndFetchTranslations(params: ILocalizationParams): ng.IPromise<void> {
        // NOTE: Updating query params forces the current state to re-initialize.
        // This is due to the state "reloadOnSearch" being set to true by default.
        // With "reloadOnSearch" set to false, external navigation to the state would ignore params,
        // and the init function, or in this case, the fetchTranslations api,
        // would need to be called seperately.
        return this.updateQueryParams(params);
    }

    public fetchTranslationsByKey(key: string): ng.IPromise<IProtocolResponse<ITranslation[]>> {
        return this.localizationApiService.getTranslationsByKey(key);
    }

    public updateTranslations(terms: ITranslation[]): ng.IPromise<IProtocolResponse<void>> {
        return this.localizationApiService.updateTranslations(terms).then((response: IProtocolResponse<void>): IProtocolResponse<void> => {
            if (!response.result) return response;
            const currentLanguageUpdate = find(terms, (term: ITranslation): boolean => term.languageCode === this.getCurrentLanguage());
            if (!currentLanguageUpdate) return response;
            if (!currentLanguageUpdate.value) currentLanguageUpdate.value = currentLanguageUpdate.defaultValue;
            this.tenantTranslationService.addIndividualTranslations({ [currentLanguageUpdate.key]: currentLanguageUpdate.value });
            return response;
        });
    }

    public setLocalizationPage(translationsPage: ITranslationsPage): void {
        this.store.dispatch({
            type: LocalizationActions.SET_LOCALIZATION_DATA_AND_FETCHED,
            translationsPage,
        });
    }

    public resetPage(): void {
        this.store.dispatch({ type: LocalizationActions.RESET_LOCALIZATION });
    }

    public restoreSingleTranslations(key: string): ng.IPromise<IProtocolResponse<void>> {
        return this.localizationApiService.restoreSingleTranslations(key);
    }

    public restoreAllTranslations(): ng.IPromise<IProtocolResponse<void>> {
        return this.localizationApiService.restoreAllTranslations();
    }

    public getLocalizationExportFile(): Observable<IProtocolResponse<any>> {
        const url = `/globalization/v1/translations/export`;
        const config: IProtocolConfig = this.protocolService.customConfig("GET", url);
        const messages: IProtocolMessages = {
            Error: { custom: this.$rootScope.t("LocalizationExportError") },
        };
        return from(this.protocolService.http(config, messages));
    }

    private updateQueryParams(newParams: ILocalizationParams): ng.IPromise<void> {
        this.params = {...this.params, ...newParams};
        const routeParams = { ...this.params, page: this.params.page + 1, search: this.params.searchKey, time: new Date().getTime() };
        return this.$state.go(".", routeParams, { notify: false });
    }

    private getCurrentLanguage(): string {
        return this.tenantTranslationService.getCurrentLanguage();
    }

}
