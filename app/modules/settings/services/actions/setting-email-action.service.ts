import {
    getSmtpSetting,
    getSmtpSettingFetchStatus,
    isSmtpConnectionEnabled,
    SettingActions,
    getTempSmtpSetting,
} from "oneRedux/reducers/setting.reducer";

import { SettingEmailApiService } from "settingsServices/apis/setting-email-api.service";
import { ModalService } from "sharedServices/modal.service";

import { IProtocolPacket } from "interfaces/protocol.interface";
import { IStore } from "interfaces/redux.interface";
import { ISmtpSetting } from "interfaces/smtp-settings.interface";

export class SettingEmailActionService {

    static $inject: string[] = ["store", "$q", "SettingEmailApi", "ModalService"];

    constructor(
        private store: IStore,
        private $q: ng.IQService,
        private SettingEmailApi: SettingEmailApiService,
        private ModalService: ModalService,
    ) { }

    saveEmailButtonColor(buttonBgColor: string, buttonFgColor: string): ng.IPromise<boolean> {
        return this.SettingEmailApi.saveEmailButtonColor(buttonBgColor, buttonFgColor);
    }

}
