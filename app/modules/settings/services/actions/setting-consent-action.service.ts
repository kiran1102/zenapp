import { Injectable, Inject } from "@angular/core";
import { IPromise } from "angular";

// Redux
import {
    SettingActions,
    getConsentSettings,
    getConsentSettingsFetchStatus,
} from "oneRedux/reducers/setting.reducer";
import { StoreToken } from "tokens/redux-store.token";

// Interfaces
import { IStore } from "interfaces/redux.interface";
import { IConsentSettings } from "interfaces/consent-settings.interface";

// Services
import { ConsentSettingsService } from "crservice/cr-settings.service";

@Injectable()
export class SettingConsentActionService {
    constructor(
        @Inject(StoreToken) private store: IStore,
        private readonly ConsentSettings: ConsentSettingsService,
    ) { }

    addConsentSettings(consentSettings: IConsentSettings): void {
        this.store.dispatch({ type: SettingActions.ADD_CONSENT_SETTINGS, consentSettings });
    }

    updateConsentSettings(consentKeyToMerge: Partial<IConsentSettings>): void {
        this.store.dispatch({ type: SettingActions.UPDATE_CONSENT_SETTINGS, consentKeyToMerge: { ...consentKeyToMerge } });
    }

    fetchConsentSettings(): Promise<boolean> {
        if (!getConsentSettingsFetchStatus(this.store.getState())) {
            this.store.dispatch({ type: SettingActions.FETCHING_CONSENT_SETTINGS });
            const fetchPromise: IPromise<boolean> = this.ConsentSettings.getConsent().then((res: IConsentSettings): boolean => {
                if (res) {
                    this.addConsentSettings(res);
                }
                this.store.dispatch({ type: SettingActions.FETCHED_CONSENT_SETTINGS });
                return Boolean(res);
            });
            return Promise.resolve(fetchPromise);
        }
        return Promise.resolve(true);
    }

    saveConsentSettings(): ng.IPromise<boolean> {
        const consentSettings = getConsentSettings(this.store.getState());
        return this.ConsentSettings.setConsent(consentSettings).then((res: boolean): boolean => {
            if (res) {
                this.store.dispatch({ type: SettingActions.SAVED_CONSENT_SETTINGS });
            }
            return res;
        });
    }

    recreateTokens(): ng.IPromise<boolean> {
        return this.ConsentSettings.recreateMagicLinks();
    }

    deleteTokens(): ng.IPromise<boolean> {
        return this.ConsentSettings.deleteMagicLinks();
    }
}
