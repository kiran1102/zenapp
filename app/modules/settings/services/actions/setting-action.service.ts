declare const window: any;
declare const document: any;

import { Settings } from "sharedServices/settings.service";
import { Permissions } from "modules/shared/services/helper/permissions.service";

import {
    getRiskSettings,
    getRiskSettingsFetchStatus,
    getProjectSettings,
    getProjectSettingsFetchStatus,
    getHelpSettingsFetchStatus,
} from "oneRedux/reducers/setting.reducer";
import { SettingActions } from "oneRedux/reducers/setting.reducer";

import {
    IProjectSettings,
    IRiskSettings,
    IHelpSettings,
} from "interfaces/setting.interface";
import { IStore } from "interfaces/redux.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";

export class SettingActionService {

    static $inject: string[] = ["store", "$q", "Permissions", "Settings", "HeatmapSettings"];

    constructor(
        private store: IStore,
        private $q: ng.IQService,
        private permissions: Permissions,
        private settings: Settings,
        private HeatmapSettings: any,
    ) {}

    addProjectSettings(projectSettings: IProjectSettings): void {
        this.store.dispatch({ type: SettingActions.ADD_PROJECT_SETTINGS, projectSettings });
    }

    fetchProjectSettings(): ng.IPromise<boolean> {
        if (!getProjectSettings(this.store.getState()) && !getProjectSettingsFetchStatus(this.store.getState())) {
            this.store.dispatch({ type: SettingActions.FETCHING_PROJECT_SETTINGS });

            return this.settings.getProject().then((res: IProtocolResponse<IProjectSettings>): boolean => {
                this.addProjectSettings(res.data);
                this.store.dispatch({ type: SettingActions.FETCHED_PROJECT_SETTINGS });
                return true;
            });
        }
        return this.$q.when(true);
    }

    addRiskSettings(riskSettings: IRiskSettings): void {
        this.store.dispatch({ type: SettingActions.ADD_RISK_SETTINGS, riskSettings });
    }

    fetchRiskSettings(): ng.IPromise<boolean> {
        if (!getRiskSettings(this.store.getState()) && !getRiskSettingsFetchStatus(this.store.getState())) {
            this.store.dispatch({ type: SettingActions.FETCHING_RISK_SETTINGS });

            return this.HeatmapSettings.getHeatmap().then((res: IProtocolResponse<IRiskSettings>): boolean => {
                this.addRiskSettings(res.data);
                this.store.dispatch({ type: SettingActions.FETCHED_RISK_SETTINGS });
                return true;
            });
        }
        return this.$q.when(true);
    }

    addHelpSettings(helpSettings: IHelpSettings): void {
        this.store.dispatch({ type: SettingActions.ADD_HELP_SETTINGS, helpSettings });
    }

    fetchHelpSettings(): ng.IPromise<boolean> {
        if (this.permissions.canShow("SettingsHelpGet") && !getHelpSettingsFetchStatus(this.store.getState())) {
            this.store.dispatch({ type: SettingActions.FETCHING_HELP_SETTINGS });
            return this.settings.getHelp().then((res: IProtocolResponse<IHelpSettings>): boolean => {
                this.addHelpSettings(res.data);
                this.store.dispatch({ type: SettingActions.FETCHED_HELP_SETTINGS });
                return true;
            });
        }
        return this.$q.when(true);
    }

    saveHelpSettings(helpSettings: IHelpSettings): ng.IPromise<boolean> {
        return this.settings.saveHelp(helpSettings).then((response: IProtocolResponse<IHelpSettings>): boolean => {
            this.addHelpSettings(helpSettings);
            return response.result;
        });
    }

    resetSettings(): void {
        this.store.dispatch({ type: SettingActions.RESET_SETTINGS });
    }

    setIfBrowserIsIE11(): void {
        const isIE11 = !!document.documentMode && !!window.MSInputMethodContext;
        this.store.dispatch({ type: SettingActions.SET_IS_IE_11, isIE11 });
    }
}
