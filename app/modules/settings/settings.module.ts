// Modules
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from "@angular/core";
import { SharedModule } from "sharedModules/shared.module";
import { UpgradedLegacyModule } from "sharedModules/upgrade/upgraded-legacy.module";
import { HttpClientModule } from "@angular/common/http";
import { FormsModule } from "@angular/forms";
import { ReactiveFormsModule } from "@angular/forms";
import { VitreusModule } from "@onetrust/vitreus";
import { DndListModule } from "ngx-drag-and-drop-lists";
import { OTPipesModule, PIPES } from "modules/pipes/pipes.module";
import { VirtualScrollModule } from "od-virtualscroll";
import { OneVideoModule } from "modules/video/video.module";
import { ConsentModule } from "consentModule/consent.module";
import { SettingsRisksModule } from "./settings-risks/settings-risks.module";
import { SettingsSspModule } from "settingsComponents/settings-ssp/settings-ssp.module";

import { CommonModule } from "@angular/common";
import { TranslateModule } from "@ngx-translate/core";

// Components
import { LocalizationEditorComponent } from "settingsComponents/localization-editor/localization-editor.component";
import { LocalizationEditorTableComponent } from "settingsComponents/localization-editor/localization-editor-table.component";
import { SettingsOrgGroupMapping } from "settingsComponents/settings-sso/settings-org-group-mapping.component";
import { SettingsOrgGroupMappingModal } from "settingsComponents/settings-sso/settings-org-group-mapping-modal.component";
import { SettingSsoConfigTable } from "settingsComponents/settings-sso/settings-sso-config-table.component";
import { SettingsConsentComponent } from "settingsComponents/settings-consent/settings-consent.component";
import { SettingsInventoryComponent } from "settingsComponents/settings-inventory/settings-inventory.component";
import { EmailLanguageActivatorComponent } from "settingsComponents/email-language-activator/email-language-activator.component";
import { EmailTemplateListComponent } from "settingsComponents/email-template-list/email-template-list.component";
import { UpgradedSettingsEmailTemplateDirective } from "settingsComponents/settings-email-template/upgraded-settings-email-template.directive";
import { EmailTemplateEditComponent } from "settingsComponents/email-template-edit/email-template-edit.component";
import { EmailTemplatePreviewModal } from "settingsComponents/email-template-preview-modal/email-template-preview-modal.component";
import { EmailTemplatePreviewModalV2 } from "settingsComponents/email-template-preview-modal-v2/email-template-preview-modal.component";
import { SettingsHelpComponent } from "settingsComponents/settings-help/settings-help.component";
import { SettingsSmtpComponent } from "settingsComponents/settings-smtp/settings-smtp.component";
import { TimestampSettingsComponent } from "settingsComponents/settings-timestamp/timestamp-settings.component";
import { SettingsManageSubscriptionsComponent } from "settingsComponents/settings-manage-subscriptions/settings-manage-subscriptions.component";
import { SettingsLearningAndFeedbackToolComponent } from "settingsComponents/settings-learning-and-feedback-tool/settings-learning-and-feedback-tool.component";
import { SettingSsoComponent } from "settingsComponents/settings-sso/settings-sso.component";
import { SettingSsoCertificateUploadModal } from "settingsComponents/settings-sso/settings-sso-certificate-upload-modal.component";
import { AATemplateSettingsComponent } from "settingsComponents/aa-settings/aa-template-settings/aa-template-settings.component";
import { LocalizationEditModalV2 } from "settingsComponents/localization-edit-modal-v2/localization-edit-modal-v2.component";
import { SettingScimcomponent } from "settingsComponents/setting-scim/setting-scim.component";
import { SettingScimGenerateCredsModalComponent } from "settingsComponents/setting-scim/setting-scim-generate-creds-modal.component";
import { AAAssessmentSettingsComponent } from "settingsComponents/aa-settings/aa-assessment-settings/aa-assessment-settings.component";
import { SettingsVendorManagementComponent } from "settingsComponents/settings-vendor-management/settings-vendor-management.component";

// Services
import { SettingConsentActionService } from "settingsModule/services/actions/setting-consent-action.service";
import { LocalizationActionService } from "settingsModule/services/actions/localization-action.service";
import { SettingSmtpActionService } from "settingsModule/services/actions/setting-smtp-action.service";
import { SettingSmtpApiService } from "settingsModule/services/apis/setting-smtp-api.service";
import { SmtpConnectionsTemplateListService } from "settingsComponents/smtp-connections-list/smtp-connections-template-list.service";
import { SettingsLearningAndFeedbackService } from "settingsServices/apis/settings-learning-and-feedback-api.service";
import { LocalizationApiService } from "settingsServices/apis/localization-api.service";
import { SettingsAAActionService } from "settingsServices/actions/settings-assessment-action.service";
import { SettingsAAApiService } from "settingsServices/apis/settings-assessment-api.service";
import { MessageApiService } from "settingsServices/apis/message-api.service";
import { SettingSmtpAdapterService } from "settingsServices/adapters/setting-smtp-adapter.service";
import { SettingScimApiService } from "settingsServices/apis/setting-scim-api.service";
import { SettingsVendorManagementService } from "settingsServices/apis/settings-vendor-management-api.service";

export function getLocalizationActionService(injector) {
    return injector.get("LocalizationActions");
}

export function getSettingSmtpActionService(injector) {
    return injector.get("SettingSmtpAction");
}

const PROVIDERS = [
    SettingConsentActionService,
    SettingsLearningAndFeedbackService,
    LocalizationApiService,
    SettingsAAActionService,
    SettingsAAApiService,
    MessageApiService,
    SettingSmtpAdapterService,
    SmtpConnectionsTemplateListService,
    SettingSmtpApiService,
    SettingScimApiService,
    SettingsVendorManagementService,
    {
        provide: LocalizationActionService,
        deps: ["$injector"],
        useFactory: getLocalizationActionService,
    },
    {
        provide: SettingSmtpActionService,
        deps: ["$injector"],
        useFactory: getSettingSmtpActionService,
    },
];

const COMPONENTS = [
    UpgradedSettingsEmailTemplateDirective,
];

const COMPONENTS_TO_BE_DOWNGRADED = [
    LocalizationEditorComponent,
    LocalizationEditorTableComponent,
    SettingsOrgGroupMapping,
    SettingsOrgGroupMappingModal,
    SettingSsoConfigTable,
    SettingsConsentComponent,
    SettingsInventoryComponent,
    EmailLanguageActivatorComponent,
    EmailTemplateListComponent,
    EmailTemplateEditComponent,
    EmailTemplatePreviewModal,
    EmailTemplatePreviewModalV2,
    SettingsHelpComponent,
    SettingsSmtpComponent,
    TimestampSettingsComponent,
    SettingsManageSubscriptionsComponent,
    SettingsLearningAndFeedbackToolComponent,
    SettingSsoComponent,
    SettingSsoCertificateUploadModal,
    AATemplateSettingsComponent,
    LocalizationEditModalV2,
    SettingScimcomponent,
    SettingScimGenerateCredsModalComponent,
    AAAssessmentSettingsComponent,
    SettingsVendorManagementComponent,
];

const COMPONENTS_TO_BE_UPGRADED = [
    UpgradedSettingsEmailTemplateDirective,
];

@NgModule({
    imports: [
        SharedModule,
        UpgradedLegacyModule,
        CommonModule,
        HttpClientModule,
        FormsModule,
        ReactiveFormsModule,
        VitreusModule,
        DndListModule,
        OTPipesModule,
        VirtualScrollModule,
        OneVideoModule,
        ConsentModule,
        SettingsRisksModule,
        SettingsSspModule,
        TranslateModule,
    ],
    providers: [
      ...PROVIDERS,
      ...PIPES, // TODO: Remove from providers when ot-common is implemented. Only necessary if you are using pipe in TS file, else just module is fine
    ],
    declarations: [
      ...COMPONENTS,
      ...COMPONENTS_TO_BE_DOWNGRADED,
    ],
    entryComponents: [
      ...COMPONENTS_TO_BE_DOWNGRADED,
    ],
    exports: [
        ...COMPONENTS_TO_BE_UPGRADED,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class SettingsModule {}
