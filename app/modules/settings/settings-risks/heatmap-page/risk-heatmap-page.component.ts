// Core
import {
    Component,
    OnInit,
    OnDestroy,
} from "@angular/core";
import { FormGroup } from "@angular/forms";

// Rxjs
import { takeUntil } from "rxjs/operators";
import { Subject } from "rxjs";

// Interfaces
import {
    IRiskSetting,
    IRiskLevel,
} from "interfaces/risks-settings.interface";

// Services
import { RiskAPIService } from "oneServices/api/risk-api.service";
import { ModalService } from "sharedServices/modal.service";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

@Component({
    selector: "risk-heatmap-page",
    templateUrl: "./risk-heatmap-page.component.html",
})

export class RiskHeatmapPageComponent implements OnInit, OnDestroy {

    isLoading = true;
    riskSettings: IRiskSetting;
    heatmapForm: FormGroup;
    saveInProgress = false;
    resetInProgress = false;
    riskLevels: IRiskLevel[];

    private destroy$ = new Subject();

    constructor(
        private readonly riskApiService: RiskAPIService,
        private readonly modalService: ModalService,
        private translatePipe: TranslatePipe,
    ) {
    }

    ngOnInit() {
        this.riskApiService.getRiskLevel()
            .pipe(
                takeUntil(this.destroy$),
            )
            .subscribe((riskLevels: IRiskLevel[]) => {
                if (riskLevels) {
                    this.riskLevels = riskLevels;
                }
            });

        this.riskApiService
            .getRiskSetting(true)
            .pipe(
                takeUntil(this.destroy$),
            )
            .subscribe((settings: IRiskSetting): void => {
                if (settings) {
                    this.riskSettings = settings;
                }
                this.isLoading = false;
            });

        this.heatmapForm = new FormGroup({});
    }

    ngOnDestroy() {
        this.destroy$.next();
        this.destroy$.complete();
    }

    toggleHeatmapSetting() {
        this.riskApiService
            .updateRiskSetting({
                enableRiskHeatMap: !this.riskSettings.enableRiskHeatMap,
            })
            .pipe(
                takeUntil(this.destroy$),
            )
            .subscribe((settings: IRiskSetting) => {
                if (settings) {
                    this.riskSettings = settings;
                }
            });
    }

    saveRiskSettings() {
        this.saveInProgress = true;
        this.riskApiService
            .saveHeatmapSettings(this.heatmapForm.value)
            .pipe(
                takeUntil(this.destroy$),
            )
            .subscribe(() => this.saveInProgress = false);
    }

    resetRiskSettings() {
        this.modalService.setModalData({
            modalTitle: this.translatePipe.transform("ResetHeatMapSetting"),
            confirmationText: this.translatePipe.transform("ResetHeatMapSettingWarning"),
            cancelButtonText: this.translatePipe.transform("Cancel"),
            submitButtonText: this.translatePipe.transform("Reset"),
            promiseToResolve: (): ng.IPromise<boolean> => {
                this.resetInProgress = true;
                return this.riskApiService
                    .resetHeatmapSettings()
                    .then(() => {
                        this.resetInProgress = false;
                        return true;
                    });
            },
        });
        this.modalService.openModal("deleteConfirmationModal");
    }
}
