import { NgModule } from "@angular/core";

// Modules
import { CommonModule } from "@angular/common";
import { SharedModule } from "sharedModules/shared.module";
import { OTPipesModule } from "modules/pipes/pipes.module";
import { VitreusModule } from "@onetrust/vitreus";
import { RiskHeatmapModule } from "../risk-heatmap/risk-heatmap.module";

// Components
import { RiskHeatmapPageComponent } from "./risk-heatmap-page.component";

@NgModule({
    imports: [
        CommonModule,
        SharedModule,
        OTPipesModule,
        VitreusModule,
        RiskHeatmapModule,
    ],
    declarations: [
        RiskHeatmapPageComponent,
    ],
    entryComponents: [
        RiskHeatmapPageComponent,
    ],
})
export class HeatmapPageModule { }
