// Angular
import {
    Component,
    Input,
} from "@angular/core";

// Interfaces
import {
    IRiskLevel,
} from "interfaces/risks-settings.interface";

@Component({
    selector: "risk-level-list",
    templateUrl: "./risk-level-list.component.html",
})

export class RiskLevelListComponent {

    @Input() riskLevelList: IRiskLevel[];

    trackByRiskLevelId(riskLevel: IRiskLevel ): number {
        return riskLevel.id;
    }
}
