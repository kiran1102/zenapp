export enum IndividualRiskMethodologies {
    STANDARD = "Standard",
    MATRIX = "Matrix",
}
