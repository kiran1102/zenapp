import { NgModule } from "@angular/core";

// Modules
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";
import { SharedModule } from "sharedModules/shared.module";
import { OTPipesModule } from "modules/pipes/pipes.module";
import { RiskHeatmapModule } from "../risk-heatmap/risk-heatmap.module";
import { VitreusModule } from "@onetrust/vitreus";

// Components
import { IndividualRiskSettingsTabComponent } from "./individual-risk-settings-tab.component";
import { RiskLevelListComponent } from "./risk-level-list/risk-level-list.component";

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        SharedModule,
        OTPipesModule,
        RiskHeatmapModule,
        VitreusModule,
    ],
    declarations: [
        IndividualRiskSettingsTabComponent,
        RiskLevelListComponent,
    ],
    exports: [
        IndividualRiskSettingsTabComponent,
    ],
})
export class IndividualRiskSettingsTabModule { }
