// Angular
import {
    Component,
    OnInit,
    OnDestroy,
} from "@angular/core";
import { FormGroup } from "@angular/forms";

// External
import { Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";

// Services
import { RiskAPIService } from "oneServices/api/risk-api.service";
import { ModalService } from "sharedServices/modal.service";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

// Interfaces
import { IRiskLevel } from "interfaces/risks-settings.interface";

import {
    IIndividualRiskSetting,
    IRiskScoringMethod,
} from "modules/settings/settings-risks/shared/settings-risks.interface";

// Constants/Enums
import {
    RiskLevelLabels,
    RiskLevelColorClasses,
} from "modules/settings/settings-risks/shared/settings-risks.constants";

import { IndividualRiskMethodologies } from "./individual-risk-settings-tab.constants";

@Component({
    selector: "individual-risk-settings-tab",
    templateUrl: "./individual-risk-settings-tab.component.html",
})

export class IndividualRiskSettingsTabComponent implements OnInit, OnDestroy {

    isLoading: boolean;
    heatmapForm: FormGroup;
    individualRiskSettings: IIndividualRiskSetting;
    saveInProgress = false;
    resetInProgress = false;

    private destroy$ = new Subject();

    constructor(
        private riskApiService: RiskAPIService,
        private modalService: ModalService,
        private translatePipe: TranslatePipe,
    ) { }

    ngOnInit() {
        this.heatmapForm = new FormGroup({});
        this.getIndividualRiskSetting();
    }

    ngOnDestroy(): void {
        this.destroy$.next(true);
        this.destroy$.complete();
    }

    selectScoringMethodology(event: IRiskScoringMethod) {
        this.individualRiskSettings.currentScoringMethod = this.individualRiskSettings.scoringMethods.find(
            (currentScoringMethod: IRiskScoringMethod): boolean => {
            return currentScoringMethod.id === event.id;
        });
        this.getIndividualRiskScoreByValuationMethod(this.individualRiskSettings.currentScoringMethod.methodName, true);
    }

    getIndividualRiskSetting() {
        this.isLoading = true;
        this.riskApiService
            .getIndividualRiskSetting(true)
            .pipe(
                takeUntil(this.destroy$),
            )
            .subscribe((settings: IIndividualRiskSetting): void => {
                if (settings) {
                    this.individualRiskSettings = settings;
                    this.getIndividualRiskScoreByValuationMethod(this.individualRiskSettings.currentScoringMethod.methodName, true);
                }
                this.isLoading = false;
            });
    }

    getIndividualRiskScoreByValuationMethod(methodName: string, forceLoad: boolean) {
        this.isLoading = true;
        this.riskApiService
            .getIndividualRiskScoreByValuationMethod(methodName, forceLoad)
            .pipe(
                takeUntil(this.destroy$),
            )
            .subscribe((individualRiskScores: IRiskLevel[]): void => {
                if (individualRiskScores) {
                    this.individualRiskSettings.riskLevelList = individualRiskScores;
                    this.setRiskFlagBgColors(this.individualRiskSettings.riskLevelList);
                }
                this.isLoading = false;
            });
    }

    saveRiskSettings(currentMethodName: string) {
        switch (currentMethodName) {
            case IndividualRiskMethodologies.STANDARD:
                this.saveInProgress = true;
                this.riskApiService
                    .updateIndividualRiskSetting(this.individualRiskSettings)
                    .pipe(takeUntil(this.destroy$))
                    .subscribe(() => this.saveInProgress = false);
                break;
            case IndividualRiskMethodologies.MATRIX:
                this.saveInProgress = true;
                this.riskApiService
                    .updateIndividualRiskSetting(this.individualRiskSettings)
                    .pipe(takeUntil(this.destroy$))
                    .subscribe();
                this.riskApiService
                    .saveHeatmapSettings(this.heatmapForm.value)
                    .pipe(takeUntil(this.destroy$))
                    .subscribe(() => this.saveInProgress = false);
                break;
        }
    }

    setRiskFlagBgColors(riskLevelList: IRiskLevel[]): void {
        riskLevelList.forEach((riskLevel: IRiskLevel) => {
                switch (riskLevel.name) {
                    case RiskLevelLabels.LOW:
                        riskLevel.riskFlagBgClass = RiskLevelColorClasses.LOW;
                        break;
                    case RiskLevelLabels.MEDIUM:
                        riskLevel.riskFlagBgClass = RiskLevelColorClasses.MEDIUM;
                        break;
                    case RiskLevelLabels.HIGH:
                        riskLevel.riskFlagBgClass = RiskLevelColorClasses.HIGH;
                        break;
                    case RiskLevelLabels.VERY_HIGH:
                        riskLevel.riskFlagBgClass = RiskLevelColorClasses.VERY_HIGH;
                        break;
                }
            });
    }

    resetRiskSettings() {
        this.modalService.setModalData({
            modalTitle: this.translatePipe.transform("ResetHeatMapSetting"),
            confirmationText: this.translatePipe.transform("ResetHeatMapSettingWarning"),
            cancelButtonText: this.translatePipe.transform("Cancel"),
            submitButtonText: this.translatePipe.transform("Reset"),
            promiseToResolve: (): ng.IPromise<boolean> => {
                this.resetInProgress = true;
                return this.riskApiService
                    .resetHeatmapSettings()
                    .then(() => {
                        this.resetInProgress = false;
                        return true;
                    });
            },
        });
        this.modalService.openModal("deleteConfirmationModal");
    }
}
