// Core
import {
    Component,
    Output,
    EventEmitter,
    Input,
    OnInit,
    OnChanges,
    SimpleChanges,
} from "@angular/core";
import {
    FormBuilder,
    FormGroup,
    Validators,
    FormArray,
} from "@angular/forms";

// Interfaces
import {
    ISettingsRiskProbability,
    ISettingsRiskImpact,
} from "modules/settings/settings-risks/shared/settings-risks.interface";

@Component({
    selector: "risk-heatmap-options",
    templateUrl: "./risk-heatmap-options.component.html",
})
export class RiskHeatmapOptionsComponent implements OnInit, OnChanges {

    @Input() probabilityLevels: ISettingsRiskProbability[];
    @Input() impactLevels: ISettingsRiskImpact[];
    @Input() heatmapForm: FormGroup;
    @Output() onMatrixChange = new EventEmitter();

    constructor(
        private readonly formBuilder: FormBuilder,
    ) { }

    ngOnInit() {
        this.heatmapForm.addControl("impactLevels", this.getImpactLevelFormArray());
        this.heatmapForm.addControl("probabilityLevels", this.getProbabilityLevelFormArray());
    }

    ngOnChanges(changes: SimpleChanges) {
        if (changes.impactLevels && !changes.impactLevels.firstChange) {
            this.heatmapForm.setControl("impactLevels", this.getImpactLevelFormArray());
        }
        if (changes.probabilityLevels && !changes.probabilityLevels.firstChange) {
            this.heatmapForm.setControl("probabilityLevels", this.getProbabilityLevelFormArray());
        }
    }

    onMatrixOptionChange(optionValue: string, index: number, isImpactMatrix: boolean): void {
        if (isImpactMatrix) {
            this.impactLevels[index].name = optionValue;
            this.onMatrixChange.emit({ optionValue, index, isImpactMatrix });
        } else {
            this.probabilityLevels[index].name = optionValue;
            this.onMatrixChange.emit({ optionValue, index, isImpactMatrix });
        }
    }

    private getProbabilityLevelFormArray(): FormArray {
        return this.formBuilder.array(
            this.probabilityLevels
                .map((level: ISettingsRiskProbability) =>
                    this.formBuilder.group({
                        name: [level.name, Validators.required],
                        position: [level.position],
                        value: [level.position],
                        id: [level.id],
                    }),
            ),
        );
    }

    private getImpactLevelFormArray(): FormArray {
        return this.formBuilder.array(
            this.impactLevels
                .map((level: ISettingsRiskImpact) =>
                    this.formBuilder.group({
                        name: [level.name, [Validators.required, Validators.maxLength(100)]],
                        position: [level.position],
                        value: [level.position],
                        id: [level.id],
                    }),
            ),
        );
    }
}
