import { NgModule } from "@angular/core";

// Modules
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";
import { ReactiveFormsModule } from "@angular/forms";
import { OTPipesModule } from "modules/pipes/pipes.module";
import { SharedModule } from "sharedModules/shared.module";
import { VitreusModule } from "@onetrust/vitreus";

// Components
import { RiskHeatmapComponent } from "./risk-heatmap.component";
import { RiskHeatmapOptionsComponent } from "./risk-heatmap-options/risk-heatmap-options.component";

@NgModule({
    imports: [
        SharedModule,
        OTPipesModule,
        VitreusModule,
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
    ],
    declarations: [
        RiskHeatmapComponent,
        RiskHeatmapOptionsComponent,
    ],
    exports: [
        RiskHeatmapComponent,
    ],
})
export class RiskHeatmapModule { }
