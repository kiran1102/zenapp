// Rxjs
import { Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";

// Angular
import {
    Component,
    OnInit,
    Input,
    OnDestroy,
} from "@angular/core";
import {
    FormGroup,
} from "@angular/forms";

// Services
import { RiskHelperService } from "sharedModules/services/helper/risk-helper.service";
import { RiskAPIService } from "oneServices/api/risk-api.service";

// Interfaces
import {
    ISettingsRiskProbability,
    ISettingsRiskImpact,
    ISettingsRiskHeatmapMatrix,
    IRiskHeatmapSettings,
} from "modules/settings/settings-risks/shared/settings-risks.interface";

@Component({
    selector: "risk-heatmap",
    templateUrl: "./risk-heatmap.component.html",
})

export class RiskHeatmapComponent implements OnInit, OnDestroy {

    @Input() heatmapForm: FormGroup;

    impactLevels: ISettingsRiskImpact[] = [];
    probabilityLevels: ISettingsRiskProbability[] = [];
    heatmapMatrix: ISettingsRiskHeatmapMatrix[][];
    impactLevelsName: string[];
    probabilityLevelsName: string[];

    private destroy$ = new Subject();

    constructor(
        private readonly helper: RiskHelperService,
        private readonly api: RiskAPIService,
    ) { }

    ngOnInit() {
        this.api
            .getHeatmapSettings(true)
            .pipe(
                takeUntil(this.destroy$),
            )
            .subscribe((settings: IRiskHeatmapSettings) => {
                if (settings) {
                    this.impactLevels = settings.impactLevels;
                    this.impactLevelsName = this.impactLevels.map((level: ISettingsRiskImpact) => level.name);
                    this.probabilityLevels = settings.probabilityLevels;
                    this.probabilityLevelsName = this.probabilityLevels.map((level: ISettingsRiskImpact) => level.name);
                    this.initializeMatrix();
                }
            });

        this.heatmapForm.valueChanges
            .pipe(
                takeUntil(this.destroy$),
            )
            .subscribe((value: IRiskHeatmapSettings) => {
                if (value.impactLevels && value.impactLevels.length) {
                    this.impactLevelsName = value.impactLevels.map((level: ISettingsRiskImpact) => level.name);
                }
                if (value.probabilityLevels && value.probabilityLevels.length) {
                    this.probabilityLevelsName = value.probabilityLevels.map((level: ISettingsRiskImpact) => level.name);
                }
            });
    }

    ngOnDestroy() {
        this.destroy$.next();
        this.destroy$.complete();
    }

    onMatrixOptionChange(value: { optionValue: string, index: number, isImpactMatrix: boolean }): void {
        if (value.isImpactMatrix) {
            this.impactLevels[value.index].name = value.optionValue;
        } else {
            this.probabilityLevels[value.index].name = value.optionValue;
        }
    }

    private initializeMatrix() {
        this.heatmapMatrix = [];
        this.impactLevels.forEach((impact: ISettingsRiskImpact) => {
            const impactRow: ISettingsRiskHeatmapMatrix[] = [];
            this.probabilityLevels.forEach((probability: ISettingsRiskProbability) => {
                const score = this.helper.getScore(impact.value, probability.value);
                impactRow.push({
                    impact,
                    probability,
                    score,
                    color: this.helper.getRiskColor(score),
                });
            });
            this.heatmapMatrix.push(impactRow);
        });
    }
}
