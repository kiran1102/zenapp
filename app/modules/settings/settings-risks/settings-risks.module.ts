import { NgModule } from "@angular/core";

// Modules
import { HeatmapPageModule } from "./heatmap-page/heatmap.page.module";
import { RiskSettingsPageModule } from "./risk-settings-page/risk-settings-page.module";

// Components

@NgModule({
    imports: [
        HeatmapPageModule,
        RiskSettingsPageModule,
    ],
})
export class SettingsRisksModule {}
