// 3rd party
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { SharedModule } from "sharedModules/shared.module";
import { OTPipesModule } from "modules/pipes/pipes.module";
import { VitreusModule } from "@onetrust/vitreus";

// Modules
import { RiskSettingsTabModule } from "../risk-settings-tab/risk-settings-tab.module";
import { IndividualRiskSettingsTabModule } from "../individual-risk-settings-tab/individual-risk-settings-tab.module";

// Components
import { RiskSettingsPageComponent } from "./risk-settings-page.component";

@NgModule({
    imports: [
        CommonModule,
        SharedModule,
        OTPipesModule,
        VitreusModule,
        RiskSettingsTabModule,
        IndividualRiskSettingsTabModule,
    ],
    declarations: [
        RiskSettingsPageComponent,
    ],
    entryComponents: [
        RiskSettingsPageComponent,
    ],
})
export class RiskSettingsPageModule { }
