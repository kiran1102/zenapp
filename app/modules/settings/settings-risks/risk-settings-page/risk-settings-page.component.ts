// Core
import {
    Component,
    OnInit,
} from "@angular/core";

// Services
import { Permissions } from "modules/shared/services/helper/permissions.service";

// Interfaces
import { ITabsNav } from "interfaces/one-tabs-nav.interface";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

// Constants/Enums
import {
    RiskTabNameKeys,
    RiskReferenceType,
    AssessmentRiskTranslationKeys,
    VendorRiskTranslationKeys,
    AssetRiskTranslationKeys,
    ProcessingActivityRiskTranslationKeys,
    EntityRiskTranslationKeys,
} from "modules/settings/settings-risks/shared/settings-risks.constants";

@Component({
    selector: "risk-settings-page",
    templateUrl: "risk-settings-page.component.html",
})

export class RiskSettingsPageComponent implements OnInit {

    selectedTab: string;
    maxNumberTabs: number;
    tabOptions: ITabsNav[];
    assessmentRiskTranslationKeys = AssessmentRiskTranslationKeys;
    assetRiskTranslationKeys = AssetRiskTranslationKeys;
    processingActivityRiskTranslationKeys =  ProcessingActivityRiskTranslationKeys;
    vendorRiskTranslationKeys = VendorRiskTranslationKeys;
    entityRiskTranslationKeys = EntityRiskTranslationKeys;

    public readonly riskTabNameKeys = RiskTabNameKeys;
    public readonly riskReferenceType = RiskReferenceType;

    constructor(
        private readonly translatePipe: TranslatePipe,
        private readonly permissions: Permissions,
    ) { }

    ngOnInit() {
        this.tabOptions = [
            { id: this.riskTabNameKeys.INDIVIDUAL, text: this.translatePipe.transform(this.riskTabNameKeys.INDIVIDUAL) },
        ];
        if (this.permissions.canShow("AssessmentRiskScore")) {
            this.tabOptions.push(
                { id: this.riskTabNameKeys.ASSESSMENT, text: this.translatePipe.transform(this.riskTabNameKeys.ASSESSMENT) },
            );
        }
        if (this.permissions.canShow("InventoryRiskScore")) {
            this.tabOptions.push(
                { id: this.riskTabNameKeys.ASSET, text: this.translatePipe.transform(this.riskTabNameKeys.ASSET) },
                { id: this.riskTabNameKeys.PROCESSING_ACTIVITY, text: this.translatePipe.transform(this.riskTabNameKeys.PROCESSING_ACTIVITY) },
                { id: this.riskTabNameKeys.VENDOR, text: this.translatePipe.transform(this.riskTabNameKeys.VENDOR) },
            );
            if (this.permissions.canShow("DataMappingEntitiesInventory")) {
                this.tabOptions.push({ id: this.riskTabNameKeys.ENTITY, text: this.translatePipe.transform(this.riskTabNameKeys.ENTITY) });
            }
        }
        this.maxNumberTabs = this.tabOptions.length;
        this.selectedTab = this.riskTabNameKeys.INDIVIDUAL;
    }

    onRiskTabClick(option: ITabsNav): void {
        this.selectedTab = option.id;
    }
}
