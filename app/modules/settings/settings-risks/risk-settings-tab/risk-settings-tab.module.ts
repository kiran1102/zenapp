import { NgModule } from "@angular/core";

// Modules
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";
import { ReactiveFormsModule } from "@angular/forms";
import { SharedModule } from "sharedModules/shared.module";
import { OTPipesModule } from "modules/pipes/pipes.module";
import { VitreusModule } from "@onetrust/vitreus";
import { RiskLevelRangeSliderModule } from "../risk-level-range-slider/risk-level-range-slider.module";

// Components
import { RiskSettingsTabComponent } from "./risk-settings-tab.component";
import { ScoringMethodAverageComponent } from "./scoring-method-average/scoring-method-average.component";
import { ScoringMethodCountOfRisksComponent } from "./scoring-method-count-of-risks/scoring-method-count-of-risks.component";
import { ScoringMethodSumOfRisksComponent } from "./scoring-method-sum-of-risks/scoring-method-sum-of-risks.component";
import { CountRisksFormulaComponent } from "./scoring-method-count-of-risks/count-risks-formula/count-risks-formula.component";

// Services
import { SettingsRisksHelper } from "modules/settings/settings-risks/shared/settings-risks-helper.service";
import { SettingsRisksApiService } from "../shared/settings-risks-api.service";
import { ScoringMethodCountOfRisksHelper } from "./scoring-method-count-of-risks/scoring-method-count-of-risk-helper.service";
import { ScoringMethodSumOfRisksHelper } from "./scoring-method-sum-of-risks/scoring-method-sum-of-risk-helper.service";

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        SharedModule,
        OTPipesModule,
        VitreusModule,
        RiskLevelRangeSliderModule,
    ],
    declarations: [
        RiskSettingsTabComponent,
        ScoringMethodAverageComponent,
        ScoringMethodCountOfRisksComponent,
        ScoringMethodSumOfRisksComponent,
        CountRisksFormulaComponent,
    ],
    exports: [
        RiskSettingsTabComponent,
    ],
    providers: [
        SettingsRisksHelper,
        SettingsRisksApiService,
        ScoringMethodCountOfRisksHelper,
        ScoringMethodSumOfRisksHelper,
    ],
})
export class RiskSettingsTabModule { }
