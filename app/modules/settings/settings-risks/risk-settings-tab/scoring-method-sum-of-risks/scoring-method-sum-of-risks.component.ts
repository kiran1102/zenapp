// Angular
import {
    Component,
    OnInit,
    Input,
    Output,
    EventEmitter,
    OnDestroy,
} from "@angular/core";
import { FormGroup } from "@angular/forms";

import { cloneDeep } from "lodash";

// Rxjs
import { Subject, Subscription } from "rxjs";
import { takeUntil } from "rxjs/operators";

// Services
import { SettingsRisksApiService } from "../../shared/settings-risks-api.service";
import { NotificationService } from "modules/shared/services/provider/notification.service";
import { ScoringMethodSumOfRisksHelper } from "./scoring-method-sum-of-risk-helper.service";

// Interface
import {
    IAggregateRiskScoringMethodSumOfRisksCondtions,
    IRiskSliderConfigDetails,
    IAggregateRiskScoreRange,
} from "modules/settings/settings-risks/shared/settings-risks.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";
import { IStringMap } from "interfaces/generic.interface";

// Constants/Enums
import {
    RiskReferenceType,
    SumOfRisksPageScaleValueKeys,
} from "modules/settings/settings-risks/shared/settings-risks.constants";
import { Color } from "constants/color.constant";
import { RiskV2Levels } from "enums/riskV2.enum";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

// TODO: import SliderActions from vitreus when available.
enum SliderActions {
    DESTROY_SLIDER = "DESTROY_SLIDER",
    INITIALIZE_SLIDER = "INITIALIZE_SLIDER",
    REINITIALIZE_SLIDER = "REINITIALIZE_SLIDER",
    RESET_SLIDER = "RESET_SLIDER",
    UPDATE_SLIDER_VALUES = "UPDATE_SLIDER_VALUES",
    UPDATE_SLIDER_OPTIONS = "UPDATE_SLIDER_OPTIONS",
}

@Component({
    selector: "scoring-method-sum-of-risks",
    templateUrl: "./scoring-method-sum-of-risks.component.html",
})

export class ScoringMethodSumOfRisksComponent implements OnInit, OnDestroy {

    @Input() riskReferenceType: RiskReferenceType;
    @Input() riskSettingsTranslationKeys: IStringMap<string>;

    @Output() onSettingsSaved = new EventEmitter();
    @Output() isFormValid = new EventEmitter<boolean>();

    sumOfRisksFormulaForm: FormGroup;
    formSubscription: Subscription;
    config: IRiskSliderConfigDetails;
    connectedBarColors = [Color.OffWhite, Color.RiskLow, Color.RiskMedium, Color.RiskHigh, Color.RiskVeryHigh];
    handleColors = [Color.RiskLow, Color.RiskMedium, Color.RiskHigh, Color.RiskVeryHigh];
    isSliderDisabled = true;
    disabledOpacity = 0.8;
    sliderAction: SliderActions = SliderActions.INITIALIZE_SLIDER;
    isLoadingRiskRangeData = true;
    sumOfRisksScaleValues: IAggregateRiskScoringMethodSumOfRisksCondtions = {
        scaleLimit: 100,
        lowRiskLowerBound: 10,
        lowRiskUpperBound: 25,
        mediumRiskUpperBound: 50,
        highRiskUpperBound: 75,
    };
    sumOfRisksResponse: IAggregateRiskScoreRange[] = [];

    scaleValueKeys = SumOfRisksPageScaleValueKeys;

    riskV2Levels = RiskV2Levels;

    private destroy$ = new Subject();

    constructor(
        private translatePipe: TranslatePipe,
        private notificationService: NotificationService,
        private settingsRisksApiService: SettingsRisksApiService,
        private scoringMethodSumOfRisksHelper: ScoringMethodSumOfRisksHelper,
    ) { }

    ngOnInit() {
        this.getSumOfRisksFormula();
    }

    getSumOfRisksFormula() {
        this.isLoadingRiskRangeData = true;
        this.settingsRisksApiService.getRiskAggregateScoringSumOfRiskMethodSetting(this.riskReferenceType)
            .pipe(takeUntil(this.destroy$))
            .subscribe((sumOfRisksResponse: IProtocolResponse<IAggregateRiskScoreRange[]>) => {
                if (sumOfRisksResponse.result) {
                    this.sumOfRisksResponse = sumOfRisksResponse.data;
                    this.sumOfRisksScaleValues = this.scoringMethodSumOfRisksHelper.parseSumOfRisksResponse(sumOfRisksResponse.data);
                    this.sumOfRisksFormulaForm = this.scoringMethodSumOfRisksHelper.createFormSumOfRisks(this.sumOfRisksScaleValues);
                    this.setFormChanges(this.sumOfRisksFormulaForm);
                    this.config = this.scoringMethodSumOfRisksHelper.getSliderConfig(this.sumOfRisksFormulaForm);
                }
                this.isLoadingRiskRangeData = false;
            });
    }

    ngOnDestroy() {
        this.destroy$.next();
        this.destroy$.complete();
    }

    setFormChanges(sumOfRisksFormulaForm: FormGroup): void {
        if (this.formSubscription && !this.formSubscription.closed) {
            this.formSubscription.unsubscribe();
        }
        this.formSubscription = sumOfRisksFormulaForm.valueChanges.subscribe(() => {
            if (sumOfRisksFormulaForm.valid) {
                this.scoringMethodSumOfRisksHelper.updateMaxLimitOfScale(sumOfRisksFormulaForm);
                this.updateScaleConfig(sumOfRisksFormulaForm, SliderActions.UPDATE_SLIDER_OPTIONS);
            }
            sumOfRisksFormulaForm.markAsDirty();
            this.isFormValid.emit(sumOfRisksFormulaForm.valid);
        });
    }

    updateScaleConfig(form: FormGroup, currentSliderAction: SliderActions) {
        this.sliderAction = currentSliderAction;
        this.config.range.max = form.value.scaleLimit;
        this.config.start = [
            form.value.lowRiskLowerBound,
            form.value.lowRiskUpperBound,
            form.value.mediumRiskUpperBound,
            form.value.highRiskUpperBound,
        ];
        this.config = cloneDeep(this.config);
    }

    pageHasChanges(): boolean {
        return this.sumOfRisksFormulaForm.dirty;
    }

    resetSettings() {
        this.isLoadingRiskRangeData = true;
        this.settingsRisksApiService
            .resetRiskAggregateScoringSumOfRisksMethodSetting(this.riskReferenceType, false)
            .pipe(takeUntil(this.destroy$))
            .subscribe((sumOfRisksResponse: IProtocolResponse<IAggregateRiskScoreRange[]>) => {
                if (sumOfRisksResponse.result) {
                    this.sumOfRisksResponse = sumOfRisksResponse.data;
                    this.sumOfRisksScaleValues = this.scoringMethodSumOfRisksHelper.parseSumOfRisksResponse(sumOfRisksResponse.data);
                    this.sumOfRisksFormulaForm = this.scoringMethodSumOfRisksHelper.createFormSumOfRisks(this.sumOfRisksScaleValues);
                    this.setFormChanges(this.sumOfRisksFormulaForm);
                    this.config = this.scoringMethodSumOfRisksHelper.getSliderConfig(this.sumOfRisksFormulaForm);
                    this.notificationService.alertSuccess(
                        this.translatePipe.transform("Success"),
                        this.translatePipe.transform("Risk.SumOfRiskMethodologySettingsResetSuccessfully"));
                }
                this.isLoadingRiskRangeData = false;
            });
    }

    saveSettings(doRescore: boolean) {
        if (this.sumOfRisksFormulaForm.valid) {
            this.sumOfRisksResponse = this.scoringMethodSumOfRisksHelper.updateSumOfRisksRanges(this.sumOfRisksResponse, this.sumOfRisksFormulaForm);
            this.settingsRisksApiService
                .saveRiskAggregateScoringSumOfRiskMethodSetting(this.sumOfRisksResponse, this.riskReferenceType, doRescore)
                .pipe(takeUntil(this.destroy$))
                .subscribe((saveResponse: IProtocolResponse<IAggregateRiskScoreRange[]>) => {
                    if (saveResponse.result) {
                        this.onSettingsSaved.emit();
                        this.sumOfRisksFormulaForm.markAsPristine();
                        this.notificationService.alertSuccess(
                            this.translatePipe.transform("Success"),
                            this.translatePipe.transform("SumOfRiskSettingsSavedSuccessfully"));
                    }
                });
        }
    }
}
