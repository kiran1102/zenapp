// Angular
import { Injectable } from "@angular/core";
import {
    FormBuilder,
    FormGroup,
    Validators,
} from "@angular/forms";

// Interface
import {
    IAggregateRiskScoringMethodSumOfRisksCondtions,
    IRiskSliderConfigDetails,
    IAggregateRiskScoreRange,
} from "modules/settings/settings-risks/shared/settings-risks.interface";

// Constants
import { RiskLevelLabels } from "modules/settings/settings-risks/shared/settings-risks.constants";

const RiskSliderDefaults = {
    STEP: 0.1,
    MARGIN: 0.1,
    PADDING: [0.1, 0],
    MIN: 0.1,
    MAX: 100,
    ANIMATE: true,
};

const veryHighRiskSumOfRisksSliderRangeFactor = 1.15;

const sumOfRisksFormValidator = (form: FormGroup): { [key: string]: boolean } => {
    const lowRiskLowerBound = form.get("lowRiskLowerBound");
    const lowRiskUpperBound = form.get("lowRiskUpperBound");
    const mediumRiskUpperBound = form.get("mediumRiskUpperBound");
    const highRiskUpperBound = form.get("highRiskUpperBound");
    const error = {
        lowRiskLowerBoundInvalid: false,
        lowRiskUpperBoundInvalid: false,
        mediumRiskUpperBoundInvalid: false,
    };
    error.lowRiskLowerBoundInvalid = lowRiskLowerBound.value < 0 || lowRiskLowerBound.value >= lowRiskUpperBound.value;
    error.lowRiskUpperBoundInvalid = lowRiskUpperBound.value >= mediumRiskUpperBound.value;
    error.mediumRiskUpperBoundInvalid = mediumRiskUpperBound.value >= highRiskUpperBound.value;

    return (error.lowRiskLowerBoundInvalid || error.lowRiskUpperBoundInvalid || error.mediumRiskUpperBoundInvalid)
        ? error
        : null;
};

@Injectable()
export class ScoringMethodSumOfRisksHelper {

    constructor(
        private formBuilder: FormBuilder,
    ) { }

    createFormSumOfRisks(sumOfRisksFormula: IAggregateRiskScoringMethodSumOfRisksCondtions): FormGroup {
        return this.formBuilder.group({
            scaleLimit: [sumOfRisksFormula.scaleLimit, Validators.required],
            lowRiskLowerBound: [sumOfRisksFormula.lowRiskLowerBound, Validators.required],
            lowRiskUpperBound: [sumOfRisksFormula.lowRiskUpperBound, Validators.required],
            mediumRiskUpperBound: [sumOfRisksFormula.mediumRiskUpperBound, Validators.required],
            highRiskUpperBound: [sumOfRisksFormula.highRiskUpperBound, Validators.required],
        }, { validator: sumOfRisksFormValidator });
    }

    getSliderConfig(form: FormGroup): IRiskSliderConfigDetails {
        return {
            start: [
                form.value.lowRiskLowerBound,
                form.value.lowRiskUpperBound,
                form.value.mediumRiskUpperBound,
                form.value.highRiskUpperBound,
            ],
            animate: RiskSliderDefaults.ANIMATE,
            connect: [true, true, true, true, true],
            tooltips: [true, true, true, true],
            step: RiskSliderDefaults.STEP,
            margin: RiskSliderDefaults.MARGIN,
            padding: RiskSliderDefaults.PADDING,
            range: {
                min: RiskSliderDefaults.MIN,
                max: form.value.scaleLimit,
            },
            format: {
                from: (value: string) => Math.round(parseFloat(value)),
                to: (value: string) => Math.round(parseFloat(value)),
            },
        };
    }

    parseSumOfRisksResponse(sumOfRisksResponse: IAggregateRiskScoreRange[]): IAggregateRiskScoringMethodSumOfRisksCondtions {
        const sumOfRisksFormula = {
            lowRiskLowerBound: 1,
            lowRiskUpperBound: 2,
            mediumRiskUpperBound: 3,
            highRiskUpperBound: 4,
            scaleLimit: 5,
        };
        sumOfRisksResponse.forEach((formulaEntry: IAggregateRiskScoreRange) => {
            switch (formulaEntry.riskLevelName) {
                case RiskLevelLabels.LOW:
                    sumOfRisksFormula.lowRiskLowerBound = formulaEntry.minScore - 1;
                    sumOfRisksFormula.lowRiskUpperBound = formulaEntry.maxScore;
                    break;
                case RiskLevelLabels.MEDIUM:
                    sumOfRisksFormula.mediumRiskUpperBound = formulaEntry.maxScore;
                    break;
                case RiskLevelLabels.HIGH:
                    sumOfRisksFormula.highRiskUpperBound = formulaEntry.maxScore;
                    break;
            }
        });
        sumOfRisksFormula.scaleLimit = sumOfRisksFormula.highRiskUpperBound * veryHighRiskSumOfRisksSliderRangeFactor;
        return sumOfRisksFormula;
    }

    updateMaxLimitOfScale(sumOfRisksFormulaForm: FormGroup) {
        const oldScaleLimit = sumOfRisksFormulaForm.value.scaleLimit;
        const newValue = sumOfRisksFormulaForm.value.highRiskUpperBound * veryHighRiskSumOfRisksSliderRangeFactor;
        if (newValue !== oldScaleLimit) sumOfRisksFormulaForm.get("scaleLimit").setValue(sumOfRisksFormulaForm.value.highRiskUpperBound * veryHighRiskSumOfRisksSliderRangeFactor);
    }

    updateSumOfRisksRanges(sumOfRisksRanges: IAggregateRiskScoreRange[], form: FormGroup): IAggregateRiskScoreRange[]  {
        return [
            {
                riskLevelId: 1,
                riskLevelName: RiskLevelLabels.LOW,
                riskLevelDisplayName: RiskLevelLabels.LOW,
                minScore: form.value.lowRiskLowerBound + 1,
                maxScore: form.value.lowRiskUpperBound,
            },
            {
                riskLevelId: 2,
                riskLevelName: RiskLevelLabels.MEDIUM,
                riskLevelDisplayName: RiskLevelLabels.MEDIUM,
                minScore: form.value.lowRiskUpperBound + 1,
                maxScore: form.value.mediumRiskUpperBound,
            },
            {
                riskLevelId: 3,
                riskLevelName: RiskLevelLabels.HIGH,
                riskLevelDisplayName: RiskLevelLabels.HIGH,
                minScore: form.value.mediumRiskUpperBound + 1,
                maxScore: form.value.highRiskUpperBound,
            },
            {
                riskLevelId: 4,
                riskLevelName: RiskLevelLabels.VERY_HIGH,
                riskLevelDisplayName: RiskLevelLabels.VERY_HIGH,
                minScore: form.value.highRiskUpperBound + 1,
                maxScore: sumOfRisksRanges.find((entry: IAggregateRiskScoreRange) => entry.riskLevelName === RiskLevelLabels.VERY_HIGH).maxScore,
            },
        ];
    }
}
