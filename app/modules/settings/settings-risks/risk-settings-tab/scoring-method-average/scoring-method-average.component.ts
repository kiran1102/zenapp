// Angular
import {
    Component,
    OnInit,
    Input,
    Output,
    EventEmitter,
    OnDestroy,
} from "@angular/core";

// Rxjs
import { Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";

// Services
import { SettingsRisksApiService } from "../../shared/settings-risks-api.service";
import { NotificationService } from "modules/shared/services/provider/notification.service";

// Interface
import { IAggregateRiskScoreRange } from "modules/settings/settings-risks/shared/settings-risks.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";
import { IStringMap } from "interfaces/generic.interface";

// Constants/Enums
import { RiskReferenceType } from "modules/settings/settings-risks/shared/settings-risks.constants";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

@Component({
    selector: "scoring-method-average",
    templateUrl: "./scoring-method-average.component.html",
})

export class ScoringMethodAverageComponent implements OnInit, OnDestroy {

    @Input() riskReferenceType: RiskReferenceType;
    @Input() riskSettingsTranslationKeys: IStringMap<string>;

    @Output() onSettingsSaved = new EventEmitter();

    isLoadingRiskRangeData = true;
    assessmentRiskScoreRanges: IAggregateRiskScoreRange[];
    initialRiskScoreRanges: IAggregateRiskScoreRange[];

    private destroy$ = new Subject();

    constructor(
        private translatePipe: TranslatePipe,
        private notificationService: NotificationService,
        private settingsRisksApiService: SettingsRisksApiService,
    ) { }

    ngOnInit() {
        this.isLoadingRiskRangeData = true;
        this.settingsRisksApiService.getRiskAggregateScoringAverageMethodSetting(this.riskReferenceType)
        .pipe(takeUntil(this.destroy$))
        .subscribe(
            (response: IProtocolResponse<IAggregateRiskScoreRange[]>) => {
                if (response.result) {
                    this.assessmentRiskScoreRanges = response.data;
                    this.initialRiskScoreRanges = this.assessmentRiskScoreRanges.map((x: IAggregateRiskScoreRange) => ({...x}));
                }
                this.isLoadingRiskRangeData = false;
            });
    }

    ngOnDestroy() {
        this.destroy$.next();
        this.destroy$.complete();
    }

    onRiskLevelChange(levels: IAggregateRiskScoreRange[]) {
        this.assessmentRiskScoreRanges = levels as IAggregateRiskScoreRange[];
    }

    pageHasChanges(): boolean {
        return JSON.stringify(this.assessmentRiskScoreRanges) !== JSON.stringify(this.initialRiskScoreRanges);
    }

    saveSettings(doRescore: boolean) {
        this.isLoadingRiskRangeData = true;
        this.settingsRisksApiService
            .saveRiskAggregateScoringAverageMethodSetting(this.riskReferenceType, this.assessmentRiskScoreRanges, doRescore)
            .pipe(takeUntil(this.destroy$))
            .subscribe((saveResponse: IProtocolResponse<IAggregateRiskScoreRange[]>) => {
                if (saveResponse.result) {
                    this.assessmentRiskScoreRanges = saveResponse.data;
                    this.onSettingsSaved.emit();
                    this.notificationService.alertSuccess(
                        this.translatePipe.transform("Success"),
                        this.translatePipe.transform("Risk.AverageMethodologySettingsSavedSuccessfully"));
                }
                this.isLoadingRiskRangeData = false;
            });
    }
}
