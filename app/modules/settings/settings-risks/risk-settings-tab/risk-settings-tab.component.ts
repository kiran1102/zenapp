// Angular
import {
    Component,
    OnInit,
    OnDestroy,
    TemplateRef,
    ViewChild,
    Input,
    OnChanges,
} from "@angular/core";
import {
    TransitionService,
    Transition,
} from "@uirouter/core";

// Vitreus
import {
    OtModalService,
    ConfirmModalType,
    ModalSize,
} from "@onetrust/vitreus";

// External
import { Subject } from "rxjs";
import {
    takeUntil,
    first,
} from "rxjs/operators";

// Interfaces
import { IRisksScoring } from "modules/settings/settings-risks/shared/settings-risks.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";
import { IStringMap } from "interfaces/generic.interface";

// Constants/Enums
import { AggregateRiskMethodologies } from "modules/settings/settings-risks/shared/settings-risks.constants";
import {
    RiskReferenceType,
    RiskMethodType,
} from "modules/settings/settings-risks/shared/settings-risks.constants";

// Services
import { SettingsRisksApiService } from "../shared/settings-risks-api.service";
import { NotificationService } from "modules/shared/services/provider/notification.service";
import { SettingsRisksHelper } from "modules/settings/settings-risks/shared/settings-risks-helper.service";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

// Components
import { ScoringMethodCountOfRisksComponent } from "./scoring-method-count-of-risks/scoring-method-count-of-risks.component";
import { ScoringMethodAverageComponent } from "./scoring-method-average/scoring-method-average.component";
import { ScoringMethodSumOfRisksComponent } from "./scoring-method-sum-of-risks/scoring-method-sum-of-risks.component";

@Component({
    selector: "risk-settings-tab",
    templateUrl: "./risk-settings-tab.component.html",
})

export class RiskSettingsTabComponent implements OnInit, OnChanges, OnDestroy {

    @Input() riskReferenceType: RiskReferenceType;
    @Input() riskSettingsTranslationKeys: IStringMap<string>;
    @Input() isInventory: boolean;

    public readonly aggregateRiskMethodologies = AggregateRiskMethodologies;

    @ViewChild("AssessmentRiskChangeConfirmationModal") changeModalContentRef: TemplateRef<any>;
    @ViewChild("AssessmentRiskResetConfirmationModal") resetModalContentRef: TemplateRef<any>;
    @ViewChild("navigationConfirmModal") navigationModalContentRef: TemplateRef<unknown>;

    isLoading: boolean;
    savingRiskSettings: boolean;
    disableSave = false;
    currentAssessmentRiskScoringMethod: IRisksScoring;
    initialAssessmentRiskScoringMethod: IRisksScoring;
    pendingAssessmentRiskScoringMethodTransition: IRisksScoring;
    assessmentRiskScoringMethods: IRisksScoring[];
    changeConfirmationModalIconType = ConfirmModalType.WARNING;
    resetConfirmationModalIconType = ConfirmModalType.DELETE;
    resetConfirmationMessage: string;

    @ViewChild(ScoringMethodCountOfRisksComponent) private scoringMethodCountOfRisksComponent: ScoringMethodCountOfRisksComponent;
    @ViewChild(ScoringMethodAverageComponent) private scoringMethodAverageComponent: ScoringMethodAverageComponent;
    @ViewChild(ScoringMethodSumOfRisksComponent) private scoringMethodSumOfRisksComponent: ScoringMethodSumOfRisksComponent;
    private resetModalCloseEvent = new Subject();
    private changeModalCloseEvent = new Subject();
    private navigationModalClose$: Subject<boolean>;
    private destroy$ = new Subject();

    constructor(
        private settingsRisksApiService: SettingsRisksApiService,
        private otModalService: OtModalService,
        private $transitions: TransitionService,
        private notificationService: NotificationService,
        private translatePipe: TranslatePipe,
        private settingsRisksHelper: SettingsRisksHelper,
    ) { }

    ngOnInit() {
        this.assessmentRiskScoringMethods = this.settingsRisksHelper.getAggregateRiskScoringMethods();
        this.getCurrentRiskScoringMethod();
        this.setTransitionHook();
    }

    ngOnChanges() {
        this.currentAssessmentRiskScoringMethod = null;
    }

    ngOnDestroy(): void {
        this.destroy$.next(true);
        this.destroy$.complete();
    }

    getCurrentRiskScoringMethod() {
        this.isLoading = true;
        this.settingsRisksApiService
            .getCurrentRiskAggregateScoringMethod(this.riskReferenceType)
            .pipe(
                takeUntil(this.destroy$),
            )
            .subscribe((setting: IProtocolResponse<RiskMethodType>): void => {
                this.isLoading = false;
                if (setting.result) {
                    this.currentAssessmentRiskScoringMethod = this.assessmentRiskScoringMethods
                        .find((method: IRisksScoring) => method.name.toUpperCase() === setting.data);
                    this.initialAssessmentRiskScoringMethod = this.currentAssessmentRiskScoringMethod;
                    this.pendingAssessmentRiskScoringMethodTransition = this.currentAssessmentRiskScoringMethod;
                }
            });
    }

    onAssessmentRiskScoringMethodSelection(event: IRisksScoring) {
        this.disableSave = false;
        this.pendingAssessmentRiskScoringMethodTransition = event;
        if ((event.name !== this.currentAssessmentRiskScoringMethod.name) && this.pageHasChanges()) {
            this.navigationModalClose$ = this.otModalService.create(
                this.navigationModalContentRef, {});
        } else {
            this.currentAssessmentRiskScoringMethod = this.pendingAssessmentRiskScoringMethodTransition;
        }
    }

    pageHasChanges(): boolean {
        switch (this.currentAssessmentRiskScoringMethod.name) {
            case this.aggregateRiskMethodologies.SUM:
                return this.scoringMethodSumOfRisksComponent.pageHasChanges();
            case this.aggregateRiskMethodologies.COUNT_OF_RISKS:
                return this.scoringMethodCountOfRisksComponent.pageHasChanges();
            case this.aggregateRiskMethodologies.AVERAGE:
                return this.scoringMethodAverageComponent.pageHasChanges();
            case this.aggregateRiskMethodologies.HIGHEST:
                return false;
        }
        return false;
    }

    // Reset Modal Function Calls
    onReset() {
        switch (this.currentAssessmentRiskScoringMethod.name) {
            case this.aggregateRiskMethodologies.COUNT_OF_RISKS:
                this.resetConfirmationMessage = this.riskSettingsTranslationKeys.CountOfRiskMethodologyResetConfirmationDescription;
                break;
            case this.aggregateRiskMethodologies.SUM:
                this.resetConfirmationMessage = this.riskSettingsTranslationKeys.SumRiskMethodologyResetConfirmationDescription;
                break;
        }
        this.resetModalCloseEvent = this.otModalService
            .create(
                this.resetModalContentRef,
                {
                    size: ModalSize.SMALL,
                },
            );
    }

    closeResetModal() {
        this.resetModalCloseEvent.next();
    }

    resetSettings(currentMethod: string) {
        switch (currentMethod) {
            case this.aggregateRiskMethodologies.COUNT_OF_RISKS:
                this.scoringMethodCountOfRisksComponent.resetSettings();
                break;
            case this.aggregateRiskMethodologies.SUM:
                this.scoringMethodSumOfRisksComponent.resetSettings();
                break;
        }
        this.closeResetModal();
    }

    // Change Modal Function Calls
    onSave() {
        if (this.navigationModalClose$) {
            this.navigationModalClose$.next(false);
        }
        this.changeModalCloseEvent = this.otModalService
            .create(
                this.changeModalContentRef,
                {},
            );
    }

    saveSettingsAndRescore() {
        this.savingRiskSettings = true;
        this.saveSettings(true);
    }

    saveSettings(rescore = false) {
        this.savingRiskSettings = true;
        switch (this.currentAssessmentRiskScoringMethod.name) {
            case this.aggregateRiskMethodologies.AVERAGE:
                this.scoringMethodAverageComponent.saveSettings(rescore);
                break;
            case this.aggregateRiskMethodologies.COUNT_OF_RISKS:
                this.scoringMethodCountOfRisksComponent.saveSettings(rescore);
                break;
            case this.aggregateRiskMethodologies.SUM:
                this.scoringMethodSumOfRisksComponent.saveSettings(rescore);
                break;
            case this.aggregateRiskMethodologies.HIGHEST:
                this.settingsRisksApiService
                    .saveRiskAggregateScoringHighestMethodSetting(this.riskReferenceType, rescore)
                    .pipe(takeUntil(this.destroy$))
                    .subscribe((settingSaved: IProtocolResponse<boolean>) => {
                        if (settingSaved.result) {
                            this.notificationService.alertSuccess(
                                this.translatePipe.transform("Success"),
                                this.translatePipe.transform("Risk.HighestMethodologySettingsSavedSuccessfully"));
                            this.updateCurrentScoringMethodologySelection();
                        }
                    });
                break;
        }
    }

    updateCurrentScoringMethodologySelection() {
        this.savingRiskSettings = false;
        if (this.changeModalCloseEvent && !this.changeModalCloseEvent.closed) this.closeChangeModal();
        if (this.navigationModalClose$ && !this.navigationModalClose$.closed) this.closeNavigationModal();
        this.initialAssessmentRiskScoringMethod = this.pendingAssessmentRiskScoringMethodTransition;
        this.currentAssessmentRiskScoringMethod = this.pendingAssessmentRiskScoringMethodTransition;
    }

    // Transition Modal Functions
    setTransitionHook() {
        const transitionDeregisterHook = this.$transitions.onExit({ exiting: "zen.app.pia.module.settings.risks" }, (transition: Transition) => {
            if ((this.initialAssessmentRiskScoringMethod.name !== this.currentAssessmentRiskScoringMethod.name) ||
                this.pageHasChanges()) {
                this.navigationModalClose$ = this.otModalService.create(
                    this.navigationModalContentRef, {});
                return this.navigationModalClose$.pipe(first()).toPromise();
            }
            return true;
        });

        this.destroy$.subscribe(() => {
            transitionDeregisterHook();
        });
    }

    onDiscardChanges() {
        this.initialAssessmentRiskScoringMethod = this.pendingAssessmentRiskScoringMethodTransition;
        this.currentAssessmentRiskScoringMethod = this.pendingAssessmentRiskScoringMethodTransition;
        this.navigationModalClose$.next(true);
    }

    abortNavigation() {
        this.navigationModalClose$.next(false);
    }

    closeChangeModal() {
        this.changeModalCloseEvent.next(true);
        this.changeModalCloseEvent.complete();
    }

    closeNavigationModal() {
        this.navigationModalClose$.next(true);
        this.navigationModalClose$.complete();
    }
}
