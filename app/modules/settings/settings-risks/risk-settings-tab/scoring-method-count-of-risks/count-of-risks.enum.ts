export enum CountOfRisksComparisonOperatorTypes {
    GREATER_THAN = "GREATER_THAN",
    EQUAL_TO = "EQUAL_TO",
}
