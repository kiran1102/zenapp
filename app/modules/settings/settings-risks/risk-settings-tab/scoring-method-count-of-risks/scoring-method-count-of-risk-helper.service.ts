// Angular
import { Injectable } from "@angular/core";
import {
    FormBuilder,
    FormGroup,
    Validators,
} from "@angular/forms";

// Interface
import { ILabelValue } from "interfaces/generic.interface";
import {
    IAggregateRiskScoringMethodCountOfRisksFormulaEntry,
    IAggregateRiskScoringMethodCountOfRisksConditions,
} from "modules/settings/settings-risks/shared/settings-risks.interface";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

// Enum
import { CountOfRisksComparisonOperatorTypes } from "./count-of-risks.enum";
import { POSITIVE_NUMBER_WITH_ZERO } from "constants/regex.constant";

@Injectable()
export class ScoringMethodCountOfRisksHelper {

    countOfRisksComparisonOperators: Array<ILabelValue<string>> = [
        {
            label: this.translatePipe.transform("IsGreaterThan"),
            value: CountOfRisksComparisonOperatorTypes.GREATER_THAN,
        },
        {
            label: this.translatePipe.transform("IsEqualTo"),
            value: CountOfRisksComparisonOperatorTypes.EQUAL_TO,
        },
    ];
    constructor(
        private readonly translatePipe: TranslatePipe,
        private formBuilder: FormBuilder,
    ) { }

    createFormRiskEntry(riskEntry: IAggregateRiskScoringMethodCountOfRisksFormulaEntry): FormGroup {
        return this.formBuilder.group({
            id: [riskEntry.id],
            ruleCategory: [riskEntry.ruleCategory],
            sequence: [riskEntry.sequence],
            conditionGroups: this.createFormRiskConditionGroup(riskEntry.conditionGroups[0].conditions[0]),
            actionType: [riskEntry.actionType],
            actionParametersRiskLevel: [riskEntry.actionParametersRiskLevel, Validators.required],
            actionParameters: [riskEntry.actionParameters],
        });
    }

    createFormRiskConditionGroup(condition: IAggregateRiskScoringMethodCountOfRisksConditions): FormGroup {
        return this.formBuilder.group({
            id: [condition.id],
            field: [condition.field],
            fieldRiskLevel: [condition.fieldRiskLevel, Validators.required],
            operator: [condition.operator, Validators.required],
            operatorSelection: [condition.operatorSelection],
            value: [condition.value, [Validators.required, Validators.pattern(POSITIVE_NUMBER_WITH_ZERO)]],
            logicalOperator: [condition.logicalOperator],
        });
    }
}
