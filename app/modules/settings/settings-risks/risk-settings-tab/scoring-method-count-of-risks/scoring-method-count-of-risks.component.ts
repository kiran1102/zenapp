// Angular
import {
    Component,
    OnInit,
    Input,
    Output,
    OnDestroy,
    EventEmitter,
    ViewChild,
} from "@angular/core";
import {
    FormBuilder,
    FormGroup,
    FormArray,
} from "@angular/forms";

import { CountRisksFormulaComponent } from "./count-risks-formula/count-risks-formula.component";

// Rxjs
import {
    Subject,
    Subscription,
} from "rxjs";
import { takeUntil } from "rxjs/operators";

// Services
import { RiskAPIService } from "oneServices/api/risk-api.service";
import { SettingsRisksApiService } from "../../shared/settings-risks-api.service";
import { ScoringMethodCountOfRisksHelper } from "./scoring-method-count-of-risk-helper.service";
import { NotificationService } from "sharedModules/services/provider/notification.service";

// Interface
import {
    IAggregateRiskScoringMethodCountOfRisksFormula,
    IAggregateRiskScoringMethodCountOfRisksFormulaEntry,
} from "modules/settings/settings-risks/shared/settings-risks.interface";
import { IRiskLevel } from "interfaces/risks-settings.interface";
import { ILabelValue, IStringMap } from "interfaces/generic.interface";

// Constants
import { RiskReferenceType } from "modules/settings/settings-risks/shared/settings-risks.constants";
import { IProtocolResponse } from "interfaces/protocol.interface";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

@Component({
    selector: "scoring-method-count-of-risks",
    templateUrl: "./scoring-method-count-of-risks.component.html",
})

export class ScoringMethodCountOfRisksComponent implements OnInit, OnDestroy {

    @Input() riskReferenceType: RiskReferenceType;
    @Input() riskSettingsTranslationKeys: IStringMap<string>;

    @Output() onSettingsSaved = new EventEmitter();
    @Output() isFormValid = new EventEmitter<boolean>();

    isLoading = false;
    riskLevels: IRiskLevel[];
    riskLevelActionParams: IRiskLevel[];
    riskFormula: IAggregateRiskScoringMethodCountOfRisksFormulaEntry[] = [];
    riskRuleEntries: IAggregateRiskScoringMethodCountOfRisksFormulaEntry[] = [];
    defaultRiskRuleEntry: IAggregateRiskScoringMethodCountOfRisksFormulaEntry;
    templateForm: FormGroup = new FormGroup({});

    @ViewChild(CountRisksFormulaComponent)
    private countRisksFormulaComponent: CountRisksFormulaComponent;
    private templateSubscription: Subscription;

    private destroy$ = new Subject();

    constructor(
        private riskApiService: RiskAPIService,
        private settingsRisksApiService: SettingsRisksApiService,
        private formBuilder: FormBuilder,
        private scoringMethodCountOfRisksHelper: ScoringMethodCountOfRisksHelper,
        private translatePipe: TranslatePipe,
        private notificationService: NotificationService,
    ) { }

    ngOnInit() {
            this.getRiskLevels();
    }

    onFormChanges(): void {
        if (this.templateSubscription && !this.templateSubscription.closed) {
            this.templateSubscription.unsubscribe();
        }
        this.templateSubscription = this.templateForm.valueChanges.subscribe(() => {
            this.templateForm.markAsDirty();
            this.isFormValid.emit(this.templateForm.status === "VALID");
        });
    }

    ngOnDestroy() {
        this.destroy$.next();
        this.destroy$.complete();
    }

    getRiskLevels() {
        this.isLoading = true;
        this.riskApiService.getRiskLevel()
            .pipe(
                takeUntil(this.destroy$),
            )
            .subscribe((riskLevels: IRiskLevel[]) => {
                if (riskLevels) {
                    this.riskLevels = riskLevels;
                    this.riskLevelActionParams = this.riskLevels.map((x) => ({...x}));
                    const riskLevelNone = {
                        id: 0,
                        displayName: "None",
                        name: "NONE",
                        score: null,
                        maxScore: null,
                        minScore: null,
                    };
                    this.riskLevelActionParams.push(riskLevelNone);
                    this.getCountOfRisksFormula();
                }
            });
    }

    getCountOfRisksFormula() {
        this.isLoading = true;
        this.settingsRisksApiService.getRiskAggregateScoringCountOfRiskMethodSetting(this.riskReferenceType)
        .pipe(takeUntil(this.destroy$))
            .subscribe((countOfRisksFormula: IProtocolResponse<IAggregateRiskScoringMethodCountOfRisksFormulaEntry[]>) => {
                if (countOfRisksFormula.result) {
                    this.riskFormula = countOfRisksFormula.data;
                    this.riskRuleEntries = this.riskFormula.slice(0, -1);
                    this.defaultRiskRuleEntry = this.riskFormula[this.riskFormula.length - 1];
                    this.parseRiskLevelsInCurrentFormula(this.riskRuleEntries);
                    this.templateForm = new FormGroup({
                        riskRuleEntries : this.createFromRiskEntries(),
                    });
                    this.onFormChanges();
                }
                this.isLoading = false;
            });
    }

    parseRiskLevelsInCurrentFormula(riskRules: IAggregateRiskScoringMethodCountOfRisksFormulaEntry[]) {
        let sequence = 0;
        riskRules.forEach((currentRiskRule: IAggregateRiskScoringMethodCountOfRisksFormulaEntry) => {
            currentRiskRule.sequence = sequence;
            sequence++;
            currentRiskRule.actionParametersRiskLevel = this.riskLevelActionParams.find((currentRiskLevel: IRiskLevel) => {
                return currentRiskLevel.name === currentRiskRule.actionParameters;
            });
            currentRiskRule.conditionGroups[0].conditions[0].fieldRiskLevel = this.riskLevels.find((currentRiskLevel: IRiskLevel) => {
                return currentRiskLevel.name === currentRiskRule.conditionGroups[0].conditions[0].field;
            });
            currentRiskRule.conditionGroups[0].conditions[0].operatorSelection = this.scoringMethodCountOfRisksHelper.countOfRisksComparisonOperators.find((operatorSelection: ILabelValue<string>) => {
                return operatorSelection.value === currentRiskRule.conditionGroups[0].conditions[0].operator;
            });
        });
        this.defaultRiskRuleEntry.sequence = sequence;
        this.defaultRiskRuleEntry.actionParametersRiskLevel = this.riskLevelActionParams.find((currentRiskLevel: IRiskLevel) => {
            return currentRiskLevel.name === this.defaultRiskRuleEntry.actionParameters;
        });
    }

    createFromRiskEntries(): FormArray {
        return this.formBuilder.array(
            this.riskRuleEntries
                .map((riskEntry: IAggregateRiskScoringMethodCountOfRisksFormulaEntry) => this.scoringMethodCountOfRisksHelper.createFormRiskEntry(riskEntry),
                ),
        );
    }

    createRiskRuleModelFromFormGroup(): IAggregateRiskScoringMethodCountOfRisksFormulaEntry[] {
        const riskRuleEntriesFormValues = this.templateForm.value;
        const newRiskRuleEntries: IAggregateRiskScoringMethodCountOfRisksFormulaEntry[] = [];
        let sequence = 0;
        riskRuleEntriesFormValues.riskRuleEntries.forEach((risk) => {
            const newRiskRuleEntry: IAggregateRiskScoringMethodCountOfRisksFormulaEntry = {
                sequence: sequence++,
                actionType: risk.actionType,
                actionParameters: risk.actionParametersRiskLevel.name,
                conditionGroups: [
                    {
                        logicalOperator: risk.conditionGroups.logicalOperator,
                        conditions: [
                            {
                                field: risk.conditionGroups.fieldRiskLevel.name,
                                operator: risk.conditionGroups.operatorSelection.value,
                                value: risk.conditionGroups.value,
                                logicalOperator: risk.conditionGroups.logicalOperator,
                            },
                        ],
                    },
                ],
            };
            newRiskRuleEntries.push(newRiskRuleEntry);
        });
        return newRiskRuleEntries;
    }

    pageHasChanges(): boolean {
        return this.templateForm.dirty;
    }

    resetSettings() {
        this.isLoading = true;
        this.settingsRisksApiService
            .resetRiskAggregateScoringCountOfRisksMethodSetting(this.riskReferenceType, false)
            .pipe(takeUntil(this.destroy$))
            .subscribe((resetResponse: IProtocolResponse<IAggregateRiskScoringMethodCountOfRisksFormulaEntry[]>) => {
                if (resetResponse.result) {
                    this.riskFormula = resetResponse.data;
                    this.riskRuleEntries = this.riskFormula.slice(0, -1);
                    this.defaultRiskRuleEntry = this.riskFormula[this.riskFormula.length - 1];
                    this.parseRiskLevelsInCurrentFormula(this.riskRuleEntries);
                    this.templateForm = new FormGroup({
                        riskRuleEntries : this.createFromRiskEntries(),
                    });
                    this.onFormChanges();
                    this.notificationService.alertSuccess(
                        this.translatePipe.transform("Success"),
                        this.translatePipe.transform("Risk.CountOfRiskMethodologySettingsResetSuccessfully"));
                }
                this.isLoading = false;
            });
    }

    saveSettings(doRescore: boolean) {
        let riskFormulaEntriesRequest: IAggregateRiskScoringMethodCountOfRisksFormulaEntry[];
        riskFormulaEntriesRequest = this.createRiskRuleModelFromFormGroup().map((x) => ({...x}));
        riskFormulaEntriesRequest.push(this.defaultRiskRuleEntry);
        const reqObject: IAggregateRiskScoringMethodCountOfRisksFormula = {
            rules: riskFormulaEntriesRequest,
        };
        this.settingsRisksApiService
            .saveRiskAggregateScoringCountOfRiskMethodSetting(reqObject, this.riskReferenceType, doRescore)
            .pipe(takeUntil(this.destroy$))
            .subscribe((saveResponse: IProtocolResponse<IAggregateRiskScoringMethodCountOfRisksFormulaEntry[]>) => {
                if (saveResponse.result) {
                    this.onSettingsSaved.emit();
                    this.templateForm.markAsPristine();
                    this.notificationService.alertSuccess(
                        this.translatePipe.transform("Success"),
                        this.translatePipe.transform("CountOfRiskSettingsSavedSuccessfully"));
                }
            });
    }
}
