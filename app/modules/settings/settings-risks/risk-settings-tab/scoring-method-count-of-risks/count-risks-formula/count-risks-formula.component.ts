// Angular
import {
    Component,
    Input,
} from "@angular/core";

import {
    FormGroup,
    FormArray,
} from "@angular/forms";

// Interface
import { IRiskLevel } from "interfaces/risks-settings.interface";

import { IAggregateRiskScoringMethodCountOfRisksFormulaEntry } from "modules/settings/settings-risks/shared/settings-risks.interface";

// Service
import { ScoringMethodCountOfRisksHelper } from "modules/settings/settings-risks/risk-settings-tab/scoring-method-count-of-risks/scoring-method-count-of-risk-helper.service";

// Enums/Constants
import { RiskLevelLabels } from "modules/settings/settings-risks/shared/settings-risks.constants";
import { LogicalOperators } from "enums/operators.enum";
import { CountOfRisksComparisonOperatorTypes } from "modules/settings/settings-risks/risk-settings-tab/scoring-method-count-of-risks/count-of-risks.enum";

@Component({
    selector: "count-risks-formula",
    templateUrl: "./count-risks-formula.component.html",
})

export class CountRisksFormulaComponent {

    @Input() riskLevels: IRiskLevel[];
    @Input() riskLevel: string;
    @Input() riskLevelActionParams: IRiskLevel[];
    @Input() defaultRiskRuleEntry: IAggregateRiskScoringMethodCountOfRisksFormulaEntry;
    @Input() templateForm: FormGroup;

    hasFormulaChanged = false;
    countOfRisksComparisonOperators = this.scoringMethodCountOfRisksHelper.countOfRisksComparisonOperators;

    constructor(
        private scoringMethodCountOfRisksHelper: ScoringMethodCountOfRisksHelper,
    ) { }

    trackBySequence(index: number, ruleEntry: IAggregateRiskScoringMethodCountOfRisksFormulaEntry) {
        return ruleEntry.sequence;
    }

    selectRuleEntryDefaultActionParameters(event: IRiskLevel) {
        this.defaultRiskRuleEntry.actionParametersRiskLevel = event;
        this.defaultRiskRuleEntry.actionParameters = event.name;
        this.templateForm.markAsDirty();
    }

    removeRuleEntry(index: number) {
        (this.templateForm.get("riskRuleEntries") as FormArray).removeAt(index);
        this.templateForm.markAsDirty();
    }

    addRuleEntry(index: number) {
        const newRiskRuleEntry: IAggregateRiskScoringMethodCountOfRisksFormulaEntry = {
            sequence: 0,
            actionType: "AGGREGATE_RISK_INFO_UPDATE",
            actionParameters: RiskLevelLabels.LOW,
            actionParametersRiskLevel: this.riskLevelActionParams.find((action) => action.name === RiskLevelLabels.LOW ),
            conditionGroups: [
                {
                    logicalOperator: LogicalOperators.OR,
                    conditions: [
                        {
                            field: RiskLevelLabels.LOW,
                            fieldRiskLevel: this.riskLevels.find((level) => level.name === RiskLevelLabels.LOW),
                            operator: CountOfRisksComparisonOperatorTypes.GREATER_THAN,
                            operatorSelection: this.countOfRisksComparisonOperators.find((operator) => operator.value === CountOfRisksComparisonOperatorTypes.GREATER_THAN),
                            value: 0,
                            logicalOperator: LogicalOperators.OR,
                        },
                    ],
                },
            ],
        };

        (this.templateForm.get("riskRuleEntries") as FormArray).insert(index + 1,  this.scoringMethodCountOfRisksHelper.createFormRiskEntry(newRiskRuleEntry));
        this.hasFormulaChanged = true;
    }
}
