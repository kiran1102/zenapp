import { ILabelValue } from "interfaces/generic.interface";
import { IRiskLevel } from "interfaces/risks-settings.interface";

export interface ISettingsRiskProbability {
    id: number;
    name: string;
    position: number;
    value: number;
}

export interface ISettingsRiskImpact {
    id: number;
    name: string;
    position: number;
    value: number;
}

export interface ISettingsRiskHeatmapMatrix {
    probability: ISettingsRiskProbability;
    impact: ISettingsRiskProbability;
    score: number;
    color: string;
}

export interface IRiskHeatmapSettings {
    probabilityLevels: ISettingsRiskProbability[];
    impactLevels: ISettingsRiskImpact[];
}

export interface IRisksScoring {
    id: number;
    name: string;
    displayName: string;
}

export interface IRiskScoringMethod {
    id: number;
    methodName: string;
    displayName: string;
}

export interface IAggregateRiskScoreRange {
    riskLevelId: number;
    riskLevelName: string;
    riskLevelDisplayName: string;
    minScore: number;
    maxScore: number;
}

export interface IIndividualRiskSetting {
    currentScoringMethod: IRiskScoringMethod;
    riskLevelList: IRiskLevel[];
    scoringMethods: IRiskScoringMethod[];
}

export interface IAggregateRiskScoringMethodCountOfRisksFormula {
    rules: IAggregateRiskScoringMethodCountOfRisksFormulaEntry[];
}

export interface IAggregateRiskScoringMethodCountOfRisksFormulaEntry {
    id?: string;
    ruleCategory?: string;
    conditionGroups: IAggregateRiskScoringMethodCountOfRisksConditionGroups[];
    sequence: number;
    actionType: string;
    actionParameters: string;
    actionParametersRiskLevel?: IRiskLevel;
}

export interface IAggregateRiskScoringMethodCountOfRisksConditionGroups {
    conditions: IAggregateRiskScoringMethodCountOfRisksConditions[];
    logicalOperator: string;
}

export interface IAggregateRiskScoringMethodCountOfRisksConditions {
    id?: string;
    field: string;
    fieldRiskLevel?: IRiskLevel;
    operator: string;
    operatorSelection?: ILabelValue<string>;
    value: number;
    logicalOperator: string;
}

export interface IAggregateRiskScoringMethodSumOfRisksCondtions {
    scaleLimit: number;
    lowRiskLowerBound: number;
    lowRiskUpperBound: number;
    mediumRiskUpperBound: number;
    highRiskUpperBound: number;
}

export interface IRiskSettingsTranslationsKeys {
    RiskAggregationInfo: string;
    RiskMethodHighestInfo: string;
    ChangeRiskScoringMethodology: string;
    RiskMethodologyChangeConfirmationDescription: string;
    ResetRiskScoringMethodology: string;
    CountOfRiskMethodologyResetConfirmationDescription: string;
    SumRiskMethodologyResetConfirmationDescription: string;
    RiskUnsavedChangesConfirmationMessage: string;
    RiskLevelRanges: string;
    RiskMethodAverageInfo: string;
    RiskFormulaDescription: string;
    RiskLevel: string;
    MethodSumOfRisksDescription: string;
}

export interface IRiskSliderConfigDetails {
    start: number[];
    animate: boolean;
    connect?: boolean[];
    step?: number;
    margin?: number;
    padding?: number | number[];
    tooltips?: boolean[];
    range: {
        min: number;
        max: number;
    };
    pips?: {
        mode: string;
        values?: number | number[];
        density?: number;
        stepped?: boolean;
    };
    format?: unknown;
}
