// Angular
import { Injectable } from "@angular/core";

// Rxjs
import {
    Observable,
    from,
} from "rxjs";

// Services
import { ProtocolService } from "modules/core/services/protocol.service";

// Interfaces
import {
    IProtocolConfig,
    IProtocolMessages,
    IProtocolResponse,
} from "interfaces/protocol.interface";
import {
    IAggregateRiskScoringMethodCountOfRisksFormulaEntry,
    IAggregateRiskScoringMethodCountOfRisksFormula,
    IAggregateRiskScoreRange,
} from "modules/settings/settings-risks/shared/settings-risks.interface";

// Constants + Enums
import {
    RiskReferenceType,
    RiskMethodType,
} from "modules/settings/settings-risks/shared/settings-risks.constants";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

@Injectable()
export class SettingsRisksApiService {

    constructor(
        private readonly oneProtocol: ProtocolService,
        private readonly translatePipe: TranslatePipe,
    ) { }

    // Risk settings APIs
    getCurrentRiskAggregateScoringMethod(referenceType: RiskReferenceType): Observable<IProtocolResponse<RiskMethodType>> {
        const config: IProtocolConfig = this.oneProtocol.customConfig("GET", `/api/risk-v2/v2/risks/aggregate/setting/${referenceType}/method`);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("Setting.Error.RetrievingCurrentAggregationMethod") } };
        return from(this.oneProtocol.http(config, messages));
    }

    getRiskAggregateScoringAverageMethodSetting(referenceType: RiskReferenceType): Observable<IProtocolResponse<IAggregateRiskScoreRange[]>> {
        const config: IProtocolConfig = this.oneProtocol.customConfig("GET", `/api/risk-v2/v2/risks/aggregate/setting/${referenceType}/AVG/range`);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("Setting.Error.RetrievingAverageMethodologySettings") } };
        return from(this.oneProtocol.http(config, messages));
    }

    getRiskAggregateScoringCountOfRiskMethodSetting(referenceType: RiskReferenceType): Observable<IProtocolResponse<IAggregateRiskScoringMethodCountOfRisksFormulaEntry[]>> {
        const config: IProtocolConfig = this.oneProtocol.customConfig("GET", `/api/risk-v2/v2/risks/aggregate/setting/${referenceType}/countMethod/rules`);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("Setting.Error.RetrievingCountOfRiskMethodologySettings") } };
        return from(this.oneProtocol.http(config, messages));
    }

    getRiskAggregateScoringSumOfRiskMethodSetting(referenceType: RiskReferenceType): Observable<IProtocolResponse<IAggregateRiskScoreRange[]>> {
        const config: IProtocolConfig = this.oneProtocol.customConfig("GET", `/api/risk-v2/v2/risks/aggregate/setting/${referenceType}/SUM/range`);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("Setting.Error.RetrievingSumOfRiskMethodologySettings") } };
        return from(this.oneProtocol.http(config, messages));
    }

    saveRiskAggregateScoringHighestMethodSetting(referenceType: RiskReferenceType, rescore: boolean): Observable<IProtocolResponse<boolean>> {
        const config: IProtocolConfig = this.oneProtocol.customConfig("POST", `/api/risk-v2/v2/risks/aggregate/setting/${referenceType}/highestMethod`, { rescore });
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("Setting.Error.SavingHighestMethodologySettings") } };
        return from(this.oneProtocol.http(config, messages));
    }

    saveRiskAggregateScoringAverageMethodSetting(
            referenceType: RiskReferenceType,
            requestBody: IAggregateRiskScoreRange[],
            rescore: boolean): Observable<IProtocolResponse<IAggregateRiskScoreRange[]>> {
        const config: IProtocolConfig = this.oneProtocol.
            customConfig("POST", `/api/risk-v2/v2/risks/aggregate/setting/${referenceType}/AVG/range`, { rescore }, requestBody);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("Setting.Error.SavingAverageMethodologySettings") } };
        return from(this.oneProtocol.http(config, messages));
    }

    saveRiskAggregateScoringCountOfRiskMethodSetting(
            inventoryAssessmentCountOfRisksFormula: IAggregateRiskScoringMethodCountOfRisksFormula,
            referenceType: RiskReferenceType,
            rescore: boolean): Observable<IProtocolResponse<IAggregateRiskScoringMethodCountOfRisksFormulaEntry[]>> {
        const config: IProtocolConfig = this.oneProtocol.customConfig("POST", `/api/risk-v2/v2/risks/aggregate/setting/${referenceType}/countMethod/rules`, { rescore }, inventoryAssessmentCountOfRisksFormula);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("Setting.Error.SavingCountOfRiskMethodologySettings") } };
        return from(this.oneProtocol.http(config, messages));
    }

    saveRiskAggregateScoringSumOfRiskMethodSetting(
        sumOfRisksFormula: IAggregateRiskScoreRange[],
        referenceType: RiskReferenceType,
        rescore: boolean): Observable<IProtocolResponse<IAggregateRiskScoreRange[]>> {
        const config: IProtocolConfig = this.oneProtocol.customConfig("POST", `/api/risk-v2/v2/risks/aggregate/setting/${referenceType}/SUM/range`, { rescore }, sumOfRisksFormula);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("Setting.Error.SavingSumOfRiskMethodologySettings") } };
        return from(this.oneProtocol.http(config, messages));
    }

    resetRiskAggregateScoringCountOfRisksMethodSetting(referenceType: RiskReferenceType, rescore: boolean): Observable<IProtocolResponse<IAggregateRiskScoringMethodCountOfRisksFormulaEntry[]>> {
        const config: IProtocolConfig = this.oneProtocol.customConfig("PUT", `/api/risk-v2/v2/risks/aggregate/setting/${referenceType}/countMethod/rules/reset`, { rescore });
        const messages: IProtocolMessages = {
            Error: { custom: this.translatePipe.transform("CouldNotUpdateSettings") },
        };
        return from(this.oneProtocol.http(config, messages));
    }

    resetRiskAggregateScoringSumOfRisksMethodSetting(referenceType: RiskReferenceType, rescore: boolean): Observable<IProtocolResponse<IAggregateRiskScoreRange[]>> {
        const config: IProtocolConfig = this.oneProtocol.customConfig("PUT", `/api/risk-v2/v2/risks/aggregate/setting/${referenceType}/SUM/range/reset`, { rescore });
        const messages: IProtocolMessages = {
            Error: { custom: this.translatePipe.transform("CouldNotUpdateSettings") },
        };
        return from(this.oneProtocol.http(config, messages));
    }
}
