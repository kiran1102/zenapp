import { IRiskSettingsTranslationsKeys } from "./settings-risks.interface";

export enum RiskLevelLabels {
    NONE = "NONE",
    LOW = "LOW",
    MEDIUM = "MEDIUM",
    HIGH = "HIGH",
    VERY_HIGH = "VERY_HIGH",
}

export enum RiskMethodType {
    HIGHEST = "HIGHEST",
    AVERAGE = "AVERAGE",
    COUNTOFRISKS = "COUNTOFRISKS",
}

export enum RiskReferenceType {
    PIA = "PIA",
    ASSETS = "ASSETS",
    PROCESSING_ACTIVITIES = "PROCESSING_ACTIVITIES",
    VENDORS = "VENDORS",
    ENTITIES = "ENTITIES",
}

export const AggregateRiskMethodologies = {
    HIGHEST: "Highest",
    AVERAGE: "Average",
    COUNT_OF_RISKS: "CountOfRisks",
    SUM: "Sum",
};

export const RiskLevelColorClasses = {
    LOW: "risk-low--bg",
    MEDIUM: "risk-medium--bg ",
    HIGH: "risk-high--bg",
    VERY_HIGH: "risk-very-high--bg ",
};

export const RiskTabNameKeys = {
    INDIVIDUAL: "Individual",
    ASSESSMENT: "Assessment",
    ASSET: "Asset",
    PROCESSING_ACTIVITY: "ProcessingActivity",
    VENDOR: "Vendor",
    ENTITY: "Entity",
};

export const SumOfRisksPageScaleValueKeys = {
    SCALE_LIMIT: "SCALE_LIMIT",
    NO_RISK_UPPER: "NO_RISK_UPPER",
    LOW_UPPER: "LOW_UPPER",
    MEDIUM_UPPER: "MEDIUM_UPPER",
    HIGH_UPPER: "HIGH_UPPER",
};

export const AssessmentRiskTranslationKeys: IRiskSettingsTranslationsKeys = {
    RiskAggregationInfo: "AssessmentRiskAggregationInfo",
    RiskMethodHighestInfo: "AssessmentRiskMethodHighestInfo",
    ChangeRiskScoringMethodology: "ChangeAssessmentRiskScoringMethodology",
    RiskMethodologyChangeConfirmationDescription: "AssessmentRiskMethodologyChangeConfirmationDescription",
    ResetRiskScoringMethodology: "ResetAssessmentRiskScoringMethodology",
    CountOfRiskMethodologyResetConfirmationDescription: "AssessmentRiskMethodologyResetConfirmationDescription",
    SumRiskMethodologyResetConfirmationDescription: "SumRiskMethodologyResetConfirmationDescription",
    RiskUnsavedChangesConfirmationMessage: "RiskUnsavedChangesConfirmationMessage",
    RiskLevelRanges: "AssessmentRiskLevelRanges",
    RiskMethodAverageInfo: "AssessmentRiskMethodAverageInfo",
    RiskFormulaDescription: "RiskFormulaDescription",
    RiskLevel: "AssessmentRiskLevel",
    MethodSumOfRisksDescription: "Risk.MethodSumOfRisksDescription.Assessment",
};

export const AssetRiskTranslationKeys: IRiskSettingsTranslationsKeys = {
    RiskAggregationInfo: "Risk.AssetRiskAggregationInfo",
    RiskMethodHighestInfo: "Risk.AssetHighestMethodInfo",
    ChangeRiskScoringMethodology: "Risk.ChangeAssetRiskScoringMethodology",
    RiskMethodologyChangeConfirmationDescription: "Risk.ChangeAssetMethodologyConfirmationDescription",
    ResetRiskScoringMethodology: "Risk.ResetAssetRiskScoringMethodology",
    CountOfRiskMethodologyResetConfirmationDescription: "AssessmentRiskMethodologyResetConfirmationDescription",
    SumRiskMethodologyResetConfirmationDescription: "SumRiskMethodologyResetConfirmationDescription",
    RiskUnsavedChangesConfirmationMessage: "RiskUnsavedChangesConfirmationMessage",
    RiskLevelRanges: "Risk.AssetRiskLevelRanges",
    RiskMethodAverageInfo: "Risk.AssetRiskMethodAverageInfo",
    RiskFormulaDescription: "Risk.AssetRiskFormulaDescription",
    RiskLevel: "Risk.AssetRiskLevel",
    MethodSumOfRisksDescription: "Risk.MethodSumOfRisksDescription.Asset",
};

export const ProcessingActivityRiskTranslationKeys: IRiskSettingsTranslationsKeys = {
    RiskAggregationInfo: "Risk.PARiskAggregationInfo",
    RiskMethodHighestInfo: "Risk.PAHighestMethodInfo",
    ChangeRiskScoringMethodology: "Risk.ChangePARiskScoringMethodology",
    RiskMethodologyChangeConfirmationDescription: "Risk.ChangePAMethodologyConfirmationDescription",
    ResetRiskScoringMethodology: "Risk.ResetPARiskScoringMethodology",
    CountOfRiskMethodologyResetConfirmationDescription: "AssessmentRiskMethodologyResetConfirmationDescription",
    SumRiskMethodologyResetConfirmationDescription: "SumRiskMethodologyResetConfirmationDescription",
    RiskUnsavedChangesConfirmationMessage: "RiskUnsavedChangesConfirmationMessage",
    RiskLevelRanges: "Risk.PARiskLevelRanges",
    RiskMethodAverageInfo: "Risk.PARiskMethodAverageInfo",
    RiskFormulaDescription: "Risk.PARiskFormulaDescription",
    RiskLevel: "Risk.PARiskLevel",
    MethodSumOfRisksDescription: "Risk.MethodSumOfRisksDescription.ProcessingActivity",
};

export const VendorRiskTranslationKeys: IRiskSettingsTranslationsKeys = {
    RiskAggregationInfo: "Risk.VendorRiskAggregationInfo",
    RiskMethodHighestInfo: "Risk.VendorHighestMethodInfo",
    ChangeRiskScoringMethodology: "Risk.ChangeVendorRiskScoringMethodology",
    RiskMethodologyChangeConfirmationDescription: "Risk.ChangeVendorMethodologyConfirmationDescription",
    ResetRiskScoringMethodology: "Risk.ResetVendorRiskScoringMethodology",
    CountOfRiskMethodologyResetConfirmationDescription: "AssessmentRiskMethodologyResetConfirmationDescription",
    SumRiskMethodologyResetConfirmationDescription: "SumRiskMethodologyResetConfirmationDescription",
    RiskUnsavedChangesConfirmationMessage: "RiskUnsavedChangesConfirmationMessage",
    RiskLevelRanges: "Risk.VendorRiskLevelRanges",
    RiskMethodAverageInfo: "Risk.VendorRiskMethodAverageInfo",
    RiskFormulaDescription: "Risk.VendorRiskFormulaDescription",
    RiskLevel: "Risk.VendorRiskLevel",
    MethodSumOfRisksDescription: "Risk.MethodSumOfRisksDescription.Vendor",
};

export const EntityRiskTranslationKeys: IRiskSettingsTranslationsKeys = {
    RiskAggregationInfo: "Risk.EntityRiskAggregationInfo",
    RiskMethodHighestInfo: "Risk.EntityHighestMethodInfo",
    ChangeRiskScoringMethodology: "Risk.ChangeEntityRiskScoringMethodology",
    RiskMethodologyChangeConfirmationDescription: "Risk.ChangeEntityMethodologyConfirmationDescription",
    ResetRiskScoringMethodology: "Risk.ResetEntityRiskScoringMethodology",
    CountOfRiskMethodologyResetConfirmationDescription: "AssessmentRiskMethodologyResetConfirmationDescription",
    SumRiskMethodologyResetConfirmationDescription: "SumRiskMethodologyResetConfirmationDescription",
    RiskUnsavedChangesConfirmationMessage: "RiskUnsavedChangesConfirmationMessage",
    RiskLevelRanges: "Risk.EntityRiskLevelRanges",
    RiskMethodAverageInfo: "Risk.EntityRiskMethodAverageInfo",
    RiskFormulaDescription: "Risk.EntityRiskFormulaDescription",
    RiskLevel: "Risk.EntityRiskLevel",
    MethodSumOfRisksDescription: "Risk.MethodSumOfRisksDescription.Entity",
};
