// Angular
import { Injectable } from "@angular/core";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

// Interfaces
import { IRisksScoring } from "modules/settings/settings-risks/shared/settings-risks.interface";

// Enum / Constants
import { AggregateRiskMethodologies } from "modules/settings/settings-risks/shared/settings-risks.constants";

@Injectable()
export class SettingsRisksHelper {

    constructor(
        private readonly translatePipe: TranslatePipe,
    ) { }

    getAggregateRiskScoringMethods(): IRisksScoring[] {
        const aggregateRiskMethodologies: IRisksScoring[] = [];
        let initialId = 1;
        Object.values(AggregateRiskMethodologies).forEach((methodName: string) => {
            aggregateRiskMethodologies.push({
                id: initialId++,
                name: methodName,
                displayName: this.translatePipe.transform(methodName),
            });
        });
        return aggregateRiskMethodologies;
    }

}
