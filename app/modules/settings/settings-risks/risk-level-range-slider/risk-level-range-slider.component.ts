import {
    Component,
    Input,
    OnInit,
    EventEmitter,
    Output,
} from "@angular/core";

// Interface
import { IAggregateRiskScoreRange } from "modules/settings/settings-risks/shared/settings-risks.interface";

// Constants/Enums
import { Color } from "constants/color.constant";

interface IRiskSliderConfigDetails {
    start: number[];
    connect?: boolean[];
    step?: number;
    margin?: number;
    padding?: number | number[];
    tooltips?: boolean[];
    range: {
        min: number;
        max: number;
    };
    pips?: {
        mode: string;
        values?: number | number[];
        density?: number;
        stepped?: boolean;
    };
}

const RiskSliderDefaults = {
    STEP: 0.1,
    MARGIN: 0.1,
    PADDING: [0.1, 0],
    MIN: 0.1,
    MAX: 8,
    PIP_MODE: "values",
    PIPS_DENSITY: 6,
    PIPS_VALUES: [.1, 1, 2, 3, 4, 5, 6, 7, 8],
};

@Component({
    selector: "risk-level-range-slider",
    templateUrl: "./risk-level-range-slider.component.html",
})

export class RiskLevelRangeSliderComponent implements OnInit {

    @Input() riskLevelRange: IAggregateRiskScoreRange[];

    @Output() onRiskLevelChange = new EventEmitter<IAggregateRiskScoreRange[]>();

    config: IRiskSliderConfigDetails;

    connectedBarColors = [Color.RiskLow, Color.RiskMedium, Color.RiskHigh, Color.RiskVeryHigh];
    handleColors = [Color.RiskMedium, Color.RiskHigh, Color.RiskVeryHigh];

    ngOnInit() {
        this.initializeSlider();
    }

    initializeSlider() {
        this.config = {
            start: [],
            connect: [],
            tooltips: [],
            step: RiskSliderDefaults.STEP,
            margin: RiskSliderDefaults.MARGIN,
            padding: RiskSliderDefaults.PADDING,
            range: {
                min: RiskSliderDefaults.MIN,
                max: RiskSliderDefaults.MAX,
            },
            pips: {
                mode: RiskSliderDefaults.PIP_MODE,
                values: RiskSliderDefaults.PIPS_VALUES,
                density: RiskSliderDefaults.PIPS_DENSITY,
                stepped: true,
            },
        };
        // Initialize the slider
        this.config.connect.push(true);
        for (let i = 1; i < this.riskLevelRange.length; i++) {
            this.config.start.push(this.riskLevelRange[i].minScore);
            this.config.tooltips.push(true);
            this.config.connect.push(true);
        }
        this.config.range.min = this.riskLevelRange[0].minScore;
        this.config.range.max = this.riskLevelRange[this.riskLevelRange.length - 1].maxScore;
    }

    onValuesUpdate(event: string[]): void {
        const updatedRiskLevelRange: IAggregateRiskScoreRange[] = [];
        updatedRiskLevelRange.push({
            ...this.riskLevelRange[0],
        });
        for (let i = 0; i < event.length; i++) {
            const riskValue = parseFloat(event[i]);
            updatedRiskLevelRange[i].maxScore = parseFloat((riskValue - this.config.step).toFixed(2));
            updatedRiskLevelRange.push({
                ...this.riskLevelRange[i + 1],
                minScore: riskValue,
            });
        }
        this.onRiskLevelChange.emit(updatedRiskLevelRange);
    }
}
