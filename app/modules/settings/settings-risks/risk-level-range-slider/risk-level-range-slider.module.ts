// Angular
import { NgModule } from "@angular/core";

// Other modules
import { VitreusModule } from "@onetrust/vitreus";
import { SharedModule } from "sharedModules/shared.module";

// Components
import { RiskLevelRangeSliderComponent } from "./risk-level-range-slider.component";

@NgModule({
    imports: [
        SharedModule,
        VitreusModule,
    ],
    declarations: [
        RiskLevelRangeSliderComponent,
    ],
    exports: [
        RiskLevelRangeSliderComponent,
    ],
})
export class RiskLevelRangeSliderModule { }
