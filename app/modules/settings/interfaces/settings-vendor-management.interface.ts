export interface INameValue<T> {
    name: T;
    value: T;
    valueType?: T;
}
