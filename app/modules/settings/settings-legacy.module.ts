import * as angular from "angular";

import { routes } from "./settings.routes";

import { LocalizationActionService } from "settingsServices/actions/localization-action.service";

import { settingsWrapper } from "settingsComponents/settings-wrapper/settings-wrapper.component";
import { settingsEmailTemplate } from "settingsComponents/settings-email-template/settings-email-template.component";

import { SettingActionService } from "./services/actions/setting-action.service";
import { SettingSmtpAdapterService } from "./services/adapters/setting-smtp-adapter.service";
import { SettingSmtpActionService } from "settingsServices/actions/setting-smtp-action.service";
import { SettingConsentActionService } from "settingsServices/actions/setting-consent-action.service";

import { SettingEmailApiService } from "./services/apis/setting-email-api.service";
import { SettingEmailActionService } from "./services/actions/setting-email-action.service";
import { SettingsManageSubscriptionsService } from "./services/apis/settings-manage-subscriptions-api.service";

export const settingsLegacyModule = angular
    .module("zen.settings", [
        "ui.router",
        // Custom modules
        "zen.consentreceipt",
    ])
    .config(routes)

    .service("LocalizationActions", LocalizationActionService)

    .component("settingsWrapper", settingsWrapper)
    .component("settingsEmailTemplate", settingsEmailTemplate)

    .service("SettingAction", SettingActionService)
    .service("SettingConsentAction", SettingConsentActionService)
    .service("SettingSmtpAction", SettingSmtpActionService)
    .service("SettingSmtpAdapter", SettingSmtpAdapterService)

    .service("SettingEmailAction", SettingEmailActionService)
    .service("SettingEmailApi", SettingEmailApiService)
    .service("SettingsManageSubscriptionService", SettingsManageSubscriptionsService)
    ;
