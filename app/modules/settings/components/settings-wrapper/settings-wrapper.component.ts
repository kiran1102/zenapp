// RxJs
import { BehaviorSubject } from "rxjs";

// Services
import { StateService } from "@uirouter/core";
import { Permissions } from "modules/shared/services/helper/permissions.service";
import { GlobalSidebarService } from "modules/global-sidebar/services/one-global-sidebar.service";
import { McmModalService } from "modules/mcm/mcm-modal.service";

// Interfaces
import { INavMenuItem } from "modules/global-sidebar/interfaces/nav-menu-item.interface";
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";

class SettingsWrapperCtrl implements ng.IComponentController {
    static $inject: string[] = [
        "Permissions",
        "$rootScope",
        "GlobalSidebarService",
        "McmModalService",
    ];

    menu$: BehaviorSubject<INavMenuItem[]>;
    title$: BehaviorSubject<string>;
    collapsible$: BehaviorSubject<boolean>;

    constructor(
        private permissions: Permissions,
        private $rootScope: IExtendedRootScopeService,
        private globalSidebarService: GlobalSidebarService,
        private mcmModalService: McmModalService,
    ) { }

    $onInit() {
        const menuRoutes = this.getMenuRoutes();
        this.globalSidebarService.set();
        this.menu$ = new BehaviorSubject<INavMenuItem[]>(menuRoutes);
        this.title$ = new BehaviorSubject<string>(null);
        this.collapsible$ = new BehaviorSubject<boolean>(false);
    }

    private getMenuRoutes() {
        const menuGroup: INavMenuItem[] = [];
        const generalSettingsGroup: INavMenuItem = {
            id: "SettingsGeneralSidebarItemGroup",
            title: this.$rootScope.t("General"),
            children: [],
            open: true,
        };
        const UsersAndGroupsSettingsGroup: INavMenuItem = {
            id: "SettingsUsersAndGroupsSidebarItemGroup",
            title: this.$rootScope.t("UsersAndGroups"),
            children: [],
            open: true,
        };
        const ToolsSettingsGroup: INavMenuItem = {
            id: "SettingsToolsSidebarItemGroup",
            title: this.$rootScope.t("Tools"),
            children: [],
            open: true,
        };
        const customizationSettingsGroup: INavMenuItem = {
            id: "SettingsCustomizationSidebarItemGroup",
            title: this.$rootScope.t("Customization"),
            children: [],
            open: true,
        };
        const systemSettingsGroup: INavMenuItem = {
            id: "SettingsSystemSidebarItemGroup",
            title: this.$rootScope.t("System"),
            children: [],
            open: true,
        };
        const assessmentsSettingGroup: INavMenuItem = {
            id: "SettingsAssessmentSidebarItemGroup",
            title: this.$rootScope.t("AssessmentAutomation"),
            children: [],
            open: true,
        };
        const dataMappingSettingsGroup: INavMenuItem = {
            id: "SettingsDataMappingSidebarItemGroup",
            title: this.$rootScope.t("DataMapping"),
            children: [],
            open: true,
        };

        if (!this.permissions.canShow("Assessments") && this.permissions.canShow("Projects")) {
            generalSettingsGroup.children.push({
                id: "SettingsProjectsSidebarItem",
                title: this.$rootScope.t("Projects"),
                route: "zen.app.pia.module.settings.project",
                activeRoute: "zen.app.pia.module.settings.project",
            });
        }
        if (this.permissions.canShow("SettingsConsent")) {
            generalSettingsGroup.children.push({
                id: "SettingsConsentSidebarItem",
                title: this.$rootScope.t("Consent"),
                route: "zen.app.pia.module.settings.consent",
                activeRoute: "zen.app.pia.module.settings.consent",
            });
        }
        if (this.permissions.canShow("SelfServicePortalConfig")) {
            generalSettingsGroup.children.push({
                id: "SettingsSelfServicePortalSidebarItem",
                title: this.$rootScope.t("SelfServicePortal"),
                route: "zen.app.pia.module.settings.ssp",
                activeRoute: "zen.app.pia.module.settings.ssp",
            });
        }
        if (!this.permissions.canShow("Assessments") && this.permissions.canShow("SettingsTagging")) {
            generalSettingsGroup.children.push({
                id: "SettingsTaggingSidebarItem",
                title: this.$rootScope.t("Tagging"),
                route: "zen.app.pia.module.settings.tagging",
                activeRoute: "zen.app.pia.module.settings.tagging",
            });
        }
        if (this.permissions.canShow("TaggingUpgrade")) {
            generalSettingsGroup.children.push({
                id: "SettingsTaggingSidebarItem",
                title: this.$rootScope.t("Tagging"),
                upgrade: true,
                action: (): void => this.mcmModalService.openMcmModal("TaggingUpgrade"),
            });
        }
        if (this.permissions.canShow("AssessmentAutomationSettings")) {
            assessmentsSettingGroup.children.push({
                id: "SettingsAssessmentTemplateSidebarItem",
                title: this.$rootScope.t("Templates"),
                route: "zen.app.pia.module.settings.assessments.template",
                activeRoute: "zen.app.pia.module.settings.assessments.template",
            });

            assessmentsSettingGroup.children.push({
                id: "SettingsAssessmentSidebarItem",
                title: this.$rootScope.t("Assessments"),
                route: "zen.app.pia.module.settings.assessments.assessment",
                activeRoute: "zen.app.pia.module.settings.assessments.assessment",
            });
        }
        if ((!this.permissions.canShow("Assessments") && this.permissions.canShow("AssessmentRiskSettings"))
            || this.permissions.canShow("RiskV2Setting")) {
            generalSettingsGroup.children.push({
                id: "SettingsRisksSidebarItem",
                title: this.$rootScope.t("Risks"),
                route: "zen.app.pia.module.settings.risks",
                activeRoute: "zen.app.pia.module.settings.risks",
            });
        }
        if (this.permissions.canShow("SettingsSSO") && !this.permissions.canShow("SettingsSSONew")) {
            generalSettingsGroup.children.push({
                id: "SettingsSingleSignOnSidebarItem",
                title: this.$rootScope.t("SingleSignOn"),
                route: "zen.app.pia.module.settings.directory",
                activeRoute: "zen.app.pia.module.settings.directory",
            });
        }
        if (this.permissions.canShow("SettingsSSOUpgrade")) {
            generalSettingsGroup.children.push({
                id: "SettingsSingleSignOnSidebarItem",
                title: this.$rootScope.t("SingleSignOn"),
                upgrade: true,
                action: (): void => this.mcmModalService.openMcmModal("SettingsSSOUpgrade"),
            });
        }
        if (this.permissions.canShow("CustomSMTPEditor")) {
            generalSettingsGroup.children.push({
                id: "SettingsEmailSettingsSidebarItem",
                title: this.$rootScope.t("EmailSettings"),
                route: "zen.app.pia.module.settings.smtp",
                activeRoute: "zen.app.pia.module.settings.smtp",
            });
        }
        if (this.permissions.canShow("SettingsCustomBranding")) {
            customizationSettingsGroup.children.push({
                id: "SettingsCustomBrandingSidebarItem",
                title: this.$rootScope.t("CustomBranding"),
                route: "zen.app.pia.module.settings.branding",
                activeRoute: "zen.app.pia.module.settings.branding",
            });
        }
        if (this.permissions.canShow("SettingsCustomBrandingUpgrade")) {
            customizationSettingsGroup.children.push({
                id: "SettingsCustomBrandingSidebarItem",
                title: this.$rootScope.t("CustomBranding"),
                upgrade: true,
                action: (): void => this.mcmModalService.openMcmModal("SettingsCustomBrandingUpgrade"),
            });
        }
        if (this.permissions.canShow("SettingsCustomEmailTemplate") && !this.permissions.canShow("CustomEmailTemplateView")) {
            customizationSettingsGroup.children.push({
                id: "SettingsEmailTemplateSidebarItem",
                title: this.$rootScope.t("EmailTemplate"),
                route: "zen.app.pia.module.settings.messagetemplate",
                params: { typeId: 0, languageCode: "en-us" },
                activeRoute: "zen.app.pia.module.settings.messagetemplate",
            });
        }
        if (this.permissions.canShow("CustomEmailTemplateView")) {
            customizationSettingsGroup.children.push({
                id: "SettingsEmailTemplateSidebarItem",
                title: this.$rootScope.t("EmailTemplate"),
                route: "zen.app.pia.module.settings.messagetemplatev2.list",
                params: { typeId: 0, languageCode: "en-us" },
                activeRoute: "zen.app.pia.module.settings.messagetemplatev2",
            });
        }
        if (this.permissions.canShow("CustomEmailTemplateView")) {
            customizationSettingsGroup.children.push({
                id: "SettingsLanguageActivatorHeadingSidebarItem",
                title: this.$rootScope.t("LanguageActivatorHeading"),
                route: "zen.app.pia.module.settings.languageactivator",
                activeRoute: "zen.app.pia.module.settings.languageactivator",
            });
        }
        if (this.permissions.canShow("SettingsCustomEmailTemplateUpgrade")) {
            customizationSettingsGroup.children.push({
                id: "SettingsEmailTemplateSidebarItem",
                title: this.$rootScope.t("EmailTemplate"),
                upgrade: true,
                action: (): void => this.mcmModalService.openMcmModal("SettingsCustomEmailTemplateUpgrade"),
            });
        }
        if (this.permissions.canShow("LocalizationEditor")) {
            customizationSettingsGroup.children.push({
                id: "SettingsLocalizationEditorSidebarItem",
                title: this.$rootScope.t("LocalizationEditor"),
                route: "zen.app.pia.module.settings.localization",
                activeRouteFn: (stateService: StateService) => {
                    return stateService.includes("zen.app.pia.module.settings.localization");
                },
                params: { page: null, size: null, search: null, module: "System" },
            });
        }
        if (this.permissions.canShow("SettingsCustomTerminology")) {
            customizationSettingsGroup.children.push({
                id: "SettingsCustomTerminologySidebarItem",
                title: this.$rootScope.t("CustomTerminology"),
                route: "zen.app.pia.module.settings.terminology",
                activeRoute: "zen.app.pia.module.settings.terminology",
            });
        }
        if (this.permissions.canShow("SettingsCustomTerminologyUpgrade")) {
            customizationSettingsGroup.children.push({
                id: "SettingsCustomTerminologySidebarItem",
                title: this.$rootScope.t("CustomTerminology"),
                upgrade: true,
                action: (): void => this.mcmModalService.openMcmModal("SettingsCustomTerminologyUpgrade"),
            });
        }
        if (this.permissions.canShow("UISettingsAdminUser")) {
            customizationSettingsGroup.children.push({
                id: "SettingsDateTimeSettingsSidebarItem",
                title: this.$rootScope.t("DateTimeSettings"),
                route: "zen.app.pia.module.settings.timestamp",
                activeRoute: "zen.app.pia.module.settings.timestamp",
            });
        }
        if (this.permissions.canShow("DateTimeSettingsUpgrade")) {
            customizationSettingsGroup.children.push({
                id: "SettingsDateTimeSettingsSidebarItem",
                title: this.$rootScope.t("DateTimeSettings"),
                upgrade: true,
                action: (): void => this.mcmModalService.openMcmModal("Upgrade"),
            });
        }
        if (this.permissions.canShow("SettingsHelp")) {
            systemSettingsGroup.children.push({
                id: "SettingsHelpSidebarItem",
                title: this.$rootScope.t("Help"),
                route: "zen.app.pia.module.settings.help-and-privacy",
                activeRoute: "zen.app.pia.module.settings.help-and-privacy",
            });
        }
        if (this.permissions.canShow("SettingsSSONew")) {
            generalSettingsGroup.children.push({
                id: "SettingsSingleSignOnSidebarItem",
                title: this.$rootScope.t("SingleSignOn"),
                route: "zen.app.pia.module.settings.sso",
                activeRoute: "zen.app.pia.module.settings.sso",
            });
        }
        if (this.permissions.canShow("ScimAdminSetting")) {
            generalSettingsGroup.children.push({
                id: "SettingsScimsSidebarItem",
                title: this.$rootScope.t("scim"),
                route: "zen.app.pia.module.settings.scim-settings",
                activeRoute: "zen.app.pia.module.settings.scim-settings",
            });
        }
        if (this.permissions.canShow("ManageSubscriptions")) {
            generalSettingsGroup.children.push({
                id: "SettingsManageSubscriptionsSidebarItem",
                title: this.$rootScope.t("ManageSubscriptions"),
                route: "zen.app.pia.module.settings.manage-subscriptions",
                activeRoute: "zen.app.pia.module.settings.manage-subscriptions",
            });
        }
        if ((this.permissions.canShow("AdminToolSetting")) && (this.permissions.canShow("UserFeedBack") || this.permissions.canShow("UserGuidance"))) {
            generalSettingsGroup.children.push({
                id: "SettingsLearningAndFeedbackToolSidebarItem",
                title: this.$rootScope.t("LearningAndFeedbackTools"),
                route: "zen.app.pia.module.settings.learning-and-feedback-tool",
                activeRoute: "zen.app.pia.module.settings.learning-and-feedback-tool",
            });
        }
        if (this.permissions.canShow("DataMappingInventorySettings")) {
            dataMappingSettingsGroup.children.push({
                id: "SettingsInventorySettingsSidebarItem",
                title: this.$rootScope.t("Inventory"),
                route: "zen.app.pia.module.settings.inventory-settings",
                activeRoute: "zen.app.pia.module.settings.inventory-settings",
            });
        }
        if (this.permissions.canShow("EnableDataSubjectsElementsV2") && this.permissions.canShow("DataMappingManageDataElementVisibility")) {
            dataMappingSettingsGroup.children.push({
                id: "SettingsDataMappingOrganizationVisibilityFiltersSidebarItem",
                title: this.$rootScope.t("OrganizationVisibilityFilters"),
                route: "zen.app.pia.module.settings.manage-visibility",
                activeRoute: "zen.app.pia.module.settings.manage-visibility",
            });
        }
        if (generalSettingsGroup.children.length) {
            menuGroup.push(generalSettingsGroup);
        }
        if (dataMappingSettingsGroup.children.length) {
            menuGroup.push(dataMappingSettingsGroup);
        }
        if (assessmentsSettingGroup.children.length &&  this.permissions.canShow("AssessmentAutomationSettings")) {
            menuGroup.push(assessmentsSettingGroup);
        }
        if (UsersAndGroupsSettingsGroup.children.length) {
            menuGroup.push(UsersAndGroupsSettingsGroup);
        }
        if (this.permissions.canShow("VendorManagementSettings")) {
            menuGroup.push({
                id: "SettingsVendorManagementSidebarItem",
                title: this.$rootScope.t("VendorRiskManagement"),
                route: "zen.app.pia.module.settings.vendor-management",
                activeRoute: "zen.app.pia.module.settings.vendor-management",
            });
        }
        if (customizationSettingsGroup.children.length) {
            menuGroup.push(customizationSettingsGroup);
        }
        if (systemSettingsGroup.children.length) {
            menuGroup.push(systemSettingsGroup);
        }

        return menuGroup;
    }

}

export const settingsWrapper: ng.IComponentOptions = {
    controller: SettingsWrapperCtrl,
    template: `
        <div class="settings-wrapper column-nowrap full-size stretch-vertical text-color-default background-white">
            <one-header class="settings-wrapper__header static-vertical text-center color-white">
                <header-title></header-title>
                <header-body>
                    <span class="heading2 text-thin text-center" ng-bind="$root.t('GlobalSettings')"></span>
                </header-body>
            </one-header>
            <div class="row-nowrap full-size stretch-vertical">
                <one-global-sidebar
                    [menu$]="$ctrl.menu$"
                    [title$]="$ctrl.title$"
                    [light-theme]="true"
                    [prevent-collapse]="true">
                </one-global-sidebar>
                <ui-view class="stretch-horizontal full-size"></ui-view>
            </div>
        </div>
    `,
};
