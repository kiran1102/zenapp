// Angular
import {
    Component,
    OnInit,
} from "@angular/core";

// Interfaces
import { IProtocolResponse } from "interfaces/protocol.interface";
import { ISspTemplatesRestrictView } from "interfaces/self-service-portal/ssp.interface";

// Services
import { NotificationService } from "modules/shared/services/provider/notification.service";
import { SettingsSspActionService } from "./settings-ssp-action-service";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

@Component({
    selector: "settings-ssp",
    templateUrl: "./settings-ssp.component.html",
})
export class SettingsSspComponent implements OnInit {

    loading = false;
    saving = false;
    restrictViewSettingValue: boolean;

    constructor(
        private translatePipe: TranslatePipe,
        private settingsSspAction: SettingsSspActionService,
        private notificationService: NotificationService,
    ) { }

    ngOnInit() {
        this.getRestrictViewSetting();
    }

    getRestrictViewSetting() {
        this.loading = true;
        this.settingsSspAction.getSspRestrictViewSetting()
            .subscribe((response: IProtocolResponse<ISspTemplatesRestrictView>): void => {
                if (response) {
                    this.restrictViewSettingValue = response.data.restrictProjectOwnerView;
                } else {
                    this.restrictViewSettingValue = false;
                }
                this.loading = false;
            });
    }

    saveRestrictViewSetting(restrictViewSettingValue: boolean) {
        this.saving = true;
        this.settingsSspAction.saveSspRestrictViewSettings({restrictProjectOwnerView: restrictViewSettingValue})
        .subscribe((response: IProtocolResponse<boolean>) => {
            if (response.result) {
                this.notificationService.alertSuccess(
                    this.translatePipe.transform("Success"),
                    this.translatePipe.transform("TemplateSettingsSuccessMessage"),
                );
            }
            this.saving = false;
            return;
        });
    }
}
