// Angular
import { Injectable } from "@angular/core";

// 3rd party
import {
    Observable,
    from,
} from "rxjs";

// Services
import { ProtocolService } from "modules/core/services/protocol.service";

// Interfaces
import {
    IProtocolConfig,
    IProtocolMessages,
    IProtocolResponse,
} from "interfaces/protocol.interface";
import { TranslatePipe } from "pipes/translate.pipe";
import { ISspTemplatesRestrictView } from "interfaces/self-service-portal/ssp.interface";

@Injectable()
export class SettingsSspApiService {

    constructor(
        private protocolService: ProtocolService,
        private translatePipe: TranslatePipe,
    ) { }

    getSspRestrictViewSetting(): Observable<IProtocolResponse<ISspTemplatesRestrictView>> {
        const config: IProtocolConfig = this.protocolService.customConfig("GET", `/assessment-v2/v2/assessments/setting`);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("CouldNotRetrieveSetting") } };
        return from(this.protocolService.http(config, messages));
    }

    saveSspRestrictViewSettings(restrictViewSettingValue: ISspTemplatesRestrictView): Observable<IProtocolResponse<boolean>> {
        const config: IProtocolConfig = this.protocolService.customConfig("POST", `/assessment-v2/v2/assessments/setting/ssp`, null, restrictViewSettingValue);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("CouldNotSaveTemplateAccessSettings") } };
        return from(this.protocolService.http(config, messages));
    }
}
