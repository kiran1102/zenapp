// Angular
import { Injectable } from "@angular/core";

// 3rd party
import { Observable } from "rxjs";

// Interfaces
import { IProtocolResponse } from "interfaces/protocol.interface";

// Services
import { SettingsSspApiService } from "./settings-ssp-api.service";
import { ISspTemplatesRestrictView } from "interfaces/self-service-portal/ssp.interface";

@Injectable()
export class SettingsSspActionService {

    constructor(
        private settingsSspApiService: SettingsSspApiService,
    ) {}

    getSspRestrictViewSetting(): Observable<IProtocolResponse<ISspTemplatesRestrictView>> {
        return this.settingsSspApiService.getSspRestrictViewSetting();
    }

    saveSspRestrictViewSettings(restrictViewSettingValue: ISspTemplatesRestrictView): Observable<IProtocolResponse<boolean>> {
        return this.settingsSspApiService.saveSspRestrictViewSettings(restrictViewSettingValue);
    }
}
