// Modules
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { VitreusModule } from "@onetrust/vitreus";
import { OTPipesModule } from "modules/pipes/pipes.module";

// Components
import { SettingsSspComponent } from "./settings-ssp.component";

// Services
import { SettingsSspActionService } from "./settings-ssp-action-service";
import { SettingsSspApiService } from "./settings-ssp-api.service";

@NgModule({
    imports: [
        CommonModule,
        VitreusModule,
        OTPipesModule,
    ],
    exports: [
        SettingsSspComponent,
    ],
    entryComponents: [
        SettingsSspComponent,
    ],
    declarations: [
        SettingsSspComponent,
    ],
    providers: [
        SettingsSspActionService,
        SettingsSspApiService,
    ],
})
export class SettingsSspModule { }
