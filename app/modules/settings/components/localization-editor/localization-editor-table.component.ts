// 3rd Party
import { Component,
    EventEmitter,
    Input,
    Output,
} from "@angular/core";
import { findIndex } from "lodash";

// Interfaces
import { IBasicTableCell } from "interfaces/tables.interface";
import { ITableColumnConfig } from "interfaces/table-config.interface";
import { ITranslation, ITranslationCell } from "interfaces/localization.interface";
import { IDropdownOption } from "interfaces/one-dropdown.interface";
import { IStringMap } from "interfaces/generic.interface";

// Constants & Enums
import { LocalizationEditorActions } from "constants/localization-editor.constant";
import { TranslationModules } from "constants/translation.constant";
import { TableColumnTypes } from "enums/data-table.enum";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

@Component({
    selector: "localization-editor-table",
    templateUrl: "./localization-editor-table.component.html",
})

export class LocalizationEditorTableComponent {

    @Input() rows: ITranslationCell[];
    @Input() columns: ITableColumnConfig[];
    @Input() permissions: IStringMap<string>;
    @Input() tabId: string;
    @Output() handleActions: EventEmitter<IBasicTableCell> = new EventEmitter<IBasicTableCell>();

    rowBordered = true;
    responsive = true;
    columnTypes = TableColumnTypes;
    private menuClass: string;
    private tabIds: IStringMap<string> = TranslationModules;

    constructor(
        private translatePipe: TranslatePipe,
    ) {}

    columnTrackBy(column: ITableColumnConfig): string {
        return column.columnKey;
    }

    cellTrackBy(cell: IBasicTableCell): string {
        return cell.key;
    }

    viewDetails(row: ITranslation): void {
        if (this.permissions.edit) {
            this.handleActions.emit({type: LocalizationEditorActions.OPEN_EDIT_MODAL, payload: row.key});
        }
    }

    getActions(row: ITranslation): () => IDropdownOption[] {
        const rowActions: IDropdownOption[] = [
            {
                text: this.translatePipe.transform("Edit"),
                action: (): void => {
                    this.handleActions.emit(
                        { type: LocalizationEditorActions.OPEN_EDIT_MODAL, payload: row.key },
                    );
                },
                isDisabled: !this.permissions.edit,
            },
        ];
        if (this.tabId === this.tabIds.System) {
            rowActions.push({
                text: this.translatePipe.transform("RestoreDefaultBtn"),
                action: (): void => {
                    this.handleActions.emit(
                        { type: LocalizationEditorActions.RESTORE_SINGLE_VALUE_MODAL, payload: row.key },
                    );
                },
                isDisabled: !this.permissions.edit,
            });
        }
        return (): IDropdownOption[] => rowActions;
    }

    private setMenuClass(row: ITranslation): void | undefined {
        this.menuClass = "actions-table__context-list";
        if (this.rows.length <= 10) return;
        const halfwayPoint: number = (this.rows.length - 1) / 2;
        const rowIndex: number = findIndex(this.rows, (item: ITranslation): boolean => row.key === item.key);
        if (rowIndex > halfwayPoint) this.menuClass += " actions-table__context-list--above";
    }
}
