// 3rd Party
import {
    Component,
    Inject,
    OnInit,
    OnDestroy,
} from "@angular/core";
import { StateService } from "@uirouter/core";
import {
    each,
    map,
    isUndefined,
    replace,
    findIndex,
} from "lodash";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

// Redux
import { Unsubscribe } from "redux";
import {
    getTranslationsPage,
    getTranslationsPageFetchStatus,
} from "settingsReducers/localization.reducer";
import { StoreToken } from "tokens/redux-store.token";

// Services
import { LocalizationActionService } from "settingsServices/actions/localization-action.service";
import { Permissions } from "modules/shared/services/helper/permissions.service";

// Interfaces
import { IProtocolResponse } from "interfaces/protocol.interface";
import { IStore, IStoreState } from "interfaces/redux.interface";
import { IStringMap } from "interfaces/generic.interface";
import { IAction } from "interfaces/redux.interface";
import { ITableColumnConfig } from "interfaces/table-config.interface";
import {
    ITranslationsTable,
    ITranslationsPage,
    ITranslation,
    ITranslationCell,
    ILocalizationParams,
    ILocalizationEditModalData,
} from "interfaces/localization.interface";
import { ITabsNav } from "interfaces/one-tabs-nav.interface";

// Constants + Enums
import { LocalizationEditorActions } from "constants/localization-editor.constant";
import { TranslationModules } from "constants/translation.constant";
import { TableColumnTypes } from "enums/data-table.enum";

// Vitreus
import {
    OtModalService,
    ConfirmModalType,
} from "@onetrust/vitreus";

// Components
import { OTNotificationModalComponent } from "sharedModules/components/ot-notification-modal/ot-notification-modal.component";
import { LocalizationEditModalV2 } from "settingsComponents/localization-edit-modal-v2/localization-edit-modal-v2.component";

// RxJs
import { take } from "rxjs/operators";

@Component({
    selector: "localization-editor",
    templateUrl: "./localization-editor.component.html",
})

export class LocalizationEditorComponent implements OnInit, OnDestroy {

    LocalizationEditorActions = LocalizationEditorActions;
    isLoading = true;
    termsTable: ITranslationsTable;
    tabIds: IStringMap<string> = TranslationModules;
    tabs: ITabsNav[];
    currentTab: string;
    searchTerm: string;
    viewPermissions: IStringMap<boolean> = {
        edit: this.permissions.canShow("LocalizationEditorEdit"),
        search: this.permissions.canShow("LocalizationEditorSearch"),
    };
    private unsub: Unsubscribe;
    private termsPage: ITranslationsPage;
    private params: ILocalizationParams = {
        page: null,
        size: null,
        searchKey: null,
    };

    constructor(
        @Inject(StoreToken) private store: IStore,
        private stateService: StateService,
        private LocalizationActions: LocalizationActionService,
        private permissions: Permissions,
        private translatePipe: TranslatePipe,
        private otModalService: OtModalService,
    ) {}

    ngOnInit() {
        this.unsub = this.store.subscribe(() =>
            this.componentWillReceiveState(),
        );
        this.params = this.LocalizationActions.fetchQueryParams();
        this.searchTerm = this.params.searchKey;
        this.currentTab = this.stateService.params.module || this.tabIds.System;
        this.LocalizationActions.fetchTranslationsPage(this.params, this.currentTab);
        this.tabs = this.getTabs();
        this.setTermsTable(this.termsPage);
    }

    ngOnDestroy() {
        this.unsub();
    }

    handleActions(action: IAction): ng.IPromise<void> {
        switch (action.type) {
            case LocalizationEditorActions.GET_PAGE: {
                const getPageParam: ILocalizationParams = {
                    page: isUndefined(action.payload.page) ? this.params.page : action.payload.page,
                    size: isUndefined(action.payload.size) ? this.params.size : action.payload.size,
                    searchKey: this.params.searchKey,
                };
                return this.fetchTranslations(getPageParam);
            }
            case LocalizationEditorActions.SEARCH: {
                this.searchTerm = action.payload.model;
                const searchPageParam: ILocalizationParams = {
                    page: 0,
                    size: this.params.size,
                    searchKey: action.payload.model,
                };
                return this.fetchTranslations(searchPageParam);
            }
            case LocalizationEditorActions.SORT:
                return;
            case LocalizationEditorActions.OPEN_EDIT_MODAL:
                if (!action.payload) return;
                this.openEditModal(action.payload);
                return;
            case LocalizationEditorActions.RESTORE_SINGLE_VALUE_MODAL:
                if (!action.payload) return;
                this.openRestoreSingleValueModal(action.payload, false);
                return;
            case LocalizationEditorActions.RESTORE_ALL_VALUE_MODAL:
                if (!action.payload) return;
                this.openRestoreSingleValueModal(action.payload, true);
                return;
            case LocalizationEditorActions.GET_MODULE: {
                const getPageParam: ILocalizationParams = {
                    page: 0,
                    size: this.params.size,
                    searchKey: "",
                    module: action.payload,
                };
                return this.fetchTranslations(getPageParam);
            }
            default:
                return;
        }
    }

    openEditModal(termKey: string) {
        const termIndex: number = findIndex(this.termsTable.rows, (term: ITranslation): boolean => term.key === termKey);
        const termSinglePageIndex: number = (this.params.page * this.params.size) + termIndex;
        if (termIndex < 0) return;
        const modalData: ILocalizationEditModalData = {
            termKey,
            termIndex: termSinglePageIndex,
            lastTermIndex: this.termsPage.totalElements - 1,
            searchKey: this.params.searchKey,
            tabId: this.currentTab,
        };
        this.otModalService
            .create(
                LocalizationEditModalV2,
                {
                    isComponent: true,
                },
                modalData,
            ).pipe(
                take(1),
            ).subscribe(() => {
                this.fetchTranslations(this.params);
            });
    }

    openRestoreSingleValueModal(termKey: string, restoreAllValues: boolean) {
        this.otModalService.confirm({
            type: ConfirmModalType.WARNING,
            translations: {
                title: restoreAllValues ? this.translatePipe.transform("RestoreAll") : this.translatePipe.transform("RestoreItem"),
                desc: restoreAllValues ? this.translatePipe.transform("TryingToRestAllValues") : `${this.translatePipe.transform("TryingToRestoreValuesFor")} ${termKey}`,
                confirm: this.translatePipe.transform("ConfirmationTextForRestoringValues"),
                cancel: this.translatePipe.transform("Cancel"),
                submit: this.translatePipe.transform("RestoreBtn"),
            },
        }).pipe(take(1))
        .subscribe((data: boolean): boolean => {
            if (!data) return;
            if (restoreAllValues) {
                this.LocalizationActions.restoreAllTranslations().then((response: IProtocolResponse<void>) => {
                    if (response.result) {
                        this.fetchTranslations(this.params);
                    }
                });
            } else {
                this.LocalizationActions.restoreSingleTranslations(termKey).then((response: IProtocolResponse<void>) => {
                    if (response.result) {
                        this.fetchTranslations(this.params);
                    }
                });
            }
        });
    }

    exportLocalization() {
        this.LocalizationActions.getLocalizationExportFile().subscribe(
            (response: IProtocolResponse<any>) => {
                if (response.result) {
                    const taskNotificationModal = this.otModalService
                    .create(
                        OTNotificationModalComponent,
                        { isComponent: true },
                        {
                            modalTitle: "LocalizationExport",
                            confirmationText: this.translatePipe.transform("LocalizationExportFileReadyToDownload"),
                        },
                    )
                    .subscribe(() => {
                        taskNotificationModal.unsubscribe();
                    });
                }
            });
    }

    private fetchTranslations(params: ILocalizationParams): ng.IPromise<void> {
        return this.LocalizationActions.updateQueryParamsAndFetchTranslations(params);
    }

    private toBasicTableRows(rows: ITranslation[], columns: ITableColumnConfig[]): ITranslationCell[] {
        return map(rows, (row: ITranslation): ITranslationCell => {
            const cells: any[] = [];
            each(columns, (column: ITableColumnConfig) => {
                cells.push({
                    key: column.columnKey,
                    displayValue: this.params.searchKey
                        ? this.highlightText(this.params.searchKey, row[column.columnKey] || "")
                        : row[column.columnKey] || "",
                });
            });
            return { ...row, cells };
        });
    }

    private highlightText(searchTerm: string, text: string): string {
        return replace(
            text,
            new RegExp(searchTerm, "gi"),
            (subString: string): string => `<span class="highlight">${subString}</span>`,
        );
    }

    private getTabs(): ITabsNav[] {
        return [
            {
                id: this.tabIds.System,
                text: this.translatePipe.transform("System"),
                action: (option: ITabsNav) => {
                    this.goToTab(option.id);
                },
            },
            {
                id: this.tabIds.Inventory,
                text: this.translatePipe.transform("Custom"),
                action: (option: ITabsNav) => {
                    this.goToTab(option.id);
                },
            },
        ];
    }

    private goToTab(tabId: string) {
        this.currentTab = tabId;
        this.handleActions({ type: this.LocalizationEditorActions.GET_MODULE, payload: tabId });
    }

    private setTermsTable(metaData: ITranslationsPage) {
        const columns = this.getColumns();
        const rows = this.toBasicTableRows(metaData ? metaData.content : [], columns);
        this.termsTable = metaData && metaData.totalElements ? { columns, rows, metaData } : null;
    }

    private componentWillReceiveState() {
        const storeState: IStoreState = this.store.getState();
        this.params = this.LocalizationActions.fetchQueryParams();
        this.isLoading = getTranslationsPageFetchStatus(storeState);
        this.termsPage = getTranslationsPage(storeState);
        this.setTermsTable(this.termsPage);
    }

    private getColumns(): ITableColumnConfig[] {
        const columns: ITableColumnConfig[] = [{
            columnKey: "value",
            name: this.translatePipe.transform("Term"),
            type: TableColumnTypes.Text,
        }];
        if (this.currentTab === TranslationModules.System) {
            columns.unshift({
                columnKey: "key",
                name: this.translatePipe.transform("Key"),
                type: TableColumnTypes.Link,
            });
        } else {
            columns.unshift({
                columnKey: "defaultValue",
                name: this.translatePipe.transform("Key"),
                type: TableColumnTypes.Link,
            });
        }
        return columns;
    }

}
