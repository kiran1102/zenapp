// Angular
import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";

// rxjs
import { Observable, from } from "rxjs";
import { catchError, map, tap } from "rxjs/operators";
import { of } from "rxjs";

// Interfaces
import { ITableData, IGroupFilter } from "interfaces/smtp-settings.interface";

// Vitreus
import { ToastService } from "@onetrust/vitreus";

// Service
import { TranslateService } from "@ngx-translate/core";

// Interfaces
import {
    IResetEmailData,
    IBrandingLanguageDataContent,
    ILanguageActivatorData,
    IBindToTemplateScopeResponse,
} from "interfaces/email-template.interface";

@Injectable()
export class SmtpConnectionsTemplateListService {

    translations = {};
    keys = [
        "Error",
        "Success",
        "ErrorRetrievingResponses",
        "CustomEmailTemplateOverrideSuccessMessage",
        "AllLanguages",
        "EditLanguageSuccess",
        "EditLanguageFailure",
    ];

    constructor(
        private translateService: TranslateService,
        private toastService: ToastService,
        private http: HttpClient,
    ) {
        this.translateService
            .stream(this.keys)
            .subscribe((values) => (this.translations = values));
    }

    showSuccessMsg(key: string) {
        this.toastService.success(this.translations["Success"],
            this.translations[key],
        );
    }
    showErrorMessage(key: string) {
        this.toastService.error(this.translations["Error"],
            this.translations[key],
        );
    }

    getListofAllTemplates(page: number, size: number): Observable<ITableData> {
        return this.http.get<ITableData>(`/api/notification/v1/messageTemplates/all?page=${page}&size=${size}`);
    }

    getDetailsofParentTemplates(templateId: number, languageId: number): Promise<IBindToTemplateScopeResponse | boolean> {
        return this.http.get<IBindToTemplateScopeResponse>(`/api/notification/v1/messageTemplates/messagecontent/${templateId}/language/${languageId}`)
        .pipe(
            tap((res: IBindToTemplateScopeResponse) => {
                return res;
            }),
            catchError((err) => {
                this.showErrorMessage("ErrorRetrievingResponses");
                return of(false);
            }),
        )
        .toPromise();
    }

    resetEmailTemplate(templateId: number): Promise<boolean> {
        return this.http.post(`/api/notification/v1/messageTemplates/${templateId}/restore`, null)
        .pipe(
            map(() => {
                return true;
            }),
            catchError((err) => {
                this.showErrorMessage("ErrorRetrievingResponses");
                return of(false);
            }),
        )
        .toPromise();
    }

    resetAllEmailTemplate(templateId: number): Promise<boolean> {
        const params = { typeId: templateId.toString() };
        return this.http.put(`/api/notification/v1/messageTemplates/restore-all`, null, { params })
        .pipe(
            map(() => {
                return true;
            }),
            catchError((err) => {
                this.showErrorMessage("ErrorRetrievingResponses");
                return of(false);
            }),
        )
        .toPromise();
    }

    createCopyofTemplate(templateId: number): Promise<boolean> {
        return this.http.post(`/api/notification/v1/messageTemplates/messagecontent/${templateId}/override`, null)
        .pipe(
            map(() => {
                this.showSuccessMsg("CustomEmailTemplateOverrideSuccessMessage");
                return true;
            }),
            catchError((err) => {
                this.showErrorMessage("ErrorRetrievingResponses");
                return of(false);
            }),
        )
        .toPromise();
    }

    disableEmailNotifications(messageTypeId: number, disableEmailValue: boolean): Observable<null> {
        return this.http.put<null>(`/api/notification/v1/messageTemplates/${messageTypeId}/disable/${disableEmailValue}`, null);
    }

    getEmailLanguages(): Promise<ILanguageActivatorData[] | boolean> {
        return this.http.get(`/api/globalization/v1/languages/tenant`)
        .pipe(
            tap((res: ILanguageActivatorData[]) => {
                return res;
            }),
            catchError((err) => {
                this.showErrorMessage("ErrorRetrievingResponses");
                return of(false);
            }),
        )
        .toPromise();
    }

    getGroupFilterList(): Observable<IGroupFilter[]> {
        return this.http.get<IGroupFilter[]>(`/api/notification/v1/message-type-groups`);
    }

    getGroupFilterListFiltered(page: number, size: number, values: number[]): Observable<ITableData> {
        return this.http.get<ITableData>(`/api/notification/v1/messageTemplates/all?page=${page}&size=${size}&groupIds=${values}`);
    }

    updateEmailLanguages(selectedEmailData: ILanguageActivatorData[]): Promise<boolean> {
        return this.http.put(`/settings/languages/`, selectedEmailData)
        .pipe(
            map(() => {
                this.showSuccessMsg("EditLanguageSuccess");
                return true;
            }),
            catchError((err) => {
                this.showErrorMessage("EditLanguageFailure");
                return of(false);
            }),
        )
        .toPromise();
    }

}
