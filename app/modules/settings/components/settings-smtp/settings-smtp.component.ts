// Rxjs
import { Subject } from "rxjs";
import {
    startWith,
    takeUntil,
} from "rxjs/operators";

// Angular
import {
    Component,
    Inject,
} from "@angular/core";
import {
    FormGroup,
    FormBuilder,
    Validators,
    FormControl,
} from "@angular/forms";

// External Libraries
import {
    Transition,
    StateDeclaration,
    TransitionService,
} from "@uirouter/core";

// Interfaces
import {
    IStore,
    IStoreState,
} from "interfaces/redux.interface";
import { ISmtpSetting } from "interfaces/smtp-settings.interface";

// Tokens
import { StoreToken } from "tokens/redux-store.token";

// Redux
import {
    getTempSmtpSetting,
    isSmtpConnectionEnabled,
    isTogglingSmtpConnection,
    hasPendingSmtpSettingChange,
    getSmtpSetting,
    SettingActions,
} from "oneRedux/reducers/setting.reducer";
import observableFromStore from "oneRedux/utils/observable-from-store";

// Services
import { SettingSmtpActionService } from "settingsServices/actions/setting-smtp-action.service";
import {
    OtModalService,
    ConfirmModalType,
} from "@onetrust/vitreus";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

// Constants
import {
    NON_NUMBER,
    NEGATIVE_NUMBER,
    NEGATIVE_NUMBER_PERIOD,
} from "constants/regex.constant";

@Component({
    selector: "smtp-setting",
    templateUrl: "./settings-smtp.component.html",
    host: {
        class: "flex-full-height height-100",
    },
})
export class SettingsSmtpComponent {

    loading = false;
    tempSmtpSetting: ISmtpSetting;
    smtpEnabled = false;
    installInProgress = false;
    testingInProgress = false;
    toggleSmtpInProgress = false;
    componentDestroy = new Subject();
    hasInputWithError = false;
    firstPasswordEdit = true;
    unRegistrationHook: any;
    isConnected: boolean = null;
    emailSettingsManageForm: FormGroup;
    errorMessage: string;
    private destroy$ = new Subject();

    constructor(
        @Inject(StoreToken) readonly store: IStore,
        public translatePipe: TranslatePipe,
        private SettingSmtpAction: SettingSmtpActionService,
        private $transitions: TransitionService,
        private formBuilder: FormBuilder,
        private otModalService: OtModalService,
    ) {
        this.unRegistrationHook = this.$transitions.onExit(
            { from: "zen.app.pia.module.settings.smtp" },
            (transition: Transition, state: StateDeclaration) => {
                if (state.name === "zen.app.pia.module.settings.smtp" && hasPendingSmtpSettingChange(store.getState())) {
                    this.otModalService.confirm({
                        type: ConfirmModalType.WARNING,
                        translations: {
                            title: this.translatePipe.transform("UnsavedChangesTitle"),
                            desc: this.translatePipe.transform("UnsavedChangesDescription"),
                            confirm: this.translatePipe.transform("AreYouSureContinue"),
                            submit: this.translatePipe.transform("DiscardChanges"),
                            cancel: this.translatePipe.transform("Cancel"),
                        },
                        }).pipe(
                            takeUntil(this.destroy$),
                        ).subscribe(
                            (response: boolean) => {
                                if (response) {
                                    this.store.dispatch({ type: SettingActions.SET_SMTP_SETTING_PENDING_CHANGES_TO_FALSE });
                                    transition.router.stateService.go(transition.to(), transition.params(), transition.options());
                                }
                            },
                        );
                    return false;
                }
                return true;
            },
        );
    }

    ngOnInit(): void {
        this.loading = true;
        this.SettingSmtpAction.fetchSmtpSetting().then(
            (success: boolean): void => {
                if (!success || !getSmtpSetting(this.store.getState())) {
                    this.SettingSmtpAction.addBlankSmtpSetting();
                }
                observableFromStore(this.store).pipe(
                    startWith(this.store.getState()),
                    takeUntil(this.componentDestroy),
                ).subscribe(
                        (state: IStoreState): void =>
                            this.componentWillReceiveState(),
                    );
                this.loading = false;
                this.initForm();
            },
        );
    }

    getError(field: string): boolean {
        const input = this.emailSettingsManageForm.get(field);
        if (!input || !input.errors) {
            return false;
        } else if (input.dirty && input.touched && input.errors.required) {
            this.errorMessage = this.translatePipe.transform("ThisFieldCannotBeEmpty");
            return true;
        } else if (field === "smtpPort") {
            if (
                input.dirty &&
                input.touched &&
                ((input.errors.validateSmtpPort && !input.errors.validateSmtpPort.valid) ||
                    input.errors.min
                    )
            ) {
                this.errorMessage = this.translatePipe.transform("PleaseEnterAValidNumberGreaterThan",
                    { value: 1 },
                );
                return true;
            }
        } else if (field === "smtpSenderEmail") {
            if (input.dirty && input.touched && input.errors.email) {
                this.errorMessage = this.translatePipe.transform("PleaseProvideAValidEmailAddress");
                return true;
            }
        }
        return false;
    }

    onDestroy() {
        this.componentDestroy.next(true);
        this.unRegistrationHook();
    }

    toggleSmtpConnection() {
        this.SettingSmtpAction.toggleSmtpConnection();
    }

    handleInputChange(payload: string, prop: string): void | undefined {
        if (
            this.firstPasswordEdit &&
            this.tempSmtpSetting.smtpPassword &&
            prop === "smtpPassword"
        ) {
            this.SettingSmtpAction.updateTempSmtpSetting({
                smtpPassword: payload.substr(this.tempSmtpSetting.smtpPassword.length),
            });
            this.firstPasswordEdit = false;
            return;
        }
        this.SettingSmtpAction.updateTempSmtpSetting({ [prop]: payload });
    }

    installSmtpConnection() {
        this.installInProgress = true;
        this.SettingSmtpAction.installSmtpConnection().then(
            (success: boolean) => {
                this.installInProgress = false;
            },
        );
    }

    validateSmtpConnection() {
        this.installInProgress = true;
        this.SettingSmtpAction.updateTempSmtpSetting({ validate: true });
        this.SettingSmtpAction.installSmtpConnection().then(
            (success: boolean) => {
                this.isConnected = Boolean(success);
                if (!success) {
                    this.isConnected = false;
                }
                this.installInProgress = false;
                this.SettingSmtpAction.updateTempSmtpSetting({
                    validate: false,
                });
            },
        );
    }

    enableSslTls() {
        this.SettingSmtpAction.updateTempSmtpSetting({smtpSslTls: !this.tempSmtpSetting.smtpSslTls});
    }

    validateSmtpPort(formControl: FormControl) {
        return NON_NUMBER.test(formControl.value) ||
            NEGATIVE_NUMBER.test(formControl.value) ||
            NEGATIVE_NUMBER_PERIOD.test(formControl.value)
            ? { validateSmtpPort: { valid: false } }
            : null;
    }

    private initForm() {
        this.emailSettingsManageForm = this.formBuilder.group({
            smtpHostname: new FormControl(
                this.tempSmtpSetting ? this.tempSmtpSetting.smtpHostname : "",
                [Validators.required],
            ),
            smtpUsername: new FormControl(
                this.tempSmtpSetting ? this.tempSmtpSetting.smtpUsername : "",
                [Validators.required],
            ),
            smtpPassword: new FormControl(
                this.tempSmtpSetting ? this.tempSmtpSetting.smtpPassword : "",
                [Validators.required],
            ),
            smtpSenderName: new FormControl(
                this.tempSmtpSetting ? this.tempSmtpSetting.smtpSenderName : "",
                [Validators.required],
            ),
            smtpPort: new FormControl(
                this.tempSmtpSetting ? this.tempSmtpSetting.smtpPort : "",
                [Validators.required, this.validateSmtpPort, Validators.min(1)],
            ),
            smtpSenderEmail: new FormControl(
                this.tempSmtpSetting ? this.tempSmtpSetting.smtpSenderEmail : "",
                [Validators.email, Validators.required],
            ),
        });
    }

    private componentWillReceiveState() {
        this.tempSmtpSetting = getTempSmtpSetting(this.store.getState());
        this.smtpEnabled = isSmtpConnectionEnabled(this.store.getState());
        this.toggleSmtpInProgress = isTogglingSmtpConnection(
            this.store.getState(),
        );
    }
}
