// Rxjs
import { Subject } from "rxjs";
import {
    takeUntil,
    filter,
} from "rxjs/operators";

// Core
import {
    Transition,
    StateDeclaration,
    TransitionService,
} from "@uirouter/core";
import {
    Component,
    Inject,
} from "@angular/core";

// Redux
import {
    hasPendingConsentSettingsChange,
    getConsentSettings,
    SettingActions,
} from "oneRedux/reducers/setting.reducer";
import { StoreToken } from "tokens/redux-store.token";

// Interfaces
import { IStore, IStoreState } from "interfaces/redux.interface";
import { IConsentSettings } from "interfaces/consent-settings.interface";

// Services
import { SettingConsentActionService } from "settingsServices/actions/setting-consent-action.service";
import { ReduxBaseComponent } from "sharedModules/components/redux-base/redux-base.component";
import {
    OtModalService,
    ConfirmModalType,
} from "@onetrust/vitreus";
import { Permissions } from "modules/shared/services/helper/permissions.service";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

@Component({
    selector: "settings-consent",
    templateUrl: "./settings-consent.component.html",
})

export class SettingsConsentComponent extends ReduxBaseComponent {
    loading = false;
    actionInProgress = false;
    hasInputWithError = false;
    consentSetting: IConsentSettings;
    showNoConsentTransactionToggle = this.permissions.canShow("ConsentSettingNoConsentTransaction");
    consentAcknowledgementEmailToggle = this.permissions.canShow("ConsentAcknowledgementEmail");

    error: any = {};
    componentDestroy = new Subject();
    unRegistrationHook: any;

    private destroy$ = new Subject();

    constructor(
        @Inject(StoreToken) private store: IStore,
        private settingConsentActionService: SettingConsentActionService,
        private $transitions: TransitionService,
        private otModalService: OtModalService,
        private permissions: Permissions,
        private translatePipe: TranslatePipe,
    ) {
        super(store);
        this.unRegistrationHook = this.$transitions.onExit(
            { from: "zen.app.pia.module.settings.consent" },
            (transition: Transition, state: StateDeclaration) => {
                if (state.name === "zen.app.pia.module.settings.consent" && hasPendingConsentSettingsChange(store.getState())) {
                    this.otModalService.confirm({
                        type: ConfirmModalType.WARNING,
                        translations: {
                            title: this.translatePipe.transform("UnsavedChangesTitle"),
                            desc: this.translatePipe.transform("UnsavedChangesDescription"),
                            confirm: this.translatePipe.transform("AreYouSureContinue"),
                            submit: this.translatePipe.transform("DiscardChanges"),
                            cancel: this.translatePipe.transform("Cancel"),
                        },
                        }).pipe(
                            takeUntil(this.destroy$),
                        ).subscribe(
                            (response: boolean) => {
                                if (response) {
                                    this.store.dispatch({ type: SettingActions.SAVED_CONSENT_SETTINGS });
                                    transition.router.stateService.go(transition.to(), transition.params(), transition.options());
                                }
                                return response;
                            },
                        );
                    return false;
                }
                return true;
            },
        );
    }

    ngOnInit() {
        super.ngOnInit();
        this.loading = true;
        this.settingConsentActionService.fetchConsentSettings().then((success: boolean) => {
            if (!success || !getConsentSettings(this.store.getState())) {
                const defaultSettings: IConsentSettings = {
                    magicLinkEnabled: false,
                    magicLinkTimeSpan: 12,
                };
                this.settingConsentActionService.addConsentSettings(defaultSettings);
            }
            this.loading = false;
        });
    }

    ngOnDestroy() {
        this.componentDestroy.next(true);
        this.unRegistrationHook();
    }

    toggleSetting(prop: keyof IConsentSettings) {
        this.consentSetting[prop] = !this.consentSetting[prop];
        this.settingConsentActionService.updateConsentSettings({ [prop]: this.consentSetting[prop] });
    }

    handleTimeSpanChange(value: number | null | undefined) {
        const isValid: boolean = value < 13 && value > 0;
        this.error.magicLinkTimeSpan = !isValid;
        this.hasInputWithError = !isValid;
        this.settingConsentActionService.updateConsentSettings({ magicLinkTimeSpan: value });
    }

    openSaveSettingsModal() {
        this.otModalService.confirm({
            type: ConfirmModalType.WARNING,
            translations: {
                title: this.translatePipe.transform("ConfirmSaveConsentSettingsTitle"),
                desc: this.translatePipe.transform("ConfirmSaveConsentSettings"),
                confirm: this.translatePipe.transform("AreYouSureContinue"),
                submit: this.translatePipe.transform("Save"),
                cancel: this.translatePipe.transform("Cancel"),
            },
        }).pipe(
            takeUntil(this.destroy$),
            filter(Boolean),
        ).subscribe(() =>  this.saveSettings());
    }

    openRecreateMagicLinksModal() {
        this.otModalService.confirm({
            type: ConfirmModalType.WARNING,
            translations: {
                title: this.translatePipe.transform("ConfirmRecreateMagicLinksTitle"),
                desc: this.translatePipe.transform("ConfirmRecreateMagicLinks"),
                confirm: this.translatePipe.transform("AreYouSureContinue"),
                submit: this.translatePipe.transform("Recreate"),
                cancel: this.translatePipe.transform("Cancel"),
            },
        }).pipe(
            takeUntil(this.destroy$),
            filter(Boolean),
        ).subscribe(() =>  this.recreateMagicLinks());
    }

    openDeleteMagicLinksModal() {
        this.otModalService.confirm({
            type: ConfirmModalType.DELETE,
            translations: {
                title: this.translatePipe.transform("ConfirmDeleteMagicLinksTitle"),
                desc: this.translatePipe.transform("ConfirmDeleteMagicLinks"),
                confirm: this.translatePipe.transform("AreYouSureContinue"),
                submit: this.translatePipe.transform("Delete"),
                cancel: this.translatePipe.transform("Cancel"),
            },
        }).pipe(
            takeUntil(this.destroy$),
            filter(Boolean),
        ).subscribe(() =>  this.deleteMagicLinks());
    }

    protected onStateUpdate(state: IStoreState) {
        this.consentSetting = getConsentSettings(state);
    }

    private saveSettings(): ng.IPromise<boolean> {
        this.actionInProgress = true;
        return this.settingConsentActionService.saveConsentSettings().then((result: boolean): boolean => {
            this.actionInProgress = false;
            return result;
        });
    }

    private recreateMagicLinks(): ng.IPromise<boolean> {
        this.actionInProgress = true;
        return this.settingConsentActionService.recreateTokens().then((result: boolean): boolean => {
            this.actionInProgress = false;
            return result;
        });
    }

    private deleteMagicLinks(): ng.IPromise<boolean> {
        this.actionInProgress = true;
        return this.settingConsentActionService.deleteTokens().then((result: boolean): boolean => {
            this.actionInProgress = false;
            return result;
        });
    }
}
