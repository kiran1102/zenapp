import {
    Component,
    Inject,
    OnInit,
    Input,
} from "@angular/core";
import { StateService } from "@uirouter/core";
import {
    each,
    map,
    forEach,
} from "lodash";

// Services
import { UserActionService } from "oneServices/actions/user-action.service";
import { SmtpConnectionsTemplateListService } from "settingsComponents/smtp-connections-list/smtp-connections-template-list.service";
import { TranslateService } from "@ngx-translate/core";

// Interfaces
import { IStringMap } from "interfaces/generic.interface";
import { ITabsNav } from "interfaces/one-tabs-nav.interface";
import { IStore, IStoreState } from "interfaces/redux.interface";
import {
    ITableData,
    ITableDataContent,
    ITableColumnList,
    IGroupFilter,
} from "interfaces/smtp-settings.interface";

// Enums / Constants
import { TableColumnTypes } from "enums/data-table.enum";
import { LanguageIds } from "enums/general.enum";

// Redux
import { StoreToken } from "tokens/redux-store.token";
import { getOrgNameById } from "oneRedux/reducers/org.reducer";
import {
    getUserById,
    getUserDisplayName,
} from "oneRedux/reducers/user.reducer";

// rxjs
import { take } from "rxjs/operators";

// Vitreus
import { ToastService } from "@onetrust/vitreus";

// Pipe
import { TranslatePipe } from "pipes/translate.pipe";

@Component({
    selector: "email-template-list",
    templateUrl: "./email-template-list.component.html",
})

export class EmailTemplateListComponent implements OnInit {

    @Input() tableData: ITableData;

    isTemplateLoaded = false;
    currentTab = "";
    tabOptions: ITabsNav[] = [];
    tabIds: IStringMap<string> = {
        templates: "Templates",
        branding: "Branding",
    };
    tableColumnTypes = TableColumnTypes;
    columns: ITableColumnList[];
    pageSize = 10;
    filterIsOpen: boolean;
    groupFilterOptions: IGroupFilter[];
    selectedOptions: IGroupFilter[] = [];
    filteredValues: number[];
    filterActive: boolean;
    translations = {};
    keys = [
        "Error",
        "Success",
        "Templates",
        "Branding",
        "MessageName",
        "Description",
        "Organization",
        "Group",
        "DateModified",
        "LastModifiedBy",
        "ErrorRetrievingResponses",
        "System",
    ];
    constructor(
        @Inject(UserActionService) private UserActions: UserActionService,
        @Inject(StoreToken) private store: IStore,
        private smtpConnectionsTemplateListService: SmtpConnectionsTemplateListService,
        private stateService: StateService,
        private toastService: ToastService,
        private translateService: TranslateService,
        private translatePipe: TranslatePipe,
    ) { }

    ngOnInit(): void {
        this.translateService
            .stream(this.keys)
            .subscribe((values) => (this.translations = values));
        this.filterIsOpen = false;
        this.tabOptions = [
            {
                id: this.tabIds.templates,
            text: this.translations["Templates"],
                action: (option: ITabsNav): void => { this.goToTab(option.id); },
            },
            {
                id: this.tabIds.branding,
                text: this.translations["Branding"],
                action: (option: ITabsNav): void => { this.goToTab(option.id); },
            },
        ];
        this.currentTab = this.stateService.params.tabId || this.tabIds.templates;
        this.columns = [
            { columnType: this.tableColumnTypes.Link, columnName: this.translations["MessageName"], cellValueKey: "templateNameTranslationKey", columnIndex: true },
            { columnType: this.tableColumnTypes.Translate, columnName: this.translations["Description"], cellValueKey: "templateDetailTranslationKey", columnIndex: false },
            { columnType: this.tableColumnTypes.Text, columnName: this.translations["Organization"], cellValueKey: "organization", columnIndex: false },
            { columnType: this.tableColumnTypes.Input, columnName: this.translations["Group"], cellValueKey: "group", columnIndex: false },
            { columnType: this.tableColumnTypes.Date, columnName: this.translations["DateModified"], cellValueKey: "lastModifiedDate", columnIndex: false },
            { columnType: this.tableColumnTypes.Text, columnName: this.translations["LastModifiedBy"], cellValueKey: "currentUser", columnIndex: false },
        ];
        this.loadTemplateList();
        this.loadFilterOptions();
    }

    onToggle(filterIsOpen: boolean): void {
        this.filterIsOpen = filterIsOpen;
    }

    showErrorMessage(key: string) {
        this.toastService.error(this.translations["Error"],
            this.translations[key],
        );
    }

    selectOption(value: IGroupFilter[]) {
        this.selectedOptions = value;
        this.filteredValues = map(this.selectedOptions, "id");
    }

    applyGroupFilter(page: number = 0): void {
        this.isTemplateLoaded = true;
        this.filterActive = true;
        this.filterIsOpen = false;
        this.smtpConnectionsTemplateListService.getGroupFilterListFiltered(page, this.pageSize, this.filteredValues)
        .pipe(take(1))
        .subscribe(
            (response: ITableData) => {
                this.tableData = response;
                this.updateOrg(this.tableData);
            },
            (err) => {
                this.showErrorMessage("ErrorRetrievingResponses");
            },
        );
    }

    loadFilterOptions(): void {
        this.smtpConnectionsTemplateListService.getGroupFilterList()
        .pipe(take(1))
        .subscribe(
            (response: IGroupFilter[]): void => {
                const filterData = response;
                this.groupFilterOptions = map(filterData, (option: IGroupFilter): IGroupFilter => {
                    return {
                        ...option,
                        translatedName: this.translatePipe.transform(option.name),
                    };
                });
            },
            (err) => {
                this.showErrorMessage("ErrorRetrievingResponses");
            },
        );
    }

    clearFilters(): void {
        this.filterIsOpen = false;
        this.selectedOptions = [];
        this.filterActive = false;
        this.loadTemplateList();
    }

    loadTemplateList(page: number = 0) {
        this.isTemplateLoaded = true;
        return this.smtpConnectionsTemplateListService.getListofAllTemplates(page, this.pageSize)
            .pipe(take(1))
            .subscribe(
                (response: ITableData): void => {
                    this.tableData = response;
                    this.updateOrg(this.tableData);
                },
                (err) => {
                    this.showErrorMessage("ErrorRetrievingResponses");
                },
            );
    }

    pageChange(event: number): void {
        this.filterActive ? this.applyGroupFilter(event) : this.loadTemplateList(event);
    }

    handleNameClick(messageTypeId: number): void {
        this.stateService.go("zen.app.pia.module.settings.messagetemplatev2.edit", { messageTypeId, languageCode: LanguageIds.English });
    }

    private updateOrg(tableData: ITableData): void {
        this.isTemplateLoaded = true;
        const userIds: string[] = [];
        each(tableData.content, (template: ITableDataContent) => {
            const id = template.lastModifiedBy ? template.lastModifiedBy.toLowerCase() : "";
            if (id && userIds.indexOf(id) < 0) {
                userIds.push(id);
            }
            if (template.organization === "System") {
                template.organization = this.translations["System"];
            }
        });
        if (!userIds.length) {
            this.tableData.content = this.modifyTemplateData(tableData.content);
            this.isTemplateLoaded = false;
        } else {
            this.UserActions.fetchUsersById(userIds).then(() => {
                this.tableData.content = this.modifyTemplateData(tableData.content);
                this.isTemplateLoaded = false;
            });
        }
    }

    private goToTab(tabId: string): void {
        const routeParams: { id: string, attributeId: string, tabId: string } = {
            id: this.stateService.params.id,
            attributeId: this.stateService.params.attributeId,
            tabId,
        };
        this.stateService.go(".", routeParams);
        this.currentTab = tabId;
    }

    private modifyTemplateData(templates: ITableDataContent[]): ITableDataContent[] {
        const state: IStoreState = this.store.getState();
        return forEach(templates, (template: ITableDataContent) => {
            if (template.orgGroupId) {
                template.organization = getOrgNameById(template.orgGroupId)(state);
            }
            template.currentUser = template.lastModifiedBy ? getUserDisplayName(getUserById(template.lastModifiedBy.toLowerCase())(state)) : "- - - -";
            return template;
        });
    }

}
