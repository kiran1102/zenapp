// Angular
import {
    Component,
    OnInit,
    Inject,
    OnDestroy,
} from "@angular/core";

// Constants & Enums
import { OrgUserDropdownTypes } from "enums/org-user-dropdown.enum";
import { OrgUserTraversal } from "enums/org-user-dropdown.enum";

// Interfaces
import { IStore, IStoreState } from "interfaces/redux.interface";
import { IHelpSettings } from "interfaces/setting.interface";
import { IOtOrgUserOutput } from "sharedModules/components/ot-org-user/ot-org-user.interface";

// Redux
import { Unsubscribe } from "redux";
import {
    getHelpSettings,
    getHelpSettingsFetchStatus,
} from "oneRedux/reducers/setting.reducer";
import { StoreToken } from "tokens/redux-store.token";

// Services
import { SettingActionService } from "settingsModule/services/actions/setting-action.service";

@Component({
    selector: "settings-help",
    templateUrl: "./settings-help.component.html",
    host: {
        class: "flex-full-height height-100",
    },
})
export class SettingsHelpComponent implements OnInit, OnDestroy {

    unsub: Unsubscribe;
    isLoading = true;
    isSaving = false;
    helpSettings: IHelpSettings;
    userDropdownType: number = OrgUserDropdownTypes.HelpContact;

    orgUserTraversal = OrgUserTraversal;

    constructor(
        @Inject(StoreToken) private store: IStore,
        private readonly SettingAction: SettingActionService,
    ) {}

    ngOnInit(): void {
        this.unsub = this.store.subscribe(() => this.componentWillReceiveState());
        this.SettingAction.fetchHelpSettings();
    }

    ngOnDestroy(): void {
        this.unsub();
    }

    componentWillReceiveState(): void {
        const storeState: IStoreState = this.store.getState();
        this.isLoading = getHelpSettingsFetchStatus(storeState);
        this.helpSettings = getHelpSettings(storeState);
    }

    enableCustomMessage(value: boolean): void {
        this.helpSettings.Enabled = value;
    }

    updateCustomMessageEmail(user: IOtOrgUserOutput): void {
        this.helpSettings.ContactId = user.selection ? user.selection.Id : null;
    }

    updateCustomMessageText(text: string): void {
        this.helpSettings.DefaultComment = text;
    }

    canSave(): boolean {
        return Boolean(this.helpSettings
                && (!this.helpSettings.Enabled
                    || (this.helpSettings.Enabled && this.helpSettings.ContactId)));
    }

    saveSetting(): void {
        this.isSaving = true;
        this.SettingAction.saveHelpSettings(this.helpSettings).then((result: boolean): void => {
            this.isSaving = false;
        });
    }
}
