// 3rd Party
import {
    find,
    isArray,
} from "lodash";
import {
    Component,
    Inject,
    OnDestroy,
    OnInit,
} from "@angular/core";
import Utilities from "Utilities";

// Interfaces
import { ISettingsTestEmailModalResolve } from "interfaces/email-template.interface";
import { ILanguageActivatorData } from "interfaces/email-template.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";
import { IOtModalContent } from "@onetrust/vitreus";

// Constants
import { Regex } from "constants/regex.constant";

// rxJs
import { Subject } from "rxjs";

// Services
import { SmtpConnectionsTemplateListService } from "settingsComponents/smtp-connections-list/smtp-connections-template-list.service";
import { MessageApiService } from "settingsServices/apis/message-api.service";

@Component({
    selector: "email-template-preview-modal",
    templateUrl: "./email-template-preview-modal.component.html",
})

export class EmailTemplatePreviewModalV2 implements OnInit, IOtModalContent, OnDestroy {

    otModalData: ISettingsTestEmailModalResolve;
    isLoading: boolean;
    modalTitle: string;
    submitEmailButtonText: string;
    isSaving: boolean;
    testEmail: string;
    EmailRegex = Regex.EMAIL;
    languages: ILanguageActivatorData[];
    selectedLanguage: ILanguageActivatorData;
    otModalCloseEvent: Subject<boolean>;
    private languageId: number;
    private messageTypeId: number;
    private destroy$ = new Subject();

    constructor(
        private messageApiService: MessageApiService,
        private smtpConnectionsTemplateListService: SmtpConnectionsTemplateListService,
    ) { }

    ngOnInit() {
        this.isLoading = true;
        this.languageId = this.otModalData.languageId;
        this.messageTypeId = this.otModalData.messageTypeId;
        this.modalTitle = this.otModalData.modalTitle;
        this.submitEmailButtonText = this.otModalData.submitEmailButtonText;
        this.smtpConnectionsTemplateListService.getEmailLanguages()
        .then((response: ILanguageActivatorData[]) => {
            if (response && isArray(response)) {
                response.forEach((selection: ILanguageActivatorData) => {
                    if (selection.Translated) {
                        selection.Activated = true;
                    }
                });
                this.languages = response.filter((languageData: ILanguageActivatorData) => languageData.Activated);
                this.selectedLanguage = find(this.languages, (language: ILanguageActivatorData) => language.Id === this.languageId);
            }
            this.isLoading = false;
        });
    }

    ngOnDestroy() {
        this.destroy$.next();
        this.destroy$.complete();
    }

    setEmail(selection: string) {
        const validateEmail = Utilities.matchRegex(selection, this.EmailRegex);
        if (validateEmail) {
            this.testEmail = selection;
        }
    }

    onEmailLanguageChange(selection: ILanguageActivatorData) {
        this.selectedLanguage = selection;
    }

    closeModal() {
        this.otModalCloseEvent.next(false);
    }

    validateForm(): boolean {
        return this.languageId ? this.testEmail && this.selectedLanguage && Boolean(this.selectedLanguage.Id)
            : Boolean(this.testEmail);
    }

    sendTestEmail() {
        this.isSaving = true;
        if (this.languageId) {
            const previewEmailSettings = {
                messageTypeId: this.messageTypeId,
                email: this.testEmail,
                languageId: this.selectedLanguage.Id,
            };
            return this.messageApiService.sendPreviewMessage(previewEmailSettings).then(() => {
                this.isSaving = false;
                this.closeModal();
            });
        } else {
            const testEmailSettings = {
                messageTypeId: this.messageTypeId,
                email: this.testEmail,
            };
            return this.messageApiService.sendTestMessage(testEmailSettings).then(() => {
                this.isSaving = false;
                this.closeModal();
            });
        }
    }
}
