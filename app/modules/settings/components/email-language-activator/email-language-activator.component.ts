import { Transition } from "@uirouter/core";
import {
    Component,
    Inject,
    OnInit,
    OnDestroy,
    Input,
} from "@angular/core";
import { IPromise } from "angular";
import {
    filter,
    forEach,
    isArray,
} from "lodash";

// Services
import { SmtpConnectionsTemplateListService } from "settingsComponents/smtp-connections-list/smtp-connections-template-list.service";

// Interfaces
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import { ILanguageActivatorData } from "interfaces/email-template.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";
import { ColumnDefinition } from "crmodel/column-definition";
import { IStringMap } from "interfaces/generic.interface";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

@Component({
    selector: "email-language-activator",
    templateUrl: "./email-language-activator.component.html",
})

export class EmailLanguageActivatorComponent implements OnInit {
    public translations: IStringMap<string>;
    public languageColumns: ColumnDefinition[];
    public languageTableData: ILanguageActivatorData[];
    public allRowSelected = true;
    public isLoading = false;
    constructor(
        private translatePipe: TranslatePipe,
        private smtpConnectionsTemplateListService: SmtpConnectionsTemplateListService,
    ) {
        this.translations = {
            save: this.translatePipe.transform("Save"),
            cancel: this.translatePipe.transform("Cancel"),
            languageActivatorHeading: this.translatePipe.transform("LanguageActivatorHeading"),
            languageActivatorSubHeading: this.translatePipe.transform("LanguageActivatorSubHeading"),
            selectAll: this.translatePipe.transform("SelectAll"),
        };
    }

    ngOnInit(): void {
        this.isLoading = true;
        this.getAllLanguages();
        this.languageColumns = [
            { columnName: this.translations.selectAll, cellValueKey: "Name" },
            { columnType: "checkbox" },
        ];
    }

    getAllLanguages(): Promise<boolean> {
        this.allRowSelected = true;
        return this.smtpConnectionsTemplateListService.getEmailLanguages().then((response: ILanguageActivatorData[] | boolean): boolean => {
            if (response && isArray(response)) {
                this.languageTableData = response;
                forEach(this.languageTableData, (languageData: ILanguageActivatorData): void => {
                    if (languageData.Translated) {
                        languageData.Activated = true;
                    }
                    if (!languageData.Activated) {
                        this.allRowSelected = false;
                    }
                });
                this.isLoading = false;
            } else {
                this.isLoading = true;
            }
            return true;
        });
    }

    handleTableSelectClick(actionType: string, isChecked: boolean, code?: number): void {
        switch (actionType) {
            case "header":
                forEach(this.languageTableData, (languageData: ILanguageActivatorData): void => {
                    if (languageData.Translated === false) {
                        languageData.Activated = isChecked;
                    }
                });
                break;
            case "row":
                this.allRowSelected = true;
                forEach(this.languageTableData, (languageData: ILanguageActivatorData): void => {
                    if (languageData.Id === code) {
                        languageData.Activated = isChecked;
                    }
                    if (!languageData.Activated) {
                        this.allRowSelected = false;
                    }
                });
                break;
        }
    }

    onSave(): void {
        this.isLoading = true;
        const selectedEmailData: ILanguageActivatorData[] = filter(this.languageTableData, (languageData: ILanguageActivatorData) => languageData.Activated);
        this.smtpConnectionsTemplateListService.updateEmailLanguages(selectedEmailData).then((response: boolean): boolean => {
            if (response) {
                this.getAllLanguages();
            }
            return true;
        });
    }

    onCancel(): void {
        this.isLoading = true;
        this.getAllLanguages();
    }
}
