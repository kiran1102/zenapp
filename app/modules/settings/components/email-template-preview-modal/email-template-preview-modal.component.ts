// 3rd Party
import {
    find,
    isArray,
} from "lodash";

import {
    Component,
    Inject,
    Input,
    OnInit,
} from "@angular/core";
import Utilities from "Utilities";

// Interfaces
import { ISettingsTestEmailModalResolve } from "interfaces/email-template.interface";
import { ILanguageActivatorData } from "interfaces/email-template.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";
import { IStore } from "interfaces/redux.interface";

// Constants
import { Regex } from "constants/regex.constant";

// Templates
import { buildDefaultModalTemplate } from "modals/modal-template/modal-template";

// Reduxx
import { StoreToken } from "tokens/redux-store.token";
import { getModalData } from "oneRedux/reducers/modal.reducer";

// Services
import { ModalService } from "sharedServices/modal.service";
import { SmtpConnectionsTemplateListService } from "settingsComponents/smtp-connections-list/smtp-connections-template-list.service";
import { MessageApiService } from "settingsServices/apis/message-api.service";

@Component({
    selector: "email-template-preview-modal",
    templateUrl: "./email-template-preview-modal.component.html",
})

export class EmailTemplatePreviewModal implements OnInit {

    @Input() resolve: ISettingsTestEmailModalResolve;
    isLoading: boolean;
    modalTitle: string;
    sendConnectionEmail: string;
    isSaving: boolean;
    testEmail: string;
    EmailRegex = Regex.EMAIL;
    languages: ILanguageActivatorData[];
    selectedLanguage: ILanguageActivatorData;
    private languageId: number;
    private messageTypeId: number;

    constructor(
        @Inject(StoreToken) private store: IStore,
        private modalService: ModalService,
        private readonly smtpConnectionsTemplateListService: SmtpConnectionsTemplateListService,
        private messageApiService: MessageApiService,
    ) { }

    ngOnInit() {
        this.isLoading = true;
        const modalData = getModalData(this.store.getState());
        this.modalTitle = modalData.modalTitle;
        this.sendConnectionEmail = modalData.submitEmailButtonText;
        this.languageId = modalData.languageId;
        this.messageTypeId = modalData.messageTypeId;
        this.smtpConnectionsTemplateListService.getEmailLanguages()
        .then((response: ILanguageActivatorData[] | boolean): void => {
            if (response && isArray(response)) {
                response.forEach((selection: ILanguageActivatorData): void => {
                    if (selection.Translated) {
                        selection.Activated = true;
                    }
                 });
                this.languages = response.filter((languageData: ILanguageActivatorData) => languageData.Activated);
                this.selectedLanguage = find(this.languages, (language: ILanguageActivatorData) => language.Id === this.languageId);
            }
            this.isLoading = false;
        });
    }

    setEmail(selection: string) {
        const validateEmail = Utilities.matchRegex(selection, this.EmailRegex);
        if (validateEmail) {
            this.testEmail = selection;
        }
    }

    onEmailLanguageChange(selection: ILanguageActivatorData) {
        this.selectedLanguage = selection;
    }

    closeModal() {
        this.modalService.closeModal();
        this.modalService.clearModalData();
    }

    validateForm(): boolean {
        return this.languageId ? this.testEmail && this.selectedLanguage && Boolean(this.selectedLanguage.Id)
            : Boolean(this.testEmail);
    }

    sendTestEmail() {
        this.isSaving = true;
        if (this.languageId) {
            const previewEmailSettings = {
                messageTypeId: this.messageTypeId,
                email: this.testEmail,
                languageId: this.selectedLanguage.Id,
            };
            this.messageApiService.sendPreviewMessage(previewEmailSettings).then(() => {
                this.isSaving = false;
                this.closeModal();
            });
        } else {
            const testEmailSettings = {
                messageTypeId: this.messageTypeId,
                email: this.testEmail,
            };
            this.messageApiService.sendTestMessage(testEmailSettings).then(() => {
                this.isSaving = false;
                this.closeModal();
            });
        }
    }
}
