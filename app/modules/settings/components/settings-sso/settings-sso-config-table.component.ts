import {
    Component,
    EventEmitter,
    Input,
    Output,
} from "@angular/core";

// Interfaces
import {
    ISamlHost,
    ISamlAttributeMapping,
} from "interfaces/settings-sso.interface";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

@Component({
    selector: "settings-config-table",
    templateUrl: "./settings-sso-config-table.component.html",
})

export class SettingSsoConfigTable {

    @Input() rows: ISamlHost;
    @Input() attributeRows: ISamlAttributeMapping;
    @Input() enableAdd: boolean;
    @Output() addNew = new EventEmitter();
    @Output() validateAttributeMapping = new EventEmitter();
    @Output() hostChanges = new EventEmitter();
    @Output() removeHost = new EventEmitter<number>();

    HostConfigColumns = [
        {
            columnName: this.translatePipe.transform("Name"),
            columnKey: "Name",
            cellValueKey: "Name",
            style: {
                "font-weight": "bold",
                "padding": "0.4rem 0.8rem",
            },
        },
        {
            columnName: this.translatePipe.transform("Description"),
            columnKey: "Description",
            cellValueKey: "Description",
            style: {
                "font-weight": "bold",
                "padding": "0.4rem 0.8rem",
            },
        },
        {
            columnName: "",
            columnKey: "Actions",
            cellValueKey: "",
        },
    ];

    AttributeMappingsColumns = [
        { columnName: this.translatePipe.transform("Attribute"), columnKey: "Attribute", cellValueKey: "Name" },
        { columnName: this.translatePipe.transform("AttributeKey"), columnKey: "AttributeKey", cellValueKey: "MapKey" },
        { columnName: this.translatePipe.transform("DefaultValue"), columnKey: "DefaultValue", cellValueKey: "DefaultValue" },
    ];

    constructor(
        private translatePipe: TranslatePipe,
    ) { }

    removeRow(rowIndex: number) {
        if (this.removeHost.observers.length) {
            this.removeHost.emit(rowIndex);
        }
    }
    onChange(event: string, row: ISamlAttributeMapping, col: string) {
        row[col] = event;
        this.validateAttributeMapping.emit();
    }

    onChangeHost(event: string, col: string, row: ISamlHost) {
        row[col] = event;
        this.hostChanges.emit();
    }

    addRow() {
        if (!this.enableAdd) {
            return this.addNew.emit();
        }
    }

}
