// Angular
import { Component } from "@angular/core";

// Vitreus
import { IOtModalContent, OtModalService } from "@onetrust/vitreus";

// Interfaces
import {
    ISamlIdpCertificate,
    ICertificateFileUpload,
} from "interfaces/settings-sso.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";
import { Settings } from "sharedServices/settings.service";

// 3rd party
import { Subject } from "rxjs";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

@Component({
    selector: "settings-sso-certificate-upload-modal",
    templateUrl: "./settings-sso-certificate-upload-modal.component.html",
})

export class SettingSsoCertificateUploadModal implements IOtModalContent {

    otModalCloseEvent: Subject<IProtocolResponse<ISamlIdpCertificate> | boolean>;
    allowedFileExtension = ["cert", "crt", "pem", "cer"];
    isLoading: boolean;
    certificateFile: ICertificateFileUpload = {
        Password: "",
        FileName: "",
        Extension: "",
        Data: "",
    };

    constructor(
        private otModalService: OtModalService,
        private translatePipe: TranslatePipe,
        private settings: Settings,
    ) {}

    uploadCertificate() {
        this.isLoading = true;
        this.settings.addCertificate(this.certificateFile).then((response: IProtocolResponse<ISamlIdpCertificate>): void => {
            if (response.result) {
                this.otModalCloseEvent.next({result: response.result, data: response.data});
            }
            this.isLoading = false;
        });
    }

    certificateSelected(files: File[]) {
        if (!files || !files.length) return;
        this.certificateFile.Extension = files[0].name.substr(files[0].name.lastIndexOf(".") + 1);
        this.certificateFile.FileName = files[0].name.replace(`.${this.certificateFile.Extension}`, "");
        const reader: FileReader = new FileReader();
        reader.readAsDataURL(files[0]);
        reader.onloadend = () => {
            this.certificateFile.Data = reader.result.toString();
        };
    }

    removeFile() {
        this.certificateFile = {
            FileName: "",
            Extension: "",
            Data: "",
            Password: "",
        };
    }

    closeModal() {
        this.otModalCloseEvent.next(false);
    }

}
