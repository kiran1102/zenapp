// Angular
import {
    Component,
    Inject,
    OnInit,
} from "@angular/core";
import {
    FormGroup,
    FormBuilder,
    Validators,
    FormControl,
} from "@angular/forms";

// Vitreus
import {
    ConfirmModalType,
    OtModalService,
    ModalSize,
    IOtModalContent,
} from "@onetrust/vitreus";

// Lodash
import {
    clone,
    isEmpty,
    find,
    isUndefined,
    isNil,
    includes,
} from "lodash";

// Enums + constants
import { ExportTypes } from "enums/export-types.enum";
import { SsoSetupTabs } from "constants/settings-sso.constant";
import {
    BindingTypes,
    SamlAttributeType,
} from "enums/settings-sso.enum";
import {
    FormDataTypes,
    FormInputTypes,
} from "constants/form-types.constant";
import { Regex } from "constants/regex.constant";

// Interfaces
import { IProtocolResponse } from "interfaces/protocol.interface";
import { IStringMap } from "interfaces/generic.interface";
import { ITabsNav } from "interfaces/one-tabs-nav.interface";
import { Settings } from "sharedServices/settings.service";
import { ICertificateFileUpload } from "interfaces/settings-sso.interface";
import {
    IAddSamlSettings,
    ISamlIdpCertificate,
    ISamlHost,
    ISamlAttributeMapping,
    ISamlCertificateResponse,
} from "interfaces/settings-sso.interface";

// Pipe
import { TranslatePipe } from "pipes/translate.pipe";

// Rxjs
import { Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";

// Component
import { SettingSsoCertificateUploadModal } from "settingsComponents/settings-sso/settings-sso-certificate-upload-modal.component";
import { Permissions } from "modules/shared/services/helper/permissions.service";

@Component({
    selector: "setting-sso",
    templateUrl: "./settings-sso.component.html",
})

export class SettingSsoComponent implements OnInit {

    navTabs = SsoSetupTabs;
    getTabs: IStringMap<ITabsNav> = {
        Configuration: {
            id: this.navTabs.CONFIGURATION,
            text: this.translatePipe.transform("Configuration"),
            action: (option: ITabsNav): void => { this.currentTab = option.id; },
        },
        Org_Asgmt: {
            id: this.navTabs.ORG_ASGMT,
            text: this.translatePipe.transform("OrgAssignment"),
            action: (option: ITabsNav): void => { this.currentTab = option.id; },
        },
    };
    singleSignOnForm: FormGroup;
    errorMessage: string;
    currentTab: string = SsoSetupTabs.CONFIGURATION;
    tabOptions: ITabsNav[] = [];
    FormDataTypes = FormDataTypes;
    FormInputTypes = FormInputTypes;
    settingSsoConfig: IAddSamlSettings;
    bindingTypes = BindingTypes;
    original: IAddSamlSettings;
    isReady = false;
    canSave: boolean;
    isOrgAssgmtTabEnable: boolean;
    hostClone: ISamlHost[];
    isValidHost = false;
    isAddHostEnable: boolean;
    newHost: ISamlHost;
    samlAttributeType = SamlAttributeType;
    private destroy$ = new Subject();

    constructor(
        @Inject("Export") private Export: any,
        private settings: Settings,
        private translatePipe: TranslatePipe,
        private formBuilder: FormBuilder,
        private otModalService: OtModalService,
        private permission: Permissions,
    ) {}

    ngOnInit() {
        this.original = clone(this.settingSsoConfig);
        this.tabOptions.push(this.getTabs[this.navTabs.CONFIGURATION]);
        this.getSsoSettings();
    }

    initForm() {
        this.singleSignOnForm = this.formBuilder.group({
            IdentityProviderName: new FormControl(
                this.settingSsoConfig.IdentityProviderName,
                [Validators.required],
            ),
            IdentityProviderSignOnUrl: new FormControl(
                this.settingSsoConfig.IdentityProviderSignOnUrl,
                [Validators.required, Validators.pattern(Regex.URL)],
            ),
            IdentityProviderSignOutUrl: new FormControl(
                this.settingSsoConfig ? this.settingSsoConfig.IdentityProviderSignOutUrl : "",
                [Validators.pattern(Regex.URL)],
            ),
        });
    }

    changeTab(option: ITabsNav) {
        this.currentTab = option.id;
        if (this.currentTab === this.navTabs.CONFIGURATION) {
            this.getSsoSettings();
        }
    }

    ngOnDestroy() {
        this.destroy$.next();
        this.destroy$.complete();
    }

    getError(field: string): boolean {
        const input = this.singleSignOnForm.get(field);
        this.errorMessage = null;
        if (!input || !input.errors) {
            return false;
        } else if (input.dirty && input.touched && input.errors.required) {
            this.errorMessage = this.translatePipe.transform("ThisFieldCannotBeEmpty");
            return true;
        } else if (field === "IdentityProviderSignOnUrl" || field === "IdentityProviderSignOutUrl" &&
            (input.dirty && input.touched && input.invalid)) {
            this.errorMessage = this.translatePipe.transform("PleaseEnterValidIdentityProviderSignOnUrl");
            return true;
        }
        return false;
    }

    setFormValue() {
        this.settingSsoConfig.IdentityProviderName = this.singleSignOnForm.get("IdentityProviderName").value;
        this.settingSsoConfig.IdentityProviderSignOnUrl = this.singleSignOnForm.get("IdentityProviderSignOnUrl").value;
        this.settingSsoConfig.IdentityProviderSignOutUrl = this.singleSignOnForm.get("IdentityProviderSignOutUrl").value;
    }

    getSsoSettings() {
        this.isReady = true;
        this.settings.getDirectory().then((response: IProtocolResponse<IAddSamlSettings>): void => {
            if (response.result && response.data) {
                this.settingSsoConfig = response.data;
                this.original = clone(this.settingSsoConfig);
                this.getOrgTabMethods();
                this.initForm();
            }
            this.isReady = false;
        });
    }

    getOrgTabMethods() {
        this.hostClone = clone(this.settingSsoConfig.Hosts);
        this.enableOrgTab();
        this.toggleTab();
        this.validateAttributeMapping();
        if (isEmpty(this.hostClone)) {
            this.addHost();
        }
    }

    toggleSamlRequest() {
        this.settingSsoConfig.WantRequestsSign = !this.settingSsoConfig.WantRequestsSign;
    }

    hasOrgTab(): boolean {
        return Boolean(find (this.tabOptions, (option: ITabsNav) => option.id === this.navTabs.ORG_ASGMT));
    }

    toggleTab(): ITabsNav | string | number {
        if (this.settingSsoConfig.IsEnabled && !this.hasOrgTab() && !this.isOrgAssgmtTabEnable) {
            return this.tabOptions.push(this.getTabs[this.navTabs.ORG_ASGMT]);
        } else if (!this.settingSsoConfig.IsEnabled || (this.settingSsoConfig.IsEnabled && this.isOrgAssgmtTabEnable)) {
           return includes(this.tabOptions, this.getTabs[this.navTabs.ORG_ASGMT]) ? this.tabOptions.pop() : "";
        }
    }

    getConfirmationText(hasId: string, isEnabled: boolean): string {
        if (hasId && isEnabled) {
            return this.translatePipe.transform("TryingToEditSSOSettings");
        } else if (hasId && !isEnabled) {
            return this.translatePipe.transform("ToContinueConvertingSSOUserToLocal");
        }
        return this.translatePipe.transform("ToContinueConvertingLocalUserToSSO");
    }

    cancelSamlSettings() {
        this.getSsoSettings();
    }

    addSamlSettings() {
        if (!this.settingSsoConfig.IsEnabled && (this.settingSsoConfig.IsEnabled === this.original.IsEnabled )) {
            this.isReady = true;
            this.saveSettings();
        } else {
            this.validateHostInputs();
            this.otModalService
                .confirm({
                    type: ConfirmModalType.WARNING,
                    translations: {
                        title: this.translatePipe.transform("Confirm"),
                        desc: this.getConfirmationText(this.settingSsoConfig.Id, this.settingSsoConfig.IsEnabled),
                        confirm: this.settingSsoConfig.IsEnabled && this.settingSsoConfig.Id
                            ? this.translatePipe.transform("AreYouSureContinue") : "",
                        submit: this.translatePipe.transform("Ok"),
                        cancel: this.translatePipe.transform("Cancel"),
                    },
                }).pipe(
                    takeUntil(this.destroy$),
                ).subscribe(
                    (res: boolean) => {
                        if (res) {
                            this.saveSettings();
                        }
                    },
                );
        }
    }

    validateHostInputs() {
        this.settingSsoConfig.Hosts = this.hostClone &&
        (this.hostClone.length === 1 &&
            !this.hostClone[0].Name &&
            !this.hostClone[0].Description
        )  ? [] : this.hostClone;
    }

    addHost() {
        this.newHost = {
            Name: null,
            Description: null,
        };
        this.hostClone.push(this.newHost);
        this.hostInputChanges();
    }

    removeHost(index: number) {
        this.hostClone.splice(index, 1);
        if (this.hostClone.length === 0) {
            this.addHost();
        }
        this.hostInputChanges();
    }

    clearCertificate() {
        this.settingSsoConfig.IdpCertificate = "";
    }

    saveSettings() {
        this.setFormValue();
        this.settingSsoConfig.IdpCertificateId = this.settingSsoConfig.IdpCertificate ? (this.settingSsoConfig.IdpCertificate as ISamlIdpCertificate).Id : "";
        this.settings.setSSOSettings(this.settingSsoConfig).then((response: IProtocolResponse<IAddSamlSettings>): boolean => {
            if (response.result) {
                this.original = clone(this.settingSsoConfig);
                this.getSsoSettings();
            }
            return response.result;
        });
    }

    setEnable() {
        this.settingSsoConfig.IsEnabled = !this.settingSsoConfig.IsEnabled;
        this.validateAttributeMapping(this.settingSsoConfig.IsEnabled);
    }

    setBindingType(id: number, isRequest: boolean) {
        (isRequest) ? this.settingSsoConfig.ResponseBindingTypeId = id :
            this.settingSsoConfig.RequestBindingTypeId = id;
    }

    resetSettings() {
        this.otModalService
            .confirm({
                type: ConfirmModalType.WARNING,
                translations: {
                    title: this.translatePipe.transform("Confirm"),
                    desc: this.translatePipe.transform("SamlResetSettingsConfirmModalText"),
                    confirm: this.translatePipe.transform("AreYouSureContinue"),
                    submit: this.translatePipe.transform("Ok"),
                    cancel: this.translatePipe.transform("Cancel"),
                },
            }).pipe(
                takeUntil(this.destroy$),
            ).subscribe(
                (res: boolean) => {
                    if (res) {
                        this.settings.resetSSOSettings(this.settingSsoConfig).then((response: IProtocolResponse<IAddSamlSettings>): boolean => {
                            if (response.result) {
                                this.getSsoSettings();
                            }
                            return response.result;
                        });
                    }
                },
            );
    }

    downloadLogs(): ng.IPromise<void> {
        return this.Export.downloadFile(
            "/Report/ExportSamlLogs",
            ExportTypes.XLSX,
            null,
            this.translatePipe.transform("SorryErrorWhileRetrievingLogs"),
        );
    }

    openCertificateUploadtModal() {
        this.otModalService.create(
            SettingSsoCertificateUploadModal,
            {
              isComponent: true,
            },
        ).pipe(
            takeUntil(this.destroy$),
        ).subscribe(
            (res: ISamlCertificateResponse): void => {
                if (res.result) {
                    this.settingSsoConfig.IdpCertificate = res.data;
                }
        });
    }

    hostInputChanges() {
        const index: number = this.hostClone.length - 1 ;
        this.isAddHostEnable = isNil(this.hostClone[index].Name) || isEmpty(this.hostClone[index].Name);
        if (this.hostClone && this.hostClone.length === 1 ) {
            this.isValidHost = this.hostRowValidations(index);
        } else if (this.hostClone.length > 1) {
            if ((isEmpty(this.hostClone[index].Name)) && (isEmpty(this.hostClone[index].Description))) {
                this.isValidHost = true;
            } else { this.isValidHost = this.hostRowValidations(index); }
        } else {
            this.isValidHost = false;
        }
        this.validateAttributeMapping(this.settingSsoConfig.IsEnabled, this.isValidHost);
    }

    hostRowValidations(index: number): boolean {
        return isEmpty(this.hostClone[index].Name) && !isEmpty(this.hostClone[index].Description);
    }

    validateAttributeMapping(isToggle?: boolean, hostvalid?: boolean) {
        const attributeValue: boolean = Boolean(find(this.settingSsoConfig.AttributeMappings,
            (attr: ISamlAttributeMapping): boolean => isEmpty(attr.DefaultValue || attr.MapKey)));
        if ((isToggle || isUndefined(isToggle))) {
            this.canSave = hostvalid || attributeValue;
        } else if (isNil(this.settingSsoConfig.Id)) {
            this.canSave = true;
        } else {
            this.canSave = false;
        }
    }

    enableOrgTab() {
        const enableOrgTabOptions: boolean = !this.isReady && this.permission.canShow("SettingsSSOOrgGroupMappings");
        const attributeValue: boolean = Boolean(find(this.settingSsoConfig.AttributeMappings,
            (attr: ISamlAttributeMapping): boolean => {
                return (attr.SamlAttributeTypeId === this.samlAttributeType.Organization &&
                isEmpty(attr.MapKey));
            }));
        this.isOrgAssgmtTabEnable =  enableOrgTabOptions || attributeValue;
    }

}
