// Angular
import {
    Component,
    Inject,
    OnInit,
} from "@angular/core";

// Lodash
import {
    find,
    findIndex,
    isEmpty,
} from "lodash";

// Vitreus
import {
    OtModalService,
    IOtModalContent,
} from "@onetrust/vitreus";

// 3rd party
import { Subject } from "rxjs";

// Constants
import { SamlAttributeType } from "enums/settings-sso.enum";

// redux
import { StoreToken } from "tokens/redux-store.token";
import { getOrgList } from "oneRedux/reducers/org.reducer";

// Service
import { Settings } from "sharedServices/settings.service";
import { Principal } from "modules/shared/services/helper/principal.service";

// Interfaces
import { IStore } from "interfaces/redux.interface";
import { IOrganization } from "interfaces/org.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";
import {
    ISamlOrgGroupMapping,
    ISamlOrgMappingResponse,
    IAddSamlSettings,
    ISamlAttributeMapping,
    ISamlOrgGroupMappingTable,
    ISamlOrgGroupMappingModalResolve,
} from "interfaces/settings-sso.interface";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

@Component({
    selector: "settings-org-group-mapping-modal",
    templateUrl: "./settings-org-group-mapping-modal.component.html",
})

export class SettingsOrgGroupMappingModal implements OnInit, IOtModalContent {

    selectedOrgGroup: string;
    showOrgDropdown = false;
    attributeName = "";
    attrInputValue = "";
    samlGroupName = "";
    isLoading = false;
    isSavingNext = false;
    loadingAttributes = false;
    selectedOrganization: IOrganization;
    OrgMappingParams: ISamlOrgGroupMappingTable;
    otModalCloseEvent: Subject<boolean>;
    otModalData: ISamlOrgGroupMappingModalResolve;

    constructor(
        @Inject(StoreToken) private store: IStore,
        private principal: Principal,
        private settings: Settings,
        private translatePipe: TranslatePipe,
        private otModalService: OtModalService,
    ) {}

    ngOnInit() {
        this.showOrgDropdown = this.otModalData.showOrgDropdown;
        this.populateAttributeValues();
    }

    getOrgList(): IOrganization[] {
        const rootOrgId = this.principal.getRootOrg();
        return getOrgList(rootOrgId)(this.store.getState());
    }

    populateAttributeValues() {
        const organizationList: IOrganization[] = this.getOrgList();
        if (this.otModalData.isEditOrgMappingAttr && this.otModalData.showOrgDropdown) {
            this.attributeName = !this.otModalData.addNewAttribute ? this.otModalData.row.Key : "";
            const organization: IOrganization = find(organizationList, (org: IOrganization): boolean => {
                return !this.otModalData.addNewAttribute ? org.Name === this.otModalData.row.Value : false ;
            });
            this.selectedOrgGroup = organization ? organization.Id : "";
        } else if (this.otModalData.isEditOrgMappingAttr && !this.otModalData.showOrgDropdown) {
            this.samlGroupName = this.otModalData.orgName;
        }
        this.attrInputValue = this.otModalData.showOrgDropdown ? this.attributeName : this.samlGroupName;
    }

    selectOrgGroup(selection: string) {
        const organizationList: IOrganization[] = this.getOrgList();
        this.selectedOrgGroup = selection;
        this.selectedOrganization = find(organizationList, (organization: IOrganization): boolean => {
            return organization.Id === selection;
        });
    }

    closeModal(reloadList: boolean = false) {
        this.otModalCloseEvent.next(reloadList);
    }

    addAttribute(addNextRow: boolean) {
        const newAttributeRequest: ISamlOrgGroupMapping = {
            SamlAttributeType: SamlAttributeType.Organization,
            Key: this.attributeName,
            Value: this.selectedOrganization.Name,
        };
        if (addNextRow) this.isSavingNext = true;
        this.isLoading = true;
        this.settings.addOrgMappingAttribute(newAttributeRequest).then((res: IProtocolResponse<ISamlOrgMappingResponse>): void => {
            if (res.result) {
                this.loadingAttributes = true;
                if (addNextRow) {
                    this.isLoading = true;
                    this.setNextRowAttribute();
                } else {
                    this.otModalCloseEvent.next(this.loadingAttributes);
                }
            }
            this.isSavingNext = false;
            this.isLoading = false;
        });
    }

    setNextRowAttribute() {
        this.isSavingNext = false;
        this.isLoading = false;
        const newAttributeRequest: ISamlOrgGroupMapping = {
            SamlAttributeType: SamlAttributeType.Organization,
            Key: "",
            Value: "",
        };
        this.loadingAttributes = true;
        if (newAttributeRequest) {
            this.otModalData.row = newAttributeRequest;
            this.populateAttributeValues();
        } else {
            this.otModalCloseEvent.next(this.loadingAttributes);
        }
    }

    editAttribute(editNextRow: boolean) {
        const editAttributeRequest: ISamlOrgGroupMapping = {
            Guid: this.otModalData.row.Guid,
            SamlAttributeType: SamlAttributeType.Organization,
            Key: this.attributeName,
            Value: this.selectedOrganization ? this.selectedOrganization.Name : this.otModalData.row.Value,
        };
        if (editNextRow) {
            this.isSavingNext = true;
        } else {
            this.isLoading = true;
        }
        this.settings.updateMappingAttribute(editAttributeRequest, this.otModalData.row.Guid).then((res: IProtocolResponse<ISamlOrgGroupMapping>): void => {
            if (res.result) {
                this.loadingAttributes = true;
                if (editNextRow) {
                    this.getNextRowAttribute();
                    this.isLoading = true;
                } else {
                    this.otModalCloseEvent.next(this.loadingAttributes);
                }
            }
            this.isSavingNext = false;
            this.isLoading = false;
        });
    }

    editSamlGroupName() {
        this.otModalData.samlConfigValues.AttributeMappings.forEach((attr: ISamlAttributeMapping): void => {
            if (attr.SamlAttributeTypeId === SamlAttributeType.Organization)  {
                attr.MapKey = this.samlGroupName;
            }
        });
        this.isLoading = true;
        this.settings.setSSOSettings(this.otModalData.samlConfigValues).then((res: IProtocolResponse<IAddSamlSettings>): void => {
            if (res.result) {
                this.otModalCloseEvent.next(true);
            }
        });
    }

    handleSubmit(editNextRow: boolean = false) {
        if (this.otModalData.showOrgDropdown) {
            if (this.otModalData.addNewAttribute) {
                this.addAttribute(editNextRow);
            }  else {
                this.editAttribute(editNextRow);
            }
        } else {
            this.editSamlGroupName();
        }
    }

    setAttributeName(value: string) {
        if (this.otModalData.showOrgDropdown) {
            this.attributeName = value;
        } else {
            this.samlGroupName = value;
        }
    }

    getNextRowAttribute() {
        let index: number = findIndex(this.otModalData.samlOrgMapping, (row: ISamlOrgGroupMapping): boolean => row.Key === this.otModalData.row.Key);
        if (this.otModalData.samlOrgMapping.length < 1) return;
        index = (this.otModalData.pageNumber * 10) + index;
        const rowIndex: number = index + 1;
        this.loadingAttributes = true;
        if (this.otModalData.search) {
            this.OrgMappingParams = {
                page: rowIndex,
                size: 1,
                sort: this.otModalData.sort,
                attributeValue: this.otModalData.search,
            };
        } else {
            this.OrgMappingParams = {
                page: rowIndex,
                size: 1,
                search: SamlAttributeType.Organization,
                sort: this.otModalData.sort,
            };
        }
        this.settings.getMappingAttribute(this.OrgMappingParams).then((response: IProtocolResponse<ISamlOrgMappingResponse>): void => {
            if (response.result && response.data && response.data.content && response.data.content[0]) {
                this.otModalData.row = response.data.content[0];
                this.isSavingNext = false;
                this.isLoading = false;
                this.populateAttributeValues();
            } else {
                this.otModalCloseEvent.next(false);
            }
        });
    }

    validationsForEditModal(): boolean {
        return (this.otModalData.showOrgDropdown ? (isEmpty(this.attributeName)
            || isEmpty(this.selectedOrgGroup)) : isEmpty(this.samlGroupName) );
    }

}
