import {
    Component,
    OnInit,
} from "@angular/core";

// Lodash
import { isEmpty } from "lodash";

// Rxjs
import { Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";

// Vitreus
import {
    ConfirmModalType,
    OtModalService,
} from "@onetrust/vitreus";

// Enums
import { SamlAttributeType } from "enums/settings-sso.enum";
import { SamlOrgMappingActions } from "constants/settings-sso.constant";

// Component
import { SettingsOrgGroupMappingModal } from "settingsComponents/settings-sso/settings-org-group-mapping-modal.component";

// Interfaces
import { IAction } from "interfaces/redux.interface";
import { IIGenericTableConfig } from "interfaces/table-config.interface";
import {
    IAddSamlSettings,
    ISamlAttributeMapping,
    ISamlOrgGroupMapping,
    ISamlOrgMappingResponse,
    ISamlOrgGroupMappingTable,
    ISamlOrgMappingTableSort,
} from "interfaces/settings-sso.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";
import { IDropdownOption } from "interfaces/one-dropdown.interface";

// Services
import { Settings } from "sharedServices/settings.service";
import { Permissions } from "modules/shared/services/helper/permissions.service";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

@Component({
    selector: "settings-org-group-mapping",
    templateUrl: "./settings-org-group-mapping.component.html",
})
export class SettingsOrgGroupMapping implements OnInit {

    AttributeMappingConfigColumns = [
        { columnName: this.translatePipe.transform("AttributeValue"), columnKey: "Key", cellValueKey: "Key" },
        { columnName: this.translatePipe.transform("OrganizationGroup"), columnKey: "Value", cellValueKey: "Value" },
    ];
    showAddAttribute = false;
    showAddGroupName = false;
    isHovering = false;
    menuClass: string;
    clickedItem: string;
    borderClass: string;
    orgMappingRows: ISamlOrgMappingResponse;
    attrGroupName: string;
    pageNumber = 0;
    sortData: ISamlOrgMappingTableSort;
    sortOrderAndKey = "value,asc";
    getOrgMappingParams: ISamlOrgGroupMappingTable;
    searchKey: string;
    noRecordsFoundText: string;
    orgGroupMappingRows: ISamlOrgMappingResponse;
    isLoading = false;
    searchOrgMappingKey: string;
    settingSso: IAddSamlSettings;
    rowActions: IDropdownOption[] = [];
    menuPosition: string;
    private destroy$ = new Subject();

    constructor(
        private otModalService: OtModalService,
        private translatePipe: TranslatePipe,
        private settings: Settings,
        private permission: Permissions,
    ) {}

    ngOnInit() {
        this.getDirectory();
        this.handleOrgMappingActions({type: "GET_PAGE", payload: { page: 0 }});
    }

    ngOnChanges() {
        this.setHovering(false);
    }

    columnTrackBy(column: IIGenericTableConfig): string {
        return column.id;
    }

    ngOnDestroy() {
        this.destroy$.next();
        this.destroy$.complete();
    }

    getDirectory() {
        this.isLoading = true;
        this.settings.getDirectory().then((response: IProtocolResponse<IAddSamlSettings>): void => {
            if (response.result && response.data) {
                this.settingSso = response.data;
                this.setOrgGroupName();
            }
            this.isLoading = false;
        });
    }

    menuOptions(menuIsOpen: boolean, row: ISamlOrgGroupMapping) {
        this.setMenuClass(menuIsOpen);
        this.rowActions = [
            {
              text: this.translatePipe.transform("Edit"),
              action: (): void => { this.openEditAttributeModal(row); },
          },
          {
            text: this.translatePipe.transform("Delete"),
            action: (): void => { this.openDeleteAttributeModal(row); },
          },
      ];
    }

    setMenuClass(event?: any) {
        this.menuPosition = "bottom-right";
        if ((window.innerHeight - 200) / 2 < (event.clientY - 200)) {
            this.menuPosition = "top-right";
        }
    }

    viewDetails(row: ISamlOrgGroupMapping) {
        if (this.permission.canShow("SettingsSSOOrgGroupMappingsSave")) {
            this.handleActions(
                { type: SamlOrgMappingActions.EDIT_ATTRIBUTE, payload: row },
            );
        }
    }

    handleActions(action: IAction) {
        switch (action.type) {
            case SamlOrgMappingActions.EDIT_ATTRIBUTE:
                return this.openEditAttributeModal(action.payload);
            case SamlOrgMappingActions.DELETE_ATTRIBUTE:
                return this.openDeleteAttributeModal(action.payload);
            default:
                return;
        }
    }

    setHovering(hovering: boolean) {
        this.isHovering = hovering;
        if (this.isHovering) {
            this.borderClass = "padding-all-half border-blue";
        } else {
          this.borderClass = "padding-all-half border-default";
        }
    }

    onToggle(type: string, isOpen: boolean) {
        if (isOpen && this.clickedItem !== type) {
            this.clickedItem = type;
        } else if (!isOpen && this.clickedItem === type) {
            this.clickedItem = "";
        }
    }

    pageChange(page: number) {
        if (this.orgGroupMappingRows.number !== page) {
            this.pageNumber = page;
            this.handleActionChange(this.searchOrgMappingKey);
        }
    }

    handleSortChange(sortValue: ISamlOrgMappingTableSort) {
        this.sortOrderAndKey = `${sortValue.sortKey.toLowerCase()},${sortValue.sortOrder}`;
        this.sortData = sortValue;
        this.handleActionChange(this.searchOrgMappingKey);
    }

    onChangeSearchKey(event: string) {
        this.searchOrgMappingKey = event.trim();
        if (isEmpty(this.searchOrgMappingKey)) {
            this.handleOrgMappingActions({ type: "GET_PAGE", payload: { key: "" } });
        } else {
            this.handleOrgMappingActions({ type: "SEARCH", payload: { key: this.searchOrgMappingKey } });
        }
    }

    handleOrgMappingActions(action?: IAction) {
        switch (action.type) {
            case "GET_PAGE":
                this.getOrgMappingParams = {
                    search: SamlAttributeType.Organization,
                    page: action.payload.page,
                    size: 10,
                    sort: this.sortOrderAndKey,
                };
                return this.getOrgMappingAttribute(this.getOrgMappingParams);
            case "SEARCH":
                this.searchOrgMappingKey = action.payload.key;
                this.getOrgMappingParams = {
                    page: action.payload.page,
                    size: 10,
                    sort: this.sortOrderAndKey,
                    attributeValue: action.payload.key,
                };
                return this.getOrgMappingAttribute(this.getOrgMappingParams, true);
        }
    }

    clearSearchKey() {
        this.handleOrgMappingActions({ type: "GET_PAGE", payload: { key: "" } });
    }

    addNewAttribute() {
        this.otModalService.create(
            SettingsOrgGroupMappingModal,
            {
              isComponent: true,
            },
            {
                title: this.translatePipe.transform("AddNew"),
                inputName: this.translatePipe.transform("AttributeValue"),
                showOrgDropdown: true,
                isEditOrgMappingAttr: true,
                addNewAttribute: true,
            },
        ).pipe(
            takeUntil(this.destroy$),
        ).subscribe(
            () => this.handleActionChange(this.searchOrgMappingKey, true),
        );
    }

    openEditAttributeModal(row) {
        this.otModalService.create(
            SettingsOrgGroupMappingModal,
            {
              isComponent: true,
            },
            {
                title: this.translatePipe.transform("Edit"),
                inputName: this.translatePipe.transform("AttributeValue"),
                showOrgDropdown: true,
                isEditOrgMappingAttr: true,
                addNewAttribute: false,
                samlOrgMapping: this.orgGroupMappingRows.content,
                pageNumber: this.pageNumber,
                sort: this.sortOrderAndKey,
                search: this.searchOrgMappingKey,
                row,
            },
        ).pipe(
            takeUntil(this.destroy$),
        ).subscribe(
            () => this.handleActionChange(this.searchOrgMappingKey),
        );
    }

    setOrgGroupName() {
        this.settingSso.AttributeMappings.forEach((attr: ISamlAttributeMapping): void => {
            this.attrGroupName = attr.SamlAttributeTypeId === SamlAttributeType.Organization ? attr.MapKey : "";
        });
    }

    addGroupName() {
        this.setHovering(false);
        this.otModalService.create(
            SettingsOrgGroupMappingModal,
            {
              isComponent: true,
            },
            {
                title: this.translatePipe.transform("Edit"),
                inputName: this.translatePipe.transform("SamlGroupAttributeName"),
                showOrgDropdown: false,
                isEditOrgMappingAttr: true,
                orgName: this.attrGroupName,
                samlConfigValues: this.settingSso,
            },
        ).pipe(
            takeUntil(this.destroy$),
        ).subscribe(
          (res: boolean): void => {
              if (res) {
                  this.getDirectory();
              }
        });
    }

    handleActionChange(search: string, addNewAttr?: boolean) {
        if (search && addNewAttr) {
            this.handleOrgMappingActions({type: "GET_PAGE", payload: { page: 0 }});
        } else if (search) {
            this.handleOrgMappingActions({type: "SEARCH", payload: { page: this.pageNumber, key: this.searchOrgMappingKey }});
        } else {
            this.handleOrgMappingActions({type: "GET_PAGE", payload: { page: this.pageNumber}});
        }
    }

    getOrgMappingAttribute(params: ISamlOrgGroupMappingTable, search?: boolean) {
        this.searchOrgMappingKey = params.attributeValue;
        this.isLoading = true;
        this.settings.getMappingAttribute(params).then(
            (response: IProtocolResponse<ISamlOrgMappingResponse>): void => {
                this.orgGroupMappingRows = response.result && response.data ? response.data : null;
                this.noRecordsFoundText = this.orgGroupMappingRows && !this.orgGroupMappingRows.content.length && search ?
                    this.translatePipe.transform("NoRecordsMatchYourFilter") : this.translatePipe.transform("NoRecordsFound");
                this.isLoading = false;
            },
        );
    }

    openDeleteAttributeModal(row: ISamlOrgGroupMapping) {
        this.otModalService
            .confirm({
                type: ConfirmModalType.DELETE,
                translations: {
                    title: this.translatePipe.transform("DeleteOrgAssgmnt"),
                    desc: this.translatePipe.transform("DeleteOrgAssgmntMsg"),
                    confirm: this.translatePipe.transform("AreYouSureToDeleteAttr"),
                    submit: this.translatePipe.transform("Delete"),
                    cancel: this.translatePipe.transform("Cancel"),
                },
            }).pipe(
                takeUntil(this.destroy$),
            ).subscribe(
                (res: boolean) => {
                    if (res) {
                        this.settings.deleteMappingAttribute(row.Guid).then((response: IProtocolResponse<ISamlOrgGroupMapping>): boolean => {
                            if (response.result) {
                                this.handleActionChange(this.searchOrgMappingKey);
                            }
                            return response.result;
                        });
                    }
                },
            );
    }

}
