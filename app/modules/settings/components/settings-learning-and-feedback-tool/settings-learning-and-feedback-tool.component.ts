// Angular
import {
    Component,
    OnInit,
} from "@angular/core";

// Externa Libraries
import { cloneDeep } from "lodash";

// Services
import { SettingsLearningAndFeedbackService } from "settingsServices/apis/settings-learning-and-feedback-api.service";
import { Userlane } from "sharedModules/services/provider/userlane.service";
import { Wootric } from "sharedModules/services/provider/wootric.service";
import { Permissions } from "modules/shared/services/helper/permissions.service";

// Interfaces
import { IProtocolResponse } from "interfaces/protocol.interface";
import { ILearningAndFeedbackSetting } from "interfaces/settings-learning-and-feedback-tool.interface";

// Constants
import { ToolNames } from "constants/learning-and-feedback-tool.constant";

@Component({
    selector: "learning-and-feedback-tool",
    templateUrl: "./settings-learning-and-feedback-tool.component.html",
    host: {
        class: "flex-full-height height-100",
    },
})
export class SettingsLearningAndFeedbackToolComponent implements OnInit {
    ready = false;
    loading = false;
    currentUserGuidanceSettings: ILearningAndFeedbackSetting;
    updatedUserGuidanceSettings: ILearningAndFeedbackSetting;
    updatedUserFeedbackSettings: ILearningAndFeedbackSetting;
    currentUserFeedbackSettings: ILearningAndFeedbackSetting;

    constructor(
        private settingsLearningAndFeedbackService: SettingsLearningAndFeedbackService,
        private userlane: Userlane,
        private wootric: Wootric,
        private permissions: Permissions,
    ) {}

    ngOnInit() {
        this.getLearningAndFeedbackSettings();
    }

    getLearningAndFeedbackSettings() {
        this.ready = false;
        this.settingsLearningAndFeedbackService
            .getLearningAndFeedbackSettings().then(
                (response: IProtocolResponse<ILearningAndFeedbackSetting[]>) => {
                    if (response.result) {
                        const settingsData = response.data;
                        settingsData.forEach((setting: ILearningAndFeedbackSetting) => {
                            if (setting.toolName === ToolNames.UserFeedback) {
                                this.currentUserFeedbackSettings = setting;
                                this.updatedUserFeedbackSettings = cloneDeep(setting);
                            } else if (setting.toolName === ToolNames.UserGuidance) {
                                this.currentUserGuidanceSettings = setting;
                                this.updatedUserGuidanceSettings = cloneDeep(setting);
                            }
                        });
                    }
                    this.ready = true;
                },
            );
    }

    toggleUserFeedback(feedbackEnable: boolean) {
        this.updatedUserFeedbackSettings.enabled = feedbackEnable;
    }

    toggleUserGuidance(guidanceEnable: boolean) {
        this.updatedUserGuidanceSettings.enabled = guidanceEnable;
    }

    saveSettings() {
        this.loading = true;
        const settingsObject = [
            this.updatedUserGuidanceSettings,
            this.updatedUserFeedbackSettings,
        ];
        this.settingsLearningAndFeedbackService.saveLearningAndFeedbackSettings(settingsObject).then(
            (response: IProtocolResponse<ILearningAndFeedbackSetting[]>) => {
                if (response.result) {
                    if (this.updatedUserFeedbackSettings.enabled !== this.currentUserFeedbackSettings.enabled) {
                        if (this.updatedUserFeedbackSettings.enabled) {
                            this.wootric.initialize();
                        } else {
                            this.wootric.logout();
                        }
                    }
                    if (this.updatedUserGuidanceSettings.enabled !== this.currentUserGuidanceSettings.enabled) {
                        if (this.updatedUserGuidanceSettings.enabled) {
                            this.userlane.initialize();
                        } else {
                            this.userlane.logout();
                        }
                    }
                    this.currentUserFeedbackSettings.enabled = this.updatedUserFeedbackSettings.enabled;
                    this.currentUserGuidanceSettings.enabled = this.updatedUserGuidanceSettings.enabled;
                }
                this.loading = false;
            });
    }

    checkDisable() {
       return this.updatedUserFeedbackSettings.enabled === this.currentUserFeedbackSettings.enabled &&
            this.currentUserGuidanceSettings.enabled === this.updatedUserGuidanceSettings.enabled;
    }
}
