import {
    Component,
    Inject,
    OnInit,
    Input,
} from "@angular/core";
import {
    isObject,
    forEach,
    filter,
    find,
    isArray,
} from "lodash";
import {
    Transition,
    StateDeclaration,
    TransitionService,
    StateService,
} from "@uirouter/core";

// Services
import { SmtpConnectionsTemplateListService } from "settingsComponents/smtp-connections-list/smtp-connections-template-list.service";
import { MessageApiService } from "settingsServices/apis/message-api.service";

// Interfaces
import { IStore } from "interfaces/redux.interface";
import { IStringMap } from "interfaces/generic.interface";
import {
    ISettingsTestEmailModalResolve,
    ISaveMessageTemplate,
    IBindToTemplateScopeResponse,
    IResetEmailData,
    ILanguageActivatorData,
} from "interfaces/email-template.interface";
import { IBreadcrumb } from "interfaces/breadcrumb.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";

// Constants
import { LanguageCodes } from "constants/language-codes.constant";

// Redux
import { StoreToken } from "tokens/redux-store.token";
import { getCurrentOrgId } from "oneRedux/reducers/org.reducer";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

// rxjs
import {
    take,
    takeUntil,
} from "rxjs/operators";
import { Subject } from "rxjs";

// Templates
import { EmailTemplatePreviewModalV2 } from "settingsComponents/email-template-preview-modal-v2/email-template-preview-modal.component";
import { UnsavedChangesConfirmationModal } from "modules/shared/components/unsaved-changes-confirmation-modal/unsaved-changes-confirmation-modal.component";

// Vitreus
import {
    ToastService,
    OtModalService,
    ConfirmModalType,
} from "@onetrust/vitreus";
@Component({
    selector: "email-template-edit",
    templateUrl: "./email-template-edit.component.html",
})

export class EmailTemplateEditComponent implements OnInit {

    @Input() messageTypeId: number = Number(this.stateService.params.messageTypeId);
    @Input() languageId: number = Number(this.stateService.params.languageCode);

    isPageLoading = false;
    languages: ILanguageActivatorData[];
    selectedLanguage: ILanguageActivatorData;
    breadCrumb: IBreadcrumb;
    emailData: IBindToTemplateScopeResponse;
    emailSubject: string;
    emailBody: string;
    toggleOptionValue = false;
    templateId: number;
    private currentOrgGroupId: string;
    private disableSave = false;
    private transitionDeregisterHook: () => any;
    private saveDataBeforeExit = false;
    private destroy$ = new Subject();

    constructor(
        @Inject(StoreToken) private store: IStore,
        private stateService: StateService,
        private translatePipe: TranslatePipe,
        private $transitions: TransitionService,
        private smtpConnectionsTemplateListService: SmtpConnectionsTemplateListService,
        private toastService: ToastService,
        private messageApiService: MessageApiService,
        private otModalService: OtModalService,
    ) {}

    ngOnInit() {
        this.isPageLoading = true;
        this.breadCrumb = {
            stateName: `zen.app.pia.module.settings.messagetemplatev2.list({typeId: 0, languageCode: "en-us"})`,
            text: this.translatePipe.transform("EmailTemplate"),
        };
        this.currentOrgGroupId = getCurrentOrgId(this.store.getState());
        const languagePromise: Promise<void> = this.loadLanguage().then((): void => {
            this.selectedLanguage = find(this.languages, (language: ILanguageActivatorData) => language.Id === this.languageId);
        });
        const loadTemplateDataPromise: Promise<void> = this.loadEditTemplateData(this.messageTypeId, this.languageId);
        Promise.all([languagePromise, loadTemplateDataPromise]).then((): void => {
            this.isPageLoading = false;
        });
        this.setTransitionAwayHook();
    }

    ngOnDestroy() {
        if (this.transitionDeregisterHook) {
            this.transitionDeregisterHook();
        }
    }

    previewEmailTemplate() {
        const modalData: ISettingsTestEmailModalResolve = {
            messageTypeId: this.emailData.messageTypeId,
            modalTitle: this.translatePipe.transform("Preview"),
            submitEmailButtonText: this.translatePipe.transform("Send"),
            languageId: this.selectedLanguage.Id,
        };
        this.otModalService
            .create(
                EmailTemplatePreviewModalV2,
                {
                    isComponent: true,
                },
                modalData,
            );
    }

    toggleStateChangeCallback(overrideValue: boolean) {
        this.toggleOptionValue = !this.toggleOptionValue;
        if (!this.toggleOptionValue && this.emailData.orgGroupId === this.currentOrgGroupId) {
            this.otModalService.confirm({
                type: ConfirmModalType.WARNING,
                translations: {
                    title: this.translatePipe.transform("RestoreDefaultTemplate"),
                    desc: this.translatePipe.transform("RestoreDefaultTemplateHeader"),
                    confirm: this.translatePipe.transform("AreYouSureContinue"),
                    submit: this.translatePipe.transform("Restore"),
                    cancel: this.translatePipe.transform("Cancel"),
                },
            }).pipe(
                takeUntil(this.destroy$))
            .subscribe((data: boolean): boolean => {
                if (!data) {
                    this.toggleOptionValue = !this.toggleOptionValue;
                    return;
                }
                this.restoreAllTemplate(this.emailData.messageTypeId);
            });
        }
        if (this.emailData.orgGroupId !== this.currentOrgGroupId) {
            this.isPageLoading = true;
            this.smtpConnectionsTemplateListService.createCopyofTemplate(this.emailData.messageTypeId)
            .then((response: boolean): boolean => {
                if (response) {
                    this.loadEditTemplateData(this.emailData.messageTypeId, this.emailData.languageId, true);
                } else {
                    this.isPageLoading = false;
                    this.toggleOptionValue = !this.toggleOptionValue;
                }
                return true;
            });
        }
    }

    disableEmailState(disableEmailValue: boolean) {
        this.emailData.disabled = disableEmailValue;
        this.emailData.disabled = disableEmailValue;
        this.smtpConnectionsTemplateListService.disableEmailNotifications(
            this.emailData.messageTypeId,
            disableEmailValue,
        )
        .pipe(take(1))
        .subscribe(
            () => {
                if (this.emailData.disabled) {
                    this.toastService.success(
                        this.translatePipe.transform("Success"),
                        this.translatePipe.transform("DisableEmailNotificationSuccessToaster"),
                    );
                } else {
                    this.toastService.success(
                        this.translatePipe.transform("Success"),
                        this.translatePipe.transform("EnableEmailNotificationSuccess"),
                    );
                }
            },
            () => {
                if (!this.emailData.disabled) {
                    this.toastService.error(
                        this.translatePipe.transform("Error"),
                        this.translatePipe.transform("DisableEmailNotificationFailureToaster"),
                    );
                } else {
                    this.toastService.error(
                        this.translatePipe.transform("Error"),
                        this.translatePipe.transform("EnableEmailNotificationFailure"),
                    );
                }
                this.emailData.disabled = !this.emailData.disabled;
            },
        );
    }

    onEmailLanguageChange(selection: ILanguageActivatorData, oldValue: number) {
        this.selectedLanguage = selection;
        if (this.emailData.subject !== this.emailSubject || this.emailData.bodyText !== this.emailBody) {
            this.otModalService
            .create(
                UnsavedChangesConfirmationModal,
                {
                    isComponent: true,
                    hideClose: true,
                },
                {
                    promiseToResolve: (): Promise<boolean> => this.SaveEmailEditTemplateDetails().then((): boolean => {
                        this.loadEditTemplateData(this.messageTypeId, selection.Id);
                        return true;
                    }),
                    discardCallback: () => {
                        this.loadEditTemplateData(this.messageTypeId, selection.Id);
                    },
                    modalTitle: this.translatePipe.transform("SaveChanges"),
                    descText: this.translatePipe.transform("DefaultExitWithoutSaveTemplateHeader"),
                    saveButton: this.translatePipe.transform("Save"),
                    cancelButtonText: this.translatePipe.transform("DiscardChangesAndContinue"),
                },
            ).pipe(
                takeUntil(this.destroy$),
            ).subscribe(
                (success: boolean) => {
                    if (!success) {
                        this.languageId = oldValue;
                    }
                },
            );
        } else {
            this.loadEditTemplateData(this.messageTypeId, selection.Id);
        }
    }

    saveCancelled() {
        this.stateService.go("zen.app.pia.module.settings.messagetemplatev2.list", { typeId: 0, languageCode: LanguageCodes.English });
    }

    SaveEmailEditTemplateDetails(): Promise<boolean> {
        this.disableSave = true;
        const editTrue = true;
        this.emailData.subject = this.emailSubject;
        this.emailData.bodyText = this.emailBody;
        return this.messageApiService.saveMessageTemplate(this.emailData, editTrue).then((response: IBindToTemplateScopeResponse) => {
            if (response && isObject(response)) {
                this.loadEditTemplateData(response.messageTypeId, response.languageId);
            }
            this.disableSave = false;
            return true;
        });
    }

    handleEmailSubjectChange(value: string) {
        this.emailSubject = value;
    }

    handleEmailBodyChange(value: string) {
        this.emailBody = value;
    }

    resetEmailEditTemplateDetails(id: number) {
        this.otModalService.confirm({
            type: ConfirmModalType.WARNING,
            translations: {
                title: this.translatePipe.transform("ResetContent"),
                desc: this.translatePipe.transform("EmailTemplateContentReset"),
                confirm: this.translatePipe.transform("AreYouSureContinue"),
                submit: this.translatePipe.transform("Reset"),
                cancel: this.translatePipe.transform("Cancel"),
            },
        }).pipe(
            takeUntil(this.destroy$),
        ).subscribe((data: boolean): boolean => {
            if (!data) return;
            this.restoreSelectedTemplate(id);
        });
    }

    private restoreAllTemplate(templateId: number): Promise<boolean> {
        return this.smtpConnectionsTemplateListService.resetAllEmailTemplate(templateId)
        .then((response: boolean): boolean => {
            if (response) {
                this.loadEditTemplateData(templateId, this.emailData.languageId);
            }
            return true;
        });
    }

    private loadLanguage(): Promise<boolean> {
        return this.smtpConnectionsTemplateListService.getEmailLanguages().then((response: ILanguageActivatorData[] | boolean): boolean => {
            if (response && isArray(response)) {
                forEach(response, (selection: ILanguageActivatorData): void => {
                    if (selection.Translated) {
                        selection.Activated = true;
                    }
                });
                this.languages = response.filter((languageData: ILanguageActivatorData) => languageData.Activated);
            }
            return true;
        });
    }

    private loadEditTemplateData(templateId: number, languageId: number, onLanguageChange?: boolean): Promise<void> {
        if (templateId && languageId) {
            return this.smtpConnectionsTemplateListService.getDetailsofParentTemplates(templateId, languageId)
                .then((response: IBindToTemplateScopeResponse): void => {
                    if (response) {
                        this.emailData = {
                            subject: response.subject,
                            bodyText: response.bodyText,
                            templateDetailTranslationKey: response.templateDetailTranslationKey,
                            orgGroupId: response.orgGroupId,
                            messageStateId: response.messageStateId,
                            messageTypeId: response.messageTypeId,
                            parentMessageTypeId: response.parentMessageTypeId,
                            version: response.version,
                            channelId: response.channelId,
                            templateNameTranslationKey: response.templateNameTranslationKey,
                            languageId: response.languageId,
                            disabled: response.disabled,
                        };
                        this.templateId = response.id;
                        this.emailSubject = this.emailData.subject;
                        this.emailBody = this.emailData.bodyText;
                        if (this.emailData.orgGroupId === this.currentOrgGroupId) {
                            this.toggleOptionValue = true;
                        }
                        if (onLanguageChange) {
                            this.isPageLoading = false;
                        }
                    }
                });
        }
    }

    private restoreSelectedTemplate(templateId: number): Promise<boolean> {
        return this.smtpConnectionsTemplateListService.resetEmailTemplate(templateId).then((response: boolean): boolean => {
            if (response) {
                this.loadEditTemplateData(this.emailData.messageTypeId, this.emailData.languageId);
            }
            return true;
        });
    }

    private setTransitionAwayHook() {
        const currentRouteName = this.stateService.current.name;
        this.transitionDeregisterHook = this.$transitions.onExit(
            { from: currentRouteName },
            (transition: Transition, state: StateDeclaration) => {
                if (state.name === currentRouteName && (this.emailData.subject !== this.emailSubject || this.emailData.bodyText !== this.emailBody)) {
                    return this.handleStateChange(transition.to(), transition.params("to"), transition.options);
                }
            }) as any;
    }

    private handleStateChange(toState: IStringMap<any>, toParams: IStringMap<any>, options: IStringMap<any>): boolean {
        if (this.saveDataBeforeExit) return true;
        const callback = () => {
            this.stateService.go(toState, toParams, options);
        };
        this.launchSaveChangesModal(callback);
        return false;
    }

    private launchSaveChangesModal(callback: () => void) {
        this.otModalService
        .create(
            UnsavedChangesConfirmationModal,
            {
                isComponent: true,
                hideClose: true,
            },
            {
                promiseToResolve: (): Promise<boolean> => this.SaveEmailEditTemplateDetails().then((): boolean => {
                    callback();
                    return true;
                }),
                discardCallback: () => {
                    this.saveDataBeforeExit = true;
                    callback();
                    return true;
                },
                modalTitle: this.translatePipe.transform("SaveChanges"),
                descText: this.translatePipe.transform("DefaultExitWithoutSaveTemplateHeader"),
                saveButton: this.translatePipe.transform("Save"),
                cancelButtonText: this.translatePipe.transform("DiscardChangesAndContinue"),
            },
        ).pipe(
            takeUntil(this.destroy$),
        ).subscribe(
            (success: boolean) => {
                if (!success) return;
            },
        );
    }
}
