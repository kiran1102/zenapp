// Angular
import {
    Component,
    Inject,
    OnInit,
    OnDestroy,
} from "@angular/core";
import {
    FormGroup,
    FormBuilder,
    FormArray,
} from "@angular/forms";

// Rxjs
import {
    Subscription,
    Subject,
} from "rxjs";

// 3rd Party
import {
    differenceWith,
    isEqual,
} from "lodash";

// Redux
import { StoreToken } from "tokens/redux-store.token";

// Services
import { LocalizationActionService } from "settingsServices/actions/localization-action.service";
import { TenantTranslationService } from "sharedServices/tenant-translation.service";

// Interfaces
import { IStore } from "interfaces/redux.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";
import { IStringMap } from "interfaces/generic.interface";
import {
    ILocalizationEditModalData,
    ITranslationsPage,
    ITranslation,
    ILanguage,
} from "interfaces/localization.interface";

// Constants
import { TranslationModules } from "constants/translation.constant";
import { IOtModalContent } from "@onetrust/vitreus";

@Component({
    selector: "localization-editor",
    templateUrl: "./localization-edit-modal-v2.component.html",
})

export class LocalizationEditModalV2 implements OnInit, IOtModalContent, OnDestroy {

    otModalData: ILocalizationEditModalData;
    otModalCloseEvent: Subject<boolean>;
    translations: IStringMap<string>;
    subscription: Subscription;
    languageByCode: IStringMap<string>;
    isInitialized = false;
    isLoading = true;
    isSaving = false;
    isSavingTerm = false;
    isSavingTermAndLoadingNext = false;
    shouldReloadPageOnClose = false;
    restoreSingleValue: boolean;
    termKey: string;
    termIndex: number;
    searchKey: string;
    terms: ITranslation[];
    originalTerms: ITranslation[];
    tabIds: IStringMap<string> = TranslationModules;
    currentTabId: string;
    lastTermIndex: number;
    defaultTerm: string;
    localizationEditForm: FormGroup;
    languageLists: FormArray;
    private destroy$ = new Subject();

    constructor(
        @Inject("TenantTranslationService") readonly tenantTranslationService: TenantTranslationService,
        @Inject(StoreToken) private store: IStore,
        private localizationActions: LocalizationActionService,
        private formBuilder: FormBuilder,
    ) { }

    ngOnInit() {
        this.languageByCode = this.toLanguageCodeMap(this.tenantTranslationService.getLanguages());
        this.loadData(this.otModalData);
    }

    ngOnDestroy() {
        this.destroy$.next();
    }

    closeModal() {
        this.otModalCloseEvent.next(false);
    }

    showDefaultValues() {
        this.terms = this.originalTerms.map((term: ITranslation): ITranslation => {
            return { ...term, value: term.defaultValue };
        });
        this.initForm();
    }

    saveTerm(editNextTerm: boolean = false) {
        this.isSaving = true;
        if (editNextTerm) {
            this.isSavingTermAndLoadingNext = true;
        } else {
            this.isSavingTerm = true;
        }
        if (this.restoreSingleValue) {
            this.localizationActions.restoreSingleTranslations(this.termKey);
        }
        this.localizationActions.updateTranslations(this.serializeTerms()).then((response: IProtocolResponse<void>) => {
            if (response.result) {
                this.shouldReloadPageOnClose = true;
                if (editNextTerm) {
                    this.getNextTermToEdit();
                } else {
                    this.otModalCloseEvent.next(true);
                    this.closeModal();
                }
            } else {
                this.resetInitState();
            }
        });
    }

    private resetInitState(resetInitialized: boolean = false) {
        this.isInitialized = resetInitialized ? false : this.isInitialized;
        this.isLoading = false;
        this.isSaving = false;
        this.isSavingTerm = false;
        this.isSavingTermAndLoadingNext = false;
    }

    private getNextTermToEdit() {
        if (this.termIndex < 0) return;
        this.termIndex = this.termIndex + 1;
        this.localizationActions.fetchTranslations({
            page: this.termIndex,
            size: 1,
            searchKey: this.searchKey,
        }, this.currentTabId).then((response: IProtocolResponse<ITranslationsPage>) => {
            if (response.result && response.data && response.data.content && response.data.content[0]) {
                this.terms = response.data.content;
                this.termKey = response.data.content[0].key;
                this.resetInitState();
                this.getTranslationsByKey();
            } else {
                this.closeModal();
            }
        });
    }
    private initForm() {
        this.localizationEditForm = new FormGroup({
            languageList: new FormArray([]),
        });
        this.languageLists = this.localizationEditForm.get("languageList") as FormArray;
        this.createLanguageList();
    }

    private createLanguageList() {
        if (this.terms) {
            this.terms.forEach((language: ITranslation) =>
            this.languageLists.push(this.formBuilder.group(language)));
        }
    }

    private loadData(localizationEditModalData: ILocalizationEditModalData) {
        if (!localizationEditModalData || !localizationEditModalData.termKey) return;
        this.termKey = localizationEditModalData.termKey;
        this.termIndex = localizationEditModalData.termIndex;
        this.searchKey = localizationEditModalData.searchKey;
        this.currentTabId = localizationEditModalData.tabId;
        this.lastTermIndex = localizationEditModalData.lastTermIndex;
        if (!this.isInitialized) {
            this.getTranslationsByKey();
            this.isInitialized = true;
        }
    }

    private getTranslationsByKey() {
        this.isLoading = true;
        this.localizationActions.fetchTranslationsByKey(this.termKey).then((response: IProtocolResponse<ITranslation[]>) => {
            this.originalTerms = response.result ? this.applyLanguageText(response.data) : [];
            this.terms = [...this.originalTerms];
            this.defaultTerm = this.terms[0] ? this.terms[0].defaultValue : "";
            this.isLoading = false;
            this.initForm();
        });
    }

    private toLanguageCodeMap(languages: ILanguage[]): IStringMap<string> {
        const languageCodeMap: IStringMap<string> = {};
        languages.forEach((lang: ILanguage) => {
            languageCodeMap[lang.Code] = lang.Name;
        });
        return languageCodeMap;
    }

    private applyLanguageText(terms: ITranslation[]): ITranslation[] {
        return terms.map((term: ITranslation): ITranslation => {
            return { ...term, language: this.languageByCode[term.languageCode] };
        });
    }

    private serializeTerms(): ITranslation[] {
        const diff: ITranslation[] = differenceWith(this.languageLists.value, this.originalTerms, isEqual);
        return diff.map((term: ITranslation): ITranslation => {
            return {
                languageCode: term.languageCode,
                key: term.key,
                value: term.value,
                defaultValue: term.defaultValue,
                customizedValue: term.customizedValue,
            };
        });
    }
}
