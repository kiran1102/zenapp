// Angular
import {
    Component,
    OnInit,
} from "@angular/core";

// Services
import { SettingsVendorManagementService } from "settingsServices/apis/settings-vendor-management-api.service";

// Interfaces
import { INameValue } from "modules/settings/interfaces/settings-vendor-management.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";
import { SettingNames } from "modules/settings/constants/settings-vendor-management.constant";

@Component({
    selector: "settings-vendor-management",
    templateUrl: "./settings-vendor-management.component.html",
    host: {
        class: "flex-full-height height-100",
    },
})
export class SettingsVendorManagementComponent implements OnInit {

    isLoading = true;
    isSaving = false;
    useCreationEnabled = true;
    responseValue = true;

    constructor(
        private settingsVendorManagementService: SettingsVendorManagementService,
    ) { }

    ngOnInit(): void {
        this.settingsVendorManagementService.fetchVendorSettings().subscribe((response: IProtocolResponse<Array<INameValue<string>>>) => {
            this.setUserCreationValue(response);
        });
    }

    setUserCreationValue(response: IProtocolResponse<Array<INameValue<string>>>) {
        if (response.result && response.data) {
            response.data.forEach((item: INameValue<string>) => {
                if (item.name === SettingNames.UserCreationWhileOnboarding) {
                    this.useCreationEnabled = item.value === "true";
                    this.responseValue = item.value === "true";
                }
            });
        }
        this.isLoading = false;
        this.isSaving = false;
    }

    enableCustomMessage(value: boolean): void {
        this.useCreationEnabled = value;
    }

    saveSetting(): void {
        this.isSaving = true;
        const payload = [{
            name: SettingNames.UserCreationWhileOnboarding,
            value: this.useCreationEnabled.toString(),
        }];
        this.settingsVendorManagementService.saveSettingsVendorManagement(payload).subscribe((response: IProtocolResponse<Array<INameValue<string>>>) => {
            this.setUserCreationValue(response);
        });
    }
}
