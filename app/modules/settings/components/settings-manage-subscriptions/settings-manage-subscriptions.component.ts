// Angular
import {
    Component,
    OnInit,
} from "@angular/core";

// Services
import { SettingsManageSubscriptionsService } from "settingsServices/apis/settings-manage-subscriptions-api.service";

// Interfaces
import { IStringMap } from "interfaces/generic.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";
import { ISubscriptionManager } from "interfaces/settings-manage-subscriptions.interface";

@Component({
    selector: "manage-subscriptions",
    templateUrl: "./settings-manage-subscriptions.component.html",
    host: {
        class: "flex-full-height height-100",
    },
    providers: [SettingsManageSubscriptionsService],
})
export class SettingsManageSubscriptionsComponent implements OnInit {
    url: string;
    adminList: ISubscriptionManager[] = [];
    currentAdmin: ISubscriptionManager;
    selectedAdmin: ISubscriptionManager;
    loading = false;
    ready = false;

    constructor(
        private settingsManageSubscriptionService: SettingsManageSubscriptionsService,
    ) {}

    ngOnInit() {
        this.getSubscriptionPortalUrl();
        this.getAdminsList();
        this.getCurrentAdmin();
    }

    getSubscriptionPortalUrl() {
        this.ready = false;
        this.settingsManageSubscriptionService.getRedirectionDetails().then((response: IStringMap<string>) => {
            if (response) {
                this.url = response.SubscriptionUrl;
            }
        });
    }

    getAdminsList() {
        this.ready = false;
        this.settingsManageSubscriptionService.getAdminsList().then((response: IProtocolResponse<ISubscriptionManager[]>) => {
            if (response.result) {
                this.adminList = response.data;
            }
        });
    }

    getCurrentAdmin() {
        this.ready = false;
        this.settingsManageSubscriptionService.getCurrentAdmin().then((response: IProtocolResponse<ISubscriptionManager>) => {
            if (response.result) {
                this.currentAdmin = response.data;
                this.selectedAdmin = this.currentAdmin;
            }
            this.ready = true;
        });
    }

    selectOption(entry: ISubscriptionManager) {
        this.selectedAdmin = entry;
    }

    cancelSubscriptionSettings() {
        this.selectedAdmin = this.currentAdmin;
    }
    save() {
        this.loading = true;
        const userId = this.selectedAdmin.UserId;
        this.settingsManageSubscriptionService.setSubscriptionManager(userId).then((response: IProtocolResponse<ISubscriptionManager>) => {
            if (response) {
                this.currentAdmin = this.selectedAdmin;
            }
            this.loading = false;
        });
    }
}
