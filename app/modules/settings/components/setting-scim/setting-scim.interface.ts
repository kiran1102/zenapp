export interface IScimSettings {
    name: string;
    value: string;
    id?: string;
}

export interface IScimSettingGenerateCreds {
    clientDescription: string;
    scopes?: string[];
    ipWhitelist: string;
    clientDetailId?: string;
}

export interface IScimSettingGenerateCredsResponse {
    clientId: string;
    secret: string;
    description?: string;
}
