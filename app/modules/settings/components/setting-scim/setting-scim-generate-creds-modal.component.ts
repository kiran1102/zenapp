// Angular
import {
    Component,
    Inject,
} from "@angular/core";

// Vitreus
import {
    IOtModalContent,
    ToastService,
} from "@onetrust/vitreus";

// Utilities
import Utilities from "Utilities";

// 3rd party
import { Subject } from "rxjs";
import { take } from "rxjs/operators";

// Service
import { SettingScimApiService } from "settingsServices/apis/setting-scim-api.service";
import { TranslateService } from "@ngx-translate/core";

// Interface
import {
    IScimSettingGenerateCreds,
    IScimSettingGenerateCredsResponse,
} from "./setting-scim.interface";

@Component({
    selector: "setting-scim-generate-creds-modal",
    templateUrl: "./setting-scim-generate-creds-modal.component.html",
})

export class SettingScimGenerateCredsModalComponent implements IOtModalContent {

    otModalCloseEvent: Subject<boolean>;
    otModalData: IScimSettingGenerateCreds;
    isLoading = true;
    scimCreds: IScimSettingGenerateCredsResponse;
    isCredentialsCopied = false;
    copiedId = false;
    copiedPassword = false;
    translations = {};
    keys = [
        "CopiedClentSecretSuccessfully",
        "Success",
        "CopiedClentIdSuccessfully",
    ];

    constructor(
        private toastService: ToastService,
        private settingScimApiService: SettingScimApiService,
        private translateService: TranslateService,
    ) {}

    ngOnInit() {
        this.translateService
        .stream(this.keys)
        .subscribe((values) => (this.translations = values));
        this.settingScimApiService.generateScimCreds(this.otModalData)
        .pipe((take(1)))
        .subscribe(
            (res: IScimSettingGenerateCredsResponse) => {
                this.scimCreds = res;
                this.isLoading = false;
            },
            (err) => {
                this.isCredentialsCopied = true;
            },
        );
        this.isLoading = false;

    }

    closeModal() {
        this.otModalCloseEvent.next(false);
    }

    copyClientCreds(copyString: string) {
        Utilities.copyToClipboard(copyString);
        if (copyString === this.scimCreds.secret) {
            this.toastService.success(
                this.translations["Success"],
                this.translations["CopiedClentSecretSuccessfully"],
            );
            this.copiedPassword = true;
        } else if (copyString === this.scimCreds.clientId) {
            this.toastService.success(
                this.translations["Success"],
            this.translations["CopiedClentIdSuccessfully"],
            );
            this.copiedId = true;
        }
        this.isCredentialsCopied = this.copiedId && this.copiedPassword;
    }

}
