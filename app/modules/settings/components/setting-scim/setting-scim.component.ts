// Angular
import {
    Component,
    Inject,
} from "@angular/core";
import {
    TransitionService,
    Transition,
    StateDeclaration,
    StateService,
} from "@uirouter/core";

// Service
import { SettingScimApiService } from "settingsServices/apis/setting-scim-api.service";
import { TranslateService } from "@ngx-translate/core";

// Component
import { SettingScimGenerateCredsModalComponent } from "settingsComponents/setting-scim/setting-scim-generate-creds-modal.component";

// redux
import { StoreToken } from "tokens/redux-store.token";
import { getCurrentUser } from "oneRedux/reducers/user.reducer";
import OrgGroupStoreNew from "oneServices/org-group.store";

// interfae
import { IStringMap } from "interfaces/generic.interface";
import { IOrganization } from "interfaces/organization.interface";
import { ITabsNav } from "interfaces/one-tabs-nav.interface";
import {
    IRole,
    IRoleDetails,
} from "interfaces/role.interface";
import { IStore } from "interfaces/redux.interface";
import {
    IScimSettings,
    IScimSettingGenerateCreds,
} from "./setting-scim.interface";

// constant
import { EmptyGuid } from "constants/empty-guid.constant";
import { Regex } from "constants/regex.constant";

// lodash
import {
    has,
    map,
    cloneDeep,
    differenceBy,
    orderBy,
    find,
    isEqual,
} from "lodash";

// rxjs
import {
    take,
    takeUntil,
} from "rxjs/operators";
import { Subject } from "rxjs";

// utilities
import Utilities from "Utilities";

// Vitreus
import {
    ToastService,
    OtModalService,
    ConfirmModalType,
} from "@onetrust/vitreus";

const ScimTabs = {
    GENERAL: "General",
    AUTH: "Auth",
};
@Component({
    selector: "scim-setting",
    templateUrl: "./setting-scim.component.html",
})
export class SettingScimcomponent {
    navTabs = ScimTabs;
    currentTab: string = ScimTabs.GENERAL;
    selectedOrg: string;
    options: IRole[];
    selectedRole: IRole;
    scimBaseUrl: string;
    scimSettings: IScimSettings[];
    cloneScimSettings: IScimSettings[];
    unRegistrationHook: any;
    allowStateChange = false;
    isSaveDisabled = true;
    transitionTo: string;
    clientDescription: string;
    ipWhitelist: string;
    isLoading = false;
    isRolesLoading = false;
    regex = Regex;
    clientCredentials: IScimSettingGenerateCreds;
    cloneClientCredentials: IScimSettingGenerateCreds;
    translations = {};
    tabs: ITabsNav[];
    keys = [
        "Success",
        "Error",
        "ErrorSavingClientCredentials",
        "ClientCredentialsSavedSuccessfully",
        "ErrorSavingSettings",
        "SettingsSavedSuccessfully",
        "UrlCopied",
        "SaveChanges",
        "DefaultExitWithoutSaveTemplateHeader",
        "WouldYouLikeToSaveAndContinue",
        "SaveAndContinue",
        "Discard",
        "General",
        "Authentication",
        "ErrorRetrievingResponses",
    ];
    private destroy$ = new Subject();

    constructor(
        @Inject(StoreToken) public store: IStore,
        @Inject("OrgGroupStoreNewService")
        private orgGroupStoreNew: OrgGroupStoreNew,
        private settingScimApiService: SettingScimApiService,
        private toastService: ToastService,
        private otModalService: OtModalService,
        private $transitions: TransitionService,
        private stateService: StateService,
        private translateService: TranslateService,
    ) {}

    ngOnInit() {
        this.translateService
            .stream(this.keys)
            .subscribe((values) => (this.translations = values));
        this.fetchScimSettings();
        this.setTransitionAwayHook();
        this.getTabs();
    }

    getTabs() {
        this.tabs = [
            {
                id: this.navTabs.GENERAL,
                text: this.translations["General"],
                action: (option: ITabsNav): void => {
                    this.currentTab = option.id;
                },
            },
            {
                id: this.navTabs.AUTH,
                text: this.translations["Authentication"],
                action: (option: ITabsNav): void => {
                    this.currentTab = option.id;
                },
            },
        ];
    }

    fetchScimSettings() {
        this.isLoading = true;
        this.settingScimApiService
            .getScimSettings()
            .pipe(take(1))
            .subscribe((res: IScimSettings[]) => {
                const hasOrgAndRole = !!(find(res, ["name", "DefaultOrganization"])
                    && find(res,  ["name", "DefaultRole"])
                );
                if (!hasOrgAndRole) {
                    const settings = [
                        { name: "DefaultOrganization", value: ""},
                        { name: "DefaultRole", value: ""},
                    ];
                    res = settings.concat(res);
                }
                let roleId;
                res.forEach((settings: IScimSettings) => {
                    if (settings.name === "DefaultOrganization") {
                        this.selectedOrg = settings.value.toLowerCase();
                    } else if (settings.name === "DefaultRole") {
                        roleId = settings.value.toLowerCase();
                    } else if (settings.name === "BaseUrl") {
                        this.scimBaseUrl = `${window.location.protocol}//${window.location.host}/${
                            settings.value
                        }`;
                    }
                });
                res = res.filter((settings) => settings.name !== "BaseUrl");
                this.scimSettings = orderBy(res, "name", "asc");
                this.cloneScimSettings = cloneDeep(res);
                this.isLoading = false;
                this.getRoles(this.selectedOrg, roleId);
            });
    }

    changeTab(option: ITabsNav) {
        this.currentTab = option.id;
        this.currentTab === this.navTabs.AUTH
            ? this.fetchClientCredentials()
            : this.fetchScimSettings();
        this.isSaveDisabled = true;
    }

    setTransitionAwayHook() {
        const currentRouteName = this.stateService.current.name;
        this.unRegistrationHook = this.$transitions.onExit(
            { from: currentRouteName },
            (transition: Transition, state: StateDeclaration) => {
                if (
                    state.name === currentRouteName &&
                    (differenceBy(
                        this.scimSettings,
                        this.cloneScimSettings,
                        "value",
                    ).length > 0 ||
                        !isEqual(
                            this.cloneClientCredentials,
                            this.clientCredentials,
                        ))
                ) {
                    if (this.allowStateChange) return true;
                    this.otModalService
                        .confirm({
                            type: ConfirmModalType.WARNING,
                            translations: {
                                title: this.translations["SaveChanges"],
                                desc: this.translations[
                                    "DefaultExitWithoutSaveTemplateHeader"
                                ],
                                confirm: this.translations[
                                    "WouldYouLikeToSaveAndContinue"
                                ],
                                submit: this.translations["SaveAndContinue"],
                                cancel: this.translations["Discard"],
                            },
                            hideClose: false,
                        })
                        .pipe(takeUntil(this.destroy$))
                        .subscribe((response: boolean | null) => {
                            this.transitionTo = transition.to().name;
                            if (response) {
                                this.saveScimSettings(this.currentTab, true);
                            } else if (!response || response !== null) {
                                this.allowStateChange = true;
                                this.stateService.go(this.transitionTo);
                            }
                        });
                    return false;
                } else {
                    return true;
                }
            },
        );
    }

    getRoles(orgId: string = "", roleId?: string) {
        this.isRolesLoading = true;
        this.settingScimApiService
            .listRoles(true, orgId)
            .pipe(take(1))
            .subscribe((res: IRole[]) => {
                const role = this.normalizeRolePageable(res);
                this.options = this.settingScimApiService.translateRoleNamesAndDescription(role);
                if (roleId) {
                    this.getSelectedRole(roleId);
                } else {
                    this.selectedRole = orgId && this.selectedRole && find(this.options, ["Id", this.selectedRole.Id]) ? this.selectedRole : null;
                    this.isSaveDisabled = this.selectedRole === null || this.validateFields();
                }
                this.isRolesLoading = false;
            },
            (err) => {
                this.isRolesLoading = false;
                this.getRoles();
            },
        );
    }

    selectValue(type: string, value: any, index: number) {
        switch (type) {
            case "DefaultOrganization":
                this.scimSettings[index].value = value;
                this.selectedOrg = value;
                this.getRoles(this.selectedOrg);
                break;
            case "DefaultRole":
                this.scimSettings[index].value = value.Id;
                this.selectedRole = value;
                break;
        }
        this.isSaveDisabled = this.validateFields();
        return this.scimSettings;
    }

    validateFields(): boolean {
        const hasDifference = differenceBy(
            this.scimSettings,
            this.cloneScimSettings,
            "value",
        );
        return !(hasDifference.length > 0) ||
            !!find(hasDifference, ["value", undefined]);
    }

    getSelectedRole(roleId: string) {
        this.options.forEach((role) => {
            if (role.Id === roleId) {
                this.selectedRole = role;
            }
            return this.selectedRole;
        });
    }

    normalizeRolePageable(data: IRole[]): IRole[] {
        const tenantOrgTable: IStringMap<IOrganization | string> = this
            .orgGroupStoreNew.tenantOrgTable;
        const orgTable: IStringMap<IOrganization | string> = this
            .orgGroupStoreNew.orgTable;
        const list: IRoleDetails[] = map(
            data,
            (role: IRoleDetails): IRoleDetails => {
                return this.normalizeRole(role, tenantOrgTable, orgTable);
            },
        );
        return list;
    }

    normalizeRole(
        role: IRoleDetails,
        tenantOrgTable: IStringMap<IOrganization | string> = this
            .orgGroupStoreNew.tenantOrgTable,
        orgTable: IStringMap<IOrganization | string> = this.orgGroupStoreNew
            .orgTable,
    ): IRoleDetails {
        if (!role.orgGroupId || role.orgGroupId === EmptyGuid) {
            role.orgGroupId = tenantOrgTable.rootId as string;
            role.IsSeeded = true;
        }
        if (role.orgGroupId && tenantOrgTable[role.orgGroupId]) {
            role.OrgGroupName = (tenantOrgTable[
                role.orgGroupId
            ] as IOrganization).name;
        }
        role.CanEdit = role.CanDelete = role.CanReplace =
            !role.IsSeeded && has(orgTable, role.orgGroupId);
        return role;
    }

    copyScimBaseUrl() {
        Utilities.copyToClipboard(this.scimBaseUrl);
        this.toastService.success(
            this.translations["Success"],
            this.translations["UrlCopied"],
        );
    }

    ipWhiteListChange(ip: string) {
        this.clientCredentials.ipWhitelist = ip ? ip : "";
        this.isSaveDisabled = !this.clientCredentials.clientDetailId;
    }

    inputChange(des: string) {
        this.clientCredentials.clientDescription = des ? des : "";
        this.isSaveDisabled = !this.clientCredentials.clientDetailId;
    }

    generateCredentialModal() {
        this.otModalService
            .create(
                SettingScimGenerateCredsModalComponent,
                {
                    isComponent: true,
                },
                {
                    clientDescription: this.clientCredentials.clientDescription
                        ? this.clientCredentials.clientDescription
                        : "",
                    ipWhitelist: this.clientCredentials.ipWhitelist
                        ? this.clientCredentials.ipWhitelist
                        : "",
                    scopes: ["SCIM"],
                },
            )
            .pipe(takeUntil(this.destroy$))
            .subscribe((res) => {
                this.fetchClientCredentials();
            });
    }

    pageRedirection(redirect: boolean, translationKey: string) {
        if (redirect) {
            this.allowStateChange = true;
            this.stateService.go(this.transitionTo);
        }
        this.toastService.success(
            this.translations["Success"],
            this.translations[translationKey],
        );
        this.isSaveDisabled = true;
    }

    errorMessage(translateKey: string) {
        this.toastService.error(
            this.translations["Error"],
            this.translations[translateKey],
        );
    }

    saveScimSettings(currentTab, pageRedirect?: boolean) {
        const isEdit = !!(this.scimSettings[0] && this.scimSettings[0].id);
        if (currentTab === this.navTabs.GENERAL) {
            this.settingScimApiService
                .saveScimSettings(this.scimSettings, isEdit)
                .pipe(take(1))
                .subscribe(
                    (res) => {
                        this.pageRedirection(true, "SettingsSavedSuccessfully");
                        this.fetchScimSettings();
                    },
                    (err) => {
                        this.errorMessage(
                            "ErrorSavingSettings",
                        );
                    },
                );
        } else if (currentTab === this.navTabs.AUTH) {
            this.settingScimApiService
                .updateClientCredentials(this.clientCredentials)
                .pipe(take(1))
                .subscribe(
                    (res) => {
                        this.isSaveDisabled = true;
                        this.pageRedirection(
                            true,
                            "ClientCredentialsSavedSuccessfully",
                        );
                        this.fetchClientCredentials();
                    },
                    (err) => {
                        this.errorMessage(
                            "ErrorSavingClientCredentials",
                        );
                    },
                );
        }
    }

    fetchClientCredentials() {
        this.isLoading = true;
        this.settingScimApiService
            .getCredentials()
            .pipe(take(1))
            .subscribe(
                (res: IScimSettingGenerateCreds) => {
                    this.clientCredentials = res;
                    this.cloneClientCredentials = cloneDeep(res);
                    this.isLoading = false;
                },
                (err) => {
                    this.errorMessage("ErrorRetrievingResponses");
                    this.isLoading = false;
                },
            );
    }

    cancelSamlSettings(currentTab: string) {
        currentTab === this.navTabs.GENERAL
            ? this.fetchScimSettings()
            : this.fetchClientCredentials();
        this.isSaveDisabled = true;
    }

    ngOnDestroy() {
        this.destroy$.next();
        this.destroy$.complete();
        this.unRegistrationHook();
    }
}
