// 3rd party
import {
    Component,
    OnInit,
} from "@angular/core";

import { cloneDeep } from "lodash";

// Services
import { InventoryService } from "inventorySharedModule/shared/services/inventory-api.service";
import { InventoryAdapterV2Service } from "inventoryModule/services/adapter/inventory-adapter-v2.service";
import { Permissions } from "modules/shared/services/helper/permissions.service";

// Interface
import { IProtocolResponse } from "interfaces/protocol.interface";
import {
    IInventorySetting,
    IInventorySettingList,
} from "interfaces/inventory.interface";
import { ILabelValue } from "interfaces/generic.interface";

// Constant
import {
    InventorySettingsDetails,
    InventorySettings,
    InventoryStatusDescTranslationKeys,
} from "constants/inventory.constant";

// Enums
import {
    InventorySettingValueTypes,
    InventoryStatusOptions,
} from "enums/inventory.enum";

@Component({
    selector: "settings-inventory",
    templateUrl: "./settings-inventory.component.html",
})
export class SettingsInventoryComponent implements OnInit {
    isLoading = true;
    hasChange = false;
    inventorySettings: IInventorySetting[] = [];
    inventorySettingNames = InventorySettings;
    isSaving = false;
    settingDetails = InventorySettingsDetails;
    inventorySettingValueTypes = InventorySettingValueTypes;
    inventoryStatusDescTranslationKeys = InventoryStatusDescTranslationKeys;

    constructor(
        private inventoryService: InventoryService,
        private inventoryAdapterV2Service: InventoryAdapterV2Service,
        private permissions: Permissions,
    ) {}

    ngOnInit() {
        this.inventoryService.getInventorySettings().then((response: IProtocolResponse<IInventorySettingList>) => {
            if (response.result && response.data && response.data.data && response.data.data.length) {
                this.inventorySettings = this.serializeSettings(response.data.data);
            }
            this.isLoading = false;
        });
    }

    saveSetting() {
        this.isSaving = true;
        const updatedSettings = this.deserializeSettings(cloneDeep(this.inventorySettings));
        this.inventoryService.updateInventorySettings(updatedSettings).then(() => {
            this.isSaving = false;
            this.hasChange = false;
        });
    }

    updateSetting(index: number, value: string, selectedOption?: ILabelValue<any>) {
        this.inventorySettings[index].value = value;
        if (selectedOption) this.inventorySettings[index].selectedOption = selectedOption;
        this.hasChange = true;
    }

    getSettingName(index: number, setting: IInventorySetting): string {
        return setting.name;
    }

    private serializeSettings(settings: IInventorySetting[]): IInventorySetting[] {
        return this.filterSettings(settings).map((setting: IInventorySetting): IInventorySetting => {
            if (setting.valueType === this.inventorySettingValueTypes.Boolean) {
                setting.value = setting.value === "true";
            }
            if (setting.name === this.inventorySettingNames.AssessmentDefaultInventoryStatus) {
                this.inventoryAdapterV2Service.formatInventoryStatusOptions().then((options: Array<ILabelValue<InventoryStatusOptions>>) => {
                    setting.options = options;
                    setting.selectedOption = setting.options.find((option: ILabelValue<InventoryStatusOptions>): boolean => option.value === setting.value);
                });
            }
            return setting;
        });
    }

    private filterSettings(settings: IInventorySetting[]): IInventorySetting[] {
        if (this.permissions.canShow("DataMappingInventorySelectionAccessControlOverride")) {
            return settings;
        } else {
            return settings.filter((setting: IInventorySetting) => setting.name !== InventorySettings.InventoryAccessControlOverrideInventorySelection);
        }
    }

    private deserializeSettings(settings: IInventorySetting[]): IInventorySetting[] {
        return settings.map((setting: IInventorySetting): IInventorySetting => {
            if (setting.valueType === this.inventorySettingValueTypes.Boolean) {
                setting.value = String(setting.value);
            }
            return setting;
        });
    }
}
