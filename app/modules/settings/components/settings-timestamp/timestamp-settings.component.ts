// Angular
import {
    Component,
} from "@angular/core";

// External Libraries
import { find } from "lodash";

// Services
import { TimeStamp } from "oneServices/time-stamp.service";

// Interfaces
import { IKeyValue } from "interfaces/generic.interface";
import { ILabelValue } from "interfaces/generic.interface";

// Constants
import { DateTimeFormats } from "constants/date-time.constant";

@Component({
    selector: "timestamp-settings",
    templateUrl: "./timestamp-settings.component.html",
    host: {
        class: "flex-full-height height-100",
    },
})

export class TimestampSettingsComponent {

    dateFormats: Array<ILabelValue<string>>;
    timeFormats: Array<ILabelValue<string>>;
    currentDateFormat: string;
    currentTimeFormat: string;
    isLoading = false;
    selectedDateOption: ILabelValue<string>;
    selectedTimeOption: ILabelValue<string>;

    constructor(private TimeStamp: TimeStamp) {}

    ngOnInit() {
        this.dateFormats = this.TimeStamp.dateFormats;
        this.timeFormats = this.TimeStamp.timeFormats;
        this.currentTimeFormat = this.TimeStamp.currentTimeFormatting;
        this.currentDateFormat = this.TimeStamp.currentDateFormatting;
        this.selectedDateOption = find(this.dateFormats, (dateEntry) => dateEntry.value === this.currentDateFormat);
        this.selectedTimeOption = find(this.timeFormats, (timeEntry) => timeEntry.value === this.currentTimeFormat);
    }

    isButtonDisabled(): boolean {
        return !this.currentDateFormat || !this.currentTimeFormat;
    }

    selectOption(entry: ILabelValue<string>, format: string) {
        if (format === DateTimeFormats.Date) {
            this.selectedDateOption = entry;
            this.currentDateFormat = this.selectedDateOption.value;
        } else if (format === DateTimeFormats.Time) {
            this.selectedTimeOption = entry;
            this.currentTimeFormat = this.selectedTimeOption.value;
        }
    }

    saveDateTime() {
        this.isLoading = true;
        const currentDateTimeFormats: Array<IKeyValue<string>> = [
            {
                key: DateTimeFormats.Date,
                value: this.currentDateFormat,
            },
            {
                key: DateTimeFormats.Time,
                value: this.currentTimeFormat,
            },
        ];
        this.TimeStamp.updateTimeStamp(currentDateTimeFormats).then(
            (): void => {
                this.isLoading = false;
            },
        );
    }
}
