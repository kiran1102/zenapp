// 3rd Party
import {
    isObject,
    isArray,
    merge,
    isEqual,
    forEach,
} from "lodash";
import { Transition } from "@uirouter/core";

// Constants
import {
    DEFAULT_BACKGROUND_COLOR,
    DEFAULT_TEXT_COLOR,
} from "constants/branding.constant";
import { LanguageCodes } from "constants/language-codes.constant";

import { getCurrentUser } from "oneRedux/reducers/user.reducer";

// Interfaces
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import { Permissions } from "modules/shared/services/helper/permissions.service";
import { IStringMap } from "interfaces/generic.interface";
import {
    ITinymceOptions,
    ITemplate,
    IButtonColors,
    IButtonColorStyle,
    IMinicolorOptions,
    ISettingsTestEmailModalResolve,
    IBindToTemplateScopeResponse,
    IMessageTemplate,
    IMessageTemplateRequestParams,
    ISaveMessageTemplate,
    IBrandingLanguageDataContent,
    IUpdateBrandingLanguage,
    IMessageTemplateData,
} from "interfaces/email-template.interface";
import { IStore } from "interfaces/redux.interface";
import { ISmtpSetting, ILanguage } from "interfaces/smtp-settings.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";
import { IUser } from "interfaces/user.interface";

// Redux
import { getSmtpSetting } from "oneRedux/reducers/setting.reducer";

// Services
import { ModalService } from "sharedServices/modal.service";
import { SettingSmtpActionService } from "settingsServices/actions/setting-smtp-action.service";
import { SettingEmailApiService } from "settingsServices/apis/setting-email-api.service";
import { TenantTranslationService } from "sharedServices/tenant-translation.service";
import { MessageApiService } from "settingsServices/apis/message-api.service";

declare const OneConfirm: any;

class EmailTemplateController implements ng.IComponentController {

    static $inject: string[] = [
        "$rootScope",
        "$q",
        "Permissions",
        "$stateParams",
        "ModalService",
        "SettingSmtpAction",
        "store",
        "SettingEmailAction",
        "$transitions",
        "MessageApiService",
    ];

    public isTemplateSaveDisabled = false;
    public ready = false;
    public canSave = false;
    public permissions = {
        ButtonColorEditor: this.permissionsService.canShow("ButtonColorEditor"),
        ButtonColorEditorEdit: this.permissionsService.canShow("ButtonColorEditorEdit"),
    };
    public tinymceOptions: ITinymceOptions;
    public template: IBindToTemplateScopeResponse;
    public buttonColors: IButtonColors;
    public buttonColorStyle: IButtonColorStyle;
    public buttonColorSaveInProgress = false;
    private disableSave = false;
    private defaultButtonBgColor: string;
    private defaultButtonFgColor: string;
    private smtpSetting: ISmtpSetting;
    private minicolorOptions: IMinicolorOptions;
    private id: number;
    private transitionDeregisterHook: any;
    private currentUser: IUser;

    constructor(
        private $rootScope: IExtendedRootScopeService,
        private $q: ng.IQService,
        private permissionsService: Permissions,
        private $stateParams: ng.ui.IStateParamsService,
        private modalService: ModalService,
        private SettingSmtpAction: SettingSmtpActionService,
        private store: IStore,
        private SettingEmailAction: SettingEmailApiService,
        private $transitions: Transition,
        private messageApiService: MessageApiService,
    ) { }

    public $onInit(): void {

        this.ready = false;

        (require as any).ensure([], (require) => {
            const m = require("tinymce/tinymce");
            (window as any).tinyMCE = m;
            (window as any).tinymce = m;
            require("tinymce/themes/modern/theme");
            require("tinymce/plugins/paste");
            require("tinymce/plugins/link");
            require("tinymce/plugins/autoresize");
            require("tinymce/plugins/image");
            require("tinymce/plugins/code");
            require("tinymce/plugins/textcolor");
            require("tinymce/plugins/colorpicker");
            require("tinymce/plugins/fullpage");

            this.tinymceOptions = {
                entity_encoding: "raw",
                fullpage_default_title: "",
                fullpage_hide_in_source_view: false,
                menu: {
                    edit: { title: "Edit", items: "undo redo | cut copy paste pastetext | selectall" },
                    insert: { title: "Insert", items: "link image" },
                    view: { title: "View", items: "visualaid" },
                    format: { title: "Format", items: "bold italic underline strikethrough superscript subscript | formats | removeformat" },
                    tools: { title: "Tools", items: "code" },
                },
                plugins: "autoresize link image textcolor colorpicker fullpage code",
                textcolor_cols: "5",
                textcolor_rows: "5",
                toolbar: false,
            };
            this.template = {
                subject: "",
                bodyText: "",
                messageStateId: undefined,
                messageTypeId: 1,
                version: undefined,
                parentMessageTypeId: undefined,
            };
            this.minicolorOptions = {
                position: "bottom right",
                defaultValue: "",
                animationSpeed: 50,
                animationEasing: "swing",
                change: null,
                changeDelay: 0,
                control: "hue",
                hide: null,
                hideSpeed: 100,
                inline: false,
                letterCase: "lowercase",
                opacity: false,
                show: null,
                showSpeed: 100,
            };
            this.buttonColors = { buttonBgColor: null, buttonFgColor: null };
            this.defaultButtonBgColor = DEFAULT_BACKGROUND_COLOR;
            this.defaultButtonFgColor = DEFAULT_TEXT_COLOR;
            const promiseArr = [this.getMessageTemplate(), this.SettingSmtpAction.fetchSmtpSetting()];
            this.$q.all(promiseArr as any).then((res: any): void => {
                const buttonColorFetchSuccess = res[1];
                if (buttonColorFetchSuccess) {
                    this.smtpSetting = getSmtpSetting(this.store.getState());
                    this.buttonColors.buttonBgColor =
                        this.smtpSetting.htmlButtonBgColor ||
                        this.defaultButtonBgColor;
                    this.buttonColors.buttonFgColor =
                        this.smtpSetting.htmlButtonFgColor ||
                        this.defaultButtonFgColor;
                    this.buttonColorStyle = {
                        "background-color": this.buttonColors.buttonBgColor,
                        "border-color": this.buttonColors.buttonBgColor,
                        "color": this.buttonColors.buttonFgColor,
                        "font-weight": "bold",
                    };
                } else {
                    this.buttonColors.buttonBgColor = this.defaultButtonBgColor;
                    this.buttonColors.buttonFgColor = this.defaultButtonFgColor;
                }
                this.ready = true;
            });

        }, "tinymce");
        this.currentUser = getCurrentUser(this.store.getState());
    }

    public unRegistrationHook(): void {
        this.transitionDeregisterHook = this.$transitions.onExit({ from: "zen.app.pia.module.settings.messagetemplatev2" }, (transition, state) => {
            if (state.name === "zen.app.pia.module.settings.messagetemplatev2"
                && (!isEqual(this.buttonColors.buttonBgColor, this.smtpSetting.htmlButtonBgColor)
                    || !isEqual(this.buttonColors.buttonFgColor, this.smtpSetting.htmlButtonFgColor))) {
                return OneConfirm("", this.$rootScope.t("UnsavedChangesWouldYouLikeToSave")).then((result: boolean): boolean => {
                    if (result) {
                        this.SettingEmailAction.saveEmailButtonColor(this.buttonColors.buttonBgColor, this.buttonColors.buttonFgColor);
                    }
                    return true; // if cancel is clicked, just continue with transition
                });
            }
            return true;
        });
    }

    public getDefaultTemplate(): void {
        const params: IMessageTemplateRequestParams = this.getMessageTemplateRequestParams();
        params.sort = "Version,ASC";
        this.messageApiService.getMessageTemplate(params)
        .then((res) => {
            this.processResponse(res);
        });
    }

    public processResponse(response: IMessageTemplateData): void {
        if (response &&
            isArray(response.content) &&
            response.content.length === 1) {
            this.bindToTemplateScope(response.content[0]);
        }
    }

    public bindToTemplateScope(responseContent: IBindToTemplateScopeResponse): void {
        responseContent = {
            bodyText: responseContent.bodyText,
            channelId: responseContent.channelId,
            guid: responseContent.guid,
            languageId: responseContent.languageId,
            messageStateId: responseContent.messageStateId,
            messageTypeId: responseContent.messageTypeId,
            orgGroupId: responseContent.orgGroupId,
            parentMessageTypeId: responseContent.parentMessageTypeId,
            subject: responseContent.subject,
            version: responseContent.version,
        };
        this.template = merge(this.template, responseContent);
        // Force 'en-us'
        this.template.languageId = 1;
    }

    public getMessageTemplateRequestParams(): IMessageTemplateRequestParams {
        return {
            languageCode: this.$stateParams.languageCode,
            typeId: this.$stateParams.typeId,
            messageStateId: 20,
            page: 0,
            size: 1,
            sort: "Version,DESC",
        };
    }

    public getMessageTemplate(): Promise<void> {
        return this.messageApiService.getMessageTemplate(this.getMessageTemplateRequestParams())
            .then((res: IMessageTemplateData) => {
                return this.processResponse(res);
            });
    }

    public editTemplate(): void {
        this.canSave = true;
    }

    public openTestEmailModal(): void {
        const modalData: ISettingsTestEmailModalResolve = {
            messageTypeId: this.template.messageTypeId,
            modalTitle: this.$rootScope.t("TestEmailTemplate"),
            submitEmailButtonText: this.$rootScope.t("SendTestTemplate"),
        };
        this.modalService.setModalData(modalData);
        this.modalService.openModal("EmailTemplatePreviewModal");
    }

    public restoreEmailButtonColor(): void {
        this.buttonColors.buttonBgColor = this.defaultButtonBgColor;
        this.buttonColors.buttonFgColor = this.defaultButtonFgColor;
    }

    public saveEmailButtonColor(): void {
        this.buttonColorSaveInProgress = true;
        this.SettingEmailAction.saveEmailButtonColor(this.buttonColors.buttonBgColor, this.buttonColors.buttonFgColor)
            .then((res: boolean) => {
                if (res) {
                    this.smtpSetting.htmlButtonBgColor = this.buttonColors.buttonBgColor;
                    this.smtpSetting.htmlButtonFgColor = this.buttonColors.buttonFgColor;
                }
                this.buttonColorSaveInProgress = false;
            });
    }

    public updateButtonColors(): void {
        this.buttonColorStyle = {
            "background-color": this.buttonColors.buttonBgColor,
            "border-color": this.buttonColors.buttonBgColor,
            "color": this.buttonColors.buttonFgColor,
            "font-weight": "bold",
        };
    }

    public saveTemplate(): void {
        this.isTemplateSaveDisabled = true;
        const editTrue = false;
        this.template.orgGroupId = this.currentUser.OrgGroupId || this.template.orgGroupId;
        this.messageApiService.saveMessageTemplate(this.template, editTrue).then((response: IBindToTemplateScopeResponse) => {
            if (response && isObject(response)) {
                this.bindToTemplateScope(response);
            }
            this.isTemplateSaveDisabled = false;
        });
    }

    // un-register the created hook to avoid duplicate confirmation
    public $onDestroy(): void {
        if (this.transitionDeregisterHook) {
            this.transitionDeregisterHook();
        }
    }
}

export const settingsEmailTemplate: ng.IComponentOptions = {
    controller: EmailTemplateController,
    template: `
    <div class="flex-full-height height-100 background-white">
        <div ng-if="$ctrl.Permissions.canShow('SettingsCustomEmailTemplate')"
            class="settings-body-header">
            {{::$root.t('EmailTemplate')}}
        </div>
        <loading
            class="flex-full-height justify-center"
            ng-if="!$ctrl.ready"
            text="{{::$root.t('EmailTemplate')}}">
        </loading>
        <div
            class="padding-left-right-5 padding-top-bottom-3 overflow-y-auto"
            ng-if="$ctrl.ready"
            >
            <fieldset>
                <form
                    class="variable-messagetemplate settings-input-section"
                    name="messagetemplateForm"
                    ot-auto-id="EmailSettingsTemplateForm">
                    <textarea
                        ui-tinymce="$ctrl.tinymceOptions"
                        ng-model="$ctrl.template.bodyText"
                        required="required"
                        ot-auto-id="EmailSettingsTemplateBodyText">
                    </textarea>
                </form>
            </fieldset>
            <div class="settings-submit-section margin-bottom-4">
                <one-button
                    identifier="TemplateRestoreButton"
                    button-click="$ctrl.getDefaultTemplate()"
                    uib-tooltip="{{::$root.t('RestoreToDefault')}}"
                    text="{{::$root.t('RestoreDefault')}}"
                ></one-button>
                <one-button
                    identifier="TemplateEditButton"
                    button-click="$ctrl.editTemplate()"
                    uib-tooltip="{{::$root.t('EditTemplate')}}"
                    ng-if="$ctrl.canSave"
                    text="{{::$root.t('EditTemplate')}}"
                ></one-button>
                <one-button
                    identifier="TemplateTestButton"
                    button-click="$ctrl.openTestEmailModal()"
                    uib-tooltip="{{::$root.t('TestCustomEmailTemplate')}}"
                    ng-disabled="messagetemplateForm.$invalid"
                    text="{{::$root.t('TestSampleEmail')}}"
                ></one-button>
                <one-button
                    identifier="TemplateSaveButton"
                    type="primary"
                    button-click="$ctrl.saveTemplate()"
                    uib-tooltip="{{::$root.t('SaveCustomEmailTemplate')}}"
                    ng-disabled="messagetemplateForm.$invalid || $ctrl.isTemplateSaveDisabled"
                    text="{{::$root.t('SaveTemplate')}}"
                ></one-button>
            </div>

            <div class="column-horizontal-center" ng-if="$ctrl.permissions.ButtonColorEditor">
                <div class="row-horizontal-center margin-bottom-2">
                    <div class="column-vertical-center margin-right-2">
                        <label>{{::$root.t("ButtonBackgroundColor")}}</label>
                        <input
                            class="one-input"
                            type="text"
                            minicolors="$ctrl.minicolorsSettings"
                            ng-model="$ctrl.buttonColors.buttonBgColor"
                            ng-change="$ctrl.updateButtonColors($event)"
                            ng-disabled="!$ctrl.permissions.ButtonColorEditorEdit"
                        />
                        <label class="margin-top-1">{{::$root.t("ButtonTextColor")}}</label>
                        <input
                            class="one-input"
                            type="text"
                            minicolors="$ctrl.minicolorsSettings"
                            ng-model="$ctrl.buttonColors.buttonFgColor"
                            ng-change="$ctrl.updateButtonColors()"
                            ng-disabled="!$ctrl.permissions.ButtonColorEditorEdit"
                            ot-auto-id="EmailSettingsTemplateFormUpdateButton"
                        />
                    </div>
                    <div class="column-horizontal-vertical-center">{{::$root.t("ThisIsASample")}}
                        <one-button
                            identifier="PreviewEmailButtonButton"
                            text="{{::$root.t('ButtonPreview')}}"
                            button-style="$ctrl.buttonColorStyle">
                        </one-button>
                    </div>
                </div>
                <div class="row-horizontal-center" ng-if="$ctrl.permissions.ButtonColorEditorEdit">
                    <one-button
                        class="margin-right-half"
                        identifier="RestoreEmailButtonColorButton"
                        button-click="$ctrl.restoreEmailButtonColor()"
                        uib-tooltip="{{::$root.t('RestoreButtonBrandingToDefault')}}"
                        text="{{::$root.t('RestoreDefault')}}">
                    </one-button>
                    <one-button
                        identifier="SaveEmailButtonColorButton"
                        type="primary"
                        button-click="$ctrl.saveEmailButtonColor()"
                        uib-tooltip="{{::$root.t('SaveCustomButtonBranding')}}"
                        ng-disabled="$ctrl.buttonColorSaveInProgress"
                        text="{{::$root.t('SaveButtonBranding')}}">
                    </one-button>
                </div>
            </div>
        </div>
    </div>
` };
