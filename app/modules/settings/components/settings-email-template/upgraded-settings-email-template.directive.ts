import { Directive, ElementRef, Injector, Input, Output } from "@angular/core";
import { UpgradeComponent } from "@angular/upgrade/static";

@Directive({ selector: "upgraded-settings-email-template" })
export class UpgradedSettingsEmailTemplateDirective extends UpgradeComponent {
    constructor(elementRef: ElementRef, injector: Injector) {
        super("settingsEmailTemplate", elementRef, injector);
    }
}
