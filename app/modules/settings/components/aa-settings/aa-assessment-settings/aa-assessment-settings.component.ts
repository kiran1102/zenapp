// Angular
import {
    Component,
    OnInit,
    OnDestroy,
} from "@angular/core";

// 3rd Party
import { TransitionService } from "@uirouter/core";

// Rxjs
import { Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";

// Vitreus
import {
    OtModalService,
    ModalSize,
} from "@onetrust/vitreus";

// Interfaces
import { IProtocolResponse } from "interfaces/protocol.interface";
import { ISspTemplatesRestrictView } from "interfaces/self-service-portal/ssp.interface";

// Services
import { SettingsAAActionService } from "settingsServices/actions/settings-assessment-action.service";
import { NotificationService } from "modules/shared/services/provider/notification.service";

// Pipes
import { TranslationsService } from "@onetrust/common";

// Components
import { UnsavedChangesConfirmationModal } from "sharedModules/components/unsaved-changes-confirmation-modal/unsaved-changes-confirmation-modal.component";

@Component({
    selector: "aa-assessment-settings",
    templateUrl: "./aa-assessment-settings.component.html",
})
export class AAAssessmentSettingsComponent implements OnInit, OnDestroy {
    bulkSendIR = false;
    loading = true;
    saving = false;
    modelChanged = false;

    private initialSettings: {
        bulkSendIR: boolean;
    } = {
            bulkSendIR: false,
        };

    private destroy$ = new Subject();
    private ALL_TRANSLATIONS = ["Success", "AssessmentSettingsSuccessMessage"];
    private translationKeys = {};
    private transitionDeregisterHook: any;

    constructor(
        private settingsAssessmentAction: SettingsAAActionService,
        private notificationService: NotificationService,
        private translations: TranslationsService,
        private otModalService: OtModalService,
        private $transitions: TransitionService,
    ) { }

    ngOnInit(): void {
        this.getAssessmentSettings();
        this.initializePreNavigationHook();
        this.translations.translate
            .stream(this.ALL_TRANSLATIONS)
            .pipe(takeUntil(this.destroy$))
            .subscribe((v) => {
                this.translationKeys = v;
            });
    }

    ngOnDestroy(): void {
        this.destroy$.next();
        this.destroy$.complete();
        this.transitionDeregisterHook();
    }

    toggleBulkSendIR(bulkSendIR: boolean): void {
        this.bulkSendIR = bulkSendIR;
        this.enableSave();
    }

    saveBulkSendIRSettings(): void {
        this.saving = true;
        this.settingsAssessmentAction.saveAssessmentSettings(this.bulkSendIR)
            .pipe(takeUntil(this.destroy$))
            .subscribe((response) => {
                this.saving = false;
                if (response.result) {
                    this.notificationService.alertSuccess(
                        this.translationKeys["Success"],
                        this.translationKeys["AssessmentSettingsSuccessMessage"],
                    );
                    this.modelChanged = false;
                    this.initialSettings.bulkSendIR = this.bulkSendIR;
                    return;
                }
            });
    }

    private getAssessmentSettings(): void {
        this.settingsAssessmentAction.getAssessmentSettings()
            .pipe(takeUntil(this.destroy$))
            .subscribe((response: IProtocolResponse<ISspTemplatesRestrictView>) => {
                if (response.result) {
                    this.bulkSendIR = response.data.infoRequestBulkEmailAllowed;
                    this.initialSettings.bulkSendIR = response.data.infoRequestBulkEmailAllowed;
                }
                this.loading = false;
            });
    }

    private enableSave(): void {
        this.modelChanged = this.initialSettings.bulkSendIR !== this.bulkSendIR;
    }

    private initializePreNavigationHook(): void {
        this.transitionDeregisterHook = this.$transitions.onBefore({ exiting: "zen.app.pia.module.settings.assessments.assessment" }, (): any => {
            if (this.modelChanged) {
                return new Promise<boolean>((resolve, reject) => {
                    this.otModalService
                        .create(
                            UnsavedChangesConfirmationModal,
                            {
                                isComponent: true,
                                size: ModalSize.SMALL,
                                hideClose: true,
                            },
                            {
                                promiseToResolve: () => {
                                    this.saving = true;
                                    this.settingsAssessmentAction.saveAssessmentSettings(this.bulkSendIR)
                                        .pipe(takeUntil(this.destroy$))
                                        .subscribe((response: IProtocolResponse<ISspTemplatesRestrictView>) => {
                                            if (response.result) {
                                                this.notificationService.alertSuccess(
                                                    this.translationKeys["Success"],
                                                    this.translationKeys["AssessmentSettingsSuccessMessage"],
                                                );
                                                this.saving = true;
                                                resolve(true);
                                            } else {
                                                resolve(false);
                                            }
                                        });

                                },
                                discardCallback: () => resolve(true),
                            },
                        );
                });
            }
            return true;
        });
    }
}
