// Angular
import {
    Component,
    OnInit,
    OnDestroy,
} from "@angular/core";

// 3rd Party
import { TransitionService } from "@uirouter/core";

// Vitreus
import {
    OtModalService,
    ModalSize,
} from "@onetrust/vitreus";

// rxjs
import { Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";

// Interfaces
import { IProtocolResponse } from "interfaces/protocol.interface";
import { ConfirmModalType } from "@onetrust/vitreus";
import { ITemplateSettings } from "modules/template/interfaces/template.interface";

// Enums
import { OrgUserTraversal } from "enums/org-user-dropdown.enum";

// Services
import { SettingsAAActionService } from "settingsServices/actions/settings-assessment-action.service";
import { NotificationService } from "modules/shared/services/provider/notification.service";
import { TranslationsService } from "@onetrust/common";

// Components
import { UnsavedChangesConfirmationModal } from "sharedModules/components/unsaved-changes-confirmation-modal/unsaved-changes-confirmation-modal.component";

@Component({
    selector: "aa-template-settings",
    templateUrl: "./aa-template-settings.component.html",
})
export class AATemplateSettingsComponent implements OnInit, OnDestroy {

    allowAccessControl: boolean;
    allowBranchOrg = false;
    loading = true;
    saving = false;
    touched = false;

    private initialTemplateSettingsModel: {
        allowAccessControl: boolean;
        allowBranchOrg: boolean;
    };
    private destroy$ = new Subject();
    private transitionDeregisterHook: any;
    private ALL_TRANSLATIONS = [
        "Success",
        "TemplateSettingsSuccessMessage",
        "DisableAccessControls",
        "TemplateDisableAccessModalDesc",
        "DoYouWantToContinue",
        "Confirm",
        "Cancel",
    ];
    private translationKeys = {};

    constructor(
        private settingsAssessmentAction: SettingsAAActionService,
        private otModalService: OtModalService,
        private translations: TranslationsService,
        private notificationService: NotificationService,
        private $transitions: TransitionService,
    ) { }

    ngOnInit(): void {
        this.getTemplateSettings();
        this.translations.translate
            .stream(this.ALL_TRANSLATIONS)
            .pipe(takeUntil(this.destroy$))
            .subscribe((v) => {
                this.translationKeys = v;
            });
        this.initializePreNavigationHook();
    }

    ngOnDestroy(): void {
        this.destroy$.next();
        this.destroy$.complete();
        this.transitionDeregisterHook();
    }

    toggleTemplateAccessControl(allowAccessControl: boolean): void {
        this.allowAccessControl = allowAccessControl;
        this.touched = this.isModelChanged();
    }

    selectTemplateOrgTraversal(templateOrgTraversalToggle: boolean): void {
        this.allowBranchOrg = templateOrgTraversalToggle;
        this.touched = this.isModelChanged();
    }

    saveTemplateSettings(): void {
        if (this.initialTemplateSettingsModel.allowAccessControl && !this.allowAccessControl) {
            this.openDisableSettingsConfirmModal();
        } else {
            this.saveTemplateAccessControlSettings(this.allowAccessControl, this.allowBranchOrg);
        }
    }

    private getTemplateSettings(): void {
        this.settingsAssessmentAction.getAssessmentTemplateSettings()
            .pipe(takeUntil(this.destroy$))
            .subscribe((response: IProtocolResponse<ITemplateSettings>) => {
                if (response.result) {
                    this.initializeTemplateSettings(response.data.allowAccessControl, response.data.allowedAssessmentCreationOrgHierarchy);
                }
                this.loading = false;
            });
    }

    private saveTemplateAccessControlSettings(allowAccessControl: boolean, allowBranchOrg: boolean): void {
        const templateSettingsPayload: ITemplateSettings = {
            allowAccessControl,
            allowedAssessmentCreationOrgHierarchy: allowBranchOrg ? OrgUserTraversal.Branch : OrgUserTraversal.Down,
        };
        this.saving = true;
        this.settingsAssessmentAction.saveTemplateAccessControlSettings(templateSettingsPayload)
            .pipe(takeUntil(this.destroy$))
            .subscribe((response: IProtocolResponse<boolean>) => {
                this.saving = false;
                if (response.result) {
                    this.notificationService.alertSuccess(
                        this.translationKeys["Success"],
                        this.translationKeys["TemplateSettingsSuccessMessage"],
                    );
                    this.initializeTemplateSettings(templateSettingsPayload.allowAccessControl, templateSettingsPayload.allowedAssessmentCreationOrgHierarchy);
                    this.touched = false;
                    return;
                }
            });
    }

    private openDisableSettingsConfirmModal(): void {
        this.allowAccessControl = false;
        this.otModalService
            .confirm({
                type: ConfirmModalType.WARNING,
                translations: {
                    title: this.translationKeys["DisableAccessControls"],
                    desc: this.translationKeys["TemplateDisableAccessModalDesc"],
                    confirm: this.translationKeys["DoYouWantToContinue"],
                    submit: this.translationKeys["Confirm"],
                    cancel: this.translationKeys["Cancel"],
                },
            }).pipe(takeUntil(this.destroy$))
            .subscribe((data: boolean) => {
                if (data) {
                    this.saveTemplateAccessControlSettings(this.allowAccessControl, this.allowBranchOrg);
                }
            });
    }

    private initializeTemplateSettings(allowAccessControl: boolean, allowedOrgs: string): void {
        this.allowAccessControl = allowAccessControl;
        this.allowBranchOrg = (allowedOrgs === OrgUserTraversal.Branch);
        this.initialTemplateSettingsModel = {
            allowAccessControl,
            allowBranchOrg: this.allowBranchOrg,
        };
    }

    private isModelChanged(): boolean {
        return (this.initialTemplateSettingsModel.allowAccessControl !== this.allowAccessControl) ||
            (this.allowAccessControl && this.initialTemplateSettingsModel.allowBranchOrg !== this.allowBranchOrg);
    }

    private initializePreNavigationHook(): void {
        this.transitionDeregisterHook = this.$transitions.onBefore({ exiting: "zen.app.pia.module.settings.assessments.template" }, (): any => {
            if (this.isModelChanged()) {
                return new Promise<boolean>((resolve, reject) => {
                    this.otModalService
                        .create(
                            UnsavedChangesConfirmationModal,
                            {
                                isComponent: true,
                                size: ModalSize.SMALL,
                                hideClose: true,
                            },
                            {
                                promiseToResolve: () => {
                                    this.saving = true;
                                    this.settingsAssessmentAction.saveTemplateAccessControlSettings({
                                        allowAccessControl: this.allowAccessControl,
                                        allowedAssessmentCreationOrgHierarchy: this.allowBranchOrg ? OrgUserTraversal.Branch : OrgUserTraversal.Down,
                                    })
                                        .pipe(takeUntil(this.destroy$))
                                        .subscribe((response: IProtocolResponse<boolean>) => {
                                            if (response.result) {
                                                this.notificationService.alertSuccess(
                                                    this.translationKeys["Success"],
                                                    this.translationKeys["TemplateSettingsSuccessMessage"],
                                                );
                                                this.saving = true;
                                                resolve(true);
                                            } else {
                                                resolve(false);
                                            }
                                        });

                                },
                                discardCallback: () => resolve(true),
                            },
                        );
                });
            }
            return true;
        });
    }
}
