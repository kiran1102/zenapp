import { Permissions } from "modules/shared/services/helper/permissions.service";
import { IStringMap } from "interfaces/generic.interface";
import { TranslationModules } from "constants/translation.constant";

export function routes($stateProvider: ng.ui.IStateProvider) {
    $stateProvider
        .state("zen.app.pia.module.settings", {
            url: "settings/",
            resolve: {
                SettingsPermission: [
                    "GeneralData",
                    "Permissions",
                    (
                        GeneralData: IStringMap<any>,
                        permissions: Permissions,
                    ): boolean => {
                        if (permissions.canShow("Settings")) {
                            return true;
                        } else {
                            permissions.goToFallback();
                        }
                        return false;
                    },
                ],
            },
            abstract: true,
            views: {
                module: {
                    template: "<settings-wrapper class='column-nowrap full-size'></settings-wrapper>",
                },
            },
        })
        .state("zen.app.pia.module.settings.ssp", {
            url: "ssp",
            template: "<downgrade-settings-ssp></downgrade-settings-ssp>",
        })
        .state("zen.app.pia.module.settings.project", {
            url: "project",
            params: { time: null },
            template: require("piaviews/Settings/Projects/project.jade"),
            controller: "ProjectSettingsController",
            resolve: {
                SettingsProjectPermission: [
                    "SettingsPermission",
                    (SettingsPermission: boolean): boolean => {
                        return SettingsPermission;
                    },
                ],
            },
        })
        .state("zen.app.pia.module.settings.tagging", {
            url: "tagging",
            params: { time: null },
            template: require("piaviews/Settings/Tagging/tagging.jade"),
            controller: "TaggingSettingsController",
            resolve: {
                SettingsTaggingPermission: [
                    "SettingsPermission",
                    (SettingsPermission: boolean): boolean => {
                        return SettingsPermission;
                    },
                ],
            },
        })
        .state("zen.app.pia.module.settings.directory", {
            url: "directory",
            params: { time: null },
            resolve: {
                SettingsDirectoryPermission: [
                    "SettingsPermission",
                    "Permissions",
                    (
                        SettingsPermission: boolean,
                        permissions: Permissions,
                    ): boolean => {
                        if (permissions.canShow("SettingsSSO")) {
                            return true;
                        } else {
                            permissions.goToFallback();
                        }
                        return false;
                    },
                ],
            },
            template: require("piaviews/Settings/Directory/directory.jade"),
            controller: "DirectorySettingsController",
        })
        .state("zen.app.pia.module.settings.smtp", {
            url: "smtp",
            params: { time: null },
            template: "<downgrade-settings-smtp></downgrade-settings-smtp>",
            resolve: {
                SettingsDirectoryPermission: [
                    "SettingsPermission",
                    "Permissions",
                    (
                        SettingsPermission: boolean,
                        permissions: Permissions,
                    ): boolean => {
                        if (permissions.canShow("CustomSMTPEditor")) {
                            return true;
                        } else {
                            permissions.goToFallback();
                        }
                        return false;
                    },
                ],
            },
        })
        .state("zen.app.pia.module.settings.branding", {
            url: "branding",
            params: { time: null },
            resolve: {
                SettingsBrandingPermission: [
                    "SettingsPermission",
                    "Permissions",
                    (
                        SettingsPermission: boolean,
                        permissions: Permissions,
                    ): boolean => {
                        if (permissions.canShow("SettingsCustomBranding")) {
                            return true;
                        } else {
                            permissions.goToFallback();
                        }
                        return false;
                    },
                ],
            },
            template: require("piaviews/Settings/LookAndFeel/branding.jade"),
            controller: "BrandingSettingsController",
        })
        .state("zen.app.pia.module.settings.timestamp", {
            url: "timestamp",
            params: { time: null },
            template: "<downgrade-timestamp-settings></downgrade-timestamp-settings>",
            resolve: {
                SettingsTimestampPermission: [
                    "SettingsPermission",
                    "Permissions",
                    (
                        SettingsPermission: boolean,
                        permissions: Permissions,
                    ): boolean => {
                        // TODO: need better naming for permission
                        if (permissions.canShow("UISettingsAdminUser")) {
                            return true;
                        } else {
                            permissions.goToFallback();
                        }
                        return false;
                    },
                ],
            },
        })
        .state("zen.app.pia.module.settings.messagetemplate", {
            url: "message-template/:typeId/:languageCode",
            params: { time: null },
            template: "<settings-email-template-component></settings-email-template-component>",
            resolve: {
                SettingsMailPermission: [
                    "SettingsPermission",
                    "Permissions",
                    (
                        SettingsPermission: boolean,
                        permissions: Permissions,
                    ): boolean => {
                        if (permissions.canShow("SettingsCustomEmailTemplate")) {
                            return true;
                        } else {
                            permissions.goToFallback();
                        }
                        return false;
                    },
                ],
            },
        })
        .state("zen.app.pia.module.settings.messagetemplatev2", {
            abstract: true,
            url: "",
            template: `<ui-view></ui-view>`,
            resolve: {
                SettingsMailPermission: [
                    "SettingsPermission",
                    "Permissions",
                    (
                        SettingsPermission: boolean,
                        permissions: Permissions,
                    ): boolean => {
                        if (permissions.canShow("CustomEmailTemplateView")) {
                            return true;
                        } else {
                            permissions.goToFallback();
                        }
                        return false;
                    },
                ],
            },
        })
        .state("zen.app.pia.module.settings.messagetemplatev2.list", {
            url: "message-templatev2/:typeId/:languageCode",
            params: { time: null },
            resolve: {
                SettingsMailPermission: [
                    "SettingsPermission",
                    "Permissions",
                    (
                        SettingsPermission: boolean,
                        permissions: Permissions,
                    ): boolean => {
                        if (permissions.canShow("CustomEmailTemplateView")) {
                            return true;
                        } else {
                            permissions.goToFallback();
                        }
                        return false;
                    },
                ],
            },
            template: "<email-template-list></email-template-list>",
        })
        .state("zen.app.pia.module.settings.messagetemplatev2.edit", {
            url: "message-template/edit/:messageTypeId/:languageCode",
            resolve: {
                SettingsMailPermission: [
                    "SettingsPermission",
                    "Permissions",
                    (
                        SettingsPermission: boolean,
                        permissions: Permissions,
                    ): boolean => {
                        if (permissions.canShow("CustomEmailTemplateView")) {
                            return true;
                        } else {
                            permissions.goToFallback();
                        }
                        return false;
                    },
                ],
            },
            template: "<email-template-edit></email-template-edit>",
        })
        .state("zen.app.pia.module.settings.languageactivator", {
            url: "languageactivator",
            resolve: {
                SettingsMailPermission: [
                    "SettingsPermission",
                    "Permissions",
                    (
                        SettingsPermission: boolean,
                        permissions: Permissions,
                    ): boolean => {
                        if (permissions.canShow("LanguageActivatorEdit")) {
                            return true;
                        } else {
                            permissions.goToFallback();
                        }
                        return false;
                    },
                ],
            },
            template: "<email-language-activator></email-language-activator>",
        })
        .state("zen.app.pia.module.settings.localization", {
            url: "localization?module&page&size&search",
            params: {
                time: null,
                module: { dynamic: true, value: TranslationModules.System },
            },
            template: "<localization-editor></localization-editor>",
            resolve: {
                SettingsLocalizationPermission: [
                    "SettingsPermission",
                    "Permissions",
                    (
                        SettingsPermission: boolean,
                        permissions: Permissions,
                    ): boolean => {
                        if (permissions.canShow("LocalizationEditor")) {
                            return true;
                        } else {
                            permissions.goToFallback();
                        }
                        return false;
                    },
                ],
            },
        })
        .state("zen.app.pia.module.settings.terminology", {
            url: "terminology",
            params: { time: null },
            resolve: {
                SettingsTerminologyPermission: [
                    "SettingsPermission",
                    "Permissions",
                    (
                        SettingsPermission: boolean,
                        permissions: Permissions,
                    ): boolean => {
                        if (permissions.canShow("SettingsCustomTerminology")) {
                            return true;
                        } else {
                            permissions.goToFallback();
                        }
                        return false;
                    },
                ],
            },
            template: require("piaviews/Settings/LookAndFeel/terminology.jade"),
            controller: "CustomTermSettingsController",
        })
        .state("zen.app.pia.module.settings.assessments", {
            abstract: true,
            url: "assessments/",
            resolve: {
                SettingsAssessmentAutomationPermission: [
                    "SettingsPermission",
                    "Permissions",
                    (SettingsPermission: boolean, permissions: Permissions): boolean => {
                        if (permissions.canShow("AssessmentAutomationSettings")) {
                            return true;
                        }

                        permissions.goToFallback();
                        return false;
                    },
                ],
            },
            template: "<ui-view></ui-view>",
        })
        .state("zen.app.pia.module.settings.assessments.template", {
            url: "template",
            template: "<downgrade-aa-template-settings></downgrade-aa-template-settings>",
        })
        .state("zen.app.pia.module.settings.assessments.assessment", {
            url: "assessment",
            template: "<downgrade-aa-assessment-settings></downgrade-aa-template-settings>",
        })
        .state("zen.app.pia.module.settings.risks", {
            url: "risks",
            params: { time: null },
            templateProvider: ["Permissions", (permissions: Permissions) => {
                if (permissions.canShow("RiskV2Setting")) {
                    if (permissions.canShow("AssessmentRiskScore") || permissions.canShow("InventoryRiskScore")) return "<risk-settings-page></risk-settings-page>";
                    return "<risk-heatmap-page></risk-heatmap-page>";
                } else {
                    return require("piaviews/Settings/Risks/risks.jade");
                }
            }],
            controllerProvider: ["Permissions", (permissions: Permissions) => {
                if (permissions.canShow("RiskV2Setting")) return;
                return "RisksSettingsController";
            }],
            resolve: {
                Permissions: "Permissions",
                SettingsRiskSPermission: [
                    "SettingsPermission",
                    "Permissions",
                    (
                        SettingsPermission: boolean,
                        permissions: Permissions,
                    ): boolean => {
                        if ((!permissions.canShow("Assessments") && permissions.canShow("AssessmentRiskSettings"))
                            || permissions.canShow("RiskV2Setting")) {
                            return true;
                        }
                        permissions.goToFallback();
                        return false;
                    },
                ],
            },
        })
        .state("zen.app.pia.module.settings.help-and-privacy", {
            url: "help-and-privacy",
            params: { time: null },
            template: "<downgrade-settings-help></downgrade-settings-help>",
            resolve: {
                SettingsHelpPermission: [
                    "SettingsPermission",
                    "Permissions",
                    (
                        SettingsPermission: boolean,
                        permissions: Permissions,
                    ): boolean => {
                        if (permissions.canShow("SettingsHelp")) {
                            return true;
                        } else {
                            permissions.goToFallback();
                        }
                        return false;
                    },
                ],
            },
        })
        .state("zen.app.pia.module.settings.sso", {
            url: "saml",
            params: { time: null },
            template: "<downgrade-setting-sso></downgrade-setting-sso>",
            resolve: {
                SettingsHelpPermission: [
                    "SettingsPermission",
                    "Permissions",
                    (
                        SettingsPermission: boolean,
                        permissions: Permissions,
                    ): boolean => {
                        if (permissions.canShow("SettingsSSONew")) {
                            return true;
                        } else {
                            permissions.goToFallback();
                        }
                        return false;
                    },
                ],
            },
        })
        .state("zen.app.pia.module.settings.manage-subscriptions", {
            url: "manage-subscriptions",
            template: "<downgrade-settings-manage-subscriptions></downgrade-settings-manage-subscriptions>",
            resolve: {
                SettingsManageSubscriptionsPermission: [
                    "SettingsPermission",
                    "Permissions",
                    (
                        SettingsPermission: boolean,
                        permissions: Permissions,
                    ): boolean => {
                        if (permissions.canShow("ManageSubscriptions")) {
                            return true;
                        } else {
                            permissions.goToFallback();
                        }
                        return false;
                    },
                ],
            },
        })
        .state("zen.app.pia.module.settings.consent", {
            url: "consent",
            params: { time: null },
            resolve: {
                SettingsDirectoryPermission: [
                    "SettingsPermission",
                    "Permissions",
                    (
                        SettingsPermission: boolean,
                        permissions: Permissions,
                    ): boolean => {
                        if (permissions.canShow("SettingsConsent")) {
                            return true;
                        } else {
                            permissions.goToFallback();
                        }
                        return false;
                    },
                ],
            },
            template: "<settings-consent></settings-consent>",
        })
        .state("zen.app.pia.module.settings.inventory-settings", {
            url: "inventory",
            params: { time: null },
            template: "<downgrade-settings-inventory></downgrade-settings-inventory>",
            resolve: {
                SettingsDirectoryPermission: [
                    "SettingsPermission",
                    "Permissions",
                    (
                        SettingsPermission: boolean,
                        permissions: Permissions,
                    ): boolean => {
                        if (permissions.canShow("DataMappingInventorySettings")) {
                            return true;
                        } else {
                            permissions.goToFallback();
                        }
                        return false;
                    },
                ],
            },
        })
        .state("zen.app.pia.module.settings.manage-visibility", {
            url: "manage-visibility",
            params: { time: null },
            template: "<inventory-manage-visibility-wrapper></inventory-manage-visibility-wrapper>",
            resolve: {
                SettingsDirectoryPermission: [
                    "SettingsPermission",
                    "Permissions",
                    (
                        SettingsPermission: boolean,
                        permissions: Permissions,
                    ): boolean => {
                        if (permissions.canShow("EnableDataSubjectsElementsV2") && permissions.canShow("DataMappingManageDataElementVisibility")) {
                            return true;
                        } else {
                            permissions.goToFallback();
                        }
                        return false;
                    },
                ],
            },
        })
        .state("zen.app.pia.module.settings.learning-and-feedback-tool", {
            url: "learning-and-feedback-tool",
            template: "<downgrade-settings-learning-and-feedback-tool></downgrade-settings-learning-and-feedback-tool>",
            resolve: {
                SettingsLearningAndFeedbackToolPermission: [
                    "SettingsPermission",
                    "Permissions",
                    (
                        SettingsPermission: boolean,
                        permissions: Permissions,
                    ): boolean => {
                        if ((permissions.canShow("AdminToolSetting")) && (permissions.canShow("UserFeedBack") || permissions.canShow("UserGuidance"))) {
                            return true;
                        } else {
                            permissions.goToFallback();
                        }
                        return false;
                    },
                ],
            },
        })
        .state("zen.app.pia.module.settings.scim-settings", {
            url: "scim",
            template: "<downgrade-setting-scim></downgrade-setting-scim>",
            resolve: {
                SettingScimPermission: [
                    "SettingsPermission",
                    "Permissions",
                    (
                        SettingsPermission: boolean,
                        permissions: Permissions,
                    ): boolean => {
                        if (permissions.canShow("ScimAdminSetting")) {
                            return true;
                        } else {
                            permissions.goToFallback();
                        }
                        return false;
                    },
                ],
            },
        })
        .state("zen.app.pia.module.settings.vendor-management", {
            url: "vendor-risk-management",
            params: { time: null },
            template: "<downgrade-settings-vendor-management></downgrade-settings-vendor-management>",
            resolve: {
                SettingsHelpPermission: [
                    "SettingsPermission",
                    "Permissions",
                    (
                        SettingsPermission: boolean,
                        permissions: Permissions,
                    ): boolean => {
                        if (permissions.canShow("VendorManagementSettings")) {
                            return true;
                        } else {
                            permissions.goToFallback();
                        }
                        return false;
                    },
                ],
            },
        });
}

routes.$inject = ["$stateProvider"];
