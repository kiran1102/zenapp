import { AnyAction } from "redux";

import { createSelector, Selector } from "reselect";
import { IStoreState } from "interfaces/redux.interface";
import {
    ILocalizationState,
    IGetInitialState,
    ITranslationsPageSelector,
    ITranslationsPageFetchStatus,
} from "interfaces/localization-reducer.interface";
import {
    ITranslationsPage,
    ITranslation,
} from "interfaces/localization.interface";

export const LocalizationActions = {
    RESET_LOCALIZATION: "RESET_LOCALIZATION",
    FETCHED_LOCALIZATION_PAGE: "FETCHED_LOCALIZATION_PAGE",
    FETCHING_LOCALIZATION_PAGE: "FETCHING_LOCALIZATION_PAGE",
    SET_LOCALIZATION_DATA: "SET_LOCALIZATION_DATA",
    SET_LOCALIZATION_DATA_AND_FETCHED: "SET_LOCALIZATION_DATA_AND_FETCHED",
};

const initialState: ILocalizationState = {
    translationsPage: null,
    customTranslationsPage: null,
    isFetchingPage: false,
    isFetchingCustomPage: false,
};

export default function localizationReducer(state = getInitialState(), action: AnyAction): ILocalizationState {
    switch (action.type) {
        case LocalizationActions.RESET_LOCALIZATION:
            return getInitialState();
        case LocalizationActions.FETCHING_LOCALIZATION_PAGE:
            return { ...state, isFetchingPage: true };
        case LocalizationActions.FETCHED_LOCALIZATION_PAGE:
            return { ...state, isFetchingPage: false };
        case LocalizationActions.SET_LOCALIZATION_DATA:
            const { translationsPage } = action;
            return { ...state, translationsPage: { ...translationsPage } };
        case LocalizationActions.SET_LOCALIZATION_DATA_AND_FETCHED:
            const translationsPageFetched = action.translationsPage;
            return { ...state, translationsPage: { ...translationsPageFetched }, isFetchingPage: false };
        default:
            return state;
    }
}

export const getInitialState: IGetInitialState = (): ILocalizationState => ({...initialState });
export const localizationState: Selector<IStoreState, ILocalizationState> = (state: IStoreState): ILocalizationState => state.localization;
export const getTranslationsPage: ITranslationsPageSelector = createSelector(localizationState, (localizationState: ILocalizationState): ITranslationsPage => localizationState.translationsPage);
export const getTranslationsPageFetchStatus: ITranslationsPageFetchStatus = createSelector(localizationState, (localizationState: ILocalizationState): boolean => localizationState.isFetchingPage);
