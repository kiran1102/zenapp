// Angular
import { NgModule } from "@angular/core";

// Modules
import { PolicySharedModule } from "./shared/policy-shared.module";
import { PrivacyNoticeSharedModule } from "./privacy-notice/shared/privacy-notice-shared.module";
import { PrivacyNoticeDetailsModule } from "./privacy-notice/privacy-notice-details/privacy-notice-details.module";
import { PrivacyNoticeListModule } from "./privacy-notice/privacy-notice-list/privacy-notice-list.module";

@NgModule({
    imports: [
        PolicySharedModule,
        PrivacyNoticeSharedModule,
        PrivacyNoticeDetailsModule,
        PrivacyNoticeListModule,
    ],
})
export class PolicyModule { }
