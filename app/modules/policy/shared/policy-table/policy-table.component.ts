// Angular
import {
    Component,
    EventEmitter,
    Input,
    Output,
} from "@angular/core";

// 3rd Party
import {
    get,
    keys,
    pickBy,
} from "lodash";

// Interfaces
import { IPageable } from "interfaces/pagination.interface";
import { IBasicTableCell } from "interfaces/tables.interface";
import { IDropdownOption } from "interfaces/one-dropdown.interface";

// Models
import { ColumnDefinition } from "crmodel/column-definition";

// Models
import { policyStatusStyleClass } from "modules/policy/privacy-notice/shared/models/policy-status-style";

@Component({
    selector: "policy-table",
    templateUrl: "./policy-table.component.html",
})
export class PolicyTable {
    @Input() rows: IPageable;
    @Input() columns: ColumnDefinition[];
    @Input() loading: boolean;
    @Input() noDataKey: string;
    @Input() errorKey: string;
    @Input() responsive = true;
    @Input() rowFlagKey: string;
    @Output() goToRoute: EventEmitter<IBasicTableCell> = new EventEmitter<IBasicTableCell>();
    @Output() pageChange: EventEmitter<number> = new EventEmitter<number>();
    @Output() selectionChange: EventEmitter<string[]> = new EventEmitter<string[]>();
    @Output() noDataRoute: EventEmitter<void> = new EventEmitter<void>();

    sortable = false;
    sortOrder: string;
    sortedKey: string | number;
    rowBordered = true;
    rowSelection: { [id: string]: boolean } = {};
    allRowsSelected: boolean;
    ellipsisOptions: IDropdownOption[] = [];

    resolveProperty(object: any, path: string): string {
        if (!object) {
            return "";
        }
        return get(object, path);
    }

    getEllipsisOptions(col: any, row: any) {
        this.ellipsisOptions = col.getOptions(row);
    }

    handleSelectAll(columnValue: string) {
        this.allRowsSelected = !this.allRowsSelected;
        this.setAllSelections(columnValue, this.allRowsSelected);
        this.selectionChange.emit(this.selectedRowIds);
    }

    handleSelectRow(rowId: string) {
        this.rowSelection[rowId] = !this.rowSelection[rowId];
        this.getAllRowsSelected();
        this.selectionChange.emit(this.selectedRowIds);
    }

    generateRoute(row: any, col: ColumnDefinition) {
        if (col.routeVersionField) {
            this.goToRoute.emit({ route: col.route, params: { id: row[col.routeIdField || "id"], version: row[col.routeVersionField] } });
        } else {
            this.goToRoute.emit({ route: col.route, params: { id: row[col.routeIdField || "id"] } });
        }
    }

    getAllRowsSelected() {
        this.allRowsSelected = this.rows && this.rows.content && this.selectedRowIds.length === this.rows.content.length;
    }

    getBadgeClass(status: string) {
        return policyStatusStyleClass[status.toUpperCase()];
    }

    get selectedRowIds(): string[] {
        return keys(pickBy(this.rowSelection));
    }

    private setAllSelections(columnValue: string, value: boolean) {
        if (!(this.rows && this.rows.content)) {
            return;
        }
        this.rows.content.forEach((row) => {
            const rowId = row[columnValue];
            this.rowSelection[rowId] = value;
        });
    }
}
