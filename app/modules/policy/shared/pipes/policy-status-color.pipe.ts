import {
    Pipe,
    PipeTransform,
} from "@angular/core";

import { policyStatusStyleClass } from "modules/policy/privacy-notice/shared/models/policy-status-style";

@Pipe({
    name: "policyStatusColor",
})
export class PolicyStatusColorPipe implements PipeTransform {
    transform(status: string): string {
        return policyStatusStyleClass[status];
    }
}
