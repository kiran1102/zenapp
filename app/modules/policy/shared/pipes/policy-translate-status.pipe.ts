// Third Party
import {
    Pipe,
    PipeTransform,
} from "@angular/core";
import { find } from "lodash";

// Interfaces
import { IStringMap } from "interfaces/generic.interface";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

@Pipe({
    name: "policyTranslateStatus",
})
export class PolicyTranslateStatusPipe implements PipeTransform {
    constructor(
        private translatePipe: TranslatePipe,
    ) { }

    transform(value: string, constType: IStringMap<string>): string {
        if (!constType || !value) {
            return "";
        }
        const keys: string[] = Object.keys(constType);
        const constKey: string = find(keys, (key: string): boolean => constType[key] === value);
        return this.translatePipe.transform(constKey);
    }
}
