import {
    Pipe,
    PipeTransform,
} from "@angular/core";

@Pipe({
    name: "policyVersionLabel",
})

export class PolicyVersionLabelPipe implements PipeTransform {
    transform(version: number): string {
        return `V${version}`;
    }
}
