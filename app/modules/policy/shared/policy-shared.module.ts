// Angular
import { NgModule } from "@angular/core";
import { UIRouterModule } from "@uirouter/angular";

// Components
import { PolicyWrapperComponent } from "./policy-wrapper/policy-wrapper.component";
import { PolicyTable } from "./policy-table/policy-table.component";

// Modules
import { SharedModule } from "sharedModules/shared.module";
import { UpgradedLegacyModule } from "sharedModules/upgrade/upgraded-legacy.module";
import { CommonModule } from "@angular/common";
import { HttpClientModule } from "@angular/common/http";
import { FormsModule } from "@angular/forms";
import { ReactiveFormsModule } from "@angular/forms";
import { VitreusModule } from "@onetrust/vitreus";
import { DndListModule } from "ngx-drag-and-drop-lists";
import { OTPipesModule, PIPES } from "modules/pipes/pipes.module";
import { VirtualScrollModule } from "od-virtualscroll";
import { OneVideoModule } from "modules/video/video.module";

// Pipes
import { PolicyStatusColorPipe } from "modules/policy/shared/pipes/policy-status-color.pipe";
import { PolicyTranslateStatusPipe } from "modules/policy/shared/pipes/policy-translate-status.pipe";
import { PolicyVersionLabelPipe } from "modules/policy/shared/pipes/policy-version-label.pipe";

@NgModule({
    imports: [
        SharedModule,
        UpgradedLegacyModule,
        CommonModule,
        HttpClientModule,
        FormsModule,
        ReactiveFormsModule,
        VitreusModule,
        DndListModule,
        OTPipesModule,
        VirtualScrollModule,
        OneVideoModule,
        UIRouterModule,
    ],
    declarations: [
        PolicyStatusColorPipe,
        PolicyTranslateStatusPipe,
        PolicyVersionLabelPipe,
        PolicyWrapperComponent,
        PolicyTable,
    ],
    exports: [
        PolicyStatusColorPipe,
        PolicyTranslateStatusPipe,
        PolicyVersionLabelPipe,
        PolicyWrapperComponent,
        PolicyTable,
    ],
    entryComponents: [
        PolicyWrapperComponent,
    ],
    providers: [
        PIPES, // TODO: Remove from providers when ot-common is implemented. Only necessary if you are using pipe in TS file, else just module is fine
    ],
})
export class PolicySharedModule { }
