// Angular
import {
    Component,
    OnInit,
} from "@angular/core";

// Services
import { Permissions } from "modules/shared/services/helper/permissions.service";
import { GlobalSidebarService } from "modules/global-sidebar/services/one-global-sidebar.service";
import { Wootric } from "sharedModules/services/provider/wootric.service";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

// Interfaces
import { INavMenuItem } from "modules/global-sidebar/interfaces/nav-menu-item.interface";

// Constants
import { WootricModuleMap } from "modules/shared/constants/wootric.constants";

@Component({
    selector: "policy-wrapper",
    templateUrl: "./policy-wrapper.component.html",
})
export class PolicyWrapperComponent implements OnInit {
    constructor(
        private permissions: Permissions,
        private translatePipe: TranslatePipe,
        private globalSidebarService: GlobalSidebarService,
        private wootric: Wootric,
    ) {}

    ngOnInit() {
        this.globalSidebarService.set(this.getMenuRoutes(), this.translatePipe.transform("PolicyAndNoticeManagement"));
        this.wootric.run(WootricModuleMap.PolicyManagement);
    }

    private getMenuRoutes(): INavMenuItem[] {
        const menuGroup: INavMenuItem[] = [];
        if (this.permissions.canShow("PolicyManagement")) {
            menuGroup.push({
                id: "PolicyPrivacyNoticeSidebarItem",
                title: this.translatePipe.transform("PrivacyNotices"),
                route: "zen.app.pia.module.policy.privacynotices.list",
                activeRoute: "zen.app.pia.module.policy.privacynotices",
            });
        }

        return menuGroup;
    }
}
