import {
    Component,
    EventEmitter,
    Inject,
    Input,
    Output,
} from "@angular/core";

import { StateService } from "@uirouter/core";

// Interfaces
import { IDropdownOption } from "interfaces/one-dropdown.interface";

// Models
import { PolicyVersion } from "modules/policy/privacy-notice/shared/models/policy-version";
import { PrivacyNotice } from "modules/policy/privacy-notice/shared/models/privacy-notice";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

// Redux
import { StoreToken } from "tokens/redux-store.token";
import { IStore } from "interfaces/redux.interface";

// Services
import {
    OtModalService,
    ConfirmModalType,
    IConfirmModalConfig,
} from "@onetrust/vitreus";

import { PrivacyNoticeService } from "modules/policy/privacy-notice/shared/services/privacy-notice.service";

// Enums
import { BuilderEditingMode } from "modules/policy//privacy-notice/shared/enums/builder-editing-mode.enum";
import { PolicyStatus } from "modules/policy/privacy-notice/shared/enums/policy-status.enum";

// rxjs
import { Observable } from "rxjs";
import { take, filter } from "rxjs/operators";

// Constants
import {
    policyStatusLabel,
    policyStatusStyleClass,
} from "modules/policy/privacy-notice/shared/models/policy-status-style";

// Components
import { PrivacyNoticeNewVersionModalComponent } from "modules/policy/privacy-notice/privacy-notice-details/privacy-notice-new-version-modal/privacy-notice-new-version-modal.component";
import { CreatePrivacyNoticeDto } from "modules/policy/privacy-notice/shared/models/create-privacy-notice-dto";

@Component({
    selector: "privacy-notice-header",
    templateUrl: "./privacy-notice-header.component.html",
})
export class PrivacyNoticeHeaderComponent {
    @Input() privacyNotice: PrivacyNotice;
    @Input() editingMode: BuilderEditingMode;
    @Input() actionInProgress: boolean;
    @Input() hasPendingChanges: boolean;

    @Output() toggleDrawer = new EventEmitter<boolean>();
    @Output() editingModeToggled = new EventEmitter<BuilderEditingMode>();
    @Output() onSaveRequested = new EventEmitter<void>();
    @Output() onPublishRequested = new EventEmitter<string>();
    @Output() onCreateNewVersionRequested = new EventEmitter<CreatePrivacyNoticeDto>();
    @Output() onResetRequested = new EventEmitter<void>();

    isPrivacyNoticeValid: boolean;
    ellipsisOptions: IDropdownOption[] = [];

    constructor(
        private stateService: StateService,
        @Inject(StoreToken) private readonly store: IStore,
        private privacyNoticeService: PrivacyNoticeService,
        private otModalService: OtModalService,
        private translatePipe: TranslatePipe,
    ) {
    }

    get version(): PolicyVersion {
        return this.privacyNoticeService.getCurrentVersion(this.privacyNotice.versions);
    }

    get isDraft(): boolean {
        return this.version.status === PolicyStatus.DRAFT;
    }

    getEllipsisOptions() {
        this.ellipsisOptions = [{
            text: this.translatePipe.transform("ViewVersions"),
            action: (): void => {
                this.toggleDrawer.emit();
            },
        }];

        if (this.hasPendingChanges && this.isPrivacyNoticeValid) {
            this.ellipsisOptions.push(
                {
                    text: this.translatePipe.transform("Save"),
                    action: (): void => {
                        this.savePrivacyNotice();
                    },
                },
            );
        }

        if (this.hasPendingChanges) {
            this.ellipsisOptions.push(
                {
                    text: this.translatePipe.transform("ResetChanges"),
                    action: (): void => {
                        this.resetChanges();
                    },
                },
            );
        }

        const editingModeLabel = this.editingMode === BuilderEditingMode.WYSIWYG ?
            "SwitchToHTMLEditingMode" : "SwitchToRichTextEditingMode";

        this.ellipsisOptions.push(
            {
                text: this.translatePipe.transform(editingModeLabel),
                action: () => {
                    const editingMode = this.editingMode === BuilderEditingMode.WYSIWYG ?
                        BuilderEditingMode.HTML : BuilderEditingMode.WYSIWYG;
                    this.setSelectedEditingMode(editingMode);
                },
            },
        );
    }

    requestPublishPrivacyNotice() {
        this.onPublishRequested.emit(this.version.id);
    }

    createNewVersion() {
        this.otModalService
            .create(
                PrivacyNoticeNewVersionModalComponent,
                { isComponent: true },
                {...this.privacyNotice},
            )
            .pipe(
                filter(Boolean),
            )
            .subscribe((version: CreatePrivacyNoticeDto) => {
                this.onCreateNewVersionRequested.emit(version);
            });
    }

    openPublishPrivacyNoticeModal() {
        this.otModalService.confirm({
            type: ConfirmModalType.WARNING,
            translations: {
                title: this.translatePipe.transform("PublishPrivacyNotice"),
                desc: this.translatePipe.transform("PublishPrivacyNoticeConfirmation"),
                confirm: this.translatePipe.transform("AreYouSureContinue"),
                submit: this.translatePipe.transform("Publish"),
                cancel: this.translatePipe.transform("Cancel"),
            },
        }).pipe(
            take(1),
            filter(Boolean),
        ).subscribe(() => {
            this.requestPublishPrivacyNotice();
        });
    }

    savePrivacyNotice() {
        this.onSaveRequested.emit();
    }

    previewPrivacyNotice() {
        this.privacyNoticeService.previewPrivacyNotice();
    }

    resetChanges() {
        this.onResetRequested.emit();
    }

    get displayStatusLabel(): string {
        const translation = policyStatusLabel[this.version.status];
        return this.translatePipe.transform(translation);
    }

    get displayStatusClass(): string {
        const className = policyStatusStyleClass[this.version.status];
        return this.translatePipe.transform(className);
    }

    onHeaderRouteClick(route: { path: string, params: any }) {
        this.stateService.go(route.path);
    }

    setSelectedEditingMode(editingMode: BuilderEditingMode) {
        if (editingMode === this.editingMode) {
            return;
        }

        if (this.hasPendingChanges && editingMode === BuilderEditingMode.WYSIWYG) {
            this.switchToRichTextEditor();
            return;
        }

        this.editingModeToggled.emit(editingMode);
    }

    private switchToRichTextEditor() {
        this.showConfirmationModal()
            .pipe(
                filter(Boolean),
            )
            .subscribe(() => {
                this.editingModeToggled.emit(BuilderEditingMode.WYSIWYG);
            });
    }

    private showConfirmationModal(): Observable<boolean> {
        const config: IConfirmModalConfig = {
            type: ConfirmModalType.WARNING,
            translations: {
                title: this.translatePipe.transform("SwitchingToRichTextEditingMode"),
                desc: this.translatePipe.transform("AreYouSureYouWantToSwitchToRichTextDescription"),
                confirm: "",
                submit: this.translatePipe.transform("SwitchToRichText"),
                cancel: this.translatePipe.transform("BackToHTML"),
            },
        };

        return this.otModalService
            .confirm(config)
            .pipe(
                take(1),
            );
    }
}
