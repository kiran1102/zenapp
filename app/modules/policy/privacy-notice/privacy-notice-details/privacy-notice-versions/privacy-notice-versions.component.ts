// Angular
import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from "@angular/core";

// Components
import { PrivacyNotice } from "modules/policy/privacy-notice/shared/models/privacy-notice";

// Enums
import { PolicyStatus } from "modules/policy/privacy-notice/shared/enums/policy-status.enum";

// Models
import { policyStatusLabel } from "modules/policy/privacy-notice/shared/models/policy-status-style";
import { PolicyVersion } from "modules/policy/privacy-notice/shared/models/policy-version";

// Services
import { PrivacyNoticeService } from "modules/policy/privacy-notice/shared/services/privacy-notice.service";

@Component({
    selector: "privacy-notice-versions",
    templateUrl: "./privacy-notice-versions.component.html",
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PrivacyNoticeVersionsComponent {
    @Input() privacyNotice: PrivacyNotice;
    @Input() drawerOpen: boolean;

    @Output() toggleDrawer = new EventEmitter<void>();
    @Output() onChangeVersionRequested = new EventEmitter<string>();
    @Output() onDeleteRequested = new EventEmitter<PolicyVersion>();

    readonly toggledItemsMap: { [key: string]: boolean } = {};
    readonly policyStatusLabels = policyStatusLabel;
    readonly EMPTY_DATE_FALLBACK = "- - - -";

    constructor(private service: PrivacyNoticeService) {
    }

    get versions(): PolicyVersion[] {
        const versions = [...this.privacyNotice.versions];
        return versions.sort((prev: PolicyVersion, curr: PolicyVersion) => {
            if (prev.version > curr.version) {
                return -1;
            }

            if (prev.version < curr.version) {
                return 1;
            }

            return 0;
        });
    }

    get currentVersion(): PolicyVersion {
        return this.service.getCurrentVersion(this.privacyNotice.versions);
    }

    get canDeleteVersion(): boolean {
        return this.privacyNotice.versions.some((version: PolicyVersion) => {
            return version.status === PolicyStatus.ACTIVE;
        });
    }

    isDraft(version: PolicyVersion): boolean {
        return version.status === PolicyStatus.DRAFT;
    }

    changeVersion(versionId: string) {
        if (this.currentVersion.id === versionId) {
            return;
        }

        this.onChangeVersionRequested.emit(versionId);
    }

    trackByFn(index: number, item: PolicyVersion) {
        return item.id;
    }

    toggleItem(id: string) {
        this.toggledItemsMap[id] = !this.toggledItemsMap[id];
    }
}
