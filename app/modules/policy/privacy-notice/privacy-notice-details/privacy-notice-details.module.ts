// Angular
import { NgModule } from "@angular/core";

// Components
import { PrivacyNoticeDetailsComponent } from "./privacy-notice-details.component";
import { PrivacyNoticeHeaderComponent } from "./privacy-notice-header/privacy-notice-header.component";
import { PrivacyNoticeBuilderComponent } from "./privacy-notice-builder/privacy-notice-builder.component";
import { PrivacyNoticeBuilderSidebarComponent } from "./privacy-notice-builder/privacy-notice-builder-sidebar/privacy-notice-builder-sidebar.component";
import { PrivacyNoticeIntegrationsComponent } from "./privacy-notice-integrations/privacy-notice-integrations.component";
import { PrivacyNoticePublishScriptModalComponent } from "./privacy-notice-publish-script-modal/privacy-notice-publish-script-modal.component";
import { PrivacyNoticeVersionsComponent } from "./privacy-notice-versions/privacy-notice-versions.component";
import { PrivacyNoticeNewVersionModalComponent } from "./privacy-notice-new-version-modal/privacy-notice-new-version-modal.component";
import { PrivacyNoticeIntegrationSnippetComponent } from "modules/policy/privacy-notice/privacy-notice-details/privacy-notice-integrations/privacy-notice-integration-snippet/privacy-notice-integration-snippet.component";

// Modules
import { PrivacyNoticeSharedModule } from "modules/policy/privacy-notice/shared/privacy-notice-shared.module";
import { SharedModule } from "sharedModules/shared.module";
import { UpgradedLegacyModule } from "sharedModules/upgrade/upgraded-legacy.module";
import { CommonModule } from "@angular/common";
import { HttpClientModule } from "@angular/common/http";
import { FormsModule } from "@angular/forms";
import { ReactiveFormsModule } from "@angular/forms";
import { VitreusModule } from "@onetrust/vitreus";
import { DndListModule } from "ngx-drag-and-drop-lists";
import { OTPipesModule, PIPES } from "modules/pipes/pipes.module";
import { VirtualScrollModule } from "od-virtualscroll";
import { OneVideoModule } from "modules/video/video.module";

@NgModule({
    imports: [
        PrivacyNoticeSharedModule,
        SharedModule,
        UpgradedLegacyModule,
        CommonModule,
        HttpClientModule,
        FormsModule,
        ReactiveFormsModule,
        VitreusModule,
        DndListModule,
        OTPipesModule,
        VirtualScrollModule,
        OneVideoModule,
    ],
    declarations: [
        PrivacyNoticeDetailsComponent,
        PrivacyNoticeHeaderComponent,
        PrivacyNoticeBuilderComponent,
        PrivacyNoticeBuilderSidebarComponent,
        PrivacyNoticeIntegrationsComponent,
        PrivacyNoticePublishScriptModalComponent,
        PrivacyNoticeVersionsComponent,
        PrivacyNoticeNewVersionModalComponent,
        PrivacyNoticeIntegrationSnippetComponent,
    ],
    exports: [
        PrivacyNoticeDetailsComponent,
    ],
    entryComponents: [
        PrivacyNoticeDetailsComponent,
        PrivacyNoticePublishScriptModalComponent,
        PrivacyNoticeNewVersionModalComponent,
    ],
    providers: [
        PIPES, // TODO: Remove from providers when ot-common is implemented. Only necessary if you are using pipe in TS file, else just module is fine
    ],
})
export class PrivacyNoticeDetailsModule { }
