// Angular
import { Component, Inject, OnInit } from "@angular/core";

// Rxjs
import { BehaviorSubject, merge, Observable, Subject } from "rxjs";
import { filter, map, mergeMap, share, shareReplay, take, takeUntil, withLatestFrom } from "rxjs/operators";

import { StateService, TransitionService } from "@uirouter/core";

// Constants
import { PolicyConstants } from "constants/policy-constants";

// Models
import { PrivacyNotice } from "modules/policy/privacy-notice/shared/models/privacy-notice";
import { CreateNewVersionDto } from "modules/policy/privacy-notice/shared/models/create-new-version-dto";
import { PolicyVersion } from "modules/policy/privacy-notice/shared/models/policy-version";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";
import { ITabsNav } from "interfaces/one-tabs-nav.interface";

// Redux
import { StoreToken } from "tokens/redux-store.token";
import { IStore } from "interfaces/redux.interface";

// Services
import { Permissions } from "modules/shared/services/helper/permissions.service";
import { ConfirmModalType, OtModalService } from "@onetrust/vitreus";
import { PrivacyNoticeEffects } from "modules/policy/privacy-notice/shared/services/privacy-notice-effects";
import { PrivacyNoticeService } from "modules/policy/privacy-notice/shared/services/privacy-notice.service";

// Enums
import { BuilderEditingMode } from "modules/policy/privacy-notice/shared/enums/builder-editing-mode.enum";
import { PolicyStatus } from "modules/policy/privacy-notice/shared/enums/policy-status.enum";

// Components
import { PrivacyNoticePublishScriptModalComponent } from "modules/policy/privacy-notice/privacy-notice-details/privacy-notice-publish-script-modal/privacy-notice-publish-script-modal.component";

@Component({
    selector: "privacy-notice-details",
    templateUrl: "./privacy-notice-details.component.html",
})
export class PrivacyNoticeDetailsComponent implements OnInit {
    privacyNotice$: Observable<PrivacyNotice>;

    tabs: ITabsNav[];
    currentTab: string;
    drawerOpen: boolean;
    isInactive = false;
    isValid = true;
    policyConstants = PolicyConstants;
    selectedEditingMode = BuilderEditingMode.WYSIWYG;
    unRegistrationHook: () => void;

    isActionInProgress$ = this.effects.isActionInProgress$.pipe(share());
    hasPendingChanges$ = this.effects.hasPendingChanges$.pipe(share());

    private hasPendingChangesSync: boolean;
    private privacyNoticeUpdates$ = new BehaviorSubject<PrivacyNotice>(undefined);
    private destroy$ = new Subject();

    constructor(
        @Inject(StoreToken) private store: IStore,
        private stateService: StateService,
        private permissions: Permissions,
        private otModalService: OtModalService,
        private transitionService: TransitionService,
        private translatePipe: TranslatePipe,
        private privacyNoticeService: PrivacyNoticeService,
        private effects: PrivacyNoticeEffects,
    ) {
    }

    ngOnInit() {
        const privacyNoticeUpdates$ = this.privacyNoticeUpdates$.asObservable();

        this.privacyNotice$ = merge(this.getPrivacyNotice(), privacyNoticeUpdates$).pipe(
            filter(Boolean),
            shareReplay(1),
        );

        this.unRegistrationHook = this.getRegistrationHook();
        this.configureTabs(this.stateService.params.tab);
        this.setCurrentVersion();

        this.hasPendingChanges$.pipe(
            takeUntil(this.destroy$),
        ).subscribe((hasPendingChanges) => {
            this.hasPendingChangesSync = hasPendingChanges;
        });
    }

    ngOnDestroy() {
        this.unRegistrationHook();
        this.destroy$.next();
        this.destroy$.unsubscribe();
    }

    toggleDrawer() {
        this.drawerOpen = !this.drawerOpen;
    }

    onEditModeChanged(selectedEditingMode: BuilderEditingMode) {
        this.selectedEditingMode = selectedEditingMode;
    }

    onPublishRequested(id: string) {
        this.publishVersion(id);
    }

    onDeleteRequested(version: PolicyVersion) {
        this.displayConfirmDeleteVersionModal().pipe(
            filter(Boolean),
        ).subscribe(() => {
            this.deleteVersion(version.id);
        });
    }

    onCreateNewVersionRequested(payload: CreateNewVersionDto) {
        this.createNewVersion(payload)
            .pipe(
                filter(Boolean),
                mergeMap((version: PolicyVersion): Observable<Partial<PrivacyNotice>> => {
                    return this.privacyNotice$.pipe(
                        take(1),
                        map((privacyNotice: PrivacyNotice): Partial<PrivacyNotice> => {
                            return {
                                draftVersion: version,
                                versions: [...privacyNotice.versions, version],
                            };
                        }),
                    );
                }),
                takeUntil(this.destroy$),
            )
            .subscribe((privacyNotice: PrivacyNotice) => {
                this.updatePrivacyNotice(privacyNotice);
            });
    }

    onSaveRequested() {
        this.effects.savePrivacyPolicy();
    }

    onResetRequested() {
        this.effects.resetChanges();
    }

    async goToVersion(versionId: string) {
        const confirmDiscardChanges = await this.confirmDiscardChanges();

        if (!confirmDiscardChanges) {
            return;
        }

        this.drawerOpen = false;
        this.effects.actionInProgress();
        this.effects.hasNoPendingChanges();

        this.privacyNoticeService
            .getPrivacyNoticeVersion(versionId)
            .pipe(
                takeUntil(this.destroy$),
                filter(Boolean),
            )
            .subscribe((version: PolicyVersion) => {
                this.swapVersion(version);
                this.effects.actionNotInProgress();
            });
    }

    private setCurrentVersion(version?: string) {
        const queryParamVersion = this.stateService.params["version"];

        if (!version && queryParamVersion) {
            return;
        }

        const updateVersionQueryParam = (value: string) => {
            if (queryParamVersion === value) {
                return;
            }

            this.stateService.go(".", { version: value });
        };

        if (!version) {
            this.privacyNotice$.pipe(
                take(1),
            ).subscribe((privacyNotice: PrivacyNotice) => {
                updateVersionQueryParam(
                    privacyNotice.draftVersion ?
                    privacyNotice.draftVersion.id : privacyNotice.activeVersion.id,
                );
            });

            return;
        }

        updateVersionQueryParam(version);
    }

    private getVersions(): Observable<PolicyVersion[]> {
        return this.privacyNotice$.pipe(
            take(1),
            map((privacyNotice) => privacyNotice.versions),
        );
    }

    private onVersionPublished(activeVersion: PolicyVersion) {
        this.getVersions().pipe(
            map((versions: PolicyVersion[]): PolicyVersion[] => {
                return versions.map((version: PolicyVersion) => {
                    if (version.id === activeVersion.id) {
                        return activeVersion;
                    }

                    if (version.status === PolicyStatus.ACTIVE) {
                        return { ...version, status: PolicyStatus.RETIRED };
                    }

                    return version;
                });
            }),
        ).subscribe((versions: PolicyVersion[]) => {
            this.updatePrivacyNotice({
                activeVersion,
                draftVersion: undefined,
                versions,
            });

            this.showPublishScriptModal(activeVersion);
        });
    }

    private onVersionRemoved(privacyNotice: PrivacyNotice, versionId: string) {
        const versions = privacyNotice.versions.filter((version) => version.id !== versionId);

        this.updatePrivacyNotice({
            ...privacyNotice,
            versions,
            draftVersion: undefined,
        });
    }

    private swapVersion(version: PolicyVersion) {
        const isActive = version.status === PolicyStatus.ACTIVE;
        const properties = isActive ? {
            activeVersion: version,
        } : { draftVersion: version };

        this.setCurrentVersion(version.id);
        this.updatePrivacyNotice(properties);
    }

    private updatePrivacyNotice(properties: Partial<PrivacyNotice>) {
        if (this.privacyNoticeUpdates$.value) {
            return this.privacyNoticeUpdates$.next({
                ...this.privacyNoticeUpdates$.value,
                ...properties,
            });
        }

        this.privacyNotice$.pipe(
            take(1),
        ).subscribe((privacyNotice: PrivacyNotice) => {
            this.privacyNoticeUpdates$.next({
                ...privacyNotice,
                ...properties,
            });
        });
    }

    private publishVersion(versionId: string) {
        this.privacyNoticeService
            .publishPrivacyNotice(versionId)
            .pipe(
                takeUntil(this.destroy$),
                filter(Boolean),
            )
            .subscribe((activeVersion: PolicyVersion) => {
                this.onVersionPublished(activeVersion);
            });
    }

    private deleteVersion(versionId: string) {
        this.privacyNoticeService
            .deleteVersion(versionId)
            .pipe(
                takeUntil(this.destroy$),
                withLatestFrom(this.privacyNotice$),
                map(([_, privacyNotice]: [PolicyVersion, PrivacyNotice]) => {
                    this.onVersionRemoved(privacyNotice, versionId);
                    return privacyNotice;
                }),
            )
            .subscribe(async (privacyNotice: PrivacyNotice) => {
                await this.goToVersion(privacyNotice.activeVersion.id);
            });
    }

    private createNewVersion(payload: CreateNewVersionDto): Observable<PolicyVersion |  void> {
        return this.privacyNotice$.pipe(
            take(1),
            map((privacyNotice: PrivacyNotice) => privacyNotice.id),
            mergeMap((id: string) => {
                return this.privacyNoticeService.createNewVersion(id, payload);
            }),
            takeUntil(this.destroy$),
        );
    }

    private showPublishScriptModal(version: PolicyVersion) {
        this.otModalService.create(PrivacyNoticePublishScriptModalComponent, { isComponent: true }, version);
    }

    private getRegistrationHook(): () => void {
        return this.transitionService.onExit({}, () => this.confirmDiscardChanges()) as () => void;
    }

    private confirmDiscardChanges(): Promise<boolean> {
        const executeTransition = () => {
            this.effects.hasNoPendingChanges();
            return true;
        };

        if (this.hasPendingChangesSync) {
            return this.displayPendingChangesModal()
                .pipe(takeUntil(this.destroy$))
                .toPromise()
                .then((confirmed: boolean) => {
                    return confirmed && executeTransition();
                });
        }

        return Promise.resolve(true);
    }

    private configureTabs(currentTab: string) {
        this.currentTab = currentTab || "builder";

        this.tabs = [
            {
                text: this.translatePipe.transform("Builder"),
                id: "builder",
            },
            {
                text: this.translatePipe.transform("Integrations"),
                id: "integrations",
            },
            {
                text: this.translatePipe.transform("Settings"),
                id: "settings",
            },
        ];

        this.tabs.forEach((option: ITabsNav) => {
            option.action = async (tabOptions: ITabsNav) => {
                const canChangeTab = await this.confirmDiscardChanges();
                return canChangeTab && this.changeTab(tabOptions);
            };
        });
    }

    private getPrivacyNotice(): Observable<PrivacyNotice> {
        const id = this.stateService.params.id;
        return this.privacyNoticeService.getPrivacyNotice(id);
    }

    private changeTab(option: ITabsNav) {
        const id = this.stateService.params.id;

        this.currentTab = option.id;
        this.stateService.go(".", { Id: id, tab: option.id });
    }

    private displayConfirmDeleteVersionModal(): Observable<boolean> {
        return this.otModalService.confirm({
            type: ConfirmModalType.WARNING,
            translations: {
                title: this.translatePipe.transform("PrivacyPolicyDeletingVersion"),
                desc: this.translatePipe.transform("PrivacyPolicyDeletingVersionDesc"),
                confirm: this.translatePipe.transform("AreYouSureContinue"),
                submit: this.translatePipe.transform("Delete"),
                cancel: this.translatePipe.transform("Cancel"),
            },
        }).pipe(
            takeUntil(this.destroy$),
            take(1),
        );
    }

    private displayPendingChangesModal(): Observable<boolean> {
        return this.otModalService.confirm({
            type: ConfirmModalType.WARNING,
            translations: {
                title: this.translatePipe.transform("UnsavedChangesTitle"),
                desc: this.translatePipe.transform("UnsavedChangesDescription"),
                confirm: this.translatePipe.transform("AreYouSureContinue"),
                submit: this.translatePipe.transform("DiscardChanges"),
                cancel: this.translatePipe.transform("Cancel"),
            },
        }).pipe(
            takeUntil(this.destroy$),
            take(1),
        );
    }
}
