import {
    Component,
    Input,
} from "@angular/core";

// Models
import { PrivacyNotice } from "modules/policy/privacy-notice/shared/models/privacy-notice";

@Component({
    selector: "privacy-notice-builder-sidebar",
    templateUrl: "./privacy-notice-builder-sidebar.component.html",
})

export class PrivacyNoticeBuilderSidebarComponent {
    @Input() privacyNotice: PrivacyNotice;

    loading: boolean;
    isInactive: boolean;

    constructor() {}
}
