// Angular
import { Component, Inject, Input, OnInit, ViewChild } from "@angular/core";
import { ControlValueAccessor } from "@angular/forms";

// Redux
import { StoreToken } from "tokens/redux-store.token";
import { IStore } from "interfaces/redux.interface";

// Models
import { PrivacyNotice } from "modules/policy/privacy-notice/shared/models/privacy-notice";
import { PrivacyPolicyLanguage } from "modules/policy/privacy-notice/shared/models/privacy-policy-language";
import { PrivacyPolicyContent } from "modules/policy/privacy-notice/shared/models/privacy-policy-content";
import { CreatePolicyContentsRequestDto } from "modules/policy/privacy-notice/shared/models/create-policy-contents-request-dto";

// Enums
import { BuilderEditingMode } from "modules/policy/privacy-notice/shared/enums/builder-editing-mode.enum";
import { PolicyVersion } from "modules/policy/privacy-notice/shared/models/policy-version";
import { PrivacyPolicyContentSection } from "modules/policy/privacy-notice/shared/enums/policy-content-section.enum";
import { PolicyStatus } from "modules/policy/privacy-notice/shared/enums/policy-status.enum";

// Services
import { PrivacyNoticeService } from "modules/policy/privacy-notice/shared/services/privacy-notice.service";
import { PrivacyNoticeEffects } from "modules/policy/privacy-notice/shared/services/privacy-notice-effects";
import { StateService } from "@uirouter/angular";

// rxjs
import {
    BehaviorSubject,
    merge,
    Observable,
    Subject,
} from "rxjs";

import {
    map,
    mergeMap,
    share,
    shareReplay,
    switchMap,
    take,
    takeUntil,
    withLatestFrom,
    filter,
    tap,
} from "rxjs/operators";

interface IRichTextEditor extends ControlValueAccessor {
    contentModel: string;
}

@Component({
    selector: "privacy-notice-builder",
    templateUrl: "./privacy-notice-builder.component.html",
})
export class PrivacyNoticeBuilderComponent implements OnInit {
    @Input() editingMode: BuilderEditingMode;

    @Input()
    set privacyNotice(privacyNotice: PrivacyNotice) {
        this._privacyNotice = privacyNotice;

        if (privacyNotice && this.version) {
            this.language$ = this.getLanguage();
            this.content$ = this.getContents();
        }
    }

    get privacyNotice(): PrivacyNotice {
        return this._privacyNotice;
    }

    content$: Observable<string>;
    isInactive: boolean;

    readonly modules = getQuillCustomModules();
    readonly editingModes = BuilderEditingMode;
    readonly policyStatus = PolicyStatus;

    @ViewChild("richTextEditor")
    private editor: IRichTextEditor;

    private _privacyNotice: PrivacyNotice;
    private destroy$ = new Subject<void>();
    private contentChanges$ = new BehaviorSubject<string>("");
    private language$: Observable<PrivacyPolicyLanguage>;
    private _snapshot: PrivacyPolicyContent;

    constructor(
        @Inject(StoreToken) private store: IStore,
        private stateService: StateService,
        private service: PrivacyNoticeService,
        private effects: PrivacyNoticeEffects,
    ) {
    }

    get version(): PolicyVersion {
        return this.service.getCurrentVersion(this.privacyNotice.versions);
    }

    ngOnInit() {
        this.subscribeToContentChanges();
        this.subscribeToSaveRequests();
        this.subscribeToResetRequests();
    }

    ngOnDestroy() {
        this.destroy$.next();
        this.destroy$.complete();
    }

    onContentChanged(content: string) {
        this.contentChanges$.next(content);
    }

    private getLanguage(): Observable<PrivacyPolicyLanguage> {
        return this.getPrivacyNoticeVersion().pipe(
            mergeMap((version: PolicyVersion) => {
                const language = version.languages.find((versionLanguage) => {
                    return versionLanguage.defaultLanguage;
                });
                return this.getPrivacyNoticeLanguageDetails(language.id);
            }),
            shareReplay(),
        );
    }

    private getContents() {
        const EMPTY_CONTENT = "";
        const INITIAL_SECTION: PrivacyPolicyContent = {
            content: EMPTY_CONTENT,
            section: PrivacyPolicyContentSection.Body,
        };

        const initialBodyContent$ = this.language$.pipe(
            map((details: PrivacyPolicyLanguage) => {
                const section = details.contents
                    .find((content: PrivacyPolicyContent) => {
                        return content.section === PrivacyPolicyContentSection.Body;
                    });

                const snapshot = section || INITIAL_SECTION;
                this.storeSnapshot(snapshot);

                return section ? section.content : EMPTY_CONTENT;
            }),
            tap((content: string) => this.writeEditorValue(content)),
        );

        const bodyTextChanges$ = this.contentChanges$.pipe(
            filter(Boolean),
        );

        return merge(initialBodyContent$, bodyTextChanges$).pipe(
            shareReplay(),
        );
    }

    private getPrivacyNoticeVersion(): Observable<PolicyVersion> {
        return this.service.getPrivacyNoticeVersion(this.version.id);
    }

    private getPrivacyNoticeLanguageDetails(languageId: string): Observable<PrivacyPolicyLanguage> {
        return this.service.getPrivacyNoticeContents(languageId);
    }

    private subscribeToContentChanges() {
        this.contentChanges$.pipe(
            takeUntil(this.destroy$),
            map((content: string) => {
                return this._snapshot && content !== this._snapshot.content;
            }),
        ).subscribe((contentHasChanged: boolean) => {
            return contentHasChanged ?
                this.effects.hasPendingChanges() :
                this.effects.hasNoPendingChanges();
        });
    }

    private subscribeToSaveRequests() {
        this.effects.onSave$.pipe(
            takeUntil(this.destroy$),
            switchMap(() => {
                return this.language$.pipe(
                    withLatestFrom(this.content$),
                    take(1),
                );
            }),
            switchMap(([privacyPolicyLanguage, content]: [PrivacyPolicyLanguage, string]) => {
                const id = privacyPolicyLanguage.id;
                const section = {
                    content,
                    section: PrivacyPolicyContentSection.Body,
                };
                const payload: CreatePolicyContentsRequestDto = { contents: [section] };

                this.storeSnapshot(section);
                return this.service.savePrivacyNoticeContents(id, payload);
            }),
            share(),
        ).subscribe(() => {
            this.effects.hasNoPendingChanges();
            this.effects.actionNotInProgress();
        });
    }

    private subscribeToResetRequests() {
        this.effects.resetChanges$
            .pipe(
                takeUntil(this.destroy$),
            )
            .subscribe(() => {
                this.onContentChanged(this._snapshot.content);
                // Workaround: rich-text-editor is unable to receive programmatic updates - which is why we fix it this way
                this.writeEditorValue(this._snapshot.content);
            });
    }

    private writeEditorValue(value: string) {
        if (!this.editor) {
            return;
        }

        this.editor.contentModel = value;
        this.editor.writeValue(value);
    }

    private storeSnapshot(content: PrivacyPolicyContent) {
        this._snapshot = content;
    }
}

function getQuillCustomModules() {
    return [
        { header: [1, 2, 3, 4, 5, 6, false] },
        { font: [] },
        { size: [ false, "small", "large", "huge"] },
        "bold",
        "italic",
        "underline",
        { color: [] },
        { background: [] },
        "blockquote",
        "link",
        { indent: "-1" },
        { indent: "+1" },
        { list: "ordered" },
        { list: "bullet" },
        { align: [] },
        { direction: "rtl" },
        "image",
        "clean",
    ];
}
