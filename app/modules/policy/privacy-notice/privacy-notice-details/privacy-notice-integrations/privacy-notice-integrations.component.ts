// Angular
import { ChangeDetectionStrategy, Component, Inject, Input } from "@angular/core";

// Redux
import { StoreToken } from "tokens/redux-store.token";
import { IStore } from "interfaces/redux.interface";

// Models
import { PrivacyNotice } from "modules/policy/privacy-notice/shared/models/privacy-notice";
import { PrivacyNoticeIntegrationSettings } from "modules/policy/privacy-notice/shared/enums/privacy-notice-integration-settings";

// Services
import { PrivacyNoticeService } from "modules/policy/privacy-notice/shared/services/privacy-notice.service";

// rxjs
import { Observable, throwError } from "rxjs";
import { shareReplay } from "rxjs/operators";

@Component({
    selector: "privacy-notice-integrations",
    templateUrl: "./privacy-notice-integrations.component.html",
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PrivacyNoticeIntegrationsComponent {
    @Input() privacyNotice: PrivacyNotice;

    settings$: Observable<PrivacyNoticeIntegrationSettings>;

    constructor(
        @Inject(StoreToken) private store: IStore,
        private service: PrivacyNoticeService,
    ) {}

    ngOnInit() {
        this.settings$ = this.getIntegrationsSettings().pipe(
            shareReplay(1),
        );
    }

    private getIntegrationsSettings(): Observable<PrivacyNoticeIntegrationSettings | undefined> {
        const versionId = this.privacyNotice.activeVersion && this.privacyNotice.activeVersion.id;

        if (!versionId) {
            return throwError(undefined);
        }

        return this.service.getPrivacyNoticeIntegrationSettings(versionId);
    }
}
