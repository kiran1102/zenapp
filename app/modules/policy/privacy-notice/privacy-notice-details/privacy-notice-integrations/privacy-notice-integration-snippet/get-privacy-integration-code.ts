declare interface IIntegrationScriptPayload {
    url: string;
    id: string;
    scriptLocation: string;
}

// Constants
const NOTICE_CONTAINER_ID_PREFIX = "notice-";
const NOTICE_SCRIPT_ID = "privacy-notice-script";
const CHARSET = "UTF-8";
const SCRIPT_TYPE = "text/javascript";

export function getPrivacyIntegrationCode(payload: IIntegrationScriptPayload): string {
    return `
<!-- OneTrust Privacy Notice start -->

<!-- Container in which the privacy notice will be rendered -->
<div id="${NOTICE_CONTAINER_ID_PREFIX}${payload.id}"></div>

<script src="${payload.scriptLocation}" type="${SCRIPT_TYPE}" charset="${CHARSET}" id="${NOTICE_SCRIPT_ID}"></script>

<script type="${SCRIPT_TYPE}" charset="${CHARSET}">
    // To ensure external settings are loaded, use the Initialized promise:
    OneTrust.NoticeApi.Initialized.then(function() {
        OneTrust.NoticeApi.LoadNotices(["${payload.url}"]);
    });
</script>

<!-- OneTrust Privacy Notice end -->
    `.trim();
}
