// Angular
import {
    Component,
    ChangeDetectionStrategy,
    Input,
    OnInit,
    AfterViewInit,
} from "@angular/core";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

// Vitreus
import { ToastService } from "@onetrust/vitreus";

// Utilities
import Utilities from "Utilities";
import { getPrivacyIntegrationCode } from "modules/policy/privacy-notice/privacy-notice-details/privacy-notice-integrations/privacy-notice-integration-snippet/get-privacy-integration-code";

declare function prettyPrint(): void;

@Component({
    selector: "privacy-notice-integration-snippet",
    templateUrl: "./privacy-notice-integration-snippet.component.html",
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PrivacyNoticeIntegrationSnippetComponent implements OnInit, AfterViewInit {
    @Input() id: string;
    @Input() contentUrl: string;
    @Input() scriptUrl: string;

    snippet: string;

    constructor(
        private toastService: ToastService,
        private translatePipe: TranslatePipe,
    ) {}

    ngOnInit() {
        this.snippet = getPrivacyIntegrationCode({
            url: this.contentUrl,
            id: this.id,
            scriptLocation: this.scriptUrl,
        });
    }

    ngAfterViewInit() {
        prettyPrintSnippet();
    }

    copyPrivacyNoticeScript() {
        Utilities.copyToClipboard(this.snippet);

        this.toastService.show(
            this.translatePipe.transform("LinkCopied"),
            this.translatePipe.transform("PrivacyNoticeScriptCopiedSuccessfully"),
            "success",
        );
    }
}

function prettyPrintSnippet() {
    if (prettyPrint && typeof prettyPrint === "function") {
        prettyPrint();
    }
}
