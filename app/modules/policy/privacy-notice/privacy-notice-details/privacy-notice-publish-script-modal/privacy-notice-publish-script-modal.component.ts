// Angular
import {
    Component,
    Inject,
    Input,
} from "@angular/core";

// Redux
import { StoreToken } from "tokens/redux-store.token";
import { IStore } from "interfaces/redux.interface";

// Models
import { PrivacyNotice } from "modules/policy/privacy-notice/shared/models/privacy-notice";
import { PolicyVersion } from "modules/policy/privacy-notice/shared/models/policy-version";

// Rxjs
import { Subject } from "rxjs";

// Vitreus
import { ConfirmModalType } from "@onetrust/vitreus";

@Component({
    selector: "privacy-notice-publish-script-modal",
    templateUrl: "./privacy-notice-publish-script-modal.component.html",
})
export class PrivacyNoticePublishScriptModalComponent {
    @Input() privacyNotice: PrivacyNotice;

    isInactive: boolean;
    otModalCloseEvent: Subject<PrivacyNotice>;
    otModalData: PolicyVersion;
    type = ConfirmModalType.SUCCESS;

    constructor(
        @Inject(StoreToken) private store: IStore,
    ) {}

    closeModal() {
        this.otModalCloseEvent.next();
    }
}
