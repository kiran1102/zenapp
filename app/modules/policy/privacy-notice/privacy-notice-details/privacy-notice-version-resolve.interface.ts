export interface IVersionListItem {
    id: string;
    label: string;
    status: string;
    version: string;
    createdBy: string;
    // Change to Date
    createdDate: string;
    updatedBy: string;
    // Change to Date
    updatedDate: string;
    isExpanded?: boolean;
}

export interface IVersionGroup {
    title: string;
    versions: IVersionListItem[];
}
