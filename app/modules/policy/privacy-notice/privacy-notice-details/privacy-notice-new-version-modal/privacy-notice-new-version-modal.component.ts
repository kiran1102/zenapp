// Angular
import { Component } from "@angular/core";

// Models
import { PrivacyNotice } from "modules/policy/privacy-notice/shared/models/privacy-notice";

// Components
import { CreatePrivacyNoticeModalComponent } from "modules/policy/privacy-notice/privacy-notice-list/create-privacy-notice-modal/create-privacy-notice-modal.component";

@Component({
    selector: "privacy-notice-new-version-modal",
    templateUrl: "./privacy-notice-new-version-modal.component.html",
})
export class PrivacyNoticeNewVersionModalComponent extends CreatePrivacyNoticeModalComponent {
    otModalData: PrivacyNotice;

    ngOnInit() {
        super.ngOnInit();

        const { name, description, publicVersion } = this.otModalData.activeVersion;

        this.privacyNotice = {
            name,
            publicVersion: publicVersion || "",
            description,
            organizationId: undefined,
        };
    }

    confirm() {
        this.otModalCloseEvent.next(this.privacyNotice);
    }

    get isCreateNewVersionButtonEnabled() {
        return this.hasVersionChanged() ||
            this.hasNameChanged() ||
            this.hasDescriptionChanged();
    }

    private hasVersionChanged(): boolean {
        const publicVersion = this.otModalData.activeVersion.publicVersion;
        return publicVersion && publicVersion.trim() !== this.privacyNotice.publicVersion.trim();
    }

    private hasNameChanged(): boolean {
        return this.otModalData.activeVersion.name.trim() !== this.privacyNotice.name.trim();
    }

    private hasDescriptionChanged(): boolean {
        return this.otModalData.activeVersion.description.trim() !== this.privacyNotice.description.trim();
    }
}
