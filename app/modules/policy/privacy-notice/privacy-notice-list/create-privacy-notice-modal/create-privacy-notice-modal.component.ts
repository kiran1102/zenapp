// Angular
import {
    Component,
    Inject,
    OnInit,
} from "@angular/core";

// Third Party
import { StateService } from "@uirouter/core";

// Interfaces
import { IOrganization } from "interfaces/org.interface";
import { IStore } from "interfaces/redux.interface";

// Redux
import { StoreToken } from "tokens/redux-store.token";
import { getCurrentOrgList } from "oneRedux/reducers/org.reducer";

// Rxjs
import { Subject } from "rxjs";
import { finalize, filter } from "rxjs/operators";

// Services
import { PrivacyNoticeService } from "modules/policy/privacy-notice/shared/services/privacy-notice.service";

// Enums
import { PolicyRoutes } from "modules/policy/privacy-notice/shared/enums/routes.enum";

// Types
import { PrivacyNotice } from "modules/policy/privacy-notice/shared/models/privacy-notice";
import { CreatePrivacyNoticeDto } from "modules/policy/privacy-notice/shared/models/create-privacy-notice-dto";
import { CreateNewVersionDto } from "modules/policy/privacy-notice/shared/models/create-new-version-dto";

@Component({
    selector: "create-privacy-notice-modal",
    templateUrl: "./create-privacy-notice-modal.component.html",
})
export class CreatePrivacyNoticeModalComponent implements OnInit {
    createInProgress: boolean;
    organizations: IOrganization[];
    privacyNotice = new CreatePrivacyNoticeDto();
    selectedOrganization: IOrganization;
    otModalCloseEvent: Subject<CreatePrivacyNoticeDto | CreateNewVersionDto | PrivacyNotice>;

    constructor(
        @Inject(StoreToken) private store: IStore,
        private stateService: StateService,
        private privacyNoticeService: PrivacyNoticeService,
    ) { }

    ngOnInit() {
        this.organizations = getCurrentOrgList(this.store.getState());
        this.selectedOrganization = this.organizations[0];
    }

    closeModal(privacyNotice?: PrivacyNotice) {
        this.otModalCloseEvent.next(privacyNotice);
    }

    onModelChange(payload: string, prop: keyof CreatePrivacyNoticeDto) {
        this.privacyNotice[prop] = payload;
    }

    selectOrganization(organization: IOrganization) {
        this.selectedOrganization = organization;
    }

    createPrivacyNotice() {
        this.privacyNotice.organizationId = this.selectedOrganization.Id;

        if (this.privacyNotice.publicVersion) {
            this.privacyNotice.publicVersion = this.privacyNotice.publicVersion.trim();
        }

        this.createInProgress = true;

        this.privacyNoticeService
            .createPrivacyNotice(this.privacyNotice)
            .pipe(
                finalize(() => this.createInProgress = false),
                filter(Boolean),
            )
            .subscribe((result: PrivacyNotice) => this.onPrivacyCreated(result));
    }

    hasEmptyField(field: keyof CreatePrivacyNoticeDto): boolean {
        return !this.privacyNotice[field] || !this.privacyNotice[field].trim();
    }

    get isSubmitButtonDisabled(): boolean {
        const hasEmptyFields = this.hasEmptyField("name") || this.hasEmptyField("description");
        return this.createInProgress || hasEmptyFields || !this.isPublicVersionValid;
    }

    get isPublicVersionValid(): boolean {
        const version = this.privacyNotice.publicVersion;
        return !version || this.privacyNoticeService.validatePublicVersion(version.trim());
    }

    private onPrivacyCreated(privacyNotice: PrivacyNotice) {
        const tab = "builder";
        const id = privacyNotice.id;

        this.closeModal(privacyNotice);
        this.stateService.go(PolicyRoutes.PolicyNoticeDetails, { id, tab });
    }
}
