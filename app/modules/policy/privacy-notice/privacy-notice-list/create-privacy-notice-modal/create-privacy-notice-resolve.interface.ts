export interface ICreatePrivacyNoticeModalData {
    Name: string;
    OrganizationId: string;
    Description: string;
    PublicVersion: string;
}
