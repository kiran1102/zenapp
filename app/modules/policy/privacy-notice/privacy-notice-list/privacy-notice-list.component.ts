// Third Party
import {
    Component,
    Inject,
    OnInit,
} from "@angular/core";

import { StateService } from "@uirouter/core";

// Rxjs
import { Observable, Subject } from "rxjs";
import { takeUntil, shareReplay } from "rxjs/operators";

// Models
import { ColumnDefinition } from "crmodel/column-definition";
import { PrivacyNotice } from "../shared/models/privacy-notice";
import { IBasicTableCell } from "interfaces/tables.interface";
import { PagedList } from "crmodel/paged-list";
import { policyStatusLabel } from "modules/policy/privacy-notice/shared/models/policy-status-style";

// Components
import { CreatePrivacyNoticeModalComponent } from "./create-privacy-notice-modal/create-privacy-notice-modal.component";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

// Services
import { PrivacyNoticeService } from "modules/policy/privacy-notice/shared/services/privacy-notice.service";
import { OtModalService } from "@onetrust/vitreus";

// Enums
import { PolicyRoutes } from "modules/policy/privacy-notice/shared/enums/routes.enum";

// Redux
import { StoreToken } from "tokens/redux-store.token";
import { IStore } from "interfaces/redux.interface";
import { getCurrentOrgList } from "oneRedux/reducers/org.reducer";

@Component({
    selector: "privacy-notice-list",
    templateUrl: "./privacy-notice-list.component.html",
})
export class PrivacyNoticeListComponent implements OnInit {
    columns: ColumnDefinition[];
    isDrawerOpen: boolean;
    privacyPolicies$: Observable<PagedList<PrivacyNotice>>;

    private destroy$ = new Subject();

    constructor(
        @Inject(StoreToken) private store: IStore,
        private stateService: StateService,
        private privacyNoticeService: PrivacyNoticeService,
        private otModalService: OtModalService,
        private translatePipe: TranslatePipe,
    ) {
    }

    ngOnInit() {
        const getVersion = (row: PrivacyNotice) => row.draftVersion || row.activeVersion;

        const organizations = getCurrentOrgList(this.store.getState());

        this.privacyPolicies$ = this.privacyNoticeService
            .getAllPrivacyPolicies()
            .pipe(
                shareReplay(),
            );

        this.columns = [
            {
                columnName: this.translatePipe.transform("Select"),
                cellValueKey: "id",
                columnType: "selectRow",
            },
            {
                columnName: this.translatePipe.transform("Name"),
                rowToText: (row: PrivacyNotice) => getVersion(row).name,
                cellClass: "max-width-50",
                columnType: "idLink",
                route: PolicyRoutes.PolicyNoticeDetails,
            },
            {
                columnName: this.translatePipe.transform("Status"),
                rowToText: (row: PrivacyNotice) => {
                    const status = getVersion(row).status;
                    const translation = policyStatusLabel[status];
                    return this.translatePipe.transform(translation);
                },
                columnType: "statusLabel",
            },
            {
                columnName: this.translatePipe.transform("Version"),
                cellValueKey: "Version",
                columnType: "versionTag",
                rowToText: (row: PrivacyNotice): string => `V${getVersion(row).version}`,
            },
            {
                columnName: this.translatePipe.transform("PublicVersion"),
                rowToText: (row: PrivacyNotice) => getVersion(row).publicVersion,
            },
            {
                columnName: this.translatePipe.transform("LastPublished"),
                rowToText: (row: PrivacyNotice) => getVersion(row).lastModifiedDate,
                columnType: "timestamp",
            },
            {
                columnName: this.translatePipe.transform("Organization"),
                rowToText: (row: PrivacyNotice) => {
                    const organization = organizations.find((item) => item.Id === row.organizationId);
                    return organization ? organization.Name : "";
                },
            },
        ];
    }

    ngOnDestroy() {
        this.destroy$.next();
        this.destroy$.complete();
    }

    goToRoute(cell: IBasicTableCell) {
        this.stateService.go(cell.route, cell.params);
    }

    createPolicyModal() {
        // TODO: call policy notice creation modal
        this.otModalService.create(
            CreatePrivacyNoticeModalComponent,
            {
                isComponent: true,
            },
        ).pipe(
            takeUntil(this.destroy$),
        );
    }
}
