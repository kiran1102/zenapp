// Angular
import { NgModule } from "@angular/core";

// Components
import { PrivacyNoticeListComponent } from "./privacy-notice-list.component";
import { CreatePrivacyNoticeModalComponent } from "./create-privacy-notice-modal/create-privacy-notice-modal.component";

// Modules
import { PrivacyNoticeSharedModule } from "modules/policy/privacy-notice/shared/privacy-notice-shared.module";
import { SharedModule } from "sharedModules/shared.module";
import { UpgradedLegacyModule } from "sharedModules/upgrade/upgraded-legacy.module";
import { CommonModule } from "@angular/common";
import { HttpClientModule } from "@angular/common/http";
import { FormsModule } from "@angular/forms";
import { ReactiveFormsModule } from "@angular/forms";
import { VitreusModule } from "@onetrust/vitreus";
import { DndListModule } from "ngx-drag-and-drop-lists";
import { OTPipesModule, PIPES } from "modules/pipes/pipes.module";
import { VirtualScrollModule } from "od-virtualscroll";
import { OneVideoModule } from "modules/video/video.module";

@NgModule({
    imports: [
        PrivacyNoticeSharedModule,
        SharedModule,
        UpgradedLegacyModule,
        CommonModule,
        HttpClientModule,
        FormsModule,
        ReactiveFormsModule,
        VitreusModule,
        DndListModule,
        OTPipesModule,
        VirtualScrollModule,
        OneVideoModule,
    ],
    declarations: [
        PrivacyNoticeListComponent,
        CreatePrivacyNoticeModalComponent,
    ],
    exports: [
        PrivacyNoticeListComponent,
    ],
    entryComponents: [
        PrivacyNoticeListComponent,
        CreatePrivacyNoticeModalComponent,
    ],
    providers: [
        PIPES, // TODO: Remove from providers when ot-common is implemented. Only necessary if you are using pipe in TS file, else just module is fine
    ],
})
export class PrivacyNoticeListModule { }
