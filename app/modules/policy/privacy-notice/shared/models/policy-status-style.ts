import { PolicyStatus } from "modules/policy/privacy-notice/shared/enums/policy-status.enum";

export const policyStatusLabel = {
    [PolicyStatus.DRAFT]: "Draft",
    [PolicyStatus.ACTIVE]: "Active",
    [PolicyStatus.RETIRED]: "Retired",
};

export const policyStatusStyleClass = {
    [PolicyStatus.DRAFT]: "warning",
    [PolicyStatus.ACTIVE]: "success",
    [PolicyStatus.RETIRED]: "hold",
};
