import { PolicyVersion } from "modules/policy/privacy-notice/shared/models/policy-version";
import { PrivacyNoticeBase } from "modules/policy/privacy-notice/shared/models/privacy-notice-base";

export class PrivacyNotice extends PrivacyNoticeBase {
    id: string;
    lastPublished: string;
    draftVersion: PolicyVersion | undefined;
    activeVersion: PolicyVersion | undefined;
    createdDate: string;
    lastModifiedDate: string;
    versions: PolicyVersion[];
}
