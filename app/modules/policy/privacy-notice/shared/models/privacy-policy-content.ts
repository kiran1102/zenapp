import { PrivacyPolicyContentSection } from "modules/policy/privacy-notice/shared/enums/policy-content-section.enum";

export class PrivacyPolicyContent {
    section: PrivacyPolicyContentSection;
    content: string;
}
