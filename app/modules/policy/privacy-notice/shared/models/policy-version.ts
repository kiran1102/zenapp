import { PolicyStatus } from "modules/policy/privacy-notice/shared/enums/policy-status.enum";
import { PrivacyPolicyLanguage } from "modules/policy/privacy-notice/shared/models/privacy-policy-language";

export class PolicyVersion {
    name: string;
    description: string;
    publicVersion: string;
    version: number;
    status: PolicyStatus;
    id: string;
    createdDate: string;
    lastModifiedDate: string;
    languages: PrivacyPolicyLanguage[];
    url?: string;
}
