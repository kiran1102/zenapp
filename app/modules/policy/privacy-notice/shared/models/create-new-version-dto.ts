export class CreateNewVersionDto {
    name: string;
    description: string;
    publicVersion: string;
    privacyPolicyId: string;
}
