import { PrivacyNoticeBase } from "modules/policy/privacy-notice/shared/models/privacy-notice-base";

export class CreatePrivacyNoticeDto extends PrivacyNoticeBase {
    name: string;
    publicVersion: string;
    description: string;
}
