import { PrivacyPolicyContent } from "modules/policy/privacy-notice/shared/models/privacy-policy-content";
import { PolicyVersion } from "modules/policy/privacy-notice/shared/models/policy-version";

export class PrivacyPolicyLanguage {
    createdDate: string;
    languageCode: string;
    defaultLanguage: boolean;
    id: string;
    contents: PrivacyPolicyContent[];
    lastModifiedDate: string;
    privacyPolicyVersion: PolicyVersion;
}
