export class CreateNewLanguageRequestDto {
    language: string;
    defaultLanguage: boolean;
    versionId: string;
}
