import { PrivacyPolicyContent } from "modules/policy/privacy-notice/shared/models/privacy-policy-content";

export class CreatePolicyContentsRequestDto {
    contents: PrivacyPolicyContent[];
}
