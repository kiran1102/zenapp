export enum BuilderEditingMode {
    WYSIWYG = "WYSIWYG",
    HTML = "HTML",
}
