export enum PrivacyPolicyContentSection {
    Header = "HEADER",
    Footer = "FOOTER",
    Body = "BODY",
}
