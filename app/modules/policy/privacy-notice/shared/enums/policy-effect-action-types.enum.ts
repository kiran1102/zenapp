export enum PolicyEffectActionTypes {
    SaveContents = "SAVE_CONTENTS",
    ActionInProgress = "ACTION_IN_PROGRESS",
    ActionNotInProgress = "ACTION_NOT_IN_PROGRESS",
    HasPendingChanges = "HAS_PENDING_CHANGES",
    HasNoPendingChanges = "HAS_NO_PENDING_CHANGES",
    ResetChanges = "RESET_CHANGES",
}
