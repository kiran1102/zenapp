export enum PolicyEndpoints {
    PrivacyPolicies = "/api/privacynotice/v1/privacypolicies",
    PrivacyPoliciesVersions = "/api/privacynotice/v1/privacypolicies/-/versions",
    PrivacyPoliciesLanguages = "/api/privacynotice/v1/privacypolicies/-/versions/-/languages",
}
