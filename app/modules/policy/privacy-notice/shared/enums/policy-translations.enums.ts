export enum PolicyTranslations {
    CreatePolicySuccess = "CreatePolicySuccess",
    CreatePolicyError = "CreatePolicyError",
    PublishPolicySuccess = "PublishPolicySuccess",
    PublishPolicyError = "PublishPolicyError",
    DeletePolicySuccess = "DeletePolicySuccess",
    DeletePolicyError = "DeletePolicyError",
    CreatePolicyVersionSuccess = "CreatePolicyVersionSuccess",
    CreatePolicyVersionError = "CreatePolicyVersionError",
    SavePolicySuccess = "SavePolicySuccess",
    SavePolicyError = "SavePolicyError",
}
