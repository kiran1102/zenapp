export class PrivacyNoticeIntegrationSettings {
    contentUrl: string;
    scriptUrl: string;
}
