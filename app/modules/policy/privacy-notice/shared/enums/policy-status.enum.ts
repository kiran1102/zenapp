export enum PolicyStatus {
    DRAFT = "DRAFT",
    ACTIVE = "ACTIVE",
    RETIRED = "RETIRED",
}
