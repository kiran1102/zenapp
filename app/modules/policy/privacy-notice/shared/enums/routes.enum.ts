export enum PolicyRoutes {
    Policy = "zen.app.pia.module.policy",
    PolicyNotices = "zen.app.pia.module.policy.privacynotices",
    PolicyNoticesList = "zen.app.pia.module.policy.privacynotices.list",
    PolicyNoticeDetails = "zen.app.pia.module.policy.privacynotices.details",
}
