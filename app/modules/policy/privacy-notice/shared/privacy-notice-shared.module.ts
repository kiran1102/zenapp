// Angular
import { NgModule } from "@angular/core";

// Modules
import { SharedModule } from "sharedModules/shared.module";
import { UpgradedLegacyModule } from "sharedModules/upgrade/upgraded-legacy.module";
import { CommonModule } from "@angular/common";
import { HttpClientModule } from "@angular/common/http";
import { FormsModule } from "@angular/forms";
import { ReactiveFormsModule } from "@angular/forms";
import { VitreusModule } from "@onetrust/vitreus";
import { DndListModule } from "ngx-drag-and-drop-lists";
import { OTPipesModule, PIPES } from "modules/pipes/pipes.module";
import { VirtualScrollModule } from "od-virtualscroll";
import { OneVideoModule } from "modules/video/video.module";
import { PolicySharedModule } from "modules/policy/shared/policy-shared.module";

// Services
import { PrivacyNoticeService } from "modules/policy/privacy-notice/shared/services/privacy-notice.service";
import { PrivacyNoticeApiService } from "modules/policy/privacy-notice/shared/services/privacy-notice-api.service";
import { PrivacyNoticeEffects } from "modules/policy/privacy-notice/shared/services/privacy-notice-effects";

@NgModule({
    imports: [
        SharedModule,
        UpgradedLegacyModule,
        CommonModule,
        HttpClientModule,
        FormsModule,
        ReactiveFormsModule,
        VitreusModule,
        DndListModule,
        OTPipesModule,
        VirtualScrollModule,
        OneVideoModule,
        PolicySharedModule,
    ],
    providers: [
        PrivacyNoticeService,
        PrivacyNoticeEffects,
        PrivacyNoticeApiService,
        PIPES, // TODO: Remove from providers when ot-common is implemented. Only necessary if you are using pipe in TS file, else just module is fine
    ],
    exports: [
        PolicySharedModule,
    ],
})
export class PrivacyNoticeSharedModule { }
