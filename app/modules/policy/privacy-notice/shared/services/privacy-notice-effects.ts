// Angular
import { Injectable } from "@angular/core";

// Rxjs
import { Observable, Subject, merge } from "rxjs";
import { filter, mapTo } from "rxjs/operators";

// Enums
import { PolicyEffectActionTypes } from "modules/policy/privacy-notice/shared/enums/policy-effect-action-types.enum";

const filterByType = (type: PolicyEffectActionTypes) => {
    return filter((action: PolicyEffectActionTypes) => {
        return action === type;
    });
};

@Injectable()
export class PrivacyNoticeEffects {
    onSave$: Observable<PolicyEffectActionTypes>;
    isActionInProgress$: Observable<boolean>;
    hasPendingChanges$: Observable<boolean>;
    resetChanges$: Observable<PolicyEffectActionTypes>;

    private readonly effects$ = new Subject<PolicyEffectActionTypes>();

    constructor() {
        this.onSave$ = this.effects$.pipe(
            filterByType(PolicyEffectActionTypes.SaveContents),
        );

        this.resetChanges$ = this.effects$.pipe(
            filterByType(PolicyEffectActionTypes.ResetChanges),
        );

        this.isActionInProgress$ = this.getActionInProgressObservable();
        this.hasPendingChanges$ = this.getHasPendingChangesObservable();
    }

    public savePrivacyPolicy() {
        this.actionInProgress();
        this.emit(PolicyEffectActionTypes.SaveContents);
    }

    public hasPendingChanges() {
        this.emit(PolicyEffectActionTypes.HasPendingChanges);
    }

    public hasNoPendingChanges() {
        this.emit(PolicyEffectActionTypes.HasNoPendingChanges);
    }

    public actionInProgress() {
        this.emit(PolicyEffectActionTypes.ActionInProgress);
    }

    public actionNotInProgress() {
        this.emit(PolicyEffectActionTypes.ActionNotInProgress);
    }

    public resetChanges() {
        this.emit(PolicyEffectActionTypes.ResetChanges);
        this.hasNoPendingChanges();
    }

    private emit(action: PolicyEffectActionTypes) {
        this.effects$.next(action);
    }

    private getActionInProgressObservable(): Observable<boolean> {
        const actionInProgress$ = this.effects$.pipe(
            filterByType(PolicyEffectActionTypes.ActionInProgress),
            mapTo(true),
        );

        const actionNotInProgress$ = this.effects$.pipe(
            filterByType(PolicyEffectActionTypes.ActionNotInProgress),
            mapTo(false),
        );

        return merge(actionInProgress$, actionNotInProgress$);
    }

    private getHasPendingChangesObservable(): Observable<boolean> {
        const hasPendingChanges$ = this.effects$.pipe(
            filterByType(PolicyEffectActionTypes.HasPendingChanges),
            mapTo(true),
        );

        const hasNoPendingChanges$ = this.effects$.pipe(
            filterByType(PolicyEffectActionTypes.HasNoPendingChanges),
            mapTo(false),
        );

        return merge(hasPendingChanges$, hasNoPendingChanges$);
    }
}
