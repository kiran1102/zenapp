// Angular
import { Injectable } from "@angular/core";

// Interfaces
import { IStringMap } from "interfaces/generic.interface";
import { PolicyVersion } from "modules/policy/privacy-notice/shared/models/policy-version";
import { PagedList } from "crmodel/paged-list";
import { CreateNewVersionDto } from "modules/policy/privacy-notice/shared/models/create-new-version-dto";
import { PrivacyPolicyLanguage } from "modules/policy/privacy-notice/shared/models/privacy-policy-language";
import { PrivacyNotice } from "modules/policy/privacy-notice/shared/models/privacy-notice";
import { CreatePrivacyNoticeDto } from "modules/policy/privacy-notice/shared/models/create-privacy-notice-dto";
import { CreatePolicyContentsRequestDto } from "modules/policy/privacy-notice/shared/models/create-policy-contents-request-dto";
import { PrivacyNoticeIntegrationSettings } from "modules/policy/privacy-notice/shared/enums/privacy-notice-integration-settings";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

// Services
import { PrivacyNoticeApiService } from "modules/policy/privacy-notice/shared/services/privacy-notice-api.service";
import { NotificationService } from "sharedModules/services/provider/notification.service";

// Enums
import { PolicyTranslations } from "modules/policy/privacy-notice/shared/enums/policy-translations.enums";

// rxjs
import { catchError, tap } from "rxjs/operators";
import { Observable, of } from "rxjs";

// Router
import { StateService } from "@uirouter/angular";

@Injectable()
export class PrivacyNoticeService {
    private static VERSION_REGEXP_VALIDATOR = /\b((\d{1,3})\.){2}(\d{1,3})\b/;

    constructor(
        private translatePipe: TranslatePipe,
        private api: PrivacyNoticeApiService,
        private notificationService: NotificationService,
        private stateService: StateService,
    ) {
    }

    getAllPrivacyPolicies(): Observable<PagedList<PrivacyNotice>> {
        return this.api.getAllPrivacyPolicies();
    }

    getPrivacyNoticeContents(languageId: string): Observable<PrivacyPolicyLanguage> {
        return this.api.getPrivacyNoticeContents(languageId);
    }

    getPrivacyNotice(id: string): Observable<PrivacyNotice> {
        return this.api.getPrivacyNotice(id);
    }

    getPrivacyNoticeIntegrationSettings(versionId: string): Observable<PrivacyNoticeIntegrationSettings> {
        return this.api.getPrivacyNoticeIntegrationSettings(versionId);
    }

    getPrivacyNoticeVersion(versionId: string): Observable<PolicyVersion> {
        return this.api.getPrivacyNoticeVersion(versionId);
    }

    createPrivacyNotice(privacy: CreatePrivacyNoticeDto): Observable<PrivacyNotice | void> {
        const onSuccess = () => this.onApiSuccess(PolicyTranslations.CreatePolicySuccess);
        const onError = () => of(this.onApiError(PolicyTranslations.CreatePolicyError));

        return this.api
            .createPrivacyNotice(privacy)
            .pipe(
                tap(onSuccess),
                catchError(onError),
            );
    }

    createNewVersion(privacyPolicyId: string, payload: CreateNewVersionDto): Observable<PolicyVersion | void> {
        const onSuccess = () => this.onApiSuccess(PolicyTranslations.CreatePolicyVersionSuccess);
        const onError = () => of(this.onApiError(PolicyTranslations.CreatePolicyVersionError));

        return this.api
            .createNewVersion(privacyPolicyId, payload)
            .pipe(
                tap(onSuccess),
                catchError(onError),
            );
    }

    deleteVersion(versionId: string): Observable<PolicyVersion | void> {
        const onSuccess = () => this.onApiSuccess(PolicyTranslations.DeletePolicySuccess);
        const onError = () => of(this.onApiError(PolicyTranslations.DeletePolicyError));

        return this.api
            .deleteVersion(versionId)
            .pipe(
                tap(onSuccess),
                catchError(onError),
            );
    }

    savePrivacyNoticeContents(languageId: string, contents: CreatePolicyContentsRequestDto): Observable<void> {
        const onSuccess = () => this.onApiSuccess(PolicyTranslations.SavePolicySuccess);
        const onError = () => of(this.onApiError(PolicyTranslations.SavePolicyError));

        return this.api
            .savePrivacyNoticeContents(languageId, contents)
            .pipe(
                tap(onSuccess),
                catchError(onError),
            );
    }

    publishPrivacyNotice(versionId: string): Observable<PolicyVersion | void> {
        const onSuccess = () => this.onApiSuccess(PolicyTranslations.PublishPolicySuccess);
        const onError = () => of(this.onApiError(PolicyTranslations.PublishPolicyError));

        return this.api
            .publishPrivacyNotice(versionId)
            .pipe(
                tap(onSuccess),
                catchError(onError),
            );
    }

    previewPrivacyNotice() {
        return null;
    }

    getCurrentVersion(versions: PolicyVersion[]): PolicyVersion {
        const currentVersionId = this.stateService.params["version"];
        return versions.find((version: PolicyVersion) => {
            return version.id === currentVersionId;
        });
    }

    translateConstValue(value: string, constType: IStringMap<string>): string {
        if (!constType || !value) {
            return "";
        }

        const keys = Object.keys(constType);
        const constKey = keys.find((key: string) => constType[key] === value);
        return this.translatePipe.transform(constKey);
    }

    validatePublicVersion(version: string): boolean {
        return PrivacyNoticeService.VERSION_REGEXP_VALIDATOR.test(version);
    }

    protected onApiSuccess(message: string) {
        const translated = this.translatePipe.transform(message);
        const title = "Success";
        this.notificationService.alertSuccess(title, translated);
    }

    protected onApiError(message: string) {
        const translated = this.translatePipe.transform(message);
        const title = "Error";
        this.notificationService.alertError(title, translated);
    }
}
