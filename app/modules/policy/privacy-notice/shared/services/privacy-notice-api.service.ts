// Angular
import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";

// Models
import { PolicyVersion } from "modules/policy/privacy-notice/shared/models/policy-version";
import { CreateNewVersionDto } from "modules/policy/privacy-notice/shared/models/create-new-version-dto";
import { PagedList } from "crmodel/paged-list";
import { CreateNewLanguageRequestDto } from "modules/policy/privacy-notice/shared/models/create-new-language-request-dto";
import { CreateNewLanguageResponseDto } from "modules/policy/privacy-notice/shared/models/create-new-language-response-dto";
import { PrivacyPolicyLanguage } from "modules/policy/privacy-notice/shared/models/privacy-policy-language";
import { PrivacyNotice } from "modules/policy/privacy-notice/shared/models/privacy-notice";
import { CreatePrivacyNoticeDto } from "modules/policy/privacy-notice/shared/models/create-privacy-notice-dto";
import { CreatePolicyContentsRequestDto } from "modules/policy/privacy-notice/shared/models/create-policy-contents-request-dto";
import { PrivacyNoticeIntegrationSettings } from "modules/policy/privacy-notice/shared/enums/privacy-notice-integration-settings";

// Rxjs
import { Observable } from "rxjs";

// Enums
import { PolicyEndpoints } from "modules/policy/privacy-notice/shared/enums/endpoints.enum";

@Injectable()
export class PrivacyNoticeApiService {
    constructor(
        private http: HttpClient,
    ) {
    }

    getAllPrivacyPolicies(): Observable<PagedList<PrivacyNotice>> {
        return this.http.get<PagedList<PrivacyNotice>>(PolicyEndpoints.PrivacyPolicies);
    }

    createPrivacyNotice(privacyNotice: CreatePrivacyNoticeDto): Observable<PrivacyNotice> {
        return this.http.post<PrivacyNotice>(PolicyEndpoints.PrivacyPolicies, privacyNotice);
    }

    getPrivacyNoticeContents(languageId: string): Observable<PrivacyPolicyLanguage> {
        const url = `${PolicyEndpoints.PrivacyPolicies}/-/versions/-/languages/${languageId}`;
        return this.http.get<PrivacyPolicyLanguage>(url);
    }

    getPrivacyNotice(privacyNoticeId: string): Observable<PrivacyNotice> {
        const url = `${PolicyEndpoints.PrivacyPolicies}/${privacyNoticeId}`;
        return this.http.get<PrivacyNotice>(url);
    }

    getPrivacyNoticeVersions(privacyNoticeId: string): Observable<PolicyVersion[]> {
        const url = `${PolicyEndpoints.PrivacyPolicies}/${privacyNoticeId}/versions`;
        return this.http.get<PolicyVersion[]>(url);
    }

    getPrivacyNoticeVersion(versionId: string): Observable<PolicyVersion> {
        const url = `${PolicyEndpoints.PrivacyPoliciesVersions}/${versionId}`;
        return this.http.get<PolicyVersion>(url);
    }

    createNewVersion(privacyPolicyId: string, version: CreateNewVersionDto): Observable<PolicyVersion> {
        const url = `${PolicyEndpoints.PrivacyPolicies}/${privacyPolicyId}/versions`;
        return this.http.post<PolicyVersion>(url, version);
    }

    createNewLanguage(privacyPolicyId: string, language: CreateNewLanguageRequestDto): Observable<CreateNewLanguageResponseDto> {
        return this.http.post<CreateNewLanguageResponseDto>(PolicyEndpoints.PrivacyPoliciesLanguages, language);
    }

    getPrivacyNoticeIntegrationSettings(versionId: string): Observable<PrivacyNoticeIntegrationSettings> {
        const url = `${PolicyEndpoints.PrivacyPoliciesVersions}/${versionId}/setting`;
        return this.http.get<PrivacyNoticeIntegrationSettings>(url);
    }

    deleteVersion(versionId: string): Observable<PolicyVersion> {
        const url = `${PolicyEndpoints.PrivacyPoliciesVersions}/${versionId}`;
        return this.http.delete<PolicyVersion>(url);
    }

    publishPrivacyNotice(versionId: string): Observable<PolicyVersion> {
        const url = `${PolicyEndpoints.PrivacyPoliciesVersions}/${versionId}/publish`;
        return this.http.put<PolicyVersion>(url, {});
    }

    savePrivacyNoticeContents(languageId: string, contents: CreatePolicyContentsRequestDto): Observable<void> {
        const url = `${PolicyEndpoints.PrivacyPoliciesLanguages}/${languageId}/contents`;
        return this.http.put<void>(url, contents);
    }
}
