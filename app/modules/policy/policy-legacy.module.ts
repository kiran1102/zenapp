import angular from "angular";

import { routes } from "./policy.routes";

export const policyLegacyModule = angular
    .module("zen.policy", [
        "ui.router",
    ])
    .config(routes)
    ;
