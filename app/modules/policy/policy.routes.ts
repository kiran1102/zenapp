import { IGeneralData } from "app/scripts/PIA/pia.routes";
import { Permissions } from "modules/shared/services/helper/permissions.service";
import { PolicyRoutes } from "modules/policy/privacy-notice/shared/enums/routes.enum";

export const policyResolve = (permission: string, fallback: string = "") => {
    return {
        PolicyPermission: [
            "GeneralData", "Permissions",
            (GeneralData: IGeneralData, permissions: Permissions): boolean => {
                if (permissions.canShow(permission)) {
                    return true;
                } else {
                    permissions.goToFallback();
                }
                return false;
            },
        ],
    };
};

export function routes($stateProvider: ng.ui.IStateProvider) {
    $stateProvider
        .state(PolicyRoutes.Policy, {
            abstract: true,
            url: "policy",
            resolve: policyResolve("PolicyManagement"),
            views: {
                module: {
                    template: "<ui-view></ui-view>",
                },
            },
        })
        .state(PolicyRoutes.PolicyNotices, {
            abstract: true,
            url: "/privacy-notices",
            template: "<ui-view></ui-view>",
            data: {
                breadcrumb: {
                    text: "Policies",
                    stateName: PolicyRoutes.PolicyNoticesList,
                },
            },
        })
        .state(PolicyRoutes.PolicyNoticesList, {
            url: "",
            template: "<policy-wrapper><privacy-notice-list></privacy-notice-list></policy-wrapper>",
            params: { time: null },
        })
        .state(PolicyRoutes.PolicyNoticeDetails, {
            url: "/:id/:tab?version",
            template: "<policy-wrapper><privacy-notice-details></privacy-notice-details></policy-wrapper>",
            params: {
                time: null,
                tab: {
                    value: "builder",
                    dynamic: true,
                },
            },
            data: {
                breadcrumb: "PrivacyNotice",
            },
            resolve: policyResolve("PolicyManagement"),
        });
}

routes.$inject = ["$stateProvider"];
