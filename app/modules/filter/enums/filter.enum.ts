export enum FilterValueTypes {
    SingleSelect,
    MultiSelect,
    Text,
    Number,
    Date,
    DateTime,
    DateRange,
    User,
    MultiUser,
    Org,
}

export enum FilterOperators {
    Equals = "EQ",
    NotEquals = "NE",
    LessThan = "LT",
    GreaterThan = "GT",
    LessThanOrEquals = "LE",
    GreaterThanOrEquals = "GE",
    Between = "BW",
    Regex = "RE",
}

export enum FilterOperatorSymbol {
    EqualTo = "=",
    GreaterThan = ">",
    LessThan = "<",
}
