import { Injectable, Inject } from "@angular/core";

// Services
import { matchRegex } from "modules/utils/match-regex";
import { TimeStamp } from "oneServices/time-stamp.service";

// Redux
import {
    getUserById,
    getUserDisplayName,
} from "oneRedux/reducers/user.reducer";
import { getOrgById } from "oneRedux/reducers/org.reducer";
import { StoreToken } from "tokens/redux-store.token";

// Interfaces
import { IUser } from "interfaces/user.interface";
import { IStore } from "interfaces/redux.interface";
import { ILabelValue } from "interfaces/generic.interface";
import { IFilterV2 } from "modules/filter/interfaces/filter.interface";
import { IOrganization } from "interfaces/org.interface";

// Constants
import { GUID } from "constants/regex.constant";

// Enums
import { FilterValueTypes } from "modules/filter/enums/filter.enum";

@Injectable()
export class OneFilterService {

    constructor(
        private timeStamp: TimeStamp,
        @Inject(StoreToken) private readonly store: IStore,
    ) {}

    getFilterValuesFromSelected<V>(selections: V[], type: FilterValueTypes, getLabelValue: (value: V) => ILabelValue<string | number>): Array<ILabelValue<string | number>> {
        if (!(selections && selections.length)) return;
        return selections.map((selection: V): ILabelValue<string | number> => {
            return this.getFilterValue(selection, type, getLabelValue);
        });
    }

    getUnknownUserValues<T, U, V>(filters: Array<IFilterV2<T, U, V>>, getLabelValue: (value: V) => ILabelValue<string|number>): string[] {
        if (!filters || !filters.length) return [];
        const unknownUsers: string[] = [];
        filters.forEach((filter: IFilterV2<T, U, V>) => {
            if ((filter.type !== FilterValueTypes.User && filter.type !== FilterValueTypes.MultiUser) || !(filter.values && filter.values.length)) return;
            filter.values.forEach((value: V) => {
                const unknownUser = this.checkValueForUnknownUser(value, getLabelValue);
                if (unknownUser) unknownUsers.push(unknownUser);
            });
        });
        return unknownUsers;
    }

    getOrgSelection(orgId: string): ILabelValue<string> {
        if (!orgId) return;
        const org: IOrganization = getOrgById(orgId)(this.store.getState());
        return { label: org ? org.Name : orgId, value: orgId };
    }

    getDateRangeSelection(beginISO: string, endISO: string): Array<ILabelValue<string>> {
        if (!beginISO || !endISO) return [];
        return [this.getDateSelection(beginISO), this.getDateSelection(endISO) ];
    }

    getDateSelection(dateISO: string): ILabelValue<string> {
        return { label: this.timeStamp.formatDateWithoutTime(dateISO), value: dateISO };
    }

    getUserSelection(userdId: string): ILabelValue<string> {
        const userModel: IUser = getUserById(userdId as string)(this.store.getState());
        return { label: userModel ? getUserDisplayName(userModel) : userdId, value: userdId };
    }

    private checkValueForUnknownUser<V>(value: V | ILabelValue<string | number>, getLabelValue: (value: V) => ILabelValue<string|number>): string {
        const option = getLabelValue ? getLabelValue(value as V) : value as ILabelValue<string | number>;
        if (!option || !option.value || option.label) return;
        const userModel: IUser = getUserById(option.value as string)(this.store.getState());
        if (!userModel && matchRegex(option.value as string, GUID)) return option.value as string;
    }

    private getFilterValue<V>(selection: V | ILabelValue<string | number>, type: FilterValueTypes, getLabelValue: (value: V) => ILabelValue<string|number>): ILabelValue<string | number> {
        const option = getLabelValue ? getLabelValue(selection as V) : selection as ILabelValue<string | number>;
        if (!option || option.label || !option.value) return option;
        if (type === FilterValueTypes.User || type === FilterValueTypes.MultiUser) return this.getUserSelection(option.value as string);
        if (type === FilterValueTypes.DateRange) return this.getDateSelection(option.value as string);
        if (type === FilterValueTypes.Org) return this.getOrgSelection(option.value as string);
        return option;
    }
}
