import { ILabelValue } from "interfaces/generic.interface";
import { FilterValueTypes } from "../enums/filter.enum";

export interface IFilterV2<T, U, V> {
    field: T;
    operator: U;
    values: V[];
    type: FilterValueTypes;
}

export interface IFilterOutput extends IFilterV2<
    ILabelValue<string | number>,
    ILabelValue<string | number>,
    ILabelValue<string | number>
> {}

export interface IFilterLabels {
    addFilter?: string;
    removeAllFilters?: string;
    newFilter?: string;
    field?: string;
    operator?: string;
    value?: string;
    save?: string;
    cancel?: string;
    apply?: string;
    equalTo?: string;
    isBetween?: string;
}
