import {
    Component,
    Input,
    OnInit,
    OnDestroy,
    Output,
    EventEmitter,
    Inject,
    SimpleChanges,
} from "@angular/core";

// RXJS
import {
    Observable,
    Subject,
} from "rxjs";
import { takeUntil } from "rxjs/operators";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

// Intefaces
import {
    IFilterV2,
    IFilterLabels,
    IFilterOutput,
} from "../../interfaces/filter.interface";
import { ILabelValue } from "interfaces/generic.interface";
import { IOrgUserAdapted } from "interfaces/org-user.interface";
import { IStore } from "interfaces/redux.interface";
import { IMyDateRangeModel } from "mydaterangepicker";
import { IMyDateModel } from "mydatepicker";

// Enums
import { FilterValueTypes } from "../../enums/filter.enum";
import { OrgUserTraversal } from "enums/org-user-dropdown.enum";

// Services
import { getCommaSeparatedList } from "sharedServices/string-helper.service";
import { UserActionService } from "oneServices/actions/user-action.service";
import { OneFilterService } from "../../services/one-filter.service";
import { TimeStamp } from "oneServices/time-stamp.service";

// Redux
import { StoreToken } from "tokens/redux-store.token";

@Component({
    selector: "one-filter",
    templateUrl: "./one-filter.component.html",
})

export class FilterComponent<T, U, V> implements OnInit, OnDestroy {

    @Input() labels: IFilterLabels;
    @Input() popOverPosition = "left";

    @Input() fields: T[];
    @Input() getFieldLabelValue: (field: T) => ILabelValue<string|number>;

    @Input() currentOperators: U[];
    @Input() getOperatorLabelValue: (operator: U) => ILabelValue<string|number>;

    @Input() currentValues: V[] = [];
    @Input() getValueLabelValue: (value: V) => ILabelValue<string|number>;

    @Input() currentType: number = FilterValueTypes.MultiSelect;
    @Input() loadingFilter: boolean;
    @Input() loadingSavedFilter: boolean;
    @Input() allowDuplicateFields = false; // Allow the same field to be selected for multiple filters

    @Input() disableAddFilter: boolean;
    @Input() disableVirtualScroll: boolean; // Todo: Remove binding once virtual scroll bugs are resolved.
    @Input() disableApply = false;

    @Input() togglePopup$: Observable<boolean>;

    @Input() set selectedFilters(filters: Array<IFilterV2<T, U, V>>) {
        const unknownUsers: string[] = this.oneFilterService.getUnknownUserValues(filters, this.getValueLabelValue);
        if (unknownUsers.length) {
            this._loading = true;
            this.userActionService.fetchUsersById(unknownUsers).then(() => {
                this._filters = filters && filters.length ? this.mapSelectionsToFilters(filters) : [];
                this._loading = false;
            });
        } else {
            this._filters = filters && filters.length ? this.mapSelectionsToFilters(filters) : [];
        }
    }

    @Output() selectField = new EventEmitter<ILabelValue<string|number>>();
    @Output() selectOperator = new EventEmitter<ILabelValue<string | number>>();
    @Output() addFilter = new EventEmitter<{ currentList: IFilterOutput[], filter: IFilterOutput }>();
    @Output() editFilter = new EventEmitter<{ currentList: IFilterOutput[], filter: IFilterOutput, index: number }>();
    @Output() removeFilter = new EventEmitter<{ currentList: IFilterOutput[], index: number }>();
    @Output() removeAllFilters = new EventEmitter<null>();
    @Output() applyFilters = new EventEmitter<IFilterOutput[]>();
    @Output() cancel = new EventEmitter<null>();

    valueTypes = FilterValueTypes;
    fieldSelection: ILabelValue<string|number>;
    operatorSelection: ILabelValue<string|number>;
    valueSelection: Array<ILabelValue<string|number>>;
    fieldSearchTerm: string;
    valueSearchTerm: string;
    selectedFilterIndex = -1;
    dateFormat: string;
    traversal = OrgUserTraversal.All;
    destroy$ = new Subject();
    _filters: IFilterOutput[] = [];
    _loading: boolean;
    _showPopup = false;
    _restoreFields = false;
    _currentRestoreField: ILabelValue<string|number>;

    private defaultLabels: IFilterLabels = {
        addFilter: this.translatePipe.transform("AddFilter"),
        removeAllFilters: this.translatePipe.transform("RemoveAllFilters"),
        newFilter: this.translatePipe.transform("NewFilter"),
        field: this.translatePipe.transform("Field"),
        operator: this.translatePipe.transform("Operator"),
        value: this.translatePipe.transform("Value"),
        save: this.translatePipe.transform("Save"),
        cancel: this.translatePipe.transform("Cancel"),
        apply: this.translatePipe.transform("Apply"),
        equalTo: this.translatePipe.transform("EqualTo"),
        isBetween: this.translatePipe.transform("IsBetween"),
    };

    constructor(
        private translatePipe: TranslatePipe,
        private userActionService: UserActionService,
        private oneFilterService: OneFilterService,
        private timeStamp: TimeStamp,
        @Inject(StoreToken) private readonly store: IStore,
    ) {}

    ngOnInit() {
        this.dateFormat = this.timeStamp.currentDateFormatting;
        this.labels = { ...this.defaultLabels, ...this.labels };
        if (this.togglePopup$) {
            this.togglePopup$
                .pipe(takeUntil(this.destroy$))
                .subscribe((isOpen: boolean) => {
                    this.togglePopup(isOpen);
                });
        }
    }

    ngOnChanges(changes: SimpleChanges) {
        if (changes.currentOperators && changes.currentOperators.currentValue &&
            changes.currentOperators.currentValue.length > 0) {
                this.operatorSelection = Boolean(this.operatorSelection) ? this.operatorSelection : this.getOperatorLabelValue(this.currentOperators[0]);
        }
    }

    ngOnDestroy() {
        this.destroy$.next();
        this.destroy$.complete();
    }

    togglePopup(isOpen: boolean = true, selectedFilterIndex: number = -1) {
        this._showPopup = isOpen;
        this.valueSearchTerm = "";
        if (!isOpen) {
            this.selectedFilterIndex = -1;
            return;
        }
        this.selectedFilterIndex = selectedFilterIndex;
        if (selectedFilterIndex !== -1) {
            const filter: IFilterOutput = this._filters[selectedFilterIndex];
            this.fieldSelection = filter.field;
            this.selectField.emit(filter.field);
            this.operatorSelection = filter.operator;
            this.valueSelection = filter.values;
        } else {
            this.fieldSelection = null;
            this.operatorSelection = null;
            this.valueSelection = null;
            this.fieldSearchTerm = "";
        }
    }

    onFieldChange(selectedField: ILabelValue<string|number>) {
        this._restoreFields = !selectedField && Boolean(this.fieldSelection);
        if (this._restoreFields) {
            this._currentRestoreField = this.fieldSelection;
            this.fields = [...this.fields];
        }
        this.valueSelection = null;
        this.operatorSelection = null;
        this.fieldSelection = selectedField;
        this.selectField.emit(selectedField);
    }

    clearInput() {
        this.fieldSearchTerm = "";
    }

    onOperatorChange(selectedOperator: ILabelValue<string|number>) {
        this.operatorSelection = selectedOperator;
        this.selectOperator.emit(selectedOperator);
    }

    onSingleSelectChange(value: ILabelValue<string|number>) {
        this.valueSelection = value ? [value] : null;
    }

    onMultiSelectChange(value: Array<ILabelValue<string | number>>) {
        this.valueSelection = value;
        this.valueSearchTerm = "";
    }

    onUserChange({selection}) {
        this.valueSelection = selection ? [{ label: selection.FullName, value: selection.Id }] : null;
    }

    onMultiUserChange(selections: IOrgUserAdapted[]) {
        this.valueSelection = selections && selections.length
            ? selections.map((selection: IOrgUserAdapted): ILabelValue<string> => ({ label: selection.FullName, value: selection.Id }))
            : [];
    }

    onTextChange(value: string) {
        this.valueSelection = value ? [{ label: value, value }] : null;
    }

    onNumberChange(value: number) {
        this.valueSelection = value || value === 0 ? [{ label: value.toString(), value }] : null;
    }

    onOrgChange(orgId: string) {
        if (!orgId) {
            this.valueSelection = null;
        } else {
            this.valueSelection = [this.oneFilterService.getOrgSelection(orgId)];
        }
    }

    onDateChange(selectedValue: IMyDateModel) {
        this.valueSelection = selectedValue && selectedValue.formatted ?
            [this.oneFilterService.getDateSelection(selectedValue.jsdate.toISOString())] :
            null;
    }

    onDateRangeChange(selectedValues: IMyDateRangeModel) {
        this.valueSelection = selectedValues && selectedValues.formatted ?
            this.oneFilterService.getDateRangeSelection(
                selectedValues.beginJsDate.toISOString(),
                selectedValues.endJsDate.toISOString(),
            ) : null;
    }

    fieldSearch({ key, value }) {
        if (key === "Enter") return;
        this.fieldSearchTerm = value;
    }

    valueSearch({ key, value }) {
        if (key === "Enter") return;
        this.valueSearchTerm = value;
    }

    saveFilter() {
        const newFilter: IFilterOutput = {
            field: this.fieldSelection,
            operator: this.operatorSelection,
            values: this.valueSelection,
            type: this.currentType,
        };
        if (this.selectedFilterIndex !== -1) {
            if (this.editFilter.observers.length) {
                this.editFilter.emit({ currentList: this._filters, filter: newFilter, index: this.selectedFilterIndex });
            } else {
                this._filters.splice(this.selectedFilterIndex, 1, newFilter);
                this.disableApply = false;
            }
        } else {
            if (this.addFilter.observers.length) {
                this.addFilter.emit({ currentList: this._filters, filter: newFilter });
            } else {
                this._filters.push(newFilter);
                this.disableApply = false;
            }
        }
        this._showPopup = false;
        this.fieldSelection = null;
        this.valueSelection = null;
        this.operatorSelection = null;
        this.selectedFilterIndex = -1;
    }

    getValueLabel = (filter: IFilterOutput): string => {
        if (!(filter.values && filter.values.length)) return "";
        return getCommaSeparatedList(filter.values, (singleValue: ILabelValue<string | number>) => singleValue.label);
    }

    getDefaultOperatorByType = (valueType: FilterValueTypes): string => {
        return valueType === FilterValueTypes.DateRange ? this.labels.isBetween : this.labels.equalTo;
    }

    getUserModel = (option: ILabelValue<string>): IOrgUserAdapted => {
        return { FullName: option.label, Id: option.value };
    }

    getDateModel = (selection: Array<ILabelValue<string>>): string => {
        if (!selection || !selection.length) return "";
        return this.timeStamp.formatDateWithoutTime(selection[0].value);
    }

    getDateRangeModel = (selections: Array<ILabelValue<string>>): string => {
        if (!selections || !selections.length || selections.length < 2) return "";
        return this.timeStamp.getRangePickerModelFromISO(selections[0].value, selections[1].value);
    }

    remove(index: number) {
        if (this.removeFilter.observers.length) {
            this.removeFilter.emit({ currentList: this._filters, index });
        } else {
            this._filters.splice(index, 1);
            this.disableApply = false;
        }
    }

    removeAll() {
        if (this._filters && this._filters.length) {
            if (this.removeAllFilters.observers.length) {
                this.removeAllFilters.emit();
            } else {
                this._filters = [];
                this.disableApply = false;
            }
        }
    }

    apply() {
        this.applyFilters.emit(this._filters);
    }

    filterSelectedFields = (field: ILabelValue<string|number>): boolean => {
        if (this.allowDuplicateFields || !(this._filters && this._filters.length)) return true;
        if (this._restoreFields && field.value === this._currentRestoreField.value) {
            this._restoreFields = false;
            return true;
        }
        return !(this._filters.find((filter: IFilterOutput): boolean => filter.field.value === field.value));
    }

    private mapSelectionsToFilters(selections: Array<IFilterV2<T, U, V>>): IFilterOutput[] {
        if (!selections || !selections.length) return [];
        return selections.map((filter: IFilterV2<T, U, V>): IFilterOutput => {
            return {
                field: this.getFieldLabelValue(filter.field),
                operator: filter.operator ? this.getOperatorLabelValue(filter.operator) : null,
                values: this.oneFilterService.getFilterValuesFromSelected(filter.values, filter.type, this.getValueLabelValue),
                type: filter.type,
            };
        });
    }
}
