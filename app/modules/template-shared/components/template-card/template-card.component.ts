// Angular
import {
    Component,
    Input,
    Inject,
} from "@angular/core";
import { StateService } from "@uirouter/core";

// Constants
import { TemplateStateProperty } from "constants/template-states.constant";

// Interface
import { ICustomTemplateDetails } from "modules/template/interfaces/custom-templates.interface";

// Service
import TemplateAction from "templateServices/actions/template-action.service";

@Component({
    selector: "template-card",
    templateUrl: "./template-card.component.html",
    styles: ["./template-card.component.scss"],
})

export class TemplateCardComponent {

    @Input() activeState: string;
    @Input() draft: ICustomTemplateDetails;
    @Input() icon: string;
    @Input() infoIconTooltipText: string;
    @Input() isLocked: boolean;
    @Input() published: ICustomTemplateDetails;
    @Input() showInfoIcon: boolean;
    @Input() showSettings: boolean;
    @Input() templateId: string;
    @Input() templateName: string;
    @Input() templateRootId: string;
    @Input() templateStates: typeof TemplateStateProperty;
    @Input() version: number;

    isSettingsShown = false;

    constructor(
        @Inject("TemplateAction") private templateAction: TemplateAction,
        private stateService: StateService,
    ) { }

    toggleSettingsMenu(event: Event): void {
        this.isSettingsShown = !this.isSettingsShown;
    }

    hideSettingsMenu(event: Event): void {
        this.isSettingsShown = false;
    }

    templateDetailsNavigator(): void {
        if (this.templateRootId && this.version) {
            this.stateService.transitionTo("zen.app.pia.module.templatesv2.studio.manage", { templateId: this.templateRootId, version: this.version });
        }
    }

    toggleState(state: string): void {
        this.activeState = state;
        this.isSettingsShown = false;
        if (state === TemplateStateProperty.Draft) {
            this.version = this.draft.version;
            this.icon = this.draft.icon;
            this.templateName = this.draft.name;
        } else {
            this.version = this.published.version;
            this.icon = this.published.icon;
            this.templateName = this.published.name;
        }
        this.templateAction.updateTemplatesActiveState(this.templateRootId, this.activeState);
    }

}
