// Angular
import {
    Component,
    Input,
    Output,
    EventEmitter,
    OnInit,
    Inject,
    OnDestroy,
} from "@angular/core";

// vitreus
import {
    ConfirmModalType,
    OtModalService,
} from "@onetrust/vitreus";

// Rxjs
import {
    BehaviorSubject,
    Observable,
    Subject,
} from "rxjs";
import { takeUntil } from "rxjs/operators";

// Interfaces
import { IStringMap } from "interfaces/generic.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";
import { ICustomTemplateDetails } from "modules/template/interfaces/custom-templates.interface";
import {
    IPublishTemplatePayload,
    IPublishTemplateModalData,
} from "interfaces/assessment-template-detail.interface";

// Services
import TemplateAction from "templateServices/actions/template-action.service";
import { NotificationService } from "modules/shared/services/provider/notification.service";
import { TemplateAPI } from "templateServices/api/template-api.service";
import { TemplateCreateHelperService } from "modules/template/services/template-create-helper.service";
import { TemplateDetailsService } from "modules/template/services/template-details.service";

// Constants
import { AssessmentTemplateErrorCodes } from "constants/assessment-template-error-codes.constant";
import { TemplateStateProperty } from "constants/template-states.constant";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";
import { PermitPipe } from "modules/pipes/permit.pipe";

// Component
import { AaTemplateWithAssessmentPublishModal } from "../../templates-with-assessment-publish-modal/template-with-assessment-publish-modal.component";
import { ArchiveTemplateModalComponent } from "modules/template/components/archive-template-modal/archive-template-modal.component";

@Component({
    selector: "template-card-settings",
    templateUrl: "./template-card-settings.component.html",
    styles: ["./template-card-settings.component.scss"],
})

export class TemplateCardSettingsComponent implements OnInit, OnDestroy {

    options: Array<{
        text: string;
        icon: string;
        click: () => void,
        show: boolean,
        otAutoId?: string,
    }>;
    loadingAssessmentCount: number;
    openAssessmentPayload: IPublishTemplatePayload = {
        pushTemplateChanges: false,
        notifyRespondents: false,
    };

    @Input() private draft: ICustomTemplateDetails;
    @Input() private published: ICustomTemplateDetails;
    @Input() private templateRootId: string;
    @Input() private activeState: string;
    @Input() private templateArchived: boolean;

    @Output() private onClose: EventEmitter<void> = new EventEmitter<void>();

    private permissions: IStringMap<boolean>;
    private isTemplateLocked: BehaviorSubject<boolean>;
    private isTemplateLocked$: Observable<boolean>;
    private isTemplateLockEnabled: boolean;
    private translations: IStringMap<string>;
    private destroy$ = new Subject();

    constructor(
        @Inject("TemplateAction") private templateAction: TemplateAction,
        @Inject("TemplateAPI") private templateAPI: TemplateAPI,
        private permissionPipe: PermitPipe,
        private translatePipe: TranslatePipe,
        private notificationService: NotificationService,
        private otModalService: OtModalService,
        private templateCreateHelperService: TemplateCreateHelperService,
        private templateDetailsService: TemplateDetailsService,
    ) { }

    ngOnInit(): void {
        this.initializeData();
    }

    ngOnDestroy() {
        this.destroy$.next();
        this.destroy$.complete();
    }

    onToggle(open: boolean): void {
        if (!open) {
            this.onClose.emit();
        }
    }

    listItemCallback(id: string) {
        this.options.find((option) => {
            return option.otAutoId === id;
        }).click();
    }

    private onPublishTemplate = (): void => {
        this.templateAPI.getOpenAssessmentCount(this.templateRootId)
            .then((openAssessmentCount: number) => {
                // this.loadingAssessmentCount = false;
                if (openAssessmentCount) {
                    this.onPublishTemplateWithOpenAssessments(openAssessmentCount);
                    return;
                }
                this.onPublishTemplateWithoutOpenAssessments();
            });
    }

    private onPublishTemplateWithOpenAssessments(openAssessmentCount: number) {
        this.otModalService.create(
                AaTemplateWithAssessmentPublishModal,
                {
                    isComponent: true,
                },
                {
                    ...this.openAssessmentPayload,
                    count: openAssessmentCount,
                },
            ).subscribe((data: IPublishTemplateModalData) => {
                if (data) {
                    this.openAssessmentPayload.pushTemplateChanges = data.pushTemplateChanges;
                    this.openAssessmentPayload.notifyRespondents = data.notifyRespondents;
                    const payload: IPublishTemplatePayload = this.openAssessmentPayload.pushTemplateChanges ? this.openAssessmentPayload : undefined;
                    this.templateAction.publishTemplate(this.draft.id, this.templateRootId, payload)
                        .then((response: IProtocolResponse<void>): boolean => {
                            if (!response.result && response.errorCode === AssessmentTemplateErrorCodes.TEMPLATE_INVALID) {
                                this.notificationService.alertError(this.translatePipe.transform("Error"), this.translations.invalidTemplate);
                            }
                            return true;
                        });
                }
            });
    }

    private onPublishTemplateWithoutOpenAssessments() {
        this.otModalService
            .confirm({
                type: ConfirmModalType.SUCCESS,
                translations: {
                    title: this.translations.publishTemplate,
                    desc: this.translations.publishTemplateModalDesc,
                    confirm: this.translations.doYouWantToContinue,
                    submit: this.translations.confirmButtonText,
                    cancel: this.translations.cancelButtonText,
                },
            }).pipe(
                takeUntil(this.destroy$),
            ).subscribe((data: boolean) => {
                if (data) {
                    this.templateAction.publishTemplate(this.draft.id, this.templateRootId)
                        .then((response: IProtocolResponse<void>): boolean => {
                            if (!response.result && response.errorCode === AssessmentTemplateErrorCodes.TEMPLATE_INVALID) {
                                this.notificationService.alertError(this.translatePipe.transform("Error"), this.translations.invalidTemplate);
                            }
                            return true;
                        });
                }
            });
    }

    private lockTemplate = (): void => {
        this.isTemplateLocked.next(!this.isTemplateLockEnabled);
        this.isTemplateLockEnabled = this.activeState === TemplateStateProperty.Draft ? !this.draft.isLocked : !this.published.isLocked;
        this.templateDetailsService.lockTemplate(this.isTemplateLockEnabled, false, this.templateRootId)
            .subscribe(() => {});
    }

    private discardDraft = (): void => {
        this.otModalService
            .confirm({
                type: ConfirmModalType.DELETE,
                translations: {
                    title: this.translations.discardTemplate,
                    desc: this.translatePipe.transform("SureToDiscardTemplateDraft", { TemplateDraftName: this.draft.name }),
                    confirm: null,
                    submit: this.translations.confirmButtonText,
                    cancel: this.translations.cancelButtonText,
                },
            }).pipe(
                takeUntil(this.destroy$),
            ).subscribe((data: boolean) => {
                if (data) {
                    this.templateAction.discardDraftedTemplate(this.draft.id, this.templateRootId);
                }
            });
    }

    private copyTemplateModal = (): void => {
        this.templateCreateHelperService.openCopyTemplateModal(this.templateRootId);
    }

    private initializeData() {
        this.initializeTranslations();
        this.initializePermissions();
        this.initializeListItems();
    }

    private initializeTranslations() {
        this.translations = {
            discardTemplate: this.translatePipe.transform("DiscardTemplate"),
            cancelButtonText: this.translatePipe.transform("Cancel"),
            confirmButtonText: this.translatePipe.transform("Confirm"),
            invalidTemplate: this.translatePipe.transform("InvalidTemplate"),
            publishTemplate: this.translatePipe.transform("PublishTemplate"),
            publishTemplateModalDesc: this.translatePipe.transform("PublishTemplateModalDesc"),
            copy: this.translatePipe.transform("Copy"),
            doYouWantToContinue: this.translatePipe.transform("DoYouWantToContinue"),
        };
    }

    private initializePermissions() {
        this.permissions = {
            TemplatesCreateAndPublish: this.permissionPipe.transform("TemplatesCreateAndPublish"),
            TemplatesDiscardDraft: this.permissionPipe.transform("TemplatesDiscardDraft"),
            TemplatesCopy: this.permissionPipe.transform("TemplatesCopy"),
            TemplatesLock: this.permissionPipe.transform("TemplatesLock"),
            TemplatesArchive: this.permissionPipe.transform("TemplatesArchive"),
            TemplatesUnarchive: this.permissionPipe.transform("TemplatesUnarchive"),
        };
    }

    private initializeListItems() {

        this.isTemplateLockEnabled = this.activeState.toUpperCase() === "DRAFT" ? this.draft.isLocked : this.published.isLocked;
        this.isTemplateLocked = new BehaviorSubject(this.isTemplateLockEnabled);
        this.isTemplateLocked$ = this.isTemplateLocked.asObservable();

        this.isTemplateLocked$.subscribe((isTemplateLocked) => {
            this.options = [{
                text: this.translatePipe.transform("PublishDraft"),
                icon: "ot ot-cloud-upload",
                click: this.onPublishTemplate,
                show: this.permissions.TemplatesCreateAndPublish && !this.templateArchived && this.activeState === TemplateStateProperty.Draft && !isTemplateLocked,
                otAutoId: "TemplateCardPublishDraftButton",
            },
            {
                text: this.translatePipe.transform("DiscardDraft"),
                icon: "ot ot-trash-o",
                click: this.discardDraft,
                show: this.permissions.TemplatesDiscardDraft && !this.templateArchived && this.activeState === TemplateStateProperty.Draft && !isTemplateLocked,
                otAutoId: "DiscardDraftButton",
            },
            {
                text: this.translatePipe.transform("CopyDraft"),
                icon: "ot ot-copy",
                click: this.copyTemplateModal,
                show: this.permissions.TemplatesCopy && !this.templateArchived && this.activeState === TemplateStateProperty.Draft,
                otAutoId: "CopyDraftButton",
            },
            {
                text: this.translatePipe.transform("CopyPublished"),
                icon: "ot ot-copy",
                click: this.copyTemplateModal,
                show: this.permissions.TemplatesCopy && !this.templateArchived && this.activeState === TemplateStateProperty.Published,
                otAutoId: "CopyPublishedButton",
            },
            {
                text: this.translatePipe.transform("LockTemplate"),
                icon: "ot ot-lock",
                click: this.lockTemplate,
                show: !isTemplateLocked && this.permissions.TemplatesLock && !this.templateArchived,
                otAutoId: "LockTemplateButton",
            },
            {
                text: this.translatePipe.transform("UnlockTemplate"),
                icon: "ot ot-unlock",
                click: this.lockTemplate,
                show: isTemplateLocked && this.permissions.TemplatesLock && !this.templateArchived,
                otAutoId: "UnLockTemplateButton",
            },
            {
                text: this.translatePipe.transform("ArchiveTemplate"),
                icon: "ot ot-archive",
                click: this.archiveTemplate,
                show: this.permissions.TemplatesArchive && !isTemplateLocked && !this.templateArchived,
                otAutoId: "ArchiveTemplateButton",
            },
            {
                text: this.translatePipe.transform("UnarchiveTemplate"),
                icon: "fa fa-undo",
                click: this.unarchiveTemplate,
                show: this.permissions.TemplatesUnarchive && this.templateArchived,
                otAutoId: "UnarchiveTemplateButton",
            }];
        });
    }

    private archiveTemplate = (): void => {
        const templateName: string = this.activeState === TemplateStateProperty.Draft ? this.draft.name : this.published.name;
        this.otModalService.create(
            ArchiveTemplateModalComponent,
            {
                isComponent: true,
            },
            {
                title: this.translatePipe.transform("ArchiveTemplate"),
                archiveTemplate: true,
                templateName,
            },
        ).subscribe((data: boolean): void => {
            if (data) {
                this.templateAction.archiveTemplate(this.templateRootId);
            }
        });
    }

    private unarchiveTemplate = (): void => {
        const templateName: string = this.activeState === TemplateStateProperty.Draft ? this.draft.name : this.published.name;
        this.otModalService.create(
            ArchiveTemplateModalComponent,
            {
                isComponent: true,
            },
            {
                title: this.translatePipe.transform("UnarchiveTemplate"),
                archiveTemplate: false,
                templateName,
            },
        ).subscribe((data: boolean): void => {
            if (data) {
                this.templateAction.unarchiveTemplate(this.templateRootId);
            }
        });
    }
}
