// Angular
import {
    Component,
    OnInit,
} from "@angular/core";

// 3rd party
import { Subject } from "rxjs";

// Interfaces
import { IOtModalContent } from "@onetrust/vitreus";
import { IPublishTemplateModalData } from "interfaces/assessment-template-detail.interface";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

@Component({
    selector: "template-with-assessment-publish-modal",
    templateUrl: "./template-with-assessment-publish-modal.component.html",
    styles: [`
        ot-setting ::ng-deep .ot-setting .ot-setting__content {
            width: 10rem;
        }`,
    ],
})

export class AaTemplateWithAssessmentPublishModal implements IOtModalContent, OnInit {

    otModalData: IPublishTemplateModalData;
    otModalCloseEvent: Subject<boolean | IPublishTemplateModalData>;
    openAssessmentCountText: string;

    constructor(private translatePipe: TranslatePipe) { }

    ngOnInit() {
        this.openAssessmentCountText = this.translatePipe.transform("InflightAssessmentCountText", {
            IN_FLIGHT_ASSESSMENT_COUNT: `<span class="color-ui-blue">${this.otModalData.count}</span>`,
        });

    }

    toggleNotifyRespondent(value: boolean) {
        this.otModalData.notifyRespondents = value;
    }

    toggleApplytoAll(value: boolean) {
        this.otModalData.pushTemplateChanges = value;
    }

    closeOpenTemplateConfirmationModal(value: boolean) {
        this.otModalCloseEvent.next(value ? this.otModalData : null);
    }
}
