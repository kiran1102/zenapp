// Angular
import { NgModule } from "@angular/core";

// Modules
import { SharedModule } from "sharedModules/shared.module";
import { CommonModule } from "@angular/common";
import { HttpClientModule } from "@angular/common/http";
import { FormsModule } from "@angular/forms";
import { ReactiveFormsModule } from "@angular/forms";
import { VitreusModule } from "@onetrust/vitreus";
import { DndListModule } from "ngx-drag-and-drop-lists";
import { OTPipesModule, PIPES } from "modules/pipes/pipes.module";
import { VirtualScrollModule } from "od-virtualscroll";
import { OneVideoModule } from "modules/video/video.module";

// Components
import { TemplateCardComponent } from "./components/template-card/template-card.component";
import { TemplateCardSettingsComponent } from "./components/template-card/template-card-settings/template-card-settings.component";
import { AaTemplateWithAssessmentPublishModal } from "./components/templates-with-assessment-publish-modal/template-with-assessment-publish-modal.component";

@NgModule({
    imports: [
        SharedModule,
        CommonModule,
        HttpClientModule,
        FormsModule,
        ReactiveFormsModule,
        VitreusModule,
        DndListModule,
        OTPipesModule,
        VirtualScrollModule,
        OneVideoModule,
    ],
    declarations: [
        TemplateCardComponent,
        TemplateCardSettingsComponent,
        AaTemplateWithAssessmentPublishModal,
    ],
    exports: [
        TemplateCardComponent,
        TemplateCardSettingsComponent,
        AaTemplateWithAssessmentPublishModal,
    ],
    entryComponents: [
        AaTemplateWithAssessmentPublishModal,
    ],
    providers: [
        PIPES, // TODO: Remove from providers when ot-common is implemented. Only necessary if you are using pipe in TS file, else just module is fine
    ],
})
export class TemplateSharedModule { }
