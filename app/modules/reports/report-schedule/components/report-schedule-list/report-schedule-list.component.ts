// 3rd party
import {
    Component,
    OnInit,
    Inject,
    OnDestroy,
} from "@angular/core";
import {
    take,
    takeUntil,
    filter,
    concatMap,
} from "rxjs/operators";
import {
    Observable,
    of,
    from,
    Subject,
} from "rxjs";
import { flatten } from "lodash";

// Redux
import { StoreToken } from "tokens/redux-store.token";
import { getAllUserModels } from "oneRedux/reducers/user.reducer";

// Services
import { OtModalService } from "@onetrust/vitreus";
import { ReportAPI } from "reportsModule/reports-shared/services/report-api.service";
import { UserActionService } from "oneServices/actions/user-action.service";
import { getCommaSeparatedList } from "sharedServices/string-helper.service";
import { getFrequency } from "sharedModules/services/helper/cron.service";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

// Components
import { AddReportScheduleModalComponent } from "reportScheduleModule/components/add-report-schedule-modal/add-report-schedule-modal.component";

// Constants
import {
    FrequencyTranslations,
    DayOfWeekTranslations,
    DayOfMonthLabels,
} from "sharedModules/constants/cron.constant";

// Enums
import { Frequency } from "sharedModules/enums/cron.enum";
import { ConfirmModalType } from "@onetrust/vitreus";

// Interfaces
import {
    IReportScheduleListResponse,
    IReportSchedule,
} from "reportScheduleModule/interfaces/report-schedule.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";
import { ICronObject } from "sharedModules/interfaces/cron.interface";
import { IStore } from "interfaces/redux.interface";
import { IUserMap } from "interfaces/user-reducer.interface";
import { IDropdownOption } from "interfaces/one-dropdown.interface";
import { ISortEvent } from "sharedModules/interfaces/sort.interface";

enum CellTypes { Text, User, Frequency, Cadence }

interface IScheduleColumn {
    propKey: string;
    labelKey: string;
    type: CellTypes;
}

@Component({
    selector: "report-schedule-list",
    templateUrl: "./report-schedule-list.component.html",
})
export class ReportScheduleListComponent implements OnInit, OnDestroy {

    isLoading: boolean;
    schedules: IReportSchedule[];
    columns: IScheduleColumn[] = [
        { propKey: "name", labelKey: "Name", type: CellTypes.Text },
        { propKey: "reportName", labelKey: "Report", type: CellTypes.Text },
        { propKey: "audience", labelKey: "Audience", type: CellTypes.User },
        { propKey: "cronConfig", labelKey: "Frequency", type: CellTypes.Frequency },
    ];
    pageData: IReportScheduleListResponse;
    userMap: IUserMap;
    menuPosition: string;
    rowActions: IDropdownOption[] = [
        {
            text: this.translatePipe.transform("Edit"),
            action: (row: IReportSchedule) => this.addSchedule(row),
            iconClass: "ot-edit",
        },
        {
            text: this.translatePipe.transform("Delete"),
            action: (row: IReportSchedule) => this.confirmDelete(row.id),
            iconClass: "ot-trash",
        },
    ];
    currentPage: number;
    currentSortKey: string;
    currentSortOrder: string;
    private destroy$ = new Subject();

    constructor(
        private otModalService: OtModalService,
        private translatePipe: TranslatePipe,
        private reportAPI: ReportAPI,
        private userActionService: UserActionService,
        @Inject(StoreToken) private store: IStore,
    ) {}

    ngOnInit() {
        this.userMap = getAllUserModels(this.store.getState());
        const savedSort: ISortEvent = JSON.parse(sessionStorage.getItem(`ReportsSchedulerSort`));
        this.currentSortKey = savedSort ? savedSort.sortKey : "name";
        this.currentSortOrder = savedSort ? savedSort.sortOrder : "desc";
        this.getSchedules();
    }

    ngOnDestroy() {
        this.destroy$.next();
        this.destroy$.complete();
    }

    getSchedules() {
        this.isLoading = true;
        this.reportAPI.getSchedules(this.currentPage, this.currentSortKey, this.currentSortOrder)
            .pipe(
                takeUntil(this.destroy$),
                filter((response: IProtocolResponse<IReportScheduleListResponse>) => Boolean(response.result && response.data)),
                concatMap(this.setUnknownUsers),
            )
            .subscribe((response: IProtocolResponse<IReportScheduleListResponse>) => {
                this.userMap = getAllUserModels(this.store.getState());
                this.schedules = response.data.content;
                this.pageData = response.data;
                this.isLoading = false;
            });
    }

    addSchedule(schedule?: IReportSchedule) {
        this.otModalService
            .create(
                AddReportScheduleModalComponent,
                { isComponent: true },
                schedule ? JSON.parse(JSON.stringify(schedule)) : null,
            )
            .pipe(take(1))
            .subscribe((reload: boolean) => {
                if (reload) {
                    this.currentPage = schedule ? this.currentPage : 0;
                    this.getSchedules();
                }
            });
    }

    setUnknownUsers = (response: IProtocolResponse<IReportScheduleListResponse>): Observable<IProtocolResponse<IReportScheduleListResponse>> => {
        if (!(response.result && response.data && response.data.content && response.data.content.length)) return of(response);
        const userValues: Array<Array<{ userId: string }>> = response.data.content.map((schedule: IReportSchedule): Array<{userId: string}> => schedule.audience || []);
        const userIds: string[] = flatten(userValues).map((user: { userId: string }) => user.userId);
        if (!userIds.length) return of(response);
        const unknownIds: string[] = userIds.filter((id: string): boolean => !this.userMap[id]);
        if (!unknownIds.length) return of(response);
        return from(this.userActionService.fetchUsersById(unknownIds).then(() => response));
    }

    getUsersLabel(values: Array<{ userId: string }>): string {
        if (!(values && values.length)) return "";
        const userNames: string[] = values.map((value: { userId: string }): string => this.userMap[value.userId].FullName || this.userMap[value.userId].Email);
        return getCommaSeparatedList(userNames);
    }

    getFrequencyLabel(value: ICronObject): string {
        const frequency: Frequency = getFrequency(value);
        return `${this.translatePipe.transform(FrequencyTranslations[frequency])} - ${this.getCadenceLabel(value, frequency)}`;
    }

    getCadenceLabel(value: ICronObject, frequency: Frequency): string {
        if (frequency === Frequency.Daily) return this.translatePipe.transform(value.daysOfTheWeek.length < 7 ? "Weekdays" : "EveryDay");
        if (frequency === Frequency.Weekly) return this.translatePipe.transform(DayOfWeekTranslations[value.daysOfTheWeek[0]]);
        if (frequency === Frequency.Monthly) return DayOfMonthLabels[value.dayOfMonth];
    }

    getCellLabel = (value: any, type: CellTypes): string => {
        switch (type) {
            case CellTypes.Text:
                return value;
            case CellTypes.User:
                return this.getUsersLabel(value);
            case CellTypes.Frequency:
                return this.getFrequencyLabel(value);
        }
    }

    setMenuClass(event?: MouseEvent) {
        this.menuPosition = "bottom-right";
        if ((window.innerHeight - 200) / 2 < (event.clientY - 200)) {
            this.menuPosition = "top-right";
        }
    }

    handleSortChange({ sortKey, sortOrder }) {
        sessionStorage.setItem("ReportsSchedulerSort", JSON.stringify({ sortKey, sortOrder }));
        this.currentSortOrder = sortOrder;
        this.currentSortKey = sortKey;
        this.currentPage = 0;
        this.getSchedules();
    }

    paginate(page: number) {
        this.currentPage = page;
        this.getSchedules();
    }

    private confirmDelete(scheduleId: string) {
        this.otModalService.confirm({
                type: ConfirmModalType.WARNING,
                translations: {
                    title: this.translatePipe.transform("DeleteSchedule"),
                    desc: this.translatePipe.transform("AreYouSureDeleteSchedule"),
                    confirm: "",
                    submit: this.translatePipe.transform("Confirm"),
                    cancel: this.translatePipe.transform("Cancel"),
                },
            })
            .pipe(
                takeUntil(this.destroy$),
                take(1),
                filter(((response: boolean) => response)),
            )
            .subscribe(() => this.deleteSchedule(scheduleId));
    }

    private deleteSchedule(scheduleId: string) {
        this.isLoading = true;
        this.currentPage = this.pageData.first || this.pageData.content.length > 1 ? this.pageData.number : this.pageData.number - 1;
        this.reportAPI.deleteSchedule(scheduleId)
            .pipe(take(1))
            .subscribe((res: IProtocolResponse<IReportSchedule>) => {
                if (!(res.result)) {
                    this.isLoading = false;
                    return;
                }

                this.getSchedules();
            });
    }

}
