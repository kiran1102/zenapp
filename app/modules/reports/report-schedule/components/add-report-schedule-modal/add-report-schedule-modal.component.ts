import { Component } from "@angular/core";

// RXJS
import { Subject } from "rxjs";
import {
    takeUntil,
    debounceTime,
} from "rxjs/operators";

// Interfaces
import { IOtModalContent } from "@onetrust/vitreus";
import {
    IAddScheduleFormState,
    IReportSchedule,
} from "reportScheduleModule/interfaces/report-schedule.interface";
import { IReport } from "modules/reports/reports-shared/interfaces/report.interface";
import { IOrgUserAdapted } from "interfaces/org-user.interface";

// Services
import { CreateReportScheduleService } from "reportScheduleModule/services/create-report-schedule.service";

// Constants
import {
    TimeOfDayOptions,
    FrequencyOptions,
    DayOfTheWeekOptions,
    DayOfTheMonthOptions,
} from "sharedModules/constants/cron.constant";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

// Enums
import { OrgUserTraversal } from "enums/org-user-dropdown.enum";
import { Frequency } from "sharedModules/enums/cron.enum";

interface IFormOption {
    label: string;
    labelKey?: string;
    value: number;
}

interface ILabelExtension {
    label: string;
}

@Component({
    selector: "add-report-schedule-modal",
    templateUrl: "./add-report-schedule-modal.component.html",
    host: { class: "flex-full-height" },
})
export class AddReportScheduleModalComponent implements IOtModalContent {

    isLoading: boolean;
    frequencyOptions: IFormOption[];
    dayOfWeekOptions: IFormOption[];
    dayOfMonthOptions: IFormOption[];
    timeOptions: IFormOption[];
    reportOptions: IReport[];
    userOptions: IFormOption[];
    selectedUsers: IOrgUserAdapted[];
    selectedReport: IReport;
    selectedFrequency: IFormOption;
    selectedTime: IFormOption;
    selectedDayOfWeek: IFormOption;
    selectedDayOfMonth: IFormOption;
    weekdayOnly: boolean;
    scheduleName: string;
    isLoadingReports: boolean;
    orgUserTraversal = OrgUserTraversal;
    formIsValid: boolean;
    frequency = Frequency;
    permissions: string[];

    otModalCloseEvent: Subject<boolean>;
    otModalData: IReportSchedule;

    private isLastReportsPage: boolean;
    private destroy$ = new Subject<string>();
    private debounceReportSearch$ = new Subject();

    constructor(
        private createReportScheduleService: CreateReportScheduleService,
        private translatePipe: TranslatePipe,
    ) { }

    ngOnInit() {
        this.permissions = ["Login", "ReportsExportNew", "FileRead"]; // Permissions to limit users list
        this.createReportScheduleService.initializeForm(this.otModalData);
        this.frequencyOptions = this.translateLabels(FrequencyOptions, "labelKey");
        this.dayOfWeekOptions = this.translateLabels(DayOfTheWeekOptions, "labelKey");
        this.dayOfMonthOptions = DayOfTheMonthOptions.slice(0, 28);
        this.timeOptions = TimeOfDayOptions;
        this.createReportScheduleService.formState$
            .pipe(
                takeUntil(this.destroy$),
            )
            .subscribe((state: IAddScheduleFormState) => {
                this.isLoading = state.isLoading;
                this.selectedFrequency = this.frequencyOptions.find((option: IFormOption): boolean => option.value === state.selectedFrequency);
                this.selectedTime = this.timeOptions.find((option: IFormOption): boolean => option.value === state.selectedTime);
                this.selectedDayOfWeek = this.dayOfWeekOptions.find((option: IFormOption): boolean => option.value === state.selectedDayOfWeek);
                this.selectedDayOfMonth = this.dayOfMonthOptions.find((option: IFormOption): boolean => option.value === state.selectedDayOfMonth);
                this.weekdayOnly = state.weekdayOnly;
                this.reportOptions = state.reportOptions;
                this.isLoadingReports = state.isLoadingReports;
                this.isLastReportsPage = state.isLastReportsPage;
                this.selectedReport = state.selectedReport;
                this.selectedUsers = [...state.selectedUsers];
                this.scheduleName = state.name;
                this.formIsValid = state.formIsValid;
            });
        this.debounceReportSearch$
            .pipe(debounceTime(1000))
            .subscribe((searchTerm: string) => this.createReportScheduleService.searchReports(searchTerm));
    }

    onFrequencyChange(selection: IFormOption) {
        this.createReportScheduleService.updateFrequency(selection.value);
    }

    onTimeChange(selection: IFormOption) {
        this.createReportScheduleService.updateTime(selection.value);
    }

    onWeekdayOnlyChange(weekdayOnly: boolean) {
        this.createReportScheduleService.updateWeekdayOnly(weekdayOnly);
    }

    onReportChange({ currentValue }) {
        this.createReportScheduleService.updateReportSelection(currentValue);
    }

    onUserChange(users: IOrgUserAdapted[]) {
        this.createReportScheduleService.updateUserSelection(users);
    }

    onNameChange(name: string) {
        this.createReportScheduleService.updateName(name);
    }

    onDayOfWeekChange(selection: IFormOption) {
        this.createReportScheduleService.updateDayOfWeek(selection.value);
    }

    onDayOfMonthChange(selection: IFormOption) {
        this.createReportScheduleService.updateDayOfMonth(selection.value);
    }

    saveSchedule() {
        this.createReportScheduleService.saveReport(this.otModalData ? this.otModalData.id : null)
            .subscribe((closeModal: boolean) => {
                if (!closeModal) return;
                this.otModalCloseEvent.next(true);
                this.otModalCloseEvent.complete();
            });
    }

    getNextReportPage() {
        if (this.isLastReportsPage) return;
        this.createReportScheduleService.getNextReportsPage();
    }

    reportSearch({ key, value }) {
        if (key === "Enter") return;
        this.isLoadingReports = true;
        this.debounceReportSearch$.next(value);
    }

    closeModal() {
        this.otModalCloseEvent.next(false);
        this.otModalCloseEvent.complete();
    }

    ngOnDestroy() {
        this.destroy$.next();
        this.destroy$.complete();
    }

    private translateLabels<T extends object>(options: T[], labelKey: string): Array<T & ILabelExtension> {
        return options.map((option: T): T & ILabelExtension => {
            return { ...(option as object), label: this.translatePipe.transform(option[labelKey]) } as T & ILabelExtension;
        });
    }
}
