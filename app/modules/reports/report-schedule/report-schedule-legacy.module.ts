declare var angular: any;

import routes from "./report-schedule.routes";

export const reportScheduleLegacyModule = angular
    .module("zen.report-schedule", [
        "ui.router",
    ])
    .config(routes)
    ;
