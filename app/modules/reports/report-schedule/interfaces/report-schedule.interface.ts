import {
    Frequency,
    DaysOfTheWeek,
} from "sharedModules/enums/cron.enum";
import { IReport } from "modules/reports/reports-shared/interfaces/report.interface";
import { IOrgUserAdapted } from "interfaces/org-user.interface";
import { ICronObject } from "sharedModules/interfaces/cron.interface";
import { IPageableOf } from "interfaces/pagination.interface";

export interface IAddScheduleFormState {
    isLoading: boolean;
    selectedFrequency: Frequency;
    selectedTime: number;
    weekdayOnly: boolean;
    lastFetchedReportPage: number;
    isLoadingReports: boolean;
    reportOptions: IReport[];
    selectedReport: IReport;
    isLastReportsPage: boolean;
    reportsSearchTerm: string;
    selectedUsers: IOrgUserAdapted[];
    name: string;
    formIsValid: boolean;
    selectedDayOfWeek: DaysOfTheWeek;
    selectedDayOfMonth: number;
}

export interface INewReportSchedule {
    name: string;
    reportName: string;
    reportId: string;
    cronConfig: ICronObject;
    audience: Array<{userId: string}>;
}

export interface IReportSchedule extends INewReportSchedule {
    id: string;
}

export interface IReportScheduleListResponse extends IPageableOf<IReportSchedule> {}
