// Services
import { Permissions } from "modules/shared/services/helper/permissions.service";

export default function routes($stateProvider: ng.ui.IStateProvider) {
    $stateProvider
        .state("zen.app.pia.module.reports-new.schedule", {
            url: "/schedule",
            params: { time: null },
            template: "<report-schedule-list class='flex-full-height'></report-schedule-list>",
            resolve: {
                ReportsPermission: [
                    "ReportsPermission", "Permissions", (ReportsPermission: boolean, permissions: Permissions): boolean => {
                        if (permissions.canShow("ReportsScheduler")) {
                            return ReportsPermission;
                        }
                        permissions.goToFallback();
                        return false;
                    },
                ],
            },
        })
    ;
}

routes.$inject = ["$stateProvider"];
