// 3rd Party
import {
    Injectable,
    Inject,
} from "@angular/core";
import {
    Observable,
    BehaviorSubject,
} from "rxjs";
import {
    take,
    map,
    first,
    tap,
 } from "rxjs/operators";

 // Redux
import { getAllUserModels } from "oneRedux/reducers/user.reducer";

// Interfaces
import { IProtocolResponse } from "interfaces/protocol.interface";
import {
    IAddScheduleFormState,
    INewReportSchedule,
    IReportSchedule,
} from "reportScheduleModule/interfaces/report-schedule.interface";
import {
    IReport,
    IReportListResponse,
} from "modules/reports/reports-shared/interfaces/report.interface";
import { IOrgUserAdapted } from "interfaces/org-user.interface";
import { IStore } from "interfaces/redux.interface";

// Services
import { ReportAPI } from "reportsModule/reports-shared/services/report-api.service";
import {
    CronBuilder,
    getFrequency,
} from "sharedModules/services/helper/cron.service";

// Enums
import {
    Frequency,
    DaysOfTheWeek,
} from "sharedModules/enums/cron.enum";

// Constants
import {
    DefaultCronObject,
    WeekdayOnlyValues,
} from "sharedModules/constants/cron.constant";

// Tokens
import { StoreToken } from "tokens/redux-store.token";

@Injectable()
export class CreateReportScheduleService {

    formState$: Observable<IAddScheduleFormState>;

    private defaultFormState: IAddScheduleFormState = {
        isLoading: true,
        selectedFrequency: Frequency.Daily,
        selectedTime: 0,
        weekdayOnly: false,
        isLoadingReports: true,
        lastFetchedReportPage: -1,
        reportOptions: [],
        selectedReport: null,
        isLastReportsPage: false,
        reportsSearchTerm: "",
        selectedUsers: [],
        name: "",
        formIsValid: false,
        selectedDayOfWeek: null,
        selectedDayOfMonth: null,
    };
    private _formState: BehaviorSubject<IAddScheduleFormState> = new BehaviorSubject(this.defaultFormState);
    private cronBuilder = new CronBuilder();

    constructor(
        private reportAPI: ReportAPI,
        @Inject(StoreToken) private store: IStore,
    ) {
        this.formState$ = this._formState.asObservable();
    }

    initializeForm(schedule?: IReportSchedule) {
        this.resetForm(schedule);
        this._formState.next({ ...this._formState.getValue(), isLoading: false });
    }

    updateFrequency(selectedFrequency: Frequency) {
        const currentState = this._formState.getValue();
        if (selectedFrequency === Frequency.Daily) {
            this.cronBuilder.setToDaily(currentState.weekdayOnly);
            this._formState.next({ ...this._formState.getValue(), selectedFrequency });
        } else if (selectedFrequency === Frequency.Weekly) {
            const selectedDayOfWeek: DaysOfTheWeek = currentState.selectedDayOfWeek || DaysOfTheWeek.Sunday;
            this.cronBuilder.setDayOfTheWeek(selectedDayOfWeek);
            this._formState.next({ ...this._formState.getValue(), selectedFrequency, selectedDayOfWeek });
        } else {
            const selectedDayOfMonth: number = currentState.selectedDayOfMonth || 1;
            this.cronBuilder.setDayOfTheMonth(selectedDayOfMonth);
            this._formState.next({ ...this._formState.getValue(), selectedFrequency, selectedDayOfMonth });
        }
    }

    updateTime(time: number) {
        this.cronBuilder.setHour(time);
        this._formState.next({ ...this._formState.getValue(), selectedTime: time });
    }

    updateWeekdayOnly(weekdayOnly: boolean) {
        this.cronBuilder.setWeekdayOnly(weekdayOnly);
        this._formState.next({ ...this._formState.getValue(), weekdayOnly });
    }

    updateReportSelection(report: IReport) {
        this.validateForm({ ...this._formState.getValue(), selectedReport: report });
    }

    updateName(name: string) {
        this.validateForm({ ...this._formState.getValue(), name });
    }

    updateUserSelection(selectedUsers: IOrgUserAdapted[]) {
        this.validateForm({ ...this._formState.getValue(), selectedUsers });
    }

    updateDayOfWeek(selectedDayOfWeek: DaysOfTheWeek) {
        this.cronBuilder.setDayOfTheWeek(selectedDayOfWeek);
        this._formState.next({ ...this._formState.getValue(), selectedDayOfWeek });
    }

    updateDayOfMonth(selectedDayOfMonth: number) {
        this.cronBuilder.setDayOfTheMonth(selectedDayOfMonth);
        this._formState.next({ ...this._formState.getValue(), selectedDayOfMonth });
    }

    getNextReportsPage() {
        const currentState = this._formState.getValue();
        this._formState.next({ ...currentState, isLoadingReports: true });
        this.reportAPI.getReportList(currentState.lastFetchedReportPage + 1, undefined, undefined, undefined, currentState.reportsSearchTerm)
            .pipe(
                take(1),
                map((response: IProtocolResponse<IReportListResponse>): IReport[] => response.result ? response.data.content : []),
            )
            .subscribe((reports: IReport[]) => {
                const isLastReportsPage = !(reports && reports.length);
                this._formState.next({
                    ...this._formState.getValue(),
                    isLoadingReports: false,
                    lastFetchedReportPage: isLastReportsPage ? currentState.lastFetchedReportPage : currentState.lastFetchedReportPage + 1,
                    reportOptions: [...currentState.reportOptions, ...reports],
                    isLastReportsPage,
                });
            });
    }

    searchReports(reportsSearchTerm: string) {
        this._formState.next({
            ...this._formState.getValue(),
            reportsSearchTerm,
            lastFetchedReportPage: -1,
            isLastReportsPage: false,
            reportOptions: [],
        });
        this.getNextReportsPage();
    }

    saveReport(scheduleId?: string): Observable<boolean> {
        const currentState: IAddScheduleFormState = this._formState.getValue();
        this._formState.next({ ...currentState, isLoading: true });
        const schedule: INewReportSchedule = {
            name: currentState.name,
            reportName: currentState.selectedReport.name,
            reportId: currentState.selectedReport.id,
            cronConfig: this.cronBuilder.getValue(),
            audience: currentState.selectedUsers.map((user: IOrgUserAdapted): { userId: string } => ({ userId: user.Id })),
        };
        const saveMethod: () => Observable<IProtocolResponse<IReportSchedule>> = scheduleId ?
            () => this.reportAPI.editSchedule(scheduleId, schedule) :
            () => this.reportAPI.addSchedule(schedule);
        return saveMethod().pipe(
            first(),
            map((response: IProtocolResponse<IReportSchedule>) => response.result),
            tap(() => this._formState.next({ ...this._formState.getValue(), isLoading: false })),
        );
    }

    private validateForm(updatedState: IAddScheduleFormState) {
        const formIsValid: boolean = Boolean(
            updatedState.name.trim()
            && updatedState.selectedReport
            && updatedState.selectedUsers && updatedState.selectedUsers.length,
        );
        this._formState.next({
            ...this._formState.getValue(),
            ...updatedState,
            formIsValid,
        });
    }

    private resetForm(schedule?: IReportSchedule) {
        const state: IAddScheduleFormState = schedule ? {
            ...this.defaultFormState,
            selectedFrequency: getFrequency(schedule.cronConfig),
            selectedTime: schedule.cronConfig.hour,
            weekdayOnly: schedule.cronConfig.daysOfTheWeek && schedule.cronConfig.daysOfTheWeek.length === 5,
            selectedReport: { id: schedule.reportId, name: schedule.reportName } as IReport,
            selectedUsers: schedule.audience && schedule.audience.length ?
                schedule.audience.map((user: { userId: string}): IOrgUserAdapted => {
                    return { Id: user.userId, FullName: getAllUserModels(this.store.getState())[user.userId].FullName };
                }) : [],
            name: schedule.name,
            selectedDayOfWeek: getFrequency(schedule.cronConfig) === Frequency.Weekly ? schedule.cronConfig.daysOfTheWeek[0] : null,
            selectedDayOfMonth: getFrequency(schedule.cronConfig) === Frequency.Monthly ? schedule.cronConfig.dayOfMonth : null,
            formIsValid: true,
        } : { ...this.defaultFormState };
        this.cronBuilder.setAll(schedule ? schedule.cronConfig : { ...DefaultCronObject });
        this._formState.next(state);
        this.getNextReportsPage();
    }
}
