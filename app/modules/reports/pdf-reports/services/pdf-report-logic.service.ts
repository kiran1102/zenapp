// Angular
import { Injectable } from "@angular/core";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

// Interfaces
import { IFilterColumnOption } from "modules/reports/reports-shared/interfaces/report.interface";
import { ILabelValue } from "interfaces/generic.interface";

// Enums
import {
    PDFSectionTypes,
    PDFSectionOptions,
    ReportEntityTypes,
} from "../../reports-shared/enums/reports.enum";
import { ModuleTypes } from "enums/module-types.enum";

// Constants
import { EmptyGuid } from "constants/empty-guid.constant";

@Injectable()
export class PDFReportLogicService {

    constructor(
        private translatePipe: TranslatePipe,
    ) {}

    getSectionTypes(reportModule: number, sourceId: string): Array<ILabelValue<string>> {
        const sectionTypes = [];
        if (reportModule === ModuleTypes.PIA || reportModule === ModuleTypes.VRM) {
            sectionTypes.push({
                label: this.translatePipe.transform("AssessmentQuestion"),
                value: sourceId === EmptyGuid ? PDFSectionTypes.AssessmentAllQuestions : PDFSectionTypes.AssessmentQuestion,
            });
            sectionTypes.push({
                label: this.translatePipe.transform("AssessmentDetail"),
                value: PDFSectionTypes.AssessmentDetail,
            });
            sectionTypes.push({
                label: this.translatePipe.transform("AssessmentNotes"),
                value: PDFSectionTypes.AssessmentNotes,
            });
        }
        if (reportModule === ModuleTypes.INV) {
            sectionTypes.push({
                label: this.translatePipe.transform("InventoryAttribute"),
                value: PDFSectionTypes.InventoryAttribute,
            });
        }
        sectionTypes.push({
            label: this.translatePipe.transform("TextEditor"),
            value: PDFSectionTypes.TextEditor,
        });
        return sectionTypes;
    }

    filterColumns(columns: IFilterColumnOption[], sectionType: string): IFilterColumnOption[] {
        if (
            !sectionType
            || sectionType === PDFSectionTypes.InventoryAttribute
        ) {
            return columns;
        }
        if (sectionType === PDFSectionTypes.TextEditor) return [];
        return columns.filter((column: IFilterColumnOption): boolean => {
            if (sectionType === PDFSectionTypes.AssessmentQuestion) {
                return column.entityType === ReportEntityTypes.Questions;
            }
            if (sectionType === PDFSectionTypes.AssessmentDetail) {
                return column.entityType !== ReportEntityTypes.Questions;
            }
        });
    }

    getSectionOptions(sectionType: string): Array<ILabelValue<string>> {
        // TODO : Add all section options per type
        if (!sectionType || sectionType) return [];
        if (sectionType === PDFSectionTypes.AssessmentQuestion) {
            return [
                {
                    label: this.translatePipe.transform("Risk"),
                    value: PDFSectionOptions.Risk,
                },
            ];
        }
    }

    getEntityType(type: ModuleTypes): ModuleTypes {
        if (type === ModuleTypes.INV) return type;
        return ModuleTypes.PIA;
    }

    getExportEntityType(type: ModuleTypes): ModuleTypes {
        if (type === ModuleTypes.PIA) return ModuleTypes.AE;
        if (type === ModuleTypes.VRM) return ModuleTypes.VRME;
        return type;
    }
}
