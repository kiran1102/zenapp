import { from, forkJoin } from "rxjs";
// Angular
import {
    Injectable,
    Inject,
} from "@angular/core";

// Rxjs
import {
    BehaviorSubject,
    Observable,
    of,
} from "rxjs";
import { concatMap, map } from "rxjs/operators";

// 3rd party
import { cloneDeep } from "lodash";

// Interfaces
import {
    IPDFSection,
    IReportView,
    IPDFReportState,
    IColumnActiveMap,
    IReportExport,
    ISelectionOption,
    IFilterColumnOption,
    IFilterColumn,
} from "modules/reports/reports-shared/interfaces/report.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";
import { IStringMap } from "interfaces/generic.interface";

// Enums
import {
    ReportPaperSizes,
    ReportType,
    PDFSectionTypes,
} from "reportsModule/reports-shared/enums/reports.enum";
import { ModuleTypes } from "enums/module-types.enum";

// Constants
import { TranslationModules } from "constants/translation.constant";
import { SelectionLabels } from "reportsModule/reports-shared/constants/reports.constant";
import { EmptyGuid } from "constants/empty-guid.constant";

// Services
import { ReportAPI } from "reportsModule/reports-shared/services/report-api.service";
import { TenantTranslationService } from "sharedServices/tenant-translation.service";
import { LookupService } from "sharedModules/services/helper/lookup.service";
import { ReportsAdapter } from "reportsModule/reports-shared/services/reports-adapter.service";
import { PDFReportLogicService } from "reportsModule/pdf-reports/services/pdf-report-logic.service";
import { OtModalService } from "@onetrust/vitreus";
import { ReportTemplateExportService } from "./../../reports-shared/services/report-template-export.service";

// Components
import { InvalidReportModalComponent } from "reportsModule/pdf-reports/components/invalid-report-modal/invalid-report-modal.component";
import { OTNotificationModalComponent } from "sharedModules/components/ot-notification-modal/ot-notification-modal.component";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

@Injectable()
export class PDFReportService {

    selectionLabels = SelectionLabels;
    readonly reportView$: Observable<IReportView>;
    readonly reportState$: Observable<IPDFReportState>;
    private defaultReport: IReportView = {
        layout: {
            pageHeader: false,
            paperSize: ReportPaperSizes.A4,
            sections: [],
            pageHeaderImage: null,
            headerOnEveryPage: false,
            entityId: "",
        },
        reportName: "",
        sourceId: "",
        filterCriteria: null,
        columns: [],
        sort: null,
        itemCount: 0,
        pageNumber: 0,
        pageSize: 0,
        totalItemCount: 0,
        totalPageCount: 0,
        type: 0,
        content: [],
    };
    private defaultReportState: IPDFReportState = {
        isSaving: false,
        hasChanges: false,
        isLoadingReport: true,
        options: [],
        selectedOption: null,
        openSectionMap: {},
        moduleType: null,
    };
    private _reportView: BehaviorSubject<IReportView> = new BehaviorSubject(this.defaultReport);
    private _reportState: BehaviorSubject<IPDFReportState> = new BehaviorSubject(this.defaultReportState);
    private _reportId: string;
    private _allSelectionOptions: ISelectionOption[] = [];
    private _allColumnOptions: IFilterColumnOption[];

    constructor(
        private reportAPI: ReportAPI,
        private lookupService: LookupService,
        private reportsAdapter: ReportsAdapter,
        private pdfReportLogicService: PDFReportLogicService,
        private otModalService: OtModalService,
        private translatePipe: TranslatePipe,
        private reportTemplateExportService: ReportTemplateExportService,
        @Inject("TenantTranslationService") private tenantTranslationService: TenantTranslationService,
    ) {
        this.reportView$ = this._reportView.asObservable();
        this.reportState$ = this._reportState.asObservable();
    }

    initReportView(reportId: string) {
        this._reportId = reportId;
        this._reportState.next(this.defaultReportState);
        this._allColumnOptions = null;
        const reportObservables: Array<Observable<IProtocolResponse<IReportView> | boolean>> = [this.reportAPI.getReport(reportId)];
        if (!this.tenantTranslationService.loadedModuleTranslations[TranslationModules.Inventory]) {
            reportObservables.push(from(this.tenantTranslationService.addModuleTranslations(TranslationModules.Inventory)));
        }
        forkJoin(reportObservables).subscribe((responses: Array<IProtocolResponse<IReportView>>) => {
            if (!responses[0].result) return;
            const report: IReportView = (responses[0] as IProtocolResponse<IReportView>).data;
            const entityType: ModuleTypes = this.pdfReportLogicService.getEntityType(report.type);
            this.reportAPI.getEntityOptions(entityType, report.sourceId).subscribe((selectionsResponses: IProtocolResponse<ISelectionOption[]>) => {
                if (!selectionsResponses.result) return;
                this._allSelectionOptions = this.reportsAdapter.formatPAOptions((selectionsResponses as IProtocolResponse<ISelectionOption[]>).data);
                const selectedOption: ISelectionOption = this.reportsAdapter.getSelectedOption(report, this._allSelectionOptions);
                const options = this.lookupService.filterOptionsBySelections([selectedOption], this._allSelectionOptions, "id");
                if (report.layout && report.layout.sections) {
                    report.layout.sections.forEach((value, index) => {
                        value.order = index;
                    });
                }
                this._reportState.next({
                    ...this._reportState.getValue(),
                    isLoadingReport: false,
                    hasChanges: report.layout && !report.layout.entityId && this._allSelectionOptions.length > 0,
                    options,
                    selectedOption,
                    moduleType: report.type,
                });
                this._reportView.next({
                    ...this.defaultReport,
                    ...report,
                    layout: report.layout ? {
                        ...report.layout,
                        entityId: selectedOption ? selectedOption.id : "",
                        paperSize: report.layout.paperSize || this.defaultReport.layout.paperSize,
                        sections: report.layout.sections || this.defaultReport.layout.sections,
                    } : {
                        ...cloneDeep(this.defaultReport.layout),
                        entityId: selectedOption ? selectedOption.id : "",
                    },
                });
            });
        });
    }

    setLayoutProperty(propertKey: string, propertyValue: any) {
        const currentView: IReportView = this._reportView.getValue();
        this._reportView.next({
            ...currentView,
            layout: {
                ...currentView.layout,
                [propertKey]: propertyValue,
            },
        });
        this._reportState.next({ ...this._reportState.getValue(), hasChanges: true });
    }

    saveReport(): Observable<boolean> {
        const report: IReportView = this._reportView.getValue();
        this._reportState.next({ ...this._reportState.getValue(), isSaving: true });
        const reportIsValid: boolean = this.checkReportValidity();
        if (!reportIsValid) {
            const invalidReportModal = this.otModalService
                .create(
                    InvalidReportModalComponent,
                    { isComponent: true },
                    this.translatePipe.transform("ReportSectionInvalidMessage"),
                )
                .subscribe(() => {
                    invalidReportModal.unsubscribe();
                });
            this._reportState.next({ ...this._reportState.getValue(), isSaving: false });
            return of(false);
        } else {
            return this.reportAPI.saveReportView(this._reportId, report).pipe(concatMap((): Observable<boolean> => {
                this._reportState.next({
                    ...this._reportState.getValue(),
                    isSaving: false,
                    hasChanges: false,
                });
                return of(true);
            }));
        }
    }

    selectEntity(selectedOption: ISelectionOption) {
        const options = this.lookupService.filterOptionsBySelections([selectedOption], this._allSelectionOptions, "id");
        this._reportState.next({
            ...this._reportState.getValue(),
            selectedOption,
            options,
            hasChanges: true,
        });
        const currentView: IReportView = this._reportView.getValue();
        this._reportView.next({
            ...currentView,
            layout: {
                ...currentView.layout,
                entityId: selectedOption ? selectedOption.id : "",
            },
        });
    }

    filterEntity(searchTerm: string) {
        const options = this.lookupService.filterOptionsByInput([this._reportState.getValue().selectedOption], this._allSelectionOptions, searchTerm, "id", "label");
        this._reportState.next({
            ...this._reportState.getValue(),
            options,
        });
    }

    getColumnOptions(section: IPDFSection): Observable<IColumnActiveMap> {
        if (this._allColumnOptions) {
            const filteredColumns = this.pdfReportLogicService.filterColumns(this._allColumnOptions, section.type);
            return of(this.reportsAdapter.splitColumnsByActive(section.columns || [], filteredColumns));
        }
        const currentView: IReportView = this._reportView.getValue();
        const columnPromise = currentView.type === ModuleTypes.INV
            ? () => this.reportAPI.getColumns(currentView.type, currentView.sourceId)
            : () => this.reportAPI.getAssessmentColumnsMetadata(currentView.type, currentView.sourceId);
        return columnPromise().pipe(
            map((response: IProtocolResponse<IFilterColumnOption[]>): IColumnActiveMap => {
                if (!response.result) return;
                this._allColumnOptions = this.reportsAdapter.createGroupNames(response.data, "name") as IFilterColumnOption[];
                const filteredColumns = this.pdfReportLogicService.filterColumns(this._allColumnOptions, section.type);
                return this.reportsAdapter.splitColumnsByActive(section.columns || [], filteredColumns);
            }));
    }

    getAssessmentQuestions(section: IPDFSection): Observable<IFilterColumn[]> {
        if (this._allColumnOptions) {
            const questions: IFilterColumnOption[] = this.pdfReportLogicService.filterColumns(this._allColumnOptions, section.type);
            return of(this.reportsAdapter.formatColumnData(questions));
        }
        const currentView: IReportView = this._reportView.getValue();
        return this.reportAPI.getAssessmentColumnsMetadata(currentView.type, currentView.sourceId).pipe(
            map((response: IProtocolResponse<IFilterColumnOption[]>): IFilterColumn[] => {
            if (!response.result) return;
            const questions: IFilterColumnOption[] = this.pdfReportLogicService.filterColumns(response.data, section.type);
            return this.reportsAdapter.formatColumnData(questions);
        }));
    }

    setSection(section: IPDFSection, isNew: boolean = false) {
        const currentView: IReportView = this._reportView.getValue();
        currentView.layout.sections.splice(section.order, 1, section);
        this._reportView.next(currentView);
        const currentState: IPDFReportState = this._reportState.getValue();
        this._reportState.next({
            ...currentState,
            openSectionMap: {
                ...currentState.openSectionMap,
                [section.order]: isNew || currentState.openSectionMap[section.order],
            },
            hasChanges: true,
        });
    }

    setSectionColumns(section: IPDFSection, columns: IFilterColumnOption[]) {
        section.columns = this.reportsAdapter.formatColumnData(columns);
        this.setSection(section);
    }

    toggleSection(section: IPDFSection) {
        const currentState = this._reportState.getValue();
        this._reportState.next({
            ...currentState,
            openSectionMap: {
                ...currentState.openSectionMap,
                [section.order]: !currentState.openSectionMap[section.order],
            },
        });
    }

    async exportReport() {
        if (this._reportState.getValue().hasChanges) {
            const canExport: boolean = await this.saveReport().toPromise();
            if (!canExport) return;
        }
        const taskNotificationModal = this.otModalService
            .create(
                OTNotificationModalComponent,
                { isComponent: true },
                { modalTitle: "ReportDownload", confirmationText: this.translatePipe.transform("ReportIsGeneratedCheckTasksBar") },
            )
            .subscribe(() => {
                taskNotificationModal.unsubscribe();
            });
        const currentView = this._reportView.getValue();
        const currentState = this._reportState.getValue();
        if (currentView.sourceId === EmptyGuid && (currentView.type === ModuleTypes.AE || currentView.type === ModuleTypes.PIA)) {
            this.reportTemplateExportService.getAssessmentQuestionColumnMetadata(currentState.selectedOption.sourceId).subscribe(
                (columnResponse: IFilterColumnOption[]) => {
                    const layout = this.reportTemplateExportService.setAssessmentAnyTemplateMetadata(columnResponse, currentView.layout);
                    this.reportTemplateExportService.export(layout, currentView.reportName, currentState.selectedOption.id, ModuleTypes.AE);
                });
        } else {
            const exportBody: IReportExport = {
                reportExportFormat: ReportType.PDF,
                name: currentView.reportName,
                entityId: currentView.layout.entityId,
                sourceId: currentView.sourceId,
                reportEntityType: this.pdfReportLogicService.getExportEntityType(currentView.type),
                layout: { sections: [] },
                reportId: this._reportId,
            };
            this.reportAPI.exportPDF(exportBody);
        }
    }

    deleteSection(section: IPDFSection) {
        const currentView: IReportView = this._reportView.getValue();
        currentView.layout.sections.splice(section.order, 1);
        this.resetOrderBasedOnIndex(currentView.layout.sections);
    }

    moveSection(section: IPDFSection, direction: string) {
        const currentView: IReportView = this._reportView.getValue();
        const sectionList = currentView.layout.sections;
        const swapIndex: number = direction === "up" ? section.order - 1 : section.order + 1;
        if (sectionList[swapIndex]) {
            sectionList[section.order] = sectionList[swapIndex];
            sectionList[swapIndex] = section;
            this.resetOrderBasedOnIndex(currentView.layout.sections);
        }
    }

    updateState(changes: IStringMap<boolean>) {
        this._reportState.next({ ...this._reportState.getValue(), ...changes });
    }

    private resetOrderBasedOnIndex(sections: IPDFSection[]): IPDFSection[] {
        const currentView: IReportView = this._reportView.getValue();
        const currentState: IPDFReportState = this._reportState.getValue();
        if (!currentView.layout) return;
        const newOpenSectionMap: IStringMap<boolean> = {};
        sections.forEach((section: IPDFSection, index: number) => {
            newOpenSectionMap[index] = currentState.openSectionMap[section.order];
            section.order = index;
        });
        this._reportState.next({
            ...currentState,
            openSectionMap: newOpenSectionMap,
            hasChanges: true,
        });
        this._reportView.next({
            ...currentView,
            layout: {
                ...currentView.layout,
                sections,
            },
        });
    }

    private checkReportValidity(): boolean {
        let isValid = true;
        const currentView: IReportView = this._reportView.getValue();
        if (!(currentView.layout && currentView.layout.sections && currentView.layout.sections.length)) return isValid;
        for (let i = 0; i < currentView.layout.sections.length; i++) {
            const section = currentView.layout.sections[i];
            if (
                this.sectionCanHaveColumns(section.type)
                && !(section.columns && section.columns.length)
                && !(section.nameKey || section.name)
                && !(section.type === PDFSectionTypes.AssessmentAllQuestions)
            ) {
                isValid = false;
                break;
            }
        }
        return isValid;
    }

    private sectionCanHaveColumns(sectionType: PDFSectionTypes): boolean {
        return !(sectionType === PDFSectionTypes.TextEditor || sectionType === PDFSectionTypes.AssessmentNotes);
    }
}
