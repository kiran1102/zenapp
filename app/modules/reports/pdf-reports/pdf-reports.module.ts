import {
    NgModule,
} from "@angular/core";

// Modules
import { SharedModule } from "sharedModules/shared.module";
import { CommonModule } from "@angular/common";
import { HttpClientModule } from "@angular/common/http";
import { FormsModule } from "@angular/forms";
import { ReactiveFormsModule } from "@angular/forms";
import { VitreusModule } from "@onetrust/vitreus";
import { DndListModule } from "ngx-drag-and-drop-lists";
import { OTPipesModule, PIPES } from "modules/pipes/pipes.module";
import { VirtualScrollModule } from "od-virtualscroll";
import { OneVideoModule } from "modules/video/video.module";
import { ReportsSharedModule } from "reportsModule/reports-shared/reports-shared.module";

// Components
import { PDFReportComponent } from "reportsModule/pdf-reports/components/pdf-report/pdf-report.component";
import { PDFBuilderComponent } from "reportsModule/pdf-reports/components/pdf-builder/pdf-builder.component";
import { PDFSettingsComponent } from "reportsModule/pdf-reports/components/pdf-settings/pdf-settings.component";
import { PDFSectionModal } from "reportsModule/pdf-reports/components/pdf-section-modal/pdf-section-modal.component";
import { PDFDetailsComponent } from "reportsModule/pdf-reports/components/pdf-details/pdf-details.component";
import { InvalidReportModalComponent } from "reportsModule/pdf-reports/components/invalid-report-modal/invalid-report-modal.component";

// Services
import { PDFReportService } from "reportsModule/pdf-reports/services/pdf-report.service";
import { PDFReportLogicService } from "reportsModule/pdf-reports/services/pdf-report-logic.service";

@NgModule({
    imports: [
        SharedModule,
        CommonModule,
        HttpClientModule,
        FormsModule,
        ReactiveFormsModule,
        VitreusModule,
        DndListModule,
        OTPipesModule,
        VirtualScrollModule,
        OneVideoModule,
        ReportsSharedModule,
    ],
    declarations: [
        PDFSectionModal,
        PDFReportComponent,
        PDFBuilderComponent,
        PDFSettingsComponent,
        PDFDetailsComponent,
        InvalidReportModalComponent,
    ],
    entryComponents: [
        PDFSectionModal,
        PDFReportComponent,
        InvalidReportModalComponent,
    ],
    providers: [
        PDFReportService,
        PDFReportLogicService,
        PIPES, // TODO: Remove from providers when ot-common is implemented. Only necessary if you are using pipe in TS file, else just module is fine
    ],
})
export class PDFReportsModule {}
