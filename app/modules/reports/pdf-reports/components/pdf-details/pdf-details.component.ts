// 3rd party
import { Component } from "@angular/core";

// Services
import { PDFReportService } from "reportsModule/pdf-reports/services/pdf-report.service";

// Enums
import { ModuleTranslationKeys } from "enums/module-types.enum";
import { InventoryTableIds } from "enums/inventory.enum";
import { ModuleTypes } from "enums/module-types.enum";

// Constanst
import { InventoryTypeTranslations } from "constants/inventory-config.constant";
import { SelectionLabels } from "reportsModule/reports-shared/constants/reports.constant";

@Component({
    selector: "pdf-details",
    templateUrl: "./pdf-details.component.html",
})
export class PDFDetailsComponent  {

    moduleTranslations = ModuleTranslationKeys;
    inventoryTableIds = InventoryTableIds;
    inventoryTranslations = InventoryTypeTranslations;
    moduleTypes = ModuleTypes;
    selectionLabels = SelectionLabels;

    constructor(
        public pdfReportService: PDFReportService,
    ) {}
}
