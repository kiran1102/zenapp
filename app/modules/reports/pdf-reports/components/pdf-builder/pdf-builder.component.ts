// Angular
import {
    Component,
    OnDestroy,
} from "@angular/core";

// RXJS
import { Subject, of } from "rxjs";
import {
    takeUntil,
    filter,
    tap,
} from "rxjs/operators";

// Interfaces
import {
    IPDFSection,
    IReportSectionModalResolve,
    IReportColumnModalResolve,
    ISectionOptionsInfo,
    IFilterColumnOption,
    IFilterColumn,
} from "modules/reports/reports-shared/interfaces/report.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";

// Services
import { PDFReportService } from "reportsModule/pdf-reports/services/pdf-report.service";
import {
    OtModalService,
    ModalSize,
    ConfirmModalType,
} from "@onetrust/vitreus";

// Enums
import { PDFSectionTypes } from "reportsModule/reports-shared/enums/reports.enum";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";
import { ColumnLabelPipe } from "reportsModule/reports-shared/pipes/column-label.pipe";

// Components
import { ColumnPickerModal } from "../../../reports-shared/components/column-picker-modal/column-picker-modal.component";
import { PDFSectionModal } from "../pdf-section-modal/pdf-section-modal.component";

@Component({
    selector: "pdf-builder",
    templateUrl: "./pdf-builder.component.html",
})

export class PDFBuilderComponent implements OnDestroy {

    pdfSectionTypes = PDFSectionTypes;
    customModules = [
        ["bold", "italic", "underline"],
        [{ size: ["small", false, "large"] }],
        [{ font: [] }],
        [{ list: "ordered" }, { list: "bullet" }],
        [{ color: [] }, { background: [] }],
        [{ align: [] }],
    ];
    private destroy$ = new Subject();

    constructor(
        readonly pdfReportService: PDFReportService,
        private otModalService: OtModalService,
        private translatePipe: TranslatePipe,
        private columnLabelPipe: ColumnLabelPipe,
    ) {}

    ngOnDestroy() {
        this.destroy$.next();
        this.destroy$.complete();
    }

    addSection(sectionIndex: number) {
        const newSection: IPDFSection = {
            name: "",
            type: null,
            textContent: "",
            order: sectionIndex,
            options: null,
        };
        this.openSectionModal(newSection, true);
    }

    openSectionModal(section: IPDFSection, isNew: boolean) {
        const modalData: IReportSectionModalResolve = {
            title: isNew ? "AddSection" : "EditSection",
            isNew,
            sectionName: section.nameKey ? this.translatePipe.transform(section.nameKey) : section.name,
            sectionType: section.type,
            sectionOptions: section.options,
            saveSection: (updatedSectionName: string, updatedSectionType: PDFSectionTypes, updatedSectionOptions: ISectionOptionsInfo, includeAllQuestions: boolean) => {
                const updatedSection: IPDFSection = {
                    ...section,
                    name: updatedSectionName,
                    nameKey: section.name === updatedSectionName ? section.nameKey : null,
                    type: updatedSectionType,
                    options: updatedSectionOptions,
                };
                if (includeAllQuestions) {
                    return this.pdfReportService.getAssessmentQuestions(updatedSection).pipe(
                        tap((response: IFilterColumn[]) => {
                            if (!response) return;
                            updatedSection.columns = response;
                            this.pdfReportService.setSection(updatedSection, isNew);
                        }));
                } else {
                    return of(this.pdfReportService.setSection(updatedSection, isNew));
                }
            },
        };
        this.otModalService.create(
            PDFSectionModal,
            { isComponent: true },
            modalData,
        );
    }

    openColumnModal(section: IPDFSection) {
        const modalData: IReportColumnModalResolve = {
            getColumns: () => this.pdfReportService.getColumnOptions(section),
            callback: (response: IProtocolResponse<IFilterColumnOption[]>) => {
                this.pdfReportService.setSectionColumns(section, response.data);
            },
        };
        this.otModalService.create(
            ColumnPickerModal,
            { isComponent: true, size: ModalSize.MEDIUM },
            modalData,
        );
    }

    deleteSection(section: IPDFSection) {
        this.otModalService
            .confirm({
                type: ConfirmModalType.DELETE,
                translations: {
                    title: this.translatePipe.transform("DeleteSection"),
                    desc: this.translatePipe.transform("AreYouSureDeleteSection"),
                    confirm: "",
                    cancel: this.translatePipe.transform("Cancel"),
                    submit: this.translatePipe.transform("Confirm"),
                },
            })
            .pipe(
                takeUntil(this.destroy$),
                filter((confirmed: boolean) => confirmed),
            )
            .subscribe(() => this.pdfReportService.deleteSection(section));
    }

    columnTrackBy(index: number, column: IFilterColumn): string {
        return column.columnId;
    }

    trackByIndex(index: number): number {
        return index;
    }

    handleTextEditorChange(textContent: string, section: IPDFSection) {
        section.textContent = textContent;
        this.pdfReportService.setSection(section);
    }

    getColumnLabel = (column: IFilterColumn): string => {
        return this.columnLabelPipe.transform(column);
    }
}
