// Rxjs
import { Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";

// Angular
import {
    Component,
} from "@angular/core";

// 3rd party
import { find } from "lodash";

// Services
import { PDFReportService } from "reportsModule/pdf-reports/services/pdf-report.service";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

// Enums
import { ReportPaperSizes } from "reportsModule/reports-shared/enums/reports.enum";

// Interfaces
import { ILabelValue } from "interfaces/generic.interface";
import { IReportView } from "modules/reports/reports-shared/interfaces/report.interface";

@Component({
    selector: "pdf-settings",
    templateUrl: "./pdf-settings.component.html",
})

export class PDFSettingsComponent  {

    paperSizeOptions: Array<ILabelValue<string>> = [
        { label: "A4", value: ReportPaperSizes.A4 },
        { label: this.translatePipe.transform("USLetter"), value: ReportPaperSizes.Letter },
    ];
    selectedPaperSize: ILabelValue<string>;
    customizeHeaders: boolean;
    showingHeaderSettings: boolean;
    isHoveringFile: boolean;
    selectedFileName: string;
    selectedFileError: string;
    headerOnEveryPage: boolean;
    maxHeaderImageSize = 1;
    allowedHeaderImagesExtensions = ["jpg", "jpeg", "png"];
    private destroy$ = new Subject();

    constructor(
        public translatePipe: TranslatePipe,
        private pdfReportService: PDFReportService,
    ) {}

    ngOnInit() {
        this.pdfReportService.reportView$.pipe(
            takeUntil(this.destroy$),
        ).subscribe((view: IReportView) => {
                if (view.layout) {
                    this.selectedPaperSize = find(this.paperSizeOptions, (option) => option.value === view.layout.paperSize);
                    this.customizeHeaders = view.layout.pageHeader;
                    this.headerOnEveryPage = view.layout.headerOnEveryPage;
                    this.selectedFileName = view.layout.pageHeaderImage ? view.layout.pageHeaderImage.name : "";
                }
                this.showingHeaderSettings = view.layout && view.layout.pageHeader;
            });
    }

    ngOnDestroy() {
        this.destroy$.next();
        this.destroy$.complete();
    }

    selectPaperSize(sizeOption: ILabelValue<string>) {
        this.pdfReportService.setLayoutProperty("paperSize", sizeOption.value);
    }

    toggleCustomizeHeaders(customizeHeaders: boolean) {
        this.pdfReportService.setLayoutProperty("pageHeader", customizeHeaders);
    }

    toggleHeaderOnEveryPage(headerOnEveryPage: boolean) {
        this.pdfReportService.setLayoutProperty("headerOnEveryPage", headerOnEveryPage);
    }

    handleHeaderBackgroundChange(files: File[]) {
        if (!files || !files.length) return;
        this.selectedFileError = "";
        const reader: FileReader = new FileReader();
        reader.readAsDataURL(files[0]);
        reader.onloadend = () => {
            this.pdfReportService.setLayoutProperty("pageHeaderImage", {
                name: files[0].name,
                file: reader.result,
            });
        };
    }

    handleFilesHover(isHovering: boolean) {
        this.isHoveringFile = isHovering;
    }

    handleInvalidFiles(files: File[]) {
        if (!files || !files.length) return;
        this.selectedFileError = this.translatePipe.transform("PDFFileError");
        this.pdfReportService.setLayoutProperty("pageHeaderImage", null);
    }

}
