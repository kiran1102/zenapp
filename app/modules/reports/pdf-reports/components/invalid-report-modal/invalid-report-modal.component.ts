import { Component } from "@angular/core";
import { Subject } from "rxjs";
import { IOtModalContent } from "@onetrust/vitreus";

@Component({
    selector: "invalid-report-modal",
    template: `
        <ot-modal-header (close)="closeModal()">
            <div>{{ 'InvalidReport' | otTranslate }}</div>
        </ot-modal-header>
        <ot-modal-content>
            <ot-confirm-modal-icon type="warning"></ot-confirm-modal-icon>
            <ot-modal-content class="text-center block">
                {{ otModalData }}
            </ot-modal-content>
        </ot-modal-content>
        <ot-modal-footer>
            <button otButton brand
                [innerText]="'Continue' | otTranslate"
                (click)="closeModal()"
                >
                {{ 'Continue' | otTranslate }}
            </button>
        </ot-modal-footer>
    `,
})
export class InvalidReportModalComponent implements IOtModalContent {
    otModalData: string;
    otModalCloseEvent: Subject<null>;

    closeModal() {
        this.otModalCloseEvent.next();
    }

}
