// Angular
import {
    OnInit,
    Component,
} from "@angular/core";

// RXJS
import { Subject } from "rxjs";
import { take, takeUntil } from "rxjs/operators";

// Services
import { PDFReportLogicService } from "../../services/pdf-report-logic.service";
import { PDFReportService } from "../../services/pdf-report.service";

// Interfaces
import {
    IReportSectionModalResolve,
    IReportView,
    ISectionOptionsInfo,
} from "../../../reports-shared/interfaces/report.interface";
import { ILabelValue } from "interfaces/generic.interface";
import { IOtModalContent } from "@onetrust/vitreus";

// Enums
import {
    SectionOptionTranslationKeys,
    PDFSectionTypes,
} from "../../../reports-shared/enums/reports.enum";

// Constant
import { DefaultSelectedQuestionSectionOptions } from "../../../reports-shared/constants/reports.constant";

@Component({
    selector: "pdf-section-modal",
    templateUrl: "./pdf-section-modal.component.html",
})
export class PDFSectionModal implements OnInit, IOtModalContent {

    sectionTypeOptions: Array<ILabelValue<string>> = [];
    isDisabled = true;
    selectedSectionType: ILabelValue<string>;
    selectedQuestionSectionOptions: ISectionOptionsInfo;
    questionSectionOptions = Object.keys(DefaultSelectedQuestionSectionOptions);
    sectionOptionTranslationKeys = SectionOptionTranslationKeys;
    pdfSectionTypes = PDFSectionTypes;
    includeAllQuestions = false;
    isSaving: boolean;
    otModalData: IReportSectionModalResolve;
    otModalCloseEvent: Subject<string>;

    private destroy$ = new Subject();

    constructor(
        private pdfReportLogicService: PDFReportLogicService,
        private pdfReportService: PDFReportService,
    ) {}

    ngOnInit() {
        let moduleType: number;
        this.pdfReportService.reportView$
            .pipe(take(1))
            .subscribe((view: IReportView) => {
                moduleType = view.type;
                this.sectionTypeOptions = this.pdfReportLogicService.getSectionTypes(moduleType, view.sourceId);
            });
        this.selectedSectionType = this.findMatchingOption(this.sectionTypeOptions, this.otModalData.sectionType);
        this.selectedQuestionSectionOptions = this.otModalData.sectionOptions;
    }

    ngOnDestroy() {
        this.destroy$.next();
        this.destroy$.complete();
    }

    save() {
        this.isSaving = true;
        this.otModalData.saveSection(this.otModalData.sectionName, this.otModalData.sectionType, this.selectedQuestionSectionOptions, this.includeAllQuestions)
            .pipe(
                takeUntil(this.destroy$),
            ).subscribe(
                () => {
                    this.cancel();
                    this.isSaving = false;
                });
    }

    cancel() {
        this.otModalCloseEvent.next();
        this.otModalCloseEvent.complete();
    }

    toggleSectionOption(option: string) {
        this.selectedQuestionSectionOptions[option] = !this.selectedQuestionSectionOptions[option];
        this.isDisabled = this.saveDisabled();
    }

    handleSectionTypeChange(sectionType: ILabelValue<string>) {
        this.otModalData.sectionType = sectionType.value;
        this.selectedSectionType = sectionType;
        this.includeAllQuestions = this.selectedSectionType.value === this.pdfSectionTypes.AssessmentAllQuestions;
        if (this.selectedSectionType.value === this.pdfSectionTypes.AssessmentQuestion || this.selectedSectionType.value === this.pdfSectionTypes.AssessmentAllQuestions) {
            this.selectedQuestionSectionOptions = JSON.parse(JSON.stringify(DefaultSelectedQuestionSectionOptions));
        } else {
            this.selectedQuestionSectionOptions = null;
        }
        this.isDisabled = this.saveDisabled();
    }

    handleSectionNameChange(sectionName: string) {
        this.otModalData.sectionName = sectionName;
        if (this.otModalData.sectionName.trim() === "") {
            this.otModalData.sectionName = "";
        }
        this.isDisabled = this.saveDisabled();
    }

    saveDisabled(): boolean {
        let saveDisabled = true;
        if (this.selectedSectionType && (this.selectedSectionType.value === this.pdfSectionTypes.AssessmentQuestion || this.selectedSectionType.value === this.pdfSectionTypes.AssessmentAllQuestions)) {
            for (let i = 0; i < this.questionSectionOptions.length; i++) {
                if (this.selectedQuestionSectionOptions[this.questionSectionOptions[i]]) {
                    saveDisabled = false;
                    break;
                }
            }
        } else {
            saveDisabled = false;
        }
        return !(this.otModalData && this.otModalData.sectionType && this.otModalData.sectionType !== "") || saveDisabled;
    }

    private findMatchingOption(options: Array<ILabelValue<string>>, value: string): ILabelValue<string> {
        return options.find((option: ILabelValue<string>): boolean => option.value === value);
    }
}
