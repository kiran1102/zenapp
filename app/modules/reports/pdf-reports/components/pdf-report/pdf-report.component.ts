// Interfaces
import {
    IPDFSection,
    ISelectionOption,
} from "modules/reports/reports-shared/interfaces/report.interface";
import { IStringMap } from "interfaces/generic.interface";
import {
    ISelection,
    IKeyValue ,
} from "interfaces/generic.interface";
import { ITab } from "interfaces/ot-tabs.interface";
import { IPDFReportState } from "reportsModule/reports-shared/interfaces/report.interface";

// 3rd party
import { Component } from "@angular/core";
import {
    StateService,
    Transition,
    HookResult,
    StateOrName,
    RawParams,
    TransitionService,
    TransitionOptions,
} from "@uirouter/core";

// Constants / Enums
import {
    ReportDetailsTabs,
    SelectionLabels,
} from "reportsModule/reports-shared/constants/reports.constant";
import { ModuleTypes } from "enums/module-types.enum";

// Services
import { PDFReportService } from "reportsModule/pdf-reports/services/pdf-report.service";
import { Permissions } from "modules/shared/services/helper/permissions.service";
import {
    OtModalService,
    ModalSize,
} from "@onetrust/vitreus";
import { LoadingService } from "modules/core/services/loading.service";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

// RXJS
import { Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";

// Modals
import { UnsavedChangesConfirmationModal } from "modules/shared/components/unsaved-changes-confirmation-modal/unsaved-changes-confirmation-modal.component";

@Component({
    selector: "pdf-report",
    templateUrl: "./pdf-report.component.html",
})
export class PDFReportComponent  {
    sections: IPDFSection[];
    currentTab: string;
    tabs: ITab[];
    moduleTypes = ModuleTypes;
    selectionLabels = SelectionLabels;
    reportState: IPDFReportState;

    private tabIds: IStringMap<string> = ReportDetailsTabs;
    private destroy$ = new Subject();
    private transitionDeregisterHook = this.$transitions.onStart({ from: "zen.app.pia.module.reports-new.pdf" }, (transition: Transition): HookResult => {
        return this.handleStateChange(transition.to(), transition.params("to"), transition.options());
    });

    constructor(
        private stateService: StateService,
        public pdfReportService: PDFReportService,
        private translatePipe: TranslatePipe,
        private permissions: Permissions,
        public $transitions: TransitionService,
        private otModalService: OtModalService,
        private loadingService: LoadingService,
    ) {}

    ngOnInit() {
        this.tabs = this.getTabs();
        this.currentTab =  this.tabIds.Details;
        this.pdfReportService.initReportView(this.stateService.params.id);

        this.pdfReportService.reportState$
            .pipe(takeUntil(this.destroy$))
            .subscribe((state: IPDFReportState) => {
                this.reportState = state;
            });
    }

    ngOnDestroy() {
        if (this.transitionDeregisterHook) {
            this.transitionDeregisterHook();
        }
        this.destroy$.next();
        this.destroy$.complete();
    }

    select({ currentValue }: ISelection<ISelectionOption>) {
        this.pdfReportService.selectEntity(currentValue);
    }

    search(searchEvent: IKeyValue<string>) {
        this.pdfReportService.filterEntity(searchEvent.value);
    }

    exportReport() {
        this.pdfReportService.exportReport();
    }

    goToReportList(event: any) {
        this.stateService.go(event.path);
    }

    onTabClick(tab: ITab) {
        this.currentTab = tab.id;
    }

    saveReport() {
        this.pdfReportService.saveReport().subscribe();
    }

    private getTabs(): ITab[] {
        const tabs: ITab[] = [
            {
                id: this.tabIds.Details,
                text: this.translatePipe.transform("Details"),
                otAutoId: "details",
            },
        ];
        if (this.permissions.canShow("ReportsEdit")) {
            tabs.push({
                id: this.tabIds.Settings,
                text: this.translatePipe.transform("Settings"),
                otAutoId: "settings",
            });
        }
        tabs.push({
            id: this.tabIds.PDFBuilder,
            text: this.translatePipe.transform("ViewBuilder"),
            otAutoId: "viewBuilder",
        });
        return tabs;
    }

    private launchSaveChangesModal(callback: () => void): void {
        this.otModalService
            .create(
                UnsavedChangesConfirmationModal,
                {
                    isComponent: true,
                    size: ModalSize.SMALL,
                    hideClose: true,
                },
                {
                    promiseToResolve: () => {
                        this.loadingService.set(true);
                        this.pdfReportService.saveReport().subscribe(
                            (): boolean => {
                                callback();
                                this.loadingService.set(false);
                                return true;
                            });
                    },
                    discardCallback: () => {
                        this.pdfReportService.updateState({ hasChanges: false });
                        callback();
                        return false;
                    },
                },
            ).pipe(
                takeUntil(this.destroy$),
            ).subscribe(
                (success: boolean) => {
                    if (!success) {
                        this.loadingService.set(false);
                    }
                },
            );
    }

    private handleStateChange(toState: StateOrName, toParams: RawParams, options: TransitionOptions): boolean {
        if (!this.reportState.hasChanges) return true;
        const callback = () => {
            this.stateService.go(toState, toParams, options);
        };
        this.launchSaveChangesModal(callback);
        return false;
    }
}
