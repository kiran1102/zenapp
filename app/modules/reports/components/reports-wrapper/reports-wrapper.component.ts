import { StateService } from "@uirouter/core";

// Services
import { GlobalSidebarService } from "modules/global-sidebar/services/one-global-sidebar.service";

// Permissions
import { Permissions } from "modules/shared/services/helper/permissions.service";

// Interfaces
import { INavMenuItem } from "modules/global-sidebar/interfaces/nav-menu-item.interface";
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";

export class ReportsWrapperController implements ng.IComponentController {

    static $inject: string[] = [
        "$rootScope",
        "Permissions",
        "GlobalSidebarService",
    ];

    constructor(
        private $rootScope: IExtendedRootScopeService,
        private permissions: Permissions,
        private globalSidebarService: GlobalSidebarService,
    ) {}

    $onInit() {
        this.globalSidebarService.set(this.getMenuRoutes(), this.$rootScope.t("Reports"), null, true);
    }

    getMenuRoutes(): INavMenuItem[] {
        const menuGroup: INavMenuItem[] = [];

        menuGroup.push(
            {
                id: "ReportsSidebarItem",
                title: this.$rootScope.t("Reports"),
                route: "zen.app.pia.module.reports-new.list",
                activeRouteFn: (stateService: StateService) =>
                    stateService.includes("zen.app.pia.module.reports-new.list"),
                icon: "fa fa-file-text",
            },
        );
        if (this.permissions.canShow("ReportTemplateGallery") && this.permissions.canShow("ReportsCreate")) {
            menuGroup.push(
                {
                    id: "ReportsGallerySidebarItem",
                    title: this.$rootScope.t("Gallery"),
                    route: "zen.app.pia.module.reports-new.template-gallery",
                    activeRouteFn: (stateService: StateService) =>
                        stateService.includes("zen.app.pia.module.reports-new.template-gallery"),
                    icon: "fa fa-clone",
                },
            );
        }
        if (this.permissions.canShow("ReportsScheduler")) {
            menuGroup.push(
                {
                    id: "ReportsSchedule",
                    title: this.$rootScope.t("Scheduler"),
                    route: "zen.app.pia.module.reports-new.schedule",
                    activeRouteFn: (stateService: StateService) =>
                        stateService.includes("zen.app.pia.module.reports-new.schedule"),
                    icon: "fa fa-clock-o",
                },
            );
        }
        if (this.permissions.canShow("CustomDashboards")) {
            menuGroup.push(
                {
                    id: "ReportsDashboardList",
                    title: this.$rootScope.t("Dashboards"),
                    route: "zen.app.pia.module.reports-new.dashboard-list",
                    activeRouteFn: (stateService: StateService) =>
                        stateService.includes("zen.app.pia.module.reports-new.dashboard-list"),
                    icon: "ot ot-dashboard",
                },
            );
        }
        return menuGroup;
    }
}

export const ReportsWrapperComponent: ng.IComponentOptions = {
    controller: ReportsWrapperController,
    template: `
        <div ui-view class="flex-full-height"></div>
    `,
};
