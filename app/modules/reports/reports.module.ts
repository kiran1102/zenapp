import {
    NgModule,
} from "@angular/core";

// Modules
import { ReportScheduleModule } from "reportScheduleModule/report-schedule.module";
import { ReportsListModule } from "reportsModule/reports-list/reports-list.module";
import { ColumnReportsModule } from "reportsModule/column-reports/column-reports.module";
import { PDFReportsModule } from "reportsModule/pdf-reports/pdf-reports.module";
import { DashboardModule } from "reportsModule/dashboard/dashboard.module";

@NgModule({
    imports: [
        ReportScheduleModule,
        ReportsListModule,
        ColumnReportsModule,
        PDFReportsModule,
        DashboardModule,
    ],
})
export class ReportsModule {}
