// Services
import { Permissions } from "modules/shared/services/helper/permissions.service";

export default function routes($stateProvider: ng.ui.IStateProvider) {
    $stateProvider
        .state("zen.app.pia.module.reports-new.list", {
            url: "/list",
            params: { time: null },
            template: "<reports-list class='flex-full-height'></reports-list>",
            resolve: {
                ReportsPermission: [
                    "ReportsPermission", (ReportsPermission: boolean): boolean => {
                        return ReportsPermission;
                    },
                ],
            },
        })
        .state("zen.app.pia.module.reports-new.template-gallery", {
            url: "/template-gallery",
            params: { time: null },
            template: "<reports-template-select class='flex-full-height'></reports-template-select>",
            resolve: {
                ReportsPermission: [
                    "ReportsPermission", (ReportsPermission: boolean): boolean => {
                        return ReportsPermission;
                    },
                ],
            },
        })
        .state("zen.app.pia.module.reports-new.view", {
            url: "/column/:id",
            params: { time: null },
            template: "<column-report class='flex-full-height'></column-report>",
            resolve: {
                ReportsPermission: [
                    "ReportsPermission", (ReportsPermission: boolean): boolean => {
                        return ReportsPermission;
                    },
                ],
            },
        })
        .state("zen.app.pia.module.reports-new.pdf", {
            url: "/pdf/:id",
            params: { time: null },
            template: "<pdf-report class='flex-full-height'></pdf-report>",
            resolve: {
                ReportsPermission: [
                    "ReportsPermission", "Permissions", (ReportsPermission: boolean, permissions: Permissions): boolean => {
                        if (permissions.canShow("PDFBuilder")) {
                            return ReportsPermission;
                        }
                        permissions.goToFallback();
                        return false;
                    },
                ],
            },
        })
        .state("zen.app.pia.module.reports-new.dashboard-list", {
            url: "/dashboards",
            params: { time: null },
            template: "<dashboard-list class='flex-full-height'></dashboard-list>",
            resolve: {
                ReportsPermission: [
                    "ReportsPermission", "Permissions", (ReportsPermission: boolean, permissions: Permissions): boolean => {
                        if (permissions.canShow("CustomDashboards")) {
                            return ReportsPermission;
                        }
                        permissions.goToFallback();
                        return false;
                    },
                ],
            },
        })
        .state("zen.app.pia.module.reports-new.dashboard-edit", {
            url: "/dashboard/:id",
            params: { time: null },
            template: "<dashboard-edit class='flex-full-height'></dashboard-edit>",
            resolve: {
                ReportsPermission: [
                    "ReportsPermission", "Permissions", (ReportsPermission: boolean, permissions: Permissions): boolean => {
                        if (permissions.canShow("CustomDashboards")) {
                            return ReportsPermission;
                        }
                        permissions.goToFallback();
                        return false;
                    },
                ],
            },
        })
    ;
}

routes.$inject = ["$stateProvider"];
