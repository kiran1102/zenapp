declare var angular: any;

import routes from "./reports.routes";

import { ReportsWrapperComponent } from "./components/reports-wrapper/reports-wrapper.component";

export const reportsLegacyModule = angular
    .module("zen.reports", [
        "ui.router",
    ])
    .config(routes)
    .component("reportsWrapper", ReportsWrapperComponent)
    ;
