// 3rd Party
import {
    Injectable,
    Inject,
} from "@angular/core";

// Redux
import { getOrgById } from "oneRedux/reducers/org.reducer";
import { getUserById } from "oneRedux/reducers/user.reducer";
import { StoreToken } from "tokens/redux-store.token";

// Enums
import { InventoryTableIds } from "enums/inventory.enum";
import {
    ReportType,
    ReportsTypePrettyNames,
    ReportListColumnProps,
    ReportTemplateIds,
} from "reportsModule/reports-shared/enums/reports.enum";
import {
    ModuleTypes,
    ModuleTranslationKeys,
} from "enums/module-types.enum";
import {
    FilterValueTypes,
    FilterOperators,
} from "modules/filter/enums/filter.enum";
import { FilterColumnTypes } from "enums/filter-column-types.enum";
import { TemplateStates } from "enums/template-states.enum";

// RXJS
import { map, filter } from "rxjs/operators";
import { Observable } from "rxjs/Observable";
import { of, from } from "rxjs";

// Constants
import { TemplateTypes } from "constants/template-types.constant";
import { EmptyGuid } from "constants/empty-guid.constant";
import { TemplateCriteria } from "constants/template-states.constant";

// Interfaces
import { IProtocolResponse } from "interfaces/protocol.interface";
import {
    IReportTemplateOption,
    ISeededReportTemplate,
    IReportListColumn,
    IReportTemplate,
} from "modules/reports/reports-shared/interfaces/report.interface";
import { ITemplateNameValue } from "modules/template/interfaces/template.interface";
import {
    ICustomTemplate,
    ICustomTemplateDetails,
} from "modules/template/interfaces/custom-templates.interface";
import { ILabelValue } from "interfaces/generic.interface";
import { IUser } from "interfaces/user.interface";
import { IStore } from "interfaces/redux.interface";
import { ISeededGRATemplateDetail } from "modules/global-readiness/interfaces/gra-welcome.interface";

// Services
import { ReportAPI } from "reportsModule/reports-shared/services/report-api.service";
import { Permissions } from "modules/shared/services/helper/permissions.service";
import { TemplateAPI } from "templateServices/api/template-api.service";
import { AATemplateApiService } from "modules/assessment/services/api/aa-template-api.service";
import { GRAWelcomeApiService } from "modules/global-readiness/services/api/gra-welcome-api.service";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";
import { OtDatePipe } from "pipes/ot-date.pipe";

@Injectable()
export class ReportBusinessLogic {

    constructor(
        private translatePipe: TranslatePipe,
        private permissions: Permissions,
        private otDatePipe: OtDatePipe,
        private reportAPI: ReportAPI,
        private AATemplateApi: AATemplateApiService,
        private GRAApi: GRAWelcomeApiService,
        @Inject("TemplateAPI") private templateAPI: TemplateAPI,
        @Inject("$q") private $q: ng.IQService,
        @Inject(StoreToken) private store: IStore,
    ) { }

    getReportModuleOptions(): Array<ILabelValue<string|number>> {
        const options: Array<ILabelValue<string|number>> = [];
        if (this.permissions.canShow("Projects") || this.permissions.canShow("Assessments")) {
            options.push({ label: this.translatePipe.transform("AssessmentAutomation"), value: ModuleTypes.PIA });
        }
        if (this.permissions.canShow("RiskReportPIA") && this.permissions.canShow("Projects")) {
            options.push({ label: this.translatePipe.transform("Risk"), value: ModuleTypes.RISK });
        }
        if (this.permissions.canShow("DataMappingAssessments")) {
            options.push({ label: this.translatePipe.transform("DataMapping"), value: ModuleTypes.DM });
        }
        if (this.permissions.canShow("VendorRiskManagement")) {
            options.push({ label: this.translatePipe.transform("VendorRiskManagement"), value: ModuleTypes.VRM });
        }
        if (this.permissions.canShow("ConsentManagerReport") && !this.permissions.canShow("ReportsConsentUpgrade")) {
            options.push({ label: this.translatePipe.transform("ConsentManager"), value: ModuleTypes.CM });
        }
        if (this.permissions.canShow("ReportsDMInventory")) {
            options.push({ label: this.translatePipe.transform("DataMappingInventory"), value: ModuleTypes.INV });
        }
        if (this.permissions.canShow("IncidentColumnReport")) {
            options.push({ label: this.translatePipe.transform("Incident"), value: ModuleTypes.INC });
        }
        if (this.permissions.canShow("ReadinessAssessmentV2") && this.permissions.canShow("CreateGRAReport") && this.permissions.canShow("GRAReports")) {
            options.push({ label: this.translatePipe.transform("MaturityAndBenchmarking"), value: ModuleTypes.GRAR });
        }
        return options;
    }

    getReportTypeOptions(): Array<ILabelValue<string|number>> {
        return [
            { label: "PDF", value: ReportType.PDF },
            { label: this.translatePipe.transform("Column"), value: ReportType.Column },
        ];
    }

    getInventoryReportTemplateOptions(type: ReportType): Observable<IReportTemplateOption[]> {
        const templateOptions: IReportTemplateOption[] = [];
        if (type === ReportType.Column) {
            if (!this.permissions.canShow("ReportsAssetUpgrade")) {
                templateOptions.push({
                    name: this.translatePipe.transform("CustomAssetReport"),
                    sourceId: String(InventoryTableIds.Assets),
                    templateId: ReportTemplateIds.CustomAsset,
                    layoutTemplateId: null,
                });
            }
            if (!this.permissions.canShow("ReportsPAUpgrade")) {
                templateOptions.push({
                    name: this.translatePipe.transform("CustomProcessingActivityReport"),
                    sourceId: String(InventoryTableIds.Processes),
                    templateId: ReportTemplateIds.CustomProcessingActivity,
                    layoutTemplateId: null,
                });
            }
            if (!this.permissions.canShow("ReportsEntityUpgrade") && this.permissions.canShow("DataMappingEntitiesInventory")) {
                templateOptions.push({
                    name: this.translatePipe.transform("CustomEntityReport"),
                    sourceId: String(InventoryTableIds.Entities),
                    templateId: ReportTemplateIds.CustomEntity,
                    layoutTemplateId: null,
                });
            }
        }
        if (type === ReportType.PDF) {
            if (!this.permissions.canShow("ReportsPAPDFUpgrade")) {
                templateOptions.push({
                    name: this.translatePipe.transform("CustomProcessingActivityReport"),
                    sourceId: String(InventoryTableIds.Processes),
                    templateId: ReportTemplateIds.CustomProcessingActivity,
                    layoutTemplateId: null,
                });
            }
            if (!this.permissions.canShow("ReportsEntityUpgrade") && this.permissions.canShow("DataMappingEntitiesInventory")) {
                templateOptions.push({
                    name: this.translatePipe.transform("CustomEntityReport"),
                    sourceId: String(InventoryTableIds.Entities),
                    templateId: ReportTemplateIds.CustomEntity,
                    layoutTemplateId: null,
                });
            }
        }
        if (
            this.permissions.canShow("SeededArticle30Report")
            && (type !== ReportType.PDF || !this.permissions.canShow("ReportsArt30PDFUpgrade"))
        ) {
            const apiCall: Observable<IProtocolResponse<ISeededReportTemplate[]>> = type === ReportType.PDF ?
                this.reportAPI.getPDFReportTemplates(ModuleTypes.INV) : this.reportAPI.getColumnReportTemplates();
            return apiCall.pipe(
                map((response: IProtocolResponse<ISeededReportTemplate[]>): IReportTemplateOption[] => {
                    if (!response) return templateOptions;
                    return [
                        ...templateOptions,
                        ...response.data.map((template: ISeededReportTemplate): IReportTemplateOption => {
                            return {
                                name: template.nameKey ? this.translatePipe.transform(template.nameKey) : template.name,
                                sourceId: String(InventoryTableIds.Processes),
                                templateId: type === ReportType.Column ? ReportTemplateIds.Article30Column : ReportTemplateIds.Article30PDF,
                                layoutTemplateId: template.id,
                            };
                        }),
                    ];
                }));
        } else {
            return of(templateOptions);
        }
    }

    getVendorReportTemplateOptions(type: ReportType): Observable<IReportTemplateOption[]> {
        const options: IReportTemplateOption[] = [];
        if (
            !this.permissions.canShow("ReportsVendorInventoryUpgrade")
            && (
                (type === ReportType.Column && this.permissions.canShow("VendorInventoryColumnReport"))
                || (type === ReportType.PDF && this.permissions.canShow("VendorInventoryPDFReport"))
            )
        ) {
            options.push({
                name: this.translatePipe.transform("CustomVendorInventoryReport"),
                sourceId: String(InventoryTableIds.Vendors),
                templateId: ReportTemplateIds.CustomVendorInventory,
                layoutTemplateId: null,
            });
        }
        if (type === ReportType.Column || this.permissions.canShow("VendorAssessmentPDFReport")) {
            return from(this.templateAPI.getAssessmentPublishedTemplates(TemplateTypes.VENDOR).then((response: ITemplateNameValue[] | boolean): IReportTemplateOption[] => {
                if (!response) return options;
                return [
                    ...options,
                    ...(response as ITemplateNameValue[]).map((option: ITemplateNameValue): IReportTemplateOption => {
                        return {
                            name: option.name,
                            sourceId: option.id,
                            templateId: option.id,
                            layoutTemplateId: null,
                        };
                    }),
                ];
            }));
        }
        return of(options);
    }

    getAssessmentReportTemplateOptions(viewType: ReportType): Observable<IReportTemplateOption[]> {
        return from(this.AATemplateApi.getAssessmentTemplates(TemplateTypes.PIA, TemplateStates.Published, TemplateCriteria.Both)
            .then((response: IProtocolResponse<ICustomTemplateDetails[]>): IReportTemplateOption[] => {
            const options: ICustomTemplate[] = response.data;

            const reportTemplateOptions: IReportTemplateOption[] = options.map((option: ICustomTemplateDetails): IReportTemplateOption => {
                const templateName: string = option.isActive ? option.name : `${option.name} (${this.translatePipe.transform("Archived")})`;
                return {
                    name: templateName,
                    sourceId: option.rootVersionId,
                    templateId: option.rootVersionId,
                    layoutTemplateId: null,
                };
            });
            if (viewType === ReportType.Column && this.permissions.canShow("AssessmentDetailColumnReport")) {
                reportTemplateOptions.unshift({
                    name: this.translatePipe.transform("AssessmentDetailsReport"),
                    sourceId: EmptyGuid,
                    templateId: ReportTemplateIds.AssessmentDetails,
                    layoutTemplateId: null,
                });
            }
            if (viewType === ReportType.PDF && this.permissions.canShow("SetDefaultAssessmentPDF")) {
                reportTemplateOptions.unshift({
                    name: this.translatePipe.transform("AnyTemplate"),
                    sourceId: EmptyGuid,
                    templateId: ReportTemplateIds.CustomAssessment,
                    layoutTemplateId: null,
                });
            }
            return reportTemplateOptions;
        }));
    }

    getGRAReportTemplatesOptions(): Observable<IReportTemplateOption[]> {
        return this.GRAApi.getSeededTemplateDetails().pipe(
            map((response: ISeededGRATemplateDetail[]): IReportTemplateOption[] => {
                return response.map((option): IReportTemplateOption => {
                        return {
                        name: option.name,
                        sourceId: option.id,
                        templateId: option.id,
                        layoutTemplateId: null,
                    };
                });
            }));
    }

    canShowViewType(moduleType: ModuleTypes): boolean {
        return this.permissions.canShow("PDFBuilder")
            && (
                moduleType === ModuleTypes.INV
                || (moduleType === ModuleTypes.VRM && (this.permissions.canShow("VendorAssessmentPDFReport") || this.permissions.canShow("VendorInventoryPDFReport")))
                || (moduleType === ModuleTypes.PIA && this.permissions.canShow("CustomAssessmentPDF"))
            );
    }

    getRelatedTemplateOptions(moduleType: ModuleTypes): Observable<IReportTemplateOption[]> {
        return this.reportAPI.fetchAllRelatedTemplates(moduleType).pipe(
            filter((response: IReportTemplate[] | boolean): boolean => Boolean(response)),
            map((response: IReportTemplate[] | boolean) => {
                return (response as IReportTemplate[]).map((template: IReportTemplate): IReportTemplateOption => {
                    return {
                        name: template.name,
                        sourceId: template.id,
                        templateId: template.id,
                        layoutTemplateId: null,
                    };
                });
        }));
    }

    getReportListColumns(): IReportListColumn[] {
        return [
            {
                headerText: this.translatePipe.transform("ReportName"),
                modelPropKey: ReportListColumnProps.Name,
                getDisplayText: (value: string): string => {
                    return value;
                },
                sortable: true,
            },
            {
                headerText: this.translatePipe.transform("Module"),
                modelPropKey: ReportListColumnProps.Type,
                getDisplayText: (value: string): string => {
                    return this.translatePipe.transform(ModuleTranslationKeys[value]);
                },
                filterType: FilterValueTypes.MultiSelect,
                dataType: FilterColumnTypes.MultiInt,
                options: this.getReportModuleOptions(),
                sortable: true,
            },
            {
                headerText: this.translatePipe.transform("Default"),
                modelPropKey: ReportListColumnProps.Default,
                isDefaultColumn: true,
                showPermission: this.permissions.canShow("SetDefaultAssessmentPDF") || this.permissions.canShow("SetDefaultPAPDF"),
                sortable: false,
            },
            {
                headerText: this.translatePipe.transform("Organization"),
                modelPropKey: ReportListColumnProps.Org,
                getDisplayText: (value: string): string => {
                    return getOrgById(value)(this.store.getState()).Name;
                },
                filterType: FilterValueTypes.Org,
                dataType: FilterColumnTypes.Multi,
                sortable: true,
            },
            {
                headerText: this.translatePipe.transform("Description"),
                modelPropKey: ReportListColumnProps.Description,
                getDisplayText: (value: string): string => {
                    return value;
                },
                sortable: true,
            },
            {
                headerText: this.translatePipe.transform("Type"),
                sortKey: ReportListColumnProps.ViewType,
                modelPropKey: ReportListColumnProps.ViewType,
                getDisplayText: (value: string): string => {
                    return value === ReportType.PDF ? ReportType.PDF : this.translatePipe.transform(ReportsTypePrettyNames[value]);
                },
                filterType: FilterValueTypes.MultiSelect,
                dataType: FilterColumnTypes.Multi,
                options: [
                    { label: "PDF", value: ReportType.PDF },
                    { label: this.translatePipe.transform("Column"), value: ReportType.Column },
                ],
                showPermission: this.permissions.canShow("PDFBuilder"),
                sortable: true,
            },
            {
                headerText: this.translatePipe.transform("CreatedBy"),
                modelPropKey: ReportListColumnProps.CreatedBy,
                getDisplayText: (value: string): string => {
                    const systemUser: IUser = getUserById(value)(this.store.getState());
                    return systemUser ? systemUser.FullName : value || "";
                },
                dataType: FilterColumnTypes.Multi,
                filterType: FilterValueTypes.MultiUser,
                sortable: true,
            },
            {
                headerText: this.translatePipe.transform("DateCreated"),
                modelPropKey: ReportListColumnProps.CreatedDate,
                defaultSorted: true,
                defaultSortDirection: 1,
                getDisplayText: (value: string): string => {
                    return this.otDatePipe.transform(value);
                },
                filterType: FilterValueTypes.DateRange,
                dataType: FilterColumnTypes.DateRange,
                operator: FilterOperators.Between,
                sortable: true,
            },
            {
                headerText: this.translatePipe.transform("LastModified"),
                modelPropKey: ReportListColumnProps.LastModifiedDate,
                getDisplayText: (value: string): string => {
                    return this.otDatePipe.transform(value);
                },
                sortable: true,
            },
        ];
    }

    noTemplateRequired(selectedModule: ModuleTypes): boolean {
        return selectedModule === ModuleTypes.CM
            || selectedModule === ModuleTypes.INC
            || selectedModule === ModuleTypes.RISK;
    }
}
