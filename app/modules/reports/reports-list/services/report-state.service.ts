// 3rd Party
import {
    Injectable,
} from "@angular/core";
import {
    Observable,
    BehaviorSubject,
} from "rxjs";

// Interfaces
import {
    IReport,
    IFilter,
    IReportState,
    IReportMap,
} from "modules/reports/reports-shared/interfaces/report.interface";
import { IPageable } from "interfaces/pagination.interface";

// Redux
import { ReportActions } from "../../reports-shared/constants/reports.constant";

@Injectable()
export class ReportsStateService {

    reportState$: Observable<IReportState>;
    private initialState: IReportState = {
        allIds: [],
        byId: {},
        tempModel: {},
        selectedReportIds: {},
        isFetchingReportList: false,
        paginationMetadata: {},
        didInvalidate: false,
        sortColumn: "name",
        sortAscending: true,
        searchTerm: "",
        filters: [],
    };
    private _reportState: BehaviorSubject<IReportState> = new BehaviorSubject(this.initialState);

    constructor() {
        this.reportState$ = this._reportState.asObservable();
    }

    dispatch(action: { type: string, [key: string]: any }) {
        this._reportState.next(this.updateState(action));
    }

    getReportById(id: string): IReport {
        return this.state.byId[id];
    }

    getTempReportById(id: string): IReport {
        return this.state.tempModel[id];
    }

    isFetchingReportList(): boolean {
        return this.state.isFetchingReportList;
    }

    getAllReportIds(): string[] {
        return this.state.allIds;
    }

    getAllReportMap(): IReportMap {
        return this.state.byId;
    }

    getReportListInOrder(): IReport[] {
        const allModels = this.getAllReportMap();
        const ids = this.getAllReportIds();
        return ids.map((id: string): IReport => allModels[id]);
    }

    getReportPaginationMetadata(): IPageable {
        return this.state.paginationMetadata as IPageable;
    }

    isReportStateInvalidated(): boolean {
        return this.state.didInvalidate;
    }

    getSelectedReportPageNumber(): number {
        const paginationMetadata = this.getReportPaginationMetadata();
        if (paginationMetadata.content.length === 1 && paginationMetadata.number > 0) {
            return paginationMetadata.number - 1;
        } else {
            return paginationMetadata.number;
        }
    }

    getSortColumn(): string {
        return this.state.sortColumn;
    }

    getSortAscending(): boolean {
        return this.state.sortAscending;
    }

    getSearchTerm(): string {
        return this.state.searchTerm;
    }

    getFilters(): IFilter[] {
        return this.state.filters;
    }

    private updateState(action: { type: string, [key: string]: any }) {
        const state = this._reportState.getValue();
        switch (action.type) {
            case ReportActions.FETCH_REPORT_LIST:
                return { ...state, isFetchingReportList: true };
            case ReportActions.REPORT_LIST_FETCHED:
                return { ...state, isFetchingReportList: false };
            case ReportActions.RECEIVE_REPORT_LIST:
                const { allIds, byId, paginationMetadata } = action;
                return {
                    ...state,
                    allIds,
                    byId,
                    paginationMetadata,
                    selectedReportIds: {},
                };
            case ReportActions.ADD_REPORT:
                const { reportToAdd } = action;
                return {
                    ...state,
                    allIds: [...state.allIds, reportToAdd.id],
                    byId: {
                        ...state.byId,
                        [reportToAdd.id]: reportToAdd,
                    },
                };
            case ReportActions.ADD_BLANK_REPORT:
            case ReportActions.ADD_TEMP_REPORT:
                const { tempReportToAdd } = action;
                return {
                    ...state,
                    tempModel: {
                        ...state.tempModel,
                        [tempReportToAdd.id]: tempReportToAdd,
                    },
                };
            case ReportActions.UPDATE_REPORT:
            case ReportActions.UPDATE_REPORT_WITH_TEMP_PROP: {
                const { reportId, keyValueToMerge } = action;
                return {
                    ...state,
                    byId: {
                        ...state.byId,
                        [reportId]: {...state.byId[reportId], ...keyValueToMerge},
                    },
                };
            }
            case ReportActions.UPDATE_TEMP_REPORT:
                const { reportIdTemp, keyValueToMergeTemp } = action;
                return {
                    ...state,
                    tempModel: {
                        ...state.tempModel,
                        [reportIdTemp]: { ...state.tempModel[reportIdTemp], ...keyValueToMergeTemp },
                    },
                };
            case ReportActions.REPORT_SAVING:
                const { reportIdSaving } = action;
                return {
                    ...state,
                    byId: {
                        ...state.byId,
                        [reportIdSaving]: { ...state.byId[reportIdSaving], isSaving: true },
                    },
                };
            case ReportActions.REPORT_SAVED:
                const { reportIdSaved } = action;
                return {
                    ...state,
                    byId: {
                        ...state.byId,
                        [reportIdSaved]: { ...state.byId[reportIdSaved], isSaving: false, isDirty: false },
                    },
                };
            case ReportActions.DELETE_REPORT:
                const { reportToDelete } = action;
                return {
                    ...state,
                    allIds: state.allIds.filter((id: string): boolean => id !== reportToDelete),
                    byId: { ...state.byId, [reportToDelete]: undefined },
                    tempModel: { ...state.tempModel, [reportToDelete]: undefined },
                };
            case ReportActions.DELETE_TEMP_REPORT:
                const { tempReportToDelete } = action;
                return {
                    ...state,
                    tempModel: { ...state.tempModel, [tempReportToDelete]: undefined },
                };
            case ReportActions.SELECT_REPORT:
                const { reportIdToSelect } = action;
                return {
                    ...state,
                    selectedReportIds: {
                        ...state.selectedReportIds,
                        [reportIdToSelect]: true,
                    },
                };
            case ReportActions.UNSELECT_REPORT:
                const { reportIdToUnSelect } = action;
                return {
                    ...state,
                    selectedReportIds: { ...state.selectedReportIds, [reportIdToUnSelect]: undefined },
                };
            case ReportActions.SELECT_ALL_REPORTS:
                return {
                    ...state,
                    selectedReportIds: Object.keys(state.byId).reduce((collector, key: string): { [key: string]: boolean } => {
                        collector[key] = true;
                        return collector;
                    }, {}),
                };
            case ReportActions.UNSELECT_ALL_REPORTS:
                return {
                    ...state,
                    selectedReportIds: {},
                };
            case ReportActions.VALIDATE_REPORT_STATE:
                return {
                    ...state,
                    didInvalidate: false,
                };
            case ReportActions.INVALIDATE_REPORT_STATE:
                return {
                    ...state,
                    didInvalidate: true,
                };
            case ReportActions.REPORT_EXPORT_BEGIN: {
                const { reportId } = action;
                return {
                    ...state,
                    byId: {
                        ...state.byId,
                        [reportId]: { ...state.byId[reportId], exportInProgress: true },
                    },
                };
            }
            case ReportActions.REPORT_EXPORT_END: {
                const { reportId } = action;
                return {
                    ...state,
                    byId: {
                        ...state.byId,
                        [reportId]: { ...state.byId[reportId], exportInProgress: false },
                    },
                };
            }
            case ReportActions.SORT_REPORT: {
                const { columnName } = action;
                const { sortAscending, sortColumn } = state;
                const ascending = sortColumn !== columnName || !sortAscending;
                return {
                    ...state,
                    sortColumn: columnName,
                    sortAscending: ascending,
                };
            }
            case ReportActions.SEARCH_REPORT: {
                const { searchTerm } = action;
                return {
                    ...state,
                    searchTerm,
                };
            }
            case ReportActions.FILTER_REPORT: {
                const { filters } = action;
                return {
                    ...state,
                    filters,
                };
            }
            default:
                return state;
        }
    }

    private get state(): IReportState {
        return this._reportState.getValue();
    }

}
