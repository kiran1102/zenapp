// 3rd Party
import { Injectable } from "@angular/core";
import { uniqueId } from "lodash";

// RXJS
import {
    filter,
    concatMap,
    map,
} from "rxjs/operators";
import { Observable } from "rxjs/Observable";

// services
import { ReportAPI } from "reportsModule/reports-shared/services/report-api.service";
import { ReportsAdapter } from "reportsModule/reports-shared/services/reports-adapter.service";
import { ReportBusinessLogic } from "reportsModule/reports-list/services/report-business-logic.service";
import { ReportsStateService } from "./report-state.service";
import { UserActionService } from "oneServices/actions/user-action.service";
import {
    OtModalService,
    ConfirmModalType,
} from "@onetrust/vitreus";

// interfaces
import {
    IReport,
    ICreateReportResponse,
    IReportListColumn,
    IReportListResponse,
    IFilterColumnOption,
    IFilter,
} from "modules/reports/reports-shared/interfaces/report.interface";
import {
    IProtocolPacket,
    IProtocolResponse,
} from "interfaces/protocol.interface";
import { IReportMap } from "reportsModule/reports-shared/interfaces/report.interface";
import { IPageable } from "interfaces/pagination.interface";
import { TaskPollingService } from "sharedServices/task-polling.service";

// enums
import { ReportType } from "reportsModule/reports-shared/enums/reports.enum";
import { ModuleTypes } from "enums/module-types.enum";
import { InventoryTableIds } from "enums/inventory.enum";
import { FilterOperators } from "modules/filter/enums/filter.enum";

// Constants
import { ReportActions } from "reportsModule/reports-shared/constants/reports.constant";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

// Components
import { EditReportModal } from "../components/edit-report-modal/edit-report-modal.component";
import { ReportsCreateModal } from "../components/reports-create-modal/reports-create-modal.component";
import { OTNotificationModalComponent } from "sharedModules/components/ot-notification-modal/ot-notification-modal.component";

@Injectable()
export class ReportAction {

    constructor(
        private translatePipe: TranslatePipe,
        private reportAPI: ReportAPI,
        private otModalService: OtModalService,
        private taskPollingService: TaskPollingService,
        private reportsAdapter: ReportsAdapter,
        private reportBusinessLogic: ReportBusinessLogic,
        private reportsStateService: ReportsStateService,
        private userActionService: UserActionService,
    ) {}

    addReport(reportModel: IReport) {
        this.reportsStateService.dispatch({ type: ReportActions.ADD_REPORT, reportToAdd: reportModel });
    }

    addTempReport(reportId: string) {
        const reportModel: IReport = this.reportsStateService.getReportById(reportId);
        this.reportsStateService.dispatch({ type: ReportActions.ADD_TEMP_REPORT, tempReportToAdd: reportModel });
    }

    fetchReportList(page: number = 0): Promise<IProtocolResponse<IReportListResponse>> {
        this.reportsStateService.dispatch({ type: ReportActions.FETCH_REPORT_LIST });
        const columnName: string = this.reportsStateService.getSortColumn();
        const columnAscending: boolean = this.reportsStateService.getSortAscending();
        const searchTerm: string = this.reportsStateService.getSearchTerm();
        const filters: IFilter[] = this.reportsStateService.getFilters();
        return this.reportAPI.getReportList(page, undefined, columnName, columnAscending, searchTerm, filters)
            .toPromise()
            .then((res: IProtocolResponse<IReportListResponse>): Promise<IProtocolResponse<IReportListResponse>> => {
                if (res.result && res.data) {
                    const allIds: string[] = res.data.content.map((report: IReport): string => report.id);
                    const byId: IReportMap = res.data.content.reduce((prev: IReportMap, curr: IReport) => { prev[curr.id] = curr; return prev; }, {});
                    const paginationMetadata: IPageable = {
                        content: res.data.content,
                        first: res.data.first,
                        last: res.data.last,
                        number: res.data.number,
                        numberOfElements: res.data.numberOfElements,
                        size: res.data.size,
                        sort: res.data.sort,
                        totalElements: res.data.totalElements,
                        totalPages: res.data.totalPages,
                    };
                    this.reportsStateService.dispatch({ type: ReportActions.RECEIVE_REPORT_LIST, allIds, byId, paginationMetadata });
                }
                const users: string[]  = Array.from(new Set(res.data.content.map((report) => report.createdBy)));
                return Promise.resolve(this.userActionService.fetchUsersById(users)
                    .then(() => {
                        this.reportsStateService.dispatch({ type: ReportActions.REPORT_LIST_FETCHED});
                        return res;
                    }));
            });
    }

    addBlankReportModel(): string {
        const tempId = `temp-report-${uniqueId()}`;
        const blankReportModel: IReport = {
            name: "",
            description: "",
            id: tempId,
            type: null,
            templateId: "",
            orgId: "",
            creatorId: "",
            createdAt: "",
            modifiedAt: "",
            viewType: ReportType.Column,
        };
        this.reportsStateService.dispatch({ type: ReportActions.ADD_BLANK_REPORT, tempReportToAdd: blankReportModel });
        return tempId;
    }

    openEditReportModal(reportId: string, modalTitle: string = "EditReport", isCopy: boolean = false) {
        this.otModalService
            .create(
                EditReportModal,
                { isComponent: true },
                {
                    modalTitle,
                    reportModelId: reportId,
                    isCopy,
                    promiseToResolve: (report: IReport): Observable<boolean> => {
                        if (isCopy) {
                            return this.copyReportInfo(report)
                                .pipe(
                                    map((res: IProtocolResponse<ICreateReportResponse>): boolean => {
                                        if (res && res.data && res.data.id) {
                                            this.InvalidateReportState();
                                            const pageNumberToSelect = this.reportsStateService.getSelectedReportPageNumber();
                                            this.fetchReportList(pageNumberToSelect);
                                            this.deleteTempReportModel(report.id);
                                            return true;
                                        }
                                        return false;
                                    }));
                        } else {
                            return this.updateReportInfo(report)
                                .pipe(
                                    map((res: boolean): boolean => {
                                        if (res) {
                                            this.updateReportModel(report.id, report);
                                            this.InvalidateReportState();
                                            this.deleteTempReportModel(report.id);
                                            return true;
                                        }
                                        return false;
                                    }));
                        }
                    },
                },
            );
    }

    openDeleteReportFromReportListModal(reportId: string) {
        this.otModalService.confirm({
                type: ConfirmModalType.WARNING,
                translations: {
                    title: this.translatePipe.transform("DeleteReport"),
                    desc: this.translatePipe.transform("SureToDeleteThisReport"),
                    confirm: "",
                    submit: this.translatePipe.transform("Confirm"),
                    cancel: this.translatePipe.transform("Cancel"),
                },
            })
            .pipe(filter((response: boolean) => response))
            .subscribe(() => {
                this.deleteReport(reportId).subscribe((success: boolean): boolean => {
                    if (!success) return;
                    const pageNumberToSelect: number = this.reportsStateService.getSelectedReportPageNumber();
                    this.fetchReportList(pageNumberToSelect);
                });
            });
    }

    updateReportModel(reportId: string, keyValueToMerge: any) {
        this.reportsStateService.dispatch({ type: ReportActions.UPDATE_REPORT, reportId, keyValueToMerge: { ...keyValueToMerge } });
    }

    updateTempReportModel(reportIdTemp: string, keyValueToMergeTemp: any) {
        this.reportsStateService.dispatch({ type: ReportActions.UPDATE_TEMP_REPORT, reportIdTemp, keyValueToMergeTemp: { ...keyValueToMergeTemp, isDirty: true } });
    }

    deleteTempReportModel(reportId: string) {
        if (!reportId) throw new Error("Report Id cannot be undefined");
        this.reportsStateService.dispatch({ type: ReportActions.DELETE_TEMP_REPORT, tempReportToDelete: reportId });
    }

    deleteReport(reportId: string): Observable<boolean> {
        if (!reportId) throw new Error("Report Id cannot be undefined");
        return this.reportAPI.deleteReport(reportId).pipe(map((response: IProtocolPacket): boolean => {
            if (!response.result) return false;
            this.reportsStateService.dispatch({ type: ReportActions.DELETE_REPORT, reportToDelete: reportId });
            return true;
        }));
    }

    createReport(reportId: string): Observable<IProtocolResponse<ICreateReportResponse>> {
        const reportModel: IReport = this.reportsStateService.getTempReportById(reportId);
        const isVendorInventory: boolean = reportModel.type === ModuleTypes.VRM && reportModel.sourceId === String(InventoryTableIds.Vendors);
        const requestPayload = {
            name:             reportModel.name,
            description:      reportModel.description,
            sourceId:         reportModel.type === ModuleTypes.RISK ? null : reportModel.sourceId,
            layoutTemplateId: reportModel.layoutTemplateId,
            reportEntityType: isVendorInventory ? ModuleTypes.INV : reportModel.type,
            reportViewType:   reportModel.viewType,
            visibleColumns:   null,
        };
        if ((reportModel.type === ModuleTypes.INV || reportModel.type === ModuleTypes.INC || isVendorInventory) && !reportModel.layoutTemplateId) {
            return this.reportAPI.getColumns(requestPayload.reportEntityType, reportModel.sourceId).pipe(
                concatMap((response: IProtocolResponse<IFilterColumnOption[]>): Observable<IProtocolResponse<ICreateReportResponse>> => {
                    requestPayload.visibleColumns = this.reportsAdapter.formatColumnData(response.data.filter((column: IFilterColumnOption): boolean => !column.group ));
                    return this.reportAPI.createReport(requestPayload);
                }));
        }
        return this.reportAPI.createReport(requestPayload);
    }

    updateReportInfo(reportModel: IReport): Observable<any> {
        return this.reportAPI.updateReportInfo(reportModel);
    }

    copyReportInfo(reportModel: IReport): Observable<IProtocolResponse<ICreateReportResponse>> {
        return this.reportAPI.copyReportInfo(reportModel);
    }

    exportReport(reportId: string) {
        this.reportsStateService.dispatch({ type: ReportActions.REPORT_EXPORT_BEGIN, reportId });
        this.reportAPI.exportReportAsync(reportId).subscribe((): void => {
            this.otModalService
                .create(
                    OTNotificationModalComponent,
                    { isComponent: true },
                    { modalTitle: "ReportDownload", confirmationText: this.translatePipe.transform("ReportIsGeneratedCheckTasksBar") },
                );
            this.taskPollingService.startPolling();
            this.reportsStateService.dispatch({ type: ReportActions.REPORT_EXPORT_END, reportId });
        });
    }

    validateReportState() {
        this.reportsStateService.dispatch({ type: ReportActions.VALIDATE_REPORT_STATE });
    }

    InvalidateReportState() {
        this.reportsStateService.dispatch({ type: ReportActions.INVALIDATE_REPORT_STATE });
    }

    sortReportList(columnName: string) {
        this.reportsStateService.dispatch({ type: ReportActions.SORT_REPORT, columnName });
        this.fetchReportList();
    }

    searchReportList(searchTerm: string) {
        this.reportsStateService.dispatch({ type: ReportActions.SEARCH_REPORT, searchTerm });
        this.fetchReportList();
    }

    filterReportList(filters: IFilter[]) {
        this.reportsStateService.dispatch({ type: ReportActions.FILTER_REPORT, filters });
        this.fetchReportList();
    }

    openReportsCreateModal(selectedReportModule?: number, selectedReportType?: string, selectedReportTemplate?: string) {
        this.otModalService
            .create(
                ReportsCreateModal,
                { isComponent: true },
                {
                    selectedReportModule,
                    selectedReportType,
                    selectedReportTemplate,
                    blankReportModelId: this.addBlankReportModel(),
                    updateTempReportModel: (id: string, mapKey: any) => this.updateTempReportModel(id, mapKey),
                    deleteTempReportModel: (id: string) => this.deleteTempReportModel(id),
                    createReport: (id: string) => this.createReport(id),
                },
            );
    }

    setReportListFilter(filterMap: { [columnName: string]: Array<string | number> }) {
        const columns: IReportListColumn[] = this.reportBusinessLogic.getReportListColumns();
        const filters: IFilter[] = Object.keys(filterMap).map((property: string) => {
            const matchingColumn: IReportListColumn = columns.find((column: IReportListColumn): boolean => column.modelPropKey === property);
            if (!matchingColumn) return;
            return {
                columnId: property,
                attributeKey: property,
                dataType: matchingColumn.dataType,
                operator: matchingColumn.operator || FilterOperators.Equals,
                values: filterMap[property],
            };
        });
        this.reportsStateService.dispatch({ type: ReportActions.FILTER_REPORT, filters });
    }
}
