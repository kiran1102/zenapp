import {
    NgModule,
} from "@angular/core";

// Modules
import { SharedModule } from "sharedModules/shared.module";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";
import { VitreusModule } from "@onetrust/vitreus";
import { OTPipesModule, PIPES } from "modules/pipes/pipes.module";
import { ReportsSharedModule } from "reportsModule/reports-shared/reports-shared.module";
import { FilterModule } from "modules/filter/filter.module";

// Components
import { ReportsCreateModal } from "reportsModule/reports-list/components/reports-create-modal/reports-create-modal.component";
import { ReportsListComponent } from "reportsModule/reports-list/components/reports-list/reports-list.component";
import { EditReportModal } from "reportsModule/reports-list/components/edit-report-modal/edit-report-modal.component";
import { ReportsTemplateSelectComponent } from "reportsModule/reports-list/components/reports-template-select/reports-template-select.component";

// Services
import { ReportAction } from "reportsModule/reports-list/services/report-action.service";
import { ReportBusinessLogic } from "reportsModule/reports-list/services/report-business-logic.service";
import { ReportsStateService } from "./services/report-state.service";

@NgModule({
    imports: [
        SharedModule,
        CommonModule,
        FormsModule,
        VitreusModule,
        OTPipesModule,
        ReportsSharedModule,
        FilterModule,
    ],
    declarations: [
        ReportsCreateModal,
        ReportsListComponent,
        EditReportModal,
        ReportsTemplateSelectComponent,
    ],
    entryComponents: [
        ReportsCreateModal,
        ReportsListComponent,
        EditReportModal,
        ReportsTemplateSelectComponent,
    ],
    providers: [
        ReportAction,
        ReportBusinessLogic,
        ReportsStateService,
        PIPES, // TODO: Remove from providers when ot-common is implemented. Only necessary if you are using pipe in TS file, else just module is fine
    ],
    schemas: [],
})
export class ReportsListModule {}
