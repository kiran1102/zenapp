// Core
import {
    Component,
    OnInit,
    OnDestroy,
    Inject,
} from "@angular/core";
import { StateService } from "@uirouter/core";

// External libraries
import {
    forEach,
} from "lodash";

// Rxjs
import {
    Subject,
    Subscription,
} from "rxjs";
import {
    takeUntil,
    tap,
    filter,
    switchMap,
} from "rxjs/operators";
import { Observable } from "rxjs/Observable";

// Services
import { ReportBusinessLogic } from "reportsModule/reports-list/services/report-business-logic.service";
import { Permissions } from "modules/shared/services/helper/permissions.service";
import { ReportsStateService } from "../../services/report-state.service";

// Interfaces
import {
    IReport,
    IReportTemplateOption,
    ICreateReportResponse,
    IReportsCreateModalResolve,
} from "modules/reports/reports-shared/interfaces/report.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";
import { ILabelValue } from "interfaces/generic.interface";
import { IOtModalContent } from "@onetrust/vitreus";

// Enums
import { ReportType } from "reportsModule/reports-shared/enums/reports.enum";
import { ModuleTypes } from "enums/module-types.enum";

@Component({
    selector: "reports-create-modal",
    templateUrl: "./reports-create-modal.component.html",
})
export class ReportsCreateModal implements OnInit, OnDestroy, IOtModalContent {

    otModalData: IReportsCreateModalResolve;
    showViewType: boolean;
    templateSelected = false;
    selectedReportModule: ILabelValue<string|number>;
    selectedReportType: ILabelValue<string|number>;
    selectedReportTemplate: IReportTemplateOption;
    reportTemplateDropdownEnabled = false;
    fetchingTemplateList = false;
    createButtonEnabled = false;
    templateSelectPageEnabled = false;
    createInProgress = false;
    showTemplateSelection = true;
    tempReportModel: IReport;
    reportModuleOptions: Array<ILabelValue<string|number>>;
    reportTypesOptions: Array<ILabelValue<string|number>>;
    reportTemplateOptions: IReportTemplateOption[];
    reportDetail = {name: "", description: ""};
    otModalCloseEvent: Subject<null>;
    private subArray: Subscription[] = [];
    private reportModuleSelection$: Subject<number>;
    private reportTypeSelection$: Subject<string>;
    private destroy$ = new Subject();

    constructor(
        private stateService: StateService,
        @Inject(Permissions) public permissions: Permissions,
        private readonly reportBusinessLogic: ReportBusinessLogic,
        private reportsStateService: ReportsStateService,
    ) { }

    ngOnInit() {
        this.reportsStateService.reportState$
            .pipe(takeUntil(this.destroy$))
            .subscribe(() => this.componentWillReceiveState());
        this.reportModuleOptions = this.reportBusinessLogic.getReportModuleOptions();
        this.reportTypesOptions = this.reportBusinessLogic.getReportTypeOptions();
        this.tempReportModel = this.reportsStateService.getTempReportById(this.otModalData.blankReportModelId);
        this.reportModuleSelection$ = new Subject();
        this.reportTypeSelection$ = new Subject();

        const reportModuleSubscription: Subscription = this.reportModuleSelection$.pipe(
            tap((moduleSelection: number) => {
                this.showViewType = this.reportBusinessLogic.canShowViewType(moduleSelection) ;
                this.otModalData.updateTempReportModel(this.otModalData.blankReportModelId, { type: moduleSelection });
                if (this.reportBusinessLogic.noTemplateRequired(moduleSelection)) {
                    this.otModalData.updateTempReportModel(this.otModalData.blankReportModelId, {
                        sourceId: moduleSelection === ModuleTypes.INC ? "10" : "",
                    });
                }
                if (!this.otModalData.selectedReportType) {
                    if (moduleSelection === ModuleTypes.GRAR) {
                        this.reportTypeSelection$.next(ReportType.PDF);
                    } else {
                        this.reportTypeSelection$.next(ReportType.Column);
                    }
                } else {
                    this.selectedReportType = this.reportTypesOptions.find((option: ILabelValue<string|number>) => option.value === this.otModalData.selectedReportType);
                    if (this.selectedReportType) {
                        this.reportTypeSelection$.next(this.selectedReportType.value as string);
                    }
                    this.otModalData.selectedReportType = null;
                }
                this.selectedReportTemplate = null;
                this.reportTemplateDropdownEnabled = false;
                this.showTemplateSelection = !(this.reportBusinessLogic.noTemplateRequired(moduleSelection));
                this.fetchingTemplateList = true;
            }),
            filter((moduleSelection: number): boolean => !this.reportBusinessLogic.noTemplateRequired(moduleSelection)),
            switchMap(this.fetchTemplatesByReportType),
        ).subscribe(this.setTemplateList);
        this.subArray.push(reportModuleSubscription);

        const reportTypeSubscription: Subscription = this.reportTypeSelection$.pipe(
            tap((viewType: ReportType) => {
                this.otModalData.updateTempReportModel(this.otModalData.blankReportModelId, { viewType });
                this.selectedReportTemplate = null;
                this.fetchingTemplateList = true;
            }),
            filter((): boolean => !this.reportBusinessLogic.noTemplateRequired(this.tempReportModel.type)),
            switchMap(() => this.fetchTemplatesByReportType(this.tempReportModel.type)),
        ).subscribe(this.setTemplateList);
        this.subArray.push(reportTypeSubscription);

        if (this.reportModuleOptions.length === 1) {
            this.handleReportModuleChange(this.reportModuleOptions[0]);
        }

        if (this.otModalData.selectedReportModule) {
            this.selectedReportModule = this.reportModuleOptions.find((option: ILabelValue<string|number>) => option.value === this.otModalData.selectedReportModule);
            if (this.selectedReportModule) {
                this.handleReportModuleChange(this.selectedReportModule);
            }
        }
    }

    ngOnDestroy() {
        this.otModalData.deleteTempReportModel(this.otModalData.blankReportModelId);
        if (this.subArray) {
            forEach(this.subArray, (sub: Subscription) => sub.unsubscribe());
        }
        this.destroy$.next();
        this.destroy$.complete();
    }

    handleReportModuleChange(selected: ILabelValue<string|number>) {
        this.templateSelected = false;
        this.reportModuleSelection$.next(selected.value as number);
    }

    handleReportTemplateChange(option: ILabelValue<string|number>) {
        const selectedTemplate: IReportTemplateOption = this.reportTemplateOptions.find((template: IReportTemplateOption): boolean => template.templateId === option.value);
        this.setSelectedTemplate(selectedTemplate);
    }

    setSelectedTemplate(selected: IReportTemplateOption) {
        this.templateSelected = true;
        this.otModalData.updateTempReportModel(this.otModalData.blankReportModelId, {
            sourceId: selected.sourceId,
            layoutTemplateId: selected.layoutTemplateId,
            templateId: selected.templateId,
        });
    }

    handleReportTypeChange(selected: ILabelValue<string|number>) {
        this.templateSelected = false;
        this.reportTypeSelection$.next(selected.value as string);
    }

    handleInputChange(value: string, modelProp: string) {
        this.reportDetail[modelProp] = value;
        this.otModalData.updateTempReportModel(this.otModalData.blankReportModelId, { [modelProp]: value });
    }

    createReport() {
        this.createInProgress = true;
        this.otModalData.createReport(this.otModalData.blankReportModelId).subscribe((res: IProtocolResponse<ICreateReportResponse>) => {
            if (res && res.data && res.data.id) {
                const route: string = res.data.viewType === ReportType.PDF ? "zen.app.pia.module.reports-new.pdf" : "zen.app.pia.module.reports-new.view";
                this.stateService.go(route, { id: res.data.id });
            }
            this.createInProgress = false;
            this.otModalCloseEvent.next();
            this.otModalCloseEvent.complete();
        });
    }

    cancel() {
        this.otModalData.deleteTempReportModel(this.otModalData.blankReportModelId);
        this.otModalCloseEvent.next();
        this.otModalCloseEvent.complete();
    }

    getTemplateDropdownOption = (option: IReportTemplateOption): ILabelValue<string|number> => {
        if (!option) return;
        return { label: option.name, value: option.templateId };
    }

    private componentWillReceiveState() {
        this.tempReportModel = this.reportsStateService.getTempReportById(this.otModalData.blankReportModelId);
        if (!this.tempReportModel) return;
        if (this.tempReportModel.type) {
            this.selectedReportModule = this.reportModuleOptions.find((option: ILabelValue<string|number>) => option.value === this.tempReportModel.type);
        }
        if (this.showViewType) {
            this.selectedReportType = this.reportTypesOptions.find((option: ILabelValue<string|number>) => option.value === this.tempReportModel.viewType);
        }
        if (this.tempReportModel.templateId) {
            this.selectedReportTemplate = this.reportTemplateOptions.find((option: IReportTemplateOption) => option.templateId === this.tempReportModel.templateId);
        }
        this.createButtonEnabled = (this.reportDetail.name && this.reportDetail.name.trim() !== "") && (this.reportBusinessLogic.noTemplateRequired(this.tempReportModel.type) || this.templateSelected);
    }

    private fetchTemplatesByReportType = (reportType: number): Observable<IReportTemplateOption[]> => {
        if (this.permissions.canShow("Assessments") && reportType === ModuleTypes.PIA) {
            return this.reportBusinessLogic.getAssessmentReportTemplateOptions(this.tempReportModel.viewType);
        } else if (reportType === ModuleTypes.GRAR) {
            return this.reportBusinessLogic.getGRAReportTemplatesOptions();
        } else if (reportType === ModuleTypes.VRM) {
            return this.reportBusinessLogic.getVendorReportTemplateOptions(this.tempReportModel.viewType);
        } else if (reportType === ModuleTypes.INV) {
            return this.reportBusinessLogic.getInventoryReportTemplateOptions(this.tempReportModel.viewType);
        } else {
            return this.reportBusinessLogic.getRelatedTemplateOptions(reportType);
        }
    }

    private setTemplateList = (templateList: IReportTemplateOption[]) => {
        this.fetchingTemplateList = false;
        if (templateList) {
            this.reportTemplateOptions = templateList;
        }
        this.selectedReportTemplate = this.reportTemplateOptions.find((option: IReportTemplateOption) => option.templateId === this.otModalData.selectedReportTemplate);
        if (this.selectedReportTemplate) {
            this.templateSelected = true;
            this.setSelectedTemplate(this.selectedReportTemplate);
        }
        this.reportTemplateDropdownEnabled = true;
    }
}
