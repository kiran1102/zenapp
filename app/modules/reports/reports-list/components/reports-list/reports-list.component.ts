// 3rd party
import {
    Component,
    OnInit,
    OnDestroy,
} from "@angular/core";
import { StateService } from "@uirouter/core";

// rxjs
import {
    takeUntil,
    filter,
} from "rxjs/operators";
import { Subject } from "rxjs";

// Services
import { ReportAction } from "reportsModule/reports-list/services/report-action.service";
import { Permissions } from "modules/shared/services/helper/permissions.service";
import { Wootric } from "sharedModules/services/provider/wootric.service";
import { ReportBusinessLogic } from "reportsModule/reports-list/services/report-business-logic.service";
import { TimeStamp } from "oneServices/time-stamp.service";
import { ReportsStateService } from "../../services/report-state.service";
import { ReportAPI } from "reportsModule/reports-shared/services/report-api.service";

// Interfaces
import {
    IReport,
    IReportListActions,
    IReportListItem,
    IReportListColumn,
    IFilter,
} from "modules/reports/reports-shared/interfaces/report.interface";
import { ILabelValue } from "interfaces/generic.interface";
import {
    IFilterOutput,
    IFilterV2,
} from "modules/filter/interfaces/filter.interface";
import { IPageable } from "interfaces/pagination.interface";

// Enums
import { ReportType } from "reportsModule/reports-shared/enums/reports.enum";
import { ModuleTypes } from "enums/module-types.enum";
import {
    FilterValueTypes,
    FilterOperators,
} from "modules/filter/enums/filter.enum";
import { InventoryTableIds } from "enums/inventory.enum";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

// Constants
import { WootricModuleMap } from "modules/shared/constants/wootric.constants";
import { EmptyGuid } from "constants/empty-guid.constant";
import { ReportActions } from "reportsModule/reports-shared/constants/reports.constant";

@Component({
    selector: "reports-list",
    templateUrl: "./reports-list.component.html",
})
export class ReportsListComponent implements OnInit, OnDestroy {

    columnConfig: IReportListColumn[];
    fetchingReport = false;
    originalReportModelList: IReport[];
    reportModelList: IReport[];
    reportPaginationData: IPageable;
    translations: any;
    sortColumn: string;
    sortOrder: string;
    menuOptions: IReportListActions[] = [];
    searchTerm: string;
    menuPosition: string;
    filtersOpen: boolean;
    currentFilterValueType: number;
    currentFilterValues: Array<ILabelValue<number|string>>;
    showingFilteredResults;
    selectedFilters: IFilter[];
    closeFilter$ = new Subject<boolean>();
    showContextMenu = false;
    moduleTypes = ModuleTypes;
    private destroy$ = new Subject();

    constructor(
        private stateService: StateService,
        readonly translatePipe: TranslatePipe,
        readonly permissions: Permissions,
        private reportAction: ReportAction,
        private wootric: Wootric,
        private reportBusinessLogic: ReportBusinessLogic,
        private timeStamp: TimeStamp,
        private reportsStateService: ReportsStateService,
        private reportAPI: ReportAPI,
    ) {}

    ngOnDestroy() {
        this.destroy$.next();
        this.destroy$.complete();
    }

    ngOnInit() {
        this.reportsStateService.reportState$
            .pipe(takeUntil(this.destroy$))
            .subscribe(() => {
                this.componentWillReceiveState();
            });
        this.columnConfig = this.reportBusinessLogic.getReportListColumns();
        this.reportAction.fetchReportList(0);
        this.wootric.run(WootricModuleMap.Reports);
        this.showContextMenu = this.permissions.canShow("ReportsExportNew") || this.permissions.canShow("ReportsEdit") ||
            this.permissions.canShow("ReportsDelete") ||  this.permissions.canShow("ReportsCreate");
    }

    handleRowAction(model: IReportListItem, action: string) {
        switch (action) {
            case ReportActions.DOWNLOAD_REPORT:
                if (!model.exportInProgress) this.reportAction.exportReport(model.id);
                break;
            case ReportActions.EDIT_REPORT:
                this.reportAction.addTempReport(model.id);
                this.reportAction.openEditReportModal(model.id);
                break;
            case ReportActions.DELETE_REPORT:
                this.reportAction.openDeleteReportFromReportListModal(model.id);
                break;
            case ReportActions.COPY_REPORT:
                this.reportAction.addTempReport(model.id);
                this.reportAction.openEditReportModal(model.id, "CopyReport", true);
                break;
            case ReportActions.SET_DEFAULT_INV_PDF:
                this.setDefaultInventoryPDF(model.id);
                break;
            case ReportActions.SET_DEFAULT_PIA_PDF:
                this.setDefaultAssessmentPDF(model.id);
                break;
        }
    }

    setDefaultInventoryPDF(modelId: string) {
        this.reportAPI.setDefaultPDF(modelId, ModuleTypes.INV).pipe(
            filter((res): boolean => {
                return res.result;
            }),
        ).subscribe(() => {
            this.reportAction.fetchReportList(this.reportPaginationData.number);
        });
    }

    setDefaultAssessmentPDF(modelId: string) {
        this.reportAPI.setDefaultPDF(modelId, ModuleTypes.PIA).pipe(
            filter((res): boolean => {
                return res.result;
            }),
        ).subscribe(() => {
            this.reportAction.fetchReportList(this.reportPaginationData.number);
        });
    }

    handleSearch(searchTerm: string) {
        this.reportAction.searchReportList(searchTerm);
    }

    handlePagination(page: number) {
        this.reportAction.fetchReportList(page);
    }

    toggleSort({sortKey}) {
        this.reportAction.sortReportList(sortKey);
    }

    goToReport(report: IReport) {
        const route: string = report.viewType === ReportType.PDF ? "zen.app.pia.module.reports-new.pdf" : "zen.app.pia.module.reports-new.view";
        this.stateService.go(route, { id: report.id });
    }

    goToRoute(route: string) {
        this.stateService.go(route);
    }

    setMenuClass(event?: MouseEvent) {
        this.menuPosition = "bottom-right";
        if ((window.innerHeight - 200) / 2 < (event.clientY - 200)) {
            this.menuPosition = "top-right";
        }
    }

    setMenuOptions(menuIsOpen: boolean, row: IReportListItem) {
        if (!menuIsOpen) return;
        this.menuOptions = [];
        if (this.permissions.canShow("ReportsExportNew") && row.viewType === ReportType.Column) {
            this.menuOptions.push({
                label: this.translatePipe.transform("Download"),
                getIconClass: (report: IReportListItem) => report.exportInProgress ? "ot-spinner ot-pulse" : "ot-download",
                action: (report: IReportListItem) => this.handleRowAction(report, ReportActions.DOWNLOAD_REPORT),
            });
        }
        if (this.permissions.canShow("ReportsEdit")) {
            this.menuOptions.push({
                label: this.translatePipe.transform("Edit"),
                iconClass: "ot-edit",
                action: (report: IReportListItem) => this.handleRowAction(report, ReportActions.EDIT_REPORT),
            });
        }
        if (this.permissions.canShow("ReportsDelete")) {
            this.menuOptions.push({
                label: this.translatePipe.transform("Delete"),
                iconClass: "ot-trash",
                action: (report: IReportListItem) => this.handleRowAction(report, ReportActions.DELETE_REPORT),
            });
        }
        if (this.permissions.canShow("ReportsCreate")) {
            this.menuOptions.push({
                label: this.translatePipe.transform("Copy"),
                iconClass: "ot-copy",
                action: (report: IReportListItem) => this.handleRowAction(report, ReportActions.COPY_REPORT),
            });
        }
        if (this.permissions.canShow("SetDefaultPAPDF", "UpdateDefaultPAPDF") && row.viewType === ReportType.PDF && row.type === ModuleTypes.INV && row.sourceId === InventoryTableIds.Processes.toString() && row.layout.default !== true) {
            this.menuOptions.push({
                label: this.translatePipe.transform("SetAsDefaultPDF"),
                iconClass: "ot-check",
                action: (report: IReportListItem) => this.handleRowAction(report, ReportActions.SET_DEFAULT_INV_PDF),
            });
        }

        if (this.permissions.canShow("SetDefaultAssessmentPDF", "UpdateDefaultAssessmentPDF") && row.viewType === ReportType.PDF && (row.type === ModuleTypes.PIA || row.type === ModuleTypes.AE) && row.sourceId === EmptyGuid && row.layout.default !== true) {
            this.menuOptions.push({
                label: this.translatePipe.transform("SetAsDefaultPDF"),
                iconClass: "ot-check",
                action: (report: IReportListItem) => this.handleRowAction(report, ReportActions.SET_DEFAULT_PIA_PDF),
            });
        }
    }

    toggleFilters(isOpen: boolean) {
        this.filtersOpen = isOpen;
        if (!isOpen) this.closeFilter$.next(isOpen);
    }

    getFieldLabelValue(column: IReportListColumn): ILabelValue<string> {
        return { label: column.headerText, value: column.modelPropKey };
    }

    getFilterableColumns(column: IReportListColumn): boolean {
        return Boolean(column.filterType);
    }

    setFilterValues(selectedField: ILabelValue<string>) {
        if (!selectedField) return;
        const selectedColumn: IReportListColumn = this.columnConfig.find((column: IReportListColumn): boolean => column.modelPropKey === selectedField.value);
        this.currentFilterValueType = selectedColumn.filterType;
        this.currentFilterValues = selectedColumn.options;
    }

    applyFilters(filters: IFilterOutput[]) {
        this.toggleFilters(false);
        this.reportAction.filterReportList(this.deserializeFilters(filters));
    }

    serializeFilters = (newFilter: IFilter): IFilterV2<IFilter, null, ILabelValue<string|number>> => {
        const matchingColumn: IReportListColumn = this.columnConfig.find((column: IReportListColumn): boolean => column.modelPropKey === newFilter.columnId);
        return {
            field: matchingColumn,
            operator: null,
            values: this.serializeFilterSelections(newFilter.values, matchingColumn),
            type: matchingColumn.filterType,
        };
    }

    getColumnPermitted = (column: IReportListColumn): boolean => {
        if (typeof column.showPermission === "undefined") {
            return true;
        } else {
            return column.showPermission;
        }
    }

    private serializeFilterSelections(selections: Array<string | number>, column: IReportListColumn): Array<ILabelValue<string | number>> {
        return selections.map((selection: string | number): ILabelValue<string | number> => {
            if (
                column.filterType === FilterValueTypes.DateRange
                || column.filterType === FilterValueTypes.User
                || column.filterType === FilterValueTypes.MultiUser
                || column.filterType === FilterValueTypes.Org
            ) {
                return { label: "", value: selection };
            }
            return column.options.find((option: ILabelValue<string|number>): boolean => option.value === selection);
        });
    }

    private deserializeFilters(filters: IFilterOutput[]): IFilter[] {
        if (!filters) return [];
        return (filters as IFilterOutput[]).map((filterOutput: IFilterOutput): IFilter => {
            const matchingColumn: IReportListColumn = this.columnConfig.find((column: IReportListColumn): boolean => column.modelPropKey === filterOutput.field.value);
            return {
                columnId: matchingColumn.modelPropKey,
                attributeKey: matchingColumn.modelPropKey,
                dataType: matchingColumn.dataType,
                operator: matchingColumn.operator || FilterOperators.Equals,
                values: this.deserializeFilterValues(filterOutput.values, matchingColumn.filterType, matchingColumn.modelPropKey),
            };
        });
    }

    private deserializeFilterValues(values: Array<ILabelValue<string | number>>, type: FilterValueTypes, columnId: string | number): Array<string | number> {
        if (!(values && values.length)) return [];
        if (type === FilterValueTypes.DateRange) {
            const stateDateRangeFilter = this.reportsStateService.getFilters().find((stateFilter) => stateFilter.columnId === columnId);
            const dateRangeUnchanged = stateDateRangeFilter ? stateDateRangeFilter.values[1] === values[1].value : false;
            return [
                values[0].value,
                dateRangeUnchanged ? values[1].value : this.timeStamp.getEndOfDayISO(values[1].value as string),
            ];
        }
        return values.map((value: ILabelValue<number | string>): number | string => value.value);
    }

    private componentWillReceiveState() {
        this.fetchingReport = this.reportsStateService.isFetchingReportList();
        this.sortColumn = this.reportsStateService.getSortColumn();
        this.sortOrder = this.reportsStateService.getSortAscending() ? "asc" : "desc";
        this.searchTerm = this.reportsStateService.getSearchTerm();
        this.selectedFilters = this.reportsStateService.getFilters();
        this.showingFilteredResults = this.selectedFilters && this.selectedFilters.length;
        const didReportInvalidate: boolean = this.reportsStateService.isReportStateInvalidated();
        this.reportModelList = this.reportsStateService.getReportListInOrder();
        this.reportPaginationData = this.reportsStateService.getReportPaginationMetadata();
        if (didReportInvalidate) { // this was done for performance improvement. every time there was a dispatch, the table was re-rendering
            this.reportAction.validateReportState();
        }
    }

}
