import {
    Component,
} from "@angular/core";

// Constants
import { ReportTemplateCards } from "reportsModule/reports-shared/constants/reports.constant";

// Services
import { ReportAction } from "reportsModule/reports-list/services/report-action.service";
import { McmModalService } from "modules/mcm/mcm-modal.service";

@Component({
    selector: "reports-template-select",
    templateUrl: "./reports-template-select.component.html",
})
export class ReportsTemplateSelectComponent {

    cards = ReportTemplateCards;

    constructor(
        public reportAction: ReportAction,
        public mcmModalService: McmModalService,
    ) {}
}
