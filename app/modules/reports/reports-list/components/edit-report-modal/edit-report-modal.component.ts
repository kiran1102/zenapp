// Rxjs
import { Subject } from "rxjs";

// 3rd Party
import {
    Component,
    OnInit,
    Inject,
} from "@angular/core";

// Tokens
import { StoreToken } from "tokens/redux-store.token";

// Services
import { ReportsStateService } from "../../services/report-state.service";

// Interfaces
import { IReport,
    IEditReportModalResolve,
} from "modules/reports/reports-shared/interfaces/report.interface";
import {
    IStore,
} from "interfaces/redux.interface";
import { IOtModalContent } from "@onetrust/vitreus";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

@Component({
    selector: "edit-report-modal",
    templateUrl: "./edit-report-modal.component.html",
})
export class EditReportModal implements OnInit, IOtModalContent {

    canSubmit = false;
    isSubmitting = false;
    modalTitle: string;
    reportModel: IReport;
    reportModelCopy: IReport;
    isCopy: boolean;
    selectedOrgId: string;
    otModalData: IEditReportModalResolve;
    otModalCloseEvent: Subject<null>;

    constructor(
        @Inject(StoreToken) public store: IStore,
        private translatePipe: TranslatePipe,
        private reportsStateService: ReportsStateService,
    ) {}

    ngOnInit(): void {
        this.modalTitle = this.otModalData.modalTitle;
        this.isCopy = this.otModalData.isCopy;
        this.reportModel = this.reportsStateService.getReportById(this.otModalData.reportModelId);
        this.reportModelCopy = {...this.reportModel};
        if (this.otModalData.isCopy) {
            this.reportModelCopy.name = `${this.reportModelCopy.name} - ${this.translatePipe.transform("Copy")}`;
            this.canSubmit = true;
        }
        this.selectedOrgId = this.reportModel.orgGroupId;
    }

    closeModal(): void {
        this.otModalCloseEvent.next();
        this.otModalCloseEvent.complete();
    }

    handleInputChange(value: string, isNameField: boolean): void {
        if (isNameField) {
            this.reportModelCopy.name = value;
        } else {
            this.reportModelCopy.description = value;
        }

        this.canSubmit = (this.reportModelCopy.name && this.reportModelCopy.name.trim() !== "") && (this.reportModel.name !== this.reportModelCopy.name || this.reportModel.description !== this.reportModelCopy.description);
    }

    submitModal(): void {
        this.isSubmitting = true;
        this.otModalData.promiseToResolve(this.reportModelCopy)
            .subscribe((success: boolean) => {
                if (success) this.closeModal();
                this.isSubmitting = false;
            });
    }

    orgSelect(event: string) {
        this.selectedOrgId = event;
        this.reportModelCopy.orgGroupId = this.selectedOrgId;
    }
}
