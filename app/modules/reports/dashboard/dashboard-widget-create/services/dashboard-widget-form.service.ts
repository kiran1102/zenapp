// Angular
import { Injectable } from "@angular/core";

// RXJS
import {
    BehaviorSubject,
    Observable,
    Subject,
} from "rxjs";
import {
    filter,
    map,
    elementAt,
    switchMap,
    tap,
    take,
} from "rxjs/operators";

// Interfaces
import { IProtocolResponse } from "interfaces/protocol.interface";
import {
    IWidgetEntity,
    IWidgetSubEntity,
    IWidgetFormData,
    IWidgetFormValidity,
    IDashboardWidget,
} from "reportsModule/dashboard/dashboard-api/interfaces/dashboard.interface";
import {
    IFilterColumnOption,
    IFilter,
} from "reportsModule/reports-shared/interfaces/report.interface";

// Services
import { DashboardAPIService } from "reportsModule/dashboard/dashboard-api/services/dashboard-api.service";
import { ReportAPI } from "reportsModule/reports-shared/services/report-api.service";
import { ReportsAdapter } from "reportsModule/reports-shared/services/reports-adapter.service";
import { DashboardLogicService } from "reportsModule/dashboard/dashboard-api/services/dashboard-logic.service";

// Enums
import { ReportModuleTypes } from "reportsModule/reports-shared/enums/reports.enum";
import { WidgetCreateForms } from "reportsModule/dashboard/dashboard-api/enums/dashboard.enum";

// Constants
import { EntityMetaDataTypes } from "reportsModule/dashboard/dashboard-api/constants/dashboard.constant";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

interface ILabelExtension {
    label: string;
}

@Injectable()
export class DashboardWidgetFormService {

    countEntityOptions$: Observable<IWidgetEntity[]>;
    countEntitySelection$: Observable<IWidgetEntity>;

    countSubEntityOptions$: Observable<IWidgetSubEntity[]>;
    countSubEntitySelection$: Observable<IWidgetSubEntity>;

    groupByEntityOptions$: Observable<IWidgetEntity[]>;
    groupByEntitySelection$: Observable<IWidgetEntity>;

    groupBySubEntityOptions$: Observable<IWidgetSubEntity[]>;
    groupBySubEntitySelection$: Observable<IWidgetSubEntity>;

    groupByAttributeOptions$: Observable<IFilterColumnOption[]>;
    groupByAttributeSelection$: Observable<IFilterColumnOption>;

    filterSelection$: Observable<IFilter[]>;

    validateForm$: Observable<void>;
    formValidityState$: Observable<IWidgetFormValidity>;

    private _countEntityOptions$ = new BehaviorSubject<IWidgetEntity[]>([]);
    private _countEntitySelection$ = new BehaviorSubject<IWidgetEntity>(null);

    private _countSubEntityOptions$ = new BehaviorSubject<IWidgetSubEntity[]>([]);
    private _countSubEntitySelection$ = new BehaviorSubject<IWidgetSubEntity>(null);

    private _groupByEntityOptions$ = new BehaviorSubject<IWidgetEntity[]>([]);
    private _groupByEntitySelection$ = new BehaviorSubject<IWidgetEntity>(null);

    private _groupBySubEntityOptions$ = new BehaviorSubject<IWidgetSubEntity[]>([]);
    private _groupBySubEntitySelection$ = new BehaviorSubject<IWidgetSubEntity>(null);

    private _groupByAttributeOptions$ = new BehaviorSubject<IFilterColumnOption[]>([]);
    private _groupByAttributeSelection$ = new BehaviorSubject<IFilterColumnOption>(null);

    private _filterSelection$ = new BehaviorSubject<IFilter[]>([]);

    private _validateForm$ = new Subject<void>();
    private defaultFormValidity: IWidgetFormValidity = {
        allValid: true,
        [WidgetCreateForms.Count]: true,
        [WidgetCreateForms.GroupBy]: true,
        [WidgetCreateForms.Filters]: true,
    };
    private _formValidityState$ = new BehaviorSubject<IWidgetFormValidity>({...this.defaultFormValidity});

    constructor(
        private dashboardAPIService: DashboardAPIService,
        private reportAPI: ReportAPI,
        private reportsAdapter: ReportsAdapter,
        private translatePipe: TranslatePipe,
        private dashboardLogicService: DashboardLogicService,
    ) {
        this.countEntityOptions$ = this._countEntityOptions$.asObservable();
        this.countEntitySelection$ = this._countEntitySelection$.asObservable();

        this.countSubEntityOptions$ = this._countSubEntityOptions$.asObservable();
        this.countSubEntitySelection$ = this._countSubEntitySelection$.asObservable();

        this.groupByEntityOptions$ = this._groupByEntityOptions$.asObservable();
        this.groupByEntitySelection$ = this._groupByEntitySelection$.asObservable();

        this.groupBySubEntityOptions$ = this._groupBySubEntityOptions$.asObservable();
        this.groupBySubEntitySelection$ = this._groupBySubEntitySelection$.asObservable();

        this.groupByAttributeOptions$ = this._groupByAttributeOptions$.asObservable();
        this.groupByAttributeSelection$ = this._groupByAttributeSelection$.asObservable();

        this.filterSelection$ = this._filterSelection$.asObservable();

        this.validateForm$ = this._validateForm$.asObservable();
        this.formValidityState$ = this._formValidityState$.asObservable();
    }

    initializeForm(moduleType: ReportModuleTypes, widget: IDashboardWidget<unknown>) {
        this.reset();
        if (widget) this.setFormFields(widget);
        this.dashboardAPIService.getCountEntityOptions(moduleType)
            .pipe(
                filter((res: IProtocolResponse<IWidgetEntity[]>): boolean => res.result),
            )
            .subscribe((res: IProtocolResponse<IWidgetEntity[]>) => {
                this._countEntityOptions$.next(this.translateLabels<IWidgetEntity>(res.data, "nameKey"));
                if (this._countEntityOptions.length === 1 && !widget) this.updateCountEntity(this._countEntityOptions[0]);
            });
    }

    updateCountEntity(selection: IWidgetEntity, autoSelect: boolean = true) {
        this._countEntitySelection$.next(selection);
        this._countSubEntitySelection$.next(null);
        if (!(this._countEntitySelection && this._countEntitySelection.id)) return;
        if (this._countEntitySelection.entitySubTypeOptions && this._countEntitySelection.entitySubTypeOptions.length) {
            this._countSubEntityOptions$.next(this.translateLabels<IWidgetSubEntity>(this._countEntitySelection.entitySubTypeOptions, "nameKey"));
            return;
        }
        if (this._countEntitySelection.entitySubTypeLink && this._countEntitySelection.entitySubTypeLink.url) {
            this.dashboardAPIService.getSubEntityOptions(this._countEntitySelection.entitySubTypeLink.url)
                .pipe(
                    filter((res: IProtocolResponse<IWidgetSubEntity[]>): boolean => res.result),
                )
                .subscribe((res: IProtocolResponse<IWidgetSubEntity[]>) => {
                    this._countSubEntityOptions$.next(this.translateLabels<IWidgetSubEntity>(res.data, "nameKey"));
                    if (this._countSubEntityOptions.length === 1 && autoSelect) this.updateCountSubEntity(this._countSubEntityOptions[0]);
                });
        }
    }

    updateCountSubEntity(selection: IWidgetSubEntity, autoSelect: boolean = true) {
        this._countSubEntitySelection$.next(selection);
        this._groupByEntitySelection$.next(null);
        if (!(this._countEntitySelection && this._countEntitySelection.id) || !(this._countSubEntitySelection && this._countSubEntitySelection.id)) return;
        this.dashboardAPIService.getGroupByEntityOptions(this._countEntitySelection.id as number, this._countSubEntitySelection.id as string)
            .pipe(
                filter((res: IProtocolResponse<IWidgetEntity[]>): boolean => res.result),
            )
            .subscribe((res: IProtocolResponse<IWidgetEntity[]>) => {
                this._groupByEntityOptions$.next(this.translateLabels<IWidgetEntity>(res.data, "nameKey"));
                if (this._groupByEntityOptions.length === 1 && autoSelect) this.updateGroupByEntity(this._groupByEntityOptions[0]);
            });
    }

    updateGroupByEntity(selection: IWidgetEntity, autoSelect: boolean = true) {
        this._groupByEntitySelection$.next(selection);
        this._groupBySubEntitySelection$.next(null);
        if (!(this._groupByEntitySelection && this._groupByEntitySelection.id)) return;
        if (this._groupByEntitySelection.entitySubType) {
            const translatedOption: IWidgetSubEntity = this.translateLabel<IWidgetSubEntity>(this._groupByEntitySelection.entitySubType, "nameKey");
            this._groupBySubEntityOptions$.next([translatedOption]);
            if (autoSelect) this.updateGroupBySubEntity(translatedOption);
            return;
        }
        if (this._groupByEntitySelection.entitySubTypeOptions && this._groupByEntitySelection.entitySubTypeOptions.length) {
            this._groupBySubEntityOptions$.next(this.translateLabels<IWidgetSubEntity>(this._groupByEntitySelection.entitySubTypeOptions, "nameKey"));
            return;
        }
        if (this._groupByEntitySelection.entitySubTypeLink && this._groupByEntitySelection.entitySubTypeLink.url) {
            this.dashboardAPIService.getSubEntityOptions(this._groupByEntitySelection.entitySubTypeLink.url)
                .pipe(
                    filter((res: IProtocolResponse<IWidgetSubEntity[]>): boolean => res.result),
                )
                .subscribe((res: IProtocolResponse<IWidgetSubEntity[]>) => {
                    this._groupBySubEntityOptions$.next(this.translateLabels<IWidgetSubEntity>(res.data, "nameKey"));
                    if (this._groupBySubEntityOptions.length === 1) this._groupBySubEntitySelection$.next(this._groupBySubEntityOptions[0]);
                });
            return;
        }
        const translatedCountOption: IWidgetSubEntity = this.translateLabel<IWidgetSubEntity>(this._countSubEntitySelection, "nameKey");
        this._groupBySubEntityOptions$.next([translatedCountOption]);
        if (autoSelect) this.updateGroupBySubEntity(translatedCountOption);
    }

    updateGroupBySubEntity(selection: IWidgetSubEntity, autoSelect: boolean = true) {
        this._groupBySubEntitySelection$.next(selection);
        this._groupByAttributeSelection$.next(null);
        if (!(this._groupByEntitySelection && this._groupByEntitySelection.id) || !(this._groupBySubEntitySelection && this._groupBySubEntitySelection.id)) return;
        this.reportAPI.getModuleColumnMetaData(EntityMetaDataTypes[this._groupByEntitySelection.id], this._groupBySubEntitySelection.id as string)
            .pipe(
                filter((res: IProtocolResponse<IFilterColumnOption[]>): boolean => Boolean(res.result && res.data && res.data.length)),
                map((res: IProtocolResponse<IFilterColumnOption[]>): IFilterColumnOption[] => {
                    return this.reportsAdapter.createGroupNames(
                        this.dashboardLogicService.filterAllowedAttributes(res.data),
                        "name",
                    ) as IFilterColumnOption[];
                }),
            )
            .subscribe((columns: IFilterColumnOption[]) => {
                this._groupByAttributeOptions$.next(columns);
                if (this._groupByAttributeOptions.length === 1 && autoSelect) this.updateGroupByAttribute(this._groupByAttributeOptions[0]);
            });
    }

    updateGroupByAttribute(selection: IFilterColumnOption) {
        this._groupByAttributeSelection$.next(selection);
    }

    setCurrentFilters(filters: IFilter[]) {
        this._filterSelection$.next(filters);
    }

    setFormFields(widget: IDashboardWidget<unknown>) {
        const setCountEntity$: Observable<IWidgetEntity[]> = this.updateSelectionOnOptionChange<IWidgetEntity>(
            this._countEntityOptions$,
            widget.primaryReportableType.entityType.toString(),
            (option: IWidgetEntity) => this.updateCountEntity(option, false),
        );
        const setCountSubEntity$: Observable<IWidgetSubEntity[]> = this.updateSelectionOnOptionChange<IWidgetSubEntity>(
            this._countSubEntityOptions$,
            widget.primaryReportableType.reportSubType.toString(),
            (option: IWidgetSubEntity) => this.updateCountSubEntity(option, false),
        );
        const setGroupByEntity$: Observable<IWidgetEntity[]> = this.updateSelectionOnOptionChange<IWidgetEntity>(
            this._groupByEntityOptions$,
            widget.groupBy.reportableType.entityType.toString(),
            (option: IWidgetEntity) => this.updateGroupByEntity(option, false),
        );
        const setGroupBySubEntity$: Observable<IWidgetSubEntity[]> = this.updateSelectionOnOptionChange<IWidgetSubEntity>(
            this._groupBySubEntityOptions$,
            widget.groupBy.reportableType.reportSubType.toString(),
            (option: IWidgetSubEntity) => this.updateGroupBySubEntity(option, false),
        );
        const setGroupByAttribute$: Observable<IFilterColumnOption[]> = this.updateSelectionOnOptionChange<IFilterColumnOption>(
            this._groupByAttributeOptions$,
            widget.groupBy.attributeKey.toString(),
            (option: IFilterColumnOption) => this.updateGroupByAttribute(option),
        );
        setCountEntity$
            .pipe(
                switchMap(() => setCountSubEntity$),
                switchMap(() => setGroupByEntity$),
                switchMap(() => setGroupBySubEntity$),
                switchMap(() => setGroupByAttribute$),
                tap(() => {
                    this.setCurrentFilters(
                        widget.filter && widget.filter.criteria && widget.filter.criteria.AND && widget.filter.criteria.AND.length
                        ? widget.filter.criteria.AND
                        : [],
                    );
                }),
                take(1),
            ).subscribe();
    }

    validateForm(): Observable<IWidgetFormValidity> {
        return Observable.create((observer) => {
            this._formValidityState$
                .pipe(elementAt(this._validateForm$.observers.length))
                .subscribe((state: IWidgetFormValidity) => {
                    observer.next(state);
                });
            this._validateForm$.next();
        });
    }

    markFormAsValid(formId: WidgetCreateForms, isValid: boolean) {
        const currentValidityState = {...this._formValidityState$.getValue()};
        currentValidityState[formId] = isValid;
        let allValid = true;
        for (const prop in currentValidityState) {
            if (prop !== "allValid" && !currentValidityState[prop]) {
                allValid = false;
            }
        }
        currentValidityState.allValid = allValid;
        this._formValidityState$.next(currentValidityState);
    }

    getFormData(): IWidgetFormData {
        return {
            countEntity: this._countEntitySelection,
            countSubEntity: this._countSubEntitySelection,
            groupByEntity: this._groupByEntitySelection,
            groupBySubEntity: this._groupBySubEntitySelection,
            groupByAttribute: this._groupByAttributeSelection,
            filters: this._filterSelection,
        };
    }

    private updateSelectionOnOptionChange<T extends {id: string | number}>(options$: BehaviorSubject<T[]>, matchingId: string, selectionFn: (option: T) => void): Observable<T[]> {
        return options$.pipe(
            filter((options: T[]) => Boolean(options && options.length && options.find((option) => option.id === matchingId))),
            tap((options: T[]) => {
                const matchingOption: T = options.find((option) => option.id === matchingId);
                selectionFn(matchingOption);
            }),
        );
    }

    private translateLabels<T extends object>(options: T[], labelKey: string): Array<T & ILabelExtension> {
        return options.map((option: T) => this.translateLabel(option, labelKey));
    }

    private translateLabel<T extends object>(option: T, labelKey: string): T & ILabelExtension {
        return { ...(option as object), label: this.translatePipe.transform(option[labelKey]) } as T & ILabelExtension;
    }

    private get _countEntityOptions(): IWidgetEntity[] {
        return this._countEntityOptions$.getValue();
    }

    private get _countEntitySelection(): IWidgetEntity {
        return this._countEntitySelection$.getValue();
    }

    private get _countSubEntityOptions(): IWidgetSubEntity[] {
        return this._countSubEntityOptions$.getValue();
    }

    private get _countSubEntitySelection(): IWidgetSubEntity {
        return this._countSubEntitySelection$.getValue();
    }

    private get _groupByEntityOptions(): IWidgetEntity[] {
        return this._groupByEntityOptions$.getValue();
    }

    private get _groupByEntitySelection(): IWidgetEntity {
        return this._groupByEntitySelection$.getValue();
    }

    private get _groupBySubEntityOptions(): IWidgetSubEntity[] {
        return this._groupBySubEntityOptions$.getValue();
    }

    private get _groupBySubEntitySelection(): IWidgetSubEntity {
        return this._groupBySubEntitySelection$.getValue();
    }

    private get _groupByAttributeOptions(): IFilterColumnOption[] {
        return this._groupByAttributeOptions$.getValue();
    }

    private get _groupByAttributeSelection(): IFilterColumnOption {
        return this._groupByAttributeSelection$.getValue();
    }

    private get _filterSelection(): IFilter[] {
        return this._filterSelection$.getValue();
    }

    private reset() {
        this._countEntityOptions$.next([]);
        this._countEntitySelection$.next(null);

        this._countSubEntityOptions$.next([]);
        this._countSubEntitySelection$.next(null);

        this._groupByEntityOptions$.next([]);
        this._groupByEntitySelection$.next(null);

        this._groupBySubEntityOptions$.next([]);
        this._groupBySubEntitySelection$.next(null);

        this._groupByAttributeOptions$.next([]);
        this._groupByAttributeSelection$.next(null);

        this._filterSelection$.next([]);

        this._formValidityState$.next({...this.defaultFormValidity});
    }

}
