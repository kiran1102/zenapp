import {
    Component,
    OnInit,
    OnDestroy,
    Input,
} from "@angular/core";

// RXJS
import {
    Subject,
    BehaviorSubject,
    combineLatest,
} from "rxjs";
import {
    takeUntil,
    filter,
    withLatestFrom,
    map,
    tap,
    switchMap,
} from "rxjs/operators";

// Interfaces
import {
    IFilterColumnOption,
    IFilterValueOption,
    IFilter,
} from "reportsModule/reports-shared/interfaces/report.interface";
import { ILabelValue } from "interfaces/generic.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

// Services
import { DashboardWidgetFormService } from "../../services/dashboard-widget-form.service";
import { ReportAPI } from "reportsModule/reports-shared/services/report-api.service";
import { ReportsAdapter } from "reportsModule/reports-shared/services/reports-adapter.service";
import { DashboardLogicService } from "reportsModule/dashboard/dashboard-api/services/dashboard-logic.service";

// Enums
import { ModuleTypes } from "enums/module-types.enum";
import { FilterOperators } from "modules/filter/enums/filter.enum";

// Constants
import { EntityMetaDataTypes } from "reportsModule/dashboard/dashboard-api/constants/dashboard.constant";

@Component({
    selector: "dashboard-widget-filter",
    templateUrl: "./dashboard-widget-filter.component.html",
})
export class DashboardWidgetFilterComponent implements OnInit, OnDestroy {

    @Input() moduleType: ModuleTypes;
    @Input() set filter(newFilter: IFilter) {
        this.filter$.next(newFilter);
    }

    attributeOptions$: BehaviorSubject<IFilterColumnOption[]> = new BehaviorSubject([]);
    attributeSelection: IFilterColumnOption;
    attributeSearchTerm: string;
    showAttributeError = false;

    operatorOptions: Array<ILabelValue<FilterOperators>> = [];
    operatorSelection: ILabelValue<FilterOperators>;
    operatorSearchTerm: string;
    showOperatorError = false;

    valueOptions: Array<ILabelValue<string>> = [];
    valueSelections: Array<ILabelValue<string>> = [];
    valueSearchTerm: string;
    showValueError = false;

    private entityId: string | number;
    private subEntityId: string | number;
    private filter$: BehaviorSubject<IFilter> = new BehaviorSubject(null);

    private destroy$ = new Subject();

    constructor(
        public dashboardWidgetFormService: DashboardWidgetFormService,
        private translatePipe: TranslatePipe,
        private reportAPI: ReportAPI,
        private reportsAdapter: ReportsAdapter,
        private dashboardLogicService: DashboardLogicService,
    ) { }

    ngOnInit() {
        // TODO : decouple filter entity selections from count
        this.dashboardWidgetFormService.countSubEntitySelection$
            .pipe(
                takeUntil(this.destroy$),
                withLatestFrom(this.dashboardWidgetFormService.countEntitySelection$),
                tap(([subEntitySelection, entitySelection]) => {
                    this.attributeOptions$.next([]);
                    this.attributeSelection = null;
                    this.entityId = entitySelection ? entitySelection.id : null;
                    this.subEntityId = subEntitySelection ? subEntitySelection.id : null;
                }),
                filter(() => Boolean(this.entityId && this.subEntityId)),
                switchMap(() => this.reportAPI.getModuleColumnMetaData(EntityMetaDataTypes[this.entityId], this.subEntityId as string)),
                filter((res: IProtocolResponse<IFilterColumnOption[]>): boolean => Boolean(res.result && res.data && res.data.length)),
                map((res: IProtocolResponse<IFilterColumnOption[]>): IFilterColumnOption[] => {
                    return this.reportsAdapter.createGroupNames(
                        this.dashboardLogicService.filterAllowedAttributes(res.data),
                        "name",
                    ) as IFilterColumnOption[];
                }),
            )
            .subscribe((columns: IFilterColumnOption[]) => {
                this.attributeOptions$.next(columns);
                if (columns.length === 1) this.attributeSelection = columns[0];
            });
        combineLatest(this.attributeOptions$, this.filter$)
            .pipe(
                takeUntil(this.destroy$),
                filter((value: [IFilterColumnOption[], IFilter]) => {
                    return Boolean(
                        value[0] && value[0].length
                        && value[1]
                        && value[0].find((option: IFilterColumnOption) => option.id === value[1].columnId),
                    );
                }),
            )
            .subscribe((value: [IFilterColumnOption[], IFilter]) => {
                this.attributeSelection = value[0].find((option: IFilterColumnOption) => option.id === value[1].columnId);
                this.onAttributeChange(this.attributeSelection);
                // TODO make these dynamic
                this.operatorSelection = this.operatorOptions[0];
                this.onOperatorChange(this.operatorSelection);
                this.valueSelections = value[1].values.map((selection: string) => {
                    return this.valueOptions.find((option) => option.label === selection || option.value === selection);
                });
            });
    }

    ngOnDestroy() {
        this.destroy$.next();
        this.destroy$.complete();
    }

    onAttributeChange(selection: IFilterColumnOption) {
        this.attributeSelection = selection;
        this.showAttributeError = false;
        if (selection) {
            this.operatorOptions = this.getOperatorOptions(this.attributeSelection.type);
            if (this.operatorOptions.length < 2) {
                this.operatorSelection = this.operatorOptions[0];
                this.showOperatorError = false;
            }
            this.valueOptions = this.getValueOptions(this.attributeSelection);
            if (this.valueOptions.length < 2) {
                this.valueSelections = [...this.valueOptions];
                this.showValueError = false;
            }
        } else {
            this.operatorOptions = [];
            this.operatorSelection = null;
            this.valueOptions = [];
            this.valueSelections = [];
        }
    }

    onOperatorChange(selection: ILabelValue<FilterOperators>) {
        this.operatorSelection = selection;
        this.showOperatorError = false;
    }

    onValueChange(selections: Array<ILabelValue<string>>) {
        this.valueSelections = selections;
        this.showValueError = false;
    }

    getOperatorOptions(attributeType: number): Array<ILabelValue<FilterOperators>> {
        // TODO : add other operator options
        return [{ label: this.translatePipe.transform("EqualTo"), value: FilterOperators.Equals }];
    }

    getValueOptions(attribute: IFilterColumnOption): Array<ILabelValue<string>> {
        if (!(attribute && attribute.valueOptions)) return [];
        return attribute.valueOptions.map((option: IFilterValueOption): ILabelValue<string> => {
            return {
                label: option.translationKey ? this.translatePipe.transform(option.translationKey) : option.value as string,
                value: option.value as string,
            };
        });
    }

    attributeSearch({ key, value }) {
        if (key === "Enter") return;
        this.attributeSearchTerm = value;
    }

    operatorSearch({ key, value }) {
        if (key === "Enter") return;
        this.operatorSearchTerm = value;
    }

    valueSearch({ key, value }) {
        if (key === "Enter") return;
        this.valueSearchTerm = value;
    }

    getIsFilterValid(): boolean {
        this.showAttributeError = !this.attributeSelection;
        this.showOperatorError = !this.operatorSelection;
        this.showValueError = !(this.valueSelections && this.valueSelections.length);
        return !this.showAttributeError && !this.showOperatorError && !this.showValueError;
    }

    getCurrentFilter(): IFilter {
        return {
            columnId: this.attributeSelection.id,
            attributeKey: this.attributeSelection.id,
            entityType: this.attributeSelection.entityType,
            dataType: this.attributeSelection.type,
            operator: this.operatorSelection.value,
            values: this.valueSelections.map((selection: ILabelValue<string>): string => selection.label || selection.value),
        };
    }

}
