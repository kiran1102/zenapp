import {
    Component,
    OnInit,
    OnDestroy,
    Input,
    ViewChildren,
    QueryList,
} from "@angular/core";

// RXJS
import { Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";

// Interfaces
import { IFilter } from "reportsModule/reports-shared/interfaces/report.interface";

// Enums
import { ModuleTypes } from "enums/module-types.enum";
import { WidgetCreateForms } from "reportsModule/dashboard/dashboard-api/enums/dashboard.enum";

// Services
import { DashboardWidgetFormService } from "../../services/dashboard-widget-form.service";

// Components
import { DashboardWidgetFilterComponent } from "../dashboard-widget-filter/dashboard-widget-filter.component";

@Component({
    selector: "dashboard-widget-filter-list",
    templateUrl: "./dashboard-widget-filter-list.component.html",
})
export class DashboardWidgetFilterListComponent implements OnInit, OnDestroy {

    @Input() moduleType: ModuleTypes;

    @ViewChildren(DashboardWidgetFilterComponent) filterControllers: QueryList<DashboardWidgetFilterComponent>;

    filters: IFilter[] = [];

    private destroy$ = new Subject();

    constructor(
        private dashboardWidgetFormService: DashboardWidgetFormService,
    ) {}

    ngOnInit() {
        this.dashboardWidgetFormService.validateForm$
            .pipe(takeUntil(this.destroy$))
            .subscribe(() => {
                const { allValid, currentFilters } = this.getCurrentFilters();
                if (allValid) this.dashboardWidgetFormService.setCurrentFilters(currentFilters);
                this.dashboardWidgetFormService.markFormAsValid(WidgetCreateForms.Filters, allValid);
            });
        this.dashboardWidgetFormService.filterSelection$
            .pipe(takeUntil(this.destroy$))
            .subscribe((filters: IFilter[]) => {
                this.filters = filters;
            });
    }

    ngOnDestroy() {
        this.destroy$.next();
        this.destroy$.complete();
    }

    addFilter() {
        this.filters.push(null);
    }

    removeFilter(index: number) {
        this.filters.splice(index, 1);
    }

    getCurrentFilters(): { allValid: boolean, currentFilters: IFilter[] } {
        let allValid = true;
        if (!(this.filterControllers && this.filterControllers.length)) return { allValid, currentFilters: [] };
        const currentFilters: IFilter[] = this.filterControllers.map((controller: DashboardWidgetFilterComponent) => {
            if (!allValid) return null;
            allValid = controller.getIsFilterValid();
            if (allValid) return controller.getCurrentFilter();
            return null;
        });
        return { allValid, currentFilters };
    }

}
