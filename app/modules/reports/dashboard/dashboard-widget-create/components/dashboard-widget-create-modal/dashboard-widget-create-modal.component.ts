import {
    Component,
    OnInit,
} from "@angular/core";

// RXJS
import {
    Subject,
    Observable,
} from "rxjs";
import {
    takeUntil,
    tap,
    filter,
    concatMap,
} from "rxjs/operators";

// Interfaces
import { IOtModalContent } from "@onetrust/vitreus/src/app/vitreus/components/modal/modal";
import {
    IDashboardWidgetModalData,
    IDashboardWidget,
    IDashboardWidgetRequest,
    IWidgetFormData,
    IWidgetFormValidity,
    IWidgetFilters,
} from "reportsModule/dashboard/dashboard-api/interfaces/dashboard.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";
import { ILabelValue } from "interfaces/generic.interface";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

// Contants
import {
    WidgetOptions,
    DefaultBarChart,
    DefaultDonutChart,
    DefaultTableWidget,
} from "reportsModule/dashboard/dashboard-api/constants/dashboard.constant";

// Services
import { DashboardAPIService } from "reportsModule/dashboard/dashboard-api/services/dashboard-api.service";
import { DashboardWidgetFormService } from "../../services/dashboard-widget-form.service";

// Enums
import {
    WidgetTranslationKeys,
    WidgetTypes,
    EntityTypes,
} from "reportsModule/dashboard/dashboard-api/enums/dashboard.enum";
import {
    ReportModuleTypes,
    ReportEntityTypes,
} from "reportsModule/reports-shared/enums/reports.enum";

enum ModalStages {
    First,
    Second,
}

@Component({
    selector: "dashboard-widget-create-modal",
    templateUrl: "./dashboard-widget-create-modal.component.html",
})
export class DashboardWidgetCreateModalComponent implements IOtModalContent, OnInit {

    modalTitleKey = "AddWidget";

    isLoading = false;
    editMode = false;
    modalStages = ModalStages;
    currentStage = ModalStages.First;
    widgetName: string;
    widgetTypes: Array<ILabelValue<WidgetTypes>>;
    widgetData: unknown;
    widgetSelection: ILabelValue<WidgetTypes>;
    moduleSelection: ILabelValue<EntityTypes>;
    showWarningMessage: boolean;
    warningClosed = false;

    otModalCloseEvent: Subject<string>;
    otModalData: IDashboardWidgetModalData;

    private widgetDataMap = {
        [WidgetTypes.Bar]: DefaultBarChart,
        [WidgetTypes.Donut]: DefaultDonutChart,
        [WidgetTypes.Table]: DefaultTableWidget,
    };

    constructor(
        public dashboardWidgetFormService: DashboardWidgetFormService,
        private translatePipe: TranslatePipe,
        private dashboardAPIService: DashboardAPIService,
    ) { }

    ngOnInit() {
        this.widgetTypes = this.getWidgetTypes(this.otModalData.moduleType);
        if (this.otModalData.widget) {
            this.editMode = true;
            this.modalTitleKey = "EditWidget";
            this.widgetName = this.otModalData.widget.name;
            this.widgetSelection = this.findWidgetSelection(this.otModalData.widget.type);
            this.showWarningMessage = true;
        } else {
            this.widgetSelection = this.widgetTypes[0];
            this.showWarningMessage = false;
        }
        this.widgetData = this.widgetDataMap[this.widgetSelection.value];
        this.dashboardWidgetFormService.initializeForm(this.otModalData.moduleType, this.otModalData.widget);
    }

    validateAndSave() {
        this.isLoading = true;
        this.dashboardWidgetFormService.validateForm()
            .pipe(
                takeUntil(this.otModalCloseEvent),
                tap((validity: IWidgetFormValidity) => { if (!validity.allValid) this.isLoading = false; }),
                filter((validity: IWidgetFormValidity) => validity.allValid),
                concatMap(() => this.saveWidget()),
                tap(() => this.isLoading = false),
                filter((response: IProtocolResponse<IDashboardWidget<unknown>>) => Boolean(response.result && response.data)),
            ).subscribe((response: IProtocolResponse<IDashboardWidget<unknown>>) => {
                this.otModalCloseEvent.next(response.data.id);
                this.otModalCloseEvent.complete();
            });
    }

    saveWidget(): Observable<IProtocolResponse<IDashboardWidget<unknown>>> {
        const widgetFormData: IWidgetFormData = this.dashboardWidgetFormService.getFormData();
        const widget: IDashboardWidgetRequest = {
            name: this.widgetName,
            type: this.widgetSelection ? this.widgetSelection.value : null,
            primaryReportableType: {
                entityType: widgetFormData.countEntity.id as number,
                reportSubType: widgetFormData.countSubEntity.id as string,
            },
            groupBy: {
                reportableType: {
                    entityType: widgetFormData.groupByEntity.id as number,
                    reportSubType: widgetFormData.groupBySubEntity.id as string,
                },
                attributeKey: widgetFormData.groupByAttribute.id,
                attributeEntityType: widgetFormData.groupByAttribute.entityType as ReportEntityTypes,
            },
            filter: widgetFormData.filters && widgetFormData.filters.length
                ? {entityType: widgetFormData.countEntity.id as EntityTypes, criteria: { AND: widgetFormData.filters }}
                : {} as IWidgetFilters,
        };
        return this.editMode ? this.dashboardAPIService.editWidget(this.otModalData.widget.id, widget) : this.dashboardAPIService.createWidget(widget);
    }

    onWidgetChange(widget: ILabelValue<WidgetTypes>) {
        this.widgetSelection = widget;
        this.widgetData = this.widgetDataMap[widget.value];
    }

    onNameChange(name: string) {
        this.widgetName = name ? name.trim() : "";
    }

    setStage(stage: ModalStages) {
        this.currentStage = stage;
        if (stage === ModalStages.First) {
            this.showWarningMessage = true;
        }
    }

    closeWarning() {
        this.showWarningMessage = false;
        this.warningClosed = true;
    }

    closeModal() {
        this.otModalCloseEvent.next();
        this.otModalCloseEvent.complete();
    }

    private getWidgetTypes(moduleId: ReportModuleTypes): Array<ILabelValue<WidgetTypes>> {
        return WidgetOptions[moduleId].map((option: WidgetTypes): ILabelValue<WidgetTypes> => ({
            label: this.translatePipe.transform(WidgetTranslationKeys[option]),
            value: option,
        }));
    }

    private findWidgetSelection(type: WidgetTypes): ILabelValue<WidgetTypes> {
        return this.widgetTypes.find((widgetType: ILabelValue<WidgetTypes>) => {
            return widgetType.value === type;
        });
    }

}
