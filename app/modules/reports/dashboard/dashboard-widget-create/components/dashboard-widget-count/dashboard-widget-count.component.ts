import {
    Component,
    Input,
    OnInit,
    OnDestroy,
} from "@angular/core";

// RXJS
import { Subject } from "rxjs";
import {
    takeUntil,
    withLatestFrom,
} from "rxjs/operators";

// Interfaces
import {
    IWidgetEntity,
    IWidgetSubEntity,
} from "reportsModule/dashboard/dashboard-api/interfaces/dashboard.interface";

// Services
import { DashboardWidgetFormService } from "../../services/dashboard-widget-form.service";

// Enums
import { ModuleTypes } from "enums/module-types.enum";
import { WidgetCreateForms } from "reportsModule/dashboard/dashboard-api/enums/dashboard.enum";

@Component({
    selector: "dashboard-widget-count",
    templateUrl: "./dashboard-widget-count.component.html",
})
export class DashboardWidgetCountComponent implements OnInit, OnDestroy {

    @Input() moduleType: ModuleTypes;

    countEntitySearchTerm: string;
    countSubEntitySearchTerm: string;

    private destroy$ = new Subject();

    constructor(
        public dashboardWidgetFormService: DashboardWidgetFormService,
    ) { }

    ngOnInit() {
        this.dashboardWidgetFormService.validateForm$
            .pipe(
                takeUntil(this.destroy$),
                withLatestFrom(
                    this.dashboardWidgetFormService.countEntitySelection$,
                    this.dashboardWidgetFormService.countSubEntitySelection$,
                ),
            )
            .subscribe(([validate, entity, subEntity]) => {
                // TODO : mark invalid fields in UI
                this.dashboardWidgetFormService.markFormAsValid(WidgetCreateForms.Count, Boolean(entity && subEntity));
            });
    }

    ngOnDestroy() {
        this.destroy$.next();
    }

    onCountEntityChange(selection: IWidgetEntity) {
        this.dashboardWidgetFormService.updateCountEntity(selection);
    }

    onCountSubEntityChange(selection: IWidgetSubEntity) {
        this.dashboardWidgetFormService.updateCountSubEntity(selection);
    }

    countEntitySearch({ key, value }) {
        if (key === "Enter") return;
        this.countEntitySearchTerm = value;
    }

    countSubEntitySearch({ key, value }) {
        if (key === "Enter") return;
        this.countSubEntitySearchTerm = value;
    }

}
