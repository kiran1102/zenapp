import {
    Component,
    Input,
    OnInit,
    OnDestroy,
} from "@angular/core";

// RXJS
import { Subject } from "rxjs";
import {
    takeUntil,
    withLatestFrom,
} from "rxjs/operators";

// Interfaces
import {
    IWidgetEntity,
    IWidgetSubEntity,
} from "reportsModule/dashboard/dashboard-api/interfaces/dashboard.interface";
import { IFilterColumnOption } from "reportsModule/reports-shared/interfaces/report.interface";

// Services
import { DashboardWidgetFormService } from "../../services/dashboard-widget-form.service";

// Enums
import { ModuleTypes } from "enums/module-types.enum";
import { WidgetCreateForms } from "reportsModule/dashboard/dashboard-api/enums/dashboard.enum";

@Component({
    selector: "dashboard-widget-group-by",
    templateUrl: "./dashboard-widget-group-by.component.html",
})
export class DashboardWidgetGroupByComponent implements OnInit, OnDestroy {

    @Input() moduleType: ModuleTypes;

    groupByEntitySearchTerm: string;
    groupByAttributeSearchTerm: string;
    groupBySubEntitySearchTerm: string;

    showEntityError = false;
    showSubEntityError = false;
    showAttributeError = false;

    private destroy$ = new Subject();

    constructor(
        public dashboardWidgetFormService: DashboardWidgetFormService,
    ) { }

    ngOnInit() {
        this.dashboardWidgetFormService.validateForm$
            .pipe(
                takeUntil(this.destroy$),
                withLatestFrom(
                    this.dashboardWidgetFormService.groupByEntitySelection$,
                    this.dashboardWidgetFormService.groupBySubEntitySelection$,
                    this.dashboardWidgetFormService.groupByAttributeSelection$,
                ),
            )
            .subscribe(([validate, entity, subEntity, attribute]) => {
                this.showEntityError = !entity;
                this.showSubEntityError = !subEntity;
                this.showAttributeError = !attribute;
                this.dashboardWidgetFormService.markFormAsValid(WidgetCreateForms.GroupBy, Boolean(entity && subEntity && attribute));
            });
    }

    ngOnDestroy() {
        this.destroy$.next();
    }

    onGroupByEntityChange(selection: IWidgetEntity) {
        this.dashboardWidgetFormService.updateGroupByEntity(selection);
        this.showEntityError = false;
    }

    onGroupBySubEntityChange(selection: IWidgetSubEntity) {
        this.dashboardWidgetFormService.updateGroupBySubEntity(selection);
        this.showSubEntityError = false;
    }

    onGroupByAttributeChange(selection: IFilterColumnOption) {
        this.dashboardWidgetFormService.updateGroupByAttribute(selection);
        this.showAttributeError = false;
    }

    groupByEntitySearch({ key, value }) {
        if (key === "Enter") return;
        this.groupByEntitySearchTerm = value;
    }

    groupBySubEntitySearch({ key, value }) {
        if (key === "Enter") return;
        this.groupBySubEntitySearchTerm = value;
    }

    groupByAttributeSearch({ key, value }) {
        if (key === "Enter") return;
        this.groupByAttributeSearchTerm = value;
    }

}
