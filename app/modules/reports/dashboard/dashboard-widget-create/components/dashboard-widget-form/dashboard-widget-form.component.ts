import {
    Component,
    Input,
} from "@angular/core";

// Interfaces
import { ILabelValue } from "interfaces/generic.interface";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

// Enums
import { ReportModuleTypes } from "reportsModule/reports-shared/enums/reports.enum";
import { WidgetCreateForms } from "reportsModule/dashboard/dashboard-api/enums/dashboard.enum";

// Services
import { DashboardWidgetFormService } from "../../services/dashboard-widget-form.service";

enum TabValues {
    count,
    groupBy,
    filter,
}

interface ITabOption {
    label: string;
    value: TabValues;
    formId: WidgetCreateForms;
}

@Component({
    selector: "dashboard-widget-form",
    templateUrl: "./dashboard-widget-form.component.html",
})
export class DashboardWidgetFormComponent {

    @Input() moduleType: ReportModuleTypes;

    tabValues = TabValues;
    widgetCreateForms = WidgetCreateForms;
    tabOptions: ITabOption[] = [
        { label: this.translatePipe.transform("GroupBy"), value: this.tabValues.groupBy, formId: WidgetCreateForms.GroupBy },
        { label: this.translatePipe.transform("Filter"), value: this.tabValues.filter, formId: WidgetCreateForms.Filters },
    ];
    currentTab: ILabelValue<number> = this.tabOptions[0];

    constructor(
        public dashboardWidgetFormService: DashboardWidgetFormService,
        private translatePipe: TranslatePipe,
    ) { }

    changeTab(tab: ITabOption) {
        this.currentTab = tab;
    }

}
