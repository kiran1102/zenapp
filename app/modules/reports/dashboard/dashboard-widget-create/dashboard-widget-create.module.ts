import {
    NgModule,
} from "@angular/core";

// Modules
import { DashboardAPIModule } from "../dashboard-api/dashboard-api.module";
import { DashboardWidgetModule } from "../dashboard-widget/dashboard-widget.module";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";
import {
    OtModalModule,
    OtRadioModule,
    OtTabsModule,
    OtLoadingModule,
    OtButtonModule,
    OtFormModule,
    OtDirectivesModule,
    OtNotificationModule,
    OtAddRemoveModule,
} from "@onetrust/vitreus";
import { OTPipesModule, PIPES } from "modules/pipes/pipes.module";
import { TranslateModule } from "@ngx-translate/core";

// Components
import { DashboardWidgetCreateModalComponent } from "./components/dashboard-widget-create-modal/dashboard-widget-create-modal.component";
import { DashboardWidgetFormComponent } from "./components/dashboard-widget-form/dashboard-widget-form.component";
import { DashboardWidgetFilterComponent } from "./components/dashboard-widget-filter/dashboard-widget-filter.component";
import { DashboardWidgetFilterListComponent } from "./components/dashboard-widget-filter-list/dashboard-widget-filter-list.component";
import { DashboardWidgetGroupByComponent } from "./components/dashboard-widget-group-by/dashboard-widget-group-by.component";
import { DashboardWidgetCountComponent } from "./components/dashboard-widget-count/dashboard-widget-count.component";

// Services
import { DashboardWidgetFormService } from "./services/dashboard-widget-form.service";

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        OtModalModule,
        OTPipesModule,
        DashboardAPIModule,
        DashboardWidgetModule,
        OtRadioModule,
        OtTabsModule,
        OtLoadingModule,
        OtButtonModule,
        OtFormModule,
        OtDirectivesModule,
        OtNotificationModule,
        OtAddRemoveModule,
        TranslateModule,
    ],
    declarations: [
        DashboardWidgetCreateModalComponent,
        DashboardWidgetFormComponent,
        DashboardWidgetFilterComponent,
        DashboardWidgetFilterListComponent,
        DashboardWidgetGroupByComponent,
        DashboardWidgetCountComponent,
    ],
    entryComponents: [
        DashboardWidgetCreateModalComponent,
    ],
    providers: [
        PIPES, // TODO: Remove from providers when ot-common is implemented. Only necessary if you are using pipe in TS file, else just module is fine
        DashboardWidgetFormService,
    ],
})
export class DashboardWidgetCreateModule {}
