// 3rd Party
import {
    Injectable,
} from "@angular/core";
import {
    Observable,
    BehaviorSubject,
    Subject,
} from "rxjs";
import {
    filter,
} from "rxjs/operators";

// Interfaces
import { IProtocolResponse } from "interfaces/protocol.interface";
import {
    IDashboard,
    IDashboardState,
    IDashboardWidgetAction,
    IDashboardWidget,
    IDashboardDashboardAction,
} from "reportsModule/dashboard/dashboard-api/interfaces/dashboard.interface";

// Services
import { DashboardAPIService } from "reportsModule/dashboard/dashboard-api/services/dashboard-api.service";

// Enums
import {
    LayoutIds,
    DashboardActions,
} from "reportsModule/dashboard/dashboard-api/enums/dashboard.enum";
import { Directions } from "sharedModules/enums/directions.enum";

@Injectable()
export class DashboardViewService {

    dashboard$: Observable<IDashboard>;
    state$: Observable<IDashboardState>;
    widgetEvent$: Observable<IDashboardWidgetAction>;
    dashboardEvent$: Observable<IDashboardDashboardAction>;

    private defaultDashboard: IDashboard = {
        moduleType: [],
        orgGroupId: "",
        name: "",
        layout: {
            type: LayoutIds.One,
            columns: [],
        },
    };
    private defaultState: IDashboardState = {
        isLoading: true,
    };

    private _dashboard$: BehaviorSubject<IDashboard> = new BehaviorSubject({ ...this.defaultDashboard });
    private _state$: BehaviorSubject<IDashboardState> = new BehaviorSubject({ ...this.defaultState });
    private _widgetEvent$: Subject<IDashboardWidgetAction> = new Subject();
    private _dashboardEvent$: Subject<IDashboardDashboardAction> = new Subject();

    constructor(
        private dashboardAPIService: DashboardAPIService,
    ) {
        this.dashboard$ = this._dashboard$.asObservable();
        this.state$ = this._state$.asObservable();
        this.widgetEvent$ = this._widgetEvent$.asObservable();
        this.dashboardEvent$ = this._dashboardEvent$.asObservable();
    }

    initializeDashboard(dashboardId?: string) {
        this.reset();
        this.dashboardAPIService.getDashboard(dashboardId)
            .pipe(filter((res: IProtocolResponse<IDashboard>) => Boolean(res.result && res.data)))
            .subscribe((response: IProtocolResponse<IDashboard>) => {
                this.refreshDashboard(response.data);
            });
    }

    launchWidgetModal(columnIndex: number) {
        this.dispatchDashboardAction({ type: DashboardActions.LaunchWidgetModal, payload: { columnIndex } });
    }

    launchEditWidgetModal(widget: IDashboardWidget<unknown>) {
        this.dispatchDashboardAction({ type: DashboardActions.LaunchEditWidgetModal, payload: { widget } });
    }

    moveWidget(direction: Directions, columnIndex: number, widgetIndex: number) {
        this.dispatchDashboardAction({ type: DashboardActions.MoveWidget, payload: { direction, columnIndex, widgetIndex } });
    }

    confirmDeleteWidget(widgetId: string) {
        this.dispatchDashboardAction({ type: DashboardActions.DeleteWidget, payload: { widgetId } });
    }

    dispatchWidgetAction(action: IDashboardWidgetAction) {
        this._widgetEvent$.next(action);
    }

    refreshDashboard(dashboard: IDashboard) {
        this._dashboard$.next({
            id: dashboard.id,
            name: dashboard.name,
            moduleType: dashboard.moduleType,
            orgGroupId: dashboard.orgGroupId,
            layout: {
                ...this.dashboard.layout,
                ...(dashboard.layout || { ...this.defaultDashboard.layout }),
            },
        });
        this._state$.next({ ...this.state, isLoading: false });
    }

    private dispatchDashboardAction(action: IDashboardDashboardAction) {
        this._dashboardEvent$.next(action);
    }

    private get dashboard(): IDashboard {
        return this._dashboard$.getValue();
    }

    private get state(): IDashboardState {
        return this._state$.getValue();
    }

    private reset() {
        this._state$.next({...this.defaultState});
        this._dashboard$.next({...this.defaultDashboard});
    }

}
