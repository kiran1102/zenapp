import {
    NgModule,
} from "@angular/core";

// Modules
import { DashboardAPIModule } from "../dashboard-api/dashboard-api.module";
import { DashboardWidgetModule } from "../dashboard-widget/dashboard-widget.module";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";
import {
    OtGridModule,
    OtCardModule,
    OtButtonModule,
    OtLoadingModule,
    OtBreadcrumbModule,
    OtPageHeaderModule,
    OtLookupModule,
    OtDirectivesModule,
} from "@onetrust/vitreus";
import { OTPipesModule, PIPES } from "modules/pipes/pipes.module";
import { TranslateModule } from "@ngx-translate/core";

// Components
import { DashboardViewComponent } from "./components/dashboard-view/dashboard-view.component";
import { DashboardSwitcherComponent } from "./components/dashboard-switcher/dashboard-switcher.component";

// Services
import { DashboardViewService } from "./services/dashboard-view.service";

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        OTPipesModule,
        DashboardAPIModule,
        DashboardWidgetModule,
        OtGridModule,
        OtCardModule,
        OtButtonModule,
        OtLoadingModule,
        OtBreadcrumbModule,
        OtPageHeaderModule,
        OtLookupModule,
        OtDirectivesModule,
        TranslateModule,
    ],
    declarations: [
        DashboardViewComponent,
        DashboardSwitcherComponent,
    ],
    entryComponents: [
        DashboardSwitcherComponent,
    ],
    providers: [
        PIPES,
        DashboardViewService,
    ],
    exports: [
        DashboardViewComponent,
        DashboardSwitcherComponent,
    ],
})
export class DashboardViewModule {}
