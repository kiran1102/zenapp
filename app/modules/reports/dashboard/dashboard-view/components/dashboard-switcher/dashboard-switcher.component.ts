// 3rd party
import {
    Component,
    OnInit,
    OnDestroy,
    Input,
} from "@angular/core";
import {
    takeUntil,
    filter,
    map,
} from "rxjs/operators";
import {
    Subject,
    BehaviorSubject,
} from "rxjs";

// Services
import { DashboardAPIService } from "reportsModule/dashboard/dashboard-api/services/dashboard-api.service";

// Interfaces
import { IProtocolResponse } from "interfaces/protocol.interface";

import {
    IDashboard,
    IDashboardListResponse,
} from "reportsModule/dashboard/dashboard-api/interfaces/dashboard.interface";

// Enums
import { ModuleTypes } from "enums/module-types.enum";

@Component({
    selector: "dashboard-switcher",
    templateUrl: "./dashboard-switcher.component.html",
})
export class DashboardSwitcherComponent implements OnInit, OnDestroy {

    @Input() moduleType: ModuleTypes;

    dashboardSelection$: BehaviorSubject<IDashboard> = new BehaviorSubject(null);
    dashboardOptions: IDashboard[] = [];
    dashboardSearchTerm: string;
    currentDashboardId: string;
    dashboardsLoading = true;
    private destroy$ = new Subject();

    constructor(
        private dashboardAPIService: DashboardAPIService,
    ) {}

    ngOnInit() {
        // TODO : filter list by moduleType
        this.dashboardAPIService.getDashboardList()
            .pipe(
                takeUntil(this.destroy$),
                filter((res: IProtocolResponse<IDashboardListResponse>) => Boolean(res.result && res.data)),
                map((res: IProtocolResponse<IDashboardListResponse>) => res.data.content),
            )
            .subscribe((dashboards: IDashboard[]) => {
                this.dashboardOptions = dashboards;
                this.dashboardSelection$.next(this.dashboardOptions && this.dashboardOptions.length ? this.dashboardOptions[0] : null);
                this.dashboardsLoading = false;
            });
        this.dashboardSelection$
            .pipe(
                takeUntil(this.destroy$),
                filter((selection: IDashboard) => Boolean(selection && selection.id)),
            )
            .subscribe((selection: IDashboard) => this.currentDashboardId = selection.id);
    }

    ngOnDestroy() {
        this.destroy$.next();
        this.destroy$.complete();
    }

    onDashboardChange(selection: IDashboard) {
        this.dashboardSelection$.next(selection);
    }

    dashboardSearch({ key, value }) {
        if (key === "Enter") return;
        this.dashboardSearchTerm = value;
    }

}
