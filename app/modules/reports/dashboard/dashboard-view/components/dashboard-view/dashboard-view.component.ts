// 3rd party
import {
    Component,
    OnInit,
    OnChanges,
    OnDestroy,
    Input,
    SimpleChanges,
} from "@angular/core";
import { takeUntil } from "rxjs/operators";
import {
    Subject,
    combineLatest,
} from "rxjs";

// Services
import { DashboardViewService } from "../../services/dashboard-view.service";

// Enums
import { LayoutIds } from "../../../dashboard-api/enums/dashboard.enum";
import { Directions } from "sharedModules/enums/directions.enum";

// Constants
import { LayoutColumnWidths } from "../../../dashboard-api/constants/dashboard.constant";

// Interfaces
import { IDashboardWidgetAction, IDashboardWidget } from "../../../dashboard-api/interfaces/dashboard.interface";

@Component({
    selector: "dashboard-view",
    templateUrl: "./dashboard-view.component.html",
})
export class DashboardViewComponent implements OnDestroy, OnChanges, OnInit {

    @Input() dashboardId: string;
    @Input() editMode: boolean;

    isLoading: boolean;
    layoutId: LayoutIds;
    widgets: string[][];
    columnWidths = LayoutColumnWidths;
    private destroy$ = new Subject();

    constructor(
        private dashboardViewService: DashboardViewService,
    ) {}

    ngOnInit() {
        combineLatest(this.dashboardViewService.dashboard$, this.dashboardViewService.state$)
            .pipe(takeUntil(this.destroy$))
            .subscribe(([dashboard, state]) => {
                this.widgets = dashboard.layout && dashboard.layout.columns ? dashboard.layout.columns : [];
                this.layoutId = dashboard.layout && dashboard.layout.type ? dashboard.layout.type : LayoutIds.One;
                this.isLoading = state.isLoading;
            });
    }

    ngOnChanges(changes: SimpleChanges) {
        if (changes.dashboardId && changes.dashboardId.currentValue) {
            this.dashboardViewService.initializeDashboard(changes.dashboardId.currentValue);
        }
    }

    ngOnDestroy() {
        this.destroy$.next();
        this.destroy$.complete();
    }

    addWidget(columnIndex: number) {
        this.dashboardViewService.launchWidgetModal(columnIndex);
    }

    editWidget(widget: IDashboardWidget<unknown>) {
        this.dashboardViewService.launchEditWidgetModal(widget);
    }

    deleteWidget(widgetId: string) {
        this.dashboardViewService.confirmDeleteWidget(widgetId);
    }

    handleWidgetAction(action: IDashboardWidgetAction) {
        this.dashboardViewService.dispatchWidgetAction(action);
    }

    moveWidget(direction: Directions, columnIndex: number, widgetIndex: number) {
        this.dashboardViewService.moveWidget(direction, columnIndex, widgetIndex);
    }

    getWidgetId(index: number, widgetId: string) {
        return widgetId;
    }

    trackByIndex(index: number) {
        return index;
    }

}
