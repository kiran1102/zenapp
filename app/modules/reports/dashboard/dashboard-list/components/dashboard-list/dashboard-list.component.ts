// 3rd party
import {
    Component,
    OnInit,
    Inject,
    OnDestroy,
} from "@angular/core";
import {
    take,
    takeUntil,
    filter,
} from "rxjs/operators";
import { Subject } from "rxjs";

// Redux
import { getOrgById } from "oneRedux/reducers/org.reducer";
import { StoreToken } from "tokens/redux-store.token";

// Services
import { OtModalService } from "@onetrust/vitreus";
import { DashboardAPIService } from "reportsModule/dashboard/dashboard-api/services/dashboard-api.service";
import { StateService } from "@uirouter/core";
import { GridViewStorageService } from "sharedModules/services/helper/grid-view-storage.service";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

// Enums
import { ConfirmModalType } from "@onetrust/vitreus";
import { ModuleTranslationKeys } from "enums/module-types.enum";
import { GridViewStorageNames } from "sharedModules/enums/grid-view-storage.enum";

// Constants
import { LayoutNumbers } from "reportsModule/dashboard/dashboard-api/constants/dashboard.constant";

// Interfaces
import {
    IDashboard,
    IDashboardListResponse,
} from "reportsModule/dashboard/dashboard-api/interfaces/dashboard.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";
import { IDropdownOption } from "interfaces/one-dropdown.interface";
import { IStore } from "interfaces/redux.interface";
import { ISortEvent } from "sharedModules/interfaces/sort.interface";

// Components
import { DashboardCreateModalComponent } from "reportsModule/dashboard/dashboard-create/components/dashboard-create-modal/dashboard-create-modal.component";

enum CellTypes { Text, Org, Module, Layout, Link }

interface IDashboardColumn {
    propKey: string;
    labelKey: string;
    type: CellTypes;
    sortable: boolean;
}

@Component({
    selector: "dashboard-list",
    templateUrl: "./dashboard-list.component.html",
})
export class DashboardListComponent implements OnInit, OnDestroy {

    isLoading: boolean;
    dashboards: IDashboard[];
    columns: IDashboardColumn[] = [
        { propKey: "name", labelKey: "Name", type: CellTypes.Link, sortable: true },
        { propKey: "orgGroupId", labelKey: "Organization", type: CellTypes.Org, sortable: true },
        { propKey: "moduleType", labelKey: "VisibleModule", type: CellTypes.Module, sortable: true },
        { propKey: "layout", labelKey: "Layout", type: CellTypes.Layout, sortable: true },
    ];
    pageData: IDashboardListResponse;
    menuPosition: string;
    rowActions: IDropdownOption[] = [
        // TODO : add edit path
        // {
        //     text: this.translatePipe.transform("Edit"),
        //     action: (row: IDashboard) => this.addDashboard(row),
        //     iconClass: "ot-edit",
        // },
        {
            text: this.translatePipe.transform("Delete"),
            action: (row: IDashboard) => this.confirmDelete(row.id),
            iconClass: "ot-trash",
        },
    ];
    currentPage: number;
    defaultSort: ISortEvent = {sortKey: "name", sortOrder: "asc"};
    currentSortKey: string;
    currentSortOrder: string;
    cellTypes = CellTypes;
    storageName = GridViewStorageNames.DASHBOARDS_LIST;
    private destroy$ = new Subject();

    constructor(
        private otModalService: OtModalService,
        private translatePipe: TranslatePipe,
        private dashboardAPIService: DashboardAPIService,
        private browserStorageListService: GridViewStorageService,
        private stateService: StateService,
        @Inject(StoreToken) private store: IStore,
    ) {}

    ngOnInit() {
        const savedSort = this.browserStorageListService.getBrowserStorageList(this.storageName);
        this.currentSortKey = savedSort.sort ? savedSort.sort.sortKey : "name";
        this.currentSortOrder = savedSort.sort ? savedSort.sort.sortOrder : "desc";
        this.getDashboards();
    }

    ngOnDestroy() {
        this.destroy$.next();
        this.destroy$.complete();
    }

    getDashboards() {
        this.isLoading = true;
        this.dashboardAPIService.getDashboardList(this.currentPage, this.currentSortKey, this.currentSortOrder)
            .pipe(take(1))
            .subscribe((response: IProtocolResponse<IDashboardListResponse>) => {
                if (!(response.result && response.data)) return;
                this.dashboards = response.data.content;
                this.pageData = response.data;
                this.isLoading = false;
            });
    }

    createDashboard() {
        this.otModalService
            .create(
                DashboardCreateModalComponent,
                { isComponent: true },
            )
            .pipe(
                takeUntil(this.destroy$),
                filter(((response: string) => Boolean(response))),
            )
            .subscribe((dashboardId: string) => {
                this.stateService.go("zen.app.pia.module.reports-new.dashboard-edit", { id: dashboardId });
            });
    }

    getCellLabel = (value: any, type: CellTypes): string => {
        switch (type) {
            case CellTypes.Org:
                return this.getOrgName(value);
            case CellTypes.Module:
                return this.translatePipe.transform(ModuleTranslationKeys[value]);
            case CellTypes.Layout:
                return value && value.type ? this.translatePipe.transform("TemplateNumber", { Number: LayoutNumbers[value.type]}) : "";
            default:
                return value || "";
        }
    }

    setMenuClass(event?: MouseEvent) {
        this.menuPosition = "bottom-right";
        if ((window.innerHeight - 200) / 2 < (event.clientY - 200)) {
            this.menuPosition = "top-right";
        }
    }

    handleSortChange(sortEvent: ISortEvent = this.defaultSort) {
        this.browserStorageListService.saveSessionStorageSort(this.storageName, sortEvent);
        this.currentSortOrder = sortEvent.sortOrder;
        this.currentSortKey = sortEvent.sortKey;
        this.currentPage = 0;
        this.getDashboards();
    }

    paginate(page: number) {
        this.currentPage = page;
        this.getDashboards();
    }

    goToDashboard(id: string) {
        this.stateService.go("zen.app.pia.module.reports-new.dashboard-edit", { id });
    }

    private confirmDelete(dashboardId: string) {
        this.otModalService
            .confirm({
                type: ConfirmModalType.WARNING,
                translations: {
                    title: this.translatePipe.transform("DeleteDashboard"),
                    desc: this.translatePipe.transform("AreYouSureDeleteDashboard"),
                    confirm: "",
                    submit: this.translatePipe.transform("Confirm"),
                    cancel: this.translatePipe.transform("Cancel"),
                },
            })
            .pipe(
                takeUntil(this.destroy$),
                take(1),
                filter(((response: boolean) => response)),
            )
            .subscribe(() => this.deleteDashboard(dashboardId));
    }

    private deleteDashboard(dashboardId: string) {
        this.isLoading = true;
        this.currentPage = (this.pageData.content.length > 1 && !this.pageData.last) || this.pageData.first ? this.pageData.number : this.pageData.number - 1;
        this.dashboardAPIService.deleteDashboard(dashboardId)
            .pipe(take(1))
            .subscribe((res: IProtocolResponse<IDashboard>) => {
                if (!(res.result)) {
                    this.isLoading = false;
                    return;
                }
                this.getDashboards();
            });
    }

    private getOrgName(orgId: string): string {
        const org = getOrgById(orgId)(this.store.getState());
        return org ? org.Name : "";
    }

}
