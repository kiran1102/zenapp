import {
    NgModule,
} from "@angular/core";

// Modules
import { DashboardAPIModule } from "../dashboard-api/dashboard-api.module";
import { DashboardCreateModule } from "../dashboard-create/dashboard-create.module";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";
import { VitreusModule } from "@onetrust/vitreus";
import { OTPipesModule, PIPES } from "modules/pipes/pipes.module";

// Components
import { DashboardListComponent } from "./components/dashboard-list/dashboard-list.component";

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        VitreusModule,
        OTPipesModule,
        DashboardAPIModule,
        DashboardCreateModule,
    ],
    declarations: [
        DashboardListComponent,
    ],
    entryComponents: [
        DashboardListComponent,
    ],
    providers: [
        PIPES, // TODO: Remove from providers when ot-common is implemented. Only necessary if you are using pipe in TS file, else just module is fine
    ],
})
export class DashboardListModule {}
