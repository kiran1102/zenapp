import {
    NgModule,
} from "@angular/core";

// Modules
import { DashboardAPIModule } from "../dashboard-api/dashboard-api.module";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";
import { VitreusModule } from "@onetrust/vitreus";
import { OTPipesModule, PIPES } from "modules/pipes/pipes.module";
// TODO : pull org-select component out of shared and import separately
import { SharedModule } from "sharedModules/shared.module";

// Components
import { DashboardCreateModalComponent } from "./components/dashboard-create-modal/dashboard-create-modal.component";
import { DashboardLayoutOptionComponent } from "./components/dashboard-layout-option/dashboard-layout-option.component";

@NgModule({
    imports: [
        SharedModule,
        CommonModule,
        FormsModule,
        VitreusModule,
        OTPipesModule,
        DashboardAPIModule,
    ],
    declarations: [
        DashboardCreateModalComponent,
        DashboardLayoutOptionComponent,
    ],
    entryComponents: [
        DashboardCreateModalComponent,
    ],
    providers: [
        PIPES, // TODO: Remove from providers when ot-common is implemented. Only necessary if you are using pipe in TS file, else just module is fine
    ],
})
export class DashboardCreateModule {}
