import {
    Component,
    OnInit,
    OnDestroy,
} from "@angular/core";

// RXJS
import { Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";
import { filter, tap } from "rxjs/operators";

// Interfaces
import { IOtModalContent } from "@onetrust/vitreus";
import { IDashboard } from "reportsModule/dashboard/dashboard-api/interfaces/dashboard.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

// Contants
import { VisibleModuleOptions } from "reportsModule/dashboard/dashboard-api/constants/dashboard.constant";
import {
    LayoutOptions,
    LayoutColumnDefaults,
} from "reportsModule/dashboard/dashboard-api/constants/dashboard.constant";

// Services
import { DashboardAPIService } from "reportsModule/dashboard/dashboard-api/services/dashboard-api.service";

interface IFormOption {
    label: string;
    labelKey?: string;
    value: number;
    isSelected?: boolean;
    imagePath?: string;
}

interface ILabelExtension {
    label: string;
}

@Component({
    selector: "dashboard-create-modal",
    templateUrl: "./dashboard-create-modal.component.html",
})
export class DashboardCreateModalComponent implements IOtModalContent, OnInit, OnDestroy {

    modalTitleKey = "CreateDashboard";

    isLoading = false;
    formIsValid = false;
    editMode = false;
    moduleOptions: IFormOption[];
    moduleSelection: IFormOption;
    templateOptions: IFormOption[];
    templateSelection: number;
    orgSelection: string;
    dashboardName: string;
    dashboardId: string;

    otModalCloseEvent: Subject<string>;
    otModalData: IDashboard;
    private destroy$ = new Subject();

    constructor(
        private translatePipe: TranslatePipe,
        private dashboardAPIService: DashboardAPIService,
    ) { }

    ngOnInit() {
        this.moduleOptions = this.translateLabels(VisibleModuleOptions, "labelKey");
        this.templateOptions = LayoutOptions;
        if (this.otModalData) this.initModalEdit();
    }

    ngOnDestroy() {
        this.destroy$.next();
        this.destroy$.complete();
    }

    initModalEdit() {
        this.editMode = true;
        this.modalTitleKey = "EditDashboard";
        this.dashboardId = this.otModalData.id;
        this.dashboardName = this.otModalData.name;
        this.moduleSelection = this.findModuleOption(this.otModalData.moduleType[0]);
        this.orgSelection = this.otModalData.orgGroupId;
        this.templateSelection = this.findTemplateOption(this.otModalData.layout.type).value;
    }

    onOrgChange(org: string) {
        this.orgSelection = org;
        this.formIsValid = this.validateForm();
    }

    onModuleChange(selectedModule: IFormOption) {
        this.moduleSelection = selectedModule;
        this.formIsValid = this.validateForm();
    }

    onNameChange(name: string) {
        this.dashboardName = name ? name.trim() : "";
        this.formIsValid = this.validateForm();
    }

    onTemplateSelect(templateId: number) {
        this.templateSelection = templateId;
        this.formIsValid = this.validateForm();
    }

    saveDashboard() {
        this.isLoading = true;
        const dashboard: IDashboard = {
            name: this.dashboardName,
            moduleType: [this.moduleSelection.value],
            orgGroupId: this.orgSelection,
            layout: {
                type: this.templateSelection,
                columns: this.editMode ? this.reconfigureColumns(this.otModalData.layout.columns, this.templateSelection) : LayoutColumnDefaults[this.templateSelection],
            },
        };
        const save = () => this.editMode ? this.dashboardAPIService.editDashboard(this.dashboardId, dashboard) : this.dashboardAPIService.addDashboard(dashboard);
        save()
            .pipe(
                takeUntil(this.destroy$),
                filter((response: IProtocolResponse<IDashboard>) => Boolean(response.result && response.data)),
                tap(() => this.isLoading = false),
            )
            .subscribe((response: IProtocolResponse<IDashboard>) => {
                this.otModalCloseEvent.next(response.data.id);
                this.otModalCloseEvent.complete();
            });
    }

    closeModal() {
        this.otModalCloseEvent.next();
        this.otModalCloseEvent.complete();
    }

    private findModuleOption(moduleId: number): IFormOption {
        return this.moduleOptions.find((option: IFormOption) => {
            return option.value === moduleId;
        });
    }

    private findTemplateOption(layoutType: number): IFormOption {
        return this.templateOptions.find((option: IFormOption) => {
            return option.value === layoutType;
        });
    }

    private reconfigureColumns(columns: string[][], newLayoutId: number): string[][] {
        const newNumColumns = LayoutColumnDefaults[newLayoutId].length;
        const columnDifference =  columns.length - newNumColumns;

        if (columnDifference === 0) return columns;
        if (columnDifference > 0) {
            // shrink and merge
            const colsToMerge = columns.splice(newNumColumns, columnDifference);
            colsToMerge.forEach((col) => {
                const tempCol = columns[newNumColumns - 1].concat(col);
                columns.splice(newNumColumns - 1, 1, tempCol);
            });
        } else {
            // add diff number of columns
            const numArrsToAdd = Math.abs(columnDifference);
            for (let i = 0; i < numArrsToAdd; i++) {
                columns.push([]);
            }
        }
        return columns;
    }

    private validateForm(): boolean {
        return Boolean(this.dashboardName
            && this.moduleSelection && this.moduleSelection.value
            && this.orgSelection
            && this.templateSelection);
    }

    private translateLabels<T extends object>(options: T[], labelKey: string): Array<T & ILabelExtension> {
        return options.map((option: T): T & ILabelExtension => {
            return { ...(option as object), label: this.translatePipe.transform(option[labelKey]) } as T & ILabelExtension;
        });
    }
}
