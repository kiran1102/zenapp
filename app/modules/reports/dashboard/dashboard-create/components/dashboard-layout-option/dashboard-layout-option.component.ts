import {
    Component,
    Input,
} from "@angular/core";

// Enums
import { LayoutIds } from "reportsModule/dashboard/dashboard-api/enums/dashboard.enum";

@Component({
    selector: "dashboard-layout-option",
    templateUrl: "./dashboard-layout-option.component.html",
})
export class DashboardLayoutOptionComponent {

    @Input() layoutType = LayoutIds.One;
    @Input() isActive = false;

    layoutIds = LayoutIds;
}
