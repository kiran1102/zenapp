import {
    NgModule,
} from "@angular/core";

// Services
import { DashboardAPIService } from "./services/dashboard-api.service";
import { DashboardAdapterService } from "./services/dashboard-adapter.service";
import { DashboardLogicService } from "./services/dashboard-logic.service";

@NgModule({
    providers: [
        DashboardAPIService,
        DashboardAdapterService,
        DashboardLogicService,
    ],
})
export class DashboardAPIModule {}
