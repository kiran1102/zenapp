export enum WidgetTypes {
    Donut = 10,
    Bar = 20,
    Map = 30,
    Pie = 40,
    Table = 60,
}

export enum WidgetTranslationKeys {
    Donut = 10,
    BarChart = 20,
    Map = 30,
    PieChart = 40,
    Table = 60,
}

export enum LayoutIds {
    One = 10,
    Two = 20,
    Three = 30,
    Four = 40,
    Five = 50,
}

export enum EntityTypes {
    Assessment = 10,
    Inventory = 20,
    Risk = 30,
    Consent = 40,
    Incident = 50,
}

export enum WidgetCreateForms {
    Filters,
    Count,
    GroupBy,
}

export enum DashboardActions {
    LaunchWidgetModal = "LaunchWidgetModal",
    LaunchEditWidgetModal = "LaunchEditWidgetModal",
    DeleteWidget = "DeleteWidget",
    MoveWidget = "MoveWidget",
}
