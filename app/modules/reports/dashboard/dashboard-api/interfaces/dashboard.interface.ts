// Enums
import {
    ReportModuleTypes,
    ReportEntityTypes,
} from "reportsModule/reports-shared/enums/reports.enum";
import {
    LayoutIds,
    WidgetTypes,
    EntityTypes,
    WidgetCreateForms,
    DashboardActions,
} from "reportsModule/dashboard/dashboard-api/enums/dashboard.enum";

// Interfaces
import { IPageableOf } from "interfaces/pagination.interface";
import {
    IFilterTree,
    IFilter,
    IFilterColumnOption,
} from "reportsModule/reports-shared/interfaces/report.interface";
import { IChartInputData } from "@onetrust/vitreus";

export interface IDashboardListResponse extends IPageableOf<IDashboard> {}

export interface IDashboardLayout {
    type: LayoutIds;
    columns: string[][];
}

export interface IDashboardMetaData {
    id?: string;
    name: string;
    moduleType: ReportModuleTypes[];
    orgGroupId: string;
}

export interface IDashboard extends IDashboardMetaData {
    layout: IDashboardLayout;
}

export interface IDashboardState {
    isLoading: boolean;
}

export interface IDashboardWidgetModalData {
    moduleType: ReportModuleTypes;
    widget?: IDashboardWidget<unknown>;
}

export interface IDashboardWidgetRequest {
    name: string;
    type: WidgetTypes;
    primaryReportableType: IDashboardWidgetReportableType;
    groupBy: IDashboardWidgetGroupByType;
    filter?: IWidgetFilters;
    action?: string;
}

export interface IDashboardWidgetReportableType {
    entityType: EntityTypes;
    reportSubType: string;
}

export interface IDashboardWidgetGroupByType {
    reportableType: IDashboardWidgetReportableType;
    attributeKey: string;
    attributeEntityType: ReportEntityTypes;
}

export interface IDashboardWidget<T> extends IDashboardWidgetRequest {
    id?: string;
    data: T;
}

export interface IBarChartData {
    id: string;
    labelKey: string;
    value: string;
}

export interface ITableWidgetCell {
    value: string;
    valueKey: string;
    hyperLink: string;
    preformatted: boolean;
}

export interface ITableWidgetRow {
    id: number;
    cells: ITableWidgetCell[];
}

export interface ITableWidgetColumn {
    columnName: string;
    columnNameKey: string;
    dataType: number;
}

export interface ITableWidgetData {
    id: string;
    columns: ITableWidgetColumn[];
    content: ITableWidgetRow[];
}

export interface IBarChartWidgetResponse extends IDashboardWidget<IBarChartData[]> { }

export interface ITableWidgetResponse extends IDashboardWidget<ITableWidgetData> { }

export interface IBarChartWidget extends IDashboardWidget<IChartInputData> { }

export interface IDonutChartWidget extends IDashboardWidget<IChartInputData> { }

export interface ITableWidget extends IDashboardWidget<ITableWidgetData> { }

export interface IDashboardWidgetAction {
    actionId: string;
    widgetId: string;
    targetId: string;
    targetValue: number;
}

export interface IDashboardDashboardAction {
    type: DashboardActions;
    payload: { [key: string]: unknown };
}

export interface IWidgetEntity {
    id: string | number;
    name: string;
    nameKey: string;
    entitySubTypeOptions?: IWidgetSubEntity[];
    entitySubType?: IWidgetSubEntity;
    entitySubTypeLink?: {
        url: string;
    };
}

export interface IWidgetSubEntity {
    id: string | number;
    nameKey: string;
}

export interface IWidgetFilters {
    entityType: EntityTypes;
    criteria: IFilterTree;
}

export interface IWidgetFormData {
    countEntity: IWidgetEntity;
    countSubEntity: IWidgetSubEntity;
    groupByEntity: IWidgetEntity;
    groupBySubEntity: IWidgetSubEntity;
    groupByAttribute: IFilterColumnOption;
    filters: IFilter[];
}

export interface IWidgetFormValidity {
    allValid: boolean;
    [WidgetCreateForms.Count]: boolean;
    [WidgetCreateForms.GroupBy]: boolean;
    [WidgetCreateForms.Filters]: boolean;
}
