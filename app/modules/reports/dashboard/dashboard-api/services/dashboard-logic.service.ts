// 3rd Party
import { Injectable } from "@angular/core";

// Interfaces
import { IFilterColumnOption } from "reportsModule/reports-shared/interfaces/report.interface";

// Enums
import { FilterColumnTypes } from "enums/filter-column-types.enum";

@Injectable()
export class DashboardLogicService {

    filterAllowedAttributes(attributes: IFilterColumnOption[]): IFilterColumnOption[] {
        return attributes.filter((attribute: IFilterColumnOption): boolean => {
            // TODO : add support for user
            return (attribute.type === FilterColumnTypes.Multi || attribute.type === FilterColumnTypes.MultiInt)
                && !(attribute.groupKey && attribute.groupKey.length);
        });
    }
}
