// 3rd Party
import { Injectable } from "@angular/core";

// Interfaces
import {
    IDashboardWidget,
    IBarChartWidget,
    IBarChartWidgetResponse,
    IBarChartData,
    ITableWidget,
    ITableWidgetResponse,
} from "../interfaces/dashboard.interface";
import { IChartDataElement } from "@onetrust/vitreus";

// Services
import { TranslateService } from "@ngx-translate/core";

// Enums
import { WidgetTypes } from "reportsModule/dashboard/dashboard-api/enums/dashboard.enum";

// Constants
import {
    DefaultTableWidget,
    GraphColors,
} from "reportsModule/dashboard/dashboard-api/constants/dashboard.constant";

@Injectable()
export class DashboardAdapterService {

    constructor(
        private translateService: TranslateService,
    ) {}

    formatWidgetData(widget: IDashboardWidget<unknown>): IDashboardWidget<unknown> {
        switch (widget.type) {
            case WidgetTypes.Bar:
            case WidgetTypes.Donut: {
                return this.formatBarData(widget as IBarChartWidgetResponse);
            }
            case WidgetTypes.Table: {
                return this.formatTableData(widget as ITableWidgetResponse);
            }
            default: {
                return widget;
            }
        }
    }

    private formatBarData(widget: IBarChartWidgetResponse): IBarChartWidget {
        return {
            ...widget,
            data: {
                id: widget.id,
                values: widget.data.map((item: IBarChartData, index: number): IChartDataElement => {
                    return {
                        id: item.id,
                        label: this.translateService.instant(item.labelKey),
                        value: parseInt(item.value, 10),
                        backgroundColor: GraphColors[index % (GraphColors.length)],
                    };
                }),
            },
        };
    }

    // TODO: Remove mock data when BE has support
    private formatTableData(widget: ITableWidgetResponse): ITableWidget {
        return {
            ...widget,
            data: {
                id: widget.id,
                ...DefaultTableWidget.data,
            },
        };
    }
}
