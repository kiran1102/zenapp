// 3rd Party
import { Injectable } from "@angular/core";
import {
    from,
    Observable,
} from "rxjs";

// Interfaces
import {
    IProtocolMessages,
    IProtocolConfig,
    IProtocolResponse,
} from "interfaces/protocol.interface";

import {
    IDashboard,
    IDashboardListResponse,
    IDashboardWidgetRequest,
    IDashboardWidget,
    IWidgetEntity,
    IWidgetSubEntity,
} from "../interfaces/dashboard.interface";

// Services
import { ProtocolService } from "modules/core/services/protocol.service";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

// Enums
import { EntityTypes } from "reportsModule/dashboard/dashboard-api/enums/dashboard.enum";
import {
    ReportModuleTypes,
} from "reportsModule/reports-shared/enums/reports.enum";

@Injectable()
export class DashboardAPIService {

    private readonly defaultPageSize = 20;

    constructor(
        private translatePipe: TranslatePipe,
        private  OneProtocol: ProtocolService,
    ) { }

    addDashboard(dashboard: IDashboard): Observable<IProtocolResponse<IDashboard>> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("POST", "/reporting/dashboard", null, dashboard);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("ErrorAddingDashboard") } };
        return from(this.OneProtocol.http(config, messages));
    }

    editDashboard(dashboardId: string, dashboard: IDashboard): Observable<IProtocolResponse<IDashboard>> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("PUT", `/reporting/dashboard/${dashboardId}`, null, dashboard);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("ErrorUdatingDashboard") } };
        return from(this.OneProtocol.http(config, messages));
    }

    getDashboardList(page: number = 0, sortKey: string = "name", sortOrder: string = "desc"): Observable<IProtocolResponse<IDashboardListResponse>> {
        const pageParams: { page: number, size: number, sort: string } = { page, size: this.defaultPageSize, sort: `${sortKey},${sortOrder}`};
        const config: IProtocolConfig = this.OneProtocol.customConfig("GET", "/reporting/dashboard/list", pageParams);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("ErrorRetrievingDashboard") } };
        return from(this.OneProtocol.http(config, messages));
    }

    getDashboard(dashboardId: string): Observable<IProtocolResponse<IDashboard>> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("GET", `/reporting/dashboard/${dashboardId}`);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("ErrorRetrievingDashboard") } };
        return from(this.OneProtocol.http(config, messages));
    }

    deleteDashboard(id: string): Observable<IProtocolResponse<IDashboard>> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("DELETE", `/reporting/dashboard/${id}`);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("ErrorDeletingDashboard") } };
        return from(this.OneProtocol.http(config, messages));
    }

    createWidget(widget: IDashboardWidgetRequest): Observable<IProtocolResponse<IDashboardWidget<unknown>>> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("POST", "/reporting/dashboard/widget", null, widget);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("ErrorCreatingWidget") } };
        return from(this.OneProtocol.http(config, messages));
    }

    editWidget(widgetId: string, widget: IDashboardWidgetRequest): Observable<IProtocolResponse<IDashboardWidget<unknown>>> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("PUT", `/reporting/dashboard/widget/${widgetId}`, null, widget);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("ErrorEditingWidget") } };
        return from(this.OneProtocol.http(config, messages));
    }

    getWidget(widgetId: string): Observable<IProtocolResponse<IDashboardWidget<unknown>>> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("GET", `/reporting/dashboard/widget/${widgetId}`);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("ErrorRetrievingWidget") } };
        return from(this.OneProtocol.http(config, messages));
    }

    deleteWidget(widgetId: string): Observable<IProtocolResponse<boolean>> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("DELETE", `/reporting/dashboard/widget/${widgetId}`);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("ErrorDeletingWidget") } };
        return from(this.OneProtocol.http(config, messages));
    }

    getCountEntityOptions(moduleId: ReportModuleTypes): Observable<IProtocolResponse<IWidgetEntity[]>> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("GET", `/reporting/dashboard/widget/entities/${moduleId}`);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("ErrorRetrievingEntities") } };
        return from(this.OneProtocol.http(config, messages));
    }

    getGroupByEntityOptions(entity: EntityTypes, subEntity: string): Observable<IProtocolResponse<IWidgetEntity[]>> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("GET", `/reporting/dashboard/widget/secondary/${entity}/${subEntity}`);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("ErrorRetrievingGroupByOptions") } };
        return from(this.OneProtocol.http(config, messages));
    }

    getSubEntityOptions(url: string): Observable<IProtocolResponse<IWidgetSubEntity[]>> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("GET", url);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("ErrorRetrievingSubEntityOptions") } };
        return from(this.OneProtocol.http(config, messages));
    }
}
