import {
    LayoutIds,
    WidgetTypes,
    EntityTypes,
} from "../enums/dashboard.enum";
import { ReportModuleTypes } from "../../../reports-shared/enums/reports.enum";
import { ModuleTypes } from "enums/module-types.enum";

export const VisibleModuleOptions = [
    { labelKey: ReportModuleTypes[ReportModuleTypes.Vendor], value: ReportModuleTypes.Vendor },
    { labelKey: ReportModuleTypes[ReportModuleTypes.Inventory], value: ReportModuleTypes.Inventory },
    { labelKey: ReportModuleTypes[ReportModuleTypes.Assessment], value: ReportModuleTypes.Assessment },
];

export const LayoutNumbers = {
    [LayoutIds.One]: 1,
    [LayoutIds.Two]: 2,
    [LayoutIds.Three]: 3,
    [LayoutIds.Four]: 4,
    [LayoutIds.Five]: 5,
};

export const LayoutColumnWidths = {
    [LayoutIds.One]: [3, 3, 3, 3],
    [LayoutIds.Two]: [3, 3, 6],
    [LayoutIds.Three]: [6, 3, 3],
    [LayoutIds.Four]: [6, 6],
    [LayoutIds.Five]: [12],
};

export const LayoutOptions = [
    { label: "1", value: LayoutIds.One, imagePath: "images/layout1.svg" },
    { label: "2", value: LayoutIds.Two, imagePath: "images/layout2.svg" },
    { label: "3", value: LayoutIds.Three, imagePath: "images/layout3.svg" },
    { label: "4", value: LayoutIds.Four, imagePath: "images/layout4.svg" },
    { label: "5", value: LayoutIds.Five, imagePath: "images/layout5.svg" },
];

export const DefaultLayout = {
    type: LayoutIds.One,
    columns: [],
};

export const LayoutColumnDefaults = {
    [LayoutIds.One]: [[], [], [], []],
    [LayoutIds.Two]: [[], [], []],
    [LayoutIds.Three]: [[], [], []],
    [LayoutIds.Four]: [[], []],
    [LayoutIds.Five]: [[]],
};

export const WidgetOptions = {
    [ReportModuleTypes.Inventory]: [
        WidgetTypes.Bar,
        WidgetTypes.Donut,
        WidgetTypes.Table,
    ],
    [ReportModuleTypes.Assessment]: [
        WidgetTypes.Bar,
        WidgetTypes.Donut,
        WidgetTypes.Table,
    ],
    [ReportModuleTypes.Risk]: [
        WidgetTypes.Bar,
        WidgetTypes.Donut,
        WidgetTypes.Table,
    ],
    [ReportModuleTypes.Vendor]: [
        WidgetTypes.Bar,
        WidgetTypes.Donut,
        WidgetTypes.Table,
    ],
};

export const DefaultBarChart = {
    // TODO : use better mock data
    type: WidgetTypes.Bar,
    data: {
        id: "123",
        values: [
            {
                id: "123",
                label: "Label 1",
                value: "10",
                backgroundColor: "#A5B1C2",
            },
            {
                id: "124",
                label: "Label 2",
                value: "6",
                backgroundColor: "#EA3B5A",
            },
            {
                id: "126",
                label: "Label 3",
                value: "12",
                backgroundColor: "#FA8231",
            },
            {
                id: "128",
                label: "Much Longer Label",
                value: "18",
                backgroundColor: "#6CC04A",
            },
        ],
    },
};

export const DefaultDonutChart = {
    // TODO : use better mock data
    type: WidgetTypes.Donut,
    data: {
        id: "123",
        values: [
            {
                id: "123",
                label: "Label 1",
                value: "10",
                backgroundColor: "#A5B1C2",
            },
            {
                id: "124",
                label: "Label 2",
                value: "6",
                backgroundColor: "#EA3B5A",
            },
            {
                id: "126",
                label: "Label 3",
                value: "12",
                backgroundColor: "#FA8231",
            },
            {
                id: "128",
                label: "Much Longer Label",
                value: "18",
                backgroundColor: "#6CC04A",
            },
        ],
    },
};

export const EntityMetaDataTypes = {
    [EntityTypes.Inventory]: ModuleTypes.INV,
    [EntityTypes.Assessment]: ModuleTypes.PIA,
    [EntityTypes.Consent]: ModuleTypes.CM,
    [EntityTypes.Incident]: ModuleTypes.INC,
    [EntityTypes.Risk]: ModuleTypes.RISK,
};

export const DefaultTableWidget = {
    type: WidgetTypes.Table,
    data: {
        columns: [
            {
                columnName: "Name",
                columnNameKey: "AssetsName",
                dataType: 10,
            },
            {
                columnName: "Managing Organization",
                columnNameKey: "AssetsOrganization",
                dataType: 30,
            },
            {
                columnName: "Hosting Location",
                columnNameKey: "AssetsLocation",
                dataType: 30,
            },
            {
                columnName: "Data Disposal",
                columnNameKey: "AssetsDataDisposal",
                dataType: 30,
            },
            {
                columnName: "Created Date",
                columnNameKey: "InventoryCreatedDate",
                dataType: 42,
            },
            {
                columnName: "Last Updated Date",
                columnNameKey: "InventoryLastUpdatedDate",
                dataType: 42,
            },
        ],
        content: [
            {
            id: 1,
            cells: [
                {
                    value: "Samsung S8",
                    valueKey: null,
                    hyperLink: null,
                    preformatted: false,
                },
                {
                    value: "Data Centers",
                    valueKey: null,
                    hyperLink: null,
                    preformatted: false,
                },
                {
                    value: "United States",
                    valueKey: "UnitedStates",
                    hyperLink: null,
                    preformatted: false,
                },
                {
                    value: "Archive Server",
                    valueKey: "AssetsDataDisposalArchiveServer",
                    hyperLink: null,
                    preformatted: false,
                },
                {
                    value: "2019-01-24T18:25:11Z",
                    valueKey: null,
                    hyperLink: null,
                    preformatted: false,
                },
                {
                    value: "2019-01-24T18:34:44Z",
                    valueKey: null,
                    hyperLink: null,
                    preformatted: false,
                },
            ],
        }],
    },
};

export const GraphColors = [
    "#FD5A65", // Red
    "#2B98DA", // Blue
    "#0EBAB1", // Teal
    "#F7B732", // Yellow
];
