// 3rd Party
import { Injectable } from "@angular/core";

// RXJS
import {
    filter,
    map,
} from "rxjs/operators";
import { Observable } from "rxjs";

// Interfaces
import {
    IDashboardWidget,
    IDashboard,
} from "reportsModule/dashboard/dashboard-api/interfaces/dashboard.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";

// Services
import { TranslateService } from "@ngx-translate/core";
import {
    OtModalService,
    ConfirmModalType,
    ModalSize,
} from "@onetrust/vitreus";
import { DashboardAPIService } from "reportsModule/dashboard/dashboard-api/services/dashboard-api.service";

// Components
import { DashboardWidgetCreateModalComponent } from "reportsModule/dashboard/dashboard-widget-create/components/dashboard-widget-create-modal/dashboard-widget-create-modal.component";
import { DashboardCreateModalComponent } from "reportsModule/dashboard/dashboard-create/components/dashboard-create-modal/dashboard-create-modal.component";

// Enums
import { ReportModuleTypes } from "reportsModule/reports-shared/enums/reports.enum";
import { Directions } from "sharedModules/enums/directions.enum";

@Injectable()
export class DashboardEditService {

    private translations: { [key: string]: string } = {};

    constructor(
        private translateService: TranslateService,
        private otModalService: OtModalService,
        private dashboardAPIService: DashboardAPIService,
    ) {
        this.translateService
            .stream([
                "DeleteWidget",
                "AreYouSureDeleteWidget",
                "Confirm",
                "Cancel",
            ])
            .subscribe((values) => (this.translations = values));
    }

    launchWidgetModal(moduleType: ReportModuleTypes, columnIndex: number, dashboard: IDashboard): Observable<IDashboard> {
        return this.otModalService
            .create(
                DashboardWidgetCreateModalComponent,
                { isComponent: true, size: ModalSize.MEDIUM },
                { moduleType },
            )
            .pipe(
                filter((response: string) => Boolean(response)),
                map((widgetId: string) => this.addWidget(widgetId, columnIndex, dashboard)),
            );
    }

    changeLayout(dashboard: IDashboard): Observable<string> {
        return this.otModalService
            .create(
                DashboardCreateModalComponent,
                { isComponent: true },
                dashboard,
            )
            .pipe(filter((response: string) => Boolean(response)));
    }

    launchEditWidgetModal(widget: IDashboardWidget<unknown>, moduleType: ReportModuleTypes): Observable<string> {
        return this.otModalService
            .create(
                DashboardWidgetCreateModalComponent,
                { isComponent: true, size: ModalSize.MEDIUM },
                { moduleType, widget },
            )
            .pipe(filter(((response: string) => Boolean(response))));
    }

    confirmDeleteWidget(widgetId: string, dashboard: IDashboard): Observable<IDashboard> {
        return this.otModalService
            .confirm({
                type: ConfirmModalType.WARNING,
                translations: {
                    title: this.translations.DeleteWidget,
                    desc: this.translations.AreYouSureDeleteWidget,
                    confirm: "",
                    submit: this.translations.Confirm,
                    cancel: this.translations.Cancel,
                },
            })
            .pipe(
                filter(((response: boolean) => response)),
                map(() => this.deleteWidget(widgetId, dashboard)),
            );
    }

    moveWidget(direction: Directions, columnIndex: number, widgetIndex: number, dashboard: IDashboard): IDashboard {
        if (direction === Directions.Up || direction === Directions.Down) {
            dashboard.layout.columns[columnIndex] = this.swapWidgetsInColumn(
                dashboard.layout.columns[columnIndex],
                widgetIndex,
                direction === Directions.Up ? widgetIndex - 1 : widgetIndex + 1,
            );
        }
        if (direction === Directions.Left || direction === Directions.Right) {
            dashboard.layout.columns = this.moveWidgetToNewColumn(
                dashboard.layout.columns,
                columnIndex,
                direction === Directions.Left ? columnIndex - 1 : columnIndex + 1,
                widgetIndex,
            );
        }
        return dashboard;
    }

    saveCurrentView(dashboard: IDashboard): Observable<IDashboard> {
        return this.dashboardAPIService.editDashboard(dashboard.id, dashboard)
            .pipe(
                filter((res: IProtocolResponse<IDashboard>) => Boolean(res.result && res.data)),
                map((res: IProtocolResponse<IDashboard>): IDashboard => res.data),
            );
    }

    private addWidget(widgetId: string, columnIndex: number, dashboard: IDashboard): IDashboard {
        if (!(dashboard.layout.columns && dashboard.layout.columns[columnIndex])) return dashboard;
        dashboard.layout.columns[columnIndex].push(widgetId);
        return dashboard;
    }

    private deleteWidget(widgetId: string, dashboard: IDashboard): IDashboard {
        let columnIndex;
        let widgetIndex;
        const columns: string[][] = dashboard.layout.columns;
        for (let cIndex = 0; cIndex < columns.length; cIndex++) {
            const wIndex: number = columns[cIndex].findIndex((value: string) => value === widgetId);
            if (wIndex !== -1) {
                columnIndex = cIndex;
                widgetIndex = wIndex;
                break;
            }
        }
        dashboard.layout.columns[columnIndex].splice(widgetIndex, 1);
        return dashboard;
    }

    private swapWidgetsInColumn(widgets: string[], prevIndex: number, newIndex: number): string[] {
        if (newIndex === -1 || newIndex === widgets.length) return widgets;
        const temp: string = widgets[newIndex];
        widgets[newIndex] = widgets[prevIndex];
        widgets[prevIndex] = temp;
        return widgets;
    }

    private moveWidgetToNewColumn(columns: string[][], prevColumnIndex: number, newColumnIndex: number, widgetIndex: number): string[][] {
        if (newColumnIndex === -1 || newColumnIndex === columns.length) return columns;
        const widget: string = columns[prevColumnIndex][widgetIndex];
        columns[prevColumnIndex].splice(widgetIndex, 1);
        columns[newColumnIndex].push(widget);
        return columns;
    }

}
