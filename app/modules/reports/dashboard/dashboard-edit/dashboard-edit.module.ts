import {
    NgModule,
} from "@angular/core";

// Modules
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";
import { VitreusModule } from "@onetrust/vitreus";
import { OTPipesModule, PIPES } from "modules/pipes/pipes.module";
import { DashboardAPIModule } from "../dashboard-api/dashboard-api.module";
import { DashboardViewModule } from "../dashboard-view/dashboard-view.module";
import { DashboardWidgetCreateModule } from "../dashboard-widget-create/dashboard-widget-create.module";

// Components
import { DashboardEditComponent } from "./components/dashboard-edit/dashboard-edit.component";

// Services
import { DashboardEditService } from "./services/dashboard-edit.service";

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        VitreusModule,
        OTPipesModule,
        DashboardAPIModule,
        DashboardViewModule,
        DashboardWidgetCreateModule,
    ],
    declarations: [
        DashboardEditComponent,
    ],
    entryComponents: [
        DashboardEditComponent,
    ],
    providers: [
        PIPES,
        DashboardEditService,
    ],
})
export class DashboardEditModule {}
