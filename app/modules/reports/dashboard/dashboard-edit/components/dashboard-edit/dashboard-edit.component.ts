// 3rd party
import {
    Component,
    OnInit,
    OnDestroy,
} from "@angular/core";
import {
    takeUntil,
    filter,
    debounceTime,
    concatMap,
} from "rxjs/operators";
import { Subject } from "rxjs";

// Services
import { StateService } from "@uirouter/core";
import { DashboardViewService } from "reportsModule/dashboard/dashboard-view/services/dashboard-view.service";
import { DashboardEditService } from "reportsModule/dashboard/dashboard-edit/services/dashboard-edit.service";

// Interfaces
import {
    IDashboard,
    IDashboardLayout,
    IDashboardWidget,
} from "reportsModule/dashboard/dashboard-api/interfaces/dashboard.interface";

// Enums
import { ReportModuleTypes } from "reportsModule/reports-shared/enums/reports.enum";
import { Directions } from "sharedModules/enums/directions.enum";
import { DashboardActions } from "reportsModule/dashboard/dashboard-api/enums/dashboard.enum";

@Component({
    selector: "dashboard-edit",
    templateUrl: "./dashboard-edit.component.html",
})
export class DashboardEditComponent implements OnInit, OnDestroy {

    isLoading: boolean;
    dashboardId: string;
    dashboard: IDashboard;
    moduleType: ReportModuleTypes;
    editMode: boolean;
    private destroy$ = new Subject();
    private saveChange$: Subject<IDashboard> = new Subject();

    constructor(
        public dashboardViewService: DashboardViewService,
        private stateService: StateService,
        private dashboardEditService: DashboardEditService,
    ) {}

    ngOnInit() {
        this.dashboardId = this.stateService.params.id;

        this.saveChange$
            .pipe(
                takeUntil(this.destroy$),
                debounceTime(1000),
                concatMap((dashboard: IDashboard) => this.dashboardEditService.saveCurrentView(dashboard)),
            )
            .subscribe((dashboard: IDashboard) => this.dashboardViewService.refreshDashboard(dashboard));

        this.dashboardViewService.dashboard$
            .pipe(
                takeUntil(this.destroy$),
                filter((dashboard: IDashboard) => Boolean(dashboard)),
            )
            .subscribe((dashboard: IDashboard) => {
                this.dashboard = dashboard;
                this.moduleType = dashboard.moduleType[0];
                this.editMode = this.getWidgetCount(dashboard.layout) === 0 || this.editMode;
            });

        this.dashboardViewService.dashboardEvent$
            .pipe(takeUntil(this.destroy$))
            .subscribe(({ type, payload }) => {
                switch (type) {
                    case DashboardActions.LaunchWidgetModal:
                        this.launchWidgetModal(payload.columnIndex as number);
                        break;
                    case DashboardActions.LaunchEditWidgetModal:
                        this.launchEditWidgetModal(payload.widget as IDashboardWidget<unknown>);
                        break;
                    case DashboardActions.DeleteWidget:
                        this.confirmDeleteWidget(payload.widgetId as string);
                        break;
                    case DashboardActions.MoveWidget:
                        this.moveWidget(payload.direction as Directions, payload.columnIndex as number, payload.widgetIndex as number);
                        break;
                    default:
                }
            });
    }

    ngOnDestroy() {
        this.destroy$.next();
        this.destroy$.complete();
    }

    toggleEditMode(isEditing: boolean) {
        this.editMode = isEditing;
    }

    goToRoute(route: { path: string }) {
        this.stateService.go(route.path);
    }

    launchWidgetModal(columnIndex: number) {
        this.dashboardEditService.launchWidgetModal(this.moduleType, columnIndex, this.dashboard)
            .pipe(takeUntil(this.destroy$))
            .subscribe((dashboard: IDashboard) => {
                this.saveChange$.next(dashboard);
            });
    }

    changeLayout() {
        const dashboard: IDashboard = this.dashboard;
        this.dashboardEditService.changeLayout(dashboard)
            .pipe(takeUntil(this.destroy$))
            .subscribe(() => {
                this.dashboardViewService.initializeDashboard(this.dashboardId);
            });
    }

    launchEditWidgetModal(widget: IDashboardWidget<unknown>) {
        this.dashboardEditService.launchEditWidgetModal(widget, this.moduleType)
            .pipe(takeUntil(this.destroy$))
            .subscribe(() => {
                this.dashboardViewService.initializeDashboard(this.dashboardId);
            });
    }

    confirmDeleteWidget(widgetId: string) {
        this.dashboardEditService.confirmDeleteWidget(widgetId, this.dashboard)
            .pipe(takeUntil(this.destroy$))
            .subscribe((dashboard: IDashboard) => this.saveChange$.next(dashboard));
    }

    moveWidget(direction: Directions, columnIndex: number, widgetIndex: number) {
        const updatedDashboard: IDashboard = this.dashboardEditService.moveWidget(direction, columnIndex, widgetIndex, this.dashboard);
        this.saveChange$.next(updatedDashboard);
    }

    private getWidgetCount(layout: IDashboardLayout): number {
        const columns: string[][] = layout.columns;
        if (!(columns && columns.length)) return 0;
        return columns.reduce((prev: number, next: string[]) => {
            return prev + next.length;
        }, 0);
    }
}
