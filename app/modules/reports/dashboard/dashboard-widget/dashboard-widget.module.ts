import {
    NgModule,
} from "@angular/core";

// Modules
import { DashboardAPIModule } from "../dashboard-api/dashboard-api.module";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";
import { OTPipesModule, PIPES } from "modules/pipes/pipes.module";
import { TranslateModule } from "@ngx-translate/core";
import {
    OtLoadingModule,
    OtChartsModule,
    OtDataTableModule,
} from "@onetrust/vitreus";

// Components
import { DashboardWidgetComponent } from "./components/dashboard-widget/dashboard-widget.component";
import { TableWidgetComponent } from "./components/table-widget/table-widget.component";

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        OTPipesModule,
        DashboardAPIModule,
        OtLoadingModule,
        OtChartsModule,
        OtDataTableModule,
        TranslateModule,
    ],
    declarations: [
        DashboardWidgetComponent,
        TableWidgetComponent,
    ],
    exports: [
        DashboardWidgetComponent,
    ],
    providers: [
        PIPES, // TODO: Remove from providers when ot-common is implemented. Only necessary if you are using pipe in TS file, else just module is fine
    ],
})
export class DashboardWidgetModule {}
