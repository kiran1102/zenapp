import {
    Component,
    OnInit,
    Input,
} from "@angular/core";

// Enums
import { FilterColumnTypes } from "enums/filter-column-types.enum";

// Interfaces
import {
    ITableWidgetRow,
    ITableWidgetColumn,
    ITableWidgetData,
} from "reportsModule/dashboard/dashboard-api/interfaces/dashboard.interface";

@Component({
    selector: "table-widget",
    templateUrl: "./table-widget.component.html",
})
export class TableWidgetComponent implements OnInit {

    @Input() data: ITableWidgetData;

    widgetId: string;
    filterColumnTypes = FilterColumnTypes;

    columns: ITableWidgetColumn[] = [];
    rows: ITableWidgetRow[] = [];

    ngOnInit() {
        this.widgetId = this.data.id;
        this.columns = this.data.columns;
        this.rows = this.data.content;
    }
}
