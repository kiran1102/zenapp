import {
    Component,
    OnChanges,
    OnDestroy,
    Input,
    SimpleChanges,
    Output,
    EventEmitter,
} from "@angular/core";

// RXJS
import { Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";

// Interfaces
import {
    IDashboardWidget,
    IDashboardWidgetAction,
} from "reportsModule/dashboard/dashboard-api/interfaces/dashboard.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";
import { IChartDataElement } from "@onetrust/vitreus";

// Services
import { DashboardAPIService } from "reportsModule/dashboard/dashboard-api/services/dashboard-api.service";
import { DashboardAdapterService } from "reportsModule/dashboard/dashboard-api/services/dashboard-adapter.service";

// Enums
import { WidgetTypes } from "reportsModule/dashboard/dashboard-api/enums/dashboard.enum";
import { Directions } from "sharedModules/enums/directions.enum";

@Component({
    selector: "dashboard-widget",
    templateUrl: "./dashboard-widget.component.html",
})
export class DashboardWidgetComponent implements OnChanges, OnDestroy {

    @Input() widgetId: string;
    @Input() set widgetData(data: IDashboardWidget<unknown>) {
        this._widget = data;
    }
    @Input() showTitle = true;
    @Input() showActions = false;

    @Output() handleAction = new EventEmitter<IDashboardWidgetAction>();
    @Output() move = new EventEmitter<Directions>();
    @Output() edit = new EventEmitter<IDashboardWidget<unknown>>();
    @Output() delete = new EventEmitter<string>();

    isLoading: boolean;
    widgetTypes = WidgetTypes;
    directions = Directions;

    _widget: IDashboardWidget<unknown>;
    private destroy$ = new Subject();

    constructor(
        private dashboardAPIService: DashboardAPIService,
        private dashboardAdapterService: DashboardAdapterService,
    ) { }

    ngOnChanges(changes: SimpleChanges) {
        if (changes.widgetId && changes.widgetId.currentValue) {
            this.isLoading = true;
            this.dashboardAPIService.getWidget(changes.widgetId.currentValue)
                .pipe(takeUntil(this.destroy$))
                .subscribe((response: IProtocolResponse<IDashboardWidget<unknown>>) => {
                    if (response.result && response.data) {
                        this._widget = this.dashboardAdapterService.formatWidgetData(response.data);
                    }
                    this.isLoading = false;
                });
        }
    }

    ngOnDestroy() {
        this.destroy$.next();
        this.destroy$.complete();
    }

    handleClick(event: IChartDataElement) {
        if (!event) return;
        this.handleAction.emit({
            widgetId: this._widget.id,
            actionId: this._widget.action,
            targetId: event.id,
            targetValue: event.value,
        });
    }

    moveWidget(direction: Directions) {
        this.move.emit(direction);
    }

    editWidget(widget: IDashboardWidget<unknown>) {
        this.edit.emit(widget);
    }

    deleteWidget(widgetId: string) {
        widgetId = widgetId || this.widgetId;
        this.delete.emit(widgetId);
    }

}
