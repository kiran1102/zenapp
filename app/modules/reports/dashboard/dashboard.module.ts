import {
    NgModule,
} from "@angular/core";

// Modules
import { DashboardListModule } from "reportsModule/dashboard/dashboard-list/dashboard-list.module";
import { DashboardEditModule } from "reportsModule/dashboard/dashboard-edit/dashboard-edit.module";

@NgModule({
    imports: [
        DashboardListModule,
        DashboardEditModule,
    ],
})
export class DashboardModule {}
