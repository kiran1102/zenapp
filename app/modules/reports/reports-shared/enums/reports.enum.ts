export const ReportsTypePrettyNames = {
    COLUMN: "Column",
    PDF: "PDF",
};

export enum ReportType {
    PDF = "PDF",
    Column = "COLUMN",
}

export enum ReportTypeLowercase {
    PDF = "pdf",
    Column= "column",
}

export enum ReportTemplateIds {
    CustomProcessingActivity = "customPA",
    CustomAsset = "customAsset",
    CustomEntity = "customEntity",
    Article30PDF = "article30PDF",
    Article30Column = "article30Column",
    CustomAssessment = "customAssessment",
    CustomVendorInventory = "customVendorInventory",
    AssessmentDetails = "assessmentDetails",
}

export enum ReportPaperSizes {
    Letter = "letter",
    A4 = "A4",
}

export enum PDFSectionTypes {
    TextEditor = "TEXT",
    Column = "COLUMN",
    InventoryAttribute = "INVENTORY_ATTRIBUTE",
    AssessmentQuestion = "ASSESSMENT_QUESTION",
    AssessmentDetail = "ASSESSMENT_DETAIL",
    AssessmentNotes = "ASSESSMENT_NOTES",
    AssessmentAllQuestions = "ASSESSMENT_QUESTION_UNIVERSAL",
}

export enum PDFSectionOptions {
    Risk = "RISK",
}

export enum ReportEntityTypes {
    DynamicAttributes = "dynamicAttributes",
    Questions = "questions",
}

export enum SectionOptionTranslationKeys {
    question = "Question",
    response = "Response",
    justification = "Justification",
    comments = "Comments",
    risks = "Risks",
}

export enum ReportListColumnProps {
    Name = "name",
    Type = "type",
    Default = "isDefault",
    Org = "orgGroupId",
    Description = "description",
    CreatedBy = "createdBy",
    CreatedDate = "createDT",
    LastModifiedDate = "lastModifiedDate",
    ViewType = "viewType",
}

export enum ReportModuleTypes {
    Assessment = 10,
    Inventory = 20,
    Vendor = 30,
    Consent = 40,
    Risk = 50,
    Readiness = 70,
    Incident = 80,
}
