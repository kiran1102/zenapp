// 3rd Party
import {
    Pipe,
    PipeTransform,
} from "@angular/core";

// Interfaces
import { IFilterColumn } from "modules/reports/reports-shared/interfaces/report.interface";

// Services
import { ReportsAdapter } from "reportsModule/reports-shared/services/reports-adapter.service";

@Pipe({
    name: "columnLabel",
})

export class ColumnLabelPipe implements PipeTransform {

    constructor(
        private reportsAdapter: ReportsAdapter,
    ) {}

    transform(column: IFilterColumn, columnNameKey: string = "columnName"): string {
        return this.reportsAdapter.translateColumnGroupName(column, columnNameKey);
    }
}
