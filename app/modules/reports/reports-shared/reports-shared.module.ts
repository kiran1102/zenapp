import {
    NgModule,
} from "@angular/core";

// Modules
import { SharedModule } from "sharedModules/shared.module";
import { CommonModule } from "@angular/common";
import { HttpClientModule } from "@angular/common/http";
import { FormsModule } from "@angular/forms";
import { ReactiveFormsModule } from "@angular/forms";
import { VitreusModule } from "@onetrust/vitreus";
import { DndListModule } from "ngx-drag-and-drop-lists";
import { OTPipesModule, PIPES } from "modules/pipes/pipes.module";
import { VirtualScrollModule } from "od-virtualscroll";
import { OneVideoModule } from "modules/video/video.module";

// Services
import { ReportAPI } from "reportsModule/reports-shared/services/report-api.service";
import { ReportsAdapter } from "reportsModule/reports-shared/services/reports-adapter.service";
import { ReportTemplateExportService } from "./services/report-template-export.service";

// Components
import { ColumnPickerModal } from "reportsModule/reports-shared/components/column-picker-modal/column-picker-modal.component";
import { ListManagerComponent } from "reportsModule/reports-shared/components/list-manager/list-manager.component";
import { SelectionListComponent } from "reportsModule/reports-shared/components/selection-list/selection-list.component";
import { SearchScrollerComponent } from "reportsModule/reports-shared/components/search-scroller/search-scroller.component";

// Pipes
import { ColumnLabelPipe } from "./pipes/column-label.pipe";

@NgModule({
    imports: [
        SharedModule,
        CommonModule,
        HttpClientModule,
        FormsModule,
        ReactiveFormsModule,
        VitreusModule,
        DndListModule,
        OTPipesModule,
        VirtualScrollModule,
        OneVideoModule,
    ],
    declarations: [
        ColumnPickerModal,
        ListManagerComponent,
        SelectionListComponent,
        SearchScrollerComponent,
        ColumnLabelPipe,
    ],
    entryComponents: [
        ColumnPickerModal,
    ],
    providers: [
        ReportAPI,
        ReportsAdapter,
        ColumnLabelPipe,
        ReportTemplateExportService,
        PIPES, // TODO: Remove from providers when ot-common is implemented. Only necessary if you are using pipe in TS file, else just module is fine
    ],
    exports: [
        ColumnLabelPipe,
    ],
})
export class ReportsSharedModule {}
