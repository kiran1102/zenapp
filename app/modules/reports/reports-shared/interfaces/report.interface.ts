// Interfaces
import { IProtocolResponse } from "interfaces/protocol.interface";
import {
    IStringMap,
    ILabelValue,
} from "interfaces/generic.interface";
import { IPageableOf } from "interfaces/pagination.interface";
import { IPageable } from "interfaces/pagination.interface";

// Enums
import { ModuleTypes } from "enums/module-types.enum";
import { FilterOperators } from "modules/filter/enums/filter.enum";
import {
    PDFSectionTypes,
    ReportType,
} from "reportsModule/reports-shared/enums/reports.enum";

// RXJS
import { Observable } from "rxjs";

export interface IReportRow {
    id: string;
    cells: IReportCell[];
}

export interface IReportCell {
    columnId: string;
    hyperLink?: string;
    translationKey?: string;
    value: string;
}

export interface IColumnReportMetaData {
    reportName: string;
    sourceId: string;
    itemCount: number;
    pageNumber: number;
    pageSize: number;
    totalItemCount: number;
    totalPageCount: number;
    type: ModuleTypes;
}

export interface IReportView extends IColumnReportMetaData {
    filterCriteria: IFilterTree;
    columns: IFilterColumn[];
    sort: ISort;
    content: IReportRow[];
    layout: IReportLayout;
}

export interface IReport {
    name: string;
    description: string;
    id: string;
    type: number;
    templateId: string;
    orgId: string;
    orgGroupId?: string;
    creatorId: string;
    createdBy?: string;
    createdAt: string;
    modifiedAt: string;
    viewType?: ReportType;
    layoutTemplateId?: string;
    sourceId?: string;
    layout?: IReportLayout;
}

export interface IReportListResponse extends IPageableOf<IReport> {}

export interface IReportExport {
    reportExportFormat: string;
    name: string;
    entityId: string;
    sourceId?: string;
    reportEntityType: ModuleTypes;
    layout: IReportLayout;
    reportId?: string;
}

export interface IReportSectionInfo {
    sectionNumber: number;
    sectionName: string;
    visibleColumns: IFilterColumn[];
}

export interface IReportColumnModalResolve {
    callback: (response: IProtocolResponse<ISelectionColumn[]>) => void;
    getColumns: () => Observable<IColumnActiveMap | undefined>;
}

export interface IReportSectionModalResolve {
    title: string;
    sectionName: string;
    sectionType?: string;
    isNew: boolean;
    sectionOptions?: ISectionOptionsInfo;
    saveSection: (sectionName: string, sectionType?: string, sectionOptions?: ISectionOptionsInfo, includeAllQuestions?: boolean) => Observable<IFilterColumn[] | void>;
}

export interface ISectionOptionsInfo {
    question: boolean;
    response: boolean;
    justification: boolean;
    comments: boolean;
    risks: boolean;
}

export interface IEditReportModalResolve {
    modalTitle: string;
    reportModelId: string;
    isCopy?: boolean;
    promiseToResolve: (report: IReport) => Observable<boolean>;
}

export interface IColumnActiveMap {
    activeColumns: IFilterColumnOption[];
    inactiveColumns: IFilterColumnOption[];
}

export interface ISelectionColumn extends IFilterColumnOption {
    isSelected ?: boolean;
}

export interface IPDFSection {
    name: string;
    type?: PDFSectionTypes;
    textContent?: string;
    nameKey?: string;
    order: number;
    options?: ISectionOptionsInfo;
    columns?: IFilterColumn[];
}

export interface IPDFColumn {
    columnName: string;
}

export interface IReportTemplateOption {
    name: string;
    sourceId: string;
    layoutTemplateId: string;
    templateId: string;
}

export interface ISeededReportTemplate {
    id: string;
    name: string;
    nameKey: string;
}

export interface ICreateReportResponse {
    ancestorOrgGroupIds: string[];
    createDT: string;
    createdBy: string;
    description: string;
    id: string;
    lastModifiedBy: string;
    lastModifiedDate: string;
    name: string;
    new: boolean;
    orgGroupId: string;
    sortInfo: ISort;
    sourceId: string;
    tenantId: string;
    type: number;
    version: number;
    viewType: string;
    visibleColumns: IFilterColumn[];
}

export interface ISelectionOption {
    id: string;
    name: string;
    orgGroupId: string;
    label?: string;
    sourceId: string;
}

export interface IReportLayout {
    pageHeader?: boolean;
    nameKey?: string;
    paperSize?: string;
    sections?: IPDFSection[];
    entityId?: string;
    pageHeaderImage?: {
        name: string;
        file: string;
    };
    headerOnEveryPage?: boolean;
    default?: boolean;
}

export interface IPDFReportState {
    isSaving: boolean;
    hasChanges: boolean;
    isLoadingReport: boolean;
    options: ISelectionOption[];
    selectedOption: ISelectionOption;
    openSectionMap: IStringMap<boolean>;
    moduleType: number;
}

export interface IReportListActions {
    label: string;
    action: (report: IReportListItem) => void;
    iconClass?: string;
    getIconClass?: (report: IReportListItem) => string;
}

export interface IReportsCreateModalResolve {
    selectedReportModule?: number;
    selectedReportType?: string;
    selectedReportTemplate?: string;
    blankReportModelId: string;
    updateTempReportModel: (reportIdTemp: string, keyValueToMergeTemp: any) => void;
    deleteTempReportModel: (id: string) => void;
    createReport: (id: string) => Observable<IProtocolResponse<ICreateReportResponse>>;
}

export interface IReportListItem extends IReport {
    exportInProgress?: boolean;
}

export interface IDataTableSort {
    sortOrder: string;
    sortKey: string;
}

export interface ITemplateCard {
    header: string;
    imagePaths: string[];
    body: string;
}

export interface IReportListColumn {
    headerText: string;
    modelPropKey: string;
    getDisplayText?: (value: string) => string;
    filterType?: number;
    dataType?: number;
    defaultSorted?: boolean;
    defaultSortDirection?: number;
    sortKey?: string;
    options?: Array<ILabelValue<number|string>>;
    showPermission?: boolean;
    operator?: FilterOperators;
    isDefaultColumn?: boolean;
    sortable?: boolean;
}

export interface IColumnReportState {
    reportInitialized: boolean;
    loadingReport: boolean;
    loadingColumnMetaData: boolean;
    hasUnsavedChanges: boolean;
    exportInProgress: boolean;
    saveInProgress: boolean;
    hasFiltersApplied: boolean;
}

export interface IReportTemplate {
    id: string;
    name: string;
}

export interface IFilterTree {
    AND?: IFilter[];
    OR?: IFilter[];
}

export interface IFilter extends IFilterTree {
    columnId?: string;
    attributeKey?: string;
    entityType?: string;
    dataType?: number;
    operator?: string;
    values?: Array<string | number>;
}

export interface ISort {
    columnName: string;
    ascending: boolean;
}

export interface IFilterColumn {
    columnId: string;
    columnName: string;
    entityType: string;
    dataType: number;
    translationKey?: string;
    group?: string;
    groupName?: string;
    groupKey?: string[];
    groupLabel?: string;
    sortable?: boolean;
}

export interface IFilterColumnOption {
    name: string;
    translationKey?: string;
    id: string;
    type: number;
    entityType?: string;
    valueOptions?: IFilterValueOption[];
    valueOptionLink?: IFilterValueOptionLink;
    groupName?: string;
    group?: string;
    groupKey?: string[];
    groupLabel?: string;
    comparatorOptions?: number[];
    sortable?: boolean;
    filterOnKey?: boolean;
}

export interface IFilterValueOptionLink {
    url: string;
}

export interface IFilterValueOptionResponse {
    id: string;
    name: string;
    orgGroupId: string;
}

export interface IFilterValueOption {
    key: string | number;
    value: string | number;
    translationKey?: string;
}

export interface IReportState {
    allIds: string[];
    byId: IReportMap;
    tempModel: { [key: string]: IReport };
    selectedReportIds: { [key: string]: boolean };
    isFetchingReportList: boolean;
    paginationMetadata: IPageable | {};
    didInvalidate: boolean;
    sortColumn: string;
    sortAscending: boolean;
    searchTerm: string;
    filters: IFilter[];
}

export interface IReportMap {
    [key: string]: IReport;
}
