// Angular
import {
    Component,
    Input,
    Output,
    OnChanges,
    EventEmitter,
} from "@angular/core";

@Component({
    selector: "search-scroller",
    templateUrl: "./search-scroller.component.html",
})
export class SearchScrollerComponent implements OnChanges {

    @Input() count: number;
    @Input() placeholder: string;

    @Output() handleSearch = new EventEmitter<string>();
    @Output() goToItem = new EventEmitter<number>();

    searchTerm: string;
    currentSelectionIndex = 0;

    ngOnChanges(changes: ng.IOnChangesObject) {
        if (changes.count && !changes.count.isFirstChange) this.onSearch(this.searchTerm);
    }

    incrementSelection() {
        this.currentSelectionIndex++;
        this.goToItem.emit(this.currentSelectionIndex);
    }

    decrementSelection() {
        this.currentSelectionIndex--;
        this.goToItem.emit(this.currentSelectionIndex);
    }

    clearSearch() {
        this.searchTerm = "";
    }

    onSearch(searchTerm: string) {
        this.searchTerm = searchTerm;
        this.currentSelectionIndex = 0;
        this.handleSearch.emit(this.searchTerm);
        this.goToItem.emit(this.currentSelectionIndex);
    }
}
