// 3rd Party
import {
    OnInit,
    Component,
} from "@angular/core";

// RXJS
import { Subject } from "rxjs";

// Services
import { OtModalService } from "@onetrust/vitreus";

import {
    IColumnActiveMap,
    IReportColumnModalResolve,
    ISelectionColumn,
} from "modules/reports/reports-shared/interfaces/report.interface";
import { IOtModalContent } from "@onetrust/vitreus/src/app/vitreus/components/modal/modal";

@Component({
    selector: "column-picker-modal",
    templateUrl: "./column-picker-modal.component.html",
})
export class ColumnPickerModal implements OnInit, IOtModalContent {

    ready = false;
    otModalCloseEvent: Subject<string>;
    otModalData: IReportColumnModalResolve;
    private activeColumns: ISelectionColumn[];
    private inactiveColumns: ISelectionColumn[];

    constructor() {}

    ngOnInit() {
        this.otModalData.getColumns().subscribe( (response: IColumnActiveMap): void | undefined => {
            if (!response) return;
            this.activeColumns = response.activeColumns.map((column: ISelectionColumn) => {
                return this.cloneColumn(column);
            });
            this.inactiveColumns = response.inactiveColumns.map((column: ISelectionColumn) => {
                return this.cloneColumn(column);
            });
            this.ready = true;
        });
    }

    handleChange(columns: ISelectionColumn[]) {
        this.activeColumns = columns;
    }

    apply() {
        this.otModalData.callback({ result: true, data: this.activeColumns });
        this.closeModal();
    }

    closeModal() {
        this.clearSelected(this.inactiveColumns);
        this.clearSelected(this.activeColumns);
        this.otModalCloseEvent.next();
        this.otModalCloseEvent.complete();
    }

    private cloneColumn(column: ISelectionColumn): ISelectionColumn {
        const clonedColumn = JSON.parse(JSON.stringify(column));
        clonedColumn.valueOptions = null;
        return clonedColumn;
    }

    private clearSelected(columns: ISelectionColumn[]) {
        columns.forEach((column: ISelectionColumn): void => {
            column.isSelected = false;
        });
    }
}
