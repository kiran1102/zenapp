// Angular
import {
    Component,
    Input,
    Output,
    OnChanges,
    EventEmitter,
    ElementRef,
} from "@angular/core";

// 3rd Party
import { remove } from "lodash";

// Interfaces
import { IStringMap } from "interfaces/generic.interface";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

@Component({
    selector: "list-manager",
    templateUrl: "./list-manager.component.html",
})
export class ListManagerComponent implements OnChanges {

    @Input() activeList: Array<IStringMap<any>>;
    @Input() inactiveList: Array<IStringMap<any>>;
    @Input() activeLabel: string;
    @Input() inactiveLabel: string;
    @Input() labelKey: string;
    @Input() translationKey: string;
    @Input() wrapperClass: string;

    @Output() handleChange = new EventEmitter<Array<IStringMap<any>>>();

    filteredInactiveList: Array<IStringMap<any>>;
    filteredActiveList: Array<IStringMap<any>>;
    activeSearchTerm: string;
    directions = { up: 1, down: 0 };
    private inactiveSearchTerm: string;
    private isFocusSelected = false;

    constructor(
        private translatePipe: TranslatePipe,
        private elementRef: ElementRef,
    ) {}

    ngOnChanges(changes: ng.IOnChangesObject) {
        if (changes.inactiveList || changes.activeList) {
            this.inactiveList = [...this.inactiveList];
            this.activeList = [...this.activeList];
            this.filteredInactiveList = this.inactiveList.filter(this.inactiveFilter);
        }
    }

    moveItem(toListName: string, fromListName: string) {
        const selectedItems: Array<IStringMap<any>> = remove(this[fromListName], (item) => item.isSelected === true);
        this[toListName] = [...this[toListName], ...selectedItems];
        this.handleChange.emit(this.activeList);
        this.focusOnEnd(this.directions.down);
    }

    reorderList(list: Array<IStringMap<any>>, direction: number) {
        const orderedList: Array<IStringMap<any>> = direction === this.directions.up ? [...list] : [...list].reverse();
        orderedList.forEach((item: IStringMap<any>) => {
            if (item.isSelected) this.reorderItem(item, list, direction);
        });
        this.handleChange.emit(list);
        this.focusOnSelectedItem(direction !== this.directions.up);
    }

    moveToEnd(list: Array<IStringMap<any>>, direction: number) {
        const selectedItems: Array<IStringMap<any>> = remove(list, (item) => item.isSelected === true);
        list = direction === this.directions.up ? selectedItems.concat(list) : list.concat(selectedItems);
        this.handleChange.emit(list);
        this.focusOnEnd(direction);
    }

    getItemGroupNameLabel = (item: IStringMap<any>, returnHighlight?: boolean): string => {
        if (returnHighlight && item.highlight) return item.highlight;
        if (item.groupName) return item.groupName;
        if (item[this.translationKey]) return this.translatePipe.transform(item[this.translationKey]);
        if (item[this.labelKey]) return item[this.labelKey];
        return "";
    }

    scrollToItem(index: number, listSelector: string) {
        if (!this.isFocusSelected) {
            const highlightedEls = this.elementRef.nativeElement.querySelectorAll(`${listSelector} .highlight`);
            const selectedItemEL = highlightedEls && highlightedEls.length ? highlightedEls[index] : null;
            if (selectedItemEL) selectedItemEL.scrollIntoView(true);
        }
        this.isFocusSelected = false;
    }

    getItemLabel = (item: IStringMap<any>, returnHighlight?: boolean): string => {
        if (returnHighlight && item.highlight) return item.highlight;
        if (item[this.translationKey]) return this.translatePipe.transform(item[this.translationKey]);
        if (item[this.labelKey]) return item[this.labelKey];
        return "";
    }

    setInactiveSearchTerm(term: string) {
        this.inactiveSearchTerm = term;
        this.filteredInactiveList = this.inactiveList.filter(this.inactiveFilter);
    }

    searchActiveList(searchTerm: string) {
        this.activeSearchTerm = searchTerm;
        this.filteredActiveList = this.activeList.filter(this.activeFilter);
    }

    private reorderItem(item: IStringMap<any>, list: Array<IStringMap<any>>, direction: number) {
        if (!item || list.length < 2) return;
        const currentPosition: number = list.indexOf(item);
        const swapIndex: number = direction === this.directions.up ? currentPosition - 1 : currentPosition + 1;
        if (list[swapIndex] && !list[swapIndex].isSelected) {
            list[currentPosition] = list[swapIndex];
            list[swapIndex] = item;
        }
    }

    private focusOnSelectedItem(direction: boolean) {
        const selectedItemEL = this.elementRef.nativeElement.querySelector(".selection-list__item--selected");
        if (selectedItemEL) selectedItemEL.scrollIntoView(direction);
        this.isFocusSelected = true;
    }

    private focusOnEnd(direction: number) {
        const listEl = this.elementRef.nativeElement.querySelectorAll(".selection-list")[1];
        listEl.scrollTop = direction === this.directions.up ? 0 : listEl.scrollHeight + 100;
        this.isFocusSelected = true;
    }

    private inactiveFilter = (item: IStringMap<any>): boolean => {
        if (!this.inactiveSearchTerm) return true;
        const itemLabel: string = this.getItemLabel(item, false);
        return itemLabel ? itemLabel.toLowerCase().indexOf(this.inactiveSearchTerm.toLowerCase()) > -1 : false;
    }

    private activeFilter = (item: IStringMap<any>): boolean => {
        if (!this.activeSearchTerm) return true;
        const itemLabel: string = this.getItemGroupNameLabel(item);
        return itemLabel && this.activeSearchTerm ? itemLabel.toLowerCase().indexOf(this.activeSearchTerm.toLowerCase()) > -1 : false;
    }
}
