// Angular
import {
    Component,
    Input,
    Output,
    OnChanges,
    OnInit,
    EventEmitter,
} from "@angular/core";

import { TranslatePipe } from "pipes/translate.pipe";

// Pipes
import { GroupByPipe } from "pipes/group-by.pipe";

// Interfaces
import { IKeyValue } from "interfaces/generic.interface";

interface IListItem {
    [key: string]: any;
    isSelected?: boolean;
}

@Component({
    selector: "selection-list",
    templateUrl: "./selection-list.component.html",
})
export class SelectionListComponent implements OnChanges, OnInit {

    @Input() list: IListItem[];
    @Input() labelKey: string;
    @Input() translationKey: string;
    @Input() isMultiSelect: boolean;
    @Input() groupKey: string;
    @Input() highlightTerm: string;
    @Input() filter: (itemParam: { item: any }) => boolean;
    @Input() getLabel: (item: any) => string;

    @Output() handleSelect = new EventEmitter<any>();

    itemFilter: (item: any) => boolean;
    private lastClickedIndex = -1;

    constructor(
        private translatePipe: TranslatePipe,
        private groupByPipe: GroupByPipe,
    ) {}

    ngOnInit(): void {
        this.itemFilter = this.filter ? (item: any): boolean => this.filter({ item }) : (): boolean => true;
        this.flattenList();
    }

    ngOnChanges(changes: ng.IOnChangesObject): void {
        if (changes.list) {
            this.lastClickedIndex = -1;
            this.flattenList();
        }
    }

    flattenList() {
        this.list = this.groupByPipe.transform(this.list, this.groupKey)
            .reduce((prev: IListItem[], next: IKeyValue<IListItem[]>) => {
                return [...prev, ...next.value];
            }, []);
    }

    selectItem(item: IListItem, event: MouseEvent): void | undefined {
        const itemIndex: number = this.list.indexOf(item);
        item.isSelected = !item.isSelected;
        if (this.isMultiSelect) {
            if (event.shiftKey && this.lastClickedIndex > -1) {
                this.selectGroup(itemIndex, this.lastClickedIndex);
            }
            if (!event.shiftKey && !event.metaKey && !event.ctrlKey) {
                this.deselectAll(item);
            }
            this.lastClickedIndex = item.isSelected ? itemIndex : -1;
            return;
        }
        if (item.isSelected) {
            this.deselectAll(item);
        }
    }

    getItemLabel = (item: any): string => {
        if (this.getLabel) return this.getLabel(item);
        if (item[this.translationKey]) return this.translatePipe.transform(item[this.translationKey]);
        if (item[this.labelKey]) return item[this.labelKey];
        if (typeof item === "string") return item;
        return "";
    }

    trackByIndex(index: number): number {
        return index;
    }

    private selectGroup(from: number, to: number): void {
        if (from > to) {
            to = from + (from = to, 0);
        }
        for (let i = from; i <= to; i++) {
            this.list[i].isSelected = true;
        }
    }

    private deselectAll(skipItem?: IListItem): void {
        this.list.forEach((listItem: IListItem): void => {
            if (listItem !== skipItem) {
                listItem.isSelected = false;
            }
        });
    }
}
