// 3rd Party
import { Injectable } from "@angular/core";

// Services
import { ReportAPI } from "./report-api.service";
import { ReportsAdapter } from "reportsModule/reports-shared/services/reports-adapter.service";

// Interfaces
import { IProtocolResponse } from "interfaces/protocol.interface";
import {
    IFilterColumnOption,
    IFilterColumn,
    IReportExport,
    IReportLayout,
} from "../interfaces/report.interface";
import { IAssessmentTableRow } from "interfaces/assessment/assessment.interface";
import { IInventoryRecordV2 } from "interfaces/inventory.interface";

// Enums
import { ModuleTypes } from "enums/module-types.enum";
import {
    PDFSectionTypes,
    ReportEntityTypes,
    ReportType,
} from "../enums/reports.enum";
import { InventoryTableIds } from "enums/inventory.enum";

// RXJS
import { map, filter } from "rxjs/operators";
import { Observable } from "rxjs/Observable";

@Injectable()
export class ReportTemplateExportService {
    constructor(
        private reportAPI: ReportAPI,
        private reportsAdapter: ReportsAdapter,
    ) {}

    getDefaultLayout(reportType: string, moduleType: ModuleTypes): Promise<IProtocolResponse<IReportLayout>> {
        return this.reportAPI.getDefaultReportTemplate(reportType, moduleType).toPromise();
    }

    getAssessmentQuestionColumnMetadata(rootTemplateVersionId: string): Observable<IFilterColumnOption[]> {
        return this.reportAPI.getAssessmentColumnsMetadata(ModuleTypes.AE, rootTemplateVersionId).pipe(
            filter((response: IProtocolResponse<IFilterColumnOption[]>) => Boolean(response)),
            map((response: IProtocolResponse<IFilterColumnOption[]>): IFilterColumnOption[] => {
                return response.data.filter((option) => {
                    return option.entityType === ReportEntityTypes.Questions;
                });
            }));
    }

    setAssessmentAnyTemplateMetadata(columnMetadata: IFilterColumnOption[], layout: IReportLayout): IReportLayout {
        let filterColumns: IFilterColumn[] = [];

        filterColumns = columnMetadata.map((column) => {
            return  {
                ...column,
                columnId: column.id,
                columnName: column.name,
                dataType: column.type,
                entityType: column.entityType,
            };
        });

        layout.sections.forEach((section) => {
            if (section.type === PDFSectionTypes.AssessmentAllQuestions) {
                section.columns = filterColumns;
            }
        });
        return layout;
    }

    export(layout: IReportLayout, name: string, entityId: string, moduleType: ModuleTypes): Observable<IProtocolResponse<string>> {
        const exportBody: IReportExport = {
            reportExportFormat: ReportType.PDF,
            name,
            entityId,
            reportEntityType: moduleType,
            layout,
        };
        return this.reportAPI.exportPDF(exportBody);
    }

    assessmentsViewExport(assessment: IAssessmentTableRow, defaultExportLayout: IReportLayout) {
        this.getAssessmentQuestionColumnMetadata(assessment.templateRootVersionId).subscribe(
            (columnResponse: IFilterColumnOption[]) => {
                const layout = this.setAssessmentAnyTemplateMetadata(columnResponse, defaultExportLayout);
                this.export(layout, assessment.name, assessment.assessmentId, ModuleTypes.AE);
            });
    }
}
