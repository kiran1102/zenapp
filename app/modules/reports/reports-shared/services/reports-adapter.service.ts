// Angular
import {
    Injectable,
    Inject,
} from "@angular/core";

// Redux
import { getOrgById } from "oneRedux/reducers/org.reducer";
import { StoreToken } from "tokens/redux-store.token";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

// Interfaces
import {
    IReportView,
    IColumnActiveMap,
    ISelectionOption,
    IFilterColumn,
    IFilterColumnOption,
    IFilterValueOption,
} from "modules/reports/reports-shared/interfaces/report.interface";

import { IStore } from "interfaces/redux.interface";
import { IOrgUserAdapted } from "interfaces/org-user.interface";

@Injectable()
export class ReportsAdapter {

    constructor(
        private translatePipe: TranslatePipe,
        @Inject(StoreToken) private store: IStore,
    ) {}

    getSelectedOption(report: IReportView, options: ISelectionOption[]): ISelectionOption {
        if (!options || !options.length || !report) return null;
        if (!report.layout) return options[0];
        const selectedOption = options.find((option: ISelectionOption) => option.id === report.layout.entityId);
        return selectedOption || options[0];
    }

    createGroupNames(columns: Array<IFilterColumnOption | IFilterColumn>, columnNameKey: string): Array<IFilterColumnOption | IFilterColumn> {
        columns.forEach((column: IFilterColumnOption | IFilterColumn) => {
            column.groupLabel = this.getGroupLabel(column);
            column.groupName = this.translateColumnGroupName(column, columnNameKey);
        });
        return columns;
    }

    translateColumnGroupName(column: IFilterColumnOption | IFilterColumn, columnNameKey: string): string {
        if (!column.group) return column.translationKey ? this.translatePipe.transform(column.translationKey) : column[columnNameKey];
        const columnLabel: string = column.translationKey ? this.translatePipe.transform(column.translationKey) : column[columnNameKey];
        return `${columnLabel} - ${this.getGroupLabel(column)}`;
    }

    getGroupLabel(column: IFilterColumnOption | IFilterColumn): string {
        if (!column.group) return "";
        if (!column.groupKey || !column.groupKey.length) return column.group;
        if (!column.groupKey[1]) return this.translatePipe.transform(column.groupKey[0]);
        return `${this.translatePipe.transform(column.groupKey[0])} (${this.translatePipe.transform(column.groupKey[1])})`;
    }

    splitColumnsByActive(currentColumns: IFilterColumn[], allColumns: IFilterColumnOption[]): IColumnActiveMap {
        const inactiveColumns: IFilterColumnOption[] = [...allColumns];
        const activeColumns: IFilterColumnOption[] = [];
        currentColumns.forEach((activeColumn: IFilterColumn): void => {
            const matchingIndex: number = inactiveColumns.findIndex((column: IFilterColumnOption): boolean => column.id === activeColumn.columnId);
            if (matchingIndex > -1) {
                activeColumns.push(inactiveColumns[matchingIndex]);
                inactiveColumns.splice(matchingIndex, 1);
            }
        });
        return {
            activeColumns,
            inactiveColumns,
        };
    }

    formatColumnData(columns: IFilterColumnOption[]): IFilterColumn[] {
        return columns.map((column: IFilterColumnOption): IFilterColumn => {
            return {
                columnId: column.id,
                columnName: column.name,
                entityType: column.entityType,
                translationKey: column.translationKey,
                dataType: column.type,
                sortable: column.sortable,
                group: column.group,
                groupName: this.translateColumnGroupName(column, "name"),
                groupLabel: this.getGroupLabel(column),
                groupKey: column.groupKey,
            };
        });
    }

    formatPAOptions(options: ISelectionOption[]) {
        return options.map((option: ISelectionOption): ISelectionOption => {
            const orgName = this.getOrgName(option);
            option.label = orgName ? `${option.name} - ${orgName}` : `${option.name}`;
            return option;
        });
    }

    getOrgName(option: ISelectionOption): string {
        const org = getOrgById(option.orgGroupId)(this.store.getState());
        return org ? org.Name : "";
    }

    formatUserOptions(users: IOrgUserAdapted[]): IFilterValueOption[] {
        return users.map((user: IOrgUserAdapted): IFilterValueOption => {
            return { value: user.FullName, key: user.Id };
        });
    }

}
