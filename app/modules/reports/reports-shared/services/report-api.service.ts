// Angular
import { Injectable, Inject } from "@angular/core";
import { HttpRequest, HttpParams } from "@angular/common/http";

// rxjs
import {
    from,
    Observable,
} from "rxjs";

// Enums
import { ExportTypes } from "enums/export-types.enum";
import { ModuleTypes } from "enums/module-types.enum";

// Interfaces
import {
    IProtocolPacket,
    IProtocolMessages,
    IProtocolConfig,
    IProtocolResponse,
} from "interfaces/protocol.interface";
import {
    IReport,
    ISeededReportTemplate,
    ICreateReportResponse,
    IReportView,
    IReportExport,
    ISelectionOption,
    IReportTemplate,
    IReportListResponse,
    IFilterTree,
    IFilterColumn,
    IFilterColumnOption,
    ISort,
    IFilter,
    IFilterValueOption,
    IFilterValueOptionResponse,
    IReportLayout,
} from "modules/reports/reports-shared/interfaces/report.interface";
import {
    INewReportSchedule,
    IReportSchedule,
    IReportScheduleListResponse,
} from "reportScheduleModule/interfaces/report-schedule.interface";

// Services
import { ProtocolService } from "modules/core/services/protocol.service";
import { TaskPollingService } from "sharedServices/task-polling.service";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

@Injectable()
export class ReportAPI {

    private readonly defaultPageSize = 20;

    constructor(
        @Inject("Export") readonly Export: any,
        private translatePipe: TranslatePipe,
        private OneProtocol: ProtocolService,
        private taskPollingService: TaskPollingService,
    ) { }

    getReportList(pagenumber: number = 0, pagesize: number = this.defaultPageSize, sortColumn: string = "name", sortAscending: boolean = true, searchTerm?: string, filters: IFilter[] = []): Observable<IProtocolResponse<IReportListResponse>> {
        const pageParams: any = {
            "pagination.pagenumber": pagenumber,
            "pagination.pagesize": pagesize,
        };
        const filtersAndSearch: IFilter[] = [
            ...(searchTerm ? [{
                columnId: "name",
                attributeKey: "name",
                dataType: 30,
                operator: "RE",
                values: [searchTerm],
            }] : []),
            ...filters,
        ];
        const pageBody: { filterCriteria: IFilterTree, sortInfo: ISort } = {
            filterCriteria: filtersAndSearch && filtersAndSearch.length ? { AND: filtersAndSearch } : null,
            sortInfo: {
                columnName: sortColumn,
                ascending: sortAscending,
            },
        };
        const config: IProtocolConfig = this.OneProtocol.config("PUT", "/report/reports", pageParams, pageBody);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("ErrorGettingReport") } };
        return from(this.OneProtocol.http(config, messages));
    }

    createReport(reportModel: any): Observable<IProtocolResponse<ICreateReportResponse>> {
        const config: IProtocolConfig = this.OneProtocol.config("POST", "/report/view", null, reportModel);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("ErrorCreatingReport") } };
        return from(this.OneProtocol.http(config, messages));
    }

    getEntityOptions(moduleType: number, subType: string): Observable<IProtocolResponse<ISelectionOption[]>> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("GET", `/reporting/entity/${moduleType}/${subType}`);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("ErrorGettingAssessment") } };
        return from(this.OneProtocol.http(config, messages));
    }

    getAssessmentColumnsMetadata(moduleType: number, templateId: string): Observable<IProtocolResponse<IFilterColumnOption[]>> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("GET", "/api/template/v1/reports/metadata", { type: moduleType, templateId });
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("ErrorGettingReport") } };
        return from(this.OneProtocol.http(config, messages));
    }

    getRiskMetaData(): Observable<IProtocolResponse<IFilterColumnOption[]>> {
        const config: IProtocolConfig = this.OneProtocol.config("GET", "/risk/reports/metadata", { type: ModuleTypes.PIA });
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("ErrorGettingReport") } };
        return from(this.OneProtocol.http(config, messages));
    }

    fetchAllRelatedTemplates(moduleType: ModuleTypes): Observable<IReportTemplate[] | boolean> {
        const pageParams: any = { module: moduleType };
        const config: IProtocolConfig = this.OneProtocol.config("GET", "/report/templates", pageParams);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("ErrorGettingReport") } };
        return from(this.OneProtocol.http(config, messages).then((res: IProtocolResponse<IReportTemplate[] | boolean>): IReportTemplate[] | boolean => {
            if (!res.result) return false;
            return res.data;
        }));
    }

    getReport(id: string, pageNumber: number = 0, pageSize: number = this.defaultPageSize): Observable<IProtocolResponse<IReportView>> {
        const pageParams: { "pagination.pagenumber": number, "pagination.pagesize": number } = {
            "pagination.pagenumber": pageNumber,
            "pagination.pagesize": pageSize,
        };
        const config: IProtocolConfig = this.OneProtocol.config("GET", `/report/view/${id}`, pageParams);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("ErrorGettingReport") } };
        return from(this.OneProtocol.http(config, messages));
    }

    updateReport(id: string, pageNumber: number = 0, pageSize: number = this.defaultPageSize, filterCriteria?: IFilterTree, visibleColumns?: IFilterColumn[], sortInfo?: ISort, save: boolean = false): Observable<IProtocolPacket> {
        const pageParams: { "pagination.pagenumber": number, "pagination.pagesize": number, save: boolean } = {
            "pagination.pagenumber": pageNumber,
            "pagination.pagesize": pageSize,
            save,
        };
        const errorVerb: string = this.translatePipe.transform(save ? "save" : "get");
        const body: { filterCriteria: IFilterTree, visibleColumns: IFilterColumn[], sortInfo: ISort } = { filterCriteria, visibleColumns, sortInfo };
        const config: IProtocolConfig = this.OneProtocol.config("PUT", `/report/view/${id}`, pageParams, body);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("ErrorGettingReport") } };
        return from(this.OneProtocol.http(config, messages));
    }

    saveReport(id: string, filterCriteria?: IFilterTree, columns?: IFilterColumn[], sort?: ISort): Observable<IProtocolPacket> {
        return this.updateReport(id, undefined, undefined, filterCriteria, columns, sort, true);
    }

    getColumns(module: number, templateId: string): Observable<IProtocolResponse<IFilterColumnOption[]>> {
        const queryParams: any = { module, templateId };
        const config: IProtocolConfig = this.OneProtocol.config("GET", "/report/columnmetadata", queryParams);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("ErrorGettingReport") } };
        return from(this.OneProtocol.http(config, messages));
    }

    exportReportAsync(reportId: string, exportType: ExportTypes = ExportTypes.XLSX): Observable<IProtocolResponse<string>> {
        const type: string = exportType === ExportTypes.CSV ? "csv" : "xlsx";
        const params = new HttpParams().set("type", type);
        const config = new HttpRequest("GET", `/report/exportNew/${reportId}`, { params, responseType: "text" });
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("ErrorExportingReport") } };
        return from(this.OneProtocol.http(config, messages));
    }

    deleteReport(reportId: string): Observable<IProtocolPacket> {
        const config: IProtocolConfig = this.OneProtocol.config("DELETE", `/report/view/${reportId}`);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("ErrorDeletingReport") } };
        return from(this.OneProtocol.http(config, messages));
    }

    updateReportInfo(reportModel: IReport): Observable<boolean> {
        if (!reportModel) throw new Error("report cannot be undefined.");
        const config: IProtocolConfig = this.OneProtocol.config("PUT", `/report/view/${reportModel.id}/header`, null, reportModel);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("ErrorUpdatingReport") } };
        return from(this.OneProtocol.http(config, messages).then((res: IProtocolPacket): boolean => res.result));
    }

    copyReportInfo(reportModel: IReport): Observable<IProtocolResponse<ICreateReportResponse>> {
        if (!reportModel.name) throw new Error("report cannot be undefined.");
        const config: IProtocolConfig = this.OneProtocol.config("POST", `/report/view/${reportModel.id}/copy`, null, { name: reportModel.name, orgGroupId: reportModel.orgGroupId });
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("ErrorCopyingReport") } };
        return from(this.OneProtocol.http(config, messages));
    }

    getPDFReportTemplates(moduleType: ModuleTypes): Observable<IProtocolResponse<ISeededReportTemplate[]>> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("GET", `/reporting/layouts/templates/pdf/summary/${moduleType}`);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("ErrorRetrievingTemplates") } };
        return from(this.OneProtocol.http(config, messages));
    }

    getColumnReportTemplates(): Observable<IProtocolResponse<ISeededReportTemplate[]>> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("GET", `/reporting/layouts/templates/column/`);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("ErrorRetrievingTemplates") } };
        return from(this.OneProtocol.http(config, messages));
    }

    saveReportView(reportId: string, report: IReportView): Observable<IProtocolResponse<boolean>> {
        const pageParams: { "pagination.pagenumber": number, "pagination.pagesize": number, save: boolean } = {
            "pagination.pagenumber": 0,
            "pagination.pagesize": this.defaultPageSize,
            "save": true,
        };
        const config: IProtocolConfig = this.OneProtocol.config("PUT", `/report/view/${reportId}`, pageParams, report);
        const messages: IProtocolMessages = {
            Error: { custom: this.translatePipe.transform("ErrorSavingReport") },
            Success: { custom: this.translatePipe.transform("SuccessfullySavedReport") },
        };
        return from(this.OneProtocol.http(config, messages));
    }

    exportPDF(requestPayload: IReportExport): Observable<IProtocolResponse<string>> {
        this.taskPollingService.startPolling(5000);
        const config: IProtocolConfig = this.OneProtocol.customConfig("PUT", `/reporting/async/export/entity`, null, requestPayload, null, null, null, "text");
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("ExportFailed") } };
        return from(this.OneProtocol.http(config, messages));
    }

    getFilterValueOptions(url: string): Observable<IFilterValueOption[]> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("GET", url);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("ErrorRetrievingOptions") } };
        return from(this.OneProtocol.http(config, messages).then((response: IProtocolResponse<IFilterValueOptionResponse[]>): IFilterValueOption[] => {
            if (!(response && response.result)) return [];
            return response.data.map((option: IFilterValueOptionResponse): IFilterValueOption => ({ value: option.name, key: option.id }));
        }));
    }

    addSchedule(schedule: INewReportSchedule): Observable<IProtocolResponse<IReportSchedule>> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("POST", "/reporting/schedule", null, schedule);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("ErrorAddingSchedule") } };
        return from(this.OneProtocol.http(config, messages));
    }

    editSchedule(scheduleId: string, schedule: INewReportSchedule): Observable<IProtocolResponse<IReportSchedule>> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("PUT", `/reporting/schedule/${scheduleId}`, null, schedule);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("ErrorUdatingSchedule") } };
        return from(this.OneProtocol.http(config, messages));
    }

    getSchedules(page: number = 0, sortKey: string = "name", sortOrder: string = "desc"): Observable<IProtocolResponse<IReportScheduleListResponse>> {
        const pageParams: { page: number, size: number, sort: string } = { page, size: this.defaultPageSize, sort: `${sortKey},${sortOrder}` };
        const config: IProtocolConfig = this.OneProtocol.customConfig("GET", "/reporting/schedule", pageParams);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("ErrorRetrievingSchedule") } };
        return from(this.OneProtocol.http(config, messages));
    }

    deleteSchedule(id: string): Observable<IProtocolResponse<IReportSchedule>> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("DELETE", `/reporting/schedule/${id}`);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("ErrorDeletingSchedule") } };
        return from(this.OneProtocol.http(config, messages));
    }

    setDefaultPDF(id: string, reportEntityType: ModuleTypes): Observable<IProtocolResponse<void>> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("PUT", `/reporting/layouts/templates/pdf/default/${reportEntityType}`, null, { id });
        const messages: IProtocolMessages = reportEntityType === ModuleTypes.INV ? {
            Success: { custom: this.translatePipe.transform("DefaultInventoryPDFSet")},
            Error: { custom: this.translatePipe.transform("ErrorSettingDefaultInventoryPDF")},
        } : {
            Success: { custom: this.translatePipe.transform("DefaultAssessmentPDFSet")},
            Error: { custom: this.translatePipe.transform("ErrorSettingDefaultAssessmentPDF")},
        };
        return from(this.OneProtocol.http(config, messages));
    }

    getDefaultReportTemplate(reportType: string, moduleType: number): Observable<IProtocolResponse<IReportLayout>> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("GET", `/reporting/layouts/templates/${reportType}/default/${moduleType}`);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("ErrorGettingDefaultLayout") } };
        return from(this.OneProtocol.http(config, messages));
    }

    getModuleColumnMetaData(moduleType: ModuleTypes, sourceId: string): Observable<IProtocolResponse<IFilterColumnOption[]>> {
        if (moduleType === ModuleTypes.PIA) return this.getAssessmentColumnsMetadata(moduleType, sourceId);
        if (moduleType === ModuleTypes.RISK) return this.getRiskMetaData();
        return this.getColumns(moduleType, sourceId);
    }
}
