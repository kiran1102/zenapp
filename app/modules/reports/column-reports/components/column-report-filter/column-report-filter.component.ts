// 3rd Party
import {
    Component,
    OnInit,
    EventEmitter,
    Output,
    Input,
    OnDestroy,
} from "@angular/core";

// RXJS
import {
    Subject,
    Observable,
    combineLatest,
    Subscription,
} from "rxjs";
import {
    takeUntil,
    map,
    filter,
} from "rxjs/operators";

// Services
import { ColumnReportService } from "reportsModule/column-reports/services/column-report.service";
import { ReportsAdapter } from "reportsModule/reports-shared/services/reports-adapter.service";
import { TimeStamp } from "oneServices/time-stamp.service";

// Interfaces
import {
    IFilterV2,
    IFilterOutput,
} from "modules/filter/interfaces/filter.interface";
import { ILabelValue } from "interfaces/generic.interface";
import {
    IFilterColumnOption,
    IFilterValueOption,
    IFilter,
    IFilterTree,
} from "modules/reports/reports-shared/interfaces/report.interface";

// Enum + Constants
import { ExportTypes } from "enums/export-types.enum";
import { FilterColumnTypes } from "enums/filter-column-types.enum";
import {
    FilterValueTypes,
    FilterOperators,
} from "modules/filter/enums/filter.enum";
import { ReportFilterColumnTypes } from "reportsModule/reports-shared/constants/reports.constant";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

@Component({
    selector: "column-report-filter",
    template: `
        <one-filter
            class="flex-full-height"
            [selectedFilters]="selectedFilters"
            [fields]="columnOptions"
            [getFieldLabelValue]="getFieldLabelValue"
            [currentValues]="currentFilterValues"
            [getValueLabelValue]="getColumnOptionLabelValue"
            [currentType]="currentFilterValueType"
            [loadingFilter]="loadingFilter"
            [togglePopup$]="filtersOpen$"
            (selectField)="setFilterValues($event)"
            (applyFilters)="apply($event)"
            (cancel)="cancel.emit()"
            >
        </one-filter>
    `,
})
export class ColumnReportFilterComponent implements OnInit, OnDestroy {

    @Input() set filtersOpen(isOpen: boolean) {
        if (!isOpen) this.filtersOpen$.next(isOpen);
    }

    @Output() applyFilters = new EventEmitter<null>();
    @Output() cancel = new EventEmitter<null>();

    filterColumnTypes = FilterColumnTypes;
    exportTypes = ExportTypes;
    showingExportOptions = false;
    currentFilterValues: IFilterValueOption[] = [];
    selectedFilters: Array<IFilterV2<IFilterColumnOption, ILabelValue<string>, IFilterValueOption | string | number>>;
    currentFilterValueType: FilterValueTypes;
    columnOptions: IFilterColumnOption[];
    loadingFilter: boolean;
    filtersOpen$ = new Subject<boolean>();
    private userOptions: IFilterValueOption[];
    private destroy$ = new Subject();

    constructor(
        public columnReportService: ColumnReportService,
        private reportsAdapter: ReportsAdapter,
        private translatePipe: TranslatePipe,
        private timeStamp: TimeStamp,
    ) {}

    ngOnInit() {
        const allColumn$: Observable<IFilterColumnOption[]> = this.columnReportService.allColumn$.pipe(
            filter((columns: IFilterColumnOption[]): boolean => Boolean(columns && columns.length)),
            map((columns: IFilterColumnOption[]): IFilterColumnOption[] => this.getOptionTypeColumns(columns)),
        );
        const userOption$: Observable<IFilterValueOption[]> = this.columnReportService.userOption$.pipe(
            filter((options: IFilterValueOption[]): boolean => Boolean(options && options.length)),
        );
        combineLatest(allColumn$, userOption$)
            .pipe(takeUntil(this.destroy$))
            .subscribe(([columns, userOptions]) => {
                this.columnOptions = columns;
                this.userOptions = userOptions;
            });
        this.columnReportService.reportFilter$
            .pipe(takeUntil(this.destroy$))
            .subscribe((filters: IFilterTree) => {
                this.selectedFilters = filters && filters.AND ? this.serializeFilters(filters.AND) : [];
            });
    }

    ngOnDestroy() {
        this.destroy$.next();
        this.destroy$.complete();
    }

    apply(filters: IFilterOutput[]) {
        this.columnReportService.applyFilters(filters && filters.length ? { AND: this.deserializeFilters(filters) } : null);
        this.applyFilters.emit();
    }

    getFieldLabelValue = (column: IFilterColumnOption): ILabelValue<string> => {
        return {
            label: this.reportsAdapter.translateColumnGroupName(column, "name"),
            value: column.id,
        };
    }

    setFilterValues(selectedField: ILabelValue<string>) {
        if (!selectedField) return;
        const selectedColumn = this.columnOptions.find((column: IFilterColumnOption) => column.id === selectedField.value);
        this.currentFilterValueType = ReportFilterColumnTypes[selectedColumn.type] || FilterValueTypes.MultiSelect;
        if (selectedColumn.valueOptionLink && !selectedColumn.valueOptions) {
            this.loadingFilter = true;
            const optionSubscripton: Subscription = this.columnReportService.getValueOptions(selectedColumn)
                .subscribe((options: IFilterValueOption[]) => {
                    this.currentFilterValues = options;
                    this.loadingFilter = false;
                    optionSubscripton.unsubscribe();
                });
        } else {
            this.currentFilterValues = selectedColumn
                ? (selectedColumn.type === FilterColumnTypes.User ? this.userOptions : selectedColumn.valueOptions)
                : [];
        }
    }

    getColumnOptionLabelValue = (option: IFilterValueOption): ILabelValue<string | number> => {
        return {
            label: option.translationKey ? this.translatePipe.transform(option.translationKey) : option.value as string,
            value: option.key,
        };
    }

    private formatFilterSelections(column: IFilterColumnOption, selections: string[]): IFilterValueOption[] {
        const uniqueValues: string[] = selections.filter((selection: string, index: number, arr: string[]) => arr.indexOf(selection) === index);
        if (column.type === FilterColumnTypes.User) {
            return uniqueValues.reduce((acc: IFilterValueOption[], curr: string): IFilterValueOption[] => {
                const option: IFilterValueOption = this.userOptions.find((filterOption: IFilterValueOption) => filterOption.value === curr);
                if (option) acc.push(option);
                return acc;
            }, []);
        }
        if (
            column.type === FilterColumnTypes.Date
            || column.type === FilterColumnTypes.DateRange
            || column.type === FilterColumnTypes.DateTime
        ) {
            return [
                { key: uniqueValues[0], value: null },
                { key: this.timeStamp.revertEndOfDayISO(uniqueValues[1]), value: null },
            ];
        }
        if (column.valueOptions && column.valueOptions.length) {
            return uniqueValues.reduce((acc: IFilterValueOption[], curr: string): IFilterValueOption[] => {
                const option: IFilterValueOption = column.valueOptions.find((filterOption: IFilterValueOption) => String(filterOption.value) === String(curr));
                if (option) acc.push(option);
                return acc;
            }, []);
        }
        return uniqueValues.map((value: string): IFilterValueOption => ({ key: value, value }));
    }

    private serializeFilters(filters: IFilter[]): Array<IFilterV2<IFilterColumnOption, ILabelValue<string>, IFilterValueOption | string | number>> {
        return filters.map((selectedFilter: IFilter): IFilterV2<IFilterColumnOption, ILabelValue<string>, IFilterValueOption | string | number> => {
            const matchingColumn: IFilterColumnOption = this.columnOptions.find((column: IFilterColumnOption): boolean => column.id === selectedFilter.columnId);
            return {
                field: matchingColumn,
                operator: null,
                values: selectedFilter.values && selectedFilter.values.length ? this.formatFilterSelections(matchingColumn, selectedFilter.values as string[]) : selectedFilter.values,
                type: ReportFilterColumnTypes[matchingColumn.type] || FilterValueTypes.MultiSelect,
            };
        });
    }

    private deserializeFilters(filters: IFilterOutput[]): IFilter[] {
        if (!(filters && filters.length)) return [];
        return (filters as IFilterOutput[]).map((selectedFilter: IFilterOutput): IFilter => {
            const matchingColumn: IFilterColumnOption = this.columnOptions.find((column: IFilterColumnOption): boolean => column.id === selectedFilter.field.value);
            return {
                columnId: matchingColumn.id,
                attributeKey: matchingColumn.id,
                entityType: matchingColumn.entityType,
                dataType: matchingColumn.type,
                operator: this.deserializeOperator(matchingColumn.type),
                values: this.deserializeFilterValues(selectedFilter.values, matchingColumn.valueOptions, selectedFilter.type),
            };
        });
    }

    private deserializeFilterValues(values: Array<ILabelValue<string | number>>, options: IFilterValueOption[], type: FilterValueTypes): Array<string|number> {
        if (!(values && values.length)) return [];
        if (options && options.length) {
            return values.map((value: ILabelValue<string | number>): string | number => {
                const matchingOption: IFilterValueOption = options.find((option: IFilterValueOption): boolean => option.key === value.value);
                if (matchingOption) return matchingOption.value;
                return value.label || value.value;
            });
        }
        if (type === FilterValueTypes.DateRange) {
            return [
                values[0].value || values[0].label,
                this.timeStamp.getEndOfDayISO((values[1].value || values[1].label) as string),
            ];
        }
        return values.map((value: ILabelValue<string|number>): string | number => value.label || value.value);
    }

    private deserializeOperator(columnType: FilterColumnTypes): FilterOperators {
        if (
            columnType === FilterColumnTypes.Date
            || columnType === FilterColumnTypes.DateRange
            || columnType === FilterColumnTypes.DateTime
        ) {
            return FilterOperators.Between;
        }
        return FilterOperators.Equals;
    }

    private getOptionTypeColumns(columns: IFilterColumnOption[]): IFilterColumnOption[] {
        return columns.filter((column: IFilterColumnOption): boolean => {
            return column.type === FilterColumnTypes.Multi
                || column.type === FilterColumnTypes.MultiInt
                || column.type === FilterColumnTypes.User
                || (column.type === FilterColumnTypes.Text && Boolean(column.valueOptionLink))
                || column.type === FilterColumnTypes.Date
                || column.type === FilterColumnTypes.DateRange
                || column.type === FilterColumnTypes.DateTime;
        });
    }

}
