// 3rd Party
import {
    Component,
    Inject,
    OnInit,
    OnDestroy,
} from "@angular/core";
import { isUndefined } from "lodash";
import {
    TransitionService,
    StateService,
    TransitionOptions,
    RawParams,
    StateOrName,
} from "@uirouter/core";
import {
    Transition,
    HookResult,
} from "@uirouter/angularjs";

// RXJS
import { Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";

// Services
import { ColumnReportService } from "reportsModule/column-reports/services/column-report.service";
import { DrawerActionService } from "oneServices/actions/drawer-action.service";
import { TenantTranslationService } from "sharedServices/tenant-translation.service";
import { Permissions } from "modules/shared/services/helper/permissions.service";
import { ReportsAdapter } from "reportsModule/reports-shared/services/reports-adapter.service";
import { LoadingService } from "modules/core/services/loading.service";
import {
    OtModalService,
    ModalSize,
} from "@onetrust/vitreus";

// Interfaces
import { IProtocolResponse } from "interfaces/protocol.interface";
import { IDropdownOption } from "interfaces/one-dropdown.interface";
import {
    IReportColumnModalResolve,
    ISelectionColumn,
    IColumnReportMetaData,
    IColumnReportState,
    IFilterColumnOption,
    ISort,
} from "modules/reports/reports-shared/interfaces/report.interface";
import { IPageableMeta } from "interfaces/pagination.interface";

// Enum + Constants
import { ExportTypes } from "enums/export-types.enum";
import { ModuleTypes } from "enums/module-types.enum";
import { FilterColumnTypes } from "enums/filter-column-types.enum";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

// Components
import { ColumnPickerModal } from "../../../reports-shared/components/column-picker-modal/column-picker-modal.component";
import { UnsavedChangesConfirmationModal } from "modules/shared/components/unsaved-changes-confirmation-modal/unsaved-changes-confirmation-modal.component";

@Component({
    selector: "column-report",
    templateUrl: "./column-report.component.html",
})
export class ColumnReportComponent implements OnInit, OnDestroy {

    showExport: boolean;
    showExportCsv: boolean;
    filterColumnTypes = FilterColumnTypes;
    exportTypes = ExportTypes;
    showingExportOptions = false;
    filtersOpen = false;
    reportName: string;
    pageData: IPageableMeta;
    reportState: IColumnReportState;
    dropdownOptions: IDropdownOption[] = [];
    private destroy$ = new Subject();

    private transitionDeregisterHook = this.$transitions.onStart({ from: "zen.app.pia.module.reports-new.view" }, (transition: Transition): HookResult => {
        return this.handleStateChange(transition.to(), transition.params("to"), transition.options());
    });

    constructor(
        public $transitions: TransitionService,
        @Inject("TenantTranslationService") public tenantTranslationService: TenantTranslationService,
        public columnReportService: ColumnReportService,
        private permissions: Permissions,
        private drawerAction: DrawerActionService,
        private translatePipe: TranslatePipe,
        private reportsAdapter: ReportsAdapter,
        private $state: StateService,
        private loadingService: LoadingService,
        private otModalService: OtModalService,
    ) {}

    ngOnInit() {
        this.showExport = this.permissions.canShow("ReportsExportNew");
        this.columnReportService.reportMetaData$
            .pipe(takeUntil(this.destroy$))
            .subscribe((metaData: IColumnReportMetaData) => {
                this.showExportCsv = this.permissions.canShow("ReportsExportNewCsv") && metaData.type === ModuleTypes.CM;
                if (this.showExport && this.showExportCsv) {
                    this.dropdownOptions = this.setDropdownOptions();
                }
                this.reportName = metaData.reportName;
                this.pageData = {
                    number: metaData.pageNumber,
                    numberOfElements: metaData.itemCount,
                    size: metaData.pageSize,
                    totalElements: metaData.totalItemCount,
                    totalPages: metaData.totalPageCount,
                    first: metaData.pageNumber === 0,
                    last: metaData.pageNumber === metaData.totalPageCount - 1,
                };
            });
        this.columnReportService.reportState$
            .pipe(takeUntil(this.destroy$))
            .subscribe((state: IColumnReportState) => {
                this.reportState = state;
            });
        this.columnReportService.initReport(this.$state.params.id);
    }

    ngOnDestroy() {
        if (this.filtersOpen) this.drawerAction.closeDrawer();
        if (this.transitionDeregisterHook) {
            this.transitionDeregisterHook();
        }
        this.destroy$.next();
        this.destroy$.complete();
    }

    launchColumnModal() {
        const callback = (response: IProtocolResponse<ISelectionColumn[]>) => {
            if (!response || !response.result || !response.data) return;
            this.columnReportService.updateColumns(this.reportsAdapter.formatColumnData(response.data as IFilterColumnOption[]));
        };
        const modalData: IReportColumnModalResolve = {
            getColumns: this.columnReportService.getColumnsForManager,
            callback,
        };
        this.otModalService.create(
            ColumnPickerModal,
            { isComponent: true, size: ModalSize.MEDIUM },
            modalData,
        );
    }

    toggleFilters(isOpen?: boolean) {
        if (!isUndefined(isOpen)) {
            this.filtersOpen = isOpen;
        } else {
            this.filtersOpen = !this.filtersOpen;
        }
        if (this.filtersOpen) {
            this.drawerAction.openDrawer();
        } else {
            this.drawerAction.closeDrawer();
        }
    }

    goToReportList(event) {
        this.$state.go(event.path);
    }

    handleSort({ sortKey, sortOrder }) {
        const sort: ISort = {
            columnName: sortKey,
            ascending: sortOrder === "asc",
        };
        this.columnReportService.sortTable(sort);
    }

    toggleExportDropdown(showOptions: boolean) {
        this.showingExportOptions = showOptions;
    }

    saveReport() {
        this.columnReportService.saveReport().subscribe();
    }

    exportReport(exportType: ExportTypes) {
        this.columnReportService.exportReport(exportType);
    }

    setDropdownOptions(): IDropdownOption[] {
        return [
            {
                id: "ReportsExportOptionXLSX",
                text: this.translatePipe.transform("Excel"),
                action: () => this.exportReport(this.exportTypes.XLSX),
            },
            {
                id: "ReportsExportOptionCSV",
                text: this.translatePipe.transform("Csv"),
                action: () => this.exportReport(this.exportTypes.CSV),
            },
        ];
    }

    private launchSaveChangesModal(callback: () => void) {
        this.otModalService
            .create(
                UnsavedChangesConfirmationModal,
                {
                    isComponent: true,
                    size: ModalSize.SMALL,
                    hideClose: true,
                },
                {
                    promiseToResolve: () => this.columnReportService.saveReport().subscribe((): boolean => {
                        callback();
                        return true;
                    }),
                    discardCallback: () => {
                        this.columnReportService.updateState({ hasUnsavedChanges: false });
                        callback();
                        return true;
                    },
                },
            ).pipe(
                takeUntil(this.destroy$),
            ).subscribe(
                (success: boolean) => {
                    if (!success) {
                        this.loadingService.set(false);
                    }
                },
            );
    }

    private handleStateChange(toState: StateOrName, toParams: RawParams, options: TransitionOptions): boolean {
        if (!this.reportState.hasUnsavedChanges) return true;
        const callback = () => {
            this.$state.go(toState, toParams, options);
        };
        this.launchSaveChangesModal(callback);
        return false;
    }
}
