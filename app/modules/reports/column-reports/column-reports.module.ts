import {
    NgModule,
} from "@angular/core";

// Modules
import { SharedModule } from "sharedModules/shared.module";
import { CommonModule } from "@angular/common";
import { HttpClientModule } from "@angular/common/http";
import { FormsModule } from "@angular/forms";
import { ReactiveFormsModule } from "@angular/forms";
import { VitreusModule } from "@onetrust/vitreus";
import { DndListModule } from "ngx-drag-and-drop-lists";
import { OTPipesModule, PIPES } from "modules/pipes/pipes.module";
import { VirtualScrollModule } from "od-virtualscroll";
import { OneVideoModule } from "modules/video/video.module";
import { ReportsSharedModule } from "reportsModule/reports-shared/reports-shared.module";
import { FilterModule } from "modules/filter/filter.module";

// Components
import { ColumnReportComponent } from "reportsModule/column-reports/components/column-report/column-report.component";
import { ColumnReportFilterComponent } from "reportsModule/column-reports/components/column-report-filter/column-report-filter.component";

// Services
import { ColumnReportService } from "reportsModule/column-reports/services/column-report.service";

@NgModule({
    imports: [
        SharedModule,
        CommonModule,
        HttpClientModule,
        FormsModule,
        ReactiveFormsModule,
        VitreusModule,
        DndListModule,
        OTPipesModule,
        VirtualScrollModule,
        OneVideoModule,
        ReportsSharedModule,
        FilterModule,
    ],
    declarations: [
        ColumnReportComponent,
        ColumnReportFilterComponent,
    ],
    entryComponents: [
        ColumnReportComponent,
    ],
    providers: [
        ColumnReportService,
        PIPES, // TODO: Remove from providers when ot-common is implemented. Only necessary if you are using pipe in TS file, else just module is fine
    ],
})
export class ColumnReportsModule {}
