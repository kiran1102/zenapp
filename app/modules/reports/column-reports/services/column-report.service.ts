// 3rd Party
import { Injectable, Inject } from "@angular/core";
import { cloneDeep } from "lodash";
import {
    Observable,
    BehaviorSubject,
    from,
    forkJoin,
} from "rxjs";
import { map, tap } from "rxjs/operators";

// Redux
import { getCurrentOrgId } from "oneRedux/reducers/org.reducer";
import { StoreToken } from "tokens/redux-store.token";
import { IStore } from "interfaces/redux.interface";

// Enums
import { ModuleTypes } from "enums/module-types.enum";
import { OrgUserTraversal } from "enums/org-user-dropdown.enum";

// Constants
import { TranslationModules } from "constants/translation.constant";

// Interfaces
import { IProtocolResponse } from "interfaces/protocol.interface";
import {
    IReportView,
    IColumnActiveMap,
    IColumnReportState,
    IColumnReportMetaData,
    IReportRow,
    IFilterColumn,
    IFilterTree,
    IFilterColumnOption,
    ISort,
    IFilterValueOption,
} from "modules/reports/reports-shared/interfaces/report.interface";
import { ITaskConfirmationModalResolve } from "interfaces/task-confirmation-modal-resolve.interface";
import { IStringMap } from "interfaces/generic.interface";
import { IOrgUserAdapted } from "interfaces/org-user.interface";

// Services
import { ReportAPI } from "reportsModule/reports-shared/services/report-api.service";
import { ExportTypes } from "enums/export-types.enum";
import { TaskPollingService } from "sharedServices/task-polling.service";
import { Permissions } from "modules/shared/services/helper/permissions.service";
import Utilities from "Utilities";
import { ReportsAdapter } from "reportsModule/reports-shared/services/reports-adapter.service";
import { TenantTranslationService } from "sharedServices/tenant-translation.service";
import { OrgGroupApiService } from "oneServices/api/org-group-api.service";
import { OtModalService } from "@onetrust/vitreus";

// Components
import { OTNotificationModalComponent } from "sharedModules/components/ot-notification-modal/ot-notification-modal.component";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

@Injectable()
export class ColumnReportService {

    allColumn$: Observable<IFilterColumnOption[]>;
    reportState$: Observable<IColumnReportState>;
    reportMetaData$: Observable<IColumnReportMetaData>;
    reportFilter$: Observable<IFilterTree>;
    reportColumn$: Observable<IFilterColumn[]>;
    reportRow$: Observable<IReportRow[]>;
    reportSort$: Observable<ISort>;
    userOption$: Observable<IFilterValueOption[]>;
    private defaultMetaData: IColumnReportMetaData = {
        reportName: "",
        sourceId: "",
        itemCount: 0,
        pageNumber: 0,
        pageSize: 20,
        totalItemCount: 0,
        totalPageCount: 0,
        type: ModuleTypes.PIA,
    };
    private defaultReportState: IColumnReportState = {
        reportInitialized: false,
        loadingReport: true,
        loadingColumnMetaData: true,
        hasUnsavedChanges: false,
        exportInProgress: false,
        saveInProgress: false,
        hasFiltersApplied: false,
    };
    private _allColumns: BehaviorSubject<IFilterColumnOption[]> = new BehaviorSubject(null);
    private _reportState: BehaviorSubject<IColumnReportState> = new BehaviorSubject(this.defaultReportState);
    private _metaData: BehaviorSubject<IColumnReportMetaData> = new BehaviorSubject(this.defaultMetaData);
    private _filters: BehaviorSubject<IFilterTree> = new BehaviorSubject(null);
    private _reportColumns: BehaviorSubject<IFilterColumn[]> = new BehaviorSubject(null);
    private _reportRows: BehaviorSubject<IReportRow[]> = new BehaviorSubject(null);
    private _sort: BehaviorSubject<ISort> = new BehaviorSubject(null);
    private _userOptions: BehaviorSubject<IFilterValueOption[]> = new BehaviorSubject(null);
    private reportId: string;
    private unresolvedColumnsPromise: Promise<IProtocolResponse<IFilterColumnOption[]>>;
    private renderId: string;

    constructor(
        private translatePipe: TranslatePipe,
        private reportAPI: ReportAPI,
        private taskPollingService: TaskPollingService,
        private reportsAdapter: ReportsAdapter,
        private orgGroupApiService: OrgGroupApiService,
        private permissions: Permissions,
        private otModalService: OtModalService,
        @Inject("TenantTranslationService") public tenantTranslationService: TenantTranslationService,
        @Inject(StoreToken) private store: IStore,
    ) {
        this.reportState$ = this._reportState.asObservable();
        this.reportMetaData$ = this._metaData.asObservable();
        this.reportFilter$ = this._filters.asObservable();
        this.reportColumn$ = this._reportColumns.asObservable();
        this.reportRow$ = this._reportRows.asObservable();
        this.allColumn$ = this._allColumns.asObservable();
        this.reportSort$ = this._sort.asObservable();
        this.userOption$ = this._userOptions.asObservable();
    }

    initReport(reportId: string) {
        this.reportId = reportId;
        this.resetReport();
        const observableArray: Array<Observable<IProtocolResponse<IReportView>|boolean|void>> = [
            this.reportAPI.getReport(reportId),
            this.setUserOptions(),
        ];
        if (!this.tenantTranslationService.loadedModuleTranslations[TranslationModules.Inventory]) observableArray.push(from(this.tenantTranslationService.addModuleTranslations(TranslationModules.Inventory)));
        forkJoin(observableArray).subscribe((response: [IProtocolResponse<IReportView>, boolean]) => {
            if (!response[0].result) return;
            this.updateReport(response[0].data);
            this._reportState.next({ ...this._reportState.getValue(), reportInitialized: true, loadingReport: false });
            this.getColumns().then(() => {
                this._reportState.next({ ...this._reportState.getValue(), loadingColumnMetaData: false });
            });
        });
    }

    getColumnsForManager = (): Observable<IColumnActiveMap> => {
        return from(this.getColumns().then((response: IProtocolResponse<IFilterColumnOption[]>): IColumnActiveMap => {
            if (!response || !response.data) return;
            return this.reportsAdapter.splitColumnsByActive(this._reportColumns.getValue(), response.data);
        }));
    }

    async exportReport(exportType: ExportTypes = ExportTypes.XLSX) {
        const currentState: IColumnReportState = this._reportState.getValue();
        this._reportState.next({ ...currentState, exportInProgress: true });
        if (currentState.hasUnsavedChanges) {
            await this.saveReport().toPromise();
        }

        this.reportAPI.exportReportAsync(this.reportId, exportType).subscribe(() => {
            this.otModalService
                .create(
                    OTNotificationModalComponent,
                    { isComponent: true },
                    { modalTitle: "ReportDownload", confirmationText: this.translatePipe.transform("ReportIsGeneratedCheckTasksBar") },
                );
            this.taskPollingService.startPolling();
            this._reportState.next({ ...this._reportState.getValue(), exportInProgress: false });
        });
    }

    saveReport(): Observable<IProtocolResponse<IReportView>> {
        this._reportState.next({
            ...this._reportState.getValue(),
            saveInProgress: true,
        });
        return this.reportAPI.saveReport(
            this.reportId,
            this._filters.getValue(),
            this._reportColumns.getValue(),
            this._sort.getValue(),
        ).pipe(
            tap((response: IProtocolResponse<IReportView>) => {
                if (!response.result) {
                    this._reportState.next({
                        ...this._reportState.getValue(),
                        saveInProgress: false,
                    });
                    return;
                }
                this._reportState.next({
                    ...this._reportState.getValue(),
                    hasUnsavedChanges: false,
                    saveInProgress: false,
                });
            }));
    }

    applyFilters(filters: IFilterTree) {
        this._filters.next(filters);
        this.updateTable(0, true);
    }

    updateColumns(columns: IFilterColumn[]) {
        this._reportColumns.next(columns);
        this.updateTable(0, true);
    }

    sortTable(sort: ISort) {
        this._sort.next(sort);
        this.updateTable(0, true);
    }

    changePage(page: number) {
        this.updateTable(page, false);
    }

    getValueOptions(column: IFilterColumnOption): Observable<IFilterValueOption[]> {
        if (!(column.valueOptionLink && column.valueOptionLink.url)) return from([]);
        return this.reportAPI.getFilterValueOptions(column.valueOptionLink.url).pipe(
            map((response: IFilterValueOption[]): IFilterValueOption[] => {
                const allColumns: IFilterColumnOption[] = this._allColumns.getValue();
                const matchingIndex: number = allColumns.findIndex((option: IFilterColumnOption) => column.id === option.id);
                if (matchingIndex !== -1) {
                    allColumns[matchingIndex].valueOptions = response;
                    this._allColumns.next(allColumns);
                }
                return response;
            }));
    }

    updateState(changes: IStringMap<boolean>) {
        this._reportState.next({ ...this._reportState.getValue(), ...changes });
    }

    private updateTable(page: number, enableSave: boolean = false) {
        this._reportState.next({ ...this._reportState.getValue(), loadingReport: true });
        return this.reportAPI.updateReport(
            this.reportId,
            page,
            undefined,
            this._filters.getValue(),
            this._reportColumns.getValue(),
            this._sort.getValue(),
        ).subscribe(
            (response: IProtocolResponse<IReportView>): Observable<void> => {
                const currentState: IColumnReportState = this._reportState.getValue();
                this._reportState.next({
                    ...currentState,
                    loadingReport: false,
                    hasUnsavedChanges: enableSave || currentState.hasUnsavedChanges,
                });
                if (!response || !response.result || !response.data) return;
                this.updateReport(response.data);
            });
    }

    private updateReport(responseData: IReportView) {
        this._filters.next(responseData.filterCriteria);
        const currentState: IColumnReportState = this._reportState.getValue();
        this._reportState.next({
            ...currentState,
            hasFiltersApplied: Boolean(responseData.filterCriteria && responseData.filterCriteria.AND && responseData.filterCriteria.AND.length),
        });
        this._sort.next(responseData.sort);
        const { reportName, sourceId, itemCount, pageNumber, pageSize, totalItemCount, totalPageCount, type } = responseData;
        this._metaData.next({
            reportName,
            sourceId,
            itemCount,
            pageNumber,
            pageSize,
            totalItemCount,
            totalPageCount,
            type,
        });
        this.staggerTableRendering(
            this.reportsAdapter.createGroupNames(responseData.columns, "columnName") as IFilterColumn[],
            responseData.content,
        );
    }

    private getColumns(): Promise<IProtocolResponse<IFilterColumnOption[]>> {
        if (this.unresolvedColumnsPromise) return this.unresolvedColumnsPromise;
        const allColumns: IFilterColumnOption[] = this._allColumns.getValue();
        const metaData: IColumnReportMetaData = this._metaData.getValue();
        if (allColumns) {
            return Promise.resolve({ result: true, data: [...allColumns] });
        }
        const callback = (response: IProtocolResponse<IFilterColumnOption[]>): IProtocolResponse<IFilterColumnOption[]> => {
            this.unresolvedColumnsPromise = null;
            if (!response || !response.result) return;
            this._allColumns.next(this.reportsAdapter.createGroupNames(response.data, "name") as IFilterColumnOption[]);
            return response;
        };
        if (
            (this.permissions.canShow("Assessments") && metaData.type === ModuleTypes.PIA)
            || (this.permissions.canShow("VendorRiskManagement") && metaData.type === ModuleTypes.VRM)
        ) {
            this.unresolvedColumnsPromise = this.reportAPI.getAssessmentColumnsMetadata(metaData.type, metaData.sourceId).toPromise().then(callback);
        } else if (metaData.type === ModuleTypes.RISK) {
            this.unresolvedColumnsPromise = this.reportAPI.getRiskMetaData().toPromise().then(callback);
        } else {
            this.unresolvedColumnsPromise = this.reportAPI.getColumns(metaData.type, metaData.sourceId).toPromise().then(callback);
        }
        return this.unresolvedColumnsPromise;
    }

    private staggerTableRendering(columns: IFilterColumn[], rows: IReportRow[]) {
        this._reportColumns.next([]);
        const emptyRows: IReportRow[] = (cloneDeep(rows)).map((row: IReportRow): IReportRow => {
            row.cells = [];
            return row;
        });
        this._reportRows.next(emptyRows);
        this.renderId = Utilities.uuid();
        this.setTableByColumnCount(columns, rows, 10, this.renderId);
    }

    private setTableByColumnCount(remainingColumns: IFilterColumn[], remainingRows: IReportRow[], count: number, id: string) {
        if (!remainingColumns || !remainingColumns.length || id !== this.renderId) return;
        this._reportColumns.next([...this._reportColumns.getValue(), ...remainingColumns.splice(0, count)]);
        const currentRows: IReportRow[] = this._reportRows.getValue();
        currentRows.forEach((row: IReportRow, index: number): void => {
            row.cells.push(...(remainingRows[index].cells.splice(0, count)));
        });
        this._reportRows.next(currentRows);
        setTimeout((): void => {
            this.setTableByColumnCount(remainingColumns, remainingRows, count, id);
        }, 0);
    }

    private setUserOptions(): Observable<void> {
        return from(this.orgGroupApiService.getOrgUsersWithPermission(
            getCurrentOrgId(this.store.getState()),
            OrgUserTraversal.All,
        ).then((response: IProtocolResponse<IOrgUserAdapted[]>) => {
            this._userOptions.next(!(response && response.result) ? [] : this.reportsAdapter.formatUserOptions(response.data));
        }));
    }

    private resetReport() {
        this._allColumns.next(null);
        this._metaData.next(this.defaultMetaData);
        this._filters.next(null);
        this._reportColumns.next(null);
        this._reportRows.next(null);
        this._sort.next(null);
        this._reportState.next(this.defaultReportState);
    }
}
