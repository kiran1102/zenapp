import { NgModule } from "@angular/core";

// Modules
import { SharedModule } from "sharedModules/shared.module";
import { CommonModule } from "@angular/common";
import { HttpClientModule } from "@angular/common/http";
import { FormsModule } from "@angular/forms";
import { ReactiveFormsModule } from "@angular/forms";
import { VitreusModule } from "@onetrust/vitreus";
import { DndListModule } from "ngx-drag-and-drop-lists";
import { OTPipesModule, PIPES } from "modules/pipes/pipes.module";
import { VirtualScrollModule } from "od-virtualscroll";
import { OneVideoModule } from "modules/video/video.module";
import { VendorSharedModule } from "modules/vendor/shared/vendor-shared.module";
import { FilterModule } from "modules/filter/filter.module";
import { VendorpediaExchangeModule } from "vendorpediaExchange/vendorpedia-exchange.module";
import { ContractsModule } from "modules/vendor/contracts/contracts.module";
import { EngagementsModule } from "modules/vendor/engagements/engagements.module";
import { ControlsSharedModule } from "controls/shared/controls-shared.module";
import { InventorySharedModule } from "inventorySharedModule/inventory-shared.module";
import { VendorpediaRequestModule } from "vendorpediaRequest/vendorpedia-request.module";
import { VendorControlsModule } from "modules/vendor/vendor-controls/vendor-controls.module";
import { VendorRulesModule } from "modules/vendor/rules/vendor-rules.module";
import { CertificatesDetailsModule } from "modules/vendor/certificates/certificates-details.module";

// Services
import { VendorApiService } from "modules/vendor/services/api/vendor-api.service";
import { VendorRecordActionService } from "modules/vendor/services/actions/vendor-record-action.service";
import { VendorDashboardService } from "modules/vendor/services/shared/vendor-dashboard.service.ts";
import { VendorDetailActionService } from "modules/vendor/services/actions/vendor-detail-action.service";
import { VendorpediaAdapterService } from "modules/vendor/services/adapters/vendorpedia-adapter.service";
import { ContractService } from "contracts/shared/services/contract.service";
import { VendorInformationService } from "modules/vendor/services/shared/vendor-information.service";
import { ContractAttachmentsService } from "contracts/shared/services/contract-attachments.service";
import { InventoryService } from "inventorySharedModule/shared/services/inventory-api.service";

// Legacy Provider Injections
export function getLegacyNotificationService(injector: any): any {
    return injector.get("Notification");
}

const SERVICES = [
    VendorRecordActionService,
    VendorDetailActionService,
    VendorpediaAdapterService,
    VendorApiService,
    ContractService,
    VendorInformationService,
    ContractAttachmentsService,
    VendorDashboardService,
    InventoryService,
];

const LEGACY_SERVICES = [
    {
        provide: "Notification",
        deps: ["$injector"],
        useFactory: getLegacyNotificationService,
    },
];

@NgModule({
    imports: [
        SharedModule,
        CommonModule,
        HttpClientModule,
        FormsModule,
        ReactiveFormsModule,
        VitreusModule,
        DndListModule,
        OTPipesModule,
        VirtualScrollModule,
        OneVideoModule,
        ControlsSharedModule,
        ContractsModule,
        EngagementsModule,
        VendorSharedModule,
        FilterModule,
        VendorpediaExchangeModule,
        VendorpediaRequestModule,
        InventorySharedModule,
        VendorControlsModule,
        VendorRulesModule,
        CertificatesDetailsModule,
    ],
    exports: [
        VendorSharedModule,
        CertificatesDetailsModule,
    ],
    providers: [
        ...SERVICES,
        ...LEGACY_SERVICES,
        ...PIPES, // TODO: Remove from providers when ot-common is implemented. Only necessary if you are using pipe in TS file, else just module is fine
    ],
})
export class VendorModule { }
