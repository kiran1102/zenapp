// Core
import { Component, Input } from "@angular/core";

// Interfaces
import { IEngagementListViewInformation } from "engagements/shared/engagement.interface";

@Component({
    selector: "engagement-details",
    templateUrl: "./engagement-details.component.html",
})
export class EngagementDetailsComponent {

    @Input() engagement: IEngagementListViewInformation;

}
