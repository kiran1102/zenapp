import { NgModule } from "@angular/core";

// Modules
import { SharedModule } from "sharedModules/shared.module";
import { CommonModule } from "@angular/common";
import { HttpClientModule } from "@angular/common/http";
import { FormsModule } from "@angular/forms";
import { ReactiveFormsModule } from "@angular/forms";
import { VitreusModule } from "@onetrust/vitreus";
import { OTPipesModule } from "modules/pipes/pipes.module";
import { FilterModule } from "modules/filter/filter.module";
import { TranslateModule } from "@ngx-translate/core";
import { InventorySharedModule } from "modules/inventory/inventory-shared/inventory-shared.module";
import { EngagementSharedModule } from "engagements/shared/engagement-shared.module";
import { ContractListModule } from "modules/vendor/contracts/contract-list/contract-list.module";

// Components
import { EngagementDetailComponent } from "engagements/engagement-detail/engagement-detail.component";
import { EngagementDetailsComponent } from "engagements/engagement-detail/engagement-details/engagment-details.component";

// Services
import { EngagementDetailService } from "engagements/engagement-detail/engagement-detail.service";

@NgModule({
    imports: [
        SharedModule,
        CommonModule,
        HttpClientModule,
        FormsModule,
        ReactiveFormsModule,
        VitreusModule,
        OTPipesModule,
        FilterModule,
        TranslateModule,
        InventorySharedModule,
        EngagementSharedModule,
        ContractListModule,
    ],
    declarations: [
        EngagementDetailComponent,
        EngagementDetailsComponent,
    ],
    entryComponents: [
        EngagementDetailComponent,
    ],
    providers: [
        EngagementDetailService,
    ],
})
export class EngagementDetailModule {}
