import { Injectable } from "@angular/core";

// Rxjs
import { Observable } from "rxjs";
import { map } from "rxjs/operators";

// Services
import { ILabelValue } from "@onetrust/vitreus";
import { EngagementApiService } from "engagements/shared/engagement-api.service";

// Intefaces
import {
    IEngagementListViewInformation,
    IEngagementCertificateInformation,
    IEngagementInventoryInformation,
} from "engagements/shared/engagement.interface";

// Enums
import { EngagementType } from "engagements/shared/engagement.enum";
import { EngagementTypeOption } from "engagements/shared/engagement.constant";

@Injectable()
export class EngagementDetailService {

    constructor(
        private engagementApiService: EngagementApiService,
    ) {}

    fetchEngagementById(id: string): Observable<IEngagementListViewInformation> {
        return this.engagementApiService.fetchEngagementById(id).pipe(
            map((response) => response.result ? response.data : null),
        );
    }

    fetchEngagementTypes(): Observable<Array<ILabelValue<string, EngagementType>>> {
        return this.engagementApiService.fetchEngagementTypes().pipe(
            map((response) => response.result ? this.filterMapEngagmentTypesToOptions(response.data) : []),
        );
    }

    fetchCertificatesByEngagementId(id: string): Observable<IEngagementCertificateInformation[]> {
        return this.engagementApiService.fetchCertificatesByEngagementId(id).pipe(
            map((response) => response.result ? response.data : []),
        );
    }

    fetchContractsByEngagementId(id: string): Observable<string[]> {
        return this.engagementApiService.fetchContractsByEngagementId(id).pipe(
            map((response) => response.result ? response.data : []),
        );
    }

    fetchServicesByEngagementId(id: string): Observable<IEngagementInventoryInformation[]> {
        return this.engagementApiService.fetchServicesByEngagementId(id).pipe(
            map((response) => response.result ? response.data : []),
        );
    }

    private filterMapEngagmentTypesToOptions(types: EngagementType[]): Array<ILabelValue<string, EngagementType>> {
        if (!types || !types.length) return [];
        return types
            .filter((type) => type === EngagementType.VENDOR)
            .map((type) => EngagementTypeOption[type]);
    }

}
