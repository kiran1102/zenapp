// Core
import { Component } from "@angular/core";
import { StateService } from "@uirouter/core";

// Rxjs
import {
    Subject,
    combineLatest,
} from "rxjs";

import { takeUntil } from "rxjs/operators";

// Services
import { TranslateService } from "@ngx-translate/core";
import { EngagementDetailService } from "engagements/engagement-detail/engagement-detail.service";

// Interfaces
import { ILabelValue } from "@onetrust/vitreus";
import { ITabsNav } from "interfaces/one-tabs-nav.interface";
import { IEngagementListViewInformation } from "engagements/shared/engagement.interface";

// Constants / Enums
import { EngagementTabOptions } from "engagements/engagement-detail/engagement-detail.enum";
import { EngagementType } from "engagements/shared/engagement.enum";
import { ContractListContext } from "contracts/contract-list/contract-list.enum";

@Component({
    selector: "engagement-detail",
    templateUrl: "./engagement-detail.component.html",
})
export class EngagementDetailComponent {

    currentTab = this.stateService.params.currentTab || EngagementTabOptions.Details;
    contractListContext = ContractListContext;
    engagement: IEngagementListViewInformation;
    engagementId = this.stateService.params.id;
    engagementTypes: Array<ILabelValue<string, EngagementType>>;
    engagementTabOptions = EngagementTabOptions;
    isLoading = true;
    model: string;
    tabOptions: ITabsNav[];
    translationKeys = [
        "Details",
        "Contracts",
        "Certificates",
        "Services",
    ];

    private destroy$ = new Subject();

    constructor(
        private stateService: StateService,
        private translateService: TranslateService,
        private engagementDetailService: EngagementDetailService,
    ) { }

    ngOnInit(): void {
        if (!this.engagementId) {
            this.stateService.go("zen.app.pia.module.vendor.engagements");
        } else {
            combineLatest(
                this.translateService.stream(this.translationKeys),
                this.engagementDetailService.fetchEngagementById(this.engagementId),
            ).pipe(
                takeUntil(this.destroy$),
            ).subscribe(([translations, engagement]) => {
                if (translations && engagement) {
                    this.tabOptions = this.setTabOptions(translations);
                    this.engagement = engagement;
                }
                this.isLoading = false;
            });
        }
    }

    ngOnDestroy(): void {
        this.destroy$.next();
        this.destroy$.complete();
    }

    changeTab(option: EngagementTabOptions): void {
        this.currentTab = option;
    }

    goToRoute(route: { path: string, params: any }): void {
        this.stateService.go(route.path);
    }

    setTabOptions(translations: ILabelValue<string>): ITabsNav[] {
        const tabs = [];
        tabs.push({
            text: translations["Details"],
            id: EngagementTabOptions.Details,
            otAutoId: "EngagementDetailTabDetails",
        });
        tabs.push({
            text: translations["Contracts"],
            id: EngagementTabOptions.Contracts,
            otAutoId: "EngagementDetailTabContracts",
        });
        tabs.push({
            text: translations["Services"],
            id: EngagementTabOptions.Services,
            otAutoId: "EngagementDetailTabServices",
        });
        tabs.push({
            text: translations["Certificates"],
            id: EngagementTabOptions.Certificates,
            otAutoId: "EngagementDetailTabCertificates",
        });
        return tabs;
    }
}
