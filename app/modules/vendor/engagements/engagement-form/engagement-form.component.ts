// Angular
import { Component, EventEmitter, Output } from "@angular/core";
import {
    FormBuilder,
    FormGroup,
    Validators,
} from "@angular/forms";

// rxjs
import { Subject } from "rxjs/Subject";
import { takeUntil } from "rxjs/operators";

// Services
import { TimeStamp } from "oneServices/time-stamp.service";

// Interfaces
import {
    ILabelValue,
    IDateTimeOutputModel,
} from "@onetrust/vitreus";
import {
    IRelatedListOptionData,
    IRelatedListOption,
} from "modules/vendor/shared/interfaces/vendor-upload-document.interface";
import { IOtOrgUserOutput } from "sharedModules/components/ot-org-user/ot-org-user.interface";
import { IInventoryMetadata } from "engagements/shared/engagement.interface";

// Enums & Constants
import { InventoryTableIds } from "enums/inventory.enum";
import { EngagementType } from "engagements/shared/engagement.enum";
import {
    EngagementTypeOption,
    EngagementTypeOptions,
} from "engagements/shared/engagement.constant";
import { Regex } from "constants/regex.constant";

@Component({
    selector: "engagement-form",
    templateUrl: "./engagement-form.component.html",
})
export class EngagementFormComponent {

    @Output() formValueChanged = new EventEmitter();
    @Output() formStatusChanged = new EventEmitter();

    form: FormGroup;
    disableUntilDate = this.timeStamp.getDisableUntilDate();
    format = this.timeStamp.getDatePickerDateFormat();
    is24Hour = this.timeStamp.getIs24Hour();
    internalOwner: string;
    inventoryTableIds = InventoryTableIds;
    engagementTypeOption = EngagementTypeOption;
    engagementTypeOptions = EngagementTypeOptions;
    vendorSelection: IRelatedListOption;

    private destroy$ = new Subject();

    constructor(
        private timeStamp: TimeStamp,
        private formBuilder: FormBuilder,
    ) { }

    ngOnInit() {
        this.form = this.createNewForm();
        this.form.valueChanges.pipe(
            takeUntil(this.destroy$),
        ).subscribe((form) => {
            if (form.services) {
                form.services = form.services.map((service) => {
                    return {
                        id: service.id,
                        name: service.name,
                        orgGroupId: service.organization.id,
                    };
                });
            }
            this.formValueChanged.emit(form);
        });
        this.form.statusChanges.pipe(
            takeUntil(this.destroy$),
        ).subscribe((status) => {
            this.formStatusChanged.emit(status);
        });
    }

    ngOnDestroy() {
        this.destroy$.next();
        this.destroy$.complete();
    }

    selectType(event: ILabelValue<EngagementType>) {
        this.form.get("type").setValue(event.value);
    }

    selectStartDate(event: IDateTimeOutputModel) {
        this.form.get("startDate").setValue(event.dateTime);
    }

    selectEndDate(event: IDateTimeOutputModel) {
        this.form.get("endDate").setValue(event.dateTime);
    }

    selectVendor({ options }: IRelatedListOptionData) {
        this.vendorSelection = options ? options[0] : null;
        const vendor: IInventoryMetadata = this.vendorSelection ? {
            id: this.vendorSelection.id,
            name: this.vendorSelection.name,
            label: this.vendorSelection.label,
            orgGroupId: this.vendorSelection.organization.id,
        } : null;
        this.form.get("vendor").setValue(vendor);
    }

    selectServices(event: IRelatedListOptionData) {
        // TODO: Need to update ot-inventory-lookup to support inventory relations
        this.form.get("services").setValue(event.options);
    }

    internalOwnerSelected(event: IOtOrgUserOutput) {
        this.internalOwner = event.selection.Id;
        const isEmail = event.selection.Id.match(Regex.EMAIL);
        const isGuid = event.selection.Id.match(Regex.GUID);
        this.form.get("internalOwnerId").setValue(isGuid && !isEmail ? event.selection.Id : null);
        this.form.get("internalOwnerEmail").setValue(isEmail ? event.selection.Id : null);
    }

    private createNewForm(): FormGroup {
        return this.formBuilder.group({
            name: [null, [Validators.required]],
            type: [EngagementTypeOptions[0].value, [Validators.required]],
            startDate: [null],
            endDate: [null],
            vendor: [null, [Validators.required]],
            services: [null],
            internalOwnerId: [null],
            internalOwnerEmail: [null],
            externalContact: [null],
            notes: [null],
        });
    }
}
