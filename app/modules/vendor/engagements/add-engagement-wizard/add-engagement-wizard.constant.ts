import { AddEngagmentStates } from "engagements/add-engagement-wizard/add-engagement-wizard.enum";

export const AddEngagmentStateDetails = {
    [AddEngagmentStates.ADD_NEW_ENGAGEMENT]: {
        title: "AddNewEngagement",
        description: "AddNewEngagement",
    },
    [AddEngagmentStates.ATTACH_CONTRACT]: {
        title: "AttachContract",
        description: "AttachContract",
    },
    [AddEngagmentStates.SCOPE_STATEMENT]: {
        title: "ScopeStatementSelection",
        description: "ScopeStatementSelection",
    },
};
