export enum AddEngagmentStates {
    ADD_NEW_ENGAGEMENT,
    ATTACH_CONTRACT,
    SCOPE_STATEMENT,
}

export enum ProgressIndicatorStates {
    COMPLETED,
    ACTIVE,
    ERROR,
}
