// Angular
import { Component } from "@angular/core";
import { FormGroup } from "@angular/forms";

// rxjs
import { BehaviorSubject } from "rxjs/internal/BehaviorSubject";
import { Subject } from "rxjs/Subject";
import {
    takeUntil,
} from "rxjs/operators";
import {
    combineLatest,
} from "rxjs";

// Services
import { VendorApiService } from "modules/vendor/services/api/vendor-api.service";
import { TranslateService } from "@ngx-translate/core";
import { StateService } from "@uirouter/core";
import { EngagementApiService } from "engagements/shared/engagement-api.service";
import { EngagementAttachContractService } from "engagements/shared/engagement-contract-formdata.service";

// Interface
import {
    ICreateData,
    IContractAttachment,
} from "modules/vendor/shared/interfaces/vendor-upload-document.interface";

// Enums & Constants
import {
    ProgressIndicatorStates,
    AddEngagmentStates,
} from "engagements/add-engagement-wizard/add-engagement-wizard.enum";
import { AddEngagmentStateDetails } from "engagements/add-engagement-wizard/add-engagement-wizard.constant";
import {
    ProgressIndicatorLabels,
    ProgressIndicatorState,
} from "@onetrust/vitreus";
import { AddAttachmentFormControlName } from "engagements/shared/engagement.enum";

// Interfaces
import {
    IEngagementCreateRequest,
    IEngagementWizardDetails,
} from "engagements/shared/engagement.interface";
import { EngagementTabOptions } from "engagements/engagement-detail/engagement-detail.enum";
import { IProtocolResponse } from "interfaces/protocol.interface";
import { IAttachmentFile } from "interfaces/attachment-file.interface";

@Component({
    selector: "add-engagement-wizard",
    templateUrl: "./add-engagement-wizard.component.html",
})
export class AddEngagementWizardComponent {
    addEngagementPayload: IEngagementCreateRequest;
    engagementDetails: IEngagementWizardDetails;
    addEngagmentStates = AddEngagmentStates;
    currentState = AddEngagmentStates.ADD_NEW_ENGAGEMENT;
    labels: ProgressIndicatorLabels;
    isLoading = false;
    progressState$ = new BehaviorSubject<ProgressIndicatorState[]>([]);
    stateChange$ = new Subject<AddEngagmentStates>();
    stateDetails = AddEngagmentStateDetails;
    formData: ICreateData[] = [];
    formDataWithAdvOpt: ICreateData[] = [];
    isButtonDisabled = true;
    contractAttachmentForm: FormGroup;
    contractAttachedFiles: IAttachmentFile[];

    private destroy$ = new Subject();
    private progressStates: ProgressIndicatorState[] = [];
    private attachmentResponse: IContractAttachment[];
    private translations: {};
    private translationKeys = [
        "AddNewEngagement",
        "AttachContract",
        "ScopeStatementSelection",
        "Completed",
        "Active",
        "Progress",
        "ERROR",
    ];

    constructor(
        private stateService: StateService,
        private translate: TranslateService,
        private engagementApiService: EngagementApiService,
        private engagementAttachContractService: EngagementAttachContractService,
        private VendorApi: VendorApiService,
    ) { }

    ngOnInit() {
        this.configureTranslations();
        this.setupStateChangeListener();
        this.engagementAttachContractService.generateFormData();
        combineLatest(
            this.engagementAttachContractService.formData$,
            this.engagementAttachContractService.formDataWithAdvOpt$,
        ).pipe(
            takeUntil(this.destroy$),
        ).subscribe(([formData, formDataWithAdvOpt]) => {
            if (formData && formDataWithAdvOpt) {
                this.formData = formData;
                this.formDataWithAdvOpt = formDataWithAdvOpt;
            }
        });
    }

    ngOnDestroy() {
        this.destroy$.next();
        this.destroy$.complete();
    }

    goToRoute(route: { path: string, params: any }) {
        this.stateService.go(route.path);
    }

    cancel() {
        // TODO: Confirmation modal about losing changes
        this.stateService.go("zen.app.pia.module.vendor.engagements");
    }

    addEngagement() {
        this.isLoading = true;
        // TODO: Update payload with Contract & Certificates data
        this.addEngagementPayload = {
            ...this.engagementDetails,
            contract: null,
            certificates: null,
        };
        this.engagementApiService.addEngagment(this.addEngagementPayload).subscribe((res) => {
            if (res.result) {
                this.stateService.go("zen.app.pia.module.vendor.engagement-detail", { id: res.data, currentTab: EngagementTabOptions.Details });
            } else {
                // TODO: Route to page with issue on Error
            }
            this.isLoading = false;
        });
    }

    engagementFormChanged(event) {
        this.engagementDetails = event;
    }

    engagementFormStatusChanged(event) {
        // TODO: Handle Next button disabling based on form statuses
    }

    contractAttachmentFormData(event: { contractForm: FormGroup, canAdd: boolean }) {
        // TODO - use contractForm for request payload
        this.isButtonDisabled = !event.canAdd;
        this.contractAttachmentForm = event.contractForm;
        this.contractAttachedFiles = this.contractAttachmentForm.value.filesUpload;
    }

    generateAttachments() {
        if (this.contractAttachedFiles && this.contractAttachedFiles.length) {
            const promiseArr: Array<ng.IPromise<IProtocolResponse<IAttachmentFile>>> = [];
            this.contractAttachedFiles.forEach((file: IAttachmentFile) => {
                const fileData: IAttachmentFile = {
                    Comments: file.Comments,
                    FileName: `${file.FileName}.${file.Extension}`,
                    Extension: file.Extension,
                    Blob: file.Blob,
                    IsInternal: true,
                    Name: file.FileName,
                    RequestId: this.contractAttachmentForm.value.contractType ? this.contractAttachmentForm.value.contractType.id : null,
                    AttachmentRefId: file.Id,
                };
                promiseArr.push(this.VendorApi.uploadDocument(fileData));
            });
            Promise.all(promiseArr)
                .then((responses: Array<IProtocolResponse<IAttachmentFile>>): void => {
                    this.attachmentResponse = [];
                    if (Array.isArray(responses) && responses.length) {
                        const failedResponses: Array<IProtocolResponse<IAttachmentFile>> = responses.filter((response) => !response.result);
                        if (!failedResponses.length) {
                            responses.forEach((res: IProtocolResponse<IAttachmentFile>): void => {
                                const attachmentObjectForComment: IContractAttachment = {
                                    attachmentId: res.data.Id,
                                    fileName: res.data.FileName,
                                    attachmentDescription: res.data.Comments,
                                };
                                this.attachmentResponse.push(
                                    attachmentObjectForComment,
                                );
                            },
                            );
                        }
                    }
                });
            // use attachmentResponse object for adding the uploaded files in contract attachment in the request payload.
        }
    }

    private configureTranslations() {
        this.translate.stream(this.translationKeys).subscribe((translations) => {
            this.translations = translations;
            this.progressStates = [
                { text: this.translations["AddNewEngagement"], state: ProgressIndicatorStates.ACTIVE },
                { text: this.translations["AttachContract"], state: null },
                { text: this.translations["ScopeStatementSelection"], state: null },
            ];
            this.labels = {
                completed: this.translations["Completed"],
                active: this.translations["Active"],
                progress: this.translations["Progress"],
                error: this.translations["ERROR"],
            };
            this.progressState$.next(this.progressStates);
        });
    }

    private setupStateChangeListener() {
        this.stateChange$.pipe(
            takeUntil(this.destroy$),
        ).subscribe((state: AddEngagmentStates) => {
            this.currentState = state;
            this.progressState$.next(this.setProgressIndicator(state));
            if (state === 1) {
                this.formData.forEach((item) => {
                    if (item.formControlName === AddAttachmentFormControlName.PRIMARY_VENDOR) {
                        item.inputValue = this.engagementDetails.vendor;
                    }
                });
            }
        });
    }

    private setProgressIndicator(state: AddEngagmentStates): ProgressIndicatorState[] {
        return this.progressStates.map((s, i) => {
            s.state = i < state ? ProgressIndicatorStates.COMPLETED : i === state ? ProgressIndicatorStates.ACTIVE : null;
            return s;
        });
    }
}
