// Angular
import { Component, Input } from "@angular/core";

// Services
import { CertificatesApiService } from "modules/vendor/certificates/shared/services/certificates-api.service";
import { CertificatesListService } from "modules/vendor/certificates/shared/services/certificates-list.service";

// Interfaces
import {
    IEngagementWizardDetails,
    IInventoryMetadata,
} from "engagements/shared/engagement.interface";

// Enums & Constants
import { CertificateContext } from "modules/vendor/certificates/shared/enums/certificates.enums";
import { GetLogoNameForCert } from "modules/vendor/certificates/shared/constants/certificates.constants";
import { EngagementScopeStatus } from "engagements/shared/engagement.enum";

@Component({
    selector: "engagement-scope-statement",
    templateUrl: "./engagement-scope-statement.component.html",
})
export class EngagementScopeStatementComponent {

    @Input() engagementDetails: IEngagementWizardDetails;

    certificates$ = this.certificatesListService.certificates$;
    hasNoCertificates$ = this.certificatesListService.hasNoCertificates$;

    vendor: IInventoryMetadata;
    selectedServices: IInventoryMetadata[] = [];
    selectedService: IInventoryMetadata;
    scopeStatus = EngagementScopeStatus;
    isLoading = true;

    constructor(
        private certificatesApiService: CertificatesApiService,
        private certificatesListService: CertificatesListService,
    ) {}

    ngOnInit() {
        if (this.engagementDetails) {
            const { vendor, services } = this.engagementDetails;
            this.selectedServices.push({...vendor, type: CertificateContext.Vendors});
            if (services && services.length) {
                services.forEach((s) => this.selectedServices.push({...s, type: CertificateContext.Assets}));
            }
            this.selectedService = this.selectedServices[0];
            this.loadCertificates();
        }
    }

    onAddNote(certificate: any) {
        certificate.showNotes = true;
    }

    onScopeSelect(event, certificate) {
        certificate.scopeStatus = event;
    }

    selectService(service: IInventoryMetadata) {
        this.selectedService = service;
        this.loadCertificates();
    }

    private loadCertificates() {
        if (this.selectedService !== null) {
            this.certificatesApiService.getCertificates(this.selectedService.type, this.selectedService.id)
            .subscribe((certificatesResponse) => {
                if (certificatesResponse.result) {
                    let certificates = certificatesResponse.data.data;
                    certificates = certificates.map((c) => ({
                        ...c,
                        scopeStatus: this.scopeStatus.PENDING.toString(),
                        logoUrl: `images/vendor/certificates/${GetLogoNameForCert(c.logoName || c.certificateName)}.png`,
                }));
                    this.certificatesListService.setCertificates(certificates);
                }
                this.isLoading = false;
            });
        }
    }
}
