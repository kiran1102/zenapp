import { NgModule } from "@angular/core";

// Modules
import { SharedModule } from "sharedModules/shared.module";
import { CommonModule } from "@angular/common";
import { HttpClientModule } from "@angular/common/http";
import { FormsModule } from "@angular/forms";
import { ReactiveFormsModule } from "@angular/forms";
import { VitreusModule } from "@onetrust/vitreus";
import { OTPipesModule } from "modules/pipes/pipes.module";
import { FilterModule } from "modules/filter/filter.module";
import { TranslateModule } from "@ngx-translate/core";
import { InventorySharedModule } from "modules/inventory/inventory-shared/inventory-shared.module";
import { EngagementDetailModule } from "engagements/engagement-detail/engagement-detail.module";
import { EngagementListModule } from "engagements/engagement-list/engagement-list.module";
import { ContractsSharedModule } from "contracts/shared/contracts-shared.module";

// Components
import { AddEngagementWizardComponent } from "engagements/add-engagement-wizard/add-engagement-wizard.component";
import { EngagementFormComponent } from "engagements/engagement-form/engagement-form.component";
import { EngagementScopeStatementComponent } from "engagements/engagement-scope-statement/engagement-scope-statement.component";

// Services
import { EngagementApiService } from "engagements/shared/engagement-api.service";
import { EngagementAttachContractService } from "engagements/shared/engagement-contract-formdata.service";

@NgModule({
    imports: [
        SharedModule,
        CommonModule,
        HttpClientModule,
        FormsModule,
        ReactiveFormsModule,
        VitreusModule,
        OTPipesModule,
        FilterModule,
        TranslateModule,
        InventorySharedModule,
        EngagementDetailModule,
        EngagementListModule,
        ContractsSharedModule,
    ],
    declarations: [
        AddEngagementWizardComponent,
        EngagementFormComponent,
        EngagementScopeStatementComponent,
    ],
    entryComponents: [
        AddEngagementWizardComponent,
    ],
    providers: [
        EngagementApiService,
        EngagementAttachContractService,
    ],
})
export class EngagementsModule {}
