import {
    EngagementType,
    EngagementScopeStatus,
} from "modules/vendor/engagements/shared/engagement.enum";
import { IContractDetails } from "modules/vendor/shared/interfaces/vendor-upload-document.interface";

export interface IInventoryMetadata {
    id: string;
    name: string;
    orgGroupId: string;
    label?: string;
    type?: string;
}

export interface IEngagementInventoryInformation {
    id: string;
    inventoryId: string;
    inventoryName: string;
    inventoryTypeId: number;
    orgGroupId: string;
}

export interface IEngagementDetails {
    endDate: string;
    externalContact: string;
    internalOwnerEmail: string;
    internalOwnerId: string;
    name: string;
    notes: string;
    startDate: string;
    type: EngagementType;
    vendor: IInventoryMetadata;
}

export interface IEngagementListViewInformation extends IEngagementDetails {
    engagementId: string;
    internalOwnerUser: string;
    isSelected?: boolean;
}

export interface IEngagementWizardDetails extends IEngagementDetails {
    services: IInventoryMetadata[];
}

export interface IEngagementCreateRequest extends IEngagementWizardDetails {
    // TODO: update certificates once API completed
    certificates: any[];
    contract: IContractDetails;
}

export interface IEngagementCertificateInformation {
    id: string;
    inventoryCertificateId: string;
    notes: string;
    scopeStatus: EngagementScopeStatus;
}
