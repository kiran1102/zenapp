import { NgModule } from "@angular/core";

// Modules
import { TranslateModule } from "@ngx-translate/core";

// Services
import { EngagementApiService } from "engagements/shared/engagement-api.service";

@NgModule({
    imports: [
        TranslateModule,
    ],
    declarations: [],
    entryComponents: [],
    providers: [
        EngagementApiService,
    ],
})
export class EngagementSharedModule {}
