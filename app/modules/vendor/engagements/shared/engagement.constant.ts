import { EngagementType } from "engagements/shared/engagement.enum";

export const EngagementTypeOption = {
    [EngagementType.VENDOR]: { label: "VendorEngagement", value: EngagementType.VENDOR },
    // [EngagementType.SERVICES]: { label: "ServicesEngagement", value: EngagementType.SERVICES },
};

export const EngagementTypeOptions = [
    EngagementTypeOption[EngagementType.VENDOR],
    // EngagementTypeOption[EngagementType.SERVICES],
];
