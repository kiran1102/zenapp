export enum EngagementType {
    VENDOR = "VendorEngagement",
    // SERVICES = "ServicesEngagement",
}

export enum AddAttachmentFormControlName {
    PRIMARY_VENDOR = "primaryVendor",
    CONTRACT_NAME = "nameOfContract",
    CONTRACT_TYPE = "contractType",
    CREATED_DATE = "createdDate",
    EXPIRATION_DATE = "expirationDate",
    CONTRACT_STATUS = "contractStatus",
    CONTRACT_URL = "contractURL",
    RELATED_ASSETS = "relatedAssetsLookUp",
    PROCESSING_ACTIVITIES = "relatedProcessingActivitiesLookUp",
    RELATED_VENDORS = "relatedVendorsLookUp",
    APPROVED_DATE = "approvedDate",
    CONTRACT_OWNER = "contractOwner",
    VENDOR_CONTACT = "vendorContact",
    APPROVERS = "approvers",
    CURRENCY = "currency",
    COST = "cost",
    TEXTAREA = "textArea",
}

export enum EngagementScopeStatus {
    IN_SCOPE = "InScope",
    OUT_OF_SCOPE = "OutOfScope",
    PENDING = "Pending",
}
