// Core
import { Injectable } from "@angular/core";

// rxjs
import { Subject } from "rxjs/Subject";
import { takeUntil } from "rxjs/operators";

// Services
import { TimeStamp } from "oneServices/time-stamp.service";
import { ContractService } from "contracts/shared/services/contract.service";
import { ContractsApiService } from "modules/vendor/contracts/shared/services/contracts-api.service";
import { TranslateService } from "@ngx-translate/core";

// Interface
import { ICreateData } from "modules/vendor/shared/interfaces/vendor-upload-document.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";
import { ILabelValue } from "interfaces/generic.interface";
import {
    IContractTypeResponse,
    ICurrenciesResponse,
} from "modules/vendor/shared/interfaces/vendor-upload-document.interface";

// Pipes
import { PermitPipe } from "modules/pipes/permit.pipe";

// Enums & Constants
import { FormDataTypes } from "constants/form-types.constant";
import { InventoryTableIds } from "enums/inventory.enum";
import { OrgUserTraversal } from "enums/org-user-dropdown.enum";
import { AddAttachmentFormControlName } from "engagements/shared/engagement.enum";

// Interfaces
import { IEngagementDetails } from "engagements/shared/engagement.interface";

@Injectable()
export class EngagementAttachContractService {

    formData = new Subject<ICreateData[]>();
    formData$ = this.formData.asObservable();
    formDataWithAdvOpt = new Subject<ICreateData[]>();
    formDataWithAdvOpt$ = this.formDataWithAdvOpt.asObservable();
    engagementDetails: IEngagementDetails;
    formDataTypes = FormDataTypes;
    defaultLookupModel = { id: null, name: null, nameKey: null };
    inventoryTableIds = InventoryTableIds;
    format = this.timeStamp.getDatePickerDateFormat();
    orgUserTraversal = OrgUserTraversal;
    statusList: Array<ILabelValue<string>>;
    typeList: IContractTypeResponse[];
    currencyList: ICurrenciesResponse[];

    private destroy$ = new Subject();

    constructor(
        private timeStamp: TimeStamp,
        private permitPipe: PermitPipe,
        private contractService: ContractService,
        private contractsApiService: ContractsApiService,
        private translate: TranslateService,
    ) {}

    ngOnDestroy() {
        this.destroy$.next();
        this.destroy$.complete();
    }

    async generateFormData() {
        this.contractService.retrieveContractStatuses();
        await this.getContractTypes();
        await this.getAllCurrencies();
        await this.contractService.contractStatuses$.pipe(
            takeUntil(this.destroy$),
        ).subscribe((statuses: Array<ILabelValue<string>>) => {
            if (statuses) {
                this.statusList = statuses.map((item) => {
                    return { label: this.translate.instant(item.label), value: item.value };
                });
            }
        });
        this.formData.next([
            {
                name: "addContractFilesUpload",
                type: FormDataTypes.FILES_UPLOAD,
                isRequired: false,
                id: "AddContractFilesUpload",
                formControlName: "filesUpload",
            },
            {
                label: "PrimaryVendor",
                name: "primaryVendor",
                type: FormDataTypes.INPUT,
                isRequired: true,
                formControlName: AddAttachmentFormControlName.PRIMARY_VENDOR,
                id: "AddContractPrimayVendor",
                inputValue: null,
                disable: true,
            },
            {
                label: "ContractName",
                name: "contractName",
                type: FormDataTypes.INPUT,
                isRequired: true,
                formControlName: AddAttachmentFormControlName.CONTRACT_NAME,
                id: "AddContractName",
                placeholder: "TextInput",
            },
            {
                labelKey: "name",
                valueKey: "nameKey",
                name: "contractType",
                label: "Type",
                type: FormDataTypes.PICKLIST,
                isRequired: true,
                formControlName: AddAttachmentFormControlName.CONTRACT_TYPE,
                options: this.typeList,
                id: "AddContolsContractTypePicklist",
                placeholder: "Select",
            },
            {
                label: "DateCreated",
                name: "addControlsDateCreated",
                type: FormDataTypes.DATE,
                isRequired: true,
                formControlName: AddAttachmentFormControlName.CREATED_DATE,
                format: this.format,
                hideTimePicker: true,
                dateDisabled: false,
                timeDisabled: true,
                id: "AddControlsDateCreated",
                placeholder: "SelectDate",
            },
            {
                label: "ExpirationDate",
                name: "addControlsExpirationDate",
                type: FormDataTypes.DATE,
                isRequired: false,
                formControlName: AddAttachmentFormControlName.EXPIRATION_DATE,
                format: this.format,
                hideTimePicker: true,
                dateDisabled: false,
                timeDisabled: true,
                id: "AddControlsExpirationDate",
                placeholder: "SelectDate",
            },
            {
                labelKey: "label",
                valueKey: "value",
                name: "addControlsContractStatus",
                label: "Status",
                type: FormDataTypes.PICKLIST,
                isRequired: true,
                formControlName: AddAttachmentFormControlName.CONTRACT_STATUS,
                options: this.statusList,
                id: "AddContolsContractStatusPicklist",
                placeholder: "Select",
            },
            {
                label: "ContractUrl",
                name: "addControlsContractURL",
                type: FormDataTypes.INPUT,
                isRequired: false,
                formControlName: AddAttachmentFormControlName.CONTRACT_URL,
                placeholder: "TextInput",
                id: "AddContractURL",
            },
            {
                name: "",
                label: "",
                type: FormDataTypes.TOGGLE,
                isRequired: false,
                formControlName: "",
                id: "AddContractFormToggle",
            },
        ]);

        this.formDataWithAdvOpt.next([
            {
                label: "RelatedAssets",
                name: "addControlsRelatedAssets",
                type: FormDataTypes.LOOKUP,
                isRequired: false,
                formControlName: AddAttachmentFormControlName.RELATED_ASSETS,
                id: "AddContractRelatedAssetsLookup",
                inputValue: this.defaultLookupModel,
                isMultiSelectable: true,
                inventoryType: this.inventoryTableIds.Assets,
                placeholder: "ChooseAnAsset",
                permission: this.permitPipe.transform("VendorContractInventoryLink"),
            },
            {
                label: "RelatedProcessingActivites",
                name: "addControlsRelatedProcessingActivites",
                type: FormDataTypes.LOOKUP,
                isRequired: false,
                formControlName: AddAttachmentFormControlName.PROCESSING_ACTIVITIES,
                id: "AddContractRelatedProcessingActivitiesLookup",
                inputValue: this.defaultLookupModel,
                isMultiSelectable: true,
                inventoryType: this.inventoryTableIds.Processes,
                placeholder: "ChooseProcessingActivities",
                permission: this.permitPipe.transform("VendorContractInventoryLink"),
            },
            {
                label: "RelatedVendors",
                name: "addControlsRelatedVendors",
                type: FormDataTypes.LOOKUP,
                isRequired: false,
                formControlName: AddAttachmentFormControlName.RELATED_VENDORS,
                id: "AddContractRelatedVendorsLookup",
                inputValue: this.defaultLookupModel,
                isMultiSelectable: true,
                inventoryType: this.inventoryTableIds.Vendors,
                placeholder: "ChooseVendor",
                permission: this.permitPipe.transform("VendorContractInventoryLink"),
            },
            {
                label: "ApprovedDate",
                name: "addControlsApprovedDate",
                type: FormDataTypes.DATE,
                isRequired: false,
                formControlName: AddAttachmentFormControlName.APPROVED_DATE,
                format: this.format,
                hideTimePicker: true,
                dateDisabled: false,
                timeDisabled: true,
                id: "AddControlsApprovedDate",
                placeholder: "SelectDate",
            },
            {
                label: "ContractOwner",
                name: "addControlsContractOwner",
                type: FormDataTypes.ORGANIZATION,
                isRequired: false,
                formControlName: AddAttachmentFormControlName.CONTRACT_OWNER,
                allowEmail: true,
                fetchOptionsOnClick: true,
                traversal: OrgUserTraversal.Up,
                id: "AddControlsContractOwner",
                placeholder: "SelectContractOwner",
            },
            {
                label: "VendorContact",
                name: "addControlsVendorContact",
                type: FormDataTypes.INPUT,
                isRequired: false,
                formControlName: AddAttachmentFormControlName.VENDOR_CONTACT,
                id: "AddControlsVendorContact",
                placeholder: "TextInput",
            },
            {
                label: "Approvers/Signers",
                name: "addControlsApprovers",
                type: FormDataTypes.INPUT,
                isRequired: false,
                formControlName: AddAttachmentFormControlName.APPROVERS,
                id: "AddControlsApprovers",
                placeholder: "TextInput",
            },
            {
                label: "Cost",
                name: "addControlsCost",
                type: FormDataTypes.CURRENCY,
                isRequired: false,
                requiredErrorMessage: "TextInput",
                costFormControlName: AddAttachmentFormControlName.COST,
                currencyFormControlName: AddAttachmentFormControlName.CURRENCY,
                id: "AddControlsCost",
                precision: "14",
                scale: "2",
                currencyId: "AddControlsCurrency",
                currencyFieldname: "addControlsCurrency",
                options: this.currencyList,
                costPlaceholder: "EnterTheValueOfThisContract",
                currencyPlaceholder: "Select",
            },
            {
                label: "Notes",
                name: "addControlsNotes",
                type: FormDataTypes.TEXTAREA,
                isRequired: false,
                formControlName: AddAttachmentFormControlName.TEXTAREA,
                id: "AddContractNotesTextArea",
                placeholder: "Notes",
            },
        ]);
    }

    private getContractTypes(): ng.IPromise<IProtocolResponse<IContractTypeResponse[]>> {
        return this.contractsApiService.fetchAllContractTypes().then(
            (response: IProtocolResponse<IContractTypeResponse[]>): IProtocolResponse<IContractTypeResponse[]> => {
                if (response.result) {
                    this.typeList = response.data;
                    this.typeList.map((contractType: IContractTypeResponse) => {
                        return contractType.name = contractType.nameKey ? this.translate.instant(contractType.nameKey) : contractType.name;
                    });
                    return response;
                }
            });
    }

    private getAllCurrencies(): ng.IPromise<IProtocolResponse<ICurrenciesResponse[]>> {
        return this.contractsApiService.fetchAllCurrencies().then(
            (response: IProtocolResponse<ICurrenciesResponse[]>): IProtocolResponse<ICurrenciesResponse[]> => {
                if (response.result) {
                    this.currencyList = response.data;
                    return response;
                }
            });
    }
}
