// Core
import { Injectable } from "@angular/core";

// RxJs
import { Observable, from } from "rxjs";

// Interfaces
import {
    IProtocolResponse,
    IProtocolConfig,
    IProtocolMessages,
} from "interfaces/protocol.interface";
import {
    IEngagementCreateRequest,
    IEngagementListViewInformation,
    IEngagementCertificateInformation,
    IEngagementInventoryInformation,
} from "modules/vendor/engagements/shared/engagement.interface";
import { IGridPaginationParams } from "interfaces/grid.interface";
import { IPageableOf } from "interfaces/pagination.interface";

// Services
import { ProtocolService } from "modules/core/services/protocol.service";
import { TranslateService } from "@ngx-translate/core";

// Enums & Constants
import { EngagementType } from "engagements/shared/engagement.enum";

@Injectable()
export class EngagementApiService {

    translations = {};
    translationKeys = [
        "SuccessCreatingEngagement",
        "EngagementGetError",
        "ErrorCreatingEngagement",
        "ErrorFetchingEngagementTypes",
        "ErrorFetchingEngagement",
        "Vendorpedia.Error.FetchServiceForVendor",
        "ErrorRetrievingContractAttachments",
        "ErrorFetchingCertificates",
    ];

    constructor(
        private protocolService: ProtocolService,
        private translate: TranslateService,
    ) {
        this.translate.stream(this.translationKeys).subscribe((t) => {
            this.translations = t;
        });
    }

    addEngagment(payload: IEngagementCreateRequest): Observable<IProtocolResponse<string>> {
        const config: IProtocolConfig = this.protocolService.customConfig("POST", "/api/vendor/v1/engagements", null, payload);
        const messages: IProtocolMessages = {
            Success: { custom: this.translations["SuccessCreatingEngagement"] },
            Error: { custom: this.translations["ErrorCreatingEngagement"] },
        };
        return from(this.protocolService.http(config, messages));
    }

    fetchEngagementList(params: IGridPaginationParams, fullText: string, payload?: any): Observable<IProtocolResponse<IPageableOf<IEngagementListViewInformation>>> {
        const requestBody = {
            fullText,
            filters: [...payload],
        };
        const config: IProtocolConfig = this.protocolService.customConfig("POST", "/api/vendor/v1/engagements/pages", params, requestBody);
        const messages: IProtocolMessages = {
            Error: { custom: this.translations["EngagementGetError"] },
        };
        return from(this.protocolService.http(config, messages));
    }

    fetchEngagementTypes(): Observable<IProtocolResponse<EngagementType[]>> {
        const config: IProtocolConfig = this.protocolService.customConfig("GET", "/api/vendor/v1/engagements/types");
        const messages: IProtocolMessages = { Error: { custom: this.translations["ErrorFetchingEngagementTypes"] } };
        return from(this.protocolService.http(config, messages));
    }

    fetchEngagementById(id: string): Observable<IProtocolResponse<IEngagementListViewInformation>> {
        const config: IProtocolConfig = this.protocolService.customConfig("GET", `/api/vendor/v1/engagements/${id}/engagements`);
        const messages: IProtocolMessages = { Error: { custom: this.translations["ErrorFetchingEngagement"] } };
        return from(this.protocolService.http(config, messages));
    }

    fetchCertificatesByEngagementId(id: string): Observable<IProtocolResponse<IEngagementCertificateInformation[]>> {
        const config: IProtocolConfig = this.protocolService.customConfig("GET", `/api/vendor/v1/engagements/${id}/certificates`);
        const messages: IProtocolMessages = { Error: { custom: this.translations["ErrorFetchingCertificates"] } };
        return from(this.protocolService.http(config, messages));
    }

    fetchContractsByEngagementId(id: string): Observable<IProtocolResponse<string[]>> {
        const config: IProtocolConfig = this.protocolService.customConfig("GET", `/api/vendor/v1/engagements/${id}/contracts`);
        const messages: IProtocolMessages = { Error: { custom: this.translations["ErrorRetrievingContractAttachments"] } };
        return from(this.protocolService.http(config, messages));
    }

    fetchServicesByEngagementId(id: string): Observable<IProtocolResponse<IEngagementInventoryInformation[]>> {
        const config: IProtocolConfig = this.protocolService.customConfig("GET", `/api/vendor/v1/engagements/${id}/services`);
        const messages: IProtocolMessages = { Error: { custom: this.translations["Vendorpedia.Error.FetchServiceForVendor"] } };
        return from(this.protocolService.http(config, messages));
    }
}
