export enum ListContext {
    ENGAGEMENTS,
}

export enum ListRouteNames {
    ENGAGEMENTS = "zen.app.pia.module.vendor.engagements",
}
