// Angular
import {
    Component,
    Input,
    Output,
    EventEmitter,
} from "@angular/core";

// Interfaces
import { ISortEvent } from "sharedModules/interfaces/sort.interface";
import { IEngagementListViewInformation } from "engagements/shared/engagement.interface";

// Constants/Enums
import { TableColumnTypes } from "enums/data-table.enum";
import { ITableColumnConfig } from "interfaces/table-config.interface";

@Component({
    selector: "engagements-table",
    templateUrl: "./engagements-table.component.html",
})
export class EngagementsTableComponent {

    @Input() columns: ITableColumnConfig[];
    @Input() tableData: IEngagementListViewInformation[];
    @Input() sortOrder: string;
    @Input() sortKey: string;

    @Output() sortChange = new EventEmitter<ISortEvent>();
    @Output() routeChange = new EventEmitter<{ row: IEngagementListViewInformation, col: ITableColumnConfig }>();

    tableColumnTypes = TableColumnTypes;
    emptyText = "- - - -";
    allRowSelected = false;

    goToRoute(row: IEngagementListViewInformation, col: ITableColumnConfig): void {
        this.routeChange.emit({ row, col });
    }

    onSortChange({ sortKey, sortOrder }: ISortEvent) {
        this.sortChange.emit({ sortKey, sortOrder });
    }
    handleTableSelectClick(actionType: string, isChecked: boolean, code?: string): void {
        switch (actionType) {
            case "header":
                this.tableData.forEach((row: IEngagementListViewInformation): void => {
                    row.isSelected = isChecked;
                });
                break;
            case "row":
                this.allRowSelected = true;
                this.tableData.forEach((row: IEngagementListViewInformation): void => {
                    if (row.engagementId === code) {
                        row.isSelected = isChecked;
                    }
                    if (!row.isSelected) {
                        this.allRowSelected = false;
                    }
                });
                break;
        }
    }
}
