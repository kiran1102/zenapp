export interface IListAddItemButton {
    permission: string;
    route: string;
    text: string;
    headerId: string;
    emptyStateId: string;
}
