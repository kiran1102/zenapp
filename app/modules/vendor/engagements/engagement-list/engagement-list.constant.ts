import {
    ListContext,
    ListRouteNames,
} from "engagements/engagement-list/engagement-list.enum";
import { GridViewStorageNames } from "sharedModules/enums/grid-view-storage.enum";

export const PageHeaderTitle = {
    [ListContext.ENGAGEMENTS]: "Engagements",
};

export const AddItemButton = {
    [ListContext.ENGAGEMENTS]: {
        emptyStateId: "AddEngagementButtonEmptyState",
        headerId: "AddEngagementButton",
        permission: "VRMEngagementsAdd",
        route: "zen.app.pia.module.vendor.add-engagement-wizard",
        text: "AddEngagement",
    },
};

export const ListBrowserStorage = {
    [ListContext.ENGAGEMENTS]: GridViewStorageNames.ENGAGEMENT_LIST,
};

export const ListContextByRouteName = {
    [ListRouteNames.ENGAGEMENTS]: ListContext.ENGAGEMENTS,
};
