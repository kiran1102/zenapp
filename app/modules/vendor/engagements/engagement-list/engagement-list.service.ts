// Core
import { Injectable } from "@angular/core";

// Rxjs
import { BehaviorSubject } from "rxjs";
import { map } from "rxjs/operators";

// Services
import { EngagementApiService } from "engagements/shared/engagement-api.service";
import { IPageableOf } from "interfaces/pagination.interface";
import { IEngagementListViewInformation } from "engagements/shared/engagement.interface";
import { GridViewStorageService } from "sharedModules/services/helper/grid-view-storage.service";

// Enums
import { GridViewStorageNames } from "sharedModules/enums/grid-view-storage.enum";

// Interfaces
import { IGridPaginationParams } from "interfaces/grid.interface";

@Injectable()
export class EngagementListService {
    engagementList = new BehaviorSubject<IPageableOf<IEngagementListViewInformation>>(null);
    engagementList$ = this.engagementList.asObservable();

    storageKey = GridViewStorageNames.ENGAGEMENT_LIST;

    constructor(
        private engagementApiService: EngagementApiService,
        private browserStorageListService: GridViewStorageService,
    ) { }

    fetchEngagementList(params: IGridPaginationParams, fullText: string, payload?: any) {
        this.engagementApiService.fetchEngagementList(params, fullText, payload).pipe(
            map((response) => response.result ? response.data : null),
        ).subscribe((response: IPageableOf<IEngagementListViewInformation>) => {
            this.engagementList.next(response);
        });
    }

}
