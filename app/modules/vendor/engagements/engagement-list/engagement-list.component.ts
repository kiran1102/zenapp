// Angular
import {
    Component,
    OnInit,
} from "@angular/core";

// Rxjs
import { Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";

// Third party
import { StateService } from "@uirouter/core";

// Interfaces
import { IGridPaginationParams } from "interfaces/grid.interface";
import { IPageableMeta } from "interfaces/pagination.interface";
import { IContractRowContent } from "interfaces/vendor/vendor-document.interface";
import { ITableColumnConfig } from "interfaces/table-config.interface";
import { ISortEvent } from "sharedModules/interfaces/sort.interface";
import { IListAddItemButton } from "engagements/engagement-list/engagement-list.interface";

// Constants/Enums
import { TableColumnTypes } from "enums/data-table.enum";
import { GridViewStorageNames } from "sharedModules/enums/grid-view-storage.enum";
import { InventoryTableIds } from "enums/inventory.enum";
import { InventoryDetailsTabs } from "constants/inventory.constant";
import {
    PageHeaderTitle,
    AddItemButton,
    ListContextByRouteName,
    ListBrowserStorage,
} from "engagements/engagement-list/engagement-list.constant";
import { ListContext } from "engagements/engagement-list/engagement-list.enum";

// Services
import { GridViewStorageService } from "sharedModules/services/helper/grid-view-storage.service";
import { EngagementListService } from "engagements/engagement-list//engagement-list.service";
import { IEngagementListViewInformation } from "engagements/shared/engagement.interface";
import { ControlsFilterService } from "modules/controls/shared/components/controls-filter/controls-filter.service";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

@Component({
    selector: "engagement-list",
    templateUrl: "./engagement-list.component.html",
})
export class EngagementListComponent implements OnInit {

    activeColumnList: ITableColumnConfig[] = [];
    defaultPagination: IGridPaginationParams = { page: 0, size: 20, sort: null };
    paginationParams: IGridPaginationParams = { ...this.defaultPagination };
    paginationDetail: IPageableMeta;
    tableData: IEngagementListViewInformation[] = [];
    isPageLoading = true;
    searchText = "";
    sortOrder = "asc";
    sortKey = "name";
    currentFilters: any[] = [];
    isFiltering = false;
    pageHeaderTitle: string;
    addItemButton: IListAddItemButton;
    listContext: ListContext;
    storageKey: GridViewStorageNames;
    query: any[] = [];

    private destroy$ = new Subject();

    constructor(
        private translatePipe: TranslatePipe,
        private engagementListService: EngagementListService,
        private stateService: StateService,
        private browserStorageListService: GridViewStorageService,
        private controlsFilterService: ControlsFilterService,
    ) { }

    ngOnInit() {
        this.listContext = ListContextByRouteName[this.stateService.current.name];
        this.addItemButton = AddItemButton[this.listContext];
        this.pageHeaderTitle = PageHeaderTitle[this.listContext];
        this.storageKey = ListBrowserStorage[this.listContext];

        const storage = this.browserStorageListService.getBrowserStorageList(this.storageKey);
        this.searchText = storage && storage.searchText;
        if (storage.sort) {
            this.sortKey = storage.sort.sortKey;
            this.sortOrder = storage.sort.sortOrder;
            this.defaultPagination = { ...this.defaultPagination, sort: `${this.sortKey},${this.sortOrder}` };
            this.paginationParams.sort = `${this.sortKey},${this.sortOrder}`;
        }
        if (storage.pagination) {
            this.paginationParams.page = storage.pagination.page;
        }
        this.currentFilters = this.controlsFilterService.loadFilters(this.storageKey);
        this.query = this.controlsFilterService.buildQuery(this.currentFilters);

        this.activeColumnList = [
            {
                name: "",
                sortKey: null,
                type: TableColumnTypes.Checkbox,
                cellValueKey: null,
                canFilter: false,
            },
            {
                name: this.translatePipe.transform("EngagementName"),
                columnKey: "EngagementName",
                type: TableColumnTypes.Link,
                cellValueKey: "name",
                sortKey: "name",
                sortable: true,
            },
            {
                name: this.translatePipe.transform("Vendor"),
                columnKey: "Vendor",
                type: TableColumnTypes.Link,
                cellValueKey: "vendor",
                isChildObject: true,
                childObjectRef: "name",
                filterKey: "vendorId",
                sortKey: "vendor",
                canFilter: true,
            },
            {
                name: this.translatePipe.transform("Type"),
                type: TableColumnTypes.Translate,
                sortable: true,
                sortKey: "type",
                cellValueKey: "type",
            },
            {
                name: this.translatePipe.transform("StartDate"),
                columnKey: "StartDate",
                type: TableColumnTypes.Date,
                sortKey: "startDate",
                cellValueKey: "startDate",
                sortable: true,
            },
            {
                name: this.translatePipe.transform("EndDate"),
                type: TableColumnTypes.Date,
                cellValueKey: "endDate",
                sortKey: "endDate",
                sortable: true,
            },
            {
                name: this.translatePipe.transform("InternalOwner"),
                type: TableColumnTypes.Text,
                cellValueKey: "internalOwnerUser",
            },
            {
                name: this.translatePipe.transform("ExternalContact"),
                type: TableColumnTypes.Text,
                sortKey: "externalContact",
                sortable: true,
                cellValueKey: "externalContact",
            },
        ];

        this.engagementListService.engagementList$
            .pipe(
                takeUntil(this.destroy$),
            )
            .subscribe((response) => {
                if (response) {
                    this.tableData = response.content;
                    delete response.content;
                    this.paginationDetail = response;
                }
                this.isPageLoading = false;
            });

        this.fetchAllEngagements();
    }

    ngOnDestroy(): void {
        this.destroy$.next();
    }

    onPageChanged(page: number) {
        this.paginationParams.page = page;
        this.browserStorageListService.saveSessionStoragePagination(this.storageKey, this.paginationParams);
        this.fetchAllEngagements();
    }

    fetchAllEngagements() {
        this.tableData = [];
        this.isPageLoading = true;
        this.engagementListService.fetchEngagementList(this.paginationParams, this.searchText, this.query);
    }

    addItem() {
        this.stateService.go(this.addItemButton.route);
    }

    routeChange(event: { row: IEngagementListViewInformation, col: ITableColumnConfig }) {
        if (event.col.cellValueKey === "vendor") {
            this.stateService.go("zen.app.pia.module.vendor.inventory.details", {
                inventoryId: InventoryTableIds.Vendors,
                recordId: event.row.vendor.id,
                tabId: InventoryDetailsTabs.Details,
            });
        }
        if (event.col.cellValueKey === "name") {
            this.stateService.go("zen.app.pia.module.vendor.engagement-detail", {
                id: event.row.engagementId,
                name: event.row.name,
            });
        }
    }

    onSearchChange(searchText: string = ""): void {
        this.searchText = searchText;
        this.browserStorageListService.saveSessionStorageSearch(this.storageKey, this.searchText);
        this.paginationParams = { ...this.defaultPagination };
        this.browserStorageListService.saveSessionStoragePagination(this.storageKey, this.paginationParams);
        this.fetchAllEngagements();
    }

    sortChange(sortEvent: ISortEvent) {
        this.sortOrder = sortEvent.sortOrder;
        this.sortKey = sortEvent.sortKey;
        this.defaultPagination = { ...this.defaultPagination, sort: `${this.sortKey},${this.sortOrder}` };
        this.paginationParams = { ...this.paginationParams, sort: `${this.sortKey},${this.sortOrder}` };
        this.browserStorageListService.saveSessionStorageSort(this.storageKey, sortEvent);
        this.fetchAllEngagements();
    }

    toggleFilterDrawer() {
        this.isFiltering = !this.isFiltering;
    }

    getFilterableColumns(column: ITableColumnConfig): boolean {
        return Boolean(column.canFilter);
    }

    filterApplied(filters: any) {
        this.isFiltering = false;
        this.query = this.controlsFilterService.buildQuery(filters);
        this.currentFilters = filters;
        this.paginationParams.page = 0;
        this.fetchAllEngagements();
    }

    filterCanceled() {
        this.isFiltering = false;
    }
}
