import { NgModule } from "@angular/core";

// Modules
import { SharedModule } from "sharedModules/shared.module";
import { CommonModule } from "@angular/common";
import { HttpClientModule } from "@angular/common/http";
import { FormsModule } from "@angular/forms";
import { ReactiveFormsModule } from "@angular/forms";
import { VitreusModule } from "@onetrust/vitreus";
import { OTPipesModule } from "modules/pipes/pipes.module";
import { FilterModule } from "modules/filter/filter.module";
import { TranslateModule } from "@ngx-translate/core";
import { InventorySharedModule } from "modules/inventory/inventory-shared/inventory-shared.module";
import { EngagementSharedModule } from "engagements/shared/engagement-shared.module";
import { ControlsSharedModule } from "modules/controls/shared/controls-shared.module";

// Components
import { EngagementListComponent } from "engagements/engagement-list/engagement-list.component";
import { EngagementsTableComponent } from "engagements/engagement-list/engagements-table/engagements-table.component";
import { EngagementListService } from "./engagement-list.service";

@NgModule({
    imports: [
        SharedModule,
        CommonModule,
        HttpClientModule,
        FormsModule,
        ReactiveFormsModule,
        VitreusModule,
        OTPipesModule,
        FilterModule,
        TranslateModule,
        InventorySharedModule,
        EngagementSharedModule,
        ControlsSharedModule,
    ],
    declarations: [
        EngagementListComponent,
        EngagementsTableComponent,
    ],
    entryComponents: [
        EngagementListComponent,
    ],
    providers: [
        EngagementListService,
    ],
})
export class EngagementListModule {}
