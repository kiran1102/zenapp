// angular
import { NgModule } from "@angular/core";

// modules
import { SharedModule } from "sharedModules/shared.module";
import { VendorSharedModule } from "modules/vendor/shared/vendor-shared.module";
import { CommonModule } from "@angular/common";
import { HttpClientModule } from "@angular/common/http";
import { FormsModule } from "@angular/forms";
import { ReactiveFormsModule } from "@angular/forms";
import { VitreusModule } from "@onetrust/vitreus";
import { DndListModule } from "ngx-drag-and-drop-lists";
import { OTPipesModule, PIPES } from "modules/pipes/pipes.module";
import { VirtualScrollModule } from "od-virtualscroll";
import { OneVideoModule } from "modules/video/video.module";
import { ControlsSharedModule } from "controls/shared/controls-shared.module";
import { ContractsSharedModule } from "contracts/shared/contracts-shared.module";

// components
import { VendorControlsListComponent } from "modules/vendor/vendor-controls/vendor-controls-list/vendor-controls-list.component";
import { VendorControlsTableComponent } from "modules/vendor/vendor-controls/vendor-controls-list/vendor-controls-table/vendor-controls-table.component";
import { VendorControlsListDetailsComponent } from "modules/vendor/vendor-controls/vendor-controls-list/vendor-controls-list-details/vendor-controls-list-details.component";
import { VendorControlsDetailsComponent } from "modules/vendor/vendor-controls/vendor-controls-list/vendor-controls-list-details/vendor-controls-details/vendor-controls-details.component";

@NgModule({
    imports: [
        SharedModule,
        VendorSharedModule,
        CommonModule,
        HttpClientModule,
        FormsModule,
        ReactiveFormsModule,
        VitreusModule,
        DndListModule,
        OTPipesModule,
        VirtualScrollModule,
        OneVideoModule,
        ControlsSharedModule,
        ContractsSharedModule,
    ],
    declarations: [
        VendorControlsListComponent,
        VendorControlsTableComponent,
        VendorControlsListDetailsComponent,
        VendorControlsDetailsComponent,
    ],
    exports: [
        VendorControlsListComponent,
        VendorControlsTableComponent,
        VendorControlsDetailsComponent,
        ContractsSharedModule,
    ],
    entryComponents: [
        VendorControlsListDetailsComponent,
    ],
    providers: [
        PIPES, // TODO: Remove from providers when ot-common is implemented. Only necessary if you are using pipe in TS file, else just module is fine
    ],
})
export class VendorControlsModule { }
