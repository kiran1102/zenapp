// Core
import {
    Component,
    Input,
    OnInit,
    OnDestroy,
    EventEmitter,
    Output,
} from "@angular/core";

// Vitreus
import {
    ContextMenuType,
    OtModalService,
    ConfirmModalType,
} from "@onetrust/vitreus";

// Interfaces
import { IDropdownOption } from "interfaces/one-dropdown.interface";
import { IDataTableColumn } from "interfaces/tables.interface";
import {
    IInventoryControlsTable,
    IVendorControlDetailInformation,
} from "modules/vendor/shared/interfaces/inventory-controls.interface";

// Services
import { Permissions } from "modules/shared/services/helper/permissions.service";
import { StateService } from "@uirouter/core";
import { InventoryControlsService } from "modules/vendor/shared/services/inventory-controls.service";

// Rxjs
import { Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";

// Enums / Constants
import { TableColumnTypes } from "enums/data-table.enum";
import { VPControlsStatusColorMap } from "modules/vendor/shared/constants/vendor-controls.constants";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

@Component({
    selector: "vendor-controls-table",
    templateUrl: "./vendor-controls-table.component.html",
})
export class VendorControlsTableComponent implements OnInit, OnDestroy {

    @Input() table: IInventoryControlsTable;
    @Input() query;

    @Output() tableActions = new EventEmitter<{ sortKey: string, sortOrder: string }>();

    vPControlsStatusColorMap = VPControlsStatusColorMap;
    menuClass: string;
    resizableCol = false;
    responsiveTable = false;
    rowBordered = true;
    rowNoHover = false;
    rowStriped = false;
    selectedRowMap = {};
    sortKey = "";
    sortOrder = "";
    sortable = false;
    tableColumnTypes = TableColumnTypes;
    truncateCellContent = true;
    wrapCellContent = false;
    menuOptions: IDropdownOption[] = [];
    menuType = ContextMenuType.Small;
    vendorId: string;
    emptyData = "- - - -";

    private destroy$ = new Subject();

    constructor(
        private stateService: StateService,
        private translatePipe: TranslatePipe,
        private permissions: Permissions,
        private otModalService: OtModalService,
        private inventoryControlsService: InventoryControlsService,
    ) { }

    ngOnInit() {
        this.vendorId = this.stateService.params.recordId;
        this.generateMenuOptions();
        this.setDefaultSorting();
    }

    ngOnDestroy() {
        this.destroy$.next();
        this.destroy$.complete();
    }

    columnTrackBy(index: number, column: IDataTableColumn): string {
        return column.id;
    }

    handleSortChange({ sortKey, sortOrder }) {
        this.sortKey = sortKey;
        this.sortOrder = sortOrder;
        this.tableActions.emit({ sortKey, sortOrder });
    }

    setDefaultSorting() {
        if (this.table.metaData.sort && this.table.metaData.sort.length > 0) {
            this.sortable = true;
            this.sortKey = this.table.metaData.sort[0].property;
            this.sortOrder = (this.table.metaData.sort[0].direction as string).toLowerCase();
        }
    }

    goToRecord(row: IVendorControlDetailInformation, editMode = false) {
        this.stateService.go("zen.app.pia.module.vendor.control-details", { controlId: row.id, editMode });
    }

    private generateMenuOptions() {
        if (this.permissions.canShow("VRMEditControlOnVendor")) {
            this.menuOptions.push({
                text: this.translatePipe.transform("Edit"),
                action: (row: IVendorControlDetailInformation) => {
                    this.goToRecord(row, true);
                },
            });
        }
        if (this.permissions.canShow("VRMRemoveControlFromVendor")) {
            this.menuOptions.push({
                text: this.translatePipe.transform("Remove"),
                action: (row: IVendorControlDetailInformation) => {
                    this.openDeleteModal(row.control.id);
                },
            });
        }
    }

    private openDeleteModal(controlId: string) {
        this.otModalService.confirm({
            type: ConfirmModalType.DELETE,
            translations: {
                title: this.translatePipe.transform("RemoveControls"),
                desc: this.translatePipe.transform("AreYouSureToDeleteControls"),
                confirm: this.translatePipe.transform("DoYouWantToContinue"),
                submit: this.translatePipe.transform("Remove"),
                cancel: this.translatePipe.transform("Cancel"),
            },
        }).pipe(
            takeUntil(this.destroy$),
        ).subscribe((res) => {
            if (res) {
                this.inventoryControlsService.vendorControlsLoading$.next(true);
                this.inventoryControlsService.deleteControl(this.vendorId, null, controlId, this.query);
            }
        });
    }
}
