// Angular
import {
    Component,
    OnInit,
} from "@angular/core";

// Rxjs
import { takeUntil } from "rxjs/operators";
import { Subject } from "rxjs";

// Interfaces
import { IVendorControlDetailInformation } from "modules/vendor/shared/interfaces/inventory-controls.interface";

// Constants & Enums
import { InventoryTableIds } from "enums/inventory.enum";
import { InventoryDetailsTabs } from "constants/inventory.constant";

// Services
import { StateService } from "@uirouter/core";
import { InventoryControlsService } from "modules/vendor/shared/services/inventory-controls.service";

// Interfaces
import { ITabsNav } from "interfaces/one-tabs-nav.interface";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";
import { ControlDetailsTabOptions } from "modules/controls/shared/enums/controls.enum";

@Component({
    selector: "vendor-controls-list-details",
    templateUrl: "./vendor-controls-list-details.component.html",
})
export class VendorControlsListDetailsComponent implements OnInit {

    controlId: string;
    control: IVendorControlDetailInformation;
    vendorId: string;
    isLoading = true;
    selectedTab = ControlDetailsTabOptions.Details;
    ControlDetailsTabOptions = ControlDetailsTabOptions;
    tabOptions: ITabsNav[] = [];
    editMode: boolean;

    private destroy$ = new Subject();

    constructor(
        private stateService: StateService,
        private inventoryControlsService: InventoryControlsService,
        private translatePipe: TranslatePipe,
    ) { }

    ngOnInit() {
        this.setTabOptions();
        this.controlId = this.stateService.params.controlId || null;
        this.vendorId = this.stateService.params.vendorId || null;
        this.editMode = this.stateService.params.editMode === "true" || false;
        this.inventoryControlsService.retrieveAllMaturities();
        this.inventoryControlsService.retrieveVendorControlsStatus();
        this.inventoryControlsService.retrieveVendorControlById(this.controlId);
        this.inventoryControlsService.control$
            .pipe(takeUntil(this.destroy$))
            .subscribe((control: IVendorControlDetailInformation) => {
                this.control = control;
                this.isLoading = false;
            });
    }

    ngOnDestroy() {
        this.inventoryControlsService.reset();
        this.destroy$.next();
        this.destroy$.complete();
    }

    onHeaderRouteClick(route: { path: string, params: any }) {
        switch (route.path) {
            case "zen.app.pia.module.vendor.inventory.list":
                this.stateService.go(route.path, { Id: InventoryTableIds.Vendors, page: null, size: null });
                break;
            case "zen.app.pia.module.vendor.inventory.details":
                this.stateService.go(route.path, {
                    inventoryId: InventoryTableIds.Vendors,
                    recordId: this.control.inventory.id,
                    tabId: InventoryDetailsTabs.Controls,
                });
                break;
        }
    }

    onTabClick(option: ITabsNav) {
        this.selectedTab = option.id as ControlDetailsTabOptions;
    }

    setTabOptions() {
        this.tabOptions.push({
            text: this.translatePipe.transform("Details"),
            id: ControlDetailsTabOptions.Details,
            otAutoId: "VendorControlsListDetails",
        });
    }

    setEditMode(editMode: boolean) {
        this.editMode = editMode;
    }
}
