// Angular
import {
    Component,
    Input,
    Output,
    EventEmitter,
} from "@angular/core";
import {
    FormBuilder,
    FormGroup,
    Validators,
} from "@angular/forms";

// 3rd Party
import {
    Transition,
    TransitionService,
    StateService,
    HookResult,
} from "@uirouter/core";

// Rxjs
import {
    Subject,
    combineLatest,
} from "rxjs";
import { takeUntil } from "rxjs/operators";

// Interfaces
import {
    IVendorControlDetailInformation,
    IVendorControlDetailData,
} from "modules/vendor/shared/interfaces/inventory-controls.interface";
import { ICreateData } from "modules/vendor/shared/interfaces/vendor-upload-document.interface";
import { ICategoryInformation } from "modules/controls/shared/interfaces/controls.interface";
import { IStringMap } from "interfaces/generic.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";

// Services
import { VendorApiService } from "modules/vendor/services/api/vendor-api.service";
import { InventoryControlsService } from "modules/vendor/shared/services/inventory-controls.service";

// Enums
import { VendorControlsTabColumnNames } from "modules/vendor/shared/enums/vendor-controls-tab.enum";

// Constants
import { FormDataTypes } from "constants/form-types.constant";

// Vitreus
import {
    OtModalService,
    ModalSize,
} from "@onetrust/vitreus";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

// Modal Component
import { UnsavedChangesConfirmationModal } from "sharedModules/components/unsaved-changes-confirmation-modal/unsaved-changes-confirmation-modal.component";

@Component({
    selector: "vendor-controls-details",
    templateUrl: "./vendor-controls-details.component.html",
})
export class VendorControlsDetailsComponent {

    @Input() editMode: boolean;
    @Input() isEditable: boolean;
    @Output() toggleEdit = new EventEmitter<boolean>();

    controlForm: FormGroup;
    hasEdits = false;
    isSaving = false;
    vendorControlsTabColumnNames = VendorControlsTabColumnNames;
    formDataTypes = FormDataTypes;
    formData: ICreateData[] = [];
    control: IVendorControlDetailInformation;
    maturities: ICategoryInformation[];
    filteredMaturities: ICategoryInformation[];
    selectedMaturity: ICategoryInformation;
    statusOptions: ICategoryInformation[];
    selectedStatusOption: ICategoryInformation;
    emptyData = "- - - -";

    private transitionDeregisterHook: () => any;
    private destroy$ = new Subject();

    constructor(
        private fb: FormBuilder,
        private vendorApiService: VendorApiService,
        private inventoryControlsService: InventoryControlsService,
        private translatePipe: TranslatePipe,
        private otModalService: OtModalService,
        private stateService: StateService,
        private $transitions: TransitionService,
    ) { }

    ngOnInit() {
        combineLatest(
            this.inventoryControlsService.control$,
            this.inventoryControlsService.controlAllMaturities$,
            this.inventoryControlsService.vendorControlStatus$,
        ).pipe(
            takeUntil(this.destroy$),
        ).subscribe(([control, controlMaturities, controlStatus]) => {
            if (control) {
                this.control = control;
                this.hasEdits = false;
                this.generateControlFormGroup();
                this.onFormChanges();
                this.setDefaultMaturity();
                this.setDefaultStatus();
            }
            if (controlMaturities) {
                this.maturities = controlMaturities;
                this.filteredMaturities = this.maturities.map((m) => {
                    return {
                        id: null ? "" : m.id,
                        name: null ? "" : m.nameKey ? this.translatePipe.transform(m.nameKey) : m.name,
                        nameKey: null ? "" : m.nameKey,
                    };
                });
                this.filteredMaturities.unshift({ id: "", name: "- - - -", nameKey: ""});
            }
            if (controlStatus) {
                this.statusOptions = controlStatus.map((status) => {
                    if (status) return { nameKey: status, name: this.translatePipe.transform(status), id: null };
                });
            }
            this.generateFormData();
        });
        this.setTransitionAwayHook();
    }

    ngOnDestroy() {
        this.inventoryControlsService.reset();
        this.destroy$.next();
        this.destroy$.complete();
        if (this.transitionDeregisterHook) {
            this.transitionDeregisterHook();
        }
    }

    generateFormData() {
        this.formData = [
            {
                name: VendorControlsTabColumnNames.CONTROLID,
                label: "ControlID",
                type: FormDataTypes.INPUT,
                isRequired: false,
                formControlName: "controlIdentifier",
                readOnly: true,
                isChildObject: true,
                childObjectRef: "control",
            },
            {
                name: VendorControlsTabColumnNames.NAME,
                label: "Name",
                type: FormDataTypes.TEXTAREA,
                isRequired: false,
                formControlName: "controlName",
                readOnly: true,
                isChildObject: true,
                childObjectRef: "control",
            },
            {
                name: VendorControlsTabColumnNames.FRAMEWORK,
                label: "Framework",
                type: FormDataTypes.LOOKUP,
                isRequired: false,
                formControlName: "frameworkName",
                readOnly: true,
                isChildObject: true,
                childObjectRef: "control",
            },
            {
                name: VendorControlsTabColumnNames.CATEGORY,
                label: "Category",
                type: FormDataTypes.LOOKUP,
                isRequired: false,
                formControlName: "controlCategoryName",
                readOnly: true,
                isChildObject: true,
                childObjectRef: "control",
            },
            {
                name: VendorControlsTabColumnNames.DESCRIPTION,
                label: "Description",
                type: FormDataTypes.TEXTAREA,
                isRequired: false,
                formControlName: "controlDescription",
                readOnly: true,
                isChildObject: true,
                childObjectRef: "control",
            },
            {
                name: VendorControlsTabColumnNames.MATURITY,
                label: "Maturity",
                type: FormDataTypes.PICKLIST,
                isRequired: false,
                formControlName: "maturityNameKey",
                labelKey: "name",
                valueKey: "nameKey",
                options: this.filteredMaturities,
                id: "VendorControlsDetailsTabMaturityPicklist",
                readOnly: false,
            },
            {
                name: VendorControlsTabColumnNames.STATUS,
                label: "Status",
                type: FormDataTypes.PICKLIST,
                isRequired: false,
                formControlName: "status",
                labelKey: "name",
                valueKey: "nameKey",
                options: this.statusOptions,
                id: "VendorControlsDetailsTabStatusPicklist",
                readOnly: false,
            },
            {
                name: VendorControlsTabColumnNames.NOTES,
                label: "Notes",
                type: FormDataTypes.TEXTAREA,
                isRequired: false,
                formControlName: "note",
                rows: "2",
                id: "VendorControlsDetailsTabNotesTextarea",
                readOnly: false,
            },
        ];
    }

    onFormChanges(): void {
        this.controlForm.valueChanges.subscribe((val) => {
            this.hasEdits = true;
        });
    }

    get controlMaturity() { return this.controlForm.get("maturityNameKey"); }
    get controlStatus() { return this.controlForm.get("status"); }

    onDropdownChange(formControlName: string, event: ICategoryInformation) {
        this.hasEdits = true;
        if (formControlName === "maturityNameKey") {
            this.control["maturityId"] = event.id;
            this.controlMaturity.setValue(event);
            this.selectedMaturity = event;
        } else if (formControlName === "status") {
            this.controlStatus.setValue(event);
            this.selectedStatusOption = event;
        }
    }

    onToggleEdit(readOnly = false) {
        if (this.isEditable && !readOnly) {
            this.editMode = true;
            this.hasEdits = false;
            this.toggleEdit.emit(this.editMode);
        }
    }

    onSave(callback?: () => void) {
        if (this.controlForm.valid && this.hasEdits && !this.isSaving) {
            this.isSaving = true;
            const values = this.controlForm.value;
            const updateControlPayload: IStringMap<string> = {
                maturityId: this.control.maturityId || null,
                status: this.selectedStatusOption.nameKey || null,
                note: values.note ? values.note.trim() : null,
            };
            this.vendorApiService.updateVendorControl(this.control.id, updateControlPayload)
                .pipe(takeUntil(this.destroy$))
                .subscribe((response: IProtocolResponse<IVendorControlDetailData>) => {
                    if (response.result) {
                        this.inventoryControlsService.control$.next(response.result ? response.data.data : null);
                        if (callback) {
                            callback();
                        }
                    }
                    this.editMode = false;
                    this.hasEdits = false;
                    this.isSaving = false;
                });
        }
    }

    cancelAction() {
        if (!this.isSaving) {
            this.editMode = false;
            this.hasEdits = false;
            this.setDefaultStatus();
            this.setDefaultMaturity();
            this.generateControlFormGroup();
            this.onFormChanges();
            this.toggleEdit.emit(this.editMode);
        }
    }

    // Form builder
    private generateControlFormGroup() {
        this.controlForm = this.fb.group({
            maturityNameKey: [this.control.maturityId ? {
                id: this.control.maturityId,
                name: this.translatePipe.transform(this.control.maturityName),
                nameKey: this.control.maturityNameKey,
            } : null],
            status: [this.control.status ? {
                id: this.control.status,
                name: this.translatePipe.transform(this.control.status),
                nameKey: this.control.status,
            } : null],
            note: [this.control.note, [Validators.maxLength(500)]],
        });
    }

    private handleStateChange(toState: IStringMap<any>, toParams: IStringMap<any>, options: IStringMap<any>): boolean {
        if (!this.hasEdits) return true;
        const callback = () => {
            this.stateService.go(toState, toParams, options);
        };
        this.launchSaveChangesModal(callback);
        return false;
    }

    private launchSaveChangesModal(callback: () => void): void {
        this.otModalService.create(
                UnsavedChangesConfirmationModal,
                {
                    isComponent: true,
                    size: ModalSize.SMALL,
                    hideClose: true,
                },
                {
                    promiseToResolve: () => {
                        this.onSave(callback);
                    },
                    discardCallback: () => {
                        this.cancelAction();
                        callback();
                    },
                },
            ).pipe(
                takeUntil(this.destroy$),
            ).subscribe();
    }

    private setTransitionAwayHook(): void {
        this.transitionDeregisterHook = this.$transitions.onExit({ from: "zen.app.pia.module.vendor.control-details" }, (transition: Transition): HookResult => {
            return this.handleStateChange(transition.to(), transition.params("to"), transition.options);
        }) as any;
    }

    private setDefaultMaturity() {
        if (this.control.maturityId || this.control.maturityName) {
            this.selectedMaturity = {
                id: this.control.maturityId,
                name: this.control.maturityName,
                nameKey: this.control.maturityNameKey,
            };
        }
    }

    private setDefaultStatus() {
        if (this.control.status) {
            this.selectedStatusOption = {
                id: this.control.status || null,
                name: this.translatePipe.transform(this.control.status),
                nameKey: this.control.status,
            };
        }
    }
}
