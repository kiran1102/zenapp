// Rxjs
import { Subject, combineLatest } from "rxjs";
import { takeUntil } from "rxjs/operators";

// Vitreus
import {
    ModalSize,
    OtModalService,
} from "@onetrust/vitreus";

// 3rd party
import {
    Component,
    Input,
    SimpleChanges,
} from "@angular/core";
import { StateService } from "@uirouter/core";

// Services
import { InventoryControlsService } from "modules/vendor/shared/services/inventory-controls.service";
import { ControlsFilterService } from "controls/shared/components/controls-filter/controls-filter.service";
import { GridViewStorageService } from "sharedModules/services/helper/grid-view-storage.service";

// Constants + Enums
import { DefaultPagination } from "modules/vendor/shared/constants/vendor-documents.constants";
import { TableColumnTypes } from "enums/data-table.enum";
import { VendorControlsTabColumnNames } from "modules/vendor/shared/enums/vendor-controls-tab.enum";
import { GridViewStorageNames } from "sharedModules/enums/grid-view-storage.enum";

// Interfaces
import {
    IVendorControlDetailInformation,
    IInventoryControlsTable,
} from "modules/vendor/shared/interfaces/inventory-controls.interface";
import {
    IAttributeOptionV2,
    IInventoryRecordV2,
} from "interfaces/inventory.interface";
import { IPageableMeta } from "interfaces/pagination.interface";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

// Modals
import {
    InventoryAddControlsModalCloseAction,
    InventoryAddControlsModalComponent,
    IInventoryAddControlsModalData,
} from "inventorySharedModule/shared/components/inventory-add-controls-modal/inventory-add-controls-modal.component";

@Component({
    selector: "vendor-controls-list",
    templateUrl: "./vendor-controls-list.component.html",
})
export class VendorControlsListComponent {

    @Input() record: IInventoryRecordV2;

    isPageloading = true;
    isFiltering: boolean;
    defaultPagination = DefaultPagination;
    paginationParams = { ...DefaultPagination };
    table: IInventoryControlsTable;
    tableMetaData: IPageableMeta;

    query = [];
    currentFilters = [];

    storageNamespace = GridViewStorageNames.VENDOR_INVENTORY_LIST;
    storageKey: string;

    private destroy$ = new Subject();

    constructor(
        public inventoryControlsService: InventoryControlsService,
        private translatePipe: TranslatePipe,
        private otModalService: OtModalService,
        private controlsFilterService: ControlsFilterService,
        readonly stateService: StateService,
        private browserStorageListService: GridViewStorageService,
    ) { }

    ngOnChanges(changes: SimpleChanges) {
        if (changes.record && changes.record.currentValue && changes.record.currentValue.id) {
            this.record = changes.record.currentValue;
            this.initializeList(changes.record.currentValue.id);
        }
    }

    ngOnDestroy() {
        this.inventoryControlsService.reset();
        this.destroy$.next();
        this.destroy$.complete();
    }

    initializeList(recordId: string) {
        this.storageKey = `${this.storageNamespace}.${recordId}`;
        const storage = this.browserStorageListService.getBrowserStorageList(this.storageNamespace);
        if (storage && storage.pagination) this.inventoryControlsService.paginationParams$.next(storage.pagination);
        this.inventoryControlsService.storePaginationDetails();
        this.currentFilters = this.controlsFilterService.loadFilters(this.storageKey);
        this.query = this.controlsFilterService.buildQuery(this.currentFilters);

        this.isPageloading = true;
        this.inventoryControlsService.retrieveControlsPerVendor(recordId, null, this.query);
        combineLatest(
            this.inventoryControlsService.vendorControlsMeta$,
            this.inventoryControlsService.vendorControls$,
        ).pipe(
            takeUntil(this.destroy$),
        ).subscribe(([controlsMeta, controlsLibrary]) => {
            if (controlsMeta && controlsLibrary) {
                this.table = this.configureTable(controlsLibrary, controlsMeta);
                this.tableMetaData = controlsMeta;
                this.isPageloading = false;
            }
        });
    }

    toggleFilterDrawer() {
        this.isFiltering = !this.isFiltering;
    }

    onPaginationChange(event: number = 0) {
        this.paginationParams = { ...this.paginationParams, page: event };
        this.inventoryControlsService.paginationParams$.next(this.paginationParams);
        this.inventoryControlsService.retrieveControlsPerVendor(this.record.id, null, this.query);
    }

    onSortChange({ sortKey, sortOrder }) {
        this.paginationParams.sort.sortKey = sortKey;
        this.paginationParams.sort.sortOrder = sortOrder;
        this.inventoryControlsService.paginationParams$.next(this.paginationParams);
        this.inventoryControlsService.retrieveControlsPerVendor(this.record.id, null, this.query);
    }

    onSearchName(searchName: string) {
        this.inventoryControlsService.vendorControlsSearchName$.next(searchName);
        this.inventoryControlsService.paginationParams$.next(this.defaultPagination);
        this.inventoryControlsService.retrieveControlsPerVendor(this.record.id, null, this.query);
    }

    addNewControls() {
        const organization = this.record.organization as IAttributeOptionV2;
        const modalData: IInventoryAddControlsModalData = {
            inventoryId: this.record.id,
            inventoryName: this.record.name as string,
            orgGroupId: organization.id,
        };
        this.otModalService.create(
            InventoryAddControlsModalComponent,
            {
                isComponent: true,
                size: ModalSize.MEDIUM,
            },
            modalData,
        ).subscribe((action: InventoryAddControlsModalCloseAction) => {
            switch (action) {
                case InventoryAddControlsModalCloseAction.CONTROLS_ADDED:
                    return this.initializeList(this.record.id);
                case InventoryAddControlsModalCloseAction.ADD_FRAMEWORKS:
                    return this.stateService.go("zen.app.pia.module.risks.views.controls.library");
            }
        });
    }

    filterApplied(filters) {
        this.isFiltering = false;
        this.query = this.controlsFilterService.buildQuery(filters);
        this.currentFilters = filters;
        this.inventoryControlsService.paginationParams$.next(this.defaultPagination);
        this.inventoryControlsService.retrieveControlsPerVendor(this.record.id, null, this.query);
    }

    filterCanceled() {
        this.isFiltering = false;
    }

    getFilterableColumns(column): boolean {
        return Boolean(column.canFilter);
    }

    private configureTable(records: IVendorControlDetailInformation[], controlsMeta: IPageableMeta): IInventoryControlsTable {
        return {
            rows: records,
            columns: [
                {
                    name: this.translatePipe.transform("ControlID"),
                    sortKey: "controlIdentifier",
                    type: TableColumnTypes.Link,
                    cellValueKey: VendorControlsTabColumnNames.CONTROLID,
                    isChildObject: true,
                    childObjectRef: "control",
                    sortable: true,
                },
                {
                    name: this.translatePipe.transform("Name"),
                    sortKey: "controlName",
                    type: TableColumnTypes.Text,
                    cellValueKey: VendorControlsTabColumnNames.NAME,
                    isChildObject: true,
                    childObjectRef: "control",
                    sortable: true,
                },
                {
                    name: this.translatePipe.transform("Framework"),
                    columnKey: "Framework",
                    sortKey: VendorControlsTabColumnNames.FRAMEWORK,
                    type: TableColumnTypes.Text,
                    cellValueKey: VendorControlsTabColumnNames.FRAMEWORK,
                    canFilter: true,
                    isChildObject: true,
                    childObjectRef: "control",
                    filterKey: "frameworkId",
                    sortable: true,
                },
                {
                    name: this.translatePipe.transform("Category"),
                    columnKey: "Category",
                    sortKey: VendorControlsTabColumnNames.CATEGORY,
                    type: TableColumnTypes.Text,
                    cellValueKey: VendorControlsTabColumnNames.CATEGORY,
                    canFilter: true,
                    isChildObject: true,
                    childObjectRef: "control",
                    filterKey: "controlCategoryId",
                },
                {
                    name: this.translatePipe.transform("Maturity"),
                    columnKey: "Maturity",
                    sortKey: VendorControlsTabColumnNames.MATURITY,
                    type: TableColumnTypes.Text,
                    cellValueKey: VendorControlsTabColumnNames.MATURITY,
                    canFilter: true,
                    filterKey: "maturityId",
                },
                {
                    name: this.translatePipe.transform("Status"),
                    sortKey: VendorControlsTabColumnNames.STATUS,
                    type: TableColumnTypes.Status,
                    cellValueKey: VendorControlsTabColumnNames.STATUS,
                },
                {
                    name: "",
                    type: TableColumnTypes.Action,
                    cellValueKey: "",
                },
            ],
            metaData: controlsMeta,
        };
    }
}
