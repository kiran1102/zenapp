// Core
import { Injectable } from "@angular/core";

// Services
import { ProtocolService } from "modules/core/services/protocol.service";

// Interfaces
import {
    IProtocolConfig,
    IProtocolMessages,
    IProtocolResponse,
} from "interfaces/protocol.interface";
import {
    INewVendorRecord,
    IVendorRecordOption,
    IVendorRecordV2,
    IInventoryVendorNames,
    IVendorInformation,
    IVendorOnboardingRequest,
    ISuggestNewVendor,
} from "modules/vendor/shared/interfaces/vendorpedia-exchange.interface";

import {
    IPageableOf,
    IPaginationFilter,
    IPaginatedResponse,
} from "interfaces/pagination.interface";
import {
    IAttachmentFile,
} from "interfaces/attachment-file.interface";
import { ISamlSettingsResponse } from "interfaces/setting.interface";
import { IAuditHistoryItem } from "interfaces/audit-history.interface";
import {
    IStringMap,
    INameId,
} from "interfaces/generic.interface";
import {
    IVendorControlAddRequest,
    IVendorControlDetailInformation,
    IVendorControlMaturityInformation,
    IFrameworkCertificateInformation,
    IVendorControlDetailData,
} from "modules/vendor/shared/interfaces/inventory-controls.interface";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

// Rxjs
import {
    from,
    Observable,
} from "rxjs";

@Injectable()
export class VendorApiService {

    vendorId: string;
    vendorName: string;

    constructor(
        private protocolService: ProtocolService,
        private translatePipe: TranslatePipe,
    ) { }

    addVendorRecord(schemaId: string, payload: INewVendorRecord): ng.IPromise<IProtocolResponse<IVendorRecordOption>> {
        const config: IProtocolConfig = this.protocolService.customConfig("POST", `/api/inventory/v2/inventories/${schemaId}/`, null, payload);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("VendorAlreadyinInventory") } };
        return this.protocolService.http(config, messages);
    }

    updateVendorRecord(schemaId: string, recordId: string, payload: IVendorRecordV2): ng.IPromise<IProtocolResponse<IVendorRecordV2[]>> {
        const config: IProtocolConfig = this.protocolService.customConfig("PUT", `/api/inventory/v2/inventories/${schemaId}/${recordId}`, null, payload);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("ErrorUpdatingRecord") } };
        return this.protocolService.http(config, messages);
    }

    fetchInventoryVendorNames(assessmentOrgId?: string): ng.IPromise<IProtocolResponse<IInventoryVendorNames[]>> {
        const config: IProtocolConfig = this.protocolService.customConfig("GET", `/api/inventory/v2/inventories/vendors`, { orgGroupId: assessmentOrgId, search: "" });
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("Error.GetInventoryOptions") } };
        return this.protocolService.http(config, messages);
    }

    vendorOnboarding(payload: IVendorOnboardingRequest): ng.IPromise<IProtocolResponse<IVendorRecordOption>> {
        const config: IProtocolConfig = this.protocolService.customConfig("POST", `/api/vendor/v1/vendors/onboardings`, null, payload);
        const messages: IProtocolMessages = {
            Error: { custom: this.translatePipe.transform("ErrorAddingVendor") },
            Success: { custom: this.translatePipe.transform("VendorSuccessfullyAdded") },
        };
        return this.protocolService.http(config, messages);
    }

    uploadDocument(file: IAttachmentFile): ng.IPromise<IProtocolResponse<IAttachmentFile>> {
        const fileData: FormData = new FormData();
        fileData.append("File", file.Blob);
        fileData.append("FileName", file.FileName);
        fileData.append("Name", file.Name);
        fileData.append("Type", "70");
        fileData.append("Encrypt", "true");
        fileData.append("IsInternal", `${file.IsInternal}`);
        fileData.append("FileRefId", file.RequestId);
        fileData.append("RefIds", file.RequestId);

        const config: IProtocolConfig = {
            method: "POST",
            url: "/file/addFile",
            body: fileData,
            multipartFormData: true,
        };
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("ErrorUploadingFiles") } };
        return this.protocolService.http(config, messages);
    }

    downloadContract(attachmentId: string): ng.IPromise<IProtocolResponse<any>> {
        const config: IProtocolConfig = this.protocolService.customConfig("GET", `/api/v1/private/file/downloadFile/${attachmentId}`, null, null, null, null, null, "arraybuffer");
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("ErrorDownloadingFile") } };
        return this.protocolService.http(config, messages);
    }

    onboardVendor(vendorId: string, payload: IVendorInformation): ng.IPromise<IProtocolResponse<IVendorInformation>> {
        const config: IProtocolConfig = this.protocolService.customConfig("POST", `/vendor/v1/vendors/${vendorId}/vendors`, null, payload);
        const messages: IProtocolMessages = {
            Error: { custom: this.translatePipe.transform("ErrorUpdatingRecord") },
            Success: { custom: this.translatePipe.transform("VendorSuccessfullyAdded") },
        };
        return this.protocolService.http(config, messages);
    }

    // Below API's for linking inventory to controls
    fetchVendorControls(vendorId, fullText: string, filters: IPaginationFilter[], paginationParams, payload?): Observable<IProtocolResponse<IPaginatedResponse<IVendorControlDetailInformation>>> {
        const sort = paginationParams && paginationParams.sort && paginationParams.sort.sortKey && paginationParams.sort.sortOrder ? `${paginationParams.sort.sortKey},${paginationParams.sort.sortOrder}` : "";
        const filter: string = filters && filters.length > 0 ? encodeURI(JSON.stringify(filters)) : "";
        const config: IProtocolConfig = this.protocolService.customConfig("POST", `/api/inventory/v2/vendors/${vendorId}/controls/page`, { text: fullText, filter, ...paginationParams, sort }, { fullText, filters: payload });
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("Vendor.Error.RetrievingControls") } };
        return from(this.protocolService.http(config, messages));
    }

    deleteControlFromVendorRecord(vendorId: string, controlId: string): Observable<IProtocolResponse<ISamlSettingsResponse>> {
        const config: IProtocolConfig = this.protocolService.customConfig("DELETE", `/api/inventory/v2/vendors/${vendorId}/controls/${controlId}`);
        const messages: IProtocolMessages = {
            Error: { custom: this.translatePipe.transform("Vendor.Error.DeleteControl") },
            Success: { custom: this.translatePipe.transform("ControlSuccessfullyDeleted") },
        };
        return from(this.protocolService.http(config, messages));
    }

    addControlsToVendor(id: string, payload: IVendorControlAddRequest): Observable<IProtocolResponse<null>> {
        const config: IProtocolConfig = this.protocolService.customConfig("POST", `/api/inventory/v2/vendors/${id}/controls`, null, payload);
        const messages: IProtocolMessages = {
            Error: { custom: this.translatePipe.transform("Vendor.Error.AddControl") },
            Success: { custom: this.translatePipe.transform("ControlSuccessfullyAdded") },
        };
        return from(this.protocolService.http(config, messages));
    }

    fetchControlByVendorControlID(vendorControlId: string): Observable<IProtocolResponse<IVendorControlDetailData>> {
        const config: IProtocolConfig = this.protocolService.customConfig("GET", `/api/inventory/v2/controls/${vendorControlId}/controls`);
        const messages: IProtocolMessages = { Error: { Hide: true } };
        return from(this.protocolService.http(config, messages));
    }

    fetchAllMaturities(): Observable<IProtocolResponse<{ data: IVendorControlMaturityInformation[] }>> {
        const config: IProtocolConfig = this.protocolService.customConfig("GET", `/api/inventory/v2/controls/maturities`);
        const messages: IProtocolMessages = { Error: { Hide: true } };
        return from(this.protocolService.http(config, messages));
    }

    updateVendorControl(vendorControlId: string, payload: IStringMap<string>): Observable<IProtocolResponse<IVendorControlDetailData>> {
        const config: IProtocolConfig = this.protocolService.customConfig("PUT", `/api/inventory/v2/controls/${vendorControlId}/controls`, null, payload);
        const messages: IProtocolMessages = {
            Error: { custom: this.translatePipe.transform("VendorControl.Error.UpdateVendorControlDetails") },
            Success: { custom: this.translatePipe.transform("ChangesSuccessfullySaved") },
        };
        return from(this.protocolService.http(config, messages));
    }

    fetchCertificatesByVendorId(vendorId: string): Observable<IProtocolResponse<IFrameworkCertificateInformation[]>> {
        const config: IProtocolConfig = this.protocolService.customConfig("GET", `/api/vendor/v1/vendors/${vendorId}/certificates`);
        const messages: IProtocolMessages = { Error: { Hide: true } };
        return from(this.protocolService.http(config, messages));
    }

    fetchControlsCertificatesByVendorId(vendorId: string): Observable<IProtocolResponse<IFrameworkCertificateInformation[]>> {
        const config: IProtocolConfig = this.protocolService.customConfig("GET", `/api/vendor/v1/vendors/${vendorId}/controls/frameworks/names`);
        const messages: IProtocolMessages = { Error: { Hide: true } };
        return from(this.protocolService.http(config, messages));
    }

    suggestNewVendor(payload: ISuggestNewVendor): Observable<IProtocolResponse<null>> {
        const config: IProtocolConfig = this.protocolService.customConfig("POST", `/api/vendor/v1/vendors/requests/onboardings/vendorpedia`, null, payload);
        const messages: IProtocolMessages = {
            Success: { custom: this.translatePipe.transform("RequestedNewVendorSuccessfully") },
            Error: { custom: this.translatePipe.transform("RequestingNewVendorFailed") },
        };
        return from(this.protocolService.http(config, messages));
    }

    fetchServiceForVendor(vendorId: string): Observable<IProtocolResponse<Array<INameId<string>>>> {
        const config: IProtocolConfig = this.protocolService.customConfig("GET", `/api/vendor/v1//vendorpedia/vendors/${vendorId}/services`);
        const messages: IProtocolMessages = {
            Error: { custom: this.translatePipe.transform("Vendorpedia.Error.FetchServiceForVendor") },
        };
        return from(this.protocolService.http(config, messages));
    }

    fetchVendorInventoryControlsStatus(): Observable<IProtocolResponse<{ data: string[] }>> {
        const config: IProtocolConfig = this.protocolService.customConfig("GET", `/api/inventory/v2/controls/statuses`);
        const messages: IProtocolMessages = { Error: { Hide: true } };
        return from(this.protocolService.http(config, messages));
    }
}
