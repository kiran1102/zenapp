// Core
import {
    Injectable,
    Inject,
} from "@angular/core";

// 3rd Party
import { BehaviorSubject } from "rxjs";

// Redux
import { StoreToken } from "tokens/redux-store.token";

// Services
import { VendorpediaExchangeApiService } from "vendorpediaExchange/shared/services/vendorpedia-exchange-api.service";

// Interfaces
import { IVendorCatalogInformation } from "modules/vendor/shared/interfaces/vendorpedia-exchange.interface";
import { IStore } from "interfaces/redux.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";

// Enums
import { VendorDetailActions } from "modules/vendor/shared/enums/vendor-actions.enums";

@Injectable()
export class VendorDetailActionService {
    vendorDetailModel$ = new BehaviorSubject<IVendorCatalogInformation> (null);

    constructor(
        @Inject(StoreToken) private store: IStore,
        private vendorpediaApiService: VendorpediaExchangeApiService,
    ) { }

    reset(): void {
        this.vendorDetailModel$.next(null);
    }

    fetchVendorById(vendorId: string): void {
        this.isFetchingVendors(true);
        this.vendorpediaApiService.fetchVendorpediaById(vendorId).then((res: IProtocolResponse<IVendorCatalogInformation>): void => {
            if (res.result) {
                this.store.dispatch({ type: VendorDetailActions.UPDATE_VENDOR_DETAIL, payload: res.data });
                this.vendorDetailModel$.next(res.data);
            }
        });
    }

    isFetchingVendors(isFetching: boolean): void {
        this.store.dispatch({ type: VendorDetailActions.FETCHING_VENDOR_DETAIL, payload: isFetching });
    }

    resetVendorRecords(): void {
        this.store.dispatch({ type: VendorDetailActions.RESET_VENDOR_DETAIL });
    }
}
