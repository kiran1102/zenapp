import {
    Injectable,
    Inject,
} from "@angular/core";

// Redux
import { StoreToken } from "tokens/redux-store.token";

// Services
import { VendorpediaExchangeApiService } from "vendorpediaExchange/shared/services/vendorpedia-exchange-api.service";

// Interfaces
import { IVendorCatalogInformation } from "modules/vendor/shared/interfaces/vendorpedia-exchange.interface";
import { IStore } from "interfaces/redux.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";
import {
    IPageableOf,
    IPaginationFilter,
} from "interfaces/pagination.interface";

// Enums
import { VendorRecordActions } from "modules/vendor/shared/enums/vendor-actions.enums";

@Injectable()
export class VendorRecordActionService {

    constructor(
        @Inject(StoreToken) private store: IStore,
        private vendorpediaApiService: VendorpediaExchangeApiService,
    ) { }

    fetchVendorRecords(searchText = "", filters: IPaginationFilter[] = [], paginationParams = { page: 0, size: 24, sort: "popularity,name,ASC" }): void {
        this.isFetchingVendors(true);
        this.vendorpediaApiService.fetchVendorRecords(searchText, filters, paginationParams)
            .then((res: IProtocolResponse<IPageableOf<IVendorCatalogInformation>>): void => {
                if (res.result) {
                    this.store.dispatch({ type: VendorRecordActions.UPDATE_VENDOR_RECORD, payload: res.data });
                }
            });
    }

    isFetchingVendors(isFetching: boolean): void {
        this.store.dispatch({ type: VendorRecordActions.FETCHING_VENDOR_RECORD, payload: isFetching });
    }

    resetVendorRecords(): void {
        this.store.dispatch({ type: VendorRecordActions.RESET_VENDOR_RECORD });
    }
}
