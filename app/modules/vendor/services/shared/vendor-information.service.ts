// Core
import { Injectable, Inject } from "@angular/core";

// Rxjs
import { BehaviorSubject } from "rxjs";

// Services
import { VendorApiService } from "modules/vendor/services/api/vendor-api.service";
import { VendorpediaExchangeApiService } from "vendorpediaExchange/shared/services/vendorpedia-exchange-api.service";

// Interfaces
import { IStringMap } from "interfaces/generic.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";
import {
    IVendorpediaService,
    IVendorpediaExtendedDetail,
    IVendorSubprocessors,
    INewVendorRecord,
    IVendorRecordOption,
    IVendorRecordV2,
    ICertificationsList,
    IVendorInformation,
} from "modules/vendor/shared/interfaces/vendorpedia-exchange.interface";
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";

@Injectable()
export class VendorInformationService {

    vendorServices$ = new BehaviorSubject<IVendorpediaService[]>(null);
    vendorExtendedDetails$ = new BehaviorSubject<IVendorpediaExtendedDetail>(null);
    vendorSubprocessors$ = new BehaviorSubject<IVendorSubprocessors[]>(null);
    vendorCertificates$ = new BehaviorSubject<IStringMap<IVendorpediaService[]>>(null);
    loadSideMenuAttributes$ = new BehaviorSubject<boolean>(null);
    vendorPreviewPagination$ = new BehaviorSubject<number>(null);
    vendorSearchName$ = new BehaviorSubject<string>(null);

    constructor(
        @Inject("$rootScope") private rootScope: IExtendedRootScopeService,
        private VendorApi: VendorApiService,
        private vendorpediaExchangeApiService: VendorpediaExchangeApiService,
    ) { this.clearRisksCache(); }

    reset(): void {
        this.vendorServices$.next(null);
        this.vendorExtendedDetails$.next(null);
        this.vendorSubprocessors$.next(null);
        this.vendorCertificates$.next(null);
    }

    addVendorRecord(schemaId: string, payload: INewVendorRecord): ng.IPromise<IProtocolResponse<IVendorRecordOption>> {
        return this.VendorApi.addVendorRecord(schemaId, payload);
    }

    updateVendorRecord(schemaId: string, recordId: string, payload: IVendorRecordV2): ng.IPromise<IProtocolResponse<IVendorRecordV2[]>> {
        return this.VendorApi.updateVendorRecord(schemaId, recordId, payload);
    }

    fetchServicesByVendorId(vendorId: string): void {
        this.vendorpediaExchangeApiService.fetchServicesByVendorId(vendorId).subscribe((res: IProtocolResponse<IVendorpediaService[]>): void => {
            if (res.result) {
                this.vendorServices$.next(res.data);
                return;
            }
            this.vendorServices$.next(res.data);
        });
    }

    fetchExtendedVendorDetails(vendorId: string, serviceIds: string[]): void {
        this.vendorpediaExchangeApiService.fetchExtendedVendorDetails(vendorId, serviceIds).then((res: IProtocolResponse<IVendorpediaExtendedDetail>): void => {
            if (res.result) {
                this.vendorExtendedDetails$.next(res.data);
                this.vendorSubprocessors$.next(res.data.subprocessors);
                this.vendorCertificates$.next(res.data.certificates);
            }
        });
    }

    onboardVendor(vendorId: string, payload: IVendorInformation) {
        return this.VendorApi.onboardVendor(vendorId, payload);
    }

    getMockVendorCertificates(): ICertificationsList[] {
        // TODO: Replace with API call to get Vendor Certificates
        return [];
    }

    loadSideMenuAttributes(isShow: boolean) {
        this.loadSideMenuAttributes$.next(isShow);
    }

    vendorPreviewPagination(paginationRecord: number) {
        this.vendorPreviewPagination$.next(paginationRecord);
    }

    private clearRisksCache() {
        this.rootScope.$on("event:auth-logout", () => {
            this.vendorPreviewPagination$.next(null);
            this.vendorSearchName$.next(null);
        });
    }
}
