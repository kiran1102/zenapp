// Angular
import { Injectable } from "@angular/core";
import { ProtocolService } from "modules/core/services/protocol.service";

// RXJS
import {
    from,
    Observable,
} from "rxjs";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

// Interfaces
import {
    IProtocolConfig,
    IProtocolMessages,
    IProtocolResponse,
} from "interfaces/protocol.interface";
import {
    IExpiringContractsResponse,
    IAssessmentsByStatusResponse,
} from "modules/vendor/shared/interfaces/vendor-dashboard.interface.ts";

@Injectable()
export class VendorDashboardService {
    constructor(
        private protocolService: ProtocolService,
        private translatePipe: TranslatePipe,
    ) {}

    fetchExpiringContracts(): Observable<IProtocolResponse<IExpiringContractsResponse>> {
        const config: IProtocolConfig = this.protocolService.customConfig("GET", `/api/vendor/v1/dashboards/contracts/expiry`);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("Vendor.Error.RetrievingContract") } };
        return from(this.protocolService.http(config, messages));
    }

    fetchAssessmentsByStatus(): Observable<IProtocolResponse<IAssessmentsByStatusResponse>> {
        const config: IProtocolConfig = this.protocolService.customConfig("GET", `/assessment-v2/v2/assessments/inventory/types/50/dashboards/assessments/statuses`);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("Vendor.Error.RetrievingContract") } };
        return from(this.protocolService.http(config, messages));
    }
}
