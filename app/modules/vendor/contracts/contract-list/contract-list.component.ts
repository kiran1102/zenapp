// Angular
import {
    Component,
    OnInit,
    Input,
} from "@angular/core";

// Rxjs
import { Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";

// Interfaces
import { IGridPaginationParams } from "interfaces/grid.interface";
import { IPageableMeta } from "interfaces/pagination.interface";
import { IContractRowContent } from "interfaces/vendor/vendor-document.interface";
import { ITableColumnConfig } from "interfaces/table-config.interface";
import { ISortEvent } from "sharedModules/interfaces/sort.interface";
import { IContractAPIConfig } from "contracts/contract-list/contract-list.interface";

// Constants/Enums
import { TableColumnTypes } from "enums/data-table.enum";
import { ContractListContext } from "contracts/contract-list/contract-list.enum";
import {
    ContractGridViewStorageNames,
    AddContractRoutes,
} from "contracts/contract-list/contract-list.constant";

// Services
import { ContractService } from "contracts/shared/services/contract.service";
import { GridViewStorageService } from "sharedModules/services/helper/grid-view-storage.service";
import { ControlsFilterService } from "controls/shared/components/controls-filter/controls-filter.service";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

// Third party
import { StateService } from "@uirouter/core";

@Component({
    selector: "contract-list",
    templateUrl: "./contract-list.component.html",
})
export class ContractListComponent implements OnInit {

    @Input() showHeader = true;
    @Input() context = ContractListContext.CONTRACTS;
    @Input() contextId = null;

    activeColumnList: ITableColumnConfig[] = [];
    defaultPagination: IGridPaginationParams = { page: 0, size: 20, sort: [] };
    paginationParams: IGridPaginationParams = { ...this.defaultPagination };
    paginationDetail: IPageableMeta;
    tableData: IContractRowContent[] = [];
    isPageLoading = true;
    storageKey = ContractGridViewStorageNames[this.context];
    searchText = "";
    sortOrder = "asc";
    sortKey = "vendorName";
    currentFilters: any[] = [];
    isFiltering = false;
    query: any[] = [];

    private destroy$ = new Subject();

    constructor(
        private translatePipe: TranslatePipe,
        private contractService: ContractService,
        private stateService: StateService,
        private browserStorageListService: GridViewStorageService,
        private controlsFilterService: ControlsFilterService,
    ) { }

    ngOnInit() {
        const storage = this.browserStorageListService.getBrowserStorageList(this.storageKey);
        this.searchText = storage && storage.searchText;
        if (storage.sort) {
            this.sortKey = storage.sort.sortKey;
            this.sortOrder = storage.sort.sortOrder;
            this.defaultPagination = { ...this.defaultPagination, sort: `${this.sortKey},${this.sortOrder}` };
            this.paginationParams.sort = `${this.sortKey},${this.sortOrder}`;
        }
        if (storage.pagination) {
            this.paginationParams.page = storage.pagination.page;
        }
        this.currentFilters = this.controlsFilterService.loadFilters(this.storageKey);
        this.query = this.controlsFilterService.buildQuery(this.currentFilters);

        this.activeColumnList = [
            {
                name: this.translatePipe.transform("PrimaryVendor"),
                columnKey: "PrimaryVendor",
                type: TableColumnTypes.Link,
                cellValueKey: "vendorName",
                sortable: true,
                sortKey: "vendorName",
                filterKey: "vendorId",
                canFilter: true,
            },
            {
                name: this.translatePipe.transform("ContractName"),
                type: TableColumnTypes.Link,
                cellValueKey: "name",
            },
            {
                name: this.translatePipe.transform("Type"),
                type: TableColumnTypes.Translate,
                cellValueKey: "contractTypeNameKey",
            },
            {
                name: this.translatePipe.transform("Status"),
                columnKey: "Status",
                type: TableColumnTypes.Translate,
                sortKey: "statusKey",
                filterKey: "status",
                cellValueKey: "statusKey",
                canFilter: true,
            },
            {
                name: this.translatePipe.transform("CreatedDate"),
                type: TableColumnTypes.Date,
                cellValueKey: "agreementCreatedDate",
                sortable: true,
                sortKey: "agreementCreatedDate",
            },
            {
                name: this.translatePipe.transform("ExpirationDate"),
                type: TableColumnTypes.Date,
                cellValueKey: "expirationDate",
                sortable: true,
                sortKey: "expirationDate",
            },
            {
                name: this.translatePipe.transform("ContractOwner"),
                type: TableColumnTypes.Text,
                cellValueKey: "contractOwner",
            },
            {
                name: this.translatePipe.transform("Cost"),
                type: TableColumnTypes.Currency,
                cellValueKey: "cost",
            },
        ];

        this.contractService.contracts$
            .pipe(
                takeUntil(this.destroy$),
            )
            .subscribe((response) => {
                this.isPageLoading = false;
                if (response) {
                    this.tableData = response.content;
                    this.paginationDetail = {
                        first: response.first,
                        last: response.last,
                        number: response.number,
                        numberOfElements: response.numberOfElements,
                        size: response.size,
                        sort: response.sort,
                        totalElements: response.totalElements,
                        totalPages: response.totalPages,
                    };
                }
            });

        this.fetchAllContracts();
    }

    ngOnDestroy(): void {
        this.destroy$.next();
    }

    onPageChanged(page: number) {
        this.paginationParams.page = page;
        this.browserStorageListService.saveSessionStoragePagination(this.storageKey, this.paginationParams);
        this.fetchAllContracts();
    }

    fetchAllContracts() {
        this.isPageLoading = true;
        this.tableData = [];
        const apiConfig: IContractAPIConfig = {
            context: this.context,
            contextId: this.contextId,
            pagination: this.paginationParams,
            searchText: this.searchText,
            query: this.query,
        };
        this.contractService.fetchAllContracts(apiConfig);
    }

    addContract() {
        if (AddContractRoutes[this.context]) {
            this.stateService.go(AddContractRoutes[this.context]);
        }
    }

    onSearchChange(searchText: string = ""): void {
        this.searchText = searchText;
        this.browserStorageListService.saveSessionStorageSearch(this.storageKey, this.searchText);
        this.paginationParams = { ...this.defaultPagination };
        this.browserStorageListService.saveSessionStoragePagination(this.storageKey, this.paginationParams);
        this.fetchAllContracts();
    }

    onSortChange(sortEvent: ISortEvent) {
        this.sortOrder = sortEvent.sortOrder;
        this.sortKey = sortEvent.sortKey;
        this.defaultPagination = { ...this.defaultPagination, sort: `${this.sortKey},${this.sortOrder}` };
        this.paginationParams = { ...this.paginationParams, sort: `${this.sortKey},${this.sortOrder}` };
        this.browserStorageListService.saveSessionStorageSort(this.storageKey, sortEvent);
        this.fetchAllContracts();
    }

    // Filter callbacks
    toggleFilterDrawer() {
        this.isFiltering = !this.isFiltering;
    }

    getFilterableColumns(column: ITableColumnConfig): boolean {
        return Boolean(column.canFilter);
    }

    filterApplied(filters: any) {
        this.isFiltering = false;
        this.query = this.controlsFilterService.buildQuery(filters);
        this.currentFilters = filters;
        this.paginationParams.page = 0;
        this.fetchAllContracts();
    }

    filterCanceled() {
        this.isFiltering = false;
    }
}
