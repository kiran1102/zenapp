import { IGridPaginationParams } from "interfaces/grid.interface";
import { ContractListContext } from "contracts/contract-list/contract-list.enum";

export interface IContractAPIConfig {
    context: ContractListContext;
    contextId: string;
    pagination: IGridPaginationParams;
    searchText: string;
    query: any[];
}
