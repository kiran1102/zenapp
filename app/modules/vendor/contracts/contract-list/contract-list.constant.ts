import { ContractListContext } from "contracts/contract-list/contract-list.enum";
import { GridViewStorageNames } from "sharedModules/enums/grid-view-storage.enum";

export const ContractGridViewStorageNames = {
    [ContractListContext.CONTRACTS]: GridViewStorageNames.CONTRACT_LIST,
    [ContractListContext.ENGAGEMENTS]: GridViewStorageNames.ENGAGEMENT_CONTRACTS,
};

export const AddContractRoutes = {
    [ContractListContext.CONTRACTS]: "zen.app.pia.module.vendor.add-contract",
    [ContractListContext.ENGAGEMENTS]: null,
};

export const ContractsAPI = {
    [ContractListContext.CONTRACTS]: {
        getContracts: (engagementId: string) => "/api/vendor/v1/contracts/pages",
    },
    [ContractListContext.ENGAGEMENTS]: {
        getContracts: (engagementId: string) => `/api/vendor/v1/engagements/${engagementId}/contracts/pages`,
    },
};
