// Angular
import {
    Component,
    Input,
    Output,
    EventEmitter,
} from "@angular/core";
import { StateService } from "@uirouter/core";

// Interfaces
import { IContractRowContent } from "interfaces/vendor/vendor-document.interface";
import { ISortEvent } from "sharedModules/interfaces/sort.interface";

// Constants/Enums
import { TableColumnTypes } from "enums/data-table.enum";
import { InventoryDetailsTabs } from "constants/inventory.constant";
import { InventoryTableIds } from "enums/inventory.enum";
import { ITableColumnConfig } from "interfaces/table-config.interface";

@Component({
    selector: "contracts-table",
    templateUrl: "./contracts-table.component.html",
})
export class ContractsTableComponent {

    @Input() columns: ITableColumnConfig[];
    @Input() tableData: IContractRowContent[];
    @Input() sortOrder: string;
    @Input() sortKey: string;

    @Output() sortChange = new EventEmitter<ISortEvent>();

    tableColumnTypes = TableColumnTypes;
    emptyText = "- - - -";

    constructor(private stateService: StateService) {}

    goToRoute(row: IContractRowContent, col: ITableColumnConfig): void {
        if (col.cellValueKey === "vendorName") {
            this.stateService.go("zen.app.pia.module.vendor.inventory.details", {
                inventoryId: InventoryTableIds.Vendors,
                recordId: row.vendorId,
                tabId: InventoryDetailsTabs.Details,
            });
        }
        if (col.cellValueKey === "name") {
            this.stateService.go("zen.app.pia.module.vendor.contract-detail", {
                contractId: row.id,
                recordId: row.vendorId,
            });
        }
    }

    onSortChange({ sortKey, sortOrder }: ISortEvent) {
        this.sortChange.emit({ sortKey, sortOrder });
    }

}
