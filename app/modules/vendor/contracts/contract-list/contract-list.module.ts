import { NgModule } from "@angular/core";

// Modules
import { SharedModule } from "sharedModules/shared.module";
import { UpgradedLegacyModule } from "sharedModules/upgrade/upgraded-legacy.module";
import { CommonModule } from "@angular/common";
import { HttpClientModule } from "@angular/common/http";
import { FormsModule } from "@angular/forms";
import { ReactiveFormsModule } from "@angular/forms";
import { VitreusModule } from "@onetrust/vitreus";
import { OTPipesModule } from "modules/pipes/pipes.module";
import { InventorySharedModule } from "modules/inventory/inventory-shared/inventory-shared.module";
import { ControlsSharedModule } from "modules/controls/shared/controls-shared.module";

// Components
import { AddContractComponent } from "contracts/contract-list/add-contract/add-contract.component";
import { ContractListComponent } from "contracts/contract-list/contract-list.component";
import { ContractsTableComponent } from "contracts/contract-list/contracts-table/contracts-table.component";

@NgModule({
    imports: [
        SharedModule,
        UpgradedLegacyModule,
        CommonModule,
        HttpClientModule,
        FormsModule,
        ReactiveFormsModule,
        VitreusModule,
        OTPipesModule,
        InventorySharedModule,
        ControlsSharedModule,
    ],
    declarations: [
        AddContractComponent,
        ContractListComponent,
        ContractsTableComponent,
    ],
    entryComponents: [
        AddContractComponent,
        ContractListComponent,
    ],
    exports: [
        ContractListComponent,
    ],
    providers: [],
})
export class ContractListModule {}
