// 3rd party
import {
    Component,
    Inject,
    OnInit,
    OnDestroy,
    ViewChild,
} from "@angular/core";
import { StateService } from "@uirouter/core";

import {
    replace,
} from "lodash";

// Redux
import { StoreToken } from "tokens/redux-store.token";
import { getCurrentOrgList } from "oneRedux/reducers/org.reducer";
import observableFromStore from "oneRedux/utils/observable-from-store";
import { getRecord } from "oneRedux/reducers/inventory-record.reducer";
import { getCurrentOrgId } from "oneRedux/reducers/org.reducer";

// Rxjs
import { Subject } from "rxjs";
import {
    startWith,
    takeUntil,
} from "rxjs/operators";

// Services
import { VendorApiService } from "modules/vendor/services/api/vendor-api.service";
import { ContractService } from "contracts/shared/services/contract.service";
import { TimeStamp } from "oneServices/time-stamp.service";
import { ContractsApiService } from "modules/vendor/contracts/shared/services/contracts-api.service";
import { NotificationService } from "modules/shared/services/provider/notification.service";

// Interfaces
import { IAttachmentFile } from "interfaces/attachment-file.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";
import {
    IContractTypeResponse,
    IContractDetails,
    ICurrenciesResponse,
    IRelatedListOptionData,
    IRelatedAssetsPA,
    IContractAttachment,
} from "modules/vendor/shared/interfaces/vendor-upload-document.interface";
import { IFileInput } from "interfaces/sub-tasks.interface";
import { IDateTimeOutputModel } from "@onetrust/vitreus";
import { IOtOrgUserOutput } from "sharedModules/components/ot-org-user/ot-org-user.interface";
import {
    IStore,
    IStoreState,
} from "interfaces/redux.interface";
import { IRelatedListOption } from "interfaces/inventory.interface";
import { IOrganization } from "interfaces/org.interface";

// Constants / Enums
import { InventoryTableIds } from "enums/inventory.enum";
import { OrgUserTraversal } from "enums/org-user-dropdown.enum";
import { ILabelValue } from "interfaces/generic.interface";
import { FileConstraints } from "constants/file.constant";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";
import { PermitPipe } from "modules/pipes/permit.pipe";

@Component({
    selector: "add-contract",
    templateUrl: "./add-contract.component.html",
})
export class AddContractComponent implements OnInit, OnDestroy {

    @ViewChild("fileSelect") fileSelectRef;

    vendorContractInventoryLink = this.permitPipe.transform("VendorContractInventoryLink");
    vRMContractMultipleAttachments = this.permitPipe.transform("VRMContractMultipleAttachments");
    inventoryTableIds = InventoryTableIds;
    selectedAssets: IRelatedAssetsPA[] = [];
    selectedPA: IRelatedAssetsPA[] = [];
    selectedVendors: IRelatedAssetsPA[] = [];
    orgUserError: boolean;
    orgUserTraversal = OrgUserTraversal;
    selectedApproversSigners: string;
    selectedOwner: string;
    selectedOwnerId: string;
    selectedOwnerEmail: string;
    selectedContact: string;
    selectedApprovers: string;
    vendorId: string;
    selectedTypeName: string;
    selectedTypeId: string;
    requestId: string;
    selectedCurrencyCode: string;
    toggleButtonIcon: string;
    dateCreatedValue: string;
    dateApprovedValue: string;
    dateExpirationValue: string;
    enteredNotes: string;
    selectedCost: number;
    statusList: Array<ILabelValue<string>>;
    selectedStatus: ILabelValue<string>;
    currencyList: ICurrenciesResponse[];
    selectedCurrency: ICurrenciesResponse;
    typeList: IContractTypeResponse[];
    selectedType: IContractTypeResponse;
    attachments: IAttachmentFile[] = [];
    contractName = "";
    contractUrl = "";
    isLoading = true;
    isSaving = false;
    isExpanded: boolean;
    setReminderEnabled = false;
    canAdd = false;
    hideAutoRenew = false;
    selectedOrganization: IOrganization;
    organizations: IOrganization[];
    destroy$ = new Subject();
    orgGroupId: string;
    dateCreatedModel: string;
    dateApprovedModel: string;
    dateExpirationModel: string;
    selectedAssetsModel: IRelatedListOption[] = [];
    selectedPAModel: IRelatedListOption[] = [];
    selectedVendorsModel: IRelatedListOption[] = [];
    selectedPrimaryVendorsModel: IRelatedListOption;
    format = this.timeStamp.getDatePickerDateFormat();

    allowedExtensions = ["csv", "doc", "docx", "jpg", "jpeg", "mpp", "msg", "pdf", "png", "ppt", "pptx", "txt", "vsd", "vsdx", "xls", "xlsx"];
    isHovering: boolean;
    fileDescription = "";
    fileSizeError = false;
    isSavingFile = false;

    private attachmentResponse: IContractAttachment[];

    constructor(
        @Inject(StoreToken) private store: IStore,
        private stateService: StateService,
        private VendorApi: VendorApiService,
        private contractService: ContractService,
        private contractsApiService: ContractsApiService,
        private timeStamp: TimeStamp,
        private translatePipe: TranslatePipe,
        private permitPipe: PermitPipe,
        private notificationService: NotificationService,
    ) { }

    ngOnInit(): void {
        this.contractService.contractStatuses$.pipe(
            takeUntil(this.destroy$),
        ).subscribe((statuses: Array<ILabelValue<string>>) => {
            if (statuses) {
                this.statusList = statuses;
                this.isLoading = false;
            }
        });
        this.organizations = getCurrentOrgList(this.store.getState());
        this.selectedOrganization = this.organizations[0];
        this.getContractTypes();
        this.getAllCurrencies();
        this.contractService.retrieveContractStatuses();
        this.toggleButtonIcon = this.isExpanded ? "ot ot-caret-down" : "fa fa-caret-right";
        observableFromStore(this.store).pipe(
            startWith(this.store.getState()),
            takeUntil(this.destroy$),
        ).subscribe((state: IStoreState) => this.componentWillReceiveState(state));
    }

    ngOnDestroy() {
        this.destroy$.next();
        this.destroy$.complete();
    }

    triggerFileUpload(): void {
        this.fileSelectRef.nativeElement.click();
    }

    handleFiles(files: File[]): void {
        this.fileSizeError = false;
        if (!files.length) {
            return;
        }
        if (files[0].size === 0 || files[0].size > FileConstraints.FILE_SIZE_LIMIT) {
            this.fileSizeError = true;
            this.notificationService.alertError(this.translatePipe.transform("InvalidFile"),
                this.translatePipe.transform("FileSizeLimitViolation"));
        } else if (files[0].name.length > FileConstraints.FILE_NAME_MAX_LENGTH) {
            this.notificationService.alertError(this.translatePipe.transform("InvalidFile"),
                this.translatePipe.transform("FileNameExceedsLimitOf100Characters"));
        } else {
            this.getDocument(files);
        }
    }

    handleInvalidFiles(files: File[]): void {
        if (!files.length) {
            return;
        }

        if (files[0].size === 0 || files[0].size > FileConstraints.FILE_SIZE_LIMIT) {
            this.fileSizeError = true;
            this.notificationService.alertError(this.translatePipe.transform("InvalidFile"), this.translatePipe.transform("FileSizeLimitViolation"));
        } else {
            this.notificationService.alertError(this.translatePipe.transform("ThisFileTypeIsNotAllowed"),
                this.translatePipe.transform("AllowedFileFormats",
                    { formats: this.allowedExtensions.join(", ") }));
        }
    }

    handleFilesHover(isHovering: boolean): void {
        this.isHovering = isHovering;
    }

    removeFile(index: number): void {
        if (!this.isSavingFile) {
            this.attachments.splice(index, 1);
            this.validateSubmit();
        }
    }

    onHeaderRouteClick(route: { path: string, params: any }) {
        this.stateService.go(route.path);
    }

    toggleForm(): void {
        this.isExpanded = !this.isExpanded;
        this.toggleButtonIcon = this.isExpanded ? "ot ot-caret-down" : "fa fa-caret-right";
    }

    toggleSetReminder(): void {
        this.setReminderEnabled = !this.setReminderEnabled;
    }

    getFileExtension(fileName: string): string {
        // Return only file extension
        return fileName.split(".").pop() || "";
    }

    getFileName(fileName: string): string {
        const ext = this.getFileExtension(fileName);
        // Return file name without extension
        return replace(fileName, `.${ext}`, "");
    }

    uploadDocument() {
        if (!this.canAdd) return;
        this.isLoading = true;
        this.isSaving = true;
        const promiseArr: Array<ng.IPromise<IProtocolResponse<IAttachmentFile>>> = [];
        this.attachments.forEach((file: IAttachmentFile) => {
            const fileData: IAttachmentFile = {
                Comments: file.Comments,
                FileName: `${file.FileName}.${file.Extension}`,
                Extension: file.Extension,
                Blob: file.Blob,
                IsInternal: true,
                Name: file.FileName,
                RequestId: this.requestId,
                AttachmentRefId: file.Id,
            };
            promiseArr.push(this.VendorApi.uploadDocument(fileData));
        });
        if (!promiseArr.length) {
            this.addUploadedDocument();
        } else {
            Promise.all(promiseArr)
                .then((responses: Array<IProtocolResponse<IAttachmentFile>>): void => {
                    this.attachmentResponse = [];
                    if (Array.isArray(responses) && responses.length) {
                        const failedResponses: Array<IProtocolResponse<IAttachmentFile>> = responses.filter((response) => !response.result);
                        if (!failedResponses.length) {
                            responses.forEach((res: IProtocolResponse<IAttachmentFile>): void => {
                                const attachmentObjectForComment: IContractAttachment = {
                                    attachmentId: res.data.Id,
                                    fileName: res.data.FileName,
                                    attachmentDescription: res.data.Comments,
                                };
                                this.attachmentResponse.push(
                                    attachmentObjectForComment,
                                );
                            },
                            );
                            this.addUploadedDocument();
                        } else {
                            this.isLoading = false;
                            this.isSaving = false;
                        }
                    }
                });
        }
    }

    selectDateCreated(dateValue: IDateTimeOutputModel): void {
        this.dateCreatedValue = dateValue.dateTime ? this.timeStamp.formatDateToIso({ day: dateValue.jsdate.getDate(), month: dateValue.jsdate.getMonth() + 1, year: dateValue.jsdate.getFullYear() }) : null;
        this.dateCreatedModel = dateValue.date;
        this.validateSubmit();
    }

    selectDateApproved(dateValue: IDateTimeOutputModel): void {
        this.dateApprovedValue = dateValue.dateTime ? this.timeStamp.formatDateToIso({ day: dateValue.jsdate.getDate(), month: dateValue.jsdate.getMonth() + 1, year: dateValue.jsdate.getFullYear() }) : null;
        this.dateApprovedModel = dateValue.date;
    }

    selectDateExpiration(dateValue: IDateTimeOutputModel): void {
        this.dateExpirationValue = dateValue.dateTime ? this.timeStamp.formatDateToIso({ day: dateValue.jsdate.getDate(), month: dateValue.jsdate.getMonth() + 1, year: dateValue.jsdate.getFullYear() }) : null;
        this.dateExpirationModel = dateValue.date;
    }

    updateSelectedType(type: IContractTypeResponse) {
        this.selectedType = type;
        this.selectedTypeId = type.id;
        this.selectedTypeName = type.name;
        this.requestId = type.id;
        this.validateSubmit();
    }

    updateSelectedCurrency(currency: ICurrenciesResponse) {
        this.selectedCurrency = currency;
        this.selectedCurrencyCode = currency.code;
    }

    updateStatus(status: ILabelValue<string>) {
        this.selectedStatus = status;
        this.validateSubmit();
    }

    selectContractOwner(option: IOtOrgUserOutput): void {
        // we have to send null to backend if we are removing a selected owner.
        if (option && option.selection && option.selection.Id) {
            // Since the component is returning Id and FullName same for External/Invited user Id
            if (option.selection.Id === option.selection.FullName) {
                option.selection.Id = null;
            }
            this.selectedOwnerEmail = option.selection.FullName;
            this.selectedOwner = option.selection.Id || option.selection.FullName;
            this.selectedOwnerId = option.selection.Id;
        } else {
            this.selectedOwner = null;
            this.selectedOwnerEmail = null;
            this.selectedOwnerId = null;
        }
    }

    selectPrimaryVendor({ options }: IRelatedListOptionData): void {
        this.selectedPrimaryVendorsModel = options ? options[0] : null;
        if (this.validateSelectedVendor()) {
            this.notificationService.alertWarning(
                this.translatePipe.transform("Warning"),
                this.translatePipe.transform("PrimaryAndRelatedVendorShouldNotBeSame"));
        }
        this.validateSubmit();
    }

    selectedInventoryList(optionsData: IRelatedListOptionData): void {
        const { options, type } = optionsData;
        if (options) {
            if (type === this.inventoryTableIds.Assets) {
                this.selectedAssets = [];
                this.selectedAssetsModel = options as IRelatedListOption[];
                (options as IRelatedListOption[]).forEach((option) => {
                    this.selectedAssets.push({
                        inventoryType: "ASSETS",
                        inventoryName: option.name,
                        inventoryId: option.id,
                        orgGroupId: option.organization.id,
                        attributes: {
                            location: {
                                id: option.location.id,
                                value: option.location.value,
                                valueKey: option.location.value || "",
                            },
                        },
                    });
                });
            } else if (type === this.inventoryTableIds.Processes) {
                this.selectedPA = [];
                this.selectedPAModel = (options as IRelatedListOption[]);
                (options as IRelatedListOption[]).forEach((option) => {
                    this.selectedPA.push({
                        inventoryType: "PROCESSING_ACTIVITIES",
                        inventoryName: option.name,
                        inventoryId: option.id,
                        orgGroupId: option.organization.id,
                    });
                });
            } else if (type === this.inventoryTableIds.Vendors) {
                this.selectedVendors = [];
                this.selectedVendorsModel = (options as IRelatedListOption[]);
                (options as IRelatedListOption[]).forEach((option) => {
                    this.selectedVendors.push({
                        inventoryType: "VENDORS",
                        inventoryName: option.name,
                        inventoryId: option.id,
                        orgGroupId: option.organization.id,
                        attributes: {
                            type: {
                                id: option.type.id,
                                value: option.type.value,
                                valueKey: option.type.value || "",
                            },
                        },
                    });
                });
            }
        }
        if (this.validateSelectedVendor()) {
            this.notificationService.alertWarning(this.translatePipe.transform("Warning"), this.translatePipe.transform("PrimaryAndRelatedVendorShouldNotBeSame"));
        }
        this.validateSubmit();
    }

    selectApproversSigners(option: IOtOrgUserOutput) {
        // we have to send null to backend if we are removing a selected owner.
        if (option && option.selection && option.selection.Id) {
            // Since the component is returning Id and FullName same for External/Invited user Id
            if (option.selection.Id === option.selection.FullName) {
                option.selection.Id = null;
            }
            this.selectedApproversSigners = option.selection.FullName;
        } else {
            this.selectedApproversSigners = null;
        }
    }

    selectCostValue(event: number) {
        this.selectedCost = event;
    }

    selectVendorContact(event: string) {
        this.selectedContact = event;
    }

    selectApprovers(event: string) {
        this.selectedApprovers = event;
    }

    handleEnteredNotes(note: string) {
        this.enteredNotes = note;
    }

    goToContracts() {
        this.stateService.go("zen.app.pia.module.vendor.contract-list");
    }

    contractNameChange(name) {
        this.contractName = name.trim();
        this.validateSubmit();
    }

    contractUrlChange(name) {
        this.contractUrl = name;
        this.validateSubmit();
    }

    private getDocument(files: File[]) {
        if (files.length) {
            files.forEach((file: File) => {
                const reader: FileReader = new FileReader();
                const fileObj: IFileInput = {
                    Data: "",
                    FileName: this.getFileName(file.name),
                    Extension: this.getFileExtension(file.name),
                    Blob: file,
                };
                reader.readAsDataURL(file);
                reader.onload = () => {
                    fileObj.Data = reader.result.toString();
                    if (fileObj.Data.length) {
                        this.attachments.push(fileObj);
                        this.validateSubmit();
                    }
                };
            });
        }
    }

    private componentWillReceiveState(state) {
        this.orgGroupId = getCurrentOrgId(state);
    }

    private getAllCurrencies(): ng.IPromise<IProtocolResponse<ICurrenciesResponse[]>> {
        return this.contractsApiService.fetchAllCurrencies().then(
            (response: IProtocolResponse<ICurrenciesResponse[]>): IProtocolResponse<ICurrenciesResponse[]> => {
                if (response.result) {
                    this.currencyList = response.data;
                    return response;
                }
            });
    }

    private getContractTypes(): ng.IPromise<IProtocolResponse<IContractTypeResponse[]>> {
        return this.contractsApiService.fetchAllContractTypes().then(
            (response: IProtocolResponse<IContractTypeResponse[]>): IProtocolResponse<IContractTypeResponse[]> => {
                if (response.result) {
                    this.typeList = response.data;
                    this.typeList.forEach((contractType: IContractTypeResponse) => {
                        contractType.name = contractType.nameKey ? this.translatePipe.transform(contractType.nameKey) : contractType.name;
                    });
                    return response;
                }
            });
    }

    private addUploadedDocument(): void {
        const newContract: IContractDetails = {
            orgGroupId: this.selectedPrimaryVendorsModel.organization.id,
            name: this.contractName,
            vendorId: this.selectedPrimaryVendorsModel.id,
            vendorName: this.selectedPrimaryVendorsModel.name,
            status: this.selectedStatus.value,
            agreementCreatedDate: this.dateCreatedValue,
            expirationDate: this.dateExpirationValue,
            approvedDate: this.dateApprovedValue,
            attachments: this.attachmentResponse || [],
            contractOwner: this.selectedOwner,
            contractOwnerId: this.selectedOwnerId,
            contractOwnerEmail: this.selectedOwner,
            contractTypeId: this.selectedTypeId,
            contractTypeName: this.selectedTypeName,
            url: this.contractUrl,
            vendorContact: this.selectedContact,
            approvers: this.selectedApprovers,
            note: this.enteredNotes,
            cost: this.selectedCost,
            currency: this.selectedCurrencyCode,
            links: [...this.selectedAssets, ...this.selectedPA, ...this.selectedVendors],
        };
        this.contractsApiService.addContract(newContract).then((response: IProtocolResponse<string>): void => {
            if (response.result) {
                this.stateService.go("zen.app.pia.module.vendor.contract-list");
            } else {
                this.isLoading = false;
            }
        });
    }

    private validateSubmit(): void {
        this.canAdd = Boolean(
            this.selectedType && this.contractName && this.dateCreatedValue
            && this.selectedStatus && this.selectedPrimaryVendorsModel && !this.fileSizeError && !this.validateSelectedVendor(),
        );
    }

    private validateSelectedVendor(): IRelatedAssetsPA {
        let identicalVendors: IRelatedAssetsPA;
        if (this.selectedPrimaryVendorsModel && this.selectedVendors) {
            identicalVendors = this.selectedVendors.find((vendor) => vendor.inventoryId === this.selectedPrimaryVendorsModel.id);
        }
        return identicalVendors;
    }
}
