// Core
import {
    Component,
    Inject,
    OnInit,
} from "@angular/core";
import { StateService } from "@uirouter/core";

// Rxjs
import {
    Subject,
    combineLatest,
} from "rxjs";
import {
    takeUntil,
    skip,
} from "rxjs/operators";

// 3rd party
import {
    reverse,
    toString,
    isUndefined,
    cloneDeep,
    find,
} from "lodash";

// Redux
import { getUserById } from "oneRedux/reducers/user.reducer";
import { StoreToken } from "tokens/redux-store.token";

// Services
import { TimeStamp } from "oneServices/time-stamp.service";
import { ContractService } from "contracts/shared/services/contract.service";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

// Enums/Constants
import {
    VendorContractActivityTypes,
    VendorContractActivityFieldTypes,
} from "modules/vendor/shared/enums/contract-activity.enums";
import { InventoryTableNames } from "constants/inventory.constant";
import { Color } from "constants/color.constant";
import {
    ActivityColor,
    ActivityIcon,
} from "constants/activity-stream.constant";
import { StatusTranslationKeys } from "modules/vendor/shared/constants/vendor-documents.constants";

// Interfaces
import { IStringMap } from "interfaces/generic.interface";
import { IStore } from "interfaces/redux.interface";
import {
    IFormattedActivity,
    IAuditChange,
} from "interfaces/audit-history.interface";
import { IPageableMeta } from "interfaces/pagination.interface";
import { IAuditHistoryItem } from "interfaces/audit-history.interface";
import { IContractTypeResponse } from "modules/vendor/shared/interfaces/vendor-upload-document.interface";

@Component({
    selector: "contract-activity",
    templateUrl: "./contract-activity.component.html",
})
export class ContractActivityComponent implements OnInit {
    activitiesAscending = true;
    allItemsExpanded = true;
    contractId = this.stateService.params.contractId;
    contractTypes: IContractTypeResponse[];
    expandedItemMap: IStringMap<boolean> = {};
    formattedActivities: IFormattedActivity[] = [];
    hasMoreActivities: boolean;
    isLoadingMoreActivities = true;
    orderIcon = ActivityIcon.OrderAscend;

    private activityList: IAuditHistoryItem[];
    private activityListMetaInfo: IPageableMeta;
    private destroy$ = new Subject();

    constructor(
        @Inject(StoreToken) private store: IStore,
        private stateService: StateService,
        private contractService: ContractService,
        readonly timeStamp: TimeStamp,
        private translatePipe: TranslatePipe,
    ) { }

    ngOnInit() {
        combineLatest(
            this.contractService.vendorContractActivityList$,
            this.contractService.vendorContractTypes$,
        ).pipe(
            takeUntil(this.destroy$),
            skip(1),
        ).subscribe(([contractActivityList, contractTypes]) => {
            if (contractActivityList && contractTypes) {
                this.activityList = cloneDeep(contractActivityList);
                this.activityListMetaInfo = this.contractService.vendorContractActivityListMetaData$.getValue();
                this.hasMoreActivities = this.activityListMetaInfo ? !this.activityListMetaInfo.last : false;
                this.contractTypes = contractTypes;
                this.formatData();
                this.setExpandedItemMap();
                if (!this.activitiesAscending) {
                    this.reverseActivityOrder();
                }
                this.isLoadingMoreActivities = false;
            }
        });

        this.contractService.retrieveContractActivity(this.contractId);
        this.contractService.retrieveContractTypes();
    }

    ngOnDestroy() {
        this.destroy$.next();
        this.destroy$.complete();
    }

    public addActivitiesToView() {
        if (this.hasMoreActivities) {
            this.isLoadingMoreActivities = true;
            this.contractService.retrieveMoreContractActivities(this.contractId, this.activityListMetaInfo.number + 1);
        }
    }

    public toggleActivity(itemId: string) {
        this.expandedItemMap[itemId] = !this.expandedItemMap[itemId];
        if (Object.values(this.expandedItemMap).every((item) => item === false) && this.allItemsExpanded) {
            this.setAllItemsExpanded();
        } else if (Object.values(this.expandedItemMap).every((item) => item === true) && !this.allItemsExpanded) {
            this.setAllItemsExpanded();
        }
    }

    public setExpandedItemMap() {
        this.formattedActivities.forEach((activity: IFormattedActivity) => {
            if (isUndefined(this.expandedItemMap[activity.id])) {
                this.expandedItemMap[activity.id] = this.allItemsExpanded;
            }
        });
    }

    public setAllItemsExpanded() {
        this.allItemsExpanded = !this.allItemsExpanded;
    }

    public expandAll() {
        Object.keys(this.expandedItemMap).forEach((key) => this.expandedItemMap[key] = true);
    }

    public collapseAll() {
        Object.keys(this.expandedItemMap).forEach((key) => this.expandedItemMap[key] = false);
    }

    public toggleExpand() {
        if (this.allItemsExpanded) {
            this.collapseAll();
        } else {
            this.expandAll();
        }
        this.setAllItemsExpanded();
    }

    public toggleOrder() {
        this.activitiesAscending = !this.activitiesAscending;
        this.orderIcon = this.activitiesAscending ? ActivityIcon.OrderAscend : ActivityIcon.OrderDescend;
        this.reverseActivityOrder();
    }

    private reverseActivityOrder() {
        this.formattedActivities = reverse(this.formattedActivities);
    }

    private formatData() {
        this.formattedActivities = [];
        this.activityList.map((activity: IAuditHistoryItem, index: number) => {
            if (activity && activity.changes && activity.changes.length) {
                activity.userName = getUserById(activity.userId)(this.store.getState()) ? getUserById(activity.userId)(this.store.getState()).FullName : this.translatePipe.transform("UserNotFound");
                const formattedActivity: IFormattedActivity = {
                    id: toString(index),
                    title: this.translatePipe.transform("UserMadeContractChanges", { userName: activity.userName }),
                    fieldLabel: this.translatePipe.transform("FieldName"),
                    description: activity.description,
                    timeStamp: this.timeStamp.formatDate(activity.changeDateTime),
                    icon: ActivityIcon.Edit,
                    iconColor: Color.White,
                    iconBackgroundColor: ActivityColor.Edit,
                    changes: activity.changes,
                    showOldNewValueField: true,
                };

                switch (activity.description) {
                    case VendorContractActivityTypes.CONTRACT_CREATE:
                        formattedActivity.title = this.translatePipe.transform("UserCreatedVendorContract", { userName: activity.userName });
                        formattedActivity.icon = ActivityIcon.Created;
                        formattedActivity.iconColor = Color.White;
                        formattedActivity.iconBackgroundColor = ActivityColor.Created;
                        formattedActivity.fieldLabel = this.translatePipe.transform("ContractName");
                        formattedActivity.showOldNewValueField = false;
                        break;
                    case VendorContractActivityTypes.CONTRACT_LINK_ADDITION:
                        formattedActivity.title = this.translatePipe.transform("UserAddedContractLink", { userName: activity.userName });
                        formattedActivity.icon = ActivityIcon.Linked;
                        formattedActivity.iconBackgroundColor = ActivityColor.Linked;
                        formattedActivity.showOldNewValueField = false;
                        break;
                    case VendorContractActivityTypes.CONTRACT_LINK_REMOVAL:
                        formattedActivity.title = this.translatePipe.transform("UserRemovedContractLink", { userName: activity.userName });
                        formattedActivity.icon = ActivityIcon.Unlinked;
                        formattedActivity.iconBackgroundColor = ActivityColor.Unlinked;
                        formattedActivity.showOldNewValueField = false;
                        break;
                    case VendorContractActivityTypes.CONTRACT_REMOVAL:
                        formattedActivity.title = this.translatePipe.transform("UserRemovedContract", { userName: activity.userName });
                        formattedActivity.icon = ActivityIcon.Delete;
                        formattedActivity.iconBackgroundColor = ActivityColor.Delete;
                        formattedActivity.fieldLabel = this.translatePipe.transform("ContractName");
                        formattedActivity.showOldNewValueField = false;
                        break;
                    case VendorContractActivityTypes.ATTACHMENT_REMOVED:
                        formattedActivity.title = this.translatePipe.transform("UserRemovedAttachment", { userName: activity.userName });
                        formattedActivity.icon = ActivityIcon.Delete;
                        formattedActivity.iconBackgroundColor = ActivityColor.Delete;
                        formattedActivity.fieldLabel = this.translatePipe.transform("AttachmentName");
                        formattedActivity.showOldNewValueField = false;
                        break;
                    case VendorContractActivityTypes.ATTACHMENT_ADDED:
                        formattedActivity.title = this.translatePipe.transform("UserAddedAttachment", { userName: activity.userName });
                        formattedActivity.icon = ActivityIcon.Created;
                        formattedActivity.iconBackgroundColor = ActivityColor.Created;
                        formattedActivity.fieldLabel = this.translatePipe.transform("AttachmentName");
                        formattedActivity.showOldNewValueField = false;
                        break;
                    case VendorContractActivityTypes.CONTRACT_UPDATE:
                        formattedActivity.title = this.translatePipe.transform("UserUpdatedContract", { userName: activity.userName });
                        break;
                }

                formattedActivity.changes = this.massageChanges(formattedActivity);
                this.formattedActivities.push({ ...formattedActivity });
            }
        });
    }

    private convertActivityDateField(change: IAuditChange): IAuditChange {
        if (change.newValue && change.newValue.name) {
            change.newValue.name = this.timeStamp.formatDateWithoutTimezone(change.newValue.name);
        }
        if (change.oldValue && change.oldValue.name) {
            change.oldValue.name = this.timeStamp.formatDateWithoutTimezone(change.oldValue.name);
        }

        return change;
    }

    private convertValueTranslations(change: IAuditChange): IAuditChange {
        if (change.newValue && change.newValue.name) {
            change.newValue.name = this.handleContractTypeTranslation(change.newValue.name);
        }

        if (change.oldValue && change.oldValue.name) {
            change.oldValue.name = this.handleContractTypeTranslation(change.oldValue.name);
        }

        return change;
    }

    private handleContractTypeTranslation(type: string): string {
        const contractType: IContractTypeResponse = find(this.contractTypes, (obj) => obj.name === type);
        return contractType && contractType.nameKey ? this.translatePipe.transform(contractType.nameKey) : type;
    }

    private convertStatusTranslations(change: IAuditChange): IAuditChange {
        if (change.newValue && change.newValue.name) {
            change.newValue.name = this.handleStatusTranslations(change.newValue.name);
        }

        if (change.oldValue && change.oldValue.name) {
            change.oldValue.name = this.handleStatusTranslations(change.oldValue.name);
        }

        return change;
    }

    private handleStatusTranslations(status: string): string {
        return this.translatePipe.transform(StatusTranslationKeys[status]);
    }

    private convertCost(change: IAuditChange): IAuditChange {
        if (change.newValue && change.newValue.name && !change.newValue.name.includes(".")) {
            change.newValue.name = change.newValue.name.toString() + ".00";
        }

        return change;
    }

    private convertFieldName(change: IAuditChange): IAuditChange {
        if (change.newValue && change.newValue.name) {
            change.fieldName = change.newValue.name;
        }
        if (change.oldValue && change.oldValue.name) {
            change.fieldName = change.oldValue.name;
        }

        return change;
    }

    private convertInventoryType(change: IAuditChange): IAuditChange {
        if (change.newValue && change.newValue.name) {
            change.fieldName = this.translatePipe.transform(InventoryTableNames.Singular[change.newValue.name]);
        }
        if (change.oldValue && change.oldValue.name) {
            change.fieldName = this.translatePipe.transform(InventoryTableNames.Singular[change.oldValue.name]);
        }

        return change;
    }

    private massageChanges(formattedActivity: IFormattedActivity): IAuditChange[] {
        const changes = formattedActivity.changes.map((change) => {
            switch (change.fieldName) {
                case VendorContractActivityFieldTypes.CONTRACT_TYPE_ID:
                    change.fieldLabel = formattedActivity.fieldLabel;
                    change.fieldName = this.translatePipe.transform("Type");
                    change = this.convertValueTranslations(change);
                    break;
                case VendorContractActivityFieldTypes.NAME:
                    change.fieldLabel = formattedActivity.fieldLabel;
                    change.fieldName = this.translatePipe.transform("ContractName");
                    change = formattedActivity.description === VendorContractActivityTypes.CONTRACT_CREATE ? this.convertFieldName(change) : change;
                    break;
                case VendorContractActivityFieldTypes.STATUS:
                    change.fieldLabel = formattedActivity.fieldLabel;
                    change.fieldName = this.translatePipe.transform("Status");
                    change = this.convertStatusTranslations(change);
                    break;
                case VendorContractActivityFieldTypes.NOTE:
                    change.fieldLabel = formattedActivity.fieldLabel;
                    change.fieldName = this.translatePipe.transform("Note");
                    break;
                case VendorContractActivityFieldTypes.COST:
                    change.fieldLabel = formattedActivity.fieldLabel;
                    change.fieldName = this.translatePipe.transform("Cost");
                    change = this.convertCost(change);
                    break;
                case VendorContractActivityFieldTypes.CONTRACT_OWNER_ID:
                    change.fieldLabel = formattedActivity.fieldLabel;
                    change.fieldName = this.translatePipe.transform("ContractOwner");
                    break;
                case VendorContractActivityFieldTypes.FILE_NAME:
                    change.fieldLabel = formattedActivity.fieldLabel;
                    change = this.convertFieldName(change);
                    break;
                case VendorContractActivityFieldTypes.VENDOR_CONTACT:
                    change.fieldLabel = formattedActivity.fieldLabel;
                    change.fieldName = this.translatePipe.transform("VendorContact");
                    break;
                case VendorContractActivityFieldTypes.APPROVERS:
                    change.fieldLabel = formattedActivity.fieldLabel;
                    change.fieldName = this.translatePipe.transform("Approvers/Signers");
                    break;
                case VendorContractActivityFieldTypes.CURRENCY:
                    change.fieldLabel = formattedActivity.fieldLabel;
                    change.fieldName = this.translatePipe.transform("Currency");
                    break;
                case VendorContractActivityFieldTypes.APPROVED_DATE:
                    change.fieldLabel = formattedActivity.fieldLabel;
                    change.fieldName = this.translatePipe.transform("ApprovedDate");
                    change = this.convertActivityDateField(change);
                    break;
                case VendorContractActivityFieldTypes.AGREEMENT_CREATED_DATE:
                    change.fieldLabel = formattedActivity.fieldLabel;
                    change.fieldName = this.translatePipe.transform("DateCreated");
                    change = this.convertActivityDateField(change);
                    break;
                case VendorContractActivityFieldTypes.EXPIRATION_DATE:
                    change.fieldLabel = formattedActivity.fieldLabel;
                    change.fieldName = this.translatePipe.transform("ExpirationDate");
                    change = this.convertActivityDateField(change);
                    break;
                case VendorContractActivityFieldTypes.INVENTORY_NAME:
                    change.fieldLabel = this.translatePipe.transform("Name");
                    change = this.convertFieldName(change);
                    break;
                case VendorContractActivityFieldTypes.INVENTORY_TYPE:
                    change.fieldLabel = this.translatePipe.transform("InventoryType");
                    change = this.convertInventoryType(change);
                    break;
                default:
                    change.fieldLabel = formattedActivity.fieldLabel;
                    if (change.newValue && change.newValue.name) {
                        change.fieldName = change.newValue.name;
                    }
            }
            return change;
        });

        return changes;
    }
}
