// Core
import { Component, Input, Inject } from "@angular/core";

// Interface
import {
    IStringMap,
    IValueId,
} from "interfaces/generic.interface";
import { IInventoryRecordV2 } from "interfaces/inventory.interface";
import { IStore } from "interfaces/redux.interface";

// Constants / Enums
import {
    AttributeTypes,
    AttributeResponseTypes,
} from "constants/inventory.constant";
import { OrgUserTraversal } from "enums/org-user-dropdown.enum";

// Reducer
import { getAllUserModels } from "oneRedux/reducers/user.reducer";
import { StoreToken } from "tokens/redux-store.token";

// Pipes
import { OtDatePipe } from "modules/pipes/ot-date.pipe";

@Component({
    selector: "contract-related-form-cell",
    templateUrl: "./contract-related-form-cell.component.html",
})

export class ContractRelatedFormCellComponent {
    @Input() attribute: IInventoryRecordV2;
    @Input() record: IInventoryRecordV2;

    attributeTypes = AttributeTypes;
    orgUserTraversal = OrgUserTraversal;
    responseTypes = AttributeResponseTypes;
    otherMap: IStringMap<string> = {};
    value = null;
    parameterVal: string;

    constructor(
        @Inject(StoreToken) private store: IStore,
        private otDatePipe: OtDatePipe,
    ) { }

    ngOnInit() {
        this.parameterVal = this.attribute.fieldName as string;
        if (this.record[this.parameterVal] !== undefined && this.attribute.status === "enabled") {
            switch (this.attribute.responseType) {
                case "single-select":
                    this.singleSelectAttribute(this.record[this.parameterVal] as Array<IValueId<string>>);
                    break;

                case "multi-select":
                    this.multiSelectAttribute(this.record[this.parameterVal] as Array<IValueId<string>>);
                    break;

                case "text":
                    this.value = this.record[this.parameterVal] as string;
                    break;

                case "date":
                    this.value = this.otDatePipe.transform(this.record[this.parameterVal] as string, "date");
                    break;
            }
        }
    }

    singleSelectAttribute(singleSelectparam: (Array<IValueId<string>> | IValueId<string>)) {
        if (singleSelectparam && (singleSelectparam as Array<IValueId<string>>).length > 0) {
            if (singleSelectparam[0].value) {
                this.value = singleSelectparam[0].value;
            } else if (singleSelectparam[0].id) {
                this.getUser(singleSelectparam[0].id);
            }
        } else if ((singleSelectparam["value"] || singleSelectparam["key"]) as IValueId<string>) {
            this.value = singleSelectparam["value"] || singleSelectparam["key"];
        } else if (singleSelectparam["id"]) {
            this.getUser(singleSelectparam["id"]);
        }
    }

    multiSelectAttribute(multiSelectparam: Array<IValueId<string>>) {
        if (multiSelectparam && multiSelectparam.length > 0) {
            this.value = multiSelectparam.map((element: IValueId<string>) => element.value).join(",");
        }
    }

    getUser(userId: string) {
        const userMap = getAllUserModels(this.store.getState());
        const user = userMap[userId];
        if (user && user.IsActive) {
            this.value = user.FullName ? user.FullName : user.Email;
        }
    }

}
