import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from "@angular/core";

// Modules
import { SharedModule } from "sharedModules/shared.module";
import { UpgradedLegacyModule } from "sharedModules/upgrade/upgraded-legacy.module";
import { CommonModule } from "@angular/common";
import { HttpClientModule } from "@angular/common/http";
import { FormsModule } from "@angular/forms";
import { ReactiveFormsModule } from "@angular/forms";
import { VitreusModule } from "@onetrust/vitreus";
import { DndListModule } from "ngx-drag-and-drop-lists";
import { OTPipesModule, PIPES } from "modules/pipes/pipes.module";
import { VirtualScrollModule } from "od-virtualscroll";
import { OneVideoModule } from "modules/video/video.module";
import { VendorSharedModule } from "modules/vendor/shared/vendor-shared.module";

// Components
import { ContractDetailsComponent } from "./contract-details.component";
import { ContractFormComponent } from "contracts/contract-details/contract-form/contract-form.component";
import { ContractRelatedFormCellComponent } from "contracts/contract-details/contract-related-form-cell/contract-related-form-cell.component";
import { ContractRelatedFormComponent } from "contracts/contract-details/contract-related-form/contract-related-form.component";
import { ContractAttachmentsComponent } from "contracts/contract-details/contract-attachments/contract-attachments.component";
import { ContractAttachmentsTableComponent } from "contracts/contract-details/contract-attachments/contract-attachments-table/contract-attachments-table.component";
import { ContractAttachmentsModalComponent } from "contracts/contract-details/contract-attachments/contract-attachments-modal/contract-attachments-modal.component";
import { ContractActivityComponent } from "contracts/contract-details/contract-activity/contract-activity.component";
import { ContractRelatedListComponent } from "contracts/contract-details/contract-related-list/contract-related-list.component";
import { ContractLinkedTableComponent } from "contracts/contract-details/contract-related-list/contract-linked-table/contract-linked-table.component";

@NgModule({
    imports: [
        SharedModule,
        UpgradedLegacyModule,
        CommonModule,
        HttpClientModule,
        FormsModule,
        ReactiveFormsModule,
        VitreusModule,
        DndListModule,
        OTPipesModule,
        VirtualScrollModule,
        OneVideoModule,
        VendorSharedModule,
    ],
    declarations: [
        ContractDetailsComponent,
        ContractFormComponent,
        ContractRelatedFormCellComponent,
        ContractRelatedFormComponent,
        ContractAttachmentsComponent,
        ContractAttachmentsTableComponent,
        ContractAttachmentsModalComponent,
        ContractActivityComponent,
        ContractRelatedListComponent,
        ContractLinkedTableComponent,
    ],
    exports: [
        ContractDetailsComponent,
        ContractFormComponent,
        ContractRelatedFormCellComponent,
        ContractRelatedFormComponent,
        ContractAttachmentsComponent,
        ContractAttachmentsTableComponent,
        ContractAttachmentsModalComponent,
        ContractActivityComponent,
        ContractRelatedListComponent,
        ContractLinkedTableComponent,
    ],
    entryComponents: [
        ContractDetailsComponent,
        ContractAttachmentsModalComponent,
    ],
    providers: [
        PIPES, // TODO: Remove from providers when ot-common is implemented. Only necessary if you are using pipe in TS file, else just module is fine
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class ContractDetailsModule {}
