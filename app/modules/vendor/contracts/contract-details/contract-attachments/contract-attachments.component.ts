import { Component } from "@angular/core";
import { StateService } from "@uirouter/core";
import { OtModalService } from "@onetrust/vitreus";
import { ContractAttachmentsModalComponent } from "contracts/contract-details/contract-attachments/contract-attachments-modal/contract-attachments-modal.component";

// Interfaces
import { IPageableMeta } from "interfaces/pagination.interface";
import { IAttachmentFileResponse } from "interfaces/attachment-file.interface";
import { IContractAttachmentsTable } from "modules/vendor/shared/interfaces/contract-attachments.interface";

// Services
import { ContractAttachmentsService } from "contracts/shared/services/contract-attachments.service";
import { ContractService } from "contracts/shared/services/contract.service";

// 3rd Party
import { Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

// Enums
import { TableColumnTypes } from "enums/data-table.enum";
import { ContractAttachmentsTableColumns } from "modules/vendor/shared/enums/contract-attachments.enum.ts";

@Component({
    selector: "contract-attachments",
    templateUrl: "./contract-attachments.component.html",
})
export class ContractAttachmentsComponent {

    loadingAttachments = true;
    defaultPagination = { page: 0, size: 10};
    paginationParams = this.defaultPagination;
    table: IContractAttachmentsTable;
    tableMetaData: IPageableMeta;

    private destroy$ = new Subject();

    constructor(
        private contractAttachmentsService: ContractAttachmentsService,
        private contractService: ContractService,
        private stateService: StateService,
        private translatePipe: TranslatePipe,
        private otModalService: OtModalService,
    ) { }

    ngOnInit(): void {
        this.contractService.contractDetailModel$.pipe(
            takeUntil(this.destroy$),
        ).subscribe(() => {
            this.loadingAttachments = true;
            this.contractAttachmentsService.retrieveVendorAttachments(this.stateService.params.contractId, this.defaultPagination);
        });

        this.contractAttachmentsService.vendorAttachments$.pipe(
            takeUntil(this.destroy$),
        ).subscribe((attachmentList: IAttachmentFileResponse[]) => {
            if (attachmentList) {
                this.table = this.configureTable(attachmentList);
                this.tableMetaData = this.contractAttachmentsService.vendorAttachmentMeta$.getValue();
                this.loadingAttachments = false;
            }
        });
    }

    ngOnDestroy(): void {
        this.contractAttachmentsService.reset();
        this.destroy$.next();
        this.destroy$.complete();
    }

    onAddAttachments(): void {
        this.otModalService
            .create(
                ContractAttachmentsModalComponent,
                {
                    isComponent: true,
                },
            ).pipe(
                takeUntil(this.destroy$),
            );
    }

    configureTable(records: IAttachmentFileResponse[]): IContractAttachmentsTable {
        return {
            rows: records,
            columns: [
                {
                    name: this.translatePipe.transform("Name"),
                    sortKey: ContractAttachmentsTableColumns.NAME,
                    type: TableColumnTypes.Link,
                    cellValueKey: ContractAttachmentsTableColumns.NAME,
                },
                {
                    name: this.translatePipe.transform("DateAdded"),
                    sortKey: ContractAttachmentsTableColumns.DATE_ADDED,
                    type: TableColumnTypes.Date,
                    cellValueKey: ContractAttachmentsTableColumns.DATE_ADDED,
                },
                {
                    name: this.translatePipe.transform("AddedBy"),
                    sortKey: ContractAttachmentsTableColumns.ADDED_BY,
                    type: TableColumnTypes.Text,
                    cellValueKey: ContractAttachmentsTableColumns.ADDED_BY,
                },
                {
                    name: "",
                    type: TableColumnTypes.Action,
                    cellValueKey: "",
                },
            ],
        };
    }

    onPaginationChange(event: number = 0): void {
        this.paginationParams = { ...this.paginationParams, page: event };
        this.loadingAttachments = true;
        this.contractAttachmentsService.retrieveVendorAttachments(this.stateService.params.contractId, this.paginationParams);
    }
}
