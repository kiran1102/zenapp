// Core
import {
    Component,
    Input,
    Inject,
} from "@angular/core";
import {
    OtModalService,
    ConfirmModalType,
    ContextMenuType,
} from "@onetrust/vitreus";
import { StateService } from "@uirouter/core";

// Rxjs
import { Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";

// Services
import { ContractsApiService } from "contracts/shared/services/contracts-api.service";
import { ContractAttachmentsService } from "contracts/shared/services/contract-attachments.service";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

// Interfaces
import { IDropdownOption } from "interfaces/one-dropdown.interface";
import { IDataTableColumn } from "interfaces/tables.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";
import { IDocumentAttachment } from "interfaces/vendor/vendor-document.interface";
import { IContractAttachmentsTable } from "modules/vendor/shared/interfaces/contract-attachments.interface";

// Enums / Constants
import { TableColumnTypes } from "enums/data-table.enum";

@Component({
    selector: "contract-attachments-table",
    templateUrl: "./contract-attachments-table.component.html",
})
export class ContractAttachmentsTableComponent {

    @Input() table: IContractAttachmentsTable;
    @Input() loadingAttachments: boolean;

    menuClass: string;
    resizableCol = false;
    responsiveTable = true;
    rowBordered = true;
    rowNoHover = false;
    rowStriped = false;
    selectedRowMap = {};
    sortKey = "";
    sortOrder = "";
    sortable = false;
    tableColumnTypes = TableColumnTypes;
    truncateCellContent = true;
    wrapCellContent = false;
    menuOptions: IDropdownOption[] = [];

    readonly menuType = ContextMenuType.Small;

    private destroy$ = new Subject();

    constructor(
        @Inject("Export") readonly Export: any,
        private otModalService: OtModalService,
        private contractsApiService: ContractsApiService,
        private contractAttachmentsService: ContractAttachmentsService,
        private translatePipe: TranslatePipe,
        private stateService: StateService,
    ) {}

    ngOnInit(): void {
        this.generateMenuOptions();
    }

    ngOnDestroy(): void {
        this.destroy$.next();
        this.destroy$.complete();
    }

    columnTrackBy(index: number, column: IDataTableColumn): string {
        return column.id;
    }

    handleSortChange({ sortKey, sortOrder }): void {
        this.sortKey = sortKey;
        this.sortOrder = sortOrder;
        this.table.rows.reverse();
    }

    generateMenuOptions(): void {
        this.menuOptions.push({
            text: this.translatePipe.transform("DownloadAttachment"),
            action: (row): void => this.downloadAttachment(row.attachmentId, row),
        });
        this.menuOptions.push({
            text: this.translatePipe.transform("DeleteAttachment"),
            action: (row): void => this.removeAttachment(row.attachmentId),
        });
    }

    private downloadAttachment(attachmentId, attachment?: IDocumentAttachment): void {
        this.contractsApiService.downloadContract(attachmentId).then((res: IProtocolResponse<any>): void => {
            if (res.result) {
                this.Export.basicFileDownload(res.data, attachment.fileName);
            }
        });
    }

    private removeAttachment(attachmentId: string): void {
        this.otModalService
            .confirm({
                type: ConfirmModalType.DELETE,
                translations: {
                    title: this.translatePipe.transform("DeleteAttachment"),
                    desc: this.translatePipe.transform("AreYouSureDeleteAttachment"),
                    confirm: this.translatePipe.transform("DoYouWantToContinue"),
                    submit: this.translatePipe.transform("Delete"),
                    cancel: this.translatePipe.transform("Cancel"),
                },
                hideClose: false,
            }).pipe(
                takeUntil(this.destroy$),
            ).subscribe((res) => {
                if (res) {
                    this.contractAttachmentsService.removeAttachment(this.stateService.params.contractId, attachmentId);
                }
            });
        }
}
