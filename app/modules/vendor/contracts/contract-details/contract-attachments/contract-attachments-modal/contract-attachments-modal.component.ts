import {
    Component,
    ViewChild,
} from "@angular/core";
import {
    IOtModalContent,
    ToastService,
} from "@onetrust/vitreus";
import { StateService } from "@uirouter/core";

// 3rd party
import { Subject } from "rxjs";

// Services
import { ContractAttachmentsService } from "contracts/shared/services/contract-attachments.service";

// Constants
import { FileConstraints } from "constants/file.constant.ts";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

@Component({
    selector: "contract-attachments-modal",
    templateUrl: "./contract-attachments-modal.component.html",
})
export class ContractAttachmentsModalComponent implements IOtModalContent {

    @ViewChild("fileSelect") fileSelectRef;
    otModalCloseEvent: Subject<string>;
    allowedExtensions = ["csv", "doc", "docx", "jpg", "jpeg", "mpp", "msg", "pdf", "png", "ppt", "pptx", "txt", "vsd", "vsdx", "xls", "xlsx"];
    isHovering: boolean;
    files: File[] = [];
    fileName = "";
    fileDescription = "";
    hasError = false;
    fileSizeError = false;
    isSavingFile = false;

    constructor(
        private contractAttachmentsService: ContractAttachmentsService,
        private stateService: StateService,
        private readonly toastService: ToastService,
        private readonly translatePipe: TranslatePipe,
    ) { }

    onSave(): void {
        if (!this.files || !this.files.length || !this.fileName.trim()) return;
        this.saveFile(this.files)
            .then(() => this.cancel())
            .catch(() => null);
    }

    saveFile(fileConfig): Promise<boolean> {
        return new Promise((resolve, reject) => {
            this.isSavingFile = true;
            this.contractAttachmentsService.saveFile(this.stateService.params.contractId, fileConfig)
                .then(() => {
                    this.isSavingFile = false;
                    resolve(true);
                }).catch(() => {
                    this.isSavingFile = false;
                    reject(false);
                });
        });
    }

    cancel(): void {
        this.otModalCloseEvent.next();
    }

    triggerFileUpload(): void {
        this.fileSelectRef.nativeElement.click();
    }

    handleFiles(files: File[]): void {
        this.fileSizeError = false;
        if (!files.length) {
            return;
        }

        if (files.find(this.checkFilesSizeLimit)) {
            this.fileSizeError = true;
            this.toastService.error(this.translatePipe.transform("InvalidFile"),
            this.translatePipe.transform("FileSizeLimitViolation"));
        } else if (files.find(this.checkFilesNameLimit)) {
            this.toastService.error(this.translatePipe.transform("InvalidFile"),
            this.translatePipe.transform("FileNameExceedsLimitOf100Characters"));
        } else {
            this.files = [...this.files, ...files];
            this.fileName = files[0].name;
        }
    }

    checkFilesSizeLimit(file: File) {
       return file.size === 0 || file.size > FileConstraints.FILE_SIZE_LIMIT;
    }

    checkFilesNameLimit(file: File) {
       return file.name.length > FileConstraints.FILE_NAME_MAX_LENGTH;
    }

    handleInvalidFiles(files: File[]): void {
        if (!files.length) {
            return;
        }

        if (files[0].size === 0 || files[0].size > FileConstraints.FILE_SIZE_LIMIT) {
            this.fileSizeError = true;
            this.toastService.error(this.translatePipe.transform("InvalidFile"),
            this.translatePipe.transform("FileSizeLimitViolation"));
        } else {
            this.toastService.error(this.translatePipe.transform("ThisFileTypeIsNotAllowed"),
            this.translatePipe.transform("AllowedFileFormats",
            { formats: this.allowedExtensions.join(", ") }));
        }
    }

    handleFilesHover(isHovering: boolean): void {
        this.isHovering = isHovering;
    }

    removeFile(index: number): void {
        if (!this.isSavingFile) {
            this.files.splice(index, 1);
        }
    }
}
