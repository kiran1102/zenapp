// 3rd party
import {
    Component,
    Input,
    Output,
    EventEmitter,
    ElementRef,
    SimpleChanges,
    OnChanges,
    OnInit,
} from "@angular/core";

// Rxjs
import {
    Subject,
    BehaviorSubject,
    combineLatest,
} from "rxjs";

import {
    takeUntil,
} from "rxjs/operators";

import { isNull } from "lodash";

// Services
import { TimeStamp } from "oneServices/time-stamp.service";
import { UserActionService } from "oneServices/actions/user-action.service";
import { ContractService } from "contracts/shared/services/contract.service";

// Interfaces
import { ICreateData } from "modules/vendor/shared/interfaces/vendor-upload-document.interface";
import {
    IContractDetails,
    IContractTypeResponse,
    ICurrenciesResponse,
} from "modules/vendor/shared/interfaces/vendor-upload-document.interface";
import { TranslatePipe } from "modules/pipes/translate.pipe";
import { IDateTimeOutputModel } from "@onetrust/vitreus";
import { ILabelValue } from "interfaces/generic.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";
import { IUser } from "interfaces/user.interface";

// Enums and Constants
import { OrgUserTraversal } from "enums/org-user-dropdown.enum";
import {
    StatusResponseValues,
    StatusTranslationKeys,
} from "modules/vendor/shared/constants/vendor-documents.constants";
import { FormDataTypes } from "constants/form-types.constant";

@Component({
    selector: "contract-form",
    templateUrl: "./contract-form.component.html",
})
export class ContractFormComponent implements OnInit, OnChanges {

    @Input() contract: IContractDetails;
    @Input() wrapperClass: string;
    @Input() isEditable: boolean;
    @Input() editMode: boolean;
    @Output() toggleEdit = new EventEmitter();
    @Output() saveContractAction = new EventEmitter();

    currencyList: ICurrenciesResponse[];
    contractTypes: IContractTypeResponse[];
    statusList: Array<ILabelValue<string>>;
    format = this.timeStamp.getDatePickerDateFormat();
    formDataTypes = FormDataTypes;
    orgUserTraversal = OrgUserTraversal;
    displayOptions = {};
    defaultCurrency = "USD";

    formData: ICreateData[] = [
        {
            name: "name",
            label: "ContractName",
            type: FormDataTypes.INPUT,
            isRequired: true,
        },
        {
            name: "contractType",
            label: "Type",
            type: FormDataTypes.PICKLIST,
            isRequired: true,
            labelKey: "name",
            valueKey: "nameKey",
        },
        {
            name: "agreementCreatedDate",
            label: "DateCreated",
            type: FormDataTypes.DATE,
            isRequired: true,
        },
        {
            name: "expirationDate",
            label: "ExpirationDate",
            type: FormDataTypes.DATE,
            isRequired: false,
        },
        {
            name: "statusOption",
            label: "Status",
            type: FormDataTypes.PICKLIST,
            isRequired: true,
            labelKey: "label",
            valueKey: "value",
        },
        {
            name: "approvedDate",
            label: "ApprovedDate",
            type: FormDataTypes.DATE,
            isRequired: false,
        },
        {
            name: "contractOwner",
            label: "ContractOwner",
            type: FormDataTypes.APPROVER,
            isRequired: false,
        },
        {
            name: "vendorContact",
            label: "VendorContact",
            type: FormDataTypes.INPUT,
            isRequired: false,
        },
        {
            name: "approvers",
            label: "Approvers/Signers",
            type: FormDataTypes.INPUT,
            isRequired: false,
        },
        {
            name: "cost",
            label: "Cost",
            type: FormDataTypes.CURRENCY,
            labelKey: "code",
            valueKey: "code",
            isRequired: false,
        },
        {
            name: "url",
            label: "ContractUrl",
            type: FormDataTypes.INPUT,
            isRequired: false,
            maxLength: 300,
        },
        {
            name: "note",
            label: "Notes",
            type: FormDataTypes.TEXTAREA,
            isRequired: false,
        },
    ];

    loading$ = new BehaviorSubject<boolean>(true);

    private destroy$ = new Subject();

    constructor(
        readonly element: ElementRef,
        private contractService: ContractService,
        private timeStamp: TimeStamp,
        private translatePipe: TranslatePipe,
        private userActionService: UserActionService,
    ) { }

    ngOnInit(): void {

        combineLatest(
            this.contractService.vendorContractTypes$,
            this.contractService.vendorContractAllCurrencies$,
            this.contractService.contractStatuses$,
        ).pipe(
            takeUntil(this.destroy$),
        ).subscribe(([contractTypes, currencies, contractStatuses]) => {
            if (contractTypes && currencies && contractStatuses) {
                this.contractTypes = contractTypes.map((contractType: IContractTypeResponse) => {
                    contractType.name = contractType.nameKey ? this.translatePipe.transform(contractType.nameKey) : contractType.name;
                    return contractType;
                });
                this.displayOptions["contractType"] = this.contractTypes;
                this.currencyList = currencies;
                this.statusList = contractStatuses.map((status) => {
                    return {
                        label: this.translatePipe.transform(status.label),
                        value: status.value,
                    };
                });
                this.displayOptions["statusOption"] = this.statusList;
                this.loading$.next(false);
            }
        });

        this.contractService.retrieveContractTypes();
        this.contractService.retrieveCurrencies();
        this.contractService.retrieveContractStatuses();
    }

    ngOnChanges(changes: SimpleChanges) {
        if (changes.contract) {
            this.contract = changes.contract.currentValue;
        }
        if (this.contract.cost && this.contract.currencyOption && this.contract.currencyOption.code === null) {
            this.contract.currencyOption = { code: this.contract.currency, description: "" };
        }
        if (!this.contract.cost) {
            this.contract.currency = this.defaultCurrency;
            this.contract.currencyOption = { code: null, description: "" };
        }
    }

    formToggleEdit() {
        if (this.isEditable) {
            this.editMode = true;
            this.toggleEdit.emit(this.editMode);
            this.focus();
        }
    }

    onDateChange(name, dateValue: IDateTimeOutputModel) {
        this.contract[name] = dateValue.dateTime ? this.timeStamp.formatDateToIso({ day: dateValue.jsdate.getDate(), month: dateValue.jsdate.getMonth() + 1, year: dateValue.jsdate.getFullYear() }) : null;
        if (this.contract[name] !== dateValue.date) {
            this.saveContractAction.emit({ name, event: this.contract[name] });
        }
    }

    onInputChange(name, event) {
        if (this.contract[name] !== event) {
            this.saveContractAction.emit({ name, event });
        }

        if (name === "name" || "url") {
            this.contract[name] = event;
        }
    }

    handleEnteredNotes(name, event) {
        if (this.contract[name] !== event) {
            this.contract[name] = event;
            this.saveContractAction.emit({ name, event });
        }
    }

    onDropdownChange(name, event) {
        if (name === "currency") {
            this.contract.currency = event.code;
            this.contract["currencyOption"] = { code: event.code, description: event.description };
        } else if (name === "statusOption") {
            this.contract.status = event.value;
            this.contract["statusOption"] = { label: event.label, value: event.value };
        } else {
            this.contract[name] = event;
        }
        this.saveContractAction.emit({ name, event });
    }

    onSelectContractOwner(name, event) {
        if (!isNull(event.selection) && event.selection.Id) {
            if (event.selection.Id === event.selection.FullName) {
                event.selection.Id = null;
            }
            if (event.selection.Id) {
                this.userActionService.getUser(event.selection.Id, true).then((res: IProtocolResponse<IUser>) => {
                    if (res.result) {
                        this.contract.contractOwner = res.data.FullName;
                    }
                });
            } else {
                this.contract.contractOwner = event.selection.FullName || "";
            }

            this.contract.contractOwnerId = event.selection.Id;
            this.contract.contractOwnerEmail = event.selection.FullName || "";
        } else {
            this.contract[name] = null;
            this.contract.contractOwnerId = null;
            this.contract.contractOwnerEmail = null;
        }
        this.saveContractAction.emit({ name, event });
    }

    hasAttachments(data, contract): string {
        const contractName = contract[data.name];
        const firstAttachment = contract.attachments[0];
        if (contractName) return contractName;
        if (firstAttachment) return firstAttachment[data.name];
        return "";
    }

    private focus() {
        setTimeout(() => {
            this.setupInputContents();
        }, 0);
    }

    private setupInputContents() {
        const inputEl: HTMLElement = this.getInputElement(this.element.nativeElement);
        if (inputEl) {
            inputEl.focus();
            inputEl.click();
        }
    }

    private getInputElement(elem: HTMLElement): HTMLElement {
        return (elem && elem[0]) ? elem[0].querySelector("input") : null;
    }
}
