
// Core
import {
    Component,
    Inject,
    Input,
    Output,
    EventEmitter,
    SimpleChanges,
    ViewChild,
    TemplateRef,
} from "@angular/core";

// Redux
import observableFromStore from "oneRedux/utils/observable-from-store";
import { getRecord } from "oneRedux/reducers/inventory-record.reducer";

// Rxjs
import { Subject } from "rxjs";
import {
    startWith,
    takeUntil,
} from "rxjs/operators";

// Services
import { ModalService } from "sharedServices/modal.service";
import { VendorInformationService } from "modules/vendor/services/shared/vendor-information.service";

// Vitreus
import {
    OtModalService,
    ModalSize,
} from "@onetrust/vitreus";

// Interfaces
import { INumberMap } from "interfaces/generic.interface";
import { IDropdownOption } from "interfaces/one-dropdown.interface";
import {
    IStore,
    IStoreState,
} from "interfaces/redux.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";
import {
    IRelatedTable,
    IInventoryTableColumn,
    IContractRelatedRow,
} from "interfaces/inventory.interface";
import { IInventoryRecordV2 } from "interfaces/inventory.interface";
import {
    IRelatedAssetsPA,
    IRelatedListOption,
    IRelatedListOptionData,
} from "modules/vendor/shared/interfaces/vendor-upload-document.interface";
import {
    IRelatedTableRow,
} from "interfaces/inventory.interface";

// constants + enums
import { CellTypes } from "enums/general.enum";
import { InventoryTableIds, DefaultTableConfig } from "enums/inventory.enum";
import {
    InventoryTranslationKeys,
    InventoryDetailRoutes,
    InventoryPermissions,
} from "constants/inventory.constant";
import { VendorTableTypes } from "modules/vendor/shared/constants/vendor-documents.constants";

// Tokens
import { StoreToken } from "tokens/redux-store.token";
import { ContractsApiService } from "contracts/shared/services/contracts-api.service";
import { StateService } from "@uirouter/core";
import { ISamlSettingsResponse } from "interfaces/setting.interface";

// Pipes
import { PermitPipe } from "modules/pipes/permit.pipe";
import { TranslatePipe } from "pipes/translate.pipe";

// vitreus
import { ContextMenuType } from "@onetrust/vitreus";

type PageUpdatedSuccess = () => void;

@Component({
    selector: "contract-linked-table",
    templateUrl: "./contract-linked-table.component.html",
})
export class ContractLinkedTableComponent {

    @Input() contractId: string;
    @Input() inventory: IRelatedTable;
    @Input() type: number;
    @Input() isExpanded: boolean;
    @Input() pageUpdated: boolean;
    @Output() pageChanged = new EventEmitter<{ page: number, size: number, pageUpdatedSuccess: PageUpdatedSuccess }>();
    @Output() toggle = new EventEmitter<void>();
    @Output() viewRecord: EventEmitter<IRelatedTableRow> = new EventEmitter<IRelatedTableRow>();
    @ViewChild("VendorRelatedContractLinking") contentRef: TemplateRef<string>;

    cellTypes = CellTypes;
    config = DefaultTableConfig;
    hasRelatedViewPermission: boolean;
    isReady = true;
    menuClass: string;
    vendorRelatedContractLinkingText: string;
    relatedInventories: INumberMap<IRelatedTable>;
    tableIds = InventoryTableIds;
    actions: IDropdownOption[] = [];
    canShowContextMenuOptions = false;
    isSaving = false;
    destroy$ = new Subject();
    modalCloseEvent: Subject<string>;
    modalSize: ModalSize;
    orgGroupId: string;
    record: IInventoryRecordV2;
    chooseVendorItemText: string;
    selectedOptions: IRelatedAssetsPA[] = [];
    selectedVendor: IRelatedListOption;
    requiredFieldsPopulated = false;
    clickPreventClass = "prevent-row-click";
    contextMenuType = ContextMenuType;

    constructor(
        @Inject(StoreToken) private store: IStore,
        private contractsApiService: ContractsApiService,
        private translatePipe: TranslatePipe,
        private modalService: ModalService,
        private stateService: StateService,
        private otModalService: OtModalService,
        private permit: PermitPipe,
        private vendorInformationService: VendorInformationService,
    ) { }

    checkIfRowIsDisabled = (row: IContractRelatedRow): boolean => false;

    ngOnInit() {
        this.vendorRelatedContractLinkingText = this.translatePipe.transform(InventoryTranslationKeys[this.type].addRelatedInventoryHeader);
        this.chooseVendorItemText = this.translatePipe.transform(InventoryTranslationKeys[this.type].chooseInventoryItem) || "";
        if (this.checkRelatedInventoriesLength()) {
            this.pageChange(0);
        }
        this.actions = this.getActions();
        this.modalSize = ModalSize.SMALL;

        observableFromStore(this.store).pipe(
            startWith(this.store.getState()),
            takeUntil(this.destroy$),
        ).subscribe((state: IStoreState) => this.componentWillReceiveState(state));
        this.hasRelatedViewPermission = this.permit.transform(InventoryPermissions[this.type].details);

    }

    ngOnChanges(changes: SimpleChanges) {
        if (changes.isExpanded && !changes.isExpanded.firstChange) {
            this.pageChange(0);
        }
    }

    getActions(): IDropdownOption[] {
        const actionsList: IDropdownOption[] = [];
        if (this.permit.transform("VendorContractInventoryLinkRemove")) {
            actionsList.push({
                text: this.translatePipe.transform("Remove"),
                action: (row: IContractRelatedRow): void => {
                    this.canShowContextMenuOptions = false;
                    this.launchDeleteContractLink(row);
                },
            });
        }
        return actionsList;
    }

    columnTrackBy(column: IInventoryTableColumn): string {
        return column.dataKey;
    }

    toggleExpanded() {
        this.isReady = false;
        this.toggle.emit();
    }

    goToRecord(row: IContractRelatedRow) {
        this.stateService.go(InventoryDetailRoutes[this.type], { inventoryId: this.type, recordId: row.inventoryId, tabId: "details" });
    }

    pageChange(page: number, pageSize?: number): void {
        this.isReady = false;
        const size: number = pageSize || this.isExpanded ? this.config.expandedSize : this.config.defaultSize;
        const pageUpdatedSuccess = () => { this.isReady = true; };
        this.pageChanged.emit({ page, size, pageUpdatedSuccess });
    }

    onAddContractRelatedLink() {
        this.modalCloseEvent = this.otModalService
            .create(
                this.contentRef,
                {
                    size: this.modalSize,
                },
            );
    }

    handleRowClick(click: { event: any, row: IRelatedTableRow }): void {
        const target = click && click.event ? click.event.target : null;
        const isInPreventClickContainer: boolean = target ? this.isChildOfNonClickElement(target) : null;
        if (!isInPreventClickContainer) {
            this.viewRecord.emit(click.row);
            this.vendorInformationService.loadSideMenuAttributes(true);
        }
    }

    assetsDataMap = (option: IRelatedListOption) => {
        return {
            inventoryType: VendorTableTypes.Assets,
            inventoryName: option.name,
            inventoryId: option.id,
            orgGroupId: option.organization.id,
            attributes: {
                location: {
                    id: option.location.id,
                    value: option.location.value,
                    valueKey: option.location.valueKey || "",
                },
            },
        };
    }

    processDataMap = (option: IRelatedListOption) => {
        return {
            inventoryType: VendorTableTypes.Processes,
            inventoryName: option.name,
            inventoryId: option.id,
            orgGroupId: option.organization.id,
        };
    }

    vendorDataMap = (option: IRelatedListOption) => {
        return {
            inventoryType: VendorTableTypes.Vendors,
            inventoryName: option.name,
            inventoryId: option.id,
            orgGroupId: option.organization.id,
            attributes: {
                type: {
                    id: option.type.id,
                    value: option.type.value,
                    valueKey: option.type.valueKey || "",
                },
            },
        };
    }

    getOptionDataMap(option: IRelatedListOption): IRelatedAssetsPA {
        if (this.type === this.tableIds.Assets) {
            return this.assetsDataMap(option);
        } else if (this.type === this.tableIds.Processes) {
            return this.processDataMap(option);
        } else if (this.type === this.tableIds.Vendors) {
            return this.vendorDataMap(option);
        }
    }

    onSelectTrigger(optionsData: IRelatedListOptionData): void {
        this.requiredFieldsPopulated = false;
        const { options } = optionsData;
        if (options && options.length) {
            this.selectedVendor = options[0];
            this.selectedOptions = [];
            options.forEach((option) => {
                this.selectedOptions.push(this.getOptionDataMap(option));
            });
            this.requiredFieldsPopulated = true;
        } else {
            this.selectedVendor = null;
            this.selectedOptions = [];
        }
        this.isReady = true;
    }

    addContractLink(isCloseModal: boolean = true) {
        this.isSaving = true;
        if (this.selectedOptions && this.selectedOptions.length > 0) {
            const payload = this.selectedOptions[0];
            this.contractsApiService.addContractLinksByContractId(this.contractId, payload)
                .then((response: IProtocolResponse<string>) => {
                    if (response.result) {
                        this.selectedVendor = undefined;
                        this.selectedOptions = [];
                        this.pageUpdate();
                        if (isCloseModal) {
                            this.closeModal();
                        }
                        this.requiredFieldsPopulated = false;
                    }
                    this.isSaving = false;
                });
        }
    }

    closeModal() {
        this.selectedVendor = undefined;
        this.modalCloseEvent.next();
    }

    private componentWillReceiveState(state) {
        this.record = { ...getRecord(state) };
    }

    private checkRelatedInventoriesLength(): boolean {
        return !this.isExpanded && this.relatedInventories && this.relatedInventories[this.type].rows && this.relatedInventories[this.type].rows.length > this.config.defaultSize;
    }

    private launchDeleteContractLink(row: IContractRelatedRow) {
        const deleteModalData = {
            modalTitle: this.translatePipe.transform("DeleteContract"),
            confirmationText: this.translatePipe.transform("AreYouSureDeleteContractLink"),
            submitButtonText: this.translatePipe.transform("Delete"),
            cancelButtonText: this.translatePipe.transform("Cancel"),
            promiseToResolve: (): ng.IPromise<boolean> =>
                this.contractsApiService.deleteContractLink(this.contractId, row.id).then((response: IProtocolResponse<ISamlSettingsResponse>): boolean => {
                    if (response.result) {
                        this.pageUpdate();
                        if (this.isExpanded && this.inventory.totalElements === this.config.defaultSize + 1 && this.inventory.number === 0) {
                            this.isExpanded = false;
                            this.toggleExpanded();
                        }
                    }
                    return response.result;
                }),
        };
        this.modalService.setModalData(deleteModalData);
        this.modalService.openModal("deleteConfirmationModal");
    }

    private pageUpdate() {
        const currentPageSize = this.inventory.numberOfElements;
        let currentPageNumber = this.inventory.number;
        currentPageNumber = currentPageSize !== 1 || currentPageNumber === 0 ? currentPageNumber : currentPageNumber - 1;
        this.pageChange(currentPageNumber);
    }

    private isChildOfNonClickElement(target: any): boolean {
        if (!target || target.hasAttribute("otdatatable")) return false;
        if ([target.className].indexOf(this.clickPreventClass) > -1) return true;
        return this.isChildOfNonClickElement(target.parentNode);
    }
}
