// Angular
import {
    Component,
    Input,
} from "@angular/core";

// 3rd party
import { without } from "lodash";

// service
import { ContractsApiService } from "contracts/shared/services/contracts-api.service";
import { InventoryViewLogic } from "oneServices/view-logic/inventory-view-logic.service";
import { RecordActionService } from "inventoryModule/services/action/record-action.service";
import { InventoryService } from "inventorySharedModule/shared/services/inventory-api.service";
import { AttributeGroupsActionService } from "modules/attribute-manager/services/attribute-groups-action.service";
import { VendorInformationService } from "modules/vendor/services/shared/vendor-information.service";

// constant & enums
import { InventoryTableIds, DefaultTableConfig } from "enums/inventory.enum";
import { CellTypes } from "enums/general.enum";
import { VendorTableTypes } from "modules/vendor/shared/constants/vendor-documents.constants";
import { InventoryTranslationKeys } from "constants/inventory.constant";
import { NewInventorySchemaIds, LegacyInventorySchemaIds } from "constants/inventory-config.constant";

// Interfaces
import { IPageableOf } from "interfaces/pagination.interface";
import {
    IStringMap,
    INumberMap,
} from "interfaces/generic.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";
import {
    IRelatedTable,
    IRelatedTableRow,
    IContractRelatedRow,
    IAttributeDetailV2,
    IInventoryRecordResponseV2,
} from "interfaces/inventory.interface";
import { IAttributeGroup } from "interfaces/attributes.interface";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

type PageUpdatedSuccess = () => void;

@Component({
    selector: "contract-related-list",
    templateUrl: "./contract-related-list.component.html",
})
export class ContractRelatedListComponent {

    @Input() contractId: string;

    expandedTableMap: IStringMap<boolean> = {};
    tableExpanded = false;
    relatedVendorArray: IRelatedTable[];
    relatedContract: INumberMap<IRelatedTable>;
    drawerHeader: string;
    drawerReady = false;
    drawerIsOpen: boolean;
    inventoryType: string;
    inventoryId: number;
    currentRecordId: string;
    isDataElement: boolean;
    cellTypes = CellTypes;
    tableIds = InventoryTableIds;
    isEditing: boolean;
    isShowRelatedForm: boolean;
    attributeDetails: IStringMap<IAttributeDetailV2>;
    recordDetails: IProtocolResponse<IInventoryRecordResponseV2>;

    constructor(
        public contractsApiService: ContractsApiService,
        public translatePipe: TranslatePipe,
        public inventoryViewLogic: InventoryViewLogic,
        public recordActionService: RecordActionService,
        private inventoryService: InventoryService,
        private attributeGroupsActionService: AttributeGroupsActionService,
        private vendorInformationService: VendorInformationService,
    ) { }

    ngOnInit() {
        this.setExpandedTableMap();
        this.drawerReady = false;
        this.drawerIsOpen = false;
        this.relatedContract = this.setDefaultRelatedContracts();
        this.setRelatedContracts();
    }

    toggleDrawer(open: boolean, row?: IRelatedTableRow): void {
        if (open) {
            this.drawerIsOpen = true;
            this.drawerReady = false;
            this.currentRecordId = row.inventoryId;
            this.isShowRelatedForm = true;
            if (typeof(row.inventoryType) === "string") {
                this.inventoryType = row.inventoryType.toLowerCase().replace(/_/ , "-");
                this.inventoryId = LegacyInventorySchemaIds[this.inventoryType];
            } else {
                this.inventoryId = row.inventoryType;
                this.inventoryType = NewInventorySchemaIds[this.inventoryId];
            }
            this.drawerHeader = this.inventoryViewLogic.getLabel(this.inventoryId, "attributeDrawerHeader");
            this.relatedGetRecordV2(this.inventoryType, this.currentRecordId);
        } else {
            this.drawerIsOpen = false;
            this.drawerReady = false;
            this.isShowRelatedForm = false;
        }
    }

    relatedGetRecordV2(inventoryType: string , currentRecordId: string) {
        this.inventoryService.getRecordV2(inventoryType, currentRecordId).then((data: IProtocolResponse<IInventoryRecordResponseV2>) => {
            if (data) {
             this.recordDetails = data;
            }
        });
        this.relatedGetAttributesList(inventoryType);
    }

    relatedGetAttributesList(inventoryType: string) {
        this.inventoryService.getAttributes(inventoryType, true).then((data: IProtocolResponse<IAttributeDetailV2[]>) => {
            if (data) {
                const attributeMap: IStringMap<IAttributeDetailV2> = {};
                data.data.forEach((attribute: IAttributeDetailV2) => {
                attributeMap[attribute.fieldName] = attribute;
                });
                this.attributeDetails = attributeMap;
                this.relatedAttributeGroups(inventoryType);
            }
        });
    }

    relatedAttributeGroups(inventoryType: string) {
        this.inventoryService.getAttributeGroups(inventoryType).then((response: IProtocolResponse<IAttributeGroup[]>) => {
            if (response.result) {
                this.attributeGroupsActionService.setGroupsData(response.data);
                this.vendorInformationService.loadSideMenuAttributes(false);
            }
        });
    }

    onPageChanged(params: { page: number, size: number, pageUpdatedSuccess: PageUpdatedSuccess }) {
        this.setRelatedContracts(params.page, params.size, params.pageUpdatedSuccess);
    }

    setExpandedTableMap() {
        if (this.relatedVendorArray) {
            this.relatedVendorArray.forEach((table: IRelatedTable) => {
                this.expandedTableMap[table.id] = false;
            });
        }
    }

    tableIsExpanded() {
        this.tableExpanded = Object.values(this.expandedTableMap).includes(true);
    }

    toggleTable(inventoryId: string) {
        this.expandedTableMap[inventoryId] = !this.expandedTableMap[inventoryId];
        this.tableIsExpanded();
    }

    setDefaultRelatedContracts(): INumberMap<IRelatedTable> {
        return {
            [InventoryTableIds.Assets]: null,
            [InventoryTableIds.Processes]: null,
            [InventoryTableIds.Vendors]: null,
        };
    }
    setRelatedContracts(page = 0, size = DefaultTableConfig.defaultSize, pageUpdatedSuccess? ) {
        const assetParams = { type: VendorTableTypes.Assets, page, size };
        const pasParams = { type: VendorTableTypes.Processes, page, size };
        const vendorParams = { type: VendorTableTypes.Vendors, page, size };
        const promises: Array<ng.IPromise<IProtocolResponse<IPageableOf<IContractRelatedRow>>>> = [
            this.contractsApiService.getRelatedContract(this.contractId, assetParams),
            this.contractsApiService.getRelatedContract(this.contractId, pasParams),
            this.contractsApiService.getRelatedContract(this.contractId, vendorParams),
        ];

        Promise.all(promises).then((responses: [IProtocolResponse<IPageableOf<IContractRelatedRow>>, IProtocolResponse<IPageableOf<IContractRelatedRow>>, IProtocolResponse<IPageableOf<IContractRelatedRow>>]) => {
            if (responses[0] && responses[0].result && responses[1] && responses[1].result && responses[2] && responses[2].result) {
                this.relatedContract[InventoryTableIds.Assets] = this.formatLinkedInventory(responses[0].data, InventoryTableIds.Assets);
                this.relatedContract[InventoryTableIds.Processes] = this.formatLinkedInventory(responses[1].data, InventoryTableIds.Processes);
                this.relatedContract[InventoryTableIds.Vendors] = this.formatLinkedInventory(responses[2].data, InventoryTableIds.Vendors);
                this.relatedVendorArray = without(Object.values(this.relatedContract), null);
                if (pageUpdatedSuccess) {
                    pageUpdatedSuccess();
                }
            }
        });

    }

    formatLinkedInventory(response: IPageableOf<IContractRelatedRow>, vendorType: number, context?: number): IRelatedTable {
        const formattedData: IRelatedTable = {
            name: this.translatePipe.transform(InventoryTranslationKeys[vendorType].relatedInventoryHeader),
            id: vendorType,
            numberOfElements: response.numberOfElements,
            totalElements: response.totalElements,
            size: response.size,
            totalPages: response.totalPages,
            first: response.first,
            last: response.last,
            number: response.number,
            columns: [],
            rows: response.content || [],
        };
        switch (vendorType) {
            case InventoryTableIds.Assets:
                formattedData.columns = [
                    { label: this.translatePipe.transform("Name"), dataKey: "inventoryName", type: this.cellTypes.link },
                    { label: this.translatePipe.transform("Organization"), dataKey: "orgGroupName", type: this.cellTypes.text },
                    { label: this.translatePipe.transform("Location"), rowTranslationKey: "location", dataKey: "location", type: this.cellTypes.text, dataObjRef: "value", keyObjRef: "valueKey" },
                ];
                break;
            case InventoryTableIds.Processes:
                formattedData.columns = [
                    { label: this.translatePipe.transform("Name"), dataKey: "inventoryName", type: this.cellTypes.link },
                    { label: this.translatePipe.transform("Organization"), dataKey: "orgGroupName", type: this.cellTypes.text },
                ];
                break;
            case InventoryTableIds.Vendors:
                formattedData.columns = [
                    { label: this.translatePipe.transform("Name"), dataKey: "inventoryName", type: this.cellTypes.link },
                    { label: this.translatePipe.transform("Type"), rowTranslationKey: "type", dataKey: "type", type: this.cellTypes.text, dataObjRef: "value", keyObjRef: "valueKey" },
                    { label: this.translatePipe.transform("Organization"), dataKey: "orgGroupName", type: this.cellTypes.text },
                ];
                break;
        }
        return formattedData;
    }

    trackByFn(index: number): number {
        return index;
    }
}
