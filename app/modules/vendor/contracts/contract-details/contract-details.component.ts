// Rxjs
import {
    Subject,
    combineLatest,
} from "rxjs";
import {
    takeUntil,
} from "rxjs/operators";

// 3rd Party
import {
    Component,
    Inject,
    Output,
    EventEmitter,
} from "@angular/core";
import {
    Transition,
    HookResult,
} from "@uirouter/angularjs";
import {
    TransitionService,
    StateService,
} from "@uirouter/core";

import { isNull } from "lodash";

// Services
import { ContractsApiService } from "contracts/shared/services/contracts-api.service";
import { ContractService } from "contracts/shared/services/contract.service";
import { ModalService } from "sharedServices/modal.service";
import { Permissions } from "modules/shared/services/helper/permissions.service";

// Constants & Enums
import { ContractTabOptions } from "modules/vendor/shared/enums/contract-details.enums";
import { InventoryTableIds } from "enums/inventory.enum";
import { InventoryDetailsTabs } from "constants/inventory.constant";

// Interfaces
import { ITabsNav } from "interfaces/one-tabs-nav.interface";
import {
    IContractDetails,
    IContractTypeResponse,
    ICurrenciesResponse,
    IContractUpdateRequest,
    IContractAttachment,
} from "modules/vendor/shared/interfaces/vendor-upload-document.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";
import { ISamlSettingsResponse } from "interfaces/setting.interface";
import { ISaveChangesModalResolve } from "interfaces/save-changes-modal-resolve.interface";
import { IStringMap } from "interfaces/generic.interface";

// Constants
import {
    StatusResponseValues,
    StatusTranslationKeys,
} from "modules/vendor/shared/constants/vendor-documents.constants";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

@Component({
    selector: "contract-details",
    templateUrl: "./contract-details.component.html",
})
export class ContractDetailsComponent {

    @Output() onDeleteContract: EventEmitter<void> = new EventEmitter<void>();

    ready = false;
    selectedTab = ContractTabOptions.Details;
    contractTabOptions = ContractTabOptions;
    tabOptions = [];
    vendorId: string;
    contractId: string;
    contractName: string;
    contract: IContractDetails;
    contractResponse: IContractDetails;
    currencyList: ICurrenciesResponse[];
    selectedCurrency: ICurrenciesResponse;
    contractTypes: IContractTypeResponse[];
    selectedType: IContractTypeResponse;
    showTabs = false;
    hasEdits = false;
    editMode = false;
    previousValues: IContractDetails;
    canSave = true;

    private transitionDeregisterHook: () => any;
    private destroy$ = new Subject();

    constructor(
        @Inject("Export") readonly Export: any,
        public $transitions: TransitionService,
        private contractsApiService: ContractsApiService,
        private stateService: StateService,
        private contractService: ContractService,
        private modalService: ModalService,
        private translatePipe: TranslatePipe,
        private permissions: Permissions,
    ) { }

    ngOnInit(): void {
        this.contractId = this.stateService.params.contractId;
        this.contractService.retrieveContractById(this.contractId);
        this.contract = this.contractService.contractDetailModel$.getValue();
        this.setTabOptions();
        this.vendorId = this.stateService.params.recordId;

        combineLatest(this.contractService.contractDetailModel$).pipe(
            takeUntil(this.destroy$),
        ).subscribe(([contract]) => {
            this.ready = false;
            if (contract) {
                contract["contractType"] = {
                    id: contract.contractTypeId,
                    name: this.translatePipe.transform(contract.contractTypeNameKey),
                    nameKey: contract.contractTypeNameKey,
                };
                contract["statusOption"] = {
                    label: this.translatePipe.transform(StatusTranslationKeys[contract.status]),
                    value: StatusResponseValues[contract.status],
                };
                contract["note"] = contract["note"] === null ? "" : contract["note"];
                contract["approvers"] = contract["approvers"] === null ? "" : contract["approvers"];
                contract["vendorContact"] = contract["vendorContact"] === null ? "" : contract["vendorContact"];
                contract["currencyOption"] = { code: contract.currency, description: "" };
                const contractData = contract;
                this.contractService.setContractDetails(contractData);
                this.previousValues = this.contractService.getContractDetails();
                this.contract = contract;
                this.contractResponse = { ...this.contract };
                this.contract.contractOwnerEmail = this.contract.contractOwner;
                this.ready = true;
            }
        });
        this.setTransitionAwayHook();
    }

    ngOnDestroy(): void {
        this.destroy$.next();
        this.destroy$.complete();
        this.contractService.reset();
        if (this.transitionDeregisterHook) {
            this.transitionDeregisterHook();
        }
    }

    onHeaderRouteClick(route: { path: string, params: any }) {
        switch (route.path) {
            case "zen.app.pia.module.vendor.inventory.list":
                this.stateService.go(route.path, { Id: InventoryTableIds.Vendors, page: null, size: null });
                break;
            case "zen.app.pia.module.vendor.inventory.documents":
                this.stateService.go(route.path, {
                    inventoryId: InventoryTableIds.Vendors,
                    recordId: this.vendorId,
                    tabId: InventoryDetailsTabs.Documents,
                });
                break;
        }
    }

    onTabClick(option: ITabsNav): void {
        this.selectedTab = option.id as ContractTabOptions;
    }

    setTabOptions(): void {
        if (this.showTabs) {
            this.tabOptions.push({
                text: this.translatePipe.transform("Document"),
                id: ContractTabOptions.Document,
                otAutoId: "VendorDetailTabDocument",
            });
        }

        this.tabOptions.push({
            text: this.translatePipe.transform("Details"),
            id: ContractTabOptions.Details,
            otAutoId: "VendorDetailTabDetails",
        });

        if (this.permissions.canShow("VendorContractInventoryLink")) {
            this.tabOptions.push({
                text: this.translatePipe.transform("Related"),
                id: ContractTabOptions.Related,
                otAutoId: "VendorDetailTabRelated",
            });
        }

        if (this.permissions.canShow("VRMContractMultipleAttachments")) {
            this.tabOptions.push({
                text: this.translatePipe.transform("Attachments"),
                id: ContractTabOptions.Attachments,
                otAutoId: "VendorDetailTabAttachments",
            });
        }

        if (this.permissions.canShow("VendorContractDetailActivity")) {
            this.tabOptions.push({
                text: this.translatePipe.transform("Activity"),
                id: ContractTabOptions.ActivityStream,
                otAutoId: "VendorDetailTabActivityStream",
            });
        }
    }

    saveContractAction(formFieldData) {
        if (formFieldData.name === "contractType") {
            this.contract.contractTypeId = formFieldData.event.id;
            this.contract.contractTypeName = formFieldData.event.name;
            this.contract.contractTypeNameKey = formFieldData.event.nameKey;
        } else if (formFieldData.name === "contractOwner") {
            const event = formFieldData.event;
            if (!isNull(event.selection)) {
                if (event.selection.Id && event.selection.Id === event.selection.FullName) {
                    event.selection.Id = null;
                }
                this.contract.contractOwner = event.selection.Id || event.selection.FullName;
                this.contract.contractOwnerId = event.selection.Id;
            } else {
                this.contract.contractOwner = null;
                this.contract.contractOwnerId = null;
            }

        } else if (formFieldData.name === "status") {
            this.contract.status = formFieldData.event.value;
        } else if (formFieldData.name === "currency") {
            this.contract.currency = formFieldData.event.code;
        } else if (formFieldData.name === "name") {
            this.contract.name = formFieldData.event;
        } else {
            this.contract[formFieldData.name] = typeof formFieldData.event === "object" ?
            formFieldData.event : formFieldData.event.trim();
        }
        this.hasEdits = true;
        this.validateSubmit();
    }

    updateContract(): ng.IPromise<IProtocolResponse<IContractDetails>> {
        const updateContractData: IContractUpdateRequest = {
            name: this.contract.name.trim(),
            status: this.contract.status,
            agreementCreatedDate: this.contract.agreementCreatedDate,
            contractTypeId: this.contract.contractTypeId,
            contractTypeName: this.contract.contractTypeName,
            contractTypeNameKey: this.contract.contractTypeNameKey,
            approvedDate: this.contract.approvedDate,
            expirationDate: this.contract.expirationDate,
            contractOwnerEmail: this.contract.contractOwnerEmail,
            contractOwnerId: this.contract.contractOwnerId,
            vendorContact: this.contract.vendorContact.trim(),
            approvers: this.contract.approvers.trim(),
            cost: this.contract.cost,
            currency: this.contract.currency,
            links: [],
            note: this.contract.note.trim(),
            url: this.contract.url,
        };
        this.hasEdits = false;
        this.contractService.setContractDetails(this.contract);
        return this.contractService.updateContract(this.contractId, updateContractData);
    }

    onSave() {
        this.updateContract().then((data: IProtocolResponse<IContractDetails>) => {
            this.editToggle(!data.result);
            this.contractResponse = { ...this.contract };
        });
    }
    cancelAction() {
        if (this.hasEdits) {
            this.contract = { ...this.contractResponse };
            this.hasEdits = false;
        }
        this.saveCancelled();
    }

    downloadContract() {
        this.launchDownloadContract(this.contract.attachments[0]);
    }

    openDeleteModal() {
        this.launchDeleteContract(this.contract.id);
    }

    editToggle(editMode) {
        this.editMode = editMode;
    }

    get currentContractName() {
        return this.contract.name;
    }

    private launchDownloadContract(attachment: IContractAttachment): void {
        this.contractsApiService.downloadContract(attachment.attachmentId).then((res: IProtocolResponse<any>): void => {
            if (res.result) {
                this.Export.basicFileDownload(res.data, attachment.fileName);
            }
        });
    }

    private launchDeleteContract(contractId: string) {
        const deleteModalData = {
            modalTitle: this.translatePipe.transform("DeleteContract"),
            confirmationText: this.translatePipe.transform("AreYouSureDeleteContract"),
            submitButtonText: this.translatePipe.transform("Delete"),
            cancelButtonText: this.translatePipe.transform("Cancel"),
            promiseToResolve: (): ng.IPromise<boolean> =>
                this.contractsApiService.deleteContract(contractId).then((response: IProtocolResponse<ISamlSettingsResponse>): boolean => {
                    if (response.result) {
                        this.onDeleteContract.emit(null);
                    }
                    this.goToDocuments();
                    return response.result;
                }),
        };
        this.modalService.setModalData(deleteModalData);
        this.modalService.openModal("deleteConfirmationModal");
    }

    private launchSaveChangesModal(callback: () => void): void {
        const modalData: ISaveChangesModalResolve = {
            modalTitle: this.translatePipe.transform("SaveChanges"),
            confirmationText: this.translatePipe.transform("UnsavedChangesSaveBeforeContinuing"),
            cancelButtonText: this.translatePipe.transform("Cancel"),
            discardChangesButtonText: this.translatePipe.transform("DiscardChangesAndContinue"),
            saveButtonText: this.translatePipe.transform("SaveAndContinue"),
            promiseToResolve: () => this.updateContract().then((data: IProtocolResponse<IContractDetails>): boolean => {
                if (data.result) {
                    this.editToggle(false);
                    callback();
                    this.hasEdits = false;
                } else {
                    this.editToggle(true);
                    this.modalService.closeModal();
                }
                return data.result;
            },
            ),
            discardCallback: () => {
                this.saveCancelled();
                callback();
                this.hasEdits = false;
                return true;
            },
        };
        this.modalService.setModalData(modalData);
        this.modalService.openModal("saveChangesModal");
        return;
    }

    private handleStateChange(toState: IStringMap<any>, toParams: IStringMap<any>, options: IStringMap<any>): boolean {
        if (!this.hasEdits) return true;
        const callback = () => {
            this.stateService.go(toState, toParams, options);
        };
        this.launchSaveChangesModal(callback);
        return false;
    }

    private setTransitionAwayHook(): void {
        const exitRoute = "zen.app.pia.module.vendor.contract-detail";
        this.transitionDeregisterHook = this.$transitions.onExit({ from: exitRoute }, (transition: Transition): HookResult => {
            return this.handleStateChange(transition.to(), transition.params("to"), transition.options);
        }) as any;
    }

    private saveCancelled(): void {
        this.editMode = false;
    }

    private goToDocuments() {
        const launchVendorParams = {
            inventoryId: InventoryTableIds.Vendors,
            recordId: this.vendorId,
            tabId: InventoryDetailsTabs.Documents,
        };
        this.stateService.go("zen.app.pia.module.vendor.inventory.documents", launchVendorParams);
    }

    private validateSubmit(): void {
        this.canSave = Boolean((this.contract.attachments && this.contract.name) && this.contract.status && this.contract.contractTypeName && this.contract.agreementCreatedDate);
    }
}
