// core
import {
    Component,
    Input,
    OnInit,
} from "@angular/core";

// services
import { AttributeGroupsActionService } from "modules/attribute-manager/services/attribute-groups-action.service";
import { VendorInformationService } from "modules/vendor/services/shared/vendor-information.service";

// Interface
import { IAttributeGroup } from "interfaces/attributes.interface";
import { IStringMap } from "interfaces/generic.interface";
import { IAttributeDetailV2 } from "interfaces/inventory.interface";

// RXJS
import { takeUntil } from "rxjs/operators";
import {
    Subject,
    combineLatest,
} from "rxjs";

@Component({
    selector: "contract-related-form",
    templateUrl: "./contract-related-form.component.html",
})

export class ContractRelatedFormComponent implements OnInit {
    @Input() attributeDetails: IStringMap<IAttributeDetailV2>;
    @Input() recordDetails: IStringMap<IAttributeDetailV2>;

    groupsData: IAttributeGroup[];
    isLoading: boolean;
    groupsOpenMap: IStringMap<boolean>;

    private destroy$ = new Subject();

    constructor(
        private attributeGroupsActionService: AttributeGroupsActionService,
        private vendorInformationService: VendorInformationService,
    ) {
    }

    ngOnInit(): void {
        combineLatest(
            this.attributeGroupsActionService.attributeGroupsView$,
            this.vendorInformationService.loadSideMenuAttributes$,
        ).pipe(
            takeUntil(this.destroy$),
        ).subscribe(([viewData, data]) => {
            if (viewData) {
                this.groupsData = viewData.groups;
                this.groupsOpenMap = this.setGroupMap(viewData.groups);
            }
            this.isLoading = data;
        });
    }

    setGroupMap(groupsData: IAttributeGroup[]): IStringMap<boolean> {
        const openMap: IStringMap<boolean> = {};
        groupsData.forEach((group: IAttributeGroup) => {
            openMap[group.id] = true;
        });
        return openMap;
    }
}
