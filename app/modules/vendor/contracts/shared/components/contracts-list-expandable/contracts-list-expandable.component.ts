// 3rd party
import {
    Component,
    Input,
    ViewChild,
    EventEmitter,
    Output,
} from "@angular/core";
import { StateService } from "@uirouter/core";

// service
import { Permissions } from "modules/shared/services/helper/permissions.service";
import { VendorApiService } from "modules/vendor/services/api/vendor-api.service";
import { ContractsApiService } from "contracts/shared/services/contracts-api.service";

// constant
import { ContractsTableColumns } from "modules/vendor/shared/constants/vendor-documents.constants";

// Interfaces
import { IProtocolResponse } from "interfaces/protocol.interface";
import { ITableColumnConfig } from "interfaces/table-config.interface";
import { IPageableMeta } from "interfaces/pagination.interface";
import {
    IContractRowContent,
    IDocumentFileResponse,
} from "interfaces/vendor/vendor-document.interface";
import { IContractTypeResponse } from "modules/vendor/shared/interfaces/vendor-upload-document.interface";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

// Enums
import { TableColumnTypes } from "enums/data-table.enum";
import { InventoryTableIds, DefaultTableConfig } from "enums/inventory.enum";

interface IContractsDocumentTable {
    rows: IContractRowContent[];
    columns?: ITableColumnConfig[];
    metaData?: IPageableMeta;
}

@Component({
    selector: "contracts-list-expandable",
    templateUrl: "./contracts-list-expandable.component.html",
})
export class ContractsListExpandableComponent {

    @Input() public recordId: string;
    @Input() public inventoryPermissions;
    @Input() public inventoryId: number;
    @Input() public recordName: string;
    @Input() isShowPagination: boolean;

    @Output() onContractPaginationDisplayed: EventEmitter<boolean> = new EventEmitter<boolean>();
    @Output() onAddContractClicked: EventEmitter<boolean> = new EventEmitter<boolean>();

    defaultTableConfig = DefaultTableConfig;
    isLoading = false;
    table: IContractsDocumentTable;
    inventoryTableIds = InventoryTableIds;

    constructor(
        private stateService: StateService,
        public translatePipe: TranslatePipe,
        public permissions: Permissions,
        private vendorApiService: VendorApiService,
        private contractsApiService: ContractsApiService,
    ) { }

    ngOnInit() {
        this.table = {
            rows: [],
            columns: [
                {
                    name: this.translatePipe.transform("ContractName"),
                    type: TableColumnTypes.Link,
                    cellValueKey: ContractsTableColumns.NAME,
                },
                {
                    name: this.translatePipe.transform("Type"),
                    sortKey: "",
                    type: TableColumnTypes.Text,
                    cellValueKey: ContractsTableColumns.TYPE,
                },
                {
                    name: this.translatePipe.transform("Status"),
                    sortKey: "",
                    type: TableColumnTypes.Text,
                    cellValueKey: ContractsTableColumns.STATUS,
                },
                {
                    name: this.translatePipe.transform("ExpirationDate"),
                    sortKey: "",
                    type: TableColumnTypes.Date,
                    cellValueKey: ContractsTableColumns.EXPIRATION,
                },
                {
                    name: this.translatePipe.transform("Cost"),
                    sortKey: "",
                    type: TableColumnTypes.Currency,
                    cellValueKey: ContractsTableColumns.COST,
                },
            ],
            metaData: {
                first: true,
                last: false,
                number: 0,
                numberOfElements: 0,
                size: DefaultTableConfig.defaultSize,
                sort: null,
                totalElements: 0,
                totalPages: 0,
            },
        };
        this.getPageData(0, DefaultTableConfig.defaultSize);
        this.vendorApiService.vendorId = this.recordId;
        this.vendorApiService.vendorName = this.recordName;
    }

    getPageData(pageNumber: number, sizeParam = DefaultTableConfig.expandedSize) {
        this.getVendorDocumentList({ page: pageNumber, size: sizeParam });
    }

    toggleExpanded() {
        this.isShowPagination = !this.isShowPagination;
        this.getPageData(0, this.isShowPagination ? DefaultTableConfig.expandedSize : DefaultTableConfig.defaultSize);
        this.onContractPaginationDisplayed.emit(this.isShowPagination);
    }

    getVendorDocumentList(paginationParams: { page: number, size: number }) {
        this.isLoading = true;
        this.contractsApiService.fetchContractsList(this.recordId, this.inventoryId, { ...paginationParams, sort: "agreementCreatedDate,desc" })
            .then((response: IProtocolResponse<IDocumentFileResponse>): void => {
                if (response.result) {
                    this.table.rows = response.data.content;
                    this.table.metaData = {
                        first: response.data.first,
                        last: response.data.last,
                        number: response.data.number,
                        numberOfElements: response.data.numberOfElements,
                        size: response.data.size,
                        sort: response.data.sort,
                        totalElements: response.data.totalElements,
                        totalPages: response.data.totalPages,
                    };
                    if (this.table.metaData.totalElements <= this.defaultTableConfig.defaultSize) {
                        this.isShowPagination = false;
                        this.onContractPaginationDisplayed.emit(this.isShowPagination);
                    }
                }
                this.isLoading = false;
            });
    }

    goToAddContract() {
        this.onAddContractClicked.emit(true);
        this.contractsApiService.fetchAllContractTypes().then((res: IProtocolResponse<IContractTypeResponse[]>): void => {
            if (res.result) {
                this.stateService.go("zen.app.pia.module.vendor.contract", { recordId: this.recordId });
            }
        });
    }
}
