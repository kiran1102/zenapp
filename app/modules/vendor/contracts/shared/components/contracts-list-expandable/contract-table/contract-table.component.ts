// 3rd party
import {
    Component,
    Inject,
    Input,
    EventEmitter,
    Output,
} from "@angular/core";
import { StateService } from "@uirouter/core";

// service
import { Permissions } from "modules/shared/services/helper/permissions.service";
import { ContractsApiService } from "contracts/shared/services/contracts-api.service";
import { ModalService } from "sharedServices/modal.service";

// constant
import {
    InventoryPermissions,
} from "constants/inventory.constant";
import {
    ContractsTableColumns,
    StatusTranslationKeys,
    StatusResponseValues,
} from "modules/vendor/shared/constants/vendor-documents.constants";

// Interfaces
import { ITableColumnConfig } from "interfaces/table-config.interface";
import {
    IContractRowContent,
    IDocumentAttachment,
} from "interfaces/vendor/vendor-document.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";
import { ISamlSettingsResponse } from "interfaces/setting.interface";
import { IDropdownOption } from "interfaces/one-dropdown.interface";
import { IStringMap } from "interfaces/generic.interface";
import { IPageableMeta } from "interfaces/pagination.interface";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

// Enums
import { TableColumnTypes } from "enums/data-table.enum";

@Component({
    selector: "contract-table",
    templateUrl: "./contract-table.component.html",
})
export class ContractTableComponent {
    @Input() rows: IContractRowContent[];
    @Input() columns: ITableColumnConfig[];
    @Input() metaData: IPageableMeta;
    @Input() public recordId: string;
    @Input() public inventoryId: string;

    @Output() addContractFromTable: EventEmitter<void> = new EventEmitter<void>();
    @Output() onDeleteContract: EventEmitter<{ page: number, size: number }> = new EventEmitter<{ page: number, size: number }>();

    colBordered = false;
    disabledRowMap = {};
    editColIndex: number;
    editRowIndex: number;
    menuClass: string;
    resizableCol = false;
    responsiveTable = true;
    rowBordered = true;
    rowNoHover = false;
    rowStriped = false;
    selectedRowMap = {};
    tableColumnTypes = TableColumnTypes;
    truncateCellContent = true;
    wrapCellContent = false;
    vendorPermissions: IStringMap<boolean>;
    isContextMenuOptionOpen = false;
    actions: IDropdownOption[];
    minimumElements = 1;
    readonly ContractsTableColumns = ContractsTableColumns;
    readonly StatusTranslationKeys = StatusTranslationKeys;
    readonly StatusResponseValues = StatusResponseValues;

    constructor(
        @Inject("Export") readonly Export: any,
        public permissions: Permissions,
        public translate: TranslatePipe,
        private contractsApiService: ContractsApiService,
        private modalService: ModalService,
        private stateService: StateService,
    ) { }

    ngOnInit(): void {
        this.vendorPermissions = {
            canViewContracts: this.permissions.canShow(InventoryPermissions[this.inventoryId].contracts),
            canCreateContracts: this.permissions.canShow(InventoryPermissions[this.inventoryId].createContracts),
            canEditContracts: this.permissions.canShow(InventoryPermissions[this.inventoryId].createContracts),
            canDownloadContracts: this.permissions.canShow(InventoryPermissions[this.inventoryId].downloadContracts),
            canDeleteContracts: this.permissions.canShow(InventoryPermissions[this.inventoryId].deleteContracts),
        };
        this.actions = this.getActions();
    }

    getActions(): IDropdownOption[] {
        const menuActions: IDropdownOption[] = [];

        if (this.vendorPermissions.canDownloadContracts && !this.permissions.canShow("VRMContractMultipleAttachments")) {
            menuActions.push({
                text: this.translate.transform("Download"),
                action: (fileResponse: IContractRowContent): void => { this.callDownloadContractApi(fileResponse.attachments[0]); },
            });
        }
        if (this.vendorPermissions.canDeleteContracts) {
            menuActions.push({
                text: this.translate.transform("Delete"),
                action: (fileRsponse: IContractRowContent): void => { this.openDeleteModal(fileRsponse.id); },
            });
        }
        return menuActions;
    }

    goToRoute(row: IContractRowContent): void {
        this.stateService.go("zen.app.pia.module.vendor.contract-detail", { contractId: row.id, recordId: row.vendorId });
    }

    toggleContextMenu(value: boolean): void {
        this.isContextMenuOptionOpen = value;
    }

    openDeleteModal(contractId: string) {
        const deleteModalData = {
            modalTitle: this.translate.transform("DeleteContract"),
            confirmationText: this.translate.transform("AreYouSureDeleteContract"),
            submitButtonText: this.translate.transform("Delete"),
            cancelButtonText: this.translate.transform("Cancel"),
            promiseToResolve: (): ng.IPromise<boolean> =>
                this.contractsApiService.deleteContract(contractId).then((response: IProtocolResponse<ISamlSettingsResponse>): boolean => {
                    if (response.result) {
                        this.onDeleteContract.emit({ page: this.metaData.numberOfElements === this.minimumElements && this.metaData.totalPages > this.minimumElements ? this.metaData.number - this.minimumElements : this.metaData.number, size: this.metaData.size });
                    }
                    return response.result;
                }),
        };
        this.modalService.setModalData(deleteModalData);
        this.modalService.openModal("deleteConfirmationModal");
    }

    private callDownloadContractApi(attachment: IDocumentAttachment): void {
        this.contractsApiService.downloadContract(attachment.attachmentId).then((res: IProtocolResponse<any>): void => {
            if (res.result) {
                this.Export.basicFileDownload(res.data, attachment.fileName);
            }
        });
    }

}
