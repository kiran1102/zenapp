// Core
import {
    Component,
    OnInit,
    ViewChild,
    Input,
    Output,
    EventEmitter,
} from "@angular/core";

// Forms
import {
    FormGroup,
    FormBuilder,
    Validators,
} from "@angular/forms";

// Service
import { NotificationService } from "modules/shared/services/provider/notification.service";
import { TimeStamp } from "oneServices/time-stamp.service";
import { TranslateService } from "@ngx-translate/core";

// Interface
import { IFileInput } from "interfaces/sub-tasks.interface";
import { IRelatedListOption } from "interfaces/inventory.interface";
import { IAttachmentFile } from "interfaces/attachment-file.interface";
import { IDateTimeOutputModel } from "@onetrust/vitreus";
import {
    IContractTypeResponse,
    ICurrenciesResponse,
    IRelatedListOptionData,
    IRelatedAssetsPA,
} from "modules/vendor/shared/interfaces/vendor-upload-document.interface";
import { ICreateData } from "modules/vendor/shared/interfaces/vendor-upload-document.interface";
import { IOtOrgUserOutput } from "sharedModules/components/ot-org-user/ot-org-user.interface";
import { IInventoryMetadata } from "modules/vendor/engagements/shared/engagement.interface";
import { ILabelValue } from "interfaces/generic.interface";

// Constants / Enums
import { FileConstraints } from "constants/file.constant";
import { InventoryTableIds } from "enums/inventory.enum";
import { FormDataTypes } from "constants/form-types.constant";
import { AddAttachmentFormControlName } from "engagements/shared/engagement.enum";

// rxjs
import { Subject } from "rxjs/Subject";
import { takeUntil } from "rxjs/operators";

@Component({
    selector: "add-contract-form",
    templateUrl: "./add-contract-form.component.html",
})

export class AddContractFormComponent implements OnInit {
    @ViewChild("fileSelect") fileSelectRef;

    @Input() formData: ICreateData[] = [];
    @Input() formDataWithAdvOpt: ICreateData[] = [];

    @Output() formValues = new EventEmitter();

    contractForm: FormGroup;
    fileSizeError = false;
    canAdd = false;
    dateCreatedValue: string;
    dateExpirationValue: string;
    dateApprovedValue: string;
    selectedContractType: IContractTypeResponse;
    contractName: string;
    selectedVendors: IRelatedAssetsPA[] = [];
    attachments: IAttachmentFile[] = [];
    isHovering: boolean;
    inventoryTableIds = InventoryTableIds;
    isSavingFile = false;
    formDataTypes = FormDataTypes;
    selectedContractStatus: ILabelValue<string>;
    contractUrl: string;
    toggleButtonIcon: string;
    isExpanded: boolean;
    originalFormData: ICreateData[] = [];
    enteredNotes: string;
    requestId: string;
    selectedCurrencyCode: string;
    selectedCost: number;
    selectedOwner: string;
    selectedOwnerId: string;
    selectedOwnerEmail: string;
    contactOfVendor: string;
    selectedApprovers: string;
    selectedAssets: IRelatedAssetsPA[] = [];
    selectedAssetsModel: IRelatedListOption[] = [];
    selectedPA: IRelatedAssetsPA[] = [];
    selectedPAModel: IRelatedListOption[] = [];
    selectedVendorsModel: IRelatedListOption[] = [];
    selectedPrimaryVendorsModel: IInventoryMetadata;
    allowedExtensions = ["csv", "doc", "docx", "jpg", "jpeg", "mpp", "msg", "pdf", "png", "ppt", "pptx", "txt", "vsd", "vsdx", "xls", "xlsx"];

    private destroy$ = new Subject();
    private translations: {};
    private translationKeys = [
        "InvalidFile",
        "FileSizeLimitViolation",
        "FileNameExceedsLimitOf100Characters",
        "ThisFileTypeIsNotAllowed",
        "AllowedFileFormats",
        "Warning",
        "PrimaryAndRelatedVendorShouldNotBeSame",
    ];

    get filesUpload() { return this.contractForm.get("filesUpload"); }
    get primaryVendor() { return this.contractForm.get("primaryVendor"); }
    get nameOfContract() { return this.contractForm.get("nameOfContract"); }
    get contractType() { return this.contractForm.get("contractType"); }
    get createdDate() { return this.contractForm.get("createdDate"); }
    get expirationDate() { return this.contractForm.get("expirationDate"); }
    get contractStatus() { return this.contractForm.get("contractStatus"); }
    get contractURL() { return this.contractForm.get("contractURL"); }
    get relatedAssetsLookUp() { return this.contractForm.get("relatedAssetsLookUp"); }
    get relatedProcessingActivitiesLookUp() { return this.contractForm.get("relatedProcessingActivitiesLookUp"); }
    get relatedVendorsLookUp() { return this.contractForm.get("relatedVendorsLookUp"); }
    get approvedDate() { return this.contractForm.get("approvedDate"); }
    get contractOwner() { return this.contractForm.get("contractOwner"); }
    get vendorContact() { return this.contractForm.get("vendorContact"); }
    get approvers() { return this.contractForm.get("approvers"); }
    get cost() { return this.contractForm.get("cost"); }
    get currency() { return this.contractForm.get("currency"); }
    get textArea() { return this.contractForm.get("textArea"); }

    constructor(
        private formBuilder: FormBuilder,
        private notificationService: NotificationService,
        private timeStamp: TimeStamp,
        private translate: TranslateService,
    ) { }

    ngOnInit() {
        this.originalFormData = [...this.formData];
        this.configureTranslations();
        this.addcontractFormGroup();
        this.toggleButtonIcon = this.isExpanded ? "ot ot-caret-down" : "fa fa-caret-right";
        this.contractForm.valueChanges.pipe(
            takeUntil(this.destroy$),
        ).subscribe(() => {
            this.validateSubmit();
        });
        this.formData.forEach((item) => {
            if (item.formControlName === AddAttachmentFormControlName.PRIMARY_VENDOR) {
                this.selectedPrimaryVendorsModel = item.inputValue as IInventoryMetadata;
                this.primaryVendor.setValue(this.selectedPrimaryVendorsModel.label);
            }
        });
    }

    ngOnDestroy() {
        this.destroy$.next();
        this.destroy$.complete();
    }

    handleFiles(files: File[]): void {
        this.fileSizeError = false;
        if (!files.length) {
            return;
        }
        if (files.find(this.checkFilesSizeLimit)) {
            this.fileSizeError = true;
            this.notificationService.alertError(this.translations["InvalidFile"],
            this.translations["FileSizeLimitViolation"]);
        } else if (files.find(this.checkFilesNameLimit)) {
            this.notificationService.alertError(this.translations["InvalidFile"],
            this.translations["FileNameExceedsLimitOf100Characters"]);
        } else {
            this.getDocument(files);
        }
    }

    checkFilesSizeLimit(file: File) {
        return file.size === 0 || file.size > FileConstraints.FILE_SIZE_LIMIT;
     }

     checkFilesNameLimit(file: File) {
        return file.name.length > FileConstraints.FILE_NAME_MAX_LENGTH;
     }

    getFileExtension(fileName: string): string {
        // Return only file extension
        return fileName.split(".").pop() || "";
    }

    getFileName(fileName: string): string {
        const ext = this.getFileExtension(fileName);
        // Return file name without extension
        return fileName.replace(`.${ext}`, "");
    }

    handleFilesHover(isHovering: boolean): void {
        this.isHovering = isHovering;
    }

    removeFile(index: number): void {
        if (!this.isSavingFile) {
            this.attachments.splice(index, 1);
            this.validateSubmit();
        }
    }

    toggleForm(): void {
        this.isExpanded = !this.isExpanded;
        this.toggleButtonIcon = this.isExpanded ? "ot ot-caret-down" : "fa fa-caret-right";
        if (this.isExpanded) {
            this.formData = [...this.formData, ...this.formDataWithAdvOpt && this.formDataWithAdvOpt.length ? this.formDataWithAdvOpt : null];
        } else {
            this.formData = this.originalFormData;
        }
    }

    selectDate(dateValue: IDateTimeOutputModel, type: string): void {
        if (type === "createdDate") {
            this.dateCreatedValue = this.formatDateToIso(dateValue);
            this.createdDate.setValue(dateValue.jsdate);
        } else if (type === "expirationDate") {
            this.dateExpirationValue = this.formatDateToIso(dateValue);
            this.expirationDate.setValue(dateValue.jsdate);
        } else {
            this.dateApprovedValue = this.formatDateToIso(dateValue);
            this.approvedDate.setValue(dateValue.jsdate);
        }
    }

    formatDateToIso(dateValue: IDateTimeOutputModel) {
        return dateValue.dateTime ? this.timeStamp.formatDateToIso({ day: dateValue.jsdate.getDate(), month: dateValue.jsdate.getMonth() + 1, year: dateValue.jsdate.getFullYear() }) : null;
    }

    handleInvalidFiles(files: File[]): void {
        if (!files.length) {
            return;
        }
        if (files[0].size === 0 || files[0].size > FileConstraints.FILE_SIZE_LIMIT) {
            this.fileSizeError = true;
            this.notificationService.alertError(this.translations["InvalidFile"], this.translations["FileSizeLimitViolation"]);
        } else {
            const errMsg = this.translate.instant(["AllowedFileFormats"], {formats: this.allowedExtensions.join(", ") });
            this.notificationService.alertError(this.translations["ThisFileTypeIsNotAllowed"], errMsg["AllowedFileFormats"]);
        }
    }

    triggerFileUpload(): void {
        this.fileSelectRef.nativeElement.click();
    }

    selectFromLookUp(data: IRelatedListOptionData): void {
        const { options, type } = data;
        if (options) {
            if (type === this.inventoryTableIds.Assets) {
                this.selectedAssets = [];
                this.selectedAssetsModel = options as IRelatedListOption[];
                (options as IRelatedListOption[]).forEach((option) => {
                    this.selectedAssets.push({
                        inventoryType: "ASSETS",
                        inventoryName: option.name,
                        inventoryId: option.id,
                        orgGroupId: option.organization.id,
                        attributes: {
                            location: {
                                id: option.location.id,
                                value: option.location.value,
                                valueKey: option.location.value || "",
                            },
                        },
                    });
                });
            } else if (type === this.inventoryTableIds.Processes) {
                this.selectedPA = [];
                this.selectedPAModel = (options as IRelatedListOption[]);
                (options as IRelatedListOption[]).forEach((option) => {
                    this.selectedPA.push({
                        inventoryType: "PROCESSING_ACTIVITIES",
                        inventoryName: option.name,
                        inventoryId: option.id,
                        orgGroupId: option.organization.id,
                    });
                });
            } else if (type === this.inventoryTableIds.Vendors) {
                this.selectedVendors = [];
                this.selectedVendorsModel = (options as IRelatedListOption[]);
                (options as IRelatedListOption[]).forEach((option) => {
                    this.selectedVendors.push({
                        inventoryType: "VENDORS",
                        inventoryName: option.name,
                        inventoryId: option.id,
                        orgGroupId: option.organization.id,
                        attributes: {
                            type: {
                                id: option.type.id,
                                value: option.type.value,
                                valueKey: option.type.value || "",
                            },
                        },
                    });
                });
            }
            if (this.validateSelectedVendor()) {
                this.notificationService.alertWarning(this.translations["Warning"], this.translations["PrimaryAndRelatedVendorShouldNotBeSame"]);
            }
            this.validateSubmit();
        }
    }

    updateSelectedCurrency(currency: ICurrenciesResponse) {
        this.currency.setValue(currency);
        this.selectedCurrencyCode = currency.code;
    }

    handleEnteredNotes(note: string) {
        this.enteredNotes = note;
    }

    selectOption(option: IContractTypeResponse | ILabelValue<string>, type: string) {
        if (type === "contractType") {
            this.selectedContractType = (option as IContractTypeResponse);
            this.contractType.setValue({name: this.translate.instant(this.selectedContractType.nameKey), nameKey: this.selectedContractType.name, id: this.selectedContractType.id});
        } else {
            this.selectedContractStatus = (option as ILabelValue<string>);
            this.contractStatus.setValue(this.selectedContractStatus);
        }
    }

    selectContractOwner(option: IOtOrgUserOutput): void {
        // we have to send null to backend if we are removing a selected owner.
        if (option && option.selection && option.selection.Id) {
            // Since the component is returning Id and FullName same for External/Invited user Id
            if (option.selection.Id === option.selection.FullName) {
                option.selection.Id = null;
            }
            this.selectedOwnerEmail = option.selection.FullName;
            this.selectedOwner = option.selection.Id || option.selection.FullName;
            this.selectedOwnerId = option.selection.Id;
        } else {
            this.selectedOwner = null;
            this.selectedOwnerEmail = null;
            this.selectedOwnerId = null;
        }
        this.contractOwner.setValue(this.selectedOwner);
    }

    selectCostValue(event: number) {
        this.selectedCost = event;
    }

    inputValueChange(inputVal: string, type: string) {
        if (inputVal) {
            if (type === "nameOfContract") {
                this.contractName = inputVal.trim();
            } else if (type === "contractURL") {
                this.contractUrl = inputVal.trim();
            } else if (type === "vendorContact") {
                this.contactOfVendor = inputVal.trim();
            } else if (type === "approvers") {
                this.selectedApprovers = inputVal.trim();
            }
        }
    }

    private configureTranslations() {
        this.translate.stream(this.translationKeys).subscribe((translations) => {
            this.translations = translations;
        });
    }

    private getDocument(files: File[]) {
        if (files.length) {
            files.forEach((file: File) => {
                const reader: FileReader = new FileReader();
                const fileObj: IFileInput = {
                    Data: "",
                    FileName: this.getFileName(file.name),
                    Extension: this.getFileExtension(file.name),
                    Blob: file,
                };
                reader.readAsDataURL(file);
                reader.onload = () => {
                    fileObj.Data = reader.result.toString();
                    if (fileObj.Data.length) {
                        this.attachments.push(fileObj);
                        this.validateSubmit();
                    }
                };
            });
        }
    }

    private validateSubmit(): void {
        this.canAdd = Boolean(
            this.contractForm.valid && !this.fileSizeError && !this.validateSelectedVendor(),
        );
        this.addControlFieldValues();
        this.formValues.emit({ contractForm: this.contractForm, canAdd: this.canAdd });
    }

    private addControlFieldValues() {
        this.contractForm.value["relatedAssetsLookUp"] =   this.selectedAssets && this.selectedAssets.length ? this.selectedAssets : null;
        this.contractForm.value["relatedProcessingActivitiesLookUp"] =   this.selectedPA && this.selectedPA.length ? this.selectedPA : null;
        this.contractForm.value["relatedVendorsLookUp"] =   this.selectedVendors && this.selectedVendors.length ? this.selectedVendors : null;
        this.contractForm.value["filesUpload"] = this.attachments && this.attachments.length ? this.attachments : null;
        this.contractForm.value["primaryVendor"] = this.selectedPrimaryVendorsModel;
    }

    private validateSelectedVendor(): IRelatedAssetsPA {
        let identicalVendors: IRelatedAssetsPA;
        if (this.selectedPrimaryVendorsModel && this.selectedVendors) {
            identicalVendors = this.selectedVendors.find((vendor) => vendor.inventoryId === this.selectedPrimaryVendorsModel.id);
        }
        return identicalVendors;
    }

    private addcontractFormGroup() {
        this.contractForm = this.formBuilder.group({
            filesUpload: [null],
            primaryVendor: [{ value: null, disabled: true }, [Validators.required]],
            nameOfContract: [null, [Validators.required, Validators.maxLength(100)]],
            contractType: [null, [Validators.required]],
            createdDate: [null, [Validators.required]],
            expirationDate: [null],
            contractStatus: [null, [Validators.required]],
            contractURL: [null, [Validators.maxLength(300)]],
            relatedAssetsLookUp: [null],
            relatedProcessingActivitiesLookUp: [null],
            relatedVendorsLookUp: [null],
            approvedDate: [null],
            contractOwner: [null],
            vendorContact: [null, [Validators.maxLength(100)]],
            approvers: [null, [Validators.maxLength(100)]],
            cost: [null],
            currency: [null],
            textArea: [null],
        });
    }
}
