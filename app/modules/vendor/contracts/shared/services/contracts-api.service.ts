// Core
import { Injectable } from "@angular/core";

// Rxjs
import {
    Observable,
    from,
} from "rxjs";

// Services
import { ProtocolService } from "modules/core/services/protocol.service";

// Interfaces
import {
    IProtocolConfig,
    IProtocolMessages,
    IProtocolResponse,
} from "interfaces/protocol.interface";
import { IFetchDetailsParams } from "modules/vendor/shared/interfaces/vendorpedia-exchange.interface";
import {
    IContractTypeResponse,
    IContractDetails,
    IContractUpdateRequest,
    ICurrenciesResponse,
    IRelatedAssetsPA,
    IContractAttachment,
} from "modules/vendor/shared/interfaces/vendor-upload-document.interface";
import { IPageableOf } from "interfaces/pagination.interface";
import {
    IDocumentFileResponse,
    IContractRowContent,
} from "interfaces/vendor/vendor-document.interface";
import {
    IAttachmentFileResponse,
} from "interfaces/attachment-file.interface";
import {
    IStringMap,
    ILabelValue,
} from "interfaces/generic.interface";
import { ISamlSettingsResponse } from "interfaces/setting.interface";
import { IAuditHistoryItem } from "interfaces/audit-history.interface";
import { IContractRelatedRow } from "interfaces/inventory.interface";
import { IGridPaginationParams } from "interfaces/grid.interface";
import { IContractAPIConfig } from "contracts/contract-list/contract-list.interface";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

// Enums
import { InventoryTableIds } from "enums/inventory.enum";
import { ContractListContext } from "contracts/contract-list/contract-list.enum";
import { ContractsAPI } from "contracts/contract-list/contract-list.constant";

@Injectable()
export class ContractsApiService {

    constructor(
        private protocolService: ProtocolService,
        private translatePipe: TranslatePipe,
    ) { }

    fetchContractsList(recordId: string, inventoryId: number, params?: IStringMap<number | string>): ng.IPromise<IProtocolResponse<IDocumentFileResponse>> {
        const config: IProtocolConfig = this.protocolService.customConfig("POST", `/api/vendor/v1/contracts/${inventoryId === InventoryTableIds.Vendors ? "vendors" : "links/inventories"}/${recordId}/contracts`, params);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("ErrorUpdatingRecord") } };
        return this.protocolService.http(config, messages);
    }

    fetchAllContractTypes(): ng.IPromise<IProtocolResponse<IContractTypeResponse[]>> {
        const config: IProtocolConfig = this.protocolService.customConfig("GET", `/api/vendor/v1/contracts/types`);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("Vendor.Error.GetContractTypes") } };
        return this.protocolService.http(config, messages);
    }

    fetchAllCurrencies(): ng.IPromise<IProtocolResponse<ICurrenciesResponse[]>> {
        const config: IProtocolConfig = this.protocolService.customConfig("GET", `/api/vendor/v1/contracts/currencies`);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("Vendor.Error.GetAllCurrencies") } };
        return this.protocolService.http(config, messages);
    }

    fetchContractStatuses(): Observable<IProtocolResponse<Array<ILabelValue<string>>>> {
        const config: IProtocolConfig = this.protocolService.customConfig("GET", `/api/vendor/v1/contracts/statuses`);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("ErrorGettingContractStuses") } };
        return from(this.protocolService.http(config, messages));
    }

    addContract(payload: IContractDetails): ng.IPromise<IProtocolResponse<string>> {
        const config: IProtocolConfig = this.protocolService.customConfig("POST", `/api/vendor/v1/contracts`, null, payload);
        const messages: IProtocolMessages = {
            Error: { custom: this.translatePipe.transform("ErrorAddingContract") },
            Success: { custom: this.translatePipe.transform("ContractSuccessfullyAdded") },
        };
        return this.protocolService.http(config, messages);
    }

    updateContract(contractId: string, contract: IContractUpdateRequest): ng.IPromise<IProtocolResponse<IContractDetails>> {
        const config: IProtocolConfig = this.protocolService.customConfig("PUT", `/api/vendor/v1/contracts/${contractId}/contracts`, null, contract);
        const messages: IProtocolMessages = {
            Error: { custom: this.translatePipe.transform("Vendor.Error.UpdatingContract") },
            Success: { custom: this.translatePipe.transform("Vendor.Success.UpdatingContract") },
        };
        return this.protocolService.http(config, messages);
    }

    fetchContractById(contractId: string): ng.IPromise<IProtocolResponse<IContractDetails>> {
        const config: IProtocolConfig = this.protocolService.customConfig("GET", `/api/vendor/v1/contracts/${contractId}/contracts`);
        const messages: IProtocolMessages = { Error: { Hide: true } };
        return this.protocolService.http(config, messages);
    }

    // TODO: Attachment API Service
    downloadContract(attachmentId: string): ng.IPromise<IProtocolResponse<any>> {
        const config: IProtocolConfig = this.protocolService.customConfig("GET", `/api/v1/private/file/downloadFile/${attachmentId}`, null, null, null, null, null, "arraybuffer");
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("ErrorDownloadingFile") } };
        return this.protocolService.http(config, messages);
    }

    deleteContract(contractId: string): ng.IPromise<IProtocolResponse<ISamlSettingsResponse>> {
        const config: IProtocolConfig = this.protocolService.customConfig("DELETE", `/api/vendor/v1/contracts/${contractId}/contracts`);
        const messages: IProtocolMessages = {
            Error: { custom: this.translatePipe.transform("ErrorInDeletingContract") },
            Success: { custom: this.translatePipe.transform("ContractSuccessfullyDeleted") },
        };
        return this.protocolService.http(config, messages);
    }

    // TODO: Audit API Service
    fetchContractActivity(id: string, page: number = 0, size: number = 8): ng.IPromise<IProtocolResponse<IPageableOf<IAuditHistoryItem>>> {
        const config: IProtocolConfig = this.protocolService.customConfig("GET", `/api/audit/v1/audit/${id}/history/`, { page, size });
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("ErrorRetrievingActivity") } };
        return this.protocolService.http(config, messages);
    }

    // Below API's for vendor related tab
    getRelatedContract(contractId: string, params: IStringMap<number | string>): ng.IPromise<IProtocolResponse<IPageableOf<IContractRelatedRow>>> {
        const config: IProtocolConfig = this.protocolService.customConfig("GET", `/api/vendor/v1/contracts/${contractId}/links`, params);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("ErrorRetrievingRelatedVendor") } };
        return this.protocolService.http(config, messages);
    }

    addContractLinksByContractId(contractId: string, payload: IRelatedAssetsPA): ng.IPromise<IProtocolResponse<string>> {
        const config: IProtocolConfig = this.protocolService.customConfig("POST", `/api/vendor/v1/contracts/${contractId}/links`, null, payload);
        const messages: IProtocolMessages = {
            Error: { custom: this.translatePipe.transform("ErrorInAddingContractLink") },
            Success: { custom: this.translatePipe.transform("ContractLinkSuccessfullyAdded") },
        };
        return this.protocolService.http(config, messages);
    }

    deleteContractLink(contractId: string, linkId: string): ng.IPromise<IProtocolResponse<ISamlSettingsResponse>> {
        const config: IProtocolConfig = this.protocolService.customConfig("DELETE", `/api/vendor/v1/contracts/${contractId}/links/${linkId}/links`);
        const messages: IProtocolMessages = {
            Error: { custom: this.translatePipe.transform("ErrorInDeletingContractLink") },
            Success: { custom: this.translatePipe.transform("ContractLinkSuccessfullyDeleted") },
        };
        return this.protocolService.http(config, messages);
    }

    // Below API's for vendor attachments tab
    getContractAttachments(contractId: string, params: IStringMap<number | IFetchDetailsParams>): ng.IPromise<IProtocolResponse<IPageableOf<IAttachmentFileResponse>>> {
        const config: IProtocolConfig = this.protocolService.customConfig("GET", `/api/vendor/v1/contracts/${contractId}/attachments`, params);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("ErrorRetrievingContractAttachments") } };
        return this.protocolService.http(config, messages);
    }

    addContractAttachments(contractId: string, payload: IContractAttachment[]): ng.IPromise<IProtocolResponse<IContractAttachment[]>> {
        const config: IProtocolConfig = this.protocolService.customConfig("POST", `/api/vendor/v1/contracts/${contractId}/attachments`, null, payload);
        const messages: IProtocolMessages = {
            Error: { custom: this.translatePipe.transform("ErrorAddingAttachment") },
            Success: { custom: this.translatePipe.transform("AttachmentSuccessfullyAdded") },
        };
        return this.protocolService.http(config, messages);
    }

    deleteAttachment(contractId: string, attachmentId: string): ng.IPromise<IProtocolResponse<ISamlSettingsResponse>> {
        const config: IProtocolConfig = this.protocolService.customConfig("DELETE", `/api/vendor/v1/contracts/${contractId}/attachments/${attachmentId}/attachments`);
        const messages: IProtocolMessages = {
            Error: { custom: this.translatePipe.transform("ErrorInDeletingAttachment") },
            Success: { custom: this.translatePipe.transform("AttachmentSuccessfullyDeleted") },
        };
        return this.protocolService.http(config, messages);
    }

    fetchAllContracts(apiConfig: IContractAPIConfig): ng.IPromise<IProtocolResponse<IPageableOf<IContractRowContent>>> {
        const requestBody = {
            fullText: apiConfig.searchText,
            filters: [...apiConfig.query],
        };
        const url = ContractsAPI[apiConfig.context].getContracts(apiConfig.contextId);
        const config: IProtocolConfig = this.protocolService.customConfig("POST", url, apiConfig.pagination, requestBody);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("ErrorUpdatingRecord") } };
        return this.protocolService.http(config, messages);
    }
}
