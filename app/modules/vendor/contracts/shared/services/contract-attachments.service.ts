import { Injectable } from "@angular/core";

// Rxjs
import {
    BehaviorSubject,
} from "rxjs";

// Services
import { VendorApiService } from "modules/vendor/services/api/vendor-api.service";
import { ContractsApiService } from "contracts/shared/services/contracts-api.service";

// Interfaces
import { IProtocolResponse } from "interfaces/protocol.interface";
import {
    IPageableOf,
    IPageableMeta,
} from "interfaces/pagination.interface";
import {
    IAttachmentFile,
    IAttachmentFileResponse,
} from "interfaces/attachment-file.interface";
import { IStringMap } from "interfaces/generic.interface";
import { IContractAttachment } from "modules/vendor/shared/interfaces/vendor-upload-document.interface";

@Injectable()
export class ContractAttachmentsService {

    vendorAttachments$ = new BehaviorSubject<IAttachmentFileResponse[]> (null);
    vendorAttachmentMeta$ = new BehaviorSubject<IPageableMeta> (null);

    constructor(
        private vendorApiService: VendorApiService,
        private contractsApiService: ContractsApiService,

    ) { }

    reset(): void {
        this.vendorAttachments$.next(null);
        this.vendorAttachmentMeta$.next(null);
    }

    retrieveVendorAttachments(contractId: string, paginationParams: IStringMap<number>): void {
        this.contractsApiService.getContractAttachments(contractId, paginationParams).then((res: IProtocolResponse<IPageableOf<IAttachmentFileResponse>>): void => {
            if (res.result) {
                const attachmentMeta = { ...res.data };
                delete attachmentMeta.content;
                this.vendorAttachmentMeta$.next(attachmentMeta);
                this.vendorAttachments$.next(res.data.content);
            }
        });
    }

    refreshVendorAttachments(contractId: string): void {
        let page = 0;
        let size = 10;
        const meta = this.vendorAttachmentMeta$.getValue();
        if (meta && typeof meta.number === "number") {
            page = meta.number !== 0 && meta.numberOfElements === 1 ? meta.number - 1 : meta.number;
            size = meta.size;
        }
        this.retrieveVendorAttachments(contractId, { page, size });
    }

    saveFile(contractId, files): Promise<boolean> {
        return new Promise((resolve, reject) => {
            const attachments: IContractAttachment[] = [];
            const promiseArr: Array<ng.IPromise<IProtocolResponse<IAttachmentFile>>> = [];
            if (files && files.length > 0) {
                files.forEach((file, i) => {
                    const fileData: IAttachmentFile = {
                        Blob: file,
                        FileName: file.name,
                        Name: name,
                        RequestId: contractId,
                        RefIds: [contractId],
                        Comments: "",
                        IsInternal: true,
                        Extension: file.name.substr(file.name.lastIndexOf(".") + 1),
                    };
                    promiseArr.push(this.vendorApiService.uploadDocument(fileData));
                });
                if (promiseArr.length) {
                    Promise.all(promiseArr)
                        .then((responses: Array<IProtocolResponse<IAttachmentFile>>): void => {
                            if ((responses instanceof Array) && responses.length) {
                                const failedResponses: Array<IProtocolResponse<IAttachmentFile>> = responses.filter((res) => !res.result);
                                if (!failedResponses.length) {
                                    responses.forEach(
                                        (res: IProtocolResponse<IAttachmentFile>): void => {
                                            const attachmentObjectForComment: IContractAttachment = {
                                                attachmentId: res.data.Id,
                                                fileName: res.data.FileName,
                                                attachmentDescription: res.data.Comments,
                                            };
                                            attachments.push(
                                                attachmentObjectForComment,
                                            );
                                        },
                                    );
                                    this.contractsApiService.addContractAttachments(contractId, attachments)
                                        .then((response: IProtocolResponse<IContractAttachment[]>): void => {
                                            if (!response || !response.result) {
                                                reject(false);
                                            }
                                            this.refreshVendorAttachments(contractId);
                                            resolve(true);
                                        });
                                }
                            }

                    });
                }
            }
        });
    }

    removeAttachment(contractId, attachmentId: string): void {
        this.contractsApiService.deleteAttachment(contractId, attachmentId)
            .then((res) => this.refreshVendorAttachments(contractId))
            .catch((err) => null);
    }
}
