import { Injectable } from "@angular/core";

// Rxjs
import {
    BehaviorSubject,
    Observable,
} from "rxjs";

// Services
import { NotificationService } from "sharedModules/services/provider/notification.service";
import { ContractsApiService } from "contracts/shared/services/contracts-api.service";

// Interfaces
import { IProtocolResponse } from "interfaces/protocol.interface";
import {
    IContractDetails,
    IContractUpdateRequest,
    ICurrenciesResponse,
    IContractTypeResponse,
} from "modules/vendor/shared/interfaces/vendor-upload-document.interface";
import { IVendorPath } from "modules/vendor/shared/interfaces/vendor-path.interface";
import { IAuditHistoryItem } from "interfaces/audit-history.interface";
import { IPageableOf, IPageableMeta } from "interfaces/pagination.interface";
import { IContractRowContent } from "interfaces/vendor/vendor-document.interface";
import { ILabelValue } from "interfaces/generic.interface";
import { IContractAPIConfig } from "contracts/contract-list/contract-list.interface";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

@Injectable()
export class ContractService {

    contractDetailModel$ = new BehaviorSubject<IContractDetails>(null);
    vendorContractTypes$ = new BehaviorSubject<IContractTypeResponse[]>(null);
    vendorContractAllCurrencies$ = new BehaviorSubject<ICurrenciesResponse[]>(null);
    vendorContractActivityList$ = new BehaviorSubject<IAuditHistoryItem[]>(null);
    vendorContractActivityListMetaData$ = new BehaviorSubject<IPageableMeta>(null);
    vendorStagePath$ = new BehaviorSubject<IVendorPath>(null);
    contracts = new BehaviorSubject<IPageableOf<IContractRowContent>>(null);
    contracts$ = this.contracts.asObservable();
    contractId: string;
    contractStatuses$: Observable<Array<ILabelValue<string>>>;

    private _contractStatuses = new BehaviorSubject<Array<ILabelValue<string>>>(null);
    private contract: IContractDetails;

    constructor(
        private translatePipe: TranslatePipe,
        private contractsApiService: ContractsApiService,
        private notificationService: NotificationService,
    ) {
        this.contractStatuses$ = this._contractStatuses.asObservable();
    }

    setContractDetails(value) {
        this.contract = { ...value };
    }

    getContractDetails() {
        return { ...this.contract };
    }

    reset(): void {
        this.contractDetailModel$.next(null);
        this.vendorContractTypes$.next(null);
        this.vendorContractAllCurrencies$.next(null);
        this.vendorStagePath$.next(null);
        this._contractStatuses.next(null);
    }

    retrieveContractStatuses() {
        if (!this._contractStatuses.getValue()) {
            this.contractsApiService.fetchContractStatuses().subscribe((response: IProtocolResponse<Array<ILabelValue<string>>>) => {
                if (response && response.result) {
                    this._contractStatuses.next(response.data);
                }
            });
        }
    }

    createNewContract(newContract: IContractDetails): ng.IPromise<IProtocolResponse<string>> {
        return this.contractsApiService.addContract(newContract);
    }

    retrieveContractById(contractId: string): void {
        this.contractsApiService.fetchContractById(contractId).then((res: IProtocolResponse<IContractDetails>): void => {
            if (res.result) {
                this.contractDetailModel$.next(res.data);
            } else {
                this.notificationService.alertError(this.translatePipe.transform("Error"), this.translatePipe.transform("Vendor.Error.RetrievingContract"));
            }
        });
    }

    updateContract(contractId: string, contract: IContractUpdateRequest): ng.IPromise<IProtocolResponse<IContractDetails>> {
        return this.contractsApiService.updateContract(contractId, contract);
    }

    retrieveContractTypes(): void {
        this.contractsApiService.fetchAllContractTypes().then((res: IProtocolResponse<IContractTypeResponse[]>): void => {
            if (!res.result) {
                this.vendorContractTypes$.next([]);
                return;
            }
            this.vendorContractTypes$.next(res.data);
        });
    }

    retrieveCurrencies(): void {
        this.contractsApiService.fetchAllCurrencies().then((res: IProtocolResponse<ICurrenciesResponse[]>): void => {
            if (!res.result) {
                this.vendorContractAllCurrencies$.next([]);
                return;
            }
            this.vendorContractAllCurrencies$.next(res.data);
        });
    }

    retrieveContractActivity(id: string, page: number = 0, size: number = 8): void {
        this.contractsApiService.fetchContractActivity(id, page, size).then((res: IProtocolResponse<IPageableOf<IAuditHistoryItem>>): void => {
            if (res.result) {
                const activityListMeta = { ...res.data };
                delete activityListMeta.content;
                this.vendorContractActivityListMetaData$.next(activityListMeta);
                this.vendorContractActivityList$.next(res.data.content);
            }
        });
    }

    retrieveMoreContractActivities(incidentId: string, page: number = 0, size: number = 8): void {
        this.contractsApiService.fetchContractActivity(incidentId, page, size).then((res: IProtocolResponse<IPageableOf<IAuditHistoryItem>>): void => {
            if (res.result) {
                const activityListMeta = { ...res.data };
                delete activityListMeta.content;
                this.vendorContractActivityListMetaData$.next(activityListMeta);
                this.vendorContractActivityList$.next([...this.vendorContractActivityList$.getValue(), ...res.data.content]);
            }
        });
    }

    fetchAllContracts(config: IContractAPIConfig): void {
        this.contractsApiService.fetchAllContracts(config).then((res: IProtocolResponse<IPageableOf<IContractRowContent>>): void => {
            if (res.result) {
                const contracts = { ...res.data };
                this.contracts.next(contracts);
            }
        });
    }

}
