import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from "@angular/core";

// Modules
import { SharedModule } from "sharedModules/shared.module";
import { UpgradedLegacyModule } from "sharedModules/upgrade/upgraded-legacy.module";
import { CommonModule } from "@angular/common";
import { HttpClientModule } from "@angular/common/http";
import { FormsModule } from "@angular/forms";
import { ReactiveFormsModule } from "@angular/forms";
import { VitreusModule } from "@onetrust/vitreus";
import { DndListModule } from "ngx-drag-and-drop-lists";
import { OTPipesModule, PIPES } from "modules/pipes/pipes.module";
import { VirtualScrollModule } from "od-virtualscroll";
import { OneVideoModule } from "modules/video/video.module";
import { InventorySharedModule } from "modules/inventory/inventory-shared/inventory-shared.module";
import { TranslateModule } from "@ngx-translate/core";

// Services
import { ContractsApiService } from "contracts/shared/services/contracts-api.service";
import { ContractService } from "contracts/shared/services/contract.service";
import { ContractAttachmentsService } from "contracts/shared/services/contract-attachments.service";

// Components
import { ContractTableComponent } from "contracts/shared/components/contracts-list-expandable/contract-table/contract-table.component";
import { ContractsListExpandableComponent } from "contracts/shared/components/contracts-list-expandable/contracts-list-expandable.component";
import { AddContractFormComponent } from "contracts/shared/components/add-contract-form/add-contract-form.component";

@NgModule({
    imports: [
        SharedModule,
        UpgradedLegacyModule,
        CommonModule,
        HttpClientModule,
        FormsModule,
        ReactiveFormsModule,
        VitreusModule,
        DndListModule,
        OTPipesModule,
        VirtualScrollModule,
        OneVideoModule,
        InventorySharedModule,
        TranslateModule,
    ],
    declarations: [
        ContractsListExpandableComponent,
        ContractTableComponent,
        AddContractFormComponent,
    ],
    exports: [
        ContractsListExpandableComponent,
        ContractTableComponent,
        AddContractFormComponent,
    ],
    entryComponents: [
        AddContractFormComponent,
    ],
    providers: [
        ContractService,
        ContractsApiService,
        ContractAttachmentsService,
        PIPES, // TODO: Remove from providers when ot-common is implemented. Only necessary if you are using pipe in TS file, else just module is fine
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class ContractsSharedModule {}
