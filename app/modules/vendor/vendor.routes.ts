// Interfaces
import { IGeneralData } from "app/scripts/PIA/pia.routes";

// Services
import { Permissions } from "modules/shared/services/helper/permissions.service";
import { InventoryService } from "modules/inventory/inventory-shared/shared/services/inventory-api.service";

// Lodash
import { toInteger } from "lodash";

// Enums & Constants
import { InventoryTableIds } from "enums/inventory.enum";
import {
    InventoryDetailsTabs,
    InventoryPermissions,
    ModuleContext,
} from "constants/inventory.constant";
import {
    LegacyInventorySchemaIds,
    InventorySchemaIds,
} from "constants/inventory-config.constant";

export default function routes($stateProvider: ng.ui.IStateProvider) {
    $stateProvider
        .state("zen.app.pia.module.vendor", {
            abstract: true,
            url: "vendor/",
            resolve: {
                VendorPermission: [
                    "GeneralData",
                    "$state",
                    "Permissions",
                    "Inventory",
                    (
                        GeneralData: IGeneralData,
                        $state: ng.ui.IStateService,
                        permissions: Permissions,
                        Inventory: InventoryService,
                    ) => {
                        if (permissions.canShow("VendorRiskManagement")) {
                            // Required to initiate flyway scripts for the VendorControls => InventoryControls migration
                            return Inventory.getSchemasV2().then((res) => {
                                if (res.result) {
                                    return true;
                                } else {
                                    permissions.goToFallback();
                                }
                            });
                        } else {
                            permissions.goToFallback();
                        }
                        return false;
                    },
                ],
            },
            views: {
                module: {
                    template: "<vendor-wrapper></vendor-wrapper>",
                },
            },
        })
        .state("zen.app.pia.module.vendor.dashboard", {
            url: "dashboard",
            views: {
                vendor_body: {
                    template: "<downgrade-vendor-dashboard></downgrade-vendor-dashboard>",
                },
            },
        })
        .state("zen.app.pia.module.vendor.inventory", {
            abstract: true,
            url: "inventory",
            views: {
                vendor_body: {
                    template: "<ui-view></ui-view>",
                },
            },
        })
        .state("zen.app.pia.module.vendor.engagements", {
            url: "engagements",
            resolve: {
                EngagementsPermission: [
                    "VendorPermission",
                    "$state",
                    "Permissions",
                    (
                        VendorPermission: boolean,
                        $state: ng.ui.IStateService,
                        permissions: Permissions,
                    ): boolean => {
                        if (permissions.canShow("VRMEngagements")) {
                            return true;
                        } else {
                            permissions.goToFallback();
                        }
                        return false;
                    },
                ],
            },
            views: {
                vendor_body: {
                    template: "<downgrade-engagement-list></downgrade-engagement-list>",
                },
            },
        })
        .state("zen.app.pia.module.vendor.add-engagement-wizard", {
            url: "engagements/add",
            resolve: {
                EngagementsAddPermission: [
                    "VendorPermission",
                    "$state",
                    "Permissions",
                    (
                        VendorPermission: boolean,
                        $state: ng.ui.IStateService,
                        permissions: Permissions,
                    ): boolean => {
                        if (permissions.canShow("VRMEngagementsAdd")) {
                            return true;
                        } else {
                            permissions.goToFallback();
                        }
                        return false;
                    },
                ],
            },
            views: {
                vendor_body: {
                    template: "<downgrade-add-engagement-wizard></downgrade-add-engagement-wizard>",
                },
            },
        })
        .state("zen.app.pia.module.vendor.engagement-detail", {
            url: "engagements/:id/details",
            params: { currentTab: null, name: null },
            resolve: {
                EngagementsPermission: [
                    "VendorPermission",
                    "$state",
                    "Permissions",
                    (
                        VendorPermission: boolean,
                        $state: ng.ui.IStateService,
                        permissions: Permissions,
                    ): boolean => {
                        if (permissions.canShow("VRMEngagements")) {
                            return true;
                        } else {
                            permissions.goToFallback();
                        }
                        return false;
                    },
                ],
            },
            views: {
                vendor_body: {
                    template: "<downgrade-engagement-detail></downgrade-engagement-detail>",
                },
            },
        })
        .state("zen.app.pia.module.vendor.templates", {
            url: "templates",
            resolve: {
                VendorTemplatesPermission: [
                    "VendorPermission",
                    "$state",
                    "Permissions",
                    (
                        VendorPermission: boolean,
                        $state: ng.ui.IStateService,
                        permissions: Permissions,
                    ): boolean => {
                        if (permissions.canShow("VendorRiskManagement")) {
                            return true;
                        } else {
                            permissions.goToFallback();
                        }
                        return false;
                    },
                ],
            },
            views: {
                vendor_body: {
                    template: "<downgrade-custom-templates-component></downgrade-custom-templates-component>",
                },
            },
        })
        .state("zen.app.pia.module.vendor.attribute-manager", {
            url: "attribute-manager/:id",
            views: {
                vendor_body: {
                    template: "<attribute-manager></attribute-manager>",
                },
            },
            params: { context: ModuleContext.Vendor },
        })
        .state("zen.app.pia.module.vendor.attribute-manager-details", {
            url: "attribute-manager/:id/:attributeId/:tabId",
            params: {
                time: null,
                tabId: { dynamic: true, value: "details" },
                context: ModuleContext.Vendor,
            },
            views: {
                vendor_body: {
                    template: "<attribute-manager-details></attribute-manager-details>",
                },
            },
        })
        .state("zen.app.pia.module.vendor.vendorpedia-exchange", {
            url: "vendorpedia-exchange",
            views: {
                vendor_body: {
                    template: "<downgrade-vendorpedia-exchange></downgrade-vendorpedia-exchange>",
                },
            },
        })
        .state("zen.app.pia.module.vendor.vendorpedia-request", {
            url: "vendorpedia-request",
            views: {
                vendor_body: {
                    template: "<downgrade-vendorpedia-request></downgrade-vendorpedia-request>",
                },
            },
        })
        .state("zen.app.pia.module.vendor.vendorpedia-request-new", {
            url: "vendorpedia-request/new",
            params: {
                name: "",
                org: "",
                inventoryId: "",
                vendorpediaId: "",
                previousState: "",
            },
            views: {
                vendor_body: {
                    template: "<downgrade-vendorpedia-request-new class='height-100 flex column-nowrap'></downgrade-vendorpedia-request-new>",
                },
            },
        })
        .state("zen.app.pia.module.vendor.vendorpedia-preview", {
            url: "vendorpedia-preview/:inventoryId",
            params: {
                inventoryId: "",
                vendorpediaId: "",
            },
            views: {
                vendor_body: {
                    template: "<downgrade-vendorpedia-preview class='height-100'></downgrade-vendorpedia-preview>",
                },
            },
        })
        .state("zen.app.pia.module.vendor.inventory.list", {
            url: "/:Id?size&page",
            template: "<inventory-list-wrapper></inventory-list-wrapper>",
            params: { time: null, Filters: {}, Records: [] },
            resolve: {
                VendorInventoryId: [
                    "$stateParams", "Permissions",
                    ($stateParams: ng.ui.IStateParamsService, permissions: Permissions): boolean | void => {
                        const inventoryId: number = toInteger($stateParams.Id);
                        if (inventoryId === InventoryTableIds.Vendors) {
                            return true;
                        } else {
                            permissions.goToFallback();
                            return false;
                        }
                    },
                ],
            },
        })
        .state("zen.app.pia.module.vendor.inventory.details", {
            url: "/:inventoryId/:recordId/:tabId",
            template: "<inventory-details></inventory-details>",
            params: {
                time: null,
                tabId: { dynamic: true, value: InventoryDetailsTabs.Details },
            },
            resolve: {
                VendorInventoryId: [
                    "$stateParams", "Permissions",
                    ($stateParams: ng.ui.IStateParamsService, permissions: Permissions): boolean | void => {
                        const inventoryId: number = toInteger($stateParams.inventoryId);
                        if (inventoryId === InventoryTableIds.Vendors) {
                            return true;
                        } else {
                            permissions.goToFallback();
                            return false;
                        }
                    },
                ],
            },
        })
        .state("zen.app.pia.module.vendor.inventory.assessments", {
            url: "/:inventoryId/:recordId/:tabId",
            template: "<inventory-details></inventory-details>",
            params: {
                time: null,
                tabId: { dynamic: true, value: InventoryDetailsTabs.Assessments },
            },
            resolve: {
                VendorInventoryId: [
                    "$stateParams", "Permissions",
                    ($stateParams: ng.ui.IStateParamsService, permissions: Permissions): boolean | void => {
                        const inventoryId: number = toInteger($stateParams.inventoryId);
                        if (inventoryId === InventoryTableIds.Vendors) {
                            return true;
                        } else {
                            permissions.goToFallback();
                            return false;
                        }
                    },
                ],
            },
        })
        .state("zen.app.pia.module.vendor.inventory.documents", {
            url: "/:inventoryId/:recordId/:tabId",
            template: "<inventory-details></inventory-details>",
            params: {
                time: null,
                tabId: { dynamic: true, value: InventoryDetailsTabs.Documents },
            },
            resolve: {
                VendorInventoryId: [
                    "$stateParams", "Permissions",
                    ($stateParams: ng.ui.IStateParamsService, permissions: Permissions): boolean | void => {
                        const inventoryId: number = toInteger($stateParams.inventoryId);
                        if (inventoryId === InventoryTableIds.Vendors && permissions.canShow(InventoryPermissions[inventoryId].vendorDocumentTab)) {
                            return true;
                        } else {
                            permissions.goToFallback();
                            return false;
                        }
                    },
                ],
            },
        })
        .state("zen.app.pia.module.vendor.inventory.risk-details", {
            url: "/details/:inventoryId/:recordId/:inventoryName/risk/:riskId/:tabId",
            params: {
                time: null,
            },
            template: "<downgrade-inventory-risk-details></downgrade-inventory-risk-details>",
            resolve: {
                DataMappingPermission: [
                    "Permissions",
                    "$stateParams",
                    (
                        permissions: Permissions,
                        $stateParams: ng.ui.IStateParamsService,
                    ): boolean => {
                        const inventoryId: number = toInteger($stateParams.inventoryId);
                        if (inventoryId === InventoryTableIds.Vendors && permissions.canShow(InventoryPermissions[inventoryId].details)) {
                            return true;
                        } else {
                            permissions.goToFallback();
                            return false;
                        }
                    },
                ],
            },
        })
        .state("zen.app.pia.module.vendor.gallery", {
            url: "gallery",
            resolve: {
                VendorTemplatesPermission: [
                    "VendorPermission",
                    "$state",
                    "Permissions",
                    (
                        VendorPermission: boolean,
                        $state: ng.ui.IStateService,
                        permissions: Permissions,
                    ): boolean => {
                        if (permissions.canShow("VendorRiskManagement")) {
                            return true;
                        } else {
                            permissions.goToFallback();
                            return false;
                        }
                    },
                ],
            },
            views: {
                vendor_body: {
                    template: "<downgrade-gallery-templates-component></downgrade-gallery-templates-component>",
                },
            },
        })
        .state("zen.app.pia.module.vendor.bulk-edit", {
            url: "bulk-edit/:schemaId",
            params: {
                recordIds: [],
                returnRoute: "",
            },
            resolve: {
                VendorInventoryId: [
                    "$stateParams", "Permissions",
                    ($stateParams: ng.ui.IStateParamsService, permissions: Permissions): boolean | void => {
                        if (
                            $stateParams.schemaId === InventorySchemaIds.Vendors
                            && permissions.canShow(InventoryPermissions[$stateParams.schemaId].bulkEdit)
                            && $stateParams.recordIds && $stateParams.recordIds.length
                            && $stateParams.returnRoute
                        ) {
                            return true;
                        } else {
                            permissions.goToFallback("zen.app.pia.module.vendor.inventory.list", { Id: LegacyInventorySchemaIds[$stateParams.schemaId] });
                            return false;
                        }
                    },
                ],
            },
            views: {
                vendor_body: {
                    template: "<inventory-bulk-edit></inventory-bulk-edit>",
                },
            },
        })
        .state("zen.app.pia.module.vendor.contract", {
            url: "contract/:recordId",
            views: {
                vendor_body: {
                    template: "<downgrade-vendor-upload-document></downgrade-vendor-upload-document>",
                },
            },
        })
        .state("zen.app.pia.module.vendor.add-contract", {
            url: "add-contract",
            views: {
                vendor_body: {
                    template: "<downgrade-add-contract></downgrade-add-contract>",
                },
            },
        })
        .state("zen.app.pia.module.vendor.contract-detail", {
            url: ":recordId/contract-detail/:contractId",
            views: {
                vendor_body: {
                    template: "<downgrade-contract-details></downgrade-contract-details>",
                },
            },
        })
        .state("zen.app.pia.module.vendor.vendor-related", {
            url: ":recordId/vendor-related/:contractId",
            views: {
                vendor_body: {
                    template: "<downgrade-contract-related-list-component></downgrade-contract-related-list-component>",
                },
            },
        })
        .state("zen.app.pia.module.vendor.select-services", {
            url: "select-services/:vendorId",
            params: {
                selectedServices: null,
                vendorId: null,
            },
            resolve: {
                VendorInventoryId: [
                    "$stateParams", "Permissions",
                    ($stateParams: ng.ui.IStateParamsService, permissions: Permissions): boolean | void => {
                        if ($stateParams.vendorId) {
                            return true;
                        } else {
                            permissions.goToFallback("zen.app.pia.module.vendor.vendorpedia-exchange");
                            return false;
                        }
                    },
                ],
            },
            views: {
                vendor_body: {
                    template: "<downgrade-vendor-select-services></downgrade-vendor-select-services>",
                },
            },
        })
        .state("zen.app.pia.module.vendor.information", {
            url: "information/:vendorId",
            params: {
                selectedServices: null,
                vendorId: null,
            },
            resolve: {
                VendorInventoryId: [
                    "$stateParams", "Permissions",
                    ($stateParams: ng.ui.IStateParamsService, permissions: Permissions): boolean | void => {
                        if ($stateParams.vendorId && $stateParams.selectedServices) {
                            return true;
                        } else {
                            permissions.goToFallback("zen.app.pia.module.vendor.vendorpedia-exchange");
                            return false;
                        }
                    },
                ],
            },
            views: {
                vendor_body: {
                    template: "<downgrade-vendor-information></downgrade-vendor-information>",
                },
            },
        })
        .state("zen.app.pia.module.vendor.control-details", {
            url: "vendor-controls/:controlId/:editMode",
            params: {
                controlId: null,
            },
            views: {
                vendor_body: {
                    template: "<downgrade-vendor-controls-list-details></downgrade-vendor-controls-list-details>",
                },
            },
        })
        .state("zen.app.pia.module.vendor.rules", {
            url: "rules",
            params: {
                controlId: null,
            },
            views: {
                vendor_body: {
                    template: "<downgrade-vendor-rules></downgrade-vendor-rules>",
                },
            },
        })
        .state("zen.app.pia.module.vendor.contract-list", {
            url: "contracts",
            views: {
                vendor_body: {
                    template: "<downgrade-contract-list></downgrade-contract-list>",
                },
            },
        });

}

routes.$inject = ["$stateProvider"];
