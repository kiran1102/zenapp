// Services
import { StateService } from "@uirouter/core";
import { Permissions } from "modules/shared/services/helper/permissions.service";
import { GlobalSidebarService } from "modules/global-sidebar/services/one-global-sidebar.service";
import { TenantTranslationService } from "sharedServices/tenant-translation.service";
import { Wootric } from "sharedModules/services/provider/wootric.service";
import { McmModalService } from "modules/mcm/mcm-modal.service";

// Interfaces
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import { INavMenuItem } from "modules/global-sidebar/interfaces/nav-menu-item.interface";

// Constants / Enums
import { InventoryTableIds } from "enums/inventory.enum";
import { InventorySchemaIds } from "constants/inventory-config.constant";
import { TranslationModules } from "constants/translation.constant";

// Constants
import { WootricModuleMap } from "modules/shared/constants/wootric.constants";

class VendorWrapperController implements ng.IComponentController {
    static $inject: string[] = [
        "$rootScope",
        "Permissions",
        "GlobalSidebarService",
        "TenantTranslationService",
        "Wootric",
        "McmModalService",
    ];

    constructor(
        private $rootScope: IExtendedRootScopeService,
        private permissions: Permissions,
        private globalSidebarService: GlobalSidebarService,
        private tenantTranslationService: TenantTranslationService,
        private wootric: Wootric,
        private mcmModalService: McmModalService,
    ) { }

    $onInit() {
        if (!this.tenantTranslationService.loadedModuleTranslations[TranslationModules.Inventory]) {
            this.getInventoryTranslations().then(() => {
                this.setSidebarItems();
            });
        } else {
            this.setSidebarItems();
        }
        this.wootric.run(WootricModuleMap.VendorManagement);
    }

    private setSidebarItems() {
        this.globalSidebarService.set(
            this.getMenuRoutes(),
            this.$rootScope.t("VendorRiskManagement"),
        );
    }

    private getInventoryTranslations = (): ng.IPromise<boolean> => {
        return this.tenantTranslationService.addModuleTranslations(TranslationModules.Inventory);
    }

    private getMenuRoutes(): INavMenuItem[] {
        const menuGroup: INavMenuItem[] = [];

        if (this.permissions.canShow("VendorRiskManagement")) {

            if (this.permissions.canShow("VRMDashboard")) {
                menuGroup.push({
                    id: "VendorDashboardSidebarItem",
                    title: this.$rootScope.t("Dashboard"),
                    route: "zen.app.pia.module.vendor.dashboard",
                    activeRouteFn: (stateService: StateService) => {
                        return stateService.includes("zen.app.pia.module.vendor.dashboard");
                    },
                    icon: "fa fa-tachometer",
                });
            }

            menuGroup.push(
                {
                    id: "VendorsSidebarItem",
                    title: this.$rootScope.t("Vendors"),
                    route: "zen.app.pia.module.vendor.inventory.list",
                    params: { Id: InventoryTableIds.Vendors },
                    activeRouteFn: (stateService: StateService) => {
                        return stateService.includes("zen.app.pia.module.vendor.inventory")
                        || stateService.includes("zen.app.pia.module.vendor.bulk-edit")
                        || stateService.includes("zen.app.pia.module.vendor.inventory.details")
                        || stateService.includes("zen.app.pia.module.vendor.control-details")
                        || stateService.includes("zen.app.pia.module.vendor.contract")
                        || stateService.includes("zen.app.pia.module.vendor.contract-detail");
                    },
                    icon: "ot ot-vendor",
                },
            );

            if (this.permissions.canShow("VRMEngagements")) {
                menuGroup.push({
                    id: "VendorEngagementsSidebarItem",
                    title: this.$rootScope.t("Engagements"),
                    route: "zen.app.pia.module.vendor.engagements",
                    activeRouteFn: (stateService: StateService) => {
                        return stateService.includes("zen.app.pia.module.vendor.engagements")
                            || stateService.includes("zen.app.pia.module.vendor.add-engagement-wizard")
                            || stateService.includes("zen.app.pia.module.vendor.engagement-detail");
                    },
                    icon: "ot ot-engage",
                });
            }

            if (this.permissions.canShow("VRMContractsListView")) {
                menuGroup.push({
                    id: "ContractsSidebarItem",
                    title: this.$rootScope.t("Contracts"),
                    route: "zen.app.pia.module.vendor.contract-list",
                    activeRouteFn: (stateService: StateService) => {
                        return stateService.includes("zen.app.pia.module.vendor.contract-list")
                            || stateService.includes("zen.app.pia.module.vendor.add-contract");
                    },
                    icon: "ot ot-list",
                });
            }

            if (this.permissions.canShow("Vendorpedia")) {
                const vendorpediaChildren: INavMenuItem[] = [];

                // Vendorpedia Exchange
                vendorpediaChildren.push({
                    id: "VendorpediaExchangeSidebarItem",
                    title: this.$rootScope.t("Exchange"),
                    route: "zen.app.pia.module.vendor.vendorpedia-exchange",
                    activeRouteFn: (stateService: StateService) => {
                        return stateService.includes("zen.app.pia.module.vendor.vendorpedia-exchange")
                            || stateService.includes("zen.app.pia.module.vendor.vendorpedia-preview");
                    },
                });

                // Vendorpedia Request
                if (this.permissions.canShow("VPExchange")) {
                    vendorpediaChildren.push({
                        id: "VendorpediaRequestSidebarItem",
                        title: this.$rootScope.t("Requests"),
                        route: "zen.app.pia.module.vendor.vendorpedia-request",
                        activeRouteFn: (stateService: StateService) => {
                            return stateService.includes("zen.app.pia.module.vendor.vendorpedia-request")
                                || stateService.includes("zen.app.pia.module.vendor.vendorpedia-request-new");
                        },
                    });
                }

                menuGroup.push({
                    id: "VendorpediaSidebarItem",
                    title: this.$rootScope.t("Vendorpedia"),
                    children: vendorpediaChildren,
                    open: true,
                    icon: "ot ot-vendorpedia",
                });
            }

            if (this.permissions.canShow("VRMRules")) {
                menuGroup.push({
                    id: "VendorRulesSidebarItem",
                    title: this.$rootScope.t("AutomationRules"),
                    route: "zen.app.pia.module.vendor.rules",
                    activeRouteFn: (stateService: StateService) => {
                        return stateService.includes("zen.app.pia.module.vendor.rules");
                    },
                    icon: "ot ot-workflow",
                });
            }

            const setupChildren: INavMenuItem[] = [];

            setupChildren.push({
                id: "TemplatesSidebarItem",
                title: this.$rootScope.t("Templates"),
                route: "zen.app.pia.module.vendor.templates",
                activeRouteFn: (stateService: StateService) => {
                    return stateService.includes("zen.app.pia.module.vendor.gallery")
                        || stateService.includes("zen.app.pia.module.vendor.templates");
                },
            });

            if (this.permissions.canShow("VendorAttributes")) {
                setupChildren.push({
                    id: "AttributeManagerSidebarItem",
                    title: this.$rootScope.t("AttributeManager"),
                    route: "zen.app.pia.module.vendor.attribute-manager",
                    params: { id: InventorySchemaIds.Vendors },
                    activeRouteFn: (stateService: StateService) => {
                        return stateService.includes("zen.app.pia.module.vendor.attribute-manager")
                            || stateService.includes("zen.app.pia.module.vendor.attribute-manager-details");
                    },
                });
            }

            if (this.permissions.canShow("ControlsLibrary")) {
                setupChildren.push({
                    id: "SettingsControlsLibraryMenuItem",
                    title: this.$rootScope.t("ControlsLibrary"),
                    route: "zen.app.pia.module.risks.views.controls.library",
                    activeRoute: "zen.app.pia.module.risks.views.controls.library",
                });
            } else if (this.permissions.canShow("ControlsLibraryLock")) {
                setupChildren.push({
                    id: "SettingsControlsLockibraryMenuItem",
                    title: this.$rootScope.t("ControlsLibrary"),
                    upgrade: true,
                    action: (): void => this.mcmModalService.openMcmModal("ControlsLibraryLock"),
                });
            }

            if (setupChildren.length) {
                menuGroup.push({
                    id: "DataMappingSetupSideMenuItem",
                    title: this.$rootScope.t("Setup"),
                    children: setupChildren,
                    open: true,
                    icon: "ot ot-wrench",
                });
            }
        }

        return menuGroup;
    }
}

export const VendorWrapperComponent: ng.IComponentOptions = {
    controller: VendorWrapperController,
    template: `
        <div class="flex-full-height height-100 background-grey text-color-default">
            <div class="ib-content row-nowrap width-100-percent height-100 overflow-hidden">
                <section ui-view="vendor_body" class="ib-body width-100-percent padding-all-0 background-grey min-width-1"></section>
            </div>
        </div>
    `,
};
