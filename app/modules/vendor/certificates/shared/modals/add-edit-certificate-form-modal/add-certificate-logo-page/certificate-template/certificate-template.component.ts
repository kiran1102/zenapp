// Angular
import {
    Component,
    Input,
    Output,
    EventEmitter,
} from "@angular/core";

// Interfaces
import {
    ICertificateInformation,
    ICertificateLogoInformation,
} from "modules/vendor/certificates/shared/interfaces/certificates.interface";

// Constants
import { CertificateLogos } from "modules/vendor/certificates/shared/constants/certificates.constants";

@Component({
    selector: "certificate-template",
    templateUrl: "./certificate-template.component.html",
})
export class CertificateTemplateComponent {

    @Input() certificateData: ICertificateInformation;
    @Output() selectedLogo = new EventEmitter<ICertificateInformation>();

    certificates: ICertificateLogoInformation[];
    selectedCertificateLogo: ICertificateLogoInformation;

    ngOnInit() {
        this.certificates = CertificateLogos;
        if (this.certificateData) {
            this.selectedCertificateLogo = { logoName: this.certificateData.logoName, logoUrl: this.certificateData.logoUrl };
            this.selectedLogo.emit(this.certificateData);
        }
    }

    selectLogo(certificate: ICertificateLogoInformation) {
        this.selectedCertificateLogo = { ...certificate };
        const certificateData = { ...this.certificateData, ...certificate };
        this.selectedLogo.emit(certificateData);
    }

    trackByFn(index: number): number {
        return index;
    }
}
