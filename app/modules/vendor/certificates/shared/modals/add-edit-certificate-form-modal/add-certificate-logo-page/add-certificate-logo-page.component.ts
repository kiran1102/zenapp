// Angular
import {
    Component,
    OnInit,
    Input,
    Output,
    EventEmitter,
} from "@angular/core";

// Enums
import { AddCertificateTabOptions } from "modules/vendor/certificates/shared/enums/certificates.enums";

// Interfaces
import { ITabsNav } from "interfaces/one-tabs-nav.interface";
import { ICertificateInformation } from "modules/vendor/certificates/shared/interfaces/certificates.interface";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

@Component({
    selector: "add-certificate-logo-page",
    templateUrl: "./add-certificate-logo-page.component.html",
})
export class AddCertificateLogoPageComponent implements OnInit {

    @Input() certificateData: ICertificateInformation;
    @Output() selectedLogo = new EventEmitter();

    selectedTab = AddCertificateTabOptions.Logos;
    addCertificateTabOptions = AddCertificateTabOptions;
    tabOptions: ITabsNav[] = [];
    showTabs: false; // remove when 'upload your own' tab is implemented

    constructor(
        private translatePipe: TranslatePipe,
    ) { }

    ngOnInit() {
        this.setTabOptions();
    }

    onTabClick(option: ITabsNav) {
        this.selectedTab = option.id as AddCertificateTabOptions;
    }

    setTabOptions() {
        if (this.showTabs) {
            this.tabOptions.push({
                text: this.translatePipe.transform("Logos"),
                id: AddCertificateTabOptions.Logos,
                otAutoId: "AddCertificateTabLogos",
            });
        }
    }

    selectLogo(event: ICertificateInformation) {
        this.selectedLogo.emit(event);
    }
}
