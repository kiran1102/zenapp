// Core
import {
    Component,
    OnInit,
} from "@angular/core";

// Forms
import {
    FormGroup,
    FormBuilder,
    Validators,
} from "@angular/forms";

// Services
import { CertificatesApiService } from "modules/vendor/certificates/shared/services/certificates-api.service";
import { TimeStamp } from "oneServices/time-stamp.service";
import { NotificationService } from "modules/shared/services/provider/notification.service";

// Interface
import { ILabelValue } from "interfaces/generic.interface";
import { ICertificateInformation } from "modules/vendor/certificates/shared/interfaces/certificates.interface";
import { IProtocolPacket } from "interfaces/protocol.interface";

// Vitreus
import {
    IOtModalContent,
    IDateTimeOutputModel,
} from "@onetrust/vitreus";
import { IMyDate } from "mydaterangepicker";

// RXJS
import { Subject } from "rxjs";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

// Constants
import {
    CertificateErrorCodes,
    defaultLogo,
} from "modules/vendor/certificates/shared/constants/certificates.constants";

@Component({
    selector: "add-edit-certificate-form-modal",
    templateUrl: "./add-edit-certificate-form-modal.component.html",
    styles: [`footer { flex: 1 1 auto; }`],
})

export class AddEditCertificateFormModalComponent implements IOtModalContent, OnInit {

    otModalCloseEvent: Subject<boolean>;
    form: FormGroup;
    dateFormat = this.timeStamp.getDatePickerDateFormat();
    certificateDateValueFormat: string;
    expirationDateValueFormat: string;
    dateDisableUntil: IMyDate = { year: 0, month: 0, day: 0 };
    dateDisableSince: IMyDate = { year: 0, month: 0, day: 0 };
    statusList: Array<ILabelValue<string>>;
    certificateDate = null;
    expirationDate = null;
    showCertForm = true;
    otModalData: any;
    editMode = false;
    loadingModalData = false;
    certificateData: ICertificateInformation;
    selectedLogo: ICertificateInformation;
    isAddedOrEdited: boolean;
    isCertDateHandled = false;
    isExpDateHandled = false;
    contentModel: string;

    constructor(
        private formBuilder: FormBuilder,
        private timeStamp: TimeStamp,
        private certificatesApiService: CertificatesApiService,
        private translatePipe: TranslatePipe,
        private notificationService: NotificationService,
    ) { }

    get certificateName() { return this.form.get("certificateName"); }
    get certificateURL() { return this.form.get("certificateURL"); }
    get sourceURL() { return this.form.get("sourceURL"); }
    get verificationStatus() { return this.form.get("verificationStatus"); }
    get certDateModel() { return this.form.get("certDateModel"); }
    get expDateModel() { return this.form.get("expDateModel"); }
    get scopeStatement() {return this.form.get("scopeStatement"); }

    ngOnInit() {
        this.loadingModalData = true;
        this.editMode = this.otModalData.isEditMode;
        this.fetchCertificateStatus();
    }

    closeModal() {
        this.otModalCloseEvent.next();
    }

    selectDate(dateType: string, selectedDate: IDateTimeOutputModel): void {
        switch (dateType) {
            case "CertificationDate":
                this.certificateDateHandler(selectedDate);
                break;
            case "ExpirationDate":
                this.expirationDateHandler(selectedDate);
                break;
            default:
                break;
        }
    }

    cancelAddCertificate() {
        this.closeModal();
    }

    selectStatus(event: ILabelValue<string>) {
        this.verificationStatus.setValue(event);
    }

    selectLogo(certificateData: ICertificateInformation) {
        this.selectedLogo = certificateData;
    }

    addCertificate() {
        if (this.form.get("certificateName").value && this.form.get("certificateName").value.trim().length === 0 || this.form.invalid || this.isAddedOrEdited) return;
        if (this.showCertForm) {
            this.showCertForm = false;
        } else {
            this.isAddedOrEdited = true;
            const formData = this.form.value;
            const addCertificatePayload: ICertificateInformation = {
                certificateName: formData.certificateName ? formData.certificateName.trim() : null,
                certificateUrl: formData.certificateURL ? formData.certificateURL.trim() : null,
                certificationDate: this.certificateDateValueFormat ? this.certificateDateValueFormat : null,
                certificationVerificationLevel: formData.verificationStatus.value,
                expirationDate: this.expirationDateValueFormat ? this.expirationDateValueFormat : null,
                sourceUrl: formData.sourceURL ? formData.sourceURL.trim() : null,
                logoName: this.selectedLogo ? this.selectedLogo.logoName : defaultLogo.defaultLogoName,
                logoUrl: this.selectedLogo ? this.selectedLogo.logoUrl : defaultLogo.defaultLogoURL,
                scopeStatement: formData.scopeStatement ? formData.scopeStatement.trim() : null,
            };
            this.certificatesApiService.addCertificate(this.otModalData.recordType, this.otModalData.recordId, addCertificatePayload)
                .subscribe((res) => {
                    if (res.result) {
                        this.otModalData.handleModalAddCert$.next(true);
                        this.closeModal();
                    } else {
                        this.checkForErrorScenariosReqAssessment(res, "Adding");
                    }
                    this.isAddedOrEdited = false;
                });
        }
    }

    editCertificate() {
        if (this.form.invalid || this.isAddedOrEdited) return;
        if (this.showCertForm) {
            this.showCertForm = false;
        } else {
            this.isAddedOrEdited = true;
            const formData = this.form.value;
            const certificateId = this.otModalData.certificateDetails.id;
            const logoName = this.selectedLogo.logoName || this.otModalData.certificateDetails.logoName;
            const logoUrl = this.selectedLogo.logoUrl || this.otModalData.certificateDetails.logoUrl;
            const addCertificatePayload: ICertificateInformation = {
                certificateName: formData.certificateName ? formData.certificateName.trim() : null,
                certificateUrl: formData.certificateURL ? formData.certificateURL.trim() : null,
                certificationDate: this.certificateDateValueFormat ? this.certificateDateValueFormat : this.convertDateToSyncBE("CertDate", this.otModalData.certificateDetails.certificationDate),
                certificationVerificationLevel: formData.verificationStatus.value,
                expirationDate: this.expirationDateValueFormat ? this.expirationDateValueFormat : this.convertDateToSyncBE("ExpDate", this.otModalData.certificateDetails.expirationDate),
                sourceUrl: formData.sourceURL ? formData.sourceURL.trim() : null,
                scopeStatement: formData.scopeStatement ? formData.scopeStatement.trim() : null,
                logoName,
                logoUrl,
            };
            this.certificatesApiService.updateCertificate(this.otModalData.recordType, certificateId, addCertificatePayload)
                .subscribe((res) => {
                    if (res.result) {
                        this.otModalData.handleModalAddCert$.next(true);
                        this.closeModal();
                    } else {
                        this.checkForErrorScenariosReqAssessment(res, "Updating");
                    }
                    this.isAddedOrEdited = false;
                });
        }
    }

    convertDateToSyncBE(dateField: string , date: string) {
        if (dateField === "CertDate") {
            if (!date || this.isCertDateHandled) return ;
            return date;
        }

        if (dateField === "ExpDate") {
            if (!date || this.isExpDateHandled) return;
            return date;
        }
    }

    goToPrevious() {
        this.showCertForm = true;
        if (this.isCertDateHandled) this.certDateModel.setValue(this.certificateDate);
        if (this.isExpDateHandled) this.expDateModel.setValue(this.expirationDate);
    }

    scopeStatementHandler(data: string): void {
        this.contentModel = data;
    }

    private certificateDateHandler(selectedDate: IDateTimeOutputModel) {
        this.isCertDateHandled = true;
        if (selectedDate.dateTime && selectedDate.date) {
            this.certificateDateValueFormat = this.getDate(selectedDate);
            this.certificateDate = selectedDate.jsdate;
            this.dateDisableUntil = this.timeStamp.getDisableUntilDate(this.certificateDate);
            this.dateDisableUntil.day++;
        } else {
            this.certificateDateValueFormat = null;
            this.dateDisableUntil = { year: 0, month: 0, day: 0 };
            this.certificateDate = null;
        }
    }

    private expirationDateHandler(selectedDate: IDateTimeOutputModel) {
        this.isExpDateHandled = true;
        if (selectedDate.dateTime && selectedDate.date) {
            this.expirationDateValueFormat = this.getDate(selectedDate);
            this.expirationDate = selectedDate.jsdate;
            this.dateDisableSince = this.timeStamp.getDisableSinceDate(this.expirationDate);
        } else {
            this.expirationDateValueFormat = null;
            this.dateDisableSince = { year: 0, month: 0, day: 0 };
            this.expirationDate = null;
        }
    }

    private getDate(selectedDate: IDateTimeOutputModel): string {
        return this.timeStamp.formatDateToIso({ day: selectedDate.jsdate.getDate(), month: selectedDate.jsdate.getMonth() + 1, year: selectedDate.jsdate.getFullYear() });
    }

    private fetchCertificateStatus() {
        this.certificatesApiService.getCertificateStatus(this.otModalData.recordType).subscribe((res) => {
            if (res.result) {
                this.statusList = res.data.map((item: string) => {
                    return { label: this.translatePipe.transform(item), value: item };
                });
            }
            this.loadingModalData = false;
            this.certificateModalFormGroup();
            if (this.otModalData && this.otModalData.isEditMode && this.otModalData.certificateDetails) {
                this.certificateData = { ...this.otModalData.certificateDetails };
                this.prepopulateOnEditMode();
            }
        });
    }

    private certificateModalFormGroup() {
        this.form = this.formBuilder.group({
            certificateName: [null, [Validators.required, Validators.maxLength(100)]],
            certificateURL: [null, [Validators.maxLength(255)]],
            sourceURL: [null, [Validators.maxLength(255)]],
            verificationStatus: [this.statusList && this.statusList.length > 0 ? this.statusList[0] : null, [Validators.required]],
            certDateModel: [null],
            expDateModel: [null],
            scopeStatement: [null, [Validators.maxLength(2000)]],
        });
    }

    private prepopulateOnEditMode() {
        const verificationStatusValue = this.certificateData.certificationVerificationLevel;
        this.editMode = true;
        this.certificateName.setValue(this.certificateData.certificateName);
        this.certificateURL.setValue(this.certificateData.certificateUrl);
        this.sourceURL.setValue(this.certificateData.sourceUrl);
        this.scopeStatement.setValue(this.certificateData.scopeStatement);
        this.verificationStatus.setValue({ label: this.translatePipe.transform(verificationStatusValue), value: verificationStatusValue });
        this.certDateModel.setValue(this.certificateData.certificationDate ? this.timeStamp.formatDateWithoutTime(this.certificateData.certificationDate) : null);
        this.expDateModel.setValue(this.certificateData.expirationDate ? this.timeStamp.formatDateWithoutTime(this.certificateData.expirationDate) : null);
        if (this.expDateModel.value) this.dateDisableSince = this.timeStamp.getDisableSinceDate(this.expDateModel.value);
        if (this.certDateModel.value) {
            this.dateDisableUntil = this.timeStamp.getDisableUntilDate(this.certDateModel.value);
            this.dateDisableUntil.day++;
        }
    }

    private checkForErrorScenariosReqAssessment(res: IProtocolPacket, action: string ) {
        let errorMsg = this.translatePipe.transform("ErrorInCertificate", {formType: action});
        if ((res.errorCode || res.errors.code) && !errorMsg) {
            errorMsg = this.translatePipe.transform(CertificateErrorCodes[res.errorCode])
                || this.translatePipe.transform(CertificateErrorCodes[res.errors.code]);
        }
        this.notificationService.alertError(this.translatePipe.transform("Error"), errorMsg);
    }
}
