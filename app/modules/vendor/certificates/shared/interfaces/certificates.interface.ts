export interface ICertificateResponse {
    data: ICertificateInformation[];
}

export interface ICertificateInformation {
    certificateName: string;
    certificateUrl: string;
    certificationDate: string;
    expirationDate: string;
    sourceUrl: string;
    certificationVerificationLevel: string;
    scopeStatement: string;
    id?: string;
    logoName?: string;
    logoUrl?: string;
    scopeStatus?: string;
}

export interface ICertificateLogoInformation {
    logoName: string;
    logoUrl: string;
}
