import { CertificateContext } from "modules/vendor/certificates/shared/enums/certificates.enums";
import {
    ICertificateInformation,
    ICertificateLogoInformation,
} from "modules/vendor/certificates/shared/interfaces/certificates.interface";

export const CertificatesApiConfig = {
    [CertificateContext.Vendors]: {
        getCertificates: (recordId: string) => {
            return {
                url: `/api/inventory/v2/inventories/${CertificateContext.Vendors}/${recordId}/certificates`,
            };
        },
        getCertStatus: () => {
            return {
                url: `/api/inventory/v2/certificates/statuses`,
            };
        },
        addCertificate: (recordId: string, addCertificatePayload: ICertificateInformation) => {
            return {
                url: `/api/inventory/v2/inventories/${CertificateContext.Vendors}/${recordId}/certificates`,
                payload: addCertificatePayload,
            };
        },
        updateCertificate: (certificateId: string, addCertificatePayload: ICertificateInformation) => {
            return {
                url: `/api/inventory/v2/certificates/${certificateId}`,
                payload: addCertificatePayload,
            };
        },
        deleteCertificate: (certificateId: string) => {
            return {
                url: `/api/inventory/v2/certificates/${certificateId}`,
            };
        },
    },
    [CertificateContext.Assets]: {
        getCertificates: (recordId: string) => {
            return {
                url: `/api/inventory/v2/inventories/${CertificateContext.Assets}/${recordId}/certificates`,
            };
        },
        getCertStatus: () => {
            return {
                url: `/api/inventory/v2/certificates/statuses`,
            };
        },
        addCertificate: (recordId: string, addCertificatePayload: ICertificateInformation) => {
            return {
                url: `/api/inventory/v2/inventories/${CertificateContext.Assets}/${recordId}/certificates`,
                payload: addCertificatePayload,
            };
        },
        updateCertificate: (certificateId: string, addCertificatePayload: ICertificateInformation) => {
            return {
                url: `/api/inventory/v2/certificates/${certificateId}`,
                payload: addCertificatePayload,
            };
        },
        deleteCertificate: (certificateId: string) => {
            return {
                url: `/api/inventory/v2/certificates/${certificateId}`,
            };
        },
    },
};

export const CertificatePermissions = {
    [CertificateContext.Vendors]: {
        addCert: "AddVendorCert",
        editCert: "EditVendorCert",
        deleteCert: "DeleteVendorCert",
    },
    [CertificateContext.Assets]: {
        addCert: "AddAssetCert",
        editCert: "EditAssetCert",
        deleteCert: "DeleteAssetCert",
    },
};

export const IMAGE_PATH = `images/vendor/certificates/`;

// Remove once BE provides images
export const CertificateLogoNames = [
    "FrameworkDefault",
    "Adobe",
    "AICPA SOC1",
    "AICPA SOC 2",
    "AICPA SOC 3",
    "Cobit",
    "CSA CCM",
    "CSA STAR",
    "CyberEssential",
    "DoD IL2",
    "DoD IL4",
    "FedRAMP",
    "FERPA",
    "FIPS 140-2",
    "GLBA",
    "HIPAA",
    "HITECH",
    "HiTrust",
    "ISO/IEC 27000",
    "ISO/IEC 27001",
    "ISO/IEC 27007",
    "ISO/IEC 27017",
    "ISO/IEC 27018",
    "ISO/IEC 27552",
    "ISO/IEC 29001",
    "ISO/IEC 29100",
    "ISO 9001",
    "NIST 800-171",
    "NIST SP 800-53 rev4",
    "NIST_800-53",
    "NIST_CSFv1.1",
    "PCI DSS",
    "PCI",
    "Privacy Shield",
    "PrivacyMark",
    "Shared Assessment",
];

export const GetLogoNameForCert = (certName: string) => {
    switch (certName) {
        case "CSA STAR Assessment":
        case "CSA STAR Attestation":
        case "CSA STAR Certification":
        case "CSA STAR Continuous Monitoring":
            return "CSA STAR";
        case "Cyber Essentials Plus":
        case "UK Cyber Essentials":
            return "CyberEssential";
        case "FedRAMP High":
        case "FedRAMP High (GovCloud)":
        case "FedRAMP Li SaaS":
        case "FedRAMP Low":
        case "FedRAMP Moderate":
            return "FedRAMP";
        case "HIPPA":
        case "HIPAA ":
        case "HIPPA":
            return "HIPAA";
        case "HITRUST":
            return "HiTrust";
        case "ISO 9001:2015":
        case "ISO/IEC 27001:2013":
        case "ISO/IEC 27017:2015":
        case "ISO/IEC 27018:2014":
            return certName.split(":")[0];
        case "NIST 800-171":
        case "NIST SP 800-171":
        case "NIST 800-171/DFARS 252.7012":
            return "NIST 800-171";
        case "NIST 800-53":
            return "NIST_800-53";
        case "NIST CSF":
            return "NIST_CSFv1.1";
        case "PCI ":
            return "PCI";
        case "PCI Level 4 Merchant":
            return "PCI";
        case "Shared Assessments":
            return "Shared Assessment";
        case "SOC 1":
            return "AICPA SOC1";
        case "SOC 2":
        case "SOC 2 ":
        case "SOC 2 Type II":
            return "AICPA SOC 2";
        case "SOC 3":
            return "AICPA SOC 3";
        default:
            return certName;
    }
};

export const CertificateLogos: ICertificateLogoInformation[] = CertificateLogoNames.map((image: string) => {
    return {
        logoName: GetLogoNameForCert(image),
        logoUrl: (IMAGE_PATH + GetLogoNameForCert(image) + ".png"),
    };
});

export const AddCertificatesErrorCodes = {
    INVALID_EXPIRATION_DATE: 1710029,
    VENDOR_CERTIFICATE_ALREADY_EXIST: 1710030,
    ASSET_CERTIFICATE_ALREADY_EXIST: "already-exists",
};

export const CertificateErrorCodes = {
    [AddCertificatesErrorCodes.INVALID_EXPIRATION_DATE]: "Certificate.Error.InvalidExpDate",
    [AddCertificatesErrorCodes.VENDOR_CERTIFICATE_ALREADY_EXIST]: "Certificate.Error.DuplicateCertificate",
    [AddCertificatesErrorCodes.ASSET_CERTIFICATE_ALREADY_EXIST]: "Certificate.Error.DuplicateCertificate",
};

export const defaultLogo = {
    defaultLogoName : "FrameworkDefault",
    defaultLogoURL : `images/vendor/certificates/FrameworkDefault.png`,
};
