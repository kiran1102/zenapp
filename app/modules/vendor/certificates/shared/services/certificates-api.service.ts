// Core
import { Injectable } from "@angular/core";

// RxJs
import {
    Observable,
    from,
} from "rxjs";

// Interfaces
import {
    IProtocolResponse,
    IProtocolConfig,
    IProtocolMessages,
    IProtocolPacket,
} from "interfaces/protocol.interface";
import {
    ICertificateResponse,
    ICertificateInformation,
} from "modules/vendor/certificates/shared/interfaces/certificates.interface";

// Constants
import { CertificatesApiConfig } from "modules/vendor/certificates/shared/constants/certificates.constants";

// Services
import { ProtocolService } from "modules/core/services/protocol.service";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

@Injectable()
export class CertificatesApiService {

    constructor(
        private protocolService: ProtocolService,
        private translatePipe: TranslatePipe,
    ) { }

    getCertificates(recordType: string, recordId: string): Observable<IProtocolResponse<ICertificateResponse>> {
        const _config = CertificatesApiConfig[recordType].getCertificates(recordId);
        const config: IProtocolConfig = this.protocolService.customConfig("GET", _config.url);
        const messages: IProtocolMessages = { Error: { Hide: true } };
        return from(this.protocolService.http(config, messages));
    }

    getCertificateStatus(recordType: string): Observable<IProtocolResponse<string[]>> {
        const _config = CertificatesApiConfig[recordType].getCertStatus();
        const config: IProtocolConfig = this.protocolService.customConfig("GET", _config.url);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("ErrorWhileFetchingCertificationStatus") } };
        return from(this.protocolService.http(config, messages));
    }

    addCertificate(recordType: string, recordId: string, addCertificatePayload: ICertificateInformation): Observable<IProtocolResponse<IProtocolPacket>> {
        const _config = CertificatesApiConfig[recordType].addCertificate(recordId, addCertificatePayload);
        const config: IProtocolConfig = this.protocolService.customConfig("POST", _config.url, null, _config.payload);
        const messages: IProtocolMessages = {
            Error: { Hide: true },
            Success: { custom: this.translatePipe.transform("CertificateSuccessfullyAdded") },
        };
        return from(this.protocolService.http(config, messages));
    }

    updateCertificate(recordType: string, certificateId: string, addCertificatePayload: ICertificateInformation): Observable<IProtocolResponse<IProtocolPacket>> {
        const _config = CertificatesApiConfig[recordType].updateCertificate(certificateId, addCertificatePayload);
        const config: IProtocolConfig = this.protocolService.customConfig("PUT", _config.url, null, _config.payload);
        const messages: IProtocolMessages = {
            Error: { Hide: true },
            Success: { custom: this.translatePipe.transform("CertificateSuccessfullyUpdated") },
        };
        return from(this.protocolService.http(config, messages));
    }

    deleteCertificate(recordType: string, certificateId: string): Observable<IProtocolResponse<IProtocolPacket>> {
        const _config = CertificatesApiConfig[recordType].deleteCertificate(certificateId);
        const config: IProtocolConfig = this.protocolService.customConfig("DELETE", _config.url);
        const messages: IProtocolMessages = {
            Error: { custom: this.translatePipe.transform("ErrorDeletingCertificate") },
            Success: { custom: this.translatePipe.transform("CertificateSuccessfullyDeleted") },
        };
        return from(this.protocolService.http(config, messages));
    }
}
