// Core
import { Injectable } from "@angular/core";

// RxJs
import { BehaviorSubject } from "rxjs/internal/BehaviorSubject";
import { Observable } from "rxjs/Observable";
import { map } from "rxjs/operators";

// Interfaces
import { ICertificateInformation } from "modules/vendor/certificates/shared/interfaces/certificates.interface";

@Injectable()
export class CertificatesListService {

    certificates$: Observable<ICertificateInformation[] | null>;
    hasNoCertificates$: Observable<boolean>;

    private _certificates = new BehaviorSubject<ICertificateInformation[]>(null);

    constructor() {
        this.certificates$ = this._certificates.asObservable();
        this.hasNoCertificates$ = this.certificates$.pipe(map((certificates) => !certificates || !certificates.length));
    }

    get certificates(): ICertificateInformation[] {
        return this._certificates.getValue();
    }

    setCertificates(certificates: ICertificateInformation[] | null) {
        this._certificates.next(certificates ? certificates : null);
    }
}
