export enum CertificateContext {
    Vendors = "vendors",
    Assets = "assets",
}

export enum AddCertificateTabOptions {
    Logos = "LOGOS",
    UploadYourOwn = "UPLOAD_YOUR_OWN",
}
