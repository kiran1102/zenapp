// Angular
import {
    Component,
    OnInit,
    Input,
} from "@angular/core";

// RxJs
import { Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";

// Interfaces
import { IStringMap } from "interfaces/generic.interface";

// Services
import { CertificatesApiService } from "modules/vendor/certificates/shared/services/certificates-api.service";
import { CertificatesListService } from "modules/vendor/certificates/shared/services/certificates-list.service";

// Constants
import { CertificatePermissions } from "modules/vendor/certificates/shared/constants/certificates.constants";

// Modals
import { AddEditCertificateFormModalComponent } from "modules/vendor/certificates/shared/modals/add-edit-certificate-form-modal/add-edit-certificate-form-modal.component";

// Vitreus
import { OtModalService } from "@onetrust/vitreus";

@Component({
    selector: "certificates-details",
    templateUrl: "./certificates-details.component.html",
})
export class CertificatesDetailsComponent implements OnInit {

    @Input() recordType: string;
    @Input() recordId: string;

    hasNoCertificates$ = this.certificatesListService.hasNoCertificates$;
    isLoading = true;
    certificatePermissions: IStringMap<string>;
    handleModalAddCert = new Subject();
    private destroy$ = new Subject();

    constructor(
        private certificatesApiService: CertificatesApiService,
        private certificatesListService: CertificatesListService,
        private otModalService: OtModalService,
    ) { }

    ngOnInit() {
        this.loadCertificates();
        this.handleModalAddCert.pipe(
            takeUntil(this.destroy$),
        ).subscribe((res: boolean) => {
            if (res) {
                this.isLoading = true;
                this.loadCertificates();
            }
        });
        this.certificatePermissions = CertificatePermissions[this.recordType];
    }

    ngOnDestroy() {
        this.destroy$.next();
        this.destroy$.complete();
    }

    addNewCertificate() {
        this.otModalService
            .create(
                AddEditCertificateFormModalComponent,
                {
                    isComponent: true,
                },
                {
                    recordType: this.recordType,
                    recordId: this.recordId,
                    handleModalAddCert$: this.handleModalAddCert,
                },
            );
    }

    loadCertOnEdit(event: boolean) {
        if (event) {
            this.isLoading = true;
            this.loadCertificates();
        }
    }

    private loadCertificates() {
        this.certificatesApiService.getCertificates(this.recordType, this.recordId)
        .subscribe((certificatesResponse) => {
            if (certificatesResponse.result) {
                this.certificatesListService.setCertificates(certificatesResponse.data.data);
            }
            this.isLoading = false;
        });
    }
}
