// Angular
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import {
    FormsModule,
    ReactiveFormsModule,
} from "@angular/forms";

// Modules
import { VitreusModule } from "@onetrust/vitreus";
import {
    OTPipesModule,
    PIPES,
} from "modules/pipes/pipes.module";
import { SharedModule } from "sharedModules/shared.module";

// Components
import { CertificatesDetailsComponent } from "modules/vendor/certificates/certificates-details.component";
import { CertificatesListComponent } from "modules/vendor/certificates/certificates-list/certificates-list.component";
import { CertificatePanelComponent } from "modules/vendor/certificates/certificates-list/certificate-panel/certificate-panel.component";
import { CertificatePanelDetailsComponent } from "modules/vendor/certificates/certificates-list/certificate-panel-details/certificate-panel-details.component";
import { AddCertificateLogoPageComponent } from "modules/vendor/certificates/shared/modals/add-edit-certificate-form-modal/add-certificate-logo-page/add-certificate-logo-page.component";
import { CertificateTemplateComponent } from "modules/vendor/certificates/shared/modals/add-edit-certificate-form-modal/add-certificate-logo-page/certificate-template/certificate-template.component";

// Services
import { CertificatesApiService } from "modules/vendor/certificates/shared/services/certificates-api.service";
import { CertificatesListService } from "modules/vendor/certificates/shared/services/certificates-list.service";

// Modals
import { AddEditCertificateFormModalComponent } from "modules/vendor/certificates/shared/modals/add-edit-certificate-form-modal/add-edit-certificate-form-modal.component";

// TODO: Remove when BACK-END provides the icons
// Images
import "images/vendor/certificates/FrameworkDefault.png";
import "images/vendor/certificates/Adobe.png";
import "images/vendor/certificates/AICPA SOC 2.png";
import "images/vendor/certificates/AICPA SOC 3.png";
import "images/vendor/certificates/AICPA SOC1.png";
import "images/vendor/certificates/Cobit.png";
import "images/vendor/certificates/CSA CCM.png";
import "images/vendor/certificates/CSA STAR.png";
import "images/vendor/certificates/CyberEssential.png";
import "images/vendor/certificates/DoD IL2.png";
import "images/vendor/certificates/DoD IL4.png";
import "images/vendor/certificates/FedRAMP.png";
import "images/vendor/certificates/FERPA.png";
import "images/vendor/certificates/FIPS 140-2.png";
import "images/vendor/certificates/GLBA.png";
import "images/vendor/certificates/HIPAA.png";
import "images/vendor/certificates/HITECH.png";
import "images/vendor/certificates/HiTrust.png";
import "images/vendor/certificates/ISO/IEC 27000.png";
import "images/vendor/certificates/ISO/IEC 27001.png";
import "images/vendor/certificates/ISO/IEC 27007.png";
import "images/vendor/certificates/ISO/IEC 27017.png";
import "images/vendor/certificates/ISO/IEC 27018.png";
import "images/vendor/certificates/ISO/IEC 27552.png";
import "images/vendor/certificates/ISO/IEC 29001.png";
import "images/vendor/certificates/ISO/IEC 29100.png";
import "images/vendor/certificates/ISO 9001.png";
import "images/vendor/certificates/NIST 800-171.png";
import "images/vendor/certificates/NIST SP 800-53 rev4.png";
import "images/vendor/certificates/NIST_800-53.png";
import "images/vendor/certificates/NIST_CSFv1.1.png";
import "images/vendor/certificates/PCI DSS.png";
import "images/vendor/certificates/PCI.png";
import "images/vendor/certificates/PrivacyMark.png";
import "images/vendor/certificates/Privacy Shield.png";
import "images/vendor/certificates/Shared Assessment.png";

@NgModule({
    imports: [
        SharedModule,
        CommonModule,
        FormsModule,
        VitreusModule,
        OTPipesModule,
        ReactiveFormsModule,
    ],
    declarations: [
        CertificatesDetailsComponent,
        CertificatesListComponent,
        CertificatePanelComponent,
        CertificatePanelDetailsComponent,
        AddEditCertificateFormModalComponent,
        AddCertificateLogoPageComponent,
        CertificateTemplateComponent,
    ],
    exports: [
        CertificatesDetailsComponent,
        CertificatesListComponent,
        CertificatePanelComponent,
        CertificatePanelDetailsComponent,
        AddEditCertificateFormModalComponent,
        AddCertificateLogoPageComponent,
        CertificateTemplateComponent,
    ],
    entryComponents: [
        AddEditCertificateFormModalComponent,
    ],
    providers: [
        CertificatesApiService,
        CertificatesListService,
        PIPES, // TODO: Remove from providers when ot-common is implemented. Only necessary if you are using pipe in TS file, else just module is fine
    ],
})
export class CertificatesDetailsModule { }
