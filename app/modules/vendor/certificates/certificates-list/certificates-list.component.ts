// Angular
import {
    Component,
    Input,
    Output,
    EventEmitter,
} from "@angular/core";

// Services
import { CertificatesListService } from "modules/vendor/certificates/shared/services/certificates-list.service";

@Component({
    selector: "certificates-list",
    templateUrl: "./certificates-list.component.html",
})
export class CertificatesListComponent {

    @Input() recordId: string;
    @Input() recordType: string;

    @Output() loadCertificatesOnEdit = new EventEmitter<boolean>();

    certificates$ = this.certificatesListService.certificates$;

    constructor(
        private certificatesListService: CertificatesListService,
    ) { }

    trackByFn(index: number) {
        return index;
    }

    loadCertOnEdit(event: boolean) {
        this.loadCertificatesOnEdit.emit(event);
    }
}
