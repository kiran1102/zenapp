// Angular
import {
    Component,
    Input,
} from "@angular/core";

// Interfaces
import { ICertificateInformation } from "modules/vendor/certificates/shared/interfaces/certificates.interface";

@Component({
    selector: "certificate-panel-details",
    templateUrl: "./certificate-panel-details.component.html",
})
export class CertificatePanelDetailsComponent {

    @Input() certificate: ICertificateInformation;

    emptyData = "- - - -";
}
