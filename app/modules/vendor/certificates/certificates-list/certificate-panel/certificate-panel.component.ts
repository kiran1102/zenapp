// Angular
import {
    Component,
    Input,
    OnInit,
    Output,
    EventEmitter,
} from "@angular/core";

// RxJs
import { Subject } from "rxjs";
import {
    takeUntil,
    filter,
    take,
} from "rxjs/operators";

// Interfaces
import { IStringMap } from "interfaces/generic.interface";
import { ICertificateInformation } from "modules/vendor/certificates/shared/interfaces/certificates.interface";

// Services
import { CertificatesApiService } from "modules/vendor/certificates/shared/services/certificates-api.service";

// Constants
import {
    CertificatePermissions,
    GetLogoNameForCert,
} from "modules/vendor/certificates/shared/constants/certificates.constants";

// Vitreus
import {
    OtModalService,
    ConfirmModalType,
} from "@onetrust/vitreus";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

// Modals
import { AddEditCertificateFormModalComponent } from "modules/vendor/certificates/shared/modals/add-edit-certificate-form-modal/add-edit-certificate-form-modal.component";

@Component({
    selector: "certificate-panel",
    templateUrl: "./certificate-panel.component.html",
})
export class CertificatePanelComponent implements OnInit {

    @Input() certificate: ICertificateInformation;
    @Input() recordId: string;
    @Input() recordType: string;

    @Output() loadCertificatesOnEdit = new EventEmitter<boolean>();

    imgSrc: string;
    emptyData = "- - - -";
    certificatePermissions: IStringMap<string>;
    showDetails: boolean;
    isLoading = true;
    handleModalAddCert = new Subject();

    private destroy$ = new Subject();

    constructor(
        private otModalService: OtModalService,
        private translatePipe: TranslatePipe,
        private certificatesApiService: CertificatesApiService,
    ) {}

    ngOnInit() {
        this.imgSrc = `images/vendor/certificates/${GetLogoNameForCert(this.certificate.logoName || this.certificate.certificateName)}.png`;
        this.handleModalAddCert.pipe(
            takeUntil(this.destroy$),
        ).subscribe((res: boolean) => {
            if (res) {
                this.loadCertificatesOnEdit.emit(true);
            }
        });
        this.certificatePermissions = CertificatePermissions[this.recordType];
    }

    editCertificate() {
        this.otModalService
            .create(
                AddEditCertificateFormModalComponent,
                {
                    isComponent: true,
                },
                {
                    isEditMode: true,
                    certificateDetails: this.certificate,
                    handleModalAddCert$: this.handleModalAddCert,
                    recordType: this.recordType,
                },
            );
    }

    deleteCertificate() {
        this.otModalService.confirm({
            type: ConfirmModalType.DELETE,
            translations: {
                title: this.translatePipe.transform("DeleteCertificate"),
                desc: this.translatePipe.transform("AreYouSureDeleteCertificate"),
                confirm: this.translatePipe.transform("DoYouWantToContinue"),
                submit: this.translatePipe.transform("Delete"),
                cancel: this.translatePipe.transform("Cancel"),
            },
        }).pipe(
            take(1),
            filter((result: boolean) => result),
        ).subscribe(() => {
            this.certificatesApiService.deleteCertificate(this.recordType, this.certificate.id).subscribe((data) => {
                if (data.result) {
                    this.loadCertificatesOnEdit.emit(true);
                }
            });
        });
    }
}
