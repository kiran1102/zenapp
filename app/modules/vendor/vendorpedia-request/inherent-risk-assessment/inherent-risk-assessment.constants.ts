import { OptionValue, QuestionTypes } from "./inherent-risk-assessment.emuns";

export const Options = {
    [OptionValue.YES]: {
        label: "Yes",
        value: 1,
    },
    [OptionValue.NO]: {
        label: "No",
        value: 0,
    },
    [OptionValue.NA]: {
        label: "NotApplicable",
        value: 0,
    },
    [OptionValue.LIMITED_EFFECT]: {
        label: "LimitedAdverseEffect",
        value: 1,
        hint: [
            "Vendor.AssessmentLevelQuestions.MinorReductionInEffectiveness",
            "Vendor.AssessmentLevelQuestions.MinorDamageToOrganizationalAssets",
            "Vendor.AssessmentLevelQuestions.MinorFinancialLossOrPenalty",
            "Vendor.AssessmentLevelQuestions.MinorHarmToIndividuals",
        ],
    },
    [OptionValue.SERIOUS_EFFECT]: {
        label: "SeriousAdverseEffect",
        value: 2,
        hint: [
            "Vendor.AssessmentLevelQuestions.SignificantReductionInTheEffectiveness",
            "Vendor.AssessmentLevelQuestions.SignificantDamageToOrganizationalAssets",
            "Vendor.AssessmentLevelQuestions.SignificantFinancialLossOrPenalty",
            "Vendor.AssessmentLevelQuestions.SignificantHarmToIndividuals",
        ],
    },
    [OptionValue.SEVERE_EFFECT]: {
        label: "SevereOrCatastrophicEffect",
        value: 3,
        hint: [
            "Vendor.AssessmentLevelQuestions.SevereReductionInTheEffectiveness",
            "Vendor.AssessmentLevelQuestions.SevereDamageToOrganizationalAssets",
            "Vendor.AssessmentLevelQuestions.SevereFinancialLossOrPenalty",
            "Vendor.AssessmentLevelQuestions.SevereHarmToIndividuals",
        ],
    },
};

export const OptionTypes = {
    [QuestionTypes.YES_NO_NA]: [
        Options[OptionValue.YES],
        Options[OptionValue.NO],
        Options[OptionValue.NA],
    ],
    [QuestionTypes.EFFECT_LEVEL]: [
        Options[OptionValue.LIMITED_EFFECT],
        Options[OptionValue.SERIOUS_EFFECT],
        Options[OptionValue.SEVERE_EFFECT],
    ],
};

export const Questions = [
    {
        key: "Vendor.AssessmentLevelQuestions.ProprietaryConfidentialInformation",
        options: OptionTypes[QuestionTypes.YES_NO_NA],
    },
    {
        key: "Vendor.AssessmentLevelQuestions.PersonalData",
        options: OptionTypes[QuestionTypes.YES_NO_NA],
    },
    {
        key: "Vendor.AssessmentLevelQuestions.SensitivePersonalData",
        description: [
            "DataRevealingRacialOrEthnicOrigin",
            "DataRevealingPoliticalOpinion",
            "DataRevealingReligiousOrPhilosophicalBeliefs",
            "DataRevealingTradeUnionMembership",
            "GeneticData",
            "BiometricData",
            "HealthData",
            "DataConcerningSexLife",
            "DataConcerningSexualOrientation",
            "DataConcerningCriminalConvictionOrOffenses",
            "ElectronicCommunicationsData",
            "LocationData",
            "FinancialData",
            "NationalIdentifier",
            "NotSure",
            "NotApplicable",
        ],
        options: OptionTypes[QuestionTypes.YES_NO_NA],
    },
    {
        key: "Vendor.AssessmentLevelQuestions.CrossBorderDataTransfers",
        options: OptionTypes[QuestionTypes.YES_NO_NA],
    },
    {
        key: "Vendor.AssessmentLevelQuestions.CriticalBusinessFunctions",
        options: OptionTypes[QuestionTypes.YES_NO_NA],
    },
    {
        key: "Vendor.AssessmentLevelQuestions.Confidentiality",
        options: OptionTypes[QuestionTypes.EFFECT_LEVEL],
        description: "Vendor.AssessmentLevelQuestions.AnswerIsSubjective",
        hintHeader: "Vendor.AssessmentLevelQuestions.Confidentiality.HintHeader",
    },
    {
        key: "Vendor.AssessmentLevelQuestions.Integrity",
        options: OptionTypes[QuestionTypes.EFFECT_LEVEL],
        description: "Vendor.AssessmentLevelQuestions.AnswerIsSubjective",
        hintHeader: "Vendor.AssessmentLevelQuestions.Integrity.HintHeader",
    },
    {
        key: "Vendor.AssessmentLevelQuestions.Availability",
        options: OptionTypes[QuestionTypes.EFFECT_LEVEL],
        description: "Vendor.AssessmentLevelQuestions.AnswerIsSubjective",
        hintHeader: "Vendor.AssessmentLevelQuestions.Availability.HintHeader",
    },
];
