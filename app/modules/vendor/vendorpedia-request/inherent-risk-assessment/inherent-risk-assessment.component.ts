// Core
import {
    Component,
    OnInit,
} from "@angular/core";
import {
    FormControl,
    FormGroup,
    Validators,
} from "@angular/forms";

// RXJS
import { Subject } from "rxjs";

// Services
import { StateService } from "@uirouter/core";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

// Constants
import { Questions, Options } from "./inherent-risk-assessment.constants";
import { InherentRiskAssessmentLevels } from "vendorpediaRequest/shared/constants/inherent-risk-assessment-levels.constants";

interface ISelectedVendorModalData {
    selectedVendor: {
        "vendor": null,
        "vendorService": null,
        "vendorContactName": null,
        "vendorContactEmail": null,
        "assessmentLevel": null,
    };
}

@Component({
    selector: "vendorpedia-assessment-level-form",
    templateUrl: "./inherent-risk-assessment.component.html",
})
export class InherentRiskAssessmentComponent implements OnInit {
    otModalCloseEvent = new Subject();
    otModalData: ISelectedVendorModalData;
    questions = Questions;
    form: FormGroup;
    submitButtonText = this.translatePipe.transform("Submit");
    results = false;
    assessmentLevels = InherentRiskAssessmentLevels;
    assessmentLevel: number;
    score: number;
    bounds = ".ot-modal-base__container";

    constructor(
        private stateService: StateService,
        private translatePipe: TranslatePipe,
    ) { }

    ngOnInit() {
        this.createForm();
    }

    formatExampleArray(arr) {
        return arr.map((term, i) => {
            const translation = this.translatePipe.transform(term).toLowerCase();
            if (arr.length === i + 1) {
                return `${this.translatePipe.transform("Or")} ${translation}.`;
            } else {
                return `${translation}, `;
            }
        });
    }

    createForm() {
        const group = {};
        this.questions.map((question: any, index) => {
            group[index] = new FormControl("", Validators.required);
            return question;
        });
        this.form = new FormGroup(group);
    }

    next() {
        if (!this.results) {
            this.calculateResults();
        } else {
            this.otModalCloseEvent.next(this.assessmentLevel);
        }
    }

    backToQuestions() {
        this.results = false;
        this.submitButtonText = "Submit";
    }

    closeModal() {
        this.otModalCloseEvent.next(this.assessmentLevel);
    }

    isArray(val) { return Array.isArray(val); }

    private calculateResults() {
        this.score = 0;
        Object.keys(this.form.controls).forEach((key) => {
            const type = this.form.get(key).value;
            const value = Options[type].value;
            this.score = this.score + value;
        });
        this.assessmentLevel = this.determineAssesmentLevel(this.score);
        this.results = true;
        this.submitButtonText = this.translatePipe.transform("Done");
    }

    private determineAssesmentLevel(score) {
        if (score >= 7) return 3;
        if (score >= 4) return 2;
        return 1;
    }
}
