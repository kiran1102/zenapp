export enum OptionValue {
    YES = "Yes",
    NO = "No",
    NA = "NotApplicable",
    LIMITED_EFFECT = "LimitedAdverseEffect",
    SERIOUS_EFFECT = "SeriousAdverseEffect",
    SEVERE_EFFECT = "SevereOrCatastrophicEffect",
}

export enum QuestionTypes {
    YES_NO_NA = "yesNoNa",
    EFFECT_LEVEL = "effectLevel",
}
