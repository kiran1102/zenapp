// Core
import { Injectable } from "@angular/core";

// Services
import { ProtocolService } from "modules/core/services/protocol.service";

// Interfaces
import {
    IProtocolConfig,
    IProtocolMessages,
    IProtocolResponse,
} from "interfaces/protocol.interface";
import { IStringMap } from "interfaces/generic.interface";
import {
    IPageableOf,
    IPaginatedResponse,
} from "interfaces/pagination.interface";
import { IVPRequestInformation } from "modules/vendor/shared/interfaces/vendorpedia-request.interface";
import { IServiceForSelectedVendor } from "modules/vendor/shared/interfaces/vendorpedia-exchange.interface";
import { IInventoryRecordResponseV2 } from "interfaces/inventory.interface";
import { IUser } from "interfaces/user.interface";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

// Rxjs
import {
    from,
    Observable,
} from "rxjs";

@Injectable()
export class VendorpediaRequestApiService {

    constructor(
        private protocolService: ProtocolService,
        private translatePipe: TranslatePipe,
    ) { }

    retrieveVendorAssessments(paginationParams?, payload?): Observable<IProtocolResponse<IPageableOf<IVPRequestInformation>>> {
        const config: IProtocolConfig = this.protocolService.customConfig("POST", `/api/vendor/v1/vendors/exchanges/assessments/requests/pages`, { ...paginationParams }, payload);
        const messages: IProtocolMessages = {
            Error: { custom: this.translatePipe.transform("ErrorInRetrivingVendorExchangeAssessmentsRequests") },
        };
        return from(this.protocolService.http(config, messages));
    }

    fetchAssessmentStatus(payload: IStringMap<string>): Observable<IProtocolResponse<boolean>> {
        const config: IProtocolConfig = this.protocolService.customConfig("POST", `/api/vendor/v1/vendors/exchanges/assessments/requests/open/exists`, null, payload);
        const messages: IProtocolMessages = {
            Error: { custom: this.translatePipe.transform("ErrorInRetrivingVendorExchangeAssessmentsRequests") },
        };
        return from(this.protocolService.http(config, messages));
    }

    addVendorAssessment(assessmentDetails: IStringMap<string>): Observable<IProtocolResponse<string>> {
        const config: IProtocolConfig = this.protocolService.customConfig("POST", `/api/vendor/v1/vendors/exchanges/assessments/requests`, null, assessmentDetails);
        const messages: IProtocolMessages = {
            Error: { Hide: true },
            Success: { custom: this.translatePipe.transform("AssessmentSuccessfullySubmitted") },
        };
        return from(this.protocolService.http(config, messages));
    }

    fetchServiceByVendorId(vendorId: string, associationType: string): Observable<IProtocolResponse<IPaginatedResponse<IServiceForSelectedVendor>>> {
        const config: IProtocolConfig = this.protocolService.customConfig("GET", `/api/inventory/v2/inventories/${vendorId}/relations/assets/relationtypes/${associationType}`);
        const messages: IProtocolMessages = {
            Error: { custom: this.translatePipe.transform("ErrorInFetchingServices") },
        };
        return from(this.protocolService.http(config, messages));
    }

    fetchAssessmentStandards(): Observable<IProtocolResponse<string[]>> {
        const config: IProtocolConfig = this.protocolService.customConfig("GET", `/api/vendor/v1/vendors/exchanges/requests/standards`);
        const messages: IProtocolMessages = {
            Error: { custom: this.translatePipe.transform("ErrorInFetchingAssessmentStandards") },
        };
        return from(this.protocolService.http(config, messages));
    }

    getVendorDetailsByVendorId(vendorId: string): Observable<IProtocolResponse<IInventoryRecordResponseV2>> {
        const config: IProtocolConfig = this.protocolService.customConfig("GET", `/api/inventory/v2/inventories/vendors/${vendorId}`);
        const messages: IProtocolMessages = {
            Error: { custom: this.translatePipe.transform("ErrorInFetchingVendorContactDetails") },
        };
        return from(this.protocolService.http(config, messages));
    }

    getVendorContactDetails(primaryVendorContact: string): Observable<IProtocolResponse<IUser>>  {
        const config: IProtocolConfig = this.protocolService.customConfig("GET", `/api/v1/private/orggroupuser/getdetails/${primaryVendorContact}`);
        const messages: IProtocolMessages = {
            Error: { custom: this.translatePipe.transform("ErrorInFetchingVendorContactDetails") },
        };
        return from(this.protocolService.http(config, messages));
    }
}
