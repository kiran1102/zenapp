export const InherentRiskAssessmentLevels = [
    {
        id: "AssessmentLevel1",
        key: "LevelNAssessment",
        description: "Vendor.AssessmentLevelQuestions.Level1.Description",
        level: 1,
    },
    {
        id: "AssessmentLevel2",
        key: "LevelNAssessment",
        description: "Vendor.AssessmentLevelQuestions.Level2.Description",
        level: 2,
    },
    {
        id: "AssessmentLevel3",
        key: "LevelNAssessment",
        description: "Vendor.AssessmentLevelQuestions.Level3.Description",
        level: 3,
    },
];
