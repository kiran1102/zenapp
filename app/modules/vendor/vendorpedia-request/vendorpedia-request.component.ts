// Core
import {
    Component,
    OnInit,
    Input,
} from "@angular/core";

import { StateService } from "@uirouter/core";

// Rxjs
import { takeUntil } from "rxjs/operators";
import { Subject } from "rxjs";

// Interfaces
import { IProtocolResponse } from "interfaces/protocol.interface";
import { IStringMap } from "interfaces/generic.interface";
import {
    IVPRequestInformation,
    IVPRequestTable,
 } from "modules/vendor/shared/interfaces/vendorpedia-request.interface";
import { IPageableOf } from "interfaces/pagination.interface";

// Services
import { VendorpediaRequestApiService } from "vendorpediaRequest/shared/services/vendorpedia-request-api.service";
import { GridViewStorageService } from "sharedModules/services/helper/grid-view-storage.service";

// Enums & Constants
import { VPRequestTableColumnNames } from "modules/vendor/shared/enums/vendorpedia-request.enums";
import { GridViewStorageNames } from "sharedModules/enums/grid-view-storage.enum";
import { TableColumnTypes } from "enums/data-table.enum";
import { DefaultPagination } from "../shared/constants/vendor-documents.constants";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

@Component({
    selector: "vendorpedia-request",
    templateUrl: "./vendorpedia-request.component.html",
})
export class VendorpediaRequestComponent implements OnInit {

    colPermissions: IStringMap<string>;
    isPageloading = true;
    table: IVPRequestTable;
    paginationParams = { ...DefaultPagination, sort: null };
    storageKey = GridViewStorageNames.VENDORPEDIA_EXCHANGE_LIST;
    sendAndRequest = new Subject();
    private destroy$ = new Subject();

    constructor(
        private vendorpediaRequestApi: VendorpediaRequestApiService,
        private browserStorageListService: GridViewStorageService,
        private translatePipe: TranslatePipe,
        private stateService: StateService,
    ) { }

    ngOnInit() {
        const storage = this.browserStorageListService.getBrowserStorageList(this.storageKey);
        if (storage && storage.pagination) {
            this.paginationParams = storage.pagination;
        }
        this.colPermissions = {
            [VPRequestTableColumnNames.LEVEL]: "VPAssessmentLevel",
        };
        this.getVendorAssessments();
        this.sendAndRequest.pipe(takeUntil(this.destroy$),
        ).subscribe((res: boolean) => {
            this.isPageloading = true;
            this.paginationParams.page = 0;
            this.getVendorAssessments();
        });
    }

    getVendorAssessments() {
        this.vendorpediaRequestApi.retrieveVendorAssessments(this.paginationParams).subscribe((res: IProtocolResponse<IPageableOf<IVPRequestInformation>>) => {
            if (res.result) {
                this.table = this.configureTable(res.data);
                if (this.table) this.isPageloading = false;
            }
        });
    }

    onPaginationChange(event: number = 0): void {
        this.isPageloading = true;
        this.paginationParams.page = event;
        this.getVendorAssessments();
        this.browserStorageListService.saveSessionStoragePagination(this.storageKey, this.paginationParams);
    }

    requestAssessment() {
        this.stateService.go("zen.app.pia.module.vendor.vendorpedia-request-new");
    }

    private configureTable(records: IPageableOf<IVPRequestInformation>): IVPRequestTable {
        if (records.numberOfElements === 0 && !records.first && records.last) {
            this.paginationParams.page--;
            this.browserStorageListService.saveSessionStoragePagination(this.storageKey, this.paginationParams);
            this.getVendorAssessments();
            return null;
        }
        return {
            rows: records.content,
            columns: [
                {
                    name: this.translatePipe.transform("ID"),
                    type: TableColumnTypes.Text,
                    cellValueKey: VPRequestTableColumnNames.ID,
                },
                {
                    name: this.translatePipe.transform("Vendor"),
                    type: TableColumnTypes.Text,
                    cellValueKey: VPRequestTableColumnNames.VENDOR,
                },
                {
                    name: this.translatePipe.transform("Service"),
                    type: TableColumnTypes.Text,
                    cellValueKey: VPRequestTableColumnNames.SERVICE,
                },
                {
                    name: this.translatePipe.transform("AssessmentLevel"),
                    type: TableColumnTypes.Translate,
                    cellValueKey: VPRequestTableColumnNames.LEVEL,
                },
                {
                    name: this.translatePipe.transform("VendorOrganization"),
                    type: TableColumnTypes.Text,
                    cellValueKey: VPRequestTableColumnNames.ORGANIZATION,
                },
                // TODO: Uncomment later - temporarily hiding the status for 4.5
                // {
                //     name: this.translatePipe.transform("Status"),
                //     type: TableColumnTypes.Stage,
                //     cellValueKey: VPExchangeTableColumnNames.STATUS,
                //     columnKey: VPExchangeTableColumnNames.STATUS,
                // },
                {
                    name: this.translatePipe.transform("DateRequested"),
                    type: TableColumnTypes.Date,
                    cellValueKey: VPRequestTableColumnNames.DATE_REQUESTED,
                },
                {
                    name: this.translatePipe.transform("LastUpdated"),
                    type: TableColumnTypes.Date,
                    cellValueKey: VPRequestTableColumnNames.LAST_UPDATED,
                },
            ],
            metaData: records,
        };
    }
}
