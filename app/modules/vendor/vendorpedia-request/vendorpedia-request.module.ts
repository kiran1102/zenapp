// Core
import {
    NgModule,
    CUSTOM_ELEMENTS_SCHEMA,
} from "@angular/core";

// Modules
import { SharedModule } from "sharedModules/shared.module";
import { UpgradedLegacyModule } from "sharedModules/upgrade/upgraded-legacy.module";
import { CommonModule } from "@angular/common";
import { HttpClientModule } from "@angular/common/http";
import { FormsModule } from "@angular/forms";
import { ReactiveFormsModule } from "@angular/forms";
import { VitreusModule } from "@onetrust/vitreus";
import { DndListModule } from "ngx-drag-and-drop-lists";
import { OTPipesModule, PIPES } from "modules/pipes/pipes.module";
import { VirtualScrollModule } from "od-virtualscroll";
import { OneVideoModule } from "modules/video/video.module";
import { VendorSharedModule } from "modules/vendor/shared/vendor-shared.module";
import { TranslateModule } from "@ngx-translate/core";

// Services
import { VendorpediaRequestApiService } from "vendorpediaRequest/shared/services/vendorpedia-request-api.service";

// Components
import { VendorpediaRequestComponent } from "vendorpediaRequest/vendorpedia-request.component";
import { VendorpediaRequestTableComponent } from "vendorpediaRequest/vendorpedia-request-table/vendorpedia-request-table.component";

import { VendorpediaRequestFormComponent } from "vendorpediaRequest/vendorpedia-request-form/vendorpedia-request-form.component";
import { InherentRiskAssessmentComponent } from "vendorpediaRequest/inherent-risk-assessment/inherent-risk-assessment.component";

@NgModule({
    imports: [
        SharedModule,
        UpgradedLegacyModule,
        CommonModule,
        HttpClientModule,
        FormsModule,
        ReactiveFormsModule,
        VitreusModule,
        DndListModule,
        OTPipesModule,
        VirtualScrollModule,
        OneVideoModule,
        VendorSharedModule,
        TranslateModule,
    ],
    declarations: [
        VendorpediaRequestComponent,
        VendorpediaRequestTableComponent,
        VendorpediaRequestFormComponent,
        InherentRiskAssessmentComponent,
    ],
    exports: [
    ],
    entryComponents: [
        VendorpediaRequestComponent,
        VendorpediaRequestFormComponent,
        VendorpediaRequestTableComponent,
        InherentRiskAssessmentComponent,
    ],
    providers: [
        VendorpediaRequestApiService,
        PIPES, // TODO: Remove from providers when ot-common is implemented. Only necessary if you are using pipe in TS file, else just module is fine
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class VendorpediaRequestModule { }
