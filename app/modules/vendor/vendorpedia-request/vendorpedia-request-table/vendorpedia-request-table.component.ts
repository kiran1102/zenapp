// Core
import {
    Component,
    Input,
} from "@angular/core";

// Enums
import { TableColumnTypes } from "enums/data-table.enum";

// Interfaces
import { IStringMap } from "interfaces/generic.interface";
import { IVPRequestTable } from "modules/vendor/shared/interfaces/vendorpedia-request.interface";
import { VPRequestStageBadgeColorMap } from "modules/vendor/shared/constants/vendorpedia-request.constants";

@Component({
    selector: "vendorpedia-request-table",
    templateUrl: "./vendorpedia-request-table.component.html",
})
export class VendorpediaRequestTableComponent {

    @Input() table: IVPRequestTable;
    @Input() colPermissions: IStringMap<string>;

    vPExchangeStageBadgeColorMap = VPRequestStageBadgeColorMap;
    resizableCol = false;
    responsiveTable = true;
    rowBordered = true;
    rowNoHover = false;
    rowStriped = false;
    tableColumnTypes = TableColumnTypes;
    truncateCellContent = true;
    wrapCellContent = false;
    emptyData = "- - - -";

}
