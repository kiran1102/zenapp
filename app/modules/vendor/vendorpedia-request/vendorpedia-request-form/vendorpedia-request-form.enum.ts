export enum AssessmentStandards {
    NIST = "Nist",
    ISO = "Iso",
    SIG_LITE = "SigLite",
    CSA_CAIQ = "CsaCaiq",
}
