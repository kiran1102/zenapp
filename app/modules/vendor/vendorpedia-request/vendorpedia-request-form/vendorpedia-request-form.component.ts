// Core
import {
    Component,
    Inject,
} from "@angular/core";

// Services
import { StateService } from "@uirouter/core";
import { VendorApiService } from "modules/vendor/services/api/vendor-api.service";
import { VendorpediaRequestApiService } from "vendorpediaRequest/shared/services/vendorpedia-request-api.service";
import { NotificationService } from "modules/shared/services/provider/notification.service";
import { Permissions } from "modules/shared/services/helper/permissions.service";
import { TranslateService } from "@ngx-translate/core";

// Interface
import { IStore } from "interfaces/redux.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";
import { IPaginatedResponse } from "interfaces/pagination.interface";
import { IInventoryRecordResponseV2 } from "interfaces/inventory.interface";
import { IUser } from "interfaces/user.interface";
import {
    IServiceForSelectedVendor,
    IInventoryVendorNames,
} from "modules/vendor/shared/interfaces/vendorpedia-exchange.interface";
import {
    ISelection,
    IStringMap,
    INameId,
    IKeyValue,
    IValueId,
    ICommentNameId,
} from "interfaces/generic.interface";

// Forms
import {
    FormGroup,
    FormBuilder,
    Validators,
} from "@angular/forms";

// Reducers
import { getCurrentOrgId } from "oneRedux/reducers/org.reducer";
import { StoreToken } from "tokens/redux-store.token";

// Vitreus
import {
    IOtModalContent,
    ModalSize,
    OtModalService,
} from "@onetrust/vitreus";

// Rxjs
import { Subject } from "rxjs";

// Constants
import { Regex } from "constants/regex.constant";
import { InherentRiskAssessmentLevels } from "vendorpediaRequest/shared/constants/inherent-risk-assessment-levels.constants";
import { vendorpediaRequestAssessmentErrorCodes } from "modules/vendor/shared/constants/vendorpedia-error-codes.constants";

// Pipe
import { FilterPipe } from "modules/pipes/filter.pipe";
import { TranslatePipe } from "modules/pipes/translate.pipe";
import { takeUntil } from "rxjs/operators";

// Components
import { InherentRiskAssessmentComponent } from "vendorpediaRequest/inherent-risk-assessment/inherent-risk-assessment.component";

@Component({
    selector: "vendorpedia-request-form",
    templateUrl: "./vendorpedia-request-form.component.html",
})

export class VendorpediaRequestFormComponent implements IOtModalContent {

    otModalCloseEvent: Subject<boolean>;
    requestAssessmentModalForm: FormGroup;
    vendorList: Array<INameId<string>>;
    filterredVendorList: Array<ICommentNameId<string>>;
    emailRegex = Regex.EMAIL;
    validateEmail: any;
    buttonDisableonSubmission = false;
    loadingModalData: boolean;
    showDropDown = true;
    vendorNameDropDownDisabled: string;
    servicesForSelectedVendor: Array<INameId<string>>;
    serviceList: Array<INameId<string>>;
    disableService: boolean;
    serviceVendorAssociationType = "PRODUCT_SERVICE_PROVIDER";
    vendorName: string = null;
    vendorEmail: string = null;

    assessmentLevels = InherentRiskAssessmentLevels;
    assessmentStandards = [];
    openAssessments;

    private destroy$ = new Subject();
    private translations: {};
    private translationKeys = [
       "VendorOnlyAssessment",
       "ErrorInSubmittingVendorRequestAssessment",
       "Error",
    ];

    constructor(
        @Inject(StoreToken) private store: IStore,
        private formBuilder: FormBuilder,
        private vendorApi: VendorApiService,
        private filterPipe: FilterPipe,
        private vendorpediaRequestApi: VendorpediaRequestApiService,
        private notificationService: NotificationService,
        private stateService: StateService,
        private otModalService: OtModalService,
        private permissions: Permissions,
        private translate: TranslateService,
    ) { }

    get vendor() { return this.requestAssessmentModalForm.get("vendor"); }
    get vendorService() { return this.requestAssessmentModalForm.get("vendorService"); }
    get vendorContactName() { return this.requestAssessmentModalForm.get("vendorContactName"); }
    get vendorContactEmail() { return this.requestAssessmentModalForm.get("vendorContactEmail"); }
    get assessmentLevel() { return this.requestAssessmentModalForm.get("assessmentLevel"); }
    get assessmentStandard() { return this.requestAssessmentModalForm.get("assessmentStandard"); }

    ngOnInit() {
        this.vendorpediaRequestApi.fetchAssessmentStandards().subscribe((res: IProtocolResponse<string[]>) => {
            if (res.result) this.assessmentStandards = res.data.map((item) => {
                return { name: this.translate.instant(item), id: item };
            });
        });
        this.configureTranslations();
        // Creates Form Inputs Groups
        this.generateRequestAssessmentsFormGroup();
        // Determine who loaded the component
        if (
            this.stateService.params
            && this.stateService.params.name
            && this.stateService.params.org
            && this.stateService.params.inventoryId
        ) {
            this.requestAssessmentModalFromVPExchange();
        } else {
            this.requestAssessmentModalFromVPRequest();
        }
    }

    requestAssessmentModalFromVPRequest() {
        this.disableService = true;
        const orgId = getCurrentOrgId(this.store.getState());
        this.vendorApi.fetchInventoryVendorNames(orgId).then((res: IProtocolResponse<IInventoryVendorNames[]>): void => {
            if (res.result) {
                this.loadingModalData = false;
                this.vendorList = res.data.map((item: IInventoryVendorNames) => {
                    return { name: item.name, id: item.id, comment: (item.organization as IValueId<string>).id };
                });
                this.filterredVendorList = [...this.vendorList];
            }
        });
    }

    requestAssessmentModalFromVPExchange() {
        this.loadingModalData = true;
        this.showDropDown = !this.showDropDown;
        this.vendorNameDropDownDisabled = this.stateService.params.name;
        this.filterredVendorList = [{ id: this.stateService.params.inventoryId, name: this.stateService.params.name, comment: this.stateService.params.org }];
        this.vendor.setValue(this.filterredVendorList[0]);
        this.fetchServiceForSelectedVendor(this.stateService.params.inventoryId, this.serviceVendorAssociationType);
    }

    cancel() {
        if (this.stateService.params.previousState) {
            this.stateService.go(this.stateService.params.previousState, { inventoryId: this.stateService.params.vendorpediaId });
        } else {
            this.stateService.go("zen.app.pia.module.vendor.vendorpedia-request");
        }
    }

    filter(type: string, event: IKeyValue<string>) {
        if (type === "vendor") {
            this.vendor.markAsDirty();
            this.filterredVendorList = this.filterVendorOrService(this.vendorList, event);
            this.servicesForSelectedVendor = [];
            this.vendorService.setValue(null);
            if (!this.vendor.value) this.disableService = true;
        } else {
            if (event) {
                this.servicesForSelectedVendor = this.filterVendorOrService(this.serviceList, event);
            }
        }
    }

    filterVendorOrService(arrayList: Array<INameId<string>>, event: IKeyValue<string>) {
        if (arrayList) return this.filterPipe.transform(arrayList, ["name"], event != null ? event.value : "");
    }

    updateVendor(type: string, event: ISelection<INameId<string>>) {
        if (!event.change) return;
        if (type === "vendor") {
            this.servicesForSelectedVendor = [];
            this.vendorService.setValue(null);
            this.vendorContactName.setValue(null);
            this.vendorContactEmail.setValue(null);
            this.vendor.setValue(event.currentValue);
            this.vendorService.setValue(null);
            if (event.currentValue) {
                this.fetchServiceForSelectedVendor(event.currentValue.id, this.serviceVendorAssociationType);
                this.disableService = false;
            } else {
                this.disableService = true;
            }
        } else if (type === "standard") {
            this.requestAssessmentModalForm.get("assessmentStandard").setValue(event.currentValue);
        } else {
            this.requestAssessmentModalForm.get("vendorService").setValue(event.currentValue);
            if (event.currentValue) this.fetchOpenAssessments();
        }
    }

    fetchServiceForSelectedVendor(id: string, associationType: string) {
        this.disableService = true;
        this.vendorpediaRequestApi.fetchServiceByVendorId(id, associationType)
            .subscribe((res: IProtocolResponse<IPaginatedResponse<IServiceForSelectedVendor>>) => {
                if (res.result) {
                    this.serviceList = res.data.data.map((item: IServiceForSelectedVendor) => {
                        return { name: item.name, id: item.id };
                    });
                    this.servicesForSelectedVendor = [...this.serviceList];
                    this.serviceList.unshift({ name: this.translations["VendorOnlyAssessment"], id: null });
                    this.getVendorDetails(id);
                } else {
                    this.loadingModalData = false;
                }
                this.disableService = false;
            });
    }

    getVendorDetails(vendorId: string) {
        this.vendorpediaRequestApi.getVendorDetailsByVendorId(vendorId).subscribe((res: IProtocolResponse<IInventoryRecordResponseV2>) => {
            if (res.result) {
                if (res.data && res.data.data && res.data.data.primaryContact && res.data.data.primaryContact[0].id) {
                    this.getVendorContactDetails(res.data.data.primaryContact[0].id);
                } else {
                    this.loadingModalData = false;
                }
            } else {
                this.loadingModalData = false;
            }
        });
    }

    getVendorContactDetails(vendorContactId: string) {
        this.vendorpediaRequestApi.getVendorContactDetails(vendorContactId).subscribe((response: IProtocolResponse<IUser>) => {
            if (response.result && response.data) {
                this.vendorEmail = response.data.Email;
                this.vendorContactEmail.setValue(this.vendorEmail);
                if (response.data.FirstName) {
                    this.vendorName = response.data.FullName;
                    this.vendorContactName.setValue(this.vendorName);
                }
            }
            this.loadingModalData = false;
        });
    }

    defaultDropDown() {
        if (!this.vendorService.value) this.servicesForSelectedVendor = this.serviceList;
        if (!this.vendor.value) this.filterredVendorList = this.vendorList;
    }

    initializeFilter(type: string) {
        if (type === "vendor") {
            if (!this.vendor.value && (this.filterredVendorList && this.filterredVendorList.length === 0)) this.filter(type, null);
        } else {
            if (!this.vendorService.value && this.servicesForSelectedVendor) {
                this.vendorService.setValue(null);
            }
        }
    }

    onSave(sendRequest = false) {
        if (this.requestAssessmentModalForm.invalid) return;
        this.buttonDisableonSubmission = true;
        const formData = this.requestAssessmentModalForm.value;
        const requestAssessmentPayLoad: IStringMap<string> = {
            vendorId: formData.vendor.id,
            vendorName: formData.vendor.name,
            vendorOrgGroupId: formData.vendor.comment,
            serviceName: formData.vendorService.name,
            serviceId: formData.vendorService.id,
            status: "Requested",
            vendorContactName: formData.vendorContactName ? formData.vendorContactName.trim() : null,
            vendorContactEmail: formData.vendorContactEmail ? formData.vendorContactEmail.trim() : null,
            assessmentLevel: formData.assessmentLevel ? InherentRiskAssessmentLevels[formData.assessmentLevel - 1].id : InherentRiskAssessmentLevels[0].id,
            assessmentStandards: formData.assessmentStandard.map((item) => item.id) || [],
        };
        this.vendorpediaRequestApi.addVendorAssessment(requestAssessmentPayLoad).subscribe((res: IProtocolResponse<string>) => {
            this.buttonDisableonSubmission = false;
            this.generateRequestAssessmentsFormGroup();
            if (res.result) {
                this.stateService.go("zen.app.pia.module.vendor.vendorpedia-request-new");
            } else {
                this.checkForErrorScenariosReqAssessment(res, requestAssessmentPayLoad);
            }
            this.handleFormFields(sendRequest);
        });
    }

    handleFormFields(sendRequest: boolean) {
        if (this.stateService.params && this.stateService.params.name && this.stateService.params.org && this.stateService.params.inventoryId) {
            this.vendor.setValue(this.filterredVendorList[0]);
            this.vendorContactName.setValue(this.vendorName);
            this.vendorContactEmail.setValue(this.vendorEmail);
        } else {
            if (!this.vendor.value) this.disableService = true;
        }
        if (sendRequest) this.stateService.go("zen.app.pia.module.vendor.vendorpedia-request");
    }

    checkForErrorScenariosReqAssessment(res: IProtocolResponse<string>, requestAssessmentPayLoad: IStringMap<string>) {
        let errorMsg = this.translations["ErrorInSubmittingVendorRequestAssessment"];
        if (res.errorCode === vendorpediaRequestAssessmentErrorCodes.EXISTING_EXCHANGE_REQUEST_WITHOUT_SERVICE) {
            errorMsg = this.translate.instant("ThereIsAlreadyAnOpenRequestForGiven", { vendorName: requestAssessmentPayLoad.vendorName });
        } else if (res.errorCode === vendorpediaRequestAssessmentErrorCodes.EXISTING_EXCHANGE_REQUEST_WITH_SERVICE) {
            errorMsg = this.translate.instant("ThereIsAlreadyAnOpenRequestForGivenVendorService", { vendorName: requestAssessmentPayLoad.vendorName, serviceName: requestAssessmentPayLoad.serviceName });
        }
        this.notificationService.alertError(this.translations["Error"], errorMsg);
    }

    selectAssessmentLevel(level: number) {
        return level === this.assessmentLevel.value ? this.assessmentLevel.setValue(null) : this.assessmentLevel.setValue(level);
    }

    openAssessmentLevelModal() {
        this.otModalService
            .create(
                InherentRiskAssessmentComponent,
                {
                    isComponent: true,
                    size: ModalSize.MEDIUM,
                },
                {
                    selectedVendor: this.requestAssessmentModalForm.value,
                },
            ).pipe(takeUntil(this.destroy$),
            ).subscribe((level: number) => this.assessmentLevel.setValue(level));
    }

    fetchOpenAssessments() {
        this.buttonDisableonSubmission = true;
        const payload = {
            vendorId: this.vendor.value.id,
            serviceId: this.vendorService.value.id,
            serviceName: this.vendorService.value.name,
        };
        this.vendorpediaRequestApi.fetchAssessmentStatus(payload).subscribe((res) => {
            if (res.result && res.data) {
                let message: string;
                if (!this.vendorService.value.id) {
                    message = this.translate.instant(
                        "ThereIsAlreadyAnOpenRequestForGiven",
                        { vendorName: this.vendor.value.name },
                        );
                } else {
                    message = this.translate.instant(
                        "ThereIsAlreadyAnOpenRequestForGivenVendorService",
                        { vendorName: this.vendor.value.name, serviceName: this.vendorService.value.name },
                        );
                }
                this.vendorService.setErrors({message});
            }
            this.buttonDisableonSubmission = false;
        });
    }

    private configureTranslations() {
        this.translate.stream(this.translationKeys).subscribe((translations) => {
            this.translations = translations;
        });
    }

    private generateRequestAssessmentsFormGroup() {
        this.requestAssessmentModalForm = this.formBuilder.group({
            vendor: [null, [Validators.required]],
            vendorService: [null, [Validators.required]],
            vendorContactName: [null, [Validators.maxLength(200)]],
            vendorContactEmail: [null, Validators.compose([Validators.maxLength(200), Validators.pattern(this.emailRegex)])],
            assessmentStandard: [null, [Validators.required]],
            assessmentLevel: [null, this.permissions.canShow("VPAssessmentLevel") ? [Validators.required] : null],
        });
    }
}
