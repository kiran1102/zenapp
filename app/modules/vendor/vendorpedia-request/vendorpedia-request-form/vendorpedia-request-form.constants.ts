import { AssessmentStandards } from "./vendorpedia-request-form.enum";

export const AssessmentStandard = {
    [AssessmentStandards.NIST]: "NIST",
    [AssessmentStandards.ISO]: "ISO",
    [AssessmentStandards.SIG_LITE]: "SIG Lite",
    [AssessmentStandards.CSA_CAIQ]: "CSA CAIQ",
};
