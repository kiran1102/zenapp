// 3rd party
import { pick } from "lodash";
import { AnyAction } from "redux";
import {
    createSelector,
    Selector,
} from "reselect";

// Interfaces
import { IStoreState } from "interfaces/redux.interface";
import {
    IVendorRecordState,
    IVendorCatalogInformation,
} from "modules/vendor/shared/interfaces/vendorpedia-exchange.interface";
import {
    IPageableOf,
    IPageableMeta,
} from "interfaces/pagination.interface";

// Enums
import { VendorRecordActions } from "modules/vendor/shared/enums/vendor-actions.enums";

export const initialVendorRecordState: IVendorRecordState = {
    isLoadingVendorRecord: false,
    data: {
        content: null,
        first: true,
        last: true,
        number: 0,
        numberOfElements: 0,
        size: 0,
        totalElements: 0,
        totalPages: 0,
    },
};

// tslint:disable-next-line:cyclomatic-complexity
export function vendorRecord(state: IVendorRecordState = initialVendorRecordState, action: AnyAction): IVendorRecordState {
    switch (action.type) {
        case VendorRecordActions.FETCHING_VENDOR_RECORD:
            return {
                ...state,
                isLoadingVendorRecord: action.payload,
            };
        case VendorRecordActions.RESET_VENDOR_RECORD:
            return initialVendorRecordState;
        case VendorRecordActions.UPDATE_VENDOR_RECORD:
            return {
                ...state,
                data: action.payload,
                isLoadingVendorRecord: false,
            };
        default:
            return state;
    }
}

export const vendorRecordState: Selector<IStoreState, IVendorRecordState> = (state: IStoreState): IVendorRecordState => state.vendor.vendorRecord;
export const isLoadingVendorRecord = createSelector(vendorRecordState, (state: IVendorRecordState): boolean => state.isLoadingVendorRecord);
export const vendorRecordData = createSelector(vendorRecordState, (state: IVendorRecordState): IPageableOf<IVendorCatalogInformation> => state.data);
export const vendorRecordCardData = createSelector(vendorRecordData, (data: IPageableOf<IVendorCatalogInformation>): IVendorCatalogInformation[] => data.content);
export const vendorRecordMetaData = createSelector(vendorRecordData, (data: IPageableOf<IVendorCatalogInformation>): IPageableMeta =>
    pick(data, ["first", "last", "number", "numberOfElements", "size", "sort", "totalElements", "totalPages"]));
