// Redux
import { Selector } from "reselect";
import { AnyAction } from "redux";
import {
    vendorRecord,
    initialVendorRecordState,
} from "modules/vendor/shared/reducers/vendor-record.reducer";
import {
    vendorDetail,
    initialVendorDetailState,
 } from "modules/vendor/shared/reducers/vendor-detail.reducer";

// Interfaces
import { IStoreState } from "interfaces/redux.interface";
import { IVendorState } from "modules/vendor/shared/interfaces/vendorpedia-reducer.interface";

// Enums
import {
    VendorRecordActions,
    VendorDetailActions,
} from "modules/vendor/shared/enums/vendor-actions.enums";

const initialState: IVendorState = {
    vendorRecord: initialVendorRecordState,
    vendorDetail: initialVendorDetailState,
};

export function vendor(state = initialState, action: AnyAction): IVendorState {
    if (VendorRecordActions[action.type]) {
        return {
            ...state,
            vendorRecord: vendorRecord(state.vendorRecord, action),
        };
    }

    if (VendorDetailActions[action.type]) {
        return {
            ...state,
            vendorDetail: vendorDetail(state.vendorDetail, action),
        };
    }

    return state;
}

export const vendorState: Selector<IStoreState, IVendorState> = (state: IStoreState): IVendorState => state.vendor;
