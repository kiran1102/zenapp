import { AnyAction } from "redux";
import {
    createSelector,
    Selector,
} from "reselect";

// Interfaces
import { IStoreState } from "interfaces/redux.interface";
import {
    IVendorDetailState,
    IVendorCatalogInformation,
} from "modules/vendor/shared/interfaces/vendorpedia-exchange.interface";

// Enums
import { VendorDetailActions } from "modules/vendor/shared/enums/vendor-actions.enums";

export const initialVendorDetailState: IVendorDetailState = {
    isLoadingVendorDetail: false,
    data: null,
};

// tslint:disable-next-line:cyclomatic-complexity
export function vendorDetail(state: IVendorDetailState = initialVendorDetailState, action: AnyAction): IVendorDetailState {
    switch (action.type) {
        case VendorDetailActions.FETCHING_VENDOR_DETAIL:
            return {
                ...state,
                isLoadingVendorDetail: action.payload,
           };
        case VendorDetailActions.RESET_VENDOR_DETAIL:
           return initialVendorDetailState;
        case VendorDetailActions.UPDATE_VENDOR_DETAIL:
            return {
                ...state,
                data: action.payload,
                isLoadingVendorDetail: false,
            };
        default:
            return state;
    }
}

export const vendorDetailState: Selector<IStoreState, IVendorDetailState> = (state: IStoreState): IVendorDetailState => state.vendor.vendorDetail;
export const isLoadingVendorDetail = createSelector(vendorDetailState, (state: IVendorDetailState): boolean => state.isLoadingVendorDetail);
export const getVendorDetail = createSelector(vendorDetailState, (state: IVendorDetailState): IVendorCatalogInformation => state.data);
