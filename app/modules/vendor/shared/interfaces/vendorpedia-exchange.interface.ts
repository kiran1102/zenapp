import { IPageableOf } from "interfaces/pagination.interface";
import { IAttributeOptionV2 } from "interfaces/inventory.interface";
import {
    IKeyValue,
    IStringMap,
    IValueId,
    INameId,
} from "interfaces/generic.interface";
import { ICardImage } from "@onetrust/vitreus";

export interface IVendorRecordOption {
    data: IVendorRecordId;
}

export interface IVendorRecordId {
    id: string;
}

export interface INewVendorRecord {
    name: string;
    organization: IStringMap<string>;
    type: string;
    attributes: IVendorAttributes;
}

export interface IVendorpediaServiceAttr {
    vendorpediaServiceId: string;
    vendorpediaServiceName: string;
}

export interface IVendorOnboardingRequest {
    name: string;
    orgGroupId: string;
    vendorCatalogId: string;
    externalId: string;
    attributes: IVendorAttributes;
    services: IVendorpediaServiceAttr[];
}

export interface IVendorRecordV2 {
    [key: string]: string | string[] | Date | IAttributeOptionV2 | IAttributeOptionV2[];
}

export interface IVendorRecordState {
    isLoadingVendorRecord: boolean;
    data: IPageableOf<IVendorCatalogInformation>;
}

export interface IVendorDetailState {
    isLoadingVendorDetail: boolean;
    data: IVendorCatalogInformation;
}

export interface IVendorCatalogInformation {
    attributes: IVendorAttributes;
    externalId: string;
    iconLink: string;
    id: string;
    name: string;
    relatedVendors: string;
    status: "ACTIVE" | "INACTIVE";
    website: string;
    vendorVersion: number;
    disableAdd?: boolean;
    hasServices: boolean;
    inventoryId?: string;
}

export interface IVendorCatalogCard extends IVendorCatalogInformation {
    disableAdd: boolean;
}

export interface IVendorAttributes {
    contactEmail?: IStringMap<string>;
    contactPhoneNumber: Array<IStringMap<string>>;
    dataCollected: Array<IStringMap<string>>;
    euUsPrivacyShieldStatus: Array<IStringMap<string>>;
    nextEuUsCertificationDate: Array<IStringMap<string>>;
    originalEuUsCertificationDate: Array<IStringMap<string>>;
    primaryContact: Array<IStringMap<string>>;
    privacyShieldNoticeLink: Array<IStringMap<string>>;
    productService: Array<IStringMap<string>>;
    purposeOfDataCollection: Array<IStringMap<string>>;
    vendorContact: Array<IStringMap<string>>;
    privacyShieldCoveredEntities: Array<IStringMap<string>>;
}

export interface IInventoryVendorNames {
    id: string;
    name: string;
    organization: IKeyValue<string> | IValueId<string>;
    type: IKeyValue<string>;
}

export interface IInventoryInformation {
    id?: string;
    inventoryId?: string;
    inventoryName: string;
    inventoryType: "ASSETS" | "PROCESSING_ACTIVITIES" | "VENDORS";
    orgGroupId: string;
    orgGroupName?: string;
}

export interface IVendorInformation {
    attributes?: IStringMap<Array<{ value: string }>>;
    certificates?: string[];
    externalId: string;
    name: string;
    orgGroupId: string;
    serviceNames?: string[];
    services: IStringMap<IInventoryInformation>;
    subprocessorNames?: string[];
    subprocessors: IStringMap<IInventoryInformation>;
}

export interface ICertificationsList {
    iconLink: string;
    name: string;
    type?: string;
    serviceId?: string;
}

export interface IServiceForSelectedVendor {
    canEdit: boolean;
    id: string;
    inbound: boolean;
    inventoryType: number;
    location: string;
    name: string;
    organization: string;
    relation: string;
}

export interface IVendorSubprocessors {
    country: string;
    id: string;
    name: string;
    type: string;
    vendorCatalogId: string;
}

export interface IVendorpediaService {
    externalId: string;
    id: string;
    name: string;
    vendorCatalogId: string;
}

export interface IVendorpediaServiceSelection extends IVendorpediaService {
    selected: boolean;
}

export interface IVendorpediaExtendedDetail {
    attributes: IStringMap<Array<IStringMap<string>>>;
    certificates: IStringMap<IVendorpediaService[]>;
    externalId: string;
    iconLink: string;
    id: string;
    name: string;
    services: IVendorpediaService[];
    status: "ACTIVE" | "INACTIVE";
    subprocessors: IVendorSubprocessors[];
    vendorVersion: number;
    website: string;
}

export interface IFetchDetailsParams {
    type?: number;
    page: number;
    size: number;
    sort?: string;
}

export interface IVendorpediaServiceMenuItem extends INameId<string> {
    selected: boolean;
}

export interface IVendorCatalogCertificateInformation {
    frameworkName: string;
    certificateUrl: string;
    certificationDate: string;
    expirationDate: string;
    certificationVerificationLevel: string;
    sourceUrl?: string;
    scopeStatement: string;
}

export interface IVendorCatalogCertificateCard  extends IVendorCatalogCertificateInformation {
    cardImage: ICardImage;
}

export interface ISuggestNewVendor {
    name: string;
    services: string;
    url: string;
}
