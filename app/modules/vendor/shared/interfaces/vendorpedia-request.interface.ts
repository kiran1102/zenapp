import { ITableColumnConfig } from "interfaces/table-config.interface";
import { IPageableMeta } from "interfaces/pagination.interface";

export interface IVPRequestTable {
    rows: IVPRequestInformation[];
    columns: ITableColumnConfig[];
    metaData?: IPageableMeta;
}

export interface IVPRequestInformation {
    createdDate: string;
    exchangeRequestQueueId: string;
    lastModifiedDate: string;
    requestId: number | string;
    serviceName: string;
    serviceId?: string;
    status: string;
    vendorContactEmail: string;
    vendorContactName: string;
    vendorId: string;
    vendorName: string;
    vendorOrgGroupId: string;
}
