export interface IExpiringContractsResponse {
    data: IExpiringContractsResponseData;
}

export interface IExpiringContractsResponseData {
    "NoExpiry": number;
    "PastDue": number;
    "Next30Days": number;
    "Next60Days": number;
    "Next90Days": number;
    "Next180Days": number;
    "Beyond180Days": number;
}

export interface IAssessmentsByStatusResponse {
    assessments: IAssessmentByStatus[];
    inventoryTypeId: number;
    orgGroupId: string;
    totalCount: number;
}

export interface IAssessmentByStatus {
    count: number;
    status: string;
}

export interface IGraphData {
    values: IGraphDataElement[];
}

export interface IGraphDataElement {
    id: string;
    label: string;
    value: number;
    backgroundColor?: string;
    order?: number;
}

export interface IAssessmentsByStatusTerms {
    total: string;
}
