import { IAttachmentFileResponse } from "interfaces/attachment-file.interface";
import { ITableColumnConfig } from "interfaces/table-config.interface";
import { IPageableMeta } from "interfaces/pagination.interface";

export interface IContractAttachmentsTable {
    rows: IAttachmentFileResponse[];
    columns?: ITableColumnConfig[];
    metaData?: IPageableMeta;
}
