import { ITableColumnConfig } from "interfaces/table-config.interface";
import { IPageableMeta } from "interfaces/pagination.interface";
import { IControlInformation } from "controls/shared/interfaces/controls.interface";
import { IFrameworkListInformation } from "controls/shared/interfaces/controls-library.interface";
import { INameId } from "interfaces/generic.interface";

export interface IInventoryControlsTable {
    rows: IVendorControlDetailInformation[];
    columns?: IVendorLibraryTableColumn[];
    metaData?: IPageableMeta;
}

export interface IVendorLibraryTableColumn extends ITableColumnConfig {
    isChildObject?: boolean;
    childObjectRef?: string;
    filterKey?: string;
}

export interface IVendorControlDetailInformation {
    id: string;
    control: IControlInformation;
    inventory: INameId<string>;
    note?: string;
    maturityId?: string;
    maturityName?: string;
    maturityNameKey?: string;
    status: string;
}

export interface IVendorControlDetailData {
    data: IVendorControlDetailInformation;
}

export interface IFrameworkControlInformation extends IFrameworkListInformation {
    controls: IControlInformation[];
}

export interface IVendorControlAddRequest {
    controlIds: string[];
    orgGroupId: string;
    vendorName: string;
}

export interface IVendorControlMaturityInformation {
    id: string;
    name: string;
    nameKey: string;
}

export interface IFrameworkCertificateInformation {
    frameworkName: string;
    certificateUrl?: string;
    certificationDate?: string;
    expirationDate?: string;
    sourceUrl?: string;
    certificationVerificationLevel?: string;
}
