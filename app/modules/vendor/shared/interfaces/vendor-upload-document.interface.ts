import { IPublishedWorkflow } from "interfaces/workflows.interface";
import {
    IValueId,
    IStringMap,
    ILabelValue,
} from "interfaces/generic.interface";
import { ICategoryInformation } from "controls/shared/interfaces/controls.interface";
import { IInventoryMetadata } from "modules/vendor/engagements/shared/engagement.interface";

export interface IVendorDocDetails {
    dateCreated: string;
    dateApproved: string;
    dateExpiration: string;
}

export interface IContractTypeResponse {
    id: string;
    name: string;
    nameKey: string;
    seeded: boolean;
}

export interface ICurrenciesResponse {
    code: string;
    description: string;
}

export interface IContractDetails {
    agreementCreatedDate: string;
    approvedDate: string;
    approvers: string;
    attachments?: IContractAttachment[];
    contractOwner: string;
    contractOwnerEmail: string;
    contractOwnerId: string;
    contractTypeId: string;
    contractTypeName: string;
    contractTypeNameKey?: string;
    cost: number;
    currency: string;
    currencyOption?: ICurrenciesResponse;
    expirationDate: string;
    id?: string;
    links?: any[];
    name: string;
    note: string;
    orgGroupId: string;
    status: string;
    url?: string;
    vendorContact: string;
    vendorId?: string;
    vendorName?: string;
}

export interface IContractUpdateRequest {
    name: string;
    status: string;
    agreementCreatedDate: string;
    contractTypeId: string;
    contractTypeName: string;
    contractTypeNameKey: string;
    approvedDate: string;
    expirationDate: string;
    contractOwnerId: string;
    contractOwnerEmail: string;
    vendorContact: string;
    approvers: string;
    note: string;
    cost: number;
    currency: string;
    links: any[];
    url?: string;
}

export interface IContractAttachment {
    id?: string;
    attachmentId: string;
    fileName: string;
    attachmentDescription: string;
}

export interface ICreateData {
    label?: string;
    type: string;
    name: string;
    isRequired: boolean;
    value?: string;
    options?: IPublishedWorkflow[] | ICategoryInformation[] | Array<ILabelValue<string>> | ICurrenciesResponse[];
    maxLength?: number;
    [key: string]: string | boolean | number | IPublishedWorkflow[] | ICategoryInformation[] | ICategoryInformation | Array<ILabelValue<string>> | ICurrenciesResponse[] | IInventoryMetadata;
}

export interface IAttributeOptions {
    id: string;
    value: string;
    valueKey?: string;
}

export interface IRelatedListOption {
    id: string;
    name: string;
    label: string;
    organization: IValueId<string>;
    location?: IAttributeOptions;
    type?: IAttributeOptions;
}

export interface IRelatedListOptionData {
    options: IRelatedListOption[];
    type: number;
}

export interface IRelatedAssetsPA {
    inventoryType: string;
    inventoryName: string;
    inventoryId: string;
    orgGroupId: string;
    attributes?: IStringMap<IAttributeOptions>;
}
