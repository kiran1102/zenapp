import {
    IVendorRecordState,
    IVendorDetailState,
} from "modules/vendor/shared/interfaces/vendorpedia-exchange.interface";

export interface IVendorState {
    vendorRecord: IVendorRecordState;
    vendorDetail: IVendorDetailState;
}
