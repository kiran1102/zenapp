import { PathItem } from "@onetrust/vitreus";

export interface IVendorPath {
    pathItems: PathItem[];
    activeIndex: number;
}
