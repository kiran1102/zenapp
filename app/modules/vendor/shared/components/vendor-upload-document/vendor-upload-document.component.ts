// 3rd party
import {
    Component,
    Inject,
    OnInit,
    OnDestroy,
} from "@angular/core";
import { StateService } from "@uirouter/core";

import {
    filter,
    isArray,
    each,
    replace,
    forEach,
    isNull,
} from "lodash";

// Redux
import { StoreToken } from "tokens/redux-store.token";
import { getCurrentOrgList } from "oneRedux/reducers/org.reducer";
import observableFromStore from "oneRedux/utils/observable-from-store";
import { getRecord } from "oneRedux/reducers/inventory-record.reducer";
import { getCurrentOrgId } from "oneRedux/reducers/org.reducer";

// Rxjs
import { Subject } from "rxjs";
import {
    startWith,
    takeUntil,
} from "rxjs/operators";

// Services
import { VendorApiService } from "modules/vendor/services/api/vendor-api.service";
import { ContractService } from "contracts/shared/services/contract.service";
import { TimeStamp } from "oneServices/time-stamp.service";
import { InventoryService } from "inventorySharedModule/shared/services/inventory-api.service";
import { ContractsApiService } from "modules/vendor/contracts/shared/services/contracts-api.service";

// Interfaces
import { IAttachmentFile } from "interfaces/attachment-file.interface";
import { IInventoryRecordResponseV2 } from "interfaces/inventory.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";
import {
    IContractTypeResponse,
    IContractDetails,
    ICurrenciesResponse,
    IRelatedListOptionData,
    IRelatedAssetsPA,
    IContractAttachment,
} from "modules/vendor/shared/interfaces/vendor-upload-document.interface";
import { InventorySchemaIds } from "constants/inventory-config.constant";
import { IFileInput } from "interfaces/sub-tasks.interface";
import { IDateTimeOutputModel } from "@onetrust/vitreus";
import { IOtOrgUserOutput } from "sharedModules/components/ot-org-user/ot-org-user.interface";
import {
    IStore,
    IStoreState,
} from "interfaces/redux.interface";
import {
    IInventoryDetails,
    IInventoryRecordV2,
    IRelatedListOption,
} from "interfaces/inventory.interface";
import { IOrganization } from "interfaces/org.interface";

// Constants / Enums
import { InventoryDetailsTabs } from "constants/inventory.constant";
import { InventoryTableIds } from "enums/inventory.enum";
import { OrgUserTraversal } from "enums/org-user-dropdown.enum";
import { ILabelValue } from "interfaces/generic.interface";
import {
    StatusResponseValues,
    StatusTranslationKeys,
} from "modules/vendor/shared/constants/vendor-documents.constants";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";
import { PermitPipe } from "modules/pipes/permit.pipe";

@Component({
    selector: "vendor-upload-document",
    templateUrl: "./vendor-upload-document.component.html",
})
export class VendorUploadDocumentComponent implements OnInit, OnDestroy {

    vendorContractInventoryLink = this.permitPipe.transform("VendorContractInventoryLink");
    vRMContractMultipleAttachments = this.permitPipe.transform("VRMContractMultipleAttachments");
    inventoryTableIds = InventoryTableIds;
    selectedAssets: IRelatedAssetsPA[] = [];
    selectedPA: IRelatedAssetsPA[] = [];
    selectedVendors: IRelatedAssetsPA[] = [];
    orgUserError: boolean;
    orgUserTraversal = OrgUserTraversal;
    selectedApproversSigners: string;
    selectedOwner: string;
    selectedOwnerId: string;
    selectedOwnerEmail: string;
    selectedContact: string;
    selectedApprovers: string;
    vendorId: string;
    selectedTypeName: string;
    selectedTypeId: string;
    requestId: string;
    selectedCurrencyCode: string;
    toggleButtonIcon: string;
    dateCreatedValue: string;
    dateApprovedValue: string;
    dateExpirationValue: string;
    enteredNotes: string;
    selectedCost: number;
    vendorName: string;
    statusList: Array<ILabelValue<string>>;
    selectedStatus: ILabelValue<string>;
    currencyList: ICurrenciesResponse[];
    selectedCurrency: ICurrenciesResponse;
    typeList: IContractTypeResponse[];
    selectedType: IContractTypeResponse;
    attachments: IAttachmentFile[] = [];
    contractName = "";
    contractUrl = "";
    maxSize = 64;
    format = this.timeStamp.getDatePickerDateFormat();
    isLoading = true;
    isExpanded: boolean;
    setReminderEnabled = false;
    canAdd = false;
    hideAutoRenew = false;
    allowedFileExtensions = ["csv", "doc", "docx", "jpg", "jpeg", "mpp", "msg", "pdf", "png", "ppt", "pptx", "txt", "vsd", "vsdx", "xls", "xlsx"];
    selectedOrganization: IOrganization;
    organizations: IOrganization[];
    destroy$ = new Subject();
    record: IInventoryRecordV2;
    orgGroupId: string;
    dateCreatedModel: string;
    dateApprovedModel: string;
    dateExpirationModel: string;
    selectedAssetsModel: IRelatedListOption[] = [];
    selectedPAModel: IRelatedListOption[] = [];
    selectedVendorsModel: IRelatedListOption[] = [];

    private attachmentResponse: IContractAttachment[];

    constructor(
        @Inject(StoreToken) private store: IStore,
        private stateService: StateService,
        private vendorApi: VendorApiService,
        private inventoryApiService: InventoryService,
        private contractService: ContractService,
        private contractsApiService: ContractsApiService,
        private timeStamp: TimeStamp,
        private translatePipe: TranslatePipe,
        private permitPipe: PermitPipe,
    ) { }

    ngOnInit(): void {
        this.contractService.contractStatuses$.pipe(
            takeUntil(this.destroy$),
        ).subscribe((statuses: Array<ILabelValue<string>>) => {
            if (statuses) {
                this.statusList = statuses;
                this.isLoading = false;
            }
        });
        observableFromStore(this.store).pipe(
            startWith(this.store.getState()),
            takeUntil(this.destroy$),
        ).subscribe((state: IStoreState) => this.componentWillReceiveState(state));
        this.contractService.retrieveContractStatuses();
        this.vendorId = this.stateService.params.recordId;
        this.organizations = getCurrentOrgList(this.store.getState());
        this.selectedOrganization = this.organizations[0];
        if (this.record && this.record.id === this.stateService.params.recordId) {
            this.vendorName = this.record.name as string;
        } else {
            this.getVendorName();
        }
        this.getContractTypes();
        this.getAllCurrencies();
        this.toggleButtonIcon = this.isExpanded ? "ot ot-caret-down" : "fa fa-caret-right";
    }

    ngOnDestroy() {
        this.destroy$.next();
        this.destroy$.complete();
    }

    onHeaderRouteClick(route: { path: string, params: any }) {
        this.stateService.go(route.path, { Id: InventoryTableIds.Vendors, page: null, size: null });
    }

    toggleForm(): void {
        this.isExpanded = !this.isExpanded;
        this.toggleButtonIcon = this.isExpanded ? "ot ot-caret-down" : "fa fa-caret-right";
    }

    toggleSetReminder(): void {
        this.setReminderEnabled = !this.setReminderEnabled;
    }

    getDocument(files: File[]) {
        if (files.length) {
            each(files, (file: File) => {
                if (file.size <= this.maxSize * 1024 * 1024) {
                    const reader: FileReader = new FileReader();
                    const fileObj: IFileInput = {
                        Data: "",
                        FileName: this.getFileName(file.name),
                        Extension: this.getFileExtension(file.name),
                        Blob: file,
                    };
                    reader.readAsDataURL(file);
                    reader.onload = () => {
                        fileObj.Data = reader.result.toString();
                        if (fileObj.Data.length) {
                            this.attachments.push(fileObj);
                            this.validateSubmit();
                        }
                    };
                }
            });
        }
    }

    removeDocument($event) {
        this.attachments.splice($event, 1);
        this.validateSubmit();
    }

    getFileExtension(fileName: string): string {
        // Return only file extension
        return fileName.split(".").pop() || "";
    }

    getFileName(fileName: string): string {
        const ext = this.getFileExtension(fileName);
        // Return file name without extension
        return replace(fileName, `.${ext}`, "");
    }

    uploadDocument() {
        if (!this.canAdd) return;
        this.isLoading = true;
        const promiseArr: Array<ng.IPromise<IProtocolResponse<IAttachmentFile>>> = [];
        forEach(this.attachments, (file: IAttachmentFile) => {
            const fileData: IAttachmentFile = {
                Comments: file.Comments,
                FileName: `${file.FileName}.${file.Extension}`,
                Extension: file.Extension,
                Blob: file.Blob,
                IsInternal: true,
                Name: file.FileName,
                RequestId: this.requestId,
                AttachmentRefId: file.Id,
            };
            promiseArr.push(this.vendorApi.uploadDocument(fileData));
        });
        if (!promiseArr.length) {
            this.addUploadedDocument();
        } else {
            Promise.all(promiseArr)
                .then((responses: Array<IProtocolResponse<IAttachmentFile>>): void => {
                    this.attachmentResponse = [];
                    if (isArray(responses) && responses.length) {
                        const failedResponses: Array<IProtocolResponse<IAttachmentFile>> = filter(responses, {
                            result: false,
                        });
                        if (!failedResponses.length) {
                            forEach(
                                responses,
                                (res: IProtocolResponse<IAttachmentFile>): void => {
                                    const attachmentObjectForComment: IContractAttachment = {
                                        attachmentId: res.data.Id,
                                        fileName: res.data.FileName,
                                        attachmentDescription: res.data.Comments,
                                    };
                                    this.attachmentResponse.push(
                                        attachmentObjectForComment,
                                    );
                                },
                            );
                            this.addUploadedDocument();
                        } else {
                            this.isLoading = false;
                        }
                    }
                });
        }
    }

    selectDateCreated(dateValue: IDateTimeOutputModel): void {
        this.dateCreatedValue = dateValue.dateTime ? this.timeStamp.formatDateToIso({ day: dateValue.jsdate.getDate(), month: dateValue.jsdate.getMonth() + 1, year: dateValue.jsdate.getFullYear() }) : null;
        this.dateCreatedModel = dateValue.date;
        this.validateSubmit();
    }

    selectDateApproved(dateValue: IDateTimeOutputModel): void {
        this.dateApprovedValue = dateValue.dateTime ? this.timeStamp.formatDateToIso({ day: dateValue.jsdate.getDate(), month: dateValue.jsdate.getMonth() + 1, year: dateValue.jsdate.getFullYear() }) : null;
        this.dateApprovedModel = dateValue.date;
    }

    selectDateExpiration(dateValue: IDateTimeOutputModel): void {
        this.dateExpirationValue = dateValue.dateTime ? this.timeStamp.formatDateToIso({ day: dateValue.jsdate.getDate(), month: dateValue.jsdate.getMonth() + 1, year: dateValue.jsdate.getFullYear() }) : null;
        this.dateExpirationModel = dateValue.date;
    }

    updateSelectedType(type: IContractTypeResponse) {
        this.selectedType = type;
        this.selectedTypeId = type.id;
        this.selectedTypeName = type.name;
        this.requestId = type.id;
        this.validateSubmit();
    }

    updateSelectedCurrency(currency: ICurrenciesResponse) {
        this.selectedCurrency = currency;
        this.selectedCurrencyCode = currency.code;
    }

    updateStatus(status: ILabelValue<string>) {
        this.selectedStatus = status;
        this.validateSubmit();
    }

    selectContractOwner(option: IOtOrgUserOutput): void {
        // we have to send null to backend if we are removing a selected owner.
        if (!isNull(option.selection) && option.selection.Id) {
            // Since the component is returning Id and FullName same for External/Invited user Id
            if (option.selection.Id === option.selection.FullName) {
                option.selection.Id = null;
            }
            this.selectedOwnerEmail = option.selection.FullName;
            this.selectedOwner = option.selection.Id || option.selection.FullName;
            this.selectedOwnerId = option.selection.Id;
        } else {
            this.selectedOwner = null;
            this.selectedOwnerEmail = null;
            this.selectedOwnerId = null;
        }
    }

    selectedInventoryList(optionsData: IRelatedListOptionData): void {
        const { options, type } = optionsData;
        if (options) {
            if (type === this.inventoryTableIds.Assets) {
                this.selectedAssets = [];
                this.selectedAssetsModel = options;
                options.forEach((option) => {
                    this.selectedAssets.push({
                        inventoryType: "ASSETS",
                        inventoryName: option.name,
                        inventoryId: option.id,
                        orgGroupId: option.organization.id,
                        attributes: {
                            location: {
                                id: option.location.id,
                                value: option.location.value,
                                valueKey: option.location.valueKey || "",
                            },
                        },
                    });
                });
            } else if (type === this.inventoryTableIds.Processes) {
                this.selectedPA = [];
                this.selectedPAModel = options;
                options.forEach((option) => {
                    this.selectedPA.push({
                        inventoryType: "PROCESSING_ACTIVITIES",
                        inventoryName: option.name,
                        inventoryId: option.id,
                        orgGroupId: option.organization.id,
                    });
                });
            } else if (type === this.inventoryTableIds.Vendors) {
                this.selectedVendors = [];
                this.selectedVendorsModel = options;
                options.forEach((option) => {
                    this.selectedVendors.push({
                        inventoryType: "VENDORS",
                        inventoryName: option.name,
                        inventoryId: option.id,
                        orgGroupId: option.organization.id,
                        attributes: {
                            type: {
                                id: option.type.id,
                                value: option.type.value,
                                valueKey: option.type.valueKey || "",
                            },
                        },
                    });
                });
            }
        }
    }

    selectApproversSigners(option: IOtOrgUserOutput) {
        // we have to send null to backend if we are removing a selected owner.
        if (!isNull(option.selection) && option.selection.Id) {
            // Since the component is returning Id and FullName same for External/Invited user Id
            if (option.selection.Id === option.selection.FullName) {
                option.selection.Id = null;
            }
            this.selectedApproversSigners = option.selection.FullName;
        } else {
            this.selectedApproversSigners = null;
        }
    }

    selectCostValue(event) {
        this.selectedCost = event;
    }

    selectVendorContact(event) {
        this.selectedContact = event;
    }

    selectApprovers(event) {
        this.selectedApprovers = event;
    }

    handleEnteredNotes(note: string) {
        this.enteredNotes = note;
    }

    goToDocuments() {
        const launchVendorParams = {
            inventoryId: this.inventoryTableIds.Vendors,
            recordId: this.vendorId,
            tabId: InventoryDetailsTabs.Documents,
        };
        this.stateService.go("zen.app.pia.module.vendor.inventory.documents", launchVendorParams);
    }

    contractNameChange(name) {
        this.contractName = name;
        this.validateSubmit();
    }

    contractUrlChange(name) {
        this.contractUrl = name;
        this.validateSubmit();
    }

    private componentWillReceiveState(state) {
        this.record = { ...getRecord(state) };
        this.orgGroupId = getCurrentOrgId(state);
    }

    private getVendorName() {
        this.inventoryApiService.getRecordV2(InventorySchemaIds.Vendors, this.vendorId).then(
            (res: IProtocolResponse<IInventoryRecordResponseV2>) => {
                if (res.result) {
                    this.vendorName = res.data.data.name as string;
                }
            });
    }

    private getAllCurrencies(): ng.IPromise<IProtocolResponse<ICurrenciesResponse[]>> {
        return this.contractsApiService.fetchAllCurrencies().then(
            (response: IProtocolResponse<ICurrenciesResponse[]>): IProtocolResponse<ICurrenciesResponse[]> => {
                if (response.result) {
                    this.currencyList = response.data;
                    return response;
                }
            });
    }

    private getContractTypes(): ng.IPromise<IProtocolResponse<IContractTypeResponse[]>> {
        return this.contractsApiService.fetchAllContractTypes().then(
            (response: IProtocolResponse<IContractTypeResponse[]>): IProtocolResponse<IContractTypeResponse[]> => {
                if (response.result) {
                    this.typeList = response.data;
                    forEach(this.typeList, (contractType: IContractTypeResponse) => {
                        contractType.name = contractType.nameKey ? this.translatePipe.transform(contractType.nameKey) : contractType.name;
                    });
                    return response;
                }
            });
    }

    private addUploadedDocument(): void {
        const launchVendorParams = {
            inventoryId: this.inventoryTableIds.Vendors,
            recordId: this.vendorId,
            tabId: InventoryDetailsTabs.Documents,
        };
        const newContract: IContractDetails = {
            orgGroupId: this.orgGroupId,
            name: this.contractName,
            vendorId: this.vendorId,
            vendorName: this.vendorName,
            status: this.selectedStatus.value,
            agreementCreatedDate: this.dateCreatedValue,
            expirationDate: this.dateExpirationValue,
            approvedDate: this.dateApprovedValue,
            attachments: this.attachmentResponse || [],
            contractOwner: this.selectedOwner,
            contractOwnerId: this.selectedOwnerId,
            contractOwnerEmail: this.selectedOwner,
            contractTypeId: this.selectedTypeId,
            contractTypeName: this.selectedTypeName,
            url: this.contractUrl,
            vendorContact: this.selectedContact,
            approvers: this.selectedApprovers,
            note: this.enteredNotes,
            cost: this.selectedCost,
            currency: this.selectedCurrencyCode,
            links: [...this.selectedAssets, ...this.selectedPA, ...this.selectedVendors],
        };
        this.contractService.createNewContract(newContract).then((response: IProtocolResponse<string>): void => {
            if (response.result) {
                this.stateService.go("zen.app.pia.module.vendor.inventory.documents", launchVendorParams);
            } else {
                this.isLoading = false;
            }
        });
    }

    private validateSubmit(): void {
        this.canAdd = Boolean(
            this.selectedType && this.contractName
            && this.dateCreatedValue && this.selectedStatus,
        );
    }
}
