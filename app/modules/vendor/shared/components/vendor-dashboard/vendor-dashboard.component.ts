import {
    Component,
    OnInit,
} from "@angular/core";

// Services
import { VendorDashboardService } from "vendorModule/services/shared/vendor-dashboard.service.ts";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

// Interfaces
import {
    IAssessmentByStatus,
    IAssessmentsByStatusTerms,
    IExpiringContractsResponseData,
    IGraphData,
} from "vendorModule/shared/interfaces/vendor-dashboard.interface";

// Constants
import {
    ContractStatus,
    AssessmentStatus,
} from "vendorModule/shared/constants/vendor-dashboard.constants";

@Component({
    selector: "vendor-dashboard",
    templateUrl: "./vendor-dashboard.component.html",
})
export class VendorDashboardComponent implements OnInit {
    expiringContracts: IGraphData;
    noContracts: boolean;
    assessmentsByStatus: IGraphData;
    noAssessments: boolean;

    height = 300;
    ready: boolean;
    terms: IAssessmentsByStatusTerms;

    constructor(
        private translatePipe: TranslatePipe,
        private vendorDashboardService: VendorDashboardService,
    ) { }

    ngOnInit() {
        this.terms = {
            total: this.translatePipe.transform("Total"),
        };

        this.vendorDashboardService.fetchExpiringContracts().subscribe((res) => {
            if (res.result) return this.expiringContracts = this.formatExpiringContracts(res.data.data);
            this.noContracts = true;
        });

        this.vendorDashboardService.fetchAssessmentsByStatus().subscribe((res) => {
            if (res.result) return this.assessmentsByStatus = this.formatAssessments(res.data.assessments);
            this.noAssessments = true;
        });
    }

    formatExpiringContracts(res: IExpiringContractsResponseData): IGraphData {
        let hasValue;
        const values = Object.keys(res)
            .map((item) => {
                const days: number = ContractStatus[item].days;
                const value = res[item];
                if (value > 0) hasValue = true;
                return {
                    id: item,
                    label: this.translatePipe.transform(
                        ContractStatus[item].label,
                        days ? {Days: days} : null,
                    ),
                    value,
                    backgroundColor: ContractStatus[item].color,
                    days,
                };
            });

        this.noContracts = !hasValue;
        return { values };
    }
    // TODO: debug res type
    formatAssessments(res: any): IGraphData {
        let hasValue;
        const values = res
            .map((item) => {
                const value = res[item];
                if (item.count > 0) hasValue = true;
                return {
                    id: item.status,
                    label: this.translatePipe.transform(AssessmentStatus[item.status].label),
                    value: item.count,
                    backgroundColor: AssessmentStatus[item.status].color,
                    order: AssessmentStatus[item.status].order,
                };
            }).sort((a, b) => a.order - b.order);

        this.noAssessments = !hasValue;
        return { values };
    }
}
