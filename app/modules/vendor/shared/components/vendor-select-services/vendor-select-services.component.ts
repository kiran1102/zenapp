// Core
import { Component } from "@angular/core";
import { StateService } from "@uirouter/core";

// 3rd Party
import {
    BehaviorSubject,
    Subject,
    combineLatest,
} from "rxjs";
import { takeUntil, filter } from "rxjs/operators";
import { find } from "lodash";

// Interfaces
import {
    IVendorCatalogInformation,
    IVendorpediaService,
    IVendorpediaServiceSelection,
} from "modules/vendor/shared/interfaces/vendorpedia-exchange.interface";

// Services
import { VendorDetailActionService } from "modules/vendor/services/actions/vendor-detail-action.service";
import { VendorInformationService } from "modules/vendor/services/shared/vendor-information.service";

// Constants / Enums
import { InventoryTableIds } from "enums/inventory.enum";

@Component({
    selector: "vendor-select-services",
    templateUrl: "./vendor-select-services.component.html",
})
export class VendorSelectServicesComponent {

    loadingVendorDetail$ = new BehaviorSubject<boolean> (true);
    selectedServices: IVendorpediaService[] = [];
    vendorId: string;
    vendor: IVendorCatalogInformation;
    vendorServices: IVendorpediaServiceSelection[] = [];

    private destroy$ = new Subject();

    constructor(
        private stateService: StateService,
        private vendorDetailActionService: VendorDetailActionService,
        private vendorInformationService: VendorInformationService,
    ) { }

    ngOnInit() {
        combineLatest(
            this.vendorDetailActionService.vendorDetailModel$,
            this.vendorInformationService.vendorServices$,
        ).pipe(
            takeUntil(this.destroy$),
            filter(([ vendor, services ]) => !!(vendor && services)),
        ).subscribe(([ vendor, services ]) => {
            if (vendor && services) {
                this.vendor = vendor;
                this.vendorServices = services
                    .map((s) => this.addSelectedProperty(s))
                    .sort((a, b) => a.name.localeCompare(b.name));
                this.selectedServices = this.vendorServices.filter((s) => s.selected);
                this.loadingVendorDetail$.next(false);
            }
        });

        this.vendorId = this.stateService.params.vendorId || null;
        if (this.vendorId) {
            this.vendorDetailActionService.fetchVendorById(this.vendorId);
            this.vendorInformationService.fetchServicesByVendorId(this.vendorId);
        }
    }

    ngOnDestroy(): void {
        this.destroy$.next();
        this.destroy$.complete();
        this.vendorDetailActionService.reset();
    }

    addSelectedProperty(service: IVendorpediaService): IVendorpediaServiceSelection {
        const s: IVendorpediaServiceSelection = {...service, selected: this.isServicePreviouslySelected(service)};
        if (s.selected) this.selectedServices.push(service);
        return s;
    }

    isServicePreviouslySelected(service: IVendorpediaService): boolean {
        return this.stateService && this.stateService.params.selectedServices && Boolean(find(this.stateService.params.selectedServices, (s) => s.id === service.id));
    }

    onHeaderRouteClick(route: { path: string, params: any }) {
        this.stateService.go(route.path, { Id: InventoryTableIds.Vendors, page: null, size: null });
    }

    serviceClicked(toggleState: boolean, service: IVendorpediaService) {
        this.vendorServices = this.vendorServices.map((s) => {
            if (s.id === service.id) s.selected = toggleState;
            return s;
        });
        this.selectedServices = this.vendorServices.filter((s) => s.selected);
    }

    selectAllServices() {
        this.vendorServices = this.vendorServices.map((s) => {
            s.selected = this.selectedServices.length < this.vendorServices.length;
            return s;
        });
        this.selectedServices = this.vendorServices.filter((s) => s.selected);
    }

    cancelButtonClicked() {
        this.stateService.go("zen.app.pia.module.vendor.vendorpedia-exchange");
    }

    addButtonClicked() {
        this.stateService.go("zen.app.pia.module.vendor.information", { vendorId: this.vendorId, selectedServices: this.selectedServices });
    }
}
