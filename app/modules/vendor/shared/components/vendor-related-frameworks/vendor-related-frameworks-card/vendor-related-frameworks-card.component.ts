// Core
import {
    Component,
    Input,
    OnInit,
} from "@angular/core";

// Interfaces
import { IFrameworkCertificateInformation } from "modules/vendor/shared/interfaces/inventory-controls.interface";

// Constants
import { GetLogoNameForCert } from "modules/vendor/certificates/shared/constants/certificates.constants";

@Component({
    selector: "vendor-related-frameworks-card",
    templateUrl: "./vendor-related-frameworks-card.component.html",
})
export class VendorRelatedFrameworksCardComponent implements OnInit {

    @Input() vendorFrameworks: IFrameworkCertificateInformation;
    @Input() showCertificateDetails: boolean;
    imgSrc: string;

    ngOnInit() {
        this.imgSrc = `images/vendor/certificates/${GetLogoNameForCert(this.vendorFrameworks.frameworkName)}.png`;
    }
}
