// Core
import {
    Component,
    Input,
} from "@angular/core";

// Interfaces
import { IFrameworkCertificateInformation } from "modules/vendor/shared/interfaces/inventory-controls.interface";

// Constants
import { GetLogoNameForCert } from "modules/vendor/certificates/shared/constants/certificates.constants";

@Component({
    selector: "vendor-related-frameworks",
    templateUrl: "./vendor-related-frameworks.component.html",
})
export class VendorRelatedFrameworksComponent {

    @Input() vendorId: string;
    @Input() frameworkCertificates: IFrameworkCertificateInformation[] = [];
    @Input() showCertificateDetails: boolean;

    selectedCertificate: IFrameworkCertificateInformation;
    imgSrc: string;
    emptyData = "- - - -";

    trackByFn(index: number) {
        return index;
    }

    showDetails(certificate: IFrameworkCertificateInformation) {
        if (this.showCertificateDetails) {
            this.onCertificateClicked(certificate);
        }
    }

    showCertificates() {
        this.selectedCertificate = null;
    }

    private onCertificateClicked(certificate: IFrameworkCertificateInformation) {
        this.selectedCertificate = certificate;
        this.imgSrc = `images/vendor/certificates/${GetLogoNameForCert(certificate.frameworkName)}.png`;
    }
}
