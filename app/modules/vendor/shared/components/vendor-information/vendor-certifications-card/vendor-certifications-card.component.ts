// Core
import {
    Component,
    Input,
} from "@angular/core";

// Interfaces
import { ICertificationsList } from "modules/vendor/shared/interfaces/vendorpedia-exchange.interface";

@Component({
    selector: "vendor-certifications-card",
    templateUrl: "./vendor-certifications-card.component.html",
})
export class VendorCertificationsCardComponent {

    @Input() vendorCertifications: ICertificationsList;

}
