// Core
import { Component, Inject } from "@angular/core";
import { StateService } from "@uirouter/core";

// 3rd Party
import {
    BehaviorSubject,
    Subject,
    combineLatest,
} from "rxjs";
import { takeUntil, filter } from "rxjs/operators";
import { forIn } from "lodash";

// Redux
import { getCurrentOrgId } from "oneRedux/reducers/org.reducer";
import { IStore } from "interfaces/redux.interface";
import { StoreToken } from "tokens/redux-store.token";

// Interfaces
import { IProtocolResponse } from "interfaces/protocol.interface";
import {
    ICertificationsList,
    IVendorpediaServiceSelection,
    IVendorpediaExtendedDetail,
    IVendorSubprocessors,
    IVendorpediaService,
    IVendorCatalogInformation,
    INewVendorRecord,
    IVendorRecordOption,
    IVendorRecordV2,
    IInventoryInformation,
    IVendorInformation,
} from "modules/vendor/shared/interfaces/vendorpedia-exchange.interface";
import { ITableColumnConfig } from "interfaces/table-config.interface";
import { IStringMap } from "interfaces/generic.interface";

// Services
import { VendorDetailActionService } from "modules/vendor/services/actions/vendor-detail-action.service";
import { VendorInformationService } from "modules/vendor/services/shared/vendor-information.service";

// Constants / Enums
import { VendorInfoTableColumns } from "modules/vendor/shared/constants/vendor-information.constants";
import { InventoryTableIds } from "enums/inventory.enum";
import { InventoryDetailsTabs } from "constants/inventory.constant";
import { InventorySchemaIds } from "constants/inventory-config.constant";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

interface IVendorInfoTable {
    rows: IVendorSubprocessors[];
    columns?: ITableColumnConfig[];
}

@Component({
    selector: "vendor-information",
    templateUrl: "./vendor-information.component.html",
})
export class VendorInformationComponent {
    certificationsMap: IStringMap<IVendorpediaService[]>;
    certifications: ICertificationsList[] = [];
    isToggleCertificationsOpen = true;
    isToggleContactInfoOpen = true;
    isToggleSubprocessorsOpen = true;
    onboardingVendor$ = new Subject<boolean>();
    loadingVendorDetail$ = new BehaviorSubject<boolean>(true);
    orgGroupId = getCurrentOrgId(this.store.getState());
    rowBordered = true;
    selectedServices: IVendorpediaServiceSelection[];
    vendor: IVendorCatalogInformation;
    vendorId: string;
    vendorExtended: IVendorpediaExtendedDetail;
    vendorDetails: Array<IStringMap<string>>;
    table: IVendorInfoTable;
    subprocessors: IVendorSubprocessors[];
    selectedServiceIndex = -1;

    private destroy$ = new Subject();

    constructor(
        public translatePipe: TranslatePipe,
        private stateService: StateService,
        private vendorInformationService: VendorInformationService,
        private vendorDetailActionService: VendorDetailActionService,
        @Inject(StoreToken) private store: IStore,
    ) { }

    ngOnInit(): void {
        combineLatest(
            this.vendorDetailActionService.vendorDetailModel$,
            this.vendorInformationService.vendorExtendedDetails$,
            this.vendorInformationService.vendorSubprocessors$,
            this.vendorInformationService.vendorCertificates$,
        ).pipe(
            takeUntil(this.destroy$),
            filter(([ vendor, vendorExtended, subprocessors, certificates ]) => !!(vendor && vendorExtended && subprocessors && certificates)),
        ).subscribe(([ vendor, vendorExtended, subprocessors, certificates ]) => {
            this.vendor = vendor;
            this.vendorExtended = vendorExtended;
            this.subprocessors = subprocessors;
            this.table = this.configureTable(subprocessors);
            this.certificationsMap = certificates;
            this.loadAllCertifications();
            this.vendorDetails = this.mapVendorAttributes(vendor.attributes);
            this.loadingVendorDetail$.next(false);
        });

        // Fetch Vendor by Id
        this.vendorId = this.stateService.params.vendorId || null;
        this.selectedServices = this.stateService.params.selectedServices || [];
        if (this.vendorId && this.selectedServices) {
            this.vendorDetailActionService.fetchVendorById(this.vendorId);
            this.vendorInformationService.fetchExtendedVendorDetails(this.vendorId, this.selectedServices.map((s) => s.id));
        }
    }

    ngOnDestroy(): void {
        this.destroy$.next();
        this.destroy$.complete();
        this.vendorDetailActionService.reset();
    }

    loadAllCertifications() {
        this.clearCertifications();

        for (const serviceId in this.certificationsMap) {
            if (this.certificationsMap.hasOwnProperty(serviceId)) {
                this.loadCertifications(serviceId);
            }
        }

        this.certifications.sort((a, b) => a.name.localeCompare(b.name));
    }

    loadCertifications(serviceId) {
        this.certificationsMap[serviceId].forEach((sC) => {
            if (!this.certifications.some((cert) => cert.name === sC.name)) {
                this.certifications.push({
                    iconLink: `images/vendor_generic.svg`,
                    name: sC.name,
                    serviceId,
                });
            }
        });
    }

    selectService(index, serviceId = "") {
        this.selectedServiceIndex = index;
        if (index === -1) {
            this.loadAllCertifications();
        } else if (index > - 1 && this.certificationsMap[serviceId] !== undefined) {
            this.clearCertifications();
            this.loadCertifications(serviceId);
        }
    }

    columnTrackBy = (i: number): number => i;

    onHeaderRouteClick(route: { path: string, params: any }) {
        this.stateService.go(route.path, { Id: InventoryTableIds.Vendors, page: null, size: null });
    }

    toggleContactInfo(isToggleContactInfoOpen: boolean) {
        this.isToggleContactInfoOpen = isToggleContactInfoOpen;
    }

    toggleCertifications(isToggleCertificationsOpen: boolean) {
        this.isToggleCertificationsOpen = isToggleCertificationsOpen;
    }

    toggleSubprocessors(isToggleSubprocessorsOpen: boolean) {
        this.isToggleSubprocessorsOpen = isToggleSubprocessorsOpen;
    }

    trackByFn(index: number, certificate: ICertificationsList) {
        return index;
    }

    onCancelButtonClick() {
        this.stateService.go("zen.app.pia.module.vendor.vendorpedia-exchange");
    }

    onPreviousButtonClick() {
        this.stateService.go("zen.app.pia.module.vendor.select-services", { vendorId: this.vendorId, selectedServices: this.selectedServices });
    }

    onAddButtonClick() {
        this.onboardingVendor$.next(true);
        const payload: INewVendorRecord = {
            name: this.vendor.name,
            organization: {
                id: this.orgGroupId,
            },
            type: null,
            attributes: this.vendor.attributes,
        };
        this.vendorInformationService.addVendorRecord(InventorySchemaIds.Vendors, payload).then((response: IProtocolResponse<IVendorRecordOption>): void => {
            if (response.result) {
                const recordId = response.data.data.id;
                const updateRecord: IVendorRecordV2 = {
                    contactPhoneNumber: payload.attributes.contactPhoneNumber ? payload.attributes.contactPhoneNumber[0].value : null,
                    dataCollected: payload.attributes.dataCollected ? payload.attributes.dataCollected[0].value : null,
                    euUsPrivacyShieldStatus: [{id: null, value: payload.attributes.euUsPrivacyShieldStatus[0] ? payload.attributes.euUsPrivacyShieldStatus[0].value : null}],
                    nextEuUsCertificationDate: payload.attributes.nextEuUsCertificationDate ? payload.attributes.nextEuUsCertificationDate[0].value : null,
                    originalEuUsCertificationDate: payload.attributes.originalEuUsCertificationDate ? payload.attributes.originalEuUsCertificationDate[0].value : null,
                    primaryContact: [{value: payload.attributes.primaryContact ? payload.attributes.primaryContact[0].value : null, id: null}],
                    privacyShieldNoticeLink: payload.attributes.privacyShieldNoticeLink ? payload.attributes.privacyShieldNoticeLink[0].value : null,
                    productService: payload.attributes.productService ? payload.attributes.productService[0].value : null,
                    purposeOfDataCollection: payload.attributes.purposeOfDataCollection ? payload.attributes.purposeOfDataCollection[0].value : null,
                    vendorContact: payload.attributes.vendorContact ? payload.attributes.vendorContact[0].value : null,
                    privacyShieldCoveredEntities: payload.attributes.privacyShieldCoveredEntities ? payload.attributes.privacyShieldCoveredEntities[0].value : null,
                };
                this.vendorInformationService.updateVendorRecord(InventorySchemaIds.Vendors, recordId, updateRecord).then((res: IProtocolResponse<IVendorRecordV2[]>): void => {
                    if (res.result) {
                        this.onboardVendor(recordId);
                    } else {
                        this.onboardingVendor$.next(false);
                    }
                });
            } else {
                this.onboardingVendor$.next(false);
            }
        });
    }

    onboardVendor(vendorRecordId: string) {
        const payload: IVendorInformation = {
            externalId: this.vendor.externalId,
            name: this.vendor.name,
            orgGroupId: this.orgGroupId,
            services: this.mapVendorServices(),
            subprocessors: this.mapVendorSubprocessors(),
        };

        this.vendorInformationService.onboardVendor(vendorRecordId, payload).then((res: IProtocolResponse<IVendorInformation>) => {
            if (res.result) {
                const launchVendorParams = {
                    inventoryId: InventoryTableIds.Vendors,
                    recordId: vendorRecordId,
                    tabId: InventoryDetailsTabs.Details,
                };
                this.stateService.go("zen.app.pia.module.vendor.inventory.details", launchVendorParams);
            }
        });
    }

    private configureTable(records: IVendorSubprocessors[]): IVendorInfoTable {
        return {
            rows: records,
            columns: [
                {
                    name: this.translatePipe.transform("EntityName"),
                    cellValueKey: VendorInfoTableColumns.ENTITY_NAME,
                },
                {
                    name: this.translatePipe.transform("EntityType"),
                    cellValueKey: VendorInfoTableColumns.ENTITY_TYPE,
                },
                {
                    name: this.translatePipe.transform("Country"),
                    cellValueKey: VendorInfoTableColumns.COUNTRY,
                },
            ],
        };
    }

    private clearCertifications() {
        this.certifications = [];
    }

    private mapVendorServices(): IStringMap<IInventoryInformation> {
        return this.selectedServices.reduce((obj, item) => {
            obj[item.name] = {
                inventoryName: item.name,
                inventoryType: "PROCESSING_ACTIVITIES",
                orgGroupId: this.orgGroupId,
            };
            return obj;
        }, {});
    }

    private mapVendorSubprocessors(): IStringMap<IInventoryInformation> {
        return this.subprocessors.reduce((obj, item) => {
            obj[item.name] = {
                inventoryName: item.name,
                inventoryType: "VENDORS",
                orgGroupId: this.orgGroupId,
            };
            return obj;
        }, {});
    }

    // TODO: Rework this to include field types once they become editable
    private mapVendorAttributes(attributes) {
        const details = [];
        forIn(attributes, (value, key) => {
            const obj = { label: key, value: value[0].value };
            details.push(obj);
        });
        return details;
    }
}
