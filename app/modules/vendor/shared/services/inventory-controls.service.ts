import { Injectable, OnDestroy } from "@angular/core";

// Rxjs
import {
    BehaviorSubject,
    Observable,
    Subject,
} from "rxjs";
import { distinctUntilChanged, takeUntil } from "rxjs/operators";

// Services
import { VendorApiService } from "modules/vendor/services/api/vendor-api.service";
import { GridViewStorageService } from "sharedModules/services/helper/grid-view-storage.service";

// Interfaces
import { IProtocolResponse } from "interfaces/protocol.interface";
import {
    IPaginationFilter,
    IPageableMeta,
    IPaginatedResponse,
} from "interfaces/pagination.interface";
import { ISamlSettingsResponse } from "interfaces/setting.interface";
import {
    IVendorControlAddRequest,
    IVendorControlDetailInformation,
    IVendorControlMaturityInformation,
    IFrameworkCertificateInformation,
    IVendorControlDetailData,
} from "modules/vendor/shared/interfaces/inventory-controls.interface";
import { IFrameworkListInformation } from "controls/shared/interfaces/controls-library.interface";

// Pipes
import { DefaultPagination } from "modules/vendor/shared/constants/vendor-documents.constants";
import { GridViewStorageNames } from "sharedModules/enums/grid-view-storage.enum";

@Injectable()
export class InventoryControlsService implements OnDestroy {

    // tslint:disable:member-ordering
    private _vendorControlsFrameworkCertificates$ = new BehaviorSubject<IFrameworkCertificateInformation[]>(null);
    private _vendorControlsFrameworksLoading$ = new BehaviorSubject<boolean>(false);
    private _vendorFrameworkCertificates$ = new BehaviorSubject<IFrameworkCertificateInformation[]>(null);
    private _vendorFrameworksLoading$ = new BehaviorSubject<boolean>(false);

    vendorControls$ = new BehaviorSubject<IVendorControlDetailInformation[]>(null);
    vendorControlsMeta$ = new BehaviorSubject<IPageableMeta>(null);
    vendorControlsLoading$ = new BehaviorSubject<boolean>(false);
    controlsFrameworks$ = new BehaviorSubject<IFrameworkListInformation[]>(null);
    vendorControlsSearchName$ = new BehaviorSubject<string>(null);
    paginationParams$ = new BehaviorSubject<{ page: number, size: number, sort: { sortKey: string, sortOrder: string } }>({ ...DefaultPagination });
    control$ = new BehaviorSubject<IVendorControlDetailInformation>(null);
    controlAllMaturities$ = new BehaviorSubject<IVendorControlMaturityInformation[]>(null);
    vendorControlStatus$ = new BehaviorSubject<string[]>(null);
    selectedCertificate$ = new BehaviorSubject<IFrameworkCertificateInformation>(null);

    vendorFrameworkCertificates$ = this._vendorFrameworkCertificates$.asObservable();
    vendorFrameworksLoading$ = this._vendorFrameworksLoading$.asObservable();
    vendorControlsFrameworkCertificates$ = this._vendorControlsFrameworkCertificates$.asObservable();
    vendorControlsFrameworksLoading$ = this._vendorControlsFrameworksLoading$.asObservable();
    destroy$ = new Subject();
    storageKey = GridViewStorageNames.VENDOR_INVENTORY_LIST;

    constructor(
        private vendorApiService: VendorApiService,
        private browserStorageListService: GridViewStorageService,
    ) { }

    ngOnDestroy() {
        this.destroy$.next();
        this.destroy$.complete();
    }

    reset(): void {
        this.vendorControls$.next(null);
        this.vendorControlsMeta$.next(null);
        this.controlsFrameworks$.next(null);
        this.control$.next(null);
    }

    retrieveControlsPerVendor(vendorId, filters: IPaginationFilter[], payload?) {
        const pagination = this.paginationParams$.getValue();
        this.vendorControlsLoading$.next(true);
        this.vendorApiService.fetchVendorControls(vendorId, this.vendorControlsSearchName$.getValue(),
            filters,
            pagination,
            payload,
        ).subscribe((res: IProtocolResponse<IPaginatedResponse<IVendorControlDetailInformation>>): void => {
            if (res.result) {
                this.vendorControlsMeta$.next(res.data.meta.page);
                this.vendorControls$.next(res.data.data);
                if (!res.data.meta.page.first && res.data.data.length === 0 && res.data.meta.page.number > 0) {
                    pagination.page--;
                    this.paginationParams$.next(pagination);
                    this.retrieveControlsPerVendor(vendorId, filters, payload);
                } else {
                    this.vendorControlsLoading$.next(false);
                }
                this.paginationParams$.next({
                    page: res.data.meta.page.number,
                    size: res.data.meta.page.size,
                    sort: {
                        sortKey: res.data.meta.page.sort[0].property,
                        sortOrder: (res.data.meta.page.sort[0].direction as string).toLowerCase(),
                    },
                });
            }
        });
    }

    storePaginationDetails() {
        this.paginationParams$.pipe(
            distinctUntilChanged(),
            takeUntil(this.destroy$),
        ).subscribe((paginationMeta: { page: number, size: number, sort: { sortKey: string, sortOrder: string } }) => {
            this.browserStorageListService.saveSessionStoragePagination(this.storageKey, paginationMeta);
        });
    }

    retrieveVendorControlById(vendorControlId: string) {
        this.vendorApiService.fetchControlByVendorControlID(vendorControlId)
            .subscribe((res: IProtocolResponse<IVendorControlDetailData>) => {
                this.control$.next(res.result ? res.data.data : null);
            });

    }

    retrieveVendorCertificates(vendorId: string) {
        this._vendorFrameworksLoading$.next(true);
        this.vendorApiService.fetchCertificatesByVendorId(vendorId)
            .subscribe((res: IProtocolResponse<IFrameworkCertificateInformation[]>) => {
                if (res.result) {
                    this._vendorFrameworkCertificates$.next(res.data);
                }
                this._vendorFrameworksLoading$.next(false);
            });
    }

    retrieveVendorControlsCertificates(vendorId: string) {
        this._vendorControlsFrameworksLoading$.next(true);
        this.vendorApiService.fetchControlsCertificatesByVendorId(vendorId)
            .subscribe((res: IProtocolResponse<IFrameworkCertificateInformation[]>) => {
                if (res.result) {
                    this._vendorControlsFrameworkCertificates$.next(res.data);
                }
                this._vendorControlsFrameworksLoading$.next(false);
            });
    }

    retrieveAllMaturities() {
        this.vendorApiService.fetchAllMaturities()
            .subscribe((res: IProtocolResponse<{ data: IVendorControlMaturityInformation[] }>) => {
                if (res.result) {
                    this.controlAllMaturities$.next(res.data.data);
                }
            });
    }

    retrieveVendorControlsStatus() {
        this.vendorApiService.fetchVendorInventoryControlsStatus()
            .subscribe((res: IProtocolResponse<{ data: string[] }>) => {
                if (res.result) {
                    this.vendorControlStatus$.next(res.data.data);
                }
            });
    }

    deleteControl(vendorId, filters: IPaginationFilter[], controlId: string, payload?) {
        this.vendorApiService.deleteControlFromVendorRecord(vendorId, controlId)
            .subscribe((response: IProtocolResponse<ISamlSettingsResponse>) => {
                if (response.result) {
                    this.retrieveControlsPerVendor(vendorId, filters, payload);
                }
            });
    }

    addControlsToVendor(id: string, orgGroupId: string, inventoryName: string, controlIds: string[]): Observable<any> {
        const payload: IVendorControlAddRequest = { controlIds, orgGroupId, vendorName: inventoryName };
        return this.vendorApiService.addControlsToVendor(id, payload);
    }
}
