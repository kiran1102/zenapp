export enum VPRequestTableColumnNames {
    ID = "requestId",
    LEVEL = "assessmentLevel",
    VENDOR = "vendorName",
    SERVICE = "serviceName",
    ORGANIZATION = "vendorOrgGroupName",
    TYPE = "vendorType",
    STATUS = "status",
    DATE_REQUESTED = "createdDate",
    LAST_UPDATED = "lastModifiedDate",
}
