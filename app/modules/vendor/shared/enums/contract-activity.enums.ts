export enum VendorContractActivityTypes {
    CONTRACT_CREATE = "Contract Created",
    CONTRACT_LINK_ADDITION = "Contract Link Added",
    CONTRACT_LINK_REMOVAL = "Contract Link Removed",
    CONTRACT_REMOVAL = "Contract Removed",
    CONTRACT_UPDATE = "Contract Updated",
    ATTACHMENT_ADDED = "Attachment Added",
    ATTACHMENT_REMOVED = "Attachment Removed",
}

export enum VendorContractActivityFieldTypes {
    AGREEMENT_CREATED_DATE = "agreementCreatedDate",
    APPROVED_DATE = "approvedDate",
    APPROVERS = "approvers",
    CONTRACT_OWNER_ID = "contractOwnerId",
    CONTRACT_TYPE_ID = "contractTypeId",
    COST = "cost",
    CURRENCY = "currency",
    EXPIRATION_DATE = "expirationDate",
    FILE_NAME = "fileName",
    INVENTORY_NAME = "inventoryName",
    INVENTORY_TYPE = "inventoryType",
    NAME = "name",
    NOTE = "note",
    STATUS = "status",
    VENDOR_CONTACT = "vendorContact",
}
