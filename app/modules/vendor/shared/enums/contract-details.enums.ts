export enum ContractTabOptions {
    Document = "DOCUMENT",
    Details = "DETAILS",
    ActivityStream = "ACTIVITY_STREAM",
    Related = "RELATED",
    Attachments = "ATTACHMENTS",
}
