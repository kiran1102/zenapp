export enum ContractAttachmentsTableColumns {
    NAME = "fileName",
    DATE_ADDED = "createdDate",
    ADDED_BY = "createdBy",
}
