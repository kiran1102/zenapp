export enum VendorControlsTabColumnNames {
    CONTROLID = "identifier",
    NAME = "name",
    FRAMEWORK = "frameworkName",
    CATEGORY = "categoryName",
    DESCRIPTION = "description",
    MATURITY = "maturityNameKey",
    NOTES = "note",
    STATUS = "status",
}
