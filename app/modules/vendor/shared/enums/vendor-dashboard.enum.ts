export enum ChartColors {
    GREEN = "#6CC04A",
    ORANGE = "#FA8231",
    YELLOW = "#F7B731",
    RED = "#EA3B5A",
    BLUE = "#2C98DA",
    BLUE_LIGHT = "#0EBAB1",
    BLUE_GRAY = "#A5B1C2",
}

export enum ContractStatuses {
    NO_EXPIRY = "NoExpiry",
    PAST_DUE = "PastDue",
    NEXT_30_DAYS = "Next30Days",
    NEXT_60_DAYS = "Next60Days",
    NEXT_90_DAYS = "Next90Days",
    NEXT_180_DAYS = "Next180Days",
    BEYOND_180_DAYS = "Beyond180Days",
}

export enum AssessmentStatuses {
    IN_PROGRESS = "IN_PROGRESS",
    NOT_STARTED = "NOT_STARTED",
    COMPLETED = "COMPLETED",
    UNDER_REVIEW = "UNDER_REVIEW",
}
