import {
    AssessmentStatuses,
    ChartColors,
    ContractStatuses,
} from "../enums/vendor-dashboard.enum";

export const AssessmentStatus = {
    [AssessmentStatuses.NOT_STARTED]: {
        color: ChartColors.RED,
        label: "NotStarted",
        order: 0,
    },
    [AssessmentStatuses.IN_PROGRESS]: {
        color: ChartColors.BLUE,
        label: "InProgress",
        order: 1,
    },
    [AssessmentStatuses.UNDER_REVIEW]: {
        color: ChartColors.YELLOW,
        label: "UnderReview",
        order: 2,
    },
    [AssessmentStatuses.COMPLETED]: {
        color: ChartColors.GREEN,
        label: "Completed",
        order: 3,
    },
};

export const ContractStatus = {
    [ContractStatuses.NO_EXPIRY]: {
        color: ChartColors.BLUE_GRAY,
        label: "NoExpirationDate",
        days: 0,
    },
    [ContractStatuses.PAST_DUE]: {
        color: ChartColors.RED,
        label: "PastDue",
        days: 0,
    },
    [ContractStatuses.NEXT_30_DAYS]: {
        color: ChartColors.ORANGE,
        label: "ExpiresInNDays",
        days: 30,
    },
    [ContractStatuses.NEXT_60_DAYS]: {
        color: ChartColors.YELLOW,
        label: "ExpiresInNDays",
        days: 60,
    },
    [ContractStatuses.NEXT_90_DAYS]: {
        color: ChartColors.GREEN,
        label: "ExpiresInNDays",
        days: 90,
    },
    [ContractStatuses.NEXT_180_DAYS]: {
        color: ChartColors.BLUE_LIGHT,
        label: "ExpiresInNDays",
        days: 180,
    },
    [ContractStatuses.BEYOND_180_DAYS]: {
        color: ChartColors.BLUE,
        label: "AfterNDays",
        days: 180,
    },
};
