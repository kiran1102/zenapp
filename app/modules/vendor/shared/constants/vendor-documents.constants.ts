export const ContractsTableColumns = {
    NAME: "name",
    FILE_NAME: "fileName",
    STATUS: "status",
    SERVICE: "service",
    TYPE: "contractTypeNameKey",
    TYPE_NAME: "contractTypeName",
    EXPIRATION: "expirationDate",
    COST: "cost",
};

export const StatusTranslationKeys = {
    NEW: "New",
    IN_PROGRESS: "InProgress",
    UNDER_REVIEW: "UnderReview",
    SIGNED: "Signed",
    EXPIRED: "Expired",
    ON_HOLD: "OnHold",
};

export const StatusResponseValues = {
    NEW: "NEW",
    IN_PROGRESS: "IN_PROGRESS",
    UNDER_REVIEW: "UNDER_REVIEW",
    SIGNED: "SIGNED",
    EXPIRED: "EXPIRED",
    ON_HOLD: "ON_HOLD",
};

export const VendorTableTypes = {
    Assets: "ASSETS",
    Processes: "PROCESSING_ACTIVITIES",
    Vendors: "VENDORS",
};

export const DefaultPagination = {
    page: 0,
    size: 20,
    sort: { sortKey: "", sortOrder: "" },
};
