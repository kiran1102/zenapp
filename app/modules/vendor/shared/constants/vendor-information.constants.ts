export const VendorInfoTableColumns = {
    ENTITY_NAME: "name",
    ENTITY_TYPE: "type",
    COUNTRY: "country",
};
