import { BadgeColorTypes } from "incidentModule/shared/constants/incident-badge-color.constants";

export const VPRequestStageBadgeColorMap = {
    Requested: BadgeColorTypes.DEFAULT,
    InProgress: BadgeColorTypes.PRIMARY,
    Declined: BadgeColorTypes.WARNING,
    Complete: BadgeColorTypes.SUCCESS,
};
