import { BadgeColorTypes } from "incidentModule/shared/constants/incident-badge-color.constants";

export const VPControlsStatusColorMap = {
    Active: BadgeColorTypes.SUCCESS,
    Implemented: BadgeColorTypes.SUCCESS,
    Inactive: BadgeColorTypes.DISABLED,
    Pending: BadgeColorTypes.PRIMARY,
    PendingReview: BadgeColorTypes.PRIMARY,
    NotDoing: BadgeColorTypes.DISABLED,
    Archived: BadgeColorTypes.EDIT,
};
