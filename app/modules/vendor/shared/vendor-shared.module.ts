import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from "@angular/core";

// Modules
import { SharedModule } from "sharedModules/shared.module";
import { UpgradedLegacyModule } from "sharedModules/upgrade/upgraded-legacy.module";
import { CommonModule } from "@angular/common";
import { HttpClientModule } from "@angular/common/http";
import { FormsModule } from "@angular/forms";
import { ReactiveFormsModule } from "@angular/forms";
import { VitreusModule } from "@onetrust/vitreus";
import { DndListModule } from "ngx-drag-and-drop-lists";
import { OTPipesModule, PIPES } from "modules/pipes/pipes.module";
import { VirtualScrollModule } from "od-virtualscroll";
import { OneVideoModule } from "modules/video/video.module";
import { InventorySharedModule } from "inventorySharedModule/inventory-shared.module";

// Services
import { InventoryControlsService } from "modules/vendor/shared/services/inventory-controls.service";

// Components
import { VendorRelatedFrameworksComponent } from "modules/vendor/shared/components/vendor-related-frameworks/vendor-related-frameworks.component";
import { VendorRelatedFrameworksCardComponent } from "modules/vendor/shared/components/vendor-related-frameworks/vendor-related-frameworks-card/vendor-related-frameworks-card.component";
import { VendorUploadDocumentComponent } from "modules/vendor/shared/components/vendor-upload-document/vendor-upload-document.component";
import { VendorSelectServicesComponent } from "modules/vendor/shared/components/vendor-select-services/vendor-select-services.component";
import { VendorInformationComponent } from "modules/vendor/shared/components/vendor-information/vendor-information.component";
import { VendorCertificationsCardComponent } from "modules/vendor/shared/components/vendor-information/vendor-certifications-card/vendor-certifications-card.component";
import { VendorDashboardComponent } from "modules/vendor/shared/components/vendor-dashboard/vendor-dashboard.component";

@NgModule({
    imports: [
        SharedModule,
        UpgradedLegacyModule,
        CommonModule,
        HttpClientModule,
        FormsModule,
        ReactiveFormsModule,
        VitreusModule,
        DndListModule,
        OTPipesModule,
        VirtualScrollModule,
        OneVideoModule,
        InventorySharedModule,
    ],
    declarations: [
        VendorRelatedFrameworksComponent,
        VendorRelatedFrameworksCardComponent,
        VendorUploadDocumentComponent,
        VendorSelectServicesComponent,
        VendorInformationComponent,
        VendorCertificationsCardComponent,
        VendorDashboardComponent,
    ],
    exports: [
        VendorRelatedFrameworksComponent,
        VendorRelatedFrameworksCardComponent,
        VendorUploadDocumentComponent,
        VendorSelectServicesComponent,
        VendorInformationComponent,
        VendorCertificationsCardComponent,
        InventorySharedModule,
    ],
    entryComponents: [
        VendorUploadDocumentComponent,
        VendorSelectServicesComponent,
        VendorInformationComponent,
        VendorDashboardComponent,
    ],
    providers: [
        InventoryControlsService,
        PIPES, // TODO: Remove from providers when ot-common is implemented. Only necessary if you are using pipe in TS file, else just module is fine
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class VendorSharedModule { }
