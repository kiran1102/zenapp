import angular from "angular";

import routes from "./vendor.routes";

import { VendorWrapperComponent } from "modules/vendor/vendor-wrapper.component";
import { InventoryService } from "modules/inventory/inventory-shared/shared/services/inventory-api.service";

export const vendorLegacyModule = angular
    .module("zen.vendor", [
        "ui.router",
        "ui.bootstrap",
        "isoCurrency",
        // Angular modules
        "ngAnimate",
    ])
    .config(routes)
    .service("InventoryService", InventoryService)
    .component("vendorWrapper", VendorWrapperComponent)
    ;
