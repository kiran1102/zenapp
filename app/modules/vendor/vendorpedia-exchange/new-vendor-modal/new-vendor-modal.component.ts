// Core
import { Component, OnInit } from "@angular/core";

// Forms
import {
    FormGroup,
    FormBuilder,
    Validators,
    AbstractControl,
} from "@angular/forms";

// Vitreus
import {
    IOtModalContent,
} from "@onetrust/vitreus";

// Rxjs
import { Subject } from "rxjs";

// Interfaces
import { ISuggestNewVendor } from "modules/vendor/shared/interfaces/vendorpedia-exchange.interface";

// Services
import { VendorApiService } from "modules/vendor/services/api/vendor-api.service";

@Component({
    selector: "new-vendor-modal",
    templateUrl: "./new-vendor-modal.component.html",
})

export class NewVendorModalComponent implements IOtModalContent {

    otModalCloseEvent: Subject<boolean>;
    newVendorModalForm: FormGroup;
    disableRequestButton: boolean;

    get vendorName() { return this.newVendorModalForm.get("vendorName"); }
    get vendorServicesPurchased() { return this.newVendorModalForm.get("vendorServicesPurchased"); }
    get vendorURL() { return this.newVendorModalForm.get("vendorURL"); }

    constructor(
        private fb: FormBuilder,
        private vendorApiService: VendorApiService,
    ) { }

    ngOnInit() {
        this.generateControlFormGroup();
    }

    closeModal(isClose: boolean) {
        this.otModalCloseEvent.next(isClose);
    }

    requestVendor() {
        this.disableRequestButton = true;
        const newVendor: ISuggestNewVendor = {
            name: this.newVendorModalForm.value.vendorName,
            services: this.newVendorModalForm.value.vendorServicesPurchased,
            url: this.newVendorModalForm.value.vendorURL,
        };
        this.vendorApiService.suggestNewVendor(newVendor)
            .subscribe(() => {
                this.disableRequestButton = false;
                this.closeModal(false);
            }, () => {
                this.disableRequestButton = false;
            });
    }

    private generateControlFormGroup() {
        this.newVendorModalForm = this.fb.group({
            vendorName: [null, [Validators.required, Validators.minLength(1), Validators.maxLength(200)]],
            vendorServicesPurchased: [null, [Validators.maxLength(200)]],
            vendorURL: [null, [Validators.maxLength(200)]],
        });
    }

}
