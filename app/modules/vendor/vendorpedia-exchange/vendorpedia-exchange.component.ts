// Rxjs
import {
    Subscription,
    Subject,
} from "rxjs";
import { startWith } from "rxjs/operators";

// Core
import {
    Component,
    Inject,
} from "@angular/core";
import { StateService } from "@uirouter/core";

// 3rd Party
import { some } from "lodash";

// Redux
import observableFromStore from "oneRedux/utils/observable-from-store";
import {
    vendorRecordCardData,
    isLoadingVendorRecord,
    vendorRecordMetaData,
} from "modules/vendor/shared/reducers/vendor-record.reducer";
import { StoreToken } from "tokens/redux-store.token";
import { getCurrentOrgId } from "oneRedux/reducers/org.reducer";

// Services
import { VendorRecordActionService } from "modules/vendor/services/actions/vendor-record-action.service";
import { VendorApiService } from "modules/vendor/services/api/vendor-api.service";
import { VendorInformationService } from "modules/vendor/services/shared/vendor-information.service";

// Interfaces
import {
    IVendorCatalogInformation,
    IInventoryVendorNames,
    IVendorCatalogCard,
} from "modules/vendor/shared/interfaces/vendorpedia-exchange.interface";
import {
    IStoreState,
    IStore,
} from "interfaces/redux.interface";
import { IPageableMeta } from "interfaces/pagination.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";

// Constants / Enums
import { InventoryTableIds } from "enums/inventory.enum";

// Vitreus
import {
    OtModalService,
} from "@onetrust/vitreus";

// Modals
import { NewVendorModalComponent } from "./new-vendor-modal/new-vendor-modal.component";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

@Component({
    selector: "vendorpedia-exchange",
    templateUrl: "./vendorpedia-exchange.component.html",
})
export class VendorpediaExchangeComponent {

    isLoading: boolean;
    searchText = "";
    resetSearch$ = new Subject();
    vendorList:  IVendorCatalogCard[];
    vendorRecordMetaData: IPageableMeta;
    vendorDetail: IVendorCatalogInformation;
    inventoryVendorList: IInventoryVendorNames[];
    pageNum: number;
    paginationParams = { page: 0, size: 24, sort: "popularity,name,ASC" };
    isVendorNamesFetched = false;
    private subscription: Subscription;

    constructor(
        @Inject(StoreToken) private store: IStore,
        private VendorRecordAction: VendorRecordActionService,
        private VendorApi: VendorApiService,
        private stateService: StateService,
        private vendorInformationService: VendorInformationService,
        private otModalService: OtModalService,
        private translatePipe: TranslatePipe,
    ) { }

    ngOnInit(): void {
        this.subscription = observableFromStore(this.store).pipe(
            startWith(this.store.getState()),
        ).subscribe((state: IStoreState): void => this.componentWillReceiveState(state));
        this.pageNum = this.vendorInformationService.vendorPreviewPagination$.getValue();
        if (this.pageNum) {
            this.paginationParams.page = this.pageNum;
            this.VendorRecordAction.fetchVendorRecords(this.vendorInformationService.vendorSearchName$.getValue(), null, this.paginationParams);
        } else {
            this.VendorRecordAction.fetchVendorRecords(this.vendorInformationService.vendorSearchName$.getValue());
        }
    }

    ngOnDestroy(): void {
        this.subscription.unsubscribe();
        this.VendorRecordAction.resetVendorRecords();
    }

    componentWillReceiveState(state: IStoreState): void {
        this.isLoading = isLoadingVendorRecord(state);
        if (!this.isLoading && vendorRecordCardData(state)) {
            this.vendorList = vendorRecordCardData(state) as IVendorCatalogCard[];
            this.vendorRecordMetaData = vendorRecordMetaData(state);
            if (!this.isVendorNamesFetched) {
                this.fetchInventoryVendorNamesList();
            }
        }
    }

    onHeaderRouteClick(route: { path: string, params: any }) {
        this.stateService.go(route.path, { Id: InventoryTableIds.Vendors, page: null, size: null });
    }

    getPageData(pageNumber: number): void {
        this.paginationParams.page = pageNumber;
        this.isVendorNamesFetched = false;
        this.vendorInformationService.vendorPreviewPagination(pageNumber);
        this.VendorRecordAction.fetchVendorRecords(this.vendorInformationService.vendorSearchName$.getValue(), null, this.paginationParams);
    }

    onSearchChange(searchText: string = ""): void {
        this.paginationParams.page = 0;
        this.isVendorNamesFetched = false;
        this.vendorInformationService.vendorSearchName$.next(searchText);
        this.searchText = searchText;
        this.vendorInformationService.vendorPreviewPagination(this.paginationParams.page);
        this.VendorRecordAction.fetchVendorRecords(this.vendorInformationService.vendorSearchName$.getValue(), [], this.paginationParams);
        this.isLoading = true;
    }

    trackByFn(index: number): number {
        return index;
    }
    fetchInventoryVendorNamesList() {
        this.isLoading = true;
        const orgId = getCurrentOrgId(this.store.getState());
        this.VendorApi.fetchInventoryVendorNames(orgId).then((res: IProtocolResponse<IInventoryVendorNames[]>): void => {
            if (res.result) {
                this.isVendorNamesFetched = true;
                this.inventoryVendorList = res.data;
                for (let i = 0; i < this.vendorList.length; i++) {
                    this.vendorList[i].disableAdd = some(this.inventoryVendorList, ["name", this.vendorList[i].name]);
                }
            }
            this.isLoading = false;
        });
    }

    suggestNewVendor() {
        this.otModalService
            .create(
                NewVendorModalComponent,
                {
                    isComponent: true,
                },
            );
    }
}
