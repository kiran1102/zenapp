import { Injectable } from "@angular/core";

// rxjs
import { from, Observable } from "rxjs";

// Services
import { ProtocolService } from "modules/core/services/protocol.service";

// Interfaces
import {
    IProtocolConfig,
    IProtocolMessages,
    IProtocolResponse,
} from "interfaces/protocol.interface";
import {
    IVendorCatalogInformation,
    IVendorpediaExtendedDetail,
    IVendorCatalogCertificateInformation,
} from "modules/vendor/shared/interfaces/vendorpedia-exchange.interface";
import { IFrameworkCertificateInformation } from "modules/vendor/shared/interfaces/inventory-controls.interface";
import {
    IPageableOf,
    IPaginationFilter,
} from "interfaces/pagination.interface";
import { INameId } from "interfaces/generic.interface";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

@Injectable()
export class VendorpediaExchangeApiService {

    constructor(
        private protocolService: ProtocolService,
        private translatePipe: TranslatePipe,
    ) {}

    fetchVendorpediaById(vendorId: string): ng.IPromise<IProtocolResponse<IVendorCatalogInformation>> {
        const config: IProtocolConfig = this.protocolService.customConfig("GET", `/api/vendor/v1/vendorpedia/vendors/${vendorId}/vendors`);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("Vendor.Error.RetrievingVendor") } };
        return this.protocolService.http(config, messages);
    }

    fetchVendorRecords(fullText: string, filters: IPaginationFilter[], paginationParams): ng.IPromise<IProtocolResponse<IPageableOf<INameId<string>>>> {
        const filter: string = filters && filters.length > 0 ? encodeURI(JSON.stringify(filters)) : "";
        const config: IProtocolConfig = this.protocolService.customConfig("POST", "/api/vendor/v1/vendorpedia/search", { text: "", filter, ...paginationParams }, { fullText });
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("Vendor.Error.RetrievingVendors") } };
        return this.protocolService.http(config, messages);
    }

    fetchServicesByVendorId(vendorId: string): Observable<IProtocolResponse<Array<INameId<string>>>> {
        const config: IProtocolConfig = this.protocolService.customConfig("GET", `/api/vendor/v1/vendorpedia/vendors/${vendorId}/services`);
        const messages: IProtocolMessages = { Error: { Hide: true } };
        return from(this.protocolService.http(config, messages));
    }

    fetchCertificatesByServiceId(vendorServiceId: string): Observable<IProtocolResponse<IVendorCatalogCertificateInformation[]>> {
        const config: IProtocolConfig = this.protocolService.customConfig("GET", `/api/vendor/v1/vendorpedia/vendors/services/${vendorServiceId}/certificates`);
        const messages: IProtocolMessages = { Error: { Hide: true } };
        return from(this.protocolService.http(config, messages));
    }

    fetchExtendedVendorDetails(vendorId: string, serviceIds: string[]): ng.IPromise<IProtocolResponse<IVendorpediaExtendedDetail>> {
        const config: IProtocolConfig = this.protocolService.customConfig("POST", `/api/vendor/v1/vendorpedia/vendors/${vendorId}/ext/vendors`, null, serviceIds);
        const messages: IProtocolMessages = { Error: { Hide: true } };
        return this.protocolService.http(config, messages);
    }

    fetchVendorpediaCertificatesByVendorId(vendorId: string): Observable<IProtocolResponse<IFrameworkCertificateInformation[]>> {
        const config: IProtocolConfig = this.protocolService.customConfig("GET", `/api/vendor/v1/vendorpedia/vendors/${vendorId}/certificates`);
        const messages: IProtocolMessages = { Error: { Hide: true } };
        return from(this.protocolService.http(config, messages));
    }
}
