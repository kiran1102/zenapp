import { Injectable } from "@angular/core";

// Rxjs
import { BehaviorSubject } from "rxjs";

// Services
import { VendorpediaExchangeApiService } from "vendorpediaExchange/shared/services/vendorpedia-exchange-api.service";

// Interfaces
import { IProtocolResponse } from "interfaces/protocol.interface";
import { IFrameworkCertificateInformation } from "modules/vendor/shared/interfaces/inventory-controls.interface";

@Injectable()
export class VendorpediaExchangeCertificatesService {

    // tslint:disable:member-ordering
    private _vendorpediaCertificates$ = new BehaviorSubject<IFrameworkCertificateInformation[]>(null);
    private _vendorpediaCertificatesLoading$ = new BehaviorSubject<boolean>(false);

    vendorpediaCertificates$ = this._vendorpediaCertificates$.asObservable();
    vendorpediaCertificatesLoading$ = this._vendorpediaCertificatesLoading$.asObservable();

    constructor(
        private vendorpediaExchangeApiService: VendorpediaExchangeApiService,
    ) { }

    retrieveVendorpediaCertificates(vendorId: string) {
        this._vendorpediaCertificatesLoading$.next(true);
        this.vendorpediaExchangeApiService.fetchVendorpediaCertificatesByVendorId(vendorId)
            .subscribe((res: IProtocolResponse<IFrameworkCertificateInformation[]>) => {
                if (res.result) {
                    this._vendorpediaCertificates$.next(res.data);
                }
                this._vendorpediaCertificatesLoading$.next(false);
            });
    }
}
