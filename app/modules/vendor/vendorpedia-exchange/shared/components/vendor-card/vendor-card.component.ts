// Core
import {
    Component,
    Input,
    Inject,
} from "@angular/core";
import { StateService } from "@uirouter/core";

// Services
import { VendorApiService } from "modules/vendor/services/api/vendor-api.service";
import { Permissions } from "modules/shared/services/helper/permissions.service";
import { VendorpediaExchangeApiService } from "vendorpediaExchange/shared/services/vendorpedia-exchange-api.service";

// Interfaces
import {
    IVendorRecordOption,
    IVendorCatalogInformation,
    IVendorCatalogCard,
    IVendorOnboardingRequest,
    IVendorpediaServiceAttr,
    IVendorAttributes,
} from "modules/vendor/shared/interfaces/vendorpedia-exchange.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";
import { IOrganization } from "interfaces/org.interface";
import { IStore } from "interfaces/redux.interface";
import { INameId } from "interfaces/generic.interface";

// Redux
import { StoreToken } from "tokens/redux-store.token";
import { getCurrentOrgList } from "oneRedux/reducers/org.reducer";

// Enums & Constants
import { VendorpediaExchangeTabNames } from "modules/vendor/shared/enums/vendorpedia-exchange-preview.enum";

// Vitreus
import {
    OtModalService,
    ModalSize,
} from "@onetrust/vitreus";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

// Modals
import { VendorSelectServiceModalComponent } from "vendorpediaExchange/modals/vendor-select-services-modal/vendor-select-services-modal.component";

// RXJS
import { Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";

@Component({
    selector: "vendor-card",
    templateUrl: "./vendor-card.component.html",
})
export class VendorCardComponent {
    @Input() vendorpedia: IVendorCatalogCard;
    @Input() vendorId: string;
    @Input() disableAdd: boolean;

    isLoading: boolean;
    organizations: IOrganization[];
    selectedOrganization: IOrganization;
    addButtonClicked = false;
    isAdded: boolean;
    vendorServicesArray: Array<INameId<string>>;
    selectedVendorServices: IVendorpediaServiceAttr[];
    vendorServices = new Subject();

    private destroy$ = new Subject();

    constructor(
        @Inject(StoreToken) public store: IStore,
        private VendorApi: VendorApiService,
        private vendorpediaExchangeApiService: VendorpediaExchangeApiService,
        private stateService: StateService,
        private permissions: Permissions,
        private otModalService: OtModalService,
        private translatePipe: TranslatePipe,
    ) { }

    ngOnInit(): void {
        this.organizations = getCurrentOrgList(this.store.getState());
        this.selectedOrganization = this.organizations[0];
        this.isAdded = this.vendorpedia.disableAdd;
        this.vendorServices.pipe(
            takeUntil(this.destroy$),
        ).subscribe((services: Array<INameId<string>>) => {
            if (services.length) {
                this.selectedVendorServices = services.map((item) => {
                    return {vendorpediaServiceId: item.id , vendorpediaServiceName: item.name};
                });
            }
            this.constructPayload(this.selectedVendorServices && this.selectedVendorServices.length ? this.selectedVendorServices : null);
        });
    }

    ngOnDestroy(): void {
        this.destroy$.next();
        this.destroy$.complete();
    }

    addVendor(payload: IVendorOnboardingRequest): void {
        this.VendorApi.vendorOnboarding(payload).then((response: IProtocolResponse<IVendorRecordOption>): void => {
            if (response.result) {
                this.isAdded = true;
            }
            this.isLoading = false;
        });
    }

    displayServicesOffered() {
        this.addButtonClicked = true;
        this.isLoading = true;
        this.VendorApi.fetchServiceForVendor(this.vendorId).subscribe((response: IProtocolResponse<Array<INameId<string>>>) => {
            if (response.result) {
                if (response.data.length > 0) {
                    this.vendorServicesArray = response.data;
                    this.otModalService
                        .create(
                            VendorSelectServiceModalComponent,
                            {
                                isComponent: true,
                                size: ModalSize.MEDIUM,
                            },
                            {
                                vendorServiceList: this.vendorServicesArray,
                                vendorServices$: this.vendorServices,
                            },
                        ).subscribe((res: boolean) => {
                            if (!res) this.isLoading = false;
                        });
                } else {
                    this.constructPayload(null);
                }
            } else {
                this.isLoading = false;
            }
            this.addButtonClicked = false;
        });
    }

    goToRoute(vendorId: string): void {
        this.stateService.go("zen.app.pia.module.vendor.select-services", { vendorId });
    }

    displayVendorDetails() {
        if (!this.addButtonClicked) {
            this.stateService.go("zen.app.pia.module.vendor.vendorpedia-preview",
                {
                    inventoryId: this.vendorId,
                    tab: VendorpediaExchangeTabNames.DETAILS,
                },
            );
        }
    }

    private constructPayload(selectedVendorServices: IVendorpediaServiceAttr[]) {
        this.isLoading = true;
        this.vendorpediaExchangeApiService.fetchVendorpediaById(this.vendorId).then((res: IProtocolResponse<IVendorCatalogInformation>): void => {
            if (res.result) {
                const vendorDetail: IVendorAttributes = res.data.attributes;
                const payload: IVendorOnboardingRequest = {
                    name: this.vendorpedia.name,
                    orgGroupId: this.selectedOrganization.Id,
                    vendorCatalogId: this.vendorpedia.id,
                    externalId: this.vendorpedia.externalId,
                    attributes: vendorDetail ? {
                        contactPhoneNumber: [{ value: vendorDetail.contactPhoneNumber ? vendorDetail.contactPhoneNumber[0].value : null }],
                        dataCollected: [{ value: vendorDetail.dataCollected ? vendorDetail.dataCollected[0].value : null }],
                        nextEuUsCertificationDate: [{ value: vendorDetail.nextEuUsCertificationDate ? vendorDetail.nextEuUsCertificationDate[0].value : null }],
                        euUsPrivacyShieldStatus: [{
                            id: null,
                            value: vendorDetail.euUsPrivacyShieldStatus ? vendorDetail.euUsPrivacyShieldStatus[0].value : null,
                        }],
                        originalEuUsCertificationDate: [{ value: vendorDetail.originalEuUsCertificationDate ? vendorDetail.originalEuUsCertificationDate[0].value : null }],
                        primaryContact: [{
                            value: vendorDetail.primaryContact ? vendorDetail.primaryContact[0].value : null,
                            id: null,
                        }],
                        privacyShieldNoticeLink: [{ value: vendorDetail.privacyShieldNoticeLink ? vendorDetail.privacyShieldNoticeLink[0].value : null }],
                        productService: [{ value: vendorDetail.productService ? vendorDetail.productService[0].value : null }],
                        purposeOfDataCollection: [{ value: vendorDetail.purposeOfDataCollection ? vendorDetail.purposeOfDataCollection[0].value : null }],
                        vendorContact: [{ value: vendorDetail.vendorContact ? vendorDetail.vendorContact[0].value : null }],
                        privacyShieldCoveredEntities: [{ value: vendorDetail.privacyShieldCoveredEntities ? vendorDetail.privacyShieldCoveredEntities[0].value : null }],
                    } : null,
                    services: selectedVendorServices,
                };
                this.addVendor(payload);
            } else {
                this.isLoading = false;
            }
        });
    }
}
