import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from "@angular/core";

// Modules
import { SharedModule } from "sharedModules/shared.module";
import { UpgradedLegacyModule } from "sharedModules/upgrade/upgraded-legacy.module";
import { CommonModule } from "@angular/common";
import { HttpClientModule } from "@angular/common/http";
import { FormsModule } from "@angular/forms";
import { ReactiveFormsModule } from "@angular/forms";
import { VitreusModule } from "@onetrust/vitreus";
import { DndListModule } from "ngx-drag-and-drop-lists";
import { OTPipesModule, PIPES } from "modules/pipes/pipes.module";
import { VirtualScrollModule } from "od-virtualscroll";
import { OneVideoModule } from "modules/video/video.module";

// Components
import { VendorCardComponent } from "./components/vendor-card/vendor-card.component";

// Services
import { VendorpediaExchangeCertificatesService } from "vendorpediaExchange/shared/services/vendorpedia-exchange-certificates.service";
import { VendorpediaExchangeApiService } from "vendorpediaExchange/shared/services/vendorpedia-exchange-api.service";

@NgModule({
    imports: [
        SharedModule,
        UpgradedLegacyModule,
        CommonModule,
        HttpClientModule,
        FormsModule,
        ReactiveFormsModule,
        VitreusModule,
        DndListModule,
        OTPipesModule,
        VirtualScrollModule,
        OneVideoModule,
    ],
    declarations: [
        VendorCardComponent,
    ],
    exports: [
        VendorCardComponent,
    ],
    entryComponents: [
    ],
    providers: [
        VendorpediaExchangeCertificatesService,
        VendorpediaExchangeApiService,
        PIPES, // TODO: Remove from providers when ot-common is implemented. Only necessary if you are using pipe in TS file, else just module is fine
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class VendorpediaSharedModule {}
