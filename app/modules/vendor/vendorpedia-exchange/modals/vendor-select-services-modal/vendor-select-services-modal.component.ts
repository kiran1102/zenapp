// Core
import {
    Component,
    OnInit,
} from "@angular/core";

// Rxjs
import { Subject } from "rxjs";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

// Interface
import {
    INameId,
    IStringMap,
} from "interfaces/generic.interface";

@Component({
    selector: "vendor-select-services-modal",
    templateUrl: "./vendor-select-services-modal.component.html",
})

export class VendorSelectServiceModalComponent implements OnInit {

    otModalCloseEvent: Subject<boolean>;
    subHeading = this.translatePipe.transform("VendorOnboardingServiceModalTitle");
    selectAll = this.translatePipe.transform("SelectAll");
    allServicesChecked = false;
    servicesList: Array<INameId<string>>;
    selectedRowMap: IStringMap<boolean> = {};
    selectedServices: Array<INameId<string>> = [];
    isExtraCell: boolean;
    otModalData;

    constructor(
        private translatePipe: TranslatePipe,
    ) { }

    ngOnInit() {
        if (this.otModalData && this.otModalData.vendorServiceList) {
            this.servicesList = this.otModalData.vendorServiceList;
            this.isExtraCell = (this.servicesList.length % 2) === 0;
        }
    }

    closeModal() {
        this.otModalCloseEvent.next(false);
    }

    handleCheckBoxClick(rowCheck: string, row?: INameId<string>) {
        switch (rowCheck) {
            case "allSelected":
                this.handleAllSelection();
                break;

            case "singleSelected":
                this.handleSingleSelection(row);
                break;

        }
    }

    handleAllSelection() {
        this.allServicesChecked = !this.allServicesChecked;
        this.selectedServices = [];
        if (this.allServicesChecked) {
            this.servicesList.map((item) => {
                this.selectedRowMap[item.id] = true;
            });
            this.selectedServices = [...this.servicesList];
        } else {
            this.servicesList.map((item) => {
                this.selectedRowMap[item.id] = false;
            });
        }
    }

    handleSingleSelection(row: INameId<string>) {
        this.selectedRowMap[row.id] = !this.selectedRowMap[row.id];
        this.allServicesChecked = Object.values(this.selectedRowMap).filter(Boolean).length === this.servicesList.length;
        if (this.selectedRowMap[row.id]) {
            this.selectedServices.push(row);
        } else if (this.selectedServices.some((item) => item.id === row.id)) {
            this.selectedServices.splice(this.selectedServices.indexOf(row), 1);
        }
    }

    onContinue() {
        this.otModalData.vendorServices$.next(this.selectedServices);
        this.otModalCloseEvent.next(true);
        this.otModalCloseEvent.complete();
    }
}
