import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from "@angular/core";

// Modules
import { SharedModule } from "sharedModules/shared.module";
import { UpgradedLegacyModule } from "sharedModules/upgrade/upgraded-legacy.module";
import { CommonModule } from "@angular/common";
import { HttpClientModule } from "@angular/common/http";
import { FormsModule } from "@angular/forms";
import { ReactiveFormsModule } from "@angular/forms";
import { VitreusModule } from "@onetrust/vitreus";
import { DndListModule } from "ngx-drag-and-drop-lists";
import { OTPipesModule, PIPES } from "modules/pipes/pipes.module";
import { VirtualScrollModule } from "od-virtualscroll";
import { OneVideoModule } from "modules/video/video.module";
import { VendorpediaSharedModule } from "./shared/vendorpedia-shared.module";
import { VendorpediaPreviewModule } from "./vendorpedia-preview/vendorpedia-preview.module";

// Components
import { VendorpediaExchangeComponent } from "vendorpediaExchange/vendorpedia-exchange.component";
import { NewVendorModalComponent } from "vendorpediaExchange/new-vendor-modal/new-vendor-modal.component";

// Modals
import { VendorSelectServiceModalComponent } from "vendorpediaExchange/modals/vendor-select-services-modal/vendor-select-services-modal.component";

// Services

@NgModule({
    imports: [
        SharedModule,
        UpgradedLegacyModule,
        CommonModule,
        HttpClientModule,
        FormsModule,
        ReactiveFormsModule,
        VitreusModule,
        DndListModule,
        OTPipesModule,
        VirtualScrollModule,
        OneVideoModule,
        VendorpediaPreviewModule,
        VendorpediaSharedModule,
    ],
    declarations: [
        VendorpediaExchangeComponent,
        NewVendorModalComponent,
        VendorSelectServiceModalComponent,
    ],
    exports: [
        VendorpediaExchangeComponent,
        VendorSelectServiceModalComponent,
    ],
    entryComponents: [
        VendorpediaExchangeComponent,
        NewVendorModalComponent,
        VendorSelectServiceModalComponent,
    ],
    providers: [
        PIPES, // TODO: Remove from providers when ot-common is implemented. Only necessary if you are using pipe in TS file, else just module is fine
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class VendorpediaExchangeModule {}
