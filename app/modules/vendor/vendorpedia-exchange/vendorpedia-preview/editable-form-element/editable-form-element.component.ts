// Core
import { Component, Input } from "@angular/core";

// Interface
import { IKeyValue } from "interfaces/generic.interface";

@Component({
    selector: "editable-form-element",
    template: `
    <div
        class="stretch-horizontal slds-form-element"
        otFormElement
        [label]="item.key | otTranslate">
            <button
                class="row-space-between-center full-width text-left position-relative background-none border-none inventory-form__button"
                ot-auto-id="InventoryFormToggleEdit"
                >
                <span class="word-break" [innerText]="item.value || '- - - -'"></span>
                <i class="ot display-none position-absolute top-half right-0 inventory-form__icon ot-lock"></i>
            </button>
    </div>`,
})

export class EditableFormElementComponent {
    @Input() item: IKeyValue<string>;
}
