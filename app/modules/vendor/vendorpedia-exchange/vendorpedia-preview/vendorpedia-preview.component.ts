// Core
import { Component, OnInit, Inject } from "@angular/core";
import { StateService } from "@uirouter/angularjs";

// Services
import { VendorApiService } from "modules/vendor/services/api/vendor-api.service";
import { VendorpediaPreviewService } from "vendorpediaExchange/vendorpedia-preview/shared/services/vendorpedia-preview.service";
import { VendorpediaExchangeApiService } from "vendorpediaExchange/shared/services/vendorpedia-exchange-api.service";
import { VendorpediaExchangeCertificatesService } from "vendorpediaExchange/shared/services/vendorpedia-exchange-certificates.service";

// Interface
import { IProtocolResponse } from "interfaces/protocol.interface";
import {
    IVendorCatalogInformation,
    IVendorOnboardingRequest,
    IVendorRecordOption,
    IInventoryVendorNames,
    IVendorpediaServiceAttr,
    IVendorAttributes,
} from "modules/vendor/shared/interfaces/vendorpedia-exchange.interface";
import { ITabsMeta } from "interfaces/one-tabs-nav.interface";
import { IOrganization } from "interfaces/org.interface";
import { IStore } from "interfaces/redux.interface";
import {
    IKeyValue,
    INameId,
} from "interfaces/generic.interface";

// Enums && constants
import { InventoryTableIds } from "enums/inventory.enum";
import { VendorpediaExchangeTabNames } from "modules/vendor/shared/enums/vendorpedia-exchange-preview.enum";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

// Rxjs
import {
    Subject,
    Observable,
} from "rxjs";
import {
    takeUntil,
    startWith,
} from "rxjs/operators";

// Redux
import {
    getCurrentOrgList,
    getCurrentOrgId,
} from "oneRedux/reducers/org.reducer";
import { StoreToken } from "tokens/redux-store.token";

// 3rd Party
import { some } from "lodash";

// Vitrues
import {
    OtModalService,
    ModalSize,
} from "@onetrust/vitreus";

// Modals
import { VendorSelectServiceModalComponent } from "vendorpediaExchange/modals/vendor-select-services-modal/vendor-select-services-modal.component";

@Component({
    selector: "vendorpedia-preview",
    templateUrl: "./vendorpedia-preview.component.html",
    host: {
        "[class.column-nowrap]": "true",
        "[class.max-height-100-percent]": "true",
    },
})

export class VendorpediaPreviewComponent implements OnInit {
    headLine = this.translatePipe.transform("VendorInformation");
    imgSrc: string;
    isAdding = false;
    isAdded: boolean;
    isLoading: boolean;
    items: Array<IKeyValue<string>> = [];
    organizations: IOrganization[];
    selectedTab = VendorpediaExchangeTabNames.DETAILS;
    tabs = [];
    vendorDetail: IVendorCatalogInformation;
    vendorName = "";
    vendorpediaId: string;
    vendorpediaTabNames = VendorpediaExchangeTabNames;
    orgId = "";
    inventoryVendorList: IInventoryVendorNames[];
    frameworkCertificates$ = this.vendorpediaExchangeCertificatesService.vendorpediaCertificates$;
    vendorpediaCertificatesLoading$ = this.vendorpediaExchangeCertificatesService.vendorpediaCertificatesLoading$;
    selectedVendorServices: IVendorpediaServiceAttr[];
    vendorServicesArray: Array<INameId<string>>;
    isLoaded: boolean;
    sendAndRequest = new Subject();
    vendorServices = new Subject();
    hasServices: boolean;

    private destroy$ = new Subject();

    constructor(
        @Inject(StoreToken) public store: IStore,
        public vendorpediaExchangeCertificatesService: VendorpediaExchangeCertificatesService,
        private stateService: StateService,
        private VendorApi: VendorApiService,
        private vendorpediaExchangeApiService: VendorpediaExchangeApiService,
        private translatePipe: TranslatePipe,
        private vendorpediaPreviewService: VendorpediaPreviewService,
        private otModalService: OtModalService,
    ) { }

    ngOnInit() {
        this.isLoading = true;
        this.vendorpediaId = this.stateService.params.inventoryId;
        this.organizations = getCurrentOrgList(this.store.getState());
        this.orgId = getCurrentOrgId(this.store.getState());
        this.vendorpediaExchangeApiService.fetchVendorpediaById(this.vendorpediaId).then((res: IProtocolResponse<IVendorCatalogInformation>): void => {
            if (res.result) {
                this.vendorDetail = res.data;
                this.hasServices = this.vendorDetail.hasServices;
                this.tabs = this.vendorpediaPreviewService.configureTabs(this.hasServices);
                if (this.vendorDetail) this.items = this.vendorpediaPreviewService.generateAttributeValues(this.vendorDetail);
                this.showPreviewdata();
            } else {
                this.isLoading = false;
            }
        });
        this.sendAndRequest.pipe(takeUntil(this.destroy$),
        ).subscribe((res: boolean) => {
            if (res) this.stateService.go("zen.app.pia.module.vendor.vendorpedia.request");
        });
        this.vendorpediaExchangeCertificatesService.retrieveVendorpediaCertificates(this.vendorpediaId);
        this.vendorServices.pipe(
            takeUntil(this.destroy$),
        ).subscribe((services: Array<INameId<string>>) => {
            this.isLoaded = true;
            if (services.length) {
                this.selectedVendorServices = services.map((item) => {
                    return {vendorpediaServiceId: item.id , vendorpediaServiceName: item.name};
                });
            }
            this.constructPayload(this.selectedVendorServices && this.selectedVendorServices.length ? this.selectedVendorServices : null);
        });
    }

    ngOnDestroy(): void {
        this.destroy$.next();
        this.destroy$.complete();
    }

    showPreviewdata() {
        this.VendorApi.fetchInventoryVendorNames(this.orgId).then((response: IProtocolResponse<IInventoryVendorNames[]>): void => {
            if (response.result) {
                this.inventoryVendorList = response.data;
                this.vendorName = this.vendorDetail.name;
                this.imgSrc = this.vendorDetail.iconLink;
                this.vendorDetail.disableAdd = some(this.inventoryVendorList, ["name", this.vendorName]);
                const inventoryVendorResult = this.inventoryVendorList.find((item) => item.name === this.vendorName);
                this.vendorDetail.inventoryId = inventoryVendorResult ? inventoryVendorResult.id : null;
                this.isAdded = this.vendorDetail.disableAdd;
                this.isLoading = false;
            }
        });
    }

    getDefaultImg(): void {
        this.imgSrc = `images/vendor_generic.svg`;
    }

    onHeaderRouteClick(route: { path: string, params: any }) {
        this.stateService.go(route.path, { Id: InventoryTableIds.Vendors, page: null, size: null });
    }

    onTabClick(option: ITabsMeta<VendorpediaExchangeTabNames>) {
        this.selectedTab = option.id;
    }

    displayServicesOffered() {
        if (this.isAdding || this.isAdded) return;
        this.isAdding = true;
        this.isLoaded = true;
        this.VendorApi.fetchServiceForVendor(this.vendorpediaId).subscribe((response: IProtocolResponse<Array<INameId<string>>>) => {
            if (response.result) {
                if (response.data.length) {
                    this.isLoaded = false;
                    this.vendorServicesArray = response.data;
                    this.otModalService
                        .create(
                            VendorSelectServiceModalComponent,
                            {
                                isComponent: true,
                                size: ModalSize.MEDIUM,
                            },
                            {
                                vendorServiceList: this.vendorServicesArray,
                                vendorServices$: this.vendorServices,
                            },
                        ).subscribe((res: boolean) => {
                            if (!res) this.isAdding = false;
                        });
                } else {
                    this.constructPayload(null);
                }
            } else {
                this.isAdding = false;
            }
        });
    }

    addVendor(payload: IVendorOnboardingRequest): void {
        this.VendorApi.vendorOnboarding(payload).then((response: IProtocolResponse<IVendorRecordOption>): void => {
            if (response.result) {
                this.stateService.go("zen.app.pia.module.vendor.vendorpedia-exchange");
            }
            this.isAdding = false;
            this.isLoaded = false;
        });
    }

    goToRoute(route: { path: string, params: any }) {
        this.stateService.go(route.path, route.params);
    }

    trackByFn(index: number): number {
        return index;
    }

    vendorpediaPreviewRequestAssessment() {
        const params = {
            name: this.vendorDetail.name,
            org: this.organizations[0].Id,
            inventoryId: this.vendorDetail.inventoryId,
            vendorpediaId: this.vendorpediaId,
            previousState: this.stateService.current.name,
        };
        this.stateService.go("zen.app.pia.module.vendor.vendorpedia-request-new", params);
    }

    private constructPayload(selectedVendorServices: IVendorpediaServiceAttr[]) {
        const vendorDetailsAttr: IVendorAttributes = this.vendorDetail.attributes;
        const payload: IVendorOnboardingRequest = {
            name: this.vendorDetail.name,
            orgGroupId: this.organizations[0].Id,
            vendorCatalogId: this.vendorDetail.id,
            externalId: this.vendorDetail.externalId,
            attributes: vendorDetailsAttr ? {
                contactPhoneNumber: [{ value: vendorDetailsAttr.contactPhoneNumber ? vendorDetailsAttr.contactPhoneNumber[0].value : null }],
                dataCollected: [{ value: vendorDetailsAttr.dataCollected ? vendorDetailsAttr.dataCollected[0].value : null }],
                nextEuUsCertificationDate: [{ value: vendorDetailsAttr.nextEuUsCertificationDate ? vendorDetailsAttr.nextEuUsCertificationDate[0].value : null }],
                euUsPrivacyShieldStatus: [{
                    id: null,
                    value: vendorDetailsAttr.euUsPrivacyShieldStatus ? vendorDetailsAttr.euUsPrivacyShieldStatus[0].value : null,
                }],

                originalEuUsCertificationDate: [{ value: vendorDetailsAttr.originalEuUsCertificationDate ? vendorDetailsAttr.originalEuUsCertificationDate[0].value : null }],
                primaryContact: [{
                    value: vendorDetailsAttr.primaryContact ? vendorDetailsAttr.primaryContact[0].value : null,
                    id: null,
                }],
                privacyShieldNoticeLink: [{ value: vendorDetailsAttr.privacyShieldNoticeLink ? vendorDetailsAttr.privacyShieldNoticeLink[0].value : null }],
                productService: [{ value: vendorDetailsAttr.productService ? vendorDetailsAttr.productService[0].value : null }],
                purposeOfDataCollection: [{ value: vendorDetailsAttr.purposeOfDataCollection ? vendorDetailsAttr.purposeOfDataCollection[0].value : null }],
                vendorContact: [{ value: vendorDetailsAttr.vendorContact ? vendorDetailsAttr.vendorContact[0].value : null }],
                privacyShieldCoveredEntities: [{ value: vendorDetailsAttr.privacyShieldCoveredEntities ? vendorDetailsAttr.privacyShieldCoveredEntities[0].value : null }],
            } : null,
            services: selectedVendorServices,
        };
        this.addVendor(payload);
    }
}
