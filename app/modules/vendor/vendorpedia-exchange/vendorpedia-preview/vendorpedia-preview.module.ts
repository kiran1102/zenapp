import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from "@angular/core";

// Modules
import { SharedModule } from "sharedModules/shared.module";
import { UpgradedLegacyModule } from "sharedModules/upgrade/upgraded-legacy.module";
import { CommonModule } from "@angular/common";
import { HttpClientModule } from "@angular/common/http";
import { FormsModule } from "@angular/forms";
import { ReactiveFormsModule } from "@angular/forms";
import { VitreusModule } from "@onetrust/vitreus";
import { DndListModule } from "ngx-drag-and-drop-lists";
import { OTPipesModule, PIPES } from "modules/pipes/pipes.module";
import { VirtualScrollModule } from "od-virtualscroll";
import { OneVideoModule } from "modules/video/video.module";
import { VendorSharedModule } from "modules/vendor/shared/vendor-shared.module";

// Components
import { VendorpediaPreviewComponent } from "./vendorpedia-preview.component";
import { EditableFormElementComponent } from "./editable-form-element/editable-form-element.component";
import { VendorpediaServicesComponent } from "./vendorpedia-services/vendorpedia-services.component";
import { VPServicesCertificatePanelComponent } from "modules/vendor/vendorpedia-exchange/vendorpedia-preview/vendorpedia-services/vp-services-certificate-panel/vp-services-certificate-panel.component";
import { VPCertificatePanelDetailsComponent } from "modules/vendor/vendorpedia-exchange/vendorpedia-preview/vendorpedia-services/vp-certificate-panel-details/vp-certificate-panel-details.component";

// Services
import { VendorpediaPreviewService } from "./shared/services/vendorpedia-preview.service";

@NgModule({
    imports: [
        SharedModule,
        UpgradedLegacyModule,
        CommonModule,
        HttpClientModule,
        FormsModule,
        ReactiveFormsModule,
        VitreusModule,
        DndListModule,
        OTPipesModule,
        VirtualScrollModule,
        OneVideoModule,
        VendorSharedModule,
    ],
    declarations: [
        VendorpediaPreviewComponent,
        EditableFormElementComponent,
        VendorpediaServicesComponent,
        VPServicesCertificatePanelComponent,
        VPCertificatePanelDetailsComponent,
    ],
    exports: [
        VendorpediaPreviewComponent,
        EditableFormElementComponent,
        VendorpediaServicesComponent,
        VPServicesCertificatePanelComponent,
        VPCertificatePanelDetailsComponent,
    ],
    entryComponents: [
        VendorpediaPreviewComponent,
    ],
    providers: [
        VendorpediaPreviewService,
        PIPES, // TODO: Remove from providers when ot-common is implemented. Only necessary if you are using pipe in TS file, else just module is fine
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class VendorpediaPreviewModule {}
