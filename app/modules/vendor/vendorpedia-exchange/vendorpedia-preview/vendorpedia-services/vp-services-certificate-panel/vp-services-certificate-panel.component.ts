// Angular
import {
    Component,
    Input,
} from "@angular/core";

// Interfaces
import { IVendorCatalogCertificateCard } from "modules/vendor/shared/interfaces/vendorpedia-exchange.interface";

@Component({
    selector: "vp-services-certificate-panel",
    templateUrl: "./vp-services-certificate-panel.component.html",
})
export class VPServicesCertificatePanelComponent {

    @Input() certificate: IVendorCatalogCertificateCard;

    emptyData = "- - - -";
    showDetails: boolean;
}
