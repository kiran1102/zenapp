// Core
import {
    Component,
    OnInit,
    Input,
} from "@angular/core";

// rxjs
import { Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";

// Services
import { VendorpediaExchangeApiService } from "vendorpediaExchange/shared/services/vendorpedia-exchange-api.service";
import { VendorpediaPreviewService } from "vendorpediaExchange/vendorpedia-preview/shared/services/vendorpedia-preview.service";

// Interfaces
import { INameId } from "interfaces/generic.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";
import {
    IVendorCatalogCertificateInformation,
    IVendorCatalogCertificateCard,
} from "modules/vendor/shared/interfaces/vendorpedia-exchange.interface";

@Component({
    selector: "vendorpedia-services",
    templateUrl: "./vendorpedia-services.component.html",
})

export class VendorpediaServicesComponent implements OnInit {

    @Input() vendorId: string;

    destroy$ = new Subject();
    pageLoading = true;
    servicesLoading = false;
    certificateCards: IVendorCatalogCertificateCard[];
    services: Array<INameId<string>>;

    constructor(
        private vendorpediaExchangeApiService: VendorpediaExchangeApiService,
        private vendorpediaPreviewService: VendorpediaPreviewService,
    ) { }

    ngOnInit() {
        this.vendorpediaExchangeApiService.fetchServicesByVendorId(this.vendorId)
            .pipe(
                takeUntil(this.destroy$),
            ).subscribe((res) => {
                if (res.result && res.data && res.data.length) {
                    this.services = this.vendorpediaPreviewService.mapVendorpediaServiceMenuItem(res.data);
                    this.selectService(this.services[0].id);
                }
                this.pageLoading = false;
            });
    }

    ngOnDestroy() {
        this.destroy$.next();
        this.destroy$.complete();
    }

    selectService(id: string | "all") {
        this.servicesLoading = true;
        this.services = this.services.map((s) => {
            return {...s, selected: id === s.id };
        });

        this.vendorpediaExchangeApiService.fetchCertificatesByServiceId(id)
            .pipe(
                takeUntil(this.destroy$),
            ).subscribe((res: IProtocolResponse<IVendorCatalogCertificateInformation[]>) => {
                if (res.result && res.data && res.data.length) {
                    this.certificateCards = this.vendorpediaPreviewService.mapVendorpediaCatalogCards(res.data);
                } else {
                    this.certificateCards = null;
                }
                this.servicesLoading = false;
            });
    }

    trackByFn(index: number): number {
        return index;
    }
}
