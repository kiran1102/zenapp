// Angular
import {
    Component,
    Input,
} from "@angular/core";

// Interfaces
import { IVendorCatalogCertificateCard } from "modules/vendor/shared/interfaces/vendorpedia-exchange.interface";

@Component({
    selector: "vp-certificate-panel-details",
    templateUrl: "./vp-certificate-panel-details.component.html",
})
export class VPCertificatePanelDetailsComponent {

    @Input() certificate: IVendorCatalogCertificateCard;

    emptyData = "- - - -";
}
