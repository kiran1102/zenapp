import { Injectable } from "@angular/core";

// Interfaces
import { ITabsMeta } from "interfaces/one-tabs-nav.interface";
import {
    IVendorCatalogInformation,
    IVendorpediaServiceMenuItem,
    IVendorCatalogCertificateInformation,
    IVendorCatalogCertificateCard,
} from "modules/vendor/shared/interfaces/vendorpedia-exchange.interface";
import { INameId } from "interfaces/generic.interface";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";
import { OtDatePipe } from "modules/pipes/ot-date.pipe";

// Enums & Constants
import { VendorpediaExchangeTabNames } from "modules/vendor/shared/enums/vendorpedia-exchange-preview.enum";
import { GetLogoNameForCert } from "modules/vendor/certificates/shared/constants/certificates.constants";

@Injectable()
export class VendorpediaPreviewService {

    constructor(
        private translatePipe: TranslatePipe,
        private otDatePipe: OtDatePipe,
    ) {}

    configureTabs(vendorHasServices: boolean): Array<ITabsMeta<VendorpediaExchangeTabNames>> {
        const tabs = [];
        if (vendorHasServices) {
            tabs.push({
                text: this.translatePipe.transform("Details"),
                id: VendorpediaExchangeTabNames.DETAILS,
                otAutoId: "VendorpediaDetailsTab",
            });

            tabs.push({
                text: this.translatePipe.transform("Services"),
                id: VendorpediaExchangeTabNames.SERVICES,
                otAutoId: "VendorpediaServicesTab",
            });
        }
        return tabs;
    }

    generateAttributeValues(data: IVendorCatalogInformation) {
        return [
            { key: "Name", value: data.name },
            { key: "contactPhoneNumber", value: data.attributes && data.attributes.contactPhoneNumber ? data.attributes.contactPhoneNumber[0].value : null },
            { key: "dataCollected", value: data.attributes && data.attributes.dataCollected ? data.attributes.dataCollected[0].value : null },
            { key: "euUsPrivacyShieldStatus", value: data.attributes && data.attributes.euUsPrivacyShieldStatus ? data.attributes.euUsPrivacyShieldStatus[0].value : null },
            { key: "nextEuUsCertificationDate", value: data.attributes && data.attributes.nextEuUsCertificationDate ? this.dateFormat(data.attributes.nextEuUsCertificationDate[0].value) : null },
            { key: "originalEuUsCertificationDate", value: data.attributes && data.attributes.originalEuUsCertificationDate ? this.dateFormat(data.attributes.originalEuUsCertificationDate[0].value) : null },
            { key: "VendorsPrimaryContact", value: data.attributes && data.attributes.primaryContact ? data.attributes.primaryContact[0].value : null },
            { key: "privacyShieldNoticeLink", value: data.attributes && data.attributes.privacyShieldNoticeLink ? data.attributes.privacyShieldNoticeLink[0].value : null },
            { key: "ProductService", value: data.attributes && data.attributes.productService ? data.attributes.productService[0].value : null },
            { key: "VendorContact", value: data.attributes && data.attributes.vendorContact ? data.attributes.vendorContact[0].value : null },
            { key: "purposeOfDataCollection", value: data.attributes && data.attributes.purposeOfDataCollection ? data.attributes.purposeOfDataCollection[0].value : null },
            { key: "privacyShieldCoveredEntities", value: data.attributes && data.attributes.privacyShieldCoveredEntities ? data.attributes.privacyShieldCoveredEntities[0].value : null },
        ];
    }

    dateFormat(date: string) {
        return this.otDatePipe.transform(date, "date");
    }

    mapVendorpediaServiceMenuItem(services: Array<INameId<string>>): IVendorpediaServiceMenuItem[] {
        return services.map((s) => {
            return {...s, selected: false };
        });
    }

    mapVendorpediaCatalogCards(certificates: IVendorCatalogCertificateInformation[]) {
        return certificates.map((cert: IVendorCatalogCertificateCard) => {
            cert.cardImage = {
                url: `images/vendor/certificates/${GetLogoNameForCert(cert.frameworkName)}.png`,
            };
            return cert;
        });
    }
}
