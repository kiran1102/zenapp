// Angular
import {
    Component,
    OnInit,
} from "@angular/core";

// Rxjs
import { Subject } from "rxjs";

// Constants & Enums
import { VendorRulesTabOptions } from "modules/vendor/rules/shared/enums/vendor-rules.enums";
import { RuleContext } from "modules/rules/enums/rules.enums";

// Interfaces
import { ITabsNav } from "interfaces/one-tabs-nav.interface";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

@Component({
    selector: "vendor-rules",
    templateUrl: "./vendor-rules.component.html",
})
export class VendorRulesComponent implements OnInit {

    ruleContext = RuleContext.Vendor;
    isLoading = true;
    selectedTab = VendorRulesTabOptions.Rules;
    vendorRulesTabOptions = VendorRulesTabOptions;
    tabOptions: ITabsNav[] = [];

    private destroy$ = new Subject();

    constructor(
        private translatePipe: TranslatePipe,
    ) { }

    ngOnInit() {
        this.setTabOptions();
        this.isLoading = false;
    }

    ngOnDestroy() {
        this.destroy$.next();
        this.destroy$.complete();
    }

    onTabClick(option: ITabsNav) {
        this.selectedTab = option.id as VendorRulesTabOptions;
    }

    setTabOptions() {
        this.tabOptions.push({
            text: this.translatePipe.transform("Rules"),
            id: VendorRulesTabOptions.Rules,
            otAutoId: "VendorRulesTab",
        });

        // TODO: Apply once Activity is scoped
        // this.tabOptions.push({
        //     text: this.translatePipe.transform("Activity"),
        //     id: VendorRulesTabOptions.Activity,
        //     otAutoId: "VendorRulesActivityTab",
        // });
    }
}
