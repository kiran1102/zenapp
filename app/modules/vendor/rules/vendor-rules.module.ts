// Angular
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";

// Modules
import { VitreusModule } from "@onetrust/vitreus";
import { RulesModule } from "modules/rules/rules.module";
import { OTPipesModule, PIPES } from "modules/pipes/pipes.module";

// Components
import { VendorRulesComponent } from "modules/vendor/rules/vendor-rules.component";

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        VitreusModule,
        OTPipesModule,
        RulesModule,
    ],
    declarations: [
        VendorRulesComponent,
    ],
    providers: [
        PIPES, // TODO: Remove from providers when ot-common is implemented. Only necessary if you are using pipe in TS file, else just module is fine
    ],
    entryComponents: [
        VendorRulesComponent,
    ],
})
export class VendorRulesModule { }
