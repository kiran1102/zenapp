// Angular
import { Injectable } from "@angular/core";
import { IPromise } from "angular";

// 3rd Party
import {
    each,
    forEach,
    isEmpty,
    assign,
    sortBy,
    map,
    omit,
    find,
    merge,
    get,
    filter,
    isArray,
    set,
    reduce,
} from "lodash";
import { take } from "rxjs/operators";

// Services
import { NotificationService } from "modules/shared/services/provider/notification.service";
import { ModalService } from "sharedServices/modal.service";
import { Permissions } from "modules/shared/services/helper/permissions.service";
import { TranslateService } from "@ngx-translate/core";
import { WebformApiService } from "./services/webform-api.service";

// Interfaces
import {
    IStringMap,
    IKeyValue,
    ILabelValue,
} from "interfaces/generic.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";
import { IGenerateScriptModal, IWebformPublishResponse } from "dsarModule/interfaces/webform.interface";
import {
    IWebformSettings,
    IWebFormSettingsInput,
    IWebformFetchDataForExport,
    IWebFormFieldResponse,
} from "dsarModule/interfaces/webform.interface";
import { IDSARFormField } from "@onetrust/dsar-components";
import { OtModalService, ModalSize } from "@onetrust/vitreus";

// Constants & Enums
import { RequestTypeStatus } from "dsarModule/enums/web-form.enum";
import { DSARSettings } from "dsarModule/constants/dsar-settings.constant";
import { InputType } from "dsarModule/constants/web-form-control.constant";
import {
    WebformTabs,
    DSARStoreActions,
} from "dsarModule/enums/dsar-webform.enum";

// Components
import { PrepublishConfirmationModal } from "./components/webforms/prepublish-confirmation-modal/prepublish-confirmation-modal.component";

type IUnsubscribeCallback = () => void;
@Injectable()
export class WebformStore {

    tokenNotRequired = false;

    private dsarState: any;
    private hrefString: string;
    private resetState: any;
    private observers: any = [];
    private fetchDataPromise: IPromise<any>;
    private formTranslations: IStringMap<string>;
    private requestTypeStatus: any = RequestTypeStatus;
    private settingsMap: IStringMap<IWebFormSettingsInput>;
    private publishWebFormResponse: IWebformPublishResponse;

    private settingsOrder = [
        DSARSettings.EnableReCaptcha,
        DSARSettings.MultiCaptcha,
        DSARSettings.AttachmentSubmission,
        DSARSettings.Organization,
        DSARSettings.DefaultDaysToRespond,
        DSARSettings.DefaultReminder,
        DSARSettings.DefaultReviewer,
        DSARSettings.BccEmail,
        DSARSettings.TranslateWebForm,
        DSARSettings.AllowDsPortalAttachment,
        DSARSettings.EnableSubjectTypes,
        DSARSettings.EnableRequestTypes,
        DSARSettings.Workflows,
        DSARSettings.EmailVerification,
        DSARSettings.ExpiryRequest,
        DSARSettings.ExpiryRequestDays,
        DSARSettings.IsLogoEnabled,
    ];

    private translations: IStringMap<string> = {};
    private translationKeys = [
        "PublishWebForm",
        "AreYouSurePublishForm",
        "AnyChangesToFormOverwrite",
        "Cancel",
        "Publish",
        "Success",
        "AllChangesWereSaved",
        "Error",
        "AllChangesWereNotSaved",
        "DSARWebformScriptPublished",
        "WebformCanNotBePublished",
        "WebFormLink",
        "CopyLink",
    ];

    constructor(
        private translateService: TranslateService,
        private permissions: Permissions,
        private downWebformApiService: WebformApiService,
        private modalService: ModalService,
        private otModalService: OtModalService,
        private notificationService: NotificationService,
    ) {
        this.dsarState = {
            loading: true,
            permissions: {},
            webForm: null,
            webFormId: null,
            published: null,
            publishedPath: null,
            publishedHost: null,
            redirect: false,
            formFields: null,
            formStylingInputs: null,
            settingsInputs: null,
            isDirty: {},
            changesMadeToForm: false,
            savingInProgress: false,
            editSidebarEnabled: false,
            errorOnForm: false,
            selectedEditInput: "",
            hrefString: "",
            haveRequestTypes: true,
            requestTypesDeselected: null,
            subjectTypesDeselected: null,
            formTranslations: [],
            languageSelected: "",
            attachmentUpload : null,
            isWebformDeleted: false,
            submitButton: {
                type: "submitButton",
            },
            captchaType: null,
            widgetID: null,
            configureFormFieldType: false,
            configureFormField: false,
            editFormField: false,
            selectedFormField: null,
            rulesDirty: false,
        };

        this.translateService
            .stream(this.translationKeys)
            .subscribe((values) => {
                this.translations = values;
            });
   }

    public dispatch(action: any): void {
        this.dsarState = this.reducer(this.dsarState, action);
        const obsLength: number = this.observers.length;
        for (let i = 0; i < obsLength; i++) {
            this.observers[i]();
        }
    }

    public subscribe(observer: any): IUnsubscribeCallback {
        observer();
        this.observers.push(observer);
        return (): void => {
            this.observers = this.observers.filter((obs: any): boolean => obs !== observer);
        };
    }

    public languageSwitch(langSelected: string): void {
        if (!this.dsarState.savingInProgress) {
            this.saveDSARWebForm(true, false).then((): void => {
                this.downWebformApiService.getTranslations(this.dsarState.webFormId, langSelected, this.tokenNotRequired).then((response: IProtocolResponse<any>): void => {
                    if (response.result) {
                        this.dsarState.formTranslations = response.data.Translations;
                        this.dispatch({ type: DSARStoreActions.LANG_SWITCH, payload: { formTranslations: this.dsarState.formTranslations, languageSelected: langSelected } });
                    }
                });
            });
        }
    }

    public modifyMasterLanguageList(state: any, payload: any): any {
        const webForm = {
            ...state.webForm,
            languageList: payload.languageList,
            webformLanguagesList: payload.languageList,
            defaultLanguage: payload.defaultLanguage,
            languageModified: true,
        };
        return assign({}, state, { webForm });
    }

    public setTokenAccess(tokenNotRequired = false) {
        this.tokenNotRequired = tokenNotRequired;
    }

    public fetchWebForm(webFormId?: string, tabView?: number): IPromise<void> {
        if (this.fetchDataPromise) { return this.fetchDataPromise; }
        this.dispatch({ type: DSARStoreActions.FETCHING_DATA, payload: { loading: true } });
        this.fetchDataPromise = webFormId ? this.downWebformApiService.getWebFormById(webFormId, this.tokenNotRequired) : this.downWebformApiService.getDefaultWebForm();
        this.fetchDataPromise.then((response: any): any => {
            if (response.result) {
                const webForm: any = response.data;
                const templateId = webForm.templateId;
                this.dsarState.published = webForm.published;
                this.dsarState.publishedPath = webForm.publishedPath;
                this.dsarState.publishedHost = webForm.publishedHost;
                this.dsarState.redirect = webForm.redirect;
                webForm.defaultLanguage = find(webForm.languageList, "IsDefault").Code;
                if (tabView === WebformTabs.Settings) {
                    return this.fetchWebformSettings(templateId, webForm).then((): void => {
                        this.fetchDataPromise = null;
                    });
                } else {
                    return this.fetchFormData(templateId, webForm).then((): void  => {
                        this.fetchDataPromise = null;
                    });
                }
            } else {
                this.dispatch({ type: DSARStoreActions.WEBFORM_DELETED, payload: { } });
                this.dispatch({ type: DSARStoreActions.DATA_NOT_FETCHED, payload: { loading: false } });
                this.fetchDataPromise = null;
            }
        });
        return this.fetchDataPromise;
    }

    public fetchWebformSettings(webFormId: string, webForm: any): Promise<void> {
        return Promise.all([
            this.downWebformApiService.getSettingsInputs(webFormId),
            this.downWebformApiService.getAllLanguages(),
        ]).then((response: any[]): void => {
            if (response[0].result && response[1].result) {
                webForm.masterLanguagesList = response[1].data;
                webForm.webformLanguagesList = map(webForm.languageList, (language) => ({...language}));
                this.dispatch({
                    type: DSARStoreActions.DATA_FETCHED,
                    payload: {
                        settingsInputs: response[0].result ? this.buildSettingsInputs(response[0].data) : [],
                        webForm,
                        webFormId,
                    },
                });
                this.dispatch({ type: DSARStoreActions.DATA_LOADED, payload: { loading: false } });
            } else {
                this.dispatch({ type: DSARStoreActions.DATA_NOT_FETCHED, payload: { loading: false } });
            }
        });
    }

    public fetchFormData(webFormId: string, webForm: any): Promise<void> {
        return Promise.all([
            this.downWebformApiService.getFormFields(webFormId, this.tokenNotRequired),
            this.downWebformApiService.getFormStylingInputs(webFormId, this.tokenNotRequired),
            this.downWebformApiService.getSettingsInputs(webFormId, this.tokenNotRequired),
            this.downWebformApiService.getTranslations(webFormId, webForm.defaultLanguage, this.tokenNotRequired),
        ]).then((response: any[]): void => {
            // do NOT remove sorting for now
            webForm.requestTypes = sortBy(webForm.requestTypes, "order");
            webForm.subjectTypes = sortBy(webForm.subjectTypes, "order");
            forEach(webForm.requestTypes, (requestType: any) => {
                requestType.description = requestType.fieldName + "Desc";
            });
            forEach(webForm.subjectTypes, (subjectType: any) => {
                subjectType.description = subjectType.fieldName + "Desc";
            });
            webForm.webformLanguagesList = map(webForm.languageList, (language) => ({...language}));
            this.dispatch({
                type: DSARStoreActions.DATA_FETCHED, payload: {
                    permissions: {
                        formsName: this.permissions.canShow("DSARWebFormsWebFormName"),
                        formsText: this.permissions.canShow("DSARWebFormsFormText"),
                        formsTextEdit: this.permissions.canShow("DSARWebFormsFormTextEdit"),
                        formsFields: this.permissions.canShow("DSARWebFormFields"),
                        formsFieldsEdit: this.permissions.canShow("DSARWebFormFieldsEdit"),
                        formsDetails: this.permissions.canShow("DSARWebFormsFormDetails"),
                        formsDetailsEdit: this.permissions.canShow("DSARWebFormsFormDetailsEdit"),
                        formsStyling: this.permissions.canShow("DSARWebFormsFormStyling"),
                        formsStylingEdit: this.permissions.canShow("DSARWebFormsFormStylingEdit"),
                        formsSettings: this.permissions.canShow("DSARWebFormsSettings"),
                        formsSettingsEdit: this.permissions.canShow("DSARWebFormsSettingsEdit"),
                        formsResetTemplate: this.permissions.canShow("DSARWebFormsReset"),
                        formsSaveTemplate: this.permissions.canShow("DSARWebFormsSaveTemplate"),
                        formsGenerateLink: this.permissions.canShow("DSARWebFormsGenerateLink"),
                        enableMultiLanguage: this.permissions.canShow("DSARWebFormGetStatus"),
                        enableWebFormPublish: this.permissions.canShow("DSARWebFormPublish"),
                        dsarMultiCaptcha: this.permissions.canShow("DSARMultiCaptcha"),
                        DSARWebFormRuleEdit: this.permissions.canShow("DSARWebFormRuleEdit"),
                        DSARWebFormPublishV2: this.permissions.canShow("DSARWebFormPublishV2"),
                        DSARWebFormLegacy: this.permissions.canShow("DSARWebFormLegacy"),
                    },
                    isDirty: {
                        webForm: 0,
                        formFields: 0,
                        formStylingInputs: 0,
                        settingsInputs: 0,
                        requestTypes: 0,
                        subjectTypes: 0,
                    },
                    changesMadeToForm: false,
                    editSidebarEnabled: false,
                    savingInProgress: false,
                    errorOnForm: false,
                    configureFormField: false,
                    configureFormFieldType: false,
                    editFormField: false,
                    haveRequestTypes: !isEmpty(webForm.requestTypes) && !isEmpty(webForm.subjectTypes),
                    webForm,
                    webFormId,
                    formFields: response[0].result ? this.addStatusOnFormFields(response[0].data) : [],
                    formStylingInputs: response[1].result ? response[1].data : [],
                    settingsInputs: response[2].result ? this.buildSettingsInputs(response[2].data) : [],
                    formTranslations: response[3].result ? response[3].data.Translations : [],
                    languageSelected:  webForm.defaultLanguage,
                    attachmentUpload : {
                        type: "upload",
                        isRequired: this.settingsMap[DSARSettings.AttachmentSubmission]
                            ? this.settingsMap[DSARSettings.AttachmentSubmission].isRequired
                            : false,
                    },
                },
            });
            this.dispatch({ type: DSARStoreActions.DATA_LOADED, payload: { loading: false } });
            this.translateFormFieldOptions();
        });
    }

    translateFormFieldOptions(): void {
        each(this.dsarState.formFields, (input: IDSARFormField): IDSARFormField => {
            if (input.inputType === InputType.Multiselect && input.status === RequestTypeStatus.Add) {
                each(input.options, (option: IKeyValue<string>): IKeyValue<string> => {
                    option.value = this.dsarState.formTranslations[option.key];
                    return option;
                });
            }
            return input;
        });
    }

    public publishDSARWebForm(formID: string, callbackFn: any ): void {
        this.dispatch({type: DSARStoreActions.MAKE_RULES_DIRTY, payload: false});

        this.otModalService.create(
            PrepublishConfirmationModal,
            {
                isComponent: true,
                size: ModalSize.XSMALL,
            },
            {
                title: this.translations["PublishWebForm"],
                desc: this.dsarState.webForm.templateName,
                dialog: this.translations["AreYouSurePublishForm"],
                confirm: this.translations["AnyChangesToFormOverwrite"],
                cancel: this.translations["Cancel"],
                submit: this.translations["Publish"],
            },
        ).pipe(
            take(1),
        ).subscribe((isPublish: boolean) => {
            if (isPublish) {
                this.saveDSARWebForm(false, true);
                callbackFn();
            }
        });
    }

    public convertToList(options: Array<ILabelValue<string>>): string[] {
        return options.map((option) => option.label || option.value);
    }

    public saveDSARWebForm(isDraft: boolean, isLoaded: boolean): Promise<void> {

        if (this.dsarState.editFormField) {
            this.makeDirty("formFields");
        }
        this.dispatch({ type: DSARStoreActions.SAVING_PROGRESS, payload: { savingInProgress: true } });
        const promiseArr: any[] = [];
        if (this.isDirty("webForm")) {
            const webFormToSave: any = {
                templateName: this.dsarState.webForm.templateName,
                title: this.dsarState.webForm.title,
                welcomeText: this.dsarState.formTranslations["WelcomeText"],
                footerText: this.dsarState.formTranslations["FooterText"],
                thankYouText: this.dsarState.formTranslations["ThankYouText"],
                requestText: this.dsarState.formTranslations["RequestTypeText"],
                subjectText: this.dsarState.formTranslations["SubjectTypeText"],
                language: this.dsarState.languageSelected,
                submitButtonText: this.dsarState.formTranslations["SubmitButtonText"],
                attachmentInfoDesc: this.dsarState.formTranslations["AttachmentInfoDesc"],
                attachmentButtonText: this.dsarState.formTranslations["AttachmentButtonText"],
                attachmentSizeDescription: this.dsarState.formTranslations["FilesLargerThanSizeNotSupported"],
            };
            promiseArr.push(this.downWebformApiService.updateDefaultWebForm(this.dsarState.webFormId, webFormToSave));
        }
        if (this.isDirty("formStylingInputs")) {
            promiseArr.push(this.downWebformApiService.updateWebFormStylings(this.dsarState.webFormId, { data: this.dsarState.formStylingInputs.data }));
        }
        if (this.isDirty("settingsInputs")) {
            promiseArr.push(this.downWebformApiService.updateWebFormSettings(this.dsarState.webFormId, this.dsarState.settingsInputs ));
        }
        if (this.isDirty("formFields")) {
            if (this.permissions.canShow("DSARWebFormPublishV2")) {
                const countryField = this.dsarState.formFields.data.find((field) => field.fieldKey === "country");
                if (countryField && typeof countryField.options[0] === "object") {
                    countryField.options = this.convertToList(countryField.options);
                }
            }
            promiseArr.push(this.downWebformApiService.updateWebFormFields(this.dsarState.webFormId, { data: this.dsarState.formFields.data, language: this.dsarState.languageSelected }));
        }
        if (this.isDirty("subjectTypes")) {
            promiseArr.push(this.downWebformApiService.updateSubjectTypes(this.dsarState.webFormId, { subjectTypes: this.dsarState.webForm.subjectTypes, language: this.dsarState.languageSelected }));
        }
        if (this.isDirty("requestTypes")) {
            promiseArr.push(this.downWebformApiService.updateRequestTypes(this.dsarState.webFormId, { requestTypes: this.dsarState.webForm.requestTypes, language: this.dsarState.languageSelected }));
        }

        return Promise.all(promiseArr).then((responses: any[]): void => {
            if (isArray(responses) && responses.length) {
                const failedResponses: any[] = responses.filter((response: any) => !response.result);
                if (!failedResponses.length) {
                    this.notificationService.alertSuccess(this.translations["Success"], this.translations["AllChangesWereSaved"]);
                    if (this.isDirty("requestTypes") && this.isDirty("subjectTypes") && this.isDirty("formFields")) {
                        this.saveFormFields(responses[responses.length - 3]);
                        this.saveRequestTypes(responses[responses.length - 2]);
                        this.saveRequestTypes(responses[responses.length - 1], true);
                    } else if (this.isDirty("requestTypes") && this.isDirty("subjectTypes")) {
                        this.saveRequestTypes(responses[responses.length - 2]);
                        this.saveRequestTypes(responses[responses.length - 1], true);
                    } else if (this.isDirty("requestTypes") && this.isDirty("formFields")) {
                        this.saveFormFields(responses[responses.length - 2]);
                        this.saveRequestTypes(responses[responses.length - 1], true);
                    } else if (this.isDirty("subjectTypes") && this.isDirty("formFields")) {
                        this.saveFormFields(responses[responses.length - 2]);
                        this.saveRequestTypes(responses[responses.length - 1]);
                    } else if (this.isDirty("subjectTypes")) {
                        this.saveRequestTypes(responses[responses.length - 1]);
                    } else if (this.isDirty("requestTypes")) {
                        this.saveRequestTypes(responses[responses.length - 1], true);
                    } else if (this.isDirty("formFields")) {
                        this.saveFormFields(responses[responses.length - 1]);
                    }
                    this.dispatch({ type: DSARStoreActions.RESET_DIRTY, payload: null });
                } else {
                    this.notificationService.alertError(this.translations["Error"], this.translations["AllChangesWereNotSaved"]);
                }
            }
            if ((this.permissions.canShow("DSARWebFormPublish") || this.permissions.canShow("DSARWebFormPublishV2")) && isLoaded) {
                if ((this.permissions.canShow("DSARWebFormPublish") || this.permissions.canShow("DSARWebFormPublishV2")) && !isDraft) {
                    this.exportForm(false);
                } else {
                    this.exportForm(isDraft);
                }
            }
            this.dispatch({ type: DSARStoreActions.SAVING_PROGRESS, payload: { savingInProgress: false } });
            this.dispatch({ type: DSARStoreActions.EDIT_SIDEBAR_DISABLED, payload: { editSidebarEnabled: false } });
            this.dispatch({ type: DSARStoreActions.TOGGLE_EDIT_INPUT_MODE, payload: { editMode: false } });
            this.dispatch({ type: DSARStoreActions.SET_EDIT_INPUT, payload: { selectedEditInput: "" } });
            this.dispatch({
                type: DSARStoreActions.CONFIGURE_FORM_FIELD_DISABLED,
                payload: {
                            configureFormField: false,
                            selectedFormField: "",
                            formDirty: false,
                        },
            });
            this.resetState = merge({}, this.dsarState);
        });
    }

    private updatePublishLinks(webFormId?: string): IPromise<void> {
        if (this.fetchDataPromise) { return this.fetchDataPromise; }
        this.dispatch({ type: DSARStoreActions.FETCHING_DATA, payload: { loading: false } });
        this.fetchDataPromise = webFormId ? this.downWebformApiService.getWebFormById(webFormId) : this.downWebformApiService.getDefaultWebForm();
        this.fetchDataPromise.then((response: any): any => {
            if (response.result) {
                const webForm: IWebformFetchDataForExport = response.data;
                this.dsarState.published = webForm.published;
                this.dsarState.publishedPath = webForm.publishedPath;
                this.dsarState.publishedHost = webForm.publishedHost;
                this.dispatch({ type: DSARStoreActions.DATA_FETCHED, payload: { } });
                this.fetchDataPromise = null;
            } else {
                this.dispatch({ type: DSARStoreActions.DATA_NOT_FETCHED, payload: { loading: false } });
                this.fetchDataPromise = null;
            }
        });
        return this.fetchDataPromise;
    }

    private buildSettingsInputs(payload: IWebformSettings): IWebformSettings {
        this.settingsMap = reduce(payload.data, (settings: IStringMap<IWebFormSettingsInput>, value: IWebFormSettingsInput) => {
            settings[value["fieldName"]] = value;
            return settings;
        }, {});
        payload.data = [];
        forEach(this.settingsOrder, (settings: string): void => {
            const input = this.settingsMap[settings];
            if (input) {
                payload.data.push(input);
            }
        });
        return payload;
    }

    private addStatusOnFormFields(payload: IWebFormFieldResponse): IWebFormFieldResponse {
        if (!payload.data[0].status) {
            each(payload.data, (input: IDSARFormField): IDSARFormField => {
                input.status = this.requestTypeStatus.Add;
                return input;
            });
        }
        return payload;
    }

    private saveRequestTypes(response: IProtocolResponse<any>, requestType?: boolean): void {
        if (response.result) {
            response.data = sortBy(response.data, "order");
            forEach(response.data, (data: any) => {
                data.description = data.fieldName + "Desc";
            });
            this.dispatch({ type: DSARStoreActions.REQUEST_TYPES, payload: { fieldName: requestType ? "requestTypes" : "subjectTypes", value: response.data } });
        }
    }

    private saveFormFields(response: IProtocolResponse<any>): void {
        if (response.result) {
            this.dispatch({ type: DSARStoreActions.FORM_FIELDS, payload: { fieldName: "formFields", value: response.data }});
        }
    }

    private openPublishConfirmationModal(modalData: IWebformPublishResponse): void {
        this.modalService.setModalData(modalData);
        this.modalService.openModal("dsarPublishFormConfirmationModal");
    }

    private exportForm(isDraft: boolean): ng.IPromise<any> {
        const publishPromise = this.permissions.canShow("DSARWebFormPublishV2")
            ? this.downWebformApiService.publishDefaultWebFormV2(this.dsarState.webFormId, isDraft)
            : this.downWebformApiService.publishDefaultWebForm(this.dsarState.webFormId, isDraft);

        return publishPromise.then((response: IProtocolResponse<any>): IProtocolResponse<any> => {
            if (response.result) {
                this.publishWebFormResponse = response.data;
                this.downWebformApiService.hrefString = response.data.link;
                if (!isDraft) {
                    this.openPublishConfirmationModal(response.data);
                    this.notificationService.alertSuccess(this.translations["Success"], this.translations["DSARWebformScriptPublished"]);
                }
                this.updatePublishLinks(this.dsarState.webFormId);
            } else {
                this.notificationService.alertError(this.translations["Error"], this.translations["WebformCanNotBePublished"]);
            }
            return response;
        });
    }

    private reducer(state: any, action: any): any {
        switch (action.type) {
            case DSARStoreActions.DATA_FETCHED:
            case DSARStoreActions.FETCHING_DATA:
            case DSARStoreActions.DATA_NOT_FETCHED:
                return assign({}, state, action.payload);
            case DSARStoreActions.DATA_LOADED:
                this.resetState = merge({}, state, action.payload); // needed to make a reference to a new object
                if (state.formFields) {
                    state.formFields.data = map(state.formFields.data, (formField) => {
                        return omit(formField, "fieldName");
                     });
                }
                return assign({}, state, action.payload);
            case DSARStoreActions.REQUEST_TYPES:
                const requestTypeState: any = assign({}, state, {
                    ...state,
                    webForm: {
                        ...state.webForm,
                        [action.payload.fieldName]: action.payload.value,
                    },
                });
                this.resetState = requestTypeState;
                return requestTypeState;
            case DSARStoreActions.FORM_FIELDS:
                return assign({}, state, { formFields: action.payload.value });
            case DSARStoreActions.RESET_FORM:
                return merge({}, this.resetState);
            // for Inputs changed in form styling and settings
            case DSARStoreActions.INPUT_CHANGED:
            case DSARStoreActions.LOGO_UPLOADED:
                const updatedInputs: any = this.reduceInputs(state, action);
                if (action.payload.fieldName === DSARSettings.EnableReCaptcha && !action.payload.value) {
                    this.dispatch({type: DSARStoreActions.STORE_WIDGET_ID, payload: {widgetID: null}});
                }
                this.makeDirty(action.payload.panelName);
                return assign({}, state, { [action.payload.panelName]: updatedInputs });
            case DSARStoreActions.ATTACHMENT_TOGGLE_CHANGED:
                const updatedAttachment = { ...state.attachmentUpload, isRequired: action.payload.isRequired};
                const settings = { ...state.settingsInputs };
                forEach(settings.data, (setting: IWebFormSettingsInput): void => {
                    if (setting.fieldName === DSARSettings.AttachmentSubmission) {
                        setting.isRequired = action.payload.isRequired;
                    }
                });
                return assign({}, state, { settingsInputs: settings, attachmentUpload: updatedAttachment });
            // for Inputs changed in form name and text
            case DSARStoreActions.DEFAULT_WEBFORM_CHANGED:
                const webForm: any = { ...state.webForm, [action.payload.fieldName]: action.payload.value };
                const formTranslations: any = { ...state.formTranslations, [action.payload.fieldName]: action.payload.value };
                this.makeDirty("webForm");
                return assign({}, state, { webForm, formTranslations });
            // for Inputs changed in form request and fields
            case DSARStoreActions.INPUT_ADDED:
                const newInputsArr: any[] = [...state[action.payload.panelName][action.payload.panelProperty], action.payload.data];
                return this.inputsAddDelete(state, action, newInputsArr);
            case DSARStoreActions.INPUT_EDIT:
                const formTranslationsInputEdit = this.inputAddEditTranslation(state, action);
                this.makeDirty(action.payload.toDirty);
                return assign({}, state, { formTranslations: formTranslationsInputEdit });
            case DSARStoreActions.SET_EDIT_INPUT:
                return { ...state, selectedEditInput: action.payload.selectedEditInput };
            case DSARStoreActions.EDIT_SIDEBAR_ENABLED:
            case DSARStoreActions.EDIT_SIDEBAR_DISABLED:
                return { ...state, editSidebarEnabled: action.payload.editSidebarEnabled };
            case DSARStoreActions.TOGGLE_EDIT_INPUT_MODE:
                const editInput: any = get(state, state.selectedEditInput);
                if (editInput) { editInput.editMode = action.payload.editMode; }
                return { ...state };
            case DSARStoreActions.INPUT_DELETED:
                // TODO: keep blob commented till BE is fixed
                // let deleteInputsArr: any[] = [...state[action.payload.panelName][action.payload.panelProperty]];
                // deleteInputsArr = _(deleteInputsArr).filter((input: any): boolean => input !== action.payload.data).value();
                // return this.inputsAddDelete(state, action, deleteInputsArr);
                let deleteInputsArr: any[] = [...state[action.payload.panelName][action.payload.panelProperty]];
                if (action.payload.panelName !== "formFields" && action.payload.data.id === undefined
                    && action.payload.data.status === this.requestTypeStatus.Delete) {
                        deleteInputsArr = filter(deleteInputsArr, (input: any): boolean => {
                            return input.id === undefined ? input.status !== this.requestTypeStatus.Delete : true;
                        });
                }
                return this.inputsAddDelete(state, action, deleteInputsArr);

            // for moving inputs up and down
            case DSARStoreActions.INPUT_MOVE:
                // merge needed to make a new reference to the array
                const webFormInputs: any = merge({}, state[action.payload.panelName], { [action.payload.panelProperty]: action.payload.data });
                this.makeDirty(action.payload.toDirty);
                return assign({}, state, { [action.payload.panelName]: webFormInputs });
            case DSARStoreActions.INPUT_TOGGLED:
                this.makeDirty(action.payload.toDirty);
                return { ...state };
            case DSARStoreActions.HAVE_REQUEST_TYPES:
                return assign({}, state, { haveRequestTypes: action.payload });
            case DSARStoreActions.SAVING_PROGRESS:
                return { ...state, savingInProgress: action.payload.savingInProgress };
            case DSARStoreActions.ERROR_FORM:
                return { ...state, errorOnForm: action.payload.errorOnForm };
            case DSARStoreActions.RESET_DIRTY:
                const isDirty: any = this.resetDirty();
                return { ...state, isDirty };
            case DSARStoreActions.GENERATE_LINK:
                const generateScriptModalData: IGenerateScriptModal = {
                    promiseToResolve: null,
                    modalTitle: this.translations["WebFormLink"],
                    confirmationText: null,
                    submitButtonText: this.translations["CopyLink"],
                    cancelButtonText: this.translations["Cancel"],
                    webFormId: this.dsarState.webFormId,
                    publishedHost: this.dsarState.publishedHost,
                    publishedPath: this.dsarState.publishedPath,
                };
                this.modalService.setModalData(generateScriptModalData);
                this.modalService.openModal("downgradeGenerateScriptModal");
                return state;
            case DSARStoreActions.GENERATE_LINK_PUBLISHED:
                this.openPublishConfirmationModal(this.publishWebFormResponse);
                return state;
            case DSARStoreActions.ORG_CHANGED:
                const webFormWithOrg: any = { ...state[action.payload.panelName], [action.payload.fieldName]: action.payload.value };
                this.makeDirty("settingsInputs");
                return assign({}, state, { [action.payload.panelName]: webFormWithOrg });
            case DSARStoreActions.LANG_SWITCH:
                return assign({}, state, { formTranslations: action.payload.formTranslations, languageSelected: action.payload.languageSelected});
            case DSARStoreActions.LANG_PREFERENCE_SWITCH:
                return this.modifyMasterLanguageList(state, action.payload);
            case DSARStoreActions.MULTI_SELECT_TOGGLE:
                const toggleSettings = { ...state.settingsInputs };
                find(toggleSettings.data, ["fieldName", action.payload.type]).value = action.payload.value;
                this.makeDirty("settingsInputs");
                return assign({}, state, {settingsInputs: toggleSettings});
            case DSARStoreActions.DEFAULT_WORKFLOW_CHANGED:
                const workflowSettings = { ...state.settingsInputs };
                find(workflowSettings.data, ["fieldName", "Workflows"]).value = action.payload;
                this.makeDirty("settingsInputs");
                return assign({}, state, { settingsInputs: workflowSettings });
            case DSARStoreActions.STORE_WIDGET_ID:
                return assign({}, state, { widgetID: action.payload.widgetID });
            case DSARStoreActions.WEBFORM_DELETED:
                return assign({}, state, { isWebformDeleted: true });
            case DSARStoreActions.UPDATE_TEMPLATE_NAME:
                const webFormCopy = state.webForm;
                webFormCopy.templateName = action.payload;
                webFormCopy.published = false;
                return assign({}, state, { webForm: webFormCopy, published: false });
            case DSARStoreActions.CONFIGURE_FORM_FIELD_TYPE_ENABLED:
            case DSARStoreActions.CONFIGURE_FORM_FIELD_TYPE_DISABLED:
                return { ...state, configureFormFieldType: action.payload.configureFormFieldTypeEnabled };
            case DSARStoreActions.CLEAR_WEBFORM_CHANGES:
                this.resetDirty();
                return merge({}, this.resetState);
            case DSARStoreActions.CONFIGURE_INPUT_TYPE:
                const configuredInput = [...state["formFields"]["data"], action.payload];
                return set(state, "formFields.data", configuredInput);
            case DSARStoreActions.CONFIGURE_FORM_FIELD_ENABLED:
            case DSARStoreActions.CONFIGURE_FORM_FIELD_DISABLED:
                if (action.payload.formDirty) {
                    this.makeDirty("formFields");
                }
                if (action.payload.selectedFormField) {
                    action.payload.selectedFormField.editMode = true;
                }
                return {
                        ...state,
                        configureFormField: action.payload.configureFormField,
                        selectedFormField: action.payload.selectedFormField,
                        configureFormFieldType: false,
                        errorOnForm: action.payload.configureFormField,
                    };
            case DSARStoreActions.FORM_FIELD_EDIT:
                const formFieldTranslations = this.formFieldTranslation(state, action, action.payload.dirtyKey);
                return assign({}, state, {
                    formTranslations: formFieldTranslations,
                    editFormField: true,
                    errorOnForm: action.payload.errorOnForm,
                });
            case DSARStoreActions.DELETE_ADDED_INPUT:
                const inputArr = [...state[action.payload.panelName][action.payload.panelProperty]];
                const slicedInputArr = inputArr.slice(0, inputArr.length - 1);
                const data = action.payload.data;
                action.payload.dirtyKeys.forEach((dirtyKey) => {
                    if (dirtyKey === "options") {
                        data.options.forEach((key: string) => {
                            delete state.formTranslations[key];
                        });
                    } else if (dirtyKey === "fieldName") {
                        delete state.formTranslations[data["fieldKey"]];
                    } else {
                        delete state.formTranslations[data["description"]];
                    }
                });
                return set(state, "formFields.data", slicedInputArr);
            case DSARStoreActions.MAKE_RULES_DIRTY:
                return {...state, rulesDirty: action.payload};
            default:
                return state;
        }
    }

    private formFieldTranslation(state: any, action: any, dirtyField: string): IStringMap<string> {
        let translationKey = "";
        let translationValue = "";
        if (action.payload.data.fieldName && dirtyField === "fieldName") {
            translationKey = action.payload.data.fieldKey;
            translationValue = action.payload.data.fieldName;
            state.formTranslations[translationKey] = translationValue;
        }
        // allow blank strings
        if ((action.payload.data.descriptionValue || action.payload.data.descriptionValue === "") && dirtyField === "descriptionValue") {
            translationKey = action.payload.data.description;
            translationValue = action.payload.data.descriptionValue;
            state.formTranslations[translationKey] = translationValue;
        }
        if (action.payload.data.options && dirtyField === "options") {
            each(action.payload.data.options, (option: IKeyValue<string>): void => {
                translationKey = option.key;
                translationValue = option.value;
                state.formTranslations[translationKey] = translationValue;
            });
        }

        const formTranslations = { ...state.formTranslations };
        return formTranslations;
    }

    // Finds the index inside the array of the inputs e.g. settings inputs that needs to be updated
    // and update that index with the needed data
    private reduceInputs(state: any, action: any): any {
        // curry function - key is fieldName, value is action.payload.fieldName, elem is findByFieldName
        const findByKeyValuePair: any = (key: any): any => (value: any): any => (elem: any): any => elem[key] === value;
        const findByFieldName: any = findByKeyValuePair("fieldName")(action.payload.fieldName);
        const index: number = state[action.payload.panelName].data.findIndex(findByFieldName);
        const inputs: any[] = [
            ...state[action.payload.panelName].data.slice(0, index),
            assign({}, state[action.payload.panelName].data[index], { value: action.payload.value }),
            ...state[action.payload.panelName].data.slice(index + 1),
        ];
        return assign({}, state[action.payload.panelName], { data: inputs });
    }

    private inputsAddDelete(state: any, action: any, inputsArr: any[]): any {
        const webFormInputs: any = assign({}, state[action.payload.panelName], { [action.payload.panelProperty]: inputsArr });
        const formTranslations = this.inputAddEditTranslation(state, action);
        this.makeDirty(action.payload.toDirty);
        return assign({}, state, { [action.payload.panelName]: webFormInputs, formTranslations });
    }

    private inputAddEditTranslation(state: any, action: any): IStringMap<string> {
        let translationKey = "";
        let translationValue = "";
        if (action.payload.panelName === "formFields") {
            if (action.payload.data.fieldName && action.payload.data.dirtyKey === "fieldName") {
                translationKey = action.payload.data.fieldKey;
                translationValue = action.payload.data.fieldName;
                state.formTranslations[translationKey] = translationValue;
            }
        } else {
            if (action.payload.data.fieldName !== "" && action.payload.data.dirtyKey === "fieldValue") {
                translationKey = action.payload.data.fieldName;
                translationValue = action.payload.data.fieldValue;
                state.formTranslations[translationKey] = translationValue;
            }
        }
        // allow blank strings
        if ((action.payload.data.descriptionValue || action.payload.data.descriptionValue === "") && action.payload.data.dirtyKey === "descriptionValue") {
            translationKey = action.payload.data.description;
            translationValue = action.payload.data.descriptionValue;
            state.formTranslations[translationKey] = translationValue;
        }

        const formTranslations = { ...state.formTranslations };
        return formTranslations;
    }

    private makeDirty(webFormAttr: string): void {
        this.dsarState.changesMadeToForm = true;
        this.dsarState.isDirty[webFormAttr]++;
    }

    private isDirty(webFormAttr: string): boolean {
        return this.dsarState.isDirty[webFormAttr] > 0;
    }

    private resetDirty(): any {
        this.dsarState.changesMadeToForm = false;
        this.dsarState.editFormField = false;
        return {
            webForm: 0,
            formFields: 0,
            formStylingInputs: 0,
            settingsInputs: 0,
            requestTypes: 0,
            subjectTypes: 0,
        };
    }

    get DSARState(): any {
        return this.dsarState;
    }

    set DSARState(state: any) {
        throw new Error("Setting the DSAR State this way is forbidden. Use dispatcher instead");
    }

}
