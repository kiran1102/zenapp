// Core
import { NgModule } from "@angular/core";
import {
    CommonModule,
    CurrencyPipe,
} from "@angular/common";
import { HttpClientModule } from "@angular/common/http";
import { FormsModule } from "@angular/forms";
import { ReactiveFormsModule } from "@angular/forms";
import {
    VitreusModule,
    OtCountCardModule,
} from "@onetrust/vitreus";
import { OtDataMapsModule } from "@onetrust/vitreus";
import { DndListModule } from "ngx-drag-and-drop-lists";
import { OTPipesModule, PIPES } from "modules/pipes/pipes.module";
import { VirtualScrollModule } from "od-virtualscroll";
import { OneVideoModule } from "modules/video/video.module";

// Modules
import { SharedModule } from "sharedModules/shared.module";

// Services
import { DsarDashboardApiService } from "./shared/services/dsar-dashboard-api.service";
import { DsarDashboardService } from "./shared/services/dsar-dashboard.service";

// Components
import { DSARDashboardLineChartComponent } from "./dashboard/dsar-dashboard-line-chart/dsar-dashboard-line-chart.component";
import { DSARDashboardComponent } from "./dashboard/dsar-dashboard.component";
import { DSARDashboardCountryMapComponent } from "./dashboard/dsar-dashboard-country-map/dsar-dashboard-country-map.component";
import { DSARDashboardRequestTypesChartComponent } from "./dashboard/dsar-dashboard-request-type-chart/dsar-dashboard-request-types-chart.component";
import { DSARDashboardSubjectTypesChartComponent } from "./dashboard/dsar-dashboard-subject-types-chart/dsar-dashboard-subject-types-chart.component";
import { DSARDashboardCountryMapRangeIndicatorComponent } from "./dashboard/dsar-dashboard-country-map/dsar-dashboard-country-map-range-indicator/dsar-dashboard-country-map-range-indicator.component";
import { DSARDashboardMetricsHeaderComponent } from "./dashboard/dsar-dashboard-metrics-header/dsar-dashboard-metrics-header.component";

@NgModule({
    imports: [
        SharedModule,
        CommonModule,
        HttpClientModule,
        FormsModule,
        ReactiveFormsModule,
        VitreusModule,
        DndListModule,
        OTPipesModule,
        VirtualScrollModule,
        OneVideoModule,
        OtDataMapsModule,
        OtCountCardModule,
    ],
    declarations: [
        DSARDashboardComponent,
        DSARDashboardLineChartComponent,
        DSARDashboardCountryMapComponent,
        DSARDashboardRequestTypesChartComponent,
        DSARDashboardSubjectTypesChartComponent,
        DSARDashboardCountryMapRangeIndicatorComponent,
        DSARDashboardMetricsHeaderComponent,
    ],
    providers: [
        DsarDashboardApiService,
        DsarDashboardService,
        CurrencyPipe,
    ],
    entryComponents: [
        DSARDashboardComponent,
    ],
})
export class DSARDashboardModule { }
