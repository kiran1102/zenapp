// Angular
import {
    Component,
    Input,
} from "@angular/core";

// Services
import { DsarDashboardService } from "modules/dsar/dsar-dashboard/shared/services/dsar-dashboard.service";

// Interfaces
import { ICountryMapRangeIndicatorData } from "../../../shared/interfaces/dashboard.interface";

@Component({
    selector: "dsar-dashboard-country-map-range-indicator",
    templateUrl: "./dsar-dashboard-country-map-range-indicator.component.html",
})

export class DSARDashboardCountryMapRangeIndicatorComponent {
    @Input() totalRequests: number;

    defaultData: ICountryMapRangeIndicatorData[] = [
            {
                className: "blue",
                showCircle: true,
                showRightTriangle: true,
                baseClassName: "width-25-percent",
                data: "25",
            },
            {
                className: "yellow",
                showCircle: true,
                baseClassName: "width-25-percent",
                data: "50",
            },
            {
                className: "pink",
                showCircle: true,
                baseClassName: "width-25-percent",
                data: "75",
            },
            {
                className: "red",
                showCircle: false,
                showLeftTriangle: true,
                baseClassName: "width-25-percent",
                data: "100",
            },
        ];

    data: ICountryMapRangeIndicatorData[] = [];

    constructor(
        private dsarDashboardService: DsarDashboardService,
    ) { }

    generateData() {
        let data = [...this.defaultData];
        if (this.totalRequests === 1) {
            data[3].baseClassName = "width-100-percent";
            data[3].showRightTriangle = true;
            data = [data[3]];
        } else if (this.totalRequests === 2) {
            data[3].baseClassName = "width-50-percent";
            data[2].baseClassName = "width-50-percent";
            data[2].showRightTriangle = true;
            data[3].data = "2";
            data[2].data = "1";
            data = [data[2], data[3]];
        } else if (this.totalRequests === 3) {
            data[3].baseClassName = "width-33-percent";
            data[2].baseClassName = "width-33-percent";
            data[1].baseClassName = "width-33-percent";
            data[1].data = "1";
            data[2].data = "2";
            data[3].data = "3";
            data[1].showRightTriangle = true;
            data = [data[1], data[2], data[3]];
        } else {
            const interval = Math.floor(this.totalRequests / 4);
            for (let i = 0, index = 1; i < data.length - 1; i++, index++) {
                data[i].data = this.dsarDashboardService.normalizeCount(interval * index);
            }
        }
        return data;
    }

    ngOnChanges() {
        this.data = this.generateData();
        this.data = this.data.map((d) => {
            return {
                ...d,
                className: "dsar-dashboard-country-map-range-indicator__base--" + d.className,
            };
        });
    }
}
