// Angular
import {
    Component,
    OnInit,
} from "@angular/core";

// Rxjs
import { BehaviorSubject } from "rxjs";
import { take } from "rxjs/operators";

// Services
import { DsarDashboardApiService } from "../../shared/services/dsar-dashboard-api.service";
import { DsarDashboardService } from "../../shared/services/dsar-dashboard.service";

// Constants and enums
import { DSARChartTypes } from "../../shared/constants/dsar-dashboard.constants";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

// Interfaces
import {
    IDataMapConfig,
    IGeographyConfig,
} from "@onetrust/vitreus/src/app/vitreus/components/data-maps/data-maps.interface";
import { ICountryPopupData } from "../../shared/interfaces/dashboard.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";
import { IFilterGetData } from "modules/dsar/interfaces/dsar-sidebar-drawer.interface";

@Component({
    selector: "dsar-dashboard-country-map",
    templateUrl: "./dsar-dashboard-country-map.component.html",
})

export class DSARDashboardCountryMapComponent implements OnInit  {
    loading = true;
    config: IDataMapConfig = null;
    config$ = new BehaviorSubject<IDataMapConfig>(null);
    translations = {
        requestNumber: this.translatePipe.transform("RequestNumber"),
        amongAllRequests: this.translatePipe.transform("AmongAllRequests"),
    };

    constructor(
        private dsarDashboardApiService: DsarDashboardApiService,
        private translatePipe: TranslatePipe,
        private dsarDashboardService: DsarDashboardService,
    ) {}

    ngOnInit() {
        this.dsarDashboardApiService.getDashboardCountryMapData()
            .pipe(take(1))
            .subscribe((response: IProtocolResponse<IDataMapConfig>) => {
                if (response.data) {
                    this.config = response.data;
                    this.config.geographyConfig.popupTemplate = this.popupTemplate;
                    this.config.enableClick = true;
                    this.config.geographyConfig.popupConfig.placement = "auto";
                }
                this.loading = false;
                this.config$.next(this.config);
            });
    }

    popupTemplate =  (d: IGeographyConfig, countryData: ICountryPopupData) => {
        let template = "";
        if (countryData && countryData.data) {
            template = `
                <div class="datamaps__popover--host datamaps__host">
                    <div class="column-wrap text-left padding-all-1">
                        <div class="text-bold">${countryData.properties.name}</div>
                        <div>${this.translations.requestNumber}: ${countryData.data.numberOfRequests}</div>
                        <div>${this.translations.amongAllRequests}: ${countryData.data.percentage}%</div>
                    </div>
                </div>
                <div class="datamaps__popover--arrow"></div>`;
        }
        return template;
    }

    click(countryData: ICountryPopupData) {
        let filterData: IFilterGetData;
        if (countryData.data) {
            filterData = this.dsarDashboardService.formatToFilterData(countryData.data, DSARChartTypes.Country);
            if (filterData) {
                this.dsarDashboardService.redirectToRequestQueueGrid(filterData);
            }
        }
    }

}
