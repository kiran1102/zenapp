// Angular
import {
    Component,
    OnInit,
    Input,
} from "@angular/core";

// Lodash
import {
    sortBy,
    isNil,
    isEmpty,
} from "lodash";

import {
    take,
    filter,
} from "rxjs/operators";

// Services
import { Permissions } from "modules/shared/services/helper/permissions.service";
import { DSARSettingsService } from "dsarservice/dsar-settings.service";
import { DsarDashboardApiService } from "../../shared/services/dsar-dashboard-api.service";
import { DsarDashboardService } from "../../shared/services/dsar-dashboard.service";

// Enums
import { Month } from "dsarModule/enums/month.enum";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

// Interfaces
import {
    IDSARSettings,
    IDSARCostCalcSettings,
} from "dsarModule/interfaces/dsar-settings.interface";
import { IDSARSettingsGroup } from "dsarModule/interfaces/dsar-settings-group.interface";

import { IProtocolResponse } from "interfaces/protocol.interface";
import {
    ILineChart,
    IDashboardHeader,
    IDashboardData,
} from "modules/dsar/dsar-dashboard/shared/interfaces/dashboard.interface";
import { IStringMap } from "interfaces/generic.interface";

// todo: WEBPACK 4 Type fixes Temporary type fix
declare class Chart {
    static Event: any;

    constructor(
        context: string | CanvasRenderingContext2D | HTMLCanvasElement | ArrayLike<CanvasRenderingContext2D | HTMLCanvasElement>,
        options: any,
    );
}

@Component({
    selector: "dsar-dashboard-line-chart",
    templateUrl: "./dsar-dashboard-line-chart.component.html",
})

export class DSARDashboardLineChartComponent implements OnInit  {

    currencyIso: string;
    hourlyCost: number;
    isCostCalcEnabled: boolean;
    costCalculationSettings: IDSARSettings[];
    ready = false;
    headerData: IDashboardHeader;

    private translations: IStringMap<string>;
    private yAxisPoints: number[] = [];
    private xAxisPoints: string[] = [];

    constructor(
        private permissions: Permissions,
        private translatePipe: TranslatePipe,
        private dsarDashboardApiService: DsarDashboardApiService,
        private dsarDashboardService: DsarDashboardService,
        private dsarSettingsService: DSARSettingsService,
    ) { }

    ngOnInit(): void {

        this.translations = {
            january: this.translatePipe.transform(Month[1]),
            february: this.translatePipe.transform(Month[2]),
            march: this.translatePipe.transform(Month[3]),
            april: this.translatePipe.transform(Month[4]),
            may: this.translatePipe.transform(Month[5]),
            june: this.translatePipe.transform(Month[6]),
            july: this.translatePipe.transform(Month[7]),
            august: this.translatePipe.transform(Month[8]),
            september: this.translatePipe.transform(Month[9]),
            october: this.translatePipe.transform(Month[10]),
            november: this.translatePipe.transform(Month[11]),
            december: this.translatePipe.transform(Month[12]),
            ticketsSubmitted: this.translatePipe.transform("TotalRequestsSubmitted"),
            totalNumOfTickets: this.translatePipe.transform("Total#OfRequests"),
            months: this.translatePipe.transform("Months"),
        };
        this.getDataPoints();
    }

    private getDataPoints(): void {
        this.dsarDashboardApiService.getDashboardHeader()
        .pipe(
            take(1),
            filter((response) => response.result),
        )
        .subscribe((response: IProtocolResponse<IDashboardHeader>) => {
            this.headerData = response.data;
            if (this.permissions.canShow("DSARCostCalculationView")) {
                this.getCostCalculationData().then(() => {
                    this.ready = true;
                });
            } else {
                this.ready = true;
            }
        });
        if (this.permissions.canShow("DSARCasesByMonth")) {
            this.ready = false;
            this.dsarDashboardApiService.getTotalCasesByMonth()
                .pipe(
                    take(1),
                    filter((response) => response.result),
                )
                .subscribe((response: IProtocolResponse<IDashboardData>) => {
                    response.data.casesByMonth = sortBy(response.data.casesByMonth, ["year", "month"]);
                    response.data.casesByMonth.forEach((ticket: ILineChart): void => {
                        this.yAxisPoints.push(ticket.totalTickets);
                        const label = this.translations[Month[ticket.month].toLowerCase()] + "-" + ticket.year.toString().substring(2);
                        this.xAxisPoints.push(label);
                    });
                    this.ready = true;
                    setTimeout(() => {
                        this.generateChart();
                    }, 500);
                });
        }
    }

    private getCostCalculationData(): any {
        const DSARCostCalcSettings: IDSARCostCalcSettings = JSON.parse(sessionStorage.getItem("DSARCostCalcSettings"));
        if (isNil(DSARCostCalcSettings) || isEmpty(DSARCostCalcSettings)) {
            return this.dsarSettingsService.getSettingsByGroup("CostCalculation").then((res: IProtocolResponse<IDSARSettingsGroup>): boolean => {
                if (res.result && res.data) {
                    this.costCalculationSettings = res.data.settings;
                    const costCalcEnabledSetting: IDSARSettings = this.costCalculationSettings.find((setting: IDSARSettings) => setting.name === "Enabled");
                    const costCalcHourlySetting: IDSARSettings = this.costCalculationSettings.find((setting: IDSARSettings) => setting.name === "CostPerHour");
                    const costCurrencySetting: IDSARSettings = this.costCalculationSettings.find((setting: IDSARSettings) => setting.name === "Currency");
                    this.isCostCalcEnabled = costCalcEnabledSetting.value === "true";
                    this.hourlyCost = parseFloat(costCalcHourlySetting.value) || 0;
                    this.currencyIso = costCurrencySetting.value;
                    this.persistCostCalcSettingsLocally();
                    this.dsarDashboardService.metrics$.next({
                        ...this.headerData,
                        hourlyCost: this.hourlyCost,
                        currencyIso: this.currencyIso,
                    });
                }
                return res.result;
            });
        } else {
            this.isCostCalcEnabled = DSARCostCalcSettings.isCostCalcEnabled;
            this.hourlyCost = DSARCostCalcSettings.hourlyCost;
            this.currencyIso = DSARCostCalcSettings.currencyIso;
            this.dsarDashboardService.metrics$.next({
                ...this.headerData,
                hourlyCost: this.hourlyCost,
                currencyIso: this.currencyIso,
            });
            return new Promise((resolve) => resolve(true));
        }
    }

    private persistCostCalcSettingsLocally(): void {
        const costCalcSettings: IDSARCostCalcSettings = {
            isCostCalcEnabled: this.isCostCalcEnabled,
            hourlyCost: this.hourlyCost,
            currencyIso: this.currencyIso,
        };
        sessionStorage.setItem("DSARCostCalcSettings", JSON.stringify(costCalcSettings));
    }

    private generateChart(): void {
        this.ready = true;
        const ctx = document.getElementById("dsar-line-chart") as HTMLCanvasElement;

        import(/* webpackChunkName: "chart" */ "chart.js").then((m) => {

            const myChart: Chart = new Chart(ctx, {
                type: "line",
                data: {
                    labels: this.xAxisPoints,
                    datasets: [
                        {
                            label: this.translations.ticketsSubmitted,
                            data: this.yAxisPoints,
                            fill: "start",
                            borderColor: ["#1F96DB"],
                            backgroundColor: "rgba(31, 150, 219, .5)",
                        },
                    ],
                },
                options: {
                    legend: {
                        // // todo: WEBPACK 4 Type fixes Temporary type fix
                        // onClick: (e: Chart.Event): void => e.stopPropagation(),
                        onClick: (e: any): void => e.stopPropagation(),
                    },
                    scales: {
                        yAxes: [
                            {
                                scaleLabel: {
                                    display: true,
                                    labelString: this.translations.totalNumOfTickets,
                                },
                            },
                        ],
                        xAxes: [
                            {
                                scaleLabel: {
                                    display: true,
                                    labelString: this.translations.months,
                                },
                            },
                        ],
                    },
                    elements: {
                        line: {
                            tension: 0,
                        },
                    },
                    animation: {
                        duration: 0,
                    },
                    hover: {
                        animationDuration: 0,
                    },
                    responsiveAnimationDuration: 0,
                    maintainAspectRatio: false,
                    responsive: true,
                },
            });
        });
    }
}
