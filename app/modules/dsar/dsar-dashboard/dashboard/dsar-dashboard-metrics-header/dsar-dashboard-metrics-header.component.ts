// Angular
import { Component } from "@angular/core";

// Rxjs
import { combineLatest } from "rxjs";

// Services
import { DsarDashboardService } from "../../shared/services/dsar-dashboard.service";
import { TranslateService } from "@onetrust/common";

// Constants and Enum
import { OtCountCardSize } from "@onetrust/vitreus";

// Pipes
import { CurrencyPipe } from "@angular/common";

// Interfaces
import {
    IDashboardMetrics,
    ICard,
} from "../../shared/interfaces/dashboard.interface";
import { IStringMap } from "interfaces/generic.interface";

@Component({
    selector: "dsar-dashboard-metrics-header",
    templateUrl: "./dsar-dashboard-metrics-header.component.html",
})

export class DSARDashboardMetricsHeaderComponent {
    translationKeys = [
        "Requests",
        "Days",
        "Completed",
        "Received",
        "AverageCost",
        "AverageTimeToComplete",
    ];
    translations: IStringMap<string> = {};
    cards: [ICard[], ICard[]];
    isLoading = true;

    constructor(
        private translateService: TranslateService,
        private dsarDashboardService: DsarDashboardService,
        private currencyPipe: CurrencyPipe,
    ) { }

    getCards(translations, header: IDashboardMetrics): [ICard[], ICard[]] {
        const cards: [ICard[], ICard[]] = [
            [
                {
                    amount: this.dsarDashboardService.normalizeCount(header.totalTickets),
                    title: translations.Requests,
                    description: translations.Received,
                    size: OtCountCardSize.SMALL,
                    color: "#2C98DA",
                },
                {
                    amount: this.dsarDashboardService.normalizeCount(header.completedTickets),
                    title: translations.Requests,
                    description: translations.Completed,
                    size: OtCountCardSize.SMALL,
                    color: "#6CC04A",
                },
            ],
            [
                {
                    amount: header.turnAroundDays,
                    title: translations.Days,
                    size: OtCountCardSize.SMALL,
                    description: translations.AverageTimeToComplete,
                },
                {
                    amount: this.currencyPipe.transform(header.avgHoursWorked * header.hourlyCost, header.currencyIso, "symbol-narrow"),
                    title: header.currencyIso,
                    description: translations.AverageCost,
                    size: OtCountCardSize.SMALL,
                    color: "#FD5B65",
                },
            ],
        ];
        if (header.avgHoursWorked === null) {
            cards[1] = [cards[1][0]];
        }
        return cards;
    }

    ngOnInit() {
        combineLatest(
            this.translateService.stream(this.translationKeys),
            this.dsarDashboardService.metrics$,
        )
        .subscribe(([translations, headerResponse]) => {
            this.translations = translations;
            this.cards = this.getCards(this.translations, headerResponse);
            this.isLoading = false;
        });

    }

}
