// Angular
import { Component, OnInit } from "@angular/core";

// Rxjs
import { take } from "rxjs/operators";

// Services
import { DsarDashboardApiService } from "../../shared/services/dsar-dashboard-api.service";
import { DsarDashboardService } from "../../shared/services/dsar-dashboard.service";

// Constants and enums
import {
    DSARChartTypes,
    BAR_CHART_HEIGHT,
} from "../../shared/constants/dsar-dashboard.constants";

// Interfaces
import {
    IChartInputData,
    IChartDataElement,
} from "@onetrust/vitreus";
import { IProtocolResponse } from "interfaces/protocol.interface";
import { IBarChartData, IBarChartInputData } from "../../shared/interfaces/dashboard.interface";
import { IFilterGetData } from "modules/dsar/interfaces/dsar-sidebar-drawer.interface";

@Component({
    selector: "dsar-dashboard-subject-types-chart",
    templateUrl: "./dsar-dashboard-subject-types-chart.component.html",
})

export class DSARDashboardSubjectTypesChartComponent implements OnInit {
    height = BAR_CHART_HEIGHT;
    chartData: IChartInputData;
    barData: IBarChartData;
    emptyState = false;
    ready = false;

    constructor(
        private dsarDashboardApiService: DsarDashboardApiService,
        private dsarDashboardService: DsarDashboardService,
    ) {}

    ngOnInit() {
        this.dsarDashboardApiService.getDashboardBarChartData("SubjectType")
            .pipe(take(1))
            .subscribe((res: IProtocolResponse<IBarChartData>) => {
                if (res.data) {
                    this.barData = res.data;
                    this.chartData = this.dsarDashboardService.formatBarChartData(res.data);
                    this.emptyState = !this.chartData.values.length;
                }
                this.ready = true;
            });
    }

    handleAction(data: IChartDataElement) {
        const dataToFilter = this.barData.ichartDataElement.find((element: IBarChartInputData) => element.id === data.id);
        let filterData: IFilterGetData;
        if (data) {
            filterData = this.dsarDashboardService.formatToFilterData(dataToFilter, DSARChartTypes.SubjectType);
            if (filterData) {
                this.dsarDashboardService.redirectToRequestQueueGrid(filterData);
            }
        }
    }

}
