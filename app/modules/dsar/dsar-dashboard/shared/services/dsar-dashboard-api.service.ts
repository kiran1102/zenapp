import { Injectable, Inject } from "@angular/core";

// Rxjs
import {
    Observable,
    from,
} from "rxjs";

// Constants
import { DSARDashboardApiEndpoints } from "dsarModule/dsar-dashboard/shared/constants/dsar-dashboard-api-endpoints.constants";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

// Services
import { ProtocolService } from "modules/core/services/protocol.service";
import { TenantTranslationService } from "sharedServices/tenant-translation.service";

// Interfaces
import {
    IProtocolConfig,
    IProtocolMessages,
    IProtocolResponse,
} from "interfaces/protocol.interface";
import {
    IDashboardData,
    IDashboardHeader,
    IBarChartData,
} from "../interfaces/dashboard.interface";
import { DSARChartTypes } from "../constants/dsar-dashboard.constants";
import { IDataMapConfig } from "@onetrust/vitreus/src/app/vitreus/components/data-maps/data-maps.interface";

@Injectable()
export class DsarDashboardApiService {

    constructor(
        private oneProtocol: ProtocolService,
        private translatePipe: TranslatePipe,
        @Inject("TenantTranslationService") private tenantTranslationService: TenantTranslationService,
    ) {
    }

    getTotalCasesByMonth(): Observable<IProtocolResponse<IDashboardData>> {
        const config: IProtocolConfig = this.oneProtocol.customConfig("GET", DSARDashboardApiEndpoints.casesByMonth);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("ErrorGettingCaseChartData") } };
        return from(this.oneProtocol.http(config, messages));
    }

    getDashboardHeader(): Observable<IProtocolResponse<IDashboardHeader>> {
        const config: IProtocolConfig = this.oneProtocol.customConfig("GET", DSARDashboardApiEndpoints.header);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("ErrorGettingCaseHeaderData") } };
        return from(this.oneProtocol.http(config, messages));
    }

    getDashboardBarChartData(type: string): Observable<IProtocolResponse<IBarChartData>> {
        const userSessionLanguage = this.tenantTranslationService.getCurrentLanguage();
        const config: IProtocolConfig = this.oneProtocol.customConfig("GET", `${DSARDashboardApiEndpoints.requestsByType}/${userSessionLanguage}?requestsByType=${type}`);
        const errorKey = type === DSARChartTypes.RequestType ? "DSARRequestTypeChartError" : "DSARSubjectTypeChartError";
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform(errorKey) } };
        return from(this.oneProtocol.http(config, messages));
    }

    getDashboardCountryMapData(): Observable<IProtocolResponse<IDataMapConfig>> {
        const config: IProtocolConfig = this.oneProtocol.customConfig("GET", `${DSARDashboardApiEndpoints.countryMapData}`);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("DSARCountryMapError") } };
        return from(this.oneProtocol.http(config, messages));
    }

}
