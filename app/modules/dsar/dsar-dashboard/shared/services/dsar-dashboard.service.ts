import { Injectable } from "@angular/core";
import { StateService } from "@uirouter/core";

// Rxjs
import { Subject } from "rxjs";

// Constants and enums
import { MAX_LABEL_LENGTH } from "../constants/dsar-dashboard.constants";
import { FilterOperators } from "modules/dsar/constants/dsar-filter.constants";
import { DSARChartTypes } from "../../shared/constants/dsar-dashboard.constants";

// Interfaces
import {
    IBarChartData,
    IBarChartInputData,
    ICountryData,
    IDashboardMetrics,
} from "../interfaces/dashboard.interface";
import {
    IChartInputData,
    IChartDataElement,
} from "@onetrust/vitreus";
import { IFilterGetData } from "modules/dsar/interfaces/dsar-sidebar-drawer.interface";

@Injectable()
export class DsarDashboardService {
    metrics$ = new Subject<IDashboardMetrics>();

    constructor(private stateService: StateService) {}

    formatBarChartData(barData: IBarChartData): IChartInputData {
        const chartData: IChartInputData = {
            id: "",
            values: [],
        };
        chartData.id = barData.id;
        if (barData.ichartDataElement) {
            chartData.values = barData.ichartDataElement.map((data, index): IChartDataElement => {
                let label;
                if (data.label.length > MAX_LABEL_LENGTH) {
                    label = data.label.substr(0, 15) + "...";
                }
                return {
                    label: label || data.label,
                    id: data.id,
                    backgroundColor: data.backgroundColor,
                    value: data.value,
                };
            });
        }
        return chartData;
    }

    formatToFilterData(data: IBarChartInputData | ICountryData, type: string): IFilterGetData {
        const filterData: IFilterGetData = {
            operator: "EQ",
            attributeKey: type,
            fromValue: [],
            dataType: FilterOperators.Prepopulated,
        };
        switch (type) {
            case DSARChartTypes.RequestType:
            case DSARChartTypes.SubjectType:
                return {
                    ...filterData,
                    fromValue: (data as IBarChartInputData).typeIds ,
                    dataType: FilterOperators.Prepopulated,
                };
            case DSARChartTypes.Country:
                return {
                    ...filterData,
                    fromValue: [(data as ICountryData).countryCode],
                    dataType: FilterOperators.Prepopulated,
                };
        }
    }

    redirectToRequestQueueGrid(filters: IFilterGetData) {
        this.stateService.go("zen.app.pia.module.dsar.views.queue", ({ filters: [filters] }));
    }

    normalizeCount(count: number): string {
        if (count > 1000) {
            if (!(count % 1000)) {
                return (count / 1000) + "k";
            }
            return Math.round((count / 1000)).toFixed(1) + "k";
        }
        return count.toString();
    }
}
