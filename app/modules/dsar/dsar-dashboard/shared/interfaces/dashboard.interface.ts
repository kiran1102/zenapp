import { OtCountCardSize } from "@onetrust/vitreus";

export interface ILineChart {
    totalTickets: number;
    month: number;
    year: number;
}
export interface IDashboardHeader {
    totalTickets: number;
    completedTickets: number;
    turnAroundDays: number;
    avgHoursWorked?: number;
}

export interface IDashboardMetrics extends IDashboardHeader {
    hourlyCost: number;
    currencyIso: string;
}

export interface ICard {
    amount: number | string;
    title: string;
    description: string;
    size: OtCountCardSize;
    color?: string;
    maxHeight?: string;
    maxWidth?: string;
}

export interface IDashboardData {
    casesByMonth: ILineChart[];
}

export interface ICountryData {
    numberOfRequests: number;
    id: string;
    fillColor: string;
    percentage: string;
    countryCode: string;
}

export interface ICountryPopupData {
    data: ICountryData;
    properties: {
        name: string;
    };
}

export interface ICountryMapRangeIndicatorData {
    className: string;
    baseClassName: string;
    data: string;
    showCircle?: boolean;
    showRightTriangle?: boolean;
    showLeftTriangle?: boolean;
}

export interface IBarChartData {
    id: string;
    ichartDataElement: IBarChartInputData[];
}

export interface IBarChartInputData {
    label: string;
    id: string;
    backgroundColor: string;
    value: number;
    typeIds: string[];
}
