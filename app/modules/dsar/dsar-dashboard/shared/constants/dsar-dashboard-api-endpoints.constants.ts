import { DSARApiEndpoints } from "dsarModule/shared/constants/dsar-api-endpoints.constants";

export const DSARDashboardApiEndpoints = {
    casesByMonth: `${DSARApiEndpoints.baseUrl}/dashboard/cases/month`,
    header: `${DSARApiEndpoints.baseUrl}/dashboard/header`,
    requestsByType: `${DSARApiEndpoints.baseUrl}/dashboard/cases/requestsByType`,
    countryMapData: `${DSARApiEndpoints.baseUrl}/dashboard/requests/country`,
};
