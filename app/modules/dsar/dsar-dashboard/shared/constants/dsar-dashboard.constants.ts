export const MAX_LABEL_LENGTH = 15;
export const BAR_CHART_HEIGHT = 302;

export enum DSARChartTypes {
    RequestType = "RequestType",
    SubjectType = "SubjectType",
    Country = "Country",
}
