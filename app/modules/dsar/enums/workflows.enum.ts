export enum WorkflowsStatus {
    Active = 10,
    Draft = 20,
}

export enum WorkflowSettingsLabel {
    MultiFactorAuthentication = "MultiFactorAuthentication",
    IncludeNumbers = "IncludeNumbers",
    IncludeSpecialCharacters = "IncludeSpecialCharacters",
    IncludeLowerCase = "IncludeLowercase",
    IncludeUpperCase = "IncludeUppercase",
    AccessCodeLength = "AccessCodeLength",
    MultiFactorAuthenticationEnabled = "MultiFactorAuthenticationEnabled",
    MultiFactorAuthenticationEnabledSubtext = "DSARMFASubtext",
    AllowUpperCase = "AllowUpperCase",
    AllowNumbers = "AllowNumbers",
    AllowLowerCase = "AllowLowerCase",
    AllowSpecialCharacters = "AllowSpecialCharacters",
    IntegrationSystemId = "IntegrationSystemId",
    IntegrationWorkflowId = "IntegrationWorkflowId",
}
