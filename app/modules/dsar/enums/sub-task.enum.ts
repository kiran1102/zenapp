export enum SubTaskStatus {
    Assigned = 10,
    UnAssigned = 20,
    Complete = 30,
    Open = 0,
    ReadyForReview = 25,
    Deleted = 90,
}

export enum SubTaskCommentType {
    Request = 10,
    Response = 20,
}

export enum SubTaskType {
    User = 10,
    System = 20,
}
