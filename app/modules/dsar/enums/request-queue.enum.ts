export enum RequestQueueTypes {
    TextField = "Text Field",
    TextArea = "Text Area",
    Select = "Select",
    SubjectTypeText = "SubjectTypeText",
    RequestTypeText = "RequestTypeText",
    Multiselect = "Multiselect",
}
