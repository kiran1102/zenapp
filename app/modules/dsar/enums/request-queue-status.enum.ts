export enum RequestQueueStatus {
    New = 0,
    VerifyingIdentity = 15,
    InProgress = 20,
    Complete = 80,
    Close = 85,
    Locked = 90,
    OnHold = 91,
    Unverified = 92,
    Expired = 93,
    Rejected = 99,
}

export enum RequestQueueCommentStatus {
    Update = 50,
    Delete = 60,
    UpdateAttachments = 70,
}

export enum RequestQueueActionMap {
    New = 10,
    Progress = 20,
    Complete = 30 ,
    Reject = 40,
    Closed = 50,
    Unverified = 92,
    Expired = 93,
}
