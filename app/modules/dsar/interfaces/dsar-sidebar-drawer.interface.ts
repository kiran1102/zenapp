export interface IFilter {
    name: IFilterData;
    label: string;
    operatorName: IOperator;
    selectedValues: IOptionData[] | string | string[];
    isEditing: boolean;
    operatorOptions: IOperator[];
    valueOptions: IOptionData[] | IOperator[];
    selectedType: number;
}

export interface IFilterData {
    type: number;
    name: string;
    translationKey: string;
    translatedValue: string;
    valueOptions?: IOptionData[] | IOperator[];
    filterOnKey: boolean;
    id: string;
    sortable: boolean;
}

export interface IOptionData {
    id: string | {
        beginJsDate: string;
        endJsDate: string;
    };
    label: string;
}

export interface IOperator {
    key: string;
    value: string;
}

export interface IFilterSessionData {
    filterList: IFilter[];
}

export interface IFilterTree {
    AND: IFilterGetData[];
    OR?: IFilterGetData[];
}

export interface IFilterGetData {
    attributeKey: string;
    operator: string;
    dataType: number;
    fromValue: string[] | string;
    toValue?: string[] | string;
}
