import { IStringMap } from "interfaces/generic.interface";
import { IPageableMeta } from "interfaces/pagination.interface";

export interface IFormattedActivity {
    id: string;
    title: string;
    description: string;
    timeStamp: string;
    icon: string;
    iconColor: string;
    iconBackgroundColor: string;
    changes: IAuditChange[];
}

export interface IAuditHistoryItem {
    changeDateTime: string;
    description: string;
    objectId: string;
    recordedDateTime: string;
    userId: string;
    userName?: string;
    changes: IAuditChange[];
}

export interface IAuditChange {
    description: string;
    fieldName: string;
    newValue: IStringMap<string>;
    oldValue: IStringMap<string>;
}

export interface IGetPageableAudit extends IPageableMeta {
    content: IAuditHistoryItem[];
}
