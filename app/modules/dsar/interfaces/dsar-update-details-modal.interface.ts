import { IRequestQueueDetails } from "dsarModule/interfaces/request-queue.interface";
import { IStringMap, ILabelValue } from "interfaces/generic.interface";
import { IGetWebformLanguageResponse } from "interfaces/dsar/dsar-edit-language.interface";

export interface IEntityItem {
    Id: string;
    Label: string;
}

export interface IUpdateDetailsModalResolve {
    promiseToResolve: (data: IUpdateDetailsPutData) => ng.IPromise <any> ;
    modalTitle: string;
    requestQueueDetails: IRequestQueueDetails;
    confirmationText: string;
    cancelButtonText: string;
    submitButtonText: string;
    fromView?: string;
}

export interface IUpdateDetailsFieldData {
    currentOptions?: IEntityItem[];
    customField?: boolean;
    dataType: string;
    deadlineChanged?: boolean;
    deadlineValue?: string;
    description: string;
    descriptionValue: string;
    fieldKey: string;
    inputType: string;
    isRequired: boolean;
    isValid?: boolean;
    options?: IEntityItem[] | string[];
    selectedValue?: ILabelValue<string> | string;
    selectedValues?: IEntityItem[];
    errorMessage: string;
}

export interface IUpdateDetailsPutData {
    firstName: string;
    lastName: string;
    email: string;
    otherData: IStringMap<string>;
    requestTypes: string[];
    subjectTypes: string[];
    language: string;
    multiSelectFields: IStringMap<string[]>;
}

export interface IUpdateDetailsGetData {
    form: IUpdateFormData;
    fields: IUpdateFieldData;
}

export interface ITranslations {
    Translations: IStringMap<string>;
    Language: string;
}

export interface IUpdateFieldData {
    data: IUpdateFormFieldData[];
    templateId: string;
}

export interface IUpdateFormFieldData {
    canDelete: boolean;
    description: string;
    fieldKey: string;
    inputType: string;
    isMasked: boolean;
    isRequired: boolean;
    isSelected: boolean;
    previewClass: string;
    status: number;
    options?: any;
    cannotUnselect?: boolean;
    dataType?: string;
    fieldName?: string;
}
export interface IUpdateFormData {
    default: boolean;
    defaultLanguage: string;
    footerText: string;
    languageList: IGetWebformLanguageResponse[];
    requestText: string;
    requestTypes: IUpdateRequestSubjectTypes[];
    subjectText: string;
    subjectTypes: IUpdateRequestSubjectTypes[];
    templateId: string;
    templateName: string;
    title: string;
    urlLink: string;
    welcomeText: string;
    thankYouText?: string;
}

export interface IUpdateRequestSubjectTypes {
    canDelete: boolean;
    description: string;
    id: string;
    descriptionValue: string;
    fieldName: string;
    fieldValue: string;
    isSelected: boolean;
    isUsed: boolean;
    order: number;
    selected: boolean;
    status: number;
    used: boolean;
}
