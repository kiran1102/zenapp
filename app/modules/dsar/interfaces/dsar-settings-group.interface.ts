import {IDSARSettings} from "dsarModule/interfaces/dsar-settings.interface";

export interface IDSARSettingsGroup {
    name: string;
    id: string;
    description: string;
    settings: IDSARSettings[];
}
