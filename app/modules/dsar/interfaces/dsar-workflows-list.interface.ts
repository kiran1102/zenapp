import { IPageable } from "interfaces/pagination.interface";
import { INameId } from "interfaces/generic.interface";
import { IPublishedWorkflow } from "interfaces/workflows.interface";

export interface IGetWorkflowsList extends IPageable {
    content: IWorkflowList[];
}

export interface IWorkflowList {
    id: string;
    name: string;
    workflowStatus: number;
    createdDate: Date;
    createdBy: string;
    lastUpdatedBy: string;
    lastUpdatedDate: Date;
}

export interface ICreateData {
    label: string;
    type: string;
    name: string;
    isRequired: boolean;
    value?: string;
    options?: IPublishedWorkflow[];
    [key: string]: string | boolean | IPublishedWorkflow[];
}
