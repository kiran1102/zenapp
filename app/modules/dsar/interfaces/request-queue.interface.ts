import { IWorkflow, IWorkflowItem } from "interfaces/workflows.interface";
import { IDropdownOption } from "interfaces/one-dropdown.interface";
import { PathItem } from "@onetrust/vitreus";
import { IFilterGetData } from "dsarModule/interfaces/dsar-sidebar-drawer.interface";
import { IRequestQueueDetails } from "dsarModule/interfaces/request-queue.interface";
import { IAttachmentFileResponse } from "interfaces/attachment-file.interface";
import { IGetWebformLanguageResponse } from "interfaces/dsar/dsar-edit-language.interface";
import { IRequestQueueAssign } from "modules/dsar/interfaces/request-queue-actions.interface";
import { IRequestQueueComment } from "modules/dsar/interfaces/request-queue-comment.interface";
import { IStringMap } from "interfaces/generic.interface";
import { ITypeFormField } from "@onetrust/dsar-components";

export interface IRequestQueueGenericModalResolve {
    modalTitle: string;
    confirmationText: string;
    cancelButtonText: string;
    submitButtonText: string;
}
export interface IRequestQueueRejectModalResolve extends IRequestQueueGenericModalResolve {
    promiseToResolve: (attachments: IAttachmentFileResponse[], isInternal: boolean, resolutionId: string, comment?: string) => ng.IPromise<boolean> ;
    requestQueueDetails: IRequestQueueDetails;
    data?: {
        attachments: IAttachmentFileResponse[];
        comment: string;
    };
    resolutionOptions: IRequestResolutionFormatted[];
}

export interface IRequestQueueAssignModalResolve extends IRequestQueueGenericModalResolve {
    promiseToResolve: (requestPayload: IRequestQueueAssign[]) => ng.IPromise<boolean>;
    requestQueueDetails: IRequestQueueDetails;
}

export interface IRequestQueueExtendModalResolve {
    promiseToResolve: (days: number, comment: string) => ng.IPromise<boolean> ;
    modalTitle: string;
    confirmationText: string;
    cancelButtonText: string;
    submitButtonText: string;
    comment?: string;
}
export interface IRequestQueueDropdownAction extends IDropdownOption {
    rowId?: number;
}

export interface IRequestQueueParams {
    page?: number | string;
    size?: number | string;
    sort?: string;
}

export interface IMultiselectFields {
    FieldKey: string;
    FieldName: string;
    Values: string[];
}

export interface IRequestQueueDetails {
    comments: IRequestQueueComment[];
    dateCreated: string;
    daysLeft: any;
    email: string;
    extended: boolean;
    firstName: string;
    formFields?: Array<{
        fieldName: string;
        value: any;
    }>;
    hasSubTaskEditAccess: boolean;
    isExtended: string;
    lastName: string;
    name: string;
    orgGroupId: string;
    organization: string;
    otherData: any;
    requestDetails: IRequestQueueDetails;
    requestQueueId: string;
    requestQueueRefId: string;
    requestType: string;
    requestTypes: ITypeFormField[];
    reviewer: string;
    reviewerId?: string;
    resolution: string;
    status?: number;
    statusLabel?: number | string;
    statusLabelColour?: string;
    subjectType: string;
    subjectTypes: ITypeFormField[];
    hoursWorked: number;
    statusIcon?: string;
    taskMode?: boolean;
    languageList: IGetWebformLanguageResponse[];
    language: string;
    dsAccessRevoked: boolean;
    workflow?: IWorkflow;
    currentStageInfo?: IWorkflowItem;
    templateId?: string;
    deadline?: string;
    currentStage?: string;
    workflowDetailDto?: IWorkflowItem;
    completedSubtasks?: number | string;
    totalSubtasks?: number | string;
    remainingSubtasks?: number;
    dsLanguageName?: string;
    multiselectFields?: IMultiselectFields[];
    password?: string;
    authEnabled?: boolean;
    passwordReGenerate?: boolean;
    isFirstPublicReply?: boolean;
    workflowName?: string;
    workflowReferenceId?: string;
    webformName?: string;
}

export interface IRequestQueuePathItem extends PathItem {
    guidanceText?: string;
    status?: number;
}

export interface IGrantAccessSubmit {
    requestQueueId: string;
    grantAccessExpiryDate: string;
}

export interface IRevokeAccessSubmit {
    requestQueueId: string;
}

export interface IGrantAccessModal {
    grantAccessCallback: () => void;
    requestQueueId: string;
}

export interface IDeadlineChangeModal {
    changeDeadlineCallback: (days: string) => void;
    requestQueueId: string;
    deadline: string;
}

export interface IRequestChangeOrgModal {
    promiseToResolve?: (orgId: string, approverId: string) => ng.IPromise<boolean>;
    requestQueueId?: string;
    reviewerId: string;
    orgGroupId: string;
}

export interface IGetAllRequests {
    content: IRequestQueueDetails[];
    totalElements?: number;
    last?: boolean;
    totalPages?: number;
    sort?: IGetRequestsSort[];
    first?: boolean;
    numberOfElements?: number;
    size?: number;
    number?: number;
}

export interface IGetRequestsSort {
    direction: string;
    property: string;
    ignoreCase: boolean;
    nullHandling: string;
    ascending: boolean;
    descending: boolean;
}

export interface ITableColumns {
    sortKey: string | number;
    columnKey: string;
    columnName?: string;
    cellValueKey: string;
    columnType: string;
    sortable: boolean;
    width?: string;
    isMandatory?: boolean;
    isActive?: boolean;
}

export interface IColumnMap {
    activeColumnList: ITableColumns[];
    disabledColumnList: ITableColumns[];
}

export interface IExport {
    sort: string;
    requestBody: {
        searchString: string;
        filterCriteria: IFilterGetData[];
    };
}

export interface IRequestQueueSidebarDetails {
    Approver: string;
    ManagingOrganization: string;
    DateOpened: string;
    Extended: string;
    Deadline: string;
    PreferredLanguage?: string;
    TotalCost?: number;
    HoursWorked?: number;
}

export interface IMFAModalData {
    modalTitle: string;
    confirmationText: string;
    firstRequestPasswordFlow?: boolean;
    regenerateFlow?: boolean;
    secondRequestPasswordFlow?: boolean;
    requestQueueId?: string;
    modalCloseButton?: string;
    modalRegenerateButton?: string;
    callback?: () => void;
}

export interface IRequestQueueSubTasks {
    requestApprover: string;
    requestApproverId: string;
    requestOrgName: string;
    requestQueueDateCreated: string;
    requestQueueDeadline: string;
    requestQueueId: string;
    requestQueueRefId: string;
    requestTypes: string[];
    subTaskAssignee: string;
    subTaskCreatedDate: string;
    subTaskDeadline: string;
    subTaskDescription: string;
    subTaskId: string;
    subTaskLastUpdatedDate: string;
    subTaskName: string;
    subTaskReminderDate: string;
    subTaskRequired: boolean;
    subTaskStatus: string;
    subTaskType: string;
    subjectTypes: string[];
    subTaskApproverId: string;
    subTaskStatusName: string;
}

export type IViewType = "requestqueuelist" | "subtasklist";

export interface ISavedView {
    id: string;
    name: string;
    nameKey: string;
    label?: string;
    qualifier: boolean;
    owner: string;
    isLocked: boolean;
    data?: string;
    locked: boolean;
    pinned: boolean;
    public: true;
    sequence: number;
    viewType: IViewType;
    params?: IRequestQueueParams;
}
export interface IChangeWorkflowRequest {
    requestIds: string[];
    targetWorkflow: string;
    deleteOpenSubtasks: boolean;
}

export interface IRequestResolution {
    id: string;
    title: string;
    description: string;
    isClose: boolean;
    isComplete: boolean;
    isReject: boolean;
    canDelete: boolean;
    status: string;
}

export interface IRequestResolutionFormatted {
    id: string;
    label: string;
    value: number;
    description: string;
}
export interface IFilterItem {
    filters: number;
    items: number;
}

export interface ISavedViewRequest {
    viewTypes: string[];
}

export interface ISavedViewResponse {
    defaultViewInfo: {
        data: string;
        viewId: string;
    };
    uiViewInformation: ISavedView[];
}

export interface IRequestQueueBreadcrumbItem {
    link: string;
    label: string;
    autoId: string;
    params?: IStringMap<any>;
}

export interface ISavedViewData {
    filterConditions: IFilterGetData;
    searchString: string;
    columnSelections: ITableColumns[];
}
