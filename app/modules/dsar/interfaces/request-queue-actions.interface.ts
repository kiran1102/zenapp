import { IAttachmentFileResponse } from "interfaces/attachment-file.interface";

export interface IRequestQueueExtend {
    requestQueueId: string;
    daysToExtend: number;
    comment: string;
    isInternalComment: boolean;
}

export interface IRequestQueueReject {
    requestQueueId: string;
    comment: string;
    isInternalComment: boolean;
    attachments?: IAttachmentFileResponse[];
    currentStatus: number;
    newStatus: number;
    resolutionId: string;
}

export interface IRequestQueueAssign {
    requestQueueId: string;
    reviewer: string;
    comment: string;
    isInternalComment: boolean;
}

export interface IRequestQueueComplete {
    requestQueueId: string;
    comment: string;
    isInternalComment: boolean;
    attachments?: IAttachmentFileResponse[];
    currentStatus: number;
    newStatus: number;
}

export interface IRequestQueueClose {
    requestQueueId: string;
    currentStatus: number;
    newStatus: number;
}
