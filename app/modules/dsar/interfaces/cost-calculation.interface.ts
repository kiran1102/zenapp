export interface ICostCalculation {
    isCostCalculationEnabled: boolean;
    hourlyRate: number;
    currency: string;
}
