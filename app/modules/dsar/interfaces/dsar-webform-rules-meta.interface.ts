import {
    IKeyValue,
    IStringMap,
 } from "interfaces/generic.interface";

export interface IWebformRuleMeta {
    webformId: string;
    languages: Array<IKeyValue<string>>;
    countries: Array<IKeyValue<string>>;
    requestTypes: Array<IKeyValue<string>>;
    subjectTypes: Array<IKeyValue<string>>;
    multiselectFields: IStringMap<Array<IKeyValue<string>>>;
}
