import { ILabelValue } from "interfaces/generic.interface";
import { IFilterValueOption } from "interfaces/filter.interface";
import { FilterValueTypes } from "modules/filter/enums/filter.enum";

export interface IFilterField {
    type: number;
    name: string;
    translationKey?: string;
    translatedValue?: string;
    valueOptions?: IFilterValueOption[];
    filterOnKey?: boolean;
    id?: string;
    sortable?: boolean;
    selectedValues?: IFilterValueOption[];
    selectedOperator?: IFilterValueOption;
}
