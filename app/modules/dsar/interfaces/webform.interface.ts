import { IPublishedWorkflow } from "interfaces/workflows.interface";
import { IDeleteConfirmationModalResolve } from "interfaces/delete-confirmation-modal-resolve.interface";
import { IStringMap } from "interfaces/generic.interface";
import { IDropdownOption } from "interfaces/one-dropdown.interface";
import { IDSARFormField } from "@onetrust/dsar-components";

export interface IWebFormOtherResponse {
    [index: string]: string | string[];
}

export interface IWebFormResponse {
    firstName: string;
    lastName: string;
    email: string;
    requestTypes: string[];
    subjectTypes: string[];
    recaptcha?: string;
    otherData?: IWebFormOtherResponse;
    daysToRespond: string;
    language: string;
    attachment?: IWebformAttachment;
    captchaId?: string;
    captchaCode?: string;
    multiselectFields?: IStringMap<string[]>;
}

export interface IWebformAttachment {
    Name: string;
    Type: number;
    Encrypt: boolean;
    FileName: string;
    IsInternal: boolean;
    Extension: string;
}

export interface IWebFormData {
    form: IWebFormResponse;
    attachment?: Blob;
}

export interface IWebFormSettingsInput {
    canDelete: boolean;
    dataType: string;
    fieldName: string;
    inputType: string;
    value: any;
    isRequired?: boolean;
    options?: string[];
}

export interface IWebformSettings {
    publishWorkflowDtos: IPublishedWorkflow[];
    templateId: string;
    orgGroupId: string;
    workflowRefId: string;
    data: IWebFormSettingsInput[];
}

export interface IWebformCreate {
    templateName: string;
    orgGroupId: string;
    workflowReferenceId: string;
}

export interface IWebformCreateResponse {
    templateId: string;
    templateName: string;
    orgGroupId: string;
    isUsed: boolean;
    status: number;
}

export interface IWebformPublishResponse {
    link: string;
    templateId: string;
    uri: string;
    draft: boolean;
    filename: string;
    isDraft: boolean;
    scriptVersion: any;
    cdnCacheHours: string;
}

export interface IWebformFetchDataForExport {
    published: boolean;
    publishedHost?: string;
    publishedPath?: string;
    templateId?: string;
}

export interface IWebformAction {
    id: string;
    textKey: string;
    action: (option: IWebformAction) => void;
}

export interface IWebFormFieldResponse {
    templateId: string;
    data: IDSARFormField[];
}

export interface IGenerateScriptModal extends IDeleteConfirmationModalResolve {
    webFormId: string;
    publishedHost: string;
    publishedPath: string;
}

export interface IWebformDetailRequest {
    templateName: string;
    title: string;
    welcomeText: string;
    footerText: string;
    thankYouText: string;
    requestText: string;
    subjectText: string;
    language: string;
    submitButtonText: string;
    attachmentInfoDesc: string;
    attachmentButtonText: string;
    attachmentSizeDescription: string;
}

export interface IFormAction {
    type: number;
    payload?: any;
}

export interface IWebFormItem {
    templateName: string;
    templateId: string;
    orgGroupId: string;
    orgName: string;
    isUsed?: boolean;
    status?: number;
}
export interface IWebformListItem extends IWebFormItem {
    dropdownList: IDropdownOption[];
}

export interface IWebFormSubmitData {
    multiselectFields: IStringMap<string[]>;
    firstName: string;
    lastName: string;
    email: string;
    daysToRespond: string;
    language: string;
    requestTypes: string[];
    subjectTypes: string[];
    captchaId: string;
    captchaCode: string;
    otherData: IWebFormOtherResponse;
    fileName: string;
    name: string;
}

export interface IWebformAutoPublish {
    dismissed: boolean;
    id: string;
    message?: string;
    migratedCount: number;
    success: boolean;
}
