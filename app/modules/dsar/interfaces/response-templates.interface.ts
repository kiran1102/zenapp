import {
    IPaginationParams,
    IPaginationFilter,
} from "interfaces/pagination.interface";

export interface IResponseTemplate {
    id?: string;
    templateName?: string;
    templateContent?: string;
    status?: number | string;
    type?: number | string;
    isEditable?: boolean;
    orgGroupId?: string;
    translations?: ITranslations[];
    language?: string;
    lastModifiedBy?: string;
    modifiedDate?: string;
}

export interface ITranslations {
    language: string;
    templateName: string;
    templateContent: string;
}

export interface IResponseTemplateListParams extends IPaginationParams<IPaginationFilter[]> {
    searchKey?: string;
}
