import { IAttachmentFileResponse } from "interfaces/attachment-file.interface";
import { ICommentActivityStream } from "interfaces/comment-activity-stream.interface";

export interface IRequestQueueComment extends ICommentActivityStream {
    actionType: string;
    authorDetails: {
        authorEmail: string;
        authorFirstName: string;
        authorFullName: string;
        authorId: string;
        authorLastName: string;
    };
    assignedTo: string;
    commentString: string;
    createdDate: string;
    id: number;
    isInternalComment: boolean;
    requestStatus: number;
    daysExtended?: number;
    attachments: any;
    showDeleteIcon?: boolean;
}

export interface IRequestQueueCommentUpdate {
    commentId: string;
    statusCode: number;
    comment: string;
    isInternalComment: boolean;
    attachments: any | IAttachmentFileResponse; // depends on BE (that's why the any)
}
