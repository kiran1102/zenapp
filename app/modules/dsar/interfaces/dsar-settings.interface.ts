export interface IDSARSettings {
    category: string;
    name: string;
    value: any;
    description: string;
    settingPropertyMetadata: {
        valueType: string,
        allowNull: boolean;
        hidden: boolean;
    };
}

export interface ICurrencyList {
    countryCode: string;
    countryName: string;
}

export interface IDSARSettingsUpdate {
    name: string;
    settings: IDSARSingleSettingUpdate[];
    orgGroupId?: string;

}

export interface IDSARSingleSettingUpdate {
    name: string;
    value: any; // any since it could be of any type - object, boolean, string, number, etc.
}

export interface IDSARCostCalcSettings {
    isCostCalcEnabled: boolean;
    hourlyCost: number;
    currencyIso: string;
}
