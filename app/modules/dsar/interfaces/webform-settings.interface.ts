import { ILabelValue } from "interfaces/generic.interface";
import { IPublishedWorkflow } from "interfaces/workflows.interface";

export interface IWebformSettingsMap {
    EnableReCaptcha: boolean;
    AttachmentSubmission: boolean;
    Organization: string;
    DefaultDaysToRespond: number;
    DefaultReminder: number;
    DefaultReviewer: {
        Id: string;
        FullName: string;
        OrgGroupId?: string;
        Email?: string;
    };
    BccEmail: string;
    TranslateWebForm: boolean;
    AllowDsPortalAttachment: boolean;
    EnableSubjectTypes: boolean;
    EnableRequestTypes: boolean;
    IsLogoEnabled?: boolean;
    OrgGroupId: string;
    Workflows: IPublishedWorkflow;
    MultiCaptcha?: ILabelValue<string>;
    EmailVerification: boolean;
    ExpiryRequest: boolean;
    ExpiryRequestDays: number;
    RequireAttachment?: boolean;
    TemplateName?: string;
}

export interface IWebformAccordionMeta {
    title: string;
    open: boolean;
}
