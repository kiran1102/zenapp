import { Permissions } from "modules/shared/services/helper/permissions.service";
import { WebformTabs } from "dsarModule/enums/dsar-webform.enum";

export function routes($stateProvider: ng.ui.IStateProvider) {
    $stateProvider
        .state("zen.app.pia.module.dsar.views", {
            abstract: true,
            url: "",
            views: {
                dsar_body: {
                    template: "<ui-view></ui-view>",
                },
            },
            resolve: {
                DSARViewsPermission: [
                    "DSARPermission",
                    (DSARPermission: boolean): boolean => {
                        return DSARPermission;
                    },
                ],
            },
        })
        .state("zen.app.pia.module.dsar.views.queue", {
            url: "/queue?page&size&sort&filter",
            params: { time: null, filters: [] },
            template: "<downgrade-dsar-request-queue-wrapper></downgrade-dsar-request-queue-wrapper>",
            resolve: {
                DSARQueuePermission: ["DSARViewsPermission", "Permissions",
                    (DSARViewsPermission: boolean, permissions: Permissions): boolean => {
                        if (permissions.canShow("DSARRequestQueue")) {
                            return true;
                        } else {
                            permissions.goToFallback();
                        }
                        return false;
                    },
                ],
            },
        })
        .state("zen.app.pia.module.dsar.views.queue-details", {
            url: "/queue/:id",
            params: { time: null, previousParams: null },
            template: "<downgrade-dsar-request-queue-detail></downgrade-dsar-request-queue-detail>",
            resolve: {
                DSARQueuePermission: ["DSARViewsPermission", "Permissions",
                    (DSARViewsPermission: boolean, permissions: Permissions): boolean => {
                        if (permissions.canShow("DSARRequestQueueDetailPage")) {
                            return true;
                        } else {
                            permissions.goToFallback();
                        }
                        return false;
                    },
                ],
            },
        })
        // Keep for future implementation
        // .state("zen.app.pia.module.dsar.views.subtask-queue-details", {
        //     url: "/subtaskqueue/:id",
        //     params: { time: null, previousParams: null, subtaskView: true },
        //     template: "<downgrade-dsar-request-queue-detail></downgrade-dsar-request-queue-detail>",
        //     resolve: {
        //         DSARQueuePermission: ["Permissions",
        //             (permissions: Permissions): boolean => {
        //                 if (permissions.canShow("DSARRequestQueueDetailPage")) {
        //                     return true;
        //                 } else {
        //                     permissions.goToFallback();
        //                 }
        //                 return false;
        //             },
        //         ],
        //     },
        //   })
        .state("zen.app.pia.module.dsar.views.webform", {
            abstract: true,
            url: "/webform",
            template: "<ui-view></ui-view>",
            resolve: {
                DSARViewsPermission: [
                    "DSARPermission",
                    (DSARPermission: boolean): boolean => {
                        return DSARPermission;
                    },
                ],
            },
        })
        .state("zen.app.pia.module.dsar.views.webform.list", {
            url: "s",
            params: { time: null },
            template: "<downgrade-dsar-webforms></downgrade-dsar-webforms>",
            resolve: {
                DSARWebFormsPermission: [
                    "DSARViewsPermission", "Permissions",
                    (DSARViewsPermission: boolean, permissions: Permissions): boolean => {
                        if (permissions.canShow("DSARWebFormsRead")) {
                            return true;
                        } else {
                            permissions.goToFallback();
                        }
                        return false;
                    },
                ],
            },
        })
        .state("zen.app.pia.module.dsar.views.webform.manage", {
            url: "/manage/:webFormId",
            params: {
                time: null,
                webformTab: WebformTabs.WebForm,
            },
            template: "<webform-manager></webform-manager>",
            resolve: {
                DSARWebFormPermission: [
                    "DSARViewsPermission", "Permissions",
                    (DSARViewsPermission: boolean, permissions: Permissions): boolean => {
                        if (permissions.canShow("DSARWebForms")) {
                            return true;
                        } else {
                            permissions.goToFallback();
                        }
                        return false;
                    },
                ],
            },
        })
        .state("zen.app.pia.module.dsar.views.webform.add", {
            url: "/create",
            params: { time: null },
            template: "<downgrade-dsar-add-new-webform></downgrade-dsar-add-new-webform>",
            resolve: {
                DSARWebFormPermission: [
                    "DSARViewsPermission", "Permissions",
                    (DSARViewsPermission: boolean, permissions: Permissions): boolean => {
                        if (permissions.canShow("DSARWebFormsCreate")) {
                            return true;
                        } else {
                            permissions.goToFallback();
                        }
                        return false;
                    },
                ],
            },
        })
        .state("zen.app.pia.module.dsar.views.dashboard", {
            url: "/dashboard",
            params: { time: null },
            template: "<downgrade-dsar-dashboard></downgrade-dsar-dashboard>",
            resolve: {
                DSARWebFormPermission: ["DSARViewsPermission", "Permissions",
                    (DSARViewsPermission: boolean, permissions: Permissions): boolean => {
                        if (permissions.canShow("DSARDashboard")) {
                            return true;
                        } else {
                            permissions.goToFallback();
                        }
                        return false;
                    },
                ],
            },
        })
        .state("zen.app.pia.module.dsar.views.settings", {
            url: "/settings",
            params: { time: null },
            template: "<dsar-settings></dsar-settings>",
            resolve: {
                DSARWebFormPermission: ["DSARViewsPermission", "Permissions",
                    (DSARViewsPermission: boolean, permissions: Permissions): boolean => {
                        if (permissions.canShow("DSARSettings")) {
                            return true;
                        } else {
                            permissions.goToFallback();
                        }
                        return false;
                    },
                ],
            },
        })
        .state("zen.app.pia.module.dsar.views.workflows", {
            url: "/workflows",
            params: { time: null },
            template: "<dsar-workflows-list></dsar-workflows-list>",
            resolve: {
                DSARWorkflowPermission: [ "Permissions",
                    (permissions: Permissions): boolean => {
                        if (permissions.canShow("DSARCustomWorkflows")) {
                            return true;
                        } else {
                            permissions.goToFallback();
                        }
                        return false;
                    },
                ],
            },
        })
        .state("zen.app.pia.module.dsar.views.workflow-details", {
            url: "/workflows/workflowDetail",
            params: { time: null, previousParams: null},
            template: "<downgrade-dsar-workflow-detail></downgrade-dsar-workflow-detail>",
            resolve: {
                DSARWorflowDetailPermission: ["Permissions",
                    (permissions: Permissions): boolean => {
                        if (permissions.canShow("DSARCustomWorkflows")) {
                            return true;
                        } else {
                            permissions.goToFallback();
                        }
                        return false;
                    },
                ],
            },
        })
        .state("zen.app.pia.module.dsar.views.responseTemplates", {
            url: "/responsetemplates",
            params: { time: null },
            template: "<downgrade-response-templates-tabs></downgrade-response-templates-tabs>",
            resolve: {
                DSARResponseTemplatesPermission: [ "Permissions",
                    (permissions: Permissions): boolean => {
                        if (permissions.canShow("DSARResponseTemplate")) {
                            return true;
                        } else {
                            permissions.goToFallback();
                        }
                        return false;
                    },
                ],
            },
        })
        .state("zen.app.pia.module.dsar.views.email_templates", {
            url: "/email-templates",
            params: { time: null },
            template: "<dsar-email-templates></dsar-email-templates>",
            resolve: {
                DSARQEmailTemplatesPermission: ["DSARViewsPermission", "Permissions",
                    (DSARViewsPermission: boolean, permissions: Permissions): boolean => {
                        if (permissions.canShow("DSAREmailTemplate")) {
                            return true;
                        } else {
                            permissions.goToFallback();
                        }
                        return false;
                    },
                ],
            },
        })
    ;
}

routes.$inject = ["$stateProvider"];
