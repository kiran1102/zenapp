import {
    Pipe,
    PipeTransform,
} from "@angular/core";
import { RequestQueueStatus } from "dsarModule/enums/request-queue-status.enum";

@Pipe({
    name: "dsarRequestStatusColor",
})

export class DsarRequestStatusColorPipe implements PipeTransform {
    transform(status: RequestQueueStatus): string {
        switch (status) {
            case RequestQueueStatus.Complete:
                return "success";
            case RequestQueueStatus.New:
                return "warning";
            case RequestQueueStatus.Locked:
                return "base";
            case RequestQueueStatus.Close:
            case RequestQueueStatus.Rejected:
                return "destructive";
            case RequestQueueStatus.Unverified:
                return "hold";
            case RequestQueueStatus.Expired:
                return "disabled";
            default:
                return "primary";
        }
    }
}
