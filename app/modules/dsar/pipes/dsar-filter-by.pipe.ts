import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
    name: "dsarFilterBy",
})

export class DSARFilterByPipe implements PipeTransform {
    transform(values: Array<unknown>, filterByFn: (item: any, key: string) => boolean, key: string): any[] {
        return values.filter((value) => filterByFn(value, key));
    }
}
