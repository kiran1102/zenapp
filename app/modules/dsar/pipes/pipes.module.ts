import { NgModule } from "@angular/core";

// Pipes
import { DsarRequestStatusColorPipe } from "dsarModule/pipes/dsar-request-status-color.pipe";
import { DsarSubTaskStatusColorPipe } from "dsarModule/pipes/dsar-sub-task-status-color.pipe";
import { DSARFilterByPipe } from "modules/dsar/pipes/dsar-filter-by.pipe";

export const PIPES = [
    DsarRequestStatusColorPipe,
    DsarSubTaskStatusColorPipe,
    DSARFilterByPipe,
];

@NgModule({
    imports: [],
    providers: [],
    exports: [PIPES],
    declarations: [PIPES],
})

export class DsarPipesModule { }
