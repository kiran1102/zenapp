import {
    Pipe,
    PipeTransform,
} from "@angular/core";
import { SubTaskStatus } from "dsarModule/enums/sub-task.enum";

@Pipe({
    name: "dsarSubTaskStatusColor",
})

export class DsarSubTaskStatusColorPipe implements PipeTransform {
    transform(status: SubTaskStatus): string {
        switch (status) {
            case SubTaskStatus.Assigned:
                return "primary";
            case SubTaskStatus.UnAssigned:
               return "warning";
            case SubTaskStatus.Complete:
               return "success";
            case SubTaskStatus.Open:
                return "warning";
            case SubTaskStatus.ReadyForReview:
                return "primary";
            default:
                return "primary";
        }
    }
}
