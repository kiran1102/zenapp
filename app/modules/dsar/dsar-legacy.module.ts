declare var angular: angular.IAngularStatic;

import { routes } from "./dsar-legacy.routes";

import { RequestQueueHelper } from "dsarservice/request-queue-helper.service";
import { DSARSettingsService } from "dsarservice/dsar-settings.service";

import DSARWrapperComponent from "dsarModule/components/dsar.component.ts";
import WebFormManagerComponent from "dsarModule/components/webforms/webform-manager.component.ts";
import WebFormControlComponent from "dsarModule/components/webforms/control/webform-control.component.ts";
import WebFormLiveComponent from "dsarModule/components/webforms/live/webform-live.component.ts";
import { DSARSettingsComponent } from "dsarModule/components/settings/dsar-settings.component.ts";
import { DSARSettingsGeneralComponent } from "dsarModule/components/settings/dsar-settings-general.component.ts";

import dsarAttachmentModal from "generalcomponent/Modals/dsar-attachment-modal/dsar-attachment-modal.component.ts";
import { DSARWorkflowsComponent } from "dsarModule/components/workflows/dsar-workflows.component.ts";

export const dsarModule = angular.module("zen.dsar", [
    "ui.router",
    "ui.bootstrap",
    "isoCurrency",
    // Angular modules
    "ngAnimate",
    "BotDetectCaptcha",
])
    .config(routes)
    // Services
    .service("RequestQueueHelper", RequestQueueHelper)
    .service("DSARSettingsService", DSARSettingsService)

    // Components
    .component("dsarWrapper", DSARWrapperComponent)
    .component("webformManager", WebFormManagerComponent)
    .component("webformControl", WebFormControlComponent)
    .component("webformLive", WebFormLiveComponent)
    .component("dsarAttachmentModal", dsarAttachmentModal)
    .component("dsarSettings", DSARSettingsComponent)
    .component("dsarSettingsGeneral", DSARSettingsGeneralComponent)
    .component("dsarWorkflows", DSARWorkflowsComponent)
    ;
