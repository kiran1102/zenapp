// Angular
import {
    Component,
    Inject,
    OnInit,
} from "@angular/core";

// Redux
import { getModalData } from "oneRedux/reducers/modal.reducer";
import { StoreToken } from "tokens/redux-store.token";

// Interfaces
import { IConfirmationModalResolve } from "interfaces/confirmation-modal-resolve.interface";
import { IStore } from "interfaces/redux.interface";

// 3rd Party Library
import { isFunction } from "lodash";

// services
import { ModalService } from "sharedServices/modal.service";

@Component({
    selector: "request-queue-complete-modal",
    templateUrl: "./request-queue-complete-modal.component.html",
})
export class RequestQueueCompleteModal implements OnInit {
    saveInProgress = false;
    modalData: IConfirmationModalResolve;

    constructor(
        @Inject(StoreToken) private store: IStore,
        private readonly modalService: ModalService,
    ) {
    }

    ngOnInit() {
        this.modalData = getModalData(this.store.getState());
    }

    handleSubmit(): void {
        if (isFunction(this.modalData.promiseToResolve)) {
            this.saveInProgress = true;
            this.modalData.promiseToResolve().then((response: boolean) => {
                if (response) {
                    this.modalService.closeModal();
                    this.modalService.clearModalData();
                    if (isFunction(this.modalData.successCallback)) {
                        this.modalData.successCallback();
                    }
                } else {
                    this.saveInProgress = false;
                }
            });
        }
    }

    closeModal(): void {
        this.modalService.closeModal();
        this.modalService.clearModalData();
    }

}
