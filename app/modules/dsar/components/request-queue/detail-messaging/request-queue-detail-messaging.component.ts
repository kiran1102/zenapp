// Angular
import {
    Component,
    Input,
    OnInit,
    OnDestroy,
} from "@angular/core";

// 3rd party
import { isNil } from "lodash";

// RxJS
import {
    BehaviorSubject,
    Observable,
    Subscription,
} from "rxjs";

// Services
import { Permissions } from "modules/shared/services/helper/permissions.service";
import { RequestQueueDetailService } from "dsarservice/request-queue-detail-service";

// Interfaces
import { IResponseTemplate } from "dsarModule/interfaces/response-templates.interface";
import { IAttachmentFileResponse } from "interfaces/attachment-file.interface";
import { IDropdownOption } from "interfaces/one-dropdown.interface";
import { IStringMap } from "interfaces/generic.interface";

// Enums and Constants
import { RequestQueueStatus } from "dsarModule/enums/request-queue-status.enum";
import { RequestQueueActions } from "dsarModule/constants/dsar-request-queue.constants";
import { ContextMenuType } from "@onetrust/vitreus";

@Component({
    selector: "dsar-request-queue-detail-messaging",
    templateUrl: "./request-queue-detail-messaging.component.html",
})
export class RequestQueueDetailMessagingComponent implements OnInit, OnDestroy {
    @Input() isInternalComment = false;
    @Input() requestQueueDetails;
    @Input() responseTemplates: IResponseTemplate[];

    isUnverified: boolean;
    isExpired: boolean;
    selectedTemplate: string;
    customModules = [["bold", "italic", "underline", "strike", "image", { list: "ordered" }, { list: "bullet" }],
    [{ align: "" }, { align: "right" }, { align: "center" }, { align: "justify" }, "link"]];

    internalComment = "";
    externalComment = "";
    externalContentSource = new BehaviorSubject(this.externalComment);
    internalContentSource = new BehaviorSubject(this.internalComment);
    attachments: IAttachmentFileResponse[];
    externalContentObservable: Observable<string> = this.externalContentSource.asObservable();
    internalContentObservable: Observable<string> = this.internalContentSource.asObservable();
    subscription: Subscription;
    permissionsMap: IStringMap<boolean>;
    submitButtonText = "Reply";
    contextMenuType = ContextMenuType;
    dropdownActions: IDropdownOption[] = [];
    disablePostingComment: boolean;

    constructor(
        readonly permissions: Permissions,
        readonly requestQueueDetailService: RequestQueueDetailService,
    ) { }

    ngOnInit(): void {
        this.subscription = this.requestQueueDetailService.getObservable$.subscribe(() => {
            this.requestQueueDetails = this.requestQueueDetailService.requestQueueDetails;
            this.permissionsMap = this.requestQueueDetailService.permissionsMap;
            if (this.requestQueueDetailService.clearComments) {
                this.internalComment = "";
                this.externalComment = "";
                this.internalContentSource.next(this.internalComment);
                this.externalContentSource.next(this.externalComment);
                this.requestQueueDetailService.clearComments = false;
            }
            this.disablePostingComment = !this.permissionsMap.DSARExternalCommentCreate && !this.permissionsMap.DSARInternalCommentCreate;
            this.isUnverified = this.requestQueueDetails.status === RequestQueueStatus.Unverified;
            this.isExpired = this.requestQueueDetails.status === RequestQueueStatus.Expired;
            this.dropdownActions = this.getActions();
            this.attachments = this.isInternalComment
                ? [...this.requestQueueDetailService.internalAttachments]
                : [...this.requestQueueDetailService.externalAttachments];
            this.attachments.forEach((attachment: IAttachmentFileResponse) => {
                attachment.showDeleteIcon = false;
            });
        });
        if (this.permissionsMap.DSARExternalCommentCreate && !this.permissionsMap.DSARInternalCommentCreate) {
            this.changeCommentType(false);
        } else {
            this.changeCommentType(true);
        }
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

    changeCommentType(isInternalComment: boolean): void {
        this.isInternalComment = isInternalComment;
        if (isInternalComment) {
            this.submitButtonText = "Post";
            this.attachments = [...this.requestQueueDetailService.internalAttachments];
            this.internalContentSource.next(this.internalComment.replace(/(?:\r\n|\r|\n)/g, "<br>"));
        } else {
            this.submitButtonText = "Reply";
            this.attachments = [...this.requestQueueDetailService.externalAttachments];
            this.externalContentSource.next(this.externalComment.replace(/(?:\r\n|\r|\n)/g, "<br>"));
        }
    }

    handleAction(isInternal: boolean): void {
        const comment: { comment: string, isInternalComment: boolean } = {
            comment: isInternal ? this.internalComment : this.externalComment,
            isInternalComment: isInternal,
        };
        this.requestQueueDetailService.handleAction(RequestQueueActions.COMMENT, comment);
    }

    attachmentActions(type: string, attachment: IAttachmentFileResponse, isInternal?: boolean): void {
        switch (type) {
            case RequestQueueActions.DOWNLOAD_FILE:
                this.requestQueueDetailService.handleAction(RequestQueueActions.DOWNLOAD_FILE, attachment);
                break;
            case RequestQueueActions.DELETE_FILE:
                attachment.isInternal = isInternal;
                this.requestQueueDetailService.handleAction(RequestQueueActions.DELETE_FILE, attachment);
                break;
        }
    }

    onItemClick(actionItem) {
        actionItem.action(actionItem);
    }

    getActions(): IDropdownOption[] {
        const actions = [];
        if (this.requestQueueDetails.status === RequestQueueStatus.Close) {
            return actions;
        }
        if (this.permissions.canShow("DSARRequestQueueComplete") &&
            this.requestQueueDetails.status !== RequestQueueStatus.Rejected) {
            actions.push({
                id: "respondAsCompleted",
                textKey: "ReplyAsCompleted",
                action: (): void => this.requestQueueDetailService.handleAction(RequestQueueActions.RESP_AS_COMPLETED, { data: { comment: this.externalComment } }),
            });
        }
        if (this.permissions.canShow("DSARRequestQueueExtend") &&
            this.requestQueueDetails.status !== RequestQueueStatus.Rejected &&
            this.requestQueueDetails.status !== RequestQueueStatus.Complete &&
            !isNil(this.requestQueueDetails.deadline)) {
            actions.push({
                id: "respondAsExtended",
                textKey: "ReplyAsExtended",
                action: (): void => this.requestQueueDetailService.handleAction(RequestQueueActions.EXTEND_REQUEST, { data: { comment: this.externalComment } }),
            });
        }
        if (this.permissions.canShow("DSARRequestQueueReject") &&
            this.requestQueueDetails.status !== RequestQueueStatus.Complete) {
            actions.push({
                id: "respondAsRejected",
                textKey: "ReplyAsRejected",
                action: (): void => this.requestQueueDetailService.handleAction(RequestQueueActions.REJECT_REQUEST, { data: { comment: this.externalComment.replace(/\n/g, "<br />") } }),
            });
        }
        return actions;
    }

    selectTemplate(templateContent: string): void {
        if (templateContent) {
            if (this.isInternalComment) {
                if (this.internalComment.length) {
                    this.internalComment += `<br><br>${templateContent}`;
                } else {
                    this.internalComment += templateContent;
                }
                this.internalContentSource.next(this.internalComment.replace(/(?:\r\n|\r|\n)/g, "<br>"));
            } else {
                if (this.externalComment.length) {
                    this.externalComment += `<br><br>${templateContent}`;
                } else {
                    this.externalComment += templateContent;
                }
                this.externalContentSource.next(this.externalComment.replace(/(?:\r\n|\r|\n)/g, "<br>"));
            }
        }
    }

    handleMessageChange(message: string, isInternal: boolean): void {
        if (message && !message.includes("img") && !$(message).text().trim()) {
            message = "";
        }
        if (isInternal) {
            this.internalComment = message || "";
        } else {
            this.externalComment = message || "";
        }
    }
}
