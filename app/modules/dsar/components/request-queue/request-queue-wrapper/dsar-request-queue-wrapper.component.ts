// Angular
import {
    Component,
    OnInit,
    Inject,
} from "@angular/core";

// Rxjs
import { take } from "rxjs/operators";

// Lodash
import { sortBy } from "lodash";

// Services
import { RequestQueueService } from "dsarservice/request-queue.service";
import { Permissions } from "modules/shared/services/helper/permissions.service";
import { RequestQueueFilterService } from "dsarservice/request-queue-filter.service";
import { StateService } from "@uirouter/core";

// Constants and Enums
import {
    RequestQueueActions,
    RequestQueueViewTypes,
} from "dsarModule/constants/dsar-request-queue.constants";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

// Interfaces
import {
    ISavedView,
    IFilterItem,
    ISavedViewRequest,
    ISavedViewResponse,
    IViewType,
} from "modules/dsar/interfaces/request-queue.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";
import { IFilterGetData } from "dsarModule/interfaces/dsar-sidebar-drawer.interface";
import { IStore } from "interfaces/redux.interface";
import { getCurrentUser } from "oneRedux/reducers/user.reducer";
import { IUser } from "interfaces/user.interface";
import { StoreToken } from "tokens/redux-store.token";
import { RequestQueueCacheService } from "dsarservice/request-queue-cache.service";

@Component({
    selector: "dsar-request-queue-wrapper",
    templateUrl: "./dsar-request-queue-wrapper.component.html",
})

export class DSARRequestQueueWrapperComponent implements OnInit {

    searchString: string;
    searchPlaceholder = "";
    currentView: ISavedView;
    requestQueueActions = RequestQueueActions;
    requestQueueViewTypes = RequestQueueViewTypes;
    showingFilteredResults = false;
    savedViewsTranslations = {
        UpdateView: this.translatePipe.transform("UpdateView"),
        CloneView: this.translatePipe.transform("CloneView"),
        CreateView: this.translatePipe.transform("CreateView"),
        ManageViews: this.translatePipe.transform("ManageViews"),
        SearchPlaceHolder: this.translatePipe.transform("Search") + "...",
        NoViewsFound: this.translatePipe.transform("NoViewsFound"),
    };
    views: ISavedView[] = [];
    showListDropdown = false;
    viewSelectorEnabled = false;
    noOfItems = "";
    filtersApplied = "";
    ready = false;
    error = false;
    filters: IFilterGetData[];
    reloadGrid: boolean;
    currentType: IViewType;
    subtaskViewId: number;
    filtersFromDashboard: IFilterGetData[];
    viewAllRequests = this.permissions.canShow("DSARViewAllRequests");
    defaultViews = ["allrequests", "allsubtasks"];

    constructor(
        private requestQueueService: RequestQueueService,
        private translatePipe: TranslatePipe,
        private permissions: Permissions,
        private requestQueueFilterService: RequestQueueFilterService,
        private stateService: StateService,
        private requestQueueCacheService: RequestQueueCacheService,
        @Inject(StoreToken) private store: IStore,
    ) { }

    ngOnInit() {
        this.requestQueueCacheService.initializeViewMap();
        this.subtaskViewId = this.stateService.params.subtaskViewId;
        this.filtersFromDashboard = this.stateService.params.filters;
        this.viewSelectorEnabled = this.permissions.canShow("DSARSavedViews");
        const currentUser: IUser = getCurrentUser(this.store.getState());
        this.requestQueueCacheService.currentUser = currentUser;
        const viewGetData: ISavedViewRequest = { viewTypes: ["requestqueuelist", "subtasklist"]};
        this.requestQueueService.getSavedViews(viewGetData)
        .pipe(
            take(1),
        )
        .subscribe((response: IProtocolResponse<ISavedViewResponse>) => {
            if (response.result) {
                this.views = response.data.uiViewInformation;
                this.views.map((value) => (value.name = this.translatePipe.transform(value.nameKey)));
                this.views = sortBy(this.views, "name");
                if (!this.viewAllRequests) {
                    if (this.viewSelectorEnabled) {
                        this.views = this.views.filter((view: ISavedView) => !this.defaultViews.includes(view.nameKey));
                        this.currentView = this.requestQueueCacheService.currentView ||
                            this.views.find((view: ISavedView) => view.id === response.data.defaultViewInfo.viewId) ||
                            this.views.find((view: ISavedView) => view.nameKey === "myrequests") ||
                            this.views[0];
                    } else {
                        this.currentView = this.views.find((view: ISavedView) => view.nameKey === this.defaultViews[0]) || this.views[0];
                    }
                    this.onViewChange(this.currentView);
                } else {
                    if (this.filtersFromDashboard && this.filtersFromDashboard.length) {
                        this.currentView = this.views.find((view: ISavedView) => view.nameKey === this.defaultViews[0]);
                    } else {
                        this.currentView = this.requestQueueCacheService.currentView ||
                        this.views.find((view: ISavedView) => view.id === response.data.defaultViewInfo.viewId) ||
                        this.views[0];
                    }
                    this.requestQueueCacheService.addView({...this.currentView, data: response.data.defaultViewInfo.data});
                    this.requestQueueCacheService.currentView = this.currentView;
                    this.error = false;
                    this.loadView();
                }
            } else {
                this.error = true;
                this.ready = true;
            }
        });
   }

   loadView() {
        this.searchPlaceholder = this.currentView.viewType === RequestQueueViewTypes.RequestQueueGrid ?
            this.translatePipe.transform("Search") :
            "";
        if (this.filtersFromDashboard && this.filtersFromDashboard.length) {
            this.filters = [...this.filtersFromDashboard];
            this.requestQueueCacheService.updateSearchString("");
            this.filtersFromDashboard = [];
        } else {
            this.filters = this.requestQueueCacheService.getFilters();
            if (this.filters && this.filters.length) {
                this.requestQueueFilterService.formatFilter(this.filters);
            }
        }
        if (this.currentView.viewType === RequestQueueViewTypes.RequestQueueGrid) {
            this.searchPlaceholder = this.translatePipe.transform("Search");
            this.requestQueueCacheService.updateFilters(this.filters);
        } else {
            this.searchPlaceholder = "";
            this.requestQueueCacheService.updateFilters(this.filters);
        }
        this.ready = true;
   }

   onViewChange(view: ISavedView) {
        this.showListDropdown = false;
        this.ready = false;
        this.resetHeaderValues();
        this.requestQueueService.getSavedView(view.id)
            .pipe(take(1))
            .subscribe((response: IProtocolResponse<ISavedView>) => {
                if (response.result) {
                    this.currentView = response.data;
                    this.requestQueueCacheService.addView(this.currentView);
                    this.requestQueueCacheService.currentView = this.currentView;
                    this.currentType = this.currentView.viewType;
                    this.loadView();
                    this.reloadGrid = !this.reloadGrid;
                }
                this.ready = true;
                this.error = false;
            });
   }

   resetHeaderValues() {
        this.showingFilteredResults = false;
        this.noOfItems = "";
        this.filtersApplied = "";
   }

   handleViewSelectorOutsideClick() {
        this.showListDropdown = false;
   }

    handleAction(type: string, payload: any) {
        this.requestQueueService.eventsSource.next({
            type,
            payload,
        });
    }

    filtersAdded(value: boolean | IFilterItem) {
        this.showingFilteredResults = !!value;
        if (value) {
            const filterItem: IFilterItem = value as IFilterItem;
            if (filterItem.items > 1) {
                this.noOfItems = `${filterItem.items} ${this.translatePipe.transform("Items")}`;
            } else {
                this.noOfItems = `${filterItem.items} ${this.translatePipe.transform("Item")}`;
            }
            this.filtersApplied = `${filterItem.filters} ${this.translatePipe.transform("FiltersApplied")}`;
        } else {
            this.noOfItems = "";
            this.filtersApplied = "";
        }
    }
}
