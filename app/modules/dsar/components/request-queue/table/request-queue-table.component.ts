// Angular
import {
    Component,
    EventEmitter,
    Input,
    Output,
} from "@angular/core";

// Lodash
import {
    remove,
    isNull,
 } from "lodash";

 // Services
import { RequestQueueDropdownService } from "dsarservice/request-queue-dropdown.service";
import {
    IRequestQueueDropdownAction,
    IRequestQueueDetails,
    IGetAllRequests,
    ITableColumns,
} from "dsarModule/interfaces/request-queue.interface";
import { IAction } from "interfaces/redux.interface";
import { RequestQueueStatus } from "dsarModule/enums/request-queue-status.enum";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

@Component({
    selector: "dsar-request-queue-table",
    templateUrl: "./request-queue-table.component.html",
})
export class RequestQueueTable {

    @Input() rows;
    @Input() sortOrder: string;
    @Input() sortKey: string | number;
    @Input() columnsMap: ITableColumns[];
    @Output() handleTableActions = new EventEmitter<IAction>();

    columns;
    // table config
    sortable = true;
    resizable = true;
    responsive = true;
    rowBordered = true;
    menuClass: string;
    RequestQueueStatusEnums = RequestQueueStatus;
    pageRange: string;

    constructor(
        private requestQueueDropdownService: RequestQueueDropdownService,
        private readonly translatePipe: TranslatePipe,
    ) { }

    handlePageChange(page: number): void {
        this.handleTableActions.emit({ type: "CHANGE_PAGE", payload: { page } });
    }

    handleSortChange(event: any): void {
        this.sortKey = event.sortKey;
        this.sortOrder = event.sortOrder;
        this.handleTableActions.emit({ type: "SORT_QUEUE", payload: { sortKey: this.sortKey, sortOrder: this.sortOrder } });
    }

    handleNameClick(rowData: IGetAllRequests, event: JQueryInputEventObject): void {
        if (event.ctrlKey || event.metaKey || event.shiftKey) {
            return;
        }
        this.handleTableActions.emit({ type: "GO_TO_DETAIL_PAGE", payload: { rowData } });
    }

    getActions(rowData: IRequestQueueDetails): () => IRequestQueueDropdownAction[] {
        return (): IRequestQueueDropdownAction[] => {
            this.setMenuClass(rowData);
            let actions: IRequestQueueDropdownAction[] = [];
            if (!rowData.taskMode && rowData.status !== this.RequestQueueStatusEnums.Locked) {
                actions = this.requestQueueDropdownService.getActions(rowData);
                actions = remove(this.requestQueueDropdownService.getActions(rowData), (action: IRequestQueueDropdownAction): boolean => {
                    switch (action.id) {
                        case "requestQueueUpdateRequestEntries":
                            return rowData.workflowDetailDto.allowRequestQueueUpdate;
                        case "requestQueueExtend":
                            return !isNull(rowData.deadline);
                        case "requestQueueRegenerate":
                            return rowData.authEnabled && rowData.passwordReGenerate;
                        default:
                            return true;
                    }
                });
            }
            if (!actions.length) {
                actions.push({
                    text: this.translatePipe.transform("NoActionsAvailable"),
                    action: (): void => { }, // Keep Empty
                });
            }
            return actions.map((actionItem: IRequestQueueDropdownAction): IRequestQueueDropdownAction => {
                switch (actionItem.id) {
                    case "requestQueueAssign":
                        actionItem.action = (action: IRequestQueueDropdownAction): void => this.handleTableActions.emit({ type: "ASSIGN_REQUEST", payload: actionItem.rowRefId });
                        break;
                    case "requestQueueExtend":
                        actionItem.action = (action: IRequestQueueDropdownAction): void => this.handleTableActions.emit({ type: "EXTEND_REQUEST", payload: actionItem.rowRefId });
                        break;
                    case "requestQueueReject":
                        actionItem.action = (action: IRequestQueueDropdownAction): void => this.handleTableActions.emit({ type: "REJECT_REQUEST", payload: actionItem.rowRefId });
                        break;
                    case "requestQueueDelete":
                        actionItem.action = (action: IRequestQueueDropdownAction): void => this.handleTableActions.emit({ type: "DELETE_REQUEST", payload: actionItem.rowRefId });
                        break;
                    case "requestQueueRevoke":
                        if (rowData.dsAccessRevoked) {
                            actionItem.textKey = "GrantAccess";
                            actionItem.action = (action: IRequestQueueDropdownAction): void => this.handleTableActions.emit({ type: "GRANT_ACCESS", payload: actionItem.rowRefId });
                        } else {
                            actionItem.textKey = "RevokeAccess";
                            actionItem.action = (action: IRequestQueueDropdownAction): void => this.handleTableActions.emit({ type: "REVOKE_ACCESS", payload: actionItem.rowRefId });
                        }
                        break;
                    case "requestQueueClose":
                        actionItem.action = (action: IRequestQueueDropdownAction): void => this.handleTableActions.emit({ type: "CLOSE_REQUEST", payload: actionItem.rowRefId });
                        break;
                    case "requestQueueUpdateRequestEntries":
                        actionItem.action = (action: IRequestQueueDropdownAction): void => this.handleTableActions.emit({ type: "UPDATE_REQUEST_ENTRIES", payload: actionItem.rowRefId });
                        break;
                    case "requestQueueChangeWorkflow":
                        actionItem.action = (action: IRequestQueueDropdownAction): void => this.handleTableActions.emit({ type: "CHANGE_WORKFLOW", payload: actionItem.rowRefId });
                        break;
                    default:
                        actionItem.action = (): void | undefined => { return; };
                }
                return actionItem;
            });
        };
    }

    private setMenuClass(row: IRequestQueueDetails): void {
        this.menuClass = "actions-table__context-list";
        if (this.rows.length <= 10) return;
        const halfwayPoint: number = (this.rows.length - 1) / 2;
        const rowIndex: number = this.rows.findIndex((item: any): boolean => row.requestQueueId === item.requestQueueId);
        if (rowIndex > halfwayPoint) this.menuClass += " actions-table__context-list--above";
    }
}
