// Angular
import {
    Component,
    OnInit,
    Inject,
} from "@angular/core";

// Rxjs
import { Subject } from "rxjs";
import { take } from "rxjs/operators";

// services
import { RequestQueueService } from "dsarservice/request-queue.service";
import { DSARWorkflowService } from "dsarservice/dsar-workflows.service";
import { NotificationService } from "modules/shared/services/provider/notification.service";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

// Interfaces
import { IConfirmationModalResolve } from "interfaces/confirmation-modal-resolve.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";
import { IPublishedWorkflow } from "interfaces/workflows.interface";
import { IOtModalContent } from "@onetrust/vitreus";
import { IChangeWorkflowRequest } from "modules/dsar/interfaces/request-queue.interface";

@Component({
    selector: "request-queue-change-workflow",
    templateUrl: "./request-queue-change-workflow.component.html",
})
export class RequestQueueChangeWorkflow implements OnInit, IOtModalContent {
    saveInProgress = false;
    canSubmit = false;
    modalData: IConfirmationModalResolve;
    isLoading = true;
    otModalCloseEvent: Subject<boolean | null>;
    otModalData: {
        requestId: string;
        title: string;
        workflowName: string;
        workflowId: string;
        authEnabled: boolean;
    };
    targetWorkflow: IPublishedWorkflow;
    deleteOpenSubtasks = true;
    publishedWorkflows: IPublishedWorkflow[];

    constructor(
        readonly requestQueueService: RequestQueueService,
        readonly workflowsService: DSARWorkflowService,
        private translatePipe: TranslatePipe,
        private notificationService: NotificationService,
    ) { }

    ngOnInit() {
        const params = {
            mfaSetting: this.otModalData.authEnabled ? "mfaEnabled" : "mfaDisabled",
        };
        this.workflowsService.getPublishedWorkflows(params).then((response: IProtocolResponse<IPublishedWorkflow[]>): void => {
            if (response.result) {
                this.publishedWorkflows = response.data;
                const currentWorkflow = this.publishedWorkflows.find(
                (workflow: IPublishedWorkflow) => workflow.referenceId === this.otModalData.workflowId);
                currentWorkflow.workflowName = currentWorkflow.workflowName + ` (${this.translatePipe.transform("LatestVersion")}) `;
            }
            this.isLoading = false;
        });
    }

    changeWorkflow(workflow: IPublishedWorkflow) {
        this.targetWorkflow = workflow;
        this.canSubmit = true;
    }

    handleSubmit() {
        this.saveInProgress = true;
        const changeWorkflowRequest: IChangeWorkflowRequest = {
            requestIds: [this.otModalData.requestId],
            targetWorkflow: this.targetWorkflow.referenceId,
            deleteOpenSubtasks: this.deleteOpenSubtasks,
        };
        this.requestQueueService.changeWorkflow(changeWorkflowRequest)
            .pipe(take(1))
            .subscribe((response: IProtocolResponse<void>) => {
                this.saveInProgress = false;
                if (response.result) {
                    this.notificationService.alertSuccess(
                        this.translatePipe.transform("WorkflowChanged"),
                        this.translatePipe.transform("WorkflowChangedSubtext"),
                    );
                    this.otModalCloseEvent.next(true);
                } else {
                    this.notificationService.alertError(
                        this.translatePipe.transform("Error"),
                        this.translatePipe.transform("ErrorChangingWorkflow"),
                    );
                }
            });
    }

    closeModal() {
        this.otModalCloseEvent.next();
    }

}
