// Angular
import {
    Component,
    Inject,
    Input,
    OnInit,
    OnDestroy,
} from "@angular/core";

// Rxjs
import {
    tap,
    concatMap,
    takeUntil,
    take,
} from "rxjs/operators";
import {
    from,
    forkJoin,
    throwError,
    of,
    Subject,
    BehaviorSubject,
} from "rxjs";

// Lodash
import {
    reverse,
    toString,
    forEach,
    cloneDeep,
    concat,
    isNil,
    isObject,
    sortBy,
    find,
} from "lodash";

// Services
import { TimeStamp } from "oneServices/time-stamp.service";
import { RequestQueueService } from "dsarservice/request-queue.service";
import { IProtocolResponse } from "interfaces/protocol.interface";
import { RequestQueueHistoryService } from "dsarservice/request-queue-history.service";
import { DSARWorkflowService } from "dsarservice/dsar-workflows.service";
import { UserApiService } from "oneServices/api/user-api.service";
import { WebformApiService } from "dsarservice/webform-api.service";
import { TenantTranslationService } from "sharedServices/tenant-translation.service";
import Utilities from "Utilities";

// Interfaces
import { IStringMap } from "interfaces/generic.interface";
import { IStore } from "interfaces/redux.interface";
import { ITranslations } from "dsarModule/interfaces/dsar-update-details-modal.interface";
import {
    IAuditHistoryItem,
    IFormattedActivity,
    IAuditChange,
    IGetPageableAudit,
} from "dsarModule/interfaces/audit-history.interface";
import {
    IUser,
} from "interfaces/user.interface";
import { IUserMap } from "interfaces/user-reducer.interface";
import { IPublishedWorkflow } from "interfaces/workflows.interface";
import { IRequestResolution } from "dsarModule/interfaces/request-queue.interface";

// Constants
import { EmptyGuid } from "constants/empty-guid.constant";
import { FieldNames } from "dsarModule/constants/dsar-history.constants";
import { LatestVersionWorkFlowID } from "modules/dsar/constants/dsar-settings.constant";
import { Regex } from "constants/regex.constant";

// Tokens
import { StoreToken } from "tokens/redux-store.token";
import { RequestQueueStatus } from "dsarModule/enums/request-queue-status.enum";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

@Component({
    selector: "request-queue-history",
    templateUrl: "./request-queue-history.component.html",
})
export class RequestQueueHistoryComponent implements OnInit, OnDestroy {
    @Input() requestQueueId;
    @Input() workflowList;
    @Input() templateId: string;

    destroy$ = new Subject();
    formTranslations: IStringMap<string>;
    public expandedItemMap: IStringMap<boolean> = {};
    public allItemsExpanded = true;
    public activitiesAscending = true;
    public activitiesInView: IFormattedActivity[] = [];
    activitiesInView$: BehaviorSubject<IFormattedActivity[]> = new BehaviorSubject([] as IFormattedActivity[]);
    public hasMoreActivities: boolean;
    public orderIcon = "ot ot-sort-amount-asc";
    public iconClass;
    public iconBgColor;
    public isLoading: boolean;
    public loadingEnabled: boolean;
    public totalHistoryItems: number;
    public currentItemCount: number;
    public workflows: IPublishedWorkflow[];
    resolutions: IRequestResolution[];
    mappedUsers: IStringMap<IUser | { FullName: string }> = {
        "00000000-0000-0000-0000-ffffffffffff": {
            FullName: "Data Subject",
        },
    };

    private activityList = [] as IAuditHistoryItem[];
    private formattedActivities: IFormattedActivity[] = [];
    private currentPage: number;
    private readonly defaultConfig = {
        increment: 8,
        iconColor: "#FFFFFF",
        iconBackgroundColor: "#1F96DB",
    };

    constructor(
        @Inject(StoreToken) private store: IStore,
        @Inject(UserApiService) private userApi: UserApiService,
        @Inject("TenantTranslationService") private tenantTranslationService: TenantTranslationService,
        readonly timeStampService: TimeStamp,
        readonly requestQueueService: RequestQueueService,
        readonly HistoryService: RequestQueueHistoryService,
        private webformService: WebformApiService,
        private translatePipe: TranslatePipe,
        readonly WorkflowsService: DSARWorkflowService,
    ) { }

    public ngOnInit(): void {
        this.isLoading = this.loadingEnabled = true;
        this.HistoryService.resetHistory();
        this.HistoryService.updateHistory(this.requestQueueId);
        this.fetchData();
    }

    fetchData() {
        const sessionLanguage = this.tenantTranslationService.getCurrentLanguage();
        forkJoin(
            this.requestQueueService.getAllResolutions(),
            from(this.WorkflowsService.getPublishedWorkflows()),
            from(this.webformService.getTranslations(this.templateId, sessionLanguage)),
        ).pipe(
            takeUntil(this.destroy$),
            tap((responses: [
                IProtocolResponse<IRequestResolution[]>,
                IProtocolResponse<IPublishedWorkflow[]>,
                IProtocolResponse<ITranslations>
            ]) => {
                if (responses[0].result) {
                    this.resolutions = responses[0].data;
                }
                if (responses[1].result) {
                    this.workflows = responses[1].data;
                }
                if (responses[2].result) {
                    this.formTranslations = responses[2].data.Translations;
                }
            }),
            concatMap((values: [IProtocolResponse<IRequestResolution[]>, IProtocolResponse<IPublishedWorkflow[]>, IProtocolResponse<ITranslations>]) => {
                return this.HistoryService.contentObservable.pipe(
                    tap((res: IProtocolResponse<IGetPageableAudit>) => {
                        if (res.result) {
                            this.activityList = cloneDeep(res.data.content);
                            this.totalHistoryItems = res.data.totalElements;
                            this.currentPage = res.data.number;
                            this.currentItemCount = 0;
                        }
                    }),
                    concatMap((res: IProtocolResponse<IGetPageableAudit>) => {
                        const userList = [];
                        if (res.result) {
                            this.activityList.forEach((activity: IAuditHistoryItem): void => {
                                activity.changes.forEach((change: IAuditChange): void => {
                                    userList.push(activity.userId);
                                    if (change.fieldName === FieldNames.Approver || change.fieldName === FieldNames.RequestDetailsEmailSent) {
                                        if (Utilities.matchRegex(change.newValue.name, Regex.GUID)) {
                                            userList.push(change.newValue.name);
                                        } else if (Utilities.matchRegex(change.oldValue.name, Regex.GUID)) {
                                            userList.push(change.oldValue.name);
                                        } else if (Regex.GUID.test(change.newValue.name.split(":")[0])) {
                                            userList.push(change.newValue.name.split(":")[0]);
                                        }
                                    }
                                });
                            });
                        }
                        return of(userList);
                    }),
                );
            }),
            concatMap((userList: any[]) => {
                if (userList && userList.length) {
                    return this.userApi.getBasicUserInfoList(userList);
                } else {
                    return of({} as IProtocolResponse<IUserMap>);
                }
            }),
            take(1),
        ).subscribe((response: IProtocolResponse<IUserMap>) => {
            if (response.result) {
                for (const userId in response.data) {
                    if (response.data.hasOwnProperty(userId)) {
                        this.mappedUsers[userId] = response.data[userId];
                    }
                }
            }
        },
        (err) => {
            throwError(err);
        },
        () => {
            this.formattedActivities = this.formatData(this.activityList);
            this.setExpandedItemMap();
            this.addActivitiesToView(this.formattedActivities, true);
            this.isLoading = this.loadingEnabled = false;
        });
    }

    public formatData(activityList: IAuditHistoryItem[]): IFormattedActivity[] {
        this.workflowList = sortBy(this.workflowList, "status");
        this.workflowList[RequestQueueStatus.Rejected] = {
            status: RequestQueueStatus.Rejected,
            stage: this.translatePipe.transform("Rejected"),
        };
        this.workflowList[RequestQueueStatus.Complete] = {
            status: RequestQueueStatus.Complete,
            stage: find(this.workflowList, ["status", RequestQueueStatus.Complete]).stage,
        };
        this.workflowList[RequestQueueStatus.Close] = {
            status: RequestQueueStatus.Close,
            stage: this.translatePipe.transform("Closed"),
        };
        this.workflowList[RequestQueueStatus.Unverified] = {
            status: RequestQueueStatus.Unverified,
            stage: this.translatePipe.transform("Unverified"),
        };
        this.workflowList[RequestQueueStatus.Expired] = {
            status: RequestQueueStatus.Expired,
            stage: this.translatePipe.transform("Expired"),
        };
        const allUsers = this.mappedUsers;
        const orgTableById = this.store.getState().orgs.rootOrgTable;
        const formattedActivities: IFormattedActivity[] = [];
        let overrideDateFormatting: boolean;
        if (!isNil(this.timeStampService.currentDateFormatting)) {
            overrideDateFormatting = !this.timeStampService.currentDateFormatting.trim().length;
        }
        activityList.forEach((activity: IAuditHistoryItem, index: number): void => {
            if (activity.userId === EmptyGuid) {
                activity.userName = this.translatePipe.transform("System");
                this.iconClass = "ot ot-edit";
                this.iconBgColor = "#778CA2";
            } else {
                activity.userName = allUsers[activity.userId.toLowerCase()] ? allUsers[activity.userId.toLowerCase()].FullName : "- - - -";
                this.iconClass = "ot ot-check-square";
                this.iconBgColor = this.defaultConfig.iconBackgroundColor;
            }
            // handle DSAccessRevoked DSAccessGranted PasswordGenerated descriptions
            if (["DSAccessRevoked", "DSAccessGranted", "PasswordGenerated"].indexOf(activity.description) !== -1) {
                activity.description = this.translatePipe.transform(activity.description);
            } else if (activity.description && activity.description !== "Edit") {
                activity.description = `${this.translatePipe.transform("RulesApplied")}: ${activity.description}`;
            }

            forEach(activity.changes, (change: IAuditChange): void => {
                if (change.fieldName === FieldNames.Approver) {
                    if (change.newValue.name) {
                        if (Utilities.matchRegex(change.newValue.name, Regex.GUID)) {
                            change.newValue.name = allUsers[change.newValue.name] ? allUsers[change.newValue.name].FullName : "Unassigned";
                        }
                    } else {
                        change.newValue.name = null;
                    }
                    if (change.oldValue.name) {
                        if (Utilities.matchRegex(change.oldValue.name, Regex.GUID)) {
                            change.oldValue.name = allUsers[change.oldValue.name] ? allUsers[change.oldValue.name].FullName : "Unassigned";
                        }
                    } else {
                        change.oldValue.name = null;
                    }
                }
                if (change.fieldName === FieldNames.ManagingOrganization || change.fieldName === FieldNames.OrganizationName) {
                    change.newValue.name = orgTableById[change.newValue.name].Name;
                    change.oldValue.name = orgTableById[change.oldValue.name].Name;
                }
                if ((change.fieldName === FieldNames.Deadline) || (change.fieldName === FieldNames.DeadlineExtended)) {
                    if (change.newValue.name) {
                        const dateObject = new Date(change.newValue.name);
                        change.newValue.name = this.timeStampService.formatDate((dateObject.toISOString()), overrideDateFormatting);
                    }
                    if (change.oldValue.name) {
                        // IE not able to handle scenario where time separator absent
                        if (isNaN(Date.parse(change.oldValue.name))) {
                            change.oldValue.name = change.oldValue.name.split(" ").join("T");
                        }
                        const dateObject = new Date(change.oldValue.name);
                        change.oldValue.name = this.timeStampService.formatDate((dateObject.toISOString()), overrideDateFormatting);
                    }
                }
                if (change.fieldName === FieldNames.Stage) {
                    if (change.newValue.name.match(/^\{"status":/) && isObject(JSON.parse(change.newValue.name))) {
                        if (JSON.parse(change.newValue.name).status !== null) {
                            change.newValue.name = this.workflowList[JSON.parse(change.newValue.name).status].stage;
                        }
                        if (JSON.parse(change.oldValue.name).status !== null) {
                            change.oldValue.name = this.workflowList[JSON.parse(change.oldValue.name).status].stage;
                        }
                    }
                }
                if (change.fieldName === FieldNames.Resolution) {
                    change.newValue.name = change.newValue.name ? this.resolutions.find((element) => element.id === change.newValue.name).title : "";
                    change.oldValue.name = change.oldValue.name ? this.resolutions.find((element) => element.id === change.oldValue.name).title : "";
                }
                if (change.fieldName === FieldNames.WorkflowName) {
                    for (const workflow of this.workflows) {
                        if (workflow.referenceId === change.oldValue.name) {
                            change.oldValue.name = workflow.workflowName;
                        } else if (workflow.referenceId === change.newValue.name) {
                            change.newValue.name = workflow.workflowName;
                        }
                    }
                    if (change.newValue.name === LatestVersionWorkFlowID) {
                        change.newValue.name = `${change.oldValue.name} (${this.translatePipe.transform("LatestVersion")})`;
                    }
                }
                if (change.fieldName === FieldNames.RequestDetailsEmailSent) {
                    const userId = change.newValue.name.split(":")[0];
                    const emailId = change.newValue.name.split(":")[1];
                    change.newValue.name = `${allUsers[userId].FullName} (${emailId})`;
                }
                if (change.fieldName === FieldNames.ExpirationDate) {
                    if (change.newValue.name) {
                        change.newValue.name = this.timeStampService.formatDate(change.newValue.name);
                    }
                    if (change.oldValue.name) {
                        change.oldValue.name = this.timeStampService.formatDate(change.oldValue.name);
                    }
                }
                const fieldName = change.fieldName.replace(/ /g, "");
                // we need to capitalize this as the history api sends keys like country in camel case
                const fieldNameCapitalized = fieldName.charAt(0).toUpperCase() + fieldName.slice(1);
                if (FieldNames[fieldNameCapitalized]) {
                    change.fieldName = this.translatePipe.transform(fieldNameCapitalized);
                } else {
                    change.fieldName = this.formTranslations[fieldName];
                }
            });
            formattedActivities.push({
                id: toString(this.currentItemCount++),
                title: this.translatePipe.transform("UserMadeChanges", { userName: activity.userName }),
                description: activity.description,
                timeStamp: this.timeStampService.formatDate(activity.changeDateTime),
                icon: this.iconClass,
                iconColor: this.defaultConfig.iconColor,
                iconBackgroundColor: this.iconBgColor,
                changes: activity.changes,
            });
        });
        return formattedActivities;
    }

    public addMoreElements(): void {
        this.loadingEnabled = true;
        this.requestQueueService.getHistory(this.requestQueueId, ++this.currentPage).then((res: IProtocolResponse<IGetPageableAudit>): void => {
            const itemsList = res.data.content;
            this.activityList = concat(this.activityList, res.data.content);
            const formattedActivities: IFormattedActivity[] = this.formatData(itemsList);
            this.formattedActivities = concat(this.formattedActivities, formattedActivities);
            this.setExpandedItemMap();
            this.setAllItemsExpanded();
            this.addActivitiesToView(formattedActivities);
            this.loadingEnabled = false;
        });
    }

    public addActivitiesToView(formattedActivities: IFormattedActivity[], listReplace: boolean = false): void {
        const formatActivities: IFormattedActivity[] = cloneDeep(formattedActivities);
        if (listReplace) {
            this.activitiesInView = formatActivities;
            this.activitiesInView$.next(this.activitiesInView);
        } else {
            this.activitiesInView = concat(this.activitiesInView, formatActivities);
            this.activitiesInView$.next(this.activitiesInView);
        }
        if (!this.activitiesAscending) {
            this.reverseActivityOrder();
        }
    }

    public toggleActivity(itemId: string): void {
        this.expandedItemMap[itemId] = !this.expandedItemMap[itemId];
        this.setAllItemsExpanded();
    }

    public setExpandedItemMap(): void {
        this.expandedItemMap = {};
        this.formattedActivities.forEach((activity: IFormattedActivity): void => {
            this.expandedItemMap[activity.id] = true;
        });
        this.setAllItemsExpanded();
    }

    public setAllItemsExpanded(): void {
        this.allItemsExpanded = this.checkIfAllExpanded();
    }

    public toggleExpand(): void {
        Object.keys(this.expandedItemMap).forEach((key: string) => this.expandedItemMap[key] = !this.allItemsExpanded);
        this.setAllItemsExpanded();
    }

    public toggleOrder(): void {
        this.activitiesAscending = !this.activitiesAscending;
        this.orderIcon = this.activitiesAscending ? "ot ot-sort-amount-asc" : "ot ot-sort-amount-desc";
        this.reverseActivityOrder();
    }

    public reverseActivityOrder(): void {
        this.activitiesInView = reverse(this.activitiesInView);
        this.activitiesInView$.next(this.activitiesInView);
    }

    ngOnDestroy() {
        this.destroy$.next();
        this.destroy$.complete();
    }

    private checkIfAllExpanded(): boolean {
        const keys = Object.keys(this.expandedItemMap);

        if (keys.length === this.activityList.length) {
            for (let i = 0; i < keys.length; i++) {
                if (!this.expandedItemMap[keys[i]]) {
                    return false;
                }
            }
        } else {
            return false;
        }
        return true;
    }
}
