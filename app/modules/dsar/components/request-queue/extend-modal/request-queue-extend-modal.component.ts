// Angular
import {
    Component,
    OnInit,
} from "@angular/core";

// RxJs
import {
    Subject,
    BehaviorSubject,
    Observable,
} from "rxjs";

// Constants
import { Regex } from "constants/regex.constant";

// 3rd Party Library
import { isNil } from "lodash";

// services
import { RequestQueueService } from "dsarservice/request-queue.service";
import { DSARSettingsService } from "dsarservice/dsar-settings.service";

// Interfaces
import { IOtModalContent } from "@onetrust/vitreus";
import { IRequestQueueExtend } from "modules/dsar/interfaces/request-queue-actions.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";
import { IResponseTemplate } from "modules/dsar/interfaces/response-templates.interface";

@Component({
    selector: "request-queue-extend-modal",
    templateUrl: "./request-queue-extend-modal.component.html",
})

export class RequestQueueExtendModal implements OnInit, IOtModalContent {
    comment = "";
    daysToExtend = 60;
    daysToExtendError = false;
    daysToExtendTouched = false;
    saveInProgress = false;
    canSubmit = false;
    commentTouched = false;
    customModules = [["bold", "italic", "underline", "strike", "image", { list: "ordered" }, { list: "bullet" }],
    [{ align: "" }, { align: "right" }, { align: "center" }, { align: "justify" }, "link"]];
    otModalCloseEvent: Subject<boolean | null>;
    otModalData: {
        comment: string;
        requestQueueId: string;
        responseTemplates?: IResponseTemplate[];
    };
    options = [{
        key: "1",
        value: "1",
    }, {
        key: "2",
        value: "2",
    }];
    currentTemplate = {
        key: "1",
        value: "1",
    };
    contentSource = new BehaviorSubject("");
    contentObservable: Observable<string> = this.contentSource.asObservable();
    responseTemplates: IResponseTemplate[];
    selectedTemplate: IResponseTemplate;
    isLoading = true;

    constructor(
        private requestQueueService: RequestQueueService,
        private readonly dsarSettingsService: DSARSettingsService,
    ) { }

    ngOnInit() {
        if (this.otModalData.comment) {
            this.comment = this.otModalData.comment || "";
            this.contentSource.next(this.comment);
            this.validateSubmit();
        }
        this.responseTemplates = this.otModalData.responseTemplates;
        this.isLoading = !this.responseTemplates;
        if (!this.responseTemplates) {
            this.dsarSettingsService.getAllEnabledResponseTemplatesByRequestQueue(this.otModalData.requestQueueId)
                .then((res: IProtocolResponse<IResponseTemplate[]>): void => {
                    if (res.result) {
                        this.responseTemplates = res.data;
                    }
                    this.isLoading = false;
                });
        }
    }

    handleSubmit(): void {
        this.saveInProgress = true;
        const extendRequest: IRequestQueueExtend[] = [{
            requestQueueId: this.otModalData.requestQueueId,
            daysToExtend: this.daysToExtend,
            comment: this.comment,
            isInternalComment: false,
        }];
        this.requestQueueService.extendRequest(extendRequest).then((response: IProtocolResponse<void>) => {
            if (response.result) {
                this.otModalCloseEvent.next(true);
            } else {
                this.saveInProgress = false;
            }
        });
    }

    validateSubmit(): void {
        this.canSubmit = Boolean(this.comment && this.daysToExtend && !this.daysToExtendError);
    }

    public validateDays = (event: KeyboardEvent): void => {
        const days = (event as any).target.value.trim();
        this.daysToExtendTouched = true;
        const checkNumber = new RegExp(Regex.NON_NUMBER);
        // Valid days are between 1 and 120.
        this.daysToExtendError = isNil(days) || days < 1  || days > 120 || checkNumber.test(days);
        if (!this.daysToExtendError) {
            this.daysToExtend = days;
        }
        this.validateSubmit();
    }

    handleMessageChange(message: string): void {
        if (message && !message.includes("img") && !$(message).text().trim()) {
            this.comment = "";
        } else {
            this.comment = message;
        }
        this.commentTouched = true;
        this.validateSubmit();
    }

    selectTemplate(template: IResponseTemplate): void {
        if (template.templateContent) {
            this.comment += template.templateContent;
            this.contentSource.next(this.comment.replace(/(?:\r\n|\r|\n)/g, "<br>"));
        }
    }

    closeModal(): void {
       this.otModalCloseEvent.next();
    }
}
