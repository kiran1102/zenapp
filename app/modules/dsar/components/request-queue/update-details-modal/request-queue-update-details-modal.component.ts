// Angular
import {
    Component,
    OnInit,
    Inject,
    ViewChild,
} from "@angular/core";

// Lodash
import {
    forEach,
    includes,
    sortBy,
    keys,
} from "lodash";

import Utilities from "Utilities";

// 3rd party
import { Subject } from "rxjs";

// Services
import { TenantTranslationService } from "sharedServices/tenant-translation.service";
import { RequestQueueService } from "dsarservice/request-queue.service";
import { WebformApiService } from "dsarservice/webform-api.service";

// Constants and enums
import { Regex } from "constants/regex.constant";
import { WebformControls } from "dsarModule/constants/web-form-control.constant";
import { IOptionsType } from "sharedModules/enums/options-type.enum";
import { RequestQueueTypes } from "dsarModule/enums/request-queue.enum";

// Pipes
import { ConvertToOptionsPipe } from "modules/pipes/convert-to-options.pipe";

// Interfaces
import { IProtocolResponse } from "interfaces/protocol.interface";
import { IOtModalContent } from "@onetrust/vitreus";
import {
    IRequestQueueDetails,
    IMultiselectFields,
} from "dsarModule/interfaces/request-queue.interface";
import {
    IUpdateDetailsModalResolve,
    IUpdateDetailsFieldData,
    IEntityItem,
    IUpdateDetailsPutData,
    IUpdateDetailsGetData,
    IUpdateFormFieldData,
    IUpdateRequestSubjectTypes,
    IUpdateFormData,
    ITranslations,
 } from "dsarModule/interfaces/dsar-update-details-modal.interface";
import {
    IGetWebformLanguageResponse,
    Status,
} from "interfaces/dsar/dsar-edit-language.interface";
import {
    IStringMap,
    ILabelValue,
    IKeyValue,
} from "interfaces/generic.interface";
import { TranslatePipe } from "modules/pipes/translate.pipe";

@Component({
    selector: "request-queue-update-details-modal",
    templateUrl: "./request-queue-update-details-modal.component.html",
})
export class RequestQueueUpdateDetailsModal implements OnInit, IOtModalContent {
    requestQueueTypes = RequestQueueTypes;
    otModalCloseEvent: Subject<IProtocolResponse<IRequestQueueDetails>>;
    otModalData: {
        title: string;
        requestQueueDetails: IRequestQueueDetails,
    };
    resolve: IUpdateDetailsModalResolve;
    isSubmitting: boolean;
    isLoading: boolean;
    formTranslations: any;
    fieldData: IUpdateDetailsFieldData[];
    requestQueueDetails: IRequestQueueDetails;
    canSubmit = false;
    currentFilterValue = "";
    @ViewChild("lookup") lookupInput;
    @ViewChild("subjectTypeLookup") subjectTypeLookup;
    @ViewChild("requestTypeLookup") requestTypeLookup;

    private validationMap = {
        [WebformControls.Email]: this.translatePipe.transform("ErrorEnterValidEmailAddress"),
        [WebformControls.FirstName]: this.translatePipe.transform("ErrorEnterValidFirstName"),
        [WebformControls.LastName]: this.translatePipe.transform("ErrorEnterValidLastName"),
    };
    private defaultErrorMessage = this.translatePipe.transform("ErrorFillThisField");
    private languageList: IGetWebformLanguageResponse[];
    private allLanguagesList: IGetWebformLanguageResponse[];
    private selectedLanguage: IGetWebformLanguageResponse;
    private modalData: IUpdateDetailsGetData;
    private userSessionLanguage: string;
    private fieldKeys: string[] = [
        "email",
        "firstName",
        "lastName",
        "requestTypes",
        "subjectTypes",
        "RequestTypeText",
        "SubjectTypeText",
    ];

    constructor(
        private dsarWebformService: WebformApiService,
        private requestQueueService: RequestQueueService,
        @Inject("TenantTranslationService") private tenantTranslationService: TenantTranslationService,
        private convertToOptionsPipe: ConvertToOptionsPipe,
        private translatePipe: TranslatePipe,
    ) { }

    ngOnInit(): void {
        this.isLoading = true;
        this.requestQueueDetails = this.otModalData.requestQueueDetails;
        this.userSessionLanguage = this.tenantTranslationService.getCurrentLanguage();
        Promise.all([
            this.requestQueueService.getUpdateDetails(this.requestQueueDetails.templateId),
            this.requestQueueService.getDefaultRequestById(this.requestQueueDetails.requestQueueId, this.userSessionLanguage, false),
        ]).then((response: Array<IProtocolResponse<any>>): void => {
            this.modalData = response[0].data;
            this.requestQueueDetails = response[1].data;
            this.allLanguagesList = response[1].data.languageList;
            this.languageList = this.allLanguagesList.filter((language: IGetWebformLanguageResponse) => language.Status === Status.Enabled);
            const sessionLanguage: IGetWebformLanguageResponse = this.languageList.find(
                (language: IGetWebformLanguageResponse) => language.Code === this.userSessionLanguage);
            const defaultLanguage: string = sessionLanguage ?
                this.userSessionLanguage :
                this.languageList.find((language: IGetWebformLanguageResponse) => language.IsDefault).Code;
            this.selectLanguage(defaultLanguage);
        });
    }

    closeModal(): void {
        this.otModalCloseEvent.next(null);
    }

    filterOptions = (field: IEntityItem, key: string): boolean => {
        const fieldData = this.getFieldData(key);
        if (fieldData.selectedValues) {
            return !(fieldData.selectedValues.find((value: IEntityItem): boolean => value.Id === field.Id));
        }
        return true;
    }

    fieldSearch({ key, value }) {
        if (key === "Enter") return;
        this.currentFilterValue = value;
    }

    clearInput() {
        this.currentFilterValue = "";
    }

    updateField(input: any, value: string) {
        input.isDirty = true;
        if (input.fieldKey === WebformControls.Email) {
            input.isValid = Utilities.matchRegex(value, Regex.EMAIL);
        } else {
            input.isValid = input.isRequired ? value.trim().length > 0 : true;
        }
        this.canSubmit = this.validateForm();
    }

    onCountryChange(currentSelection: ILabelValue<string>): void {
        const country = this.getFieldData("country");
        country.selectedValue = currentSelection;
        this.canSubmit = this.validateForm();
    }

    handleAddMultiSelect(input: any, data: IEntityItem[]) {
        input.isDirty = true;
        this.currentFilterValue = "";
        const index: number = this.fieldData.findIndex(
            (fieldDataObj: IUpdateDetailsFieldData) => fieldDataObj.fieldKey === input.fieldKey);
        this.fieldData[index].selectedValues = data;
        this.fieldData[index].options = [...this.fieldData[index].options] as any;
        input.isValid = input.isRequired ? this.fieldData[index].selectedValues.length > 0 : true;
        this.canSubmit = this.validateForm();
    }

    handleSubmit(): void {
        const data = this.getRequestData();
        this.isSubmitting = true;
        this.requestQueueService.updateRequestQueue(
            this.requestQueueDetails.requestQueueId, data).then((response: IProtocolResponse<void>) => {
                if (response.result) {
                    this.requestQueueService.getRequest(this.requestQueueDetails.requestQueueId, data.language)
                        .then((res: IProtocolResponse<IRequestQueueDetails>) => {
                            this.otModalCloseEvent.next(res);
                            this.isSubmitting = false;
                        });
                } else {
                    this.isSubmitting = false;
                    this.otModalCloseEvent.next(null);
                }
            });
    }

    private getLanguage(languageId: string): IGetWebformLanguageResponse {
        return this.allLanguagesList.find(
            (language: IGetWebformLanguageResponse) => language.Code === languageId);
    }

    private selectLanguage(language: string): void {
        this.selectedLanguage = this.getLanguage(language);
        this.isLoading = true;
        this.dsarWebformService.getTranslations(this.requestQueueDetails.templateId, this.selectedLanguage.Code)
            .then((response: IProtocolResponse<ITranslations>): void => {
            this.formTranslations = response.data.Translations;
            this.fieldData = this.buildModalData(this.modalData);
            this.isLoading = false;
        });
    }

    private getAllOptions(type: string, data: IUpdateFormData): IEntityItem[] {
        const fieldData = sortBy(data[type], "order");
        return fieldData.map((field: IUpdateRequestSubjectTypes) => {
            return {
                Id: field.id,
                Label: this.formTranslations[field.fieldName],
            };
        });
    }

    private getAllMultiselectOptions(options: Array<IKeyValue<string>>): IEntityItem[] {
        const filterOptions = options.map((field: IKeyValue<string>) => {
            return {
                Label: this.formTranslations[field.key],
                Id: field.key,
            };
        });
        return filterOptions;
    }

    private getValues(type: string, data: IUpdateFormData): IEntityItem[] {
        return this.requestQueueDetails[type].map((field: IUpdateRequestSubjectTypes) => {
            return {
                Id: field.id,
                Label: this.formTranslations[
                    data[type].find((subField: any) => subField.id === field.id).fieldName
                ],
            };
        });
    }

    private getMultiSelectValues(type: string, data: IMultiselectFields[], options: IEntityItem[]): any {
        const filterData =  data.filter((section: IMultiselectFields): boolean => section.FieldKey === type);
        if (filterData.length) {
            const selectedValueOptions =  filterData[0].Values;
            return options.filter((option: IEntityItem) => selectedValueOptions.includes(option.Label));
        } else {
            return null;
        }
    }

    private getErrorMessage(key = "") {
        if (Object.keys(this.validationMap).includes(key)) {
            return this.validationMap[key];
        }
        return this.defaultErrorMessage;
    }

    private buildModalData(data: IUpdateDetailsGetData): IUpdateDetailsFieldData[] {
        const fieldData: IUpdateDetailsFieldData[] = [];
        const otherData = this.requestQueueDetails.otherData;
        const multiSelectData = this.requestQueueDetails.multiselectFields;
        let requestSelectedValues = this.getValues("requestTypes", data.form);
        let requestAllOptions = this.getAllOptions("requestTypes", data.form);
        fieldData.push({
            dataType: "Array",
            inputType: "MultiSelect",
            isRequired: true,
            fieldKey: "RequestTypeText",
            description: "",
            descriptionValue: "",
            selectedValues: requestSelectedValues,
            options: requestAllOptions,
            customField: false,
            isValid: true,
            errorMessage: this.getErrorMessage(),
        });
        requestSelectedValues = this.getValues("subjectTypes", data.form);
        requestAllOptions = this.getAllOptions("subjectTypes", data.form);
        fieldData.push({
            dataType: "Array",
            inputType: "MultiSelect",
            isRequired: true,
            fieldKey: "SubjectTypeText",
            description: "",
            descriptionValue: "",
            selectedValues: requestSelectedValues,
            options: requestAllOptions,
            customField: false,
            isValid: true,
            errorMessage: this.getErrorMessage(),
        });
        forEach(data.fields.data, (field: IUpdateFormFieldData) => {
            if (((field.isSelected && field.status !== Status.Disabled) // fields which are not deleted
                || otherData[field.fieldKey] !== undefined) // fields which are deleted but value exists
                && field.fieldKey !== "requestDetails") {
                const obj = {
                    dataType: field.dataType,
                    inputType: field.inputType,
                    isRequired: field.isRequired,
                    fieldKey: field.fieldKey,
                    description: "",
                    descriptionValue: "",
                    options: field.options,
                    customField: false,
                    isValid: true,
                    errorMessage: this.getErrorMessage(field.fieldKey),
                };
                if (this.fieldKeys.includes(field.fieldKey)) {
                    fieldData.push({...obj , selectedValue: this.requestQueueDetails[field.fieldKey]});
                } else {
                    if (field.inputType === "Multiselect") {
                        obj.options = this.getAllMultiselectOptions(field.options);
                        fieldData.push({
                            ...obj,
                            selectedValues: this.getMultiSelectValues(field.fieldKey, multiSelectData, obj.options as IEntityItem[]),
                            isRequired: false,
                        });
                    } else if (field.inputType === "Select") {
                        let options;
                        if (obj.options && typeof obj.options[0] === "object") {
                            options = obj.options.map((option: IKeyValue<string>) => {
                                return {
                                    label: option.value,
                                    value: option.key,
                                };
                            });
                        } else if (obj.options && obj.options.length) {
                            options = this.convertToOptionsPipe.transform(obj.options, IOptionsType.ILabelValue);
                        }
                        const selectedValue = options.find((option: ILabelValue<string>) => option.label === otherData[field.fieldKey]) || "";
                        fieldData.push({...obj, selectedValue, isRequired: false, options });
                    }
                }
            }
        });
        // custom url fields
        forEach(keys(otherData), (dataObj: string) => {
            if (!fieldData.find((fieldDataObj: any) => fieldDataObj.fieldKey === dataObj)) {
                let parsedValue = "";
                try {
                    parsedValue = JSON.parse(`"${otherData[dataObj]}"`);
                } catch (err) {
                    parsedValue = otherData[dataObj];
                }
                fieldData.push({
                    dataType: "Text",
                    inputType: "Text Field",
                    isRequired: false,
                    fieldKey: dataObj,
                    description: "",
                    descriptionValue: "",
                    selectedValue: parsedValue,
                    customField: true,
                    isValid: true,
                    errorMessage: this.getErrorMessage(),
                });
            }
        });
        return fieldData;
    }

    private validateForm(): boolean {
        return this.fieldData.every((field: IUpdateDetailsFieldData) => field.isValid);
    }

    private getOtherData(): IStringMap<string> {
        const otherData = {};
        forEach(this.fieldData, (field: IUpdateDetailsFieldData) => {
            if (!this.fieldKeys.includes(field.fieldKey) && field.inputType !== "Multiselect") {
                if (field.options && (field.selectedValue as ILabelValue<string>).label) {
                    otherData[field.fieldKey] = (field.selectedValue as ILabelValue<string>).label;
                } else if (!field.options) {
                    otherData[field.fieldKey] = field.selectedValue;
                }
            }
        });
        return otherData;
    }

    private getMultiSelectData(): IStringMap<string[]> {
        const otherData = {};
        forEach(this.fieldData, (field: IUpdateDetailsFieldData) => {
            if (!this.fieldKeys.includes(field.fieldKey)) {
                if (field.inputType === "Multiselect") {
                    otherData[field.fieldKey] = this.getOtherMultiSelectValues(field.fieldKey);
                }
            }
        });
        return otherData;
    }

    private getOtherMultiSelectValues(type: string): string[] {
        const field = this.getFieldData(type);
        if (field && field.selectedValues) {
            return field.selectedValues.map((value: IEntityItem) => (value as any).Id);
        }
        return [];
    }

    private getFieldData(type: string): IUpdateDetailsFieldData {
        return this.fieldData.find((field: IUpdateDetailsFieldData) => field.fieldKey === type);
    }

    private getRequestData(): IUpdateDetailsPutData {
        return {
            email: this.getFieldData("email").selectedValue as string,
            firstName: this.getFieldData("firstName").selectedValue as string,
            lastName: this.getFieldData("lastName").selectedValue as string,
            otherData: this.getOtherData(),
            multiSelectFields: this.getMultiSelectData(),
            requestTypes: this.getOtherMultiSelectValues("RequestTypeText"),
            subjectTypes: this.getOtherMultiSelectValues("SubjectTypeText"),
            language: this.selectedLanguage.Code,
        };
    }

}
