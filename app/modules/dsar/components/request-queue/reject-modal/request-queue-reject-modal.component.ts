// Angular
import {
    Component,
    Inject,
    OnInit,
    ElementRef,
} from "@angular/core";

// Redux

// Constants
import { RequestQueueStatus } from "dsarModule/enums/request-queue-status.enum";

// Interfaces
import {
    IRequestQueueRejectModalResolve,
    IRequestResolutionFormatted,
} from "dsarModule/interfaces/request-queue.interface";
import { IOtModalContent } from "@onetrust/vitreus/src/app/vitreus/components/modal/modal";
import { IRequestQueueReject } from "modules/dsar/interfaces/request-queue-actions.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";
import { IAttachmentFileResponse } from "interfaces/attachment-file.interface";

// 3rd Party Library
import { isFunction } from "lodash";
import { Subject } from "rxjs";

// services
import { RequestQueueService } from "dsarservice/request-queue.service";

@Component({
    selector: "request-queue-reject-modal",
    templateUrl: "./request-queue-reject-modal.component.html",
})
export class RequestQueueRejectModal implements IOtModalContent, OnInit {
    dsNotification = false;
    selectedRejectResolution: IRequestResolutionFormatted = {} as IRequestResolutionFormatted;
    attachments: IAttachmentFileResponse[];
    comment = "";
    saveInProgress = false;
    canSubmit = false;
    commentTouched = false;
    customModules = [["bold", "italic", "underline", "strike", "image", { list: "ordered" }, { list: "bullet" }],
    [{ align: "" }, { align: "right" }, { align: "center" }, { align: "justify" }, "link"]];
    otModalData: IRequestQueueRejectModalResolve;
    otModalCloseEvent: Subject<boolean>;
    dropdownOpen = false;
    previousIsDropdownOpen = false;
    currentIsDropdownOpen = false;

    constructor(
        private requestQueueService: RequestQueueService,
        private elementRef: ElementRef,
    ) {
    }

    ngOnInit() {
        if (this.otModalData.data && this.otModalData.data.comment) {
            this.comment = this.otModalData.data.comment;
            this.validateSubmit();
        }
    }

    ngAfterViewChecked() {
        this.currentIsDropdownOpen = this.elementRef.nativeElement.querySelector(".slds-is-open");
        if (this.currentIsDropdownOpen !== this.previousIsDropdownOpen) {
            const modalContent = this.elementRef.nativeElement.querySelector("ot-modal-content");
            modalContent.scrollTop = modalContent.scrollHeight;
            this.previousIsDropdownOpen = this.currentIsDropdownOpen;
        }
    }

    selectResolution(data: IRequestResolutionFormatted) {
        this.selectedRejectResolution = data;
    }

    handleSubmit(): void {
        const newRequest: IRequestQueueReject[] = [{
            requestQueueId: this.otModalData.requestQueueDetails.requestQueueId,
            comment: this.comment,
            isInternalComment: this.dsNotification,
            currentStatus: this.otModalData.requestQueueDetails.currentStageInfo ? this.otModalData.requestQueueDetails.currentStageInfo.status : this.otModalData.requestQueueDetails.status,
            newStatus: RequestQueueStatus.Rejected,
            resolutionId: this.selectedRejectResolution.id,
        }];
        if (this.attachments) {
            newRequest[0].attachments = this.attachments;
        }
        this.saveInProgress = true;
        this.requestQueueService.rejectRequest(newRequest).then((response: IProtocolResponse<void>) => {
            if (response.result) {
                this.closeModal(true);
            } else {
                this.saveInProgress = false;
            }
        });
    }

    toggleNotification(isChecked: boolean): void {
        this.dsNotification = isChecked;
    }

    validateSubmit(): void {
        this.canSubmit = Boolean(this.comment);
    }

    handleMessageChange(message: string): void {
        if (message && !message.includes("img") && !$(message).text().trim()) {
            this.comment = "";
        } else {
            this.comment = message;
        }
        this.commentTouched = true;
        this.validateSubmit();
    }

    closeModal(responseResult?: boolean): void {
        this.otModalCloseEvent.next(responseResult);
    }

}
