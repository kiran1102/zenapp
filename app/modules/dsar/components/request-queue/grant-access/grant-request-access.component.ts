// Angular
import {
    Component,
    Inject,
    OnInit,
} from "@angular/core";

// Redux
import { getModalData } from "oneRedux/reducers/modal.reducer";
import { StoreToken } from "tokens/redux-store.token";

// Interfaces
import {
    IGrantAccessSubmit,
    IGrantAccessModal,
} from "dsarModule/interfaces/request-queue.interface";
import { IStore } from "interfaces/redux.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";

// services
import { ModalService } from "sharedServices/modal.service";
import { RequestQueueService } from "dsarservice/request-queue.service";

@Component({
    selector: "grant-request-access",
    templateUrl: "./grant-request-access.component.html",
})
export class GrantRequestAccessComponent implements OnInit {
    requestQueueId: string;
    saveLoading = false;
    saveDisabled = false;
    dateToRevokeText: string;
    dateToRevoke: Date;
    saveButtonText: string;
    cancelButtonText: string;
    headerTitle: string;
    bodyText: string;
    label: string;
    daysToRevoke: number;
    today: Date;
    private static readonly MAX_DAYS_TO_REVOKE = 9999;
    private modalData: IGrantAccessModal;

    constructor(
        @Inject(StoreToken) private store: IStore,
        private readonly ModalService: ModalService,
        private readonly RequestQueueService: RequestQueueService,
    ) {
        this.saveButtonText = "GrantAccess";
        this.cancelButtonText = "Cancel";
        this.headerTitle = "GrantAccess";
        this.bodyText = "GrantAccessWarningInfo";
        this.label = "DaysToGrantAccess";
    }

    ngOnInit(): void {
        this.modalData = getModalData(this.store.getState());
        this.daysToRevoke = 1;
        this.today = new Date();
        this.dateToRevoke = new Date();
        this.setDateToRevoke(1);
    }

    setDateToRevoke(days: number): void {
        this.dateToRevoke = new Date();
        this.dateToRevoke.setDate(this.today.getDate() + days);
        this.dateToRevokeText = this.dateToRevoke.toLocaleDateString();
        this.saveDisabled = false;
    }

    validateForSubmit(value: number ): void {
        if (value < 1 || value > GrantRequestAccessComponent.MAX_DAYS_TO_REVOKE) {
            this.saveDisabled = true;
        } else {
            this.setDateToRevoke(value);
        }
    }

    onSave(): void {
        this.saveLoading = true;
        const grantAccess: IGrantAccessSubmit = {
            requestQueueId: this.modalData.requestQueueId,
            grantAccessExpiryDate: this.dateToRevoke.toISOString(),
        };
        this.RequestQueueService.grantDsAccess(grantAccess).then((response: IProtocolResponse<void>) => {
            if (response.result) {
                this.modalData.grantAccessCallback();
                this.saveLoading = false;
                this.closeModal();
            }
        });
    }

    closeModal(): void {
        this.ModalService.closeModal();
        this.ModalService.clearModalData();
    }
}
