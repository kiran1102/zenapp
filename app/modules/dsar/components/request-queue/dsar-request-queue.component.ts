// Angular
import {
    Component,
    Inject,
    OnInit,
    OnDestroy,
    Output,
    EventEmitter,
    Input,
    SimpleChanges,
} from "@angular/core";
import { StateService } from "@uirouter/core";

// Libraries
import {
    cloneDeep,
    assign,
    map,
    remove,
    findIndex,
} from "lodash";

// RxJs
import { Subject } from "rxjs";
import {
    takeUntil,
    take,
    filter as filterResponse,
    concatMap,
} from "rxjs/operators";

// Interfaces
import {
    IRequestQueueDetails,
    IRequestQueueParams,
    IGrantAccessModal,
    ITableColumns,
    IColumnMap,
    IExport,
    IRequestQueueRejectModalResolve,
    IRequestQueueAssignModalResolve,
    IRequestQueueExtendModalResolve,
    IRevokeAccessSubmit,
    IFilterItem,
    IRequestResolutionFormatted,
} from "dsarModule/interfaces/request-queue.interface";
import { IDeleteConfirmationModalResolve } from "interfaces/delete-confirmation-modal-resolve.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";
import {
    IUpdateDetailsModalResolve,
    IUpdateDetailsPutData,
} from "dsarModule/interfaces/dsar-update-details-modal.interface";
import { IAction } from "interfaces/redux.interface";
import {
    IFilterGetData,
} from "dsarModule/interfaces/dsar-sidebar-drawer.interface";
import { ITaskConfirmationModalResolve } from "interfaces/task-confirmation-modal-resolve.interface";
import {
    IRequestQueueClose,
    IRequestQueueReject,
} from "dsarModule/interfaces/request-queue-actions.interface";
import { IPageableOf } from "interfaces/pagination.interface";
import { IColumnSelectorModalResolve } from "interfaces/column-selector-modal.interface";
import {
    IListState,
    IListItem,
} from "@onetrust/vitreus";
import { IAttachmentFileResponse } from "interfaces/attachment-file.interface";

// Constants and Enums
import { RequestQueueStatus } from "dsarModule/enums/request-queue-status.enum";
import {
    RequestQueueActions,
    RequestQueueTableView,
 } from "dsarModule/constants/dsar-request-queue.constants";

// Services
import { ModalService } from "sharedServices/modal.service";
import { TimeStamp } from "oneServices/time-stamp.service";
import { Permissions } from "modules/shared/services/helper/permissions.service";
import { RequestQueueService } from "dsarservice/request-queue.service";
import { McmModalService } from "modules/mcm/mcm-modal.service";
import {
    OtModalService,
    ModalSize,
} from "@onetrust/vitreus";

// Components
import { RequestQueueAssignModal } from "modules/dsar/components/request-queue/assign-modal/request-queue-assign-modal.component";
import { RequestQueueChangeWorkflow } from "dsarModule/components/request-queue/change-workflow/request-queue-change-workflow.component";
import { RequestQueueExtendModal } from "dsarModule/components/request-queue/extend-modal/request-queue-extend-modal.component";
import { RequestQueueRejectModal } from "dsarModule/components/request-queue/reject-modal/request-queue-reject-modal.component";
import { RequestQueueUpdateDetailsModal } from "dsarModule/components/request-queue/update-details-modal/request-queue-update-details-modal.component";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";
import { RequestQueueCacheService } from "dsarservice/request-queue-cache.service";

@Component({
    selector: "dsar-request-queue",
    templateUrl: "./dsar-request-queue.component.html",
    host: {
        class: "flex-full-height",
    },
})

export class DSARRequestQueueComponent implements OnInit, OnDestroy {
    @Input() reloadGrid: boolean;
    @Output() filtersAdded = new EventEmitter<boolean | IFilterItem>();
    @Output() searchRequests = new EventEmitter<string>();

    readonly RequestQueueTableView = RequestQueueTableView;
    readonly loadingText = {
        loading: this.translatePipe.transform("Loading"),
        sorting: this.translatePipe.transform("Sorting"),
        filtering: this.translatePipe.transform("Filtering"),
    };
    readonly permissionsMap = {
        DSARRequestQueueDetailPage: this.permissions.canShow("DSARRequestQueueDetailPage"),
        DSARRequestQueueExport: this.permissions.canShow("DSARRequestQueueExport"),
    };
    readonly searchPlaceholder = this.translatePipe.transform("Search");
    readonly searchCharLimit = { charLimit: 6 };

    exportInProgress = false;
    isDrawerOpen = false;
    loadingPrefix = this.loadingText.loading;
    params: IRequestQueueParams;
    ready = false;
    requestsList: IPageableOf<IRequestQueueDetails>;
    requestsListToFilter: IPageableOf<IRequestQueueDetails>;
    searchString: string;
    showingFilteredResults: boolean | IFilterItem;
    set _showingFilteredResults(value: boolean | IFilterItem) {
        this.showingFilteredResults = value;
        this.filtersAdded.emit(value);
    }
    get _showingFilteredResults() {
        return this.showingFilteredResults;
    }
    set _searchString(value) {
        this.searchString = value;
        this.searchRequests.emit(value);
    }
    get _searchString() {
        return this.searchString;
    }
    sortKey: string | number = "createdDate";
    sortOrder = "desc";
    requestQueueActions = RequestQueueActions;
    columns: ITableColumns[] = this.requestQueueCacheService.getColumns();
    activeColumnList: IListItem[];
    disabledColumnList: IListItem[];
    requestQueueDetail: IRequestQueueDetails;
    destroy$: Subject<void> = new Subject();
    currentFilters: IFilterGetData[];

    private readonly defaultParams: IRequestQueueParams = {
        page: 0,
        size: 20,
        sort: "createdDate,desc",
    };
    private readonly emptyPageResponse: IPageableOf<IRequestQueueDetails> = {
        content: [],
        first: true,
        last: true,
        number: 0,
        numberOfElements: 0,
        size: 0,
        totalElements: 0,
        totalPages: 1,
    };

    private rowIndex: number;
    private tableView: string;
    private deleteQueueModalData: IDeleteConfirmationModalResolve = {
        promiseToResolve: null,
        modalTitle: this.translatePipe.transform("DeleteRequest"),
        confirmationText: this.translatePipe.transform("DeleteRequestQueueConfirmation"),
        submitButtonText: this.translatePipe.transform("Delete"),
        cancelButtonText: this.translatePipe.transform("Cancel"),
    };
    private revokeAccessModalData: IDeleteConfirmationModalResolve = {
        promiseToResolve: null,
        modalTitle: this.translatePipe.transform("RevokePortalAccess"),
        confirmationText: this.translatePipe.transform("RevokeAccessPortalWarningInfo"),
        submitButtonText: this.translatePipe.transform("Revoke"),
        cancelButtonText: this.translatePipe.transform("Cancel"),
    };
    private closeQueueModalData: IDeleteConfirmationModalResolve = {
        promiseToResolve: null,
        modalTitle: this.translatePipe.transform("CloseRequest"),
        confirmationText: this.translatePipe.transform("CloseRequestQueueConfirmation"),
        submitButtonText: this.translatePipe.transform("Confirm"),
        cancelButtonText: this.translatePipe.transform("Cancel"),
    };
    private rejectQueueModalData: IRequestQueueRejectModalResolve = {
        promiseToResolve: null,
        modalTitle: this.translatePipe.transform("RejectRequest"),
        confirmationText: this.translatePipe.transform("DSARRejectModalText"),
        submitButtonText: this.translatePipe.transform("Send"),
        cancelButtonText: this.translatePipe.transform("Cancel"),
        resolutionOptions: null,
        requestQueueDetails: null,
    };
    private assignQueueModalData: IRequestQueueAssignModalResolve = {
        promiseToResolve: null,
        modalTitle: "AssignRequest",
        confirmationText: null,
        submitButtonText: this.translatePipe.transform("Assign"),
        cancelButtonText: this.translatePipe.transform("Cancel"),
        requestQueueDetails: null,
    };

    constructor(
        private stateService: StateService,
        private modalService: ModalService,
        private permissions: Permissions,
        private requestQueueService: RequestQueueService,
        private timeStamp: TimeStamp,
        private translatePipe: TranslatePipe,
        private otModalService: OtModalService,
        private mcmModalService: McmModalService,
        private requestQueueCacheService: RequestQueueCacheService,
    ) { }

    ngOnInit(): void {
        this.requestQueueService.eventsObservable
            .pipe(takeUntil(this.destroy$))
            .subscribe(({type, payload}) => {
                this.handleAction(type, payload);
            });
        this.initializeGrid();
    }

    initializeGrid() {
        this._searchString = this.requestQueueCacheService.getSearchString() || "";
        this.tableView = this._searchString.length ? RequestQueueTableView.Search : RequestQueueTableView.Filter;
        this.init();
    }

    ngOnChanges(changes: SimpleChanges) {
        if (changes.reloadGrid && !changes.reloadGrid.firstChange) {
            this.initializeGrid();
        }
    }

    ngOnDestroy() {
        this.destroy$.next();
        this.destroy$.complete();
    }

    handleTableAction(action: IAction): void {
        this.handleAction(action.type, action.payload);
    }

    activateColumn(column: ITableColumns, active: boolean): ITableColumns {
        return {
            ...column,
            isActive: active,
        };
    }

    getActionColumn(columns: ITableColumns[]) {
        return columns.find((column: ITableColumns) => column.columnKey === "Action");
    }

    updateSelectedColumns(updatedList: IListState) {
        this.activeColumnList = updatedList.right.map(
            (column: ITableColumns) => this.activateColumn(column, true));
        this.disabledColumnList = updatedList.left.map(
            (column: ITableColumns) => this.activateColumn(column, false));
        this.columns = [...this.activeColumnList, this.getActionColumn(this.columns)] as ITableColumns[];
        this.columns = this.columns.map(this.translateColumn);
        const updatedColumns = [].concat(this.activeColumnList, this.disabledColumnList, this.getActionColumn(this.columns));
        this.requestQueueCacheService.updateColumns(updatedColumns as ITableColumns[]);
    }

    translateColumn = (column: ITableColumns) => {
        return {
            ...column,
            columnName : this.translatePipe.transform(column.columnKey),
        };
    }

    updateTableColumns(): void {
        let columns = this.requestQueueCacheService.getColumns();
        columns = columns.filter((column: ITableColumns) => column.columnKey !== "Action").map(this.translateColumn);
        this.activeColumnList = columns.filter((column: ITableColumns) => column.isActive);
        this.disabledColumnList = columns.filter((column: ITableColumns) => !column.isActive);
        this.columns = [...this.activeColumnList, this.getActionColumn(this.columns)] as ITableColumns[];
    }

    search(search: string): void {
        this._searchString = search;
        this.requestQueueCacheService.updateSearchString(this._searchString);
        this.requestQueueCacheService.updateFilters([]);
        this.loadingPrefix = this.loadingText.filtering;
        this.isDrawerOpen = false;
        this.currentFilters = [];
        this.tableView = this._searchString.length ? RequestQueueTableView.Search : RequestQueueTableView.Filter;
        this._showingFilteredResults = false;
        this.storeParams({ page: 0 });
        this.init();
    }

    applyFilters() {
        this._searchString = "";
        this.requestQueueCacheService.updateSearchString("");
        this.toggleFiltersDrawer();
        this.storeParams({ page: 0 });
        this.tableView = RequestQueueTableView.Filter;
        this.init();
    }

    toggleFiltersDrawer = (): void => {
        this.isDrawerOpen = !this.isDrawerOpen;
    }

    handleAction(type: string, payload: any): void {
        this.getRowByRequestQueueRefId(payload);
        switch (type) {
            case RequestQueueActions.SEARCH:
                this.search(payload);
                break;
            case RequestQueueActions.CHANGE_PAGE:
                this.loadingPrefix = this.loadingText.loading;
                this.isDrawerOpen = false;
                this.storeParams({ page: payload });
                this.init();
                break;
            case RequestQueueActions.SORT_QUEUE:
                this.loadingPrefix = this.loadingText.sorting;
                this.sortKey = payload.sortKey;
                this.sortOrder = payload.sortOrder;
                this.storeParams({ sort: `${this.sortKey},${this.sortOrder}` });
                this.init();
                break;
            case RequestQueueActions.UPDATE_SINGLE_ROW:
                this.requestsList.content = map(this.requestsList.content, (rowData: any): any => {
                    return (rowData.requestQueueRefId === payload.data.requestQueueRefId) ? payload.data : rowData;
                });
                this.requestsListToFilter.content[payload.index] = assign(this.requestsListToFilter.content[payload.index], payload.data);
                this.requestsListToFilter.content = cloneDeep(this.requestsListToFilter.content);
                break;
            case RequestQueueActions.EXTEND_REQUEST:
                this.otModalService.create(
                        RequestQueueExtendModal,
                        {
                            isComponent: true,
                            size: ModalSize.MEDIUM,
                        },
                        {
                            comment: payload.data ? payload.data.comment : "",
                            requestQueueId: this.requestQueueDetail.requestQueueId,
                        },
                    )
                    .pipe(
                        take(1),
                        filterResponse((response: boolean | null ) => response),
                    )
                    .subscribe(() => {
                        this.init();
                    });
                break;
            case RequestQueueActions.REJECT_REQUEST:
                this.rejectQueueModalData.requestQueueDetails = this.requestQueueDetail;
                this.requestQueueService.getRequestRejectResolutions().pipe(
                    take(1),
                    concatMap((value: IRequestResolutionFormatted[]) => {
                        this.rejectQueueModalData.resolutionOptions = value;
                        return this.otModalService.create(
                            RequestQueueRejectModal,
                            {
                                isComponent: true,
                                size: ModalSize.SMALL,
                            },
                            this.rejectQueueModalData,
                        );
                    }),
                    ).subscribe((modalReturnValue: boolean) => {
                        if (modalReturnValue) {
                            this.init();
                        }
                    });
                break;
            case RequestQueueActions.ASSIGN_REQUEST:
                const assignQueueModalData: IRequestQueueAssignModalResolve = this.assignQueueModalData;
                this.otModalService.create(
                        RequestQueueAssignModal,
                        {
                            isComponent: true,
                        },
                        {
                            ...assignQueueModalData,
                            requestQueueDetails: this.requestQueueDetail,
                        },
                    )
                    .pipe(takeUntil(this.destroy$))
                    .subscribe((response: boolean) => {
                        if (response) {
                            this.init();
                        }
                    });
                break;
            case RequestQueueActions.DELETE_REQUEST:
                this.deleteQueueModalData.promiseToResolve = (): ng.IPromise<boolean> => this.requestQueueService.deleteRequestQueue(this.requestQueueDetail.requestQueueId)
                    .then((res: IProtocolResponse<void>) => {
                        if (res.result) {
                            remove(this.requestsListToFilter.content, (row: IRequestQueueDetails): boolean => row.requestQueueRefId === payload);
                            if (!this.requestsListToFilter.content.length && (this.requestsListToFilter.number > 1)) {
                                this.storeParams({ page: this.requestsListToFilter.number - 1 });
                            }
                            this.init();
                            this.requestsListToFilter.totalElements--;
                        }
                        return !!res;
                    });
                this.modalService.setModalData(this.deleteQueueModalData);
                this.modalService.openModal("otDeleteConfirmationModal");
                break;
            case RequestQueueActions.REVOKE_ACCESS:
                const revokeAccess: IRevokeAccessSubmit = {
                  requestQueueId: this.requestQueueDetail.requestQueueId,
                };
                this.revokeAccessModalData.promiseToResolve = (): ng.IPromise<boolean> => this.requestQueueService.revokeDsAccess(revokeAccess)
                    .then((response: IProtocolResponse<void>): boolean => {
                        this.requestQueueDetail.dsAccessRevoked = true;
                        this.init();
                        return response.result;
                    });
                this.modalService.setModalData(this.revokeAccessModalData);
                this.modalService.openModal("otDeleteConfirmationModal");
                break;
            case RequestQueueActions.CLOSE_REQUEST:
                const requestClose: IRequestQueueClose = {
                    requestQueueId: this.requestQueueDetail.requestQueueId,
                    currentStatus: this.requestQueueDetail.status,
                    newStatus: RequestQueueStatus.Close,
                };
                this.closeQueueModalData.promiseToResolve = (): ng.IPromise<any> => this.requestQueueService.closeRequestQueue(requestClose)
                    .then((response: IProtocolResponse<void>) => {
                        if (response.result) {
                            this.init();
                        }
                        return response;
                    });
                this.modalService.setModalData(this.closeQueueModalData);
                this.modalService.openModal("otDeleteConfirmationModal");
                break;
            case RequestQueueActions.GRANT_ACCESS:
                const modalData: IGrantAccessModal = {
                    requestQueueId: this.requestQueueDetail.requestQueueId,
                    grantAccessCallback: (): void => {
                        this.requestQueueDetail.dsAccessRevoked = false;
                        this.init();
                    },
                };
                this.modalService.setModalData(modalData);
                this.modalService.openModal("grantRequestAccess");
                break;
            case RequestQueueActions.EXPORT:
                this.exportInProgress = true;
                const filterList = this.requestQueueCacheService.getFilters();
                const exportData: IExport = {
                    sort: `${this.sortKey},${this.sortOrder}`,
                    requestBody: {
                        filterCriteria: filterList && filterList.length ?
                            filterList :
                            [],
                        searchString: this._searchString,
                    },
                };
                this.requestQueueService.getExportFile(exportData).
                    then((response: IProtocolResponse<void>): void => {
                        this.exportInProgress = false;
                        if (response.result) {
                            const exportModalData: ITaskConfirmationModalResolve = {
                                modalTitle: this.translatePipe.transform("ReportDownload"),
                                confirmationText: this.translatePipe.transform("DSARReportIsGeneratedCheckTasksBar"),
                            };
                            this.modalService.setModalData(exportModalData);
                            this.modalService.openModal("oneNotificationModalComponent");
                        }
                    });
                break;
            case RequestQueueActions.UPDATE_REQUEST_ENTRIES:
                this.otModalService
                    .create(
                        RequestQueueUpdateDetailsModal,
                        {
                            isComponent: true,
                        },
                        {
                            title: `${this.translatePipe.transform("EditRequest")} - ${this.requestQueueDetail.requestQueueRefId}`,
                            requestQueueDetails: this.requestQueueDetail,
                        },
                    )
                    .pipe(take(1))
                    .subscribe((response: IProtocolResponse<IRequestQueueDetails> | null) => {
                        if (response && response.result) {
                            this.init();
                        }
                    });
                break;
            case RequestQueueActions.GO_TO_DETAIL_PAGE:
                if (payload.rowData.status === RequestQueueStatus.Locked) {
                    this.mcmModalService.openMcmModal("limitModalContent");
                } else if (this.permissionsMap.DSARRequestQueueDetailPage) {
                    this.stateService.go("zen.app.pia.module.dsar.views.queue-details", ({ id: payload.rowData.requestQueueId, previousParams: this.stateService.params }));
                }
                break;
            case RequestQueueActions.TOGGLE_DRAWER:
                this.toggleFiltersDrawer();
                break;
            case RequestQueueActions.USING_FILTER_DATA:
                this._showingFilteredResults = payload.value;
                break;
            case RequestQueueActions.APPLY_FILTER:
                this.params = this.defaultParams;
                this._searchString = "";
                this.requestQueueCacheService.updateSearchString("");
                this.tableView = RequestQueueTableView.Filter;
                this.storeParams({ sort: `${this.sortKey},${this.sortOrder}` });
                this.init();
                break;
            case RequestQueueActions.OPEN_COLUMN_SELECTOR:
                const columnSelectorModalData: IColumnSelectorModalResolve = {
                    modalTitle: "ColumnSelector",
                    leftList: this.disabledColumnList,
                    rightList: this.activeColumnList,
                    labelKey: "columnName",
                };
                columnSelectorModalData.promiseToResolve = (columnSelectionMap: IListState): Promise<boolean> => {
                    this.updateSelectedColumns(columnSelectionMap);
                    return Promise.resolve(true);
                };
                this.modalService.setModalData(columnSelectorModalData);
                this.modalService.openModal("columnSelectorModal");
                break;
            case RequestQueueActions.CHANGE_WORKFLOW:
                this.otModalService.create(
                        RequestQueueChangeWorkflow,
                        {
                            isComponent: true,
                        },
                        {
                            requestId: this.requestQueueDetail.requestQueueId,
                            title: "ChangeWorkflow",
                            workflowName: this.requestQueueDetail.workflowName,
                            workflowId: this.requestQueueDetail.workflowReferenceId,
                            authEnabled: this.requestQueueDetail.authEnabled,
                        },
                    )
                    .pipe(
                        take(1),
                        filterResponse((reload) => reload),
                        )
                    .subscribe(() => {
                        this.init();
                    });
                break;
            default:
                break;
        }
    }

    private init(): void {
        this.ready = false;
        const sessionParams = this.requestQueueCacheService.getPageParams();
        this.params = {
            page: parseInt(sessionParams && sessionParams.page as string, 10) || this.defaultParams.page,
            size: parseInt(sessionParams && sessionParams.size as string, 10) || this.defaultParams.size,
            sort: (sessionParams && sessionParams.sort) || this.defaultParams.sort,
        };
        this.updateTableColumns();
        const filterList = this.requestQueueCacheService.getFilters();
        this.currentFilters = (filterList && filterList.length) ? [...filterList] : [];
        if (this.tableView === RequestQueueTableView.Filter && filterList && filterList.length) {
            this.requestQueueService.applyFilter(
                filterList,
                cloneDeep(this.params),
            ).then((response: IProtocolResponse<IPageableOf<IRequestQueueDetails>>) => {
                this.handlePageOverrides(response, sessionParams);
                this._showingFilteredResults = {
                    filters: this.currentFilters.length,
                    items: this.requestsList ? this.requestsList.totalElements : 0,
                };
            });
        } else if (this.tableView === RequestQueueTableView.Search && this._searchString.length !== 0) {
            this.requestQueueService.getRequestsBySearch(cloneDeep(this.params), this._searchString).then((response: IProtocolResponse<IPageableOf<IRequestQueueDetails>>): void => {
                this.handlePageOverrides(response, sessionParams);
            });
        } else {
            this._showingFilteredResults = false;
            this.requestQueueService.getRequests(cloneDeep(this.params)).then((response: IProtocolResponse<IPageableOf<IRequestQueueDetails>>): void => {
                this.handlePageOverrides(response, sessionParams);
            });
        }
    }

    private handlePageOverrides(response: IProtocolResponse<IPageableOf<IRequestQueueDetails>>, sessionParams) {
        this.requestsList = response.result ? response.data : this.emptyPageResponse;
        if (!response.data && (this.params.page > 1)) {
            this.storeParams({ page: (this.params.page as number) - 1 });
            this.init();
        } else if (sessionParams && this.requestsList &&
                !this.requestsList.content.length && !sessionParams.page && !response.data.first) {
            this.storeParams({ page: 0 });
            this.init();
        } else {
            this.requestsListToFilter = cloneDeep(this.requestsList);
            this.ready = true;
        }
    }

    private storeParams(newParams = this.params): void {
        newParams = assign(this.params, newParams);
        this.requestQueueCacheService.updatePageParams(this.params);
    }

    private getRowByRequestQueueRefId(requestQueueRefId: string): IRequestQueueDetails {
        if (requestQueueRefId) {
            this.rowIndex = findIndex(this.requestsListToFilter.content, (row: IRequestQueueDetails): boolean => row.requestQueueRefId === requestQueueRefId);
            if (this.rowIndex < 0) return;
            this.requestQueueDetail = this.requestsListToFilter.content[this.rowIndex];
            return this.requestQueueDetail;
        }
    }

}
