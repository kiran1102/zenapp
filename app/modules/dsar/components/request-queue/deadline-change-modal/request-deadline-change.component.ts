// Angular
import {
    Component,
    Inject,
    OnInit,
} from "@angular/core";

// 3rd party
import { isNil } from "lodash";

// Redux
import { getModalData } from "oneRedux/reducers/modal.reducer";
import { StoreToken } from "tokens/redux-store.token";

// Interfaces
import { IDeadlineChangeModal } from "dsarModule/interfaces/request-queue.interface";
import { IStore } from "interfaces/redux.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";
import { IMyDateModel } from "mydatepicker";

// Services
import { ModalService } from "sharedServices/modal.service";
import { RequestQueueService } from "dsarservice/request-queue.service";
import { TimeStamp } from "oneServices/time-stamp.service";

@Component({
    selector: "request-deadline-change",
    templateUrl: "./request-deadline-change.component.html",
})
export class RequestDeadlineChangeComponent implements OnInit {
    saveLoading = false;
    saveDisabled = false;
    deadline: string;
    deadlineValue: Date;
    format = this.timeStamp.getDatePickerDateFormat();
    disableUntil = this.timeStamp.getDisableUntilDate();
    private modalData: IDeadlineChangeModal;

    constructor(
        @Inject(StoreToken) private store: IStore,
        private readonly modalService: ModalService,
        private readonly requestQueueService: RequestQueueService,
        private readonly timeStamp: TimeStamp,
    ) { }

    ngOnInit(): void {
        let overrideDateFormatting = false;
        this.modalData =  getModalData(this.store.getState());
        if (!isNil(this.timeStamp.currentDateFormatting)) {
            overrideDateFormatting = !this.timeStamp.currentDateFormatting.trim().length;
        }
        this.deadline = isNil(this.modalData.deadline)
                            ? this.modalData.deadline
                            : this.timeStamp.formatDateWithoutTime(this.modalData.deadline, overrideDateFormatting);
    }

    public dateSelected(value: IMyDateModel): void {
        if (value.jsdate) {
            this.deadline = value.formatted;
            this.deadlineValue = value.jsdate;
            this.saveDisabled = false;
        } else {
            this.saveDisabled = true;
        }
    }

    onSave(): void {
        this.saveLoading = true;
        const deadline: string | null = this.deadlineValue ? this.deadlineValue.toISOString() : null;
        this.requestQueueService.changeDeadline(this.modalData.requestQueueId, deadline).
            then((response: IProtocolResponse<void>) => {
                if (response.result) {
                    this.modalData.changeDeadlineCallback(deadline);
                    this.closeModal();
                }
                this.saveLoading = false;
            });
    }

    closeModal(): void {
        this.modalService.closeModal();
        this.modalService.clearModalData();
    }
}
