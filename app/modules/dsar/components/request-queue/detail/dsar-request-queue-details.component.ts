// Angular
import {
    Component,
    Inject,
    OnInit,
    ViewChild,
    TemplateRef,
} from "@angular/core";
import { StateService } from "@uirouter/core";

import {
    remove,
    isNull,
    map,
    cloneDeep,
} from "lodash";

// Rxjs
import { Subject } from "rxjs";
import { first } from "rxjs/operators";

// Redux
import { getCurrentUser } from "oneRedux/reducers/user.reducer";

// Services
import { RequestQueueDetailService } from "dsarservice/request-queue-detail-service";
import { RequestQueueHelper } from "dsarservice/request-queue-helper.service";
import { RequestQueueService } from "dsarservice/request-queue.service";
import { ModalService } from "sharedServices/modal.service";
import { RequestQueueDropdownService } from "dsarservice/request-queue-dropdown.service";
import { OtModalService } from "@onetrust/vitreus";
import { Permissions } from "modules/shared/services/helper/permissions.service";

// Constants and Enums
import { RequestQueueActions } from "dsarModule/constants/dsar-request-queue.constants";
import { ConfirmModalType } from "@onetrust/vitreus";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

// Interfaces
import { IDeleteConfirmationModalResolve } from "interfaces/delete-confirmation-modal-resolve.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";
import {
    IRequestQueueDropdownAction,
    IRequestQueueDetails,
    IRequestQueueBreadcrumbItem,
} from "modules/dsar/interfaces/request-queue.interface";
import { IResponseTemplate } from "modules/dsar/interfaces/response-templates.interface";
import { IStringMap } from "interfaces/generic.interface";
import { IPageableOf } from "interfaces/pagination.interface";
import { ISubTask } from "modules/dsar/components/sub-tasks/shared/interfaces/sub-tasks.interface";
import { IStore } from "interfaces/redux.interface";
import { StoreToken } from "tokens/redux-store.token";
import { IUser } from "interfaces/user.interface";

@Component({
    selector: "dsar-request-queue-detail",
    templateUrl: "./dsar-request-queue-details.component.html",
})
export class DSARRequestQueueDetailComponent implements OnInit {

    @ViewChild("SubtaskClosed") contentRef: TemplateRef<any>;

    requestQueueId: string;
    previousPageParms;
    requestQueueDetails: IRequestQueueDetails;
    permissions: IStringMap<boolean>;
    taskMode = false;
    responseTemplates: IResponseTemplate[];
    actions: IRequestQueueDropdownAction[];
    ready = false;
    confirmModalType = ConfirmModalType;
    modalCloseEvent: Subject<void>;
    subtaskView = false;
    breadCrumbList: IRequestQueueBreadcrumbItem[] = [{
        link: "zen.app.pia.module.dsar.views.queue",
        label: "Requests",
        autoId: "DSARNavigateQueueListPageLink",
    }];

    private deleteQueueModalData: IDeleteConfirmationModalResolve = {
        promiseToResolve: null,
        modalTitle: this.translatePipe.transform("DeleteRequest"),
        confirmationText: this.translatePipe.transform("DeleteRequestQueueConfirmation"),
        submitButtonText: this.translatePipe.transform("Delete"),
        cancelButtonText: this.translatePipe.transform("Cancel"),
    };

    constructor(
        private stateService: StateService,
        private requestQueueDetailService: RequestQueueDetailService,
        private requestQueueHelper: RequestQueueHelper,
        private translatePipe: TranslatePipe,
        private requestQueueService: RequestQueueService,
        private modalService: ModalService,
        private otModalService: OtModalService,
        private requestQueueDropdownService: RequestQueueDropdownService,
        private permissionsService: Permissions,
        @Inject(StoreToken) private store: IStore,
    ) { }

    ngOnInit() {
        const currentUser: IUser = getCurrentUser(this.store.getState());
        this.requestQueueId = this.stateService.params.id || "";
        this.subtaskView = this.stateService.params.subtaskView;
        this.previousPageParms = this.stateService.params.previousParams;
        if (this.requestQueueId) {
            this.updateBreadcrumbList();
            this.requestQueueDetailService.initializeRequestDetail(this.requestQueueId).then((response: boolean) => {
                if (response) {
                    this.requestQueueDetailService.getObservable$.subscribe(() => {
                        if (this.requestQueueDetailService.requestQueueDetails) {
                            this.requestQueueDetails = this.requestQueueDetailService.requestQueueDetails;
                            this.permissions = this.requestQueueDetailService.permissionsMap;
                            this.responseTemplates = this.requestQueueDetailService.responseTemplates;
                            this.actions = this.getActions(this.requestQueueDetails);
                            this.taskMode = this.requestQueueDetails.taskMode;
                            this.requestQueueHelper.updatePermissions();
                        }
                    });
                    this.requestQueueDetailService.getObservable$
                        .pipe(first())
                        .subscribe(() => {
                            if (this.requestQueueDetailService.requestQueueDetails &&
                                currentUser.Id !== this.requestQueueDetailService.requestQueueDetails.reviewerId &&
                                !this.permissionsService.canShow("DSARViewAllRequests")) {
                                this.requestQueueService.getAllSubTasksPaginated(
                                    {
                                        page: 0,
                                        size: 10,
                                    },
                                    this.requestQueueDetailService.requestQueueDetails.requestQueueId,
                                    this.requestQueueDetailService.workflowList,
                                )
                                .then(
                                    (res: IProtocolResponse<IPageableOf<ISubTask>>): void => {
                                        if (res.result && !res.data.totalElements) {
                                            this.showSubtaskCloseModal();
                                        }
                                    },
                                );
                                }
                            });
                    }
                this.ready = true;
            });
        }
    }

    updateBreadcrumbList() {
        if (this.subtaskView) {
            this.breadCrumbList = [{
                link: "zen.app.pia.module.dsar.views.queue",
                label: "SubTasks",
                autoId: "DSARNavigateSubtasksQueueListPageLink",
            }];
        }
    }

    showSubtaskCloseModal() {
        this.modalCloseEvent = this.otModalService.create(this.contentRef, {});
    }

    closeSubtaskModal() {
        this.modalCloseEvent.next();
    }

    handleAction(type: string, payload: any): void {
        switch (type) {
            case RequestQueueActions.BACK_TO_QUEUE:
                if (this.permissions.AccessRequestQueueUI) {
                    this.stateService.go("zen.app.pia.module.dsar.views.queue", this.previousPageParms);
                }
                break;
            case RequestQueueActions.DELETE_REQUEST:
                this.deleteQueueModalData.promiseToResolve = (): ng.IPromise<any> =>
                    this.requestQueueService.deleteRequestQueue(this.requestQueueId)
                    .then((res: IProtocolResponse<void>) => {
                        if (res.result) {
                            this.stateService.go("zen.app.pia.module.dsar.views.queue");
                        }
                        return res;
                    });
                this.modalService.setModalData(this.deleteQueueModalData);
                this.modalService.openModal("otDeleteConfirmationModal");
                break;
            default:
                this.requestQueueDetailService.handleAction(type, payload);
                break;
        }
    }

    handleActionClicks(actionItem) {
       actionItem.action(actionItem);
    }

    onRouteClick(route: { path: string, params: any }) {
        if (this.permissions.AccessRequestQueueUI) {
            this.stateService.go(route.path, route.params);
        }
    }

    getActions(rowData: IRequestQueueDetails): IRequestQueueDropdownAction[] {
        let actionList: IRequestQueueDropdownAction[] = cloneDeep(this.requestQueueDropdownService.getActions(rowData));
        actionList = remove(actionList, (action: IRequestQueueDropdownAction): boolean => {
            switch (action.id) {
                case "requestQueueAssign":
                    return false;
                case "requestQueueUpdateRequestEntries":
                    return rowData.currentStageInfo.allowRequestQueueUpdate;
                case "requestQueueExtend":
                    return !isNull(rowData.deadline);
                case "requestQueueRegenerate":
                    return rowData.authEnabled && rowData.passwordReGenerate;
                default:
                    return true;
            }
        });
        if (!actionList.length) {
            return [
                {
                    textKey: this.translatePipe.transform("NoActionsAvailable"),
                    action: () => { },
                },
            ];
        }
        return map(actionList, (actionItem: IRequestQueueDropdownAction): IRequestQueueDropdownAction => {
            actionItem.textKey = this.translatePipe.transform(actionItem.textKey);
            switch (actionItem.id) {
                case "requestQueueAssign":
                    actionItem.action = (action: IRequestQueueDropdownAction): void => this.handleAction("ASSIGN_REQUEST", action.rowRefId);
                    break;
                case "requestQueueExtend":
                    actionItem.action = (action: IRequestQueueDropdownAction): void => this.handleAction("EXTEND_REQUEST", action.rowRefId);
                    break;
                case "requestQueueReject":
                    actionItem.action = (action: IRequestQueueDropdownAction): void => this.handleAction("REJECT_REQUEST", action.rowRefId);
                    break;
                case "requestQueueSuspend":
                    actionItem.action = (action: IRequestQueueDropdownAction): void => this.handleAction("SUSPEND_REQUEST", action.rowRefId);
                    break;
                case "requestQueueDelete":
                    actionItem.action = (action: IRequestQueueDropdownAction): void => this.handleAction("DELETE_REQUEST", action.rowRefId);
                    break;
                case "requestQueueRevoke":
                    if (rowData.dsAccessRevoked) {
                        actionItem.textKey = this.translatePipe.transform("GrantAccess");
                        actionItem.action = (action: IRequestQueueDropdownAction): void => this.handleAction("GRANT_ACCESS", action.rowRefId);
                    } else {
                        actionItem.textKey = this.translatePipe.transform("RevokeAccess");
                        actionItem.action = (action: IRequestQueueDropdownAction): void => this.handleAction("REVOKE_ACCESS", action.rowRefId);
                    }
                    break;
                case "requestQueueClose":
                        actionItem.action = (action: IRequestQueueDropdownAction): void => this.handleAction("CLOSE_REQUEST", action.rowRefId);
                        break;
                case "requestQueueUpdateRequestEntries":
                    actionItem.action = (action: IRequestQueueDropdownAction): void => this.handleAction("UPDATE_REQUEST_ENTRIES", action.rowRefId);
                    break;
                case "requestQueueRegenerate":
                    actionItem.action = (action: IRequestQueueDropdownAction): void => this.handleAction("MFA_PASSWORD", action.rowRefId);
                    break;
                case "requestQueueChangeWorkflow":
                    actionItem.action = (action: IRequestQueueDropdownAction): void => this.handleAction("CHANGE_WORKFLOW", action.rowRefId);
                    break;
                default:
                    actionItem.action = (): void | undefined => {
                        return;
                    };
            }
            return actionItem;
        });
    }
}
