// 3rd party
import {
    Component,
    OnInit,
} from "@angular/core";

// Services
import { RequestQueueDetailService } from "dsarservice/request-queue-detail-service";

// Pipes
import { IKeyValue } from "interfaces/generic.interface";

@Component({
    selector: "dsar-request-queue-detail-panel",
    templateUrl: "./dsar-request-queue-detail-panel.component.html",
})
export class RequestQueueDetailPanelComponent implements OnInit{
    panelTitle: string;
    details: Array<IKeyValue<string | boolean | number>>;
    minRows = 5;
    isToggled = false;
    defaultTable: {
        details: Array<IKeyValue<string | boolean | number>>;
    };
    overflowTable: {
        details: Array<IKeyValue<string | boolean | number>>;
    };

    constructor(
        readonly requestQueueDetailService: RequestQueueDetailService,
    ) {}

    ngOnInit() {
        this.requestQueueDetailService.getObservable$.subscribe(() => {
            this.panelTitle = this.requestQueueDetailService.requestQueueDetails.name;
            this.details = this.requestQueueDetailService.headerRequestDetails;
            this.defaultTable = { details: this.details.slice(0, this.minRows) };
            this.overflowTable = { details: this.details.slice(this.minRows, this.details.length) };
        });
    }

}
