// Angular
import {
    Component,
    Inject,
    OnInit,
} from "@angular/core";

// Redux
import { getModalData } from "oneRedux/reducers/modal.reducer";
import { StoreToken } from "tokens/redux-store.token";

// Enums
import { OrgUserTraversal } from "enums/org-user-dropdown.enum";

// Constants
import { EmptyGuid } from "constants/empty-guid.constant";

// Interfaces
import { IRequestChangeOrgModal } from "dsarModule/interfaces/request-queue.interface";
import { IStore } from "interfaces/redux.interface";
import { IOtOrgUserOutput } from "sharedModules/components/ot-org-user/ot-org-user.interface";

// services
import { ModalService } from "sharedServices/modal.service";
import { IUser } from "interfaces/user.interface";

@Component({
    selector: "request-queue-change-org-modal",
    templateUrl: "./request-queue-change-org-modal.component.html",
})
export class RequestQueueChangeOrgModal implements OnInit {
    saveInProgress = false;
    canSubmit = true;
    reviewer: string;
    reviewerToSubmit: string;
    isAssignedToMe = false;
    currentUser: IUser;
    requestOrgId: string;
    modalData: IRequestChangeOrgModal;
    orgGroupId: string;
    orgUserTraversal = OrgUserTraversal;

    constructor(
        @Inject(StoreToken) private store: IStore,
        private modalService: ModalService,
    ) { }

    ngOnInit() {
        this.modalData = getModalData(this.store.getState());
        this.reviewerToSubmit = this.modalData.reviewerId === EmptyGuid ? null : this.modalData.reviewerId;
        this.orgGroupId = this.modalData.orgGroupId;
        this.canSubmit = false;
    }

    orgSelect(orgId: string): void {
        if (!orgId) {
            this.canSubmit = false;
        } else {
            this.canSubmit = true;
        }
        this.reviewerToSubmit = "";
        this.orgGroupId = orgId;
    }

    assigneeSelected(user: IOtOrgUserOutput): void {
        if (this.isAssignedToMe) return;
        this.reviewerToSubmit = user.selection ? user.selection.Id : "";
        this.canChangeOrg();
    }

    canChangeOrg(): void {
        if (this.orgGroupId) {
            this.canSubmit = true;
        }
    }

    handleSubmit(): void {
        this.saveInProgress = true;
        this.modalData.promiseToResolve(this.orgGroupId, this.reviewerToSubmit).then((response: boolean) => {
            if (response) {
                this.modalService.closeModal();
                this.modalService.clearModalData();
            } else {
                this.saveInProgress = false;
            }
        });
    }

    closeModal(): void {
        this.modalService.closeModal();
        this.modalService.clearModalData();
    }
}
