// Angular
import {
    Component,
    OnInit,
    Input,
} from "@angular/core";

// 3rd Party
import { each } from "lodash";

// Services
import { RequestQueueDetailService } from "dsarservice/request-queue-detail-service";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";
import { DsarRequestStatusColorPipe } from "dsarModule/pipes/dsar-request-status-color.pipe";

// Constants
import { RequestQueueActions } from "dsarModule/constants/dsar-request-queue.constants";

// Interfaces
import { ITabsNav } from "interfaces/one-tabs-nav.interface";
import { IRequestQueueDetails } from "modules/dsar/interfaces/request-queue.interface";
import { IResponseTemplate } from "modules/dsar/interfaces/response-templates.interface";
import { IRequestQueueComment } from "modules/dsar/interfaces/request-queue-comment.interface";

interface ITabEnums {
    Activity: string;
    Attachments: string;
    ConsentReceipts: string;
    SubTasks: string;
    History: string;
}

@Component({
    selector: "dsar-request-queue-detail-body",
    templateUrl: "./dsar-request-queue-detail-body.component.html",
})
export class DSARRequestQueueDetailBodyComponent implements OnInit {
    @Input() permissions;

    responseTemplates: IResponseTemplate[];
    requestQueueDetails: IRequestQueueDetails;
    currentTab: string;
    landOnInternalComments = false;
    tabOptions: ITabsNav[] = [];
    messagingReady = true;
    firstLoad = true;
    showActivityTab = false;

    tabEnums: ITabEnums = {
        Activity: "activity",
        Attachments: "attachments",
        ConsentReceipts: "consent-receipts",
        SubTasks: "subTasks",
        History: "history",
    };

    constructor(
        private readonly requestQueueDetailService: RequestQueueDetailService,
        private readonly translatePipe: TranslatePipe,
        private readonly dsarRequestStatusColorPipe: DsarRequestStatusColorPipe,
    ) { }

    ngOnInit() {
        this.showActivityTab =
            this.permissions.AccessInternalComments || this.permissions.AccessExternalComments
            || this.permissions.DSARExternalCommentCreate || this.permissions.DSARInternalCommentCreate;
        if (this.permissions.DSARSubTaskView) {
            this.tabOptions.push({ id: "subTasks", text: this.translatePipe.transform("SubTasks") });
        }
        if (this.showActivityTab) {
            this.tabOptions.push(
                { id: "activity", text: this.translatePipe.transform("Activity")},
            );
        }
        this.landOnInternalComments = this.permissions.AccessInternalComments;
        if (this.permissions.History) {
            this.tabOptions.push({ id: "history", text: this.translatePipe.transform("History")});
        }
        this.currentTab = this.showActivityTab ?
            this.tabEnums.Activity : this.permissions.DSARSubTaskView ?
            this.tabEnums.SubTasks : this.tabEnums.History;
        each(this.tabOptions, (options: ITabsNav): void => {
            options.action = (option: ITabsNav): void => {
                this.changeTab(option);
            };
        });

        this.requestQueueDetailService.getObservable$.subscribe(() => {
            this.requestQueueDetails = this.requestQueueDetailService.requestQueueDetails;
            this.responseTemplates = this.requestQueueDetailService.responseTemplates;
            if (this.requestQueueDetails.taskMode && this.firstLoad && this.permissions.DSARSubTaskView) {
                this.currentTab = this.tabEnums.SubTasks;
            }
            this.firstLoad = false;
            this.updateStatusColor(this.requestQueueDetails.comments);
        });
    }

    updateStatusColor(comments: IRequestQueueComment[]) {
        comments.forEach((comment: IRequestQueueComment): void => {
            comment.statusColor = this.dsarRequestStatusColorPipe.transform(comment.requestStatus);
            comment.statusText = comment.statusText.toUpperCase();
            comment.showDeleteIcon = false;
        });
    }

    changeTab(option: ITabsNav): void {
        this.currentTab = option.id;
        if (this.currentTab === this.tabEnums.Activity) {
            this.messagingReady = false;
            this.requestQueueDetailService.getRequestQueueDetails().then((res: boolean): void => {
                this.messagingReady = true;
                this.handleAction(RequestQueueActions.UPDATE_VIEW);
            });
        }
    }

    handleAction(event) {
        this.requestQueueDetailService.handleAction(event.type, event.payload);
    }
}
