// Angular
import {
    Component,
    OnInit,
} from "@angular/core";

// RxJs
import { Subject } from "rxjs";

// Services
import { RequestQueueService } from "dsarservice/request-queue.service";

// Constants
import { EmptyGuid } from "constants/empty-guid.constant";

// Enums
import { OrgUserDropdownTypes } from "enums/org-user-dropdown.enum";
import { OrgUserTraversal } from "enums/org-user-dropdown.enum";

// Interfaces
import { IRequestQueueAssign } from "modules/dsar/interfaces/request-queue-actions.interface";
import { IRequestQueueAssignModalResolve } from "dsarModule/interfaces/request-queue.interface";
import { IOtModalContent } from "@onetrust/vitreus";
import { IProtocolResponse } from "interfaces/protocol.interface";

@Component({
    selector: "request-queue-assign-modal",
    templateUrl: "./request-queue-assign-modal.component.html",
})
export class RequestQueueAssignModal implements OnInit, IOtModalContent {
    comment = "";
    saveInProgress = false;
    canSubmit = false;
    reviewer: string;
    reviewerToSubmit: string;
    orgUserTraversal = OrgUserTraversal;
    customModules = [["bold", "italic", "underline", "strike", "image", { list: "ordered" }, { list: "bullet" }],
    [{ align: "" }, { align: "right" }, { align: "center" }, { align: "justify" }, "link"]];
    requestOrgId: string;
    userDropdownType: number = OrgUserDropdownTypes.DSARRequestAssignee;
    cloneReviewerToSubmit: string;
    otModalCloseEvent: Subject<boolean>;
    otModalData: IRequestQueueAssignModalResolve;
    constructor(
        readonly requestQueueService: RequestQueueService,
    ) { }

    ngOnInit() {
        this.requestOrgId = this.otModalData.requestQueueDetails.orgGroupId;
        const reviewerId = this.otModalData.requestQueueDetails.reviewerId;
        this.reviewerToSubmit = reviewerId === EmptyGuid ? null : reviewerId;
        this.cloneReviewerToSubmit = this.reviewerToSubmit;
    }

    assigneeSelected(value: any, valid: boolean): void {
        if (valid && value) {
            this.reviewerToSubmit = value.Id || value;
            this.canSubmit = this.cloneReviewerToSubmit !== this.reviewerToSubmit;
        } else if (valid && !value) {
            this.reviewerToSubmit = "";
            this.canSubmit = Boolean(this.cloneReviewerToSubmit && !this.comment);
        } else  {
            this.canSubmit = false;
        }
    }

    handleMessageChange(message: string): void {
        if ( message && !message.includes("img") && !$(message).text().trim()) {
            this.comment = "";
        } else {
            this.comment = message;
        }
        // Disable button when empty assignee and comments entered
        if (!this.reviewerToSubmit) {
            this.canSubmit = !this.comment;
        } else {
            this.canSubmit = Boolean(this.comment);
        }
    }

    handleSubmit(): void {
        const newRequest: IRequestQueueAssign[] = [{
            requestQueueId: this.otModalData.requestQueueDetails.requestQueueId,
            reviewer: this.reviewerToSubmit,
            comment: this.comment,
            isInternalComment: true,
        }];
        this.saveInProgress = true;
        this.requestQueueService.assignRequest(newRequest)
            .then((response: IProtocolResponse<IRequestQueueAssign[]>) => {
                if (response.result) {
                    this.otModalCloseEvent.next(true);
                    this.otModalCloseEvent.complete();
                } else {
                    this.saveInProgress = false;
                }
            });
    }

    closeModal(): void {
        this.otModalCloseEvent.next(false);
        this.otModalCloseEvent.complete();
    }

}
