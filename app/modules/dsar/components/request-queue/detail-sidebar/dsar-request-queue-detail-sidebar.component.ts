// 3rd party
import {
    Component,
    OnInit,
    Inject,
} from "@angular/core";
import {
    find,
    map,
    isNil,
} from "lodash";

// Services
import { RequestQueueService } from "dsarservice/request-queue.service";
import { RequestQueueHistoryService } from "dsarservice/request-queue-history.service";
import { ModalService } from "sharedServices/modal.service";
import { TimeStamp } from "oneServices/time-stamp.service";
import Utilities from "Utilities";
import { RequestQueueDetailService } from "dsarservice/request-queue-detail-service";

// Interfaces
import { IStringMap, IKeyValue } from "interfaces/generic.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";
import { IStore } from "interfaces/redux.interface";
import {
    IGetWebformLanguageResponse,
} from "interfaces/dsar/dsar-edit-language.interface";
import {
    IRequestQueueDetails,
    IDeadlineChangeModal,
    IRequestChangeOrgModal,
} from "modules/dsar/interfaces/request-queue.interface";

// Constants
import {
    RequestQueueSidebar,
    CostCalc,
    RequestQueueActions,
} from "dsarModule/constants/dsar-request-queue.constants";
import { Regex } from "constants/regex.constant";

// Redux
import { getOrgById } from "oneRedux/reducers/org.reducer";
import { getUserById } from "oneRedux/reducers/user.reducer";
import { StoreToken } from "tokens/redux-store.token";

@Component({
    selector: "request-queue-detail-sidebar",
    templateUrl: "./request-queue-detail-sidebar.component.html",
})
export class RequestQueueSidebarDetails implements OnInit {
    disableEdit: boolean;
    currencyIso: string;
    hourlyCost: number;
    permissions: IStringMap<boolean>;
    sidebarData: Array<IKeyValue<string | boolean | number>>;
    requestQueueDetails: IRequestQueueDetails;
    requestQueueSidebar = RequestQueueSidebar;
    costCalc = CostCalc;
    requestQueueActions = RequestQueueActions;
    costEditModeEnabled: boolean;
    langEditModeEnabled: boolean;
    hoursWorked: number;
    orgGroupId: string;
    selectedLanguage: IGetWebformLanguageResponse;

    constructor(
        @Inject(StoreToken) private store: IStore,
        private readonly requestQueueService: RequestQueueService,
        private HistoryService: RequestQueueHistoryService,
        private readonly modalService: ModalService,
        private readonly timeStamp: TimeStamp,
        readonly requestQueueDetailService: RequestQueueDetailService,
    ) {}

    public ngOnInit() {
        this.requestQueueDetailService.getObservable$.subscribe(() => {
            this.requestQueueDetails = this.requestQueueDetailService.requestQueueDetails;
            this.sidebarData = this.requestQueueDetailService.sidebarRequestDetails;
            this.permissions = this.requestQueueDetailService.permissionsMap;
            this.disableEdit = this.requestQueueDetailService.disableEdit;
            this.currencyIso = this.requestQueueDetailService.currencyIso;
            this.hourlyCost = this.requestQueueDetailService.hourlyCost;
            this.selectedLanguage = this.requestQueueDetails.languageList.find((lang: IGetWebformLanguageResponse) => lang.Code === this.requestQueueDetails.language);
            this.hoursWorked = this.requestQueueDetails.hoursWorked;
            this.orgGroupId = this.requestQueueDetails.orgGroupId;
        });
    }

    validateHoursWorked(value: number): void {
        this.hoursWorked = +value;
        if (this.hoursWorked > 999999999.99) {
            this.hoursWorked = 999999999.99;
        } else if (!this.hoursWorked) {
            this.hoursWorked = 0;
        }
    }

    selectLanguage(lang: IGetWebformLanguageResponse) {
        this.selectedLanguage = lang;
    }

    handleAction(type: string, payload: any = {}): void {
        this.requestQueueDetailService.handleAction(type, payload);
    }

    updateHoursWorked(): void {
        if (this.hoursWorked >= 0) {
            const requestQueueId: string = this.requestQueueDetails.requestQueueId;
            this.requestQueueService.updateHoursWorked(requestQueueId, { hoursWorked: this.hoursWorked }).then((response: IProtocolResponse<void>): void => {
                this.HistoryService.updateHistory(requestQueueId);
                this.requestQueueDetails.hoursWorked = this.hoursWorked;
                this.sidebarData = map(this.sidebarData, (sideBarArrItem: IKeyValue<string | boolean | number>): IKeyValue<string | boolean | number> => {
                    if (sideBarArrItem.key === CostCalc.TotalCost) {
                        return {
                            key: CostCalc.TotalCost,
                            value: this.hoursWorked * this.hourlyCost,
                        };
                    } else if (sideBarArrItem.key === CostCalc.HoursWorked) {
                        return {
                            key: CostCalc.HoursWorked,
                            value: this.hoursWorked,
                        };
                    }
                    return sideBarArrItem;
                });
                this.costEditModeEnabled = false;
            });
        }
    }

    updateLanguage(): void {
        const requestQueueId: string = this.requestQueueDetails.requestQueueId;
        this.requestQueueService.updateLanguage(requestQueueId, this.selectedLanguage.Code).then((response: IProtocolResponse<void>): void => {
            if (this.permissions.DSARResponseTemplateLanguageSupport) {
                this.handleAction(RequestQueueActions.DS_LANGUAGE_CHANGE);
            }
            this.HistoryService.updateHistory(requestQueueId);
            this.requestQueueDetails.language = this.selectedLanguage.Code;
            this.handleAction(RequestQueueActions.UPDATE_REQUEST, {requestQueueDetails: this.requestQueueDetails});
            const preferredLanguage: IGetWebformLanguageResponse = find(this.requestQueueDetails.languageList, { Code: this.selectedLanguage.Code});
            this.sidebarData = map(this.sidebarData, (sideBarArrItem: IKeyValue<string | boolean | number>): IKeyValue<string | boolean | number> => {
                if (sideBarArrItem.key === RequestQueueSidebar.PreferredLanguage) {
                    return {
                        key: RequestQueueSidebar.PreferredLanguage,
                        value: preferredLanguage ? preferredLanguage.Name : "- - - -",
                    };
                }
                return sideBarArrItem;
            });
            this.langEditModeEnabled = false;
        });
    }

    updateDeadline = (): void => {
        const modalData: IDeadlineChangeModal = {
            requestQueueId: this.requestQueueDetails.requestQueueId,
            deadline: this.requestQueueDetails.deadline,
            changeDeadlineCallback: (days: string): void => {
                this.sidebarData = map(this.sidebarData, (sideBarArrItem: IKeyValue<string | boolean | number>): IKeyValue<string | boolean | number> => {
                    if (sideBarArrItem.key === RequestQueueSidebar.Deadline) {
                        let overrideDateFormatting: boolean;
                        if (!isNil(this.timeStamp.currentDateFormatting)) {
                            overrideDateFormatting = !this.timeStamp.currentDateFormatting.trim().length;
                        }
                        return {
                            key: RequestQueueSidebar.Deadline,
                            value: this.timeStamp.formatDate(days, overrideDateFormatting),
                        };
                    }
                    return sideBarArrItem;
                });
                this.requestQueueDetails.deadline = days;
                this.handleAction(RequestQueueActions.UPDATE_REQUEST, {requestQueueDetails: this.requestQueueDetails});
                this.HistoryService.updateHistory(this.requestQueueDetails.requestQueueId);
            },
        };
        this.modalService.setModalData(modalData);
        this.modalService.openModal("requestDeadlineChange");
    }

    updateOrg = (): void => {
        const modalData: IRequestChangeOrgModal = {
            requestQueueId: this.requestQueueDetails.requestQueueId,
            reviewerId: this.requestQueueDetails.reviewerId,
            orgGroupId: this.requestQueueDetails.orgGroupId,
            promiseToResolve: (orgId: string, approverId: string): ng.IPromise<boolean> =>
                this.requestQueueService.changeOrg({
                    orgGroupId: orgId,
                    reviewerId: approverId,
                    requestQueueId: this.requestQueueDetails.requestQueueId,
            }).then((response: IProtocolResponse<void>): boolean => {
                this.sidebarData = map(this.sidebarData, (sideBarArrItem: IKeyValue<string | boolean | number>): IKeyValue<string | boolean | number> => {
                    if (sideBarArrItem.key === RequestQueueSidebar.Approver) {
                        if (Utilities.matchRegex(approverId, Regex.EMAIL)) {
                            return {
                                key: RequestQueueSidebar.Approver,
                                value: approverId,
                            };
                        }
                        const user = getUserById(approverId)(this.store.getState());
                        return {
                            key: RequestQueueSidebar.Approver,
                            value: user ? user.FullName : "- - - -",
                        };
                    }
                    if (sideBarArrItem.key === RequestQueueSidebar.ManagingOrganization) {
                        return {
                            key: RequestQueueSidebar.ManagingOrganization,
                            value: getOrgById(orgId)(this.store.getState()).Name,
                        };
                    }
                    return sideBarArrItem;
                });
                this.requestQueueDetails.reviewerId = approverId;
                this.requestQueueDetails.orgGroupId = orgId;
                this.handleAction(RequestQueueActions.UPDATE_REQUEST, {requestQueueDetails: this.requestQueueDetails});
                this.HistoryService.updateHistory(this.requestQueueDetails.requestQueueId);
                return response.result;
            }),
        };
        this.modalService.setModalData(modalData);
        this.modalService.openModal("requestQueueChangeOrgModal");
    }
}
