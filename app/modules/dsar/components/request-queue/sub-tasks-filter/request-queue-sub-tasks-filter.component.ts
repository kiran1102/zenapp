// 3rd Party
import {
    Component,
    EventEmitter,
    Output,
    Input,
    Inject,
} from "@angular/core";

// Services
import { OrgGroupApiService } from "oneServices/api/org-group-api.service";
import { RequestQueueService } from "dsarservice/request-queue.service";
import { Permissions } from "sharedModules/services/helper/permissions.service";
import { RequestQueueFilterService } from "dsarservice/request-queue-filter.service";
import { RequestQueueCacheService } from "dsarservice/request-queue-cache.service";

// Enum + Constants
import { ExportTypes } from "enums/export-types.enum";
import { OrgUserTraversal } from "enums/org-user-dropdown.enum";
import {
    FilterOperators,
    FilterOperatorLabels,
    DSARFilterValueTypes,
    OperatorValues,
    FilterLabels,
} from "dsarModule/constants/dsar-filter.constants";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

// Rxjs
import {
    Subject,
    combineLatest,
} from "rxjs";
import { take } from "rxjs/operators";

// Redux
import {
    getOrgList,
    getCurrentOrgId,
} from "oneRedux/reducers/org.reducer";
import { StoreToken } from "tokens/redux-store.token";

// Interfaces
import {
    IFilterColumnOption,
    IFilterValueOption,
} from "interfaces/filter.interface";
import { IFilterV2, IFilterOutput } from "modules/filter/interfaces/filter.interface";
import { IUser } from "interfaces/user.interface";
import { ILabelValue } from "interfaces/generic.interface";
import { IStore } from "interfaces/redux.interface";
import { IOrgUserAdapted } from "interfaces/org-user.interface";
import { IOrganization } from "interfaces/org.interface";
import { IFilterField } from "dsarModule/interfaces/dsar-filters.interface";
import { IFilterGetData } from "modules/dsar/interfaces/dsar-sidebar-drawer.interface";
import { FilterValueTypes } from "modules/filter/enums/filter.enum";

@Component({
    selector: "request-queue-sub-tasks-filter",
    template: `
        <one-filter
            class="flex-full-height"
            [selectedFilters]="selectedFilters"
            [disableApply]="disableApply"
            [fields]="filterFields"
            [getFieldLabelValue]="getFieldLabelValue"
            [currentValues]="currentFilterValues"
            [getValueLabelValue]="getColumnOptionLabelValue"
            [getOperatorLabelValue]="getOperatorLabelValue"
            [currentType]="currentFilterValueType"
            [togglePopup$]="closeFilter$"
            [loadingFilter]="loadingFilter"
            [loadingSavedFilter]="loadingFilter"
            [currentOperators]="currentOperators"
            [disableAddFilter]="disableAddFilter"
            (removeFilter)="removeFilter($event)"
            (removeAllFilters)="removeAllFilters()"
            (addFilter)="addFilter($event)"
            (selectField)="setFilterValues($event)"
            (selectOperator)="setFilterValuesOnOperatorChange($event)"
            (applyFilters)="apply($event)"
            (cancel)="cancel.emit()"
            >
        </one-filter>
    `,
})
export class RequestQueueSubTasksFilterComponent {

    @Input() set filtersOpen(isOpen: boolean) {
        if (!isOpen) {
            this.closeFilter$.next(isOpen);
        } else {
            this.loadFilterOptions();
        }
    }
    @Input()
    set currentFilters(filters) {
        if (filters) {
            this.filterList = [...filters];
        } else {
            this.filterList = [];
        }
    }

    additionalSeedUsers: IFilterValueOption[] = [
            {
                value: this.translatePipe.transform("MyGroups"),
                key: "00000000-0000-0000-0000-999999999999",
            },
        ];

    @Output() applyFilters = new EventEmitter<void>();
    @Output() cancel = new EventEmitter<null>();

    exportTypes = ExportTypes;
    showingExportOptions = false;
    loadingFilter = false;
    currentFilterValues: IFilterValueOption[] = [];
    templatesList: IFilterValueOption[] = [];
    tagsList: IFilterValueOption[] = [];
    userList: IFilterValueOption[] = [];
    orgList: IFilterValueOption[] = [];
    closeFilter$ = new Subject<boolean>();
    destroy$ = new Subject();
    currentField: IFilterField;
    currentFilterValueType: number;
    selectedFilters: Array<IFilterV2<IFilterField, IFilterValueOption, IFilterValueOption>> = [];
    columnOptions: IFilterColumnOption[];
    currentOperators: IFilterValueOption[];
    filterFields: IFilterField[];
    filterList: IFilterGetData[];
    disableApply = true;
    disableAddFilter = false;
    isProjectOwner = !this.permissions.canShow("DSARViewAllRequests");
    operators = {
        [FilterOperators.MultiSelect]: [
            {
                key: FilterOperatorLabels.EqualTo,
                value: OperatorValues.EqualTo,
            },
            {
                key: FilterOperatorLabels.NotEqualTo,
                value: OperatorValues.NotEqualTo,
            },
        ],
        [FilterOperators.Prepopulated]: [
            {
                key: FilterOperatorLabels.EqualTo,
                value: OperatorValues.EqualTo,
            },
            {
                key: FilterOperatorLabels.NotEqualTo,
                value: OperatorValues.NotEqualTo,
            },
        ],
        [FilterOperators.Date]: [
            {
                key: FilterOperatorLabels.Is,
                value: OperatorValues.Is,
            },
            {
                key: FilterOperatorLabels.IsAfter,
                value: OperatorValues.IsAfter,
            },
            {
                key: FilterOperatorLabels.IsBefore,
                value: OperatorValues.IsBefore,
            },
            {
                key: FilterOperatorLabels.IsBetween,
                value: OperatorValues.IsBetween,
            },
        ],
        [FilterOperators.DateRange]: [
            {
                key: FilterOperatorLabels.Is,
                value: OperatorValues.Is,
            },
            {
                key: FilterOperatorLabels.IsAfter,
                value: OperatorValues.IsAfter,
            },
            {
                key: FilterOperatorLabels.IsBefore,
                value: OperatorValues.IsBefore,
            },
            {
                key: FilterOperatorLabels.IsBetween,
                value: OperatorValues.IsBetween,
            },
        ],
    };

    constructor(
        @Inject(StoreToken) private store: IStore,
        private translatePipe: TranslatePipe,
        private orgGroupApi: OrgGroupApiService,
        private requestQueueService: RequestQueueService,
        private requestQueueFilterService: RequestQueueFilterService,
        private requestQueueCacheService: RequestQueueCacheService,
        private permissions: Permissions,
    ) {}

    loadFilterOptions() {
        this.loadingFilter = true;
        this.disableApply = true;
        const orgList = getOrgList(getCurrentOrgId(this.store.getState()))(this.store.getState());
        this.orgList = orgList && orgList.length ? orgList.map((org: IOrganization): IFilterValueOption => {
            return { value: org.Name, key: org.Id, translationKey: null };
        }) : [];

        combineLatest(
            this.requestQueueService.getSubTaskFilterData(),
            this.orgGroupApi.getOrgUsersWithPermission(getCurrentOrgId(this.store.getState()), OrgUserTraversal.All),
        ).pipe(
            take(1),
        ).subscribe(([filterMeta, orgUsers]) => {
            if (filterMeta && orgUsers && orgList) {
                this.userList = orgUsers.data && orgUsers.data.length ? orgUsers.data.map((user: IOrgUserAdapted): IFilterValueOption => {
                    return { value: user.FullName, key: user.Id, translationKey: null };
                }) : [];
                this.filterFields = filterMeta.data.sort(
                    (a, b) => (a.name > b.name) ? 1 : ((b.name > a.name) ? -1 : 0));
                this.initializeFilterData();
            }
        });
    }

    removeFilter({ currentList, index }) {
        this.selectedFilters.splice(index, 1);
        this.selectedFilters = [...this.selectedFilters];
        this.disableApply = false;
        this.updateAddFilterButton();
    }

    removeAllFilters() {
        this.selectedFilters = [];
        this.disableApply = false;
        this.updateAddFilterButton();
    }

    addFilter(filters) {
        filters.currentList.push(filters.filter);
        const deserializeFilter = this.requestQueueFilterService.deserializeFilters(filters.currentList, this.filterFields);
        const serializeFilter = this.requestQueueFilterService.serializeFilters(deserializeFilter, this.filterFields);
        this.selectedFilters = [...serializeFilter];
        this.disableApply = false;
        this.updateAddFilterButton();
    }

    updateAddFilterButton() {
        this.disableAddFilter = this.selectedFilters.length === this.filterFields.length;
    }

    ngOnDestroy(): void {
        this.destroy$.next();
        this.destroy$.complete();
    }

    initializeFilterData() {
        if (this.userList) {
            this.filterFields = this.requestQueueFilterService.updateApproverOptions(this.userList, this.filterFields);
            const approverField = this.filterFields.find((field: IFilterField) => field.name === FilterLabels.Approver);
            if (approverField) {
                approverField.valueOptions = [...approverField.valueOptions, ...this.additionalSeedUsers];
            }
            this.updateFilterList();
            this.loadingFilter = false;
        } else {
            this.loadingFilter = false;
        }
    }

    updateFilterList() {
        if (this.filterList && this.filterList.length) {
            this.selectedFilters = this.requestQueueFilterService.serializeFilters(this.filterList, this.filterFields);
            this.updateAddFilterButton();
        } else {
            this.selectedFilters = [];
        }
    }

    apply(filters: IFilterOutput[]) {
        let savedFilters: IFilterGetData[] = [];
        if (filters && filters.length) {
            savedFilters = this.requestQueueFilterService.deserializeFilters(filters, this.filterFields);
        }
        this.requestQueueCacheService.updateFilters(savedFilters);
        this.applyFilters.emit();
    }

    getFieldLabelValue = (field: IFilterField): ILabelValue<string> => {
        return {
            label: field.translationKey ? this.translatePipe.transform(field.translationKey) : (field && field.name ? field.name : ""),
            value: field.name,
        };
    }

    setFilterValues(selectedField: ILabelValue<string>) {
        if (!selectedField) {
            this.currentOperators = [];
            this.currentFilterValues = [];
            return;
        }
        const filterIndex = this.filterFields.findIndex((field: IFilterField) => field.name === selectedField.value);
        this.currentField = this.filterFields[filterIndex];
        this.currentFilterValueType = DSARFilterValueTypes[this.currentField.type];
        this.currentFilterValues = this.currentField ? this.currentField.valueOptions : [];
        this.currentOperators = this.operators[this.currentField.type];
        if (this.isProjectOwner && this.currentField.name === FilterLabels.Approver) {
            this.currentOperators = this.currentOperators.filter(
                (operator: IFilterValueOption) => operator.key === FilterOperatorLabels.EqualTo);
        }
    }

    setFilterValuesOnOperatorChange(operator: ILabelValue<string | number>) {
        if (this.currentFilterValueType !== FilterValueTypes.Date &&
            this.currentFilterValueType !== FilterValueTypes.DateRange) return;
        if (operator.value === OperatorValues.IsBetween) {
            this.currentField.type = FilterOperators.DateRange;
            this.currentFilterValueType = FilterValueTypes.DateRange;
        } else {
            this.currentField.type = FilterOperators.Date;
            this.currentFilterValueType = FilterValueTypes.Date;
        }
    }

    getColumnOptionLabelValue = (option: IFilterValueOption): ILabelValue<string | number> => {
        return {
            label: option.value as string,
            value: option.key,
        };
    }

    getOperatorLabelValue = (operator: IFilterValueOption): ILabelValue<string | number> => {
        return {
            label: this.translatePipe.transform(operator.key as string),
            value: operator.value,
        };
    }

}
