// Angular
import {
    Component,
    OnInit,
} from "@angular/core";

// RxJs
import { Subject } from "rxjs";

// Interfaces
import { IProtocolResponse } from "interfaces/protocol.interface";
import { IMFAModalData } from "modules/dsar/interfaces/request-queue.interface";

// services
import { NotificationService } from "modules/shared/services/provider/notification.service";
import { RequestQueueService } from "dsarservice/request-queue.service";
import { IOtModalContent } from "@onetrust/vitreus";
import Utilities from "Utilities";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

@Component({
    selector: "mfa-password-modal",
    templateUrl: "./mfa-password-modal.component.html",
})
export class MFAPasswordModal implements OnInit, IOtModalContent {
    generatedPassword: string;
    ready = true;
    otModalCloseEvent: Subject<string>;
    otModalData: IMFAModalData;

    constructor(
        private notificationService: NotificationService,
        private translatePipe: TranslatePipe,
        readonly requestQueueService: RequestQueueService,
    ) { }

    ngOnInit() {
        if (this.otModalData.firstRequestPasswordFlow) {
            this.regeneratePassword(false);
        }
    }

    regeneratePassword(regenerate = true) {
        this.ready = false;
        this.requestQueueService.regeneratePassword(this.otModalData.requestQueueId, regenerate)
            .then((response: IProtocolResponse<string>) => {
                if (response.result) {
                    this.generatedPassword = response.data;
                    if (this.otModalData.regenerateFlow || this.otModalData.secondRequestPasswordFlow) {
                        this.otModalData.confirmationText = "MFARegenerateDescription";
                    } else {
                        this.otModalData.callback();
                    }
                }
                this.ready = true;
            });
    }

    copyLink = (): void => {
        Utilities.copyToClipboard(this.generatedPassword);
        this.notificationService.alertNote(
            this.translatePipe.transform("Success"),
            this.translatePipe.transform("PasswordCopied"),
        );
    }

    closeModal(): void {
        this.otModalCloseEvent.next();
    }

}
