// Angular
import {
    Component,
    Input,
    OnInit,
} from "@angular/core";

// Interfaces
import { IRequestQueuePathItem } from "dsarModule/interfaces/request-queue.interface";

// Services
import { RequestQueueDetailService } from "dsarservice/request-queue-detail-service";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

@Component({
    selector: "dsar-request-queue-status-bar",
    templateUrl: "./request-queue-status-bar.component.html",
})
export class RequestQueueStatusBar implements OnInit {
    @Input() readOnly = false;

    pathItems: IRequestQueuePathItem[];
    activeIndex = 0;
    buttonText = this.translatePipe.transform("Advance");
    buttonDisabled = false;
    buttonLoading = false;
    statusText: string;
    statusTextLabel: string;
    statusTextDisplay: string;
    canShowMore: boolean;
    isExpanded: boolean;
    statusTextCharCount: number;
    toggleButtonText: string;
    toggleButtonIcon: string;

    constructor(
        private readonly translatePipe: TranslatePipe,
        readonly requestQueueDetailService: RequestQueueDetailService,
    ) { }

    ngOnInit(): void {
        this.requestQueueDetailService.getObservable$.subscribe(() => {
            this.pathItems = this.requestQueueDetailService.pathItems;
            this.activeIndex = this.requestQueueDetailService.statusBarActiveIndex;
            this.buttonLoading = this.requestQueueDetailService.loadingStatusBarChanges;
            this.buttonDisabled = this.requestQueueDetailService.statusBarButtonDisabled || this.buttonLoading;
            this.readOnly = !this.requestQueueDetailService.permissionsMap.RequestQueueStatusBar || this.buttonLoading;
            if (this.activeIndex >= 0) {
                this.guidanceTextDisplay();
            } else {
                this.statusTextLabel = "";
            }
        });
    }

    toggleText(): void {
        this.isExpanded = !this.isExpanded;
        this.statusTextDisplay =  this.isExpanded ? this.statusText : `${this.statusText.substr(0, 650)}...`;
        this.toggleButtonText = this.isExpanded ? this.translatePipe.transform("ShowLess") : this.translatePipe.transform("ShowMore");
        this.toggleButtonIcon = this.isExpanded ? "ot ot-caret-down" : "fa fa-caret-right";
    }

    guidanceTextDisplay(): void {
        this.isExpanded = false;
        this.statusTextLabel = this.pathItems[this.activeIndex].label;
        this.statusText = this.pathItems[this.activeIndex].guidanceText;
        this.statusTextCharCount = this.statusText.length;
        this.canShowMore = this.statusTextCharCount > 750;
        this.statusTextDisplay = this.canShowMore ? `${this.statusText.substr(0, 650)}...` : this.statusText;
        this.toggleButtonText = this.isExpanded ? this.translatePipe.transform("ShowLess") : this.translatePipe.transform("ShowMore");
        this.toggleButtonIcon = this.isExpanded ? "ot ot-caret-down" : "fa fa-caret-right";
    }

    handleActiveIndexChange(event: number): void {
            this.buttonLoading = true;
            this.buttonDisabled = true;
            this.requestQueueDetailService.handleStatusChange(event);
        }
    }
