// Libraries
import {
    Component,
    EventEmitter,
    Input,
    Output,
    OnInit,
} from "@angular/core";
import { findIndex } from "lodash";
import { IPageable } from "interfaces/pagination.interface";

// Interfaces
import { IDropdownOption } from "interfaces/one-dropdown.interface";
import { IAction } from "interfaces/redux.interface";
import { ISubTask, ISubTaskPaginationParams } from "interfaces/sub-tasks.interface";

// Services
import { Permissions } from "modules/shared/services/helper/permissions.service";

// Constants and Enums
import { WorkflowActions } from "dsarModule/constants/dsar-workflows.constants";
import { SubTaskStatus } from "dsarModule/enums/sub-task.enum";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

@Component({
    selector: "dsar-sub-tasks-table",
    templateUrl: "./sub-tasks-table.component.html",
})
export class SubTasksTable implements OnInit {
    @Input() rows: IPageable;
    @Input() showDropdown;
    @Input() workflowSubtaskTable = false;
    @Input() readOnly = false;
    @Output() handleTableActions: EventEmitter<IAction> = new EventEmitter<IAction>();
    @Output() pageChange: EventEmitter<number> = new EventEmitter<number>();
    hasSubTaskEditAccess = this.permissionsFactory.canShow("DSARSubTaskEdit");
    hasSubTaskSDeleteAccess = this.permissionsFactory.canShow("DSARSubTaskDelete");
    public columns;

    // table config
    sortable = false;
    resizable = false;
    responsive = true;
    sortOrder: string;
    sortedKey: string | number;
    rowBordered = true;
    menuClass: string;
    isLoading = false;
    defaultPagination: ISubTaskPaginationParams = { page: 0, size: 1, sort: `createDT,desc` };
    paginationParams = this.defaultPagination;

    constructor(
        private translatePipe: TranslatePipe,
        private permissionsFactory: Permissions,
    ) {}

    ngOnInit(): void {
        if (this.workflowSubtaskTable) {
            this.columns = [
                { sortKey: 1, columnType: "name", columnName: this.translatePipe.transform("Task"), cellValueKey: "taskName" },
                { sortKey: 2, columnType: "type", columnName: this.translatePipe.transform("Type"), cellValueKey: "typeLabel" },
                { sortKey: 3, columnType: null, columnName: this.translatePipe.transform("Assignee"), cellValueKey: "assignee" },
                { sortKey: -1, columnType: "action", columnName: "Action", cellValueKey: "taskNull" },
            ];
        } else {
            this.columns = [
                { sortKey: 1, columnType: "name", columnName: this.translatePipe.transform("Task"), cellValueKey: "taskName" },
                { sortKey: 2, columnType: "type", columnName: this.translatePipe.transform("Type"), cellValueKey: "typeLabel" },
                { sortKey: 3, columnType: null, columnName: this.translatePipe.transform("Stage"), cellValueKey: "workflowStage" },
                { sortKey: 4, columnType: "status", columnName: this.translatePipe.transform("Status"), cellValueKey: "statusLabel" },
                { sortKey: 5, columnType: null, columnName: this.translatePipe.transform("Assignee"), cellValueKey: "assignee" },
                { sortKey: 6, columnType: "date", columnName: this.translatePipe.transform("Deadline"), cellValueKey: "deadline" },
                { sortKey: -1, columnType: "action", columnName: "Action", cellValueKey: "taskNull" },
            ];
        }
    }

    columnTrackBy(column: any): string {
        return column.id;
    }

    handleSortChange(event: any): void {
        return;
    }

    handleNameClick(row: ISubTask): void {
        if (!this.readOnly) {
            this.handleTableActions.emit({ payload: row, type: WorkflowActions.RESPOND_TO_TASK });
        } else {
            this.handleTableActions.emit({ payload: row, type: WorkflowActions.VIEW_SUBTASK });
        }
    }

    handleMenuItemClicks(row: ISubTask, value: string): void {
        switch (value) {
            case WorkflowActions.EDIT_TASK:
                this.handleTableActions.emit({
                    payload: row,
                    type: WorkflowActions.EDIT_TASK,
                });
                break;
            case WorkflowActions.DELETE_TASK:
                this.handleTableActions.emit({
                    payload: row,
                    type: WorkflowActions.DELETE_TASK,
                });
                break;
        }
    }

    setMenuPosition(row: ISubTask): string {
        if (this.rows.content.length <= 5) return "top-right";
        const halfwayPoint: number = (this.rows.content.length - 1) / 2;
        const rowIndex: number = findIndex(
            this.rows.content,
            (item: ISubTask): boolean => row.id === item.id,
        );
        if (rowIndex > halfwayPoint) {
            return "top-right";
        } else {
            return "bottom-right";
        }
    }

    onPaginationChange(event: number = 0): void {
        this.paginationParams = { ...this.paginationParams, page: event - 1 };
        this.isLoading = true;
    }

    private generateMenuOptions(row: ISubTask): void {
        const menuOptions: IDropdownOption[] = [];
        if (row.status !== SubTaskStatus.Complete && this.showDropdown) {
            if (this.hasSubTaskEditAccess || this.workflowSubtaskTable) {
                menuOptions.push({
                    text: this.translatePipe.transform("Edit"),
                    value: WorkflowActions.EDIT_TASK,
                });
            }
        }
        if ((this.hasSubTaskSDeleteAccess || this.workflowSubtaskTable) && this.showDropdown) {
            menuOptions.push({
                text: this.translatePipe.transform("Delete"),
                value: WorkflowActions.DELETE_TASK,
            });
        }
        if (!menuOptions.length) {
            menuOptions.push({
                text: this.translatePipe.transform("NoActionsAvailable"),
                action: (): void => { }, // Keep Empty
            });
        }
        row.dropDownMenu = menuOptions;
    }
}
