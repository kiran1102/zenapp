// Angular
import {
    Component,
    Inject,
} from "@angular/core";

// 3rd party
import {
    unescape as lodashUnescape,
} from "lodash";
import {
    BehaviorSubject,
    Observable,
    Subject,
} from "rxjs";
import moment from "moment";

// Interfaces
import { IProtocolResponse } from "interfaces/protocol.interface";
import { IOtModalContent } from "@onetrust/vitreus";
import { IStore } from "interfaces/redux.interface";
import { StoreToken } from "tokens/redux-store.token";
import { IOrganizationOrgUserDropdown, IOrganizationOrgUserOrGroupDropdown } from "interfaces/organization-and-groups.interface";
import { IDateTimeOutputModel } from "@onetrust/vitreus";
import {
    ISubTask,
    ISubTaskModalData,
} from "modules/dsar/components/sub-tasks/shared/interfaces/sub-tasks.interface";
import { IPageableOf } from "interfaces/pagination.interface";
import { IISystem, IIWorkflow } from "modules/intg/interfaces/integration.interface";

// Enums + Constants
import { OrgUserDropdownTypes } from "enums/org-user-dropdown.enum";
import { SubTaskType } from "dsarModule/enums/sub-task.enum";
import { EmptyGuid } from "constants/empty-guid.constant";

// Services
import { RequestQueueService } from "dsarservice/request-queue.service";
import { TimeStamp } from "oneServices/time-stamp.service";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

// Redux
import { first } from "rxjs/operators";

@Component({
    selector: "dsar-save-system-task-modal",
    templateUrl: "./dsar-save-system-task-modal.component.html",
})
export class DsarSaveSystemTaskModal implements IOtModalContent {
    cancelButtonText: string;
    deadlineLabel: string;
    reminderLabel: string;
    submitButtonText: string;
    saveAndAddButtonText: string;
    isSubmitting: boolean;
    canSubmit = false;
    taskId: string;
    taskName = "";
    taskType: number;
    taskComment: string;
    deadline: Date | number; // needed to do arithmic
    reminder: Date | number;
    deadlineInput: string;
    reminderInput: string;
    required: boolean;
    isManualRun: boolean;
    userId: any;
    userDropdownType: number = OrgUserDropdownTypes.DSARSubTaskAssignee;
    requestId: string;
    workflowId: string;
    workflowSubtaskModal = false;
    editModal = false;
    taskCommentTouched = false;
    format = this.timeStamp.getDatePickerDateFormat();
    deadlineDisabledUntil = this.timeStamp.getDisableUntilDate();
    reminderDisableUntil = this.timeStamp.getDisableUntilDate(moment().add(1, "d") as any);
    deadlineDisableSince = this.timeStamp.getDisableSinceDate(this.deadline as Date);
    remainderDisabledUntil;
    systems: IISystem[] = [];
    filterSystems: IISystem[] = [];
    workFlowList: IIWorkflow[] = [];
    workFlowListFilter: IIWorkflow[] = [];
    workFlowListCustomFind: IIWorkflow[] = [];
    selectedSystem: IISystem;
    selectedSystemWorkflow: IIWorkflow;
    isLoading = true;
    isUserGroup: boolean;
    disableReminder: boolean;
    viewMode = false;

    modalData: any;
    customModules = [
        [
            "bold",
            "italic",
            "underline",
            "strike",
            { list: "ordered" },
            { list: "bullet" },
        ],
        [
            { align: "" },
            { align: "right" },
            { align: "center" },
            { align: "justify" },
            "link",
        ],
    ];
    taskTypes = SubTaskType;
    contentSource = new BehaviorSubject(this.taskComment);
    contentObservable: Observable<string> = this.contentSource.asObservable();
    otModalCloseEvent: Subject<ISubTask>;
    otModalData: ISubTaskModalData;
    limitTaskName = false;
    readonly MAX_TASK_NAME = 300;

    constructor(
        @Inject(StoreToken) public store: IStore,
        private readonly requestQueueService: RequestQueueService,
        private timeStamp: TimeStamp,
        private translatePipe: TranslatePipe,
    ) { }

    ngOnInit(): void {
        this.requestId = this.otModalData.requestId;
        this.workflowId = this.otModalData.workflowId;
        this.workflowSubtaskModal = this.otModalData.workflowSubtaskModal;
        this.editModal = this.otModalData.isEditModal;
        this.taskType = this.otModalData.type;
        this.viewMode = this.otModalData.viewMode;
        this.cancelButtonText = this.translatePipe.transform("Cancel");
        this.submitButtonText =
            this.editModal || this.workflowSubtaskModal
                ? this.translatePipe.transform("Save")
                : this.translatePipe.transform("Send");
        this.saveAndAddButtonText = this.workflowSubtaskModal
            ? this.translatePipe.transform("SaveAndAdd")
            : this.translatePipe.transform("SendAndAdd");
        this.deadlineLabel = this.workflowSubtaskModal
            ? this.translatePipe.transform("DeadlineDaysEntry")
            : this.translatePipe.transform("Deadline");
        this.reminderLabel = this.workflowSubtaskModal
            ? this.translatePipe.transform("ReminderDaysEntry")
            : this.translatePipe.transform("Reminder");
        this.deadline = this.workflowSubtaskModal ? 0 : null;
        if (this.editModal) {
            this.taskId = this.otModalData.row.id;
            this.taskName = this.otModalData.row.taskName;
            this.taskComment = this.otModalData.workflowSubtaskModal ? lodashUnescape(this.otModalData.row.comment) : lodashUnescape(this.otModalData.row.taskDescription);
            this.contentSource.next(this.taskComment);
            this.required = this.otModalData.row.isRequired;
            this.taskType = this.otModalData.row.type;
            this.userId = this.otModalData.row.userId;
            if (this.userId === EmptyGuid) {
                this.userId = "";
            }
            this.isManualRun = this.otModalData.row.isManualRun;
            this.isUserGroup = this.otModalData.row.isGroup;
            if (this.taskType === SubTaskType.System) {
                this.getCustomSystem(this.otModalData.row.integrationSystemId);
                this.getCustomWorkFlow(this.otModalData.row.integrationSystemId, this.otModalData.row.integrationWorkflowId);
            }
            if (typeof this.otModalData.row.deadline === "string") {
                this.deadline = new Date(this.otModalData.row.deadline);
                this.disableReminder = this.isDeadLineToday(this.deadline);
                this.reminder = new Date(this.otModalData.row.reminder);
                this.deadlineInput = this.otModalData.row.deadline ?
                    this.timeStamp.formatDateToIso({
                        day: (this.deadline as Date).getDate(),
                        month: (this.deadline as Date).getMonth() + 1,
                        year: (this.deadline as Date).getFullYear(),
                    }) : null;
                this.reminderInput = this.otModalData.row.reminder ?
                    this.timeStamp.formatDateToIso({
                        day: (this.reminder as Date).getDate(),
                        month: (this.reminder as Date).getMonth() + 1,
                        year: (this.reminder as Date).getFullYear(),
                    }) : null;
                this.deadlineDisableSince = this.timeStamp.getDisableSinceDate(this.deadline as Date);
            } else {
                this.deadline = this.otModalData.row.deadline;
                this.reminder = this.otModalData.row.reminder;
            }
        } else {
            if (this.taskType === SubTaskType.System) {
                this.getCustomSystems();
                this.getCustomWorkflows();
            }
        }
        this.limitTaskName = this.taskName.trim().length > this.MAX_TASK_NAME;
    }

    closeModal(data: ISubTask | null): void {
        this.otModalCloseEvent.next(data);
    }

    setSystem(value: IISystem) {
        this.selectedSystem = value;
        this.selectedSystemWorkflow = null;
        this.filterWorkflows(value.id);
        this.validateSubmit();
    }

    getSelectedSystemFlow(id: string) {
        this.getCustomSystems();
        this.systems = this.systems.filter((data) => {
            return data.id.match(id);
        });
        this.selectedSystem = this.systems[0];
    }

    filterWorkflows(id: string) {
        this.workFlowListFilter = this.workFlowList.filter((data) => {
            return data.referenceIntegrationId.match(id);
        });
    }

    setSystemIntegrationFlow(value: IIWorkflow) {
        this.selectedSystemWorkflow = value;
        this.validateSubmit();
    }

    getCustomWorkflows() {
        return this.requestQueueService.getWorkflows().pipe(first(),
        ).subscribe(
            (workflowsResponse: IProtocolResponse<IPageableOf<IIWorkflow>>) => {
                this.workFlowList = workflowsResponse.data.content || [];
                this.isLoading = false;
            },
        );
    }
    getCustomSystems() {
        this.requestQueueService.getSeededIntegrations(true).pipe(
            first(),
        ).subscribe(
            (systems: IProtocolResponse<IPageableOf<IISystem>>) => {
                this.systems = systems.data.content.filter((data) => {
                    return !data.name.startsWith("OneTrust-");
                });
                this.isLoading = false;
            },
        );
    }

    getCustomSystem(id: string) {
        return this.requestQueueService.getSeededIntegrations(true).pipe(
            first(),
        ).subscribe(
            (systems: IProtocolResponse<IPageableOf<IISystem>>) => {
                this.systems = systems.data.content.filter((data) => {
                    return !data.name.startsWith("OneTrust-");
                });
                this.filterSystems = this.systems.filter((data) => {
                    return data.id.match(id);
                });
                this.selectedSystem = this.filterSystems[0];
                this.isLoading = false;
            },
        );
    }

    getCustomWorkFlow(systemId: string, workFlowId: string) {
        return this.requestQueueService.getWorkflows().pipe(first(),
        ).subscribe(
            (workflowsResponse: IProtocolResponse<IPageableOf<IIWorkflow>>) => {
                this.workFlowList = workflowsResponse.data.content || [];
                this.filterWorkflows(systemId);
                this.workFlowListCustomFind = this.workFlowListFilter.filter((data) => {
                    return data.id.match(workFlowId);
                });
                this.selectedSystemWorkflow = this.workFlowListCustomFind[0];
                this.isLoading = false;
            },
        );
    }

    onToggle(checked: boolean): void {
        this.required = checked;
        this.validateSubmit();
    }

    onToggleRunManual(checked: boolean): void {
        this.isManualRun = checked;
        this.validateSubmit();
    }

    handleSubmit(addAnotherTask: boolean): void {
        const deadline = this.workflowSubtaskModal
            ? (this.deadline as number)
            : this.deadline ? (this.deadline as Date).toISOString() : null;
        const reminder = this.workflowSubtaskModal
            ? (this.reminder as number)
            : this.reminder ? (this.reminder as Date).toISOString() : null;
        this.isSubmitting = true;
        const saveTask: ISubTask = {
            taskName: this.taskName,
            comment: this.taskComment,
            deadline,
            reminder,
            isRequired: this.required,
            isGroup: this.isUserGroup,
            isManualRun: this.isManualRun,
            userId: this.userId || EmptyGuid,
            type: this.taskType,
            integrationSystemId: this.selectedSystem ? this.selectedSystem.id : "",
            integrationWorkflowId: this.selectedSystemWorkflow ? this.selectedSystemWorkflow.id : "",
            workflowDetailId: this.otModalData.workflowId,
        };
        if (!this.workflowSubtaskModal && this.editModal) {
            this.requestQueueService.updateSubTask(this.taskId, saveTask).then((res: IProtocolResponse<ISubTask>): void => {
                if (res.result) {
                    this.closeModal(res.data);
                }
                this.isSubmitting = false;
            });
        } else if (!this.workflowSubtaskModal && !this.editModal) {
            this.requestQueueService.addSubTask(this.requestId, saveTask).then((res: IProtocolResponse<ISubTask>): void => {
                if (res.result) {
                    this.otModalData.updateSubtask$.next(res.data);
                    if (!addAnotherTask) {
                        this.closeModal(null);
                    }
                    this.clearModalData();
                }
                this.isSubmitting = false;
            });
        } else if (this.workflowSubtaskModal && this.editModal) {
            saveTask.id = this.taskId;
            saveTask.workflowDetailId = this.workflowId;
            this.requestQueueService.updateWorkflowSubTask(saveTask).then((res: IProtocolResponse<ISubTask>): void => {
                if (res.result) {
                    this.closeModal(res.data);
                }
                this.isSubmitting = false;
            });
        } else {
            saveTask.workflowDetailId = this.workflowId;
            this.requestQueueService.addWorkflowSubTask(saveTask).then((res: IProtocolResponse<ISubTask>): void => {
                if (res.result) {
                    this.otModalData.updateSubtask$.next(res.data);
                    if (!addAnotherTask) {
                        this.closeModal(null);
                    }
                    this.clearModalData();
                }
                this.isSubmitting = false;
            });
        }
    }

    saveTaskName(event): void {
        this.taskName = event;
        this.validateSubmit();
    }

    deadlineChange(value: IDateTimeOutputModel): void {
        if (value.date) {
            this.deadlineInput = value.date;
            this.deadline = value.jsdate;
            this.disableReminder = this.isDeadLineToday(this.deadline);
            this.deadlineDisableSince = this.timeStamp.getDisableSinceDate(this.deadline);
            if (this.reminder > this.deadline) {
                this.reminder = null;
                this.reminderInput = null;
            }
        } else {
            this.deadline = null;
            this.deadlineInput = "";
            this.reminderInput = "";
            this.deadlineDisableSince = this.timeStamp.getDisableSinceDate();
            this.reminder = null;
        }
        this.validateSubmit();
    }

    reminderChange(value: IDateTimeOutputModel): void {
        if (value.date) {
            this.reminderInput = value.date;
            this.reminder = value.jsdate;
        } else {
            this.reminderInput = "";
            this.reminder = null;
        }
        this.validateSubmit();
    }

    handleMessageChange(message: string): void {
        if (message && !message.includes("img") && !$(message).text().trim()) {
            this.taskComment = "";
        } else {
            this.taskComment = message;
        }
        this.taskCommentTouched = true;
        this.validateSubmit();
    }

    selectUser(user: IOrganizationOrgUserDropdown): void {
        if (user && user.selection && user.selection.Id) {
            this.userId = user.selection.Id;
            this.isUserGroup = false;

        } else {
            this.userId = "";
        }
        this.validateSubmit();
    }

    selectUserOrGroup(user: IOrganizationOrgUserOrGroupDropdown): void {
        if (user && user.selection && user.selection.id) {
            this.userId = user.selection.id;
            this.isUserGroup = user.selection.type === "Group";

        } else {
            this.userId = "";
        }
        this.validateSubmit();
    }

    validateDeadline(value: number, deadlineInView?: HTMLInputElement): void {
        this.deadline = value;
        if (!this.deadline || this.deadline > 365) {
            this.deadline = 0;
        }
        deadlineInView.value = this.deadline.toString();
        this.validateReminder();
    }

    validateReminder(value?: number, reminderInView?: HTMLInputElement): void {
        if (value) this.reminder = value;
        if (this.deadline && this.reminder) {
            if (this.reminder >= this.deadline) {
                this.reminder = (this.deadline as number) - 1;
            }
        } else if (this.deadline && !this.reminder) {
            this.reminder = 0;
        } else if (!this.deadline) {
            this.reminder = null;
        }
        if (reminderInView) reminderInView.value = this.reminder.toString();
        this.validateSubmit();
    }

    private validateSubmit(): void {
        if (this.taskType === SubTaskType.System) {
            this.canSubmit = !!(this.taskName.trim() && this.taskComment && this.selectedSystem && this.selectedSystemWorkflow);
        } else {
            this.canSubmit = !!(this.taskName.trim() && this.taskComment);
        }
    }

    private isDeadLineToday(deadLine: Date): boolean {
        const today: Date = new Date();
        const isDeadLineToday = (today.toLocaleDateString() === deadLine.toLocaleDateString());
        return isDeadLineToday;
    }

    private clearModalData(): void {
        this.taskName = "";
        this.taskComment = "";
        this.reminder = null;
        this.reminderInput = "";
        this.deadline = null;
        this.deadlineInput = "";
        this.required = false;
        this.taskCommentTouched = false;
        this.selectedSystem = null;
        this.selectedSystemWorkflow = null;
        this.userId = "";
        this.contentSource.next(this.taskComment);
    }
}
