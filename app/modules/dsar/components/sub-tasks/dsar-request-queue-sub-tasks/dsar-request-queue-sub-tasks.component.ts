// Angular
import {
    Component,
    OnInit,
    OnDestroy,
    Output,
    EventEmitter,
    SimpleChanges,
    Input,
} from "@angular/core";

// Libraries
import {
    cloneDeep,
    assign,
} from "lodash";

// RxJs
import { Subject } from "rxjs";
import {
    takeUntil,
    finalize,
    filter,
    take,
} from "rxjs/operators";

// Constants and Enums
import {
    RequestQueueActions,
    RequestQueueTableView,
 } from "dsarModule/constants/dsar-request-queue.constants";
import { ConfirmModalType } from "@onetrust/vitreus";

// Services
import { ModalService } from "sharedServices/modal.service";
import { Permissions } from "modules/shared/services/helper/permissions.service";
import { RequestQueueService } from "dsarservice/request-queue.service";
import { OtModalService, ModalSize } from "@onetrust/vitreus";
import { RequestQueueCacheService } from "dsarservice/request-queue-cache.service";

// Components
import { DsarResponseSubTaskModal } from "dsarModule/components/response-templates/dsar-response-sub-task-modal/dsar-response-sub-task-modal.component";
import { DsarSaveSystemTaskModal } from "dsarModule/components/sub-tasks/dsar-save-system-task-modal/dsar-save-system-task-modal.component";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

// Interfaces
import {
    IRequestQueueDetails,
    IRequestQueueParams,
    ITableColumns,
    IRequestQueueSubTasks,
    IFilterItem,
} from "dsarModule/interfaces/request-queue.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";
import { IAction } from "interfaces/redux.interface";
import { IPageableOf } from "interfaces/pagination.interface";
import { ISubTask, ISubtaskResponseModalData } from "interfaces/sub-tasks.interface";
import { IFilterField } from "modules/dsar/interfaces/dsar-filters.interface";
import { IFilterGetData } from "modules/dsar/interfaces/dsar-sidebar-drawer.interface";

@Component({
    selector: "dsar-request-queue-sub-tasks",
    templateUrl: "./dsar-request-queue-sub-tasks.component.html",
    host: {
        class: "flex-full-height",
    },
})

export class DSARRequestQueueSubTasksComponent implements OnInit, OnDestroy {
    @Input() reloadGrid: boolean;
    @Output() filtersAdded = new EventEmitter<boolean | IFilterItem>();
    @Output() searchRequests = new EventEmitter<string>();

    readonly loadingText = {
        loading: this.translatePipe.transform("Loading"),
        sorting: this.translatePipe.transform("Sorting"),
        filtering: this.translatePipe.transform("Filtering"),
    };
    showingFilteredResults: boolean | IFilterItem;
    set _showingFilteredResults(value: boolean | IFilterItem) {
        this.showingFilteredResults = value;
        this.filtersAdded.emit(value);
    }
    get _showingFilteredResults() {
        return this.showingFilteredResults;
    }

    loadingPrefix = this.loadingText.loading;
    params: IRequestQueueParams;
    ready = false;
    subTasksList: IPageableOf<IRequestQueueSubTasks>;
    subTasksListToFilter: IPageableOf<IRequestQueueSubTasks>;
    sortKey: string | number = "createdDate";
    sortOrder = "desc";
    requestQueueActions = RequestQueueActions;
    columns: ITableColumns[] = this.requestQueueCacheService.getColumns();
    requestQueueDetail: IRequestQueueDetails;
    destroy$: Subject<void> = new Subject();
    isFilterOpen = false;
    currentFilters: IFilterGetData[];

    private tableView: string;
    private readonly defaultParams: IRequestQueueParams = {
        page: 0,
        size: 20,
        sort: "createdDate,desc",
    };
    private readonly emptyPageResponse: IPageableOf<IRequestQueueSubTasks> = {
        content: [],
        first: true,
        last: true,
        number: 0,
        numberOfElements: 0,
        size: 0,
        totalElements: 0,
        totalPages: 1,
    };

    constructor(
        readonly modalService: ModalService,
        readonly permissions: Permissions,
        readonly requestQueueService: RequestQueueService,
        private readonly translatePipe: TranslatePipe,
        private otModalService: OtModalService,
        private requestQueueCacheService: RequestQueueCacheService,
    ) { }

    ngOnInit(): void {
        this.requestQueueService.eventsObservable
            .pipe(takeUntil(this.destroy$))
            .subscribe(({type, payload}) => {
                this.handleAction(type, payload);
            });
        this.tableView = RequestQueueTableView.Filter;
        this.init();
    }

    ngOnChanges(changes: SimpleChanges) {
        if (changes.reloadGrid && !changes.reloadGrid.firstChange) {
            this.tableView = RequestQueueTableView.Filter;
            this.init();
        }
    }

    ngOnDestroy() {
        this.destroy$.next();
        this.destroy$.complete();
    }

    handleTableAction(action: IAction): void {
        this.handleAction(action.type, action.payload);
    }

    toggleFilterPane(): void {
        this.isFilterOpen = !this.isFilterOpen;
    }

    applyFilters() {
        this.toggleFilterPane();
        this.storeParams({ page: 0 });
        this.tableView = RequestQueueTableView.Filter;
        this.init();
    }

    updateTableColumns(): void {
        const translateColumn = (column: ITableColumns) => {
            column.columnName = this.translatePipe.transform(column.columnKey);
        };
        this.columns.forEach(translateColumn);
    }

    handleAction(type: string, payload: any): void {
        switch (type) {
            case RequestQueueActions.CHANGE_PAGE:
                this.loadingPrefix = this.loadingText.loading;
                this.storeParams({ page: payload });
                this.init();
                break;
            case RequestQueueActions.OPEN_SUB_TASK:
                payload = payload.rowData;
                this.otModalService.create(
                    DsarResponseSubTaskModal,
                    {
                        isComponent : true,
                        size: ModalSize.MEDIUM,
                    },
                    {
                        row: {
                            id: payload.subTaskId,
                            taskDescription: payload.subTaskDescription,
                            status: payload.subtaskStatus,
                            assignee: payload.subTaskAssignee,
                            deadline: payload.subTaskDeadline,
                            comments: [],
                            taskName: payload.subTaskName,
                        } as ISubtaskResponseModalData,
                        requestId: payload.requestQueueId,
                        disableFields: false,
                    },
                )
                .pipe(
                    take(1),
                    filter((res) => res && res.result),
                    )
                .subscribe(() => {
                    this.init();
                });
                break;
            case RequestQueueActions.EDIT_SUB_TASK:
                this.otModalService.create(
                        DsarSaveSystemTaskModal,
                        {
                            isComponent: true,
                        },
                        {
                            requestId: payload.requestQueueId,
                            title: this.translatePipe.transform("EditSubTask"),
                            isEditModal: true,
                            row: {
                                id: payload.subTaskId,
                                taskName: payload.subTaskName,
                                isRequired: payload.subTaskRequired,
                                userId: payload.subTaskApproverId,
                                isGroup: payload.subTaskAssignedToGroup,
                                deadline: payload.subTaskDeadline,
                                reminder: payload.subTaskReminderDate,
                                taskDescription: payload.subTaskDescription,
                                type: payload.subTaskType === "USER_TASK" ? 10 : 20,
                                comment: "",
                            },
                        },
                    )
                    .pipe(
                        take(1),
                        filter((res) => !!res),
                        )
                    .subscribe(() => {
                        this.init();
                    });
                break;
            case RequestQueueActions.DELETE_SUB_TASK:
                this.otModalService.confirm({
                        type: ConfirmModalType.DELETE,
                        translations: {
                            title: this.translatePipe.transform("DeleteTask"),
                            desc: "",
                            confirm:  this.translatePipe.transform(
                                "AreYouSureDeleteSubTask",
                            ),
                            submit: this.translatePipe.transform("Delete"),
                            cancel: this.translatePipe.transform("Cancel"),
                        },
                    }).pipe(
                        take(1),
                        filter((res) => res),
                    ).subscribe(() => {
                        this.requestQueueService
                            .deleteSubTask(payload.subTaskId)
                            .then(
                                (res: IProtocolResponse<void>): boolean => {
                                    if (res.result) {
                                        const index = this.subTasksListToFilter.content.findIndex((task: IRequestQueueSubTasks) => {
                                            return task.subTaskId === payload.subTaskId;
                                        });
                                        this.subTasksListToFilter.content.splice(index, 1);
                                        if (!this.subTasksListToFilter.content.length) {
                                            this.storeParams({ page: 0 });
                                        }
                                        this.init();
                                    }
                                    return res.result;
                                },
                            );
                        },
                    );
                break;
            case RequestQueueActions.TOGGLE_DRAWER:
                this.toggleFilterPane();
                break;
        }
    }

    private init(): void {
        this.ready = false;
        const sessionParams = this.requestQueueCacheService.getPageParams();
        this.params = {
            page: parseInt(sessionParams && sessionParams.page as string, 10) || this.defaultParams.page,
            size: parseInt(sessionParams && sessionParams.size as string, 10) || this.defaultParams.size,
            sort: (sessionParams && sessionParams.sort) || this.defaultParams.sort,
        };
        this.updateTableColumns();
        const filterList = this.requestQueueCacheService.getFilters();
        this.currentFilters = (filterList && filterList.length) ? [...filterList] : [];
        if (this.tableView === RequestQueueTableView.Filter && filterList && filterList.length) {
            this.requestQueueService.applySubtaskFilter(
                filterList,
                cloneDeep(this.params),
            ).pipe(
                take(1),
                finalize(() => this.ready = true),
                filter((res) => !!res),
                )
            .subscribe((response: IProtocolResponse<IPageableOf<any>>) => {
                this.subTasksList = response.result ? response.data : this.emptyPageResponse;
                if (!response.data && (this.params.page > 1)) {
                    this.storeParams({ page: (this.params.page as number) - 1 });
                    this.init();
                }
                this.subTasksListToFilter = cloneDeep(this.subTasksList);
                this._showingFilteredResults = {
                    filters: this.currentFilters.length,
                    items: this.subTasksList.totalElements,
                };
            });
        } else {
            this._showingFilteredResults = false;
            this.requestQueueService.getRequestQueueSubTasks(cloneDeep(this.params))
            .pipe(
                take(1),
                finalize(() => this.ready = true),
                )
            .subscribe((response: IProtocolResponse<IPageableOf<IRequestQueueSubTasks>>): void => {
                this.subTasksList = response.result ? response.data : this.emptyPageResponse;
                if (!response.data && (this.params.page > 1)) {
                    this.storeParams({ page: (this.params.page as number) - 1 });
                    this.init();
                } else if (sessionParams && this.subTasksList &&
                        !this.subTasksList.content.length && !sessionParams.page && !response.data.first) {
                    this.storeParams({ page: 0 });
                    this.init();
                }
                this.subTasksListToFilter = cloneDeep(this.subTasksList);
            });
        }
    }

    private storeParams(newParams = this.params): void {
        newParams = assign(this.params, newParams);
        this.requestQueueCacheService.updatePageParams(this.params);
    }

}
