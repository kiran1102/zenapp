// Angular
import {
    Component,
    EventEmitter,
    Input,
    Output,
} from "@angular/core";

 // Services
import { Permissions } from "modules/shared/services/helper/permissions.service";
import { StateService } from "@uirouter/core";

// Constants and Enums
import { RequestQueueStatus } from "dsarModule/enums/request-queue-status.enum";
import {
    SubTaskStatus,
    SubTaskType,
} from "dsarModule/enums/sub-task.enum";
import { RequestQueueActions } from "dsarModule/constants/dsar-request-queue.constants";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

// Interfaces
import { ISubTask } from "interfaces/sub-tasks.interface";
import { IDropdownOption } from "interfaces/one-dropdown.interface";
import { IRequestQueueSubTasks } from "dsarModule/interfaces/request-queue.interface";
import { IAction } from "interfaces/redux.interface";

@Component({
    selector: "dsar-request-queue-sub-tasks-table",
    templateUrl: "./dsar-request-queue-sub-tasks-table.component.html",
})
export class DSARRequestQueueSubTasksTable {

    @Input() rows;
    @Input() sortOrder: string;
    @Input() sortKey: string | number;
    @Input() columnsMap: any;
    @Output() handleTableActions = new EventEmitter<IAction>();

    columns;
    // table config
    sortable = true;
    resizable = true;
    responsive = true;
    rowBordered = true;
    menuClass: string;
    RequestQueueStatusEnums = RequestQueueStatus;
    pageRange: string;
    hasSubTaskEditAccess = this.permissions.canShow("DSARSubTaskEdit");
    hasSubTasksDeleteAccess = this.permissions.canShow("DSARSubTaskDelete");
    subTaskType = SubTaskType;
    userSubtaskConfig = {
        content: this.translatePipe.transform("UserSubtask"),
        placement: "right",
    };
    systemSubtaskConfig = {
        content: this.translatePipe.transform("SystemSubTask"),
        placement: "right",
    };

    constructor(
        private translatePipe: TranslatePipe,
        private permissions: Permissions,
        private stateService: StateService,
    ) { }

    handlePageChange(page: number): void {
        this.handleTableActions.emit({ type: "CHANGE_PAGE", payload: { page } });
    }

    handleSortChange(event: any): void {
        this.sortKey = event.sortKey;
        this.sortOrder = event.sortOrder;
        this.handleTableActions.emit({ type: "SORT_QUEUE", payload: { sortKey: this.sortKey, sortOrder: this.sortOrder } });
    }

    handleNameClick(rowData: IRequestQueueSubTasks, event: JQueryInputEventObject, key: string): void {
        if (event.ctrlKey || event.metaKey || event.shiftKey) {
            return;
        }
        if (key === "subTaskName") {
            this.handleTableActions.emit({ type: RequestQueueActions.OPEN_SUB_TASK, payload: { rowData } });
        }
        if (key === "requestQueueRefId") {
            this.stateService.go("zen.app.pia.module.dsar.views.queue-details", {
                id: rowData.requestQueueId,
            });
        }
    }

    handleMenuItemClicks(row: ISubTask, value: string): void {
        this.handleTableActions.emit({
            payload: row,
            type: value,
        });
    }

    generateMenuOptions(row: ISubTask): void {
        const menuOptions: IDropdownOption[] = [];
        if (row.status !== SubTaskStatus.Complete && this.hasSubTaskEditAccess) {
            menuOptions.push({
                text: this.translatePipe.transform("Edit"),
                value: RequestQueueActions.EDIT_SUB_TASK,
            });
        }
        if (this.hasSubTasksDeleteAccess) {
            menuOptions.push({
                text: this.translatePipe.transform("Delete"),
                value: RequestQueueActions.DELETE_SUB_TASK,
            });
        }
        if (!menuOptions.length) {
            menuOptions.push({
                text: this.translatePipe.transform("NoActionsAvailable"),
                action: (): void => { }, // Keep Empty
            });
        }
        row.dropDownMenu = menuOptions;
    }

    setMenuPosition(row: ISubTask): string {
        if (this.rows.length <= 5) return "top-right";
        const halfwayPoint: number = (this.rows.length - 1) / 2;
        const rowIndex: number = this.rows.findIndex(
            (item: ISubTask): boolean => row.id === item.id,
        );
        if (rowIndex > halfwayPoint) {
            return "top-right";
        } else {
            return "bottom-right";
        }
    }

}
