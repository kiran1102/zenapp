import { Directive, ElementRef, Injector, Input, Output } from "@angular/core";
import { UpgradeComponent } from "@angular/upgrade/static";

@Directive({ selector: "upgraded-dsar-sub-tasks" })
export class UpgradedDSARSubTasks extends UpgradeComponent {
    constructor(elementRef: ElementRef, injector: Injector) {
        super("dsarSubTasks", elementRef, injector);
    }
}
