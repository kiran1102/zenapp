// libraries
import {
    Component,
    Inject,
    OnInit,
    Input,
    OnDestroy,
} from "@angular/core";
import { IPromise } from "angular";
import {
    remove,
    findIndex,
} from "lodash";

// Interfaces
import {
    IDeleteConfirmationModalResolve,
} from "interfaces/delete-confirmation-modal-resolve.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";
import { IAction } from "interfaces/redux.interface";
import {
    ISubTask,
    IDSARRespondSubTaskModalResolve,
    ISubTaskPaginationParams,
} from "modules/dsar/components/sub-tasks/shared/interfaces/sub-tasks.interface";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

// Constants and Enums
import { RequestQueueStatus } from "dsarModule/enums/request-queue-status.enum";
import { WorkflowActions } from "dsarModule/constants/dsar-workflows.constants";
import { SubTaskType } from "dsarModule/enums/sub-task.enum";
import { ConfirmModalType } from "@onetrust/vitreus";

// services
import { RequestQueueService } from "dsarservice/request-queue.service";
import { ModalService } from "sharedServices/modal.service";
import { RequestQueueDetailService } from "dsarservice/request-queue-detail-service";
import { IPageableOf } from "interfaces/pagination.interface";
import { Permissions } from "modules/shared/services/helper/permissions.service";
import { OtModalService, ModalSize } from "@onetrust/vitreus";
import { Subject } from "rxjs";
import {
    takeUntil,
    take,
    filter,
} from "rxjs/operators";

// Components
import { DsarResponseSubTaskModal } from "dsarModule/components/response-templates/dsar-response-sub-task-modal/dsar-response-sub-task-modal.component";
import { DsarSaveSystemTaskModal } from "dsarModule/components/sub-tasks/dsar-save-system-task-modal/dsar-save-system-task-modal.component";

@Component({
    selector: "dsar-sub-tasks",
    templateUrl: "./dsar-sub-tasks.component.html",
})
export class DsarSubTasksComponent implements OnInit, OnDestroy {
    @Input() refreshTable: boolean;
    ready = false;
    subTasksTableData: IPageableOf<ISubTask>;
    requestQueueDetails: any;
    requestId: string;
    showAddSubTaskButton: boolean;
    showAddSystemTaskButton: boolean;
    showDropdown: boolean;
    paginationParam: ISubTaskPaginationParams;
    previousStatus: number;
    updateSubtask$ = new Subject<ISubTask | null>();

    private destroy$ = new Subject();

    constructor(
        @Inject(Permissions) public permissions: Permissions,
        @Inject(ModalService) private modalService: ModalService,
        @Inject(RequestQueueService) private requestQueueService: RequestQueueService,
        @Inject(RequestQueueDetailService) private requestQueueDetailService: RequestQueueDetailService,
        private translatePipe: TranslatePipe,
        private otModalService: OtModalService,
    ) {}

    ngOnInit(): void {
        this.requestQueueDetailService.getObservable$.subscribe(() => {
            this.requestQueueDetails = this.requestQueueDetailService.requestQueueDetails;
            this.requestId = this.requestQueueDetails.requestQueueId;
            this.showDropdown = this.requestQueueDetailService.showDropdown;
            this.showAddSubTaskButton = this.permissions.canShow("DSARSubTaskCreate") &&
                this.requestQueueDetails.status !== RequestQueueStatus.Complete;
            this.showAddSystemTaskButton = this.permissions.canShow("DSARSystemSubTask") &&
                this.requestQueueDetails.status !== RequestQueueStatus.Complete;
            if (this.previousStatus !== this.requestQueueDetails.status
                || !this.subTasksTableData || !this.subTasksTableData.content.length) {
                this.getAllSubTasks();
            }
            this.previousStatus = this.requestQueueDetails.status;
        });
    }

    ngOnDestroy(): void {
        this.destroy$.next();
        this.destroy$.complete();
    }

    loadPage(page: number) {
        this.getAllSubTasks(page);
    }

    private getAllSubTasks(currentPage: number = 0): void {
        this.ready = false;
        this.paginationParam = {
            page: currentPage,
            size: 10,
        };
        this.requestQueueService.getAllSubTasksPaginated(
                this.paginationParam,
                this.requestId,
                this.requestQueueDetailService.workflowList,
            )
            .then(
                (res: IProtocolResponse<IPageableOf<ISubTask>>): void => {
                    if (res.result) {
                        this.subTasksTableData = res.data;
                    }
                    this.ready = true;
                },
            );
    }

    private handleAction(action: IAction): void {
        switch (action.type) {
            case WorkflowActions.CREATE_TASK:
                this.otModalService.create(
                        DsarSaveSystemTaskModal,
                        {
                            isComponent: true,
                        },
                        {
                            requestId: this.requestId,
                            title: this.translatePipe.transform("AddSubTask"),
                            isEditModal: false,
                            type: SubTaskType.User,
                            updateSubtask$: this.updateSubtask$,
                            workflowId: this.requestQueueDetails.currentStageInfo.id,
                        },
                    );
                if (this.updateSubtask$.observers.length < 1) {
                    this.updateSubtask$
                        .pipe(
                            takeUntil(this.destroy$),
                            filter((res) => !!res),
                            )
                        .subscribe(() => {
                            this.requestQueueDetailService.subTasksAdded = true;
                            this.getAllSubTasks(this.paginationParam.page);
                        });
                }
                break;
            case WorkflowActions.CREATE_SYSTEM_TASK:
                this.otModalService.create(
                        DsarSaveSystemTaskModal,
                        {
                            isComponent: true,
                        },
                        {
                            requestId: this.requestId,
                            title: this.translatePipe.transform("AddSystemSubTask"),
                            isEditModal: false,
                            type: SubTaskType.System,
                            updateSubtask$: this.updateSubtask$,
                            workflowId: this.requestQueueDetails.currentStageInfo.id,
                        },
                    );
                if (this.updateSubtask$.observers.length < 1) {
                    this.updateSubtask$
                        .pipe(
                            takeUntil(this.destroy$),
                            filter((res) => !!res),
                            )
                        .subscribe(() => {
                            this.requestQueueDetailService.subTasksAdded = true;
                            this.getAllSubTasks(this.paginationParam.page);
                        });
                }
                break;
            case WorkflowActions.EDIT_TASK:
                this.otModalService.create(
                        DsarSaveSystemTaskModal,
                        {
                            isComponent: true,
                        },
                        {
                            requestId: this.requestId,
                            isEditModal: true,
                            title: this.translatePipe.transform("EditSubTask"),
                            row: action.payload,
                        },
                    )
                    .pipe(
                        take(1),
                        filter((res) => !!res),
                        )
                    .subscribe((res: ISubTask | null) => {
                        this.requestQueueDetailService.subTasksAdded = true;
                        this.getAllSubTasks(this.paginationParam.page);
                    });
                break;
            case WorkflowActions.DELETE_TASK:
                this.otModalService.confirm({
                        type: ConfirmModalType.DELETE,
                        translations: {
                            title: this.translatePipe.transform("DeleteTask"),
                            desc: "",
                            confirm:  this.translatePipe.transform(
                                "AreYouSureDeleteSubTask",
                            ),
                            submit: this.translatePipe.transform("Delete"),
                            cancel: this.translatePipe.transform("Cancel"),
                        },
                    }).pipe(
                        take(1),
                        filter((res) => res),
                    ).subscribe(() => {
                        this.requestQueueService
                            .deleteSubTask(action.payload.id)
                            .then(
                                (res: IProtocolResponse<void>): boolean => {
                                    if (res.result) {
                                        remove(
                                            this.subTasksTableData.content,
                                            (row: ISubTask): boolean =>
                                                row.id === action.payload.id,
                                        );
                                        if (!this.subTasksTableData.content.length) {
                                            this.loadPage(--this.paginationParam.page);
                                        } else {
                                            this.loadPage(this.paginationParam.page);
                                        }
                                    }
                                    return res.result;
                                },
                            );
                        },
                    );
                break;
            case WorkflowActions.RESPOND_TO_TASK:
                const responseSubTaskModalData: IDSARRespondSubTaskModalResolve = {
                    requestId: this.requestId,
                    row: action.payload,
                    disableFields: !this.requestQueueDetailService.showDropdown,
                };
                this.otModalService.create(
                    DsarResponseSubTaskModal,
                    {
                        isComponent : true,
                        size: action.payload.type === SubTaskType.System ? ModalSize.MEDIUM : ModalSize.SMALL,
                    },
                    responseSubTaskModalData,
                )
                .pipe(
                    take(1),
                    filter((res) => res && res.result),
                    )
                .subscribe((): void => {
                        this.requestQueueDetailService.subTasksAdded = true;
                        this.getAllSubTasks(this.paginationParam.page);
                    },
                );
                break;
            default:
                break;
        }
    }
}
