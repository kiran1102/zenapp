class DSARWorkflowsController {
    ready = false;

    public $onInit(): void {
        this.ready = true;
    }
}

export const DSARWorkflowsComponent: any = {
    controller: DSARWorkflowsController,
    template: `
        <section class="flex-full-height height-100">
            <div
                ng-if="$ctrl.ready"
                class="background-white height-100" >
                <dsar-workflow-detail>
                </dsar-workflow-detail>
            </div>
        </section>
    `,
};
