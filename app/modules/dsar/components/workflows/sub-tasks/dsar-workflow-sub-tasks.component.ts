// libraries
import {
    Component,
    Input,
    SimpleChanges,
    OnChanges,
    OnDestroy,
} from "@angular/core";
import { remove } from "lodash";

// Rxjs
import { Subject } from "rxjs";
import {
    takeUntil,
    take,
    filter,
} from "rxjs/operators";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

// Constants and Enums
import { RequestQueueStatus } from "dsarModule/enums/request-queue-status.enum";
import { WorkflowActions } from "dsarModule/constants/dsar-workflows.constants";
import { SubTaskType } from "dsarModule/enums/sub-task.enum";
import { ConfirmModalType } from "@onetrust/vitreus";

// services
import { RequestQueueService } from "dsarservice/request-queue.service";
import { IPageableOf } from "interfaces/pagination.interface";
import { Permissions } from "modules/shared/services/helper/permissions.service";
import { OtModalService } from "@onetrust/vitreus";

// Components
import { DsarSaveSystemTaskModal } from "dsarModule/components/sub-tasks/dsar-save-system-task-modal/dsar-save-system-task-modal.component";

// Interfaces
import { IWorkflowItem } from "interfaces/workflows.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";
import { IAction } from "interfaces/redux.interface";
import {
    ISubTask,
    ISubTaskPaginationParams,
    ISubTaskRowTable,
} from "interfaces/sub-tasks.interface";

@Component({
    selector: "dsar-workflow-sub-tasks",
    templateUrl: "./dsar-workflow-sub-tasks.component.html",
})
export class DsarWorkflowSubTasksComponent implements OnChanges, OnDestroy {
    @Input() updateSubtask: boolean;
    @Input() draftMode: boolean;
    @Input() selectedIndex: number;
    @Input() workflowList: IWorkflowItem[];
    @Input() enableAddSubtask: boolean;

    ready = false;
    subTasksTableData: IPageableOf<ISubTask>;
    selectedWorkflowItem: IWorkflowItem;
    disableSubTaskActions: boolean;
    showAddSubTaskButton: boolean;
    showAddSystemTaskButton: boolean;
    paginationParam: ISubTaskPaginationParams;
    updateSubtask$ = new Subject<ISubTask | null>();

    private destroy$ = new Subject();

    constructor(
        private permissions: Permissions,
        private requestQueueService: RequestQueueService,
        private translatePipe: TranslatePipe,
        private otModalService: OtModalService,
    ) {}

    ngOnChanges(changes: SimpleChanges): void {
        if ((changes.selectedIndex || changes.workflowList)
            && (this.updateSubtask)) {
            this.getAllSubTasks();
        }
        if (changes.enableAddSubtask) {
            this.disableSubTaskActions = false;
            this.selectedWorkflowItem = this.workflowList[this.selectedIndex];
        }
        this.showAddSubTaskButton = (this.workflowList[this.selectedIndex].status !== RequestQueueStatus.Complete);
        this.showAddSystemTaskButton = this.showAddSubTaskButton && this.permissions.canShow("DSARSystemSubTask");
    }

    ngOnDestroy(): void {
        this.destroy$.next();
        this.destroy$.complete();
    }

    loadPage(page: number) {
        this.getAllSubTasks(page);
    }

    private getAllSubTasks(currentPage: number = 0): void {
        this.ready = false;
        this.paginationParam = {
            page: currentPage,
            size: 10,
        };
        this.selectedWorkflowItem = this.workflowList[this.selectedIndex];
        if (this.selectedWorkflowItem.id) {
            this.requestQueueService.getAllWorkflowSubTasks(this.paginationParam, this.selectedWorkflowItem.id).then((res: IProtocolResponse<ISubTaskRowTable>): void => {
                if (res.result) {
                    this.subTasksTableData =  res.data;
                    this.disableSubTaskActions = false;
                }
                this.ready = true;
            });
        } else {
            this.subTasksTableData = null;
            this.disableSubTaskActions = true;
            this.ready = true;
        }
    }

    private handleAction(action: IAction): void {
        // only allow actions if the workflow is in draft status and if all changes have been saved
        if (this.draftMode && !this.disableSubTaskActions) {
            switch (action.type) {
                case WorkflowActions.CREATE_TASK:
                    this.otModalService.create(
                            DsarSaveSystemTaskModal,
                            {
                                isComponent: true,
                            },
                            {
                                workflowId: this.selectedWorkflowItem.id,
                                workflowSubtaskModal: true,
                                title: this.translatePipe.transform("AddSubTask"),
                                isEditModal: false,
                                type: SubTaskType.User,
                                updateSubtask$: this.updateSubtask$,
                            },
                        );
                    if (this.updateSubtask$.observers.length < 1) {
                        this.updateSubtask$
                            .pipe(
                                takeUntil(this.destroy$),
                                filter((res) => !!res),
                                )
                            .subscribe(() => {
                                this.getAllSubTasks(this.paginationParam.page);
                            });
                    }
                    break;
                case WorkflowActions.CREATE_SYSTEM_TASK:
                    this.otModalService.create(
                            DsarSaveSystemTaskModal,
                            {
                                isComponent: true,
                            },
                            {
                                workflowId: this.selectedWorkflowItem.id,
                                title: this.translatePipe.transform("AddSystemSubTask"),
                                isEditModal: false,
                                workflowSubtaskModal: true,
                                type: SubTaskType.System,
                                updateSubtask$: this.updateSubtask$,
                            },
                        );
                    if (this.updateSubtask$.observers.length < 1) {
                        this.updateSubtask$
                            .pipe(
                                takeUntil(this.destroy$),
                                filter((res) => !!res),
                                )
                            .subscribe(() => {
                                this.getAllSubTasks(this.paginationParam.page);
                            });
                    }
                    break;
                case WorkflowActions.EDIT_TASK:
                case WorkflowActions.RESPOND_TO_TASK:
                    this.otModalService.create(
                            DsarSaveSystemTaskModal,
                            {
                                isComponent: true,
                            },
                            {
                                title: this.translatePipe.transform("EditSubTask"),
                                workflowId: this.selectedWorkflowItem.id,
                                workflowSubtaskModal: true,
                                isEditModal: true,
                                row: action.payload,
                            },
                        )
                        .pipe(
                            take(1),
                            filter((res) => !!res),
                        )
                        .subscribe(() => {
                            this.getAllSubTasks(this.paginationParam.page);
                        });
                    break;
                case WorkflowActions.DELETE_TASK:
                    this.otModalService.confirm({
                            type: ConfirmModalType.DELETE,
                            translations: {
                                title: this.translatePipe.transform("DeleteTask"),
                                desc: "",
                                confirm:  this.translatePipe.transform(
                                    "AreYouSureDeleteSubTask",
                                ),
                                submit: this.translatePipe.transform("Delete"),
                                cancel: this.translatePipe.transform("Cancel"),
                            },
                        }).pipe(
                            take(1),
                            filter((res) => res),
                        ).subscribe(() => {
                            this.requestQueueService.deleteWorkflowSubTask(action.payload.id)
                                    .then((res: IProtocolResponse<void>): boolean => {
                                        if (res.result) {
                                            remove(this.subTasksTableData.content, (row: ISubTask): boolean => row.id === action.payload.id);
                                            if (!this.subTasksTableData.content.length) {
                                                this.loadPage(--this.paginationParam.page);
                                            } else {
                                                this.loadPage(this.paginationParam.page);
                                            }
                                        }
                                        return res.result;
                                    });
                                },
                        );
                    break;
                default:
                    break;
            }
        } else if (!this.draftMode) {
            switch (action.type) {
                case WorkflowActions.VIEW_SUBTASK:
                    this.otModalService.create(
                            DsarSaveSystemTaskModal,
                            {
                                isComponent: true,
                            },
                            {
                                title: this.translatePipe.transform("ViewSubtask"),
                                workflowId: this.selectedWorkflowItem.id,
                                workflowSubtaskModal: true,
                                isEditModal: true,
                                viewMode: true,
                                row: action.payload,
                            },
                        );
                    break;
            }
        }
    }
}
