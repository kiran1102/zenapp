// Angular
import {
    Component,
    OnInit,
} from "@angular/core";
import {
    FormBuilder,
    FormGroup,
    Validators,
} from "@angular/forms";

// 3rd party
import {
    find,
    filter,
    sortBy,
 } from "lodash";

// Rxjs
import { Subject } from "rxjs";

 // Services
import { DSARWorkflowService } from "dsarservice/dsar-workflows.service";

// constants and enums
import { FormDataTypes } from "constants/form-types.constant";
import { WorkflowsStatus } from "dsarModule/enums/workflows.enum";
import { NO_BLANK_SPACE } from "constants/regex.constant";

// interfaces
import { IProtocolResponse } from "interfaces/protocol.interface";
import { ICreateData } from "dsarModule/interfaces/dsar-workflows-list.interface";
import {
    IWorkflow,
    IPublishedWorkflow,
    ICreateWorkflow,
} from "interfaces/workflows.interface";
import { IOtModalContent } from "@onetrust/vitreus";

@Component({
    selector: "workflow-create-modal",
    templateUrl: "./dsar-workflow-create-modal.component.html",
})
export class WorkflowCreateModalComponent implements OnInit, IOtModalContent {
    formDataTypes = FormDataTypes;
    formData: ICreateData[] = [
        {
            name: "defaultWorkflow",
            label: "ChooseExistingWorkflow",
            type: FormDataTypes.SELECT,
            isRequired: true,
            labelKey: "workflowName",
            valueKey: "referenceId",
        },
        {
            name: "workflowName",
            label: "WorkflowName",
            type: FormDataTypes.TEXT,
            isRequired: true,
        },
    ];
    isSubmitting: boolean;
    isLoading: boolean;
    workflowForm: FormGroup;
    showAllWorkflows = true;
    otModalCloseEvent: Subject<IWorkflow | null>;
    readonly maxLength = 150;

    constructor(
        private WorkflowsService: DSARWorkflowService,
        private fb: FormBuilder,
    ) {}

    ngOnInit(): void {
        this.isLoading = true;
        this.workflowForm = this.fb.group({
            defaultWorkflow: this.fb.control("", Validators.required),
            workflowName: this.fb.control("",
                [Validators.pattern(NO_BLANK_SPACE), Validators.minLength(1), Validators.required, Validators.maxLength(this.maxLength)]),
        });
        this.WorkflowsService.getPublishedWorkflows().then((response: IProtocolResponse<IPublishedWorkflow[]>): void => {
            const defaultWorkflow: ICreateData = find(this.formData, ["name", "defaultWorkflow"]);
            defaultWorkflow.options = sortBy(response.data, [(res: IPublishedWorkflow) =>
                res.workflowName.toLowerCase()]) || [];
            defaultWorkflow.optionsLookup = [...defaultWorkflow.options];
            this.isLoading = false;
        });
    }

    onInputChange({ key, value }, controlName: string): void {
        if (key === "Enter") { return; }
        if (value) {
            value = value.toUpperCase();
            this.showAllWorkflows = false;
            const workflows: ICreateData = find(this.formData, ["name", controlName]);
            workflows.optionsLookup = filter(workflows.options, (workflow: IPublishedWorkflow): boolean => (workflow.workflowName.toUpperCase().search(value) !== -1));
        } else {
            this.showAllWorkflows = true;
        }
    }

    updateField({currentValue}, controlName: string): void {
        this.workflowForm.get(controlName).setValue(currentValue);
    }

    handleSubmit(): void {
        this.isSubmitting = true;
        const defaultWorkflow: IPublishedWorkflow = this.workflowForm.get("defaultWorkflow").value;
        const workflowName: string = this.workflowForm.get("workflowName").value;
        const createWorkflow: ICreateWorkflow = {
            name: workflowName,
            id: defaultWorkflow.workflowId,
            referenceId: defaultWorkflow.referenceId,
            workflowStatus: WorkflowsStatus.Draft,
            language: null,
        };
        this.WorkflowsService.createNewWorkflow(createWorkflow)
            .then((res: IProtocolResponse<IWorkflow>) => {
                if (res && res.result) {
                    this.otModalCloseEvent.next(res.data);
                }
                this.isSubmitting = false;
        });
    }

    closeModal(): void {
        this.otModalCloseEvent.next();
    }
}
