// Angular
import {
    Component,
    Input,
    Output,
    EventEmitter,
    SimpleChanges,
} from "@angular/core";

// 3rd party
import { BehaviorSubject, Observable } from "rxjs";

// Services

import { ModalService } from "sharedServices/modal.service";
import { Permissions } from "modules/shared/services/helper/permissions.service";

// Constants and enums

import { RequestQueueStatus } from "dsarModule/enums/request-queue-status.enum";
import { WorkflowActions } from "dsarModule/constants/dsar-workflows.constants";

@Component({
    selector: "dsar-edit-workflow",
    templateUrl: "./dsar-edit-workflow.component.html",
})
export class DsarEditWorkflowComponent {
    @Input() workflowList;
    @Input() selectedIndex;
    @Input() draftMode;
    @Input() isDefaultWorkflow;
    @Output() actionCallback = new EventEmitter<{
        type: string,
        payload: any,
    }>();
    allowUpdate: boolean;
    guidanceText = "";
    canEditRequestQueueWorkflow: boolean;
    canCustomWorkflowCalculateDeadline: boolean;
    canCustomWorkflowEditDeadline: boolean;
    canShowStageOutBoundSettings: boolean;
    guidanceTextSource = new BehaviorSubject(this.guidanceText);
    guidanceTextObservable: Observable<string> = this.guidanceTextSource.asObservable();
    changeFromParent = false;

    constructor(
        private readonly permissions: Permissions,
    ) { }

    ngOnChanges(changes: SimpleChanges): void {
        this.canEditRequestQueueWorkflow =
            this.permissions.canShow("DSAREditRequestQueueWorkflow") &&
            this.workflowList[this.selectedIndex].status !==
                RequestQueueStatus.Complete;
        this.canCustomWorkflowCalculateDeadline =
            this.permissions.canShow("DSARCustomWorkflowCalculateDeadline") &&
            this.workflowList[this.selectedIndex].status !==
                RequestQueueStatus.Complete;
        this.canCustomWorkflowEditDeadline =
            this.permissions.canShow("DSARCustomWorkflowEditDeadline") &&
            this.workflowList[this.selectedIndex].status !==
                RequestQueueStatus.Complete;
        this.canShowStageOutBoundSettings =
            this.permissions.canShow("DSARWorkflowSetting") &&
            this.workflowList[this.selectedIndex].status !==
                RequestQueueStatus.Complete;
        this.changeFromParent = (changes.workflowList && !changes.workflowList.firstChange ) ||
            (changes.selectedIndex && !changes.selectedIndex.firstChange);
        this.guidanceTextSource.next(this.workflowList[this.selectedIndex].guidanceText);
        if (!this.workflowList[this.selectedIndex].guidanceText && this.changeFromParent) {
            this.changeFromParent = false;
        }
    }

    handleAction(type: string, payload: any): void {
        this.actionCallback.emit({ type, payload });
    }

    handleMessageChange(message: string): void {
        if (message && !message.includes("img") && !$(message).text().trim()) {
            message = "";
        }
        if (this.draftMode && !this.changeFromParent) {
            this.handleAction(WorkflowActions.WORKFLOW_GUIDANCE_CHANGE, {
                index: this.selectedIndex,
                text: message,
            });
        }
        this.changeFromParent = false;
    }

    saveModal(event) {
        this.workflowList[this.selectedIndex].stage = event;
        this.handleAction("WORKFLOW_TITLE_CHANGE", event.trim());
    }

    toggleUpdateRequest(): void {
        this.handleAction(WorkflowActions.WORKFLOW_UPDATE_TOGGLE, {
            index: this.selectedIndex,
            allowRequestQueueUpdate: !this.workflowList[this.selectedIndex]
                .allowRequestQueueUpdate,
        });
    }

    toggleStageDeadline(): void {
        this.workflowList[this.selectedIndex].beginDeadlineCalc = !this
            .workflowList[this.selectedIndex].beginDeadlineCalc;
        this.handleAction(WorkflowActions.WORKFLOW_DEADLINE_TOGGLE, {
            index: this.selectedIndex,
            beginDeadlineCalculation: this.workflowList[this.selectedIndex]
                .beginDeadlineCalc,
        });
    }

    toggleEditDeadlineAtStage(): void {
        this.handleAction(WorkflowActions.WORKFLOW_DEADLINE_EDIT, {
            index: this.selectedIndex,
            allowDeadlineEdit: !this.workflowList[this.selectedIndex]
                .allowDeadlineEdit,
        });
    }

    scrollRow(scrollLeft: boolean): void {
        if (scrollLeft) {
            $("#workflowRow").animate({
                scrollLeft: "-=250rem",
            });
        } else {
            $("#workflowRow").animate({
                scrollLeft: "+=250rem",
            });
        }
    }
}
