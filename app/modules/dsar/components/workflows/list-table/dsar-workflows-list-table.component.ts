import {
    Component,
    EventEmitter,
    Input,
    Output,
    OnInit,
} from "@angular/core";

// Interfaces
import { IAction } from "interfaces/redux.interface";
import { Permissions } from "modules/shared/services/helper/permissions.service";
import { IGetAllRequests } from "dsarModule/interfaces/request-queue.interface";
import { IUserMap } from "interfaces/user-reducer.interface";

// enums
import { WorkflowsStatus } from "dsarModule/enums/workflows.enum";
import { WorkflowActions } from "dsarModule/constants/dsar-workflows.constants";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

@Component({
    selector: "dsar-workflows-list-table",
    templateUrl: "./dsar-workflows-list-table.component.html",
})
export class WorkflowsListTable implements OnInit {

    @Input() rows;
    @Input() users: IUserMap;
    @Output() handleTableActions: EventEmitter<IAction> = new EventEmitter<IAction>();

    columns;
    // table config
    sortable = false;
    resizable = true;
    responsive = true;
    rowBordered = true;
    menuClass: string;
    workflowsStatus = WorkflowsStatus;

    constructor(
        private translatePipe: TranslatePipe,
        private permissionsFactory: Permissions,
    ) {
    }

    ngOnInit(): void {
        this.columns = [
            { columnType: "link", columnName: this.translatePipe.transform("WorkflowName"), cellValueKey: "workflowName" },
            { columnType: "status", columnName: this.translatePipe.transform("Status"), cellValueKey: "status" },
            { columnType: null, columnName: this.translatePipe.transform("LastUpdatedBy"), cellValueKey: "updatedBy" },
            { columnType: "date", columnName: this.translatePipe.transform("LastUpdatedDate"), cellValueKey: "updatedDate" },
            { columnType: null, columnName: this.translatePipe.transform("CreatedBy"), cellValueKey: "createdBy" },
            { columnType: "date", columnName: this.translatePipe.transform("CreatedDate"), cellValueKey: "createDT" },
        ];
    }

    handleNameClick(rowData: IGetAllRequests): void {
        this.handleTableActions.emit({ type: WorkflowActions.GO_TO_DETAIL_PAGE, payload: { rowData } });
    }
}
