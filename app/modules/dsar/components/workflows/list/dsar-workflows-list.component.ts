import {
    cloneDeep,
    isString,
    assign,
 } from "lodash";
import {
    Component,
    Inject,
    OnInit,
} from "@angular/core";
import { StateService } from "@uirouter/core";

// Redux
import { getCurrentOrgId } from "oneRedux/reducers/org.reducer";

// Rxjs
import {
    take,
    filter,
} from "rxjs/operators";

// Services
import { ModalService } from "sharedServices/modal.service";
import { DSARWorkflowService } from "dsarservice/dsar-workflows.service";
import { OtModalService } from "@onetrust/vitreus";
import { OrgGroupApiService } from "oneServices/api/org-group-api.service";

// Constants and Enums
import { WorkflowActions } from "dsarModule/constants/dsar-workflows.constants";
import { OrgUserTraversal } from "enums/org-user-dropdown.enum";

// Components
import { WorkflowCreateModalComponent } from "dsarModule/components/workflows/create-modal/dsar-workflow-create-modal.component";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

// Interfaces
import { IGetWorkflowsList } from "dsarModule/interfaces/dsar-workflows-list.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";
import { IRequestQueueParams } from "dsarModule/interfaces/request-queue.interface";
import { IAction } from "interfaces/redux.interface";
import {
    IWorkflow,
    ICreateWorkflow,
    ICreateWorkflowModalResolve,
} from "interfaces/workflows.interface";
import { Permissions } from "modules/shared/services/helper/permissions.service";
import { IStore } from "interfaces/redux.interface";
import { IOrgUserAdapted } from "interfaces/org-user.interface";
import { IUserMap } from "interfaces/user-reducer.interface";

// Tokens
import { StoreToken } from "tokens/redux-store.token";

@Component({
    selector: "dsarWorkflowsList",
    templateUrl: "./dsar-workflows-list.component.html",
})
export class WorkflowsList implements OnInit {
    public ready = false;
    public apisInFlight = 0;
    public startEventWatcher;
    public workflowsList: IGetWorkflowsList;
    public workflowsListToFilter: IGetWorkflowsList;
    public loadingText: any = {
        loading: "Loading",
        sorting: "Sorting",
        filtering: "Filtering",
    };
    public loadingPrefix = this.loadingText.loading;
    public defaultParams: IRequestQueueParams = {
        page: 0,
        size: 20,
        sort: "workflowName",
    };
    public params: IRequestQueueParams;
    public createModalData: ICreateWorkflowModalResolve = {
        promiseToResolve: null,
        modalTitle: "CreateNewWorkflow",
        confirmationText: null,
        submitButtonText: "Create",
        cancelButtonText: "Cancel",
    };
    users: IUserMap = {};

    constructor(
        @Inject(StoreToken) private store: IStore,
        private stateService: StateService,
        private permissions: Permissions,
        private WorkflowsService: DSARWorkflowService,
        private otModalService: OtModalService,
        private translatePipe: TranslatePipe,
        private orgGroupApi: OrgGroupApiService,
    ) { }

    public ngOnInit(): void {
        this.init();
    }

    public handleTableAction(action: IAction): void {
        this.handleAction(action.type, action.payload);
    }

    public init(softReload: boolean = false): void {
        this.ready = false;
        const sessionParams = JSON.parse(sessionStorage.getItem("DSARWorkflowParams"));
        this.params = {
            page: parseInt(sessionParams && sessionParams.page, 10) || this.defaultParams.page,
            size: parseInt(sessionParams && sessionParams.size, 10) || this.defaultParams.size,
            sort: (sessionParams && sessionParams.sort) || this.defaultParams.sort,
        };
        this.apisInFlight++;
        this.WorkflowsService.getAllWorkflows(cloneDeep(this.params)).then((response: IProtocolResponse<IGetWorkflowsList>): void => {
            this.workflowsList = response.result ? response.data : {content: []} as IGetWorkflowsList;
            if (!response.data && (this.params.page > 1)) {
                this.params.page = (this.params.page as number) - 1;
                this.softInit(this.params);
            }
            this.workflowsListToFilter = cloneDeep(this.workflowsList);
            this.apisInFlight--;
            if (!this.apisInFlight) this.ready = true;
        });
        this.orgGroupApi.getOrgUsersWithPermission(
            getCurrentOrgId(this.store.getState()),
            OrgUserTraversal.All,
        ).then((response: IProtocolResponse<IOrgUserAdapted[]>): void => {
            if (response.result) {
                response.data.forEach((orgUser: IOrgUserAdapted): void => {
                    this.users[orgUser.Id] = orgUser;
                });
            }
        });
    }

    public softInit(newParams: any = this.params): void {
        newParams = assign(this.params, newParams);
        const params: any = cloneDeep(newParams);
        this.params = params;
        params.filter = !isString(params.filter) ? JSON.stringify(newParams.filter) : params.filter;
        sessionStorage.setItem("DSARWorkflowParams", JSON.stringify(params));
        this.init(true);
    }

    public onPageChange(page) {
        this.softInit({page});
    }

    public handleAction(type: string, payload: any): void {
        switch (type) {
            case WorkflowActions.GO_TO_DETAIL_PAGE:
                if (this.permissions.canShow("DSARCustomWorkflows")) {
                    sessionStorage.setItem("DSARWorkflowId", payload.rowData.workflowId);
                    this.stateService.go("zen.app.pia.module.dsar.views.workflow-details", ({ previousParams: this.stateService.params }));
                }
                break;
            case WorkflowActions.CREATE_REQUEST:
                this.otModalService.create(
                        WorkflowCreateModalComponent,
                        {
                            isComponent: true,
                        },
                    )
                    .pipe(
                        take(1),
                        filter((response: IWorkflow | null) => !!response),
                        )
                    .subscribe((response: IWorkflow) => {
                        sessionStorage.setItem("DSARWorkflowId", response.id);
                        this.stateService.go("zen.app.pia.module.dsar.views.workflow-details", ({ previousParams: this.stateService.params }));
                    });
                break;
            default:
                break;
        }
    }
}
