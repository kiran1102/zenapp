// Angular
import {
    Component,
    OnInit,
    Input,
    Output,
    EventEmitter,
} from "@angular/core";

// 3rd Party
import {
    sortBy,
    forEach,
} from "lodash";
import {
    Observable,
    Subject,
    Subscription,
} from "rxjs";
import {
    debounceTime,
    takeUntil,
    first,
} from "rxjs/operators";

// Interfaces
import { IProtocolResponse } from "interfaces/protocol.interface";
import { IDSARSettingsGroup } from "dsarModule/interfaces/dsar-settings-group.interface";
import { IKeyValue } from "interfaces/generic.interface";
import { ISettingsMeta } from "sharedModules/interfaces/settings.interface";
import { IDSARSettings } from "dsarModule/interfaces/dsar-settings.interface";
import { IPageableOf } from "interfaces/pagination.interface";

import {
    IISystem,
    IIWorkflow,
} from "modules/intg/interfaces/integration.interface";

// Enums and Constants
import { WorkflowSettingsLabel } from "dsarModule/enums/workflows.enum";
import { SettingsType } from "sharedModules/enums/settings.enum";

// Services
import { DSARSettingsService } from "dsarservice/dsar-settings.service";
import { RequestQueueService } from "dsarservice/request-queue.service";
import { Permissions } from "modules/shared/services/helper/permissions.service";

// Pipes
import { InputNumberPipe } from "pipes/input-number.pipe";

@Component({
    selector: "dsar-workflow-settings",
    templateUrl: "./dsar-workflow-settings.component.html",
    host: {
        class: "flex-full-height height-100 padding-left-1 workFlowSettings",
    },
})

export class DsarWorkFlowSettings implements OnInit {
    @Input() workFlowId: string;
    @Input() isWorkFlowLevel: boolean;
    @Input() enableLoading: boolean;
    @Input() isEditMode: boolean;
    @Input() workflowName: string;
    @Output() editEvent = new EventEmitter<boolean>();
    @Output() updateSettings = new EventEmitter<any>();
    @Output() changeWorkflowName = new EventEmitter<string>();
    workflowSettings: IDSARSettings[];
    workflowSettingToggle: boolean[] = [];
    outboundSettings: IDSARSettings[] = [];
    showOutBoundEvents: boolean;
    workflowSettingsMeta: ISettingsMeta[];
    noSettingsFound: boolean;
    systems: IISystem[] = [];
    filterSystems: IISystem[] = [];
    workFlowList: IIWorkflow[] = [];
    workFlowListFilter: IIWorkflow[] = [];
    workFlowListCustomFind: IIWorkflow[] = [];
    selectedSystem: IISystem;
    selectedSystemWorkflow: IIWorkflow;
    ready = false;
    isLoading = true;
    workFlowSettingsOutput: Array<IKeyValue<boolean>> = [];
    SettingsTranslations = {
        4020: {
            name: "EnableSystemSubTaskCreateEvent",
            description: "EnableSystemSubTaskCreateEvent.description",
        },
        4010: {
            name: "EnableRequestCreationEvent",
            description: "EnableRequestCreationEvent.description",
        },
        4000: {
            name: "EnableSubTaskCreationEvent",
            description: "EnableSubTaskCreationEvent.description",
        },
    };
    workflowSettingsMap = {
        MultiFactorAuthenticationEnabled: false,
        AllowNumbers: false,
        AllowUpperCase: false,
        AllowLowerCase: false,
        AllowSpecialCharacters: false,
        AccessCodeLength: 10,
        IntegrationSystemId: "",
        IntegrationWorkflowId: "",
    };
    inputObservable$: Observable<any>;
    showReqestCreateEventToggle: boolean;
    private contentSource = new Subject<any>();
    private destroy$ = new Subject();

    constructor(
        readonly dsarSettingsService: DSARSettingsService,
        private readonly inputNumber: InputNumberPipe,
        private readonly requestQueueService: RequestQueueService,
        private permissions: Permissions,

    ) {
        this.showReqestCreateEventToggle = permissions.canShow("DSARSystemSubTask");
    }

    ngOnInit() {
        this.inputObservable$ = this.contentSource.asObservable();
        this.inputObservable$.pipe(
                debounceTime(1000),
                takeUntil(this.destroy$),
            ).subscribe((input: any) => {
                this.workflowSettingsMap[input.actionType] = this.inputNumber.transform(input.target.value, input.minValue, input.maxValue);
            });
        this.loadSettings();
        this.workflowSettingsMeta = [
            {
                title: WorkflowSettingsLabel.MultiFactorAuthentication,
                type: SettingsType.TOGGLE,
                actionType: WorkflowSettingsLabel.MultiFactorAuthenticationEnabled,
                description: WorkflowSettingsLabel.MultiFactorAuthenticationEnabledSubtext,
                isVisible: true,
                children: [
                    {
                        actionType: WorkflowSettingsLabel.AccessCodeLength,
                        title: WorkflowSettingsLabel.AccessCodeLength,
                        type: SettingsType.NUMBER,
                        minValue: 10,
                        maxValue: 15,
                        stepValue: 1,
                        visibleString: WorkflowSettingsLabel.MultiFactorAuthenticationEnabled,
                    },
                    {
                        actionType: WorkflowSettingsLabel.AllowUpperCase,
                        title: WorkflowSettingsLabel.IncludeUpperCase,
                        type: SettingsType.CHECKBOX,
                        visibleString: WorkflowSettingsLabel.MultiFactorAuthenticationEnabled,
                    },
                    {
                        actionType: WorkflowSettingsLabel.AllowLowerCase,
                        title: WorkflowSettingsLabel.IncludeLowerCase,
                        type: SettingsType.CHECKBOX,
                        visibleString: WorkflowSettingsLabel.MultiFactorAuthenticationEnabled,
                    },
                    {
                        actionType: WorkflowSettingsLabel.AllowSpecialCharacters,
                        title: WorkflowSettingsLabel.IncludeSpecialCharacters,
                        type: SettingsType.CHECKBOX,
                        visibleString: WorkflowSettingsLabel.MultiFactorAuthenticationEnabled,
                    },
                    {
                        actionType: WorkflowSettingsLabel.AllowNumbers,
                        title: WorkflowSettingsLabel.IncludeNumbers,
                        type: SettingsType.CHECKBOX,
                        visibleString: WorkflowSettingsLabel.MultiFactorAuthenticationEnabled,
                    },
                ],
            },
        ];
    }
     setSystem(value: IISystem) {
        this.selectedSystem = value;
        this.selectedSystemWorkflow = null;
        this.filterWorkflows(value.id);
        this.toggleAndSaveWorkFlowSettingByNameAction("IntegrationSystemId", value.id);
    }

    getSelectedSystemFlow(id: string) {
        this.getCustomSystems();
        this.systems = this.systems.filter((data) => {
            return data.id.match(id);
        });
        this.selectedSystem = this.systems[0];
    }

    filterWorkflows(id: string) {
        this.workFlowListFilter = this.workFlowList.filter((data) => {
            return data.referenceIntegrationId.match(id);
        });
    }

    setSystemIntegrationFlow(value: IIWorkflow) {
        this.selectedSystemWorkflow = value;
        this.toggleAndSaveWorkFlowSettingByNameAction("IntegrationWorkflowId", value.id);
    }

    getCustomWorkflows() {
        return this.requestQueueService.getWorkflows().pipe(first(),
        ).subscribe(
            (workflowsResponse: IProtocolResponse<IPageableOf<IIWorkflow>>) => {
                this.workFlowList = workflowsResponse.data.content || [];
                this.isLoading = false;
            },
        );
    }

    getCustomSystems() {
        this.requestQueueService.getSeededIntegrations(true).pipe(
            first(),
        ).subscribe(
            (systems: IProtocolResponse<IPageableOf<IISystem>>) => {
                this.systems = systems.data.content.filter((data) => {
                    return !data.name.startsWith("OneTrust-");
                });
                this.isLoading = false;
        });
    }

    getCustomSystem(id: string) {
        return this.requestQueueService.getSeededIntegrations(true).pipe(
            first(),
        ).subscribe(
            (systems: IProtocolResponse<IPageableOf<IISystem>>) => {
            this.systems = systems.data.content.filter((data) => {
                return !data.name.startsWith("OneTrust-");
            });
            this.filterSystems = this.systems.filter((data) => {
                return data.id.match(id);
            });
            this.selectedSystem = this.filterSystems[0];
            this.filterWorkflows(this.selectedSystem.id);
            this.isLoading = false;
        });
    }

    getCustomWorkFlow(id: string) {
        return this.requestQueueService.getWorkflows().pipe(first(),
        ).subscribe(
            (workflowsResponse: IProtocolResponse<IPageableOf<IIWorkflow>>) => {
                this.workFlowList = workflowsResponse.data.content || [];
                this.workFlowListCustomFind = this.workFlowList.filter((data) => {
                    return data.id.match(id);
                });
                this.selectedSystemWorkflow = this.workFlowListCustomFind[0];
                this.isLoading = false;
            });
    }
    loadSettings() {
        if (this.workFlowId) {
            this.dsarSettingsService.getSettingsByGroup(this.workFlowId)
                .then((response: IProtocolResponse<IDSARSettingsGroup>): boolean => {
                    if (response.result) {
                        this.workflowSettings = response.data ? response.data.settings : [];
                        this.workflowSettings = sortBy(this.workflowSettings, "name");
                        this.noSettingsFound = !this.workflowSettings.length;
                        forEach(this.workflowSettings, (param: IDSARSettings) => {
                            if ((param.name === WorkflowSettingsLabel.AllowNumbers) ||
                                (param.name === WorkflowSettingsLabel.AllowLowerCase) ||
                                (param.name === WorkflowSettingsLabel.AllowSpecialCharacters) ||
                                (param.name === WorkflowSettingsLabel.AllowUpperCase) ||
                                (param.name === WorkflowSettingsLabel.MultiFactorAuthenticationEnabled) ||
                                ["4020", "4010", "4000"].includes(param.name)) {
                                param.value = param.value === "true";
                            } else if (param.name === WorkflowSettingsLabel.AccessCodeLength) {
                                param.value = parseInt(param.value, 10);
                            }
                            if (["4020", "4010", "4000"].includes(param.name)) {
                                this.outboundSettings.push(param);
                                this.workFlowSettingsOutput[param.name] = param.value;
                                this.showOutBoundEvents = this.workFlowSettingsOutput["4010"];
                                if (this.showOutBoundEvents) {
                                    this.getCustomSystems();
                                    this.getCustomWorkflows();
                                }
                            } else {
                                this.workflowSettingsMap[param.name] = param.value;
                            }
                            if (param.name === "IntegrationSystemId" && param.value && this.showOutBoundEvents) {
                                this.getCustomSystem(param.value);
                                this.workflowSettingsMap[param.name] = param.value;
                            }
                            if (param.name === "IntegrationWorkflowId" && param.value && this.showOutBoundEvents) {
                                this.getCustomWorkFlow(param.value);
                                this.workflowSettingsMap[param.name] = param.value;
                             }
                        });
                        if (this.isWorkFlowLevel) {
                            this.dsarSettingsService.workFlowSettingsOutput = this.workFlowSettingsOutput;
                        } else {
                            this.workflowSettingsMap = null;
                            this.dsarSettingsService.stageSettingsOutput = this.workFlowSettingsOutput;
                        }
                        this.ready = true;
                    } else {
                        this.noSettingsFound = true;
                    }
                    return response.result;
                });
        }
    }

   handleSettingsAction(type: string, payload: any): void {
        const eventData = { payload: null };
        switch (type) {
            case WorkflowSettingsLabel.MultiFactorAuthenticationEnabled:
                this.workflowSettingsMap[type] = !this.workflowSettingsMap[type];
                if (!this.workflowSettingsMap[type]) {
                    this.workflowSettingsMap.AllowLowerCase = false;
                    this.workflowSettingsMap.AllowNumbers = false;
                    this.workflowSettingsMap.AllowSpecialCharacters = false;
                    this.workflowSettingsMap.AllowUpperCase = false;
                    this.workflowSettingsMap.AccessCodeLength = 10;
                }
                break;
            case WorkflowSettingsLabel.AllowUpperCase:
            case WorkflowSettingsLabel.AllowLowerCase:
            case WorkflowSettingsLabel.AllowSpecialCharacters:
            case WorkflowSettingsLabel.AllowNumbers:
                this.workflowSettingsMap[type] = !this.workflowSettingsMap[type];
                break;
            case WorkflowSettingsLabel.AccessCodeLength:
                eventData.payload = {...payload};
                eventData.payload.event.actionType = type;
                if (eventData.payload.event) {
                    this.contentSource.next(eventData.payload.event);
                }
                break;
            case WorkflowSettingsLabel.IntegrationSystemId:
                eventData.payload = {...payload};
                break;
            case WorkflowSettingsLabel.IntegrationWorkflowId:
                eventData.payload = {...payload};
                break;
        }
        this.updateSettings.emit(this.workflowSettingsMap);
    }

    trackByFn(index: number, setting: ISettingsMeta): string {
        return setting.title;
    }

    toggleAndSaveWorkFlowSettingByNameAction(name: string, value: string) {
         this.workflowSettingsMap[name] = value;
         this.handleSettingsAction(name, value);
    }

    toggleAndSaveWorkFlowSettingByName(name: string) {
        this.workFlowSettingsOutput[name] = !this.workFlowSettingsOutput[name];
        this.sendWorkFlowSettings();
        this.showOutBoundEvents = this.workFlowSettingsOutput["4010"];
        if (this.showOutBoundEvents) {
            this.getCustomSystems();
            this.getCustomWorkflows();
        }
        this.dsarSettingsService.workFlowSettingsOutput = this.workFlowSettingsOutput;
    }

    sendWorkFlowSettings() {
        this.editEvent.emit(true);
    }

    editWorkflowName(name: string) {
        this.workflowName = name;
        this.changeWorkflowName.emit(name);
    }
}
