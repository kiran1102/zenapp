import { IProtocolResponse } from "interfaces/protocol.interface";
import {
    isNil,
    find,
} from "lodash";
import { IPromise } from "angular";
import { Permissions } from "modules/shared/services/helper/permissions.service";
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import { IStringMap } from "interfaces/generic.interface";
import { DSARSettingsService } from "dsarservice/dsar-settings.service";
import {
    ICurrencyList,
    IDSARSettings,
    IDSARSettingsUpdate,
} from "dsarModule/interfaces/dsar-settings.interface";
import { IConfirmationModalResolve } from "interfaces/confirmation-modal-resolve.interface";
import { ModalService } from "sharedServices/modal.service";

class DSARSettingsGeneralController implements ng.IComponentController {
    static $inject: string[] = [
        "$rootScope",
        "Permissions",
        "DSARSettingsService",
        "$q",
        "ModalService",
    ];

    static readonly MAX_DAYS_TO_REVOKE = 9999;
    static readonly DEFAULT_DAYS_TO_REVOKE = 90;

    currencyList: ICurrencyList[] = [];
    isCostCalcEnabled = false;
    hourlyCost: number;
    currencyIso: string;
    permissions: IStringMap<boolean>;
    costCalcSaveInProgress: boolean;
    costCalcIsLoading: boolean;
    costCalcIsDirty = false;
    isAutoDeleteEnabled = false;
    isAutoDeleteOnCompletionEnabled = false;
    daysToDelete: number;
    isAutoDelDirty = false;
    autoDelSaveInProgress: boolean;
    isAutoDelLoading: boolean;
    isDsRequestAccessEnabled = false;
    daysBeforeRevoke: number;
    isPortalAccessDirty = false;
    portalAccessSaveInProgress: boolean;
    isPortalAccessLoading: boolean;

    private translations: IStringMap<string>;
    private warningIconClass = "ot ot-exclamation-circle fa-2x text-warning";
    private costCalculationSettings: IDSARSettings[];
    private autoDeleteSettings: IDSARSettings[];
    private portalAccessSettings: IDSARSettings[];

    constructor(
        private $rootScope: IExtendedRootScopeService,
        private permissionsService: Permissions,
        private dsarSettingsService: DSARSettingsService,
        private $q: ng.IQService,
        private modalService: ModalService,
    ) { }

    public $onInit(): void {
        this.translations = {
            cancelButtonText: this.$rootScope.t("Cancel"),
            confirmButtonText: this.$rootScope.t("Confirm"),
            changeButtonText: this.$rootScope.t("Change"),
            autoDeleteModalTitle: this.$rootScope.t("ChangingRetentionPolicy"),
            autoDeleteConfirmationText: this.$rootScope.t("ChangeRetentionPolicyConfirmation"),
            currencyChangeModalTitle: this.$rootScope.t("CurrencyChangeConfirmation"),
            currencyChangeConfirmationText: this.$rootScope.t("ChangeCostCalcConfirmation"),
            portalAccessModalTitle: this.$rootScope.t("DataSubjectRequestPortalSettings"),
            portalAccessConfirmationText: this.$rootScope.t("DSPortalChangeAccessInfo"),
        };

        this.costCalcIsLoading = true;
        this.isAutoDelLoading = true;
        this.isPortalAccessLoading = true;
        const promiseArr: Array<IPromise<IProtocolResponse<any>>> = [
            this.dsarSettingsService.getSettingsByGroup("CostCalculation"),
            this.dsarSettingsService.getSettingsByGroup("AutoDeleteAttachment"),
            this.dsarSettingsService.getSettingsByGroup("DSRequestQueueAccess"),
        ];
        if (this.permissionsService.canShow("CurrencyCodes")) {
            promiseArr.push(this.dsarSettingsService.getCurrencyList());
        }
        this.$q.all(promiseArr).then((response: Array<IProtocolResponse<any>>): void => {
            this.costCalculationSettings = response[0] && response[0].result ? response[0].data.settings : [];
            this.currencyList = response[3] && response[3].result ? response[3].data : [];
            const costCalcEnabledSetting: IDSARSettings = find(this.costCalculationSettings, ["name", "Enabled"]);
            const costCalcHourlySetting: IDSARSettings = find(this.costCalculationSettings, ["name", "CostPerHour"]);
            const costCurrencySetting: IDSARSettings = find(this.costCalculationSettings, ["name", "Currency"]);
            this.isCostCalcEnabled = costCalcEnabledSetting.value === "true";
            this.hourlyCost = parseFloat(costCalcHourlySetting.value) || 0;
            this.currencyIso = costCurrencySetting.value;
            this.persistCostCalcSettingsLocally();
            this.costCalcIsLoading = false;

            // Auto Delete Settings
            this.autoDeleteSettings = response[1] && response[1].result ? response[1].data.settings : [];
            const autoDelEnabledSetting = find(this.autoDeleteSettings, ["name", "AutoDeleteEnabled"]);
            const autoDelCompletionSetting = find(this.autoDeleteSettings, ["name", "AutoDeleteOnCompletion"]);
            const daysAfterDelCompSetting = find(this.autoDeleteSettings, ["name", "DaysAfterDeletionCompletion"]);
            this.isAutoDeleteEnabled = autoDelEnabledSetting.value === "true";
            this.isAutoDeleteOnCompletionEnabled = autoDelCompletionSetting.value === "true";
            this.daysToDelete = parseFloat(daysAfterDelCompSetting.value) || 0;
            this.isAutoDelLoading = false;

            // DS access settings Settings
            this.portalAccessSettings = response[2] && response[2].result ? response[2].data.settings : [];
            const requestAccessEnabledSetting = find(this.portalAccessSettings, ["name", "DSRequestAccessEnabled"]);
            const daysBeforeRevokeSetting = find(this.portalAccessSettings, ["name", "DSDaysToRevokeAccess"]);
            this.isDsRequestAccessEnabled = requestAccessEnabledSetting.value === "true";
            this.daysBeforeRevoke = parseFloat(daysBeforeRevokeSetting.value) || 1;
            this.isPortalAccessLoading = false;
        });
    }

    public toggleCostCalcSetting(): void {
        this.isCostCalcEnabled = !this.isCostCalcEnabled;
        this.costCalcIsDirty = true;
    }

    public toggleAutoDeleteSetting(): void {
        this.isAutoDeleteEnabled = !this.isAutoDeleteEnabled;
        if (!this.isAutoDeleteEnabled) {
            this.isAutoDeleteOnCompletionEnabled = false;
            this.daysToDelete = 30;
        }
        this.isAutoDelDirty = true;
    }

    public toggleAutoDeleteOnCompSetting(): void {
        this.isAutoDeleteOnCompletionEnabled = !this.isAutoDeleteOnCompletionEnabled;
        if (!this.isAutoDeleteOnCompletionEnabled) {
            this.daysToDelete = 30;
        }
        this.isAutoDelDirty = true;
    }

    public selectCurrency(countryIso: string): void {
        this.currencyIso = countryIso;
        this.costCalcIsDirty = true;
    }

    public saveAutoDelChanges(): IPromise<boolean> {
        const autoDelUpdateBody: IDSARSettingsUpdate = {
            name: "AutoDeleteAttachment",
            settings: [
                { name: "AutoDeleteEnabled", value: this.isAutoDeleteEnabled },
                { name: "AutoDeleteOnCompletion", value: this.isAutoDeleteOnCompletionEnabled },
                { name: "DaysAfterDeletionCompletion", value: this.daysToDelete },
            ],
        };
        return this.dsarSettingsService.updateSettingsByGroup(autoDelUpdateBody)
            .then((response: IProtocolResponse<void>): boolean => {
                this.autoDelSaveInProgress = false;
                this.isAutoDelDirty = false;
                return response.result;
        });
    }

    public saveAutoDeleteSettings(): void {
        this.autoDelSaveInProgress = false;
        const confirmAutoDelModalData: IConfirmationModalResolve = {
            modalTitle: this.translations.autoDeleteModalTitle,
            confirmationText: this.translations.autoDeleteConfirmationText,
            cancelButtonText: this.translations.cancelButtonText,
            submitButtonText: this.translations.confirmButtonText,
            iconClass: this.warningIconClass,
            promiseToResolve: (): IPromise<boolean> => {
                return this.saveAutoDelChanges();
            },
        };
        this.modalService.setModalData(confirmAutoDelModalData);
        this.modalService.openModal("confirmationModal");
    }

    public saveCostCalcSettings(): void {
        this.costCalcSaveInProgress = false;
        const confirmCostCalcModalData: IConfirmationModalResolve = {
            modalTitle: this.translations.currencyChangeModalTitle,
            confirmationText: this.translations.currencyChangeConfirmationText,
            cancelButtonText: this.translations.cancelButtonText,
            submitButtonText: this.translations.changeButtonText,
            iconClass: this.warningIconClass,
            promiseToResolve: (): IPromise<boolean> => {
                return this.saveCostCalcChanges();
            },
        };
        this.modalService.setModalData(confirmCostCalcModalData);
        this.modalService.openModal("confirmationModal");
    }

    public validateSchedulingDays(): void {
        if (!this.daysToDelete || this.daysToDelete > 9999) {
            this.daysToDelete = 30;
        }
        this.isAutoDelDirty = true;
    }

    // Need this validation because that's what the business req is
    public validateHourlyCost(): void {
        if (this.hourlyCost > 999999999.99 ) {
            this.hourlyCost = 999999999.99;
        } else if (isNil(this.hourlyCost)) {
            this.hourlyCost = 0;
        }
        this.costCalcIsDirty = true;
    }

    public togglePortalAccessSetting(): void {
        this.isDsRequestAccessEnabled = !this.isDsRequestAccessEnabled;
        this.daysBeforeRevoke = DSARSettingsGeneralController.DEFAULT_DAYS_TO_REVOKE;
        this.isPortalAccessDirty = true;
    }

    public savePortalAccessSettings(): void {
        this.portalAccessSaveInProgress = false;
        const confirmPortalAccessModalData: IConfirmationModalResolve = {
            modalTitle: this.translations.portalAccessModalTitle,
            confirmationText: this.translations.portalAccessConfirmationText,
            cancelButtonText: this.translations.cancelButtonText,
            submitButtonText: this.translations.confirmButtonText,
            iconClass: this.warningIconClass,
            promiseToResolve: (): IPromise<boolean> => {
                return this.savePortalAccessChanges();
            },
        };
        this.modalService.setModalData(confirmPortalAccessModalData);
        this.modalService.openModal("confirmationModal");
    }

    public savePortalAccessChanges(): IPromise<boolean> {
        const autoDelUpdateBody: IDSARSettingsUpdate = {
            name: "DSRequestQueueAccess",
            settings: [
                { name: "DSRequestAccessEnabled", value: this.isDsRequestAccessEnabled },
                { name: "DSDaysToRevokeAccess", value: this.daysBeforeRevoke },
            ],
        };
        return this.dsarSettingsService.updateSettingsByGroup(autoDelUpdateBody)
            .then((response: IProtocolResponse<void>): boolean => {
                this.portalAccessSaveInProgress = false;
                this.isPortalAccessDirty = false;
                return response.result;
        });
    }

    public validateRevokeSchedulingDays(): void {
        if (!this.daysBeforeRevoke || this.daysBeforeRevoke > DSARSettingsGeneralController.MAX_DAYS_TO_REVOKE) {
            this.daysBeforeRevoke = DSARSettingsGeneralController.DEFAULT_DAYS_TO_REVOKE;
        }
        this.isPortalAccessDirty = true;
    }

    private persistCostCalcSettingsLocally(): void {
        const costCalcSettings: {
            isCostCalcEnabled: boolean;
            hourlyCost: number;
            currencyIso: string;
        } = {
            isCostCalcEnabled: this.isCostCalcEnabled,
            hourlyCost: this.hourlyCost,
            currencyIso: this.currencyIso,
        };
        sessionStorage.setItem("DSARCostCalcSettings", JSON.stringify(costCalcSettings));
    }

    private saveCostCalcChanges(): IPromise<boolean> {
        const costCalcUpdateBody: IDSARSettingsUpdate = {
            name: "CostCalculation",
            settings: [
                { name: "Enabled", value: this.isCostCalcEnabled },
                { name: "CostPerHour", value: this.hourlyCost },
                { name: "Currency", value: this.currencyIso },
            ],
        };
        return this.dsarSettingsService.updateSettingsByGroup(costCalcUpdateBody)
            .then((response: IProtocolResponse<void>): boolean => {
                if (response.result) {
                    this.persistCostCalcSettingsLocally();
                }
                this.costCalcSaveInProgress = false;
                this.costCalcIsDirty = false;
                return response.result;
            });
    }
}

export const DSARSettingsGeneralComponent: any = {
    controller: DSARSettingsGeneralController,
    template: `
        <section class="gray-shadow margin-all-3 padding-left-right-3 padding-top-bottom-2 column-nowrap background-white">
            <div>
                <h2
                    class="header margin-top-0 margin-bottom-3 text-thin static-vertical"
                    ng-bind="$root.t('CostCalculator')"
                ></h2>
            <loading
                ng-if="$ctrl.costCalcIsLoading"
                class="stretch-vertical row-vertical-center"
                text="{{::$root.t('CostCalculator')}}"
                show-loading-prefix="true"
            ></loading>
            <div ng-if="!$ctrl.costCalcIsLoading">
                <div class="position-relative margin-bottom-4">
                    <form name="generalSettingsForm" novalidate>
                        <one-label-content
                            inline="true"
                            split="true"
                            name="{{::$root.t('CostCalculator')}}"
                            wrapper-class="margin-top-1 border-bottom"
                            name-description="{{::$root.t('CostCalculatorInfoText')}}"
                            >
                            <downgrade-switch-toggle
                                [is-disabled]="false"
                                [toggle-state]="$ctrl.isCostCalcEnabled"
                                (callback)="$ctrl.toggleCostCalcSetting()"
                            ></downgrade-switch-toggle>
                        </one-label-content>
                        <div ng-if="$ctrl.isCostCalcEnabled" class="row-nowrap padding-top-2">
                            <div class="width-30-percent padding-right-2">
                                <one-label-content
                                    inline="false"
                                    name="{{::$root.t('HourlyRate')}}"
                                    wrapper-class="margin-top-1"
                                    is-required="true"
                                    >
                                    <input
                                        name="hourlyInput"
                                        type="number"
                                        min="0"
                                        step="0.01"
                                        class="one-input full-width"
                                        ng-model="$ctrl.hourlyCost"
                                        ng-change="$ctrl.validateHourlyCost()"
                                    ></input>
                                </one-label-content>
                            </div>
                            <div class="width-70-percent">
                                <one-label-content
                                    inline="false"
                                    name="{{::$root.t('Currency')}}"
                                    wrapper-class="margin-top-1"
                                    is-required="true"
                                    >
                                    <one-select
                                        options="::$ctrl.currencyList"
                                        on-change="$ctrl.selectCurrency(selection)"
                                        label-key="countryName"
                                        value-key="countryCode"
                                        model="$ctrl.currencyIso"
                                        identifier="currencyDropdown">
                                    </one-select>
                                </one-label-content>
                            </div>
                        </div>
                    </form>
                </div>
                <footer class="margin-top-half row-flex-end static-horizontal padding-top-5">
                    <one-button
                        button-class="margin-right-1 no-outline"
                        button-click="$ctrl.saveCostCalcSettings()"
                        enable-loading="true"
                        identifier="saveCostCalculationSettings"
                        is-disabled="$ctrl.costCalcSaveInProgress || !$ctrl.costCalcIsDirty"
                        is-loading="$ctrl.costCalcSaveInProgress"
                        text="{{::$root.t('Save')}}"
                        type="primary">
                    </one-button>
                </footer>
            </div>
        </section>
        <section class="gray-shadow margin-all-3 padding-left-right-3 padding-top-bottom-2 column-nowrap background-white">
        <div>
            <h2
                class="header margin-top-0 margin-bottom-3 text-thin static-vertical"
                ng-bind="$root.t('RetentionPolicy')"
            ></h2>
        <loading
            ng-if="$ctrl.isAutoDelLoading"
            class="stretch-vertical row-vertical-center"
            text="{{::$root.t('RetentionPolicy')}}"
            show-loading-prefix="true"
        ></loading>
        <div ng-if="!$ctrl.isAutoDelLoading">
            <div class="position-relative margin-bottom-4">
                <form name="autoDelSettingsForm" novalidate>
                    <one-label-content
                        inline="true"
                        split="true"
                        name="{{::$root.t('AutoDeleteAttachments')}}"
                        wrapper-class="margin-top-1 border-bottom"
                        name-description="{{::$root.t('AutoDeleteAttachmentsInfoText')}}"
                        >
                        <downgrade-switch-toggle
                            [is-disabled]="false"
                            [toggle-state]="$ctrl.isAutoDeleteEnabled"
                            (callback)="$ctrl.toggleAutoDeleteSetting()"
                        ></downgrade-switch-toggle>
                    </one-label-content>
                    <div ng-if="$ctrl.isAutoDeleteEnabled" class="padding-top-2">
                        <one-label-content
                            inline="true"
                            split="true"
                            name="{{::$root.t('AutoDeleteCompletion')}}"
                            wrapper-class="margin-top-1 margin-left-3 padding-bottom-1 border-bottom"
                            >
                            <downgrade-switch-toggle
                                [is-disabled]="false"
                                [toggle-state]="$ctrl.isAutoDeleteOnCompletionEnabled"
                                (callback)="$ctrl.toggleAutoDeleteOnCompSetting()"
                            ></downgrade-switch-toggle>
                        </one-label-content>
                        <div ng-if="$ctrl.isAutoDeleteOnCompletionEnabled" class="padding-top-2">
                            <one-label-content
                                inline="true"
                                split="true"
                                name="{{::$root.t('DaysAfterCompletionDeleteAttachments')}}"
                                wrapper-class="margin-top-1 margin-left-6 padding-bottom-1 border-bottom row-space-between"
                                >
                                <input
                                    name="completionDayInput"
                                    type="number"
                                    min="1"
                                    step="1"
                                    class="one-input width-50-percent pull-right"
                                    ng-model="$ctrl.daysToDelete"
                                    ng-change="$ctrl.validateSchedulingDays()"
                                ></input>
                            </one-label-content>
                        </div>
                    </div>
                </form>
            </div>
            <footer class="margin-top-half row-flex-end static-horizontal padding-top-5">
                <one-button
                    button-class="margin-right-1 no-outline"
                    button-click="$ctrl.saveAutoDeleteSettings()"
                    enable-loading="true"
                    identifier="saveAutoDelSettings"
                    is-disabled="$ctrl.autoDelSaveInProgress || !$ctrl.isAutoDelDirty"
                    is-loading="$ctrl.autoDelSaveInProgress"
                    text="{{::$root.t('Save')}}"
                    type="primary">
                </one-button>
            </footer>
        </div>
    </section>
    <section class="gray-shadow margin-all-3 padding-left-right-3 padding-top-bottom-2 column-nowrap background-white">
        <div>
            <h2
                class="header margin-top-0 margin-bottom-3 text-thin static-vertical"
                ng-bind="$root.t('DataSubjectRequestPortalSettings')"
            ></h2>
            <loading
                ng-if="$ctrl.isPortalAccessLoading"
                class="stretch-vertical row-vertical-center"
                text="{{::$root.t('DataSubjectRequestPortalSettings')}}"
                show-loading-prefix="true"
            ></loading>
            <div ng-if="!$ctrl.isPortalAccessLoading">
                <div class="position-relative margin-bottom-4">
                    <form name="portalAccessSettingsForm" novalidate>
                        <one-label-content
                            inline="true"
                            split="true"
                            name="{{::$root.t('ExpirePortalAccess')}}"
                            wrapper-class="margin-top-1 border-bottom"
                            name-description="{{::$root.t('ExpirePortalAccessInfo')}}"
                            >
                            <downgrade-switch-toggle
                                ot-auto-id="DsRequestAccessEnabled"
                                [toggle-state]="$ctrl.isDsRequestAccessEnabled"
                                (callback)="$ctrl.togglePortalAccessSetting()"
                            ></downgrade-switch-toggle>
                        </one-label-content>
                        <div ng-if="$ctrl.isDsRequestAccessEnabled" class="padding-top-2">
                            <one-label-content
                                inline="true"
                                split="true"
                                name="{{::$root.t('DaysDataSubjectCanAccessRequest')}}"
                                wrapper-class="margin-top-1 margin-left-6 padding-bottom-1 border-bottom row-space-between"
                                >
                                <input
                                    name="revokeAccessInput"
                                    ot-auto-id="revokeAccessInput"
                                    type="number"
                                    min="1"
                                    step="1"
                                    class="one-input width-50-percent pull-right"
                                    ng-model="$ctrl.daysBeforeRevoke"
                                    ng-change="$ctrl.validateRevokeSchedulingDays()"
                                ></input>
                            </one-label-content>
                        </div>
                    </form>
                </div>
                <footer class="margin-top-half row-flex-end static-horizontal padding-top-5">
                    <one-button
                        button-class="margin-right-1 no-outline"
                        button-click="$ctrl.savePortalAccessSettings()"
                        enable-loading="true"
                        identifier="savePortalAccessSettings"
                        is-disabled="$ctrl.portalAccessSaveInProgress || !$ctrl.isPortalAccessDirty"
                        is-loading="$ctrl.portalAccessSaveInProgress"
                        text="{{::$root.t('Save')}}"
                        type="primary">
                    </one-button>
                </footer>
            </div>
        </div>
    </section>
    `,
};
