
import { DSARSettingsOptions } from "dsarModule/constants/dsar.constant";
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import { ITabsNav } from "interfaces/one-tabs-nav.interface";
import { IStringMap } from "interfaces/generic.interface";

interface ITabEnums {
    Responses: string;
    General: string;
    Workflows: string;
    AutomationRules: string;
}

class DSARSettingsController {
    static $inject: string[] = [
        "$rootScope",
    ];

    settingsTabOptions: IStringMap<string> = DSARSettingsOptions;

    private ready = false;
    private currentTab: string;
    private tabOptions: ITabsNav[] = [];

    constructor(
        private $rootScope: IExtendedRootScopeService,
    ) {}

    public $onInit(): void {
        this.ready = true;
        this.tabOptions = this.getTabOptions();
        this.currentTab = this.tabOptions[0].id;
    }

    private getTabOptions = (): ITabsNav[] => {
        return [
            {
                id: this.settingsTabOptions.General,
                text: this.$rootScope.t("General"),
                action: (option: ITabsNav): void => this.changeTab(option),
            },
        ];
    }

    private changeTab(option: ITabsNav): void {
        if (this.currentTab !== option.id) {
            this.currentTab = option.id;
        }
    }
}

export const DSARSettingsComponent: any = {
    controller: DSARSettingsController,
    template: `
        <section class="flex-full-height height-100">
            <one-header>
                <header-title>
                    <span>{{::$root.t("DSARSettings")}}</span>
                </header-title>
            </one-header>
            <div class="flex-full-height height-100" ng-if="$ctrl.ready">
                <one-tabs-nav
                    class="padding-top-2 border-bottom"
                    ng-if="$ctrl.tabOptions.length > 1"
                    selected-tab="$ctrl.currentTab",
                    options="$ctrl.tabOptions"
                ></one-tabs-nav>
                <dsar-settings-general
                    ng-if="$ctrl.currentTab === $ctrl.settingsTabOptions.General"
                    class="overflow-y-scroll"></dsar-settings-general>
            </div>
        </section>
        `,
};
