// Angular
import {
    Component,
    Input,
    EventEmitter,
    Output,
} from "@angular/core";

@Component({
    selector: "webform-edit-block",
    templateUrl: "./webform-edit-block.component.html",
})
export class WebformEditBlockComponent {

    /**
     * @description Provides the permission to edit text.
     */
    @Input() hasEditAccess: boolean;

    /**
     * @description Text to display/edit.
     */
    @Input() text: string;

    /**
     * @description Sets the limit of length of the text the user can input.
     */
    @Input() maxTextLength: number;

    /**
     * @description Adds css class for the text.
     */
    @Input() textStyle: string;

    /**
     * @description Emits the edited text to parent component.
     */
    @Output() inputChanged = new EventEmitter<string>();

    editModeEnabled = false;

    /**
     * @param {string} value
     * @description Updates the text property with the current value.
     */
    changeModel(value: string): void {
        if (value && value.length) {
            this.text = value;
        }
    }

    /**
     * @param {string} value
     * @description Shortens the text and saves it.
     */
    saveModel(value: string): void {
        value = this.shortenText(value);
        this.changeModel(value);
        this.editModeEnabled = false;
    }

    /**
     * @param {string} text
     * @returns {string} Accepts the string, returns the shortened version.
     * @description Trim the text value to be emitted.
     */
    shortenText(text: string): string {
        let maxLength = 23;
        if (this.maxTextLength) {
            maxLength = this.maxTextLength;
        }
        text = text.length > maxLength ? text.slice(0, maxLength) : text;
        return text;
    }

    /**
     * @param {string} text
     * @description Shortens the text and emits it to parent component.
     */
    inputChangedCallback(text: string): void {
        if (!text) return;
        text = this.shortenText(text);
        this.inputChanged.emit(text);
    }
}
