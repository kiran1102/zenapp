// Angular
import {
    Component,
    Inject,
    OnInit,
} from "@angular/core";
import {
    map,
    every,
    findIndex,
    find,
    filter,
    forEach,
    orderBy,
    cloneDeep,
} from "lodash";
import { getModalData } from "oneRedux/reducers/modal.reducer";
import { StoreToken } from "tokens/redux-store.token";

import { ModalService } from "sharedServices/modal.service";
import { TranslatePipe } from "pipes/translate.pipe";

import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";
import { IStore } from "interfaces/redux.interface";
import {
    IGetAllLanguageResponse,
    ISetLanguageTableData,
    IGetWebformLanguageResponse,
    ISetLanguagePutData,
    ISetLanguageModalData,
    ISetLanguageRadio,
    Status,
} from "interfaces/dsar/dsar-edit-language.interface";

@Component({
    selector: "webform-edit-language-body",
    templateUrl: "./webform-edit-language.component.html",
})
export class WebformEditLanguageComponent implements OnInit {
    public sampleColumns;
    public allRowSelected = false;
    public truncateCellContent = true;
    public rowBordered = true;
    public sampleTableData: ISetLanguageTableData[];
    public cloneSampleTableData: ISetLanguageTableData[];
    public loadingTemplate = true;
    public saveDisabled = true;
    public translations;
    public saveLoading = false;
    public radioID: ISetLanguageRadio;
    public defaultRadioID: ISetLanguageRadio;
    public defaultLanguage: string;
    public singleLanguageEnabled: boolean;
    public readonly overflowLength = 8;
    promiseToResolve: (changes: ISetLanguagePutData[], defaultLanguage?: string) => ng.IPromise<boolean>;
    private modalData: ISetLanguageModalData;

    constructor(
        @Inject(StoreToken) private store: IStore,
        private readonly modalService: ModalService,
        private translatePipe: TranslatePipe,
    ) {
        this.translations = {
            save: this.translatePipe.transform("Save"),
            cancel: this.translatePipe.transform("Cancel"),
            success: this.translatePipe.transform("EditLanguageSuccess"),
            failure: this.translatePipe.transform("EditLanguageFailure"),
            selectAll: this.translatePipe.transform("SelectAll"),
            default: this.translatePipe.transform("Default"),
            noLanguageFound: this.translatePipe.transform("NoLanguageFound"),
            search: this.translatePipe.transform("Search") + "...",
        };
    }

    ngOnInit(): void {
        this.sampleColumns = [{
            columnType: null,
            columnName: this.translations.selectAll,
            columnWidth: null,
            cellValueKey: "textdata",
        },
        {
            columnName: "Select Row",
            columnType: "checkbox",
        },
        {
            columnName: this.translations.default,
            columnType: "radio",
        },
        ];
        this.modalData = getModalData(this.store.getState());
        this.translations.headerTitle = this.modalData.headerTitle;
        this.translations.bodyText = this.modalData.bodyText;
        this.promiseToResolve = this.modalData.promiseToResolve;
        if (this.modalData.promiseArray) {
            this.modalData.promiseArray().then((res: [IProtocolResponse<IGetWebformLanguageResponse[]>, IProtocolResponse<IGetAllLanguageResponse[]>]) => {
                this.modalData.webformLanguagesList = res[0].data;
                this.modalData.masterLanguagesList = res[1].data;
                this.afterLoadInit();
            });
        } else {
            this.afterLoadInit();
        }
    }

    public afterLoadInit() {
        this.defaultLanguage = find(this.modalData.webformLanguagesList, "IsDefault").Code;
        this.sampleTableData = this.buildLanguageData(this.modalData.masterLanguagesList, this.modalData.webformLanguagesList);
        this.sampleTableData = orderBy(this.sampleTableData, ["enabled", "disabled"], ["desc", "asc"]);
        this.cloneSampleTableData = cloneDeep(this.sampleTableData);
        this.defaultRadioID = {
            id: find( this.sampleTableData, ["isDefault", true]).id,
            code: this.defaultLanguage,
         };
        this.radioID = {...this.defaultRadioID};
        this.allRowSelected = every(this.sampleTableData, "enabled");
        this.singleLanguageEnabled = filter(this.sampleTableData, ["enabled", true]).length === 1;
        this.loadingTemplate = false;
    }

    public buildLanguageData(masterLanguagesList: any, webformLanguagesList: any): ISetLanguageTableData[] {
        return masterLanguagesList.map((language: any) => {
            const intIndex: number = findIndex(webformLanguagesList, ["Code", language.Code]);
            if (intIndex >= 0) {
                language.enabled = webformLanguagesList[intIndex].Status === 10 ;
            } else {
                language.enabled = false;
            }
            return {
                textdata: language.Name,
                id: language.Id,
                enabled: language.enabled,
                code: language.Code,
                isDefault: language.Code === this.defaultLanguage,
                dirty: language.enabled,
            };
        });
    }

    public onSearch(value: string) {
        this.updateCloneTableData();
        this.sampleTableData = filter(this.cloneSampleTableData, (lang: ISetLanguageTableData): boolean => {
            return lang.textdata.toLowerCase().indexOf(value.toLowerCase()) === 0;
        });
        this.sampleTableData = orderBy(this.sampleTableData, ["enabled", "disabled"], ["desc", "asc"]);
        this.checkForMutations();
    }

    public handleSelect(radio: ISetLanguageRadio): void {
        this.radioID = radio;
        this.defaultRadioID = {...this.radioID};
        find(this.cloneSampleTableData, "isDefault").isDefault = false;
        this.sampleTableData.forEach((elem: ISetLanguageTableData) => {
           elem.isDefault = false;
        });
        const selectedIndex = findIndex(this.sampleTableData, ["code", this.radioID.code]);
        this.sampleTableData[selectedIndex].enabled = true;
        this.sampleTableData[selectedIndex].isDefault = true;
        this.sampleTableData[selectedIndex].dirty = true;
        this.checkForMutations();
    }

    public handleTableSelectClick(actionType: string, code: number): boolean {
        const defaultIndex: number = findIndex(this.sampleTableData, ["code", this.radioID.code]);
        const selectedIndex: number = findIndex(this.sampleTableData, ["code", code]);
        switch (actionType) {
            case "header":
                this.allRowSelected = !this.allRowSelected;
                this.sampleTableData.forEach((elem: ISetLanguageTableData, index: number) => {
                    if (index !== defaultIndex) {
                        elem.enabled = this.allRowSelected;
                    }
                    elem.dirty = true;
                    });
                this.checkForMutations();
                break;
            case "row":
                if (selectedIndex !== defaultIndex) {
                    this.sampleTableData[selectedIndex].enabled = !this.sampleTableData[selectedIndex].enabled;
                    this.sampleTableData[selectedIndex].dirty = true;
                    this.checkForMutations();
                    } else {
                    this.sampleTableData[selectedIndex].enabled = this.sampleTableData[selectedIndex].enabled;
                    return false;
                }
                break;
        }
        return true;
    }

    public checkForMutations(): void {
        this.allRowSelected = every(this.sampleTableData, "enabled");
        this.singleLanguageEnabled = filter(this.sampleTableData, ["enabled", true]).length === 1;
        this.saveDisabled = false;
    }

    public onSave(): void {
        this.saveLoading = true;
        this.updateCloneTableData();
        const changes: ISetLanguagePutData[] = this.buildResponseData();
        this.promiseToResolve(changes, this.radioID.code).
            then((res: boolean) => {
                if (res) this.closeModal();
                this.saveLoading = false;
            });
    }

    public closeModal(): void {
        this.modalService.closeModal();
        this.modalService.clearModalData();
    }

    private updateCloneTableData() {
        forEach(filter(this.sampleTableData, "dirty"), (lang: ISetLanguageTableData): void => {
            const langObject = find(this.cloneSampleTableData, ["textdata", lang.textdata]);
            langObject.enabled = lang.enabled;
            langObject.isDefault = lang.isDefault;
            langObject.dirty = true;
        });
    }

    private buildResponseData(): ISetLanguagePutData[] {
        const tableData = filter(filter(this.cloneSampleTableData, "dirty"), (obj: ISetLanguageTableData): boolean => {
            return find(this.modalData.webformLanguagesList, ["Code", obj.code]) || (obj.enabled);
        });
        const changes: ISetLanguagePutData[] = map(tableData, (obj: ISetLanguageTableData) => {
                return {
                    id: obj.id,
                    name: obj.textdata,
                    status: obj.enabled ? Status.Enabled : Status.Disabled,
                    code: obj.code,
                    isDefault: obj.isDefault,
                };
        });
        return changes;
    }
}
