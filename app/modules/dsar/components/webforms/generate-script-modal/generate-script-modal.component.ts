// Angular
import {
    Component,
    Inject,
    OnInit,
} from "@angular/core";

// Redux
import { getModalData } from "oneRedux/reducers/modal.reducer";
import { StoreToken } from "tokens/redux-store.token";

// Interfaces
import { IStore } from "interfaces/redux.interface";
import { IGenerateScriptModal } from "dsarModule/interfaces/webform.interface";

// 3rd Party Library
import Utilities from "Utilities";

// services
import { ModalService } from "sharedServices/modal.service";
import { Permissions } from "modules/shared/services/helper/permissions.service";
import { NotificationService } from "modules/shared/services/provider/notification.service";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

@Component({
    selector: "webform-generate-script-modal",
    templateUrl: "./generate-script-modal.component.html",
})
export class GenerateScriptModal implements OnInit {
    hrefString: string;
    anchorScript: string;
    liveLink: string;
    modalData: IGenerateScriptModal;
    constructor(
        @Inject(StoreToken) private store: IStore,
        private readonly modalService: ModalService,
        readonly permissions: Permissions,
        private readonly translatePipe: TranslatePipe,
        private notificationService: NotificationService,
    ) {
    }

    ngOnInit() {
        this.modalData = getModalData(this.store.getState());
        if (this.permissions.canShow("DSARWebFormPublishV2") && !this.permissions.canShow("DSARWebFormLegacy")) {
            if (this.modalData.publishedPath) {
                this.liveLink = this.modalData.publishedPath.replace("_Draft", "");
                this.hrefString = `${this.modalData.publishedHost}${this.liveLink}`;
                this.anchorScript = `<a target="blank" href="${this.hrefString}">${this.translatePipe.transform("WebForm")}</a>`;
            } else {
                this.anchorScript = this.translatePipe.transform("DSARPublishFormRequest");
            }
        } else {
            this.hrefString = `${window.location.protocol}//${window.location.host}/app/#/webform/${this.modalData.webFormId}`;
            this.anchorScript = `<a target="blank" href="${this.hrefString}">${this.translatePipe.transform("WebForm")}</a>`;
        }
    }

    generateScript(): void {
        Utilities.copyToClipboard(this.anchorScript);
        this.notificationService.alertNote(this.translatePipe.transform("WebFormLinkCopied"), null);
    }

    closeModal(): void {
        this.modalService.closeModal();
        this.modalService.clearModalData();
    }

}
