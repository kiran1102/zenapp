// 3rd party
import {
    Component,
    Input,
    OnInit,
    Output,
    OnDestroy,
    EventEmitter,
} from "@angular/core";
import {
    find,
    sortBy,
} from "lodash";
import {
    Observable,
    Subject,
} from "rxjs";
import {
    debounceTime,
    takeUntil,
} from "rxjs/operators";

// Interfaces
import {
    IWebFormSettingsInput,
    IWebformSettings,
    IWebformDetailRequest,
} from "dsarModule/interfaces/webform.interface";
import {
    IWebformSettingsMap,
    IWebformAccordionMeta,
} from "dsarModule/interfaces/webform-settings.interface";
import {
    IPublishedWorkflow,
} from "interfaces/workflows.interface";
import { ISettingsMeta } from "sharedModules/interfaces/settings.interface";
import { ILabelValue } from "interfaces/generic.interface";
import {
    ISetLanguageModalData,
    ISetLanguagePutData,
    IGetWebformLanguageResponse,
} from "interfaces/dsar/dsar-edit-language.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";
import { IPromise } from "angular";

interface IEventTarget extends EventTarget {
    value: any;
}
interface IWebformEvent extends Event {
    actionType?: string;
    minValue?: number;
    maxValue?: number;
    target: IEventTarget;
}

// Constants
import {
    DSARStoreActions,
    DSARWebformAccordionSection,
    DSARWebformSettingsLabel,
} from "dsarModule/enums/dsar-webform.enum";
import { SettingsType } from "sharedModules/enums/settings.enum";
import { OrgUserTraversal } from "enums/org-user-dropdown.enum";
import { EmptyGuid } from "constants/empty-guid.constant";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";
import { InputNumberPipe } from "pipes/input-number.pipe";

// Services
import { ModalService } from "sharedServices/modal.service";
import { WebformApiService } from "dsarservice/webform-api.service";
import { Permissions } from "modules/shared/services/helper/permissions.service";

@Component({
    selector: "dsar-webform-settings",
    templateUrl: "./dsar-webform-settings.component.html",
})
export class DSARWebformSettingsComponent implements OnInit, OnDestroy {
    @Input() webformSettings: IWebformSettings;
    @Input() webform;
    @Input() savePromise: () => ng.IPromise<boolean>;
    @Output() actionCallback = new EventEmitter<{
        type: number,
        payload: any,
    }>();
    settings: IWebformSettingsMap = {
        AllowDsPortalAttachment: false,
        AttachmentSubmission: false,
        BccEmail: "",
        DefaultDaysToRespond: 0,
        DefaultReminder: 0,
        DefaultReviewer: {
            Id: EmptyGuid,
            FullName: "- - - -",
        },
        EmailVerification: false,
        EnableReCaptcha: true,
        EnableRequestTypes: false,
        EnableSubjectTypes: false,
        ExpiryRequest: false,
        ExpiryRequestDays: 1,
        IsLogoEnabled: false,
        MultiCaptcha: null,
        Organization: "",
        OrgGroupId: "",
        RequireAttachment: false,
        TranslateWebForm: false,
        Workflows: {
            referenceId: "",
            workflowName: "",
        },
        TemplateName: "",
    };
    accordionMeta: IWebformAccordionMeta[];
    captchaOptions: Array<ILabelValue<string>>;
    dsarWebformAccordionSection = DSARWebformAccordionSection;
    dsportalSettingMeta: ISettingsMeta[];
    languageSettingMeta: ISettingsMeta[];
    loadingSave = false;
    settingsDirty = false;
    webformNameExists = true;
    webformGeneralSetting: IWebformDetailRequest;
    webformGeneralSettingMeta: ISettingsMeta[];
    webformSubmissionMeta: ISettingsMeta[];
    inputObservable$: Observable<any>;
    orgEmpty = false;
    dsarWebformSettingsLabel = DSARWebformSettingsLabel;

    private contentSource = new Subject<IWebformEvent>();
    private destroy$ = new Subject();

    constructor(
        private readonly translatePipe: TranslatePipe,
        private readonly inputNumber: InputNumberPipe,
        readonly modalService: ModalService,
        private dsarWebformService: WebformApiService,
        readonly permissions: Permissions,
    ) {}

    public ngOnInit(): void {
        this.inputObservable$ = this.contentSource.asObservable();
        this.inputObservable$.pipe(
                debounceTime(1000),
                takeUntil(this.destroy$),
            ).subscribe((input: IWebformEvent) => {
                this.settings[input.actionType] = this.inputNumber.transform(input.target.value, input.minValue, input.maxValue);
                this.settingsDirty = true;
                if (input.actionType === this.dsarWebformSettingsLabel.DefaultDaysToRespond) {
                    if (this.settings.DefaultReminder > this.settings.DefaultDaysToRespond) {
                        this.settings.DefaultReminder = this.settings.DefaultDaysToRespond;
                    }
                    this.webformGeneralSettingMeta.find(
                            (setting) => setting.actionType === this.dsarWebformSettingsLabel.DefaultReminder,
                        ).maxValue = this.settings.DefaultDaysToRespond;
                }
                this.actionCallback.emit({
                    type: DSARStoreActions.STORE_SETTINGS,
                    payload: {
                        settings: this.settings,
                        dirty: this.settingsDirty,
                    },
                });
            });
        this.webformSettings.data.forEach((setting: IWebFormSettingsInput) => {
            if (setting.fieldName === DSARWebformSettingsLabel.AttachmentSubmission) {
                this.settings.RequireAttachment = setting.isRequired;
                this.settings.AttachmentSubmission = setting.value;
            } else if (setting.fieldName === DSARWebformSettingsLabel.MultiCaptcha) {
                this.captchaOptions = this.getCaptchaOptions(["Google reCAPTCHA", "BotDetect CAPTCHA"], setting.options);
                this.settings[setting.fieldName] = this.captchaOptions.find(
                    (option: ILabelValue<string>) => option.value === setting.value);
            } else {
                this.settings[setting.fieldName] = setting.value;
            }
        });
        if (this.settings.DefaultReviewer.Id === EmptyGuid) {
            this.settings.DefaultReviewer.Id = null;
        }
        const sortedWorkflows = sortBy(this.webformSettings.publishWorkflowDtos, (workflow: IPublishedWorkflow) => workflow.workflowName.toLowerCase());
        this.settings.Organization = this.webformSettings.orgGroupId;
        this.settings.Workflows = find(this.webformSettings.publishWorkflowDtos, ["referenceId", this.webformSettings.workflowRefId]);
        this.settings.TemplateName = this.webform.templateName;
        this.accordionMeta = [
            {
                title: DSARWebformAccordionSection.General,
                open: true,
            },
            {
                title: DSARWebformAccordionSection.LanguageSettings,
                open: true,
            },
            {
                title: DSARWebformAccordionSection.WebFormSubmission,
                open: true,
            }, {
                title: DSARWebformAccordionSection.DataSubjectPortal,
                open: true,
            },
        ];
        this.webformGeneralSettingMeta = [
            {
                actionType: DSARWebformSettingsLabel.TemplateName,
                class: "margin-bottom-2",
                required: true,
                title: DSARWebformSettingsLabel.WebFormName,
                type: SettingsType.INPUT,
                value: this.settings.TemplateName,
                dirty: true,
                maxLength: 30,
            },
            {
                actionType: DSARWebformSettingsLabel.Workflows,
                class: "margin-bottom-2",
                labelKey: "workflowName",
                options: sortedWorkflows,
                required: true,
                title: DSARWebformSettingsLabel.DefaultWorkflow,
                type: SettingsType.LOOKUP,
                valueKey: "referenceId",
            },
            {
                actionType: DSARWebformSettingsLabel.Organization,
                class: "margin-bottom-2",
                orgSelectChoosesUserOrg: true,
                required: true,
                title: DSARWebformSettingsLabel.DefaultOrganization,
                type: SettingsType.ORGANIZATION,
                orgRequired: true,
            },
            {
                actionType: DSARWebformSettingsLabel.DefaultReviewer,
                class: "margin-bottom-2",
                orgSelectChoosesUserOrg: true,
                orgUserOrgId: this.webformSettings.orgGroupId,
                orgUserTraversal: OrgUserTraversal.Up,
                title: DSARWebformSettingsLabel.DefaultApprover,
                type: SettingsType.APPROVER,
                allowEmail: false,
            },
            {
                actionType: DSARWebformSettingsLabel.DefaultDaysToRespond,
                class: "margin-bottom-2",
                maxValue: 365,
                minValue: 1,
                stepValue: 1,
                title: DSARWebformSettingsLabel.DefaultDaysToRespond,
                type: SettingsType.NUMBER,
                value: this.settings.DefaultDaysToRespond,
            }, {
                actionType: DSARWebformSettingsLabel.DefaultReminder,
                class: "margin-bottom-2",
                maxValue: this.settings.DefaultDaysToRespond,
                minValue: 1,
                stepValue: 1,
                title: DSARWebformSettingsLabel.DefaultReminder,
                type: SettingsType.NUMBER,
            },
        ];
        this.webformSubmissionMeta = [
            {
                title: DSARWebformSettingsLabel.RequireCaptchaValidation,
                type: SettingsType.TOGGLE,
                value: this.settings.EnableReCaptcha,
                actionType: DSARWebformSettingsLabel.EnableReCaptcha,
                isVisible: true,
                children: [
                    {
                        actionType: DSARWebformSettingsLabel.MultiCaptcha,
                        labelKey: "label",
                        model: this.settings.MultiCaptcha,
                        options: this.captchaOptions,
                        title: DSARWebformSettingsLabel.CaptchaService,
                        type: SettingsType.PICKLIST,
                        value: this.settings.MultiCaptcha,
                        valueKey: "value",
                        visibleString: DSARWebformSettingsLabel.EnableReCaptcha,
                    },
                    {
                        actionType: DSARWebformSettingsLabel.AttachmentSubmission,
                        title: DSARWebformSettingsLabel.EnableAttachmentUpload,
                        type: SettingsType.TOGGLE,
                        value: this.settings.AttachmentSubmission,
                        visibleString: DSARWebformSettingsLabel.EnableReCaptcha,
                        children: [{
                            actionType: DSARWebformSettingsLabel.RequireAttachment,
                            title: DSARWebformSettingsLabel.RequireAttachment,
                            type: SettingsType.TOGGLE,
                            value: this.settings.RequireAttachment,
                            visibleString: DSARWebformSettingsLabel.AttachmentSubmission,
                        }],
                    },
                ],
            },
            {
                actionType: DSARWebformSettingsLabel.EmailVerification,
                description: DSARWebformSettingsLabel.EnableEmailVerificationSubtext,
                isVisible: true,
                title: DSARWebformSettingsLabel.EnableEmailVerification,
                type: SettingsType.TOGGLE,
                value: this.settings.EmailVerification,
                children : [{
                    actionType: DSARWebformSettingsLabel.ExpiryRequest,
                    description: DSARWebformSettingsLabel.ExpireRequestSubtext,
                    title: DSARWebformSettingsLabel.ExpireRequests,
                    type: SettingsType.TOGGLE,
                    value: this.settings.ExpiryRequest,
                    visibleString: "EmailVerification",
                    children: [{
                        actionType: DSARWebformSettingsLabel.ExpiryRequestDays,
                        description: DSARWebformSettingsLabel.ExpiryDaysSubtext,
                        maxValue: 7,
                        minValue: 1,
                        stepValue: 1,
                        title: DSARWebformSettingsLabel.ExpiryDaysLabel,
                        type: SettingsType.NUMBER,
                        value: this.settings.ExpiryRequestDays,
                        visibleString: "ExpiryRequest",
                    }],
                }],
            },
        ];
        this.dsportalSettingMeta = [{
            actionType: DSARWebformSettingsLabel.AllowDsPortalAttachment,
            description: DSARWebformSettingsLabel.AllowDsPortalAttachment,
            isVisible: true,
            title: DSARWebformSettingsLabel.AllowAttachmentUpload,
            type: SettingsType.TOGGLE,
            value: this.settings.AllowDsPortalAttachment,
        }];
        this.languageSettingMeta = [{
            actionType: DSARWebformSettingsLabel.TranslateWebForm,
            description: "",
            isVisible: true,
            linkText: "EditLanguages",
            linkType: DSARWebformSettingsLabel.OpenLanguageModal,
            showLink: true,
            title: DSARWebformSettingsLabel.EnableLanguagePreference,
            type: SettingsType.TOGGLE,
            value: this.settings.TranslateWebForm,
        }];
    }

    getCaptchaOptions(labelArray: string[], valueArray: string[]): Array<ILabelValue<string>> {
        return labelArray.map((label: string, index: number) => {
            return {
                label,
                value: valueArray[index],
            };
        });
    }

    ngOnDestroy() {
        this.destroy$.next();
        this.destroy$.complete();
    }

    handleAction(action) {
        let eventData;
        switch (action.type) {
            case DSARWebformSettingsLabel.EmailVerification:
                this.settings.EmailVerification = !this.settings.EmailVerification;
                if (!this.settings.EmailVerification) {
                    this.settings.ExpiryRequest = false;
                    this.settings.ExpiryRequestDays = 1;
                }
                break;
            case DSARWebformSettingsLabel.ExpiryRequest:
                this.settings.ExpiryRequest = !this.settings.ExpiryRequest;
                if (!this.settings.ExpiryRequest) {
                    this.settings.ExpiryRequestDays = 1;
                }
                break;
            case DSARWebformSettingsLabel.ExpiryRequestDays:
                eventData = {...action};
                eventData.payload.event.actionType = action.type;
                if (eventData.payload.event) {
                    this.contentSource.next(eventData.payload.event);
                }
                break;
            case DSARWebformSettingsLabel.Organization:
                this.settings[action.type] = action.payload;
                if (action.payload) {
                    this.orgEmpty = false;
                } else {
                    this.orgEmpty = true;
                    return;
                }
                break;
            case DSARWebformSettingsLabel.Workflows:
                this.settings[action.type] = action.payload.currentValue;
                break;
            case DSARWebformSettingsLabel.MultiCaptcha:
                this.settings[action.type] = action.payload.event;
                break;
            case DSARWebformSettingsLabel.DefaultReviewer:
                this.settings.DefaultReviewer = action.payload.selection;
                break;
            case DSARWebformSettingsLabel.DefaultDaysToRespond:
            case DSARWebformSettingsLabel.DefaultReminder:
                eventData = {...action};
                eventData.payload.actionType = action.type;
                if (eventData.payload) {
                    this.contentSource.next(eventData.payload);
                }
                break;
            case DSARWebformSettingsLabel.EnableReCaptcha:
                this.settings.EnableReCaptcha = !this.settings.EnableReCaptcha;
                if (!this.settings.EnableReCaptcha) {
                    this.settings.RequireAttachment = false;
                    this.settings.AttachmentSubmission = false;
                }
                break;
            case DSARWebformSettingsLabel.AllowDsPortalAttachment:
            case DSARWebformSettingsLabel.AttachmentSubmission:
            case DSARWebformSettingsLabel.RequireAttachment:
            case DSARWebformSettingsLabel.TranslateWebForm:
                this.settings[action.type] = !this.settings[action.type];
                break;
            case DSARWebformSettingsLabel.TemplateName:
                if (action.payload.target.value.trim()) {
                    this.settings.TemplateName = action.payload.target.value.trim();
                    this.webformNameExists = Boolean(this.settings.TemplateName);
                } else {
                    this.webformNameExists = false;
                    this.settingsDirty = false;
                    this.actionCallback.emit({
                        type: DSARStoreActions.STORE_SETTINGS,
                        payload: {
                            settings: this.settings,
                            dirty: false,
                        },
                    });
                    return;
                }
                break;
            case DSARWebformSettingsLabel.OpenLanguageModal:
                this.openLanguageModal();
                return;
            default:
               break;
        }
        this.settingsDirty = Boolean(this.settings.Workflows);
        this.actionCallback.emit({
            type: DSARStoreActions.STORE_SETTINGS,
            payload: {
                settings: this.settings,
                dirty: this.settingsDirty,
            },
        });
    }

    saveSettings() {
        this.loadingSave = true;
        this.savePromise().then((result: boolean) => {
            if (result) {
                this.settingsDirty = false;
            }
            this.loadingSave = false;
        });
    }

    trackByFn(index: number, setting: ISettingsMeta): string {
        return setting.title;
    }

    private openLanguageModal(): void {
        const setLanguageData: ISetLanguageModalData = {
            masterLanguagesList: this.webform.masterLanguagesList,
            webformLanguagesList: this.webform.webformLanguagesList,
            templateId: this.webform.templateId,
            headerTitle: this.translatePipe.transform("WebFormLanguages"),
            bodyText: this.translatePipe.transform("WebformSetLanguageBodyText"),
            promiseToResolve: this.onLanguageSave.bind(this),
            promiseArray: null,
        };
        this.modalService.setModalData(setLanguageData);
        this.modalService.openModal("webformEditLanguageBody");
    }

    private onLanguageSave(changes: ISetLanguagePutData[], defaultLanguage: string): IPromise<boolean> {
        if (this.permissions.canShow("DSARWebFormLanguageStatus")) {
            return this.dsarWebformService.putLanguagePreferencesData(changes, this.webform.templateId).
                then((response: IProtocolResponse<IGetWebformLanguageResponse[]>) => {
                if (response.result) {
                    this.actionCallback.emit({
                        type: DSARStoreActions.LANG_PREFERENCE_SWITCH,
                        payload: {
                            languageList: response.data,
                            defaultLanguage,
                        },
                    });
                    this.actionCallback.emit({
                        type: DSARStoreActions.FETCHING_DATA,
                        payload: {
                            published: false,
                        },
                    });
                    return true;
                }
                return false;
            });
        }
    }
}
