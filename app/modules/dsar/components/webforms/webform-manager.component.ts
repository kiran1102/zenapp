// 3rd Party
import {
    isUndefined,
    find,
} from "lodash";

// Services
import { DsarWebformRulesApiService } from "dsarservice/dsar-webform-rules-api.service";
import { WebformApiService } from "dsarservice/webform-api.service";
import { OneRulesHelperService } from "modules/dsar/components/webforms/shared/services/one-rules-helper.service";
import { NotificationService } from "modules/shared/services/provider/notification.service";
import { Content } from "sharedModules/services/provider/content.service";
import { WebformStore } from "modules/dsar/webform.store";

// Interfaces
import { IStringMap } from "interfaces/generic.interface";
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";
import {
    IWebformSettings,
    IWebformAction,
    IWebFormSettingsInput,
    IFormAction,
} from "dsarModule/interfaces/webform.interface";
import { IPermissions } from "modules/shared/interfaces/permissions.interface";
import { ITabsMeta } from "interfaces/one-tabs-nav.interface";
import { Permissions } from "modules/shared/services/helper/permissions.service";
import {
    IRuleMeta,
    IRule,
    IRuleValidate,
} from "modules/dsar/components/webforms/shared/interfaces/one-rules.interface";
import { IWebformModel } from "@onetrust/dsar-components";

// Enums & Constants
import {
    WebformTabs,
    DSARStoreActions,
    DSARWebformSettingsLabel,
} from "dsarModule/enums/dsar-webform.enum";
import { EmptyGuid } from "constants/empty-guid.constant";
import { DSARSettings, DSARCaptchaType } from "modules/dsar/constants/dsar-settings.constant";

declare var OneConfirm: any;

class WebFormManagerController implements ng.IComponentController {
    static $inject: string[] = [
        "webformStore",
        "$state",
        "$rootScope",
        "$transitions",
        "Permissions",
        "webformApiService",
        "dsarRulesServiceDowngraded",
        "oneRulesHelperService",
        "NotificationService",
        "Content",
    ];

    readonly WebformTabs = WebformTabs;

    currentTab: number;
    tabOptions: Array<ITabsMeta<WebformTabs>> = [
        {
            text: this.$rootScope.t("WebForm"),
            id: WebformTabs.WebForm,
            action: (option: ITabsMeta<WebformTabs>): void => {
                this.changeTab(option);
                },
        },
        {
            text: this.$rootScope.t("Settings"),
            id: WebformTabs.Settings,
            action: (option: ITabsMeta<WebformTabs>): void => {
                this.changeTab(option);
            },
        },
    ];
    dsarStoreActions = DSARStoreActions;
    ready = false;
    actionsDropdown = false;
    webForm: any; // TODO enforce typing in webform store
    webformMeta: any; // TODO enforce typing in webform store
    webformModel: IWebformModel;
    permissions: IPermissions = {};
    actions: IWebformAction[];
    savingInProgress: boolean;
    changesMadeToForm: boolean;
    changesMadeToSettings: boolean;
    changesMadeToRules: boolean;
    haveRequestTypes: boolean;
    editSidebarEnabled: boolean;
    selectedEditInput: string;
    errorOnForm: boolean;
    formTranslations: IStringMap<string>;
    webformId: string;
    published: boolean;
    webFormPublishStatus: string;
    publishStatusBadgeClass: string;
    webFormInProgress: boolean;
    widgetId: number;
    saveTemplateButtonType: string;
    isPreviewGenerateDisabled: boolean;
    loadingWebform = false;
    loadingSettings = false;
    settings = [];
    savingSettings = false;
    savingRules = false;
    showNewWebForm = false;
    configureFormFieldType = false;
    configureFormField = false;
    selectedFormField = null;
    defaultLogo: string;
    editFormField = false;
    rulesDirty = false;

    private isInitialized = false;
    private unsubscribeFn: any; // TODO enforce typing in webform store
    private transitionDeregisterHook: () => {};
    private publishedHost: string;
    private publishedPath: string;
    private hrefString: string;

    constructor(
        private dsarWebFormStore: WebformStore,
        private $state: ng.ui.IStateService,
        private $rootScope: IExtendedRootScopeService,
        private $transitions: any,
        private permissionsService: Permissions,
        private webformApiService: WebformApiService,
        private dsarWebformRulesApiService: DsarWebformRulesApiService,
        private oneRulesHelperService: OneRulesHelperService,
        private notificationService: NotificationService,
        private content: Content,
    ) {}

    $onInit() {
        this.showNewWebForm = this.permissionsService.canShow("DSARWebFormPublishV2");
        this.currentTab = this.$state.params.webformTab;
        if (this.permissionsService.canShow("DSARWebFormRuleEdit")) {
            this.tabOptions.splice(1, 0, {
                text: this.$rootScope.t("Rules"),
                id: WebformTabs.Rules,
                action: (option: ITabsMeta<WebformTabs>): void => {
                    this.changeTab(option);
                },
            });
        }
        this.unsubscribeFn = this.dsarWebFormStore.subscribe(this.getState.bind(this));
        this.webformId = this.$state.params.webFormId;
        if (this.$state.params.webFormId) {
            this.dsarWebFormStore.fetchWebForm(this.$state.params.webFormId, this.currentTab);
        } else {
            this.dsarWebFormStore.fetchWebForm();
        }
        this.setTransitionAwayHook();
        this.defaultLogo = this.content ? this.content.GetKey("WebformLogo") || this.content.GetKey("GlobalHeaderLogo") : null;
    }

    $onDestroy() {
        if (this.transitionDeregisterHook) this.transitionDeregisterHook();
        this.unsubscribeFn();
    }

    dispatch(action: IFormAction): void {
        this.handleAction(action.type, action.payload);
    }

    handleAction(type: number, payload?: any) {
        switch (type) {
            case DSARStoreActions.PUBLISH:
                this.dsarWebFormStore.DSARState.published = true;
                this.published = true;
                this.dsarWebFormStore.publishDSARWebForm(this.webformId, () => {
                    this.dsarWebFormStore.DSARState.published = false;
                    this.published = false;
                });
                break;
            case DSARStoreActions.SAVE:
                this.dsarWebFormStore.saveDSARWebForm(true, true);
                this.dsarWebFormStore.DSARState.published = false;
                this.published = false;
                break;
            case DSARStoreActions.LANGUAGE_SWITCH:
                this.dsarWebFormStore.languageSwitch(payload.language);
                break;
            case DSARStoreActions.LOAD_SETTINGS:
                this.loadingSettings = true;
                this.dsarWebFormStore.fetchWebformSettings(this.webformId, this.webForm).then(() => {
                    this.loadingSettings = false;
                });
                break;
            case DSARStoreActions.LOAD_WEBFORM:
                this.loadingWebform = true;
                this.dsarWebFormStore.fetchFormData(this.webformId, this.webForm).then(() => {
                    this.loadingWebform = false;
                });
                break;
            case DSARStoreActions.LOAD_RULES:
                this.dsarWebFormStore.fetchFormData(this.webformId, this.webForm);
                break;
            case DSARStoreActions.STORE_RULES:
                this.changesMadeToRules = payload;
                break;
            case DSARStoreActions.STORE_SETTINGS:
                this.settings = {...payload.settings};
                this.changesMadeToSettings = payload.dirty;
                break;
            case DSARStoreActions.CONFIGURE_FORM_FIELD_TYPE_DISABLED:
                this.dsarWebFormStore.dispatch({type: DSARStoreActions.CONFIGURE_FORM_FIELD_TYPE_DISABLED, payload: { configureFormFieldTypeEnabled: false }});
                break;
            default:
                this.dsarWebFormStore.dispatch({ type, payload });
                break;
        }
    }

    handleActionNewWebform(action: { type: number; payload: any }) {
        this.handleAction(action.type, action.payload);
    }

    goToWebForms() {
        this.$state.go("zen.app.pia.module.dsar.views.webform.list");
    }

    updateSettingByName = (name: string, value: any) => {
        let updateSetting: IWebFormSettingsInput;
        if (name === DSARWebformSettingsLabel.RequireAttachment) {
            const data = this.webformMeta.settings.data.find(
                (setting: IWebFormSettingsInput) =>
                setting.fieldName === DSARWebformSettingsLabel.AttachmentSubmission);
            data.isRequired = value;
        } else if (name === DSARWebformSettingsLabel.TemplateName && value) {
            this.webformMeta.settings.templateName = value;
        } else {
            updateSetting = this.webformMeta.settings.data.find((setting: IWebFormSettingsInput) => setting.fieldName === name);
        }
        if (updateSetting) {
            switch (updateSetting.fieldName) {
                case DSARWebformSettingsLabel.Workflows:
                    updateSetting.value = {
                        workflowRefId: value.referenceId,
                    };
                    break;
                case DSARWebformSettingsLabel.Organization:
                    updateSetting.value = {
                        id: value,
                    };
                    this.webformMeta.settings.orgGroupId = value;
                    break;
                case DSARWebformSettingsLabel.DefaultReviewer:
                    updateSetting.value = {
                        FullName: value ? value.FullName : "- - - -",
                        Id: value ? value.Id : EmptyGuid,
                        Email: value ? value.Email : null,
                        OrgGroupId: value ? value.OrgGroupId : EmptyGuid,
                    };
                    break;
                case DSARWebformSettingsLabel.MultiCaptcha:
                    updateSetting.value = value.value;
                    break;
                default:
                    updateSetting.value = value;
                    break;
            }
        }
    }

    saveSettings = (): ng.IPromise<boolean> => {
        Object.keys(this.settings).forEach((key) => {
            this.updateSettingByName(key, this.settings[key]);
        });
        return this.webformApiService.updateWebFormSettings(this.webformId, this.webformMeta.settings).then((response: IProtocolResponse<IWebformSettings>) => {
            if (response.result) {
                this.changesMadeToSettings = false;
                this.savingSettings = false;
                this.notificationService.alertSuccess(this.$rootScope.t("Success"), this.$rootScope.t("SettingsSaveSuccess"));
                this.handleAction(DSARStoreActions.UPDATE_TEMPLATE_NAME, this.webformMeta.settings.templateName);
            }
            return response.result;
        });
    }

    saveRules = (rules?: IRule[]): ng.IPromise<IRule[]> | boolean => {
        const rulesValidate: IRuleValidate = this.oneRulesHelperService.areChangesValid();
        const saveRules: IRule[] = rules || this.oneRulesHelperService.getRules();
        if (rulesValidate.isValid && saveRules.length) {
            return this.dsarWebformRulesApiService.saveRules(this.webformId, saveRules).then((res: IRuleMeta[]) => {
                this.handleAction(DSARStoreActions.MAKE_RULES_DIRTY, true);
                if (res && res.length) {
                    const ruleMetaList: IRuleMeta[] = [];
                    for (const rule of saveRules) {
                        ruleMetaList.push(find(res, (ruleMeta: IRuleMeta) => {
                            return rule.id === ruleMeta.id || rule.id === ruleMeta.oldId;
                        }));
                    }
                    return this.dsarWebformRulesApiService.reorderRules(this.webformId, ruleMetaList).then((response: IRuleMeta[]) => {
                        this.changesMadeToRules = false;
                        this.savingRules = false;
                        return response;
                    });
                } else {
                    this.savingRules = false;
                    return [];
                }
            });
        } else {
            this.notificationService.alertError(this.$rootScope.t("Error"), this.$rootScope.t("DSARErrorSavingRules"));
            this.savingRules = false;
            return true;
        }
    }

    private setTransitionAwayHook() {
        this.transitionDeregisterHook = this.$transitions.onStart({}, (transition: IStringMap<any>, options: IStringMap<any>): boolean => {
            return this.handleStateChange(transition.to(), transition.params("to"), options);
        });
    }

    private handleStateChange(toState: string, toParams: IStringMap<any>, options: IStringMap<any>): boolean {
        const rules: IRule[] = this.oneRulesHelperService.getRules();
        // no changes made on current state
        if ((!this.changesMadeToForm || this.errorOnForm || !this.haveRequestTypes) &&
            (!this.changesMadeToRules || !(rules && rules.length)) &&
            !this.changesMadeToSettings && (!this.editFormField || this.errorOnForm)) {
                return true;
            }
        // if state is dirty
        this.launchLeavingModal().then((continueChange: boolean): void | undefined => {
            if (!continueChange) return; // if cancel state change
            this.$state.go(toState, toParams, options);
        });
        return false;
    }

    private handleTabChange(): ng.IPromise<boolean> | boolean {
        const rules: IRule[] = this.oneRulesHelperService.getRules();
        // no changes made on current state
        if ((!this.changesMadeToForm || this.errorOnForm || !this.haveRequestTypes) &&
            (!this.changesMadeToRules || !(rules && rules.length)) &&
            !this.changesMadeToSettings && (!this.editFormField || this.errorOnForm)) {
                return true;
            }
        // if state is dirty
        return this.launchLeavingModal().then((continueChange: boolean): boolean => {
            if (!continueChange) // if cancel tab change
                return false;
            return true;
        });
    }

    private launchLeavingModal(): ng.IPromise<boolean> {
        return OneConfirm("", this.$rootScope.t("UnsavedChangesSaveBeforeContinuing"), "", "", true, this.$rootScope.t("SaveChanges"), this.$rootScope.t("DontSave"))
            .then((result: boolean): ng.IPromise<boolean> | boolean | ng.IPromise<IRule[]>  => {
                if (isUndefined(result) || result === null) {
                    return false;
                }
                if (result) {
                    switch (this.currentTab) {
                        case WebformTabs.WebForm:
                            this.handleAction(DSARStoreActions.SAVE, null);
                            this.changesMadeToForm = false;
                            this.editFormField = false;
                            break;
                        case WebformTabs.Settings:
                            this.changesMadeToSettings = false;
                            this.savingSettings = true;
                            return this.saveSettings();
                        case WebformTabs.Rules:
                            this.changesMadeToRules = false;
                            this.savingRules = true;
                            return this.saveRules();
                        default:
                            this.handleAction(DSARStoreActions.SAVE, null);
                            this.changesMadeToForm = false;
                    }
                    return true;
                }
                this.changesMadeToForm = false;
                if (this.currentTab === WebformTabs.WebForm)
                    this.dsarWebFormStore.dispatch({ type: DSARStoreActions.CLEAR_WEBFORM_CHANGES });
                this.changesMadeToSettings = false;
                this.changesMadeToRules = false;
                return true;
        });
    }

    private getState() {
        this.ready = !this.dsarWebFormStore.DSARState.loading;
        const state = this.dsarWebFormStore.DSARState;
        if (state.webForm) {
            this.webForm = state.webForm;
            this.publishedHost = state.publishedHost;
            this.publishedPath = state.publishedPath;
            this.isPreviewGenerateDisabled = !this.publishedPath;
            this.published = state.published;
            this.webFormInProgress = this.published || this.isPreviewGenerateDisabled;
            this.webFormPublishStatus = this.published ? "Published" : "Draft" ;
            this.publishStatusBadgeClass = this.published ? "one-tag--primary-green padding-none" : "one-tag--primary-grey padding-none";
            this.formTranslations = state.formTranslations;
            const settingsInput: IWebformSettings = state.settingsInputs;
            this.webformMeta = {
                requestTypes: this.webForm.requestTypes,
                subjectTypes: this.webForm.subjectTypes,
                formFields: state.formFields ? state.formFields.data : null,
                formStylingInputs: state.formStylingInputs ? state.formStylingInputs.data : null,
                settingsInputs: settingsInput.data,
                settings: settingsInput,
                webFormOrgId: settingsInput.orgGroupId,
                workflows: {
                    publishedWorkflows: settingsInput.publishWorkflowDtos,
                    selectedWorkflow: settingsInput.workflowRefId,
                },
            };
            const recaptcha = this.webformMeta.settingsInputs.find((setting) => {
                return setting.fieldName === DSARSettings.EnableReCaptcha;
            });
            const captchaType = this.webformMeta.settingsInputs.find((setting) => {
                return setting.fieldName === DSARSettings.MultiCaptcha;
            });
            if ((recaptcha && recaptcha.value) && (captchaType && captchaType.value === DSARCaptchaType.BotdetectCaptcha)) {
                this.webformModel = {
                    botDetectCaptchaSettings: {
                        endpoint: this.webForm.captchaUrl + "/bc/botdetectcaptcha",
                        styleName: "angularBasicCaptcha",
                    },
                };
            }
            this.rulesDirty = state.rulesDirty;
        }
        this.widgetId = state.widgetId;
        this.permissions = state.permissions;
        this.savingInProgress = state.savingInProgress;
        this.changesMadeToForm = state.changesMadeToForm;
        this.haveRequestTypes = state.haveRequestTypes;
        this.editSidebarEnabled = state.editSidebarEnabled;
        this.selectedEditInput = state.selectedEditInput;
        this.errorOnForm = state.errorOnForm;
        this.configureFormFieldType = state.configureFormFieldType;
        this.configureFormField = state.configureFormField;
        this.selectedFormField = state.selectedFormField;
        this.editFormField = state.editFormField;

        if (this.ready && !this.isInitialized && this.currentTab === WebformTabs.WebForm) {
            this.setActions();
            this.saveTemplateButtonType = (this.permissions.enableWebFormPublish || this.showNewWebForm) ? "neutral" : "primary";
            this.actionsDropdown = (this.permissions.enableWebFormPublish || this.showNewWebForm) ? true : false;
            this.isInitialized = this.ready;
        }
    }

    private setActions() {
        this.actions = [];
        if (this.permissions.formsGenerateLink) {
            this.actions.push({
                id: "dsarWebformPreview",
                textKey: "Preview",
                action: (): void => this.previewWebForm(),
            });
            this.actions.push({
                id: "dsarWebformGenerateLink",
                textKey: "GenerateLink",
                action: (): void => this.handleAction(DSARStoreActions.GENERATE_LINK, null),
            });
        }
    }

    private previewWebForm() {
        if (this.showNewWebForm) {
            const previewWindow = window.open();
            try {
                previewWindow.document.body.innerHTML = this.getSpinnerToInjectDuringPreview();
            } catch (err) {
                throw new Error(err);
            }

            this.webformApiService.previewWebform(this.webformId).then((response) => {
                // clear the loader first
                previewWindow.document.body.innerHTML = "";
                previewWindow.document.write(response.data);
                previewWindow.document.close();
            }, (error) => {
                previewWindow.document.body.innerHTML = "";
                previewWindow.document.body.innerHTML = this.$rootScope.t("ErrorCompletingRequest");
                throw new Error(error);
            });
        } else {
            const local: boolean = window.location.hostname === "localhost";
            this.hrefString = `${window.location.protocol}//${window.location.host}/${local ? "" : "app/"}#/webform/${this.dsarWebFormStore.DSARState.webFormId}`;
            if (this.permissions.enableWebFormPublish) {
                this.hrefString = `${this.publishedHost}${this.publishedPath}`;
            }
            window.open(this.hrefString, "_blank");
        }
    }

    private getSpinnerToInjectDuringPreview() {
        return `
            <style>
                body {
                    color: #696969;
                    font-family: 'Open Sans', sans-serif;
                }
                .ot-loading {
                    display: flex;
                    flex-flow: column;
                    align-items: center;
                }
                .ot-loading-page {
                    height: 100%;
                    width: 100%;
                    justify-content: center;
                }
                .ot-loading__icon {
                    width: 10rem;
                    height: 10rem;
                    max-width: 100px;
                    max-height: 100px;
                    border-radius: 1000rem;
                    border: 1em solid rgba(31, 150, 219, 0.3);
                    border-top: 1em solid #1F96DB;
                    -webkit-animation: spin 1s infinite ease-in-out;
                    animation: spin 1s infinite ease-in-out;
                }
                .ot-loading__content {
                    margin-top: 0.5rem;
                    font-size: 1.2em;
                }
                @keyframes spin {
                    0% { transform: rotate(0deg); }
                    100% { transform: rotate(360deg); }
                }
            </style>
            <div class="ot-loading ot-loading-page">
                <div class="ot-loading__icon"></div>
                <div class="ot-loading__content"> ${this.$rootScope.t("Loading")}... </div>
            </div>`;
    }

    private changeTab(option: ITabsMeta<WebformTabs>) {
        this.currentTab = option.id;
        switch (this.currentTab) {
            case WebformTabs.WebForm:
                this.handleAction(DSARStoreActions.LOAD_WEBFORM);
                break;
            case WebformTabs.Settings:
                this.handleAction(DSARStoreActions.LOAD_SETTINGS);
                break;
            case WebformTabs.Rules:
                this.handleAction(DSARStoreActions.LOAD_RULES);
                break;
            default:
                this.handleAction(DSARStoreActions.LOAD_WEBFORM);
                break;
        }
    }
}

const WebFormManagerComponent: ng.IComponentOptions = {
    controller: WebFormManagerController,
    template: `
        <div class="column-nowrap height-100">
            <nav
                ng-if='$ctrl.ready && $ctrl.webForm'
                class="dsar-request-queue-detail-header__breadcrumb-header-bar padding-left-right-2 padding-top-1 padding-bottom-half border-bottom static-vertical"
                >
                <div class="row-space-between">
                    <div class="column-nowrap">
                        <div class="margin-bottom-half text-xlarge">{{::$root.t("WebFormCustomization")}}
                            <span ng-if="($ctrl.permissions.enableWebFormPublish || $ctrl.showNewWebForm) && $ctrl.currentTab === $ctrl.WebformTabs.WebForm"
                            class="position-relative top-half">
                                <one-tag
                                    class="margin-left-2 margin-top-2"
                                    size="medium"
                                    translated-text="{{$ctrl.webFormPublishStatus}}"
                                    wrapper-class="one-tag--override {{$ctrl.publishStatusBadgeClass}}"
                                ></one-tag>
                            </span>
                        </div>
                        <div class="text-small">
                            <span class="remove-anchor-style" ng-click="$ctrl.goToWebForms()">{{::$root.t("WebFormGallery")}}</span>
                            <i class="dsar-request-queue-detail-header__breadcrumb-icon ot ot-chevron-right text-tiny"></i>
                            <span class="dsar-request-queue-detail-header__project-name text-bold">{{$ctrl.webForm.templateName}}</span>
                        </div>
                    </div>
                    <div class="align-self-center padding-bottom-half">
                        <one-button
                            ng-if="($ctrl.permissions.enableWebFormPublish || $ctrl.showNewWebForm) && $ctrl.currentTab === $ctrl.WebformTabs.WebForm"
                            text="{{::$root.t('Publish')}}"
                            identifier="dsarWebformPublishTemplate"
                            type="primary"
                            button-click="$ctrl.handleAction($ctrl.dsarStoreActions.PUBLISH, null)"
                            enable-loading="true"
                            is-loading="$ctrl.savingInProgress"
                            is-disabled="$ctrl.published && !$ctrl.changesMadeToForm && !$ctrl.rulesDirty"
                            is-rounded="true"
                        ></one-button>
                        <one-button
                            ng-if="$ctrl.permissions.formsSaveTemplate && $ctrl.currentTab === $ctrl.WebformTabs.WebForm"
                            text="{{::$root.t('SaveTemplate')}}"
                            identifier="dsarWebformSaveTemplate"
                            type="{{$ctrl.saveTemplateButtonType}}"
                            button-click="$ctrl.handleAction($ctrl.dsarStoreActions.SAVE, null)"
                            is-disabled="$ctrl.savingInProgress || !$ctrl.changesMadeToForm || !$ctrl.haveRequestTypes || $ctrl.errorOnForm"
                            enable-loading="true"
                            is-loading="$ctrl.savingInProgress"
                            is-rounded="true"
                        ></one-button>
                        <one-button
                            ng-if="($ctrl.actions.length && !$ctrl.actionsDropdown) && $ctrl.currentTab === $ctrl.WebformTabs.WebForm"
                            ng-repeat="actionItem in $ctrl.actions track by actionItem.id"
                            identifier="{{::actionItem.id}}"
                            text="{{::actionItem.textKey ? $root.t(actionItem.textKey) : actionItem.text}}"
                            is-rounded="true"
                            button-click="actionItem.action()"
                        ></one-button>
                        <one-dropdown
                            ng-if="($ctrl.actions.length && $ctrl.actionsDropdown) && $ctrl.currentTab === $ctrl.WebformTabs.WebForm"
                            icon="fa fa-ellipsis-h"
                            hide-caret="true"
                            align-right="true"
                            options="$ctrl.actions"
                            round-button="true"
                            flat-button="false"
                        ></one-dropdown>
                    </div>
                </div>
            </nav>
            <div class="height-100" ng-if="!$ctrl.ready || !$ctrl.webForm">
                <loading ng-if='!$ctrl.ready' text='{{::$root.t("WebForm")}}'></loading>
                <one-background-message ng-if="$ctrl.ready && !$ctrl.webForm" row1="{{::$root.t('WebFormNotFound')}}"></one-background-message>
            </div>

            <one-tabs-nav
                ng-if="$ctrl.ready && ($ctrl.tabOptions.length > 1)"
                class="background-white static-vertical padding-top-2"
                selected-tab="$ctrl.currentTab"
                options="$ctrl.tabOptions"
                resolve-before-tab-change="$ctrl.handleTabChange()"
            ></one-tabs-nav>

            <div
                ng-if="$ctrl.ready && $ctrl.currentTab === $ctrl.WebformTabs.Rules"
                class="row-nowrap stretch-vertical"
            >
                <div class="align-self-center width-100-percent" ng-if="$ctrl.savingRules">
                    <loading
                        text='{{::$root.t("SavingRules")}}'
                        show-loading-prefix="false"
                    ></loading>
                </div>
                <downgrade-dsar-webform-rules
                    ng-if="$ctrl.currentTab === $ctrl.WebformTabs.Rules && !$ctrl.savingRules"
                    class="flex-full-height justify-center"
                    [form-translations]="$ctrl.formTranslations"
                    [webform-id]="$ctrl.webformId"
                    [webform-meta]="$ctrl.webformMeta"
                    [save-promise]="$ctrl.saveRules"
                    (action-callback)="$ctrl.handleAction($event.type, $event.payload)"
                ></downgrade-dsar-webform-rules>
            </div>

            <div
                ng-if="$ctrl.ready && $ctrl.currentTab === $ctrl.WebformTabs.Settings"
                class="row-nowrap stretch-vertical"
            >
                <div class="align-self-center width-100-percent" ng-if="$ctrl.loadingSettings">
                    <loading text='{{::$root.t("Settings")}}'></loading>
                </div>
                <div class="align-self-center width-100-percent" ng-if="$ctrl.savingSettings">
                    <loading
                        text='{{::$root.t("SavingSettings")}}'
                        show-loading-prefix="false"
                    ></loading>
                </div>
                <downgrade-dsar-webform-settings
                    ng-if="!$ctrl.loadingSettings && !$ctrl.savingSettings"
                    class="full-width-block"
                    [webform-settings]="$ctrl.webformMeta.settings"
                    [webform]="$ctrl.webForm"
                    [save-promise]="$ctrl.saveSettings"
                    (action-callback)="$ctrl.handleAction($event.type, $event.payload)"
                >
                </downgrade-dsar-webform-settings>
            </div>

            <div ng-if="$ctrl.ready && $ctrl.webForm && $ctrl.currentTab === $ctrl.WebformTabs.WebForm" class="row-nowrap stretch-vertical">
                <div class="align-self-center width-100-percent" ng-if="$ctrl.loadingWebform">
                    <loading text='{{::$root.t("WebForm")}}'></loading>
                </div>
                <webform-sidebar
                    ng-if="!$ctrl.editSidebarEnabled
                            && !$ctrl.configureFormFieldType
                            && !$ctrl.loadingWebform
                            && !$ctrl.configureFormField"
                    [web-form]="$ctrl.webForm"
                    [sidebar-data]="$ctrl.webformMeta"
                    [permissions]="$ctrl.permissions"
                    [form-translations]="$ctrl.formTranslations"
                    (action-callback)="$ctrl.dispatch($event)"
                ></webform-sidebar>
                <form-field-type-configure
                    ng-if="$ctrl.configureFormFieldType && !$ctrl.configureFormField && !$ctrl.loadingWebform"
                    [count]="$ctrl.webformMeta.formFields.length"
                    (handle-action)="$ctrl.dispatch($event)"
                ></form-field-type-configure>
                <form-field-configure
                    ng-if="!$ctrl.configureFormFieldType && $ctrl.configureFormField && !$ctrl.loadingWebform"
                    [field]="$ctrl.selectedFormField"
                    [form-translations]="$ctrl.formTranslations"
                    [all-form-fields]="$ctrl.webformMeta.formFields"
                    (handle-action)="$ctrl.dispatch($event)"
                ></form-field-configure>
                <downgrade-webform-sidebar-edit
                    ng-if="$ctrl.editSidebarEnabled && !$ctrl.loadingWebform"
                    [selected-edit-input]="$ctrl.selectedEditInput"
                    [form-translations]="$ctrl.formTranslations"
                    (action-callback)="$ctrl.dispatch($event)"
                ></downgrade-webform-sidebar-edit>
                <webform-control
                    ng-if="!$ctrl.showNewWebForm && !$ctrl.loadingWebform"
                    class="full-width-block background-grey"
                    is-external="false"
                    web-form="$ctrl.webForm"
                    web-form-languages-list="$ctrl.webForm.masterLanguagesList"
                    widgetId="$ctrl.widgetId"
                    form-translations="$ctrl.formTranslations"
                    sidebar-data="$ctrl.webformMeta"
                    action-callback="$ctrl.handleAction(type, payload)"
                ></webform-control>
                <webform-preview
                    ng-if="$ctrl.showNewWebForm && !$ctrl.loadingWebform"
                    class="full-width-block background-grey"
                    [is-external]="false"
                    [webform]="$ctrl.webForm"
                    [webform-model]="$ctrl.webformModel"
                    [widget-id]="$ctrl.widgetId"
                    [form-translations]="$ctrl.formTranslations"
                    [sidebar-data]="$ctrl.webformMeta"
                    [default-logo]="$ctrl.defaultLogo"
                    (action-callback)="$ctrl.handleActionNewWebform($event)"
                ></webform-preview>
            </div>
        </div>
    `,
};

export default WebFormManagerComponent;
