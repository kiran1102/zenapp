// 3rd party
import {
    Component,
    Input,
    OnInit,
    Output,
    EventEmitter,
    OnChanges,
    SimpleChanges,
} from "@angular/core";

// Interfaces
import { ISettingsMeta } from "sharedModules/interfaces/settings.interface";
import { IStringMap } from "interfaces/generic.interface";

interface ISettingsPayload {
    type: number;
    payload: any;
}

// Constants
import {
    DSARStoreActions,
    DSARWebformSettingsLabel,
} from "dsarModule/enums/dsar-webform.enum";
import { SettingsType } from "sharedModules/enums/settings.enum";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

// Services
import { NotificationService } from "modules/shared/services/provider/notification.service";

@Component({
    selector: "webform-attachment-settings",
    templateUrl: "./webform-attachment-settings.component.html",
})
export class WebformAttachmentSettingsComponent implements OnInit, OnChanges {
    webformGeneralSettingMeta: ISettingsMeta[];
    settings: IStringMap<string>;
    @Input() formTranslations: IStringMap<string>;
    @Output() actionCallback = new EventEmitter<ISettingsPayload>();

    constructor(
        private readonly translatePipe: TranslatePipe,
        private notificationService: NotificationService,
    ) { }

    public ngOnInit(): void {
        this.webformGeneralSettingMeta = [
            {
                actionType: DSARWebformSettingsLabel.AttachmentInfoDesc,
                autoId: "DSARWebformSidebarAttachmentTextArea",
                class: "margin-bottom-2",
                hasError: false,
                maxLength: 300,
                placeholder: this.translatePipe.transform("AttachmentDescPlaceholder"),
                showDivider: true,
                title: DSARWebformSettingsLabel.Description,
                type: SettingsType.TEXTAREA,
            },
            {
                actionType: DSARWebformSettingsLabel.AttachmentButtonText,
                autoId: "DSARWebformSidebarButtonTextArea",
                class: "margin-bottom-2",
                hasError: false,
                maxLength: 150,
                placeholder: this.translatePipe.transform("UploadAttachment"),
                required: true,
                showDivider: true,
                title: DSARWebformSettingsLabel.ButtonText,
                type: SettingsType.TEXTAREA,
            },
            {
                actionType: DSARWebformSettingsLabel.FilesLargerThanSizeNotSupported,
                autoId: "DSARWebformSidebarUploadDescriptionTextArea",
                class: "margin-bottom-2",
                hasError: false,
                maxLength: 150,
                required: true,
                title: DSARWebformSettingsLabel.FileSizeLimitText,
                type: SettingsType.TEXTAREA,
            }];
    }

    ngOnChanges(changes: SimpleChanges) {
        if (changes.formTranslations) {
            this.settings = {
                [DSARWebformSettingsLabel.AttachmentInfoDesc]: this.formTranslations[DSARWebformSettingsLabel.AttachmentInfoDesc],
                [DSARWebformSettingsLabel.AttachmentButtonText]: this.formTranslations[DSARWebformSettingsLabel.AttachmentButtonText],
                [DSARWebformSettingsLabel.FilesLargerThanSizeNotSupported]: this.formTranslations[DSARWebformSettingsLabel.FilesLargerThanSizeNotSupported],
            };
        }
    }

    handleAction(action) {
        const value: string = action.payload.target.value;
        switch (action.type) {
            case DSARWebformSettingsLabel.AttachmentButtonText:
            case DSARWebformSettingsLabel.FilesLargerThanSizeNotSupported:
                this.webformGeneralSettingMeta.find((setting: ISettingsMeta) => {
                    return setting.actionType === action.type;
                }).hasError = !value;
                if (!value) {
                    this.notificationService.alertWarning(
                        this.translatePipe.transform("Alert"),
                        this.translatePipe.transform(
                            action.type === DSARWebformSettingsLabel.AttachmentButtonText ? "PleaseAddButtonText" : "PleaseAddFileSizeDescriptionText"),
                    );
                }
                break;
        }
        const attachmentError: boolean = this.webformGeneralSettingMeta.some((setting: ISettingsMeta) => setting.hasError);
        if (attachmentError) {
            this.actionCallback.emit({
                type: DSARStoreActions.ERROR_FORM,
                payload: {
                    errorOnForm: true,
                },
            });
        } else {
            this.actionCallback.emit({
                type: DSARStoreActions.ERROR_FORM,
                payload: {
                    errorOnForm: false,
                },
            });
        }
        this.actionCallback.emit({
            type: DSARStoreActions.DEFAULT_WEBFORM_CHANGED,
            payload: {
                fieldName: action.type,
                value,
            },
        });
    }
}
