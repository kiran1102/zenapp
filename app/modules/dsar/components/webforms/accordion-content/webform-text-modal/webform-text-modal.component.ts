// Angular
import {
    Component,
    Inject, Input, OnInit,
} from "@angular/core";

// Constants and Interfaces
import { IWebformTextModalResolve } from "interfaces/webform-text-modal-resolve.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";
import { Regex } from "constants/regex.constant";
import { IStore } from "interfaces/redux.interface";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

// Redux and Rxjs
import { StoreToken } from "tokens/redux-store.token";
import {
    BehaviorSubject,
    Observable,
    Subject,
} from "rxjs";

@Component({
    selector: "webform-text-modal-component",
    templateUrl: "./webform-text-modal-component.html",
})
export class WebformTextModalComponent implements OnInit {
    text: string;
    textTouched = false;
    cancelButtonText: string;
    submitButtonText: string;
    isSubmitting: boolean;
    canSubmit = false;
    otModalCloseEvent: Subject<IProtocolResponse<string>>;
    otModalData: IWebformTextModalResolve;
    customModules = [
        [
            "bold",
            "italic",
            "underline",
            "strike",
            { list: "ordered" },
            { list: "bullet" },
        ],
        [
            { align: "" },
            { align: "right" },
            { align: "center" },
            { align: "justify" },
            "link",
        ],
    ];
    contentSource = new BehaviorSubject(this.text);
    contentObservable: Observable<string> = this.contentSource.asObservable();

    constructor(
        private translatePipe: TranslatePipe,
        @Inject(StoreToken) public store: IStore,
    ) {}

    ngOnInit(): void {
        this.submitButtonText = this.translatePipe.transform("Save");
        this.cancelButtonText = this.translatePipe.transform("Cancel");
        this.text = this.otModalData.text;
        this.contentSource.next(this.text);
    }

    handleMessageChange(message: string): void {
        this.textTouched = true;
        this.text = message && message.length > 1000 ? message.slice(0, 5000) : message;
        this.validateSubmit();
    }

    handleSubmit(): void {
        this.isSubmitting = true;
        this.closeModal({ result: true, data: this.text });
        this.isSubmitting = false;
    }

    closeModal(data?: IProtocolResponse<string>) {
        this.otModalCloseEvent.next(data);
    }

    private validateSubmit(): void {
        if (this.text) {
            const trimmedText = this.text.replace(Regex.REMOVE_HTML_TAGS, "");
            let textWithoutSpace = trimmedText.replace(Regex.REMOVE_SPACE_BETWEEN_TAGS, "");
            textWithoutSpace = trimmedText.replace(/&nbsp;/g, "");
            this.canSubmit = textWithoutSpace.trim().length > 0;
        } else {
            this.canSubmit = false;
        }
    }
}
