// Angular
import {
    Input,
    Output,
    EventEmitter,
    Component,
    OnInit,
} from "@angular/core";

// Constants
import { DSARStoreActions } from "dsarModule/enums/dsar-webform.enum";

// Interfaces
import { IStringMap} from "interfaces/generic.interface";

@Component({
    selector: "webform-request-type-content",
    templateUrl: "./webform-request-type-content.component.html",
})
export class WebFormRequestTypeContentComponent implements OnInit {

    @Input() permissions: IStringMap<boolean>;
    @Input() data: any;
    @Input() webForm: any;
    @Input() formTranslations: IStringMap<string>;
    @Output() actionCallback = new EventEmitter();

    requestTypeToggle: boolean;
    subjectTypeToggle: boolean;

    ngOnInit() {
        this.requestTypeToggle = this.data.settingsInputs.find((value) => value.fieldName === "EnableRequestTypes").value;
        this.subjectTypeToggle = this.data.settingsInputs.find((value) => value.fieldName === "EnableSubjectTypes").value;
    }

    inputChanged(inputData: string, webFormProperty: string): void {
        this.handleAction({type: DSARStoreActions.DEFAULT_WEBFORM_CHANGED, payload: { fieldName: webFormProperty, value: inputData }});
    }

    toggleChanged(type: string): void {
        switch (type) {
            case "EnableRequestTypes":
                this.requestTypeToggle = !this.requestTypeToggle;
                this.handleAction({type: DSARStoreActions.MULTI_SELECT_TOGGLE, payload: {type, value: this.requestTypeToggle}});
                break;
            case "EnableSubjectTypes":
                this.subjectTypeToggle = !this.subjectTypeToggle;
                this.handleAction({type: DSARStoreActions.MULTI_SELECT_TOGGLE, payload: {type, value: this.subjectTypeToggle}});
                break;
            default:
                break;
        }
    }

    handleAction(data: { type: number, payload: any }): void {
        this.actionCallback.emit(data);
    }

}
