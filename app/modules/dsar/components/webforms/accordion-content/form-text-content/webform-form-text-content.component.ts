// Angular
import {
    Component,
    Inject,
    Input,
    Output,
    EventEmitter,
    OnDestroy,
} from "@angular/core";
// Rxjs
import { Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

// Services
import { Permissions } from "modules/shared/services/helper/permissions.service";

// Interfaces and Constants
import { IWebformTextModalResolve } from "interfaces/webform-text-modal-resolve.interface";
import { IStringMap } from "interfaces/generic.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";
import { DSARModalType } from "dsarModule/constants/dsar-settings.constant";
import { DSARStoreActions } from "dsarModule/enums/dsar-webform.enum";

// Components
import { WebformTextModalComponent } from "dsarModule/components/webforms/accordion-content/webform-text-modal/webform-text-modal.component";
import { OtModalService, ModalSize } from "@onetrust/vitreus";

interface ISettingsPayload {
    type: number;
    payload: any;
}
@Component({
    selector: "webform-form-text-content",
    templateUrl: "./webform-form-text-content.component.html",
})
export class WebformFormTextContent implements OnDestroy {
    @Input() formTranslations: IStringMap<string>;
    @Input() data: any;
    @Output() actionCallback = new EventEmitter<ISettingsPayload>();
    public DSARModalType = DSARModalType;
    private translatedWT: string;
    private translatedFT: string;
    private translatedTYT: string;
    private destroy$ = new Subject();

    constructor(
        @Inject(Permissions) public permissions: Permissions,
        private otModalService: OtModalService,
        private translatePipe: TranslatePipe,

    ) {}

    handleAction(
        type: number, payload:
            | { editSidebarEnabled: boolean }
            | { selectedEditInput: string }
            | { fieldName: string; value: string },
    ): void {
        this.actionCallback.emit({ type, payload });
    }

    inputChanged(type: string): void {
        this.handleAction(DSARStoreActions.EDIT_SIDEBAR_ENABLED, { editSidebarEnabled: true});
        this.handleAction(DSARStoreActions.SET_EDIT_INPUT, {selectedEditInput: type});
    }

    launchTextModal(modalType: string): void {
        this.translatedWT = this.formTranslations["WelcomeText"];
        this.translatedFT = this.formTranslations["FooterText"];
        this.translatedTYT = this.formTranslations["ThankYouText"];
        const webformTextModalData: Partial<IWebformTextModalResolve> = {
            typeOfModal: modalType,
            text:
                modalType === DSARModalType.WELCOME
                    ? this.translatedWT
                    : modalType === DSARModalType.FOOTER
                    ? this.translatedFT
                    : this.translatedTYT,
            callback: (res: IProtocolResponse<string>): void => {
                if (res.result) {
                    if (modalType === DSARModalType.WELCOME) {
                        this.translatedWT = res.data;
                        this.handleAction(
                            DSARStoreActions.DEFAULT_WEBFORM_CHANGED,
                            {
                                fieldName: "WelcomeText",
                                value: this.translatedWT,
                            },
                        );
                    } else if (modalType === DSARModalType.FOOTER) {
                        this.translatedFT = res.data;
                        this.handleAction(
                            DSARStoreActions.DEFAULT_WEBFORM_CHANGED,
                            {
                                fieldName: "FooterText",
                                value: this.translatedFT,
                            },
                        );
                    } else {
                        this.translatedTYT = res.data;
                        this.handleAction(
                            DSARStoreActions.DEFAULT_WEBFORM_CHANGED,
                            {
                                fieldName: "ThankYouText",
                                value: this.translatedTYT,
                            },
                        );
                    }
                }
            },
        };
        switch (modalType) {
            case DSARModalType.WELCOME:
                webformTextModalData.title = "WelcomeText";
                break;
            case DSARModalType.FOOTER:
                webformTextModalData.title = "FooterText";
                break;
            default:
                webformTextModalData.title = "ThankYouPage";
        }
        this.otModalService.create(
                    WebformTextModalComponent,
                    {
                        isComponent : true,
                        size: ModalSize.SMALL,
                    },
                    webformTextModalData,
                ).pipe(takeUntil(this.destroy$)).subscribe(
                    (res: IProtocolResponse<string>): void => {
                if (res && res.result) {
                    if (modalType === DSARModalType.WELCOME) {
                        this.translatedWT = res.data;
                        this.handleAction(
                            DSARStoreActions.DEFAULT_WEBFORM_CHANGED,
                            {
                                fieldName: "WelcomeText",
                                value: this.translatedWT,
                            },
                        );
                    } else if (modalType === DSARModalType.FOOTER) {
                        this.translatedFT = res.data;
                        this.handleAction(
                            DSARStoreActions.DEFAULT_WEBFORM_CHANGED,
                            {
                                fieldName: "FooterText",
                                value: this.translatedFT,
                            },
                        );
                    } else {
                        this.translatedTYT = res.data;
                        this.handleAction(
                            DSARStoreActions.DEFAULT_WEBFORM_CHANGED,
                            {
                                fieldName: "ThankYouText",
                                value: this.translatedTYT,
                            },
                        );
                    }
                }
            },
        );
    }

    ngOnDestroy(): void {
        this.destroy$.next();
        this.destroy$.complete();
    }
}
