// Angular
import {
    Input,
    Output,
    EventEmitter,
    Component,
} from "@angular/core";

// Interfaces
import { IStringMap } from "interfaces/generic.interface";

@Component({
    selector: "webform-form-fields-content",
    templateUrl: "./webform-form-fields-content.component.html",
})
export class WebformFormFieldsContentComponent {
    // TO DO
    // change the below data types from any to actual data types
    @Input() formTranslations: IStringMap<string>;
    @Input() permissions: any;
    @Input() data: any;
    @Output() actionCallback = new EventEmitter();

    handleAction(data: { type: string, payload: any }) {
        this.actionCallback.emit(data);
    }
}
