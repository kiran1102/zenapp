// Angular
import { Component,
    Input,
    EventEmitter,
    Output,
    OnInit,
} from "@angular/core";

// Constants & Enums
import { DSARStoreActions } from "dsarModule/enums/dsar-webform.enum";

// Interfaces
import { IPermissions } from "sharedModules/interfaces/permissions.interface";
import { IWebformStyleField } from "../../shared/interfaces/webform-styling-response.interface";

@Component({
    selector: "webform-form-styling-content",
    templateUrl: "./webform-form-styling-content.component.html",
})
export class WebformFormStylingContentComponent implements OnInit {

    @Input() otLogoEnabled: boolean;
    @Input() permissions: IPermissions;
    @Input() data: IWebformStyleField[];
    @Output() actionCallback = new EventEmitter();

    dsarStoreActions = DSARStoreActions;

    numberFieldValue = 0;
    inputField: IWebformStyleField;
    isAsyncDone = true;

    ngOnInit(): void {
        this.data.forEach((input) => {
            if (input.dataType === "Color" && typeof input.value === "string") {
                input.value = input.value.trim();
            }
        });
    }

    trackByIndex(index: number): number {
        return index;
    }

    modelChangeForColor(event: boolean, input: IWebformStyleField): void {
        if (event) {
            this.handleAction(this.dsarStoreActions.INPUT_CHANGED, { panelName: "formStylingInputs", fieldName: input.fieldName, value: input.value });
        }
    }

    modelChangeForNumberFields(newValue: number, input: IWebformStyleField): void {
        this.numberFieldValue = newValue;
        this.inputField = input;
        if (this.isAsyncDone) {
            this.isAsyncDone = false;
            setTimeout(() => {
                this.changeNumberFields( this.numberFieldValue, this.inputField);
            }, 500);
        }
    }

    handleAction(type: number, payload: { panelName: string, fieldName: string, value: string | boolean | number }): void {
        this.actionCallback.emit({ type, payload });
    }

    toggleLogoSwitch(): void {
        this.otLogoEnabled = !this.otLogoEnabled;
        this.handleAction(DSARStoreActions.INPUT_CHANGED, { panelName: "settingsInputs", fieldName: "ShowOneTrustLogo", value: this.otLogoEnabled });
    }

    addLogo(imgFile: any, input: any): void {
        if (imgFile) {
            const header: File = imgFile[0];
            const reader: FileReader = new FileReader();
            reader.onloadend = (e: any): void => {
                this.handleAction(DSARStoreActions.LOGO_UPLOADED, { panelName: "formStylingInputs", fieldName: input.fieldName, value: e.target.result });
            };
            reader.readAsDataURL(header);
        }
    }

    clearLogo(input: any): void {
        this.handleAction(DSARStoreActions.LOGO_UPLOADED, { panelName: "formStylingInputs", fieldName: input.fieldName, value: "" });
    }

    /**
     * Run this validation asynchronously as the user is not able to enter
     * values like 150 or 200 because as soon we enter 2 this function runs
     * checks 2 is less than the min value without checking the number following 2
     * and resets to a default value.
     *
     * @param {number} newValue
     * @param {IWebformStyleField} input
     */
    private changeNumberFields(newValue: number, input: IWebformStyleField): void {
        if (newValue < input.min || newValue === undefined) {
            input.value = input.min;
        } else if (newValue > input.max || newValue === undefined) {
            input.value = input.max;
        } else {
            input.value = newValue;
        }
        this.handleAction(this.dsarStoreActions.INPUT_CHANGED, { panelName: "formStylingInputs", fieldName: input.fieldName, value: input.value });
        this.isAsyncDone = true;
    }
}
