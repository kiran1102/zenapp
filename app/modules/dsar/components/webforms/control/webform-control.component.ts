// 3rd Party
import {
    each,
    filter,
    isFunction,
    find,
    isNil,
    isEmpty,
    assign,
    pull,
    keys,
    omit,
    isArray,
    forEach,
    debounce,
} from "lodash";
import Utilities from "Utilities";

// Redux
import { AnyAction } from "redux";

// Services
import { WebformApiService } from "dsarservice/webform-api.service";

// Interfaces
import { IProtocolResponse } from "interfaces/protocol.interface";
import { ITenantTranslations } from "interfaces/dsar/dsar-edit-language.interface";
import {
    IStringMap,
    IKeyValue,
} from "interfaces/generic.interface";
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import { IAttachmentFile } from "interfaces/attachment-file.interface";
import {
    IWebFormResponse,
    IWebFormData,
    IWebformAttachment,
} from "dsarModule/interfaces/webform.interface";
import { IDSARFormField } from "@onetrust/dsar-components";

// Constants & Enums
import {
    WebformControls,
    InputType,
    DisplayType,
 } from "dsarModule/constants/web-form-control.constant";
import {
    DSARSettings,
    DSARCaptchaType,
} from "dsarModule/constants/dsar-settings.constant";
import { RequestTypeStatus } from "dsarModule/enums/web-form.enum";
import { DSARStoreActions } from "dsarModule/enums/dsar-webform.enum";

class WebFormControlController implements ng.IComponentController {
    static $inject: string[] = [
        "$rootScope",
        "ENUMS",
        "Content",
        "webformApiService",
        "vcRecaptchaService",
        "Captcha",
        "captchaSettings",
        "$timeout",
    ];

    formTranslations: IStringMap<string>;
    attachment: IAttachmentFile;
    requestTypeStatus = RequestTypeStatus;
    tenantTranslations: IStringMap<string>;
    widgetId: number;

    botdetectCaptcha: any;
    captchaCode: string;
    captchaId: string;
    captchaType = DSARCaptchaType;
    validCaptcha: boolean;
    botdetectInputDirty: boolean;
    requestDetailsSubmitted = false;
    displayType = DisplayType;
    debouncedInputUpdate = debounce((input: any, value: string) => this.answer(input, value), 300);

    private contentWebformLogo: any;
    private contentDefaultHeaderLogo: any;

    private typeKey = "id";
    private isCompleted = false;
    private queueId = "";
    private subjectEmail = "";

    private webForm: any;
    private sidebarData: any;
    private formData: IWebFormResponse;
    private onSubmit: any;

    private styles: any;
    private settings: any;

    private recaptcha: string;
    private canSubmit = false;
    private saving = false;
    private actionCallback: (action: AnyAction) => void;
    private isExternal: boolean;
    private fieldsetExtClass: string;
    private formExtClass: string;
    private languagesList: string[];
    private isLanguageDisabled: boolean;
    private maxSizeText: string;
    private readonly MAX_SIZE = 4;
    private hiddenRouteParam: string;
    private previousLanguage: string;
    private validBotdetectCaptcha = false;

    constructor(
        readonly $rootScope: IExtendedRootScopeService,
        readonly ENUMS: any,
        Content: any,
        readonly dsarWebFormService: WebformApiService,
        readonly vcRecaptchaService: any,
        readonly Captcha: any,
        readonly captchaSettings: any,
        readonly $timeout: ng.ITimeoutService,
    ) {
        this.hiddenRouteParam = window.location.href.split("?")[1];
        this.contentWebformLogo = Content.GetKey("WebformLogo");
        this.contentDefaultHeaderLogo = Content.GetKey("GlobalHeaderLogo");
    }
    public $onInit(): void {
        this.captchaSettings["captchaEndpoint"] = this.webForm.captchaUrl + "/bc/botdetectcaptcha";
        if (this.isExternal) {
            this.fieldsetExtClass = "";
            this.formExtClass = "max-width-full";
        } else {
            this.fieldsetExtClass = "padding-all-2 overflow-y-auto";
            this.formExtClass = "shadow2 background-white margin-all-auto";
        }
        this.webForm.selectedLanguage = this.webForm.defaultLanguage;
        this.initializeCaptcha(this.webForm.selectedLanguage);

        this.modifyCountryFields();
    }

    public $onChanges(change: ng.IOnChangesObject): void {
        if (change.webForm) this.resetForm();
        this.styles = {
            image: this.getStylesByName("HeaderLogo") || this.contentWebformLogo || this.contentDefaultHeaderLogo || "images/logo_white.svg",
            header: {
                height: this.getStylesByName("HeaderHeight"),
                background: this.getStylesByName("HeaderColor"),
            },
            welcomeText: {
                "color": this.getStylesByName("WelcomeTextColor"),
                "font-size": this.getStylesByName("WelcomeTextSize"),
            },
            labelText: {
                color: this.getStylesByName("FormLabelColor"),
            },
            buttonActive: {
                "background-color": this.getStylesByName("ActiveButtonColor"),
                "color": this.getStylesByName("ActiveButtonTextColor"),
            },
            buttonHover: {
                "color": this.getStylesByName("ActiveButtonColor"),
                "border-color": this.getStylesByName("ActiveButtonColor"),
            },
            buttonActiveHover: {
                "border-color": this.getStylesByName("ActiveButtonTextColor"),
            },
            attachmentDescription: {
                "color": this.getStylesByName("AttachmentDescriptionTextColor"),
                "font-size": this.getStylesByName("AttachmentDescriptionTextSize"),
            },
            attachmentButton: {
                "color": this.getStylesByName("AttachmentButtonTextColor"),
                "font-size": this.getStylesByName("AttachmentButtonTextSize"),
                "word-break": "break-word",
                "word-wrap": "break-word",
                "white-space": "normal",
            },
            footerText: {
                "color": this.getStylesByName("FooterTextColor"),
                "font-size": this.getStylesByName("FooterTextSize"),
            },
            queueId: {
                color: this.getStylesByName("AttachmentDescriptionTextColor"),
            },
        };

        this.settings = {
            recaptcha: this.getSettingByName(DSARSettings.EnableReCaptcha),
            captchaType: this.getSettingByName(DSARSettings.MultiCaptcha),
            languageEnabled: this.getSettingByName(DSARSettings.TranslateWebForm),
            attachmentUpload: this.getSettingByName(DSARSettings.AttachmentSubmission),
            isLogoEnabled: this.getSettingByName(DSARSettings.IsLogoEnabled, true),
            enableSubjectTypes: this.getSettingByName(DSARSettings.EnableSubjectTypes),
            enableRequestTypes: this.getSettingByName(DSARSettings.EnableRequestTypes),
            isVerificationEnabled: this.getSettingByName(DSARSettings.EmailVerification),
            isAttachmentUploadRequired: this.isSettingRequired(DSARSettings.AttachmentSubmission),
        };
        if (!this.settings.enableSubjectTypes && (this.formData["subjectTypes"].length !== 1)) {
            this.formData["subjectTypes"] = [];
        }
        if (!this.settings.enableRequestTypes && (this.formData["requestTypes"].length !== 1)) {
            this.formData["requestTypes"] = [];
        }
        if (!Object.keys(this.formData["multiselectFields"]).length)  {
            this.formData["multiselectFields"] = {};
        }
        this.maxSizeText = this.formTranslations["FilesLargerThanSizeNotSupported"];
        if (this.maxSizeText.match("{MaxSize}")) {
            this.maxSizeText = this.maxSizeText.replace(/{MaxSize}/, `${this.MAX_SIZE}`);
        }
        this.languagesList = filter(this.webForm.webformLanguagesList, ["Status" , 10]);
        if (this.isExternal) {
            this.isLanguageDisabled = this.languagesList.length === 1 ;
        }
        if (!this.isExternal && this.webForm.selectedLanguage) {
            if (this.webForm.selectedLanguage !== this.previousLanguage) {
                this.previousLanguage = this.webForm.selectedLanguage;
                this.answer({ fieldKey: "language" }, this.webForm.selectedLanguage);
                this.initializeCaptcha(this.webForm.selectedLanguage);
            }
        }
        this.translateFormFieldOptions();
        this.validateForm();
    }

     getStylesByName(name: string, defaultValue: string = ""): string {
        const style = find(this.sidebarData.formStylingInputs, ["fieldName", name]);
        return style ? style.value : defaultValue;
    }

    translateFormFieldOptions(): void {
        each(this.sidebarData.formFields, (input: IDSARFormField): IDSARFormField => {
            if (input.inputType === InputType.Multiselect && input.status === RequestTypeStatus.Add) {
                each(input.options, (option: IKeyValue<string>): IKeyValue<string> => {
                    option.value = this.formTranslations[option.key];
                    return option;
                });
                // Doing it for Old data
                if (!input.displayType) {
                    input.displayType = this.displayType.Multiselect;
                }
            }
            return input;
        });
    }
    public selectLanguage(language: string): void {
        if (this.isExternal) {
            this.handleAction(DSARStoreActions.LANGUAGE_SWITCH, { language });
            this.initializeCaptcha(language);
        }
        this.answer({ fieldKey: "language" }, language);
    }

    public isSettingRequired(name: string): boolean {
        return find(this.sidebarData.settingsInputs, ["fieldName", name]).isRequired;
    }

    isOptionSelected(input: IDSARFormField, option: IKeyValue<string>): boolean {
        const multiSelectField = this.formData["multiselectFields"][input.fieldKey];
        return multiSelectField && multiSelectField.indexOf(option.key) > -1;
    }

    public initializeCaptcha(language: string) {
        try {
            if (Number.isInteger(this.widgetId)) {
                this.vcRecaptchaService.useLang(this.widgetId, language.split("-")[0]);
            }
        } catch (e) {
            this.handleAction(DSARStoreActions.STORE_WIDGET_ID, { widgetId: null });
        }
    }

    public onWidgetCreate(widgetId: number): void {
        this.widgetId = widgetId;
        this.handleAction(DSARStoreActions.STORE_WIDGET_ID, { widgetId });
    }

    validateBotdetect(form: any, pristine: boolean): void {
        this.$timeout((): void => {
            if (!pristine) {
                this.validBotdetectCaptcha = !(form.$error && form.$error.incorrectCaptcha);
                this.botdetectInputDirty = true;
            }
            this.validateForm();
        }, 2000);

    }
    handleMultiSelect(input: IDSARFormField, data: Array<IKeyValue<string>>): void {
        let keyArr = [];
        if (input.displayType === this.displayType.Button) {
            const value = data["key"];
            keyArr = this.formData["multiselectFields"][input.fieldKey] || [];
            if (input.enableMultipleSelection) {
                if (keyArr.indexOf(value) > -1) {
                    pull(keyArr, value);
                } else {
                    keyArr.push(value);
                }
            } else {
                keyArr[0] = value;
            }
        } else {
            each(data, (option: IKeyValue<string>): void => {
                keyArr.push(option.key);
            });
        }
        const valueSelected = keyArr.length ? keyArr.join(",") : "";
        this.answer(input, valueSelected);
    }
    uploadFile(file: IAttachmentFile[]) {
        this.attachment = (isArray(file) && file.length) ? file[0] : null;
        this.validateForm();
    }

    private resetForm(): void {
        this.formData = {
            firstName: "",
            lastName: "",
            email: "",
            requestTypes: [],
            subjectTypes: [],
            otherData: {},
            multiselectFields: {},
            daysToRespond: "",
            language: this.webForm.defaultLanguage,
            attachment: undefined,
        };
    }

    /**
     * There are still some places where country list is coming as an array of { value: USA, Key: 33 }
     * hence displayed as [object, object] in webform's country form field.
     *
     * If the country is object then formatting them to be a normal array of country names.
     */
    private modifyCountryFields() {
        const countryField = this.sidebarData.formFields.find((field) => field.fieldKey.toLowerCase() === "country");
        countryField.options = countryField.options.map((country) => {
            return typeof country !== "string" ? country.value : country;
        });
    }

    private getStyles(index: number, defaultValue: any = ""): string {
        return this.getValue(this.sidebarData.formStylingInputs, index, defaultValue);
    }

    private handleAction(type: number, payload: any): void {
        if (isFunction(this.actionCallback)) {
            this.actionCallback({type, payload});
        }
    }

    private getSettingByName(name: string, defaultValue: boolean = false): boolean {
        const setting = find(this.sidebarData.settingsInputs, ["fieldName", name]);
        return setting ? setting.value : defaultValue;
    }

    private getSetting(index: number, defaultValue: any = false): boolean {
        return this.getValue(this.sidebarData.settingsInputs, index, defaultValue);
    }

    private getValue(object: any, index: number, defaultValue: any): any {
        if (isNil(index) || isEmpty(object)) return defaultValue;
        const property: any = object[index];
        return property && property.value ? property.value : defaultValue;
    }

    private getButtonStyles(active: boolean, hover: boolean): any {
        if (active && hover) {
            return assign({}, this.styles.buttonActive, this.styles.buttonActiveHover);
        } else if (active) {
            return this.styles.buttonActive;
        } else if (hover) {
            return this.styles.buttonHover;
        }
        return {};
    }

    private answerType(fieldKey: string, type: any) {
        if (!fieldKey || !type) return;
        this.answer({ fieldKey }, type[this.typeKey]);
    }

    private isTypeAnswer(fieldKey: string, type: any): boolean {
        if (!fieldKey || !type) return false;
        return this.isAnswer({ fieldKey }, type[this.typeKey]);
    }

    private answerRecaptcha(response: string = "") {
        this.answer({ fieldKey: "recaptcha" }, response);
    }

    private answer(input: any, value: string = "") {
        if (!input || !(input.fieldKey || input.fieldName)) return;
        switch (input.fieldKey) {
            case WebformControls.FirstName:
            case WebformControls.LastName:
            case WebformControls.Email:
            case WebformControls.Language:
                this.formData[input.fieldKey] = value;
                break;
            case WebformControls.Recaptcha:
                this.recaptcha = value;
                break;
            case WebformControls.SubjectTypes:
            case WebformControls.RequestTypes:
                if (isEmpty(this.formData[input.fieldKey])) {
                    this.formData[input.fieldKey] = [value];
                } else {
                    if (this.formData[input.fieldKey].indexOf(value) > -1) {
                        pull(this.formData[input.fieldKey], value);
                    } else {
                        // remove single select of subject types as per Business requirements
                        // if (input.fieldKey === WebformControls.SubjectTypes) this.formData[input.fieldKey] = [];
                        const enableMultiSelect = input.fieldKey === "subjectTypes" ? this.settings.enableSubjectTypes : this.settings.enableRequestTypes;
                        if (enableMultiSelect) {
                            this.formData[input.fieldKey].push(value);
                        } else {
                            this.formData[input.fieldKey][0] = value;
                        }
                    }
                }
                break;
            default:
                if (value) {
                    if (input.inputType === InputType.Multiselect) {
                        let multiselectValues = [];
                        if (input.enableMultipleSelection) {
                            multiselectValues = value.split(",");
                        } else {
                            multiselectValues = [value];
                        }
                        this.formData["multiselectFields"][input.fieldKey] = multiselectValues;
                    } else {
                        this.formData.otherData[input.fieldKey] = value;
                        if (input.fieldKey === WebformControls.RequestDetails) {
                            this.requestDetailsSubmitted = true;
                        }
                    }
                } else {
                    if (input.inputType === InputType.Multiselect) {
                        delete this.formData["multiselectFields"][input.fieldKey];
                    }
                    delete this.formData.otherData[input.fieldKey];
                }
        }

        this.validateInputTypes(input, value);
    }

    private validateInputTypes(input: any, value: string) {
        switch (input.inputType) {
            case InputType.TextField:
            case InputType.Select:
            case InputType.TextArea:
            case InputType.Multiselect:
                this.validateInputText(input, value);
                break;
            default:
                this.validateForm();
        }
    }

    private isAnswer(input: any, value: string): boolean {
        if (!input || !value) return false;
        switch (input.fieldKey) {
            case WebformControls.SubjectTypes:
            case WebformControls.RequestTypes:
                return this.formData[input.fieldKey].indexOf(value) > -1;
            case WebformControls.FirstName:
            case WebformControls.LastName:
            case WebformControls.Email:
                return this.formData[input.fieldKey] === value;
            case WebformControls.Recaptcha:
                return this.recaptcha === value;
            default:
                return this.formData.otherData[input.fieldKey] === value;
        }
    }

    private getInputDataType(input: any): string {
        if (input.dataType === "Email") return "email";
        // TODO: Fill out other data types once backend sends them down.
        return "text";
    }

    private getInputClasses(input: any): string {
        let inputClass = "";
        if (input.editMode) {
            inputClass += "webform-control__edit-mode padding-all-1 ";
        }
        // if (input.fieldKey === WebformControls.FirstName || input.fieldKey === WebformControls.LastName) {
        // 	if (input.fieldKey === WebformControls.FirstName) {
        // 		inputClass += "padding-right-2 ";
        // 	}
        // 	return inputClass += "stretch-horizontal centered-50-percent ";
        // }
        return inputClass += "full-width";
    }

    private getValidationText(input: any): string {
        if (input.fieldKey === WebformControls.Email) {
            return this.$rootScope.t("ErrorEnterValidEmailAddress");
        } else if (input.fieldKey === WebformControls.FirstName) {
            return this.$rootScope.t("ErrorEnterValidFirstName");
        } else if (input.fieldKey === WebformControls.LastName) {
            return this.$rootScope.t("ErrorEnterValidLastName");
        }
        return this.$rootScope.t("ErrorFillThisField");
    }

    private validateInputText(input: any, model: string): void {
        input.isDirty = true;
        if (input.fieldKey === WebformControls.FirstName || input.fieldKey === WebformControls.LastName) {
            input.isValid = model && model.length > 0;
        } else if (input.fieldKey === WebformControls.Email) {
            input.isValid = Utilities.matchRegex(model, this.ENUMS.Regex.Email);
        } else {
            input.isValid = model && model.length > 0;
        }
        this.validateForm();
    }

    //  TODO: Keep for future if/when we decide to have other select inputs
    // private validateSelectField(input: any, model: string): void {
    //     input.isDirty = true;
    //     input.isValid = model && model.length > 0;
    // }

    private validateForm(): boolean {
        this.canSubmit = Boolean(
            !isEmpty(this.formData.subjectTypes) &&
            !isEmpty(this.formData.requestTypes) &&
            !find(this.sidebarData.formFields, (field: any): any => {
                return field.isSelected && field.status === this.requestTypeStatus.Add && field.isRequired && !field.isValid;
            }) &&
            (this.settings.recaptcha ? this.isCaptchaValid() : true) &&
            ((this.settings.recaptcha && this.settings.attachmentUpload) ? (this.attachment || !this.settings.isAttachmentUploadRequired) : true ));
        return this.canSubmit;
    }

    private isCaptchaValid(): boolean {
        if (this.settings.captchaType === DSARCaptchaType.BotdetectCaptcha) {
            if (isEmpty(this.captchaCode)) {
                this.validCaptcha = false;
                this.validBotdetectCaptcha = false;
            } else {
                this.validCaptcha = this.validBotdetectCaptcha;
            }
        } else {
            this.validCaptcha = !isNil(this.recaptcha);
        }
        return this.validCaptcha;
    }

    private doSubmit(): void {
        if (this.validateForm() && this.onSubmit) {
            this.saving = true;
            if (this.hiddenRouteParam) {
                const routeParams = this.hiddenRouteParam.split("&");
                forEach(routeParams, (routeParam: string): void => {
                    const hiddenFormField = routeParam.split("=");
                    this.formData.otherData[hiddenFormField[0]] = hiddenFormField[1];
                });
            }
            if (!this.requestDetailsSubmitted) {
                // This is a CE, text is hardcoded temporarily till there is more clarity on design.
                this.formData.otherData[WebformControls.RequestDetails] = "No additional request details provided.";
            }
            const hasOtherData: boolean = Boolean(keys(this.formData.otherData).length);
            const data: any = hasOtherData ? this.formData : omit(this.formData, "otherData");
            if (this.settings.recaptcha && (this.settings.captchaType === DSARCaptchaType.BotdetectCaptcha)) {
                if (isEmpty(this.captchaCode)) {
                    this.saving = false;
                    this.canSubmit = false;
                    return;
                }
                const botdetectCaptcha = new this.Captcha();
                this.captchaId = botdetectCaptcha.captchaId;
                this.formData.captchaId = this.captchaId;
                this.formData.captchaCode = this.captchaCode;
            }
            const formData: IWebFormData = {
                form: this.formData,
            };
            if (this.attachment) {
                const fileData: IWebformAttachment = {
                    Name: this.attachment.FileName,
                    FileName: `${this.attachment.FileName}.${this.attachment.Extension}`,
                    Extension: this.attachment.Extension,
                    IsInternal: true,
                    Type: 30,
                    Encrypt: true,
                };
                this.formData.attachment = fileData;
                formData.attachment = this.attachment.Blob;
            }
            const submitCb: ng.IPromise<any> = this.onSubmit({ data: formData });
            if (submitCb) {
                submitCb.then((response): void => {
                    this.onAfterSubmit(response);
                });
            }
        }
    }

    private onAfterSubmit(response: any): void {
        if (response.result) {
            this.isCompleted = true;
            this.subjectEmail = this.formData.email;
            this.queueId = response.data ? response.data.requestQueueRefId : "";
        }
        this.saving = false;
    }

}
const WebFormControlComponent: ng.IComponentOptions = {
    controller: WebFormControlController,
    bindings: {
        sidebarData: "<",
        formTranslations: "<",
        webForm: "<",
        onSubmit: "&?",
        actionCallback: "&",
        isExternal: "<",
        webFormLanguagesList: "<",
        widgetId: "<",
    },
    template: `
        <fieldset
            class="webform-control full-size"
            ng-class="$ctrl.fieldsetExtClass"
            ng-disabled="$ctrl.saving"
            >
            <form
                name="webForm"
                class="webform-control__form column-nowrap width-100-percent margin-bottom-1"
                ng-class="$ctrl.formExtClass"
                >
                <div
                    class="webform-control__header margin-bottom-1 padding-all-half row-horizontal-vertical-center"
                    ng-style="$ctrl.styles.header"
                    >
                    <img
                        ng-if="$ctrl.styles.image"
                        class="webform-control__header-img center"
                        id="logo-preview"
                        ng-src="{{$ctrl.styles.image}}"/>
                </div>
                <div
                    ng-if="$ctrl.settings.languageEnabled && !$ctrl.isCompleted"
                    class="padding-top-bottom-1 padding-right-2 row-flex-end"
                    >
                    <one-select
                        options="$ctrl.languagesList"
                        on-change="$ctrl.selectLanguage(selection)"
                        label-key="Name"
                        value-key="Code"
                        model="$ctrl.webForm.selectedLanguage"
                        is-disabled="$ctrl.isLanguageDisabled || !$ctrl.isExternal"
                        identifier="languageDropdown"
                    ></one-select>
                </div>
                <div class="padding-all-1 full-width centered-max-50 align-self-center" ng-if="!$ctrl.isCompleted">
                    <div
                        class="webform-control__text ql-editor padding-all-2 word-break height-auto"
                        ng-style="$ctrl.styles.welcomeText"
                        ng-bind-html="$ctrl.formTranslations['WelcomeText']"
                        >
                    </div>
                    <div
                        class="text-bold"
                        ng-style="$ctrl.styles.labelText"
                        >
                        {{$ctrl.formTranslations["SubjectTypeText"]}}
                        <sup class="helper-icon-required"></sup>
                    </div>
                    <section
                        class="margin-left-right-auto margin-top-bottom-1 row-wrap row-horizontal-center"
                        >
                        <label
                            ng-if="type.isSelected && type.status === $ctrl.requestTypeStatus.Add"
                            class="row-horizontal-vertical-center webform-control__label-button text-center text-pointer word-break"
                            ng-repeat="type in $ctrl.webForm.subjectTypes track by $index"
                            ng-style="$ctrl.getButtonStyles($ctrl.isTypeAnswer('subjectTypes', type), hover)"
                            ng-mouseover="hover=true"
                            ng-mouseleave="hover=false"
                            ng-click="$ctrl.answerType('subjectTypes', type);$event.stopPropagation();"
                            tabindex="0"
                            uib-tooltip="{{$ctrl.formTranslations[type.description]}}"
                            tooltip-placement="top-right"
                            tooltip-append-to-body="true"
                            >
                                <span class="word-wrap block flex-grow-1 flex-basis-0 max-width-full">
                                    {{$ctrl.formTranslations[type.fieldName]}}
                                </span>
                            <input
                                class="hide"
                                type="checkbox"
                                name="check"
                                ng-value="type[$ctrl.typeKey]"
                                ng-click="$event.stopPropagation()"
                            />
                        </label>
                    </section>
                    <div
                        class="text-bold"
                        ng-style="$ctrl.styles.labelText"
                        >
                        {{$ctrl.formTranslations["RequestTypeText"]}}
                        <sup class="helper-icon-required"></sup>
                    </div>
                    <section
                        class="margin-left-right-auto margin-top-bottom-1 row-wrap row-horizontal-center"
                        >
                        <label
                            ng-if="type.isSelected && type.status === $ctrl.requestTypeStatus.Add"
                            class="row-horizontal-vertical-center webform-control__label-button text-center text-pointer word-break"
                            ng-repeat="type in $ctrl.webForm.requestTypes track by $index"
                            ng-style="$ctrl.getButtonStyles($ctrl.isTypeAnswer('requestTypes', type), hover)"
                            ng-mouseover="hover=true"
                            ng-mouseleave="hover=false"
                            ng-click="$ctrl.answerType('requestTypes', type);$event.stopPropagation();"
                            tabindex="0"
                            uib-tooltip="{{ $ctrl.formTranslations[type.description] }}"
                            tooltip-placement="top-right"
                            tooltip-append-to-body="true"
                            >
                                <span class="word-wrap block flex-grow-1 flex-basis-0 max-width-full">
                                    {{$ctrl.formTranslations[type.fieldName]}}
                                </span>
                            <input
                                class="hide"
                                type="checkbox"
                                name="check"
                                ng-value="type[$ctrl.typeKey]"
                                ng-click="$event.stopPropagation();"
                                />
                        </label>
                    </section>
                    <section class="margin-left-right-auto margin-top-bottom-1 row-wrap row-space-between">
                        <div
                            class="margin-top-half"
                            ng-if="input.isSelected && input.status === $ctrl.requestTypeStatus.Add"
                            ng-repeat="input in $ctrl.sidebarData.formFields track by $index"
                            ng-if="input.isSelected"
                            ng-class="$ctrl.getInputClasses(input)"
                            >
                            <one-label-content
                                name="{{ $ctrl.formTranslations[input.fieldKey]}}"
                                label-style="$ctrl.styles.labelText"
                                is-required="input.isRequired"
                                description="{{ $ctrl.formTranslations[input.description] }}"
                                wrapper-class="word-wrap"
                                >
                                <input
                                    ng-if="(input.inputType === 'Text Field')"
                                    ng-model="input.inputValue"
                                    ng-keyup="$ctrl.debouncedInputUpdate(input, input.inputValue)"
                                    ng-blur="$ctrl.answer(input, input.inputValue)"
                                    maxlength="100"
                                    class="one-input full-width"
                                    ng-class="{'border-has-error': !input.isValid && input.isDirty && input.isRequired}"
                                    type="{{::$ctrl.getInputDataType(input)}}"
                                />
                                <textarea
                                    ng-if="(input.inputType === 'Text Area')"
                                    maxlength="{{input.fieldKey === 'requestDetails' ? 5000 : 1000}}"
                                    ng-model="input.inputValue"
                                    ng-keyup="$ctrl.debouncedInputUpdate(input, input.inputValue)"
                                    ng-blur="$ctrl.answer(input, input.inputValue)"
                                    ng-class="{'border-has-error': !input.isValid && input.isDirty && input.isRequired}"
                                    class="one-text-area full-width"
                                    rows="3"
                                ></textarea>
                                <div ng-if="(input.inputType === 'Text Area')" class="row-flex-end">
                                    {{ input.inputValue.length || 0 }} / {{ input.fieldKey === "requestDetails" ? 5000 : 1000 }}
                                </div>
                                <one-select
                                    ng-if="(input.inputType === 'Select')"
                                    options="::input.options"
                                    placeholder-text="- - - -"
                                    on-change="$ctrl.answer(input, selection)"
                                    model="selection"
                                ></one-select>
                                <one-select
                                    ng-if="(!input.enableMultipleSelection && input.displayType === $ctrl.displayType.Multiselect)"
                                    identifier="DSARSelectDropdown"
                                    placeholder-text="- - - -"
                                    label-key="value"
                                    model="selection"
                                    on-change="$ctrl.answer(input, selection)"
                                    options="input.options"
                                    value-key="key"
                                ></one-select>
                                <one-multiselect
                                    ng-if="(input.enableMultipleSelection && input.displayType === $ctrl.displayType.Multiselect)"
                                    list="input.options"
                                    identifier="DSARMultiSelectDropdown"
                                    placeholder="- - - -"
                                    label-key="value"
                                    value-key="key"
                                    model="selection"
                                    throttle-value="20"
                                    handle-select="$ctrl.handleMultiSelect(input, $model)"
                                    handle-remove="$ctrl.handleMultiSelect(input, $model)"
                                ></one-multiselect>
                                <label
                                    ng-if="input.displayType === $ctrl.displayType.Button"
                                    class="webform-control__label-button text-center text-pointer word-break"
                                    ng-repeat="option in input.options track by $index"
                                    ng-style="$ctrl.getButtonStyles($ctrl.isOptionSelected(input, option), hover)"
                                    ng-mouseover="hover=true"
                                    ng-mouseleave="hover=false"
                                    ng-click="$ctrl.handleMultiSelect(input, option);$event.stopPropagation();"
                                    tabindex="0"
                                    >
                                        <span class="word-wrap block flex-grow-1 flex-basis-0 max-width-full">
                                            {{$ctrl.formTranslations[option.key]}}
                                        </span>
                                        <input
                                            ot-auto-id="DSAROptions_{{option.key}}"
                                            class="hide"
                                            type="checkbox"
                                            ng-value="option['key']"
                                            ng-click="$event.stopPropagation();"
                                        />
                                </label>
                            </one-label-content>
                        </div>
                    </section>
                    <div class="gray-shadow padding-all-1 column-nowrap background-grey"
                        ng-class="{'border-has-error': !$ctrl.validBotdetectCaptcha && $ctrl.botdetectInputDirty}"
                        ng-if="$ctrl.settings.recaptcha
                                && ($ctrl.settings.captchaType === $ctrl.captchaType.BotdetectCaptcha)"
                        >
                        <div class="row-space-between">
                            <botdetect-captcha
                                styleName="angularBasicCaptcha"
                                ng-click="$event.stopPropagation()"
                            ></botdetect-captcha>
                            <input
                                type="text"
                                ot-auto-id="captchaCode"
                                id="captchaCode"
                                name="captchaCode"
                                ng-model="$ctrl.captchaCode"
                                class="padding-left-half"
                                correct-captcha
                                ng-blur="$ctrl.validateBotdetect(webForm, webForm.captchaCode.$pristine)"
                            ></input>
                        </div>
                    </div>
                    <div
                        ng-if="$ctrl.settings.recaptcha
                                && ($ctrl.settings.captchaType === $ctrl.captchaType.GoogleReCaptcha)"
                        class="row-flex-end"
                        vc-recaptcha
                        lang="$ctrl.webForm.selectedLanguage.split('-')[0]"
                        key="'6LfiqCUUAAAAAGzo0BG2sKBIF-oZVi1_rXgUm5xn'"
                        ng-model="$ctrl.recaptcha"
                        on-create="$ctrl.onWidgetCreate(widgetId)"
                        on-success="$ctrl.answerRecaptcha(response)"
                        on-expire="$ctrl.answerRecaptcha()"
                        >
                    </div>
                    <section class="column-space-between-center">
                        <div
                            ng-if="$ctrl.settings.recaptcha && $ctrl.settings.attachmentUpload"
                            class="webform-control__attachmentDesc margin-bottom-half text-center word-wrap max-width-full max-35 margin-top-2"
                            ng-style="$ctrl.styles.attachmentDescription"
                            >
                            {{$ctrl.formTranslations['AttachmentInfoDesc']}}
                        </div>
                        <file-input
                            ng-if="$ctrl.settings.recaptcha && $ctrl.settings.attachmentUpload"
                            class="margin-bottom-2 max-width-full"
                            input-style="$ctrl.styles.attachmentButton"
                            ng-class="{'margin-top-2' : !$ctrl.formTranslations['AttachmentInfoDesc']}"
                            allow-multiple="false"
                            max-size="::$ctrl.MAX_SIZE"
                            max-size-text="{{$ctrl.maxSizeText}}"
                            select-text="$ctrl.formTranslations['AttachmentButtonText']"
                            accept="{{$ctrl.acceptedFileTypes}}"
                            done="$ctrl.uploadFile(files)"
                            name="Data"
                            show-file-name="true"
                            show-file-list="true"
                            select-text="$ctrl.formTranslations['AttachmentButtonText']"
                            is-disabled="!$ctrl.validCaptcha"
                            is-required="$ctrl.settings.isAttachmentUploadRequired"
                            identifier="DSARWebformAttachmentSubmitButton"
                        ></file-input>
                    </section>
                    <one-button
                        type="primary"
                        class="margin-top-1 row-flex-end"
                        identifier="webformControlSubmit"
                        text="{{$ctrl.formTranslations['SubmitButtonText']}}"
                        is-disabled="!$ctrl.canSubmit"
                        button-click="$ctrl.doSubmit()"
                        enable-loading="true"
                        is-loading="$ctrl.saving"
                    ></one-button>
                    <hr class="margin-bottom-1">
                    <div class="row-flex-end" ng-if="$ctrl.settings.isLogoEnabled">
                        <img src="images/powered-by-ot.svg" class="margin-bottom-1" />
                    </div>
                </div>
                <div class="webform-control__completed align-self-center centered-max-50 margin-top-4 column-space-between text-center"
                    ng-if="$ctrl.isCompleted && !$ctrl.settings.isVerificationEnabled">
                    <section>
                        <h1 class="text-thin text-x-large margin-top-bottom-2">{{$ctrl.formTranslations["ThankYou"]}}!</h1>
                        <div class="text-thin-2 text-medium margin-top-bottom-1"
                            ng-bind-html="$ctrl.formTranslations['ThankYouText']" >
                        </div>
                        <h3 ng-if="$ctrl.subjectEmail" class="text-thin-2 text-bold margin-bottom-1">
                            {{$ctrl.subjectEmail}}
                        </h3>
                        <p ng-if="$ctrl.queueId" class="text-thin-2 text-medium margin-top-2">{{$ctrl.formTranslations["YourRequestIdIsTxt"]}}</p>
                        <h2 ng-if="$ctrl.queueId" class="text-thin-2 text-bold">
                            {{$ctrl.queueId}}
                        </h2>
                    </section>
                </div>
                <div class="webform-control__completed align-self-center margin-top-4 column-space-between text-center width-100-percent"
                    ng-if="$ctrl.isCompleted && $ctrl.settings.isVerificationEnabled">
                    <section class="align-self-center column-space-between text-center overflow-x-hidden overflow-y-auto shadow8 border-platinum min-height-85-percent max-width-30 border-radius-5">
                        <div class="webform-control__completed--verification padding-all-1 column-vertical-center">
                            <img src="images/thankyou-icon.svg" class="margin-top-1 align-self-center">
                            <div class="text-bold margin-top-1">{{$ctrl.formTranslations["OneMoreStep"]}}</div>
                            <div class="text-bold">{{$ctrl.formTranslations["AccessRequestConfirm"]}}</div>
                            <p class="margin-top-2">{{$ctrl.formTranslations["AccessClickConfirm"]}}</p>
                        </div>
                    </section>
                </div>
                <div
                   class="full-width ql-editor static-vertical centered-max-50 padding-all-2 word-break"
                   ng-style="$ctrl.styles.footerText"
                   ng-bind-html="$ctrl.formTranslations['FooterText']"
                ></div>
            </form>
        </fieldset>
    `,
};

export default WebFormControlComponent;
