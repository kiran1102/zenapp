// Angular
import {
    Component,
    Input,
    Output,
    EventEmitter,
} from "@angular/core";

// Constants & Enums
import { DSARStoreActions } from "dsarModule/enums/dsar-webform.enum";

// Interfaces
import { IStringMap } from "interfaces/generic.interface";

@Component({
    selector: "webform-add-new-input",
    templateUrl: "./webform-add-new-input.component.html",
})
export class WebformAddNewInputComponent {
    @Input() isFields: boolean;
    @Input() permissions: IStringMap<boolean>;
    @Output() inputAction = new EventEmitter();
    fieldValue: string;
    editModeEnabled = false;

    openConfiguration(): void {
        if (this.isFields) {
            this.inputAction.emit({action: DSARStoreActions.CONFIGURE_FORM_FIELD_TYPE_ENABLED, payload: null});
        } else {
            this.editModeEnabled = true;
        }
    }

    close(data?: boolean): void {
        this.fieldValue = null;
        this.editModeEnabled = false;
    }

    saveNewInput(fieldValue: string): void {
        fieldValue = fieldValue.length > 200 ? fieldValue.slice(0, 200) : fieldValue;
        this.inputAction.emit({action: DSARStoreActions.INPUT_ADDED, payload: { fieldValue, canDelete: true, isSelected: true }});
        this.close();
    }
}
