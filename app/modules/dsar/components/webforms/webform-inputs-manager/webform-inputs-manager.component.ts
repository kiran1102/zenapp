import { WebformStore } from "modules/dsar/webform.store";
// Angular
import {
    Component,
    SimpleChanges,
    OnChanges,
    Input,
    Output,
    EventEmitter,
} from "@angular/core";

// 3rd Party
import {
    findIndex,
    filter,
    some,
} from "lodash";

// Constants & Enums
import { RequestTypeStatus } from "dsarModule/enums/web-form.enum";
import { DSARStoreActions } from "dsarModule/enums/dsar-webform.enum";

// Interfaces
import { IStringMap } from "interfaces/generic.interface";
import { IWedformInput } from "dsarModule/components/webforms/shared/interfaces/webform-input.interface";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

// Services
import { NotificationService } from "modules/shared/services/provider/notification.service";

@Component({
    selector: "webform-inputs-manager",
    templateUrl: "./webform-inputs-manager.component.html",
})
export class WebformInputsManagerComponent implements OnChanges {

    @Input() permissions: IStringMap<boolean>;
    @Input() inputs: IWedformInput[];
    @Input() hasEditAccess: boolean;
    @Input() isFields: boolean;
    @Input() isRequestTypes: boolean;
    @Input() isSubjectTypes: boolean;
    @Input() formTranslations: IStringMap<string>;
    @Output() actionCallback = new EventEmitter();
    dsarStoreActions = DSARStoreActions;
    requestTypeStatusEnum = RequestTypeStatus;
    inputCount: number;

    constructor(
        private dsarWebFormStore: WebformStore,
        private translatePipe: TranslatePipe,
        private notificationService: NotificationService,
    ) { }

    ngOnChanges() {
        if (this.inputs) {
            this.inputCount = this.inputs.length;
        }
    }

    upgradedInputsChanged(data: {action: number, payload: null | { fieldValue: string, canDelete: boolean, isSelected: boolean}}) {
        this.inputsChanged(data.action, data.payload);
    }

    inputsChanged(action: number, input?: any, index?: number): void {
        const panelName: string = this.isFields ? "formFields" : "webForm";
        const panelProperty: string = this.isRequestTypes ? "requestTypes" : this.isSubjectTypes ? "subjectTypes" : "data";
        const toDirty: string = panelProperty === "data" ? "formFields" : panelProperty;
        switch (action) {
            case DSARStoreActions.INPUT_TOGGLED:
                if (this.isFields) {
                    input.isSelected = !input.isSelected;
                    this.handleAction(DSARStoreActions.INPUT_TOGGLED, { panelName, toDirty });
                    break;
                }
                const selectedInputs = filter(this.inputs, {status: this.requestTypeStatusEnum.Add, isSelected: true});
                if (selectedInputs.length > 1 || !input.isSelected) {
                    input.isSelected = !input.isSelected;
                    this.handleAction(DSARStoreActions.INPUT_TOGGLED, { panelName, toDirty });
                } else {
                    if (this.isRequestTypes) {
                        this.notificationService.alertWarning(this.translatePipe.transform("Alert"), this.translatePipe.transform("PleaseAddRequestType"));
                    } else if (this.isSubjectTypes) {
                        this.notificationService.alertWarning(this.translatePipe.transform("Alert"), this.translatePipe.transform("PleaseAddSubjectType"));
                    }
                }
                break;
            case DSARStoreActions.INPUT_DELETED:
                if (input.canDelete) {
                    let isLastSelected: boolean;
                    if (input.isSelected) {
                        isLastSelected = filter(this.inputs, {status: this.requestTypeStatusEnum.Add, isSelected: true}).length === 1;
                    }
                    input.status = this.requestTypeStatusEnum.Delete;
                    if ( !this.isFields
                            && (!some(this.inputs, {status: this.requestTypeStatusEnum.Add})
                                || isLastSelected )) {
                        input.status = this.requestTypeStatusEnum.Add;
                        this.notificationService.alertWarning(this.translatePipe.transform("Alert"), this.translatePipe.transform("AtleastOneEntityShouldBePresent"));
                        this.handleAction(DSARStoreActions.ERROR_FORM, {payload: { errorOnForm: true } });
                        break;
                    }
                    this.handleAction(DSARStoreActions.INPUT_DELETED, { panelName, panelProperty, data: input, toDirty });
                }
                break;
            case DSARStoreActions.INPUT_ADDED:
                if (input.fieldValue && this.isInputUnique(input)) {
                    const keyIdentifier = this.inputCount + 1;
                    input.canDelete = true;
                    input.status = this.requestTypeStatusEnum.Add;
                    if (this.isFields) {
                        input.inputType = "Text Field";
                        input.fieldKey = "formField" + keyIdentifier;
                        input.fieldName = input.fieldValue;
                        input.description = "formField" + keyIdentifier + "Desc";
                        input.isRequired = false;
                        input.isMasked = false;
                        input.isInternal = false;
                        // set the dirtyKey property on the input to know which property (fieldName, fieldValue, descriptionValue) got changed
                        input.dirtyKey = "fieldName";
                    } else {
                        input.order = keyIdentifier;
                        if (this.isSubjectTypes) {
                            input.fieldName = "SubjectType" + keyIdentifier;
                        } else {
                            input.fieldName = "RequestType" + keyIdentifier;
                        }
                        input.description = input.fieldName + "Desc";
                        // set the dirtyKey property on the input to know which property (fieldName, fieldValue, descriptionValue) got changed
                        input.dirtyKey = "fieldValue";
                    }
                    this.handleAction(DSARStoreActions.INPUT_ADDED, { panelName, panelProperty, data: input, toDirty });
                }
                break;
            case DSARStoreActions.INPUT_EDIT:
                if (this.isFields) {
                    this.handleAction(DSARStoreActions.CONFIGURE_FORM_FIELD_ENABLED, { configureFormField: true, selectedFormField: input });
                } else {
                    this.handleAction(DSARStoreActions.EDIT_SIDEBAR_ENABLED, { editSidebarEnabled: true });
                    this.handleAction(DSARStoreActions.SET_EDIT_INPUT, { selectedEditInput: `${panelName}.${panelProperty}[${index}]` });
                }

                break;
            case DSARStoreActions.INPUT_MOVE_UP:
                if (index !== 0) {
                    const dataArr: any[] = this.dsarWebFormStore.DSARState[panelName][panelProperty];
                    this.swap(dataArr, index, index - 1);
                    this.handleAction(DSARStoreActions.INPUT_MOVE, { panelName, panelProperty, data: dataArr, toDirty });
                }
                break;
            case DSARStoreActions.INPUT_MOVE_DOWN:
                if (index !== this.inputs.length - 1) {
                    const dataArr: any[] = this.dsarWebFormStore.DSARState[panelName][panelProperty];
                    this.swap(dataArr, index, index + 1);
                    this.handleAction(DSARStoreActions.INPUT_MOVE, { panelName, panelProperty, data: dataArr, toDirty });
                }
                break;
            case DSARStoreActions.CONFIGURE_FORM_FIELD_TYPE_ENABLED:
                this.handleAction(DSARStoreActions.CONFIGURE_FORM_FIELD_TYPE_ENABLED, { configureFormFieldTypeEnabled: true });
                break;
            default:
                break;
        }
    }

    handleAction(type: number, payload: any): void {
        this.actionCallback.emit({ type, payload });
    }

    swap(array: any, index1: any, index2: any): void {
        if (!this.isFields) {
            [array[index1].order, array[index2].order] = [array[index2].order, array[index1].order];
        }
        [array[index1], array[index2]] = [array[index2], array[index1]];
    }

    isInputUnique(newInput: any): boolean {
        let index = 0;
        if (this.isFields) {
            index = findIndex( this.inputs, (input: any): boolean => {
                return this.formTranslations[input.fieldKey] === newInput.fieldValue && input.status !== this.requestTypeStatusEnum.Delete;
            });
        } else {
            index = findIndex( this.inputs, (input: any): boolean => {
                return this.formTranslations[input.fieldName] === newInput.fieldValue && input.status !== this.requestTypeStatusEnum.Delete;
            });
        }
        return index === -1;
    }

    getIndex(input: any, index: number): number {
        if (this.isFields) {
            return input.fieldKey.includes("formField") ? input.fieldKey.slice(9) - 1 : index;
        } else {
            return input.fieldName.slice(11) - 1;
        }
    }
}
