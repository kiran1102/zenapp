// Angular
import { Component } from "@angular/core";

// Rxjs
import { Subject } from "rxjs";

// External
import { IOtModalContent } from "@onetrust/vitreus";

// Interfaces

@Component({
    selector: "dsar-webform-auto-publish-modal",
    templateUrl: "./dsar-webform-auto-publish-modal.component.html",
})
export class DsarWebformAutoPublishModal implements IOtModalContent {
    dismissNotification = false;
    otModalCloseEvent: Subject<boolean>;
    otModalData: number;
    formsCount: number;

    constructor() {}

    ngOnInit(): void {
        this.formsCount = this.otModalData;
    }

    closeModal() {
        this.otModalCloseEvent.next(this.dismissNotification);
    }

    disableNotification(): void {
        this.dismissNotification = !this.dismissNotification;
    }
}
