// Angular
import { Component } from "@angular/core";

// 3rd Party
import { Subject } from "rxjs";

// Interfaces
import { IOtModalContent } from "@onetrust/vitreus";

@Component({
    selector: "prepublish-confirmation-modal",
    templateUrl: "./prepublish-confirmation-modal.component.html",
})
export class PrepublishConfirmationModal implements IOtModalContent {
    otModalData: any;
    otModalCloseEvent: Subject<boolean>;

    closeModal(closedModalEvent: any) {
        this.otModalCloseEvent.next(closedModalEvent);
    }
}
