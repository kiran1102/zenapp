// Angular
import {
    Component,
    Input,
    Output,
    EventEmitter,
    OnInit,
    OnChanges,
    SimpleChanges,
} from "@angular/core";

// 3rd Party
import { filter, cloneDeep } from "lodash";

// Constants & Enums
import { DSARStoreActions } from "dsarModule/enums/dsar-webform.enum";
import {
    DSARWebformSettingsLabel,
} from "dsarModule/enums/dsar-webform.enum";

// Intefaces
import { IStringMap } from "interfaces/generic.interface";
import { IWebFormSettingsInput } from "dsarModule/interfaces/webform.interface";
import { IPermissions } from "sharedModules/interfaces/permissions.interface";

// Services
import { WebformStore } from "modules/dsar/webform.store";

@Component({
    selector: "webform-sidebar",
    templateUrl: "./webform-sidebar.component.html",
})
export class WebFormSidebarComponent implements OnInit, OnChanges {

    @Input() permissions: IPermissions;
    @Input() formTranslations: IStringMap<string>;
    @Input() sidebarData: any;
    @Input() webForm: any;
    @Output() actionCallback = new EventEmitter();

    isAttachmentSectionEnabled = false;
    newFormStylingInputs: any;
    isLanguageSwitchEnabled: boolean;
    isLogoEnabled: boolean;
    languagesList: any[];
    languageAndLogoInputs: IWebFormSettingsInput[];
    modelForLanguageSelector: any;

    constructor(
        private dsarWebFormStore: WebformStore,
    ) {}

    ngOnInit(): void {
        this.isAttachmentSectionEnabled = this.sidebarData.settingsInputs.find(
            (input: IWebFormSettingsInput) =>
            input.fieldName === DSARWebformSettingsLabel.AttachmentSubmission).value;
        this.languageAndLogoInputs = filter(
            this.sidebarData.settingsInputs,
            ((input: IWebFormSettingsInput): boolean =>
            (input.fieldName === DSARWebformSettingsLabel.TranslateWebForm ||
                input.fieldName === DSARWebformSettingsLabel.ShowOneTrustLogo)));
        this.isLanguageSwitchEnabled = this.languageAndLogoInputs[0].value;
        this.isLogoEnabled = this.languageAndLogoInputs[1] ? this.languageAndLogoInputs[1].value : true;
        if (!this.webForm.viewFirst) {
            this.webForm.viewFirst = true;
            this.webForm.selectedLanguage = this.webForm.defaultLanguage;
            this.selectLanguage(this.webForm.selectedLanguage);
        }
        this.newFormStylingInputs = cloneDeep(this.sidebarData.formStylingInputs);
        this.setLanguage();
    }

    ngOnChanges(changes: SimpleChanges): void {
        this.languagesList = filter(this.webForm.webformLanguagesList , ["Status", 10]);
        if (this.webForm.languageModified) {
            this.webForm.languageModified = false;
            this.webForm.currentLanguage = this.webForm.selectedLanguage = this.webForm.defaultLanguage;
            this.selectLanguage(this.webForm.currentLanguage);
        } else if (this.webForm.selectedLanguage !==  this.webForm.currentLanguage) {
            this.webForm.currentLanguage = this.webForm.selectedLanguage;
            this.selectLanguage(this.webForm.currentLanguage);
        }
    }

    /**
     *  On Editing any fields or form types the language to the picklist seems to be missing.
     *  This method sets the language to the picklist.
     */
    setLanguage() {
        this.modelForLanguageSelector = this.languagesList.find((lang) => lang.Code === this.webForm.currentLanguage);
    }

    selectLanguage(language: string): void {
        this.modelForLanguageSelector = this.languagesList.find((lang) => lang.Code === language);
        this.webForm.currentLanguage = language;
        this.webForm.selectedLanguage = language;
        this.handleAction({type: DSARStoreActions.LANGUAGE_SWITCH, payload: { language }});
    }

    inputChanged(inputData: string): void {
        this.handleAction({ type: DSARStoreActions.DEFAULT_WEBFORM_CHANGED, payload: { fieldName: "templateName", value: inputData }});
    }

    handleAction(action: {type: number, payload: any} ): void {
        this.actionCallback.emit({type: action.type, payload: action.payload});
    }

    toggleLanguageSwitch(): void {
        this.isLanguageSwitchEnabled = !this.isLanguageSwitchEnabled;
        this.dsarWebFormStore.DSARState.published = false;
        this.handleAction({ type: DSARStoreActions.INPUT_CHANGED, payload: {
            panelName: "settingsInputs",
            fieldName: DSARWebformSettingsLabel.TranslateWebForm,
            value: this.isLanguageSwitchEnabled }});
        if (!this.isLanguageSwitchEnabled && this.webForm.defaultLanguage !== "en-us") {
            this.handleAction({type: DSARStoreActions.LANGUAGE_SWITCH, payload: { language: this.webForm.defaultLanguage }});
        }
    }

    editLanguage(): void {
        this.handleAction({ type: DSARStoreActions.LANG_EDIT_MODAL_OPEN, payload: {}});
    }
}
