// Angular
import {
    Component,
    Input,
    Output,
    OnInit,
    EventEmitter,
} from "@angular/core";
import {
    FormGroup,
    FormBuilder,
    Validators,
    FormControl,
    FormArray,
    ValidationErrors,
} from "@angular/forms";

// Interfaces
import {
    IFormAction,
} from "dsarModule/interfaces/webform.interface";
import { IDSARFormField } from "@onetrust/dsar-components";
import { RequestTypeStatus } from "dsarModule/enums/web-form.enum";
import {
    InputType,
    DisplayType,
    WebformControls,
 } from "dsarModule/constants/web-form-control.constant";
import {
    IStringMap,
    IKeyValue,
    ILabelValue,
} from "interfaces/generic.interface";

// Enums & Constants
import { NO_BLANK_SPACE } from "constants/regex.constant";
import {
    DSARStoreActions,
} from "dsarModule/enums/dsar-webform.enum";

// Services
import { NotificationService } from "modules/shared/services/provider/notification.service";

import {
    find,
    keys,
    startsWith,
    maxBy,
    each,
    map,
 } from "lodash";

// Pipe
import { TranslatePipe } from "pipes/translate.pipe";

@Component({
    selector: "form-field-configure",
    templateUrl: "./form-field-configure.component.html",
})
export class FormFieldConfigureComponent implements OnInit {
    @Input() field: IDSARFormField;
    @Input() formTranslations: IStringMap<string>;
    @Input() allFormFields: IDSARFormField[];
    @Output() handleAction = new EventEmitter<IFormAction>();

    changedControls = new Set<string>();
    initialFieldState: IDSARFormField;
    isEdit: boolean;
    showOptions = { internal: true, required: true, masked: true};
    dsarStoreActions = DSARStoreActions;
    enableMultiSelection = false;
    dirtyKey = "";
    highestOptionIndex = 0;
    formFieldGroup: FormGroup;
    displayTypeOptions = [
        {
            label: this.translatePipe.transform("Selection"),
            value: InputType.Multiselect,
        },
        {
            label: this.translatePipe.transform("Button"),
            value: DisplayType.Button,
        },
    ];
    selectedDisplayType = {
        label: this.translatePipe.transform("Selection"),
        value: InputType.Multiselect,
    };
    displayType = DisplayType;

    constructor(
        public translatePipe: TranslatePipe,
        private formBuilder: FormBuilder,
        private notificationService: NotificationService,
    ) {}

    ngOnInit(): void {
        this.isEdit = Boolean(this.formTranslations[this.field.fieldKey]);
        this.initForm();
        this.formFieldGroup.valueChanges.subscribe((values: any): void => {
            const options = this.formFieldGroup.controls["options"] as FormArray;
            if (options && options.errors && options.invalid) {
                this.generateAlertMessage("DuplicateOption");
            }
            if (options && options.dirty && options.valid) {
                this.dirtyKey = "options";
                this.changedControls.add(this.dirtyKey);
                this.field.options = options.value;
            }
            const label = this.formFieldGroup.controls["label"] as FormGroup;
            if (label.dirty && this.field.fieldName !== label.value && label.valid) {
                this.dirtyKey = "fieldName";
                this.changedControls.add(this.dirtyKey);
                this.field.fieldName = label.value;
            }
            const description = this.formFieldGroup.controls["description"];
            if (description.dirty && this.field.descriptionValue !== description.value) {
                this.dirtyKey = "descriptionValue";
                this.changedControls.add(this.dirtyKey);
                this.field.descriptionValue = description.value;
            }
            this.field.isInternal = values.isInternal;
            this.field.isRequired = values.isRequired;
            this.field.isMasked = values.isEmailMasked;
            this.field.enableMultipleSelection = values.isMultiselect;
            this.changeFormFields(this.dirtyKey);
        });

        this.hideFields();
    }

    hideFields() {
        if (this.field.fieldKey === WebformControls.FirstName || this.field.fieldKey === WebformControls.LastName) {
            this.showOptions = { internal: false, required: false, masked: true};
        } else if (this.field.fieldKey === WebformControls.Email) {
            this.showOptions = { internal: false, required: false, masked: false};
        }
    }

    initForm(): void {
        const formField = this.field;
        this.initialFieldState = Object.assign({}, this.field);
        const label = this.formTranslations[formField.fieldKey] || "";
        const description = this.formTranslations[formField.description] || "";
        this.initialFieldState.fieldName = label;
        this.initialFieldState.descriptionValue = description;
        this.formFieldGroup = this.formBuilder.group({
            label: new FormControl(label || "", [
                Validators.compose([Validators.required, Validators.pattern(NO_BLANK_SPACE), this.duplicateLabelValidator]),
            ]),
            description: new FormControl(description, [
                Validators.maxLength(500),
            ]),
            isInternal: new FormControl(formField.isInternal || false),
            isRequired:  new FormControl(formField.isRequired || false),
            isEmailMasked: new FormControl(formField.isMasked || false),
            isMultiselect: new FormControl(formField.enableMultipleSelection || false),
        });
        if (formField.inputType === InputType.Multiselect) {
            if (formField.options) {
                this.calculateHighestIndex();
            }
            if (formField.displayType) {
                this.selectedDisplayType = find(this.displayTypeOptions, {value: formField.displayType});
            }
            this.formFieldGroup.addControl("options", this.fetchFieldOptions(this.formBuilder));
            this.initialFieldState.options = this.formFieldGroup.controls["options"].value;
        }
    }

    duplicateLabelValidator = (labelControl: FormControl): ValidationErrors | null => {
        const label = labelControl.value;
        const formField = find(this.allFormFields, (field: IDSARFormField): boolean => {
            return this.formTranslations[field.fieldKey] === label
                    && field.status !== RequestTypeStatus.Delete
                    && field.fieldKey !== this.field.fieldKey;
        });
        if (formField) {
            this.generateAlertMessage("DuplicateLabel");
            return { duplicateLabel: true };
        }
        return null;
    }

    duplicateOptionValidator = (optionControl: FormControl): ValidationErrors | null => {
        const optionLabels = optionControl.value.map((option) => option.value);
        const uniqueOptionLabels = new Set(optionLabels);
        if (optionLabels.length !== uniqueOptionLabels.size) {
            return { duplicateOptions: true };
        }
        return null;
    }

    fetchFieldOptions(formBuilder: FormBuilder): FormArray {
        if (this.field.options) {
            return formBuilder.array(map(this.field.options, (option: IKeyValue<string>): FormGroup => {
                return formBuilder.group({
                    key: option.key,
                    value: [option.value, [Validators.required, Validators.pattern(NO_BLANK_SPACE)]],
                });
            }), this.duplicateOptionValidator);
        } else {
                return formBuilder.array([formBuilder.group({
                    key: `${this.field.fieldKey}Opt${this.highestOptionIndex}`,
                    value: ["", [Validators.required, Validators.pattern(NO_BLANK_SPACE)]],
                }),
            ], this.duplicateOptionValidator);
        }
    }

    calculateHighestIndex() {
        const indexArr = [];
        const trimSize = this.field.fieldKey.length + 3;
        each(keys(this.formTranslations), (key: string): void => {
            if (startsWith(key, this.field.fieldKey + "Opt")) {
                indexArr.push(parseInt(key.substr(trimSize - key.length), 10));
            }
        });
        this.highestOptionIndex = maxBy(indexArr);
    }

    addCondition(): void {
        this.highestOptionIndex += 1;
        const control = this.formFieldGroup.controls["options"] as FormArray;
        control.push(this.formBuilder.group({
            key: `${this.field.fieldKey}Opt${this.highestOptionIndex}`,
            value: ["", [Validators.required, Validators.pattern(NO_BLANK_SPACE)]],
        }));
    }

    removeCondition(index: number): void {
        const control = this.formFieldGroup.controls["options"] as FormArray;
        control.removeAt(index);
        this.field.options.splice(index, 1);
    }

    saveFormField(): void {
        if (!this.formFieldGroup.invalid) {
            this.field.editMode = false;
            this.handleAction.emit({
                type: DSARStoreActions.CONFIGURE_FORM_FIELD_DISABLED,
                payload: {
                    configureFormField: false,
                    selectedFormField: "",
                    formDirty: true,
                },
            });
        }
    }

    generateAlertMessage(alertMessage: string): void {
        this.notificationService.alertWarning(this.translatePipe.transform("Alert"), this.translatePipe.transform(alertMessage));
    }

    changeFormFields(key: string): void {
        this.handleAction.emit({
            type: DSARStoreActions.FORM_FIELD_EDIT,
            payload: {
                panelName: "formFields",
                dirtyKey: key,
                data: this.field,
                errorOnForm: this.formFieldGroup.invalid,
            },
        });
    }

    selectDisplayType(type: ILabelValue<string>): void {
        this.selectedDisplayType = type;
        this.field.displayType = this.selectedDisplayType.value;
    }

    cancelFormField() {
        if (this.isEdit) {
            Object.assign(this.field, this.initialFieldState);
            this.changedControls.forEach((control: string) => this.changeFormFields(control));
        } else {
            this.handleAction.emit({
                type: DSARStoreActions.DELETE_ADDED_INPUT,
                payload: {
                    panelName: "formFields",
                    panelProperty: "data",
                    data: this.field,
                    errorOnForm: false,
                    dirtyKeys: this.changedControls,
                },
            });
        }
        this.handleAction.emit({
            type: DSARStoreActions.CONFIGURE_FORM_FIELD_DISABLED,
            payload: {
                configureFormField: false,
                selectedFormField: "",
                formDirty: true,
            },
        });
    }
}
