import { WebformStore } from "modules/dsar/webform.store";
// Angular
import {
    Component,
    OnInit,
    Input,
    Output,
    EventEmitter,
} from "@angular/core";

import {
    get,
} from "lodash";

// Interfaces
import { IStringMap } from "interfaces/generic.interface";

// Constants and enums
import { DSARAttachment } from "dsarModule/constants/dsar-settings.constant";
import { DSARStoreActions } from "dsarModule/enums/dsar-webform.enum";

// Services
import { NotificationService } from "modules/shared/services/provider/notification.service";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

@Component({
    selector: "webform-sidebar-edit",
    templateUrl: "./webform-sidebar-edit.component.html",
})
export class WebformSidebarEditComponent implements OnInit {
    @Input() formTranslations: IStringMap<string>;
    @Input() selectedEditInput: string;
    @Output() actionCallback = new EventEmitter();

    dsarStoreActions = DSARStoreActions;
    isInternal: boolean;
    errorOnField = false;
    errorOnAttachmentSize = false;
    errorOnSubmitButton = false;
    nonEmptyFields: string[] = [
        DSARAttachment.AttachmentSizeDesc,
        DSARAttachment.SubmitButtonText,
        DSARAttachment.AttachmentButtonText,
    ];

    inputToEdit: any;
    panelName: string;
    panelProperty: string;
    toDirty: string;
    toggleIsRequired: boolean;
    toggleIsMasked: boolean;
    hasDescriptionProperty: boolean;

    constructor(
        private dsarWebFormStore: WebformStore,
        private translatePipe: TranslatePipe,
        private notificationService: NotificationService,
    ) {}

    ngOnInit() {
        this.inputToEdit = this.getEditInput();
        // BE was not able to set NULL as it is a JSON string there.
        if (this.selectedEditInput.includes("formFields")) {
            this.inputToEdit.descriptionValue = null;
        }
        this.inputToEdit.editMode = true;
        this.configureInputProperty();
    }

    getEditInput(): any {
        return get(this.dsarWebFormStore.DSARState, this.selectedEditInput);
    }

    configureInputProperty(): void {
        const isFields: boolean =
            this.selectedEditInput.indexOf("formFields") > -1;
        const isSubjectTypes: boolean =
            this.selectedEditInput.indexOf("subjectTypes") > -1;
        const isRequestTypes: boolean =
            this.selectedEditInput.indexOf("requestTypes") > -1;
        this.panelName = isFields ? "formFields" : "webForm";
        this.panelProperty = isRequestTypes
            ? "requestTypes"
            : isSubjectTypes
            ? "subjectTypes"
            : "data";
        this.toDirty = this.panelProperty === "data" ? "formFields" : this.panelProperty;
        this.toggleIsRequired =
            this.inputToEdit.hasOwnProperty("isRequired") &&
            !this.inputToEdit.cannotUnselect;
        this.toggleIsMasked = this.inputToEdit.hasOwnProperty("isMasked");
        this.hasDescriptionProperty = this.inputToEdit.hasOwnProperty(
            "description",
        );
        this.isInternal = this.inputToEdit.hasOwnProperty("isInternal");
    }

    handleAction(type: number, payload: any): void {
        if (type === DSARStoreActions.EDIT_SIDEBAR_DISABLED) {
            this.inputToEdit.editMode = false;
        }
        this.actionCallback.emit({ type, payload });
    }

    inputChanged(value: boolean | string, key: string): void {
        let dirtyKey = key;
        if (key === "fieldName") {
            dirtyKey = this.panelName === "formFields" ? "fieldName" : "fieldValue";
        } else if (key === "description") {
            dirtyKey = "descriptionValue";
        }
        this.inputToEdit[dirtyKey] = value;
        // set the dirtyKey property on the input to know which property (fieldName, fieldValue, descriptionValue) got changed
        this.inputToEdit["dirtyKey"] = dirtyKey;
        this.handleAction(DSARStoreActions.INPUT_EDIT, {
            panelName: this.panelName,
            panelProperty: this.panelProperty,
            data: this.inputToEdit,
            toDirty: this.toDirty,
        });
    }

     attachmentInfoChanged(
        value: string,
        key: string,
        type: number,
    ): void {
        switch (type) {
            case DSARStoreActions.DEFAULT_WEBFORM_CHANGED:
                switch (key) {
                    case DSARAttachment.SubmitButtonText:
                        if (!value.length) {
                            this.errorOnSubmitButton = true;
                            this.notificationService.alertWarning(
                                this.translatePipe.transform("Alert"),
                                this.translatePipe.transform(
                                    "leaseAddSubmitButtonText",
                                ),
                            );
                            this.handleAction(DSARStoreActions.ERROR_FORM, {
                                errorOnForm: true,
                            });
                        } else {
                            this.errorOnSubmitButton = false;
                            this.onSuccessEntry(value, key);
                        }
                        break;
                }
                break;
            case DSARStoreActions.INPUT_CHANGED:
                this.inputToEdit.isRequired = !this.inputToEdit.isRequired;
                this.handleAction(DSARStoreActions.INPUT_CHANGED, {
                    panelName: "settingsInputs",
                    fieldName: "AttachmentSubmission",
                    isRequired: this.inputToEdit.isRequired,
                    value: true,
                });
                this.handleAction(DSARStoreActions.ATTACHMENT_TOGGLE_CHANGED, {
                    isRequired: this.inputToEdit.isRequired,
                });
                break;
        }
    }

     onSuccessEntry(value: string, key: string): void {
        this.handleAction(DSARStoreActions.ERROR_FORM, {
            errorOnForm:
                this.errorOnAttachmentSize ||
                this.errorOnSubmitButton ||
                this.errorOnField,
        });
        this.handleAction(DSARStoreActions.DEFAULT_WEBFORM_CHANGED, {
            fieldName: key,
            value,
        });
    }

     inputNameChanged(value: string, key: string): void {
        if (!value.length && key !== "description") {
            this.errorOnField = true;
            this.notificationService.alertWarning(
                this.translatePipe.transform("Alert"),
                this.translatePipe.transform("PleaseAddFieldName"),
            );
            this.handleAction(DSARStoreActions.ERROR_FORM, {
                errorOnForm: true,
            });
        } else {
            this.errorOnField = false;
            this.inputChanged(value, key);
            this.handleAction(DSARStoreActions.ERROR_FORM, {
                errorOnForm: false,
            });
        }
    }
}
