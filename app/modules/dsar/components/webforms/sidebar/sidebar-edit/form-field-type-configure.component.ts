// Angular
import {
    Component,
    Input,
    Output,
    OnInit,
    EventEmitter,
} from "@angular/core";

// Interfaces
import {
    IFormAction,
} from "dsarModule/interfaces/webform.interface";
import {
    ILabelValue,
} from "interfaces/generic.interface";

// Enums & Constants
import {
    DSARStoreActions,
} from "dsarModule/enums/dsar-webform.enum";
import {
    InputType,
 } from "dsarModule/constants/web-form-control.constant";

// Pipe
import { TranslatePipe } from "pipes/translate.pipe";

@Component({
    selector: "form-field-type-configure",
    templateUrl: "./form-field-type-configure.component.html",
})
export class FormFieldTypeConfigureComponent implements OnInit {
    @Input() count: number;
    @Output() handleAction = new EventEmitter<IFormAction>();
    options = [
        {
            label: this.translatePipe.transform("UserInput"),
            value: InputType.TextField,
        },
        {
            label: this.translatePipe.transform("Selection"),
            value: InputType.Multiselect,
        },
    ];
    selectedFieldType = {
        label: this.translatePipe.transform("UserInput"),
        value: InputType.TextField,
    };
    dsarStoreActions = DSARStoreActions;
    typeLabel = "UserInputDescription";
    typeDescription = "UserInputDescriptionText";

    constructor(
        public translatePipe: TranslatePipe,
    ) { }

    ngOnInit(): void {}

    createFormField(): void {
        const formField = {
            fieldKey: "formField" + (this.count + 1) ,
            description: "formField" + (this.count + 1) + "Desc",
            canDelete: true,
            inputType: this.selectedFieldType.value,
            status: 10,
            isSelected: true,
        };
        this.handleAction.emit({
            type: DSARStoreActions.CONFIGURE_INPUT_TYPE,
            payload: formField,
        });
        this.handleAction.emit({
            type: DSARStoreActions.CONFIGURE_FORM_FIELD_ENABLED,
            payload: {
                configureFormField: true,
                selectedFormField: formField,
            },
        });
    }

    selectFieldType(type: ILabelValue<string>): void {
        this.selectedFieldType = type;
        if (this.selectedFieldType.value === InputType.TextField) {
            this.typeLabel = "UserInputDescription";
            this.typeDescription = "UserInputDescriptionText";
        } else {
            this.typeLabel = "SelectionDescription";
            this.typeDescription = "SelectionDescriptionText";
        }
    }

    closeSidebar(): void {
        this.handleAction.emit({type: this.dsarStoreActions.CONFIGURE_FORM_FIELD_TYPE_DISABLED});
    }
}
