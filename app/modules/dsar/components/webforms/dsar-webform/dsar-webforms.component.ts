// Angular
import {
    Component,
    OnInit,
    Inject,
} from "@angular/core";
import { StateService } from "@uirouter/core";
import { IPromise } from "angular";

// 3rd Party
import { map } from "lodash";

// Interfaces
import { IProtocolResponse } from "interfaces/protocol.interface";
import { IDropdownOption } from "interfaces/one-dropdown.interface";
import { IDeleteConfirmationModalResolve } from "interfaces/delete-confirmation-modal-resolve.interface";
import {
    IWebFormItem,
    IWebformListItem,
    IWebformAutoPublish,
} from "dsarModule/interfaces/webform.interface";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

// Rxjs
import { concatMap, take } from "rxjs/operators";
import { of } from "rxjs";

// Services
import { Permissions } from "modules/shared/services/helper/permissions.service";
import { WebformApiService } from "dsarservice/webform-api.service";
import { ModalService } from "sharedServices/modal.service";
import OrgGroupStoreNew from "oneServices/org-group.store";
import { OtModalService, ModalSize } from "@onetrust/vitreus";

// Components
import { DsarWebformAutoPublishModal } from "dsarModule/components/webforms/dsar-webform-auto-publish-modal/dsar-webform-auto-publish-modal.component";

@Component({
    selector: "dsar-webforms",
    templateUrl: "./dsar-webforms.component.html",
})
export class DSARWebFormsComponent implements OnInit {
    ready = false;
    webFormsList: IWebformListItem[] = [];
    actions: IDropdownOption[];
    canAddWebForm = this.permissions.canShow("DSARWebFormsCreate");
    showNewWebForm = this.permissions.canShow("DSARWebFormPublishV2");

    private deleteModalData: IDeleteConfirmationModalResolve = {
        promiseToResolve: null,
        modalTitle: this.translatePipe.transform("DeleteWebForm"),
        confirmationText: this.translatePipe.transform("AreYouSureDeleteWebForm"),
        submitButtonText: this.translatePipe.transform("Yes"),
        cancelButtonText: this.translatePipe.transform("No"),
    };

    constructor(
        private stateService: StateService,
        @Inject("OrgGroupStoreNewService") private readonly orgGroupStoreNew: OrgGroupStoreNew,
        private readonly modalService: ModalService,
        private readonly permissions: Permissions,
        private dsarWebformService: WebformApiService,
        private readonly translatePipe: TranslatePipe,
        private otModalService: OtModalService,
    ) {}

    ngOnInit() {
        this.getAllWebForms();
        if (this.showNewWebForm) {
            this.migrateWebForms();
        }
    }

    migrateWebForms(): void {
        this.ready = false;
        this.dsarWebformService.migrateWebForms().pipe(
            concatMap((value: IWebformAutoPublish) => {
                if (!value.dismissed && value.migratedCount) {
                    return this.otModalService.create(DsarWebformAutoPublishModal, {
                        isComponent: true,
                        size: ModalSize.MEDIUM,
                    }, value.migratedCount);
                } else {
                   return of(null);
                }
            }), concatMap((value: boolean | null, index: number) => {
                return value ? this.dsarWebformService.disableAutoPublishNotification() : of(null);
            }), take(1)).subscribe();
    }

    getAllWebForms(): void {
        this.ready = false;
        this.dsarWebformService.getWebForms(this.orgGroupStoreNew.selectedOrg.id).
            then((response: IProtocolResponse<IWebFormItem[]>): void => {
                if (response.result) {
                    this.webFormsList = map(response.data, (webFormItem: IWebFormItem): IWebformListItem => {
                        webFormItem.orgName = this.orgGroupStoreNew.rootOrgTable[webFormItem.orgGroupId].name;
                        const dropdownList: IDropdownOption[] = this.getDropdownOptions(webFormItem);
                        return {...webFormItem, dropdownList};
                    });
                }
                this.ready = true;
        });
    }

    goToAddNewWebform() {
        this.stateService.go("zen.app.pia.module.dsar.views.webform.add");
    }

    goToWebForm(webFormId: string): void {
        this.stateService.go("zen.app.pia.module.dsar.views.webform.manage", { webFormId });
    }

    private getDropdownOptions(webFormItem: IWebFormItem): IDropdownOption[] {
        const dropdownOptions: IDropdownOption[] = [];
        if (this.permissions.canShow("DSARWebFormsDelete") && !webFormItem.isUsed) {
            dropdownOptions.push({
                textKey: this.translatePipe.transform("Delete"),
                action: (): void => {
                    this.deleteModalData.promiseToResolve = (): IPromise<any> => this.dsarWebformService.deleteWebForm(webFormItem.templateId)
                        .then((res: IProtocolResponse<void>) => {
                            if (res.result) {
                                this.getAllWebForms();
                            }
                            return res.result;
                        });
                    this.modalService.setModalData(this.deleteModalData);
                    this.modalService.openModal("deleteConfirmationModal");
                },
            });
        }
        if (!dropdownOptions.length) {
            dropdownOptions.push({
                textKey: this.translatePipe.transform("NoActionsAvailable"),
                action: (): void => { }, // Keep Empty
            });
        }
        return dropdownOptions;
    }

}
