// Angular
import {
    Component,
    Input,
    EventEmitter,
    Output,
} from "@angular/core";

// Interfaces
import { IDropdownOption } from "interfaces/one-dropdown.interface";
import { IAction } from "interfaces/redux.interface";
import { IWebformListItem } from "dsarModule/interfaces/webform.interface";

// Enums
import { ContextMenuType } from "@onetrust/vitreus";

@Component({
    selector: "webform-tile",
    templateUrl: "./webform-tile.component.html",
})
export class WebformTileComponent {

    @Input() tileName: string;
    @Input() tileIcon: string;
    @Input() tileButtonText: string;
    @Input() descriptionTitle: string;
    @Input() descriptionContent: string;
    @Input() dropdownList: IDropdownOption[];
    @Input() tileObject: IWebformListItem;
    @Output() tileButtonCallback = new EventEmitter<IAction>();

    contextMenuType = ContextMenuType;

    handleActionClicks(actionItem) {
        actionItem.action(actionItem);
    }

    tileButtonClick() {
        this.tileButtonCallback.emit({
            type: "TileButtonClick",
            payload: this.tileObject,
        });
    }

}
