import {
    Component,
    Input,
    Output,
    EventEmitter,
    OnInit,
} from "@angular/core";

// Interfaces
import { IProtocolPacket } from "interfaces/protocol.interface";
import { IStringMap } from "interfaces/generic.interface";
import { IWebFormData } from "dsarModule/interfaces/webform.interface";
import { IWebformModel, ISidebarData } from "@onetrust/dsar-components";

@Component({
    selector: "webform-preview",
    templateUrl: "./webform-preview.component.html",
})
export class WebformPreviewComponent implements OnInit {
    @Input() webformModel: IWebformModel;
    @Input() sidebarData: ISidebarData;
    @Input() formTranslations: IStringMap<string>;
    @Input() result: IProtocolPacket = null;
    @Input() webform: any;
    @Input() isExternal: boolean;
    @Input() widgetId: number;
    @Input() defaultLogo: string;
    @Output() onSubmit = new EventEmitter<IWebFormData>();
    @Output() actionCallback = new EventEmitter<{ type: number, payload: any }>();

    constructor() { }
    ngOnInit() { }

    submit(data: IWebFormData) {
        this.onSubmit.emit(data);
    }

    handleAction(action: { type: number, payload: any }) {
        this.actionCallback.emit(action);
    }
}
