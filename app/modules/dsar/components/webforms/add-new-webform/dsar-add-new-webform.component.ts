// 3rd party
import {
    Component,
    OnInit,
    Inject,
} from "@angular/core";
import {
    find,
    sortBy,
} from "lodash";
import { Observable } from "rxjs";

// Interfaces
import { IWebformCreateResponse } from "dsarModule/interfaces/webform.interface";
import { IPublishedWorkflow } from "interfaces/workflows.interface";
import { ISettingsMeta } from "sharedModules/interfaces/settings.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";

// Constants
import { DSARWebformSettingsLabel } from "dsarModule/enums/dsar-webform.enum";
import { SettingsType } from "sharedModules/enums/settings.enum";
import { DefaultWorkflowID } from "dsarModule/constants/dsar-settings.constant";
import { WebformTabs } from "dsarModule/enums/dsar-webform.enum";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

// Services
import { WebformApiService } from "dsarservice/webform-api.service";
import { DSARWorkflowService } from "dsarservice/dsar-workflows.service";
import { StateService } from "@uirouter/core";
import OrgGroupStoreNew from "oneServices/org-group.store";
import { NotificationService } from "modules/shared/services/provider/notification.service";

@Component({
    selector: "dsar-add-new-webform",
    templateUrl: "./dsar-add-new-webform.component.html",
})
export class DSARAddNewWebformComponent implements OnInit {

    canSubmit = false;
    dsarWebformSettingsLabel = DSARWebformSettingsLabel;
    isLoading = true;
    loadingSave = false;
    orgEmpty = false;
    selectedWorkflow: IPublishedWorkflow;
    settings: {
        WebFormName: string;
        Organization: string;
        Workflow: IPublishedWorkflow;
    };
    settingsDirty = false;
    webformCreateMeta: ISettingsMeta[];
    webFormName = "";
    webFormOrgId = "";
    workflowOptions: IPublishedWorkflow[];

    constructor(
        @Inject("OrgGroupStoreNewService") private orgGroupStoreNew: OrgGroupStoreNew,
        private translatePipe: TranslatePipe,
        private dsarWebformService: WebformApiService,
        private workflowsService: DSARWorkflowService,
        private stateService: StateService,
        private notificationService: NotificationService,
    ) {}

    public ngOnInit(): void {
        this.settings = {
            Workflow: null,
            WebFormName: "",
            Organization: this.orgGroupStoreNew.orgList[0].id,
        };
        this.workflowsService.getPublishedWorkflows().then((response: IProtocolResponse<IPublishedWorkflow[]>): void => {
            this.workflowOptions = sortBy(response.data, [(res: IPublishedWorkflow) =>
                res.workflowName.toLowerCase()]);
            this.settings.Workflow = find(this.workflowOptions, ["referenceId", DefaultWorkflowID]);
            this.webformCreateMeta = [
                {
                    actionType: DSARWebformSettingsLabel.WebFormName,
                    class: "margin-bottom-2",
                    maxLength: 30,
                    placeholder: this.translatePipe.transform("EnterName"),
                    required: true,
                    title: DSARWebformSettingsLabel.WebFormName,
                    type: SettingsType.INPUT,
                    value: "",
                },
                {
                    actionType: DSARWebformSettingsLabel.Organization,
                    class: "margin-bottom-2",
                    hideClearAction: true,
                    orgRequired: true,
                    orgSelectChoosesUserOrg: true,
                    required: true,
                    title: DSARWebformSettingsLabel.Organization,
                    type: SettingsType.ORGANIZATION,
                },
                {
                    actionType: DSARWebformSettingsLabel.Workflow,
                    class: "margin-bottom-2",
                    labelKey: "workflowName",
                    options: this.workflowOptions,
                    required: true,
                    title: DSARWebformSettingsLabel.Workflow,
                    type: SettingsType.LOOKUP,
                    valueKey: "referenceId",
                },
            ];
            this.isLoading = false;
        });
    }

    handleCancel() {
        this.stateService.go("zen.app.pia.module.dsar.views.webform.list");
    }

    handleSubmit() {
        this.loadingSave = true;
        this.dsarWebformService.createNewWebForm({
            templateName: this.settings.WebFormName,
            orgGroupId: this.settings.Organization,
            workflowReferenceId: this.settings.Workflow.referenceId,
         }).then((response: IProtocolResponse<IWebformCreateResponse | string>): void => {
            this.loadingSave = false;
            if (response.result) {
                const result: IWebformCreateResponse = response.data as IWebformCreateResponse;
                this.stateService.go("zen.app.pia.module.dsar.views.webform.manage", {
                    webFormId: result.templateId,
                    webformTab: WebformTabs.WebForm,
                });
            } else if (response.errors && response.errors.code === "ERROR_DATASUBJECT-ACCESS_DUPLICATEWEBFORM") {
                this.notificationService.alertError(
                    this.translatePipe.transform("Error"),
                    this.translatePipe.transform("DuplicateWebForm"),
                );
            }
        });
    }

    updateOrganization(organization: string) {
        this.settings.Organization = organization;
    }

    updateWorkflow(workflow: IPublishedWorkflow) {
        this.settings.Workflow = workflow;
        this.validateSubmit();
    }

    validateSubmit() {
        this.canSubmit = Boolean(this.settings.WebFormName.trim()) && Boolean(this.settings.Workflow);
    }

    updateWebformName(webformName: string) {
        this.webformCreateMeta[0].dirty = true;
        this.settings.WebFormName = webformName;
        this.validateSubmit();
    }

    handleAction(action) {
        switch (action.type) {
            case DSARWebformSettingsLabel.Organization:
                this.updateOrganization(action.payload);
                break;
            case DSARWebformSettingsLabel.Workflow:
                this.updateWorkflow(action.payload.currentValue);
                break;
            case DSARWebformSettingsLabel.WebFormName:
                this.updateWebformName(action.payload.target.value);
                break;
        }
    }
}
