export interface IWebformStyleField {
    canDelete: boolean;
    dataType: string;
    fieldName: string;
    endOfSection: boolean;
    inputType: string;
    value: string | number;
    min?: number;
    max: number;
}

export interface IWebformStyleResponse {
    templateId: string;
    data: IWebformStyleField[];
}
