export interface IWedformInput {
    id?: number;
    fieldName: string;
    canDelete: boolean;
    isSelected: boolean;
    dataType?: string;
    inputType?: string;
    options?: any[];
    status?: number;
}
