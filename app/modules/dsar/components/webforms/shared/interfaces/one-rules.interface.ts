import {
    RuleActionType,
    RuleOperator,
} from "modules/dsar/components/webforms/shared/enums/one-rules.enum";
import { OrgUserTraversal } from "enums/org-user-dropdown.enum";
import { IOrganization } from "interfaces/org.interface";
import { IDeleteConfirmationModalResolve } from "interfaces/delete-confirmation-modal-resolve.interface";

export interface IRuleMeta {
    id?: string;
    name: string;
    status: number;
    isNew?: boolean;
    delete?: boolean;
    oldId?: string;
    isOpen?: boolean;
}

export interface IRule extends IRuleMeta {
    subRules?: ISubRule[];
    actions?: IRuleAction[];
}

export interface ISubRule {
    id: string;
    conditions: IRuleCondition[];
}

export interface IRuleCondition {
    id: string;
    field: string;
    operator: string;
    value: string;
}

export interface IRuleAction {
    field: string;
    value: any;
}

export interface IRulesConfig {
    actions: IRuleActionConfig[];
    conditionMeta: IRuleConditionMeta[];
}

export interface IRuleActionConfig {
    field: string;
    fieldDisplay: string;
    defaultValue: any;
    type: RuleActionType;
    required: boolean;
    inputNumberMin?: number;
    inputNumberMax?: number;
    picklistOptions?: Array<{}>;
    picklistLabelKey?: string;
    picklistValueKey?: string;
    orgSelectOptions?: IOrganization[];
    orgSelectChoosesUserOrg?: boolean;
    orgSelectDefaultValue?: string;
    orgUserOrgId?: string;
    orgUserTraversal?: OrgUserTraversal;
    placeholder?: string;
}

export interface IRuleConditionMeta {
    field: string;
    fieldDisplay: string;
    operators: IRuleOperator[];
    options: Array<{}>;
    labelKey: string;
    valueKey: string;
}

export interface IRuleOperator {
    operator: RuleOperator;
    operatorDisplay: string;
}

export interface IRuleDeleteConfirmationModalResolve extends IDeleteConfirmationModalResolve {
    callbackPromise: (rules: IRule[]) => ng.IPromise<boolean>;
}

export interface IRuleValidate {
    validationRules: boolean[];
    isValid: boolean;
}
