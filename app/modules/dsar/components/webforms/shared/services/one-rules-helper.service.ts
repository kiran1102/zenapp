// Rxjs
import { BehaviorSubject } from "rxjs";

// Angular
import { Injectable } from "@angular/core";

// 3rd Party
import {
    find,
    isFunction,
} from "lodash";

// Services
import { ModalService } from "sharedServices/modal.service";

// Interfaces
import {
    IRule,
    IRuleDeleteConfirmationModalResolve,
    ISubRule,
    IRuleCondition,
    IRuleValidate,
    // IRulesConfig,
} from "modules/dsar/components/webforms/shared/interfaces/one-rules.interface";

// Constants & Enums
import { RuleStatus } from "modules/dsar/components/webforms/shared/enums/one-rules.enum";

@Injectable()
export class OneRulesHelperService {

    contentSource = new BehaviorSubject({saveValidated: false});
    private rules: IRule[];
    private update: () => void;
    private updateOrgUserOrg: (org: string) => void;
    private internalId = 100000000000;

    constructor(
        readonly modalService: ModalService,
    ) {}

    getRules(): IRule[] {
        return this.rules;
    }

    setRules(rules: IRule[]) {
        this.rules = rules;
        this.update();
    }

    setUpdate(update: () => void) {
        this.update = update;
    }

    updateRules() {
        this.update();
    }

    saveChanges(): IRuleValidate {
        this.contentSource.next({saveValidated: true});
        const validate: IRuleValidate = this.areChangesValid();
        if (validate.isValid) {
            this.update();
            return validate;
        }
        return {
            validationRules: validate.validationRules,
            isValid: false,
        };
    }

    resetSaveValidated() {
        this.contentSource.next({saveValidated: false});
    }

    areChangesValid(): IRuleValidate {
        const validationRules: boolean[] = [];
        let isValid = true;
        this.rules.forEach((rule: IRule, index: number) => {
            if (rule.subRules && rule.subRules.length) {
                validationRules[index] = !!(this.validateSubrules(rule.subRules)
                                            && rule.name && rule.name.length <= 100);
                if (isValid) {
                    isValid = validationRules[index];
                }
            } else {
                validationRules[index] = true;
            }
        });
        return {validationRules, isValid};
    }

    validateSubrules(subRules: ISubRule[]): boolean {
        for (const subRule of subRules) {
            if (!this.validateConditions(subRule.conditions)) {
                return false;
            }
        }
        return true;
    }

    validateConditions(conditions: IRuleCondition[]): boolean {
        return conditions.every((condition: IRuleCondition): boolean => {
            return !!(condition.operator && condition.field && condition.value);
        });
    }

    addRule() {
        this.contentSource.next({saveValidated: false});
        this.rules.push({
            id: this.generateUniqueId(),
            name: "",
            status: RuleStatus.ACTIVE,
            isNew: true,
            isOpen: true,
            subRules: [
                {
                    id: this.generateUniqueId(),
                    conditions: [
                        {
                            id: this.generateUniqueId(),
                            field: "",
                            operator: "",
                            value: "",
                        },
                    ],
                },
            ],
            actions: [],
        });
        this.update();
    }

    markDeletedRules(rules: IRule[], ruleIndex: number): IRule[] {
        if (rules[ruleIndex].isNew) {
            rules.splice(ruleIndex, 1);
        } else {
            rules[ruleIndex].delete = true;
        }
        return rules;
    }

    deleteRule(ruleIndex: number, deleteConfig?: IRuleDeleteConfirmationModalResolve) {
        if (isFunction(deleteConfig.callbackPromise) && !this.rules[ruleIndex].isNew) {
            deleteConfig.promiseToResolve = (): ng.IPromise<boolean> => {
                return deleteConfig.callbackPromise(this.markDeletedRules(this.rules, ruleIndex)).
                    then((res: boolean) => {
                        if (res) {
                            this.update();
                        }
                        return res;
                    });
            };
            this.modalService.setModalData(deleteConfig);
            this.modalService.openModal("otDeleteConfirmationModal");
        } else {
            this.markDeletedRules(this.rules, ruleIndex);
            this.update();
        }
    }

    ruleShiftUp(ruleIndex: number) {
        if (ruleIndex !== 0) {
            const tempRule = this.rules[ruleIndex - 1];
            this.rules[ruleIndex - 1] = this.rules[ruleIndex];
            this.rules[ruleIndex] = tempRule;
            this.update();
        }
    }

    ruleShiftDown(ruleIndex: number) {
        if (ruleIndex !== this.rules.length - 1) {
            const tempRule = this.rules[ruleIndex + 1];
            this.rules[ruleIndex + 1] = this.rules[ruleIndex];
            this.rules[ruleIndex] = tempRule;
            this.update();
        }
    }

    setRuleName(ruleIndex: number, name: string) {
        this.rules[ruleIndex].name = name;
    }

    addSubRule(ruleIndex: number) {
        this.rules[ruleIndex].subRules.push({
            id: this.generateUniqueId(),
            conditions: [
                {
                    id: this.generateUniqueId(),
                    field: "",
                    operator: "",
                    value: "",
                },
            ],
        });
        this.update();
    }

    removeSubRule(ruleIndex: number, subRuleIndex: number) {
        this.rules[ruleIndex].subRules.splice(subRuleIndex, 1);
        if (!this.rules[ruleIndex].subRules.length) {
            this.addSubRule(ruleIndex);
        }
    }

    addCondition(ruleIndex: number, subRuleIndex: number) {
        this.rules[ruleIndex].subRules[subRuleIndex].conditions.push({
            id: this.generateUniqueId(),
            field: "",
            operator: "",
            value: "",
        });
    }

    removeCondition(ruleIndex: number, subRuleIndex: number, conditionIndex: number) {
        this.rules[ruleIndex].subRules[subRuleIndex].conditions.splice(conditionIndex, 1);
        if (!this.rules[ruleIndex].subRules[subRuleIndex].conditions.length) {
            this.addCondition(ruleIndex, subRuleIndex);
        }
    }

    setConditionField(ruleIndex: number, subRuleIndex: number, conditionIndex: number, field: string) {
        this.rules[ruleIndex].subRules[subRuleIndex].conditions[conditionIndex].field = field;
    }

    setConditionOperator(ruleIndex: number, subRuleIndex: number, conditionIndex: number, operator: string) {
        this.rules[ruleIndex].subRules[subRuleIndex].conditions[conditionIndex].operator = operator;
    }

    setConditionValue(ruleIndex: number, subRuleIndex: number, conditionIndex: number, value: string) {
        this.rules[ruleIndex].subRules[subRuleIndex].conditions[conditionIndex].value = value;
    }

    setActionValue(ruleIndex: number, field: string, value: any) {
        const action = find(this.rules[ruleIndex].actions, (x) => x.field === field);
        if (action) {
            action.value = value;
        } else {
            this.rules[ruleIndex].actions.push(
                {
                    field,
                    value,
                },
            );
        }
    }

    setOrgUserCallback(updateOrgUserOrg: (org: string) => void) {
        this.updateOrgUserOrg = updateOrgUserOrg;
    }

    updateOrg(org: string) {
        this.updateOrgUserOrg(org);
    }

    private generateUniqueId() {
        return `00000000-0000-0000-0000-${this.internalId++}`;
    }
}
