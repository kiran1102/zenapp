export enum RuleActionType {
    Toggle,
    Checkbox,
    InputNumber,
    InputString,
    Picklist,
    OrgSelect,
    OrgUser,
    OrgUserMultiSelect,
}

export enum RuleOperator {
    EQUAL_TO = "EQUAL_TO",
    NOT_EQUAL_TO = "NOT_EQUAL_TO",
    IN = "IN",
    NOT_IN = "NOT_IN",
    GREATER_THAN = "GREATER_THAN",
    GREATER_THAN_EQUAL_TO = "GREATER_THAN_EQUAL_TO",
    LESS_THAN_EQUAL_TO = "LESS_THAN_EQUAL_TO",
    LESS_THAN = "LESS_THAN",
}

export enum RuleStatus {
    ACTIVE = 0,
    INACTIVE = 1,
}
