// Angular
import {
    Component,
    Input,
} from "@angular/core";

// 3rd Party
import { find } from "lodash";

// Interfaces
import {
    IRuleActionConfig,
    IRuleAction,
} from "modules/dsar/components/webforms/shared/interfaces/one-rules.interface";
import { IOtOrgUserOutput } from "sharedModules/components/ot-org-user/ot-org-user.interface";
import { IUser } from "interfaces/user.interface";
import { IOrgUserAdapted } from "interfaces/org-user.interface";

// Constants & Enums
import { RuleActionType } from "modules/dsar/components/webforms/shared/enums/one-rules.enum";
import { DsarWebformRuleActionKeys } from "modules/dsar/components/webforms/shared/constants/dsar-webform-rules.constants";

// Service
import { OneRulesHelperService } from "modules/dsar/components/webforms/shared/services/one-rules-helper.service";

@Component({
    selector: "one-rule-action",
    templateUrl: "./one-rule-action.component.html",
})
export class OneRuleActionComponent {
    @Input() readonly ruleIndex: number;
    @Input() readonly actionIndex: number;
    @Input() readonly editMode: boolean;
    @Input() readonly config: IRuleActionConfig;
    @Input() set actions(actions: IRuleAction[]) {
        const action = find(actions, (x) => this.config.field === x.field);
        if (this.config.type === RuleActionType.Picklist) {
            this.valueModel = action ? find(this.config.picklistOptions, [this.config.picklistValueKey, action.value]) : this.config.defaultValue;
            this.valueSet(this.valueModel[this.config.picklistValueKey]);
        } else if (this.config.orgSelectChoosesUserOrg && this.config.type === RuleActionType.OrgUser) {
            this.oneRulesHelperService.setOrgUserCallback((org: string) => {
                this.orgUserOrg = org;
            });
            this.orgUserOrg = this.config.orgSelectDefaultValue;
            this.defaultUser = this.config.defaultValue;
            if (this.defaultUser.FullName === "- - - -"  ) {
                this.defaultUser.Id = "";
            }
            this.valueModel = action && action.value ? action.value : this.defaultUser.Id;
            this.valueSet(this.valueModel);
        } else if (this.config.type === RuleActionType.OrgUserMultiSelect) {
            let selectedUsersForEmails = [];
            const emailAction = actions.find((value) => value.field === DsarWebformRuleActionKeys.CONFIRMATIONMAIL);
            if (!emailAction) {
                this.valueSet([] as IOrgUserAdapted[]);
            } else {
                selectedUsersForEmails = emailAction.value;
            }
            let formattedSelectedUsersForEmails = [];
            if (selectedUsersForEmails && selectedUsersForEmails.length > 0) {
                formattedSelectedUsersForEmails = selectedUsersForEmails.map((value) => {
                    return { Id: value, FullName: "" } as IOrgUserAdapted;
                });
            }
            this.valueModel = formattedSelectedUsersForEmails;
            this.valueSet(selectedUsersForEmails);
        } else {
            this.valueModel = action ? action.value : this.config.defaultValue;
            this.valueSet(this.valueModel);
        }
    }

    readonly ruleActionType = RuleActionType;
    valueModel: any;
    orgUserOrg: string;
    defaultUser: IUser;

    constructor(
        private readonly oneRulesHelperService: OneRulesHelperService,
    ) { }

    valueSet(value: any) {
        this.oneRulesHelperService.setActionValue(this.ruleIndex, this.config.field, value);
    }

    numberValueSet(event: any) {
        let value = parseInt(event.target.value, 0);
        if ((value > this.config.inputNumberMax) || (value < this.config.inputNumberMin) || !value) {
            value = this.config.inputNumberMin;
        }
        this.valueModel = value;
        this.oneRulesHelperService.setActionValue(this.ruleIndex, this.config.field, value);
    }

    picklistValueSet(value: any) {
        this.valueModel = value;
        this.valueSet(value[this.config.picklistValueKey]);
    }

    orgSelectValueSet(value: any) {
        this.valueModel = value;
        if (this.config.orgSelectChoosesUserOrg) {
            this.oneRulesHelperService.updateOrg(value);
        }
        this.valueSet(value);
    }

    orgUserValueSet(value: IOtOrgUserOutput) {
        const id = value && value.selection ? value.selection.Id : null;
        this.valueModel = id;
        this.valueSet(id);
    }

    orgUserMultiSetValue(value: IOrgUserAdapted[]) {
        this.valueModel = value;
        const selectedValues = value ? value.map((user) => user.Id) : this.valueModel;
        this.valueSet(selectedValues);
    }
}
