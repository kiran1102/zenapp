// Angular
import {
    Component,
    Input,
} from "@angular/core";

// Interfaces
import {
    ISubRule,
    IRuleConditionMeta,
    IRuleCondition,
} from "modules/dsar/components/webforms/shared/interfaces/one-rules.interface";
import { IStringMap } from "interfaces/generic.interface";

// Service
import { OneRulesHelperService } from "modules/dsar/components/webforms/shared/services/one-rules-helper.service";

@Component({
    selector: "one-sub-rule",
    templateUrl: "./one-sub-rule.component.html",
})
export class OneSubRuleComponent {
    @Input() readonly editMode: boolean;
    @Input() readonly ruleIndex: number;
    @Input() readonly subRuleIndex: number;
    @Input() readonly translations: IStringMap<string>;
    @Input() readonly subRule: ISubRule;
    @Input() readonly conditionMeta: IRuleConditionMeta[];

    constructor(
        private readonly oneRulesHelperService: OneRulesHelperService,
    ) { }

    removeSubRule() {
        this.oneRulesHelperService.removeSubRule(
            this.ruleIndex,
            this.subRuleIndex,
        );
    }

    trackByFn(index: number, condition: IRuleCondition): string {
        return condition.id;
    }
}
