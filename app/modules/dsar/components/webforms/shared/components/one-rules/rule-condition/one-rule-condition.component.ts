// Angular
import {
    Component,
    Input,
    OnInit,
} from "@angular/core";

// Interfaces
import {
    IRuleCondition,
    IRuleConditionMeta,
    IRuleOperator,
} from "modules/dsar/components/webforms/shared/interfaces/one-rules.interface";
import { IStringMap } from "interfaces/generic.interface";

// Service
import { OneRulesHelperService } from "modules/dsar/components/webforms/shared/services/one-rules-helper.service";

@Component({
    selector: "one-rule-condition",
    templateUrl: "./one-rule-condition.component.html",
})
export class OneRuleConditionComponent implements OnInit {
    @Input() readonly first: boolean;
    @Input() readonly last: boolean;
    @Input() readonly ruleIndex: number;
    @Input() readonly subRuleIndex: number;
    @Input() readonly conditionIndex: number;
    @Input() readonly translations: IStringMap<string>;
    @Input() readonly editMode: boolean;
    @Input() readonly conditionMeta: IRuleConditionMeta[];
    @Input() set condition(condition: IRuleCondition) {
        if (condition.field) {
            this.fieldModel = this.conditionMeta.find((x) => x.field === condition.field);
            this.operatorModel = this.fieldModel.operators.find((x) => x.operator === condition.operator);
            this.valueModel = this.fieldModel.options.find((x) => x[this.fieldModel.valueKey] === condition.value);
        } else {
            this.fieldModel = {
                field: "",
                fieldDisplay: "",
                operators: [],
                options: [],
                labelKey: "label",
                valueKey: "value",
            };
        }
    }

    fieldModel: IRuleConditionMeta;
    operatorModel: IRuleOperator;
    valueModel: {};
    saveValidated: boolean;
    currentInputValue = "";

    constructor(
        private readonly oneRulesHelperService: OneRulesHelperService,
    ) { }

    ngOnInit() {
        this.oneRulesHelperService.contentSource.asObservable().subscribe((res: { saveValidated: boolean }) => {
            this.saveValidated = res.saveValidated;
        });
    }

    addCondition() {
        this.oneRulesHelperService.addCondition(
            this.ruleIndex,
            this.subRuleIndex,
        );
    }

    removeCondition() {
        this.oneRulesHelperService.removeCondition(
            this.ruleIndex,
            this.subRuleIndex,
            this.conditionIndex,
        );
    }

    filterInput({ key, value }, fieldName: string) {
        if (key === "Enter") { return; }
        this.currentInputValue = value;
    }

    clearInput() {
        this.currentInputValue = "";
    }

    fieldSelect(field: IRuleConditionMeta) {
        this.currentInputValue = "";
        this.fieldModel = field;
        this.operatorSelect(null);
        this.valueSelect(null);
        if (field) {
            this.oneRulesHelperService.setConditionField(
                this.ruleIndex,
                this.subRuleIndex,
                this.conditionIndex,
                field.field,
            );
        } else {
            this.fieldModel = {
                field: "",
                fieldDisplay: "",
                operators: [],
                options: [],
                labelKey: "label",
                valueKey: "value",
            };
        }
    }

    operatorSelect(operator: IRuleOperator) {
        this.operatorModel = operator;
        this.oneRulesHelperService.setConditionOperator(
            this.ruleIndex,
            this.subRuleIndex,
            this.conditionIndex,
            operator ? operator.operator : null,
        );
    }

    valueSelect(value: {}) {
        this.valueModel = value;
        this.oneRulesHelperService.setConditionValue(
            this.ruleIndex,
            this.subRuleIndex,
            this.conditionIndex,
            value ? value[this.fieldModel.valueKey] : null,
        );
    }
}
