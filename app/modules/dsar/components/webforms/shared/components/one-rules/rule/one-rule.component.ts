// Angular
import {
    Component,
    Input,
    Output,
    EventEmitter,
} from "@angular/core";

// Interfaces
import {
    IRule,
    IRulesConfig,
    ISubRule,
    IRuleDeleteConfirmationModalResolve,
} from "modules/dsar/components/webforms/shared/interfaces/one-rules.interface";
import { IStringMap } from "interfaces/generic.interface";

// Service
import { OneRulesHelperService } from "modules/dsar/components/webforms/shared/services/one-rules-helper.service";

@Component({
    selector: "one-rule",
    templateUrl: "./one-rule.component.html",
})
export class OneRuleComponent {
    @Input() readonly ruleIndex: number;
    @Input() readonly translations: IStringMap<string>;
    @Input() readonly editMode: boolean;
    @Input() readonly enableReorderRules: boolean;
    @Input() readonly rule: IRule;
    @Input() readonly config: IRulesConfig;
    @Input() hasError: boolean;
    @Input() deleteConfig: IRuleDeleteConfirmationModalResolve;
    @Output() editModeToggled = new EventEmitter<boolean>();
    @Output() ruleIdToOneRules = new EventEmitter<string>();

    constructor(
        private readonly oneRulesHelperService: OneRulesHelperService,
    ) {}

    enableEditMode() {
        this.toggleRuleContent(true);
        this.editModeToggled.emit(true);
        this.oneRulesHelperService.resetSaveValidated();
    }

    setRuleName(name: string) {
        this.oneRulesHelperService.setRuleName(this.ruleIndex, name);
    }

    deleteRule() {
        this.oneRulesHelperService.deleteRule(this.ruleIndex, this.deleteConfig);
    }

    shiftRuleUp() {
        this.oneRulesHelperService.ruleShiftUp(this.ruleIndex);
    }

    shiftRuleDown() {
        this.oneRulesHelperService.ruleShiftDown(this.ruleIndex);
    }

    addSubRule() {
        this.oneRulesHelperService.addSubRule(this.ruleIndex);
    }

    toggleRuleContent(ruleContent: boolean) {
        this.rule.isOpen = ruleContent;
        if (this.rule.isOpen) {
            this.ruleIdToOneRules.emit(this.rule.id);
        }
    }

    trackByFn(index: number, subRule: ISubRule): string {
        return subRule.id;
    }
}
