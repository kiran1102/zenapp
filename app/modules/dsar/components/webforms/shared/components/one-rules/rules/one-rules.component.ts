// Angular
import {
    Component,
    Input,
    Output,
    EventEmitter,
    OnInit,
} from "@angular/core";

// Interfaces
import {
    IRule,
    IRulesConfig,
    IRuleDeleteConfirmationModalResolve,
    IRuleValidate,
} from "modules/dsar/components/webforms/shared/interfaces/one-rules.interface";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

// Service
import { OneRulesHelperService } from "modules/dsar/components/webforms/shared/services/one-rules-helper.service";

@Component({
    selector: "one-rules",
    templateUrl: "./one-rules.component.html",
    host: {
        "class": "flex-full-height",
        "[class.background-white]": "!editMode",
    },
})
export class OneRulesComponent implements OnInit {
    @Input() config: IRulesConfig;
    @Input("rules") set _rules(rules: IRule[]) {
        this.rules = rules;
        this.oneRulesHelperService.setUpdate(() => {
            this.rules = this.oneRulesHelperService.getRules();
            this.enableReorderRules = this.rules.length > 1;
        });
        this.oneRulesHelperService.setRules(rules); // TODO find correct place for this, `Cancel` does not work currently
    }
    @Input() editMode: boolean;
    @Input() deleteConfig: IRuleDeleteConfirmationModalResolve;
    @Output() editModeChanged = new EventEmitter<boolean>();
    @Output() rulesSaved = new EventEmitter<IRule[]>();
    @Output() ruleOpened = new EventEmitter<string>();
    @Output() ruleCancelled = new EventEmitter<string>();
    enableReorderRules: boolean;

    rules: IRule[];
    validationRules: boolean[];

    readonly translations = {
        AND: this.translatePipe.transform("DSARWebformRules.And"),
        AddRule: this.translatePipe.transform("AddRule"),
        Cancel: this.translatePipe.transform("Cancel"),
        IfAllOfTheFollowingConditionsAreMet: this.translatePipe.transform("IfAllOfTheFollowingConditionsAreMet"),
        OR: this.translatePipe.transform("Or").toUpperCase(),
        RuleName: this.translatePipe.transform("RuleName"),
        Save: this.translatePipe.transform("Save"),
        Field: this.translatePipe.transform("DSARWebformRules.Field"),
        Operator: this.translatePipe.transform("DSARWebformRules.Operator"),
        Value: this.translatePipe.transform("DSARWebformRules.Value"),
        ThenPerformTheFollowingTasks: this.translatePipe.transform("ThenPerformTheFollowingTasks"),
    };

    constructor(
        private readonly oneRulesHelperService: OneRulesHelperService,
        private readonly translatePipe: TranslatePipe,
    ) {}

    ngOnInit() {
        this.oneRulesHelperService.resetSaveValidated();
    }

    addRule() {
        this.validationRules = null;
        this.oneRulesHelperService.addRule();
        this.editMode = true;
        this.editModeChanged.emit(true);
    }

    deleteRule(ruleIndex: number) {
        this.oneRulesHelperService.deleteRule(ruleIndex, this.deleteConfig);
    }

    ruleShiftUp(ruleIndex: number) {
        this.oneRulesHelperService.ruleShiftUp(ruleIndex);
    }

    ruleShiftDown(ruleIndex: number) {
        this.oneRulesHelperService.ruleShiftDown(ruleIndex);
    }

    ruleIdToWebform(ruleId: string) {
        this.ruleOpened.emit(ruleId);
    }

    saveRules() {
        const validate: IRuleValidate = this.oneRulesHelperService.saveChanges();
        this.validationRules = validate.validationRules.length ? [...validate.validationRules] : this.validationRules;
        if (validate.isValid) {
            this.editMode = false;
            this.rulesSaved.emit(this.rules);
        }
    }

    cancelRules() {
        this.ruleCancelled.emit("");
        this.editMode = false;
    }

    toggleEditMode(editMode: boolean) {
        this.editMode = editMode;
        this.editModeChanged.emit(editMode);
    }

    trackByFn(index: number, rule: IRule): string {
        return rule.id;
    }
}
