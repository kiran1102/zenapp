// Angular
import {
    Component,
    Input,
    Output,
    OnInit,
    EventEmitter,
} from "@angular/core";

@Component({
    selector: "dsar-editable-text",
    templateUrl: "./dsar-editable-text.component.html",
})
export class DsarEditableTextComponent {
    @Input() text: string;
    @Output() cancelled = new EventEmitter<boolean>();
    @Output() submitted = new EventEmitter<string>();

    handleSubmit() {
        this.submitted.emit(this.text);
    }

    handleCancel() {
        this.cancelled.emit(true);
    }
}
