// Third Party
import {
    Component,
    Inject,
} from "@angular/core";

import Utilities from "Utilities";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

// Interfaces
import { IStore } from "interfaces/redux.interface";

// Redux
import { StoreToken } from "tokens/redux-store.token";
import { getModalData } from "oneRedux/reducers/modal.reducer";

// Services
import { ModalService } from "sharedServices/modal.service";
import { WebformApiService } from "dsarservice/webform-api.service";
import { NotificationService } from "modules/shared/services/provider/notification.service";
import { IWebformPublishResponse } from "modules/dsar/interfaces/webform.interface";

@Component({
    selector: "dsar-publish-form-confirmation-modal",
    templateUrl: "./dsar-publish-form-confirmation-modal.component.html",
})
export class DSARPublishFormConfirmationModal {
    modalData: IWebformPublishResponse;
    actionInProgress: boolean;
    title: string = this.translatePipe.transform("SuccessFullyPublished");
    content: string = this.title;
    successMessage = this.translatePipe.transform("DSARWebformScriptPublished");
    copyLinkMessage = "";
    formTemplateName: string;
    anchorScript = "";
    hrefString: string;

    constructor(
        @Inject(StoreToken) public store: IStore,
        private dsarWebFormService: WebformApiService,
        private modalService: ModalService,
        private translatePipe: TranslatePipe,
        private notificationService: NotificationService,
    ) {}

    ngOnInit() {
        this.modalData = getModalData(this.store.getState());
        this.copyLinkMessage = this.translatePipe.transform("copyLinkMessage", { cdnCacheHours: this.modalData.cdnCacheHours });
        this.hrefString = this.dsarWebFormService.hrefString;
        this.anchorScript = `<a target="blank" href="${this.hrefString}">${this.translatePipe.transform("WebForm")}</a>`;
    }

    generateScript = (): void => {
        Utilities.copyToClipboard(this.anchorScript);
        this.notificationService.alertNote(
            this.translatePipe.transform("Success"),
            this.translatePipe.transform("WebFormLinkCopied"),
        );
    }

    closeModal(): void {
        this.modalService.closeModal();
        this.modalService.clearModalData();
    }
}
