// Services
import { RequestQueueService } from "dsarservice/request-queue.service";
import { Content } from "sharedModules/services/provider/content.service";
import { WebformStore } from "modules/dsar/webform.store";

// Interfaces
import { IWebFormData } from "dsarModule/interfaces/webform.interface";
import { IProtocolPacket } from "interfaces/protocol.interface";
import { IStringMap } from "interfaces/generic.interface";
import { Permissions } from "modules/shared/services/helper/permissions.service";
import { ISidebarData, IWebformModel } from "@onetrust/dsar-components";

// Enums
import { DSARStoreActions } from "dsarModule/enums/dsar-webform.enum";
import { DSARSettings, DSARCaptchaType } from "modules/dsar/constants/dsar-settings.constant";

class WebFormLiveController implements ng.IComponentController {
    static $inject: string[] = [
        "$stateParams",
        "webformStore",
        "requestQueueServiceDowngraded",
        "Permissions",
        "Content",
        "$rootScope",
    ];

    isWebformDeleted = false;
    showNewWebForm = false;
    ready = false;
    webForm: any;
    webformModel: IWebformModel;
    sidebarData: ISidebarData;
    result: IProtocolPacket = null;
    formTranslations: IStringMap<string>;
    widgetId: number;
    defaultLogo: string;
    private unsubscribeFn: () => void;
    private webFormId: string;

    constructor(
        private $stateParams: ng.ui.IStateParamsService,
        private dsarWebFormStore: WebformStore,
        private requestQueueService: RequestQueueService,
        private permissions: Permissions,
        private content: Content,
    ) {}

    $onInit() {
        this.unsubscribeFn = this.dsarWebFormStore.subscribe(this.getState.bind(this));
        this.webFormId = this.$stateParams.webFormId;
        this.dsarWebFormStore.setTokenAccess(true);
        this.defaultLogo = this.content ? this.content.GetKey("WebformLogo") || this.content.GetKey("GlobalHeaderLogo") : null;
        this.showNewWebForm = this.permissions.canShow("DSARWebFormPublishV2");
        this.dsarWebFormStore.fetchWebForm(this.$stateParams.webFormId).then(() => {
            if (this.dsarWebFormStore.DSARState.redirect && this.dsarWebFormStore.DSARState.publishedHost && this.dsarWebFormStore.DSARState.publishedPath) {
                window.location.href = `${this.dsarWebFormStore.DSARState.publishedHost}${this.dsarWebFormStore.DSARState.publishedPath}`;
            }
        });
    }

    $onDestroy(): void {
        this.unsubscribeFn();
    }

    handleAction(type: number, payload: any): void {
        if (type === DSARStoreActions.LANGUAGE_SWITCH) {
            this.dsarWebFormStore.languageSwitch(payload.language);
        } else {
            this.dsarWebFormStore.dispatch({ type, payload });
        }
    }

    handleActionNewWebform(action: { type: number; payload: any }) {
        this.handleAction(action.type, action.payload);
    }

    onSubmit(data: IWebFormData): ng.IPromise<IProtocolPacket> {
        return this.requestQueueService.submitRequest(this.webFormId, data, true);
    }

    onSubmitNewWebform(data: IWebFormData): void {
        this.requestQueueService.submitRequest(this.webFormId, data).then(
            (result: IProtocolPacket) => {
                this.result = result;
            },
        );
    }

    private getState(): void {
        this.isWebformDeleted = this.dsarWebFormStore.DSARState.isWebformDeleted;
        this.ready = !this.dsarWebFormStore.DSARState.loading;
        const state = this.dsarWebFormStore.DSARState;
        if (state.webForm) {
            this.webForm = state.webForm;
            this.formTranslations = state.formTranslations;
            this.sidebarData = {
                requestTypes: state.webForm.requestTypes,
                subjectTypes: state.webForm.subjectTypes,
                formFields: state.formFields.data,
                formStylingInputs: state.formStylingInputs.data,
                settingsInputs: state.settingsInputs.data,
            };
            const recaptcha = this.sidebarData.settingsInputs.find((setting) => {
                return setting.fieldName === DSARSettings.EnableReCaptcha;
            });
            const captchaType = this.sidebarData.settingsInputs.find((setting) => {
                return setting.fieldName === DSARSettings.MultiCaptcha;
            });
            if ((recaptcha && recaptcha.value) && (captchaType && captchaType.value === DSARCaptchaType.BotdetectCaptcha)) {
                this.webformModel = {
                    botDetectCaptchaSettings: {
                        endpoint: this.webForm.captchaUrl + "/bc/botdetectcaptcha",
                        styleName: "angularBasicCaptcha",
                    },
                };
            }
        }
        this.widgetId = state.widgetId;
    }
}

const WebFormLiveComponent: ng.IComponentOptions = {
    controller: WebFormLiveController,
    bindings: {
        webFormId: "@",
    },
    template: `
        <loading
            ng-if="!$ctrl.ready"
            class="webform-live"
            show-text="false">
        </loading>
        <webform-control
            ng-if="!$ctrl.showNewWebForm && $ctrl.ready && !$ctrl.isWebformDeleted"
            class="overflow-y-scroll full-size-block background-white overflow-x-hidden"
            is-external="true"
            web-form="$ctrl.webForm"
            web-form-languages-list="$ctrl.webForm.languageList"
            widget-id="$ctrl.widgetId"
            form-translations="$ctrl.formTranslations"
            sidebar-data="$ctrl.sidebarData"
            action-callback="$ctrl.handleAction(type, payload)"
            on-submit="$ctrl.onSubmit(data)"
        ></webform-control>
        <webform-preview
            ng-if="$ctrl.showNewWebForm && $ctrl.ready && !$ctrl.isWebformDeleted"
            class="overflow-y-scroll full-size-block background-white overflow-x-hidden"
            [is-external]="true"
            [result]="$ctrl.result"
            [webform]="$ctrl.webForm"
            [webform-model]="$ctrl.webformModel"
            [widget-id]="$ctrl.widgetId"
            [form-translations]="$ctrl.formTranslations"
            [sidebar-data]="$ctrl.sidebarData"
            [default-logo]="$ctrl.defaultLogo"
            (action-callback)="$ctrl.handleActionNewWebform($event)"
            (on-submit)="$ctrl.onSubmitNewWebform($event)"
        ></webform-preview>
        <one-empty-state
            ng-if="$ctrl.ready && $ctrl.isWebformDeleted"
            class="webform-live--webform-not-available align-self-center flex-full-height justify-center"
            empty-state-message="This webform is no longer available"
        ></one-empty-state>
    `,
};

export default WebFormLiveComponent;
