// Angular
import {
    Component,
    OnInit,
    Input,
    Output,
    EventEmitter,
} from "@angular/core";

// 3rd Party
import {
    find,
    remove,
    cloneDeep,
    orderBy,
    filter,
    forIn,
    isEmpty,
} from "lodash";

// Services
import { DsarWebformRulesApiService } from "dsarservice/dsar-webform-rules-api.service";

// Interfaces
import {
    IRule,
    IRulesConfig,
    IRuleMeta,
    IRuleDeleteConfirmationModalResolve,
    IRuleActionConfig,
} from "modules/dsar/components/webforms/shared/interfaces/one-rules.interface";
import {
    IStringMap,
    IKeyValue,
} from "interfaces/generic.interface";

// Constants & Enums
import {
    RuleOperator,
    RuleActionType,
} from "modules/dsar/components/webforms/shared/enums/one-rules.enum";
import { IWebformRuleMeta } from "modules/dsar/interfaces/dsar-webform-rules-meta.interface";
import { OrgUserTraversal } from "enums/org-user-dropdown.enum";
import {
    DsarWebformRuleActionKeys,
    DsarWebformRuleConditionKeys,
} from "modules/dsar/components/webforms/shared/constants/dsar-webform-rules.constants";
import { DSARStoreActions } from "dsarModule/enums/dsar-webform.enum";
export const MAX_DEADLINE = 365;

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";
import { IOrgUserAdapted } from "interfaces/org-user.interface";

@Component({
    selector: "dsar-webform-rules",
    templateUrl: "./dsar-webform-rules.component.html",
})
export class DsarWebformRulesComponent implements OnInit {
    @Input() webformId: string;
    @Input() webformMeta: any; // TODO add type to parent component
    @Input() savePromise: (rules: IRule[]) => ng.IPromise<IRule[]>;
    @Input() formTranslations: IStringMap<string>;
    @Output() actionCallback = new EventEmitter<{
        type: number,
        payload: boolean,
    }>();
    webformRuleMeta: IWebformRuleMeta;
    rules: IRule[];
    originalCopy: IRule[];
    config: IRulesConfig;
    ruleEditMode = false;
    ready = false;
    deleteConfig: IRuleDeleteConfirmationModalResolve = {
        promiseToResolve: null,
        modalTitle: this.translatePipe.transform("DeleteRule"),
        confirmationText: this.translatePipe.transform("DeleteRuleConfirmationText"),
        submitButtonText: this.translatePipe.transform("Delete"),
        cancelButtonText: this.translatePipe.transform("Cancel"),
        callbackPromise: (rules): ng.IPromise<boolean> => {
            const deletedRule: IRule[] = remove(rules, (rule: IRule) => {
                return rule.delete;
            });
            deletedRule[0].subRules = [];
            return this.dsarWebformRulesApiService.saveRules(this.webformId, deletedRule, true).then((res: IRule[]) => {
                this.originalCopy = rules.filter((rule: IRule) => {
                    return !rule.isNew;
                });
                return !!res;
            });
        },
    };

    translations = {
        EqualTo: this.translatePipe.transform("EqualTo"),
        NotEqualTo: this.translatePipe.transform("NotEqualTo"),
        SubjectType: this.translatePipe.transform("SubjectType"),
        RequestType: this.translatePipe.transform("RequestType"),
        Language: this.translatePipe.transform("Language"),
        Country: this.translatePipe.transform("Country"),
        Workflow: this.translatePipe.transform("Workflow"),
        DaysToComplete: this.translatePipe.transform("DaysToComplete"),
        Organization: this.translatePipe.transform("Organization"),
        Approver: this.translatePipe.transform("Approver"),
        EmailConfirmation: this.translatePipe.transform("SendRequestDetailsEmail"),
    };

    constructor(
        private dsarWebformRulesApiService: DsarWebformRulesApiService,
        private readonly translatePipe: TranslatePipe,
    ) {}

    ngOnInit() {
        this.dsarWebformRulesApiService.geWebformRuleMeta(this.webformId).then((meta: IWebformRuleMeta) => {
            this.webformRuleMeta = meta;
            this.initConfig();
            this.dsarWebformRulesApiService.getAllRuleMetas(this.webformId).then((rules: IRuleMeta[]): void => {
                this.rules = rules;
                this.originalCopy = cloneDeep(this.rules);
            }).finally(() => this.ready = true);
        });

    }

    initConfig() {
        const defaultWorkflow = find(this.webformMeta.workflows.publishedWorkflows, (x) => x.referenceId === this.webformMeta.workflows.selectedWorkflow);
        const defaultDeadline = find(this.webformMeta.settingsInputs, (x) => x.fieldName === "DefaultDaysToRespond");
        const defaultReviewer = find(this.webformMeta.settingsInputs, (x) => x.fieldName === "DefaultReviewer");
        const sortedWorkflows = orderBy(this.webformMeta.workflows.publishedWorkflows, [(workflow) => workflow.workflowName.toLowerCase()], ["asc"]);
        const dsarRulesSharedConfig = {
            operators: [
                {
                    operator: RuleOperator.EQUAL_TO,
                    operatorDisplay: this.translations.EqualTo,
                },
                {
                    operator: RuleOperator.NOT_EQUAL_TO,
                    operatorDisplay: this.translations.NotEqualTo,
                },
            ],
            labelKey: "value",
            valueKey: "key",
        };
        this.config = {
            actions: [
                {
                    field: DsarWebformRuleActionKeys.WORKFLOWID,
                    fieldDisplay: `${this.translations.Workflow}:`,
                    type: RuleActionType.Picklist,
                    defaultValue: defaultWorkflow,
                    picklistOptions: sortedWorkflows,
                    picklistValueKey: "referenceId",
                    picklistLabelKey: "workflowName",
                    required: true,
                },
                {
                    field: DsarWebformRuleActionKeys.ORGGROUPID,
                    fieldDisplay: `${this.translations.Organization}:`,
                    type: RuleActionType.OrgSelect,
                    defaultValue: this.webformMeta.webFormOrgId,
                    orgSelectChoosesUserOrg: true,
                    required: true,
                },
                {
                    field: DsarWebformRuleActionKeys.DEADLINE,
                    fieldDisplay: this.translations.DaysToComplete,
                    type: RuleActionType.InputNumber,
                    defaultValue: defaultDeadline.value,
                    inputNumberMin: defaultDeadline.min,
                    inputNumberMax: MAX_DEADLINE,
                    required: true,
                },
                {
                    field: DsarWebformRuleActionKeys.REVIEWER,
                    fieldDisplay: `${this.translations.Approver}:`,
                    type: RuleActionType.OrgUser,
                    defaultValue: defaultReviewer.value,
                    orgUserTraversal: OrgUserTraversal.Up,
                    orgSelectDefaultValue: this.webformMeta.webFormOrgId,
                    orgSelectChoosesUserOrg: true,
                    required: false,
                },
                {
                    defaultValue: [] as IOrgUserAdapted[],
                    field: DsarWebformRuleActionKeys.CONFIRMATIONMAIL,
                    fieldDisplay: `${this.translations.EmailConfirmation}`,
                    orgUserTraversal: OrgUserTraversal.Up,
                    orgSelectDefaultValue: this.webformMeta.webFormOrgId,
                    orgSelectChoosesUserOrg: true,
                    required: false,
                    type: RuleActionType.OrgUserMultiSelect,
                    placeholder: "SelectRecipient",
                },
            ],
            conditionMeta: [
                {
                    field: DsarWebformRuleConditionKeys.SUBJECT_TYPE,
                    fieldDisplay: this.translations.SubjectType,
                    options: filter(this.webformRuleMeta.subjectTypes, "selected"),
                    ...dsarRulesSharedConfig,
                },
                {
                    field: DsarWebformRuleConditionKeys.REQUEST_TYPE,
                    fieldDisplay: this.translations.RequestType,
                    options: filter(this.webformRuleMeta.requestTypes, "selected"),
                    ...dsarRulesSharedConfig,
                },
                {
                    field: DsarWebformRuleConditionKeys.LANGUAGE,
                    fieldDisplay: this.translations.Language,
                    options: this.webformRuleMeta.languages,
                    ...dsarRulesSharedConfig,
                },
                {
                    field: DsarWebformRuleConditionKeys.COUNTRY,
                    fieldDisplay: this.translations.Country,
                    options: this.webformRuleMeta.countries,
                    ...dsarRulesSharedConfig,
                },
            ],
        };

        if (!isEmpty(this.webformRuleMeta.multiselectFields)) {
            forIn(this.webformRuleMeta.multiselectFields, (value: Array<IKeyValue<string>>, key: string): void => {
                const condition = {
                    field: "multiselectFields." + key,
                    fieldDisplay: this.formTranslations[key],
                    options: value,
                    ...dsarRulesSharedConfig,
                };
                this.config.conditionMeta.push(condition);
            });
        }
    }

    editModeChanged(mode: boolean) {
        this.ruleEditMode = mode;
        this.actionCallback.emit({
            type: DSARStoreActions.STORE_RULES,
            payload: this.ruleEditMode,
        });
    }

    resetRules() {
        this.rules = cloneDeep(this.originalCopy);
    }

    rulesSaved(rules: IRule[]) {
        this.savePromise(rules).then((savedRules: IRule[]) => {
            if (savedRules.length) {
                this.rules = savedRules;
                this.originalCopy = cloneDeep(this.rules);
            } else {
                this.rules = [];
                this.originalCopy = [];
            }
        });
    }

    getRuleById(ruleId: string) {
        const rule = find(this.rules, (x) => x.id === ruleId);
        if (!rule.subRules || !rule.actions) {
            this.dsarWebformRulesApiService.getRuleById(ruleId).then((ruleData: IRule): void => {
                rule.subRules = ruleData.subRules;
                // modify existing rules to have CONFIRMATIONMAIL field
                if (!ruleData.actions.find((value) => value.field === DsarWebformRuleActionKeys.CONFIRMATIONMAIL)) {
                    ruleData.actions.push({ field: DsarWebformRuleActionKeys.CONFIRMATIONMAIL, value: [] as IOrgUserAdapted[]});
                }
                rule.actions = ruleData.actions;
                rule.isOpen = true;
                const reviewer = ruleData.actions.find((value) => value.field === DsarWebformRuleActionKeys.REVIEWER);
                if (!reviewer.value) {
                    this.config.actions.forEach((value: IRuleActionConfig): IRuleActionConfig => {
                        if (value.field === DsarWebformRuleActionKeys.REVIEWER) {
                            value.defaultValue = "";
                        }
                        return value;
                    });
                }
            });
        }
    }
}
