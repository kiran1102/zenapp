// Angular
import {
    Component,
    Inject,
} from "@angular/core";
import { NgForm } from "@angular/forms";

// Rxjs
import {
    Observable,
    BehaviorSubject,
    Subject,
} from "rxjs";

import { Regex } from "constants/regex.constant";

// Interfaces
import {
    IProtocolPacket,
    IProtocolResponse,
} from "interfaces/protocol.interface";
import {
    IResponseTemplate,
    ITranslations,
} from "dsarModule/interfaces/response-templates.interface";
import { IDSARSaveResponseTemplateModalResolve } from "interfaces/dsar-save-response-template-modal-resolve.interface";
import { IGetWebformLanguageResponse } from "interfaces/dsar/dsar-edit-language.interface";

// Services
import { Permissions } from "modules/shared/services/helper/permissions.service";
import { DSARSettingsService } from "dsarservice/dsar-settings.service";
import { TenantTranslationService } from "sharedServices/tenant-translation.service";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

// Vitreus
import { IOtModalContent } from "@onetrust/vitreus";

@Component({
    selector: "dsar-response-templates-save-modal",
    templateUrl: "./dsar-response-templates-save-modal.component.html",
})
export class DSARSaveResponseTemplateModalComponent implements IOtModalContent {

    templateTranslation: boolean;
    isLoading = true;
    languageIndex = -1;
    sessionLanguage: string;
    templateContentTouched = false;
    templateNameTouched = false;
    addTemplateForm: NgForm;

    otModalCloseEvent: Subject<any>;
    otModalData: IDSARSaveResponseTemplateModalResolve;

    submitButtonText: string;
    isSubmitting: boolean;
    canSubmit = false;
    templateName: string;
    templateContent: string;
    contentSource = new BehaviorSubject("");
    contentObservable: Observable<string> = this.contentSource.asObservable();
    customModules = [
        ["bold", "italic", "underline", "strike", { list: "ordered" }, { list: "bullet" }],
        [{ align: "" }, { align: "right" }, { align: "center" }, { align: "justify" }, "link"]];
    languageList: IGetWebformLanguageResponse[];
    selectedLanguage: IGetWebformLanguageResponse;
    translations: ITranslations[];
    hasTextContent: boolean;

    constructor(
        @Inject("TenantTranslationService") readonly tenantTranslationService: TenantTranslationService,
        private readonly dsarSettingsService: DSARSettingsService,
        private readonly permissions: Permissions,
        private readonly translatePipe: TranslatePipe,
    ) { }

    ngOnInit() {
        this.templateTranslation = this.permissions.canShow("DSARResponseTemplateLanguageSupport");
        this.sessionLanguage = this.tenantTranslationService.getCurrentLanguage();
        this.submitButtonText = this.otModalData.isEditModal ? this.translatePipe.transform("Save") : this.translatePipe.transform("Add");
        const list: IGetWebformLanguageResponse[] = [...this.otModalData.languages];
        this.languageList = list.filter((lang: IGetWebformLanguageResponse) => lang.Name !== "----");
        if (this.otModalData.isEditModal) {
            this.dsarSettingsService.getResponseTemplateById(this.otModalData.row.id)
                .then((res: IProtocolResponse<IResponseTemplate>): boolean => {
                    if (res.result) {
                        this.selectedLanguage = this.languageList.find((language: IGetWebformLanguageResponse): boolean => {
                            return this.otModalData.row.language === language.Code;
                        });
                        this.translations = res.data.translations;
                        this.getLanguageIndexOnTranslations();
                        const templateNameAndContent = this.translations[this.languageIndex];
                        this.templateName = templateNameAndContent.templateName;
                        this.templateContent = templateNameAndContent.templateContent;
                        this.contentSource.next(templateNameAndContent.templateContent.replace(/(?:\r\n|\r|\n)/g, "<br>"));
                        this.isLoading = false;
                    }
                    return res.result;
                });
        } else {
            this.selectedLanguage = this.languageList.find((language: IGetWebformLanguageResponse): boolean => {
                return language.IsDefault;
            });
            this.translations = [];
            this.templateName = "";
            this.contentSource.next("");
            this.isLoading = false;
        }
    }

    getLanguageIndexOnTranslations() {
        this.languageIndex = this.translations.findIndex((translation: ITranslations): boolean => {
            return translation.language === this.selectedLanguage.Code;
        });
    }

    selectLanguage(lang: IGetWebformLanguageResponse) {
        this.selectedLanguage = lang;
        this.getLanguageIndexOnTranslations();
        const translationForSelectedLanguage = this.translations[this.languageIndex];
        if (translationForSelectedLanguage) {
            this.templateName = translationForSelectedLanguage.templateName;
            this.contentSource.next(translationForSelectedLanguage.templateContent);
        } else {
            this.templateName = "";
            this.contentSource.next("");
        }
    }

    handleInputChange(payload: string) {
        this.templateName = payload;
        this.templateNameTouched = true;
        this.validateSubmit();
    }

    handleMessageChange(message: string) {
        if (message && !$(message).text().trim()) {
            this.templateContent = "";
        } else {
            this.templateContent = message;
        }
        this.templateContentTouched = true;
        this.validateSubmit();
    }

    closeModal(result: boolean, data: any) {
        this.otModalCloseEvent.next({ result, data });
    }

    private handleSubmit() {
        this.isSubmitting = true;
        this.translations[this.languageIndex].templateContent = this.translations[this.languageIndex].templateContent.replace(" <", "&nbsp;<").replace("> ", ">&nbsp;");
        if (this.otModalData.isEditModal) {
            const editTemplate: IResponseTemplate = {
                id: this.otModalData.row.id,
                status: this.otModalData.row.status,
                type: this.otModalData.row.type,
                translations: this.translations,
            };
            this.dsarSettingsService.editResponseTemplate(editTemplate).then((res: IProtocolPacket) => {
                this.closeModal(res.result, res.data);
                this.isSubmitting = false;
            });
        } else {
            const newTemplate: IResponseTemplate = {
                translations: this.translations,
            };
            this.dsarSettingsService.createResponseTemplate(newTemplate).then((res: IProtocolPacket) => {
                this.closeModal(res.result, res.data);
                this.isSubmitting = false;
            });
        }
    }

    private validateSubmit() {
        this.canSubmit = false;
        if (this.templateContent) {
            const trimmedText = this.templateContent.replace(Regex.REMOVE_HTML_TAGS, "");
            const textWithoutSpace = trimmedText.replace(Regex.REMOVE_SPACE_BETWEEN_TAGS, "");
            this.hasTextContent = textWithoutSpace.trim().length > 0;
        } else {
            this.hasTextContent = false;
        }
        this.canSubmit = Boolean(this.templateName.trim() && this.hasTextContent);
        if (this.canSubmit) {
            if (this.languageIndex !== -1) {
                this.translations[this.languageIndex].templateName = this.templateName;
                this.translations[this.languageIndex].templateContent = this.templateContent;
            } else {
                this.translations.push({
                    language: this.selectedLanguage.Code,
                    templateName: this.templateName,
                    templateContent: this.templateContent,
                });
                this.languageIndex = this.translations.length - 1;
            }
        }
    }
}
