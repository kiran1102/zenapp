// Angular
import {
    Component,
    Inject,
} from "@angular/core";

// Lodash
import {
    isArray,
    replace,
    unescape as lodashUnescape,
} from "lodash";

import {
    IISystem,
    IIWorkflow,
} from "modules/intg/interfaces/integration.interface";

// Enums and Constants
import {
    SubTaskStatus,
    SubTaskCommentType,
} from "dsarModule/enums/sub-task.enum";
import { RequestQueueActions } from "dsarModule/constants/dsar-request-queue.constants";

// Rxjs
import { Subject } from "rxjs";

// Services
import { RequestQueueService } from "dsarservice/request-queue.service";
import { StoreToken } from "tokens/redux-store.token";
import { Permissions } from "modules/shared/services/helper/permissions.service";
import { RequestQueueDetailService } from "dsarservice/request-queue-detail-service";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";
import { OtDatePipe } from "pipes/ot-date.pipe";

// Interfaces
import { IStore } from "interfaces/redux.interface";
import { IDSARRespondSubTaskModalResolve } from "interfaces/sub-tasks.interface";
import {
    IAttachmentFile,
    IAttachmentFileResponse,
} from "interfaces/attachment-file.interface";
import { IPageableOf } from "interfaces/pagination.interface";

import {
    IProtocolPacket,
    IProtocolResponse,
} from "interfaces/protocol.interface";
import { IStringMap } from "interfaces/generic.interface";
import { SubTaskType } from "dsarModule/enums/sub-task.enum";
import { first } from "rxjs/operators";

import {
    ISubTaskAttachment,
    ISubTaskComment,
    ISubTask,
    ISubTaskData,
    IFileInput,
} from "modules/dsar/components/sub-tasks/shared/interfaces/sub-tasks.interface";

@Component({
    selector: "dsar-response-sub-task-modal",
    templateUrl: "./dsar-response-sub-task-modal.component.html",
})
export class DsarResponseSubTaskModal {
    isSubmitting: boolean;
    systems: IISystem[] = [];
    filterSystems: IISystem[] = [];
    workFlowList: IIWorkflow[] = [];
    workFlowListFilter: IIWorkflow[] = [];
    workFlowListCustomFind: IIWorkflow[] = [];
    selectedSystem: IISystem;
    dataDiscoveryFileResponse: ISubTaskAttachment;
    selectedSystemName = "";
    selectedSystemWorkflow: IIWorkflow;
    selectedWorkFlowName = "";
    isLoading = true;
    canSubmit = false;
    taskId: string;
    taskName: string;
    taskType: number;
    taskTypes = SubTaskType;
    isSystemTask: boolean;
    taskComment: string;
    taskResponse = "";
    maxSize = 64;
    assignee: string;
    internalComment = true;
    attachments: IAttachmentFile[];
    completed = false;
    completeCheckbox: boolean;
    allowedFileExtensions = ["csv", "doc", "docx", "jpg", "jpeg", "mpp", "msg", "pdf", "png", "ppt", "pptx", "txt", "vsd", "vsdx", "xls", "xlsx"];
    requestId: string;
    disableFields: boolean;
    taskResponseTouched = false;
    otModalCloseEvent: Subject<IProtocolResponse<ISubTask>>;
    otModalData: IDSARRespondSubTaskModalResolve;
    customModules = [
        [   "bold", "italic", "underline", "strike", "image",
            { list: "ordered" },
            { list: "bullet" },
        ],
        [
            { align: "" },
            { align: "right" },
            { align: "center" },
            { align: "justify" },
            "link",
        ],
    ];
    deadline: string | number;
    maxSizeText = this.translatePipe.transform("FilesLargerThanSizeNotSupported", { MaxSize: this.maxSize });
    permissionsDSAR: IStringMap<boolean>;
    cancelButtonText: string;
    dataDiscoveryResponse = "";
    integrationSystemId = "";
    integrationWorkflowId = "";
    responseLabelText = "";
    private attachmentResponse: ISubTaskAttachment[];

    constructor(
        @Inject(StoreToken) public store: IStore,
        private readonly permissions: Permissions,
        private requestQueueService: RequestQueueService,
        private translatePipe: TranslatePipe,
        private otDate: OtDatePipe,
        readonly requestQueueDetailService: RequestQueueDetailService,

    ) {}

    ngOnInit(): void {
        this.taskId = this.otModalData.row.id;
        this.taskType = this.otModalData.row.type;
        this.isSystemTask = this.taskType === this.taskTypes.System;
        this.integrationSystemId = this.otModalData.row.integrationSystemId;
        if (this.isSystemTask) {
            this.getCustomSystem(this.integrationSystemId);
            this.integrationWorkflowId = this.otModalData.row.integrationWorkflowId;
            this.getCustomWorkFlow(this.integrationSystemId, this.integrationWorkflowId);
        }
        this.attachments = [];
        this.taskName = this.otModalData.row.taskName;
        this.taskComment = lodashUnescape(this.otModalData.row.taskDescription);
        this.requestId = this.otModalData.requestId;
        this.completed = this.otModalData.row.status === SubTaskStatus.Complete;
        this.assignee = this.otModalData.row.assignee;
        if (this.otModalData.row.comments) {
            const taskResponseComment: ISubTaskComment = this.otModalData.row.comments.find(
                (comment: ISubTaskComment): boolean =>
                    comment.type === SubTaskCommentType.Response,
            );
            if (taskResponseComment) {
                this.taskResponse = lodashUnescape(taskResponseComment.comment);
            }

            const taskResponseDiscoveryComment: ISubTaskComment[] = this.otModalData.row.comments.filter(
                (comment: ISubTaskComment): boolean => (comment.isDataDiscovery === true));
            if (taskResponseDiscoveryComment.length > 0) {
                this.dataDiscoveryResponse = taskResponseDiscoveryComment[0].comment;
                this.dataDiscoveryFileResponse = taskResponseDiscoveryComment[0].files
                    ? taskResponseDiscoveryComment[0].files[0] : null;
            }
        }
        this.disableFields = this.otModalData.disableFields;
        this.deadline = this.otModalData.row.deadline;
        this.permissionsDSAR = {
            DSARSubTaskResponse: this.permissions.canShow("DSARSubTaskResponse"),
            DSARSubTaskComplete: this.permissions.canShow("DSARSubTaskComplete"),
        };
        this.cancelButtonText = this.permissionsDSAR.DSARSubTaskComplete
            ? this.translatePipe.transform("Cancel")
            : this.translatePipe.transform("Close");
        this.responseLabelText = this.isSystemTask
            ? this.translatePipe.transform("Comments")
            : this.translatePipe.transform("TaskResponse");
    }

    closeModal(data?: IProtocolResponse<ISubTask>) {
        this.otModalCloseEvent.next(data);
    }

    filterWorkflows(id: string) {
        this.workFlowListFilter = this.workFlowList.filter((data) => {
            return data.referenceIntegrationId.match(id);
        });
    }
    getCustomWorkFlow(systemId: string, workFlowId: string) {
        return this.requestQueueService.getWorkflows().pipe(first(),
        ).subscribe(
            (workflowsResponse: IProtocolResponse<IPageableOf<IIWorkflow>>) => {
                this.workFlowList = workflowsResponse.data.content || [];
                this.filterWorkflows(systemId);
                this.workFlowListCustomFind = this.workFlowListFilter.filter((data) => {
                    return data.id.match(workFlowId);
                });
                this.selectedSystemWorkflow = this.workFlowListCustomFind[0];
                this.selectedWorkFlowName = this.selectedSystemWorkflow.name;
                this.isLoading = false;
            },
        );
    }

    getCustomSystem(id: string) {
        return this.requestQueueService.getSeededIntegrations(true).pipe(
            first(),
        ).subscribe(
            (systems: IProtocolResponse<IPageableOf<IISystem>>) => {
            this.systems = systems.data.content.filter((data) => {
                return !data.name.startsWith("OneTrust-");
            });
            this.filterSystems = this.systems.filter((data) => {
                return data.id.match(id);
            });
            this.selectedSystem = this.filterSystems[0];
            this.selectedSystemName = this.selectedSystem.name;
            this.isLoading = false;
            },
        );
    }

    toggleComplete(): void {
        this.completed = !this.completed;
        this.validateSubmit();
    }

    attachmentActions(type: string, attachment: IAttachmentFileResponse): void {
        switch (type) {
            case RequestQueueActions.DOWNLOAD_FILE:
                this.requestQueueDetailService.handleAction(RequestQueueActions.DOWNLOAD_FILE, attachment);
                break;
            case RequestQueueActions.DELETE_FILE:
                this.requestQueueDetailService.handleAction(RequestQueueActions.DELETE_FILE, attachment);
                break;
        }
    }
    getFiles(files: any) {
        if (files.length)
        files.forEach((file: File) => {
            if (file.size < this.maxSize * 1024 * 1024) {
                const reader: FileReader = new FileReader();
                const fileObj: IFileInput = {
                    Data: "",
                    FileName: this.getFileName(file.name),
                    Extension: this.getFileExtension(file.name),
                    Blob: file,
                };
                reader.readAsDataURL(file);
                reader.onload = () => {
                    fileObj.Data = reader.result as string;
                    if (fileObj.Data.length) {
                        this.attachments.push(fileObj);
                    }
                };
            }
        });
        this.validateSubmit();
    }

    formattedDeadline(): string {
        return this.otDate.transform(this.deadline.toString());
    }

    removeFile($event) {
        this.attachments.splice($event, 1);
    }

    getFileExtension(fileName: string): string {
        // Return only file extension
        return fileName.split(".").pop() || "";
    }
    getFileName(fileName: string): string {
        const ext = this.getFileExtension(fileName);
        // Return file name without extension
        return replace(fileName, `.${ext}`, "");
    }

    handleMessageChange(message: string): void {
        if (message && !message.includes("img") && !$(message).text().trim()) {
            this.taskResponse = "";
        } else {
            this.taskResponse = message;
        }
        this.taskResponseTouched = true;
        this.validateSubmit();
    }

    handleSubmit(): void {
        this.isSubmitting = true;
        // TODO: use the attachment service and remove this dry code - waiting for it to be available in dev.
        // Till then, this will be used to deliver the feature for clients ASAP
        const promiseArr: Array<ng.IPromise<IProtocolPacket>> = [];
        this.attachments.forEach((file: IAttachmentFile) => {
            const fileData: IAttachmentFile = {
                FileName: `${file.FileName}.${file.Extension}`,
                Extension: file.Extension,
                Blob: file.Blob,
                IsInternal: true,
                Type: 30,
                RequestId: this.requestId,
                Name: file.FileName,
            };
            promiseArr.push(this.requestQueueService.addAttachment(fileData));
        });
        if (!promiseArr.length) {
            this.callCompleteSubTaskEndpoint();
        } else {
            Promise.all(promiseArr).then(
                (
                    responses: Array<IProtocolResponse<IAttachmentFileResponse>>,
                ): void => {
                    this.attachmentResponse = [];
                    if (isArray(responses) && responses.length) {
                        const failedResponses: Array<
                            IProtocolResponse<IAttachmentFileResponse>
                            > = responses.filter((response) => !response.result);
                        if (!failedResponses.length) {
                            responses.forEach((response: IProtocolResponse<IAttachmentFileResponse>) => {
                                const attachmentObjectForComment: ISubTaskAttachment = {
                                    fileId: response.data.Id,
                                    fileName: response.data.FileName,
                                    isInternal: true,
                                };
                                this.attachmentResponse.push(
                                    attachmentObjectForComment,
                                );
                            });
                            this.callCompleteSubTaskEndpoint();
                        } else {
                            this.isSubmitting = false;
                        }
                    }
                    this.isSubmitting = false;
                },
            );
        }
    }

    private validateSubmit(): void {
        this.canSubmit = Boolean(this.taskResponse);
    }

    private callCompleteSubTaskEndpoint(): void {
        const saveTask: ISubTaskData = {
            comment: this.taskResponse,
            markAsComplete: this.completed,
            files: this.attachmentResponse,
            isInternal: this.internalComment,
        };
        this.requestQueueService.completeSubTask(this.taskId, saveTask).then(
            (res: IProtocolResponse<ISubTask>): void => {
                this.closeModal(res);
            });
    }
}
