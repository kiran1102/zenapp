// Angular
import {
    Component,
    OnInit,
    OnChanges,
    Input,
    Inject,
} from "@angular/core";
import { StateService } from "@uirouter/core";

// Rxjs
import { Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";

// 3rd Party
import {
    filter,
    find,
    orderBy,
} from "lodash";

// Services
import { WebformApiService } from "modules/dsar/services/webform-api.service";
import { DSARSettingsService } from "dsarservice/dsar-settings.service";
import { TenantTranslationService } from "sharedServices/tenant-translation.service";
import { Permissions } from "modules/shared/services/helper/permissions.service";
import {
    ConfirmModalType,
    OtModalService,
    ModalSize,
    ToastService,
} from "@onetrust/vitreus";
import { OrgGroupApiService } from "oneServices/api/org-group-api.service";

// Interfaces
import { IAction } from "interfaces/redux.interface";
import { IDSARSaveResponseTemplateModalResolve } from "interfaces/dsar-save-response-template-modal-resolve.interface";
import {
    IGetWebformLanguageResponse,
    Status,
} from "interfaces/dsar/dsar-edit-language.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";
import {
    IResponseTemplate,
    IResponseTemplateListParams,
} from "dsarModule/interfaces/response-templates.interface";
import { IPageableOf } from "interfaces/pagination.interface";
import { IStore } from "interfaces/redux.interface";
import { IOrgUserAdapted } from "interfaces/org-user.interface";
import { IUserMap } from "interfaces/user-reducer.interface";

// Constants and Enums
import {
    Actions,
    DefaultResponseTemplateId,
} from "dsarModule/constants/response-templates.constants";
import { OrgUserTraversal } from "enums/org-user-dropdown.enum";

// Components
import { DSARSaveResponseTemplateModalComponent } from "dsarModule/components/response-templates/save-modal/dsar-response-templates-save-modal.component";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

// Tokens
import { StoreToken } from "tokens/redux-store.token";

// Redux
import { getCurrentOrgId } from "oneRedux/reducers/org.reducer";

@Component({
    selector: "dsar-response-templates",
    templateUrl: "./dsar-response-templates.component.html",
    host: {
        class: "flex-full-height",
    },
})
export class DsarResponseTemplatesComponent implements OnInit, OnChanges {
    @Input() selectedLanguageList;
    @Input() addNewModal;

    readonly defaultParams: IResponseTemplateListParams = {
        page: 1,
        size: 20,
        searchKey: "",
    };

    enableTemplates: boolean;
    editTemplate: boolean;
    templateTranslation: boolean;

    params: IResponseTemplateListParams = {
        page: (parseInt(this.stateService.params.page, 10) || this.defaultParams.page) - 1 || 0,
        size: parseInt(this.stateService.params.size, 10) || this.defaultParams.size,
        searchKey: this.stateService.params.search,
    };

    isLoading: boolean;
    languageList: IGetWebformLanguageResponse[];
    searchString = "";
    selectedLanguage: IGetWebformLanguageResponse;
    sessionLanguage: string;
    templateList: IPageableOf<IResponseTemplate>;
    templateTableData: IResponseTemplate[];
    users: IUserMap = {};

    private destroy$ = new Subject();

    constructor(
        @Inject(StoreToken) private store: IStore,
        private stateService: StateService,
        @Inject("TenantTranslationService") private readonly tenantTranslationService: TenantTranslationService,
        private readonly dsarSettingsService: DSARSettingsService,
        private dsarWebFormService: WebformApiService,
        private readonly otModalService: OtModalService,
        private readonly toastService: ToastService,
        private readonly permissions: Permissions,
        private readonly translatePipe: TranslatePipe,
        private orgGroupApi: OrgGroupApiService,
    ) { }

    ngOnInit() {
        this.enableTemplates = this.permissions.canShow("DSARSettingResponseTemplates");
        this.templateTranslation = this.permissions.canShow("DSARResponseTemplate");
        this.isLoading = true;
        this.sessionLanguage = this.tenantTranslationService.getCurrentLanguage();
        this.orgGroupApi.getOrgUsersWithPermission(
            getCurrentOrgId(this.store.getState()),
            OrgUserTraversal.All,
        ).then((response: IProtocolResponse<IOrgUserAdapted[]>): void => {
            if (response.result) {
                response.data.forEach((orgUser: IOrgUserAdapted): void => {
                    this.users[orgUser.Id] = orgUser;
                });
            }
        });
        this.dsarWebFormService.getSelectedLanguages(DefaultResponseTemplateId)
            .then((res: IProtocolResponse<IGetWebformLanguageResponse[]>) => {
                if (res.result) {
                    this.populateLanguageList(res.data);
                    if (this.languageList.length > 1) {
                        this.languageList.unshift({
                            Id: 0,
                            Code: "",
                            Name: "----",
                            Status: Status.Enabled,
                            IsDefault: false,
                        });
                    }
                    this.getResponseTemplates();
                } else {
                    this.isLoading = false;
                }
            });
    }

    ngOnChanges(data: ng.IOnChangesObject) {
        if (data.addNewModal && !data.addNewModal.isFirstChange()) {
            this.handleAction({ type: Actions.ADD_NEW_TEMPLATE, payload: {} });
        }
        if (data.selectedLanguageList && !data.selectedLanguageList.isFirstChange()) {
            this.languageList = [];
            const list: IGetWebformLanguageResponse[] = data.selectedLanguageList.currentValue;
            this.populateLanguageList(list);
            if (this.languageList.length > 1) {
                this.languageList.unshift({
                    Id: 0,
                    Code: "",
                    Name: "----",
                    Status: Status.Enabled,
                    IsDefault: false,
                });
            }
            this.selectLanguage(this.selectedLanguage);
        }
    }

    getResponseTemplates(language?: IGetWebformLanguageResponse, page?: number, searchString?: string) {
        this.dsarSettingsService.getAllEnabledResponseTemplates(
                language ? language.Code : this.selectedLanguage.Code,
                page || this.params.page,
                searchString || this.searchString,
            ).then((res: IProtocolResponse<IPageableOf<IResponseTemplate>>) => {
            if (res.result) {
                this.templateList = res.data;
                this.buildResponseTemplateList(res.data.content);
            }
            this.isLoading = false;
        });
    }

    populateLanguageList(languages: IGetWebformLanguageResponse[]) {
        this.languageList = filter(languages, ["Status", Status.Enabled]);
        const sessionLanguage = find(this.languageList, ["Code", this.sessionLanguage]);
        this.selectedLanguage = sessionLanguage ? sessionLanguage : find(this.languageList, ["IsDefault", true]);
    }

    search(searchInput: string) {
        this.isLoading = true;
        this.params.page = 0;
        this.searchString = searchInput;
        this.getResponseTemplates(this.selectedLanguage, 0, searchInput);
    }

    selectLanguage(lang: IGetWebformLanguageResponse) {
        this.isLoading = true;
        this.params.page = 0;
        this.searchString = "";
        this.selectedLanguage = lang;
        this.getResponseTemplates(lang, this.params.page, "");
    }

    handlePageChange(page: number) {
        this.handleAction({ type: Actions.CHANGE_PAGE, payload: { page } });
    }

    handleAction(action: IAction) {
        switch (action.type) {
            case Actions.ADD_NEW_TEMPLATE:
                const dsarAddTemplateModalData: IDSARSaveResponseTemplateModalResolve = {
                    isEditModal: false,
                    title: this.translatePipe.transform("AddNewResponse"),
                    languages: this.languageList,
                };
                this.otModalService.create(
                        DSARSaveResponseTemplateModalComponent,
                        {
                            isComponent: true,
                            size: ModalSize.MEDIUM,
                        },
                        dsarAddTemplateModalData,
                    ).pipe(
                        takeUntil(this.destroy$),
                    ).subscribe(
                        (res: IProtocolResponse<IResponseTemplate>) => {
                            if (res && res.result && typeof res.result !== "undefined") {
                                if (res.result) {
                                    this.toastService.show(this.translatePipe.transform("Success"), this.translatePipe.transform("ResponseTemplateCreated"), "success");
                                    this.getResponseTemplates();
                                } else {
                                    this.toastService.show(this.translatePipe.transform("Error"), this.translatePipe.transform("ResponseTemplateCreationError"), "error");
                                }
                            }
                    });
                break;
            case Actions.EDIT_TEMPLATE:
                const dsarEditTemplateModalData: IDSARSaveResponseTemplateModalResolve = {
                    isEditModal: true,
                    row: action.payload,
                    title: this.translatePipe.transform("EditResponse"),
                    languages: this.languageList,
                };
                this.otModalService.create(DSARSaveResponseTemplateModalComponent,
                        {
                            isComponent: true,
                            size: ModalSize.MEDIUM,
                        },
                        dsarEditTemplateModalData,
                    ).pipe(
                        takeUntil(this.destroy$),
                    ).subscribe(
                        (res: IProtocolResponse<IResponseTemplate>) => {
                            if (res && res.result && typeof res.result !== "undefined") {
                                if (res.result) {
                                    this.toastService.show(this.translatePipe.transform("Success"), this.translatePipe.transform("ResponseTemplateSaved"), "success");
                                    this.getResponseTemplates();
                                } else {
                                    this.toastService.show(this.translatePipe.transform("Error"), this.translatePipe.transform("ResponseTemplateSaveError"), "error");
                                }
                            }
                    });
                break;
            case Actions.DELETE_TEMPLATE:
                this.otModalService.confirm({
                    type: ConfirmModalType.DELETE,
                    translations: {
                        title: this.translatePipe.transform("DeleteResponse"),
                        confirm: this.translatePipe.transform("AreYouSureContinue"),
                        desc: this.translatePipe.transform("DeleteResponseDescription"),
                        submit: this.translatePipe.transform("Delete"),
                        cancel: this.translatePipe.transform("Cancel"),
                    },
                }).pipe(
                    takeUntil(this.destroy$),
                ).subscribe(
                    (data: boolean) => {
                        if (data) {
                            this.dsarSettingsService.deleteResponseTemplate(action.payload.id).then((response: IProtocolResponse<void>) => {
                                if (response.result) {
                                    this.toastService.show(this.translatePipe.transform("Success"), this.translatePipe.transform("ResponseTemplateDeleted"), "success");
                                    if (this.templateTableData.length === 1) { --this.params.page; }
                                    this.getResponseTemplates();
                                } else {
                                    this.toastService.show(this.translatePipe.transform("Error"), this.translatePipe.transform("ResponseTemplateDeletionError"), "error");
                                }
                        });
                        }
                    },
                );
                break;
            case Actions.CHANGE_PAGE:
                this.params.page = action.payload.page;
                this.isLoading = true;
                this.getResponseTemplates();
                break;
            default:
                return;
        }
    }

    ngOnDestroy() {
        this.destroy$.next();
        this.destroy$.complete();
    }

    private buildResponseTemplateList(data: IResponseTemplate[]) {
        this.templateTableData = orderBy(data, [(template: IResponseTemplate): string => template.templateName.toLowerCase()]);
    }
}
