// Angular
import {
    Component,
    OnInit,
} from "@angular/core";

// 3rd Party
import { find } from "lodash";

// Interfaces
import { IDSARSettingsUpdate } from "dsarModule/interfaces/dsar-settings.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";
import { IDSARSettingsGroup } from "dsarModule/interfaces/dsar-settings-group.interface";

// Services
import { DSARSettingsService } from "dsarservice/dsar-settings.service";

@Component({
    selector: "dsar-response-templates-settings",
    templateUrl: "./dsar-response-templates-settings.component.html",
    host: {
        class: "flex-full-height height-100 padding-all-2 background-white",
    },
})

export class ResponseTemplateSettings implements OnInit {
    limitResponseTemplate: boolean;
    isLoading = true;

    constructor(
        readonly dsarSettingsService: DSARSettingsService,
    ) {}

    ngOnInit(): void {
        this.dsarSettingsService.getSettingsByGroup("DSResponseTemplate")
            .then((response: IProtocolResponse<IDSARSettingsGroup>): boolean => {
                if (response.result) {
                    const responseTemplateSettings = response.data ? response.data.settings : [];
                    this.limitResponseTemplate = find(responseTemplateSettings, ["name", "DSResponseTemplatePreferredLanguageRestrict"]).value === "true";
                    this.isLoading = false;
                }
                return response.result;
            });
    }

    toggleAndSaveResponseTemplateSetting(): void {
        this.limitResponseTemplate = !this.limitResponseTemplate;
        const responseTemplateUpdateBody: IDSARSettingsUpdate = {
            name: "DSResponseTemplate",
            settings: [
                { name: "DSResponseTemplatePreferredLanguageRestrict", value: this.limitResponseTemplate },
            ],
        };
        this.dsarSettingsService.updateSettingsByGroup(responseTemplateUpdateBody)
            .then((response: IProtocolResponse<void>): boolean => {
                return response.result;
        });
    }
}
