// Angular
import {
    Component,
    EventEmitter,
    Input,
    Output,
    SecurityContext,
} from "@angular/core";
import { DomSanitizer } from "@angular/platform-browser";

// Interfaces
import { IResponseTemplate } from "dsarModule/interfaces/response-templates.interface";
import {
    IAction,
} from "interfaces/redux.interface";
import { IDropdownOption } from "interfaces/one-dropdown.interface";
import { IUserMap } from "interfaces/user-reducer.interface";

// Constants
import { Actions } from "dsarModule/constants/response-templates.constants";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

// Services
import { Permissions } from "modules/shared/services/helper/permissions.service";

@Component({
    selector: "dsar-response-templates-table",
    templateUrl: "./dsar-response-templates-table.component.html",
})
export class DsarResponseTemplatesTable {

    @Input() rows;
    @Input() users: IUserMap;
    @Output() deleteResponseTemplate = new EventEmitter<IAction>();
    @Output() editResponseTemplate = new EventEmitter<IAction>();

    columns;
    menuClass: string;
    sortOrder: string;
    sortedKey: string | number;
    editTemplate: boolean;
    ellipsisOptions: IDropdownOption[];

    constructor(
        private readonly permissions: Permissions,
        private readonly translatePipe: TranslatePipe,
        private readonly sanitizer: DomSanitizer,
    ) {}

    ngOnInit() {
        this.editTemplate = this.permissions.canShow("DSARSettingResponseTemplatesEdit");
        this.columns = [
            { sortKey: 1, columnType: "name", columnName: this.translatePipe.transform("Name"), cellValueKey: "templateName" },
            { sortKey: 2, columnType: null, columnName: this.translatePipe.transform("Preview"), cellValueKey: "templateContent" },
            { sortKey: 3, columnType: "date", columnName: this.translatePipe.transform("DateModified"), cellValueKey: "modifiedDate" },
            { sortKey: 4, columnType: "user", columnName: this.translatePipe.transform("LastModifiedBy"), cellValueKey: "lastModifiedBy" },
            { sortKey: 5, columnType: "action", columnName: "Test", cellValueKey: "status" },
        ];
        this.getActions();
    }

    handleNameClick(row: IResponseTemplate) {
        this.editResponseTemplate.emit({ type: Actions.EDIT_TEMPLATE, payload: row });
    }

    getActions() {
        this.ellipsisOptions = [];
        if (this.editTemplate) {
            this.ellipsisOptions.push({
                text: this.translatePipe.transform("Edit"),
                action: (row: IResponseTemplate) => { this.editResponseTemplate.emit({ type: Actions.EDIT_TEMPLATE, payload: row }); },
            });
            this.ellipsisOptions.push({
                text: this.translatePipe.transform("Delete"),
                action: (row: IResponseTemplate) => { this.deleteResponseTemplate.emit({ type: Actions.DELETE_TEMPLATE, payload: row }); },
            });
        }
        if (!this.ellipsisOptions.length) {
            this.ellipsisOptions.push({
                text: this.translatePipe.transform("NoActionsAvailable"),
                action: () => { }, // Keep Empty
            });
        }
    }

    removeHtmlTags(htmlStr: string): string {
        const textArea = document.createElement("textarea");
        textArea.innerHTML = this.sanitizer.sanitize(SecurityContext.HTML, htmlStr.replace(/(<([^>]+)>)/ig, ""));
        return textArea.value;
    }
}
