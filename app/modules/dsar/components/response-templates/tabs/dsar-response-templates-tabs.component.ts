// Angular
import {
    Component,
    OnInit,
} from "@angular/core";
import { IPromise } from "angular";

// Services
import { ModalService } from "sharedServices/modal.service";
import { WebformApiService } from "dsarservice/webform-api.service";
import { Permissions } from "modules/shared/services/helper/permissions.service";

// Interfaces
import {
    ISetLanguageModalData,
    ISetLanguagePutData,
    IGetWebformLanguageResponse,
} from "interfaces/dsar/dsar-edit-language.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";

// Constants
import {
    Tabs,
    DefaultResponseTemplateId,
 } from "dsarModule/constants/response-templates.constants";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

interface IResponseTemplateTabs {
    id: string;
    textKey: string;
    action: (option_selected: IResponseTemplateTabs) => void;
}

@Component({
    selector: "dsar-response-templates-tabs",
    templateUrl: "./dsar-response-templates-tabs.component.html",
})
export class ResponseTemplateTabs implements OnInit {
    readonly Tabs = Tabs;
    tabOptions: IResponseTemplateTabs[] = [
        {
            id: Tabs.Templates,
            textKey: "Templates",
            action: (option_selected: IResponseTemplateTabs): void => {
                this.changeTab(option_selected);
            },
        },
    ];
    selectedTab = this.tabOptions[0];

    addNewModal = false;
    hideAddButton = false;
    languageList: IGetWebformLanguageResponse[] = [];

    constructor(
        private readonly modalService: ModalService,
        private dsarWebFormService: WebformApiService,
        private readonly permissions: Permissions,
        private readonly translatePipe: TranslatePipe,
    ) {}

    ngOnInit() {
        if (this.permissions.canShow("DSARResponseTemplateLanguageSupport")) {
            this.tabOptions.push(
                {
                    id: Tabs.Settings,
                    textKey: "Settings",
                    action: (option_selected: IResponseTemplateTabs): void => {
                        this.changeTab(option_selected);
                    },
                },
            );
        }
    }

    openAddNewModal() {
        this.addNewModal = !this.addNewModal;
    }

    changeTab(tab: IResponseTemplateTabs): void {
        this.selectedTab = tab;
        this.hideAddButton = this.selectedTab.id === this.Tabs.Settings;
    }

    launchLanguagePicker() {
        const setLanguageData: ISetLanguageModalData = {
            masterLanguagesList: null,
            webformLanguagesList: null,
            templateId: DefaultResponseTemplateId,
            headerTitle: this.translatePipe.transform("TranslatedLanguages"),
            bodyText: this.translatePipe.transform("TranslatedLanguagesBodyText"),
            promiseToResolve: this.onLanguageSave.bind(this),
            promiseArray: () => {
                return Promise.all([
                    this.dsarWebFormService.getSelectedLanguages(DefaultResponseTemplateId),
                    this.dsarWebFormService.getAllLanguages(),
                ]);
            },
        };
        this.modalService.setModalData(setLanguageData);
        this.modalService.openModal("webformEditLanguageBody");
    }

    private onLanguageSave(changes: ISetLanguagePutData[]): IPromise<boolean> {
        return this.dsarWebFormService.putLanguagePreferencesData(changes, DefaultResponseTemplateId).
            then((response: IProtocolResponse<IGetWebformLanguageResponse[]>) => {
                this.languageList = [...response.data];
                return response.result;
        });
    }
}
