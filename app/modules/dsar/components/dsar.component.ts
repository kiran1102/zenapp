// Services
import { GlobalSidebarService } from "modules/global-sidebar/services/one-global-sidebar.service";
import { Permissions } from "modules/shared/services/helper/permissions.service";
import { StateService } from "@uirouter/core";
import { Wootric } from "sharedModules/services/provider/wootric.service";

// Interfaces
import { INavMenuItem } from "modules/global-sidebar/interfaces/nav-menu-item.interface";
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";

// Constants
import { WootricModuleMap } from "modules/shared/constants/wootric.constants";

class DSARWrapperController implements ng.IComponentController {

    static $inject: string[] = [
        "$rootScope",
        "Permissions",
        "GlobalSidebarService",
        "Wootric",
    ];

    constructor(
        private $rootScope: IExtendedRootScopeService,
        private permissions: Permissions,
        private globalSidebarService: GlobalSidebarService,
        private wootric: Wootric,
    ) {}

    $onInit() {
        this.globalSidebarService.set(this.getMenuRoutes(), this.$rootScope.t("DataSubjectRequests"));
        this.wootric.run(WootricModuleMap.DataSubjectRequets);
    }

    private getMenuRoutes(): INavMenuItem[] {
        const menuGroup: INavMenuItem[] = [];

        if (this.permissions.canShow("DSARDashboard")) {
            menuGroup.push({
                id: "DSARDashboardSidebarItem",
                title: this.$rootScope.t("Dashboard"),
                route: "zen.app.pia.module.dsar.views.dashboard",
                activeRoute: "zen.app.pia.module.dsar.views.dashboard",
                icon: "ot ot-dashboard",
            });
        }
        if (this.permissions.canShow("DSARWebForms")) {
            menuGroup.push({
                id: "DSARWebFormsSidebarItem",
                title: this.$rootScope.t("WebForms"),
                route: "zen.app.pia.module.dsar.views.webform.list",
                activeRoute: "zen.app.pia.module.dsar.views.webform",
                icon: "ot ot-webform",
            });
        }
        if (this.permissions.canShow("DSARRequestQueue")) {
            menuGroup.push({
                id: "DSARRequestQueueSidebarItem",
                title: this.$rootScope.t("Requests"),
                route: "zen.app.pia.module.dsar.views.queue",
                activeRouteFn: (stateService: StateService) =>
                    stateService.includes("zen.app.pia.module.dsar.views.queue")
                    || stateService.includes("zen.app.pia.module.dsar.views.queue-details"),
                icon: "fa fa-ticket",
            });
        }
        if (this.permissions.canShow("DSARCustomWorkflows")) {
            menuGroup.push({
                id: "DSARWorkflowsSidebarItem",
                title: this.$rootScope.t("Workflows"),
                route: "zen.app.pia.module.dsar.views.workflows",
                activeRouteFn: (stateService: StateService) =>
                    stateService.includes("zen.app.pia.module.dsar.views.workflows")
                    || stateService.includes("zen.app.pia.module.dsar.views.workflow-details"),
                icon: "ot ot-workflow",
            });
        }
        if (this.permissions.canShow("DSARResponseTemplate")) {
            menuGroup.push({
                id: "DSARResponseTemplatesSidebarItem",
                title: this.$rootScope.t("ResponseTemplates"),
                route: "zen.app.pia.module.dsar.views.responseTemplates",
                activeRoute: "zen.app.pia.module.dsar.views.responseTemplates",
                icon: "ot ot-response-template",
            });
        }
        if (this.permissions.canShow("DSARSettings")) {
            menuGroup.push({
                id: "DSARSettingsSidebarItem",
                title: this.$rootScope.t("Settings"),
                route: "zen.app.pia.module.dsar.views.settings",
                activeRoute: "zen.app.pia.module.dsar.views.settings",
                icon: "ot ot-cog",
            });
        }
        if (this.permissions.canShow("DSAREmailTemplates__")) {
            menuGroup.push({
                id: "DSAREmailTemplatesSidebarItem",
                title: this.$rootScope.t("EmailTemplates"),
                route: "zen.app.pia.module.dsar.views.email_templates",
                activeRoute: "zen.app.pia.module.dsar.views.email_templates",
                icon: "ot ot-templates",
            });
        }

        return menuGroup;
    }

}

const DSARWrapperComponent: ng.IComponentOptions = {
    controller: DSARWrapperController,
    template: `
        <div class="height-100 flex-full-width flex-full-height dsar">
            <div class="dsar-content height-100">
                <section ui-view='dsar_body' class="dsar-body show width-100-percent height-100"></section>
            </div>
        </div>
    `,
};

export default DSARWrapperComponent;
