export const RequestQueueStatusTags = {
    Verify: "Verify",
    Assign: "Assign",
    Extend: "Extend",
    Suspend: "Suspend",
    Complete: "Complete",
    Comment: "Comment",
    Reject: "Reject",
    TaskComment: "TaskComment",
    TaskResponse: "TaskResponse",
    TaskComplete: "TaskComplete",
    Create: "Create",
};
