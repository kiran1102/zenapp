import { FilterValueTypes } from "modules/filter/enums/filter.enum";

export const SidebarDrawerEvents = {
    FieldChanged: "FIELD_CHANGED",
    OperatorChanged: "OPERATOR_CHANGED",
    ValueChanged: "VALUE_CHANGED",
    FilterUpdated: "FILTER_UPDATED",
    ResetFields: "RESET_FIELDS",
    EnableApply: "ENABLE_APPLY",
};

export const FilterOperators = {
    Prepopulated: 30,
    MultiSelect: 50,
    Select: 70,
    Date: 60,
    DateRange: 65,
    Number: 80,
};

export const OperatorValues = {
    EqualTo: "EQ",
    IsBetween: "BW",
    Is: "EQ",
    IsBefore: "LT",
    IsAfter: "GT",
    NotEqualTo: "NE",
    GreaterThan: "GT",
    LessThan: "LT",
};

export const FilterLabels = {
    Approver: "Approver",
    Organization: "Organization",
    Country: "Country",
    Deadline: "Deadline",
    DateCreated: "Date Created",
    Stage: "Stage",
    RequestType: "RequestType",
    SubjectType: "SubjectType",
    RemainingSubtasks: "RemainingSubTasks",
};

export enum FilterOperatorLabels {
    EqualTo = "EqualTo",
    NotEqualTo = "NotEqualTo",
    Is = "Is",
    IsAfter = "IsAfter",
    IsBefore = "IsBefore",
    IsBetween = "IsBetween",
    GreaterThan = "GreaterThan",
    LessThan = "LessThan",
}

export const DSARFilterValueTypes = {
    [FilterOperators.MultiSelect]: FilterValueTypes.MultiSelect,
    [FilterOperators.Prepopulated]: FilterValueTypes.MultiSelect,
    [FilterOperators.Number]: FilterValueTypes.Number,
    [FilterOperators.Select]: FilterValueTypes.SingleSelect,
    [FilterOperators.Date]: FilterValueTypes.Date,
    [FilterOperators.DateRange]: FilterValueTypes.DateRange,
};

export const OperatorValuesInverse = {
    EQ: "EqualTo",
    BW: "IsBetween",
    LT: "IsBefore",
    GT: "IsAfter",
    NE: "NotEqualTo",
};
