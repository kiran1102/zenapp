export const CostCalc =  {
    HoursWorked: "HoursWorked",
    TotalCost: "TotalCost",
};

export const RequestQueue =  {
    PreferredLanguage: "PreferredLanguage",
    Approver: "Approver",
    Deadline: "Deadline",
};

export const DSARSettingsOptions = {
    General: "general",
    Workflows: "workflows",
    AutomationRules: "automation-rules",
};
