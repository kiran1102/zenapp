export const WebformControls = {
    FirstName: "firstName",
    LastName: "lastName",
    Email: "email",
    RequestTypes: "requestTypes",
    SubjectTypes: "subjectTypes",
    Recaptcha: "recaptcha",
    Language: "language",
    RequestDetails: "requestDetails",
};

export const InputType = {
    TextField: "Text Field",
    Select: "Select",
    TextArea: "Text Area",
    Multiselect: "Multiselect",
};

export const DisplayType = {
    Button: "Button",
    Multiselect: "Multiselect",
};
