export const DSARSettings = {
    EnableReCaptcha: "EnableReCaptcha",
    AttachmentSubmission: "AttachmentSubmission",
    Organization: "Organization",
    DefaultDaysToRespond: "DefaultDaysToRespond",
    DefaultReminder: "DefaultReminder",
    DefaultReviewer: "DefaultReviewer",
    BccEmail: "BccEmail",
    TranslateWebForm: "TranslateWebForm",
    AllowDsPortalAttachment: "AllowDsPortalAttachment",
    EnableSubjectTypes: "EnableSubjectTypes",
    EnableRequestTypes: "EnableRequestTypes",
    IsLogoEnabled: "ShowOneTrustLogo",
    OrgGroupId: "orgGroupId",
    Workflows: "Workflows",
    MultiCaptcha: "MultiCaptcha",
    EmailVerification: "EmailVerification",
    ExpiryRequest: "ExpiryRequest",
    ExpiryRequestDays: "ExpiryRequestDays",
    WebFormName: "WebFormName",
    RequireAttachment: "RequireAttachment",
    TemplateName: "TemplateName",
};

export const DSARStyles = {
    HeaderLogo: "HeaderLogo",
    HeaderHeight: "HeaderHeight",
    HeaderColor: "HeaderColor",
    WelcomeTextColor: "WelcomeTextColor",
    WelcomeTextSize: "WelcomeTextSize",
    FormLabelColor: "FormLabelColor",
    ActiveButtonColor: "ActiveButtonColor",
    ActiveButtonTextColor: "ActiveButtonTextColor",
    AttachmentDescriptionTextColor: "AttachmentDescriptionTextColor",
    AttachmentDescriptionTextSize: "AttachmentDescriptionTextSize",
    AttachmentButtonTextColor: "AttachmentButtonTextColor",
    AttachmentButtonTextSize: "AttachmentButtonTextSize",
    FooterTextColor: "FooterTextColor",
    FooterTextSize: "FooterTextSize",
};

export const DSARCaptchaType = {
    BotdetectCaptcha: "botdetectCaptcha",
    GoogleReCaptcha: "googleReCaptcha",
};

export const DSARAttachment = {
    AttachmentButtonText: "AttachmentButtonText",
    AttachmentInfoDesc: "AttachmentInfoDesc",
    AttachmentSizeDesc: "FilesLargerThanSizeNotSupported",
    SubmitButtonText: "SubmitButtonText",
};

export const DSARModalType =  {
    WELCOME: "WELCOME",
    FOOTER: "FOOTER",
    THANKYOU: "THANKYOU",
};

export const DSARSettingsType = {
    TOGGLE: "TOGGLE",
    NUMBER: "NUMBER",
};

export const DefaultWorkflowID = "00000000-0000-0000-0000-222222222222";
export const CCPAWorkFlowID = "00000000-0000-0000-0000-999999999999";
export const LatestVersionWorkFlowID = "00000000-0000-0000-0000-444444444444";
