export const RequestQueueActions = {
    CHANGE_PAGE: "CHANGE_PAGE",
    CHANGE_WORKFLOW: "CHANGE_WORKFLOW",
    SORT_QUEUE: "SORT_QUEUE",
    SEARCH_QUEUE: "SEARCH_QUEUE",
    UPDATE_SINGLE_ROW: "UPDATE_SINGLE_ROW",
    EXTEND_REQUEST: "EXTEND_REQUEST",
    REJECT_REQUEST: "REJECT_REQUEST",
    ASSIGN_REQUEST: "ASSIGN_REQUEST",
    DELETE_REQUEST: "DELETE_REQUEST",
    REVOKE_ACCESS: "REVOKE_ACCESS",
    CLOSE_REQUEST: "CLOSE_REQUEST",
    GRANT_ACCESS: "GRANT_ACCESS",
    EXPORT: "EXPORT",
    UPDATE_REQUEST_ENTRIES: "UPDATE_REQUEST_ENTRIES",
    GO_TO_DETAIL_PAGE: "GO_TO_DETAIL_PAGE",
    TOGGLE_DRAWER: "TOGGLE_DRAWER",
    USING_FILTER_DATA: "USING_FILTER_DATA",
    APPLY_FILTER: "APPLY_FILTER",
    BACK_TO_QUEUE: "BACK_TO_QUEUE",
    COMMENT: "COMMENT",
    DS_LANGUAGE_CHANGE: "DS_LANGUAGE_CHANGE",
    RESP_AS_COMPLETED: "RESP_AS_COMPLETED",
    ADD_ATTACHMENTS: "ADD_ATTACHMENTS",
    DOWNLOAD_FILE: "DOWNLOAD_FILE",
    DELETE_FILE_FROM_COMMENT: "DELETE_FILE_FROM_COMMENT",
    DELETE_FILE: "DELETE_FILE",
    OPEN_COLUMN_SELECTOR: "OPEN_COLUMN_SELECTOR",
    UPDATE_REQUEST: "UPDATE_REQUEST",
    MFA_PASSWORD: "MFA_PASSWORD",
    UPDATE_VIEW: "UPDATE_VIEW",
    SEARCH: "SEARCH",
    EDIT_SUB_TASK: "EDIT_SUB_TASK",
    DELETE_SUB_TASK: "DELETE_SUB_TASK",
    OPEN_SUB_TASK: "OPEN_SUB_TASK",
    SHOW_CLOSED_SUBTASK_MODAL: "SHOW_CLOSED_SUBTASK_MODAL",
};

export const RequestQueueTableView = {
    Filter: "Filter",
    Search: "Search",
    Table: "Table",
};

export const RequestQueueSidebar = {
    PreferredLanguage: "PreferredLanguage",
    Approver: "Approver",
    Deadline: "Deadline",
    ManagingOrganization: "ManagingOrganization",
    DateOpened: "DateOpened",
    Extended: "Extended",
    Resolution: "Resolution",
};

export const CostCalc = {
    HoursWorked: "HoursWorked",
    TotalCost: "TotalCost",
};

export const RequestQueueHeader = {
    RequestId: "RequestId",
    RequestTypes: "RequestTypes",
    SubjectTypes: "SubjectTypes",
    Email: "Email",
};

export const RequestQueueViewTypes = {
    RequestQueueGrid: "requestqueuelist",
    SubtaskGrid: "subtasklist",
};
