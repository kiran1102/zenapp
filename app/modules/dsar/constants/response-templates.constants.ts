export const Actions = {
        ADD_NEW_TEMPLATE: "ADD_NEW_TEMPLATE",
        CHANGE_PAGE: "CHANGE_PAGE",
        EDIT_TEMPLATE: "EDIT_TEMPLATE",
        DELETE_TEMPLATE: "DELETE_TEMPLATE",
        SEARCH_KEYPRESS: "SEARCH_KEYPRESS",
        SEARCH: "SEARCH",
};

export const DefaultResponseTemplateId = "00000000-0000-0000-0000-111111111111";

export const Tabs = {
    Settings: "settings",
    Templates: "templates",
};
