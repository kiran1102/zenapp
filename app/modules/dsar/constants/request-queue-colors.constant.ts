import { IStringMap } from "interfaces/generic.interface";

export const RequestQueueColors: IStringMap<string> = {
    Assigned: "#008acc",
    Unassigned: "#ffa500",
    Complete: "#6cc04a",
    New: "#ffa500",
    Rejected: "#be2f2b",
    Closed: "#be2f2b",
};
