// Angular
import {
    Injectable,
    Inject,
} from "@angular/core";

// Services
import { ProtocolService } from "modules/core/services/protocol.service";
import { NotificationService } from "modules/shared/services/provider/notification.service";
import { TenantTranslationService } from "sharedServices/tenant-translation.service";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

// Interfaces
import {
    IProtocolConfig,
    IProtocolMessages,
    IProtocolResponse,
} from "interfaces/protocol.interface";
import { IRequestQueueParams } from "dsarModule/interfaces/request-queue.interface";
import {
    IGetWorkflowsList,
} from "dsarModule/interfaces/dsar-workflows-list.interface";
import {
    IWorkflow,
    ICreateWorkflow,
    IPublishedWorkflow,
    IEditTitle,
} from "interfaces/workflows.interface";

// Rxjs
import { from, Observable } from "rxjs";

@Injectable()
export class DSARWorkflowService {

    private defaultParams: IRequestQueueParams = {
        page: 0,
        size: 20,
        sort: "requestQueueRefId,DESC",
    };

    constructor(
        private readonly OneProtocol: ProtocolService,
        @Inject("TenantTranslationService") private readonly tenantTranslationService: TenantTranslationService,
        private notificationService: NotificationService,
        private readonly translatePipe: TranslatePipe,
    ) {}

    public getAllWorkflows(params: IRequestQueueParams = this.defaultParams): ng.IPromise<IProtocolResponse<IGetWorkflowsList>> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("GET", `/api/datasubject/v1/workflow/`, params);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("ErrorGettingWorkflows")} };
        return this.OneProtocol.http(config, messages).then((response: IProtocolResponse<IGetWorkflowsList>): IProtocolResponse<IGetWorkflowsList> => {
            if (response.result) {
                return response;
            }
        });
    }

    public createNewWorkflow(workflow: ICreateWorkflow): ng.IPromise<IProtocolResponse<IWorkflow>> {
        workflow.language = this.tenantTranslationService.getCurrentLanguage();
        const params: ICreateWorkflow = {...workflow};
        params.referenceId = null;
        const config: IProtocolConfig = this.OneProtocol.customConfig("POST", `/api/datasubject/v1/workflow/${workflow.referenceId}`, null, params);
        const messages: IProtocolMessages = {
            Success: { custom: this.translatePipe.transform("WorkflowCreatedSuccessfully")},
            Error: { custom: this.translatePipe.transform("ErrorCreatingWorkflow") },
        };
        return this.OneProtocol.http(config, messages).then((response: IProtocolResponse<IWorkflow>): IProtocolResponse<IWorkflow> => {
            if (response.result) {
                return response;
            }
        });
    }

    public editWorkflow(workflow: ICreateWorkflow): ng.IPromise<IProtocolResponse<IWorkflow>> {
        workflow.language = this.tenantTranslationService.getCurrentLanguage();
        const params: ICreateWorkflow = {...workflow};
        params.name = null;
        const config: IProtocolConfig = this.OneProtocol.customConfig("PUT", `/api/datasubject/v1/workflow/${workflow.referenceId}`, null, params);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("ErrorUpdatingWorkflows") } };
        return this.OneProtocol.http(config, messages);
    }

    public deleteWorkFlow(workflowId: string): Observable<IProtocolResponse<IWorkflow>> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("DELETE", `/api/datasubject/v1/workflow/${workflowId}`);
        const messages: IProtocolMessages = {
            Error: { custom: this.translatePipe.transform("ErrorDeletingWorkflows") } ,
            Success: { custom: this.translatePipe.transform("SuccessfullyDeletedWorkflow")},
        };
        return from(this.OneProtocol.http(config, messages));
    }

    public getPublishedWorkflows(params?: {mfaSetting: string}): ng.IPromise<IProtocolResponse<IPublishedWorkflow[]>> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("GET", `/api/datasubject/v1/workflow/activated`, params);
        const messages: IProtocolMessages = {
            Error: { Hide: true},
        };
        return this.OneProtocol.http(config, messages).then((response: IProtocolResponse<IPublishedWorkflow[]>): IProtocolResponse<IPublishedWorkflow[]> => {
            if (response.result) {
                return response;
            } else if (response.errors) {
                let errorKey = "ErrorGettingPublishedWorkflows";
                if (response.errors.code === "ERROR_DATASUBJECT-ACCESS_NOMFADISABLEDWORKFLOWS") {
                    errorKey = "NoMFADisabledWorkflows";
                } else if (response.errors.code === "ERROR_DATASUBJECT-ACCESS_NOMFAENABLEDWORKFLOWS") {
                    errorKey = "NoMFAEnabledWorkflows";
                }
                this.notificationService.alertError(this.translatePipe.transform("Error"), this.translatePipe.transform(errorKey));
                return response;
            }
        });
    }

    public editTitle(workflow: IEditTitle): ng.IPromise<IProtocolResponse<string>> {
        if (!workflow.workflowName) {
            this.notificationService.alertError(this.translatePipe.transform("Error"), this.translatePipe.transform("EmptyWorkflow"));
            return new Promise(() => {
                    const res: IProtocolResponse<string> = {
                        data: null,
                        result: false,
                    };
                    return res;
                },
            ) as any;
        } else {
            const config: IProtocolConfig = this.OneProtocol.customConfig("PUT", `/api/datasubject/v1/workflow/${workflow.workflowReferenceId}/name`, null, workflow );
            const messages: IProtocolMessages = {
                Success: { custom: this.translatePipe.transform("WorkflowNameUpdatedSuccessfully")},
                Error: { Hide: true},
            };
            return this.OneProtocol.http(config, messages).then((response: IProtocolResponse<string>): IProtocolResponse<string> => {
                if (response.result) {
                    return response;
                } else if (response.data === "DuplicateWorkflow") {
                    this.notificationService.alertError(this.translatePipe.transform("Error"), this.translatePipe.transform("DuplicateWorkflow"));
                    return response;
                } else if (workflow.workflowName.length > 150) {
                    this.notificationService.alertError(this.translatePipe.transform("Error"), this.translatePipe.transform("WorkflowNameLengthLimit", { Length: "150" }));
                    return response;
                } else {
                    this.notificationService.alertError(this.translatePipe.transform("Error"), this.translatePipe.transform("ErrorUpdatingWorkflowName"));
                    return response;
                }
            });
        }
    }
}
