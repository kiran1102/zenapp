import { IKeyValue } from "interfaces/generic.interface";
import { ProtocolService } from "modules/core/services/protocol.service";
import { IProtocolResponse, IProtocolConfig, IProtocolMessages} from "interfaces/protocol.interface";
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import { IRequestQueueParams } from "dsarModule/interfaces/request-queue.interface";
import { ICurrencyList } from "dsarModule/interfaces/dsar-settings.interface";
import { IDSARSettingsGroup } from "dsarModule/interfaces/dsar-settings-group.interface";
import { IGetAllLanguageResponse } from "interfaces/dsar/dsar-edit-language.interface";

import { IResponseTemplate } from "dsarModule/interfaces/response-templates.interface";
import { IPageableOf } from "interfaces/pagination.interface";

export class DSARSettingsService {
    static $inject: string[] = ["$rootScope", "OneProtocol"];
    public workFlowSettingsOutput: Array<IKeyValue<boolean>> = [];
    public stageSettingsOutput: Array<IKeyValue<boolean>> = [];
    public draftMode: boolean;
    public isDefaultWorkFlow: boolean;

    constructor(
        readonly $rootScope: IExtendedRootScopeService,
        readonly OneProtocol: ProtocolService,
    ) {}

    public createResponseTemplate(template: IResponseTemplate): ng.IPromise<IProtocolResponse<void>> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("POST", "/api/datasubject/v1/responsetemplates", {}, template);
        const messages: IProtocolMessages = { Error: { custom: this.$rootScope.t("ErrorCreatingResponseTemplate") } };
        return this.OneProtocol.http(config, messages);
    }

    public editResponseTemplate(template: IResponseTemplate): ng.IPromise<IProtocolResponse<void>> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("PUT", "/api/datasubject/v1/responsetemplates", {}, template);
        const messages: IProtocolMessages = { Error: { custom: this.$rootScope.t("ErrorUpdatingResponseTemplate") } };
        return this.OneProtocol.http(config, messages);
    }

    public getResponseTemplateById(templateId: string): ng.IPromise<IProtocolResponse<IResponseTemplate>> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("GET", `/api/datasubject/v1/responsetemplates/${templateId}`);
        const messages: IProtocolMessages = { Error: { custom: this.$rootScope.t("ErrorGettingResponseTemplate") } };
        return this.OneProtocol.http(config, messages);
    }

    public getAllEnabledResponseTemplates(languageCode: string, page: number, filter?: string): ng.IPromise<IProtocolResponse<IPageableOf<IResponseTemplate>>> {
        const params = {};
        params["page"] = page;
        if (languageCode) {
            params["language"] = languageCode;
        }
        if (filter) {
            params["filter"] = filter;
        }
        const config: IProtocolConfig = this.OneProtocol.customConfig("GET", `/api/datasubject/v1/responsetemplates`, params);
        const messages: IProtocolMessages = { Error: { custom: this.$rootScope.t("ErrorGettingResponseTemplate") } };
        return this.OneProtocol.http(config, messages);
    }

    public deleteResponseTemplate(templateId: string): ng.IPromise<IProtocolResponse<void>> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("DELETE", `/api/datasubject/v1/responsetemplates/${templateId}`);
        const messages: IProtocolMessages = { Error: { custom: this.$rootScope.t("ErrorDeletingFile") } };
        return this.OneProtocol.http(config, messages);
    }

    public getCurrencyList(): ng.IPromise<IProtocolResponse<ICurrencyList[]>> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("GET", "/api/datasubject/v1/currencies");
        const messages: IProtocolMessages = { Error: { custom: this.$rootScope.t("ErrorGettingCurrencyList") } };
        return this.OneProtocol.http(config, messages);
    }

    public getSettingsByGroup(groupName: string): ng.IPromise<IProtocolResponse<IDSARSettingsGroup>> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("GET", `/api/datasubject/v1/commonsettings/groups/${groupName}`);
        const messages: IProtocolMessages = { Error: { custom: this.$rootScope.t("ErrorGettingDSARSettings") } };
        return this.OneProtocol.http(config, messages);
    }

    public updateSettingsByGroup(settingsGroup: any): ng.IPromise<IProtocolResponse<void>> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("POST", `/api/datasubject/v1/commonsettings/groups`, {}, settingsGroup);
        const messages: IProtocolMessages = { Error: { custom: this.$rootScope.t("ErrorUpdatingDSARSettings") } };
        return this.OneProtocol.http(config, messages);
    }

    public getAllLanguages(): ng.IPromise<IProtocolResponse<IGetAllLanguageResponse[]>> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("GET", `/api/globalization/v1/languages/`, { includeUntranslated: false});
        const messages: IProtocolMessages = { Error: { custom: this.$rootScope.t("AllLanguages")} };
        return this.OneProtocol.http(config, messages);
    }

    public getAllEnabledResponseTemplatesByRequestQueue(requestQueueId: string): ng.IPromise<IProtocolResponse<IResponseTemplate[]>> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("GET", `/api/datasubject/v1/responsetemplates?requestQueueId=${requestQueueId}`);
        const messages: IProtocolMessages = { Error: { custom: this.$rootScope.t("ErrorGettingResponseTemplate") } };
        return this.OneProtocol.http(config, messages);
    }

    public workFlowSettingsOutputMethod(): Array<IKeyValue<boolean>> {
        return this.workFlowSettingsOutput;
    }

    public stageSettingsOutputMethod(): Array<IKeyValue<boolean>> {
        return this.stageSettingsOutput;
    }
}
