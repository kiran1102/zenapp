// Rxjs
import {
    BehaviorSubject,
    Observable,
} from "rxjs";

// 3rd party
import { Injectable } from "@angular/core";

// Services
import { RequestQueueService } from "dsarservice/request-queue.service";

// Interfaces
import { IProtocolResponse } from "interfaces/protocol.interface";
import { IGetPageableAudit } from "dsarModule/interfaces/audit-history.interface";

@Injectable()
export class RequestQueueHistoryService {
    defaultResponse: IProtocolResponse<IGetPageableAudit> = {
        data: {
            content: [],
            first: null,
            last: null,
            number: null,
            numberOfElements: null,
            size: null,
            totalElements: null,
            totalPages: null,
        },
        result: false,
    };
    public contentObservable: Observable<IProtocolResponse<IGetPageableAudit>>;
    private contentSource = new BehaviorSubject({...this.defaultResponse});
    constructor(
        readonly requestQueueService: RequestQueueService,
    ) {
        this.contentObservable = this.contentSource.asObservable();
    }

    resetHistory(): void {
        this.contentSource.next({...this.defaultResponse});
    }

    updateHistory(requestQueueId: string, page: number = 0, size: number = 8): void {
        this.requestQueueService.getHistory(requestQueueId, page, size).then((res: IProtocolResponse<IGetPageableAudit>): void => {
            this.contentSource.next(res);
        });
    }
}
