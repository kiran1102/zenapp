// Angular
import { Injectable } from "@angular/core";

// Interfaces
import {
    ISavedView,
    ITableColumns,
    IRequestQueueParams,
} from "dsarModule/interfaces/request-queue.interface";
import { IFilterGetData } from "dsarModule/interfaces/dsar-sidebar-drawer.interface";
import { IUser } from "interfaces/user.interface";

@Injectable()
export class RequestQueueCacheService {

    set currentView(view: ISavedView) {
        sessionStorage.setItem("DSARCurrentView", JSON.stringify(view));
    }

    get currentView(): ISavedView {
        return JSON.parse(sessionStorage.getItem("DSARCurrentView"));
    }
    _currentUser: IUser;
    set currentUser(user: IUser) {
        this._currentUser = user;
    }
    get currentUser(): IUser {
        return this._currentUser;
    }

    private _viewsMap: Map<string, ISavedView> = new Map();

    initializeViewMap() {
        this._viewsMap = new Map();
        const views = JSON.parse(sessionStorage.getItem("DSARViews"));
        if (views) {
            this._viewsMap = new Map(views);
        }
    }

    addView(view: ISavedView) {
        if (!this._viewsMap.has(view.id)) {
            this.updateView(view);
        }
    }

    updateView(view: ISavedView) {
        this._viewsMap.set(view.id, view);
        this.storeView();
    }

    storeView() {
        sessionStorage.setItem("DSARViews", JSON.stringify(this._viewsMap));
    }

    updateFilters(filters: IFilterGetData[]) {
        const view = this._viewsMap.get(this.currentView.id);
        const data = JSON.parse(view.data);
        data.filterConditions = filters;
        view.data = JSON.stringify(data);
        this.updateView(view);
    }

    getFilters(): IFilterGetData[] {
        const view = this._viewsMap.get(this.currentView.id);
        if (view.data) {
            const data = JSON.parse(view.data);
            if (data) {
                return data.filterConditions;
            }
        }
        return null;
    }

    updateSearchString(search: string) {
        const view = this._viewsMap.get(this.currentView.id);
        const data = JSON.parse(view.data);
        data.searchString = search;
        view.data = JSON.stringify(data);
        this.updateView(view);
    }

    getSearchString(): string {
        const view = this._viewsMap.get(this.currentView.id);
        if (view.data) {
            const data = JSON.parse(view.data);
            if (data) {
                return data.searchString;
            }
        }
        return "";
    }

    updateColumns(columns: ITableColumns[]) {
        const view = this._viewsMap.get(this.currentView.id);
        const data = JSON.parse(view.data);
        data.columnSelections = columns;
        view.data = JSON.stringify(data);
        this.updateView(view);
    }

    getColumns(): ITableColumns[] {
        const view = this._viewsMap.get(this.currentView.id);
        if (view.data) {
            const data = JSON.parse(view.data);
            if (data) {
                return data.columnSelections;
            }
        }
        return [];
    }

    updatePageParams(params: IRequestQueueParams) {
        const view = this._viewsMap.get(this.currentView.id);
        view.params = params;
        this.updateView(view);
    }

    getPageParams(): IRequestQueueParams {
        const view = this._viewsMap.get(this.currentView.id);
        if (view.params) {
            return view.params;
        }
        return null;
    }

}
