// Angular
import {
    Injectable,
    Inject,
} from "@angular/core";

import { IStringMap } from "interfaces/generic.interface";
import { RequestQueueStatus } from "dsarModule/enums/request-queue-status.enum";
import { SubTaskStatus, SubTaskType } from "dsarModule/enums/sub-task.enum";
import * as _ from "lodash";
import OrgGroupStoreNew from "oneServices/org-group.store";
import { Permissions } from "modules/shared/services/helper/permissions.service";
import { IOrganization } from "interfaces/organization.interface";
import {
    IRequestQueueDetails,
    IMultiselectFields,
} from "dsarModule/interfaces/request-queue.interface";
import { IRequestQueueComment } from "dsarModule/interfaces/request-queue-comment.interface";
import { RequestQueueStatusTags } from "dsarModule/constants/request-queue-status-tags.constant";
import { IAttachment } from "interfaces/attachment.interface";
import { IAttachmentFileResponse } from "interfaces/attachment-file.interface";
import {
    ISubTask,
    IStatusLabel,
    ITypeLabel,
    ISubTaskRowTable,
} from "interfaces/sub-tasks.interface";
import moment from "moment";
import "moment-timezone";
import { IWorkflowItem } from "interfaces/workflows.interface";
import { RequestQueueColors } from "dsarModule/constants/request-queue-colors.constant";
import {
    find,
    each,
 } from "lodash";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

@Injectable()
export class RequestQueueHelper {

    private RequestQueueStatusIdMap: IStringMap<IStatusLabel>; // see getRequestStatusIdMap() to see the object defined
    private showOnlyExternalComments: boolean;
    private showOnlyInternalComments: boolean;
    private hideAllComments: boolean;
    private showDeleteIcon: boolean;
    private maskedText = "XXXXXXXX";

    constructor(
        @Inject("OrgGroupStoreNewService") private orgGroupStoreNew: OrgGroupStoreNew,
        private translatePipe: TranslatePipe,
        private permissions: Permissions,
    ) {
        this.RequestQueueStatusIdMap = this.getRequestStatusIdMap();
        this.updatePermissions();
    }

    public updatePermissions() {
        this.showOnlyExternalComments = this.permissions.canShow("AccessExternalComments") && !this.permissions.canShow("AccessInternalComments");
        this.showOnlyInternalComments = this.permissions.canShow("AccessInternalComments") && !this.permissions.canShow("AccessExternalComments");
        this.hideAllComments = !this.permissions.canShow("AccessExternalComments") && !this.permissions.canShow("AccessInternalComments");
        this.showDeleteIcon = this.permissions.canShow("AttachmentsDelete");
    }

    public getRequestColumns(): any[] {
        return [
            { columnKey: "requestQueueRefId", name: "ID" },
            { columnKey: "name", name: "Name" },
            { columnKey: "organization", name: "Organization" },
            { columnKey: "statusLabel", name: "Status" },
            { columnKey: "requestType", name: "RequestType" },
            { columnKey: "daysLeft", name: "DaysLeftToRespond" },
            { columnKey: "isExtended", name: "Extended" },
            { columnKey: "dateCreated", name: "DateCreated" },
            { columnKey: "subjectType", name: "SubjectType" } ,
            { columnKey: "reviewer", name: "Approver" },
        ];
    }

    public getColumnConfig(): any {
        return {
            requestQueueRefId: { sortable: false, defaultSorted: true, defaultSortDirection: 2, style: { weight: 5 }, sortKey: "requestQueueRefId" },
            name: { sortable: false, style: { weight: 6.5 }, sortKey: "firstName" },
            organization: { sortable: false, style: { weight: 7 } },
            statusLabel: { sortable: false, style: { weight: 6.5 }, component: "tag", colorKey: "statusLabelColour", sortKey: "requestQueueStatus" },
            requestType: { sortable: false, style: { weight: 6 }, sortKey: "requestTypes.option" },
            daysLeft: { sortable: false, style: { weight: 4 }, component: "daysLeft", sortKey: "daysToRespond" },
            isExtended: { sortable: false, style: { weight: 4 }, sortKey: "extended" },
            dateCreated: { sortable: false, style: { weight: 7 }, component: "date", sortKey: "createdDate" },
            subjectType: { sortable: false, style: { weight: 6 }, sortKey: "subjectTypes.option" },
            reviewer: { sortable: false, style: { weight: 6 }, sortKey: "reviewer" },
        };
    }

    public configureRequestData(requestData: IRequestQueueDetails[]): any[] {
        _.forEach(requestData, (row: IRequestQueueDetails): void => {
            row = this.configureSingleRequestData(row) || row;
        });
        return requestData;
    }

    public configureSingleRequestData(row: IRequestQueueDetails): any | undefined {
        if (!row.requestQueueId) {
            row.requestQueueId = "";
        }
        this.RequestQueueStatusIdMap = this.getRequestStatusIdMap();
        if (!row || _.isEmpty(row)) return;
        if (row.status === RequestQueueStatus.Locked) {
            row.statusIcon = "fa fa-lock";
            row.name = this.maskedText;
            row.organization = this.maskedText;
            row.requestType =  this.maskedText;
            row.subjectType = this.maskedText;
            row.reviewer = this.maskedText;
            row.completedSubtasks = this.maskedText;
            row.totalSubtasks = this.maskedText;
            row.webformName = this.maskedText;
            row.dsLanguageName = this.maskedText;
        } else {
            row.name = row.firstName || row.lastName ? `${row.firstName} ${row.lastName}` : row.email;
            const otherData = row.otherData || {};
            if (otherData["Request Details"]) {
                otherData["Request Details"] = otherData["Request Details"].replace(/\\\\/g, " ");
            }
            if (row.multiselectFields) {
                each(row.multiselectFields, (field: IMultiselectFields): void => {
                    otherData[field.FieldName] = field.Values.join(", ");
                });
            }
            row.formFields = _.map(_.keys(otherData), (key: string): any => ({ fieldName: key, value: otherData[key] }));

            const requestDetails: any[] = _.remove(row.formFields, (field: any): boolean => field.fieldName === "Request Details");
            row.requestDetails = requestDetails.length ? requestDetails[0] : null;

            // This is to convert the array of subject/request types into a single string
            row.requestType = _(row.requestTypes).map("fieldName").join(", ") || _(row.requestTypes).join(", ");
            row.subjectType = _(row.subjectTypes).map("fieldName").join(", ") || _(row.subjectTypes).join(", ");
            const org: IOrganization = this.orgGroupStoreNew.tenantOrgTable[row.orgGroupId];
            row.organization = org ? org.name : "---";

            let filteredComments: any[] = row.comments;
            if (this.showOnlyExternalComments) {
                filteredComments = _.filter(row.comments, (comment: IRequestQueueComment): boolean => comment.isInternalComment === false);
            } else  if (this.showOnlyInternalComments) {
                filteredComments = _.filter(row.comments, (comment: IRequestQueueComment): boolean => comment.isInternalComment === true);
            } else if ( this.hideAllComments) {
                filteredComments = [];
            }

            row.comments = _.each(filteredComments, (comment: IRequestQueueComment): void => {

                if (!_.isNil(comment.attachments)) {
                    _.map(comment.attachments, (attachment: IAttachmentFileResponse): IAttachment => {
                        attachment.name = attachment.FileName;
                        attachment.icon = "ot ot-file";
                        attachment.showDeleteIcon = this.showDeleteIcon;
                        if (attachment.StatusId === 90) {
                            attachment.isDeleted = true;
                            attachment.deleteDescriptionText = this.translatePipe.transform("AutoDeleteAttachmentInfo");
                        }
                        return attachment;
                    });
                }

                comment.date = comment.createdDate;
                comment.styleModifier = comment.isInternalComment ? "green" : "";
                comment.name = this.getCommentNameText(comment);
                comment.icon = this.getCommentIcon(comment);
                if (comment.authorDetails) {
                    comment.prefixText = comment.authorDetails.authorFullName || comment.authorDetails.authorEmail || `${comment.authorDetails.authorFirstName} ${comment.authorDetails.authorLastName}` || "";
                } else {
                    comment.prefixText = "";
                }
                comment.content = this.buildCommentString(row, comment);
                comment.showStatus = true;
                comment.statusText = this.getCommentStatusName(comment.requestStatus, row.workflow.workflowDetailDtos);
                comment.statusColor = this.RequestQueueStatusIdMap[comment.requestStatus]
                    ? this.RequestQueueStatusIdMap[comment.requestStatus].color
                    : "#1f96db";
            });

            if (row.comments) {
                row.comments.sort((commentA: any, commentB: any): number => {
                    const dateA: any = new Date(commentA.date).getTime();
                    const dateB: any = new Date(commentB.date).getTime();
                    return dateB - dateA;
                });
            }
        }
        // saftey check
        if (row.currentStageInfo) {
            row.status = row.currentStageInfo.status;
            row.statusLabelColour = row.currentStageInfo.labelColor;
            if (row.status === RequestQueueStatus.Rejected || row.status === RequestQueueStatus.Close) {
                row.statusLabel = this.RequestQueueStatusIdMap[row.status].label.toUpperCase();
            } else {
                row.statusLabel = row.currentStageInfo.stage;
            }
        } else {
            const requestStatusMap: IStatusLabel = this.RequestQueueStatusIdMap[row.status];
            row.statusLabelColour = requestStatusMap ? requestStatusMap.color : "";
            row.statusLabel = row.currentStage || (requestStatusMap ? requestStatusMap.label : "- - - -");
        }

        row.isExtended = row.extended ? this.translatePipe.transform("Yes") : this.translatePipe.transform("No");
        if (_.isNil(row.daysLeft) || row.status === RequestQueueStatus.Rejected ||
            row.status === RequestQueueStatus.Close || row.status === RequestQueueStatus.Complete ||
            _.isNil(row.deadline)) {
            row.daysLeft = "---";
        }
        return row;
    }

    public configureSubTaskData(subTaskData: ISubTaskRowTable, workflowList?: IWorkflowItem[]): ISubTaskRowTable {
        _.forEach(subTaskData.content, (row: ISubTask): void => {
            row = this.configureSingleSubTaskData(row, workflowList) || row;
            if (row.type) {
                const taskTypeMap: IStringMap<ITypeLabel> = this.getSubTaskTypeMap();
                row.typeLabel = taskTypeMap[row.type].label;
            }
        });
        return subTaskData;
    }

    public configureSingleSubTaskData(row: ISubTask, workflowList: IWorkflowItem[]): ISubTask | undefined {
        if (!row || _.isEmpty(row)) return;
        if (row.status !== undefined) {
            const statusIdMap: IStringMap<IStatusLabel> = this.getSubTaskIdMap();
            row.statusLabel = statusIdMap[row.status].label;
            row.statusColor = statusIdMap[row.status].color;
        }
        if (row.type) {
            const taskTypeMap: IStringMap<ITypeLabel> = this.getSubTaskTypeMap();
            row.typeLabel = taskTypeMap[row.type].label;
        }
        if (_.isNil(row.deadline)) {
            row.deadlineDate = "- - -";
        } else {
            row.deadlineDate = moment.utc(row.deadline).tz(moment.tz.guess()).format("L");
        }
        if (_.isNil(row.reminder)) {
            row.reminderDate = "- - -";
        } else {
            row.reminderDate = moment.utc(row.reminder).tz(moment.tz.guess()).format("L");
        }
        if (_.isNil(row.assignee)) row.assignee = "- - -";
        const worflowStageItem = find(workflowList, (workflowItem: IWorkflowItem): boolean => workflowItem.status === row.workflowStatus);
        row.workflowStage = worflowStageItem ? worflowStageItem.stage : " - - - -";
        return row;

    }

     private getCommentNameText(comment: IRequestQueueComment): string {
        switch (comment.actionType) {
            case RequestQueueStatusTags.Assign:
                return this.translatePipe.transform("ReassignedRequest");
            case RequestQueueStatusTags.Extend:
                return this.translatePipe.transform("ExtendedDeadline");
            case RequestQueueStatusTags.Reject:
                return this.translatePipe.transform("RejectedRequest");
            case RequestQueueStatusTags.Complete:
                return this.translatePipe.transform("CompletedRequest");
            case RequestQueueStatusTags.TaskComment:
                return this.translatePipe.transform("CreatedSubTask");
                case RequestQueueStatusTags.TaskResponse:
                case RequestQueueStatusTags.TaskComplete:
                return this.translatePipe.transform("RespondedSubTask");
            case RequestQueueStatusTags.Create:
                return this.translatePipe.transform("SubmittedRequest");
            case RequestQueueStatusTags.Comment:
            default:
                return comment.isInternalComment ? `${this.translatePipe.transform("PostedInternalComment")}` : `${this.translatePipe.transform("MadePublicReply")}`;
        }
    }

    private getCommentIcon(comment: IRequestQueueComment): string {
        switch (comment.actionType) {
            case RequestQueueStatusTags.Assign:
                return "ot ot-exchange";
            case RequestQueueStatusTags.Extend:
                return "ot ot-history";
            case RequestQueueStatusTags.Reject:
                return "ot ot-times-circle text-error";
            case RequestQueueStatusTags.Suspend:
                return "ot ot-pause-circle-o text-warning";
            case RequestQueueStatusTags.Complete:
                return "ot ot-check-square-o text-success";
            case RequestQueueStatusTags.Verify:
                return "ot ot-check-circle-o";
            case RequestQueueStatusTags.TaskComment:
                return "fa fa-list-ul";
            case RequestQueueStatusTags.TaskResponse:
                return "fa fa-reply";
            case RequestQueueStatusTags.Comment:
            default:
                return comment.isInternalComment ? "ot ot-comments-o" : "ot ot-envelope";
        }
    }

    private getCommentStatusName(status: number, workflowList: IWorkflowItem[]): string {
        const workflowItem: IWorkflowItem = _.find(workflowList, (item: IWorkflowItem): boolean => item.status === status);
        if (workflowItem) {
            return workflowItem.stage;
        } else {
            return this.RequestQueueStatusIdMap[status]
                ? this.RequestQueueStatusIdMap[status].label
                : "- - - -";
        }
    }

    private buildCommentString(row: IRequestQueueDetails, comment: IRequestQueueComment): string {
        const commentString: string = comment.commentString;
        switch (comment.actionType) {
            case "Assign":
                return `<div class="margin-bottom-1"><span class="text-bold">${this.translatePipe.transform("AssignedTo")}: </span><span>${comment.assignedTo || "- - - -"}</span></div><div>${commentString}</div>`;
            case "Extend":
                return `<div class="margin-bottom-1"><span class="text-bold">${this.translatePipe.transform("DaysExtended")}: </span><span>${comment.daysExtended}</span></div><div>${commentString}</div>`;
            case "Create":
                // This is a CE MVP version, text is hardcoded and read temporarily till there is more clarity on design.
                if (commentString === "<p>No additional request details provided.</p>") {
                    return `<p><em>** System Generated Message: No text was entered into the Request Details field by the individual **</em><p>`;
                } else {
                    return commentString;
                }
            case "Comment":
            default:
                return commentString;
        }
    }

    private getRequestStatusIdMap(): IStringMap<IStatusLabel> {
        return {
            [RequestQueueStatus.New]: {
                label: this.translatePipe.transform("New"),
                id: RequestQueueStatus.New,
                color: RequestQueueColors.New,
            },
            [RequestQueueStatus.Complete]: {
                label: this.translatePipe.transform("Complete"),
                id: RequestQueueStatus.Complete,
                color: RequestQueueColors.Complete,
            },
            [RequestQueueStatus.Rejected]: {
                label: this.translatePipe.transform("Rejected"),
                id: RequestQueueStatus.Rejected,
                color: RequestQueueColors.Rejected,
            },
            [RequestQueueStatus.Close]: {
                label: this.translatePipe.transform("Closed"),
                id: RequestQueueStatus.Close,
                color: RequestQueueColors.Closed,
            },
            [RequestQueueStatus.Locked]: {
                label: this.translatePipe.transform("Locked"),
                id: RequestQueueStatus.Locked,
                color: "",
            },
            [RequestQueueStatus.Unverified]: {
                label: this.translatePipe.transform("Unverified"),
                id: RequestQueueStatus.Unverified,
                color: "",
            },
            [RequestQueueStatus.Expired]: {
                label: this.translatePipe.transform("Expired"),
                id: RequestQueueStatus.Expired,
                color: "",
            },
        };
    }

    private getSubTaskIdMap(): IStringMap<IStatusLabel> {
        return {
            [SubTaskStatus.Open]: {
                label: this.translatePipe.transform("Open"),
                id: SubTaskStatus.Open,
                color: RequestQueueColors.Unassigned,
            },
            [SubTaskStatus.Assigned]: {
                label: this.translatePipe.transform("Assigned"),
                id: SubTaskStatus.Assigned,
                color: RequestQueueColors.Assigned,
            },
            [SubTaskStatus.UnAssigned]: {
                label: this.translatePipe.transform("Unassigned"),
                id: SubTaskStatus.UnAssigned,
                color: RequestQueueColors.Unassigned,
            },
            [SubTaskStatus.ReadyForReview]: {
                label: this.translatePipe.transform("ReadyForReview"),
                id: SubTaskStatus.ReadyForReview,
                color: RequestQueueColors.Complete,
            },
            [SubTaskStatus.Complete]: {
                label: this.translatePipe.transform("Complete"),
                id: SubTaskStatus.Complete,
                color: RequestQueueColors.Complete,
            },
            [SubTaskStatus.Deleted]: {
                label: this.translatePipe.transform("Deleted"),
                id: SubTaskStatus.Deleted,
                color: "",
            },
        };
    }

    private getSubTaskTypeMap(): IStringMap<ITypeLabel> {
        return {
            [SubTaskType.User]: {
                label: this.translatePipe.transform("Manual"),
                id: SubTaskType.User,
            },
            [SubTaskType.System]: {
                label: this.translatePipe.transform("System"),
                id: SubTaskType.System,
            },
        };
    }

}
