import { Injectable } from "@angular/core";

// Interfaces
import {
    IRequestQueueDropdownAction,
    IRequestQueueDetails,
} from "dsarModule/interfaces/request-queue.interface";
import {
    RequestQueueStatus,
    RequestQueueActionMap,
} from "dsarModule/enums/request-queue-status.enum";
import { Permissions } from "modules/shared/services/helper/permissions.service";

interface IRequestQueueDropdownActionMap {
    [RequestQueueActionMap.New]: IRequestQueueDropdownAction[];
    [RequestQueueActionMap.Progress]: IRequestQueueDropdownAction[];
    [RequestQueueActionMap.Complete]: IRequestQueueDropdownAction[];
    [RequestQueueActionMap.Reject]: IRequestQueueDropdownAction[];
    [RequestQueueActionMap.Closed]: IRequestQueueDropdownAction[];
    [RequestQueueActionMap.Unverified]: IRequestQueueDropdownAction[];
    [RequestQueueActionMap.Expired]: IRequestQueueDropdownAction[];
}

@Injectable()
export class RequestQueueDropdownService {

    private actionsMap: IRequestQueueDropdownActionMap = { // actions for all the statuses for the RQ row
        [RequestQueueActionMap.New]: [], // new and verifying identity
        [RequestQueueActionMap.Progress]: [], // progress - any status that is not new, complete, or rejected
        [RequestQueueActionMap.Complete]: [], // complete
        [RequestQueueActionMap.Reject]: [], // reject
        [RequestQueueActionMap.Closed]: [], // closed
        [RequestQueueActionMap.Unverified]: [], // Unverified
        [RequestQueueActionMap.Expired]: [], // Expired
    };
    private actions: IRequestQueueDropdownAction[];

    constructor(readonly permissions: Permissions) {
        this.setActions();
    }

    public getActions(rowData: IRequestQueueDetails): IRequestQueueDropdownAction[] {
        if (!this.actions.length) return this.actions;
        let tempActions: IRequestQueueDropdownAction[] = [];
        switch (rowData.status) {
            case RequestQueueStatus.New:
            case RequestQueueStatus.VerifyingIdentity:
                tempActions = this.actionsMap[RequestQueueActionMap.New];
                break;
            case RequestQueueStatus.InProgress:
                tempActions = this.actionsMap[RequestQueueActionMap.Progress];
                break;
            case RequestQueueStatus.Complete:
                tempActions = this.actionsMap[RequestQueueActionMap.Complete];
                break;
            case RequestQueueStatus.Rejected:
                tempActions = this.actionsMap[RequestQueueActionMap.Reject];
                break;
            case RequestQueueStatus.Close:
                tempActions = this.actionsMap[RequestQueueActionMap.Closed];
                break;
            case RequestQueueStatus.Unverified:
                tempActions = this.actionsMap[RequestQueueActionMap.Unverified];
                break;
            case RequestQueueStatus.Expired:
                tempActions = this.actionsMap[RequestQueueActionMap.Expired];
                break;
            default:
                tempActions = this.actionsMap[RequestQueueActionMap.Progress];
        }

        return tempActions.map((actionItem: IRequestQueueDropdownAction): IRequestQueueDropdownAction => {
            actionItem.rowRefId = rowData.requestQueueRefId;
            return actionItem;
        });
    }

    private setActions(): void {
        this.actions = [];
        if (this.permissions.canShow("DSARRequestQueueAssign")) {
            this.actions.push({
                id: "requestQueueAssign",
                textKey: "Assign",
            });
            this.actionsMap[RequestQueueActionMap.New].push(this.actions[this.actions.length - 1]);
            this.actionsMap[RequestQueueActionMap.Progress].push(this.actions[this.actions.length - 1]);
        }
        if (this.permissions.canShow("DSARRequestQueueExtend")) {
            this.actions.push({
                id: "requestQueueExtend",
                textKey: "Extend",
            });
            this.actionsMap[RequestQueueActionMap.New].push(this.actions[this.actions.length - 1]);
            this.actionsMap[RequestQueueActionMap.Progress].push(this.actions[this.actions.length - 1]);
        }
        if (this.permissions.canShow("DSARRequestQueueReject")) {
            this.actions.push({
                id: "requestQueueReject",
                textKey: "Reject",
            });
            this.actionsMap[RequestQueueActionMap.New].push(this.actions[this.actions.length - 1]);
            this.actionsMap[RequestQueueActionMap.Progress].push(this.actions[this.actions.length - 1]);
            this.actionsMap[RequestQueueActionMap.Unverified].push(this.actions[this.actions.length - 1]);
        }
        if (this.permissions.canShow("DSARRequestQueueDelete")) {
            this.actions.push({
                id: "requestQueueDelete",
                textKey: "Delete",
            });
            this.actionsMap[RequestQueueActionMap.New].push(this.actions[this.actions.length - 1]);
            this.actionsMap[RequestQueueActionMap.Progress].push(this.actions[this.actions.length - 1]);
            this.actionsMap[RequestQueueActionMap.Complete].push(this.actions[this.actions.length - 1]);
            this.actionsMap[RequestQueueActionMap.Reject].push(this.actions[this.actions.length - 1]);
            this.actionsMap[RequestQueueActionMap.Closed].push(this.actions[this.actions.length - 1]);
            this.actionsMap[RequestQueueActionMap.Expired].push(this.actions[this.actions.length - 1]);
        }
        if (this.permissions.canShow("DSARRequestQueueDsAccess")) {
            this.actions.push({
                id: "requestQueueRevoke",
                textKey: "RevokeAccess",
            });
            this.actionsMap[RequestQueueActionMap.New].push(this.actions[this.actions.length - 1]);
            this.actionsMap[RequestQueueActionMap.Progress].push(this.actions[this.actions.length - 1]);
            this.actionsMap[RequestQueueActionMap.Complete].push(this.actions[this.actions.length - 1]);
        }
        if (this.permissions.canShow("DSARRequestQueueClose")) {
            this.actions.push({
                id: "requestQueueClose",
                textKey: "Close",
            });
            this.actionsMap[RequestQueueActionMap.New].push(this.actions[this.actions.length - 1]);
            this.actionsMap[RequestQueueActionMap.Progress].push(this.actions[this.actions.length - 1]);
            this.actionsMap[RequestQueueActionMap.Unverified].push(this.actions[this.actions.length - 1]);
        }
        if (this.permissions.canShow("DSAREditRequestQueue")) {
            this.actions.push({
                id: "requestQueueUpdateRequestEntries",
                textKey: "EditRequest",
            });
            this.actionsMap[RequestQueueActionMap.New].push(this.actions[this.actions.length - 1]);
            this.actionsMap[RequestQueueActionMap.Progress].push(this.actions[this.actions.length - 1]);
        }
        if (this.permissions.canShow("DSARMultiFactAuthentication")) {
            this.actions.push({
                id: "requestQueueRegenerate",
                textKey: "RegeneratePassword",
            });
            this.actionsMap[RequestQueueActionMap.New].push(this.actions[this.actions.length - 1]);
            this.actionsMap[RequestQueueActionMap.Progress].push(this.actions[this.actions.length - 1]);
        }
        if (this.permissions.canShow("DSARChangeWorkflow")) {
            this.actions.push({
                id: "requestQueueChangeWorkflow",
                textKey: "ChangeWorkflow",
            });
            this.actionsMap[RequestQueueActionMap.New].push(this.actions[this.actions.length - 1]);
            this.actionsMap[RequestQueueActionMap.Progress].push(this.actions[this.actions.length - 1]);
        }
        if (!this.actionsMap[RequestQueueActionMap.Closed].length) {
            this.actions.push({
                id: "requestQueueNoActions",
                textKey: "NoActionsAvailable",
            });
            this.actionsMap[RequestQueueActionMap.Closed].push(this.actions[this.actions.length - 1]);
        }
    }

}
