// Angular
import {
    Injectable,
    Inject,
} from "@angular/core";

// Services
import { ProtocolService } from "modules/core/services/protocol.service";
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";

// Interfaces
import {
    IProtocolConfig,
    IProtocolMessages,
    IProtocolResponse,
} from "interfaces/protocol.interface";
import {
    IRule,
    IRuleMeta,
} from "modules/dsar/components/webforms/shared/interfaces/one-rules.interface";
import { IWebformRuleMeta } from "modules/dsar/interfaces/dsar-webform-rules-meta.interface";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

@Injectable()
export class DsarWebformRulesApiService {

    constructor(
        @Inject("$rootScope") private $rootScope: IExtendedRootScopeService,
        private OneProtocol: ProtocolService,
        private translatePipe: TranslatePipe,
    ) { }

    geWebformRuleMeta(webformId: string): ng.IPromise<IWebformRuleMeta> {
        const lang = this.$rootScope.tenantLanguage;
        const config: IProtocolConfig = this.OneProtocol.customConfig("GET", `/api/datasubject/v1/rule/metadata/${webformId}/${lang}`);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("") } }; // TODO get error msg
        return this.OneProtocol.http(config, messages).then((response: IProtocolResponse<IWebformRuleMeta>): IWebformRuleMeta => {
            if (response.result) {
                return response.data;
            }
        });
    }

    getAllRuleMetas(webformId: string): ng.IPromise<IRuleMeta[]> {
        const lang = this.$rootScope.tenantLanguage;
        const config: IProtocolConfig = this.OneProtocol.customConfig("GET", `/api/datasubject/v1/rule/webform/${webformId}/${lang}`);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("") } }; // TODO get error msg
        return this.OneProtocol.http(config, messages).then((response: IProtocolResponse<IRuleMeta[]>): IRuleMeta[] => {
            if (response.result) {
                return response.data;
            }
        });
    }

    getRuleById(ruleId: string): ng.IPromise<IRule> {
        const lang = this.$rootScope.tenantLanguage;
        const config: IProtocolConfig = this.OneProtocol.customConfig("GET", `/api/datasubject/v1/rule/${ruleId}/${lang}`);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("") } }; // TODO get error msg
        return this.OneProtocol.http(config, messages).then((response: IProtocolResponse<IRule>): IRule => {
            if (response.result) {
                return response.data;
            }
        });
    }

    reorderRules(webformId: string, rules: IRule[]): ng.IPromise<IRuleMeta[]> {
        const lang = this.$rootScope.tenantLanguage;
        const config: IProtocolConfig = this.OneProtocol.customConfig("POST", `/api/datasubject/v1/rule/webform/${webformId}/reorder/${lang}`, null, { rules });
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("") } }; // TODO get error msg
        return this.OneProtocol.http(config, messages).then((response: IProtocolResponse<IRuleMeta[]>): IRuleMeta[] => {
            if (response.result) {
                return response.data;
            }
        });
    }

    saveRules(webformId: string, rules: IRule[], isDelete?: boolean): ng.IPromise<IRuleMeta[]> {
        const lang = this.$rootScope.tenantLanguage;
        const successMessageKey = isDelete ? "RulesDeletedSuccessfully" : "RulesSavedSuccessfully";
        const failureMessageKey = isDelete ? "ErrorDeletingRules" : "ErrorSavingRules";
        const config: IProtocolConfig = this.OneProtocol.customConfig("POST", `/api/datasubject/v1/rule/webform/${webformId}/${lang}`, null, { rules });
        const messages: IProtocolMessages = {
            Success: { custom: this.translatePipe.transform(successMessageKey)},
            Error: { custom: this.translatePipe.transform(failureMessageKey) },
         };
        return this.OneProtocol.http(config, messages).then((response: IProtocolResponse<IRuleMeta[]>): IRuleMeta[] => {
            if (response.result) {
                return response.data;
            }
        });
    }
}
