// libraries
import {
    Injectable,
    Inject,
    ViewChild,
} from "@angular/core";
import { StateService } from "@uirouter/core";
import {
    isNil,
    isEmpty,
    find,
    map,
    sortBy,
    assign,
    filter,
    cloneDeep,
} from "lodash";
import {
    IPromise,
} from "angular";

// Rxjs
import {
    Observable,
    BehaviorSubject,
} from "rxjs";
import {
    take,
    filter as filterResponse,
    concatMap,
} from "rxjs/operators";

// interfaces
import { IWorkflowItem } from "interfaces/workflows.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";
import { IDSARAttachmentModalResolve } from "interfaces/dsar-attachment-modal-resovle.interface";
import { IConfirmationModalResolve } from "interfaces/confirmation-modal-resolve.interface";
import {
     IRequestQueueDetails,
     IGrantAccessModal,
     IRequestQueueRejectModalResolve,
     IRequestQueuePathItem,
     IRequestQueueAssignModalResolve,
     IRequestQueueExtendModalResolve,
     IRevokeAccessSubmit,
     IRequestResolutionFormatted,
} from "dsarModule/interfaces/request-queue.interface";
import {
    IRequestQueueClose,
    IRequestQueueReject,
    IRequestQueueAssign,
} from "dsarModule/interfaces/request-queue-actions.interface";
import { IAttachmentFileResponse } from "interfaces/attachment-file.interface";
import { IAttachment } from "interfaces/attachment.interface";
import { IDeleteConfirmationModalResolve } from "interfaces/delete-confirmation-modal-resolve.interface";
import { IRequestQueueCommentUpdate } from "dsarModule/interfaces/request-queue-comment.interface";
import { IStringMap } from "interfaces/generic.interface";
import { IDSARSettings } from "dsarModule/interfaces/dsar-settings.interface";
import {
    IUpdateDetailsModalResolve,
    IUpdateDetailsPutData,
} from "dsarModule/interfaces/dsar-update-details-modal.interface";
import { IResponseTemplate } from "dsarModule/interfaces/response-templates.interface";
import {
    IRequestQueueComplete,
    IRequestQueueExtend,
 } from "dsarModule/interfaces/request-queue-actions.interface";
import { IKeyValue } from "interfaces/generic.interface";
import { IGetWebformLanguageResponse } from "interfaces/dsar/dsar-edit-language.interface";
import { RequestQueueRejectModal } from "dsarModule/components/request-queue/reject-modal/request-queue-reject-modal.component";

interface IHeaderDetailList extends IKeyValue<string | boolean | number> {
    translate?: boolean;
}

// Constants and Enums
import {
    RequestQueueStatus,
    RequestQueueCommentStatus,
} from "dsarModule/enums/request-queue-status.enum";
import {
    RequestQueueActions,
    RequestQueueSidebar,
    RequestQueueHeader,
    CostCalc,
} from "dsarModule/constants/dsar-request-queue.constants";

// Services
import { ModalService } from "sharedServices/modal.service";
import { TimeStamp } from "oneServices/time-stamp.service";
import { RequestQueueHistoryService } from "dsarservice/request-queue-history.service";
import { NotificationService } from "modules/shared/services/provider/notification.service";
import { Permissions } from "modules/shared/services/helper/permissions.service";
import { RequestQueueService } from "dsarservice/request-queue.service";
import { DSARSettingsService } from "dsarservice/dsar-settings.service";
import {
    OtModalService,
    ModalSize,
} from "@onetrust/vitreus";

// Components
import { MFAPasswordModal } from "dsarModule/components/request-queue/mfa-password-modal/mfa-password-modal.component";
import { RequestQueueAssignModal } from "modules/dsar/components/request-queue/assign-modal/request-queue-assign-modal.component";
import { RequestQueueChangeWorkflow } from "dsarModule/components/request-queue/change-workflow/request-queue-change-workflow.component";
import { RequestQueueExtendModal } from "dsarModule/components/request-queue/extend-modal/request-queue-extend-modal.component";
import { RequestQueueUpdateDetailsModal } from "dsarModule/components/request-queue/update-details-modal/request-queue-update-details-modal.component";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

@Injectable()
export class RequestQueueDetailService {

    internalAttachments: IAttachmentFileResponse[];
    externalAttachments: IAttachmentFileResponse[];
    responseTemplates: IResponseTemplate[];
    postingComment: boolean;
    permissionsMap: IStringMap<boolean> = {
        AccessInternalComments: this.permissions.canShow("AccessInternalComments"),
        AccessExternalComments: this.permissions.canShow("AccessExternalComments"),
        DSARExternalCommentCreate: this.permissions.canShow("DSARExternalCommentCreate"),
        DSARInternalCommentCreate: this.permissions.canShow("DSARInternalCommentCreate"),
        LeftColumnInDSRDetailPage: this.permissions.canShow("LeftColumnInDSRDetailPage"),
        HeaderSectionWithRequestDetails: this.permissions.canShow("HeaderSectionWithRequestDetails"),
        SeeActivityStream: this.permissions.canShow("SeeActivityStream"),
        AttachmentsAccess: this.permissions.canShow("AttachmentsAccess"),
        AttachmentsUpload: this.permissions.canShow("FileCreate"),
        AccessRequestQueueUI: this.permissions.canShow("DSARRequestQueue"),
        RequestQueueStatusBar: this.permissions.canShow("DSARRequestQueueStatusBar"),
        DSARCostCalculationEdit: this.permissions.canShow("DSARCostCalculationEdit"),
        DSARCostCalculationView: this.permissions.canShow("DSARCostCalculationView"),
        DSARSubTaskView: this.permissions.canShow("DSARSubTaskView"),
        DSARRequestQueueLanguageView: this.permissions.canShow("DSARRequestQueueLanguageView"),
        DSARRequestQueueLanguageUpdate: this.permissions.canShow("DSARRequestQueueLanguageUpdate"),
        DSARRequestQueueAssign: this.permissions.canShow("DSARRequestQueueAssign"),
        History: this.permissions.canShow("DSARRequestQueueHistoryAccess"),
        DSARResponseTemplateLanguageSupport: this.permissions.canShow("DSARResponseTemplateLanguageSupport"),
        DSAREditOrganization: this.permissions.canShow("DSAREditOrganization"),
    };
    loadingStatusBarChanges = true;
    statusBarActiveIndex: number;
    statusBarButtonDisabled = false;
    pathItems: IRequestQueuePathItem[] = [];
    RequestQueueStatusEnums = RequestQueueStatus;
    currencyIso: any;
    hourlyCost: number;
    isCostCalcEnabled: boolean;
    subTasksAdded = false;
    taskMode = false; // used to determine if an user can only see the the sub task and comments for RQ details
    workflowStageChanged = false; // used to determine if the sub-task list needs to be refreshed - if this is true, make the api call to get sub-tasks
    workflowList: IWorkflowItem[] = [];
    disableEdit: boolean; // used to disable sidebar edit
    isUnverified: boolean;
    isExpired: boolean;
    isRejected: boolean;
    isClosed: boolean;
    isCompleted: boolean;
    currentStatus: number;
    initialLoad = true;
    clearComments = false;
    requestQueueId: string;
    requestQueueDetails: IRequestQueueDetails;
    sidebarRequestDetails: Array<IKeyValue<string | boolean | number>>; // see buildSidebarDetails() for what the object returns - it is dynamic
    headerRequestDetails: IHeaderDetailList[]; // see buildHeaderDetails() for what the ojbect return - it is dynamic
    showDropdown: boolean; // logic is to check if case is either complete or rejected
    languageList: IGetWebformLanguageResponse[];
    selectedLanguage: string;
    isFirstPublicReply: boolean;

    get replyModalData(): IConfirmationModalResolve {
        return {
            modalTitle: this.translatePipe.transform("RespondToDataSubject"),
            confirmationText: this.translatePipe.transform("AreYouSureYouWantToSendMessage"),
            cancelButtonText: this.translatePipe.transform("Cancel"),
            submitButtonText: this.translatePipe.transform("Send"),
            iconClass: "ot ot-envelope",
            promiseToResolve: null,
        };
    }
    get deleteAttachmentModalData(): IDeleteConfirmationModalResolve  {
        return {
            promiseToResolve: null,
            modalTitle: this.translatePipe.transform("DeleteAttachment"),
            confirmationText: this.translatePipe.transform("AreYouSureDeleteFile"),
            submitButtonText: this.translatePipe.transform("Delete"),
            cancelButtonText: this.translatePipe.transform("Cancel"),
        };
    }
    get revokeAccessModalData(): IDeleteConfirmationModalResolve {
        return {
            promiseToResolve: null,
            modalTitle: this.translatePipe.transform("RevokePortalAccess"),
            confirmationText: this.translatePipe.transform("RevokeAccessPortalWarningInfo"),
            submitButtonText: this.translatePipe.transform("Revoke"),
            cancelButtonText: this.translatePipe.transform("Cancel"),
        };
    }
    get closeQueueModalData(): IDeleteConfirmationModalResolve {
        return {
            promiseToResolve: null,
            modalTitle: this.translatePipe.transform("CloseRequest"),
            confirmationText: this.translatePipe.transform("CloseRequestQueueConfirmation"),
            submitButtonText: this.translatePipe.transform("Confirm"),
            cancelButtonText: this.translatePipe.transform("Cancel"),
        };
    }
    get completeQueueModalData(): IConfirmationModalResolve {
        return {
            promiseToResolve: null,
            modalTitle: this.translatePipe.transform("Complete"),
            confirmationText: this.translatePipe.transform("AreYouSureCompleteRequest"),
            submitButtonText: this.translatePipe.transform("Confirm"),
            cancelButtonText: this.translatePipe.transform("Cancel"),
        };
    }
    get rejectQueueModalData(): IRequestQueueRejectModalResolve {
        return {
            promiseToResolve: null,
            modalTitle: this.translatePipe.transform("RejectRequest"),
            confirmationText: this.translatePipe.transform("DSARRejectModalText"),
            submitButtonText: this.translatePipe.transform("Send"),
            cancelButtonText: this.translatePipe.transform("Cancel"),
            requestQueueDetails: null,
            resolutionOptions: null,
            data: {
                comment: null,
                attachments: null,
            },
        };
    }

    get extendQueueModalData(): IRequestQueueExtendModalResolve {
        return {
            promiseToResolve: null,
            modalTitle: this.translatePipe.transform("ExtendRequest"),
            confirmationText: this.translatePipe.transform("DSARRejectModalText"),
            submitButtonText: this.translatePipe.transform("Send"),
            cancelButtonText: this.translatePipe.transform("Cancel"),
            comment: "",
        };
    }
    get assignQueueModalData(): IRequestQueueAssignModalResolve {
        return {
            promiseToResolve: null,
            modalTitle: "AssignRequest",
            confirmationText: null,
            submitButtonText: this.translatePipe.transform("Assign"),
            cancelButtonText: this.translatePipe.transform("Cancel"),
            requestQueueDetails: null,
        };
    }
    costCalculationSettings: IDSARSettings[];
    getObservable$: Observable<RequestQueueDetailService>;
    isCommentSuccessful: boolean;

    private contentSource = new BehaviorSubject(this);

    constructor(
        private stateService: StateService,
        @Inject("Export") readonly Export: any,
        readonly requestQueueService: RequestQueueService,
        readonly modalService: ModalService,
        private readonly dsarSettingsService: DSARSettingsService,
        private readonly timeStamp: TimeStamp,
        private historyService: RequestQueueHistoryService,
        private notificationService: NotificationService,
        readonly permissions: Permissions,
        private translatePipe: TranslatePipe,
        private otModalService: OtModalService,
    ) {
        this.getObservable$ = this.contentSource.asObservable();
     }

    initializeRequestDetail(requestQueueId: string): Promise<boolean> {
        this.requestQueueId = requestQueueId;
        const promiseArr: Array<IPromise<IProtocolResponse<any>>> = [
            this.requestQueueService.getRequest(this.requestQueueId),
            this.dsarSettingsService.getAllEnabledResponseTemplatesByRequestQueue(this.requestQueueId),
        ];
        if (this.permissionsMap.DSARCostCalculationView) {
            const DSARCostCalcSettings: IStringMap<any> = JSON.parse(sessionStorage.getItem("DSARCostCalcSettings"));
            if (isNil(DSARCostCalcSettings) || isEmpty(DSARCostCalcSettings)) {
                promiseArr.push(this.dsarSettingsService.getSettingsByGroup("CostCalculation"));
            } else {
                this.isCostCalcEnabled = DSARCostCalcSettings.isCostCalcEnabled;
                this.hourlyCost = DSARCostCalcSettings.hourlyCost;
                this.currencyIso = DSARCostCalcSettings.currencyIso;
            }
        }
        return Promise.all(promiseArr).then((response: Array<IProtocolResponse<any>>): boolean => {
            if (response[2] && response[2].result && response[2].data) {
                this.costCalculationSettings = response[2].data.settings;
                const costCalcEnabledSetting: IDSARSettings = find(this.costCalculationSettings, ["name", "Enabled"]);
                const costCalcHourlySetting: IDSARSettings = find(this.costCalculationSettings, ["name", "CostPerHour"]);
                const costCurrencySetting: IDSARSettings = find(this.costCalculationSettings, ["name", "Currency"]);
                this.isCostCalcEnabled = costCalcEnabledSetting.value === "true";
                this.hourlyCost = parseFloat(costCalcHourlySetting.value) || 0;
                this.currencyIso = costCurrencySetting.value;
                this.persistCostCalcSettingsLocally();
            }
            if (response[0] && response[0].result && response[0].data) {
                this.requestQueueDetails = response[0].data;
                this.buildDetails(this.requestQueueDetails);
            } else {
                this.requestQueueDetails = null;
            }
            if (response[1].result) {
                this.responseTemplates = response[1].data;
            }
            this.initialLoad = false;
            this.contentSource.next(this);
            return true;
        });
    }

    handleAction(type: string, payload: any): void {
        switch (type) {
            case RequestQueueActions.COMMENT:
                if (payload.isInternalComment) {
                    if (this.internalAttachments && this.internalAttachments.length) {
                        payload.attachments = this.internalAttachments;
                    }
                } else {
                    if (this.externalAttachments && this.externalAttachments.length) {
                        payload.attachments = this.externalAttachments;
                    }
                }
                this.addComment(payload.comment, payload.isInternalComment, payload.attachments);
                break;
            case RequestQueueActions.EXTEND_REQUEST:
                this.otModalService.create(
                        RequestQueueExtendModal,
                        {
                            isComponent: true,
                            size: ModalSize.MEDIUM,
                        },
                        {
                            comment: payload.data ? payload.data.comment : "",
                            requestQueueId: this.requestQueueDetails.requestQueueId,
                            responseTemplates: this.responseTemplates,
                        },
                    )
                    .pipe(
                        take(1),
                        filterResponse((response: boolean | null ) => response),
                    )
                    .subscribe(() => {
                        this.requestQueueService.getRequest(this.requestQueueId)
                            .then((res: IProtocolResponse<IRequestQueueDetails>): boolean => {
                                if (res.result) {
                                    this.historyService.updateHistory(this.requestQueueId);
                                    this.updateSingleRequest(res);
                                }
                                return res.result;
                            });

                    });
                break;
            case RequestQueueActions.REJECT_REQUEST:
                const rejectQueueModalData: IRequestQueueRejectModalResolve = this.rejectQueueModalData;
                rejectQueueModalData.requestQueueDetails = this.requestQueueDetails;
                rejectQueueModalData.data = {...payload.data};
                this.requestQueueService.getRequestRejectResolutions().pipe(
                    take(1),
                    concatMap((value: IRequestResolutionFormatted[]) => {
                        rejectQueueModalData.resolutionOptions = value;
                        if (this.externalAttachments && this.externalAttachments.length) {
                            rejectQueueModalData.data.attachments = this.externalAttachments;
                        }
                        return this.otModalService.create(
                            RequestQueueRejectModal,
                            {
                                isComponent: true,
                                size: ModalSize.SMALL,
                            },
                            rejectQueueModalData,
                        );
                    }),
                    take(1),
                    ).subscribe((modalReturnValue: boolean) => {
                        if (modalReturnValue) {
                            this.requestQueueService.getRequest(this.requestQueueId).then((res: IProtocolResponse<IRequestQueueDetails>) => {
                                if (res.result) {
                                    this.historyService.updateHistory(this.requestQueueId);
                                    this.updateSingleRequest(res);
                                }
                            });
                        }
                    });
                break;
            case RequestQueueActions.ASSIGN_REQUEST:
                const assignQueueModalData: IRequestQueueAssignModalResolve = this.assignQueueModalData;
                this.otModalService.create(
                        RequestQueueAssignModal,
                        {
                            isComponent: true,
                        },
                        {
                            ...assignQueueModalData,
                            requestQueueDetails: this.requestQueueDetails,
                        },
                    )
                    .subscribe((response: boolean) => {
                        if (response) {
                            this.requestQueueService.getRequest(this.requestQueueId)
                                .then((res: IProtocolResponse<IRequestQueueDetails>) => {
                                    if (res.result) {
                                        this.historyService.updateHistory(this.requestQueueId);
                                        this.updateSingleRequest(res);
                                    }
                                });
                        }
                    });
                break;
            case RequestQueueActions.DS_LANGUAGE_CHANGE:
                this.dsarSettingsService.getAllEnabledResponseTemplatesByRequestQueue(this.requestQueueId)
                    .then((res: IProtocolResponse<IResponseTemplate[]>): void => {
                        if (res.result) {
                            this.responseTemplates = res.data;
                            this.buildDetails(this.requestQueueDetails);
                        }
                });
                break;
            case RequestQueueActions.CLOSE_REQUEST:
                const closeQueueModalData: IDeleteConfirmationModalResolve = this.closeQueueModalData;
                closeQueueModalData.promiseToResolve = (): ng.IPromise<any> => {
                    const requestClose: IRequestQueueClose = {
                        requestQueueId: this.requestQueueId,
                        currentStatus: this.requestQueueDetails.currentStageInfo.status,
                        newStatus: RequestQueueStatus.Close,
                    };
                    return this.requestQueueService.closeRequestQueue(requestClose)
                    .then((res: IProtocolResponse<void>) => {
                        if (res.result) {
                            this.requestQueueService.getRequest(this.requestQueueId)
                                .then((response: IProtocolResponse<IRequestQueueDetails>) => {
                                    if (response.result) {
                                        this.historyService.updateHistory(this.requestQueueId);
                                        this.updateSingleRequest(response);
                                    }
                                    return response;
                            });
                        }
                        return res;
                    });
                };
                this.modalService.setModalData(closeQueueModalData);
                this.modalService.openModal("otDeleteConfirmationModal");
                break;
            case RequestQueueActions.REVOKE_ACCESS:
                const revokeAccessModalData: IDeleteConfirmationModalResolve = this.revokeAccessModalData;
                const revokeAccess: IRevokeAccessSubmit = {
                  requestQueueId: this.requestQueueDetails.requestQueueId,
                };
                revokeAccessModalData.promiseToResolve = (): ng.IPromise<boolean> => this.requestQueueService.revokeDsAccess(revokeAccess)
                    .then((response: IProtocolResponse<void>): boolean => {
                        this.requestQueueDetails.dsAccessRevoked = true;
                        this.contentSource.next(this);
                        return response.result;
                    });
                this.modalService.setModalData(revokeAccessModalData);
                this.modalService.openModal("otDeleteConfirmationModal");
                break;
            case RequestQueueActions.GRANT_ACCESS:
                const modalData: IGrantAccessModal = {
                    requestQueueId: this.requestQueueDetails.requestQueueId,
                    grantAccessCallback: (): void => {
                        this.requestQueueDetails.dsAccessRevoked = false;
                        this.contentSource.next(this);
                    },
                };
                this.modalService.setModalData(modalData);
                this.modalService.openModal("grantRequestAccess");
                break;
            case RequestQueueActions.UPDATE_REQUEST_ENTRIES:
                this.otModalService.create(
                    RequestQueueUpdateDetailsModal,
                    {
                        isComponent: true,
                    },
                    {
                        title: `${this.translatePipe.transform("EditRequest")} - ${this.requestQueueDetails.requestQueueRefId}`,
                        requestQueueDetails: this.requestQueueDetails,
                    },
                )
                .pipe(take(1))
                .subscribe((response: IProtocolResponse<IRequestQueueDetails> | null) => {
                    if (response && response.result) {
                        this.historyService.updateHistory(this.requestQueueId);
                        this.updateSingleRequest(response);
                    }
                });
                break;
            case RequestQueueActions.RESP_AS_COMPLETED:
                if (this.externalAttachments && this.externalAttachments.length) {
                    payload.data.attachments = this.externalAttachments;
                }
                this.isFirstPublicReply = this.requestQueueDetails.isFirstPublicReply;
                const completeQueueModalData: IConfirmationModalResolve = this.completeQueueModalData;
                completeQueueModalData.promiseToResolve = (): ng.IPromise<boolean> => {
                    const completeRequest: IRequestQueueComplete[] = [{
                        requestQueueId: this.requestQueueDetails.requestQueueId,
                        comment: payload.data.comment,
                        isInternalComment: false,
                        currentStatus: this.requestQueueDetails.currentStageInfo ? this.requestQueueDetails.currentStageInfo.status : this.requestQueueDetails.status,
                        newStatus: RequestQueueStatus.Complete,
                    }];
                    if (payload.data && payload.data.attachments) {
                        completeRequest[0].attachments = payload.data.attachments;
                    }
                    return this.requestQueueService.completeRequest(completeRequest).then((response: IProtocolResponse<IRequestQueueDetails>): IPromise<boolean> | boolean => {
                        if (response.result) {
                            return this.requestQueueService.getRequest(this.requestQueueId)
                                .then((res: IProtocolResponse<IRequestQueueDetails>) => {
                                    if (res.result) {
                                        this.historyService.updateHistory(this.requestQueueId);
                                        this.updateSingleRequest(res);
                                    }
                                    return !!res;
                                });
                        } else {
                            return false;
                        }
                    });
                };
                completeQueueModalData.successCallback = this.handleMFAFlow.bind(this);
                this.modalService.setModalData(completeQueueModalData);
                this.modalService.openModal("downgradeRequestQueueCompleteModal");
                break;
            case RequestQueueActions.ADD_ATTACHMENTS:
                const dsarAttachmentModalData: IDSARAttachmentModalResolve = {
                    isInternal: payload,
                    requestId: this.requestQueueId,
                    callback: (res: IProtocolResponse<any>): void => {
                        if (res.result) {
                            if (payload) {
                                this.internalAttachments.push(...res.data);
                                map(this.internalAttachments, (attachment: IAttachmentFileResponse): IAttachment => {
                                    attachment.name = attachment.FileName;
                                    attachment.icon = "ot ot-file";
                                    return attachment;
                                });
                            } else {
                                this.externalAttachments.push(...res.data);
                                map(this.externalAttachments, (attachment: IAttachmentFileResponse): IAttachment => {
                                    attachment.name = attachment.FileName;
                                    attachment.icon = "ot ot-file";
                                    return attachment;
                                });
                            }
                            this.contentSource.next(this);
                        }
                    },
                };
                this.modalService.setModalData(dsarAttachmentModalData);
                this.modalService.openModal("dsarAttachmentModal");
                break;
            case RequestQueueActions.DOWNLOAD_FILE:
                this.requestQueueService.downloadAttachment(payload.Id || payload.fileId).then((res: IProtocolResponse<any>): void => {
                    if (res.result) {
                        this.Export.basicFileDownload(res.data, payload.FileName || payload.fileName);
                    }
                });
                break;
            case RequestQueueActions.DELETE_FILE:
                const deleteAttachmentModalData: IDeleteConfirmationModalResolve = this.deleteAttachmentModalData;
                deleteAttachmentModalData.promiseToResolve = (): IPromise<any> => this.requestQueueService.deleteAttachment(payload.Id)
                    .then((res: IProtocolResponse<any>) => {
                            if (res.result && res.status === 204) {
                                if (payload.isInternal) {
                                    this.internalAttachments = filter(this.internalAttachments, (attachment: IAttachmentFileResponse): boolean => attachment.Id !== payload.Id);
                                } else {
                                    this.externalAttachments = filter(this.externalAttachments, (attachment: IAttachmentFileResponse): boolean => attachment.Id !== payload.Id);
                                }
                                this.contentSource.next(this);
                                return true;
                            }
                            this.contentSource.next(this);
                            return false;
                        });
                this.modalService.setModalData(deleteAttachmentModalData);
                this.modalService.openModal("deleteConfirmationModal");
                break;
            case RequestQueueActions.DELETE_FILE_FROM_COMMENT:
                const deleteFileAttachmentModalData: IDeleteConfirmationModalResolve = this.deleteAttachmentModalData;
                deleteFileAttachmentModalData.promiseToResolve = (): IPromise<any> => this.requestQueueService.deleteAttachment(payload.attachment.Id)
                    .then((res: IProtocolResponse<any>) => {
                            if (res.result && res.status === 204) {
                                const updatedComment: IRequestQueueCommentUpdate = {
                                    commentId: payload.activity.id,
                                    statusCode: RequestQueueCommentStatus.UpdateAttachments,
                                    comment: payload.activity.commentString,
                                    isInternalComment: payload.activity.isInternalComment,
                                    attachments: filter(payload.activity.attachments, (attachment: IAttachmentFileResponse): boolean => attachment.Id === payload.attachment.Id),
                                    // ^^^ send only deleted attachment ID to the BE ^^^
                                };
                                this.requestQueueService.updateComment(this.requestQueueId, updatedComment).then((response: IProtocolResponse<IRequestQueueDetails>): void => {
                                    this.updateSingleRequest(response);
                                });
                                return true;
                            }
                            return false;
                        });
                this.modalService.setModalData(deleteFileAttachmentModalData);
                this.modalService.openModal("deleteConfirmationModal");
                break;
            case RequestQueueActions.UPDATE_REQUEST:
                this.requestQueueDetails = {...payload.requestQueueDetails};
                break;
            case RequestQueueActions.UPDATE_VIEW:
                this.contentSource.next(this);
                break;
            case RequestQueueActions.MFA_PASSWORD:
                if (payload === "REGENERATED") {
                    this.otModalService.create(
                            MFAPasswordModal,
                            {
                                isComponent: true,
                            },
                            {
                                confirmationText: "MFAPasswordGeneratedDescription",
                                modalTitle: "RegeneratePassword",
                                modalCloseButton: "Close",
                                secondRequestPasswordFlow: true,
                                modalRegenerateButton: "ResetPassword",
                                requestQueueId: this.requestQueueDetails.requestQueueId,
                            },
                        );
                } else if (payload) {
                    this.otModalService.create(
                            MFAPasswordModal,
                            {
                                isComponent: true,
                            },
                            {
                                confirmationText: "MFARegenerateConfirmDescription",
                                modalTitle: "RegeneratePassword",
                                modalCloseButton: "Cancel",
                                regenerateFlow: true,
                                modalRegenerateButton: "Regenerate",
                                requestQueueId: this.requestQueueDetails.requestQueueId,
                            },
                        );
                } else {
                    this.otModalService.create(
                            MFAPasswordModal,
                            {
                                isComponent: true,
                            },
                            {
                                confirmationText: "MFAPasswordSentDescription",
                                modalTitle: "MessageSent",
                                firstRequestPasswordFlow: true,
                                modalCloseButton: "Cancel",
                                requestQueueId: this.requestQueueDetails.requestQueueId,
                                callback: () => {
                                    this.requestQueueDetails.passwordReGenerate = true;
                                    this.contentSource.next(this);
                                },
                            },
                        );
                }
                break;
            case RequestQueueActions.CHANGE_WORKFLOW:
                this.otModalService.create(
                        RequestQueueChangeWorkflow,
                        {
                            isComponent: true,
                        },
                        {
                            title: "ChangeWorkflow",
                            requestId: this.requestQueueDetails.requestQueueId,
                            workflowName: this.requestQueueDetails.workflow.name,
                            workflowId: this.requestQueueDetails.workflow.referenceId,
                            authEnabled: this.requestQueueDetails.authEnabled,
                        },
                    )
                    .pipe(take(1))
                    .subscribe((reload: boolean) => {
                        if (reload) {
                            this.stateService.reload();
                        }
                    });
                break;
            default:
                break;
        }
    }

    handleStatusChange(index: number): void {
        this.loadingStatusBarChanges = true;
        const statusBarPayload: IStringMap<number> = {
            currentStatus: this.requestQueueDetails.status,
            nextStatus: this.pathItems[index].status,
        };
        if (this.isUnverified) {
            if (!index) {
                this.updateStatusBar(statusBarPayload);
            } else {
                this.loadingStatusBarChanges = false;
            }
        } else if (this.pathItems[index].status === RequestQueueStatus.Complete) {
            const completeStatusModalData: IConfirmationModalResolve = {
                modalTitle: this.translatePipe.transform("Complete"),
                confirmationText: this.translatePipe.transform("TicketToCompletedStatus"),
                cancelButtonText: this.translatePipe.transform("Cancel"),
                submitButtonText: this.translatePipe.transform("Yes"),
                iconTemplate: `<span class="fa-stack fa-3x"><i class="fa fa-circle fa-stack-2x text-success"></i><i class="fa fa-check fa-stack-1x fa-inverse"></i></span>`,
                promiseToResolve: (): IPromise<boolean> => {
                    return this.updateStatusBar(statusBarPayload);
                },
                callback: (): void => {
                    this.loadingStatusBarChanges = false;
                    this.contentSource.next(this);
                },
            };
            this.modalService.setModalData(completeStatusModalData);
            this.modalService.openModal("confirmationModal");
        } else if (index < this.statusBarActiveIndex || this.isRejected || this.isClosed || this.isExpired) {
            const revertStatusModalData: IConfirmationModalResolve = {
                modalTitle: this.translatePipe.transform("RevertStatus"),
                confirmationText: this.translatePipe.transform("AreYouSureRevertStatus"),
                cancelButtonText: this.translatePipe.transform("Cancel"),
                submitButtonText: this.translatePipe.transform("Yes"),
                iconClass: "fa fa-exclamation-circle fa-2x text-warning",
                promiseToResolve: (): IPromise<boolean> => {
                    return this.updateStatusBar(statusBarPayload);
                },
                callback: (): void => {
                    this.loadingStatusBarChanges = false;
                    this.contentSource.next(this);
                },
            };
            this.modalService.setModalData(revertStatusModalData);
            this.modalService.openModal("confirmationModal");
        } else {
            this.updateStatusBar(statusBarPayload);
        }
    }

    getRequestQueueDetails(): IPromise<boolean> {
        return this.requestQueueService.getRequest(this.requestQueueId).then((res: IProtocolResponse<IRequestQueueDetails>): boolean => {
            if (res.result && res.data) {
                this.buildDetails(res.data);
            }
            return res.result;
        });
    }

    private persistCostCalcSettingsLocally(): void {
        const costCalcSettings: {
            isCostCalcEnabled: boolean;
            hourlyCost: number;
            currencyIso: string;
        } = {
            isCostCalcEnabled: this.isCostCalcEnabled,
            hourlyCost: this.hourlyCost,
            currencyIso: this.currencyIso,
        };
        sessionStorage.setItem("DSARCostCalcSettings", JSON.stringify(costCalcSettings));
    }

    private updateStatus() {
        this.isExpired = this.currentStatus === RequestQueueStatus.Expired;
        this.isUnverified = this.currentStatus === RequestQueueStatus.Unverified;
        this.isRejected = this.currentStatus === RequestQueueStatus.Rejected;
        this.isClosed = this.currentStatus === RequestQueueStatus.Close;
        this.isCompleted = this.currentStatus === RequestQueueStatus.Complete;
    }

    private buildDetails(requestQueueDetails: IRequestQueueDetails): void {
        this.internalAttachments = [];
        this.externalAttachments = [];
        this.clearComments = true;
        this.loadingStatusBarChanges = false;
        this.taskMode = requestQueueDetails.taskMode;
        this.requestQueueDetails = requestQueueDetails;
        this.currentStatus = requestQueueDetails.status;
        this.updateStatus();
        // disabling sidebar edit
        this.disableEdit = this.isCompleted || this.isRejected || this.isClosed ||
            this.isUnverified || this.isExpired;
        // enable subtasks dropdown and buttons
        this.showDropdown = !this.isRejected && !this.isClosed && !this.isUnverified && !this.isExpired;
        this.workflowList = sortBy(requestQueueDetails.workflow.workflowDetailDtos, "order");
        this.pathItems = map(this.workflowList, (item: IWorkflowItem) => {
            return {
                isActive: true,
                label: item.stage,
                guidanceText: item.guidanceText,
                status: item.status,
            };
        });
        if (requestQueueDetails.language) {
            this.languageList = requestQueueDetails.languageList;
            this.selectedLanguage = find(this.languageList, { Code: requestQueueDetails.language})["Name"];
        }
        this.sidebarRequestDetails = this.buildSidebarDetails(this.requestQueueDetails);
        this.headerRequestDetails = this.buildHeaderDetails(this.requestQueueDetails);
        // make the active index the last item since complete (status 80) will always be last
        if (this.isCompleted) {
            this.statusBarActiveIndex = this.pathItems.length - 1;
        } else if ((!this.isRejected) && (!this.isClosed)
                && (!this.isUnverified) && (!this.isExpired)) {
            this.statusBarActiveIndex = this.currentStatus;
        } else {
            this.statusBarActiveIndex = undefined;
        }
        this.subTasksAdded = false;
        this.updateStatusBarItems(this.statusBarActiveIndex);
        this.workflowStageChanged = false;
        if (!this.initialLoad) {
            this.contentSource.next(this);
        }
    }

    private updateStatusBarItems(index: number): void {
        for (let i = 0; i < this.pathItems.length; i++) {
            if (i <= this.statusBarActiveIndex) {
                this.pathItems[i].isActive = true;
            } else {
                this.pathItems[i].isActive = false;
            }
        }
        this.statusBarButtonDisabled = this.isRejected || this.isClosed || this.isUnverified || this.isExpired;
        this.loadingStatusBarChanges = false;
    }

    private buildSidebarDetails(requestDetails: IRequestQueueDetails): Array<IKeyValue<string | boolean | number>> {
        let deadline = "- - - -";
        if (!isNil(requestDetails.deadline) &&
            !this.isExpired && !this.isClosed &&
            !this.isCompleted && !this.isUnverified &&
            !this.isRejected) {
            let overrideDateFormatting: boolean;
            if (!isNil(this.timeStamp.currentDateFormatting)) {
                overrideDateFormatting = !this.timeStamp.currentDateFormatting.trim().length;
            }
            deadline = this.timeStamp.formatDate(requestDetails.deadline, overrideDateFormatting);
        }

        const sideBarArr: Array<IKeyValue<string | boolean | number>> = [
            {
                key: RequestQueueSidebar.Approver,
                value: requestDetails.reviewer || "- - - -",
            },
            {
                key: RequestQueueSidebar.ManagingOrganization,
                value: requestDetails.organization || "- - - -",
            },
            {
                key: RequestQueueSidebar.DateOpened,
                value: this.timeStamp.formatDate(requestDetails.dateCreated),
            },
            {
                key: RequestQueueSidebar.Extended,
                value: requestDetails.isExtended,
            },
            {
                key: RequestQueueSidebar.Deadline,
                value: deadline,
            },
        ];

        if (requestDetails.resolution) {
            sideBarArr.push({
                key: RequestQueueSidebar.Resolution,
                value: requestDetails.resolution,
            });
        }

        if (this.permissionsMap.DSARRequestQueueLanguageView && requestDetails.language) {
            sideBarArr.unshift({
                key: RequestQueueSidebar.PreferredLanguage,
                value: this.selectedLanguage,
            });
        }
        if (this.permissionsMap.DSARCostCalculationView && this.isCostCalcEnabled) {
            sideBarArr.unshift({
                key: CostCalc.TotalCost,
                value: requestDetails.hoursWorked * this.hourlyCost,
            });
            sideBarArr.unshift({
                key: CostCalc.HoursWorked,
                value: requestDetails.hoursWorked,
            });
        }
        return sideBarArr;
    }

    private buildHeaderDetails(requestDetails: IRequestQueueDetails): IHeaderDetailList[] {
        const headerDetailsArr: IHeaderDetailList[] = [
            {
                key: RequestQueueHeader.RequestId,
                value: requestDetails.requestQueueRefId,
                translate: true,
            },
            {
                key: RequestQueueHeader.RequestTypes,
                value: requestDetails.requestType,
                translate: true,
            },
            {
                key: RequestQueueHeader.SubjectTypes,
                value: requestDetails.subjectType,
                translate: true,
            },
            {
                key: RequestQueueHeader.Email,
                value: requestDetails.email,
                translate: true,
            },
        ];
        map(requestDetails.formFields, (field: any): void => {
            if (field.value) {
                let parsedValue = "";
                try {
                    parsedValue = JSON.parse(`"${field.value}"`);
                    headerDetailsArr.push({
                        key: field.fieldName,
                        value: parsedValue,
                        translate: false,
                    });
                } catch (err) {
                    headerDetailsArr.push({
                        key: field.fieldName,
                        value: field.value,
                        translate: false,
                    });
                }
            }
        });

        return headerDetailsArr;
    }

    private addComment(comment: string, isInternalComment: boolean, attachments?: IAttachmentFileResponse[]): void {
        if (isInternalComment) {
            this.postingComment = true;
            this.requestQueueService.editRequest(this.requestQueueDetails.requestQueueId, {
                comment,
                isInternalComment,
                statusCode: this.requestQueueDetails.status,
                attachments,
                taskMode: this.taskMode,
            }).then((response: IProtocolResponse<IRequestQueueDetails>): void => {
                if (response.result && response.data) {
                    this.buildDetails(response.data);
                }
                this.isCommentSuccessful = response.result;
                this.postingComment = false;
            });
        } else {
            this.isFirstPublicReply = this.requestQueueDetails.isFirstPublicReply;
            const replyModalData: IConfirmationModalResolve = this.replyModalData;
            replyModalData.promiseToResolve =
                (): IPromise<boolean> => this.requestQueueService.editRequest(this.requestQueueDetails.requestQueueId,
                    {
                        comment,
                        isInternalComment,
                        statusCode: this.requestQueueDetails.status,
                        attachments,
                    }).then((response: IProtocolResponse<IRequestQueueDetails>): boolean => {
                    if (response.result && response.data) {
                        this.buildDetails(response.data);
                    }
                    this.isCommentSuccessful = response.result;
                    return response.result;
                });
            replyModalData.successCallback = this.handleMFAFlow.bind(this);
            this.modalService.setModalData(replyModalData);
            this.modalService.openModal("confirmationModal");
        }
    }

    private updateSingleRequest(response: IProtocolResponse<IRequestQueueDetails>): void {
        if (response && response.result) {
            this.requestQueueDetails = assign(this.requestQueueDetails, response.data);
            this.requestQueueDetails = cloneDeep(this.requestQueueDetails);
            this.buildDetails(this.requestQueueDetails);
        }
    }

    private handleMFAFlow() {
        if (this.requestQueueDetails.authEnabled && // Message Sent Modal for first unique request
            !this.requestQueueDetails.passwordReGenerate &&
            this.isFirstPublicReply) {
            this.handleAction(RequestQueueActions.MFA_PASSWORD, null);
        } else if (this.requestQueueDetails.authEnabled && // REGENERATED Modal for second request
            this.requestQueueDetails.passwordReGenerate &&
            this.isFirstPublicReply) {
            this.handleAction(RequestQueueActions.MFA_PASSWORD, "REGENERATED");
        }
        this.isFirstPublicReply = this.requestQueueDetails.isFirstPublicReply;
    }

    private updateStatusBar(statusBarPayload: IStringMap<number>): IPromise<boolean> {
        return this.requestQueueService.updateStatusBar(this.requestQueueDetails.requestQueueId, statusBarPayload)
            .then((response: IProtocolResponse<void>): boolean => {
                if (response.result) {
                    this.notificationService.alertSuccess(this.translatePipe.transform("Success"), this.translatePipe.transform("StageChangedSuccessfully"));
                    this.workflowStageChanged = true;
                    this.requestQueueService.getRequest(this.requestQueueDetails.requestQueueId).then((res: IProtocolResponse<IRequestQueueDetails>): void => {
                        this.updateSingleRequest(res);
                        this.historyService.updateHistory(this.requestQueueId);
                        this.contentSource.next(this);
                    });
                } else {
                    this.loadingStatusBarChanges = false;
                    this.contentSource.next(this);
                }
                return response.result;
            });
    }

}
