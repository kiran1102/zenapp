// Angular
import { Injectable } from "@angular/core";

// 3rd Party
import { Observable, from } from "rxjs";
import { map } from "rxjs/operators";

// Constants
import { EmptyGuid } from "constants/empty-guid.constant";

// Interfaces
import { IStringMap } from "interfaces/generic.interface";
import { IWebformStyleResponse } from "modules/dsar/components/webforms/shared/interfaces/webform-styling-response.interface";
import { IProtocolPacket, IProtocolConfig, IProtocolMessages, IProtocolResponse} from "interfaces/protocol.interface";
import {
    IGetAllLanguageResponse,
    ISetLanguagePutData,
    IGetWebformLanguageResponse,
    ITenantTranslations,
} from "interfaces/dsar/dsar-edit-language.interface";
import {
    IWebformCreateResponse,
    IWebformCreate,
    IWebFormItem,
    IWebformAutoPublish,
} from "dsarModule/interfaces/webform.interface";
import { IPromise } from "angular";

// Services
import { Principal } from "modules/shared/services/helper/principal.service";
import { TranslateService } from "@ngx-translate/core";
import { ProtocolService } from "modules/core/services/protocol.service";

@Injectable()
export class WebformApiService {
    hrefString: string;
    private translations: IStringMap<string> = {};
    private translationKeys = [
        "WebForms",
        "Retrieving",
        "Disabling",
        "DefaultWebForm",
        "WebForm",
        "WebFormFields",
        "WebFormStylings",
        "WebFormSettings",
        "EditLanguageSuccess",
        "GetLanguageFailure",
        "EditLanguageFailure",
        "AllLanguages",
        "DefaultWebformError",
        "WebformFieldsError",
        "WebformStylingsError",
        "WebformPreviewError",
        "WebformDeleteError",
        "ProblemGettingYourCustomTranslations",
        "SorryProblemGettingTranslations",
    ];
    constructor(
        private OneProtocol: ProtocolService,
        private principalService: Principal,
        private translateService: TranslateService,
    ) {
        this.translateService
            .stream(this.translationKeys)
            .subscribe((values) => {
                this.translations = values;
        });
    }

    public getWebForms(orgGroupId: string): ng.IPromise<IProtocolResponse<IWebFormItem[]>> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("GET", `/api/datasubject/v1/webform/templates/${orgGroupId}`);
        const messages: IProtocolMessages = { Error: { object: this.translations["WebForms"], action: this.translations["Retrieving"] } };
        return this.OneProtocol.http(config, messages);
    }

    public migrateWebForms(): Observable<IWebformAutoPublish> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("GET", `/api/datasubject/v2/webform/migration`);
        const messages: IProtocolMessages = { Error: { object: this.translations["WebForms"], action: this.translations["Retrieving"] } };
        return from(this.OneProtocol.http(config, messages)).pipe(map((response: IProtocolResponse<IWebformAutoPublish>) => response.data));
    }
    public disableAutoPublishNotification(): Observable<IProtocolResponse<void>> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("GET", `/api/datasubject/v2/webform/migration/dismiss`);
        const messages: IProtocolMessages = { Error: { object: this.translations["WebForms"], action: this.translations["Disabling"] } };
        return from(this.OneProtocol.http(config, messages));
    }
    public getDefaultWebForm(): IPromise<IProtocolPacket> {
        const config: IProtocolConfig = this.OneProtocol.config("GET", `/webforms/Default`);
        const messages: IProtocolMessages = { Error: { object: this.translations["DefaultWebForm"], action: this.translations["Retrieving"] } };
        return this.OneProtocol.http(config, messages);
    }

    public getWebForm(webFormId: string): ng.IPromise<IProtocolPacket> {
        const config: IProtocolConfig = this.OneProtocol.config("GET", `/webforms/${webFormId}`);
        const messages: IProtocolMessages = { Error: { object: this.translations["WebForm"], action: this.translations["Retrieving"] } };
        return this.OneProtocol.http(config, messages);
    }

    public getWebFormById(templateId: string, tokenNotRequired = false): IPromise<IProtocolPacket> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("GET", `/api/datasubject/v1/webform/${templateId}`);
        config.tokenNotRequired = tokenNotRequired;
        const messages: IProtocolMessages = { Error: { object: this.translations["WebForm"], action: this.translations["Retrieving"] } };
        return this.OneProtocol.http(config, messages);
    }

    public getFormFields(templateId: string, tokenNotRequired = false): ng.IPromise<IProtocolPacket> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("GET", `/api/datasubject/v1/webform/${templateId}/fields`);
        config.tokenNotRequired = tokenNotRequired;
        const messages: IProtocolMessages = { Error: { object: this.translations["WebFormFields"], action: this.translations["Retrieving"] } };
        return this.OneProtocol.http(config, messages);
    }

    public getFormStylingInputs(templateId: string, tokenNotRequired = false): ng.IPromise<IProtocolResponse<IWebformStyleResponse>> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("GET", `/api/datasubject/v1/webform/${templateId}/styles`);
        config.tokenNotRequired = tokenNotRequired;
        const messages: IProtocolMessages = { Error: { object: this.translations["WebFormStylings"], action: this.translations["Retrieving"] } };
        return this.OneProtocol.http(config, messages);
    }

    public getSettingsInputs(templateId: string, tokenNotRequired = false): ng.IPromise<IProtocolPacket> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("GET", `/api/datasubject/v1/webform/${templateId}/settings`);
        config.tokenNotRequired = tokenNotRequired;
        const messages: IProtocolMessages = { Error: { object: this.translations["WebFormSettings"], action: this.translations["Retrieving"] } };
        return this.OneProtocol.http(config, messages);
    }

    public putLanguagePreferencesData(params: ISetLanguagePutData[], templateId: string): ng.IPromise<IProtocolResponse<IGetWebformLanguageResponse[]>> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("PUT", `/api/datasubject/v1/webform/language/${templateId}`, [], params);
        const messages: IProtocolMessages = {Success: {custom: this.translations["EditLanguageSuccess"] }, Error: {custom: this.translations["GetLanguageFailure"]}};
        return this.OneProtocol.http(config, messages);
    }

    public getSelectedLanguages(templateId: string): ng.IPromise<IProtocolResponse<IGetWebformLanguageResponse[]>> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("GET", `/api/datasubject/v1/webform/language/${templateId}`, []);
        const messages: IProtocolMessages = {Error: {custom: this.translations["EditLanguageFailure"]}};
        return this.OneProtocol.http(config, messages);
    }

    public getAllLanguages(): ng.IPromise<IProtocolResponse<IGetAllLanguageResponse[]>> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("GET", `/api/globalization/v1/languages/`, { tenantId: EmptyGuid, includeUntranslated: true});
        const messages: IProtocolMessages = { Error: { custom: this.translations["AllLanguages"]} };
        return this.OneProtocol.http(config, messages);
    }

    public updateDefaultWebForm(templateId: string, defaultWebForm: any): ng.IPromise<IProtocolPacket> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("PUT", `/api/datasubject/v1/webform/${templateId}`, [], defaultWebForm);
        const messages: IProtocolMessages = {
            Error: { custom: this.translations["DefaultWebformError"] },
        };
        return this.OneProtocol.http(config, messages);
    }

    public updateRequestTypes(templateId: string, requestTypes: any): ng.IPromise<IProtocolPacket> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("PUT", `/api/datasubject/v1/webform/${templateId}/requesttypes`, [], requestTypes);
        const messages: IProtocolMessages = {
            Error: { custom: this.translations["DefaultWebformError"] },
        };
        return this.OneProtocol.http(config, messages);
    }

    public updateSubjectTypes(templateId: string, subjectTypes: any): ng.IPromise<IProtocolPacket> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("PUT", `/api/datasubject/v1/webform/${templateId}/subjecttypes`, [], subjectTypes);
        const messages: IProtocolMessages = {
            Error: { custom: this.translations["DefaultWebformError"] },
        };
        return this.OneProtocol.http(config, messages);
    }

    public updateWebFormFields(templateId: string, formFields: any): ng.IPromise<IProtocolPacket> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("PUT", `/api/datasubject/v1/webform/${templateId}/fields`, [], formFields);
        const messages: IProtocolMessages = {
            Error: { custom: this.translations["WebformFieldsError"] },
        };
        return this.OneProtocol.http(config, messages);
    }

    public updateWebFormStylings(templateId: string, formStylingInputs: any): ng.IPromise<IProtocolPacket> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("PUT", `/api/datasubject/v1/webform/${templateId}/styles`, [], formStylingInputs);
        const messages: IProtocolMessages = {
            Error: { custom: this.translations["WebformStylingsError"] },
        };
        return this.OneProtocol.http(config, messages);
    }

    public updateWebFormSettings(templateId: string, settingsInputs: any): ng.IPromise<IProtocolPacket> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("PUT", `/api/datasubject/v1/webform/${templateId}/settings`, [], settingsInputs);
        const messages: IProtocolMessages = {
            Error: { custom: this.translations["WebformSettingsError"] },
        };
        return this.OneProtocol.http(config, messages);
    }

    public publishDefaultWebForm(templateId: string, isDraft: boolean, formLinkInfo?: any): ng.IPromise<IProtocolResponse<void>> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("POST", `/api/datasubject/v1/webform/${templateId}/publish?isDraft=${isDraft}`, [], formLinkInfo);
        const messages: IProtocolMessages = {
            Error: { custom: this.translations["WebformSettingsError"] },
        };
        return this.OneProtocol.http(config, messages);
    }

    public publishDefaultWebFormV2(templateId: string, isDraft: boolean, formLinkInfo?: any): ng.IPromise<IProtocolResponse<void>> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("POST", `/api/datasubject/v2/webform/${templateId}/publish?isDraft=${isDraft}`, [], formLinkInfo);
        const messages: IProtocolMessages = {
            Error: { custom: this.translations["WebformSettingsError"] },
        };
        return this.OneProtocol.http(config, messages);
    }

    public previewWebform(templateId: string): ng.IPromise<IProtocolResponse<string>> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("GET", `/api/datasubject/v2/webform/${templateId}/preview`, null, null, null, null, null, "text");
        const messages: IProtocolMessages = {
            Error: { custom: this.translations["WebformPreviewError"] },
        };
        return this.OneProtocol.http(config, messages);
    }

    public createNewWebForm(newWebFormData: IWebformCreate): ng.IPromise<IProtocolResponse<IWebformCreateResponse | string>> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("POST", `/api/datasubject/v1/webform/`, [], newWebFormData);
        const messages: IProtocolMessages = { Error: { Hide: true } };
        return this.OneProtocol.http(config, messages);
    }

    public deleteWebForm(templateId: string): ng.IPromise<IProtocolResponse<void>> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("DELETE", `/api/datasubject/v1/webform/${templateId}`);
        const messages: IProtocolMessages = {
            Error: { custom: this.translations["WebformDeleteError"] },
        };
        return this.OneProtocol.http(config, messages);
    }

    public getTranslations(templateId: string, language: string, tokenNotRequired = false): ng.IPromise<IProtocolResponse<any>> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("GET", `/api/datasubject/v1/webform/${templateId}/${language}`);
        config.tokenNotRequired = tokenNotRequired;
        const messages: IProtocolMessages = {
            Error: { custom: this.translations["ProblemGettingYourCustomTranslations"] },
        };
        return this.OneProtocol.http(config, messages);
    }

    public getTenantTranslations(language: string): ng.IPromise<IProtocolResponse<ITenantTranslations>> {
        const tenantId: string = this.principalService.getTenant() || EmptyGuid;
        const config: IProtocolConfig = this.OneProtocol.customConfig("GET", `/api/globalization/v1/translations/${language}`);
        const messages: IProtocolMessages = {
            Error: { custom: this.translations["SorryProblemGettingTranslations"] },
        };
        return this.OneProtocol.http(config, messages);
    }
}
