// 3rd party
import { Injectable } from "@angular/core";

// Constants
import {
    FilterOperators,
    OperatorValues,
    FilterLabels,
    FilterOperatorLabels,
    DSARFilterValueTypes,
} from "dsarModule/constants/dsar-filter.constants";
const CURRENT_APPROVER_ID_MAP_VALUE = "00000000-DDDD-0000-UUUU-000000000000";

// Services
import { TimeStamp } from "oneServices/time-stamp.service";
import { RequestQueueCacheService } from "dsarservice/request-queue-cache.service";
import { Permissions } from "sharedModules/services/helper/permissions.service";
import { TranslateService } from "@onetrust/common";

// Interfaces
import { IFilterGetData } from "dsarModule/interfaces/dsar-sidebar-drawer.interface";
import { IFilterField } from "dsarModule/interfaces/dsar-filters.interface";
import { IFilterValueOption } from "interfaces/filter.interface";
import {
    IFilterV2,
    IFilterOutput,
} from "modules/filter/interfaces/filter.interface";
import { ILabelValue } from "@onetrust/vitreus/src/app/interface/label-value.interface";
import { IUser } from "interfaces/user.interface";
import { EmptyGuid } from "constants/empty-guid.constant";

@Injectable()
export class RequestQueueFilterService {

    operators = {
        [FilterOperators.MultiSelect]: [
            {
                key: FilterOperatorLabels.EqualTo,
                value: OperatorValues.EqualTo,
            },
            {
                key: FilterOperatorLabels.NotEqualTo,
                value: OperatorValues.NotEqualTo,
            },
        ],
        [FilterOperators.Prepopulated]: [
            {
                key: FilterOperatorLabels.EqualTo,
                value: OperatorValues.EqualTo,
            },
            {
                key: FilterOperatorLabels.NotEqualTo,
                value: OperatorValues.NotEqualTo,
            },
        ],
        [FilterOperators.Date]: [
            {
                key: FilterOperatorLabels.Is,
                value: OperatorValues.Is,
            },
            {
                key: FilterOperatorLabels.IsAfter,
                value: OperatorValues.IsAfter,
            },
            {
                key: FilterOperatorLabels.IsBefore,
                value: OperatorValues.IsBefore,
            },
            {
                key: FilterOperatorLabels.IsBetween,
                value: OperatorValues.IsBetween,
            },
        ],
        [FilterOperators.DateRange]: [
            {
                key: FilterOperatorLabels.IsBetween,
                value: OperatorValues.IsBetween,
            },
        ],
        [FilterOperators.Number]: [
            {
                key: FilterOperatorLabels.EqualTo,
                value: OperatorValues.EqualTo,
            },
            {
                key: FilterOperatorLabels.NotEqualTo,
                value: OperatorValues.NotEqualTo,
            },
            {
                key: FilterOperatorLabels.GreaterThan,
                value: OperatorValues.GreaterThan,
            },
            {
                key: FilterOperatorLabels.LessThan,
                value: OperatorValues.LessThan,
            },
        ],
    };

    constructor(
        private timestamp: TimeStamp,
        private requestQueueCacheService: RequestQueueCacheService,
        private permissions: Permissions,
        private translateService: TranslateService,
    ) { }

    deserializeSelectedValues(selectedValues: IFilterValueOption[], filter: IFilterField): string[] | string {
        switch (filter.type) {
            case FilterOperators.Prepopulated:
            case FilterOperators.MultiSelect:
            case FilterOperators.Select:
                let fromValue: string[] = [];
                if ([FilterLabels.Stage, FilterLabels.SubjectType, FilterLabels.RequestType].includes(filter.name)) {
                    selectedValues.forEach((valueObj: IFilterValueOption): void => {
                        fromValue = fromValue.concat((valueObj.value as string).split(","));
                    });
                }  else {
                    fromValue = selectedValues.map((valueObj: IFilterValueOption): string => {
                        return valueObj.value as string;
                    });
                }
                return fromValue;
            case FilterOperators.Number:
                return selectedValues[0].key as string;
            case FilterOperators.Date:
            case FilterOperators.DateRange:
                return selectedValues[0].value as string;
            default:
                return;
        }
    }

    deserializeFilters(filters: IFilterOutput[], filterFields: IFilterField[]): IFilterGetData[] {
        if (!filters) return [];
        return (filters).map((selectedFilter: IFilterOutput): IFilterGetData => {
            const currentField: IFilterField = filterFields.find((field: IFilterField): boolean => field.name === selectedFilter.field.value);
            const selectedValues: IFilterValueOption[] = this.convertLabelValueToFilterValueOptions(selectedFilter.values);
            const filterResponse: IFilterGetData = {
                attributeKey: currentField.name,
                operator: selectedFilter.operator.value as string,
                dataType: currentField.type,
                fromValue: this.deserializeSelectedValues(selectedValues, currentField),
            };
            if (currentField.type === FilterOperators.DateRange) {
                    return {
                        ...filterResponse,
                        dataType: FilterOperators.Date,
                        toValue: selectedFilter.values[1].value as string,
                    };
                }
            if (currentField.name === FilterLabels.RemainingSubtasks) {
                return {
                    ...filterResponse,
                    fromValue: filterResponse.fromValue + "",
                };
            }
            return filterResponse;
        });
    }

    serializeFilters(filters: IFilterGetData[], filterFields: IFilterField[]): Array<IFilterV2<IFilterField, IFilterValueOption, IFilterValueOption>> {
        return filters.map((selectedFilter: IFilterGetData): IFilterV2<IFilterField, IFilterValueOption, IFilterValueOption> => {
            const currentFilter: IFilterField = filterFields.find((field: IFilterField): boolean => field.name === selectedFilter.attributeKey);
            const operator = this.getOperatorByValue(selectedFilter.operator, this.operators[currentFilter.type]);
            if (selectedFilter.operator === "BW" && selectedFilter.dataType === FilterOperators.Date) {
                currentFilter.type = FilterOperators.DateRange;
                selectedFilter.dataType = FilterOperators.DateRange;
            }
            const values = this.serializeSelectedValues(currentFilter, selectedFilter);
            return {
                field: currentFilter,
                operator,
                values,
                type: DSARFilterValueTypes[selectedFilter.dataType],
            };
        });
    }

    getOperatorByValue(value: string, options: IFilterValueOption[]): IFilterValueOption {
        return options.find((operator: IFilterValueOption) => operator.value === value);
    }

    formatFilter(filterList: IFilterGetData[]) {
        filterList.forEach((filter: IFilterGetData) => {
            if (filter.attributeKey === FilterLabels.Approver && filter.fromValue[0] === CURRENT_APPROVER_ID_MAP_VALUE) {
                (filter.fromValue as string[])[0] = this.requestQueueCacheService.currentUser.Id as string;
            }
        });
    }

    updateApproverOptions(list: IFilterValueOption[], filterFields: IFilterField[]): IFilterField[] {
        const filterIndex = filterFields.findIndex((column: IFilterField) => column.name === FilterLabels.Approver);
        let valueOptions = filterFields[filterIndex].valueOptions;
        valueOptions = list.map((item: IFilterValueOption) => {
            return {
                key: item.key,
                value: item.value,
            };
        });
        if (this.permissions.canShow("DSARViewAllRequests")) {
            if (valueOptions.length) {
                valueOptions.push({
                   key: EmptyGuid,
                   value: this.translateService.instant("Unassigned"),
               });
            }
        } else {
                const user: IUser = this.requestQueueCacheService.currentUser;
                valueOptions = [{
                    key: user.Id,
                    value: user.FullName,
                }];
        }
        filterFields[filterIndex].valueOptions = valueOptions;
        return filterFields;
    }

    private serializeSelectedValues(currentFilter: IFilterField, selectedFilter: IFilterGetData): IFilterValueOption[] {
        switch (selectedFilter.dataType) {
            case FilterOperators.Prepopulated:
            case FilterOperators.MultiSelect:
            case FilterOperators.Select:
                if ([FilterLabels.Stage, FilterLabels.SubjectType, FilterLabels.RequestType].includes(currentFilter.name)) {
                    const matchString = (selectedFilter.fromValue as string[]).join().toLowerCase();
                    const values = currentFilter.valueOptions.filter((option: IFilterValueOption) => {
                        return matchString.match((option.key as string).toLowerCase());
                    });
                    return values;
                }  else {
                    const values = currentFilter.valueOptions.filter((option: IFilterValueOption) => {
                        return (selectedFilter.fromValue as string[]).includes(option.key as string);
                    });
                    if (!values.length) {
                        return [currentFilter.valueOptions[0]];
                    }
                    return values;
                }
            case FilterOperators.Number:
                return [{
                    key: selectedFilter.fromValue as string,
                    value: selectedFilter.fromValue as string,
                }];
            case FilterOperators.Date:
                const fromValue = this.timestamp.formatDateWithoutTime(selectedFilter.fromValue as string);
                return [{
                        key: selectedFilter.fromValue as string,
                        value: fromValue as string,
                    }];
            case FilterOperators.DateRange:
                const fromValueDateRange = this.timestamp.formatDateWithoutTime(selectedFilter.fromValue as string);
                const toValueDateRange = this.timestamp.formatDateWithoutTime(selectedFilter.toValue as string);
                return [
                    {
                        key: selectedFilter.fromValue as string,
                        value: fromValueDateRange as string,
                    },
                    {
                        key: selectedFilter.toValue as string,
                        value: toValueDateRange as string,
                    },
                ];
        }
    }

    private convertLabelValueToFilterValueOptions(fieldValues: Array<ILabelValue<string | number>>): IFilterValueOption[] {
        return fieldValues.map((field: ILabelValue<string | number>): IFilterValueOption  => {
            return {
                key: field.label,
                value: field.value,
            };
        });
    }

}
