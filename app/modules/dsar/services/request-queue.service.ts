// Angular
import {
    Inject,
    Injectable,
} from "@angular/core";

// Rxjs
import {
    Subject,
    Observable,
    from,
} from "rxjs";
import { map } from "rxjs/operators";

// Constants
import { EmptyGuid } from "constants/empty-guid.constant";
import { WorkflowType } from "modules/intg/enums/workflow.enum";

// Services
import { RequestQueueHelper } from "dsarservice/request-queue-helper.service";
import { ProtocolService } from "modules/core/services/protocol.service";
import { TenantTranslationService } from "sharedServices/tenant-translation.service";
import { TaskPollingService } from "sharedServices/task-polling.service";
import { NotificationService } from "modules/shared/services/provider/notification.service";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

// Interfaces
import { IAttachmentFile } from "interfaces/attachment-file.interface";
import { IStringMap } from "interfaces/generic.interface";
import {
    IProtocolConfig,
    IProtocolMessages,
    IProtocolPacket,
    IProtocolResponse,
} from "interfaces/protocol.interface";
import {
    IRequestQueueAssign,
    IRequestQueueComplete,
    IRequestQueueExtend,
    IRequestQueueReject,
    IRequestQueueClose,
} from "dsarModule/interfaces/request-queue-actions.interface";
import {
    IRequestQueueParams,
    IRequestQueueDetails,
    IGrantAccessSubmit,
    IRevokeAccessSubmit,
    IGetAllRequests,
    IExport,
    IRequestChangeOrgModal,
    IRequestQueueSubTasks,
    IChangeWorkflowRequest,
    IRequestResolutionFormatted,
    IRequestResolution,
    ISavedViewRequest,
    ISavedView,
    ISavedViewResponse,
} from "dsarModule/interfaces/request-queue.interface";
import {
    IISystem,
    IIWorkflow,
} from "modules/intg/interfaces/integration.interface";
import {
    ISubTask,
    ISubTaskPaginationParams,
    ISubTaskRowTable } from "modules/dsar/components/sub-tasks/shared/interfaces/sub-tasks.interface";
import { IWebFormData, IWebFormSubmitData } from "dsarModule/interfaces/webform.interface";
import {
    IWorkflowRequest,
    IWorkflow,
    IWorkflowItem } from "interfaces/workflows.interface";
import {
    IGetPageableAudit,
} from "dsarModule/interfaces/audit-history.interface";
import {
    IUpdateDetailsPutData,
} from "dsarModule/interfaces/dsar-update-details-modal.interface";
import {
    IFilterData,
    IFilterGetData,
 } from "dsarModule/interfaces/dsar-sidebar-drawer.interface";
import { IGetAllLanguageResponse } from "interfaces/dsar/dsar-edit-language.interface";
import { IPageableOf } from "interfaces/pagination.interface";
import { IFilterField } from "../interfaces/dsar-filters.interface";

@Injectable()
export class RequestQueueService {

    eventsObservable: Observable<any>;
    eventsSource = new Subject();

    private readonly defaultParams: IRequestQueueParams = {
        page: 1,
        size: 20,
        sort: "requestQueueRefId,DESC",
    };

    constructor(
        readonly OneProtocol: ProtocolService,
        @Inject("TenantTranslationService") readonly tenantTranslationService: TenantTranslationService,
        private readonly requestQueueHelper: RequestQueueHelper,
        private readonly taskPollingService: TaskPollingService,
        private notificationService: NotificationService,
        readonly translatePipe: TranslatePipe,
    ) {
        this.eventsObservable = this.eventsSource.asObservable();
    }

    getRequests(params = this.defaultParams): ng.IPromise<IProtocolResponse<IPageableOf<IRequestQueueDetails>>> {
        const userSessionLanguage = this.tenantTranslationService.getCurrentLanguage();
        const config: IProtocolConfig = this.OneProtocol.customConfig("GET", `/api/datasubject/v1/request/${userSessionLanguage}`, params);
        const messages: IProtocolMessages = { Error: { object: this.translatePipe.transform("Requests"), action: this.translatePipe.transform("Retrieving")} };
        return this.OneProtocol.http(config, messages).then((response: IProtocolResponse<IPageableOf<IRequestQueueDetails>>): IProtocolResponse<IPageableOf<IRequestQueueDetails>> => {
            if (response.result && response.data && response.data.content) {
                this.requestQueueHelper.updatePermissions();
                response.data.content = this.requestQueueHelper.configureRequestData(response.data.content);
            }
            return response;
        });
    }

    public getSeededIntegrations(isTemplate: boolean, page: number = 0, size: number = 1000, sort = "name,asc"): Observable<IProtocolResponse<IPageableOf<IISystem>>> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("GET", `/integrationmanager/api/v1/integrations`, { isTemplate, page, size, sort });
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("CannotRetrieveIntegrations") } };
        return from(this.OneProtocol.http(config, messages));
    }

    getWorkflows(): Observable<IProtocolResponse<IPageableOf<IIWorkflow>>> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("GET", `/integrationmanager/api/v1/workflows?page=0&size=100&sort=name,asc&type=COMPOSITE&enabled=true`);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("CannotRetrieveWorkflows") } };
        return from(this.OneProtocol.http(config, messages));
    }

    public getRequestsBySearch(params = this.defaultParams, term = ""): ng.IPromise<IProtocolResponse<IPageableOf<IRequestQueueDetails>>> {
        const userSessionLanguage = this.tenantTranslationService.getCurrentLanguage();
        const config: IProtocolConfig = this.OneProtocol.customConfig("POST", `/api/datasubject/v1/request/search/${userSessionLanguage}`, params, { term });
        const messages: IProtocolMessages = { Error: { object: this.translatePipe.transform("Requests"), action: this.translatePipe.transform("Retrieving") } };
        return this.OneProtocol.http(config, messages).then((response: IProtocolResponse<IPageableOf<IRequestQueueDetails>>): IProtocolResponse<IPageableOf<IRequestQueueDetails>> => {
            if (response.result && response.data && response.data.content) {
                this.requestQueueHelper.updatePermissions();
                response.data.content = this.requestQueueHelper.configureRequestData(response.data.content);
            }
            return response;
        });
    }

    getRequest(requestId: string, language?: string): ng.IPromise<IProtocolResponse<IRequestQueueDetails>> {
        if (!language) {
            language = this.tenantTranslationService.getCurrentLanguage();
        }
        const config: IProtocolConfig = this.OneProtocol.customConfig("GET", `/api/datasubject/v1/request/${requestId}/${language}?translate=true`);
        const messages: IProtocolMessages = { Error: { Hide: true } };
        return this.OneProtocol.http(config, messages).then((response: IProtocolResponse<IRequestQueueDetails>): IProtocolResponse<IRequestQueueDetails> => {
            if (response.result && response.data) {
                response.data = this.requestQueueHelper.configureSingleRequestData(response.data);
            }
            return response;
        });
    }

    submitRequest(webFormId: string, data: IWebFormData, tokenNotRequired = false): ng.IPromise<IProtocolPacket> {
        const formData: FormData = new FormData();
        const jsonFormData: IWebFormSubmitData = {
            multiselectFields: data.form.multiselectFields,
            firstName: data.form.firstName,
            lastName: data.form.lastName,
            email: data.form.email,
            daysToRespond: data.form.daysToRespond,
            language: data.form.language,
            requestTypes: data.form.requestTypes,
            subjectTypes: data.form.subjectTypes,
            captchaId: null,
            captchaCode: null,
            otherData: data.form.otherData,
            fileName: null,
            name: null,
        };
        if (data.form.captchaId) {
            jsonFormData.captchaId = data.form.captchaId;
            jsonFormData.captchaCode = data.form.captchaCode;
        }
        if (data.attachment) {
            formData.append("DataStream", data.attachment);
            jsonFormData.name = data.form.attachment.Name;
            jsonFormData.fileName = data.form.attachment.FileName;
        }
        formData.append("attachment ", JSON.stringify(jsonFormData));

        const config: IProtocolConfig = this.OneProtocol.customConfig("POST", `/api/datasubject/v1/request/${webFormId}/request`, {}, formData, {}, {}, {}, "", 50000, true);
        const messages: IProtocolMessages = { Error: { object: this.translatePipe.transform("NewRequest").toLowerCase(), action: this.translatePipe.transform("submitting") } };
        return this.OneProtocol.http(config, messages);
    }

    editRequest(requestId: string, data: any): ng.IPromise<IProtocolPacket> {
        const userSessionLanguage = this.tenantTranslationService.getCurrentLanguage();
        const config: IProtocolConfig = this.OneProtocol.customConfig("POST", `/api/datasubject/v1/request/${requestId}/${userSessionLanguage}`, {}, data);
        const messages: IProtocolMessages = { Error: { object: this.translatePipe.transform("Requests").toLowerCase(), action: this.translatePipe.transform("updating") } };
        return this.OneProtocol.http(config, messages).then((response: IProtocolPacket): IProtocolPacket => {
            response.data = response.result && response.data ? this.requestQueueHelper.configureSingleRequestData(response.data) : response.data;
            return response;
        });
    }

    assignRequest(data: IRequestQueueAssign[]): ng.IPromise<IProtocolPacket> {
        data.forEach((assignData: IRequestQueueAssign) => {
            if (assignData.reviewer === "") {
                assignData.reviewer = EmptyGuid;
            }
        });
        const config: IProtocolConfig = this.OneProtocol.customConfig("PUT", `/api/datasubject/v1/request/assign`, {}, data);
        const messages: IProtocolMessages = { Error: { object: this.translatePipe.transform("Requests").toLowerCase(), action: this.translatePipe.transform("updating") } };
        return this.OneProtocol.http(config, messages);
    }

    extendRequest(data: IRequestQueueExtend[]): ng.IPromise<IProtocolResponse<void>> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("PUT", `/api/datasubject/v1/request/extend`, {}, data);
        const messages: IProtocolMessages = { Error: { object: this.translatePipe.transform("Requests").toLowerCase(), action: this.translatePipe.transform("updating") } };
        return this.OneProtocol.http(config, messages);
    }

    rejectRequest(data: IRequestQueueReject[]): ng.IPromise<IProtocolPacket> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("PUT", `/api/datasubject/v1/request/reject`, {}, data);
        const messages: IProtocolMessages = { Error: { object: this.translatePipe.transform("Requests").toLowerCase(), action: this.translatePipe.transform("updating") } };
        return this.OneProtocol.http(config, messages);
    }

    completeRequest(data: IRequestQueueComplete[]): ng.IPromise<IProtocolPacket> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("PUT", `/api/datasubject/v1/request/complete`, {}, data);
        const messages: IProtocolMessages = {
            Error: { Hide: true },
        };
        return this.OneProtocol.http(config, messages).then((res: IProtocolResponse<any>): IProtocolResponse<any> => {
            if (!res.result) {
                this.notificationService.alertError(this.translatePipe.transform("Error"), res.errors.detail === "UncompletedTask" ? this.translatePipe.transform("UncompletedTask") : this.translatePipe.transform("ErrorCompletingRequest"));
            }
            return res;
        });
    }

    addAttachment(file: IAttachmentFile): ng.IPromise<IProtocolPacket> {
        const fileData: FormData = new FormData();
        fileData.append("File", file.Blob);
        fileData.append("FileName", file.FileName);
        fileData.append("Name", file.Name);
        fileData.append("Type", "30");
        fileData.append("Encrypt", "true");
        fileData.append("IsInternal", `${file.IsInternal}`);
        fileData.append("FileRefId", file.RequestId);
        fileData.append("RefIds", file.RequestId);

        const config: IProtocolConfig = {
            method: "POST",
            url: "/file/addFile",
            body: fileData,
            multipartFormData: true,
        };
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("ErrorUploadingFiles") } };
        return this.OneProtocol.http(config, messages);
    }

    downloadAttachment(attachmentId: string): ng.IPromise<IProtocolPacket> {
        const config: IProtocolConfig = {
            method: "GET",
            url: `/file/downloadFile/${attachmentId}`,
            responseType: "arraybuffer",
        };
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("ErrorDownloadingFile") } };
        return this.OneProtocol.http(config, messages);
    }

    deleteAttachment(attachmentId: string): ng.IPromise<IProtocolPacket> {
        const config: IProtocolConfig = this.OneProtocol.config("DELETE", `/file/deleteFile/${attachmentId}`);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("ErrorDeletingFile") } };
        return this.OneProtocol.http(config, messages);
    }

    updateComment(requestId: string, data: any): ng.IPromise<IProtocolPacket> {
        const userSessionLanguage = this.tenantTranslationService.getCurrentLanguage();
        const config: IProtocolConfig = this.OneProtocol.customConfig("PUT", `/api/datasubject/v1/request/${requestId}/comment/${userSessionLanguage}`, {}, data);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("ErrorUpdatingComment") } };
        return this.OneProtocol.http(config, messages).then((response: IProtocolPacket): IProtocolPacket => {
            response.data = response.result && response.data ? this.requestQueueHelper.configureSingleRequestData(response.data) : response.data;
            return response;
        });
    }

    updateHoursWorked(requestId: string, data: IStringMap<number>): ng.IPromise<IProtocolResponse<void>> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("PUT", `/api/datasubject/v1/request/${requestId}/hours`, {}, data);
        const messages: IProtocolMessages = {
            Success: { custom: this.translatePipe.transform("TimeEntrySaved") },
            Error: { custom: this.translatePipe.transform("ErrorUpdatingHours") } };
        return this.OneProtocol.http(config, messages);
    }

    updateStatusBar(requestId: string, data: IStringMap<number>): ng.IPromise<IProtocolResponse<void>> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("PUT", `/api/datasubject/v1/request/${requestId}/status`, {}, data);
        const messages: IProtocolMessages = {
            Error: { Hide: true },
        };
        return this.OneProtocol.http(config, messages).then((res: IProtocolResponse<any>): IProtocolResponse<any> => {
            if (!res.result) {
                this.notificationService.alertError(
                    this.translatePipe.transform("Error"),
                    res.errors.code === "ERROR_DATASUBJECT-ACCESS_UNCOMPLETEDTASK" ?
                        this.translatePipe.transform("UncompletedTask") :
                        this.translatePipe.transform("ErrorUpdatingStatus"),
                    );
            }
            return res;
        });
    }

    addSubTask(requestId: string, data: ISubTask): ng.IPromise<IProtocolResponse<ISubTask>> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("POST", `/api/datasubject/v1/subtask/${requestId}`, {}, data);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("ErrorAddingSubTask") } };
        return this.OneProtocol.http(config, messages);
    }

    updateSubTask(taskId: string, data: ISubTask): ng.IPromise<IProtocolResponse<ISubTask>> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("PUT", `/api/datasubject/v1/subtask/${taskId}`, {}, data);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("ErrorUpdatingSubTask") } };
        return this.OneProtocol.http(config, messages);
    }

    getAllSubTasksPaginated(requestParam: ISubTaskPaginationParams, requestId: string, workflowList: IWorkflowItem[]): ng.IPromise<IProtocolResponse<ISubTaskRowTable>> {
        const requestBody = {
            ...requestParam,
        };
        const config: IProtocolConfig = this.OneProtocol.customConfig("GET", `/api/datasubject/v1/subtask/request/${requestId}`, requestBody);
        const messages: IProtocolMessages = {
            Error: { custom: "ErrorFetchingRecords" },
        };
        return this.OneProtocol.http(config, messages).then((response: IProtocolResponse<ISubTaskRowTable>): IProtocolResponse<ISubTaskRowTable> => {
            if (response.result && response.data) {
                response.data = this.requestQueueHelper.configureSubTaskData(response.data, workflowList);
            }
            return response;
        });
    }

    getAllSubTasks(requestId: string, workflowList: IWorkflowItem[]): ng.IPromise<IProtocolResponse<ISubTaskRowTable>> {
        const config: IProtocolConfig = this.OneProtocol.config("GET", `/subtask/GetAll/${requestId}`);
        const messages: IProtocolMessages = { Error: { Hide: true } };
        return this.OneProtocol.http(config, messages).then((response: IProtocolResponse<ISubTaskRowTable>): IProtocolResponse<ISubTaskRowTable> => {
            if (response.result && response.data) {
                response.data = this.requestQueueHelper.configureSubTaskData(response.data, workflowList);
            }
            return response;
        });
    }

    deleteSubTask(taskId: string): ng.IPromise<IProtocolResponse<void>> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("DELETE", `/api/datasubject/v1/subtask/${taskId}`);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("ErrorDeletingSubTask") } };
        return this.OneProtocol.http(config, messages);
    }

    completeSubTask(taskId: string, data: any): ng.IPromise<IProtocolResponse<ISubTask>> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("PUT", `/api/datasubject/v1/subtask/${taskId}/complete`, {}, data);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("ErrorCompletingSubTask") } };
        return this.OneProtocol.http(config, messages);
    }

    updateLanguage(requestId: string, language: string): ng.IPromise<IProtocolResponse<void>> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("PUT", `/api/datasubject/v1/request/${requestId}/language/${language}`);
        const messages: IProtocolMessages = {
            Success: { custom: this.translatePipe.transform("LanguageChangedSuccessfully") },
            Error: { custom: this.translatePipe.transform("ProblemUpdatingLanguage") },
        };
        return this.OneProtocol.http(config, messages);
    }

    deleteRequestQueue(queueId: string): ng.IPromise<IProtocolResponse<void>> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("DELETE", `/api/datasubject/v1/request/${queueId}`);
        const messages: IProtocolMessages = {
            Success: { custom: this.translatePipe.transform("RequestSuccessfullyDeleted")},
            Error: { custom: this.translatePipe.transform("ErrorDeletingRequest") },
        };
        return this.OneProtocol.http(config, messages);
    }

    revokeDsAccess(revokeDsAccess: IRevokeAccessSubmit): ng.IPromise<IProtocolResponse<void>> {
        const data: IRevokeAccessSubmit[] = [revokeDsAccess];
        const config: IProtocolConfig = this.OneProtocol.customConfig("DELETE", `/api/datasubject/v1/request/dsaccess`, {}, data);
        const messages: IProtocolMessages = {
            Success: { custom: this.translatePipe.transform("RevokedAccessSuccessfully")},
            Error: { custom: this.translatePipe.transform("ErrorRevokingAccess") },
        };
        return this.OneProtocol.http(config, messages);
    }

    getLatestWorkflow(language: string, workflowId: string): ng.IPromise<IProtocolResponse<IWorkflow>> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("GET", `/api/datasubject/v1/workflow/${workflowId}/${language}`);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("ErrorGettingWorkflows") } };
        return this.OneProtocol.http(config, messages);
    }

    getHistory(requestQueueId: string, page: number = 0, size: number = 8): ng.IPromise<IProtocolResponse<IGetPageableAudit>> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("GET", `/api/audit/v1/audit/${requestQueueId}/history/`, { page, size });
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("ErrorGettingHistory") } };
        return this.OneProtocol.http(config, messages);
    }

    // TODO : to be deleted
    editWorkflow(workflowId: string, language: string): ng.IPromise<IProtocolResponse<IWorkflow>> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("GET", `/datasubject/v1/workflow/createWorkflow/${workflowId}/${language}`);
        const messages: IProtocolMessages = { Error: { Hide: true } };
        return this.OneProtocol.http(config, messages);
    }

    saveWorkflow(workflowRequest: IWorkflowRequest): ng.IPromise<IProtocolResponse<IWorkflow>> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("PUT", `/api/datasubject/v1/workflow/detail`, null, workflowRequest);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("ErrorUpdatingWorkflows") } };
        return this.OneProtocol.http(config, messages);
    }

    grantDsAccess(grantDsAccess: IGrantAccessSubmit): ng.IPromise<IProtocolResponse<void>> {
        // TODO this method should take in an array of IGrantAccessSubmit
        const data: IGrantAccessSubmit[] = [grantDsAccess];
        const config: IProtocolConfig = this.OneProtocol.customConfig("PUT", `/api/datasubject/v1/request/dsaccess`, {}, data);
        const messages: IProtocolMessages = {
            Success: { custom: this.translatePipe.transform("GrantAccessSuccessfully")},
            Error: { custom: this.translatePipe.transform("ErrorGrantingAccess") },
        };
        return this.OneProtocol.http(config, messages);
    }

    closeRequestQueue(requestClose: IRequestQueueClose): ng.IPromise<IProtocolResponse<void>> {
        // TODO modify the method parameters to support an array
        const data: IRequestQueueClose[] = [requestClose];
        const config: IProtocolConfig = this.OneProtocol.customConfig("PUT", `/api/datasubject/v1/request/close`, {}, data);
        const messages: IProtocolMessages = {
            Success: { custom: this.translatePipe.transform("RequestSuccessfullyClosed")},
            Error: { custom: this.translatePipe.transform("ErrorClosingRequest") },
        };
        return this.OneProtocol.http(config, messages);
    }

    addWorkflowSubTask(data: ISubTask): ng.IPromise<IProtocolResponse<ISubTask>> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("POST", `/api/datasubject/v1/workflow/subtask`, null, data);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("ErrorAddingSubTask") } };
        return this.OneProtocol.http(config, messages);
    }

    updateWorkflowSubTask(data: ISubTask): ng.IPromise<IProtocolResponse<ISubTask>> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("PUT", `/api/datasubject/v1/workflow/subtask`, null, data);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("ErrorUpdatingSubTask") } };
        return this.OneProtocol.http(config, messages);
    }

    getAllWorkflowSubTasks(requestParam: ISubTaskPaginationParams, workflowStageId: string): ng.IPromise<IProtocolResponse<ISubTaskRowTable>> {
        const requestBody = {
            ...requestParam,
        };
        const config: IProtocolConfig = this.OneProtocol.customConfig("GET", `/api/datasubject/v1/workflow/subtask/all/${workflowStageId}`, requestBody);
        const messages: IProtocolMessages = { Error: { Hide: true } };
        return this.OneProtocol.http(config, messages).then((response: IProtocolResponse<ISubTaskRowTable>): IProtocolResponse<ISubTaskRowTable> => {
            if (response.result && response.data) {
                response.data = this.requestQueueHelper.configureSubTaskData(response.data);
            }
            return response;
        });
    }

    deleteWorkflowSubTask(taskId: string): ng.IPromise<IProtocolResponse<void>> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("DELETE", `/api/datasubject/v1/workflow/subtask/${taskId}`);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("ErrorDeletingSubTask") } };
        return this.OneProtocol.http(config, messages);
    }

    getUpdateDetails(templateId: string): ng.IPromise<IProtocolResponse<any>> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("GET", `/api/datasubject/v1/webform/${templateId}/requestqueue`);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("ErrorGettingUpdateRequest") } };
        return this.OneProtocol.http(config, messages);
    }

    updateRequestQueue(requestId: string, data: IUpdateDetailsPutData): ng.IPromise<IProtocolResponse<void>> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("PUT", `/api/datasubject/v1/request/${requestId}`, null, data);
        const messages: IProtocolMessages = {
            Success: { custom: this.translatePipe.transform("ChangesSuccessfullySaved")},
            Error: { custom: this.translatePipe.transform("ErrorUpdatingRequest") },
        };
        return this.OneProtocol.http(config, messages);
    }

    getDefaultRequestById(requestId: string, language: string, translate: boolean = true): ng.IPromise<IProtocolResponse<IRequestQueueDetails>> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("GET", `/api/datasubject/v1/request/${requestId}/${language}?translate=${translate}`);
        const messages: IProtocolMessages = { Error: { Hide: true } };
        return this.OneProtocol.http(config, messages);
    }

    getFilterData():  ng.IPromise<IProtocolResponse<IFilterField[]>> {
        const userSessionLanguage = this.tenantTranslationService.getCurrentLanguage();
        const config: IProtocolConfig = this.OneProtocol.customConfig("GET", `/api/datasubject/v1/request/filter/${userSessionLanguage}`);
        const messages: IProtocolMessages = { Error: { Hide: true } };
        return this.OneProtocol.http(config, messages);
    }

    getFilterMetaData(): Observable<IProtocolResponse<IFilterField[]>> {
        const userSessionLanguage = this.tenantTranslationService.getCurrentLanguage();
        const config: IProtocolConfig = this.OneProtocol.customConfig("GET", `/api/datasubject/v1/request/filter/${userSessionLanguage}`);
        const messages: IProtocolMessages = { Error: { Hide: true } };
        return from(this.OneProtocol.http(config, messages).then((response: IProtocolResponse<IFilterField[]>): IProtocolResponse<IFilterField[]> => {
            return response;
        }));
    }

    applyFilter(filterData: IFilterGetData[], params: IRequestQueueParams = this.defaultParams):  ng.IPromise<IProtocolResponse<IGetAllRequests>> {
        const userSessionLanguage = this.tenantTranslationService.getCurrentLanguage();
        const config: IProtocolConfig = this.OneProtocol.customConfig("POST", `/api/datasubject/v1/request/filter/${userSessionLanguage}`, params, { filterCriteria: filterData });
        const messages: IProtocolMessages = { Error: { Hide: true } };
        return this.OneProtocol.http(config, messages).then((response: IProtocolResponse<IGetAllRequests>): IProtocolResponse<IGetAllRequests> => {
            if (response.result && response.data && response.data.content) {
                response.data.content = this.requestQueueHelper.configureRequestData(response.data.content);
            }
            return response;
        });
    }

    changeDeadline(requestId: string, deadline: string):  ng.IPromise<IProtocolResponse<void>> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("PUT", `/api/datasubject/v1/request/${requestId}/deadline`, null, {deadline});
        const messages: IProtocolMessages = {
            Success: { custom: this.translatePipe.transform("DeadlineChangedSuccessfully")},
            Error: { custom: this.translatePipe.transform("ErrorUpdatingDeadline") },
        };
        return this.OneProtocol.http(config, messages);
    }

    getAllLanguages(): ng.IPromise<IProtocolResponse<IGetAllLanguageResponse[]>> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("GET", `/api/globalization/v1/languages/`, { tenantId: EmptyGuid, includeUntranslated: false});
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("AllLanguages")} };
        return this.OneProtocol.http(config, messages);
    }

    getExportFile(exportData: IExport): ng.IPromise<IProtocolResponse<void>> {
        this.taskPollingService.startPolling(5000);
        const config: IProtocolConfig = this.OneProtocol.customConfig("POST", `/api/datasubject/v1/request/export`, null, exportData.requestBody);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("ErrorRequestQueueExport")} };
        return this.OneProtocol.http(config, messages);
    }

    changeOrg(changeOrgData: IRequestChangeOrgModal): ng.IPromise<IProtocolResponse<void>> {
        if (changeOrgData.reviewerId === "") {
            changeOrgData.reviewerId = EmptyGuid;
        }
        const config: IProtocolConfig = this.OneProtocol.customConfig("PUT", `/api/datasubject/v1/request/${changeOrgData.requestQueueId}/organization`, null, changeOrgData);
        const messages: IProtocolMessages = {
            Success: { custom: this.translatePipe.transform("OrganizationChangedSuccessfully")},
            Error: { custom: this.translatePipe.transform("ErrorUpdatingOrganization") },
        };
        return this.OneProtocol.http(config, messages);
    }

    regeneratePassword(requestId: string, regenerate: boolean = false): ng.IPromise<IProtocolResponse<string>> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("GET", `/api/datasubject/v1/request/${requestId}/password`, {regenerate}, null, null, null, null, "text");
        const messages: IProtocolMessages = { Error: { Hide: true } };
        return this.OneProtocol.http(config, messages).then((response: IProtocolResponse<string>): IProtocolResponse<string> => {
            return response;
        });
    }

    getRequestQueueSubTasks(params = this.defaultParams): Observable<IProtocolResponse<IPageableOf<IRequestQueueSubTasks>>> {
        params.sort = null;
        const userSessionLanguage = this.tenantTranslationService.getCurrentLanguage();
        const config: IProtocolConfig = this.OneProtocol.customConfig("GET", `/api/datasubject/v1/subtask/all/${userSessionLanguage}`, params);
        const messages: IProtocolMessages = { Error: { Hide: true } };
        return from(this.OneProtocol.http(config, messages).then((response: IProtocolResponse<IPageableOf<IRequestQueueSubTasks>>): IProtocolResponse<IPageableOf<IRequestQueueSubTasks>> => {
            return response;
        }));
    }

    changeWorkflow(data: IChangeWorkflowRequest): Observable<IProtocolResponse<void>> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("PUT", `/api/datasubject/v1/request/workflow`, null, data);
        const messages: IProtocolMessages = { Error: { Hide: true } };
        return from(this.OneProtocol.http(config, messages).then((response: IProtocolResponse<void>): IProtocolResponse<void> => {
            return response;
        }));
    }

    getRequestRejectResolutions(): Observable<IRequestResolutionFormatted[]> {
        const userSessionLanguage = this.tenantTranslationService.getCurrentLanguage();
        const config: IProtocolConfig = this.OneProtocol.customConfig("GET", `/api/datasubject/v1/resolution/reject/${userSessionLanguage}`);
        return from(this.OneProtocol.http(config)).pipe(
            map((response) => {
                let formattedResolutionsForModal: IRequestResolutionFormatted[];
                if (response.result) {
                    const requestResolutions: IRequestResolution[] = response.data;
                    formattedResolutionsForModal = requestResolutions.map((value, index) => {
                        const resolution = {} as IRequestResolutionFormatted;
                        resolution.label = value.title;
                        resolution.value = index + 1;
                        resolution.id = value.id;
                        resolution.description = value.description;
                        return resolution;
                    });
                    formattedResolutionsForModal.sort((a: IRequestResolutionFormatted, b: IRequestResolutionFormatted) => a.label.charCodeAt(0) - b.label.charCodeAt(0));
                }
                return formattedResolutionsForModal;
            }),
        );
    }

    getSavedViews(viewsGetData: ISavedViewRequest): Observable<IProtocolResponse<ISavedViewResponse>> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("POST", `/api/datasubject/v1/uiviews/viewtypes/`, null, viewsGetData);
        const messages: IProtocolMessages = { Error: { Hide: true } };
        return from(this.OneProtocol.http(config, messages).then((response: IProtocolResponse<ISavedViewResponse>): IProtocolResponse<ISavedViewResponse> => {
            return response;
        }));
    }

    getSubTaskFilterData():  Observable<IProtocolResponse<IFilterField[]>> {
        const userSessionLanguage = this.tenantTranslationService.getCurrentLanguage();
        const config: IProtocolConfig = this.OneProtocol.customConfig("GET", `/api/datasubject/v1/subtask/filter/${userSessionLanguage}`);
        const messages: IProtocolMessages = { Error: { Hide: true } };
        return from(this.OneProtocol.http(config, messages).then((response: IProtocolResponse<IFilterField[]>): IProtocolResponse<IFilterField[]> => {
            return response;
        }));
    }

    applySubtaskFilter(filterData: IFilterGetData[], params: IRequestQueueParams = this.defaultParams): Observable<IProtocolResponse<IPageableOf<any>>> {
        const userSessionLanguage = this.tenantTranslationService.getCurrentLanguage();
        const config: IProtocolConfig = this.OneProtocol.customConfig("POST", `/api/datasubject/v1/subtask/filter/${userSessionLanguage}`, params, { filterCriteria: filterData });
        const messages: IProtocolMessages = { Error: { Hide: true } };
        return from(this.OneProtocol.http(config, messages).then((response: IProtocolResponse<IPageableOf<IRequestQueueSubTasks>>): IProtocolResponse<IPageableOf<IRequestQueueSubTasks>> => {
            return response;
        }));
    }

    getSavedView(viewId: string): Observable<IProtocolResponse<ISavedView>> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("GET", `/api/datasubject/v1/uiviews/${viewId}`, null);
        const messages: IProtocolMessages = { Error: { Hide: true } };
        return from(this.OneProtocol.http(config, messages).then((response: IProtocolResponse<ISavedView>): IProtocolResponse<ISavedView> => {
            return response;
        }));
    }

    getAllResolutions(): Observable<IProtocolResponse<IRequestResolution[]>> {
        const userSessionLanguage = this.tenantTranslationService.getCurrentLanguage();
        const config: IProtocolConfig = this.OneProtocol.customConfig("GET", `/api/datasubject/v1/resolution/${userSessionLanguage}`);
        return from(this.OneProtocol.http(config));
    }
}
