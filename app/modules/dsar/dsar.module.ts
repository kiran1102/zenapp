// 3rd Party
import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";

// Modules
import { SharedModule } from "sharedModules/shared.module";
import { UpgradedLegacyModule } from "sharedModules/upgrade/upgraded-legacy.module";
import { CommonModule } from "@angular/common";
import { HttpClientModule } from "@angular/common/http";
import { FormsModule } from "@angular/forms";
import { ReactiveFormsModule } from "@angular/forms";
import { VitreusModule } from "@onetrust/vitreus";
import { DndListModule } from "ngx-drag-and-drop-lists";
import { OTPipesModule, PIPES } from "modules/pipes/pipes.module";
import { VirtualScrollModule } from "od-virtualscroll";
import { OneVideoModule } from "modules/video/video.module";
import { DsarPipesModule, PIPES as DsarPipes } from "dsarModule/pipes/pipes.module";
import { DsarComponentsModule } from "@onetrust/dsar-components";
import { DSARDashboardModule } from "./dsar-dashboard/dsar-dashboard.module";
import { OtOrgGroupUserModule } from "sharedModules/components/ot-org-group-user/ot-org-group-user.module";
import { FilterModule } from "modules/filter/filter.module";
import { TranslateModule } from "@ngx-translate/core";

// Services
import { WebformStore } from "./webform.store";
import { RequestQueueService } from "dsarservice/request-queue.service";
import { WebformApiService } from "dsarservice/webform-api.service";
import { RequestQueueHistoryService } from "dsarservice/request-queue-history.service";
import { DSARWorkflowService } from "dsarservice/dsar-workflows.service";
import { DSARSettingsService } from "dsarservice/dsar-settings.service";
import { RequestQueueDropdownService } from "dsarservice/request-queue-dropdown.service";
import { DsarWebformRulesApiService } from "dsarModule/services/dsar-webform-rules-api.service";
import { RequestQueueDetailService } from "dsarservice/request-queue-detail-service";
import { RequestQueueHelper } from "dsarservice/request-queue-helper.service";
import { RequestQueueFilterService } from "dsarModule/services/request-queue-filter.service";
import { OneRulesHelperService } from "./components/webforms/shared/services/one-rules-helper.service";
import { RequestQueueCacheService } from "dsarservice/request-queue-cache.service";

// Components
import { WebFormSidebarComponent } from "./components/webforms/sidebar/webform-sidebar.component";
import { WebformEditBlockComponent } from "./components/webforms/webform-edit-block/webform-edit-block.component";
import { WebFormRequestTypeContentComponent } from "./components/webforms/accordion-content/request-type-content/webform-request-type-content.component";
import { WebformFormTextContent } from "dsarModule/components/webforms/accordion-content/form-text-content/webform-form-text-content.component";
import { WebformTextModalComponent } from "dsarModule/components/webforms/accordion-content/webform-text-modal/webform-text-modal.component";
import { DSARPublishFormConfirmationModal } from "dsarModule/components/webforms/publish-confirmation-modal/dsar-publish-form-confirmation-modal.component";
import { DsarResponseTemplatesTable } from "dsarModule/components/response-templates/table/dsar-response-templates-table.component";
import { RequestQueueStatusBar } from "dsarModule/components/request-queue/detail-status-bar/request-queue-status-bar.component";
import { SubTasksTable } from "dsarModule/components/sub-tasks/sub-tasks-table/sub-tasks-table.component";
import { WebformEditLanguageComponent } from "dsarModule/components/webforms/webform-edit-language/webform-edit-language.component";
import { GrantRequestAccessComponent } from "dsarModule/components/request-queue/grant-access/grant-request-access.component";
import { DSARRequestQueueComponent } from "dsarModule/components/request-queue/dsar-request-queue.component.ts";
import { RequestQueueHistoryComponent } from "dsarModule/components/request-queue/detail-history/request-queue-history.component";
import { RequestQueueTable } from "dsarModule/components/request-queue/table/request-queue-table.component";
import { RequestDeadlineChangeComponent } from "dsarModule/components/request-queue/deadline-change-modal/request-deadline-change.component";
import { WorkflowsListTable } from "dsarModule/components/workflows/list-table/dsar-workflows-list-table.component";
import { WorkflowsList } from "dsarModule/components/workflows/list/dsar-workflows-list.component";
import { DSARWorkflowDetailComponent } from "modules/dsar/components/workflows/detail/dsar-workflow-detail.component";
import { DsarResponseTemplatesComponent } from "dsarModule/components/response-templates/dsar-response-templates.component";
import { DsarResponseSubTaskModal } from "./components/response-templates/dsar-response-sub-task-modal/dsar-response-sub-task-modal.component";
import { DsarWebformAutoPublishModal } from "dsarModule/components/webforms/dsar-webform-auto-publish-modal/dsar-webform-auto-publish-modal.component";
import { ResponseTemplateTabs } from "dsarModule/components/response-templates/tabs/dsar-response-templates-tabs.component";
import { ResponseTemplateSettings } from "dsarModule/components/response-templates/settings/dsar-response-templates-settings.component";
import { DSARSaveResponseTemplateModalComponent } from "dsarModule/components/response-templates/save-modal/dsar-response-templates-save-modal.component";
import { DsarWorkFlowSettings } from "dsarModule/components/workflows/settings/dsar-workflow-settings.component";
import { DsarEditWorkflowComponent} from "dsarModule/components/workflows/edit-workflow/dsar-edit-workflow.component";
import { WorkflowCreateModalComponent } from "dsarModule/components/workflows/create-modal/dsar-workflow-create-modal.component";
import { DSARWebformSettingsComponent } from "dsarModule/components/webforms/webform-settings/dsar-webform-settings.component";
import { DsarWebformRulesComponent } from "dsarModule/components/webforms/webform-rules/dsar-webform-rules.component";
import { RequestQueueRejectModal } from "dsarModule/components/request-queue/reject-modal/request-queue-reject-modal.component";
import { RequestQueueCompleteModal } from "dsarModule/components/request-queue/complete-modal/request-queue-complete-modal.component";
import { RequestQueueSidebarDetails } from "dsarModule/components/request-queue/detail-sidebar/dsar-request-queue-detail-sidebar.component";
import { RequestQueueAssignModal } from "dsarModule/components/request-queue/assign-modal/request-queue-assign-modal.component";
import { GenerateScriptModal } from "dsarModule/components/webforms/generate-script-modal/generate-script-modal.component";
import { RequestQueueChangeOrgModal } from "dsarModule/components/request-queue/change-org/request-queue-change-org-modal.component";
import { DsarSaveSystemTaskModal } from "dsarModule/components/sub-tasks/dsar-save-system-task-modal/dsar-save-system-task-modal.component";
import { RequestQueueExtendModal } from "dsarModule/components/request-queue/extend-modal/request-queue-extend-modal.component";
import { RequestQueueDetailPanelComponent } from "modules/dsar/components/request-queue/detail-panel/dsar-request-queue-detail-panel.component";
import { DSARRequestQueueDetailComponent } from "modules/dsar/components/request-queue/detail/dsar-request-queue-details.component";
import { DSARRequestQueueDetailBodyComponent } from "modules/dsar/components/request-queue/detail-body/dsar-request-queue-detail-body.component";
import { WebformPreviewComponent } from "dsarModule/components/webforms/webform-preview/webform-preview.component";
import { RequestQueueDetailMessagingComponent } from "dsarModule/components/request-queue/detail-messaging/request-queue-detail-messaging.component";
import { DsarSubTasksComponent } from "dsarModule/components/sub-tasks/dsar-sub-tasks.component";
import { FormFieldTypeConfigureComponent } from "modules/dsar/components/webforms/sidebar/sidebar-edit/form-field-type-configure.component";
import { FormFieldConfigureComponent } from "modules/dsar/components/webforms/sidebar/sidebar-edit/form-field-configure.component";
import { MFAPasswordModal } from "dsarModule/components/request-queue/mfa-password-modal/mfa-password-modal.component";
import { DSARWebFormsComponent } from "dsarModule/components/webforms/dsar-webform/dsar-webforms.component";
import { WebformTileComponent } from "dsarModule/components/webforms/webform-tile/webform-tile.component";
import { WebformAttachmentSettingsComponent } from "modules/dsar/components/webforms/accordion-content/attachment-settings/webform-attachment-settings.component";
import { DSARRequestQueueWrapperComponent } from "dsarModule/components/request-queue/request-queue-wrapper/dsar-request-queue-wrapper.component";
import { DsarWorkflowSubTasksComponent } from "dsarModule/components/workflows/sub-tasks/dsar-workflow-sub-tasks.component";
import { DSARRequestQueueSubTasksComponent } from "dsarModule/components/sub-tasks/dsar-request-queue-sub-tasks/dsar-request-queue-sub-tasks.component";
import { DSARRequestQueueSubTasksTable } from "dsarModule/components/sub-tasks/dsar-request-queue-sub-tasks/sub-tasks-table/dsar-request-queue-sub-tasks-table.component";
import { RequestQueueChangeWorkflow } from "dsarModule/components/request-queue/change-workflow/request-queue-change-workflow.component";
import { DSARAddNewWebformComponent } from "dsarModule/components/webforms/add-new-webform/dsar-add-new-webform.component";
import { RequestQueueSubTasksFilterComponent } from "dsarModule/components/request-queue/sub-tasks-filter/request-queue-sub-tasks-filter.component";
import { RequestQueueFilterComponent } from "dsarModule/components/request-queue/request-queue-filter/request-queue-filter.component";
import { OneRulesComponent } from "dsarModule/components/webforms/shared/components/one-rules/rules/one-rules.component";
import { OneRuleComponent } from "dsarModule/components/webforms/shared/components/one-rules/rule/one-rule.component";
import { OneSubRuleComponent } from "dsarModule/components/webforms/shared/components/one-rules/sub-rule/one-sub-rule.component";
import { OneRuleConditionComponent } from "dsarModule/components/webforms/shared/components/one-rules/rule-condition/one-rule-condition.component";
import { OneRuleActionComponent } from "dsarModule/components/webforms/shared/components/one-rules/action/one-rule-action.component";
import { RequestQueueUpdateDetailsModal } from "dsarModule/components/request-queue/update-details-modal/request-queue-update-details-modal.component";
import { CUSTOM_ELEMENTS_SCHEMA } from "@angular/core";
import { DsarEditableTextComponent } from "./components/webforms/shared/components/dsar-editable-text/dsar-editable-text.component";
import { WebformAddNewInputComponent } from "./components/webforms/webform-inputs-manager/webform-add-new-input/webform-add-new-input.component";
import { WebformInputsManagerComponent } from "./components/webforms/webform-inputs-manager/webform-inputs-manager.component";
import { WebformFormFieldsContentComponent } from "./components/webforms/accordion-content/form-fields-content/webform-form-fields-content.component";
import { WebformFormStylingContentComponent } from "./components/webforms/accordion-content/form-styling-content/webform-form-styling-content.component";
import { WebformSidebarEditComponent } from "dsarModule/components/webforms/sidebar/sidebar-edit/webform-sidebar-edit.component.ts";
import { PrepublishConfirmationModal } from "./components/webforms/prepublish-confirmation-modal/prepublish-confirmation-modal.component";

export function getDSARSettingsService(injector) {
    return injector.get("DSARSettingsService");
}

export function getDSARWebformStore(injector) {
    return injector.get("DSARWebFormStore");
}

export function getDSARRequestDetailService(injector) {
    return injector.get("RequestQueueDetailService");
}

// TODO: Evaluate what needs to be an entryComponent and not
const COMPONENTS_TO_BE_DOWNGRADED = [
    WebformFormStylingContentComponent,
    WebFormSidebarComponent,
    OneRulesComponent,
    OneRuleComponent,
    OneSubRuleComponent,
    OneRuleConditionComponent,
    OneRuleActionComponent,
    DsarResponseTemplatesTable,
    DsarEditableTextComponent,
    WebFormRequestTypeContentComponent,
    WebformAddNewInputComponent,
    WebformInputsManagerComponent,
    WebformFormFieldsContentComponent,
    WebformEditBlockComponent,
    WebformSidebarEditComponent,
    WebformTextModalComponent,
    RequestQueueStatusBar,
    SubTasksTable,
    WebformEditLanguageComponent,
    GrantRequestAccessComponent,
    DSARRequestQueueComponent,
    RequestQueueHistoryComponent,
    RequestQueueTable,
    RequestDeadlineChangeComponent,
    WorkflowsListTable,
    WorkflowsList,
    DsarResponseTemplatesComponent,
    ResponseTemplateTabs,
    ResponseTemplateSettings,
    DSARSaveResponseTemplateModalComponent,
    DsarWorkFlowSettings,
    DsarEditWorkflowComponent,
    DSARPublishFormConfirmationModal,
    WebformFormTextContent,
    WorkflowCreateModalComponent,
    DSARWebformSettingsComponent,
    DsarWebformRulesComponent,
    RequestQueueRejectModal,
    RequestQueueCompleteModal,
    RequestQueueSidebarDetails,
    RequestQueueAssignModal,
    GenerateScriptModal,
    RequestQueueChangeOrgModal,
    DsarSaveSystemTaskModal,
    RequestQueueExtendModal,
    RequestQueueDetailPanelComponent,
    DSARRequestQueueDetailComponent,
    DSARRequestQueueDetailBodyComponent,
    WebformPreviewComponent,
    DsarResponseSubTaskModal,
    DsarWebformAutoPublishModal,
    DsarSubTasksComponent,
    FormFieldTypeConfigureComponent,
    FormFieldConfigureComponent,
    RequestQueueDetailMessagingComponent,
    MFAPasswordModal,
    DSARWebFormsComponent,
    WebformTileComponent,
    WebformAttachmentSettingsComponent,
    DSARRequestQueueWrapperComponent,
    DSARRequestQueueSubTasksComponent,
    DSARRequestQueueSubTasksTable,
    DsarWorkflowSubTasksComponent,
    RequestQueueChangeWorkflow,
    DSARAddNewWebformComponent,
    DSARWorkflowDetailComponent,
    RequestQueueUpdateDetailsModal,
    PrepublishConfirmationModal,
];

@NgModule({
    imports: [
        SharedModule,
        TranslateModule,
        UpgradedLegacyModule,
        CommonModule,
        HttpClientModule,
        FormsModule,
        ReactiveFormsModule,
        VitreusModule,
        DndListModule,
        OTPipesModule,
        VirtualScrollModule,
        OneVideoModule,
        RouterModule,
        DsarPipesModule,
        DsarComponentsModule,
        DSARDashboardModule,
        FilterModule,
        OtOrgGroupUserModule,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA],
    declarations: [
        ...COMPONENTS_TO_BE_DOWNGRADED,
        RequestQueueSubTasksFilterComponent,
        RequestQueueFilterComponent,
    ],
    entryComponents: COMPONENTS_TO_BE_DOWNGRADED,
    providers: [
        WebformApiService,
        {
            provide: DSARSettingsService,
            deps: ["$injector"],
            useFactory: getDSARSettingsService,
        },
        WebformStore,
        {
            provide: RequestQueueDetailService,
            deps: ["$injector"],
            useFactory: getDSARRequestDetailService,
        },
        RequestQueueHistoryService,
        DsarWebformRulesApiService,
        RequestQueueDetailService,
        DSARWorkflowService,
        RequestQueueDropdownService,
        RequestQueueService,
        RequestQueueHelper,
        RequestQueueFilterService,
        OneRulesHelperService,
        RequestQueueCacheService,
        ...PIPES,
        ...DsarPipes,
    ],
})
export class DSARModule {}
