// Angular
import {
    Component,
    Inject,
    OnDestroy,
    OnInit,
    Input,
} from "@angular/core";

// 3rd Party
import { StateService } from "@uirouter/core";
import { TranslateService } from "@ngx-translate/core";

// Rxjs
import { Subject } from "rxjs";
import {
    startWith,
    takeUntil,
} from "rxjs/operators";

// Vitreus
import {
    OtModalService,
    ModalSize,
} from "@onetrust/vitreus";

// Pipes

// Redux
import observableFromStore from "oneRedux/utils/observable-from-store";
import { getCurrentUser } from "oneRedux/reducers/user.reducer";
import {
    isAssessmentDetailWelcomeSection,
    isAssessmentDetailFirstSection,
    isAssessmentDetailLastSection,
    getAssessmentDetailCanSubmit,
    getAssessmentDetailModel,
    getAssessmentDetailSelectedSectionId,
    isSubmittingAssessment,
    getAssessmentDetailStatus,
    getQuestionIdsBeingSaved,
    getQuestionIdsToSave,
    getSubQuestionIdsBeingSaved,
    getSubQuestionIdsToSave,
    isAssessmentReady,
    isReadinessTemplate,
    getAssessmentDetailAssessmentId,
    getAssessmentDetailApprovers,
    assessmentHasInvalidQuestions,
    getAssessmentDetailCanApprove,
    isDINATemplate,
} from "modules/assessment/reducers/assessment-detail.reducer";

// Enums / Constants
import { EmptyGuid } from "constants/empty-guid.constant";
import {
    AssessmentStatus,
    SendBackType,
} from "enums/assessment/assessment-status.enum";
import { AASectionFilterTypes } from "modules/assessment/enums/aa-filter.enum";

// Interfaces
import {
    IStore,
    IStoreState,
} from "interfaces/redux.interface";
import { IAssessmentApprover } from "interfaces/assessment/assessment-model.interface";
import { IChangedQuestion } from "interfaces/assessment/assessment-model.interface";
import { ILabelValue } from "interfaces/generic.interface";

// Services
import { StoreToken } from "tokens/redux-store.token";
import { Permissions } from "modules/shared/services/helper/permissions.service";
import { AssessmentDetailService } from "modules/assessment/services/view-logic/assessment-detail.service";
import { AaDetailActionService } from "modules/assessment/services/action/aa-detail-action.service";
import { AaSectionNavMessagingService } from "assessmentServices/messaging/aa-section-messaging.service";
import { AssessmentDetailViewScrollerService } from "modules/assessment/services/view-scroller/assessment-detail-view-scroller.service";
import { NotificationService } from "sharedModules/services/provider/notification.service";

// Component
import { AaReviewChangedResponseComponent } from "modules/assessment/components/aa-review-changed-responses-modal/aa-review-changed-responses.component";
import { AaFinishReviewModalComponent } from "modules/assessment/components/aa-finish-review-modal/aa-finish-review-modal.component";
import { AAUndoReviewModalComponent } from "modules/assessment/components/aa-undo-review-modal/aa-undo-review-modal.component";

@Component({
    selector: "aa-section-footer",
    templateUrl: "./aa-section-footer.component.html",
})

export class AASectionFooterComponent implements OnInit, OnDestroy {

    @Input() approverSendBackFlow: boolean;
    @Input() openInfoRequestMsg: string;

    assessmentDetailStatus: string;
    assessmentStatus: typeof AssessmentStatus;
    sendBackType: typeof SendBackType;
    canSendBackAssessment = false;
    showUnapproveButton = false;
    showApproveButton = false;
    disableNextButton = false;
    disablePreviousButton = false;
    isSubmitDisabled = true;
    isApproveDisabled = true;
    isReadinessTemplate = false;
    isDINATemplate = false;
    assessmentId: string;
    hideApproversFooter = true;
    approvers: IAssessmentApprover[] = [];
    isReadOnly = false;
    isViewSummaryDisabled = false;
    sectionNavFiltersApplied = false;

    private destroy$ = new Subject();
    private reviewChangedResponses: boolean;

    constructor(
        @Inject(StoreToken) private store: IStore,
        private stateService: StateService,
        public permissions: Permissions,
        private assessmentDetailLogic: AssessmentDetailService,
        private assessmentDetailAction: AaDetailActionService,
        private aaSectionNavMessagingService: AaSectionNavMessagingService,
        private otModalService: OtModalService,
        private assessmentDetailViewScroller: AssessmentDetailViewScrollerService,
        private notificationService: NotificationService,
        private translateService: TranslateService,
    ) {
        this.assessmentStatus = AssessmentStatus;
        this.sendBackType = SendBackType;
    }

    ngOnInit(): void {
        observableFromStore(this.store).pipe(
            startWith(this.store.getState()),
            takeUntil(this.destroy$),
        ).subscribe(() => this.componentWillReceiveState());
        this.assessmentDetailLogic.fetchAssessmentReadOnlyState().subscribe((readonly: boolean) => {
            this.isReadOnly = readonly;
        });
        this.aaSectionNavMessagingService.sectionFilterDataListener
            .pipe(
                takeUntil(this.destroy$),
            ).subscribe((filter: ILabelValue<string>) => {
                this.sectionNavFiltersApplied = filter && (filter.value !== AASectionFilterTypes.AllQuestions);
            });
    }

    ngOnDestroy(): void {
        this.destroy$.next();
        this.destroy$.complete();
        this.otModalService.destroy();
        this.assessmentDetailAction.reset();
    }

    goToPreviousSection(): void {
        if (isAssessmentReady(this.store.getState())) {
            const state = this.store.getState();
            const isFirstSection = isAssessmentDetailFirstSection(state);
            if (isFirstSection) {
                this.assessmentDetailAction.goToSectionHelper(EmptyGuid);
            } else {
                this.assessmentDetailAction.goToPreviousSection();
            }
        }
    }

    goToNextSection(): void {
        if (isAssessmentReady(this.store.getState())) {
            const state = this.store.getState();
            const isWelcomeSection = isAssessmentDetailWelcomeSection(state);

            if (isWelcomeSection) {
                this.assessmentDetailAction.goToFirstSection();
            } else {
                this.assessmentDetailAction.goToNextSection();
            }
        }
    }

    toggleApproversView() {
        this.hideApproversFooter = !this.hideApproversFooter;
    }

    isSavingResponses(state: IStoreState): boolean {
        return isSubmittingAssessment(state)
            || getQuestionIdsToSave(state).length > 0
            || getQuestionIdsBeingSaved(state).length > 0
            || getSubQuestionIdsToSave(state).length > 0
            || getSubQuestionIdsBeingSaved(state).length > 0;
    }

    shouldDisableSubmit(state: IStoreState): boolean {
        return this.isSavingResponses(state) || !getAssessmentDetailCanSubmit(state);
    }

    shouldDisableApprove(state: IStoreState): boolean {
        return this.isSavingResponses(state) || assessmentHasInvalidQuestions(state) || !getAssessmentDetailCanApprove(state);
    }

    submitAssessment(): void {
        if (this.reviewChangedResponses) {
            this.openChangedResponsesModal();
            return;
        }
        if (isAssessmentReady(this.store.getState())) {
            this.assessmentDetailAction.submitAssessment();
        }
    }

    viewSummary(): void {
        this.stateService.go("zen.app.pia.module.globalreadiness.views.assessmentreport", { assessmentId: this.assessmentId });
    }

    approveAssessment(): void {
        this.otModalService
            .create(
                AaFinishReviewModalComponent,
                {
                    isComponent: true,
                },
                {
                    assessmentId: this.assessmentId,
                },
            );
    }

    showUndoReviewAssessmentModal(): void {
        if (isAssessmentReady(this.store.getState())) {
            this.otModalService
                .create(
                    AAUndoReviewModalComponent,
                    {
                        isComponent: true,
                    },
                );
        }
    }

    showSendBackModal(sendBackType: string): void {
        this.assessmentDetailAction.sendBack(sendBackType, this.approverSendBackFlow, this.openInfoRequestMsg);
    }

    saveAndExit(): void {
        this.notificationService.alertSuccess(this.translateService.instant("Success"), this.translateService.instant("AssessmentResponsesSaved"));
        this.assessmentDetailAction.routeToListOrThankYouPage(this.assessmentId, true);
    }

    viewIncident() {
        this.stateService.go("zen.app.pia.module.incident.views.incident-register");
    }

    private componentWillReceiveState(): void {
        const state = this.store.getState();
        const assessmentModel = getAssessmentDetailModel(state);
        this.assessmentDetailStatus = getAssessmentDetailStatus(state);
        this.disableNextButton = this.isNextSectionDisabled(state);
        this.disablePreviousButton = isAssessmentDetailWelcomeSection(state);
        this.isReadinessTemplate = isReadinessTemplate()(state);
        this.isDINATemplate = isDINATemplate()(state);
        this.assessmentId = getAssessmentDetailAssessmentId(state);
        this.approvers = getAssessmentDetailApprovers(state);
        this.showUnapproveButton = this.canShowUnapproveButton();
        this.showApproveButton = this.canShowApproveButton();
        this.reviewChangedResponses = assessmentModel.reviewChangedResponses;

        if (this.assessmentDetailStatus === this.assessmentStatus.Not_Started
            || this.assessmentDetailStatus === this.assessmentStatus.In_Progress) {
            this.isSubmitDisabled = this.shouldDisableSubmit(state);
        } else if (this.assessmentDetailStatus === this.assessmentStatus.Under_Review) {
            this.isApproveDisabled = this.shouldDisableApprove(state);
        }
        this.isViewSummaryDisabled = this.isSavingResponses(state);
    }

    private isNextSectionDisabled(state: IStoreState): boolean {
        if (isAssessmentDetailLastSection(state)) return true;

        const selectedSectionId = getAssessmentDetailSelectedSectionId(state);
        const assessmentModel = getAssessmentDetailModel(state);
        const sectionList = assessmentModel.sectionHeaders.allIds;
        const sectionHeaders = assessmentModel.sectionHeaders.byIds;
        const selectedSectionIndex = sectionList.findIndex((sectionId: string) => sectionId === selectedSectionId);
        const remainingSectionList = sectionList.slice(selectedSectionIndex + 1);

        return !remainingSectionList.some((sectionId: string) => sectionHeaders[sectionId].hidden === false) && !this.isReadinessTemplate;
    }

    private isAssessmentUnderReview(): boolean {
        return this.assessmentDetailStatus === this.assessmentStatus.Under_Review;
    }

    private canShowApproveButton(): boolean {
        const isUnderReview = this.isAssessmentUnderReview();
        const hasApprovePermission = this.permissions.canShow("AssessmentApprove");
        const currentUser = getCurrentUser(this.store.getState());
        const isApproverInList = this.approvers.some((approver: IAssessmentApprover) => approver.id === currentUser.Id);
        const approverHasNotApproved = this.approvers.some((approver: IAssessmentApprover) => approver.id === currentUser.Id && !approver.approvedOn);

        return ((hasApprovePermission && !isApproverInList) || (isApproverInList && approverHasNotApproved)) && isUnderReview;
    }

    private canShowUnapproveButton(): boolean {
        const isUnderReview = this.isAssessmentUnderReview();
        const currentUser = getCurrentUser(this.store.getState());
        const hasCurrentUserApprovedAssessment = this.approvers.some((approver: IAssessmentApprover) => Boolean(approver.id === currentUser.Id && approver.approvedOn));
        return isUnderReview && hasCurrentUserApprovedAssessment;
    }

    private openChangedResponsesModal() {
        this.otModalService
            .create(
                AaReviewChangedResponseComponent,
                {
                    isComponent: true,
                    size: ModalSize.MEDIUM,
                },
            ).subscribe((data: boolean | IChangedQuestion) => {
                if (data === true) {
                    this.assessmentDetailAction.saveAssessment();
                } else if (data && (data as IChangedQuestion).sectionId) {
                    this.assessmentDetailAction.goToSectionHelper(data.sectionId);
                    setTimeout((): void => {
                        this.assessmentDetailViewScroller.scrollToQuestion(data.questionId);
                    }, 1500);
                }
            });
    }

}
