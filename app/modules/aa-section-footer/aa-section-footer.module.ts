// Modules
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { VitreusModule } from "@onetrust/vitreus";
import { SharedModule } from "./../shared/shared.module";
import { OTPipesModule } from "modules/pipes/pipes.module";
import { AAPipesModule } from "modules/assessment/pipes/aa-pipes.module";

// Components
import { AASectionFooterComponent } from "./aa-section-footer/aa-section-footer.component";
import { AASectionApproversFooter } from "./aa-section-approvers-footer/aa-section-approvers-footer.component";

// Directives

@NgModule({
    imports: [
        CommonModule,
        VitreusModule,
        SharedModule,
        OTPipesModule,
        AAPipesModule,
    ],
    exports: [
        AASectionFooterComponent,
        AASectionApproversFooter,
    ],
    entryComponents: [
    ],
    declarations: [
        AASectionFooterComponent,
        AASectionApproversFooter,
    ],
})
export class AASectionFooterModule { }
