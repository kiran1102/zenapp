// Angular
import {
    Component,
    Input,
    OnChanges,
} from "@angular/core";

// Lodash
import {
    fill,
    pullAt,
} from "lodash";

// Interfaces
import { IAssessmentApproverFooterViewModel } from "interfaces/assessment/assessment-model.interface";

// Enums
import { ApprovalStatus } from "enums/assessment/assessment-status.enum";

@Component({
    selector: "aa-section-approvers-footer",
    template: `
        <footer otPageFooter
            class="background-grey overflow-x-auto"
            [justify]="'flex-start'"
        >
            <ng-container *ngFor="let approver of approvers; let i = index;">
                <div class="margin-left-1">
                    <button otButton neutral
                        *ngIf="!approver.approvedOn"
                        class="align-self-center"
                        [attr.ot-auto-id]="'AASectionFooterApprovers_' + i"
                        [otPopover]="approverPopover"
                        [position]="'top'"
                        [size]="'small'"
                        [interactive]="true"
                        [isOpen]="approverPopoverOpen[i]"
                        [innerText]="approver.name | otUserInitials"
                        [icon]="''"
                        (click)="togglePopover(i)"
                        >
                    </button>
                    <button otButton otIconTag
                        *ngIf="approver.approvedOn"
                        class="align-self-center"
                        [ngClass]="approver.approved ? 'aa-footer__approved-button' : 'aa-footer__rejected-button'"
                        [iconTagClass]="approver.stateIconClass"
                        [attr.ot-auto-id]="'AASectionFooterApprovers_' + i"
                        [otPopover]="approverPopover"
                        [position]="'top'"
                        [size]="'small'"
                        [interactive]="true"
                        [isOpen]="approverPopoverOpen[i]"
                        [icon]="''"
                        (click)="togglePopover(i)"
                        >
                        {{ approver.name | otUserInitials }}
                    </button>
                </div>
                <ng-template #approverPopover>
                    <span class="row-nowrap">
                        <span
                            *ngIf="approver.approvedOn"
                            class="align-self-center margin-right-1"
                            [ngClass]="approver.stateIconClass"
                        ></span>
                        <div>
                            <p class="text-small margin-0">{{approver.name}}</p>
                            <p class="text-xsmall color-slate text-italic">{{approver.approvedOn | otDate: "dateTime" }}</p>
                        </div>
                    </span>
                </ng-template>
            </ng-container>
        </footer>
    `,
})

export class AASectionApproversFooter implements OnChanges {
    @Input() approvers: IAssessmentApproverFooterViewModel[] = [];

    approverPopoverOpen: boolean[] = [];

    ngOnChanges() {
        this.approvers.map((approver: IAssessmentApproverFooterViewModel) => {
            this.approverPopoverOpen.push(false);
            approver.approved = approver.approvedOn && approver.approvalState === ApprovalStatus.Approved;
            approver.stateIconClass = approver.approved ? "ot ot-check-circle color-blue" : "ot ot-times-circle color-cherry";
        });
    }

    togglePopover(index: number) {
        this.approverPopoverOpen[index] = !this.approverPopoverOpen[index];
        // remove element at index
        const toggleElement = pullAt(this.approverPopoverOpen, index)[0];
        // change remaining elements to false
        this.approverPopoverOpen = fill(this.approverPopoverOpen, false);
        // add removed element back
        this.approverPopoverOpen.splice(index, 0, toggleElement);
    }
}
