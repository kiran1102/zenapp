// Modules
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { VitreusModule } from "@onetrust/vitreus";
import { SharedModule } from "./../shared/shared.module";
import { OTPipesModule } from "modules/pipes/pipes.module";
import { AAPipesModule } from "modules/assessment/pipes/aa-pipes.module";

// Components
import { AASectionListFilterComponent } from "./aa-section-list-filter/aa-section-list-filter.component";

@NgModule({
    imports: [
        CommonModule,
        VitreusModule,
        SharedModule,
        OTPipesModule,
        AAPipesModule,
    ],
    exports: [
        AASectionListFilterComponent,
    ],
    entryComponents: [
    ],
    declarations: [
        AASectionListFilterComponent,
    ],
})
export class AASectionModule { }
