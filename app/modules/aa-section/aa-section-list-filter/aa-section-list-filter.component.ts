// Angular
import {
    Component,
    Inject,
    OnInit,
    OnDestroy,
} from "@angular/core";

// Rxjs
import { Subject } from "rxjs";
import {
    startWith,
    takeUntil,
} from "rxjs/operators";

// Redux
import observableFromStore from "oneRedux/utils/observable-from-store";
import {
    getAssessmentDetailModel,
    getAssessmentDetailSelectedSectionId,
    getQuestionsInOrder,
    isAssessmentReady,
    isLoadingSection,
    AssessmentDetailAction,
} from "modules/assessment/reducers/assessment-detail.reducer";

// Enums
import {
    RiskLevels,
    RiskLabel,
} from "enums/risk.enum";
import { AASectionFilterTypes } from "modules/assessment/enums/aa-filter.enum";

// Constants
import { AssessmentPermissions } from "constants/assessment.constants";

// Services
import { AaDetailActionService } from "modules/assessment/services/action/aa-detail-action.service";
import { AssessmentDetailViewScrollerService } from "modules/assessment/services/view-scroller/assessment-detail-view-scroller.service";
import { AaSectionNavMessagingService } from "assessmentServices/messaging/aa-section-messaging.service";
import { Permissions } from "modules/shared/services/helper/permissions.service";
import { AASectionApiService } from "modules/assessment/services/api/aa-section-api.service";
import { AssessmentDetailApiService } from "modules/assessment/services/api/assessment-detail-api.service";

// Interfaces
import { ILabelValue } from "interfaces/generic.interface";
import { IAssessmentModel } from "interfaces/assessment/assessment-model.interface";
import { IQuestionModel } from "interfaces/assessment/question-model.interface";
import { IRiskStatistics } from "interfaces/risk.interface";
import { ISectionHeaderInfo } from "interfaces/assessment/section-header-info.interface";
import {
    IStore,
    IStoreState,
} from "interfaces/redux.interface";

// Constants
import { EmptyGuid } from "constants/empty-guid.constant";
import { StoreToken } from "tokens/redux-store.token";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

@Component({
    selector: "aa-section-list-filter",
    templateUrl: "./aa-section-list-filter.component.html",
})

export class AASectionListFilterComponent implements OnInit, OnDestroy {

    collapseGroupExpanded = true;
    sectionList: string[];
    sectionHeaders: Map<string, ISectionHeaderInfo>;
    riskStatistics: Map<string, IRiskStatistics>;
    templateName: string;
    emptyGuid = EmptyGuid;
    isLoadingSection: boolean;
    isLoadingSectionPane: boolean;
    isSectionNavDisabled = false;
    questions: IQuestionModel[];
    selectedSectionId: string;
    sectionFilters: Array<ILabelValue<string>>;
    selectedFilter: ILabelValue<string>;

    private assessmentModel: IAssessmentModel;
    private destroy$ = new Subject();

    constructor(
        @Inject(StoreToken) readonly store: IStore,
        private translate: TranslatePipe,
        private permissions: Permissions,
        private assessmentDetailAction: AaDetailActionService,
        private assessmentDetailViewScroller: AssessmentDetailViewScrollerService,
        private aaSectionNavMessagingService: AaSectionNavMessagingService,
        private aASectionApiService: AASectionApiService,
        private assessmentDetailService: AssessmentDetailApiService,
    ) { }

    ngOnInit(): void {
        observableFromStore(this.store).pipe(
            startWith(this.store.getState()),
            takeUntil(this.destroy$),
        ).subscribe((state: IStoreState) => this.componentWillReceiveState(state));

        this.initializeFilter();
    }

    initializeFilter() {
        this.aaSectionNavMessagingService.sectionFilterDataListener.pipe (
            takeUntil(this.destroy$),
        ).subscribe((filter: ILabelValue<string>) => {
            if (this.selectedFilter !== filter) {
                this.selectedFilter = filter;
            }
        });

        this.sectionFilters = [
            {
                label: this.translate.transform(AASectionFilterTypes.AllQuestions),
                value: AASectionFilterTypes.AllQuestions,
            },
            {
                label: this.translate.transform(AASectionFilterTypes.RequiredQuestions),
                value: AASectionFilterTypes.RequiredQuestions,
            },
            {
                label: this.translate.transform(AASectionFilterTypes.UnansweredQuestions),
                value: AASectionFilterTypes.UnansweredQuestions,
            },
            {
                label: this.translate.transform(AASectionFilterTypes.RequiredAndUnansweredQuestions),
                value: AASectionFilterTypes.RequiredAndUnansweredQuestions,
            },
        ];
        this.selectedFilter = this.aaSectionNavMessagingService.sectionFilter ?
            this.aaSectionNavMessagingService.sectionFilter :
            this.sectionFilters[0];
        this.aaSectionNavMessagingService.publishSectionFilterData(this.selectedFilter);
    }

    ngOnDestroy(): void {
        this.destroy$.next();
        this.destroy$.complete();
    }

    onFilterSelection(filter: ILabelValue<string>) {
        // Update service with selected filter for welcome, current, first, next apis calls.
        this.aaSectionNavMessagingService.sectionFilter = filter;

        this.isLoadingSectionPane = true;
        const currentSelectionSectionId = this.selectedSectionId;
        this.assessmentDetailService.fetchAssessmentWelcomeSection(getAssessmentDetailModel(this.store.getState()).assessmentId, filter.value)
            .then((response: IAssessmentModel | null) => {
                this.store.dispatch({ type: AssessmentDetailAction.UPDATE_ASSESSMENT_ALL, assessmentModel: response });
                this.store.dispatch({ type: AssessmentDetailAction.FINISH_LOADING_ASSESSMENT_SECTION });
                this.goToSection(currentSelectionSectionId);
                this.isLoadingSectionPane = false;
                // AaSectionNavMessagingService drives the questions display based on selected filters.
                // Any components like sectionNav, aaDetailHeader, aaDetailFooter etc needs to subscribe
                // to AaSectionNavMessagingService to receive updates about any filter changes.
                this.aaSectionNavMessagingService.publishSectionFilterData(filter);
            });
    }

    goToSection(sectionId: string): void {
        if (this.canGoToSection(sectionId)) {
            this.assessmentDetailAction.goToSectionHelper(sectionId);
            this.assessmentDetailAction.toggleExpandSection(sectionId);
            this.assessmentDetailAction.closeAllExpandedSection();
            this.assessmentDetailViewScroller.resetCycle();
        }
    }

    canGoToSection(sectionId: string): boolean {
        return isAssessmentReady(this.store.getState())
            && !this.isSectionActive(sectionId)
            && !this.sectionHeaders[sectionId].hidden;
    }

    scrollToRequiredQuestion(event: Event, sectionId: string): void {
        event.preventDefault(); // prevent the app from reloading
        event.stopPropagation(); // prevent the accordion from expanding

        if (!this.isSectionActive(sectionId)) {
            this.goToSection(sectionId);
            setTimeout((): void => {
                this.assessmentDetailViewScroller.cycleThroughQuestions(this.sectionHeaders[sectionId].requiredUnansweredQuestionIds);
            }, 600);
        } else {
            this.assessmentDetailViewScroller.cycleThroughQuestions(this.sectionHeaders[sectionId].requiredUnansweredQuestionIds);
        }
    }

    scrollToInvalidQuestion(event: Event, sectionId: string): void {
        event.preventDefault(); // prevent the app from reloading
        event.stopPropagation(); // prevent the accordion from expanding

        if (!this.isSectionActive(sectionId)) {
            this.goToSection(sectionId);
            setTimeout((): void => {
                this.assessmentDetailViewScroller.cycleThroughQuestions(this.sectionHeaders[sectionId].invalidQuestionIds);
            }, 600);
        } else {
            this.assessmentDetailViewScroller.cycleThroughQuestions(this.sectionHeaders[sectionId].invalidQuestionIds);
        }
    }

    scrollToFirstFilteredQuestion(filter: ILabelValue<string>) {
        let questionId = "";
        if (filter) {
            const sectionId = getAssessmentDetailSelectedSectionId(this.store.getState());
            switch (filter.value) {
                case AASectionFilterTypes.AllQuestions:
                    const questions = getAssessmentDetailModel(this.store.getState()).questions;
                    questionId = questions && questions.allIds && questions.allIds.length ? questions.allIds[0] : null;
                    break;
                case AASectionFilterTypes.RequiredQuestions:
                    questionId = this.sectionHeaders[sectionId].requiredQuestionIds &&
                        this.sectionHeaders[sectionId].requiredQuestionIds.length ?
                        this.sectionHeaders[sectionId].requiredQuestionIds[0] : null;
                    break;
                case AASectionFilterTypes.UnansweredQuestions:
                    // TODO - Add logic when BE is ready with Unanswered questions
                    questionId = this.sectionHeaders[sectionId].unansweredQuestionIds &&
                        this.sectionHeaders[sectionId].unansweredQuestionIds.length ?
                        this.sectionHeaders[sectionId].unansweredQuestionIds[0] : null;
                    break;
                case AASectionFilterTypes.RequiredAndUnansweredQuestions:
                    questionId = this.sectionHeaders[sectionId].requiredUnansweredQuestionIds &&
                        this.sectionHeaders[sectionId].requiredUnansweredQuestionIds.length ?
                        this.sectionHeaders[sectionId].requiredUnansweredQuestionIds[0] : null;
            }
        }
        if (questionId && filter.value !== AASectionFilterTypes.AllQuestions) {
            this.assessmentDetailViewScroller.scrollToQuestion(questionId);
        }
    }

    scrollToQuestion(event: Event, questionId, sectionId): void {
        event.preventDefault(); // prevent the app from reloading
        event.stopPropagation(); // prevent the accordion from expanding

        if (!this.isSectionActive(sectionId)) {
            this.goToSection(sectionId);
            setTimeout((): void => {
                this.assessmentDetailViewScroller.scrollToQuestion(questionId);
            }, 600);
        } else {
            this.assessmentDetailViewScroller.scrollToQuestion(questionId);
        }
    }

    getRiskLevelClass(riskLevel: number): string {
        switch (riskLevel) {
            case RiskLevels.Low:
                return RiskLabel.RISK_LOW;
            case RiskLevels.Medium:
                return RiskLabel.RISK_MEDIUM;
            case RiskLevels.High:
                return RiskLabel.RISK_HIGH;
            case RiskLevels.VeryHigh:
                return RiskLabel.RISK_VERY_HIGH;
            default:
                return "";
        }
    }

    trackBySectionId(index: number, item: string): string {
        return item;
    }

    trackByQuestion(index: number, item: IQuestionModel): string {
        return item.id;
    }

    private componentWillReceiveState(state: IStoreState): void {
        this.assessmentModel = getAssessmentDetailModel(state);
        this.sectionList = this.assessmentModel.sectionHeaders.allIds;
        this.sectionHeaders = this.assessmentModel.sectionHeaders.byIds;
        this.publishInvalidQuestions(this.sectionHeaders);
        this.riskStatistics = this.assessmentModel.sectionHeaders.riskStatisticsBySectionIds;
        this.templateName = this.assessmentModel.template.name;
        this.isLoadingSection = isLoadingSection(state);
        this.isSectionNavDisabled = !isAssessmentReady(state);
        this.selectedSectionId = getAssessmentDetailSelectedSectionId(this.store.getState());

        if (this.assessmentModel && this.assessmentModel.questions) {
            this.questions = getQuestionsInOrder(this.store.getState());
        }
    }

    private publishInvalidQuestions(sectionHeaders: Map<string, ISectionHeaderInfo>) {
        let count = 0;
        for (const sectionHeader in sectionHeaders) {
            if (sectionHeaders.hasOwnProperty(sectionHeader) && sectionHeaders[sectionHeader].invalidQuestionIds &&
                    sectionHeaders[sectionHeader].invalidQuestionIds.length) {
                count = count + sectionHeaders[sectionHeader].invalidQuestionIds.length;
            }
        }
        this.aaSectionNavMessagingService.publishInvalidQuestionCountData(count);
    }

    // TODO - Move this logic to BE, and get "unansweredQuestionIds" returned
    // like requiredIds & unansweredRequiredIds.
    private updateUnansweredQuestions(sectionHeaders: Map<string, ISectionHeaderInfo>, questions: IQuestionModel[], selectedSectionId: string): Map<string, ISectionHeaderInfo> {
        sectionHeaders[selectedSectionId].unansweredQuestionIds = [];
        questions.forEach((question: IQuestionModel) => {
            if (question.responses === null || !question.responses.allIds.length) {
                sectionHeaders[selectedSectionId].unansweredQuestionIds.push(question.id);
            }
          });
        return sectionHeaders;
    }

    private isSectionActive(sectionId: string): boolean {
        return sectionId === getAssessmentDetailSelectedSectionId(this.store.getState());
    }
}
