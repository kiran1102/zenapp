// Modules
import { NgModule } from "@angular/core";
import { SharedModule } from "sharedModules/shared.module";
import { UpgradedLegacyModule } from "sharedModules/upgrade/upgraded-legacy.module";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";
import { ReactiveFormsModule } from "@angular/forms";
import { VitreusModule } from "@onetrust/vitreus";
import { OTPipesModule } from "modules/pipes/pipes.module";

import { SspSharedModule } from "../shared/ssp-shared.module";

// Components
import { SelfServicePortalComponent } from "./self-service-portal/self-service-portal.component";
import { SspLaunchAssessmentModalComponent } from "./ssp-launch-assessment-modal/ssp-launch-assessment-modal.component";
import { SspTemplateCardComponent } from "./ssp-template-card/ssp-template-card.component";
import { SspTemplateModalComponent } from "./ssp-template-modal/ssp-template-modal.component";

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        VitreusModule,
        SharedModule,
        UpgradedLegacyModule,
        OTPipesModule,
        SspSharedModule,
    ],
    exports: [
        SelfServicePortalComponent,
    ],
    entryComponents: [
        SelfServicePortalComponent,
        SspLaunchAssessmentModalComponent,
        SspTemplateModalComponent,
    ],
    declarations: [
        SelfServicePortalComponent,
        SspLaunchAssessmentModalComponent,
        SspTemplateCardComponent,
        SspTemplateModalComponent,
    ],
})
export class SspPageModule { }
