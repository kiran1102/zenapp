// 3rd party
import {
    Component,
    OnInit,
    Inject,
} from "@angular/core";
import { StateService } from "@uirouter/core";

// Interfaces
import { IProtocolResponse } from "interfaces/protocol.interface";
import { ISspTemplateDetails } from "interfaces/self-service-portal/ssp.interface";
import {
    IAssessmentFilterParams,
    IAssessmentTableRow,
    IAssessmentTable,
} from "interfaces/assessment/assessment.interface";
import { IPageableMeta } from "interfaces/pagination.interface";
import { IGridPaginationParams } from "interfaces/grid.interface";

// Enums
import { AssessmentStatus } from "enums/assessment/assessment-status.enum";
import { AssessmentStatusKeys } from "constants/assessment.constants";

// Services
import { Permissions } from "modules/shared/services/helper/permissions.service";
import { AssessmentListApiService } from "modules/assessment/services/api/assessment-list-api.service";
import { SSPApiService } from "../../shared/ssp-api.service";
import { SspHelperService } from "modules/self-service-portal/shared/ssp-helper.service";
import { Wootric } from "sharedModules/services/provider/wootric.service";

// Constants
import { WootricModuleMap } from "modules/shared/constants/wootric.constants";

@Component({
    selector: "self-service-portal",
    templateUrl: "self-service-portal.component.html",
})
export class SelfServicePortalComponent implements OnInit {

    canConfigure: boolean;
    sspTemplates: ISspTemplateDetails[] = [];
    viewReady: boolean;
    paginationDetail: IPageableMeta;
    assessmentList: IAssessmentTableRow[];
    assessmentcolumns: any;
    viewMore: boolean;
    isLoadingMore: boolean;
    assessmentStatusKeys = AssessmentStatusKeys;
    isLoading: boolean;
    isAssessmentListEmpty: boolean;
    templateRootVersionId: string;

    private appliedFilters: IAssessmentFilterParams[] = [];
    private paginationParams: IGridPaginationParams = { page: 0, size: 10, sort: "createDT,desc" };
    private searchText = "";

    constructor(
        private stateService: StateService,
        private AssessmentListApi: AssessmentListApiService,
        private permissions: Permissions,
        private SSPApi: SSPApiService,
        private sspHelper: SspHelperService,
        private wootric: Wootric,
    ) { }

    ngOnInit(): void {
        this.canConfigure = this.permissions.canShow("SelfServicePortalConfig");
        this.fetchSspTemplateList(this.stateService.params.templateId);
        this.getAssessmentList();
        this.sspHelper.$sspTemplateAdded.subscribe(() => {
            this.paginationParams.page = 0;
            this.getAssessmentList();
        });
        this.assessmentcolumns = [
            { columnType: "null", columnName: "ID", cellValueKey: "number"},
            { columnType: "link", columnName: "Name", cellValueKey: "name"},
            { columnType: "status", columnName: "Stage", cellValueKey: "status"},
            { columnType: "null", columnName: "Approver", cellValueKey: "approver"},
            { columnType: "date", columnName: "Deadline", cellValueKey: "deadline"},
        ];
        this.wootric.run(WootricModuleMap.SelfServicePortal);
    }

    goToSspConfig(): void {
        this.stateService.go("zen.app.pia.module.ssp.config");
    }

    fetchSspTemplateList(templateRootVersionId: string) {
        this.isLoading = true;
        this.SSPApi.getAllSspTemplates(true)
            .then((response: IProtocolResponse<ISspTemplateDetails[]>): void => {
                if (response.result) {
                    if (templateRootVersionId) {
                        this.sspTemplates = response.data.filter((template) => template.rootVersionId === templateRootVersionId);
                        this.sspTemplates = (this.sspTemplates && this.sspTemplates.length) ? this.sspTemplates : response.data;
                    } else {
                        this.sspTemplates = response.data;
                    }
                }
                this.isLoading = false;
            });
    }

    trackByFn(index: number, item: ISspTemplateDetails): string {
        return item.id;
    }

    handleNameClick(assessmentId: string): void {
        this.stateService.go("zen.app.pia.module.assessment.detail", { assessmentId });
    }

    getAssessmentList(): void {
        this.viewReady = this.paginationParams.page !== 0;
        this.isLoadingMore = this.paginationParams.page !== 0;
        this.AssessmentListApi.getAssignedAssessments(this.searchText, this.appliedFilters, this.paginationParams)
        .then((response: IProtocolResponse<IAssessmentTable>): void => {
            if (response.result) {
                this.isAssessmentListEmpty = response.data.totalElements === 0;
                this.assessmentList = this.paginationParams.page === 0 ? response.data.content : this.assessmentList.concat(response.data.content);
                this.viewMore = response.data.totalElements > response.data.numberOfElements;
                this.paginationDetail = {
                    first: response.data.first,
                    last: response.data.last,
                    number: response.data.number,
                    numberOfElements: response.data.numberOfElements,
                    size: response.data.size,
                    sort: response.data.sort,
                    totalElements: response.data.totalElements,
                    totalPages: response.data.totalPages,
                };
            }
        }).finally((): void => {
            this.viewReady = true;
            this.isLoadingMore = false;
        });
    }

    onViewMoreClick(): void {
        this.paginationParams.page++;
        this.getAssessmentList();
    }

    getStatusIcon(assessmentModelStatus: string): string {
        switch (assessmentModelStatus) {
            case AssessmentStatus.Not_Started:
                return "";
            case AssessmentStatus.In_Progress:
                return "primary";
            case AssessmentStatus.Under_Review:
                return "warning";
            case AssessmentStatus.Completed:
                return "success";
        }
    }
}
