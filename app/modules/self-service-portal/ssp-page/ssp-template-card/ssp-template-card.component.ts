// 3rd party
import {
    Component,
    Inject,
    Input,
} from "@angular/core";
import { StateService } from "@uirouter/core";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

// Injection Token
import { StoreToken } from "tokens/redux-store.token";

// Services
import { SspHelperService } from "../../shared/ssp-helper.service";
import { ModalService } from "sharedServices/modal.service";
import { SSPApiService } from "../../shared/ssp-api.service";
import { NotificationService } from "modules/shared/services/provider/notification.service";

// Redux
import { IStore } from "interfaces/redux.interface";

// Interfaces
import {
    ISspTemplateDetails,
    ISspTemplatesConfiguration,
} from "interfaces/self-service-portal/ssp.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";

@Component({
    selector: "ssp-template-card",
    templateUrl: "ssp-template-card.component.html",
})
export class SspTemplateCardComponent {

    @Input() sspTemplateDetails: ISspTemplateDetails;

    launchInProgress = false;

    constructor(
        private stateService: StateService,
        @Inject(StoreToken) private store: IStore,
        private sspHelper: SspHelperService,
        private sspApi: SSPApiService,
        private modalService: ModalService,
        private translatePipe: TranslatePipe,
        private notificationService: NotificationService,
    ) { }

    launchAssessment() {
        this.launchInProgress = true;
        if (this.sspTemplateDetails.directLaunchEnabled) {
            this.sspHelper.directLaunchAssessment(this.sspTemplateDetails).subscribe((assessmentId: string) => {
                if (assessmentId) {
                    this.notificationService.alertSuccess(this.translatePipe.transform("Success"), this.translatePipe.transform("AssessmentLaunchedSuccessfully"));
                    this.stateService.go("zen.app.pia.module.assessment.detail", { assessmentId });
                } else {
                    this.launchInProgress = false;
                }
            });
        } else {
            this.sspApi.getSspTemplateConfigurationDetail(this.sspTemplateDetails.id).then((response: IProtocolResponse<ISspTemplatesConfiguration>) => {
                    if (response.result) {
                        const modalData: ISspTemplatesConfiguration = {
                            id: this.sspTemplateDetails.id,
                            ...response.data,
                        };
                        this.modalService.setModalData(modalData);
                        this.modalService.openModal("downgradeSspLaunchAssessmentModal");
                        this.launchInProgress = false;
                }
            });
        }
    }

}
