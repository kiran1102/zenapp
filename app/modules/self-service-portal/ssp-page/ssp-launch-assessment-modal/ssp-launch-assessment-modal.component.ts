// Angular
import { Component, Inject } from "@angular/core";
import {
    FormGroup,
    FormBuilder,
    Validators,
} from "@angular/forms";
import { StateService } from "@uirouter/core";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

// Redux
import { getModalData } from "oneRedux/reducers/modal.reducer";
import { StoreToken } from "tokens/redux-store.token";
import { IStore } from "interfaces/redux.interface";
import { getCurrentUser } from "oneRedux/reducers/user.reducer";
import { getOrgById } from "oneRedux/reducers/org.reducer";

// Services
import { ModalService } from "sharedServices/modal.service";
import { SspHelperService } from "modules/self-service-portal/shared/ssp-helper.service";
import { NotificationService } from "modules/shared/services/provider/notification.service";

// Enums
import { OrgUserDropdownTypes } from "enums/org-user-dropdown.enum";

// Interfaces
import { IOrgUserAdapted } from "interfaces/org-user.interface";
import {
    IAssessmentCreationDetails,
    IAssessmentRespondentDetails,
} from "interfaces/assessment/assessment-creation-details.interface";
import {  ISspTemplatesConfiguration } from "interfaces/self-service-portal/ssp.interface";
import { IUser } from "interfaces/user.interface";
import { INameId } from "interfaces/generic.interface";

// 3rd party
import { some } from "lodash";

@Component({
    selector: "ssp-launch-assessment-modal",
    templateUrl: "./ssp-launch-assessment-modal.component.html",
})
export class SspLaunchAssessmentModalComponent {

    assessmentForm: FormGroup;
    launchInProgress = false;
    orgUserTypes = OrgUserDropdownTypes;
    templateId: string;
    showApproverSelection: boolean;
    showRespondentSelection: boolean;
    showOrgGroupSelection: boolean;
    currentUser: IUser;
    removeCurrentUser = false;
    respondentsList: IOrgUserAdapted[] = [];
    approversList: IOrgUserAdapted[] = [];

    constructor(
        @Inject(StoreToken) private store: IStore,
        private stateService: StateService,
        private modalService: ModalService,
        private formBuilder: FormBuilder,
        private sspHelper: SspHelperService,
        private translatePipe: TranslatePipe,
        private notificationService: NotificationService,
    ) { }

    ngOnInit() {
        const modalData = getModalData(this.store.getState()) as ISspTemplatesConfiguration;
        let orgGroupId;
        let respondentId;
        let respondentName;
        this.templateId = modalData.id;
        this.showApproverSelection = modalData.approverSelectionAllowed;
        this.showRespondentSelection = modalData.respondentSelectionAllowed;
        this.showOrgGroupSelection = modalData.orgGroupSelectionAllowed;
        this.currentUser = getCurrentUser(this.store.getState());
        if (!modalData.orgGroupSelectionAllowed) {
            orgGroupId = this.currentUser.OrgGroupId;
        }
        if (!modalData.respondentSelectionAllowed) {
            respondentId = this.currentUser.Id;
            respondentName = this.currentUser.FullName;
        }
        this.assessmentForm = this.formBuilder.group({
            assessmentName: ["", [Validators.required, Validators.maxLength(250)]],
            respondent: [[{ Id: respondentId, FullName: respondentName }], Validators.required],
            approver: [[]],
            orgGroup: [ orgGroupId, Validators.required],
        });
    }

    setOrgSelected(organization: string): void {
        this.removeCurrentUser = true;
        this.assessmentForm.get("orgGroup").setValue(organization);
        if (this.showRespondentSelection) {
            this.assessmentForm.get("respondent").setValue([]);
            this.respondentsList = [];
        }
        if (this.showApproverSelection) {
            this.assessmentForm.get("approver").setValue([]);
            this.approversList = [];
        }
    }

    launchAssessment() {
        this.launchInProgress = true;
        const orgGroupId = this.assessmentForm.get("orgGroup").value as string;
        const orgGroupName = getOrgById(orgGroupId)(this.store.getState()).Name;

        const respondents: IAssessmentRespondentDetails[] = [];
        const assessment: IAssessmentCreationDetails = {
            inventoryDetails: null,
            name: this.assessmentForm.value["assessmentName"],
            orgGroupId,
            orgGroupName,
            respondents,
            templateId: this.templateId,
        };
        const respondentsList = this.assessmentForm.get("respondent").value as IOrgUserAdapted[];
        if (respondentsList.length) {
            respondentsList.forEach((respondent: IOrgUserAdapted): void => {
                if (respondent.Id || respondent.FullName) {
                    const respondentId = respondent.Id === respondent.FullName ? null : respondent.Id;
                    assessment.respondents.push({
                        respondentId,
                        respondentName: respondent.FullName,
                        comment: null,
                    });
                }
            });
        }
        assessment.approvers = [];
        const approversList = this.assessmentForm.get("approver").value as IOrgUserAdapted[];
        if (approversList.length && (approversList[0].FullName !== undefined || approversList[0].Id !== undefined)) {
            approversList.forEach((approver: IOrgUserAdapted): void => {
                assessment.approvers.push({
                    approverId: approver.Id,
                    approverName: approver.FullName,
                    comment: null,
                });
            });
        }
        this.sspHelper.launchAssessment(assessment).subscribe((assessmentId: string) => {
            if (assessmentId) {
                this.notificationService.alertSuccess(this.translatePipe.transform("Success"), this.translatePipe.transform("AssessmentLaunchedSuccessfully"));
                if (some(respondentsList, (respondent: IOrgUserAdapted) => respondent.Id === this.currentUser.Id)) {
                    this.stateService.go("zen.app.pia.module.assessment.detail", { assessmentId });
                }
            }
            this.launchInProgress = false;
            this.closeModal();
        });
    }

    handleRespondentSelection(respondent: IOrgUserAdapted) {
        this.assessmentForm.get("respondent").setValue(respondent);
    }

    handleApproverSelection(approver: IOrgUserAdapted) {
        this.assessmentForm.get("approver").setValue(approver);
    }

    closeModal() {
        this.modalService.clearModalData();
        this.modalService.closeModal();
    }
}
