// Angular
import {
    Component,
    OnInit,
} from "@angular/core";
import {
    FormGroup,
    FormControl,
    Validators,
} from "@angular/forms";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

// Interfaces
import { IProtocolResponse } from "interfaces/protocol.interface";
import {
    ISspTemplatesConfiguration,
    ISspTemplateDetails,
} from "interfaces/self-service-portal/ssp.interface";

// Services
import { SSPApiService } from "../../shared/ssp-api.service";
import { ModalService } from "sharedServices/modal.service";

@Component({
    selector: "ssp-template-modal",
    templateUrl: "ssp-template-modal.component.html",
})

export class SspTemplateModalComponent implements OnInit {

    sspTemplate: ISspTemplatesConfiguration;
    templateForm: FormGroup;
    templateOptions: ISspTemplateDetails[] = [];
    saveInProgress = false;
    saveAndAddInProgress = false;
    templatePlaceHolder: string;
    disableTemplateDropdown = false;
    hasSaveAndAddDone = false;

    constructor(
        private readonly modalService: ModalService,
        private readonly SspApi: SSPApiService,
        private translatePipe: TranslatePipe,
    ) { }

    ngOnInit(): void {
        this.sspTemplate = this.getInitialTemplate();
        this.templatePlaceHolder = this.translatePipe.transform("SelectTemplate");
        this.fetchBaseTemplates();
        this.templateForm = new FormGroup({
            template: new FormControl(this.sspTemplate.id, Validators.required),
            directLaunch: new FormControl(this.sspTemplate.directLaunchEnabled),
            approver: new FormControl(this.sspTemplate.approverSelectionAllowed),
            respondent: new FormControl(this.sspTemplate.respondentSelectionAllowed),
            organization: new FormControl(this.sspTemplate.orgGroupSelectionAllowed),
        });
    }

    closeModal(): void {
        if (this.hasSaveAndAddDone) {
            this.modalService.handleModalCallback();
        }
        this.modalService.closeModal();
        this.modalService.clearModalData();
    }

    onDirectLaunchToggle(value: boolean): void {
        this.templateForm.get("directLaunch").setValue(value);
        if (value) {
            this.templateForm.patchValue({ approver: false, respondent: false, organization: false });
        }
    }

    submitChanges(isSaveAndAdd: boolean): void {
        this.saveInProgress = !isSaveAndAdd;
        this.saveAndAddInProgress = isSaveAndAdd;
        this.SspApi.updateSSPTemplateConfiguration(this.getSspTemplateModelFromTemplateForm()).then((response: IProtocolResponse<void>) => {
            if (response.result) {
                if (this.saveAndAddInProgress) {
                    this.templateForm.reset(this.getInitialTemplate());
                    this.templateOptions = [];
                    this.fetchBaseTemplates();
                    this.hasSaveAndAddDone = true;
                } else {
                    this.modalService.handleModalCallback();
                    this.closeModal();
                }
            }
            this.saveInProgress = false;
            this.saveAndAddInProgress = false;
        });
    }

    private getSspTemplateModelFromTemplateForm(): ISspTemplatesConfiguration {
        return {
            id: this.templateForm.get("template").value.id,
            directLaunchEnabled: this.templateForm.get("directLaunch").value,
            approverSelectionAllowed: this.templateForm.get("approver").value,
            respondentSelectionAllowed: this.templateForm.get("respondent").value,
            orgGroupSelectionAllowed: this.templateForm.get("organization").value,
            selfServiceEnabled: true,
            rootVersionId: null,
        };
    }

    private getInitialTemplate(): ISspTemplatesConfiguration {
        return {
            rootVersionId: null,
            directLaunchEnabled: false,
            approverSelectionAllowed: false,
            respondentSelectionAllowed: false,
            orgGroupSelectionAllowed: false,
        };
    }

    private fetchBaseTemplates(): void {
        const templatePlaceHolder = this.templatePlaceHolder;
        this.templatePlaceHolder = this.translatePipe.transform("Loading");
        this.disableTemplateDropdown = true;
        this.SspApi.fetchBaseTemplates().then((response: IProtocolResponse<ISspTemplateDetails[]>) => {
            if (response.result) {
                this.templatePlaceHolder = templatePlaceHolder;
                this.templateOptions = response.data;
                this.disableTemplateDropdown = false;
            }
        });
    }
}
