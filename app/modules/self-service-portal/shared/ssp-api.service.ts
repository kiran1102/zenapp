// Rxjs
import { Observable ,  from as fromPromise } from "rxjs";

// Core
import {
    Injectable,
} from "@angular/core";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

// Interfaces
import {
    IProtocolConfig,
    IProtocolMessages,
    IProtocolResponse,
} from "interfaces/protocol.interface";
import {
    ISspTemplateDetails,
    ISspTemplatesConfiguration,
    ISspTemplatesRestrictView,
} from "interfaces/self-service-portal/ssp.interface";

// Service
import { ProtocolService } from "modules/core/services/protocol.service";
import { IAssessmentCreationDetails } from "interfaces/assessment/assessment-creation-details.interface";

@Injectable()
export class SSPApiService {

    constructor(
        private OneProtocol: ProtocolService,
        private translatePipe: TranslatePipe,
    ) { }

    public getAllSspTemplates(assessmentCreationAllowed?: boolean): ng.IPromise<IProtocolResponse<ISspTemplateDetails[]>> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("GET", `/api/template/v1/templates/selfservice/enabled`, { assessmentCreationAllowed });
        const messages: IProtocolMessages = { Error: { Hide: true } };
        return this.OneProtocol.http(config, messages);
    }

    public getSspTemplateConfigurationDetail(id: string): ng.IPromise<IProtocolResponse<ISspTemplatesConfiguration>> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("GET", `/api/template/v1/templates/selfservice/templates/${id}/settings`);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("CouldNotRetrieveTemplate") } };
        return this.OneProtocol.http(config, messages);
    }

    public  updateSSPTemplateConfiguration(sspTemplate: ISspTemplatesConfiguration): ng.IPromise<IProtocolResponse<void>> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("POST", `/api/template/v1/templates/selfservice/templates/${sspTemplate.id}/settings`, null, sspTemplate);
        let messages: IProtocolMessages;
        if (sspTemplate.selfServiceEnabled) {
            messages = {
                Error: { custom: this.translatePipe.transform("CouldNotUpdateTemplate")},
                Success: { custom: this.translatePipe.transform("TemplateUpdatedSuccessfully") },
            };
        } else {
            messages = {
                Error: { custom: this.translatePipe.transform("CouldNotRemoveTemplate") },
                Success: { custom: this.translatePipe.transform("TemplateRemovedSuccessfully") },
            };
        }
        return this.OneProtocol.http(config, messages);
    }

    public fetchBaseTemplates(): ng.IPromise<IProtocolResponse<ISspTemplateDetails[]>> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("GET", `/api/template/v1/templates/selfservice/eligible`);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("CouldNotRetrieveTemplates") } };
        return this.OneProtocol.http(config, messages);
    }

    public launchAssessment(assessment: IAssessmentCreationDetails): Observable<string> {
        const config: IProtocolConfig = this.OneProtocol.config("POST", "/assessmentv2/createassessments", null, assessment);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("ErrorCreatingAssessment") } };
        return fromPromise(this.OneProtocol.http(config, messages).then((response: IProtocolResponse<string>): string => response.result ? response.data : null ));
    }

    getSspRestrictViewSetting(): ng.IPromise<IProtocolResponse<ISspTemplatesRestrictView>> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("GET", `/assessment-v2/v2/assessments/setting`);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("CouldNotRetrieveSetting") } };
        return this.OneProtocol.http(config, messages);
    }

    updateSspRestrictViewSetting(selfServiceSetting: ISspTemplatesRestrictView): ng.IPromise<IProtocolResponse<void>> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("POST", `/assessment-v2/v2/assessments/setting`, null, selfServiceSetting);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("CouldNotUpdateSetting") }, Success: { custom: this.translatePipe.transform("SettingsAppliedSuccessfully") } };
        return this.OneProtocol.http(config, messages);
    }
}
