// Modules
import { NgModule } from "@angular/core";
import { SharedModule } from "sharedModules/shared.module";

// Services
import { SSPApiService } from "./ssp-api.service";
import { SspHelperService } from "./ssp-helper.service";

@NgModule({
    imports: [
        SharedModule,
    ],
    providers: [
        SspHelperService,
        SSPApiService,
    ],
})
export class SspSharedModule { }
