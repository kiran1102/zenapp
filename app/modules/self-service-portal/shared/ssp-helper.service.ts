// Angular
import {
    Injectable,
    Inject,
} from "@angular/core";

// 3rd Party
import {
    Observable,
    Subject,
} from "rxjs";
import { map } from "rxjs/operators";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

// Redux
import { getCurrentUser } from "oneRedux/reducers/user.reducer";
import { StoreToken } from "tokens/redux-store.token";
import { IStore } from "interfaces/redux.interface";

// Services
import { SSPApiService } from "./ssp-api.service";
import { TimeStamp } from "oneServices/time-stamp.service";
import { ModalService } from "sharedServices/modal.service";

// Interfaces
import { IAssessmentCreationDetails } from "interfaces/assessment/assessment-creation-details.interface";
import {
    ISspTemplateDetails,
    ISspTemplatesConfiguration,
} from "interfaces/self-service-portal/ssp.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";

@Injectable()
export class SspHelperService {

    private sspTemplateRemoved = new Subject();
    // tslint:disable-next-line:member-ordering
    $sspTemplateRemoved = this.sspTemplateRemoved.asObservable();

    private sspTemplateAdded = new Subject();
    // tslint:disable-next-line:member-ordering
    $sspTemplateAdded = this.sspTemplateAdded.asObservable();

    constructor(
        @Inject(StoreToken) private readonly store: IStore,
        private readonly sspApiService: SSPApiService,
        private readonly timeStamp: TimeStamp,
        private readonly modalService: ModalService,
        private translatePipe: TranslatePipe,
    ) { }

    directLaunchAssessment(sspTemplate: ISspTemplateDetails): Observable<string> {
        const currentUser = getCurrentUser(this.store.getState());
        let assessmentName = `${currentUser.FullName}_${sspTemplate.name}`;
        // max-length of the assessment name is 250
        // 250 - 1 (underscore)  - 10 (date format)
        if (assessmentName.length > 239) {
            assessmentName = assessmentName.substr(0, 239);
        }
        assessmentName += `_${this.timeStamp.getFormattedCurrentDate()}`;
        const createRequest: IAssessmentCreationDetails = {
            inventoryDetails: null,
            name: assessmentName,
            orgGroupId: currentUser.OrgGroupId,
            orgGroupName: currentUser.OrgGroupName,
            respondents: [{
                respondentId: currentUser.Id,
                respondentName: currentUser.FullName,
            }],
            templateId: sspTemplate.id,
        };

        return this.launchAssessment(createRequest, true);
    }

    launchAssessment(assessment: IAssessmentCreationDetails, directLaunch = false): Observable<string> {
        return this.sspApiService.launchAssessment(assessment).pipe(
            map((assessmentId: string) => {
            if (assessmentId && !directLaunch) {
                this.sspTemplateAdded.next(assessmentId);
            }
            return assessmentId;
        }));
    }

    removeTemplate(id: string): void {
        this.modalService.setModalData({
            modalTitle: this.translatePipe.transform("RemoveTemplate"),
            confirmationText: this.translatePipe.transform("RemoveTemplateWarning"),
            cancelButtonText: this.translatePipe.transform("Cancel"),
            submitButtonText: this.translatePipe.transform("Remove"),
            promiseToResolve: (): ng.IPromise<boolean> => {
                const sspTemplate: ISspTemplatesConfiguration = {
                    id,
                    approverSelectionAllowed: false,
                    directLaunchEnabled: false,
                    orgGroupSelectionAllowed: false,
                    respondentSelectionAllowed: false,
                    selfServiceEnabled: false,
                    rootVersionId: null,
                };
                return this.sspApiService.updateSSPTemplateConfiguration(sspTemplate).then((response: IProtocolResponse<void>) => {
                    if (response.result) {
                        this.sspTemplateRemoved.next(id);
                    }
                    return response.result;
                });
            },
        });
        this.modalService.openModal("deleteConfirmationModal");
    }
}
