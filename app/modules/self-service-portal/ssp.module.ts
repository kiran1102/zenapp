// Services
import { AssessmentListApiService } from "modules/assessment/services/api/assessment-list-api.service";

// Modules
import { NgModule } from "@angular/core";
import { SspPageModule } from "./ssp-page/ssp-page.module";
import { SspConfigModule } from "./ssp-config/ssp-config.module";

@NgModule({
    imports: [
        SspConfigModule,
        SspPageModule,
    ],
    providers: [
        AssessmentListApiService,
    ],
})
export class SspModule {}
