// Modules
import {
    NgModule,
} from "@angular/core";
import { SharedModule } from "sharedModules/shared.module";
import { UpgradedLegacyModule } from "sharedModules/upgrade/upgraded-legacy.module";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";
import { ReactiveFormsModule } from "@angular/forms";
import { VitreusModule } from "@onetrust/vitreus";
import { OTPipesModule, PIPES } from "modules/pipes/pipes.module";
import { SspSharedModule } from "../shared/ssp-shared.module";

// Components
import { SspConfigComponent } from "./ssp-config/ssp-config.component";
import { SspTemplateListComponent } from "./ssp-config/ssp-template-list/ssp-template-list.component";
import { SspConfigTemplateCardComponent } from "./ssp-config-template-card/ssp-config-template-card.component";
import { SspTemplateConfigEditComponent } from "./ssp-template-config-edit/ssp-template-config-edit.component";

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        VitreusModule,
        OTPipesModule,
        SharedModule,
        UpgradedLegacyModule,
        SspSharedModule,
    ],
    exports: [
        SspConfigComponent,
    ],
    entryComponents: [
        SspConfigComponent,
    ],
    declarations: [
        SspConfigComponent,
        SspTemplateListComponent,
        SspConfigTemplateCardComponent,
        SspTemplateConfigEditComponent,
    ],
    providers: [
        PIPES, // TODO: Remove from providers when ot-common is implemented. Only necessary if you are using pipe in TS file, else just module is fine
    ],
})
export class SspConfigModule { }
