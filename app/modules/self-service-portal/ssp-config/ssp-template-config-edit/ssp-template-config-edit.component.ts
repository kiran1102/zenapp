// Angular
import {
    Component,
    Input,
    OnChanges,
    EventEmitter,
    Output,
} from "@angular/core";
import {
    FormGroup,
    FormControl,
} from "@angular/forms";

// Interfaces
import {
    ISspTemplateDetails,
    ISspTemplatesConfiguration,
} from "interfaces/self-service-portal/ssp.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";

// Services
import { SSPApiService } from "../../shared/ssp-api.service";

@Component({
    selector: "ssp-template-config-edit",
    templateUrl: "ssp-template-config-edit.component.html",
})

export class SspTemplateConfigEditComponent implements OnChanges {

    templateForm: FormGroup;
    saveInProgress = false;
    sspTemplateConfig: ISspTemplatesConfiguration;
    isLoadingTemplate = false;

    @Input() selectedSspTemplate: ISspTemplateDetails;
    @Output() removeSspTemplate = new EventEmitter<string>();

    constructor(
        private readonly SspApi: SSPApiService,
    ) { }

    ngOnChanges(): void {
        if (this.selectedSspTemplate) {
            this.fetchSspConfigTemplate(this.selectedSspTemplate.id);
        }
    }

    onDirectLaunchToggle(value: boolean): void {
        this.templateForm.get("directLaunch").setValue(value);
        if (value) {
            this.templateForm.patchValue({ approver: false, respondent: false, organization: false });
        }
    }

    submitChanges(): void {
        this.saveInProgress = true;
        this.SspApi.updateSSPTemplateConfiguration(this.getSspTemplateModelFromTemplateForm()).then(() => {
            this.saveInProgress = false;
        });
    }

    private getSspTemplateModelFromTemplateForm(): ISspTemplatesConfiguration {
        return {
            id: this.selectedSspTemplate.id,
            rootVersionId: null,
            directLaunchEnabled: this.templateForm.get("directLaunch").value,
            approverSelectionAllowed: this.templateForm.get("approver").value,
            respondentSelectionAllowed: this.templateForm.get("respondent").value,
            orgGroupSelectionAllowed: this.templateForm.get("organization").value,
            selfServiceEnabled: true,
        };
    }

    private fetchSspConfigTemplate(id: string): void {
        this.isLoadingTemplate = true;
        this.SspApi.getSspTemplateConfigurationDetail(id).then((response: IProtocolResponse<ISspTemplatesConfiguration>) => {
            if (response.result) {
                this.sspTemplateConfig = response.data;
                this.templateForm = new FormGroup({
                    directLaunch: new FormControl(this.sspTemplateConfig.directLaunchEnabled),
                    approver: new FormControl(this.sspTemplateConfig.approverSelectionAllowed),
                    respondent: new FormControl(this.sspTemplateConfig.respondentSelectionAllowed),
                    organization: new FormControl(this.sspTemplateConfig.orgGroupSelectionAllowed),
                });
            }
            this.isLoadingTemplate = false;
        });
    }
}
