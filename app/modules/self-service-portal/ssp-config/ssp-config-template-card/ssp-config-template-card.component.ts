// 3rd party
import {
    Component,
    OnInit,
    Input,
    OnDestroy,
    TemplateRef,
    ViewChild,
} from "@angular/core";
import { StateService } from "@uirouter/core";

// Rxjs
import { Subject } from "rxjs";

// Interfaces
import { ISspTemplateDetails } from "interfaces/self-service-portal/ssp.interface";
import { ILabelValue } from "interfaces/generic.interface";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

// Services
import { SspHelperService } from "../../shared/ssp-helper.service";
import {
    OtModalService,
    ModalSize,
} from "@onetrust/vitreus";
import { NotificationService } from "modules/shared/services/provider/notification.service";

enum SspTemplateCardActions {
    RemoveTemplate,
    GetLink,
}

@Component({
    selector: "ssp-config-template-card",
    templateUrl: "ssp-config-template-card.component.html",
})
export class SspConfigTemplateCardComponent implements OnInit {

    @Input() sspTemplateDetails: ISspTemplateDetails;

    @ViewChild("GetLinkModal") contentRef: TemplateRef<any>;

    contextMenuOptions: Array<ILabelValue<SspTemplateCardActions>>;
    isContextMenuOpen = false;
    otModalCloseEvent: Subject<string>;
    templateIdLink: string;

    private destroy$ = new Subject();

    constructor(
        private readonly sspHelper: SspHelperService,
        private otModalService: OtModalService,
        private translatePipe: TranslatePipe,
        private stateService: StateService,
        private notificationService: NotificationService,
    ) { }

    ngOnInit(): void {
        this.contextMenuOptions = [{
            label: SspTemplateCardActions[SspTemplateCardActions.RemoveTemplate],
            value: SspTemplateCardActions.RemoveTemplate,
        },
        {
            label: SspTemplateCardActions[SspTemplateCardActions.GetLink],
            value: SspTemplateCardActions.GetLink,
        }];
    }

    ngOnDestroy(): void {
        this.destroy$.next();
        this.destroy$.complete();
    }

    toggleContextMenu(value: boolean, event: JQueryMouseEventObject): void {
        if (event) {
            event.stopPropagation();
        }
        this.isContextMenuOpen = value;
    }

    onContextMenuOptionClick(option: SspTemplateCardActions): void {
        switch (option) {
            case SspTemplateCardActions.RemoveTemplate:
                this.openDeleteTemplate(this.sspTemplateDetails.id);
                break;

            case SspTemplateCardActions.GetLink:
                this.templateIdLink = window.location.href.replace("config", "").concat(this.sspTemplateDetails.rootVersionId);
                this.openGetLinkModal();
                break;
        }
    }

    openDeleteTemplate(templateId: string): void {
        this.sspHelper.removeTemplate(templateId);
    }

    openGetLinkModal(): void {
        this.otModalCloseEvent = this.otModalService
            .create(
                this.contentRef,
                {
                    size: ModalSize.XSMALL,
                },
            );
    }

    copyToClipBoard(): void {
        const copyText = document.getElementById("TemplateLinkField") as HTMLFormElement;
        copyText.select();
        document.execCommand("copy");
        this.notificationService.alertSuccess(
            this.translatePipe.transform("Success"),
            this.translatePipe.transform("LinkCopiedToClipboard"),
        );
    }

    closeModal(): void {
        this.otModalCloseEvent.next();
    }
}
