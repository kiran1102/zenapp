// Angular
import {
    Component,
    OnInit,
    Output,
    EventEmitter,
    Input,
    OnChanges,
} from "@angular/core";

// 3rd Party
import { remove } from "lodash";

// Interfaces
import { IProtocolResponse } from "interfaces/protocol.interface";
import { ISspTemplateDetails } from "interfaces/self-service-portal/ssp.interface";

// Services
import { SSPApiService } from "../../../shared/ssp-api.service";
import { SspHelperService } from "modules/self-service-portal/shared/ssp-helper.service";

@Component({
    selector: "ssp-template-list",
    templateUrl: "ssp-template-list.component.html",
})
export class SspTemplateListComponent implements OnInit, OnChanges {

    @Input() reloadTemplates: boolean;
    @Output() public sspTemplateSelected = new EventEmitter<ISspTemplateDetails>();

    sspTemplates: ISspTemplateDetails[];
    selected: ISspTemplateDetails;
    isLoading: boolean;

    constructor(
        private readonly sspHelper: SspHelperService,
        private readonly SSPApi: SSPApiService,
    ) { }

    ngOnInit(): void {
        this.fetchSspTemplateList();
    }

    ngOnChanges() {
        if (this.reloadTemplates) {
            this.fetchSspTemplateList();
        }
        this.sspHelper.$sspTemplateRemoved.subscribe((templateId: string) => {
            remove(this.sspTemplates, (template) => template.id === templateId );
            if (this.sspTemplates.length > 0) {
                this.onSspTemplateSelected(this.sspTemplates[0]);
            } else {
                this.onSspTemplateSelected(null);
            }
        });
    }

    fetchSspTemplateList() {
        this.isLoading = true;
        this.SSPApi.getAllSspTemplates()
            .then((response: IProtocolResponse<ISspTemplateDetails[]>): void => {
                if (response.result) {
                    this.sspTemplates = response.data;
                    if (this.sspTemplates && this.sspTemplates.length) {
                        this.onSspTemplateSelected(this.sspTemplates[0]);
                    }
                }
                this.isLoading = false;
            });
    }

    trackByFn(index: number, item: ISspTemplateDetails): string {
        return item.id;
    }

    onSspTemplateSelected(sspTemplate): void {
        this.selected = sspTemplate;
        this.sspTemplateSelected.emit(sspTemplate);
    }
}
