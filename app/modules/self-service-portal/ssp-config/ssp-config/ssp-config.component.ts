// 3rd party
import {
    Component,
    OnInit,
} from "@angular/core";
import { StateService } from "@uirouter/core";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

// Interfaces
import { IBreadcrumb } from "interfaces/breadcrumb.interface";

// Constants
import {
    ISspTemplateDetails,
} from "interfaces/self-service-portal/ssp.interface";

// Services
import { SspHelperService } from "modules/self-service-portal/shared/ssp-helper.service";

// Services
import { ModalService } from "sharedServices/modal.service";

@Component({
    selector: "ssp-config",
    templateUrl: "ssp-config.component.html",
})
export class SspConfigComponent implements OnInit {

    breadCrumb: IBreadcrumb;
    selectedTemplate: ISspTemplateDetails;
    reloadTemplates = false;
    toggleValue: boolean;

    constructor(
        private readonly modalService: ModalService,
        private stateService: StateService,
        private sspHelper: SspHelperService,
        private translatePipe: TranslatePipe,
    ) {}

    ngOnInit(): void {
        this.configurebreadcrumb();
    }

    addTemplate(): void {
        this.reloadTemplates = false;
        this.modalService.setModalData({
            callback: (): void => {
                this.reloadTemplates = true;
            },
        });
        this.modalService.openModal("downgradedSspTemplateModal");
    }

    configurebreadcrumb(): void {
        this.breadCrumb = { stateName: "", text: "" };
        this.breadCrumb.stateName = "zen.app.pia.module.ssp.list";
        this.breadCrumb.text = this.translatePipe.transform("SelfServicePortal");

    }

    goToRoute(route: { path: string, params: any }): void {
        this.stateService.go(route.path);
    }

    onSspTemplateSelect(sspTemplate: ISspTemplateDetails) {
        this.selectedTemplate = sspTemplate;
    }

    removeSspTemplate(id: string) {
        this.sspHelper.removeTemplate(id);
    }
}
