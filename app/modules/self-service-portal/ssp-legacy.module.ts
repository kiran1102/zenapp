import * as angular from "angular";

// Routes
import sspRoutes from "./ssp.routes";

export const sspLegacyModule = angular
    .module("zen.ssp-module", ["ui.router"])
    .config(sspRoutes)
    ;
