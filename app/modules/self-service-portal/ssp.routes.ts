import { Permissions } from "modules/shared/services/helper/permissions.service";
import { GlobalSidebarService } from "modules/global-sidebar/services/one-global-sidebar.service";

export default function sspRoutes($stateProvider: ng.ui.IStateProvider) {
    $stateProvider
        .state("zen.app.pia.module.ssp", {
            abstract: true,
            url: "ssp",
            views: {
                module: {
                    template: "<ui-view></ui-view>",
                    controller: [
                        "GlobalSidebarService",
                        (globalSidebarService: GlobalSidebarService) => {
                            globalSidebarService.set();
                        },
                    ],
                },
            },
            resolve: {
                SspPermission: [
                    "Permissions",
                    (permissions: Permissions): boolean => {
                        if (permissions.canShow("SelfServicePortal")) {
                            return true;
                        } else {
                            permissions.goToFallback();
                        }
                    },
                ],
            },
        })
        .state("zen.app.pia.module.ssp.list", {
            url: "",
            template: "<downgrade-self-service-portal></downgrade-self-service-portal>",
        })
        .state("zen.app.pia.module.ssp.list.template", {
            url: "/:templateId",
            template: "<downgrade-self-service-portal></downgrade-self-service-portal>",
        })
        // self service portal config route
        .state("zen.app.pia.module.ssp.config", {
            url: "/config",
            template: "<downgrade-ssp-config></downgrade-ssp-config>",
            resolve: {
                SspConfigPermission: [
                    "Permissions",
                    (permissions: Permissions): boolean => {
                        if (permissions.canShow("SelfServicePortalConfig")) {
                            return true;
                        } else {
                            permissions.goToFallback();
                        }
                    },
                ],
            },
        });
}

sspRoutes.$inject = ["$stateProvider"];
