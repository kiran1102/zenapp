// Angular
import {
    Component,
    Input,
} from "@angular/core";
import { FormGroup } from "@angular/forms";

// Enums & Constants
import { RuleContext } from "modules/rules/enums/rules.enums";

@Component({
    selector: "rules-side-menu",
    templateUrl: "./rules-side-menu.component.html",
    styles: [":host { flex: 0 0 35rem; max-width: 35rem; }"],
    host: {
        class: "padding-top-3 flex-full-height border-right background-grey",
    },
})
export class RulesSideMenuComponent {

    @Input() ruleContext: RuleContext;
    @Input() ruleForms: FormGroup[];

    trackByFn(index: number) {
        return index;
    }

}
