// angular
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

// modules
import { SharedModule } from "sharedModules/shared.module";
import { HttpClientModule } from "@angular/common/http";
import { FormsModule } from "@angular/forms";
import { ReactiveFormsModule } from "@angular/forms";
import { TranslateModule } from "@ngx-translate/core";
import { VitreusModule } from "@onetrust/vitreus";
import { DndListModule } from "ngx-drag-and-drop-lists";
import { OTPipesModule, PIPES } from "modules/pipes/pipes.module";
import { VirtualScrollModule } from "od-virtualscroll";
import { OneVideoModule } from "modules/video/video.module";

// components
import { RulesSideMenuComponent } from "./rules-side-menu.component";
import { RulesSideMenuItemComponent } from "./side-menu-item/rules-side-menu-item.component";

@NgModule({
    imports: [
        CommonModule,
        SharedModule,
        HttpClientModule,
        FormsModule,
        ReactiveFormsModule,
        TranslateModule,
        VitreusModule,
        DndListModule,
        OTPipesModule,
        VirtualScrollModule,
        OneVideoModule,
    ],
    declarations: [
        RulesSideMenuComponent,
        RulesSideMenuItemComponent,
    ],
    exports: [
        RulesSideMenuComponent,
        RulesSideMenuItemComponent,
    ],
    providers: [
        PIPES, // TODO: Remove from providers when ot-common is implemented. Only necessary if you are using pipe in TS file, else just module is fine
    ],
})
export class RulesSideMenuModule { }
