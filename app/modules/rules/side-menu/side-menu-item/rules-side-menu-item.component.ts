// Angular
import {
    Component,
    Input,
    Output,
    EventEmitter,
} from "@angular/core";
import { FormGroup } from "@angular/forms";

// Constants & Enums
import { ContextMenuType } from "@onetrust/vitreus";
import { RuleContext } from "modules/rules/enums/rules.enums";

// Interfaces
import { IRuleSideMenuItemConfig } from "../interfaces/rules-side-menu.interface";
import { IDropdownOption } from "interfaces/one-dropdown.interface";

// Services
import { RulesFormService } from "modules/rules/rules-list/services/rules-form.service";

@Component({
    selector: "rules-side-menu-item",
    templateUrl: "./rules-side-menu-item.component.html",
})
export class RulesSideMenuItemComponent {

    @Input() index: number;
    @Input() itemConfig: IRuleSideMenuItemConfig;
    @Input() rule: FormGroup;
    @Input() ruleContext: RuleContext;
    @Input() selected: boolean;

    @Output() onSelect = new EventEmitter();

    ruleMenuOptions$ = this.rulesFormService.ruleMenuOptions$;
    contextMenuType = ContextMenuType;
    menu: IDropdownOption[] = null;

    constructor(
        private rulesFormService: RulesFormService,
    ) {}

    get name() { return this.rule.get("name"); }
}
