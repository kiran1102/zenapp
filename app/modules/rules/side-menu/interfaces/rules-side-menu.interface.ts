export interface IRuleSideMenuItem {
    name: string;
}

export interface IRuleSideMenuItemConfig {
    icon: string;
}
