// Angular
import {
    Component,
    OnInit,
    Input,
} from "@angular/core";

// RxJs
import { Subject } from "rxjs";
import { forkJoin } from "rxjs/internal/observable/forkJoin";

// Constants & Enums
import {
    RuleContext,
    RuleTriggerTypes,
} from "modules/rules/enums/rules.enums";

// Services
import { RulesFormService } from "modules/rules/rules-list/services/rules-form.service";
import { RulesApiService } from "modules/rules/rules-list/services/rules-api.service";

@Component({
    selector: "rules-manager",
    templateUrl: "./rules-manager.component.html",
})
export class RulesMangerComponent implements OnInit {

    @Input() ruleContext: RuleContext;

    destroy$ = new Subject();
    hasNoRules$ = this.rulesFormService.hasNoRules$;
    hasOneRuleExpanded$ = this.rulesFormService.hasOneRuleExpanded$;
    isLoading = true;
    rulesEditMode$ = this.rulesFormService.rulesEditMode$;

    constructor(
        private rulesFormService: RulesFormService,
        private rulesApiService: RulesApiService,
    ) {}

    ngOnInit() {
        this.rulesFormService.setRuleContext(this.ruleContext);
        const getRules = this.rulesApiService.getRules();
        const getSpecifications = this.rulesApiService.getSpecifications();

        forkJoin([getRules, getSpecifications])
            .subscribe(([rulesResponse, specificationsResponse]) => {
                if (rulesResponse.result && specificationsResponse.result) {

                    // Create Rule Form Groups
                    if (rulesResponse.data && rulesResponse.data.length) {
                        this.rulesFormService.setRules(rulesResponse.data, this.ruleContext);
                    }

                    // Set Rule Specifications (Conditions Schema)
                    // NOTE: Api currently sends more specifications than UI is ready to show, so we filter down
                    // TODO: Will api always send all specifications? Or will they send specifications based on ruleContext?
                    let specs = specificationsResponse.data;
                    if (this.ruleContext === RuleContext.Vendor) {
                        specs = specificationsResponse.data.filter((s) => {
                            return s.name !== RuleTriggerTypes.TEMPLATE_LAST_COMPLETE_DATE
                                && s.name !== RuleTriggerTypes.TEMPLATE_ROOT_VERSION_ID;
                        });
                    }
                    this.rulesFormService.setSpecifications(specs);
                    this.rulesFormService.setRuleContextMenuOptions();
                    this.rulesFormService.fetchAllActions();
                }
                this.isLoading = false;
            });
    }

    ngOnDestroy() {
        this.destroy$.next();
        this.destroy$.complete();
    }

    addRule() {
        this.rulesFormService.addRule();
    }

    collapseAll() {
        this.rulesFormService.collapseAll();
    }

}
