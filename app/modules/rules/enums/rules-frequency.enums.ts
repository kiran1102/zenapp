export enum RuleScheduleFrequency {
    ONCE_A_MONTH = "OnceAMonth",
    ONCE_A_WEEK = "OnceAWeek",
    ONCE_A_DAY = "OnceADay",
}

export enum RuleDaysOfTheWeek {
    SUNDAY = "SUNDAY",
    MONDAY = "MONDAY",
    TUESDAY = "TUESDAY",
    WEDNESDAY = "WEDNESDAY",
    THURSDAY = "THURSDAY",
    FRIDAY = "FRIDAY",
    SATURDAY = "SATURDAY",
}
