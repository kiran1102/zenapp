export enum RuleContext {
    Vendor,
}

export enum ConditionGroupSetting {
    ALL = "ALL",
    ANY = "ANY",
}

export enum LogicalOperator {
    AND = "AND",
    NONE = "NONE",
    OR = "OR",
}

export enum RuleOperandType {
    ARRAY = "Array",
    BOOLEAN = "Boolean",
    LONG = "Long",
    STRING = "String",
}

export enum RuleOperator {
    EQUALS = "EQUALS",
    EQUAL_TO = "EQUAL_TO",
    GREATER_THAN = "GREATER_THAN",
    GREATER_THAN_EQUAL_TO = "GREATER_THAN_EQUAL_TO",
    IN = "IN",
    LESS_THAN = "LESS_THAN",
    LESS_THAN_EQUAL_TO = "LESS_THAN_EQUAL_TO",
    NOT_EQUAL_TO = "NOT_EQUAL_TO",
    NOT_IN = "NOT_IN",
}

export enum RuleStatus {
    ACTIVE = 0,
    INACTIVE = 1,
}

export enum RuleTriggerTypes {
    CONTRACT_EXPIRING_DATE_IN_DAYS = "contractExpiryDateInDays",
    LAST_ASSESSMENT_DATE = "lastAssessmentDateInDays",
    TEMPLATE_ROOT_VERSION_ID = "templateRootVersionId",
    TEMPLATE_LAST_COMPLETE_DATE = "templateLastCompleteDateInDays",
}

export enum RuleActionType {
    SEND_ASSESSMENT = "CopyAssessment",
    SEND_EMAIL = "SendEmail",
}
