import { FormGroup } from "@angular/forms";

export const validateSendAssessmentForm = (group: FormGroup): {[key: string]: boolean} => {
    const deadline = group.get("deadline");
    const reminder = group.get("reminder");
    const usePreviousAssessmentRespondents = group.get("usePreviousAssessmentRespondents");
    const usePrimaryContactAsRespondent = group.get("usePrimaryContactAsRespondent");
    const respondents = group.get("respondents");

    if (deadline.value && reminder.value && deadline.value <= reminder.value) {
        return {
            deadlineIsLessThanReminder: true,
        };
    }

    if (!usePreviousAssessmentRespondents.value
        && !usePrimaryContactAsRespondent.value
        && (!respondents.value || !respondents.value.length)) {
        return {
            respondentsMustBeSelected: true,
        };
    }
};

export const validateSendEmailForm = (group: FormGroup): {[key: string]: boolean} => {
    const recipients = group.get("recipients");
    const usePrimaryVendorContact = group.get("usePrimaryVendorContact");

    if (!usePrimaryVendorContact.value
        && (!recipients.value || !recipients.value.length)) {
        return {
            recipientsMustBeSelected: true,
        };
    }
};
