import {
    FormArray,
    FormGroup,
    FormControl,
    Validators,
} from "@angular/forms";
import { RuleConditionGroup } from "modules/rules/models/condition-group.model";
import { RuleCondition } from "modules/rules/models/condition.model";

export function initRuleConditionGroup(g: RuleConditionGroup): FormGroup {
    let conditions = [];
    if (g.conditions && g.conditions.length) {
        conditions = g.conditions.map((c) => addConditionFormGroup(c));
    }
    return new FormGroup({
        logicalOperator: new FormControl(g.logicalOperator),
        conditions: new FormArray(conditions),
    });
}

export function addConditionFormGroup(c: RuleCondition): FormGroup {
    return new FormGroup({
        field: new FormControl(c.field, Validators.required),
        operator: new FormControl(c.operator, Validators.required),
        value: new FormControl(c.value, Validators.required),
        valueType: new FormControl(c.valueType),
        logicalOperator: new FormControl(c.logicalOperator),
    });
}
