import {
    ConditionGroupSetting,
    LogicalOperator,
    RuleActionType,
    RuleOperandType,
    RuleOperator,
    RuleTriggerTypes,
} from "modules/rules/enums/rules.enums";
import {
    RuleDaysOfTheWeek,
    RuleScheduleFrequency,
} from "modules/rules/enums/rules-frequency.enums";
import { ILabelValue } from "@onetrust/vitreus";

export interface IRuleSpecification {
    name: RuleTriggerTypes;
    operandType: RuleOperandType;
    operators: RuleOperator[];
}

export interface IRuleSpecificationAdapted {
    name: RuleTriggerTypes;
    operandType: RuleOperandType;
    operators: Array<ILabelValue<string, RuleOperator>>;
}

export interface IRule extends IRuleMeta {
    actions: IRuleAction[];
    active: boolean;
    conditionGroups: IConditionGroup[];
    id: string;
    name: string;
    ruleId: string;
    sequence: number;
    scheduleActionOnWeekDayOnly: boolean;
    scheduleDayOfMonth: number;
    scheduleDayOfWeek: RuleDaysOfTheWeek;
    scheduleFrequency: RuleScheduleFrequency;
    scheduleTimeOfDay: string;
}

export interface IRuleMeta {
    conditionGroupSetting: ConditionGroupSetting;
    editMode: boolean;
    expanded: boolean;
}

export interface IConditionGroup {
    conditions: ICondition[];
    logicalOperator: LogicalOperator;
}

export interface ICondition {
    field: RuleTriggerTypes;
    logicalOperator: LogicalOperator;
    operator: RuleOperator;
    value: any[] | string | number | boolean;
    valueType: RuleOperandType;
}

export interface IConditionValidity {
    triggerValid: boolean;
    operatorValid: boolean;
    valueValid: boolean;
}

export interface IRuleAction {
    parameters: any;
    type: RuleActionType;
}

export interface IRuleActionValidity {
    actionValid: boolean;
    paramsValid: boolean;
}

export interface IRuleActionCopyAssessment {
    approvers: Array<{
        approverId: string;
        approverName: string;
    }>;
    copyComments: boolean;
    copyPreviousAssessment: boolean;
    copyResponses: boolean;
    deadline: number;
    description: string;
    latestTemplateVersion: boolean;
    reminder: number;
    respondents: Array<{
        respondentId: string;
        respondentName: string;
    }>;
    respondentComments: string;
    reviewChangedResponses: boolean;
    sendAssessment: boolean;
    templateRootVersionId: string;
    usePreviousAssessmentRespondents: boolean;
    usePrimaryContactAsRespondent: boolean;
}

export interface IRuleActionSendEmail {
    recipients: Array<{
        recipientId: string;
        recipientEmail: string;
    }>;
    messageTypeId: string;
    usePrimaryVendorContact: boolean;
}
