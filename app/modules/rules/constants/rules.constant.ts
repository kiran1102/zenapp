import {
    RuleContext,
    RuleOperator,
    RuleTriggerTypes,
    LogicalOperator,
    ConditionGroupSetting,
    RuleActionType,
    RuleOperandType,
} from "modules/rules/enums/rules.enums";
import { EmailTemplateGroupId } from "modules/settings/enums/email-template.enums";

export const ConditionGroupSettingToLogicalOperator = {
    [ConditionGroupSetting.ANY]: LogicalOperator.OR,
    [ConditionGroupSetting.ALL]: LogicalOperator.AND,
};

export const LogicalOperatorToConditionGroupSetting = {
    [LogicalOperator.OR]:   ConditionGroupSetting.ANY,
    [LogicalOperator.AND]:  ConditionGroupSetting.ALL,
};

export const RuleTranslations = {
    [ConditionGroupSetting.ANY]:                        "any",
    [ConditionGroupSetting.ALL]:                        "All",
    [RuleOperator.EQUAL_TO]:                            "EqualTo",
    [RuleOperator.NOT_EQUAL_TO]:                        "NotEqualTo",
    [RuleOperator.IN]:                                  "In",
    [RuleOperator.NOT_IN]:                              "NotIn",
    [RuleOperator.GREATER_THAN]:                        "GreaterThan",
    [RuleOperator.GREATER_THAN_EQUAL_TO]:               "GreaterThanOrEqualTo",
    [RuleOperator.LESS_THAN_EQUAL_TO]:                  "LessThanOrEqualTo",
    [RuleOperator.LESS_THAN]:                           "LessThan",
    [LogicalOperator.OR]:                               "Or",
    [LogicalOperator.AND]:                              "DSARWebformRules.And",
    [LogicalOperator.NONE]:                             null,
    [RuleTriggerTypes.CONTRACT_EXPIRING_DATE_IN_DAYS]:  "ContractExpirationIn",
    [RuleTriggerTypes.LAST_ASSESSMENT_DATE]:            "LastAssessmentCompletionDate",
    [RuleTriggerTypes.TEMPLATE_ROOT_VERSION_ID]:        "Template",
    [RuleTriggerTypes.TEMPLATE_LAST_COMPLETE_DATE]:     "LastAssessmentCompletedWith",
    [RuleActionType.SEND_ASSESSMENT]:                   "SendVendorAssessment",
    [RuleActionType.SEND_EMAIL]:                        "SendEmail",
};

export const RuleConditionOptions = [
    { label: RuleTranslations[ConditionGroupSetting.ANY], value: ConditionGroupSetting.ANY },
    { label: RuleTranslations[ConditionGroupSetting.ALL], value: ConditionGroupSetting.ALL },
];

export const RuleTriggerOptions = {
    [RuleTriggerTypes.CONTRACT_EXPIRING_DATE_IN_DAYS]:  { label: RuleTranslations[RuleTriggerTypes.CONTRACT_EXPIRING_DATE_IN_DAYS], value: RuleTriggerTypes.CONTRACT_EXPIRING_DATE_IN_DAYS },
    [RuleTriggerTypes.LAST_ASSESSMENT_DATE]:            { label: RuleTranslations[RuleTriggerTypes.LAST_ASSESSMENT_DATE], value: RuleTriggerTypes.LAST_ASSESSMENT_DATE },
    [RuleTriggerTypes.TEMPLATE_ROOT_VERSION_ID]:        { label: RuleTranslations[RuleTriggerTypes.TEMPLATE_ROOT_VERSION_ID], value: RuleTriggerTypes.TEMPLATE_ROOT_VERSION_ID },
    [RuleTriggerTypes.TEMPLATE_LAST_COMPLETE_DATE]:     { label: RuleTranslations[RuleTriggerTypes.TEMPLATE_LAST_COMPLETE_DATE], value: RuleTriggerTypes.TEMPLATE_LAST_COMPLETE_DATE },
};

export const RuleOperatorOptions = {
    [RuleOperator.EQUALS]:                  { label: RuleTranslations[RuleOperator.EQUALS], value: RuleOperator.EQUALS },
    [RuleOperator.EQUAL_TO]:                { label: RuleTranslations[RuleOperator.EQUAL_TO], value: RuleOperator.EQUAL_TO },
    [RuleOperator.GREATER_THAN]:            { label: RuleTranslations[RuleOperator.GREATER_THAN], value: RuleOperator.GREATER_THAN },
    [RuleOperator.GREATER_THAN_EQUAL_TO]:   { label: RuleTranslations[RuleOperator.GREATER_THAN_EQUAL_TO], value: RuleOperator.GREATER_THAN_EQUAL_TO },
    [RuleOperator.IN]:                      { label: RuleTranslations[RuleOperator.IN], value: RuleOperator.IN },
    [RuleOperator.LESS_THAN]:               { label: RuleTranslations[RuleOperator.LESS_THAN], value: RuleOperator.LESS_THAN },
    [RuleOperator.LESS_THAN_EQUAL_TO]:      { label: RuleTranslations[RuleOperator.LESS_THAN_EQUAL_TO], value: RuleOperator.LESS_THAN_EQUAL_TO },
    [RuleOperator.NOT_EQUAL_TO]:            { label: RuleTranslations[RuleOperator.NOT_EQUAL_TO], value: RuleOperator.NOT_EQUAL_TO },
    [RuleOperator.NOT_IN]:                  { label: RuleTranslations[RuleOperator.NOT_IN], value: RuleOperator.NOT_IN },
};

export const RuleTriggerOperandType = {
    [RuleTriggerTypes.CONTRACT_EXPIRING_DATE_IN_DAYS]:  RuleOperandType.LONG,
    [RuleTriggerTypes.LAST_ASSESSMENT_DATE]:            RuleOperandType.LONG,
    [RuleTriggerTypes.TEMPLATE_ROOT_VERSION_ID]:        RuleOperandType.STRING,
    [RuleTriggerTypes.TEMPLATE_LAST_COMPLETE_DATE]:     RuleOperandType.LONG,
};

export const RuleActionOptions = {
    [RuleActionType.SEND_ASSESSMENT]: { label: RuleTranslations[RuleActionType.SEND_ASSESSMENT], value: RuleActionType.SEND_ASSESSMENT },
    [RuleActionType.SEND_EMAIL]: { label: RuleTranslations[RuleActionType.SEND_EMAIL], value: RuleActionType.SEND_EMAIL },
};

export const RulePermissions = {
    [RuleContext.Vendor]: {
        Delete: "VRMRulesDelete",
        Edit: "VRMRulesEdit",
        Run: "VRMRulesRun",
    },
};

export const RuleActionOptionsByContext = {
    [RuleContext.Vendor]: [
        RuleActionOptions[RuleActionType.SEND_ASSESSMENT],
        RuleActionOptions[RuleActionType.SEND_EMAIL],
    ],
};

export const RuleActionTypeDefaultByContext = {
    [RuleContext.Vendor]: RuleActionType.SEND_ASSESSMENT,
};

export const RuleEmailTemplate = {
    [RuleContext.Vendor]: EmailTemplateGroupId.VENDOR_RULES,
};
