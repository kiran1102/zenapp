import {
    RuleDaysOfTheWeek,
    RuleScheduleFrequency,
} from "modules/rules/enums/rules-frequency.enums";
import { DayOfMonthLabels } from "sharedModules/constants/cron.constant";

const FrequencyTranslations = {
    [RuleScheduleFrequency.ONCE_A_DAY]:     "Daily",
    [RuleScheduleFrequency.ONCE_A_WEEK]:    "Weekly",
    [RuleScheduleFrequency.ONCE_A_MONTH]:   "Monthly",
    [RuleDaysOfTheWeek.SUNDAY]:             "Sunday",
    [RuleDaysOfTheWeek.MONDAY]:             "Monday",
    [RuleDaysOfTheWeek.TUESDAY]:            "Tuesday",
    [RuleDaysOfTheWeek.WEDNESDAY]:          "Wednesday",
    [RuleDaysOfTheWeek.THURSDAY]:           "Thursday",
    [RuleDaysOfTheWeek.FRIDAY]:             "Friday",
    [RuleDaysOfTheWeek.SATURDAY]:           "Saturday",
};

export const RuleFrequencyOption = {
    [RuleScheduleFrequency.ONCE_A_DAY]:     { label: FrequencyTranslations[RuleScheduleFrequency.ONCE_A_DAY], value: RuleScheduleFrequency.ONCE_A_DAY },
    [RuleScheduleFrequency.ONCE_A_WEEK]:    { label: FrequencyTranslations[RuleScheduleFrequency.ONCE_A_WEEK], value: RuleScheduleFrequency.ONCE_A_WEEK },
    [RuleScheduleFrequency.ONCE_A_MONTH]:   { label: FrequencyTranslations[RuleScheduleFrequency.ONCE_A_MONTH], value: RuleScheduleFrequency.ONCE_A_MONTH },
};

export const RuleFrequencyOptions = [
    RuleFrequencyOption[RuleScheduleFrequency.ONCE_A_DAY],
    RuleFrequencyOption[RuleScheduleFrequency.ONCE_A_WEEK],
    RuleFrequencyOption[RuleScheduleFrequency.ONCE_A_MONTH],
];

export const RuleDaysOfWeekOption = {
    [RuleDaysOfTheWeek.SUNDAY]:     { label: FrequencyTranslations[RuleDaysOfTheWeek.SUNDAY], value: RuleDaysOfTheWeek.SUNDAY },
    [RuleDaysOfTheWeek.MONDAY]:     { label: FrequencyTranslations[RuleDaysOfTheWeek.MONDAY], value: RuleDaysOfTheWeek.MONDAY },
    [RuleDaysOfTheWeek.TUESDAY]:    { label: FrequencyTranslations[RuleDaysOfTheWeek.TUESDAY], value: RuleDaysOfTheWeek.TUESDAY },
    [RuleDaysOfTheWeek.WEDNESDAY]:  { label: FrequencyTranslations[RuleDaysOfTheWeek.WEDNESDAY], value: RuleDaysOfTheWeek.WEDNESDAY },
    [RuleDaysOfTheWeek.THURSDAY]:   { label: FrequencyTranslations[RuleDaysOfTheWeek.THURSDAY], value: RuleDaysOfTheWeek.THURSDAY },
    [RuleDaysOfTheWeek.FRIDAY]:     { label: FrequencyTranslations[RuleDaysOfTheWeek.FRIDAY], value: RuleDaysOfTheWeek.FRIDAY },
    [RuleDaysOfTheWeek.SATURDAY]:   { label: FrequencyTranslations[RuleDaysOfTheWeek.SATURDAY], value: RuleDaysOfTheWeek.SATURDAY },
};

export const RuleDaysOfWeekOptions = [
    RuleDaysOfWeekOption[RuleDaysOfTheWeek.SUNDAY],
    RuleDaysOfWeekOption[RuleDaysOfTheWeek.MONDAY],
    RuleDaysOfWeekOption[RuleDaysOfTheWeek.TUESDAY],
    RuleDaysOfWeekOption[RuleDaysOfTheWeek.WEDNESDAY],
    RuleDaysOfWeekOption[RuleDaysOfTheWeek.THURSDAY],
    RuleDaysOfWeekOption[RuleDaysOfTheWeek.FRIDAY],
    RuleDaysOfWeekOption[RuleDaysOfTheWeek.SATURDAY],
];

export const DayOfTheMonthOption = {
    [1]: { label: DayOfMonthLabels[1], value: 1 },
    [2]: { label: DayOfMonthLabels[2], value: 2 },
    [3]: { label: DayOfMonthLabels[3], value: 3 },
    [4]: { label: DayOfMonthLabels[4], value: 4 },
    [5]: { label: DayOfMonthLabels[5], value: 5 },
    [6]: { label: DayOfMonthLabels[6], value: 6 },
    [7]: { label: DayOfMonthLabels[7], value: 7 },
    [8]: { label: DayOfMonthLabels[8], value: 8 },
    [9]: { label: DayOfMonthLabels[9], value: 9 },
    [10]: { label: DayOfMonthLabels[10], value: 10 },
    [11]: { label: DayOfMonthLabels[11], value: 11 },
    [12]: { label: DayOfMonthLabels[12], value: 12 },
    [13]: { label: DayOfMonthLabels[13], value: 13 },
    [14]: { label: DayOfMonthLabels[14], value: 14 },
    [15]: { label: DayOfMonthLabels[15], value: 15 },
    [16]: { label: DayOfMonthLabels[16], value: 16 },
    [17]: { label: DayOfMonthLabels[17], value: 17 },
    [18]: { label: DayOfMonthLabels[18], value: 18 },
    [19]: { label: DayOfMonthLabels[19], value: 19 },
    [20]: { label: DayOfMonthLabels[20], value: 20 },
    [21]: { label: DayOfMonthLabels[21], value: 21 },
    [22]: { label: DayOfMonthLabels[22], value: 22 },
    [23]: { label: DayOfMonthLabels[23], value: 23 },
    [24]: { label: DayOfMonthLabels[24], value: 24 },
    [25]: { label: DayOfMonthLabels[25], value: 25 },
    [26]: { label: DayOfMonthLabels[26], value: 26 },
    [27]: { label: DayOfMonthLabels[27], value: 27 },
    [28]: { label: DayOfMonthLabels[28], value: 28 },
    [29]: { label: DayOfMonthLabels[29], value: 29 },
    [30]: { label: DayOfMonthLabels[30], value: 30 },
    [31]: { label: DayOfMonthLabels[31], value: 31 },
};

export const DayOfTheMonthOptions = [
    DayOfTheMonthOption[1],
    DayOfTheMonthOption[2],
    DayOfTheMonthOption[3],
    DayOfTheMonthOption[4],
    DayOfTheMonthOption[5],
    DayOfTheMonthOption[6],
    DayOfTheMonthOption[7],
    DayOfTheMonthOption[8],
    DayOfTheMonthOption[9],
    DayOfTheMonthOption[10],
    DayOfTheMonthOption[11],
    DayOfTheMonthOption[12],
    DayOfTheMonthOption[13],
    DayOfTheMonthOption[14],
    DayOfTheMonthOption[15],
    DayOfTheMonthOption[16],
    DayOfTheMonthOption[17],
    DayOfTheMonthOption[18],
    DayOfTheMonthOption[19],
    DayOfTheMonthOption[20],
    DayOfTheMonthOption[21],
    DayOfTheMonthOption[22],
    DayOfTheMonthOption[23],
    DayOfTheMonthOption[24],
    DayOfTheMonthOption[25],
    DayOfTheMonthOption[26],
    DayOfTheMonthOption[27],
    DayOfTheMonthOption[28],
    DayOfTheMonthOption[29],
    DayOfTheMonthOption[30],
    DayOfTheMonthOption[31],
];
