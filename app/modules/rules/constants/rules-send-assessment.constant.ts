import { RuleContext } from "modules/rules/enums/rules.enums";

export const RulesSendAssessmentValidations = {
    deadline: [
        { type: "max", message: "DeadlineMustBeLessThanThousand" },
        { type: "min", message: "DeadlineMustBeGreaterThanZero" },
    ],
    reminder: [
        { type: "max", message: "ReminderMustBeLessThanThousand" },
        { type: "min", message: "ReminderMustBeGreaterThanZero" },
    ],
};

export const PrimaryContactAsRespondentText = {
    [RuleContext.Vendor]: "SendToPrimaryVendorContact",
};
