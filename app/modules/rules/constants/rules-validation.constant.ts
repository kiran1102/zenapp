import { RuleTriggerTypes } from "modules/rules/enums/rules.enums";

export const RuleTriggerValidations = {
    [RuleTriggerTypes.CONTRACT_EXPIRING_DATE_IN_DAYS]: [
        { type: "max", message: "ErrorMaxLengthExceeded" },
        { type: "min", message: "ValueMustBeGreaterThanZero" },
    ],
    [RuleTriggerTypes.LAST_ASSESSMENT_DATE]: [
        { type: "max", message: "ErrorMaxLengthExceeded" },
        { type: "min", message: "ValueMustBeGreaterThanZero" },
    ],
};
