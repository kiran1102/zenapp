import { FormControl, FormArray } from "@angular/forms";
import { RuleConditionGroup } from "./condition-group.model";
import { ConditionFormGroup } from "./condition-form.model";

export class ConditionGroupFormGroup {

    logicalOperator = new FormControl();
    conditions = new FormArray([]);

    constructor(conditionGroup?: RuleConditionGroup) {
        if (conditionGroup) {
            if (conditionGroup.logicalOperator) {
                this.logicalOperator.setValue(conditionGroup.logicalOperator);
            }

            if (conditionGroup.conditions) {
                const ruleConditionFormGroups = conditionGroup.conditions.map((c) => new ConditionFormGroup(c));
                this.conditions.setValue(ruleConditionFormGroups);
            }
        }
    }
}
