import {
    LogicalOperator,
    RuleTriggerTypes,
    RuleOperator,
    RuleOperandType,
} from "modules/rules/enums/rules.enums";

export class RuleCondition {

    field: RuleTriggerTypes = null;
    logicalOperator: LogicalOperator = LogicalOperator.NONE;
    operator: RuleOperator = null;
    value: any[] | string | number | boolean = null;
    valueType: RuleOperandType = null;

    constructor(condition?: RuleCondition) {
        if (condition) {
            if (condition.field) {
                this.field = condition.field;
            }
            if (condition.logicalOperator) {
                this.logicalOperator = condition.logicalOperator;
            }
            if (condition.operator) {
                this.operator = condition.operator;
            }
            if (condition.value) {
                this.value = condition.value;
            }
            if (condition.valueType) {
                this.valueType = condition.valueType;
            }
        }
    }
}
