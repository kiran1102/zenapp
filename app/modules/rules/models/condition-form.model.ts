import { FormControl } from "@angular/forms";
import { RuleCondition } from "./condition.model";

export class ConditionFormGroup {

    field = new FormControl();
    operator = new FormControl();
    value = new FormControl();
    valueType = new FormControl();
    logicalOperator = new FormControl();

    constructor(condition: RuleCondition) {
        if (condition.field) {
            this.field.setValue(condition.field);
        }

        if (condition.operator) {
            this.operator.setValue(condition.operator);
        }

        if (condition.value) {
            this.value.setValue(condition.value);
        }

        if (condition.valueType) {
            this.valueType.setValue(condition.valueType);
        }

        if (condition.logicalOperator) {
            this.logicalOperator.setValue(condition.logicalOperator);
        }
    }
}
