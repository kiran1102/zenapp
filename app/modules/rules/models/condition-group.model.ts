import { LogicalOperator } from "modules/rules/enums/rules.enums";
import { RuleCondition } from "./condition.model";

export class RuleConditionGroup {
    logicalOperator = LogicalOperator.NONE;
    conditions: RuleCondition[] = [ new RuleCondition() ];

    constructor(conditionGroup?: RuleConditionGroup) {
        if (conditionGroup) {
            if (conditionGroup.logicalOperator) {
                this.logicalOperator = conditionGroup.logicalOperator;
            }

            if (conditionGroup.conditions) {
                this.conditions = conditionGroup.conditions;
            }
        }
    }
}
