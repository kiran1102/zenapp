// angular
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

// modules
import { SharedModule } from "sharedModules/shared.module";
import { HttpClientModule } from "@angular/common/http";
import { FormsModule } from "@angular/forms";
import { ReactiveFormsModule } from "@angular/forms";
import { TranslateModule } from "@ngx-translate/core";
import { VitreusModule } from "@onetrust/vitreus";
import { DndListModule } from "ngx-drag-and-drop-lists";
import { OTPipesModule, PIPES } from "modules/pipes/pipes.module";
import { VirtualScrollModule } from "od-virtualscroll";
import { OneVideoModule } from "modules/video/video.module";

// components
import { RulesListComponent } from "modules/rules/rules-list/rules-list.component";
import { RuleFormComponent } from "modules/rules/rules-list/form/rule-form.component";
import { RulesSendAssessmentComponent } from "modules/rules/rules-list/form/actions/send-assessment/rules-send-assessment.component";
import { RulesSendEmailComponent } from "modules/rules/rules-list/form/actions/send-email/rules-send-email.component";

// services
import { RulesFormService } from "modules/rules/rules-list/services/rules-form.service";
import { RulesApiService } from "modules/rules/rules-list/services/rules-api.service";
import { RulesSendAssessmentService } from "modules/rules/rules-list/services/rules-send-assessment.service";
import { RulesSendEmailService } from "modules/rules/rules-list/services/rules-send-email.service";

// directives
import { DisableFormControlDirective } from "modules/rules/directives/disable-form-control.directive";

@NgModule({
    imports: [
        CommonModule,
        SharedModule,
        HttpClientModule,
        FormsModule,
        ReactiveFormsModule,
        VitreusModule,
        TranslateModule,
        DndListModule,
        OTPipesModule,
        VirtualScrollModule,
        OneVideoModule,
    ],
    declarations: [
        RulesListComponent,
        RuleFormComponent,
        RulesSendAssessmentComponent,
        RulesSendEmailComponent,
        DisableFormControlDirective,
    ],
    exports: [
        RulesListComponent,
        RuleFormComponent,
        RulesSendAssessmentComponent,
        RulesSendEmailComponent,
    ],
    providers: [
        RulesApiService,
        RulesFormService,
        RulesSendAssessmentService,
        RulesSendEmailService,
    ],
})
export class RulesListModule { }
