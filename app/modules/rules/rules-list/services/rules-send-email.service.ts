import { Injectable } from "@angular/core";
import { IOrgUserAdapted } from "interfaces/org-user.interface";
import { Regex } from "constants/regex.constant";

@Injectable()
export class RulesSendEmailService {

    mapRecipientsToOrgUser(users: Array<{ recipientId: string; recipientEmail: string; }>): IOrgUserAdapted[] {
        if (!users || !users.length) return [];
        return users.map((user) => ({ Id: user.recipientId, FullName: user.recipientEmail }));
    }

    mapOrgUserToRecipients(users: IOrgUserAdapted[]): Array<{ recipientId: string; recipientEmail: string; }> {
        if (!users || !users.length) return null;
        return users.map((user) => ({
            recipientId: user.Id && user.Id.match(Regex.GUID) ? user.Id : null,
            recipientEmail: !user.Id || (user.Id === user.FullName) ? user.FullName : null,
        }));
    }
}
