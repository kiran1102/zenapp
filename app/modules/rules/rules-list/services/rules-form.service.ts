// Core
import { Injectable } from "@angular/core";
import {
    FormBuilder,
    FormGroup,
    FormArray,
    Validators,
    ValidatorFn,
} from "@angular/forms";

// RxJs
import { BehaviorSubject } from "rxjs/internal/BehaviorSubject";
import { Observable } from "rxjs/Observable";
import {
    map,
    take,
    filter,
} from "rxjs/operators";

// Enums & Constants
import {
    RuleTriggerTypes,
    ConditionGroupSetting,
    LogicalOperator,
    RuleActionType,
    RuleOperator,
    RuleContext,
} from "modules/rules/enums/rules.enums";
import {
    LogicalOperatorToConditionGroupSetting,
    RulePermissions,
} from "modules/rules/constants/rules.constant";
import { EmptyGuid } from "constants/empty-guid.constant";
import {
    RuleScheduleFrequency,
    RuleDaysOfTheWeek,
} from "modules/rules/enums/rules-frequency.enums";
import {
    ConditionGroupSettingToLogicalOperator,
    RuleOperatorOptions,
    RuleTriggerOperandType,
    RuleTriggerOptions,
    RuleActionTypeDefaultByContext,
} from "modules/rules/constants/rules.constant";
import {
    initRuleConditionGroup,
    addConditionFormGroup,
} from "modules/rules/helpers/rules-helpers";

// Interfaces
import {
    IRule,
    IConditionGroup,
    IRuleSpecification,
    IRuleSpecificationAdapted,
    ICondition,
    IRuleAction,
    IRuleActionCopyAssessment,
    IRuleActionSendEmail,
} from "modules/rules/interfaces/rules.interface";
import {
    ILabelValue,
    ITimePickerModel,
    OtModalService,
    ConfirmModalType,
} from "@onetrust/vitreus";
import { IDropdownOption } from "interfaces/one-dropdown.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";

// Models
import { RuleConditionGroup } from "modules/rules/models/condition-group.model";
import { RuleCondition } from "modules/rules/models/condition.model";

// Services
import { RulesApiService } from "./rules-api.service";
import {
    validateSendAssessmentForm,
    validateSendEmailForm,
} from "modules/rules/helpers/rules-validators";
import { Permissions } from "sharedModules/services/helper/permissions.service";
import { TranslateService } from "@ngx-translate/core";

@Injectable()
export class RulesFormService {
    form$: Observable<FormGroup>;
    editModeForm$: Observable<FormGroup>;
    ruleMenuOptions$: Observable<IDropdownOption[]>;
    ruleActionOptions$: Observable<IDropdownOption[]>;
    ruleForms$: Observable<FormGroup[]>;
    selectedAction$: Observable<RuleActionType>;
    specifications$: Observable<IRuleSpecification[]>;
    specificationsMap$: Observable<ILabelValue<RuleTriggerTypes, IRuleSpecificationAdapted>>;
    triggerOptions$: Observable<Array<ILabelValue<string, RuleTriggerTypes>>>;
    hasNoRules$: Observable<boolean>;
    hasOneRuleExpanded$: Observable<boolean>;
    rulesEditMode$: Observable<boolean>;

    private _form = new BehaviorSubject<FormGroup>(null);
    private _ruleMenuOptions = new BehaviorSubject<IDropdownOption[]>(null);
    private _ruleForms = new BehaviorSubject<FormGroup[]>([]);
    private _selectedAction = new BehaviorSubject<RuleActionType>(null);
    private _specifications = new BehaviorSubject<IRuleSpecification[]>(null);
    private _specificationsMap = new BehaviorSubject<ILabelValue<RuleTriggerTypes, IRuleSpecificationAdapted>>(null);
    private _triggerOptions = new BehaviorSubject<Array<ILabelValue<string, RuleTriggerTypes>>>(null);
    private _ruleActionOptions = new BehaviorSubject<IDropdownOption[]>(null);
    private rules: IRule[];
    private ruleContext: RuleContext;
    private translations: {};
    private translateKeys = [
        "Edit",
        "Delete",
        "RunRuleNow",
        "RunRule",
        "AreYouSureYouWishToRunThisRule",
        "Cancel",
        "DeleteRule",
        "DeleteRuleConfirmationText",
        "Confirm",
    ];

    constructor(
        private otModalService: OtModalService,
        private rulesApiService: RulesApiService,
        private permissions: Permissions,
        private formBuilder: FormBuilder,
        private translate: TranslateService,
    ) {
        this.translate.stream(this.translateKeys).subscribe((t) => {
            this.translations = t;
        });
        this.form$ = this._form.asObservable();
        this.ruleMenuOptions$ = this._ruleMenuOptions.asObservable();
        this.ruleForms$ = this._ruleForms.asObservable();
        this.selectedAction$ = this._selectedAction.asObservable();
        this.specifications$ = this._specifications.asObservable();
        this.specifications$ = this._specifications.asObservable();
        this.specificationsMap$ = this._specificationsMap.asObservable();
        this.triggerOptions$ = this._triggerOptions.asObservable();
        this.ruleActionOptions$ = this._ruleActionOptions.asObservable();

        this.hasNoRules$ = this.ruleForms$.pipe(map((forms) => !forms || !forms.length));
        this.hasOneRuleExpanded$ = this.ruleForms$.pipe(map((forms) => forms.some((rule) => rule.value.expanded)));
        this.rulesEditMode$ = this.ruleForms$.pipe(map((forms) => this.isAnyRuleInEditMode(forms)));
        this.editModeForm$ = this.ruleForms$.pipe(map((forms) => this.getRuleInEditMode(forms)));
    }

    get ruleForms(): FormGroup[] {
        return this._ruleForms.getValue();
    }

    get specifications(): IRuleSpecification[] {
        return this._specifications.getValue();
    }

    setRules(rules: IRule[], context: RuleContext) {
        this.rules = rules;
        this._ruleForms.next(rules ? this.configureRuleForms(rules, context) : []);
    }

    setSpecifications(specs: IRuleSpecification[]) {
        this._specifications.next(specs);
        this._specificationsMap.next(this.setSpecificationsMap(specs));
        this.setTriggerOptions(specs.map((s) => s.name));
    }

    setRuleContextMenuOptions(showEdit = true) {
        const options = [];
        if (this.permissions.canShow(RulePermissions[this.ruleContext].Edit) && showEdit) {
            options.push({
                text: this.translations["Edit"],
                action: (rule: IRule) => { this.editRule(rule.id); },
            });
        }
        if (this.permissions.canShow(RulePermissions[this.ruleContext].Delete)) {
            options.push({
                text: this.translations["Delete"],
                action: (rule: IRule) => { this.deleteRule(rule); },
            });
        }
        if (this.permissions.canShow(RulePermissions[this.ruleContext].Run) && showEdit) {
            options.push({
                text: this.translations["RunRuleNow"],
                action: (rule: IRule) => { this.runRule(rule.id); },
            });
        }
        this._ruleMenuOptions.next(options);
    }

    setRuleContext(context: RuleContext) {
        this.ruleContext = context;
    }

    collapseAll() {
        this._ruleForms.next(
            this.ruleForms.map((form) => {
                form.get("expanded").setValue(false);
                return form;
            }),
        );
    }

    addRule() {
        this.setRuleContextMenuOptions(false);
        this._ruleForms.next(
            [...this.ruleForms, this.newRuleForm(this.ruleContext)],
        );
    }

    collapseOne(ruleId: string) {
        this._ruleForms.next(
            this.ruleForms.map((form) => {
                if (form.get("id").value === ruleId) {
                    form.get("expanded").setValue(false);
                    return form;
                } else {
                    return form;
                }
            }),
        );
    }

    expandOne(ruleId: string) {
        this._ruleForms.next(
            this.ruleForms.map((form) => {
                if (form.get("id").value === ruleId) {
                    form.get("expanded").setValue(true);
                    return form;
                } else {
                    return form;
                }
            }),
        );
    }

    setRuleActivation(ruleId: string, active: boolean) {
        if (active) {
            this.rulesApiService.activateRule(ruleId).subscribe((res) => {
                this.updateRulesActivation(ruleId, res.result ? active : !active);
            });
        } else {
            this.rulesApiService.deactivateRule(ruleId).subscribe((res) => {
                this.updateRulesActivation(ruleId, res.result ? active : !active);
            });
        }
    }

    cancelEdit() {
        this.setRuleContextMenuOptions();
        this.setRules(this.rules, this.ruleContext);
    }

    removeConditionGroup(form: FormGroup, groupIndex: number): FormGroup {
        const conditionGroups = form.get("conditionGroups") as FormArray;
        conditionGroups.removeAt(groupIndex);

        // Reset LogicalOperator to NONE when only 1 ConditionGroup
        if (conditionGroups.controls.length === 1) {
            const conditionGroupsUpdated = conditionGroups.value.map((g) => ({...g, logicalOperator: LogicalOperator.NONE}));
            conditionGroups.setValue(conditionGroupsUpdated);
        }
        return form;
    }

    onScheduleFrequencySelect(form: FormGroup, value: RuleScheduleFrequency): FormGroup {
        form.get("scheduleFrequency").setValue(value);
        form.get("scheduleActionOnWeekDayOnly").setValue(false);
        form.get("scheduleDayOfWeek").setValue(null);
        form.get("scheduleDayOfMonth").setValue(null);
        form.markAsDirty();
        return form;
    }

    onScheduleDayOfWeekSelect(form: FormGroup, value: RuleDaysOfTheWeek): FormGroup {
        form.get("scheduleDayOfWeek").setValue(value);
        form.markAsDirty();
        return form;
    }

    onDayOfTheMonthSelect(form: FormGroup, value: number): FormGroup {
        form.get("scheduleDayOfMonth").setValue(value);
        form.markAsDirty();
        return form;
    }

    onChangeWeekdayOnly(form: FormGroup, value: boolean): FormGroup {
        form.get("scheduleActionOnWeekDayOnly").setValue(value);
        form.markAsDirty();
        return form;
    }

    onTimeOfDaySelect(form: FormGroup, value: ITimePickerModel): FormGroup {
        form.get("scheduleTimeOfDay").setErrors({ timeOfDayHasError : value.hasError ? true : null });
        if (!value.hasError) form.get("scheduleTimeOfDay").setValue(value.time);
        form.markAsDirty();
        return form;
    }

    setConditionGroupSetting(form: FormGroup, value: ConditionGroupSetting): FormGroup {
        const conditionGroups = form.get("conditionGroups") as FormArray;
        form.get("conditionGroupSetting").setValue(value);
        const conditionGroupsUpdated = this.setLogicalOperatorModel(conditionGroups.value, value);
        conditionGroups.setValue(conditionGroupsUpdated);
        form.markAsDirty();
        return form;
    }

    addConditionGroup(form: FormGroup): FormGroup {
        const conditionGroups = form.get("conditionGroups") as FormArray;
        const conditionGroupSetting = conditionGroups.length + 1 === 2 ? ConditionGroupSetting.ALL : form.get("conditionGroupSetting").value;
        const conditionGroupsUpdated = conditionGroups.value.map((g) => ({...g, logicalOperator: ConditionGroupSettingToLogicalOperator[conditionGroupSetting]}));
        const newConditionGroup = initRuleConditionGroup(new RuleConditionGroup());
        form.get("conditionGroupSetting").setValue(conditionGroupSetting);
        conditionGroups.setValue(conditionGroupsUpdated);
        conditionGroups.push(newConditionGroup);
        form.markAsDirty();
        return form;
    }

    onTriggerSelect(form: FormGroup, groupIndex: number, conditionIndex: number, field: RuleTriggerTypes): FormGroup {
        const conditionGroups = form.get("conditionGroups") as FormArray;
        const conditions = conditionGroups.at(groupIndex).get("conditions") as FormArray;
        const condition = conditions.at(conditionIndex).value;
        conditions.at(conditionIndex).setValue({
            ...condition,
            field,
            operator: null,
            value: null,
            valueType: RuleTriggerOperandType[field],
        });
        // set form validations based on new trigger selected
        conditions.at(conditionIndex).get("value").setValidators(this.getConditionTriggerValidators(field));
        form.markAsDirty();
        return form;
    }

    onOperatorSelect(form: FormGroup, groupIndex: number, conditionIndex: number, value: RuleOperator): FormGroup {
        const conditionGroups = form.get("conditionGroups") as FormArray;
        const conditions = conditionGroups.at(groupIndex).get("conditions") as FormArray;
        const condition = conditions.at(conditionIndex).value;
        conditions.at(conditionIndex).setValue({
            ...condition,
            operator: value,
        });
        form.markAsDirty();
        return form;
    }

    addCondition(form: FormGroup, groupIndex: number): FormGroup {
        const conditionGroups = form.get("conditionGroups") as FormArray;
        const conditions = conditionGroups.at(groupIndex).get("conditions") as FormArray;
        const newCondition = addConditionFormGroup(new RuleCondition());
        conditions.push(newCondition);
        form.markAsDirty();
        return form;
    }

    removeCondition(form: FormGroup, groupIndex: number, conditionIndex: number): FormGroup {
        const conditionGroups = form.get("conditionGroups") as FormArray;
        const conditions = conditionGroups.at(groupIndex).get("conditions") as FormArray;
        conditions.removeAt(conditionIndex);

        if (conditionGroups.controls.length === 1) {
            const conditionsUpdated = conditionGroups.value.map((c) => ({...c, logicalOperator: LogicalOperator.NONE}));
            conditionGroups.setValue(conditionsUpdated);
        }

        form.markAsDirty();
        return form;
    }

    fetchAllActions() {
        this.rulesApiService.fetchAllActions().subscribe((res) => {
            if (res.result) {
                this._ruleActionOptions.next(res.data);
            }
        });
    }

    selectAction(form: FormGroup, type: RuleActionType, index: number): FormGroup {
        const actions: FormArray = form.get("actions") as FormArray;
        let action = actions.at(index).value;
        action = this.prepopulateRuleAction([{ type, parameters: null }], this.ruleContext)[0];
        actions.setControl(index, action);
        form.markAsDirty();
        return form;
    }

    addAction(form: FormGroup): FormGroup {
        const actions = form.get("actions") as FormArray;
        const newAction = this.prepopulateRuleAction([{type: null, parameters: null}], this.ruleContext)[0];
        actions.push(newAction);
        form.markAsDirty();
        return form;
    }

    removeAction(form: FormGroup, groupIndex: number): FormGroup {
        const actions = form.get("actions") as FormArray;
        actions.removeAt(groupIndex);
        form.markAsDirty();
        return form;
    }

    private configureRuleForms(rules: IRule[], context: RuleContext): FormGroup[] {
        const _ruleForms = this.adaptRules(rules);
        const forms = [];
        for (let i = 0; i < _ruleForms.length; i++) {
            forms[i] = this.prepopulateRuleForm(_ruleForms[i], context);
        }
        return forms;
    }

    private adaptRules(rules: IRule[]): IRule[] {
        return rules.map((rule: IRule) => {
            rule.editMode = false;
            rule.expanded = !!rule.expanded;
            rule.conditionGroupSetting = rule.conditionGroups.find((g) => g.logicalOperator === LogicalOperator.AND)
                ? LogicalOperatorToConditionGroupSetting[LogicalOperator.AND]
                : LogicalOperatorToConditionGroupSetting[LogicalOperator.OR];
            return rule;
        });
    }

    private setSpecificationsMap(specs: IRuleSpecification[]): ILabelValue<RuleTriggerTypes, IRuleSpecificationAdapted> {
        return specs.reduce((newMap, obj) => {
            newMap[obj.name] = {...obj, operators: obj.operators.map((op) => RuleOperatorOptions[op])};
            return newMap;
        }, {} as ILabelValue<RuleTriggerTypes, IRuleSpecificationAdapted>);
    }

    private setTriggerOptions(triggers: RuleTriggerTypes[]) {
        this._triggerOptions.next(
            triggers.map((t) => RuleTriggerOptions[t]),
        );
    }

    private newRuleForm(ruleContext: RuleContext): FormGroup {
        const form = this.formBuilder.group({
            actions: this.formBuilder.array(this.prepopulateRuleAction([{ type: null, parameters: null }], ruleContext)),
            active: [true],
            conditionGroupSetting: [ConditionGroupSetting.ANY],
            conditionGroups: this.formBuilder.array(this.prepopulateConditionGroups([new RuleConditionGroup()])),
            editMode: [true],
            expanded: [true],
            id: [EmptyGuid],
            name: ["", [ Validators.required ]],
            sequence: [0],
            scheduleActionOnWeekDayOnly: [false],
            scheduleDayOfMonth: [null],
            scheduleDayOfWeek: [null],
            scheduleFrequency: [RuleScheduleFrequency.ONCE_A_MONTH, Validators.required],
            scheduleTimeOfDay: [null],
        });

        setTimeout(() => {
            const wrapper: Element = document.getElementById("rules-list");
            const forms: any = document.querySelectorAll("rule-form");
            const offset = forms[forms.length - 1].offsetTop;
            wrapper.scroll({
                top: offset - 120,
                behavior: "smooth",
            });
        }, 0);

        return form;
    }

    private prepopulateRuleForm(rule: IRule, ruleContext: RuleContext): FormGroup {
        const form = this.formBuilder.group({
            actions: this.formBuilder.array(this.prepopulateRuleAction(rule.actions, ruleContext)),
            active: [rule.active],
            conditionGroupSetting: [rule.conditionGroupSetting],
            conditionGroups: this.formBuilder.array(this.prepopulateConditionGroups(rule.conditionGroups)),
            editMode: [rule.editMode],
            expanded: [rule.expanded],
            id: [rule.id],
            name: [rule.name, [ Validators.required ]],
            sequence: [rule.sequence],
            scheduleActionOnWeekDayOnly: [rule.scheduleActionOnWeekDayOnly],
            scheduleDayOfMonth: [rule.scheduleDayOfMonth],
            scheduleDayOfWeek: [rule.scheduleDayOfWeek],
            scheduleFrequency: [rule.scheduleFrequency, Validators.required],
            scheduleTimeOfDay: [rule.scheduleTimeOfDay],
        });
        return form;
    }

    private prepopulateConditionGroups(conditionGroups: IConditionGroup[]): FormGroup[] {
        return conditionGroups.map((group) => {
            return this.formBuilder.group({
                logicalOperator: [group.logicalOperator],
                conditions: this.formBuilder.array(this.prepopulateConditions(group.conditions)),
            });
        });
    }

    private prepopulateConditions(conditions: ICondition[]): FormGroup[] {
        return conditions.map((condition) => {
            return this.formBuilder.group({
                field: [condition.field, Validators.required],
                operator: [condition.operator, Validators.required],
                value: [condition.value, Validators.compose(
                    this.getConditionTriggerValidators(condition.field),
                )],
                valueType: [condition.valueType],
                logicalOperator: [condition.logicalOperator],
            });
        });
    }

    private prepopulateRuleAction(ruleActions: IRuleAction[], ruleContext: RuleContext): FormGroup[] {
        return ruleActions.map((ruleAction) => {
            const type = this.setActionType(ruleAction.type, ruleContext);
            return this.formBuilder.group({
                type: [type, Validators.required],
                parameters: this.setActionParams(type, ruleAction.parameters),
            });
        });
    }

    private setActionType(type: RuleActionType, ruleContext: RuleContext): RuleActionType {
        return type || RuleActionTypeDefaultByContext[ruleContext];
    }

    private setActionParams(type: RuleActionType, params: any = null): FormGroup {
        switch (type) {
            case RuleActionType.SEND_ASSESSMENT:
                return params ? this.prepopulateSendAssessmentForm(params as IRuleActionCopyAssessment) : this.newSendAssessmentForm();
            case RuleActionType.SEND_EMAIL:
                return params ? this.prepopulateSendEmailForm(params as IRuleActionSendEmail) : this.newSendEmailForm();
        }
    }

    private newSendAssessmentForm(): FormGroup {
        return this.formBuilder.group({
            approvers: [null],
            respondentComments: [null],
            copyComments: [true],
            copyPreviousAssessment: [true],
            copyResponses: [true],
            deadline: [null, Validators.compose([
                Validators.min(1),
                Validators.max(999),
            ])],
            description: [null],
            latestTemplateVersion: [true],
            reminder: [null, Validators.compose([
                Validators.min(1),
                Validators.max(999),
            ])],
            respondents: [null],
            reviewChangedResponses: [true],
            sendAssessment: [true],
            templateRootVersionId: [null, Validators.required],
            usePreviousAssessmentRespondents: [true],
            usePrimaryContactAsRespondent: [false],
        }, { validator: validateSendAssessmentForm });
    }

    private prepopulateSendAssessmentForm(params: IRuleActionCopyAssessment): FormGroup {
        return this.formBuilder.group({
            approvers: [params.approvers],
            respondentComments: [params.respondentComments],
            copyComments: [params.copyComments],
            copyPreviousAssessment: [params.copyPreviousAssessment],
            copyResponses: [params.copyResponses],
            deadline: [params.deadline, Validators.compose([
                Validators.min(1),
                Validators.max(999),
            ])],
            description: [params.description],
            latestTemplateVersion: [params.latestTemplateVersion],
            reminder: [params.reminder, Validators.compose([
                Validators.min(1),
                Validators.max(999),
            ])],
            respondents: [params.respondents],
            reviewChangedResponses: [params.reviewChangedResponses],
            sendAssessment: [params.sendAssessment],
            templateRootVersionId: [params.templateRootVersionId, Validators.required],
            usePreviousAssessmentRespondents: [params.usePreviousAssessmentRespondents],
            usePrimaryContactAsRespondent: [params.usePrimaryContactAsRespondent],
        }, { validator: validateSendAssessmentForm });
    }

    private newSendEmailForm(): FormGroup {
        return this.formBuilder.group({
            recipients: [null],
            messageTypeId: [null, Validators.required],
            usePrimaryVendorContact: [true],
        }, { validator: validateSendEmailForm });
    }

    private prepopulateSendEmailForm(params: IRuleActionSendEmail): FormGroup {
        return this.formBuilder.group({
            recipients: [params.recipients],
            messageTypeId: [params.messageTypeId, Validators.required],
            usePrimaryVendorContact: [params.usePrimaryVendorContact],
        }, { validator: validateSendEmailForm });
    }

    private isAnyRuleInEditMode(forms: FormGroup[]): boolean {
        if (!forms || !forms.length) return false;
        return !!forms.find((form) => form.value.editMode);
    }

    private getRuleInEditMode(forms: FormGroup[]): FormGroup {
        if (!forms || !forms.length) return null;
        return forms.find((form) => form.value.editMode) || null;
    }

    private editRule(ruleId: string) {
        this.setRuleContextMenuOptions(false);
        this._ruleForms.next(
            this.ruleForms.map((form) => {
                if (form.get("id").value === ruleId) {
                    form.get("editMode").setValue(true);
                    form.get("expanded").setValue(true);
                    return form;
                } else {
                    return form;
                }
            }),
        );
    }

    private runRule(ruleId: string) {
        this.otModalService.confirm({
            type: ConfirmModalType.WARNING,
            translations: {
                title: this.translations["RunRule"],
                desc: "",
                confirm: this.translations["AreYouSureYouWishToRunThisRule"],
                submit: this.translations["RunRule"],
                cancel: this.translations["Cancel"],
            },
        }).pipe(
            take(1),
            filter((result: boolean) => result),
        ).subscribe(() => {
            this.rulesApiService.runRule(ruleId).subscribe((res: IProtocolResponse<IRule>) => res);
        });
    }

    private deleteRule(rule: IRule) {
        const translation = this.translate.instant(["AssessmentDeleteConfirmation"], { ASSESSMENT_NAME: rule.name });
        this.otModalService.confirm({
            type: ConfirmModalType.DELETE,
            translations: {
                title: this.translations["DeleteRule"],
                desc: translation["AssessmentDeleteConfirmation"],
                confirm: this.translations["DeleteRuleConfirmationText"],
                submit: this.translations["Confirm"],
                cancel: this.translations["Cancel"],
            },
        }).pipe(
            take(1),
            filter((result: boolean) => result),
        ).subscribe(() => {
            if (rule.id !== EmptyGuid) {
                this.rulesApiService.deleteRule(rule.id).subscribe((res: IProtocolResponse<IRule[]>) => {
                    if (res.result) {
                        const rules = this.ruleForms.filter((form) => form.get("id").value !== rule.id);
                        this.setRuleContextMenuOptions(rule.editMode || !this.isAnyRuleInEditMode(rules));
                        this._ruleForms.next(rules);
                    }
                });
            }
        });
    }

    private updateRulesActivation(ruleId: string, active: boolean) {
        this._ruleForms.next(
            this.ruleForms.map((form) => {
                if (form.get("id").value === ruleId) {
                    form.get("active").setValue(active);
                    return form;
                } else {
                    return form;
                }
            }),
        );
    }

    private setLogicalOperatorModel(conditionGroups: IConditionGroup[], value: ConditionGroupSetting): IConditionGroup[] {
        const lastIndex = conditionGroups.length - 1;
        return conditionGroups.map((group, i) => {
            group.logicalOperator = i === lastIndex ? LogicalOperator.NONE : ConditionGroupSettingToLogicalOperator[value];
            return group;
        });
    }

    private getConditionTriggerValidators(field: RuleTriggerTypes): ValidatorFn[] {
        switch (field) {
            case RuleTriggerTypes.CONTRACT_EXPIRING_DATE_IN_DAYS:
            case RuleTriggerTypes.LAST_ASSESSMENT_DATE:
                return [
                    Validators.required,
                    Validators.min(1),
                    Validators.max(9999),
                ];
            default:
                return [];
        }
    }

}
