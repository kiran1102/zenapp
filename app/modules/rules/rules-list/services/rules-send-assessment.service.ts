import { Injectable } from "@angular/core";
import { IOrgUserAdapted } from "interfaces/org-user.interface";
import { Regex } from "constants/regex.constant";

@Injectable()
export class RulesSendAssessmentService {

    mapRespondentsToOrgUser(users: Array<{ respondentId: string; respondentName: string; }>): IOrgUserAdapted[] {
        if (!users || !users.length) return [];
        return users.map((user) => ({ Id: user.respondentId, FullName: user.respondentName }));
    }

    mapApproversToOrgUser(users: Array<{ approverId: string; approverName: string; }>): IOrgUserAdapted[] {
        if (!users || !users.length) return [];
        return users.map((user) => ({ Id: user.approverId, FullName: user.approverName }));
    }

    mapOrgUsersToRespondents(users: IOrgUserAdapted[]): Array<{ respondentId: string; respondentName: string; }> {
        if (!users || !users.length) return null;
        return users.map((user) => ({
            respondentId: user.Id && user.Id.match(Regex.GUID) ? user.Id : null,
            respondentName: user.FullName,
        }));
    }

    mapOrgUserToApprovers(users: IOrgUserAdapted[]): Array<{ approverId: string; approverName: string; }> {
        if (!users || !users.length) return null;
        return users.map((user) => ({ approverId: user.Id, approverName: user.FullName }));
    }
}
