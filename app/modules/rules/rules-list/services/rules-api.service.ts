// Core
import { Injectable } from "@angular/core";

// RxJs
import { Observable, from } from "rxjs";

// Interfaces
import {
    IProtocolResponse,
    IProtocolConfig,
    IProtocolMessages,
} from "interfaces/protocol.interface";
import {
    IRule,
    IRuleSpecification,
} from "modules/rules/interfaces/rules.interface";

// Services
import { ProtocolService } from "modules/core/services/protocol.service";
import { TranslateService } from "@ngx-translate/core";

@Injectable()
export class RulesApiService {

    translations = {};
    translationKeys = [
        "Rule.ErrorFetching",
        "Rule.ErrorSpecifications",
        "Rule.ErrorSaving",
        "RuleSavedSuccessfully",
        "ErrorDeletingRules",
        "RulesDeletedSuccessfully",
        "ErrorActivatingRule",
        "SuccessActivatingRule",
        "ErrorDeactivatingRule",
        "SuccessDeactivatingRule",
        "ErrorRunningRule",
        "SuccessRunningRule",
    ];

    constructor(
        private protocolService: ProtocolService,
        private translate: TranslateService,
    ) {
        this.translate.stream(this.translationKeys).subscribe((t) => {
            this.translations = t;
        });
    }

    getRules(): Observable<IProtocolResponse<IRule[]>> {
        const config: IProtocolConfig = this.protocolService.customConfig("GET", "/api/rule-engine/v1/rules");
        const messages: IProtocolMessages = { Error: { custom: this.translations["Rule.ErrorFetching"] } };
        return from(this.protocolService.http(config, messages));
    }

    getSpecifications(): Observable<IProtocolResponse<IRuleSpecification[]>> {
        const config: IProtocolConfig = this.protocolService.customConfig("GET", "/api/rule-engine/v1/rules/specifications");
        const messages: IProtocolMessages = { Error: { custom: this.translations["Rule.ErrorSpecifications"]} };
        return from(this.protocolService.http(config, messages));
    }

    addRule(payload: IRule): Observable<IProtocolResponse<IRule[]>> {
        const config: IProtocolConfig = this.protocolService.customConfig("POST", "/api/rule-engine/v1/rules", null, payload);
        const messages: IProtocolMessages = {
            Error: { custom: this.translations["Rule.ErrorSaving"] },
            Success: { custom: this.translations["RuleSavedSuccessfully"] },
        };
        return from(this.protocolService.http(config, messages));
    }

    updateRule(payload: IRule): Observable<IProtocolResponse<IRule[]>> {
        const config: IProtocolConfig = this.protocolService.customConfig("PUT", `/api/rule-engine/v1/rules/${payload.id}/rules`, null, payload);
        const messages: IProtocolMessages = {
            Error: { custom: this.translations["Rule.ErrorSaving"] },
            Success: { custom: this.translations["RuleSavedSuccessfully"] },
        };
        return from(this.protocolService.http(config, messages));
    }

    deleteRule(ruleId: string): Observable<IProtocolResponse<IRule[]>> {
        const config: IProtocolConfig = this.protocolService.customConfig("DELETE", `/api/rule-engine/v1/rules/${ruleId}/rules`);
        const messages: IProtocolMessages = {
            Error: { custom: this.translations["ErrorDeletingRules"] },
            Success: { custom: this.translations["RulesDeletedSuccessfully"] },
        };
        return from(this.protocolService.http(config, messages));
    }

    activateRule(ruleId: string): Observable<IProtocolResponse<IRule[]>> {
        const config: IProtocolConfig = this.protocolService.customConfig("POST", `/api/rule-engine/v1/rules/${ruleId}/rules/activation`);
        const messages: IProtocolMessages = {
            Error: { custom: this.translations["ErrorActivatingRule"] },
            Success: { custom: this.translations["SuccessActivatingRule"] },
        };
        return from(this.protocolService.http(config, messages));
    }

    deactivateRule(ruleId: string): Observable<IProtocolResponse<IRule[]>> {
        const config: IProtocolConfig = this.protocolService.customConfig("POST", `/api/rule-engine/v1/rules/${ruleId}/rules/deactivation`);
        const messages: IProtocolMessages = {
            Error: { custom: this.translations["ErrorDeactivatingRule"] },
            Success: { custom: this.translations["SuccessDeactivatingRule"] },
        };
        return from(this.protocolService.http(config, messages));
    }

    runRule(ruleId: string): Observable<IProtocolResponse<IRule>> {
        const config: IProtocolConfig = this.protocolService.customConfig("POST", `/api/rule-engine/v1/rules/${ruleId}/triggers/rules`);
        const messages: IProtocolMessages = {
            Error: { custom: this.translations["ErrorRunningRule"] },
            Success: { custom: this.translations["SuccessRunningRule"] },
        };
        return from(this.protocolService.http(config, messages));
    }

    fetchAllActions(): Observable<IProtocolResponse<IRule[]>> {
        const config: IProtocolConfig = this.protocolService.customConfig("GET", `/api/rule-engine/v1/rules/domains/VENDORS/actions`);
        const messages: IProtocolMessages = { Error: { custom: this.translations["Rule.ErrorFetchingActions"] } };
        return from(this.protocolService.http(config, messages));
    }
}
