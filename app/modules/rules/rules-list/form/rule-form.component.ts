// Angular
import {
    Component,
    Input,
    ChangeDetectionStrategy,
    OnInit,
    OnDestroy,
} from "@angular/core";
import { FormGroup, FormArray } from "@angular/forms";

// Rxjs
import { Subject } from "rxjs";
import { takeUntil, debounceTime } from "rxjs/operators";

// Constants & Enums
import {
    RuleContext,
    RuleActionType,
    ConditionGroupSetting,
    RuleTriggerTypes,
    RuleOperator,
    RuleOperandType,
} from "modules/rules/enums/rules.enums";
import {
    RuleActionOptions,
    RuleActionOptionsByContext,
    RuleOperatorOptions,
    RuleTriggerOptions,
    RuleTranslations,
    RuleConditionOptions,
} from "modules/rules/constants/rules.constant";
import {
    RuleDaysOfWeekOption,
    RuleDaysOfWeekOptions,
    RuleFrequencyOption,
    RuleFrequencyOptions,
    DayOfTheMonthOptions,
    DayOfTheMonthOption,
} from "modules/rules/constants/rules-frequency.constant";
import {
    RuleScheduleFrequency,
    RuleDaysOfTheWeek,
} from "modules/rules/enums/rules-frequency.enums";

// Services
import { RulesFormService } from "modules/rules/rules-list/services/rules-form.service";

// Interfaces
import {
    ContextMenuType,
    ILabelValue,
    ITimePickerModel,
} from "@onetrust/vitreus";
import { IRuleSpecificationAdapted } from "modules/rules/interfaces/rules.interface";
import { RuleTriggerValidations } from "modules/rules/constants/rules-validation.constant";

@Component({
    selector: "rule-form",
    templateUrl: "./rule-form.component.html",
    host: {
        class: "flex-full-height centered-max-80",
    },
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class RuleFormComponent implements OnInit, OnDestroy {

    @Input() ruleContext: RuleContext;
    @Input() form: FormGroup;
    @Input() ruleIndex: number;

    collapseFrequencyInputs = true;
    contextMenuType = ContextMenuType;
    dayOfTheMonthOption = DayOfTheMonthOption;
    dayOfTheMonthOptions = DayOfTheMonthOptions.slice(0, 28);
    frequency = RuleScheduleFrequency;
    ruleActionOptions = RuleActionOptions;
    ruleActionOptionsByContext = RuleActionOptionsByContext;
    ruleActionType = RuleActionType;
    ruleConditionOptions = RuleConditionOptions;
    ruleDaysOfWeekOption = RuleDaysOfWeekOption;
    ruleDaysOfWeekOptions = RuleDaysOfWeekOptions;
    ruleFrequencyOption = RuleFrequencyOption;
    ruleFrequencyOptions = RuleFrequencyOptions;
    ruleMenuOptions$ = this.rulesFormService.ruleMenuOptions$;
    ruleOperandType = RuleOperandType;
    ruleOperatorOptions = RuleOperatorOptions;
    ruleTranslations = RuleTranslations;
    ruleTriggerTypes = RuleTriggerTypes;
    ruleTriggerOptions = RuleTriggerOptions;
    triggerValidations = RuleTriggerValidations;
    specificationsMap$;
    specificationsMap: ILabelValue<RuleTriggerTypes, IRuleSpecificationAdapted>;
    toggleClick$ = new Subject<boolean>();
    triggerOptions$ = this.rulesFormService.triggerOptions$;
    ruleActionOptions$ = this.rulesFormService.ruleActionOptions$;

    private destroy$ = new Subject();

    constructor(
        private rulesFormService: RulesFormService,
    ) {}

    ngOnInit() {
        this.specificationsMap$ = this.rulesFormService.specificationsMap$
            .subscribe((specs) => {
                this.specificationsMap = specs;
            });

        this.toggleClick$.asObservable().pipe(
                takeUntil(this.destroy$),
                debounceTime(400),
            ).subscribe((event: boolean) => {
                if (event !== this.active.value) {
                    this.rulesFormService.setRuleActivation(this.id.value, event);
                }
            });
    }

    ngOnDestroy() {
        this.destroy$.next();
        this.destroy$.complete();
    }

    trackByFn(index: number) {
        return index;
    }

    onExpandClick() {
        this.rulesFormService.expandOne(this.id.value);
    }

    onCollapseClick() {
        this.rulesFormService.collapseOne(this.id.value);
    }

    cancelEdit() {
        this.rulesFormService.cancelEdit();
    }

    onCollapseFrequencyClick() {
        this.collapseFrequencyInputs = !this.collapseFrequencyInputs;
    }

    get actions() { return this.form.get("actions") as FormArray; }
    get active() { return this.form.get("active"); }
    get conditionGroupSetting() { return this.form.get("conditionGroupSetting"); }
    get conditionGroups() { return this.form.get("conditionGroups") as FormArray; }
    get editMode() { return this.form.get("editMode"); }
    get expanded() { return this.form.get("expanded"); }
    get id() { return this.form.get("id"); }
    get name() { return this.form.get("name"); }
    get sequence() { return this.form.get("sequence"); }
    get scheduleActionOnWeekDayOnly() { return this.form.get("scheduleActionOnWeekDayOnly"); }
    get scheduleDayOfMonth() { return this.form.get("scheduleDayOfMonth"); }
    get scheduleDayOfWeek() { return this.form.get("scheduleDayOfWeek"); }
    get scheduleFrequency() { return this.form.get("scheduleFrequency"); }
    get scheduleTimeOfDay() { return this.form.get("scheduleTimeOfDay"); }

    onActiveToggleClick(event: boolean) {
        if (this.editMode.value) {
            this.active.setValue(event);
            this.form.markAsDirty();
        } else {
            this.toggleClick$.next(event);
        }
    }

    onScheduleFrequencySelect(event: ILabelValue<RuleScheduleFrequency>) {
        if (!this.editMode.value || this.scheduleFrequency.value === event.value) return;
        this.form = this.rulesFormService.onScheduleFrequencySelect(this.form, event.value);
    }

    onDayOfTheMonthSelect(event: ILabelValue<number>) {
        if (!this.editMode.value || this.scheduleDayOfMonth.value === event.value) return;
        this.form = this.rulesFormService.onDayOfTheMonthSelect(this.form, event.value);
    }

    onScheduleDayOfWeekSelect(event: ILabelValue<RuleDaysOfTheWeek>) {
        if (!this.editMode.value || this.scheduleDayOfWeek.value === event.value) return;
        this.form = this.rulesFormService.onScheduleDayOfWeekSelect(this.form, event.value);
    }

    onTimeOfDaySelect(event: ITimePickerModel) {
        if (!this.editMode.value || this.scheduleTimeOfDay.value === event.time) return;
        this.form = this.rulesFormService.onTimeOfDaySelect(this.form, event);
    }

    onToggleWeekdayOnly(event: boolean) {
        if (!this.editMode.value) return;
        this.form = this.rulesFormService.onChangeWeekdayOnly(this.form, event);
    }

    onConditionOptionSelect(event: ILabelValue<string, ConditionGroupSetting>) {
        if (!this.editMode.value || this.conditionGroupSetting.value === event.value) return;
        this.form = this.rulesFormService.setConditionGroupSetting(this.form, event.value);
    }

    onConditionGroupAdd() {
        if (!this.editMode.value) return;
        this.form = this.rulesFormService.addConditionGroup(this.form);
    }

    onConditionGroupRemove(groupIndex: number) {
        if (!this.editMode.value) return;
        this.form = this.rulesFormService.removeConditionGroup(this.form, groupIndex);
    }

    onTriggerChange(groupIndex: number, conditionIndex: number, event: ILabelValue<string, RuleTriggerTypes>) {
        if (!this.editMode.value) return;
        this.form = this.rulesFormService.onTriggerSelect(this.form, groupIndex, conditionIndex, event.value);
    }

    onOperatorChange(groupIndex: number, conditionIndex: number, event: ILabelValue<string, RuleOperator>) {
        if (!this.editMode.value) return;
        this.form = this.rulesFormService.onOperatorSelect(this.form, groupIndex, conditionIndex, event.value);
    }

    addCondition(groupIndex: number) {
        if (!this.editMode.value) return;
        this.form = this.rulesFormService.addCondition(this.form, groupIndex);
    }

    removeCondition(groupIndex: number, conditionIndex: number) {
        if (!this.editMode.value) return;
        this.form = this.rulesFormService.removeCondition(this.form, groupIndex, conditionIndex);
    }

    selectAction(event: ILabelValue<string, RuleActionType>, index: number) {
        if (!this.editMode.value) return;
        this.form = this.rulesFormService.selectAction(this.form, event.value, index);
    }

    addAction() {
        if (!this.editMode.value) return;
        this.form = this.rulesFormService.addAction(this.form);
    }

    removeAction(groupIndex: number) {
        if (!this.editMode.value) return;
        this.form = this.rulesFormService.removeAction(this.form, groupIndex);
    }
}
