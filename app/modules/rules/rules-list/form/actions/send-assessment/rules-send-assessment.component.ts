// Angular
import { Component, Output, EventEmitter, Input } from "@angular/core";
import { FormGroup } from "@angular/forms";

// Services
import { RulesSendAssessmentService } from "modules/rules/rules-list/services/rules-send-assessment.service";

// Interfaces
import { ITemplateNameValue } from "modules/template/interfaces/template.interface";
import { IOrgUserAdapted } from "interfaces/org-user.interface";

// Enums / Constants
import { RulesSendAssessmentToggles } from "modules/rules/enums/rules-actions.enums";
import {
    RuleActionType,
    RuleContext,
} from "modules/rules/enums/rules.enums";
import { OrgUserTraversal } from "enums/org-user-dropdown.enum";
import {
    RulesSendAssessmentValidations,
    PrimaryContactAsRespondentText,
} from "modules/rules/constants/rules-send-assessment.constant";

@Component({
    selector: "rules-send-assessment",
    templateUrl: "./rules-send-assessment.component.html",
})
export class RulesSendAssessmentComponent {
    @Input() editMode = false;
    @Input() assessmentForm: FormGroup;
    @Input() ruleContext = RuleContext;
    @Input() type: RuleActionType;

    @Output() templateChanged = new EventEmitter();

    approverList: IOrgUserAdapted[] = [];
    enteredComments: string;
    enteredDescription: string;
    primaryContactAsRespondentText = PrimaryContactAsRespondentText;
    reminderInput: string;
    respondentList: IOrgUserAdapted[] = [];
    selectedTemplate: ITemplateNameValue;
    showAdvancedOptions: boolean;
    toggleType = RulesSendAssessmentToggles;
    validations = RulesSendAssessmentValidations;
    orgUserTraversal = OrgUserTraversal;

    constructor(
        private rulesSendAssessmentService: RulesSendAssessmentService,
    ) {}

    ngOnInit() {
        if (this.assessmentForm) {
            this.approverList = this.rulesSendAssessmentService.mapApproversToOrgUser(this.assessmentForm.get("approvers").value);
            this.respondentList = this.rulesSendAssessmentService.mapRespondentsToOrgUser(this.assessmentForm.get("respondents").value);
        }
    }

    get templateRootVersionId() { return this.assessmentForm.get("templateRootVersionId"); }
    get deadline() { return this.assessmentForm.get("deadline"); }
    get reminder() { return this.assessmentForm.get("reminder"); }
    get description() { return this.assessmentForm.get("description"); }
    get approvers() { return this.assessmentForm.get("approvers"); }
    get respondents() { return this.assessmentForm.get("respondents"); }
    get respondentComments() { return this.assessmentForm.get("respondentComments"); }
    get copyPreviousAssessment() { return this.assessmentForm.get("copyPreviousAssessment"); }
    get copyResponses() { return this.assessmentForm.get("copyResponses"); }
    get copyComments() { return this.assessmentForm.get("copyComments"); }
    get latestTemplateVersion() { return this.assessmentForm.get("latestTemplateVersion"); }
    get reviewChangedResponses() { return this.assessmentForm.get("reviewChangedResponses"); }
    get sendAssessment() { return this.assessmentForm.get("sendAssessment"); }
    get usePreviousAssessmentRespondents() { return this.assessmentForm.get("usePreviousAssessmentRespondents"); }
    get usePrimaryContactAsRespondent() { return this.assessmentForm.get("usePrimaryContactAsRespondent"); }

    toggleAdvancedOptions(): void {
        this.showAdvancedOptions = !this.showAdvancedOptions;
    }

    onTemplateSelection(template: ITemplateNameValue) {
        this.selectedTemplate = template;

        if (!template) {
            this.assessmentForm.get("templateRootVersionId").setValue(null);
            return;
        }

        this.templateRootVersionId.setValue(template ? template.id : null);
        this.assessmentForm.markAsDirty();
    }

    onDescriptionChange(description: string) {
        this.description.setValue(description.trim());
        this.assessmentForm.markAsDirty();
    }

    onCommentsChange(respondentComments: string) {
        this.respondentComments.setValue(respondentComments.trim());
        this.assessmentForm.markAsDirty();
    }

    onDeadlineChange(value: number) {
        if (typeof value !== "number" || !value) this.reminder.setValue(null);
        this.deadline.setValue(typeof value !== "number" ? null : value);
        this.assessmentForm.markAsDirty();
    }

    onReminderChange(value: number) {
        this.reminder.setValue(typeof value !== "number" ? null : value);
        this.assessmentForm.markAsDirty();
    }

    onSelectApprovers(approverList: IOrgUserAdapted[]) {
        this.approvers.setValue(this.rulesSendAssessmentService.mapOrgUserToApprovers(approverList));
        this.assessmentForm.markAsDirty();
    }

    onSelectRespondents(respondentList: IOrgUserAdapted[]) {
        this.respondents.setValue(this.rulesSendAssessmentService.mapOrgUsersToRespondents(respondentList));
        this.assessmentForm.markAsDirty();
    }

    onToggle(event: boolean, toggleType: RulesSendAssessmentToggles) {
        switch (toggleType) {
            case RulesSendAssessmentToggles.COPY_RESPONSES:
                this.copyResponses.setValue(event);
                break;
            case RulesSendAssessmentToggles.COPY_COMMENTS:
                this.copyComments.setValue(event);
                break;
            case RulesSendAssessmentToggles.LATEST_TEMPLATE_VERSION:
                this.latestTemplateVersion.setValue(event);
                break;
            case RulesSendAssessmentToggles.REVIEW_CHANGED_RESPONSES:
                this.reviewChangedResponses.setValue(event);
                break;
            case RulesSendAssessmentToggles.SEND_NEW_ASSESSMENT_FALLBACK:
                this.sendAssessment.setValue(event);
                break;
            case RulesSendAssessmentToggles.USE_PREVIOUS_RESPONDENT:
                this.usePreviousAssessmentRespondents.setValue(event);
                break;
            case RulesSendAssessmentToggles.USE_PRIMARY_CONTACT_RESPONDENT:
                this.usePrimaryContactAsRespondent.setValue(event);
                break;
        }
        this.assessmentForm.markAsDirty();
    }

    onToggleCopyPreviousAssessment(event: boolean) {
        this.copyPreviousAssessment.setValue(event);
        this.copyResponses.setValue(event);
        this.copyComments.setValue(event);
        this.latestTemplateVersion.setValue(event);
        this.sendAssessment.setValue(event);
        this.usePreviousAssessmentRespondents.setValue(event);
        this.assessmentForm.markAsDirty();
    }
}
