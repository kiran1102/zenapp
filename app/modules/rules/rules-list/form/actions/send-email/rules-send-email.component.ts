// Angular
import {
    Component,
    Input,
    Output,
    EventEmitter,
} from "@angular/core";
import { FormGroup } from "@angular/forms";

// Services
import { RulesSendEmailService } from "modules/rules/rules-list/services/rules-send-email.service";
import { SmtpConnectionsTemplateListService } from "settingsComponents/smtp-connections-list/smtp-connections-template-list.service";
import { TranslateService } from "@ngx-translate/core";

// Interfaces
import { IOrgUserAdapted } from "interfaces/org-user.interface";
import { ILabelValue } from "interfaces/generic.interface";
import { ITableData } from "interfaces/smtp-settings.interface";

// Enums / Constants
import { RulesSendEmailToggles } from "modules/rules/enums/rules-actions.enums";
import {
    RuleContext,
    RuleActionType,
} from "modules/rules/enums/rules.enums";
import { OrgUserTraversal } from "enums/org-user-dropdown.enum";
import { RuleEmailTemplate } from "modules/rules/constants/rules.constant";

// rxjs
import { take } from "rxjs/operators";

@Component({
    selector: "rules-send-email",
    templateUrl: "./rules-send-email.component.html",
})
export class RulesSendEmailComponent {

    @Input() editMode = false;
    @Input() emailForm: FormGroup;
    @Input() ruleContext: RuleContext;
    @Input() type: RuleActionType;

    @Output() templateChanged = new EventEmitter();

    recipientList: IOrgUserAdapted[] = [];
    toggleType = RulesSendEmailToggles;
    emailTemplates: Array<ILabelValue<string>> = [];
    emailTemplateSelected: ILabelValue<string>;
    orgUserTraversal = OrgUserTraversal;

    constructor(
        private rulesSendEmailService: RulesSendEmailService,
        private smtpConnectionsTemplateListService: SmtpConnectionsTemplateListService,
        private translate: TranslateService,
    ) {}

    ngOnInit() {
        this.fetchEmailTemplates();
        this.recipientList = this.rulesSendEmailService.mapRecipientsToOrgUser(this.emailForm.get("recipients").value);
    }

    get recipients() { return this.emailForm.get("recipients"); }
    get messageTypeId() { return this.emailForm.get("messageTypeId"); }
    get usePrimaryVendorContact() { return this.emailForm.get("usePrimaryVendorContact"); }

    selectRecipients(recipientList: IOrgUserAdapted[]) {
        this.recipients.setValue(this.rulesSendEmailService.mapOrgUserToRecipients(recipientList));
        this.emailForm.markAsDirty();
    }

    togglePrimaryContact(event: boolean, toggleType: RulesSendEmailToggles) {
        switch (toggleType) {
            case RulesSendEmailToggles.USE_PRIMARY_VENDOR_CONTACT:
                this.usePrimaryVendorContact.setValue(event);
                break;
        }
        this.emailForm.markAsDirty();
    }

    selectTemplate(selection: ILabelValue<string>) {
        this.emailTemplateSelected = selection;
        this.messageTypeId.setValue(selection ? selection.value : null);
        this.emailForm.markAsDirty();
    }

    private fetchEmailTemplates() {
        this.smtpConnectionsTemplateListService.getGroupFilterListFiltered(null, null, [RuleEmailTemplate[this.ruleContext]])
        .pipe(take(1))
        .subscribe((response: ITableData) => {
            this.emailTemplates = response.content.map((e) => ({
                label: this.translate.instant(e.templateNameTranslationKey),
                value: e.messageTypeId.toString(),
            }));
            if (this.messageTypeId && this.messageTypeId.value) {
                this.emailTemplateSelected = this.emailTemplates.find((e) => e.value === this.messageTypeId.value.toString());
            }
        });
    }
}
