// Angular
import {
    Component,
    Input,
} from "@angular/core";
import { FormGroup } from "@angular/forms";

// RXJS
import { Subject } from "rxjs/Subject";
import { take, filter, takeUntil } from "rxjs/operators";

// Constants & Enums
import { RuleContext } from "modules/rules/enums/rules.enums";

// Services
import { TranslateService } from "@ngx-translate/core";
import { RulesFormService } from "modules/rules/rules-list/services/rules-form.service";
import { RulesApiService } from "modules/rules/rules-list/services/rules-api.service";
import { EmptyGuid } from "constants/empty-guid.constant";
import { OtModalService, ConfirmModalType } from "@onetrust/vitreus";

@Component({
    selector: "rules-list",
    templateUrl: "./rules-list.component.html",
    host: {
        class: "flex-full-height full-width",
    },
})
export class RulesListComponent {

    @Input() ruleContext: RuleContext;
    @Input() ruleForms: FormGroup[];

    translations: {};
    translateKeys = [
        "CancelEdit",
        "DoYouWantToDiscardTheseChangesAndContinue",
        "Cancel",
        "DiscardChanges",
    ];
    destroy$ = new Subject();
    editingForm: FormGroup;
    isSaving = false;
    ruleForms$ = this.rulesFormService.ruleForms$;
    form$ = this.rulesFormService.form$;
    rulesEditMode$ = this.rulesFormService.rulesEditMode$;
    editModeForm$ = this.rulesFormService.editModeForm$;

    constructor(
        private rulesApiService: RulesApiService,
        private rulesFormService: RulesFormService,
        private otModalService: OtModalService,
        private translate: TranslateService,
    ) {}

    ngOnInit() {
        this.translate.stream(this.translateKeys).pipe(
            takeUntil(this.destroy$),
        ).subscribe((t) => {
            this.translations = t;
        });

        this.editModeForm$.pipe(
            takeUntil(this.destroy$),
        ).subscribe((form: FormGroup) => {
            this.editingForm = form;
        });
    }

    ngOnDestroy() {
        this.destroy$.next();
        this.destroy$.complete();
    }

    trackByFn(index: number) {
        return index;
    }

    cancelEdit() {
        if (this.editingForm.dirty) {
            this.otModalService.confirm({
                type: ConfirmModalType.WARNING,
                translations: {
                    title: this.translations["CancelEdit"],
                    desc: "",
                    confirm: this.translations["DoYouWantToDiscardTheseChangesAndContinue"],
                    cancel: this.translations["Cancel"],
                    submit: this.translations["DiscardChanges"],
                },
            }).pipe(
                take(1),
                filter((result: boolean) => result),
            ).subscribe(() => {
                this.rulesFormService.cancelEdit();
            });
        } else {
            this.rulesFormService.cancelEdit();
        }
    }

    saveRule() {
        if (!this.editingForm.valid) return;

        const rule = this.editingForm.value;
        this.isSaving = true;

        if (rule.id && rule.id !== EmptyGuid) {
            this.rulesApiService.updateRule(this.editingForm.value).pipe(
                takeUntil(this.destroy$),
            ).subscribe((res) => {
                if (res.result) {
                    this.getRules();
                } else {
                    this.isSaving = false;
                }
            });
        } else {
            this.rulesApiService.addRule(this.editingForm.value).pipe(
                takeUntil(this.destroy$),
            ).subscribe((res) => {
                if (res.result) {
                    this.getRules();
                } else {
                    this.isSaving = false;
                }
            });
        }
    }

    private getRules() {
        this.rulesApiService.getRules().pipe(
            takeUntil(this.destroy$),
        ).subscribe((res) => {
            if (res.result) {
                this.rulesFormService.setRules(res.data, this.ruleContext);
                this.rulesFormService.setRuleContextMenuOptions();
            }
            this.isSaving = false;
        });
    }
}
