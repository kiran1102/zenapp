// Angular
import { NgModule } from "@angular/core";
import { ReactiveFormsModule } from "@angular/forms";

// Modules
import { SharedModule } from "sharedModules/shared.module";
import { CommonModule } from "@angular/common";
import { HttpClientModule } from "@angular/common/http";
import { TranslateModule } from "@ngx-translate/core";
import { FormsModule } from "@angular/forms";
import { VitreusModule } from "@onetrust/vitreus";
import { OTPipesModule } from "modules/pipes/pipes.module";
import { RulesSideMenuModule } from "./side-menu/rules-side-menu.module";
import { RulesListModule } from "./rules-list/rules-list.module";

// components
import { RulesMangerComponent } from "./rules-manager.component";

@NgModule({
    imports: [
        SharedModule,
        CommonModule,
        ReactiveFormsModule,
        HttpClientModule,
        TranslateModule,
        FormsModule,
        VitreusModule,
        OTPipesModule,
        RulesSideMenuModule,
        RulesListModule,
    ],
    declarations: [
        RulesMangerComponent,
    ],
    exports: [
        RulesMangerComponent,
        RulesSideMenuModule,
        RulesListModule,
    ],
})
export class RulesModule { }
