// Modules
import { NgModule } from "@angular/core";
import { VitreusModule } from "@onetrust/vitreus";

// Components
import { TmplContextMenuComponent } from "./tmpl-context-menu.component";

@NgModule({
    imports: [
        VitreusModule,
    ],
    declarations: [
        TmplContextMenuComponent,
    ],
    exports: [
        TmplContextMenuComponent,
    ],
})
export class TmplContextMenu { }
