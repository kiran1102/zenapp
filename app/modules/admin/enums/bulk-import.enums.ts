export enum StatusCodes {
    Pending = 1,
    InProgress = 2,
    Error = 3,
    Completed = 4,
}

export enum BulkImportCards {
    ADD_CONTROLS_TO_LIBRARY = "control_import",
    ADD_CONTROLS_TO_VENDOR = "add_controls_to_vendor",
    CREATE_ASSETS = "create_asset_import",
    CREATE_DATA_ELEMENTS = "create_de_import",
    CREATE_PROCESSING_ACTIVITIES = "create_pa_import",
    CREATE_VENDORS = "create_vendor_import",
    UPDATE_ASSETS = "update_asset_import",
    CREATE_ENTITIES = "create_entity_import",
    UPDATE_PROCESSING_ACTIVITIES = "update_pa_import",
    UPDATE_DATA_ELEMENTS = "update_de_import",
    UPDATE_VENDORS = "update_vendor_import",
    UPDATE_ENTITIES = "update_entity_import",
    CREATE_INVENTORY_LINKS = "inventory_linking_import",
    CREATE_UPDATE_DATA_SUBJECT_CONSENT = "update_ds_consents",
    USERS = "user_import",
}
