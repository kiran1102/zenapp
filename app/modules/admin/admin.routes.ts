import { Permissions } from "modules/shared/services/helper/permissions.service";

export default function routes($stateProvider: ng.ui.IStateProvider) {
    $stateProvider
        .state("zen.app.pia.module.admin", {
            url: "admin/",
            resolve: {
                AdminPermission: [
                    "GeneralData",
                    "$state",
                    "Permissions",
                    (
                        GeneralData: any,
                        $state: ng.ui.IStateService,
                        permissions: Permissions,
                    ): boolean => {
                        if (permissions.canShow("MenuAdmin")) {
                            return true;
                        } else {
                            permissions.goToFallback();
                        }
                        return false;
                    },
                ],
            },
            abstract: true,
            views: {
                module: {
                    template: "<admin-wrapper></admin-wrapper>",
                },
            },
        })
        .state("zen.app.pia.module.admin.users", {
            url: "users",
            abstract: true,
            template: "<ui-view></ui-view>",
            resolve: {
                AdminUsersPermission: [
                    "AdminPermission",
                    "$state",
                    "Permissions",
                    (
                        AdminPermission: boolean,
                        $state: ng.ui.IStateService,
                        permissions: Permissions,
                    ): boolean => {
                        if (permissions.canShow("Users") && sessionStorage.getItem("OneTrust.RefreshUserList")) {
                            $state.go("zen.app.pia.module.admin.users.list", {}, { reload: true });
                            sessionStorage.removeItem("OneTrust.RefreshUserList");
                        } else if (permissions.canShow("Users")) {
                            return true;
                        } else {
                            permissions.goToFallback();
                        }
                        return false;
                    },
                ],
            },
        })
        .state("zen.app.pia.module.admin.users.list", {
            url: "",
            params: { time: null },
            template: "<users-table></users-table>",
            resolve: {
                AdminUsersListPermission: [
                    "AdminUsersPermission",
                    (AdminUsersPermission: boolean): boolean => {
                        return AdminUsersPermission;
                    },
                ],
            },
        })
        .state("zen.app.pia.module.admin.users.create", {
            url: "/create",
            params: { time: null },
            template: `<user-manage-view></user-manage-view>`,
            resolve: {
                AdminUsersListPermission: [
                    "AdminUsersPermission",
                    (AdminUsersPermission: boolean): boolean => {
                        return AdminUsersPermission;
                    },
                ],
            },
        })
        .state("zen.app.pia.module.admin.users.edit", {
            url: "/edit/:id",
            template: `<user-manage-view></user-manage-view>`,
            resolve: {
                AdminUsersEditPermission: [
                    "AdminUsersPermission",
                    (AdminUsersPermission: boolean): boolean => {
                        return AdminUsersPermission;
                    },
                ],
            },
        })
        .state("zen.app.pia.module.admin.user-groups", {
            url: "user-groups",
            abstract: true,
            template: "<ui-view></ui-view>",
            resolve: {
                AdminUserGroupsPermission: [
                    "AdminPermission",
                    "$state",
                    "Permissions",
                    (
                        AdminPermission: boolean,
                        $state: ng.ui.IStateService,
                        permissions: Permissions,
                    ): boolean => {
                        if (permissions.canShow("UserGroups")) {
                            return true;
                        } else {
                            permissions.goToFallback();
                        }
                        return false;
                    },
                ],
            },
        })
        .state("zen.app.pia.module.admin.user-groups.list", {
            url: "?page&size",
            params: { time: null },
            template: "<user-groups-list></user-groups-list>",
            resolve: {
                AdminUsersListPermission: [
                    "AdminUserGroupsPermission",
                    (AdminUserGroupsPermission: boolean): boolean => {
                        return AdminUserGroupsPermission;
                    },
                ],
            },
        })
        .state("zen.app.pia.module.admin.user-groups.manage", {
            url: "/:id",
            params: { time: null },
            template: "<user-groups-manage-users-list></user-groups-manage-users-list>",
            resolve: {
                AdminUsersListPermission: [
                    "AdminUserGroupsPermission",
                    (AdminUserGroupsPermission: boolean): boolean => {
                        return AdminUserGroupsPermission;
                    },
                ],
            },
        })
        .state("zen.app.pia.module.admin.roles", {
            url: "roles",
            abstract: true,
            template: "<ui-view></ui-view>",
            resolve: {
                AdminRolesPermission: [
                    "AdminPermission",
                    "Permissions",
                    (
                        AdminPermission,
                        permissions: Permissions,
                    ): boolean => {
                        if (permissions.canShow("Roles")) {
                            return true;
                        } else {
                            permissions.goToFallback();
                        }
                        return false;
                    },
                ],
            },
        })
        .state("zen.app.pia.module.admin.roles.list", {
            url: "?page&size&sort",
            params: { time: null },
            template: "<roles-mgmt-list></roles-mgmt-list>",
            resolve: {
                AdminRolesListPermission: [
                    "AdminRolesPermission",
                    "Permissions",
                    (
                        AdminRolesPermission,
                        permissions: Permissions,
                    ): boolean => {
                        if (permissions.canShow("RolesList")) {
                            return true;
                        } else {
                            permissions.goToFallback();
                        }
                        return false;
                    },
                ],
            },
        })
        .state("zen.app.pia.module.admin.roles.manage", {
            url: "/manage?base&role",
            params: { time: null },
            template: "<role-manage-view></role-manage-view>",
            resolve: {
                AdminRoleManagePermission: [
                    "AdminRolesPermission",
                    "Permissions",
                    (
                        AdminRolesPermission,
                        permissions: Permissions,
                    ): boolean => {
                        if (permissions.canShow("RolesDetails")) {
                            return true;
                        } else {
                            permissions.goToFallback();
                        }
                        return false;
                    },
                ],
            },
        })
        .state("zen.app.pia.module.admin.organizations", {
            url: "org-group",
            abstract: true,
            template: "<ui-view></ui-view>",
            resolve: {
                AdminOrgsPermission: [
                    "AdminPermission",
                    "Permissions",
                    (
                        AdminPermission,
                        permissions: Permissions,
                    ): boolean => {
                        if (permissions.canShow("Organizations")) {
                            return true;
                        } else {
                            permissions.goToFallback();
                        }
                        return false;
                    },
                ],
            },
        })
        .state("zen.app.pia.module.admin.organizations.list", {
            url: "",
            params: { time: null },
            template: "<org-tree-manage-view></org-tree-manage-view>",
            resolve: {
                AdminOrgsListPermission: [
                    "AdminOrgsPermission",
                    (AdminOrgsPermission: boolean): boolean => {
                        return AdminOrgsPermission;
                    },
                ],
            },
        })
        .state("zen.app.pia.module.admin.organizations.edit", {
            url: "/details/:Id",
            params: { time: null },
            template: "<org-group-edit></org-group-edit>",
            resolve: {
                AdminOrgsEditPermission: [
                    "AdminOrgsPermission",
                    (AdminOrgsPermission: boolean): boolean => {
                        return AdminOrgsPermission;
                    },
                ],
            },
        })
        .state("zen.app.pia.module.admin.bulkimport", {
            url: "",
            abstract: true,
            // template: "<ui-view></ui-view>",
            template: "<downgrade-bulk-import-table></downgrade-bulk-import-table>",
            resolve: {
                AdminImportPermission: [
                    "AdminPermission",
                    (AdminImportPermission: boolean): boolean => {
                        return AdminImportPermission;
                    },
                ],
            },
        })
        .state("zen.app.pia.module.admin.bulkimport.status", {
            url: "bulk-import/status",
            template: "<bulk-import-status></bulk-import-status>",
            resolve: {
                AdminImportStatusPermission: [
                    "AdminImportPermission",
                    "Permissions",
                    (
                        AdminImportPermission,
                        permissions: Permissions,
                    ): boolean => {
                        if (permissions.canShow("ImportOverview")) {
                            return true;
                        } else {
                            permissions.goToFallback();
                        }
                        return false;
                    },
                ],
            },
        })
        .state("zen.app.pia.module.admin.import", {
            url: "",
            abstract: true,
            template: "<downgrade-bulk-import-templates></downgrade-bulk-import-templates>",
            resolve: {
                AdminImportPermission: [
                    "AdminPermission",
                    (AdminImportPermission: boolean): boolean => {
                        return AdminImportPermission;
                    },
                ],
            },
        })
        .state("zen.app.pia.module.admin.import.templates", {
            url: "bulk-import/templates",
            params: { activeCard: null },
            template: "<bulk-import-templates></bulk-import-templates>",
            resolve: {
                AdminImportTemplatesPermission: [
                    "AdminImportPermission",
                    "Permissions",
                    (
                        AdminImportPermission,
                        permissions: Permissions,
                    ): boolean => {
                        if (permissions.canShow("ImportTemplates")) {
                            return true;
                        } else {
                            permissions.goToFallback();
                        }
                        return false;
                    },
                ],
            },
        })
        .state("zen.app.pia.module.admin.loginhistory", {
            url: "login-history",
            abstract: true,
            template: "<ui-view></ui-view>",
            resolve: {
                AdminLoginHistoryPermission: [
                    "AdminPermission",
                    "Permissions",
                    (
                        AdminPermission,
                        permissions: Permissions,
                    ): boolean => {
                        if (permissions.canShow("UsersLogView")) {
                            return true;
                        } else {
                            permissions.goToFallback();
                        }
                        return false;
                    },
                ],
            },
        })
        .state("zen.app.pia.module.admin.loginhistory.users", {
            url: "",
            params: { time: null, reload: false, leaves: true },
            template: "<downgrade-login-history-component></downgrade-login-history-component>",
            resolve: {
                AdminLoginHistoryUsersPermission: [
                    "AdminLoginHistoryPermission",
                    (AdminLoginHistoryPermission: boolean): boolean => {
                        return AdminLoginHistoryPermission;
                    },
                ],
            },
        });
}

routes.$inject = ["$stateProvider"];
