import { IPermissionHierarchyNode } from "interfaces/role.interface";

export class PermissionNode implements IPermissionHierarchyNode {
    id = this.node.id;
    name = this.node.name;
    exists ?= this.node.exists;
    children ?= this.node.children;
    nameTranslation: string;
    descriptionTranslation: string;

    constructor(private node: IPermissionHierarchyNode, translate: (key: string) => string) {
        this.nameTranslation = translate(`permission.name.${this.node.name}`);
        this.descriptionTranslation = translate(`permission.description.${this.node.name}`);
    }
}

export class PermissionGroupNode implements IPermissionHierarchyNode {
    id = this.node.id;
    name = this.node.name;
    exists = this.node.exists;
    children ?= this.node.children;
    nameTranslation: string;
    constructor(private node: IPermissionHierarchyNode, translate: (key: string) => string) {
        this.nameTranslation = translate(`permission.group.${this.node.name}`);
    }
}
