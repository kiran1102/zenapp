// Modules
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from "@angular/core";
import { SharedModule } from "sharedModules/shared.module";
import { UpgradedLegacyModule } from "sharedModules/upgrade/upgraded-legacy.module";
import { HttpClientModule } from "@angular/common/http";
import { FormsModule } from "@angular/forms";
import { ReactiveFormsModule } from "@angular/forms";
import { VitreusModule } from "@onetrust/vitreus";
import { OTPipesModule, PIPES } from "modules/pipes/pipes.module";
import { OneVideoModule } from "modules/video/video.module";

import { CommonModule } from "@angular/common";
import { TranslateModule } from "@ngx-translate/core";

// Third Party
import { DndListModule } from "ngx-drag-and-drop-lists";
import { VirtualScrollModule } from "od-virtualscroll";

// Services
import { BulkImportService } from "./services/bulk-import/bulk-import.service";
import { UserApiService } from "oneServices/api/user-api.service";
import { RolesService } from "./services/roles.service";
import { OrgActionService } from "oneServices/actions/org-action.service";

// Components
import { BulkImportTableComponent } from "./components/bulk-import/table/bulk-import-table.component";
import { BulkImportModalComponent } from "./components/bulk-import/modal/bulk-import-modal.component";
import { BulkImportTemplatesComponent } from "./components/bulk-import/templates/bulk-import-templates.component";
import { UsersTableComponent } from "./components/users/table/users-table.component";
import { UserGroupsListComponent } from "./components/user-groups/list/user-groups-list.component";
import { UserGroupsManageUsersList } from "./components/user-groups/manage/user-groups-manage-users-list.component";
import { ExternalUserEnableModal } from "./components/external-user-enable-modal/external-user-enable-modal.component";
import { UserGroupsCreateModal } from "./components/user-groups/manage/user-groups-create-modal.component";
import { UserGroupsAddUsersModal } from "./components/user-groups/manage/user-groups-add-users-modal.component";
import { OrgGroupEdit } from "./components/orgs/org-group-edit.component";
import { OrgGroupCreate } from "./components/orgs/org-group-create.component";
import { OrgTreeManageViewComponent } from "./components/orgs/org-tree-manage-view.component";
import { OrgTreeViewComponent } from "./components/orgs/org-tree-view.component";
import { RolesMgmtListComponent } from "adminComponent/roles/list/roles-mgmt-list.component";
import { RolesImportModal } from "adminComponent/roles/roles-import-modal/roles-import-modal.component";
import { RolesReplaceModal } from "adminComponent/roles/roles-replace-modal/roles-replace-modal.component";

// API
import { UserGroupsService } from "./services/user-groups.service";

export function getLegacyUserApiService(injector) {
    return injector.get("UserApi");
}
export function getLegacyRolesService(injector) {
    return injector.get("RolesService");
}
export function getLegacyOrgActionService(injector) {
    return injector.get("OrgAction");
}

@NgModule({
    imports: [
        SharedModule,
        UpgradedLegacyModule,
        CommonModule,
        HttpClientModule,
        FormsModule,
        ReactiveFormsModule,
        VitreusModule,
        DndListModule,
        OTPipesModule,
        VirtualScrollModule,
        OneVideoModule,
        TranslateModule,
    ],
    providers: [
        UserGroupsService,
        BulkImportService,
        {
            provide: UserApiService,
            deps: ["$injector"],
            useFactory: getLegacyUserApiService,
        },
        {
            provide: RolesService,
            deps: ["$injector"],
            useFactory: getLegacyRolesService,
        },
        {
            provide: OrgActionService,
            deps: ["$injector"],
            useFactory: getLegacyOrgActionService,
        },
        PIPES, // TODO: Remove from providers when ot-common is implemented. Only necessary if you are using pipe in TS file, else just module is fine
    ],
    // TODO Figure out which are needed here
    exports: [
        UsersTableComponent,
        ExternalUserEnableModal,
        BulkImportTableComponent,
        BulkImportModalComponent,
        BulkImportTemplatesComponent,
        OrgGroupEdit,
        OrgGroupCreate,
        OrgTreeManageViewComponent,
        OrgTreeViewComponent,
        UserGroupsListComponent,
        UserGroupsCreateModal,
        UserGroupsAddUsersModal,
        UserGroupsManageUsersList,
        RolesMgmtListComponent,
        RolesImportModal,
        RolesReplaceModal,
    ],
    declarations: [
        UsersTableComponent,
        ExternalUserEnableModal,
        BulkImportTableComponent,
        BulkImportModalComponent,
        BulkImportTemplatesComponent,
        OrgGroupEdit,
        OrgGroupCreate,
        OrgTreeManageViewComponent,
        OrgTreeViewComponent,
        UserGroupsListComponent,
        UserGroupsCreateModal,
        UserGroupsAddUsersModal,
        UserGroupsManageUsersList,
        RolesMgmtListComponent,
        RolesImportModal,
        RolesReplaceModal,
    ],
    // TODO Figure out which are needed here
    entryComponents: [
        UsersTableComponent,
        ExternalUserEnableModal,
        BulkImportTableComponent,
        BulkImportModalComponent,
        BulkImportTemplatesComponent,
        OrgGroupEdit,
        OrgGroupCreate,
        OrgTreeManageViewComponent,
        OrgTreeViewComponent,
        UserGroupsListComponent,
        UserGroupsCreateModal,
        UserGroupsAddUsersModal,
        UserGroupsManageUsersList,
        RolesMgmtListComponent,
        RolesImportModal,
        RolesReplaceModal,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class AdminModule { }
