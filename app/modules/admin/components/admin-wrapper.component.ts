// Services
import { Permissions } from "modules/shared/services/helper/permissions.service";
import { GlobalSidebarService } from "modules/global-sidebar/services/one-global-sidebar.service";
import { Wootric } from "sharedModules/services/provider/wootric.service";

// Interfaces
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import { INavMenuItem } from "modules/global-sidebar/interfaces/nav-menu-item.interface";

// Constants
import { WootricModuleMap } from "modules/shared/constants/wootric.constants";

class AdminWrapperController implements ng.IComponentController {

    static $inject: string[] = [
        "$rootScope",
        "Permissions",
        "GlobalSidebarService",
        "Wootric",
    ];

    constructor(
        private $rootScope: IExtendedRootScopeService,
        private permissions: Permissions,
        private globalSidebarService: GlobalSidebarService,
        private wootric: Wootric,
    ) {}

    $onInit() {
        this.globalSidebarService.set(this.getMenuRoutes(), this.$rootScope.t("UsersAndGroups"), null, false);
        this.wootric.run(WootricModuleMap.UsersAndGroups);
    }

    private getMenuRoutes(): INavMenuItem[] {
        const menuGroup: INavMenuItem[] = [];

        if (this.permissions.canShow("Users")) {
            menuGroup.push({
                id: "AdminUsersSidebarItems",
                title: this.$rootScope.t("Users"),
                route: "zen.app.pia.module.admin.users.list",
                activeRoute: "zen.app.pia.module.admin.users",
            });
        }
        if (this.permissions.canShow("UserGroups")) {
            menuGroup.push({
                id: "AdminUserGroupsSidebarItems",
                title: this.$rootScope.t("UserGroups"),
                route: "zen.app.pia.module.admin.user-groups.list",
                activeRoute: "zen.app.pia.module.admin.user-groups",
                params: { page: undefined, size: undefined },
            });
        }
        if (this.permissions.canShow("Roles") && this.permissions.canShow("RolesList")) {
            menuGroup.push({
                id: "AdminRolesSidebarItems",
                title: this.$rootScope.t("Roles"),
                route: "zen.app.pia.module.admin.roles.list",
                params: { page: undefined, size: undefined },
                activeRoute: "zen.app.pia.module.admin.roles",
            });
        }
        if (this.permissions.canShow("Organizations")) {
            menuGroup.push({
                id: "AdminOrganizationsSidebarItems",
                title: this.$rootScope.t("Organizations"),
                route: "zen.app.pia.module.admin.organizations.list",
                activeRoute: "zen.app.pia.module.admin.organizations",
            });
        }
        if (this.permissions.canShow("ImportOverview")) {
            menuGroup.push({
                id: "AdminBulkImportSidebarItems",
                title: this.$rootScope.t("BulkImport"),
                route: "zen.app.pia.module.admin.bulkimport.status",
                activeRoute: "zen.app.pia.module.admin.bulkimport",
            });
        }
        if (this.permissions.canShow("ImportTemplates")) {
            menuGroup.push({
                id: "AdminImportTemplatesSidebarItems",
                title: this.$rootScope.t("ImportTemplates"),
                route: "zen.app.pia.module.admin.import.templates",
                activeRoute: "zen.app.pia.module.admin.import",
            });
        }
        if (this.permissions.canShow("UsersLogView")) {
            menuGroup.push({
                id: "AdminLoginHistorySidebarItems",
                title: this.$rootScope.t("LoginHistory"),
                route: "zen.app.pia.module.admin.loginhistory.users",
                activeRoute: "zen.app.pia.module.admin.loginhistory",
            });
        }

        return menuGroup;
    }

}

export default {
    controller: AdminWrapperController,
    template: `
        <section class="row-nowrap full-size text-color-default background-grey overflow-y-auto">
            <div class="stretch-horizontal" ui-view></div>
        </section>
    `,
};
