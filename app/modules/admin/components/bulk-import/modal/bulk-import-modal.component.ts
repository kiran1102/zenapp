import {
    Component,
} from "@angular/core";

import { Subject } from "rxjs";

import { IOtModalContent, OtModalService} from "@onetrust/vitreus";

import { BulkImportService } from "adminService/bulk-import/bulk-import.service";

import { StatusCodes } from "enums/status-codes.enum";
import { BulkImportFileTypes } from "constants/file.constant.ts";

import { TranslatePipe } from "pipes/translate.pipe";
import { NotificationService } from "sharedModules/services/provider/notification.service";

interface IImportFile extends File {
    data: string;
}

interface IImportJob {
    name: {
        value: string,
        touched: boolean,
    };
    description: string;
    type: {
        id: string,
        description: string,
        name: string,
        isCsvSupported: boolean;
    };
    files: IImportFile[];
}

@Component({
    selector: "bulk-import-modal",
    templateUrl: "./bulk-import-modal.component.html",
})

export class BulkImportModalComponent implements IOtModalContent {
    otModalCloseEvent: Subject<any>;
    maxSize = 64;
    isCsvSupported = true;
    isHovering: boolean;
    isSaving: boolean;
    filesFromInput: File[] = [];
    invalidFilesFromDrag: File[] = [];
    importTypeOptions = [];
    job: IImportJob;
    isSubmitting = false;

    private supportedFileTypes = BulkImportFileTypes;

    constructor(
        private ModalService: OtModalService,
        private bulkImportService: BulkImportService,
        private notificationService: NotificationService,
        private translate: TranslatePipe,
    ) {}

    async ngOnInit() {
        // Initialize Import
        this.initImportJob();
        // Fetch Import Types
        const response = await this.bulkImportService.getImportTypes();
        this.importTypeOptions = response.data.filter((item) => item.id !== "create_ds_consents").map((type) => {
            type.name = this.translate.transform(type.id);
            return type;
        });
    }

    nameChange() {
        this.job.name.touched = true;
    }

    handleFilesSelected(files: IImportFile[]): void {
        this.job.files = files;
    }

    handleInvalidFiles(files: IImportFile[]): void {
        this.invalidFilesFromDrag = files;
    }

    handleFilesHover(isHovering) {
        this.isHovering = isHovering;
    }

    onImportTypeSelection(selection) {
        this.job.type = selection;
        this.job.type.isCsvSupported = this.job.type.id !== "update_ds_consents";
    }

    closeModal(success) {
        this.initImportJob();
        this.otModalCloseEvent.next(success);
    }

    public removeFile() {
        this.job.files = [];
    }

    public async handleSubmit() {
        this.isSaving = true;
        const request = {
            Job: {
                Name: this.job.name.value,
                Description: this.job.description,
                Type: this.job.type.id,
            },
            FileData: this.job.files[0].data,
            Filename: this.job.files[0].name,
        };

        try {
            const response = await this.bulkImportService.createNewBulkImport(request);
            if (response.status === StatusCodes.BadRequest) {
                const errorMsg = `${this.translate.transform("Invalid.Headers")}: ${response.data}`;
                this.notificationService.alertError(this.translate.transform("Error"), errorMsg);
            } else if (response.result) {
                sessionStorage.setItem("OneTrust.RefreshUserList", "true");
                this.closeModal(true);
            }
            this.isSaving = false;
        } catch (e) {
            this.isSaving = false;
            this.notificationService.alertError(this.translate.transform("Error"), this.translate.transform("ErrorCreatingNewBulkImport"));
        }
    }

    public handleFiles(files) {
        files.map((file) => {
            if (file.size < this.maxSize * 1024 * 1024) {
                const reader = new FileReader();
                reader.readAsDataURL(file);
                reader.onload = () => {
                    file.extension = file.name.split(".").pop() || "";
                    file.data = reader.result;
                };
                return file;
            } else {
                this.notificationService.alertError(
                    this.translate.transform("InvalidFile"),
                    this.translate.transform("FileSizeLimitViolation"),
                );
                return;
            }
        });

        this.job.files = files;
    }

    private initImportJob() {
        this.job = {
            name: {
                value: "",
                touched: false,
            },
            description: "",
            type: {
                id: "",
                description: "",
                name: "",
                isCsvSupported: false,
            },
            files: [],
        };
    }
}
