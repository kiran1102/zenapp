import {
    Component,
    Inject,
    OnInit,
} from "@angular/core";

// Components
import { BulkImportModalComponent } from "../modal/bulk-import-modal.component";

// Services
import { OtModalService } from "@onetrust/vitreus";
import { BulkImportService } from "adminService/bulk-import/bulk-import.service";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

// Enums
import { ExportTypes } from "enums/export-types.enum";

@Component({
    selector: "bulk-import",
    templateUrl: "./bulk-import-table.component.html",
})

export class BulkImportTableComponent implements OnInit {
    ready = false;
    isLoaded = true;
    isDownloading = false;

    table = {
        columns: [],
        data: [],
    };

    pagination = {
        totalPages: 0,
        currentPage: 0,
    };

    sort = {
        key: "createdDate",
        order: "desc",
    };

    constructor(
        @Inject("Export") readonly Export: any,
        private translate: TranslatePipe,
        readonly bulkImportService: BulkImportService,
        readonly ModalService: OtModalService,

    ) {}

    async ngOnInit() {
        try {
            const response = await this.bulkImportService.initImportStatus();
            if (response.result) {
                this.table = {
                    columns: response.data.tableConfig.cells,
                    data: response.data.tableData.content,
                };
                this.pagination = {
                    totalPages: response.data.tableData.totalPages,
                    currentPage: response.data.tableData.number,
                };
                this.ready = true;
                this.isLoaded = true;
            }
        } catch (e) {
            this.ready = true;
            this.isLoaded = true;
        }
    }

    public async fetchPage(page) {
        this.isLoaded = false;
        try {
            const data = await this.bulkImportService.refreshImportStatus(page, this.sort);
            this.table.data = data.content;
            this.pagination.totalPages = data.totalPages;
            this.pagination.currentPage = page || 0;
            this.isLoaded = true;
        } catch (e) {
            this.table.data = [];
            this.pagination.currentPage = 0;
            this.isLoaded = true;
        }
    }

    public handleSortChange({ sortKey }) {
        this.sort.key = sortKey || this.sort.key;
        this.sort.order = (this.sort.order === "asc") ? "desc" : "asc";
        this.fetchPage(this.pagination.currentPage);
    }

    public newImport(): void {
        this.ModalService.create(
            BulkImportModalComponent,
            {
                isComponent: true,
            },
        ).subscribe(() => {
            this.fetchPage(this.pagination.currentPage);
        });
    }

    public downloadUploadStatus(row): void {
        const extension = row.FileName.split(".").slice(-1)[0].toUpperCase();
        const type = ExportTypes[extension] || null;

        if (type) {
            row.isDownloading = true;
            this.Export.xlsxImportStatusExport("IMPORT_STATUS_", row.Id, type).then((): void => {
                row.isDownloading = false;
            });
        }
    }
}
