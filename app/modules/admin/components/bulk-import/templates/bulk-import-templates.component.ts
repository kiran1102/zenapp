// 3rd Party
import {
    Component,
    OnInit,
} from "@angular/core";
import { StateService } from "@uirouter/core";

// Vitreus
import { OtModalService } from "@onetrust/vitreus";

// Services
import { BulkImportService } from "adminService/bulk-import/bulk-import.service";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

// Components
import { BulkImportModalComponent } from "adminComponent/bulk-import/modal/bulk-import-modal.component";

// Enums & Constants
import { BulkImportCards } from "modules/admin/enums/bulk-import.enums";

@Component({
    selector: "bulk-import-templates",
    templateUrl: "./bulk-import-templates.component.html",
})

export class BulkImportTemplatesComponent implements OnInit {
    ready = false;
    importTypes = [];

    constructor(
        readonly bulkImportService: BulkImportService,
        readonly ModalService: OtModalService,
        readonly stateService: StateService,
        private translate: TranslatePipe,
    ) {}

    async ngOnInit() {
        const activeCard: BulkImportCards = this.stateService.params ? this.stateService.params.activeCard : null;
        try {
            const response = await this.bulkImportService.getImportTypes();
            this.importTypes = response.data;
            this.importTypes.map((item) => {
                item.isActive = item.id === activeCard;
                item.downloading = false;
                return item;
            });
        } catch (e) {
            this.importTypes = [];
        }

        this.ready = true;
    }

    public newImport(): void {
        this.ModalService.create(
            BulkImportModalComponent,
            {
                isComponent: true,
            },
        ).subscribe((e) => {
            if (e) this.stateService.go("zen.app.pia.module.admin.bulkimport.status");
        });
    }

    public async downloadTemplate(index) {
        const item = this.importTypes[index];
        item.downloading = true;
        try {
            await this.bulkImportService.downloadTemplate(item.id, item.name);
            item.downloading = false;
        } catch (e) {
            item.downloading = false;
        }
    }
}
