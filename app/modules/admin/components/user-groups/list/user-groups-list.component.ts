import {
    Component,
    OnInit,
    Inject,
} from "@angular/core";
import { StateService } from "@uirouter/core";
import { isEmpty } from "lodash";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

// Interface
import {
    IUserGroupDisplay,
    IUserGroupsFilterOption,
    IUserGroupsModal,
    IUserGroup,
 } from "interfaces/user-groups.interface";
import { IDropdownOption } from "interfaces/one-dropdown.interface";
import { IStore } from "interfaces/redux.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";
import { IPageableOf } from "interfaces/pagination.interface";

// Services
import { UserGroupsService } from "modules/admin/services/user-groups.service";
import { Permissions } from "modules/shared/services/helper/permissions.service";
import { ModalService } from "sharedServices/modal.service";

// Redux  //Tokens
import { StoreToken } from "tokens/redux-store.token";

// Enums
import { CellTypes } from "enums/general.enum";

// Constants
import { UserGroupActions } from "constants/user-group-constants";

// 3rd Party
import { Subject } from "rxjs";

@Component({
    selector: "user-groups-list",
    templateUrl: "user-groups-list.component.html",
})
export class UserGroupsListComponent implements OnInit {
    isReady: boolean;
    numPages: number;
    checked = false;
    pageSize = 20;
    noRecordFound: boolean;
    searchGroupName: string;
    userGroupsMeta: IPageableOf<IUserGroupDisplay>;
    cellTypes = CellTypes;
    defaultParams = { page: 0, size: 20 };
    params = {
        page: parseInt(this.stateService.params.page, 10) || this.defaultParams.page,
        size: parseInt(this.stateService.params.size, 10) || this.defaultParams.size,
        sort: this.stateService.params.sort,
    };

    resetSearch = new Subject();

    userGroupColumns = [
        {
            columnName: this.translatePipe.transform("GroupName"),
            cellValueKey: "name",
            cellClass: "text-left",
            type: this.cellTypes.text,
        },
        {
            columnName: this.translatePipe.transform("Description"),
            cellValueKey: "description",
            cellClass: "text-center",
            type: this.cellTypes.text,
        },
        {
            columnName: this.translatePipe.transform("CreatedBy"),
            cellValueKey: "createdByDisplay",
            cellClass: "text-center",
            type: this.cellTypes.text,
        },
        {
            columnName: this.translatePipe.transform("CreatedDate"),
            cellValueKey: "createdDate",
            cellClass: "text-center",
            type: this.cellTypes.date,
        },
        {
            columnName: this.translatePipe.transform("LastModifiedBy"),
            cellValueKey: "lastModifiedByDisplay",
            cellClass: "text-center",
            type: this.cellTypes.text,
        },
        {
            columnName: this.translatePipe.transform("LastModified"),
            cellValueKey: "lastModifiedDate",
            cellClass: "text-center",
            type: this.cellTypes.date,
        },
    ];

    userGroupPermissions = {
        list: this.permissions.canShow("UserGroups"),
        add: this.permissions.canShow("UserGroupsAdd"),
        edit: this.permissions.canShow("UserGroupsEdit"),
        delete: this.permissions.canShow("UserGroupsDelete"),
        search: this.permissions.canShow("UserGroupsSearch"),
        bulkEdit: this.permissions.canShow("UserGroupsBulkEdit"),
    };

    constructor(
        @Inject(StoreToken) private store: IStore,
        private stateService: StateService,
        @Inject(ModalService) private modalService: ModalService,
        private translatePipe: TranslatePipe,
        private permissions: Permissions,
        private userGroups: UserGroupsService,
    ) {}

    ngOnInit() {
        this.getUserGroupsPage(this.params.page, this.params.size);
    }

    groupNameClicked(id: string) {
        this.stateService.go("zen.app.pia.module.admin.user-groups.manage", {id});
    }

    trackByFn(index: number, userGroup: IUserGroupDisplay) {
        return userGroup.id;
    }

    pageChangedEvent(page: number = this.params.page) {
        this.params.page = page;
        if (this.searchGroupName && this.searchGroupName !== "") {
            this.getUserGroupsPage(
                this.params.page,
                this.params.size,
                null,
                this.searchGroupName,
                UserGroupActions.SearchUserGroup,
            );
        } else {
            this.getUserGroupsPage(this.params.page, this.params.size);
        }
    }

    searchUserGroup(userGroupName: string) {
        this.searchGroupName = userGroupName;
        this.params.page = 0;
        this.getUserGroupsPage(
            this.params.page,
            this.params.size,
            null,
            this.searchGroupName,
            UserGroupActions.SearchUserGroup,
        );
    }

    openUserGroupsCreateModal() {
        const modalData: IUserGroupsModal = {
            modalTitle: this.translatePipe.transform("UserGroups.NewUserGroup"),
            userGroupActions: UserGroupActions.CreateUserGroup,
            callback: (res: IProtocolResponse<void>): boolean => {
                if (res.result) {
                    const id = res.data["id"];
                    this.stateService.go("zen.app.pia.module.admin.user-groups.manage", {id});
                }
                return res.result;
            },
        };
        this.modalService.setModalData(modalData);
        this.modalService.openModal("UserGroupsCreateModal");
    }

    getActions(groupObj: IUserGroup): () => IDropdownOption[] {
        return (): IDropdownOption[] => {
            const actions: IDropdownOption[] = [];
            if (this.userGroupPermissions.edit) {
                actions.push(this.getEditAction(groupObj));
            }
            if (this.userGroupPermissions.delete) {
                actions.push(this.getDeleteAction(groupObj));
            }
            if (
                !this.userGroupPermissions.edit &&
                !this.userGroupPermissions.delete
            ) {
                actions.push(this.getNoOptionsAction());
            }
            return actions;
        };
    }

    clearModel() {
        this.resetSearch.next();
    }

    getUserGroupsPage(
        page: number = 0,
        size: number = 20,
        filters?,
        userGroupName?: string,
        userGroupAction?: string,
    ) {
        this.isReady = false;
        const filter: IUserGroupsFilterOption[] = [];
        if (filters) {
            if (filters.searchFilter) {
                filter.push({
                    filterType: "search",
                    filterValue: filters.searchFilter,
                });
            }
        }
        return this.userGroups.listUserGroups(
            {
            filters,
            page,
            size,
            },
            userGroupName,
            userGroupAction,
        ).then((response: IProtocolResponse<IPageableOf<IUserGroupDisplay>>) => {
            if (response.result && response.data) {
                this.userGroupsMeta = response.data;
                this.noRecordFound = isEmpty(response.data.content);
            }
            this.isReady = true;
        });
    }
    private getNoOptionsAction(): IDropdownOption {
        return {
            text: this.translatePipe.transform("NoOptionsAvailable"),
            action: (): void => {},
        };
    }

    private getDeleteAction(groupObj: IUserGroup): IDropdownOption {
        return {
            text: this.translatePipe.transform("Delete"),
            action: (): void => {
                // TODO
                // this.deleteTask(task);
            },
        };
    }
    private getEditAction(groupObj: IUserGroup): IDropdownOption {
        return {
            text: this.translatePipe.transform("Edit"),
            action: (): void => {
                const modalData: IUserGroupsModal = {
                    modalTitle: this.translatePipe.transform("UserGroups.EditUserGroup"),
                    userGroupActions: UserGroupActions.EditUserGroup,
                    userGroupData: groupObj,
                    callback: (res: IProtocolResponse<void>): boolean => {
                        if (res.result) {
                            this.clearModel();
                            this.getUserGroupsPage(this.params.page);
                        }
                        return res.result;
                    },
                };
                this.modalService.setModalData(modalData);
                this.modalService.openModal("UserGroupsCreateModal");
            },
        };
    }
}
