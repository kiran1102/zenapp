// Angular
import {
    Component,
    Inject,
    OnInit,
} from "@angular/core";
import {
    FormGroup,
    FormBuilder,
    Validators,
    FormControl,
} from "@angular/forms";

// Redux / Tokens
import { StoreToken } from "tokens/redux-store.token";
import { getModalData } from "oneRedux/reducers/modal.reducer";

// Services
import { ModalService } from "sharedServices/modal.service";
import { UserGroupsService } from "modules/admin/services/user-groups.service";
import { Permissions } from "modules/shared/services/helper/permissions.service";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

// Interfaces
import {
    IUserGroup,
    IUserGroupDisplay,
    IUserGroupsModal,
} from "interfaces/user-groups.interface";
import { IStore } from "interfaces/redux.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";

// Constants
import { UserGroupActions } from "constants/user-group-constants";
import { NO_BLANK_SPACE } from "constants/regex.constant";

@Component({
    selector: "user-groups-create-modal",
    templateUrl: "user-groups-create-modal.component.html",
})
export class UserGroupsCreateModal implements OnInit {
    disabled = false;
    modalData: IUserGroupsModal;
    userGroupManageForm: FormGroup;
    ready = false;
    errorMessage: string;

    userGroupPermissions = {
        add: this.permissions.canShow("UserGroupsAdd"),
        edit: this.permissions.canShow("UserGroupsEdit"),
        delete: this.permissions.canShow("UserGroupsDelete"),
    };

    constructor(
        @Inject(StoreToken) private store: IStore,
        private formBuilder: FormBuilder,
        private modalService: ModalService,
        private translatePipe: TranslatePipe,
        private userGroups: UserGroupsService,
        private permissions: Permissions,
    ) {}

    ngOnInit() {
        this.modalData = getModalData(this.store.getState());
        this.initForm();
        this.ready = true;
    }

    closeModal() {
        this.modalService.closeModal();
        this.modalService.clearModalData();
    }

    createOrEditUserGroup() {
        this.disabled = true;
        this.ready = false;
        const userGroup: IUserGroup = {
            name: this.userGroupManageForm.get("name").value,
            description: this.userGroupManageForm.get("description").value,
        };
        if (this.modalData.userGroupActions === UserGroupActions.CreateUserGroup) {
            this.userGroups.createOrEditUserGroup(userGroup).then(
                (res: IProtocolResponse<IUserGroupDisplay>): void => {
                    if (res.result) {
                        this.modalService.handleModalCallback({
                            result: true,
                            data: res.data,
                        });
                    }
                    this.closeModal();
                    this.disabled = false;
                    this.ready = true;
                },
            );
        } else if (this.modalData.userGroupActions === UserGroupActions.EditUserGroup) {
            this.userGroups.createOrEditUserGroup(
                userGroup,
                this.modalData.userGroupData.id,
                this.modalData.userGroupActions,
            ).then((res: IProtocolResponse<IUserGroupDisplay>): void => {
                    if (res.result) {
                        this.modalService.handleModalCallback({
                            result: true,
                            data: res.data,
                        });
                    }
                    this.closeModal();
                    this.disabled = false;
                    this.ready = true;
                });
        }
    }

    getError(field: string): boolean {
        if (field === "name") {
            const name = this.userGroupManageForm.get(field);
            if (!name || !name.errors) {
                return false;
            } else if (name.dirty && name.touched && (name.errors.required || name.errors.pattern)) {
                this.errorMessage = this.translatePipe.transform(
                    "UserGroups.PleaseEnterANameForThisUserGroup",
                );
                return true;
            } else if (name.dirty && name.touched && name.errors.maxlength) {
                this.errorMessage = this.translatePipe.transform(
                    "UserGroups.GroupNameExceedsMaximumLength",
                );
                return true;
            }
        } else if (field === "description") {
            const description = this.userGroupManageForm.get(field);
            if (!description || !description.errors) {
                return false;
            } else if (description && description.errors.maxlength) {
                this.errorMessage = this.translatePipe.transform(
                    "UserGroups.GroupDescriptionExceedsMaximumLengthAllowed",
                );
                return true;
            }
        }
        return false;
    }

    private initForm() {
        this.userGroupManageForm = this.formBuilder.group({
            name: new FormControl("", [
                Validators.required,
                Validators.maxLength(100),
                Validators.pattern(NO_BLANK_SPACE),
            ]),
            description: new FormControl("", Validators.maxLength(300)),
        });

        if (UserGroupActions.EditUserGroup === this.modalData.userGroupActions) {
            this.userGroupManageForm.patchValue({
                name: this.modalData.userGroupData.name,
                description: this.modalData.userGroupData.description,
            });
        }
    }
}
