// Rxjs
import {
    Observable,
    Subscription,
    Subject,
    of,
} from "rxjs";
import { StateService } from "@uirouter/core";

import { filter } from "rxjs/operators";

// Angular
import {
    Component,
    Inject,
    OnInit,
    OnDestroy,
} from "@angular/core";

// 3rd Party
import {
    IVirtualScrollOptions,
    ScrollObservableService,
    SetScrollTopCmd,
} from "od-virtualscroll";

// Redux / Tokens
import { StoreToken } from "tokens/redux-store.token";
import { getModalData } from "oneRedux/reducers/modal.reducer";

// Services
import { ModalService } from "sharedServices/modal.service";
import { UserGroupsService } from "modules/admin/services/user-groups.service";
import { Permissions } from "modules/shared/services/helper/permissions.service";

// Interfaces
import {
    IUserGroupDisplay,
    IUserGroupsModal,
} from "interfaces/user-groups.interface";
import { IStringMap } from "interfaces/generic.interface";
import {
    IUserGroupUser,
    IModifiedUser,
} from "interfaces/user-groups-users.interface";
import { IStore } from "interfaces/redux.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";
import {
    IPageableOf,
    IPaginationParams,
} from "interfaces/pagination.interface";

@Component({
    selector: "user-groups-add-users-modal",
    templateUrl: "user-groups-add-users-modal.component.html",
    providers: [ScrollObservableService],
})

export class UserGroupsAddUsersModal implements OnInit, OnDestroy {
    destroy$ = new Subject();
    disabled = false;
    isReady: boolean;
    checked = false;
    previousSearch: string;
    search: string;
    modalData: IUserGroupsModal;
    userGroupsMeta: IPageableOf<IUserGroupUser>;
    noRecordFound: boolean;
    errorMessage: string;
    modifiedUsersList: IModifiedUser[];
    users: IUserGroupUser[] = [];
    selectedUsers: IModifiedUser[] = [];
    selectedUsersMap: IStringMap<boolean> = {};
    isVirualScrollActive = true;

    userGroupPermissions = {
        add: this.permissions.canShow("UserGroupsAdd"),
    };

    defaultParams = { page: 0, size: 20 };
    params = {
        page: parseInt(this.stateService.params.page, 10) || this.defaultParams.page,
        size: parseInt(this.stateService.params.size, 10) || this.defaultParams.size,
        sort: this.stateService.params.sort,
    };

    scrollTop$: Subject<SetScrollTopCmd> = new Subject();
    options$: Observable<IVirtualScrollOptions> = of({
        itemWidth: 400,
        itemHeight: 50,
        numAdditionalRows: 1,
        numLimitColumns: 1,
    });

    public data$: Observable<IModifiedUser[]>;
    public virtualData$: Subject<IModifiedUser[]>;
    private scrollEnd$ = this.scrollObs.scrollWin$.pipe(filter(
        ([scrollWin]) =>
            scrollWin.visibleEndRow !== -1 &&
            scrollWin.visibleEndRow === scrollWin.numVirtualRows - 1,
    ));
    private curData: IModifiedUser[] = [];
    private prevData: IModifiedUser[] = this.curData;
    private subs: Subscription[] = [];

    constructor(
        @Inject(StoreToken) private store: IStore,
        private stateService: StateService,
        private modalService: ModalService,
        private userGroups: UserGroupsService,
        private permissions: Permissions,
        private scrollObs: ScrollObservableService,
    ) {
        this.virtualData$ = new Subject();
        this.data$ = this.virtualData$.asObservable();
    }

    ngOnInit() {
        this.modalData = getModalData(this.store.getState());
        this.subs.push(
            this.scrollEnd$.subscribe(([scrollWin]) => {
                const { number, totalPages } = this.userGroupsMeta;
                let nextPage = 0;
                if (number + 1 < totalPages) {
                    nextPage = number + 1;
                }
                if (nextPage !== this.params.page) {
                    this.getUserGroupMembersPage(nextPage);
                }
            }),
        );
        this.getUserGroupMembersPage(this.params.page, this.params.size).then(
            () => {
                this.isReady = true;
            },
        );
    }

    ngOnDestroy(): void {
        this.subs.forEach((sub) => sub.unsubscribe());
        this.destroy$.next(true);
        this.destroy$.complete();
    }

    trackByFn(index: number, userEntry: IModifiedUser) {
        return userEntry.user.memberId;
    }

    reset() {
        this.getUserGroupMembersPage(this.params.page, this.params.size);
    }

    closeModal() {
        this.modalService.closeModal();
        this.modalService.clearModalData();
    }

    onSearch(search = "") {
        this.isVirualScrollActive = false;
        this.isReady = false;
        this.search = search.trim();
        this.getUserGroupMembersPage().then(() => {
            this.isReady = true;
        });
    }

    addUsers() {
        this.disabled = true;
        this.isReady = false;
        const members: string[] = [];
        for (const id in this.selectedUsersMap) {
            if (this.selectedUsersMap[id]) {
                members.push(id);
            }
        }
        this.userGroups.addUsers({ members }, this.modalData.groupId).then(
            (res: IProtocolResponse<IUserGroupDisplay>): void => {
                if (res.result) {
                    this.modalService.handleModalCallback({
                        result: true,
                        data: res.data,
                    });
                }
                this.closeModal();
                this.disabled = false;
                this.isReady = true;
            },
        );
    }

    onToggle(selected: boolean, userEntry: IModifiedUser): void {
        if (selected) {
            this.modifiedUsersList.find(
                (uncheckUser: IModifiedUser) =>
                    uncheckUser.user["memberId"] === userEntry.user["memberId"],
            ).checked = true;
            this.selectedUsers.push(userEntry);
            this.selectedUsersMap[userEntry.user.memberId] = true;
        } else {
            this.removeItem(userEntry["user"]);
        }
    }

    removeItem(user: IUserGroupUser) {
        const removeIndex = this.selectedUsers.findIndex(
            (removeUser: IModifiedUser) =>
                removeUser.user["memberId"] === user.memberId,
        );
        if (removeIndex > -1) {
            this.selectedUsers.splice(removeIndex, 1);
            const removeIndexModifiedList = this.modifiedUsersList.findIndex(
                (removeUser: IModifiedUser) =>
                    removeUser.user["memberId"] === user.memberId,
            );
            if (removeIndexModifiedList > -1) {
                this.modifiedUsersList[removeIndexModifiedList].checked = false;
            }
        }
        this.selectedUsersMap[user.memberId] = false;
    }

    isChecked(user: IUserGroupUser) {
        return this.selectedUsersMap[user.memberId];
    }

    equals = (prevDataIndex: number, curDataIndex: number): boolean => {
        if (prevDataIndex !== curDataIndex) {
            return false;
        } else if (
            this.prevData !== undefined &&
            this.curData !== undefined &&
            this.prevData.length > prevDataIndex &&
            this.curData.length > curDataIndex
        ) {
            const prev = this.prevData[prevDataIndex];
            const cur = this.curData[curDataIndex];
            if (
                cur.user.memberId === undefined ||
                prev.user.memberId === undefined
            ) {
                return false;
            } else {
                return prev.user.memberId === cur.user.memberId;
            }
        } else {
            return false;
        }
    }

    private getUserGroupMembersPage(
        page: number = 0,
        size: number = 20,
        search = this.search,
    ) {
        const params: IPaginationParams<string> = { page, size };
        if (search) {
            params.filters = this.userGroups.serializeSearch(search);
        }
        return this.userGroups.listUsersToShow(
            this.modalData.groupId,
            params,
        ).then((response: IProtocolResponse<IPageableOf<IUserGroupUser>>) => {
            if (response === null || !response.result) {
                this.noRecordFound = true;
            } else {
                this.userGroupsMeta = response.data;
                this.noRecordFound = !this.userGroupsMeta.totalElements;
                if (
                    !this.modifiedUsersList ||
                    search !== this.previousSearch ||
                    !this.isVirualScrollActive
                ) { this.modifiedUsersList = this.modifyArray(this.userGroupsMeta.content);
                } else {
                    this.modifiedUsersList.push(...this.modifyArray(this.userGroupsMeta.content));
                }
            }
            const shouldResetList = search !== this.previousSearch;
            setTimeout(() => {
                this.updateVirtualData(this.modifiedUsersList, shouldResetList);
            }, 0);
            this.previousSearch = this.search;
            this.isVirualScrollActive = true;
        });
    }

    private modifyArray(userList: IUserGroupUser[]) {
        return userList.map((user) => {
            return { user, checked: this.selectedUsersMap[user.memberId] };
        });
    }

    private updateVirtualData(users: IModifiedUser[], reset: boolean) {
        if (reset) {
            this.curData = users;
        } else {
            if (!this.curData.length) {
                this.curData = users;
            } else {
                this.curData = this.prevData;
            }
        }
        this.prevData = this.curData;
        this.virtualData$.next(this.curData);
    }
}
