import { IPageableOf } from "interfaces/pagination.interface";
import {
    Component,
    OnInit,
    Inject,
} from "@angular/core";
import { StateService } from "@uirouter/core";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

// Interface
import {
    IUserGroupDisplay,
    IUserGroupsModal,
    IUserGroupsFilterOption,
} from "interfaces/user-groups.interface";
import { IUserGroupUserDisplay } from "interfaces/user-groups-users.interface";
import { IDropdownOption } from "interfaces/one-dropdown.interface";
import { IStore } from "interfaces/redux.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";
import { IDeleteConfirmationModalResolve } from "interfaces/delete-confirmation-modal-resolve.interface";

// Services
import { UserGroupsService } from "modules/admin/services/user-groups.service";
import { Permissions } from "modules/shared/services/helper/permissions.service";
import { ModalService } from "sharedServices/modal.service";

// Redux  //Tokens
import { StoreToken } from "tokens/redux-store.token";

@Component({
    selector: "user-groups-manage-users-list",
    templateUrl: "user-groups-manage-users-list.component.html",
})
export class UserGroupsManageUsersList implements OnInit {
    isReady: boolean;
    numPages: number;
    checked = false;
    pageSize = 20;
    noRecordFound: boolean;
    description: string;
    userGroupName: string;
    userGroupDetails: IPageableOf<IUserGroupDisplay>;
    usersMeta: IPageableOf<IUserGroupUserDisplay>;

    defaultParams = { page: 0, size: 20 };
    params = {
        page: parseInt(this.stateService.params.page, 10) || this.defaultParams.page,
        size: parseInt(this.stateService.params.size, 10) || this.defaultParams.size,
        sort: this.stateService.params.sort,
    };

    userGroupUsersColumns = [
        {
            columnName: this.translatePipe.transform("Name"),
            cellValueKey: "fullName",
            cellClass: "text-left",
        },
        {
            columnName: this.translatePipe.transform("Email"),
            cellValueKey: "email",
            cellClass: "text-left",
        },
        {
            columnName: this.translatePipe.transform("AddedBy"),
            cellValueKey: "createdByDisplay",
            cellClass: "text-left",
        },
        {
            columnName: this.translatePipe.transform("DateAdded"),
            cellValueKey: "createdDate",
            cellClass: "text-center",
        },
    ];

    userGroupPermissions = {
        add: this.permissions.canShow("UserGroupsAdd"),
        edit: this.permissions.canShow("UserGroupsEdit"),
        delete: this.permissions.canShow("UserGroupsDelete"),
        search: this.permissions.canShow("UserGroupsSearch"),
        bulkEdit: this.permissions.canShow("UserGroupsBulkEdit"),
    };
    private userGroupId: string = this.stateService.params.id;

    constructor(
        @Inject(StoreToken) private store: IStore,
        private stateService: StateService,
        @Inject(ModalService) private modalService: ModalService,
        private translatePipe: TranslatePipe,
        private UserGroups: UserGroupsService,
        private permissions: Permissions,
    ) {}

    ngOnInit() {
        this.pageChangedEvent();
    }

    goToRoute(route: { path: string, params: any }) {
        this.stateService.go(route.path);
    }

    trackByFn(index: number, userGroup: IUserGroupDisplay) {
        return userGroup.id;
    }

    pageChangedEvent(page: number = this.params.page, size: number = this.params.size) {
        this.isReady = false;
        this.params.page = page;
        this.params.size = size;
        this.getUserGroupDetail().then(() => {
            return this.getUserGroupMembers(page, size);
        }).then(() => {
            this.isReady = true;
        });
    }

    openAddUserModal() {
        const modalData: IUserGroupsModal = {
            modalTitle: this.translatePipe.transform("UserGroups.AddUsers"),
            groupId: this.userGroupId,
            callback: (res: IProtocolResponse<void>): boolean => {
                if (res.result) {
                    this.getUserGroupMembers(this.params.page, this.params.size);
                }
                return res.result;
            },
        };
        this.modalService.setModalData(modalData);
        this.modalService.openModal("userGroupsAddUsersModal");
    }

    getActions(user: IUserGroupUserDisplay): () => IDropdownOption[] {
        return (): IDropdownOption[] => {
            const actions: IDropdownOption[] = [];
            actions.push(this.getDeleteAction(user));
            return actions;
        };
    }

    getUserGroupDetail() {
        return this.UserGroups.getUserGroupDetails(this.userGroupId).then((response: IProtocolResponse<IPageableOf<IUserGroupDisplay>>) => {
            if (response === null) {
                this.noRecordFound = true;
            } else {
                this.userGroupDetails = response.data;
                this.description = this.userGroupDetails["description"];
                this.userGroupName = this.userGroupDetails["name"];
            }
        });
    }

    getUserGroupMembers(page: number = 0, size: number = 20, filters?) {
        this.isReady = false;
        const filter: IUserGroupsFilterOption[] = [];
        if (filters) {
            if (filters.searchFilter) {
                filter.push({
                    filterType: "search",
                    filterValue: filters.searchFilter,
                });
            }
        }
        return this.UserGroups.listUserGroupMembers({
            filters,
            page,
            size,
        }, this.userGroupId).then((response: IProtocolResponse<IPageableOf<IUserGroupUserDisplay>> ) => {
            if (response === null) {
                this.noRecordFound = true;
            } else {
                this.usersMeta = response.data;
                this.noRecordFound = this.usersMeta.content.length === 0;
            }
            this.isReady = true;
        });
    }

    private getDeleteAction(user: IUserGroupUserDisplay): IDropdownOption {
        return {
            text: this.translatePipe.transform("Remove"),
            action: (): void => {
                const modalData: IDeleteConfirmationModalResolve = {
                    modalTitle: this.translatePipe.transform("UserGroups.RemoveUser"),
                    confirmationText: this.translatePipe.transform("UserGroup.RemoveUserAttempt"),
                    modalIconTextClass: "color-cherry",
                    customKeyText: user.fullName,
                    customConfirmationText: this.translatePipe.transform("DeleteUserConfirmationText"),
                    modalIcon: "ot ot-remove-user",
                    submitButtonText: this.translatePipe.transform("Remove"),
                    cancelButtonText: this.translatePipe.transform("Cancel"),
                    promiseToResolve: (): ng.IPromise<boolean> => {
                        return this.UserGroups.removeUser(user, this.userGroupId).then(
                            (response: IProtocolResponse<IUserGroupDisplay>): boolean => {
                            if (response.result) {
                                this.getUserGroupMembers(this.params.page);
                            }
                            return response.result;
                        });
                    },
                };
                this.modalService.setModalData(modalData);
                this.modalService.openModal("deleteConfirmationModal");
            },
            autoId: "UserActionAddRemoveBtn",
        };
    }
}
