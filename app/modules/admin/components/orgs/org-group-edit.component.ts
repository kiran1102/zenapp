// Core
import { Component, Inject, OnInit } from "@angular/core";
import { StateService } from "@uirouter/core";

// 3rd Party
import {
    find,
    isUndefined,
    cloneDeep,
    isEmpty,
    isString,
    forEach,
    isArray,
} from "lodash";

// Redux
import { getOrgById } from "oneRedux/reducers/org.reducer";
import { StoreToken } from "tokens/redux-store.token";

// Services
import { ModalService } from "sharedServices/modal.service";
import { OrgActionService } from "oneServices/actions/org-action.service";
import { TenantTranslationService } from "sharedServices/tenant-translation.service";
import { SmtpConnectionsTemplateListService } from "settingsComponents/smtp-connections-list/smtp-connections-template-list.service";
import { Principal } from "modules/shared/services/helper/principal.service";

// Interfaces
import { IStore } from "interfaces/redux.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";
import { ISelectedUser,
    IOrganizationOrgUserDropdown,
    IOrganizationResponse,
} from "interfaces/organization-and-groups.interface";
import { IUser } from "interfaces/user.interface";
import { IOrganization } from "interfaces/org.interface";
import { IDeleteConfirmationModalResolve } from "interfaces/delete-confirmation-modal-resolve.interface";
import { ILanguageActivatorData } from "interfaces/email-template.interface";
import { ILanguage } from "interfaces/localization.interface";

// Enums
import { OrgUserTraversal } from "enums/org-user-dropdown.enum";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

@Component({
    selector: "org-group-edit",
    templateUrl: "./org-group-edit.component.html",
})

export class OrgGroupEdit implements OnInit {

    isLoading: boolean;
    languages: ILanguageActivatorData[];
    allLanguages: ILanguageActivatorData[];
    orgUserTraversal = OrgUserTraversal;
    organization: IOrganization;
    paramsId: string;
    selectedLanguage: ILanguageActivatorData;
    selectedUser: ISelectedUser | string;
    showPrivacyOfficerName: string;
    tenantLanguages: ILanguage[];

    constructor(
        @Inject(ModalService) private modalService: ModalService,
        @Inject(OrgActionService) private OrgAction: OrgActionService,
        @Inject("TenantTranslationService") private tenantTranslationService: TenantTranslationService,
        @Inject(StoreToken) private store: IStore,
        private stateService: StateService,
        private translatePipe: TranslatePipe,
        private smtpConnectionsTemplateListService: SmtpConnectionsTemplateListService,
        private principal: Principal,
    ) {}

    ngOnInit(): void {
        this.isLoading = true;
        this.tenantLanguages = this.tenantTranslationService.getLanguages();
        this.smtpConnectionsTemplateListService.getEmailLanguages().then((response: ILanguageActivatorData[] | boolean): void => {
            if (response && isArray(response)) {
                forEach(response, (selection: ILanguageActivatorData): void => {
                    if (selection.Translated) {
                        selection.Activated = true;
                    }
                });
                this.languages = response.filter((languageData: ILanguageActivatorData) => languageData.Activated);
                this.allLanguages = response;
            }
            this.populateOrgFields();
        });
        this.paramsId = this.stateService.params["Id"] || "";
        this.organization = this.paramsId ? cloneDeep(getOrgById(this.paramsId)(this.store.getState())) : null;
    }

    populateOrgFields(): void {
        this.selectedUser =  this.organization.Id
            ? {
                Id: this.organization.PrivacyOfficerId,
                FullName: this.organization.PrivacyOfficerName,
            } : "";
        this.selectedLanguage = find(this.allLanguages, (lang: ILanguageActivatorData): boolean => {
            return lang && lang.Id === this.organization.LanguageId;
        }) || null;
        this.isLoading = false;
    }

    getOrganizationsList() {
        this.isLoading = true;
        return this.OrgAction.fetchOrg(this.principal.getRootOrg(), this.principal.getOrggroup())
        .then((res) => {
            this.isLoading = false;
        });
    }

    saveOrgName(event: string): void {
        this.organization.Name = event;
    }

    selectUser(user: IOrganizationOrgUserDropdown): void {
        this.selectedUser = user.selection;
    }

    selectLanguage(language: ILanguageActivatorData): void {
        this.selectedLanguage = language;
    }
    cancelEdit(): void {
        this.stateService.go("zen.app.pia.module.admin.organizations.list");
    }

    saveOrgGroup(): void {
        const orgGroupRequest: IOrganizationResponse = {
            Id: this.organization.Id,
            Name: this.organization.Name,
            PrivacyOfficerId: !isString(this.selectedUser) ? this.selectedUser.Id : "",
            LanguageId: this.selectedLanguage.Id,
        };
        this.isLoading = true;
        this.OrgAction.updateOrgGroup(orgGroupRequest).then((response: IProtocolResponse<void>): void => {
            this.getOrganizationsList();
            this.isLoading = false;
        });
    }

    deleteOrg(): void {
        const modalData: IDeleteConfirmationModalResolve = {
            modalTitle: this.translatePipe.transform("DeleteOrg"),
            confirmationText: this.translatePipe.transform("DeleteOrgConfirmationText"),
            customConfirmationText: this.translatePipe.transform("DoYouWantToContinue"),
            submitButtonText: this.translatePipe.transform("Delete"),
            cancelButtonText: this.translatePipe.transform("Cancel"),
            promiseToResolve: (): ng.IPromise<boolean> =>
                this.OrgAction.deleteOrgGroup(this.organization.Id).then((response: IProtocolResponse<void>): boolean => {
                    if (response.result) {
                        this.stateService.go("zen.app.pia.module.admin.organizations.list");
                    }
                    return response.result;
                }),
        };
        this.modalService.setModalData(modalData);
        this.modalService.openModal("deleteConfirmationModal");
    }

    validationsForEditModal(): boolean {
        return isEmpty(this.selectedLanguage) || isEmpty(this.organization.Name)
            || isEmpty(this.selectedUser);
    }

    goToRoute(route: { path: string, params: any }) {
        this.stateService.go(route.path);
    }

}
