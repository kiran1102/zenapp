// Core
import { Component, Inject, OnInit } from "@angular/core";

// Common
import { TranslationsService } from "@onetrust/common";

// 3rd Party
import {
    find,
    isUndefined,
    cloneDeep,
    isEmpty,
    forEach,
    isString,
    isObject,
    isArray,
} from "lodash";

// Redux
import { getModalData } from "oneRedux/reducers/modal.reducer";
import { getOrgById } from "oneRedux/reducers/org.reducer";
import { StoreToken } from "tokens/redux-store.token";

// Rxjs
import { take } from "rxjs/operators";

// Services
import { ModalService } from "sharedServices/modal.service";
import { OrgActionService } from "oneServices/actions/org-action.service";
import { SmtpConnectionsTemplateListService } from "settingsComponents/smtp-connections-list/smtp-connections-template-list.service";

// Interfaces
import { IStore } from "interfaces/redux.interface";
import { IUser } from "interfaces/user.interface";
import { IOrganization } from "interfaces/org.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";
import { IOrganizationResponse,
    ISelectedUser,
    IOrganizationOrgUserDropdown,
} from "interfaces/organization-and-groups.interface";
import { ILanguageActivatorData } from "interfaces/email-template.interface";
import { ILanguage } from "interfaces/localization.interface";

// Enums
import { OrgUserDropdownTypes } from "enums/org-user-dropdown.enum";
import { OrgUserTraversal } from "enums/org-user-dropdown.enum";

@Component({
    selector: "org-group-create",
    templateUrl: "./org-group-create.component.html",
})

export class OrgGroupCreate implements OnInit {

    dropdownType: number = OrgUserDropdownTypes.OrgGroupDefaultApprover;
    isLoading: boolean;
    isSavingNext: boolean;
    languages: ILanguageActivatorData[];
    allLanguages: ILanguageActivatorData[];
    loadingGroups: boolean;
    modalData: any;
    orgName: string;
    orgUserTraversal = OrgUserTraversal;
    organization: IOrganization;
    selectedLanguage: ILanguageActivatorData;
    selectedOrg: string;
    selectedUser: ISelectedUser | string;
    tenantLanguages: ILanguage[];
    orgId: string;

    constructor(
        @Inject(ModalService) private modalService: ModalService,
        @Inject(OrgActionService) private orgActionService: OrgActionService,
        @Inject(StoreToken) private store: IStore,
        private smtpConnectionsTemplateListService: SmtpConnectionsTemplateListService,
        private translationsService: TranslationsService,
    ) {}

    ngOnInit(): void {
        this.isLoading = true;
        this.modalData = getModalData(this.store.getState());
        this.orgId = this.modalData.parentId;
        this.getLanguages();
        this.smtpConnectionsTemplateListService.getEmailLanguages().then((response: ILanguageActivatorData[] | boolean): void => {
            if (response && isArray(response)) {
                forEach(response, (selection: ILanguageActivatorData): void => {
                    if (selection.Translated) {
                        selection.Activated = true;
                    }
                });
                this.languages = response.filter((languageData: ILanguageActivatorData) => languageData.Activated);
                this.allLanguages = response;
            }
            this.populateOrgGroupValues();
        });
        this.organization = cloneDeep(getOrgById(this.modalData.parentId)(this.store.getState()));
    }

    getLanguages() {
        this.translationsService.getGlobalLanguages()
        .pipe(take(1))
        .subscribe((res: ILanguage[]) => {
            this.tenantLanguages = res;
        });
    }

    populateOrgGroupValues(): void {
        this.orgName = this.modalData.isEditOrg && this.organization ? this.organization.Name : "";
        this.selectedOrg = !this.modalData.isEditOrg && isEmpty(this.selectedOrg)
            ? this.modalData.rootId : this.selectedOrg;
        this.selectedUser =  this.organization && this.modalData.isEditOrg
            ? {
                Id: this.organization.PrivacyOfficerId,
                FullName: this.organization.PrivacyOfficerName,
            } : "";
        this.selectedLanguage = this.modalData.isEditOrg ?
            (find(this.allLanguages, (lang: ILanguageActivatorData): boolean => {
                return this.organization
                    && lang.Id === this.organization.LanguageId
                    && this.modalData.isEditOrg;
            }) || null)
            : !isEmpty(this.selectedLanguage) ? this.selectedLanguage : this.languages[0];
        this.isLoading = false;
    }

    closeModal(): void {
        this.modalService.closeModal();
        this.modalService.clearModalData();
    }

    setOrgName(event: string): void {
        this.orgName = event;
    }

    selectLanguage(language: ILanguageActivatorData): void {
        this.selectedLanguage = language;
    }

    selectUser(user: IOrganizationOrgUserDropdown): void {
        this.selectedUser = user.selection;
    }

    selectOrg(selection: string): void {
        this.selectedOrg = selection;
        this.orgId = this.selectedOrg;
    }

    handleSubmit(editNextRow: boolean = false): void {
        if (!this.modalData.isEditOrg) {
            this.addOrgGroup(editNextRow);
        } else {
            this.editOrgGroup(editNextRow);
        }
    }

    addOrgGroup(addNextRow: boolean): void {
        const newOrgGroupRequest: IOrganizationResponse = {
            Name: this.orgName,
            ParentId: this.modalData.parentId ? this.modalData.parentId : this.selectedOrg,
            LanguageId: this.selectedLanguage.Id,
            PrivacyOfficerId: !isString(this.selectedUser) ? this.selectedUser.Id : "",
        };
        if (addNextRow) {
            this.isSavingNext = true;
        } else {
            this.isLoading = true;
        }
        this.orgActionService.addOrgGroup(newOrgGroupRequest).then((res: IProtocolResponse<void>): void => {
            if (res.result) {
                this.loadingGroups = true;
                if (addNextRow) {
                    this.isLoading = true;
                    this.setNextRowOrgGroup();
                    this.modalService.handleModalCallback({ result: this.loadingGroups });
                } else {
                    this.modalService.handleModalCallback({ result: true, data: res.data });
                    this.closeModal();
                }
            }
            this.isSavingNext = false;
        });
    }

    setNextRowOrgGroup(): void {
        this.isSavingNext = false;
        this.isLoading = false;
        const newAttributeRequest: IOrganizationResponse = {
            Name: "",
            ParentId: "",
            LanguageId: null,
            PrivacyOfficerId: "",
        };
        this.loadingGroups = true;
        if (newAttributeRequest) {
            this.modalService.updateModalData(newAttributeRequest);
            this.populateOrgGroupValues();
        } else {
            this.modalService.handleModalCallback({result: this.loadingGroups});
            this.modalService.closeModal();
            this.modalService.clearModalData();
        }
    }

    editOrgGroup(editNextRow: boolean): void {
        const editOrgGroupRequest: IOrganizationResponse = {
            Id: this.organization.Id,
            Name: this.orgName,
            PrivacyOfficerId: !isString(this.selectedUser) ? this.selectedUser.Id : "",
            LanguageId: this.selectedLanguage.Id,
        };
        this.isLoading = true;
        this.orgActionService.updateOrgGroup(editOrgGroupRequest).then((res: IProtocolResponse<void>): void => {
            if (res.result) {
                this.modalService.handleModalCallback({ result: res.result, data: res.data });
                this.closeModal();
            }
        });
    }

    validationsForOrgModal(): boolean {
        return isEmpty(this.selectedLanguage) || isEmpty(this.orgName) || isEmpty(this.selectedUser)
            || (!this.modalData.parentId ? isEmpty(this.selectedOrg) : isEmpty(this.modalData.parentId));
    }
}
