// Core
import { Component, Inject, OnInit } from "@angular/core";
import { StateService } from "@uirouter/core";

// 3rd Party
import { DndListModule } from "ngx-drag-and-drop-lists";
import {
    forEach,
    isNull,
    isEmpty,
    flattenDeep,
    omit,
    assign,
    map,
    union,
    uniq,
    cloneDeep,
    sortBy,
} from "lodash";

// Services
import { Principal } from "modules/shared/services/helper/principal.service";
import { OrgActionService } from "oneServices/actions/org-action.service";
import { StoreToken } from "tokens/redux-store.token";
import { ModalService } from "sharedServices/modal.service";

// Reducers
import {
    getOrgTreeById,
    getParentOrgs,
    getOrgById,
} from "oneRedux/reducers/org.reducer";
import { getCurrentUser } from "oneRedux/reducers/user.reducer";

// Interfaces
import { IDeleteConfirmationModalResolve } from "interfaces/delete-confirmation-modal-resolve.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";
import { IOrganizationModal } from "interfaces/organization-and-groups.interface";
import { IOrganization } from "interfaces/org.interface";
import { IUser } from "interfaces/user.interface";
import { IStore } from "interfaces/redux.interface";
import { IStringMap } from "interfaces/generic.interface";

// Enums & Constants
import { EmptyGuid } from "constants/empty-guid.constant";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";
import { FilterPipe } from "pipes/filter.pipe";

@Component({
    selector: "org-tree-manage-view",
    templateUrl: "./org-tree-manage-view.component.html",
    providers: [
        DndListModule,
    ],
})

export class OrgTreeManageViewComponent implements OnInit {

    currentUser: IUser;
    organizations: IOrganization[];
    isHovering = false;
    orgExpanded = true;
    searchModel = "";
    droppedNode: IOrganization;
    isLoading: boolean;
    dragNode = false;
    draggedNode: IOrganization;
    cloneOrg: IOrganization[];

    constructor(
        @Inject(OrgActionService) private OrgAction: OrgActionService,
        @Inject(StoreToken) private store: IStore,
        private stateService: StateService,
        @Inject(ModalService) private modalService: ModalService,
        private principal: Principal,
        private filterPipe: FilterPipe,
        private translatePipe: TranslatePipe,
    ) {}

    ngOnInit(): void {
        this.currentUser = getCurrentUser(this.store.getState());
        this.getOrganizationsList();
    }

    trackByNode(index: number): number {
        return index;
    }

    droppedValue(event) {
        this.droppedNode = event;
    }

    draggedValue(event) {
        this.draggedNode = event;
        const orgList = this.findChildNode(this.droppedNode, event);
        this.isLoading = true;
        this.OrgAction.reorderOrg(orgList).then((response: IProtocolResponse<void>): void => {
            if (response.result) {
                this.getOrganizationsList(this.draggedNode);
            }
            this.isLoading = false;
        });
    }

    findChildNode(list: IOrganization, node: IOrganization): IStringMap<string> {
        const org: IStringMap<string> = {};
        forEach(list.Children, (item: IOrganization): void => {
            if (item.Id === node.Id) {
                org.NewParentId = list.Id;
                org.ParentId = node.ParentId;
                org.OrgId = node.Id;
                if (list.Children.indexOf(item) > 0
                    && list.Children.indexOf(item) !== list.Children.length - 1) {
                    org.AfterId = list.Children[list.Children.indexOf(item) - 1].Id;
                } else if (list.Children.indexOf(item) === list.Children.length - 1) {
                    org.AfterId = null;
                } else {
                    org.AfterId = EmptyGuid;
                }
            } else {
                this.findChildNode(item, node);
            }
        });
        return org;
    }

    getOrganizationsList(parent: IOrganization = null, showCurrentOrg: boolean = false): ng.IPromise<void> {
        this.isLoading = true;
        this.searchModel = "";
        return this.OrgAction.fetchOrg(this.principal.getRootOrg(), this.principal.getOrggroup()).then(() => {
            const rootOrg: IOrganization = getOrgTreeById(this.currentUser.OrgGroupId)(this.store.getState());
            rootOrg.Visible = true;
            this.organizations = isNull(parent)
                ? [rootOrg]
                : this.expandOrgOnActions([rootOrg], parent, showCurrentOrg);
            this.organizations = this.sortByPosition(this.organizations[0]);
            this.cloneOrg = cloneDeep(this.organizations);
            this.isLoading = false;
        });
    }

    sortByPosition(org: IOrganization): IOrganization[] {
        const sortedTree = sortBy(org.Children, "Position");
        org.Children = sortedTree;
        forEach(org.Children, (node: IOrganization): void => {
            if (node.Children && node.Children.length) {
                this.sortByPosition(node);
            }
        });
        return [org];
    }

    expandOrgOnActions(organizations: IOrganization[], parentOrg: IOrganization, showCurrentOrg: boolean): IOrganization[] {
        const parentOrgs = getParentOrgs(parentOrg.Id)(this.store.getState());
        if (showCurrentOrg) {
            parentOrgs.push(parentOrg);
        }
        forEach(parentOrgs, (parent: IOrganization): void => {
            this.loopthroughChildren(organizations, parent);
        });
        return organizations;
    }

    loopthroughChildren(organizations: IOrganization[], parentOrgs: IOrganization): IOrganization[]  {
        return forEach(organizations, (org: IOrganization): IOrganization[] => {
            if (org.Id === parentOrgs.Id) {
                org.Visible = true;
                return organizations;
            } else {
                this.loopthroughChildren(org.Children, parentOrgs);
            }
        });
    }

    updateOrganization(id: string) {
        this.stateService.go("zen.app.pia.module.admin.organizations.edit", { Id: id });
    }

    unflattenOrg(orgList: IOrganization[]): IOrganization[] {
        const tree: IOrganization[] = [];
        const mappedOrg: IStringMap<IOrganization> = {};
        for (let i = 0; i < orgList.length; i++) {
            mappedOrg[orgList[i].Id] = orgList[i];
            mappedOrg[orgList[i].Id]["Visible"] = true;
            mappedOrg[orgList[i].Id]["Children"] = [];
            if (mappedOrg[orgList[i].Id].Id === this.principal.getOrggroup()) {
                mappedOrg[orgList[i].Id]["ParentId"] = null;
            }
        }
        for ( const org in mappedOrg) {
            if (mappedOrg.hasOwnProperty(org)) {
                if (mappedOrg[org].ParentId) {
                    mappedOrg[mappedOrg[org]["ParentId"]]["Children"].push(mappedOrg[org]);
                } else {
                    tree.push(mappedOrg[org]);
                }
            }
        }
        return tree;
    }

    removeItem(item: IOrganization, list: IOrganization[]): void {
        list.splice(list.indexOf(item), 1);
    }

    onDropCallback(item: IOrganization): void {
        this.droppedNode = item;
    }

    onSearch(filter: string): void {
        this.searchModel = filter;
        if (!isEmpty(this.searchModel)) {
            this.dragNode = true;
            const flatOrgArray = flattenDeep(this.flattenOrg(this.cloneOrg, []));
            const filteredList = this.filterPipe.transform(flatOrgArray, ["Name", "PrivacyOfficerName"], filter);
            const arrayOfNodes = [];
            forEach(filteredList, (array: IOrganization): void => {
                forEach(array.PathName, (path: string): void => {
                    arrayOfNodes.push(getOrgById(path)(this.store.getState()));
                });
            });
            this.organizations = this.unflattenOrg(uniq(arrayOfNodes));
        } else {
            this.dragNode = false;
            this.getOrganizationsList();
        }
    }

    flattenOrg(nodes: IOrganization[], path: string[]) {
        let newPath: string[];
        return map(nodes, (node: IOrganization) => {
            newPath = union(path, [node.Id]);
            return [
                assign({ PathName: newPath }, omit(node, "Children")),
                this.flattenOrg(node.Children, newPath),
            ];
        });
    }

    addOrganization(parent: IOrganization): void {
        const modalData: IOrganizationModal = {
            parentId: parent.Id,
            isAddNext: true,
            isEditOrg: false,
            modalTitle: this.translatePipe.transform("AddOrganization"),
            callback: (res: IProtocolResponse<void>): boolean => {
                if (res.result) {
                    this.getOrganizationsList(parent, true);
                }
                return res.result;
            },
        };
        this.modalService.setModalData(modalData);
        this.modalService.openModal("OrgGroupCreate");
    }

    editOrgModal(parent: IOrganization): void {
        const modalData: IOrganizationModal = {
            parentId: parent.Id,
            isAddNext: false,
            isEditOrg: true,
            modalTitle: this.translatePipe.transform("EditOrganization"),
            callback: (res: IProtocolResponse<void>): boolean => {
                if (res.result) {
                    this.getOrganizationsList(parent);
                }
                return res.result;
            },
        };
        this.modalService.setModalData(modalData);
        this.modalService.openModal("OrgGroupCreate");
    }

    addOrg(): void {
        const modalData: IOrganizationModal = {
            isAddNext: true,
            isEditOrg: false,
            rootId: this.cloneOrg[0].Id,
            modalTitle: this.translatePipe.transform("AddOrganization"),
            callback: (res: IProtocolResponse<void>): boolean => {
                if (res.result) {
                    this.getOrganizationsList();
                }
                return res.result;
            },
        };
        this.modalService.setModalData(modalData);
        this.modalService.openModal("OrgGroupCreate");
    }

    deleteOrganization(parent: IOrganization): void {
        const getParent = getOrgById(parent.ParentId)(this.store.getState());
        const modalData: IDeleteConfirmationModalResolve = {
            modalTitle: this.translatePipe.transform("DeleteOrg"),
            confirmationText: this.translatePipe.transform("DeleteOrgConfirmationText"),
            customConfirmationText: this.translatePipe.transform("DoYouWantToContinue"),
            submitButtonText: this.translatePipe.transform("Delete"),
            cancelButtonText: this.translatePipe.transform("Cancel"),
            promiseToResolve: (): ng.IPromise<boolean> =>
                this.OrgAction.deleteOrgGroup(parent.Id).then((response: IProtocolResponse<void>): boolean => {
                    if (response.result) {
                        this.getOrganizationsList(getParent, true);
                    }
                    return response.result;
                }),
        };
        this.modalService.setModalData(modalData);
        this.modalService.openModal("deleteConfirmationModal");
    }

}
