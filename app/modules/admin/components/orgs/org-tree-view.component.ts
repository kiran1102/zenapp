import { Component, EventEmitter, Input, Output, Inject } from "@angular/core";
import { DndListModule } from "ngx-drag-and-drop-lists";
import { IOrganization } from "interfaces/org.interface";

@Component({
    selector: "org-tree-view",
    templateUrl: "./org-tree-view.component.html",
    providers: [
        DndListModule,
    ],
})

export class OrgTreeViewComponent {

    isHovering = false;
    @Input() model: IOrganization;
    @Input() list: IOrganization[];
    @Input() draggable: boolean;
    @Output() droppedValue = new EventEmitter();
    @Output() draggedValue = new EventEmitter();
    @Output() editModal = new EventEmitter<string>();
    @Output() addOrg = new EventEmitter<string>();
    @Output() editOrg = new EventEmitter<string>();
    @Output() deleteOrg = new EventEmitter<string>();

    trackByNode(index): void {
        return index;
    }

    toggleVisibility(org: IOrganization): void {
        org.Visible = !org.Visible;
    }

    setHovering(hovering: boolean): void {
        this.isHovering = hovering;
    }

    removeItem(item: IOrganization, list: IOrganization[]): void {
        list.splice(list.indexOf(item), 1);
        this.draggedValue.emit(item);
    }

}
