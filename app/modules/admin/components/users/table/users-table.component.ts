// Angular
import {
    Component,
    Inject,
    OnInit,
} from "@angular/core";
import { StateService } from "@uirouter/core";

// External Libraries
import {
    each,
    find,
    map,
} from "lodash";
import { IMyDateRangeModel } from "mydaterangepicker";

// Redux
import { StoreToken } from "tokens/redux-store.token";
import { IStore } from "interfaces/redux.interface";
import { getCurrentUser } from "oneRedux/reducers/user.reducer";

// Services
import { ModalService } from "sharedServices/modal.service";
import { Permissions } from "modules/shared/services/helper/permissions.service";
import { RolesService } from "adminService/roles.service";
import { TimeStamp } from "oneServices/time-stamp.service";
import { UserActionService } from "oneServices/actions/user-action.service";
import { UserApiService } from "oneServices/api/user-api.service";

// Interfaces
import { IPageableMeta, IPaginationFilter } from "interfaces/pagination.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";
import {
    IUser,
    IUserActivateModalData,
    IUserSeedFilters,
    IUserTenantFilters,
} from "interfaces/user.interface";
import { IPageableOf } from "interfaces/pagination.interface";
import { IStringMap } from "interfaces/generic.interface";
import { IDropdownOption } from "interfaces/one-dropdown.interface";
import { ITabsNav } from "interfaces/one-tabs-nav.interface";
import { IRole } from "interfaces/role.interface";

// ENUMS
import { ExportTypes } from "enums/export-types.enum";
import { CellTypes } from "enums/general.enum";

// Constants
import { UserRoles } from "constants/user-roles.constant";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

interface IUserColumn {
    id: string;
    labelKey: string;
    rowTextKey?: string;
    rowTranslationKey?: string;
    type: number;
}
@Component({
    selector: "users-table",
    templateUrl: "users-table.component.html",
})
export class UsersTableComponent implements OnInit {
    dateModel: string;
    dateRangeModel: string;
    format = this.timeStamp.getDatePickerDateFormat();
    usersData: IPageableOf<IUser>;
    rows: IUser[];
    users: IPageableOf<IUser>;
    usersMeta: IPageableMeta;
    columns: IUserColumn[];
    tabOptions: ITabsNav[];
    cellTypes = CellTypes;
    TABS = {
        Active: "0",
        Inactive: "1",
    };
    DIALOGTYPE = {
        EnableUser: "1",
        DisableUser: "2",
        SendEmail: "3",
    };
    currentTab: string;
    currentPage: number;
    userTypes = [
        {
            label: this.translatePipe.transform("Internal"),
            value: "Internal",
        },
        {
            label: this.translatePipe.transform("External"),
            value: "External",
        },
    ];
    enableDisableEmailModalData: IUserActivateModalData;
    selectedUserType: string;
    selectedUserTypes: string[] = [];
    selectedRoles: string[] = [];
    rolesList: IRole[];
    filterIsOpen: boolean;
    buttonClass: string;
    selectedOrgGroup: string;
    isReady = false;
    exportInProgress = false;
    filterApplied = false;
    noRecordFound = false;
    isDateDisabledCheck = true;
    pageConfig: IStringMap<number> = { size: 20 };
    seedFilters: IUserSeedFilters = {};
    tenantFilters: IUserTenantFilters = {};
    menuClass: string;
    translations = {
        addUser: this.translatePipe.transform("AddUser"),
        pageTitle: this.translatePipe.transform("Users"),
        exportButtonText: this.translatePipe.transform("Export"),
        active: this.translatePipe.transform("Active"),
        inActive: this.translatePipe.transform("Inactive"),
        deactivateUser: this.translatePipe.transform("DeactivateUser"),
        enableUser: this.translatePipe.transform("EnableUser"),
        resendInvite: this.translatePipe.transform("ResendInvite"),
        deactivate: this.translatePipe.transform("Deactivate"),
        enable: this.translatePipe.transform("Enable"),
        send: this.translatePipe.transform("Send"),
        areYouSureContinue: this.translatePipe.transform("AreYouSureContinue"),
        cancel: this.translatePipe.transform("Cancel"),
        youAreTryingToDeactivateUser: this.translatePipe.transform("YouAreTryingToDeactivateUser"),
        youAreTryingToEnableUser: this.translatePipe.transform("YouAreTryingToEnableUser"),
        searchUsers: this.translatePipe.transform("SearchUsers"),
        clearFilters: this.translatePipe.transform("ClearFilters"),
        managedBy: this.translatePipe.transform("ManagedBy"),
        defaultRole: this.translatePipe.transform("DefaultRole"),
        status: this.translatePipe.transform("Status"),
        expirationDateRange: this.translatePipe.transform("ExpirationDateRange"),
        apply: this.translatePipe.transform("Apply"),
        internal: this.translatePipe.transform("Internal"),
        external: this.translatePipe.transform("External"),
    };

    activeUsersColumns = [
        {
            id: "Name",
            labelKey: "Name",
            rowTextKey: "FullName",
            type: this.cellTypes.link,
        },
        {
            id: "ManagedBy",
            labelKey: "ManagedBy",
            rowTextKey: "OrgGroupName",
            type: this.cellTypes.text,
        },
        {
            id: "RoleName",
            labelKey: "DefaultRole",
            rowTextKey: "RoleName",
            type: this.cellTypes.text,
        },
        {
            id: "Email",
            labelKey: "Email",
            rowTextKey: "Email",
            type: this.cellTypes.text,
        },
        {
            id: "InternalExternal",
            labelKey: "InternalExternal",
            type: this.cellTypes.text,
        },
        {
            id: "ExpirationDate",
            labelKey: "ExpirationDate",
            type: this.cellTypes.text,
        },
    ];

    nonActiveUserscolumns = [
        {
            id: "DisabledDate",
            labelKey: "DisabledDate",
            rowTextKey: "",
            type: this.cellTypes.text,
        },
        {
            id: "DisabledBy",
            labelKey: "DisabledBy",
            rowTextKey: "DisabledBy",
            type: this.cellTypes.text,
        },
    ];

    private userSearch: string;
    private currentTimeFormat: string;
    private currentDateFormat: string;
    private filter = { activeKey: "all" };
    private exportType: IStringMap<number> = {
        all: 0,
        disabled: 30,
    };
    private pageFilters: IPaginationFilter[];

    constructor(
        private stateService: StateService,
        @Inject(StoreToken) private store: IStore,
        @Inject(ModalService) private modalService: ModalService,
        @Inject(Permissions) public permissions: Permissions,
        @Inject(RolesService) private rolesService: RolesService,
        @Inject(UserActionService) public UserActions: UserActionService,
        @Inject(UserApiService) private UserApi: UserApiService,
        @Inject("Export") private exportService: any,
        private translatePipe: TranslatePipe,
        private readonly timeStamp: TimeStamp,
    ) {
        this.tabOptions = [
            { text: this.translations.active, id: this.TABS.Active },
            { text: this.translations.inActive, id: this.TABS.Inactive },
        ];
        this.seedFilters.enabled = true;
        this.seedFilters.internal = null;
        this.seedFilters.external = null;
        this.tenantFilters.roleId = [];
    }

    ngOnInit(): void {
        this.isReady = true;
        this.currentTab = this.TABS.Active;
        each(this.tabOptions, (option: ITabsNav): void => {
            option.action = (option_selected: ITabsNav): void => {
                this.changeTab(option_selected);
            };
        });

        const currentTab =
            this.currentTab === this.TABS.Active
                ? this.TABS.Active
                : this.TABS.Inactive;
        if (this.currentPage) {
            this.pageChange(this.currentPage);
        } else {
            this.changeTab(this.tabOptions[currentTab]);
        }
    }

    pageChange(page: number, size = this.pageConfig.size): ng.IPromise<IPageableOf<IUser>> {
        this.isReady = false;
        const params = {
            page,
            size,
            seedFilters: this.seedFilters,
            tenantFilters: this.tenantFilters,
        };
        this.currentPage = page;
        return this.UserActions.fetchUsersByPage(params).then((userPage: IPageableOf<IUser>) => {
            this.usersData = userPage;
            if (this.usersData && this.usersData.content) {
                this.usersData.content = this.setEditPermissions(this.usersData.content);
            }
            this.noRecordFound = !(this.usersData.content.length);
            this.isReady = true;
            return this.usersData;
        });
    }

    onToggle(filterIsOpen): void {
        this.filterIsOpen = filterIsOpen;
    }

    selectOrgGroup(selection: string): void {
        this.selectedOrgGroup = selection;
        this.tenantFilters.organizationId = this.selectedOrgGroup;
        this.selectedRoles = [];
        if (!this.selectedOrgGroup) {
            this.rolesList = [];
        } else {
            this.getRoles(this.selectedOrgGroup);
        }
    }

    selectUserType(value: string[]): void {
        this.selectedUserTypes = value;
        this.isDateDisabled();
        this.seedFilters.internal = Boolean(
            find(this.selectedUserTypes, { value: "Internal" }),
        );
        this.seedFilters.external = Boolean(
            find(this.selectedUserTypes, { value: "External" }),
        );
    }

    selectRoles(value: string[]): void {
        this.selectedRoles = value;
        this.tenantFilters.roleId = map(this.selectedRoles, "Id");
    }

    checkFilterOn(): boolean {
        return Boolean(this.selectedOrgGroup
                || this.selectedUserTypes.length
                || this.selectedRoles.length,
        );
    }

    isDateDisabled(): void {
        this.isDateDisabledCheck = !find(this.selectedUserTypes, { value: "External" });
    }

    dateValueChanged(selectedValues: IMyDateRangeModel): void {
        this.dateRangeModel = selectedValues.formatted;
        if (this.dateRangeModel) {
            this.seedFilters.beginDate = selectedValues.beginJsDate.toISOString();
            this.seedFilters.endDate = selectedValues.endJsDate.toISOString();
        } else {
            this.seedFilters.beginDate = null;
            this.seedFilters.endDate = null;
        }
    }

    gotoAddUser(): void {
        this.stateService.go("zen.app.pia.module.admin.users.create");
    }

    gotoEditUser(userId: string): void {
        this.stateService.go("zen.app.pia.module.admin.users.edit", {
            id: userId,
        });
    }

    columnTrackBy(column: IUserColumn): string {
        return column.id;
    }

    searchUsers(model: string): void {
        this.seedFilters.search = model;
        this.pageChange(0);
    }

    getActions(user: IUser): () => IDropdownOption[] {
        const fullName = user.FirstName || user.LastName
            ? `${user.FirstName} ${user.LastName}`.trim()
            : `${ user.Email }`;
        return (): IDropdownOption[] => {
            const menuOptions: IDropdownOption[] = [];
            if (!user.IsActive && user.canBeEnabled) {
                menuOptions.push({
                    textKey: "EnableUser",
                    action: (): void =>
                        this.openConfirmationModal(
                            fullName,
                            this.DIALOGTYPE.EnableUser,
                            user,
                        ),
                });
            }
            if (user.IsActive && user.canBeDisabled) {
                menuOptions.push({
                    textKey: "DeactivateUser",
                    action: (): void =>
                        this.openConfirmationModal(
                            fullName,
                            this.DIALOGTYPE.DisableUser,
                            user,
                        ),
                });
            }
            if (!user.EmailConfirmed && user.IsActive && user.canResendEmail) {
                menuOptions.push({
                    textKey: "ResendInvite",
                    action: (): void =>
                        this.openConfirmationModal(
                            user.Email,
                            this.DIALOGTYPE.SendEmail,
                            user,
                        ),
                });
            }
            if (!menuOptions.length) {
                menuOptions.push({
                    textKey: "NoActionsAvailable",
                    action: (): void => {},
                });
            }
            return menuOptions;
        };
    }

    changeTab(option: ITabsNav): void {
        this.currentTab = option.id;
        switch (option.id) {
            case this.TABS.Active:
                this.seedFilters.enabled = true;
                this.pageChange(0);
                this.columns = this.activeUsersColumns;
                break;
            case this.TABS.Inactive:
                this.seedFilters.enabled = false;
                this.pageChange(0);
                this.columns = [
                    ...this.activeUsersColumns,
                    ...this.nonActiveUserscolumns,
                ];
                break;
        }
    }

    openConfirmationModal(
        userName: string,
        dialogType: string,
        user: IUser,
    ): void {
        const userId = user.Id;
        this.enableDisableEmailModalData = {
            modalTitle:
                dialogType === this.DIALOGTYPE.DisableUser
                    ? this.translations.deactivateUser
                    : dialogType === this.DIALOGTYPE.EnableUser
                        ? this.translations.enableUser
                        : this.translations.resendInvite,
            submitButtonText:
                dialogType === this.DIALOGTYPE.DisableUser
                    ? this.translations.deactivate
                    : dialogType === this.DIALOGTYPE.EnableUser
                        ? this.translations.enableUser
                        : this.translations.send,
            confirmationText:
                dialogType === this.DIALOGTYPE.DisableUser
                    ? `${this.translations.youAreTryingToDeactivateUser}
                        <div class='text-bold margin-top-2'>${userName}</div>
                        <div class='text-bold margin-top-2'>${this.translations.areYouSureContinue}</div>`
                    : dialogType === this.DIALOGTYPE.EnableUser
                    ? `${this.translations.youAreTryingToEnableUser}
                        <div class='text-bold margin-top-2'>${userName}</div>
                        <div class='text-bold margin-top-2'>${this.translations.areYouSureContinue}</div>`
                    : `${this.translations.resendInvite}
                        <div class='text-bold margin-top-2'>${user.Email}</div>
                        <div class='text-bold margin-top-2'>${this.translations.areYouSureContinue}</div>`,
            iconTemplate:
                dialogType === this.DIALOGTYPE.DisableUser
                    ? `<span class="delete-confirmation-modal__icon fa-stack fa-lg">
                        <i class="fa fa-circle fa-stack-2x text-warning"></i>
                        <i class="fa fa-user-times fa-stack-1x fa-inverse"></i>
                     </span>`
                    : dialogType === this.DIALOGTYPE.EnableUser
                        ? `<span class="delete-confirmation-modal__icon fa-stack fa-lg">
                        <i class="fa fa-circle fa-stack-2x text-success"></i>
                        <i class="fa  fa-user-plus fa-stack-1x fa-inverse"></i>
                    </span>`
                        : `<span class="delete-confirmation-modal__icon fa-stack fa-lg">
                        <i class="fa fa-circle fa-stack-2x text-success"></i>
                        <i class="fa fa-send fa-stack-1x fa-inverse"></i>
                    </span>`,
            cancelButtonText: this.translations.cancel,
            promiseToResolve: (date?: Date): ng.IPromise<boolean> =>
                dialogType === this.DIALOGTYPE.DisableUser
                    ? this.deactivateUser(userId)
                    : dialogType === this.DIALOGTYPE.EnableUser
                        ? this.activateUser(userId, date)
                        : this.resendRegistrationMail(userId),
        };
        this.modalService.setModalData(this.enableDisableEmailModalData);
        if (!user.IsInternal && dialogType === this.DIALOGTYPE.EnableUser) {
            this.modalService.openModal("externalUserEnableModal");
        } else {
            this.modalService.openModal("confirmationModal");
        }
    }

    deactivateUser(userId): ng.IPromise<boolean> {
        return this.UserApi.deactivateUser(userId).then(
            (response: IProtocolResponse<void>): boolean => {
                if (response.result) {
                    this.updateUsersPage(this.tabOptions[this.TABS.Active]);
                }
                return response.result;
            },
        );
    }

    activateUser(id: string, date: Date): ng.IPromise<boolean> {
        return this.UserApi.activateUser({ UserId: id, ExpirationDT: date }).then(
            (response: IProtocolResponse<void>): boolean => {
                if (response.result) {
                    this.updateUsersPage(this.tabOptions[this.TABS.Inactive]);
                }
                return response.result;
            },
        );
    }

    resendRegistrationMail(userId): ng.IPromise<boolean> {
        return this.UserApi.resendRegistrationEmail(userId).then(
            (response: IProtocolResponse<void>): boolean => {
                if (response.result) {
                    this.updateUsersPage(this.tabOptions[this.TABS.Active]);
                }
                return response.result;
            },
        );
    }

    exportUsers(): void {
        let data: number;
        this.currentDateFormat = this.timeStamp.currentDateFormatting;
        this.currentTimeFormat = this.timeStamp.currentTimeFormatting;
        if (this.currentTab === this.TABS.Active) {
            this.filter.activeKey = "all";
            data = this.exportType.all;
        } else {
            this.filter.activeKey = "disabled";
            data = this.exportType.disabled;
        }
        const url: string =
            !this.currentTimeFormat && !this.currentDateFormat
                ? `/OrgGroupUser/DownloadUsersReport?userType=${data}`
                : `/OrgGroupUser/DownloadUsersReport?userType=${data}&dateFormat=${this.currentDateFormat}&timeFormat=${this.currentTimeFormat}`;

        this.exportInProgress = true;
        this.exportService.downloadFile(
            url,
            ExportTypes.XLSX,
            this.translatePipe.transform("usersReportExportXLSX", { userExportType: this.filter.activeKey }),
            this.translatePipe.transform("ErrorCreatingUsersReportXLSX"),
        ).then((): void => {
            this.exportInProgress = false;
        });
    }

    multiFilterUsers(): void {
        this.pageChange(0);
        this.filterIsOpen = false;
        this.filterApplied = this.checkFilterOn();
    }

    clearAllFiters(): void {
        this.seedFilters = {
            ...this.seedFilters,
            search: "",
            internal: null,
            external: null,
            beginDate: null,
            endDate: null,
        };
        this.tenantFilters = {
            ...this.tenantFilters,
            organizationId: null,
            roleId: [],
        };
        this.selectedRoles = [];
        this.selectedUserTypes = [];
        this.selectedOrgGroup = "";
        this.filterApplied = false;
        this.isDateDisabledCheck = true;
        this.dateRangeModel = "";
        this.rolesList = [];
        this.filterIsOpen = false;

        this.pageChange(0);
    }

    filterApplyButttonEnable(): boolean {
        return !this.selectedUserTypes.length
            && !this.selectedOrgGroup
            && !this.selectedRoles.length;
    }

    setMenuClass(event?: MouseEvent): void {
        this.menuClass = "actions-table__context-list";
        if ((window.innerHeight - 200) / 2 < (event.clientY - 200)) {
            this.menuClass += " actions-table__context-list--above";
        }
    }

    private getRoles(organizationId?: string): ng.IPromise<IProtocolResponse<IRole[]>> {
        return this.rolesService.listRoles({ organizationId, isAssignable: true }).then(
            (response: IProtocolResponse<IRole[]>): IProtocolResponse<IRole[]> => {
                if (response.result) {
                    this.rolesList = this.rolesService.translateRoleNamesAndDescription(response.data);
                }
                return response;
            },
        );
    }

    private updateUsersPage(tabOption: ITabsNav): void {
        if (this.currentPage > 0) {
            this.pageChange(this.currentPage);
        } else {
            this.changeTab(tabOption);
        }
    }

    private setEditPermissions(users: IUser[]): IUser[] {
        const currentUser = getCurrentUser(this.store.getState());
        return map(users, (user: IUser): IUser => {
            user.canBeEdited = this.permissions.canShow("UsersEdit") && !user.IsReadOnly;

            user.canBeDisabled = this.permissions.canShow("UsersDisableUser")
                && user.Id !== currentUser.Id
                && user.IsActive
                && !user.IsReadOnly;

            user.canBeEnabled = this.permissions.canShow("UsersEnableUser")
                && !user.IsActive
                && !user.IsReadOnly;

            user.canResendEmail = this.permissions.canShow("UsersResendEmail")
                && user.RoleName !== UserRoles.Invited
                && user.IsActive
                && !user.IsReadOnly
                && !user.EmailConfirmed
                && !user.IsDirectoryUser;

            return user;
        });
    }
}
