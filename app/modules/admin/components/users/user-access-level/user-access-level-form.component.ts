import Utilities from "Utilities";
import { map, remove } from "lodash";
import OrgGroupStoreNew from "oneServices/org-group.store";
import { AccessLevelPickerActions } from "constants/access-level-picker-actions.constant";
import { IAccessLevel } from "interfaces/user-access-level.interface";
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import { IFormFieldErrors } from "interfaces/form-field.interface";
import { IOrganization } from "interfaces/org.interface";
import { IStringMap } from "interfaces/generic.interface";

class UserAccessLevelFormController implements ng.IComponentController {
    static $inject: string[] = ["$rootScope", "OrgGroupStoreNew"];

    public accessLevels: IAccessLevel[];
    public formErrors: IFormFieldErrors;
    public handleAction: Function;
    public rootOrgId: string;

    private emptyAccess: IAccessLevel = {
        id: null,
        organizationId: "",
        roleId: null,
        isDefault: false,
    };

    constructor(
        private readonly $rootScope: IExtendedRootScopeService,
        private readonly OrgGroupStore: OrgGroupStoreNew,
    ) {
        this.rootOrgId = OrgGroupStore.orgTable.rootId;
    }

    public $onChanges(): void {
        if (!this.accessLevels) {
            this.accessLevels = [this.getEmptyAccessLevel(0)];
        } else {
            this.accessLevels = this.applyTrackingIds(this.accessLevels);
        }
    }

    public addAccessLevel(): void {
        this.accessLevels.push(this.getEmptyAccessLevel(this.accessLevels.length));
        this.handleAction({ accessLevels: this.accessLevels });
    }

    public handlePickerAction(type: string, accessLevel: IAccessLevel): void {
        switch (type) {
            case AccessLevelPickerActions.SET_DEFAULT:
                this.accessLevels = map(this.accessLevels, (level: IAccessLevel): IAccessLevel => {
                    level.isDefault = Boolean(level.id === accessLevel.id);
                    return level;
                });
                break;
            case AccessLevelPickerActions.DELETE_ACCESS:
                this.accessLevels = remove(this.accessLevels, (level: IAccessLevel): boolean => {
                    return level.id !== accessLevel.id;
                });
                break;
        }
        return this.handleAction({ type, accessLevels: this.accessLevels});
    }

    public getLevelFormError(level: IAccessLevel): IStringMap<any> {
        return this.formErrors
                && this.formErrors.map
                && this.formErrors.map[level.id];
    }

    public hasInvalidOrg(level: IAccessLevel): boolean {
        return level.organizationId && !this.OrgGroupStore.orgTable.hasOwnProperty(level.organizationId);
    }

    private getEmptyAccessLevel(index: number): IAccessLevel {
        return { ...this.emptyAccess, id: index.toString(), organizationId: this.rootOrgId, isDefault: !index };
    }

    private applyTrackingIds(levels: IAccessLevel[]): IAccessLevel[] {
        return map(levels, (level: IAccessLevel, index: number): IAccessLevel => {
            if (!level.id || (parseInt(level.id, 10))) {
                level.id = index.toString();
            }
            return level;
        });
    }

}

export const userAccessLevelForm: ng.IComponentOptions = {
    bindings: {
        accessLevels: "<",
        isDisabled: "<?",
        handleAction: "&",
        formErrors: "<?",
        roles: "<?",
    },
    controller: UserAccessLevelFormController,
    template: `
        <div class="user-access-level">
            <user-access-level-picker
                ng-repeat="level in $ctrl.accessLevels track by level.id"
                class="block border-bottom padding-top-bottom-2"
                access-level="level"
                is-disabled="$ctrl.isDisabled || $ctrl.hasInvalidOrg(level)"
                can-delete="$ctrl.accessLevels.length > 1"
                handle-action="$ctrl.handlePickerAction(type, payload)"
                field-error="$ctrl.getLevelFormError(level)"
                roles="$ctrl.roles"
            ></user-access-level-picker>
            <one-button
                identifier="UserAccessLevelPickerAdd"
                ng-if="!$ctrl.isDisabled"
                button-class="margin-top-2 width-30-percent"
                text="{{::$root.t('AddRole')}}"
                is-rounded="true"
                button-click="$ctrl.addAccessLevel()"
            ></one-button>
            <p
                ng-if="$ctrl.formErrors && $ctrl.formErrors.list"
                class="text-error margin-all-0 margin-top-half"
                ng-repeat="error in $ctrl.formErrors.list track by $index"
                ng-bind="$root.t(error)"
            ></p>
        </div>
    `,
};
