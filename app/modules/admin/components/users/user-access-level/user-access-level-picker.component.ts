import { find } from "lodash";
import OrgGroupStoreNew from "oneServices/org-group.store";
import { RolesService } from "adminService/roles.service";
import { AccessLevelPickerActions } from "constants/access-level-picker-actions.constant";

import { IRole, IRoleDetails } from "interfaces/role.interface";
import { IStringMap } from "interfaces/generic.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";
import { IOrganization } from "interfaces/organization.interface";
import { IActionIcon } from "interfaces/action-icons.interface";
import { IAccessLevel } from "interfaces/user-access-level.interface";

class UserAccessLevelPickerController implements ng.IComponentController {
    public accessLevel: IAccessLevel;
    public isDisabled: boolean;
    public canDelete: boolean;
    public handleAction: Function;
    public actions: IActionIcon[];

    public AccessLevelPickerActions = AccessLevelPickerActions;
    public orgTable: IStringMap<IOrganization> = this.OrgGroupStore.orgTable;
    public tenantOrgTable: IStringMap<IOrganization> = this.OrgGroupStore.tenantOrgTable;
    public orgList: IOrganization[] = this.OrgGroupStore.orgList;
    public roles: IRole[] = [];
    public role: IRole;

    public isInitializing = false;

    public static $inject: string[] = ["OrgGroupStoreNew", "RolesService"];

    constructor(
        public readonly OrgGroupStore: OrgGroupStoreNew,
        public readonly RolesService: RolesService,
    ) {}

    public $onInit(): void {
        if (this.accessLevel.organizationId) {
            this.isInitializing = true;
            if (this.orgTable.hasOwnProperty(this.accessLevel.organizationId)) {
                this.getRolesByOrg(this.accessLevel.organizationId).then(() => {
                    this.isInitializing = false;
                });
            } else if (this.accessLevel.roleId) {
                this.getRole(this.accessLevel.roleId).then(() => {
                    this.isInitializing = false;
                });
            } else {
                this.isInitializing = false;
            }
        }
    }

    public handleFieldAction(type: string, payload?: any): void {
        switch (type) {
            case AccessLevelPickerActions.SET_ORGANIZATION:
                const org = payload.org as IOrganization;
                this.accessLevel.organizationId = org.id;
                this.getRolesByOrg(this.accessLevel.organizationId);
                break;
            case AccessLevelPickerActions.SET_ROLE:
                this.accessLevel.roleId = payload;
                break;
        }
        this.handleAction({ type, payload: this.accessLevel });
    }

    public getRolesByOrg(organizationId: string): ng.IPromise<void> {
        return this.RolesService.listRoles({ isAssignable: true, organizationId }).then((response: IProtocolResponse<IRole[]>): void => {
            this.roles = response.result && response.data ? this.RolesService.translateRoleNamesAndDescription(response.data) : [];
        });
    }

    public getRole(roleId: string): ng.IPromise<void> {
        return this.RolesService.getRoleDetails(roleId).then((response: IProtocolResponse<IRoleDetails>): void => {
            this.role = response.result && response.data ? this.RolesService.translateRoleProperties(response.data) : null;
        });
    }

    public getRoleProperty(prop: string, roleId?: string): string {
        if (!roleId) return "";
        const role: IRole = find(
            this.roles,
            (role: IRole): boolean => role.Id === roleId,
        );
        return role && role[prop] ? role[prop] : "";
    }

}

export const userAccessLevelPicker: ng.IComponentOptions = {
    bindings: {
        accessLevel: "<",
        isDisabled: "<?",
        canDelete: "<?",
        handleAction: "&",
        fieldError: "<?",
        roles: "<",
    },
    controller: UserAccessLevelPickerController,
    template: `
        <loading ng-if="$ctrl.isInitializing" show-text="false"></loading>
        <div ng-if="!$ctrl.isInitializing" class="row-space-between align-center">
            <div class="row-space-between user-access-level-picker__fields">
                <one-label-content
                    name="{{::$root.t('Organization')}}"
                    class="block margin-top-half padding-bottom-1 user-access-level-picker__field"
                    is-required="true"
                    >
                    <one-org-input
                        identifier="UserAccessLevelOrganization"
                        ng-if="!$ctrl.isDisabled"
                        class="block"
                        ng-class="{'border-has-error': $ctrl.fieldError && $ctrl.fieldError.org}"
                        org-list="$ctrl.orgList"
                        selected-org="$ctrl.orgTable[$ctrl.accessLevel.organizationId]"
                        action-callback="$ctrl.handleFieldAction($ctrl.AccessLevelPickerActions.SET_ORGANIZATION, payload);"
                        placeholder="{{::$root.t('SelectOrganizationGroup')}}"
                    ></one-org-input>
                    <p
                        ng-if="$ctrl.isDisabled && $ctrl.accessLevel.organizationId"
                        ng-bind="$ctrl.tenantOrgTable[$ctrl.accessLevel.organizationId].name"
                    ></p>
                    <p
                        ng-if="$ctrl.fieldError && $ctrl.fieldError.org"
                        class="text-error margin-all-0 margin-top-half"
                        ng-bind="$root.t($ctrl.fieldError.org)"
                    ></p>
                </one-label-content>
                <one-label-content
                    name="{{::$root.t('Role')}}"
                    class="block margin-top-half padding-bottom-1 user-access-level-picker__field"
                    is-required="true"
                    >
                    <one-select
                        ng-if="!$ctrl.isDisabled"
                        name="UserAccessLevelRole"
                        is-required="true"
                        class="full-width"
                        modifier-class="{{$ctrl.fieldError && $ctrl.fieldError.role ? 'border-has-error' : ''}}"
                        model="$ctrl.accessLevel.roleId"
                        on-change="$ctrl.handleFieldAction($ctrl.AccessLevelPickerActions.SET_ROLE, selection);"
                        options="$ctrl.roles"
                        label-key="Name"
                        value-key="Id"
                        placeholder-text="{{::$root.t('SelectRole')}}"
                    ></one-select>
                    <p
                        ng-if="$ctrl.isDisabled && $ctrl.accessLevel.roleId && $ctrl.role"
                        ng-bind="$ctrl.role.Name"
                    ></p>
                    <p
                        ng-if="$ctrl.fieldError && $ctrl.fieldError.role"
                        class="text-error margin-all-0 margin-top-half"
                        ng-bind="$root.t($ctrl.fieldError.role)"
                    ></p>
                </one-label-content>
            </div>
            <div class="row-horizontal-vertical-center margin-top-2 user-access-level-picker__actions">
                <one-button
                    identifier="UserAccessLevelDeleteBtn"
                    ng-if="$ctrl.canDelete && !$ctrl.isDisabled"
                    type="danger"
                    icon="ot ot-minus"
                    is-circular="true"
                    button-class="user-access-level-picker__btn"
                    button-click="$ctrl.handleFieldAction($ctrl.AccessLevelPickerActions.DELETE_ACCESS, $ctrl.accessLevel);"
                ></one-button>
            </div>
        </div>
        <div ng-if="!$ctrl.isInitializing">
            <p class="user-access-level-picker__role-description">
                <span class="text-bold" ng-bind="$root.t('RoleDescription')+':'"></span>
                <span ng-if="!$ctrl.accessLevel.roleId && !$ctrl.isDisabled" ng-bind="$root.t('SelectRole')"></span>
                <span ng-if="$ctrl.accessLevel.roleId && $ctrl.roles.length" ng-bind="$ctrl.getRoleProperty('Description', $ctrl.accessLevel.roleId)"></span>
                <span ng-if="$ctrl.accessLevel.roleId && $ctrl.role" ng-bind="$ctrl.role.Description"></span>
            </p>
            <div ng-if="!$ctrl.isDisabled" class="row-vertical-center">
                <one-button
                    identifier="UserAccessLevelDefaultBtn"
                    type="radio"
                    icon="ot ot-circle"
                    is-selected="$ctrl.accessLevel.isDefault"
                    button-click="$ctrl.handleFieldAction($ctrl.AccessLevelPickerActions.SET_DEFAULT);"
                ></one-button>
                <span class="margin-left-half" ng-bind="$root.t('DefaultRole')"></span>
            </div>
            <one-tag
                ng-if="$ctrl.isDisabled && $ctrl.accessLevel.isDefault"
                plain-text="{{$root.t('DefaultRole')}}"
                label-class="one-tag--rounded one-tag--blue"
            ></one-tag>
        </div>
    `,
};
