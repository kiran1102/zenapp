// 3rd Party
import Utilities from "Utilities";
import {
    each,
    every,
    filter,
    find,
    includes,
    isNil,
    isUndefined,
    keys,
    map,
    upperFirst,
} from "lodash";

// Services
import { UserActionService } from "oneServices/actions/user-action.service";
import OrgGroupStore from "oneServices/org-group.store";
import { RolesService } from "adminService/roles.service";
import { Permissions } from "modules/shared/services/helper/permissions.service";
import { Settings } from "sharedServices/settings.service";

// Constants
import { GUID } from "constants/regex.constant";
import { FormDataTypes, FormInputTypes } from "constants/form-types.constant";

// Interfaces
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import { IPermissions } from "modules/shared/interfaces/permissions.interface";
import { IProtocolPacket, IProtocolResponse } from "interfaces/protocol.interface";
import { IStringMap } from "interfaces/generic.interface";
import { IUser, IUserManageContract } from "interfaces/user.interface";
import { IAccessLevel } from "interfaces/user-access-level.interface";
import { IOrganization } from "interfaces/organization.interface";
import { IRole } from "interfaces/role.interface";
import { ISamlSettings } from "interfaces/setting.interface";
import { IFormFieldInput, IFormFieldErrors } from "interfaces/form-field.interface";

enum UserFormTypes {
    Create = 10,
    Edit = 20,
}

interface IUserFieldOption {
    identifier: string;
    name: string;
    value: string|number|boolean;
}

interface IUserFormField extends IFormFieldInput {
    allowedPermissions?: string[];
    allowedTypes: number[];
    canShow?: () => boolean;
    descriptionKey?: string;
    disallowedPermissions?: string[];
    inputDescriptionKey?: string;
    inputKey?: string;
    inputValue?: any;
    labelKey?: string;
    minDate?: Date;
    modifier?: string;
    options?: IUserFieldOption[] | IOrganization[] | IRole[];
    readOnly?: boolean;
    valueKey?: string;
}

class UserManageCtrl implements ng.IComponentController {
    static $inject: string[] = [
        "$rootScope",
        "$state",
        "$q",
        "$timeout",
        "Permissions",
        "UserActionService",
        "OrgGroupStoreNew",
        "RolesService",
        "Settings",
    ];

    private FormDataTypes = FormDataTypes;
    private FormInputTypes = FormInputTypes;

    private ready = false;
    private saving = false;
    private userFormConfig: IUserFormField[] = [];
    private userForm: ng.IFormController;
    private formIsValid = false;
    private rolesLoadingCount = 0;

    private userId: string = this.$state.params.id;
    private user: IUser;

    private orgList: IOrganization[] = this.orgGroupStore.orgList;
    private tenantOrgTable: IStringMap<IOrganization> = this.orgGroupStore.tenantOrgTable;
    private roles: IRole[];

    private adEnabled = false;
    private isInternal = true;

    private viewPermissions: IPermissions = {
        save: this.userId ? this.permissions.canShow("UsersEdit") : this.permissions.canShow("UsersAdd"),
        settingsSSO: this.permissions.canShow("SettingsSSO"),
        multiRole: this.permissions.canShow("AdminUsersMultiRole"),
    };

    private translations = {
        user: this.$rootScope.t("User"),
        createUser: this.$rootScope.t("CreateUser"),
        editUser: this.$rootScope.t("EditUser"),
        create: this.$rootScope.t("Create"),
        edit: this.$rootScope.t("Edit"),
        emailRegistrationComplete: this.$rootScope.t("EmailRegistrationComplete"),
        cancel: this.$rootScope.t("Cancel"),
        createAnotherUser: this.$rootScope.t("CreateAnotherUser"),
        sendInvite: this.$rootScope.t("SendInvite"),
        save: this.$rootScope.t("Save"),
    };

    private headerTitle: string = this.userId ? this.translations.editUser : this.translations.createUser;
    private breadcrumbText: string = this.userId ? this.translations.edit : this.translations.create;
    private saveText: string = this.userId ? this.translations.save : this.translations.sendInvite;

    constructor(
        private $rootScope: IExtendedRootScopeService,
        private $state: ng.ui.IStateService,
        private $q: ng.IQService,
        private $timeout: ng.ITimeoutService,
        private permissions: Permissions,
        private userActionService: UserActionService,
        private orgGroupStore: OrgGroupStore,
        private rolesService: RolesService,
        private settings: Settings,
    ) {}

    public $onInit(): void {
        this.ready = false;
        const promiseArray: Array<ng.IPromise<IProtocolPacket>> = [];
        if (this.userId) {
            promiseArray.push(this.userActionService.getUser(this.userId, true).then((response: IProtocolResponse<IUser>): IProtocolResponse<IUser> => {
                this.user = response.result ? response.data : null;
                return response;
            }));
            if (this.viewPermissions.settingsSSO) {
                promiseArray.push(this.getDirectorySettings());
            }
        }
        this.$q.all(promiseArray).then((responses: IProtocolPacket[]): void => {
            const formType: number = this.userId ? UserFormTypes.Edit : UserFormTypes.Create;
            const rolesOrgGroupId = !this.viewPermissions.multiRole && this.user && this.user.RoleId ? this.user.OrgGroupId : "";
            this.getRoles(this.user && this.user.IsReadOnly ? "" : rolesOrgGroupId).then(() => {
                this.userFormConfig = this.retrieveAndApplyUserConfigToFormFields(formType);
                this.ready = true;
            });
        });
    }

    private retrieveAndApplyUserConfigToFormFields(formType: number): IUserFormField[] {
        const formConfig = this.getUserFormConfiguration(formType, this.user);
        return this.user ? this.applyValuesToFormFields(formConfig, this.user) : formConfig;
    }

    private getRoles(organizationId?: string): ng.IPromise<IProtocolPacket> {
        this.rolesLoadingCount++;
        return this.rolesService.listRoles({organizationId, isAssignable: true}).then((response: IProtocolResponse<IRole[]>): IProtocolResponse<IRole[]> => {
            if (response.result) {
                this.roles = this.rolesService.translateRoleNamesAndDescription(response.data);
            }
            this.rolesLoadingCount--;
            return response;
        });
    }

    private getDirectorySettings(): ng.IPromise<IProtocolResponse<ISamlSettings>> {
        return this.settings.getDirectory().then((response: IProtocolResponse<ISamlSettings>): IProtocolResponse<ISamlSettings> => {
            this.adEnabled = response.result && response.data.IsEnabled;
            return response;
        });
    }

    private getUserFormConfiguration(formType: number, user?: IUser): IUserFormField[] {
        const config: IUserFormField[] = [
            {
                allowedTypes: [UserFormTypes.Create, UserFormTypes.Edit],
                dataType: FormDataTypes.TEXT,
                inputType: FormInputTypes.EMAIL,
                fieldKey: "email",
                fieldNameKey: "EmailAddress",
                placeholderKey: "Email",
                validationKey: "ErrorEnterValidEmailAddress",
                required: true,
                readOnly: formType === UserFormTypes.Edit,
            },
            {
                allowedTypes: [UserFormTypes.Create, UserFormTypes.Edit],
                dataType: FormDataTypes.TEXT,
                inputType: FormInputTypes.TEXT,
                fieldKey: "firstName",
                fieldNameKey: "FirstName",
                placeholderKey: "FirstName",
                validationKey: "ErrorEnterValidFirstName",
                required: true,
                modifier: "user-manage__input--half",
            },
            {
                allowedTypes: [UserFormTypes.Create, UserFormTypes.Edit],
                dataType: FormDataTypes.TEXT,
                inputType: FormInputTypes.TEXT,
                fieldKey: "lastName",
                fieldNameKey: "LastName",
                placeholderKey: "LastName",
                validationKey: "ErrorEnterValidLastName",
                required: true,
                modifier: "user-manage__input--half",
            },
            {
                allowedTypes: [UserFormTypes.Create, UserFormTypes.Edit],
                disallowedPermissions: ["AdminUsersMultiRole"],
                dataType: FormDataTypes.ORGANIZATION,
                fieldKey: "orgGroup",
                inputValue: this.orgList[0],
                fieldNameKey: "Organization",
                placeholderKey: "SelectOrganizationGroup",
                validationKey: "ErrorSelectUserOrganizationGroup",
                required: true,
                options: this.orgList,
            },
            {
                allowedTypes: [UserFormTypes.Create, UserFormTypes.Edit],
                disallowedPermissions: ["AdminUsersMultiRole"],
                dataType: FormDataTypes.SELECT,
                fieldKey: "role",
                fieldNameKey: "Role",
                placeholderKey: "SelectRole",
                validationKey: "ErrorSelectUserRole",
                labelKey: "Name",
                valueKey: "Id",
                options: this.roles,
                required: true,
                inputDescriptionKey: "Description",
            },
            {
                allowedTypes: [UserFormTypes.Create, UserFormTypes.Edit],
                allowedPermissions: ["AdminUsersMultiRole"],
                dataType: FormDataTypes.ACCESS_LEVEL,
                inputKey: "AccessLevels",
                fieldKey: "AccessLevels",
                fieldNameKey: "UserRoles",
                fieldClass: "border-top margin-top-1",
            },
            {
                allowedTypes: [UserFormTypes.Create, UserFormTypes.Edit],
                dataType: FormDataTypes.BUTTON_SWITCH,
                fieldKey: "isInternal",
                inputValue: true,
                fieldNameKey: "UserType",
                validationKey: "ErrorSelectUserRole",
                labelKey: "Name",
                valueKey: "Id",
                options: [
                    {
                        identifier: "internalUserCreateSelect",
                        name: "InternalUser",
                        value: true,
                    },
                    {
                        identifier: "externalUserCreateSelect",
                        name: "ExternalUser",
                        value: false,
                    },
                ],
                required: true,
                descriptionKey: "SettingUserToExternalDescription",
            },
            {
                allowedTypes: [UserFormTypes.Create, UserFormTypes.Edit],
                dataType: FormDataTypes.DATE,
                fieldKey: "expirationDT",
                fieldNameKey: "ExternalUserExpirationDateOptional",
                placeholderKey: "EnterExpirationDate",
                minDate: new Date(),
                required: false,
                canShow: (): boolean => {
                    const internalField: IUserFormField = find(this.userFormConfig, (field: IUserFormField): boolean => {
                        return field.fieldKey === "isInternal";
                    });
                    return !internalField.inputValue;
                },
            },
        ];
        return filter(config, (field: IUserFormField): boolean => {
            return includes(field.allowedTypes, formType)
                && every(field.allowedPermissions, (permission: string): boolean => this.permissions.canShow(permission))
                && every(field.disallowedPermissions, (permission: string): boolean => !this.permissions.canShow(permission));
        });
    }

    private getDescription(field: IUserFormField): string {
        if (!field || !field.valueKey || !field.inputDescriptionKey || !field.inputValue || !field.options) return "";
        const item = find(field.options, (opt: any) => opt[field.valueKey] === field.inputValue);
        if (!item) return "";
        return item[field.inputDescriptionKey] || "";
    }

    private convertFormFieldsToMap(formFields: IUserFormField[]): IStringMap<any> {
        const userMap: IStringMap<any> = {};
        each(formFields, (field: IUserFormField): void => {
            let inputValue: any = field.inputValue;
            if (field.fieldKey === "AccessLevels" && inputValue && inputValue.length) {
                inputValue = map(inputValue, (level: IAccessLevel): IAccessLevel => {
                    if (!Utilities.matchRegex(level.id, GUID)) {
                        level.id = "";
                    }
                    if (!level.isDefault) {
                        level.isDefault = false;
                    }
                    return level;
                });
            }
            userMap[field.fieldKey] = inputValue;
        });
        return userMap;
    }

    private applyValuesToFormFields(formFields: IUserFormField[], user: IUser): IUserFormField[] {
        const disableAllFields: boolean = !user.CanEdit || user.IsReadOnly;
        each(formFields, (field: IUserFormField): void => {
            field.readOnly = disableAllFields || field.readOnly;
            switch (field.fieldKey) {
                case "orgGroup":
                    field.inputValue = this.orgGroupStore.tenantOrgTable[user.OrgGroupId] || null;
                    break;
                case "role":
                    const selectedRole = find(this.roles, (role: IRole): boolean => {
                        return role.Id === user.RoleId;
                    });
                    field.inputValue = selectedRole ? selectedRole.Id : null;
                    break;
                default:
                    const userValueKey: string = field.inputKey || upperFirst(field.fieldKey);
                    field.inputValue = user[userValueKey];
                    break;
            }
        });
        return formFields;
    }

    private setForm(userForm: ng.IFormController): void {
        this.userForm = userForm;
        this.validateForm();
    }

    private setValue(field: IUserFormField, value: any): void {
        if (field.dataType === FormDataTypes.ORGANIZATION) {
            value = value.org;
            this.getRoles(value.id).then(() => {
                this.userFormConfig = map(this.userFormConfig, (formField: IUserFormField) => {
                    if (formField.fieldKey === "role") {
                        formField.options = this.roles;
                    }
                    return formField;
                });
                this.validateForm();
            });
        }
        field.inputValue = value;
        this.validateForm();
    }

    private validateForm() {
        if (!this.userForm) return;
        // Wait for all elements to finish rendering or destroying. Then run form validations.
        this.$timeout(() => {
            let hasInvalidFields: boolean = Boolean(this.rolesLoadingCount);
            this.userFormConfig = map(this.userFormConfig, (field: IUserFormField): IUserFormField => {
                field = this.validateFormField(field);
                const hasErrors: boolean = field.errors && field.errors.hasErrors;
                if (hasErrors) {
                    hasInvalidFields = true;
                }
                return field;
            });
            this.formIsValid = this.userForm.$valid && !hasInvalidFields;
        });
    }

    private validateFormField(field: IUserFormField): IUserFormField {
        const errors: IFormFieldErrors = {
            hasErrors: false,
            list: [],
            map: {},
        };
        if (field.required && isNil(field.inputValue)) {
           errors.list.push(field.validationKey);
        }
        if (field.fieldKey === "AccessLevels") {
            let hasDefaultRole = false;
            const uniqueOrganizations: string[] = [];
            each(field.inputValue, (level: IAccessLevel): void => {
                if (level.isDefault) hasDefaultRole = true;
                const accessErrors: IStringMap<any> = {};
                if (!level.roleId) {
                    accessErrors.role = "ErrorSelectUserRole";
                }
                if (!level.organizationId) {
                    accessErrors.org = "ErrorSelectUserOrganizationGroup";
                } else if (includes(uniqueOrganizations, level.organizationId)) {
                    accessErrors.org = "ErrorOrganizationsMustBeUnique";
                } else {
                    uniqueOrganizations.push(level.organizationId);
                }
                if (keys(accessErrors).length) {
                    errors.map[level.id] = accessErrors;
                }
            });
            if (field.inputValue && !hasDefaultRole) {
                errors.list.push("ErrorSelectADefaultRole");
            }
        }
        if (errors.list.length || keys(errors.map).length) {
           errors.hasErrors = true;
        }
        field.errors = errors && errors.hasErrors ? errors : null;
        return field;
    }

    private adjustExpiration(date: Date): Date {
        date = new Date(date);
        if (!isUndefined(date)) {
            date.setHours(23);
            date.setMinutes(55);
        }
        return date;
    }

    private save(reloadOnSuccess: boolean = false): void {
        this.saving = true;
        const values: IStringMap<any> = this.convertFormFieldsToMap(this.userFormConfig);
        const adjustedExpiration: Date | null = !isNil(values.expirationDT) ? this.adjustExpiration(values.expirationDT) : null;

        const user: IUserManageContract = {
            Email: values.email,
            UserName: values.email,
            FirstName: values.firstName,
            LastName: values.lastName,
            IsInternal: values.isInternal,
            IsDirectoryUser: (values.isInternal && this.adEnabled),
            ExpirationDT: adjustedExpiration,
        };
        if (this.viewPermissions.multiRole) {
            user.AccessLevels = values.AccessLevels as IAccessLevel[];
            const defaultLevel: IAccessLevel = find(user.AccessLevels, (level: IAccessLevel): boolean => level.isDefault);
            user.OrgGroupId = defaultLevel.organizationId;
            user.RoleId = defaultLevel.roleId;
        } else {
            user.OrgGroupId = values.orgGroup ? values.orgGroup.id : this.user.OrgGroupId;
            user.RoleId = values.role || this.user.RoleId;
        }
        if (this.userId) {
            user.Id = this.userId;
        }

        this.userActionService.upsertUser(user).then((response: IProtocolPacket): void | undefined => {
            if (!response.result) {
                this.saving = false;
                return;
            }
            const userId = this.userId || response.data;
            this.userActionService.getUser(userId, true).then((res: IProtocolPacket): void | undefined => {
                if (!res.result) return;
                const route: string = reloadOnSuccess ? "." : "zen.app.pia.module.admin.users.list";
                this.$state.go(route, { time: (new Date().getTime()) });
            });
        });
    }

}

export const userManageView: ng.IComponentOptions = {
    controller: UserManageCtrl,
    template: `
        <div class="full-size column-nowrap background-white">
            <one-nav-header
                class="static-vertical"
                title="{{::$ctrl.headerTitle}}"
                breadcrumb-text="{{::$ctrl.translations.user}}"
                breadcrumb-route="zen.app.pia.module.admin.users.list"
                body="{{::$ctrl.breadcrumbText}}"
            ></one-nav-header>
            <loading class="full-size stretch-vertical" ng-if="!$ctrl.ready"></loading>
            <div ng-if="$ctrl.ready" class="stretch-vertical padding-all-3 overflow-y-auto">
                <form role="form" name="userForm" class="centered-max-50 row-wrap-space-between">
                    <span class="hidden" ng-init="$ctrl.setForm(userForm)"></span>
                    <one-label-content
                        ng-repeat="field in $ctrl.userFormConfig track by $index"
                        ng-if="!field.canShow || field.canShow()"
                        class="block margin-top-half padding-bottom-1 {{::field.modifier || 'flex-full-100'}}"
                        body-class="{{::field.fieldClass}}"
                        name="{{::$root.t(field.fieldNameKey)}}"
                        is-required="field.required"
                        >
                        <div ng-if="::field.dataType === $ctrl.FormDataTypes.TEXT">
                            <input
                                name="{{::field.fieldKey}}"
                                class="one-input full-width"
                                placeholder="{{::$root.t(field.placeholderKey)}}"
                                ng-model="field.inputValue"
                                ng-change="$ctrl.setValue(field, field.inputValue);"
                                ng-required="::field.required"
                                ng-disabled="field.readOnly"
                                type="{{::field.inputType}}"
                                ng-class="{'border-has-error': field.fieldKey && userForm[field.fieldKey].$invalid && (userForm[field.fieldKey].$dirty || userForm[field.fieldKey].$touched)}"
                                autocomplete="off"
                                ng-model-options="{ updateOn: 'default blur', debounce: { default: 250, blur: 0 } }"
                            />
                            <p
                                ng-if="field.fieldKey && userForm[field.fieldKey].$invalid && (userForm[field.fieldKey].$dirty || userForm[field.fieldKey].$touched)"
                                class="text-error margin-top-half"
                                ng-bind="$root.t(field.validationKey)"
                            ></p>
                        </div>
                        <div ng-if="::field.dataType === $ctrl.FormDataTypes.SELECT" ng-init="selectionMade = false">
                            <div class="row-vertical-center">
                                <one-select
                                    name="field.fieldKey"
                                    is-required="field.required"
                                    class="full-width"
                                    model="field.inputValue"
                                    on-change="$ctrl.setValue(field, selection); selectionMade = true;"
                                    is-disabled="field.readOnly"
                                    options="field.options"
                                    label-key="{{field.labelKey}}"
                                    value-key="{{field.valueKey}}"
                                    placeholder-text="{{::$root.t(field.placeholderKey)}}"
                                ></one-select>
                                <div
                                    class="margin-left-right-half align-center"
                                    ng-if="$ctrl.getDescription(field)"
                                    >
                                    <i
                                        class="ot ot-info-circle"
                                        uib-tooltip="{{$ctrl.getDescription(field)}}"
                                        tooltip-placement="top-right"
                                        tooltip-trigger="mouseenter"
                                    ></i>
                                </div>
                            </div>
                            <p
                                ng-if="field.required && (selectionMade && !field.inputValue)"
                                class="text-error margin-top-half"
                                ng-bind="$root.t(field.validationKey)"
                            ></p>
                        </div>
                        <div ng-if="::field.dataType === $ctrl.FormDataTypes.ORGANIZATION" ng-init="selectionMade = false">
                            <one-org-input
                                org-list="field.options"
                                selected-org="field.inputValue"
                                action-callback="$ctrl.setValue(field, payload); selectionMade = true"
                                placeholder="{{::$root.t(field.placeholderKey)}}"
                                ng-if="!field.readOnly"
                            ></one-org-input>
                            <span
                                ng-if="field.readOnly && field.inputValue"
                                ng-bind="$ctrl.tenantOrgTable[field.inputValue.id] ? $ctrl.tenantOrgTable[field.inputValue.id].name : ''"
                            ></span>
                            <p
                                ng-if="field.required && (selectionMade && !field.inputValue)"
                                class="text-error margin-top-half"
                                ng-bind="$root.t(field.validationKey)"
                            ></p>
                        </div>
                        <div ng-if="::field.dataType === $ctrl.FormDataTypes.DATE" ng-init="selectionMade = false">
                            <one-datepicker
                                date-model="field.inputValue"
                                is-required="field.required"
                                min-date="field.minDate"
                                on-change="$ctrl.setValue(field, value); selectionMade = true;"
                                is-disabled="field.readOnly"
                                placeholder="{{::$root.t(field.placeholderKey)}}"
                            ></one-datepicker>
                            <p
                                ng-if="field.required && (selectionMade && !field.inputValue)"
                                class="text-error margin-top-half"
                                ng-bind="$root.t(field.validationKey)"
                            ></p>
                        </div>
                        <div ng-if="::field.dataType === $ctrl.FormDataTypes.BUTTON_SWITCH" ng-init="selectionMade = false">
                            <div class="row-vertical-center">
                                <div class="one-button-group">
                                    <one-button
                                        ng-repeat="option in field.options track by option.identifier"
                                        identifier="{{::option.identifier}}"
                                        text="{{::$root.t(option.name)}}"
                                        button-click="$ctrl.setValue(field, option.value); selectionMade = true;"
                                        is-selected="field.inputValue === option.value"
                                        is-disabled="field.readOnly"
                                        type="select"
                                        is-rounded="true"
                                    ></one-button>
                                </div>
                                <div
                                    class="margin-left-right-half align-center"
                                    ng-if="::field.descriptionKey && field.descriptionKey"
                                    >
                                    <i
                                        class="ot ot-info-circle"
                                        uib-tooltip="{{::$root.t(field.descriptionKey)}}"
                                        tooltip-placement="top-right"
                                        tooltip-trigger="mouseenter"
                                    ></i>
                                </div>
                            </div>
                            <p
                                ng-if="field.required && (selectionMade && !field.inputValue)"
                                class="text-error margin-top-half"
                                ng-bind="$root.t(field.validationKey)"
                            ></p>
                        </div>
                        <div ng-if="::field.dataType === $ctrl.FormDataTypes.ACCESS_LEVEL">
                            <user-access-level-form
                                access-levels="field.inputValue"
                                handle-action="$ctrl.setValue(field, accessLevels);"
                                is-disabled="field.readonly"
                                roles="$ctrl.roles"
                                form-errors="field.errors"
                            ></user-access-level-form>
                        </div>
                    </one-label-content>
                    <div class="full-width" ng-if="!$ctrl.userId && $ctrl.formIsValid">
                        <div class="alert alert-success text-center" role="alert">{{::$ctrl.translations.emailRegistrationComplete}}</div>
                    </div>
                </form>
            </div>
            <div ng-if="$ctrl.ready" class="static-vertical full-width row-flex-end border-top padding-all-2">
                <downgrade-one-anchor-button
                    identifier="userCreateCancel"
                    class="margin-left-right-half"
                    anchor-route="zen.app.pia.module.admin.users.list"
                    text="{{::$ctrl.translations.cancel}}"
                    is-disabled="$ctrl.saving"
                    is-rounded="true"
                ></downgrade-one-anchor-button>
                <one-button
                    identifier="anotherUserCreateSave"
                    ng-if="!$ctrl.userId && $ctrl.viewPermissions.save"
                    class="margin-left-right-half"
                    button-click="$ctrl.save(true)"
                    enable-loading="true"
                    is-loading="$ctrl.isCreatingAnotherUser"
                    is-disabled="!$ctrl.formIsValid || $ctrl.saving"
                    text="{{::$ctrl.translations.createAnotherUser}}"
                    is-rounded="true"
                ></one-button>
                <one-button
                    identifier="userCreateSave"
                    ng-if="$ctrl.viewPermissions.save"
                    class="margin-left-right-half"
                    type="primary"
                    button-click="$ctrl.save()"
                    enable-loading="true"
                    is-loading="$ctrl.saving"
                    is-disabled="!$ctrl.formIsValid || $ctrl.saving"
                    text="{{::$ctrl.saveText}}"
                    is-rounded="true"
                ></one-button>
            </div>
        </div>
    `,
};
