// 3rd party
import { Component, Inject } from "@angular/core";
import { isFunction } from "lodash";
import { IMyDateModel } from "mydatepicker";

// Services
import { ModalService } from "sharedServices/modal.service";
import { StoreToken } from "tokens/redux-store.token";
import { TimeStamp } from "oneServices/time-stamp.service";

// Interfaces
import { IUserActivateModalData } from "interfaces/user.interface";
import { IStore, IStoreState } from "interfaces/redux.interface";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

// Redux
import { getModalData } from "oneRedux/reducers/modal.reducer";

@Component({
    selector: "external-user-enable-modal",
    templateUrl: "./external-user-enable-modal.component.html",
})
export class ExternalUserEnableModal {
    modalData: IUserActivateModalData;
    modalTitle: string;
    confirmationText: string;
    cancelButtonText: string;
    submitButtonText: string;
    iconClass: string;
    iconTemplate: string;
    isSubmitting: boolean;
    hideSubmitButton: boolean;
    confirmationRowKey: string;
    expirationDateValue: string;
    expirationDate: Date;
    format = this.timeStamp.getDatePickerDateFormat();

    constructor(
        @Inject(StoreToken) public store: IStore,
        private translatePipe: TranslatePipe,
        private modalService: ModalService,
        private readonly timeStamp: TimeStamp,
    ) {}

    ngOnInit(): void {
        this.modalData = getModalData(this.store.getState());
        this.modalTitle = this.modalData.modalTitle;
        this.iconClass = this.modalData.iconClass;
        this.iconTemplate = this.modalData.iconTemplate;
        this.confirmationText = this.modalData.confirmationText;
        this.cancelButtonText = this.translatePipe.transform("Cancel");
        this.submitButtonText = this.translatePipe.transform("Submit");
        this.confirmationRowKey = this.modalData.confirmationRowKey;
    }

    handleSubmit(): void {
        if (isFunction(this.modalData.promiseToResolve)) {
            this.isSubmitting = true;
            this.modalData.promiseToResolve(this.expirationDate || null).then((success: boolean) => {
                if (success) {
                    this.closeModal();
                }
                this.isSubmitting = false;
            });
        }
    }

    handleDateChange(value: IMyDateModel): void {
        this.expirationDateValue = value.formatted;
        this.expirationDate = value.jsdate;
    }

    closeModal(): void {
        this.modalService.closeModal();
        this.modalService.handleModalCallback();
        this.modalService.clearModalData();
    }
}
