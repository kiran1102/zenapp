// 3rd Party and Angular
import {
    Component,
    OnInit,
    Inject,
    Output,
    EventEmitter,
} from "@angular/core";
import { StateService } from "@uirouter/core";
import { forEach } from "lodash";

// Interface
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import { StoreToken } from "tokens/redux-store.token";
import { IStore } from "interfaces/redux.interface";
import { IPermissions } from "modules/shared/interfaces/permissions.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";
import { ITableColumnList } from "interfaces/smtp-settings.interface";
import { IDeleteConfirmationModalResolve } from "interfaces/delete-confirmation-modal-resolve.interface";
import { IDropdownOption } from "interfaces/one-dropdown.interface";
import {
    IRoleDetails,
    IPageableRoles,
    IRolesListParams,
    IRolesImportModalResolve,
    IRolesReplaceModalResolve,
} from "interfaces/role.interface";

// Enums
import { TableColumnTypes } from "enums/data-table.enum";
import { TemplateModes } from "enums/roles-mgmt-types.enum";
import { ExportTypes } from "enums/export-types.enum";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

// Services
import { Permissions } from "modules/shared/services/helper/permissions.service";
import { RolesService } from "adminService/roles.service";
import { ModalService } from "sharedServices/modal.service";

@Component({
    selector: "roles-mgmt-list",
    templateUrl: "./roles-mgmt-list.component.html",
})
export class RolesMgmtListComponent implements OnInit {

    @Output() search = new EventEmitter<string>();

    selectedOrg = null;
    tableColumnTypes = TableColumnTypes;
    columns: ITableColumnList[];
    ready = false;
    rolesList: IPageableRoles;
    exportInProgress = false;
    searchName = "";
    filterIsOpen = false;
    filterApplied = false;
    menuPosition: string;
    viewPermissions: IPermissions = {
        create: this.permissions.canShow("RolesCreate"),
        edit: this.permissions.canShow("RolesEdit"),
        copy: this.permissions.canShow("RolesCopy"),
        delete: this.permissions.canShow("RolesDelete"),
        export: this.permissions.canShow("RolesExport"),
        import: this.permissions.canShow("RolesImport"),
    };
    private pageSize = 20;

    constructor(
        private stateService: StateService,
        @Inject("Export") private Export: any,
        @Inject(RolesService) private rolesService: RolesService,
        @Inject(StoreToken) private store: IStore,
        private permissions: Permissions,
        private translatePipe: TranslatePipe,
        private modalService: ModalService,
    ) {}

    ngOnInit() {
        this.loadRoles();
        this.columns = [
            { columnType: this.tableColumnTypes.Link, columnName: this.translatePipe.transform("RoleName"), cellValueKey: "Name", columnIndex: true },
            { columnType: this.tableColumnTypes.Text, columnName: this.translatePipe.transform("Organization"), cellValueKey: "OrgGroupName", columnIndex: false },
            { columnType: this.tableColumnTypes.Text, columnName: this.translatePipe.transform("Description"), cellValueKey: "Description", columnIndex: false },
            { columnType: this.tableColumnTypes.Text, columnName: this.translatePipe.transform("LastModifiedBy"), cellValueKey: "lastModifiedBy", columnIndex: false },
        ];
    }

    handleNameClick(orgId: string) {
        this.stateService.go("zen.app.pia.module.admin.roles.manage", { role: orgId });
    }

    selectOrganization(value: string) {
        this.selectedOrg = value;
    }

    handleActionClicks(row: IRoleDetails, type: string): void {
        switch (type) {
            case TemplateModes.EditRole:
                this.stateService.go("zen.app.pia.module.admin.roles.manage", { role: row.Id });
                break;
            case TemplateModes.ExportRole:
                const url = `/role/export/${row.Id}`;
                this.Export.downloadFile(url, ExportTypes.XLSX, null, this.translatePipe.transform("SorryErrorWhileExportRolesXLSX")).then(
                    (): void => {
                        this.exportInProgress = false;
                    },
                );
                this.exportInProgress = true;
                break;
            case TemplateModes.CopyRole:
                this.stateService.go("zen.app.pia.module.admin.roles.manage", { base: row.Id });
                break;
            case TemplateModes.DeleteRole:
                if (!row || !row.Id) break;
                const deleteRoleModal: IDeleteConfirmationModalResolve = {
                    modalTitle: this.translatePipe.transform("DeleteRole"),
                    submitButtonText: this.translatePipe.transform("Delete"),
                    confirmationText: this.translatePipe.transform("SureToDeleteRole"),
                    cancelButtonText: this.translatePipe.transform("Cancel"),
                    promiseToResolve: (): ng.IPromise<boolean> =>
                        this.rolesService.deleteRole(row.Id).then((response: IProtocolResponse<IPageableRoles>): boolean => {
                            if (response.result) {
                                this.loadRoles();
                            }
                            return response.result;
                        }),
                };
                this.modalService.setModalData(deleteRoleModal);
                this.modalService.openModal("otDeleteConfirmationModal");
                break;
            case TemplateModes.ReplaceRole:
                if (!row || !row.Id) break;
                const replaceRoleModal: IRolesReplaceModalResolve = {
                    modalTitle: this.translatePipe.transform("ReplaceRole"),
                    orgGroupId: row.orgGroupId,
                    importName: row.Name,
                    description: row.Description || null,
                    isReplace: true,
                    closeCallback: (): void => { this.loadRoles(); },
                };
                this.modalService.setModalData(replaceRoleModal);
                this.modalService.openModal("RolesReplaceModal");
                break;
        }
    }

    goToAddRole() {
        this.stateService.go("zen.app.pia.module.admin.roles.manage");
    }

    openImportModal() {
        const modalData: IRolesImportModalResolve = {
            modalTitle: this.translatePipe.transform("ImportRole"),
            closeCallback: (): void => { this.loadRoles(); },
        };
        this.modalService.setModalData(modalData);
        this.modalService.openModal("RolesImportModal");
    }

    onToggle(filterIsOpen: boolean): void {
        this.filterIsOpen = filterIsOpen;
    }

    clearAllFiters() {
        this.onToggle(false);
        this.filterApplied = false;
        this.selectedOrg = null;
        this.loadRoles();
    }

    orgFilter() {
        this.onToggle(false);
        this.filterApplied = true;
        this.loadRoles();
    }

    onSearchName(searchText: string): void {
        this.search.emit(searchText);
        this.searchName = searchText;
        this.loadRoles();
    }

    setMenuClass(event?: MouseEvent) {
        this.menuPosition = "bottom-right";
        if ((window.innerHeight - 200) / 2 < (event.clientY - 200)) {
            this.menuPosition = "top-right";
        }
    }

    loadRoles(page: number = 0): void {
        this.ready = false;
        const params: IRolesListParams = { roleName: this.searchName, orgGroupId: this.selectedOrg, size: this.pageSize, page };
        this.rolesService.listRoles(params).then((response: IProtocolResponse<IPageableRoles>): void => {
            this.rolesList = response.data;
            forEach(this.rolesList.content, (role: IRoleDetails): IRoleDetails => {
                role.dropDownMenu = this.getEllipsisOptions(role);
                return role;
            });
            this.ready = true;
        });
    }

    private getEllipsisOptions(row: IRoleDetails): IDropdownOption[] {
        const menuOptions: IDropdownOption[] = [];
        if (this.viewPermissions.edit && row.CanEdit) {
            menuOptions.push({
                text: this.translatePipe.transform("Edit"),
                value: TemplateModes.EditRole,
            });
        }
        if (this.viewPermissions.copy) {
            menuOptions.push({
                text: this.translatePipe.transform("Copy"),
                value: TemplateModes.CopyRole,
            });
        }
        if (this.viewPermissions.delete && row.CanDelete) {
            menuOptions.push({
                text: this.translatePipe.transform("Delete"),
                value: TemplateModes.DeleteRole,
            });
        }
        if (this.viewPermissions.export) {
            menuOptions.push({
                text: this.translatePipe.transform("Export"),
                value: TemplateModes.ExportRole,
            });
        }
        if (row.CanReplace) {
            menuOptions.push({
                text: this.translatePipe.transform("Replace"),
                value: TemplateModes.ReplaceRole,
            });
        }
        return menuOptions;
    }
}
