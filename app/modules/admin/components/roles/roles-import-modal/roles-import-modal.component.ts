import {
    Component,
    OnInit,
    Inject,
    Input,
} from "@angular/core";
import { TranslatePipe } from "pipes/translate.pipe";
import { getModalData } from "oneRedux/reducers/modal.reducer";
import { isFunction } from "lodash";

// Enums and Constants
import { Regex } from "constants/regex.constant";
import {
    RolesImportFileTypes,
    FileConstraints,
} from "constants/file.constant.ts";

// Tokens
import { StoreToken } from "tokens/redux-store.token";

// Services
import { ModalService } from "sharedServices/modal.service";
import { RolesService } from "adminService/roles.service";

// Interfaces
import { IProtocolResponse } from "interfaces/protocol.interface";
import { IStore } from "interfaces/redux.interface";
import { IUploadFile } from "interfaces/assessment/assessment-attachment-modal.interface";
import { IRoleAttachmentFile } from "interfaces/role.interface";

@Component({
    selector: "roles-import-modal",
    templateUrl: "./roles-import-modal.component.html",
})
export class RolesImportModal implements OnInit {
    @Input() importName = null;
    @Input() selectedOrg = null;
    @Input() description = null;

    modalTitle: string;
    importNameHasBeenTouched = false;
    selectOrgHasBeenTouched = false;
    isSaveInProgress = false;
    attachment: any; // The attachment may be any data so kept it as any.
    attachmentErrorList: string[] = [];
    private closeCallback: () => any;
    private readonly attachmentSizeLimit = FileConstraints.FILE_SIZE_LIMIT; // 64MB
    private readonly extensionRegex = Regex.FILE_EXTENSION;

    constructor(
        @Inject(StoreToken) readonly store: IStore,
        @Inject(RolesService) private rolesService: RolesService,
        private modalService: ModalService,
        private translatePipe: TranslatePipe,
    ) { }

    ngOnInit() {
        const modalData = getModalData(this.store.getState());
        this.modalTitle = modalData.modalTitle;
        this.closeCallback = modalData.closeCallback;
    }

    closeModal() {
        this.modalService.closeModal();
        this.modalService.clearModalData();
    }

    importRole() {
        this.isSaveInProgress = true;
        const request: IRoleAttachmentFile = {
            blob: this.attachment,
            fileName: this.attachment.name,
            name: this.attachment.name,
            extension: this.attachment.name.substr(this.attachment.name.lastIndexOf(".") + 1), // .xlsx
            description: this.description,
            orgGroupId: this.selectedOrg,
            importName: this.importName,
        };
        this.rolesService.rolesImportAndReplace(request).then((response: IProtocolResponse<string>): void => {
            if (response.result) {
                if (isFunction(this.closeCallback)) {
                    this.closeCallback(true);
                }
                this.closeModal();
            }
            this.isSaveInProgress = false;
        });
    }

    selectOrganization(value: string) {
        this.selectOrgHasBeenTouched = true;
        this.selectedOrg = value;
    }

    setNameValue(value: string) {
        this.importNameHasBeenTouched = true;
        this.importName = value;
    }

    setDescription(value: string) {
        this.description = value;
    }

    handleFileSelected(files: IUploadFile[]): void {
        this.attachment = null;
        if (files && files.length > 0 && this.isValidFile(files[0])) {
            this.attachment = files[0];
        }
    }

    isValidFile(file: IUploadFile): boolean {
        this.attachmentErrorList.splice(0);
        let isValid = true;
        const fileExt = this.extensionRegex.exec(file.name)[0];

        if (fileExt !== RolesImportFileTypes.XLSX) {
            this.attachmentErrorList.push(this.translatePipe.transform("FileTypeExtensionNotValid"));
            isValid = false;
        }
        if (file.size > this.attachmentSizeLimit || file.size === 0) {
            this.attachmentErrorList.push(this.translatePipe.transform("FileSizeLimitViolation"));
            isValid = false;
        }
        if (file.name.length > 100) {
            this.attachmentErrorList.push(this.translatePipe.transform("FileNameExceedsLimitOf100Characters"));
            isValid = false;
        }
        return isValid;
    }

    columnTrackBy(column: number): number {
        return column;
    }

    removeFile() {
        this.attachment = null;
    }
}
