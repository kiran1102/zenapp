export default {
    require: {
        roleManageCtrl: "^roleManageView",
    },
    template: `
        <form class="margin-top-2 row-horizontal-vertical-center row-wrap full-width max-width-full margin-left-right-0">
            <one-label-content
                name="{{::$root.t('RoleName')}}"
                class="role-manage-form__field margin-top-half padding-left-right-half"
                is-required="true"
                >
                <input
                    ng-if="::!$ctrl.roleManageCtrl.readonly"
                    class="one-input full-width"
                    type="text"
                    placeholder="{{::$root.t('EnterRoleName')}}"
                    ng-model="$ctrl.roleManageCtrl.role.Name"
                    ng-required="true"
                />
                <span ng-if="::$ctrl.roleManageCtrl.readonly" ng-bind="$ctrl.roleManageCtrl.role.Name"></span>
            </one-label-content>
            <one-label-content
                name="{{::$root.t('Description')}}"
                class="role-manage-form__field min-width-20 margin-top-half padding-left-right-half"
                >
                <input
                    ng-if="::!$ctrl.roleManageCtrl.readonly"
                    class="one-input full-width"
                    type="text"
                    placeholder="{{::$root.t('EnterDescription')}}"
                    ng-model="$ctrl.roleManageCtrl.role.Description"
                />
                <span ng-if="::$ctrl.roleManageCtrl.readonly" ng-bind="$ctrl.roleManageCtrl.role.Description"></span>
            </one-label-content>
            <one-label-content
                name="{{::$root.t('Organization')}}"
                class="role-manage-form__field min-width-20 margin-top-half padding-left-right-half"
                is-required="true"
                >
                <one-org-input
                    ng-if="::!$ctrl.roleManageCtrl.readonly"
                    org-list="$ctrl.roleManageCtrl.orgList"
                    selected-org="$ctrl.roleManageCtrl.selectedOrg"
                    action-callback="$ctrl.roleManageCtrl.orgSelected(action, payload)"
                    placeholder="{{::$root.t('SelectOrganizationGroup')}}"
                ></one-org-input>
                <span ng-if="::$ctrl.roleManageCtrl.readonly" ng-bind="$ctrl.roleManageCtrl.selectedOrg.name"></span>
            </one-label-content>
        </form>
    `,
};
