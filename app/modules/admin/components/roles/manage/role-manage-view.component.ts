// 3rd Party
import {
    find,
    each,
    concat,
    flatten,
    map,
    filter,
} from "lodash";

// Services
import { Permissions } from "modules/shared/services/helper/permissions.service";
import { RolesService } from "adminService/roles.service";

// Interfaces
import { IStore } from "interfaces/redux.interface";
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import { IPermissions } from "sharedModules/interfaces/permissions.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";
import { IStringMap } from "interfaces/generic.interface";
import { IOrganization } from "interfaces/organization.interface";
import {
    IRoleDetails,
    IPermissionHierarchyNode,
    IRoleRequest,
} from "interfaces/role.interface";

// Redux
import { getCurrentUser } from "oneRedux/reducers/user.reducer";

class RolesMgmtListCtrl implements ng.IComponentController {
    static $inject: string[] = ["$rootScope", "$state", "$q", "Permissions", "OrgGroupStoreNew", "RolesService", "store"];

    private ready = false;
    private readonly = false;
    private saving: boolean;

    private roleId: string = this.$state.params.role;
    private baseRoleId: string = this.$state.params.base;

    private role: IRoleDetails = { Name: "", Description: "", orgGroupId: "", Id: "" };

    private viewPermissions: IPermissions = {
        create: this.permissions.canShow("RolesCreate"),
        edit: this.permissions.canShow("RolesEdit"),
        copy: this.permissions.canShow("RolesCopy"),
        save: this.roleId ? this.permissions.canShow("RolesEdit") : this.permissions.canShow("RolesCreate"),
    };

    private permissionTree: IPermissionHierarchyNode[];
    private permissionList: IPermissionHierarchyNode[];
    private permissionRoot: IPermissionHierarchyNode;
    private allPermissionsToggled = false;
    private permissionsListToggled = false;

    private saveText: string = this.roleId ? "Save" : "Create";
    private orgList: IOrganization[] = this.OrgGroupStoreNew.orgList;
    private selectedOrg: IOrganization;

    private search: IStringMap<string> = { groups: "", list: "" };
    private listHeaderText: string;

    constructor(
        private $rootScope: IExtendedRootScopeService,
        private $state: ng.ui.IStateService,
        private $q: ng.IQService,
        private permissions: Permissions,
        private OrgGroupStoreNew: any,
        private rolesService: RolesService,
        private store: IStore,
    ) { }

    public $onInit(): void {
        this.ready = false;
        const currentUser: string = getCurrentUser(this.store.getState()).RoleId;
        const promiseArray: Array<ng.IPromise<IProtocolResponse<IRoleDetails | IPermissionHierarchyNode[]>>> = [
            this.getPermissionTree(currentUser, this.baseRoleId || this.roleId).then(
                (response: IProtocolResponse<IPermissionHierarchyNode[]>): IProtocolResponse<IPermissionHierarchyNode[]> => {
                    this.permissionTree = response.result ? response.data : null;
                    return response;
                },
            ),
        ];
        if (this.baseRoleId || this.roleId) {
            const rolePromise: ng.IPromise<IProtocolResponse<IRoleDetails>> = this.getRole(
                this.baseRoleId || this.roleId,
            ).then((response: IProtocolResponse<IRoleDetails>): IProtocolResponse<IRoleDetails> => {
                if (response.result) {
                    this.role = response.data;
                    if (this.baseRoleId) {
                        this.role = {
                            ...response.data,
                            Name: `${response.data.Name} ${this.$rootScope.t("Copy")}`,
                        };
                    }
                }
                return response;
            });
            promiseArray.push(rolePromise);
        }
        this.$q.all(promiseArray).then((responses: Array<IProtocolResponse<IRoleDetails | IPermissionHierarchyNode[]>>): void => {
            this.readonly = this.roleId ? !(this.viewPermissions.edit && this.role.CanEdit) : !this.viewPermissions.create;
            if (this.role.orgGroupId) {
                // TODO: Remove below comments once we have proper test cases to validate this logic.
                this.selectedOrg = this.readonly
                    // NOTE: When in readonly, we retrieve from the tenantOrgTable in order to retrieve
                    // org details for roles that exist at a higher level than the current org contxt.
                    ? this.OrgGroupStoreNew.tenantOrgTable[this.role.orgGroupId]
                    // NOTE: When in edit or copy mode, we should only display the orgTable from the current org context.
                    // This brings into account, the user's rootOrg, and the context they are currenty in.
                    // Users are not allowed to manage orgs at a higher context than they are currently logged in at.
                    : (
                        this.OrgGroupStoreNew.orgTable[this.role.orgGroupId]
                        || this.OrgGroupStoreNew.orgTable[this.OrgGroupStoreNew.orgTable.rootId]
                        || null
                    );
                this.role = {
                    ...this.role,
                    orgGroupId: this.selectedOrg.id,
                    OrgGroupName: this.selectedOrg.name,
                };
            }

            this.setPermissionList();

            this.ready = true;
        });
    }

    private getRole(id: string): ng.IPromise<IProtocolResponse<IRoleDetails>> {
        return this.rolesService.getRoleDetails(id);
    }

    private getPermissionTree(id: string, compareId?: string): ng.IPromise<IProtocolResponse<IPermissionHierarchyNode[]>> {
        return this.rolesService.getPermissionTree(id, compareId);
    }

    private save(): void {
        this.saving = true;
        const roleRequest: IRoleRequest = {
            orgGroupId: this.role.orgGroupId,
            name: this.role.Name,
            description: this.role.Description,
            permissions: this.getPermissionsEnabledList(),
        };
        if (this.roleId) roleRequest.id = this.roleId;
        this.rolesService.upsertRole(roleRequest).then((response: IProtocolResponse<{ id?: string, message?: string }>): void => {
            if (response.result && response.data && response.data.id) {
                this.$state.go(".", { role: response.data.id, base: undefined }, { location: "replace" });
            } else if (!response.result && response.data && response.data.message) {
                // TODO : Display some error/conflict message.
            }
            this.saving = false;
        });
    }

    private orgSelected(action: string, payload: any): void {
        if (action === "ORG_SELECTED") {
            this.selectedOrg = payload.org;
            this.role.orgGroupId = payload.org.id;
            this.role.OrgGroupName = payload.org.name;
        }
    }

    private saveIsDisabled(): boolean {
        return !(this.role.Name && this.role.orgGroupId);
    }

    private setPermissionList(root?: IPermissionHierarchyNode): void {
        if (!this.permissionTree.length) return;
        if (root && this.permissionRoot && root.name === this.permissionRoot.name) root = null;
        this.permissionRoot = root ? find(this.permissionTree, (rolePerm: IPermissionHierarchyNode): boolean => rolePerm.name === root.name) : null;
        this.permissionList = this.recursivelyGetPermissionList(
            this.permissionRoot && this.permissionRoot.children ? this.permissionRoot.children : this.permissionTree,
        );
        this.listHeaderText = this.permissionRoot ? this.permissionRoot.nameTranslation : this.$rootScope.t("All");
        this.checkAllToggled();
    }

    private recursivelyGetPermissionList(tree: IPermissionHierarchyNode[]): IPermissionHierarchyNode[] {
        let arr: IPermissionHierarchyNode[] = [];
        if (tree.length) {
            each(tree, (perm: IPermissionHierarchyNode): void => {
                if (perm.children.length) {
                    arr = concat(arr, this.recursivelyGetPermissionList(perm.children));
                } else {
                    arr.push(perm);
                }
            });
        }
        return arr;
    }

    private getPermissionsEnabledList(): string[] {
        return map(
            filter(
                flatten(map(this.permissionTree, "children")),
                (permission: IPermissionHierarchyNode): boolean => permission.exists),
            (permission: IPermissionHierarchyNode): string => permission.name,
        );
    }

    private togglePermission(permission: IPermissionHierarchyNode, toggle: boolean = false): void {
        permission.exists = toggle;
        if (permission.children && permission.children.length) {
            each(permission.children, (child: IPermissionHierarchyNode): void => {
                this.togglePermission(child, toggle);
            });
        }
    }

    private toggleAllFromPermission(permission?: IPermissionHierarchyNode): void {
        if (permission) {
            this.togglePermission(permission, !permission.exists);
        } else {
            each(this.permissionTree, (child: IPermissionHierarchyNode): void => {
                this.togglePermission(child, !this.allPermissionsToggled);
            });
        }
        this.checkAllToggled();
    }

    private isPermissionToggled(permission?: IPermissionHierarchyNode): boolean {
        if (!permission) return this.allPermissionsToggled;
        let isToggled = true;
        if (permission.children && permission.children.length) {
            let childrenToggled = 0;
            each(permission.children, (child: IPermissionHierarchyNode): void => {
                if (this.isPermissionToggled(child)) {
                    childrenToggled++;
                }
            });
            isToggled = permission.children.length === childrenToggled;
            permission.exists = isToggled;
        } else {
            isToggled = permission.exists;
        }
        return isToggled;
    }

    private checkAllToggled(): void {
        this.allPermissionsToggled = false;
        let groupsToggled = 0;
        each(this.permissionTree, (permission: IPermissionHierarchyNode): void => {
            const isToggled: boolean = this.isPermissionToggled(permission);
            if (this.permissionRoot && this.permissionRoot === permission) {
                this.permissionsListToggled = isToggled;
            }
            if (isToggled) {
                groupsToggled++;
            }
        });
        this.allPermissionsToggled = groupsToggled === this.permissionTree.length;
    }

}

export default {
    controller: RolesMgmtListCtrl,
    template: `
        <div class="full-size column-nowrap padding-all-3 background-white overflow-y-auto">
            <div class="static-vertical">
                <div class="row-space-between">
                    <h1 class="inline-block margin-all-0 text-thin" ng-bind="$root.t('Role')"></h1>
                    <div ng-if="$ctrl.ready" class="one-button-group">
                        <one-button
                            identifier="AdminRolesManageSaveRoleBtn"
                            ng-if="$ctrl.viewPermissions.save && !$ctrl.readonly"
                            type="primary"
                            is-disabled="$ctrl.saveIsDisabled() || $ctrl.saving"
                            button-click="$ctrl.save()"
                            text="{{$root.t($ctrl.saveText)}}"
                            enable-loading="true"
                            is-loading="$ctrl.saving"
                        ></one-button>
                    </div>
                </div>
                <a
                    class="text-link text-decoration-none"
                    ui-sref="zen.app.pia.module.admin.roles.list({page: null, size: null})"
                    >
                    <span class="line-height-1 media-middle ot ot-angle-left margin-right-half heading5"></span>
                    <span class="line-height-1 media-middle" ng-bind="$root.t('BackToRoles')"></span>
                </a>
            </div>
            <loading class="stretch-vertical row-horizontal-vertical-center" ng-if="!$ctrl.ready" text="{{::$root.t('Role')}}"></loading>
            <div class="stretch-vertical column-nowrap" ng-if="$ctrl.ready">
                <role-manage-form class="static-vertical"></role-manage-form>
                <role-manage-permissions class="flex-full-height"></role-manage-permissions>
            </div>
        </div>
    `,
};
