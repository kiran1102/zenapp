export default {
    require: {
        roleManageCtrl: "^roleManageView",
    },
    template: `
        <one-background-message class="stretch-vertical" ng-if="!$ctrl.roleManageCtrl.permissionTree" row1="{{::$root.t('RoleDetailsNotAvailable')}}"></one-background-message>
        <div ng-if="$ctrl.roleManageCtrl.permissionTree" class="flex responsive-wrap stretch-vertical padding-left-right-half">
            <role-permissions-groups
                ng-if="$ctrl.roleManageCtrl.permissionTree.length > 1"
                class="role-manage-permissions__groups margin-top-2 padding-right-3"
            ></role-permissions-groups>
            <role-permissions-list
                ng-if="$ctrl.roleManageCtrl.permissionList"
                class="role-manage-permissions__list stretch-horizontal margin-top-2 column-nowrap"
            ></role-permissions-list>
        </div>
    `,
};
