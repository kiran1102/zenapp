import { fuzzyBy } from "angular-filter";
import { IPermissionHierarchyNode } from "interfaces/role.interface";

class RolePermissionsListCtrl implements ng.IComponentController {
    static $inject: string[] = ["$filter"];

    private roleManageCtrl: ng.IController;
    private tree: IPermissionHierarchyNode[] = [];
    private searchGroups: string;

    constructor(private readonly $filter: ng.IFilterService) {}

    public $onInit(): void {
        this.onSearchGroups();
    }

    private onSearchGroups(model: string = ""): void {
        this.searchGroups = model || "";
        this.tree = this.searchGroups ? (this.$filter("fuzzyBy") as fuzzyBy)(this.roleManageCtrl.permissionTree, "nameTranslation", this.searchGroups, false) : this.roleManageCtrl.permissionTree;
    }

    private onGroupSelect(group: IPermissionHierarchyNode): void {
        this.roleManageCtrl.setPermissionList(group);
    }

    private isGroupSelected(group: IPermissionHierarchyNode): boolean {
        return group === this.roleManageCtrl.permissionRoot;
    }

    private onGroupToggle(group: IPermissionHierarchyNode): void {
        this.roleManageCtrl.toggleAllFromPermission(group);
    }

    private isGroupToggled(group: IPermissionHierarchyNode): boolean {
        return group.exists;
    }

    private isGroupDisabled(group: IPermissionHierarchyNode): boolean {
        return this.roleManageCtrl.readonly;
    }
}

export default {
    controller: RolePermissionsListCtrl,
    require: {
        roleManageCtrl: "^roleManageView",
    },
    template: `
        <div class="full-size column-nowrap border border-radius">
            <div class="role-manage-permissions__header static-vertical border-bottom-5 padding-all-1">
                <one-search-input
                    name="RolesManagePermissionSearchGroup"
                    id="RolesManagePermissionSearchGroup"
                    model="$ctrl.searchGroups"
                    placeholder="{{::$root.t('Search')}}"
                    on-search="$ctrl.onSearchGroups(model)"
                    throttle="250"
                ></one-search-input>
            </div>
            <div class="stretch-vertical overflow-y-auto ms-overflow-scroll">
                <one-tree-picklist
                    tree="$ctrl.tree"
                    label-key="nameTranslation"
                    child-key="children"
                    is-toggled="$ctrl.isGroupToggled(node)"
                    is-selected="$ctrl.isGroupSelected(node)"
                    is-disabled="$ctrl.isGroupDisabled(node)"
                    on-select="$ctrl.onGroupSelect(node)"
                    on-toggle="$ctrl.onGroupToggle(node)"
                    track-by="id"
                ></one-tree-picklist>
            </div>
        </div>
    `,
};
