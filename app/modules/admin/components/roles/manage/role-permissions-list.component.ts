import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import { ITableColumnConfig } from "interfaces/table-config.interface";

class RolePermissionsListCtrl implements ng.IComponentController {
    static $inject: string[] = ["$rootScope"];

    private roleManageCtrl: ng.IController;
    private searchList: string;

    private columnConfig: ITableColumnConfig[] = [
        {
            name: this.$rootScope.t("Permission"),
            sortable: false,
            sortKey: "nameTranslation",
            columnKey: "nameTranslation",
            style: { "text-align": "initial", "justify-content": "space-between", "max-width": "20rem" },
        },
        {
            name: this.$rootScope.t("Description"),
            sortable: false,
            sortKey: "descriptionTranslation",
            columnKey: "descriptionTranslation",
            style: { "text-align": "initial", "justify-content": "space-between", "max-width": "20rem" },
        },
    ];

    constructor(private readonly $rootScope: IExtendedRootScopeService) { }

    private onSearchList(model: string): void {
        this.searchList = model || "";
    }

}

export default {
    controller: RolePermissionsListCtrl,
    require: {
        roleManageCtrl: "^roleManageView",
    },
    template: `
        <div class="static-vertical background-grey row-space-between row-vertical-center padding-all-1">
            <span class="heading3 margin-all-0" ng-bind="$ctrl.roleManageCtrl.listHeaderText"></span>
            <one-search-input
                class="margin-left-half"
                input-class="block role-manage-permissions__list-search"
                name="RolesManagePermissionSearchList"
                id="RolesManagePermissionSearchList"
                model="$ctrl.searchList"
                placeholder="{{::$root.t('Search')}}"
                on-search="$ctrl.onSearchList(model)"
            ></one-search-input>
        </div>
        <div class="flex-full-height stretch-vertical overflow-y-auto ms-overflow-scroll border-top-bottom-2">
            <list-table class="full-width margin-bottom-2">
                <thead-tr class="table-row border-bottom">
                    <div class="table-cell text-center padding-left-right-1 role-manage-permissions__checkbox media-middle">
                        <one-checkbox
                            identifier="RoleManageListToggleAll"
                            toggle="$ctrl.roleManageCtrl.toggleAllFromPermission($ctrl.roleManageCtrl.permissionRoot)"
                            is-selected="$ctrl.roleManageCtrl.isPermissionToggled($ctrl.roleManageCtrl.permissionRoot)"
                            is-disabled="$ctrl.roleManageCtrl.readonly"
                        ></one-checkbox>
                    </div>
                    <list-table-header-cell
                        class="table-cell text-nowrap text-left role-manage-permissions__cell"
                        ng-repeat="column in $ctrl.columnConfig track by $index"
                        cell-config="column"
                        header-text-key="name"
                        align-left="true"
                    ></list-table-header-cell>
                </thead-tr>
                <tbody-tr
                    class="table-row position-relative border-bottom"
                    ng-repeat="permission in $ctrl.roleManageCtrl.permissionList | filter:$ctrl.searchList track by permission.id"
                    >
                    <div class="table-cell text-center padding-left-right-1 role-manage-permissions__checkbox">
                        <one-checkbox
                            identifier="RoleManageListTogglePermission"
                            toggle="$ctrl.roleManageCtrl.toggleAllFromPermission(permission)"
                            is-selected="permission.exists"
                            is-disabled="$ctrl.roleManageCtrl.readonly"
                        ></one-checkbox>
                    </div>
                    <list-table-row-cell
                        class="table-cell text-nowrap text-left role-manage-permissions__cell"
                        ng-repeat="column in $ctrl.columnConfig track by $index"
                        model="permission"
                        model-prop-key="{{column.columnKey}}"
                        component="{{column.component}}"
                        scroll-overflow="true"
                    ></list-table-row-cell>
                </tbody-tr>
            </list-table>
        </div>
    `,
};
