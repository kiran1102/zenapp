import {
    Component,
    OnInit,
    Inject,
    Input,
} from "@angular/core";
import { TranslatePipe } from "pipes/translate.pipe";
import { getModalData } from "oneRedux/reducers/modal.reducer";
import { isFunction } from "lodash";

// Enums and Constants
import { Regex } from "constants/regex.constant";
import {
    RolesImportFileTypes,
    FileConstraints,
} from "constants/file.constant.ts";

// Tokens
import { StoreToken } from "tokens/redux-store.token";

// Services
import { ModalService } from "sharedServices/modal.service";
import { RolesService } from "adminService/roles.service";

// Interfaces
import { IProtocolResponse } from "interfaces/protocol.interface";
import { IStore } from "interfaces/redux.interface";
import { IUploadFile } from "interfaces/assessment/assessment-attachment-modal.interface";
import { IRoleAttachmentFile } from "interfaces/role.interface";

@Component({
    selector: "roles-replace-modal",
    templateUrl: "./roles-replace-modal.component.html",
})
export class RolesReplaceModal implements OnInit {
    @Input() importName: string;
    @Input() selectedOrgId: string;
    @Input() description: string;

    modalTitle: string;
    isSaveInProgress = false;
    attachment: any; // The attachment may be any data so kept it as any.
    attachmentErrorList: string[] = [];
    replaceHeaderText: string;
    isReplace: boolean;
    private closeCallback: () => any;
    private readonly attachmentSizeLimit = FileConstraints.FILE_SIZE_LIMIT; // 64MB
    private readonly extensionRegex = Regex.FILE_EXTENSION;

    constructor(
        @Inject(StoreToken) readonly store: IStore,
        @Inject(RolesService) private rolesService: RolesService,
        private modalService: ModalService,
        private translatePipe: TranslatePipe,
    ) { }

    ngOnInit() {
        const modalData = getModalData(this.store.getState());
        this.modalTitle = modalData.modalTitle;
        this.importName = modalData.importName;
        this.selectedOrgId = modalData.orgGroupId;
        this.description = modalData.description;
        this.closeCallback = modalData.closeCallback;
        this.isReplace = true;
        this.replaceHeaderText = this.translatePipe.transform("ReplaceRoleModalHeader", { RoleName: `<b>${this.importName}</b>` });
    }

    closeModal() {
        this.modalService.closeModal();
        this.modalService.clearModalData();
    }

    replaceRole() {
        this.isSaveInProgress = true;
        const request: IRoleAttachmentFile = {
            blob: this.attachment,
            fileName: this.attachment.name,
            name: this.attachment.name,
            extension: this.attachment.name.substr(this.attachment.name.lastIndexOf(".") + 1), // .xlsx
            description: this.description,
            orgGroupId: this.selectedOrgId,
            importName: this.importName,
            isReplace: this.isReplace,
        };
        this.rolesService.rolesImportAndReplace(request).then((response: IProtocolResponse<string>): void => {
            if (response.result) {
                if (isFunction(this.closeCallback)) {
                    this.closeCallback(true);
                }
                this.closeModal();
            }
            this.isSaveInProgress = false;
        });
    }

    handleFileSelected(files: IUploadFile[]): void {
        this.attachment = null;
        if (files && files.length > 0 && this.isValidFile(files[0])) {
            this.attachment = files[0];
        }
    }

    isValidFile(file: IUploadFile): boolean {
        this.attachmentErrorList.splice(0);
        let isValid = true;
        const fileExt = this.extensionRegex.exec(file.name)[0];

        if (fileExt !== RolesImportFileTypes.XLSX) {
            this.attachmentErrorList.push(this.translatePipe.transform("FileTypeExtensionNotValid"));
            isValid = false;
        }
        if (file.size > this.attachmentSizeLimit || file.size === 0) {
            this.attachmentErrorList.push(this.translatePipe.transform("FileSizeLimitViolation"));
            isValid = false;
        }
        if (file.name.length > 100) {
            this.attachmentErrorList.push(this.translatePipe.transform("FileNameExceedsLimitOf100Characters"));
            isValid = false;
        }
        return isValid;
    }

    columnTrackBy(column: number): number {
        return column;
    }

    removeFile() {
        this.attachment = null;
    }
}
