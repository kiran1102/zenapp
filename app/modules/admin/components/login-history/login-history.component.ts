import {
    Inject,
    Component,
    OnInit,
    Output,
    EventEmitter,
} from "@angular/core";

// Interface
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import { StoreToken } from "tokens/redux-store.token";
import { IStore } from "interfaces/redux.interface";
import { ILoginHistoryData } from "interfaces/login-history.interface";
import { IProtocolPacket } from "interfaces/protocol.interface";
import { ITableColumnList } from "interfaces/smtp-settings.interface";

// Enums
import { ExportTypes } from "enums/export-types.enum";
import { TableColumnTypes } from "enums/data-table.enum";

// Services
import { TimeStamp } from "oneServices/time-stamp.service";
import { LoginHistoryService } from "adminService/login-history.service";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

@Component({
    selector: "login-history",
    templateUrl: "./login-history.component.html",
})
export class LoginHistoryComponent implements OnInit {

    @Output() search = new EventEmitter<string>();

    ready = false;
    searchList: string;
    tableData: ILoginHistoryData;
    pageSize = 10;
    exportInProgress = false;
    currentTimeFormat: string;
    currentDateFormat: string;
    tableColumnTypes = TableColumnTypes;
    columns: ITableColumnList[];

    constructor(
        @Inject("Export") readonly Export: any,
        @Inject(StoreToken) public store: IStore,
        private timeStamp: TimeStamp,
        private loginHistoryService: LoginHistoryService,
        private translatePipe: TranslatePipe,
    ) { }

    ngOnInit() {
        this.columns = [
            { columnType: this.tableColumnTypes.Text, columnName: this.translatePipe.transform("Username"), cellValueKey: "userName", columnIndex: false },
            { columnType: this.tableColumnTypes.Text, columnName: this.translatePipe.transform("IPAddress"), cellValueKey: "ipAddress", columnIndex: false },
            { columnType: this.tableColumnTypes.Text, columnName: this.translatePipe.transform("UserAgent"), cellValueKey: "userAgentString", columnIndex: false },
            { columnType: this.tableColumnTypes.Date, columnName: this.translatePipe.transform("Timestamp"), cellValueKey: "createDT", columnIndex: false },
            { columnType: this.tableColumnTypes.Text, columnName: this.translatePipe.transform("Status"), cellValueKey: "status", columnIndex: false },
        ];
        this.loadLoginUsers();
        this.currentDateFormat = this.timeStamp.currentDateFormatting;
        this.currentTimeFormat = this.timeStamp.currentTimeFormatting;
    }

    exportUsers(): void {
        if (this.timeStamp.currentDateFormatting === " ") {
            this.currentDateFormat = "DND";
        }
        if (this.timeStamp.currentTimeFormatting === " ") {
            this.currentTimeFormat = "DND";
        }
        const url: string =
            this.currentTimeFormat === null && this.currentDateFormat === null
                ? `/Loginhistory/DownloadLoginHistoryReport`
                : `/Loginhistory/DownloadLoginHistoryReport?dateFormat=${this.currentDateFormat}&timeFormat=${this.currentTimeFormat}`;
        this.Export.downloadFile(url, ExportTypes.XLSX, this.translatePipe.transform("LoginHistory"), this.translatePipe.transform("ErrorCreatingUsersReportXLSX")).then(
            (): void => {
                this.exportInProgress = false;
            },
        );
        this.exportInProgress = true;
    }

    public onSearchList(searchText: string): void {
        this.search.emit(searchText);
        this.searchList = searchText;
        this.loadLoginUsers();
    }

    private loadLoginUsers(page: number = 0): void {
        this.ready = false;
        const promise: ng.IPromise<IProtocolPacket> = this.searchList
            ? this.loginHistoryService.getSearchList(page, this.pageSize, this.searchList)
            : this.loginHistoryService.getListofLoginUsers(page, this.pageSize);
        promise.then((response: IProtocolPacket): void => {
            this.tableData = response.data;
            this.ready = true;
        });
    }
}
