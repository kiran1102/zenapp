import {
    isArray,
    map,
    omit,
    has,
    isUndefined,
} from "lodash";
import Utilities from "Utilities";
import OrgGroupStore from "oneServices/org-group.store";
import { ProtocolService } from "modules/core/services/protocol.service";
import { NotificationService } from "modules/shared/services/provider/notification.service";

import { EmptyGuid } from "constants/empty-guid.constant";
import { UserRoles } from "constants/user-roles.constant";

import {
    IProtocolConfig,
    IProtocolMessages,
    IProtocolPacket,
    IProtocolResponse,
} from "interfaces/protocol.interface";
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import { IStringMap } from "interfaces/generic.interface";
import { IOrganization } from "interfaces/organization.interface";
import {
    IRole,
    IRoleDetails,
    IRoleRequest,
    IPermissionHierarchyNode,
    IRolesListParams,
    IPageableRoles,
    IRoleAttachmentFile,
} from "interfaces/role.interface";
import {
    IPageable,
    IPageableMeta,
} from "interfaces/pagination.interface";
import { PermissionGroupNode, PermissionNode } from "modules/admin/classes/permission.class";

export class RolesService {
    static $inject: string[] = ["$rootScope", "OneProtocol", "OrgGroupStoreNew", "NotificationService"];

    constructor(
        private $rootScope: IExtendedRootScopeService,
        private OneProtocol: ProtocolService,
        private OrgGroupStoreNew: OrgGroupStore,
        private notificationService: NotificationService,
    ) {}

    public listRoles(params: IRolesListParams): ng.IPromise<IProtocolPacket> {
        let config: IProtocolConfig;
        if (params.orgGroupId && params.roleName) {
            params = {
                roleName: params.roleName,
                orgGroupId: params.orgGroupId,
                page: params.page,
                size: params.size,
            };
        } else if (params.orgGroupId && !params.roleName) {
            params = {
                roleName: "",
                orgGroupId: params.orgGroupId,
                page: params.page,
                size: params.size,
            };
        } else if (params.roleName && !params.orgGroupId) {
            params = {
                roleName: params.roleName,
                page: params.page,
                size: params.size,
            };
        }
        config = this.OneProtocol.config("GET", "/role", params);
        const messages: IProtocolMessages = { Error: { custom: this.$rootScope.t("ErrorRetrievingRoles") } };
        return this.OneProtocol.http(config, messages).then((response: IProtocolPacket): IProtocolPacket => {
            if (params && !isUndefined(params.page) && response.result) {
                response.data = this.normalizeRolePageable(response.data);
            }
            return response;
        });
    }

    public getRoleDetails(roleId: string): ng.IPromise<IProtocolPacket> {
        const config: IProtocolConfig = this.OneProtocol.config("GET", `/role/get/${roleId}`);
        const messages: IProtocolMessages = { Error: { custom: this.$rootScope.t("ErrorRetrievingRole") } };
        return this.OneProtocol.http(config, messages).then((response: IProtocolPacket): IProtocolPacket => {
            if (response.result) {
                response.data = this.normalizeRole(response.data);
            }
            return response;
        });
    }

    public getPermissionTree(roleId: string, comparisonRoleId?: string): ng.IPromise<IProtocolPacket> {
        const params = comparisonRoleId ? { thisRoleGuid: roleId, thatRoleGuid: comparisonRoleId } : { roleGuid: roleId };
        const config: IProtocolConfig = this.OneProtocol.config("GET", `/role/permissiontree`, params);
        const messages: IProtocolMessages = { Error: { custom: this.$rootScope.t("ErrorRetrievingRole") } };
        return this.OneProtocol.http(config, messages).then((response: IProtocolPacket): IProtocolPacket => {
            if (response.result) {
                response.data = this.normalizePermissionTree(response.data);
            }
            return response;
        });
    }

    public upsertRole(role: IRoleRequest): ng.IPromise<IProtocolPacket> {
        const method: string = role.id ? "PUT" : "POST";
        const url = `/role${role.id ? "/put" : "/post"}`;
        const customErrorKey: string = role.id ? "ErrorUpdatingRole" : "ErrorCreatingRole";
        const customSuccessKey: string = role.id ? "UpdatedRoleSuccessfully" : "CreatedRoleSuccessfully";
        const config: IProtocolConfig = this.OneProtocol.config(method, url, null, role);
        const messages: IProtocolMessages = { Success: { custom: this.$rootScope.t(customSuccessKey)} };
        return this.OneProtocol.http(config, messages);
    }

    public deleteRole(roleId: string): ng.IPromise<IProtocolPacket> {
        const config: IProtocolConfig = this.OneProtocol.config("DELETE", `/role/delete/${roleId}`);
        const messages: IProtocolMessages = {
            Success: { custom: this.$rootScope.t("RoleDeletedSuccessfully") },
            Error: { custom: this.$rootScope.t("ErrorDeletingRole") },
        };
        return this.OneProtocol.http(config, messages);
    }

    public translateRoleNamesAndDescription(roles: IRole[]): IRole[] {
        if (!isArray(roles)) return roles;
        return map(roles, (role: IRole): IRole => {
            return this.translateRoleProperties(role);
        });
    }

    public translateRoleProperties(role: IRole): IRole {
        if (!role) return role;
        switch (role.Name) {
            case UserRoles.SiteAdmin:
                role.Name = this.$rootScope.t("SiteAdmin");
                role.Description = this.$rootScope.t("ASiteAdminHasAccessToApplicationSettings");
                break;
            case UserRoles.PrivacyOfficer:
                role.Name = this.$rootScope.t("PrivacyOfficer");
                role.Description = this.$rootScope.t("APrivacyOfficerHasAccessToAllLevel");
                break;
            case UserRoles.ProjectOwner:
                role.Name = this.$rootScope.t("ProjectOwner");
                role.Description = this.$rootScope.t("AProjectOwnerCanCreateAndCompleteProject");
                break;
            case UserRoles.Invited:
                role.Name = this.$rootScope.t("InvitedUser");
                role.Description = this.$rootScope.t("AnInvitedUserIsInvitedToTheApplication");
                break;
            case UserRoles.ProjectViewer:
                role.Name = this.$rootScope.t("ProjectViewer");
                role.Description = this.$rootScope.t("AProjectViewerHasViewOnlyAccess");
                break;
            case UserRoles.AssessmentsManager:
                role.Name = this.$rootScope.t("AssessmentsManager");
                role.Description = this.$rootScope.t("AnAssessmentsManagerCanAccessAssessmentsModule");
                break;
            case UserRoles.ConsentManager:
                role.Name = this.$rootScope.t("ConsentManager");
                role.Description = this.$rootScope.t("AConsentManagerCanAccessConsentModule");
                break;
            case UserRoles.CookieManager:
                role.Name = this.$rootScope.t("CookieManager");
                role.Description = this.$rootScope.t("ACookieManagerCanAccessCookieModule");
                break;
            case UserRoles.DataMappingManager:
                role.Name = this.$rootScope.t("DataMappingManager");
                role.Description = this.$rootScope.t("ADataMappingManagerCanAccessDMModule");
                break;
            case UserRoles.DataSubjectRequestsManager:
                role.Name = this.$rootScope.t("DataSubjectRequestsManager");
                role.Description = this.$rootScope.t("ADSARManagerCanAccessDSARModule");
                break;
            case UserRoles.IncidentsManager:
                role.Name = this.$rootScope.t("IncidentsManager");
                role.Description = this.$rootScope.t("AnIncidentsManagerCanAccessIncidentsModule");
                break;
            case UserRoles.VendorManager:
                role.Name = this.$rootScope.t("VendorManager");
                role.Description = this.$rootScope.t("AVendorManagerCanAccessVendorModule");
                break;
            default:
                if (!role.Description) {
                    role.Description = this.$rootScope.t("AGenericUser");
                }
                break;
        }
        return role;
    }

    public rolesImportAndReplace(request: IRoleAttachmentFile): ng.IPromise<IProtocolResponse<string>> {
        let isReplace;
        if (request.isReplace) {
            isReplace = {
                url: `/api/access/v1/roles/replace`,
                successMessage: this.$rootScope.t("ReplaceRoleSuccessMessage"),
                failureMessage: this.$rootScope.t("ReplaceRoleFailureMessage"),
            };
        } else {
            isReplace = {
                url: `/api/access/v1/roles/import`,
                successMessage: this.$rootScope.t("ImportRoleSuccessMessage"),
                failureMessage: this.$rootScope.t("ImportRoleFailureMessage"),
            };
        }
        const fileData: FormData = new FormData();
        fileData.append("DataStream", request.blob);
        fileData.append("FileName", request.fileName);
        fileData.append("Extension", request.extension);
        fileData.append("OrgGroupId", request.orgGroupId);
        fileData.append("Description", request.description);
        const messages: IProtocolMessages = {
            Success: { custom: isReplace.successMessage },
            Error: { custom: isReplace.failureMessage },
        };
        const importData = {
            importData: {
                name: request.importName,
                description: request.description,
                orgGroupId: request.orgGroupId,
            },
        };
        const config: IProtocolConfig = this.OneProtocol.customConfig("POST", isReplace.url, importData, fileData, null, null, null, null, null, true);
        return this.OneProtocol.http(config, messages);
    }

    private normalizeRolePageable(data: IPageable): IPageableRoles {
        const tenantOrgTable: IStringMap<IOrganization | string> = this.OrgGroupStoreNew.tenantOrgTable;
        const orgTable: IStringMap<IOrganization | string> = this.OrgGroupStoreNew.orgTable;
        const list: IRoleDetails[] = map(data.content, (role: IRoleDetails): IRoleDetails => {
            return this.normalizeRole(role, tenantOrgTable, orgTable);
        });
        return { content: list, page: omit(data, "content") as IPageableMeta };
    }

    private normalizeRole(
        role: IRoleDetails,
        tenantOrgTable: IStringMap<IOrganization | string> = this.OrgGroupStoreNew.tenantOrgTable,
        orgTable: IStringMap<IOrganization | string> = this.OrgGroupStoreNew.orgTable,
    ): IRoleDetails {
        if (!role.orgGroupId || role.orgGroupId === EmptyGuid) {
            role.orgGroupId = tenantOrgTable.rootId as string;
            role.IsSeeded = true;
        }
        if (role.orgGroupId && tenantOrgTable[role.orgGroupId]) {
            role.OrgGroupName = (tenantOrgTable[role.orgGroupId] as IOrganization).name;
        }
        role.CanEdit = role.CanDelete = role.CanReplace = !role.IsSeeded && has(orgTable, role.orgGroupId);
        return role;
    }

    private normalizePermissionTree(permissionTree: IPermissionHierarchyNode[]): IPermissionHierarchyNode[] {
        if (!permissionTree.length) return [];
        return map(permissionTree, (node: IPermissionHierarchyNode): IPermissionHierarchyNode => {
            node = node.children ? new PermissionGroupNode(node, this.$rootScope.t) : new PermissionNode(node, this.$rootScope.t);
            if (!node.id) {
                node.id = Utilities.uuid();
            }
            node.children = node.children ? this.normalizePermissionTree(node.children) : [];
            return node;
        });
    }

}
