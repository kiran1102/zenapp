// Angular
import {
    Injectable,
    Inject,
} from "@angular/core";

import { ModalService } from "sharedServices/modal.service";

import { getUserById } from "oneRedux/reducers/user.reducer";
import { StoreToken } from "tokens/redux-store.token";

// Interfaces
import { ProtocolService } from "modules/core/services/protocol.service";

// Enums
import { StatusCodes } from "../../enums/bulk-import.enums";

// Services
import { UserActionService } from "oneServices/actions/user-action.service";

// Pipes
import { OtDatePipe } from "pipes/ot-date.pipe";
import { TranslatePipe } from "modules/pipes/translate.pipe";

// Interfaces
import { IUser } from "interfaces/user.interface";
import { IStore } from "interfaces/redux.interface";
import { TableInputTypes } from "enums/inventory.enum";
import { IProtocolConfig, IProtocolMessages } from "interfaces/protocol.interface";

@Injectable()
export class BulkImportService {
    sortKey = "createdDate";
    sortOrder = "desc";
    pageNumber = 0;
    pageSize = 20;

    private cellTypes = TableInputTypes;
    private importTypes;

    constructor(
        @Inject(StoreToken) private store: IStore,
        @Inject(UserActionService) private UserActions: UserActionService,
        @Inject("Export") readonly Export: any,
        private OneProtocol: ProtocolService,
        private translatePipe: TranslatePipe,
        private modalService: ModalService,
        private DatePipe: OtDatePipe,
    ) {}

    public async initImportStatus() {
        try {
            this.importTypes = await this.getImportTypes();
            const tableConfig = await this.getConfigurations();
            const tableData = await this.getImportStatus();
            return {
                result: true,
                data: {
                    tableConfig,
                    tableData,
                },
            };
        } catch (e) {
            return { result: false, data: null };
        }
    }

    public async getImportStatus(page: number = 0, size: number = 20, sort: string = "createdDate,desc") {
        try {
            const params = { page, size, sort };
            const config = this.OneProtocol.config("GET", "/Job/ListJobs", params);
            const messages = { Error: { object: this.translatePipe.transform("ImportOverview"), action: this.translatePipe.transform("getting") } };
            const response = await this.OneProtocol.http(config, messages);
            const content = await this.formatTableData(response.data, this.importTypes.data);
            return {
                ...response.data,
                content,
            };
        } catch (e) {
            return null;
        }
    }

    public async refreshImportStatus(page, sort) {
        const order = sort.order;
        this.sortKey = sort.key || this.sortKey;
        this.sortOrder = order || this.sortOrder;
        return await this.getImportStatus(page, this.pageSize, `${this.sortKey},${this.sortOrder}`);
    }

    public createNewBulkImport(request) {
        const messages: IProtocolMessages = {Error: {Hide: true}};
        const config: IProtocolConfig = this.OneProtocol.config("POST", "/Job/CreateJobWithFile", {}, request);
        return this.OneProtocol.http(config, messages);
    }

    public getImportTypes(): any {
        const config: any = this.OneProtocol.config("GET", "/JobType/ListJobTypes", {});
        const messages: any = { Error: { object: this.translatePipe.transform("ImportTemplates"), action: this.translatePipe.transform("getting") } };
        return this.OneProtocol.http(config, messages);
    }

    public downloadTemplate(type: string, name: string): ng.IPromise<any> {
        return this.Export.xlsxImportTemplateExport(name, type);
    }

    private formatTableData(pageable, types) {
        const userIdList: any[] = [];
        const content = pageable.content.map((item) => {
            userIdList.push(item.CreatedBy);

            types.map((type) => {
                if (type.id === item.TypeId) item.Type = type.name;
                return type;
            });

            item.Status = this.translatePipe.transform(StatusCodes[item.StatusId]);
            item.ImportName = item.Name;
            item.DisplayDate = this.DatePipe.transform(item.CreatedDate);

            return item;
        });

        if (userIdList.length > 0) {
            return this.UserActions.fetchUsersById(userIdList).then(() => {
                return content.map((record) => {
                    const user: IUser = getUserById(record.CreatedBy)(this.store.getState());
                    return { ...record, CreatedBy: user ? user.FullName : "" };
                });
            });
        } else {
            return content;
        }
    }

    private getConfigurations(): any {
        return {
            cells: [
                {
                    id: "ImportId",
                    cellValueKey: "ImportId",
                    columnName: "#",
                    type: this.cellTypes.Text,
                    getLabel: (bulkImport: any): string => {
                        return bulkImport.ImportId;
                    },
                    isSortable: true,
                    sortName: "importId",
                    size: { width: "12rem" },
                },
                {
                    id: "ImportName",
                    cellValueKey: "Name",
                    columnName: this.translatePipe.transform("Name"),
                    type: this.cellTypes.Text,
                    getLabel: (bulkImport: any): string => {
                        return bulkImport.Name;
                    },
                    isSortable: true,
                    sortName: "name",
                    size: { width: "20rem" },
                },
                {
                    id: "Type",
                    cellValueKey: "Type",
                    columnName: this.translatePipe.transform("Type"),
                    type: this.cellTypes.Text,
                    getLabel: (bulkImport: any): string => {
                        return bulkImport.Type;
                    },
                    isSortable: true,
                    sortName: "type",
                    size: { width: "12rem" },
                },
                {
                    id: "Description",
                    cellValueKey: "Description",
                    columnName: this.translatePipe.transform("Description"),
                    type: this.cellTypes.Text,
                    getLabel: (bulkImport: any): string => {
                        return bulkImport.Description;
                    },
                    isSortable: true,
                    sortName: "description",
                    size: { width: "30rem" },
                },
                {
                    id: "CreatedDate",
                    cellValueKey: "DisplayDate",
                    columnName: this.translatePipe.transform("SubmitTime"),
                    type: this.cellTypes.Date,
                    getLabel: (bulkImport: any): string => {
                        return bulkImport.CreatedDate;
                    },
                    isSortable: true,
                    sortName: "createdDate",
                    size: { width: "20rem" },
                },
                {
                    id: "CreatedBy",
                    cellValueKey: "CreatedBy",
                    columnName: this.translatePipe.transform("User"),
                    type: this.cellTypes.Text,
                    getLabel: (bulkImport: any): string => {
                        return bulkImport.Username;
                    },
                    isSortable: false,
                    size: { width: "25rem" },
                },
                {
                    id: "Status",
                    cellValueKey: "Status",
                    columnName: this.translatePipe.transform("Status"),
                    type: this.cellTypes.Text,
                    getLabel: (bulkImport: any): string => {
                        return bulkImport.Status;
                    },
                    isSortable: false,
                    size: { width: "15rem" },
                },
                {
                    id: "Result",
                    cellValueKey: "Result",
                    columnName: this.translatePipe.transform("Result"),
                    type: this.cellTypes.Actions,
                    isSortable: false,
                    size: { width: "10rem" },
                },
            ],
        };
    }
}
