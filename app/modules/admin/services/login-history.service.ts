// 3rd Party
import { Injectable } from "@angular/core";

// Services
import { ProtocolService } from "modules/core/services/protocol.service";

// Interfaces
import {
    IProtocolConfig,
    IProtocolMessages,
    IProtocolPacket,
} from "interfaces/protocol.interface";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

@Injectable()
export class LoginHistoryService {

    constructor(
        private OneProtocol: ProtocolService,
        private translatePipe: TranslatePipe,
    ) { }

    getListofLoginUsers(page: number = 0, size: number): ng.IPromise<IProtocolPacket> {
        const config: IProtocolConfig = this.OneProtocol.config("GET", `/LogInHistory/getCurrentTenantLogs`, { page, size, sort: "createDt"});
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("ErrorRetrievingResponses") } };
        return this.OneProtocol.http(config, messages);
    }

    getSearchList(page: number = 0, size: number, search: string = ""): ng.IPromise<IProtocolPacket> {
        const config: IProtocolConfig = this.OneProtocol.config("GET", `/LogInHistory/searchLogsByUserData`, { page, size, sort: "createDt", search});
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("ErrorRetrievingResponses") } };
        return this.OneProtocol.http(config, messages);
    }

}
