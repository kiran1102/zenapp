// Angular
import { Injectable, Inject } from "@angular/core";

import { each, map } from "lodash";

// Redux
import { StoreToken } from "tokens/redux-store.token";
import {
    getUserById,
    getUserDisplayName,
} from "oneRedux/reducers/user.reducer";

// Services
import { ProtocolService } from "modules/core/services/protocol.service";
import { UserActionService } from "oneServices/actions/user-action.service";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

// Interfaces
import {
    IProtocolConfig,
    IProtocolMessages,
    IProtocolResponse,
} from "interfaces/protocol.interface";
import {
    IUser,
    IUserPaginationParams,
 } from "interfaces/user.interface";
import {
    IUserGroupDisplay,
    IUserGroup,
} from "interfaces/user-groups.interface";
import {
    IUserGroupUserDisplay,
    ISelectedUsers,
    IUserGroupUser,
} from "interfaces/user-groups-users.interface";
import {
    IPaginationParams,
    IPageableOf,
} from "interfaces/pagination.interface";
import {
    IStore,
    IStoreState,
} from "interfaces/redux.interface";

// Constants
import { UserGroupActions } from "constants/user-group-constants";

@Injectable()
export class UserGroupsService {
    constructor(
        @Inject(StoreToken) private store: IStore,
        private translatePipe: TranslatePipe,
        private UserActions: UserActionService,
        private OneProtocol: ProtocolService,
    ) {}

    createOrEditUserGroup(
        userGroupRequest: IUserGroup,
        userGroupId?: string,
        userGroupAction = UserGroupActions.CreateUserGroup,
    ): ng.IPromise<IProtocolResponse<IUserGroupDisplay>> {
        let httpMethod: string;
        let successMessage: string;
        let failureMessage: string;
        let url: string;
        if (userGroupAction === UserGroupActions.CreateUserGroup) {
            httpMethod = "POST";
            successMessage = "UserGroups.CreateSuccess";
            failureMessage = "UserGroups.CreateFailure";
            url = "/api/access/v1/groups";
        } else if (userGroupAction === UserGroupActions.EditUserGroup) {
            httpMethod = "PUT";
            successMessage = "UserGroups.EditSuccess";
            failureMessage = "UserGroups.EditFailure";
            url = `/api/access/v1/groups/${userGroupId}`;

        }
        const config: IProtocolConfig = this.OneProtocol.customConfig(
            httpMethod,
            url,
            null,
            userGroupRequest,
        );
        const messages: IProtocolMessages = {
            Error: {
                custom: this.translatePipe.transform(failureMessage),
            },
            Success: {
                custom: this.translatePipe.transform(successMessage),
            },
        };
        return this.OneProtocol.http(config, messages);
    }

    addUsers(
        users: ISelectedUsers,
        userGroupId: string,
    ): ng.IPromise<IProtocolResponse<IUserGroupDisplay>> {
        const url = `/api/access/v1/groups/${userGroupId}/members`;
        const config: IProtocolConfig = this.OneProtocol.customConfig("POST", url, null, users);
        const messages: IProtocolMessages = {
            Error: { custom: this.translatePipe.transform("UserGroups.AddUsersFailure") },
            Success: { custom: this.translatePipe.transform("UserGroups.AddUsersSuccess") },
        };
        return this.OneProtocol.http(config, messages);
    }

    removeUser(
        user: IUserGroupUserDisplay,
        userGroupId: string,
    ): ng.IPromise<IProtocolResponse<IUserGroupDisplay>> {
        const url = `/api/access/v1/groups/${userGroupId}/members/${user.memberId}`;
        const config: IProtocolConfig = this.OneProtocol.customConfig("DELETE", url);
        const successMessage = `${user.fullName} ${this.translatePipe.transform("UserGroups.RemoveUserSuccess")}`;
        const messages: IProtocolMessages = {
            Error: { custom: this.translatePipe.transform("UserGroups.RemoveUserFailure") },
            Success: { custom: successMessage },
        };
        return this.OneProtocol.http(config, messages);
    }

    getUserGroupDetails(
        userGroupId: string,
    ): ng.IPromise<IProtocolResponse<IPageableOf<IUserGroupDisplay>>> {
        const url = `/api/access/v1/groups/${userGroupId}`;
        const config: IProtocolConfig = this.OneProtocol.customConfig("GET", url);
        const messages: IProtocolMessages = {};
        return this.OneProtocol.http(config, messages);
    }

    listUserGroupMembers(
        params: IPaginationParams<string>,
        userGroupId: string,
    ): ng.IPromise<IProtocolResponse<IPageableOf<IUserGroupUserDisplay>>> {
        const url = `/api/access/v1/groups/${userGroupId}/members`;
        const config: IProtocolConfig = this.OneProtocol.customConfig("GET", url, this.serialize(params));
        const messages: IProtocolMessages = {};
        return this.OneProtocol.http(config, messages).then(
            (response: IProtocolResponse<IPageableOf<IUserGroupUserDisplay>>) => {
                if (response.result) {
                    return this.getUsersByIdAndApply(
                        response.data,
                        ["createdBy"],
                    ).then((pageable: IPageableOf<IUserGroupUserDisplay>) => {
                        return {
                            ...response,
                            data: pageable,
                        };
                    });
                } else {
                    return null;
                }
            },
        );
    }

    listUserGroups(
        params: IPaginationParams<string>,
        searchTerm?: string,
        apiAction = UserGroupActions.ListUserGroups,
    ): ng.IPromise<IProtocolResponse<IPageableOf<IUserGroupDisplay>>> {
        const messages: IProtocolMessages = {};
        params.filters = apiAction === UserGroupActions.ListUserGroups ? "" : searchTerm;
        const config: IProtocolConfig = this.OneProtocol.customConfig(
            "GET", "/api/access/v1/groups", this.serialize(params),
        );
        return this.OneProtocol.http(config, messages).then(
            (response: IProtocolResponse<IPageableOf<IUserGroupDisplay>>) => {
                if (response.result) {
                    return this.getUsersByIdAndApply(
                        response.data,
                        ["createdBy", "lastModifiedBy"],
                    ).then((pageable: IPageableOf<IUserGroupDisplay>) => {
                        return {
                            ...response,
                            data: pageable,
                        };
                    });
                } else {
                    return null;
                }
            },
        );
    }

    serializeSearch(value = "") {
        value = value.trim();
        const query = value ? `fullName=~=%${value}%,email=~=%${value}%` : "";
        return query;
    }

    listUsersToShow(userGroupId: string, params: IPaginationParams<string>): ng.IPromise<IProtocolResponse<IPageableOf<IUserGroupUser>>> {
        const url = `/api/access/v1/groups/${userGroupId}/members/available`;
        const config: IProtocolConfig = this.OneProtocol.customConfig("GET", url, params);
        const messages: IProtocolMessages = {};
        return this.OneProtocol.http(config, messages);
    }

    getUsersByIdAndApply(pageable: IPageableOf<any>, props: string[]): ng.IPromise<IPageableOf<any>> {
        const uniqueUserIds = new Set();
        each(pageable.content, (page) => {
            for (const prop of props) {
                if (page[prop]) {
                    uniqueUserIds.add(page[prop]);
                }
            }
        });
        const userIds: string[] = Array.from(uniqueUserIds.values());
        return this.UserActions.fetchUsersById(userIds).then(() => {
            const state: IStoreState = this.store.getState();
            return {
                ...pageable,
                content: map(pageable.content, (item) => {
                    if (!item["lastModifiedBy"] && item["createdBy"]) {
                        item.lastModifiedBy = item.createdBy;
                    }
                    if (!item["lastModifiedDate"] && item["createdDate"]) {
                        item.lastModifiedDate = item.createdDate;
                    }
                    for (const prop of props) {
                        if ( prop in item ) {
                            const user: IUser = item[prop] ? getUserById(item[prop].toLowerCase())(state) : null;
                            item[`${prop}Display`] = user ? getUserDisplayName(user) : "";
                        }
                    }
                    return item;
                }),
            };
        });
    }

    private serialize(params: IPaginationParams<string>) {
        if (!params.filters) return params;
        const paramQuerySearchTerm = params.filters ? `name=~=%${params.filters}%` : "";
        return {
            ...params,
            filters: `${paramQuerySearchTerm}`,
        };
    }
}
