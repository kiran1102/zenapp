import * as angular from "angular";
import { UserApiService } from "oneServices/api/user-api.service";
import { UserAdapter } from "oneServices/adapters/user-adapter.service.ts";

import routes from "./admin.routes";

import { RolesService } from "adminService/roles.service";

import adminWrapperComponent from "adminComponent/admin-wrapper.component";

import { userManageView } from "adminComponent/users/manage/users-manage-view.component";
import { userAccessLevelPicker } from "adminComponent/users/user-access-level/user-access-level-picker.component";
import { userAccessLevelForm } from "adminComponent/users/user-access-level/user-access-level-form.component";

import { OrgActionService } from "oneServices/actions/org-action.service";

import roleManageViewComponent from "adminComponent/roles/manage/role-manage-view.component";
import roleManageFormComponent from "adminComponent/roles/manage/role-manage-form.component";
import roleManagePermissionsComponent from "adminComponent/roles/manage/role-manage-permissions.component";
import rolePermissionsListComponent from "adminComponent/roles/manage/role-permissions-list.component";
import rolePermissionsGroupsComponent from "adminComponent/roles/manage/role-permissions-groups.component";

export const adminModule = angular
    .module("zen.admin", ["ui.router"])
    .config(routes)

    .component("adminWrapper", adminWrapperComponent)

    .component("userManageView", userManageView)
    .component("userAccessLevelPicker", userAccessLevelPicker)
    .component("userAccessLevelForm", userAccessLevelForm)

    .component("roleManageView", roleManageViewComponent)
    .component("roleManageForm", roleManageFormComponent)
    .component("roleManagePermissions", roleManagePermissionsComponent)
    .component("rolePermissionsList", rolePermissionsListComponent)
    .component("rolePermissionsGroups", rolePermissionsGroupsComponent)

    .service("RolesService", RolesService)

    .service("UserApi", UserApiService)
    .service("UserAdapter", UserAdapter)
    .service("OrgAction", OrgActionService)
;
