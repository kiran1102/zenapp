// Angular
import {
    Component,
    Input,
} from "@angular/core";
import { Platform } from "@angular/cdk/platform";

// Modules
import { TransitionService } from "@uirouter/angularjs";

// Rxjs
import { BehaviorSubject } from "rxjs";

// 3rd Party
import { isFunction } from "lodash";

// Services
import { StateService } from "@uirouter/core";
import { GlobalSidebarService } from "modules/global-sidebar/services/one-global-sidebar.service";
import { Permissions } from "modules/shared/services/helper/permissions.service";

// Interfaces
import { INavMenuItem } from "modules/global-sidebar/interfaces/nav-menu-item.interface";

@Component({
    selector: "one-global-sidebar",
    templateUrl: "./one-global-sidebar.component.html",
})
export class OneGlobalSidebar {
    @Input() isCollapsed: boolean;
    @Input() preventCollapse: boolean;
    @Input() lightTheme: boolean;
    @Input() theme: boolean;

    @Input() collapsible$: BehaviorSubject<boolean>;
    @Input() menu$: BehaviorSubject<INavMenuItem[]>;
    @Input() title$: BehaviorSubject<string>;
    @Input() menuClass = this.globalSidebarService.menuClass;

    currentUrl = "";
    storageKey = "OneTrust.SidebarCollapsed";
    isIe11 = false;
    sidebarCollapse = !this.permissions.canShow("SidebarCollapse");

    constructor(
        private permissions: Permissions,
        private  $transitions: TransitionService,
        private platform: Platform,
        private stateService: StateService,
        private globalSidebarService: GlobalSidebarService,
    ) {}

    ngOnInit(): void {
        this.isIe11 = this.platform.TRIDENT;
        if (!this.preventCollapse) this.isCollapsed = !!JSON.parse(localStorage.getItem(this.storageKey));
        this.$transitions.onSuccess({},  () => {
            this.currentUrl = window.location.href;
        });
    }

    triggerItemAction(item: INavMenuItem) {
        if (item.children && this.isCollapsed) {
            if (isFunction(item.children[0].action)) {
                return item.children[0].action(item);
            }
            return this.stateService.go(
                item.children[0].route,
                item.children[0].params,
            );
        }

        if (isFunction(item.action)) return item.action(item);

        return item.open = !item.open;
    }

    handleCollapse(value: boolean) {
        window.dispatchEvent(new Event("resize"));
        this.isCollapsed = value;
        if (value) return localStorage.setItem(this.storageKey, JSON.stringify(true));
        return window.localStorage.removeItem(this.storageKey);
    }
}
