import { TransitionOptions, StateService } from "@uirouter/core";

export interface INavLinkItem {
    activeRoute?: string;
    activeRouteFn?: (stateService: StateService) => boolean;
    options?: TransitionOptions;
    params?: any;
    route?: string;
    legacy?: boolean;
}

export interface INavActionItem {
    action?: (item: INavMenuItem) => void;
}

export interface INavGroupItem {
    children?: INavMenuItem[];
    open?: boolean;
}

export interface INavUpgradeItem {
    upgrade?: boolean;
}

export interface INavMenuItem extends INavLinkItem, INavActionItem, INavGroupItem, INavUpgradeItem  {
    icon?: string;
    id: string;
    title: string;
}
