// Angular
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { RouterModule } from "@angular/router";
import { PlatformModule } from "@angular/cdk/platform";

// Modules
import { UIRouterModule } from "@uirouter/angular";
import { TranslateModule } from "@ngx-translate/core";
import { NgxPermissionsModule } from "ngx-permissions";
import { OtSidebarModule } from "@onetrust/vitreus";
import { SharedModule } from "sharedModules/shared.module";

// Services
import { GlobalSidebarService } from "./services/one-global-sidebar.service";

// Components
import { OneGlobalSidebar } from "./one-global-sidebar/one-global-sidebar.component";

// Pipes
import { SidebarActiveItemPipe } from "modules/global-sidebar/pipes/sidebar-active-item.pipe";

@NgModule({
    imports: [
        CommonModule,
        RouterModule,
        PlatformModule,
        UIRouterModule,
        TranslateModule,
        NgxPermissionsModule.forChild(),
        OtSidebarModule,
        SharedModule,
    ],
    providers: [
        GlobalSidebarService,
    ],
    declarations: [
        OneGlobalSidebar,
        SidebarActiveItemPipe,
    ],
    entryComponents: [
        OneGlobalSidebar,
    ],
    exports: [
        OneGlobalSidebar,
    ],
})
export class GlobalSidebarModule {}
