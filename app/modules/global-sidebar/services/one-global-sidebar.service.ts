// Angular
import { Injectable } from "@angular/core";
import { BehaviorSubject } from "rxjs";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

// Services
import { SidebarService } from "@onetrust/vitreus";
import { NgxPermissionsService } from "ngx-permissions";

// Interfaces
import { INavMenuItem } from "./../interfaces/nav-menu-item.interface";

@Injectable()
export class GlobalSidebarService {

    public menuClass = new BehaviorSubject<string>(null);

    constructor(
        private sidebarService: SidebarService,
        private permissions: NgxPermissionsService,
        private translatePipe: TranslatePipe,
    ) {}

    set(
        menu: INavMenuItem[] = null,
        title: string = null,
        theme: boolean = false,
        collapsible: boolean = true,
        menuClass: string = null,
    ) {
        this.sidebarService.setContent(menu, title, collapsible);
        this.menuClass.next(menuClass);
    }

    setAAMenu() {
        const menuGroup: INavMenuItem[] = [];

         // Dashboard
        if (this.permissions.getPermission("Dashboard")) {
            menuGroup.push({
                id: "DashboardSidebarItem",
                title: "Dashboard",
                route: "zen.app.pia.module.dashboard.list",
                activeRoute: "zen.app.pia.module.dashboard",
                icon: "ot ot-dashboard",
            });
        }
        if (
            this.permissions.getPermission("ViewDashboards") &&
            this.permissions.getPermission("Assessments")
        ) {
            menuGroup.push({
                id: "DashboardSidebarItem",
                title: "Dashboard",
                route: "zen.app.pia.module.dashboardV2.list",
                activeRoute: "zen.app.pia.module.dashboardV2",
                icon: "ot ot-dashboard",
            });
        }

        if (this.permissions.getPermission("DashboardUpgrade")) {
            menuGroup.push({
                id: "DashboardSidebarItem",
                title: "Dashboard",
                icon: "ot ot-dashboard",
                upgrade: true,
            });
        }

        // Templates
        if (this.permissions.getPermission("TemplatesV2")) {
            menuGroup.push({
                id: "TemplatesSidebarItem",
                title: "Templates",
                route: "zen.app.pia.module.templatesv2.list",
                activeRoute: "zen.app.pia.module.templatesv2",
                icon: "ot ot-templates",
            });
        }
        if (this.permissions.getPermission("TemplatesUpgrade")) {
            menuGroup.push({
                id: "QuestionnairesSidebarItem",
                title: "Questionnaires",
                icon: "ot ot-templates",
                upgrade: true,
            });
        }
        if (
            this.permissions.getPermission("Templates") &&
            !this.permissions.getPermission("TemplatesV2")
        ) {
            menuGroup.push({
                id: "QuestionnairesSidebarItem",
                title: "Questionnaires",
                route: "zen.app.pia.module.templates.list",
                activeRoute: "zen.app.pia.module.templates",
                icon: "ot ot-templates",
            });
        }

        // Assessments
        if (this.permissions.getPermission("Assessments")) {
            menuGroup.push({
                id: "AssessmentsSidebarItem",
                title: "Assessments",
                route: "zen.app.pia.module.assessment.list",
                activeRoute: "zen.app.pia.module.assessment",
                icon: "ot ot-file-text-o",
            });
        }
        if (
            this.permissions.getPermission("Projects") &&
            !this.permissions.getPermission("Assessments")
        ) {
            menuGroup.push({
                id: "ProjectsSidebarItem",
                title: "Projects",
                route: "zen.app.pia.module.projects.list",
                activeRoute: "zen.app.pia.module.projects",
                icon: "ot ot-file-text-o",
            });
        }

        // Assessment Settings
        if (this.permissions.getPermission("Assessments") && this.permissions.getPermission("AssessmentAutomationSettings")) {
            menuGroup.push({
                id: "SettingsSidebarItem",
                title: "Settings",
                route: "zen.app.pia.module.settings.assessments.template",
                activeRoute: "zen.app.pia.module.settings",
                icon: "ot ot-gear",
            });
        }

        this.set(
            menuGroup,
            this.translatePipe.transform("AssessmentAutomation"),
        );
    }
}
