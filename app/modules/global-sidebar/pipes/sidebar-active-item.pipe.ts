import { isFunction } from "lodash";
import {
    Pipe,
    PipeTransform,
} from "@angular/core";
import { StateService } from "@uirouter/core";
import { INavMenuItem } from "modules/global-sidebar/interfaces/nav-menu-item.interface";

@Pipe({
    name: "SidebarActiveItem",
})

export class SidebarActiveItemPipe implements PipeTransform {
    constructor(
        private stateService: StateService,
    ) {}

    transform(item: INavMenuItem, currentUrl): boolean {
        if (item.children) {
            const arr = item.children.map((child) => {
                if (this.stateService.includes(child.activeRoute || child.route, child.params)) {
                    return true;
                } else if (isFunction(child.activeRouteFn)) {
                    return child.activeRouteFn(this.stateService);
                } else {
                    return false;
                }
            });
            return arr.includes(true);
        } else if (!(item.activeRoute || item.route) && !item.activeRouteFn) {
            return false;
        } else if (isFunction(item.activeRouteFn)) {
            return item.activeRouteFn(this.stateService);
        } else {
            return this.stateService.includes(
                item.activeRoute || item.route,
                item.params,
            );
        }
    }
}
