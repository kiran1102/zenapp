// RxJs
import { BehaviorSubject } from "rxjs";

// Services
import { StateService } from "@uirouter/core";
import { Permissions } from "modules/shared/services/helper/permissions.service";
import { GlobalSidebarService } from "modules/global-sidebar/services/one-global-sidebar.service";

// Interfaces
import { INavMenuItem } from "modules/global-sidebar/interfaces/nav-menu-item.interface";

class PreferencesWrapperCtrl implements ng.IComponentController {
    static $inject: string[] = [
        "Permissions",
        "$rootScope",
        "GlobalSidebarService",
    ];

    menu$: BehaviorSubject<INavMenuItem[]>;
    title$: BehaviorSubject<string>;
    collapsible$: BehaviorSubject<boolean>;

    constructor(
        private permissions: Permissions,
        private $rootScope: any,
        private globalSidebarService: GlobalSidebarService,
    ) { }

    $onInit() {
        const menuRoutes = this.getMenuRoutes();
        this.globalSidebarService.set();
        this.menu$ = new BehaviorSubject<INavMenuItem[]>(menuRoutes);
        this.title$ = new BehaviorSubject<string>(null);
        this.collapsible$ = new BehaviorSubject<boolean>(false);
    }

    private getMenuRoutes() {
        const menuGroup: INavMenuItem[] = [];
        const generalPreferencesGroup: INavMenuItem = {
            id: "PreferencesGeneralSidebarItemGroup",
            title: this.$rootScope.t("General"),
            children: [],
            open: true,
        };
        if (this.permissions.canShow("Profile")) {
            generalPreferencesGroup.children.push({
                id: "PreferencesProfileSidebarItem",
                title: this.$rootScope.t("ChangePassword"),
                route: "zen.app.pia.module.preferences.password",
                activeRoute: "zen.app.pia.module.preferences.password",
            });
        }
        if (this.permissions.canShow("UserFeedBack") || this.permissions.canShow("UserGuidance")) {
            generalPreferencesGroup.children.push({
                id: "PreferencesLearningAndFeedbackToolSidebarItem",
                title: this.$rootScope.t("LearningAndFeedbackTools"),
                route: "zen.app.pia.module.preferences.learning-and-feedback-tool",
                activeRoute: "zen.app.pia.module.preferences.learning-and-feedback-tool",
            });
        }
        if (generalPreferencesGroup.children.length) {
            menuGroup.push(generalPreferencesGroup);
        }
        return menuGroup;
    }

}

export const preferencesWrapper: ng.IComponentOptions = {
    controller: PreferencesWrapperCtrl,
    template: `
        <div class="preferences-wrapper column-nowrap full-size stretch-vertical text-color-default background-white">
            <one-header class="settings-wrapper__header static-vertical text-center color-white">
                <header-title></header-title>
                <header-body>
                    <span class="heading2 text-thin text-center" ng-bind="$root.t('UserPreferences')"></span>
                </header-body>
            </one-header>
            <div class="row-nowrap full-size stretch-vertical">
                <one-global-sidebar
                    [menu$]="$ctrl.menu$"
                    [title$]="$ctrl.title$"
                    [light-theme]="true"
                    [prevent-collapse]="true">
                </one-global-sidebar>
                <ui-view class="stretch-horizontal full-size"></ui-view>
            </div>
        </div>
    `,
};
