import {
    Component,
    OnInit,
    Inject,
} from "@angular/core";
import {
    FormBuilder,
    FormGroup,
    FormControl,
    Validators,
} from "@angular/forms";

// Enums and Constants
import {
    NO_BLANK_SPACE,
    SPECIAL_CHARACTERS,
} from "constants/regex.constant";

import { isUndefined } from "lodash";

// Services
import { Accounts } from "modules/shared/services/api/accounts.service";
import { StateService } from "@uirouter/angular";

// Redux
import { StoreToken } from "tokens/redux-store.token";
import { getCurrentUser } from "oneRedux/reducers/user.reducer";

// Interfaces
import { IStore } from "interfaces/redux.interface";
import { IUser } from "interfaces/user.interface";

@Component({
    selector: "preferences-change-password",
    templateUrl: "./preferences-change-password.component.html",
})
export class PreferencesChangePasswordComponent implements OnInit {

    currentUser: IUser;
    changePasswordForm: FormGroup;
    type1 = "password";
    type2 = "password";
    type3 = "password";

    constructor(
        @Inject(StoreToken) readonly store: IStore,
        public stateService: StateService,
        private readonly formBuilder: FormBuilder,
        private accounts: Accounts,
    ) { }

    ngOnInit() {
        this.currentUser = getCurrentUser(this.store.getState());
        this.initForm();
    }

    renameSubmitPassword() {
        this.accounts.changePassword({
            Password: this.changePasswordForm.get("currentPassword").value,
            NewPassword: this.changePasswordForm.get("newPassword").value,
            ConfirmNewPassword: this.changePasswordForm.get("confirmPassword").value,
        });
    }

    private initForm() {
        this.changePasswordForm = this.formBuilder.group({
            currentPassword: [null, [Validators.required]],
            newPassword:
                [null,
                    [
                        Validators.required,
                        Validators.pattern(NO_BLANK_SPACE),
                        Validators.minLength(8),
                        validateNumber,
                        validateUppercase,
                        validateLowercase,
                        this.validateSpecialChars,
                        validateUserDetails("newPassword", this.currentUser),
                        removeSpacesInPassword,
                    ],
                ],
            confirmPassword:
                [null,
                    [
                        Validators.required,
                        matchPasswordOnBothPasswordControlsValidator("newPassword"),
                    ],
                ],
        });
    }

    private validateSpecialChars = (c: FormControl) => {
        return (SPECIAL_CHARACTERS.test(c.value)) ? null : { hasSpecialChar: true };
    }

}

export function validateUserDetails(password: string, user: IUser) {
    let newPasswordControl: FormControl;
    return function validateDetails(control: FormControl) {

        if (!control.parent) { return null; }

        if (!newPasswordControl) {
            newPasswordControl = control.parent.get(password) as FormControl;
        }
        if (newPasswordControl.value) {
            if (newPasswordControl.value.toLowerCase() === user.Email.toLowerCase()
            ) {
                return { isEmailPassword: true };
            }
            return null;
        }
    };
}

export function removeSpacesInPassword(c: FormControl) {
    if (c && c.value && !c.value.replace(/\s/g, "").length) {
      c.setValue("");
    }
    return null;
  }

export function validateNumber(c: FormControl) {
    return (/[0-9]/.test(c.value)) ? null : { isNumber: true };
}

export function validateUppercase(c: FormControl) {
    return (/[A-Z]/.test(c.value)) ? null : { isUppercase: true };
}

export function blankSpaceValidator(c: FormControl) {
    return lengthOfPasswordAfterTrim(c) > 0 ? null : { isBlank: true };
}

export function lengthOfPasswordAfterTrim(c: FormControl) {
    return c.value.trim().length;
}

export function validateLowercase(c: FormControl) {
    return (/[a-z]/.test(c.value)) ? null : { isLowercase: true };
}

export function matchPasswordOnBothPasswordControlsValidator(password: string) {

    let confirmPasswordControl: FormControl;
    let newPasswordControl: FormControl;

    return function matchPasswordFromBothControlsValidate(control: FormControl) {

        if (!control.parent) { return null; }

        // Initializing the validator.
        if (!confirmPasswordControl) {
            confirmPasswordControl = control;
            newPasswordControl = control.parent.get(password) as FormControl;
            newPasswordControl.valueChanges.subscribe(() => {
                confirmPasswordControl.updateValueAndValidity();
            });
        }
        if (newPasswordControl.value !== confirmPasswordControl.value || !newPasswordControl.value || !confirmPasswordControl.value) {
            return {
                mismatch: true,
            };
        }
        return null;
    };

}
