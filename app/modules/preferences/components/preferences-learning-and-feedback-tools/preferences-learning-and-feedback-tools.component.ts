import {
    Component,
    OnInit,
    Inject,
} from "@angular/core";

// External Libraries
import { cloneDeep } from "lodash";

// Redux
import { StoreToken } from "tokens/redux-store.token";

// Constants
import { ToolNames } from "constants/learning-and-feedback-tool.constant";

// Services
import { SettingsLearningAndFeedbackService } from "settingsServices/apis/settings-learning-and-feedback-api.service";
import { Userlane } from "sharedModules/services/provider/userlane.service";
import { Wootric } from "sharedModules/services/provider/wootric.service";
import { Permissions } from "modules/shared/services/helper/permissions.service";

// Interfaces
import { IStore } from "interfaces/redux.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";
import { ILearningAndFeedbackUserSetting } from "interfaces/settings-learning-and-feedback-tool.interface";

@Component({
    selector: "preferences-learning-and-feedback-tools",
    templateUrl: "./preferences-learning-and-feedback-tools.component.html",
})
export class PreferencesLearningAndFeedbackTools implements OnInit {

    ready = false;
    loading = false;
    currentUserGuidanceSettings: ILearningAndFeedbackUserSetting;
    updatedUserGuidanceSettings: ILearningAndFeedbackUserSetting;
    updatedUserFeedbackSettings: ILearningAndFeedbackUserSetting;
    currentUserFeedbackSettings: ILearningAndFeedbackUserSetting;
    userGuidanceAdminDisabled = true;
    userFeedbackAdminDisabled = true;
    userGuidanceAdminChecked = false;
    userFeedbackAdminChecked = false;
    showSaveButton = false;
    disableSaveButton = true;

    constructor(
        @Inject(StoreToken) readonly store: IStore,
        private settingsLearningAndFeedbackService: SettingsLearningAndFeedbackService,
        private userlane: Userlane,
        private wootric: Wootric,
        private permissions: Permissions,
    ) { }

    ngOnInit() {
        this.getLearningAndFeedbackSettings();
    }

    getLearningAndFeedbackSettings() {
        this.ready = false;
        this.settingsLearningAndFeedbackService
            .getLearningAndFeedbackUserSettings().then(
            (response: IProtocolResponse<ILearningAndFeedbackUserSetting[]>) => {
                    if (response.result && response.data) {
                        const settingsData = response.data;
                        if (response.data.length !== 0) {
                            this.showSaveButton = true;
                            settingsData.forEach((setting: ILearningAndFeedbackUserSetting) => {
                                if (setting.toolName === ToolNames.UserFeedback) {
                                    this.currentUserFeedbackSettings = setting;
                                    this.userFeedbackAdminChecked = this.currentUserFeedbackSettings.enabled;
                                    this.updatedUserFeedbackSettings = { ...setting };
                                    this.userFeedbackAdminDisabled = false;
                                } else if (setting.toolName === ToolNames.UserGuidance) {
                                    this.currentUserGuidanceSettings = setting;
                                    this.userGuidanceAdminChecked = this.currentUserGuidanceSettings.enabled;
                                    this.updatedUserGuidanceSettings = { ...setting };
                                    this.userGuidanceAdminDisabled = false;
                                }
                            });
                        }
                    }
                    this.ready = true;
                },
            );
    }

    toggleUserFeedback(feedbackEnable: boolean) {
        this.updatedUserFeedbackSettings.enabled = feedbackEnable;
        this.disableSaveButton = this.getSaveStatus();
    }

    toggleUserGuidance(guidanceEnable: boolean) {
        this.updatedUserGuidanceSettings.enabled = guidanceEnable;
        this.disableSaveButton = this.getSaveStatus();
    }

    saveSettings() {
        this.loading = true;
        const settingsArray = [];
        if (this.currentUserFeedbackSettings) {
            settingsArray.push(this.updatedUserFeedbackSettings);
        }
        if (this.currentUserGuidanceSettings) {
            settingsArray.push(this.updatedUserGuidanceSettings);
        }
        this.settingsLearningAndFeedbackService.saveLearningAndFeedbackUserSettings(settingsArray).then(
            (response: IProtocolResponse<ILearningAndFeedbackUserSetting[]>) => {
                if (response.result) {
                    if (!this.userFeedbackAdminDisabled) {
                        if (this.updatedUserFeedbackSettings.enabled !== this.userGuidanceAdminChecked) {
                            if (this.updatedUserFeedbackSettings.enabled) {
                                this.wootric.initialize();
                            } else {
                                this.wootric.logout();
                            }
                        }
                    }
                    if (!this.userGuidanceAdminDisabled) {
                        if (this.updatedUserGuidanceSettings.enabled !== this.userGuidanceAdminChecked) {
                            if (this.updatedUserGuidanceSettings.enabled) {
                                this.userlane.initialize();
                            } else {
                                this.userlane.logout();
                            }
                        }
                    }
                    this.getLearningAndFeedbackSettings();
                    this.disableSaveButton = true;
                }
                this.loading = false;
            });
    }

    private getSaveStatus(): boolean {
        if (!this.userGuidanceAdminDisabled) {
            if (this.updatedUserGuidanceSettings.enabled !== this.userGuidanceAdminChecked) {
                return false;
            }
        }
        if (!this.userFeedbackAdminDisabled) {
            if (this.updatedUserFeedbackSettings.enabled !== this.userFeedbackAdminChecked) {
                return false;
            }
        }
        return true;
    }

}
