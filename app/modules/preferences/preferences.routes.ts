import { Permissions } from "modules/shared/services/helper/permissions.service";
import { GlobalSidebarService } from "modules/global-sidebar/services/one-global-sidebar.service";

export default function preferencesRoutes($stateProvider: ng.ui.IStateProvider) {
    $stateProvider
        .state("zen.app.pia.module.preferences", {
            url: "preferences",
            views: {
                module: {
                    template: "<preferences-wrapper class='column-nowrap full-size'></preferences-wrapper>",
                    controller: [
                        "GlobalSidebarService",
                        (globalSidebarService: GlobalSidebarService) => {
                            globalSidebarService.set();
                        },
                    ],
                },
            },
            resolve: {
                ProfilePermission: [
                    "Permissions",
                    (permissions: Permissions): boolean => {
                        if (permissions.canShow("Profile")) {
                            return true;
                        } else {
                            permissions.goToFallback();
                        }
                    },
                ],
            },
        })
        .state("zen.app.pia.module.preferences.password", {
            url: "/password",
            template: "<downgrade-preferences-change-password></downgrade-preferences-change-password>",
        })
        .state("zen.app.pia.module.preferences.learning-and-feedback-tool", {
            url: "/learning-and-feedback-tool",
            template: "<downgrade-preferences-learning-and-feedback-tools></downgrade-preferences-learning-and-feedback-tools>",
            resolve: {
                ToolsPermission: [
                    "Permissions",
                    (permissions: Permissions): boolean => {
                        if (permissions.canShow("UserFeedBack") || permissions.canShow("UserGuidance")) {
                            return true;
                        } else {
                            permissions.goToFallback();
                        }
                    },
                ],
            },
        });
}

preferencesRoutes.$inject = ["$stateProvider"];
