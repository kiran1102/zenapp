import * as angular from "angular";

import { preferencesWrapper } from "modules/preferences/components/preferences-wrapper/preferences-wrapper.component";

// Routes
import preferencesRoutes from "./preferences.routes";

export const preferencesLegacyModule = angular
    .module("zen.app.pia.module.preferences", ["ui.router"])
    .config(preferencesRoutes)
    .component("preferencesWrapper", preferencesWrapper)
    ;
