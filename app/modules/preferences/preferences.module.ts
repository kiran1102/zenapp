// 3rd party
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from "@angular/core";
import { RouterModule } from "@angular/router";
import { SharedModule } from "sharedModules/shared.module";
import { UpgradedLegacyModule } from "sharedModules/upgrade/upgraded-legacy.module";
import { CommonModule } from "@angular/common";
import { HttpClientModule } from "@angular/common/http";
import { FormsModule } from "@angular/forms";
import { ReactiveFormsModule } from "@angular/forms";
import { VitreusModule } from "@onetrust/vitreus";
import { DndListModule } from "ngx-drag-and-drop-lists";
import { OTPipesModule, PIPES } from "modules/pipes/pipes.module";
import { VirtualScrollModule } from "od-virtualscroll";
import { OneVideoModule } from "modules/video/video.module";
import { TranslateModule } from "@ngx-translate/core";

// Components
import { PreferencesChangePasswordComponent } from "./components/preferences-change-password/preferences-change-password.component";
import { PreferencesLearningAndFeedbackTools } from "./components/preferences-learning-and-feedback-tools/preferences-learning-and-feedback-tools.component";

@NgModule({
    imports: [
        SharedModule,
        UpgradedLegacyModule,
        CommonModule,
        HttpClientModule,
        FormsModule,
        ReactiveFormsModule,
        VitreusModule,
        DndListModule,
        OTPipesModule,
        VirtualScrollModule,
        OneVideoModule,
        RouterModule,
        TranslateModule,
    ],
    exports: [
        PreferencesChangePasswordComponent,
        PreferencesLearningAndFeedbackTools,
    ],
    declarations: [
        PreferencesChangePasswordComponent,
        PreferencesLearningAndFeedbackTools,
    ],
    entryComponents: [
        PreferencesChangePasswordComponent,
        PreferencesLearningAndFeedbackTools,
    ],
    providers: [
        PIPES, // TODO: Remove from providers when ot-common is implemented. Only necessary if you are using pipe in TS file, else just module is fine
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class PreferencesModule { }
