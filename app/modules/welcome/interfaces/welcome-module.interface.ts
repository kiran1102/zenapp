import { WelcomeModules } from "modules/welcome/enums/welcome.enum";

export interface IWelcomeLinkContent {
    Link?: string;
    ContentLinkKey?: string;
}

export interface IWelcomeTileAndDetailContent {
    id: WelcomeModules;
    title: string;
    icon: string;
    description: string;
    subtitle: string;
    video: string;
    welcomeDetailTitle: string;
    params?: any;
    route?: string;
    action?: () => void;
}

export interface IWelcomeUpgradeCard {
    title: string;
    icon: string;
    route?: string;
    action?: () => void;
}
