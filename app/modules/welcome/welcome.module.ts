// Core
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { TranslateModule } from "@ngx-translate/core";
import { UIRouterModule } from "@uirouter/angular";

// Modules
import { OtButtonModule, OtLoadingModule } from "@onetrust/vitreus";
import { OneVideoModule } from "modules/video/video.module";

// Routing
import { routing } from "./welcome.routing";

// Components
import { WelcomePageComponent } from "./welcome-page/welcome-page.component";
import { WelcomeDetailComponent } from "./welcome-detail/welcome-detail.component";

import { WelcomeTilesService } from "./services/welcome-content.service";

@NgModule({
    imports: [
        CommonModule,
        routing,
        UIRouterModule,
        TranslateModule,
        OtButtonModule,
        OtLoadingModule,
        OneVideoModule,
    ],
    providers: [
        WelcomeTilesService,
    ],
    declarations: [
        WelcomePageComponent,
        WelcomeDetailComponent,
    ],
})
export class WelcomeModule {}
