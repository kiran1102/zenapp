export enum WelcomeModules {
    AA = "assessments",
    GRA = "readiness-assessments",
    DM = "data-mapping",
    CC = "cookie-consent",
    CM = "consent",
    DSAR = "subject-rights-request",
    IBM = "incident-breach",
    VRM = "vendor-risk",
}
