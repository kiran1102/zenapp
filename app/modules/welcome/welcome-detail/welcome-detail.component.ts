// Core
import { BehaviorSubject } from "rxjs";
import {
    Component,
    ElementRef,
    OnInit,
} from "@angular/core";
import { ActivatedRoute } from "@angular/router";

// Services
import { GlobalSidebarService } from "modules/global-sidebar/services/one-global-sidebar.service";
import { WelcomeTilesService } from "../services/welcome-content.service";

// Interfaces
import { IWelcomeTileAndDetailContent } from "modules/welcome/interfaces/welcome-module.interface";

// Enums
import { WelcomeModules } from "modules/welcome/enums/welcome.enum";

// Assets
import "images/welcome-detail/welcome-detail-background.svg";

@Component({
    selector: "welcome-detail",
    templateUrl: "./welcome-detail.component.html",
    host: {
        class: "full-size flex-full-height flex-column-nowrap overflow-y-auto",
    },
})
export class WelcomeDetailComponent implements OnInit {
    welcomeDetail = new BehaviorSubject<IWelcomeTileAndDetailContent>(null);

    constructor(
        private elRef: ElementRef,
        private route: ActivatedRoute,
        private globalSidebarService: GlobalSidebarService,
        private welcomeTilesService: WelcomeTilesService,
    ) {}

    ngOnInit() {
        this.globalSidebarService.set();
        this.route.params.subscribe((params) => {
            const pathModule = params["module"] as WelcomeModules;
            this.welcomeDetail.next(this.welcomeTilesService.getWelcomeDetailContent(pathModule));
        });
    }

    scrollDown() {
        this.elRef.nativeElement.scrollTop = this.elRef.nativeElement.scrollHeight;
    }
}
