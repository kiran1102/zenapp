import {
    async,
    ComponentFixture,
    TestBed,
} from "@angular/core/testing";
import { RouterTestingModule } from "@angular/router/testing";
import { TranslateModule } from "@ngx-translate/core";

import { OtButtonModule } from "@onetrust/vitreus";
import { OneVideoModule } from "../../video/video.module";

import { WelcomeDetailComponent } from "./welcome-detail.component";

describe("WelcomeDetailComponent", () => {
    let fixture: ComponentFixture<WelcomeDetailComponent>;
    let component: WelcomeDetailComponent;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                RouterTestingModule,
                TranslateModule,
                OtButtonModule,
                OneVideoModule,
            ],
            declarations: [WelcomeDetailComponent],
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(WelcomeDetailComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it("should create", () => {
        expect(component).toBeTruthy();
    });

});
