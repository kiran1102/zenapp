import { WelcomeModule } from "../welcome/welcome.module";

describe("WelcomeModule", () => {
    let notFoundModule: WelcomeModule;

    beforeEach(() => {
        notFoundModule = new WelcomeModule();
    });

    it("should create an instance", () => {
        expect(notFoundModule).toBeTruthy();
    });
});
