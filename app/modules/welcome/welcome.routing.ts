import { ModuleWithProviders } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { WelcomePageComponent } from "./welcome-page/welcome-page.component";
import { WelcomeDetailComponent } from "./welcome-detail/welcome-detail.component";

const appRoutes: Routes = [
    { path: "", component: WelcomePageComponent },
    { path: ":module", component: WelcomeDetailComponent },
];

export const routing: ModuleWithProviders = RouterModule.forChild(appRoutes);
