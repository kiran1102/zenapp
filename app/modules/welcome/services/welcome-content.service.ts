// Angular
import { Injectable } from "@angular/core";

// Enums
import { WelcomeModules } from "modules/welcome/enums/welcome.enum";
import { InventoryTableIds } from "enums/inventory.enum";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

// Constants
import { InventorySchemaIds } from "constants/inventory-config.constant";

// Interfaces
import {
    IWelcomeTileAndDetailContent,
    IWelcomeUpgradeCard,
} from "modules/welcome/interfaces/welcome-module.interface";

// Services
import { Permissions } from "modules/shared/services/helper/permissions.service";
import { McmModalService } from "modules/mcm/mcm-modal.service";

@Injectable()
export class WelcomeTilesService {

    constructor(
        private translatePipe: TranslatePipe,
        private permissions: Permissions,
        private mcmModalService: McmModalService,
    ) {}

    getWelcomeDetailContent(module: WelcomeModules) {
        switch (module) {
            case (WelcomeModules.AA):
                return this.getAA();

            case (WelcomeModules.GRA):
                return this.getGRA();

            case (WelcomeModules.DM):
                return this.getDM();

            case (WelcomeModules.CC):
                return this.getCC();

            case (WelcomeModules.CM):
                return this.getCM();

            case (WelcomeModules.DSAR):
                return this.getDSAR();

            case (WelcomeModules.IBM):
                return this.getIBM();

            case (WelcomeModules.VRM):
                return this.getVRM();
        }
    }

    getWelcomeTiles(): IWelcomeTileAndDetailContent[] {
        const welcomeTiles: IWelcomeTileAndDetailContent[] = [];
        if (this.permissions.canShow("Assessments") || this.permissions.canShow("Projects")) {
            welcomeTiles.push(this.getAA());
        }

        if (this.permissions.canShow("ReadinessAssessmentV2")) {
            welcomeTiles.push(this.getGRA());
        }

        if (this.permissions.canShow("DataMappingOld") || this.permissions.canShow("DataMappingNew") || this.permissions.canShow("DMAssessmentV2")) {
            welcomeTiles.push(this.getDM());
        }

        if (this.permissions.canShow("Cookie")) {
            welcomeTiles.push(this.getCC());
        }

        if (this.permissions.canShow("ConsentReceiptWelcome")) {
            welcomeTiles.push(this.getCM());
        }

        if (this.permissions.canShow("SubjectRights")) {
            welcomeTiles.push(this.getDSAR());
        }

        if (this.permissions.canShow("IncidentBreachManagement")) {
            welcomeTiles.push(this.getIBM());
        }

        if (this.permissions.canShow("VendorRisk")) {
            welcomeTiles.push(this.getVRM());
        }
        return welcomeTiles;
    }

    getLockedTiles(): IWelcomeUpgradeCard[] {
        const lockedCards: IWelcomeUpgradeCard[] = [];
        if (this.permissions.canShow("ReadinessUpgrade")) {
            lockedCards.push(
                {
                    title: this.translatePipe.transform("GlobalReadiness"),
                    icon: "images/launcher/RA.svg",
                    action: () => this.mcmModalService.openMcmModal("Upgrade"),
                },
            );
        }

        if (this.permissions.canShow("AssessmentsUpgrade") || this.permissions.canShow("ProjectsUpgrade")) {
            lockedCards.push(
                {
                    title: this.translatePipe.transform("AssessmentAutomation"),
                    icon: "images/launcher/AA.svg",
                    action: () => this.mcmModalService.openMcmModal("Upgrade"),
                },
            );
        }

        if (this.permissions.canShow("DataMappingUpgrade")) {
            lockedCards.push(
                {
                    title: this.translatePipe.transform("DataMapping"),
                    icon: "images/launcher/DM.svg",
                    action: () => this.mcmModalService.openMcmModal("SettingsDataMappingUpgrade"),
                });
        }

        if (this.permissions.canShow("VendorRiskManagementUpgrade")) {
            lockedCards.push(
                {
                    title: this.translatePipe.transform("VendorManagement"),
                    icon: "images/launcher/VM.svg",
                    action: () => this.mcmModalService.openMcmModal("VendorRiskManagementUpgrade"),
                },
            );
        }

        if (this.permissions.canShow("IncidentBreachManagementUpgrade")) {
            lockedCards.push(
                {
                    title: this.translatePipe.transform("Incidents"),
                    icon: "images/launcher/IBM.svg",
                    action: () => this.mcmModalService.openMcmModal("IncidentBreachManagementUpgrade"),
                },
            );
        }

        if (this.permissions.canShow("ConsentReceiptUpgrade")) {
            lockedCards.push(
                {
                    title: this.translatePipe.transform("ConsentManagementAbbr"),
                    icon: "images/launcher/CM.svg",
                    action: () => this.mcmModalService.openMcmModal("ConsentReceiptUpgrade"),
                },
            );
        }

        if (this.permissions.canShow("CookieUpgrade")) {
            lockedCards.push(
                {
                    title: this.translatePipe.transform("CookieConsent"),
                    icon: "images/launcher/Cookies.svg",
                    action: () => this.mcmModalService.openMcmModal("Upgrade"),
                },
            );
        }

        if (this.permissions.canShow("DSARUpgrade")) {
            lockedCards.push(
                {
                    title: this.translatePipe.transform("DataSubjectRequests"),
                    icon: "images/launcher/DSAR.svg",
                    action: () => this.mcmModalService.openMcmModal("DSARUpgrade"),
                },
            );
        }
        return lockedCards;
    }

    private getAA(): IWelcomeTileAndDetailContent {
        let route;
        if (this.permissions.canShow("Assessments")) {
            route = "zen.app.pia.module.assessment.list";
        } else {
            route = "zen.app.pia.module.projects.list";
        }

        return {
            id: WelcomeModules.AA,
            welcomeDetailTitle: "WelcomeAATitle",
            title: this.translatePipe.transform("AssessmentAutomation"),
            subtitle: "WelcomeAASubtitle",
            description: "WelcomeAADescription",
            route,
            video: "https://onetrustprivacy.wistia.com/medias/3cdysk62hp",
            icon: "images/launcher/AA.svg",
        };
    }

    private getGRA(): IWelcomeTileAndDetailContent {
        return {
            id: WelcomeModules.GRA,
            title: this.translatePipe.transform("MaturityBenchmarking"),
            icon: "images/launcher/RA.svg",
            welcomeDetailTitle: "WelcomeGraTitle",
            subtitle: "WelcomeGRASubtitle",
            description: "WelcomeGRADescription",
            route: "zen.app.pia.module.globalreadiness.views.welcome",
            video: "https://onetrustprivacy.wistia.com/medias/evfcw9a0js",
        };
    }

    private getDM(): IWelcomeTileAndDetailContent {
        let params;
        let route;
        if (this.permissions.canShow("DataMappingAssessments")) {
            route = "zen.app.pia.module.datamap.views.assessment-old.list";
        } else if (this.permissions.canShow("DataMappingAssessmentsList") && !this.permissions.canShow("DMAssessmentV2")) {
            route = "zen.app.pia.module.datamap.views.assessment.list";
        } else if (this.permissions.canShow("DMAssessmentV2") && this.permissions.canShow("DataMappingInventoryNew")) {
            if (this.permissions.canShow("DataMappingInventoryAssets")) {
                route = "zen.app.pia.module.datamap.views.inventory.list";
                params = { Id: InventorySchemaIds.Assets };
            } else if (this.permissions.canShow("DataMappingInventoryProcessingActivity")) {
                route = "zen.app.pia.module.datamap.views.inventory.list";
                params = { Id: InventorySchemaIds.Processes };
            } else {
                route = "zen.app.pia.module.assessment.list";
            }
        } else {
            route = "zen.app.pia.module.datamap.views.assessmentV2";
        }

        return {
            id: WelcomeModules.DM,
            title: this.translatePipe.transform("DataMapping"),
            icon: "images/launcher/DM.svg",
            welcomeDetailTitle: "WelcomeDMTitle",
            subtitle: "WelcomeDMSubtitle",
            description: "WelcomeDMDescription",
            route,
            params,
            video: "https://onetrustprivacy.wistia.com/medias/qymcbhsksg",
        };
    }

    private getCC() {
        return {
            id: WelcomeModules.CC,
            title: this.translatePipe.transform("CookieConsent"),
            icon: "images/launcher/Cookies.svg",
            welcomeDetailTitle: "WelcomeCCTitle",
            subtitle: "WelcomeCCSubtitle",
            description: "WelcomeCCDescription",
            route: "zen.app.pia.module.cookiecompliance.views.websites",
            video: "https://onetrustprivacy.wistia.com/medias/t21yi5us4g",
        };
    }

    private getCM(): IWelcomeTileAndDetailContent {
        return {
            id: WelcomeModules.CM,
            title: this.translatePipe.transform("ConsentManagementAbbr"),
            icon: "images/launcher/CM.svg",
            welcomeDetailTitle: "WelcomeCRTitle",
            subtitle: "WelcomeCRSubtitle",
            description: "WelcomeCRDescription",
            route: "zen.app.pia.module.consentreceipt.views.dashboard",
            video: "https://onetrustprivacy.wistia.com/medias/wfz74lirj0",
        };
    }

    private getDSAR(): IWelcomeTileAndDetailContent {
        let route;
        if (this.permissions.canShow("DSARDashboard")) {
            route = "zen.app.pia.module.dsar.views.dashboard";
        } else if (this.permissions.canShow("DSARRequestQueue")) {
            route = "zen.app.pia.module.dsar.views.queue";
        } else {
            route = "";
        }

        return {
            id: WelcomeModules.DSAR,
            title: this.translatePipe.transform("DataSubjectRequests"),
            icon: "images/launcher/DSAR.svg",
            welcomeDetailTitle: "WelcomeDSARTitle",
            subtitle: "WelcomeDSARSubtitle",
            description: "WelcomeDSARDescription",
            route,
            video: "https://onetrustprivacy.wistia.com/medias/o4exyyxyio",
        };
    }

    private getIBM(): IWelcomeTileAndDetailContent {
        let route;
        if (this.permissions.canShow("IncidentBreachManagement")) {
            route = "zen.app.pia.module.incident.views.incident-register";
        } else {
            route = "zen.app.pia.module.projects.list";
        }

        return {
            id: WelcomeModules.IBM,
            title: this.translatePipe.transform("IncidentResponse"),
            icon: "images/launcher/IBM.svg",
            welcomeDetailTitle: "WelcomeIBMTitle",
            subtitle: "WelcomeIBMSubtitle",
            description: "WelcomeIBMDescription",
            route,
            video: "https://onetrustprivacy.wistia.com/medias/dx2p7akpy0",
        };
    }

    private getVRM(): IWelcomeTileAndDetailContent {
        let route;
        let params;
        if (this.permissions.canShow("VRMDashboard")) {
            route = "zen.app.pia.module.vendor.dashboard";
        } else {
            route = "zen.app.pia.module.vendor.inventory.list";
            params = { Id: InventoryTableIds.Vendors };
        }
        return {
            id: WelcomeModules.VRM,
            title: this.translatePipe.transform("VendorRiskManagement"),
            icon: "images/launcher/VM.svg",
            welcomeDetailTitle: "WelcomeVRTitle",
            subtitle: "WelcomeVRSubtitle",
            description: "WelcomeVRDescription",
            route,
            params,
            video: "https://onetrustprivacy.wistia.com/medias/6e9s4ngcei",
        };
    }
}
