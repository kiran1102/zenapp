// Angular
import {
    Component,
    OnInit,
} from "@angular/core";

// Services
import { GlobalSidebarService } from "modules/global-sidebar/services/one-global-sidebar.service";
import { WelcomeTilesService } from "../services/welcome-content.service";

// Interfaces
import {
    IWelcomeTileAndDetailContent,
    IWelcomeUpgradeCard,
} from "modules/welcome/interfaces/welcome-module.interface";

// Launcher Icons
import "images/launcher/AA.svg";
import "images/launcher/CM.svg";
import "images/launcher/Cookies.svg";
import "images/launcher/DM.svg";
import "images/launcher/DSAR.svg";
import "images/launcher/IBM.svg";
import "images/launcher/RA.svg";
import "images/launcher/VM.svg";

@Component({
    selector: "welcome-page",
    templateUrl: "./welcome-page.component.html",
    host: {
        class: "full-size flex-full-height column-nowrap overflow-y-auto background-grey",
    },
})
export class WelcomePageComponent implements OnInit {
    lockedCards: IWelcomeUpgradeCard[] = [];
    welcomeCards: IWelcomeTileAndDetailContent[];

    constructor(
        private globalSidebarService: GlobalSidebarService,
        private welcomeTilesService: WelcomeTilesService,
    ) {}

    ngOnInit() {
        this.globalSidebarService.set();
        this.getTiles();
        this.getLockedTiles();
    }

    triggerItemAction(item: IWelcomeTileAndDetailContent) {
        if ((item.action) instanceof Function) {
            item.action();
        }
    }

    private getLockedTiles() {
        this.lockedCards = this.welcomeTilesService.getLockedTiles();
    }

    private getTiles() {
        this.welcomeCards = this.welcomeTilesService.getWelcomeTiles();
    }
}
