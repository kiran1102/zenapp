// Angular
import {
    Component,
    Input,
    Output,
    EventEmitter,
} from "@angular/core";
import {
    FormBuilder,
    FormGroup,
    Validators,
} from "@angular/forms";

// 3rd Party
import {
    Transition,
    TransitionService,
    StateService,
    HookResult,
} from "@uirouter/core";

// Rxjs
import {
    Subject,
    combineLatest,
} from "rxjs";
import {
    takeUntil,
    take,
} from "rxjs/operators";

// Interfaces
import {
    ILinkedControlDetail,
    ICreateData,
    IUpdateLinkControlPayload,
} from "linkControlsAPIModule/interfaces/link-controls.interface";
import { ICategoryInformation } from "modules/controls/shared/interfaces/controls.interface";
import { IStringMap } from "interfaces/generic.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";

// Services
import { LinkControlsAPIService } from "modules/link-controls/link-controls-api/services/link-controls-api.service";
import { LinkControlsService } from "modules/link-controls/link-controls-api/services/link-controls.service";

// Constants
import { FormDataTypes } from "constants/form-types.constant";
import {
    ControlsTabColumnNames,
    EmptyData,
} from "modules/link-controls/link-controls-api/constants/link-controls.constant";

// Vitreus
import {
    OtModalService,
    ModalSize,
} from "@onetrust/vitreus";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

// Modal Component
import { UnsavedChangesConfirmationModal } from "sharedModules/components/unsaved-changes-confirmation-modal/unsaved-changes-confirmation-modal.component";

@Component({
    selector: "link-controls-detail",
    templateUrl: "./link-controls-detail.component.html",
})
export class LinkControlsDetailComponent {

    @Input() editMode: boolean;
    @Input() recordType: string;
    @Output() toggleEdit = new EventEmitter<boolean>();

    controlForm: FormGroup;
    hasEdits = false;
    isSaving = false;
    controlsTabColumnNames = ControlsTabColumnNames;
    formDataTypes = FormDataTypes;
    formData: ICreateData[] = [];
    control: ILinkedControlDetail;
    maturities: ICategoryInformation[];
    filteredMaturities: ICategoryInformation[];
    selectedMaturity: ICategoryInformation;
    statusOptions: ICategoryInformation[];
    selectedStatusOption: ICategoryInformation;
    emptyData = EmptyData;

    private transitionDeregisterHook: () => any;
    private destroy$ = new Subject();

    constructor(
        private fb: FormBuilder,
        private linkControlsAPIService: LinkControlsAPIService,
        private linkControlsService: LinkControlsService,
        private translatePipe: TranslatePipe,
        private otModalService: OtModalService,
        private stateService: StateService,
        private $transitions: TransitionService,
    ) { }

    ngOnInit() {
        combineLatest(
            this.linkControlsService.linkControlDetail$,
            this.linkControlsService.linkControlMaturityOptions$,
            this.linkControlsService.linkControlStatusOptions$,
        ).pipe(
            takeUntil(this.destroy$),
        ).subscribe(([control, controlMaturities, controlStatus]) => {
            if (control) {
                this.control = control;
                this.hasEdits = false;
                this.generateControlFormGroup();
                this.onFormChanges();
                this.setDefaultMaturity();
                this.setDefaultStatus();
            }
            if (controlMaturities) {
                this.maturities = controlMaturities;
                this.filteredMaturities = this.maturities.map((m) => {
                    return {
                        id: m.id,
                        name: m.nameKey ? this.translatePipe.transform(m.nameKey) : m.name,
                        nameKey: m.nameKey,
                    };
                });
                this.filteredMaturities.unshift({ id: "", name: this.emptyData, nameKey: "" });
            }
            if (controlStatus) {
                const validStatus = controlStatus.filter(Boolean);
                this.statusOptions = validStatus.map((status) => {
                    return {
                        nameKey: status,
                        name: this.translatePipe.transform(status),
                        id: null,
                    };
                });
            }
            this.generateFormData();
        });
        this.setTransitionAwayHook();
    }

    ngOnDestroy() {
        this.linkControlsService.reset();
        this.destroy$.next();
        this.destroy$.complete();
        if (this.transitionDeregisterHook) {
            this.transitionDeregisterHook();
        }
    }

    generateFormData() {
        this.formData = [
            {
                name: ControlsTabColumnNames.ControlId,
                label: "ControlID",
                type: FormDataTypes.INPUT,
                isRequired: false,
                formControlName: "controlIdentifier",
                readOnly: true,
                isChildObject: true,
                childObjectRef: "control",
            },
            {
                name: ControlsTabColumnNames.Name,
                label: "Name",
                type: FormDataTypes.TEXTAREA,
                isRequired: false,
                formControlName: "name",
                readOnly: true,
                isChildObject: true,
                childObjectRef: "control",
            },
            {
                name: ControlsTabColumnNames.FrameWork,
                label: "Framework",
                type: FormDataTypes.LOOKUP,
                isRequired: false,
                formControlName: "frameworkName",
                readOnly: true,
                isChildObject: true,
                childObjectRef: "control",
            },
            {
                name: ControlsTabColumnNames.Category,
                label: "Category",
                type: FormDataTypes.LOOKUP,
                isRequired: false,
                formControlName: "categoryName",
                readOnly: true,
                isChildObject: true,
                childObjectRef: "control",
            },
            {
                name: ControlsTabColumnNames.Description,
                label: "Description",
                type: FormDataTypes.TEXTAREA,
                isRequired: false,
                formControlName: "description",
                readOnly: true,
                isChildObject: true,
                childObjectRef: "control",
            },
            {
                name: ControlsTabColumnNames.Maturity,
                label: "Maturity",
                type: FormDataTypes.PICKLIST,
                isRequired: false,
                formControlName: "maturityNameKey",
                labelKey: "name",
                valueKey: "nameKey",
                options: this.filteredMaturities,
                id: "LinkControlsDetailsTabMaturityPicklist",
                readOnly: false,
            },
            {
                name: ControlsTabColumnNames.Status,
                label: "Status",
                type: FormDataTypes.PICKLIST,
                isRequired: false,
                formControlName: "status",
                labelKey: "name",
                valueKey: "nameKey",
                options: this.statusOptions,
                id: "LinkControlsDetailsTabStatusPicklist",
                readOnly: false,
            },
            {
                name: ControlsTabColumnNames.Note,
                label: "Notes",
                type: FormDataTypes.TEXTAREA,
                isRequired: false,
                formControlName: "note",
                rows: "2",
                id: "LinkControlsDetailsTabNotesTextarea",
                readOnly: false,
            },
        ];
    }

    onFormChanges(): void {
        this.controlForm.valueChanges
            .pipe(take(1))
            .subscribe((val) => {
                this.hasEdits = true;
            });
    }

    get controlMaturity() { return this.controlForm.get("maturityNameKey"); }
    get controlStatus() { return this.controlForm.get("status"); }

    onDropdownChange(formControlName: string, event: ICategoryInformation) {
        this.hasEdits = true;
        if (formControlName === "maturityNameKey") {
            this.control["maturityId"] = event.id;
            this.controlMaturity.setValue(event);
            this.selectedMaturity = event;
        } else if (formControlName === "status") {
            this.controlStatus.setValue(event);
            this.selectedStatusOption = event;
        }
    }

    onToggleEdit(readOnly = false) {
        if (!readOnly) {
            this.editMode = true;
            this.hasEdits = false;
            this.toggleEdit.emit(this.editMode);
        }
    }

    onSave(callback?: () => void) {
        if (this.controlForm.valid && this.hasEdits && !this.isSaving) {
            this.isSaving = true;
            const values = this.controlForm.value;
            const updateControlPayload: IUpdateLinkControlPayload = {
                maturityId: this.control.maturityId || null,
                status: this.selectedStatusOption.nameKey || null,
                note: values.note ? values.note.trim() : null,
            };
            this.linkControlsAPIService.updateLinkControlDetail(this.control.id, updateControlPayload)
                .pipe(takeUntil(this.destroy$))
                .subscribe((response: IProtocolResponse<string>) => {
                    if (response.result) {
                        this.linkControlsService.getControlDetail(this.recordType, this.control.id);
                        if (callback) {
                            callback();
                        }
                    }
                    this.editMode = false;
                    this.hasEdits = false;
                    this.isSaving = false;
                });
        }
    }

    cancelAction() {
        if (!this.isSaving) {
            this.editMode = false;
            this.hasEdits = false;
            this.setDefaultMaturity();
            this.setDefaultStatus();
            this.generateControlFormGroup();
            this.onFormChanges();
            this.toggleEdit.emit(this.editMode);
        }
    }

    private generateControlFormGroup() {
        this.controlForm = this.fb.group({
            maturityNameKey: [this.control.maturityId ? {
                id: this.control.maturityId,
                name: this.translatePipe.transform(this.control.maturityName),
                nameKey: this.control.maturityNameKey,
            } : null],
            status: [this.control.status ? {
                id: this.control.status,
                name: this.translatePipe.transform(this.control.status),
                nameKey: this.control.status,
            } : null],
            note: [this.control.note, [Validators.maxLength(500)]],
        });
    }

    private handleStateChange(toState: IStringMap<any>, toParams: IStringMap<any>, options: IStringMap<any>): boolean {
        if (!this.hasEdits) return true;
        const callback = () => {
            this.stateService.go(toState, toParams, options);
        };
        this.launchSaveChangesModal(callback);
        return false;
    }

    private launchSaveChangesModal(callback: () => void): void {
        this.otModalService
            .create(
                UnsavedChangesConfirmationModal,
                {
                    isComponent: true,
                    size: ModalSize.SMALL,
                    hideClose: true,
                },
                {
                    promiseToResolve: () => {
                        this.onSave(callback);
                    },
                    discardCallback: () => {
                        this.cancelAction();
                        callback();
                    },
                },
            );
    }

    private setTransitionAwayHook(): void {
        this.transitionDeregisterHook = this.$transitions.onExit({ from: "zen.app.pia.module.datamap.views.link-controls.details" }, (transition: Transition): HookResult => {
            return this.handleStateChange(transition.to(), transition.params("to"), transition.options);
        }) as any;
    }

    private setDefaultMaturity() {
        if (this.control.maturityId || this.control.maturityName) {
            this.selectedMaturity = {
                id: this.control.maturityId,
                name: this.control.maturityName,
                nameKey: this.control.maturityNameKey,
            };
        }
    }

    private setDefaultStatus() {
        if (this.control.status) {
            this.selectedStatusOption = {
                id: this.control.status || null,
                name: this.translatePipe.transform(this.control.status),
                nameKey: this.control.status,
            };
        }
    }
}
