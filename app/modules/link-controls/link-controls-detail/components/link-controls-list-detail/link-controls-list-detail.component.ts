// Angular
import {
    Component,
    OnInit,
} from "@angular/core";

// Rxjs
import { takeUntil } from "rxjs/operators";
import { Subject } from "rxjs";

// Interfaces
import { ILinkedControlDetail } from "linkControlsAPIModule/interfaces/link-controls.interface";

// Constants & Enums
import { InventoryDetailsTabs } from "constants/inventory.constant";
import { LegacyInventorySchemaIds } from "constants/inventory-config.constant";
import {
    LinkControlDetailTabOptions,
    EntityListRoutes,
    EntityDetailRoutes,
    LinkControlsEntityTranslationKey,
} from "linkControlsAPIModule/constants/link-controls.constant";

// Services
import { StateService } from "@uirouter/core";
import { LinkControlsService } from "linkControlsAPIModule/services/link-controls.service";

// Interfaces
import { ITabsNav } from "interfaces/one-tabs-nav.interface";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

@Component({
    selector: "link-controls-list-detail",
    templateUrl: "./link-controls-list-detail.component.html",
})
export class LinkControlsListDetailComponent implements OnInit {

    controlId: string;
    recordType: string;
    recordId: string;
    linkControlDetail: ILinkedControlDetail;
    isLoading = true;
    selectedTab = LinkControlDetailTabOptions.Detail;
    controlDetailTabs = LinkControlDetailTabOptions;
    linkControlsEntityTranslationKey = LinkControlsEntityTranslationKey;
    entityListRoutes = EntityListRoutes;
    entityDetailRoutes = EntityDetailRoutes;
    tabOptions: ITabsNav[] = [];
    editMode: boolean;

    private destroy$ = new Subject();

    constructor(
        readonly linkControlsService: LinkControlsService,
        private stateService: StateService,
        private translatePipe: TranslatePipe,
    ) { }

    ngOnInit() {
        this.setTabOptions();
        this.controlId = this.stateService.params.controlId || null;
        this.recordType = this.stateService.params.recordType || null;
        this.recordId = this.stateService.params.recordId || null;
        this.editMode = this.stateService.params.editMode === "true" || false;
        this.linkControlsService.getMaturitiesOptions();
        this.linkControlsService.getStatusOptions();
        this.linkControlsService.getControlDetail(this.recordType, this.controlId);
        this.linkControlsService.linkControlDetail$
            .pipe(takeUntil(this.destroy$))
            .subscribe((control: ILinkedControlDetail) => {
                this.linkControlDetail = control;
                this.isLoading = false;
            });
    }

    ngOnDestroy() {
        this.linkControlsService.reset();
        this.destroy$.next();
        this.destroy$.complete();
    }

    onHeaderRouteClick(route: { path: string, params: any }) {
        switch (route.path) {
            case "zen.app.pia.module.datamap.views.inventory.list":
                this.stateService.go(route.path, { Id: this.recordType, page: null, size: null });
                break;
            case "zen.app.pia.module.datamap.views.inventory.details":
                this.stateService.go(route.path, { recordId: this.recordId, inventoryId: LegacyInventorySchemaIds[this.recordType], tabId: InventoryDetailsTabs.Controls});
                break;
        }
    }

    onTabClick(option: ITabsNav) {
        this.selectedTab = option.id;
    }

    setEditMode(editMode: boolean) {
        this.editMode = editMode;
    }

    private setTabOptions() {
        this.tabOptions.push({
            text: this.translatePipe.transform("Details"),
            id: LinkControlDetailTabOptions.Detail,
            otAutoId: "LinkControlsListDetails",
        });
    }
}
