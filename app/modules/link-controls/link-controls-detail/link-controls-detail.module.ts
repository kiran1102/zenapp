// angular
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

// modules
import { LinkControlsAPIModule } from "linkControlsAPIModule/link-controls-api.module";
import { SharedModule } from "sharedModules/shared.module";
import { ControlsSharedModule } from "controls/shared/controls-shared.module";
import { HttpClientModule } from "@angular/common/http";
import { FormsModule } from "@angular/forms";
import { ReactiveFormsModule } from "@angular/forms";
import { VitreusModule } from "@onetrust/vitreus";
import { OTPipesModule, PIPES } from "modules/pipes/pipes.module";
import { VirtualScrollModule } from "od-virtualscroll";

// Components
import { LinkControlsDetailComponent } from "linkControlsDetailModule/components/link-controls-detail/link-controls-detail.component";
import { LinkControlsListDetailComponent } from "linkControlsDetailModule/components/link-controls-list-detail/link-controls-list-detail.component";

@NgModule({
    imports: [
        LinkControlsAPIModule,
        SharedModule,
        CommonModule,
        ControlsSharedModule,
        HttpClientModule,
        FormsModule,
        ReactiveFormsModule,
        VitreusModule,
        OTPipesModule,
        VirtualScrollModule,
    ],
    declarations: [
        LinkControlsDetailComponent,
        LinkControlsListDetailComponent,
    ],
    exports: [
        LinkControlsListDetailComponent,
    ],
    entryComponents: [
        LinkControlsListDetailComponent,
    ],
    providers: [
        PIPES, // TODO: Remove from providers when ot-common is implemented. Only necessary if you are using pipe in TS file, else just module is fine
    ],
})
export class LinkControlsDetailModule { }
