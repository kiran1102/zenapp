
// 3rd Party
import { Component, OnInit, OnDestroy } from "@angular/core";
import { StateService } from "@uirouter/core";

// rxjs
import { Subject, combineLatest } from "rxjs";
import { takeUntil } from "rxjs/operators";

// vitreus
import { IOtModalContent } from "@onetrust/vitreus";

// Interfaces
import { IProtocolResponse } from "interfaces/protocol.interface";
import { IStringMap } from "interfaces/generic.interface";
import {
    IControlsLibraryResponse,
    IFrameworkListInformation,
} from "controls/shared/interfaces/controls-library.interface";
import {
    ILinkControlsModalData,
    IControlsLibrarySelection,
    IFrameworkOption,
} from "linkControlsAPIModule/interfaces/link-controls.interface";

// Services
import { LinkControlsService } from "linkControlsAPIModule/services/link-controls.service";
import { ControlsLibraryService } from "controls/shared/services/controls-library.service";

@Component({
    selector: "link-controls-modal",
    templateUrl: "./link-controls-modal.component.html",
})
export class LinkControlsModalComponent implements IOtModalContent, OnInit, OnDestroy {
    otModalData: ILinkControlsModalData;
    otModalCloseEvent: Subject<boolean>;

    allSelected = false;
    partiallySelected = false;
    controls: string[];
    controlsMap: IStringMap<IControlsLibrarySelection>;
    frameworkControlsMap: IStringMap<string[]>;
    frameworks: IFrameworkOption[];
    frameworksCached: IFrameworkOption[];
    isSaving = false;
    loading = true;
    orgGroupId: string;
    resetSearch$ = new Subject();
    searchFrameworksModel: string;
    searchControlsModel: string;
    selectedControls: { [key: string]: string } = {};
    selectedFramework: IFrameworkListInformation;
    showSelectAll = true;

    private destroy$ = new Subject();

    constructor(
        private linkControlsService: LinkControlsService,
        private controlsLibraryService: ControlsLibraryService,
        private stateService: StateService,
    ) { }

    ngOnInit() {
        combineLatest(
            this.controlsLibraryService.controlsLibrary$,
            this.controlsLibraryService.controlsFrameworks$,
        ).pipe(
            takeUntil(this.destroy$),
        ).subscribe(([allControls, frameworkOptions]) => {
            if (allControls && frameworkOptions) {
                if (allControls.length && frameworkOptions) {
                    this.frameworks = frameworkOptions
                        .concat({
                            frameworkId: "noFramework",
                            frameworkName: "noFramework",
                            frameworkNameKey: "NoFramework",
                        })
                        .map((f: IFrameworkOption) => ({ ...f, count: 0 }));
                    this.frameworksCached = this.frameworks;
                    const { frameworkControlsMap, controlsMap } = this.createControlsMap(allControls);
                    this.frameworkControlsMap = frameworkControlsMap;
                    this.controlsMap = controlsMap;
                    this.selectFramework(this.frameworks[0]);
                }
                this.loading = false;
            }
        });

        this.controlsLibraryService.getFrameworksFromControlLibrary();
        this.controlsLibraryService.controlsSearchName$.next("");
        this.controlsLibraryService.retrieveControlsLibrary([{ field: "status", operator: "NOT_EQUAL_TO", value: ["Archived"] }], { page: 0, size: 2000, sort: null });
    }

    ngOnDestroy() {
        this.resetSearch$.next();
        this.destroy$.next();
        this.destroy$.complete();
    }

    createControlsMap(controls: IControlsLibraryResponse[]): { frameworkControlsMap: IStringMap<string[]>, controlsMap: IStringMap<IControlsLibrarySelection> } {
        const frameworkControlsMap = { noFramework: [] };
        const controlsMap = {};
        for (let i = 0; i < controls.length; i++) {
            const frameworkId = controls[i].frameworkId;
            if (frameworkId) {
                frameworkControlsMap[frameworkId] = frameworkControlsMap[frameworkId]
                    ? frameworkControlsMap[frameworkId].concat(controls[i].id)
                    : [controls[i].id];
            } else {
                frameworkControlsMap.noFramework.push(controls[i].id);
            }
            controlsMap[controls[i].id] = { ...controls[i], isSelected: false };
        }
        return { frameworkControlsMap, controlsMap };
    }

    onSave() {
        this.isSaving = true;
        const controlIds = Object.values(this.controlsMap)
            .filter((control) => control.isSelected)
            .map((control) => control.id);

        if (this.otModalData.linkControl$ && this.otModalData.linkControlSource) {
            this.otModalData.linkControl$.subscribe((success: boolean) => {
                if (success) {
                    this.otModalCloseEvent.next(true);
                } else {
                    this.isSaving = false;
                }
            });
            this.otModalData.linkControlSource.next(controlIds);
        } else {
            this.linkControlsService.linkControlsToRecord(this.otModalData.recordType, this.otModalData.recordId, controlIds)
                .subscribe((res: IProtocolResponse<null>) => {
                    if (res.result) {
                        this.linkControlsService.retrieveLinkControls(this.otModalData.recordType, this.otModalData.recordId, this.otModalData.query);
                        this.otModalCloseEvent.next();
                    } else {
                        this.isSaving = false;
                    }
                });
        }
    }

    closeModal() {
        this.otModalCloseEvent.next();
    }

    onSearchFrameworks(value: string) {
        this.searchFrameworksModel = value ? value.trim() : "";

        if (!this.searchFrameworksModel) {
            this.frameworks = this.frameworksCached;
        } else {
            this.frameworks = this.frameworksCached.filter((f) => f.frameworkName.toLowerCase().indexOf(value.trim().toLowerCase()) !== -1);
        }
    }

    onSearchControls(value: string) {
        this.searchControlsModel = value ? value.trim() : "";
        const { controlIds, controlModels } = this.getAllControlsByFrameworkId(this.selectedFramework.frameworkId);
        const selectedFrameworkControls = this.countSelectedControls(controlModels);
        this.allSelected = selectedFrameworkControls === controlIds.length;
        this.partiallySelected = !this.allSelected && !!selectedFrameworkControls;

        if (!this.searchControlsModel) {
            this.controls = controlIds;
        } else {
            this.controls = controlModels
                .filter((control) => {
                    return control.name.toLowerCase().indexOf(value.trim().toLowerCase()) !== -1
                        || control.identifier.toLowerCase().indexOf(value.trim().toLowerCase()) !== -1;
                })
                .map((control) => control.id);
        }
    }

    selectFramework(framework: IFrameworkListInformation) {
        if (framework) {
            if (!this.selectedFramework || this.selectedFramework.frameworkId !== framework.frameworkId) {
                this.selectedFramework = framework;
                this.onSearchControls(this.searchControlsModel);
            }
        }
    }

    getAllControlsByFrameworkId(frameworkId: string): { controlIds: string[], controlModels: IControlsLibrarySelection[] } {
        const controlIds = this.frameworkControlsMap[frameworkId];
        if (controlIds) {
            const controlModels = controlIds
                .map((id) => this.controlsMap[id])
                .sort((a, b) => {
                    if (a.identifier < b.identifier) { return -1; }
                    if (a.identifier > b.identifier) { return 1; }
                    return 0;
                });
            const sortedControlIds = controlModels.map((model) => model.id);
            return { controlIds: sortedControlIds, controlModels };
        } else {
            return { controlIds: [], controlModels: [] };
        }
    }

    getControlIdsByFramework(frameworkId: string): string[] {
        return frameworkId ? this.frameworkControlsMap[frameworkId] : this.frameworkControlsMap["noFramework"];
    }

    toggleAll(isSelected: boolean) {
        this.controls.forEach((controlId) => {
            this.controlsMap[controlId] = {
                ...this.controlsMap[controlId],
                isSelected,
            };
        });

        const { controlIds } = this.getAllControlsByFrameworkId(this.selectedFramework.frameworkId);

        this.frameworks = this.frameworks.map((f) => {
            if (this.selectedFramework.frameworkId === f.frameworkId) {
                f.count = isSelected ? controlIds.length : 0;
            }
            return f;
        });

        this.allSelected = isSelected;
        this.partiallySelected = false;
    }

    toggleControl(isSelected: boolean, id: string) {
        this.controlsMap = {
            ...this.controlsMap,
            [id]: { ...this.controlsMap[id], isSelected },
        };

        this.frameworks = this.frameworks.map((f) => {
            if (this.selectedFramework.frameworkId === f.frameworkId) {
                f.count = isSelected ? f.count + 1 : f.count - 1;
            }
            return f;
        });

        const { controlIds, controlModels } = this.getAllControlsByFrameworkId(this.selectedFramework.frameworkId);
        const selectedFrameworkControls = this.countSelectedControls(controlModels);
        this.allSelected = selectedFrameworkControls === controlIds.length;
        this.partiallySelected = !this.allSelected && !!selectedFrameworkControls;
    }

    countSelectedControls(controlModels?: IControlsLibrarySelection[]): number {
        if (controlModels) {
            return controlModels.filter((control) => control.isSelected).length;
        } else {
            return Object.values(this.controlsMap).filter((control) => control.isSelected).length;
        }
    }

    closeAndRouteToAddFrameworks() {
        this.otModalCloseEvent.next();
        return this.stateService.go("zen.app.pia.module.risks.views.controls.library");
    }

    isAnySelected(): boolean {
        if (this.controlsMap) {
            const controlIds = Object.values(this.controlsMap)
                .filter((control) => control.isSelected)
                .map((control) => control.id);
            return !!(controlIds && controlIds.length);
        }
    }
}
