// angular
import { NgModule } from "@angular/core";

// modules
import { LinkControlsAPIModule } from "linkControlsAPIModule/link-controls-api.module";
import { SharedModule } from "sharedModules/shared.module";
import { CommonModule } from "@angular/common";
import { HttpClientModule } from "@angular/common/http";
import { FormsModule } from "@angular/forms";
import { ReactiveFormsModule } from "@angular/forms";
import { VitreusModule } from "@onetrust/vitreus";
import { DndListModule } from "ngx-drag-and-drop-lists";
import { OTPipesModule, PIPES } from "modules/pipes/pipes.module";
import { VirtualScrollModule } from "od-virtualscroll";
import { OneVideoModule } from "modules/video/video.module";

// Components
import { LinkControlsModalComponent } from "linkControlsModalModule/components/link-controls-modal/link-controls-modal.component";

@NgModule({
    imports: [
        LinkControlsAPIModule,
        CommonModule,
        SharedModule,
        CommonModule,
        HttpClientModule,
        FormsModule,
        ReactiveFormsModule,
        VitreusModule,
        DndListModule,
        OTPipesModule,
        VirtualScrollModule,
        OneVideoModule,
    ],
    declarations: [
        LinkControlsModalComponent,
    ],
    exports: [
        LinkControlsModalComponent,
    ],
    entryComponents: [
        LinkControlsModalComponent,
    ],
    providers: [
        PIPES, // TODO: Remove from providers when ot-common is implemented. Only necessary if you are using pipe in TS file, else just module is fine
    ],
})
export class LinkControlsModalModule { }
