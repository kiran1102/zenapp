// angular
import { NgModule } from "@angular/core";

// modules
import { LinkControlsAPIModule } from "linkControlsAPIModule/link-controls-api.module";
import { LinkControlsModalModule } from "linkControlsModalModule/link-controls-modal.module";
import { SharedModule } from "sharedModules/shared.module";
import { CommonModule } from "@angular/common";
import { HttpClientModule } from "@angular/common/http";
import { FormsModule } from "@angular/forms";
import { ReactiveFormsModule } from "@angular/forms";
import { VitreusModule } from "@onetrust/vitreus";
import { DndListModule } from "ngx-drag-and-drop-lists";
import { OTPipesModule, PIPES } from "modules/pipes/pipes.module";
import { VirtualScrollModule } from "od-virtualscroll";
import { OneVideoModule } from "modules/video/video.module";
import { ControlsSharedModule } from "controls/shared/controls-shared.module";

// Components
import { LinkControlsTableComponent } from "linkControlsListModule/components/link-controls-table/link-controls-table.component";
import { LinkControlsListComponent } from "linkControlsListModule/components/link-controls-list/link-controls-list.component";

@NgModule({
    imports: [
        SharedModule,
        CommonModule,
        HttpClientModule,
        FormsModule,
        ReactiveFormsModule,
        VitreusModule,
        DndListModule,
        OTPipesModule,
        VirtualScrollModule,
        OneVideoModule,
        LinkControlsAPIModule,
        LinkControlsModalModule,
        ControlsSharedModule,
    ],
    declarations: [
        LinkControlsListComponent,
        LinkControlsTableComponent,
    ],
    exports: [
        LinkControlsListComponent,
    ],
    entryComponents: [
        LinkControlsListComponent,
    ],
    providers: [
        PIPES, // TODO: Remove from providers when ot-common is implemented. Only necessary if you are using pipe in TS file, else just module is fine
    ],
})
export class LinkControlsListModule { }
