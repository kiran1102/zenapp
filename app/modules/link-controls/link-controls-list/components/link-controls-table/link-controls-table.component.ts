// Core
import {
    Component,
    Input,
    OnInit,
    OnDestroy,
    Output,
    EventEmitter,
} from "@angular/core";

// Rxjs
import { Subject } from "rxjs";
import {
    takeUntil,
    filter,
} from "rxjs/operators";

// Vitreus
import {
    OtModalService,
    ConfirmModalType,
} from "@onetrust/vitreus";
import { ContextMenuType } from "@onetrust/vitreus";

// Interfaces
import { IDropdownOption } from "interfaces/one-dropdown.interface";
import { IDataTableColumn } from "interfaces/tables.interface";
import { IStringMap } from "interfaces/generic.interface";
import {
    ILinkedControlsTable,
    ILinkedControlInformation,
    IDeleteControlsModalData,
    IControlFilter,
} from "linkControlsAPIModule/interfaces/link-controls.interface";
import { IPageableMeta } from "interfaces/pagination.interface";

// Services
import { Permissions } from "modules/shared/services/helper/permissions.service";
import { LinkControlsService } from "linkControlsAPIModule/services/link-controls.service";
import { StateService } from "@uirouter/core";

// Enums
import { TableColumnTypes } from "enums/data-table.enum";

// Constants
import {
    ControlPermissions,
    DeleteControlConfirmDesc,
    ControlsStatusColorMap,
} from "linkControlsAPIModule/constants/link-controls.constant";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

@Component({
    selector: "link-controls-table",
    templateUrl: "./link-controls-table.component.html",
})
export class LinkControlsTableComponent implements OnInit, OnDestroy {

    @Input() table: ILinkedControlsTable;
    @Input() tableMetaData: IPageableMeta;
    @Input() recordType: string;
    @Input() recordId: string;
    @Input() query: IControlFilter[];

    @Output() tableActions = new EventEmitter<{ sortKey: string, sortOrder: string }>();

    tableColumnTypes = TableColumnTypes;
    controlsStatusColorMap = ControlsStatusColorMap;
    contextMenuType = ContextMenuType;
    sortKey = "";
    sortOrder = "";
    sortable = true;
    menuOptions: IDropdownOption[] = [];
    emptyData = "- - - -";
    controlsPermissions: IStringMap<boolean>;

    private destroy$ = new Subject();

    constructor(
        private translatePipe: TranslatePipe,
        private permissions: Permissions,
        private linkControlsService: LinkControlsService,
        private otModalService: OtModalService,
        private stateService: StateService,
    ) { }

    ngOnInit() {
        this.controlsPermissions = {
            canEditControl: this.permissions.canShow(ControlPermissions[this.recordType].editControl),
            canRemoveControl: this.permissions.canShow(ControlPermissions[this.recordType].removeControl),
        };
        this.sortKey = this.tableMetaData.sort[0].property;
        this.sortOrder = (this.tableMetaData.sort[0].direction as string).toLowerCase();
        this.generateMenuOptions();
    }

    ngOnDestroy() {
        this.destroy$.next();
        this.destroy$.complete();
    }

    columnTrackBy(index: number, column: IDataTableColumn): string {
        return column.id;
    }

    goToRecord(row: ILinkedControlInformation, editMode = false) {
        this.stateService.go("zen.app.pia.module.datamap.views.link-controls.details", { controlId: row.id, recordType: this.recordType, recordId: this.recordId, editMode});
    }

    handleSortChange({ sortKey, sortOrder }) {
        this.sortKey = sortKey;
        this.sortOrder = sortOrder;
        this.tableActions.emit({ sortKey, sortOrder });
    }

    private generateMenuOptions() {
        if (this.controlsPermissions.canEditControl) {
            this.menuOptions.push({
                text: this.translatePipe.transform("Edit"),
                action: (row: ILinkedControlInformation) => {
                    this.goToRecord(row, true);
                },
            });
        }
        if (this.controlsPermissions.canRemoveControl) {
            this.menuOptions.push({
                text: this.translatePipe.transform("Remove"),
                action: (row: ILinkedControlInformation) => {
                    this.openDeleteModal(row.control.id);
                },
            });
        }
    }

    private openDeleteModal(controlId: string) {
        const modalData: IDeleteControlsModalData = {
            controlId,
            recordType: this.recordType,
            recordId: this.recordId,
        };
        this.otModalService.confirm({
            type: ConfirmModalType.DELETE,
            translations: {
                title: this.translatePipe.transform("RemoveControls"),
                desc: this.translatePipe.transform(DeleteControlConfirmDesc[modalData.recordType]),
                confirm: this.translatePipe.transform("DoYouWantToContinue"),
                submit: this.translatePipe.transform("Remove"),
                cancel: this.translatePipe.transform("Cancel"),
            },
        }).pipe(
            takeUntil(this.destroy$),
            filter((res) => Boolean(res)),
        ).subscribe((res) => {
            this.linkControlsService.linkControlsLoading$.next(true);
            this.linkControlsService.deleteLinkControls(controlId, this.recordType, this.recordId, this.query);
        });
    }
}
