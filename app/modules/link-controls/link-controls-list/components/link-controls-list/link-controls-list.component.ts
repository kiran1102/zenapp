// Rxjs
import { Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";

// 3rd party
import {
    Component,
    Input,
    SimpleChanges,
} from "@angular/core";

// Vitreus
import {
    OtModalService,
    ModalSize,
} from "@onetrust/vitreus";

// Components
import { LinkControlsModalComponent } from "linkControlsModalModule/components/link-controls-modal/link-controls-modal.component";

// Services
import { LinkControlsService } from "linkControlsAPIModule/services/link-controls.service";
import { ControlsFilterService } from "controls/shared/components/controls-filter/controls-filter.service";

// Constants + Enums
import {
    ControlsTabColumnNames,
    DefaultPaginationParams,
    ControlPermissions,
    ControlFilterStorageName,
} from "linkControlsAPIModule/constants/link-controls.constant";
import { TableColumnTypes } from "enums/data-table.enum";

// Interfaces
import { IPageableMeta } from "interfaces/pagination.interface";
import {
    ILinkedControlInformation,
    ILinkedControlsTable,
    ILinkControlsModalData,
    IControlFilter,
} from "linkControlsAPIModule/interfaces/link-controls.interface";
import { IPaginationParams } from "interfaces/pagination.interface";
import { ITableColumnConfig } from "interfaces/table-config.interface";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

@Component({
    selector: "link-controls-list",
    templateUrl: "./link-controls-list.component.html",
})
export class LinkControlsListComponent {

    @Input() recordId: string;
    @Input() recordType: string;

    isPageloading = true;
    searchCompleted = false;
    controlPermissions = ControlPermissions;
    searchTerm: string;
    table: ILinkedControlsTable;
    filterableColumns: ITableColumnConfig[];
    tableMetaData: IPageableMeta;
    isFiltering: boolean;
    currentFilters = [];
    query: IControlFilter[] = [];
    storageKey: string;

    private params: IPaginationParams<any> = DefaultPaginationParams;
    private destroy$ = new Subject();

    constructor(
        private linkControlsService: LinkControlsService,
        private translatePipe: TranslatePipe,
        private otModalService: OtModalService,
        private controlsFilterService: ControlsFilterService,
    ) { }

    ngOnChanges(changes: SimpleChanges) {
        if (changes.recordId && changes.recordId.currentValue) {
            this.recordId = changes.recordId.currentValue;
            this.initializeList(this.recordId);
        }
    }

    ngOnDestroy() {
        this.linkControlsService.reset();
        this.destroy$.next();
        this.destroy$.complete();
    }

    initializeList(recordId: string) {
        this.storageKey = `${ControlFilterStorageName[this.recordType]}.${recordId}`;
        this.currentFilters = this.controlsFilterService.loadFilters(this.storageKey);
        this.query = this.controlsFilterService.buildQuery(this.currentFilters);
        this.isPageloading = true;
        this.linkControlsService.retrieveLinkControls(this.recordType, recordId, this.query);
        const paginationParams = this.linkControlsService.getPaginationParamFromSessionStorage(this.recordType, recordId);
        this.searchTerm = paginationParams.search;
        this.linkControlsService.linkControls$.pipe(
            takeUntil(this.destroy$),
        ).subscribe((controlsLibrary: ILinkedControlInformation[]) => {
            if (controlsLibrary) {
                this.table = this.configureTable(controlsLibrary);
                this.filterableColumns = this.getFilterableColumns();
                this.tableMetaData = this.linkControlsService.linkControlsMeta$.getValue();
                this.isPageloading = false;
            }
        });
    }

    onPaginationChange(event: number = 0) {
        this.params = { ...this.params, page: event };
        sessionStorage.setItem(this.recordType.concat(this.recordId), JSON.stringify(this.params));
        this.linkControlsService.retrieveLinkControls(this.recordType, this.recordId, this.query);
    }

    addNewControls() {
        const modalData: ILinkControlsModalData = {
            recordType: this.recordType,
            recordId: this.recordId,
            query: this.query,
        };
        this.otModalService.create(
            LinkControlsModalComponent,
            {
                isComponent: true,
                size: ModalSize.MEDIUM,
            },
            modalData,
        );
    }

    onSearchName(searchName: string) {
        this.params = { ...this.params, search: searchName, page: 0 };
        sessionStorage.setItem(this.recordType.concat(this.recordId), JSON.stringify(this.params));
        this.linkControlsService.retrieveLinkControls(this.recordType, this.recordId, this.query);
    }

    toggleFilterDrawer() {
        this.isFiltering = !this.isFiltering;
    }

    filterCanceled() {
        this.isFiltering = false;
    }

    filterApplied(filters) {
        this.isFiltering = false;
        this.query = this.controlsFilterService.buildQuery(filters);
        this.currentFilters = filters;
        const paginationParams = this.linkControlsService.getPaginationParamFromSessionStorage(this.recordType, this.recordId);
        paginationParams.size = DefaultPaginationParams.size;
        paginationParams.page = DefaultPaginationParams.page;
        sessionStorage.setItem(this.recordType.concat(this.recordId), JSON.stringify(paginationParams));
        this.linkControlsService.retrieveLinkControls(this.recordType, this.recordId, this.query);
    }

    getFilterableColumns(): ITableColumnConfig[] {
        const columns = this.table.columns.filter((column) => column.canFilter);
        columns.push(
            {
                name: this.translatePipe.transform("Category"),
                sortKey: ControlsTabColumnNames.Category,
                type: TableColumnTypes.Text,
                cellValueKey: ControlsTabColumnNames.Category,
                canFilter: true,
                filterKey: "controlCategoryId",
            },
        );
        return columns;
    }

    onSortChange({ sortKey, sortOrder }) {
        this.params.sort.sortKey = sortKey;
        this.params.sort.sortOrder = sortOrder;
        sessionStorage.setItem(this.recordType.concat(this.recordId), JSON.stringify(this.params));
        this.linkControlsService.retrieveLinkControls(this.recordType, this.recordId, this.query);
    }

    private configureTable(records: ILinkedControlInformation[]): ILinkedControlsTable {
        return {
            rows: records,
            columns: [
                {
                    name: this.translatePipe.transform("ControlID"),
                    sortable: true,
                    sortKey: "controlIdentifier",
                    type: TableColumnTypes.Link,
                    cellValueKey: ControlsTabColumnNames.ControlId,
                },
                {
                    name: this.translatePipe.transform("Name"),
                    sortable: true,
                    sortKey: "controlName",
                    type: TableColumnTypes.Text,
                    cellValueKey: ControlsTabColumnNames.Name,
                },
                {
                    name: this.translatePipe.transform("Framework"),
                    sortable: true,
                    sortKey: ControlsTabColumnNames.FrameWork,
                    type: TableColumnTypes.Text,
                    cellValueKey: ControlsTabColumnNames.FrameWork,
                    canFilter: true,
                    filterKey: "frameworkId",
                },
                {
                    name: this.translatePipe.transform("Status"),
                    sortKey: ControlsTabColumnNames.Status,
                    type: TableColumnTypes.Status,
                    cellValueKey: ControlsTabColumnNames.Status,
                },
                {
                    name: "",
                    type: TableColumnTypes.Action,
                    cellValueKey: "",
                },
            ],
        };
    }
}
