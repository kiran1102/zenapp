// angular
import { NgModule } from "@angular/core";

// Services
import { LinkControlsAPIService } from "linkControlsAPIModule/services/link-controls-api.service";
import { LinkControlsService } from "linkControlsAPIModule/services/link-controls.service";

@NgModule({
    providers: [
        LinkControlsAPIService,
        LinkControlsService,
    ],
})
export class LinkControlsAPIModule { }
