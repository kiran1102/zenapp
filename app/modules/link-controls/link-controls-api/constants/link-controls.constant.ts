import { InventorySchemaIds } from "constants/inventory-config.constant";
import { IPaginationParams } from "interfaces/pagination.interface";
import { GridViewStorageNames } from "sharedModules/enums/grid-view-storage.enum";

export const ControlsTabColumnNames = {
    Status: "status",
    ControlId: "identifier",
    Name: "name",
    FrameWork: "frameworkName",
    Category: "categoryName",
    Description: "description",
    Maturity: "maturityNameKey",
    Note: "note",
};

export const DefaultPaginationParams: IPaginationParams<any> = {
    page: 0,
    size: 20,
    search: "",
    sort: { sortKey: "", sortOrder: "" },
};

export const LinkControlsEntityType = {
    Assets: InventorySchemaIds.Assets,
    Processes: InventorySchemaIds.Processes,
    Vendors: InventorySchemaIds.Vendors,
    Entities: InventorySchemaIds.Entities,
    Risks: "Risks",
};

export const ControlFilterStorageName = {
    [LinkControlsEntityType.Assets]: GridViewStorageNames.ASSET_INVENTORY_LIST,
    [LinkControlsEntityType.Processes]: GridViewStorageNames.PA_INVENTORY_LIST,
    [LinkControlsEntityType.Vendors]: GridViewStorageNames.VENDOR_INVENTORY_LIST,
    [LinkControlsEntityType.Entities]: GridViewStorageNames.ENTITY_INVENTORY_LIST,
};

export const DeleteControlConfirmDesc = {
    [LinkControlsEntityType.Assets]: "ConfirmDescDeleteControlFromAsset",
    [LinkControlsEntityType.Processes]: "ConfirmDescDeleteControlFromPA",
    [LinkControlsEntityType.Vendors]: "AreYouSureToDeleteControls",
    [LinkControlsEntityType.Vendors]: "ConfirmDescDeleteControlFromEntity",
};

export const ControlPermissions = {
    [LinkControlsEntityType.Assets]: {
        viewControlTab: "InventoryControlsTab",
        addControl: "InventoryAddControls",
        removeControl: "InventoryRemoveControls",
        editControl: "InventoryControlsTab",
    },
    [LinkControlsEntityType.Processes]: {
        viewControlTab: "InventoryControlsTab",
        addControl: "InventoryAddControls",
        removeControl: "InventoryRemoveControls",
        editControl: "InventoryControlsTab",
    },
    [InventorySchemaIds.Entities]: {
        viewControlTab: "InventoryControlsTab",
        addControl: "InventoryAddControls",
        removeControl: "InventoryRemoveControls",
        editControl: "InventoryControlsTab",
    },
    [LinkControlsEntityType.Vendors]: {
        viewControlTab: "VRMControlsTab",
        removeControl: "VRMRemoveControlFromVendor",
        editControl: "VRMEditControlOnVendor",
    },
};

export const LinkControlDetailTabOptions = {
    Detail: "Detail",
};

export const EntityListRoutes = {
    [LinkControlsEntityType.Assets]: "zen.app.pia.module.datamap.views.inventory.list",
    [LinkControlsEntityType.Processes]: "zen.app.pia.module.datamap.views.inventory.list",
    [LinkControlsEntityType.Vendors]: "zen.app.pia.module.vendor.inventory.list",
    [LinkControlsEntityType.Entities]: "zen.app.pia.module.datamap.views.inventory.list",
};

export const LinkControlsEntityTranslationKey = {
    [LinkControlsEntityType.Assets]: "Assets",
    [LinkControlsEntityType.Processes]: "ProcessingActivities",
    [LinkControlsEntityType.Entities]: "LegalEntities",
};

export const EntityDetailRoutes = {
    [LinkControlsEntityType.Assets]: "zen.app.pia.module.datamap.views.inventory.details",
    [LinkControlsEntityType.Processes]: "zen.app.pia.module.datamap.views.inventory.details",
    [LinkControlsEntityType.Vendors]: "zen.app.pia.module.vendor.inventory.details",
    [LinkControlsEntityType.Entities]: "zen.app.pia.module.datamap.views.inventory.details",
};

export const EmptyData = "- - - -";

// TODO Temporary until there's a new Vitreus update
export const ControlsStatusColorMap = {
    Implemented: "success",
    Pending: "primary",
    NotDoing: "disabled",
};
