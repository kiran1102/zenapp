// Angular
import { Injectable } from "@angular/core";

// Rxjs
import {
    from,
    Observable,
} from "rxjs";

// Interfaces
import {
    IProtocolResponse,
    IProtocolConfig,
    IProtocolMessages,
} from "interfaces/protocol.interface";
import { ProtocolService } from "modules/core/services/protocol.service";
import {
    IControlIds,
    ILinkedControlInformation,
    ILinkControlMaturityInformationResponse,
    IUpdateLinkControlPayload,
    ILinkedControlDetailResponse,
    IRetrieveLinkControlListPayload,
    ILinkedControlStatuslResponse,
} from "linkControlsAPIModule/interfaces/link-controls.interface";
import {
    IPaginatedResponse,
    IPaginationParams,
} from "interfaces/pagination.interface";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

@Injectable()
export class LinkControlsAPIService {

    constructor(
        private OneProtocol: ProtocolService,
        private translatePipe: TranslatePipe,
    ) { }

    getInventoryLinkedControls(inventoryType: string, recordId: string, params: IPaginationParams<any>, payload: IRetrieveLinkControlListPayload): Observable<IProtocolResponse<IPaginatedResponse<ILinkedControlInformation>>> {
        const sort = params && params.sort && params.sort.sortKey && params.sort.sortOrder ? `${params.sort.sortKey},${params.sort.sortOrder}` : "";
        const config: IProtocolConfig = this.OneProtocol.customConfig("POST", `/api/inventory/v2/${inventoryType}/${recordId}/controls/page?`, { ...params, sort }, payload);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("ErrorRetrievingControlList") } };
        return from(this.OneProtocol.http(config, messages));
    }

    linkControlsToInventory(inventoryType: string, recordId: string, controlIds: IControlIds): Observable<IProtocolResponse<null>> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("POST", `/api/inventory/v2/${inventoryType}/${recordId}/controls`, null, controlIds);
        const messages: IProtocolMessages = {
            Error: { custom: this.translatePipe.transform("ErrorAddingControls") },
            Success: { custom: this.translatePipe.transform("ControlSuccessfullyAdded") },
        };
        return from(this.OneProtocol.http(config, messages));
    }

    deleteLinkedControlsFromInventory(inventoryType: string, recordId: string, controlId: string): Observable<IProtocolResponse<null>> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("DELETE", `/api/inventory/v2/${inventoryType}/${recordId}/controls/${controlId}`);
        const messages: IProtocolMessages = {
            Error: { custom: this.translatePipe.transform("ErrorDeletingControl") },
            Success: { custom: this.translatePipe.transform("ControlSuccessfullyDeleted") },
        };
        return from(this.OneProtocol.http(config, messages));
    }

    getLinkedControlDetail(controlId: string): Observable<IProtocolResponse<ILinkedControlDetailResponse>> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("GET", `/api/inventory/v2/controls/${controlId}/controls`);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("ErrorRetrievingControlDetail") } };
        return from(this.OneProtocol.http(config, messages));
    }

    getMaturitiesOptions(): Observable<IProtocolResponse<ILinkControlMaturityInformationResponse>> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("GET", `/api/inventory/v2/controls/maturities`);
        const messages: IProtocolMessages = { Error: { Hide: true } };
        return from(this.OneProtocol.http(config, messages));
    }

    getStatusOptions(): Observable<IProtocolResponse<ILinkedControlStatuslResponse>> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("GET", `/api/inventory/v2/controls/statuses`);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("ErrorRetrievingControlStatuses") } };
        return from(this.OneProtocol.http(config, messages));
    }

    updateLinkControlDetail(controlId: string, payload: IUpdateLinkControlPayload): Observable<IProtocolResponse<null>> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("PUT", `/api/inventory/v2/controls/${controlId}/controls`, null, payload);
        const message: IProtocolMessages = { Error: { custom: this.translatePipe.transform("ErrorUpdatingControlDetail") }, Success: { custom: this.translatePipe.transform("UpdatedControlDetailsSuccessfully") } };
        return from(this.OneProtocol.http(config, message));
    }
}
