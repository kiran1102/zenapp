import { Injectable } from "@angular/core";

// Rxjs
import {
    BehaviorSubject,
    Observable,
    Subscription,
} from "rxjs";
import { take } from "rxjs/operators";

// Services
import { LinkControlsAPIService } from "linkControlsAPIModule/services/link-controls-api.service";

// Interfaces
import { IProtocolResponse } from "interfaces/protocol.interface";
import { IStringMap } from "interfaces/generic.interface";
import {
    IControlIds,
    ILinkedControlInformation,
    ILinkedControlDetail,
    ILinkControlMaturityInformation,
    ILinkedControlDetailResponse,
    ILinkControlMaturityInformationResponse,
    IControlFilter,
    ILinkedControlStatuslResponse,
} from "linkControlsAPIModule/interfaces/link-controls.interface";
import {
    IPaginatedResponse,
    IPageableMeta,
} from "interfaces/pagination.interface";

// Constants
import {
    DefaultPaginationParams,
    LinkControlsEntityType,
} from "linkControlsAPIModule/constants/link-controls.constant";

@Injectable()
export class LinkControlsService {

    linkControls$ = new BehaviorSubject<ILinkedControlInformation[]>(null);
    linkControlsMeta$ = new BehaviorSubject<IPageableMeta>(null);
    linkControlsLoading$ = new BehaviorSubject<boolean>(false);
    linkControlDetail$ = new BehaviorSubject<ILinkedControlDetail>(null);
    linkControlDetailLoading$ = new BehaviorSubject<boolean>(false);
    linkControlMaturityOptions$ = new BehaviorSubject<ILinkControlMaturityInformation[]>(null);
    linkControlStatusOptions$ = new BehaviorSubject<string[]>(null);
    entityName$ = new BehaviorSubject<string>(null);
    controlName$ = new BehaviorSubject<string>(null);

    constructor(
        private linkControlsAPI: LinkControlsAPIService,
    ) { }

    reset(): void {
        this.linkControls$.next(null);
        this.linkControlsMeta$.next(null);
    }

    retrieveLinkControls(recordType: string, recordId: string, query: IControlFilter[]) {
        this.linkControlsLoading$.next(true);
        this.retrieveLinkControlsFromRecord(recordType, recordId, query)
            .subscribe((res: IProtocolResponse<IPaginatedResponse<ILinkedControlInformation>>): void => {
                if (res.result) {
                    this.linkControlsMeta$.next(res.data.meta.page);
                    this.linkControls$.next(res.data.data);
                }
                this.linkControlsLoading$.next(false);
            });
    }

    linkControlsToRecord(recordType: string, recordId: string, controlIds: string[]): Observable<IProtocolResponse<null>> {
        const payload: IControlIds = { controlIds };
        if (recordType === LinkControlsEntityType.Assets || recordType === LinkControlsEntityType.Processes || recordType === LinkControlsEntityType.Entities) return this.linkControlsAPI.linkControlsToInventory(recordType, recordId, payload);
    }

    deleteLinkControls(controlId: string, recordType: string, recordId: string, query: IControlFilter[]) {
        this.deleteLinkControlsFromRecord(recordType, recordId, controlId).subscribe((response: IProtocolResponse<null>) => {
            if (response.result) {
                const paginationParams = this.linkControlsMeta$.getValue();
                let currentPage = this.getPaginationParamFromSessionStorage(recordType, recordId);
                if (!currentPage) currentPage = DefaultPaginationParams;
                if (paginationParams) {
                    currentPage.page = paginationParams.number !== 0 && paginationParams.totalElements % 20 === 1 ? paginationParams.number - 1 : paginationParams.number;
                }
                sessionStorage.setItem(recordType.concat(recordId), JSON.stringify(currentPage));
                this.retrieveLinkControls(recordType, recordId, query);
            }
        });
    }

    getControlDetail(recordType: string, controlId: string) {
        this.linkControlDetailLoading$.next(true);
        this.getControlDetailFromRecord(recordType, controlId)
            .pipe(
                take(1),
            ).subscribe((res: IProtocolResponse<ILinkedControlDetailResponse>): void => {
                if (res.result) {
                    this.linkControlDetail$.next(res.data.data);
                    this.entityName$.next(res.data.data.inventory.name);
                    this.controlName$.next(res.data.data.control.identifier);
                }
                this.linkControlDetailLoading$.next(false);
            });
    }

    getMaturitiesOptions() {
        this.linkControlsAPI.getMaturitiesOptions()
            .subscribe((res: IProtocolResponse<ILinkControlMaturityInformationResponse>) => {
                if (res.result) {
                    this.linkControlMaturityOptions$.next(res.data.data);
                }
            });
    }

    getStatusOptions() {
        this.linkControlsAPI.getStatusOptions()
            .subscribe((res: IProtocolResponse<ILinkedControlStatuslResponse>) => {
                if (res.result) {
                    this.linkControlStatusOptions$.next(res.data.data);
                }
            });
    }

    getPaginationParamFromSessionStorage(recordType: string, recordId: string): IStringMap<any> {
        try {
            return JSON.parse(sessionStorage.getItem(recordType.concat(recordId)));
        } catch (e) {
            sessionStorage.removeItem(recordType.concat(recordId));
            return null;
        }
    }

    private retrieveLinkControlsFromRecord(recordType: string, recordId: string, query: IControlFilter[]): Observable<IProtocolResponse<IPaginatedResponse<ILinkedControlInformation>>> {
        if (recordType === LinkControlsEntityType.Assets || recordType === LinkControlsEntityType.Processes || recordType === LinkControlsEntityType.Entities) {
            let paginationParams = this.getPaginationParamFromSessionStorage(recordType, recordId);
            if (!paginationParams) paginationParams = DefaultPaginationParams;
            sessionStorage.setItem(recordType.concat(recordId), JSON.stringify(paginationParams));
            const payload = { fullText: paginationParams.search, filters: query};
            return this.linkControlsAPI.getInventoryLinkedControls(recordType, recordId, paginationParams, payload);
        }
    }

    private deleteLinkControlsFromRecord(recordType: string, recordId: string, controlId: string): Observable<IProtocolResponse<null>>  {
        if (recordType === LinkControlsEntityType.Assets || recordType === LinkControlsEntityType.Processes || recordType === LinkControlsEntityType.Entities) return this.linkControlsAPI.deleteLinkedControlsFromInventory(recordType, recordId, controlId);
    }

    private getControlDetailFromRecord(recordType: string, controlId: string): Observable<IProtocolResponse<ILinkedControlDetailResponse>> {
        if (recordType === LinkControlsEntityType.Assets || recordType === LinkControlsEntityType.Processes || recordType === LinkControlsEntityType.Entities) return this.linkControlsAPI.getLinkedControlDetail(controlId);
    }
}
