// RxJs
import { Observable, Subject } from "rxjs";

// Enum
import { ComparisonOperators } from "enums/operators.enum";

// Interfaces
import { IPageableMeta } from "interfaces/pagination.interface";
import { ITableColumnConfig } from "interfaces/table-config.interface";
import {
    IControlsLibraryResponse,
    IFrameworkListInformation,
} from "controls/shared/interfaces/controls-library.interface";
import { ICategoryInformation } from "controls/shared/interfaces/controls.interface";

export interface IControlIds {
    controlIds: string[];
}

export interface IControlInformation {
    id: string;
    identifier: string;
    name: string;
    description: string;
    frameworkId: string;
    frameworkName: string;
    categoryId: string;
    categoryName: string;
    categoryNameKey: string;
}

export interface ILinkedControlInformation {
    id: string;
    inventory?: {
        id: string;
        name: string;
    };
    risk?: { };
    control: IControlInformation;
}

export interface ILinkedControlsTable {
    rows: ILinkedControlInformation[];
    columns?: ITableColumnConfig[];
    metaData?: IPageableMeta;
}

export interface ILinkControlsModalData {
    recordId?: string;
    recordType?: string;
    linkControlSource?: Subject<string[]>;
    linkControl$?: Observable<boolean>;
    query?: IControlFilter[];
}

export interface IDeleteControlsModalData {
    controlId: string;
    recordId: string;
    recordType: string;
}

export interface IControlsLibrarySelection extends IControlsLibraryResponse {
    isSelected: boolean;
}

export interface IFrameworkOption extends IFrameworkListInformation {
    count: number;
}

export interface ILinkedControlDetail {
    control: IControlInformation;
    id: string;
    note: string;
    maturityId: string;
    maturityName: string;
    maturityNameKey: string;
    status: string;
    inventory?: {
        id: string;
        name: string;
    };
}

export interface ILinkedControlDetailResponse {
    data: ILinkedControlDetail;
}

export interface ILinkedControlStatuslResponse {
    data: string[];
}

export interface IUpdateLinkControlPayload {
    maturityId: string;
    status: string;
    note: string;
}

export interface IControlFilter {
    field: string;
    operator: ComparisonOperators;
    value: string[];
}

export interface IRetrieveLinkControlListPayload {
    fullText: string;
    filters: IControlFilter[];
}

export interface ILinkControlMaturityInformation {
    id: string;
    name: string;
    nameKey: string;
}

export interface ILinkControlMaturityInformationResponse {
    data: ILinkControlMaturityInformation[];
}

export interface ICreateData {
    label: string;
    type: string;
    name: string;
    isRequired: boolean;
    value?: string;
    options?: ICategoryInformation[];
    [key: string]: string | boolean | number | ICategoryInformation[] | ICategoryInformation;
}
