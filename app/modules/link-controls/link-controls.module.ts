import {
    NgModule,
} from "@angular/core";

// Modules
import { LinkControlsAPIModule } from "linkControlsModule/link-controls-api/link-controls-api.module";
import { LinkControlsListModule } from "linkControlsModule/link-controls-list/link-controls-list.module";
import { LinkControlsDetailModule } from "linkControlsModule/link-controls-detail/link-controls-detail.module";

@NgModule({
    imports: [
        LinkControlsAPIModule,
        LinkControlsListModule,
        LinkControlsDetailModule,
    ],
})
export class LinkControlsModule {}
