// Angular
import {
    Injectable,
    Inject,
} from "@angular/core";
import { IPromise } from "angular";

// Services
import { ProtocolService } from "modules/core/services/protocol.service";

// Interfaces
import {
    IProtocolResponse,
    IProtocolConfig,
    IProtocolMessages,
} from "interfaces/protocol.interface";
import {
    ICookieScriptVersionDiffResponse,
    IJqueryVersionDetail,
    ICookieLimitResponse,
} from "interfaces/cookies/cookie-publish-script.interface";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

@Injectable()
export class CookieApiService {

    constructor(
        private readonly translatePipe: TranslatePipe,
        private OneProtocol: ProtocolService,
    ) { }

    getVersionDiff(domainId: number): IPromise<IProtocolResponse<ICookieScriptVersionDiffResponse>> {
        const config: IProtocolConfig = this.OneProtocol.config("GET", `/consentnotice/domains/${domainId}/versiondiff`);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("ErrorGettingVersionDifferences") } };
        return this.OneProtocol.http(config, messages);
    }

    getJqueryVersions(domainId: number): IPromise<IProtocolResponse<IJqueryVersionDetail[]>> {
        const config: IProtocolConfig = this.OneProtocol.config("GET", `/consentnotice/domains/${domainId}/jQueryVersions`);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("ErrorGettingJqueryVersions") } };
        return this.OneProtocol.http(config, messages);
    }

    getDomainJqueryVersion(domainId: number): IPromise<IProtocolResponse<IJqueryVersionDetail>> {
        const config: IProtocolConfig = this.OneProtocol.config("GET", `/consentnotice/domains/${domainId}/jQueryVersion`);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("ErrorGettingDomainJqueryVersion") } };
        return this.OneProtocol.http(config, messages);
    }

    getPublishStatus(domainId: number): IPromise<IProtocolResponse<{ Disable: boolean }>> {
        const config: IProtocolConfig = this.OneProtocol.config("GET", `/consentnotice/domains/${domainId}/publishStatus`);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("ErrorFetchingPublishstatus") } };
        return this.OneProtocol.http(config, messages);
    }

    getPublishLimit(domainId: number): IPromise<IProtocolResponse<ICookieLimitResponse>> {
        const config: IProtocolConfig = this.OneProtocol.config("GET", `/consentnotice/domains/${domainId}/publishlimit`);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("ErrorFetchingPublishLimit") } };
        return this.OneProtocol.http(config, messages);
    }

    sendEmail(): IPromise<IProtocolResponse<boolean>> {
        const config: IProtocolConfig = this.OneProtocol.config("POST", `/consentnotice/domainlimit/notification`);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("ErrorFetchingPublishLimit") } };
        return this.OneProtocol.http(config, messages);
    }
}
