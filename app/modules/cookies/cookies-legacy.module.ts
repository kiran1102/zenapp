declare var angular: angular.IAngularStatic;

import { routes } from "./cookies-legacy.routes";

import AddWebsiteCtrl from "../../scripts/Cookies/Controllers/add-website.controller";
import CookieComplianceCtrl from "../../scripts/Cookies/Controllers/cc.controller";
import CookieComplianceConsentCtrl from "../../scripts/Cookies/Controllers/cc-consent.controller";
import CookieComplianceCookiesCtrl from "../../scripts/Cookies/Controllers/cc-cookies.controller";
import CookieComplianceFormsCtrl from "../../scripts/Cookies/Controllers/cc-forms.controller";
import CookieComplianceFormPagesCtrl from "../../scripts/Cookies/Controllers/cc-form-pages.controller";
import CookieCompliancePagesCtrl from "../../scripts/Cookies/Controllers/cc-pages.controller";
import CookieComplianceReportsCtrl from "../../scripts/Cookies/Controllers/cc-reports.controller";
import CookieComplianceTagsCtrl from "../../scripts/Cookies/Controllers/cc-tags.controller";
import CookieComplianceWebsitesCtrl from "../../scripts/Cookies/Controllers/cc-websites.controller";
import CookieComplianceScriptCtrl from "../../scripts/Cookies/Controllers/cc-script.controller";
import CookieComplianceCookieValuesCtrl from "../../scripts/Cookies/Controllers/cc-cookie-values.controller";
import InfoMdCtrl from "../../scripts/Cookies/Controllers/cc-info.modal.controller";
import CookieComplianceLocalStorageCtrl from "../../scripts/Cookies/Controllers/cc-local-storage.controller";
import { CcGroupVendorModalCtrl } from "generalcomponent/Cookies/Policy/Modal/cc-group-vendor-modal.controller";
import restoreDefaultModal from "generalcomponent/Cookies/Shared/Modal/cc-scan-login-modal/restore-default-modal.component";

import { WebAuditService } from "cookiesService/web-audit.service";
import { CookieConsentService } from "cookiesService/cookie-consent.service";
import { CookieComplianceStore } from "cookiesService/cookie-compliance-store.service";
import { CookiePolicyService } from "cookiesService/cookie-policy.service";
import { CookiePreferenceCenterService } from "cookiesService/cookie-preference-center.service";
import { CookieMultiLanguageService } from "cookiesService/cookie-multi-language.service";
import { CookieScriptIntegrationService } from "cookiesService/cookie-script-integration.service";
import { CookieAuditScheduleService } from "cookiesService/cookie-audit-schedule.service";
import { CookieOrgGroupService } from "cookiesService/cookie-org-group.service";
import CookieScriptArchiveService from "cookiesService/cookie-script-archive.service";
import { CookieLanguageSwitcherService } from "cookiesService/cookie-language-switcher.service";
import { CookieScanLoginService } from "cookiesService/cookie-scan-login-details.service";
import { CookieTemplateService } from "cookiesService/cookie-template.service";
import { CookieIABIntegrationService } from "cookiesService/cookie-iab-integration.service";
import { LanguagePollingService } from "cookiesService/cookie-language-polling.service";
import CcAssignedGroups from "generalcomponent/Cookies/Policy/cc-assigned-groups.component";
import CcCookieCard from "generalcomponent/Cookies/Policy/cc-cookie-card.component";
import CcGroup from "generalcomponent/Cookies/Policy/cc-group.component";
import CcHostCard from "generalcomponent/Cookies/Policy/cc-host-card.component";
import CcPolicyHeader from "generalcomponent/Cookies/Policy/cc-policy-header.component";
import CcPolicy from "generalcomponent/Cookies/Policy/cc-policy.component";
import CcSubgroupSummary from "generalcomponent/Cookies/Policy/cc-subgroup-summary.component";
import CcSubgroup from "generalcomponent/Cookies/Policy/cc-subgroup.component";
import CcUnassignedList from "generalcomponent/Cookies/Policy/cc-unassigned-list.component";
import CcGroupVendorModal from "generalcomponent/Cookies/Policy/Modal/cc-group-vendors-modal.component";
import ccPreferenceCenter from "generalcomponent/Cookies/PreferenceCenter/cc-preference-center.component";
import CcPreferenceCenterHeader from "generalcomponent/Cookies/PreferenceCenter/cc-preference-center-header.component";
import CcCookieComplianceHeader from "generalcomponent/Cookies/Shared/cc-cookie-compliance-header.component";
import CcPreferenceCenterAccordions from "generalcomponent/Cookies/PreferenceCenter/cc-preference-center-accordions.component";
import CcPreferenceCenterConsentSettings from "generalcomponent/Cookies/PreferenceCenter/cc-preference-center-consent-settings.component";
import CcPreferenceCenterEditDetails from "generalcomponent/Cookies/PreferenceCenter/cc-preference-center-edit-details.component";
import CcPreferenceCenterStyling from "generalcomponent/Cookies/PreferenceCenter/cc-preference-center-styling.component";
import { CcScriptIntegration } from "generalcomponent/Cookies/ScriptIntegration/cc-script-integration.component";
import CcCookieScriptTag from "generalcomponent/Cookies/ScriptIntegration/cc-cookie-script-tag.component";
import CcDownloadCookieScript from "generalcomponent/Cookies/ScriptIntegration/cc-download-cookie-script.component";
import CcScanResultsExport from "generalcomponent/Cookies/Shared/cc-scan-result-export.component";
import CcPolicyMenuAccordians from "generalcomponent/Cookies/Policy/cc-policy-menu-accordians.component";
import IabConsent from "generalcomponent/Cookies/Policy/cc-policy-iab-consent.component";
import { CcWebAuditActionList } from "generalcomponent/Cookies/ScanResults/cc-web-audit-action-list.component";
import { CcItemCountTile } from "generalcomponent/Cookies/ScanResults/cc-item-count-tile.component";
import preferenceCenterTemplate from "generalcomponent/Cookies/Templates/cc-preference-center-template.component";

export const cookiesLegacyModule = angular.module("zen.cookies", [
    "ui.bootstrap",
    "ui.router",
    "ngMaterial",
    "ngSanitize",
    "ngStorage",
    "ngAnimate",

    // Custom modules
    "zen.identity",
    "minicolors", // Color Picker
])
    .config(routes)

    .controller("AddWebsiteCtrl", AddWebsiteCtrl)
    .controller("CookieComplianceConsentCtrl", CookieComplianceConsentCtrl)
    .controller("CookieComplianceCtrl", CookieComplianceCtrl)
    .controller("CookieComplianceCookiesCtrl", CookieComplianceCookiesCtrl)
    .controller("CookieComplianceFormsCtrl", CookieComplianceFormsCtrl)
    .controller("CookieComplianceFormPagesCtrl", CookieComplianceFormPagesCtrl)
    .controller("CookieCompliancePagesCtrl", CookieCompliancePagesCtrl)
    .controller("CookieComplianceReportsCtrl", CookieComplianceReportsCtrl)
    .controller("CookieComplianceTagsCtrl", CookieComplianceTagsCtrl)
    .controller("CookieComplianceWebsitesCtrl", CookieComplianceWebsitesCtrl)
    .controller("CookieComplianceScriptCtrl", CookieComplianceScriptCtrl)
    .controller("CookieComplianceCookieValuesCtrl", CookieComplianceCookieValuesCtrl)
    .controller("InfoModalCtrl", InfoMdCtrl)
    .controller("ccGroupVendorModalCtrl", CcGroupVendorModalCtrl)
    .controller("CookieComplianceLocalStorageCtrl", CookieComplianceLocalStorageCtrl)

    .service("WebAuditService", WebAuditService)
    .service("CookieConsentService", CookieConsentService)
    .service("CookieComplianceStore", CookieComplianceStore)
    .service("CookiePolicyService", CookiePolicyService)
    .service("CookiePreferenceCenterService", CookiePreferenceCenterService)
    .service("CookieMultiLanguageService", CookieMultiLanguageService)
    .service("CookieScriptIntegrationService", CookieScriptIntegrationService)
    .service("CookieAuditScheduleService", CookieAuditScheduleService)
    .service("CookieOrgGroupService", CookieOrgGroupService)
    .service("CookieScriptArchiveService", CookieScriptArchiveService)
    .service("CookieLanguageSwitcherService", CookieLanguageSwitcherService)
    .service("CookieScanLoginService", CookieScanLoginService)
    .service("CookieTemplateService", CookieTemplateService)
    .service("CookieIABIntegrationService", CookieIABIntegrationService)
    .service("LanguagePollingService", LanguagePollingService)

    .component("ccAssignedGroups", CcAssignedGroups)
    .component("ccCookieCard", CcCookieCard)
    .component("ccGroup", CcGroup)
    .component("ccHostCard", CcHostCard)
    .component("ccPolicyHeader", CcPolicyHeader)
    .component("ccPolicy", CcPolicy)
    .component("ccSubgroupSummary", CcSubgroupSummary)
    .component("ccSubgroup", CcSubgroup)
    .component("ccUnassignedList", CcUnassignedList)
    .component("ccGroupVendorModal", CcGroupVendorModal)
    .component("ccPreferenceCenter", ccPreferenceCenter)
    .component("ccPreferenceCenterHeader", CcPreferenceCenterHeader)
    .component("ccCookieComplianceHeader", CcCookieComplianceHeader)
    .component("ccPreferenceCenterAccordions", CcPreferenceCenterAccordions)
    .component("ccPreferenceCenterConsentSettings", CcPreferenceCenterConsentSettings)
    .component("iabConsent", IabConsent)
    .component("ccPreferenceCenterEditDetails", CcPreferenceCenterEditDetails)
    .component("ccPreferenceCenterStyling", CcPreferenceCenterStyling)
    // .component("ccLanguageModal", CcLanguageModal)
    .component("ccScriptIntegration", CcScriptIntegration)
    .component("ccCookieScriptTag", CcCookieScriptTag)
    .component("ccDownloadCookieScript", CcDownloadCookieScript)
    .component("ccScanResultsExport", CcScanResultsExport)
    .component("ccPolicyMenuAccordians", CcPolicyMenuAccordians)
    .component("ccWebAuditActionList", CcWebAuditActionList)
    .component("ccItemCountTile", CcItemCountTile)
    .component("restoreDefaultModal", restoreDefaultModal)
    .component("preferenceCenterTemplate", preferenceCenterTemplate)
    ;
