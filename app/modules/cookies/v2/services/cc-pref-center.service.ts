import { Injectable } from "@angular/core";

// Services
import { ProtocolService } from "modules/core/services/protocol.service";

// Interfaces
import {
    IProtocolConfig,
    IProtocolMessages,
    IProtocolPacket,
} from "interfaces/protocol.interface";
import { TranslatePipe } from "modules/pipes/translate.pipe";

// This will be going away with cookies V2. Quick fix for existing codebase
@Injectable()
export class CcPrefCenterService {

  constructor(
    private protocolService: ProtocolService,
    private translatePipe: TranslatePipe) {}

  apiRequest(requestMethod: any, requestUrl: string, requestObject: any, object: string, action: string, timeout?: number): ng.IPromise<IProtocolPacket> {
    const config: IProtocolConfig = this.protocolService.config(requestMethod, requestUrl, [], requestObject, undefined, undefined, undefined, undefined, timeout);
    const messages: IProtocolMessages = { Error: { object, action } };
    return this.protocolService.http(config, messages);
  }

  getPreferenceCenter(domainId: number) {
    return this.apiRequest(
      "GET", `/consentNotice/PreferenceCenter?domainId=${domainId}`,
      null,
      this.translatePipe.transform("PreferenceCenter"),
      this.translatePipe.transform("getting")).then((response: IProtocolPacket) => {
        return response.data;
    });
  }

  getPreferencePreviewCssUrl(styleType: string): string {
    let previewCssUrl = "scripts/optanon-modern.css";
    if (styleType.toUpperCase() === "CLASSIC") {
        previewCssUrl = "scripts/optanon-classic.css";
    }
    return previewCssUrl;
  }

  getPreferencePreviewLogo(styleType: string, logoFileData: string): string {
      if (logoFileData) {
          return `'${logoFileData}'`;
      } else if (styleType.toUpperCase() === "CLASSIC") {
          return "images/cookie-collective-top-logo.png";
      } else {
          return "";
      }
  }
}
