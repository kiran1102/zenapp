// Angular
import {
    Component,
    OnInit,
} from "@angular/core";

// Vitreus
import {
    OtModalService,
    IConfirmModalConfig,
} from "@onetrust/vitreus";

// Rxjs
import { Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

// Components
import { CCCategoryDetailModal } from "cookiesV2Module/components/cc-categorizations/cc-category-detail-modal/cc-category-detail-modal.component";

// Interfaces
import { ICookiePageHeaderButtonConfiguration } from "interfaces/cookies/cookie-page-header.interface";

@Component({
    selector: "cc-category-list",
    templateUrl: "./cc-category-list.component.html",
})
export class CCCategoryList implements OnInit {
    primaryButton: ICookiePageHeaderButtonConfiguration = {
        labelKey: "CreateCategory",
        otAutoId: "CookieCategorizationCreateCategoryButton",
        action: () => {
            this.onCreateCategoryModal();
        },
    };
    secondaryButton: ICookiePageHeaderButtonConfiguration = {
        labelKey: "ManageLanguages",
        otAutoId: "CookieCategorizationManageLanguagesButton",
    };
    categories: any = []; // TODO : typing
    modalCloseEvent: Subject<string> = new Subject();

    private destroy$ = new Subject();

    constructor(
        private otModalService: OtModalService,
        private translatePipe: TranslatePipe,
    ) { }

    ngOnInit() { }

    ngOnDestroy(): void {
        this.closeModal();
        this.modalCloseEvent.unsubscribe();
        this.destroy$.next();
        this.destroy$.complete();
    }

    trackByIndex(index, category: any): number { // TODO : typing
        return index;
    }

    onCreateCategoryModal() {
        const modalData = {
            isEditMode: false,
            title: this.translatePipe.transform("CreateCategory"),
            detail: {
                name: "",
                categoryId: "",
                description: "",
            },
            promiseToResolve: (category: any): Promise<boolean> => { // TODO : typing
                // TODO: create ctegory API call
                this.categories.push(category);
                return Promise.resolve(true);
            },
        };
        this.openCategoryDetailModal(modalData);
    }

    onEditCategory(index: number, data: any) { // TODO : typing
        const modalData = { // TODO: to be replaced with data param
            isEditMode: true,
            title: this.translatePipe.transform("EditCategory"),
            detail: data ? data : {
                name: "group2",
                categoryId: "001",
                description: "description",
            },
            promiseToResolve: (category: any): Promise<boolean> => { // TODO : typing
                this.categories[index] = category;
                // TODO: update category API call
                return Promise.resolve(true);
            },
        };
        this.openCategoryDetailModal(modalData);
    }

    onRemoveCategory(index: number) {
        const cookiesAssigned = true; // TODO : refer category by index
        const modalConfig: IConfirmModalConfig = {
            icon: "ot ot-trash",
            translations: {
                title: this.translatePipe.transform("RemoveCategory"),
                desc: null,
                confirm: null,
                submit: null,
                cancel: null,
            },
        };
        if (cookiesAssigned) {
            modalConfig.translations.desc = this.translatePipe.transform("CookiesAssignedWarning");
            modalConfig.translations.submit = this.translatePipe.transform("Close");
        } else {
            modalConfig.translations.desc = this.translatePipe.transform("RemoveCookieCategoryWarning");
            modalConfig.translations.confirm = this.translatePipe.transform("DoYouWishToContinue");
            modalConfig.translations.submit = this.translatePipe.transform("Remove");
            modalConfig.translations.cancel = this.translatePipe.transform("Cancel");
        }
        this.otModalService
            .confirm({
                ...modalConfig,
            }).pipe(
                takeUntil(this.destroy$),
            ).subscribe(
                (data) => {
                    // TODO : API call
                },
            );
    }

    private openCategoryDetailModal(modalData: any) {
        this.modalCloseEvent = this.otModalService
            .create(
                CCCategoryDetailModal,
                {
                    isComponent: true,
                },
                modalData,
            );
    }

    private closeModal(): void {
        this.modalCloseEvent.next();
    }
}
