// Angular
import {
    Component,
    OnInit,
} from "@angular/core";

// Constants
import { CategorizationTabs } from "enums/cookies/categorizations.enum";

@Component({
    selector: "cc-categorizations-wrapper",
    templateUrl: "./cc-categorizations-wrapper.component.html",
})
export class CCCategorizationsWrapper implements OnInit {
    tabs = [{
        id: CategorizationTabs.Categories,
        label: "Categories",
        otAutoId: "CookieCategorizationCategoryTab",
    }, {
        id: CategorizationTabs.Cookies,
        label: "Cookies",
        otAutoId: "CookieCategorizationCookiesTab",
    }];
    categorizationTabs: typeof CategorizationTabs;
    selectedTab: number;

    constructor() { }

    ngOnInit() {
        this.categorizationTabs = CategorizationTabs;
        this.selectedTab = CategorizationTabs.Categories;
    }

    onTabClick(tab: number) {
        this.selectedTab = tab;
    }
}
