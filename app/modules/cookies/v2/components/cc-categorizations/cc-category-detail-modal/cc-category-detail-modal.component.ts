// Angular
import {
    Component,
    OnInit,
} from "@angular/core";
import {
    FormGroup,
    FormBuilder,
    Validators,
} from "@angular/forms";

// RxJS
import { Subject } from "rxjs";

// Vitreus
import { IOtModalContent } from "@onetrust/vitreus";

// Interfaces
import { ICCDynamicFormConfig } from "interfaces/cookies/cc-dynamic-form-config.interface";

@Component({
    selector: "cc-category-detail-modal",
    templateUrl: "./cc-category-detail-modal.component.html",
})

export class CCCategoryDetailModal implements IOtModalContent, OnInit {
    otModalCloseEvent: Subject<any>; // TODO : typing
    otModalData: any; // TODO : typing
    categoryFormConfig: ICCDynamicFormConfig[] = [
        {
            controlName: "name",
            isRequired: true,
            labelKey: "CategoryName",
            otAutoId: "CookieCategoryDetailModalCategoryNameInput",
            placeholderKey: "CategoryName",
            valueKey: "name",
            validation: [Validators.required],
        },
        {
            controlName: "categoryId",
            isRequired: true,
            labelKey: "CategoryId",
            otAutoId: "CookieCategoryDetailModalCategoryIdInput",
            placeholderKey: "CategoryId",
            valueKey: "categoryId",
            validation: [Validators.required, Validators.pattern("[a-zA-Z0-9]{0,3}")],
        },
        {
            controlName: "description",
            labelKey: "Description",
            otAutoId: "CookieCategoryDetailModalCategoryDescriptionInput",
            placeholderKey: "Description",
            valueKey: "description",
        },
    ];
    categoryFormGroup: FormGroup;

    constructor(
        private formBuilder: FormBuilder,
    ) { }

    ngOnInit() {
        this.initForm();
    }

    closeModal() {
        this.otModalCloseEvent.next(false);
    }

    submitModal() {
        this.otModalData.promiseToResolve(this.categoryFormGroup.getRawValue())
            .then((response: boolean): void => {
                if (!response) return;
                this.closeModal();
            });
    }

    private initForm() {
        this.categoryFormGroup = this.formBuilder.group({});
        this.categoryFormConfig.forEach((control) => {
            this.categoryFormGroup.addControl(control.controlName,
                this.formBuilder.control(this.otModalData.detail[control.valueKey], control.validation));
        });
    }
}
