// Angular
import {
    Component,
    OnInit,
} from "@angular/core";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

// Interfaces
import {
    IFilterData,
    IAddedFilter,
    IFilterTranslatedLabels,
} from "@onetrust/vitreus";
import { ICookiePageHeaderButtonConfiguration } from "interfaces/cookies/cookie-page-header.interface";

// TODO : to be moved to enum folder
enum CookieTableCloumnTypes {
    Action,
    Category,
    Checkbox,
    CookieName,
    Description,
    Domains,
    HostName,
    LifeSpan,
}

@Component({
    selector: "cc-cookie-list",
    templateUrl: "./cc-cookie-list.component.html",
})
export class CCCookieList implements OnInit {
    primaryButton: ICookiePageHeaderButtonConfiguration = {
        labelKey: "Export",
        otAutoId: "CookieCategorizationExportCookiesButton",
        action: () => {
            this.onExportCookies();
        },
    };
    secondaryButton: ICookiePageHeaderButtonConfiguration = {
        labelKey: "AddCookie",
        otAutoId: "CookieCategorizationAddCookieButton",
    };
    allRowSelected = false;
    selectedCookieListMap = {};
    cookies: any = [];  // TODO: typing
    cookieTableCloumnTypes: typeof CookieTableCloumnTypes;
    currentPage: number;
    isFilterOpen = false;
    numPages: number;
    sortable = true;
    sortKey = 1;
    sortOrder = "desc";
    searchText = "";
    contextMenuOptions = [
        {
            text: this.translatePipe.transform("Edit"),
            otAutoId: "Edit",
            action: () => {
                // TODO : action call
            },
        }, {
            text: this.translatePipe.transform("Delete"),
            otAutoId: "Delete",
            action: () => {
                // TODO: action call
            },
        },
    ];

    cookieColumns: any = [
        {
            cellValueKey: "",
            labelKey: "",
            type: CookieTableCloumnTypes.Checkbox,
        },
        {
            labelKey: "CookieName",
            cellValueKey: "name",
            type: CookieTableCloumnTypes.CookieName,
            sortKey: "name",
        }, {
            labelKey: "Domains",
            cellValueKey: "domains",
            type: CookieTableCloumnTypes.Domains,
        }, {
            labelKey: "Lifespan",
            cellValueKey: "lifeSpan",
            type: CookieTableCloumnTypes.LifeSpan,
        }, {
            labelKey: "HostName",
            cellValueKey: "hostName",
            type: CookieTableCloumnTypes.HostName,
        }, {
            labelKey: "Category",
            cellValueKey: "category",
            type: CookieTableCloumnTypes.Category,
            sortKey: "category",
        }, {
            labelKey: "Description",
            cellValueKey: "description",
            type: CookieTableCloumnTypes.Description,
        }, {
            labelKey: "",
            type: CookieTableCloumnTypes.Action,
            cellValueKey: "",
        },
    ];
    translatedLabels: IFilterTranslatedLabels;
    filterData: IFilterData[] = [];
    preSelectedFilter: IAddedFilter[] = [];

    constructor(
        private translatePipe: TranslatePipe,
    ) { }

    ngOnInit() {
        this.currentPage = 1;
        this.numPages = 10; // TODO : refer from the header
        this.cookieTableCloumnTypes = CookieTableCloumnTypes;
        this.translatedLabels = {
            addFilterLabel: this.translatePipe.transform("AddFilter"),
            addLabel: this.translatePipe.transform("Add"),
            applyLabel: this.translatePipe.transform("Apply"),
            cancelLabel: this.translatePipe.transform("Cancel"),
            closeLabel: this.translatePipe.transform("Close"),
            fieldLabel: this.translatePipe.transform("Field"),
            filterValueLabel: this.translatePipe.transform("FilterValue"),
            newFilterLabel: this.translatePipe.transform("NewFilter"),
            operatorLabel: this.translatePipe.transform("Operator"),
            orLabel: this.translatePipe.transform("Or"),
            removeFilterLabel: this.translatePipe.transform("RemoveFilters"),
            saveLabel: this.translatePipe.transform("Save"),
        };
    }

    toggleFilterPane(): void {
        this.isFilterOpen = !this.isFilterOpen;
    }

    pageChanged(newPage: number) {
        this.currentPage = newPage;
    }

    handleTableSelectClick(actionType, rowId) {
        switch (actionType) {
            case "header":
                this.allRowSelected = !this.allRowSelected;
                this.cookies.forEach((elem) => {
                    this.selectedCookieListMap[elem.id] = this.allRowSelected;
                });
                break;
            case "row":
                this.selectedCookieListMap[rowId] = !this.selectedCookieListMap[rowId];
                this.allRowSelected = this.checkIfAllSelected();
                break;
        }
    }

    private checkIfAllSelected(): boolean {
        const keys = Object.keys(this.selectedCookieListMap);
        if (keys.length === this.cookies.length) {
            return keys.every((key) => this.selectedCookieListMap[key]);
        } else {
            return false;
        }
    }

    private onExportCookies() {
        // TODO
    }
}
