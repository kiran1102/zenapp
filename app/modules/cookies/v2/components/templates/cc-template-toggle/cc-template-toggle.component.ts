import { Component,
        EventEmitter,
        Inject,
        Input,
        Output,
        OnInit } from "@angular/core";
import { IBannerTemplate } from "interfaces/cookies/cc-banner-template.interface";
import { IConfirmationModalResolve } from "interfaces/confirmation-modal-resolve.interface";
import { CookiesModel } from "interfaces/cookies/cookies-model.interface";

import { CookieTemplateService } from "cookiesService/cookie-template.service";
import { ModalService } from "sharedServices/modal.service";

import { TranslatePipe } from "pipes/translate.pipe";
import { CookiesStorageService } from "modules/cookies/services/helper/cookies-storage.service";

@Component({
    selector: "cc-template-toggle",
    templateUrl: "./cc-template-toggle.component.html",
})
export class TemplateToggleComponent implements OnInit {
    @Input() public domainId: number;
    @Input() public templateId: number;
    @Input() public templateName: string;
    @Input() public useTemplate: boolean;
    @Input() public unLinkTemplate: boolean;
    @Output() public unLinkTemplateCallback = new EventEmitter();
    @Output() public reloadWebsiteListCallback = new EventEmitter();
    @Output() public pageReadyCallback = new EventEmitter();

    public templates: IBannerTemplate[] = [];
    public selectedOption: IBannerTemplate;
    public successCallbackTrigger: boolean;
    private model: CookiesModel;

    constructor(
        public cookieTemplateService: CookieTemplateService,
        public modalService: ModalService,
        private translatePipe: TranslatePipe,
        @Inject("$rootScope") private $rootScope: any,
    ) {}

    ngOnInit(): void {
        if (!this.unLinkTemplate) {
            if (this.useTemplate) {
                this.getTemplates();
                if (!this.selectedOption) {
                    this.selectedOption = this.templates[0];
                }
            }
        }
    }

    ngOnChanges(): void {
        if (!this.unLinkTemplate) {
            if (this.useTemplate) {
                this.getTemplates();
                if (!this.selectedOption) {
                    this.selectedOption = this.templates[0];
                }
            }
        } else if (!this.templateId) {
            this.unLinkTemplateCallback.emit(false);
        } else {
            this.unLinkTemplateModal();
        }
    }

    public selectOption(value: IBannerTemplate): void {
        this.pageReadyCallback.emit(false);
        this.selectedOption = value;
        this.templateId = this.selectedOption.templateId;
        this.cookieTemplateService.assignDomainToTemplate(this.templateId, this.domainId)
            .then((): boolean => {
                this.reloadWebsiteListCallback.emit(true);
                this.pageReadyCallback.emit(true);
                this.$rootScope.$emit("ccLoadWebsitesList");
                return true;
            });
    }

    public unLinkTemplateModal = (): void => {
        this.successCallbackTrigger = false;
        const modalData: IConfirmationModalResolve = {
            modalTitle: this.translatePipe.transform("UnlinkTemplateConfirmTitle"),
            confirmationText: `${this.translatePipe.transform("CookieTemplateUnlinkMessage")}<br></br><b>${this.translatePipe.transform("AreYouSureContinue")}</b>`,
            cancelButtonText: this.translatePipe.transform("Cancel"),
            submitButtonText: this.translatePipe.transform("UnLink"),
            iconClass: "fa fa-exclamation-circle fa-2x text-warning padding-bottom-2",
            promiseToResolve:
                (): ng.IPromise<boolean> => this.cookieTemplateService.unLinkTemplate(this.templateId, this.domainId)
                    .then((): boolean => {
                        this.reloadWebsiteListCallback.emit(true);
                        this.unLinkTemplateCallback.emit(false);
                        this.successCallbackTrigger = true;
                        this.$rootScope.$emit("ccLoadWebsitesList");
                        return true;
                    }),
            callback: (): void => {
                if (!this.successCallbackTrigger && this.selectedOption) {
                    this.unLinkTemplateCallback.emit(true);
                }
            },
        };

        this.modalService.setModalData(modalData);
        this.modalService.openModal("confirmationModal");
    }

    private getTemplates(): void {
        this.pageReadyCallback.emit(false);
        this.cookieTemplateService.getAllTemplates()
            .then((response: IBannerTemplate[]): void => {
                if (response) {
                    this.templates = response;
                    this.selectedOption = {templateId: this.templateId, templateName: this.templateName};
                    this.pageReadyCallback.emit(true);
                }
            });
    }

}
