// Angular
import {
    Component,
    EventEmitter,
    Input,
    Output,
    SimpleChanges,
} from "@angular/core";

// 3rd Party
import {
    each,
    includes,
    remove,
} from "lodash";

// Interfaces
import { IBannerLayoutProperties } from "interfaces/cookies/cc-banner-template.interface";
import { ITemplateJson } from "interfaces/cookies/cc-template-json.interface";

// Services
import { CookieConsentService } from "cookiesService/cookie-consent.service";
import { Permissions } from "modules/shared/services/helper/permissions.service";
import { CookiesStorageService } from "modules/cookies/services/helper/cookies-storage.service";
import { CookiesModel } from "interfaces/cookies/cookies-model.interface";

// Enums
import { EditableCookieBannerItems } from "enums/cookies/cookie-banner.enum";

@Component({
    selector: "cc-banner-template",
    templateUrl: "./cc-banner-template.component.html",
})

export class BannerTemplate {
    @Input() templateJson: ITemplateJson;
    @Output() bannerContentFormCallback: EventEmitter<boolean> = new EventEmitter();
    @Output() validColorSelectionCallback: EventEmitter<boolean> = new EventEmitter();

    canShowCookieCenterTileLayout = this.permissions.canShow("CookieCenterTileLayout");
    layoutOptions: IBannerLayoutProperties[] = this.cookieConsentService.getLayoutOptions();
    isCenterLayout = false;
    previewBodyStyle = { "margin-right": "10rem" };
    itemBeingEdited: EditableCookieBannerItems = null;

    constructor(
        private readonly cookieConsentService: CookieConsentService,
        private readonly permissions: Permissions,
        private cookiesStorageService: CookiesStorageService,
    ) { }

    ngOnInit(): void {
        if (!this.canShowCookieCenterTileLayout) {
            remove(this.layoutOptions, (layout: IBannerLayoutProperties, index: number): boolean => layout.position === "center");
        }
        this.setLayout(this.templateJson.bannerModel.skinName);
        this.calculateMarginWidth();
    }

    ngOnChanges(changes: SimpleChanges): void {
        if (changes.templateJson && !changes.templateJson.isFirstChange()) {
            this.templateJson = changes.templateJson.currentValue;
            this.setLayout(this.templateJson.bannerModel.skinName);
            this.calculateMarginWidth();
        }
    }

    onLayoutSelected(index: number): void {
        each(this.layoutOptions, (layout: IBannerLayoutProperties): void => { layout.selected = false; });
        const selectedLayout = this.layoutOptions[index];
        const bannerModel = this.templateJson.bannerModel;
        selectedLayout.selected = true;
        this.isCenterLayout = includes(selectedLayout.position, "center");
        bannerModel.skinName = this.getSkinNameByLayout(selectedLayout.position, selectedLayout.theme);
        bannerModel.bannerStyle.top = selectedLayout.top;
        bannerModel.bannerStyle.bottom = selectedLayout.bottom;
        bannerModel.bannerStyle.left = selectedLayout.left;
        bannerModel.bannerStyle.right = selectedLayout.right;
        bannerModel.bannerStyle.width = selectedLayout.width;
        bannerModel.bannerStyle.height = selectedLayout.height;
        bannerModel.bannerStyle.transformOrigin = selectedLayout.transformOrigin;
        bannerModel.bannerStyle.transform = selectedLayout.transform;
        this.setLinkTag();
    }

    private getSkinNameByLayout(position: string, theme: string): string {
        let skinName = "";
        if (position === "center") {
            skinName = "default_flat_center";
        } else {
            skinName = includes(this.templateJson.bannerModel.skinName, "default_flat_") ? "default_flat_" : "default_responsive_alert_";
            skinName += includes(position, "top") ? "top" : "bottom";
        }
        skinName += "_two_button_";
        skinName += includes(theme, "light") ? "white" : "black";
        return skinName;
    }

    private setLinkTag(): void {
        /* TODO instead solution classes like .dark-theme etc that are dynamically set with ngClass*/
        const cssURL = `${this.templateJson.bannerModel.externalCssBaseUrl}/skins/${this.templateJson.bannerModel.skinName}/v2/css/optanon.css`;
        const linkTag: HTMLElement = document.getElementById("bannerExternalURL");
        linkTag.setAttribute("href", cssURL);
    }

    private setLayout(skinName: string): void {
        const skinPosition: string = this.cookieConsentService.getSkinPosition(skinName, this.canShowCookieCenterTileLayout);
        const skinTheme: string = includes(skinName, "white") ? "light" : "dark";
        each(this.layoutOptions, (layout: IBannerLayoutProperties, index: number): void => {
            if (skinPosition === layout.position && skinTheme === layout.theme) {
                this.onLayoutSelected(index);
            }
        });
    }

    private calculateMarginWidth(): void {
        const bannerModel = this.templateJson.bannerModel;
        const margin = (!bannerModel.showCookieSettings && !bannerModel.showAcceptCookies) ? "10rem"
            : (!bannerModel.showCookieSettings || !bannerModel.showAcceptCookies) ? "25rem" : "40rem";
        this.previewBodyStyle = { "margin-right": margin };
    }
}
