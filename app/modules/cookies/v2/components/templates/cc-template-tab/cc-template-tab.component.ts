// Angular
import {
    Component,
    Inject,
    OnInit,
} from "@angular/core";
import { StateService } from "@uirouter/core";

// 3rd Party
import {
    find,
    toInteger,
    includes,
} from "lodash";

// Interfaces
import {
    IBannerTemplate,
    ITemplateAddLanguageModalResolver,
} from "interfaces/cookies/cc-banner-template.interface";
import { ITabsNav } from "interfaces/one-tabs-nav.interface";
import {
    ITemplateJson,
    ICookieTemplate,
} from "interfaces/cookies/cc-template-json.interface";
import { ILanguage } from "interfaces/cookies/cc-banner-languages.interface";
import { IDropdownOption } from "interfaces/one-dropdown.interface";
import { IPublishTemplateModalResolver } from "interfaces/cookies/publish-template-modal.interface";
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";

// Services
import { CookieTemplateService } from "cookiesService/cookie-template.service";
import { CookieConsentService } from "cookiesService/cookie-consent.service";
import { ModalService } from "sharedServices/modal.service";
import { CookieComplianceStore } from "cookiesService/cookie-compliance-store.service";

// Constants
import { TemplateTabOptions } from "constants/cookie.constant";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

@Component({
    selector: "cc-template-tab",
    templateUrl: "./cc-template-tab.component.html",
})

export class TemplatesTabComponent implements OnInit {
    currentTab: string;
    isPageLoading = false;
    languageList: ILanguage[] = [];
    selectedLanguage: ILanguage = null;
    selectedTemplate: IBannerTemplate = null;
    tabOptions: ITabsNav[];
    templateId: number;
    templateJsonData: ITemplateJson;
    templates: IBannerTemplate[] = [];
    templateTabOptions: typeof TemplateTabOptions;
    validColorSelection = true;
    isBannerContentFormVaild = true;

    constructor(
        @Inject("$rootScope") public $rootScope: IExtendedRootScopeService,
        private stateService: StateService,
        private cookieTemplateService: CookieTemplateService,
        private modalService: ModalService,
        private cookieConsentService: CookieConsentService,
        private cookieComplianceStore: CookieComplianceStore,
        private translatePipe: TranslatePipe,
    ) { }

    ngOnInit(): void {
        this.templateTabOptions = TemplateTabOptions;
        this.currentTab = TemplateTabOptions.Banner;
        if (this.stateService.params.templateId) {
            this.templateId = toInteger(this.stateService.params.templateId);
            this.initializeTemplates();
        }
        this.tabOptions = [{
            text: this.translatePipe.transform("Banner"),
            id: TemplateTabOptions.Banner,
            action: (option: ITabsNav): void => this.changeTab(option),
        }, {
            text: this.translatePipe.transform("PreferenceCenter"),
            id: TemplateTabOptions.PreferenceCenter,
            action: (option: ITabsNav): void => this.changeTab(option),
        }];
    }

    initializeTemplates(): void {
        this.isPageLoading = true;
        // TODO subscriptions > promises
        const getTemplateByIdPromise: ng.IPromise<void> = this.getTemplateById();
        const getAlltemplatesPromise: ng.IPromise<void> = this.getAllTemplates();
        const getAllMappedLanguagesPromise: ng.IPromise<void> = this.getAllMappedLanguages();
        Promise.all([getTemplateByIdPromise, getAlltemplatesPromise, getAllMappedLanguagesPromise])
            .then((): void => {
                if (this.languageList.length) {
                    this.selectedLanguage = find(this.languageList, (language: ILanguage): boolean => language.id === this.selectedTemplate.defaultLanguage.id);
                }
                this.isPageLoading = false;
            });
    }

    publishBanner(): void {
        const modalData: IPublishTemplateModalResolver = {
            modalTitle: this.translatePipe.transform("PublishTemplate"),
            publishConfirmationText: this.translatePipe.transform("PublishConfirmationText"),
            cancelButtonText: this.translatePipe.transform("Cancel"),
            submitButtonText: this.translatePipe.transform("Publish"),
            templateId: this.templateId,
            callback: (): void => {
                this.initializeTemplates();
                this.$rootScope.$emit("ccInitCookieData", ["", "ccTemplatePublish"]);
            },
        };
        this.modalService.setModalData(modalData);
        this.modalService.openModal("publishTemplateModal");

    }

    selectTemplate(value: IBannerTemplate): void {
        this.stateService.go("zen.app.pia.module.cookiecompliance.views.manage", { templateId: value.templateId });
        this.templateId = value.templateId;
        this.initializeTemplates();
    }

    selectLanguage(selection: ILanguage): void {
        this.selectedLanguage = selection;
        this.isPageLoading = true;
        this.cookieConsentService.getDomainTemplateModel(this.templateId, this.selectedLanguage.id)
            .then((response: ITemplateJson): void => {
                if (!response) return null;
                this.templateJsonData = response;
                this.isPageLoading = false;
            });
    }

    addNewLanguage(): void {
        const modalData: ITemplateAddLanguageModalResolver = {
            templateId: this.templateId,
            templateJsonData: this.templateJsonData,
            callback: (selection: ILanguage): void => {
                this.reloadLanguageSelection(selection);
            },
        };
        this.modalService.setModalData(modalData);
        this.modalService.openModal("templateNewLanguage");
    }

    saveBannerPrefernceCenter(): void {
        const bannerModel = this.templateJsonData.bannerModel;
        if (!this.templateJsonData.bannerModel.showAddPolicyLink && (!bannerModel.cookiePolicyUrl || !bannerModel.cookiePolicyLinkText)) {
            bannerModel.cookiePolicyUrl = "";
            bannerModel.cookiePolicyLinkText = "";
        }
        this.isPageLoading = true;
        this.cookieTemplateService.updateBannerPreferenceCenterDetails(this.templateId, this.selectedLanguage.id, this.templateJsonData)
            .then((): void => {
                this.isPageLoading = false;
            });
    }

    validColorSelectionCallback(isValid: boolean): void {
        this.validColorSelection = isValid;
    }

    bannerContentFormCallback(isFormValid: boolean): void {
        this.isBannerContentFormVaild = isFormValid;
    }

    validateForm(): boolean {
        return this.validColorSelection && this.isBannerContentFormVaild;
    }

    private getTemplateById(): ng.IPromise<void> {
        return this.cookieConsentService.getTemplateById(this.templateId)
            .then((response: ICookieTemplate | null): void => {
                if (!response) return;
                const { bannerModel, prefCenterData, ...selectedTemplate } = response;
                this.selectedTemplate = selectedTemplate;
                this.templateJsonData = { bannerModel, prefCenterData };
            });
    }

    private getAllTemplates(): ng.IPromise<void> {
        if (this.templates.length) return;
        return this.cookieTemplateService.getAllTemplates().then(
            (response: IBannerTemplate[]): void => {
                if (response.length) {
                    this.templates = response;
                }
            });
    }

    private getAllMappedLanguages(): ng.IPromise<void> {
        return this.cookieTemplateService.getAllMappedLanguagesForTemplate(this.templateId)
            .then((response: ILanguage[]): void => {
                this.languageList = response;
            });
    }

    private reloadLanguageSelection(selection: ILanguage): void {
        this.isPageLoading = true;
        this.cookieTemplateService.getAllMappedLanguagesForTemplate(this.templateId)
            .then((response: ILanguage[]): void => {
                this.languageList = response;
                this.selectLanguage(selection);
                this.isPageLoading = false;
            });
    }

    private checkIsCenterTile(skinName: string): boolean {
        return includes(skinName, "default_flat_center");
    }

    private changeTab(option: ITabsNav): void {
        this.currentTab = option.id;
    }
}
