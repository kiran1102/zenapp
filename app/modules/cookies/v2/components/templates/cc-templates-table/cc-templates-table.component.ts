// Angular
import {
    Component,
    Inject,
    OnInit,
} from "@angular/core";
import { StateService } from "@uirouter/core";

// Services
import { ModalService } from "sharedServices/modal.service";
import { CookieTemplateService } from "cookiesService/cookie-template.service";

// Interfaces
import { IStringMap } from "interfaces/generic.interface";
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import { IDeleteConfirmationModalResolve } from "interfaces/delete-confirmation-modal-resolve.interface";
import { IPublishTemplateModalResolver } from "interfaces/cookies/publish-template-modal.interface";
import { IDropdownOption } from "interfaces/one-dropdown.interface";
import {
    ITemplateModalResolver,
    IBannerTemplate,
    IBannerTemplateColumns,
} from "interfaces/cookies/cc-banner-template.interface";

// Constants
import { TemplateStatus } from "constants/cookie.constant";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

@Component({
    selector: "cc-templates-table",
    templateUrl: "./cc-templates-table.component.html",
})

export class TemplatesTableComponent implements OnInit {
    public translate: (key: string) => string;
    public bordered = true;
    public title: string;
    public translations: IStringMap<string>;
    public addNewTemplate: string;
    public templateName: string;
    public isPageLoading = true;
    public selectedDomain: string;
    public rows: IBannerTemplate[];
    public columns: IBannerTemplateColumns[];
    public templateId: number;
    public menuClass: string;

    constructor(
        @Inject("$rootScope") public $rootScope: IExtendedRootScopeService,
        private stateService: StateService,
        public modalService: ModalService,
        public cookieTemplateService: CookieTemplateService,
        private translatePipe: TranslatePipe,
    ) {
        this.title = this.translatePipe.transform("Templates");
        this.addNewTemplate = this.translatePipe.transform("AddTemplate");
        this.translations = {
            modalTitle: this.translatePipe.transform("CreateTemplate"),
            createTemplateButton: this.translatePipe.transform("Create"),
            cancelButtonText: this.translatePipe.transform("Cancel"),
            templateNameLabel: this.translatePipe.transform("TemplateName"),
            organizationNameLabel: this.translatePipe.transform("Organization"),
            defaultLanguageLabel: this.translatePipe.transform("DefaultLanguage"),
            deleteTemplate: this.translatePipe.transform("DeleteTemplate"),
            confirmButtonText: this.translatePipe.transform("Confirm"),
            publishConfirmationText: this.translatePipe.transform("PublishConfirmationText"),
            submitButtonText: this.translatePipe.transform("Publish"),
            publishTemplateTitle: this.translatePipe.transform("PublishTemplate"),
            editTemplateTitle: this.translatePipe.transform("EditTemplate"),
        };
    }

    public columnTrackBy(column: IBannerTemplateColumns): string {
        return column.id;
    }

    ngOnInit(): void {
        this.columns = [
            { columnHeader: this.translatePipe.transform("TemplateName"), columnTitleKey: "templateTitle", cellValueKey: "templateName" },
            { columnHeader: this.translatePipe.transform("Organization"), columnTitleKey: "organization", cellValueKey: "organization" },
            { columnHeader: this.translatePipe.transform("DefaultLanguage"), columnTitleKey: "defaultLanguage", cellValueKey: "defaultLanguage" },
            { columnHeader: this.translatePipe.transform("CreatedBy"), columnTitleKey: "createdBy", cellValueKey: "createdBy" },
            { columnHeader: this.translatePipe.transform("CreatedDate"), columnTitleKey: "dateCreated", cellValueKey: "createdDate" },
            { columnHeader: this.translatePipe.transform("LastPublished"), columnTitleKey: "lastPublished", cellValueKey: "lastModifiedDate" },
            { columnHeader: this.translatePipe.transform("Status"), columnTitleKey: "statusText", cellValueKey: "status" },
        ];
        this.loadAllTemplates();
    }

    public loadAllTemplates(): void {
        this.isPageLoading = true;
        this.cookieTemplateService.getAllTemplates()
            .then((response: IBannerTemplate[]): void => {
                this.rows = response;
                this.rows.forEach((bannerDetails: IBannerTemplate): void => {
                    if (bannerDetails.status.name === TemplateStatus.Published_Drafted) {
                        bannerDetails.status.name = TemplateStatus.Published;
                    }
                    if (bannerDetails.status.name === TemplateStatus.Drafted) {
                        bannerDetails.status.name = TemplateStatus.Draft;
                    }
                });
                this.isPageLoading = false;
            });
    }

    public addTemplate = (): void => {
        const modalData: ITemplateModalResolver = {
            modalTitle: this.translations.modalTitle,
            createTemplateButton: this.translations.createTemplateButton,
            cancelButtonText: this.translations.cancelButtonText,
            templateNameLabel: this.translations.templateNameLabel,
            organizationNameLabel: this.translations.organizationNameLabel,
            defaultLanguageLabel: this.translations.defaultLanguageLabel,
            bannerTemplate: null,
        };
        this.modalService.setModalData(modalData);
        this.modalService.openModal("addTemplateModal");
    }

    public getEllipsisOptions(bannerTemplate: IBannerTemplate): () => IDropdownOption[] {
        return (): IDropdownOption[] => {
            return [
                {
                    text: this.translatePipe.transform("Publish"),
                    action: (): void => {
                        this.publishTemplate(bannerTemplate);
                    },
                },
                {
                    text: this.translatePipe.transform("Edit"),
                    action: (): void => {
                        this.editTemplate(bannerTemplate);
                    },
                },
                {
                    text: this.translatePipe.transform("Delete"),
                    action: (): void => {
                        this.deleteTemplate(bannerTemplate);
                    },
                },
            ];
        };
    }

    public publishTemplate(bannerTemplate: IBannerTemplate): void {
        const modalData: IPublishTemplateModalResolver = {
            modalTitle: this.translations.publishTemplateTitle,
            publishConfirmationText: this.translations.publishConfirmationText,
            cancelButtonText: this.translations.cancelButtonText,
            submitButtonText: this.translations.submitButtonText,
            templateId: bannerTemplate.templateId,
            callback: (): void => {
                this.loadAllTemplates();
                this.$rootScope.$emit("ccInitCookieData", ["", "ccTemplatePublish"]);
            },
        };
        this.modalService.setModalData(modalData);
        this.modalService.openModal("publishTemplateModal");
    }
    public editTemplate(bannerTemplate: IBannerTemplate): void {
        const modalData: ITemplateModalResolver = {
            modalTitle: this.translations.editTemplateTitle,
            createTemplateButton: this.translations.createTemplateButton,
            cancelButtonText: this.translations.cancelButtonText,
            templateNameLabel: this.translations.templateNameLabel,
            organizationNameLabel: this.translations.organizationNameLabel,
            defaultLanguageLabel: this.translations.defaultLanguageLabel,
            bannerTemplate,
        };
        this.modalService.setModalData(modalData);
        this.modalService.openModal("editTemplateModal");
    }

    public deleteTemplate = (bannerTemplate: IBannerTemplate): void => {
        const modalData: IDeleteConfirmationModalResolve = {
            modalTitle: this.translations.deleteTemplate,
            confirmationText: this.translatePipe.transform("DeleteTemplateMessage", { TemplateDraftName: this.templateName }),
            cancelButtonText: this.translations.cancelButtonText,
            submitButtonText: this.translations.confirmButtonText,
            promiseToResolve:
                (): ng.IPromise<boolean> => this.cookieTemplateService.deleteTemplate(bannerTemplate.templateId)
                    .then((response: boolean): boolean => {
                        if (response) {
                            this.isPageLoading = true;
                            this.cookieTemplateService.getAllTemplates()
                                .then((bannerTemplates: IBannerTemplate[]): void => {
                                    this.rows = bannerTemplates;
                                    this.isPageLoading = false;
                                });
                            return true;
                        } else {
                            modalData.confirmationText = this.translatePipe.transform("TemplateNameExist");
                        }
                    }),
        };
        this.modalService.setModalData(modalData);
        this.modalService.openModal("deleteConfirmationModal");
    }

    public getNewTemplate(template: IBannerTemplate): void {
        this.stateService.go("zen.app.pia.module.cookiecompliance.views.manage", { templateId: template.templateId });
    }

    private setMenuClass(event?: MouseEvent): void {
        this.menuClass = "actions-table__context-list";
        if ((window.innerHeight - 200) / 2 < (event.clientY - 200)) {
            this.menuClass += " actions-table__context-list--above";
        }
    }

}
