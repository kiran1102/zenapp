import {
    Component,
    OnInit,
    Inject,
    OnDestroy,
} from "@angular/core";
import { StateService } from "@uirouter/core";
import { Subscription } from "rxjs/Subscription";
import { find } from "lodash";

// OT
declare var OneConfirm: any;
declare var OneAlert: any;

// Services
import { CookiesStorageService } from "modules/cookies/services/helper/cookies-storage.service";
import { CookiePreferenceCenterService } from "cookiesService/cookie-preference-center.service";
import { CookieConsentService } from "cookiesService/cookie-consent.service";
import { Permissions } from "modules/shared/services/helper/permissions.service";
import { CookieComplianceStore } from "cookiesService/cookie-compliance-store.service";
import { WebAuditService } from "cookiesService/web-audit.service";
import { ToastService } from "@onetrust/vitreus";

// Interfaces
import { CookiesModel } from "interfaces/cookies/cookies-model.interface";
import { IBannerModel } from "interfaces/cookies/cc-banner-template.interface";
import { IBannerLayoutProperties } from "interfaces/cookies/cc-banner-template.interface";
import { ITemplateJson } from "interfaces/cookies/cc-template-json.interface";
import { IPrefCenterData } from "interfaces/cookies/cc-pref-center.interface";
import { CcConsentNotice } from "interfaces/cookies/cc-consent-notice.interface";
import { IDomain } from "interfaces/cookies/cc-domain.interface";
import { IBannerLanguage } from "interfaces/cookies/cc-banner-languages.interface";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

@Component({
    selector: "cc-banner-wrapper",
    templateUrl: "./cc-banner-wrapper.component.html",
})
export class CcBannerWrapperComponent implements OnInit, OnDestroy {
    // TODO BKEL 11/30/18 - a lot of this and the banner template component can probably be combined
    // Potentially a parent component can be made with the common methods, which each can extend
    model: CookiesModel;
    bannerModel: IBannerModel;
    useTemplate: boolean;
    layoutOptions: IBannerLayoutProperties[];
    isCenterLayout = false;
    templateJson: ITemplateJson = {
        bannerModel: null,
        prefCenterData: null,
    };
    pageLoading = false;
    isTopLayout = false;
    marginRight: any;
    unLinkTemplate: boolean;
    previewBodyStyle: any;
    domainId: any; // Need to make it either string or number in ALL places
    showCookieBannerTemplate = this.permissions.canShow("CookieReadBannerTemplates");

    templateId = 0;
    templateName = "";
    themeClass = "";
    formValid = false;

    private consentNotice: CcConsentNotice;
    private canShowCookieCenterTileLayout = this.permissions.canShow("CookieCenterTileLayout");
    private subscriptions: Subscription[] = [];
    private colorSelectionValid = false;
    private bannerContentValid = false;
    private loadingWebsites: boolean;

    constructor(
        @Inject("$rootScope") private $rootScope: any,
        @Inject("$scope") private $scope: any,
        private stateService: StateService,
        private permissions: Permissions,
        private cookieStorageService: CookiesStorageService,
        private cookiePreferenceCenterService: CookiePreferenceCenterService,
        private cookieConsentService: CookieConsentService,
        private cookieComplianceStore: CookieComplianceStore,
        private webAuditService: WebAuditService,
        private translatePipe: TranslatePipe,
        private toastService: ToastService,
    ) {}

    ngOnInit() {
        this.pageLoading = true;
        this.subscriptions.push(this.cookieStorageService.model.subscribe((model: CookiesModel) => {
            this.model = model;
            if (this.consentNotice) {
                this.checkUseTemplatesSwitch();
            }
            if (!model || !model.selectedWebsite) {
                this.$rootScope.$emit("ccInitCookieData", [null, null]);
            } else {
                this.getDefaultBannerModel();
                this.domainId = this.cookieComplianceStore.getSelectedDomainId();
                this.calculateMarginWidth();
                this.subscriptions.push(this.cookieStorageService.consentNotice.subscribe((consentNotice: CcConsentNotice) => {
                    this.consentNotice = consentNotice;
                    if (this.consentNotice) {
                        this.checkUseTemplatesSwitch();
                    }
                    if (this.model && this.model.selectedWebsite && this.consentNotice) {
                        this.loadInitialNotice();
                    }
                }));
                this.cookiePreferenceCenterService.getPreferenceCenter(this.domainId).then((response: IPrefCenterData): void => {
                    this.templateJson.prefCenterData = response;
                    this.checkIfLoading();
                });
                this.layoutOptions = this.getLayoutOptions();
                this.subscriptions.push(this.cookieStorageService.selectedLanguage.subscribe((lang: IBannerLanguage) => {
                    this.domainId = this.cookieComplianceStore.getSelectedDomainId();
                    this.loadInitialNotice();
                }));

                this.checkIfLoading();
            }
            this.cookieStorageService.bannerModel.subscribe((bannerModel: IBannerModel) => {
                this.bannerModel = bannerModel;
                if (bannerModel) {
                    const textColor: string = this.cookieConsentService.getContrastTextColor(this.bannerModel.buttonStyle.backgroundColor);
                    this.bannerModel.buttonStyle.color = textColor;
                    this.calculateMarginWidth();
                    this.setThemeClass();
                }
            });
            if (model.selectedWebsite && this.consentNotice) {
                this.loadInitialNotice();
            }
        }));
        this.subscriptions.push(this.cookieStorageService.loadingWebsites.subscribe((loadingWebsites: boolean) => {
            this.loadingWebsites = loadingWebsites;
            this.checkIfLoading();
        }));
    }

    ngOnDestroy() {
        this.subscriptions.forEach((sub: Subscription) => {
            sub.unsubscribe();
        });
    }

    validateColor(valid: boolean) {
        this.colorSelectionValid = valid;
        this.validateForm();
    }

    validateContent(valid: boolean) {
        this.bannerContentValid = valid;
        this.validateForm();
    }

    goTo(route: string) {
        this.stateService.go(route);
    }

    loadInitialNotice(): void {
        if (!this.model.selectedWebsite) { return; }
        this.cookieComplianceStore.setSelectedWebsite(this.model.selectedWebsite);
        this.consentNotice.DomainId = this.domainId;
        this.consentNotice.DomainName = this.model.selectedWebsite;
        if (this.domainId) {
            this.cookieConsentService.getDomainModel(this.consentNotice.DomainId).then((data: any): void => {
                this.bannerModel = data;
                this.templateJson.bannerModel = data;
                this.setLayout(this.bannerModel.skinName);
                if (this.showCookieBannerTemplate) {
                    this.checkUseTemplatesSwitch();
                }
                if (!this.bannerModel.bannerCloseButtonText) {
                    this.bannerModel.bannerCloseButtonText = this.translatePipe.transform("Close");
                }
                this.cookieStorageService.bannerModel.next(this.bannerModel);
                this.checkIfLoading();
            });
        } else {
            this.cookieStorageService.createConsentBanner(this.model.selectedWebsite);
            this.cookieStorageService.bannerModel.next(this.bannerModel);
            this.checkIfLoading();
        }
    }

    revertToDefaultBanner(): void {
        OneConfirm(this.translatePipe.transform("RevertToDefault"), this.translatePipe.transform("AreYouSureYouWantToRevert"), "Confirm", "Ok", true, this.translatePipe.transform("Ok"), this.translatePipe.transform("Cancel")).then(
            (revert: boolean): void => {
                if (revert) {
                    this.pageLoading = true;
                    const languages: any[] = this.cookieComplianceStore.getBannerLanguages();
                    if (languages.length) {
                        const language: any = find(languages, (lang: any): any => lang.domainId === this.consentNotice.DomainId);
                        this.cookieConsentService.getDefaultBannerModel(language.language.id).then((response: any): void => {
                            this.bannerModel = response;
                            this.bannerModel.domainId = this.consentNotice.DomainId;
                            this.setLayout(this.bannerModel.skinName);
                            this.cookieStorageService.bannerModel.next(this.bannerModel);
                            this.templateJson.bannerModel = this.bannerModel;
                            this.themeClass = "cc-dark-theme";
                        });
                        this.cookieConsentService.changeDefaultGroups(this.consentNotice.DomainId);
                    } else {
                        this.bannerModel = this.cookieConsentService.getDefaultModel();
                        this.bannerModel.domainId = this.consentNotice.DomainId;
                        this.setLayout(this.bannerModel.skinName);
                        this.cookieStorageService.bannerModel.next(this.bannerModel);
                        this.templateJson.bannerModel = this.bannerModel;
                        this.themeClass = "cc-dark-theme";
                    }
                    this.setDefaultColors();
                    this.checkIfLoading();
                }
            });
    }

    saveConsentBanner(): void {
        this.pageLoading = true;
        this.layoutOptions.forEach((element: any): void => {
            if (element.selected) {
                this.consentNotice.Layout = element.position;
                this.consentNotice.Theme = element.theme;
            }
        });
        let promise: any;

        if (this.consentNotice.DomainId === 0) {
            promise = this.webAuditService.createConsentNotice(this.consentNotice).then((domain: any): void => {
                this.model.domainList.forEach((domains: any, index: number): void => {
                    if (domains.auditDomain === this.consentNotice.DomainName) {
                        this.consentNotice.DomainId = domain.domainId;
                        this.model.domainList[index].domainId = domain.domainId;
                    }
                });
                this.pageLoading = false;
            });
        } else {
            const editCookieBannerRequest: any = this.cookieConsentService.getEditCookieBannerRequestByBannerModel(this.bannerModel);
            editCookieBannerRequest.ContainsUnpublishedChanges = true;
            promise = this.webAuditService.updateBannerNotice(editCookieBannerRequest).then((res: any): void => {
                this.pageLoading = false;
            });
        }

        promise.then((): void => {
            this.bannerModel.containsUnpublishedChanges = true;
        });
    }

    toggleUseTemplatesSwitch(useTemplate: boolean): void {
        this.useTemplate = useTemplate;
        if (!this.useTemplate) {
            this.unLinkTemplate = true;
        } else {
            this.$rootScope.$emit("ccSetdomainHasTemplateAssigned", true);
            this.toastService.info(this.translatePipe.transform("Info"), this.translatePipe.transform("NotificationForUseTemplate"));
        }
    }

    unLinkTemplateCallback(useTemplate: boolean): void {
        this.useTemplate = useTemplate;
        this.unLinkTemplate = false;
    }

    loadReport() {
        this.pageLoading = true;
        this.domainId = this.cookieComplianceStore.getSelectedDomainId();
        this.loadInitialNotice();
        this.$rootScope.$emit("ccLoadWebsitesList");
        // TODO this will need to be solutioned in v2 to get out of cc.controller
        this.$scope.createOrManageBanner();
    }

    private setDefaultColors() {
        this.templateJson.bannerModel.buttonStyle.backgroundColor = "";
        this.templateJson.bannerModel.bannerStyle.backgroundColor = "";
        this.templateJson.bannerModel.textStyle.color = "";
    }

    private validateForm() {
        this.formValid = this.colorSelectionValid && this.bannerContentValid;
    }

    private setThemeClass(): void {
        let className = "";
        if (this.templateJson.bannerModel.skinName.indexOf("center") !== -1) {
            className += "cc-center-theme ";
        }
        if (this.templateJson.bannerModel.skinName.indexOf("black") !== -1) {
            className += "cc-dark-theme ";
        }
        if (this.templateJson.bannerModel.skinName.indexOf("white") !== -1) {
            className += "cc-light-theme ";
        }
        this.themeClass = className;
    }

    private getDefaultBannerModel() {
        this.bannerModel = this.cookieConsentService.getDefaultModel();
        this.templateJson.bannerModel = this.bannerModel;
    }

    private calculateMarginWidth(): void {
        let margin: string;
        if (!this.bannerModel.showCookieSettings && !this.bannerModel.showAcceptCookies) {
            margin = "10rem";
        } else if (!this.bannerModel.showCookieSettings || !this.bannerModel.showAcceptCookies) {
            margin = "25rem";
        } else {
            margin = "40rem";
        }
        this.previewBodyStyle = { "margin-right": margin };
    }

    private getAssignedTemplateDetails(): void {
        this.model.domainList.forEach((domains: IDomain): void => {
            if (domains.auditDomain === this.consentNotice.DomainName) {
                this.templateId = domains.templateId;
                this.templateName = domains.templateName;
            }
        });
    }

    private checkUseTemplatesSwitch(): void {
        this.getAssignedTemplateDetails();
        this.useTemplate = this.templateId !== 0;
    }

    private selectLayout(index: number): void {
        this.layoutOptions.forEach((layout: IBannerLayoutProperties): void => { layout.selected = false; });
        this.layoutOptions[index].selected = true;
        this.isTopLayout = this.layoutOptions[index].position.indexOf("top") !== -1;
        this.isCenterLayout = this.layoutOptions[index].position.indexOf("center") !== -1;
        const propertiesToSet = ["top", "bottom", "left", "right", "width", "height", "transformOrigin", "transform"];
        propertiesToSet.forEach((property: string) => {
            this.bannerModel.bannerStyle[property] = this.layoutOptions[index][property];
        });

        this.bannerModel.skinName = this.getSkinNameByLayout(this.layoutOptions[index].position, this.layoutOptions[index].theme);
        this.cookieStorageService.bannerModel.next(this.bannerModel);
    }

    private getSkinNameByLayout(position: string, theme: string): string {
        let skinName = "";
        if (position === "center") {
            skinName = "default_flat_center";
        } else {
            skinName = this.bannerModel.skinName.indexOf("default_flat_") !== -1 ? "default_flat_" : "default_responsive_alert_";
            skinName += position.indexOf("top") !== -1 ? "top" : "bottom";
        }
        skinName += "_two_button_";
        skinName += theme.indexOf("light") !== -1 ? "white" : "black";
        return skinName;
    }

    private setLayout(skinName: string): void {
        const skinPosition: string = this.cookieConsentService.getSkinPosition(skinName, this.canShowCookieCenterTileLayout);
        const skinTheme: string = skinName.indexOf("white") !== -1 ? "light" : "dark";
        this.layoutOptions.forEach((layout: IBannerLayoutProperties, index: number): void => {
            if (skinPosition === layout.position && skinTheme === layout.theme) {
                this.selectLayout(index);
            }
        });
        this.setThemeClass();
    }

    private checkIfLoading() {
        if (this.model && this.templateJson.bannerModel && this.templateJson.prefCenterData && this.consentNotice && this.layoutOptions && !this.loadingWebsites) {
            this.pageLoading = false;
        }
    }

    private getLayoutOptions(): IBannerLayoutProperties[] {
        const layoutOptions: IBannerLayoutProperties[] = this.cookieConsentService.getLayoutOptions();
        if (!this.canShowCookieCenterTileLayout) {
            layoutOptions.forEach((layout: IBannerLayoutProperties, index: number): void => {
                if (layout.position === "center") {
                    layoutOptions.splice(index, 1);
                }
            });
        }
        return layoutOptions;
    }
}
