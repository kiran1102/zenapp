// Angular
import {
    Component,
    Inject,
    Input,
    OnInit,
    OnDestroy,
    ViewChild,
    Output,
    EventEmitter,
} from "@angular/core";
import { Subscription } from "rxjs/Subscription";

// Interfaces
import { ITemplateJson } from "interfaces/cookies/cc-template-json.interface";

// Enums
import { EditableCookieBannerItems } from "enums/cookies/cookie-banner.enum";

// Services
import { CookiesStorageService } from "modules/cookies/services/helper/cookies-storage.service";
import { OtModalService, OtModalHostDirective, ModalSize } from "@onetrust/vitreus";
import { Subject } from "rxjs";
import { CcLivePreviewComponent } from "../../cc-live-preview/cc-live-preview.component";
import { TranslatePipe } from "modules/pipes/translate.pipe";

@Component({
    selector: "banner-preview",
    templateUrl: "./banner-preview.component.html",
})
export class BannerPreview implements OnInit, OnDestroy {
    @Input() bodyStyle: string;
    @Input() isCenterLayout: boolean;
    @Input() templateJson: ITemplateJson;
    @Input() isLivePreview = false;
    @Output() openPrefCenter = new EventEmitter<null>();

    @ViewChild(OtModalHostDirective) otModalHost: OtModalHostDirective;

    EDITABLE_ITEMS = EditableCookieBannerItems;
    itemBeingEdited: EditableCookieBannerItems;
    modalCloseEvent = new Subject<string>();
    previewTooltip = {
        content: this.translatePipe.transform("BannerMustSaveForLivePreview"),
        placement: "top",
    };

    private subscriptions: Subscription[] = [];

    constructor(
        private cookiesStorageService: CookiesStorageService,
        private otModalService: OtModalService,
        private translatePipe: TranslatePipe,
    ) { }

    ngOnInit() {
        // Hack because optanon legacy code is the worst
        if (this.isLivePreview && this.isCenterLayout) {
            this.templateJson.bannerModel.bannerStyle.transform = "";
        }
        this.subscriptions.push(this.cookiesStorageService.bannerItemBeingEdited.subscribe((item: EditableCookieBannerItems) => {
            this.itemBeingEdited = item;
        }));
    }

    ngOnDestroy() {
        this.subscriptions.forEach((sub: Subscription) => {
            sub.unsubscribe();
        });
    }

    openPreferenceCenter() {
        this.openPrefCenter.emit();
    }

    openLivePreview() {
        this.modalCloseEvent = this.otModalService
            .create(
                CcLivePreviewComponent,
                {
                    isComponent: true,
                    size: ModalSize.LARGE,
                },
                {
                    showPrefCenter: false,
                },
            );
    }
}
