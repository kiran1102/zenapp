// Angular
import {
    Component,
    Injectable,
    Input,
} from "@angular/core";

// Interfaces
import { ITemplateJson } from "interfaces/cookies/cc-template-json.interface";

// Enum
import { EditableCookieBannerItems } from "enums/cookies/cookie-banner.enum";

// Services
import { CookiesStorageService } from "modules/cookies/services/helper/cookies-storage.service";

@Component({
    selector: "banner-behavior-tab",
    templateUrl: "./banner-behavior-tab.component.html",
})

@Injectable()
export class BannerBehaviorTab {
    @Input() isCenterLayout: boolean;
    @Input() templateJson: ITemplateJson;
    @Input() isCookiesBannerPage: boolean;

    EDITABLE_ITEMS = EditableCookieBannerItems;

    constructor(
        private cookiesStorageService: CookiesStorageService,
    ) { }

    setItemBeingEdited(item: EditableCookieBannerItems) {
        this.cookiesStorageService.bannerItemBeingEdited.next(item);
    }
}
