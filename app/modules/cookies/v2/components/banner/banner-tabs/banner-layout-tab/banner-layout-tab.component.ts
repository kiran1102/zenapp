// Angular
import {
    Component,
    EventEmitter,
    Input,
    Output,
} from "@angular/core";

// Interfaces
import { ITemplateJson } from "interfaces/cookies/cc-template-json.interface";
import { IBannerLayoutProperties } from "interfaces/cookies/cc-banner-template.interface";

@Component({
    selector: "banner-layout-tab",
    templateUrl: "./banner-layout-tab.component.html",
})

export class BannerLayoutTab {
    @Input() layoutOptions: IBannerLayoutProperties[];
    @Input() templateJson: ITemplateJson;
    @Output() layoutSelectionCallback: EventEmitter<any> = new EventEmitter();
}
