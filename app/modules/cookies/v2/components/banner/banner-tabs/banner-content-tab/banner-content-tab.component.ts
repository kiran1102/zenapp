// Angular
import {
    Component,
    EventEmitter,
    Input,
    Output,
    OnInit,
    OnDestroy,
} from "@angular/core";
import {
    FormBuilder,
    FormGroup,
    Validators,
} from "@angular/forms";

import { Subscription } from "rxjs/Subscription";

// 3rd party
import { assign } from "lodash";

// Interfaces
import { ITemplateJson } from "interfaces/cookies/cc-template-json.interface";
import { IBannerModel } from "interfaces/cookies/cc-banner-template.interface";

// Enums
import { EditableCookieBannerItems } from "enums/cookies/cookie-banner.enum";

// Services
import { CookiesStorageService } from "modules/cookies/services/helper/cookies-storage.service";

export interface IBannerContentFormValues {
    bannerTitle: string;
    noticeText: string;
    showAddPolicyLink: boolean;
    cookiePolicyLinkText: string;
    cookiePolicyUrl: string;
    showCookieSettings: boolean;
    cookieSettings: string;
    showAcceptCookies: boolean;
    acceptCookies: string;
    footerDescriptionText: string;
}

@Component({
    selector: "banner-content-tab",
    templateUrl: "./banner-content-tab.component.html",
})

export class BannerContentTab implements OnInit, OnDestroy {
    @Input() isCenterLayout: boolean;
    @Input() templateJson: ITemplateJson;
    @Output() bannerContentFormCallback: EventEmitter<boolean> = new EventEmitter();

    bannerContentForm: FormGroup;
    EDITABLE_ITEMS = EditableCookieBannerItems;

    private subscriptions: Subscription[] = [];

    constructor(
        private fb: FormBuilder,
        private cookiesStorageService: CookiesStorageService,
    ) { }

    ngOnInit(): void {
        this.initForm();
    }

    ngOnDestroy() {
        this.subscriptions.forEach((sub: Subscription) => {
            sub.unsubscribe();
        });
    }

    setItemBeingEdited(item: EditableCookieBannerItems) {
        this.cookiesStorageService.bannerItemBeingEdited.next(item);
    }

    onCookiePolicyToggle(): void {
        this.bannerContentForm.controls["showAddPolicyLink"].setValue(!this.bannerContentForm.controls["showAddPolicyLink"].value);
        if (this.bannerContentForm.controls["showAddPolicyLink"].value) {
            this.bannerContentForm.controls["cookiePolicyLinkText"].enable();
            this.bannerContentForm.controls["cookiePolicyUrl"].enable();
            return;
        }
        this.bannerContentForm.controls["cookiePolicyLinkText"].disable();
        this.bannerContentForm.controls["cookiePolicyUrl"].disable();
    }

    onCookieSettingToggle(): void {
        this.bannerContentForm.controls["showCookieSettings"].setValue(!this.bannerContentForm.controls["showCookieSettings"].value);
        if (this.bannerContentForm.controls["showCookieSettings"].value) {
            this.bannerContentForm.controls["cookieSettings"].enable();
        } else {
            this.bannerContentForm.controls["cookieSettings"].disable();
        }
    }

    onAcceptCookieToggle(): void {
        this.bannerContentForm.controls["showAcceptCookies"].setValue(!this.bannerContentForm.controls["showAcceptCookies"].value);
        if (this.bannerContentForm.controls["showAcceptCookies"].value) {
            this.bannerContentForm.controls["acceptCookies"].enable();
        } else {
            this.bannerContentForm.controls["acceptCookies"].disable();
        }
    }
    private initForm(): void {
        const bannerModel = this.templateJson.bannerModel;
        this.bannerContentForm = this.fb.group({
            bannerTitle: [bannerModel.bannerTitle || ""],
            noticeText: [bannerModel.noticeText || "", [Validators.required]],
            showAddPolicyLink: [bannerModel.showAddPolicyLink],
            cookiePolicyLinkText: [{ value: bannerModel.cookiePolicyLinkText || "", disabled: !bannerModel.showAddPolicyLink }, [Validators.required]],
            cookiePolicyUrl: [{ value: bannerModel.cookiePolicyUrl || "", disabled: !bannerModel.showAddPolicyLink }, [Validators.required]],
            showCookieSettings: [bannerModel.showCookieSettings],
            cookieSettings: [{ value: bannerModel.cookieSettings || "", disabled: !bannerModel.showCookieSettings }, [Validators.required]],
            showAcceptCookies: [bannerModel.showAcceptCookies],
            acceptCookies: [{ value: bannerModel.acceptCookies || "", disabled: !bannerModel.showAcceptCookies }, [Validators.required]],
            footerDescriptionText: [this.templateJson.bannerModel.footerDescriptionText || "", [Validators.maxLength(300)]],
        });
        this.bannerContentFormCallback.emit(this.bannerContentForm.valid);
        this.subscriptions.push(this.bannerContentForm.valueChanges.subscribe((values: IBannerContentFormValues) => {
            this.templateJson.bannerModel = assign({}, this.templateJson.bannerModel, values);
            this.cookiesStorageService.bannerModel.next(this.templateJson.bannerModel);
            this.bannerContentFormCallback.emit(this.bannerContentForm.valid);
        }));
    }
}
