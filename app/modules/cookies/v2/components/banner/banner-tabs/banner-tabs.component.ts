// Angular
import {
    Component,
    EventEmitter,
    Inject,
    Input,
    Output,
    OnInit,
} from "@angular/core";

// 3rd party
import { each } from "lodash";

// Constants
import { CookieBannerAccordions } from "constants/cookies/cookie-banner.constant";

// Interfaces
import { ITemplateJson } from "interfaces/cookies/cc-template-json.interface";
import { IBannerLayoutProperties } from "interfaces/cookies/cc-banner-template.interface";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

export interface IBannerAccordions {
    id: string;
    identifier: string;
    isExpanded: boolean;
    name: string;
}
@Component({
    selector: "banner-tabs",
    templateUrl: "./banner-tabs.component.html",
})

export class BannerTabs implements OnInit {
    @Input() bannerLayoutOptions: IBannerLayoutProperties[];
    @Input() isCenterLayout: boolean;
    @Input() templateJson: ITemplateJson;
    @Input() isCookiesBannerPage: boolean;
    @Output() bannerContentFormCallback: EventEmitter<boolean> = new EventEmitter();
    @Output() layoutSelectionCallback: EventEmitter<number> = new EventEmitter();
    @Output() validColorSelectionCallback: EventEmitter<boolean> = new EventEmitter();

    accordions: IBannerAccordions[];
    bannerAccordions: typeof CookieBannerAccordions;

    constructor(
        private translatePipe: TranslatePipe,
    ) { }

    ngOnInit(): void {
        this.bannerAccordions = CookieBannerAccordions;
        this.accordions = [{
            id: CookieBannerAccordions.Layout,
            identifier: "CookieBannerLayoutTabToogle",
            isExpanded: false,
            name: this.translatePipe.transform("Layout"),
        }, {
            id: CookieBannerAccordions.Colors,
            identifier: "CookieBannerColorsTabToogle",
            isExpanded: false,
            name: this.translatePipe.transform("Colors"),
        }, {
            id: CookieBannerAccordions.Content,
            identifier: "CookieBannerContentTabToogle",
            isExpanded: false,
            name: this.translatePipe.transform("Content"),
        }, {
            id: CookieBannerAccordions.Behavior,
            identifier: "CookieBannerBehviorTabToogle",
            isExpanded: false,
            name: this.translatePipe.transform("Behavior"),
        }];
        this.bannerContentFormCallback.emit(true);
        this.validColorSelectionCallback.emit(true);
    }

    onAccordionToggle(selection: IBannerAccordions): void {
        each(this.accordions, (accordion: IBannerAccordions): void => {
            if (accordion.id === selection.id) {
                accordion.isExpanded = !accordion.isExpanded;
            } else {
                accordion.isExpanded = false;
            }
        });
    }
}
