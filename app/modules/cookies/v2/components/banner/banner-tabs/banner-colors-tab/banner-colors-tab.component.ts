// Angular
import {
    Component,
    EventEmitter,
    Input,
    Output,
    OnInit,
    OnDestroy,
} from "@angular/core";

import { Subscription } from "rxjs/Subscription";

// Interfaces
import { IColorOptions } from "interfaces/color-picker.interface.ts";
import { ITemplateJson } from "interfaces/cookies/cc-template-json.interface";
import { IBannerModel } from "interfaces/cookies/cc-banner-template.interface";

// Services
import { CookieConsentService } from "cookiesService/cookie-consent.service";
import { CookiesStorageService } from "modules/cookies/services/helper/cookies-storage.service";

// Constants
import { CookieBannerColorType } from "constants/cookies/cookie-banner.constant";
import { ColorPickerConstants } from "constants/cookies/cookie.constant";

// Enums
import { EditableCookieBannerItems } from "enums/cookies/cookie-banner.enum";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

@Component({
    selector: "banner-colors-tab",
    templateUrl: "./banner-colors-tab.component.html",
})

export class BannerColorsTab implements OnInit, OnDestroy {
    @Input() templateJson: ITemplateJson;
    @Output() validColorSelectionCallback: EventEmitter<boolean> = new EventEmitter();

    bannerColorType: typeof CookieBannerColorType;
    colorOptions: IColorOptions[] = this.cookieConsentService.getColorCode();
    defaultColorPickerValue = ColorPickerConstants.defaultColorPickerValue;
    isBannerBackgroundColorValid = true;
    isButtonBackGroundColorValid = true;
    isTextColorValid = true;
    EDITABLE_ITEMS = EditableCookieBannerItems;

    private subscriptions: Subscription[] = [];

    constructor(
        private translatePipe: TranslatePipe,
        private cookieConsentService: CookieConsentService,
        private cookiesStorageService: CookiesStorageService,
    ) { }

    ngOnInit(): void {
        this.bannerColorType = CookieBannerColorType;
        this.validColorSelectionCallback.emit(this.isButtonBackGroundColorValid && this.isBannerBackgroundColorValid && this.isTextColorValid);
    }

    ngOnDestroy() {
        this.subscriptions.forEach((sub: Subscription) => {
            sub.unsubscribe();
        });
    }

    setColor(color: string, type: string): void {
        if (color === this.defaultColorPickerValue) color = "";
        switch (type) {
            case CookieBannerColorType.ButtonBgcolor:
                this.templateJson.bannerModel.buttonStyle.backgroundColor = color;
                break;
            case CookieBannerColorType.BannerBgColor:
                this.templateJson.bannerModel.bannerStyle.backgroundColor = color;
                break;
            case CookieBannerColorType.TextColor:
                this.templateJson.bannerModel.textStyle.color = color;
                break;
        }
        this.cookiesStorageService.bannerModel.next(this.templateJson.bannerModel);
    }

    validateColor(valid: boolean, type: string): void {
        switch (type) {
            case CookieBannerColorType.ButtonBgcolor:
                this.isButtonBackGroundColorValid = valid;
                break;
            case CookieBannerColorType.BannerBgColor:
                this.isBannerBackgroundColorValid = valid;
                break;
            case CookieBannerColorType.TextColor:
                this.isTextColorValid = valid;
                break;
        }
        this.validColorSelectionCallback.emit(this.isButtonBackGroundColorValid && this.isBannerBackgroundColorValid && this.isTextColorValid);
    }

    setItemBeingEdited(item: EditableCookieBannerItems) {
        this.cookiesStorageService.bannerItemBeingEdited.next(item);
    }
}
