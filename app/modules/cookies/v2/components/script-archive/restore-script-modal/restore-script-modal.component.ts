// Angular
import {
    Component,
    Inject,
    OnInit,
} from "@angular/core";

import { StoreToken } from "tokens/redux-store.token";
import { getModalData } from "oneRedux/reducers/modal.reducer";

// Services
import { ModalService } from "sharedServices/modal.service";

// Interfaces
import {
    ICookieScriptVersionDiffResponse,
    IAggregator,
} from "interfaces/cookies/cookie-publish-script.interface";
import { IStore } from "interfaces/redux.interface";

@Component({
    selector: "restore-script-modal",
    templateUrl: "./restore-script-modal.component.html",
})
export class RestoreScriptModal implements OnInit {
    cssChangesCount = 0;
    cssFilesAggregator: IAggregator;
    javascriptChangesCount = 0;
    jsFilesAggregator: IAggregator;
    requireReConsent = false;
    restoreInprogress = false;
    scriptData: ICookieScriptVersionDiffResponse;
    showNotification = true;
    totalChanges = 0;
    private promiseToResolve: (requireConsent: boolean) => Promise<boolean>;

    constructor(
        @Inject(StoreToken) private store: IStore,
        private modalService: ModalService,
    ) { }

    ngOnInit() {
        const modalData = getModalData(this.store.getState());
        this.promiseToResolve = modalData.promiseToResolve;
        this.scriptData = modalData.scriptData;
        this.cssFilesAggregator = modalData.scriptData.cssFilesAggregator;
        this.jsFilesAggregator = modalData.scriptData.jsFilesAggregator;
        this.cssFilesAggregator.original = this.unminify(modalData.scriptData.cssFilesAggregator.original);
        this.cssFilesAggregator.revised = this.unminify(modalData.scriptData.cssFilesAggregator.revised);
        this.jsFilesAggregator.original = this.unminify(modalData.scriptData.jsFilesAggregator.original.replace(/</g, "&lt;").replace(/>/g, "&gt;"));
        this.jsFilesAggregator.revised = this.unminify(modalData.scriptData.jsFilesAggregator.revised.replace(/</g, "&lt;").replace(/>/g, "&gt;"));
    }

    onCloseNotificationClick(): void {
        this.showNotification = !this.showNotification;
    }

    getCssChangesCount(count: number): void {
        this.totalChanges += count;
        this.cssChangesCount = count;
    }

    getJsChangesCount(count: number): void {
        this.totalChanges += count;
        this.javascriptChangesCount = count;
    }

    closeModal(): void {
        if (this.restoreInprogress) return;
        this.modalService.closeModal();
    }

    onRequireConsentToggle(): void {
        this.requireReConsent = !this.requireReConsent;
    }

    onSubmit(): void {
        this.restoreInprogress = true;
        this.promiseToResolve(this.requireReConsent)
            .then((result: boolean): void => {
                this.restoreInprogress = false;
                if (result) {
                    this.modalService.closeModal();
                }
            });
    }

    private unminify(code) {
        code = code
            .split("\t").join("    ")
            .replace(/\s*{\s*/g, " {\n    ")
            .replace(/;\s*/g, ";\n    ")
            .replace(/,\s*/g, ", ")
            .replace(/[ ]*}\s*/g, "}\n")
            .replace(/\}\s*(.+)/g, "}\n$1")
            .replace(/\n    ([^:]+):\s*/g, "\n    $1: ")
            .replace(/([A-z0-9\)])}/g, "$1;\n}");
        return code;
    }
}
