// Angular
import {
    Component,
    Inject,
    OnInit,
} from "@angular/core";

// Services
import { ScriptArchiveApiService } from "modules/cookies/services/api/script-archive-api.service";
import { CookieComplianceStore } from "cookiesService/cookie-compliance-store.service";

// Interfaces
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import { IScriptArchiveDetail } from "interfaces/cookies/cc-script-archive.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";

@Component({
    selector: "script-archive-wrapper",
    templateUrl: "./script-archive-wrapper.component.html",
})
export class ScriptArchiveWrapper implements OnInit {
    selectedWebsite;
    domainId: number;
    isLoaded = false;
    archiveList: IScriptArchiveDetail[] = [];

    constructor(
        @Inject("$rootScope") private $rootScope: IExtendedRootScopeService,
        private cookieComplianceStore: CookieComplianceStore,
        private scriptArchiveApi: ScriptArchiveApiService,
    ) { }

    ngOnInit() {
        // TODO: to be removed after cc.controller is upgraded
        this.selectedWebsite = this.cookieComplianceStore.getSelectedWebsite();
        if (!this.selectedWebsite) {
            this.$rootScope.$emit("ccInitCookieData", [this.selectedWebsite, "ccScriptArchive"]);
            this.$rootScope.$on("ccResumeScriptArchive", (): void => {
                this.populateData();
            });
        } else {
            this.populateData();
        }
    }

    private populateData() {
        this.isLoaded = false;
        this.domainId = this.cookieComplianceStore.getSelectedDomainId();
        this.selectedWebsite = this.cookieComplianceStore.getSelectedWebsite();
        if (!this.domainId) {
            this.isLoaded = true;
            return;
        }
        this.scriptArchiveApi.getScriptHistory(this.domainId)
            .then((response: IProtocolResponse<IScriptArchiveDetail[]>): void => {
                this.isLoaded = true;
                this.archiveList = response.data;
                this.archiveList.reverse();
            });
    }
}
