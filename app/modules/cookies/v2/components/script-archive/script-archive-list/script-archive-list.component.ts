declare var OneConfirm: any;

// Angular
import {
    Component,
    OnInit,
    Input,
} from "@angular/core";
import { IPromise } from "angular";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

// Services
import { ModalService } from "sharedServices/modal.service";
import { ScriptArchiveApiService } from "modules/cookies/services/api/script-archive-api.service";
import { Permissions } from "modules/shared/services/helper/permissions.service";

// Consants
import { ScriptArchiveColumnTypes } from "constants/cookies/script-archive.constant";

// Interfaces
import { IScriptArchiveDetail } from "interfaces/cookies/cc-script-archive.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";
import { ICookieScriptVersionDiffResponse } from "interfaces/cookies/cookie-publish-script.interface";

@Component({
    selector: "script-archive-list",
    templateUrl: "./script-archive-list.component.html",
})
export class ScriptArchiveList implements OnInit {
    @Input() archiveList: IScriptArchiveDetail[];
    sortKey = 1;
    sortOrder = "desc";
    sortable = true;
    columnTypes: typeof ScriptArchiveColumnTypes;
    archiveColumns = [
        { name: this.translatePipe.transform("PublishedDate"), sortKey: 1, type: ScriptArchiveColumnTypes.PublishedDate, cellValueKey: "createDate" },
        { name: this.translatePipe.transform("PublishedByTitle"), type: ScriptArchiveColumnTypes.PublishedBy, cellValueKey: "creatorFullName" },
        { name: this.translatePipe.transform("ReConsentRequired"), type: ScriptArchiveColumnTypes.ReConsentRequired, cellValueKey: "requireReConsent" },
        { name: this.translatePipe.transform("Language"), type: ScriptArchiveColumnTypes.Language, cellValueKey: "culture" },
        { name: this.translatePipe.transform("ConsentModel"), type: ScriptArchiveColumnTypes.ConsentModel, cellValueKey: "consentModel" },
        { name: "", type: ScriptArchiveColumnTypes.Action, cellValueKey: "" },
    ];
    contextMenuOptions = [
        {
            text: this.translatePipe.transform("Restore"),
            otAutoId: "Restore",
            action: (row): void => {
                this.restoreScriptPublish(row);
            },
        },
    ];

    private restoreSelected = false;

    constructor(
        private translatePipe: TranslatePipe,
        private modalService: ModalService,
        private scriptArchiveApi: ScriptArchiveApiService,
        private permissions: Permissions,
    ) { }

    ngOnInit() {
        this.columnTypes = ScriptArchiveColumnTypes;
    }

    handleSortChange({ sortKey, sortOrder }): void {
        this.sortKey = sortKey;
        this.sortOrder = sortOrder;
        this.archiveList.reverse();
    }

    trackByFn(index: number, item: IScriptArchiveDetail): number {
        return item.archiveScriptId;
    }

    private restoreScriptPublish(row: IScriptArchiveDetail): void {
        if (this.restoreSelected) return;
        this.restoreSelected = true;
        if (this.permissions.canShow("CookieVersionDiffNotifications")) {
            row.restoreInProgress = true;
            this.scriptArchiveApi.getArchiveScriptVersionDiff(row.archiveScriptId)
                .then((response: IProtocolResponse<ICookieScriptVersionDiffResponse>): void => {
                    row.restoreInProgress = false;
                    this.restoreSelected = false;
                    if (!response.result) return;
                    this.modalService.setModalData({
                        scriptData: response.data,
                        promiseToResolve: (): IPromise<boolean> => {
                            return this.scriptArchiveApi.restorePublishedArchiveScript(row.archiveScriptId)
                                .then((archiveResponse): boolean => {
                                    return archiveResponse.result;
                                });
                        },
                    });
                    this.modalService.openModal("downgradeRestoreScriptModal");
                });
        } else {
            this.restoreSelected = false;
            OneConfirm(this.translatePipe.transform("Restore"), this.translatePipe.transform("RestoreScriptVersionWarning"),
                "Confirm", "Yes", true, this.translatePipe.transform("Yes"), this.translatePipe.transform("No"))
                .then((requireReconsent: boolean): void => {
                    if (requireReconsent) {
                        this.scriptArchiveApi.restorePublishedArchiveScript(row.archiveScriptId);
                    }
                });
        }
    }

}
