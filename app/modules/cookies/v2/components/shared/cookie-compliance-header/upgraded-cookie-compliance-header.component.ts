import {
    Directive,
    ElementRef,
    Injector,
    Input,
    Output,
    EventEmitter,
} from "@angular/core";
import { UpgradeComponent } from "@angular/upgrade/static";

@Directive({
    selector: "upgraded-cookie-compliance-header",
})
export class UpgradedCookieComplianceHeader extends UpgradeComponent {

    /* TODO: see if we cant just use the cookie-page-header component */

    @Input() doScriptArchiveFilterForDomain;
    @Input() geoLocationSwitcherEnabled;
    @Input() hideLanguageAddButton;
    @Input() isScriptArchiveFilterOpen;
    @Input() isUnpublishedChanges;
    @Input() languageSwitcherEnabled;
    @Input() navigationTitle;
    @Input() pageName;
    @Input() showAdvanceSettingsButton;
    @Input() showLanguageSelection;
    @Input() showPublishButton;
    @Input() showScriptArchiveControls;
    @Input() useTemplate;
    @Output() domainChangeFunction: EventEmitter<any>;
    @Output() pageReadyCallback: EventEmitter<any>;
    @Output() restoreCookieListConfiguration: EventEmitter<any>;

    constructor(elementRef: ElementRef, injector: Injector) {
        super("ccCookieComplianceHeader", elementRef, injector);
    }

}
