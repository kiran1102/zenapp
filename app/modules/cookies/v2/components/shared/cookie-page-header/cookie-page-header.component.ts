// Angular
import {
    Component,
    Inject,
    Input,
    OnInit,
} from "@angular/core";
import { StateService } from "@uirouter/core";

// Interfaces
import {
    ICookiePageHeaderBreadcrumbItem,
    ICookiePageHeaderButtonConfiguration,
    ICookiePageHeaderBadgeConfiguration,
    ICookiePageHeaderContextMenuConfiguration,
} from "interfaces/cookies/cookie-page-header.interface";

@Component({
    selector: "cookie-header",
    templateUrl: "./cookie-page-header.component.html",
})
export class CookieHeader implements OnInit {
    @Input() pageTitle: string;
    @Input() showContent = true;
    @Input() headerType = "clear";
    @Input() breadcrumbItems: ICookiePageHeaderBreadcrumbItem[];
    @Input() primaryButton: ICookiePageHeaderButtonConfiguration;
    @Input() secondaryButton: ICookiePageHeaderButtonConfiguration;
    @Input() badges: ICookiePageHeaderBadgeConfiguration[] = [];
    @Input() contextMenuOptions: ICookiePageHeaderContextMenuConfiguration[] = [];

    constructor(
        private stateService: StateService,
    ) { }

    ngOnInit() {
    }

    goToRoute(route: { path: string, params: any }) {
        this.stateService.go(route.path);
    }
}
