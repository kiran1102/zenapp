import {
    Directive,
    ElementRef,
    Input,
    OnInit,
    OnChanges,
    Output,
    EventEmitter,
} from "@angular/core";
import {
    DiffMatchPatchService,
    Diff,
} from "ng-diff-match-patch";

import { each } from "lodash";

@Directive({
    selector: "[otScriptDiff]",
})
export class OTScriptDiffDirective implements OnInit {
    @Input() left: string | number | boolean;
    @Input() right: string | number | boolean;
    @Input() includeEqualCode = false;
    @Output() totalCount: EventEmitter<number> = new EventEmitter();

    constructor(
        private el: ElementRef,
        private dmp: DiffMatchPatchService) { }

    ngOnInit(): void {
        this.updateHtml();
    }

    private updateHtml(): void {
        if (!this.left) {
            this.left = "";
        }
        if (!this.right) {
            this.right = "";
        }
        if (typeof this.left === "number" || typeof this.left === "boolean") {
            this.left = this.left.toString();
        }
        if (typeof this.right === "number" || typeof this.right === "boolean") {
            this.right = this.right.toString();
        }
        this.el.nativeElement.innerHTML = this.createHtml(
            this.dmp.getLineDiff(this.left, this.right));
    }

    private createHtml(diffs: Diff[]): string {
        let html: string;
        html = "<div>";
        const changesCount = this.calculateChanges(diffs);
        for (const diff of diffs) {
            if (diff[0] === 0 && this.includeEqualCode) {
                html += "<span class='equal'>" + diff[1] + "</span>";
            }
            if (diff[0] === -1) {
                html += "<div class='del'>" + diff[1] + "</div>";
            }
            if (diff[0] === 1) {
                html += "<div class='ins'>" + diff[1] + "</div>";
            }
        }
        if (html === "<div>") {
            html += "<span class='equal'>" + diffs[0][1].substring(0, 400) + "</span>";
        }
        html += "</div>";
        this.totalCount.emit(changesCount);
        return html;
    }

    private calculateChanges(diffs: Diff[]): number {
        let count = 0;
        let valueFlag = 0;
        each(diffs, (diff: Diff): void => {
            if (diff[0] === 0 && valueFlag !== 0) {
                count++;
            } else {
                valueFlag += diff[0];
                if (valueFlag === 0 && diff[0] !== 0) {
                    count++;
                }
            }
        });
        return count;
    }
}
