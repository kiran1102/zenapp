// Angular
import {
    Component,
    Inject,
    OnInit,
} from "@angular/core";

// 3rd party
import {
    each,
    find,
    findIndex,
    some,
} from "lodash";
import { saveAs } from "file-saver";

// Redux
import { getModalData } from "oneRedux/reducers/modal.reducer";
import { StoreToken } from "tokens/redux-store.token";

// Services
import { ModalService } from "sharedServices/modal.service";
import { Permissions as CookiePermissions } from "modules/shared/services/helper/permissions.service";

// Interfaces
import {
    ICookieScriptVersionDiffResponse,
    IJqueryVersionDetail,
    IAggregator,
} from "interfaces/cookies/cookie-publish-script.interface";
import { IStore } from "interfaces/redux.interface";
import { IBannerLanguage } from "interfaces/cookies/cc-banner-languages.interface";
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import { IPublishScriptModalResolver } from "interfaces/cookies/publish-script-modal-resolver.interface";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";
import { IStringMap } from "interfaces/generic.interface";

export interface IMultipleSelectLanguagesPayload {
    change: IBannerLanguage;
    currentValue: IBannerLanguage[];
    previousValue: IBannerLanguage[];
}

@Component({
    selector: "cookie-publish-script-modal",
    templateUrl: "./cookie-publish-script-modal.component.html",
})
export class CookiePublishScriptModal implements OnInit {
    bannerLanguages: IBannerLanguage[];
    cssChangesCount = 0;
    cssFilesAggregator: IAggregator;
    defaultJuqeryVersion: IJqueryVersionDetail;
    domainId: number;
    javascriptChangesCount = 0;
    jqueryVersions: IJqueryVersionDetail[];
    jsFilesAggregator: IAggregator;
    publishIndividualLanguage: boolean;
    requireReConsent = false;
    scriptData: ICookieScriptVersionDiffResponse;
    selectedJqueryVerion: IJqueryVersionDetail;
    selectedLanguages: IBannerLanguage[];
    showNotification = true;
    totalChanges = 0;
    permissions: IStringMap<boolean>;

    private promiseToResolve: (requireConsent: boolean, jqueryVersionId: number, domainIds: number[]) => void;

    constructor(
        @Inject(StoreToken) private store: IStore,
        private translatePipe: TranslatePipe,
        private modalService: ModalService,
        private cookiePermissions: CookiePermissions,
    ) { }

    ngOnInit() {
        this.selectedLanguages = [];
        const modalData: IPublishScriptModalResolver = getModalData(this.store.getState());
        this.promiseToResolve = modalData.promiseToResolve;
        this.scriptData = modalData.scriptData;
        this.jqueryVersions = modalData.jqueryVersions;
        this.domainId = modalData.currentDomainId;
        this.bannerLanguages = modalData.bannerLanguages;
        this.permissions = {
            nojQueryScriptEnabled: this.cookiePermissions.canShow("CookieNoJquery"),
        };
        if (this.jqueryVersions.length) {
            this.populateVersion();
        }
        if (this.scriptData) {
            this.cssFilesAggregator = { ...modalData.scriptData.cssFilesAggregator };
            this.jsFilesAggregator = { ...modalData.scriptData.jsFilesAggregator };
            this.cssFilesAggregator.original = this.unminify(this.cssFilesAggregator.original);
            this.cssFilesAggregator.revised = this.unminify(this.cssFilesAggregator.revised);
            this.jsFilesAggregator.original = this.unminify(this.jsFilesAggregator.original.replace(/</g, "&lt;").replace(/>/g, "&gt;"));
            this.jsFilesAggregator.revised = this.unminify(this.jsFilesAggregator.revised.replace(/</g, "&lt;").replace(/>/g, "&gt;"));
            this.publishIndividualLanguage = !modalData.scriptData.languageDetectionToggle;
            if (this.publishIndividualLanguage) {
                this.populateLanguageSelection();
            }
        }
    }

    onPublish(): void {
        this.modalService.closeModal();
        let domainIds: number[];
        if (this.publishIndividualLanguage) {
            domainIds = this.selectedLanguages.map((language) => language.domainId);
        } else {
            domainIds = this.bannerLanguages.map((language) => language.domainId);
        }
        this.promiseToResolve(
            this.requireReConsent,
            this.selectedJqueryVerion.jqueryVersionId,
            domainIds,
        );
        this.modalService.clearModalData();
    }

    closeModal(): void {
        this.modalService.closeModal();
    }

    onJqueryVersionChange(selection: IJqueryVersionDetail): void {
        this.selectedJqueryVerion = selection;
    }

    onPublishIndividualLanguageToggle(): void {
        this.publishIndividualLanguage = !this.publishIndividualLanguage;
        this.publishIndividualLanguage ? this.populateLanguageSelection() : this.restoreLanguageSelection();
    }

    onRequireConsentToggle(): void {
        this.requireReConsent = !this.requireReConsent;
    }

    onLanguageSelection({ currentValue, change, previousValue }: IMultipleSelectLanguagesPayload): void {
        this.selectedLanguages = currentValue;
        if (!previousValue || currentValue.length > previousValue.length) {
            const optionIndex: number = findIndex(this.bannerLanguages, (language: IBannerLanguage): boolean => {
                return change.domainId === language.domainId;
            });
            this.bannerLanguages.splice(optionIndex, 1);
        } else if (change.domainId && !some(this.bannerLanguages, { domainId: change.domainId })) {
            this.bannerLanguages.push(change);
        }
    }

    onCloseNotificationClick(): void {
        this.showNotification = !this.showNotification;
    }

    getCssChangesCount(count: number): void {
        this.totalChanges += count;
        this.cssChangesCount = count;
    }

    getJsChangesCount(count: number): void {
        this.totalChanges += count;
        this.javascriptChangesCount = count;
    }

    onScirptDownlodClick(): void {
        this.downloadScriptData(this.cssFilesAggregator.original, this.cssFilesAggregator.originalFileName);
        if (this.cssChangesCount) {
            this.downloadScriptData(this.cssFilesAggregator.revised, this.cssFilesAggregator.revisedFileName);
        }
        this.downloadScriptData(this.scriptData.jsFilesAggregator.original, this.jsFilesAggregator.originalFileName);
        if (this.javascriptChangesCount) {
            this.downloadScriptData(this.scriptData.jsFilesAggregator.revised, this.jsFilesAggregator.revisedFileName);
        }
    }

    private populateLanguageSelection(): void {
        this.selectedLanguages = [];
        const currentDomainIndex: number = findIndex(this.bannerLanguages, (language: IBannerLanguage): boolean => {
            return language.domainId === this.domainId;
        });
        this.selectedLanguages.push(this.bannerLanguages[currentDomainIndex]);
        this.bannerLanguages.splice(currentDomainIndex, 1);
    }

    private restoreLanguageSelection(): void {
        each(this.selectedLanguages, (language: IBannerLanguage) => this.bannerLanguages.push(language));
        this.selectedLanguages = [];
    }

    private downloadScriptData(fileContent: string, fileName: string): void {
        const blob = new Blob([fileContent], {
            type: "text",
        });
        saveAs(blob, fileName);
    }

    private populateVersion(): void {
        this.selectedJqueryVerion = find(this.jqueryVersions, (version: IJqueryVersionDetail) => version.selected === true);
        this.defaultJuqeryVersion = find(this.jqueryVersions, (version: IJqueryVersionDetail) => version.jqueryVersionId === 1);
    }

    private unminify(code) {
        code = code
            .split("\t").join("    ")
            .replace(/\s*{\s*/g, " {\n    ")
            .replace(/;\s*/g, ";\n    ")
            .replace(/,\s*/g, ", ")
            .replace(/[ ]*}\s*/g, "}\n")
            .replace(/\}\s*(.+)/g, "}\n$1")
            .replace(/\n    ([^:]+):\s*/g, "\n    $1: ")
            .replace(/([A-z0-9\)])}/g, "$1;\n}");
        return code;
    }
}
