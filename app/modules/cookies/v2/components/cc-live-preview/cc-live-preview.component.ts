import { Component, OnInit } from "@angular/core";
import { IOtModalContent } from "@onetrust/vitreus";
import { Subject } from "rxjs";
import { ITemplateJson } from "interfaces/cookies/cc-template-json.interface";
import { CcPrefCenterService } from "../../services/cc-pref-center.service";
import { CookieComplianceStore } from "cookiesService/cookie-compliance-store.service";
import { CookiePolicyService } from "cookiesService/cookie-policy.service";
import { ICookieGroupDetail } from "interfaces/cookies/cookie-group-detail.interface";
import {
    IPrefCenterLanguageResources,
    IPrefCenterPreviewCategory,
    IPrefCenterData,
} from "interfaces/cookies/cc-pref-center.interface";
import { CookieGroup } from "constants/cookies/cookie-group.constant";
import { TranslatePipe } from "modules/pipes/translate.pipe";
import { CookieConsentService } from "cookiesService/cookie-consent.service";
import { IBannerLayoutProperties } from "interfaces/cookies/cc-banner-template.interface";

// This will be much better in the new V2 flow- temporary bandaid based on PSO request
@Component({
    selector: "cc-live-preview",
    templateUrl: "./cc-live-preview.component.html",
})
export class CcLivePreviewComponent implements IOtModalContent, OnInit {
    otModalCloseEvent: Subject<any>;
    otModalData: any;
    templateJson: ITemplateJson = {
        bannerModel: null,
        prefCenterData: null,
    };
    bodyStyle: any;
    isCenterLayout: boolean;
    themeClass: string;
    showPrefCenter: boolean;
    prefCenterData: IPrefCenterData;
    loading: boolean;
    policyGroupData: ICookieGroupDetail[];
    previewStyle: { cssURL: string; logoURL: string };
    categoryPreview: IPrefCenterPreviewCategory;
    isCenterTileSkin: boolean;

    constructor(
        private prefCenterService: CcPrefCenterService,
        private cookieComplianceStore: CookieComplianceStore,
        private cookiePolicyService: CookiePolicyService,
        private translatePipe: TranslatePipe,
        private cookieConsentService: CookieConsentService,
    ) { }

    ngOnInit() {
        this.loading = true;
        this.showPrefCenter = this.otModalData.showPrefCenter;
        const domainId = this.cookieComplianceStore.getSelectedDomainId();
        this.cookieConsentService.getDomainModel(domainId).then(
            (data: any): void => {
                this.templateJson.bannerModel = data;
                this.prefCenterService
                    .getPreferenceCenter(domainId)
                    .then((prefData) => {
                        this.cookiePolicyService
                            .getAssignedGroupByDomainId(domainId)
                            .then((response: ICookieGroupDetail[]): void => {
                                this.policyGroupData = response;
                                this.calculateMarginWidth();
                                this.prefCenterData = prefData;
                                this.setThemeClass();
                                this.setLayout();
                                const previewCssUrl = this.prefCenterService.getPreferencePreviewCssUrl(
                                    prefData.styling.style,
                                );
                                const previewLogoUrl = this.prefCenterService.getPreferencePreviewLogo(
                                    prefData.styling.style,
                                    prefData.styling.logoFileData,
                                );
                                this.previewStyle = {
                                    cssURL: previewCssUrl,
                                    logoURL: previewLogoUrl,
                                };
                                this.assignCategoriesForPreview();
                                this.cookieConsentService
                                    .getDomainModel(domainId)
                                    .then(
                                        (domainModel: any): void => {
                                            if (data.skinName) {
                                                this.isCenterTileSkin =
                                                    domainModel.skinName.indexOf("default_flat_center") !== -1;
                                            }
                                            this.loading = false;
                                        },
                                    );
                            },
                            );
                    });
            },
        );
    }

    togglePrefCenter() {
        this.showPrefCenter = !this.showPrefCenter;
    }

    closeModal() {
        this.otModalCloseEvent.next();
    }

    private setLayout() {
        const skinName = this.templateJson.bannerModel.skinName;
        const skinPosition: string = this.cookieConsentService.getSkinPosition(
            skinName,
            true,
        );
        const skinTheme: string =
            skinName.indexOf("white") !== -1 ? "light" : "dark";
        const layoutOptions = this.cookieConsentService.getLayoutOptions();
        layoutOptions.forEach(
            (layout: IBannerLayoutProperties, index: number): void => {
                if (
                    skinPosition === layout.position &&
                    skinTheme === layout.theme
                ) {
                    this.isCenterLayout =
                        layoutOptions[index].position.indexOf("center") !== -1;
                }
            },
        );
        if (skinName.indexOf("top") !== -1) {
          this.templateJson.bannerModel.bannerStyle.top = "0";
          this.templateJson.bannerModel.bannerStyle.transformOrigin = "left top";
          this.templateJson.bannerModel.bannerStyle.bottom = "inherit";
          this.templateJson.bannerModel.bannerStyle.left = "0";
          this.templateJson.bannerModel.bannerStyle.right = "0";
        }
    }

    private assignCategoriesForPreview() {
        if (this.prefCenterData.languageResources.length) {
            this.categoryPreview = {
                StrictlyNecessaryCookies: this.findCookieGroupName(
                    CookieGroup.StrictlyNecessaryCookies,
                ),
                PerformanceCookies: this.findCookieGroupName(
                    CookieGroup.PerformanceCookies,
                ),
            };
        } else {
            this.categoryPreview = {
                StrictlyNecessaryCookies: this.translatePipe.transform(
                    "StrictlyNecessary",
                ),
                PerformanceCookies: this.translatePipe.transform(
                    "PerformanceCookies",
                ),
            };
        }
    }

    private findCookieGroupName(groupKey: string) {
        const languageResources = this.prefCenterData.languageResources;
        const language = languageResources.filter(
            (resource: IPrefCenterLanguageResources): boolean => resource.key === groupKey,
        );
        return language[0].value;
    }

    private setThemeClass() {
        let className = "";
        if (this.templateJson.bannerModel.skinName.indexOf("center") !== -1) {
            className += "cc-center-theme ";
        }
        if (this.templateJson.bannerModel.skinName.indexOf("black") !== -1) {
            className += "cc-dark-theme ";
        }
        if (this.templateJson.bannerModel.skinName.indexOf("white") !== -1) {
            className += "cc-light-theme ";
        }
        this.themeClass = className;
    }

    private calculateMarginWidth() {
        let margin: string;
        if (
            !this.templateJson.bannerModel.showCookieSettings &&
            !this.templateJson.bannerModel.showAcceptCookies
        ) {
            margin = "10rem";
        } else if (
            !this.templateJson.bannerModel.showCookieSettings ||
            !this.templateJson.bannerModel.showAcceptCookies
        ) {
            margin = "25rem";
        } else {
            margin = "40rem";
        }
        this.bodyStyle = { "margin-right": margin };
    }
}
