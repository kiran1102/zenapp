import {
    NgModule,
    CUSTOM_ELEMENTS_SCHEMA,
} from "@angular/core";
import { RouterModule } from "@angular/router";

// 3rd Party
import { DiffMatchPatchModule } from "ng-diff-match-patch";

// Modules
import { SharedModule } from "sharedModules/shared.module";
import { UpgradedLegacyModule } from "sharedModules/upgrade/upgraded-legacy.module";
import { CommonModule } from "@angular/common";
import { HttpClientModule } from "@angular/common/http";
import { FormsModule } from "@angular/forms";
import { ReactiveFormsModule } from "@angular/forms";
import { VitreusModule } from "@onetrust/vitreus";
import { DndListModule } from "ngx-drag-and-drop-lists";
import { OTPipesModule, PIPES } from "modules/pipes/pipes.module";
import { VirtualScrollModule } from "od-virtualscroll";
import { OneVideoModule } from "modules/video/video.module";

// Shared components
import { CookieHeader } from "cookiesV2Module/components/shared/cookie-page-header/cookie-page-header.component";
import { UpgradedCookieComplianceHeader } from "cookiesV2Module/components/shared/cookie-compliance-header/upgraded-cookie-compliance-header.component";
import { CookiePublishScriptModal } from "cookiesV2Module/components/shared/cookie-publish-script-modal/cookie-publish-script-modal.component";
// Cookie empty state component
import { CookieEmptyState } from "modules/cookies/components/shared/cookie-empty-state/cookie-empty-state.component";

// Categorization Components
import { CCCategorizationsWrapper } from "cookiesV2Module/components/cc-categorizations/cc-categorizations-wrapper/cc-categorizations-wrapper.component";
import { CCCategoryList } from "cookiesV2Module/components/cc-categorizations/cc-category-list/cc-category-list.component";
import { CCCategoryDetailModal } from "cookiesV2Module/components/cc-categorizations/cc-category-detail-modal/cc-category-detail-modal.component";
import { CCCookieList } from "modules/cookies/v2/components/cc-categorizations/cc-cookie-list/cc-cookie-list.component";

// Banner / Template Components
import { BannerTemplate } from "cookiesV2Module/components/templates/banner-template/cc-banner-template.component";
import { TemplatesTableComponent } from "cookiesV2Module/components/templates/cc-templates-table/cc-templates-table.component";
import { TemplatesTabComponent } from "cookiesV2Module/components/templates/cc-template-tab/cc-template-tab.component";
import { TemplateToggleComponent } from "cookiesV2Module/components/templates/cc-template-toggle/cc-template-toggle.component";
import { BannerTabs } from "cookiesV2Module/components/banner/banner-tabs/banner-tabs.component";
import { BannerLayoutTab } from "cookiesV2Module/components/banner/banner-tabs/banner-layout-tab/banner-layout-tab.component";
import { BannerColorsTab } from "cookiesV2Module/components/banner/banner-tabs/banner-colors-tab/banner-colors-tab.component";
import { BannerContentTab } from "cookiesV2Module/components/banner/banner-tabs/banner-content-tab/banner-content-tab.component";
import { BannerBehaviorTab } from "cookiesV2Module/components/banner/banner-tabs/banner-behavior-tab/banner-behavior-tab.component";
import { BannerPreview } from "cookiesV2Module/components/banner/banner-preview/banner-preview.component";
import { CcBannerWrapperComponent } from "cookiesV2Module/components/banner/cc-banner-wrapper/cc-banner-wrapper.component";
import { UpgradedPreferenceCenter } from "generalcomponent/Cookies/PreferenceCenter/upgraded-preference-center.directive";
import { PreferenceCenterPreviewComponent } from "../components/preference-center/preference-center-preview/preference-center-preview.component";

// Script archive components
import { RestoreScriptModal } from "cookiesV2Module/components/script-archive/restore-script-modal/restore-script-modal.component";
import { ScriptArchiveList } from "cookiesV2Module/components/script-archive/script-archive-list/script-archive-list.component";
import { ScriptArchiveWrapper } from "cookiesV2Module/components/script-archive/script-archive-wrapper/script-archive-wrapper.component";

// Services
import { CookieTemplateService } from "cookiesService/cookie-template.service";
import { CookieIABIntegrationService } from "cookiesService/cookie-iab-integration.service";
import { CookieComplianceStore } from "cookiesService/cookie-compliance-store.service";
import { WebAuditService } from "cookiesService/web-audit.service";
import { CookieConsentService } from "cookiesService/cookie-consent.service";
import { CookieMultiLanguageService } from "cookiesService/cookie-multi-language.service";
import { CookiePolicyService } from "cookiesService/cookie-policy.service";
import { CookieScriptIntegrationService } from "cookiesService/cookie-script-integration.service";
import { CookieLanguageSwitcherService } from "cookiesService/cookie-language-switcher.service";
import { ToastService } from "@onetrust/vitreus";
import { CookiePreferenceCenterService } from "cookiesService/cookie-preference-center.service";
import { LanguagePollingService } from "cookiesService/cookie-language-polling.service";
import { CookieListApiService } from "modules/cookies/services/api/cookie-list-api.service";
import { CookieListHelperService } from "modules/cookies/services/helper/cookie-list-helper.service";
import { CookieOrgGroupService } from "cookiesService/cookie-org-group.service";
import { CookieApiService } from "modules/cookies/shared/services/cookie-api.service";
import { CookieAuditScheduleService } from "cookiesService/cookie-audit-schedule.service";
import { ConsentPolicyApiService } from "modules/cookies/services/api/consent-policy-api.service";
import { ConsentPolicyHelperService } from "modules/cookies/services/helper/consent-policy-helper.service";
import { ScriptArchiveApiService } from "modules/cookies/services/api/script-archive-api.service";
import { CookieSideMenuHelperService } from "modules/cookies/services/helper/cookie-side-menu-helper.service";
import { ScanLoginApiService } from "modules/cookies/services/api/scan-login-api.service";
import { CookiesStorageService } from "modules/cookies/services/helper/cookies-storage.service";

// Directives
import { OTScriptDiffDirective } from "cookiesV2Module/components/shared/diff-directive/ot-script-diff.directive";
import { CcLivePreviewComponent } from "./components/cc-live-preview/cc-live-preview.component";
import { CcPrefCenterService } from "./services/cc-pref-center.service";

export function getToasterService(injector) {
    return injector.get("ToastService");
}

export function getCookiePreferenceCenterService(injector) {
    return injector.get("CookiePreferenceCenterService");
}

export function getLanguagePollingService(injector) {
    return injector.get("LanguagePollingService");
}

export function getLegacyCookieTemplateService(injector) {
    return injector.get("CookieTemplateService");
}

export function getLegacyCookieComplianceStore(injector) {
    return injector.get("CookieComplianceStore");
}

export function getLegacyWebAuditService(injector) {
    return injector.get("WebAuditService");
}

export function getLegacyCookieConsentService(injector) {
    return injector.get("CookieConsentService");
}

export function getLegacyCookieMultiLanguageService(injector) {
    return injector.get("CookieMultiLanguageService");
}

export function getIABCookieIntegrationService(injector) {
    return injector.get("CookieIABIntegrationService");
}

export function getCookiePolicyService(injector) {
    return injector.get("CookiePolicyService");
}

export function getCookieAuditScheduleService(injector) {
    return injector.get("CookieAuditScheduleService");
}

export function getLegacyCookieScriptIntegrationService(injector) {
    return injector.get("CookieScriptIntegrationService");
}

export function getLegacyCookieLanguageSwitcherService(injector) {
    return injector.get("CookieLanguageSwitcherService");
}

export function getLegacyCookieOrgGroupService(injector) {
    return injector.get("CookieOrgGroupService");
}

@NgModule({
    imports: [
        SharedModule,
        UpgradedLegacyModule,
        CommonModule,
        HttpClientModule,
        FormsModule,
        ReactiveFormsModule,
        VitreusModule,
        DndListModule,
        OTPipesModule,
        VirtualScrollModule,
        OneVideoModule,
        DiffMatchPatchModule,
        RouterModule,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA],
    declarations: [
        OTScriptDiffDirective,
        CookieHeader,
        CCCategorizationsWrapper,
        CCCategoryList,
        CCCategoryDetailModal,
        CCCookieList,
        CcBannerWrapperComponent,
        TemplatesTableComponent,
        TemplatesTabComponent,
        TemplateToggleComponent,
        BannerTemplate,
        BannerTabs,
        BannerLayoutTab,
        BannerColorsTab,
        BannerContentTab,
        BannerBehaviorTab,
        BannerPreview,
        UpgradedCookieComplianceHeader,
        RestoreScriptModal,
        ScriptArchiveWrapper,
        ScriptArchiveList,
        CookiePublishScriptModal,
        UpgradedPreferenceCenter,
        CookieEmptyState,
        CcLivePreviewComponent,
        PreferenceCenterPreviewComponent,
    ],
    entryComponents: [
        CCCategorizationsWrapper,
        CcBannerWrapperComponent,
        TemplatesTableComponent,
        TemplatesTabComponent,
        TemplateToggleComponent,
        BannerTemplate,
        RestoreScriptModal,
        ScriptArchiveWrapper,
        CookiePublishScriptModal,
        CCCategoryDetailModal,
        CookieEmptyState,
        CcLivePreviewComponent,
        PreferenceCenterPreviewComponent,
    ],
    providers: [
        { provide: CookieTemplateService, deps: ["$injector"], useFactory: getLegacyCookieTemplateService },
        { provide: CookieComplianceStore, deps: ["$injector"], useFactory: getLegacyCookieComplianceStore },
        { provide: WebAuditService, deps: ["$injector"], useFactory: getLegacyWebAuditService },
        { provide: CookieConsentService, deps: ["$injector"], useFactory: getLegacyCookieConsentService },
        { provide: CookieMultiLanguageService, deps: ["$injector"], useFactory: getLegacyCookieMultiLanguageService },
        { provide: CookieIABIntegrationService, deps: ["$injector"], useFactory: getIABCookieIntegrationService },
        { provide: CookiePolicyService, deps: ["$injector"], useFactory: getCookiePolicyService },
        { provide: CookieAuditScheduleService, deps: ["$injector"], useFactory: getCookieAuditScheduleService },
        { provide: CookieScriptIntegrationService, deps: ["$injector"], useFactory: getLegacyCookieScriptIntegrationService },
        { provide: ToastService, deps: ["$injector"], useFactory: getToasterService },
        { provide: CookieLanguageSwitcherService, deps: ["$injector"], useFactory: getLegacyCookieLanguageSwitcherService },
        { provide: CookiePreferenceCenterService, deps: ["$injector"], useFactory: getCookiePreferenceCenterService },
        { provide: LanguagePollingService, deps: ["$injector"], useFactory: getLanguagePollingService },
        { provide: CookieOrgGroupService, deps: ["$injector"], useFactory: getLegacyCookieOrgGroupService },
        CookieListApiService,
        CookieListHelperService,
        CookieApiService,
        ConsentPolicyApiService,
        ConsentPolicyHelperService,
        ScriptArchiveApiService,
        CookieSideMenuHelperService,
        ScanLoginApiService,
        CookiesStorageService,
        CcPrefCenterService,
        PIPES, // TODO: Remove from providers when ot-common is implemented. Only necessary if you are using pipe in TS file, else just module is fine
    ],
    exports: [
        RestoreScriptModal,
        CookiePublishScriptModal,
    ],
})
export class CookiesV2Module { }
