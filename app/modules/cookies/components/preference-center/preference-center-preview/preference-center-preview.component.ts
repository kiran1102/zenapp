import {
    Component,
    Input,
    Output,
    EventEmitter,
    ViewChild,
} from "@angular/core";
import { IPrefCenterData, IPrefCenterPreviewCategory } from "interfaces/cookies/cc-pref-center.interface";
import { IPreviewStyle } from "interfaces/cookies/cc-preview-script.interface";
import { EditablePreferenceItems } from "enums/cookies/preference-center.enum";
import { PreferenceStyles } from "constants/cookies/cookie-preference-center.constant";
import { ICookieGroupDetail } from "interfaces/cookies/cookie-group-detail.interface";
import { Subject } from "rxjs";
import { OtModalService, OtModalHostDirective, ModalSize } from "@onetrust/vitreus";
import { CcLivePreviewComponent } from "modules/cookies/v2/components/cc-live-preview/cc-live-preview.component";
import { TranslatePipe } from "modules/pipes/translate.pipe";

export const MENU_ITEMS = {
    YOUR_PRIVACY: "YOUR_PRIVACY",
};

@Component({
    selector: "preference-center-preview",
    templateUrl: "preference-center-preview.component.html",
})
export class PreferenceCenterPreviewComponent {
    @Input() preferenceCenterData: IPrefCenterData;
    @Input() policyGroupData: ICookieGroupDetail[];
    @Input() previewStyle: IPreviewStyle;
    @Input() categoryPreview: IPrefCenterPreviewCategory;
    @Input() useTemplate: boolean;
    @Input() isCenterTileSkin: boolean;
    @Input() itemBeingEdited: EditablePreferenceItems;
    @Input() isLivePreview: boolean;
    @Output() preferenceCenterDataChange: EventEmitter<IPrefCenterData> = new EventEmitter<IPrefCenterData>();
    @Output() closePrefCenter = new EventEmitter<null>();

    @ViewChild(OtModalHostDirective) otModalHost: OtModalHostDirective;

    PREFERENCE_ITEMS = EditablePreferenceItems;
    MENU_ITEMS = MENU_ITEMS;
    activeMenu: string = MENU_ITEMS.YOUR_PRIVACY;
    STYLES = PreferenceStyles;
    modalCloseEvent = new Subject<string>();
    previewTooltip = {
        content: this.translatePipe.transform("BannerMustSaveForLivePreview"),
        placement: "top",
    };

    constructor(
        private otModalService: OtModalService,
        private translatePipe: TranslatePipe,
    ) {
    }

    selectMenuItem(item: string) {
        this.activeMenu = item;
    }

    closePreferenceCenter() {
        this.closePrefCenter.emit();
    }

    openLivePreview() {
        this.modalCloseEvent = this.otModalService
            .create(
                CcLivePreviewComponent,
                {
                    isComponent: true,
                    size: ModalSize.LARGE,
                },
                {
                    showPrefCenter: true,
                },
            );
    }
}
