// angular
import {
    Component,
    Input,
    OnInit,
    Output,
    EventEmitter,
} from "@angular/core";

// interfaces
import {
    IPrefCenterDetails,
    IPreferenceCenterEmitData,
    IPreferenceCenterDetailsFormInput,
    IPreferenceCenterDetailsFormToggle,
} from "interfaces/cookies/cc-pref-center.interface";

// services
import { Permissions } from "modules/shared/services/helper/permissions.service";

// pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

// constants
import { PreferenceCenterFormDataTypes } from "constants/cookies/cookie-preference-center.constant";

// enums
import { PrefrenceCenterDetailsToggle } from "enums/cookies/preference-center.enum";

@Component({
    selector: "preference-center-edit-details",
    templateUrl: "preference-center-details.component.html",
})

export class PreferenceCenterEditDetails implements OnInit {

    showSubgroupToggle = this.permissions.canShow("CookieSubGroupToggle");
    showSubgroupPreference: boolean;
    showSubgroupPreferenceAndToggle: boolean;
    subgroupOptOut: boolean;
    formInputData: IPreferenceCenterDetailsFormInput[];
    formToggleData: IPreferenceCenterDetailsFormToggle[];
    formDataTypes = PreferenceCenterFormDataTypes;

    @Input() preferenceCenterContent: IPrefCenterDetails;
    @Input() isCenterTileSkin: boolean;
    @Input() showSubGroupToggles: boolean;

    @Output() callbackSubGroupOptOut: EventEmitter<IPreferenceCenterEmitData> = new EventEmitter<IPreferenceCenterEmitData>();

    constructor(
        private permissions: Permissions,
        private translatePipe: TranslatePipe,
    ) { }

    ngOnInit(): void {
        this.showSubgroupPreference = this.permissions.canShow("CookiePreferenceSubgroups") && !this.isCenterTileSkin;
        this.showSubgroupPreferenceAndToggle = this.showSubgroupToggle && this.showSubgroupPreference;
        if (this.showSubgroupPreferenceAndToggle) {
            this.subgroupOptOut = this.showSubGroupToggles;
        }

        this.createFormData();
    }

    preferenceCenterToggle(index: number): void {
        switch (index) {
            case PrefrenceCenterDetailsToggle.ShowCookiesList:
                this.toggleSwitchShowCookieList();
                break;
            case PrefrenceCenterDetailsToggle.ShowLinkToCookiepedia:
                this.preferenceCenterContent.linkToCookiepedia = !this.preferenceCenterContent.linkToCookiepedia;
                break;
            case PrefrenceCenterDetailsToggle.ShowSubGroupDescription:
                this.preferenceCenterContent.showSubGroupDescription = !this.preferenceCenterContent.showSubGroupDescription;
                break;
            case PrefrenceCenterDetailsToggle.ShowSubGroupCookies:
                this.preferenceCenterContent.showSubGroupCookies = !this.preferenceCenterContent.showSubGroupCookies;
                break;
            case PrefrenceCenterDetailsToggle.SubGroupOptOut:
                this.toggleSubGroupOptOut(true);
                break;
            case PrefrenceCenterDetailsToggle.ShowCloseButton:
                this.preferenceCenterContent.showPreferenceCenterCloseButton = !this.preferenceCenterContent.showPreferenceCenterCloseButton;
                break;
            default:
                break;
        }
    }

    private toggleSubGroupOptOut(isToggleTrigger: boolean): void {
        if (isToggleTrigger) {
            this.subgroupOptOut = !this.subgroupOptOut;
        }
        this.callbackSubGroupOptOut.emit({ toggleStatus: this.subgroupOptOut, isToggleTrigger });
    }

    private toggleSwitchShowCookieList(): void {
        this.preferenceCenterContent.showCookiesList = !this.preferenceCenterContent.showCookiesList;
        this.subgroupOptOut = false;
        this.callbackSubGroupOptOut.emit({ toggleStatus: false, isToggleTrigger: true });
    }

    private createFormData(): void {
        this.formInputData = [
            {
                errorMessage: this.translatePipe.transform("PleaseEnterYourPrivacyTitle"),
                id: "CookiePreferenceCenterContentTitleInput",
                label: this.translatePipe.transform("Title"),
                model: "title",
                name: "textTitle",
                placeholder: this.translatePipe.transform("EnterTitle"),
                required: false,
                type: PreferenceCenterFormDataTypes.INPUT,
                visible: true,
            },
            {
                errorMessage: this.translatePipe.transform("PleaseEnterYourPrivacyTitle"),
                id: "CookiePreferenceCenterContentPrivacyTitleInput",
                label: this.translatePipe.transform("YourPrivacyTitle"),
                model: "privacyTitle",
                name: "textPrivacyTitle",
                placeholder: this.translatePipe.transform("EnterYourPrivacyTitle"),
                required: true,
                type: PreferenceCenterFormDataTypes.INPUT,
                visible: true,
            },
            {
                errorMessage: this.translatePipe.transform("PleaseEnterPrivacyText"),
                id: "CookiePreferenceCenterContentPrivacyTextTextarea",
                label: this.translatePipe.transform("YourPrivacyText"),
                model: "privacyText",
                name: "textPrivacyText",
                placeholder: this.translatePipe.transform("EnterPrivacyText"),
                required: true,
                type: PreferenceCenterFormDataTypes.TEXTAREA,
                visible: true,
            },
            {
                errorMessage: this.translatePipe.transform("PleaseEnterCookiePolicyLink"),
                id: "CookiePreferenceCenterContentCookiePolicyLinkInput",
                label: this.translatePipe.transform("CookiePolicyLink"),
                model: "cookiePolicyLink",
                name: "textCookiePolicyLink",
                placeholder: this.translatePipe.transform("EnterCookiePolicyLink"),
                required: true,
                type: PreferenceCenterFormDataTypes.INPUT,
                visible: true,
            },
            {
                errorMessage: this.translatePipe.transform("PleaseEnterCookiePolicyURL"),
                id: "CookiePreferenceCenterContentCookiePolicyURLInput",
                label: this.translatePipe.transform("CookiePolicyURL"),
                model: "cookiePolicyURL",
                name: "textCookiePolicyURL",
                placeholder: this.translatePipe.transform("EnterCookiePolicyURL"),
                required: true,
                type: PreferenceCenterFormDataTypes.INPUT,
                visible: true,
            },
            {
                errorMessage: this.translatePipe.transform("PleaseEnterAllowAllCookiesButtonLabel"),
                id: "CookiePreferenceCenterContentAllowAllCookiesButtonLabelInput",
                label: this.translatePipe.transform("AllowAllCookiesButton"),
                model: "allowAllCookiesButtonLabel",
                name: "textAllowAllButton",
                placeholder: this.translatePipe.transform("EnterAllowAllCookiesButtonLabel"),
                required: true,
                type: PreferenceCenterFormDataTypes.INPUT,
                visible: true,
            },
            {
                errorMessage: this.translatePipe.transform("PleaseEnterSaveSettingsButtonLabel"),
                id: "CookiePreferenceCenterContentSaveSettingsButtonLabelInput",
                label: this.translatePipe.transform("SaveSettingsButton"),
                model: "saveSettingsButtonLabel",
                name: "textSaveSettingsButton",
                placeholder: this.translatePipe.transform("EnterSaveSettingsButtonLabel"),
                required: true,
                type: PreferenceCenterFormDataTypes.INPUT,
                visible: true,
            },
            {
                errorMessage: this.translatePipe.transform("PleaseEnterSaveSettingsButtonLabel"),
                id: "CookiePreferenceCenterContentActiveLabelTextInput",
                label: this.translatePipe.transform("ActiveLabel"),
                model: "activeLabelText",
                name: "textActiveLabelText",
                placeholder: this.translatePipe.transform("EnterActiveLabel"),
                required: true,
                type: PreferenceCenterFormDataTypes.INPUT,
                visible: !this.isCenterTileSkin,
            },
            {
                errorMessage: this.translatePipe.transform("PleaseEnterActiveLabel"),
                id: "CookiePreferenceCenterContentInactiveLabelTextInput",
                label: this.translatePipe.transform("InactiveLabel"),
                model: "inactiveLabelText",
                name: "textInactiveLabelText",
                placeholder: this.translatePipe.transform("EnterInactiveLabel"),
                required: true,
                type: PreferenceCenterFormDataTypes.INPUT,
                visible: !this.isCenterTileSkin,
            },
            {
                errorMessage: this.translatePipe.transform("PleaseEnterAlwaysActiveLabel"),
                id: "CookiePreferenceCenterContentAlwaysActiveLabelTextInput",
                label: this.translatePipe.transform("AlwaysActiveLabel"),
                model: "alwaysActiveLabelText",
                name: "textAlwaysActiveLabelText",
                placeholder: this.translatePipe.transform("EnterAlwaysActiveLabel"),
                required: true,
                type: PreferenceCenterFormDataTypes.INPUT,
                visible: true,
            },
            {
                errorMessage: null,
                id: "CookiePreferenceCenterContentCookiesUsedLabelInput",
                label: this.translatePipe.transform("CookiesUsedLabel"),
                model: "cookiesUsedLabelText",
                name: "textCookiesUsedLabelText",
                placeholder: this.translatePipe.transform("EnterCookiesUsedLabel"),
                required: false,
                type: PreferenceCenterFormDataTypes.INPUT,
                visible: !this.isCenterTileSkin,
            },
        ];

        this.formToggleData = [
            {
                visible: !this.isCenterTileSkin,
                label: this.translatePipe.transform("ShowCookiesList"),
                id: "CookiePreferenceCenterContentCookiesShowCookiesListToggle",
                tooltip: false,
                tooltipText: null,
                isChecked: () => this.preferenceCenterContent.showCookiesList,
                isDisabled: () => false,
            },
            {
                visible: !this.isCenterTileSkin,
                label: this.translatePipe.transform("LinkToCookiepedia"),
                id: "CookiePreferenceCenterContentLinkToCookiepediaToggle",
                tooltip: false,
                tooltipText: null,
                isChecked: () => this.preferenceCenterContent.showCookiesList && this.preferenceCenterContent.linkToCookiepedia,
                isDisabled: () => !this.preferenceCenterContent.showCookiesList,
            },
            {
                visible: this.showSubgroupPreference,
                label: this.translatePipe.transform("ShowSubGroupDescription"),
                id: "CookiePreferenceCenterContentShowSubGroupDescriptionToggle",
                tooltip: false,
                tooltipText: null,
                isChecked: () => this.preferenceCenterContent.showCookiesList && this.preferenceCenterContent.showSubGroupDescription,
                isDisabled: () => !this.preferenceCenterContent.showCookiesList,
            },
            {
                visible: this.showSubgroupPreference,
                label: this.translatePipe.transform("ShowSubGroupCookies"),
                id: "CookiePreferenceCenterContentShowSubGroupCookiesToggle",
                tooltip: false,
                tooltipText: null,
                isChecked: () => this.preferenceCenterContent.showCookiesList && this.preferenceCenterContent.showSubGroupCookies,
                isDisabled: () => !this.preferenceCenterContent.showCookiesList,
            },
            {
                visible: this.showSubgroupPreferenceAndToggle,
                label: this.translatePipe.transform("SubGroupOptOut"),
                tooltip: true,
                tooltipText: this.translatePipe.transform("SubGroupOptOutToolTip"),
                id: "CookiePreferenceCenterContentSubGroupOptOutToggle",
                isChecked: () => this.preferenceCenterContent.showCookiesList && this.subgroupOptOut,
                isDisabled: () => !this.preferenceCenterContent.showCookiesList,
            },
            {
                visible: true,
                label: this.translatePipe.transform("ShowCloseButton"),
                id: "CookiePreferenceCenterContentShowCloseButtonToggle",
                tooltip: false,
                tooltipText: null,
                isChecked: () => this.preferenceCenterContent.showPreferenceCenterCloseButton,
                isDisabled: () => false,
            },
        ];

    }
}
