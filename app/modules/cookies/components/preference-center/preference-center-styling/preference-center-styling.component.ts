declare var OneConfirm: any;

// angular
import {
    Component,
    Input,
    OnChanges,
    Output,
    EventEmitter,
    ViewChild,
    ElementRef,
    Renderer2,
} from "@angular/core";

// interfaces
import {
    IPrefCenterStyling,
    IPreferenceCenterMinicolorOptions,
} from "interfaces/cookies/cc-pref-center.interface";
import { IColorOptions } from "interfaces/color-picker.interface";

// services
import { CookieConsentService } from "cookiesService/cookie-consent.service";

// pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

// constants
import { PrefCenterColorType } from "constants/cookies/cookie-preference-center.constant";
import { Color } from "constants/color.constant";
import { ColorPickerConstants } from "constants/cookies/cookie.constant";

@Component({
    selector: "preference-center-styling",
    templateUrl: "preference-center-styling.component.html",
})
export class PreferenceCenterStyling implements OnChanges {

    minicolorsSettings: IPreferenceCenterMinicolorOptions = this.CookieConsentService.getMinicolorSettings();
    colorOptions: IColorOptions[] = this.CookieConsentService.getColorCode();
    prefCenterColorType = PrefCenterColorType;
    defaultColorPickerValue = ColorPickerConstants.defaultColorPickerValue;
    logoFile: string;

    @Input() preferenceCenterStyling: IPrefCenterStyling;
    @Input() isCenterTileSkin: boolean;

    @Output() handleStyleChange: EventEmitter<void> = new EventEmitter<void>();

    @ViewChild("preferenceCenterLogoFile") preferenceCenterLogoFile: ElementRef;

    constructor(
        private CookieConsentService: CookieConsentService,
        private renderer2: Renderer2,
        private translatePipe: TranslatePipe,
    ) { }

    ngOnChanges(): void {
        this.logoFile = this.preferenceCenterStyling.logoFileName;
    }

    onPreferenceStyleChange(styleType: string): void {
        this.preferenceCenterStyling.style = styleType;
        this.handleStyleChange.emit();
    }

    setColor(color: string, type: string): void {
        if (color === this.defaultColorPickerValue) color = "";
        switch (type) {
            case PrefCenterColorType.PrimaryColor:
                this.preferenceCenterStyling.primaryColor = color;
                break;
            case PrefCenterColorType.ButtonColor:
                this.preferenceCenterStyling.buttonColor = color;
                break;
            case PrefCenterColorType.MenuColor:
                this.preferenceCenterStyling.menu = color;
                break;
            case PrefCenterColorType.MenuHighlightColor:
                this.preferenceCenterStyling.menuHighlightColor = color;
                break;
        }
    }

    updateLogo(event): void {
        if (this.logoFile) {
            const valid_extensions: RegExp = /(\jpg|\jpeg|\gif|\png)$/i;
            const splittedStrings: string[] = this.logoFile.split(".");
            if (valid_extensions.test(splittedStrings[splittedStrings.length - 1])) {
                const reader: FileReader = new FileReader();
                const inputFile: File = event.target.files[0];
                reader.onloadend = (e: any): void => {
                    this.preferenceCenterStyling.logoFileData = e.target.result;
                    this.preferenceCenterStyling.logoFileName = inputFile.name;
                    if (!this.preferenceCenterStyling.primaryColor || this.preferenceCenterStyling.primaryColor === this.defaultColorPickerValue) {
                        this.preferenceCenterStyling.primaryColor = Color.Grey11;
                    }
                    this.handleStyleChange.emit();
                };
                reader.readAsDataURL(inputFile);
            } else {
               OneConfirm(this.translatePipe.transform("ValidFileFormat"), this.translatePipe.transform("UploadValidImageFileFormat"), "", "", true, this.translatePipe.transform("Ok"), this.translatePipe.transform("Cancel")).then(
                    (revert: boolean): void => {
                        this.renderer2.setProperty(this.preferenceCenterLogoFile.nativeElement, "value", null);
                    });
            }
        }
    }

    resetToDefaultLogo(): void {
        OneConfirm(this.translatePipe.transform("RevertToDefault"), this.translatePipe.transform("AreYouSureYouWantToRevertToDefaultLogo"), "Confirm", "Ok", true, this.translatePipe.transform("Ok"), this.translatePipe.transform("Cancel")).then(
            (revert: boolean): void => {
                if (revert) {
                    if (this.logoFile) {
                        this.renderer2.setProperty(this.preferenceCenterLogoFile.nativeElement, "value", null);
                    }
                    this.preferenceCenterStyling.logoFileData = null;
                    this.preferenceCenterStyling.logoFileName = null;
                    if (!this.preferenceCenterStyling.primaryColor || this.preferenceCenterStyling.primaryColor === this.defaultColorPickerValue) {
                        this.preferenceCenterStyling.primaryColor = this.defaultColorPickerValue;
                    }
                    this.preferenceCenterStyling.customCSS = "";
                    this.handleStyleChange.emit();
                }
            });
    }
}
