import {
    Component,
    OnInit,
    Inject,
} from "@angular/core";
import { StateService } from "@uirouter/core";

@Component({
    selector: "cookie-empty-state",
    templateUrl: "./cookie-empty-state.component.html",
})
export class CookieEmptyState implements OnInit {
    pageName: string;
    isDomainCreated: string;
    isTemplateAssigned: string;
    displayMessageText = "DomainScanIsInProgress";

    constructor(
        private stateService: StateService,
    ) {}

    ngOnInit() {
        this.pageName = this.stateService.params.pageName;
        this.isDomainCreated = this.stateService.params.isDomainCreated;
        this.isTemplateAssigned = this.stateService.params.isTemplateAssigned;
        if (this.isDomainCreated === "true") {
            this.displayMessageText = this.isTemplateAssigned === "true" ? "DomainIsUsingTemplate" : "BannerIsYetToBeCreatedForThisDomain";
        }
    }
}
