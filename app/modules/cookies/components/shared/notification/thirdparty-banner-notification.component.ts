// Core
import { Component, Input, Output, EventEmitter } from "@angular/core";
import { WebAuditService } from "cookiesService/web-audit.service";

@Component({
    selector: "third-party-banner-notification",
    templateUrl: "./thirdparty-banner-notification.component.html",
})
export class ThirdPartyBannerNotification {

    showNotification = true;
    @Input() message: string;
    @Input() domain: string;
    @Output() callback = new EventEmitter();

    constructor(
        private webAuditService: WebAuditService,
    ) {}

    onCloseNotificationClick(): void {
        this.webAuditService.updateThirdPrtyBannerNotificationStatus(this.domain, true, true);
        this.showNotification = !this.showNotification;
        this.callback.emit();
    }

}
