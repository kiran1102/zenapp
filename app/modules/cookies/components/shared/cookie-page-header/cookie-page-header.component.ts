// Angular
import {
    Component,
    Inject,
    Input,
    OnInit,
} from "@angular/core";
import { StateService } from "@uirouter/core";

// Interfaces
import {
    ICookiePageHeaderBreadcrumbItem,
    ICookiePageHeaderButtonConfiguration,
} from "interfaces/cookies/cookie-page-header.interface";

@Component({
    selector: "cookie-page-header",
    templateUrl: "./cookie-page-header.component.html",
})
export class CookiePageHeader implements OnInit {
    @Input() pageTitle: string;
    @Input() showContent = true;
    @Input() headerType = "clear";
    @Input() breadcrumbItems: ICookiePageHeaderBreadcrumbItem[];
    @Input() primaryButton: ICookiePageHeaderButtonConfiguration;
    @Input() secondaryButton: ICookiePageHeaderButtonConfiguration;
    @Input() contextMenuOptions; // TODO : typing

    constructor(
        private stateService: StateService,
    ) { }

    ngOnInit() {
        this.contextMenuOptions = [];
    }

    goToRoute(route: { path: string, params: any }) {
        this.stateService.go(route.path);
    }
}
