// Angular
import {
    Component,
    Inject,
} from "@angular/core";
import {
    FormGroup,
    FormBuilder,
    Validators,
} from "@angular/forms";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

// 3rd Party
import { isFunction, uniqBy, filter } from "lodash";

// Services
import { ModalService } from "sharedServices/modal.service";
import { OrgGroupApiService } from "oneServices/api/org-group-api.service";
import { Principal } from "modules/shared/services/helper/principal.service";

// Redux
import { getModalData } from "oneRedux/reducers/modal.reducer";
import { StoreToken } from "tokens/redux-store.token";

// Interfaces
import { IStore } from "interfaces/redux.interface";
import { CookieIABIntegrationService } from "cookiesService/cookie-iab-integration.service";
import { IOrganization } from "interfaces/org.interface";

@Component({
    selector: "cc-add-vendor-list-modal",
    templateUrl: "./cc-add-vendor-list-modal.component.html",
})

export class AddVendorListModal {
    vendorListForm: FormGroup;
    modalTitle: string;
    saveInProgress: boolean;
    versionOrgName: string;
    versionId: number;
    versionName: string;
    versionDescription: string;
    organizations: IOrganization[];
    organizationsFiltered: IOrganization[];
    selectedOrganization: IOrganization;
    isDuplicateVersion: boolean;
    btnSaveText: string;
    edit: boolean;
    validateOrg =  false;
    private promiseToResolve: () => void;

    constructor(
        @Inject(StoreToken) private store: IStore,
        private principal: Principal,
        private translatePipe: TranslatePipe,
        private orgGroups: OrgGroupApiService,
        private modalService: ModalService,
        private cookieIABIntegrationService: CookieIABIntegrationService,
        private formBuilder: FormBuilder,
    ) { }

    ngOnInit(): void {
        const state = this.store.getState();
        const modalData = getModalData(state);
        this.promiseToResolve = modalData.promiseToResolve;
        this.edit = modalData.data.edit;
        if (this.edit) {
            this.versionId = modalData.data.version.ListId;
            this.versionName = modalData.data.version.ListName;
            this.versionDescription = modalData.data.version.ListDesc;
            this.versionOrgName = modalData.data.version.OrgName;
        }
        this.setCommonLabelText();
        this.orgGroups.rootTree().then(
            (res: any) => {
                this.organizations = Array.isArray(res) ? res : [res];
                this.organizations = this.getOrgDropdownOpts(this.organizations);
                if (this.versionOrgName) {
                    this.selectedOrganization = this.organizations.filter((e) => e.Name === this.versionOrgName)[0];
                    this.validateOrg = true;
                }
            },
        );
        this.initForm();
    }

    setCommonLabelText(): void {
        this.modalTitle = this.edit ? this.translatePipe.transform("EditVendorsList") : this.translatePipe.transform("AddVendorsList");
        this.btnSaveText = this.edit ? this.translatePipe.transform("Save") : this.translatePipe.transform("Create");
    }

    getOrgDropdownOpts(input: IOrganization[], options: IOrganization[] = []) {
        if (!input.length) {
          return options;
        }
        input.forEach((e) => {
          options.push(e);
          this.getOrgDropdownOpts(e.Children, options);
        });
        return options;
    }

    onInputChange({ key, value }): void {
        if (key === "Enter") { return; }
        this.filterList(uniqBy(this.organizations, "Name"), value);
    }

    onModelChange({ currentValue, change, previousValue }): void {
        this.validateOrg = false;
        if (change && currentValue) {
            this.validateOrg = true;
        }
        this.selectedOrganization = currentValue;
    }

    filterList(orgs: IOrganization[], stringSearch: string): void {
        this.organizationsFiltered = filter(orgs, (org: IOrganization): boolean => (org.Name.search(stringSearch) !== -1));
    }

    addVendors(): void {
        this.saveInProgress = true;
        const newVendor = {
            ListName: this.vendorListForm.get("versionName").value,
            Description: this.vendorListForm.get("versionDescription").value,
            OrgId: this.selectedOrganization.Id,
            OrgName: this.selectedOrganization.Name,
        };
        const headers = {
            TenantId: this.principal.getTenant(),
        };
        if (this.edit) {
            this.cookieIABIntegrationService.updateVendorVersions(this.versionId, newVendor, headers)
                .then((response: boolean): void => {
                    this.saveInProgress = false;
                    if (response) {
                        if (isFunction(this.promiseToResolve)) {
                            this.promiseToResolve().then(() => {
                                this.saveInProgress = false;
                                this.closeModal();
                            });
                        } else {
                            this.saveInProgress = false;
                            this.closeModal();
                        }
                    } else {
                        this.isDuplicateVersion = true;
                    }
                });
        } else {
            this.cookieIABIntegrationService.addVendorVersions(newVendor, headers)
            .then((response: boolean): void => {
                this.saveInProgress = false;
                if (response) {
                    if (isFunction(this.promiseToResolve)) {
                        this.promiseToResolve().then((data: any) => {
                            this.saveInProgress = false;
                            this.closeModal();
                        });
                    } else {
                        this.saveInProgress = false;
                        this.closeModal();
                    }
                } else {
                    this.isDuplicateVersion = true;
                }
            });
        }
    }

    closeModal(): void {
        this.modalService.closeModal();
        this.modalService.handleModalCallback();
        this.modalService.clearModalData();
    }

    private initForm(): void {
        this.vendorListForm = this.formBuilder.group({
            versionName: [this.versionName || "", Validators.required],
            versionDescription: [this.versionDescription || ""],
            selectedOrganization: [this.selectedOrganization || ""],
        });
    }
}
