// Angular
import {
    Component,
    EventEmitter,
    Inject,
    Input,
    Output,
} from "@angular/core";
import { IPromise } from "angular";

// 3rd party
import { find } from "lodash";

// Services
import { Permissions } from "modules/shared/services/helper/permissions.service";
import { CookieScriptIntegrationService } from "cookiesService/cookie-script-integration.service";
import { CookieLanguageSwitcherService } from "cookiesService/cookie-language-switcher.service";

// Interfaces
import { IBannerLanguage } from "interfaces/cookies/cc-banner-languages.interface";
import { ILanguageSwitcher } from "interfaces/cookies/cc-language-switcher.interface";
import { IStringMap } from "interfaces/generic.interface";
import {
    IGeoLocationSwitcher,
    IGeoLocationSwitcherRequest,
} from "interfaces/cookies/geo-location-switcher.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";

@Component({
    selector: "script-integration-settings",
    templateUrl: "./script-integration-settings-component.html",
})

export class ScriptIntegrationSettings {
    @Input() domainId: number;
    @Input() geoLocationSwitcher: IGeoLocationSwitcher;
    @Input() languages: IBannerLanguage[];
    @Input() languageSwitcher: ILanguageSwitcher;
    @Input() useOneTrustJqueryScript: boolean;
    @Output() switcherCallback: EventEmitter<IGeoLocationSwitcher | ILanguageSwitcher | null> = new EventEmitter();
    @Output() updateJqueryToggleCallback: EventEmitter<boolean> = new EventEmitter();

    disableGeoLocationSwitcher = false;
    disableLanguageSwitcher = false;
    disableOneTrustScriptToggle = false;
    permissions: IStringMap<boolean>;
    selectedDefaultLanguage: IBannerLanguage;
    selectedEuLanguage: IBannerLanguage;
    selectedNonEuLanguage: IBannerLanguage;

    constructor(
        private cookiePermissions: Permissions,
        private cookieScriptIntegrationService: CookieScriptIntegrationService,
        private cookieLanguageSwitcherService: CookieLanguageSwitcherService,
    ) { }

    ngOnInit(): void {
        this.permissions = {
            canShowLanguageSwitcher: this.cookiePermissions.canShow("CookieLanguageSwitcher"),
            canShowGeoLocationSwitcher: this.cookiePermissions.canShow("CookieGeoLocationBannerSwitch"),
            nojQueryScriptEnabled: this.cookiePermissions.canShow("CookieNoJquery"),
        };
        this.selectedDefaultLanguage = this.getSelectedLanguage(this.languageSwitcher.defaultLanguage) || this.languages[0];
        if (this.permissions.canShowGeoLocationSwitcher) {
            this.selectedEuLanguage = this.getSelectedLanguage(this.geoLocationSwitcher.euLanguage) || this.languages[0];
            this.selectedNonEuLanguage = this.getSelectedLanguage(this.geoLocationSwitcher.nonEuLanguage) || this.languages[0];
        }
    }

    onUseOneTrustJqueryToggle(): void {
        this.disableOneTrustScriptToggle = true;
        this.cookieScriptIntegrationService.updateScriptIntegrationToggleAction(this.domainId, this.useOneTrustJqueryScript)
            .then((): void => {
                this.updateJqueryToggleCallback.emit(this.useOneTrustJqueryScript);
                this.useOneTrustJqueryScript = !this.useOneTrustJqueryScript;
                this.disableOneTrustScriptToggle = false;
            });
    }

    onEuLanguageSelection(selection: IBannerLanguage): void {
        this.selectedEuLanguage = selection;
        this.updateGeoLocationLanguages(true);
    }

    onNonEuLanguageSelection(selection: IBannerLanguage): void {
        this.selectedNonEuLanguage = selection;
        this.updateGeoLocationLanguages(true);
    }

    onDefaultLanguageSelection(selection: IBannerLanguage): void {
        this.selectedDefaultLanguage = selection;
        this.updateLanguageSwitcherSelection(true);
    }

    onLanguageSwitcherToggle(): void {
        this.updateLanguageSwitcherSelection(!this.languageSwitcher.isEnabled)
            .then((response: ILanguageSwitcher | null): void => {
                if (response) {
                    if (this.geoLocationSwitcher) {
                        this.geoLocationSwitcher.isEnabled = false;
                    }
                    this.languageSwitcher.isEnabled = response.isEnabled;
                }
                this.switcherCallback.emit(response);
            });
    }

    onGeoLocationSwitcherToggle(): void {
        this.updateGeoLocationLanguages(!this.geoLocationSwitcher.isEnabled)
            .then((response: IGeoLocationSwitcher | null): void => {
                if (response) {
                    this.languageSwitcher.isEnabled = false;
                    this.geoLocationSwitcher.isEnabled = response.isEnabled;
                }
                this.switcherCallback.emit(response);
                this.disableGeoLocationSwitcher = false;
            });
    }

    private updateGeoLocationLanguages(isEnabled: boolean): IPromise<IGeoLocationSwitcher | null> {
        this.disableGeoLocationSwitcher = true;
        const payload: IGeoLocationSwitcherRequest = {
            isEnabled,
            euLanguage: this.selectedEuLanguage.language.culture,
            nonEuLanguage: this.selectedNonEuLanguage.language.culture,
        };
        return this.cookieLanguageSwitcherService.updateGeoLocationSwitcher(this.domainId, payload)
            .then((response: IProtocolResponse<IGeoLocationSwitcher | null>): IGeoLocationSwitcher | null => {
                this.disableGeoLocationSwitcher = false;
                return response.data;
            });
    }

    private updateLanguageSwitcherSelection(isEnabled: boolean): IPromise<ILanguageSwitcher | null> {
        this.disableLanguageSwitcher = true;
        return this.cookieLanguageSwitcherService.updateLanguageDetection(this.domainId, isEnabled, this.selectedDefaultLanguage.language.culture)
            .then((response: ILanguageSwitcher | null): ILanguageSwitcher | null => {
                this.disableLanguageSwitcher = false;
                return response;
            });
    }

    private getSelectedLanguage(selectedLanguage: string): IBannerLanguage {
        return find(this.languages, (banner: IBannerLanguage): boolean => selectedLanguage === banner.language.culture);
    }
}
