// Angular
import {
    Component,
    Inject,
    Injectable,
} from "@angular/core";

// 3rd Party
import { remove } from "lodash";

// Interfaces
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import { ICookies } from "interfaces/cookies/cc-cookie-category.interface";
import { IStore } from "interfaces/redux.interface";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

// Redux
import { getModalData } from "oneRedux/reducers/modal.reducer";

// Constants
import { StoreToken } from "tokens/redux-store.token";

// Services
import { ModalService } from "sharedServices/modal.service";
import { WebAuditService } from "cookiesService/web-audit.service";

@Component({
    selector: "remove-cookie-modal",
    templateUrl: "./remove-cookie-modal.component.html",
})

@Injectable()
export class RemoveCookieModal {
    cookies: ICookies[];
    promiseToResolve: (cookies: ICookies[]) => Promise<boolean>;

    constructor(
        @Inject(StoreToken) private store: IStore,
        private translatePipe: TranslatePipe,
        private modalService: ModalService,
        private webAuditService: WebAuditService) { }

    ngOnInit(): void {
        const state = this.store.getState();
        const modalData = getModalData(state);
        this.cookies = modalData.cookies;
        this.promiseToResolve = modalData.promiseToResolve;
    }

    closeModal(): void {
        this.modalService.closeModal();
        this.modalService.handleModalCallback();
        this.modalService.clearModalData();
    }

    removeCookies(): void {
        this.promiseToResolve(this.cookies);
        this.closeModal();
    }

    columnTrackBy(column: ICookies): number {
        return column.id;
    }

    onCancelCookie(selection: ICookies): void {
        remove(this.cookies, (cookie: ICookies): boolean => {
            return cookie.id === selection.id;
        });
    }

    getClassName(purpose: string): string {
        return this.webAuditService.getCookieClass(purpose, "graph-legend__");
    }

    getLifespanText(isSession: boolean, lifeSpan: number): string {
        return (isSession ? this.translatePipe.transform("Session") : (lifeSpan < 1 ? `<1 ${this.translatePipe.transform("Day")}` : (lifeSpan > 1 ? `${lifeSpan} ${this.translatePipe.transform("Days")}` : `${lifeSpan} ${this.translatePipe.transform("Day")}`)));
    }
}
