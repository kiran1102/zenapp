// Angular
import {
    Component,
    OnInit,
    Inject,
} from "@angular/core";

// Services
import { ModalService } from "sharedServices/modal.service";
import { CookieApiService } from "modules/cookies/shared/services/cookie-api.service";
import { ToastService } from "@onetrust/vitreus";

// Redux
import { getModalData } from "oneRedux/reducers/modal.reducer";

// Constants
import { StoreToken } from "tokens/redux-store.token";

// Interfaces
import { IStore } from "interfaces/redux.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";
import { StatusCodes } from "enums/status-codes.enum";

@Component({
    selector: "cookie-limit-modal-component",
    templateUrl: "./cookie-limit-modal.component.html",
})

export class CookieLimitModal implements OnInit {
    isAdmin: boolean;
    upgradeLink: string;

    constructor(
        @Inject(StoreToken) private readonly store: IStore,
        private modalService: ModalService,
        private cookieApiService: CookieApiService,
        private toastService: ToastService,
        private readonly translatePipe: TranslatePipe,
    ) { }

    ngOnInit() {
        const state = this.store.getState();
        const modalData = getModalData(state);
        this.isAdmin = modalData.isAdmin;
        this.upgradeLink = modalData.upgradeLink;
    }

    closeModal(): void {
        this.modalService.closeModal();
    }

    showSuccessMessage(title: string, message: string): void {
        this.toastService.updateConfig({
            positionClass: "bottom-right",
            maxOpened: 4,
            closeOldestOnMax: true,
            closeOnNavigation: true,
            timeout: 2000,
            closeLabel: "close",
        });
        this.toastService.show(title, message, "success");
    }

    sendEmail() {
        this.cookieApiService.sendEmail().then((res: IProtocolResponse<boolean>) => {
            if (res.status === StatusCodes.Success) {
                this.showSuccessMessage(this.translatePipe.transform("Success"), this.translatePipe.transform("EmailSent"));
                this.closeModal();
            }
        });
    }
}
