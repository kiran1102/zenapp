// Angular
import {
    Component,
    EventEmitter,
    Inject,
    Input,
    Output,
    Injectable,
    OnInit,
} from "@angular/core";
import {
    FormGroup,
    FormBuilder,
    FormControl,
    Validators,
    ValidationErrors,
} from "@angular/forms";

// 3rd party
import moment from "moment";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

// Services
import { ModalService } from "sharedServices/modal.service";
import { CookiePolicyService } from "cookiesService/cookie-policy.service";
import { NotificationService } from "modules/shared/services/provider/notification.service";

// Redux
import { getModalData } from "oneRedux/reducers/modal.reducer";

// Constants
import { StoreToken } from "tokens/redux-store.token";

// Interfaces
import { IStore } from "interfaces/redux.interface";
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import { ICookieCategoryOptions } from "interfaces/cookies/cookie-group-detail.interface";
import { ILabelValue } from "interfaces/generic.interface";
import { ICustomCookieModal } from "interfaces/cookies/custom-cookie-modal.interface";
import { ICookies } from "interfaces/cookies/cc-cookie-category.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";

// Constants
import {
    CookiePartyTypes,
    CookieType,
} from "constants/cookie.constant";

// Enums
import { StatusCodes } from "enums/status-codes.enum";

@Component({
    selector: "add-custom-cookie-modal",
    templateUrl: "./add-custom-cookie-modal.component.html",
})

@Injectable()
export class AddCustomCookieModal {
    addCookieForm: FormGroup;
    canShowAdvancedOption = false;
    canshowDuration = false;
    categoryOptions: ICookieCategoryOptions[];
    cookieTypes: Array<ILabelValue<string>> = [{
        label: this.translatePipe.transform("Session"),
        value: CookieType.Session,
    }, {
        label: this.translatePipe.transform("Persistent"),
        value: CookieType.Persistent,
    }];
    durationTypes: Array<ILabelValue<string>> = [{
        label: this.translatePipe.transform("Days"),
        value: "days",
    }, {
        label: this.translatePipe.transform("Weeks"),
        value: "weeks",
    }, {
        label: this.translatePipe.transform("Months"),
        value: "months",
    }, {
        label: this.translatePipe.transform("Years"),
        value: "years",
    }];
    isSaveAndCreateInProgress = false;
    isSaveInProgress = false;
    partyTypes: Array<ILabelValue<string>> = [{
        label: this.translatePipe.transform("FirstParty"),
        value: CookiePartyTypes.FirstParty,
    }, {
        label: this.translatePipe.transform("ThirdParty"),
        value: CookiePartyTypes.ThirdParty,
    }];

    private domainId: number;
    private cookiesCreated = false;
    private promiseToResolve: () => void;

    constructor(
        @Inject(StoreToken) private readonly store: IStore,
        private modalService: ModalService,
        private cookiePolicyService: CookiePolicyService,
        private fb: FormBuilder,
        private notificationService: NotificationService,
        private translatePipe: TranslatePipe,
    ) { }

    ngOnInit(): void {
        const state = this.store.getState();
        const modalData = getModalData(state);
        this.categoryOptions = modalData.categoryOptions;
        this.promiseToResolve = modalData.promiseToResolve;
        this.domainId = modalData.domainId;
        this.initForm();
    }

    onCreateAndNew(): void {
        this.isSaveAndCreateInProgress = true;
        this.createCookie((): void => {
            this.initForm();
        });
    }

    onCreate(): void {
        this.isSaveInProgress = true;
        this.createCookie((): void => {
            this.closeModal();
        });
    }

    closeModal(): void {
        this.modalService.closeModal();
        this.modalService.handleModalCallback();
        this.modalService.clearModalData();
        if (this.cookiesCreated) {
            this.promiseToResolve();
        }
    }

    private initForm(): void {
        this.addCookieForm = this.fb.group({
            name: ["", Validators.required],
            host: ["", Validators.required],
            isThirdParty: [CookiePartyTypes.FirstParty, Validators.required],
            category: [this.categoryOptions[0], Validators.required],
            isSession: [CookieType.Session, Validators.required],
            description: [""],
            value: [""],
        });
        this.canshowDuration = false;
        this.addCookieForm.controls["isSession"].valueChanges.subscribe((value) => {
            if (value === CookieType.Persistent) {
                this.canshowDuration = true;
                this.addCookieForm.addControl("duration", new FormControl("", [Validators.required, Validators.min(1)]));
                this.addCookieForm.addControl("durationType", new FormControl(this.durationTypes[0], Validators.required));
            } else {
                this.canshowDuration = false;
                this.addCookieForm.removeControl("duration");
                this.addCookieForm.removeControl("durationType");
            }
        });
    }

    private createCookie(callback: () => void): void {
        this.cookiesCreated = true;
        const payload = this.constructAddCookiePayload();
        const groupId = this.addCookieForm.get("category").value.id;
        this.cookiePolicyService.createCustomCookie(payload, this.domainId, groupId)
            .then((response: IProtocolResponse<string | ICookies>): void => {
                this.isSaveAndCreateInProgress = false;
                this.isSaveInProgress = false;
                if (response.result) {
                    callback();
                    return;
                } else if (response.status === StatusCodes.Conflict) {
                    this.notificationService.alertError(this.translatePipe.transform("Error"), this.translatePipe.transform("DuplicateCookie"));
                } else {
                    this.notificationService.alertError(this.translatePipe.transform("Error"), this.translatePipe.transform("CouldNotCreateCookie"));

                }
            });
    }

    private constructAddCookiePayload(): ICustomCookieModal {
        const paylod: ICustomCookieModal = {
            description: this.addCookieForm.get("description").value,
            host: this.addCookieForm.get("host").value.trim(),
            isSession: this.addCookieForm.get("isSession").value === CookieType.Session,
            isThirdParty: this.addCookieForm.get("isThirdParty").value === CookiePartyTypes.ThirdParty,
            name: this.addCookieForm.get("name").value,
            purpose: this.addCookieForm.get("category").value.name,
            value: this.addCookieForm.get("value").value,
            lifeSpan: null,
        };
        if (!paylod.isSession) {
            paylod.lifeSpan = moment.duration({ [this.addCookieForm.get("durationType").value.value]: this.addCookieForm.get("duration").value }).as("days");
        }
        return paylod;
    }
}
