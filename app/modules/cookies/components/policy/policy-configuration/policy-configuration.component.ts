// Angular
import {
    Component,
    EventEmitter,
    Inject,
    Injectable,
    Input,
    Output,
    SimpleChanges,
} from "@angular/core";
import {
    FormGroup,
    FormBuilder,
} from "@angular/forms";

// Interfaces
import { ICookieListConfigurationDetail } from "interfaces/cookies/cookie-list-configuration-detail.interface";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

export interface IConfigurationFormValues {
    cookiesLabel: string;
    categoriesLabel: string;
    showLifespanToggle: boolean;
    lifeSpanLabel: string;
    sessionLabel: string;
    daysLabel: string;
}

@Component({
    selector: "policy-configuration",
    templateUrl: "./policy-configuration.component.html",
})

@Injectable()
export class PolicyConfiguration {
    @Input() configurationDetail: ICookieListConfigurationDetail;
    @Output() configurationContentChangeCallback: EventEmitter<ICookieListConfigurationDetail> = new EventEmitter();

    configurationForm: FormGroup;

    constructor(
        private translatePipe: TranslatePipe,
        private fb: FormBuilder,
    ) { }

    ngOnInit(): void {
        this.initForm();
    }

    ngOnChanges(changes: SimpleChanges): void {
        if (!changes.configurationDetail.isFirstChange()) {
            this.configurationDetail = changes.configurationDetail.currentValue;
            this.initForm();
        }
    }

    private initForm(): void {
        this.configurationForm = this.fb.group({
            cookiesLabel: [this.configurationDetail.CookiesText || "", { updateOn: "blur" }],
            categoriesLabel: [this.configurationDetail.CategoriesText || "", { updateOn: "blur" }],
            showLifespanToggle: [this.configurationDetail.IsLifespanEnabled || false],
            lifeSpanLabel: [this.configurationDetail.LifespanText || "", { updateOn: "blur" }],
            sessionLabel: [this.configurationDetail.LifespanTypeText || "", { updateOn: "blur" }],
            daysLabel: [this.configurationDetail.LifespanDurationText || "", { updateOn: "blur" }],
        });
        this.configurationForm.valueChanges.subscribe((values: IConfigurationFormValues) => {
            const configurationDetail: ICookieListConfigurationDetail = {
                CookiesText: values.cookiesLabel,
                CategoriesText: values.categoriesLabel,
                IsLifespanEnabled: values.showLifespanToggle,
                LifespanText: values.lifeSpanLabel,
                LifespanTypeText: values.sessionLabel,
                LifespanDurationText: values.daysLabel,
            };
            this.configurationContentChangeCallback.emit(configurationDetail);
        });
    }
}
