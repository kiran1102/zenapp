// Angular
import {
    Component,
    Inject,
    Injectable,
} from "@angular/core";
import {
    FormGroup,
    FormControl,
    FormBuilder,
    Validators,
} from "@angular/forms";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

// Services
import { ModalService } from "sharedServices/modal.service";
import { CookiePolicyService } from "cookiesService/cookie-policy.service";
import { Permissions } from "modules/shared/services/helper/permissions.service";
import { NotificationService } from "modules/shared/services/provider/notification.service";

// Redux
import { getModalData } from "oneRedux/reducers/modal.reducer";
import { StoreToken } from "tokens/redux-store.token";

// Interfaces
import { IStore } from "interfaces/redux.interface";
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import {
    ICookieGroupDetail,
    ICookieGroupRequest,
    ICookieGroupMetaDetail,
} from "interfaces/cookies/cookie-group-detail.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";

// Enums
import { PurposeGroupId } from "enums/cookies/purpose-group-id.enum";
import { StatusCodes } from "enums/status-codes.enum";

export interface IGroupModalData {
    canShowDntOption: boolean;
    customGroupId: string;
    description?: string;
    domainId: number;
    groupId: number;
    isDntEnabled?: boolean;
    isNewGroup: boolean;
    isSubgroup: boolean;
    name?: string;
    purposeGroupId?: number;
    promiseToResolve: (value: ICookieGroupDetail | ICookieGroupMetaDetail) => Promise<boolean>;
}

@Component({
    selector: "cc-group-modal",
    templateUrl: "./cc-group-modal.component.html",
})

@Injectable()
export class CCGroupModal {
    canShowDNTOption = false;
    customGroupIdEnabled = this.permissions.canShow("CookieCustomGroupId");
    groupForm: FormGroup;
    modalData: IGroupModalData;
    modalTitle: string;
    purposeGroupId: typeof PurposeGroupId;
    saveInProgress = false;

    constructor(
        @Inject(StoreToken) private readonly store: IStore,
        private translatePipe: TranslatePipe,
        private permissions: Permissions,
        private modalService: ModalService,
        private cookiePolicyService: CookiePolicyService,
        private fb: FormBuilder,
        private notificationService: NotificationService,
    ) { }

    ngOnInit(): void {
        const state = this.store.getState();
        this.modalData = getModalData(state);
        this.purposeGroupId = PurposeGroupId;
        this.canShowDNTOption = this.permissions.canShow("CookieAllowGroupDNT")
            && this.modalData.canShowDntOption
            && this.modalData.purposeGroupId !== PurposeGroupId.StrictlyNecessaryCookies;
        this.setModalTitle();
        this.initForm();
    }

    submitModal(): void {
        if (this.modalData.isNewGroup) {
            this.createGroup();
            return;
        }
        this.updateGroup();
    }

    closeModal(): void {
        this.modalService.closeModal();
        this.modalService.handleModalCallback();
        this.modalService.clearModalData();
    }

    private initForm(): void {
        this.groupForm = this.fb.group({
            name: [this.modalData.name || "", Validators.required],
            description: [this.modalData.description || ""],
        });

        if (this.canShowDNTOption) {
            this.groupForm.addControl("isDntEnabled",
                new FormControl(this.modalData.purposeGroupId === PurposeGroupId.TargettingCookies ? this.modalData.canShowDntOption : this.modalData.isDntEnabled));
        }

        if (this.customGroupIdEnabled) {
            this.groupForm.addControl("customGroupId",
                new FormControl(this.modalData.customGroupId || null, [Validators.pattern("[a-zA-Z0-9]{0,3}")]),
            );
        }
        if (!this.modalData.isSubgroup) {
            this.groupForm.controls["description"].setValidators([Validators.required]);
        }
    }

    private setModalTitle = (): void => {
        if (this.modalData.isNewGroup) {
            this.modalTitle = this.translatePipe.transform(this.modalData.isSubgroup ? "CreateSubgroup" : "CreateGroup");
            return;
        }
        this.modalTitle = this.translatePipe.transform(this.modalData.isSubgroup ? "EditSubgroup" : "EditGroup");

    }

    private createGroup(): void {
        this.saveInProgress = true;
        this.cookiePolicyService.createGroup(this.getPayload(), this.modalData.groupId, this.modalData.domainId)
            .then((response: IProtocolResponse<ICookieGroupDetail>): void => {
                this.saveInProgress = false;
                if (!response.result) {
                    if (response.status === StatusCodes.Conflict) {
                        this.notificationService.alertError(this.translatePipe.transform("Error"), this.translatePipe.transform("CustomGroupIdExists"));
                    } else {
                        this.notificationService.alertError(this.translatePipe.transform("Error"), this.translatePipe.transform("CouldNotCreateGroup"));
                    }
                    return;
                }
                this.modalData.promiseToResolve(response.data)
                    .then((result: boolean) => {
                        if (result) this.closeModal();
                    });
            });
    }

    private updateGroup(): void {
        this.saveInProgress = true;
        const payload = this.getPayload();
        this.cookiePolicyService.updateGroup(payload, this.modalData.groupId, this.modalData.domainId)
            .then((response: IProtocolResponse<void>): void => {
                this.saveInProgress = false;
                if (!response.result) {
                    if (response.status === StatusCodes.Conflict) {
                        this.notificationService.alertError(this.translatePipe.transform("Error"), this.translatePipe.transform("CustomGroupIdExists"));
                    } else {
                        this.notificationService.alertError(this.translatePipe.transform("Error"), this.translatePipe.transform("CouldNotUpdateGroup"));
                    }
                    return;
                }
                this.modalData.promiseToResolve({
                    name: payload.groupName,
                    customGroupId: payload.customGroupId,
                    description: payload.groupDescription,
                    isDntEnabled: payload.isDntEnabled,
                }).then((result: boolean) => {
                    if (result) this.closeModal();
                });
            });
    }

    private getPayload(): ICookieGroupRequest {
        return {
            groupName: this.groupForm.get("name").value,
            groupDescription: this.groupForm.get("description").value,
            isDntEnabled: this.groupForm.get("isDntEnabled") ? this.groupForm.get("isDntEnabled").value : false,
            customGroupId: this.customGroupIdEnabled ? this.groupForm.get("customGroupId").value : null,
        };
    }
}
