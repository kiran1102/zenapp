// Angular
import {
    Component,
    OnInit,
    Inject,
} from "@angular/core";
import {
    FormGroup,
    FormBuilder,
    FormArray,
    Validators,
} from "@angular/forms";
import { StateService } from "@uirouter/core";

// 3rd party
import {
    assign,
    remove,
} from "lodash";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

// Constants
import { ScanBehindLoginError } from "constants/cookie.constant";
import { ScanLoginTabs } from "constants/cookies/scan-login.constant";

// Sevices
import { ModalService } from "sharedServices/modal.service";
import { ScanLoginApiService } from "modules/cookies/services/api/scan-login-api.service";

// Interfaces
import {
    ICookiePageHeaderBreadcrumbItem,
    ICookiePageHeaderButtonConfiguration,
} from "interfaces/cookies/cookie-page-header.interface";
import { IRestoreDefaultModalResolve } from "interfaces/cookies/restore-default-modal-resolver.interface.ts";
import {
    IScanLoginDetails,
    IAuthenticationCookiesList,
} from "interfaces/cookies/cc-scan-login-details.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";
import { IScanBehindTutorialModalResolver } from "interfaces/cookies/scan-behind-tutorial-modal-resolver.interface";
import { IScanLoginFormConfig } from "interfaces/cookies/scan-login.interface";

@Component({
    selector: "scan-login-detail",
    templateUrl: "./scan-login-detail.component.html",
})
export class ScanLoginDetail implements OnInit {
    breadcrumbItems: ICookiePageHeaderBreadcrumbItem[] = [{
        labelKey: "Websites",
        otAutoId: "CookieWebsiteListNavigation",
        path: "zen.app.pia.module.cookiecompliance.views.websites",
    }];
    primaryButton: ICookiePageHeaderButtonConfiguration = {
        labelKey: "Save",
        otAutoId: "CookieScanLoginDetailSaveButton",
        isLoading: false,
        isDisabled: false,
        action: () => {
            this.updateScanLoginDetail();
        },
    };
    secondaryButton: ICookiePageHeaderButtonConfiguration = {
        labelKey: "RestoreDefault",
        otAutoId: "CookieScanLoginDetailRestoreDefaultButton",
        isDisabled: false,
        action: () => {
            this.revertToDefaultScanLoginDetail();
        },
    };
    accordionConfig = [{
        labelKey: "AuthenticationCookies",
        id: ScanLoginTabs.AuthenticationCookies,
        descriptionKey: "AuthenticationCookiesInfo",
        isExpanded: false,
    }, {
        labelKey: "LoginFormSettings",
        id: ScanLoginTabs.LoginFormSettings,
        descriptionKey: "LoginFormInfo",
        isExpanded: false,
    }, {
        labelKey: "WebFormHTMLAttributes",
        id: ScanLoginTabs.WebFormHTMLAttributes,
        descriptionKey: "AdvancedScanLoginFormInfo",
        isExpanded: false,
    }];

    authCookiesForm: FormGroup;
    htmlAttributeConfig: IScanLoginFormConfig[] = [];
    htmlAttributeForm: FormGroup;
    isLoaded = false;
    loginDetails: IScanLoginDetails;
    loginInfoForm: FormGroup;
    loginInfoFormConfig: IScanLoginFormConfig[] = [];
    noOfAuthcookiesAllowed = 5;
    scanLoginTabs: typeof ScanLoginTabs;
    authCookies: FormArray;

    private domainName: string;

    constructor(
        private stateService: StateService,
        private formBuilder: FormBuilder,
        private modalService: ModalService,
        private translatePipe: TranslatePipe,
        private scanLoginApiService: ScanLoginApiService,
    ) { }

    ngOnInit() {
        this.scanLoginTabs = ScanLoginTabs;
        this.domainName = this.stateService.params.domain;
        this.secondaryButton.isDisabled = this.stateService.params.isNew === "true";
        this.breadcrumbItems.push({
            label: this.domainName,
            otAutoId: "CookieWebsiteName",
        });
        this.getScanLoginDetail();
    }

    onConfigurationAccordionToggle(index: number): void {
        this.accordionConfig[index].isExpanded = !this.accordionConfig[index].isExpanded;
    }

    scanBehindTutorial() {
        const modalData: IScanBehindTutorialModalResolver = {
            modalTitle: this.translatePipe.transform("HtmlInspectTutorialText"),
            closeButtonText: this.translatePipe.transform("Close"),
            altText: this.translatePipe.transform("altText"),
            note: this.translatePipe.transform("HowTo"),
        };
        this.modalService.setModalData(modalData);
        this.modalService.openModal("scanBehindTutorial");
    }

    removeCookie(index: number) {
        this.authCookies.removeAt(index);
    }

    addCookie() {
        this.addAuthCookieControl();
    }

    private getScanLoginDetail() {
        this.scanLoginApiService.getScanLoginDetail(this.domainName)
            .then((response: IProtocolResponse<IScanLoginDetails | null>) => {
                this.loginDetails = response.result && response.data ? response.data : this.getDefaultValues();
                this.setFormConfigurations();
                this.initForm();
                this.isLoaded = true;
            });
    }

    private getDefaultValues(): IScanLoginDetails {
        return {
            domainName: null,
            loginPageUrl: null,
            loginFormId: null,
            submitButtonId: this.translatePipe.transform("Submit").toLowerCase(),
            usernameFieldId: this.translatePipe.transform("Username").toLowerCase(),
            usernameValue: null,
            passwordFieldId: this.translatePipe.transform("Password").toLowerCase(),
            passwordValue: null,
            otherLoginInfo: null,
            isEnabled: false,
            cookiesJsonData: null,
            loginError: null,
        };
    }

    private addAuthCookieControl() {
        if (this.authCookies.length >= this.noOfAuthcookiesAllowed) {
            return;
        }
        this.authCookies.push(this.formBuilder.group({
            cookiesName: ["", [Validators.maxLength(100)]],
            cookiesValue: ["", [Validators.maxLength(1000)]],
        }, { updateOn: "blur" }));
    }

    private setFormConfigurations() {
        const loginError = this.loginDetails.loginError;
        this.loginInfoFormConfig = [
            {
                labelKey: "LoginPageUrl",
                name: "loginPageUrl",
                placeholderKey: "ScanBehinLoginPageUrl",
                type: "input",
                valueKey: "loginPageUrl",
                otAutoId: "CookieScanLoginPageUrlInput",
            }, {
                labelKey: "Username",
                name: "usernameValue",
                placeholderKey: "UserNameId",
                type: "input",
                valueKey: "usernameValue",
                otAutoId: "CookieScanLoginUserNameInput",
                hasError: loginError === ScanBehindLoginError.INVALID_LOGININFO,
                errorMessage: this.translatePipe.transform("InvalidLoginInfoError"),
            }, {
                labelKey: "Password",
                name: "passwordValue",
                placeholder: "******",
                type: "password",
                valueKey: "passwordValue",
                otAutoId: "CookieScanLoginPasswordInput",
            },
        ];
        this.htmlAttributeConfig = [
            {
                labelKey: "LoginFormId",
                name: "loginFormId",
                placeholderKey: "FormName",
                type: "input",
                valueKey: "loginFormId",
                otAutoId: "CookieScanLoginFormNameAttributeInput",
            }, {
                labelKey: "SubmitButtonId",
                name: "submitButtonId",
                placeholderKey: "Submit",
                type: "input",
                valueKey: "submitButtonId",
                otAutoId: "CookieScanLoginButtonNameInput",
                hasError: loginError === ScanBehindLoginError.INVALID_SUBMITBUTTON,
                errorMessage: this.translatePipe.transform("InvalidSubmitButtonError"),
            }, {
                labelKey: "UsernameFieldId",
                name: "usernameFieldId",
                placeholderKey: "Username",
                type: "input",
                valueKey: "usernameFieldId",
                otAutoId: "CookieScanLoginUserNameAttributeNameInput",
                hasError: loginError === ScanBehindLoginError.INVALID_USERNAMEFEILD,
                errorMessage: this.translatePipe.transform("InvalidUserNameFieldError"),
            }, {
                labelKey: "PasswordFieldId",
                name: "passwordFieldId",
                placeholderKey: "Password",
                type: "input",
                valueKey: "passwordFieldId",
                otAutoId: "CookieScanLoginPasswordAttributeNameInput",
                hasError: loginError === ScanBehindLoginError.INVALID_PASSWORDFIELD,
                errorMessage: this.translatePipe.transform("InvalidPasswordFieldError"),
            },
        ];
    }

    private initForm(): void {
        this.loginInfoForm = this.formBuilder.group({});
        this.htmlAttributeForm = this.formBuilder.group({});
        this.loginInfoFormConfig.forEach((control) => this.loginInfoForm.addControl(control.name,
            this.formBuilder.control(this.loginDetails[control.valueKey], [Validators.maxLength(700)])));

        this.htmlAttributeConfig.forEach((control) => this.htmlAttributeForm.addControl(control.name,
            this.formBuilder.control(this.loginDetails[control.valueKey], [Validators.maxLength(700)])));

        this.authCookiesForm = new FormGroup({
            cookies: new FormArray([]),
        });
        this.authCookies = this.authCookiesForm.get("cookies") as FormArray;
        this.createAuthCookieForm();
    }

    private createAuthCookieForm() {
        const cookieData = this.loginDetails.cookiesJsonData ? JSON.parse(this.loginDetails.cookiesJsonData) : null;
        if (!cookieData || !cookieData.length) {
            this.addAuthCookieControl();
            return;
        }
        cookieData.forEach((cookie: IAuthenticationCookiesList) =>
            this.authCookies.push(this.formBuilder.group({
                cookiesName: [cookie.cookiesName, [Validators.maxLength(100)]],
                cookiesValue: [cookie.cookiesValue, [Validators.maxLength(1000)]],
            }, { updateOn: "blur" })));
    }

    private updateScanLoginDetail() {
        this.primaryButton.isLoading = true;
        this.primaryButton.isDisabled = true;
        this.secondaryButton.isDisabled = true;
        this.loginDetails.domainName = this.domainName;
        this.loginDetails = assign({}, this.loginDetails, this.loginInfoForm.getRawValue());
        this.loginDetails = assign({}, this.loginDetails, this.htmlAttributeForm.getRawValue());
        const cookies: IAuthenticationCookiesList[] = this.authCookies.getRawValue();
        remove(cookies, (cookie: IAuthenticationCookiesList): boolean => {
            for (const propName in cookie) {
                if (!cookie[propName] || cookie[propName].length === 0) {
                    return (!cookie[propName] || cookie[propName].length === 0);
                }
            }
        });
        this.loginDetails.cookiesJsonData = JSON.stringify(cookies);
        this.scanLoginApiService.updateScanLoginDetail(this.loginDetails)
            .then((): void => {
                this.primaryButton.isLoading = false;
                this.primaryButton.isDisabled = false;
                this.secondaryButton.isDisabled = false;
            });
    }

    private revertToDefaultScanLoginDetail() {
        const modalData: IRestoreDefaultModalResolve = {
            modalTitle: this.translatePipe.transform("ReDefaultSettings"),
            confirmationText: this.translatePipe.transform("RestoreToDefaultSettings"),
            warningText: this.translatePipe.transform("RestoreWarningText"),
            cancelButtonText: this.translatePipe.transform("Cancel"),
            submitButtonText: this.translatePipe.transform("RestoreBtn"),
            promiseToResolve: (): ng.IPromise<void> =>
                this.scanLoginApiService.deleteScanLoginInfo(this.domainName)
                    .then((response: IProtocolResponse<IScanLoginDetails>): void => {
                        if (response.data) {
                            this.loginDetails = this.getDefaultValues();
                            this.initForm();
                            this.secondaryButton.isDisabled = true;
                            this.modalService.closeModal();
                        }
                    }),
        };
        this.modalService.setModalData(modalData);
        this.modalService.openModal("restoreDefaultModal");
    }
}
