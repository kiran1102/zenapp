// Angular
import {
    Component,
    Inject,
} from "@angular/core";
import {
    FormGroup,
    FormBuilder,
    Validators,
} from "@angular/forms";

import {
    filter,
    find,
    isFunction,
    uniqBy,
} from "lodash";

import { getCurrentUser } from "oneRedux/reducers/user.reducer";

import { IMyDate } from "mydatepicker";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

// Services
import { ModalService } from "sharedServices/modal.service";
import { CookieAuditScheduleService } from "cookiesService/cookie-audit-schedule.service";
import { TimeStamp } from "oneServices/time-stamp.service";
import { Permissions } from "modules/shared/services/helper/permissions.service";

// Redux
import { getModalData } from "oneRedux/reducers/modal.reducer";
import { StoreToken } from "tokens/redux-store.token";

// Interfaces
import { IStore } from "interfaces/redux.interface";
import { IUser } from "interfaces/user.interface";

@Component({
    selector: "cc-schedule-audit-modal",
    templateUrl: "./cc-schedule-audit-modal.component.html",
})

export class ScheduleAuditModal {
    ScheduleAuditForm: FormGroup;
    saveInProgress: boolean;
    isAssignedToMe = true;
    userSelectionList: IUser[];
    userSelectionListFiltered: IUser[];
    canSubmit = false;
    minDate: Date = new Date();
    isNewSchedule = true;
    typeAheadName: string;
    cancelInProgress = false;
    scanScheduleIntervalCaption = "";
    scheduleData: any;
    userList: IUser[];
    validateUser =  true;
    nextAuditString = "";
    disabledUntil = this.getDisableUntilDate();
    format = this.timeStamp.getDatePickerDateFormat();
    private promiseToResolve: () => void;

    constructor(
        @Inject(StoreToken) private store: IStore,
        private translatePipe: TranslatePipe,
        private cookieAuditScheduleService: CookieAuditScheduleService,
        private modalService: ModalService,
        private permissions: Permissions,
        private formBuilder: FormBuilder,
        private timeStamp: TimeStamp,
    ) { }

    ngOnInit(): void {
        const state = this.store.getState();
        const currentUser: IUser = getCurrentUser(this.store.getState());
        const modalData = getModalData(state);
        this.promiseToResolve = modalData.promiseToResolve;
        this.scheduleData = modalData.scheduleData;
        this.userList = modalData.userList;
        this.minDate.setDate(this.minDate.getDate() + 1);
        this.scanScheduleIntervalCaption = this.permissions.canShow("CookieTenantLevelScheduledScan") ? this.translatePipe.transform("ScanAllSite") : this.translatePipe.transform("ScanEvery");
        this.userSelectionList = filter(this.userList, (user: IUser): boolean => !user.DisabledDT);
        if (this.scheduleData) {
            if (this.scheduleData.emailRecipients) {
                const userSelected = find(this.userSelectionList, (UserData: any): any => UserData.Email === this.scheduleData.emailRecipients);
                this.scheduleData.emailRecipients = userSelected ? userSelected : this.scheduleData.emailRecipients;
                if (currentUser.Email !== this.scheduleData.emailRecipients.Email) {
                    this.isAssignedToMe = false;
                }
            } else {
                this.scheduleData.emailRecipients = currentUser;
            }
            if (this.scheduleData.nextAudit) {
                this.scheduleData.nextAudit = new Date(this.scheduleData.nextAudit);
                this.isNewSchedule = false;
            } else {
                this.scheduleData.nextAudit = this.minDate;
            }
        } else {
            this.scheduleData.emailRecipients = currentUser;
            this.scheduleData.nextAudit = this.minDate;
        }

        this.nextAuditString = this.timeStamp.formatDateWithoutTime(this.scheduleData.nextAudit).toString();
        this.initForm();
        this.validateForSubmit();
    }

    assignToMe(): void {
        this.isAssignedToMe = true;
        this.validateUser = true;
        this.scheduleData.emailRecipients = getCurrentUser(this.store.getState());
        this.validateForSubmit();
    }

    assignToSomeone(): void {
        this.isAssignedToMe = false;
        this.validateForSubmit();
    }

    closeModal(): void {
        this.modalService.closeModal();
        this.modalService.handleModalCallback();
        this.modalService.clearModalData();
    }

    changeAuditDate(value): void {
        this.nextAuditString = value.formatted;
        this.scheduleData.nextAudit = new Date(value.jsdate);
        this.validateForSubmit();
    }

    onInputChange({ key, value }): void {
        if (key === "Enter") { return; }
        this.filterList(uniqBy(this.userSelectionList, "FullName"), value);
    }

    onModelChange({ currentValue, change, previousValue }): void {
        this.validateUser = false;
        if (change && currentValue) {
            this.validateUser = true;
        }
        this.scheduleData.emailRecipients = currentValue;
        this.validateForSubmit();
    }

    public validateForSubmit(): void {
        if (this.ScheduleAuditForm.get("periodInMonths").value && this.nextAuditString && this.validateUser) {
            this.canSubmit = true;
        } else {
            this.canSubmit = false;
        }
    }

    public setSchedule(): void {
        this.saveInProgress = true;
        this.scheduleData.emailRecipients = this.scheduleData.emailRecipients.Email ? this.scheduleData.emailRecipients.Email : this.scheduleData.emailRecipients;
        this.scheduleData.periodInMonths = this.ScheduleAuditForm.get("periodInMonths").value;
        this.cookieAuditScheduleService.updateSchedule(this.scheduleData)
        .then((): void => {
            if (isFunction(this.promiseToResolve)) {
                this.promiseToResolve().then(() => {
                    this.saveInProgress = false;
                    this.closeModal();
                });
            } else {
                this.saveInProgress = false;
                this.closeModal();
            }
        });
    }

    public cancelSchedule(): void {
        this.cancelInProgress = true;
        this.cookieAuditScheduleService.cancelSchedule(this.scheduleData, this.permissions.canShow("CookieTenantLevelScheduledScan")).then((): void => {
            this.scheduleData.nextAudit = null;
            this.nextAuditString = "";
            this.validateForSubmit();
            if (isFunction(this.promiseToResolve)) {
                this.promiseToResolve().then(() => {
                    this.cancelInProgress = false;
                    this.isNewSchedule = true;
                });
            } else {
                this.cancelInProgress = false;
            }
        });
    }

    private getDisableUntilDate(): IMyDate {
        const currentDate = new Date();
        return {
            year: currentDate.getFullYear(),
            month: currentDate.getMonth() + 1,
            day: currentDate.getDate(),
        };
    }

    private filterList(users: IUser[], stringSearch: string): void {
        this.userSelectionListFiltered = filter(users, (user: IUser): boolean => (user.FullName.search(stringSearch) !== -1));
    }

    private initForm(): void {
        this.ScheduleAuditForm = this.formBuilder.group({
            periodInMonths: [this.scheduleData.periodInMonths || "", Validators.required],
        });
    }
}
