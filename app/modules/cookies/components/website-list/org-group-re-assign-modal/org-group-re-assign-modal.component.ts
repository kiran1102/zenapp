// Angular
import {
    Component,
    EventEmitter,
    Inject,
    Input,
    Output,
    Injectable,
    OnInit,
} from "@angular/core";
import {
    FormGroup,
    FormBuilder,
    FormControl,
    Validators,
    ValidationErrors,
} from "@angular/forms";

import { isFunction, isEmpty } from "lodash";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

// Services
import { ModalService } from "sharedServices/modal.service";
import { CookieOrgGroupService } from "cookiesService/cookie-org-group.service";

// Redux
import { getModalData } from "oneRedux/reducers/modal.reducer";

// Constants
import { StoreToken } from "tokens/redux-store.token";

// Interfaces
import { IStore } from "interfaces/redux.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";

@Component({
    selector: "org-group-re-assign-modal",
    templateUrl: "./org-group-re-assign-modal.component.html",
})

@Injectable()
export class OrgGroupReAssignModal {

    addOrgGroupReAssign: FormGroup;
    isSaveInProgress = false;
    isValidOrgSelection = false;
    organizationUUID: string;
    selectedDomain = "";

    private promiseToResolve: () => void;

    constructor(
        @Inject(StoreToken) private readonly store: IStore,
        public translatePipe: TranslatePipe,
        private ModalService: ModalService,
        private CookieOrgGroupService: CookieOrgGroupService,
        private fb: FormBuilder,
    ) { }

    ngOnInit(): void {
        const state = this.store.getState();
        const modalData = getModalData(state);
        this.promiseToResolve = modalData.promiseToResolve;
        this.selectedDomain = modalData.domainName;
        this.organizationUUID = modalData.organizationUUID;
        this.isValidOrgSelection = this.validateOrgSelection();
        this.initForm();
    }

    reAssignOrgGroup (): void {
        this.isSaveInProgress = true;
        this.CookieOrgGroupService.reassignOrgGroup(this.selectedDomain, this.organizationUUID).then((): void => {
            if (isFunction(this.promiseToResolve)) {
                this.promiseToResolve().then(() => {
                    this.isSaveInProgress = false;
                    this.closeModal();
                });
            } else {
                this.isSaveInProgress = false;
                this.closeModal();
            }
        });
    }

    closeModal(): void {
        this.ModalService.closeModal();
        this.ModalService.handleModalCallback();
        this.ModalService.clearModalData();
    }

    handleOrgSelection(model: string): void {
        this.organizationUUID = model;
        this.isValidOrgSelection = this.validateOrgSelection();
    }

    validateOrgSelection(): boolean {
        return this.organizationUUID && !isEmpty(this.organizationUUID);

    }

    private initForm(): void {
        this.addOrgGroupReAssign = this.fb.group({
            organization: [this.organizationUUID, Validators.required],
        });
    }

}
