// Angular
import {
    Component,
    OnInit,
    Input,
    Output,
    EventEmitter,
} from "@angular/core";

import {
    findIndex,
    some,
    cloneDeep,
} from "lodash";

// Constants
import {
    CoookieConsentPolicyRuleControls,
    CookieConsentPolicyDetailGroupColumns,
} from "constants/cookies/cookie-consent-policy.constant";

// Enums
import { SettingsType } from "sharedModules/enums/settings.enum";
import { CookieConsentPolicyConsentModel } from "enums/cookies/cookie-list.enum";

// Services
import { Permissions } from "modules/shared/services/helper/permissions.service";
import { ModalService } from "sharedServices/modal.service";
import { ConsentPolicyHelperService } from "modules/cookies/services/helper/consent-policy-helper.service";

// Interfaces
import { ISettingsMeta } from "sharedModules/interfaces/settings.interface";
import { IAction } from "interfaces/redux.interface";
import { ICookieNameId } from "interfaces/cookies/cookie-generic.interface";
import {
    IRegionRuleDetail,
    IConsentPolicyGroupDetail,
    IRegionRuleSetting,
    IImpliedOptionDetail,
    IdomainGroupTableColumns,
} from "interfaces/cookies/region-rule.interface";
import { IConsentPolicyVendorDetail } from "interfaces/cookies/consent-policy.interface";

@Component({
    selector: "region-rule",
    templateUrl: "./region-rule.component.html",
})
export class RegionRule implements OnInit {
    @Input() ruleSetting: IRegionRuleSetting;
    @Output() deleteRuleCallback = new EventEmitter();
    @Output() onRuleEdit = new EventEmitter<boolean>();
    @Output() ruleUpdatedCallback = new EventEmitter<IRegionRuleDetail>();

    groupColumns: typeof CookieConsentPolicyDetailGroupColumns;
    regionMeta: ISettingsMeta[];
    domainGroupColumns: IdomainGroupTableColumns[] = [];

    private consentModelOptions: ICookieNameId[];
    private impliedBehaviorOptions: IImpliedOptionDetail[] = [];
    private regionList: ICookieNameId[];
    private siteVisitorOptions: ICookieNameId[];
    private vendorList: IConsentPolicyVendorDetail[];

    constructor(
        private modalService: ModalService,
        private permissions: Permissions,
        private consentPolicyHelper: ConsentPolicyHelperService,
    ) { }

    ngOnInit() {
        this.domainGroupColumns.push(
            { labelKey: CookieConsentPolicyDetailGroupColumns.GroupName, cellValueKey: "name", tooltipDescriptionKey: "GroupNameDescription" },
            { labelKey: CookieConsentPolicyDetailGroupColumns.Status },
        );
        if (this.permissions.canShow("CookieAllowGroupDNT")) {
            this.domainGroupColumns.push({ labelKey: CookieConsentPolicyDetailGroupColumns.DoNotTrack, tooltipDescriptionKey: "DNTDescription" });
        }
        this.groupColumns = CookieConsentPolicyDetailGroupColumns;
        this.consentModelOptions = this.consentPolicyHelper.getConsentModel();
        this.impliedBehaviorOptions = cloneDeep(this.consentPolicyHelper.getImpliedBehaviorOptions());
        this.regionList = this.consentPolicyHelper.getRegionList();
        this.siteVisitorOptions = this.consentPolicyHelper.getSiteVisitorOptions();
        this.vendorList = this.consentPolicyHelper.getVendorList();
        this.regionMeta = [
            {
                actionType: CoookieConsentPolicyRuleControls.ShowBanner,
                description: "ShowBannerDescription",
                isVisible: true,
                title: "ShowBanner",
                type: SettingsType.CHECKBOX,
            }, {
                actionType: CoookieConsentPolicyRuleControls.ConsentModel,
                isVisible: true,
                labelKey: "name",
                linkText: "LearnMore",
                linkType: CoookieConsentPolicyRuleControls.StatusModal,
                options: this.consentModelOptions,
                showLink: true,
                title: "ConsentModel",
                type: SettingsType.PICKLIST,
                valueKey: "id",
                children: [{
                    actionType: CoookieConsentPolicyRuleControls.ImpliedBehaviors,
                    isVisible: false,
                    title: "ImpliedBehaviors",
                    type: SettingsType.LOOKUP,
                    labelKey: "name",
                    valueKey: "id",
                    options: this.impliedBehaviorOptions,
                    isMultiselect: true,
                    visibleString: CoookieConsentPolicyRuleControls.ShowImpliedBehaviors,
                    placeholder: "SelectImpliedBehaviors",
                }],
            }, {
                actionType: CoookieConsentPolicyRuleControls.EnableIAB,
                description: "EnableIabConsentSupportDescription",
                isVisible: true,
                title: "EnableIAB",
                type: SettingsType.TOGGLE,
                children: [{
                    actionType: CoookieConsentPolicyRuleControls.VendorVersion,
                    isVisible: false,
                    title: "VendorVersionList",
                    type: SettingsType.PICKLIST,
                    labelKey: "ListName",
                    valueKey: "ListId",
                    options: this.vendorList,
                    placeholder: "SelectVendorVersionList",
                    visibleString: CoookieConsentPolicyRuleControls.EnableIAB,
                }],
            }, {
                actionType: CoookieConsentPolicyRuleControls.EnableLogging,
                isVisible: true,
                title: "EnableLogging",
                type: SettingsType.TOGGLE,
                children: [{
                    actionType: CoookieConsentPolicyRuleControls.ThirdPartyEuConsentCookie,
                    isVisible: false,
                    title: "EuConsentCookie",
                    type: SettingsType.TOGGLE,
                    visibleString: CoookieConsentPolicyRuleControls.EnableLogging,
                }, {
                    actionType: CoookieConsentPolicyRuleControls.SiteVisitorId,
                    isVisible: false,
                    title: "UniqueSiteVisitorID",
                    placeholder: "CookieId",
                    type: SettingsType.PICKLIST,
                    labelKey: "name",
                    valueKey: "id",
                    options: this.siteVisitorOptions,
                    isDisabled: true,
                    visibleString: CoookieConsentPolicyRuleControls.EnableLogging,
                }],
            },
        ];
    }

    onEditRule() {
        this.ruleSetting.editMode = !this.ruleSetting.editMode;
        this.ruleSetting.isToggled = !this.ruleSetting.isToggled && this.ruleSetting.editMode ? true : this.ruleSetting.isToggled;
        this.onRuleEdit.emit(this.ruleSetting.editMode);
    }

    onDeleteRule() {
        this.deleteRuleCallback.emit();
    }

    onTogglerule() {
        this.ruleSetting.isToggled = !this.ruleSetting.isToggled;
        if (!this.ruleSetting.isToggled) {
            this.ruleSetting.editMode = false;
            this.onRuleEdit.emit(this.ruleSetting.editMode);
        }
    }

    onGroupDNTToggle(value: boolean, group: IConsentPolicyGroupDetail) {
        group.isDntEnabled = value;
    }

    onRuleNameChange(value: string) {
        this.ruleSetting.name = value;
    }

    onRegionSelectionChange({ currentValue, change, previousValue }) {
        this.ruleSetting.regions = currentValue;
        if (!previousValue || currentValue.length > previousValue.length) {
            const optionIndex: number = findIndex(this.regionList, (option: ICookieNameId): boolean => {
                return change.id === option.id;
            });
            this.regionList.splice(optionIndex, 1);
        } else if (change.id && !some(this.regionList, { id: change.id })) {
            this.regionList.push(change);
        }
    }

    handleAction(action: IAction) {
        const payload = action.payload;
        switch (action.type) {
            case CoookieConsentPolicyRuleControls.ShowBanner:
                this.ruleSetting.showBanner = payload.event;
                break;
            case CoookieConsentPolicyRuleControls.ConsentModel:
                if (payload.event.id === this.ruleSetting.consentModel.id) return;
                this.ruleSetting.consentModel = payload.event;
                this.consentPolicyHelper.setGroupStatus(this.ruleSetting, payload.event);
                if (payload.event.id === CookieConsentPolicyConsentModel.ImpliedConsent) {
                    this.ruleSetting.showImpliedBehaviors = true;
                } else {
                    this.ruleSetting.showImpliedBehaviors = false;
                }
                break;
            case CoookieConsentPolicyRuleControls.ImpliedBehaviors:
                const { currentValue, change, previousValue } = payload.event;
                this.ruleSetting.impliedBehaviors = currentValue;
                if (!previousValue || currentValue.length > previousValue.length) {
                    const optionIndex: number = findIndex(this.impliedBehaviorOptions, (option: ICookieNameId): boolean => {
                        return change.id === option.id;
                    });
                    this.impliedBehaviorOptions.splice(optionIndex, 1);
                } else if (change.id && !some(this.impliedBehaviorOptions, { id: change.id })) {
                    this.impliedBehaviorOptions.push(change);
                }
                break;
            case CoookieConsentPolicyRuleControls.EnableIAB:
                this.ruleSetting.enableIAB = payload.event;
                break;
            case CoookieConsentPolicyRuleControls.VendorVersion:
                this.ruleSetting.vendorVersion = payload.event;
                break;
            case CoookieConsentPolicyRuleControls.EnableLogging:
                this.ruleSetting.enabledConsentLogging = payload.event;
                break;
            case CoookieConsentPolicyRuleControls.StatusModal:
                this.modalService.openModal("downgradeConsentPolicyStatusModal");
                break;

        }
    }

}
