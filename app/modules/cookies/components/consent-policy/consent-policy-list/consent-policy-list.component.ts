// Angular
import {
    Component,
    OnInit,
    Inject,
} from "@angular/core";
import { StateService } from "@uirouter/core";
import { IPromise } from "angular";

import { each } from "lodash";

// Services
import { ModalService } from "sharedServices/modal.service";
import { ConsentPolicyApiService } from "modules/cookies/services/api/consent-policy-api.service";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

// Constants
import { CookieConsentPolicyStatus } from "constants/cookies/cookie-consent-policy.constant";

// Interfaces
import { IBannerTemplate } from "interfaces/cookies/cc-banner-template.interface";
import { IConsentPolicy } from "interfaces/cookies/cookie-list-consent-detail.interface";
import { ICookiePageHeaderButtonConfiguration } from "interfaces/cookies/cookie-page-header.interface";
import { IDeleteConfirmationModalResolve } from "interfaces/delete-confirmation-modal-resolve.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";
import { IPublishTemplateModalResolver } from "interfaces/cookies/publish-template-modal.interface";
import { ICreatePolicyRequestBody } from "interfaces/cookies/cookie-list-consent-detail.interface";
import { IDropdownOption } from "interfaces/one-dropdown.interface";

@Component({
    selector: "consent-policy-list",
    templateUrl: "./consent-policy-list.component.html",
})
export class ConsentPolicyList implements OnInit {
    columns = [];
    consentPolicyStatus: typeof CookieConsentPolicyStatus;
    consentPolicies: IConsentPolicy[];
    isReady: boolean;
    primaryButton: ICookiePageHeaderButtonConfiguration;

    constructor(
        private stateService: StateService,
        private translate: TranslatePipe,
        private modalService: ModalService,
        private consentPolicyApiService: ConsentPolicyApiService,
    ) { }

    ngOnInit() {
        this.consentPolicyStatus = CookieConsentPolicyStatus;
        this.primaryButton = {
            labelKey: "CreateNew",
            isDisabled: !this.isReady,
            otAutoId: "CookieConsentPolicyCreateNewButton",
            action: () => {
                this.launchAddPolicyModal();
            },
        };
        this.columns = [
            { columnHeader: this.translate.transform("ConsentPolicyName"), columnTitleKey: "listName", dataValueKey: "ConsentPolicyName", clickable: true },
            { columnHeader: this.translate.transform("Regions"), columnTitleKey: "Regions", dataValueKey: "NumberOfRegion", clickable: false },
            { columnHeader: this.translate.transform("Domains"), columnTitleKey: "Domains", dataValueKey: "NumberOfDomains", clickable: false },
            { columnHeader: this.translate.transform("Organization"), columnTitleKey: "Organization", dataValueKey: "OrgName", clickable: false },
            { columnHeader: this.translate.transform("DefaultConsentModel"), columnTitleKey: "DefaultConsentModel", dataValueKey: "DefaultConsentModel", clickable: false },
            { columnHeader: this.translate.transform("Status"), columnTitleKey: "Status", dataValueKey: "Status", clickable: false },
        ];
        this.getConsentPolicies();
    }

    deleteConsentPolicy = (bannerTemplate): void => {
        const modalData: IDeleteConfirmationModalResolve = {
            modalTitle: this.translate.transform("DeleteConsentPolicy"),
            confirmationText: this.translate.transform("DeleteConsentPolicyMessage", { TemplateDraftName: bannerTemplate.ListName }),
            cancelButtonText: this.translate.transform("Cancel"),
            submitButtonText: this.translate.transform("Submit"),
            // TODO Integrate services once the API is ready
            promiseToResolve:
                (): any => new Promise((resolve, reject) => {
                    setTimeout(() => {
                        resolve(true);
                    }, 300);
                }),
        };
        this.modalService.setModalData(modalData);
        this.modalService.openModal("deleteConfirmationModal");
    }

    assignConsentPolicy(bannerTemplate: IBannerTemplate): void {
        const modalData: IPublishTemplateModalResolver = {
            modalTitle: this.translate.transform("AssignConsentPolicy"),
            publishConfirmationText: this.translate.transform("PublishConfirmationText"),
            cancelButtonText: this.translate.transform("Cancel"),
            submitButtonText: this.translate.transform("Submit"),
            // TODO Integrate services once the API is ready
            templateId: 249,
        };
        this.modalService.setModalData(modalData);
        this.modalService.openModal("publishTemplateModal");
    }

    goToConsentDetailPage(Id: number): void {
        this.stateService.go("zen.app.pia.module.cookiecompliance.views.consentpolicy.detail", { consentPolicyId: Id });
    }

    private populateContextMenuOptions(consentPolicy: IConsentPolicy): IDropdownOption[] {
        return [
            {
                text: this.translate.transform("Edit"),
                action: (row): void => {
                    this.launchEditPolicyModal(row);
                },
            },
            {
                text: this.translate.transform("AssignConsentPolicy"),
                action: (row): void => {
                    this.assignConsentPolicy(row);
                },
            },
            {
                text: this.translate.transform("Delete"),
                isDisabled: consentPolicy.DefaultConsentPolicy,
                action: (row): void => {
                    this.deleteConsentPolicy(row);
                },
            },
        ];
    }

    private launchAddPolicyModal() {
        this.modalService.setModalData({
            modalTitleKey: "CreateNewPolicy",
            isEdit: false,
            consentDetail: {
                isDefault: this.consentPolicies.length === 0,
            },
            promiseToResolve: (data: ICreatePolicyRequestBody): IPromise<number> => this.createConsentPolicy(data),
        });
        this.modalService.openModal("consentPolicyDetailModal");
    }

    private launchEditPolicyModal(consent: IConsentPolicy) {
        this.modalService.setModalData({
            modalTitleKey: "EditPolicy",
            isEdit: true,
            consentDetail: {
                name: consent.ConsentPolicyName,
                orgId: consent.OrgId,
                description: consent.Description,
                isDefault: consent.DefaultConsentPolicy,
            },
            promiseToResolve: (data: ICreatePolicyRequestBody): IPromise<boolean> => this.updateConsentPolicy(consent.id, data),
        });
        this.modalService.openModal("consentPolicyDetailModal");
    }

    private getConsentPolicies(): IPromise<void> {
        this.isReady = false;
        return this.consentPolicyApiService.getConsentPolicies()
            .then((response: IProtocolResponse<IConsentPolicy[]>): void => {
                this.isReady = true;
                this.primaryButton.isDisabled = false;
                this.consentPolicies = response.result ? response.data : [];
                each(this.consentPolicies, (consentPolicy: IConsentPolicy): void => {
                    consentPolicy.contextMenuOptions = this.populateContextMenuOptions(consentPolicy);
                });
            });
    }

    private createConsentPolicy(consentDetail: ICreatePolicyRequestBody): IPromise<number> {
        return this.consentPolicyApiService.createConsentPolicy(consentDetail)
            .then((response: IProtocolResponse<IConsentPolicy>): number => {
                return response.result ? response.data.id : null;
            });
    }

    private updateConsentPolicy(consentPolicyId: number, consentDetail: ICreatePolicyRequestBody): IPromise<boolean> {
        return this.consentPolicyApiService.updateConsentPolicy(consentPolicyId, consentDetail)
            .then((response: IProtocolResponse<void>): boolean => {
                return response.result;
            });
    }
}
