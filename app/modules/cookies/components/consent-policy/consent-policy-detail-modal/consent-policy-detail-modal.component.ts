// Angular
import {
    Component,
    OnInit,
    Inject,
} from "@angular/core";
import {
    FormGroup,
    FormBuilder,
    Validators,
} from "@angular/forms";
import { StateService } from "@uirouter/core";
import { IPromise } from "angular";

// 3rd party
import { find } from "lodash";

// Redux
import { StoreToken } from "tokens/redux-store.token";
import { getModalData } from "oneRedux/reducers/modal.reducer";
import {
    getOrgList,
    getCurrentOrgId,
} from "oneRedux/reducers/org.reducer";

// Services
import { ModalService } from "sharedServices/modal.service";

// Interfaces
import { IOrganization } from "interfaces/org.interface";
import { IStore } from "interfaces/redux.interface";
import { ICreatePolicyRequestBody } from "interfaces/cookies/cookie-list-consent-detail.interface";
import { IConsentPolicyMetadata } from "interfaces/cookies/consent-policy.interface";

@Component({
    selector: "consent-policy-detail-modal",
    templateUrl: "./consent-policy-detail-modal.component.html",
})
export class ConsentPolicyDetailModal implements OnInit {
    consentDetail: IConsentPolicyMetadata;
    consentPolicydetail: FormGroup;
    isEditMode = false;
    modalTitleKey: string;
    orgGroupList: IOrganization[] = [];
    saveInProgress = false;
    selectedOrgGroup: IOrganization;
    private promiseToResolve: (data: ICreatePolicyRequestBody) => IPromise<boolean | number>;

    constructor(
        @Inject(StoreToken) private store: IStore,
        private stateService: StateService,
        private modalService: ModalService,
        private formBuilder: FormBuilder,
    ) { }

    ngOnInit() {
        this.orgGroupList = this.getOrgList();
        const modalData = getModalData(this.store.getState());
        this.modalTitleKey = modalData.modalTitleKey;
        this.isEditMode = modalData.isEdit;
        this.consentDetail = modalData.consentDetail;
        this.promiseToResolve = modalData.promiseToResolve;
        this.initForm();
    }

    closeModal(): void {
        if (this.saveInProgress) return;
        this.modalService.closeModal();
        this.modalService.clearModalData();
    }

    onSubmitConsentPolicy(): void {
        const payload: ICreatePolicyRequestBody = {
            PolicyName: this.consentPolicydetail.value.name,
            OrgId: this.consentPolicydetail.value.org.Id,
            OrgName: this.consentPolicydetail.value.org.Name,
            Description: this.consentPolicydetail.value.description,
            Default: this.consentDetail.isDefault,
        };
        this.saveInProgress = true;
        this.promiseToResolve(payload)
            .then((result: boolean | number): void => {
                this.saveInProgress = false;
                if (!result) return;
                this.closeModal();
                if (!this.isEditMode) {
                    this.stateService.go("zen.app.pia.module.cookiecompliance.views.consentpolicy.detail", { consentPolicyId: result });
                    this.closeModal();
                }
            });
    }

    private initForm(): void {
        let selectedOrg: IOrganization;
        if (this.consentDetail && this.consentDetail.orgId) {
            selectedOrg = find(this.orgGroupList, (org: IOrganization) => org.Id === this.consentDetail.orgId);
        }
        this.consentPolicydetail = this.formBuilder.group({
            name: [this.consentDetail && this.consentDetail.name || "", Validators.required],
            org: [selectedOrg || null, Validators.required],
            description: [this.consentDetail && this.consentDetail.description || ""],
        });
    }

    private getOrgList(): IOrganization[] {
        const selectedOrgGroup = getCurrentOrgId(this.store.getState());
        return getOrgList(selectedOrgGroup)(this.store.getState());
    }
}
