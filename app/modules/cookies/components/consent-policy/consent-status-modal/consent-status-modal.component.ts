// Angular
import {
    Component,
} from "@angular/core";

// Services
import { ModalService } from "sharedServices/modal.service";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

// Interfaces
import { ILabelValue } from "interfaces/generic.interface";

@Component({
    selector: "consent-status-modal",
    templateUrl: "./consent-status-modal.component.html",
})
export class ConsentPolicyStatusModal {

    consentStatuses: Array<ILabelValue<string>> = [
        {
            label: this.translate.transform("NoticeOnly"),
            value: this.translate.transform("NoticeOnlyDescription"),
        }, {
            label: this.translate.transform("OptOut"),
            value: this.translate.transform("OptOutDescription"),
        }, {
            label: this.translate.transform("OptIn"),
            value: this.translate.transform("OptInDescription"),
        }, {
            label: this.translate.transform("ImpliedConsent"),
            value: this.translate.transform("ImpliedConsentDescription"),
        },
    ]

    constructor(
        private modalService: ModalService,
        private translate: TranslatePipe,
    ) { }

    closeModal(): void {
        this.modalService.closeModal();
    }

}
