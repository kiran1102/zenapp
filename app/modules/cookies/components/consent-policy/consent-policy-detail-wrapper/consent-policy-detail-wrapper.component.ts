// Angular
import {
    Component,
    OnInit,
    Inject,
} from "@angular/core";
import { StateService } from "@uirouter/core";
import { IPromise } from "angular";

// 3rd party
import { each } from "lodash";

// Constants
import { CookieConsentPolicyDetailTabs } from "constants/cookies/cookie-consent-policy.constant";

// Services
import { ConsentPolicyApiService } from "modules/cookies/services/api/consent-policy-api.service";
import { ToastService } from "@onetrust/vitreus";
import { ConsentPolicyHelperService } from "modules/cookies/services/helper/consent-policy-helper.service";
import { ModalService } from "sharedServices/modal.service";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

// Interfaces
import {
    ICookiePageHeaderBreadcrumbItem,
    ICookiePageHeaderButtonConfiguration,
} from "interfaces/cookies/cookie-page-header.interface";
import {
    IRegionRuleDetail,
    IRegionRuleSetting,
    IRegionRulePayload,
    IRegionSavePayload,
} from "interfaces/cookies/region-rule.interface";
import { ICookieNameId } from "interfaces/cookies/cookie-generic.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";
import {
    IConsentPolicyMetadata,
    IConsentPolicyVendorDetail,
} from "interfaces/cookies/consent-policy.interface";
import { IDeleteConfirmationModalResolve } from "interfaces/delete-confirmation-modal-resolve.interface";

@Component({
    selector: "consent-policy-detail-wrapper",
    templateUrl: "./consent-policy-detail-wrapper.component.html",
})
export class ConsentPolicyDetailWrapper implements OnInit {
    primaryButton: ICookiePageHeaderButtonConfiguration = {
        labelKey: "Publish",
        otAutoId: "CookieConsentPolicyDetailPublishButton",
    };
    breadcrumbItems: ICookiePageHeaderBreadcrumbItem[] = [{
        labelKey: "ConsentPolicy",
        otAutoId: "ConsentPolicyDetailNavigation",
        path: "zen.app.pia.module.cookiecompliance.views.consentpolicy.list",
    }];
    consentTabs = [{
        id: CookieConsentPolicyDetailTabs.RegionRules,
        text: "RegionRules",
        otAutoId: "CookieConsentPolicyRegionRuleTab",
    }, {
        id: CookieConsentPolicyDetailTabs.AssignedDomains,
        text: "AssignedDomains",
        otAutoId: "CookieConsentPolicyAssignedDomainsTab",
    }];
    consentPolicyTabs: typeof CookieConsentPolicyDetailTabs;
    isPageReady = false;
    isRuleInEditMode = false;
    regionSettings: IRegionRuleSetting[];
    saveInProgress = false;
    selectedTab: string;

    private consentDetail: IConsentPolicyMetadata;
    private consentPolicyId: number;
    private defaultRegion: IRegionRuleDetail;

    constructor(
        private stateService: StateService,
        private consentPolicyApiService: ConsentPolicyApiService,
        private toastService: ToastService,
        private translate: TranslatePipe,
        private consentPolicyHelper: ConsentPolicyHelperService,
        private modalService: ModalService,
    ) { }

    ngOnInit() {
        this.consentPolicyId = this.stateService.params.consentPolicyId;
        this.consentPolicyTabs = CookieConsentPolicyDetailTabs;
        this.selectedTab = this.consentTabs[0].id;
        Promise.all([
            this.getConsentModalOptions(),
            this.getImpliedVehaviorOptions(),
            this.getConsentPolicyDetail(this.consentPolicyId),
            this.getRegionList(),
            this.getVendorList(),
        ]).then((): void => {
            this.breadcrumbItems.push({
                label: this.consentDetail.name,
                otAutoId: "ConsentPolicyName",
            });
            this.getConsentPolicySettings(this.consentPolicyId).then((): void => {
                this.isPageReady = true;
            });
        });
    }

    onTabClick(tab: string) {
        this.selectedTab = tab;
    }

    onCancelConsentDetail() {
        this.stateService.go("zen.app.pia.module.cookiecompliance.views.consentpolicy.list");
    }

    onSaveConsentDetail() {
        // TODO: refactor when api functionality completely done.
        this.saveInProgress = true;
        const payload: IRegionSavePayload = this.consentPolicyHelper.generateCreateConsentSettingPayload(this.regionSettings);
        const promises = [];
        each(payload.regionsToCreate, (req: IRegionRulePayload): void => {
            promises.push(this.consentPolicyApiService.createConsentPolicySetting(this.consentPolicyId, req));
        });
        each(payload.regionsToUpdate, (req: IRegionRulePayload): void => {
            promises.push(this.consentPolicyApiService.updateConsentPolicySetting(this.consentPolicyId, req.id, req));
        });
        Promise.all(promises).then((): void => {
            this.saveInProgress = false;
            this.stateService.go("zen.app.pia.module.cookiecompliance.views.consentpolicy.detail", { consentPolicyId: this.consentPolicyId });
        });
    }

    onRuleEdit(value: boolean) {
        this.isRuleInEditMode = value;
    }

    onAddRegionRule() {
        // TODO API call
        this.regionSettings.unshift(this.consentPolicyHelper.extractConsentSetting(this.defaultRegion, true));
    }

    onRuleDelete(ruleIndex: number) {
        if (this.regionSettings[ruleIndex].id) {
            const modalData: IDeleteConfirmationModalResolve = {
                modalTitle: this.translate.transform("RemoveRegion"),
                confirmationText: this.translate.transform("RemoveRegionWarning"),
                cancelButtonText: this.translate.transform("Cancel"),
                submitButtonText: this.translate.transform("Remove"),
                customConfirmationText: this.translate.transform("DoYouWantToContinue"),
                promiseToResolve: (): IPromise<boolean> => {
                    return this.consentPolicyApiService.removeConsentPolicySetting(this.consentPolicyId, this.regionSettings[ruleIndex].id)
                        .then((response: IProtocolResponse<void>): boolean => {
                            if (!response.result) return false;
                            this.regionSettings.splice(ruleIndex, 1);
                            return response.result;
                        });
                },
            };
            this.modalService.setModalData(modalData);
            this.modalService.openModal("deleteConfirmationModal");
        } else {
            this.regionSettings.splice(ruleIndex, 1);
        }
    }

    trackByRuleIndex(index: number, item: IRegionRuleSetting): number {
        return index;
    }

    private getConsentModalOptions(): IPromise<void> {
        if (this.consentPolicyHelper.getConsentModel().length) return;
        return this.consentPolicyApiService.getConsentModelOptions()
            .then((response: IProtocolResponse<ICookieNameId[]>): void => {
                this.consentPolicyHelper.setConsentModel(response.result ? response.data : []);
            });
    }

    private getImpliedVehaviorOptions(): IPromise<void> {
        if (this.consentPolicyHelper.getImpliedBehaviorOptions().length) return;
        return this.consentPolicyApiService.getImpliedBehaviorOptions()
            .then((response: IProtocolResponse<string[]>): void => {
                this.consentPolicyHelper.setImpliedBehaviorOptions(response.result ? response.data : []);
            });
    }

    private getConsentPolicyDetail(consentPolicyId: number): IPromise<void> {
        return this.consentPolicyApiService.getConsentPolicyById(consentPolicyId)
            .then((response: IProtocolResponse<IConsentPolicyMetadata>): void => {
                this.consentDetail = response.result ? response.data : null;
            });
    }

    private getConsentPolicySettings(consentPolicyId: number): IPromise<void> {
        return this.consentPolicyApiService.getConsentPolicySettings(consentPolicyId)
            .then((response: IProtocolResponse<IRegionRuleDetail[]>): void => {
                this.defaultRegion = response.data[0];
                this.regionSettings = this.consentPolicyHelper.extractConsentsettings(response.result ? response.data : []);
            });
    }

    private getVendorList(): IPromise<void> {
        if (this.consentPolicyHelper.getVendorList().length) return;
        return this.consentPolicyApiService.getVendorList()
            .then((response: IProtocolResponse<IConsentPolicyVendorDetail[]>): void => {
                this.consentPolicyHelper.setVendorList(response.result ? response.data : []);
            });
    }

    private getRegionList(): IPromise<void> {
        if (this.consentPolicyHelper.getRegionList().length) return;
        return this.consentPolicyApiService.getRegionList()
            .then((response: IProtocolResponse<ICookieNameId[]>): void => {
                this.consentPolicyHelper.setRegionList(response.result ? response.data : []);
            });
    }
}
