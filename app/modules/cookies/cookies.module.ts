import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";
// 3rd Party
import { DiffMatchPatchModule } from "ng-diff-match-patch";

// Modules
import { SharedModule } from "sharedModules/shared.module";
import { UpgradedLegacyModule } from "sharedModules/upgrade/upgraded-legacy.module";
import { CommonModule } from "@angular/common";
import { HttpClientModule } from "@angular/common/http";
import { FormsModule } from "@angular/forms";
import { ReactiveFormsModule } from "@angular/forms";
import { VitreusModule } from "@onetrust/vitreus";
import { DndListModule } from "ngx-drag-and-drop-lists";
import { OTPipesModule, PIPES } from "modules/pipes/pipes.module";
import { VirtualScrollModule } from "od-virtualscroll";
import { OneVideoModule } from "modules/video/video.module";
import { CookiesV2Module } from "cookiesV2Module/cookies-v2.module";

// Components
import { WebsiteListTableComponent } from "generalcomponent/Cookies/website-list-table/website-list-table.component";
import { IABVendorTableComponent } from "generalcomponent/Cookies/IABVendorsList/cc-iab-vendor-table.component";
import { CcLanguageModal } from "generalcomponent/Cookies/Shared/Modal/cc-language-modal.component";
import { CCIABVendorDetails } from "generalcomponent/Cookies/IABVendorsList/cc-iab-vendor-details.component";
import { CCIABVendorsList } from "generalcomponent/Cookies/IABVendorsList/cc-iab-vendors-list.component";
import { AssignTemplateComponent } from "generalcomponent/Cookies/Shared/Modal/cc-assign-template-modal/assign-template.component";

import { AddTemplateModalController } from "generalcomponent/Cookies/Shared/Modal/add-template-modal/add-template-modal.component";
import { EditTemplateModalController } from "generalcomponent/Cookies/Shared/Modal/add-template-modal/edit-template-modal.component";
import { PublishTemplateModalController } from "generalcomponent/Cookies/Shared/Modal/add-template-modal/publish-template-modal.component";

import { CUSTOM_ELEMENTS_SCHEMA } from "@angular/core";

import { TemplateNewLanguageComponent } from "generalcomponent/Cookies/Shared/Modal/template-new-language/template-new-language.component";
import { ScanBehindTutorialComponent } from "generalcomponent/Cookies/Shared/Modal/cc-scan-login-modal/scan-behind-tutorial-modal.component";
import { CcPreferenceCenterIabEditDetails } from "generalcomponent/Cookies/PreferenceCenter/cc-preference-center-iab-edit-details.component";
import { AddCustomCookieModal } from "modules/cookies/components/policy/add-custom-cookie-modal/add-custom-cookie-modal.component";
import { OrgGroupReAssignModal } from "modules/cookies/components/website-list/org-group-re-assign-modal/org-group-re-assign-modal.component";
import { CCGroupModal } from "modules/cookies/components/policy/cc-group-modal/cc-group-modal.component";
import { RemoveCookieModal } from "modules/cookies/components/policy/remove-cookie-modal/remove-cookie-modal.component";

import { ScriptIntegrationSettings } from "modules/cookies/components/script-integration/script-integration-settings/script-integration-settings-component";
import { PolicyConfiguration } from "modules/cookies/components/policy/policy-configuration/policy-configuration.component";
import { CcLanguageSelectionComponent } from "generalcomponent/Cookies/Shared/cc-upgrade-language-selection-component";
import { ScheduleAuditModal } from "modules/cookies/components/website-list/schedule-audit/cc-schedule-audit-modal.component";
import { AddVendorListModal } from "modules/cookies/components/iab/add-vendor-modal/cc-add-vendor-list-modal.component";
import { CookieDeleteConfirmationComponent } from "modules/cookies/components/policy/cookie-delete-confirmation-modal/cookie-delete-confirmation-modal.component";
import { CookieLimitModal } from "modules/cookies/components/policy/cookie-limit-modal/cookie-limit-modal.component";
import { PreferenceCenterPreviewComponent } from "modules/cookies/components/preference-center/preference-center-preview/preference-center-preview.component";

// Website components
import { ScanLoginDetail } from "modules/cookies/components/website-list/scan-login/scan-login-detail/scan-login-detail.component";

// Consent policy components
import { ConsentPolicyList } from "modules/cookies/components/consent-policy/consent-policy-list/consent-policy-list.component";
import { ConsentPolicyDetailModal } from "modules/cookies/components/consent-policy/consent-policy-detail-modal/consent-policy-detail-modal.component";
import { ConsentPolicyDetailWrapper } from "modules/cookies/components/consent-policy/consent-policy-detail-wrapper/consent-policy-detail-wrapper.component";
import { RegionRule } from "modules/cookies/components/consent-policy/region-rule/region-rule.component";
import { ConsentPolicyStatusModal } from "modules/cookies/components/consent-policy/consent-status-modal/consent-status-modal.component";

// Share components
import { CookiePageHeader } from "modules/cookies/components/shared/cookie-page-header/cookie-page-header.component";
import { ThirdPartyBannerNotification } from "modules/cookies/components/shared/notification/thirdparty-banner-notification.component";

// Upgraded components
import { PreferenceCenterEditDetails } from "modules/cookies/components/preference-center/preference-center-details/preference-center-details.component";
import { PreferenceCenterStyling } from "modules/cookies/components/preference-center/preference-center-styling/preference-center-styling.component";

// Services
import { CookieTemplateService } from "cookiesService/cookie-template.service";
import { CookieIABIntegrationService } from "cookiesService/cookie-iab-integration.service";
import { CookieComplianceStore } from "cookiesService/cookie-compliance-store.service";
import { WebAuditService } from "cookiesService/web-audit.service";
import { CookieConsentService } from "cookiesService/cookie-consent.service";
import { CookieMultiLanguageService } from "cookiesService/cookie-multi-language.service";
import { CookiePolicyService } from "cookiesService/cookie-policy.service";
import { CookieScriptIntegrationService } from "cookiesService/cookie-script-integration.service";
import { CookieLanguageSwitcherService } from "cookiesService/cookie-language-switcher.service";
import { ToastService } from "@onetrust/vitreus";
import { CookiePreferenceCenterService } from "cookiesService/cookie-preference-center.service";
import { LanguagePollingService } from "cookiesService/cookie-language-polling.service";
import { CookieListApiService } from "modules/cookies/services/api/cookie-list-api.service";
import { CookieListHelperService } from "modules/cookies/services/helper/cookie-list-helper.service";
import { CookieOrgGroupService } from "cookiesService/cookie-org-group.service";
import { CookieApiService } from "modules/cookies/shared/services/cookie-api.service";
import { CookieAuditScheduleService } from "cookiesService/cookie-audit-schedule.service";
import { ConsentPolicyApiService } from "modules/cookies/services/api/consent-policy-api.service";
import { ConsentPolicyHelperService } from "modules/cookies/services/helper/consent-policy-helper.service";
import { ScriptArchiveApiService } from "modules/cookies/services/api/script-archive-api.service";
import { CookieSideMenuHelperService } from "modules/cookies/services/helper/cookie-side-menu-helper.service";
import { ScanLoginApiService } from "modules/cookies/services/api/scan-login-api.service";
import { CookiesStorageService } from "modules/cookies/services/helper/cookies-storage.service";

export function getToasterService(injector) {
    return injector.get("ToastService");
}

export function getCookiePreferenceCenterService(injector) {
    return injector.get("CookiePreferenceCenterService");
}

export function getLanguagePollingService(injector) {
    return injector.get("LanguagePollingService");
}

export function getLegacyCookieTemplateService(injector) {
    return injector.get("CookieTemplateService");
}

export function getLegacyCookieComplianceStore(injector) {
    return injector.get("CookieComplianceStore");
}

export function getLegacyWebAuditService(injector) {
    return injector.get("WebAuditService");
}

export function getLegacyCookieConsentService(injector) {
    return injector.get("CookieConsentService");
}

export function getLegacyCookieMultiLanguageService(injector) {
    return injector.get("CookieMultiLanguageService");
}

export function getIABCookieIntegrationService(injector) {
    return injector.get("CookieIABIntegrationService");
}

export function getCookiePolicyService(injector) {
    return injector.get("CookiePolicyService");
}

export function getCookieAuditScheduleService(injector) {
    return injector.get("CookieAuditScheduleService");
}

export function getLegacyCookieScriptIntegrationService(injector) {
    return injector.get("CookieScriptIntegrationService");
}

export function getLegacyCookieLanguageSwitcherService(injector) {
    return injector.get("CookieLanguageSwitcherService");
}

export function getLegacyCookieOrgGroupService(injector) {
    return injector.get("CookieOrgGroupService");
}

// Only the components needs to be downgraded
const NG2_COMPONENTS_TO_BE_DOWNGRADED = [
    WebsiteListTableComponent,
    IABVendorTableComponent,
    CCIABVendorDetails,
    CCIABVendorsList,
    AddTemplateModalController,
    PublishTemplateModalController,
    AssignTemplateComponent,
    EditTemplateModalController,
    TemplateNewLanguageComponent,
    ScanBehindTutorialComponent,
    CcPreferenceCenterIabEditDetails,
    AddCustomCookieModal,
    OrgGroupReAssignModal,
    CCGroupModal,
    RemoveCookieModal,
    ScriptIntegrationSettings,
    PolicyConfiguration,
    CcLanguageModal,
    CcLanguageSelectionComponent,
    PreferenceCenterEditDetails,
    PreferenceCenterStyling,
    ScheduleAuditModal,
    AddVendorListModal,
    ConsentPolicyList,
    ConsentPolicyDetailModal,
    ConsentPolicyDetailWrapper,
    ConsentPolicyStatusModal,
    CookieLimitModal,
    ThirdPartyBannerNotification,
    CookieDeleteConfirmationComponent,
    ScanLoginDetail,
];

const NG2_COMPONENTS_TO_BE_DECLARED = [
    WebsiteListTableComponent,
    IABVendorTableComponent,
    CCIABVendorDetails,
    CCIABVendorsList,
    AddTemplateModalController,
    PublishTemplateModalController,
    AssignTemplateComponent,
    EditTemplateModalController,
    TemplateNewLanguageComponent,
    ScanBehindTutorialComponent,
    CcPreferenceCenterIabEditDetails,
    AddCustomCookieModal,
    OrgGroupReAssignModal,
    CCGroupModal,
    RemoveCookieModal,
    ScriptIntegrationSettings,
    PolicyConfiguration,
    CcLanguageModal,
    CcLanguageSelectionComponent,
    PreferenceCenterEditDetails,
    PreferenceCenterStyling,
    ScheduleAuditModal,
    AddVendorListModal,
    ConsentPolicyList,
    CookiePageHeader,
    ConsentPolicyDetailModal,
    ConsentPolicyDetailWrapper,
    RegionRule,
    ConsentPolicyStatusModal,
    CookieLimitModal,
    ThirdPartyBannerNotification,
    CookieDeleteConfirmationComponent,
    ScanLoginDetail,
];

@NgModule({
    imports: [
        SharedModule,
        UpgradedLegacyModule,
        CommonModule,
        HttpClientModule,
        FormsModule,
        ReactiveFormsModule,
        VitreusModule,
        DndListModule,
        OTPipesModule,
        VirtualScrollModule,
        OneVideoModule,
        DiffMatchPatchModule,
        RouterModule,
        CookiesV2Module,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA],
    declarations: NG2_COMPONENTS_TO_BE_DECLARED,
    entryComponents: NG2_COMPONENTS_TO_BE_DOWNGRADED,
    providers: [
        { provide: CookieTemplateService, deps: ["$injector"], useFactory: getLegacyCookieTemplateService },
        { provide: CookieComplianceStore, deps: ["$injector"], useFactory: getLegacyCookieComplianceStore },
        { provide: WebAuditService, deps: ["$injector"], useFactory: getLegacyWebAuditService },
        { provide: CookieConsentService, deps: ["$injector"], useFactory: getLegacyCookieConsentService },
        { provide: CookieMultiLanguageService, deps: ["$injector"], useFactory: getLegacyCookieMultiLanguageService },
        { provide: CookieIABIntegrationService, deps: ["$injector"], useFactory: getIABCookieIntegrationService },
        { provide: CookiePolicyService, deps: ["$injector"], useFactory: getCookiePolicyService },
        { provide: CookieAuditScheduleService, deps: ["$injector"], useFactory: getCookieAuditScheduleService },
        { provide: CookieScriptIntegrationService, deps: ["$injector"], useFactory: getLegacyCookieScriptIntegrationService },
        { provide: ToastService, deps: ["$injector"], useFactory: getToasterService },
        { provide: CookieLanguageSwitcherService, deps: ["$injector"], useFactory: getLegacyCookieLanguageSwitcherService },
        { provide: CookiePreferenceCenterService, deps: ["$injector"], useFactory: getCookiePreferenceCenterService },
        { provide: LanguagePollingService, deps: ["$injector"], useFactory: getLanguagePollingService },
        { provide: CookieOrgGroupService, deps: ["$injector"], useFactory: getLegacyCookieOrgGroupService },
        CookieListApiService,
        CookieListHelperService,
        CookieApiService,
        ConsentPolicyApiService,
        ConsentPolicyHelperService,
        ScriptArchiveApiService,
        CookieSideMenuHelperService,
        ScanLoginApiService,
        CookiesStorageService,
        PIPES, // TODO: Remove from providers when ot-common is implemented. Only necessary if you are using pipe in TS file, else just module is fine
    ],
})
export class CookiesModule { }
