// Angular
import {
    Injectable,
    Inject,
} from "@angular/core";
import { IPromise } from "angular";

// Services
import { ProtocolService } from "modules/core/services/protocol.service";

// Interfaces
import {
    IProtocolConfig,
    IProtocolResponse,
    IProtocolMessages,
} from "interfaces/protocol.interface";
import { ICookieNameId } from "interfaces/cookies/cookie-generic.interface";
import {
    ICreatePolicyRequestBody,
    IConsentPolicy,
} from "interfaces/cookies/cookie-list-consent-detail.interface";
import { IRegionRulePayload } from "interfaces/cookies/region-rule.interface";
import {
    IConsentPolicyMetadata,
    IConsentPolicyVendorDetail,
} from "interfaces/cookies/consent-policy.interface";
import { IRegionRuleDetail } from "interfaces/cookies/region-rule.interface";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

@Injectable()
export class ConsentPolicyApiService {

    constructor(
        private translatePipe: TranslatePipe,
        private OneProtocol: ProtocolService,
    ) { }

    getConsentModelOptions(isDeleted = false): IPromise<IProtocolResponse<ICookieNameId[]>> {
        const config: IProtocolConfig = this.OneProtocol.config("GET", "/consentpolicies/consent-models", { isDeleted });
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("CouldNotGetConsentModels") } };
        return this.OneProtocol.http(config, messages);
    }

    getConsentPolicies(): IPromise<IProtocolResponse<IConsentPolicy[]>> {
        const config: IProtocolConfig = this.OneProtocol.config("GET", "/consentpolicies");
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("CouldNotGetConsentPolicies") } };
        return this.OneProtocol.http(config, messages);
    }

    createConsentPolicy(request: ICreatePolicyRequestBody): IPromise<IProtocolResponse<IConsentPolicy>> {
        const config: IProtocolConfig = this.OneProtocol.config("POST", "/consentpolicies", [], request);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("CouldNotCreateConsentPolicy") } };
        return this.OneProtocol.http(config, messages);
    }

    getImpliedBehaviorOptions(): IPromise<IProtocolResponse<string[]>> {
        const config: IProtocolConfig = this.OneProtocol.config("GET", "/consentpolicies/consent-models/implied-behaviors");
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("CouldNotGetImpliedBehaviorOptions") } };
        return this.OneProtocol.http(config, messages);
    }

    updateConsentPolicy(consentPolicyId: number, payload: ICreatePolicyRequestBody): IPromise<IProtocolResponse<void>> {
        const config: IProtocolConfig = this.OneProtocol.config("PUT", `/consentpolicies/${consentPolicyId}/consent-policy`, [], payload);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("CouldNotUpdateConsentPolicy") } };
        return this.OneProtocol.http(config, messages);
    }

    getConsentPolicyById(consentPolicyId: number): IPromise<IProtocolResponse<IConsentPolicyMetadata>> {
        const config: IProtocolConfig = this.OneProtocol.config("GET", `/consentpolicies/${consentPolicyId}`);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("CouldNotGetConsentPolicy") } };
        return this.OneProtocol.http(config, messages);
    }

    getConsentPolicySettings(consentPolicyId: number): IPromise<IProtocolResponse<IRegionRuleDetail[]>> {
        const config: IProtocolConfig = this.OneProtocol.config("GET", `/consentpolicies/${consentPolicyId}/consent-policy-settings`);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("CouldNotGetConsentPolicySettings") } };
        return this.OneProtocol.http(config, messages);
    }

    createConsentPolicySetting(consentPolicyId: number, payload: IRegionRulePayload): IPromise<IProtocolResponse<number>> {
        const config: IProtocolConfig = this.OneProtocol.config("POST", `/consentpolicies/${consentPolicyId}/consent-policy-settings`, [], payload);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("CouldNotCreateConsentPolicySetting") } };
        return this.OneProtocol.http(config, messages);
    }

    updateConsentPolicySetting(consentPolicyId: number, settingId: number, payload: IRegionRulePayload): IPromise<IProtocolResponse<number>> {
        const config: IProtocolConfig = this.OneProtocol.config("PUT", `/consentpolicies/${consentPolicyId}/consent-policy-settings/${settingId}`, [], payload);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("CouldNotUpdateConsentPolicySetting") } };
        return this.OneProtocol.http(config, messages);
    }

    getRegionList(): IPromise<IProtocolResponse<ICookieNameId[]>> {
        const config: IProtocolConfig = this.OneProtocol.config("GET", "/consentpolicies/regions");
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("CouldNotGetRegionList") } };
        return this.OneProtocol.http(config, messages);
    }

    getVendorList(): IPromise<IProtocolResponse<IConsentPolicyVendorDetail[]>> {
        const config: IProtocolConfig = this.OneProtocol.config("GET", "/vendor/vendorLists/list");
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("CouldNotGetVendorList") } };
        return this.OneProtocol.http(config, messages);
    }

    removeConsentPolicySetting(consentPolicyId: number, consentSettingId: number): IPromise<IProtocolResponse<void>> {
        const config: IProtocolConfig = this.OneProtocol.config("DELETE", `/consentpolicies/${consentPolicyId}/consent-policy-settings/${consentSettingId}`);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("CouldNotRemoveConsentPolicySetting") } };
        return this.OneProtocol.http(config, messages);
    }
}
