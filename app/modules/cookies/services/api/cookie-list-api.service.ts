// Angular
import {
    Injectable,
    Inject,
} from "@angular/core";
import { IPromise } from "angular";

// Services
import { ProtocolService } from "modules/core/services/protocol.service";

// Interfaces
import {
    IProtocolConfig,
    IProtocolResponse,
    IProtocolMessages,
} from "interfaces/protocol.interface";
import {
    ICookieListConsentDetail,
    IConsentSettingDetail,
} from "interfaces/cookies/cookie-list-consent-detail.interface";
import { ICookieGroupDetail } from "interfaces/cookies/cookie-group-detail.interface";
import { IPurpose } from "interfaces/cookies/cc-iab-purpose.interface";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

@Injectable()
export class CookieListApiService {

    constructor(
        private translatePipe: TranslatePipe,
        private OneProtocol: ProtocolService,
    ) { }

    getPublishedStatus(domainId: number): IPromise<IProtocolResponse<{ unpublished: boolean }>> {
        const config: IProtocolConfig = this.OneProtocol.config("GET", "/cookiepolicy/isUnpublished", { domainid: domainId });
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("CouldNotGetPublishedStatus") } };
        return this.OneProtocol.http(config, messages);
    }

    getAssignedCookies(domainId: number): IPromise<IProtocolResponse<ICookieGroupDetail[]>> {
        const config: IProtocolConfig = this.OneProtocol.config("GET", "/cookiepolicy/assignedcookies", { domainId });
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("ErrorGettingAssignedCookies") } };
        return this.OneProtocol.http(config, messages);
    }

    getUnAssignedCookies(domainId: number): IPromise<IProtocolResponse<ICookieGroupDetail[]>> {
        const config: IProtocolConfig = this.OneProtocol.config("GET", "/cookiepolicy/unassignedcookies", { domainId });
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("ErrorGettingUnAssignedCookies") } };
        return this.OneProtocol.http(config, messages);
    }

    getConsentDetails(domainId: number): IPromise<IProtocolResponse<ICookieListConsentDetail>> {
        const config: IProtocolConfig = this.OneProtocol.config("GET", `/CookiePolicy/domains/${domainId}/cookieconsentset`);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("CouldNotGetConsentDetail") } };
        return this.OneProtocol.http(config, messages);
    }

    getPurposes(): IPromise<IProtocolResponse<IPurpose[]>> {
        const config: IProtocolConfig = this.OneProtocol.config("GET", `/ConsentNotice/purposes`);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("CouldNotGetPurposes") } };
        return this.OneProtocol.http(config, messages);
    }

    updateConsentDetails(domainId: number, consentSettings: ICookieListConsentDetail): IPromise<IProtocolResponse<void>> {
        const config: IProtocolConfig = this.OneProtocol.config("POST", `/CookiePolicy/domains/${domainId}/cookieconsentset`, [], consentSettings);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("CouldNotUpdate") } };
        return this.OneProtocol.http(config, messages);
    }

    getCollectionPoint(domainId: number): IPromise<IProtocolResponse<string | void>> {
        const config: IProtocolConfig = this.OneProtocol.config("GET", `/consentNotice/domains/${domainId}/consenttrans`);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("ErrorFetchingCollectionPoint") } };
        return this.OneProtocol.http(config, messages);
    }

    updateCustomLogging(domainId: number, organizationId: string, IsConsentLoggingEnabled: boolean): IPromise<IProtocolResponse<void>> {
        const config: IProtocolConfig = this.OneProtocol.config("POST", `/consentNotice/domains/${domainId}/logging`, null, { organizationId, IsConsentLoggingEnabled }, null, null, null, "text");
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("ErrorEnableLogging") } };
        return this.OneProtocol.http(config, messages);
    }

    updateDntSetting(domainId: number, isDntEnabled: boolean): IPromise<IProtocolResponse<{ groupIds: number[] }>> {
        const config: IProtocolConfig = this.OneProtocol.config("PUT", `/cookiePolicy/domains/${domainId}/dntSetting`, [], { isDntEnabled });
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("CouldNotUpdate") } };
        return this.OneProtocol.http(config, messages);
    }

    getOldcookies(domainId: number): IPromise<IProtocolResponse<ICookieGroupDetail[]>> {
        const config: IProtocolConfig = this.OneProtocol.config("GET", `/cookiepolicy/GetOldCookies`, { domainId });
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("ErrorFetchingOldCookies") } };
        return this.OneProtocol.http(config, messages);
    }

    deleteOldcookies(cookieIds: string[]): IPromise<IProtocolResponse<void>> {
        const config: IProtocolConfig = this.OneProtocol.config("PUT", `/cookiepolicy/DeleteOldCookies`, [], cookieIds);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("CouldNotRemoveOldcookies") } };
        return this.OneProtocol.http(config, messages);
    }

    updateConsentConfiguration(domainId: number, payload: IConsentSettingDetail): IPromise<IProtocolResponse<void>> {
        const config: IProtocolConfig = this.OneProtocol.config("PUT", `/CookiePolicy/domains/${domainId}/consentsetting`, [], payload);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("CouldNotUpdate") } };
        return this.OneProtocol.http(config, messages);
    }

    updateIabConfiguration(domainId: number, payload: { IsIABEnabled: boolean, SelectedList: number }): IPromise<IProtocolResponse<void>> {
        const config: IProtocolConfig = this.OneProtocol.config("PUT", `/CookiePolicy/domains/${domainId}/iabconsentsetting`, [], payload);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("CouldNotUpdate") } };
        return this.OneProtocol.http(config, messages);
    }

    movePurposes(domainId: number, groupId: number, purposes: number[]): IPromise<IProtocolResponse<void>> {
        const config: IProtocolConfig = this.OneProtocol.config("PUT", `/ConsentNotice/domains/${domainId}/groups/${groupId}/purposes`, [], purposes);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("ErrorMovingPurposes") } };
        return this.OneProtocol.http(config, messages);
    }

    removePurposes(domainId: number, groupId: number, purposeId: number): IPromise<IProtocolResponse<boolean>> {
        const config: IProtocolConfig = this.OneProtocol.config("DELETE", `/ConsentNotice/domains/${domainId}/groups/${groupId}/purposes/${purposeId}`);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("ErrorDeletingPurposes") } };
        return this.OneProtocol.http(config, messages);
    }

    getGroupCookies(domainId: number, groupId: number): IPromise<IProtocolResponse<ICookieGroupDetail>> {
        const config: IProtocolConfig = this.OneProtocol.config("GET", "/Cookiepolicy/GetGroupCookies", { domainId, groupId });
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("ErrorGettingCookies") } };
        return this.OneProtocol.http(config, messages);
    }

    moveCookies(domainId: number, groupId: number, cookieIds: number[]): IPromise<IProtocolResponse<void>> {
        const config: IProtocolConfig = this.OneProtocol.config("PUT", "/Cookiepolicy/MoveCookies", { domainId, groupId }, cookieIds);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("ErrorMovingCookies") } };
        return this.OneProtocol.http(config, messages);
    }
}
