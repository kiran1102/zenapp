// Angular
import { Injectable } from "@angular/core";
import { IPromise } from "angular";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

// Services
import { ProtocolService } from "modules/core/services/protocol.service";

// Interfaces
import {
    IProtocolConfig,
    IProtocolResponse,
    IProtocolMessages,
} from "interfaces/protocol.interface";
import { IScanLoginDetails } from "interfaces/cookies/cc-scan-login-details.interface";

@Injectable()
export class ScanLoginApiService {
    constructor(
        private translatePipe: TranslatePipe,
        private OneProtocol: ProtocolService,
    ) { }

    getScanLoginDetail(domainName: string): IPromise<IProtocolResponse<IScanLoginDetails | null>> {
        const config: IProtocolConfig = this.OneProtocol.config("GET", "/audit/GetLoginInfo", { domain: domainName });
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("ErrorGettingScanLoginDetails") } };
        return this.OneProtocol.http(config, messages);
    }

    updateScanLoginDetail(loginDetails: IScanLoginDetails): IPromise<IProtocolResponse<boolean>> {
        const config: IProtocolConfig = this.OneProtocol.config("PUT", "/audit/CreateOrUpdateLoginInfo", [], loginDetails);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("ErrorUpdatingScanLoginDetails") } };
        return this.OneProtocol.http(config, messages);
    }

    deleteScanLoginInfo(domainName: string): IPromise<IProtocolResponse<IScanLoginDetails>> {
        const config: IProtocolConfig = this.OneProtocol.config("DELETE", "/audit/DeleteLoginInfo", { domain: domainName });
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("ErrorDeletingScanLoginDetails") } };
        return this.OneProtocol.http(config, messages);
    }
}
