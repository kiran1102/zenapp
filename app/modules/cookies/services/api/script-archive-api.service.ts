// Angular
import {
    Injectable,
    Inject,
} from "@angular/core";
import { IPromise } from "angular";

// Services
import { ProtocolService } from "modules/core/services/protocol.service";

// Interfaces
import {
    IProtocolConfig,
    IProtocolResponse,
    IProtocolMessages,
} from "interfaces/protocol.interface";
import { ICookieScriptVersionDiffResponse } from "interfaces/cookies/cookie-publish-script.interface";
import { IScriptArchiveDetail } from "interfaces/cookies/cc-script-archive.interface";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

@Injectable()
export class ScriptArchiveApiService {

    constructor(
        private translatePipe: TranslatePipe,
        private OneProtocol: ProtocolService,
    ) { }

    getScriptHistory(domainId: number): IPromise<IProtocolResponse<IScriptArchiveDetail[]>> {
        const config: IProtocolConfig = this.OneProtocol.config("GET", "/cookiepolicy/GetScriptHistory", { domainId });
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("ErrorGettingScriptHistory") } };
        return this.OneProtocol.http(config, messages);
    }

    getArchiveScriptVersionDiff(archivescriptId: number): ng.IPromise<IProtocolResponse<ICookieScriptVersionDiffResponse>> {
        const config: IProtocolConfig = this.OneProtocol.config("GET", `/consentnotice/archivescripts/${archivescriptId}/versiondiff`);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("ErrorGettingDomainJqueryVersion") } };
        return this.OneProtocol.http(config, messages);
    }

    restorePublishedArchiveScript(archiveScriptId: number): ng.IPromise<IProtocolResponse<void>> {
        const config: IProtocolConfig = this.OneProtocol.config("POST", `/consentnotice/archivescript/${archiveScriptId}/restore-publish`, []);
        const messages: IProtocolMessages = {
            Error: { custom: this.translatePipe.transform("ErrorPublishingConsentNotice") },
            Success: { custom: this.translatePipe.transform("SuccessFullyPublished") },
        };
        return this.OneProtocol.http(config, messages);
    }
}
