// Angular
import { Injectable } from "@angular/core";

// Rxjs
import {
    BehaviorSubject,
    Subject,
} from "rxjs";

import { CcConsentNotice } from "interfaces/cookies/cc-consent-notice.interface";
import { IBannerLanguage } from "interfaces/cookies/cc-banner-languages.interface";
import { CookiesModel } from "interfaces/cookies/cookies-model.interface";
import { IBannerModel } from "interfaces/cookies/cc-banner-template.interface";

// Enums
import { EditableCookieBannerItems } from "enums/cookies/cookie-banner.enum";

@Injectable()
export class CookiesStorageService {

    /* TODO BKEL 11/29/18: When all components relying on cc.controller's $scope.model are
    converted to A6, use this to store the model and other $scope stored variables.*/
    model: BehaviorSubject<CookiesModel> = new BehaviorSubject(null);
    bannerModel = new Subject<IBannerModel>();
    consentNotice: BehaviorSubject<CcConsentNotice> = new BehaviorSubject(new CcConsentNotice());
    bannerDomain: BehaviorSubject<string> = new BehaviorSubject(null);
    selectedLanguage: BehaviorSubject<IBannerLanguage> = new BehaviorSubject(null);
    loadingWebsites: BehaviorSubject<boolean> = new BehaviorSubject(null);

    bannerItemBeingEdited: BehaviorSubject<EditableCookieBannerItems> = new BehaviorSubject(null);

    constructor() { }

    createConsentBanner(domain: string) {
        this.bannerDomain.next(domain);
    }

}
