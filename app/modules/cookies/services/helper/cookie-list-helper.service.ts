// Rxjs
import { Subject } from "rxjs";

// Angular
import { Injectable } from "@angular/core";

// Services
import { CookieListApiService } from "modules/cookies/services/api/cookie-list-api.service";

// Interfaces
import { IProtocolResponse } from "interfaces/protocol.interface";
import { IPurpose } from "interfaces/cookies/cc-iab-purpose.interface";
import { ICookieListConsentDetail } from "interfaces/cookies/cookie-list-consent-detail.interface";

@Injectable()
export class CookieListHelperService {
    private consentCollectionPointId: string;
    private consentDetails: ICookieListConsentDetail;
    private domainId: number;

    private purposeRemoved = new Subject();
    // tslint:disable-next-line:member-ordering
    $purposeRemoved = this.purposeRemoved.asObservable();

    private dntEnabled = new Subject();
    // tslint:disable-next-line:member-ordering
    $dntEnabled = this.dntEnabled.asObservable();

    constructor(
        private CookieListApiService: CookieListApiService,
    ) { }

    setCollectionPoint(domainId: number): void {
        this.CookieListApiService.getCollectionPoint(domainId)
            .then((response: IProtocolResponse<string | void>): void => {
                this.consentCollectionPointId = response.data ? response.data : null;
            });
    }

    getCollectionPoint(): string {
        return this.consentCollectionPointId;
    }

    addPurpose(purpose: IPurpose): void {
        this.purposeRemoved.next(purpose);
    }

    onDntToggle(groupIds: number[]): void {
        this.dntEnabled.next(groupIds);
    }

    setDomainId(domainId: number): void {
        this.domainId = domainId;
    }

    setConsentDetails(consentDetails: ICookieListConsentDetail): void {
        this.consentDetails = consentDetails;
    }

    getDomainId(): number {
        return this.domainId;
    }

    getConsentDetails(): ICookieListConsentDetail {
        return this.consentDetails;
    }
}
