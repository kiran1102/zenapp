// Angular
import { Injectable } from "@angular/core";

import {
    each,
    find,
    findIndex,
    keyBy,
} from "lodash";

// Enums
import { CookieConsentPolicyConsentModel } from "enums/cookies/cookie-list.enum";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

// Interfaces
import {
    IRegionRuleDetail,
    IRegionRuleSetting,
    IConsentPolicyGroupDetail,
    IImpliedOptionDetail,
    IRegionSavePayload,
} from "interfaces/cookies/region-rule.interface";
import { ICookieNameId } from "interfaces/cookies/cookie-generic.interface";
import { IConsentPolicyVendorDetail } from "interfaces/cookies/consent-policy.interface";
import { ImpliedBehaviorOptionKeys } from "constants/cookies/cookie-consent-policy.constant";
import { IKeyValue } from "interfaces/generic.interface";

@Injectable()
export class ConsentPolicyHelperService {
    private consentModelOptions: ICookieNameId[] = [];
    private impliedBehaviorOptions: IImpliedOptionDetail[] = [];
    private regionList: ICookieNameId[] = [];
    private siteVisitorOptions: ICookieNameId[] = [{
        id: 1,
        name: "Cookie ID",
    }];
    private vendorList: IConsentPolicyVendorDetail[] = [];

    constructor(
        private translate: TranslatePipe,
    ) { }

    extractConsentsettings(regionRules: IRegionRuleDetail[]): IRegionRuleSetting[] {
        const settings: IRegionRuleSetting[] = [];
        each(regionRules, (rule: IRegionRuleDetail): void => {
            settings.push(this.extractConsentSetting(rule));
        });
        return settings;
    }

    extractConsentSetting(rule: IRegionRuleDetail, iSDefaultRegion: boolean = false): IRegionRuleSetting {
        const setting: IRegionRuleSetting = {
            id: iSDefaultRegion ? null : rule.id,
            editMode: iSDefaultRegion,
            isToggled: true,
            isDefault: iSDefaultRegion ? false : rule.isDefault,
            name: iSDefaultRegion ? "" : rule.name,
            showBanner: JSON.parse(rule.settingDetails.ShowBanner),
            groupStatuses: rule.groupStatuses,
            showImpliedBehaviors: false,
            consentModel: null,
            impliedBehaviors: this.getImpliedBehaviorSelection(rule),
            enableIAB: JSON.parse(rule.settingDetails.EnableIAB),
            enabledConsentLogging: JSON.parse(rule.settingDetails.EnabledConsentLogging),
            siteVisitorId: this.siteVisitorOptions[0],
            thirdPartyEuConsentCookie: false,
            regions: iSDefaultRegion ? [] : rule.regions,
            vendorVersion: rule.settingDetails.VendorVersionList ? this.getVendorVersionSelection(JSON.parse(rule.settingDetails.VendorVersionList)) : null,
        };
        setting.consentModel = this.getConsentModelSelection(setting, rule.settingDetails.ConsentModel);
        return setting;
    }

    generateCreateConsentSettingPayload(regionSettings: IRegionRuleSetting[]): IRegionSavePayload {
        const payload: IRegionSavePayload = {
            regionsToCreate: [],
            regionsToUpdate: [],
        };
        each(regionSettings, (setting: IRegionRuleSetting): void => {
            const req = {
                id: setting.id,
                name: setting.name,
                regions: setting.regions.map((e) => e.id),
                groupStatuses: setting.groupStatuses,
                otherSettings: this.getRuleSettings(setting),
            };
            setting.id ? payload.regionsToUpdate.push(req) : payload.regionsToCreate.push(req);
        });
        return payload;
    }

    setVendorList(vendors: IConsentPolicyVendorDetail[]) {
        this.vendorList = vendors;
    }

    getVendorList(): IConsentPolicyVendorDetail[] {
        return this.vendorList;
    }

    setRegionList(regions: ICookieNameId[]) {
        this.regionList = regions;
    }

    getRegionList(): ICookieNameId[] {
        return this.regionList;
    }

    setConsentModel(consentModels: ICookieNameId[]) {
        this.consentModelOptions = consentModels;
    }

    getConsentModel(): ICookieNameId[] {
        return this.consentModelOptions;
    }

    getImpliedBehaviorOptions(): IImpliedOptionDetail[] {
        return this.impliedBehaviorOptions;
    }

    setImpliedBehaviorOptions(options: string[]) {
        this.impliedBehaviorOptions = [];
        each(options, (key: string, index: number): void => {
            this.impliedBehaviorOptions.push({
                id: index,
                key,
                name: this.translate.transform(key),
            });
        });
    }

    getSiteVisitorOptions(): ICookieNameId[] {
        return this.siteVisitorOptions;
    }

    setGroupStatus(setting: IRegionRuleSetting, option: ICookieNameId) {
        each(setting.groupStatuses, (group: IConsentPolicyGroupDetail): void => {
            group.status = option;
        });
    }

    private getRuleSettings(setting: IRegionRuleSetting): Array<IKeyValue<string>> {
        const attributes = [];
        attributes.push(
            {
                key: "ConsentModel",
                value: setting.consentModel.name,
            }, {
                key: "ShowBanner",
                value: setting.showBanner.toString(),
            }, {
                key: "EnableIAB",
                value: setting.enableIAB.toString(),
            }, {
                key: "EnabledConsentLogging",
                value: setting.enabledConsentLogging.toString(),
            }, {
                key: "ClickAcceptAll",
                value: "false",
            }, {
                key: "ExcludePages",
                value: "false",
            }, {
                key: "ScrollAcceptAll",
                value: "false",
            }, {
                key: "NextPage",
                value: "false",
            },
        );
        const attributeIndex = findIndex(attributes, (attribute: IKeyValue<string>): boolean => attribute.key === setting.impliedBehaviors.key);
        attributes[attributeIndex].value = "true";
        return attributes;
    }

    private getConsentModelSelection(setting: IRegionRuleSetting, value: string): ICookieNameId {
        const selection = find(this.consentModelOptions, (option: ICookieNameId) => option.name.toLowerCase() === value.toLowerCase());
        if (selection.id === CookieConsentPolicyConsentModel.ImpliedConsent) {
            setting.showImpliedBehaviors = true;
        } else {
            setting.showImpliedBehaviors = false;
        }
        this.setGroupStatus(setting, selection);
        return selection;
    }

    private getVendorVersionSelection(version: number): IConsentPolicyVendorDetail {
        return find(this.vendorList, (vendor: IConsentPolicyVendorDetail) => vendor.ListId === version);
    }

    private getImpliedBehaviorSelection(rule: IRegionRuleDetail): IImpliedOptionDetail {
        const impliedOptionByKey = keyBy(this.impliedBehaviorOptions, "key");
        if (rule.settingDetails.ScrollAcceptAll) {
            return impliedOptionByKey[ImpliedBehaviorOptionKeys.ScrollAcceptAll];
        } else if (rule.settingDetails.NextPage) {
            return impliedOptionByKey[ImpliedBehaviorOptionKeys.NextPage];
        } else if (rule.settingDetails.ExcludePages) {
            return impliedOptionByKey[ImpliedBehaviorOptionKeys.ExcludePages];
        } else if (rule.settingDetails.ClickAcceptAll) {
            return impliedOptionByKey[ImpliedBehaviorOptionKeys.ClickAcceptAll];
        }
        return;
    }
}
