// Angular
import { Injectable } from "@angular/core";
import { StateService } from "@uirouter/core";

// Interfaces
import { INavMenuItem } from "modules/global-sidebar/interfaces/nav-menu-item.interface";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";
import { PermitPipe } from "pipes/permit.pipe";

// Services
import { GlobalSidebarService } from "modules/global-sidebar/services/one-global-sidebar.service";

@Injectable()
export class CookieSideMenuHelperService {

    constructor(
        private translatePipe: TranslatePipe,
        private permitPipe: PermitPipe,
        private globalSidebarService: GlobalSidebarService,
    ) {

    }
    getMenuRoutes(state) {
        const menuGroup: INavMenuItem[] = [];
        menuGroup.push({
            id: "CookieWebsiteListSidebarItem",
            icon: "ot ot-browser-alt",
            title: this.translatePipe.transform("Websites"),
            route: "zen.app.pia.module.cookiecompliance.views.websites",
            activeRouteFn: (stateService: StateService) => {
                return stateService.includes("zen.app.pia.module.cookiecompliance.views.websites")
                    || stateService.includes("zen.app.pia.module.cookiecompliance.views.scanlogin");
            },
        });

        if (state.model.selectedWebsite) {
            menuGroup.push({
                id: "CookieScanResultsSidebarItem",
                icon: "fa fa-tachometer",
                title: this.translatePipe.transform("ScanResults"),
                route: "zen.app.pia.module.cookiecompliance.views.reports",
                activeRouteFn: (stateService: StateService) => {
                    return stateService.includes("zen.app.pia.module.cookiecompliance.views.reports")
                        || stateService.includes("zen.app.pia.module.cookiecompliance.views.cookies")
                        || stateService.includes("zen.app.pia.module.cookiecompliance.views.tags")
                        || stateService.includes("zen.app.pia.module.cookiecompliance.views.pages")
                        || stateService.includes("zen.app.pia.module.cookiecompliance.views.forms")
                        || stateService.includes("zen.app.pia.module.cookiecompliance.views.localStorage");
                },
            });
        }

        // TODO: to be removed , when websites- v2 is ready
        if (this.permitPipe.transform("UniversalMigratedCookieModule")) {
            menuGroup.push({
                id: "CookieScanResultsSidebarItem",
                icon: "ot ot-tachometer",
                title: this.translatePipe.transform("ScanResults"),
                route: "zen.app.pia.module.cookiecompliance.views.scanresults",
                activeRoute: "zen.app.pia.module.cookiecompliance.views.scanresults",
            });
        }

        if (state.showCookieBannerTemplate && state.model.selectedWebsite) {
            menuGroup.push({
                id: "CookieTemplatesSidebarItem",
                icon: "ot ot-templates",
                title: this.translatePipe.transform("Templates"),
                route: "zen.app.pia.module.cookiecompliance.views.templates",
                activeRoute: "zen.app.pia.module.cookiecompliance.views.templates",
            });
        }

        // Cookie categorizations - V2
        if (this.permitPipe.transform("UniversalMigratedCookieModule")) {
            menuGroup.push({
                id: "CookieTemplatesSidebarItem",
                icon: "ot ot-templates", // TODO: update icon
                title: this.translatePipe.transform("Categorizations"),
                route: "zen.app.pia.module.cookiecompliance.views.categorizations.list",
                activeRoute: "zen.app.pia.module.cookiecompliance.views.categorizations",
            });
        }

        menuGroup.push({
            id: "CookieBannerSidebarItem",
            icon: "ot ot-cookie-banner",
            title: this.translatePipe.transform("CookieBanner"),
            activeRouteFn: (stateService: StateService) => {
                return stateService.includes("zen.app.pia.module.cookiecompliance.views.layout")
                    || stateService.includes("zen.app.pia.module.cookiecompliance.views.banner")
                    || stateService.includes("zen.app.pia.module.cookiecompliance.views.cookieEmptyState", { pageName: state.pageNames.cookieBanner });
            },
            action: () => {
                state.createOrManageBanner();
            },
        });

        menuGroup.push({
            id: "CookieDisclosureSidebarItem",
            icon: "fa fa-list-ul",
            title: this.translatePipe.transform("CookieDisclosure"),
            activeRouteFn: (stateService: StateService) => {
                return stateService.includes("zen.app.pia.module.cookiecompliance.views.policy")
                    || stateService.includes("zen.app.pia.module.cookiecompliance.views.policyeditor")
                    || stateService.includes("zen.app.pia.module.cookiecompliance.views.cookieEmptyState", { pageName: state.pageNames.cookieDisclosure });
            },
            action: () => {
                state.switchCookiePolicyEditor();
            },
        });

        if (state.canShowIABSettings) {
            menuGroup.push({
                id: "CookieIABVendorListsSidebarItem",
                icon: "ot ot-iab-vendor",
                title: this.translatePipe.transform("IABVendorLists"),
                route: "zen.app.pia.module.cookiecompliance.views.iabvendorslist",
                activeRoute: "zen.app.pia.module.cookiecompliance.views.iabvendorslist",
            });
        }

        menuGroup.push({
            id: "CookiePreferenceCenterSidebarItem",
            icon: "fa fa-check-square-o",
            title: this.translatePipe.transform("PreferenceCenter"),
            activeRouteFn: (stateService: StateService) => {
                return stateService.includes("zen.app.pia.module.cookiecompliance.views.prefcenter")
                    || stateService.includes("zen.app.pia.module.cookiecompliance.views.prefcentereditor")
                    || stateService.includes("zen.app.pia.module.cookiecompliance.views.cookieEmptyState", { pageName: state.pageNames.preferenceCenter });
            },
            action: () => {
                state.switchPreferenceCenterEditor();
            },
        });

        if (state.model.selectedWebsite) {
            menuGroup.push({
                id: "CookieScriptIntegrationSidebarItem",
                icon: "fa fa-code",
                title: this.translatePipe.transform("ScriptIntegration"),
                route: "zen.app.pia.module.cookiecompliance.views.consent",
                activeRoute: "zen.app.pia.module.cookiecompliance.views.consent",
            });
        }

        if (state.canShowConsentPolicy) {
            menuGroup.push({
                id: "CookieConsentPolicySidebarItem",
                icon: "fa fa-list-ul",
                title: this.translatePipe.transform("ConsentPolicy"),
                route: "zen.app.pia.module.cookiecompliance.views.consentpolicy.list",
                activeRoute: "zen.app.pia.module.cookiecompliance.views.consentpolicy",
            });
        }

        menuGroup.push({
            id: "CookieScriptArchiveSidebarItem",
            icon: "fa fa-file-code-o",
            title: this.translatePipe.transform("ScriptArchive"),
            activeRouteFn: (stateService: StateService) => {
                return stateService.includes("zen.app.pia.module.cookiecompliance.views.archive")
                    || stateService.includes("zen.app.pia.module.cookiecompliance.views.cookieEmptyState", { pageName: state.pageNames.scriptArchive });
            },
            action: () => {
                state.switchScriptArchivePage();
            },
        });

        return menuGroup;
    }

    updateSideMenuItems(state) {
        this.globalSidebarService.set(this.getMenuRoutes(state), this.translatePipe.transform("CookieConsent"));
    }
}
