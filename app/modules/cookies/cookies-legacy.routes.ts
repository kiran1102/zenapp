import { Permissions } from "modules/shared/services/helper/permissions.service";

export function routes($stateProvider: ng.ui.IStateProvider): void {

    $stateProvider
        .state("zen.app.pia.module.cookiecompliance.views", {
            abstract: true,
            url: "",
            views: {
                cookiecompliance: {
                    template: "<ui-view></ui-view>",
                },
            },
        })
        .state("zen.app.pia.module.cookiecompliance.views.websites", {
            url: "/websites",
            template: require("views/Cookies/cookiecompliance.jade"),
            controller: "CookieComplianceWebsitesCtrl",
        })
        .state("zen.app.pia.module.cookiecompliance.views.reports", {
            url: "/reports",
            template: require("views/Cookies/cookiecompliance_reports.jade"),
            controller: "CookieComplianceReportsCtrl",
        })
        .state("zen.app.pia.module.cookiecompliance.views.iabvendorslist", {
            url: "/iabvendorslist",
            template: "<cc-iab-vendors-list></cc-iab-vendors-list>",
        })
        .state("zen.app.pia.module.cookiecompliance.views.iabvendorsdetails", {
            url: "/iabvendorsdetails?listId&listName",
            template: "<cc-iab-vendor-details></cc-iab-vendor-details>",
        })
        .state("zen.app.pia.module.cookiecompliance.views.cookies", {
            url: "/reports/cookies",
            template: require("views/Cookies/cookiecompliance_cookies.jade"),
            controller: "CookieComplianceCookiesCtrl",
        })
        .state("zen.app.pia.module.cookiecompliance.views.cookievalues", {
            url: "/reports/cookievalues?cookie&url",
            template: require("views/Cookies/cookiecompliance_cookievalues.jade"),
            controller: "CookieComplianceCookieValuesCtrl",
        })
        .state("zen.app.pia.module.cookiecompliance.views.tags", {
            url: "/reports/tags",
            template: require("views/Cookies/cookiecompliance_tags.jade"),
            controller: "CookieComplianceTagsCtrl",
        })
        .state("zen.app.pia.module.cookiecompliance.views.forms", {
            url: "/reports/forms",
            template: require("views/Cookies/cookiecompliance_forms.jade"),
            controller: "CookieComplianceFormsCtrl",
        })
        .state("zen.app.pia.module.cookiecompliance.views.formpages", {
            url: "/reports/formpages?formName",
            template: require("views/Cookies/cookiecompliance_form_pages.jade"),
            controller: "CookieComplianceFormPagesCtrl",
        })
        .state("zen.app.pia.module.cookiecompliance.views.pages", {
            url: "/reports/pages",
            template: require("views/Cookies/cookiecompliance_pages.jade"),
            controller: "CookieCompliancePagesCtrl",
        })
        .state("zen.app.pia.module.cookiecompliance.views.consent", {
            url: "/consent/scriptintegration",
            template: "<cc-script-integration></cc-script-integration>",
        })
        .state("zen.app.pia.module.cookiecompliance.views.layout", {
            url: "/consent/layout",
            template: require("views/Cookies/cookiecompliance_consent_layout.jade"),
            controller: "CookieComplianceConsentCtrl",
        })
        .state("zen.app.pia.module.cookiecompliance.views.banner", {
            url: "/consent/banner",
            template: "<downgrade-cc-banner-wrapper></downgrade-cc-banner-wrapper>",
        })
        .state("zen.app.pia.module.cookiecompliance.views.policy", {
            url: "/consent/policy",
            template: require("views/Cookies/cookiecompliance_consent_policy.jade"),
        })
        .state("zen.app.pia.module.cookiecompliance.views.policyeditor", {
            url: "/consent/policyeditor",
            template: "<cc-policy></cc-policy>",
        })
        .state("zen.app.pia.module.cookiecompliance.views.prefcenter", {
            url: "/consent/prefcenter",
            template: require("views/Cookies/cookiecompliance_consent_prefcenter.jade"),
        })
        .state("zen.app.pia.module.cookiecompliance.views.prefcentereditor", {
            url: "/consent/prefcentereditor",
            template: "<cc-preference-center></cc-preference-center>",
        })
        .state("zen.app.pia.module.cookiecompliance.views.localStorage", {
            url: "/reports/localStorage",
            template: require("views/Cookies/cookiecompliance_localStorage.jade"),
            controller: "CookieComplianceLocalStorageCtrl",
        })
        .state("zen.app.pia.module.cookiecompliance.views.archive", {
            url: "/consent/archive",
            template: "<downgrade-script-archive-wrapper></downgrade-script-archive-wrapper>",
        })
        .state("zen.app.pia.module.cookiecompliance.views.scanlogin", {
            url: "/websites/scanlogin?domain&isnew",
            template: "<downgrade-scan-login-detail></downgrade-scan-login-detail>",
        })
        .state("zen.app.pia.module.cookiecompliance.views.templates", {
            url: "/templates",
            template: "<cc-templates-table></cc-templates-table>",
        })
        .state("zen.app.pia.module.cookiecompliance.views.manage", {
            url: "/templates/manage?templateId",
            template: "<cc-template-tab></cc-template-tab>",
            params: {
                templateId: { dynamic: true },
            },
        })
        .state("zen.app.pia.module.cookiecompliance.views.consentpolicy", {
            url: "/consent-policy",
            abstract: true,
            resolve: {
                canShowConsentPolicy: [
                    "Permissions",
                    (permissions: Permissions): boolean => {
                        if (permissions.canShow("CookieConsentPolicy")) {
                            return true;
                        }
                        permissions.goToFallback();
                        return false;
                    },
                ],
            },
            template: "<ui-view></ui-view>",
        })
        .state("zen.app.pia.module.cookiecompliance.views.consentpolicy.list", {
            url: "/list",
            template: "<downgrade-consent-policy-list></downgrade-consent-policy-list>",
        })
        .state("zen.app.pia.module.cookiecompliance.views.consentpolicy.detail", {
            url: "/consent-detail/:consentPolicyId",
            resolve: {
                canShowConsentPolicy: [
                    "Permissions",
                    (permissions: Permissions): boolean => {
                        if (permissions.canShow("CookieConsentPolicy")) {
                            return true;
                        }
                        permissions.goToFallback();
                        return false;
                    },
                ],
            },
            template: "<downgrade-consent-policy-detail-wrapper></downgrade-consent-policy-detail-wrapper>",
            params: {
                consentPolicyId: null,
            },
        })
        .state("zen.app.pia.module.cookiecompliance.views.cookieEmptyState", {
            url: "/cookie-empty-state/?pageName&isDomainCreated&isTemplateAssigned",
            template: "<downgrade-cookie-empty-state></downgrade-cookie-empty-state>",
        })

        // V2 - routes TODO: to be moved to angular 2 route file
        .state("zen.app.pia.module.cookiecompliance.views.categorizations", {
            url: "/categorizations",
            abstract: true,
            resolve: {
                canShowCategorization: [
                    "Permissions",
                    (permissions: Permissions): boolean => {
                        if (permissions.canShow("UniversalMigratedCookieModule")) {
                            return true;
                        }
                        permissions.goToFallback();
                        return false;
                    },
                ],
            },
            template: "<ui-view></ui-view>",
        })

        .state("zen.app.pia.module.cookiecompliance.views.categorizations.list", {
            url: "/list",
            template: "<downgrade-cc-categorizations-wrapper></downgrade-cc-categorizations-wrapper>",
        });
}

routes.$inject = ["$stateProvider"];
