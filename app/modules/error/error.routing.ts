import { ModuleWithProviders } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { ErrorPageComponent } from "./error-page/error-page.component";

const appRoutes: Routes = [
    { path: "", component: ErrorPageComponent },
];

export const routing: ModuleWithProviders = RouterModule.forChild(appRoutes);
