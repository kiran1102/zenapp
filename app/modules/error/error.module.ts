import { NgModule } from "@angular/core";
import { OtButtonModule } from "@onetrust/vitreus";
import { TranslateModule } from "@ngx-translate/core";

import { routing } from "./error.routing";

import { ErrorPageComponent } from "./error-page/error-page.component";

@NgModule({
    imports: [
        routing,
        TranslateModule,
        OtButtonModule,
    ],
    declarations: [
        ErrorPageComponent,
    ],
})
export class ErrorModule {}
