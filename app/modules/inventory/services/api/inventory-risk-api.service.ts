// Angular
import { Injectable } from "@angular/core";

// Interfaces
import {
    IProtocolResponse,
    IProtocolConfig,
    IProtocolMessages,
} from "interfaces/protocol.interface";
import { ProtocolService } from "modules/core/services/protocol.service";
import { IGridPaginationParams } from "interfaces/grid.interface";
import { IInventoryRiskTable } from "interfaces/inventory.interface";
import { IStringMap } from "interfaces/generic.interface";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

@Injectable()
export class InventoryRiskAPIService {

    constructor(
        private OneProtocol: ProtocolService,
        private translatePipe: TranslatePipe,
    ) { }

    getInventoryRisks(recordId: string, fullTextSearch: string, filters: IStringMap<string | string[]>, requestParam: IGridPaginationParams): ng.IPromise<IProtocolResponse<IInventoryRiskTable>> {
        const requestBody = {
            fullTextSearch,
            filters,
        };
        const config: IProtocolConfig = this.OneProtocol.customConfig("POST", `/api/risk-v2/v2/risks/inventories/${recordId}/risks/`, requestParam, requestBody);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("CouldNotRetrieveInventoryRisks") } };
        return this.OneProtocol.http(config, messages);
    }
}
