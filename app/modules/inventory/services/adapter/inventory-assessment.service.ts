// 3rd party
import { Injectable, Inject } from "@angular/core";
import { map, remove, filter, flatten } from "lodash";

// Redux
import { getAssessmentList, getInventoryId } from "oneRedux/reducers/inventory.reducer";

// services
import { DMAssessmentsService } from "datamappingservice/Assessment/dm-assessments.service";
import { ProjectService } from "oneServices/project.service";
import { Permissions } from "modules/shared/services/helper/permissions.service";
import { AssessmentListApiService } from "modules/assessment/services/api/assessment-list-api.service";

// Tokens
import { StoreToken } from "tokens/redux-store.token";

// constants + enums
import { InventoryTypeTranslations } from "constants/inventory-config.constant";
import { DMAssessmentStateLabelKeys, PIAStateDescTranslationKeys } from "constants/assessment.constants";
import { AssessmentTypes } from "enums/assessment.enum";
import { InventoryPermissions } from "constants/inventory.constant";

// interfaces
import { IProtocolResponse } from "interfaces/protocol.interface";
import { IStringMap } from "interfaces/generic.interface";
import { IDMAssessmentListResponse, IDMAssessment } from "interfaces/dm-assessment.interface";
import { IInventoryAssessment } from "interfaces/inventory.interface";
import { IProjectPageResponse } from "interfaces/assessment.interface";
import { IStore } from "interfaces/redux.interface";
import { IAssessmentTable, IAssessmentFilterParams, IAssessmentTableRow } from "interfaces/assessment/assessment.interface";
import { IGridPaginationParams } from "interfaces/grid.interface";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

@Injectable()
export class InventoryAssessmentService {

    private appliedFilters: IAssessmentFilterParams[];
    private paginationParams: IGridPaginationParams = { page: 0, size: 10000, sort: "createDT,desc" };

    constructor(
        @Inject(StoreToken) private store: IStore,
        private assessmentLegacyService: DMAssessmentsService,
        private projectLegacyService: ProjectService,
        private permissionsLegacyService: Permissions,
        private assessmentListAPI: AssessmentListApiService,
        private translatePipe: TranslatePipe,
    ) { }

    public getAssessmentList(recordId: string): Promise<IInventoryAssessment[]> {
        const inventoryId: number = getInventoryId(this.store.getState());
        const assessmentPromises: Array<ng.IPromise<IProtocolResponse<IInventoryAssessment[]>>> = [];
        const dmAssessmentV2Enabled: boolean = this.permissionsLegacyService.canShow("DMAssessmentV2");
        const assessmentV2Enabled: boolean = this.permissionsLegacyService.canShow("TemplatesV2");
        if (this.permissionsLegacyService.canShow(InventoryPermissions[inventoryId].viewDMAssessmentLinks)
            && !(dmAssessmentV2Enabled)) {
            assessmentPromises.push(this.getDMAssessments(recordId));
        }
        if (this.permissionsLegacyService.canShow(InventoryPermissions[inventoryId].viewAssessmentV2Links)
            || dmAssessmentV2Enabled || assessmentV2Enabled) {
            assessmentPromises.push(this.getAssessmentsV2(recordId));
        }
        if (this.permissionsLegacyService.canShow(InventoryPermissions[inventoryId].viewPIAAssessmentLinks)
            && !(dmAssessmentV2Enabled || assessmentV2Enabled)) {
            assessmentPromises.push(this.getPIAAssessments(recordId));
        }
        return Promise.all(assessmentPromises).then((responses: Array<IProtocolResponse<IInventoryAssessment[]>>): IInventoryAssessment[] => {
            const successfulResponses: Array<IProtocolResponse<IInventoryAssessment[]>> = filter(responses, (response: IProtocolResponse<IInventoryAssessment[]>): boolean => {
                return response.result;
            });
            const successfulAssessments: IInventoryAssessment[][] = map(successfulResponses, (response: IProtocolResponse<IInventoryAssessment[]>): IInventoryAssessment[] => {
                return response.data;
            });
            return flatten(successfulAssessments);
        });
    }

    public removeAssessmentLink(recordId: string, assessmentId: string): ng.IPromise<IInventoryAssessment[]> {
        const assessmentList: IInventoryAssessment[] = getAssessmentList(this.store.getState());
        return this.projectLegacyService.removeInventoryProject(recordId, assessmentId).then((response: IProtocolResponse<null>): IInventoryAssessment[] => {
            if (response.result) remove(assessmentList, (assessment: IInventoryAssessment): boolean => assessment.id === assessmentId);
            return assessmentList;
        });
    }

    private getDMAssessments(recordId: string): ng.IPromise<IProtocolResponse<IInventoryAssessment[]>> {
        const page = 0;
        const size = 100000; // Large number to get all even though list is paginated
        const filters: IStringMap<string> = { inventoryId: recordId };
        return this.assessmentLegacyService.getAssessments({ page, size, filter: filters }).then((response: IProtocolResponse<IDMAssessmentListResponse>): IProtocolResponse<IInventoryAssessment[] | null> => {
            if (!(response.result && response.data && response.data.content)) return { result: false, data: null };
            return { result: true, data: this.formatDMAssessmentsForList(response.data.content) };
        });
    }

    private getPIAAssessments(recordId: string): ng.IPromise<IProtocolResponse<IInventoryAssessment[]>> {
        return this.projectLegacyService.getInventoryProjects(recordId).then((response: IProtocolResponse<IProjectPageResponse[]>): IProtocolResponse<IInventoryAssessment[] | null> => {
            if (!response.result) return { result: false, data: null };
            return { result: true, data: this.formatPIAAssessmentsForList(response.data) };
        });
    }

    private getAssessmentsV2(recordId: string): ng.IPromise<IProtocolResponse<IInventoryAssessment[]>> {
        this.appliedFilters = [];
        this.appliedFilters.push({ field: "inventoryId", operation: "=", value: recordId });
        return this.assessmentListAPI.getAssessments("", this.appliedFilters, this.paginationParams).then((response: IProtocolResponse<IAssessmentTable>): IProtocolResponse<IInventoryAssessment[] | null> => {
            if (!response.result) return { result: false, data: null };
            return { result: true, data: this.formatAssessmentsV2ForList(response.data, recordId) };
        });
    }

    private formatAssessmentsV2ForList(assessments: IAssessmentTable, recordId: string): IInventoryAssessment[] {
        const inventoryId: number = getInventoryId(this.store.getState());
        return map(assessments.content, (assessment: IAssessmentTableRow): IInventoryAssessment => {
            return {
                id: assessment.assessmentId,
                name: assessment.name,
                templateName: assessment.templateName,
                status: assessment.status,
                orgName: assessment.orgGroupName,
                completedDate: assessment.completedOn ? assessment.completedOn : "",
                route: "zen.app.pia.module.assessment.detail",
                routeParams: {
                    assessmentId: assessment.assessmentId,
                    launchAssessmentDetails: {
                        inventoryId: recordId,
                        inventoryTypeId: inventoryId,
                        inventoryName: "",
                    },
                },
                type: AssessmentTypes.PIAV2,
            };
        });
    }

    private formatDMAssessmentsForList(assessments: IDMAssessment[]): IInventoryAssessment[] {
        return map(assessments, (assessment: IDMAssessment): IInventoryAssessment => {
            return {
                id: assessment.assessmentId,
                name: assessment.name,
                templateName: this.translatePipe.transform(InventoryTypeTranslations[assessment.templateType]),
                status: DMAssessmentStateLabelKeys[assessment.statusId],
                orgName: assessment.orgGroup ? assessment.orgGroup.name : "",
                completedDate: assessment.completedDate ? assessment.completedDate : "",
                route: "zen.app.pia.module.datamap.views.assessment.single",
                routeParams: {
                    Id: assessment.assessmentId,
                    Version: 1,
                },
                type: AssessmentTypes.DM,
            };
        });
    }

    private formatPIAAssessmentsForList(assessments: IProjectPageResponse[]): IInventoryAssessment[] {
        return map(assessments, (assessment: IProjectPageResponse): IInventoryAssessment => {
            return {
                id: assessment.Id,
                name: assessment.Name,
                templateName: assessment.TemplateName,
                status: PIAStateDescTranslationKeys[assessment.StateDesc],
                orgName: assessment.OrgGroup,
                completedDate: assessment.CompletedDT ? assessment.CompletedDT : "",
                route: "zen.app.pia.module.projects.assessment.module.questions",
                routeParams: {
                    Id: assessment.Id,
                    Version: assessment.Version,
                },
                type: AssessmentTypes.PIA,
            };
        });
    }

}
