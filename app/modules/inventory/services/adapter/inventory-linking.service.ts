// 3rd party
import {
    Injectable,
    Inject,
} from "@angular/core";
import {
    forEach,
    map,
    uniq,
} from "lodash";
import { TranslatePipe } from "pipes/translate.pipe";

// Redux
import { InventoryActions } from "oneRedux/reducers/inventory.reducer";

// Services
import { InventoryService } from "inventorySharedModule/shared/services/inventory-api.service";
import { InventoryLineageService } from "modules/inventory/inventory-lineage/shared/services/inventory-lineage.service";
import { Permissions } from "modules/shared/services/helper/permissions.service";
import { ModalService } from "sharedServices/modal.service";

// Constants + Enums
import { InventoryTableIds, InventoryContexts } from "enums/inventory.enum";
import { CellTypes } from "enums/general.enum";
import {
    LinkedInventories,
    InventoryTranslationKeys,
    InventoryPermissions,
 } from "constants/inventory.constant";
import { NewInventorySchemaIds } from "constants/inventory-config.constant";

// Interfaces
import { IProtocolResponse } from "interfaces/protocol.interface";
import { IStore } from "interfaces/redux.interface";
import { IPaginatedResponse } from "interfaces/pagination.interface";
import { IStringMap } from "interfaces/generic.interface";
import {
    IRelatedTable,
    IRelatedListOption,
    IInventoryCellValue,
    IDataCategory,
    ILinkedElement,
    IInventoryListItem,
    ILinkedInventoryRow,
    IInventoryRecordV2,
    IPersonalDataItem,
} from "interfaces/inventory.interface";
import { IRelateInventoryModalResolve } from "interfaces/relate-inventory-modal.interface";

// Tokens
import { StoreToken } from "tokens/redux-store.token";

@Injectable()
export class InventoryLinkingService {

    public cellTypes = CellTypes;
    private tableIds = InventoryTableIds;

    constructor(
        @Inject(StoreToken) private store: IStore,
        readonly modalService: ModalService,
        private inventoryService: InventoryService,
        private inventoryLineageService: InventoryLineageService,
        private translatePipe: TranslatePipe,
        private permissions: Permissions,
    ) {}

    getLinkedInventory(recordId: string, inventoryType: number, context: number, params?: IStringMap<number>): Promise<IPaginatedResponse<ILinkedInventoryRow>> {
        params = params || (context === InventoryContexts.Lineage ? { size: 2000, page: 0 } : { size: 5, page: 0 });
        return Promise.resolve(this.inventoryService.getRelatedInventory(recordId, inventoryType, params).then((response: IProtocolResponse<IPaginatedResponse<ILinkedInventoryRow>>): IPaginatedResponse<ILinkedInventoryRow> => {
            if (!response.result) return null;
            if (context === InventoryContexts.Related) {
                this.store.dispatch({ type: InventoryActions.SET_RELATED_INVENTORY, relatedInventoryData: this.formatLinkedInventory(response.data, inventoryType), inventoryType });
            } else if (context === InventoryContexts.Lineage) {
                this.inventoryLineageService.setRelatedInventory(inventoryType, this.formatLinkedInventory(response.data, inventoryType));
                if (inventoryType === InventoryTableIds.Assets) {
                    const recordIds: string[] = uniq(map(response.data.data, (record: ILinkedInventoryRow) => {
                        return record.inventoryId;
                    }));
                    if (recordIds.length && this.permissions.canShow(InventoryPermissions[inventoryType].inventoryLinkingV2)) {
                        this.inventoryLineageService.updateState({ fetchingAssetDetails: true });
                        this.inventoryService.getDetailsByIds(NewInventorySchemaIds[inventoryType], recordIds).then((res: IProtocolResponse<{data: IInventoryRecordV2[]}>) => {
                            if (res && res.data && res.data.data) {
                                this.inventoryLineageService.setAssetDetails(res.data.data);
                            }
                            this.inventoryLineageService.updateState({ fetchingAssetDetails: false });
                        });
                    }
                }
            }
            return response.data;
        }));
    }

    getLinkedPersonalData(recordId: string, inventoryType: number, context: number, params?: IStringMap<number>): Promise<IPaginatedResponse<IPersonalDataItem>> {
        params = params || (context === InventoryContexts.Lineage ? {} : { size: 5, page: 0 });
        return this.inventoryService.getRelatedPersonalData(recordId, params).toPromise().then((response: IProtocolResponse<IPaginatedResponse<IPersonalDataItem>>): IPaginatedResponse<IPersonalDataItem> => {
            if (!response.result) return null;
            if (context === InventoryContexts.Related) {
                this.store.dispatch({ type: InventoryActions.SET_RELATED_INVENTORY, relatedInventoryData: this.formatLinkedInventory(response.data, inventoryType), inventoryType });
            } else if (context === InventoryContexts.Lineage) {
                this.inventoryLineageService.setRelatedInventory(inventoryType, this.formatLinkedInventory(response.data, inventoryType));
            }
            return response.data;
        });
    }

    getLinkedLists(recordId: string, inventoryType: number, context: number): Promise<Array<IPaginatedResponse<ILinkedInventoryRow | IPersonalDataItem>>> {
        const promises: Array<Promise<IPaginatedResponse<IPersonalDataItem | ILinkedInventoryRow>>> = [];
        forEach(LinkedInventories[inventoryType], (linkedInventoryType: number) => {
            if (linkedInventoryType === InventoryTableIds.Elements && this.permissions.canShow("EnableDataSubjectsElementsV2")) {
                promises.push(this.getLinkedPersonalData(recordId, linkedInventoryType, context));
            } else if (linkedInventoryType !== InventoryTableIds.Entities || (linkedInventoryType === InventoryTableIds.Entities && this.permissions.canShow("DataMappingEntitiesInventory"))) {
                promises.push(this.getLinkedInventory(recordId, linkedInventoryType, context));
            }
        });
        return Promise.all(promises as Array<Promise<IPaginatedResponse<IPersonalDataItem | ILinkedInventoryRow>>>);
    }

    getLinkAssociationType(contextInventoryType: string, relatedInventoryType: string): ng.IPromise<IInventoryListItem[] | null> {
        return this.inventoryService.getLinkAssociationType(contextInventoryType, relatedInventoryType).then((response: IProtocolResponse<IInventoryListItem[]>): IInventoryListItem[] | null => {
            if (!response.result) return null;
            return response.data;
        });
    }

    getRelatedInventoryList(inventoryType: number, orgGroupId: string): ng.IPromise<IRelatedListOption[] | null> {
        return this.inventoryService.getSearchedInventoryList(inventoryType, orgGroupId).then((response: IProtocolResponse<IRelatedListOption[]>): IRelatedListOption[] | null => {
            if (!response.result) return null;
            return response.data;
        });
    }

    getDataSubjectList(orgGroupId: string): ng.IPromise<IInventoryCellValue[] | null> {
        return this.inventoryService.getDataSubjects(orgGroupId).then((response: IProtocolResponse<IInventoryCellValue[]>): IInventoryCellValue[] | null => {
            if (!response.result) return null;
            return response.data;
        });
    }

    getDataSubjectElements(recordId: string, subjectId: string): ng.IPromise<ILinkedElement[] | null> {
        return this.inventoryService.getDataSubjectElements(recordId, subjectId).then((response: IProtocolResponse<ILinkedElement[]>): ILinkedElement[] | null => {
            if (!response.result) return null;
            return response.data;
        });
    }

    getDataCategoryList(subjectId: string, orgGroupId: string): ng.IPromise<IDataCategory[] | null> {
        return this.inventoryService.getDataCategories(subjectId, orgGroupId).then((response: IProtocolResponse<IDataCategory[]>): IDataCategory[] | null => {
            if (!response.result) return null;
            return response.data;
        });
    }

    formatLinkedInventory(response: IPaginatedResponse<ILinkedInventoryRow | IPersonalDataItem>, inventoryType: number): IRelatedTable {
        const hasMetaData = Boolean(response && response.meta && response.meta.page);
        const formattedData: IRelatedTable = {
            name: this.translatePipe.transform(InventoryTranslationKeys[inventoryType].relatedInventoryHeader),
            id: inventoryType,
            numberOfElements: hasMetaData ? response.meta.page.numberOfElements : 0,
            totalElements: hasMetaData ? response.meta.page.totalElements : 0,
            size: hasMetaData ? response.meta.page.size : 0,
            totalPages: hasMetaData ? response.meta.page.totalPages : 0,
            first: hasMetaData ? response.meta.page.first : true,
            last: hasMetaData ? response.meta.page.last : true,
            number: hasMetaData ? response.meta.page.number : 0,
            columns: [],
            rows: this.permissions.canShow("EnableDataSubjectsElementsV2") && inventoryType === this.tableIds.Elements && response && response.data ?
                this.formatPersonalDataResponse(response.data) :
                response && response.data ? response.data : [],
        };
        switch (inventoryType) {
            case this.tableIds.Assets:
                formattedData.columns = [
                    { label: this.translatePipe.transform("Name"), dataKey: "name", type: this.cellTypes.link },
                    { label: this.translatePipe.transform("Organization"), dataKey: "organization", type: this.cellTypes.text },
                    { label: this.translatePipe.transform("Location"), rowTranslationKey: "locationKey", dataKey: "location", type: this.cellTypes.text },
                    { label: this.translatePipe.transform("Relation"), rowTranslationKey: "inventoryAssociationTypeKey", type: this.cellTypes.text },
                ];
                break;
            case this.tableIds.Processes:
                formattedData.columns = [
                    { label: this.translatePipe.transform("Name"), dataKey: "name", type: this.cellTypes.link },
                    { label: this.translatePipe.transform("Organization"), dataKey: "organization", type: this.cellTypes.text },
                    { label: this.translatePipe.transform("Relation"), rowTranslationKey: "inventoryAssociationTypeKey", type: this.cellTypes.text },
                ];
                break;
            case this.tableIds.Elements:
                formattedData.columns = [
                    { label: this.translatePipe.transform("CategoryOfDataSubject"), dataKey: "subject", rowTranslationKey: "subjectKey", type: this.cellTypes.text },
                    { label: this.translatePipe.transform("DataElementsCategory"), dataKey: "category", rowTranslationKey: "categoryKey", type: this.cellTypes.text },
                    { label: this.translatePipe.transform("DataElement"), dataKey: "name", rowTranslationKey: "nameKey", type: this.cellTypes.text },
                    { label: this.translatePipe.transform("Classification"), dataKey: "classification", rowTranslationKey: "classificationKey", type: this.cellTypes.text },
                    { label: this.translatePipe.transform("Organization"), dataKey: "organization", type: this.cellTypes.text },
                ];
                break;
            case this.tableIds.Vendors:
                formattedData.columns = [
                    { label: this.translatePipe.transform("Name"), dataKey: "name", type: this.cellTypes.link },
                    { label: this.translatePipe.transform("Type"), dataKey: "type", type: this.cellTypes.text },
                    { label: this.translatePipe.transform("Organization"), dataKey: "organization", type: this.cellTypes.text },
                    { label: this.translatePipe.transform("Relation"), rowTranslationKey: "inventoryAssociationTypeKey", type: this.cellTypes.text },
                ];
                break;
            case this.tableIds.Entities:
                formattedData.columns = [
                    { label: this.translatePipe.transform("Name"), dataKey: "name", type: this.cellTypes.link },
                    { label: this.translatePipe.transform("Organization"), dataKey: "organization", type: this.cellTypes.text },
                    { label: this.translatePipe.transform("EntitiesPrimaryOperatingLocation"), rowTranslationKey: "primaryOperatingLocationKey", dataKey: "primaryOperatingLocation", type: this.cellTypes.text },
                    { label: this.translatePipe.transform("Relation"), rowTranslationKey: "inventoryAssociationTypeKey", type: this.cellTypes.text },
                ];
                break;
        }
        if (inventoryType === this.tableIds.Elements && this.permissions.canShow("EnableDataSubjectsElementsV2")) {
            formattedData.columns.pop();
        }
        return formattedData;
    }

    formatPersonalDataResponse(rowsData) {
        if (!rowsData.length) return [];
        rowsData.forEach((row) => {
            row.subject = row.dataSubjectType ? row.dataSubjectType.name : "";
            row.subjectKey = row.dataSubjectType ? row.dataSubjectType.nameKey : "";
            row.name = row.dataElement ? row.dataElement.name : "";
            row.nameKey = row.dataElement ? row.dataElement.nameKey : "";
            if (row.dataClassifications) {
                row.classification = "";
                row.category = "";
                row.dataClassifications.forEach((classification, index, array) => {
                    if (classification.nameKey) {
                        row.classification = `${row.classification}${this.translatePipe.transform(classification.nameKey)}`;
                    } else {
                        row.classification = `${row.classification}${classification.name}`;
                    }
                    if (index !== array.length - 1) row.classification = `${row.classification}, `;
                });
                row.dataCategories.forEach((category, index, array) => {
                    if (category.nameKey) {
                        row.category = `${row.category}${this.translatePipe.transform(category.nameKey)}`;
                    } else {
                        row.category = `${row.category}${category.name}`;
                    }
                    if (index !== array.length - 1) row.category = `${row.category}, `;
                });
            }
        });
        return rowsData;
    }

    addInventoryRelationship(modalData: IRelateInventoryModalResolve) {
        this.modalService.setModalData(modalData);
        this.modalService.openModal("linkInventoryModal");
    }

    addDataSubjectRelationship(modalData: IRelateInventoryModalResolve) {
        this.modalService.setModalData(modalData);
        this.modalService.openModal("linkDataSubjectModal");
    }
}
