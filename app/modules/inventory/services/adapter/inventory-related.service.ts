// 3rd party
import {
    Injectable,
    Inject,
} from "@angular/core";
import {
    forEach,
    uniq,
    map,
} from "lodash";

// Redux
import { InventoryActions } from "oneRedux/reducers/inventory.reducer";

// services
import { InventoryService } from "inventorySharedModule/shared/services/inventory-api.service";
import { InventoryLineageService } from "modules/inventory/inventory-lineage/shared/services/inventory-lineage.service";
import { ModalService } from "sharedServices/modal.service";

// constants + enums
import { InventoryTableIds, InventoryContexts } from "enums/inventory.enum";
import { CellTypes } from "enums/general.enum";
import {
    RelatedInventories,
    InventoryTranslationKeys,
    InventoryPermissions,
 } from "constants/inventory.constant";
import { NewInventorySchemaIds } from "constants/inventory-config.constant";

// interfaces
import { IProtocolResponse } from "interfaces/protocol.interface";
import { Permissions } from "modules/shared/services/helper/permissions.service";
import {
    IRelatedTable,
    IRelatedListOption,
    IInventoryCellValue,
    IDataCategory,
    ILinkedInventoryRow,
    IInventoryRecordV2,
    IRelatedTableRow,
} from "interfaces/inventory.interface";
import { IPaginatedResponse } from "interfaces/pagination.interface";
import { IStore } from "interfaces/redux.interface";
import { IStringMap } from "interfaces/generic.interface";
import { IRelateInventoryModalResolve } from "interfaces/relate-inventory-modal.interface";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

// Tokens
import { StoreToken } from "tokens/redux-store.token";

@Injectable()
export class InventoryRelatedService {

    cellTypes = CellTypes;
    private tableIds = InventoryTableIds;

    constructor(
        @Inject(StoreToken) private store: IStore,
        readonly modalService: ModalService,
        private translatePipe: TranslatePipe,
        private inventoryService: InventoryService,
        private inventoryLineageService: InventoryLineageService,
        private permissions: Permissions,
    ) {}

    getRelatedInventory(recordId: string, inventoryType: number, context: number, params?: IStringMap<number>): ng.IPromise<IPaginatedResponse<ILinkedInventoryRow | IRelatedTableRow> | null> {
        params = params || (context === InventoryContexts.Lineage ? { size: 2000, page: 0 } : { size: 5, page: 0 });
        return this.inventoryService.getRelatedInventory(recordId, inventoryType, params).then((response: IProtocolResponse<IPaginatedResponse<ILinkedInventoryRow | IRelatedTableRow>>): IPaginatedResponse<ILinkedInventoryRow | IRelatedTableRow> | null => {
            if (!response.result) return null;
            if (context === InventoryContexts.Related) {
                this.store.dispatch({ type: InventoryActions.SET_RELATED_INVENTORY, relatedInventoryData: this.formatRelatedInventory(response.data, inventoryType, context), inventoryType });
            } else if (context === InventoryContexts.Lineage) {
                this.inventoryLineageService.setRelatedInventory(inventoryType, this.formatRelatedInventory(response.data, inventoryType, context));
                if (inventoryType === InventoryTableIds.Assets) {
                    const recordIds: string[] = uniq(map(response.data.data, (record: ILinkedInventoryRow) => {
                        return record.inventoryId;
                    }));
                    if (recordIds.length && this.permissions.canShow(InventoryPermissions[inventoryType].inventoryLinkingV2)) {
                        this.inventoryLineageService.updateState({ fetchingAssetDetails: true });
                        this.inventoryService.getDetailsByIds(NewInventorySchemaIds[inventoryType], recordIds).then((res: IProtocolResponse<{data: IInventoryRecordV2[]}>) => {
                            if (res && res.data && res.data.data) {
                                this.inventoryLineageService.setAssetDetails(res.data.data);
                            } else {
                                this.inventoryLineageService.updateState({ fetchingAssetDetails: false });
                            }
                        });
                    }
                }
            }
            return response.data;
        });
    }

    getRelatedLists(recordId: string, inventoryType: number, context: number): Promise<Array<IPaginatedResponse<ILinkedInventoryRow | IRelatedTableRow> | null>> {
        const promises: Array<ng.IPromise<IPaginatedResponse<ILinkedInventoryRow | IRelatedTableRow>>> = [];
        forEach(RelatedInventories[inventoryType], (type: number): void => {
            promises.push(this.getRelatedInventory(recordId, type, context));
        });
        return Promise.all(promises);
    }

    getRelatedInventoryList(inventoryType: number, orgGroupId: string): ng.IPromise<IRelatedListOption[] | null> {
        return this.inventoryService.getSearchedInventoryList(inventoryType, orgGroupId).then((response: IProtocolResponse<IRelatedListOption[]>): IRelatedListOption[] | null => {
            if (!response.result) return null;
            return response.data;
        });
    }

    getDataSubjectList(orgGroupId: string): ng.IPromise<IInventoryCellValue[] | null> {
        return this.inventoryService.getDataSubjects(orgGroupId).then((response: IProtocolResponse<IInventoryCellValue[]>): IInventoryCellValue[] | null => {
            if (!response.result) return null;
            return response.data;
        });
    }

    getDataCategoryList(subjectId: string, orgGroupId: string): ng.IPromise<IDataCategory[] | null> {
        return this.inventoryService.getDataCategories(subjectId, orgGroupId).then((response: IProtocolResponse<IDataCategory[]>): IDataCategory[] | null => {
            if (!response.result) return null;
            return response.data;
        });
    }

    formatRelatedInventory(res: IPaginatedResponse<ILinkedInventoryRow | IRelatedTableRow>, inventoryType: number, context: number): IRelatedTable {
        const formattedData: IRelatedTable = {
            name: this.translatePipe.transform(InventoryTranslationKeys[inventoryType].relatedInventoryHeader),
            id: inventoryType,
            numberOfElements: res.meta.page.numberOfElements,
            totalElements: res.meta.page.totalElements,
            size: res.meta.page.size,
            totalPages: res.meta.page.totalPages,
            first: res.meta.page.first,
            last: res.meta.page.last,
            number: res.meta.page.number,
            columns: [],
            rows: (res.data as IRelatedTableRow[]) || [],
        };
        switch (inventoryType) {
            case this.tableIds.Assets:
                formattedData.columns = [
                    { label: this.translatePipe.transform("Name"), dataKey: "name", type: this.cellTypes.link },
                    { label: this.translatePipe.transform("Organization"), dataKey: "organization", type: this.cellTypes.text },
                    { label: this.translatePipe.transform("Location"), rowTranslationKey: "location", type: this.cellTypes.text },
                    { label: this.translatePipe.transform("Relation"), dataKey: "relation", type: this.cellTypes.text },
                ];
                break;
            case this.tableIds.Processes:
                formattedData.columns = [
                    { label: this.translatePipe.transform("Name"), dataKey: "name", type: this.cellTypes.link },
                    { label: this.translatePipe.transform("Organization"), dataKey: "organization", type: this.cellTypes.text },
                    { label: this.translatePipe.transform("Relation"), dataKey: "relation", type: this.cellTypes.text },
                ];
                break;
            case this.tableIds.Elements:
                formattedData.columns = [
                    { label: this.translatePipe.transform("CategoryOfDataSubject"), dataKey: "subject", rowTranslationKey: "subjectKey", type: this.cellTypes.text },
                    { label: this.translatePipe.transform("DataElementsCategory"), dataKey: "category", rowTranslationKey: "categoryKey", type: this.cellTypes.text },
                    { label: this.translatePipe.transform("DataElement"), dataKey: "name", type: this.cellTypes.text },
                    { label: this.translatePipe.transform("Classification"), dataKey: "classification", rowTranslationKey: "classificationKey", type: this.cellTypes.text },
                    { label: this.translatePipe.transform("Organization"), dataKey: "organization", type: this.cellTypes.text },
                ];
                break;
        }
        return formattedData;
    }

    addInventoryRelationship(modalData: IRelateInventoryModalResolve): void {
        this.modalService.setModalData(modalData);
        this.modalService.openModal("relateInventoryModal");
    }

    addDataSubjectRelationship(modalData: IRelateInventoryModalResolve): void {
        this.modalService.setModalData(modalData);
        this.modalService.openModal("relateDataSubjectModal");
    }
}
