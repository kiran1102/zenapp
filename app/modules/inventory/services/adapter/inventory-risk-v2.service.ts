// 3rd Party
import { Injectable } from "@angular/core";

import {
    take,
    filter,
} from "rxjs/operators";

// Services
import { InventoryService } from "inventorySharedModule/shared/services/inventory-api.service";
import { RiskActionService } from "oneServices/actions/risk-action.service";
import { Permissions } from "modules/shared/services/helper/permissions.service";
import { RecordActionService } from "inventoryModule/services/action/record-action.service";
import {
    OtModalService,
    ConfirmModalType,
} from "@onetrust/vitreus";

// Interfaces
import { IProtocolResponse } from "interfaces/protocol.interface";
import {
    IInventoryRecordResponseV2,
    IInventoryRecordV2,
    IInventoryRiskDetail,
} from "interfaces/inventory.interface";
import { IRiskDetails, IRiskModalData } from "interfaces/risk.interface";

// Enums
import {
    RiskTypes,
    RiskSourceTypes,
} from "enums/risk.enum";

// Const
import { NewInventorySchemaIds } from "constants/inventory-config.constant";
import { AttributeFieldNames } from "constants/inventory.constant";

// Modal
import { RiskModalComponent } from "modules/risks/shared/risk-modal/risk-modal.component";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";
import { IGridColumn } from "interfaces/grid.interface";

const INVENTORY_RISK_GRID_SELECTED_COLUMNS = "inventoryRiskGridSelectedColumns";

@Injectable()
export class InventoryRiskV2Service {

    constructor(
        private inventoryService: InventoryService,
        private riskActionService: RiskActionService,
        private permissions: Permissions,
        private recordActionService: RecordActionService,
        private otModalService: OtModalService,
        private translatePipe: TranslatePipe,
    ) { }

    getUpdatedRiskScore(record: IInventoryRecordV2, inventoryId: number): ng.IPromise<IInventoryRiskDetail> {
        const currentRiskDetail: IInventoryRiskDetail = this.getRecordRisk(record);
        return this.pollForRiskChanges(record.id, inventoryId, currentRiskDetail).then((newRiskDetail: IInventoryRiskDetail): IInventoryRiskDetail => {
            return {
                record: this.setRecordRisk(record, newRiskDetail),
                riskLevel: newRiskDetail.riskLevel,
                riskScore: newRiskDetail.riskScore,
            };
        });
    }

    launchAddRiskModal(record: IInventoryRecordV2, inventoryName: string, inventoryType: number, inventoryOrgId: string, callback: (riskModel: IRiskDetails) => void) {
        if (this.permissions.canShow("RiskInheritance")) {
            this.otModalService
                .create(
                    RiskModalComponent,
                    {
                        isComponent: true,
                    },
                    {
                        riskModel: this.riskActionService.getBlankRiskModel(record.id, RiskTypes.Inventory, RiskSourceTypes.Inventory, inventoryName, inventoryType, inventoryOrgId),
                    },
                ).subscribe((risk: IRiskDetails) => {
                    if (risk) {
                        callback(risk);
                    }
                });
        } else {
            const modalData: IRiskModalData = {
                viewService: "InventoryRiskModalViewLogic",
                userList: [],
                saveRisk: this.saveRisk,
                callback,
                inventoryName,
                inventoryOrgId,
            };
            modalData.selectedModelId = this.riskActionService.addBlankRiskModelV2(record.id, RiskTypes.Inventory, RiskSourceTypes.Inventory, modalData.inventoryName, inventoryType, inventoryOrgId);
            this.riskActionService.openCreateNewRiskModalV2(modalData);
        }
    }

    handleRiskChange(record: IInventoryRecordV2, inventoryId: number, risk: IRiskDetails, updateActiveRecord: boolean = false) {
        if (risk) this.riskActionService.updateRisk(risk.id, risk);
        this.getUpdatedRiskScore(record, inventoryId).then((riskResponse: IInventoryRiskDetail) => {
            if (updateActiveRecord) {
                this.recordActionService.setCurrentRecordRiskLevel(riskResponse.riskLevel);
                this.recordActionService.setCurrentRecordRiskScore(riskResponse.riskScore);
            }
        });
    }

    changeInventoryRiskLevel(record: IInventoryRecordV2, risk: IRiskDetails, levelId: number, inventoryId: number): void {
        this.riskActionService.updateRiskV2(risk, { levelId });
        this.handleRiskChange(record, inventoryId, { ...risk, levelId }, true);
    }

    deleteInventoryRisk(record: IInventoryRecordV2, risk: IRiskDetails, inventoryId: number): void {
        this.otModalService
            .confirm({
                type: ConfirmModalType.DELETE,
                translations: {
                    title: "",
                    desc: "",
                    confirm: this.translatePipe.transform("SureToDeleteThisRisk"),
                    submit: this.translatePipe.transform("DeleteRisk"),
                    cancel: this.translatePipe.transform("Cancel"),
                },
                hideClose: false,
            }).pipe(
                take(1),
                filter((confirmed: boolean) => confirmed))
            .subscribe(() => {
                this.handleRiskChange(record, inventoryId, risk, true);
                return this.riskActionService.deleteRiskV2(risk.id);
            });
    }

    setSelectedColumns(columns: IGridColumn[]) {
        localStorage.setItem(INVENTORY_RISK_GRID_SELECTED_COLUMNS, JSON.stringify(columns ? columns.map((column) => column.name) : []));
    }

    getActiveInactiveColumnsMap(defaultActiveColumns: IGridColumn[], defaultInactiveColumns: IGridColumn[]) {
        const selectedColumns: string[] = JSON.parse(localStorage.getItem(INVENTORY_RISK_GRID_SELECTED_COLUMNS));
        if (selectedColumns && selectedColumns.length) {
            const activeColumns = [];
            const allColumns = [...defaultActiveColumns, ...defaultInactiveColumns];
            selectedColumns.forEach((name: string) => {
                const matchedIndex = allColumns.findIndex((column: IGridColumn) => column.name === name);
                if (matchedIndex !== -1) {
                    activeColumns.push(allColumns[matchedIndex]);
                    allColumns.splice(matchedIndex, 1);
                }
            });
            const inactiveColumns: IGridColumn[] = [];
            allColumns.forEach((column: IGridColumn) => {
                if (column.isMandatory) {
                    activeColumns.push(column);
                } else {
                    inactiveColumns.push(column);
                }
            });
            return { activeColumns, inactiveColumns };
        }
        return { activeColumns: defaultActiveColumns, inactiveColumns: defaultInactiveColumns};
    }

    private pollForRiskChanges(recordId: string, inventoryId: number, currentRiskDetail: IInventoryRiskDetail, count: number = 0): ng.IPromise<IInventoryRiskDetail> {
        return this.inventoryService.getRecordV2(NewInventorySchemaIds[inventoryId], recordId)
            .then((response: IProtocolResponse<IInventoryRecordResponseV2>): any => {
                if (!response.result) return currentRiskDetail;
                const responseRiskDetail: IInventoryRiskDetail = {
                    riskLevel: response.data.data[AttributeFieldNames.Risk] as string,
                    riskScore: response.data.data[AttributeFieldNames.RiskScore] as string,
                };
                if (responseRiskDetail.riskLevel !== currentRiskDetail.riskLevel || responseRiskDetail.riskScore !== responseRiskDetail.riskScore) {
                    return responseRiskDetail;
                }
                if (count === 2) return currentRiskDetail;
                count++;
                return new Promise((resolve): void => {
                    setTimeout((): void => {
                        resolve(this.pollForRiskChanges(recordId, inventoryId, currentRiskDetail, count));
                    }, 2000);
                });
            });
    }

    private saveRisk = (tempId: string, risk: IRiskDetails): ng.IPromise<IRiskDetails | boolean> => {
        return this.riskActionService.saveRiskV2(risk).then((response: IRiskDetails | boolean): IRiskDetails | boolean => {
            this.riskActionService.deleteTempRisk(tempId);
            return response;
        });
    }

    private setRecordRisk(record: IInventoryRecordV2, riskDetail: IInventoryRiskDetail): IInventoryRecordV2 {
        record[AttributeFieldNames.Risk] = riskDetail.riskLevel;
        record[AttributeFieldNames.RiskScore] = riskDetail.riskScore;
        return record;
    }

    private getRecordRisk(record: IInventoryRecordV2): IInventoryRiskDetail {
        return {
            riskLevel: record[AttributeFieldNames.Risk] as string,
            riskScore: record[AttributeFieldNames.RiskScore] as string,
        };
    }
}
