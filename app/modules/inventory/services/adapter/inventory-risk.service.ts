// 3rd Party
import { Injectable, Inject } from "@angular/core";
import { map, toString, find, findIndex, cloneDeep, isString } from "lodash";

// Redux
import {
    getInventoryId,
    getAttributeList,
} from "oneRedux/reducers/inventory.reducer";

// Services
import { RiskAPIService } from "oneServices/api/risk-api.service";
import { InventoryService } from "inventorySharedModule/shared/services/inventory-api.service";
import { Permissions } from "modules/shared/services/helper/permissions.service";

// ENUMS + Constants
import { RiskLevels, RiskTypes } from "enums/risk.enum";
import { RiskAttributes } from "constants/inventory.constant";
import { RiskV2Levels, RiskV2States } from "enums/riskV2.enum";

// Interfaces
import { IProtocolResponse } from "interfaces/protocol.interface";
import { IRiskDetails } from "interfaces/risk.interface";
import {
    IInventoryCell,
    IAttribute,
    IRecordResponse,
    IInventoryRecord,
    IAttributeValue,
} from "interfaces/inventory.interface";
import { IStore } from "interfaces/redux.interface";
import { IFilterTree, IFilterColumnOption, IFilterValueOption, IFilter } from "interfaces/filter.interface";
import { IStringMap } from "interfaces/generic.interface";

// Tokens
import { StoreToken } from "tokens/redux-store.token";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

@Injectable()
export class InventoryRiskService {

    private riskTranslationMap: IStringMap<string> = {};

    constructor(
        @Inject(StoreToken) private store: IStore,
        private riskAPILegacy: RiskAPIService,
        private inventoryService: InventoryService,
        private permissionsLegacyService: Permissions,
        private translatePipe: TranslatePipe,
    ) { }

    public getRiskCell(cell: IInventoryCell): IInventoryCell {
        cell.displayValue = cell && cell.values && cell.values[0] && cell.values[0].value !== "0" ? this.translatePipe.transform(RiskLevels[cell.values[0].value]) : "";
        return cell;
    }

    public getRecordRisk(record: IInventoryRecord): number {
        const inventoryId: number = getInventoryId(this.store.getState());
        const riskCell: IInventoryCell = find(record.cells, (cell: IInventoryCell): boolean => cell.attributeCode === RiskAttributes[inventoryId]);
        return riskCell && riskCell.values && riskCell.values[0] && isString(riskCell.values[0].value) ? parseInt(riskCell.values[0].value, 10) : 0;
    }

    public setRecordRisk(record: IInventoryRecord, riskLevel: number): IInventoryRecord {
        const inventoryId: number = getInventoryId(this.store.getState());
        const cellIndex: number = findIndex(record.cells, (cell: IInventoryCell): boolean => cell.attributeCode === RiskAttributes[inventoryId]);
        record.cells[cellIndex] = this.createRiskCellFromLevel(riskLevel);
        return record;
    }

    public getRisksByRecordId(recordId: string): ng.IPromise<IRiskDetails[] | undefined> {
        return this.riskAPILegacy.getRisksByReference(recordId, RiskTypes[RiskTypes.Inventory])
            .then((response: IProtocolResponse<IRiskDetails[]>): IRiskDetails[] | undefined => {
                if (!response.result) return;
                return response.data;
            });
    }

    public getUpdatedRiskScore(record: IInventoryRecord): ng.IPromise<{ updatedRecord: IInventoryRecord, updateRiskLevel: number}> {
        const currentRiskLevel: number = this.getRecordRisk(record);
        return this.pollForRiskChanges(record.id, currentRiskLevel).then((newRiskLevel: number): { updatedRecord: IInventoryRecord, updateRiskLevel: number } => {
            return { updatedRecord: this.setRecordRisk(record, newRiskLevel), updateRiskLevel: newRiskLevel };
        });
    }

    public formatRiskColumns(filters: IFilterColumnOption[]): IFilterColumnOption[] {
        const riskAttribute: IAttribute = this.getRiskAttribute();
        if (riskAttribute) {
            const riskFilterIndex: number = findIndex(filters, (filter: IFilterColumnOption): boolean => filter.id === riskAttribute.id);
            if (filters[riskFilterIndex] && filters[riskFilterIndex].valueOptions && filters[riskFilterIndex].valueOptions.length) {
                filters[riskFilterIndex].valueOptions = map(filters[riskFilterIndex].valueOptions, (option: IFilterValueOption): IFilterValueOption => {
                    this.riskTranslationMap[this.translatePipe.transform(RiskLevels[option.value])] = option.value as string;
                    return {
                        value: this.translatePipe.transform(RiskLevels[option.value]),
                        key: option.key,
                    };
                });
            }
        }
        return filters;
    }

    public formatRiskFilterForSave(filters: IFilterTree): IFilterTree {
        return this.formatRiskFilter(filters);
    }

    public formatRiskFilterForDisplay(filters: IFilterTree): IFilterTree {
        return this.formatRiskFilter(filters, true);
    }

    private pollForRiskChanges(recordId: string, currentRiskLevel: number, count: number = 0): ng.IPromise<number> {
        const inventoryId: number = getInventoryId(this.store.getState());
        return this.inventoryService.getRecord(inventoryId.toString(), recordId)
            .then((response: IProtocolResponse<IRecordResponse>): number | Promise<number> => {
                if (!response.result) return currentRiskLevel;
                const responseRisk: number = this.getRiskFromRecordResponse(response.data);
                if (responseRisk !== currentRiskLevel) return responseRisk;
                if (count === 2) return currentRiskLevel;
                count++;
                return new Promise((resolve: (params) => void): void => {
                    setTimeout((): void => {
                        resolve(this.pollForRiskChanges(recordId, currentRiskLevel, count));
                    }, 2000);
                });
            });
    }

    private getRiskFromRecordResponse(response: IRecordResponse): number {
        const riskAttribute: IAttribute = this.getRiskAttribute();
        const riskAttributeId: string = riskAttribute ? riskAttribute.id : "";
        if (
            response &&
            response.map &&
            response.map[riskAttributeId] &&
            response.map[riskAttributeId][0] &&
            response.map[riskAttributeId][0].value &&
            isString(response.map[riskAttributeId][0].value)
        ) {
            return parseInt(response.map[riskAttributeId][0].value, 10);
        }
        return 0;
    }

    private createRiskCellFromLevel(riskLevel: number): IInventoryCell {
        const riskLevelString: string = toString(riskLevel);
        const inventoryId: number = getInventoryId(this.store.getState());
        const riskAttribute: IAttribute = this.getRiskAttribute();
        const riskSelection: IAttributeValue = riskAttribute ? find(riskAttribute.values, (option: IAttributeValue): boolean => option.name === riskLevelString) : null;
        return {
            attributeCode: RiskAttributes[inventoryId],
            attributeId: riskAttribute ? riskAttribute.id : "",
            displayValue: riskLevel ? this.translatePipe.transform(RiskLevels[riskLevel]) : "",
            values: riskSelection ? [{ value: riskLevelString, valueId: riskSelection.id }] : [{ value: null, valueId: null }],
        };
    }

    private getRiskAttribute(): IAttribute {
        const inventoryId: number = getInventoryId(this.store.getState());
        const attributes: IAttribute[] = getAttributeList(this.store.getState());
        return find(attributes, (attribute: IAttribute): boolean => attribute.fieldAttributeCode === RiskAttributes[inventoryId]);
    }

    private mapOldRiskFieldsToNew(riskModel: IRiskDetails): IRiskDetails {
        riskModel.level = Number(RiskV2Levels[riskModel.level]);
        riskModel.probabilityLevel = RiskV2Levels[riskModel.probabilityLevel];
        riskModel.impactLevel = RiskV2Levels[riskModel.impactLevel];
        return riskModel;
    }

    private formatRiskFilter(filters: IFilterTree, forDisplay?: boolean): IFilterTree {
        if (!filters) return;
        const filterList: IFilter[] = cloneDeep(filters.AND);
        const riskAttribute: IAttribute = this.getRiskAttribute();
        if (riskAttribute) {
            const riskFilter: IFilter = find(filterList, (filter: IFilterTree): boolean => filter.columnId === riskAttribute.id);
            if (!riskFilter || !riskFilter.values || !riskFilter.values.length) return { AND: filterList };
            riskFilter.values = map(riskFilter.values, (value: string): string => {
                return forDisplay ? this.translatePipe.transform(RiskLevels[value]) : this.riskTranslationMap[value];
            });
        }
        return { AND: filterList };
    }

}
