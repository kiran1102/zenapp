// 3rd party
import { Injectable, Inject } from "@angular/core";
import {
    forEach,
    find,
    map,
    forIn,
    isString,
    isNumber,
    isArray,
    isDate,
    filter,
} from "lodash";

// Tokens
import { StoreToken } from "tokens/redux-store.token";

// Services
import { InventoryService } from "inventorySharedModule/shared/services/inventory-api.service";
import { getCommaSeparatedList } from "sharedServices/string-helper.service";
import { InventoryUserLogicService } from "inventoryModule/services/business-logic/inventory-user-logic.service";
import { TimeStamp } from "oneServices/time-stamp.service";

// Constants + Enums
import {
    AttributeTypes,
    AttributeResponseTypes,
    AttributeFieldNames,
    RequiredFieldNames,
    InventoryStatusTranslationKeys,
    DefaultRespondentFieldNames,
    InventoryMandatoryColumnNames,
} from "constants/inventory.constant";
import {
    LegacyInventorySchemaIds,
    InventorySchemaIds,
} from "constants/inventory-config.constant";
import { RiskTranslations } from "enums/risk.enum";
import { TableColumnTypes } from "enums/data-table.enum";
import {
    InventoryTableIds,
    InventoryStatusOptions,
} from "enums/inventory.enum";

// Interfaces
import {
    IAttributeDetailV2,
    IAttributeOptionV2,
    IInventoryRecordV2,
    IFormattedAttribute,
    IFormattedAttributeOption,
    IInventoryListV2Column,
    IRecordAssignment,
    IInventoryListOption,
    IInventoryElementListItem,
    IInventoryStatusOption,
    IInventoryListItem,
} from "interfaces/inventory.interface";
import {
    IStringMap,
    ILabelValue,
} from "interfaces/generic.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";
import {
    IFilterColumnOption,
} from "interfaces/filter.interface";
import { IStore } from "interfaces/redux.interface";
import { IListItem } from "@onetrust/vitreus";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

@Injectable()
export class InventoryAdapterV2Service {

    constructor(
        @Inject(StoreToken) private store: IStore,
        private inventoryService: InventoryService,
        private inventoryUserLogicService: InventoryUserLogicService,
        private translatePipe: TranslatePipe,
        private timeStamp: TimeStamp,
    ) {}

    getSelectionDisplayText(attribute: IAttributeDetailV2, selection: string | Date | IAttributeOptionV2 | IAttributeOptionV2[] | boolean | IInventoryStatusOption): string {
        if (attribute.required && attribute.responseType !== AttributeResponseTypes.Text) return this.getMatchingOptionDisplayText(attribute, selection as IAttributeOptionV2);
        if (attribute.fieldName === AttributeFieldNames.Status) return this.translatePipe.transform(InventoryStatusTranslationKeys[(selection as IAttributeOptionV2).value]);
        if (attribute.attributeType === AttributeTypes.Orgs) return (selection as IAttributeOptionV2).value;
        if (attribute.attributeType === AttributeTypes.Users) return this.inventoryUserLogicService.getUserDisplayText(selection as IAttributeOptionV2[]);
        if (attribute.fieldName === AttributeFieldNames.Risk) return this.translatePipe.transform(RiskTranslations[selection as string]);
        if (attribute.fieldName === AttributeFieldNames.RiskScore) return selection as string;
        if (attribute.responseType === AttributeResponseTypes.Text) return selection as string;
        if (attribute.responseType === AttributeResponseTypes.MultiSelect || attribute.responseType === AttributeResponseTypes.SingleSelect) return getCommaSeparatedList(selection as IAttributeOptionV2[], (item: IAttributeOptionV2): string => {
            return this.getMatchingOptionDisplayText(attribute, item);
        });
        if (attribute.responseType === AttributeResponseTypes.Date) return this.timeStamp.formatDateWithoutTimezone(selection as string);
        return "";
    }

    updateRecord(schemaId: string, recordId: string, fieldName: string, value: string | Date | IAttributeOptionV2 | IAttributeOptionV2[] | IInventoryStatusOption): void {
        this.inventoryService.updateRecordV2(schemaId, recordId, { [fieldName]: value });
    }

    getRecordDisplayValues(record: IInventoryRecordV2, schema: IAttributeDetailV2[]): IStringMap<string> {
        const display = {};
        forEach(schema, (attribute: IAttributeDetailV2): void => {
            display[attribute.fieldName] = this.getFieldDisplayValue(record[attribute.fieldName], attribute);
        });
        return display;
    }

    getFieldDisplayValue(value: string | Date | IAttributeOptionV2 | IAttributeOptionV2[] | boolean | IInventoryStatusOption, attribute: IAttributeDetailV2): string {
        return value ? this.getSelectionDisplayText(attribute, value) : "";
    }

    getRecordAssignments(records: IInventoryRecordV2[], inventoryId: number): IRecordAssignment[] {
        return map(records, (record: IInventoryRecordV2): IRecordAssignment => {
            return {
                InventoryId: record.id,
                Name: record[AttributeFieldNames.Name] ? record[AttributeFieldNames.Name] as string : "",
                OrgGroupId: record[AttributeFieldNames.Org] ? (record[AttributeFieldNames.Org] as IAttributeOptionV2).id : "",
                defaultRespondent: this.getDefaultRespondent(record, inventoryId),
                inventoryNumber: record.number,
            };
        });
    }

    getDefaultRespondent(record: IInventoryRecordV2, inventoryId: number): string {
        const respondent = record[DefaultRespondentFieldNames[inventoryId]] as Array<{id: string}>;
        return respondent ? respondent[0].id : "";
    }

    formatOptionsForDisplay(attributes: IAttributeDetailV2[]): IFormattedAttribute[] {
        return map(attributes, (attribute: IAttributeDetailV2): IFormattedAttribute => {
            const translatedValues: IFormattedAttributeOption[] = map(attribute.values, (option: IAttributeOptionV2): IFormattedAttributeOption => {
                (option as IFormattedAttributeOption).label = option.valueKey ? this.translatePipe.transform(option.valueKey) : option.value;
                return option;
            });
            return {
                ...attribute,
                values: translatedValues,
                filteredValues: translatedValues,
            };
        });
    }

    filterOptionsForDisplay(schema: IFormattedAttribute[], record: IInventoryRecordV2): IFormattedAttribute[] {
        schema.forEach((attribute) => {
            if (attribute.fieldName === AttributeFieldNames.Location || attribute.fieldName === AttributeFieldNames.Org) {
                const selectedAttributeValue = record[attribute.fieldName] as IFormattedAttributeOption;
                attribute.filteredValues = this.removeSelectedItems(attribute.filteredValues, selectedAttributeValue ? [selectedAttributeValue] : []);
            } else if (attribute.responseType === AttributeResponseTypes.MultiSelect || attribute.responseType === AttributeResponseTypes.SingleSelect) {
                const selectedAttributeValues = (record[attribute.fieldName] as IFormattedAttributeOption[]);
                attribute.filteredValues = this.removeSelectedItems(attribute.filteredValues, selectedAttributeValues || []);
            }
        });
        return schema;
    }

    getSingleSelectModel(attribute: IFormattedAttribute, record: IInventoryRecordV2): IFormattedAttributeOption {
        if (!record[attribute.fieldName]) return null;
        if ((record[attribute.fieldName] as IInventoryStatusOption).key) return this.getSelectionModel({ value: (record[attribute.fieldName] as IInventoryStatusOption).key }, attribute);
        if (attribute.fieldName === AttributeFieldNames.Status && (record[attribute.fieldName] as IFormattedAttributeOption).value) return record[attribute.fieldName] as IFormattedAttributeOption;
        if (!attribute.required && !(record[attribute.fieldName] as IAttributeOptionV2[]).length) return null;
        const selection: IAttributeOptionV2 = attribute.required ? record[attribute.fieldName] : record[attribute.fieldName][0];
        return this.getSelectionModel(selection, attribute);
    }

    getMultiSelectModel(attribute: IFormattedAttribute, record: IInventoryRecordV2): IFormattedAttributeOption[] {
        if (!record[attribute.fieldName] || !(record[attribute.fieldName] as IAttributeOptionV2[]).length) return [];
        const selections: IAttributeOptionV2[] = record[attribute.fieldName] as IAttributeOptionV2[];
        return map(selections, (selection: IAttributeOptionV2): IFormattedAttributeOption => {
            return this.getSelectionModel(selection, attribute);
        });
    }

    serializeRecord(record: IInventoryRecordV2, schema: IFormattedAttribute[]): IInventoryRecordV2 {
        forEach(schema, (attribute: IFormattedAttribute) => {
            const selection: string | Date | IAttributeOptionV2 | IAttributeOptionV2[] | boolean | IInventoryStatusOption = record[attribute.fieldName];
            if (!selection || isString(selection) || isDate(selection) || isNumber(selection)) return;
            if (isArray(selection)) {
                record[attribute.fieldName] = this.getMultiSelectModel(attribute, record);
                return;
            }
            record[attribute.fieldName] = this.getSingleSelectModel(attribute, record);
        });
        return record;
    }

    deserializeRecord(updates: IStringMap<string | Date | IFormattedAttributeOption | IFormattedAttributeOption[] | boolean | IInventoryStatusOption>): IInventoryRecordV2 {
        const deserializedUpdates: IInventoryRecordV2 = { ...updates };
        forIn(deserializedUpdates, (update: string | Date | IFormattedAttributeOption | IFormattedAttributeOption[], key: string) => {
            if (!update || isString(update) || isDate(update)) {
                deserializedUpdates[key] = update;
                return;
            }
            if (isArray(update)) {
                deserializedUpdates[key] = map([...update], (item: IFormattedAttributeOption): IAttributeOptionV2 => {
                    return this.removeExtraSelectionProps(item);
                });
                return;
            }
            deserializedUpdates[key] = this.removeExtraSelectionProps(update);
        });
        return deserializedUpdates;
    }

    deserializeAndSaveChanges(inventoryId: string, recordId: string, updates: IInventoryRecordV2): ng.IPromise<boolean> {
        const deserializedUpdates: IInventoryRecordV2 = this.deserializeRecord(updates);
        return this.inventoryService.updateRecordV2(inventoryId, recordId, deserializedUpdates).then((response: IProtocolResponse<IInventoryRecordV2>): boolean => {
            if (!response.result) return false;
            return true;
        });
    }

    getFormattedRecordAndSchema(record: IInventoryRecordV2, schema: IAttributeDetailV2[]): [IInventoryRecordV2, IFormattedAttribute[]] {
        schema = this.formatOptionsForDisplay(schema);
        schema = this.filterOptionsForDisplay(schema, record);
        record = this.serializeRecord(record, schema);
        return [record, schema];
    }

    removeSelectedItems(options: IFormattedAttributeOption[], selections: IFormattedAttributeOption[]): IFormattedAttributeOption[] {
        return filter(options, (option: IFormattedAttributeOption): boolean => {
            return !find(selections, (selection: IFormattedAttributeOption): boolean => {
                return option.id === selection.id;
            });
        });
    }

    getRecordNameForAssessmentLink(record: IInventoryRecordV2, inventoryId: number): string {
        const organization = (record[RequiredFieldNames[inventoryId].Org] as IAttributeOptionV2).value;
        let recordName = `${record[RequiredFieldNames[inventoryId].Name]} | ${organization}`;
        if (inventoryId === InventoryTableIds.Vendors) {
            const type = (record[RequiredFieldNames[inventoryId].Type] as IAttributeOptionV2).value;
            recordName = `${recordName} | ${type}`;
        }
        if (inventoryId === InventoryTableIds.Assets) {
            const location = (record[RequiredFieldNames[inventoryId].Location] as IAttributeOptionV2).value;
            recordName = `${recordName} | ${location}`;
        }
        return recordName;
    }

    setSelectedRecords(records: IInventoryRecordV2[], selected: boolean): IStringMap<boolean> {
        const selectedRecords = {};
        records.forEach((record) => {
            if (record.isEditable) {
                selectedRecords[record.id as string] = selected;
            }
        });
        return selectedRecords;
    }

    formatColumnData(columnData: IFilterColumnOption[]): IFilterColumnOption[] {
        return map(columnData, (column: IFilterColumnOption): IFilterColumnOption => {
            if (column.translationKey) {
                column.name = this.translatePipe.transform(column.translationKey);
            }
            return column;
        });
    }

    formatListItemOptions(options: IInventoryListItem[]): IInventoryListOption[] {
        options.map((option) => {
            this.formatListItemOption(option);
        });
        return options;
    }

    formatListItemOption(option: IInventoryListItem): IInventoryListOption {
        (option as IInventoryListOption).displayName = option.nameKey ? this.translatePipe.transform(option.nameKey) : option.name;
        return option;
    }

    formatDataElementsForList(listData: IInventoryElementListItem[], isAssociatedType: boolean): IInventoryElementListItem[] {
        listData.forEach((listItem) => {
            if (listItem.classifications) {
                listItem.classificationNames = "";
                listItem.categoryNames = "";
                listItem.classifications.forEach((classification, index, array) => {
                    if (classification.nameKey) {
                        listItem.classificationNames = `${listItem.classificationNames}${this.translatePipe.transform(classification.nameKey)}`;
                    } else {
                        listItem.classificationNames = `${listItem.classificationNames}${classification.name}`;
                    }
                    if (index !== array.length - 1) listItem.classificationNames = `${listItem.classificationNames}, `;
                });
                listItem.categories.forEach((category, index, array) => {
                    if (category.nameKey) {
                        listItem.categoryNames = `${listItem.categoryNames}${this.translatePipe.transform(category.nameKey)}`;
                    } else {
                        listItem.categoryNames = `${listItem.categoryNames}${category.name}`;
                    }
                    if (index !== array.length - 1) listItem.categoryNames = `${listItem.categoryNames}, `;
                });
            }
            if ((listItem.categories.length > 1 || listItem.classifications.length > 1) && !isAssociatedType) listItem.flagged = true;
        });
        return listData;
    }

    formatInventoryStatusOptions(): ng.IPromise<Array<ILabelValue<InventoryStatusOptions>>> {
        // TODO will change later when the users need to select a specific inventory
        const inventoryId: string = InventorySchemaIds.Assets;
        return this.inventoryService.getInventoryStatusOptions(inventoryId).then((response: IProtocolResponse<IInventoryStatusOption[]>) => {
            if (response.result) {
                return response.data.map((option: IInventoryStatusOption): ILabelValue<InventoryStatusOptions> => {
                    return {
                        label: this.translatePipe.transform(option.name),
                        value: option.key,
                    };
                });
            }
            return [];
        });
    }

    mapInventoryCellType(schemaId: string, schema: IAttributeDetailV2[]): IInventoryListV2Column[] {
        return schema.map((attribute: IInventoryListV2Column) => {
            if (attribute.fieldName === RequiredFieldNames[LegacyInventorySchemaIds[schemaId]].Name) {
                attribute.cellType = TableColumnTypes.Link;
            } else if (attribute.fieldName === AttributeFieldNames.Risk) {
                attribute.cellType = TableColumnTypes.Risk;
            } else if (attribute.fieldName === AttributeFieldNames.Status) {
                attribute.cellType = TableColumnTypes.InventoryStatus;
            } else {
                attribute.cellType = TableColumnTypes.Text;
            }
            return attribute;
        });
    }

    mapAttributeToListItem(attribute: IAttributeDetailV2, index: number, schemaId: string): IListItem {
        return {
            key: attribute.nameKey ? this.translatePipe.transform(attribute.nameKey) : attribute.name,
            attribute,
            index,
            isMandatory: InventoryMandatoryColumnNames[schemaId].includes(attribute.fieldName),
        };
    }

    private getMatchingOptionDisplayText(attribute: IAttributeDetailV2, selection: IAttributeOptionV2): string {
        const matchingOption = find(attribute.values, (option: IAttributeOptionV2): boolean => option.id === (selection as IAttributeOptionV2).id);
        if (matchingOption) {
            return matchingOption.valueKey ? this.translatePipe.transform(matchingOption.valueKey) : matchingOption.value;
        } else {
            return selection.value || "";
        }
    }

    private getSelectionModel(selection: IFormattedAttributeOption, attribute: IFormattedAttribute): IFormattedAttributeOption {
        if (!selection.id && !selection.value) return selection;
        if ((selection as IFormattedAttributeOption).label) return selection;
        if (attribute.fieldName === AttributeFieldNames.Status) return selection;
        const matchingOption = find(attribute.values, (option: IFormattedAttributeOption): boolean => option.id === selection.id);
        if (matchingOption) return matchingOption;
        if (!(selection as IFormattedAttributeOption).label) (selection as IFormattedAttributeOption).label = selection.value;
        return selection;
    }

    private removeExtraSelectionProps(selection: IFormattedAttributeOption): IAttributeOptionV2 {
        const newSelection = { ...selection };
        delete newSelection.label;
        delete newSelection.valueKey;
        delete newSelection.status;
        return (newSelection as IAttributeOptionV2);
    }

}
