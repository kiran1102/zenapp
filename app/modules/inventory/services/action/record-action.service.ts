// 3rd party
import { Injectable, Inject } from "@angular/core";
import { StateService } from "@uirouter/core";

// Redux
import {
    RecordActions,
    getSchema,
    getRecord,
    getRecordChanges,
    getIsEditingRecord,
} from "oneRedux/reducers/inventory-record.reducer";

// Services
import { InventoryService } from "inventorySharedModule/shared/services/inventory-api.service";
import { InventoryUserLogicService } from "inventoryModule/services/business-logic/inventory-user-logic.service";
import { InventoryAdapterV2Service } from "inventoryModule/services/adapter/inventory-adapter-v2.service";
import { LookupService } from "sharedModules/services/helper/lookup.service";

// Tokens
import { StoreToken } from "tokens/redux-store.token";

// Constants / Enums
import {
    AttributeResponseTypes,
    AttributeFieldNames,
    InventoryDetailRoutes,
    InventoryDetailsTabs,
} from "constants/inventory.constant";

// interfaces
import {
    IInventoryRecordV2,
    IAttributeDetailV2,
    IInventoryRecordResponseV2,
    IAttributeOptionV2,
} from "interfaces/inventory.interface";
import { IStore } from "interfaces/redux.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";

@Injectable()
export class RecordActionService {

    attributeFieldNames = AttributeFieldNames;
    attributeResponseTypes = AttributeResponseTypes;

    constructor(
        private stateService: StateService,
        @Inject(StoreToken) private store: IStore,
        private inventoryService: InventoryService,
        private inventoryUserLogicService: InventoryUserLogicService,
        private inventoryAdapterV2Service: InventoryAdapterV2Service,
        private lookup: LookupService,
    ) {}

    setRecordAndSchemaById(inventoryId: string, recordId: string, isEditing: boolean = false, isSecondary: boolean = false): Promise<boolean> {
        const startLoadingAction = isSecondary ? "START_LOADING_SECONDARY_INVENTORY_RECORD" : "START_LOADING_INVENTORY_RECORD";
        const endLoadingAction = isSecondary ? "SET_SECONDARY_INVENTORY_RECORD_AND_SCHEMA" : "SET_INVENTORY_RECORD_AND_SCHEMA";
        this.store.dispatch({ type: RecordActions[startLoadingAction] });
        return Promise.all([
            this.inventoryService.getRecordV2(inventoryId, recordId),
            this.inventoryService.getAllAttributes(inventoryId),
        ]).then((responses): Promise<boolean> | boolean => {
            if (!responses[0].result || !responses[1].result) {
                this.store.dispatch({ type: RecordActions[endLoadingAction], inventoryId });
                return false;
            }
            let record = responses[0].data.data;
            let schema = responses[1].data;
            return this.inventoryUserLogicService.setRecordUsers(record, schema).then(() => {
                const displayValues = this.inventoryAdapterV2Service.getRecordDisplayValues(record, schema);
                [record, schema] = this.inventoryAdapterV2Service.getFormattedRecordAndSchema(record, schema);
                this.store.dispatch({ type: RecordActions[endLoadingAction], record, schema, displayValues, isEditing, inventoryId });
                return true;
            });
        });
    }

    setRecordById(inventoryId: string, recordId: string, isEditing: boolean = false): ng.IPromise<boolean> {
        this.store.dispatch({ type: RecordActions.START_LOADING_INVENTORY_RECORD });
        return this.inventoryService.getRecordV2(inventoryId, recordId).then((response: IProtocolResponse<IInventoryRecordResponseV2>): Promise<boolean> | boolean => {
            if (!response.result) {
                this.store.dispatch({ type: RecordActions.SET_INVENTORY_RECORD_AND_SCHEMA, inventoryId });
                return false;
            }
            let record = response.data.data;
            let schema = getSchema(this.store.getState());
            [record, schema] = this.inventoryAdapterV2Service.getFormattedRecordAndSchema(record, schema);
            return this.inventoryUserLogicService.setRecordUsers(record, schema).then((): boolean => {
                const displayValues = this.inventoryAdapterV2Service.getRecordDisplayValues(record, schema);
                this.store.dispatch({ type: RecordActions.SET_INVENTORY_RECORD_AND_SCHEMA, record, displayValues, isEditing, inventoryId });
                return true;
            });
        });
    }

    setRecord(record: IInventoryRecordV2, isEditing: boolean = false) {
        let schema = getSchema(this.store.getState());
        [record, schema] = this.inventoryAdapterV2Service.getFormattedRecordAndSchema(record, schema);
        const displayValues = this.inventoryAdapterV2Service.getRecordDisplayValues(record as IInventoryRecordV2, schema);
        this.store.dispatch({ type: RecordActions.SET_INVENTORY_RECORD, record, displayValues, isEditing });
    }

    setSchema(schema: IAttributeDetailV2[], inventoryId: string) {
        this.store.dispatch({ type: RecordActions.SET_SCHEMA, schema, inventoryId });
    }

    changeRecord(attribute: IAttributeDetailV2, change: string | Date | IAttributeOptionV2 | IAttributeOptionV2[], hasError: boolean = false) {
        const displayValue = this.inventoryAdapterV2Service.getFieldDisplayValue(change, attribute);
        this.store.dispatch({
            type: RecordActions.SET_INVENTORY_RECORD_CHANGE,
            fieldName: attribute.fieldName,
            displayValue,
            change,
            hasError,
        });
    }

    setFinishLoadingState(inventoryId: string, isEditing: boolean) {
        this.store.dispatch({ type: RecordActions.FINISH_LOADING_INVENTORY_RECORD, inventoryId, isEditing });
    }

    setRecordError(fieldName: string, hasError: boolean) {
        this.store.dispatch({ type: RecordActions.SET_INVENTORY_RECORD_ERROR, fieldName, hasError });
    }

    setDefaultRecord = () => {
        this.store.dispatch({ type: RecordActions.SET_INVENTORY_RECORD, record: {}, displayValues: {}, isEditing: false });
    }

    setDefaultSecondaryRecordAndSchema() {
        this.store.dispatch({ type: RecordActions.SET_SECONDARY_INVENTORY_RECORD_AND_SCHEMA, record: {}, schema: [], displayValues: {}, isEditing: false });
    }

    clearRecordChanges() {
        const schema = getSchema(this.store.getState());
        const record = getRecord(this.store.getState());
        const displayValues = this.inventoryAdapterV2Service.getRecordDisplayValues(record, schema);
        this.store.dispatch({ type: RecordActions.CLEAR_INVENTORY_RECORD_CHANGES, displayValues });
    }

    toggleEditingRecord(isEditing: boolean) {
        let displayValues;
        if (!isEditing) {
            const schema = getSchema(this.store.getState());
            const record = getRecord(this.store.getState());
            schema.forEach((attribute) => {
                if (attribute.responseType === this.attributeResponseTypes.MultiSelect) {
                    attribute.filteredValues = this.lookup.filterOptionsBySelections(this.inventoryAdapterV2Service.getMultiSelectModel(attribute, record), attribute.values, "id");
                } else if (attribute.responseType === this.attributeResponseTypes.SingleSelect) {
                    attribute.filteredValues = this.lookup.filterOptionsBySelections([this.inventoryAdapterV2Service.getSingleSelectModel(attribute, record)], attribute.values, "id");
                }
            });
            displayValues = this.inventoryAdapterV2Service.getRecordDisplayValues(record, schema);
        }
        this.store.dispatch({ type: RecordActions.TOGGLE_INVENTORY_RECORD_EDITING, isEditing, displayValues });
    }

    saveRecordChanges(inventoryId: string, recordId: string, keepEditing: boolean = false): ng.IPromise<boolean> {
        this.store.dispatch({ type: RecordActions.START_LOADING_INVENTORY_RECORD });
        const updates: IInventoryRecordV2 = getRecordChanges(this.store.getState());
        return this.inventoryAdapterV2Service.deserializeAndSaveChanges(inventoryId, recordId, updates).then((response: boolean): boolean => {
            if (!response) {
                this.setFinishLoadingState(inventoryId, true);
                return false;
            }
            this.store.dispatch({ type: RecordActions.SAVE_INVENTORY_RECORD_CHANGES, isEditing: keepEditing });
            return true;
        });
    }

    viewRecordRisks(record: IInventoryRecordV2, inventoryId: number): void {
        this.stateService.go(InventoryDetailRoutes[inventoryId], {
            recordId: record.id,
            inventoryId,
            tabId: InventoryDetailsTabs.Risks,
        });
    }

    setCurrentRecordRiskLevel(riskLevel: string): void {
        const record = getRecord(this.store.getState());
        record.riskLevel = riskLevel;
        this.setRecord(record, getIsEditingRecord(this.store.getState()));
    }

    setCurrentRecordRiskScore(riskScore: string): void {
        const record = getRecord(this.store.getState());
        record.riskScore = riskScore;
        this.setRecord(record, getIsEditingRecord(this.store.getState()));
    }

    setCurrentRecordStatus = (status: string) => {
        const record = getRecord(this.store.getState());
        record.status = { value: status };
        this.setRecord(record, getIsEditingRecord(this.store.getState()));
    }

    addRecord(inventoryId: string, keepEditing: boolean = false): ng.IPromise<IProtocolResponse<{ data: IInventoryRecordV2 }>> {
        this.store.dispatch({ type: RecordActions.START_LOADING_INVENTORY_RECORD });
        const updates: IInventoryRecordV2 = getRecordChanges(this.store.getState());
        const deserializedRecord = this.inventoryAdapterV2Service.deserializeRecord(updates);
        return this.inventoryService.addRecord(inventoryId, deserializedRecord).then((response: IProtocolResponse<{ data: IInventoryRecordV2 }>): IProtocolResponse<{ data: IInventoryRecordV2 }> => {
            if (!response.result) {
                this.store.dispatch({ type: RecordActions.FINISH_LOADING_INVENTORY_RECORD, inventoryId, isEditing: keepEditing });
            } else {
                this.store.dispatch({ type: RecordActions.SAVE_INVENTORY_RECORD_CHANGES, isEditing: keepEditing });
            }
            return response;
        });
    }

}
