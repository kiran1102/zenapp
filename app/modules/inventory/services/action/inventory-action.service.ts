// Rxjs
import {
    Subscription,
    Observable,
} from "rxjs";
import { startWith, take, filter } from "rxjs/operators";

// Angular
import {
    Injectable,
    Inject,
} from "@angular/core";

// Vitreus
import { OtModalService, ConfirmModalType } from "@onetrust/vitreus";

// 3rd Party
import { toString } from "lodash";

// Interfaces
import { IStringMap } from "interfaces/generic.interface";
import {
    IStoreState,
    IStore,
} from "interfaces/redux.interface";
import {
    IProtocolPacket,
    IProtocolResponse,
} from "interfaces/protocol.interface";
import {
    IAttributeMap,
    IRecordMap,
} from "interfaces/inventory-reducer.interface";
import {
    IRecordResponse,
    IInventoryRecord,
    IAttribute,
    IInventoryAddModalResolve,
    IAssessmentStatusMap,
    ILaunchModalResolve,
    IInventoryAssessment,
    ILinkAssessmentModalData,
    ILaunchResponse,
    IRecordAssignment,
    IInventoryCopyModalResolve,
    IPageableInventory,
    IInventoryDetails,
    IFetchDetailsParams,
    ILinkPersonalData,
} from "interfaces/inventory.interface";
import {
    IFilterTree,
    IFilterColumnOption,
} from "interfaces/filter.interface";
import { IPageable } from "interfaces/pagination.interface";
import { IDeleteConfirmationModalResolve } from "interfaces/delete-confirmation-modal-resolve.interface";
import {
    IRiskDetails,
    IRiskModalData,
} from "interfaces/risk.interface";
import { IDMAssessment } from "interfaces/dm-assessment.interface";
import { IMapDataPoint } from "modules/data-mapping/interfaces/dm-map-data-point.interface";
import { ITaskConfirmationModalResolve } from "interfaces/task-confirmation-modal-resolve.interface";
import { IUserMap } from "interfaces/user-reducer.interface";
import { IReportExport } from "modules/reports/reports-shared/interfaces/report.interface";
import { IFilterColumn } from "interfaces/filter.interface";
import { IOrgUserAdapted } from "interfaces/org-user.interface";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

// Services
import { InventoryService } from "inventorySharedModule/shared/services/inventory-api.service";
import { InventoryViewLogic } from "oneServices/view-logic/inventory-view-logic.service";
import { ModalService } from "sharedServices/modal.service";
import { InventoryAdapter } from "oneServices/adapters/inventory-adapter.service";
import { DMAssessmentsService } from "datamappingservice/Assessment/dm-assessments.service";
import { RiskActionService } from "oneServices/actions/risk-action.service";
import { DrawerActionService } from "oneServices/actions/drawer-action.service";
import { InventoryRiskService } from "modules/inventory/services/adapter/inventory-risk.service";
import { InventoryAssessmentService } from "inventoryModule/services/adapter/inventory-assessment.service";
import { InventoryRelatedService } from "inventoryModule/services/adapter/inventory-related.service";
import { InventoryLinkingService } from "inventoryModule/services/adapter/inventory-linking.service";
import { AssessmentLinkingAPIService } from "modules/assessment/services/api/assessment-linking-api.service";
import { InventoryListActionService } from "inventoryModule/services/action/inventory-list-action.service";
import { ReportAPI } from "reportsModule/reports-shared/services/report-api.service";
import { ReportsAdapter } from "reportsModule/reports-shared/services/reports-adapter.service";
import { RecordActionService } from "inventoryModule/services/action/record-action.service";
import { InventoryLineageService } from "inventoryModule/inventory-lineage/shared/services/inventory-lineage.service";
import { Permissions } from "modules/shared/services/helper/permissions.service";
import { InventoryLaunchModalService } from "inventoryModule/services/action/inventory-launch-modal.service";
import { OrgGroupApiService } from "oneServices/api/org-group-api.service";

// Redux
import observableFromStore from "oneRedux/utils/observable-from-store";
import {
    InventoryActions,
    getInitialRecord,
    getAttributeMap,
    getStatusMap,
    getCurrentRecord,
    getAllRecordIds,
    getInventoryListPaginationMetadata,
    getAllRecordsMap,
    getSearchTerm,
    getInventoryId,
    getPageNumber,
    getFilterData,
    isShowingFilteredResults,
} from "oneRedux/reducers/inventory.reducer";
import {
    RiskActions,
    getRiskById,
} from "oneRedux/reducers/risk.reducer";
import { getCurrentOrgId } from "oneRedux/reducers/org.reducer";

// Constants and Enums
import {
    InventoryTranslationKeys,
} from "constants/inventory.constant";
import {
    RiskTypes,
    RiskSourceTypes,
} from "enums/risk.enum";
import {
    RiskV2Levels,
    RiskV2States,
} from "enums/riskV2.enum";
import {
    InventoryContexts,
    InventoryTableIds,
} from "enums/inventory.enum";
import { ModuleTypes } from "enums/module-types.enum";
import { RiskLevelStrings } from "constants/risk.constants";
import { PDFSectionTypes } from "reportsModule/reports-shared/enums/reports.enum";
import { OrgUserTraversal } from "enums/org-user-dropdown.enum";

// Tokens
import { StoreToken } from "tokens/redux-store.token";

// Modal
import { RiskModalComponent } from "modules/risks/shared/risk-modal/risk-modal.component";

@Injectable()
export class InventoryActionService {

    isFiltering: boolean;
    subscription: Subscription;
    paginationMetadata: IPageable;
    allRecordIds: string[];
    private currentSearchTerm: string;
    private inventoryId: number;
    private currentPageNumber: number;
    private unresolvedColumnsPromise: ng.IPromise<IProtocolPacket>;
    private showingFilteredResults: boolean;
    private filterData: IFilterTree;

    constructor(
        @Inject(StoreToken) private store: IStore,
        private translatePipe: TranslatePipe,
        private inventory: InventoryService,
        private modalService: ModalService,
        private inventoryViewLogic: InventoryViewLogic,
        private inventoryAdapter: InventoryAdapter,
        private dmAssessments: DMAssessmentsService,
        private riskAction: RiskActionService,
        private inventoryRisk: InventoryRiskService,
        private drawerAction: DrawerActionService,
        private inventoryAssessment: InventoryAssessmentService,
        private inventoryRelated: InventoryRelatedService,
        private inventoryLinking: InventoryLinkingService,
        private inventoryLineageService: InventoryLineageService,
        private reportAPI: ReportAPI,
        private reportsAdapter: ReportsAdapter,
        private permissions: Permissions,
        private assessmentLinkingAPI: AssessmentLinkingAPIService,
        private recordActionService: RecordActionService,
        private otModalService: OtModalService,
        private inventoryLaunchModalService: InventoryLaunchModalService,
        private inventoryListAction: InventoryListActionService,
        private orgGroupApiService: OrgGroupApiService,
    ) {
        this.subscription = observableFromStore(this.store).pipe(
            startWith(this.store.getState()),
        ).subscribe((state: IStoreState): void => this.componentWillReceiveState(state));
    }

    fetchInventoryList(pageNumber: number = this.currentPageNumber, pageSize?: number): ng.IPromise<IProtocolPacket> {
        this.isFetchingInventory();
        const currentId: number = this.inventoryId;
        return this.inventoryAdapter.getInventoryListView(toString(this.inventoryId), pageNumber, pageSize, this.currentSearchTerm, this.filterData).then((res: IProtocolPacket) => {
            if (res.result && this.inventoryId === currentId) {
                const allRecordIds: string[] = res.data.records.map((record: IInventoryRecord): string => record.id);
                const byRecordId: IRecordMap = res.data.records.reduce((prev: IRecordMap, curr: IInventoryRecord) => { prev[curr.id] = curr; return prev; }, {});
                const allAttributeIds: string[] = res.data.attributes.map((attribute: IAttribute): string => attribute.id);
                const byAttributeId: IAttributeMap = res.data.attributes.reduce((prev: IAttributeMap, curr: IAttribute) => { prev[curr.id] = curr; return prev; }, {});
                const paginationMetadata: IPageable = {
                    content: res.data.records,
                    first: res.data.first,
                    last: res.data.last,
                    number: res.data.number,
                    numberOfElements: res.data.numberOfElements,
                    size: res.data.size,
                    sort: res.data.sort,
                    totalElements: res.data.totalElements,
                    totalPages: res.data.totalPages,
                };
                const statusMap: IAssessmentStatusMap = res.data.statusMap;
                this.store.dispatch({ type: InventoryActions.RECEIVE_INVENTORY_LIST, allRecordIds, byRecordId, allAttributeIds, byAttributeId, paginationMetadata, statusMap });
            }
            this.isDoneFetchingInventory();
            this.getColumnsForFilters().then(() => {
                this.isDoneFetchingInventory();
            });
            return res;
        });
    }

    fetchInventoryPage(pageNumber: number = this.currentPageNumber, pageSize?: number): ng.IPromise<IProtocolPacket> {
        this.isFetchingInventory();
        sessionStorage.setItem(`DMInventoryPageNumber${this.inventoryId}`, JSON.stringify(pageNumber));
        return this.inventoryAdapter.getInventoryPage(toString(this.inventoryId), pageNumber, pageSize, this.currentSearchTerm).then((res: IProtocolPacket): IProtocolPacket => {
            if (res.result) {
                const allRecordIds: string[] = res.data.records.map((record: IInventoryRecord): string => record.id);
                const byRecordId: IRecordMap = res.data.records.reduce((prev: IRecordMap, curr: IInventoryRecord) => { prev[curr.id] = curr; return prev; }, {});
                const paginationMetadata: IPageable = {
                    content: res.data.records,
                    first: res.data.first,
                    last: res.data.last,
                    number: res.data.number,
                    numberOfElements: res.data.numberOfElements,
                    size: res.data.size,
                    sort: res.data.sort,
                    totalElements: res.data.totalElements,
                    totalPages: res.data.totalPages,
                };
                this.store.dispatch({ type: InventoryActions.RECEIVE_INVENTORY_PAGE, allRecordIds, byRecordId, paginationMetadata, searchTerm: this.currentSearchTerm });
            }
            this.isDoneFetchingInventory();
            return res;
        });
    }

    fetchFilteredInventoryPage = (filterData: IFilterTree, pageNumber: number = this.currentPageNumber, pageSize?: number): ng.IPromise<IProtocolPacket> => {
        this.isFetchingInventory();
        sessionStorage.setItem(`DMInventoryPageNumber${this.inventoryId}`, JSON.stringify(pageNumber));
        return this.inventoryAdapter.getFilteredInventoryPage(toString(this.inventoryId), filterData, pageNumber, pageSize, this.currentSearchTerm).then((res: IProtocolPacket): any => {
            if (res.result) {
                const allRecordIds: string[] = res.data.records.map((record: IInventoryRecord): string => record.id);
                const byRecordId: IRecordMap = res.data.records.reduce((prev: IRecordMap, curr: IInventoryRecord) => { prev[curr.id] = curr; return prev; }, {});
                const paginationMetadata: IPageable = {
                    content: res.data.records,
                    first: res.data.first,
                    last: res.data.last,
                    number: res.data.number,
                    numberOfElements: res.data.numberOfElements,
                    size: res.data.size,
                    sort: res.data.sort,
                    totalElements: res.data.totalElements,
                    totalPages: res.data.totalPages,
                };
                this.store.dispatch({ type: InventoryActions.RECEIVE_INVENTORY_PAGE, allRecordIds, byRecordId, paginationMetadata, searchTerm: this.currentSearchTerm });
            }
            this.isDoneFetchingInventory();
            return res;
        });
    }

    getAssetMapData = (orgGroupId: string): Observable<IProtocolResponse<{ data: IMapDataPoint[] }>> => {
        return this.inventory.getAssetMapData(orgGroupId);
    }

    closeAllDrawers(): void {
        this.store.dispatch({ type: InventoryActions.CLOSE_ALL_DRAWERS });
        this.drawerAction.closeDrawer();
    }

    closeAttributeDrawer(): void {
        this.store.dispatch({ type: InventoryActions.CLOSE_ATTRIBUTE_DRAWER });
        this.drawerAction.closeDrawer();
    }

    openAttributeDrawer(): void {
        this.store.dispatch({ type: InventoryActions.OPEN_ATTRIBUTE_DRAWER });
        this.drawerAction.openDrawer();
    }

    closeFilterDrawer(): void {
        this.store.dispatch({ type: InventoryActions.CLOSE_FILTER_DRAWER });
        this.drawerAction.closeDrawer();
    }

    openFilterDrawer(): void {
        this.store.dispatch({ type: InventoryActions.OPEN_FILTER_DRAWER });
        this.drawerAction.openDrawer();
    }

    isFetchingInventory(): void {
        this.store.dispatch({ type: InventoryActions.FETCHING_INVENTORY_LIST });
    }

    isDoneFetchingInventory(): void {
        this.store.dispatch({ type: InventoryActions.INVENTORY_LIST_FETCHED });
    }

    isFetchingLinkedInventory(): void {
        this.store.dispatch({ type: InventoryActions.FETCHING_INVENTORY_RELATED_LIST });
    }

    isDoneFetchingLinkedInventory(): void {
        this.store.dispatch({ type: InventoryActions.FETCHED_INVENTORY_RELATED_LIST });
    }

    isFilteringInventory(): void {
        this.store.dispatch({ type: InventoryActions.FILTERING_INVENTORY_LIST });
    }

    isDoneFilteringInventory(): void {
        this.store.dispatch({ type: InventoryActions.INVENTORY_LIST_FILTERED });
    }

    validateRecordState(): void {
        this.store.dispatch({ type: InventoryActions.VALIDATE_RECORD_STATE });
    }

    invalidateRecordState(): void {
        this.store.dispatch({ type: InventoryActions.INVALIDATE_RECORD_STATE });
    }

    isSavingRecord(): void {
        this.store.dispatch({ type: InventoryActions.RECORD_SAVING });
    }

    isDoneSavingRecord(): void {
        this.store.dispatch({ type: InventoryActions.RECORD_SAVED });
    }

    setShowingFilteredResults(showingFilteredResults: boolean): void {
        this.store.dispatch({ type: InventoryActions.SHOWING_FILTERED_RESULTS, showingFilteredResults });
    }

    recordEdited(updatedRecordModel: IRecordResponse | {}, attributeMap: IAttributeMap, updatedOrgId?: string): void {
        const byAttributeId: IAttributeMap = attributeMap || getAttributeMap(this.store.getState());
        this.store.dispatch({ type: InventoryActions.RECORD_EDITED, updatedRecordModel, byAttributeId, updatedOrgId });
    }

    recordEditEnabled(initialRecordModel: IRecordResponse | {}): void {
        this.store.dispatch({ type: InventoryActions.RECORD_EDIT_ENABLED, initialRecordModel });
    }

    recordEditDisabled(): void {
        this.store.dispatch({ type: InventoryActions.RECORD_EDIT_DISABLED });
    }

    setInventoryId(inventoryId: number): void {
        this.store.dispatch({ type: InventoryActions.SET_INVENTORY_ID, inventoryId });
    }

    setPageNumber(page: number): void {
        this.store.dispatch({ type: InventoryActions.SET_PAGE_NUMBER, page });
    }

    setSearchTerm(searchTerm: string): void {
        this.store.dispatch({ type: InventoryActions.SET_SEARCH_TERM, searchTerm });
    }

    setFilterData(filters: IFilterTree): void {
        this.inventoryListAction.setFilterSubjectState(filters);
        this.store.dispatch({ type: InventoryActions.SET_FILTER_DATA, filters });
    }

    setDefaultFilterData(): void {
        this.inventoryListAction.defaultFilterSubjectState();
        this.store.dispatch({ type: InventoryActions.SET_DEFAULT_FILTER_DATA });
    }

    recordEditCancelled(): void {
        const storeState: IStoreState = this.store.getState();
        const savedRecord: IInventoryRecord | {} = getInitialRecord(storeState);
        const updatedOrgId: string = this.inventoryAdapter.getCurrentRecordOrgId(savedRecord as IInventoryRecord);
        this.store.dispatch({ type: InventoryActions.RECORD_EDIT_CANCELLED, savedRecord, updatedOrgId });
    }

    setRequiredFieldsAsValid(): void {
        this.store.dispatch({ type: InventoryActions.REQUIRED_FIELDS_POPULATED });
    }

    setRecordDefaultState(): void {
        this.store.dispatch({ type: InventoryActions.SET_RECORD_DEFAULT_STATE });
    }

    setRequiredFieldsAsInvalid(): void {
        this.store.dispatch({ type: InventoryActions.REQUIRED_FIELDS_NOT_POPULATED });
    }

    launchAddModal(): void {
        this.setEmptyCurrentRecord();
        this.setRequiredFieldsAsInvalid();
        const modalData: IInventoryAddModalResolve = {
            modalTitle: this.inventoryViewLogic.getLabel(this.inventoryId, "addModalTitle"),
            inventoryId: this.inventoryId,
            callback: (): void => this.getInventoryList(),
        };

        this.modalService.setModalData(modalData);
        this.modalService.openModal("addInventoryModal");
    }

    launchCopyModal(recordId: string): void {
        const modalData: IInventoryCopyModalResolve = {
            recordId,
            callback: (): void => this.getInventoryList(),
        };
        this.modalService.setModalData(modalData);
        this.modalService.openModal("copyInventoryModal");
    }

    getBulkInventoryList(records: IInventoryRecord[], inventoryId: number): IRecordAssignment[] {
        return this.inventoryAdapter.getRecordAssignments(records, inventoryId);
    }

    fetchInventoryRecord(recordId: string, inventoryId?: number): ng.IPromise<IProtocolResponse<IInventoryDetails>> {
        this.store.dispatch({ type: InventoryActions.FETCHING_RECORD });
        return this.getDetailsView(recordId, inventoryId);
    }

    getColumnsForFilters = (): ng.IPromise<void> => {
        return this.getColumns().then((response: IProtocolPacket) => {
            if (!response.data) return;
            // TODO return all columns regardless of types when filters support them
            this.inventoryListAction.setColumnFilterState(this.inventoryAdapter.formatColumnData(response.data));
        });
    }

    saveRecord(record: IInventoryRecord, setCurrentRecord: boolean = true): ng.IPromise<IProtocolPacket> {
        this.isSavingRecord();
        return this.inventoryAdapter.upsertFormattedRecord(toString(this.inventoryId), record, record.id).then((response: IProtocolPacket): IProtocolPacket => {
            if (response.result) {
                this.inventoryAdapter.setRecordUsers(response.data).then((res: IUserMap): void => {
                    if (res) {
                        this.store.dispatch({ type: InventoryActions.RECORD_SAVED });
                        if (setCurrentRecord) {
                            const statusMap: IAssessmentStatusMap = getStatusMap(this.store.getState());
                            const attributeMap: IStringMap<IAttribute> = getAttributeMap(this.store.getState());
                            const currentRecordModel: IInventoryRecord = { id: record.id, canEdit: record.canEdit, assessmentOutstanding: record.assessmentOutstanding, cells: this.inventoryAdapter.getRecordCells(attributeMap, statusMap, response.data) };
                            this.store.dispatch({ type: InventoryActions.SET_CURRENT_RECORD, currentRecordModel });
                        }
                    } else {
                        this.store.dispatch({ type: InventoryActions.RECORD_NOT_SAVING });
                    }
                });
            } else {
                this.store.dispatch({ type: InventoryActions.RECORD_NOT_SAVING });
            }
            return response;
        });
    }

    addRecord(record?: IInventoryRecord): ng.IPromise<IProtocolPacket> {
        this.isSavingRecord();
        const currentRecord: IInventoryRecord = record ? record : getCurrentRecord(this.store.getState());
        return this.inventoryAdapter.upsertFormattedRecord(toString(this.inventoryId), currentRecord, "").then((response: IProtocolPacket): IProtocolPacket => {
            this.isDoneSavingRecord();
            return response;
        });
    }

    setEmptyCurrentRecord(): void {
        const statusMap: IAssessmentStatusMap = getStatusMap(this.store.getState());
        const attributeMap: IStringMap<IAttribute> = getAttributeMap(this.store.getState());
        const currentRecordModel: IInventoryRecord = { id: null, canEdit: true, assessmentOutstanding: false, cells: this.inventoryAdapter.getRecordCells(attributeMap, statusMap) };
        this.store.dispatch({ type: InventoryActions.SET_CURRENT_RECORD, currentRecordModel });
    }

    deleteRecord(recordId: string, callback?: (response: IProtocolResponse<IStringMap<string>>) => void): void {
        const resolvePromise: () => ng.IPromise<boolean> = () => this.inventory.deleteRecord(this.inventoryId, recordId).then((response: IProtocolResponse<IStringMap<string>>): boolean => {
            if (callback) {
                callback(response);
            }
            return response.result;
        });
        const confirmationMessageKey = this.inventoryId !== InventoryTableIds.Elements && this.permissions.canShow("RiskInheritance")
            ? "SureToDeleteThisRecordWithInheritedRisks"
            : "SureToDeleteThisRecord";
        this.launchDeleteConfirmation(resolvePromise, this.translatePipe.transform("DeleteRecord"), this.translatePipe.transform(confirmationMessageKey));
    }

    unlinkInventoryRecord(recordId: string, params: any, callback?: () => void): void {
        const resolvePromise: () => ng.IPromise<boolean> = () => this.inventory.removeInventoryLink(recordId, params).then((response: IProtocolResponse<null>): boolean => {
            if (callback) {
                callback();
            }
            return response.result;
        });
        this.launchDeleteConfirmation(resolvePromise, this.translatePipe.transform("RemoveRelated"), this.translatePipe.transform("SureToRemoveRelatedAndReference"), this.translatePipe.transform("Remove"));
    }

    unlinkPersonalData(recordId: string, params: ILinkPersonalData[], callback?: () => void): void {
        this.otModalService.confirm({
            type: ConfirmModalType.DELETE,
            translations: {
                title: this.translatePipe.transform("RemoveRelated"),
                desc: "",
                confirm: this.translatePipe.transform("SureToRemoveRelated"),
                submit: this.translatePipe.transform("Remove"),
                cancel: this.translatePipe.transform("Cancel"),
            },
            hideClose: true,
        }).pipe(
            take(1),
            filter((result: boolean) => result),
        ).subscribe(() =>
            this.inventory.removePersonalDataLink(recordId, params)
                .pipe(
                    take(1),
                ).subscribe(() => {
                    if (callback) {
                        callback();
                    }
                },
            ),
        );
    }

    unlinkRecord(record: IInventoryRecord, callback?: (response: IProtocolResponse<IStringMap<string>>) => void): void {
        const resolvePromise: () => ng.IPromise<boolean> = () => this.saveRecord(record).then((response: IProtocolResponse<IStringMap<string>>): boolean => {
            if (callback) {
                callback(response);
            }
            return response.result;
        });
        this.launchDeleteConfirmation(resolvePromise, this.translatePipe.transform("RemoveRelated"), this.translatePipe.transform("SureToRemoveRelated"), this.translatePipe.transform("Remove"));
    }

    bulkDeleteRecords(records: IInventoryRecord[], page: number, pageSize?: number): void {
        const isSingleRecord = records.length === 1;
        const modaltitle = isSingleRecord ? "DeleteRecord" : "DeleteRecords";
        let confirmationMessageKey = "";
        const resolvePromise: () => ng.IPromise<boolean> = () => this.inventory.bulkDeleteRecords(this.inventoryId, records).then((response: boolean): boolean => {
            if (response && this.showingFilteredResults) {
                this.fetchFilteredInventoryPage(this.filterData);
            } else if (response && !this.showingFilteredResults) {
                this.fetchInventoryPage(page, pageSize);
            }
            return response;
        });

        if (this.inventoryId !== InventoryTableIds.Elements && this.permissions.canShow("RiskInheritance")) {
            confirmationMessageKey = isSingleRecord ? "SureToDeleteThisRecordWithInheritedRisks"
                : "SureToDeleteTheseRecordsWithInheritedRisks";
        } else {
            confirmationMessageKey = isSingleRecord ? "SureToDeleteSelectedRecord" : "SureToDeleteSelectedRecords";
        }
        this.launchDeleteConfirmation(resolvePromise, this.translatePipe.transform(modaltitle), this.translatePipe.transform(confirmationMessageKey));
    }

    deleteAssessment(recordId: string, assessmentId: string): void {
        const modalData: IDeleteConfirmationModalResolve = {
            modalTitle: this.translatePipe.transform("DeleteAssessment"),
            confirmationText: this.translatePipe.transform("CanWeDeleteAssessmentRecord"),
            cancelButtonText: this.translatePipe.transform("Cancel"),
            submitButtonText: this.translatePipe.transform("Confirm"),
            promiseToResolve:
                (): ng.IPromise<boolean> => this.inventory.deleteAssessment(assessmentId)
                    .then((response: IProtocolResponse<boolean>): boolean => {
                        if (response.result) {
                            this.reloadAssessmentList(recordId);
                        }
                        return response.result;
                    }),
        };
        this.modalService.setModalData(modalData);
        this.modalService.openModal("deleteConfirmationModal");
    }

    toggleRecordSelection(record: IInventoryRecord, isSelected: boolean): void {
        this.store.dispatch({ type: InventoryActions.TOGGLE_RECORD_SELECTION, record, isSelected });
    }

    toggleAllRecordsSelection(isSelected: boolean): void {
        this.store.dispatch({ type: InventoryActions.TOGGLE_ALL_RECORDS_SELECTION, isSelected });
    }

    setUserValidity(attributeId: string, isValid: boolean): void {
        this.store.dispatch({ type: InventoryActions.SET_USER_VALIDITY, attributeId, isValid });
    }

    launchAssessments(inventoryName: string, processes: IRecordAssignment[], callback: (response: IProtocolResponse<ILaunchResponse[]>) => void, page: number, pageSize?: number): void {
        const modalData: ILaunchModalResolve = {
            modalTitle: this.translatePipe.transform("SendAssessments"),
            Processes: processes,
            TemplateType: this.inventoryId,
            TableName: inventoryName,
            callback,
        };

        this.modalService.setModalData(modalData);
        this.modalService.openModal("launchAssessmentModal");
    }

    exportInventory(): void {
        this.store.dispatch({ type: InventoryActions.EXPORTING_INVENTORY });
        this.inventory.exportInventory(toString(this.inventoryId)).then((): void => {
            const modalData: ITaskConfirmationModalResolve = {
                modalTitle: this.translatePipe.transform("ReportDownload"),
                confirmationText: this.translatePipe.transform("ReportIsGeneratedCheckTasksBar"),
            };
            this.modalService.setModalData(modalData);
            this.modalService.openModal("oneNotificationModalComponent");
            this.store.dispatch({ type: InventoryActions.INVENTORY_EXPORTED });
        });
    }

    exportArt30Report(): void {
        this.store.dispatch({ type: InventoryActions.EXPORTING_INVENTORY });
        this.inventory.exportArt30Report().then((): void => {
            const modalData: ITaskConfirmationModalResolve = {
                modalTitle: this.translatePipe.transform("ReportDownload"),
                confirmationText: this.translatePipe.transform("ReportIsGeneratedCheckTasksBar"),
            };
            this.modalService.setModalData(modalData);
            this.modalService.openModal("oneNotificationModalComponent");
            this.store.dispatch({ type: InventoryActions.INVENTORY_EXPORTED });
        });
    }

    exportV2Report(schemaId: string): void {
        this.store.dispatch({ type: InventoryActions.EXPORTING_INVENTORY });
        this.inventory.exportV2Report(schemaId).then((): void => {
            const modalData: ITaskConfirmationModalResolve = {
                modalTitle: this.translatePipe.transform("ReportDownload"),
                confirmationText: this.translatePipe.transform("ReportIsGeneratedCheckTasksBar"),
            };
            this.modalService.setModalData(modalData);
            this.modalService.openModal("oneNotificationModalComponent");
            this.store.dispatch({ type: InventoryActions.INVENTORY_EXPORTED });
        });
    }

    exportPDF(record: IInventoryRecord, inventoryId: number): void {
        this.store.dispatch({ type: InventoryActions.EXPORTING_INVENTORY });

        const modalData: ITaskConfirmationModalResolve = {
            modalTitle: this.translatePipe.transform("ReportDownload"),
            confirmationText: this.translatePipe.transform("ReportIsGeneratedCheckTasksBar"),
        };
        this.modalService.setModalData(modalData);
        this.modalService.openModal("oneNotificationModalComponent");

        this.reportAPI.getColumns(ModuleTypes.INV, toString(inventoryId)).subscribe((response: IProtocolResponse<IFilterColumnOption[]>): void => {
            if (response.result) {
                const formattedColumns: IFilterColumn[] = this.reportsAdapter.formatColumnData(response.data);
                const requestPayload: IReportExport = {
                    reportExportFormat: "Pdf",
                    name: this.inventoryViewLogic.getRecordName(record, toString(inventoryId)),
                    entityId: record.id,
                    reportEntityType: ModuleTypes.INV,
                    layout: {
                        sections: [
                            {
                                order: 0,
                                name: "Default",
                                type: PDFSectionTypes.InventoryAttribute,
                                columns: formattedColumns,
                            },
                        ],
                    },
                };
                this.inventory.exportPDF(requestPayload);
            }
            this.store.dispatch({ type: InventoryActions.INVENTORY_EXPORTED });
        });
    }

    openAddRiskModal(recordId: string, inventoryName: string, inventoryType: InventoryTableIds, inventoryOrgId: string, callback: (riskModel: IRiskDetails) => void): void {
        if (this.permissions.canShow("RiskInheritance")) {
            this.otModalService
                .create(
                    RiskModalComponent,
                    {
                        isComponent: true,
                    },
                    {
                        riskModel: this.riskAction.getBlankRiskModel(recordId, RiskTypes.Inventory, RiskSourceTypes.Inventory, inventoryName, inventoryType, inventoryOrgId),
                    },
                ).subscribe((risk: IRiskDetails) => {
                    if (risk) {
                        callback(risk);
                    }
                });
        } else {
            const modalData: IRiskModalData = {
                viewService: "InventoryRiskModalViewLogic",
                userList: [],
                saveRisk: this.saveRisk,
                callback,
                inventoryName,
                inventoryOrgId,
            };
            modalData.selectedModelId = this.riskAction.addBlankRiskModelV2(recordId, RiskTypes.Inventory, RiskSourceTypes.Inventory, modalData.inventoryName, inventoryType, inventoryOrgId);
            this.riskAction.openCreateNewRiskModalV2(modalData);
        }
    }

    openEditRiskModal(risk: IRiskDetails, callback?: (riskModel: IRiskDetails) => void): void {
        const riskExistsInStore: boolean = Boolean(getRiskById(risk.id)(this.store.getState()));
        const modalData: IRiskModalData = {
            viewService: "InventoryRiskModalViewLogic",
            userList: [],
            saveRisk: this.updateRisk,
            callback,
        };
        if (riskExistsInStore) {
            this.riskAction.addTempRisk(risk.id, "id");
            this.riskAction.openEditRiskModalV2(risk.id, modalData);
        } else {
            this.riskAction.addRisk(risk, "id");
            this.riskAction.addTempRisk(risk.id, "id");
            this.riskAction.openEditRiskModalV2(risk.id, modalData);
        }
    }

    deleteInventoryRisk(record: IInventoryRecord, risk: IRiskDetails): void {
        const resolvePromise = (): ng.IPromise<boolean> => {
            this.handleRiskChange(record, risk, true);
            return this.riskAction.deleteRiskV2(risk.id);
        };
        this.launchDeleteConfirmation(resolvePromise, this.translatePipe.transform("DeleteRisk"), this.translatePipe.transform("SureToDeleteThisRisk"));
    }

    deleteInventoryRisks(resolvePromise: any): void {
        this.launchDeleteConfirmation(resolvePromise, this.translatePipe.transform("DeleteRisk"), this.translatePipe.transform("SureToDeleteThisRisk"));
    }

    updateAssessmentStatuses(): ng.IPromise<boolean> {
        return this.dmAssessments.getDataMappingStatuses().then((response: IProtocolPacket): boolean => {
            if (!response.result) return false;
            const currentState: IStoreState = this.store.getState();
            const statusMap: IAssessmentStatusMap = response.data;
            const byRecordId: IRecordMap = this.inventoryAdapter.setRecordStatuses(getAllRecordsMap(currentState), statusMap);
            this.store.dispatch({ type: InventoryActions.UPDATED_STATUSES, byRecordId, statusMap });
            return true;
        });
    }

    getAllRecordDetails(recordId: string, permissions: IStringMap<boolean>): void {
        // Only get v1 record if not on new feature set
        if (!permissions.canViewLinkedInventory) {
            this.store.dispatch({ type: InventoryActions.FETCHING_ALL_INVENTORY_RECORD_DETAILS });
            this.getDetailsView(recordId);
        }
        if (permissions.canViewLineage && permissions.canViewLinkedInventory) {
            this.inventoryLineageService.getLineageDiagram(recordId);
            this.getLinkedInventory(recordId, InventoryContexts.Lineage);
        }
        if (permissions.canViewLineage && !permissions.canViewLinkedInventory) {
            this.inventoryLineageService.getLineageDiagram(recordId);
            this.getRelatedInventory(recordId, InventoryContexts.Lineage);
        }
    }

    addAssessmentLink(recordId: string, callback?: () => void, typeId?: number, recordName?: string): void {
        const modalData: ILinkAssessmentModalData = {
            body: this.translatePipe.transform(InventoryTranslationKeys[this.inventoryId].linkAssessmentText),
            recordId,
            typeId,
            recordName,
            callback,
        };
        this.inventoryLaunchModalService.launchLinkAssessmentsModal(modalData);
    }

    removeAssessmentLink(recordId: string, assessmentId: string): void {
        this.store.dispatch({ type: InventoryActions.FETCHING_INVENTORY_ASSESSMENT_LIST });
        this.inventoryAssessment.removeAssessmentLink(recordId, assessmentId).then((assessments: IInventoryAssessment[]): void => {
            const allAssessmentIds: string[] = assessments.map((assessment: IInventoryAssessment): string => assessment.id);
            const byAssessmentId: IStringMap<IDMAssessment> = assessments.reduce((prev: IStringMap<IInventoryAssessment>, curr: IInventoryAssessment) => { prev[curr.id] = curr; return prev; }, {});
            this.store.dispatch({ type: InventoryActions.SET_INVENTORY_ASSESSMENT_LIST, byAssessmentId, allAssessmentIds });
        });
    }

    removeAssessmentV2Link(recordId: string, assessmentId: string): void {
        this.assessmentLinkingAPI.removeAssessmentLink(recordId, assessmentId).then((result: boolean): void => {
            if (result) {
                this.reloadAssessmentList(recordId);
            }
        });
    }

    reloadAssessmentList(recordId: string): void {
        this.store.dispatch({ type: InventoryActions.FETCHING_INVENTORY_ASSESSMENT_LIST });
        this.getLinkedAssessmentView(recordId);
    }

    changeInventoryRiskLevel(record: IInventoryRecord, risk: IRiskDetails, levelId: number): void {
        this.riskAction.updateRiskV2(risk, { levelId });
        this.handleRiskChange(record, { ...risk, levelId }, true);
    }

    handleRiskChange(record: IInventoryRecord, risk: IRiskDetails, updateActiveRecord: boolean = false): ng.IPromise<boolean> | void {
        if (risk) this.riskAction.updateRisk(risk.id, risk);
        this.inventoryRisk.getUpdatedRiskScore(record).then((riskResponse: { updatedRecord: IInventoryRecord, updateRiskLevel: number }): void | undefined => {
            if (updateActiveRecord) {
                this.store.dispatch({ type: InventoryActions.SET_CURRENT_RECORD, currentRecordModel: riskResponse.updatedRecord });
                this.recordActionService.setCurrentRecordRiskLevel(RiskLevelStrings[riskResponse.updateRiskLevel]);
            } else {
                this.store.dispatch({ type: InventoryActions.UPDATE_INVENTORY_RECORD_MODEL, record: riskResponse.updatedRecord });
            }
        });
    }

    setDefaultRelatedInventories(): void {
        this.store.dispatch({ type: InventoryActions.SET_DEFAULT_RELATED_INVENTORIES });
    }

    setRelatedInventoryPage(page: number, size: number) {
        this.store.dispatch({ type: InventoryActions.SET_RELATED_INVENTORY_PAGE, page, size});
    }

    getRelatedInventory(recordId: string, context: number): Promise<void | undefined> {
        if (context === InventoryContexts.Lineage) {
            this.inventoryLineageService.updateState({ fetchingRelated: true });
        } else if (context === InventoryContexts.Related) {
            this.isFetchingLinkedInventory();
        }
        return this.inventoryRelated.getRelatedLists(recordId, this.inventoryId, context).then((): void => {
            if (context === InventoryContexts.Lineage) {
                this.inventoryLineageService.updateState({ fetchingRelated: false, ready: true });
            } else if (context === InventoryContexts.Related) {
                this.store.dispatch({ type: InventoryActions.FETCHED_INVENTORY_RELATED_LIST });
            }
        });
    }

    getLinkedInventory(recordId: string, context: number): Promise<void | undefined> {
        if (context === InventoryContexts.Lineage) {
            this.inventoryLineageService.updateState({ fetchingRelated: true });
        } else if (context === InventoryContexts.Related) {
            this.isFetchingLinkedInventory();
        }
        return this.inventoryLinking.getLinkedLists(recordId, this.inventoryId, context).then((): void => {
            if (context === InventoryContexts.Lineage) {
                this.inventoryLineageService.updateState({ fetchingRelated: false, ready: true });
            } else if (context === InventoryContexts.Related) {
                this.store.dispatch({ type: InventoryActions.FETCHED_INVENTORY_RELATED_LIST });
            }
        });
    }

    getRecordRisks(recordId: string): ng.IPromise<void | undefined> {
        this.store.dispatch({ type: InventoryActions.FETCHING_INVENTORY_RISK_LIST });
        return this.inventoryRisk.getRisksByRecordId(recordId).then((risks: IRiskDetails[]): void | undefined => {
            const allIds: string[] = risks.map((risk: IRiskDetails): string => risk.id);
            const byId: IStringMap<IDMAssessment> = risks.reduce((prev: IStringMap<IRiskDetails>, curr: IRiskDetails) => { prev[curr.id] = curr; return prev; }, {});
            this.store.dispatch({ type: InventoryActions.FETCHED_INVENTORY_RISK_LIST });
            this.store.dispatch({ type: RiskActions.RISKS_FETCHED, byId, allIds });
        });
    }

    getLinkedAssessmentView(recordId: string): Promise<void | undefined> {
        this.store.dispatch({ type: InventoryActions.FETCHING_INVENTORY_ASSESSMENT_LIST });
        return this.inventoryAssessment.getAssessmentList(recordId).then((assessments: IInventoryAssessment[]): void | undefined => {
            const allAssessmentIds: string[] = assessments.map((assessment: IInventoryAssessment): string => assessment.id);
            const byAssessmentId: IStringMap<IDMAssessment> = assessments.reduce((prev: IStringMap<IInventoryAssessment>, curr: IInventoryAssessment) => { prev[curr.id] = curr; return prev; }, {});
            this.store.dispatch({ type: InventoryActions.SET_INVENTORY_ASSESSMENT_LIST, byAssessmentId, allAssessmentIds });
        });
    }

    setUserOptions(): Promise<void> {
        return Promise.resolve(this.orgGroupApiService.getOrgUsersWithPermission(
            getCurrentOrgId(this.store.getState()),
            OrgUserTraversal.All,
        ).then((response: IProtocolResponse<IOrgUserAdapted[]>) => {
            this.inventoryListAction.setFilterUserOptions(!(response && response.result) ? [] : this.reportsAdapter.formatUserOptions(response.data));
        }));
    }

    private mapNewRiskFieldsToOld(riskModel: IRiskDetails): IRiskDetails {
        riskModel.level = Number(RiskV2Levels[riskModel.level]);
        riskModel.probabilityLevel = RiskV2Levels[riskModel.probabilityLevel];
        riskModel.impactLevel = RiskV2Levels[riskModel.impactLevel];
        return riskModel;
    }

    private updateRisk = (id: string, risk: IRiskDetails): ng.IPromise<IRiskDetails> => {
        return this.riskAction.updateRiskV2(risk);
    }

    private saveRisk = (tempId: string, risk: IRiskDetails): ng.IPromise<IRiskDetails | boolean> => {
        return this.riskAction.saveRiskV2(risk).then((response: IRiskDetails | boolean): IRiskDetails | boolean => {
            this.riskAction.deleteTempRisk(tempId);
            return response;
        });
    }

    private getDetailsView(recordId: string, inventoryId?: number): ng.IPromise<IProtocolResponse<IInventoryDetails>> {
        if (!inventoryId) {
            inventoryId = this.inventoryId;
        }
        return this.inventoryAdapter.getInventoryDetailsView(toString(inventoryId), recordId).then((res: IProtocolResponse<IInventoryDetails>): IProtocolResponse<IInventoryDetails> => {
            if (res.result) {
                const currentRecordModel: IInventoryRecord = res.data.record;
                const statusMap: IAssessmentStatusMap = res.data.statusMap;
                const allAttributeIds: string[] = res.data.attributes.map((attribute: IAttribute): string => attribute.id);
                const byAttributeId: IAttributeMap = res.data.attributes.reduce((prev: IAttributeMap, curr: IAttribute) => { prev[curr.id] = curr; return prev; }, {});
                const currentRecordOrgId: string = res.data.currentRecordOrgId;
                this.store.dispatch({ type: InventoryActions.RECORD_FETCHED, currentRecordModel, allAttributeIds, byAttributeId, statusMap, currentRecordOrgId });
            }
            return res;
        });
    }

    private componentWillReceiveState(state: IStoreState): void {
        this.currentSearchTerm = getSearchTerm(state);
        this.allRecordIds = getAllRecordIds(state);
        this.paginationMetadata = getInventoryListPaginationMetadata(state);
        this.inventoryId = getInventoryId(state);
        this.currentPageNumber = getPageNumber(state);
        this.showingFilteredResults = isShowingFilteredResults(state);
        this.filterData = getFilterData(state);
    }

    private getColumns = (): ng.IPromise<IProtocolResponse<IFilterColumnOption[]>> => {
        if (this.unresolvedColumnsPromise) return this.unresolvedColumnsPromise;
        this.unresolvedColumnsPromise = this.inventory.getColumns(this.inventoryId).then((response: IProtocolResponse<IFilterColumnOption[]>): IProtocolResponse<IFilterColumnOption[]> => {
            this.unresolvedColumnsPromise = null;

            return response;
        });
        return this.unresolvedColumnsPromise;
    }

    private getInventoryList(): void {
        if (this.showingFilteredResults) {
            this.isFilteringInventory();
            this.fetchFilteredInventoryPage(this.filterData).then((): void => {
                this.isDoneFilteringInventory();
            });
        } else {
            this.fetchInventoryList();
        }
    }

    private launchDeleteConfirmation(resolvePromise: () => ng.IPromise<any>, title: string, text: string, submitButtonText?: string, cancelButtonText?: string): void {
        const modalData: IDeleteConfirmationModalResolve = {
            modalTitle: title,
            confirmationText: text,
            cancelButtonText: cancelButtonText || this.translatePipe.transform("Cancel"),
            submitButtonText: submitButtonText || this.translatePipe.transform("Delete"),
            promiseToResolve: resolvePromise,
        };
        this.modalService.setModalData(modalData);
        this.modalService.openModal("deleteConfirmationModal");
    }
}
