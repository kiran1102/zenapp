// Angular
import { Injectable } from "@angular/core";

// RXJS
import { first } from "rxjs/operators";

// Const and Enums
import { InventoryTranslationKeys } from "constants/inventory.constant";
import { NewInventorySchemaIds } from "constants/inventory-config.constant";
import {
    InventoryStatusOptions,
    ChangeInventoryStatusCloseAction,
} from "enums/inventory.enum";

// Vitreus
import {
    OtModalService,
    ModalSize,
    IConfirmModalConfig,
} from "@onetrust/vitreus";

// Components
import { InventoryAddModalComponent } from "inventoryModule/components/inventory-add-modal/inventory-add-modal.component";
import { InventoryListItemModal } from "inventoryModule/components/inventory-list-item-modal/inventory-list-item-modal.component";
import { InventoryManageDataElementsModal } from "inventoryModule/components/inventory-manage-data-elements-modal/inventory-manage-data-elements-modal.component";
import { ChangeInventoryStatusModalComponent } from "inventoryModule/components/change-inventory-status-modal/change-inventory-status-modal.component";
import { LinkAssessmentsModal } from "inventoryModule/components/link-assessments-modal/link-assessments-modal.component";
import { LinkPersonalDataModal } from "inventoryModule/components/link-personal-data-modal/link-personal-data-modal.component";
import { GridColumnSelectorComponent } from "sharedModules/components/grid-column-selector/grid-column-selector.component";
import { OTNotificationModalComponent } from "sharedModules/components/ot-notification-modal/ot-notification-modal.component";
import { IntgDataDiscoveryModalComponent } from "modules/intg/components/workflows/map-merge/intg-map-and-merge-modal.component";

// Interfaces
import {
    IChangeInventoryStatusModalInputData,
    IInventoryListItem,
    IInventoryElementListItem,
    ILinkAssessmentModalData,
} from "interfaces/inventory.interface";
import { IListState } from "@onetrust/vitreus";
import { IListColumnSelectorData } from "sharedModules/components/grid-column-selector/grid-column-selector.component";
import { IRelateInventoryModalResolve } from "interfaces/relate-inventory-modal.interface";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

@Injectable()
export class InventoryLaunchModalService {

    constructor(
        private otModalService: OtModalService,
        private translatePipe: TranslatePipe,
    ) {}

    launchAddModal(inventoryId: number, defaultRecord: () => void, fetchList: () => void) {
        this.otModalService.create(
            InventoryAddModalComponent,
            {
                isComponent: true,
                size: ModalSize.MEDIUM,
            },
            { inventoryId, title: this.translatePipe.transform(InventoryTranslationKeys[NewInventorySchemaIds[inventoryId]].addModalTitle) },
        ).pipe(
            first(),
        ).subscribe((reloadList: boolean) => {
            defaultRecord();
            if (reloadList) {
                fetchList();
            }
        });
    }

    launchDataListItemModal(listId: string, callback: (listId: string) => void, listItem?: IInventoryListItem | IInventoryElementListItem) {
        this.otModalService.create(
            InventoryListItemModal,
            {
                isComponent: true,
                size: ModalSize.SMALL,
            },
            {
                title: this.translatePipe.transform(listItem ? InventoryTranslationKeys[listId].editModalTitle : InventoryTranslationKeys[listId].addModalTitle),
                listId, listItem, callback,
            },
        );
    }

    launchManageDataElementsModal(modalData?: { listItem: IInventoryListItem, callback?: () => void }) {
        this.otModalService.create(
            InventoryManageDataElementsModal,
            {
                isComponent: true,
                size: ModalSize.MEDIUM,
            },
            modalData,
        );
    }

    launchStatusChangeModal(inventoryId: string, recordIds: string[], closeAction: ChangeInventoryStatusCloseAction, fetchList?: () => void, selectedInventoryStatus?: InventoryStatusOptions, inventoryName?: string, updateRecord?: (string) => void) {
        const myInputData: IChangeInventoryStatusModalInputData = {
            inventoryId,
            recordIds,
            selectedInventoryStatus,
            inventoryName,
        };
        this.otModalService.create(
            ChangeInventoryStatusModalComponent,
            {
                isComponent: true,
                size: ModalSize.SMALL,
            },
            myInputData,
        ).pipe(
            first(),
        ).subscribe((status) => {
            if (status && closeAction === ChangeInventoryStatusCloseAction.RELOAD_LIST) {
                fetchList();
            }
            if (status && closeAction === ChangeInventoryStatusCloseAction.SET_RECORD) {
                updateRecord(status);
            }
        });
    }

    launchLinkAssessmentsModal(modalData: ILinkAssessmentModalData) {
        this.otModalService.create(
            LinkAssessmentsModal,
            {
                isComponent: true,
                size: ModalSize.SMALL,
            },
            modalData,
        );
    }

    launchColumnSelectorModal(modalData: IListColumnSelectorData, callback: (value: IListState) => void ) {
        this.otModalService.create(
            GridColumnSelectorComponent,
            {
                size: ModalSize.MEDIUM,
                isComponent: true,
            },
            { ...modalData, title: "ColumnSelector" },
        ).subscribe((value: IListState) => {
            if (!value) return;
            callback(value);
        });
    }

    launchLinkPersonalDataModal(modalData: IRelateInventoryModalResolve) {
        this.otModalService.create(
            LinkPersonalDataModal,
            {
                size: ModalSize.LARGE,
                isComponent: true,
            },
            modalData,
        );
    }

    launchNotificationModal(title: string, confirmationText: string) {
        this.otModalService.create(
            OTNotificationModalComponent,
            {
                isComponent: true,
            },
            { modalTitle: title, confirmationText },
        );
    }

    launchConfirmationModal(modalData: IConfirmModalConfig) {
        return this.otModalService.confirm({
            type: modalData.type,
            translations: modalData.translations,
            hideClose: modalData.hideClose,
        });
    }

    launchDataDiscoveryModal() {
        this.otModalService
            .create(
                IntgDataDiscoveryModalComponent,
                {
                    isComponent: true,
                    size: ModalSize.SMALL,
                    hideClose: false,
                },
            );
    }

}
