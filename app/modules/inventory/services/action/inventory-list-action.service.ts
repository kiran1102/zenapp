// Angular
import {
    Injectable,
    Inject,
} from "@angular/core";
import { StateService } from "@uirouter/core";

// Rxjs
import {
    BehaviorSubject,
    Observable,
} from "rxjs";

// Redux
import { getCurrentOrgId } from "oneRedux/reducers/org.reducer";
import { getSchema } from "oneRedux/reducers/inventory-record.reducer";

// Services
import { RecordActionService } from "inventoryModule/services/action/record-action.service";
import { InventoryAdapterV2Service } from "inventoryModule/services/adapter/inventory-adapter-v2.service";
import { InventoryRiskV2Service } from "inventoryModule/services/adapter/inventory-risk-v2.service";
import { InventoryService } from "inventorySharedModule/shared/services/inventory-api.service";
import { AssessmentBulkLaunchActionService } from "sharedModules/services/actions/bulk-launch-assessment-action.service";
import { ModalService } from "sharedServices/modal.service";
import { ReportAPI } from "reportsModule/reports-shared/services/report-api.service";
import { ReportsAdapter } from "reportsModule/reports-shared/services/reports-adapter.service";
import { GridViewStorageService } from "sharedModules/services/helper/grid-view-storage.service";
import { Permissions } from "modules/shared/services/helper/permissions.service";
import { OrgGroupApiService } from "oneServices/api/org-group-api.service";
import { InventoryUserLogicService } from "inventoryModule/services/business-logic/inventory-user-logic.service";

// Tokens
import { StoreToken } from "tokens/redux-store.token";

// Interfaces
import { IStringMap } from "interfaces/generic.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";
import {
    IPaginatedResponse,
    IPageableMeta,
} from "interfaces/pagination.interface";
import {
    IInventoryListViewState,
    IInventoryRecordV2,
    IInventoryTableV2,
    IAttributeDetailV2,
    IInventoryCopyV2ModalResolve,
    IAttributeOptionV2,
    IInventoryListV2Column,
} from "interfaces/inventory.interface";
import { IFilterColumnOption } from "interfaces/filter.interface";
import { ISaveChangesModalResolve } from "interfaces/save-changes-modal-resolve.interface";
import { ITaskConfirmationModalResolve } from "interfaces/task-confirmation-modal-resolve.interface";
import { IDeleteConfirmationModalResolve } from "interfaces/delete-confirmation-modal-resolve.interface";
import { IStore } from "interfaces/redux.interface";
import { IRiskDetails } from "interfaces/risk.interface";
import { IAssessmentCreationDetails } from "interfaces/assessment/assessment-creation-details.interface";
import { IReportExport } from "modules/reports/reports-shared/interfaces/report.interface";
import {
    IFilterColumn,
    IFilterTree,
    IFilterValueOption,
} from "interfaces/filter.interface";
import { IOrgUserAdapted } from "interfaces/org-user.interface";
import { IListState } from "@onetrust/vitreus";

// Const and Enums
import {
    AttributeFieldNames,
    InventoryBrowserStorageNames,
    InventoryListV2Columns,
} from "constants/inventory.constant";
import {
    NewInventorySchemaIds,
    LegacyInventorySchemaIds,
} from "constants/inventory-config.constant";
import { ModuleTypes } from "enums/module-types.enum";
import { TableColumnTypes } from "enums/data-table.enum";
import { PDFSectionTypes } from "reportsModule/reports-shared/enums/reports.enum";
import { OrgUserTraversal } from "enums/org-user-dropdown.enum";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";
import { InventoryTableIds } from "enums/inventory.enum";

@Injectable()
export class InventoryListActionService {

    inventoryListViewState$: Observable<IInventoryListViewState>;
    allColumn$: Observable<IFilterColumnOption[]>;
    inventoryListFilter$: Observable<IFilterTree>;
    userOption$: Observable<IFilterValueOption[]>;

    private unresolvedColumnsPromise: ng.IPromise<IProtocolResponse<IFilterColumnOption[]>>;
    private defaultState: IInventoryListViewState = {
        filterDrawerOpen: false,
        attributeDrawerOpen: false,
        listReady: false,
        currentRecordId: "",
        selectedRecords: {},
        currentPageNumber: 0,
        inventoryId: 0,
        isExporting: false,
        isShowingSearchResults: false,
        isShowingFilteredResults: false,
        loadingFilterMetaData: true,
        searchTerm: "",
        filterData: {},
        table: { rows: [], columns: [], metaData: {} as IPageableMeta },
    };
    private _allColumns: BehaviorSubject<IFilterColumnOption[]> = new BehaviorSubject(null);
    private _filters: BehaviorSubject<IFilterTree> = new BehaviorSubject(null);
    private _userOptions: BehaviorSubject<IFilterValueOption[]> = new BehaviorSubject(null);

    private _inventoryListViewState: BehaviorSubject<IInventoryListViewState> = new BehaviorSubject(JSON.parse(JSON.stringify(this.defaultState)));

    constructor(
        private stateService: StateService,
        @Inject(StoreToken) private store: IStore,
        private recordActionService: RecordActionService,
        private inventoryAdapterV2Service: InventoryAdapterV2Service,
        private inventoryService: InventoryService,
        private inventoryRiskV2: InventoryRiskV2Service,
        private modalService: ModalService,
        private translatePipe: TranslatePipe,
        private assessmentBulkLaunchActionService: AssessmentBulkLaunchActionService,
        private inventoryUserLogicService: InventoryUserLogicService,
        private reportAPI: ReportAPI,
        private reportsAdapter: ReportsAdapter,
        private orgGroupApiService: OrgGroupApiService,
        private browserStorageListService: GridViewStorageService,
        private permissions: Permissions,
    ) {
        this.inventoryListViewState$ = this._inventoryListViewState.asObservable();
        this.inventoryListFilter$ = this._filters.asObservable();
        this.allColumn$ = this._allColumns.asObservable();
        this.userOption$ = this._userOptions.asObservable();
    }

    setSelectedRecord(record: IInventoryRecordV2, selected: boolean = false) {
        const currentState = this._inventoryListViewState.getValue();
        const selectedRecord = { [record.id as string]: selected };
        this._inventoryListViewState.next({
            ...currentState,
            selectedRecords: {
                ...currentState.selectedRecords,
                ...selectedRecord,
            },
        });
    }

    setSearchTerm(searchTerm: string) {
        this.updateState({ searchTerm, isShowingSearchResults: searchTerm !== "" });
    }

    setFilterData(filterData: IFilterTree) {
        this.updateState({ filterData, isShowingFilteredResults: Boolean(filterData) });
        this.setFilterSubjectState(filterData);
    }

    setFilterSubjectState(filterData: IFilterTree) {
        this._filters.next(filterData);
    }

    setDefaultState() {
        this.defaultFilterSubjectState();
        this._inventoryListViewState.next(JSON.parse(JSON.stringify(this.defaultState)));
    }

    setColumnFilterState(columns: IFilterColumnOption[]) {
        this._allColumns.next(columns);
    }

    setFilterUserOptions(userOptions: IFilterValueOption[]) {
        this._userOptions.next(userOptions);
    }

    defaultFilterSubjectState() {
        this._allColumns.next(null);
        this._filters.next(null);
    }

    toggleAttributeDrawer(attributeDrawerOpen: boolean) {
        const currentState = this._inventoryListViewState.getValue();
        this._inventoryListViewState.next({
            ...currentState,
            attributeDrawerOpen,
            filterDrawerOpen: false,
        });
    }

    closeDrawer() {
        const currentState = this._inventoryListViewState.getValue();
        this._inventoryListViewState.next({
            ...currentState,
            attributeDrawerOpen: false,
            filterDrawerOpen: false,
        });
    }

    setCurrentRecordId(currentRecordId: string) {
        const currentState = this._inventoryListViewState.getValue();
        this._inventoryListViewState.next({
            ...currentState,
            currentRecordId,
        });
    }

    toggleFilterDrawer(filterDrawerOpen: boolean) {
        const currentState = this._inventoryListViewState.getValue();
        this._inventoryListViewState.next({
            ...currentState,
            filterDrawerOpen,
            attributeDrawerOpen: false,
        });
    }

    updateState(state: {}) {
        const currentState = this._inventoryListViewState.getValue();
        this._inventoryListViewState.next({
            ...currentState,
            ...state,
        });
    }

    setTableData(table: IInventoryTableV2) {
        const currentState = this._inventoryListViewState.getValue();
        this._inventoryListViewState.next({
            ...currentState,
            table,
        });
    }

    initializeTable(inventoryId: number, page: number, size: number = 20) {
        this.updateState({ listReady: false });
        const searchTerm = this._inventoryListViewState.getValue().searchTerm;
        const params = { page, size, filter: { name: searchTerm } };
        Promise.all([
            this.loadSchema(NewInventorySchemaIds[inventoryId]),
            this.setUserOptions(),
        ]).then((responses: [IProtocolResponse<IAttributeDetailV2[]>, void]) => {
            if (responses[0].result) this.setFilterData(JSON.parse(sessionStorage.getItem(`DMInventoryFilterSettings${inventoryId}`)) as IFilterTree);
            const currentState = this._inventoryListViewState.getValue();
            this.updateState({loadingFilterMetaData: false});
            this.getColumnsForFilters(inventoryId).then(() => {
                if (currentState.filterData == null || Object.keys(currentState.filterData).length === 0 || currentState.searchTerm) {
                    this.inventoryService.getInventoryList(inventoryId, params).then((listResponse: IProtocolResponse<IPaginatedResponse<IInventoryRecordV2>>) => {
                        this.handleListResponse(inventoryId, page, responses[0], listResponse);
                    });
                } else {
                    this.inventoryService.getFilteredInventoryList(
                        inventoryId,
                        { filterCriteria: currentState.filterData },
                        params,
                    ).then((listResponse: IProtocolResponse<IPaginatedResponse<IInventoryRecordV2>>) => {
                        this.handleListResponse(inventoryId, page, responses[0], listResponse);
                    });
                }
            });
        });
    }

    handleListResponse(inventoryId, page, schemaResponse, listResponse) {
        const currentState = this._inventoryListViewState.getValue();
        const updatedState: {
            listReady: boolean,
            table?: IInventoryTableV2,
            selectedRecords?: IStringMap<boolean>,
            currentPageNumber: number,
            inventoryId: number,
            isShowingFilteredResults?: boolean,
        } = { listReady: true, currentPageNumber: page, inventoryId };
        if (currentState.filterData != null && Object.keys(currentState.filterData).length !== 0 && !currentState.searchTerm) {
            updatedState.isShowingFilteredResults = true;
        }
        if (schemaResponse.result && listResponse.result) {
            const table = {
                rows: (listResponse).data.data,
                columns: this.getColumnData(NewInventorySchemaIds[inventoryId], schemaResponse.data),
                metaData: (listResponse).data.meta.page,
            };
            updatedState.table = table;
            updatedState.selectedRecords = this.inventoryAdapterV2Service.setSelectedRecords(table.rows, false);
            this.inventoryUserLogicService.setRecordListUsers(table.rows, table.columns).then(() => {
                this.updateState(updatedState);
            });
        } else {
            this.updateState(updatedState);
        }
    }

    loadSchema(inventoryId: string): ng.IPromise<IProtocolResponse<IAttributeDetailV2[]>> {
        return this.inventoryService.getAllAttributes(inventoryId).then((response: IProtocolResponse<IAttributeDetailV2[]>): IProtocolResponse<IAttributeDetailV2[]> => {
            if (response.result) {
                this.recordActionService.setSchema(response.data, inventoryId);
            }
            return response;
        });
    }

    fetchInventoryListV2(inventoryId: number, page: number, size: number = 20) {
        this.updateState({ listReady: false });
        const currentState = this._inventoryListViewState.getValue();
        const params = { page, size, filter: { name: currentState.searchTerm } };
        sessionStorage.setItem(`DMInventoryPageNumber${inventoryId}`, JSON.stringify(page));
        this.inventoryService.getInventoryList(inventoryId, params).then((res: IProtocolResponse<IPaginatedResponse<IInventoryRecordV2>>) => {
            const updatedState: {
                listReady: boolean,
                table?: IInventoryTableV2,
                selectedRecords?: IStringMap<boolean>,
                currentPageNumber: number,
            } = { listReady: true, currentPageNumber: page };
            if (res.result) {
                const table = {
                    rows: res.data.data,
                    columns: this.getColumnData(NewInventorySchemaIds[inventoryId], getSchema(this.store.getState())),
                    metaData: res.data.meta.page,
                };
                updatedState.table = table;
                updatedState.selectedRecords = this.inventoryAdapterV2Service.setSelectedRecords(table.rows, false);
                this.inventoryUserLogicService.setRecordListUsers(table.rows, table.columns).then(() => {
                    this.updateState(updatedState);
                });
            } else {
                this.updateState(updatedState);
            }
        });
    }

    fetchFilteredInventoryListV2(inventoryId: number, page: number, size: number = 20) {
        this.updateState({ listReady: false });
        const currentState = this._inventoryListViewState.getValue();
        const schema = getSchema(this.store.getState());
        const params = { page, size };
        sessionStorage.setItem(`DMInventoryPageNumber${inventoryId}`, JSON.stringify(page));
        this.inventoryService.getFilteredInventoryList(
            inventoryId,
            { filterCriteria: currentState.filterData },
            params,
        ).then((res: IProtocolResponse<IPaginatedResponse<IInventoryRecordV2>>) => {
            const updatedState: {
                listReady: boolean,
                table?: IInventoryTableV2,
                selectedRecords?: IStringMap<boolean>,
                currentPageNumber: number,
                isShowingFilteredResults?: boolean,
            } = { listReady: true, currentPageNumber: page };
            if (res.result) {
                const table = {
                    rows: res.data.data,
                    columns: this.getColumnData(NewInventorySchemaIds[inventoryId], schema),
                    metaData: res.data.meta.page,
                };
                updatedState.table = table;
                updatedState.selectedRecords = this.inventoryAdapterV2Service.setSelectedRecords(table.rows, false);
            }
            updatedState.isShowingFilteredResults = true;
            this.updateState(updatedState);
        });
    }

    applyFilters(filters: IFilterTree, isV2: boolean = true) {
        const inventoryId = this._inventoryListViewState.getValue().inventoryId;
        this._filters.next(filters);
        if (isV2) {
            this.updateState({ isShowingSearchResults: false, searchTerm: "", filterData: filters, isShowingFilteredResults: !(filters == null || Object.keys(filters).length === 0) });
            sessionStorage.removeItem(`DMInventorySearchTerm${inventoryId}`);
            if (filters == null || Object.keys(filters).length === 0) {
                this.fetchInventoryListV2(inventoryId, 0);
                sessionStorage.removeItem(`DMInventoryFilterSettings${inventoryId}`);
            } else {
                this.fetchFilteredInventoryListV2(inventoryId, 0);
                sessionStorage.setItem(`DMInventoryFilterSettings${inventoryId}`, JSON.stringify(filters));
            }
        }
    }

    fetchCurrentList = (page?: number) => {
        const currentState = this._inventoryListViewState.getValue();
        page = page || page === 0 ? page : currentState.currentPageNumber;
        if (currentState.isShowingFilteredResults) {
            this.fetchFilteredInventoryListV2(currentState.inventoryId, page);
        } else {
            this.fetchInventoryListV2(currentState.inventoryId, page);
        }
    }

    handleBulkDelete(records: IInventoryRecordV2[], hasEdits: boolean, requiredFieldsValid: boolean) {
        const currentState = this._inventoryListViewState.getValue();
        const table = currentState.table;
        const newPage: number = (table.rows.length > records.length || !table.metaData.last) || table.metaData.first ? table.metaData.number : table.metaData.number - 1;
        if (hasEdits && requiredFieldsValid) {
            const callback = () => {
                this.closeDrawer();
                setTimeout(() => {
                    this.bulkDeleteRecords(records, newPage);
                }, 0);
            };
            this.launchSaveChangesModal(callback);
        } else {
            this.closeDrawer();
            this.bulkDeleteRecords(records, newPage);
        }
    }

    handleBulkLaunch(hasEdits: boolean, requiredFieldsValid: boolean) {
        const schema = getSchema(this.store.getState());
        const inventoryId = this._inventoryListViewState.getValue().inventoryId;
        const currentPageNumber = this._inventoryListViewState.getValue().currentPageNumber;
        const currentSearchTerm = this._inventoryListViewState.getValue().searchTerm;
        const selectedRecords = this._inventoryListViewState.getValue().selectedRecords;
        if (hasEdits && requiredFieldsValid) {
            const callback = () => {
                this.closeDrawer();
                setTimeout(() => {
                    this.inventoryService.getInventoryList(inventoryId, { size: 20, page: currentPageNumber, filter: { name: currentSearchTerm }}).then((res: IProtocolResponse<IPaginatedResponse<IInventoryRecordV2>>) => {
                        const updatedState: {
                            listReady: boolean,
                            table?: IInventoryTableV2,
                            selectedRecords?: IStringMap<boolean>,
                            currentPageNumber: number,
                        } = { listReady: true, currentPageNumber };
                        if (res.result) {
                            const table = {
                                rows: res.data.data,
                                columns: this.getColumnData(NewInventorySchemaIds[inventoryId], schema),
                                metaData: res.data.meta.page,
                            };
                            updatedState.table = table;
                            updatedState.selectedRecords = selectedRecords;
                            this.handleLaunchInventoryAssessment(this.getSelectedRecords(selectedRecords));
                            this.inventoryUserLogicService.setRecordListUsers(table.rows, table.columns).then(() => {
                                this.updateState(updatedState);
                            });
                        } else {
                            this.updateState(updatedState);
                        }
                    });
                }, 0);
            };
            this.launchSaveChangesModal(callback);
        } else {
            this.closeDrawer();
            this.handleLaunchInventoryAssessment(this.getSelectedRecords());
        }
    }

    getSelectedRecords(selectedMap?: IStringMap<boolean>): IInventoryRecordV2[] {
        const selectedRecordMap = selectedMap || this._inventoryListViewState.getValue().selectedRecords;
        const table = this._inventoryListViewState.getValue().table;
        const selectedRecords: IInventoryRecordV2[] = [];
        for (const recordId in selectedRecordMap) {
            if (selectedRecordMap[recordId]) {
                selectedRecords.push(table.rows.find((record: IInventoryRecordV2) => record.id === recordId));
            }
        }
        return selectedRecords;
    }

    handleLaunchInventoryAssessment(records: IInventoryRecordV2[]) {
        const inventoryTypeId = this._inventoryListViewState.getValue().inventoryId;
        const bulkInventoryList = this.inventoryAdapterV2Service.getRecordAssignments(records, inventoryTypeId);
        const bulkAssessmentDetails: IAssessmentCreationDetails[] = this.assessmentBulkLaunchActionService.getAssessmentCreationItems(bulkInventoryList, inventoryTypeId);
        this.assessmentBulkLaunchActionService.setBulkLaunchAssessments(bulkAssessmentDetails);
        this.stateService.go("zen.app.pia.module.assessment.bulk-launch-inventory-assessment", { inventoryTypeId });
    }

    saveRecord(): ng.IPromise<void> {
        const currentState = this._inventoryListViewState.getValue();
        const saveRecordPromise = (): ng.IPromise<boolean> => {
            return this.recordActionService.saveRecordChanges(NewInventorySchemaIds[currentState.inventoryId], currentState.currentRecordId);
        };
        return saveRecordPromise().then((response: boolean) => {
            if (!response) return;
            this.fetchCurrentList();
        });
    }

    exportV2Report(schemaId: string) {
        this.updateState({ isExporting: true });
        this.inventoryService.exportV2Report(schemaId).then(() => {
            this.updateState({ isExporting: false });
            const modalData: ITaskConfirmationModalResolve = {
                modalTitle: this.translatePipe.transform("ReportDownload"),
                confirmationText: this.translatePipe.transform("ReportIsGeneratedCheckTasksBar"),
            };
            this.modalService.setModalData(modalData);
            this.modalService.openModal("oneNotificationModalComponent");
        });
    }

    exportInventory(schemaId: string) {
        this.updateState({ isExporting: true });
        this.inventoryService.exportInventory(schemaId).then((): void => {
            this.updateState({ isExporting: false });
            const modalData: ITaskConfirmationModalResolve = {
                modalTitle: this.translatePipe.transform("ReportDownload"),
                confirmationText: this.translatePipe.transform("ReportIsGeneratedCheckTasksBar"),
            };
            this.modalService.setModalData(modalData);
            this.modalService.openModal("oneNotificationModalComponent");
        });
    }

    exportPDF(recordId: string, recordName: string, inventoryId?: number): void {
        if (inventoryId === undefined) inventoryId = this._inventoryListViewState.getValue().inventoryId;
        this.updateState({ isExporting: true });

        this.reportAPI.getColumns(ModuleTypes.INV, inventoryId.toString()).subscribe(
            (response: IProtocolResponse<IFilterColumnOption[]>): void => {
                if (response.result) {
                    const formattedColumns: IFilterColumn[] = this.reportsAdapter.formatColumnData(response.data);
                    const requestPayload: IReportExport = {
                        reportExportFormat: "Pdf",
                        name: recordName,
                        entityId: recordId,
                        reportEntityType: ModuleTypes.INV,
                        layout: {
                            sections: [
                                {
                                    order: 0,
                                    name: "Default",
                                    type: PDFSectionTypes.InventoryAttribute,
                                    columns: formattedColumns,
                                },
                            ],
                        },
                    };
                    this.inventoryService.exportPDF(requestPayload);
                }
                this.updateState({ isExporting: false });
            });
    }

    launchSaveChangesModal(callback: () => void) {
        const modalData: ISaveChangesModalResolve = {
            modalTitle: this.translatePipe.transform("SaveChanges"),
            confirmationText: this.translatePipe.transform("UnsavedChangesSaveBeforeContinuing"),
            cancelButtonText: this.translatePipe.transform("Cancel"),
            discardChangesButtonText: this.translatePipe.transform("Discard"),
            saveButtonText: this.translatePipe.transform("Save"),
            promiseToResolve: () => this.saveRecord().then((): boolean => {
                callback();
                return true;
            }),
            discardCallback: () => {
                this.saveCancelled();
                callback();
                return true;
            },
        };
        this.modalService.setModalData(modalData);
        this.modalService.openModal("saveChangesModal");
    }

    bulkDeleteRecords(records: IInventoryRecordV2[], page: number) {
        const isSingleRecord = records.length === 1;
        const modaltitle = isSingleRecord ? "DeleteRecord" : "DeleteRecords";
        let confirmationMessageKey = "";
        const inventoryId = this._inventoryListViewState.getValue().inventoryId;
        const resolvePromise: () => ng.IPromise<boolean> = () => this.inventoryService.bulkDeleteRecords(inventoryId, records).then((response: boolean): boolean => {
            if (response) {
                this.fetchCurrentList(page);
            }
            return response;
        });

        if (inventoryId !== InventoryTableIds.Elements && this.permissions.canShow("RiskInheritance")) {
            confirmationMessageKey = isSingleRecord ? "SureToDeleteThisRecordWithInheritedRisks"
                : "SureToDeleteTheseRecordsWithInheritedRisks";
        } else {
            confirmationMessageKey = isSingleRecord ? "SureToDeleteSelectedRecord" : "SureToDeleteSelectedRecords";
        }
        this.launchDeleteConfirmation(resolvePromise, this.translatePipe.transform(modaltitle), this.translatePipe.transform(confirmationMessageKey));
    }

    saveCancelled() {
        this.recordActionService.toggleEditingRecord(false);
    }

    handleAttributeDrawer(isOpen: boolean, recordEditEnabled: boolean, hasEdits: boolean, requiredFieldsValid: boolean, callback?: () => void) {
        const attributeDrawerOpen = this._inventoryListViewState.getValue().attributeDrawerOpen;
        const defaultCallback = () => { this.toggleAttributeDrawer(false); };
        callback = callback || defaultCallback;
        if (attributeDrawerOpen && recordEditEnabled && hasEdits && requiredFieldsValid) {
            this.launchSaveChangesModal(callback);
            return;
        } else {
            this.toggleAttributeDrawer(isOpen);
            callback();
        }
    }

    launchCopyModal(inventoryId: number, record: IInventoryRecordV2) {
        const modalData: IInventoryCopyV2ModalResolve = {
            inventoryId,
            record,
            callback: () => this.fetchCurrentList(),
        };
        this.modalService.setModalData(modalData);
        this.modalService.openModal("copyInventoryV2Modal");
    }

    openRiskModal(record: IInventoryRecordV2) {
        const inventoryId = Number(this._inventoryListViewState.getValue().inventoryId);
        this.inventoryRiskV2.launchAddRiskModal(record, record[AttributeFieldNames.Name] as string, inventoryId, (record[AttributeFieldNames.Org] as IAttributeOptionV2).id, (riskModel: IRiskDetails) => {
            this.inventoryRiskV2.handleRiskChange(record, inventoryId, riskModel);
        });
    }

    getColumnsForFilters(inventoryId: number): ng.IPromise<void> {
        return this.getFilterMetadata(inventoryId).then((response: IProtocolResponse<IFilterColumnOption[]>) => {
            if (!response.data) return;
            this.setColumnFilterState(this.inventoryAdapterV2Service.formatColumnData(response.data));
        });
    }

    getListColumnSelectorColumns(schemaId: string, schema: IAttributeDetailV2[], reset = false): IListState {
        const right = [];
        const left = [];
        const columnsToDisplayNames = this.getColumnsToDisplayNames(schemaId, schema, reset);
        schema.forEach((attribute) => {
            const isAttributeDefaultColumn = columnsToDisplayNames.find((fieldName: string) => fieldName === attribute.fieldName);
            if (isAttributeDefaultColumn) {
                const index = columnsToDisplayNames.findIndex((fieldName: string) => fieldName === attribute.fieldName);
                right.push(this.inventoryAdapterV2Service.mapAttributeToListItem(attribute, index, schemaId));
            } else {
                const index = columnsToDisplayNames.findIndex((fieldName: string) => fieldName === attribute.fieldName);
                left.push(this.inventoryAdapterV2Service.mapAttributeToListItem(attribute, index, schemaId));
            }
        });
        return { left: this.sortArrayByIndex(left), right: this.sortArrayByIndex(right) };
    }

    getColumnData(schemaId: string, schema: IAttributeDetailV2[]): IInventoryListV2Column[] {
        const allColumns = this.inventoryAdapterV2Service.mapInventoryCellType(schemaId, schema);
        const { columns } = this.browserStorageListService.getBrowserStorageList(InventoryBrowserStorageNames[schemaId]);
        if (columns) {
            // When Inventory schema updates by Attribute Manager, remove any invalid columns from localStorage
            const columnsFilteredBySchema = columns.filter((col: string) => allColumns.find((c) => c.fieldName === col));
            this.browserStorageListService.saveLocalStorageColumns(InventoryBrowserStorageNames[schemaId], columnsFilteredBySchema);
        }
        return this.getColumnsToDisplay(schemaId, allColumns);
    }

    getColumnsToDisplay(schemaId: string, allColumns: IInventoryListV2Column[], reset = false): IInventoryListV2Column[] {
        const columnsToDisplayNames = this.getColumnsToDisplayNames(schemaId, allColumns, reset);
        return columnsToDisplayNames.map((columnName: string) => {
            return allColumns.find((column) => column.fieldName === columnName);
        });
    }

    getColumnsToDisplayNames(schemaId: string, allColumns: IInventoryListV2Column[], reset: boolean): string[] {
        const { columns } = this.browserStorageListService.getBrowserStorageList(InventoryBrowserStorageNames[schemaId]);
        if (!reset && columns && columns.length) {
            return this.filterColumnsToDisplay(columns, allColumns);
        } else {
            return this.filterColumnsToDisplay(InventoryListV2Columns[LegacyInventorySchemaIds[schemaId]], allColumns);
        }
    }

    filterColumnsToDisplay(columnNames: string[], allColumns: IInventoryListV2Column[]): string[] {
        return columnNames.filter((name) => {
            // remove Status column when no permission to view
            if (name === TableColumnTypes.InventoryStatus && !this.permissions.canShow("DataMappingInventoryStatus")) return false;
            // remove columns that aren't active in schema
            if (!allColumns.find((column) => column.fieldName === name)) return false;
            return true;
        });
    }

    setUserOptions(): Promise<void> {
        return Promise.resolve(this.orgGroupApiService.getOrgUsersWithPermission(
            getCurrentOrgId(this.store.getState()),
            OrgUserTraversal.All,
        ).then((response: IProtocolResponse<IOrgUserAdapted[]>) => {
            this.setFilterUserOptions(!(response && response.result) ? [] : this.reportsAdapter.formatUserOptions(response.data));
        }));
    }

    private getFilterMetadata(inventoryId: number): ng.IPromise<IProtocolResponse<IFilterColumnOption[]>> {
        if (this.unresolvedColumnsPromise) return this.unresolvedColumnsPromise;
        this.unresolvedColumnsPromise = this.inventoryService.getFilterMetadata(inventoryId).then((response: IProtocolResponse<IFilterColumnOption[]>): IProtocolResponse<IFilterColumnOption[]> => {
            this.unresolvedColumnsPromise = null;
            return response;
        });
        return this.unresolvedColumnsPromise;
    }

    private launchDeleteConfirmation(resolvePromise: () => ng.IPromise<any>, title: string, text: string, submitButtonText?: string, cancelButtonText?: string) {
        const modalData: IDeleteConfirmationModalResolve = {
            modalTitle: title,
            confirmationText: text,
            cancelButtonText: cancelButtonText || this.translatePipe.transform("Cancel"),
            submitButtonText: submitButtonText || this.translatePipe.transform("Delete"),
            promiseToResolve: resolvePromise,
        };
        this.modalService.setModalData(modalData);
        this.modalService.openModal("deleteConfirmationModal");
    }

    private sortArrayByIndex(arr: any[]) {
        return arr.sort((item1, item2) => item1.index - item2.index);
    }
}
