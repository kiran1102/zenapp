// 3rd party
import { Injectable, Inject } from "@angular/core";
import { filter, map } from "lodash";

// Services
import { UserActionService } from "oneServices/actions/user-action.service";

// Redux
import { StoreToken } from "tokens/redux-store.token";
import { getAllUserModels } from "oneRedux/reducers/user.reducer";

// Constants
import { AttributeTypes } from "constants/inventory.constant";

// interfaces
import { IValueId } from "interfaces/generic.interface";
import {
    IInventoryRecordV2,
    IAttributeDetailV2,
    IAttributeOptionV2,
    IInventoryListV2Column,
} from "interfaces/inventory.interface";
import { IUserMap } from "interfaces/user-reducer.interface";
import { IUser, IUserDetail } from "interfaces/user.interface";
import { IStore } from "interfaces/redux.interface";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

@Injectable()
export class InventoryUserLogicService {

    constructor(
        @Inject(StoreToken) private store: IStore,
        private userActionService: UserActionService,
        private translatePipe: TranslatePipe,
    ) {}

    userSelectionHasChanged(oldValue: IAttributeOptionV2[], newValue: IAttributeOptionV2[]): boolean {
        if (!oldValue && !newValue) return false;
        if ((oldValue && !newValue) || (!oldValue && newValue)) return true;
        if ((oldValue.length && !newValue.length) || (!oldValue.length && newValue.length)) return true;
        if (oldValue.length !== newValue.length) return true;
        if (oldValue[0].id && newValue[0].id) return oldValue[0].id !== newValue[0].id;
        if (!oldValue[0].id && !newValue[0].id) return oldValue[0].value !== newValue[0].value;
        return true;
    }

    setRecordUsers(record: IInventoryRecordV2, schema: IAttributeDetailV2[]): Promise<boolean> {
        const unknownIds: string[] = this.getUnknownUsersFromRecord(record, schema);
        return this.userActionService.fetchUsersDetailsById(unknownIds);
    }

    setRecordListUsers(records: IInventoryRecordV2[], columns: IInventoryListV2Column[]): Promise<boolean> {
        const unknownIds: string[] = this.getUnknownUsersFromRecordList(records, columns);
        return this.userActionService.fetchUsersDetailsById(unknownIds);
    }

    getUserDisplayText(selection: IAttributeOptionV2[]): string {
        if (!selection || !selection.length) return "";
        if (selection[0].value && !selection[0].id) return selection[0].value;
        const userMap: IUserMap = getAllUserModels(this.store.getState());
        const user: IUser = userMap[selection[0].id];
        if (user) {
            const tempFullName = user.FirstName && user.LastName ? `${user.FirstName} ${user.LastName}` : user.FirstNameLastName || user.Email;
            if (user.IsActive === false) {
                return `${tempFullName} (${this.translatePipe.transform("Disabled")})`;
            } else if (user.IsInternal === false) {
                return `${tempFullName} (${this.translatePipe.transform("External")})`;
            } else {
                return tempFullName;
            }
        }
        return "";
    }

    getUserModelAsString(attribute: IAttributeDetailV2, record: IInventoryRecordV2): string {
        if (!record[attribute.fieldName] || !(record[attribute.fieldName] as Array<IValueId<string>>).length) return "";
        if (!record[attribute.fieldName][0].id) return record[attribute.fieldName][0].value;
        const userMap: IUserMap = getAllUserModels(this.store.getState());
        const user: IUser = userMap[record[attribute.fieldName][0].id];
        if (user && !user.IsActive) return user.FullName || user.Email;
        return record[attribute.fieldName][0].id || this.translatePipe.transform("UserNotFound");
    }

    private getUnknownUsersFromRecord(record: IInventoryRecordV2, schema: IAttributeDetailV2[]): string[] {
        const userMap: IUserMap = getAllUserModels(this.store.getState());
        const userAttributes = filter(schema, (attribute: IAttributeDetailV2): boolean => attribute.attributeType === AttributeTypes.Users);
        const recordUserIds = map(userAttributes, (attr: IAttributeDetailV2): string => {
            return record[attr.fieldName] && (record[attr.fieldName] as Array<IValueId<string>>).length ? record[attr.fieldName][0].id : "";
        });
        return filter(recordUserIds, (id: string): boolean => id && !userMap[id]);
    }

    private getUnknownUsersFromRecordList(records: IInventoryRecordV2[], columns: IInventoryListV2Column[]): string[] {
        const userMap: IUserMap = getAllUserModels(this.store.getState());
        const userColumns = columns.filter((column: IInventoryListV2Column): boolean => column.attributeType === AttributeTypes.Users);
        let recordListUserIds: string[] = [];
        records.forEach((record: IInventoryRecordV2) => {
            const recordUserIds: string[] = userColumns.map((column: IInventoryListV2Column): string => {
                return record[column.fieldName] && (record[column.fieldName] as Array<IValueId<string>>).length && !recordListUserIds.includes(record[column.fieldName][0].id) ? record[column.fieldName][0].id : "";
            });
            recordListUserIds = recordListUserIds.concat(recordUserIds);
        });
        return recordListUserIds.filter((id: string): boolean => id && !userMap[id]);
    }
}
