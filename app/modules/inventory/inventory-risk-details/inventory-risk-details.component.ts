// Angular
import {
    Component,
    OnInit,
    ViewChild,
} from "@angular/core";

// Constants/Enums
import {
    InventoryDetailsTabs,
    InventoryListRoutes,
    InventoryDetailRoutes,
} from "constants/inventory.constant";
import {
    InventoryTypeTranslations,
    NewInventorySchemaIds,
} from "constants/inventory-config.constant";
import {
    InventoryTableIds,
    InventoryRiskControlsTabColumnNames,
} from "enums/inventory.enum";
import { TableColumnTypes } from "enums/data-table.enum";
import { GridViewStorageNames } from "sharedModules/enums/grid-view-storage.enum";
import { RiskV2SourceTypes } from "enums/riskV2.enum";
import { InventoryRiskDetailsRoutes } from "constants/inventory.constant";

// Interface
import { ITabsNav } from "interfaces/one-tabs-nav.interface";
import { IStringMap } from "interfaces/generic.interface";
import { IRiskDetails } from "interfaces/risk.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";
import { INumberMap } from "interfaces/generic.interface";
import { IInventoryControlsTableRow } from "interfaces/inventory.interface";
import {
    IGridPaginationParams,
    IGridSort,
} from "interfaces/grid.interface";
import {
    IPageableMeta,
    IPageableOf,
} from "interfaces/pagination.interface";
// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

// Service
import { StateService } from "@uirouter/angularjs";
import { RiskAPIService } from "oneServices/api/risk-api.service";
import { RiskControlsFilterService } from "modules/risks/risk-controls-list/risk-controls-filter/risk-controls-filter.service";

// Components
import { RiskControlsListComponent } from "modules/risks/risk-controls-list/risk-controls-list.component";

export const RiskDetailsTabs = {
    Details: "details",
    Controls: "controls",
    Activity: "activity",
};

@Component({
    selector: "inventory-risk-details",
    templateUrl: "./inventory-risk-details.component.html",
})

export class InventoryRiskDetailsComponent implements OnInit {

    @ViewChild(RiskControlsListComponent) riskControlsListComponent: RiskControlsListComponent;

    currentTab: string;
    inventoryName: string;
    inventoryTranslationKey: string;
    pageUrl: string;
    isLoadingDetails: boolean;
    riskDetails: IRiskDetails;
    riskId: string;
    searchString: string;
    tabs: ITabsNav[];
    tabIds: IStringMap<string> = RiskDetailsTabs;
    isFilterOpen: boolean;
    tableColumnTypes = TableColumnTypes;
    storageKey = GridViewStorageNames.INVENTORY_RISK_CONTROLS_FILTER_LIST;
    currentFilters = [];
    query = [];
    searchText: string;
    sortData: IGridSort;
    tableData: IInventoryControlsTableRow[] = [];
    isPageloading = true;
    defaultPagination: IGridPaginationParams = { page: 0, size: 20 };
    paginationParams = this.defaultPagination;
    paginationDetail: IPageableMeta;
    filterOptions = [
        { sortKey: InventoryRiskControlsTabColumnNames.FRAMEWORK, type: this.tableColumnTypes.Text, name: this.translatePipe.transform("Framework"), cellValueKey: "frameworkName", filterKey: "frameworkId"},
        { sortKey: InventoryRiskControlsTabColumnNames.CATEGORY, type: this.tableColumnTypes.Text, name: this.translatePipe.transform("Category"), cellValueKey: "categoryName", filterKey: "categoryId"},
        { sortKey: InventoryRiskControlsTabColumnNames.STATUS, type: this.tableColumnTypes.Text, name: this.translatePipe.transform("Status"), cellValueKey: "status", filterKey: "status"},
    ];

    private inventoryId: number;
    private inventoryTranslationKeys: INumberMap<string> = InventoryTypeTranslations;
    private recordId: string;

    constructor(
        private riskAPI: RiskAPIService,
        readonly stateService: StateService,
        private riskControlsFilterService: RiskControlsFilterService,
        private translatePipe: TranslatePipe,
    ) { }

    ngOnInit(): void {
        this.currentTab = this.stateService.params.tabId || this.tabIds.Details;
        this.inventoryId = Number.parseInt(this.stateService.params.inventoryId, 10);
        this.inventoryName = this.stateService.params.inventoryName;
        this.inventoryTranslationKey = this.inventoryTranslationKeys[this.inventoryId];
        this.pageUrl = InventoryRiskDetailsRoutes[this.inventoryId];
        this.recordId = this.stateService.params.recordId;
        this.riskId = this.stateService.params.riskId;
        this.fetchRiskDetailsById();
        this.currentFilters = this.riskControlsFilterService.loadFilters(this.storageKey);
        this.isFilterOpen = false;
    }

    fetchRiskDetailsById() {
        this.isLoadingDetails = true;
        this.riskAPI.fetchRiskV2ById(this.riskId)
            .then((res: IProtocolResponse<IRiskDetails>) => {
                if (res.result) {
                    this.riskDetails = res.data;
                    if (this.riskDetails.sourceType === RiskV2SourceTypes[RiskV2SourceTypes.PIA]) {
                        if (this.currentTab === this.tabIds.Controls) {
                            this.getInventoryRiskControls();
                        } else {
                            this.currentTab = this.tabIds.Details;
                        }
                        this.tabs = this.getTabs(true);
                    } else {
                        this.currentTab = this.tabIds.Details;
                        this.tabs = this.getTabs();
                    }
                }
                this.isLoadingDetails = false;
            });
    }

    goToListView(route: { path: string, params: any }) {
        switch (route.path) {
            case "/inventory-list":
                const formatedInventoryId = this.inventoryId === InventoryTableIds.Vendors ? this.inventoryId : NewInventorySchemaIds[this.inventoryId];
                this.stateService.go(InventoryListRoutes[this.inventoryId], {
                    Id: formatedInventoryId,
                    page: null,
                    size: null,
                });
                break;
            case "/inventory-detail":
                this.stateService.go(InventoryDetailRoutes[this.inventoryId], {
                    recordId: this.recordId,
                    inventoryId: this.inventoryId,
                    tabId: InventoryDetailsTabs.Risks,
                });
                break;
        }
    }

    toggleFilterDrawer(eventData?: boolean) {
        this.isFilterOpen = !this.isFilterOpen;
    }

    filterApplied(filterData) {
        this.query = this.riskControlsFilterService.buildQuery(filterData);
        this.getInventoryRiskControls();
    }

    handleSortChange(sortData: IGridSort) {
        this.sortData = sortData;
        this.paginationParams.sort = `${sortData.sortedKey},${sortData.sortOrder}`;
        this.getInventoryRiskControls();
    }

    handleSearch(searchText: string) {
        this.searchText = searchText;
        this.getInventoryRiskControls();
    }

    pageChanged(page: number) {
        this.fetchPage(page - 1);
    }

    fetchPage(page: number) {
        if (this.paginationDetail.number !== page) {
            this.paginationParams.page = page;
            this.getInventoryRiskControls();
        }
    }

    getInventoryRiskControls() {
        this.isPageloading = true;
        this.tableData = [];
        this.paginationDetail = null;
        this.riskAPI.fetchInventoryRiskControl(this.riskId, this.paginationParams, this.searchText, this.query)
            .subscribe((response: IProtocolResponse<IPageableOf<IInventoryControlsTableRow>>) => {
                if (response.result) {
                    this.currentTab = this.tabIds.Controls;
                    this.tableData = response.data.content;
                    this.paginationDetail = {
                        first: response.data.first,
                        last: response.data.last,
                        number: response.data.number,
                        numberOfElements: response.data.numberOfElements,
                        size: response.data.size,
                        sort: response.data.sort,
                        totalElements: response.data.totalElements,
                        totalPages: response.data.totalPages,
                    };
                }
                this.isPageloading = false;
            });
    }

    private getTabs(addControlsTab = false): ITabsNav[] {
        const tabs = [
            {
                id: this.tabIds.Details,
                text: this.translatePipe.transform("Details"),
                action: (option: ITabsNav): void => { this.goToTab(option.id); },
            },
        ];

        if (addControlsTab) {
            tabs.push({
                id: this.tabIds.Controls,
                text: this.translatePipe.transform("Controls"),
                action: (option: ITabsNav): void => { this.goToTab(option.id); },
            });
        }

        tabs.push({
            id: this.tabIds.Activity,
            text: this.translatePipe.transform("Activity"),
            action: (option: ITabsNav): void => { this.goToTab(option.id); },
        });

        return tabs;
    }

    private goToTab(tabId: string): void {
        this.currentTab = tabId;
        if (tabId === this.tabIds.Controls) {
            this.getInventoryRiskControls();
        }
    }
}
