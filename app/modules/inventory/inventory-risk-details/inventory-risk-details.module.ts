// Angular
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

// Modules
import { SharedModule } from "sharedModules/shared.module";
import { VitreusModule } from "@onetrust/vitreus";
import { OTPipesModule } from "modules/pipes/pipes.module";
import { RiskDetailsFormModule } from "modules/risks/risk-details-form/risk-details-form.module";
import { RiskControlsListModule } from "modules/risks/risk-controls-list/risk-controls-list.module";
import { RiskActivityModule } from "modules/risks/risk-activities/risk-activity.module";

// Components
import { InventoryRiskDetailsComponent } from "./inventory-risk-details.component";

@NgModule({
    imports: [
        SharedModule,
        CommonModule,
        VitreusModule,
        OTPipesModule,
        RiskDetailsFormModule,
        RiskControlsListModule,
        RiskActivityModule,
    ],
    declarations: [
        InventoryRiskDetailsComponent,
    ],
    entryComponents: [
        InventoryRiskDetailsComponent,
    ],
})
export class InventoryRiskDetailsModule {}
