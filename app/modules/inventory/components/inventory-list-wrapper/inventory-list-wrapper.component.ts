// 3rd Party
import { Component } from "@angular/core";
import { StateService } from "@uirouter/core";

// Constants
import {
    InventoryPermissions,
} from "constants/inventory.constant";
import {
    InventorySchemaIds,
    NewInventorySchemaIds,
} from "constants/inventory-config.constant";

@Component({
    selector: "inventory-list-wrapper",
    template: `
        <ng-container *ngIf="(inventoryPermissions[inventoryId].inventoryLinkingV2 | otPermit) && inventoryId !== inventorySchemaIds.Elements; else V1ListView">
            <inventory-list-v2></inventory-list-v2>
        </ng-container>
        <ng-template #V1ListView>
            <inventory-list></inventory-list>
        </ng-template>
    `,
})
export class InventoryListWrapper {

    inventoryId: string;
    inventorySchemaIds = InventorySchemaIds;
    inventoryPermissions = InventoryPermissions;

    constructor(private stateService: StateService) {}

    ngOnInit() {
        this.inventoryId = NewInventorySchemaIds[this.stateService.params.Id] || this.stateService.params.Id;
    }
}
