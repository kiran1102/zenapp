import {
    Component,
    OnInit,
    Inject,
    Input,
    ViewChild,
} from "@angular/core";

// Rxjs
import { Subject } from "rxjs";
import {
    startWith,
    takeUntil,
} from "rxjs/operators";

// Redux
import observableFromStore from "oneRedux/utils/observable-from-store";
import {
    getIsLoadingAttachments,
    getAttachmentList,
} from "oneRedux/reducers/attachment.reducer";

// Services
import { AttachmentActionService } from "oneServices/actions/attachment-action.service";

// Tokens
import { StoreToken } from "tokens/redux-store.token";

// Enums + Constants
import {
    InventoryTableIds,
    DefaultTableConfig,
} from "enums/inventory.enum";
import { AttachmentTypes } from "enums/attachment-types.enum";
import { InventoryDetailsTabs } from "constants/inventory.constant";

// Interfaces
import { IAttachmentFileResponse } from "interfaces/attachment-file.interface";
import { IStringMap } from "interfaces/generic.interface";
import { IPageableOf } from "interfaces/pagination.interface";
import { IAttachmentModalResolve } from "interfaces/attachment-modal-resolve.interface";
import { IViewMoreParams } from "interfaces/inventory.interface";
import {
    IStoreState,
    IStore,
} from "interfaces/redux.interface";
import { InventoryAttachmentsComponent } from "modules/inventory/components/inventory-attachments/inventory-attachments.component";

@Component({
    selector: "inventory-attachments-wrapper",
    templateUrl: "inventory-attachments-wrapper.component.html",
})
export class InventoryAttachmentsWrapperComponent implements OnInit {

    @ViewChild(InventoryAttachmentsComponent) inventoryAttachmentsComponent: InventoryAttachmentsComponent;
    @Input() recordId: string;
    @Input() inventoryId: number;
    @Input() permissions: IStringMap<boolean>;
    @Input() tabId: string;

    viewMore = true;
    contractViewMore = false;
    isAddDocumentDisable = false;
    showSupportingDocuments = true;
    isLoadingAttachments = true;
    isContractsShown = false;
    attachmentList: IPageableOf<IAttachmentFileResponse>;
    tabIds: IStringMap<string> = InventoryDetailsTabs;

    private destroy$ = new Subject();

    constructor(
        @Inject(StoreToken) private store: IStore,
        private attachmentAction: AttachmentActionService,
    ) { }

    ngOnInit() {
        this.attachmentAction.setCurrentRefId(this.recordId);
        observableFromStore(this.store).pipe(
            startWith(this.store.getState()),
            takeUntil(this.destroy$),
        ).subscribe((state: IStoreState) => this.componentWillReceiveState(state));
        this.isContractsShown = this.getContractPermission();
        if (this.permissions.canViewAttachments) {
            this.attachmentAction.fetchAttachmentList(this.inventoryId, 0, DefaultTableConfig.defaultSize);
        }
        if (this.tabId === InventoryDetailsTabs.Documents) {
            this.showSupportingDocuments = true;
            this.isContractsShown = true;
        }
    }

    ngOnDestroy() {
        this.attachmentAction.setDefaultList();
        this.destroy$.next();
        this.destroy$.complete();
    }

    handleDisplayContracts(viewMoreParam: IViewMoreParams) {
        this.isContractsShown = this.getContractPermission() && viewMoreParam.showContract;
        if (viewMoreParam.tableParam) {
            this.viewMore = viewMoreParam.showContract;
            this.attachmentAction.fetchAttachmentList(this.inventoryId, viewMoreParam.tableParam.page, viewMoreParam.tableParam.size);
        }
    }

    handleAddFromHeader(documentParams: IViewMoreParams) {
        if (documentParams.showContract) {
            this.viewMore = documentParams.showContract;
            this.addAttachment(this.recordId, () => this.attachmentAction.fetchAttachmentList(this.inventoryId, 0, this.viewMore ? DefaultTableConfig.expandedSize : DefaultTableConfig.defaultSize));
        } else {
            this.addAttachment(this.recordId, () => this.attachmentAction.fetchAttachmentList(this.inventoryId, documentParams.tableParam.page, documentParams.tableParam.size));
        }
    }

    handleAddContract(isAddDocumentDisable: boolean) {
        this.isAddDocumentDisable = isAddDocumentDisable;
    }

    hideInventoryAttachments(show: boolean) {
        this.contractViewMore = show;
        this.showSupportingDocuments = !show;
    }

    addDocument() {
        this.inventoryAttachmentsComponent.addDocument();
    }

    private addAttachment(recordId: string, callback?: () => void) {
        const attachmentModalData: IAttachmentModalResolve = {
            showFileList: true,
            allowAttachmentNaming: true,
            allowComments: true,
            attachmentType: AttachmentTypes.Inventory,
            requestId: recordId,
            inventoryId: this.inventoryId,
            callback: callback as any,
        };
        this.attachmentAction.addAttachment(attachmentModalData);
    }

    private componentWillReceiveState(state: IStoreState) {
        this.isLoadingAttachments = getIsLoadingAttachments(state);
        this.attachmentList = getAttachmentList(state);
        if (this.getContractPermission() && this.attachmentList && this.attachmentList.totalElements <= DefaultTableConfig.defaultSize) this.isContractsShown = true;
    }

    private getContractPermission(): boolean {
        return (this.permissions.canViewDocuments && this.inventoryId === InventoryTableIds.Vendors) || this.permissions.canViewVendorContractInventoryLink;
    }
}
