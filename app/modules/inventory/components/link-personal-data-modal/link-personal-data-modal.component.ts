// Rxjs
import { Subject } from "rxjs";
import { take } from "rxjs/operators";

// 3rd Party
import {
    Component,
    OnInit,
} from "@angular/core";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

// Const
import {
    InventoryListTypes,
    InventoryTranslationKeys,
} from "constants/inventory.constant";

// Interfaces
import {
    ILinkPersonalData,
    IInventoryListItem,
    IPersonalDataItem,
    IFormattedListItem,
} from "interfaces/inventory.interface";
import { IPaginatedResponse } from "interfaces/pagination.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";
import { IStringMap } from "interfaces/generic.interface";
import { IRelateInventoryModalResolve } from "interfaces/relate-inventory-modal.interface";
import { IOtModalContent } from "@onetrust/vitreus";

// Services
import { InventoryService } from "inventorySharedModule/shared/services/inventory-api.service";

@Component({
    selector: "link-personal-data-modal",
    templateUrl: "./link-personal-data-modal.component.html",
})
export class LinkPersonalDataModal implements OnInit, IOtModalContent {
    inventoryTranslationKeys = InventoryTranslationKeys;
    submitButtonText: string;
    cancelButtonText: string;
    selectedSubjectType: IFormattedListItem;
    orgGroupId: string;
    linkedType: string;
    savedSubjectType = false;
    subjectCardSelections: IStringMap<boolean> = {};
    selectedType: IFormattedListItem;
    addingSubjectType = false;
    subjectSearchTerm = "";
    elementSearchTerm = "";
    currentRecordElementMap: IStringMap<IStringMap<IStringMap<boolean>>> = {};
    hasPrepopulatedElements = false;
    elementMap: IStringMap<IStringMap<IStringMap<boolean>>> = {};
    elementsToAdd: ILinkPersonalData[] = [];
    elementsToRemove: Array<{ dataElement: { id: string }, dataSubjectType: { id: string }}> = [];
    isSaving = false;
    dataSubjectTypeList: IFormattedListItem[] = [];
    currentDataSubjectTypeList: IFormattedListItem[] = [];
    dataCategoryList: IFormattedListItem[] = [];
    filteredCategoryList: IFormattedListItem[] = [];
    recordId: string;
    recordName: string;
    fetchingCategories = false;
    fetchingSubjectTypes = true;
    fetchingCurrentElements = true;
    hasChanges = false;
    subjectsSelectedCount: number;
    elementsSelectedCount: number;
    hasCardSelections = false;
    otModalCloseEvent: Subject<null>;
    otModalData: IRelateInventoryModalResolve;

    constructor(
        private inventoryService: InventoryService,
        private translatePipe: TranslatePipe,
    ) { }

    ngOnInit() {
        this.recordId = this.otModalData.recordId;
        this.linkedType = this.otModalData.linkedType;
        this.submitButtonText = this.otModalData.submitButtonText || this.translatePipe.transform("Update");
        this.cancelButtonText = this.otModalData.cancelButtonText || this.translatePipe.transform("Cancel");
        this.recordName = this.otModalData.recordName;
        this.orgGroupId = this.otModalData.orgGroupId;
        this.inventoryService.getRelatedPersonalData(this.recordId, {}).pipe(take(1)).subscribe((data: IProtocolResponse<IPaginatedResponse<IPersonalDataItem>>) => {
            this.currentRecordElementMap = this.setPrepopulatedElementMap(data.data.data);
            this.hasPrepopulatedElements = Boolean(Object.keys(this.currentRecordElementMap).length);
            this.elementMap = this.setPrepopulatedElementMap(data.data.data);
            this.elementsSelectedCount = this.setElementSelectionCount();
            this.fetchingCurrentElements = false;
            this.getDataSubjectTypeList();
        });
    }

    trackById(index: number, item: IFormattedListItem | IInventoryListItem) {
        return item.id;
    }

    selectSubjectType(selection: IFormattedListItem) {
        if (this.selectedSubjectType !== selection) {
            this.selectedSubjectType = selection;
            this.getDataCategoryList(this.selectedSubjectType.id);
            this.elementSearchTerm = "";
        }
    }

    selectAllCategoryElements(currentCategory: IFormattedListItem) {
        const selectedCategory = this.filteredCategoryList.find((categoryItem) => categoryItem.id === currentCategory.id);
        const checkStatus: boolean = !selectedCategory.allSelected;
        const currentElements: string[] = selectedCategory.dataElements.map((element) => element.id);
        currentCategory.dataElements.forEach((element: IInventoryListItem) => {
            this.elementMap[this.selectedSubjectType.id][currentCategory.id][element.id] = checkStatus;
        });
        Object.keys(this.elementMap[this.selectedSubjectType.id]).forEach((category) => {
            const elements = Object.keys(this.elementMap[this.selectedSubjectType.id][category]);
            if (currentElements.some((element) => elements.indexOf(element) >= 0)) {
                Object.keys(this.elementMap[this.selectedSubjectType.id][category]).forEach((element) => {
                    if (currentElements.indexOf(element) >= 0) {
                        this.elementMap[this.selectedSubjectType.id][category][element] = checkStatus;
                    }
                });
            }
        });
        this.setElementStatus();
    }

    selectAllSubjectElements() {
        this.filteredCategoryList.forEach((category) => {
            category.dataElements.forEach((element: IInventoryListItem) => {
                this.elementMap[this.selectedSubjectType.id][category.id][element.id] = !this.selectedSubjectType.allSelected;
            });
        });
        this.setElementStatus();
    }

    toggleElement(currentCategoryId: string, currentElementId: string) {
        this.elementMap[this.selectedSubjectType.id][currentCategoryId][currentElementId] = !this.elementMap[this.selectedSubjectType.id][currentCategoryId][currentElementId];
        Object.keys(this.elementMap[this.selectedSubjectType.id]).forEach((categoryId: string) => {
            if (this.elementMap[this.selectedSubjectType.id][categoryId].hasOwnProperty(currentElementId)) {
                this.elementMap[this.selectedSubjectType.id][categoryId][currentElementId] = this.elementMap[this.selectedSubjectType.id][currentCategoryId][currentElementId];
            }
        });
        this.setElementStatus();
    }

    setElementStatus() {
        this.setSelectionStatus();
        this.subjectsSelectedCount = this.setSubjectSelectionCount();
        this.elementsSelectedCount = this.setElementSelectionCount();
        this.setAddRemoveElements();
    }

    closeModal() {
        this.otModalCloseEvent.next();
        this.otModalCloseEvent.complete();
    }

    setSubjectSelectionCount(): number {
        return this.currentDataSubjectTypeList.map((subjectType) => subjectType.hasSelections).filter((item) => item).length;
    }

    setElementSelectionCount(): number {
        let elementsSelectedStatus: boolean[] = [];
        Object.keys(this.elementMap).forEach((subjectId) => {
            Object.keys(this.elementMap[subjectId]).forEach((categoryId) => {
                elementsSelectedStatus = [...Object.values(this.elementMap[subjectId][categoryId]), ...elementsSelectedStatus];
            });
        });
        return elementsSelectedStatus.filter((status) => status).length;
    }

    saveInventory() {
        this.isSaving = true;
        const promises = [];
        if (this.elementsToAdd.length) promises.push(this.inventoryService.linkPersonalData(this.recordId, this.elementsToAdd).toPromise());
        if (this.elementsToRemove.length) promises.push(this.inventoryService.removePersonalDataLink(this.recordId, this.elementsToRemove).toPromise());
        Promise.all(promises).then(() => {
            if (this.otModalData.callback) {
                this.otModalData.callback();
            }
            this.closeModal();
            this.isSaving = false;
        });
    }

    saveSelectedSubjectType() {
        Object.keys(this.subjectCardSelections).forEach((subjectId) => {
            if (this.subjectCardSelections[subjectId]) {
                this.currentDataSubjectTypeList.push(this.dataSubjectTypeList.find((subject) => subjectId === subject.id));
            }
        });
        this.selectedSubjectType = this.currentDataSubjectTypeList[0];
        this.savedSubjectType = true;
        this.getDataCategoryList(this.selectedSubjectType.id);
        this.subjectSearchTerm = "";
    }

    selectType({ currentValue }) {
        this.selectedType = currentValue;
    }

    handleLookupSearch({ key, value }) {
        this.subjectSearchTerm = value;
    }

    selectDataSubjectTypCard(subjectType: IFormattedListItem) {
        this.subjectCardSelections[subjectType.id] = !this.subjectCardSelections[subjectType.id];
        this.hasCardSelections = Object.values(this.subjectCardSelections).includes(true);
    }

    handleSubjectSearch(searchTerm: string) {
        this.subjectSearchTerm = searchTerm;
    }

    handleElementSearch(searchTerm: string) {
        this.filteredCategoryList = JSON.parse(JSON.stringify(this.dataCategoryList));
        this.elementSearchTerm = searchTerm;
        if (searchTerm) {
            const filteredArray: IFormattedListItem[] = [];
            const term: string = searchTerm.toLocaleLowerCase();
            this.filteredCategoryList.forEach((category: IFormattedListItem) => {
                const categoryHasMatch: boolean = category.name.toLocaleLowerCase().indexOf(term) !== -1;
                const elementsMatch: IInventoryListItem[] = category.dataElements.filter((elements: IInventoryListItem): boolean => {
                    return elements.name.toLocaleLowerCase().indexOf(term) !== -1;
                });
                if (categoryHasMatch || elementsMatch.length) {
                    if (elementsMatch.length && !categoryHasMatch) {
                        category.dataElements = elementsMatch;
                    }
                    filteredArray.push(category);
                }
            });
            this.filteredCategoryList = filteredArray;
        }
        this.setSelectionStatus();
    }

    setAddingSubjectType(addingSubjectType: boolean) {
        this.selectedType = undefined;
        this.addingSubjectType = addingSubjectType;
    }

    setSelectedSubjectTypes(selectedType: IFormattedListItem) {
        if (this.currentDataSubjectTypeList.includes(selectedType)) {
            const subjectIndex = this.currentDataSubjectTypeList.indexOf(selectedType);
            this.currentDataSubjectTypeList.splice(subjectIndex, 1);
            if (this.selectedSubjectType === selectedType && this.currentDataSubjectTypeList.length) {
                this.selectSubjectType(this.currentDataSubjectTypeList[0]);
            } else if (!this.currentDataSubjectTypeList.length) {
                this.selectedSubjectType = undefined;
            }
        } else {
            this.currentDataSubjectTypeList = [selectedType, ...this.currentDataSubjectTypeList];
            this.addingSubjectType = false;
            this.selectSubjectType(selectedType);
        }
        this.selectedType = undefined;
    }

    private setPrepopulatedElementMap(elements: IPersonalDataItem[]): IStringMap<IStringMap<IStringMap<boolean>>> {
        const currentRecordElementMap = {};
        elements.forEach((element) => {
            if (element.dataSubjectType) {
                if (!currentRecordElementMap.hasOwnProperty(element.dataSubjectType.id)) currentRecordElementMap[element.dataSubjectType.id] = {};
                if (element.dataCategories && element.dataCategories.length) {
                    element.dataCategories.forEach((category) => {
                        if (!currentRecordElementMap[element.dataSubjectType.id].hasOwnProperty(category.id)) currentRecordElementMap[element.dataSubjectType.id][category.id] = {};
                        currentRecordElementMap[element.dataSubjectType.id][category.id][element.dataElement.id] = true;
                    });
                }
            }
        });
        return currentRecordElementMap;
    }

    private setElementMapForCategory(categoryList: IInventoryListItem[]): IStringMap<IStringMap<IStringMap<boolean>>> {
        const elementMap = { ...this.elementMap };
        categoryList.forEach((category) => {
            if (elementMap[this.selectedSubjectType.id] === undefined) elementMap[this.selectedSubjectType.id] = {};
            category.dataElements.forEach((element) => {
                if (elementMap[this.selectedSubjectType.id][category.id] === undefined) elementMap[this.selectedSubjectType.id][category.id] = {};
                elementMap[this.selectedSubjectType.id][category.id][element.id] = Boolean(elementMap[this.selectedSubjectType.id][category.id][element.id]);
            });
        });
        return elementMap;
    }

    private setAddRemoveElements() {
        this.elementsToAdd = this.formatLinkData();
        this.elementsToRemove = this.formatRemoveLinkData();
        this.hasChanges = Boolean(this.elementsToAdd.length || this.elementsToRemove.length);
    }

    private getDataSubjectTypeList() {
        this.fetchingSubjectTypes = true;
        this.inventoryService.getListData(InventoryListTypes.DataSubjectTypes).then((response: IProtocolResponse<{data: IInventoryListItem[] }>) => {
            this.dataSubjectTypeList = this.formatDataSubjectList(response.data.data);
            this.setSelectionCardMap();
            this.subjectsSelectedCount = this.setSubjectSelectionCount();
            if (this.dataSubjectTypeList[0] && this.hasPrepopulatedElements) {
                Object.keys(this.currentRecordElementMap).forEach((subjectId: string) => {
                    const subjectType = this.dataSubjectTypeList.find((subject: IFormattedListItem): boolean => {
                        return subject.id === subjectId;
                    });
                    if (!this.currentDataSubjectTypeList.includes(subjectType)) {
                        this.currentDataSubjectTypeList = [subjectType, ...this.currentDataSubjectTypeList];
                    }
                });
                this.selectSubjectType(this.currentDataSubjectTypeList[0]);
            }
            this.fetchingSubjectTypes = false;
        });
    }

    private setSelectionCardMap() {
        this.dataSubjectTypeList.forEach((subject) => {
            this.subjectCardSelections[subject.id] = false;
        });
    }

    private formatDataSubjectList(list: IInventoryListItem[]): IFormattedListItem[] {
        list.map((item: IFormattedListItem) => {
            item.name = this.translatePipe.transform(item.nameKey);
            item.hasSelections = Object.keys(this.currentRecordElementMap).includes(item.id);
        });
        return list;
    }

    private getDataCategoryList(subjectId: string) {
        this.fetchingCategories = true;
        this.inventoryService.getDataSubjectDetails(subjectId, this.orgGroupId).toPromise().then((response) => {
            this.elementMap = this.setElementMapForCategory(response.data.data);
            this.dataCategoryList = this.setformattedCategoryList(response.data.data);
            this.filteredCategoryList = this.dataCategoryList;
            this.setElementStatus();
            this.fetchingCategories = false;
        });
    }

    private setSelectionStatus() {
        this.filteredCategoryList.forEach((category) => {
            const elementSelectionStatus: boolean[] = [];
            category.dataElements.forEach((element) => {
                elementSelectionStatus.push(this.elementMap[this.selectedSubjectType.id][category.id][element.id]);
            });
            category.hasSelections = elementSelectionStatus.includes(true);
            category.allSelected = !elementSelectionStatus.includes(false);
        });
        const hasSelections = this.filteredCategoryList.map((category) => category.hasSelections);
        this.selectedSubjectType.hasSelections = hasSelections.includes(true);
        this.selectedSubjectType.allSelected = this.filteredCategoryList.every((category) => category.allSelected === true);
    }

    private setformattedCategoryList(categoryList: IInventoryListItem[]): IFormattedListItem[] {
        const formattedList: IFormattedListItem[] = categoryList.map((category) => {
            category.name = category.nameKey ? this.translatePipe.transform(category.nameKey) : category.name;
            (category as IFormattedListItem).allSelected = !Boolean(Object.values(this.elementMap[this.selectedSubjectType.id][category.id]).includes(false));
            category.dataElements.forEach((element) => {
                element.name = element.nameKey ? this.translatePipe.transform(element.nameKey) : element.name;
            });
            return category;
        });
        return formattedList;
    }

    private formatRemoveLinkData(): Array<{ dataElement: { id: string }, dataSubjectType: { id: string }}> {
        const unlinkData: Array<{ dataElement: { id: string }, dataSubjectType: { id: string }}>  = [];
        Object.keys(this.currentRecordElementMap).forEach((subjectId) => {
            Object.keys(this.currentRecordElementMap[subjectId]).forEach((categoryId) => {
                Object.keys(this.currentRecordElementMap[subjectId][categoryId]).forEach((elementId) => {
                    if (!(this.elementMap[subjectId] && this.elementMap[subjectId][categoryId] && this.elementMap[subjectId][categoryId][elementId])) {
                        unlinkData.push({ dataElement: { id: elementId }, dataSubjectType: { id: subjectId }});
                    }
                });
            });
        });
        return unlinkData;
    }

    private formatLinkData(): ILinkPersonalData[] {
        const formattedData: ILinkPersonalData[] = [];
        Object.keys(this.elementMap).forEach((subjectId) => {
            Object.keys(this.elementMap[subjectId]).forEach((categoryId) => {
                Object.keys(this.elementMap[subjectId][categoryId]).forEach((elementId) => {
                    if (this.elementMap[subjectId][categoryId][elementId] &&
                        (!this.currentRecordElementMap[subjectId] ||
                        !this.currentRecordElementMap[subjectId][categoryId] ||
                        this.currentRecordElementMap[subjectId][categoryId][elementId] === undefined)) {
                            formattedData.push({
                                dataElement: { id: elementId },
                                dataSubjectType: { id: subjectId },
                            });
                    }
                });
            });
        });
        return formattedData;
    }
}
