// 3rd Party
import {
    Component,
    Input,
    Output,
    OnInit,
    OnChanges,
    OnDestroy,
    Inject,
    ElementRef,
    EventEmitter,
} from "@angular/core";
import { StateService } from "@uirouter/core";
import {
    map,
    find,
    filter,
    isNull,
    toLower,
    isEmpty,
    isUndefined,
} from "lodash";

// Rxjs
import { Subject } from "rxjs";
import {
    startWith,
    takeUntil,
} from "rxjs/operators";

// Redux
import { getCurrentRecordOrgId } from "oneRedux/reducers/inventory.reducer";
import { getOrgById } from "oneRedux/reducers/org.reducer";
import observableFromStore from "oneRedux/utils/observable-from-store";
import { StoreToken } from "tokens/redux-store.token";

// Services
import { getCommaSeparatedList } from "sharedServices/string-helper.service";
import { InventoryService } from "inventorySharedModule/shared/services/inventory-api.service";
import { InventoryActionService } from "inventoryModule/services/action/inventory-action.service";
import { LookupService } from "sharedModules/services/helper/lookup.service";
import Utilities from "Utilities";

// Enum + Consts
import {
    InputTypes,
    OptionStates,
    InventoryTableIds,
    OptionTypes,
} from "enums/inventory.enum";
import { OrgUserDropdownTypes, OrgUserTraversal } from "enums/org-user-dropdown.enum";
import {
    NamesAttributes,
    InventoryAttributeCodes,
} from "constants/inventory.constant";
import { GUID } from "constants/regex.constant";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

// Interfaces
import {
    IInventoryCell,
    IAttribute,
    IAttributeValue,
    IInventoryCellValue,
    IInventoryRecord,
} from "interfaces/inventory.interface";
import { IOrgUserAdapted } from "interfaces/org-user.interface";
import { IStore, IStoreState } from "interfaces/redux.interface";
import { IOtOrgUserOutput } from "sharedModules/components/ot-org-user/ot-org-user.interface";
import { ISelection } from "interfaces/generic.interface";

interface ISingleSelectModel extends IAttributeValue {
    InputValue: string;
    IsPlaceholder: boolean;
}

interface IMultiSelectModel extends IAttributeValue {
    IsPlaceholder: boolean;
    isTag: boolean;
    oneOther: string;
}

@Component({
    selector: "inventory-attribute-cell",
    templateUrl: "./inventory-attribute-cell.component.html",
})
export class InventoryAttributeCell implements OnInit, OnChanges, OnDestroy {

    @Input() cell: IInventoryCell;
    @Input() record: IInventoryRecord;
    @Input() attribute: IAttribute;
    @Input() isEditing: boolean;
    @Input() isDisabled: boolean;
    @Input() inventoryType: number;
    @Input() canEdit: boolean;
    @Input() forceChange: number | Date;
    @Input() disabledTooltip: string;
    @Input() backgroundClass: string;
    @Input() hideErrors: boolean;
    @Input() isParentModal: boolean;
    @Input() isDetailsPage: boolean;
    @Output() cellChange = new EventEmitter<{ cell: IInventoryCell }>();
    @Output() disabledClickAction: ({ dataFromCell: string }: any) => void;
    @Output() toggleEdit = new EventEmitter();

    inputTypes = InputTypes;
    OrgUserDropdownTypes = OrgUserDropdownTypes;
    orgUserTraversal = OrgUserTraversal;
    borderClass: string;
    scrollParent: Element;
    isHovering: boolean;
    emptyOptionLast: boolean;
    multiSelectModel: IAttributeValue[];
    options: IAttributeValue[] = [];
    isNameAttribute: boolean;
    isStatusAttribute: boolean;
    isUserAttribute: boolean;
    isOrgAttribute: boolean;
    userModel: string;
    currentOrgId: string;
    filteredOptions: IInventoryCellValue[];
    singleSelectOptions: IInventoryCellValue[];

    private destroy$ = new Subject();

    constructor(
        private stateService: StateService,
        @Inject(StoreToken) private store: IStore,
        private translatePipe: TranslatePipe,
        private element: ElementRef,
        private inventoryService: InventoryService,
        private inventoryAction: InventoryActionService,
        private lookupService: LookupService,
    ) { }

    ngOnInit() {
        const scrollEl: HTMLCollectionOf<Element> = document.getElementsByClassName("inventory-details__container");
        this.scrollParent = scrollEl ? scrollEl[0] : null;
        this.isNameAttribute = this.cell.attributeCode === NamesAttributes[this.attribute.inventoryTypeId];
        this.isStatusAttribute = this.cell.attributeCode === InventoryAttributeCodes[this.attribute.inventoryTypeId].Status;
        this.isUserAttribute = this.attribute.fieldAttributeTypeId === OptionTypes.Users;
        this.isOrgAttribute = this.attribute.fieldAttributeTypeId === OptionTypes.Orgs;
        setTimeout(() => {
            this.setDisabledTooltip();
        }, 0);
        this.emptyOptionLast = this.attribute.fieldResponseTypeId === this.inputTypes.Select ? Boolean(this.cell.values && this.cell.values.length) : false;
        if (this.isUserAttribute) {
            observableFromStore(this.store).pipe(
                startWith(this.store.getState()),
                takeUntil(this.destroy$),
            ).subscribe((state: IStoreState) => this.currentOrgId = getCurrentRecordOrgId(state));
        }
    }

    ngOnChanges(changes: ng.IOnChangesObject) {
        this.setBorderClasses(false);
        if (this.attribute.fieldResponseTypeId === this.inputTypes.Multiselect) {
            this.multiSelectModel = this.getSelectedOptionsFromValues(this.cell.values);
        }
        if (this.isUserAttribute) {
            this.userModel = this.getUserModel(this.cell.values);
        }
        if (this.attribute.fieldResponseTypeId === this.inputTypes.Select && changes && changes.attribute && changes.attribute.currentValue.fieldAttributeTypeId !== OptionTypes.Orgs && changes.attribute.currentValue.fieldAttributeTypeId !== OptionTypes.Users) {
            this.singleSelectOptions = this.getSingleSelectOptions(changes.attribute.currentValue);
            this.filteredOptions = this.lookupService.filterOptionsBySelections(this.cell.values, this.singleSelectOptions, "valueId");
        }
        if (changes.attribute) {
            this.options = this.isUserAttribute ? filter(this.getActiveAttributeOptions(changes.attribute.currentValue), (option: IAttributeValue): boolean => option.active) : this.getActiveAttributeOptions(changes.attribute.currentValue);
        }
    }

    ngOnDestroy() {
        this.destroy$.next();
        this.destroy$.complete();
    }

    setDisabledTooltip() {
        const isRiskAttribute = this.attribute.fieldAttributeCode === InventoryAttributeCodes[InventoryTableIds.Assets].Risk ||
                                this.attribute.fieldAttributeCode === InventoryAttributeCodes[InventoryTableIds.Processes].Risk;
        if ((!this.disabledTooltip || isUndefined(this.disabledTooltip)) && this.isDetailsPage) {
            if (this.inventoryService.attributeIsRelated(this.attribute.fieldAttributeCode, this.inventoryType)) {
                this.disabledTooltip = this.translatePipe.transform("EditLinkedInventoriesOnRelated");
            } else if (isRiskAttribute) {
                this.disabledTooltip = this.translatePipe.transform("ClickToViewAndModifyRisks");
            } else {
                this.disabledTooltip = "";
            }
        }
    }

    goToRoute(cell: IInventoryCell) {
        if (cell.routeParams) {
            this.stateService.go(cell.route, cell.routeParams);
        } else {
            this.stateService.go(cell.route);
        }
    }

    handleTextChange(model: string) {
        this.cell.values = [{ value: model.trim() }];
        this.cellChange.emit({ cell: this.cell });
    }

    handleSingleSelection(event: ISelection<IInventoryCellValue>) {
        this.cell.values = event && event.currentValue ? [event.currentValue] : [{value: null, valueId: null}];
        this.filteredOptions = this.lookupService.filterOptionsBySelections(this.cell.values, this.singleSelectOptions, "valueId");
        this.cellChange.emit({ cell: this.cell });
    }

    handleInputChange(value: string) {
        this.filteredOptions = this.lookupService.filterOptionsByInput(this.cell.values, this.singleSelectOptions, value, "valueId", "value");
    }

    handleOrgSelection(model: string) {
        const selectedOrg = getOrgById(model)(this.store.getState());
        this.cell.values = [{ value: selectedOrg ? selectedOrg.Name : null, valueId: model }];
        this.cellChange.emit({ cell: this.cell });
    }

    handleMultiSelection({ $model }: { $model: IMultiSelectModel[] }) {
        if (!isEmpty($model) && $model[$model.length - 1] && $model[$model.length - 1].IsPlaceholder) {
            const existingOption: IMultiSelectModel = this.checkForExistingOption($model[$model.length - 1], "name");
            const selectionExists = this.checkForExistingSelection($model);
            if (existingOption) {
                const optionInArray: IMultiSelectModel = find($model, ["id", existingOption.id]);
                $model.pop();
                if (!optionInArray) {
                    $model.push(existingOption);
                }
            } else if (selectionExists) {
                $model.pop();
            }
        }
        this.cell.values = this.setValuesFromSelectedOptions($model);
        this.cell.displayValue = getCommaSeparatedList($model, (item: IMultiSelectModel): string => {
            return item.nameKey ? this.translatePipe.transform(item.nameKey) : item.name;
        });
        this.cellChange.emit({ cell: this.cell });
    }

    handleUserSelection(user: IOtOrgUserOutput) {
        if (!user) return;
        const isValid = user.valid;
        const selection = user.selection;
        this.inventoryAction.setUserValidity(this.attribute.id, !selection || isValid);
        if (
            (selection && !isValid) ||
            (!selection && isUndefined(this.cell.values)) ||
            (selection && this.cell.values && this.cell.values[0] && (selection as IOrgUserAdapted).FullName === this.cell.values[0].value)
        ) {
            return;
        }
        if (!selection) {
            this.cell.values = [{ value: null, valueId: null }];
        } else if (!Utilities.matchRegex(selection.Id, GUID)) {
            this.cell.values = [{ value: selection.FullName }];
        } else {
            this.cell.values = [{ value: selection.FullName, valueId: selection.Id }];
        }
        this.userModel = this.getUserModel(this.cell.values);
        this.cellChange.emit({ cell: this.cell });
    }

    cellToggleEdit() {
        this.toggleEdit.emit();
        this.setBorderClasses(false);
        this.focus();
    }

    cellCallback(attributeCode: string) {
        if (this.disabledClickAction) {
            this.disabledClickAction({ dataFromCell: attributeCode });
        }
    }

    setBorderClasses(hovering: boolean) {
        this.isHovering = hovering;
        if (!this.isEditing && !this.isHovering) {
            this.borderClass = "padding-all-half border-default";
        } else if (!this.isEditing && this.isHovering) {
            this.borderClass = "padding-all-half border-blue";
        } else {
            this.borderClass = "";
        }
    }

    private focus() {
        setTimeout(() => {
            this.setupInputContents();
        }, 0);
    }

    private setupInputContents() {
        const inputEl: HTMLElement = this.getInputElement(this.element.nativeElement);
        if (inputEl) {
            inputEl.focus();
            inputEl.click();
        }
    }

    private getInputElement(elem: HTMLElement): HTMLElement {
        return (elem && elem[0]) ? elem[0].querySelector("input") : null;
    }

    private checkForExistingOption(model: IMultiSelectModel | ISingleSelectModel, key: string): any {
        return find(this.options, (option: IMultiSelectModel | ISingleSelectModel): boolean => toLower(option.name) === toLower(model[key]));
    }

    private checkForExistingSelection(model: IMultiSelectModel[]): boolean {
        const matchingSelections: IMultiSelectModel[] = filter(model, (selection: IMultiSelectModel): boolean => toLower(selection.name) === toLower(model[model.length - 1].name));
        return matchingSelections.length > 1;
    }

    private setValuesFromSelectedOptions(options: IMultiSelectModel[]): IInventoryCellValue[] {
        if (!options || !options.length) return [{ value: null, valueId: null }];
        return map(options, (option: IMultiSelectModel): IInventoryCellValue => {
            if (option.IsPlaceholder) {
                return {
                    value: option.name,
                    valueId: undefined,
                };
            } else {
                return {
                    value: option.value,
                    valueId: option.id,
                };
            }
        });
    }

    private getSelectedOptionsFromValues(values: IInventoryCellValue[]): IAttributeValue[] {
        if (values && values.length && isNull(values[0].value)) values = [];
        return map(values, (selection: IInventoryCellValue): IAttributeValue => {
            const foundSelection: IAttributeValue = find(this.attribute.values, (option: IAttributeValue): boolean => option.id === selection.valueId);
            return foundSelection ? foundSelection : { value: selection.value, name: selection.value };
        });
    }

    private getActiveAttributeOptions(attribute: IAttribute): IAttributeValue[] {
        if (!attribute || !attribute.values || !attribute.values.length) return [];
        return filter(attribute.values, (option: IAttributeValue): boolean => option.attributeValueStatusId !== OptionStates.Archived);
    }

    private getSingleSelectOptions(attribute: IAttribute): IInventoryCellValue[] {
        if (!attribute || !attribute.values || !attribute.values.length) return [];
        const activeOptions = filter(attribute.values, (option: IAttributeValue): boolean => option.attributeValueStatusId !== OptionStates.Archived);
        return map(activeOptions, (option: IAttributeValue): IInventoryCellValue => {
            return {value: option.nameKey ? this.translatePipe.transform(option.nameKey) : option.name, valueId: option.id};
        });
    }

    private getUserModel(selections: IInventoryCellValue[]): string {
        if (!selections || !selections.length) return "";
        return selections[0].valueId || selections[0].value;
    }
}
