// Rxjs
import { Subscription } from "rxjs";
import { startWith } from "rxjs/operators";

// Angular
import {
    Component,
    Input,
    Output,
    EventEmitter,
    Inject,
} from "@angular/core";
import { StateService } from "@uirouter/core";

// Redux
import observableFromStore from "oneRedux/utils/observable-from-store";
import {
    getSchema,
    getHasRequiredValues,
    getHasChanges,
    getCurrentInventoryId,
} from "oneRedux/reducers/inventory-record.reducer";

// Services
import { Permissions } from "modules/shared/services/helper/permissions.service";
import { InventoryListActionService } from "inventoryModule/services/action/inventory-list-action.service";
import { InventoryAdapterV2Service } from "inventoryModule/services/adapter/inventory-adapter-v2.service";
import { InventoryLaunchModalService } from "inventoryModule/services/action/inventory-launch-modal.service";
import { ReportTemplateExportService } from "reportsModule/reports-shared/services/report-template-export.service";
import { AssessmentListApiService } from "modules/assessment/services/api/assessment-list-api.service";

// Constants + Enums
import {
    RiskLabelBgClassesV2,
    RiskTranslations,
} from "enums/risk.enum";
import {
    InventoryDetailRoutes,
    AttributeFieldNames,
} from "constants/inventory.constant";
import { ChangeInventoryStatusCloseAction } from "enums/inventory.enum";

// Interfaces
import {
    IAttribute,
    IFormattedAttribute,
    IInventoryRecordV2,
    IInventoryListViewState,
    IInventoryListV2Column,
    IInventoryStatusOption,
} from "interfaces/inventory.interface";
import {
    INumberMap,
    IStringMap,
} from "interfaces/generic.interface";
import { IDropdownOption } from "interfaces/one-dropdown.interface";
import { IStoreState } from "interfaces/redux.interface";
import { IReportLayout } from "reportsModule/reports-shared/interfaces/report.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";

// Tokens
import { StoreToken } from "tokens/redux-store.token";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

// Enums
import { TableColumnTypes } from "enums/data-table.enum";
import {
    InventoryPermissions,
    InventoryDetailsTabs,
    InventoryStatusTranslationKeys,
} from "constants/inventory.constant";
import { NewInventorySchemaIds } from "constants/inventory-config.constant";
import { ControlPermissions } from "linkControlsAPIModule/constants/link-controls.constant";
import {
    InventoryStatusOptions,
    InventoryTableIds,
} from "enums/inventory.enum";
import { ReportTypeLowercase } from "reportsModule/reports-shared/enums/reports.enum";
import { ModuleTypes } from "enums/module-types.enum";

@Component({
    selector: "inventory-table-v2",
    templateUrl: "./inventory-table-v2.component.html",
})
export class InventoryTableV2Component {

    @Input() rows: IInventoryRecordV2[];
    @Input() columns: IInventoryListV2Column[];
    @Input() bulkEditMode: boolean;
    @Input() inventoryId: number;
    @Output() editMode: EventEmitter<boolean> = new EventEmitter<boolean>();

    inventoryPermissions = InventoryPermissions;
    controlPermissions = ControlPermissions;
    hasDetailPermission = false;
    currentPage: number;
    allRowsSelected: boolean;
    actionPermissions: IStringMap<boolean>;
    selectedRecords: IStringMap<boolean>;
    selectedRowId: string;
    cellTypes = TableColumnTypes;
    schema: IFormattedAttribute[];
    filterDrawerOpen: boolean;
    clickPreventClass = "prevent-row-click";
    hasEdits: boolean;
    requiredFieldsValid: boolean;
    hasEditableRows: boolean;
    menuPosition: string;
    rowActions: IDropdownOption[] = [];
    riskClasses: INumberMap<string> = RiskLabelBgClassesV2;
    riskTranslations: INumberMap<string> = RiskTranslations;
    inventoryStatusOptions = InventoryStatusOptions;
    inventoryStatusTranslationKeys = InventoryStatusTranslationKeys;
    emptyData = "- - - -";
    defaultExportLayout: IReportLayout;
    allowCreateAssessment: boolean;

    private subscription: Subscription;
    private stateSubscription: Subscription;

    constructor(
        private stateService: StateService,
        @Inject(StoreToken) private store,
        private permissions: Permissions,
        private inventoryAdapterV2Service: InventoryAdapterV2Service,
        private inventoryListActionService: InventoryListActionService,
        private inventoryLaunchModalService: InventoryLaunchModalService,
        private translatePipe: TranslatePipe,
        private reportTemplateExportService: ReportTemplateExportService,
        private assessmentListApi: AssessmentListApiService,
    ) { }

    ngOnInit() {
        this.subscription = observableFromStore(this.store).pipe(
            startWith(this.store.getState()),
        ).subscribe((state: IStoreState) => this.componentWillReceiveState(state));
        this.stateSubscription = this.inventoryListActionService.inventoryListViewState$.subscribe((stateData: IInventoryListViewState) => {
            this.currentPage = stateData.currentPageNumber;
            this.selectedRecords = stateData.selectedRecords;
            this.allRowsSelected = !Object.values(this.selectedRecords).includes(false);
            this.filterDrawerOpen = stateData.filterDrawerOpen;
        });
        this.actionPermissions = {
            canLaunch: this.permissions.canShow(this.inventoryPermissions[this.inventoryId].launch),
            canEdit: this.permissions.canShow(this.inventoryPermissions[this.inventoryId].edit),
            canDelete: this.permissions.canShow(this.inventoryPermissions[this.inventoryId].delete),
            canExport: this.permissions.canShow(this.inventoryPermissions[this.inventoryId].export),
            canAdd: this.permissions.canShow(this.inventoryPermissions[this.inventoryId].add),
            canCopy: this.permissions.canShow(this.inventoryPermissions[this.inventoryId].copy),
            canExportArt30: this.permissions.canShow(this.inventoryPermissions[this.inventoryId].exportArt30),
            canViewDetails: this.permissions.canShow(this.inventoryPermissions[this.inventoryId].details),
            canAddRisks: this.permissions.canShow(this.inventoryPermissions[this.inventoryId].addRisk),
            canViewRisks: this.permissions.canShow(this.inventoryPermissions[this.inventoryId].viewRisks),
            canAddAssessmentLinks: this.permissions.canShow(this.inventoryPermissions[this.inventoryId].addAssessmentLink),
            canViewAssessmentLinks: this.permissions.canShow(this.inventoryPermissions[this.inventoryId].viewAssessmentLinks),
            canLaunchVendorAssessment: this.permissions.canShow(this.inventoryPermissions[this.inventoryId].launchVendorAssessment),
            canViewInventoryAssessments: this.permissions.canShow(this.inventoryPermissions[this.inventoryId].launchVendorAssessment),
            canChangeStatus: this.permissions.canShow(this.inventoryPermissions[this.inventoryId].changeStatus),
            canBulkEdit: this.permissions.canShow(this.inventoryPermissions[this.inventoryId].bulkEdit),
            canExportPdf: this.permissions.canShow(this.inventoryPermissions[this.inventoryId].exportPdf),
            canExportV2: this.permissions.canShow(this.inventoryPermissions[this.inventoryId].exportV2),
            canChangeInventoryStatus: this.permissions.canShow(this.inventoryPermissions[this.inventoryId].changeInventoryStatus),
            canViewRelatedInventories: this.permissions.canShow(this.inventoryPermissions[this.inventoryId].viewRelated),
            canViewLineage: this.permissions.canShow(this.inventoryPermissions[this.inventoryId].viewLineage),
            canViewControls: this.permissions.canShow(ControlPermissions[NewInventorySchemaIds[this.inventoryId]].viewControlTab),
            canViewActivity: this.permissions.canShow(this.inventoryPermissions[this.inventoryId].viewActivity),
            canViewAttachments: this.permissions.canShow(this.inventoryPermissions[this.inventoryId].attachments),
            canViewContracts: this.permissions.canShow(this.inventoryPermissions[this.inventoryId].vendorContractInventoryLink),
            canViewDocuments: this.permissions.canShow(this.inventoryPermissions[this.inventoryId].vendorDocumentTab),
            canExportDefault: this.permissions.canShow(this.inventoryPermissions[this.inventoryId].exportDefaultPdf),
            canUseDefaultForExport: this.permissions.canShow(this.inventoryPermissions[this.inventoryId].setDefaultPdf),
        };
        this.hasDetailPermission =
            this.actionPermissions.canViewDetails ||
            this.actionPermissions.canViewAssessmentLinks ||
            this.actionPermissions.canViewRisks ||
            this.actionPermissions.canViewRelatedInventories ||
            this.actionPermissions.canViewLineage ||
            this.actionPermissions.canViewControls ||
            this.actionPermissions.canViewActivity ||
            this.actionPermissions.canViewAttachments ||
            this.actionPermissions.canViewContracts ||
            ((this.actionPermissions.canViewDocuments && this.inventoryId === InventoryTableIds.Vendors) || this.actionPermissions.canViewVendorContractInventoryLink) ||
            (this.actionPermissions.canViewAttachments && !this.actionPermissions.canViewVendorContractInventoryLink && (this.inventoryId !== InventoryTableIds.Vendors || !this.actionPermissions.canViewDocuments));
        this.hasEditableRows = this.rows.some((record): boolean => record.isEditable as boolean);
        if (this.actionPermissions.canExportDefault && this.actionPermissions.canUseDefaultForExport) {
            this.reportTemplateExportService.getDefaultLayout(ReportTypeLowercase.PDF, ModuleTypes.INV)
                .then((response: IProtocolResponse<IReportLayout>) => {
                    this.defaultExportLayout = response.data;
                });
        }
        this.assessmentListApi.getSspRestrictViewSetting().then((response): void => {
            if (response) {
                const restrictViewSetting = response.data && response.data.restrictProjectOwnerView;
                if (this.permissions.canShow("AssessmentCreate")) {
                    this.allowCreateAssessment = this.permissions.canShow("BypassSelfServicePortal") || !restrictViewSetting;
                } else {
                    this.allowCreateAssessment = false;
                }
            }
        });
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.stateSubscription.unsubscribe();
    }

    columnTrackBy(column: IAttribute): string {
        return column.id;
    }

    checkIfRowIsSelected(row: IInventoryRecordV2): boolean {
        return this.selectedRecords && this.selectedRecords[row.id] ? this.selectedRecords[row.id] : false;
    }

    checkIfRowIsDisabled(row: IInventoryRecordV2): boolean {
        return !row.isEditable;
    }

    getActions(menuIsOpen: boolean, row: IInventoryRecordV2) {
        this.setMenuClass(menuIsOpen);
        if (!menuIsOpen) return;
        this.rowActions = [];
        if (this.actionPermissions.canViewDetails && row.isEditable) {
            this.rowActions.push(this.getViewAction(row));
        }
        if (this.actionPermissions.canViewDetails && this.actionPermissions.canEdit && row.isEditable) {
            this.rowActions.push(this.getEditAction(row));
        }
        if (row.isEditable && (this.actionPermissions.canLaunch || this.actionPermissions.canLaunchVendorAssessment) && this.allowCreateAssessment) {
            this.rowActions.push(this.getLaunchAction(row));
        }
        if (this.actionPermissions.canCopy && row.isEditable) {
            this.rowActions.push(this.getCopyAction(row));
        }
        if (row.isEditable && this.actionPermissions.canChangeInventoryStatus) {
            this.rowActions.push(this.getChangeStatusAction(row));
        }
        if (row.isEditable && this.actionPermissions.canExportPdf) {
            this.rowActions.push(this.getPdfExportAction(row));
        }
        if (row.isEditable && this.actionPermissions.canViewRelatedInventories) {
            this.rowActions.push(this.viewRelatedInventories(row));
        }
        if (row.isEditable && this.actionPermissions.canDelete) {
            this.rowActions.push(this.getDeleteAction(row));
        }
        if (!this.rowActions.length) {
            this.rowActions.push(this.getEmpyListAction());
        }
    }

    toggleAllRowsSelected() {
        this.inventoryListActionService.updateState({ selectedRecords: this.inventoryAdapterV2Service.setSelectedRecords(this.rows, !this.allRowsSelected) });
    }

    toggleRowSelection(row: IInventoryRecordV2) {
        this.inventoryListActionService.setSelectedRecord(row, !this.selectedRecords[row.id as string]);
    }

    goToRecord(row: IInventoryRecordV2, mouseEvent: MouseEvent) {
        if (mouseEvent.ctrlKey || mouseEvent.metaKey) {
            const url: string = this.stateService.href(InventoryDetailRoutes[this.inventoryId], {
                recordId: row.id,
                inventoryId: this.inventoryId,
                tabId: InventoryDetailsTabs.Details,
            });
            window.open(url, "_blank");
        } else {
            this.stateService.go(InventoryDetailRoutes[this.inventoryId], {
                recordId: row.id,
                inventoryId: this.inventoryId,
                tabId: InventoryDetailsTabs.Details,
            });
        }
    }

    goToRisks(row: IInventoryRecordV2) {
        if (row.isEditable) {
            this.stateService.go(InventoryDetailRoutes[this.inventoryId], {
                recordId: row.id,
                inventoryId: this.inventoryId,
                tabId: InventoryDetailsTabs.Risks,
            });
        }
    }

    handleRowClick(click: { event: any, row: IInventoryRecordV2}): void {
        const selectedCount = Object.values(this.selectedRecords).filter((item) => item).length;
        const target = click && click.event ? click.event.target : null;
        const isInPreventClickContainer: boolean = target ? this.isChildOfNonClickElement(target) : null;
        if (!selectedCount && !isInPreventClickContainer) {
            this.selectedRowId = click.row.id as string;
            if (this.actionPermissions.canViewDetails) {
                this.inventoryListActionService.setCurrentRecordId(this.selectedRowId);
                this.editMode.emit(false);
                if (this.filterDrawerOpen) {
                    this.inventoryListActionService.toggleFilterDrawer(false);
                    setTimeout(() => {
                        this.inventoryListActionService.toggleAttributeDrawer(true);
                    }, 500);
                } else {
                    this.inventoryListActionService.toggleAttributeDrawer(true);
                }
            }
        }
    }

    setMenuClass(event?: any) {
        this.menuPosition = "bottom-right";
        if ((window.innerHeight - 200) / 2 < (event.clientY - 200)) {
            this.menuPosition = "top-right";
        }
    }

    private getChangeStatusAction(row: IInventoryRecordV2): IDropdownOption {
        const selectedStatus = row[AttributeFieldNames.Status] ? (row[AttributeFieldNames.Status] as IInventoryStatusOption).key : undefined;
        return {
            text: this.translatePipe.transform("ChangeStatus"),
            action: () => {
                this.inventoryLaunchModalService.launchStatusChangeModal(
                    getCurrentInventoryId(this.store.getState()),
                    [row.id],
                    ChangeInventoryStatusCloseAction.RELOAD_LIST,
                    this.inventoryListActionService.fetchCurrentList,
                    selectedStatus,
                    row[AttributeFieldNames.Name] as string,
                );
            },
        };
    }

    private getDeleteAction(row: IInventoryRecordV2): IDropdownOption {
        return {
            text: this.translatePipe.transform("Delete"),
            action: () => { this.inventoryListActionService.handleBulkDelete([row], this.hasEdits, this.requiredFieldsValid); },
        };
    }

    private getLaunchAction(row: IInventoryRecordV2): IDropdownOption {
        return {
            text: this.translatePipe.transform("LaunchAssessment"),
            action: () => { this.inventoryListActionService.handleLaunchInventoryAssessment([row]); },
        };
    }

    private getViewAction(row: IInventoryRecordV2): IDropdownOption {
        return {
            text: this.translatePipe.transform("ViewDetails"),
            action: () => {
                this.editMode.emit(false);
                this.inventoryListActionService.setCurrentRecordId(row.id);
                this.inventoryListActionService.toggleAttributeDrawer(true);
            },
        };
    }

    private getPdfExportAction(row: IInventoryRecordV2): IDropdownOption {
        return {
            text: this.translatePipe.transform("ExportAsPDF"),

            action: () => {
                this.inventoryLaunchModalService.launchNotificationModal("ReportDownload", this.translatePipe.transform("ReportIsGeneratedCheckTasksBar"));
                if (this.actionPermissions.canExportDefault && this.actionPermissions.canUseDefaultForExport) {
                    this.reportTemplateExportService.export(this.defaultExportLayout, row.name.toString(), row.id, ModuleTypes.INV);
                } else {
                    this.inventoryListActionService.exportPDF(row.id, row.name as string);
                }
            },
        };
    }

    private getEditAction(row: IInventoryRecordV2): IDropdownOption {
        return {
            text: this.translatePipe.transform("Edit"),
            action: () => {
                this.editMode.emit(true);
                this.inventoryListActionService.setCurrentRecordId(row.id);
                this.inventoryListActionService.toggleAttributeDrawer(true);
            },
        };
    }

    private getCopyAction(row: IInventoryRecordV2): IDropdownOption {
        return {
            text: this.translatePipe.transform("Copy"),
            action: () => { this.inventoryListActionService.launchCopyModal(this.inventoryId, row); },
        };
    }

    private viewRelatedInventories(row: IInventoryRecordV2): IDropdownOption {
        return {
            text: this.translatePipe.transform("ViewRelatedInventories"),
            action: () => {
                this.stateService.go(InventoryDetailRoutes[this.inventoryId], {
                    recordId: row.id,
                    inventoryId: this.inventoryId,
                    tabId: InventoryDetailsTabs.Related,
                });
            },
        };
    }

    private getEmpyListAction(): IDropdownOption {
        return {
            text: this.translatePipe.transform("NoActionsAvailable"),
            action: () => { }, // Keep Empty
        };
    }

    private isChildOfNonClickElement(target: any): boolean {
        if (!target || target.hasAttribute("otdatatable")) return false;
        if (target.className.includes(this.clickPreventClass)) return true;
        return this.isChildOfNonClickElement(target.parentNode);
    }

    private componentWillReceiveState(state: IStoreState) {
        this.schema = getSchema(state);
        this.hasEdits = getHasChanges(state);
        this.requiredFieldsValid = getHasRequiredValues(state);
    }
}
