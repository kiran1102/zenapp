// Rxjs
import { Subject } from "rxjs";
import {
    startWith,
    takeUntil,
} from "rxjs/operators";

// 3rd party
import {
    Component,
    Inject,
    Output,
    EventEmitter,
    Input,
} from "@angular/core";
import { StateService } from "@uirouter/core";
import { findIndex } from "lodash";

// Redux
import {
    getAssessmentList,
    getIsLoadingAssessments,
} from "oneRedux/reducers/inventory.reducer";
import observableFromStore from "oneRedux/utils/observable-from-store";

// Services
import { InventoryActionService } from "inventoryModule/services/action/inventory-action.service";
import { Permissions } from "modules/shared/services/helper/permissions.service";
import { AssessmentLinkingAPIService } from "modules/assessment/services/api/assessment-linking-api.service";
import { AssessmentBulkLaunchActionService } from "sharedModules/services/actions/bulk-launch-assessment-action.service";
import { AATemplateApiService } from "modules/assessment/services/api/aa-template-api.service";
import { NotificationService } from "modules/shared/services/provider/notification.service";
import { ShareAssessmentService } from "modules/assessment/services/api/share-assessment-api.service";
import { ModalService } from "sharedServices/modal.service";
import { AssessmentListApiService } from "modules/assessment/services/api/assessment-list-api.service";
import { InventoryAdapterV2Service } from "inventoryModule/services/adapter/inventory-adapter-v2.service";

// Constants + Enums
import { AssessmentTypes } from "enums/assessment.enum";
import { InventoryPermissions } from "constants/inventory.constant";
import { CellTypes } from "enums/general.enum";
import { InventoryTableIds } from "enums/inventory.enum";

// Interfaces
import { IDropdownOption } from "interfaces/one-dropdown.interface";
import {
    IInventoryAssessment,
    IInventoryRecordV2,
} from "interfaces/inventory.interface";
import {
    IStoreState,
    IStore,
} from "interfaces/redux.interface";
import { IStringMap } from "interfaces/generic.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";
import { IAssessmentCreationDetails } from "interfaces/assessment/assessment-creation-details.interface";
import { ISharedAssessmentUsersContractById } from "interfaces/user.interface";
import { IShareAssessmentModalData } from "interfaces/assessment/assessment.interface";
import { IViewRelatedAssessmentModalData } from "sharedModules/interfaces/view-related-assessment-modal.interface";

// Tokens
import { StoreToken } from "tokens/redux-store.token";
import { AssessmentStatus } from "enums/assessment/assessment-status.enum";
import { AssessmentStateKeys } from "constants/assessment.constants";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

// Components
import { ViewRelatedAssessmentModalComponent } from "sharedModules/components/view-related-assessment-modal/view-related-assessment-modal.component";

// Vitreus
import {
    OtModalService,
    ModalSize,
} from "@onetrust/vitreus";

interface IInventoryAssessmentColumn {
    id: string;
    labelKey: string;
    rowTextKey?: string;
    rowTranslationKey?: string;
    type: number;
}

@Component({
    selector: "inventory-assessments",
    templateUrl: "./inventory-assessments.component.html",
})
export class InventoryAssessmentsComponent {

    @Input() public recordId: string;
    @Input() public record: IInventoryRecordV2;
    @Output() public goToRoute: EventEmitter<IInventoryAssessment> = new EventEmitter<IInventoryAssessment>();

    isLoadingAssessments = true;
    allowCreateAssessment: boolean;
    bordered = true;
    inventoryId: number;
    inventoryTableIds = InventoryTableIds;
    cellTypes = CellTypes;
    assessmentTypes = AssessmentTypes;
    rows: IInventoryAssessment[];
    columns: IInventoryAssessmentColumn[];
    showAddAssessmentButton: boolean;
    relatedAssessmentModalData: IViewRelatedAssessmentModalData[];
    relatedAssessmentModalSize: ModalSize = ModalSize.MEDIUM;
    menuClass: string;
    private destroy$ = new Subject();
    private permissions: IStringMap<boolean>;

    constructor(
        private stateService: StateService,
        @Inject(StoreToken) private store: IStore,
        private inventoryAction: InventoryActionService,
        private permission: Permissions,
        private assessmentBulkLaunchActionService: AssessmentBulkLaunchActionService,
        readonly assessmentLinkingAPI: AssessmentLinkingAPIService,
        private assessmentTemplateAPI: AATemplateApiService,
        private notificationService: NotificationService,
        private shareAssessmentService: ShareAssessmentService,
        private modalService: ModalService,
        private otModalService: OtModalService,
        private translatePipe: TranslatePipe,
        private assessmentListApi: AssessmentListApiService,
        private inventoryAdapterV2Service: InventoryAdapterV2Service,
    ) {
        this.columns = [
            { id: "name", labelKey: "Name", rowTextKey: "name", type: this.cellTypes.link },
            { id: "name", labelKey: "Organization", rowTextKey: "orgName", type: this.cellTypes.text },
            { id: "name", labelKey: "Template", rowTextKey: "templateName", type: this.cellTypes.text },
            { id: "name", labelKey: "Stage", rowTranslationKey: "status", type: this.cellTypes.text },
            { id: "name", labelKey: "CompletedDate", rowTextKey: "completedDate", type: this.cellTypes.date },
        ];
    }

    public ngOnInit(): void {
        observableFromStore(this.store).pipe(
            startWith(this.store.getState()),
            takeUntil(this.destroy$),
        ).subscribe((state: IStoreState) => this.componentWillReceiveState(state));
        this.inventoryAction.getLinkedAssessmentView(this.recordId);
        this.permissions = {
            canLaunchAssessments: this.permission.canShow(InventoryPermissions[this.inventoryId].launch),
            canLaunchVendorAssessment:  this.permission.canShow(InventoryPermissions[this.inventoryId].launchVendorAssessment),
            canRemoveAssessmentLinks: this.permission.canShow(InventoryPermissions[this.inventoryId].removeAssessmentLinks),
            canDeleteAssessments: this.permission.canShow(InventoryPermissions[this.inventoryId].deleteAssessments),
            dmAssessmentV2: this.permission.canShow("DMAssessmentV2"),
            templatesV2: this.permission.canShow("TemplatesV2"),
        };
        this.launchAssessment();
    }

    public ngOnDestroy(): void {
        this.destroy$.next();
        this.destroy$.complete();
    }

    public columnTrackBy(column: IInventoryAssessmentColumn): string {
        return column.id;
    }

    public getActions(inventoryAssessment: IInventoryAssessment): () => IDropdownOption[] {
        return (): IDropdownOption[] => {
            const menuOptions: IDropdownOption[] = [];
            if ((inventoryAssessment.status !== AssessmentStatus.Completed) && this.permission.canShow("ResendLinkToAssessment")) {
                menuOptions.push({
                    text: this.translatePipe.transform("ResendLink"),
                    action: (): void => this.getResendLinkToAssessmentAction(inventoryAssessment.id),
                });
            }
            if (this.permission.canShow("AssessmentShare")) {
                menuOptions.push({
                    text: this.translatePipe.transform("Share"),
                    action: (): void => this.shareAssessmentAction(inventoryAssessment.id),
                });
            }
            if (this.permission.canShow("AssessmentViewRelatedAssessments")) {
                menuOptions.push({
                    text: this.translatePipe.transform("ViewRelatedAssessments"),
                    action: (): void => this.getRelatedAssessments(inventoryAssessment.id),
                });
            }
            if (this.permissions.canRemoveAssessmentLinks) {
                menuOptions.push({
                    text: this.translatePipe.transform("RemoveLink"),
                    action: (): void =>  this.removeLink(inventoryAssessment.id),
                });
            }
            if (this.permission.canShow("AssessmentCopy")) {
                menuOptions.push({
                    text: this.translatePipe.transform("Copy"),
                    action: (): void => {
                        this.stateService.go("zen.app.pia.module.assessment.copy-assessment", { assessmentId: inventoryAssessment.id });
                    },
                });
            }
            if (this.permissions.canDeleteAssessments) {
                menuOptions.push({
                    text: this.translatePipe.transform("Delete"),
                    action: (): void =>  this.deleteAssessment(inventoryAssessment.id),
                });
            }
            return menuOptions;
        };
    }

    public getTranslatedText = (stateDesc: string): string => {
        switch (stateDesc) {
            case AssessmentStatus.Not_Started:
                return this.translatePipe.transform(AssessmentStateKeys.NotStarted);
            case AssessmentStatus.In_Progress:
                return this.translatePipe.transform(AssessmentStateKeys.InProgress);
            case AssessmentStatus.Under_Review:
                return this.translatePipe.transform(AssessmentStateKeys.UnderReview);
            case AssessmentStatus.Completed:
                return this.translatePipe.transform(AssessmentStateKeys.Completed);
            default:
                return this.translatePipe.transform(stateDesc);
        }
    }

    onAddAssessmentClick() {
        const bulkInventoryList = this.inventoryAdapterV2Service.getRecordAssignments([this.record], this.inventoryId);
        const bulkAssessmentDetails: IAssessmentCreationDetails[] = this.assessmentBulkLaunchActionService.getAssessmentCreationItems(bulkInventoryList, this.inventoryId);
        this.assessmentBulkLaunchActionService.setBulkLaunchAssessments(bulkAssessmentDetails);
        this.stateService.go("zen.app.pia.module.assessment.bulk-launch-inventory-assessment", { inventoryTypeId: this.inventoryId });
    }

    getResendLinkToAssessmentAction(assessmentId: string): void {
        this.assessmentTemplateAPI.getResendLinkToAssessment(assessmentId)
            .then((response: IProtocolResponse<boolean>): void => {
                if (response.result) {
                    this.notificationService.alertSuccess(this.translatePipe.transform("Success"), this.translatePipe.transform("EmailSent"));
                }
            });
    }

    setMenuClass(inventoryAssessment: IInventoryAssessment): void | undefined {
        this.menuClass = "actions-table__context-list";
        if (this.rows.length <= 10) return;
        const halfwayPoint: number = (this.rows.length - 1) / 2;
        const rowIndex: number = findIndex(this.rows, (item: IInventoryAssessment): boolean => inventoryAssessment.id === item.id);
        if (rowIndex > halfwayPoint) this.menuClass += " actions-table__context-list--above";
    }

    private componentWillReceiveState(state: IStoreState): void {
        if (state
            && state.inventory.inventoryId) {
            this.inventoryId = state.inventory.inventoryId;
        }
        this.isLoadingAssessments = getIsLoadingAssessments(state);
        this.rows = getAssessmentList(state);
    }

    private removeLink(assessmentId: string): void {
        if (this.permissions.dmAssessmentV2
            || this.permissions.templatesV2
            || this.inventoryId === InventoryTableIds.Vendors) {
            this.inventoryAction.removeAssessmentV2Link(this.recordId, assessmentId);
        } else {
            this.inventoryAction.removeAssessmentLink(this.recordId, assessmentId);
        }
    }

    private deleteAssessment(assessmentId: string): void {
        this.inventoryAction.deleteAssessment(this.recordId , assessmentId);
    }

    private launchAssessment(): void {
        this.assessmentListApi.getSspRestrictViewSetting().then((response): void => {
            if (response) {
                const restrictViewSetting = response.data && response.data.restrictProjectOwnerView;
                if (this.permission.canShow("AssessmentCreate")) {
                    this.allowCreateAssessment = this.permission.canShow("BypassSelfServicePortal") || !restrictViewSetting;
                } else {
                    this.allowCreateAssessment = false;
                }
                if (this.inventoryTableIds.Vendors === this.inventoryId) {
                    this.showAddAssessmentButton = this.allowCreateAssessment && this.permissions.canLaunchVendorAssessment;
                } else {
                    this.showAddAssessmentButton = this.allowCreateAssessment && this.permissions.canLaunchAssessments && this.permissions.dmAssessmentV2;
                }
            } else {
                if (this.inventoryTableIds.Vendors === this.inventoryId) {
                    this.showAddAssessmentButton = this.permissions.canLaunchVendorAssessment;
                } else {
                    this.showAddAssessmentButton = this.permissions.canLaunchAssessments && this.permissions.dmAssessmentV2;
                }
            }

        });
    }

    private shareAssessmentAction(inventoryAssessmentId: string): void {
        this.shareAssessmentService.getAllSharedAssessmentUsers(inventoryAssessmentId)
            .then((response: IProtocolResponse<ISharedAssessmentUsersContractById>): void => {
                if (response.result) {
                    const usersToWhichAssessmentsShared: ISharedAssessmentUsersContractById = response.data;
                    const modalData: IShareAssessmentModalData = {
                        assessmentId: inventoryAssessmentId,
                        usersToWhichAssessmentsShared,
                    };
                    this.modalService.setModalData(modalData);
                    this.modalService.openModal("downgradeShareAssessmentModal");
                }
            });
    }

    private getRelatedAssessments(inventoryAssessmentId: string) {
        this.otModalService
            .create(
                ViewRelatedAssessmentModalComponent,
                {
                    isComponent: true,
                    size: this.relatedAssessmentModalSize,
                },
                inventoryAssessmentId,
            ).pipe(
                takeUntil(this.destroy$),
            );
    }
}
