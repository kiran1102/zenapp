// Rxjs
import { Subscription, Subject } from "rxjs";
import { startWith } from "rxjs/operators";
import {
    Transition,
    HookResult,
} from "@uirouter/angularjs";
import {
    TransitionService,
    StateService,
} from "@uirouter/core";

// Angular
import {
    Component,
    Inject,
    OnDestroy,
} from "@angular/core";

// Redux
import observableFromStore from "oneRedux/utils/observable-from-store";
import {
    getSchema,
    getIsEditingRecord,
    getIsLoadingRecord,
    getHasErrors,
    getHasChanges,
    getHasRequiredValues,
    getRecord,
    getCurrentInventoryId,
} from "oneRedux/reducers/inventory-record.reducer";

// Tokens
import { StoreToken } from "tokens/redux-store.token";

// Services
import { InventoryAdapterV2Service } from "inventoryModule/services/adapter/inventory-adapter-v2.service";
import { InventoryListActionService } from "inventoryModule/services/action/inventory-list-action.service";
import { InventoryLaunchModalService } from "inventoryModule/services/action/inventory-launch-modal.service";
import { RecordActionService } from "modules/inventory/services/action/record-action.service";
import { GridViewStorageService } from "sharedModules/services/helper/grid-view-storage.service";
import { Permissions } from "modules/shared/services/helper/permissions.service";
import { AssessmentListApiService } from "modules/assessment/services/api/assessment-list-api.service";

// Constants + Enums
import {
    InventoryPermissions,
    InventoryBulkEditRoutes,
    InventoryListRoutes,
    AttributeFieldNames,
    InventoryBrowserStorageNames,
    InventoryBulkImportActiveCards,
} from "constants/inventory.constant";
import {
    NewInventorySchemaIds,
    InventoryTypeTranslations,
    LegacyInventorySchemaIds,
} from "constants/inventory-config.constant";
import { MapMergeParam } from "modules/intg/constants/integration.constant";
import {
    InventoryTableIds,
    ChangeInventoryStatusCloseAction,
} from "enums/inventory.enum";

// Interfaces
import { IStringMap } from "interfaces/generic.interface";
import { IDropdownOption } from "interfaces/one-dropdown.interface";
import {
    IInventoryRecordV2,
    IFormattedAttribute,
    IInventoryTableV2,
    IInventoryParams,
    IInventoryListViewState,
    IInventoryStatusOption,
} from "interfaces/inventory.interface";
// Interfaces
import { IActionIcon } from "interfaces/action-icons.interface";
import { IFilterTree } from "interfaces/filter.interface";
import {
    IStore,
    IStoreState,
} from "interfaces/redux.interface";
import { IListColumnSelectorData } from "sharedModules/components/grid-column-selector/grid-column-selector.component";
import { IListState } from "@onetrust/vitreus";
import { ContextMenuType } from "@onetrust/vitreus";
import { IHeaderOption } from "interfaces/inventory.interface";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

@Component({
    selector: "inventory-list-v2",
    templateUrl: "./inventory-list-v2.component.html",
})
export class InventoryListV2Component implements OnDestroy {

    schemaIds = NewInventorySchemaIds;
    viewPermissions: IStringMap<string>;
    bulkActions: IActionIcon[];
    bulkEditMode = false;
    isExporting: boolean;
    listReady = false;
    currentRecord: IInventoryRecordV2;
    savingRecord: boolean;
    hasEdits: boolean;
    requiredFieldsValid: boolean;
    usersValid: boolean;
    recordEditEnabled = false;
    exportOption: IDropdownOption;
    isLoadingDetails: boolean;
    filterDrawerOpen: boolean;
    attributeDrawerOpen: boolean;
    filterData: IFilterTree;
    currentPageNumber: number;
    currentSearchTerm: string;
    currentRecordId: string;
    showingFilteredResults: boolean;
    inventoryId: InventoryTableIds;
    allowCreateAssessment: boolean;
    stateInventoryId: InventoryTableIds;
    selectedRecords: IStringMap<boolean>;
    name: string;
    table: IInventoryTableV2;
    selectedCount: number;
    schema: IFormattedAttribute[];
    contextMenuType = ContextMenuType;
    contextMenuOptions: IHeaderOption[] = [];
    isEditing = false;

    private subscription: Subscription;
    private stateSubscription: Subscription;
    private transitionDeregisterHook: any;
    private destroy$ = new Subject();
    private defaultParams: IInventoryParams = {
        page: 0,
        size: 20,
        searchTerm: "",
    };
    private params: IInventoryParams = {
        page: parseInt(this.stateService.params.page, 10) || this.defaultParams.page,
        size: parseInt(this.stateService.params.size, 10) || this.defaultParams.size,
        searchTerm: this.currentSearchTerm,
    };

    constructor(
        private stateService: StateService,
        @Inject(StoreToken) private store: IStore,
        private $transitions: TransitionService,
        private permissions: Permissions,
        private inventoryListActionService: InventoryListActionService,
        private translatePipe: TranslatePipe,
        private inventoryAdapterV2Service: InventoryAdapterV2Service,
        private inventoryLaunchModalService: InventoryLaunchModalService,
        private recordActionService: RecordActionService,
        private browserStorageListService: GridViewStorageService,
        private assessmentListApi: AssessmentListApiService,
    ) {}

    ngOnInit() {
        this.inventoryListActionService.setDefaultState();
        this.subscription = observableFromStore(this.store).pipe(
            startWith(this.store.getState()),
        ).subscribe((state: IStoreState) => this.componentWillReceiveState(state));
        this.stateSubscription = this.inventoryListActionService.inventoryListViewState$.subscribe((stateData: IInventoryListViewState) => {
            this.filterDrawerOpen = stateData.filterDrawerOpen;
            this.attributeDrawerOpen = stateData.attributeDrawerOpen;
            this.currentRecordId = stateData.currentRecordId;
            this.listReady = stateData.listReady;
            this.table = stateData.table;
            this.selectedRecords = stateData.selectedRecords;
            this.selectedCount = Object.values(this.selectedRecords).filter((item): boolean => item).length;
            this.bulkEditMode = this.selectedCount > 0;
            this.isExporting = stateData.isExporting;
            this.currentSearchTerm = stateData.searchTerm;
            this.showingFilteredResults = stateData.isShowingFilteredResults;
            this.filterData = stateData.filterData;
            this.stateInventoryId = stateData.inventoryId;
            this.currentPageNumber = stateData.currentPageNumber;
        });
        this.assessmentListApi.getSspRestrictViewSetting().then((response): void => {
            if (response) {
                const restrictViewSetting = response.data && response.data.restrictProjectOwnerView;
                if (this.permissions.canShow("AssessmentCreate")) {
                    this.allowCreateAssessment = this.permissions.canShow("BypassSelfServicePortal") || !restrictViewSetting;
                } else {
                    this.allowCreateAssessment = false;
                }
            }
        });
        this.inventoryId = LegacyInventorySchemaIds[this.stateService.params.Id] || this.stateService.params.Id;
        this.viewPermissions = InventoryPermissions[this.inventoryId];
        this.inventoryListActionService.setSearchTerm(sessionStorage.getItem(`DMInventorySearchTerm${this.inventoryId}`) || "");
        this.inventoryListActionService.updateState({currentPageNumber: JSON.parse(sessionStorage.getItem(`DMInventoryPageNumber${this.inventoryId}`)) || this.defaultParams.page });
        this.inventoryListActionService.initializeTable(this.inventoryId, this.currentPageNumber);
        this.name = this.translatePipe.transform(InventoryTypeTranslations[this.inventoryId]);
        this.contextMenuOptions = this.initializeContextMenuItems(this.schemaIds[this.inventoryId]);
        this.exportOption = {
            text: this.translatePipe.transform("Export"),
            action: () => {
                if (this.permissions.canShow(this.viewPermissions.exportV2)) {
                    this.inventoryListActionService.exportV2Report(this.schemaIds[this.inventoryId]);
                } else {
                    this.inventoryListActionService.exportInventory(this.schemaIds[this.inventoryId]);
                }
            },
        };
        this.launchDataDiscoveryModalOnLoad();
        this.setTransitionAwayHook();
    }

    ngOnDestroy() {
        this.inventoryListActionService.setDefaultState();
        this.stateSubscription.unsubscribe();
        this.subscription.unsubscribe();
        this.destroy$.next();
        this.destroy$.complete();
    }

    addNewRecord() {
        const closeAndLaunch = () => {
            this.inventoryListActionService.toggleAttributeDrawer(false);
            setTimeout(() => {
                this.recordActionService.setRecord({}, true);
                this.inventoryLaunchModalService.launchAddModal(this.inventoryId, this.recordActionService.setDefaultRecord, this.inventoryListActionService.fetchCurrentList);
            }, 500);
        };
        if (this.attributeDrawerOpen && this.recordEditEnabled && this.hasEdits && this.requiredFieldsValid) {
            this.inventoryListActionService.handleAttributeDrawer(
                this.attributeDrawerOpen,
                this.recordEditEnabled,
                this.hasEdits,
                this.requiredFieldsValid,
                closeAndLaunch,
            );
        } else if (this.attributeDrawerOpen) {
            closeAndLaunch();
        } else {
            this.recordActionService.setRecord({}, true);
            this.inventoryLaunchModalService.launchAddModal(this.inventoryId, this.recordActionService.setDefaultRecord, this.inventoryListActionService.fetchCurrentList);
        }
    }

    toggleFiltersDrawer() {
        if (this.attributeDrawerOpen) {
            const callback = () => {
                this.inventoryListActionService.toggleAttributeDrawer(false);
                setTimeout(() => {
                    this.inventoryListActionService.toggleFilterDrawer(true);
                }, 500);
            };
            this.inventoryListActionService.handleAttributeDrawer(
                false,
                this.recordEditEnabled,
                this.hasEdits,
                this.requiredFieldsValid,
                callback,
            );
        } else if (this.filterDrawerOpen) {
            this.inventoryListActionService.toggleFilterDrawer(false);
        } else {
            this.inventoryListActionService.toggleFilterDrawer(true);
        }
    }

    handleBulkEdit() {
        const selectedRecords = this.inventoryListActionService.getSelectedRecords();
        this.stateService.go(InventoryBulkEditRoutes[this.inventoryId], {
            schemaId: this.schemaIds[this.inventoryId],
            recordIds: selectedRecords.map((record: IInventoryRecordV2): string => record.id as string),
            returnRoute: InventoryListRoutes[this.inventoryId],
        });
    }

    launchColumnSelectorModal() {
        if (this.attributeDrawerOpen) {
            const attributeDrawerCallback = () => {
                this.inventoryListActionService.toggleAttributeDrawer(false);
                setTimeout(() => {
                    this.openColumnSelectorModal();
                }, 500);
            };
            this.inventoryListActionService.handleAttributeDrawer(
                false,
                this.recordEditEnabled,
                this.hasEdits,
                this.requiredFieldsValid,
                attributeDrawerCallback,
            );
        } else {
            this.openColumnSelectorModal();
        }
    }

    openColumnSelectorModal() {
        const { left, right } = this.inventoryListActionService.getListColumnSelectorColumns(this.schemaIds[this.inventoryId], this.schema);
        const resetToDefault = () => this.inventoryListActionService.getListColumnSelectorColumns(this.schemaIds[this.inventoryId], this.schema, true);
        const modalData: IListColumnSelectorData = { left, right, resetToDefault };
        const callback = (value: IListState) => {
            this.browserStorageListService.saveLocalStorageColumns(InventoryBrowserStorageNames[this.schemaIds[this.inventoryId]], value.right.map((column) => column.attribute.fieldName));
            this.inventoryListActionService.fetchCurrentList();
        };
        this.inventoryLaunchModalService.launchColumnSelectorModal(modalData, callback);
    }

    handleBulkLaunch() {
        this.inventoryListActionService.handleBulkLaunch(this.hasEdits, this.requiredFieldsValid);
    }

    handleBulkChangeStatus() {
        const inventoryId = getCurrentInventoryId(this.store.getState());
        const selectedRecords: IInventoryRecordV2[] = this.inventoryListActionService.getSelectedRecords();
        const selectedRecordIds: string[] = selectedRecords.map((record) => record.id);
        const selectedStatus = selectedRecords[0][AttributeFieldNames.Status] ? (selectedRecords[0][AttributeFieldNames.Status] as IInventoryStatusOption).key : undefined;
        const otherSelectedStatus = selectedStatus ? selectedRecords.find((record: IInventoryRecordV2) => (record[AttributeFieldNames.Status] as IInventoryStatusOption).key !== selectedStatus) : undefined;
        if (otherSelectedStatus) {
            this.inventoryLaunchModalService.launchStatusChangeModal(inventoryId, selectedRecordIds, ChangeInventoryStatusCloseAction.RELOAD_LIST, this.inventoryListActionService.fetchCurrentList);
        } else {
            this.inventoryLaunchModalService.launchStatusChangeModal(inventoryId, selectedRecordIds, ChangeInventoryStatusCloseAction.RELOAD_LIST, this.inventoryListActionService.fetchCurrentList, selectedStatus);
        }
    }

    handleBulkDelete() {
        const selectedRecords = this.inventoryListActionService.getSelectedRecords();
        this.inventoryListActionService.handleBulkDelete(selectedRecords, this.hasEdits, this.requiredFieldsValid);
    }

    toggleAllRecordsSelected(isSelected: boolean) {
        this.inventoryListActionService.updateState({ selectedRecords: this.inventoryAdapterV2Service.setSelectedRecords(this.table.rows, isSelected) });
    }

    getInventoryList(page: number = 0) {
        if (this.currentSearchTerm || !this.showingFilteredResults || this.filterData === null || Object.keys(this.filterData).length === 0) {
            this.inventoryListActionService.fetchInventoryListV2(this.inventoryId, page);
        } else {
            this.inventoryListActionService.fetchFilteredInventoryListV2(this.inventoryId, page);
        }
    }

    toggleAttributeDrawer(attributeDrawerOpen: boolean) {
        this.inventoryListActionService.handleAttributeDrawer(attributeDrawerOpen, this.recordEditEnabled, this.hasEdits, this.requiredFieldsValid);
    }

    closeDrawer() {
        if (this.attributeDrawerOpen) {
            this.toggleAttributeDrawer(false);
        } else {
            this.toggleFiltersDrawer();
        }
    }

    saveCancelled() {
        this.inventoryListActionService.saveCancelled();
    }

    saveRecord() {
        this.inventoryListActionService.saveRecord();
    }

    handleSearch(searchTerm: string) {
        this.inventoryListActionService.updateState({ isShowingSearchResults: searchTerm !== "", searchTerm, isShowingFilteredResults: false });
        sessionStorage.setItem(`DMInventorySearchTerm${this.inventoryId}`, searchTerm);
        this.getInventoryList();
    }

    applyFilters(event: { filters: IFilterTree }) {
        this.inventoryListActionService.updateState({ isShowingSearchResults: false, searchTerm: "", filterData: event.filters, isShowingFilteredResults: !(event.filters == null || Object.keys(event.filters).length === 0) });
        sessionStorage.removeItem(`DMInventorySearchTerm${this.inventoryId}`);
        if (event.filters == null || Object.keys(event.filters).length === 0) {
            this.inventoryListActionService.fetchInventoryListV2(this.inventoryId, 0);
            sessionStorage.removeItem(`DMInventoryFilterSettings${this.inventoryId}`);
        } else {
            this.inventoryListActionService.fetchFilteredInventoryListV2(this.inventoryId, 0);
            sessionStorage.setItem(`DMInventoryFilterSettings${this.inventoryId}`, JSON.stringify(event.filters));
        }
        this.closeDrawer();
    }

    contextMenuItemSelected(item: IHeaderOption) {
        item.action();
    }

    enableEditMode(isEditing: boolean) {
        this.isEditing = isEditing;
    }

    launchDataDiscoveryModalOnLoad() {
        const url = window.location.href;
        if (url.includes(MapMergeParam.MODAL_LAUNCH_PARAM) || this.stateService.params.openDataDiscoveryModal) {
            this.inventoryLaunchModalService.launchDataDiscoveryModal();
        }
    }

    private setTransitionAwayHook() {
        const exitRoute = this.inventoryId === InventoryTableIds.Vendors ? "zen.app.pia.module.vendor.inventory.list" : "zen.app.pia.module.datamap.views.inventory.list";
        this.transitionDeregisterHook = this.$transitions.onExit({ from: exitRoute }, (transition: Transition): HookResult => {
            return this.handleStateChange(transition.to(), transition.params("to"), transition.options);
        });
    }

    private handleStateChange(toState: IStringMap<any>, toParams: IStringMap<any>, options: IStringMap<any>): boolean {
        if (!this.hasEdits || !this.requiredFieldsValid) return true;
        const callback = () => {
            this.stateService.go(toState, toParams, options);
        };
        this.inventoryListActionService.launchSaveChangesModal(callback);
        return false;
    }

    private initializeContextMenuItems(schemaId: string): IHeaderOption[] {
        const menuOptions = [];
        if (this.permissions.canShow(InventoryPermissions[schemaId].bulkImportCreate)) {
            menuOptions.push({
                id: "BulkImportCreateInventory",
                text: this.translatePipe.transform("BulkCreate"),
                action: (): void => {
                    this.goToBulkImportControls(schemaId, "Create");
                },
            });
        }
        if (this.permissions.canShow(InventoryPermissions[schemaId].bulkImportUpdate)) {
            menuOptions.push({
                id: "BulkImportUpdateInventory",
                text: this.translatePipe.transform("BulkUpdate"),
                action: (): void => {
                    this.goToBulkImportControls(schemaId, "Update");
                },
            });
        }
        if (this.permissions.canShow(InventoryPermissions[schemaId].bulkImportAddControls)) {
            menuOptions.push({
                id: "BulkImportAddControlsToInventory",
                text: this.translatePipe.transform("BulkAddControls"),
                action: (): void => {
                    this.goToBulkImportControls(schemaId, "AddControls");
                },
            });
        }
        menuOptions.push({
            id: "DataDiscoveryResolveActionModal",
            text: this.translatePipe.transform("DataDiscovery"),
            action: (): void => {
                this.inventoryLaunchModalService.launchDataDiscoveryModal();
            },
        });
        return menuOptions;
    }

    private goToBulkImportControls(schemaId: string, type: "Create" | "Update" | "AddControls") {
        this.stateService.go("zen.app.pia.module.admin.import.templates", { activeCard: InventoryBulkImportActiveCards[schemaId][type] });
    }

    private componentWillReceiveState(state: IStoreState) {
        this.schema = getSchema(state);
        this.currentRecord = getRecord(state);
        this.savingRecord = getIsLoadingRecord(state);
        this.hasEdits = getHasChanges(state);
        this.requiredFieldsValid = getHasRequiredValues(state);
        this.usersValid = !getHasErrors(state);
        this.isLoadingDetails = getIsLoadingRecord(state);
        this.recordEditEnabled = getIsEditingRecord(state);
    }
}
