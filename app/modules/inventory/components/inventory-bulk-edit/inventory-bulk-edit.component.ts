// 3rd party
import { Component } from "@angular/core";
import { StateService } from "@uirouter/core";
import {
    map,
    forIn,
    filter,
    find,
} from "lodash";
import Utilities from "Utilities";

// Services
import { InventoryService } from "inventorySharedModule/shared/services/inventory-api.service";
import { InventoryAdapterV2Service } from "inventoryModule/services/adapter/inventory-adapter-v2.service";
import { ModalService } from "sharedServices/modal.service";
import { TaskPollingService } from "sharedServices/task-polling.service";
import { TimeStamp } from "oneServices/time-stamp.service";
import { InventoryUserLogicService } from "inventoryModule/services/business-logic/inventory-user-logic.service";

// Constants / Enums
import {
    InventoryTranslationKeys,
    AttributeResponseTypes,
    AttributeTypes,
    AttributeFieldNames,
} from "constants/inventory.constant";
import {
    LegacyInventorySchemaIds,
    InventorySchemaIds,
} from "constants/inventory-config.constant";
import { Regex } from "constants/regex.constant";
import { OrgUserTraversal } from "enums/org-user-dropdown.enum";

// Interfaces
import {
    IAttributeDetailV2,
    IInventoryRecordV2,
    IAttributeOptionV2,
    IFormattedAttribute,
    IFormattedAttributeOption,
    IInventoryStatusOption,
} from "interfaces/inventory.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";
import {
    IStringMap,
    IKeyValue,
} from "interfaces/generic.interface";
import { IMyDateModel } from "mydatepicker";
import { IOtOrgUserOutput } from "sharedModules/components/ot-org-user/ot-org-user.interface";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

interface ISelection<T> {
    previousValue: T;
    currentValue: T;
    change: T;
}

@Component({
    selector: "inventory-bulk-edit",
    templateUrl: "./inventory-bulk-edit.component.html",
})
export class InventoryBulkEditComponent {

    schema: IFormattedAttribute[] = [];
    record: IInventoryRecordV2 = {};
    responseTypes: IStringMap<string> = AttributeResponseTypes;
    attributeTypes = AttributeTypes;
    orgUserTraversal = OrgUserTraversal;
    selectedMap: IStringMap<boolean> = {};
    returnParams: { Id: string | number };
    returnRoute: string;
    inventoryName: string;
    isLoading: boolean;
    canSubmit = false;
    otherMap: IStringMap<string> = {};
    format = this.timeStamp.getDatePickerDateFormat();

    constructor(
        public inventoryUserLogicService: InventoryUserLogicService,
        private stateService: StateService,
        private taskPollingService: TaskPollingService,
        private inventoryAdapterV2Service: InventoryAdapterV2Service,
        private translate: TranslatePipe,
        private inventoryService: InventoryService,
        private modalService: ModalService,
        private timeStamp: TimeStamp,
    ) {}

    ngOnInit(): void {
        this.isLoading = true;
        const formatedInventoryId = this.stateService.params.schemaId === InventorySchemaIds.Vendors ? LegacyInventorySchemaIds[this.stateService.params.schemaId] : this.stateService.params.schemaId;
        this.returnRoute = this.stateService.params.returnRoute;
        this.returnParams = { Id: formatedInventoryId };
        this.inventoryName = this.translate.transform(InventoryTranslationKeys[this.stateService.params.schemaId].inventoryName);
        this.inventoryService.getAllAttributes(this.stateService.params.schemaId).then((response: IProtocolResponse<IAttributeDetailV2[]>): void => {
            if (response) this.schema = this.formatValues(response.data);
            this.isLoading = false;
        });
    }

    goToRoute(route: { path: string, params: { Id: string | number } }) {
        this.stateService.go(route.path, route.params);
    }

    textChange(value: string, attribute: IFormattedAttribute): void {
        this.record[attribute.fieldName] = value;
        this.autoSelectForEdit(attribute, Boolean(value));
    }

    singleSelectChange({ currentValue }: ISelection<IFormattedAttributeOption>, attribute: IFormattedAttribute): void {
        const newValue: IFormattedAttributeOption[] = currentValue ? [currentValue] : null;
        this.record[attribute.fieldName] = newValue;
        attribute.filteredValues = this.removeSelectedItems(attribute.values, newValue);
        this.otherMap[attribute.fieldName] = "";
        this.autoSelectForEdit(attribute, Boolean(newValue && newValue.length));
    }

    multiSelectChange({ currentValue }: ISelection<IFormattedAttributeOption[]>, attribute: IFormattedAttribute): void {
        this.record[attribute.fieldName] = currentValue || [];
        attribute.filteredValues = this.removeSelectedItems(attribute.values, currentValue || []);
        this.otherMap[attribute.fieldName] = "";
        this.autoSelectForEdit(attribute, Boolean(currentValue && currentValue.length));
    }

    selectAttribute(isChecked: boolean, attribute: IFormattedAttribute): void {
        this.selectedMap[attribute.fieldName] = isChecked;
        if (!isChecked) {
            this.record[attribute.fieldName] = undefined;
            attribute.filteredValues = attribute.values;
        }
        this.canSubmit = this.checkSubmitStatus();
    }

    getDateModel(attribute: IFormattedAttribute): string {
        if (!this.record[attribute.fieldName]) return "";
        return this.timeStamp.formatDateWithoutTimezoneForDatepicker(this.record[attribute.fieldName] as string);
    }

    attributeTrackBy(attribute: IFormattedAttribute): string {
        return attribute.id;
    }

    goBack(): void {
        this.stateService.go(this.stateService.params.returnRoute, {Id: LegacyInventorySchemaIds[this.stateService.params.schemaId]});
    }

    checkSubmitStatus(): boolean {
        for (const key in this.selectedMap) {
            if (this.selectedMap[key]) {
                return true;
            }
        }
        return false;
    }

    dateValueChanged(selectedValue: IMyDateModel, attribute: IFormattedAttribute): void {
        const formattedDate: string = selectedValue && selectedValue.date && selectedValue.date.year ? this.timeStamp.formatDateToIso(selectedValue.date) : "";
        this.record[attribute.fieldName] = formattedDate;
        this.autoSelectForEdit(attribute, Boolean(formattedDate));
    }

    submit(): void {
        this.modalService.setModalData({
            warningMessage: this.translate.transform("UpdateRecordsWarning", { RecordCount: this.stateService.params.recordIds.length }),
            callback: (confirmed: boolean): void => { if (confirmed) this.updateRecords(); },
        });
        this.modalService.openModal("bulkEditConfirmationModal");
    }

    updateRecords(): void {
        this.isLoading = true;
        const updates: IStringMap<string | Date | IFormattedAttributeOption | IFormattedAttributeOption[] | boolean | IInventoryStatusOption> = {};
        forIn(this.selectedMap, (isChecked: boolean, fieldName: string): void => {
            if (!isChecked) return;
            if (this.record[fieldName]) {
                updates[fieldName] = this.record[fieldName];
            } else {
                const matchingAttribute: IAttributeDetailV2 = find(this.schema, (attribute: IAttributeDetailV2): boolean => {
                    return attribute.fieldName === fieldName;
                });
                updates[fieldName] = matchingAttribute.responseType === AttributeResponseTypes.Text || matchingAttribute.responseType === AttributeResponseTypes.Date ? "" : [];
            }
        });
        const deserializedUpdates: IInventoryRecordV2 = this.inventoryAdapterV2Service.deserializeRecord(updates);
        this.inventoryService.bulkUpdateRecordV2(this.stateService.params.schemaId, deserializedUpdates, this.stateService.params.recordIds).then((response: IProtocolResponse<IInventoryRecordV2>): void => {
            if (response.result) {
                this.taskPollingService.startPolling(5000);
                this.stateService.go(this.stateService.params.returnRoute, { Id: LegacyInventorySchemaIds[this.stateService.params.schemaId] });
            } else {
                // TODO error validation from BE
            }
            this.isLoading = false;
        });
    }

    handleInputChange(event: IKeyValue<string>, attribute: IFormattedAttribute): void {
        if (!event.value) {
            attribute.filteredValues = attribute.values;
        } else {
            attribute.filteredValues = filter(attribute.values, (option: IFormattedAttributeOption): boolean => {
                return option.label.toLowerCase().indexOf(event.value.toLowerCase()) !== -1;
            });
        }
        attribute.filteredValues = this.removeSelectedItems(attribute.filteredValues, this.record[attribute.fieldName] as IFormattedAttributeOption[]);
        if (attribute.allowOther) this.otherMap[attribute.fieldName] = event.value;
    }

    userChange(user: IOtOrgUserOutput, attribute: IFormattedAttribute) {
        if (!user) return;
        const newSelection: { value: string, id?: string } = user.selection ? {
            value: typeof user.selection === "string" ? user.selection : user.selection.FullName,
        } : null;
        if (newSelection && Utilities.matchRegex(user.selection.Id, Regex.GUID)) {
            newSelection.id = typeof user.selection === "string" ? null : user.selection.Id;
        }
        const newValue = newSelection ? [newSelection] : null;
        this.record[attribute.fieldName] = newValue;
        this.autoSelectForEdit(attribute, Boolean(newValue && newValue.length));
    }

    selectOther(attribute: IFormattedAttribute): void {
        const currentSelections = this.record[attribute.fieldName] as IFormattedAttributeOption[];
        const otherSelection: IFormattedAttributeOption = { value: this.otherMap[attribute.fieldName], label: this.otherMap[attribute.fieldName], id: null };
        const existingOption: IFormattedAttributeOption = find(attribute.values, (option: IFormattedAttributeOption): boolean => option.label.toLowerCase().trim() === otherSelection.label.toLowerCase().trim());
        if (attribute.responseType === AttributeResponseTypes.SingleSelect) {
            this.record[attribute.fieldName] = [existingOption || otherSelection];
        } else {
            this.record[attribute.fieldName] = this.filterDuplicateOther(otherSelection, currentSelections, existingOption);
            attribute.filteredValues = this.removeSelectedItems(attribute.values, this.record[attribute.fieldName] as IFormattedAttributeOption[]);
        }
        this.otherMap[attribute.fieldName] = "";
        this.autoSelectForEdit(attribute, Boolean(this.record[attribute.fieldName] && (this.record[attribute.fieldName] as IFormattedAttributeOption[]).length));
    }

    filterDuplicateOther(otherSelection: IFormattedAttributeOption, currentSelections: IFormattedAttributeOption[], existingOption: IFormattedAttributeOption): IFormattedAttributeOption[] {
        if (currentSelections && currentSelections.length) {
            const duplicate = find(currentSelections, (selection: IFormattedAttributeOption): boolean => {
                return selection.label.toLowerCase().trim() === otherSelection.label.toLowerCase().trim();
            });
            return duplicate ? currentSelections : [existingOption || otherSelection, ...currentSelections];
        } else {
            return [existingOption || otherSelection];
        }
    }

    canEditInBulk = (attribute: IAttributeDetailV2): boolean => {
        return !attribute.required
            && !attribute.readOnly
            && attribute.attributeType !== AttributeTypes.Orgs
            && attribute.fieldName !== AttributeFieldNames.ExternalId;
    }

    private formatValues(attributes: IAttributeDetailV2[]): IFormattedAttribute[] {
        return map(attributes, (attribute: IAttributeDetailV2): IFormattedAttribute => {
            attribute.values = map(attribute.values, (option: IAttributeOptionV2): IFormattedAttributeOption => {
                return { ...option, label: option.valueKey ? this.translate.transform(option.valueKey) : option.value };
            });
            return {...attribute, filteredValues: attribute.values};
        });
    }

    private removeSelectedItems(options: IFormattedAttributeOption[], selections: IFormattedAttributeOption[]): IFormattedAttributeOption[] {
        return filter(options, (option: IFormattedAttributeOption): boolean => {
            return !find(selections, (selection: IFormattedAttributeOption): boolean => {
                return option.id === selection.id;
            });
        });
    }

    private autoSelectForEdit(attribute: IFormattedAttribute, hasValue: boolean): void {
        if (!this.selectedMap[attribute.fieldName]) {
            this.selectedMap[attribute.fieldName] = hasValue;
            this.canSubmit = this.checkSubmitStatus();
        }
    }

}
