import {
    Component,
    OnInit,
    Input,
    Output,
    EventEmitter,
} from "@angular/core";

// RXJS
import {
    Subject,
    Observable,
    combineLatest,
} from "rxjs";
import {
    takeUntil,
    map,
    filter,
} from "rxjs/operators";

// Services
import { InventoryListActionService } from "inventoryModule/services/action/inventory-list-action.service";

// Interfaces
import {
    IFilterV2,
    IFilterOutput,
} from "modules/filter/interfaces/filter.interface";
import {
    IFilterColumnOption,
    IFilterValueOption,
    IFilter,
    IFilterTree,
} from "interfaces/filter.interface";
import { ILabelValue } from "interfaces/generic.interface";

// Enum + Constants
import { FilterValueTypes } from "modules/filter/enums/filter.enum";
import { FilterColumnTypes } from "enums/filter-column-types.enum";
import { RiskLevelTranslations } from "enums/risk.enum";
import { AttributeFieldNames } from "constants/inventory.constant";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

@Component({
    selector: "inventory-list-filter",
    template: `
        <one-filter
            class="flex-full-height"
            [selectedFilters]="selectedFilters"
            [fields]="columnOptions"
            [getFieldLabelValue]="getFieldLabelValue"
            [currentValues]="currentFilterValues"
            [getValueLabelValue]="getColumnOptionLabelValue"
            [currentType]="currentFilterValueType"
            [loadingFilter]="loadingFilter"
            [togglePopup$]="filtersOpen$"
            (selectField)="setFilterValues($event)"
            (applyFilters)="apply($event)"
            (cancel)="cancel.emit()"
            >
        </one-filter>
    `,
})
export class InventoryListFilterComponent implements OnInit {

    @Input() set filtersOpen(isOpen: boolean) {
        if (!isOpen) this.filtersOpen$.next(isOpen);
    }
    @Input() isV2Filter = true;

    @Output() applyFilters = new EventEmitter<IFilterTree>();
    @Output() cancel = new EventEmitter<null>();

    filterColumnTypes = FilterColumnTypes;
    currentFilterValues: IFilterValueOption[] = [];
    selectedFilters: Array<IFilterV2<IFilterColumnOption, ILabelValue<string>, IFilterValueOption | string | number>>;
    currentFilterValueType: FilterValueTypes;
    loadingFilter: boolean;
    columnOptions: IFilterColumnOption[];
    filtersOpen$ = new Subject<boolean>();
    private userOptions: IFilterValueOption[];
    private destroy$ = new Subject();

    constructor(
        private translatePipe: TranslatePipe,
        private inventoryListActionService: InventoryListActionService,
    ) {}

    ngOnInit() {
        const allColumn$: Observable<IFilterColumnOption[]> = this.inventoryListActionService.allColumn$.pipe(
            filter((columns: IFilterColumnOption[]): boolean => Boolean(columns && columns.length)),
            map((columns: IFilterColumnOption[]): IFilterColumnOption[] => this.getOptionTypeColumns(columns)),
        );
        const userOption$: Observable<IFilterValueOption[]> = this.inventoryListActionService.userOption$.pipe(
            filter((options: IFilterValueOption[]): boolean => Boolean(options && options.length)),
        );
        combineLatest(allColumn$, userOption$)
            .pipe(takeUntil(this.destroy$))
            .subscribe(([columns, userOptions]) => {
                this.columnOptions = columns;
                this.userOptions = userOptions;
            });
        this.inventoryListActionService.inventoryListFilter$
            .pipe(takeUntil(this.destroy$))
            .subscribe((filters: IFilterTree) => {
                this.selectedFilters = filters && filters.AND ? this.serializeFilters(filters.AND) : [];
            });
    }

    ngOnDestroy() {
        this.destroy$.next();
        this.destroy$.complete();
    }

    getFieldLabelValue = (column: IFilterColumnOption): ILabelValue<string> => {
        return {
            label: column.name,
            value: column.id,
        };
    }

    setFilterValues(selectedField: ILabelValue<string>) {
        if (!selectedField) return;
        const selectedColumn = this.columnOptions.find((column: IFilterColumnOption) => column.id === selectedField.value);
        this.currentFilterValueType = FilterValueTypes.MultiSelect;
        this.currentFilterValues = selectedColumn
                ? (selectedColumn.type === FilterColumnTypes.User ? this.userOptions : selectedColumn.valueOptions)
                : [];
    }

    getColumnOptionLabelValue = (option: IFilterValueOption): ILabelValue<string | number> => {
        if (!option) return;
        return {
            label: option.translationKey ? this.translatePipe.transform(option.translationKey) : option.value as string,
            value: option.key,
        };
    }

    apply(filters: IFilterOutput[]) {
        const filterData = filters && filters.length ? { AND: this.deserializeFilters(filters) } : null;
        this.inventoryListActionService.applyFilters(filterData, this.isV2Filter);
        this.applyFilters.emit(filterData);
    }

    private formatFilterSelections(column: IFilterColumnOption, selections: string[]): IFilterValueOption[] {
        const uniqueValues: string[] = selections.filter((selection: string, index: number, arr: string[]) => arr.indexOf(selection) === index);
        if (column.type === FilterColumnTypes.User) {
            return uniqueValues.map((value: string): IFilterValueOption => this.userOptions.find((option: IFilterValueOption) => option.key === value ));
        } else if (
            column.type === FilterColumnTypes.Date
            || column.type === FilterColumnTypes.DateRange
            || column.type === FilterColumnTypes.DateTime
        ) {
            return uniqueValues.map((value: string): IFilterValueOption => ({ key: value, value: null }));
        } else if (column.id === AttributeFieldNames.Risk) {
            return uniqueValues.map((value: string): IFilterValueOption => column.valueOptions.find((option: IFilterValueOption) => {
                return option.value === this.translatePipe.transform(RiskLevelTranslations[value]) || option.value === value;
            }));
        }
        if (column.valueOptions && column.valueOptions.length) {
            return uniqueValues.map((value: string): IFilterValueOption => column.valueOptions.find((option: IFilterValueOption) => {
                return String(option.value) === String(value);
            }));
        }
        return uniqueValues.map((value: string): IFilterValueOption => ({ key: value, value }));
    }

    private serializeFilters(filters: IFilter[]): Array<IFilterV2<IFilterColumnOption, ILabelValue<string>, IFilterValueOption | string | number>> {
        return filters.map((selectedFilter: IFilter): IFilterV2<IFilterColumnOption, ILabelValue<string>, IFilterValueOption | string | number> => {
            const matchingColumn: IFilterColumnOption = this.columnOptions.find((column: IFilterColumnOption): boolean => column.id === selectedFilter.columnId);
            return {
                field: matchingColumn,
                operator: null,
                values: selectedFilter.values && selectedFilter.values.length ? this.formatFilterSelections(matchingColumn, selectedFilter.values as string[]) : selectedFilter.values,
                type: FilterValueTypes.MultiSelect,
            };
        });
    }

    private deserializeFilters(filters: IFilterOutput[]): IFilter[] {
        if (!(filters && filters.length)) return [];
        return (filters as IFilterOutput[]).map((selectedFilter: IFilterOutput): IFilter => {
            const matchingColumn: IFilterColumnOption = this.columnOptions.find((column: IFilterColumnOption): boolean => column.id === selectedFilter.field.value);
            return {
                columnId: matchingColumn.id,
                attributeKey: matchingColumn.id,
                entityType: matchingColumn.entityType,
                dataType: this.deserializeDataType(matchingColumn.type),
                operator: "EQ", // TODO update once we support more operators
                values: this.deserializeFilterValues(selectedFilter.values, matchingColumn.valueOptions, selectedFilter.type),
            };
        });
    }

    private deserializeFilterValues(values: Array<ILabelValue<string | number>>, options: IFilterValueOption[], type: FilterValueTypes): Array<string|number> {
        if (!(values && values.length)) return [];
        if (options && options.length) {
            return values.map((value: ILabelValue<string | number>): string | number => {
                const matchingOption: IFilterValueOption = options.find((option: IFilterValueOption): boolean => option.key === value.value);
                if (matchingOption) return matchingOption.value;
                return value.label || value.value;
            });
        }
        return values.map((value: ILabelValue<string|number>): string | number => {
            return value.value || value.label;
        });
    }

    private deserializeDataType(columnType: FilterColumnTypes): FilterColumnTypes {
        if (
            columnType === FilterColumnTypes.Date
            || columnType === FilterColumnTypes.DateRange
            || columnType === FilterColumnTypes.DateTime
        ) {
            return FilterColumnTypes.DateRange;
        }
        return columnType;
    }

    private getOptionTypeColumns(columns: IFilterColumnOption[]): IFilterColumnOption[] {
        return columns.filter((column: IFilterColumnOption): boolean => {
            return column.type === FilterColumnTypes.Multi
                || column.type === FilterColumnTypes.MultiInt
                || column.type === FilterColumnTypes.User
                || (column.type === FilterColumnTypes.Text && Boolean(column.valueOptionLink))
                || column.type === FilterColumnTypes.Date
                || column.type === FilterColumnTypes.DateRange
                || column.type === FilterColumnTypes.DateTime;
        });
    }

}
