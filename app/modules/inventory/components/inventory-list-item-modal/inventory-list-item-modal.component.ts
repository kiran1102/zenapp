import {
    Component,
    Inject,
    OnInit,
} from "@angular/core";

// Rxjs
import { Subject } from "rxjs";

// Services
import { InventoryService } from "inventorySharedModule/shared/services/inventory-api.service";
import { TenantTranslationService } from "sharedServices/tenant-translation.service";
import { InventoryAdapterV2Service } from "inventoryModule/services/adapter/inventory-adapter-v2.service";

// Const and Enums
import {
    InventoryTranslationKeys,
    InventoryListTypes,
} from "constants/inventory.constant";

// Interfaces
import { IProtocolResponse } from "interfaces/protocol.interface";
import {
    IInventoryListItem,
    IInventoryListOption,
    IInventoryElementListItem,
} from "interfaces/inventory.interface";
interface ISaveListItemPayload {
    value: string;
    description?: string;
    categories?: Array<{ value: string }>;
    classifications?: Array<{ value: string }>;
}

@Component({
    selector: "inventory-list-item-modal",
    templateUrl: "inventory-list-item-modal.component.html",
})
export class InventoryListItemModal implements OnInit {

    inventoryTranslationKeys = InventoryTranslationKeys;
    inventoryListTypes = InventoryListTypes;
    otModalCloseEvent: Subject<boolean>;
    categorySelection = "";
    classificationSelection = "";
    isSaving = false;
    isReady = false;
    isEditMode = false;
    listItemName = "";
    listItemDescription = "";
    categorySearchTerm: string;
    classificationSearchTerm: string;
    listItemCategoryOptions: IInventoryListOption[] = [];
    otherListItemCategoryOptions: IInventoryListOption[] = [];
    listItemCategory: IInventoryListOption;
    listItemClassificationOptions: IInventoryListOption[] = [];
    otherListItemClassificationOptions: IInventoryListOption[] = [];
    listItemClassification: IInventoryListOption;
    requiredFieldsPopulated = false;
    otModalData: { title: string, listId: string, listItem: IInventoryListItem | IInventoryElementListItem, callback: (listId: string) => {} };

    constructor(
        @Inject("TenantTranslationService") readonly tenantTranslationService: TenantTranslationService,
        private inventoryService: InventoryService,
        private inventoryAdapterV2Service: InventoryAdapterV2Service,
    ) { }

    ngOnInit() {
        if (this.otModalData.listId === InventoryListTypes.DataElements) {
            Promise.all(
                [this.inventoryService.getListData(InventoryListTypes.DataCategories),
                this.inventoryService.getListData(InventoryListTypes.DataClassifications)],
            ).then((responses: Array<IProtocolResponse<{data: IInventoryListItem[]}>>) => {
                if (responses[0].result) {
                    this.listItemCategoryOptions = this.inventoryAdapterV2Service.formatListItemOptions(responses[0].data.data);
                    if (this.otModalData.listItem && (this.otModalData.listItem as IInventoryElementListItem).categories.length > 1) {
                        this.otherListItemCategoryOptions = this.listItemCategoryOptions.filter((category) => {
                            if (!(this.otModalData.listItem as IInventoryElementListItem).categories.some((currentCategory) => category.id === currentCategory.id)) {
                                return category;
                            }
                        });
                    }
                }
                if (responses[1].result) {
                    this.listItemClassificationOptions = this.inventoryAdapterV2Service.formatListItemOptions(responses[1].data.data);
                    if (this.otModalData.listItem && (this.otModalData.listItem as IInventoryElementListItem).classifications.length > 1) {
                        this.otherListItemClassificationOptions = this.listItemClassificationOptions.filter((classification) => {
                            if (!(this.otModalData.listItem as IInventoryElementListItem).classifications.some((currentClassification) => classification.id === currentClassification.id)) {
                                return classification;
                            }
                        });
                    }
                }
                this.prepopulateValues();
                this.isReady = true;
            });
        } else {
            this.prepopulateValues();
            this.isReady = true;
        }
    }

    prepopulateValues() {
        if (this.otModalData.listItem) {
            this.listItemName = this.otModalData.listItem.name;
            this.listItemDescription = this.otModalData.listItem.description || "";
            if ((this.otModalData.listItem as IInventoryElementListItem).categories && (this.otModalData.listItem as IInventoryElementListItem).categories.length === 1) {
                this.listItemCategory = this.inventoryAdapterV2Service.formatListItemOption((this.otModalData.listItem as IInventoryElementListItem).categories[0]);
            }
            if ((this.otModalData.listItem as IInventoryElementListItem).classifications && (this.otModalData.listItem as IInventoryElementListItem).classifications.length === 1) {
                this.listItemClassification = this.inventoryAdapterV2Service.formatListItemOption((this.otModalData.listItem as IInventoryElementListItem).classifications[0]);
            }
            this.isEditMode = true;
            this.checkRequiredFieldsPopulated();
        }
    }

    selectCurrentCategory(selected: string) {
        this.categorySelection = selected;
        this.checkRequiredFieldsPopulated();
    }

    selectCurrentClassification(selected: string) {
        this.classificationSelection = selected;
        this.checkRequiredFieldsPopulated();
    }

    selectName(name: string) {
        this.listItemName = name;
        this.checkRequiredFieldsPopulated();
    }

    selectCategory({ currentValue }) {
        this.listItemCategory = currentValue;
        this.categorySearchTerm = "";
        this.checkRequiredFieldsPopulated();
    }

    categorySearch({ key, value }) {
        if (key === "Enter") return;
        this.categorySearchTerm = value;
    }

    selectClassification({ currentValue }) {
        this.listItemClassification = currentValue;
        this.classificationSearchTerm = "";
        this.checkRequiredFieldsPopulated();
    }

    classificationSearch({ key, value }) {
        if (key === "Enter") return;
        this.classificationSearchTerm = value;
    }

    saveAndClose() {
        this.saveListItem(() => {
            this.otModalData.callback(this.otModalData.listId);
            this.closeModal();
        });
    }

    saveAndAddNew() {
        this.saveListItem(() => {
            this.listItemName = "";
            this.listItemDescription = "";
            this.listItemCategory = null;
            this.listItemClassification = null;
            this.otModalData.callback(this.otModalData.listId);
            this.checkRequiredFieldsPopulated();
        });
    }

    closeModal() {
        this.otModalCloseEvent.next(true);
    }

    private checkRequiredFieldsPopulated() {
        if (this.otModalData.listId === InventoryListTypes.DataElements) {
            this.requiredFieldsPopulated = Boolean(this.listItemName.trim() &&
            ((this.listItemCategory && !this.isEditMode) ||
            (this.listItemCategory && this.isEditMode && (this.otModalData.listItem as IInventoryElementListItem).categories.length === 1) ||
            (this.listItemCategory && this.isEditMode && this.categorySelection === "otherCategory" && (this.otModalData.listItem as IInventoryElementListItem).categories.length > 1) ||
            (this.isEditMode && this.categorySelection && this.categorySelection !== "otherCategory" && (this.otModalData.listItem as IInventoryElementListItem).categories.length > 1)) &&
            (!this.isEditMode ||
            (this.isEditMode && (this.otModalData.listItem as IInventoryElementListItem).classifications.length <= 1) ||
            (this.isEditMode && this.listItemClassification && this.classificationSelection === "otherClassification" && (this.otModalData.listItem as IInventoryElementListItem).classifications.length > 1) ||
            (this.isEditMode && this.classificationSelection && this.classificationSelection !== "otherClassification" && (this.otModalData.listItem as IInventoryElementListItem).classifications.length > 1)));
        } else {
            this.requiredFieldsPopulated = Boolean(this.listItemName.trim());
        }
    }

    private saveListItem(successCb: () => void) {
        this.isSaving = true;
        const payload = this.formatPayload();
        if (this.otModalData.listItem) {
            this.updateItem(payload, successCb);
        } else {
            this.addItem(payload, successCb);
        }
    }

    private formatPayload(): ISaveListItemPayload {
        const payload: ISaveListItemPayload = { value: this.listItemName.trim() };
        if (this.listItemDescription.trim()) payload.description = this.listItemDescription.trim();
        if (!this.isEditMode) {
            if (this.listItemCategory) payload.categories = [{ value: this.listItemCategory.name }];
            if (this.listItemClassification) payload.classifications = [{ value: this.listItemClassification.name }];
        } else {
            if (this.categorySelection === "otherCategory" || (this.otModalData.listItem as IInventoryElementListItem).categories.length === 1) {
                payload.categories = [{ value: this.listItemCategory.name }];
            } else {
                payload.categories = [{ value: this.categorySelection }];
            }
            if (((this.otModalData.listItem as IInventoryElementListItem).classifications.length === 1 && this.listItemClassification) || this.classificationSelection === "otherClassification") {
                payload.classifications = [{ value: this.listItemClassification.name }];
            } else if (((this.otModalData.listItem as IInventoryElementListItem).classifications.length > 1 && this.classificationSelection !== "otherClassification")) {
                payload.classifications = [{ value: this.classificationSelection }];
            }
        }
        return payload;
    }

    private addItem(payload: { value: string, description?: string, category?: { value: string }, classification?: { value: string } }, successCb: () => void) {
        this.inventoryService.addListData(this.otModalData.listId, payload).then((response: IProtocolResponse<{ data: IInventoryListItem }>) => {
            this.handleResponse(response, successCb);
        });
    }

    private updateItem(payload: { value: string, description?: string, category?: { value: string }, classification?: { value: string } }, successCb: () => void) {
        this.inventoryService.updateListData(this.otModalData.listId, this.otModalData.listItem.id, payload).then((response: IProtocolResponse<{ data: IInventoryListItem }>) => {
            this.handleResponse(response, successCb);
        });
    }

    private handleResponse(response: IProtocolResponse<{ data: IInventoryListItem }>, successCb: () => void) {
        if (response.result) {
            const promises = [this.tenantTranslationService.refreshTermCurrentLanguage(response.data.data.nameKey)];
            if (response.data.data.descriptionKey) promises.push(this.tenantTranslationService.refreshTermCurrentLanguage(response.data.data.descriptionKey));
            Promise.all(promises).then(() => {
                successCb();
                this.isSaving = false;
            });
        } else {
            this.isSaving = false;
        }
    }
}
