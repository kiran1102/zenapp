// 3rd Party
import {
    Component,
    OnInit,
    OnDestroy,
    Inject,
    ViewChild,
} from "@angular/core";
import { isEmpty } from "lodash";
import {
    TransitionService,
    Transition,
    HookResult,
    StateService,
} from "@uirouter/angularjs";
import { Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";

// Redux
import { StoreToken } from "tokens/redux-store.token";
import {
    getCurrentRecord,
    getInventoryId,
} from "oneRedux/reducers/inventory.reducer";
import {
    getIsEditingRecord,
    getCurrentRecordName,
    getIsLoadingRecord,
    getHasErrors,
    getHasChanges,
    getHasRequiredValues,
    getRecord,
    getCurrentInventoryOrgGroupId,
} from "oneRedux/reducers/inventory-record.reducer";
import { getAttachmentList } from "oneRedux/reducers/attachment.reducer";

// Services
import { InventoryLineageService } from "inventoryModule/inventory-lineage/shared/services/inventory-lineage.service";
import { InventoryActionService } from "inventoryModule/services/action/inventory-action.service";
import { AttachmentActionService } from "oneServices/actions/attachment-action.service";
import { InventoryRiskV2Service } from "modules/inventory/services/adapter/inventory-risk-v2.service";
import { InventoryService } from "inventorySharedModule/shared/services/inventory-api.service";
import { ModalService } from "sharedServices/modal.service";
import { RiskActionService } from "oneServices/actions/risk-action.service";
import { InventoryRiskService } from "modules/inventory/services/adapter/inventory-risk.service";
import { AssessmentBulkLaunchActionService } from "sharedModules/services/actions/bulk-launch-assessment-action.service";
import { RecordActionService } from "inventoryModule/services/action/record-action.service";
import { Permissions } from "modules/shared/services/helper/permissions.service";
import { InventoryAdapterV2Service } from "inventoryModule/services/adapter/inventory-adapter-v2.service";
import { AssessmentListApiService } from "modules/assessment/services/api/assessment-list-api.service";
import { InventoryListActionService } from "inventoryModule/services/action/inventory-list-action.service";
import { InventoryActivityService } from "inventoryModule/inventory-activity/shared/services/inventory-activity.service";
import { InventoryLaunchModalService } from "inventoryModule/services/action/inventory-launch-modal.service";
import { ReportTemplateExportService } from "modules/reports/reports-shared/services/report-template-export.service";

// Constants & Enums
import {
    InventoryContexts,
    InventoryTableIds,
    InventoryStatusOptions,
    ChangeInventoryStatusCloseAction,
} from "enums/inventory.enum";
import {
    InventoryPermissions,
    InventoryDetailsTabs,
    InventoryListRoutes,
    InventoryAttributeCodes,
    ShowVendorControlList,
    AttributeFieldNames,
} from "constants/inventory.constant";
import { AttachmentTypes } from "enums/attachment-types.enum";
import {
    InventoryTypeTranslations,
    NewInventorySchemaIds,
} from "constants/inventory-config.constant";
import { ControlPermissions } from "linkControlsAPIModule/constants/link-controls.constant";
import { ModuleTypes } from "enums/module-types.enum";
import { ReportTypeLowercase } from "reportsModule/reports-shared/enums/reports.enum";

// Interfaces
import { IAttachmentModalResolve } from "interfaces/attachment-modal-resolve.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";
import { IStringMap } from "interfaces/generic.interface";
import {
    IInventoryRecord,
    IInventoryAssessment,
    ILaunchResponse,
    IInventoryRecordV2,
    IAttributeOptionV2,
} from "interfaces/inventory.interface";
import {
    IValueId,
    INumberMap,
} from "interfaces/generic.interface";
import {
    IStore,
    IStoreState,
} from "interfaces/redux.interface";
import { ISaveChangesModalResolve } from "interfaces/save-changes-modal-resolve.interface";
import { ITabsNav } from "interfaces/one-tabs-nav.interface";
import { IDropdownOption } from "interfaces/one-dropdown.interface";
import { IRiskDetails } from "interfaces/risk.interface";
import { IAssessmentCreationDetails } from "interfaces/assessment/assessment-creation-details.interface";
import { IPageableOf } from "interfaces/pagination.interface";
import { IAttachmentFileResponse } from "interfaces/attachment-file.interface";
import { IInventoryLineageState } from "inventoryModule/inventory-lineage/shared/interfaces/inventory-lineage.interface";
import { IReportLayout } from "reportsModule/reports-shared/interfaces/report.interface";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";
import { InventoryAttachmentsWrapperComponent } from "modules/inventory/components/inventory-attachments-wrapper/inventory-attachments-wrapper.component";

@Component({
    selector: "inventory-details",
    templateUrl: "./inventory-details.component.html",
})
export class InventoryDetails implements OnInit, OnDestroy {

    @ViewChild(InventoryAttachmentsWrapperComponent) inventoryAttachmentsWrapperComponent: InventoryAttachmentsWrapperComponent;
    usersValid: boolean;
    schemaIds = NewInventorySchemaIds;
    showVendorControlList = ShowVendorControlList;
    savingRecord = false;
    isLoadingDetails = false;
    inventoryTranslationKey: string;
    recordEditEnabled = false;
    tabs: ITabsNav[];
    showDropdown: boolean;
    currentTab: string;
    isLoadingAssessments: boolean;
    isLoadingRisks: boolean;
    isLoadingActivity: boolean;
    showVendorCertificates: boolean;
    context: number;
    orgGroupId: string;
    recordName: string;
    tabIds: IStringMap<string> = InventoryDetailsTabs;
    record: IInventoryRecord;
    recordV2: IInventoryRecordV2;
    inventoryPermissions: IStringMap<boolean>;
    listRoute: string;
    inventoryListRoutes = InventoryListRoutes;
    contractDisplayParams: IStringMap<number | string>;
    reloadRisks$ = new Subject();
    inventoryTableIds = InventoryTableIds;
    inventoryId: number;
    attachmentList: IPageableOf<IAttachmentFileResponse>;
    allowCreateAssessment: boolean;
    lineageReady: boolean;
    defaultExportLayout: IReportLayout;
    dropdownOptions: IDropdownOption[];

    private unsub: () => void;
    private recordId: string;
    private hasEdits = false;
    private requiredFieldsValid = false;
    private inventoryTranslationKeys: INumberMap<string> = InventoryTypeTranslations;
    private transitionDeregisterHook: () => any;
    private destroy$: Subject<void> = new Subject();

    constructor(
        @Inject(StoreToken) public store: IStore,
        private stateService: StateService,
        private $transitions: TransitionService,
        private translatePipe: TranslatePipe,
        private inventoryAction: InventoryActionService,
        private attachmentAction: AttachmentActionService,
        private inventoryLineageService: InventoryLineageService,
        private permissions: Permissions,
        private modalService: ModalService,
        private inventoryService: InventoryService,
        private inventoryRiskV2Service: InventoryRiskV2Service,
        private inventoryListAction: InventoryListActionService,
        private riskAction: RiskActionService,
        private inventoryRisk: InventoryRiskService,
        private assessmentBulkLaunchActionService: AssessmentBulkLaunchActionService,
        private recordActionService: RecordActionService,
        private inventoryAdapterV2Service: InventoryAdapterV2Service,
        private assessmentListApi: AssessmentListApiService,
        private inventoryActivityService: InventoryActivityService,
        private inventoryLaunchModalService: InventoryLaunchModalService,
        private reportTemplateExportService: ReportTemplateExportService,
    ) { }

    ngOnInit(): void {
        this.inventoryAction.setInventoryId(Number(this.stateService.params.inventoryId));
        this.inventoryActivityService.setDefaultActivity();
        this.inventoryLineageService.setDefaultState();
        this.inventoryAction.setDefaultRelatedInventories();
        this.recordId = this.stateService.params.recordId;
        this.inventoryId = Number.parseInt(this.stateService.params.inventoryId, 10);
        this.inventoryTranslationKey = this.inventoryTranslationKeys[this.inventoryId];
        this.inventoryPermissions = {
            canLaunchAssessments: this.permissions.canShow(InventoryPermissions[this.inventoryId].launch),
            canLaunchVendorAssessment: this.permissions.canShow(InventoryPermissions[this.inventoryId].launchVendorAssessment),
            canEditDetails: this.permissions.canShow(InventoryPermissions[this.inventoryId].edit),
            canDelete: this.permissions.canShow(InventoryPermissions[this.inventoryId].delete),
            canViewAssessments: this.permissions.canShow(InventoryPermissions[this.inventoryId].viewAssessmentLinks),
            canAddAssessmentLinks: this.permissions.canShow(InventoryPermissions[this.inventoryId].addAssessmentLink),
            canViewAttachments: this.permissions.canShow(InventoryPermissions[this.inventoryId].attachments),
            canCreateAttachments: this.permissions.canShow(InventoryPermissions[this.inventoryId].createAttachments),
            canDownloadAttachments: this.permissions.canShow(InventoryPermissions[this.inventoryId].attachments),
            canDeleteAttachments: this.permissions.canShow(InventoryPermissions[this.inventoryId].deleteAttachments),
            canAddRisks: this.permissions.canShow(InventoryPermissions[this.inventoryId].addRisk),
            canViewDetails: this.permissions.canShow(InventoryPermissions[this.inventoryId].details),
            canViewRisks: this.permissions.canShow(InventoryPermissions[this.inventoryId].viewRisks),
            canDeleteAssessments: this.permissions.canShow(InventoryPermissions[this.inventoryId].deleteAssessments),
            canViewActivity: this.permissions.canShow(InventoryPermissions[this.inventoryId].viewActivity),
            canViewLineage: this.permissions.canShow(InventoryPermissions[this.inventoryId].viewLineage),
            canCopy: this.permissions.canShow(InventoryPermissions[this.inventoryId].copy),
            canRemoveAssessmentLinks: this.permissions.canShow(InventoryPermissions[this.inventoryId].removeAssessmentLinks),
            canExportPdf: this.permissions.canShow(InventoryPermissions[this.inventoryId].exportPdf),
            canViewLinkedInventory: this.permissions.canShow(InventoryPermissions[this.inventoryId].inventoryLinkingV2),
            canViewDocuments: this.permissions.canShow(InventoryPermissions[this.inventoryId].vendorDocumentTab),
            canViewRelated: this.permissions.canShow(InventoryPermissions[this.inventoryId].viewRelated),
            canViewVendorContractInventoryLink: this.permissions.canShow(InventoryPermissions[this.inventoryId].vendorContractInventoryLink),
            canVendorContractInventoryLinkAdd: this.permissions.canShow(InventoryPermissions[this.inventoryId].vendorContractInventoryLinkAdd),
            canVendorContractInventoryLinkRemove: this.permissions.canShow(InventoryPermissions[this.inventoryId].vendorContractInventoryLinkRemove),
            canShowCertificatesTab: this.permissions.canShow(InventoryPermissions[this.inventoryId].certificatesPanel),
            canViewControlTab: this.permissions.canShow(ControlPermissions[this.schemaIds[this.inventoryId]].viewControlTab),
            canUpdateRelatedInventory: this.permissions.canShow(InventoryPermissions[this.inventoryId].updateRelatedInventory),
            canLaunchV2Assessment: this.permissions.canShow(InventoryPermissions[this.inventoryId].launchV2Assessment),
            canChangeInventoryStatus: this.permissions.canShow(InventoryPermissions[this.inventoryId].changeInventoryStatus),
            canUseDefaultForExport: this.permissions.canShow(InventoryPermissions[this.inventoryId].setDefaultPdf),
        };
        if (this.inventoryPermissions.canExportPdf && this.inventoryPermissions.canUseDefaultForExport) {
            this.reportTemplateExportService.getDefaultLayout(ReportTypeLowercase.PDF, ModuleTypes.INV)
                .then((response: IProtocolResponse<IReportLayout>) => {
                    this.defaultExportLayout = response.data;
                });
        }
        this.inventoryLineageService.inventoryLineageState$.pipe(takeUntil(this.destroy$)).subscribe((stateData: IInventoryLineageState) => {
            this.lineageReady = stateData.ready;
        });
        this.tabs = this.getTabs();
        this.currentTab = this.stateService.params.tabId;
        this.getCurrentTab(this.stateService.params.tabId);
        this.inventoryAction.getAllRecordDetails(this.recordId, this.inventoryPermissions);
        this.recordActionService.setRecordAndSchemaById(NewInventorySchemaIds[this.inventoryId], this.recordId);
        this.context = InventoryContexts.Details;
        this.unsub = this.store.subscribe((): void => this.componentWillReceiveState());
        this.componentWillReceiveState();
        this.inventoryAction.recordEditDisabled();
        this.setTransitionAwayHook();
        this.listRoute = InventoryListRoutes[this.inventoryId];
        this.showVendorCertificates = this.inventoryPermissions.canShowCertificatesTab;
        this.assessmentListApi.getSspRestrictViewSetting().then((response): void => {
            if (response) {
                const restrictViewSetting = response.data && response.data.restrictProjectOwnerView;
                if (this.permissions.canShow("AssessmentCreate")) {
                    this.allowCreateAssessment = this.permissions.canShow("BypassSelfServicePortal") || !restrictViewSetting;
                } else {
                    this.allowCreateAssessment = false;
                }
            }
        });
    }

    ngOnDestroy(): void {
        this.unsub();
        if (this.transitionDeregisterHook) {
            this.transitionDeregisterHook();
        }
        this.destroy$.next();
        this.destroy$.complete();
    }

    getCurrentTab(tabId: string) {
        if (!this.tabs.find((tab) => tab.id === tabId)) {
            this.goToTab(this.tabs[0].id);
        }
    }

    disabledCallback(attributeCode: string): void | undefined {
        if (this.inventoryService.attributeIsRelated(attributeCode, this.inventoryId)) {
            this.goToTab(InventoryDetailsTabs.Related);
        } else if (attributeCode === InventoryAttributeCodes[this.inventoryId].Risk) {
            this.goToTab(InventoryDetailsTabs.Risks);
        } else {
            return;
        }
    }

    goToAssessment(assessment: IInventoryAssessment): void {
        this.stateService.go(assessment.route, assessment.routeParams);
    }

    goToListView(route: { path: string, params: any }) {
        const formatedInventoryId = this.inventoryId === InventoryTableIds.Vendors ? this.inventoryId : NewInventorySchemaIds[this.inventoryId];
        this.stateService.go(route.path, { Id: formatedInventoryId, page: null, size: null });
    }

    viewRiskTab(): void {
        if (this.inventoryPermissions.canViewRisks) {
            this.goToTab(InventoryDetailsTabs.Risks);
        }
    }

    private setTransitionAwayHook(): void {
        let exitRoute: string;
        if (this.inventoryId === InventoryTableIds.Vendors && this.stateService.params.tabId === InventoryDetailsTabs.Details) {
            exitRoute = "zen.app.pia.module.vendor.inventory.details";
        } else if (this.inventoryId === InventoryTableIds.Vendors && this.stateService.params.tabId === InventoryDetailsTabs.Assessments) {
            exitRoute = "zen.app.pia.module.vendor.inventory.assessments";
        } else {
            exitRoute = "zen.app.pia.module.datamap.views.inventory.details";
        }

        this.transitionDeregisterHook = this.$transitions.onExit({ from: exitRoute }, (transition: Transition): HookResult => {
            return this.handleStateChange(transition.to(), transition.params("to"), transition.options);
        }) as any;
    }

    private getTabs(): ITabsNav[] {
        const tabs: ITabsNav[] = [];
        if (this.inventoryPermissions.canViewDetails) {
            tabs.push({
                id: this.tabIds.Details,
                text: this.translatePipe.transform("Details"),
                action: (option: ITabsNav): void => { this.goToTab(option.id); },
            });
        }
        if (this.inventoryPermissions.canViewAssessments) {
            tabs.push({
                id: this.tabIds.Assessments,
                text: this.translatePipe.transform("Assessments"),
                action: (option: ITabsNav): void => { this.goToTab(option.id); },
            });
        }
        if (this.inventoryPermissions.canViewRisks) {
            tabs.push({
                id: this.tabIds.Risks,
                text: this.translatePipe.transform("Risks"),
                action: (option: ITabsNav): void => { this.goToTab(option.id); },
            });
        }
        if (this.inventoryPermissions.canViewAttachments && !this.inventoryPermissions.canViewVendorContractInventoryLink && (this.inventoryId !== InventoryTableIds.Vendors
            || !this.inventoryPermissions.canViewDocuments)
        ) {
            tabs.push({
                id: this.tabIds.Attachments,
                text: this.translatePipe.transform("Attachments"),
                action: (option: ITabsNav): void => {
                    this.goToTab(option.id);
                },
            });
        }
        if ((this.inventoryPermissions.canViewDocuments && this.inventoryId === InventoryTableIds.Vendors) || this.inventoryPermissions.canViewVendorContractInventoryLink) {
            tabs.push({
                id: this.tabIds.Documents,
                text: this.translatePipe.transform("Documents"),
                action: (option: ITabsNav): void => {
                    this.goToTab(option.id);
                },
            });
        }
        if (this.inventoryPermissions.canViewRelated) {
            tabs.push({
                id: this.tabIds.Related,
                text: this.translatePipe.transform("Related"),
                action: (option: ITabsNav): void => { this.goToTab(option.id); },
            });
        }
        if (this.inventoryPermissions.canViewLineage) {
            tabs.push({
                id: this.tabIds.Lineage,
                text: this.translatePipe.transform("Lineage"),
                action: (option: ITabsNav): void => { this.goToTab(option.id); },
            });
        }
        if (this.inventoryPermissions.canViewControlTab) {
            tabs.push({
                id: this.tabIds.Controls,
                text: this.translatePipe.transform("Controls"),
                action: (option: ITabsNav): void => { this.goToTab(option.id); },
            });
        }
        if (this.inventoryPermissions.canShowCertificatesTab) {
            tabs.push({
                id: this.tabIds.Certificates,
                text: this.translatePipe.transform("Certificates"),
                action: (option: ITabsNav): void => { this.goToTab(option.id); },
            });
        }
        if (this.inventoryPermissions.canViewActivity) {
            tabs.push({
                id: this.tabIds.Activity,
                text: this.translatePipe.transform("Activity"),
                action: (option: ITabsNav): void => { this.goToTab(option.id); },
            });
        }
        return tabs;
    }

    private setDropdownOptions(): IDropdownOption[] {
        const dropdownOptions: IDropdownOption[] = [];
        if (
            this.allowCreateAssessment &&
            (this.inventoryPermissions.canLaunchAssessments || this.inventoryPermissions.canLaunchVendorAssessment) &&
            (this.inventoryPermissions.canViewLinkedInventory || !this.record.assessmentOutstanding)
        ) {
            dropdownOptions.push({
                text: this.translatePipe.transform("LaunchAssessment"),
                action: (): void => { this.handleAssessmentLaunch(); },
            });
        }
        if (this.inventoryPermissions.canAddRisks) {
            dropdownOptions.push({
                text: this.translatePipe.transform("AddRisk"),
                action: (): void => {
                    this.inventoryAction.openAddRiskModal(
                        this.recordId,
                        this.recordName,
                        this.inventoryId,
                        getCurrentInventoryOrgGroupId(this.store.getState()),
                        (risk: IRiskDetails): void => {

                            if (this.permissions.canShow("RiskInheritance")) {
                                this.reloadRisks$.next();
                            }

                            this.riskAction.addRisk(risk, "id");
                            this.inventoryRiskV2Service.handleRiskChange(this.recordV2, this.inventoryId, risk, true);
                        });
                },
            });
        }
        if (this.inventoryPermissions.canAddAssessmentLinks) {
            dropdownOptions.push({
                text: this.translatePipe.transform("LinkAssessment"),
                action: (): void => {
                    this.inventoryAction.addAssessmentLink(this.recordId,
                        (): void => this.inventoryAction.reloadAssessmentList(this.recordId),
                        this.inventoryId, this.recordName);
                },
            });
        }
        if (!this.inventoryPermissions.canViewVendorContractInventoryLink
            && (this.inventoryPermissions.canCreateAttachments && this.inventoryId !== InventoryTableIds.Vendors)
            || !this.inventoryPermissions.canViewDocuments
        ) {
            dropdownOptions.push({
                text: this.translatePipe.transform("AddAttachment"),
                action: (): void => {
                    if (!this.inventoryPermissions.canViewDocuments) {
                        if ((this.currentTab === this.tabIds.Attachments || this.currentTab === this.tabIds.Documents) && this.inventoryPermissions.canViewAttachments) {
                            this.addAttachment(this.recordId, (): void => this.attachmentAction.fetchAttachmentList(this.inventoryId, this.attachmentList.number, this.attachmentList.size));
                        } else {
                            this.addAttachment(this.recordId);
                        }

                    } else if (this.inventoryAttachmentsWrapperComponent) {
                        this.inventoryAttachmentsWrapperComponent.addDocument();
                    } else {
                        this.addAttachment(this.recordId);
                    }
                },
            });
        }
        if (
            this.inventoryPermissions.canDelete &&
            (this.inventoryPermissions.canViewLinkedInventory || !this.record.assessmentOutstanding)
        ) {
            dropdownOptions.push({
                text: this.translatePipe.transform("DeleteRecord"),
                action: (): void => { this.handleDelete(); },
            });
        }
        if (this.inventoryPermissions.canCopy && this.inventoryPermissions.canViewLinkedInventory) {
                dropdownOptions.push({
                    text: this.translatePipe.transform("Copy"),
                    action: (): void => { this.inventoryListAction.launchCopyModal(this.inventoryId, this.recordV2); },
                });
        }
        if (this.inventoryPermissions.canCopy && !this.inventoryPermissions.canViewLinkedInventory) {
            dropdownOptions.push({
                text: this.translatePipe.transform("Copy"),
                action: (): void => { this.inventoryAction.launchCopyModal(this.recordId); },
            });
        }
        if (this.inventoryPermissions.canExportPdf) {
            dropdownOptions.push({
                text: this.translatePipe.transform("ExportAsPDF"),
                action: (): void => {
                    this.inventoryLaunchModalService.launchNotificationModal("ReportDownload", this.translatePipe.transform("ReportIsGeneratedCheckTasksBar"));
                    if (this.inventoryPermissions.canUseDefaultForExport) {
                        this.reportTemplateExportService.export(this.defaultExportLayout, this.recordName, this.recordId, ModuleTypes.INV);
                    } else {
                        this.inventoryListAction.exportPDF(this.recordId, this.recordName, this.inventoryId);
                    }
                },
            });
        }
        if (this.inventoryPermissions.canChangeInventoryStatus) {
            dropdownOptions.push({
                text: this.translatePipe.transform("ChangeStatus"),
                action: (): void => { this.handleChangeInventoryStatus(); },
            });
        }
        return dropdownOptions;
    }

    private handleChangeInventoryStatus() {
        const selectedStatus = this.recordV2[AttributeFieldNames.Status] ? (this.recordV2[AttributeFieldNames.Status] as IAttributeOptionV2).value as InventoryStatusOptions : undefined;
        this.inventoryLaunchModalService.launchStatusChangeModal(
            NewInventorySchemaIds[this.inventoryId],
            [this.recordId],
            ChangeInventoryStatusCloseAction.SET_RECORD,
            null,
            selectedStatus,
            this.recordName,
            this.recordActionService.setCurrentRecordStatus,
        );
    }

    private handleAssessmentLaunch(): void {
        const bulkInventoryList = this.inventoryAdapterV2Service.getRecordAssignments([this.recordV2], this.inventoryId);
        if ((this.inventoryPermissions.canLaunchVendorAssessment || (this.inventoryPermissions.canLaunchAssessments && this.inventoryPermissions.canLaunchV2Assessment)) && this.allowCreateAssessment) {
            const bulkAssessmentDetails: IAssessmentCreationDetails[] = this.assessmentBulkLaunchActionService.getAssessmentCreationItems(bulkInventoryList, this.inventoryId);
            this.assessmentBulkLaunchActionService.setBulkLaunchAssessments(bulkAssessmentDetails);
            this.stateService.go(
                "zen.app.pia.module.assessment.bulk-launch-inventory-assessment",
                {
                    inventoryTypeId: this.inventoryId,
                    recordId: this.stateService.params.recordId,
                    tabId: this.stateService.params.tabId,
                },
            );
        } else {
            const actionCallback = (response: IProtocolResponse<ILaunchResponse[]>): void => {
                if (response.result) {
                    this.inventoryAction.reloadAssessmentList(this.recordId);
                    this.dropdownOptions = this.setDropdownOptions();
                    this.showDropdown = !isEmpty(this.dropdownOptions);
                }
            };
            if (this.hasEdits && this.requiredFieldsValid) {
                const callback = (): void => {
                    setTimeout((): void => {
                        this.inventoryAction.launchAssessments(this.recordName, bulkInventoryList, actionCallback, 0);
                    }, 0);
                };
                this.launchSaveChangesModal(callback);
            } else {
                this.inventoryAction.launchAssessments(this.recordName, bulkInventoryList, actionCallback, 0);
            }
        }
    }

    private handleDelete(): void {
        const callback = (response: IProtocolResponse<IStringMap<string>>): void => {
            if (response.result) {
                this.inventoryAction.recordEditDisabled();
                const formatedInventoryId = this.inventoryId === InventoryTableIds.Vendors ? this.inventoryId : NewInventorySchemaIds[this.inventoryId];
                this.stateService.go(InventoryListRoutes[this.inventoryId], { Id: formatedInventoryId });
            }
        };
        this.inventoryAction.deleteRecord(this.recordId, callback);
    }

    private componentWillReceiveState(): void {
        const store: IStoreState = this.store.getState();
        if (!this.inventoryPermissions.canViewLinkedInventory) {
            this.record = getCurrentRecord(store);
        }
        this.inventoryId = getInventoryId(store);
        this.recordV2 = getRecord(store);

        if (this.recordV2 && this.recordV2.id === this.recordId) {
            this.recordName = getCurrentRecordName(store);
            this.showDropdown = !isEmpty(this.dropdownOptions);
            this.orgGroupId = this.recordV2.organization && (this.recordV2.organization as IValueId<string>).id ? (this.recordV2.organization as IValueId<string>).id : "";

            if (this.inventoryPermissions.canViewLinkedInventory || this.record) {
                this.dropdownOptions = this.setDropdownOptions();
            }
        }

        this.attachmentList = getAttachmentList(store);

        // use v2 store
        this.savingRecord = getIsLoadingRecord(store);
        this.hasEdits = getHasChanges(store);
        this.requiredFieldsValid = getHasRequiredValues(store);
        this.usersValid = !getHasErrors(store);
        this.isLoadingDetails = getIsLoadingRecord(store);
        this.recordEditEnabled = getIsEditingRecord(store);
    }

    private launchSaveChangesModal(callback: () => void): void {
        const modalData: ISaveChangesModalResolve = {
            modalTitle: this.translatePipe.transform("SaveChanges"),
            confirmationText: this.translatePipe.transform("UnsavedChangesSaveBeforeContinuing"),
            cancelButtonText: this.translatePipe.transform("Cancel"),
            discardChangesButtonText: this.translatePipe.transform("Discard"),
            saveButtonText: this.translatePipe.transform("Save"),
            promiseToResolve: () => this.saveRecord().then((): boolean => {
                callback();
                return true;
            }),
            discardCallback: () => {
                this.saveCancelled();
                callback();
                return true;
            },
        };
        this.modalService.setModalData(modalData);
        this.modalService.openModal("saveChangesModal");
        return;
    }

    private addAttachment(recordId: string, callback?: () => void): void {
        const attachmentModalData: IAttachmentModalResolve = {
            showFileList: true,
            allowAttachmentNaming: true,
            allowComments: true,
            attachmentType: AttachmentTypes.Inventory,
            requestId: recordId,
            inventoryId: this.inventoryId,
            callback: callback as any,
        };
        this.attachmentAction.addAttachment(attachmentModalData);
    }

    private handleStateChange(toState: IStringMap<any>, toParams: IStringMap<any>, options: IStringMap<any>): boolean {
        if (!this.hasEdits || !this.requiredFieldsValid || toParams.tabId) return true;
        const callback = (): void => {
            this.stateService.go(toState, toParams, options);
        };
        this.launchSaveChangesModal(callback);
        return false;
    }

    private saveRecord(): ng.IPromise<boolean> {
        return this.recordActionService.saveRecordChanges(NewInventorySchemaIds[this.inventoryId], this.recordId).then((response: boolean): boolean => {
            return response;
        });
    }

    private saveCancelled(): void {
        this.recordActionService.toggleEditingRecord(false);
    }

    private goToTab(tabId: string): void {
        const routeParams: { inventoryId: number, recordId: string, tabId: string } = {
            inventoryId: this.inventoryId,
            recordId: this.recordId,
            tabId,
        };
        this.stateService.go(".", routeParams);
        this.currentTab = tabId;
        if (this.recordV2 && this.recordId !== this.recordV2.id) {
            this.recordActionService.setRecordAndSchemaById(NewInventorySchemaIds[this.inventoryId], this.recordId);
        }
    }
}
