// 3rd party
import {
    Component,
    Inject,
} from "@angular/core";

// Redux
import { getModalData } from "oneRedux/reducers/modal.reducer";

// Services
import { ModalService } from "sharedServices/modal.service";

// Tokens
import { StoreToken } from "tokens/redux-store.token";

// Interfaces
import { IStore } from "interfaces/redux.interface";

@Component({
    selector: "bulk-edit-confirmation-modal",
    templateUrl: "./bulk-edit-confirmation-modal.component.html",
})
export class BulkEditConfirmationModalComponent {

    modalData: { warningMessage: string, callback: (boolean) => void };

    constructor(
        @Inject(StoreToken) private store: IStore,
        private modalService: ModalService,
    ) { }

    ngOnInit(): void {
        this.modalData = getModalData(this.store.getState());
    }

    closeModal(): void {
        this.modalService.handleModalCallback(false);
        this.modalService.closeModal();
        this.modalService.clearModalData();
    }

    submitChanges(): void {
        this.modalService.handleModalCallback(true);
        this.closeModal();
    }

}
