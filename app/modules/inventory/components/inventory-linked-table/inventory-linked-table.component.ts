// 3rd party
import {
    Component,
    Inject,
    Input,
    Output,
    EventEmitter,
    SimpleChanges,
} from "@angular/core";
import { StateService } from "@uirouter/core";
import { findIndex, includes } from "lodash";
import { TranslatePipe } from "pipes/translate.pipe";

// Redux
import {
    getRelatedInventories,
    getRelatedInventoryPageNumber,
    getRelatedInventoryPageSize,
} from "oneRedux/reducers/inventory.reducer";

// Services
import { InventoryActionService } from "inventoryModule/services/action/inventory-action.service";
import { InventoryLinkingService } from "modules/inventory/services/adapter/inventory-linking.service";
import { InventoryLaunchModalService } from "inventoryModule/services/action/inventory-launch-modal.service";
import { Permissions } from "modules/shared/services/helper/permissions.service";

// Interfaces
import {
    IRelatedRecord,
    IRelatedTable,
    ILinkedInventoryRow,
    IRelatedTableRow,
    IFormattedPersonalDataTableRow,
    ILinkPersonalData,
    IPersonalDataItem,
} from "interfaces/inventory.interface";

import { IStringMap, INumberMap } from "interfaces/generic.interface";
import { IDropdownOption } from "interfaces/one-dropdown.interface";
import { IPaginatedResponse } from "interfaces/pagination.interface";
import { IStore } from "interfaces/redux.interface";
import { IRelateInventoryModalResolve } from "interfaces/relate-inventory-modal.interface";

// constants + enums
import { CellTypes } from "enums/general.enum";
import { InventoryTableIds, InventoryContexts } from "enums/inventory.enum";
import { NewInventorySchemaIds } from "constants/inventory-config.constant";
import {
    InventoryTranslationKeys,
    InventoryPermissions,
    InventoryDetailRoutes,
} from "constants/inventory.constant";

// Tokens
import { StoreToken } from "tokens/redux-store.token";

@Component({
    selector: "inventory-linked-table",
    templateUrl: "./inventory-linked-table.component.html",
})
export class InventoryLinkedTableComponent {

    @Input() inventory: IRelatedTable;
    @Input() type: number;
    @Input() linkedType: string;
    @Input() recordId: string;
    @Input() recordName: string;
    @Input() orgGroupId: string;
    @Input() isExpanded: boolean;
    @Input() resolvedPermissions;
    @Output() toggle = new EventEmitter();
    @Output() viewRecord: EventEmitter<IRelatedTableRow> = new EventEmitter<IRelatedTableRow>();

    cellTypes = CellTypes;
    clickPreventClass = "prevent-row-click";
    config: IStringMap<number> = {
        defaultSize: 5,
        expandedSize: 20,
    };
    hasRelatedViewPermission: boolean;
    isReady = false;
    menuClass: string;
    relateInventoryText: string;
    relatedInventories: INumberMap<IRelatedTable>;
    tableIds = InventoryTableIds;
    currentPageNumber: number;
    currentPageSize: number;

    constructor(
        @Inject(StoreToken) private store: IStore,
        private stateService: StateService,
        private inventoryLinkingService: InventoryLinkingService,
        private inventoryLaunchModalService: InventoryLaunchModalService,
        private inventoryAction: InventoryActionService,
        private permissions: Permissions,
        private translatePipe: TranslatePipe,
    ) {}

    checkIfRowIsDisabled = (row: ILinkedInventoryRow): boolean => false;

    ngOnInit() {
        const state = this.store.getState();
        this.relatedInventories = getRelatedInventories(state);

        this.relateInventoryText = this.translatePipe.transform(InventoryTranslationKeys[this.type].addRelatedInventoryHeader);

        this.isReady = true;
        if (!this.isExpanded && this.relatedInventories[this.type].rows && this.relatedInventories[this.type].rows.length > this.config.defaultSize) {
            this.pageChange(0);
        }
        this.hasRelatedViewPermission = this.permissions.canShow(InventoryPermissions[this.type].details);
        this.linkedType = NewInventorySchemaIds[this.stateService.params.inventoryId];
    }

    ngOnChanges(changes: SimpleChanges) {
        if (changes.isExpanded && !changes.isExpanded.firstChange) {
            this.pageChange(0);
        }
    }

    getActions(row: ILinkedInventoryRow | IFormattedPersonalDataTableRow): () => IDropdownOption[] {
        return (): IDropdownOption[] => {
            this.setMenuClass(row);
            const actions: IDropdownOption[] = [];
            if (this.resolvedPermissions.canUpdateRelatedInventory) {
                actions.push({
                    text: this.translatePipe.transform("Remove"),
                    action: () => { this.unlinkRecord(row); },
                });
            }
            if (!actions.length) {
                actions.push({
                    text: this.translatePipe.transform("NoActionsAvailable"),
                    action: () => { }, // Keep Empty
                });
            }
            return actions;
        };
    }

    unlinkRecord(row: ILinkedInventoryRow | IFormattedPersonalDataTableRow) {
        const callback = () => {
            this.currentPageNumber = getRelatedInventoryPageNumber(this.store.getState());
            this.currentPageSize = getRelatedInventoryPageSize(this.store.getState());
            this.currentPageNumber = this.currentPageSize !== 1 || this.currentPageNumber === 0 ? this.currentPageNumber : this.currentPageNumber - 1;
            (this.pageChange(this.currentPageNumber) as Promise<IPaginatedResponse<IPersonalDataItem>>).then((response: IPaginatedResponse<IPersonalDataItem>) => {
                if (response.data && response.data.length <= this.config.defaultSize && this.isExpanded && this.currentPageNumber === 0) {
                    this.toggleExpanded();
                }
            });
            this.inventoryAction.getLinkedInventory(this.recordId, InventoryContexts.Lineage);
        };
        if (this.permissions.canShow("EnableDataSubjectsElementsV2") && this.type === InventoryTableIds.Elements) {
            const unlinkParams: ILinkPersonalData[] = [{ dataElement: { id: (row as IFormattedPersonalDataTableRow).dataElement.id }, dataSubjectType: { id: (row as IFormattedPersonalDataTableRow).dataSubjectType.id } }];
            this.inventoryAction.unlinkPersonalData(this.recordId, unlinkParams, callback);
        } else {
            const unlinkParams: Array<{ inventoryAssociationId: string }> = [{ inventoryAssociationId: (row as ILinkedInventoryRow).inventoryAssociationId }];
            this.inventoryAction.unlinkInventoryRecord(this.recordId, unlinkParams, callback);
        }
    }

    addInventoryRelationship() {
        const modalData: IRelateInventoryModalResolve = {
            inventoryType: this.type,
            linkedType: this.linkedType,
            recordId: this.recordId,
            recordName: this.recordName,
            orgGroupId: this.orgGroupId,
            callback: () => {
                this.currentPageNumber = getRelatedInventoryPageNumber(this.store.getState());
                this.pageChange(this.currentPageNumber);
                if (this.recordId) {
                    this.inventoryAction.getLinkedInventory(this.recordId, InventoryContexts.Lineage);
                }
            },
        };
        if (this.type === InventoryTableIds.Elements) {
            if (this.permissions.canShow("EnableDataSubjectsElementsV2")) {
                this.inventoryLaunchModalService.launchLinkPersonalDataModal(modalData);
            } else {
                this.inventoryLinkingService.addDataSubjectRelationship(modalData);
            }
        } else {
            this.inventoryLinkingService.addInventoryRelationship(modalData);
        }
    }

    columnTrackBy(column: IRelatedRecord): string {
        return column.key;
    }

    toggleExpanded() {
        this.isReady = false;
        this.toggle.emit();
    }

    goToRecord(row: ILinkedInventoryRow) {
        this.stateService.go(InventoryDetailRoutes[this.type], { inventoryId: this.type, recordId: row.inventoryId, tabId: "details" });
    }

    pageChange(page: number, size?: number): Promise<IPaginatedResponse<ILinkedInventoryRow | IPersonalDataItem>> {
        this.isReady = false;
        const pageSize: number = size || this.isExpanded ? this.config.expandedSize : this.config.defaultSize;
        const params: IStringMap<number> = { size: pageSize, page };
        if (this.permissions.canShow("EnableDataSubjectsElementsV2") && this.type === InventoryTableIds.Elements) {
            return this.inventoryLinkingService.getLinkedPersonalData(this.recordId, this.type, InventoryContexts.Related, params).then((response: IPaginatedResponse<IPersonalDataItem>): IPaginatedResponse<IPersonalDataItem> => {
                this.isReady = true;
                this.inventoryAction.setRelatedInventoryPage(page, response.data.length);
                return response;
            });
        } else {
            return this.inventoryLinkingService.getLinkedInventory(this.recordId, this.type, InventoryContexts.Related, params).then((response: IPaginatedResponse<ILinkedInventoryRow>): IPaginatedResponse<ILinkedInventoryRow> => {
                this.isReady = true;
                this.inventoryAction.setRelatedInventoryPage(page, response.data.length);
                return response;
            });
        }
    }

    handleRowClick(click: { event: MouseEvent, row: IRelatedTableRow | IFormattedPersonalDataTableRow}) {
        const target = click && click.event ? click.event.target : null;
        const isInPreventClickContainer: boolean = target ? this.isChildOfNonClickElement(target) : null;
        if (!isInPreventClickContainer) {
            if (this.resolvedPermissions.canViewDetails && ((click.row as IRelatedTableRow).id || (click.row as IRelatedTableRow).inventoryId)) {
                this.viewRecord.emit(click.row as IRelatedTableRow);
            }
        }
    }

    private setMenuClass(row: ILinkedInventoryRow | IFormattedPersonalDataTableRow) {
        this.menuClass = "actions-table__context-list";
        if (this.inventory.rows.length <= 10) return;
        const halfwayPoint: number = (this.inventory.rows.length - 1) / 2;
        const rowIndex: number = findIndex(this.inventory.rows as IRelatedTableRow[], (item: IRelatedTableRow | IFormattedPersonalDataTableRow) => {
            if ((row as ILinkedInventoryRow).inventoryId) {
                return (row as ILinkedInventoryRow).inventoryId === (item as IRelatedTableRow).inventoryId;
            } else {
                return ((row as IFormattedPersonalDataTableRow).dataElement.id === (item as IFormattedPersonalDataTableRow).dataElement.id) &&
                    ((row as IFormattedPersonalDataTableRow).dataSubjectType.id === (item as IFormattedPersonalDataTableRow).dataSubjectType.id);
            }
        });
        if (rowIndex > halfwayPoint) this.menuClass += " actions-table__context-list--above";
    }

    private isChildOfNonClickElement(target: any): boolean {
        if (!target || target.hasAttribute("otdatatable")) return false;
        if (includes(target.className, this.clickPreventClass)) return true;
        return this.isChildOfNonClickElement(target.parentNode);
    }
}
