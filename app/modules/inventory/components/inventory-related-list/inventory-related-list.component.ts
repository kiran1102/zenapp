// Rxjs
import { Subject } from "rxjs";
import {
    startWith,
    takeUntil,
} from "rxjs/operators";

// 3rd party
import {
    values,
    includes,
    without,
} from "lodash";

// Angular
import {
    Component,
    Inject,
    Input,
} from "@angular/core";

// Redux
import observableFromStore from "oneRedux/utils/observable-from-store";
import {
    getRelatedInventories,
    getIsLoadingRelated,
} from "oneRedux/reducers/inventory.reducer";

// service
import { InventoryActionService } from "inventoryModule/services/action/inventory-action.service";
import { InventoryViewLogic } from "oneServices/view-logic/inventory-view-logic.service";
import { RecordActionService } from "inventoryModule/services/action/record-action.service";

// constant + enums
import { InventoryContexts } from "enums/inventory.enum";
import { AllowV1Details } from "constants/inventory.constant";
import { NewInventorySchemaIds, LegacyInventorySchemaIds } from "constants/inventory-config.constant";

// Interfaces
import {
    IStoreState,
    IStore,
} from "interfaces/redux.interface";
import {
    IRelatedTable,
     IRelatedTableRow,
     IInventoryDetails,
} from "interfaces/inventory.interface";
import { IStringMap, INumberMap } from "interfaces/generic.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";

// Tokens
import { StoreToken } from "tokens/redux-store.token";

@Component({
    selector: "inventory-related-list",
    templateUrl: "./inventory-related-list.component.html",
})
export class InventoryRelatedListComponent {

    @Input() recordId: string;
    @Input() recordName: string;
    @Input() orgGroupId: string;
    @Input() permissions;

    expandedTableMap: IStringMap<boolean> = {};
    tableExpanded = false;
    isLoadingRelated = true;
    relatedInventoryArray: IRelatedTable[];
    relatedInventories: INumberMap<IRelatedTable>;
    drawerHeader: string;
    drawerReady = false;
    drawerIsOpen = false;
    inventoryType: string;
    inventoryId: number;
    currentRecordId: string;
    isDataElement: boolean;
    AllowV1Details = AllowV1Details;
    private inventoryLoad$ = new Subject();

    constructor(
        @Inject(StoreToken) private store: IStore,
        public inventoryAction: InventoryActionService,
        public inventoryViewLogic: InventoryViewLogic,
        public recordActionService: RecordActionService,
    ) {}

    ngOnInit(): void {
        observableFromStore(this.store).pipe(
            startWith(this.store.getState()),
            takeUntil(this.inventoryLoad$),
        ).subscribe((state: IStoreState) => this.componentWillReceiveState(state));
        if (this.permissions.canViewLinkedInventory) {
            this.inventoryAction.getLinkedInventory(this.recordId, InventoryContexts.Related);
        } else {
            this.inventoryAction.getRelatedInventory(this.recordId, InventoryContexts.Related);
        }
        this.setExpandedTableMap();
    }

    ngOnDestroy(): void {
        this.inventoryLoad$.next();
    }

    toggleDrawer(open: boolean, row?: IRelatedTableRow): void {
        if (open) {
            this.drawerIsOpen = true;
            this.drawerReady = false;
            this.currentRecordId = row.inventoryId;
            if (typeof(row.inventoryType) === "string") {
                this.inventoryType = row.inventoryType;
                this.inventoryId = LegacyInventorySchemaIds[this.inventoryType];
            } else {
                this.inventoryId = row.inventoryType;
                this.inventoryType = NewInventorySchemaIds[this.inventoryId];
            }
            this.drawerHeader = this.inventoryViewLogic.getLabel(this.inventoryId, "attributeDrawerHeader");
            if (AllowV1Details[this.inventoryId]) {
                this.isDataElement = true;
                this.getDataElementRecord(row).then((response: IProtocolResponse<IInventoryDetails>) => {
                    if (!response.result) return;
                    this.drawerReady = true;
                });
            } else {
                this.isDataElement = false;
                this.getRecord().then((response: boolean) => {
                    if (!response) return;
                    this.drawerReady = true;
                });
            }
        } else {
            this.recordActionService.setDefaultSecondaryRecordAndSchema();
            this.drawerIsOpen = false;
            this.drawerReady = false;
        }
    }

    setExpandedTableMap(): void {
        this.relatedInventoryArray.forEach((table: IRelatedTable): void => {
            this.expandedTableMap[table.id] = false;
        });
    }

    tableIsExpanded(): void {
        this.tableExpanded = includes(values(this.expandedTableMap), true);
    }

    toggleTable(inventoryId: string): void {
        this.expandedTableMap[inventoryId] = !this.expandedTableMap[inventoryId];
        this.tableIsExpanded();
    }

    private componentWillReceiveState(state: IStoreState): void {
        this.relatedInventories = getRelatedInventories(state);
        this.relatedInventoryArray = [];
        this.relatedInventoryArray = without(values(this.relatedInventories), null);
        this.isLoadingRelated = getIsLoadingRelated(state);
    }

    private getRecord(): Promise<boolean> {
        return this.recordActionService.setRecordAndSchemaById(this.inventoryType, this.currentRecordId, false, true);
    }

    private getDataElementRecord(row: IRelatedTableRow): ng.IPromise<IProtocolResponse<IInventoryDetails>> {
        return this.inventoryAction.fetchInventoryRecord(row.inventoryId, this.inventoryId);
    }

}
