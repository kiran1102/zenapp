// Rxjs
import { Subject } from "rxjs";
import {
    startWith,
    takeUntil,
} from "rxjs/operators";

// 3rd party
import {
    Component,
    Inject,
    Input,
    Output,
    EventEmitter,
} from "@angular/core";

// Redux
import observableFromStore from "oneRedux/utils/observable-from-store";
import { getAttachmentList } from "oneRedux/reducers/attachment.reducer";

// Services
import { AttachmentActionService } from "oneServices/actions/attachment-action.service";

// Tokens
import { StoreToken } from "tokens/redux-store.token";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

// Interfaces
import { IDropdownOption } from "interfaces/one-dropdown.interface";
import { IAttachmentFileResponse } from "interfaces/attachment-file.interface";
import { IEditAttachmentModalResolve } from "interfaces/edit-attachment-modal-resolve.interface";
import {
    IStoreState,
    IStore,
} from "interfaces/redux.interface";
import { IPageableOf } from "interfaces/pagination.interface";
import {
    IViewMoreParams,
    IFetchDetailsParams,
} from "interfaces/inventory.interface";

// Constants + Enums
import { CellTypes } from "enums/general.enum";
import {
    InventoryTableIds,
    DefaultTableConfig,
} from "enums/inventory.enum";

interface IInventoryAttachmentColumn {
    id: string;
    labelKey: string;
    rowTextKey?: string;
    rowTranslationKey?: string;
    type: number;
}

@Component({
    selector: "inventory-attachments",
    templateUrl: "./inventory-attachments.component.html",
})
export class InventoryAttachmentsComponent {

    @Input() permissions;
    @Input() recordId: string;
    @Input() inventoryId: number;
    @Input() isAddDocumentDisable: boolean;
    @Input("viewMore") showViewMore: boolean;

    @Output() onAddDocumentClicked = new EventEmitter<IViewMoreParams>();
    @Output() parametersOnFooterButtonClicked = new EventEmitter<IViewMoreParams>();

    rowActions: IDropdownOption[] = [];
    bordered = true;
    cellTypes = CellTypes;
    rows: IAttachmentFileResponse[];
    columns: IInventoryAttachmentColumn[];
    canShowHeaderFooter = false;
    isContextMenuOptionOpen = false;
    configDocument = DefaultTableConfig;
    attachmentList: IPageableOf<IAttachmentFileResponse>;
    defaultTableParams: IFetchDetailsParams;
    expandedTableParams: IFetchDetailsParams;
    menuPosition: string;
    private destroy$ = new Subject();

    constructor(
        @Inject(StoreToken) private store: IStore,
        private translatePipe: TranslatePipe,
        private attachmentAction: AttachmentActionService,
    ) {
        this.columns = [
            { id: "name", labelKey: "Name", rowTextKey: "Name", type: this.cellTypes.text },
            { id: "name", labelKey: "Description", rowTextKey: "Comments", type: this.cellTypes.text },
            { id: "name", labelKey: "DateAdded", rowTextKey: "CreatedDT", type: this.cellTypes.date },
            { id: "name", labelKey: "AddedBy", rowTextKey: "CreatedBy", type: this.cellTypes.text },
        ];
    }

    ngOnInit(): void {
        this.canShowHeaderFooter = (this.permissions.canViewDocuments && this.inventoryId === InventoryTableIds.Vendors) || this.permissions.canViewVendorContractInventoryLink;
        observableFromStore(this.store).pipe(
            startWith(this.store.getState()),
            takeUntil(this.destroy$),
        ).subscribe((state: IStoreState) => this.componentWillReceiveState(state));
    }

    ngOnDestroy(): void {
        this.destroy$.next();
        this.destroy$.complete();
    }

    columnTrackBy(column: IInventoryAttachmentColumn): string {
        return column.id;
    }

    getActions(menuIsOpen: boolean): IDropdownOption[] {
        this.setMenuClass(menuIsOpen);
        if (!menuIsOpen) return;
        this.rowActions = [];
        if (this.permissions.canDeleteAttachments) {
            this.rowActions.push({
                text: this.translatePipe.transform("DeleteAttachment"),
                action: (fileResponse: IAttachmentFileResponse): void => {
                    this.deleteAttachment(fileResponse.Id);
                },
            });
        }
        if (this.permissions.canDownloadAttachments) {
            this.rowActions.push({
                text: this.translatePipe.transform("DownloadAttachment"),
                action: (fileResponse: IAttachmentFileResponse): void => { this.downloadAttachment(fileResponse); },
            });
        }
        if (this.permissions.canCreateAttachments) {
            this.rowActions.push({
                text: this.translatePipe.transform("EditAttachment"),
                action: (fileResponse: IAttachmentFileResponse): void => {
                    this.editAttachment(fileResponse, (): void => {
                        this.attachmentAction.fetchAttachmentList(this.inventoryId, this.attachmentList.number, this.attachmentList.size);
                    });
                },
            });
        }
        if (!this.rowActions.length) {
            this.rowActions.push(this.getEmpyListAction());
        }
    }

    addDocument() {
        if (!this.attachmentList || this.attachmentList.totalElements === this.configDocument.defaultSize) {
            this.showViewMore = true;
            this.onAddDocumentClicked.emit({ tableParam: this.defaultTableParams, showContract: true });
        } else {
            this.onAddDocumentClicked.emit({ tableParam: this.showViewMore ? this.defaultTableParams : this.expandedTableParams });
        }
    }

    onViewMoreButtonClicked() {
        this.parametersOnFooterButtonClicked.emit({ tableParam: this.expandedTableParams, showContract: false });
    }

    onViewLessButtonCLicked() {
        this.parametersOnFooterButtonClicked.emit({ tableParam: this.defaultTableParams, showContract: true });
    }

    createOtAutoId(buttonObjText: string): string {
        return "InventoryAttachment".concat(buttonObjText.split(" ").join(""));
    }

    toggleContextMenu(value: boolean): void {
        this.isContextMenuOptionOpen = value;
    }

    getPageData(pageNumber: number, sizeParam = this.configDocument.expandedSize) {
        this.parametersOnFooterButtonClicked.emit({ tableParam: { page: pageNumber, size: sizeParam } });
    }

    setMenuClass(event?: any) {
        this.menuPosition = "bottom-right";
        if ((window.innerHeight - 200) / 2 < (event.clientY - 200)) {
            this.menuPosition = "top-right";
        }
    }

    private getEmpyListAction(): IDropdownOption {
        return {
            text: this.translatePipe.transform("NoActionsAvailable"),
            action: () => { }, // Keep Empty
        };
    }

    private componentWillReceiveState(state: IStoreState): void {
        this.attachmentList = getAttachmentList(state);
        this.rows = this.attachmentList.content;
        this.defaultTableParams = { page: 0, size: this.configDocument.defaultSize };
        this.expandedTableParams = { page: this.attachmentList.number, size: this.configDocument.expandedSize };
    }

    private deleteAttachment(attachmentId: string): void {
        let page = this.attachmentList.number;
        if (this.attachmentList.totalElements >= this.attachmentList.size && this.attachmentList.totalElements % this.attachmentList.size === 1) {
            page = page - 1;
        }
        this.attachmentAction.deleteAttachment(attachmentId, this.inventoryId, page, this.attachmentList.size);
    }

    private downloadAttachment(attachment: IAttachmentFileResponse): void {
        this.attachmentAction.downloadAttachment(attachment);
    }

    private editAttachment(attachment: IAttachmentFileResponse, callback?: () => void): void {
        const attachmentModalData: IEditAttachmentModalResolve = {
            requestId: attachment.Id,
            attachmentName: attachment.Name,
            attachmentComment: attachment.Comments,
            inventoryId: this.inventoryId,
            callback,
        };
        this.attachmentAction.editAttachment(attachmentModalData);
    }
}
