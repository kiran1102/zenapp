import {
    Component,
    Input,
    Output,
    OnInit,
    OnDestroy,
    Inject,
    EventEmitter,
} from "@angular/core";
import {
    findIndex,
    find,
} from "lodash";

import { InventoryAdapter } from "oneServices/adapters/inventory-adapter.service";
import { InventoryActionService } from "inventoryModule/services/action/inventory-action.service";
import {
    isSavingRecord,
    recordEdited,
    getCurrentRecord,
    getAttributeMap,
    getInventoryId,
} from "oneRedux/reducers/inventory.reducer";
import { StoreToken } from "tokens/redux-store.token";

import { IInventoryRecord, IAttribute, IInventoryCell } from "interfaces/inventory.interface";
import { IAttributeMap } from "interfaces/inventory-reducer.interface";
import { IStore, IStoreState } from "interfaces/redux.interface";
import { IStringMap } from "interfaces/generic.interface";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

// Constants & Enums
import {
    AttributeAllowedContexts,
    AttributeHiddenContexts,
    ManagingOrgAttributes,
} from "constants/inventory.constant";

import {
    InventoryTableIds,
    InventoryContexts,
} from "enums/inventory.enum";

@Component({
    selector: "inventory-attribute-details",
    templateUrl: "./inventory-attribute-details.component.html",
})
export class InventoryAttributeDetails implements OnInit, OnDestroy {

    @Input() isReady: boolean;
    @Input() isEditing: boolean;
    @Input() canEdit: boolean;
    @Input() columnClass: string;
    @Input() disabledTooltip: string;
    @Input() context: string;
    @Input() backgroundClass: string;
    @Input() hideErrors: boolean;
    @Input() isParentModal: boolean;
    @Input() isDetailsPage: boolean;
    @Input() isDisabled: boolean;
    @Input() seededRecord: IInventoryRecord;
    @Input() seededAttributeMap: IAttributeMap;
    @Input() seededInventoryId: number;
    @Output() toggleEdit = new EventEmitter();
    @Output() disabledClickAction: ({ dataFromCell: string }) => void;

    inventoryId: number;
    record: IInventoryRecord;
    hasEdits = false;
    savingRecord = false;
    attributeMap: IAttributeMap;
    AttributeAllowedContexts: IStringMap<IStringMap<boolean>> = AttributeAllowedContexts;
    AttributeHiddenContexts: IStringMap<IStringMap<boolean>> = AttributeHiddenContexts;

    private unsub: () => void;

    constructor(
        @Inject(StoreToken) public store: IStore,
        readonly translatePipe: TranslatePipe,
        readonly inventoryAction: InventoryActionService,
        readonly inventoryAdapter: InventoryAdapter,
    ) { }

    ngOnInit() {
        this.unsub = this.store.subscribe(() => this.componentWillReceiveState());
        this.componentWillReceiveState();
    }

    ngOnDestroy() {
        this.unsub();
    }

    cellCallback(attributeCode: string) {
        if (this.disabledClickAction) {
            this.disabledClickAction({ dataFromCell: attributeCode });
        }
    }

    editToggle() {
        this.toggleEdit.emit();
    }

    cellUpdate(update: {cell: IInventoryCell}) {
        let updatedOrgId = "";
        const updatedAttribute: IAttribute = this.attributeMap[update.cell.attributeId];
        if (this.attributeMap[update.cell.attributeId].required) {
            if (this.allRequiredCellsHaveValues(this.record, this.attributeMap)) {
                this.inventoryAction.setRequiredFieldsAsValid();
            } else {
                this.inventoryAction.setRequiredFieldsAsInvalid();
            }
        }
        const cellIndex: number = findIndex(this.record.cells, (cell: IInventoryCell): boolean => cell.attributeId === update.cell.attributeId);
        this.record.cells.splice(cellIndex, 1, update.cell);
        if (updatedAttribute.fieldAttributeCode === ManagingOrgAttributes[updatedAttribute.inventoryTypeId]) {
            updatedOrgId = this.inventoryAdapter.getValueIdFromCell(update.cell);
        }
        this.inventoryAction.recordEdited(this.record, this.attributeMap, updatedOrgId);
    }

    getAttributeName(cell: IInventoryCell): string {
        return this.attributeMap[cell.attributeId].nameKey ? this.translatePipe.transform(this.attributeMap[cell.attributeId].nameKey) : this.attributeMap[cell.attributeId].name;
    }

    getAttributeDescription(cell: IInventoryCell): string {
        return this.attributeMap[cell.attributeId].descriptionKey ? this.translatePipe.transform(this.attributeMap[cell.attributeId].descriptionKey) : this.attributeMap[cell.attributeId].description;
    }

    attributeIdTrackBy(index, item): string {
        return item.attributeId;
    }

    private allRequiredCellsHaveValues(record: IInventoryRecord, attributeMap: IAttributeMap): boolean {
        return !find(record.cells, (cell: IInventoryCell): boolean => {
            return attributeMap[cell.attributeId].required && (!cell.values || !cell.values.length || (cell.values[0] && !cell.values[0].value));
        });
    }

    private componentWillReceiveState() {
        const store: IStoreState = this.store.getState();
        this.savingRecord = isSavingRecord(store);
        this.hasEdits = recordEdited(store);
        if (this.seededRecord && this.seededAttributeMap && this.seededInventoryId) {
            this.record = this.seededRecord;
            this.attributeMap = this.seededAttributeMap;
            this.inventoryId = this.seededInventoryId;
        } else {
            this.record = getCurrentRecord(store);
            this.attributeMap = getAttributeMap(store);
            this.inventoryId = getInventoryId(store);
        }
    }
}
