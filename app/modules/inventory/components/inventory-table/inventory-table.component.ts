// Rxjs
import {
    Subscription,
    Subject,
} from "rxjs";
import {
    startWith,
    takeUntil,
} from "rxjs/operators";

// Angular
import {
    Component,
    EventEmitter,
    Input,
    Output,
    Inject,
} from "@angular/core";

// 3rd party
import {
    findIndex,
    includes,
} from "lodash";

// Redux
import {
    getSelectedRecords,
    getCurrentRecord,
    getAttributeDrawerState,
} from "oneRedux/reducers/inventory.reducer";
import observableFromStore from "oneRedux/utils/observable-from-store";

// Constants + Enums
import { DetailsLinkAttributes, StatusLinkAttributes, RiskAttributes } from "constants/inventory.constant";
import { RiskLabelBgClasses } from "enums/risk.enum";

// Services
import { InventoryActionService } from "inventoryModule/services/action/inventory-action.service";

// Interfaces
import {
    IInventoryRecord,
    IAttribute,
    IAttributeLink,
} from "interfaces/inventory.interface";
import { IStringMap, INumberMap } from "interfaces/generic.interface";
import { IDropdownOption } from "interfaces/one-dropdown.interface";
import { IStoreState } from "interfaces/redux.interface";

// Tokens
import { StoreToken } from "tokens/redux-store.token";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

@Component({
    selector: "inventory-table-new",
    templateUrl: "./inventory-table.component.html",
})
export class InventoryTableComponentNew {

    @Input() rows;
    @Input() columns;
    @Input() permissions;
    @Input() bulkEditMode: boolean;
    @Input() allRowsSelected: boolean;
    @Input() inventoryId: number;
    @Output() setRowActions: EventEmitter<IInventoryRecord> = new EventEmitter<IInventoryRecord>();
    @Output() deleteRecord: EventEmitter<IInventoryRecord> = new EventEmitter<IInventoryRecord>();
    @Output() launchRecord: EventEmitter<IInventoryRecord[]> = new EventEmitter<IInventoryRecord[]>();
    @Output() launchInventoryAssessment: EventEmitter<IInventoryRecord> = new EventEmitter<IInventoryRecord>();
    @Output() viewRecord: EventEmitter<IInventoryRecord> = new EventEmitter<IInventoryRecord>();
    @Output() handleRowSelection: EventEmitter<IInventoryRecord> = new EventEmitter<IInventoryRecord>();
    @Output() handleAllRowsSelection: EventEmitter<boolean> = new EventEmitter<boolean>();
    @Output() addRisk: EventEmitter<IInventoryRecord> = new EventEmitter<IInventoryRecord>();
    @Output() editRecord: EventEmitter<IInventoryRecord> = new EventEmitter<IInventoryRecord>();
    @Output() goToRoute: EventEmitter<{attributeLink: IAttributeLink, mouseEvent: MouseEvent}> = new EventEmitter<{attributeLink: IAttributeLink, mouseEvent: MouseEvent}>();
    @Output() addAssessmentLink: EventEmitter<IInventoryRecord> = new EventEmitter<IInventoryRecord>();
    @Output() changeRecordStatus: EventEmitter<IInventoryRecord> = new EventEmitter<IInventoryRecord>();
    @Output() viewRecordRisks: EventEmitter<IInventoryRecord> = new EventEmitter<IInventoryRecord>();
    @Output() copyRecord: EventEmitter<IInventoryRecord> = new EventEmitter<IInventoryRecord>();

    bordered = true;
    clickPreventClass = "prevent-row-click";
    menuClass: string;

    private DetailsLinkAttributes: IStringMap<string> = DetailsLinkAttributes;
    private StatusLinkAttributes: IStringMap<string> = StatusLinkAttributes;
    private RiskAttributes: IStringMap<string> = RiskAttributes;
    private RiskClasses: INumberMap<string> = RiskLabelBgClasses;
    private selectedRowId: string;
    private subscription: Subscription;
    private destroy$ = new Subject();

    constructor(
        @Inject(StoreToken) private store,
        private inventoryAction: InventoryActionService,
        private translatePipe: TranslatePipe,
    ) { }

    checkIfRowIsSelected = (row: IInventoryRecord): boolean => row.isSelected || (row.id === this.selectedRowId);
    checkIfRowIsDisabled = (row: IInventoryRecord): boolean => !row.canEdit || row.assessmentOutstanding;

    ngOnInit(): void {
        this.subscription = observableFromStore(this.store).pipe(
            startWith(this.store.getState()),
        ).subscribe((state: IStoreState): void => this.componentWillReceiveState(state));
    }

    ngOnDestroy(): void {
        this.subscription.unsubscribe();
    }

    columnTrackBy(column: IAttribute): string {
        return column.id;
    }

    getActions(row: IInventoryRecord): () => IDropdownOption[] {
        return (): IDropdownOption[] => {
            this.setMenuClass(row);
            const actions: IDropdownOption[] = [];
            if (row.canEdit && !row.assessmentOutstanding && this.permissions.canLaunch) {
                actions.push(this.getLaunchAction(row));
            }
            if (this.permissions.canLaunchVendorAssessment) {
                actions.push(this.getLaunchInventoryAssessmentAction(row));
            }
            if (this.permissions.canViewDetails && row.canEdit) {
                actions.push(this.getViewAction(row));
            }
            if (this.permissions.canAddRisks && row.canEdit) {
                actions.push(this.getAddRiskAction(row));
            }
            if (this.permissions.canCopy && row.canEdit) {
                actions.push(this.getCopyAction(row));
            }
            if (this.permissions.canEdit && !this.permissions.canViewDetails && row.canEdit) {
                actions.push(this.getEditAction(row));
            }
            if (this.permissions.canAddAssessmentLinks && row.canEdit) {
                actions.push(this.getAddLinkAction(row));
            }
            if (row.canEdit && !row.assessmentOutstanding && this.permissions.canDelete) {
                actions.push(this.getDeleteAction(row));
            }
            if (this.permissions.canChangeStatus) {
                actions.push(this.getChangeStatusAction(row));
            }
            if (row.canEdit && this.permissions.canExportPdf) {
                actions.push(this.getPdfExportAction(row));
            }
            if (!actions.length) {
                actions.push(this.getEmpyListAction());
            }
            return actions;
        };
    }

    handleRowClick(click: { event: any, row: IInventoryRecord}): void {
        const selectedCount: number = getSelectedRecords(this.store.getState()).length;
        const target = click && click.event ? click.event.target : null;
        const isInPreventClickContainer: boolean = target ? this.isChildOfNonClickElement(target) : null;
        if (!selectedCount && !isInPreventClickContainer) {
            this.selectedRowId = click.row.id;
            if (this.permissions.canViewDetails) {
                this.viewRecord.emit(click.row);
            } else if (this.permissions.canEdit && !this.permissions.canViewDetails && click.row.canEdit) {
                this.editRecord.emit(click.row);
            }
        }
    }

    toggleAllRowsSelected(): void {
        this.handleAllRowsSelection.emit(!this.allRowsSelected);
    }

    toggleRowSelection(row: IInventoryRecord): void {
        this.handleRowSelection.emit(row);
    }

    goToRisks(row: IInventoryRecord): void {
        if (this.permissions.canViewRisks && row.canEdit) {
            this.viewRecordRisks.emit(row);
        }
    }

    goToNewRoute(attributeLink: IAttributeLink, mouseEvent: MouseEvent) {
        this.goToRoute.emit({ attributeLink, mouseEvent });
    }

    private componentWillReceiveState(state: IStoreState): void {
        const currentRecord: IInventoryRecord = getCurrentRecord(state);
        const drawerOpen: boolean = getAttributeDrawerState(state);
        const currentRecordId: string = currentRecord ? currentRecord.id : null;
        if (drawerOpen) {
            this.selectedRowId = this.selectedRowId || currentRecordId;
        } else if (currentRecordId === this.selectedRowId) {
            this.selectedRowId = null;
        }
    }

    private getDeleteAction(row: IInventoryRecord): IDropdownOption {
        return {
            text: this.translatePipe.transform("DeleteRecord"),
            action: (): void => { this.deleteRecord.emit(row); },
        };
    }

    private getLaunchAction(row: IInventoryRecord): IDropdownOption {
        return {
            text: this.translatePipe.transform("LaunchAssessment"),
            action: (): void => { this.launchRecord.emit([row]); },
        };
    }

    private getLaunchInventoryAssessmentAction(row: IInventoryRecord): IDropdownOption {
        return {
            text: this.translatePipe.transform("LaunchAssessment"),
            action: (): void => { this.launchInventoryAssessment.emit(row); },
        };
    }

    private getAddRiskAction(row: IInventoryRecord): IDropdownOption {
        return {
            text: this.translatePipe.transform("AddRisk"),
            action: (): void => { this.addRisk.emit(row); },
        };
    }

    private getEditAction(row: IInventoryRecord): IDropdownOption {
        return {
            text: this.translatePipe.transform("EditRecord"),
            action: (): void => { this.editRecord.emit(row); },
        };
    }

    private getViewAction(row: IInventoryRecord): IDropdownOption {
        return {
            text: this.translatePipe.transform("ViewDetails"),
            action: (): void => { this.viewRecord.emit(row); },
        };
    }

    private getCopyAction(row: IInventoryRecord): IDropdownOption {
        return {
            text: this.translatePipe.transform("Copy"),
            action: (): void => { this.copyRecord.emit(row); },
        };
    }

    private getAddLinkAction(row: IInventoryRecord): IDropdownOption {
        return {
            text: this.translatePipe.transform("LinkAssessment"),
            action: (): void => { this.addAssessmentLink.emit(row); },
        };
    }

    private getChangeStatusAction(row: IInventoryRecord): IDropdownOption {
        return {
            text: this.translatePipe.transform("ChangeRecordStatus"),
            action: (): void => { this.changeRecordStatus.emit(row); },
        };
    }

    private getPdfExportAction(row: IInventoryRecord): IDropdownOption {
        return {
            text: this.translatePipe.transform("ExportAsPDF"),
            action: (): void => { this.inventoryAction.exportPDF(row, this.inventoryId); },
        };
    }

    private getEmpyListAction(): IDropdownOption {
        return {
            text: this.translatePipe.transform("NoActionsAvailable"),
            action: (): void => { }, // Keep Empty
        };
    }

    private setMenuClass(row: IInventoryRecord): void | undefined {
        this.menuClass = "actions-table__context-list";
        if (this.rows.length <= 10) return;
        const halfwayPoint: number = (this.rows.length - 1) / 2;
        const rowIndex: number = findIndex(this.rows, (item: IInventoryRecord): boolean => row.id === item.id);
        if (rowIndex > halfwayPoint) this.menuClass += " actions-table__context-list--above";
    }

    private isChildOfNonClickElement(target: any): boolean {
        if (!target || target.hasAttribute("otdatatable")) return false;
        if (includes(target.className, this.clickPreventClass)) return true;
        return this.isChildOfNonClickElement(target.parentNode);
    }
}
