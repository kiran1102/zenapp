import { Component, OnInit } from "@angular/core";

// Rxjs
import { Subject } from "rxjs";

// Services
import { InventoryService } from "inventorySharedModule/shared/services/inventory-api.service";
import { InventoryAdapterV2Service } from "inventoryModule/services/adapter/inventory-adapter-v2.service";

// Const
import { InventoryListTypes } from "constants/inventory.constant";

// Interfaces
import { IOtModalContent } from "@onetrust/vitreus";
import { IStringMap } from "interfaces/generic.interface";
import {
    IInventoryListItem,
    IInventoryElementListItem,
} from "interfaces/inventory.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

@Component({
    selector: "inventory-manage-data-elements-modal",
    templateUrl: "inventory-manage-data-elements-modal.component.html",
    host: { class: "flex-full-height" },
})
export class InventoryManageDataElementsModal implements OnInit, IOtModalContent {

    selectedCategory: IInventoryListItem;
    dataCategoryList: IInventoryListItem[] = [];
    filteredElementList: IInventoryListItem[] = [];
    dataSubjectTypeList: IInventoryListItem[] = [];
    selectedSubjectType: IInventoryListItem;
    elementsToAdd: Array<{ id: string }> = [];
    elementsToRemove: Array<{ id: string }> = [];
    prepopulatedElements: IInventoryElementListItem[];
    allSelected = false;
    searchTerm = "";
    elementMap: IStringMap<boolean> = {};
    fetchingListData = true;
    hasChanges = false;
    isSaving = false;
    otModalCloseEvent: Subject<boolean>;
    otModalData: { listItem: IInventoryListItem, callback: () => void };

    constructor(
        private inventoryService: InventoryService,
        private inventoryAdapterV2: InventoryAdapterV2Service,
        private translatePipe: TranslatePipe,
    ) { }

    ngOnInit() {
        if (this.otModalData && this.otModalData.listItem) {
            this.selectedSubjectType = this.otModalData.listItem;
            this.getListsData();
        } else {
            this.inventoryService.getListData(InventoryListTypes.DataSubjectTypes).then((response: IProtocolResponse<{data: IInventoryListItem[]}>) => {
                if (response.result) {
                    this.dataSubjectTypeList = this.inventoryAdapterV2.formatListItemOptions(response.data.data);
                    this.selectedSubjectType = response.data.data[0] || null;
                }
                this.getListsData();
            });
        }
    }

    selectCategory(category: IInventoryListItem) {
        this.selectedCategory = category;
        this.filteredElementList = this.selectedCategory.dataElements || [];
        this.allElementsSelected();
    }

    selectAll() {
        const checkStatus: boolean = !this.allSelected;
        this.filteredElementList.forEach((element) => {
            this.elementMap[element.id] = checkStatus;
        });
        this.allSelected = checkStatus;
        this.setAddRemoveElements();
    }

    selectSubjectType(selection: IInventoryListItem) {
        this.selectedSubjectType = selection;
    }

    toggleElement(element: IInventoryElementListItem) {
        this.elementMap[element.id] = !this.elementMap[element.id];
        this.setAddRemoveElements();
        this.allElementsSelected();
    }

    setAddRemoveElements() {
        this.elementsToAdd = this.addElementData();
        this.elementsToRemove = this.removeElementData();
        this.hasChanges = Boolean(this.elementsToAdd.length || this.elementsToRemove.length);
    }

    save() {
        this.isSaving = true;
        const promises: Array<ng.IPromise<IProtocolResponse<{ data: IInventoryElementListItem[] }>>> = [];
        if (this.elementsToAdd.length) promises.push(this.inventoryService.addAssociatedListItems(InventoryListTypes.DataSubjectTypes, this.selectedSubjectType.id, InventoryListTypes.DataElements, this.elementsToAdd));
        if (this.elementsToRemove.length) promises.push(this.inventoryService.removeAssociatedListItems(InventoryListTypes.DataSubjectTypes, this.selectedSubjectType.id, InventoryListTypes.DataElements, this.elementsToRemove));
        Promise.all(promises).then((responses: Array<IProtocolResponse<{data: IInventoryElementListItem[]}>>) => {
            if (responses.length > 1) {
                if (responses[0].result && responses[1].result) {
                    if (this.otModalData.callback) this.otModalData.callback();
                    this.closeModal();
                }
            } else {
                if (responses[0].result) {
                    if (this.otModalData.callback) this.otModalData.callback();
                    this.closeModal();
                }
            }
            this.isSaving = false;
        });
    }

    closeModal() {
        this.otModalCloseEvent.next();
    }

    handleSearch(searchTerm: string) {
        this.searchTerm = searchTerm;
        this.filteredElementList = this.selectedCategory.dataElements || [];
        if (searchTerm) {
            const term: string = searchTerm.toLocaleLowerCase();
            this.filteredElementList = this.filteredElementList.filter((element) => element.name.toLocaleLowerCase().indexOf(term) !== -1);
        }
        this.allElementsSelected();
    }

    private getListsData() {
        this.fetchingListData = true;
        Promise.all([
            this.inventoryService.getAssociatedListItems(InventoryListTypes.DataSubjectTypes, this.selectedSubjectType.id, InventoryListTypes.DataElements),
            this.inventoryService.getListDetails(InventoryListTypes.DataCategories),
        ]).then((responses: [IProtocolResponse<{data: IInventoryElementListItem[]}>, IProtocolResponse<{data: IInventoryListItem[]}>]) => {
            this.prepopulatedElements = responses[0].data.data;
            this.dataCategoryList = this.formatCategoryList(responses[1].data.data);
            this.setElementMap();
            this.setAddRemoveElements();
            this.fetchingListData = false;
            if (this.dataCategoryList.length) this.selectCategory(this.dataCategoryList[0]);
        });
    }

    private formatCategoryList(listData: IInventoryListItem[]) {
        listData.forEach((category) => {
            if (category.dataElements.length) {
                category.dataElements.forEach((element) => {
                    element.name = element.nameKey ? this.translatePipe.transform(element.nameKey) : element.name;
                });
            }
        });
        const allDataElements: IInventoryListItem[] = [].concat.apply([], listData.map((category) => category.dataElements));
        const allCategory: IInventoryListItem = {
            id: null,
            name: "AllDataElements",
            nameKey: "AllDataElements",
            description: null,
            descriptionKey: null,
            dataElements: allDataElements.filter((element, index, array) => array.findIndex((element2) => element2.id === element.id) === index),
        };
        return [allCategory, ...listData];
    }

    private setElementMap() {
        this.dataCategoryList.forEach((category) => {
            if (category.dataElements.length) {
                category.dataElements.forEach((element) => {
                    this.elementMap[element.id] = Boolean(this.prepopulatedElements.find((prepopElement) => prepopElement.id === element.id));
                });
            }
        });
    }

    private allElementsSelected() {
        let allCurrentElementsSelected = true;
        for (let i = 0; i < this.filteredElementList.length; i++) {
            if (!this.elementMap[this.filteredElementList[i].id]) {
                allCurrentElementsSelected = false;
                break;
            }
        }
        this.allSelected = allCurrentElementsSelected && Boolean(this.filteredElementList.length);
    }

    private addElementData(): Array<{ id: string }> {
        const elementsToAdd: Array<{ id: string }> = [];
        for (const elementId of Object.keys(this.elementMap)) {
            const prepopElement = this.prepopulatedElements.find((element) => element.id === elementId);
            if (this.elementMap[elementId] && !Boolean(prepopElement)) elementsToAdd.push({ id: elementId });
        }
        return elementsToAdd;
    }

    private removeElementData(): Array<{ id: string }> {
        const elementsToRemove: Array<{ id: string }> = [];
        this.prepopulatedElements.forEach((prepopElement) => {
            if (!this.elementMap[prepopElement.id]) elementsToRemove.push({ id: prepopElement.id });
        });
        return elementsToRemove;
    }
}
