import {
    Component,
    OnInit,
    Inject,
} from "@angular/core";

// Rxjs
import { Subject } from "rxjs";

// Services
import { ProjectService } from "oneServices/project.service";
import TemplateService from "oneServices/templates.service";
import { AssessmentLinkingAPIService } from "modules/assessment/services/api/assessment-linking-api.service";
import { Permissions } from "modules/shared/services/helper/permissions.service";

// Interfaces
import {
    ILinkAssessmentModalData,
    IListOption,
} from "interfaces/inventory.interface";
import { ICustomTemplateDetails } from "modules/template/interfaces/custom-templates.interface";
import { ILinkOption } from "interfaces/inventory.interface";
import {
    IProtocolPacket,
    IProtocolResponse,
} from "interfaces/protocol.interface";

// Constants / Enum
import { TemplateTypes } from "constants/template-types.constant";
import { TemplateCriteria } from "constants/template-states.constant";
import { TemplateStates } from "enums/template-states.enum";
import { InventoryTableIds } from "enums/inventory.enum";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

@Component({
    selector: "link-assessments-modal",
    templateUrl: "link-assessments-modal.component.html",
})
export class LinkAssessmentsModal implements OnInit {

    otModalCloseEvent: Subject<boolean>;
    otModalData: ILinkAssessmentModalData;
    templateSelection: ILinkOption;
    assessmentSelections: ILinkOption[] = [];
    assessmentSearchTerm = "";
    recordId: string;
    typeId: number;
    recordName: string;
    templateOptions: ILinkOption[];
    assessmentOptions: ILinkOption[] = [];
    isLinkingAssessments = false;
    callback: () => void;

    private canShowAssessmentV2 = false;

    constructor(
        private permissions: Permissions,
        @Inject("Templates") public readonly templates: TemplateService,
        private assessmentLinkingAPI: AssessmentLinkingAPIService,
        private projects: ProjectService,
        private translatePipe: TranslatePipe,
    ) { }

    ngOnInit() {
        this.recordId = this.otModalData.recordId || "";
        this.typeId = this.otModalData.typeId;
        this.recordName = this.otModalData.recordName;
        this.callback = this.otModalData.callback;
        this.canShowAssessmentV2 = this.permissions.canShow("DMAssessmentV2") || this.permissions.canShow("TemplatesV2");
        this.getTemplateOptions();
    }

    linkAssessments() {
        this.isLinkingAssessments = true;
        const assessmentIds: string[] = this.assessmentSelections.map((assessment) => assessment.id);
        if (this.canShowAssessmentV2 || this.typeId  === InventoryTableIds.Vendors) {
            this.assessmentLinkingAPI.linkAssessments(this.recordId, assessmentIds, this.typeId, this.recordName)
                .then((response: IProtocolResponse<null>) => {
                    this.isLinkingAssessments = false;
                    if (response.result) {
                        if (this.callback) this.callback();
                        this.closeModal();
                    }
            });
        } else {
            this.projects.linkProjects(this.recordId, assessmentIds).then((response: IProtocolPacket) => {
                this.isLinkingAssessments = false;
                if (response.result) {
                    if (this.callback) this.callback();
                    this.closeModal();
                }
            });
        }
    }

    assessmentSearch({ key, value }) {
        if (key === "Enter") return;
        this.assessmentSearchTerm = value;
    }

    selectTemplate(selection: ILinkOption): ng.IPromise<ILinkOption[]> {
        if (this.templateSelection !== selection) this.assessmentSelections = [];
        this.templateSelection = selection;
        if (selection && selection.id) {
            return this.getAssessments(selection.id).then((response: ILinkOption[]): ILinkOption[] => {
                this.assessmentOptions = response;
                return response;
            });
        }
    }

    selectAssessments({ currentValue }) {
        this.assessmentSelections = currentValue;
    }

    closeModal() {
        this.otModalCloseEvent.next();
    }

    private getTemplateOptions() {
        if (this.canShowAssessmentV2 || this.typeId  === InventoryTableIds.Vendors) {
            this.assessmentLinkingAPI.getAssessmentTemplates(`${TemplateTypes.PIA},${TemplateTypes.VENDOR}`,
                TemplateStates.Published, TemplateCriteria.Both)
                .then((response: IProtocolResponse<ICustomTemplateDetails[]>) => {
                    if (response.result) {
                        this.templateOptions = this.formatTemplatesV2OptionList(response.data);
                    }
                });
        } else {
            this.templates.getTemplateDropdownList().then((response: IProtocolPacket) => {
                if (!response.result) return;
                this.templateOptions = this.formatOptionList(response.data);
            });
        }
    }

    private formatOptionList(options: IListOption[], isAssessmentList: boolean = false): ILinkOption[] {
        return options.map((option: IListOption): ILinkOption => {
            if (isAssessmentList) {
                return { name: `${option.Name}: ${option.OrgGroup}`, id: option.Id };
            } else {
                return { name: option.Name, id: option.Id };
            }
        });
    }

    private formatTemplatesV2OptionList(options: ICustomTemplateDetails[]): ILinkOption[] {
        return options.map((option: ICustomTemplateDetails): ILinkOption => {
            const templateName: string = option.isActive ? option.name : `${option.name} (${this.translatePipe.transform("Archived")})`;
            return {
                name: templateName,
                id: option.rootVersionId,
            };
        });
    }

    private getAssessments(templateId: string): ng.IPromise<ILinkOption[]> {
        this.assessmentSelections = [];
        if (this.canShowAssessmentV2 || this.typeId  === InventoryTableIds.Vendors) {
            return this.assessmentLinkingAPI.getAssessmentDropdownList(templateId, this.recordId).then((response: IProtocolPacket): ILinkOption[] | undefined => {
                if (!response.result) return;
                return response.data;
            });
        } else {
            return this.templates.getProjectDropdownList(templateId, this.recordId).then((response: IProtocolPacket): ILinkOption[] | undefined => {
                if (!response.result) return;
                return this.formatOptionList(response.data, true);
            });
        }
    }
}
