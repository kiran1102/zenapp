// Rxjs
import { startWith } from "rxjs/operators";

// 3rd party
import {
    findIndex,
    remove,
} from "lodash";

// Angular
import {
    Component,
    Inject,
    Input,
    Output,
    EventEmitter,
    SimpleChanges,
} from "@angular/core";
import { StateService } from "@uirouter/core";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

// Redux
import {
    getCurrentRecord,
    getRelatedInventories,
    getRelatedInventoryPageNumber,
    getRelatedInventoryPageSize,
} from "oneRedux/reducers/inventory.reducer";
import observableFromStore from "oneRedux/utils/observable-from-store";

// Services
import { InventoryActionService } from "inventoryModule/services/action/inventory-action.service";
import { InventoryRelatedService } from "modules/inventory/services/adapter/inventory-related.service";
import { Permissions } from "modules/shared/services/helper/permissions.service";

// Interfaces
import {
    IRelatedRecord,
    IRelatedTable,
    IRelatedTableRow,
} from "interfaces/inventory.interface";
import {
    IStringMap,
    INumberMap,
} from "interfaces/generic.interface";
import { IDropdownOption } from "interfaces/one-dropdown.interface";
import { IPaginatedResponse } from "interfaces/pagination.interface";
import {
    IInventoryRecord,
    IInventoryCellValue,
} from "interfaces/inventory.interface";
import {
    IStoreState,
    IStore,
} from "interfaces/redux.interface";
import { IRelateInventoryModalResolve } from "interfaces/relate-inventory-modal.interface";

// constants + enums
import { CellTypes } from "enums/general.enum";
import {
    InventoryTableIds,
    InventoryContexts,
} from "enums/inventory.enum";
import {
    InventoryTranslationKeys,
    InventoryPermissions,
} from "constants/inventory.constant";

// Tokens
import { StoreToken } from "tokens/redux-store.token";

@Component({
    selector: "inventory-related-table",
    templateUrl: "./inventory-related-table.component.html",
})
export class InventoryRelatedTableComponent {

    @Input() inventory: IRelatedTable;
    @Input() type: number;
    @Input() recordId: string;
    @Input() orgGroupId: string;
    @Input() isExpanded: boolean;
    @Input() inventoryPermissions;
    @Output() toggle = new EventEmitter();

    cellTypes = CellTypes;
    tableIds = InventoryTableIds;
    isReady = false;
    relatedInventories: INumberMap<IRelatedTable>;
    config: IStringMap<number> = {
        defaultSize: 5,
        expandedSize: 20,
    };
    hasRelatedViewPermission: boolean;
    menuClass: string;
    relateInventoryText: string;
    currentPageNumber: number;
    currentPageSize: number;
    private record: IInventoryRecord;

    constructor(
        @Inject(StoreToken) private store: IStore,
        private stateService: StateService,
        private inventoryRelatedService: InventoryRelatedService,
        private inventoryAction: InventoryActionService,
        private permissions: Permissions,
        private translatePipe: TranslatePipe,
    ) { }

    checkIfRowIsDisabled = (row: IRelatedTableRow): boolean => row.inbound || !row.canEdit;

    public ngOnInit(): void {
        observableFromStore(this.store).pipe(
            startWith(this.store.getState()),
        ).subscribe((state: IStoreState) => this.componentWillReceiveState(state));
        this.relateInventoryText = this.setRelateInventoryText();
        this.isReady = true;
        if (!this.isExpanded && this.relatedInventories[this.type].rows && this.relatedInventories[this.type].rows.length > this.config.defaultSize) {
            this.pageChange(0);
        }
        this.hasRelatedViewPermission = this.permissions.canShow(InventoryPermissions[this.type].details);
    }

    public ngOnChanges(changes: SimpleChanges): void {
        if (changes.isExpanded && !changes.isExpanded.firstChange) {
            this.pageChange(0);
        }
    }

    public getActions(row: IRelatedTableRow): () => IDropdownOption[] {
        return (): IDropdownOption[] => {
            this.setMenuClass(row);
            const actions: IDropdownOption[] = [];
            if (this.inventoryPermissions.canEditDetails) {
                actions.push({
                    text: this.translatePipe.transform("Remove"),
                    action: (): void => { this.unlinkRecord(row); },
                });
            }
            if (!actions.length) {
                actions.push({
                    text: this.translatePipe.transform("NoActionsAvailable"),
                    action: (): void => { }, // Keep Empty
                });
            }
            return actions;
        };
    }

    public unlinkRecord(row: IRelatedTableRow): void {
        const updatedRecord = this.record;
        const attributeIndex = findIndex(updatedRecord.cells, ["attributeId", row.attributeId]);
        remove(updatedRecord.cells[attributeIndex].values, (record: IInventoryCellValue): boolean => {
            return record.valueId === row.id;
        });
        if (!updatedRecord.cells[attributeIndex].values.length) {
            updatedRecord.cells[attributeIndex].values = [{ value: null, valueId: null }];
        }
        const callback = (): void => {
            this.currentPageNumber = this.currentPageSize !== 1 || this.currentPageNumber === 0 ? this.currentPageNumber : this.currentPageNumber - 1;
            this.pageChange(this.currentPageNumber).then((response: IPaginatedResponse<IRelatedTableRow>): void => {
                if (response.data && response.data.length <= this.config.defaultSize && this.isExpanded && this.currentPageNumber === 0) {
                    this.toggleExpanded();
                }
            });
            if (this.type === InventoryTableIds.Assets) {
                this.inventoryAction.getRelatedInventory(this.record.id, InventoryContexts.Lineage);
            }
        };
        this.inventoryAction.unlinkRecord(this.record, callback);
    }

    public addInventoryRelationship(): void {
        const modalData: IRelateInventoryModalResolve = {
            inventoryType: this.type,
            orgGroupId: this.orgGroupId,
            callback: () => {
                this.pageChange(this.currentPageNumber);
                if (this.type === InventoryTableIds.Assets || this.type === InventoryTableIds.Elements) {
                    this.inventoryAction.getRelatedInventory(this.record.id, InventoryContexts.Lineage);
                }
            },
        };
        if (this.type === InventoryTableIds.Elements) {
            this.inventoryRelatedService.addDataSubjectRelationship(modalData);
        } else {
            this.inventoryRelatedService.addInventoryRelationship(modalData);
        }
    }

    public columnTrackBy(column: IRelatedRecord): string {
        return column.key;
    }

    public toggleExpanded(): void {
        this.isReady = false;
        this.toggle.emit();
    }

    public goToRecord(row: IRelatedTableRow): void {
        this.stateService.go("zen.app.pia.module.datamap.views.inventory.details", { inventoryId: this.type, recordId: row.id, tabId: "details" });
    }

    public pageChange(page: number, size?: number): ng.IPromise<IPaginatedResponse<IRelatedTableRow> | null> {
        this.isReady = false;
        const pageSize: number = size || this.isExpanded ? this.config.expandedSize : this.config.defaultSize;
        const params: IStringMap<number> = { size: pageSize, page };
        return this.inventoryRelatedService.getRelatedInventory(this.recordId, this.type, InventoryContexts.Related, params).then((response: IPaginatedResponse<IRelatedTableRow>): IPaginatedResponse<IRelatedTableRow> => {
            this.isReady = true;
            this.inventoryAction.setRelatedInventoryPage(page, response.data.length);
            return response;
        });
    }

    private setMenuClass(row: IRelatedTableRow): void | undefined {
        this.menuClass = "actions-table__context-list";
        if (this.inventory.rows.length <= 10) return;
        const halfwayPoint: number = (this.inventory.rows.length - 1) / 2;
        const rowIndex: number = findIndex(this.inventory.rows as IRelatedTableRow[], (item: IRelatedTableRow): boolean => row.id === item.id);
        if (rowIndex > halfwayPoint) this.menuClass += " actions-table__context-list--above";
    }

    private setRelateInventoryText(): string {
        switch (this.type) {
            case InventoryTableIds.Assets:
                return this.translatePipe.transform("AddAsset");
            case InventoryTableIds.Processes:
                return this.translatePipe.transform("AddProcessingActivities");
            case InventoryTableIds.Elements:
                return this.translatePipe.transform(InventoryTranslationKeys[this.type].addRelatedInventoryHeader);
        }
    }

    private componentWillReceiveState(state: IStoreState): void {
        this.record = getCurrentRecord(state);
        this.relatedInventories = getRelatedInventories(state);
        this.currentPageNumber = getRelatedInventoryPageNumber(state);
        this.currentPageSize = getRelatedInventoryPageSize(state);
    }
}
