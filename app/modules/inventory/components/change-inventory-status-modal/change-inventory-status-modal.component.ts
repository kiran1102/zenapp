// Rxjs
import { Subject } from "rxjs";

// 3rd Party
import {
    Component,
    OnInit,
} from "@angular/core";

// Services
import { InventoryService } from "inventorySharedModule/shared/services/inventory-api.service";
import { InventoryAdapterV2Service } from "inventoryModule/services/adapter/inventory-adapter-v2.service";

// Interface
import { ILabelValue } from "interfaces/generic.interface";
import { IOtModalContent } from "@onetrust/vitreus";
import { IChangeInventoryStatusModalInputData } from "interfaces/inventory.interface";

// Enums & Constants
import { InventoryStatusOptions } from "enums/inventory.enum";
import {
    InventoryTranslationKeys,
    InventoryStatusDescTranslationKeys,
    InventoryStatusTranslationKeys,
} from "constants/inventory.constant";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

@Component({
    selector: "change-inventory-status-modal",
    templateUrl: "./change-inventory-status-modal.component.html",
})
export class ChangeInventoryStatusModalComponent implements OnInit, IOtModalContent {

    statusOptions: Array<ILabelValue<InventoryStatusOptions>>;
    selectedStatusOption: ILabelValue<InventoryStatusOptions>;
    isReady = false;
    updateDisabled = true;
    isSaving = false;
    inventoryName: string;
    recordIds: string[];
    inventoryId: string;
    inventoryStatusDescTranslationKeys = InventoryStatusDescTranslationKeys;
    inventoryStatusTranslationKeys = InventoryStatusTranslationKeys;
    otModalCloseEvent: Subject<string>;
    otModalData: IChangeInventoryStatusModalInputData;

    constructor(
        private inventoryService: InventoryService,
        private inventoryAdapterV2Service: InventoryAdapterV2Service,
        private translatePipe: TranslatePipe,
    ) {}

    ngOnInit() {
        this.inventoryId = this.otModalData.inventoryId;
        this.recordIds = this.otModalData.recordIds;
        this.inventoryName = this.otModalData.inventoryName ? this.otModalData.inventoryName : `${this.recordIds.length} ${this.translatePipe.transform(InventoryTranslationKeys[this.inventoryId].inventoryName)}`;
        this.inventoryAdapterV2Service.formatInventoryStatusOptions().then((options: Array<ILabelValue<InventoryStatusOptions>>) => {
            this.statusOptions = options;
            if (this.otModalData.selectedInventoryStatus) {
                this.selectedStatusOption = this.statusOptions.find((option: ILabelValue<InventoryStatusOptions>): boolean => option.value === this.otModalData.selectedInventoryStatus);
            }
            this.isReady = true;
        });
    }

    closeModal(reload: boolean = false) {
        const selectedOptionValue = reload && this.selectedStatusOption ? this.selectedStatusOption.value : null;
        this.otModalCloseEvent.next(selectedOptionValue);
    }

    selectStatus(newStatusOption: ILabelValue<InventoryStatusOptions>) {
        this.selectedStatusOption = newStatusOption;
        this.updateDisabled = false;
    }

    changeInventoryStatus() {
        this.isSaving = true;
        if (this.recordIds && this.recordIds.length === 1) {
            this.inventoryService.changeInventoryStatus(this.recordIds[0], this.inventoryId, { key: this.selectedStatusOption.value }).then(() => {
                this.closeModal(true);
            });
        } else {
            this.inventoryService.bulkChangeInventoryStatus(this.recordIds, this.inventoryId, { key: this.selectedStatusOption.value }).then(() => {
                setTimeout(() => { this.closeModal(true); }, 2500);
            });
        }
    }
}
