// Rxjs
import {
    Subject,
    from,
} from "rxjs";
import {
    startWith,
    switchMap,
    takeUntil,
    tap,
} from "rxjs/operators";

// Angular
import {
    Component,
    Inject,
    Input,
    OnInit,
} from "@angular/core";

// Redux
import { getIsLoadingRisks } from "oneRedux/reducers/inventory.reducer";
import { getCurrentInventoryOrgGroupId } from "oneRedux/reducers/inventory-record.reducer";
import { getAllRisksInOrder } from "oneRedux/reducers/risk.reducer";
import observableFromStore from "oneRedux/utils/observable-from-store";

// Services
import { InventoryActionService } from "inventoryModule/services/action/inventory-action.service";
import { Permissions } from "modules/shared/services/helper/permissions.service";
import { InventoryRiskV2Service } from "modules/inventory/services/adapter/inventory-risk-v2.service";

// Constants / Enums
import { InventoryPermissions } from "constants/inventory.constant";
import {
    RiskV2States,
    RiskV2Levels,
    RiskV2SourceTypes,
} from "enums/riskV2.enum";
import { TableColumnTypes } from "enums/data-table.enum";
import { QuestionTypeNames } from "constants/question-types.constant";

// Interfaces
import {
    IRiskDetails,
    IAssessmentRiskUnlinkRequest,
} from "interfaces/risk.interface";
import {
    IStoreState,
    IStore,
} from "interfaces/redux.interface";
import {
    IInventoryRecordV2,
    IInventoryRiskTableRow,
    IInventoryRiskTable,
} from "interfaces/inventory.interface";
import {
    IStringMap,
    ILabelValue,
} from "interfaces/generic.interface";
import {
    IFilterData,
    IAddedFilter,
    IFilterTranslatedLabels,
    OtModalService,
    ConfirmModalType,
} from "@onetrust/vitreus";
import { IProtocolResponse } from "interfaces/protocol.interface";
import { IRiskLevel } from "interfaces/risks-settings.interface";
import { IPageableMeta } from "interfaces/pagination.interface";
import {
    IGridColumn,
    IGridSort,
    IGridPaginationParams,
} from "interfaces/grid.interface";
import { IColumnSelectorModalResolve } from "interfaces/column-selector-modal.interface";
import { IListState } from "@onetrust/vitreus";

// Services
import { InventoryRiskAPIService } from "inventoryModule/services/api/inventory-risk-api.service";
import { ModalService } from "sharedServices/modal.service";
import { RiskActionService } from "oneServices/actions/risk-action.service";
import { RiskAPIService } from "oneServices/api/risk-api.service";

// Tokens
import { StoreToken } from "tokens/redux-store.token";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

@Component({
    selector: "inventory-risks",
    templateUrl: "./inventory-risks.component.html",
})
export class InventoryRisksComponent implements OnInit {
    @Input() schemaId: number;
    @Input() recordId: string;
    @Input() recordName: string;
    @Input() record: IInventoryRecordV2;
    @Input("reload") reload$: Subject<void>;

    activeColumns: IGridColumn[];
    inactiveColumns: IGridColumn[];
    risks: IRiskDetails[];
    isLoadingRisks = false;
    recordPermissions: IStringMap<string>;
    isLoadingTable: boolean;
    isFilterOpen: boolean;
    searchText = "";
    sortData: IGridSort;
    defaultPagination: IGridPaginationParams = { page: 0, size: 20, sort: `number,desc` || [] };
    paginationParams = this.defaultPagination;
    paginationDetail: IPageableMeta;
    tableData: IInventoryRiskTableRow[];
    canShowRiskInheritance: boolean;
    riskLevels: IRiskLevel[];
    translatedLabels: IFilterTranslatedLabels;
    inputFilterData: IFilterData[];
    appliedFilters: IStringMap<string | string[]> = {};
    preSelectedFilter: IAddedFilter[] = [];
    currentPage: number;
    riskOwnersList: Array<ILabelValue<string>>;
    tableColumnTypes = TableColumnTypes;
    questionTypeNames = QuestionTypeNames;
    columnList: IGridColumn[] = [];
    filtersApplied: boolean;

    defaultColumns: IGridColumn[] = [
        { name: "id", sortKey: "number", type: this.tableColumnTypes.Link, nameKey: this.translatePipe.transform("ID"), valueKey: "number", style: { padding: "1rem" }, isMandatory: true },
        { name: "description", sortKey: "description", type: this.tableColumnTypes.Link, nameKey: this.translatePipe.transform("Description"), valueKey: "description", isMandatory: true},
        { name: "riskLevel", sortKey: "riskLevelId", type: this.tableColumnTypes.Risk, nameKey: this.translatePipe.transform("RiskLevelOrScore"), valueKey: "riskScore", style: { width: "8rem" }},
        { name: "riskOwner", sortKey: "riskOwnerId", type: this.tableColumnTypes.Text, nameKey: this.translatePipe.transform("RiskOwner"), valueKey: "riskOwner"},
        { name: "riskStatus", sortKey: "riskState", type: this.tableColumnTypes.Status, nameKey: this.translatePipe.transform("RiskStatus"), valueKey: "state"},
        { name: "sourceType", sortKey: "sourceType", type: this.tableColumnTypes.Text, nameKey: this.translatePipe.transform("Type"), valueKey: "sourceType" },
        { name: "source", sortKey: "sourceName", type: this.tableColumnTypes.Link, nameKey: this.translatePipe.transform("Source"), valueKey: "source" },
    ];
    defaultDisabledColumns: IGridColumn[] = [
        { name: "recommendation", sortKey: "recommendation", type: this.tableColumnTypes.Text, nameKey: this.translatePipe.transform("Recommendation"), valueKey: "recommendation", style: { width: "25rem" }},
        { name: "impactLevel", sortKey: "riskImpactLevel.name", type: this.tableColumnTypes.Text, nameKey: this.translatePipe.transform("Impact"), valueKey: "impactLevel"},
        { name: "probabilityLevel", sortKey: "riskProbabilityLevel.name", type: this.tableColumnTypes.Text, nameKey: this.translatePipe.transform("Probability"), valueKey: "probabilityLevel"},
        { name: "deadline", sortKey: "deadline", type: this.tableColumnTypes.Date, nameKey: this.translatePipe.transform("Deadline"), valueKey: "deadline", style: {"justify-content": "center", "display": "flex", "padding-top": "1rem"}},
        { name: "mitigatedDate", sortKey: "mitigatedDate", type: this.tableColumnTypes.Date, nameKey: this.translatePipe.transform("MitigatedDate"), valueKey: "mitigatedDate", style: {"justify-content": "center", "display": "flex", "padding-top": "1rem"}},
        { name: "exceptionRequest", sortKey: "exceptionRequest", type: this.tableColumnTypes.Text, nameKey: this.translatePipe.transform("Justification"), valueKey: "justification", style: { width: "25rem" }},
        { name: "orgGroup", sortKey: "orgGroupId", type: this.tableColumnTypes.Organization, nameKey: this.translatePipe.transform("Organization"), valueKey: "orgGroup", style: {"justify-content": "center", "display": "flex", "padding-top": "1rem"}},
        { name: "createdDate", sortKey: "createdDate", type: this.tableColumnTypes.DateTime, nameKey: this.translatePipe.transform("CreatedDate"), valueKey: "createdUTCDateTime", style: {"justify-content": "center", "display": "flex", "padding-top": "1rem"}},
    ];
    riskControlsColumn: IGridColumn =
        { name: "controls", type: this.tableColumnTypes.Link, nameKey: this.translatePipe.transform("Controls"), valueKey: "controlsIdentifier",  style: { width: "8rem" }};
    riskCategoryColumn: IGridColumn =
        { name: "category", type: this.tableColumnTypes.Text, nameKey: this.translatePipe.transform("Category"), valueKey: "categoriesCsv", style: { width: "7rem" }};

    private destroy$ = new Subject();

    constructor(
        @Inject(StoreToken) private store: IStore,
        private inventoryRiskV2Service: InventoryRiskV2Service,
        private inventoryAction: InventoryActionService,
        private inventoryRiskAPI: InventoryRiskAPIService,
        private riskAPI: RiskAPIService,
        private modalService: ModalService,
        private permissions: Permissions,
        private riskAction: RiskActionService,
        private translatePipe: TranslatePipe,
        private otModalService: OtModalService,
    ) { }

    ngOnInit() {
        this.canShowRiskInheritance = this.permissions.canShow("RiskInheritance");
        observableFromStore(this.store)
            .pipe(
                startWith(this.store.getState()),
                takeUntil(this.destroy$),
            )
            .subscribe((state: IStoreState) => {
                if (!this.canShowRiskInheritance) {
                    this.componentWillReceiveState(state);
                }
            });

        if (!this.canShowRiskInheritance) {
            this.inventoryAction.getRecordRisks(this.recordId);
            return;
        }

        // permission check for the 'RiskControl' feature flag
        if (this.permissions.canShow("RiskControl")) {
            this.defaultColumns.splice(4, 0, this.riskControlsColumn);
        }

        // permission check for the 'RiskCategory' feature flag
        if (this.permissions.canShow("RiskViewCategory")) {
            this.defaultDisabledColumns.splice(1, 0, this.riskCategoryColumn);
        }

        if (sessionStorage.getItem("ActiveInventoryRiskFilterList")) {
            const activeInventoryRiskFilterList = JSON.parse(sessionStorage.getItem("ActiveInventoryRiskFilterList"));
            if (activeInventoryRiskFilterList.preselectedInventoryRiskFilters && activeInventoryRiskFilterList.preselectedInventoryRiskFilters.length) {
                this.filtersApplied = true;
            }
        }
        this.isFilterOpen = false;
        this.getInventoryRisks();
        this.setActiveColumns();
    }

    ngOnDestroy() {
        this.destroy$.next();
        this.destroy$.complete();
    }

    risksTrackBy(risk: IRiskDetails): string {
        return risk.id;
    }

    getTableColumnList(): IGridColumn[] {
        const columnList: IGridColumn[] = [...this.activeColumns];
        columnList.push({ sortKey: "", type: this.tableColumnTypes.Action, name: "", valueKey: "" });
        return columnList;
    }

    openRiskModal(risk: IRiskDetails) {
        this.inventoryAction.openEditRiskModal(risk, (riskChange: IRiskDetails): void => {
            this.inventoryRiskV2Service.handleRiskChange(this.record, this.schemaId, riskChange, true);
        });
    }

    openDeleteModal(risk: IRiskDetails) {
        if (this.permissions.canShow("RiskInheritance")) {
            const onDeleteCallback =
                    (): ng.IPromise<boolean> => this.riskAPI.deleteRiskV2(risk.id).then((result: boolean): boolean => {
                        if (result) {
                            this.inventoryRiskV2Service.handleRiskChange(this.record, this.schemaId, risk, true);
                            this.getInventoryRisks();
                        }
                        return result;
                    });
            this.inventoryAction.deleteInventoryRisks(onDeleteCallback);
        } else {
            this.inventoryRiskV2Service.deleteInventoryRisk(this.record, risk, this.schemaId);
        }
    }

    unlinkRisk(risk: IRiskDetails) {
        const unlinkAssessmentRiskRequest: IAssessmentRiskUnlinkRequest = {
            riskIds: [risk.id],
            reference: {
                id: this.recordId,
                type: this.questionTypeNames.Inventory,
                name: this.recordName,
            },
        };

        this.otModalService.confirm({
            type: ConfirmModalType.WARNING,
            translations: {
                title: this.translatePipe.transform("UnlinkRisk"),
                desc: "",
                confirm: this.translatePipe.transform("AreYouSureYouWantToUnlinkThisRisk"),
                cancel: this.translatePipe.transform("Cancel"),
                submit: this.translatePipe.transform("UnLink"),
            },
        }).subscribe((data: boolean) => {
            if (!data) return;
            this.riskAPI.unlinkAssessmentRisk(unlinkAssessmentRiskRequest)
                .subscribe((response: IProtocolResponse<boolean>): boolean => {
                    if (response.result) {
                        this.getInventoryRisks();
                    }
                    return response.result;
                });
        });
    }

    changeRiskLevel(payload: {risk: IRiskDetails, levelId: number}) {
        const { risk, levelId } = payload;
        if (risk.action) delete risk.action;
        this.inventoryRiskV2Service.changeInventoryRiskLevel(this.record, risk, levelId, this.schemaId);
    }

    toggleFilterPane() {
        this.isFilterOpen = !this.isFilterOpen;
    }

    openColumnPickerModal() {
        const columnPickerModalData: IColumnSelectorModalResolve = {
            modalTitle: "ColumnSelector",
            leftList: [...this.inactiveColumns],
            rightList: [...this.activeColumns],
            labelKey: "nameKey",
        };
        columnPickerModalData.promiseToResolve = (columnsMap: IListState): Promise<boolean> => {
            this.inventoryRiskV2Service.setSelectedColumns(columnsMap.right as IGridColumn[]);
            this.setActiveColumns();
            return Promise.resolve(true);
        };
        this.modalService.setModalData(columnPickerModalData);
        this.modalService.openModal("columnSelectorModal");
    }

    handleSearch(text: string) {
        this.searchText = text || "";
        this.paginationParams.page = 0;
        this.getInventoryRisks();
    }

    sortInventoryRisks(sortData: IGridSort) {
        this.sortData = sortData;
        this.paginationParams.sort = `${sortData.sortedKey},${sortData.sortOrder}`;
        this.getInventoryRisks();
    }

    applyFilter(addedFilterValues: any) {
        this.preSelectedFilter = addedFilterValues;
        this.appliedFilters = {};
        addedFilterValues.forEach((addedFilterValue) => {
            switch (addedFilterValue.field) {
                case "riskLevel":
                    this.appliedFilters = {...this.appliedFilters, riskLevelId: [RiskV2Levels[addedFilterValue.value[0]]] };
                    break;
                case "riskStatus":
                    this.appliedFilters = {...this.appliedFilters, riskState: [RiskV2States[addedFilterValue.value[0]]] };
                    break;
                case "type":
                    this.appliedFilters = {...this.appliedFilters, sourceType: [RiskV2SourceTypes[addedFilterValue.value[0]]] };
                    break;
                case "organization":
                    this.appliedFilters = {...this.appliedFilters, orgGroupId: addedFilterValue.value[0] };
                    break;
                case "riskOwner":
                    this.appliedFilters = {...this.appliedFilters, riskOwnerId: [addedFilterValue.value[0]] };
                    break;
                case "category":
                    this.appliedFilters = {...this.appliedFilters, categoryId: addedFilterValue.value };
                    break;
            }
        });
        this.paginationParams.page = 0;
        this.getInventoryRisks();
        this.isFilterOpen = false;
        this.filtersApplied = Boolean(addedFilterValues && addedFilterValues.length);
    }

    pageChanged(page: number) {
        this.fetchPage(page - 1);
    }

    fetchPage = (page: number) => {
        if (this.paginationDetail.number !== page) {
            this.paginationParams.page = page;
            this.getInventoryRisks();
        }
    }

    getInventoryRisks() {
        this.isLoadingTable = true;
        this.tableData = [];
        this.paginationDetail = null;
        this.reload$
            .pipe(
                startWith(true),
                tap(() => this.paginationParams = this.defaultPagination),
                switchMap(() => from(this.inventoryRiskAPI.getInventoryRisks(this.recordId, this.searchText, this.appliedFilters, this.paginationParams))),
                takeUntil(this.destroy$),
            ).subscribe((response: IProtocolResponse<IInventoryRiskTable>) => {
                if (response.result) {
                    this.tableData = response.data.content;
                    this.currentPage = response.data.number + 1;
                    this.paginationDetail = {
                        first: response.data.first,
                        last: response.data.last,
                        number: response.data.number,
                        numberOfElements: response.data.numberOfElements,
                        size: response.data.size,
                        sort: response.data.sort,
                        totalElements: response.data.totalElements,
                        totalPages: response.data.totalPages,
                    };
                }
                this.isLoadingTable = false;
            });
    }

    addRisk() {
        this.inventoryAction.openAddRiskModal(
            this.recordId,
            this.recordName,
            this.schemaId,
            getCurrentInventoryOrgGroupId(this.store.getState()),
            (risk: IRiskDetails): void => {
                this.riskAction.addRisk(risk, "id");
                this.inventoryRiskV2Service.handleRiskChange(this.record, this.schemaId, risk, true);
                if (this.permissions.canShow("RiskInheritance") && !this.tableData.length) {
                    this.getInventoryRisks();
                }
            });
    }

    private componentWillReceiveState(state: IStoreState) {
        this.recordPermissions = InventoryPermissions[this.schemaId];
        this.risks = getAllRisksInOrder(state) as IRiskDetails[];
        this.isLoadingRisks = getIsLoadingRisks(state);
    }

    private setActiveColumns() {
        const columnsMap = this.inventoryRiskV2Service.getActiveInactiveColumnsMap([...this.defaultColumns], [...this.defaultDisabledColumns]);
        this.activeColumns = columnsMap.activeColumns;
        this.inactiveColumns = columnsMap.inactiveColumns;
        this.columnList = this.getTableColumnList();
    }
}
