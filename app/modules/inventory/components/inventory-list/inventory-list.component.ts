// Rxjs
import { Subscription } from "rxjs";
import { startWith } from "rxjs/operators";

// 3rd Party
import {
    Component,
    Inject,
    OnInit,
    OnDestroy,
} from "@angular/core";
import {
    TransitionService,
    StateService,
} from "@uirouter/core";
import {
    filter,
    cloneDeep,
    isEmpty,
    isNull,
    map,
    find,
} from "lodash";

// Redux
import {
    getRecordList,
    getAttributeList,
    getInventoryListPaginationMetadata,
    isFetchingInventoryList,
    recordEditEnabled,
    isSavingRecord,
    getCurrentRecord,
    recordEdited,
    requiredFieldsValid,
    getSelectedRecords,
    isExportingInventory,
    getSearchTerm,
    getInventoryId,
    getPageNumber,
    getAttributeDrawerState,
    getFilterDrawerState,
    getFilterData,
    isShowingFilteredResults,
    isFilteringInventory,
    getAllUsersValid,
    inventoryState,
} from "oneRedux/reducers/inventory.reducer";
import { StoreToken } from "tokens/redux-store.token";
import { getDrawerState } from "oneRedux/reducers/drawer.reducer";
import observableFromStore from "oneRedux/utils/observable-from-store";
import {
    getRecord,
    getIsLoadingRecord,
    getHasChanges,
    getIsEditingRecord,
    getHasRequiredValues,
    getHasErrors,
} from "oneRedux/reducers/inventory-record.reducer";

// Services
import { InventoryAdapter } from "oneServices/adapters/inventory-adapter.service";
import { InventoryViewLogic } from "oneServices/view-logic/inventory-view-logic.service";
import { InventoryActionService } from "inventoryModule/services/action/inventory-action.service";
import { InventoryRiskService } from "modules/inventory/services/adapter/inventory-risk.service";
import { ModalService } from "sharedServices/modal.service";
import { AssessmentBulkLaunchActionService } from "sharedModules/services/actions/bulk-launch-assessment-action.service";
import {
    Transition,
    HookResult,
} from "@uirouter/angularjs";
import { Permissions } from "modules/shared/services/helper/permissions.service";
import { RecordActionService } from "inventoryModule/services/action/record-action.service";
import { InventoryListActionService } from "inventoryModule/services/action/inventory-list-action.service";

// Constants + Enums
import {
    AttributeAllowedContexts,
    InventoryPermissions,
    InventoryAttributeCodes,
    ManagingOrgAttributes,
    TypesAttributes,
    LocationAttributes,
    NamesAttributes,
    AllowV1Details,
    InventoryDetailRoutes,
    InventoryDetailsTabs,
} from "constants/inventory.constant";
import {
    NewInventorySchemaIds,
    LegacyInventorySchemaIds,
} from "constants/inventory-config.constant";
import {
    InventoryContexts,
    InventoryTableIds,
} from "enums/inventory.enum";
import { ContextMenuType } from "@onetrust/vitreus";

// Interfaces
import { IDropdownOption } from "interfaces/one-dropdown.interface";
import { IInventoryParams, IInventoryCell, IAttributeLink } from "interfaces/inventory.interface";
import { ISaveChangesModalResolve } from "interfaces/save-changes-modal-resolve.interface";
import {
    IInventoryRecord,
    IAttribute,
    IInventoryTable,
    ILaunchResponse,
    IInventoryRecordV2,
    IInventoryDetails,
} from "interfaces/inventory.interface";
import { IFilterTree } from "interfaces/filter.interface";
import { IPageable } from "interfaces/pagination.interface";
import { IStringMap } from "interfaces/generic.interface";
import { IActionIcon } from "interfaces/action-icons.interface";
import {
    IProtocolPacket,
    IProtocolResponse,
} from "interfaces/protocol.interface";
import {
    IStore,
    IStoreState,
} from "interfaces/redux.interface";
import { IRiskDetails } from "interfaces/risk.interface";
import { IAssessmentCreationDetails } from "interfaces/assessment/assessment-creation-details.interface";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

@Component({
    selector: "inventory-list",
    templateUrl: "./inventory-list.component.html",
})
export class InventoryListComponent implements OnInit, OnDestroy {

    usersValid: boolean;
    context: number;
    subscription: Subscription;
    isLoading = true;
    drawerHeader: string;
    drawerReady = false;
    savingRecord;
    closeAction: IActionIcon[] = [{
        handleClick: () => this.toggleAllRecordsSelected(false),
        iconClass: "ot-close",
        buttonClass: "border-round text-center",
        key: "rounded",
    }];
    bulkEditMode: boolean;
    allRowsSelected: boolean;
    isFiltering: boolean;
    isExporting: boolean;
    viewReady = false;
    exportOptions: IDropdownOption[];
    currentPageNumber: number;
    inventoryId: number;
    name: string;
    table: { rows: IInventoryRecord[], columns: IAttribute[], metaData: IPageable };
    hasEdits = false;
    requiredFieldsValid = false;
    inventoryPermissions: IStringMap<boolean>;
    recordEditEnabled = false;
    currentRecord: IInventoryRecordV2 | IInventoryRecord;
    isAttributeDrawerOpen: boolean;
    isFilterDrawerOpen: boolean;
    filterData: IFilterTree;
    currentSearchTerm: string;
    selectedCount: number;
    currentRecordId: string;
    schemaIds = NewInventorySchemaIds;
    organization: IInventoryCell;
    inventoryOrgId: string;
    allowV1Details = AllowV1Details;
    contextMenuType = ContextMenuType;
    private transitionDeregisterHook: any;
    private canBulkLaunch: boolean;
    private canShowUpgradedBulkLaunchOnDM = false;
    private viewPermissions: IStringMap<string>;
    private drawerIsOpen: boolean;
    private showingFilteredResults: boolean;
    private defaultParams: IInventoryParams = {
        page: 0,
        size: 20,
        searchTerm: "",
    };
    private params: IInventoryParams = {
        page: parseInt(this.stateService.params.page, 10) || this.defaultParams.page,
        size: parseInt(this.stateService.params.size, 10) || this.defaultParams.size,
        searchTerm: this.currentSearchTerm,
    };

    constructor(
        @Inject("$scope") public $scope: ng.IScope,
        @Inject(StoreToken) public store: IStore,
        public permissions: Permissions,
        public stateService: StateService,
        public $transitions: TransitionService,
        public inventoryAction: InventoryActionService,
        public inventoryViewLogic: InventoryViewLogic,
        public modalService: ModalService,
        public inventoryRisk: InventoryRiskService,
        public assessmentBulkLaunchActionService: AssessmentBulkLaunchActionService,
        public inventoryAdapter: InventoryAdapter,
        public translatePipe: TranslatePipe,
        public recordActionService: RecordActionService,
        public inventoryListAction: InventoryListActionService,
    ) {}

    ngOnInit() {
        this.context = InventoryContexts.EditSidebar;
        this.inventoryAction.closeAllDrawers();
        this.drawerReady = false;
        this.inventoryAction.recordEditDisabled();
        this.clearFilterData();
        this.inventoryId = LegacyInventorySchemaIds[this.stateService.params.Id] || this.stateService.params.Id;
        this.inventoryAction.setInventoryId(this.inventoryId);
        this.inventoryListAction.setUserOptions();
        this.viewPermissions = InventoryPermissions[this.inventoryId];
        this.inventoryPermissions = {
            canLaunch: this.permissions.canShow(this.viewPermissions.launch),
            canEdit: this.permissions.canShow(this.viewPermissions.edit),
            canDelete: this.permissions.canShow(this.viewPermissions.delete),
            canExport: this.permissions.canShow(this.viewPermissions.export),
            canAdd: this.permissions.canShow(this.viewPermissions.add),
            canCopy: this.permissions.canShow(this.viewPermissions.copy),
            canExportArt30: this.permissions.canShow(this.viewPermissions.exportArt30),
            canViewDetails: this.permissions.canShow(this.viewPermissions.details),
            canAddRisks: this.permissions.canShow(this.viewPermissions.addRisk),
            canViewRisks: this.permissions.canShow(this.viewPermissions.viewRisks),
            canAddAssessmentLinks: this.permissions.canShow(this.viewPermissions.addAssessmentLink),
            canViewAssessmentLinks: this.permissions.canShow(this.viewPermissions.viewAssessmentLinks),
            canLaunchVendorAssessment: this.permissions.canShow(this.viewPermissions.launchVendorAssessment),
            canViewInventoryAssessments: this.permissions.canShow(this.viewPermissions.launchVendorAssessment),
            canChangeStatus: this.permissions.canShow(this.viewPermissions.changeStatus),
            canBulkEdit: this.permissions.canShow(this.viewPermissions.bulkEdit),
            canExportPdf: this.permissions.canShow(this.viewPermissions.exportPdf),
            canExportV2: this.permissions.canShow(this.viewPermissions.exportV2),
        };
        this.drawerHeader = this.inventoryViewLogic.getLabel(this.inventoryId, "attributeDrawerHeader");
        this.currentSearchTerm = sessionStorage.getItem(`DMInventorySearchTerm${this.inventoryId}`) || "";
        this.inventoryAction.setSearchTerm(this.currentSearchTerm);
        this.filterData = JSON.parse(sessionStorage.getItem(`DMInventoryFilterSettings${this.inventoryId}`)) as IFilterTree;
        this.inventoryAction.setFilterData(this.filterData);
        if (!this.currentSearchTerm && this.filterData) {
            this.showingFilteredResults = true;
            this.inventoryAction.setShowingFilteredResults(true);
        } else {
            this.showingFilteredResults = false;
            this.inventoryAction.setShowingFilteredResults(false);
        }
        this.inventoryAction.setPageNumber(JSON.parse(sessionStorage.getItem(`DMInventoryPageNumber${this.inventoryId}`)) || this.defaultParams.page);
        this.getInventoryList();

        this.subscription = observableFromStore(this.store).pipe(
            startWith(this.store.getState()),
        ).subscribe((state: IStoreState) => this.componentWillReceiveState(state));

        this.exportOptions = this.getExportOptions()();

        this.canBulkLaunch = this.inventoryPermissions.canLaunch || this.inventoryPermissions.canLaunchVendorAssessment;
        this.canShowUpgradedBulkLaunchOnDM = this.inventoryPermissions.canLaunch && this.permissions.canShow("DMAssessmentV2");
        this.setTransitionAwayHook();
    }

    ngOnDestroy() {
        if (this.transitionDeregisterHook) {
            this.transitionDeregisterHook();
        }
        this.subscription.unsubscribe();
    }

    getPage(page: number): ng.IPromise<IProtocolPacket> {
        this.inventoryAction.setPageNumber(page);
        if (!this.showingFilteredResults || isEmpty(this.filterData) || isNull(this.filterData)) {
            return this.inventoryAction.fetchInventoryPage();
        } else {
            this.inventoryAction.isFilteringInventory();
            this.inventoryAction.fetchFilteredInventoryPage(this.filterData).then((response: IProtocolResponse<IInventoryTable>) => {
                if (response.result) {
                    this.inventoryAction.setShowingFilteredResults(true);
                }
                this.inventoryAction.isDoneFilteringInventory();
            });
        }
    }

    handleDelete(record: IInventoryRecord) {
        const newPage: number = this.table.metaData.numberOfElements > 1 || this.table.metaData.number === 0 ? this.table.metaData.number : this.table.metaData.number - 1;
        const callback = (response: IProtocolResponse<IStringMap<string>>) => {
            if (response.result && this.showingFilteredResults) {
                this.inventoryAction.fetchFilteredInventoryPage(this.filterData);
            } else if (response.result && !this.showingFilteredResults) {
                this.inventoryAction.fetchInventoryPage(newPage, this.table.metaData.size);
            }
        };
        this.inventoryAction.deleteRecord(record.id, callback);
    }

    handleCopy(record: IInventoryRecord) {
        this.inventoryAction.launchCopyModal(record.id);
    }

    toggleRecordSelection(record: IInventoryRecord) {
        this.inventoryAction.toggleRecordSelection(record, !record.isSelected);
    }

    handleLaunchInventoryAssessment(record: IInventoryRecord) {
        const inventoryRecords: IInventoryRecord[] = [record];
        const bulkInventoryList = this.inventoryAction.getBulkInventoryList(inventoryRecords, this.inventoryId);
        const bulkAssessmentDetails: IAssessmentCreationDetails[] = this.assessmentBulkLaunchActionService.getAssessmentCreationItems(bulkInventoryList, this.inventoryId);
        this.assessmentBulkLaunchActionService.setBulkLaunchAssessments(bulkAssessmentDetails);
        this.stateService.go("zen.app.pia.module.assessment.bulk-launch-inventory-assessment", { inventoryTypeId: this.inventoryId });
    }

    toggleAttributeDrawer = (row?: IInventoryRecord, enableEditing?: boolean) => {
        if (this.drawerIsOpen && this.recordEditEnabled && this.hasEdits && this.requiredFieldsValid) {
            const callback = () => {
                this.closeDrawer();
            };
            this.launchSaveChangesModal(callback);
            return;
        }
        this.closeDrawer();
        this.inventoryAction.openAttributeDrawer();
        if (this.drawerIsOpen && row) {
            this.drawerReady = false;
            this.currentRecordId = row.id;
            this.getRecord(row).then((response: boolean | IProtocolResponse<IInventoryDetails>) => {
                if (!response || AllowV1Details[this.inventoryId] && !(response as IProtocolResponse<IInventoryDetails>).result) return;
                this.drawerReady = true;
                if (enableEditing) {
                    const savedRecord: IInventoryRecord | {} = cloneDeep(row);
                    this.inventoryAction.recordEditEnabled(savedRecord);
                }
            });
        } else {
            if (this.drawerIsOpen) {
                this.closeDrawer();
            } else {
                this.inventoryAction.openAttributeDrawer();
            }
            if (this.recordEditEnabled) {
                this.saveCancelled();
            }
        }
    }

    goToRoute(event: { attributeLink: IAttributeLink, mouseEvent: MouseEvent }) {
        if (event.mouseEvent.ctrlKey || event.mouseEvent.metaKey) {
            const url: string = this.stateService.href(event.attributeLink.route, event.attributeLink.params);
            window.open(url, "_blank");
        } else {
            this.stateService.go(event.attributeLink.route, event.attributeLink.params);
        }
    }

    openRiskModal(record: IInventoryRecord) {
        this.setInventoryOrgId(record);
        let inventoryName = "";
        record.cells.some((cell: IInventoryCell): boolean => {
            switch (this.inventoryId) {
                case InventoryTableIds.Assets:
                    if (cell.attributeCode === InventoryAttributeCodes[this.inventoryId].Asset) {
                        inventoryName = cell.displayValue;
                    }
                    break;
                case InventoryTableIds.Processes:
                    if (cell.attributeCode === InventoryAttributeCodes[this.inventoryId].Processing_Activity) {
                        inventoryName = cell.displayValue;
                    }
                    break;
                case InventoryTableIds.Vendors:
                    if (cell.attributeCode === InventoryAttributeCodes[this.inventoryId].Name) {
                        inventoryName = cell.displayValue;
                    }
                    break;
            }
            if (inventoryName) return true;
        });
        this.inventoryAction.openAddRiskModal(record.id, inventoryName, InventoryTableIds[InventoryTableIds[this.inventoryId]], this.inventoryOrgId, (riskModel: IRiskDetails) => {
            this.inventoryAction.handleRiskChange(record, riskModel);
        });
    }

    setInventoryOrgId(record: IInventoryRecord): void {
        const inventoryType = inventoryState(this.store.getState()).inventoryId;
        this.organization = find(record.cells, (attribute: IInventoryCell): boolean => {
            return attribute.attributeCode === ManagingOrgAttributes[inventoryType];
        });
        if (this.organization) {
            this.inventoryOrgId = this.organization.values[0].valueId;
        }
    }

    applyFilters = (filters: IFilterTree) => {
        this.clearSearchData();
        this.inventoryAction.setFilterData(filters);
        if (isEmpty(filters) || isNull(filters)) {
            this.inventoryAction.fetchInventoryPage().then((response: IProtocolResponse<IInventoryTable>) => {
                if (response.result) {
                    this.inventoryAction.setShowingFilteredResults(false);
                }
            });
            sessionStorage.removeItem(`DMInventoryFilterSettings${this.inventoryId}`);
        } else {
            this.inventoryAction.isFilteringInventory();
            this.inventoryAction.fetchFilteredInventoryPage(filters).then((response: IProtocolResponse<IInventoryTable>) => {
                if (response.result) {
                    this.inventoryAction.setShowingFilteredResults(true);
                }
                this.inventoryAction.isDoneFilteringInventory();
                sessionStorage.setItem(`DMInventoryFilterSettings${this.inventoryId}`, JSON.stringify(this.inventoryRisk.formatRiskFilterForSave(filters)));
            });
        }
        this.closeDrawer();
    }

    handleSearch(searchTerm: string) {
        this.inventoryAction.setSearchTerm(searchTerm);
        sessionStorage.setItem(`DMInventorySearchTerm${this.inventoryId}`, searchTerm);
        this.inventoryAction.fetchInventoryPage(0).then((response: IProtocolResponse<IInventoryTable>) => {
            if (response.result) {
                this.inventoryAction.setShowingFilteredResults(false);
            }
        });
    }

    addNewRecord() {
        if (this.hasEdits && this.requiredFieldsValid) {
            const callback = () => {
                this.closeDrawer();
                this.drawerReady = false;
                setTimeout(() => {
                    this.inventoryAction.launchAddModal();
                }, 0);
            };
            this.launchSaveChangesModal(callback);
        } else {
            this.closeDrawer();
            this.inventoryAction.launchAddModal();
        }
    }

    toggleAllRecordsSelected(isSelected: boolean) {
        this.inventoryAction.toggleAllRecordsSelection(isSelected);
    }

    handleBulkLaunch(records: IInventoryRecord[]) {
        if (this.inventoryPermissions.canLaunch && !this.canShowUpgradedBulkLaunchOnDM) {
            const actionCallback = (response: IProtocolResponse<ILaunchResponse[]>) => {
                this.inventoryAction.toggleAllRecordsSelection(false);
                this.inventoryAction.isFetchingInventory();
                if (response && this.showingFilteredResults) {
                    this.inventoryAction.fetchFilteredInventoryPage(this.filterData).then((res: IProtocolResponse<IInventoryTable>) => {
                        if (res.result) {
                            this.inventoryAction.setShowingFilteredResults(true);
                            this.inventoryAction.isDoneFetchingInventory();
                        }
                    });
                } else if (response && !this.showingFilteredResults) {
                    this.inventoryAction.updateAssessmentStatuses().then(() => {
                        this.inventoryAction.setShowingFilteredResults(false);
                        this.inventoryAction.isDoneFetchingInventory();
                    });
                }
            };
            if (this.hasEdits && this.requiredFieldsValid) {
                const callback = () => {
                    this.closeDrawer();
                    setTimeout(() => {
                        if (this.showingFilteredResults) {
                            this.inventoryAction.fetchFilteredInventoryPage(this.filterData).then((response: IProtocolResponse<IInventoryTable>) => {
                                if (response.result) {
                                    this.inventoryAction.setShowingFilteredResults(true);
                                    this.inventoryAction.isDoneFetchingInventory();
                                    this.inventoryAction.launchAssessments(this.name, this.inventoryAdapter.getRecordAssignments(records, this.inventoryId), actionCallback, this.table.metaData.number, this.table.metaData.size);
                                }
                            });
                        } else {
                            this.inventoryAction.fetchInventoryPage().then((response: IProtocolResponse<IInventoryTable>) => {
                                if (response.result) {
                                    this.inventoryAction.setShowingFilteredResults(true);
                                    this.inventoryAction.isDoneFetchingInventory();
                                    this.inventoryAction.launchAssessments(this.name, this.inventoryAdapter.getRecordAssignments(records, this.inventoryId), actionCallback, this.table.metaData.number, this.table.metaData.size);
                                }
                            });
                        }
                    }, 0);
                };
                this.launchSaveChangesModal(callback);
            } else {
                this.closeDrawer();
                this.inventoryAction.launchAssessments(this.name, this.inventoryAdapter.getRecordAssignments(records, this.inventoryId), actionCallback, this.table.metaData.number, this.table.metaData.size);
            }
        } else if (this.inventoryPermissions.canLaunchVendorAssessment || this.canShowUpgradedBulkLaunchOnDM) {
            const bulkInventoryList = this.inventoryAction.getBulkInventoryList(records, this.inventoryId);
            const bulkAssessmentDetails: IAssessmentCreationDetails[] = this.assessmentBulkLaunchActionService.getAssessmentCreationItems(bulkInventoryList, this.inventoryId);
            this.assessmentBulkLaunchActionService.setBulkLaunchAssessments(bulkAssessmentDetails);
            this.stateService.go("zen.app.pia.module.assessment.bulk-launch-inventory-assessment", { inventoryTypeId: this.inventoryId });
        }
    }

    saveRecord(): ng.IPromise<void> {
        const saveRecordPromise = (): ng.IPromise<boolean | IProtocolPacket> => {
            if (AllowV1Details[this.inventoryId]) return this.inventoryAction.saveRecord(this.currentRecord as IInventoryRecord);
            return this.recordActionService.saveRecordChanges(NewInventorySchemaIds[this.inventoryId], (this.currentRecord as IInventoryRecordV2).id as string);
        };
        return saveRecordPromise().then((response: boolean | IProtocolPacket) => {
            if (!response || AllowV1Details[this.inventoryId] && !(response as IProtocolPacket).result) {
                this.recordActionService.setFinishLoadingState(NewInventorySchemaIds[this.inventoryId], true);
                return;
            }
            if (AllowV1Details[this.inventoryId]) this.toggleEdit();
            this.getInventoryList();
        });
    }

    toggleEdit() {
        if (this.recordEditEnabled) {
            this.inventoryAction.recordEditDisabled();
        } else {
            const savedRecord: IInventoryRecord | {} = cloneDeep(this.currentRecord);
            this.inventoryAction.recordEditEnabled(savedRecord);
        }
    }

    saveCancelled() {
        if (AllowV1Details[this.inventoryId]) {
            this.toggleEdit();
            this.inventoryAction.recordEditCancelled();
        } else {
            this.recordActionService.toggleEditingRecord(false);
        }
    }

    getExportOptions(): () => IDropdownOption[] {
        const exportOptions: IDropdownOption [] = [];
        if (this.inventoryPermissions.canExport) {
            exportOptions.push({
                text: this.translatePipe.transform("Export"),
                id: "ExportInventoryList",
                action: () => {
                    if (this.inventoryPermissions.canExportV2) {
                        this.inventoryAction.exportV2Report(NewInventorySchemaIds[this.inventoryId]);
                    } else {
                        this.inventoryAction.exportInventory();
                    }
                },
            });
        }
        if (this.inventoryPermissions.canExportArt30) {
            exportOptions.push({
                id: "Article30ReportExportInventoryList",
                text: this.translatePipe.transform("Article30ReportExport"),
                action: () => {
                    this.inventoryAction.exportArt30Report();
                },
            });
        }
        return (): IDropdownOption[] => exportOptions;
    }

    toggleFiltersDrawer = () => {
        if (this.drawerIsOpen && this.isAttributeDrawerOpen) {
            if (this.hasEdits && this.requiredFieldsValid) {
                const callback = () => {
                    this.closeDrawer();
                    setTimeout(() => {
                        this.drawerReady = true;
                        this.inventoryAction.openFilterDrawer();
                    }, 500);
                };
                this.launchSaveChangesModal(callback);
            } else {
                this.closeDrawer();
                setTimeout(() => {
                    this.drawerReady = true;
                    this.inventoryAction.openFilterDrawer();
                }, 500);
            }
        } else if (this.drawerIsOpen && this.isFilterDrawerOpen) {
            this.closeDrawer();
        } else {
            this.drawerReady = true;
            this.inventoryAction.openFilterDrawer();
        }
    }

    addAssessmentLink(record: IInventoryRecord): void {
        const inventoryName = this.inventoryAdapter.getCellLabelByAttributeCode(record, NamesAttributes[this.inventoryId]);
        const inventoryOrg = this.inventoryAdapter.getCellLabelByAttributeCode(record, ManagingOrgAttributes[this.inventoryId]);
        const inventoryType = this.inventoryAdapter.getCellLabelByAttributeCode(record, TypesAttributes[this.inventoryId]);
        const inventoryLocation = this.inventoryAdapter.getCellLabelByAttributeCode(record, LocationAttributes[this.inventoryId]);
        let recordName = `${inventoryName} | ${inventoryOrg}`;
        if (this.inventoryId === InventoryTableIds.Vendors) recordName = `${recordName} | ${inventoryType}`;
        if (this.inventoryId === InventoryTableIds.Assets) recordName = `${recordName} | ${inventoryLocation}`;
        this.inventoryAction.addAssessmentLink(record.id, null, this.inventoryId, recordName);
    }

    viewRecordRisks(record: IInventoryRecord): void {
        this.stateService.go(InventoryDetailRoutes[this.inventoryId], {
            recordId: record.id,
            inventoryId: this.inventoryId,
            tabId: InventoryDetailsTabs.Risks,
        });
    }

    handleBulkDelete() {
        const records: IInventoryRecord[] = getSelectedRecords(this.store.getState());
        const newPage: number = this.table.metaData.numberOfElements > records.length || this.table.metaData.number === 0 ? this.table.metaData.number : this.table.metaData.number - 1;
        if (this.hasEdits && this.requiredFieldsValid) {
            const callback = () => {
                this.closeDrawer();
                setTimeout(() => {
                    this.inventoryAction.bulkDeleteRecords(records, newPage, this.table.metaData.size);
                }, 0);
            };
            this.launchSaveChangesModal(callback);
        } else {
            this.closeDrawer();
            this.inventoryAction.bulkDeleteRecords(records, newPage, this.table.metaData.size);
        }
    }

    private setTransitionAwayHook() {
        const exitRoute = this.inventoryId === InventoryTableIds.Vendors ? "zen.app.pia.module.vendor.inventory.list" : "zen.app.pia.module.datamap.views.inventory.list";
        this.transitionDeregisterHook = this.$transitions.onExit({ from: exitRoute }, (transition: Transition): HookResult => {
            return this.handleStateChange(transition.to(), transition.params("to"), transition.options);
        });
    }

    private launchSaveChangesModal(callback: () => void) {
        const modalData: ISaveChangesModalResolve = {
            modalTitle: this.translatePipe.transform("SaveChanges"),
            confirmationText: this.translatePipe.transform("UnsavedChangesSaveBeforeContinuing"),
            cancelButtonText: this.translatePipe.transform("Cancel"),
            discardChangesButtonText: this.translatePipe.transform("Discard"),
            saveButtonText: this.translatePipe.transform("Save"),
            promiseToResolve: () => this.saveRecord().then((): boolean => {
                callback();
                return true;
            }),
            discardCallback: () => {
                this.saveCancelled();
                callback();
                return true;
            },
        };
        this.modalService.setModalData(modalData);
        this.modalService.openModal("saveChangesModal");
        return;
    }

    private handleStateChange(toState: IStringMap<any>, toParams: IStringMap<any>, options: IStringMap<any>): boolean {
        if (!this.hasEdits || !this.requiredFieldsValid) return true;
        const callback = () => {
            this.closeDrawer();
            this.stateService.go(toState, toParams, options);
        };
        this.launchSaveChangesModal(callback);
        return false;
    }

    private componentWillReceiveState(state: IStoreState) {
        this.showingFilteredResults = isShowingFilteredResults(state);
        this.currentSearchTerm = getSearchTerm(state);
        this.inventoryId = getInventoryId(state);
        this.currentPageNumber = getPageNumber(state);
        this.drawerIsOpen = getDrawerState(state);
        this.isAttributeDrawerOpen = getAttributeDrawerState(state);
        this.isFilterDrawerOpen = getFilterDrawerState(state);
        this.filterData = getFilterData(state);
        this.isFiltering = isFilteringInventory(state);
        const loadingInventory: boolean = isFetchingInventoryList(state);
        if (!loadingInventory) {
            this.table = {
                rows: getRecordList(state),
                columns: filter(getAttributeList(state), (attribute: IAttribute): boolean => {
                    if (this.permissions.canShow("DMAssessmentV2")
                    && (attribute.fieldAttributeCode === InventoryAttributeCodes[this.inventoryId].Status)) {
                        return false;
                    }
                    return AttributeAllowedContexts[InventoryContexts.List][attribute.fieldAttributeCode];
                }),
                metaData: getInventoryListPaginationMetadata(state),
            };
            const selectedRecords: IInventoryRecord[] = getSelectedRecords(state);
            this.selectedCount = selectedRecords.length;
            this.bulkEditMode = this.selectedCount > 0;
            const nonDisabledRowCount: number = filter(this.table.rows, (row: IInventoryRecord): boolean => row.canEdit && !row.assessmentOutstanding).length;
            this.allRowsSelected = nonDisabledRowCount && this.selectedCount === nonDisabledRowCount;
            this.isExporting = isExportingInventory(state);
        }
        this.isLoading = loadingInventory;
        this.params.searchTerm = this.currentSearchTerm;

        // v2 record
        if (AllowV1Details[this.inventoryId]) {
            this.recordEditEnabled = recordEditEnabled(state);
            this.savingRecord = isSavingRecord(state);
            this.currentRecord = getCurrentRecord(state);
            this.hasEdits = recordEdited(state);
            this.requiredFieldsValid = requiredFieldsValid(state);
            this.usersValid = getAllUsersValid(state);
        } else {
            this.recordEditEnabled = getIsEditingRecord(state);
            this.savingRecord = getIsLoadingRecord(state);
            this.currentRecord = getRecord(state);
            this.hasEdits = getHasChanges(state);
            this.requiredFieldsValid = getHasRequiredValues(state);
            this.usersValid = !getHasErrors(state);
        }
    }

    private getInventoryList() {
        this.inventoryAction.fetchInventoryList().then((response: IProtocolPacket) => {
            if (!response.result) return;
            this.name = response.data.nameKey ? this.translatePipe.transform(response.data.nameKey) : response.data.name;
            this.inventoryAction.setFilterData(this.inventoryRisk.formatRiskFilterForDisplay(this.filterData));
            this.viewReady = true;
        });
    }

    private closeDrawer() {
        if (this.drawerIsOpen) {
            this.inventoryAction.closeAllDrawers();
            this.drawerReady = false;
            if (this.recordEditEnabled) {
                this.inventoryAction.recordEditDisabled();
            }
        }
    }

    private clearFilterData() {
        this.inventoryAction.setDefaultFilterData();
    }

    private clearSearchData() {
        this.inventoryAction.setSearchTerm("");
        sessionStorage.removeItem(`DMInventorySearchTerm${this.inventoryId}`);
    }

    private getRecord(row: IInventoryRecord): ng.IPromise<boolean | IProtocolResponse<IInventoryDetails>> {
        if (AllowV1Details[this.inventoryId]) {
            return this.inventoryAction.fetchInventoryRecord(row.id);
        }
        return this.recordActionService.setRecordById(NewInventorySchemaIds[this.inventoryId], this.currentRecordId);
    }
}
