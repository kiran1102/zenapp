// Rxjs
import { Subscription, Subject } from "rxjs";
import { startWith } from "rxjs/operators";

// 3rd Party
import {
    Component,
    OnInit,
    OnDestroy,
    Inject,
    ChangeDetectorRef,
} from "@angular/core";

// Tokens
import { StoreToken } from "tokens/redux-store.token";

// Redux
import observableFromStore from "oneRedux/utils/observable-from-store";

import {
    getIsLoadingRecord,
    getHasErrors,
    getHasRequiredValues,
    getRecord,
} from "oneRedux/reducers/inventory-record.reducer";

// Services
import { Permissions } from "modules/shared/services/helper/permissions.service";
import { RecordActionService } from "inventoryModule/services/action/record-action.service";
import { InventoryListActionService } from "inventoryModule/services/action/inventory-list-action.service";
import { UserActionService } from "oneServices/actions/user-action.service";

// Interface
import { IProtocolResponse } from "interfaces/protocol.interface";
import {
    IAttributeDetails,
    IInventoryRecordV2,
} from "interfaces/inventory.interface";
import {
    IStore,
    IStoreState,
} from "interfaces/redux.interface";
import { IStringMap } from "interfaces/generic.interface";
import { IOtModalContent } from "@onetrust/vitreus";

// Constants and Enums
import { InventoryContexts } from "enums/inventory.enum";
import {
    InventoryPermissions,
    OwnerFieldNames,
} from "constants/inventory.constant";
import { NewInventorySchemaIds } from "constants/inventory-config.constant";

@Component({
    selector: "inventory-add-modal",
    templateUrl: "./inventory-add-modal.component.html",
})
export class InventoryAddModalComponent implements OnInit, OnDestroy, IOtModalContent {
    usersValid: boolean;
    canLaunch: boolean;
    record: IInventoryRecordV2;
    attributeSections: IAttributeDetails;
    isSaving: boolean;
    inventoryContexts = InventoryContexts;
    newInventorySchemaIds = NewInventorySchemaIds;
    recordIsValid: boolean;
    isLoadingUsers = false;
    sendAssessment: boolean;
    inventoryPermissions: IStringMap<boolean>;
    otModalCloseEvent: Subject<boolean>;
    otModalData: { inventoryId: number, title: string };
    private subscription: Subscription;

    constructor(
        @Inject(StoreToken) public store: IStore,
        @Inject(Permissions) public permissions: Permissions,
        private recordActionService: RecordActionService,
        private inventoryListActionService: InventoryListActionService,
        private userActions: UserActionService,
        private changeDetector: ChangeDetectorRef,
    ) { }

    ngOnInit() {
        this.subscription = observableFromStore(this.store).pipe(
            startWith(this.store.getState()),
        ).subscribe((state: IStoreState) => this.componentWillReceiveState(state));
        this.canLaunch = this.permissions.canShow(InventoryPermissions[this.otModalData.inventoryId].launchV2Assessment);
        this.changeDetector.detectChanges();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

    closeModal(reloadList: boolean = false) {
        this.otModalCloseEvent.next(reloadList);
    }

    saveAndClose() {
        this.isSaving = true;
        this.saveRecord(() => {
            this.closeModal(true);
        });
    }

    saveAndAddNew() {
        this.isSaving = true;
        this.saveRecord(() => {
            this.recordActionService.setRecord({}, true);
            this.inventoryListActionService.fetchCurrentList();
            this.isSaving = false;
        });
    }

    toggleSendAssessment(event) {
        this.sendAssessment = event;
    }

    routeToSendAssessment(record: IInventoryRecordV2) {
        this.inventoryListActionService.handleLaunchInventoryAssessment([record]);
    }

    private saveRecord(successCb: () => void) {
        this.recordActionService.addRecord(NewInventorySchemaIds[this.otModalData.inventoryId], true).then((response: IProtocolResponse<{ data: IInventoryRecordV2 }>) => {
            this.handleResponse(response, successCb);
        });
    }

    private handleResponse(response: IProtocolResponse<{ data: IInventoryRecordV2 }>, successCb: () => void) {
        if (response.result) {
            if (this.record[OwnerFieldNames[this.otModalData.inventoryId]] && this.record[OwnerFieldNames[this.otModalData.inventoryId]][0] && !this.record[OwnerFieldNames[this.otModalData.inventoryId]][0].id) {
                this.isLoadingUsers = true;
                this.userActions.fetchUsers().then(() => {
                    this.isLoadingUsers = false;
                    this.handleSendAssessment(response, successCb);
                });
            } else {
                this.handleSendAssessment(response, successCb);
            }
        } else {
            this.isSaving = false;
        }
    }

    private handleSendAssessment(response: IProtocolResponse<{ data: IInventoryRecordV2 }>, successCb: () => void) {
        if (this.sendAssessment) {
            const record = { ...this.record, ...response.data.data };
            this.routeToSendAssessment(record);
        }
        successCb();
    }

    private componentWillReceiveState(state: IStoreState) {
        this.record = getRecord(state);
        this.recordIsValid = getHasRequiredValues(state);
        this.usersValid = !getHasErrors(state);
    }
}
