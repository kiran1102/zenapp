// 3rd Party
import { NgModule } from "@angular/core";

// Modules
import { SharedModule } from "sharedModules/shared.module";
import { UpgradedLegacyModule } from "sharedModules/upgrade/upgraded-legacy.module";
import { CommonModule } from "@angular/common";
import { HttpClientModule } from "@angular/common/http";
import { FormsModule } from "@angular/forms";
import { ReactiveFormsModule } from "@angular/forms";
import { VitreusModule } from "@onetrust/vitreus";
import { DndListModule } from "ngx-drag-and-drop-lists";
import { OTPipesModule, PIPES } from "modules/pipes/pipes.module";
import { VirtualScrollModule } from "od-virtualscroll";
import { OneVideoModule } from "modules/video/video.module";
import { VendorSharedModule } from "modules/vendor/shared/vendor-shared.module";
import { CertificatesDetailsModule } from "modules/vendor/certificates/certificates-details.module";
import { InventorySharedModule } from "inventorySharedModule/inventory-shared.module";
import { IntgSharedModule } from "modules/intg-shared/intg-shared.module";
import { FilterModule } from "modules/filter/filter.module";
import { VendorControlsModule } from "modules/vendor/vendor-controls/vendor-controls.module";
import { LinkControlsListModule } from "modules/link-controls/link-controls-list/link-controls-list.module";
import { RiskTableModule } from "modules/risks/risk-table/risk-table.module";
import { InventoryRiskDetailsModule } from "modules/inventory/inventory-risk-details/inventory-risk-details.module";
import { InventoryActivityModule } from "inventoryModule/inventory-activity/inventory-activity.module";
import { InventoryLineageModule } from "inventoryModule/inventory-lineage/inventory-lineage.module";
import { InventoryFormModule } from "inventoryModule/inventory-form/inventory-form.module";
import { InventoryListManagerModule } from "inventoryModule/inventory-list-manager/inventory-list-manager.module";
import { InventoryManageVisibilityModule } from "inventoryModule/inventory-manage-visibility/inventory-manage-visibility.module";

// Services
import { InventoryRiskService } from "inventoryModule/services/adapter/inventory-risk.service";
import { InventoryRiskV2Service } from "inventoryModule/services/adapter/inventory-risk-v2.service";
import { InventoryAssessmentService } from "inventoryModule/services/adapter/inventory-assessment.service";
import { DMAssessmentsService } from "datamappingservice/Assessment/dm-assessments.service";
import { ProjectService } from "oneServices/project.service";
import { InventoryActionService } from "inventoryModule/services/action/inventory-action.service";
import { AttachmentService } from "oneServices/attachment.service";
import { AttachmentActionService } from "oneServices/actions/attachment-action.service";
import { InventoryRelatedService } from "inventoryModule/services/adapter/inventory-related.service";
import { InventoryLinkingService } from "inventoryModule/services/adapter/inventory-linking.service";
import { TimeStamp } from "oneServices/time-stamp.service";
import { InventoryAdapter } from "oneServices/adapters/inventory-adapter.service";
import { InventoryAdapterV2Service } from "inventoryModule/services/adapter/inventory-adapter-v2.service";
import { AssessmentLinkingAPIService } from "modules/assessment/services/api/assessment-linking-api.service";
import { TaskPollingService } from "sharedServices/task-polling.service";
import { InventoryViewLogic } from "oneServices/view-logic/inventory-view-logic.service";
import { InventoryListActionService } from "inventoryModule/services/action/inventory-list-action.service";
import { InventoryLaunchModalService } from "inventoryModule/services/action/inventory-launch-modal.service";
import { InventoryUserLogicService } from "inventoryModule/services/business-logic/inventory-user-logic.service";
import { UserActionService } from "oneServices/actions/user-action.service";
import { RecordActionService } from "inventoryModule/services/action/record-action.service";
import { DMTemplateService } from "datamappingservice/Template/dm-template.service";
import { InventoryRiskModalViewLogicService } from "oneServices/view-logic/inventory-risk-modal-view-logic.service";
import { RiskActionService } from "oneServices/actions/risk-action.service";
import { DrawerActionService } from "oneServices/actions/drawer-action.service";
import { InventoryRiskAPIService } from "inventoryModule/services/api/inventory-risk-api.service";
import { InventoryListManagerService } from "inventoryModule/inventory-list-manager/shared/services/inventory-list-manager-action.service";
import { RisksFilterService } from "modules/risks/risk-table/risks-table/risk-filter/risks-filter.service";

// Components
import { InventoryDetails } from "inventoryModule/components/inventory-details/inventory-details.component";
import { InventoryTableComponentNew } from "inventoryModule/components/inventory-table/inventory-table.component";
import { InventoryTableV2Component } from "inventoryModule/components/inventory-table-v2/inventory-table-v2.component";
import { InventoryAssessmentsComponent } from "inventoryModule/components/inventory-assessments/inventory-assessments.component";
import { InventoryAttachmentsComponent } from "inventoryModule/components/inventory-attachments/inventory-attachments.component";
import { InventoryRelatedListComponent } from "inventoryModule/components/inventory-related-list/inventory-related-list.component";
import { InventoryRelatedTableComponent } from "inventoryModule/components/inventory-related-table/inventory-related-table.component";
import { InventoryLinkedTableComponent } from "inventoryModule/components/inventory-linked-table/inventory-linked-table.component";
import { RelateInventoryModalComponent } from "generalcomponent/Modals/relate-inventory-modal/relate-inventory-modal.component.ts";
import { LinkInventoryModalComponent } from "generalcomponent/Modals/link-inventory-modal/link-inventory-modal.component.ts";
import { RelateDataSubjectModalComponent } from "generalcomponent/Modals/relate-data-subject-modal/relate-data-subject-modal.component.ts";
import { LinkDataSubjectModalComponent } from "generalcomponent/Modals/link-data-subject-modal/link-data-subject-modal.component.ts";
import { AddInventoryModalComponent } from "generalcomponent/Modals/add-inventory-modal/add-inventory-modal.component.ts";
import { CopyInventoryModalComponent } from "generalcomponent/Modals/copy-inventory-modal/copy-inventory-modal.component.ts";
import { CopyInventoryV2ModalComponent } from "generalcomponent/Modals/copy-inventory-v2-modal/copy-inventory-v2-modal.component.ts";
import { ChangeInventoryStatusModalComponent } from "inventoryModule/components/change-inventory-status-modal/change-inventory-status-modal.component";
import { InventoryBulkEditComponent } from "inventoryModule/components/inventory-bulk-edit/inventory-bulk-edit.component";
import { LaunchAssessmentModalComponent } from "generalcomponent/Modals/launch-assessment-modal/launch-assessment-modal.component.ts";
import { BulkEditConfirmationModalComponent } from "inventoryModule/components/bulk-edit-confirmation-modal/bulk-edit-confirmation-modal.component";
import { InventoryListComponent } from "inventoryModule/components/inventory-list/inventory-list.component";
import { InventoryListV2Component } from "inventoryModule/components/inventory-list-v2/inventory-list-v2.component";
import { InventoryListWrapper } from "inventoryModule/components/inventory-list-wrapper/inventory-list-wrapper.component";
import { InventoryAttributeDetails } from "inventoryModule/components/attribute-details/inventory-attribute-details.component";
import { InventoryAttributeCell } from "inventoryModule/components/attribute-cell/inventory-attribute-cell.component";
import { SectionEditModalComponent } from "generalcomponent/Modals/section-edit-modal/section-edit-modal.component";
import { InventoryAddModalComponent } from "inventoryModule/components/inventory-add-modal/inventory-add-modal.component";
import { InventoryListItemModal } from "inventoryModule/components/inventory-list-item-modal/inventory-list-item-modal.component";
import { InventoryManageDataElementsModal } from "inventoryModule/components/inventory-manage-data-elements-modal/inventory-manage-data-elements-modal.component";
import { LinkAssessmentsModal } from "inventoryModule/components/link-assessments-modal/link-assessments-modal.component";
import { InventoryListFilterComponent } from "inventoryModule/components/inventory-list-filter/inventory-list-filter.component";
import { LinkPersonalDataModal } from "inventoryModule/components/link-personal-data-modal/link-personal-data-modal.component";
import { InventoryAttachmentsWrapperComponent } from "inventoryModule/components/inventory-attachments-wrapper/inventory-attachments-wrapper.component";
import { InventoryRisksComponent } from "./components/inventory-risks/inventory-risks.component";

export function getRiskActionService(injector) {
    return injector.get("RiskActionService");
}

export function getDrawerActionService(injector) {
    return injector.get("DrawerActionService");
}

export function getLegacyAssessmentService(injector) {
    return injector.get("DMAssessments");
}

export function getLegacyProjectService(injector) {
    return injector.get("Projects");
}

export function getLegacyAttachmentService(injector) {
    return injector.get("AttachmentService");
}

export function getLegacyAttachmentActionService(injector) {
    return injector.get("AttachmentAction");
}

export function getLegacyTimeStampService(injector) {
    return injector.get("TimeStamp");
}

export function getLegacyPollingService(injector) {
    return injector.get("TaskPollingService");
}

export function getLegacyUserActionService(injector) {
    return injector.get("UserActionService");
}

@NgModule({
    imports: [
        SharedModule,
        UpgradedLegacyModule,
        CommonModule,
        HttpClientModule,
        FormsModule,
        ReactiveFormsModule,
        VitreusModule,
        DndListModule,
        OTPipesModule,
        VirtualScrollModule,
        OneVideoModule,
        VendorSharedModule,
        VendorControlsModule,
        CertificatesDetailsModule,
        LinkControlsListModule,
        InventorySharedModule,
        IntgSharedModule,
        FilterModule,
        InventoryRiskDetailsModule,
        RiskTableModule,
        InventoryActivityModule,
        InventoryLineageModule,
        InventoryFormModule,
        InventoryListManagerModule,
        InventoryManageVisibilityModule,
    ],
    declarations: [
        InventoryListWrapper,
        InventoryDetails,
        AddInventoryModalComponent,
        BulkEditConfirmationModalComponent,
        CopyInventoryModalComponent,
        CopyInventoryV2ModalComponent,
        ChangeInventoryStatusModalComponent,
        InventoryAssessmentsComponent,
        InventoryAttachmentsComponent,
        InventoryAttributeCell,
        InventoryAttributeDetails,
        InventoryBulkEditComponent,
        InventoryLinkedTableComponent,
        InventoryListComponent,
        InventoryListV2Component,
        InventoryRelatedListComponent,
        InventoryRelatedTableComponent,
        InventoryRisksComponent,
        InventoryTableComponentNew,
        InventoryTableV2Component,
        LaunchAssessmentModalComponent,
        LinkDataSubjectModalComponent,
        LinkInventoryModalComponent,
        RelateDataSubjectModalComponent,
        RelateInventoryModalComponent,
        SectionEditModalComponent,
        InventoryAddModalComponent,
        InventoryListItemModal,
        InventoryManageDataElementsModal,
        LinkAssessmentsModal,
        InventoryListFilterComponent,
        LinkPersonalDataModal,
        InventoryAttachmentsWrapperComponent,
    ],
    entryComponents: [
        InventoryListWrapper,
        InventoryDetails,
        InventoryBulkEditComponent,
        AddInventoryModalComponent,
        BulkEditConfirmationModalComponent,
        CopyInventoryModalComponent,
        CopyInventoryV2ModalComponent,
        ChangeInventoryStatusModalComponent,
        LaunchAssessmentModalComponent,
        LinkDataSubjectModalComponent,
        LinkInventoryModalComponent,
        RelateDataSubjectModalComponent,
        RelateInventoryModalComponent,
        SectionEditModalComponent,
        InventoryAddModalComponent,
        InventoryListItemModal,
        InventoryManageDataElementsModal,
        LinkAssessmentsModal,
        LinkPersonalDataModal,
        InventoryAttachmentsWrapperComponent,
    ],
    providers: [
        { provide: DrawerActionService, deps: ["$injector"], useFactory: getDrawerActionService },
        { provide: DMAssessmentsService, deps: ["$injector"], useFactory: getLegacyAssessmentService },
        { provide: ProjectService, deps: ["$injector"], useFactory: getLegacyProjectService },
        { provide: AttachmentService, deps: ["$injector"], useFactory: getLegacyAttachmentService },
        { provide: AttachmentActionService, deps: ["$injector"], useFactory: getLegacyAttachmentActionService },
        { provide: TimeStamp, deps: ["$injector"], useFactory: getLegacyTimeStampService },
        { provide: TaskPollingService, deps: ["$injector"], useFactory: getLegacyPollingService },
        { provide: UserActionService, deps: ["$injector"], useFactory: getLegacyUserActionService },
        DMTemplateService,
        InventoryActionService,
        InventoryAdapter,
        InventoryAdapterV2Service,
        InventoryAdapterV2Service,
        InventoryAssessmentService,
        InventoryLaunchModalService,
        InventoryLinkingService,
        InventoryListActionService,
        InventoryListManagerService,
        InventoryRelatedService,
        InventoryRiskAPIService,
        InventoryRiskModalViewLogicService,
        InventoryRiskService,
        InventoryRiskV2Service,
        InventoryUserLogicService,
        InventoryUserLogicService,
        InventoryViewLogic,
        RecordActionService,
        RecordActionService,
        InventoryRiskAPIService,
        AssessmentLinkingAPIService,
        RiskActionService,
        RisksFilterService,
        PIPES, // TODO: Remove from providers when ot-common is implemented. Only necessary if you are using pipe in TS file, else just module is fine
    ],
})
export class InventoryModule {}
