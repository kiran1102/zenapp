import { NgModule } from "@angular/core";

// Modules
import { SharedModule } from "sharedModules/shared.module";
import { UpgradedLegacyModule } from "sharedModules/upgrade/upgraded-legacy.module";
import { CommonModule } from "@angular/common";
import { HttpClientModule } from "@angular/common/http";
import { FormsModule } from "@angular/forms";
import { ReactiveFormsModule } from "@angular/forms";
import { VitreusModule } from "@onetrust/vitreus";
import { DndListModule } from "ngx-drag-and-drop-lists";
import { OTPipesModule, PIPES } from "modules/pipes/pipes.module";
import { VirtualScrollModule } from "od-virtualscroll";
import { OneVideoModule } from "modules/video/video.module";

// Services
import { InventoryService } from "inventorySharedModule/shared/services/inventory-api.service";

// Components
import { InventoryLookupComponent } from "inventorySharedModule/shared/components/inventory-lookup/inventory-lookup.component";
import { InventoryAddControlsModalComponent } from "inventorySharedModule/shared/components/inventory-add-controls-modal/inventory-add-controls-modal.component";

// Pipes
import { AttributeDisplayValuePipe } from "inventorySharedModule/shared/pipes/attribute-display-value.pipe";
import { UserDisplayAttributeValuePipe } from "inventorySharedModule/shared/pipes/user-display-attribute-value.pipe";

@NgModule({
    imports: [
        SharedModule,
        UpgradedLegacyModule,
        CommonModule,
        HttpClientModule,
        FormsModule,
        ReactiveFormsModule,
        VitreusModule,
        DndListModule,
        OTPipesModule,
        VirtualScrollModule,
        OneVideoModule,
    ],
    declarations: [
        InventoryLookupComponent,
        InventoryAddControlsModalComponent,
        AttributeDisplayValuePipe,
        UserDisplayAttributeValuePipe,
    ],
    exports: [
        InventoryLookupComponent,
        InventoryAddControlsModalComponent,
        AttributeDisplayValuePipe,
        UserDisplayAttributeValuePipe,
    ],
    entryComponents: [
        InventoryAddControlsModalComponent,
    ],
    providers: [
        InventoryService,
        AttributeDisplayValuePipe,
        UserDisplayAttributeValuePipe,
        PIPES, // TODO: Remove from providers when ot-common is implemented. Only necessary if you are using pipe in TS file, else just module is fine
    ],
})
export class InventorySharedModule {}
