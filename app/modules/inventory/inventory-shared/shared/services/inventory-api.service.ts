// 3rd Party
import { Injectable, Inject } from "@angular/core";

// Rxjs
import {
    from,
    Observable,
} from "rxjs";

// Services
import { ProtocolService } from "modules/core/services/protocol.service";
import { TaskPollingService } from "sharedServices/task-polling.service";

// Interfaces
import {
    IProtocolPacket,
    IProtocolConfig,
    IProtocolMessages,
    IProtocolResponse,
} from "interfaces/protocol.interface";
import {
    IRecordsResponse,
    IInventoryRecord,
    IInventoryCellValue,
    IInventorySchemaV2,
    IAttributeV2,
    IAttributeDetailV2,
    IDataCategory,
    IInventoryCell,
    IAttributeOptionV2,
    INewAttributeV2,
    IPageableInventory,
    IInventoryRecordV2,
    ILinkedInventoryRow,
    ILinkedElement,
    IRelatedListOption,
    IInventoryListItem,
    IInventoryRecordResponseV2,
    IRelationshipTypeParams,
    IRelatedTableRow,
    IInventorySetting,
    IInventorySettingList,
    IInventoryElementListItem,
    IInventoryStatusOption,
    IInventoryStatus,
    ILinkPersonalData,
    IPersonalDataItem,
} from "interfaces/inventory.interface";
import {
    IInventoryManageVisibilityState,
    IInventoryManageElementVisibilityPayload,
} from "inventoryModule/inventory-manage-visibility/shared/interfaces/inventory-manage-visibility.interface";
import { IAttributeGroup } from "interfaces/attributes.interface";
import { IPaginatedResponse } from "interfaces/pagination.interface";
import { IStringMap } from "interfaces/generic.interface";
import { IMapDataPoint } from "modules/data-mapping/interfaces/dm-map-data-point.interface";
import { IReportExport } from "modules/reports/reports-shared/interfaces/report.interface";
import { ITransfer } from "modules/data-mapping/interfaces/dm-cross-border.interface";
import {
    IFilterColumnOption,
    IFilterTree,
} from "interfaces/filter.interface";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

// Constants + Enums
import {
    InventoryTableNames,
    InventoryAttributeCodes,
    RelatedRecordAttributes,
    AttributeTypes,
} from "constants/inventory.constant";
import { InventoryTableIds } from "enums/inventory.enum";
import { NewInventorySchemaIds } from "constants/inventory-config.constant";

interface ISchemaQueryParams {
    slim: boolean;
}

@Injectable()
export class InventoryService {

    constructor(
        readonly translatePipe: TranslatePipe,
        private readonly OneProtocol: ProtocolService,
        private readonly taskPollingService: TaskPollingService,
        @Inject("$q") private readonly $q: ng.IQService,
    ) { }

    getSchemasV2 = (): ng.IPromise<IProtocolResponse<IInventorySchemaV2[]>> => {
        const config: IProtocolConfig = this.OneProtocol.customConfig("GET", "/api/inventory/v2/schemas");
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("Error.GetInventoryOptions") } };
        return this.OneProtocol.http(config, messages);
    }

    // Deprecated - use getAllAttributes or getAttributes
    getSchema = (id: string, queryParams?: ISchemaQueryParams): any => {
        const config: IProtocolConfig = id === InventoryTableIds.Elements.toString() ? this.OneProtocol.config("GET", `/api/inventory/v1/schema/10`, queryParams) : this.OneProtocol.config("GET", `/Inventory/Schema/${id}`, queryParams);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("Error.GetInventoryOptions") } };
        return this.OneProtocol.http(config, messages);
    }

    // Deprecated - use getInventoryList
    getRecords = (inventoryType: string, params?: any): ng.IPromise<IProtocolResponse<IRecordsResponse>> => {
        params = params || { size: 20, page: 0, filter: { name: "" } };
        const config: IProtocolConfig = inventoryType === InventoryTableIds.Elements.toString() ? this.OneProtocol.config("GET", `/api/inventory/v1/inventories/10`, params) : this.OneProtocol.config("GET", `/Inventory/Inventories/${inventoryType}`, params);
        const messages: IProtocolMessages = { Error: { Hide: true } };
        return this.OneProtocol.http(config, messages);
    }

    // Deprecated - Need V2 equivalent
    getFilteredRecords = (inventoryType: string, filterData: any, params?: any): ng.IPromise<IProtocolResponse<IRecordsResponse>> => {
        params = params || { size: 20, page: 0, filter: { name: "" } };
        const config: IProtocolConfig = inventoryType === InventoryTableIds.Elements.toString() ? this.OneProtocol.config("POST", `/api/inventory/v1/inventories/10/filter`, params, filterData) : this.OneProtocol.config("POST", `/Inventory/InventoriesByFilter/${inventoryType}`, params, filterData);
        const messages: IProtocolMessages = { Error: { Hide: true } };
        return this.OneProtocol.http(config, messages);
    }

    // Deprecated - use getRecordV2
    getRecord = (inventoryId: string, recordId: string): any => {
        const config: IProtocolConfig = inventoryId === InventoryTableIds.Elements.toString() ? this.OneProtocol.config("GET", `/api/inventory/v1/inventories/10/${recordId}`) : this.OneProtocol.config("GET", `/Inventory/Inventory/${recordId}`);
        const messages: IProtocolMessages = { Error: { Hide: true } };
        return this.OneProtocol.http(config, messages);
    }

    // Deprecated - use updateRecordV2
    upsertRecord = (inventoryType: string, record: any, recordId?: string): any => {
        const type: string = recordId ? "PUT" : "POST";
        const errorMessage: string = recordId ? "ErrorUpdatingRecord" : "ErrorAddingRecord";
        const successMessage: string = recordId ? "UpdatedRecordSuccessfully" : "AddedRecordSuccessfully";
        const config: IProtocolConfig = inventoryType === InventoryTableIds.Elements.toString() ? this.OneProtocol.config(type, `/api/inventory/v1/change/elements/${recordId}`, [], record) : this.OneProtocol.config(type, `/Inventory/${this.getInventoryTypeSingular(inventoryType)}/${recordId}`, [], record);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform(errorMessage) }, Success: { custom: this.translatePipe.transform(successMessage) } };
        return this.OneProtocol.http(config, messages);
    }

    deleteRecord = (inventoryType: number, recordId: string): any => {
        const config: IProtocolConfig = this.OneProtocol.customConfig("DELETE", `/api/inventory/v2/inventories/${NewInventorySchemaIds[inventoryType]}/${recordId}`);
        const messages: IProtocolMessages = { Error: { Hide: true } };
        return this.OneProtocol.http(config, messages);
    }

    getColumns = (inventoryType: number): ng.IPromise<IProtocolResponse<IFilterColumnOption[]>> => {
        const config: IProtocolConfig = inventoryType === InventoryTableIds.Elements ? this.OneProtocol.config("GET", `/api/inventory/v1/schemas/10/filtermetadata`) : this.OneProtocol.config("GET", `/Inventory/Filtermetadata/${inventoryType}`);
        const messages: IProtocolMessages = { Error: { object: this.translatePipe.transform("Inventory"), action: this.translatePipe.transform("get") } };
        return this.OneProtocol.http(config, messages);
    }

    bulkDeleteRecords = (inventoryType: number, records: Array<IInventoryRecord | IInventoryRecordV2>): ng.IPromise<boolean> => {
        const deletePromises: Array<ng.IPromise<IProtocolPacket>> = [];
        records.forEach((record: IInventoryRecord | IInventoryRecordV2): void => { deletePromises.push(this.deleteRecord(inventoryType, record.id as string)); });
        return this.$q.all(deletePromises).then((responses: IProtocolPacket[]): boolean => {
            const failedResponse: IProtocolPacket = responses.find((response: IProtocolPacket): boolean => !response.result);
            return !Boolean(failedResponse);
        });
    }

    deleteAssessment(assessmentId: string): ng.IPromise<IProtocolResponse<boolean>> {
        const config: IProtocolConfig = this.OneProtocol.config("DELETE", `/assessmentv2/assessments/${assessmentId}`);
        const messages: IProtocolMessages = {
            Error: { custom: this.translatePipe.transform("ErrorDeletingAssessment") },
            Success: { custom: this.translatePipe.transform("AssessmentDeleted") },
        };
        return this.OneProtocol.http(config, messages);
    }

    getAssetMapData = (orgGroupId: string): Observable<IProtocolResponse<{ data: IMapDataPoint[] }>> => {
        const config: IProtocolConfig = this.OneProtocol.config("GET", `/api/inventory/v2/assetmap/${orgGroupId}`);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("ErrorRetrievingAssets") } };
        return from(this.OneProtocol.http(config, messages));
    }

    getInventoryList(inventoryType: number, params?: { size: number, page: number, filter: { name: string } }): ng.IPromise<IProtocolResponse<IPaginatedResponse<IInventoryRecordV2>>> {
        params = params || { size: 20, page: 0, filter: { name: "" } };
        const config: IProtocolConfig = this.OneProtocol.customConfig("GET", `/api/inventory/v2/inventories/${NewInventorySchemaIds[inventoryType]}`, params);
        const messages: IProtocolMessages = { Error: { Hide: true } };
        return this.OneProtocol.http(config, messages);
    }

    getFilteredInventoryList(inventoryType: number, filterData: { filterCriteria: IFilterTree }, params?: { size: number, page: number }): ng.IPromise<IProtocolResponse<IPaginatedResponse<IInventoryRecordV2>>> {
        params = params || { size: 20, page: 0 };
        const config: IProtocolConfig = this.OneProtocol.customConfig("POST", `/api/inventory/v2/inventories/${NewInventorySchemaIds[inventoryType]}/filter`, params, filterData);
        const messages: IProtocolMessages = { Error: { Hide: true } };
        return this.OneProtocol.http(config, messages);
    }

    getFilterMetadata(inventoryType: number): ng.IPromise<IProtocolResponse<IFilterColumnOption[]>> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("GET", `/api/inventory/v2/schemas/${NewInventorySchemaIds[inventoryType]}/filtermetadata`);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("ErrorGettingAttributes") } };
        return this.OneProtocol.http(config, messages);
    }

    getSearchedInventoryList(inventoryType: number, orgGroupId: string): ng.IPromise<IProtocolResponse<IRelatedListOption[]>> {
        const params = { orgGroupId, search: true };
        const config: IProtocolConfig = this.OneProtocol.customConfig("GET", `/api/inventory/v2/inventories/${NewInventorySchemaIds[inventoryType]}`, params);
        const messages: IProtocolMessages = { Error: { Hide: true } };
        return this.OneProtocol.http(config, messages);
    }

    getDataSubjects(orgGroupId: string): ng.IPromise<IProtocolResponse<IInventoryCellValue[]>> {
        const config: IProtocolConfig = this.OneProtocol.config("GET", `/api/inventory/v1/subjects`, { orgGroupId });
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("ErrorRetrievingRelatedInventory") } };
        return this.OneProtocol.http(config, messages);
    }

    getDataCategories(subjectId: string, orgGroupId: string): ng.IPromise<IProtocolResponse<IDataCategory[]>> {
        const config: IProtocolConfig = this.OneProtocol.config("GET", `/api/inventory/v1/elements/bySubject/${subjectId}`, { orgGroupId });
        return this.OneProtocol.http(config);
    }

    getDataSubjectElements(recordId: string, subjectId: string): ng.IPromise<IProtocolResponse<ILinkedElement[]>> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("GET", `/api/inventory/v2/inventories/${recordId}/elementsubjectcategories/${subjectId}`);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("ErrorRetrievingRelatedInventory") } };
        return this.OneProtocol.http(config, messages);
    }

    getRelatedInventory(recordId: string, inventoryType: number, params: IStringMap<number>): ng.IPromise<IProtocolResponse<IPaginatedResponse<ILinkedInventoryRow | IRelatedTableRow>>> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("GET", `/api/inventory/v2/inventories/${recordId}/relations/${NewInventorySchemaIds[inventoryType]}`, params);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("ErrorRetrievingRelatedInventory") } };
        return this.OneProtocol.http(config, messages);
    }

    getRelatedPersonalData(recordId: string, params: IStringMap<number>): Observable<IProtocolResponse<IPaginatedResponse<IPersonalDataItem>>> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("GET", `/api/inventory/v2/inventories/${recordId}/personal-data`, params);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("ErrorRetrievingRelatedInventory") } };
        return from(this.OneProtocol.http(config, messages));
    }

    linkPersonalData(recordId: string, params: ILinkPersonalData[]): Observable<IProtocolResponse<IPersonalDataItem[]>> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("POST", `/api/inventory/v2/inventories/${recordId}/personal-data`, null, params);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("ErrorAddingInventoryLink") } };
        return from(this.OneProtocol.http(config, messages));
    }

    removePersonalDataLink(recordId: string, params: ILinkPersonalData[]): Observable<IProtocolResponse<null>> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("DELETE", `/api/inventory/v2/inventories/${recordId}/personal-data`, null, params);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("ErrorAddingInventoryLink") } };
        return from(this.OneProtocol.http(config, messages));
    }

    getDataSubjectDetails(subjectId: string, orgGroupId: string): Observable<IProtocolResponse<{ data: IInventoryListItem[] }>> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("GET", `/api/inventory/v2/data-categories/details?subjectTypeId=${subjectId}&orgGroupId=${orgGroupId}`);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("ErrorRetrievingRelatedInventory") } };
        return from(this.OneProtocol.http(config, messages));
    }

    getLinkAssociationType(contextInventoryType: string, relatedInventoryType: string): ng.IPromise<IProtocolResponse<IInventoryListItem[]>> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("GET", `/api/inventory/v2/relations/types/for/${contextInventoryType}/${relatedInventoryType}`);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("ErrorRetrievingRelatedInventory") } };
        return this.OneProtocol.http(config, messages);
    }

    addInventoryLink(recordId: string, params: Array<IStringMap<string>>): ng.IPromise<IProtocolResponse<ILinkedInventoryRow[]>> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("POST", `/api/inventory/v2/inventories/${recordId}/relations`, null, params);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("ErrorAddingInventoryLink") } };
        return this.OneProtocol.http(config, messages);
    }

    removeInventoryLink(recordId: string, params: Array<{ inventoryAssociationId: string }>): ng.IPromise<IProtocolResponse<null>> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("DELETE", `/api/inventory/v2/inventories/${recordId}/relations`, null, params);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("ErrorRemovingInventoryLink") } };
        return this.OneProtocol.http(config, messages);
    }

    getActivityData = (recordId: string, page: number = 0, size: number = 8): ng.IPromise<IProtocolResponse<IPageableInventory>> => {
        const config: IProtocolConfig = this.OneProtocol.customConfig("GET", `/api/audit/v1/audit/${recordId}/history`, { page, size });
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("ErrorRetrievingActivity") } };
        return this.OneProtocol.http(config, messages);
    }

    exportInventory = (inventoryType: string): ng.IPromise<IProtocolResponse<null>> => {
        this.taskPollingService.startPolling(5000);
        const config: IProtocolConfig = this.OneProtocol.config("GET", `/api/inventory/v1/reports/${inventoryType}`);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("ExportFailed") } };
        return this.OneProtocol.http(config, messages);
    }

    exportArt30Report = (): ng.IPromise<IProtocolResponse<null>> => {
        this.taskPollingService.startPolling(5000);
        const config: IProtocolConfig = this.OneProtocol.config("GET", `/Inventory/DownloadArticle30Report`);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("ExportFailed") } };
        return this.OneProtocol.http(config, messages);
    }

    exportV2Report = (schemaId: string): ng.IPromise<IProtocolResponse<null>> => {
        this.taskPollingService.startPolling(5000);
        const config: IProtocolConfig = this.OneProtocol.customConfig("GET", `/api/inventory/v2/inventories/${schemaId}/export`);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("ExportFailed") } };
        return this.OneProtocol.http(config, messages);
    }

    exportPDF = (requestPayload: IReportExport): ng.IPromise<IProtocolResponse<string>> => {
        this.taskPollingService.startPolling(5000);
        const config: IProtocolConfig = this.OneProtocol.customConfig("PUT", `/reporting/async/export/entity`, null, requestPayload, null, null, null, "text");
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("ExportFailed") } };
        return this.OneProtocol.http(config, messages);
    }

    getAttributes = (schemaId: string, showAll: boolean = false): ng.IPromise<IProtocolResponse<IAttributeV2[]>> => {
        const config: IProtocolConfig = this.OneProtocol.customConfig("GET", `/api/inventory/v2/schemas/${schemaId}`, { showAll: showAll || undefined });
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("ErrorGettingAttributes") } };
        return this.OneProtocol.http(config, messages);
    }

    getAllAttributes = (schemaId: string): ng.IPromise<IProtocolResponse<IAttributeDetailV2[]>> => {
        const config: IProtocolConfig = this.OneProtocol.customConfig("GET", `/api/inventory/v2/schemas/${schemaId}/details`);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("ErrorGettingAttributes") } };
        return this.OneProtocol.http(config, messages);
    }

    getAttributeDetails = (attributeId: string): ng.IPromise<IProtocolResponse<IAttributeDetailV2>> => {
        const config: IProtocolConfig = this.OneProtocol.customConfig("GET", `/api/inventory/v2/schemas/attributes/${attributeId}/details?showAll=true`);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("ErrorGettingAttributeDetails") } };
        return this.OneProtocol.http(config, messages);
    }

    addAttributeOption = (attributeId: string, option: IAttributeOptionV2): ng.IPromise<IProtocolResponse<IAttributeOptionV2>> => {
        const config: IProtocolConfig = this.OneProtocol.customConfig("POST", `/api/inventory/v2/schemas/attributes/${attributeId}/values/`, null, option);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("ErrorAddingAttributeOption") } };
        return this.OneProtocol.http(config, messages);
    }

    bulkAddAttributeOptions = (attributeId: string, options: IAttributeOptionV2[]): ng.IPromise<IProtocolResponse<IAttributeOptionV2[]>> => {
        const config: IProtocolConfig = this.OneProtocol.customConfig("POST", `/api/inventory/v2/schemas/attributes/${attributeId}/values/bulk/`, null, options);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("ErrorAddingAttributeOption") } };
        return this.OneProtocol.http(config, messages);
    }

    editAttributeOption = (attributeId: string, option: IAttributeOptionV2): ng.IPromise<IProtocolResponse<IAttributeOptionV2>> => {
        const optionId: string = option.id;
        delete option.id;
        const config: IProtocolConfig = this.OneProtocol.customConfig("PUT", `/api/inventory/v2/schemas/attributes/${attributeId}/values/${optionId}`, null, option);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("ErrorEditingAttributeOption") } };
        return this.OneProtocol.http(config, messages);
    }

    addAttribute = (schemaId: string, attribute: IAttributeDetailV2): ng.IPromise<IProtocolResponse<IAttributeDetailV2>> => {
        const payload: INewAttributeV2 = {
            name: attribute.name,
            description: attribute.description,
            responseType: attribute.responseType,
            allowOther: attribute.allowOther,
            attributeType: attribute.attributeType || AttributeTypes.None,
        };
        const config: IProtocolConfig = this.OneProtocol.customConfig("POST", `/api/inventory/v2/schemas/${schemaId}/attributes/`, null, payload);
        const messages: IProtocolMessages = {
             Error: { custom: this.translatePipe.transform("ErrorAddingAttribute")},
             Success: { custom: this.translatePipe.transform("AttributeSuccessfullyAdded")},
            };
        return this.OneProtocol.http(config, messages);
    }

    editAttribute = (schemaId: string, attribute: IAttributeDetailV2): ng.IPromise<IProtocolResponse<IAttributeDetailV2>> => {
        const attributeId: string = attribute.id;
        const payload: INewAttributeV2 = {
            name: attribute.name,
            description: attribute.description,
            responseType: attribute.responseType,
            allowOther: attribute.allowOther,
            attributeType: attribute.attributeType,
        };
        const config: IProtocolConfig = this.OneProtocol.customConfig("PUT", `/api/inventory/v2/schemas/${schemaId}/attributes/${attributeId}`, null, payload);
        const messages: IProtocolMessages = {
            Error: { custom: this.translatePipe.transform("ErrorEditingAttribute")},
            Success: { custom: this.translatePipe.transform("AttributeSuccessfullyUpdated")},
         };
        return this.OneProtocol.http(config, messages);
    }

    getOrgGroupId(record: IInventoryRecord, inventoryId: number): string {
        if (record && record.cells) {
            const managingOrg: IInventoryCell =
                record.cells.find((cell: IInventoryCell) => cell.attributeCode === InventoryAttributeCodes[inventoryId].Managing_Organization) ||
                record.cells.find((cell: IInventoryCell) => cell.attributeCode === InventoryAttributeCodes[inventoryId].Organization);
            return managingOrg && managingOrg.values ? managingOrg.values[0].valueId : "";
        } else {
            return "";
        }
    }

    attributeIsRelated(attributeCode: string, inventoryId: number): boolean {
        return RelatedRecordAttributes[inventoryId].includes(attributeCode);
    }

    getRecordV2(schemaId: string, recordId: string): ng.IPromise<IProtocolResponse<IInventoryRecordResponseV2>> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("GET", `/api/inventory/v2/inventories/${schemaId}/${recordId}`);
        const messages: IProtocolMessages = { Error: { Hide: true } };
        return this.OneProtocol.http(config, messages);
    }

    editAttributeStatus = (schemaId: string, attributeId: string, status: string): ng.IPromise<IProtocolResponse<IAttributeV2>> => {
        const payload: { status: string } = { status };
        const config: IProtocolConfig = this.OneProtocol.customConfig("PUT", `/api/inventory/v2/schemas/${schemaId}/attributes/${attributeId}`, null, payload);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("ErrorEditingAttribute") } };
        return this.OneProtocol.http(config, messages);
    }

    updateRecordV2(schemaId: string, recordId: string, payload: IInventoryRecordV2): ng.IPromise<IProtocolResponse<IInventoryRecordV2>> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("PUT", `/api/inventory/v2/inventories/${schemaId}/${recordId}`, null, payload);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("ErrorUpdatingRecord") }, Success: { custom: this.translatePipe.transform("UpdatedRecordSuccessfully") } };
        return this.OneProtocol.http(config, messages);
    }

    bulkUpdateRecordV2(schemaId: string, payload: IInventoryRecordV2, ids: string[]): ng.IPromise<IProtocolResponse<IInventoryRecordV2>> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("POST", `/api/inventory/v2/inventories/${schemaId}/bulk?ids=${ids}`, null, payload);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("ErrorUpdatingRecord") }, Success: { custom: this.translatePipe.transform("SuccessfullyUpdatedRecords") } };
        return this.OneProtocol.http(config, messages);
    }

    getTransfers(isV2: boolean): ng.IPromise<IProtocolResponse<ITransfer[]>> {
        const queryString = isV2 ? "/api/inventory/v2/transfers" : "/api/v1/private/Inventory/Transfer";
        const config: any = this.OneProtocol.customConfig("GET", queryString);
        const messages: any = { Error: { object: this.translatePipe.transform("transfers"), action: this.translatePipe.transform("getting") } };
        return this.OneProtocol.http(config, messages);
    }

    getPATransfer(processintActivityId: string): ng.IPromise<IProtocolResponse<ITransfer[]>> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("GET", `/api/inventory/v2/transfers/${processintActivityId}`);
        const messages: any = { Error: { object: this.translatePipe.transform("transfers"), action: this.translatePipe.transform("getting") } };
        return this.OneProtocol.http(config, messages);
    }

    getDynamicAssociationTypes(contextInventoryType: string, relatedQuestionType: string): ng.IPromise<IProtocolResponse<IRelationshipTypeParams[]>> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("GET", `/api/inventory/v2/relations/types/for/${contextInventoryType}/${relatedQuestionType}`);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("ErrorRetrievingRelatedInventory") } };
        return this.OneProtocol.http(config, messages);
    }

    getDetailsByIds(inventoryType: string, recordIds: string[]): ng.IPromise<IProtocolResponse<{data: IInventoryRecordV2[]}>> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("GET", `/api/inventory/v2/inventories/${inventoryType}?ids=${recordIds.join(",")}`);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("ErrorRetrievingRelatedInventory") } };
        return this.OneProtocol.http(config, messages);
    }

    copyRecord(inventoryType: string, recordId: string, payload: IStringMap<string | IAttributeOptionV2>, copyRelated: boolean): ng.IPromise<IProtocolResponse<null>> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("POST", `/api/inventory/v2/inventories/${inventoryType}/${recordId}/copy?copyRelated=${copyRelated}`, null, payload);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("ErrorCopyingRecord") }, Success: { custom: this.translatePipe.transform("CopiedRecordSuccessfully") }  };
        return this.OneProtocol.http(config, messages);
    }

    addAttributeGroup(inventoryType: string, name: string): ng.IPromise<IProtocolResponse<IAttributeGroup>> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("POST", `/api/inventory/v2/screens/inventory-detail/groups?schemaName=${inventoryType}`, null, { name });
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("ErrorAddingGroup") } };
        return this.OneProtocol.http(config, messages);
    }

    updateAttributeGroup(inventoryType: string, groupId: string, payload: IAttributeGroup): ng.IPromise<IProtocolResponse<IAttributeGroup>> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("PUT", `/api/inventory/v2/screens/inventory-detail/groups/${groupId}?schemaName=${inventoryType}`, null, payload);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("ErrorUpdatingGroup") } };
        return this.OneProtocol.http(config, messages);
    }

    getAttributeGroups(inventoryType: string): ng.IPromise<IProtocolResponse<IAttributeGroup[]>> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("GET", `/api/inventory/v2/screens/inventory-detail/groups?schemaName=${inventoryType}`);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("ErrorRetrievingGroups") } };
        return this.OneProtocol.http(config, messages);
    }

    deleteAttributeGroup(inventoryType: string, groupId: string): ng.IPromise<IProtocolResponse<void>> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("DELETE", `/api/inventory/v2/screens/inventory-detail/groups/${groupId}?schemaName=${inventoryType}`);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("ErrorDeletingGroup") } };
        return this.OneProtocol.http(config, messages);
    }

    updateAttributesInGroup(inventoryType: string, groupId: string, payload: string[]): ng.IPromise<IProtocolResponse<void>> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("PUT", `/api/inventory/v2/screens/inventory-detail/groups/${groupId}/attributes?schemaName=${inventoryType}`, null, payload);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("ErrorAddingAttributesToGroup") } };
        return this.OneProtocol.http(config, messages);
    }

    moveAttributeGroup(inventoryType: string, groupId: string, payload: { previousGroupId: string }): ng.IPromise<IProtocolResponse<void>> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("PUT", `/api/inventory/v2/screens/inventory-detail/groups/${groupId}/move?schemaName=${inventoryType}`, null, payload);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("ErrorMovingAttributeGroup") } };
        return this.OneProtocol.http(config, messages);
    }

    addRecord(schemaId: string, payload: IInventoryRecordV2): ng.IPromise<IProtocolResponse<{ data: IInventoryRecordV2 }>> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("POST", `/api/inventory/v2/inventories/${schemaId}`, null, payload);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("ErrorAddingRecord") }, Success: { custom: this.translatePipe.transform("AddedRecordSuccessfully") } };
        return this.OneProtocol.http(config, messages);
    }

    getListData(listType: string): ng.IPromise<IProtocolResponse<{data: IInventoryListItem[] | IInventoryElementListItem[]}>> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("GET", `/api/inventory/v2/${listType}`);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("ErrorRetrievingList") } };
        return this.OneProtocol.http(config, messages);
    }

    getListItem(listType: string, itemId: string): ng.IPromise<IProtocolResponse<{data: IInventoryListItem | IInventoryElementListItem}>> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("GET", `/api/inventory/v2/${listType}/${itemId}`);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("ErrorRetrievingItem") } };
        return this.OneProtocol.http(config, messages);
    }

    getAssociatedListItems(listType: string, itemId: string, associatedListType: string): ng.IPromise<IProtocolResponse<{data: IInventoryListItem[] | IInventoryElementListItem[]}>> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("GET", `/api/inventory/v2/${listType}/${itemId}/${associatedListType}`);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("ErrorRetrievingList") } };
        return this.OneProtocol.http(config, messages);
    }

    addListData(listType: string, payload: { value: string, description?: string }): ng.IPromise<IProtocolResponse<{ data: IInventoryListItem | IInventoryElementListItem }>> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("POST", `/api/inventory/v2/${listType}`, null, payload);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("ErrorAddingRecord") }, Success: { custom: this.translatePipe.transform("AddedRecordSuccessfully") } };
        return this.OneProtocol.http(config, messages);
    }

    deleteListItem(listType: string, itemId: string): Observable<IProtocolResponse<null>> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("DELETE", `/api/inventory/v2/${listType}/${itemId}`);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("ErrorDeletingRecord") } };
        return from(this.OneProtocol.http(config, messages));
    }

    updateListData(listType: string, itemId: string, payload: { value: string, description?: string }): ng.IPromise<IProtocolResponse<{ data: IInventoryListItem | IInventoryElementListItem }>> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("PUT", `/api/inventory/v2/${listType}/${itemId}`, null, payload);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("ErrorUpdatingRecord") }, Success: { custom: this.translatePipe.transform("UpdatedRecordSuccessfully") } };
        return this.OneProtocol.http(config, messages);
    }

    addAssociatedListItems(listType: string, itemId: string, associatedType: string, payload: Array<IStringMap<string>>): ng.IPromise<IProtocolResponse<{ data: IInventoryElementListItem[] }>> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("POST", `/api/inventory/v2/${listType}/${itemId}/${associatedType}`, null, payload);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("ErrorUpdatingRecord") } };
        return this.OneProtocol.http(config, messages);
    }

    removeAssociatedListItems(listType: string, itemId: string, associatedType: string, payload: Array<IStringMap<string>>): ng.IPromise<IProtocolResponse<{ data: IInventoryElementListItem[] }>> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("DELETE", `/api/inventory/v2/${listType}/${itemId}/${associatedType}`, null, payload);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("ErrorUpdatingRecord") } };
        return this.OneProtocol.http(config, messages);
    }

    getListDetails(listType: string): ng.IPromise<IProtocolResponse<{ data: IInventoryListItem[] }>> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("GET", `/api/inventory/v2/${listType}/details`);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("ErrorRetrievingList") } };
        return this.OneProtocol.http(config, messages);
    }

    getInventorySettings(): ng.IPromise<IProtocolResponse<IInventorySettingList>> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("GET", `/api/inventory/v2/settings`);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("ErrorGettingInventorySettings") } };
        return this.OneProtocol.http(config, messages);
    }

    updateInventorySettings(settings: IInventorySetting[]): ng.IPromise<IProtocolResponse<void>> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("PUT", `/api/inventory/v2/settings`, null, settings);
        const messages: IProtocolMessages = {
            Error: { custom: this.translatePipe.transform("ErrorUpdatingInventorySettings") },
            Success: { custom: this.translatePipe.transform("SettingsSavedSuccessfully") },
        };
        return this.OneProtocol.http(config, messages);
    }

    changeInventoryStatus(recordId: string, inventoryId: string, seleectedStatus: IInventoryStatus): ng.IPromise<IProtocolResponse<void>> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("PUT", `/api/inventory/v2/inventories/${inventoryId}/${recordId}/status`, null, seleectedStatus);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("ErrorChangingInventoryStatus") } };
        return this.OneProtocol.http(config, messages);
    }

    bulkChangeInventoryStatus(recordIds: string[], inventoryId: string, seleectedStatus: IInventoryStatus): ng.IPromise<IProtocolResponse<void>> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("POST", `/api/inventory/v2/inventories/${inventoryId}/status?ids=${recordIds.join(",")}`, null, seleectedStatus);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("ErrorChangingInventoryStatus") } };
        return this.OneProtocol.http(config, messages);
    }

    getInventoryStatusOptions(schemaName: string): ng.IPromise<IProtocolResponse<IInventoryStatusOption[]>> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("GET", `/api/inventory/v2/inventories/${schemaName}/statuses`);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("ErrorRetrievingInventoryStatusOptions") } };
        return this.OneProtocol.http(config, messages);
    }

    getElementVisibilityConfig(orgId: string): Observable<IProtocolResponse<{ data: IInventoryManageVisibilityState }>> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("GET", `/api/inventory/v2/filters/data-elements/organizations/${orgId}`);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("CouldNotRetrieveSetting") } };
        return from(this.OneProtocol.http(config, messages));
    }

    getInheritedElementVisibilityConfig(orgId: string): Observable<IProtocolResponse<{ data: IInventoryManageVisibilityState }>> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("GET", `/api/inventory/v2/filters/data-elements/organizations/${orgId}/inheritedConfig`);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("CouldNotRetrieveSetting") } };
        return from(this.OneProtocol.http(config, messages));
    }

    saveElementVisibilityConfig(orgId: string, payload: IInventoryManageElementVisibilityPayload): Observable<IProtocolResponse<null>> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("PUT", `/api/inventory/v2/filters/data-elements/organizations/${orgId}`, null, payload);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("CouldNotUpdateSettings") } };
        return from(this.OneProtocol.http(config, messages));
    }

    removeElementVisibilityConfig(orgId: string): Observable<IProtocolResponse<null>> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("DELETE", `/api/inventory/v2/filters/data-elements/organizations/${orgId}`);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("CouldNotUpdateSettings") } };
        return from(this.OneProtocol.http(config, messages));
    }

    private getInventoryTypeSingular(id: string): string {
        return InventoryTableNames.Singular[id] || InventoryTableNames.Default;
    }
}
