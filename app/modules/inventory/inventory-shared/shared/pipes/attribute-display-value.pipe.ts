import {
    Pipe,
    PipeTransform,
} from "@angular/core";

// Services
import { InventoryAdapterV2Service } from "inventoryModule/services/adapter/inventory-adapter-v2.service";

// Interfaces
import {
    IAttributeDetailV2,
    IAttributeOptionV2,
} from "interfaces/inventory.interface";

@Pipe({
    name: "attributeDisplayValue",
})

export class AttributeDisplayValuePipe implements PipeTransform {

    constructor(private inventoryAdapterV2Service: InventoryAdapterV2Service) {}

    transform(value: string | Date | IAttributeOptionV2 | IAttributeOptionV2[], attribute: IAttributeDetailV2): string {
        return this.inventoryAdapterV2Service.getFieldDisplayValue(value, attribute);
    }
}
