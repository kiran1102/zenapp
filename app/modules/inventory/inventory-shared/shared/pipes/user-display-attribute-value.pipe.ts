import {
    Pipe,
    PipeTransform,
} from "@angular/core";

// Services
import { InventoryUserLogicService } from "inventoryModule/services/business-logic/inventory-user-logic.service";

// Interfaces
import {
    IAttributeDetailV2,
    IAttributeOptionV2,
} from "interfaces/inventory.interface";

@Pipe({
    name: "userDisplayAttributeValue",
})

export class UserDisplayAttributeValuePipe implements PipeTransform {

    constructor(private inventoryUserLogicService: InventoryUserLogicService) {}

    transform(attributeValue: IAttributeOptionV2 | IAttributeOptionV2[], attribute: IAttributeDetailV2): string {
        const record = { [attribute.fieldName]: attributeValue };
        return this.inventoryUserLogicService.getUserModelAsString(attribute, record);
    }
}
