// Core
import {
    Component,
    Input,
    Output,
    EventEmitter,
    SimpleChanges,
    OnInit,
    OnChanges,
} from "@angular/core";
import { StateService } from "@uirouter/core";

// 3rd Party
import {
    includes,
    values,
} from "lodash";

// service
import { InventoryRelatedService } from "modules/inventory/services/adapter/inventory-related.service";
import { InventoryService } from "inventorySharedModule/shared/services/inventory-api.service";
import { LookupService } from "sharedModules/services/helper/lookup.service.ts";

// Interfaces
import { IStringMap } from "interfaces/generic.interface";
import { IInventoryRecord } from "interfaces/inventory.interface";
import { IRelatedListOption } from "interfaces/inventory.interface";
import { IRelatedListOptionData } from "modules/vendor/shared/interfaces/vendor-upload-document.interface";

// Const and Enums
import { InventoryTranslationKeys } from "constants/inventory.constant";

// Pipe
import { TranslatePipe } from "modules/pipes/translate.pipe";

@Component({
    selector: "inventory-lookup",
    templateUrl: "./inventory-lookup.component.html",
})
export class InventoryLookupComponent implements OnInit, OnChanges {

    @Input() placeholder: string;
    @Input() isMultiselect: boolean;
    @Input() inventoryType: number;
    @Input() model: IRelatedListOption[];
    @Output() onSelectTrigger = new EventEmitter<IRelatedListOptionData>();

    recordId: string;
    orgGroupId: string;
    inputValue = "";
    relatedAttributes: IStringMap<boolean> = {};
    isLoadingInventory = true;
    requiredFieldsPopulated = false;
    selectedOption: IRelatedListOption = null;
    selectedOptions: IRelatedListOption[] = [];
    filteredInventoryList: IRelatedListOption[] = [];
    inventoryList: IRelatedListOption[] = [];

    private record: IInventoryRecord;
    private recordInventoryType: number;

    constructor(
        private inventoryRelatedService: InventoryRelatedService,
        private inventoryService: InventoryService,
        private stateService: StateService,
        private translatePipe: TranslatePipe,
        private lookupService: LookupService,
    ) { }

    ngOnInit(): void {
        this.recordId = this.stateService.params.recordId;
        this.orgGroupId = this.inventoryService.getOrgGroupId(this.record, this.recordInventoryType);
        this.placeholder = this.placeholder || this.translatePipe.transform(InventoryTranslationKeys[this.inventoryType].chooseInventoryItem) || "";
        this.getInventoryList();
    }

    ngOnChanges(changes: SimpleChanges) {
        if (changes.model) {
            if (this.isMultiselect && changes.model.currentValue && changes.model.currentValue.length > 0) {
                this.selectedOptions = changes.model.currentValue;
                this.selectedOption = changes.model.currentValue;
            } else {
                this.selectedOption = changes.model.currentValue;
            }
            this.model = changes.model.currentValue;
        }
    }

    onSelect(event: any): void {
        if (!event.currentValue) {
            this.inputValue = "";
        }
        this.selectedOption = event.currentValue;
        this.searchItems({ value: this.inputValue });
        this.onSelectTrigger.emit({
            options: !event.currentValue ? null : Array.isArray(event.currentValue) ? event.currentValue : [event.currentValue],
            type: this.inventoryType,
        });
        this.hasFieldsPopulated();
    }

    hasFieldsPopulated(): void {
        this.requiredFieldsPopulated = (Boolean(this.selectedOption) && this.attributeSelected()) || Boolean(this.selectedOptions.length);
    }

    attributeSelected(): boolean {
        return includes(values(this.relatedAttributes), true);
    }

    searchItems({ value }) {
        this.filteredInventoryList = this.removeSelectionFromOptions(this.inventoryList, this.selectedOption);
        if (!value) return;
        this.filteredInventoryList = this.lookupService.filterOptionsByInput([], this.inventoryList, value, "id", "name");
        this.inputValue = value;
    }

    resetInputValue() {
        if (!this.isMultiselect) {
            if (!this.selectedOption && (this.filteredInventoryList && this.filteredInventoryList.length === 0))
                this.filteredInventoryList = this.lookupService.filterOptionsByInput([], this.inventoryList, null, "id", "name");
        }
    }

    defaultDropDown(event: MouseEvent) {
        if (!this.isMultiselect) {
            if (!this.filteredInventoryList) this.filteredInventoryList = [...this.inventoryList];
            if (!this.selectedOption && !event.srcElement["value"]) this.filteredInventoryList = [...this.inventoryList];
            this.inputValue = "";
        }
    }

    private getInventoryList(): void {
        this.inventoryRelatedService.getRelatedInventoryList(this.inventoryType, this.orgGroupId).then((response: IRelatedListOption[]): void => {
            if (response) {
                this.isLoadingInventory = false;
                this.inventoryList = this.formatInventoryList(response).filter((option: IRelatedListOption): boolean => this.recordId !== option.id);
                this.filteredInventoryList = [...this.inventoryList];
            }
        });
    }

    private formatInventoryList(options: IRelatedListOption[]): IRelatedListOption[] {
        return options.map((option: IRelatedListOption): IRelatedListOption => {
            if (option.location) {
                option.label = `${option.name} - ${option.organization.value} - ${option.location.value}`;
            } else if (option.type) {
                option.label = `${option.name} - ${option.organization.value} - ${option.type.value}`;
            } else {
                option.label = `${option.name} - ${option.organization.value}`;
            }
            return option;
        });
    }

    private removeSelectionFromOptions(options: IRelatedListOption[], selection: IRelatedListOption): IRelatedListOption[] {
        if (!selection) return options;
        return this.lookupService.filterOptionsBySelections([selection], options);
    }
}
