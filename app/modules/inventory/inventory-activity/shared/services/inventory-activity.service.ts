import { Injectable } from "@angular/core";

// Rxjs
import {
    BehaviorSubject,
    Observable,
} from "rxjs";

// Services
import { InventoryService } from "inventorySharedModule/shared/services/inventory-api.service";

// Interfaces
import { IProtocolResponse } from "interfaces/protocol.interface";
import { IPageableInventory } from "interfaces/inventory.interface";
import { IInventoryActivityState } from "inventoryModule/inventory-activity/shared/interfaces/inventory-activity.interface";

@Injectable()
export class InventoryActivityService {

    readonly inventoryActivityState$: Observable<IInventoryActivityState>;
    private defaultState: IInventoryActivityState = {
        activityList: [],
        activityListMeta: {
            recordId: "",
            content: [],
            first: true,
            last: true,
            number: 0,
            numberOfElements: 0,
            size: 0,
            totalElements: 0,
            totalPages: 0,
        },
        loadingActivityData: false,
        loadingMoreActivities: false,
    };
    private _inventoryActivityState: BehaviorSubject<IInventoryActivityState> = new BehaviorSubject(JSON.parse(JSON.stringify(this.defaultState)));

    constructor(private inventoryApiService: InventoryService) {
        this.inventoryActivityState$ = this._inventoryActivityState.asObservable();
    }

    getRecordActivity(recordId: string, page: number = 0, size: number = 8, dispatchFetch: boolean = true) {
        if (dispatchFetch) {
            this.setLoadingState(true);
        } else {
            this.setLoadingMoreState(true);
        }
        this.inventoryApiService.getActivityData(recordId, page, size).then((response: IProtocolResponse<IPageableInventory>) => {
            if (response.result) {
                const currentList = this._inventoryActivityState.getValue().activityList;
                const newActivityList = response.data.content;
                const activityListMeta = response.data;
                activityListMeta.recordId = recordId;
                delete activityListMeta.content;
                this.updateState({
                    activityList: [...currentList, ...newActivityList],
                    activityListMeta,
                });
            }
            if (dispatchFetch) {
                this.setLoadingState(false);
            } else {
                this.setLoadingMoreState(false);
            }
        });
    }

    setDefaultActivity() {
        this.updateState(JSON.parse(JSON.stringify(this.defaultState)));
    }

    updateState(state: {}) {
        const currentState = this._inventoryActivityState.getValue();
        this._inventoryActivityState.next({
            ...currentState,
            ...state,
        });
    }

    setLoadingState(loading: boolean) {
        const currentState = this._inventoryActivityState.getValue();
        this._inventoryActivityState.next({
            ...currentState,
            loadingActivityData: loading,
        });
    }

    setLoadingMoreState(loading: boolean) {
        const currentState = this._inventoryActivityState.getValue();
        this._inventoryActivityState.next({
            ...currentState,
            loadingMoreActivities: loading,
        });
    }

}
