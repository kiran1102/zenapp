export enum ActivityTypes {
    CONTROL_ADDED = "Controls Added",
    CONTROL_REMOVED = "Controls Removed",
    INVALID_VENDOR = "Invalid Vendor Asset Association",
}

export enum ActivityAction {
    ADDITION = "Added",
    REMOVAL = "Removed",
}

export enum PersonalDataType {
    "Data Element" = "DataElement",
    "Data Subject Type" = "DataSubjectType",
    "Data Category" = "DataCategory",
}
