import { IPageableInventory } from "interfaces/inventory.interface";
import { IAuditHistoryItem } from "interfaces/audit-history.interface";

export interface IInventoryActivityState {
    activityList: IAuditHistoryItem[];
    activityListMeta: IPageableInventory;
    loadingActivityData: boolean;
    loadingMoreActivities: boolean;
}
