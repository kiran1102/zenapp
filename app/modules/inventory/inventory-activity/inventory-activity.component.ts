// 3rd party
import {
    Component,
    Input,
    OnInit,
    OnDestroy,
} from "@angular/core";

// RxJs
import { Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";

// Services
import { TimeStamp } from "oneServices/time-stamp.service";
import { InventoryActivityService } from "inventoryModule/inventory-activity/shared/services/inventory-activity.service";

// Enums/Constants
import {
    ActivityTypes,
    ActivityAction,
    PersonalDataType,
} from "inventoryModule/inventory-activity/shared/enums/inventory-activity.enums";
import { ActivityIcon, ActivityColor } from "constants/activity-stream.constant";
import { Color } from "constants/color.constant";

// Interfaces
import { IStringMap } from "interfaces/generic.interface";
import {
    IAuditHistoryItem,
    IFormattedActivity,
    IAuditChange,
    IAuditHistoryUser,
} from "interfaces/audit-history.interface";
import { IPageableInventory } from "interfaces/inventory.interface";
import { IInventoryActivityState } from "inventoryModule/inventory-activity/shared/interfaces/inventory-activity.interface";

// Tokens
import { StoreToken } from "tokens/redux-store.token";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

@Component({
    selector: "inventory-activity",
    templateUrl: "./inventory-activity.component.html",
})
export class InventoryActivityComponent implements OnInit, OnDestroy {

    @Input() recordId: string;

    expandedItemMap: IStringMap<boolean> = {};
    isLoadingActivity = true;
    allItemsExpanded = true;
    activitiesAscending = true;
    hasMoreActivities: boolean;
    orderIcon = "ot ot-sort-amount-asc";
    formattedActivities: IFormattedActivity[] = [];
    isLoadingMoreActivities = false;
    activityAction = ActivityAction;
    personalDataType = PersonalDataType;
    private activityList: IAuditHistoryItem[];
    private activityListMetaInfo: IPageableInventory;
    private destroy$: Subject<void> = new Subject();
    private defaultConfig = {
        increment: 8,
        iconColor: "#FFFFFF",
        iconBackgroundColor: "#1F96DB",
    };

    constructor(
        public inventoryActivityService: InventoryActivityService,
        private timeStampService: TimeStamp,
        private translatePipe: TranslatePipe,
    ) { }

    ngOnInit() {
        this.inventoryActivityService.setDefaultActivity();
        this.inventoryActivityService.getRecordActivity(this.recordId);
        this.inventoryActivityService.inventoryActivityState$
            .pipe(takeUntil(this.destroy$))
            .subscribe((stateData: IInventoryActivityState) => {
                this.isLoadingActivity = stateData.loadingActivityData;
                this.isLoadingMoreActivities = stateData.loadingMoreActivities;
                this.activityList = stateData.activityList;
                if (this.activityList && this.activityList.length) {
                    this.formatData();
                    this.setExpandedItemMap();
                }
                this.activityListMetaInfo = stateData.activityListMeta;
                this.hasMoreActivities = this.activityListMetaInfo ? !this.activityListMetaInfo.last : false;
                if (!this.activitiesAscending) {
                    this.reverseActivityOrder();
                }
            });
    }

    ngOnDestroy() {
        this.destroy$.next();
        this.destroy$.complete();
    }

    formatData() {
        this.formattedActivities = [];
        this.activityList.forEach((activity: IAuditHistoryItem, index: number) => {
            if (activity) {
                activity.userName = this.formatUserName(activity.userDetail);

                const formattedActivity: IFormattedActivity = {
                    id: index.toString(),
                    title: this.translatePipe.transform("UserMadeChanges", { userName: activity.userName }),
                    fieldLabel: this.translatePipe.transform("FieldName"),
                    description: activity.description,
                    timeStamp: this.timeStampService.formatDate(activity.changeDateTime),
                    icon: "fa fa-check-square",
                    iconColor: this.defaultConfig.iconColor,
                    iconBackgroundColor: this.defaultConfig.iconBackgroundColor,
                    changes: activity.changes,
                    associations: activity.associations,
                    showOldNewValueField: true,
                };

                switch (activity.description) {
                    case ActivityTypes.CONTROL_ADDED:
                        formattedActivity.title = this.translatePipe.transform("UserAddedControl", { userName: activity.userName });
                        formattedActivity.icon = ActivityIcon.Created;
                        formattedActivity.iconColor = Color.White;
                        formattedActivity.iconBackgroundColor = ActivityColor.Created;
                        formattedActivity.fieldLabel = this.translatePipe.transform("ControlName");
                        formattedActivity.showOldNewValueField = false;
                        break;
                    case ActivityTypes.CONTROL_REMOVED:
                        formattedActivity.title = this.translatePipe.transform("UserRemovedControl", { userName: activity.userName });
                        formattedActivity.icon = ActivityIcon.Delete;
                        formattedActivity.iconBackgroundColor = ActivityColor.Delete;
                        formattedActivity.fieldLabel = this.translatePipe.transform("ControlName");
                        formattedActivity.showOldNewValueField = false;
                        break;
                    case ActivityTypes.INVALID_VENDOR:
                        formattedActivity.title = this.translatePipe.transform("AssetRelationshipFailed");
                        formattedActivity.icon = ActivityIcon.Failed;
                        formattedActivity.iconBackgroundColor = ActivityColor.Delete;
                        formattedActivity.showOldNewValueField = false;
                        const descriptionParams = { relationshipType: "", vendor: "Vendor" };
                        if (activity.changes && activity.changes.length > 0) {
                            activity.changes.forEach((changeItem: IAuditChange) => {
                                switch (changeItem.fieldName) {
                                    case "asset":
                                        formattedActivity.fieldLabel = this.translatePipe.transform("RelationshipTypeAsset", { asset: changeItem.newValue.name });
                                        break;
                                    case "relationshipType":
                                        descriptionParams.relationshipType = changeItem.newValue.name;
                                        break;
                                    case "toInventoryType":
                                        descriptionParams.vendor = changeItem.newValue.name;
                                        break;
                                }
                            });
                        }
                        formattedActivity.changes = [{
                            fieldLabel: null,
                            fieldName: this.translatePipe.transform("RelationshipTypeDesciption", descriptionParams),
                            newValue: null,
                            oldValue: null,
                            description: null,
                        }];
                        break;
                }

                this.updateAuditChangeMessage(formattedActivity);
                this.formattedActivities.push(formattedActivity);
            }
        });
    }

    addActivitiesToView() {
        if (this.hasMoreActivities) {
            this.isLoadingMoreActivities = true;
            this.inventoryActivityService.getRecordActivity(
                this.activityListMetaInfo.recordId,
                this.activityListMetaInfo.number + 1,
                this.defaultConfig.increment, false,
            );
        }
    }

    toggleActivity(itemId: string) {
        this.expandedItemMap[itemId] = !this.expandedItemMap[itemId];
        this.setAllItemsExpanded();
    }

    setExpandedItemMap() {
        this.formattedActivities.forEach((activity: IFormattedActivity) => {
            if (this.expandedItemMap[activity.id] === undefined) {
                this.expandedItemMap[activity.id] = true;
            }
        });
        this.setAllItemsExpanded();
    }

    setAllItemsExpanded() {
        this.allItemsExpanded = this.checkIfAllExpanded();
    }

    expandAll() {
        Object.keys(this.expandedItemMap).forEach((key) => this.expandedItemMap[key] = true);
        this.setAllItemsExpanded();
    }

    collapseAll() {
        Object.keys(this.expandedItemMap).forEach((key) => this.expandedItemMap[key] = false);
        this.setAllItemsExpanded();
    }

    toggleExpand() {
        if (this.allItemsExpanded) {
            this.collapseAll();
        } else {
            this.expandAll();
        }
    }

    toggleOrder() {
        this.activitiesAscending = !this.activitiesAscending;
        this.orderIcon = this.activitiesAscending ? "ot ot-sort-amount-asc" : "ot ot-sort-amount-desc";
        this.reverseActivityOrder();
    }

    reverseActivityOrder() {
        this.formattedActivities = this.formattedActivities.reverse();
    }

    private formatUserName(userDetail: IAuditHistoryUser): string {
        let userName = "";
        if (userDetail && (userDetail.fullName || userDetail.email)) {
            userName = userDetail.fullName || userDetail.email;
            if (userDetail.disabled === true) {
                userName = `${userName} (${this.translatePipe.transform("Disabled")})`;
            } else if (userDetail.internal === false) {
                userName = `${userName} (${this.translatePipe.transform("External")})`;
            }
        } else {
            userName = this.translatePipe.transform("UserNotFound");
        }
        return userName;

    }

    private checkIfAllExpanded(): boolean {
        return !Object.values(this.expandedItemMap).some((value: boolean) => !value);
    }

    private updateAuditChangeMessage(formattedActivity: IFormattedActivity) {
        formattedActivity.changes = formattedActivity.changes.map((change: IAuditChange) => {
            change.fieldLabel = formattedActivity.fieldLabel;
            switch (formattedActivity.description) {
                case ActivityTypes.CONTROL_ADDED:
                    change.fieldName = this.getFieldName(change);
                    break;
                case ActivityTypes.CONTROL_REMOVED:
                    change.fieldName = this.getFieldName(change);
                    break;
                default:
            }
            return change;
        });
    }

    private getFieldName(change: IAuditChange): string {
        let fieldName = "";
        if (change.newValue && change.newValue.name) {
            fieldName = `${change.newValue.associatedObjectId} - ${change.newValue.name}`;
        }
        if (change.oldValue && change.oldValue.name) {
            fieldName = `${change.oldValue.associatedObjectId} - ${change.oldValue.name}`;
        }

        return fieldName;
    }

}
