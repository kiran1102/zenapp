// Angular
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { HttpClientModule } from "@angular/common/http";

// Modules
import {
    OtActivityTimelineModule,
    OtLoadingModule,
    OtButtonModule,
} from "@onetrust/vitreus";
import { TranslateModule } from "@ngx-translate/core";

// Components
import { InventoryActivityComponent } from "inventoryModule/inventory-activity/inventory-activity.component";

// Services
import { InventoryActivityService } from "inventoryModule/inventory-activity/shared/services/inventory-activity.service";

@NgModule({
    imports: [
        CommonModule,
        HttpClientModule,
        OtActivityTimelineModule,
        OtLoadingModule,
        OtButtonModule,
        TranslateModule,
    ],
    exports: [
        InventoryActivityComponent,
    ],
    declarations: [
        InventoryActivityComponent,
    ],
    providers: [
        InventoryActivityService,
    ],
})
export class InventoryActivityModule {}
