// Rxjs
import { Subject } from "rxjs";
import {
    startWith,
    takeUntil,
} from "rxjs/operators";

// 3rd party
import {
    Component,
    Inject,
    Input,
    Output,
    SimpleChanges,
    OnChanges,
    OnInit,
    OnDestroy,
    EventEmitter,
} from "@angular/core";

// Redux
import { StoreToken } from "tokens/redux-store.token";
import {
    getSchema,
    getIsLoadingRecord,
    getHasErrors,
    getHasChanges,
    getHasRequiredValues,
    getAttributeMap,
} from "oneRedux/reducers/inventory-record.reducer";
import observableFromStore from "oneRedux/utils/observable-from-store";

// Services
import { RecordActionService } from "inventoryModule/services/action/record-action.service";
import { InventoryService } from "inventorySharedModule/shared/services/inventory-api.service";

// Const
import { EmptyGuid } from "constants/empty-guid.constant";
import { AttributeV2AllowedContexts } from "constants/inventory.constant";
import { InventorySchemaIds } from "constants/inventory-config.constant";

// Enums
import { InventoryContexts } from "enums/inventory.enum";

// Interfaces
import { IProtocolResponse } from "interfaces/protocol.interface";
import { IFormattedAttribute } from "interfaces/inventory.interface";
import { IAttributeGroup } from "interfaces/attributes.interface";
import { IStringMap } from "interfaces/generic.interface";
import {
    IStore,
    IStoreState,
} from "interfaces/redux.interface";

@Component({
    selector: "inventory-form",
    templateUrl: "./inventory-form.component.html",
})
export class InventoryFormComponent implements OnChanges, OnInit, OnDestroy {

    @Input() isEditable: boolean;
    @Input() isEditing: boolean;
    @Input() recordId: string;
    @Input() inventoryId: string;
    @Input() wrapperClass: string;
    @Input() scrollParentClass: string;
    @Input() loadDataFromStore: boolean;
    @Input() context: InventoryContexts;
    @Output() viewRisk: EventEmitter<null> = new EventEmitter<null>();

    schema: IFormattedAttribute[] = [];
    attributeMap: IStringMap<IFormattedAttribute>;
    isLoadingRecord: boolean;
    isLoadingGroups = false;
    canSubmit = false;
    groupsData: IAttributeGroup[];
    groupsOpenMap: IStringMap<boolean>;
    destroy$ = new Subject();
    onlyUngroupedAttributes = true;

    constructor(
        @Inject(StoreToken) private store: IStore,
        readonly recordActionService: RecordActionService,
        private inventoryService: InventoryService,
    ) { }

    ngOnInit() {
        observableFromStore(this.store).pipe(
            startWith(this.store.getState()),
            takeUntil(this.destroy$),
        ).subscribe((state: IStoreState) => this.componentWillReceiveState(state));
        if (!this.loadDataFromStore) {
            this.setRecordAndSchemaId();
        }
        if (this.context !== InventoryContexts.Add) {
            this.getGroupsData();
        }
    }

    ngOnDestroy() {
        this.destroy$.next();
        this.destroy$.complete();
    }

    ngOnChanges(changes: SimpleChanges) {
        if (changes.isEditing) {
            this.recordActionService.toggleEditingRecord(changes.isEditing.currentValue);
        }
        if (changes.inventoryId && this.context !== InventoryContexts.Add) {
            this.getGroupsData();
        }
        if (changes.recordId && !changes.recordId.firstChange && !this.loadDataFromStore) {
            this.setRecordAndSchemaId();
        }
    }

    setRecordAndSchemaId() {
        this.recordActionService.setRecordAndSchemaById(this.inventoryId, this.recordId, this.isEditing).then((result: boolean) => {
            if (!result) return;
            this.schema = this.filterAttributesByContext(getSchema(this.store.getState()));
            this.attributeMap = getAttributeMap(this.store.getState());
        });
    }

    checkGroupAttributes(groupsData: IAttributeGroup[]): boolean {
        let onlyUngroupedAttributes = true;
        for (let i = 0; i < groupsData.length; i++) {
            if (groupsData[i].id !== EmptyGuid && groupsData[i].attributes.length) {
                onlyUngroupedAttributes = false;
                break;
            }
        }
        return onlyUngroupedAttributes;
    }

    getGroupsData() {
        if (this.inventoryId) {
            this.isLoadingGroups = true;
            this.inventoryService.getAttributeGroups(this.inventoryId).then((response: IProtocolResponse<IAttributeGroup[]>) => {
                if (response.result && response.data) {
                    this.groupsData = response.data;
                    this.onlyUngroupedAttributes = this.checkGroupAttributes(response.data);
                    this.groupsOpenMap = this.setGroupMap(response.data);
                }
                this.isLoadingGroups = false;
            });
        }
    }

    setGroupMap(groupsData: IAttributeGroup[]): IStringMap<boolean> {
        const openMap: IStringMap<boolean> = {};
        groupsData.forEach((group: IAttributeGroup) => {
            openMap[group.id] = true;
        });
        return openMap;
    }

    idTrackBy(index: number, item: IFormattedAttribute | IAttributeGroup): string {
        return item.id;
    }

    indexTrackBy(index: number): number {
        return index;
    }

    riskClick() {
        this.viewRisk.emit();
    }

    private filterAttributesByContext(attributes: IFormattedAttribute[]): IFormattedAttribute[] {
        if (!this.context) return attributes;
        return attributes.filter((attribute: IFormattedAttribute): boolean => {
            return AttributeV2AllowedContexts[this.context][this.inventoryId][attribute.fieldName];
        });
    }

    private componentWillReceiveState(state) {
        this.isLoadingRecord = getIsLoadingRecord(state);
        this.canSubmit = !getHasErrors(state) && getHasChanges(state) && getHasRequiredValues(state);
        if (this.loadDataFromStore) {
            this.schema = this.filterAttributesByContext(getSchema(state));
            this.attributeMap = getAttributeMap(state);
        }
    }
}
