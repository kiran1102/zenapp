// 3rd party
import {
    Component,
    Input,
    Output,
    Inject,
    ElementRef,
    OnInit,
    OnDestroy,
    EventEmitter,
} from "@angular/core";

import Utilities from "Utilities";

// Rxjs
import { Subject } from "rxjs";
import {
    startWith,
    takeUntil,
} from "rxjs/operators";

// Services
import { InventoryAdapterV2Service } from "inventoryModule/services/adapter/inventory-adapter-v2.service";
import { InventoryUserLogicService } from "inventoryModule/services/business-logic/inventory-user-logic.service";
import { RecordActionService } from "inventoryModule/services/action/record-action.service";
import { TimeStamp } from "oneServices/time-stamp.service";
import { LookupService } from "sharedModules/services/helper/lookup.service";
import { UserActionService } from "oneServices/actions/user-action.service";

// Redux
import { StoreToken } from "tokens/redux-store.token";
import observableFromStore from "oneRedux/utils/observable-from-store";
import { getOrgNameById } from "oneRedux/reducers/org.reducer";
import {
    getRecord,
    getRecordChanges,
    getIsLoadingRecord,
    getIsEditingRecord,
} from "oneRedux/reducers/inventory-record.reducer";
import { getUserById } from "oneRedux/reducers/user.reducer";

// Interfaces
import {
    IAttributeDetailV2,
    IInventoryRecordV2,
    IAttributeOptionV2,
    IFormattedAttribute,
    IFormattedAttributeOption,
} from "interfaces/inventory.interface";
import {
    IStore,
    IStoreState,
} from "interfaces/redux.interface";
import {
    IStringMap,
    IKeyValue,
    ISelection,
} from "interfaces/generic.interface";
import { IMyDateModel } from "mydatepicker";
import { IOtOrgUserOutput } from "sharedModules/components/ot-org-user/ot-org-user.interface";

// Constants / Enums
import {
    AttributeTypes,
    AttributeResponseTypes,
    AttributeFieldNames,
} from "constants/inventory.constant";
import { InventoryFieldNames } from "enums/inventory.enum";
import { OrgUserDropdownTypes, OrgUserTraversal } from "enums/org-user-dropdown.enum";
import { Regex } from "constants/regex.constant";

@Component({
    selector: "inventory-form-cell",
    templateUrl: "./inventory-form-cell.component.html",
})
export class InventoryFormCellComponent implements OnInit, OnDestroy {

    @Input() isEditable: boolean;
    @Input() attribute: IAttributeDetailV2;
    @Input() scrollParentClass: string;
    @Input() tooltipPosition = "top-left";
    @Output() viewRisk: EventEmitter<null> = new EventEmitter<null>();

    attributeTypes = AttributeTypes;
    responseTypes = AttributeResponseTypes;
    orgUserDropdownTypes = OrgUserDropdownTypes;
    orgUserTraversal = OrgUserTraversal;
    scrollParent: Element;
    editMode = false;
    record: IInventoryRecordV2;
    isLoading: boolean;
    timeFormat: string;
    otherMap: IStringMap<string> = {};
    destroy$ = new Subject();

    constructor(
        @Inject(StoreToken) private store: IStore,
        readonly inventoryAdapterV2Service: InventoryAdapterV2Service,
        readonly recordActionService: RecordActionService,
        readonly inventoryUserLogicService: InventoryUserLogicService,
        readonly timeStamp: TimeStamp,
        private element: ElementRef,
        private lookupService: LookupService,
        private userActionService: UserActionService,
    ) { }

    ngOnInit() {
        const scrollEl: HTMLCollectionOf<Element> = this.scrollParentClass ? document.getElementsByClassName(this.scrollParentClass) : null;
        this.scrollParent = scrollEl ? scrollEl[0] : null;
        this.timeFormat = this.timeStamp.getDatePickerDateFormat();
        observableFromStore(this.store).pipe(
            startWith(this.store.getState()),
            takeUntil(this.destroy$),
        ).subscribe((state: IStoreState) => this.componentWillReceiveState(state));
        if (this.attribute && this.attribute.filteredValues && !this.record[this.attribute.fieldName]) delete this.attribute.filteredValues;
    }

    ngOnDestroy() {
        this.destroy$.next();
        this.destroy$.complete();
    }

    textChange(value: string, attribute: IFormattedAttribute) {
        this.recordActionService.changeRecord(attribute, value);
    }

    getSingleSelectModel(attribute: IAttributeDetailV2): IAttributeOptionV2 {
        if (!this.record[attribute.fieldName]) return null;
        if (attribute.required && (attribute.fieldName === AttributeFieldNames.Type || attribute.attributeType === AttributeTypes.Locations)) {
            return 0 !== (this.record[attribute.fieldName] as IAttributeOptionV2[]).length ? this.record[attribute.fieldName] as IAttributeOptionV2 : null;
        }
        return this.record[attribute.fieldName][0];
    }

    getDateModel(attribute: IFormattedAttribute): string {
        if (!this.record[attribute.fieldName]) return "";
        return this.timeStamp.formatDateWithoutTimezoneForDatepicker(this.record[attribute.fieldName] as string);
    }

    dateChange(selection: IMyDateModel, attribute: IFormattedAttribute) {
        const formattedDate: string = selection && selection.date && selection.date.year ? this.timeStamp.formatDateToIso(selection.date) : "";
        this.recordActionService.changeRecord(attribute, formattedDate);
    }

    singleSelectChange({ currentValue }: ISelection<IFormattedAttributeOption>, attribute: IFormattedAttribute) {
        const newValue: IFormattedAttributeOption[] | IFormattedAttributeOption = currentValue ? (attribute.required ? currentValue : [currentValue]) : [];
        this.recordActionService.changeRecord(attribute, newValue);
        attribute.filteredValues = this.lookupService.filterOptionsBySelections([currentValue], attribute.values, "id");
        this.otherMap[attribute.fieldName] = "";
    }

    handleInputChange(event: IKeyValue<string>, attribute: IFormattedAttribute) {
        const selections: IFormattedAttributeOption[] = this.record[attribute.fieldName] ?
            (attribute.required ? [(this.record[attribute.fieldName] as IAttributeOptionV2)] : this.record[attribute.fieldName]) as IFormattedAttributeOption[]
            : [];
        attribute.filteredValues = this.lookupService.filterOptionsByInput(selections, attribute.values, event.value, "id", "label");
        if (attribute.allowOther) this.otherMap[attribute.fieldName] = event.value;
    }

    orgChange(orgId: string, attribute: IFormattedAttribute) {
        this.recordActionService.changeRecord(attribute, orgId ? {
            value: getOrgNameById(orgId)(this.store.getState()),
            id: orgId,
        } : null);
    }

    multiSelectChange({ currentValue }: ISelection<IFormattedAttributeOption[]>, attribute: IFormattedAttribute) {
        this.recordActionService.changeRecord(attribute, currentValue || []);
        attribute.filteredValues = this.lookupService.filterOptionsBySelections(currentValue, attribute.values, "id");
        this.otherMap[attribute.fieldName] = "";
    }

    selectOther(attribute: IFormattedAttribute) {
        const currentSelections = this.record[attribute.fieldName] as IFormattedAttributeOption[];
        const otherSelection: IFormattedAttributeOption = { value: this.otherMap[attribute.fieldName], label: this.otherMap[attribute.fieldName], id: null };
        const existingOption: IFormattedAttributeOption = attribute.values.find((option: IFormattedAttributeOption): boolean => option.value.toLowerCase().trim() === otherSelection.value.toLowerCase().trim());
        if (attribute.responseType === AttributeResponseTypes.SingleSelect) {
            this.recordActionService.changeRecord(attribute, [existingOption || otherSelection]);
        } else {
            this.recordActionService.changeRecord(attribute, this.filterDuplicateOther(otherSelection, currentSelections, existingOption));
            attribute.filteredValues = this.inventoryAdapterV2Service.removeSelectedItems(attribute.values, this.record[attribute.fieldName] as IFormattedAttributeOption[]);
        }
        this.otherMap[attribute.fieldName] = "";
    }

    filterDuplicateOther(otherSelection: IFormattedAttributeOption, currentSelections: IFormattedAttributeOption[], existingOption: IFormattedAttributeOption): IFormattedAttributeOption[] {
        if (currentSelections && currentSelections.length) {
            const duplicate = currentSelections.find((selection: IFormattedAttributeOption): boolean => {
                return selection.value.toLowerCase().trim() === otherSelection.value.toLowerCase().trim();
            });
            return duplicate ? currentSelections : [existingOption || otherSelection, ...currentSelections];
        } else {
            return [existingOption || otherSelection];
        }
    }

    userChange(user: IOtOrgUserOutput, attribute: IFormattedAttribute) {
        if (!user) return;
        const currentSelection = this.record[attribute.fieldName];
        const newSelection: { value: string, id?: string } = user.selection ? {
            value: typeof user.selection === "string" ? user.selection : user.selection.FullName,
        } : null;
        if (newSelection && Utilities.matchRegex(user.selection.Id, Regex.GUID)) {
            newSelection.id = typeof user.selection === "string" ? null : user.selection.Id;
        }
        const selection = newSelection ? [newSelection] : null;
        if (this.inventoryUserLogicService.userSelectionHasChanged(currentSelection as IFormattedAttributeOption[], selection as IFormattedAttributeOption[]) || !user.valid) {
            this.recordActionService.changeRecord(attribute, selection || [], user.selection && !user.valid);
        }
        if (selection && selection[0] && selection[0].id) {
            const userDetail = getUserById(selection[0].id)(this.store.getState());
            if (!userDetail) {
                this.userActionService.fetchUsersDetailsById([selection[0].id]);
            }
        }
    }

    handleLockClicked(fieldName: string) {
        if (fieldName === InventoryFieldNames.RiskLevel) {
            this.viewRisk.emit();
        }
    }

    toggleOnEditMode(fieldName: string, fieldType: string) {
        this.recordActionService.toggleEditingRecord(true);
        setTimeout(() => {
            const fieldEl: HTMLElement = this.element.nativeElement.querySelector(`#${fieldName}`);
            const focusEl: HTMLElement = (fieldEl as HTMLElement).querySelector(fieldType === AttributeResponseTypes.Date ? ".btnpicker" : "input");
            if (focusEl) {
                focusEl.focus();
                focusEl.click();
            }
        }, 0);
    }

    private componentWillReceiveState(state) {
        this.record = { ...getRecord(state), ...getRecordChanges(state) };
        this.isLoading = getIsLoadingRecord(state);
        this.editMode = getIsEditingRecord(state);
    }
}
