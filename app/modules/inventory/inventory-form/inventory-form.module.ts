// Angular
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";
import { HttpClientModule } from "@angular/common/http";

// TODO: Remove shared module once ot-org-user moved to its own module
import { SharedModule } from "sharedModules/shared.module";

// Modules
import { InventorySharedModule } from "inventorySharedModule/inventory-shared.module";
import {
    OtLoadingModule,
    OtButtonModule,
    OtDateTimeModule,
    OtInputModule,
    OtTooltipModule,
    OtLookupModule,
    OtFormModule,
} from "@onetrust/vitreus";
import { TranslateModule } from "@ngx-translate/core";
import { OTPipesModule, PIPES } from "modules/pipes/pipes.module";

// Components
import { InventoryFormComponent } from "inventoryModule/inventory-form/shared/components/inventory-form/inventory-form.component";
import { InventoryFormCellComponent } from "inventoryModule/inventory-form/shared/components/inventory-form-cell/inventory-form-cell.component";

@NgModule({
    imports: [
        SharedModule,
        CommonModule,
        InventorySharedModule,
        FormsModule,
        HttpClientModule,
        OtFormModule,
        OtInputModule,
        OtDateTimeModule,
        OtLoadingModule,
        OtButtonModule,
        OtLookupModule,
        OtTooltipModule,
        OTPipesModule,
        TranslateModule,
    ],
    exports: [
        InventoryFormComponent,
        InventoryFormCellComponent,
    ],
    declarations: [
        InventoryFormComponent,
        InventoryFormCellComponent,
    ],
    providers: [
        PIPES,
    ],
})
export class InventoryFormModule {}
