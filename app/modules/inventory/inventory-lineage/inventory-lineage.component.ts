// Rxjs
import { takeUntil } from "rxjs/operators";
import { Subject } from "rxjs";

// Angular
import {
    Component,
    OnInit,
    Inject,
    OnDestroy,
    Input,
} from "@angular/core";

// 3rd Party
import * as $ from "jquery";
import {
    partial,
    filter,
    extend,
    isNil,
    includes,
    forEach,
    toString,
    uniqBy,
    map,
    flatten,
    find,
} from "lodash";
import "images/LineageIcons/swimlanes.svg";
import "images/LineageIcons/swimlanes-new.svg";

// Services
import { InventoryLineageService } from "modules/inventory/inventory-lineage/shared/services/inventory-lineage.service";
import { LineageApiService } from "modules/inventory/inventory-lineage/shared/services/lineage-api.service";
import { InventoryService } from "inventorySharedModule/shared/services/inventory-api.service";
import { InventoryAdapter } from "oneServices/adapters/inventory-adapter.service";
import { InventoryAdapterV2Service } from "inventoryModule/services/adapter/inventory-adapter-v2.service";
import { RecordActionService } from "inventoryModule/services/action/record-action.service";

// Tokens
import { StoreToken } from "tokens/redux-store.token";

// interfaces
import { Permissions } from "modules/shared/services/helper/permissions.service";
import { IStore } from "interfaces/redux.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";
import {
    ICellSize,
    ICellProperties,
    ICellAttributes,
    IInventoryLineageState,
} from "inventoryModule/inventory-lineage/shared/interfaces/inventory-lineage.interface";
import {
    IInventoryRecord,
    IInventoryRecordV2,
    IAttributeOptionV2,
    IRelatedTableRow,
    IRelatedTable,
    IFormattedPersonalDataTableRow,
    IAttributeDetailV2,
} from "interfaces/inventory.interface";
import { IAttributeMap } from "interfaces/inventory-reducer.interface";
import {
    IStringMap,
    INumberMap,
} from "interfaces/generic.interface";

// Const + Enum
import {
    InventoryTableIds,
    InventoryContexts,
    RelationshipLinkType,
} from "enums/inventory.enum";
import { LineageElementTypes,
    LineageImageTypes,
    InventoryLineageAssociationTypeKeys,
    InventoryPermissions,
    InventoryPartiesWithAccessTypes,
} from "constants/inventory.constant";
import {
    InventorySchemaIds,
    NewInventorySchemaIds,
} from "constants/inventory-config.constant";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

declare var joint: any;

@Component({
    selector: "inventory-lineage",
    templateUrl: "./inventory-lineage.component.html",
})
export class InventoryLineageComponent implements OnInit, OnDestroy {

    @Input() record: IInventoryRecordV2;
    @Input() recordId: string;

    relatedInventories: INumberMap<IRelatedTable>;
    currentDiagram: string;
    fetchingDiagram: boolean;
    graph: joint.dia.Graph;
    paper: joint.dia.Paper;
     // todo: WEBPACK 4 Type fixes Temporary type fix
    // commandManager: joint.dia.CommandManager;
    // snaplines: joint.ui.Snaplines;
    // paperScroller: joint.ui.PaperScroller;
    // stencil: joint.ui.Stencil;
    // keyboard: joint.ui.Keyboard;
    // clipboard: joint.ui.Clipboard;
    // selection: joint.ui.Selection;
    // toolbar: joint.ui.Toolbar;
    // navigator: joint.ui.Navigator;

    commandManager: any;
    snaplines: any;
    paperScroller: any;
    stencil: any;
    keyboard: any;
    clipboard: any;
    selection: any;
    toolbar: any;
    navigator: any;

    gridEnabled = true;
    isLoadingDetails = false;
    isRelatedAsset = false;
    isRelatedParty = false;
    isRelatedVendor = false;
    isContractor = false;
    isEmployee = false;
    isCustomer = false;
    isProspectiveEmployee = false;
    savingDiagram = false;
    graphReady = false;
    gridLabelCells: joint.dia.Cell[] = [];
    assetContext = InventoryContexts.Details;
    partyContext = InventoryContexts.LineageSidebar;
    relatedRecord: IInventoryRecord;
    relatedInventoryId: string;
    currentInventoryId: string;
    relatedAttributeMap: IAttributeMap;
    formattedContractorData: IStringMap<string>;
    formattedEmployeeData: IStringMap<string>;
    formattedCustomerData: IStringMap<string>;
    formattedProspectiveEmployeeData: IStringMap<string>;
    assetDetails: IInventoryRecordV2[] = [];
    assetDisposal: IAttributeOptionV2[] = [];
    contractorCategories: string[] = [];
    employeeCategories: string[] = [];
    customerCategories: string[] = [];
    prospectiveEmployeeCategories: string[] = [];
    attributeMap: IAttributeMap;
    tableIds = InventoryTableIds;
    schemaIds = NewInventorySchemaIds;
    config: any;
    inspector: any;
    dataDisposalAttribute: IAttributeDetailV2;

    width = 85;
    height = 68;

    gridCoordinatesMatrix = {
        "Data Subject": {
            "Information Source": {
                x: [20, 267],
                y: [20, 278],
            },
            "Information Storage and Processing": {
                x: [268, 514],
                y: [20, 278],
            },
            "Access and Transfers": {
                x: [515, 761],
                y: [20, 278],
            },
            "Archive or Destroy": {
                x: [762, 1008],
                y: [20, 278],
            },
        },
        "Internal": {
            "Information Source": {
                x: [20, 267],
                y: [279, 534],
            },
            "Information Storage and Processing": {
                x: [268, 514],
                y: [279, 534],
            },
            "Access and Transfers": {
                x: [515, 761],
                y: [279, 534],
            },
            "Archive or Destroy": {
                x: [762, 1008],
                y: [279, 534],
            },
        },
        "External": {
            "Information Source": {
                x: [20, 267],
                y: [535, 790],
            },
            "Information Storage and Processing": {
                x: [268, 514],
                y: [535, 790],
            },
            "Access and Transfers": {
                x: [515, 761],
                y: [535, 790],
            },
            "Archive or Destroy": {
                x: [762, 1008],
                y: [535, 790],
            },
        },
    };

    private destroy$: Subject<void> = new Subject();

    constructor(
        @Inject(StoreToken) private store: IStore,
        readonly lineageApiService: LineageApiService,
        readonly inventoryLineageService: InventoryLineageService,
        readonly inventoryService: InventoryService,
        readonly inventoryAdapter: InventoryAdapter,
        readonly permissions: Permissions,
        readonly translatePipe: TranslatePipe,
        readonly recordActionService: RecordActionService,
        readonly inventoryAdapterV2Service: InventoryAdapterV2Service,
    ) {}

    // TODO: remove all IRelatedTableRow types once all users on InventoryLinking

    ngOnInit() {
        (require as any).ensure([], (require) => {
            (window as any).joint = require("../../../../lib/rappid/rappid");
            this.config = require("../../../../lib/rappid/configuration");
            require("../../../../lib/rappid/joint.shapes.app");

            this.inventoryLineageService.inventoryLineageState$
                .pipe(takeUntil(this.destroy$))
                .subscribe((stateData: IInventoryLineageState) => {
                    this.currentDiagram = stateData.diagram;
                    this.formattedContractorData = stateData.formattedContractorData;
                    this.formattedEmployeeData = stateData.formattedEmployeeData;
                    this.formattedCustomerData = stateData.formattedCustomerData;
                    this.formattedProspectiveEmployeeData = stateData.formattedProspectiveEmployeeData;
                    this.contractorCategories = Object.keys(this.formattedContractorData);
                    this.employeeCategories = Object.keys(this.formattedEmployeeData);
                    this.customerCategories = Object.keys(this.formattedCustomerData);
                    this.prospectiveEmployeeCategories = Object.keys(this.formattedProspectiveEmployeeData);
                    this.relatedInventories = stateData.relatedInventories;
                    this.assetDetails = stateData.relatedAssetDetails;
                    this.assetDisposal = this.setAssetDisposal();
                    this.savingDiagram = stateData.savingDiagram;
                    this.inspector = stateData.inspector;
                },
            );

            this.inventoryService.getAllAttributes(InventorySchemaIds.Assets).then((response: IProtocolResponse<IAttributeDetailV2[]>): void => {
                if (response.data) this.dataDisposalAttribute = response.data.find((attribute: IAttributeDetailV2) => attribute.fieldName === "dataDisposal");
                this.inventoryLineageService.setRelatedPartyOptions(this.record);
                this.initializePaper();
                this.initializeStencil();
                this.initializeSelection();
                this.initializeHaloAndInspector();
                this.initializeNavigator();
                this.initializeToolbar();
                this.initializeKeyboardShortcuts();
                this.initializeTooltips();
                this.addGrid();
            });

        }, "rappid");

    }

    ngOnDestroy() {
        this.destroy$.next();
        this.destroy$.complete();
    }

    saveDiagram() {
        this.inventoryLineageService.updateState({savingDiagram: true});
        this.lineageApiService.saveDiagram(this.recordId, this.graph).toPromise().then((response: IProtocolResponse<joint.dia.Graph>): void => {
            if (response.result) {
                this.inventoryLineageService.setCurrentLineageDiagram(JSON.stringify(response.data));
            }
            this.inventoryLineageService.updateState({savingDiagram: false});
        });
    }

    initializePaper() {
        this.graph = new joint.dia.Graph();

        this.graph.on("add", (cell: joint.dia.Cell, collection: any, opt: any) => {
            this.setActiveCellType(cell.attributes, "add");
            if (opt.stencil) this.createInspector(cell);
        });

        this.graph.on("remove", (cell: joint.dia.Cell, collection: any, opt: any) => {
            this.setActiveCellType(cell.attributes, "remove");
        });

        this.commandManager = new joint.dia.CommandManager({
            graph: this.graph,
            cmdBeforeAdd: (cmdName: string, cell: joint.dia.Cell, graph: joint.dia.Graph, options: any): boolean => {
                return !(this.isGridCell(cell) || this.isGridLabel(cell));
            },
        });

        this.paper = new joint.dia.Paper({
            defaultRouter: (vertices, opt, linkView) => {
                const s = linkView.sourceBBox.center();
                const t = linkView.targetBBox.center();
                return [{ x: (s.x + t.x) / 2, y: s.y }, { x: (s.x + t.x) / 2, y: t.y }];
            },
            width: 1500,
            height: 1500,
            gridSize: 10,
            drawGrid: true,
            model: this.graph,
            defaultLink: new joint.shapes.app.Link(),
            restrictTranslate: true,
            clickThreshold: 1,
            defaultAnchor: {
                name: "bottomRight",
            },
            interactive: (cellView: joint.dia.CellView): boolean => {
                return cellView.model.attributes.type !== LineageElementTypes.Grid && cellView.model.attributes.type !== LineageElementTypes.GridLabel;
            },
        });

        this.paper.on("blank:mousewheel", partial(this.onMousewheel, null), this);
        this.paper.on("cell:mousewheel", this.onMousewheel.bind(this));
        this.paper.on("cell:pointerdown", (cell: any) => {
            this.setActiveCellType(cell.model.attributes);
            if (cell.model.attributes && cell.model.attributes.inventoryId) {
                const attributes = cell.model.attributes;
                this.populateRecordData(attributes);
            }
        });

        this.snaplines = new joint.ui.Snaplines({ paper: this.paper });

        this.paperScroller = new joint.ui.PaperScroller({
            paper: this.paper,
            autoResizePaper: false,
            cursor: "grab",
        });

        $(".inventory-lineage__paper--container").append(this.paperScroller.el);

        if (this.currentDiagram && !isNil(JSON.parse(this.currentDiagram)) && JSON.parse(this.currentDiagram).cells) {
            const json = JSON.parse(this.currentDiagram);
            json.cells.forEach((cell) => delete cell.router);
            this.graph.fromJSON(json);
        }

        if (this.isGridCell(this.graph.getFirstCell())) {
            this.zoomToFit();
        } else if (this.gridEnabled) {
            this.addGrid();
            this.zoomToFit();
        } else {
            this.paperScroller.render().center();
        }

        if (this.currentDiagram && !isNil(JSON.parse(this.currentDiagram)) && JSON.parse(this.currentDiagram).cells) {
            this.paperScroller.centerContent();
        }
        this.graphReady = true;
    }

    trackByIndex(index: number): number {
        return index;
    }

    setAssetDisposal(): IAttributeOptionV2[] {
        return uniqBy(flatten(filter(map(this.assetDetails, (asset: IInventoryRecordV2): IAttributeOptionV2 => {
            if (this.relatedInventories[InventoryTableIds.Assets].rows) {
                const relatedAsset = find(this.relatedInventories[InventoryTableIds.Assets].rows, (relatedInventoryAsset: IRelatedTableRow): boolean => {
                    return asset.id === relatedInventoryAsset.inventoryId && relatedInventoryAsset.inventoryAssociationTypeKey === RelationshipLinkType.Destination;
                });
                if (relatedAsset) {
                    return asset.dataDisposal as IAttributeOptionV2;
                }
            }
        }), (item: IAttributeOptionV2): boolean => Boolean(item))), "id");
    }

    setActiveCellType(cellAttributes: ICellAttributes, action?: string) {
        if (cellAttributes.inventoryId) {
            this.currentInventoryId = cellAttributes.inventoryId;
        }
        switch (cellAttributes.type) {
            case LineageElementTypes.RelatedAsset:
                this.isRelatedAsset = true;
                this.isRelatedParty = false;
                this.isRelatedVendor = false;
                this.isContractor = false;
                this.isCustomer = false;
                this.isEmployee = false;
                this.isProspectiveEmployee = false;
                break;
            case LineageElementTypes.RelatedParty:
                this.isRelatedAsset = false;
                this.isRelatedParty = true;
                this.isRelatedVendor = false;
                this.isContractor = false;
                this.isCustomer = false;
                this.isEmployee = false;
                this.isProspectiveEmployee = false;
                break;
            case LineageElementTypes.RelatedVendor:
                this.isRelatedAsset = false;
                this.isRelatedParty = false;
                this.isRelatedVendor = true;
                this.isContractor = false;
                this.isCustomer = false;
                this.isEmployee = false;
                this.isProspectiveEmployee = false;
                break;
            case LineageElementTypes.DataSubjects.Contractor:
                this.isRelatedParty = false;
                this.isRelatedAsset = false;
                this.isRelatedVendor = false;
                this.isContractor = true;
                this.isCustomer = false;
                this.isEmployee = false;
                this.isProspectiveEmployee = false;
                break;
            case LineageElementTypes.DataSubjects.Employee:
                this.isRelatedParty = false;
                this.isRelatedAsset = false;
                this.isRelatedVendor = false;
                this.isContractor = false;
                this.isCustomer = false;
                this.isEmployee = true;
                this.isProspectiveEmployee = false;
                break;
            case LineageElementTypes.DataSubjects.Customer:
                this.isRelatedParty = false;
                this.isRelatedAsset = false;
                this.isRelatedVendor = false;
                this.isContractor = false;
                this.isCustomer = true;
                this.isEmployee = false;
                this.isProspectiveEmployee = false;
                break;
            case LineageElementTypes.DataSubjects.ProspectiveEmployee:
                this.isRelatedParty = false;
                this.isRelatedAsset = false;
                this.isRelatedVendor = false;
                this.isContractor = false;
                this.isCustomer = false;
                this.isEmployee = false;
                this.isProspectiveEmployee = true;
                break;
            default:
                this.isRelatedParty = false;
                this.isRelatedAsset = false;
                this.isRelatedVendor = false;
                this.isContractor = false;
                this.isCustomer = false;
                this.isEmployee = false;
                this.isProspectiveEmployee = false;
        }
        if (action && action === "remove") {
            this.isRelatedParty = false;
            this.isRelatedAsset = false;
            this.isRelatedVendor = false;
            this.isContractor = false;
            this.isCustomer = false;
            this.isEmployee = false;
            this.isProspectiveEmployee = false;
        } else if (action && action === "add") {
            this.isRelatedAsset = false;
        } else if (cellAttributes.type === LineageElementTypes.RelatedAsset && cellAttributes.inventoryId === "") {
            this.isRelatedAsset = false;
        }
    }

    populateRecordData(attributes: ICellAttributes) {
        this.setActiveCellType(attributes);
        if (this.isNotCurrentRelatedRecord(attributes.inventoryId) && (this.isRelatedAsset || this.isRelatedVendor)) {
            this.isLoadingDetails = true;
            if (this.isRelatedAsset || this.isRelatedVendor) {
                this.recordActionService.setRecordAndSchemaById(
                    this.isRelatedVendor ? InventorySchemaIds.Vendors : InventorySchemaIds.Assets,
                    attributes.inventoryId,
                    false,
                    true,
                ).then(() => {
                    this.isLoadingDetails = false;
                });
            }
        }
    }

    isNotCurrentRelatedRecord(inventoryId: string) {
        return !this.relatedRecord || (this.relatedRecord && inventoryId !== this.relatedRecord.id);
    }

    initializeStencil() {
        this.translateStencilLabels();
        this.stencil = new joint.ui.Stencil({
            paper: this.paperScroller,
            snaplines: this.snaplines,
            scaleClones: true,
            width: 240,
            groups: this.config.stencil.customGroups,
            dropAnimation: true,
            groupsToggleButtons: true,
            // Remove comment to enable search by item name
            // search: {
            //     "*": ["type", "attrs/text/text", "attrs/.label/text"],
            //     "org.Member": ["attrs/.rank/text", "attrs/.name/text"]
            // },
            layout: {
                columnWidth: 110,
                columns: 2,
                rowHeight: 100,
            },
            // Remove tooltip definition from clone
            dragStartClone: (cell: joint.dia.Cell) => cell.clone().removeAttr("./data-tooltip"),
        });

        $(".inventory-lineage__stencil--container").append(this.stencil.el);
        this.stencil.render().load(this.config.stencil.shapes);
    }

    initializeKeyboardShortcuts() {

        this.keyboard = new joint.ui.Keyboard();
        this.keyboard.on({

            "ctrl+c": () => {
                // Copy all selected elements and their associated links.
                this.clipboard.copyElements(this.selection.collection, this.graph);
            },

            "ctrl+v": () => {
                const pastedCells = this.clipboard.pasteCells(this.graph, {
                    translate: { dx: 20, dy: 20 },
                    useLocalStorage: true,
                });

                const elements = filter(pastedCells, (cell) => cell.isElement());
                // Make sure pasted elements get selected immediately. This makes the UX better as
                // the user can immediately manipulate the pasted elements.
                this.selection.collection.reset(elements);
            },

            "ctrl+x shift+delete": () => {
                this.clipboard.cutElements(this.selection.collection, this.graph);
            },

            "delete backspace": (evt: JQueryEventObject) => {
                evt.preventDefault();
                this.graph.removeCells(this.selection.collection.toArray());
            },

            "ctrl+z": () => {
                this.commandManager.undo();
                this.selection.cancelSelection();
            },

            "ctrl+y": () => {
                this.commandManager.redo();
                this.selection.cancelSelection();
            },

            "ctrl+a": () => {
                this.selection.collection.reset(this.graph.getElements());
            },

            "ctrl+plus": (evt: JQueryEventObject) => {
                evt.preventDefault();
                this.paperScroller.zoom(0.2, { max: 5, grid: 0.2 });
            },

            "ctrl+minus": (evt: JQueryEventObject) => {
                evt.preventDefault();
                this.paperScroller.zoom(-0.2, { min: 0.2, grid: 0.2 });
            },

            "keydown:shift": (evt: JQueryEventObject) => {
                this.paperScroller.setCursor("crosshair");
            },

            "keyup:shift": () => {
                this.paperScroller.setCursor("grab");
            },
        });
    }

    initializeSelection() {

        this.clipboard = new joint.ui.Clipboard();
        this.selection = new joint.ui.Selection({
            paper: this.paper,
            handles: this.config.selection.handles,
            filter: (cell: joint.dia.Cell): boolean => {
                return this.isGridCell(cell) || this.isGridLabel(cell);
            },
        });

        // Initiate selecting when the user grabs the blank area of the paper while the Shift key is pressed.
        // Otherwise, initiate paper pan.
        this.paper.on("blank:pointerdown", (evt: JQueryEventObject, x: number, y: number) => {

            if (this.keyboard.isActive("shift", evt)) {
                this.selection.startSelecting(evt);
            } else {
                this.selection.cancelSelection();
                this.paperScroller.startPanning(evt);
            }

        });

        this.paper.on("element:pointerdown", (elementView: joint.dia.ElementView, evt: JQueryEventObject) => {

            // Select an element if CTRL/Meta key is pressed while the element is clicked.
            if (this.keyboard.isActive("ctrl meta", evt)) {
                this.selection.collection.add(elementView.model);
            }

        });

        this.selection.on("selection-box:pointerdown", (elementView: joint.dia.ElementView, evt: JQueryEventObject) => {

            // Unselect an element if the CTRL/Meta key is pressed while a selected element is clicked.
            if (this.keyboard.isActive("ctrl meta", evt)) {
                this.selection.collection.remove(elementView.model);
            }

        });
    }

    addGrid() {
        const imagePath = "images/LineageIcons/swimlanes-new.svg";
        const xPosition = 200;
        if (!this.isGridCell(this.graph.getFirstCell())) {
            const cells = [];
            const grid = new joint.shapes.basic.Grid({
                position: { x: xPosition, y: 0 },
                size: { width: 1008, height: 792 },
                attrs: {
                    image: {
                        "width": 1008,
                        "height": 792,
                        "xlink:href": imagePath,
                    },
                },
                z: 0,
            });
            cells.push(grid);
            this.graph.addCell(cells);
            this.gridEnabled = true;
            this.addGridLabels();
        }
    }

    getGridLabels(graph: joint.dia.Graph) {
        this.gridLabelCells = [];
        forEach(graph.getElements(), (cell: joint.dia.Cell) => {
            if (this.isGridLabel(cell)) {
                this.gridLabelCells.push(cell);
            }
        });
    }

    addGridLabels() {
        let labelProperties: ICellProperties[];
        const labelSize: ICellSize = { width: 90, height: 20 };
        labelProperties = [
            { position: { x: 277, y: 43 }, size: labelSize, angle: 0, text: this.translatePipe.transform("DataSubject") },
            { position: { x: 475, y: 43 }, size: labelSize, angle: 0, text: this.translatePipe.transform("InformationSource") },
            { position: { x: 675, y: 43 }, size: labelSize, angle: 0, text: this.translatePipe.transform("InformationStorageAndProcessing") },
            { position: { x: 875, y: 43 }, size: labelSize, angle: 0, text: this.translatePipe.transform("DestinationAndAccess") },
            { position: { x: 1065, y: 43 }, size: labelSize, angle: 0, text: this.translatePipe.transform("ArchiveOrDestroy") },
        ];
        this.gridLabelCells = [];

        forEach(labelProperties, (item: ICellProperties) => {
            const gridLabel = new joint.shapes.basic.GridLabel({
                position: item.position,
                size: item.size,
                angle: item.angle,
                attrs: {
                    text: {
                        text: item.text,
                    },
                },
            });
            this.gridLabelCells.push(gridLabel);
        });

        this.graph.addCells(this.gridLabelCells);
    }

    autoDraw() {
        this.noFrameworkAutoDraw();
    }

    noFrameworkAutoDraw() {
        if (this.gridEnabled) {
            this.toggleGrid();
        }
        this.clear();
        forEach(this.relatedInventories, (section, key) => {
            const records = key === toString(10) ? uniqBy(section.rows as IRelatedTableRow[], "subject") : section.rows as IRelatedTableRow[];
            forEach(records, (element) => {
                this.createGraphItem(element);
            });
        });

        forEach(this.record.partiesWithAccess as IAttributeOptionV2[], (element) => {
            this.createPartyWithAccess(element);
        });

        this.createDisposalObject();
        this.addLinks();
        this.layoutDirectedGraph();
        this.toggleGrid();
        this.zoomToFit();
    }

    createGraphItem(element: IRelatedTableRow | IFormattedPersonalDataTableRow) {
        if ((element as IRelatedTableRow).inventoryAssociationTypeKey === InventoryLineageAssociationTypeKeys.Related && !element.subject) {
            return;
        }
        let asset;
        const graphItem = {
            size: { width: this.width, height: this.height},
                inventoryId: (element as IRelatedTableRow).inventoryId,
                relation: (element as IRelatedTableRow).inventoryAssociationTypeKey || InventoryLineageAssociationTypeKeys.Related,
                attrs: {
                    image: {
                        width: this.width,
                        height: this.height,
                    },
                    text: {
                        fontSize: 11,
                    },
                },
            z: 5,
        };
        if ((element as IRelatedTableRow).inventoryType === InventoryTableIds.Elements || (element as IRelatedTableRow).inventoryType === NewInventorySchemaIds[InventoryTableIds.Elements]) {
            graphItem.attrs.image["xlink:href"] = LineageImageTypes[(element as IRelatedTableRow).inventoryAssociationTypeKey][this.dataSubjectRemoveSpace(element.subject)] || LineageImageTypes[(element as IRelatedTableRow).inventoryAssociationTypeKey].Generic;
            graphItem.attrs.text["textWrap"] = {
                text: element.subjectKey ? this.translatePipe.transform(element.subjectKey) : element.subject,
                width: "140%",
                height: "120",
            };
            asset = new joint.shapes.basic.DataSubjects(graphItem);
        } else if (this.permissions.canShow("EnableDataSubjectsElementsV2") && (element as IFormattedPersonalDataTableRow).dataElement && element.subject) {
            graphItem.attrs.image["xlink:href"] = LineageImageTypes[InventoryLineageAssociationTypeKeys.Related][this.dataSubjectRemoveSpace(element.subject)] || LineageImageTypes[InventoryLineageAssociationTypeKeys.Related].Generic;
            graphItem.attrs.text["textWrap"] = {
                text: element.subjectKey ? this.translatePipe.transform(element.subjectKey) : element.subject,
                width: "140%",
                height: "120",
            };
            asset = new joint.shapes.basic.DataSubjects(graphItem);
        } else {
            graphItem.attrs.text["textWrap"] = {
                text: element.nameKey ? this.translatePipe.transform(element.nameKey) : element.name,
                width: "140%",
                height: "120",
            };
            if ((element as IRelatedTableRow).inventoryType === InventoryTableIds.Assets || (element as IRelatedTableRow).inventoryType === NewInventorySchemaIds[InventoryTableIds.Assets]) {
                const details: IInventoryRecordV2 = this.assetDetails.find((item) => item.id === (element as IRelatedTableRow).inventoryId);
                const imageKey: string = details.type && details.type[0] ? details.type[0].value : "Application";
                graphItem.attrs.image["xlink:href"] = LineageImageTypes[(element as IRelatedTableRow).inventoryType][imageKey] || LineageImageTypes[(element as IRelatedTableRow).inventoryType]["Application"];
                asset = new joint.shapes.basic.RelatedAsset(graphItem);
            } else if ((element as IRelatedTableRow).inventoryType === InventoryTableIds.Vendors || (element as IRelatedTableRow).inventoryType === NewInventorySchemaIds[InventoryTableIds.Vendors]) {
                graphItem.attrs.image["xlink:href"] = LineageImageTypes[(element as IRelatedTableRow).inventoryType][(element as IRelatedTableRow).type] || LineageImageTypes[(element as IRelatedTableRow).inventoryType].Generic;
                asset = new joint.shapes.basic.Vendors(graphItem);
            }
        }
        if (asset) this.graph.addCell(asset);
    }

    createPartyWithAccess(element: IAttributeOptionV2) {
        const partyObject = {
            size: { width: this.width, height: this.height},
            inventoryId: element.id,
            relation: InventoryLineageAssociationTypeKeys.RelatedParty,
            attrs: {
                image: {
                    width: this.width,
                    height: this.height,
                },
                text: {
                    fontSize: 11,
                    textWrap: {
                        text: element.valueKey ? this.translatePipe.transform(element.valueKey) : element.value,
                        width: "140%",
                        height: "120%",
                    },
                },
            },
            z: 5,
        };
        const imagePath = LineageImageTypes.PartiesWithAccess[InventoryPartiesWithAccessTypes[element.value]] || LineageImageTypes.PartiesWithAccess.Generic;
        partyObject.attrs.image["xlink:href"] = imagePath;
        const asset = new joint.shapes.basic.RelatedParty(partyObject);
        this.graph.addCell(asset);
    }

    createDisposalObject() {
        forEach(this.assetDisposal, (disposal: IAttributeOptionV2) => {
            const disposalObject = {
                size: { width: this.width, height: this.height},
                attributeValueId: disposal.id,
                attrs: {
                    image: {
                        width: this.width,
                        height: this.height,
                    },
                    text: {
                        fontSize: 11,
                        textWrap: {
                            text: this.dataDisposalAttribute ? this.inventoryAdapterV2Service.getFieldDisplayValue([disposal], this.dataDisposalAttribute) : disposal.value,
                            width: "140%",
                            height: "120%",
                        },
                    },
                },
                z: 5,
            };
            const imagePath = LineageImageTypes.Disposal[disposal.value] || LineageImageTypes.Disposal.Generic;
            disposalObject.attrs.image["xlink:href"] = imagePath;
            const asset = new joint.shapes.basic.DataDisposal(disposalObject);
            this.graph.addCell(asset);
        });
    }

    dataSubjectRemoveSpace(parseItem): string {
        const parsedString = parseItem.replace(" ", "");
        return parsedString;
    }

    addLinks() {
        const cells: joint.dia.Cell[] = this.graph.getCells();

        const dataSubjectList: joint.dia.Cell[] = filter(cells, (cell) => {
            return cell.attributes.type === LineageElementTypes.DataSubjects.Custom;
        });

        const sourcesList: joint.dia.Cell[] = filter(cells, (cell) => {
            return (cell.attributes.type === LineageElementTypes.RelatedAsset || cell.attributes.type === LineageElementTypes.RelatedVendor)
                && cell.attributes.relation === InventoryLineageAssociationTypeKeys.SourceCollection;
        });

        const storageList: joint.dia.Cell[] = filter(cells, (cell) => {
            return (cell.attributes.type === LineageElementTypes.RelatedAsset || cell.attributes.type === LineageElementTypes.RelatedVendor)
                && cell.attributes.relation === InventoryLineageAssociationTypeKeys.StorageProcessing;
        });

        const destList: joint.dia.Cell[] = filter(cells, (cell) => {
            return ((cell.attributes.type === LineageElementTypes.RelatedAsset || cell.attributes.type === LineageElementTypes.RelatedVendor) && cell.attributes.relation === InventoryLineageAssociationTypeKeys.DestinationAccess) ||
                (cell.attributes.type === LineageElementTypes.RelatedParty && cell.attributes.relation === InventoryLineageAssociationTypeKeys.RelatedParty);
        });

        const disposalList: joint.dia.Cell[] = filter(cells, (cell) => {
            return cell.attributes.type === LineageElementTypes.DataDisposal;
        });

        dataSubjectList.forEach(((dataSubjectElement) => {
            sourcesList.forEach((sourceElement) => {
                this.graph.addCell(this.createLink(dataSubjectElement.id, sourceElement.id));
            });
        }));

        sourcesList.forEach((sourceElement) => {
            storageList.forEach((storageElement) => {
                this.graph.addCell(this.createLink(sourceElement.id, storageElement.id));
            });
        });

        storageList.forEach((storageElement) => {
            destList.forEach((destinationElement) => {
                this.graph.addCell(this.createLink(storageElement.id, destinationElement.id));
                const details = find(this.assetDetails, ["id", destinationElement.attributes.inventoryId]);
                if (details && details.dataDisposal && (details.dataDisposal as IAttributeOptionV2[]).length) {
                    const disposalObject = find(disposalList, ["attributes.attributeValueId", details.dataDisposal[0].id]);
                    this.graph.addCell(this.createLink(destinationElement.id, disposalObject.id));
                }
            });
        });
    }

    createLink(sourceId, destinationId): any {
        const link = new joint.shapes.app.Link ({
            connector: {
                name: "normal",
            },
            source: {
                id: sourceId,
                selector: "image",
                args: {
                },
            },
            target: {
                id: destinationId,
                selector: "image",
                args: {
                },
            },
            z: 15,
            attrs: {
            },
        });
        return link;
    }

    removeGrid() {
        const grid: joint.dia.Cell = this.graph.getFirstCell();
        this.getGridLabels(this.graph);
        this.graph.removeCells([grid, ...this.gridLabelCells]);
        this.gridEnabled = false;
    }

    toggleGrid() {
        if (this.gridEnabled) {
            this.removeGrid();
        } else {
            this.addGrid();
        }
    }

    isGridCell(cell: joint.dia.Cell): boolean {
        return cell && cell.attributes ? cell.attributes.type === LineageElementTypes.Grid : false;
    }

    isGridLabel(cell: joint.dia.Cell): boolean {
        return cell && cell.attributes ? cell.attributes.type === LineageElementTypes.GridLabel : false;
    }

    createInspector(cell: joint.dia.Cell) {
        cell.on("change:inventoryId", (change, opt) => {
            if (change === "") {
                this.isRelatedAsset = false;
            } else {
                this.populateRecordData(cell.attributes);
            }
        });
        return joint.ui.Inspector.create(".inventory-lineage__inspector--container", extend({ cell }, this.inspector[cell.get("type")]));
    }

    initializeHaloAndInspector() {

        this.paper.on("element:pointerdown link:options", (cellView: joint.dia.CellView) => {

            const cell = cellView.model;

            if (!this.selection.collection.contains(cell)) {

                if (cell.isElement()) {
                    const freeformElements: string[] = ["basic.Circle", "erd.Relationship", "erd.ISA", "basic.Rect", "basic.Label"];
                    const isFreeformElement: boolean = includes(freeformElements, cell.attributes.type);

                    if (cell.attributes && cell.attributes.type !== LineageElementTypes.GridLabel) {
                        new joint.ui.FreeTransform({
                            cellView,
                            allowRotation: false,
                            preserveAspectRatio: !isFreeformElement,
                            allowOrthogonalResize: cell.get("allowOrthogonalResize") !== false,
                        }).render();

                        new joint.ui.Halo({
                            cellView,
                            handles: this.config.halo.handles,
                        }).render();
                    }

                    this.selection.collection.reset([]);
                    this.selection.collection.add(cell, { silent: true });
                }

                this.createInspector(cell);
            }
        });
    }

    initializeNavigator() {

        this.navigator = new joint.ui.Navigator({
            width: 240,
            height: 115,
            paperScroller: this.paperScroller,
            zoom: false,
        });

        $(".inventory-lineage__navigator--container").append(this.navigator.el);
        this.navigator.render();
    }

    initializeToolbar() {

        this.toolbar = new joint.ui.Toolbar({
            groups: this.config.toolbar.groups,
            tools: this.config.toolbar.tools,
            references: {
                paperScroller: this.paperScroller,
                commandManager: this.commandManager,
            },
        });

        this.toolbar.on({
            "layout:pointerclick": () => this.layoutDirectedGraph(),
            "snapline:change": (checked: boolean) => this.changeSnapLines(checked),
            "grid-size:change": (size: number) => this.paper.setGridSize(size),
        });

        $(".inventory-lineage__toolbar--container").append(this.toolbar.el);
        this.toolbar.render();
    }

    changeSnapLines(checked: boolean) {
        if (checked) {
            this.snaplines.startListening();
            this.stencil.options.snaplines = this.snaplines;
        } else {
            this.snaplines.stopListening();
            this.stencil.options.snaplines = null;
        }
    }

    initializeTooltips() {
        const initTooltip = new joint.ui.Tooltip({
            rootTarget: document.body,
            target: "[data-tooltip]",
            // todo: WEBPACK 4 fix enum
            // direction: joint.ui.Tooltip.TooltipArrowPosition.Auto,
            direction: "auto",
            padding: 10,
        });
    }

    onMousewheel(cellView: joint.dia.CellView, evt: JQueryEventObject, ox: number, oy: number, delta: number) {

        if (this.keyboard && this.keyboard.isActive("alt", evt)) {
            evt.preventDefault();
            this.paperScroller.zoom(delta * 0.2, { min: 0.2, max: 5, grid: 0.2, ox, oy });
        }
    }

    undo() {
        this.commandManager.undo();
    }

    redo() {
        this.commandManager.redo();
    }

    clear() {
        this.graph.clear();
        if (this.gridEnabled) {
            this.addGrid();
        }
    }

    print() {
        (this.paper as any).print();
    }

    toFront() {
        this.selection.collection.invoke("toFront");
    }

    toBack() {
        this.selection.collection.invoke("toBack");
    }

    zoomToFit() {
        this.paperScroller.zoomToFit();
        this.paperScroller.zoomToFit();
    }

    layoutDirectedGraph() {

        joint.layout.DirectedGraph.layout(this.graph, {
            setVertices: false,
            rankDir: "LR",
            ranker: "tight-tree",
            marginX: 285,
            marginY: 85,
            nodeSep: 40,
            rankSep: 110,
        });

        this.paperScroller.centerContent();
    }

    private translateStencilLabels() {
        if (this.config.stencil.shapes.dataSubjects[0].translated) return;
        this.config.stencil.customGroups.dataSubjects.label = this.translatePipe.transform(this.config.stencil.customGroups.dataSubjects.label);
        this.config.stencil.customGroups.assets.label = this.translatePipe.transform(this.config.stencil.customGroups.assets.label);
        this.config.stencil.customGroups.vendors.label = this.translatePipe.transform(this.config.stencil.customGroups.vendors.label);
        this.config.stencil.customGroups.parties.label = this.translatePipe.transform(this.config.stencil.customGroups.parties.label);
        this.config.stencil.customGroups.misc.label = this.translatePipe.transform(this.config.stencil.customGroups.misc.label);

        this.config.stencil.shapes.dataSubjects = this.translateText(this.config.stencil.shapes.dataSubjects);
        this.config.stencil.shapes.assets = this.translateText(this.config.stencil.shapes.assets);
        this.config.stencil.shapes.vendors = this.translateText(this.config.stencil.shapes.vendors);
        this.config.stencil.shapes.parties = this.translateText(this.config.stencil.shapes.parties);
        this.config.stencil.shapes.misc = this.translateText(this.config.stencil.shapes.misc);
    }

    private translateText(configStencilItem: any): any {
        return configStencilItem.map((item) => {
            return {
                ...item,
                translated: true,
                attrs: {
                    ...item.attrs,
                    "text": {
                        ...item.attrs.text,
                        text: this.translatePipe.transform(item.attrs.text.text),
                    },
                    ".": {
                        ...item.attrs["."],
                        "data-tooltip": this.translatePipe.transform(item.attrs["."]["data-tooltip"]),
                    },
                },
            };
        });
    }
}
