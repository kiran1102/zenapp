// Angular
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { HttpClientModule } from "@angular/common/http";

// Modules
import { InventoryFormModule } from "inventoryModule/inventory-form/inventory-form.module";
import { InventorySharedModule } from "inventorySharedModule/inventory-shared.module";
import {
    OtLoadingModule,
    OtButtonModule,
    OtTooltipModule,
    OtFormModule,
} from "@onetrust/vitreus";
import { TranslateModule } from "@ngx-translate/core";

// Components
import { InventoryLineageComponent } from "inventoryModule/inventory-lineage/inventory-lineage.component";

// Services
import { InventoryLineageService } from "inventoryModule/inventory-lineage/shared/services/inventory-lineage.service";
import { LineageApiService } from "inventoryModule/inventory-lineage/shared/services/lineage-api.service";

@NgModule({
    imports: [
        InventorySharedModule,
        CommonModule,
        HttpClientModule,
        OtFormModule,
        OtLoadingModule,
        OtButtonModule,
        OtTooltipModule,
        TranslateModule,
        InventoryFormModule,
    ],
    exports: [
        InventoryLineageComponent,
    ],
    declarations: [
        InventoryLineageComponent,
    ],
    providers: [
        InventoryLineageService,
        LineageApiService,
    ],
})
export class InventoryLineageModule {}
