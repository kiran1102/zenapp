// Rxjs
import {
    BehaviorSubject,
    Observable,
} from "rxjs";

// 3rd party
import { Injectable } from "@angular/core";
import {
    cloneDeep,
    forEach,
    filter,
    uniqBy,
} from "lodash";
import { inspector } from "../../../../../../lib/rappid/inspector";

// Interfaces
import {
    IRelatedTable,
    IRelatedTableRow,
    IInventoryRecordV2,
    IAttributeOptionV2,
} from "interfaces/inventory.interface";
import { IStringMap } from "interfaces/generic.interface";
import { IInventoryLineageState } from "inventoryModule/inventory-lineage/shared/interfaces/inventory-lineage.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

// Const and Enums
import { InventoryTableIds } from "enums/inventory.enum";
import { StatusCodes } from "enums/status-codes.enum";

// Services
import { LineageApiService } from "modules/inventory/inventory-lineage/shared/services/lineage-api.service";
import { NotificationService } from "modules/shared/services/provider/notification.service";

@Injectable()
export class InventoryLineageService {

    readonly inventoryLineageState$: Observable<IInventoryLineageState>;
    private defaultState: IInventoryLineageState = {
        fetchingList: false,
        fetchingAssetDetails: false,
        fetchingDiagram: false,
        exportingDiagram: false,
        fetchingRelated: false,
        savingDiagram: false,
        relatedInventories: {
            [InventoryTableIds.Elements]: null,
            [InventoryTableIds.Assets]: null,
            [InventoryTableIds.Processes]: null,
            [InventoryTableIds.Vendors]: null,
        },
        relatedAssetDetails: [],
        diagram: "",
        formattedContractorData: {},
        formattedEmployeeData: {},
        formattedCustomerData: {},
        formattedProspectiveEmployeeData: {},
        ready: false,
        inspector: cloneDeep(inspector),
    };
    private _inventoryLineageState: BehaviorSubject<IInventoryLineageState> = new BehaviorSubject(cloneDeep(this.defaultState));

    constructor(
        private lineageApiService: LineageApiService,
        private notificationService: NotificationService,
        private translatePipe: TranslatePipe,
    ) {
        this.inventoryLineageState$ = this._inventoryLineageState.asObservable();
    }

    setDefaultState() {
        this.updateState(cloneDeep(this.defaultState));
    }

    updateState(state: {}) {
        const currentState = this._inventoryLineageState.getValue();
        this._inventoryLineageState.next({
            ...currentState,
            ...state,
        });
    }

    setRelatedInventory(inventoryType: number, relatedData: IRelatedTable) {
        const currentState = this._inventoryLineageState.getValue();
        this._inventoryLineageState.next({
            ...currentState,
            relatedInventories: {
                ...currentState.relatedInventories,
                [inventoryType]: relatedData,
            },
            fetchingRelated: false,
        });

        if (inventoryType === InventoryTableIds.Elements && relatedData.rows.length) {
            this.setDataSubjectsData();
        } else if (inventoryType === InventoryTableIds.Assets) {
            this.setRelatedAssetOptions();
        }
    }

    setAssetDetails(relatedAssetDetails: IInventoryRecordV2[]) {
        const currentState = this._inventoryLineageState.getValue();
        this._inventoryLineageState.next({
            ...currentState,
            relatedAssetDetails,
            fetchingAssetDetails: false,
        });
    }

    setCurrentLineageDiagram(diagram: string) {
        const currentState = this._inventoryLineageState.getValue();
        this._inventoryLineageState.next({
            ...currentState,
            diagram,
        });
    }

    setDataSubjectsData() {
        const relatedInventories = this._inventoryLineageState.getValue().relatedInventories;
        if (relatedInventories[InventoryTableIds.Elements] && relatedInventories[InventoryTableIds.Elements].rows) {
            const contractorData: IRelatedTableRow[] = filter(relatedInventories[InventoryTableIds.Elements].rows as IRelatedTableRow[], ["subject", "Contractors"]);
            const employeeData: IRelatedTableRow[] = filter(relatedInventories[InventoryTableIds.Elements].rows as IRelatedTableRow[], ["subject", "Employees"]);
            const customerData: IRelatedTableRow[] = filter(relatedInventories[InventoryTableIds.Elements].rows as IRelatedTableRow[], ["subject", "Customers"]);
            const prospectiveEmployeeData: IRelatedTableRow[] = filter(relatedInventories[InventoryTableIds.Elements].rows as IRelatedTableRow[], ["subject", "Prospective Employees"]);
            const state: IStringMap<IStringMap<string>> = {
                formattedContractorData: this.formatDataSubjectData(contractorData),
                formattedEmployeeData: this.formatDataSubjectData(employeeData),
                formattedCustomerData: this.formatDataSubjectData(customerData),
                formattedProspectiveEmployeeData: this.formatDataSubjectData(prospectiveEmployeeData),
            };
            this.updateState(state);
        }
    }

    setRelatedAssetOptions() {
        const state = this._inventoryLineageState.getValue();
        const lineageInspector = state.inspector;
        const relatedInventories = state.relatedInventories;
        if (relatedInventories[InventoryTableIds.Assets].rows) {
            lineageInspector["basic.RelatedAsset"].inputs.attrs.text.text.type = "select-box";
            lineageInspector["basic.RelatedAsset"].inputs.attrs.text.text.options = this.formatAssetOptions(relatedInventories[InventoryTableIds.Assets].rows as IRelatedTableRow[]);
            this.updateState({ inspector: lineageInspector });
        }
    }

    setRelatedPartyOptions(record: IInventoryRecordV2) {
        const lineageInspector = this._inventoryLineageState.getValue().inspector;
        const attributeValues: IAttributeOptionV2[] = record.partiesWithAccess as IAttributeOptionV2[] || [];
        lineageInspector["basic.RelatedParty"].inputs.attrs.text.text.type = "select-box";
        lineageInspector["basic.RelatedParty"].inputs.attrs.text.text.options = this.formatPartyOptions(attributeValues);
        this.updateState({ inspector: lineageInspector });
    }

    formatDataSubjectData(rows: IRelatedTableRow[]): IStringMap<string> {
        const formattedData: IStringMap<string> = {};
        forEach(rows, (row: IRelatedTableRow) => {
            if (!formattedData[row.category]) {
                formattedData[row.category] = "";
            }
            if (formattedData[row.category] === "") {
                formattedData[row.category] += `${row.name}`;
            } else {
                formattedData[row.category] += `, ${row.name}`;
            }
        });
        return formattedData;
    }

    formatAssetOptions(rows: IRelatedTableRow[]): Array<IStringMap<string>> {
        const options: Array<IStringMap<string>> = [];
        forEach(rows, (row: IRelatedTableRow) => {
            const option: IStringMap<string> = {
                inventoryId: row.inventoryId || row.id,
                value: row.name,
                content: `${row.name} | ${row.organization} | ${row.location}`,
            };
            options.push(option);
        });
        options.push({
            inventoryId: "",
            value: "Related Asset",
            content: "Related Asset",
        });
        return uniqBy(options, "inventoryId");
    }

    formatPartyOptions(attributeValues: IAttributeOptionV2[]): Array<IStringMap<string>> {
        const options: Array<IStringMap<string>> = [];
        forEach(attributeValues, (attributeValue: IAttributeOptionV2) => {
            const option: IStringMap<string> = {
                attributeValueId: attributeValue.id,
                value: attributeValue.value,
                content: attributeValue.value,
            };
            options.push(option);
        });
        options.push({
            attributeValueId: "",
            value: "Related Party",
            content: "Related Party",
        });
        return options;
    }

    getLineageDiagram(recordId: string) {
        this.updateState({ fetchingDiagram: true });
        this.lineageApiService.getDiagram(recordId).toPromise().then((res: IProtocolResponse<any>): void => {
            if (res && res.result && res.data && res.data.map) {
                this.updateState({ fetchingDiagram: false, diagram: res.data.map });
            } else if (res && res.status === StatusCodes.NotFound) {
                this.updateState({ fetchingDiagram: false, diagram: "" });
            } else {
                this.notificationService.alertError(this.translatePipe.transform("Error"), this.translatePipe.transform("LineageDiagramRetrieveError"));
            }
        });
    }
}
