// 3rd Party
import { Injectable } from "@angular/core";

// rxjs
import {
    Observable,
    from,
} from "rxjs";

// Interfaces
import {
    IProtocolPacket,
    IProtocolResponse,
    IProtocolMessages,
    IProtocolConfig,
} from "interfaces/protocol.interface";

// Services
import { ProtocolService } from "modules/core/services/protocol.service";

// Enums
import { StatusCodes } from "enums/status-codes.enum";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

@Injectable()
export class LineageApiService {

    constructor(
        private OneProtocol: ProtocolService,
        private translatePipe: TranslatePipe,
    ) {}

    public getDiagram(id: string): Observable<IProtocolResponse<string>> {
        const config: IProtocolConfig = this.OneProtocol.config("GET", `/api/inventory/v2/datalineage/${id}`);
        const messages: IProtocolMessages = { Error: { Hide: true } };
        return from(this.OneProtocol.http(config, messages).then((res: IProtocolPacket): IProtocolPacket | null => {
            if (res.status === StatusCodes.NotFound) return res;
            if (!res.result) return null;
            return res;
        }));
    }

    public saveDiagram(id: string, body: joint.dia.Graph): Observable<IProtocolResponse<joint.dia.Graph>> {
        const config: IProtocolConfig = this.OneProtocol.config("POST", `/api/inventory/v2/datalineage/${id}`, null, body);
        const messages: IProtocolMessages = { Success: { custom: this.translatePipe.transform("LineageSaveSuccess") }, Error: { custom: this.translatePipe.transform("LineageSaveError") } };
        return from(this.OneProtocol.http(config, messages));
    }
}
