import { INumberMap, IStringMap } from "interfaces/generic.interface";
import { IRelatedTable, IInventoryRecordV2 } from "interfaces/inventory.interface";

export interface IInventoryLineageState {
    fetchingList: boolean;
    fetchingAssetDetails: boolean;
    fetchingDiagram: boolean;
    exportingDiagram: boolean;
    fetchingRelated: boolean;
    savingDiagram: boolean;
    relatedInventories: INumberMap<IRelatedTable>;
    relatedAssetDetails: IInventoryRecordV2[];
    diagram: string;
    formattedContractorData: IStringMap<string>;
    formattedEmployeeData: IStringMap<string>;
    formattedCustomerData: IStringMap<string>;
    formattedProspectiveEmployeeData: IStringMap<string>;
    ready: boolean;
    inspector: any;
}

export interface ICellSize {
    width: number;
    height: number;
}

export interface ICellPosition {
    x: number;
    y: number;
}

export interface ICellProperties {
    position: ICellPosition;
    size: ICellSize;
    angle: number;
    text: string;
}

export interface ICellAttributes {
    angle: number;
    attrs: any;
    id: string;
    position: ICellPosition;
    size: ICellSize;
    type: string;
    z: number;
    inventoryId?: string;
    attributeValueId?: string;
}
