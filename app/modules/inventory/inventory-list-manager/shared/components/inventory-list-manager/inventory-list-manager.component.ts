// 3rd Party
import {
    Component,
    OnInit,
} from "@angular/core";
import { StateService } from "@uirouter/core";

// Services
import { Permissions } from "modules/shared/services/helper/permissions.service";
import { InventoryLaunchModalService } from "inventoryModule/services/action/inventory-launch-modal.service";
import { InventoryListManagerService } from "modules/inventory/inventory-list-manager/shared/services/inventory-list-manager-action.service";

// Interfaces
import {
    IInventoryListAction,
    IInventoryListItem,
    IHeaderOption,
} from "interfaces/inventory.interface";
import { ContextMenuType } from "@onetrust/vitreus";

// Const
import {
    InventoryTranslationKeys,
    InventoryListManagerRoutes,
    InventoryListTypes,
    ButtonTypes,
} from "constants/inventory.constant";
import { InventorySchemaIds } from "constants/inventory-config.constant";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

@Component({
    selector: "inventory-list-manager",
    templateUrl: "inventory-list-manager.component.html",
})
export class InventoryListManagerComponent implements OnInit {

    buttonTypes = ButtonTypes;
    contextMenuType = ContextMenuType;
    inventoryListManagerRoutes = InventoryListManagerRoutes;
    listId: string;
    listTitle: string;
    listActions: IInventoryListAction[] = [];
    currentSearchTerm = "";

    constructor(
        public inventoryListManagerService: InventoryListManagerService,
        private stateService: StateService,
        private permissions: Permissions,
        private inventoryLaunchModalService: InventoryLaunchModalService,
        private translatePipe: TranslatePipe,
    ) { }

    ngOnInit() {
        this.inventoryListManagerService.setDefaultState();
        this.listId = this.stateService.params.listId;
        this.listTitle = InventoryTranslationKeys[this.listId].listTitle;
        this.listActions = this.getListActions(this.listId);
        this.inventoryListManagerService.initializeList(this.listId);
    }

    goToListManagerEntry(route: { path: string }) {
        this.stateService.go(route.path);
    }

    getListActions(listId: string) {
        const listActions: IInventoryListAction[] = [];
        if (this.permissions.canShow("DataMappingAttributesDataElementsAdd")) {
            listActions.push({
                buttonType: ButtonTypes.Brand,
                labelKey: "AddNew",
                action: () => { this.inventoryLaunchModalService.launchDataListItemModal(listId, this.inventoryListManagerService.getRows); },
            });
        }
        if (this.permissions.canShow("DataMappingInventoryElementExport")) {
            listActions.push({
                buttonType: ButtonTypes.Neutral,
                labelKey: "Export",
                action: () => { this.inventoryListManagerService.exportV2Report(); },
            });
        }
        if (listId === InventorySchemaIds.Elements && this.permissions.canShow("DataMappingManageDataElementVisibility")) {
            listActions.push({
                buttonType: ButtonTypes.Context,
                options: [{
                    id: "InventoryListManagerManageVisibility",
                    text: this.translatePipe.transform("ManageVisibility"),
                    action: () => { this.goToManageVisibility(); },
                }],
            });
        }
        return listActions;
    }

    contextMenuItemSelected(item: IHeaderOption) {
        item.action();
    }

    goToManageVisibility() {
        this.stateService.go(InventoryListManagerRoutes.ManageVisibility);
    }

    goToDetails(listItem: IInventoryListItem) {
        if (this.listId === InventoryListTypes.DataSubjectTypes) {
            this.stateService.go(InventoryListManagerRoutes.Details, { listId: this.listId, itemId: listItem.id });
        }
    }

    handleSearch(searchTerm: string) {
        this.currentSearchTerm = searchTerm;
    }
}
