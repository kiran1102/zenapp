import { Component } from "@angular/core";
import { StateService } from "@uirouter/core";

// Services
import { Permissions } from "modules/shared/services/helper/permissions.service";

// Enums
import { ContextMenuType } from "@onetrust/vitreus";
import {
    InventoryListTypes,
    InventoryListManagerRoutes,
    InventoryTranslationKeys,
    InventoryPermissions,
} from "constants/inventory.constant";
import { InventorySchemaIds } from "constants/inventory-config.constant";

const ManagerType = {
    Attribute: "attribute",
    List: "list",
};

// Interface
interface IManagerTile {
    labelKey: string;
    id: string;
    managerType: string;
}

@Component({
    selector: "inventory-list-manager-entry",
    templateUrl: "inventory-list-manager-entry.component.html",
})
export class InventoryListManagerEntryComponent {

    contextMenuType = ContextMenuType;
    listTypes: IManagerTile[] = [];

    constructor(
        private stateService: StateService,
        private permissions: Permissions,
    ) {}

    ngOnInit() {
        this.listTypes = this.getTiles();
    }

    goToList(list: IManagerTile) {
        if (list.managerType === ManagerType.List) {
            this.stateService.go(InventoryListManagerRoutes.List, { listId: list.id });
        } else if (list.managerType === ManagerType.Attribute) {
            this.stateService.go("zen.app.pia.module.datamap.views.attribute-manager.list", { id: list.id });
        }
    }

    getTiles() {
        const listManagerTiles: IManagerTile[] = [
            {
                id: InventoryListTypes.DataSubjectTypes,
                labelKey: InventoryTranslationKeys[InventoryListTypes.DataSubjectTypes].listTitle,
                managerType: ManagerType.List,
            },
            {
                id: InventoryListTypes.DataElements,
                labelKey: InventoryTranslationKeys[InventoryListTypes.DataElements].listTitle,
                managerType: ManagerType.List,
            },
            {
                id: InventoryListTypes.DataCategories,
                labelKey: InventoryTranslationKeys[InventoryListTypes.DataCategories].listTitle,
                managerType: ManagerType.List,
            },
            {
                id: InventoryListTypes.DataClassifications,
                labelKey: InventoryTranslationKeys[InventoryListTypes.DataClassifications].listTitle,
                managerType: ManagerType.List,
            },
        ];

        const attributeManagerTiles: IManagerTile[] = [
            {
                id: InventorySchemaIds.Assets,
                labelKey: InventoryTranslationKeys[InventorySchemaIds.Assets].attributes,
                managerType: ManagerType.Attribute,
            },
            {
                id: InventorySchemaIds.Processes,
                labelKey: InventoryTranslationKeys[InventorySchemaIds.Processes].attributes,
                managerType: ManagerType.Attribute ,
            },
        ];
        if (this.permissions.canShow("DataMappingEntitiesInventory")) {
            attributeManagerTiles.push({
                id: InventorySchemaIds.Entities,
                labelKey: InventoryTranslationKeys[InventorySchemaIds.Entities].attributes,
                managerType: ManagerType.Attribute,
            });
        }
        const filteredAttributeManagerTiles = attributeManagerTiles.filter((inventory: IManagerTile): boolean => this.permissions.canShow(InventoryPermissions[inventory.id].viewAttributes));

        return this.permissions.canShow("InventoryViewAttributes") ? [...filteredAttributeManagerTiles, ...listManagerTiles] : listManagerTiles;
    }
}
