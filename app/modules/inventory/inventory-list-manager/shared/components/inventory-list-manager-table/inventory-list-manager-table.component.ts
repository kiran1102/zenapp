import {
    Component,
    Input,
    Output,
    EventEmitter,
    OnDestroy,
} from "@angular/core";

// RXJS
import {
    takeUntil,
    filter,
 } from "rxjs/operators";
import { Subject } from "rxjs";

// Vitreus
import { ConfirmModalType, IConfirmModalConfig } from "@onetrust/vitreus";

// Interfaces
import { IDropdownOption } from "interfaces/one-dropdown.interface";
import {
    IInventoryListItem,
    IInventoryElementListItem,
    IInventoryTableColumn,
} from "interfaces/inventory.interface";
import { ILocalizationEditModalData } from "interfaces/localization.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";

// Consts + Enums
import { EmptyGuid } from "constants/empty-guid.constant";
import { CellTypes } from "enums/general.enum";
import {
    InventoryListTypes,
    InventoryListActionContext,
} from "constants/inventory.constant";
import { TranslationModules } from "constants/translation.constant";

// Services
import { InventoryService } from "inventorySharedModule/shared/services/inventory-api.service";
import { InventoryLaunchModalService } from "inventoryModule/services/action/inventory-launch-modal.service";
import { InventoryListManagerService } from "modules/inventory/inventory-list-manager/shared/services/inventory-list-manager-action.service";
import { NotificationService } from "sharedModules/services/provider/notification.service";
import { ModalService } from "sharedServices/modal.service";
import { Permissions } from "modules/shared/services/helper/permissions.service";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

// Vitreus
import { OtModalService } from "@onetrust/vitreus";

// Components
import { LocalizationEditModalV2 } from "settingsComponents/localization-edit-modal-v2/localization-edit-modal-v2.component";

// RxJs
import { take } from "rxjs/operators";

@Component({
    selector: "inventory-list-manager-table",
    templateUrl: "inventory-list-manager-table.component.html",
})
export class InventoryListManagerTableComponent implements OnDestroy {

    @Input() rows: IInventoryListItem[];
    @Input() columns: IInventoryTableColumn[];
    @Input() listId: string;
    @Input() associatedListId: string;
    @Input() actionsContext = InventoryListActionContext.List;
    @Input() allowRowActions = true;
    @Input() contextItem: IInventoryListItem;
    @Output() goToRoute = new EventEmitter<IInventoryListItem>();

    cellTypes = CellTypes;
    emptyGuid = EmptyGuid;
    menuPosition: string;
    rowActions: IDropdownOption[] = [];

    private destroy$ = new Subject();

    constructor(
        private inventoryService: InventoryService,
        private modalService: ModalService,
        private inventoryLaunchModalService: InventoryLaunchModalService,
        private inventoryListManagerService: InventoryListManagerService,
        private translatePipe: TranslatePipe,
        private notificationService: NotificationService,
        private permissions: Permissions,
        private otModalService: OtModalService,
    ) {}

    ngOnDestroy() {
        this.destroy$.next();
        this.destroy$.complete();
    }

    setMenuClass(event: MouseEvent) {
        this.menuPosition = "bottom-right";
        if ((window.innerHeight - 200) / 2 < (event.clientY - 200)) {
            this.menuPosition = "top-right";
        }
    }

    getActions(event: MouseEvent, listItem: IInventoryListItem | IInventoryElementListItem) {
        this.setMenuClass(event);
        if (!event) return;
        this.rowActions = this.getRowActions(this.listId, listItem);
    }

    columnTrackBy(index: number): number {
        return index;
    }

    getRowActions(listId: string, listItem: IInventoryListItem | IInventoryElementListItem): IDropdownOption[] {
        let actions: IDropdownOption[] = [];
        if (this.actionsContext === InventoryListActionContext.List) {
            actions = [...this.formatListRowActions(listId, listItem)];
        } else if (this.actionsContext === InventoryListActionContext.Details) {
            actions = [...this.formatAssociatedListRowActions(listId, listItem)];
        }
        if (!actions.length) {
            actions.push(this.getEmpyListAction());
        }
        return actions;
    }

    private formatListRowActions(listId: string, listItem: IInventoryListItem | IInventoryElementListItem): IDropdownOption[] {
        const actions: IDropdownOption[] = [];
        if (listId === InventoryListTypes.DataSubjectTypes) {
            actions.push({
                text: this.translatePipe.transform("ManageDataElements"),
                action: () => { this.inventoryLaunchModalService.launchManageDataElementsModal({listItem}); },
            });
        }
        if (this.permissions.canShow("DataMappingAttributesDataElementsAdd")) {
            actions.push({
                text: this.translatePipe.transform("Edit"),
                action: () => { this.inventoryLaunchModalService.launchDataListItemModal(listId, this.inventoryListManagerService.getRows, listItem); },
            });
        }
        if (this.permissions.canShow("LocalizationEditor") && this.permissions.canShow("LocalizationEditorEdit")) {
            actions.push({
                text: this.translatePipe.transform("TranslateName"),
                action: () => { this.launchTranslateModal(listItem.nameKey); },
            });
            if (listItem.description && listItem.descriptionKey) {
                actions.push({
                    text: this.translatePipe.transform("TranslateDescription"),
                    action: () => { this.launchTranslateModal(listItem.descriptionKey); },
                });
            }
        }
        if (this.permissions.canShow("DataMappingAttributesDataElementsDelete")) {
            const deletePromiseToResolve = (): Promise<boolean> =>
                this.inventoryService.deleteListItem(listId, listItem.id).toPromise()
                    .then((response: IProtocolResponse<null>): boolean => {
                        if (response.result) {
                            this.inventoryListManagerService.initializeList(listId);
                        }
                        return response.result;
                    },
                );
            const confirmationText = this.translatePipe.transform(
                "DeleteListItemWarning",
                { ListItemName: listItem.nameKey ? this.translatePipe.transform(listItem.nameKey) : listItem.name},
            );
            actions.push({
                text: this.translatePipe.transform("Delete"),
                action: () => { this.launchDeleteModal(this.translatePipe.transform("Delete"), this.translatePipe.transform("Delete"), confirmationText, deletePromiseToResolve); },
            });
        }
        return actions;
    }

    private formatAssociatedListRowActions(listId: string, listItem: IInventoryListItem | IInventoryElementListItem): IDropdownOption[] {
        const actions: IDropdownOption[] = [];
        const deletePromiseToResolve = (): ng.IPromise<boolean> =>
            this.inventoryService.removeAssociatedListItems(listId, this.contextItem.id, this.associatedListId, [{ id: listItem.id }])
                .then((response: IProtocolResponse<null>): boolean => {
                    if (response.result) {
                        this.inventoryListManagerService.initializeList(listId, this.contextItem.id, this.associatedListId);
                        this.notificationService.alertSuccess(
                            this.translatePipe.transform("Success"),
                            this.translatePipe.transform("UpdatedRecordSuccessfully"),
                        );
                    }
                    return response.result;
                },
            );
        const confirmationText = this.translatePipe.transform(
            "DeleteDataElementAssociationWarning",
            { ListItemName: this.contextItem && this.contextItem.nameKey ? this.translatePipe.transform(this.contextItem.nameKey) : this.contextItem.name || ""},
        );
        actions.push({
            text: this.translatePipe.transform("Remove"),
            action: () => { this.launchDeleteModal(this.translatePipe.transform("Remove"), this.translatePipe.transform("Remove"), confirmationText, deletePromiseToResolve); },
        });
        return actions;
    }

    private launchDeleteModal(title: string, submit: string, confirm: string, deletePromiseToResolve: () => void) {
        const modalData: IConfirmModalConfig = {
            type: ConfirmModalType.DELETE,
            translations: {
                title,
                desc: "",
                confirm,
                submit,
                cancel: this.translatePipe.transform("Cancel"),
            },
            hideClose: true,
        };
        this.inventoryLaunchModalService.launchConfirmationModal(modalData)
            .pipe(
                takeUntil(this.destroy$),
                filter((result: boolean) => result))
            .subscribe(() => {
                deletePromiseToResolve();
            },
        );
    }

    private launchTranslateModal(termKey: string) {
        const modalData: ILocalizationEditModalData = {
            termKey,
            termIndex: 0,
            lastTermIndex: 0,
            tabId: TranslationModules.Inventory,
        };
        this.otModalService
            .create(
                LocalizationEditModalV2,
                {
                    isComponent: true,
                },
                modalData,
            ).pipe(
                take(1),
            ).subscribe(() => {
                this.inventoryListManagerService.initializeList(this.listId);
            });
    }

    private getEmpyListAction(): IDropdownOption {
        return {
            text: this.translatePipe.transform("NoActionsAvailable"),
            action: () => { }, // Keep Empty
        };
    }

}
