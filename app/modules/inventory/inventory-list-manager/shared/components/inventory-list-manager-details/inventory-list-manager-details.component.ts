import { Component, OnInit } from "@angular/core";
import { StateService } from "@uirouter/core";

// Services
import { InventoryService } from "inventorySharedModule/shared/services/inventory-api.service";
import { Permissions } from "modules/shared/services/helper/permissions.service";
import { InventoryListManagerService } from "modules/inventory/inventory-list-manager/shared/services/inventory-list-manager-action.service";
import { InventoryLaunchModalService } from "inventoryModule/services/action/inventory-launch-modal.service";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

// Const
import {
    InventoryTranslationKeys,
    InventoryListTypes,
    InventoryListDetailsTabs,
    InventoryListManagerRoutes,
    InventoryListActionContext,
    ButtonTypes,
} from "constants/inventory.constant";

// Intefaces
import {
    IInventoryListItem,
    IInventoryElementListItem,
    IInventoryTableColumn,
    IInventoryListAction,
} from "interfaces/inventory.interface";
import { IStringMap } from "interfaces/generic.interface";
import { ITabsNav } from "interfaces/one-tabs-nav.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";

@Component({
    selector: "inventory-list-manager-details",
    templateUrl: "inventory-list-manager-details.component.html",
})
export class InventoryListManagerDetailsComponent implements OnInit {

    inventoryListTypes = InventoryListTypes;
    buttonTypes = ButtonTypes;
    inventoryListManagerRoutes = InventoryListManagerRoutes;
    inventoryListActionContext = InventoryListActionContext;
    listId: string;
    itemId: string;
    viewReady = false;
    currentTab: string;
    listItem: IInventoryListItem | IInventoryElementListItem;
    listActions: IInventoryListAction[] = [];
    listTitle: string;
    itemTitle: string;
    associatedListRows: IInventoryListItem[] | IInventoryElementListItem[];
    associatedListColumns: IInventoryTableColumn[];
    tabIds: IStringMap<string> = InventoryListDetailsTabs;
    tabs: ITabsNav[];

    constructor(
        private stateService: StateService,
        private inventoryService: InventoryService,
        private translatePipe: TranslatePipe,
        private permissions: Permissions,
        private inventoryLaunchModalService: InventoryLaunchModalService,
        readonly inventoryListManagerService: InventoryListManagerService,
    ) { }

    ngOnInit() {
        this.inventoryListManagerService.setDefaultState();
        this.listId = this.stateService.params.listId;
        this.itemId = this.stateService.params.itemId;
        this.listTitle = InventoryTranslationKeys[this.listId].listTitle;
        this.currentTab = this.stateService.params.tabId || this.tabIds.Details;
        this.tabs = this.getTabs();
        if (this.listId === InventoryListTypes.DataSubjectTypes) {
            this.inventoryListManagerService.initializeList(this.listId, this.itemId, InventoryListTypes.DataElements);
        }
        this.inventoryService.getListItem(this.listId, this.itemId).then((response: IProtocolResponse<{data: IInventoryListItem | IInventoryElementListItem}>) => {
            if (response.result) {
                this.listItem = response.data.data;
                this.listActions = this.getListActions(this.listId);
            }
            this.viewReady = true;
        });
    }

    goToListManager(route: { path: string }) {
        if (route.path === InventoryListManagerRoutes.Entry) {
            this.stateService.go(route.path);
        } else {
            this.stateService.go(route.path, { listId: this.listId });
        }
    }

    goToDetails(listItem: IInventoryListItem) {
        if (this.listId === InventoryListTypes.DataSubjectTypes) {
            this.stateService.go(InventoryListManagerRoutes.Details, { listId: this.listId, itemId: listItem.id });
        }
    }

    getTabs() {
        let tabs: ITabsNav[] = [];
        if (this.listId === InventoryListTypes.DataSubjectTypes) {
            tabs = [
                {
                    id: this.tabIds.List,
                    text: this.translatePipe.transform("DataElements"),
                    action: (option: ITabsNav) => { this.goToTab(option.id); },
                },
                {
                    id: this.tabIds.Details,
                    text: this.translatePipe.transform("Details"),
                    action: (option: ITabsNav) => { this.goToTab(option.id); },
                },
            ];
        }
        return tabs;
    }

    getListActions(listId: string) {
        const listActions: IInventoryListAction[] = [];
        if (listId === InventoryListTypes.DataSubjectTypes) {
            const modalData: { listItem: IInventoryListItem, callback: () => void } = {
                listItem: this.listItem,
                callback: () => { this.inventoryListManagerService.initializeList(this.listId, this.itemId, InventoryListTypes.DataElements); },
            };
            listActions.push({
                buttonType: ButtonTypes.Neutral,
                labelKey: "ManageDataElements",
                action: () => { this.inventoryLaunchModalService.launchManageDataElementsModal(modalData); },
            });
        }
        if (this.permissions.canShow("DataMappingInventoryElementExport")) {
            listActions.push({
                buttonType: ButtonTypes.Neutral,
                labelKey: "Export",
                action: () => { this.inventoryListManagerService.exportV2Report(); },
            });
        }
        return listActions;
    }

    private goToTab(tabId: string) {
        const routeParams: { listId: string, itemId: string, tabId: string } = {
            listId: this.listId,
            itemId: this.itemId,
            tabId,
        };
        this.stateService.go(".", routeParams);
        this.currentTab = tabId;
    }
}
