import { Injectable } from "@angular/core";

// Rxjs
import {
    BehaviorSubject,
    Observable,
} from "rxjs";

// Services
import { InventoryService } from "inventorySharedModule/shared/services/inventory-api.service";
import { InventoryAdapterV2Service } from "inventoryModule/services/adapter/inventory-adapter-v2.service";
import { InventoryLaunchModalService } from "inventoryModule/services/action/inventory-launch-modal.service";

// Consts + Enums
import { InventoryListTypes } from "constants/inventory.constant";
import { InventorySchemaIds } from "constants/inventory-config.constant";
import { CellTypes } from "enums/general.enum";

// Interfaces
import { IProtocolResponse } from "interfaces/protocol.interface";
import {
    IInventoryListManagerViewState,
    IInventoryTableColumn,
    IInventoryListItem,
    IInventoryElementListItem,
} from "interfaces/inventory.interface";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

@Injectable()
export class InventoryListManagerService {

    readonly inventoryListManagerViewState$: Observable<IInventoryListManagerViewState>;
    private defaultState: IInventoryListManagerViewState = {
        rows: [],
        columns: [],
        listId: "",
        associatedListId: "",
        listReady: false,
    };

    private _inventoryListManagerViewState: BehaviorSubject<IInventoryListManagerViewState> = new BehaviorSubject({...this.defaultState});

    constructor(
        private inventoryService: InventoryService,
        private inventoryAdapterV2Service: InventoryAdapterV2Service,
        private inventoryLaunchModalService: InventoryLaunchModalService,
        private translatePipe: TranslatePipe,
    ) {
        this.inventoryListManagerViewState$ = this._inventoryListManagerViewState.asObservable();
     }

    setDefaultState() {
        this.updateState({...this.defaultState});
    }

    updateState(state: {}) {
        const currentState = this._inventoryListManagerViewState.getValue();
        this._inventoryListManagerViewState.next({
            ...currentState,
            ...state,
        });
    }

    initializeList(listType: string, itemId?: string, associatedListType?: string) {
        this.getColumns(listType, associatedListType);
        this.getRows(listType, itemId, associatedListType);
    }

    getRows = (listType: string, itemId?: string, associatedListType?: string) => {
        this.updateState({listReady: false});
        if (itemId && associatedListType) {
            this.inventoryService.getAssociatedListItems(listType, itemId, associatedListType).then((response: IProtocolResponse<{ data: IInventoryListItem[] | IInventoryElementListItem[] }>) => {
                this.handleRowResponse(listType, associatedListType, response);
            });
        } else {
            this.inventoryService.getListData(listType).then((response: IProtocolResponse<{ data: IInventoryListItem[] | IInventoryElementListItem[] }>) => {
                this.handleRowResponse(listType, associatedListType, response);
            });
        }
    }

    handleRowResponse(listType: string, associatedListType: string, response: IProtocolResponse<{ data: IInventoryListItem[] | IInventoryElementListItem[] }>) {
        if (response.result) {
            this.updateState(
                {
                    listId: listType,
                    associatedListId: associatedListType,
                    rows: associatedListType === InventoryListTypes.DataElements || listType === InventoryListTypes.DataElements ?
                        this.inventoryAdapterV2Service.formatDataElementsForList(response.data.data as IInventoryElementListItem[], associatedListType === InventoryListTypes.DataElements) :
                        response.data.data,
                    listReady: true,
                },
            );
        } else {
            this.updateState({listReady: true});
        }
    }

    getColumns(listId: string, associatedListType?: string) {
        let columns: IInventoryTableColumn[] = [];
        switch (listId) {
            case InventoryListTypes.DataSubjectTypes:
                columns = [
                    { label: this.translatePipe.transform("Key"), dataKey: "name", type: CellTypes.link, tooltipKey: "InventoryManagerKeyColumnDescription" },
                    { label: this.translatePipe.transform("Name"), rowTranslationKey: "nameKey", type: CellTypes.text },
                    { label: this.translatePipe.transform("Description"), rowTranslationKey: "descriptionKey", type: CellTypes.text },
                ];
                break;
            case InventoryListTypes.DataCategories:
                columns = [
                    { label: this.translatePipe.transform("Key"), dataKey: "name", type: CellTypes.text, tooltipKey: "InventoryManagerKeyColumnDescription" },
                    { label: this.translatePipe.transform("Name"), rowTranslationKey: "nameKey", type: CellTypes.text },
                    { label: this.translatePipe.transform("Description"), rowTranslationKey: "descriptionKey", type: CellTypes.text },
                ];
                break;
            case InventoryListTypes.DataClassifications:
                columns = [
                    { label: this.translatePipe.transform("Key"), dataKey: "name", type: CellTypes.text, tooltipKey: "InventoryManagerKeyColumnDescription" },
                    { label: this.translatePipe.transform("Name"), rowTranslationKey: "nameKey", type: CellTypes.text },
                    { label: this.translatePipe.transform("Description"), rowTranslationKey: "descriptionKey", type: CellTypes.text },
                ];
                break;
        }
        if (associatedListType === InventoryListTypes.DataElements || listId === InventoryListTypes.DataElements) {
            columns = [
                { label: this.translatePipe.transform("DataElement"), rowTranslationKey: "nameKey", type: CellTypes.text },
                { label: this.translatePipe.transform("Description"), rowTranslationKey: "descriptionKey", type: CellTypes.text },
                { label: this.translatePipe.transform("DataCategory"), dataKey: "categoryNames", type: CellTypes.text },
                { label: this.translatePipe.transform("DataClassification"), dataKey: "classificationNames", type: CellTypes.text },
            ];
        }
        if (listId === InventoryListTypes.DataElements) {
            columns.splice(0, 1, { label: this.translatePipe.transform("Key"), dataKey: "name", type: CellTypes.text, tooltipKey: "InventoryManagerKeyColumnDescription" });
            columns.splice(1, 0, { label: this.translatePipe.transform("Name"), rowTranslationKey: "nameKey", type: CellTypes.text });
        }
        this.updateState({columns});
    }

    exportV2Report() {
        this.inventoryService.exportV2Report(InventorySchemaIds.Elements).then((response) => {
            if (!response.result) return;
            this.inventoryLaunchModalService.launchNotificationModal(
                this.translatePipe.transform("ReportDownload"),
                this.translatePipe.transform("ReportIsGeneratedCheckTasksBar"),
            );
        });
    }
}
