// Angular
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";
import { HttpClientModule } from "@angular/common/http";

// Modules
import { InventorySharedModule } from "inventorySharedModule/inventory-shared.module";
import {
    OtLoadingModule,
    OtButtonModule,
    OtSearchModule,
    OtContextMenuModule,
    OtBreadcrumbModule,
    OtPageHeaderModule,
    OtEmptyStateModule,
    OtInputModule,
    OtDataTableModule,
    OtTooltipModule,
    OtTabsModule,
} from "@onetrust/vitreus";
import { TranslateModule } from "@ngx-translate/core";
import { OTPipesModule, PIPES } from "modules/pipes/pipes.module";

// Components
import { InventoryListManagerEntryComponent } from "inventoryModule/inventory-list-manager/shared/components/inventory-list-manager-entry/inventory-list-manager-entry.component";
import { InventoryListManagerComponent } from "inventoryModule/inventory-list-manager/shared/components/inventory-list-manager/inventory-list-manager.component";
import { InventoryListManagerTableComponent } from "inventoryModule/inventory-list-manager/shared/components/inventory-list-manager-table/inventory-list-manager-table.component";
import { InventoryListManagerDetailsComponent } from "inventoryModule/inventory-list-manager/shared/components/inventory-list-manager-details/inventory-list-manager-details.component";

@NgModule({
    imports: [
        CommonModule,
        InventorySharedModule,
        FormsModule,
        HttpClientModule,
        OtInputModule,
        OtLoadingModule,
        OtButtonModule,
        OtSearchModule,
        OtContextMenuModule,
        OtBreadcrumbModule,
        OtPageHeaderModule,
        OtEmptyStateModule,
        OtDataTableModule,
        OtTabsModule,
        OtTooltipModule,
        TranslateModule,
        OTPipesModule,
    ],
    exports: [
    ],
    declarations: [
        InventoryListManagerEntryComponent,
        InventoryListManagerComponent,
        InventoryListManagerTableComponent,
        InventoryListManagerDetailsComponent,
    ],
    entryComponents: [
        InventoryListManagerEntryComponent,
        InventoryListManagerComponent,
        InventoryListManagerDetailsComponent,
    ],
    providers: [
        PIPES,
    ],
})
export class InventoryListManagerModule {}
