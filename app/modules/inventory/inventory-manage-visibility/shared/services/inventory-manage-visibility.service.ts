import { Injectable } from "@angular/core";

// Rxjs
import {
    BehaviorSubject,
    Observable,
} from "rxjs";

// Interfaces
import {
    IInventoryManageVisibilityViewState,
    IInventoryManageVisibilityState,
    IInventoryManageElementVisibilityPayload,
} from "inventoryModule/inventory-manage-visibility/shared/interfaces/inventory-manage-visibility.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";
import { IStringMap } from "interfaces/generic.interface";

// Services
import { InventoryService } from "inventorySharedModule/shared/services/inventory-api.service";

@Injectable()
export class InventoryManageVisibilityService {

    readonly inventoryManageVisibilityViewState$: Observable<IInventoryManageVisibilityViewState>;
    readonly inventoryManageVisibilityState$: Observable<IInventoryManageVisibilityState>;
    readonly inventoryManageVisibilityInheritedState$: Observable<IInventoryManageVisibilityState>;
    private defaultViewState: IInventoryManageVisibilityViewState = {
        saveEnabled: false,
        saving: false,
        loading: false,
    };
    private defaultState: IInventoryManageVisibilityState = {
        autoAddElements : false,
        override : false,
        categories: [],
    };
    private _inventoryManageVisibilityViewState: BehaviorSubject<IInventoryManageVisibilityViewState> = new BehaviorSubject({...this.defaultViewState});
    private _inventoryManageVisibilityState: BehaviorSubject<IInventoryManageVisibilityState> = new BehaviorSubject({...this.defaultState});
    private _inventoryManageVisibilityInheritedState: BehaviorSubject<IInventoryManageVisibilityState> = new BehaviorSubject({...this.defaultState});

    constructor(private inventoryService: InventoryService) {
        this.inventoryManageVisibilityViewState$ = this._inventoryManageVisibilityViewState.asObservable();
        this.inventoryManageVisibilityState$ = this._inventoryManageVisibilityState.asObservable();
        this.inventoryManageVisibilityInheritedState$ = this._inventoryManageVisibilityInheritedState.asObservable();
    }

    setDefaultState() {
        this.updateViewState({...this.defaultViewState});
        this.updateState({...this.defaultState});
        this.updateInheritedState({...this.defaultState});
    }

    updateViewState(state: {}) {
        const currentState = this._inventoryManageVisibilityViewState.getValue();
        this._inventoryManageVisibilityViewState.next({
            ...currentState,
            ...state,
        });
    }

    updateState(state: {}) {
        const currentState = this._inventoryManageVisibilityState.getValue();
        this._inventoryManageVisibilityState.next({
            ...currentState,
            ...state,
        });
    }

    updateInheritedState(state: {}) {
        const currentState = this._inventoryManageVisibilityInheritedState.getValue();
        this._inventoryManageVisibilityInheritedState.next({
            ...currentState,
            ...state,
        });
    }

    getElementVisibilityConfig(orgId: string) {
        this.updateViewState({loading: true});
        this.inventoryService
            .getElementVisibilityConfig(orgId)
            .subscribe((res: IProtocolResponse<{ data: IInventoryManageVisibilityState }>) => {
                if (res.result) {
                    this.updateState(res.data.data);
                    if (res.data.data.override) {
                        this.getElementInheritedVisibilityConfig(orgId);
                    }
                } else {
                    this.updateState({...this.defaultState});
                }
            },
        );
    }

    getElementInheritedVisibilityConfig(orgId: string) {
        this.inventoryService
            .getInheritedElementVisibilityConfig(orgId)
            .subscribe((res: IProtocolResponse<{ data: IInventoryManageVisibilityState }>) => {
                if (res.result) {
                    this.updateInheritedState(res.data.data);
                } else {
                    this.updateInheritedState({...this.defaultState});
                }
            },
        );
    }

    saveElementVisibilityConfig(orgId: string, payload: IInventoryManageElementVisibilityPayload) {
        this.updateViewState({saving: true});
        this.inventoryService
            .saveElementVisibilityConfig(orgId, payload)
            .subscribe(() => {
                this.updateViewState({saveEnabled: false, saving: false});
                this.getElementVisibilityConfig(orgId);
            },
        );
    }

    removeElementVisibilityConfig(orgId: string) {
        this.updateViewState({saving: true});
        this.inventoryService
            .removeElementVisibilityConfig(orgId)
            .subscribe(() => {
                this.updateViewState({saveEnabled: false, saving: false});
                this.getElementVisibilityConfig(orgId);
            },
        );
    }

    formatPayloadForSave(elementMap: IStringMap<IStringMap<boolean>>, autoAddElements: boolean) {
        const payload: IInventoryManageElementVisibilityPayload = {
            autoAddElements,
            elements: [],
        };
        Object.keys(elementMap).forEach((categoryId) => {
            Object.keys(elementMap[categoryId]).forEach((elementId) => {
                if (elementMap[categoryId][elementId]) payload.elements.push({id: elementId});
            });
        });
        return payload;
    }
}
