import {
    Component,
    OnInit,
    OnDestroy,
    Inject,
} from "@angular/core";

// Rxjs
import { takeUntil } from "rxjs/operators";
import { Subject } from "rxjs";

// Lodash
import { cloneDeep } from "lodash";

// Reducers
import {
    getCurrentOrgTree,
    getOrgById,
} from "oneRedux/reducers/org.reducer";

// Tokens
import { StoreToken } from "tokens/redux-store.token";

// Services
import { InventoryManageVisibilityService } from "inventoryModule/inventory-manage-visibility/shared/services/inventory-manage-visibility.service";
import { InventoryLaunchModalService } from "inventoryModule/services/action/inventory-launch-modal.service";

// Interfaces
import { IStore } from "interfaces/redux.interface";
import {
    IFormattedListItem,
    IInventoryListItem,
} from "interfaces/inventory.interface";
import {
    IInventoryManageVisibilityState,
    IInventoryManageVisibilityViewState,
} from "inventoryModule/inventory-manage-visibility/shared/interfaces/inventory-manage-visibility.interface";
import { IStringMap } from "interfaces/generic.interface";
import { IOrganization } from "interfaces/org.interface";
import {
    ConfirmModalType,
    IConfirmModalConfig,
} from "@onetrust/vitreus";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

@Component({
    selector: "inventory-manage-visibility",
    templateUrl: "inventory-manage-visibility.component.html",
})
export class InventoryManageVisibilityComponent implements OnInit, OnDestroy {

    selectedOrgId: string = null;
    selectedOrg: IOrganization;
    orgSearchTerm = "";
    elementSearchTerm = "";
    override: boolean;
    initialOverride: boolean;
    loading = false;
    saving = false;
    saveEnabled = false;
    autoAddElements: boolean;
    initialAutoAddElements: boolean;
    inheritedAutoAddElements: boolean;
    elementMap: IStringMap<IStringMap<boolean>> = {};
    initialElementMap: IStringMap<IStringMap<boolean>> = {};
    inheritedElementMap: IStringMap<IStringMap<boolean>> = {};
    dataCategoryList: IFormattedListItem[] = [];
    inheritedDataCategoryList: IFormattedListItem[] = [];
    initialDataCategoryList: IFormattedListItem[] = [];
    filteredCategoryList: IFormattedListItem[] = [];
    private destroy$ = new Subject();

    constructor(
        @Inject(StoreToken) private store: IStore,
        private inventoryManageVisibilityService: InventoryManageVisibilityService,
        private inventoryLaunchModalService: InventoryLaunchModalService,
        private translatePipe: TranslatePipe,
    ) { }

    ngOnInit() {
        this.inventoryManageVisibilityService.inventoryManageVisibilityState$
            .pipe(takeUntil(this.destroy$))
            .subscribe((state: IInventoryManageVisibilityState) => {
                this.override = state.override;
                this.initialOverride = this.override;
                this.autoAddElements = state.autoAddElements;
                this.initialAutoAddElements = this.autoAddElements;
                this.elementMap = this.setElementMapForCategory(state.categories);
                this.initialElementMap = cloneDeep(this.elementMap);
                this.dataCategoryList = this.setformattedCategoryList(state.categories, this.elementMap);
                this.initialDataCategoryList = [...this.dataCategoryList];
                this.filteredCategoryList = [...this.dataCategoryList];
                this.setSelectionStatus(this.elementMap);
                if (!this.initialOverride) {
                    this.inheritedAutoAddElements = this.initialAutoAddElements;
                    this.inheritedElementMap = cloneDeep(this.initialElementMap);
                    this.inheritedDataCategoryList = this.setformattedCategoryList(state.categories, this.inheritedElementMap);
                    this.inventoryManageVisibilityService.updateViewState({loading: false});
                }
            },
        );
        this.inventoryManageVisibilityService.inventoryManageVisibilityInheritedState$
            .pipe(takeUntil(this.destroy$))
            .subscribe((state: IInventoryManageVisibilityState) => {
                this.inheritedAutoAddElements = state.autoAddElements;
                this.inheritedElementMap = this.setElementMapForCategory(state.categories);
                this.inheritedDataCategoryList = this.setformattedCategoryList(state.categories, this.inheritedElementMap);
                if (this.initialOverride) {
                    this.inventoryManageVisibilityService.updateViewState({loading: false});
                }
            },
        );
        this.inventoryManageVisibilityService.inventoryManageVisibilityViewState$
            .pipe(takeUntil(this.destroy$))
            .subscribe((viewState: IInventoryManageVisibilityViewState) => {
                this.loading = viewState.loading;
                this.saving = viewState.saving;
                this.saveEnabled = viewState.saveEnabled;
            },
        );
    }

    ngOnDestroy(): void {
        this.destroy$.next();
        this.destroy$.complete();
    }

    trackById(index: number, item: IFormattedListItem | IInventoryListItem) {
        return item.id;
    }

    selectOrg(selection: string) {
        this.selectedOrgId = selection;
        this.selectedOrg = getOrgById(selection)(this.store.getState());
        if (this.selectedOrgId) {
            this.inventoryManageVisibilityService.getElementVisibilityConfig(this.selectedOrgId);
        } else {
            this.inventoryManageVisibilityService.setDefaultState();
        }
    }

    resetConfig() {
        this.elementMap = cloneDeep(this.initialElementMap);
        this.dataCategoryList = [...this.initialDataCategoryList];
        this.filteredCategoryList = [...this.dataCategoryList];
        this.override = this.initialOverride;
        this.autoAddElements = this.initialAutoAddElements;
        this.elementSearchTerm = "";
        this.handleElementSearch(this.elementSearchTerm);
        this.inventoryManageVisibilityService.updateViewState({saveEnabled: false});
    }

    handleElementSearch(searchTerm: string) {
        this.filteredCategoryList = this.override ? cloneDeep(this.dataCategoryList) : cloneDeep(this.inheritedDataCategoryList);
        this.elementSearchTerm = searchTerm;
        if (searchTerm) {
            const filteredArray: IFormattedListItem[] = [];
            const term: string = searchTerm.toLocaleLowerCase();
            this.filteredCategoryList.forEach((category: IFormattedListItem) => {
                const categoryHasMatch: boolean = category.name.toLocaleLowerCase().indexOf(term) !== -1;
                const elementsMatch: IInventoryListItem[] = category.dataElements.filter((elements: IInventoryListItem): boolean => {
                    return elements.name.toLocaleLowerCase().indexOf(term) !== -1;
                });
                if (categoryHasMatch || elementsMatch.length) {
                    if (elementsMatch.length && !categoryHasMatch) {
                        category.dataElements = elementsMatch;
                    }
                    filteredArray.push(category);
                }
            });
            this.filteredCategoryList = filteredArray;
        }
        this.setSelectionStatus(this.override ? this.elementMap : this.inheritedElementMap);
    }

    toggleOverride(toggled: boolean) {
        this.override = toggled;
        this.handleElementSearch("");
        if (!toggled && this.initialOverride) {
            setTimeout(() => {
                const modalData: IConfirmModalConfig = {
                    type: ConfirmModalType.WARNING,
                    translations: {
                        title: this.translatePipe.transform("ResetDependency"),
                        desc: this.translatePipe.transform("ResetDependencyDescription"),
                        confirm: this.translatePipe.transform("AreYouSureContinue"),
                        submit: this.translatePipe.transform("Reset"),
                        cancel: this.translatePipe.transform("Cancel"),
                    },
                    hideClose: true,
                };
                this.inventoryLaunchModalService.launchConfirmationModal(modalData)
                    .pipe(takeUntil(this.destroy$))
                    .subscribe((result: boolean) => {
                        this.override = !result;
                        this.setSaveEnabled();
                    },
                );
            }, 0);
        } else {
            this.setSaveEnabled();
        }
    }

    toggleElement(currentCategoryId: string, currentElementId: string) {
        if (this.override && !this.saving) {
            this.elementMap[currentCategoryId][currentElementId] = !this.elementMap[currentCategoryId][currentElementId];
            Object.keys(this.elementMap).forEach((categoryId: string) => {
                if (this.elementMap[categoryId].hasOwnProperty(currentElementId)) {
                    this.elementMap[categoryId][currentElementId] = this.elementMap[currentCategoryId][currentElementId];
                }
            });
            this.setSelectionStatus(this.elementMap);
            this.setSaveEnabled();
        }
    }

    toggleAutoAddElements(toggled: boolean) {
        this.autoAddElements = toggled;
        this.setSaveEnabled();
    }

    selectAllCategoryElements(currentCategory: IFormattedListItem) {
        const selectedCategory = this.filteredCategoryList.find((categoryItem) => categoryItem.id === currentCategory.id);
        const checkStatus: boolean = !selectedCategory.allSelected;
        const currentElements: string[] = selectedCategory.dataElements.map((element) => element.id);
        currentCategory.dataElements.forEach((element: IInventoryListItem) => {
            this.elementMap[currentCategory.id][element.id] = checkStatus;
        });
        Object.keys(this.elementMap).forEach((category) => {
            const elements = Object.keys(this.elementMap[category]);
            if (currentElements.some((element) => elements.indexOf(element) >= 0)) {
                Object.keys(this.elementMap[category]).forEach((element) => {
                    if (currentElements.indexOf(element) >= 0) {
                        this.elementMap[category][element] = checkStatus;
                    }
                });
            }
        });
        this.setSelectionStatus(this.elementMap);
        this.setSaveEnabled();
    }

    private setElementMapForCategory(categoryList: IInventoryListItem[]): IStringMap<IStringMap<boolean>> {
        const elementMap: IStringMap<IStringMap<boolean>> = {};
        categoryList.forEach((category) => {
            category.dataElements.forEach((element) => {
                if (!elementMap.hasOwnProperty(category.id)) elementMap[category.id] = {};
                elementMap[category.id][element.id] = element.selected;
            });
        });
        return elementMap;
    }

    private setformattedCategoryList(categoryList: IInventoryListItem[], elementMap: IStringMap<IStringMap<boolean>>): IFormattedListItem[] {
        const formattedList: IFormattedListItem[] = categoryList.map((category) => {
            category.name = category.nameKey ? this.translatePipe.transform(category.nameKey) : category.name;
            (category as IFormattedListItem).allSelected = !Boolean(Object.values(elementMap[category.id]).includes(false));
            category.dataElements.forEach((element) => {
                element.name = element.nameKey ? this.translatePipe.transform(element.nameKey) : element.name;
                element.description = element.descriptionKey ? this.translatePipe.transform(element.descriptionKey) : element.description;
            });
            return category;
        });
        return formattedList;
    }

    private setSelectionStatus(elementMap: IStringMap<IStringMap<boolean>>) {
        this.filteredCategoryList.forEach((category) => {
            const elementSelectionStatus: boolean[] = [];
            category.dataElements.forEach((element) => {
                elementSelectionStatus.push(elementMap[category.id][element.id]);
            });
            category.hasSelections = elementSelectionStatus.includes(true);
            category.allSelected = !elementSelectionStatus.includes(false);
        });
    }

    private setSaveEnabled() {
        const enabled =
            ((JSON.stringify(this.elementMap) !== JSON.stringify(this.initialElementMap) ||
            this.autoAddElements !== this.initialAutoAddElements) && this.override) ||
            this.override !== this.initialOverride;
        this.inventoryManageVisibilityService.updateViewState({saveEnabled: enabled});
    }
}
