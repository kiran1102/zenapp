// Angular
import {
    Component,
    Inject,
    OnInit,
    ViewChild,
} from "@angular/core";
import {
    Transition,
    HookResult,
    TransitionService,
    StateService,
    StateOrName,
    RawParams,
    TransitionOptions,
} from "@uirouter/core";

// Rxjs
import {
    takeUntil,
    filter,
} from "rxjs/operators";
import { Subject } from "rxjs";

// Const
import { InventoryListManagerRoutes } from "constants/inventory.constant";
import { TranslationModules } from "constants/translation.constant";
import {
    ConfirmModalType,
    IConfirmModalConfig,
} from "@onetrust/vitreus";

// Services
import { TenantTranslationService } from "sharedServices/tenant-translation.service";
import { InventoryManageVisibilityService } from "inventoryModule/inventory-manage-visibility/shared/services/inventory-manage-visibility.service";
import { InventoryLaunchModalService } from "inventoryModule/services/action/inventory-launch-modal.service";

// Components
import { InventoryManageVisibilityComponent } from "inventoryModule/inventory-manage-visibility/shared/components/inventory-manage-visibility/inventory-manage-visibility.component";

// Interfaces
import { IInventoryManageVisibilityViewState } from "inventoryModule/inventory-manage-visibility/shared/interfaces/inventory-manage-visibility.interface";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

@Component({
    selector: "inventory-manage-visibility-wrapper",
    templateUrl: "inventory-manage-visibility-wrapper.component.html",
})
export class InventoryManageVisibilityWrapperComponent implements OnInit {

    @ViewChild(InventoryManageVisibilityComponent) inventoryManageVisibilityComponent: InventoryManageVisibilityComponent;
    inventoryListManagerRoutes = InventoryListManagerRoutes;
    saveEnabled = false;
    saving = false;
    ready = false;
    allowTransition = false;

    private destroy$ = new Subject();
    private transitionDeregisterHook = this.$transitions.onExit({ from: this.stateService.current.name }, (transition: Transition): HookResult => {
        return this.handleStateChange(transition.to(), transition.params("to"), transition.options());
    });

    constructor(
        @Inject("TenantTranslationService") private tenantTranslationService: TenantTranslationService,
        private $transitions: TransitionService,
        private stateService: StateService,
        private translatePipe: TranslatePipe,
        private inventoryManageVisibilityService: InventoryManageVisibilityService,
        private inventoryLaunchModalService: InventoryLaunchModalService,
    ) { }

    ngOnInit() {
        this.inventoryManageVisibilityService.setDefaultState();
        this.inventoryManageVisibilityService.inventoryManageVisibilityViewState$
            .pipe(takeUntil(this.destroy$))
            .subscribe((viewState: IInventoryManageVisibilityViewState) => {
                this.saving = viewState.saving;
                this.saveEnabled = viewState.saveEnabled;
            },
        );
        if (!this.tenantTranslationService.loadedModuleTranslations[TranslationModules.Inventory]) {
            this.tenantTranslationService.addModuleTranslations(TranslationModules.Inventory).then(() => {
                this.ready = true;
            });
        } else {
            this.ready = true;
        }
    }

    ngOnDestroy(): void {
        if (this.transitionDeregisterHook) {
            this.transitionDeregisterHook();
        }
        this.destroy$.next();
        this.destroy$.complete();
    }

    saveSettings() {
        const orgId = this.inventoryManageVisibilityComponent.selectedOrgId;
        if (this.inventoryManageVisibilityComponent.initialOverride && !this.inventoryManageVisibilityComponent.override) {
            this.inventoryManageVisibilityService.removeElementVisibilityConfig(orgId);
        } else {
            const payload = this.inventoryManageVisibilityService.formatPayloadForSave(
                this.inventoryManageVisibilityComponent.elementMap,
                this.inventoryManageVisibilityComponent.autoAddElements,
            );
            this.inventoryManageVisibilityService.saveElementVisibilityConfig(orgId, payload);
        }
    }

    saveCancelled() {
        this.inventoryManageVisibilityComponent.resetConfig();
    }

    private handleStateChange(toState: StateOrName, toParams: RawParams, options: TransitionOptions): boolean {
        if (!this.saveEnabled || this.allowTransition) return true;
        const modalData: IConfirmModalConfig = {
            type: ConfirmModalType.WARNING,
            translations: {
                title: this.translatePipe.transform("UnsavedChangesTitle"),
                desc: this.translatePipe.transform("UnsavedConfigurationChangesDescription"),
                confirm: this.translatePipe.transform("AreYouSureContinue"),
                submit: this.translatePipe.transform("SaveChanges"),
                cancel: this.translatePipe.transform("Cancel"),
            },
            hideClose: true,
        };
        this.inventoryLaunchModalService.launchConfirmationModal(modalData)
            .pipe(
                takeUntil(this.destroy$),
                filter((result: boolean) => result))
            .subscribe(() => {
                this.allowTransition = true;
                this.saveSettings();
                this.stateService.go(toState, toParams, options);
            },
        );
        return false;
    }
}
