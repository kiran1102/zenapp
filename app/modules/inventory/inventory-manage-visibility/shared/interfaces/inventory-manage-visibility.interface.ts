import { IInventoryListItem } from "interfaces/inventory.interface";

export interface IInventoryManageVisibilityViewState {
    saveEnabled: boolean;
    saving: boolean;
    loading: boolean;
}

export interface IInventoryManageVisibilityState {
    autoAddElements: boolean;
    override: boolean;
    categories: IInventoryListItem[];
}

export interface IInventoryManageElementVisibilityPayload {
    autoAddElements: boolean;
    elements: IInventoryElementVisibilityItem[];
}

export interface IInventoryElementVisibilityItem {
    id: string;
}
