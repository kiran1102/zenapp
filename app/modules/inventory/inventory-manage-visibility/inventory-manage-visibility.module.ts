// Angular
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { HttpClientModule } from "@angular/common/http";
import { OTPipesModule } from "modules/pipes/pipes.module";

// Modules
import {
    OtLoadingModule,
    OtButtonModule,
    OtTreeModule,
    OtPageHeaderModule,
    OtBreadcrumbModule,
    OtToggleModule,
    OtTooltipModule,
    OtSearchModule,
    OtEmptyStateModule,
    OtFormModule,
    OtBadgeModule,
    OtPageFooterModule,
} from "@onetrust/vitreus";
import { TranslateModule } from "@ngx-translate/core";

// TODO: Remove once empty state image placed in own module
import { SharedModule } from "sharedModules/shared.module";

// Components
import { InventoryManageVisibilityWrapperComponent } from "inventoryModule/inventory-manage-visibility/shared/components/inventory-manage-visibility-wrapper/inventory-manage-visibility-wrapper.component";
import { InventoryManageVisibilityComponent } from "inventoryModule/inventory-manage-visibility/shared/components/inventory-manage-visibility/inventory-manage-visibility.component";

// Services
import { InventoryManageVisibilityService } from "inventoryModule/inventory-manage-visibility/shared/services/inventory-manage-visibility.service";

@NgModule({
    imports: [
        CommonModule,
        HttpClientModule,
        OtLoadingModule,
        OtPageFooterModule,
        OtButtonModule,
        OtTreeModule,
        OtSearchModule,
        OtPageHeaderModule,
        OtBreadcrumbModule,
        OtEmptyStateModule,
        OtTooltipModule,
        OtToggleModule,
        OtBadgeModule,
        OTPipesModule,
        OtFormModule,
        SharedModule,
        TranslateModule,
    ],
    exports: [
        InventoryManageVisibilityComponent,
    ],
    declarations: [
        InventoryManageVisibilityWrapperComponent,
        InventoryManageVisibilityComponent,
    ],
    providers: [
        InventoryManageVisibilityService,
    ],
    entryComponents: [
        InventoryManageVisibilityWrapperComponent,
    ],
})
export class InventoryManageVisibilityModule {}
