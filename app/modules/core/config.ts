export const CommonApiConfig = {
    getTenantsEndpoint: "/api/v1/private/user/info",
    getOrgsEndPoint: "/api/v1/private/orggroup/roottree/",
    getOrgGroupUser: "/api/v1/private/orggroupuser/get",
    getOrgContent: "/api/v1/private/content/list",
    getBranding: "/api/v1/private/settings/getBranding",
};
