// Core
import { Injectable } from "@angular/core";
import { Resolve } from "@angular/router";

// Redux
import { Observable, of } from "rxjs";

// Services
import { LoadingService } from "modules/core/services/loading.service";

@Injectable({ providedIn: "root" })
export class LoadingResolver implements Resolve<void> {

    constructor(private loadingService: LoadingService) {}

    resolve(): Observable<void> {
        return of(this.loadingService.set(false));
    }
}
