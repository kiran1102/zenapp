// Core
import { Injectable } from "@angular/core";
import { Resolve } from "@angular/router";

// Redux
import { Observable } from "rxjs";

// Services
import { Translations } from "modules/core/services/translations.service";

// Interface
import { ILanguage } from "interfaces/localization.interface";

@Injectable({ providedIn: "root" })
export class TranslationsResolver implements Resolve<[ILanguage[], Map<string, string>]> {

    constructor(private translations: Translations) {}

    resolve(): Observable<[ILanguage[], Map<string, string>]> {
        return this.translations.init(true);
    }
}
