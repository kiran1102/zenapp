// Angular
import { Injectable } from "@angular/core";
import { Resolve } from "@angular/router";

// rxjs
import { Observable } from "rxjs";

// Common
import { OAuthenticationService } from "@onetrust/common";

@Injectable({ providedIn: "root" })
export class AuthenticationResolver implements Resolve<boolean> {

    constructor(private auth: OAuthenticationService) {}

    resolve(): Observable<boolean> {
        return this.auth.isLoggedIn();
    }
}
