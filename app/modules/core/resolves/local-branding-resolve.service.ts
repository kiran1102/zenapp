import { Injectable, Inject } from "@angular/core";
import { Resolve } from "@angular/router";

// Services
import { Branding } from "modules/shared/services/provider/branding.service";

// Redux
import { IStore } from "interfaces/redux.interface";
import { StoreToken } from "tokens/redux-store.token";
import { BrandingActions } from "oneRedux/reducers/branding.reducer";

// Interface
import { IBrandingData } from "interfaces/branding.interface";

@Injectable({ providedIn: "root" })
export class LocalBrandingResolver implements Resolve<IBrandingData> {

    constructor(
        @Inject(StoreToken) private store: IStore,
        private branding: Branding,
    ) {}

    resolve(): Promise<IBrandingData> {
        const brandingFromStorage = this.branding.getBrandingFromStorage();
        const branding = brandingFromStorage ? brandingFromStorage.branding : null;
        if (branding) {
            this.store.dispatch({
                type: BrandingActions.SET_BRANDING_DATA_AND_FETCHED,
                branding,
            });
        }
        return Promise.resolve(branding);
    }
}
