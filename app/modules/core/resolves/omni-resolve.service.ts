// Core
import { Injectable } from "@angular/core";
import { Resolve } from "@angular/router";

// Redux
import { Observable } from "rxjs";

// Services
import { OmniService } from "modules/core/services/omni.service";

@Injectable({ providedIn: "root" })
export class OmniResolver implements Resolve<boolean> {

    constructor(private omni: OmniService) {}

    resolve(): Observable<boolean> {
        return this.omni.fetch();
    }
}
