// Angular
import { Injectable } from "@angular/core";
import { Resolve } from "@angular/router";

// Services
import { Content } from "modules/shared/services/provider/content.service";

// Interfaces
import { IStringMap } from "interfaces/generic.interface";

@Injectable({ providedIn: "root" })
export class ContentResolver implements Resolve<IStringMap<string>> {

    constructor(private content: Content) {}

    resolve(): Promise<IStringMap<string>> {
        return Promise.resolve(this.content.GetContent());
    }
}
