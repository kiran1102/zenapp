// Angular
import { Injectable } from "@angular/core";
import {
    HttpClient,
    HttpRequest,
    HttpEvent,
    HttpResponse,
    HttpHeaders,
    HttpErrorResponse,
} from "@angular/common/http";

// Services
import { TranslateService } from "@ngx-translate/core";
import { NotificationService } from "modules/shared/services/provider/notification.service";

// Interfaces
import {
    IProtocolMessage,
    IProtocolConfig,
    IProtocolPacket,
    IProtocolError,
} from "interfaces/protocol.interface";

// Common
import { InterceptorHttpParams } from "@onetrust/common";

@Injectable()
export class ProtocolService {

    private keys = ["Success", "Error"];
    private translations = {};

    constructor(
        private httpClient: HttpClient,
        private translate: TranslateService,
        private notificationService: NotificationService,
    ) {
        this.translate
            .stream(this.keys)
            .subscribe((values) => (this.translations = values));
    }

    // use for public facing apis
    public customConfig(method: string, url: string, params?: any, data?: any, headers?: any, eventHandlers?: any, uploadEventHandlers?: any, responseType?: string, timeout?: number, multipartFormData?: boolean) {
        return this.config(method, url, params, data, headers, eventHandlers, uploadEventHandlers, responseType, timeout, multipartFormData, true);
    }

    public config(method: string, url: string, params: any = {}, body?: any, headers: any = {}, eventHandlers?: any, reportProgress?: any, responseType?: any, timeout?: number, multipartFormData?: boolean, removeURI: boolean = false) {
        if (params) {
            for (const key in params) {
                if (params.hasOwnProperty(key)) {
                    const value = params[key];
                    if (value instanceof Array) {
                        params[key] = value;
                    } else if (value instanceof Object) {
                        params[key] = JSON.stringify(value);
                    }
                }
            }
        }
        const config: IProtocolConfig = new HttpRequest(method, url, body, {
            params: new InterceptorHttpParams({
                removeURI,
                ignoreAuthModule: false,
                multipartFormData,
            }, params || {}),
            headers: new HttpHeaders(headers || {}),
            reportProgress,
            responseType,
        });
        return config;
    }

    public http(config: IProtocolConfig, messages?: any) {
        let conf: any;
        if (!(config instanceof HttpRequest)) {
            conf = this.config(
                config.method,
                config.url,
                config.params,
                config.body,
                null,
                config.headers,
                config.reportProgress,
                config.responseType,
                null,
                config.multipartFormData,
                config.removeURI,
            );
        } else {
            conf = config as HttpRequest<any>;
        }
        const msgSuccess = messages ? messages.Success : null;
        const hideSuccess: boolean = msgSuccess ? msgSuccess.Hide : true;
        const customSuccesMsg = msgSuccess ? msgSuccess.custom : "";

        const msgError = messages ? messages.Error : null;
        const hideError: boolean = msgError ? msgError.Hide : false;
        const customErrorMsg = msgError ? msgError.custom : "";

        const successCb = this.successCallback(msgSuccess, hideSuccess, customSuccesMsg);
        const errorCb = this.errorCallback(msgError, hideError, customErrorMsg);

        return (this.httpClient.request(conf).toPromise().then(
            (response: HttpEvent<any>): IProtocolPacket => successCb(response),
            (response: HttpEvent<any>): IProtocolPacket => errorCb(response),
        ) as unknown) as ng.IPromise<IProtocolPacket>;
    }

    private successCallback(messageParams: any, hideMessage: boolean, customMsg: string): any {
        return (response: HttpResponse<any>): IProtocolPacket => {
            if (!hideMessage && (customMsg || messageParams)) {
                this.notificationService.alertSuccess(this.translations["Success"], customMsg || this.successMessage(messageParams));
            }
            const headers = {};
            const keys = response.headers.keys();
            for ( const k of keys) {
                headers[k] = response.headers.get(k);
            }
            const packet: IProtocolPacket = {
                data: response.body ? response.body : null,
                result: true,
                status: response.status,
                statusText: response.statusText,
                time: (new Date()).getTime(),
                headers,
            };
            return packet;
        };
    }

    private errorCallback(messageParams: any, hideMessage: boolean, customMsg: string): any {
        return (response: HttpErrorResponse): IProtocolPacket => {
            let messages: string | IProtocolError[] = "";
            if (!hideMessage) {
                if (customMsg) {
                    this.notificationService.alertError(this.translations["Error"], customMsg);
                } else if (response.error && response.error.Message) {
                    messages = response.error.Message;
                    this.notificationService.alertWarning(this.translations["Error"], response.error.Message);
                } else if (response.error && response.error.errors && response.error.errors.length) {
                    messages = response.error.errors;
                    (messages as IProtocolError[]).forEach((errorMessage: IProtocolError) => {
                        this.notificationService.alertWarning(errorMessage.title, errorMessage.detail);
                    });
                } else if (messageParams) {
                    this.notificationService.alertError(this.translations["Error"], customMsg || this.errorMessage(messageParams));
                }
            } else if (response.error && response.error.message) {
                messages = response.error.message;
            } else if (response.error && response.error.errors && response.error.errors.length) {
                messages = response.error.errors;
            }

            const packet: IProtocolPacket = {
                data: messages,
                errorCode: response.error ? response.error.errorCode : "",
                result: false,
                status: response.status,
                errors: response.error && response.error.errors ? response.error.errors[0] : "",
                time: (new Date()).getTime(),
            };
            return packet;
        };
    }

    private successMessage = (message: IProtocolMessage): string => `Your ${message.object} has been ${message.action}.`;
    private errorMessage = (message: IProtocolMessage): string => `Sorry, there was a problem ${message.action} your ${message.object}.`;

}
