import { Injectable } from "@angular/core";
import { BehaviorSubject } from "rxjs";

@Injectable()
export class LoadingService {

    active$ = new BehaviorSubject<boolean>(true);

    set(isActive: boolean) {
        this.active$.next(isActive);
    }

}
