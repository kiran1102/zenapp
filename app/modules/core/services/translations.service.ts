// Angular
import { Injectable, Inject } from "@angular/core";

// RxJs
import { Observable, forkJoin } from "rxjs";
import { tap } from "rxjs/operators";

// Services
import { TranslationsService } from "@onetrust/common";
import { TranslateService } from "@ngx-translate/core";
import { TenantTranslationService } from "sharedServices/tenant-translation.service";

// Interface
import { ILanguage } from "interfaces/localization.interface";

@Injectable()
export class Translations {
    constructor(
        private translate: TranslateService,
        private translations: TranslationsService,
        @Inject("TenantTranslationService") private tenantTranslationService: TenantTranslationService,
    ) {}

    init(seed = false): Observable<[ILanguage[], Map<string, string>]> {
        this.tenantTranslationService.setTenantLanguageToRootScope(this.currentLanguage);
        return forkJoin(
            this.getLanguages(),
            this.changeTranslation(this.currentLanguage, seed),
        );
    }

    getLanguages(): Observable<ILanguage[]> {
        return this.translations.getGlobalLanguages().pipe(
            tap((languages: ILanguage[]) => {
                this.tenantTranslationService.setLanguages(languages);
                return languages;
            }),
        );
    }

    changeTranslation(lang: string, seed = false): Observable<Map<string, string>> {
        return this.translations.useLang(lang, seed).pipe(
            tap((translations) => {
                this.tenantTranslationService.setSystemTranslations(translations || {});
                return translations;
            }),
        );
    }

    get all() {
        return this.translate.translations[this.translate.currentLang];
    }

    get currentLanguage(): string {
        return JSON.parse(localStorage.getItem("OneTrust.languageSelected")) || "en-us";
    }
}
