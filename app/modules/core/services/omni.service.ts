// Angular
import { Injectable } from "@angular/core";

// rxjs
import {
    BehaviorSubject,
    forkJoin,
    from,
    Observable,
    of,
} from "rxjs";
import {
    tap,
    map,
    switchMap,
} from "rxjs/operators";

// Common
import {CommonAutoRefreshService, PermissionsService} from "@onetrust/common";
import { NgxPermissionsObject } from "ngx-permissions";

// Services
import { Branding } from "modules/shared/services/provider/branding.service";
import { Content } from "modules/shared/services/provider/content.service";
import { OrgActionService } from "oneServices/actions/org-action.service";
import { Permissions } from "modules/shared/services/helper/permissions.service";
import { Principal } from "modules/shared/services/helper/principal.service";
import { TimeStamp } from "oneServices/time-stamp.service";
import { Translations } from "./translations.service";
import { UserActionService } from "oneServices/actions/user-action.service";

// Interfaces
import { IBrandingData } from "interfaces/branding.interface";
import { ILanguage } from "interfaces/localization.interface";
import { IOrganization } from "interfaces/org.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";
import { IStringMap } from "interfaces/generic.interface";
import { IUser } from "interfaces/user.interface.ts";

@Injectable()
export class OmniService {

    private omniData$ = new BehaviorSubject<any>(null);
    private token$ = new BehaviorSubject<string>(null);
    private language$ = new BehaviorSubject<string>(null);

    constructor(
        private branding: Branding,
        private content: Content,
        private orgAction: OrgActionService,
        private permissions: Permissions,
        private permissionsService: PermissionsService,
        private principal: Principal,
        private timeStamp: TimeStamp,
        private translations: Translations,
        private userAction: UserActionService,
        private autoRefresh: CommonAutoRefreshService,
    ) {}

    isLatest(
        currentToken: string,
        currentLanguage = this.translations.currentLanguage,
    ) {
        return Boolean(
            currentToken === this.token$.value
            && currentLanguage === this.language$.value
            && this.omniData$.value,
        );
    }

    reset() {
        this.omniData$.next(null);
        this.token$.next(null);
        this.language$.next(null);
    }

    fetch(force?: boolean): Observable<any> {
        const currentToken = this.principal.identity(true);
        const currentAccessToken = currentToken ? currentToken.access_token : null;
        const currentLanguage = this.translations.currentLanguage;
        if (!force && this.isLatest(currentAccessToken, currentLanguage)) {
            return of(this.omniData$.value);
        } else {
            this.omniData$.next(null);
            this.token$.next(currentAccessToken);
            this.language$.next(currentLanguage);
        }
        const omniPermissions$ = this.getPermissions().pipe(
            switchMap((permResults) => {
                return this.getUsers().pipe(
                    map((users) => [ permResults, users ]),
                );
            }),
        );
        const omniStandalone$ = forkJoin(
            this.getTranslations(),
            this.getBranding(),
            this.getContent(),
            this.getCurrentUser(),
            this.getDateTime(),
            this.getOrganizations(),
        );

        const omniDataObs = forkJoin(
            omniPermissions$,
            omniStandalone$,
        ).pipe(
            map(([
                [
                    permissions,
                    users,
                ],
                [
                    [
                        languages,
                        translations,
                    ],
                    branding,
                    content,
                    currentUser,
                    dateTime,
                    organizations,
                ],
            ]) => ({
                    permissions,
                    languages,
                    translations,
                    branding,
                    content,
                    currentUser,
                    dateTime,
                    organizations,
                    users,
            })),
            tap((data) => {
                this.omniData$.next(data);
                // Init autoRefresh
                if ( this.permissionsService.permService.getPermission("AutoRefreshToken") ) {
                    this.autoRefresh.autoRefreshToken();
                }
            }),
        );
        return omniDataObs;
    }

    getContent(): Observable<IStringMap<string>> {
        return from(this.content.GetContent());
    }

    getTranslations(): Observable<[ILanguage[], Map<string, string>]> {
        return this.translations.init();
    }

    getOrganizations(): Observable<IOrganization> {
        return from(
            this.orgAction.fetchOrg(
                this.principal.getRootOrg(),
                this.principal.getOrggroup(),
            ),
        );
    }

    getPermissions(): Observable<NgxPermissionsObject> {
        return this.permissionsService.setPermissions();
    }

    getCurrentUser(): Observable<IProtocolResponse<IUser>> {
        return from(this.userAction.fetchCurrentUser());
    }

    getUsers(): Observable<null|IProtocolResponse<IUser[]>> {
        if (this.permissions.canShow("AppInitAllowAllUsersCall")) {
            return from(this.userAction.fetchUsers());
        } else {
            return of(null);
        }
    }

    getBranding(): Observable<IBrandingData> {
        return from(this.branding.getBranding(true));
    }

    getDateTime(): Observable<string[]> {
        return from(this.timeStamp.dateTimeCombinedFormats());
    }
}
