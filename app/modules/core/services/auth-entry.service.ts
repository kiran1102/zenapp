import { Injectable } from "@angular/core";
import { BehaviorSubject } from "rxjs";

// Services
import { Translations } from "modules/core/services/translations.service";
import { Principal } from "modules/shared/services/helper/principal.service";
import { Permissions } from "modules/shared/services/helper/permissions.service";
import { EulaService } from "modules/eula/shared/eula.service";
import { ModalService } from "sharedServices/modal.service";
import { BannerAlertService } from "sharedModules/services/api/banner-alert.service";
import { AppMaintenanceAlertsService } from "sharedModules/services/api/app-maintenance-alerts.service";

// Interfaces
import { IToken } from "interfaces/token.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";
import { IBannerAlertData } from "interfaces/app-maintenance-alerts.interface";

@Injectable()
export class AuthEntryService {

    private entered$ = new BehaviorSubject<boolean>(false);
    private token$ = new BehaviorSubject<IToken>(null);
    private language$ = new BehaviorSubject<string>(null);

    constructor(
        private translations: Translations,
        private principal: Principal,
        private permissions: Permissions,
        private modalService: ModalService,
        private bannerAlertService: BannerAlertService,
        private appMaintenanceAlertsService: AppMaintenanceAlertsService,
        private eulaService: EulaService,
    ) {}

    init() {
        const currentToken = this.principal.getIdentity();
        const currentLanguage = this.translations.currentLanguage;
        if (
            currentToken && this.token$.value
            && currentToken.access_token === this.token$.value.access_token
            && currentLanguage === this.language$.value
            && this.entered$.value
        ) {
            return;
        } else {
            this.entered$.next(true);
            this.token$.next(currentToken);
            this.language$.next(currentLanguage);
        }

        let alertServiceCallback: () => void;
        if (this.permissions.canShow("MaintenanceAlertView")) {
            alertServiceCallback = () => {
                this.bannerAlertService.handleMaintenantsAlerts().subscribe(() => {
                    this.appMaintenanceAlertsService.getAlertNotifications()
                        .then((response: IProtocolResponse<IBannerAlertData>) => {
                            if (!(response.result && response.data)) return;
                            this.modalService.setModalData(response.data);
                            this.modalService.openModal("appMaintenanceAlertsModal");
                        });
                });
            };
        }
        if (this.permissions.canShow("EULARequired")) {
            this.eulaService.displayEula(alertServiceCallback);
        } else if (alertServiceCallback) {
            alertServiceCallback();
        }
    }
}
