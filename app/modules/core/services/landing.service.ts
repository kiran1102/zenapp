// Angular
import { Injectable, Inject } from "@angular/core";
import { Router } from "@angular/router";

// Services
import { Principal } from "modules/shared/services/helper/principal.service";
import { Permissions } from "modules/shared/services/helper/permissions.service";
import { SettingActionService } from "settingsServices/actions/setting-action.service";
import { GlobalSidebarService } from "modules/global-sidebar/services/one-global-sidebar.service";

// Redux
import { IStore } from "interfaces/redux.interface";
import { StoreToken } from "tokens/redux-store.token";
import { getProjectSettings } from "oneRedux/reducers/setting.reducer";

// Interfaces
import { IAccessToken } from "interfaces/token.interface";
import { IProjectSettings } from "interfaces/setting.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";
import { ISspTemplatesRestrictView } from "interfaces/self-service-portal/ssp.interface";

// Constants
import { UserRoles } from "constants/user-roles.constant";

@Injectable()
export class LandingService {

    constructor(
        private router: Router,
        private principal: Principal,
        private permissions: Permissions,
        private settingAction: SettingActionService,
        private sidebarService: GlobalSidebarService,
        @Inject(StoreToken) private store: IStore,
    ) {}

    execute() {
        this.sidebarService.set();
        const selfServicePermission: boolean = this.permissions.canShow("SelfServiceExecute");
        const token: IAccessToken = this.principal.getDecodedToken();
        if (!this.permissions.canShow("Assessments")) {
            this.settingAction.fetchProjectSettings().then(() => {
                const projectSettings: IProjectSettings = getProjectSettings(this.store.getState());
                if (projectSettings && projectSettings.SelfServiceEnabled && selfServicePermission) {
                    this.navigate("zen.app.pia.module.selfservice.list");
                } else if (token.role === UserRoles.ProjectViewer) {
                    this.navigate("zen.app.pia.module.projects.list");
                } else {
                    this.navigate();
                }
            });
        } else {
            if (token.role === UserRoles.ProjectViewer && this.permissions.canShow("Assessments")) {
                this.navigate("zen.app.pia.module.assessment.list");
            } else if (this.permissions.canShow("Assessments") && this.permissions.canShow("SelfServicePortal")) {
                if (this.permissions.canShow("BypassSelfServicePortal")) {
                    this.navigate();
                } else {
                    this.navigate("zen.app.pia.module.ssp.list");
                }
            } else {
                this.navigate();
            }
        }
    }

    private navigate(path?: string) {
        if (path) {
            this.permissions.goToFallback(path);
        } else {
            setTimeout(() => {
                this.router.navigate(["/module/welcome"], { replaceUrl: true });
            });
        }
    }

}
