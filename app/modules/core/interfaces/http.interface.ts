import { HttpRequest } from "@angular/common/http";

export interface IOtHttpRequest<T> extends HttpRequest<T> {
    ignoreAuthModule?: boolean;
    multipartFormData?: boolean;
    removeURI?: boolean;
    tokenNotRequired?: boolean;
}
