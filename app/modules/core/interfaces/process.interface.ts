export interface INodeEnv {
    NODE_ENV: "development" | "production";
    ONETRUST_DEV_ENV: string;
}

export interface INodeProcess {
    env: INodeEnv;
}
