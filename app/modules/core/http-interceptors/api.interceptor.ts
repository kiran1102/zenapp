// Angular
import { Injectable } from "@angular/core";
import {
    HttpInterceptor,
    HttpEvent,
    HttpHandler,
    HttpRequest,
    HttpParams,
} from "@angular/common/http";

// rxjs
import { Observable } from "rxjs/Observable";

// Common
import { InterceptorHttpParams } from "@onetrust/common";

// Interface
import { INodeProcess } from "../interfaces/process.interface";
import { IOtHttpRequest } from "../interfaces/http.interface";

// Regex
import { API_PREFIX_PATTERN } from "constants/regex.constant";

declare const process: INodeProcess;

// interceptor that sets url for all api calls. resource calls are skipped
@Injectable()
export class ApiInterceptor implements HttpInterceptor {

    intercept(req: IOtHttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        if (req.url.match(/botdetect$/)) {
            return next.handle(req);
        }
        if (!req.url.match(API_PREFIX_PATTERN) && req.url.indexOf("https://") === -1) {
            req = req.clone({ url: this.urlBasedOnEnv(req) });
        }

        req = this.normalizeParams(req);

        return next.handle(req);
    }

    private urlBasedOnEnv(req: HttpRequest<any>): string {
        const apiUrl = "/api/v1/private";
        const config = (req.params instanceof InterceptorHttpParams)
            ? req.params.interceptorConfig
            : {};

        const modifiedAPIUrl = (this.isException(req.url) || (config ? config.removeURI : false)) ? "" : apiUrl;
        const domain = process.env.NODE_ENV === "development"
            ? process.env.ONETRUST_DEV_ENV
            : "";

        return `${domain}${modifiedAPIUrl}${req.url}`;
    }

    private isException(url: string) {
        return (url.toLowerCase().indexOf("/api/") > -1);
    }

    private normalizeParams(req: HttpRequest<any>) {
        if (!req.params || !(req.params instanceof HttpParams)) return req;
        const paramKeys = req.params.keys() || [];
        for (const key of paramKeys) {
            const value = req.params.get(key);
            if (value == null) {
                req = req.clone({
                    params: req.params.delete(key),
                });
            }
        }
        return req;
    }

}
