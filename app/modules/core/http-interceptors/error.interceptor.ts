// Angular
import { Injectable } from "@angular/core";
import {
    HttpRequest,
    HttpHandler,
    HttpEvent,
    HttpInterceptor,
    HttpErrorResponse,
} from "@angular/common/http";

// rxjs
import { Observable, throwError } from "rxjs";
import { catchError } from "rxjs/operators";

// Services
import { AuthHandlerService } from "modules/shared/services/helper/auth-handler.service";

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {

    constructor(private authHandlerService: AuthHandlerService) {}

    intercept(
        request: HttpRequest<any>,
        next: HttpHandler,
    ): Observable<HttpEvent<any>> {
        return next.handle(request).pipe(
            catchError((error: HttpErrorResponse) => {
                if (error.status === 401) {
                    this.authHandlerService.refresh();
                }
                return throwError(error);
            }),
        );
    }
}
