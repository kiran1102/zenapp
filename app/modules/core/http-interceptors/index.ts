import { HTTP_INTERCEPTORS } from "@angular/common/http";
import { ApiInterceptor } from "modules/core/http-interceptors/api.interceptor";
import { ErrorInterceptor } from "modules/core/http-interceptors/error.interceptor";

// interceptors work in order. they are chained together
// request A->B->C
// response C->B->A
export const httpInterceptorProviders = [
    // do not change the order the interceptors
    { provide: HTTP_INTERCEPTORS, useClass: ApiInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
];
