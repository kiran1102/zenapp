// Core
import { Injectable } from "@angular/core";
import { CanActivate } from "@angular/router";

// Services
import { AuthHandlerService } from "modules/shared/services/helper/auth-handler.service";

@Injectable({ providedIn: "root" })
export class AuthGuard implements CanActivate {

    constructor(private authHandlerService: AuthHandlerService) {}

    canActivate(): boolean {
        return this.authHandlerService.handleAuthGuard();
    }
}
