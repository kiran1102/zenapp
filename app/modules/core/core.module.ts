// Angular
import {
    NgModule,
    Optional,
    SkipSelf,
} from "@angular/core";

import {
    OtCommonsServicesModule,
    OAuthModule,
    TranslateModule,
    TranslateLoader,
    LanguageLoader,
    NgxPermissionsModule,
} from "@onetrust/common";

// Common
import { CommonApiConfig } from "./config";

// Mock
import { MockModule } from "../mock/mock.module";

// Interceptors
import { httpInterceptorProviders } from "./http-interceptors";

// Services
import { AuthEntryService } from "modules/core/services/auth-entry.service";
import { LoadingService } from "modules/core/services/loading.service";
import { OmniService } from "modules/core/services/omni.service";
import { ProtocolService } from "modules/core/services/protocol.service";
import { Translations } from "modules/core/services/translations.service";

// Interfaces
import { INodeProcess } from "modules/core/interfaces/process.interface";

declare const process: INodeProcess;

const devOnlyModules = [];

if (process.env.NODE_ENV === "development") {
    devOnlyModules.push(MockModule);
}

// import providers that are shared across lazy loaded modules
@NgModule({
    imports: [
        OAuthModule.forRoot(),
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useClass: LanguageLoader,
            },
        }),
        NgxPermissionsModule.forRoot(),
        ...devOnlyModules,
    ],
    providers: [
        httpInterceptorProviders,
        AuthEntryService,
        LoadingService,
        OmniService,
        ProtocolService,
        Translations,
    ],
    exports: [ OtCommonsServicesModule ],
})
export class CoreModule {
    constructor(
        @Optional()
        @SkipSelf()
            parentModule: CoreModule,
    ) {
        if (parentModule) {
            throw new Error(
                "Core Module has already been loaded. Import Core modules in the AppModule only.",
            );
        }
    }

}
