// 3rd Party
import { NgModule } from "@angular/core";

// Modules
import { SharedModule } from "sharedModules/shared.module";
import { UpgradedLegacyModule } from "sharedModules/upgrade/upgraded-legacy.module";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";
import { ReactiveFormsModule } from "@angular/forms";
import { VitreusModule } from "@onetrust/vitreus";
import { OTPipesModule, PIPES } from "modules/pipes/pipes.module";
import { InventorySharedModule } from "inventorySharedModule/inventory-shared.module";

// Services
import { AttributeAdapterService } from "attributeManagerModule/services/attribute-adapter.service";
import { AttributeActionService } from "attributeManagerModule/services/attribute-action.service";
import { AttributeLogicService } from "attributeManagerModule/services/attribute-logic.service";
import { AttributeGroupsActionService } from "attributeManagerModule/services/attribute-groups-action.service";

// Components
import { AttributeManagerComponent } from "attributeManagerModule/components/attribute-manager/attribute-manager.component";
import { AttributeGroupsBuilderComponent } from "attributeManagerModule/components/attribute-groups-builder/attribute-groups-builder.component";
import { AttributeManagerEntryComponent } from "attributeManagerModule/components/attribute-manager-entry/attribute-manager-entry.component";
import { AttributeManagerDetailsComponent } from "attributeManagerModule/components/attribute-manager-details/attribute-manager-details.component";
import { AttributeManagerOptionsComponent } from "attributeManagerModule/components/attribute-manager-options/attribute-manager-options.component";
import { AttributeManagerModalComponent } from "attributeManagerModule/components/attribute-manager-modal/attribute-manager-modal.component";
import { AttributeManagerMetadataComponent } from "attributeManagerModule/components/attribute-manager-metadata/attribute-manager-metadata.component";
import { EditAttributeOptionModalComponent } from "attributeManagerModule/components/edit-attribute-option-modal/edit-attribute-option-modal.component";
import { AttributeOptionStatusModalComponent } from "attributeManagerModule/components/attribute-option-status-modal/attribute-option-status-modal.component";
import { AttributeManagerTableComponent } from "attributeManagerModule/components/attribute-manager-table/attribute-manager-table.component";
import { AttributeStatusModalComponent } from "attributeManagerModule/components/attribute-status-modal/attribute-status-modal.component";
import { AttributeGroupModal } from "attributeManagerModule/components/attribute-group-modal/attribute-group-modal.component";
import { AttributePickerModal } from "attributeManagerModule/components/attribute-picker-modal/attribute-picker-modal.component";

@NgModule({
    imports: [
        SharedModule,
        UpgradedLegacyModule,
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        VitreusModule,
        OTPipesModule,
        InventorySharedModule,
    ],
    declarations: [
        AttributeGroupsBuilderComponent,
        AttributePickerModal,
        AttributeGroupModal,
        AttributeManagerComponent,
        AttributeManagerDetailsComponent,
        AttributeManagerEntryComponent,
        AttributeManagerMetadataComponent,
        AttributeManagerModalComponent,
        AttributeManagerOptionsComponent,
        AttributeManagerTableComponent,
        AttributeOptionStatusModalComponent,
        AttributeStatusModalComponent,
        EditAttributeOptionModalComponent,
    ],
    entryComponents: [
        AttributePickerModal,
        AttributeGroupModal,
        AttributeManagerComponent,
        AttributeManagerDetailsComponent,
        AttributeManagerEntryComponent,
        AttributeManagerModalComponent,
        AttributeOptionStatusModalComponent,
        AttributeStatusModalComponent,
        EditAttributeOptionModalComponent,
    ],
    providers: [
        AttributeActionService,
        AttributeAdapterService,
        AttributeGroupsActionService,
        AttributeLogicService,
        PIPES, // TODO: Remove from providers when ot-common is implemented. Only necessary if you are using pipe in TS file, else just module is fine
    ],
})
export class AttributeManagerModule {}
