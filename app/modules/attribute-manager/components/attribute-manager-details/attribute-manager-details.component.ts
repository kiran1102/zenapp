// Rxjs
import { Subject } from "rxjs";
import {
    startWith,
    takeUntil,
} from "rxjs/operators";

// 3rd party
import {
    Component,
    Inject,
} from "@angular/core";

// Redux
import observableFromStore from "oneRedux/utils/observable-from-store";
import {
    getCurrentAttribute,
    getIsLoadingCurrentAttribute,
} from "oneRedux/reducers/attribute.reducer";

// Services
import { AttributeActionService } from "attributeManagerModule/services/attribute-action.service";
import { Permissions } from "modules/shared/services/helper/permissions.service";
import { AttributeLogicService } from "attributeManagerModule/services/attribute-logic.service";
import { StateService } from "@uirouter/core";

// Constants / Enums
import {
    InventoryTranslationKeys,
    InventoryPermissions,
    InventoryOptionStatuses,
    ModuleContext,
} from "constants/inventory.constant";

// Tokens
import { StoreToken } from "tokens/redux-store.token";

// Interfaces
import { IAttributeDetailV2 } from "interfaces/inventory.interface";
import { IStringMap } from "interfaces/generic.interface";
import {
    IStore,
    IStoreState,
} from "interfaces/redux.interface";
import { ITab } from "interfaces/ot-tabs.interface";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

@Component({
    selector: "attribute-manager-details",
    templateUrl: "./attribute-manager-details.component.html",
})
export class AttributeManagerDetailsComponent {

    moduleContext = ModuleContext;
    module: string;
    attributePermissions: IStringMap<boolean>;
    permissions = InventoryPermissions[this.stateService.params.id];
    title: string;
    breadcrumbTitle: string;
    breadcrumbRoute: string;
    tabIds: IStringMap<string> = {
        details: "details",
        options: "options",
    };
    tabs: ITab[];
    currentTab: string;
    isLoading: boolean;
    optionStatuses: IStringMap<string> = InventoryOptionStatuses;
    isShowingOptionsTab: boolean;
    canEditAttribute: boolean;
    private destroy$: Subject<null> = new Subject();

    constructor(
        @Inject(StoreToken) private store: IStore,
        private stateService: StateService,
        readonly attributeActionService: AttributeActionService,
        readonly translate: TranslatePipe,
        private permissionsService: Permissions,
        private attributeLogicService: AttributeLogicService,
    ) {}

    ngOnInit(): void {
        this.module = this.stateService.params && this.stateService.params.context ? this.stateService.params.context : ModuleContext.Inventory;
        this.breadcrumbTitle = this.translate.transform(InventoryTranslationKeys[this.stateService.params.id].attributes);
        this.attributePermissions = {
            canAdd: this.permissionsService.canShow(this.permissions.addAttributes),
        };
        this.tabs = [
            {
                id: this.tabIds.details,
                text: this.translate.transform("Details"),
                otAutoId: "attributeManagerDetailsDetails",
            },
            {
                id: this.tabIds.options,
                text: this.translate.transform("Options"),
                otAutoId: "attributeManagerDetailsOptions",
            },
        ];
        const isVendorRoute = this.stateService.current.name.includes("zen.app.pia.module.vendor.attribute-manager-details");
        this.breadcrumbRoute = isVendorRoute ? `zen.app.pia.module.vendor.attribute-manager` : `zen.app.pia.module.datamap.views.attribute-manager.list`;
        this.breadcrumbTitle = isVendorRoute ? this.translate.transform("VendorAttributes")  : this.translate.transform(InventoryTranslationKeys[this.stateService.params.id].attributes);
        this.currentTab = this.stateService.params.tabId || this.tabIds.details;
        observableFromStore(this.store).pipe(
            startWith(this.store.getState()),
            takeUntil(this.destroy$),
        ).subscribe((state: IStoreState): void =>
            this.componentWillReceiveState(state),
        );
        this.attributeActionService.setCurrentAttribute(this.stateService.params.attributeId);
    }

    addOptions(): void {
        this.attributeActionService.openOptionsModal(this.stateService.params.id, this.stateService.params.attributeId, true);
    }

    editAttribute(): void {
        this.attributeActionService.openEditModal(this.stateService.params.id, this.stateService.params.attributeId, () => {
            this.attributeActionService.setCurrentAttribute(this.stateService.params.attributeId);
        });
    }

    ngOnDestroy(): void {
        this.destroy$.next();
        this.destroy$.complete();
    }

    goToListView(route: { path: string, params: any }) {
        this.stateService.go(route.path, { id: this.stateService.params.id});
    }

    onTabClick(tab: ITab) {
        const routeParams: { id: string, attributeId: string, tabId: string } = {
            id: this.stateService.params.id,
            attributeId: this.stateService.params.attributeId,
            tabId: tab.id,
        };
        this.stateService.go(".", routeParams);
        this.currentTab = tab.id;
    }

    private componentWillReceiveState(state: IStoreState): void {
        this.isLoading = getIsLoadingCurrentAttribute(state);
        const attribute: IAttributeDetailV2 = getCurrentAttribute(state);
        if (attribute && attribute.id === this.stateService.params.attributeId) {
            this.title = attribute.name;
            this.isShowingOptionsTab = this.attributeLogicService.canAddOptions(attribute);
            this.canEditAttribute = !attribute.required;
        } else {
            this.title = "";
        }
    }
}
