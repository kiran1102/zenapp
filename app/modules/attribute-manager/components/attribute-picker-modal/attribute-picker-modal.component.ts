// 3rd Party
import {
    Inject,
    OnInit,
    ViewChild,
    Component,
} from "@angular/core";
import {
    IListItem,
    IListState,
} from "@onetrust/vitreus";

// Redux
import { getModalData } from "oneRedux/reducers/modal.reducer";
import { getAttributeMap } from "oneRedux/reducers/inventory-record.reducer";

// Services
import { AttributeGroupsActionService } from "attributeManagerModule/services/attribute-groups-action.service";
import { ModalService } from "sharedServices/modal.service";
import { InventoryService } from "inventorySharedModule/shared/services/inventory-api.service";

// Tokens
import { StoreToken } from "tokens/redux-store.token";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

// Interfaces
import { IStore } from "interfaces/redux.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";
import { IAttributeGroup } from "interfaces/attributes.interface";
import { IAttributePickerModalResolve } from "interfaces/attributes.interface";
import { IAttributeDetailV2 } from "interfaces/inventory.interface";
import { IStringMap } from "interfaces/generic.interface";
import { OtDuelingPicklistComponent } from "interfaces/ot-dueling-picklist.interface";
interface IFormattedOption {
    fieldName: string;
    labelKey: string;
}

@Component({
    selector: "attribute-picker-modal",
    templateUrl: "./attribute-picker-modal.component.html",
})
export class AttributePickerModal implements OnInit {

    isSaving = false;
    attributeMap: IStringMap<IAttributeDetailV2> = {};
    formattedInitialUngroupedAttributes: IFormattedOption[] = [];
    formattedInitialGroupAttributes: IFormattedOption[] = [];
    ungroupedAttributes: IListItem[] = [];
    groupAttributes: IListItem[] = [];

    modalData: IAttributePickerModalResolve;

    @ViewChild("duelingPicklist")
    private duelingListComponent: OtDuelingPicklistComponent;

    constructor(
        @Inject(StoreToken) private store: IStore,
        readonly attributeGroupsActionService: AttributeGroupsActionService,
        readonly translatePipe: TranslatePipe,
        readonly modalService: ModalService,
        readonly inventoryService: InventoryService,
    ) { }

    ngOnInit() {
        this.modalData = getModalData(this.store.getState());
        this.attributeMap = getAttributeMap(this.store.getState());
        this.formattedInitialUngroupedAttributes = this.formatAttributesForList(this.modalData.ungroupedAttributes);
        this.formattedInitialGroupAttributes = this.formatAttributesForList(this.modalData.group.attributes);
    }

    formatAttributesForList(fieldNames: string[]): IFormattedOption[] {
        const attributeList: IFormattedOption[] = [];
        fieldNames.forEach((fieldName: string) => {
            if (this.attributeMap[fieldName]) {
                attributeList.push({
                    fieldName,
                    labelKey: this.attributeMap[fieldName].nameKey ?
                        this.translatePipe.transform(this.attributeMap[fieldName].nameKey) :
                        this.attributeMap[fieldName].name,
                });
            }
        });
        return attributeList;
    }

    save() {
        this.isSaving = true;
        this.getCurrentListState();
        this.inventoryService.updateAttributesInGroup(
            this.modalData.inventoryId,
            this.modalData.group.id,
            this.getCurrentAttributes(),
        ).then((response: IProtocolResponse<void>) => {
            if (response.result) {
                this.inventoryService.getAttributeGroups(this.modalData.inventoryId).then((res: IProtocolResponse<IAttributeGroup[]>) => {
                    if (res.result) {
                        this.attributeGroupsActionService.setGroupsData(res.data);
                        this.modalService.closeModal();
                        this.modalService.clearModalData();
                    }
                    this.isSaving = false;
                });
            } else {
                this.isSaving = false;
            }
        });
    }

    cancel() {
        this.modalService.closeModal();
        this.modalService.clearModalData();
    }

    getCurrentAttributes(): string[] {
        return this.groupAttributes.map((attribute: {fieldName: string, labelKey: string}): string => attribute.fieldName);
    }

    getCurrentListState() {
        const currentList: IListState = this.duelingListComponent.getListState();
        this.ungroupedAttributes = currentList.left;
        this.groupAttributes = currentList.right;
     }
}
