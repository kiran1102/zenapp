// Rxjs
import { Subject } from "rxjs";
import {
    startWith,
    takeUntil,
} from "rxjs/operators";

// 3rd party
import {
    Component,
    Inject,
    Input,
} from "@angular/core";
import { StateService } from "@uirouter/core";
import {
    filter,
    sortBy,
} from "lodash";

// Redux
import observableFromStore from "oneRedux/utils/observable-from-store";
import { getCurrentAttribute } from "oneRedux/reducers/attribute.reducer";

// Services
import { AttributeActionService } from "attributeManagerModule/services/attribute-action.service";
import { Permissions } from "modules/shared/services/helper/permissions.service";
import { AttributeLogicService } from "attributeManagerModule/services/attribute-logic.service";

// Constants / Enums
import {
    InventoryPermissions,
    InventoryOptionStatuses,
} from "constants/inventory.constant";

// Tokens
import { StoreToken } from "tokens/redux-store.token";

// Interfaces
import {
    IAttributeDetailV2,
    IAttributeOptionV2,
    IAttributeColumn,
} from "interfaces/inventory.interface";
import { IStringMap } from "interfaces/generic.interface";
import { IDropdownOption } from "interfaces/one-dropdown.interface";
import {
    IStore,
    IStoreState,
} from "interfaces/redux.interface";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

@Component({
    selector: "attribute-manager-options",
    templateUrl: "./attribute-manager-options.component.html",
})
export class AttributeManagerOptionsComponent {

    @Input() status: string;

    columns: Array<IAttributeColumn<IAttributeOptionV2>>;
    rows: IAttributeOptionV2[];
    attributePermissions: IStringMap<boolean>;
    permissions = InventoryPermissions[this.stateService.params.id];
    menuClass: string;
    title: string;
    private destroy$: Subject<null> = new Subject();

    constructor(
        public attributeLogicService: AttributeLogicService,
        public translate: TranslatePipe,
        private attributeActionService: AttributeActionService,
        private permissionsService: Permissions,
        private stateService: StateService,
        @Inject(StoreToken) private store: IStore,
    ) {
        this.columns = [{
            label: this.translate.transform("Key"),
            getCellValue: (row: IAttributeOptionV2): string => row.value,
            tooltipKey: "InventoryManagerKeyColumnDescription",
            isDetailLink: true,
        },
        {
            label: this.translate.transform("Name"),
            getCellValue: (row: IAttributeOptionV2): string => this.translate.transform(row.valueKey) || row.value,
            isDetailLink: true,
        }];
        this.attributePermissions = {
            canDisableOptions: this.permissionsService.canShow(this.permissions.disableAttributeOptions),
            canEnableOptions: this.permissionsService.canShow(this.permissions.enableAttributeOptions),
            canEditOption: this.permissionsService.canShow(this.permissions.editAttributeOptions),
        };
    }

    columnTrackBy(index: number): number {
        return index;
    }

    ngOnInit(): void {
        this.title = this.translate.transform(this.status === InventoryOptionStatuses.Enabled ? "ActiveOptions" : "InactiveOptions");
        observableFromStore(this.store).pipe(
            startWith(this.store.getState()),
            takeUntil(this.destroy$),
        ).subscribe((state: IStoreState): void =>
            this.componentWillReceiveState(state),
        );
    }

    getActions(row: IAttributeOptionV2): () => IDropdownOption[] {
        return (): IDropdownOption[] => {
            const actions: IDropdownOption[] = [];
            if (this.attributePermissions.canDisableOptions && this.status === InventoryOptionStatuses.Enabled) {
                actions.push({
                    text: this.translate.transform("DeactivateOption"),
                    action: (): void => { this.attributeActionService.launchDisableOptionModal(this.stateService.params.attributeId, row); },
                });
            }
            if (this.attributePermissions.canEnableOptions && this.status === InventoryOptionStatuses.Disabled) {
                actions.push({
                    text: this.translate.transform("ReactivateOption"),
                    action: (): void => { this.attributeActionService.launchEnableOptionModal(this.stateService.params.attributeId, row); },
                });
            }
            if (this.attributePermissions.canEditOption) {
                actions.push({
                    text: this.translate.transform("EditKey"),
                    action: (): void => { this.attributeActionService.launchEditOptionModal(this.stateService.params.attributeId, row); },
                });
            }
            if (this.attributeLogicService.canTranslate(row.valueKey)) {
                actions.push({
                    text: this.translate.transform("TranslateOption"),
                    action: (): void => { this.attributeActionService.launchTranslateModal(row.valueKey, this.stateService.params.id); },
                });
            }
            return actions;
        };
    }

    ngOnDestroy(): void {
        this.destroy$.next();
        this.destroy$.complete();
    }

    private componentWillReceiveState(state: IStoreState): void {
        const attribute: IAttributeDetailV2 = getCurrentAttribute(state);
        if (attribute && attribute.id === this.stateService.params.attributeId) {
            this.rows = sortBy(
                filter(attribute.values, (option: IAttributeOptionV2): boolean => option.status === this.status),
                (option: IAttributeOptionV2): string => option.value.toLowerCase().trim(),
            );
        }
    }

    private setMenuClass(event: MouseEvent): void | undefined {
        this.menuClass = "actions-table__context-list";
        if ((window.innerHeight - 200) / 2 < (event.clientY - 200)) {
            this.menuClass += " actions-table__context-list--above";
        }
    }

}
