// 3rd Party
import {
    Inject,
    OnInit,
    Component,
} from "@angular/core";

// Redux
import { getModalData } from "oneRedux/reducers/modal.reducer";

// Services
import { ModalService } from "sharedServices/modal.service";
import { AttributeGroupsActionService } from "attributeManagerModule/services/attribute-groups-action.service";
import { LocalizationActionService } from "settingsServices/actions/localization-action.service";
import { TenantTranslationService } from "sharedServices/tenant-translation.service";
import { InventoryService } from "inventorySharedModule/shared/services/inventory-api.service";

// Tokens
import { StoreToken } from "tokens/redux-store.token";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

// Interfaces
import { IStore } from "interfaces/redux.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";
import { IAttributeGroup } from "interfaces/attributes.interface";
import { IAttributeGroupModalResolve } from "interfaces/attributes.interface";

@Component({
    selector: "attribute-group-modal",
    templateUrl: "./attribute-group-modal.component.html",
})
export class AttributeGroupModal implements OnInit {

    isSaving = false;
    modalData: IAttributeGroupModalResolve;
    groupName: string;

    constructor(
        @Inject(StoreToken) private store: IStore,
        @Inject("TenantTranslationService") readonly tenantTranslationService: TenantTranslationService,
        readonly attributeGroupsActionService: AttributeGroupsActionService,
        readonly translatePipe: TranslatePipe,
        private inventoryService: InventoryService,
        readonly modalService: ModalService,
        readonly localizationActions: LocalizationActionService,
    ) {}

    ngOnInit() {
        this.modalData = getModalData(this.store.getState());
        this.groupName = this.modalData.group.name;
    }

    setGroupName(value: string): void {
        this.groupName = value.trim();
    }

    save() {
        this.isSaving = true;
        if (this.modalData.isNew) {
            this.attributeGroupsActionService.addGroup(
                this.modalData.inventoryId,
                this.groupName,
            ).then((response: IProtocolResponse<IAttributeGroup>) => {
                if (response.result) {
                    this.tenantTranslationService.refreshTermCurrentLanguage(response.data.nameKey);
                    this.attributeGroupsActionService.addGroupToView(response.data);
                    this.modalService.closeModal();
                    this.modalService.clearModalData();
                }
                this.isSaving = false;
            });
        } else {
            this.attributeGroupsActionService.updateGroup(
                this.modalData.inventoryId,
                this.modalData.group.id,
                { name: this.groupName },
            ).then((response: IProtocolResponse<IAttributeGroup>) => {
                if (response.result) {
                    this.tenantTranslationService.refreshTermCurrentLanguage(this.modalData.group.nameKey);
                    this.inventoryService.getAttributeGroups(this.modalData.inventoryId).then((res: IProtocolResponse<IAttributeGroup[]>) => {
                        if (res.result) {
                            this.attributeGroupsActionService.setGroupsData(res.data);
                        }
                        this.modalService.closeModal();
                        this.modalService.clearModalData();
                    });
                }
                this.isSaving = false;
            });
        }
    }

    cancel() {
        this.modalService.closeModal();
        this.modalService.clearModalData();
    }
}
