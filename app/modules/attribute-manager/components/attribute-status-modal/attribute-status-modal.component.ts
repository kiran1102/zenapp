// 3rd party
import {
    Component,
    Inject,
} from "@angular/core";

// Redux
import { getModalData } from "oneRedux/reducers/modal.reducer";

// Services
import { ModalService } from "sharedServices/modal.service";
import { InventoryService } from "inventorySharedModule/shared/services/inventory-api.service";

// Tokens
import { StoreToken } from "tokens/redux-store.token";

// Interfaces
import { IStore } from "interfaces/redux.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";
import {
    IAttributeStatusModalResolve,
    IAttributeDetailV2,
} from "interfaces/inventory.interface";

@Component({
    selector: "attribute-status-modal",
    templateUrl: "./attribute-status-modal.component.html",
})
export class AttributeStatusModalComponent {

    isLoading: boolean;
    modalData: IAttributeStatusModalResolve;
    attributeId: string;

    constructor(
        @Inject(StoreToken) private store: IStore,
        private modalService: ModalService,
        private inventoryService: InventoryService,
    ) { }

    ngOnInit(): void {
        this.modalData = getModalData(this.store.getState());
    }

    closeModal(): void {
        this.modalService.closeModal();
        this.modalService.clearModalData();
    }

    submitChanges(): void {
        this.isLoading = true;
        this.inventoryService.editAttributeStatus(this.modalData.schemaId, this.modalData.attributeId, this.modalData.newStatus)
            .then((response: IProtocolResponse<IAttributeDetailV2>): void => {
                if (response.result) {
                    this.modalService.handleModalCallback();
                    this.closeModal();
                }
                this.isLoading = false;
            });
    }

}
