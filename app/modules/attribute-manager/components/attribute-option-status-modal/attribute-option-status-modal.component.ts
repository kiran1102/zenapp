// 3rd party
import {
    Component,
    Inject,
} from "@angular/core";

// Redux
import { getModalData } from "oneRedux/reducers/modal.reducer";

// Services
import { ModalService } from "sharedServices/modal.service";
import { InventoryService } from "inventorySharedModule/shared/services/inventory-api.service";

// Tokens
import { StoreToken } from "tokens/redux-store.token";

// Interfaces
import { IStore } from "interfaces/redux.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";
import {
    IAttributeOptionV2,
    IAttributeOptionStatusModalResolve,
} from "interfaces/inventory.interface";

@Component({
    selector: "attribute-option-status-modal",
    templateUrl: "./attribute-option-status-modal.component.html",
})
export class AttributeOptionStatusModalComponent {

    isLoading: boolean;
    modalData: IAttributeOptionStatusModalResolve;
    attributeId: string;

    constructor(
        @Inject(StoreToken) private store: IStore,
        private modalService: ModalService,
        private inventoryService: InventoryService,
    ) { }

    ngOnInit(): void {
        this.modalData = getModalData(this.store.getState());
    }

    closeModal(): void {
        this.modalService.closeModal();
        this.modalService.clearModalData();
    }

    submitChanges(): void {
        this.isLoading = true;
        const newOption: IAttributeOptionV2 = { ...this.modalData.option, status: this.modalData.newStatus };
        this.inventoryService.editAttributeOption(this.modalData.attributeId, newOption).then((response: IProtocolResponse<IAttributeOptionV2>): void => {
            if (response.result) {
                this.modalService.handleModalCallback();
                this.closeModal();
            }
            this.isLoading = false;
        });
    }

}
