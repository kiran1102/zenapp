// 3rd party
import {
    Component,
    Inject,
} from "@angular/core";
import {
    FormGroup,
    FormControl,
    Validators,
    ValidationErrors,
} from "@angular/forms";

// Redux
import { getModalData } from "oneRedux/reducers/modal.reducer";

// Services
import { ModalService } from "sharedServices/modal.service";
import { InventoryService } from "inventorySharedModule/shared/services/inventory-api.service";
import { TenantTranslationService } from "sharedServices/tenant-translation.service";

// Tokens
import { StoreToken } from "tokens/redux-store.token";

// Enums
import { StatusCodes } from "enums/status-codes.enum";

// Interfaces
import { IStore } from "interfaces/redux.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";
import { IStringMap } from "interfaces/generic.interface";
import {
    IAttributeEditOptionModalResolve,
    IAttributeOptionV2,
} from "interfaces/inventory.interface";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

@Component({
    selector: "edit-attribute-option-modal",
    templateUrl: "./edit-attribute-option-modal.component.html",
})
export class EditAttributeOptionModalComponent {

    isLoading: boolean;
    modalForm: FormGroup;
    modalData: IAttributeEditOptionModalResolve;
    hasConflict: boolean;
    conflictMap: IStringMap<boolean> = {};

    constructor(
        @Inject(StoreToken) private store: IStore,
        @Inject("TenantTranslationService") private tenantTranslationService: TenantTranslationService,
        private modalService: ModalService,
        private inventoryService: InventoryService,
        private translatePipe: TranslatePipe,
    ) { }

    ngOnInit(): void {
        this.modalData = getModalData(this.store.getState());
        this.modalForm = new FormGroup({
            option: new FormControl(this.modalData.option.value, Validators.compose([Validators.required, this.duplicateValidator])),
        });
    }

    closeModal(): void {
        this.modalService.closeModal();
        this.modalService.clearModalData();
    }

    submitChanges(): void {
        this.isLoading = true;
        const updatedOption: IAttributeOptionV2 = { ...this.modalData.option, value: this.modalForm.value.option };
        this.inventoryService.editAttributeOption(this.modalData.attributeId, updatedOption).then((response: IProtocolResponse<IAttributeOptionV2>): void => {
            if (response.result) {
                if (response.data && response.data.valueKey) {
                    this.tenantTranslationService.refreshTermCurrentLanguage(response.data.valueKey);
                }
                this.modalService.handleModalCallback();
                this.closeModal();
            } else if (response.status === StatusCodes.Conflict) {
                this.conflictMap[this.modalForm.value.option] = true;
            }
            this.isLoading = false;
        });
    }

    private duplicateValidator = (option: FormControl): ValidationErrors | null => {
        if (this.conflictMap[option.value]) {
            return { hasConflict: this.translatePipe.transform("DuplicateAttributeOptionError") };
        }
        return null;
    }

}
