// Rxjs
import { Subject } from "rxjs";
import {
    startWith,
    takeUntil,
} from "rxjs/operators";

// 3rd party
import {
    Component,
    Inject,
    Input,
} from "@angular/core";
import { StateService } from "@uirouter/core";
import {
    sortBy,
    filter,
} from "lodash";

// Redux
import { getAttributeList } from "oneRedux/reducers/attribute.reducer";
import observableFromStore from "oneRedux/utils/observable-from-store";

// Services
import { Permissions } from "modules/shared/services/helper/permissions.service";
import { AttributeActionService } from "attributeManagerModule/services/attribute-action.service";
import { AttributeLogicService } from "attributeManagerModule/services/attribute-logic.service";

// Tokens
import { StoreToken } from "tokens/redux-store.token";

// Constants / Enums
import {
    ResponseTypeTranslationKeys,
    InventoryPermissions,
    AttributeStatuses,
} from "constants/inventory.constant";

// Interfaces
import {
    IAttributeV2,
    IAttributeColumn,
} from "interfaces/inventory.interface";
import { IStringMap } from "interfaces/generic.interface";
import { IDropdownOption } from "interfaces/one-dropdown.interface";
import {
    IStore,
    IStoreState,
} from "interfaces/redux.interface";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

@Component({
    selector: "attribute-manager-table",
    templateUrl: "./attribute-manager-table.component.html",
})
export class AttributeManagerTableComponent {
    @Input() status: string;

    title: string;
    columns: Array<IAttributeColumn<IAttributeV2>>;
    rows: IAttributeV2[];
    attributePermissions: IStringMap<boolean>;
    permissions = InventoryPermissions[this.stateService.params.id];
    menuClass: string;
    isVendorRoute: boolean;
    private destroy$: Subject<null> = new Subject();

    constructor(
        private stateService: StateService,
        @Inject(StoreToken) private store: IStore,
        readonly attributeActionService: AttributeActionService,
        readonly attributeLogicService: AttributeLogicService,
        readonly translate: TranslatePipe,
        private permissionsService: Permissions,
    ) {
        this.columns = [
            {
                label: this.translate.transform("Key"),
                getCellValue: (row: IAttributeV2): string => row.name,
                tooltipKey: "InventoryManagerKeyColumnDescription",
                isDetailLink: true,
            },
            {
                label: this.translate.transform("Name"),
                getCellValue: (row: IAttributeV2): string => this.translate.transform(row.nameKey) || row.name,
            },
            {
                label: this.translate.transform("Description"),
                getCellValue: (row: IAttributeV2): string => this.translate.transform(row.descriptionKey) || row.description,
            },
            {
                label: this.translate.transform("ResponseType"),
                getCellValue: (row: IAttributeV2): string => this.translate.transform(ResponseTypeTranslationKeys[row.responseType]),
            },
        ];
        this.attributePermissions = {
            canAdd: this.permissionsService.canShow(this.permissions.addAttributes),
        };
    }

    ngOnInit() {
        this.title = this.translate.transform(this.status === AttributeStatuses.Enabled ? "ActiveAttributes" : "InactiveAttributes");
        observableFromStore(this.store).pipe(
            startWith(this.store.getState()),
            takeUntil(this.destroy$),
        ).subscribe((state: IStoreState) =>
            this.componentWillReceiveState(state),
        );
        this.isVendorRoute =  this.stateService.current.name.includes("zen.app.pia.module.vendor.attribute-manager");
    }

    columnTrackBy(column: IAttributeV2): string {
        return column.id;
    }

    goToDetails(attribute: IAttributeV2) {
        this.stateService.go(this.isVendorRoute ? "zen.app.pia.module.vendor.attribute-manager-details" : "zen.app.pia.module.datamap.views.attribute-manager.details", {
            id: this.stateService.params.id,
            attributeId: attribute.id,
        });
    }

    getActions(row: IAttributeV2): () => IDropdownOption[] {
        return (): IDropdownOption[] => {
            const options: IDropdownOption[] = [{
                text: this.translate.transform("EditKey"),
                action: () => {
                    this.attributeActionService.openEditModal(
                        this.stateService.params.id,
                        row.id,
                        () => this.attributeActionService.getAttributeList(this.stateService.params.id),
                    );
                },
            }];
            if (this.attributeLogicService.canAddOptions(row)) {
                options.push({
                    text: this.translate.transform("AddOptions"),
                    action: () => { this.attributeActionService.openOptionsModal(this.stateService.params.id, row.id); },
                });
            }
            if (this.attributeLogicService.canDisable(row)) {
                options.push({
                    text: this.translate.transform("DeactivateAttribute"),
                    action: () => { this.attributeActionService.launchDisableAttributeModal(row.id, this.stateService.params.id); },
                });
            }
            if (this.status === AttributeStatuses.Disabled) {
                options.push({
                    text: this.translate.transform("ReactivateAttribute"),
                    action: () => { this.attributeActionService.launchEnableAttributeModal(row.id, this.stateService.params.id); },
                });
            }
            if (this.attributeLogicService.canTranslate(row.nameKey)) {
                options.push({
                    text: this.translate.transform("TranslateName"),
                    action: () => { this.attributeActionService.launchTranslateModal(row.nameKey, this.stateService.params.id); },
                });
            }
            if (this.attributeLogicService.canTranslate(row.descriptionKey)) {
                options.push({
                    text: this.translate.transform("TranslateDescription"),
                    action: () => { this.attributeActionService.launchTranslateModal(row.descriptionKey, this.stateService.params.id); },
                });
            }
            return options;
        };
    }

    ngOnDestroy() {
        this.destroy$.next();
        this.destroy$.complete();
    }

    private setMenuClass(event: MouseEvent) {
        this.menuClass = "actions-table__context-list";
        if ((window.innerHeight - 200) / 2 < (event.clientY - 200)) {
            this.menuClass += " actions-table__context-list--above";
        }
    }

    private componentWillReceiveState(state: IStoreState) {
        this.rows = sortBy(
            filter(getAttributeList(state), (attribute: IAttributeV2): boolean => attribute.status === this.status),
            (attribute: IAttributeV2): string => attribute.name.toLowerCase().trim(),
        );
    }

}
