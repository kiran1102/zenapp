// 3rd party
import {
    Component,
    Inject,
} from "@angular/core";
import { StateService } from "@uirouter/core";
import { filter } from "lodash";

// Services
import { Permissions } from "modules/shared/services/helper/permissions.service";

// Constants / Enums
import {
    InventoryTranslationKeys,
    InventoryPermissions,
 } from "constants/inventory.constant";
import { InventorySchemaIds } from "constants/inventory-config.constant";

 // Pipes
import { TranslatePipe } from "pipes/translate.pipe";

@Component({
    selector: "attribute-manager-entry",
    templateUrl: "./attribute-manager-entry.component.html",
})
export class AttributeManagerEntryComponent {

    inventories: Array<{label: string, id: string}> = [];

    constructor(
        private stateService: StateService,
        private permissions: Permissions,
        private translatePipe: TranslatePipe,
    ) {
        const allInventories: Array<{label: string, id: string}> = [
            { label: this.translatePipe.transform(InventoryTranslationKeys[InventorySchemaIds.Assets].attributes), id: InventorySchemaIds.Assets },
            { label: this.translatePipe.transform(InventoryTranslationKeys[InventorySchemaIds.Processes].attributes), id: InventorySchemaIds.Processes },
        ];
        if (this.permissions.canShow("DataMappingEntitiesInventory")) {
            allInventories.push({ label: this.translatePipe.transform(InventoryTranslationKeys[InventorySchemaIds.Entities].attributes), id: InventorySchemaIds.Entities });
        }
        this.inventories = filter(allInventories, (inventory: {label: string, id: string}): boolean => this.permissions.canShow(InventoryPermissions[inventory.id].viewAttributes));
    }

    public goToList(id: string): void {
        this.stateService.go("zen.app.pia.module.datamap.views.attribute-manager.list", {id});
    }
}
