// Angular
import {
    Component,
    Input,
    OnInit,
    Inject,
} from "@angular/core";

// Rxjs
import { Subject } from "rxjs";
import {
    startWith,
    takeUntil,
} from "rxjs/operators";

// Redux
import { StoreToken } from "tokens/redux-store.token";
import { getAttributeMap } from "oneRedux/reducers/inventory-record.reducer";
import observableFromStore from "oneRedux/utils/observable-from-store";

// Services
import { AttributeGroupsActionService } from "attributeManagerModule/services/attribute-groups-action.service";
import { ModalService } from "sharedServices/modal.service";
import { InventoryService } from "inventorySharedModule/shared/services/inventory-api.service";
import { AttributeActionService } from "attributeManagerModule/services/attribute-action.service";

// Interfaces
import { IDeleteConfirmationModalResolve } from "interfaces/delete-confirmation-modal-resolve.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";
import {
    IAttributeGroupModalResolve,
    IAttributePickerModalResolve,
} from "interfaces/attributes.interface";
import { IStringMap } from "interfaces/generic.interface";
import {
    IAttributeGroup,
    IAttributeGroupsState,
} from "interfaces/attributes.interface";
import { IAttributeDetailV2 } from "interfaces/inventory.interface";
import {
    IStore,
    IStoreState,
} from "interfaces/redux.interface";

// Const
import { EmptyGuid } from "constants/empty-guid.constant";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

@Component({
    selector: "attribute-groups-builder",
    templateUrl: "./attribute-groups-builder.component.html",
})
export class AttributeGroupsBuilderComponent implements OnInit {

    @Input() inventoryId: string;

    groupsData: IAttributeGroup[];
    isSaving: boolean;
    emptyGuid = EmptyGuid;
    attributeMap: IStringMap<IAttributeDetailV2> = {};
    ungroupedAttributes: string[] = [];
    destroy$ = new Subject();

    constructor(
        @Inject(StoreToken) private store: IStore,
        readonly attributeGroupsActionService: AttributeGroupsActionService,
        private inventoryService: InventoryService,
        private modalService: ModalService,
        private translatePipe: TranslatePipe,
        private attributeActionService: AttributeActionService,
    ) {}

    ngOnInit() {
        observableFromStore(this.store).pipe(
            startWith(this.store.getState()),
            takeUntil(this.destroy$),
        ).subscribe((state: IStoreState) => this.componentWillReceiveState(state));
        this.attributeGroupsActionService.attributeGroupsView$.subscribe((viewData: { groups: IAttributeGroup[] }) => {
            this.groupsData = viewData.groups;
            this.ungroupedAttributes = this.attributeGroupsActionService.getUngroupedAttributes(this.groupsData);
        });
        this.attributeGroupsActionService.attributeGroupsState$.subscribe((stateData: IAttributeGroupsState) => {
            this.isSaving = stateData.isSaving;
        });
    }

    addGroup() {
        this.openGroupModal({name: ""}, true);
    }

    openGroupModal(group: IAttributeGroup, isNew: boolean = false) {
        const modalData: IAttributeGroupModalResolve = {
            title: isNew ? this.translatePipe.transform("AddGroup") : this.translatePipe.transform("EditGroup"),
            isNew,
            group,
            inventoryId: this.inventoryId,
        };
        this.modalService.setModalData(modalData);
        this.modalService.openModal("attributeGroupModal");
    }

    openTranslationModal(group: IAttributeGroup) {
        this.attributeActionService.launchTranslateModal(group.nameKey, this.inventoryId);
    }

    openManageAttributesModal(group: IAttributeGroup) {
        const modalData: IAttributePickerModalResolve = {
            group,
            inventoryId: this.inventoryId,
            ungroupedAttributes: this.ungroupedAttributes,
        };
        this.modalService.setModalData(modalData);
        this.modalService.openModal("attributePickerModal");
    }

    trackByIndex(index: number): number {
        return index;
    }

    moveGroup(groupId: string, previousGroupId: string) {
        this.attributeGroupsActionService.setSavingState(true);
        this.attributeGroupsActionService.moveGroup(this.inventoryId, groupId, { previousGroupId }).then((response: IProtocolResponse<void>) => {
            if (response.result) {
                this.inventoryService.getAttributeGroups(this.inventoryId).then((res: IProtocolResponse<IAttributeGroup[]>) => {
                    if (res.result) {
                        this.attributeGroupsActionService.setGroupsData(res.data);
                        this.attributeGroupsActionService.setSavingState(false);
                    } else {
                        this.attributeGroupsActionService.setSavingState(false);
                    }
                });
            } else {
                this.attributeGroupsActionService.setSavingState(false);
            }
        });
    }

    deleteGroup(group: IAttributeGroup) {
        const modalData: IDeleteConfirmationModalResolve = {
            modalTitle: this.translatePipe.transform("DeleteGroup"),
            confirmationText: this.translatePipe.transform("AreYouSureDeleteGroup"),
            promiseToResolve: (): ng.IPromise<boolean> =>
                this.attributeGroupsActionService.deleteGroup(
                    this.inventoryId,
                    group.id,
                ).then((response: IProtocolResponse<void>): boolean | ng.IPromise<boolean> => {
                    if (response.result) {
                        return this.inventoryService.getAttributeGroups(this.inventoryId).then((res: IProtocolResponse<IAttributeGroup[]>) => {
                            if (res.result) {
                                this.attributeGroupsActionService.setGroupsData(res.data);
                            }
                            return res.result;
                        });
                    } else {
                        return response.result;
                    }
                }),
        };
        this.modalService.setModalData(modalData);
        this.modalService.openModal("otDeleteConfirmationModal");
    }

    getAttributeLabel = (attribute: string): string => {
        return this.attributeMap[attribute].nameKey ? this.translatePipe.transform(this.attributeMap[attribute].nameKey) : this.attributeMap[attribute].name;
    }

    isAttributeInMap = (attribute: string): boolean => {
        return Boolean(this.attributeMap[attribute]);
    }

    private componentWillReceiveState(state) {
        this.attributeMap = getAttributeMap(state);
    }
}
