// Rxjs
import { Subject } from "rxjs";
import {
    startWith,
    takeUntil,
} from "rxjs/operators";

// 3rd party
import {
    Component,
    Inject,
} from "@angular/core";
import { StateService } from "@uirouter/core";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

// Redux
import { getIsLoadingAttributeList } from "oneRedux/reducers/attribute.reducer";
import observableFromStore from "oneRedux/utils/observable-from-store";

// Services
import { Permissions } from "modules/shared/services/helper/permissions.service";
import { AttributeActionService } from "attributeManagerModule/services/attribute-action.service";
import { AttributeGroupsActionService } from "attributeManagerModule/services/attribute-groups-action.service";

// Tokens
import { StoreToken } from "tokens/redux-store.token";

// Constants / Enums
import {
    InventoryTranslationKeys,
    InventoryPermissions,
    AttributeStatuses,
    AttributeManagerTabs,
    ModuleContext,
} from "constants/inventory.constant";

// Interfaces
import { IStringMap } from "interfaces/generic.interface";
import { ITab } from "interfaces/ot-tabs.interface";
import { IStore, IStoreState } from "interfaces/redux.interface";
import { IAttributeGroupsState } from "interfaces/attributes.interface";

@Component({
    selector: "attribute-manager",
    templateUrl: "./attribute-manager.component.html",
})
export class AttributeManagerComponent {

    moduleContext = ModuleContext;
    module: string;
    tabIds = AttributeManagerTabs;
    title: string;
    loadingAttributes: boolean;
    hideBreadCrumbs: boolean;
    attributePermissions: IStringMap<boolean>;
    inventoryId = this.stateService.params.id;
    permissions = InventoryPermissions[this.inventoryId];
    attributeStatuses: IStringMap<string> = AttributeStatuses;
    attributeManagerTabs: ITab[] = [{
        id: this.tabIds.Attributes,
        text: this.translatePipe.transform("Attributes"),
        otAutoId: "AttributeManagerAttributesTab",
    }, {
        id: this.tabIds.Groups,
        text: this.translatePipe.transform("Groups"),
        otAutoId: "AttributeManagerGroupsTab",
    }];
    selectedTab: string;
    isLoadingGroups = false;
    private destroy$: Subject<null> = new Subject();

    constructor(
        @Inject(StoreToken) private store: IStore,
        private stateService: StateService,
        private permissionsService: Permissions,
        readonly attributeActionService: AttributeActionService,
        readonly attributeGroupsActionService: AttributeGroupsActionService,
        private translatePipe: TranslatePipe,
    ) {
        this.title = this.translatePipe.transform(InventoryTranslationKeys[this.inventoryId].attributes);
        this.attributePermissions = {
            canAdd: this.permissionsService.canShow(this.permissions.addAttributes),
            canGroup: this.permissionsService.canShow(this.permissions.attributeGrouping),
        };
    }

    ngOnInit(): void {
        observableFromStore(this.store).pipe(
            startWith(this.store.getState()),
            takeUntil(this.destroy$),
        ).subscribe((state: IStoreState): void =>
            this.componentWillReceiveState(state),
        );
        this.module = this.stateService.params && this.stateService.params.context ? this.stateService.params.context : ModuleContext.Inventory;
        if (this.attributePermissions.canGroup) {
            this.attributeGroupsActionService.attributeGroupsState$.subscribe((stateData: IAttributeGroupsState) => {
                this.isLoadingGroups = stateData.isLoadingGroups;
            });
            this.attributeGroupsActionService.setAttributeGroupsAndSchema(this.inventoryId);
        }
        this.selectedTab = this.attributeManagerTabs[0].id;
        this.attributeActionService.getAttributeList(this.inventoryId);
        this.hideBreadCrumbs = this.stateService.includes("zen.app.pia.module.vendor.attribute-manager") || this.stateService.includes("zen.app.pia.module.vendor.inventory-manager");
    }

    onTabClick(tab: ITab) {
        this.selectedTab = tab.id;
    }

    addAttribute(): void {
        this.attributeActionService.openAddModal(this.inventoryId, () => {
            this.attributeActionService.getAttributeList(this.inventoryId);
            if (this.attributePermissions.canGroup) {
                this.attributeGroupsActionService.setAttributeGroupsAndSchema(this.inventoryId);
            }
        });
    }

    goToListView(route: { path: string, params: any }) {
        this.stateService.go(route.path);
    }

    ngOnDestroy(): void {
        this.destroy$.next();
        this.destroy$.complete();
    }

    private componentWillReceiveState(state: IStoreState): void {
        this.loadingAttributes = getIsLoadingAttributeList(state);
    }

}
