// Rxjs
import { Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";

// 3rd party
import {
    Component,
    Inject,
} from "@angular/core";
import { StateService } from "@uirouter/core";
import {
    FormGroup,
    FormControl,
    FormArray,
    Validators,
    ValidationErrors,
    ValidatorFn,
    AbstractControl,
} from "@angular/forms";
import {
    isFunction,
    filter,
    find,
    sortBy,
    includes,
} from "lodash";

// Redux
import { getModalData } from "oneRedux/reducers/modal.reducer";

// Services
import { ModalService } from "sharedServices/modal.service";
import { AttributeLogicService } from "attributeManagerModule/services/attribute-logic.service";
import { TenantTranslationService } from "sharedServices/tenant-translation.service";

// Constants & Enums
import {
    AttributeModalTypes,
    AttributeResponseTypes,
    InventoryOptionStatuses,
    AttributeTypes,
} from "constants/inventory.constant";
import { StatusCodes } from "enums/status-codes.enum";
import { Regex } from "constants/regex.constant";

// Tokens
import { StoreToken } from "tokens/redux-store.token";

// Interfaces
import { IStringMap, ILabelValue } from "interfaces/generic.interface";
import { IStore } from "interfaces/redux.interface";
import {
    IAttributeModalResolve,
    IAttributeOptionV2,
    IAttributeDetailV2,
    IAttributeTypeOption,
} from "interfaces/inventory.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

@Component({
    selector: "attribute-manager-modal",
    templateUrl: "./attribute-manager-modal.component.html",
})
export class AttributeManagerModalComponent {

    isLoading = true;
    responseTypeOptions: Array<ILabelValue<string>>;
    modalForm: FormGroup;
    optionsForm: FormGroup;
    detailsForm: FormGroup;
    isShowingNewOptions: boolean;
    isShowingCurrentOptions: boolean;
    isShowingDetails: boolean;
    isShowingChangeTypeMessage: boolean;
    isShowingAddAnother: boolean;
    isShowingTypeSelect: boolean;
    isShowingAllowOther: boolean;
    isShowingTypeChangeWarning: boolean;
    isShowingOptionToggle: boolean;
    isCustomListType: boolean;
    modalLabels: IStringMap<string>;
    modalData: IAttributeModalResolve;
    responseTypes: IStringMap<string> = AttributeResponseTypes;
    currentActiveOptions: IAttributeOptionV2[];
    modalTypes: IStringMap<string> = AttributeModalTypes;
    conflictMap: IStringMap<boolean> = {};
    originalType: string;
    attributeTypes = AttributeTypes;
    attributeTypeOptions: IAttributeTypeOption[];
    private destroy$: Subject<null> = new Subject();
    private modalText: IStringMap<IStringMap<string>>;
    private callbackOnClose = false;

    constructor(
        private stateService: StateService,
        @Inject(StoreToken) private store: IStore,
        @Inject("TenantTranslationService") private tenantTranslationService: TenantTranslationService,
        readonly attributeLogicService: AttributeLogicService,
        readonly translate: TranslatePipe,
        private modalService: ModalService,
    ) {
        this.modalText = {
            [AttributeModalTypes.Add]: {
                title: this.translate.transform("AddAttribute"),
            },
            [AttributeModalTypes.Options]: {
                title: this.translate.transform("AddOptions"),
            },
            [AttributeModalTypes.Edit]: {
                title: this.translate.transform("EditKey"),
            },
        };
        this.attributeTypeOptions = this.attributeLogicService.getAttributeTypeOptions(this.responseTypes.SingleSelect);
    }

    ngOnInit(): void {
        this.modalData = getModalData(this.store.getState());
        this.modalLabels = this.modalText[this.modalData.type];
        if (isFunction(this.modalData.getAttribute)) {
            this.modalData.getAttribute().then((attribute: IAttributeDetailV2): void => {
                if (attribute) {
                    this.modalData.attribute = attribute;
                    this.initModal();
                } else {
                    this.modalService.closeModal();
                }
            });
        } else {
            this.initModal();
        }
    }

    addOption(event?: KeyboardEvent): void {
        if (event) event.preventDefault();
        const newValue: string = this.optionsForm.get("newOptionInput").value;
        if (newValue && newValue.trim()) {
            const newOptionInput = this.optionsForm.get("newOptionInput") as FormControl;
            const options = this.optionsForm.get("options") as FormArray;
            options.push(this.createOption(newOptionInput.value));
            newOptionInput.reset();
        }
    }

    toggleListType() {
        this.isCustomListType = this.optionsForm.get("isCustomListType").value;
        const attributeType: AbstractControl = this.optionsForm.get("attributeType");
        const other: AbstractControl = this.detailsForm.get("other");

        this.optionsForm.get("isCustomListType").setValue(!this.isCustomListType);

        if (this.isCustomListType) {
            other.enable();
        } else if (
            attributeType &&
            attributeType.value &&
            attributeType.value.type &&
            attributeType.value.type === this.attributeTypes.Locations
        ) {
            other.setValue(false);
            other.disable();
        }
        this.validateSystemAttribute();
    }

    removeOption(optionIndex: number): void {
        const options = this.optionsForm.get("options") as FormArray;
        options.removeAt(optionIndex);
    }

    closeModal(): void {
        if (this.callbackOnClose) {
            this.modalService.handleModalCallback();
        }
        this.modalService.closeModal();
        this.modalService.clearModalData();
    }

    submitChanges(callback: (attribute?: IAttributeDetailV2) => void): void {
        this.isLoading = true;
        const attribute: IAttributeDetailV2 = this.formatAttributeForSubmit();
        this.modalData.saveAttribute(attribute).then((response: IProtocolResponse<IAttributeDetailV2>): void => {
            if (response.result) {
                if (
                    (!this.modalData.attribute ||
                    (this.modalData.attribute && attribute.name && this.modalData.attribute.name !== attribute.name))
                    && response.data
                    && response.data.nameKey
                ) {
                    this.tenantTranslationService.refreshTermCurrentLanguage(response.data.nameKey);
                }
                if (
                    (!this.modalData.attribute ||
                    (this.modalData.attribute && attribute.description && this.modalData.attribute.description !== attribute.description))
                    && response.data
                    && response.data.descriptionKey
                ) {
                    this.tenantTranslationService.refreshTermCurrentLanguage(response.data.descriptionKey);
                }
                callback(response.data);
            } else if (response.status === StatusCodes.Conflict) {
                this.conflictMap[attribute.name] = true;
            }
            if (response.status !== StatusCodes.Success && response.status !== StatusCodes.Created) {
                this.isLoading = false;
            }
        });
    }

    save(addAnother: boolean = false): void {
        this.callbackOnClose = true;
        this.submitChanges((response: IAttributeDetailV2): void => {
            if (addAnother) {
                this.initModal();
            } else {
                this.closeModal();
            }
        });
    }

    selectAttributeType(attributeType: IAttributeTypeOption): void {
        this.optionsForm.get("attributeType").setValue(attributeType);
        if (attributeType.type === this.attributeTypes.Locations) {
            this.detailsForm.get("other").setValue(false);
            this.detailsForm.get("other").disable();
        } else {
            this.detailsForm.get("other").enable();
        }
    }

    selectResponseType(responseType: ILabelValue<string>): void {
        this.detailsForm.get("type").setValue(responseType);
        this.attributeTypeOptions = this.attributeLogicService.getAttributeTypeOptions(responseType.value);
        this.validateSystemAttribute();
    }

    validateSystemAttribute() {
        const currentAttributeType: IAttributeTypeOption = this.optionsForm.get("attributeType").value;
        if (!this.isCustomListType && !includes(this.attributeTypeOptions, currentAttributeType)) {
            this.selectAttributeType(this.attributeTypeOptions[0]);
        }
    }

    goToTranslationEditor() {
        this.closeModal();
        this.stateService.go("zen.app.pia.module.settings.localization");
    }

    ngOnDestroy(): void {
        this.destroy$.next();
        this.destroy$.complete();
    }

    private initModal(): void {
        this.responseTypeOptions = this.attributeLogicService.getResponseTypeOptions(this.modalData.attribute);
        this.optionsForm = new FormGroup({
            attributeType: new FormControl({
                value: this.modalData.attribute ? this.modalData.attribute.attributeType : this.attributeTypeOptions[0],
                disabled: this.attributeLogicService.canEditAttributeType(this.modalData.attribute),
            }),
            isCustomListType: new FormControl(true),
            newOptionInput: new FormControl(""),
            options: new FormArray([]),
        }, this.optionsValidator);
        this.detailsForm = new FormGroup({
            name: new FormControl({
                value: this.modalData.attribute ? this.modalData.attribute.name : "",
                disabled: this.modalData.attribute && this.modalData.attribute.required,
            }, Validators.compose([this.requiredValidator, this.duplicateValidator])),
            description: new FormControl({
                value: this.modalData.attribute ? this.modalData.attribute.description : "",
                disabled: this.modalData.attribute && this.modalData.attribute.required,
            }, this.requiredValidator),
            type: new FormControl(
                this.modalData.attribute ?
                    find(this.responseTypeOptions, (option: ILabelValue<string>): boolean => this.modalData.attribute.responseType === option.value) :
                    this.responseTypeOptions[0],
                this.changedTypeValidator,
            ),
            other: new FormControl({
                value: this.modalData.attribute ? this.modalData.attribute.allowOther : false,
                disabled: false,
            }),
        });
        this.isShowingTypeSelect = this.attributeLogicService.canChangeType(this.modalData.attribute);
        this.isShowingAllowOther = this.attributeLogicService.canChangeAllowOther({...this.modalData.attribute, responseType: this.detailsForm.value.type.value});
        if (this.modalData.attribute) {
            this.originalType = this.modalData.attribute.responseType;
            this.currentActiveOptions = sortBy(
                filter(this.modalData.attribute.values, (option: IAttributeOptionV2): boolean => option.status === InventoryOptionStatuses.Enabled),
                (option: IAttributeOptionV2): string => option.value.toLowerCase().trim(),
            );
        }
        this.isShowingDetails = this.modalData.type !== AttributeModalTypes.Options;
        this.isShowingNewOptions = this.modalData.type === AttributeModalTypes.Options;
        this.isShowingCurrentOptions = this.modalData.type === AttributeModalTypes.Options;
        this.isShowingChangeTypeMessage = this.modalData.type === AttributeModalTypes.Add;
        this.isShowingAddAnother = this.modalData.type === AttributeModalTypes.Add;
        this.isShowingOptionToggle = this.modalData.type === AttributeModalTypes.Add;
        const form: { optionsForm?: FormGroup, detailsForm?: FormGroup } = {};
        if (this.isShowingNewOptions) form.optionsForm = this.optionsForm;
        if (this.isShowingDetails) form.detailsForm = this.detailsForm;
        if (this.modalData.type !== AttributeModalTypes.Options) {
            this.detailsForm.get("type").valueChanges.pipe(
                takeUntil(this.destroy$),
            ).subscribe((selection: ILabelValue<string>) => this.updateOptionsByType(selection.value));
        }
        this.modalForm = new FormGroup(form);
        this.isLoading = false;
    }

    private optionsValidator: ValidatorFn = (control: FormGroup): ValidationErrors | null => {
        this.isCustomListType = control.value.isCustomListType;
        const optionsList: IAttributeOptionV2[] = control.value.options;
        const attributeType: IAttributeTypeOption = control.value.attributeType;
        if ((this.isCustomListType && !optionsList.length) || (!this.isCustomListType && attributeType.type === "")) {
            return { invalidOptionValues: true };
        }
        return null;
    }

    private createOption(optionText: string): FormGroup {
        return new FormGroup({
            value: new FormControl(optionText, this.requiredValidator),
        });
    }

    private updateOptionsByType(type: string): void {
        if (this.modalData.type === AttributeModalTypes.Add) {
            if (type === AttributeResponseTypes.Text || type === AttributeResponseTypes.Date) {
                this.isShowingNewOptions = false;
                this.isShowingChangeTypeMessage = true;
                this.isShowingAllowOther = false;
                this.modalForm.removeControl("optionsForm");
            } else {
                this.isShowingNewOptions = true;
                this.isShowingChangeTypeMessage = false;
                this.isShowingAllowOther = true;
                this.modalForm.addControl("optionsForm", this.optionsForm);
            }
        } else if (this.modalData.type === AttributeModalTypes.Edit) {
            this.isShowingAllowOther = this.attributeLogicService.canChangeAllowOther({...this.modalData.attribute, responseType: type});
        }
    }

    private formatAttributeForSubmit(): IAttributeDetailV2 {
        this.isCustomListType = this.optionsForm.get("isCustomListType").value;
        return {
            description: this.modalForm.value.detailsForm ? this.modalForm.value.detailsForm.description : null,
            descriptionKey: this.modalData.attribute ? this.modalData.attribute.descriptionKey : null,
            fieldName: this.modalData.attribute ? this.modalData.attribute.fieldName : null,
            id: this.modalData.attribute ? this.modalData.attribute.id : null,
            name: this.modalForm.value.detailsForm && this.modalForm.value.detailsForm.name ? this.modalForm.value.detailsForm.name.replace(Regex.MULTI_SPACES, " ").trim() : null,
            nameKey: this.modalData.attribute ? this.modalData.attribute.nameKey : null,
            required: this.modalData.attribute ? this.modalData.attribute.required : null,
            responseType: this.modalForm.value.detailsForm && this.attributeLogicService.canChangeType(this.modalData.attribute) ? this.modalForm.value.detailsForm.type.value : null,
            allowOther: this.modalForm.value.detailsForm ? Boolean(this.modalForm.value.detailsForm.other) : null,
            attributeType: this.modalData.attribute ?
                this.modalData.attribute.attributeType :
                this.modalForm.value.optionsForm
                && this.optionsForm.get("attributeType")
                && this.optionsForm.get("attributeType").value
                && this.optionsForm.get("attributeType").value.type
                && !this.isCustomListType ?
                    this.optionsForm.get("attributeType").value.type :
                    null,
            values: this.modalForm.value.optionsForm && this.isCustomListType ? this.modalForm.value.optionsForm.options : null,
            status: this.modalData.attribute ? this.modalData.attribute.status : null,
        };
    }

    private duplicateValidator = (name: FormControl): ValidationErrors | null => {
        if (this.conflictMap[name.value] || this.conflictMap[name.value.replace(Regex.MULTI_SPACES, " ").trim()]) {
            return { hasConflict: this.translate.transform("AttributeDuplicateError") };
        }
        return null;
    }

    private changedTypeValidator = (type: FormControl): ValidationErrors | null => {
        this.isShowingTypeChangeWarning = type.value.value !== this.originalType && this.modalData.type === AttributeModalTypes.Edit;
        return null;
    }

    private requiredValidator = (entry: FormControl): ValidationErrors | null => {
        if (!entry.value || !entry.value.trim().length) {
            return { required: true };
        }
        return null;
    }

}
