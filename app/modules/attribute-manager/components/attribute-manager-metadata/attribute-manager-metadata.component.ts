// Rxjs
import { Subject } from "rxjs";
import {
    startWith,
    takeUntil,
} from "rxjs/operators";

// 3rd party
import {
    Component,
    Inject,
    Input,
} from "@angular/core";

// Redux
import observableFromStore from "oneRedux/utils/observable-from-store";
import {
    getIsLoadingCurrentAttribute,
    getCurrentAttribute,
} from "oneRedux/reducers/attribute.reducer";

// Services
import { AttributeActionService } from "attributeManagerModule/services/attribute-action.service";

// Constants / Enums
import {
    ResponseTypeTranslationKeys,
    AttributeResponseTypes,
} from "constants/inventory.constant";

// Tokens
import { StoreToken } from "tokens/redux-store.token";

// Interfaces
import { IAttributeDetailV2 } from "interfaces/inventory.interface";
import {
    IStore,
    IStoreState,
} from "interfaces/redux.interface";
import { IStringMap } from "interfaces/generic.interface";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

@Component({
    selector: "attribute-manager-metadata",
    templateUrl: "./attribute-manager-metadata.component.html",
})
export class AttributeManagerMetadataComponent {

    @Input() status: string;

    isLoading: boolean;
    attribute: IAttributeDetailV2;
    typeKeys: IStringMap<string>;
    responseTypes: IStringMap<string> = AttributeResponseTypes;
    private destroy$: Subject<null> = new Subject();

    constructor(
        @Inject(StoreToken) private store: IStore,
        readonly translate: TranslatePipe,
        private attributeActionService: AttributeActionService,
    ) {
        this.typeKeys = ResponseTypeTranslationKeys;
    }

    ngOnInit(): void {
        observableFromStore(this.store).pipe(
            startWith(this.store.getState()),
            takeUntil(this.destroy$),
        ).subscribe((state: IStoreState): void =>
            this.componentWillReceiveState(state),
        );
    }

    ngOnDestroy(): void {
        this.destroy$.next();
        this.destroy$.complete();
    }

    private componentWillReceiveState(state: IStoreState): void {
        this.isLoading = getIsLoadingCurrentAttribute(state);
        this.attribute = getCurrentAttribute(state);
    }
}
