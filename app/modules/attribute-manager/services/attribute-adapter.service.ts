// 3rd party
import { Injectable, Inject } from "@angular/core";
import { sortBy, forEach } from "lodash";

// services
import { InventoryService } from "inventorySharedModule/shared/services/inventory-api.service";
import { TenantTranslationService } from "sharedServices/tenant-translation.service";

// PIPES
import { TranslatePipe } from "pipes/translate.pipe";

// interfaces
import { IProtocolResponse } from "interfaces/protocol.interface";
import {
    IAttributeV2,
    IAttributeDetailV2,
    IAttributeOptionV2,
} from "interfaces/inventory.interface";

@Injectable()
export class AttributeAdapterService {

    constructor(
        @Inject("TenantTranslationService") private tenantTranslationService: TenantTranslationService,
        private inventoryService: InventoryService,
        private translatePipe: TranslatePipe,
    ) {}

    getAttributeList(schemaId: string): ng.IPromise<IAttributeV2[] | null> {
        return this.inventoryService.getAttributes(schemaId, true).then((response: IProtocolResponse<IAttributeV2[]>): IAttributeV2[] | null => {
            if (!response.result) return null;
            const rows: IAttributeV2[] = sortBy(response.data, (row: IAttributeV2): string => {
                const sortString = row.nameKey ? this.translatePipe.transform(row.nameKey) : row.name;
                return sortString.toLowerCase();
            });
            return rows;
        });
    }

    getAttributeDetails(attributeId: string): ng.IPromise<IAttributeDetailV2 | null> {
        return this.inventoryService.getAttributeDetails(attributeId).then((response: IProtocolResponse<IAttributeDetailV2>): IAttributeDetailV2 | null => {
            if (!response.result) return null;
            return response.data;
        });
    }

    addAttributeOptions = (attribute: IAttributeDetailV2): ng.IPromise<IProtocolResponse<IAttributeDetailV2>> => {
        return this.inventoryService.bulkAddAttributeOptions(attribute.id, attribute.values).then((response: IProtocolResponse<IAttributeOptionV2[]>): IProtocolResponse<IAttributeDetailV2> => {
            if (!response.result || !response.data || !response.data.length) return { result: response.result, data: attribute };
            forEach(response.data, (newOption: IAttributeOptionV2) => {
                if (!newOption.valueKey) return;
                this.tenantTranslationService.refreshTermCurrentLanguage(newOption.valueKey);
            });
            return { result: response.result, data: attribute };
        });
    }

    addAttribute = (schemaId: string, attribute: IAttributeDetailV2): ng.IPromise<IProtocolResponse<IAttributeDetailV2>> => {
        return this.inventoryService.addAttribute(schemaId, attribute)
            .then((response: IProtocolResponse<IAttributeDetailV2>): IProtocolResponse<IAttributeDetailV2> | ng.IPromise<IProtocolResponse<IAttributeDetailV2>> => {
                if (!response.result || !attribute.values) return response;
                return this.addAttributeOptions({...response.data, values: attribute.values}).then(() => response);
            });
    }

    editAttribute = (schemaId: string, attribute: IAttributeDetailV2): ng.IPromise<IProtocolResponse<IAttributeDetailV2>> => {
        return this.inventoryService.editAttribute(schemaId, attribute);
    }

}
