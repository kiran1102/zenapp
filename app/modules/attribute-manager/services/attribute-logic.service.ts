// 3rd party
import { Injectable, Inject } from "@angular/core";

// constants + enums
import {
    AttributeResponseTypes,
    AttributeTypes,
    AttributeStatuses,
    InventoryOptionStatuses,
    AttributeFieldNames,
} from "constants/inventory.constant";

// interfaces
import {
    IAttributeV2,
    IAttributeDetailV2,
    IAttributeTypeOption,
} from "interfaces/inventory.interface";
import { ILabelValue, IStringMap } from "interfaces/generic.interface";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

// Enums
import { InventoryFieldNames } from "enums/inventory.enum";

// Services
import { Permissions } from "modules/shared/services/helper/permissions.service";

@Injectable()
export class AttributeLogicService {

    constructor(
        private translate: TranslatePipe,
        private permissions: Permissions,
    ) {}

    canAddOptions(attribute: IAttributeV2 | IAttributeDetailV2): boolean {
        return attribute.responseType !== AttributeResponseTypes.Text
            && attribute.responseType !== AttributeResponseTypes.Date
            && attribute.attributeType !== AttributeTypes.Locations
            && attribute.attributeType !== AttributeTypes.Orgs
            && attribute.attributeType !== AttributeTypes.Users
            && attribute.fieldName !== InventoryFieldNames.RiskLevel
            && attribute.fieldName !== InventoryFieldNames.RiskScore
            && attribute.fieldName !== InventoryFieldNames.Status;
    }

    canChangeType(attribute: IAttributeV2 | IAttributeDetailV2): boolean {
        if (!attribute) return true;
        return !attribute.required
            && attribute.responseType !== AttributeResponseTypes.Text
            && attribute.responseType !== AttributeResponseTypes.MultiSelect
            && attribute.responseType !== AttributeResponseTypes.Date
            && attribute.attributeType !== AttributeTypes.Locations
            && attribute.attributeType !== AttributeTypes.Orgs
            && attribute.attributeType !== AttributeTypes.Users
            && attribute.fieldName !== InventoryFieldNames.RiskLevel
            && attribute.fieldName !== InventoryFieldNames.RiskScore
            && attribute.fieldName !== InventoryFieldNames.Status;
    }

    canChangeAllowOther(attribute: IAttributeV2 | IAttributeDetailV2): boolean {
        if (!attribute) return true;
        return !attribute.required
            && attribute.responseType !== AttributeResponseTypes.Text
            && attribute.responseType !== AttributeResponseTypes.Date
            && attribute.attributeType !== AttributeTypes.Locations
            && attribute.attributeType !== AttributeTypes.Orgs
            && attribute.fieldName !== InventoryFieldNames.RiskLevel
            && attribute.fieldName !== InventoryFieldNames.RiskScore
            && attribute.fieldName !== InventoryFieldNames.Status;
    }

    canDisable(attribute: IAttributeV2 | IAttributeDetailV2): boolean {
        return attribute.status === AttributeStatuses.Enabled && !attribute.required && attribute.fieldName !== AttributeFieldNames.Number;
    }

    getAttributeTypeOptions(responseType: string): IAttributeTypeOption[] {
        const attributeTypeOptions: IAttributeTypeOption[] = [
            {
                name: this.translate.transform("LocationsCountries"),
                type: AttributeTypes.Locations,
            },
        ];
        if (responseType !== AttributeResponseTypes.MultiSelect) {
            attributeTypeOptions.push({
                name: this.translate.transform("Users"),
                type: AttributeTypes.Users,
            });
        }
        return attributeTypeOptions;
    }

    getResponseTypeOptions(attribute: IAttributeV2 | IAttributeDetailV2): Array<ILabelValue<string>> {
        if (!attribute) {
            return [
                { label: this.translate.transform("Text"), value: AttributeResponseTypes.Text },
                { label: this.translate.transform("SingleSelect"), value: AttributeResponseTypes.SingleSelect },
                { label: this.translate.transform("MultiSpaceSelect"), value: AttributeResponseTypes.MultiSelect },
                { label: this.translate.transform("Date"), value: AttributeResponseTypes.Date },
            ];
        }
        if (attribute.responseType === AttributeResponseTypes.Text) {
            return [{ label: this.translate.transform("Text"), value: AttributeResponseTypes.Text }];
        }
        if (attribute.responseType === AttributeResponseTypes.Date) {
            return [{ label: this.translate.transform("Date"), value: AttributeResponseTypes.Date }];
        }
        return [
            { label: this.translate.transform("SingleSelect"), value: AttributeResponseTypes.SingleSelect },
            { label: this.translate.transform("MultiSpaceSelect"), value: AttributeResponseTypes.MultiSelect },
        ];
    }

    hasOptionActions(status: string, permissions: IStringMap<boolean>): boolean {
        return (permissions.canDisableOptions && status === InventoryOptionStatuses.Enabled)
        || (permissions.canEnableOptions && status === InventoryOptionStatuses.Disabled)
        || permissions.canEditOption;
    }

    canEditAttributeType(attribute: IAttributeV2 | IAttributeDetailV2): boolean {
        return attribute
            && attribute.attributeType !== AttributeTypes.Orgs
            && attribute.attributeType !== AttributeTypes.Users
            && attribute.attributeType !== AttributeTypes.Locations;
    }

    canTranslate(translationKey: string): boolean {
        return translationKey
            && this.permissions.canShow("LocalizationEditor")
            && this.permissions.canShow("LocalizationEditorEdit");
    }

}
