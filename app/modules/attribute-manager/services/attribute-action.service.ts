// 3rd party
import { Injectable, Inject } from "@angular/core";

// Redux
import {
    AttributeActions,
    getCurrentAttribute,
} from "oneRedux/reducers/attribute.reducer";

// services
import { AttributeAdapterService } from "attributeManagerModule/services/attribute-adapter.service";
import { ModalService } from "sharedServices/modal.service";
import { AttributeGroupsActionService } from "attributeManagerModule/services/attribute-groups-action.service";

// Tokens
import { StoreToken } from "tokens/redux-store.token";

// constants + enums
import {
    AttributeModalTypes,
    InventoryOptionStatuses,
 } from "constants/inventory.constant";
import { TranslationModules } from "constants/translation.constant";

// interfaces
import {
    IAttributeV2,
    IAttributeDetailV2,
    IAttributeOptionV2,
    IAttributeModalResolve,
    IAttributeEditOptionModalResolve,
    IAttributeOptionStatusModalResolve,
    IAttributeStatusModalResolve,
} from "interfaces/inventory.interface";
import { IStore } from "interfaces/redux.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";
import { ILocalizationEditModalData } from "interfaces/localization.interface";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

// Vitreus
import { OtModalService } from "@onetrust/vitreus";

// Components
import { LocalizationEditModalV2 } from "settingsComponents/localization-edit-modal-v2/localization-edit-modal-v2.component";

// RxJs
import { take } from "rxjs/operators";

@Injectable()
export class AttributeActionService {

    constructor(
        @Inject(StoreToken) private store: IStore,
        private translatePipe: TranslatePipe,
        private attributeAdapterService: AttributeAdapterService,
        private modalService: ModalService,
        private attributeGroupsActionService: AttributeGroupsActionService,
        private otModalService: OtModalService,
    ) { }

    getAttributeList(schemaId: string, showLoading: boolean = true): void {
        if (showLoading) this.store.dispatch({ type: AttributeActions.ATTRIBUTES_START_LOADING_LIST });
        this.attributeAdapterService.getAttributeList(schemaId).then((response: IAttributeV2[]): void => {
            if (response) {
                this.store.dispatch({ type: AttributeActions.ATTRIBUTES_SET_LIST, attributeList: response });
            } else {
                this.store.dispatch({ type: AttributeActions.ATTRIBUTES_STOP_LOADING_LIST});
            }
        });
    }

    setCurrentAttribute(attributeId: string, showLoading: boolean = true): void {
        if (showLoading) this.store.dispatch({ type: AttributeActions.ATTRIBUTES_START_LOADING_CURRENT });
        this.attributeAdapterService.getAttributeDetails(attributeId).then((response: IAttributeDetailV2): void => {
            if (response) {
                this.store.dispatch({ type: AttributeActions.ATTRIBUTES_SET_CURRENT,  currentAttribute: response });
            } else {
                this.store.dispatch({ type: AttributeActions.ATTRIBUTES_STOP_LOADING_CURRENT});
            }
        });
    }

    openAddModal(schemaId: string, callback: () => void): void {
        const modalResolve: IAttributeModalResolve = {
            schemaId,
            type: AttributeModalTypes.Add,
            saveAttribute: (newAttribute: IAttributeDetailV2): ng.IPromise<IProtocolResponse<IAttributeDetailV2>> => this.attributeAdapterService.addAttribute(schemaId, newAttribute),
            callback,
        };
        this.modalService.setModalData(modalResolve);
        this.modalService.openModal("attributeManagerModal");
    }

    openEditModal(schemaId: string, id: string, callback: () => void): void {
        const getAttributePromise = (): ng.IPromise<IAttributeDetailV2> => this.attributeAdapterService.getAttributeDetails(id);
        const modalResolve: IAttributeModalResolve = {
            schemaId,
            type: AttributeModalTypes.Edit,
            getAttribute: getAttributePromise,
            saveAttribute: (newAttribute: IAttributeDetailV2): ng.IPromise<IProtocolResponse<IAttributeDetailV2>> => this.attributeAdapterService.editAttribute(schemaId, newAttribute),
            callback,
        };
        this.modalService.setModalData(modalResolve);
        this.modalService.openModal("attributeManagerModalSmall");
    }

    openOptionsModal(schemaId: string, attributeId: string, isCurrentAttribute: boolean = false): void {
        const getAttributePromise = (): ng.IPromise<IAttributeDetailV2> => this.attributeAdapterService.getAttributeDetails(attributeId);
        const modalResolve: IAttributeModalResolve = {
            schemaId,
            attribute: isCurrentAttribute ? getCurrentAttribute(this.store.getState()) : null,
            type: AttributeModalTypes.Options,
            getAttribute: isCurrentAttribute ? null : getAttributePromise,
            saveAttribute: this.attributeAdapterService.addAttributeOptions,
            callback: (): void => {
                if (isCurrentAttribute) this.setCurrentAttribute(attributeId, false);
            },
        };
        this.modalService.setModalData(modalResolve);
        this.modalService.openModal("attributeManagerModal");
    }

    launchEditOptionModal(attributeId: string, option: IAttributeOptionV2): void {
        const modalResolve: IAttributeEditOptionModalResolve = {
            option,
            attributeId,
            callback: (): void => this.setCurrentAttribute(attributeId, false),
        };
        this.modalService.setModalData(modalResolve);
        this.modalService.openModal("editAttributeOptionModal");
    }

    launchDisableOptionModal(attributeId: string, option: IAttributeOptionV2): void {
        const modalResolve: IAttributeOptionStatusModalResolve = {
            title: this.translatePipe.transform("DeactivateOption"),
            warningMessage: this.translatePipe.transform("DisableAttributeOptionWarning"),
            newStatus: InventoryOptionStatuses.Disabled,
            option,
            attributeId,
            callback: (): void => this.setCurrentAttribute(attributeId, false),
        };
        this.modalService.setModalData(modalResolve);
        this.modalService.openModal("attributeOptionStatusModal");
    }

    launchEnableOptionModal(attributeId: string, option: IAttributeOptionV2): void {
        const modalResolve: IAttributeOptionStatusModalResolve = {
            title: this.translatePipe.transform("ReactivateOption"),
            warningMessage: this.translatePipe.transform("EnableAttributeOptionWarning"),
            newStatus: InventoryOptionStatuses.Enabled,
            option,
            attributeId,
            callback: (): void => this.setCurrentAttribute(attributeId, false),
        };
        this.modalService.setModalData(modalResolve);
        this.modalService.openModal("attributeOptionStatusModal");
    }

    launchDisableAttributeModal(attributeId: string, schemaId: string): void {
        const modalResolve: IAttributeStatusModalResolve = {
            title: this.translatePipe.transform("DeactivateAttribute"),
            warningMessage: this.translatePipe.transform("DisableAttributeWarning"),
            newStatus: InventoryOptionStatuses.Disabled,
            attributeId,
            schemaId,
            callback: (): void => {
                this.getAttributeList(schemaId, false);
                this.attributeGroupsActionService.setAttributeGroupsAndSchema(schemaId);
            },
        };
        this.modalService.setModalData(modalResolve);
        this.modalService.openModal("attributeStatusModal");
    }

    launchEnableAttributeModal(attributeId: string, schemaId: string): void {
        const modalResolve: IAttributeStatusModalResolve = {
            title: this.translatePipe.transform("ReactivateAttribute"),
            warningMessage: this.translatePipe.transform("EnableAttributeWarning"),
            newStatus: InventoryOptionStatuses.Enabled,
            attributeId,
            schemaId,
            callback: (): void => {
                this.getAttributeList(schemaId, false);
                this.attributeGroupsActionService.setAttributeGroupsAndSchema(schemaId);
            },
        };
        this.modalService.setModalData(modalResolve);
        this.modalService.openModal("attributeStatusModal");
    }

    launchTranslateModal(termKey: string, schemaId: string) {
        const modalData: ILocalizationEditModalData = {
            termKey,
            termIndex: 0,
            lastTermIndex: 0,
            tabId: TranslationModules.Inventory,
        };
        this.otModalService
            .create(
                LocalizationEditModalV2,
                {
                    isComponent: true,
                },
                modalData,
            ).pipe(
                take(1),
            ).subscribe(() => {
                this.getAttributeList(schemaId, false);
            });
    }
}
