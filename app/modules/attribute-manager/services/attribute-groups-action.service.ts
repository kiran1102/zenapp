// Angular
import { Injectable } from "@angular/core";

// Rxjs
import {
    BehaviorSubject,
    Observable,
} from "rxjs";

// Services
import { InventoryService } from "inventorySharedModule/shared/services/inventory-api.service";
import { Permissions } from "modules/shared/services/helper/permissions.service";
import { RecordActionService } from "inventoryModule/services/action/record-action.service";

// Interfaces
import { IAttributeGroup, IAttributeGroupsState } from "interfaces/attributes.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";
import { IAttributeDetailV2 } from "interfaces/inventory.interface";

// Const
import { InventoryPermissions } from "constants/inventory.constant";
import { EmptyGuid } from "constants/empty-guid.constant";

@Injectable()
export class AttributeGroupsActionService {

    readonly attributeGroupsView$: Observable<{ groups: IAttributeGroup[] }>;
    readonly attributeGroupsState$: Observable<IAttributeGroupsState>;

    private defaultLayout: { groups: IAttributeGroup[] } = {
        groups: [],
    };
    private defaultState: IAttributeGroupsState = {
        isSaving: false,
        isLoadingGroups: false,
        openGroupMap: {},
    };
    private _attributeGroupsView: BehaviorSubject<{ groups: IAttributeGroup[] }> = new BehaviorSubject(JSON.parse(JSON.stringify(this.defaultLayout)));
    private _attributeGroupsState: BehaviorSubject<IAttributeGroupsState> = new BehaviorSubject(JSON.parse(JSON.stringify(this.defaultState)));

    constructor(
        private inventoryService: InventoryService,
        private permissionsService: Permissions,
        private recordActionService: RecordActionService,
    ) {
        this.attributeGroupsView$ = this._attributeGroupsView.asObservable();
        this.attributeGroupsState$ = this._attributeGroupsState.asObservable();
    }

    setGroupsData(groups: IAttributeGroup[]) {
        this._attributeGroupsView.next({groups});
        this.setOpenGroupMap();
    }

    setSavingState(isSaving: boolean) {
        const currentState = this._attributeGroupsState.getValue();
        this._attributeGroupsState.next({...currentState, isSaving});
    }

    addGroupToView(newGroup: IAttributeGroup) {
        const currentGroups = this._attributeGroupsView.getValue().groups;
        currentGroups.splice(currentGroups.length - 1, 0, newGroup);
        this.setGroupsData(currentGroups);
    }

    setOpenGroupMap() {
        const currentView = this._attributeGroupsView.getValue();
        const currentState = this._attributeGroupsState.getValue();
        const openGroupMap = currentState.openGroupMap;
        currentView.groups.forEach((group: IAttributeGroup) => {
            if (openGroupMap[group.id] === undefined) {
                openGroupMap[group.id] = true;
            }
        });
        this._attributeGroupsState.next({
            ...currentState,
            openGroupMap,
        });
    }

    getUngroupedAttributes(groups: IAttributeGroup[]): string[] {
        const defaultGroup = groups.find((group: IAttributeGroup) => group.id === EmptyGuid);
        return defaultGroup.attributes;
    }

    addGroup(inventoryId: string, groupName: string): ng.IPromise<IProtocolResponse<IAttributeGroup>> {
        return this.inventoryService.addAttributeGroup(inventoryId, groupName);
    }

    updateGroup(inventoryId: string, groupId: string, payload: IAttributeGroup): ng.IPromise<IProtocolResponse<IAttributeGroup>> {
        return this.inventoryService.updateAttributeGroup(inventoryId, groupId, payload);
    }

    moveGroup(inventoryId: string, groupId: string, payload: { previousGroupId: string }): ng.IPromise<IProtocolResponse<void>> {
        return this.inventoryService.moveAttributeGroup(inventoryId, groupId, payload);
    }

    toggleGroup(group: IAttributeGroup) {
        const currentState = this._attributeGroupsState.getValue();
        this._attributeGroupsState.next({
            ...currentState,
            openGroupMap: {
                ...currentState.openGroupMap,
                [group.id]: !currentState.openGroupMap[group.id],
            },
        });
    }

    setIsLoadingGroups(isLoadingGroups: boolean) {
        const currentState = this._attributeGroupsState.getValue();
        this._attributeGroupsState.next({...currentState, isLoadingGroups });
    }

    setAttributeGroupsAndSchema(inventoryId: string) {
        if (this.permissionsService.canShow(InventoryPermissions[inventoryId].attributeGrouping)) {
            this.setIsLoadingGroups(true);
            const attributePromises: Array<ng.IPromise<IProtocolResponse<IAttributeGroup[]> | IProtocolResponse<IAttributeDetailV2[]>>> = [
                this.inventoryService.getAttributeGroups(inventoryId),
                this.inventoryService.getAllAttributes(inventoryId),
            ];

            Promise.all(attributePromises).then((responses: Array<IProtocolResponse<IAttributeGroup[] | IAttributeDetailV2[]>>) => {
                if (responses[0].result) {
                    this.setGroupsData(responses[0].data);
                }
                if (responses[1].result) {
                    this.recordActionService.setSchema(responses[1].data as IAttributeDetailV2[], inventoryId);
                }
                this.setIsLoadingGroups(false);
            });
        }
    }

    deleteGroup(inventoryId: string, groupId: string): ng.IPromise<IProtocolResponse<void>> {
        return this.inventoryService.deleteAttributeGroup(inventoryId, groupId);
    }
}
