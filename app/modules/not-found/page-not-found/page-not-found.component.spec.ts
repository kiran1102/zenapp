import {
    async,
    ComponentFixture,
    TestBed,
} from "@angular/core/testing";

import {
    RouterTestingModule,
} from "@angular/router/testing";

import { OtButtonModule } from "@onetrust/vitreus";

import { PageNotFoundComponent } from "./page-not-found.component";

describe("PageNotFoundComponent", () => {
    let fixture: ComponentFixture<PageNotFoundComponent>;
    let component: PageNotFoundComponent;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                RouterTestingModule,
                OtButtonModule,
            ],
            declarations: [PageNotFoundComponent],
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(PageNotFoundComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it("should create", () => {
        expect(component).toBeTruthy();
    });

    it("should display a logo", () => {
        const el: HTMLElement = fixture.nativeElement;
        const logo = el.querySelector(".not-found__logo-image");
        expect(logo).toBeTruthy();
        const src = logo.getAttribute("src");
        expect(src).toBeTruthy();
    });

    it("should display '404' text", () => {
        const el: HTMLElement = fixture.nativeElement;
        const title = el.querySelector(".not-found__title-404");
        expect(title).toBeTruthy();
        const inner = title.innerHTML;
        expect(inner).toBe("404");
    });

    it("should display 'Page Not Found' text", () => {
        const el: HTMLElement = fixture.nativeElement;
        const subtitle = el.querySelector(".not-found__title-sub");
        expect(subtitle).toBeTruthy();
        const inner = subtitle.innerHTML;
        expect(inner).toBeTruthy();
    });

    it("should display 'Sorry, we can't find page...' text", () => {
        const el: HTMLElement = fixture.nativeElement;
        const description = el.querySelector(".not-found__description");
        expect(description).toBeTruthy();
        const inner = description.innerHTML;
        expect(inner).toBeTruthy();
    });

    it("should display a 'Back to application' button", () => {
        const el: HTMLElement = fixture.nativeElement;
        const btn = el.querySelector(".not-found__btn");
        expect(btn).toBeTruthy();
        const inner = btn.innerHTML;
        expect(inner).toBeTruthy();
    });

    it("should contain link to app in the 'Back to application' button", () => {
        const el: HTMLElement = fixture.nativeElement;
        const btn = el.querySelector(".not-found__btn");
        const href = btn.getAttribute("href");
        expect(href).toBe("#/");
    });
});
