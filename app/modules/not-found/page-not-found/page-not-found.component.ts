import { Component, Inject, OnInit } from "@angular/core";

// Redux
import { StoreToken } from "tokens/redux-store.token";
import { IStore } from "interfaces/redux.interface";
import { getBranding } from "oneRedux/reducers/branding.reducer";

// Services
import { Content } from "sharedModules/services/provider/content.service";

// Constants
import { DEFAULT_BRAND_LOGO } from "constants/branding.constant";

@Component({
    templateUrl: "./page-not-found.component.html",
    // TODO: Update build to support component based style imports.
    // styleUrls: ["./page-not-found.component.scss"],
})
export class PageNotFoundComponent implements OnInit {

    logo: string;

    constructor(
        @Inject(StoreToken) private store: IStore,
        private content: Content,
    ) {}

    ngOnInit() {
        const state = this.store.getState();
        const branding = getBranding(state);
        this.logo = branding.logo && branding.logo.url ? branding.logo.url : this.content.GetKey("loginLogo") || DEFAULT_BRAND_LOGO;
    }

}
