import { ModuleWithProviders } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { PageNotFoundComponent } from "./page-not-found/page-not-found.component";
import { PageNotFoundCloudsComponent } from "./page-not-found-clouds/page-not-found-clouds.component";

const appRoutes: Routes = [
    { path: "", component: PageNotFoundComponent },
    { path: "clouds", component: PageNotFoundCloudsComponent },
];

export const routing: ModuleWithProviders = RouterModule.forChild(appRoutes);
