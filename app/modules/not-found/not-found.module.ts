import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { OtButtonModule } from "@onetrust/vitreus";
import { TranslateModule } from "@ngx-translate/core";

import { routing } from "./not-found.routing";

import { PageNotFoundComponent } from "./page-not-found/page-not-found.component";
import { PageNotFoundCloudsComponent } from "./page-not-found-clouds/page-not-found-clouds.component";

@NgModule({
    imports: [
        CommonModule,
        routing,
        TranslateModule,
        OtButtonModule,
    ],
    declarations: [
        PageNotFoundComponent,
        PageNotFoundCloudsComponent,
    ],
})
export class NotFoundModule {}
