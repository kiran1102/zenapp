import { async, ComponentFixture, TestBed } from "@angular/core/testing";

import { PageNotFoundCloudsComponent } from "./page-not-found-clouds.component";

describe("PageNotFoundCloudsComponent", () => {
    let component: PageNotFoundCloudsComponent;
    let fixture: ComponentFixture<PageNotFoundCloudsComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [PageNotFoundCloudsComponent],
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(PageNotFoundCloudsComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it("should create", () => {
        expect(component).toBeTruthy();
    });
});
