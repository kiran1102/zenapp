import { Component } from "@angular/core";

@Component({
    templateUrl: "./page-not-found-clouds.component.html",
    // TODO: Update build to support component based style imports.
    // styleUrls: ["./page-not-found-clouds.component.scss"],
})
export class PageNotFoundCloudsComponent {}
