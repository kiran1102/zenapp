import { Injectable } from "@angular/core";

// Rxjs
import {
    Observable,
    Subject,
} from "rxjs";

// Interfaces
import { IQuestionDetails } from "interfaces/assessment-template-question.interface";

@Injectable()
export class TemplateBuilderModalHelperService {
    questionBuilderUpdate = new Subject<IQuestionDetails>();
    $questionBuilderModalListener: Observable<IQuestionDetails>;

    constructor() {
        this.$questionBuilderModalListener = this.questionBuilderUpdate.asObservable();
    }

    updateQuestion(question: IQuestionDetails): void {
        this.questionBuilderUpdate.next(question);
    }
}
