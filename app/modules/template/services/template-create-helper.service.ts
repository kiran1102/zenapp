// Angular
import {
    Injectable,
    Inject,
    OnDestroy,
} from "@angular/core";

// Rxjs
import { Subject } from "rxjs";
import {
    filter,
    takeUntil,
} from "rxjs/operators";

// Redux
import { StoreToken } from "tokens/redux-store.token";
import {
    getTemplateById,
    getCurrentTemplate,
} from "oneRedux/reducers/template.reducer";

// Services
import { OtModalService } from "@onetrust/vitreus";
import { StateService } from "@uirouter/core";

// Interface
import { IStore } from "interfaces/redux.interface";
import { ITemplateCreateModalData } from "interfaces/template-create-modal-data.interface";
import { TemplateStateProperty } from "constants/template-states.constant";

// Components
import { TemplateCreateFormComponent } from "modules/template/components/template-create-form/template-create-form.component";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

@Injectable()
export class TemplateCreateHelperService implements OnDestroy {

    private destroy$ = new Subject();

    constructor(
        @Inject(StoreToken) private store: IStore,
        private otModalService: OtModalService,
        private translatePipe: TranslatePipe,
        private stateService: StateService,
    ) { }

    ngOnDestroy(): void {
        this.destroy$.next();
        this.destroy$.complete();
    }

    openCreateTemplateModal(): void {
        this.otModalService.create(
            TemplateCreateFormComponent,
            {
                isComponent: true,
            },
        ).pipe(
            filter((data) => Boolean(data)),
            takeUntil(this.destroy$),
        ).subscribe((data) => {
            this.stateService.go("zen.app.pia.module.templatesv2.studio.manage", data);
        });
    }

    openCloneGalleryTemplateModal(templateData: ITemplateCreateModalData): void {
        this.otModalService.create(
            TemplateCreateFormComponent,
            {
                isComponent: true,
            },
            templateData,
        ).pipe(
            filter((data) => Boolean(data)),
            takeUntil(this.destroy$),
        ).subscribe((data) => {
            this.stateService.go("zen.app.pia.module.templatesv2.studio.manage", data);
        });
    }

    openCopyTemplateModal(templateId?: string): void {
        let originalTemplate = null;

        if (templateId) {
            const currentTemplate = getTemplateById(templateId)(this.store.getState());
            originalTemplate = currentTemplate.activeState === TemplateStateProperty.Published ? currentTemplate.published : currentTemplate.draft;
        } else {
            originalTemplate = getCurrentTemplate(this.store.getState());
        }

        this.otModalService
            .create(
                TemplateCreateFormComponent,
                {
                    isComponent: true,
                },
                {
                    id: originalTemplate.id,
                    name: `${originalTemplate.name} (${this.translatePipe.transform("Copy")})`,
                    icon: originalTemplate.icon,
                    description: originalTemplate.description,
                    orgGroupId: originalTemplate.orgGroupId,
                    copying: true,
                });
    }
}
