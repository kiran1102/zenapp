// Angular
import {
    Injectable,
    OnDestroy,
} from "@angular/core";

// 3rd party
import {
    Subject,
    Observable,
} from "rxjs";

// Vitreus
import {
    OtModalService,
    ConfirmModalType,
} from "@onetrust/vitreus";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

// Components
import { TemplateSaveChangesModalComponent } from "modules/template/components/template-save-changes-modal/template-save-changes-modal.component";

@Injectable()
export class TemplateDetailsHelperService implements OnDestroy {

    private destroy$ = new Subject();

    constructor(
        private otModalService: OtModalService,
        private translatePipe: TranslatePipe,
    ) { }

    ngOnDestroy(): void {
        this.destroy$.next();
        this.destroy$.complete();
    }

    openSaveUnSavedChangesModal(templateId: string, templateRootId: string, templateStatus: string): Observable<boolean> {
        return this.otModalService.create(
            TemplateSaveChangesModalComponent,
            {
                isComponent: true,
            },
            {
                templateId,
                templateRootId,
                templateStatus,
            },
        );
    }

    openSavePublishedTemplateConfirmModal(): Observable<boolean> {
        return this.otModalService.confirm(
            {
                type: ConfirmModalType.NOTIFY,
                translations: {
                    title: this.translatePipe.transform("SaveUpdates"),
                    desc: this.translatePipe.transform("ConfirmTemplateChangesAffectExistingAssessments"),
                    confirm: this.translatePipe.transform(""),
                    submit: this.translatePipe.transform("Confirm"),
                    cancel: this.translatePipe.transform("Cancel"),
                },
                hideClose: false,
            });
    }
}
