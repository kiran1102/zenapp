// Angular
import {
    Injectable,
} from "@angular/core";

// Constants
import { QuestionTypes } from "constants/question-types.constant";

@Injectable()
export class QuestionTypeService {

    constructor() {}

    public getQuestionTypeIcon(type: string): string {
        let icon = "";

        switch (type) {
            case QuestionTypes.Date.name:
                icon = QuestionTypes.Date.icon;
                break;
            case QuestionTypes.MultiChoice.name:
                icon = QuestionTypes.MultiChoice.icon;
                break;
            case QuestionTypes.Statement.name:
                icon = QuestionTypes.Statement.icon;
                break;
            case QuestionTypes.TextBox.name:
                icon = QuestionTypes.TextBox.icon;
                break;
            case QuestionTypes.YesNo.name:
                icon = QuestionTypes.YesNo.icon;
                break;
            case QuestionTypes.Inventory.name:
                icon = QuestionTypes.Inventory.icon;
                break;
            case QuestionTypes.Attribute.name:
                icon = QuestionTypes.Attribute.icon;
                break;
            case QuestionTypes.PersonalData.name:
                icon = QuestionTypes.PersonalData.icon;
                break;
            case QuestionTypes.Incident.name:
                icon = QuestionTypes.Incident.icon;
                break;
        }

        return icon;
    }

}
