import { Injectable } from "@angular/core";

// Services
import { ProtocolService } from "modules/core/services/protocol.service";

// Interfaces
import {
    IProtocolConfig,
    IProtocolMessages,
    IProtocolResponse,
} from "interfaces/protocol.interface";
import { IIncidentSchema } from "modules/template/interfaces/incident-schema.interface";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

@Injectable()
export class IncidentSchemaApiService {

    constructor(
        private protocolService: ProtocolService,
        private translatePipe: TranslatePipe,
    ) {}

    fetchSchemas(): ng.IPromise<IProtocolResponse<IIncidentSchema[]>> {
        const config: IProtocolConfig = this.protocolService.customConfig("GET", `/api/incident/v1/incidents/schemas`);
        const messages: IProtocolMessages = {
            Error: { custom: this.translatePipe.transform("Incident.Error.FetchingSchema") },
        };
        return this.protocolService.http(config, messages);
    }
}
