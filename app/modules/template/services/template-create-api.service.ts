// Angular
import { Injectable } from "@angular/core";

// 3rd party
import {
    Observable,
    from,
} from "rxjs";

// Interface
import {
    IProtocolConfig,
    IProtocolMessages,
    IProtocolResponse,
} from "interfaces/protocol.interface";
import {
    IAssessmentCustomTemplate,
    IAssessmentGalleryTemplate,
} from "modules/template/interfaces/template.interface";
import { ITemplateVersion } from "modules/template/interfaces/template.interface";
import {
    ICreateTemplateVersionResponse,
    ICopyTemplateDetails,
} from "interfaces/assessment-template-detail.interface";
import { ITemplateSettings } from "modules/template/interfaces/template.interface";

// Services
import { ProtocolService } from "modules/core/services/protocol.service";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

@Injectable()
export class TemplateCreateApiService {

    constructor(
        private protocolService: ProtocolService,
        private translatePipe: TranslatePipe,
    ) { }

    createTemplate(template: IAssessmentCustomTemplate): Observable<IProtocolResponse<ITemplateVersion>> {
        const config: IProtocolConfig = this.protocolService.customConfig("POST", "/api/template/v1/templates", [], template);
        const messages: IProtocolMessages = {
            Success: { custom: this.translatePipe.transform("QuestionnaireCreated") },
            Error: { custom: this.translatePipe.transform("ErrorAddingTemplate") },
        };
        return from(this.protocolService.http(config, messages));
    }

    cloneTemplateGallery(template: IAssessmentGalleryTemplate): Observable<IProtocolResponse<ITemplateVersion>> {
        const config: IProtocolConfig = this.protocolService.customConfig("POST", "/api/template/v1/templates/gallery/clone", [], template);
        const messages: IProtocolMessages = {
            Success: { custom: this.translatePipe.transform("TemplateCopied") },
            Error: { custom: this.translatePipe.transform("CouldNotCopyTemplate") },
        };
        return from(this.protocolService.http(config, messages));
    }

    copyTemplate(template: ICopyTemplateDetails): Observable<IProtocolResponse<ICreateTemplateVersionResponse>> {
        const config: IProtocolConfig = this.protocolService.customConfig("POST", `/api/template/v1/templates/copy`, [], template);
        const messages: IProtocolMessages = {
            Success: { custom: this.translatePipe.transform("TemplateCopied") },
            Error: { custom: this.translatePipe.transform("CouldNotCopyTemplate") },
        };
        return from(this.protocolService.http(config, messages));
    }

    // NOTE: Same Api is used in Assessment Automation Settings - Template tab
    getTemplateSettings(): Observable<IProtocolResponse<ITemplateSettings>> {
        const config: IProtocolConfig = this.protocolService.customConfig("GET", `/api/template/v1/settings`);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("CouldNotRetrieveTemplateSettings") } };
        return from(this.protocolService.http(config, messages));
    }
}
