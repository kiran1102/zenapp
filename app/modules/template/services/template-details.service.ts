// Angular
import {
    Injectable,
    Inject,
    OnDestroy,
} from "@angular/core";

// 3rd party
import {
    Observable,
    Subject,
    of,
} from "rxjs";
import {
    filter,
    tap,
    takeUntil,
} from "rxjs/operators";

// Enum
import { TemplateModes } from "enums/template-modes.enum";

// Constants
import { TemplateStates } from "constants/template-states.constant";

// Interface
import { IStore } from "interfaces/redux.interface";
import {
    IAssessmentTemplateDetail,
    IAssessmentTemplateDetailUpdateParams,
} from "interfaces/assessment-template-detail.interface";
import { ICustomTemplate } from "modules/template/interfaces/custom-templates.interface";
import { ITemplateUpdateRequest } from "modules/template/interfaces/template-update-request.interface";

// Redux
import { StoreToken } from "tokens/redux-store.token";
import {
    getCurrentTemplate,
    TemplateActions,
    getTemplateState,
    isTemplateModelValid,
    getTemplateTempModel,
} from "oneRedux/reducers/template.reducer";

// Services
import { NotificationService } from "sharedModules/services/provider/notification.service";
import { TemplateDetailsHelperService } from "modules/template/services/template-details-helper.service";
import { TemplateDetailsAPIService } from "modules/template/services/template-details-api.service";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

@Injectable()
export class TemplateDetailsService implements OnDestroy {

    private destroy$ = new Subject();

    constructor(
        @Inject(StoreToken) private store: IStore,
        private translatePipe: TranslatePipe,
        private notificationService: NotificationService,
        private templateDetailsHelperService: TemplateDetailsHelperService,
        private templateDetailsAPIService: TemplateDetailsAPIService,
    ) { }

    ngOnDestroy(): void {
        this.destroy$.next();
        this.destroy$.complete();
    }

    lockTemplate(isLocked: boolean, updateBuilder?: boolean, templateRootId?: string): Observable<boolean> {
        let templateId: string;
        let templateState: IAssessmentTemplateDetail | ICustomTemplate;
        let templateStatus: string;
        if (updateBuilder) {
            templateState = getCurrentTemplate(this.store.getState());
            templateStatus = templateState.status;
            templateId = templateState.id;
            templateRootId = templateState.rootVersionId;
        } else {
            templateState = getTemplateState(this.store.getState()).templateByIds[templateRootId];
            templateStatus = templateState.activeState.toUpperCase();
            if (templateStatus === TemplateStates.DRAFT.toUpperCase() && templateState.draft) {
                templateId = templateState.draft.id;
            } else if (templateStatus === TemplateStates.PUBLISHED.toUpperCase() && templateState.published) {
                templateId = templateState.published.id;
            }
        }

        if (updateBuilder && getTemplateState(this.store.getState()).templateMode === TemplateModes.Edit && !(templateState as IAssessmentTemplateDetail).isLocked) {
            return this.templateDetailsHelperService.openSaveUnSavedChangesModal(templateId, templateRootId, templateStatus)
                .pipe(
                    tap((saveChanges: boolean | undefined) => {
                        if (typeof saveChanges === "boolean") {
                            return this.templateLockToggle(templateRootId, templateStatus, isLocked, updateBuilder);
                        }
                    }));
        } else {
            return this.templateLockToggle(templateRootId, templateStatus, isLocked, updateBuilder);
        }
    }

    updateDraftTemplateDetails(template: IAssessmentTemplateDetailUpdateParams): Observable<boolean> {
        const temp: IAssessmentTemplateDetailUpdateParams = { ...template };

        const storeState = this.store.getState();
        const templateState = getCurrentTemplate(storeState);
        const [templateId, status] = [templateState.id, templateState.status];

        if (status === TemplateStates.DRAFT.toUpperCase()) {
            if (!isTemplateModelValid(storeState)) {
                return of(false);
            }

            this.store.dispatch({ type: TemplateActions.DISABLE_HEADER_ACTIONS, disableHeaderActions: true });
            return this.templateDetailsAPIService.updateDraftTemplateDetails(templateId, temp)
                .pipe(
                    tap(() => {
                        this.store.dispatch({ type: TemplateActions.DISABLE_HEADER_ACTIONS, disableHeaderActions: false });
                        const currentModel: ITemplateUpdateRequest = {
                            ...temp,
                            welcomeText: {
                                welcomeText: templateState.welcomeText,
                            },
                        };
                        this.store.dispatch({ type: TemplateActions.UPDATE_TEMPLATE, template: currentModel });

                    }));
        }
    }

    updatePublishedTemplate(templateId: string): ng.IPromise<boolean> {
        const templateTempModel: ITemplateUpdateRequest = getTemplateTempModel(this.store.getState());
        const request: ITemplateUpdateRequest = {
            ...templateTempModel,
            settings: {
                ...templateTempModel.settings,
                isLocked: false,
            },
        };
        return this.templateDetailsAPIService.updatePublishedTemplate(templateId, request)
            .then((result: boolean): boolean => {
                if (result) {
                    this.store.dispatch({ type: TemplateActions.UPDATE_TEMPLATE, template: request });
                }
                return result;
            });
    }

    private templateLockToggle(templateRootId: string, templateStatus: string, locked: boolean, updateBuilder: boolean): Observable<boolean> {
        if (locked) {
            return this.templateDetailsAPIService.lockTemplate(templateRootId)
                .pipe(
                    filter(Boolean),
                    takeUntil(this.destroy$),
                    tap((response): void => {
                        this.templateLockToggleSuccessCallback(templateRootId, templateStatus, locked, updateBuilder);
                    }),
                );
        } else {
            return this.templateDetailsAPIService.unLockTemplate(templateRootId)
                .pipe(
                    filter(Boolean),
                    takeUntil(this.destroy$),
                    tap((response): void => {
                        this.templateLockToggleSuccessCallback(templateRootId, templateStatus, locked, updateBuilder);
                    }),
                );
        }

    }

    private templateLockToggleSuccessCallback(templateRootId: string, templateStatus: string, isLocked: boolean, updateBuilder: boolean) {
        updateBuilder ? this.store.dispatch({ type: TemplateActions.LOCK_TEMPLATE, isLocked }) : this.store.dispatch({ type: TemplateActions.UPDATE_TEMPLATE_BY_ID, templateRootId, isLocked, templateStatus, templateId: templateRootId });
        if (isLocked) {
            this.notificationService.alertSuccess(this.translatePipe.transform("Success"), this.translatePipe.transform("TemplateLocked"));
        } else {
            this.notificationService.alertSuccess(this.translatePipe.transform("Success"), this.translatePipe.transform("TemplateUnlocked"));
        }
    }
}
