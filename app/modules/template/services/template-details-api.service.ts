// Angular
import { Injectable } from "@angular/core";

// Rxjs
import {
    Observable,
    from,
} from "rxjs";

// Services
import { ProtocolService } from "modules/core/services/protocol.service";

// Interface
import { IAssessmentTemplateDetailUpdateParams } from "interfaces/assessment-template-detail.interface";
import {
    IProtocolConfig,
    IProtocolMessages,
    IProtocolResponse,
} from "interfaces/protocol.interface";
import { ITemplateUpdateRequest } from "modules/template/interfaces/template-update-request.interface";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

@Injectable()
export class TemplateDetailsAPIService {

    constructor(
        private OneProtocol: ProtocolService,
        private translatePipe: TranslatePipe,
    ) { }

    updateDraftTemplateDetails(templateId: string, template: IAssessmentTemplateDetailUpdateParams): Observable<boolean> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("PUT", `/api/template/v1/templates/${templateId}/details`, null, template);
        const messages: IProtocolMessages = {
            Error: { custom: this.translatePipe.transform("CouldNotUpdateQuestionnaire") },
        };
        return from(this.OneProtocol.http(config, messages).then((response) => response.result));
    }

    lockTemplate(templateId: string): Observable<boolean> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("PUT", `/api/template/v1/templates/${templateId}/lock`);
        const messages: IProtocolMessages = {
            Error: { custom: this.translatePipe.transform("CouldNotUpdateQuestionnaire") },
        };
        return from(this.OneProtocol.http(config, messages).then((response) => response.result));
    }

    unLockTemplate(templateId: string): Observable<boolean> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("PUT", `/api/template/v1/templates/${templateId}/unlock`);
        const messages: IProtocolMessages = {
            Error: { custom: this.translatePipe.transform("CouldNotUpdateQuestionnaire") },
        };
        return from(this.OneProtocol.http(config, messages).then((response) => response.result));
    }

    updatePublishedTemplate(templateId: string, requestBody: ITemplateUpdateRequest): ng.IPromise<boolean> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("PUT", `/api/template/v1/templates/published/${templateId}`, null, requestBody);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("CouldNotUpdateQuestionnaire") } };
        return this.OneProtocol.http(config, messages).then((response: IProtocolResponse<void>): boolean => response.result);
    }
}
