// Angular
import {
    Injectable,
    Inject,
} from "@angular/core";

// 3rd party
import { Observable } from "rxjs";

// Redux
import { StoreToken } from "tokens/redux-store.token";
import {
    getOrgById,
} from "oneRedux/reducers/org.reducer";

// Services
import { TemplateCreateApiService } from "./template-create-api.service";

// Interface
import {
    IAssessmentCustomTemplate,
    IAssessmentGalleryTemplate,
} from "modules/template/interfaces/template.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";
import { ITemplateVersion } from "modules/template/interfaces/template.interface";
import {
    ICopyTemplateDetails,
    ICreateTemplateVersionResponse,
} from "interfaces/assessment-template-detail.interface";
import { IOrganization } from "interfaces/organization.interface";
import { IStore } from "interfaces/redux.interface";
import { ITemplateSettings } from "modules/template/interfaces/template.interface";

@Injectable()
export class TemplateCreateService {

    constructor(
        @Inject(StoreToken) private store: IStore,
        private templateCreateApiService: TemplateCreateApiService,
    ) { }

    cloneTemeplateGallery(template: IAssessmentGalleryTemplate): Observable<IProtocolResponse<ITemplateVersion>> {
        return this.templateCreateApiService.cloneTemplateGallery(template);
    }

    createTemplate(template: IAssessmentCustomTemplate): Observable<IProtocolResponse<ITemplateVersion>> {
        return this.templateCreateApiService.createTemplate(template);
    }

    copyTemplate(template: ICopyTemplateDetails): Observable<IProtocolResponse<ICreateTemplateVersionResponse>> {
        return this.templateCreateApiService.copyTemplate(template);
    }

    getTemplateSettings(): Observable<IProtocolResponse<ITemplateSettings>> {
        return this.templateCreateApiService.getTemplateSettings();
    }

    getSelectedOrgModel(id: string): IOrganization {
        const name = id && getOrgById(id)(this.store.getState()) ? getOrgById(id)(this.store.getState()).Name : null;
        return {
            id,
            name,
            canDelete: null,
            parentId: null,
            privacyOfficerId: null,
            privacyOfficerName: null,
        };
    }
}
