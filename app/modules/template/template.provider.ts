export function getTemplateAPI(injector) {
    return injector.get("TemplateAPI");
}
export function getTemplateAction(injector) {
    return injector.get("TemplateAction");
}

export function getTemplateBuilder(injector) {
    return injector.get("TemplateBuilderService");
}

export function getOtIconService(injector) {
    return injector.get("OTIconService");
}
