// Rxjs
import { Subject, SubscriptionLike } from "rxjs";
import {
    startWith,
    takeUntil,
    debounceTime,
    distinctUntilChanged,
} from "rxjs/operators";

// Angular
import {
    Component,
    Inject,
    OnInit,
    OnDestroy,
} from "@angular/core";
import { StateService } from "@uirouter/core";

// Vitreus
import { OtModalService } from "@onetrust/vitreus";

// Interfaces
import {
    IStore,
    IStoreState,
} from "interfaces/redux.interface";
import { IGalleryTemplateCardAttributes } from "interfaces/gallery-templates.interface";
import { Permissions } from "modules/shared/services/helper/permissions.service";
import { ITabsNav } from "interfaces/one-tabs-nav.interface";
import { IPageableMeta } from "interfaces/pagination.interface";

// Constants
import { TemplateTypes } from "constants/template-types.constant";
import { TemplatesTypeTabOptions } from "constants/template-tab-options.constant";

// Services
import TemplateAction from "templateServices/actions/template-action.service";
import { TemplateCreateHelperService } from "modules/template/services/template-create-helper.service";

// Redux
import observableFromStore from "oneRedux/utils/observable-from-store";
import { getTemplateState } from "oneRedux/reducers/template.reducer";
import { IStringMap } from "interfaces/generic.interface";
import { StoreToken } from "tokens/redux-store.token";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";
import { PermitPipe } from "modules/pipes/permit.pipe";

// Components
import { ArchiveTemplateModalComponent } from "modules/template/components/archive-template-modal/archive-template-modal.component";

@Component({
    selector: "custom-templates",
    templateUrl: "./custom-templates.component.html",
})

export class CustomTemplatesComponent implements OnInit, OnDestroy {

    loadingTemplate = true;
    templateIds: string[];
    translations: IStringMap<string>;
    templateNavUrl: string;
    tabOptions: ITabsNav[];
    templatesTypeTabOptions: typeof TemplatesTypeTabOptions;
    currentTab: string;
    cards: IGalleryTemplateCardAttributes[] = [];
    paginatedCards: IGalleryTemplateCardAttributes[] = [];
    currentPage: number;
    numPages: number;
    pageSize: number;
    minIndex: number;
    maxIndex: number;
    paginationDetail: IPageableMeta;
    searchText: string;
    reset$ = new Subject();

    private vendorRoutes: IStringMap<string>;
    private storeSubscription: SubscriptionLike;
    private routes: IStringMap<string>;
    private searchText$ = new Subject();
    private destroy$ = new Subject();

    constructor(
        @Inject(StoreToken) private store: IStore,
        @Inject("TemplateAction") private templateAction: TemplateAction,
        public permissions: Permissions,
        private translatePipe: TranslatePipe,
        private stateService: StateService,
        private templateCreateService: TemplateCreateHelperService,
        private otModalService: OtModalService,
        private permissionPipe: PermitPipe,
    ) {
        this.routes = {
            gallery: "zen.app.pia.module.templatesv2.studio.gallery",
            list: "zen.app.pia.module.templatesv2.list",
        };
        this.vendorRoutes = {
            templatesStateCheck: "vendor.templates",
            templatesList: "zen.app.pia.module.vendor.templates",
            gallery: "zen.app.pia.module.vendor.gallery",
        };
        this.templatesTypeTabOptions = TemplatesTypeTabOptions;
        this.tabOptions = [
            { text: this.translatePipe.transform("Templates"), id: this.templatesTypeTabOptions.Templates },
            { text: this.translatePipe.transform("Archive"), id: this.templatesTypeTabOptions.Archive },
        ];
        this.currentTab = this.templatesTypeTabOptions.Templates;
    }

    ngOnInit(): void {
        observableFromStore(this.store).pipe(
            startWith(this.store.getState()),
            takeUntil(this.destroy$),
        ).subscribe((state: IStoreState): void =>
            this.componentWillReceiveState(state),
        );
        this.getTemplatesList();
        this.setConfigurationByTemplateType();
        this.searchText$.pipe(
            debounceTime(300),
            distinctUntilChanged(),
            takeUntil(this.destroy$),
        ).subscribe((searchTerm: string) => {
            this.searchText = searchTerm;
            this.fetchPagedData(0);
        });
    }

    ngOnDestroy(): void {
        this.destroy$.next();
        this.destroy$.complete();
    }

    onChooseFromGalleryClick(): void {
        if (this.permissions.canShow("TemplatesGallery")) {
            if (this.isRenderedFromVendorModule()) {
                this.stateService.go(this.vendorRoutes.gallery);
            } else {
                this.stateService.go(this.routes.gallery);
            }
        }
    }

    openCreateTemplateModal(): void {
        this.templateCreateService.openCreateTemplateModal();
    }

    getTemplateType(): string {
        return this.isRenderedFromVendorModule() ? TemplateTypes.VENDOR : TemplateTypes.PIA;
    }

    changeTab(tab: ITabsNav): void {
        this.reset$.next();
        this.currentTab = tab.id;
        this.loadingTemplate = true;
        switch (tab.id) {
            case TemplatesTypeTabOptions.Archive:
                this.getArchiveTemplates();
                break;
            case TemplatesTypeTabOptions.Templates:
                this.getTemplatesList();
                break;
        }
        this.cards = [];
    }
    navigateToPublishedTemplate(card: IGalleryTemplateCardAttributes) {
        if (card.id && card.published.version) {
            this.stateService.transitionTo("zen.app.pia.module.templatesv2.studio.manage", { templateId: card.id, version: card.published.version });
        }
    }
    navigateToDraftTemplate(card: IGalleryTemplateCardAttributes) {
        if (card.id && card.draft.version) {
            this.stateService.transitionTo("zen.app.pia.module.templatesv2.studio.manage", { templateId: card.id, version: card.draft.version });
        }
    }
    navigateToArchivedTemplate(card: IGalleryTemplateCardAttributes) {
        if (card.id) {
            this.stateService.transitionTo("zen.app.pia.module.templatesv2.studio.manage", { templateId: card.id, version: (card.draft ? card.draft : card.published)});
        }
    }
    openUnarchiveModal(card: IGalleryTemplateCardAttributes) {
        if (this.permissionPipe.transform("TemplatesUnarchive")) {
            const templateName: string = card.draft ? card.draft.name : card.published.name;
            this.otModalService.create(
                ArchiveTemplateModalComponent,
                {
                    isComponent: true,
                },
                {
                    title: this.translatePipe.transform("UnarchiveTemplate"),
                    archiveTemplate: false,
                    templateName,
                },
            ).subscribe((data: boolean): void => {
                if (data) {
                    this.templateAction.unarchiveTemplate(card.id);
                }
            });
        }
    }
    toggleContextMenuOpen(card: IGalleryTemplateCardAttributes) {
        card.contextMenuOpen = !card.contextMenuOpen;
    }

    pageChanged(page: number): void {
        this.fetchPagedData(page - 1);
    }

    search(searchText: string): void {
        this.searchText$.next(searchText || "");
    }

    private fetchPagedData = (page: number): void => {
        this.pageSize = 20;
        let searchFilteredCards = this.cards;
        if (this.searchText) {
            searchFilteredCards = this.cards.filter((card) => card.header.text.toLowerCase().includes(this.searchText.toLowerCase()));
        }
        this.numPages = Math.ceil(searchFilteredCards.length / this.pageSize);
        this.minIndex = page * this.pageSize;
        if (this.minIndex >= searchFilteredCards.length && page) {
            page--;
            this.minIndex = page * this.pageSize;
        }
        this.currentPage = page + 1;
        this.maxIndex = (this.minIndex + this.pageSize) > searchFilteredCards.length ?
            searchFilteredCards.length :
            this.minIndex + this.pageSize;
        this.paginatedCards = searchFilteredCards.slice(this.minIndex, this.maxIndex);
        this.paginationDetail = {
            first: this.minIndex === 0,
            last: searchFilteredCards.length === this.maxIndex,
            number: page,
            numberOfElements: (this.paginatedCards.length < this.pageSize) ? this.paginatedCards.length : this.pageSize,
            size: this.pageSize,
            sort: null,
            totalElements: searchFilteredCards.length,
            totalPages: this.numPages,
        };
    }

    private isRenderedFromVendorModule(): boolean {
        return this.stateService.current.name.search(this.vendorRoutes.templatesStateCheck) > 0;
    }

    private componentWillReceiveState(state: IStoreState): void {
        this.templateIds = getTemplateState(state).templateIds;
        if (state.template.isTemplateCopied) {
            this.getTemplatesList();
            state.template.isTemplateCopied = false;
        }
        this.fetchTemplateData();
    }

    private getTemplatesList(): void {
        this.templateAction
            .setTemplateList(this.getTemplateType())
            .finally(() => this.loadingTemplate = false);
    }

    private getArchiveTemplates(): void {
        this.templateAction
            .getArchiveTemplates(this.getTemplateType())
            .finally(() => this.loadingTemplate = false);
    }

    private setConfigurationByTemplateType(): void {
        switch (this.getTemplateType()) {
            case TemplateTypes.VENDOR:
                this.translations = {
                    templateHeader: this.translatePipe.transform("VendorTemplates"),
                    addNewTemplate: this.translatePipe.transform("AddNewVendorTemplate"),
                    noVendorTemplate: this.translatePipe.transform("NoVendorTemplates"),
                    addNewTemplateFromButtons: this.translatePipe.transform("CreateVendorTemplateViaButtons"),
                };
                this.templateNavUrl = this.vendorRoutes.templatesList;
                break;
            case TemplateTypes.PIA:
            default:
                this.translations = {
                    templateHeader: this.translatePipe.transform("QuestionnaireTemplates"),
                    addNewTemplate: this.translatePipe.transform("AddNewQuestionnaireTemplate"),
                    noVendorTemplate: this.translatePipe.transform("NoQuestionnaireTemplates"),
                    addNewTemplateFromButtons: this.translatePipe.transform("CreateQuestionnaireTemplateViaButtons"),
                };
                this.templateNavUrl = this.routes.list;
                break;
        }
    }
    private fetchTemplateData(): void {
        this.cards = [];
        const state = this.store.getState();
        const templatesById = getTemplateState(state).templateByIds;

        for (const key in templatesById) {
            if (templatesById[key]) {
                const template = templatesById[key];
                this.cards.push({
                    id: key, // AKA rootVersionId
                    header: {
                        text: template[template.activeState].name,
                    },
                    image: {
                        icon: template[template.activeState].icon,
                    },
                    locked: template[template.activeState].isLocked,
                    content: template[template.activeState].description,
                    status: template.activeState,
                    name: template[template.activeState].name,
                    draft: template.draft ? template.draft : null,
                    published: template.published ? template.published : null,
                    publishButtonText: this.translatePipe.transform("ViewPublishedVersion", { PublishedVersionNumber: template.published ? template.published.version : template.draft.version }),
                    contextMenuOpen: false,
                });
            }
        }
        this.fetchPagedData(this.currentPage ? (this.currentPage - 1) : 0);
    }
}
