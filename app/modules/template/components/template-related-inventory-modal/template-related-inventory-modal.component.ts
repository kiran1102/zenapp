//#region IMPORTS
// Core
import { Component, OnInit, Inject } from "@angular/core";

// 3rd Party
import { find, includes } from "lodash";

// Redux
import { IStore } from "interfaces/redux.interface";
import {
    getQuestionById,
    getSectionByQuestionId,
    getCurrentTemplate,
    getTemplateState,
    getTemplateTempModel,
} from "oneRedux/reducers/template.reducer";
import { getModalData } from "oneRedux/reducers/modal.reducer";

// Services
import { ModalService } from "sharedServices/modal.service";
import TemplateAction from "templateServices/actions/template-action.service";
import { TemplateBuilderService } from "templateServices/template-builder.service";

// Interfaces
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import { IAssessmentConditionsModalResolve } from "interfaces/assessment/assessment-conditions-modal-resolve.interface";
import { IAssessmentTemplateQuestion } from "interfaces/assessment-template-question.interface";
import {
    IRelationshipConfiguration,
    IInventoryRelationshipContract,
    IGetInventoryRelationshipPayload,
} from "modules/template/interfaces/template-related-inventory.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";
import { IAssetRelationshipTypeCheckbox } from "interfaces/inventory.interface";

// Enums / Constants
import { QuestionTypeNames } from "constants/question-types.constant";
import { InventorySchemaIds } from "constants/inventory-config.constant";
import { RelationshipLinkType } from "enums/inventory.enum";
import { RelatedInventoryDestination } from "modules/template/enums/template-related-inventory.enum";
import { TemplateModes } from "enums/template-modes.enum";
import { TemplateStates } from "enums/template-states.enum";

// Tokens
import { StoreToken } from "tokens/redux-store.token";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";
//#endregion IMPORTS

@Component({
    selector: "template-related-inventory-modal",
    templateUrl: "template-related-inventory-modal.component.html",
})

export class TemplateRelatedInventoryModalComponent implements OnInit {

    isReady = false;
    isModalDisabled = false;
    isEditingPublished = false;
    destinationType: RelatedInventoryDestination;
    hasExistingRelationship = false;
    relatedQuestions: IAssessmentTemplateQuestion[] = [];
    selectedQuestion: IAssessmentTemplateQuestion = null;
    summary: string;
    isSaving = false;
    modalData: IAssessmentConditionsModalResolve = getModalData(this.store.getState());
    modalTitle: string;
    questionId = this.modalData.questionId;
    questionModel = getQuestionById(this.questionId)(this.store.getState());
    showAssetRelationshipFields = false;
    assetCheckboxes: IAssetRelationshipTypeCheckbox[] = [
        { label: this.translatePipe.transform("SourceAsset"), selected: false, id: RelationshipLinkType.Source },
        { label: this.translatePipe.transform("StorageAsset"), selected: false, id: RelationshipLinkType.Processing },
        { label: this.translatePipe.transform("DestinationAsset"), selected: false, id: RelationshipLinkType.Destination },
    ];

    constructor(
        @Inject(StoreToken) public store: IStore,
        @Inject("TemplateAction") private templateAction: TemplateAction,
        @Inject("TemplateBuilderService") private templateBuilder: TemplateBuilderService,
        private translatePipe: TranslatePipe,
        private modalService: ModalService,
    ) { }

    ngOnInit(): void {
        this.setModalStateVariables();
        this.configureModal(this.questionModel);
    }

    configureModal(question: IAssessmentTemplateQuestion): void {
        if (question.attributeJson.inventoryType === InventorySchemaIds.Assets) {
            this.modalTitle = this.translatePipe.transform("RelateAssetToProcessingActivity");
            this.summary = this.translatePipe.transform("RelateAssetToProcessingActivitySummary");
            this.destinationType = RelatedInventoryDestination.Asset;
            this.showAssetRelationshipFields = true;
        } else if (question.attributeJson.inventoryType === InventorySchemaIds.Processes) {
            this.modalTitle = this.translatePipe.transform("RelateProcessingActivityToAsset");
            this.summary = this.translatePipe.transform("RelateProcessingActivityToAssetSummary");
            this.destinationType = RelatedInventoryDestination.ProcessingActivity;
        } else if (question.questionType === QuestionTypeNames.PersonalData) {
            this.modalTitle = this.translatePipe.transform("RelatePersonalDataToProcessingActivity");
            this.summary = this.translatePipe.transform("RelatePersonalDataToProcessingActivitySummary");
            this.destinationType = RelatedInventoryDestination.PersonalData;
        }

        this.hasExistingRelationship = question.destinationInventoryQuestion;
        this.relatedQuestions = this.templateBuilder.fillRelatedInventoryQuestionsDropdown(this.modalData.questionId, this.destinationType);

        if (this.hasExistingRelationship) {
            this.getSelectedQuestion();
        } else {
            this.isReady = true;
        }
    }

    closeModal(): void {
        this.modalService.closeModal();
        this.modalService.handleModalCallback();
        this.modalService.clearModalData();
    }

    cancel(): void {
        this.closeModal();
    }

    trackbyFn(index: number): number {
        return index;
    }

    setModalStateVariables(): void {
        const { templateMode } = getTemplateState(this.store.getState());
        const isPublished = getCurrentTemplate(this.store.getState()).status === TemplateStates.Published;
        this.isModalDisabled = isPublished && templateMode === TemplateModes.Readonly;
        this.isEditingPublished = isPublished && templateMode === TemplateModes.Edit;
    }

    getSelectedQuestionPublished(relationship: IInventoryRelationshipContract = null): IAssessmentTemplateQuestion {
        return relationship
            ? this.templateBuilder.applyInventoryQuestionFormatting(
                getQuestionById(relationship.sourceRelationshipConfiguration[0].sourceInventoryQuestionId)(this.store.getState()),
                getSectionByQuestionId(relationship.sourceRelationshipConfiguration[0].sourceInventoryQuestionId)(this.store.getState()).sequence,
            ) : null;
    }

    getSelectedQuestion(): void {
        const state = this.store.getState();
        const templateId = getCurrentTemplate(state).id;
        const sectionId = getSectionByQuestionId(this.questionId)(state).id;
        const localRelationships = getTemplateTempModel(state).inventoryRelations || [];
        const localRelationshipQuestionIds = localRelationships.map((obj) => obj.destinationInventoryQuestionId);

        if (this.isEditingPublished && localRelationships.length && includes(localRelationshipQuestionIds, this.questionId)) {
            const relationship = find(localRelationships, (obj) => obj.destinationInventoryQuestionId === this.questionId);
            // Pre-select the assetCheckboxes
            if (this.destinationType === RelatedInventoryDestination.Asset) {
                const selectedRelationshipTypes = relationship.sourceRelationshipConfiguration.map((obj: IRelationshipConfiguration): RelationshipLinkType => obj.relationshipType);
                this.assetCheckboxes = this.configureAssetCheckboxes(selectedRelationshipTypes);
            }
            this.selectedQuestion = this.getSelectedQuestionPublished(relationship);
            this.isReady = true;
        } else {
            this.templateAction.getInventoryRelationship(templateId, sectionId, this.questionId).then((res: IProtocolResponse<IGetInventoryRelationshipPayload[]>) => {
                if (res.result && res.data.length) {
                    const question = getQuestionById(res.data[0].sourceInventoryQuestionId)(state);
                    const sectionSequence = getSectionByQuestionId(res.data[0].sourceInventoryQuestionId)(state).sequence;

                    // Pre-select the assetCheckboxes
                    if (this.destinationType === RelatedInventoryDestination.Asset) {
                        const selectedRelationshipTypes = res.data.map((obj: IGetInventoryRelationshipPayload): RelationshipLinkType => obj.relationshipType);
                        this.assetCheckboxes = this.configureAssetCheckboxes(selectedRelationshipTypes);
                    }
                    this.selectedQuestion = this.templateBuilder.applyInventoryQuestionFormatting(question, sectionSequence);
                }
                this.isReady = true;
            });
        }
    }

    configureAssetCheckboxes(selectedRelationshipTypes: RelationshipLinkType[] = []): IAssetRelationshipTypeCheckbox[] {
        return this.assetCheckboxes.map((obj: IAssetRelationshipTypeCheckbox): IAssetRelationshipTypeCheckbox => {
            obj.selected = includes(selectedRelationshipTypes, obj.id);
            return obj;
        });
    }

    canSave(): boolean {
        return Boolean(this.selectedQuestion)
            && this.hasAssetRelationships();
    }

    hasAssetRelationships(): boolean {
        if (this.questionModel.attributeJson && this.questionModel.attributeJson.inventoryType !== InventorySchemaIds.Assets) return true;
        return Boolean(find(this.assetCheckboxes, (option) => option.selected === true));
    }

    onQuestionSelect(question: IAssessmentTemplateQuestion): void {
        this.selectedQuestion = question;
    }

    onCheckboxToggle(selection: IAssetRelationshipTypeCheckbox, isSelected: boolean): void {
        this.assetCheckboxes = this.assetCheckboxes.map((opt: IAssetRelationshipTypeCheckbox) => {
            if (opt.id === selection.id) opt.selected = isSelected;
            return opt;
        });
    }

    updateRelationship(isRemoving: boolean = false): void {
        this.isSaving = true;
        const contract: IInventoryRelationshipContract = isRemoving ? this.configureRemovePayload() : this.configureAddPayload();
        const templateId = getCurrentTemplate(this.store.getState()).id;

        if (this.isEditingPublished) {
            this.templateAction.updateInventoryRelationshipPublished(this.configurePublishedPayload(contract));
            this.updateQuestionRelationshipConfig(!isRemoving);
        } else {
            this.templateAction.updateInventoryRelationshipDraft({ templateId, contract }, this.getPayloadMessages(isRemoving))
                .then((res: boolean) => {
                    if (res) {
                        this.updateQuestionRelationshipConfig(!isRemoving);
                    } else {
                        this.isSaving = false;
                    }
                });
        }
    }

    updateQuestionRelationshipConfig(isDestinationInventoryQuestion: boolean): void {
        this.templateAction.updateQuestionRelationshipConfig(this.questionId, isDestinationInventoryQuestion);
        this.closeModal();
    }

    getPayloadMessages(isRemoving: boolean = false): { success: string, error: string } {
        return isRemoving
            ? { success: "Success.RemoveInventoryRelationship", error: "Error.RemoveInventoryRelationship" }
            : { success: "Success.AddInventoryRelationship", error: "Error.AddInventoryRelationship" };
    }

    configurePublishedPayload(contract: IInventoryRelationshipContract): IInventoryRelationshipContract[] {
        const current = getTemplateTempModel(this.store.getState()).inventoryRelations || [];
        return current.length && !!find(current, (obj) => obj.destinationInventoryQuestionId === this.questionId)
            ? current.map((obj) => (obj.destinationInventoryQuestionId === this.questionId) ? contract : obj)
            : current.concat(contract);
    }

    configureRemovePayload(): IInventoryRelationshipContract {
        return {
            destinationInventorySectionId: getSectionByQuestionId(this.questionId)(this.store.getState()).id,
            destinationInventoryQuestionId: this.questionId,
            sourceRelationshipConfiguration: [],
        };
    }

    configureAddPayload(): IInventoryRelationshipContract {
        const sourceRelationshipConfiguration = this.getRelationshipConfiguration();

        if (sourceRelationshipConfiguration) {
            return {
                destinationInventorySectionId: getSectionByQuestionId(this.questionId)(this.store.getState()).id,
                destinationInventoryQuestionId: this.questionId,
                sourceRelationshipConfiguration,
            };
        } else {
            return null;
        }
    }

    getRelationshipConfiguration(): IRelationshipConfiguration[] {
        const sourceInventorySectionId = getSectionByQuestionId(this.selectedQuestion.id)(this.store.getState()).id;

        switch (this.destinationType) {
            case RelatedInventoryDestination.Asset:
                return this.assetCheckboxes
                    .filter((option) => option.selected)
                    .map((option) => {
                        return {
                            sourceInventorySectionId,
                            sourceInventoryQuestionId: this.selectedQuestion.id,
                            relationshipType: option.id,
                        };
                    });
            case RelatedInventoryDestination.ProcessingActivity:
                return [{
                    sourceInventorySectionId,
                    sourceInventoryQuestionId: this.selectedQuestion.id,
                    relationshipType: RelationshipLinkType.Related,
                }];
            case RelatedInventoryDestination.PersonalData:
                return [{
                    sourceInventorySectionId,
                    sourceInventoryQuestionId: this.selectedQuestion.id,
                    relationshipType: RelationshipLinkType.Related,
                }];
        }
    }
}
