import {
    Component,
    Input,
    Output,
    EventEmitter,
} from "@angular/core";

@Component({
    selector: "icon-list",
    template: `
    <div otFormElement
        class="icon-list"
        [label]="'TemplateIcon' | otTranslate"
    >
        <div class="row-wrap-flex-start margin-top-half">
            <div
                *ngFor="let icon of iconList; index as i"
                class="icon-list__item icon-list__border text-pointer no-outline"
                [attr.ot-auto-id]="identifier + '_' + (i+1)"
                [ngClass]="{'icon-list__item--selected': selectedIcon === icon}"
                (click)="selectIcon(icon)"
            >
                <i class="icon-list__item-icon full-width inline-block text-center ot {{icon}}"></i>
            </div>
        </div>
    </div>`,
})
export class TemplateIconListComponent {

    @Input() iconList: string[];
    @Input() selectedIcon: string;
    @Input() identifier: string;

    @Output() iconSelected: EventEmitter<string> = new EventEmitter<string>();

    selectIcon(icon: string) {
        this.iconSelected.emit(icon);
    }
}
