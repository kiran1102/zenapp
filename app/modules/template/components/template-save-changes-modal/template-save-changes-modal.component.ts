// Angular
import {
    Component,
    OnInit,
    Inject,
} from "@angular/core";

// Vitreus
import { IOtModalContent } from "@onetrust/vitreus";

// 3rd party
import { Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";

// Redux
import { StoreToken } from "tokens/redux-store.token";
import {
    TemplateActions,
    getTemplateTempModel,
} from "oneRedux/reducers/template.reducer";

// Interface
import { IStore } from "interfaces/redux.interface";
import { ITemplateUpdateRequest } from "modules/template/interfaces/template-update-request.interface";
import { TemplateStates } from "constants/template-states.constant";
import { IAssessmentTemplateDetailUpdateParams } from "interfaces/assessment-template-detail.interface";

// Enum
import { TemplateModes } from "enums/template-modes.enum";

// Services
import { TemplateDetailsAPIService } from "modules/template/services/template-details-api.service";

@Component({
    selector: "template-save-changes-modal",
    templateUrl: "template-save-changes-modal.component.html",
})
export class TemplateSaveChangesModalComponent implements OnInit, IOtModalContent {

    otModalCloseEvent: Subject<boolean>;
    otModalData = null;
    submitting: boolean;
    published: boolean;

    private destroy$ = new Subject();

    constructor(
        @Inject(StoreToken) private store: IStore,
        private templateDetailsAPIService: TemplateDetailsAPIService,
    ) { }

    ngOnInit(): void {
        this.published = this.otModalData.templateStatus === TemplateStates.PUBLISHED.toUpperCase();
    }

    closeModal(): void {
        this.otModalCloseEvent.next();
        this.otModalCloseEvent.complete();
    }

    handleDiscard(): void {
        this.store.dispatch({ type: TemplateActions.RESET_TEMP_MODEL_WITH_CURRENT });
        this.otModalCloseEvent.next(false);
        this.otModalCloseEvent.complete();
    }

    handleSubmit(): void {
        this.submitting = true;

        if (this.published) {
            this.updateTemplateDetails(this.otModalData.templateId);
        } else {
            this.updateDraftTemplateDetails();
        }
    }

    private updateDraftTemplateDetails(): void {
        const templateTempModel = getTemplateTempModel(this.store.getState());
        const templateUpdateRequest: IAssessmentTemplateDetailUpdateParams = {
            metadata: { ...templateTempModel.metadata },
            settings: {
                prepopulateAttributeQuestions: templateTempModel.settings.prepopulateAttributeQuestions,
            },
        };

        this.templateDetailsAPIService.updateDraftTemplateDetails(this.otModalData.templateId, templateUpdateRequest)
            .pipe(
                takeUntil(this.destroy$),
            ).subscribe((result): void => {
                if (result) {
                    this.updateStoreAndCloseModal(templateTempModel, TemplateModes.Create);
                }
                this.submitting = false;
            });
    }

    private updateTemplateDetails(templateId: string): ng.IPromise<boolean> {
        const templateTempModel: ITemplateUpdateRequest = getTemplateTempModel(this.store.getState());
        const request: ITemplateUpdateRequest = {
            ...templateTempModel,
            settings: {
                ...templateTempModel.settings,
                isLocked: false,
            },
        };
        return this.templateDetailsAPIService.updatePublishedTemplate(templateId, request).then((result: boolean): boolean => {
            if (result) {
                this.updateStoreAndCloseModal(request, TemplateModes.Readonly);
            }
            this.submitting = false;
            return result;
        });
    }

    private updateStoreAndCloseModal(template: ITemplateUpdateRequest, mode: string): void {
        this.store.dispatch({ type: TemplateActions.UPDATE_TEMPLATE, template });
        this.store.dispatch({ type: TemplateActions.UPDATE_TEMPLATE_MODE, templateMode: mode });
        this.otModalCloseEvent.next(true);
        this.otModalCloseEvent.complete();
    }
}
