// Angular
import {
    Component,
    Inject,
    OnInit,
} from "@angular/core";

// 3rd party
import { Subject } from "rxjs";

// Constants
import { TemplateTypes } from "constants/template-types.constant";
import { WelcomeText } from "constants/welcome-text.constant";

// Interfaces
import { IOtModalContent } from "@onetrust/vitreus";
import {
    IAssessmentCustomTemplate,
    IAssessmentGalleryTemplate,
} from "modules/template/interfaces/template.interface";
import {
    ITemplateVersion,
    ITemplateSettings,
} from "modules/template/interfaces/template.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";
import { IStore } from "interfaces/redux.interface";
import { ITemplateCreateModalData } from "interfaces/template-create-modal-data.interface";
import { IOrganization } from "interfaces/organization.interface";
import { ICreateTemplateVersionResponse } from "interfaces/assessment-template-detail.interface";
import { TemplateActions } from "oneRedux/reducers/template.reducer";

// Services
import OTIconService from "templateServices/icon.service";
import { StateService } from "@uirouter/core";
import { TemplateCreateService } from "modules/template/services/template-create.service";
import { NotificationService } from "sharedModules/services/provider/notification.service";

// Redux
import { StoreToken } from "tokens/redux-store.token";
import { getCurrentOrgId } from "oneRedux/reducers/org.reducer";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

@Component({
    selector: "template-create-form",
    templateUrl: "./template-create-form.component.html",
})
export class TemplateCreateFormComponent implements OnInit, IOtModalContent {

    otModalCloseEvent: Subject<{ templateId: string, version: number }>;
    otModalData: ITemplateCreateModalData = null;

    iconList: string[] = [];
    isSubmitting = false;
    description = "";
    icon = "";
    templateName = "";
    templateId = "";
    selectedOrg: IOrganization = null;
    templateSettings: ITemplateSettings;

    private currentOrgId = getCurrentOrgId(this.store.getState());

    constructor(
        @Inject(StoreToken) private store: IStore,
        @Inject("OTIconService") private otIconService: OTIconService,
        private stateService: StateService,
        private templateCreateService: TemplateCreateService,
        private translatePipe: TranslatePipe,
        private notificationService: NotificationService,
    ) { }

    ngOnInit(): void {
        this.iconList = this.otIconService.getIconList();
        this.selectedOrg = this.templateCreateService.getSelectedOrgModel(this.currentOrgId);
        this.getTemplateData();
        this.templateCreateService.getTemplateSettings()
            .subscribe((response) => {
                if (response.result) {
                    this.templateSettings = {
                        allowAccessControl: response.data.allowAccessControl,
                        allowedAssessmentCreationOrgHierarchy: response.data.allowedAssessmentCreationOrgHierarchy,
                    };
                }
            });
    }

    iconSelected(icon: string): void {
        this.icon = icon;
    }

    onCreateTemplateClick(): void {
        if (this.otModalData && this.otModalData.copying) {
            this.copyTemplate();
        } else {
            this.templateId ? this.cloneTemplate() : this.createTemplate();
        }
    }

    handleInputChange(value: string): void {
        this.description = value;
    }

    onNameChange(name: string): void {
        this.templateName = name;
    }

    orgSelect(orgId: string): void {
        this.selectedOrg = this.templateCreateService.getSelectedOrgModel(orgId);
    }

    closeModal(): void {
        this.otModalCloseEvent.next();
        this.otModalCloseEvent.complete();
    }

    private getTemplateType(): string {
        return (this.stateService.current.name.search("vendor.templates") > 0) ?
            TemplateTypes.VENDOR :
            TemplateTypes.PIA;
    }

    private createTemplate(): void {
        this.isSubmitting = true;
        const template: IAssessmentCustomTemplate = {
            description: this.description,
            icon: this.icon,
            name: this.templateName,
            templateType: this.getTemplateType(),
            welcomeText: WelcomeText.WELCOME_TEXT,
            orgGroupId: this.selectedOrg.id,
        };
        this.templateCreateService.createTemplate(template)
            .subscribe((response: IProtocolResponse<ITemplateVersion>): any => {
                this.isSubmitting = false;
                if (response.result) {
                    this.setSuccessResponse(response.data);
                }
            });
    }

    private cloneTemplate(): void {
        this.isSubmitting = true;
        const template: IAssessmentGalleryTemplate = {
            description: this.description,
            icon: this.icon,
            name: this.templateName,
            templateId: this.templateId,
            orgGroupId: this.selectedOrg.id,
        };
        this.templateCreateService.cloneTemeplateGallery(template)
            .subscribe((response: IProtocolResponse<ITemplateVersion>): any => {
                this.isSubmitting = false;
                if (response.result) {
                    this.setSuccessResponse(response.data);
                }
            });
    }

    private copyTemplate(): void {
        this.isSubmitting = true;
        const template: IAssessmentGalleryTemplate = {
            description: this.description,
            icon: this.icon,
            name: this.templateName,
            templateId: this.templateId,
            orgGroupId: this.selectedOrg.id,
        };

        this.templateCreateService.copyTemplate(template)
            .subscribe((response: IProtocolResponse<ICreateTemplateVersionResponse>): boolean => {
                if (response.result) {
                    this.store.dispatch({ type: TemplateActions.COPY_TEMPLATE, isTemplateCopied: true });
                    this.otModalCloseEvent.next();
                    this.otModalCloseEvent.complete();
                    this.stateService.go("zen.app.pia.module.templatesv2.studio.manage", { templateId: response.data.rootVersionId, version: response.data.templateVersion });
                } else {
                    this.notificationService.alertWarning(this.translatePipe.transform("Error"), this.translatePipe.transform("DuplicateTemplateName"));
                }
                this.isSubmitting = false;
                return response.result;
            });
    }

    private getTemplateData(): void {
        if (this.otModalData) {
            this.templateId = this.otModalData.id;
            this.icon = this.otModalData.icon;
            this.description = this.otModalData.description;
            this.templateName = this.otModalData.name;
        }
        if (!this.icon) {
            this.icon = this.iconList[0];
        }
    }

    private setSuccessResponse(templateData: ITemplateVersion): void {
        this.otModalCloseEvent.next({
            templateId: templateData.rootVersionId,
            version: templateData.templateVersion,
        });
        this.otModalCloseEvent.complete();
    }
}
