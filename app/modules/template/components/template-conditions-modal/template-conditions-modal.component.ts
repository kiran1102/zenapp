//#region IMPORTS
// Rxjs
import {
    Subscription,
    Subject,
} from "rxjs";
import {
    first,
    startWith,
    takeUntil,
} from "rxjs/operators";

// Core
import {
    Component,
    OnInit,
    OnDestroy,
    Inject,
} from "@angular/core";

// External Libraries
import {
    isFunction,
    uniqueId,
} from "lodash";

// Interfaces
import {
    IStringMap,
} from "interfaces/generic.interface";
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import { IAssessmentConditionsModalResolve } from "interfaces/assessment/assessment-conditions-modal-resolve.interface";
import { IAssessmentTemplateSection } from "interfaces/assessment-template-section.interface";
import { IAssessmentNavigation } from "interfaces/assessment/assessment-navigation.interface";
import { IAssessmentTemplateQuestion } from "interfaces/assessment-template-question.interface";

// Services
import { ModalService } from "sharedServices/modal.service";
import { TemplateAPI } from "templateServices/api/template-api.service";
import TemplateAction from "templateServices/actions/template-action.service";
import { TemplateRuleService } from "../template-page/template-rule-list/template-rule/template-rule.service";

// Redux
import {
    IStore,
    IStoreState,
} from "interfaces/redux.interface";
import observableFromStore from "oneRedux/utils/observable-from-store";
import {
    getSectionById,
    allTempNavigationValid,
    getCurrentTemplate,
    getNavigationsByQuestion,
    getQuestionById,
    getTempNavigationById,
    TemplateActions,
} from "oneRedux/reducers/template.reducer";
import { StoreToken } from "tokens/redux-store.token";
import { getModalData } from "oneRedux/reducers/modal.reducer";

// Enums / Constants
import { QuestionTypes } from "constants/question-types.constant";
import {
    LogicalOperators,
    ComparisonOperators,
} from "enums/operators.enum";
import { TemplateRuleActions } from "modules/template/enums/template-rule.enums";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

//#endregion IMPORTS

@Component({
    selector: "template-conditions-modal",
    templateUrl: "template-conditions-modal.component.html",
})

export class TemplateConditionsModalComponent implements OnInit, OnDestroy {

    allowedActions = [ TemplateRuleActions.SKIP ];
    allConditionsValid: boolean;
    cancelButtonText: string;
    conditions: IAssessmentNavigation[] = [];
    hideSubmitButton: boolean;
    iconClass: string;
    iconTemplate: string;
    isPreviewMode: boolean;
    isSubmitting: boolean;
    modalData: IAssessmentConditionsModalResolve;
    modalTitle: string;
    nextQuestions: IStringMap<IAssessmentTemplateQuestion[]>;
    nextSections: IAssessmentTemplateSection[] = [];
    isPublished: boolean;
    questionId: string;
    questionName: string;
    questionNumber: string;
    sectionId: string;
    submitButtonText: string;
    submitInProgress: boolean;

    private destroy$ = new Subject();
    private promiseToResolve: () => ng.IPromise<any>;
    private storeSubscription: Subscription;

    constructor(
        @Inject("TemplateAction") private templateAction: TemplateAction,
        @Inject("TemplateAPI") private templateAPI: TemplateAPI,
        @Inject(StoreToken) public store: IStore,
        private translatePipe: TranslatePipe,
        private modalService: ModalService,
        private TemplateRule: TemplateRuleService,
    ) { }

    ngOnInit(): void {
        this.modalData = getModalData(this.store.getState());
        this.modalTitle = this.translatePipe.transform("Conditions");
        this.cancelButtonText = this.translatePipe.transform("Cancel");
        this.submitButtonText = this.translatePipe.transform("Save");
        this.questionId = this.modalData.questionId;
        this.sectionId = this.modalData.sectionId;
        this.isPreviewMode = this.modalData.previewMode;

        if (!this.isPreviewMode) {
            observableFromStore(this.store).pipe(
                startWith(this.store.getState()),
                takeUntil(this.destroy$),
            ).subscribe((state: IStoreState) => this.componentWillReceiveState(state));
        }
        this.conditions = getNavigationsByQuestion(this.questionId)(this.store.getState());
        this.TemplateRule.generateSkipConditionCombinations(this.conditions);
        const question = getQuestionById(this.questionId)(this.store.getState());
        const section = getSectionById(this.store.getState(), this.sectionId);
        this.questionNumber = section.sequence + "." + question.sequence;
        this.questionName = question.friendlyName ? question.friendlyName : question.content;
        if (question.questionType === QuestionTypes.Attribute.name) {
            this.templateAction
                .getQuestionOptions(this.questionId, this.sectionId, getCurrentTemplate(this.store.getState()).id).pipe(
                    first(),
                ).subscribe();
        }
    }

    ngOnDestroy(): void {
        this.store.dispatch({ type: TemplateActions.RESET_TEMP_NAVIGATION });
        this.destroy$.next();
        this.destroy$.complete();
    }

    closeModal(): void {
        this.modalService.closeModal();
        this.modalService.handleModalCallback();
        this.modalService.clearModalData();
    }

    handleSubmit(): void {
        if (isFunction(this.promiseToResolve)) {
            this.isSubmitting = true;
            this.promiseToResolve().then((success: boolean) => {
                if (success) {
                    this.modalService.closeModal();
                    this.modalService.handleModalCallback();
                    this.modalService.clearModalData();
                }
                this.isSubmitting = false;
            });
        }
    }

    addCondition(): void {
        const condition: IAssessmentNavigation = {
            conditionType: this.TemplateRule.action.skip.value as any,
            conditions: [{
                logicalOperator: LogicalOperators.OR,
                comparisonOperator: ComparisonOperators.EQUAL_TO,
                optionId: null,
                optionType: null,
            }],
            destinationQuestionId: "",
            destinationSectionId: "",
            id: this.questionId + "_" + uniqueId(),
            sourceQuestionId: this.questionId,
            sourceSectionId: this.sectionId,
            added: true,
            invalid: true,
        };

        this.TemplateRule.addSelectedSkipConditionCombination(condition);
        this.conditions.push(condition);

        this.store.dispatch({ type: TemplateActions.UPDATE_TEMP_NAVIGATION, navigation: condition });
    }

    cancel(): void {
        this.closeModal();
    }

    saveNavigations(): void {
        this.submitInProgress = true;
        const updatedNavigations = this.conditions.map((condition: IAssessmentNavigation): IAssessmentNavigation => {
            return getTempNavigationById(condition.id)(this.store.getState());
        });
        updatedNavigations.forEach((condition: IAssessmentNavigation) => {
            if (condition.added) {
                condition.id = "";
                delete condition["added"];
            }
        });

        this.templateAction.updateNavigations(getCurrentTemplate(this.store.getState()).id, this.sectionId, this.questionId, updatedNavigations)
            .then((success: boolean) => {
                if (success) {
                    this.cancel();
                }
            })
            .finally(() => this.submitInProgress = false);
    }

    onConditionDeleted(conditionId: string): void {
        this.conditions = this.conditions.filter((condition: IAssessmentNavigation) => condition.id !== conditionId);
        this.store.dispatch({ type: TemplateActions.DELETE_TEMP_NAVIGATION, navigationId: conditionId });
        this.TemplateRule.removeSelectedSkipConditionCombination(this.questionId, conditionId);
    }

    trackByCondition(index: number, conditionId: string): string {
        return conditionId;
    }

    private componentWillReceiveState(state: IStoreState): void {
        this.allConditionsValid = allTempNavigationValid(this.conditions.map((condition: IAssessmentNavigation) => condition.id))(state);
    }
}
