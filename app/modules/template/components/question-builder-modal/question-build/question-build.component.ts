// Rxjs
import { distinctUntilChanged } from "rxjs/operators";

// External libraries
import {
    OnInit,
    Inject,
    Component,
    Input,
    Output,
    OnChanges,
    SimpleChanges,
    EventEmitter,
    ChangeDetectorRef,
} from "@angular/core";
import {
    FormBuilder,
    FormGroup,
    Validators,
} from "@angular/forms";

import {
    assign,
    trim,
} from "lodash";

// Constants
import { QuestionTypes } from "constants/question-types.constant";

// Enums
import { TemplateModes } from "enums/template-modes.enum";

// Interfaces
import {
    IQuestionDetails,
    IQuestionOptionDetails,
} from "interfaces/assessment-template-question.interface";
import { IStringMap } from "interfaces/generic.interface";
import { IQuestionTypes } from "modules/template/interfaces/template.interface";
import { IQuestionType } from "interfaces/question-type-attribute.interface";

// Pipe
import { TranslatePipe } from "pipes/translate.pipe";

// Services
import { QuestionBuilderService } from "templateServices/question-builder.service";
import TemplateAction from "templateServices/actions/template-action.service";
import { TemplateBuilderModalHelperService } from "modules/template/services/template-builder-modal-helper.service";

@Component({
    selector: "question-build",
    templateUrl: "./question-build.component.html",
})

export class QuestionBuildComponent implements OnInit, OnChanges {
    @Input() question: IQuestionDetails;
    @Input() selectedQuestionType: IQuestionType;
    @Input() templateEditMode: TemplateModes;
    @Input() content: string;
    @Input() isQuestionPreviewEnabled: boolean;
    @Input() isQuestionBuildEnabled: boolean;
    @Output() questionChange = new EventEmitter<IQuestionDetails>();
    @Output() scrollChange = new EventEmitter();

    questionBuildForm: FormGroup;
    questionContentTitle: string;
    questionTypes: IStringMap<IQuestionTypes> = QuestionTypes;
    templateModes = TemplateModes;
    disableOnChanges: boolean;
    questionFieldVisibility: IStringMap<boolean>;

    constructor(
        @Inject("TemplateAction") private templateAction: TemplateAction,
        private translatePipe: TranslatePipe,
        private formBuilder: FormBuilder,
        private changeDetectorRef: ChangeDetectorRef,
        private questionBuilderService: QuestionBuilderService,
        private templateBuilderModalHelperService: TemplateBuilderModalHelperService) { }

    ngOnInit(): void {
        this.setQuestionContentTitle();
        const options = this.question.options && this.question.options.map((option: IQuestionOptionDetails): IQuestionOptionDetails => ({ ...option }));
        this.question.options = options;
        if (!(this.question && this.question.id)) {
            this.question.options = [];
            this.onAddOptionClick();
        }
        this.initForm();
    }

    ngOnChanges(changes: SimpleChanges): void {
        if (changes.question || changes.selectedQuestionType) {
            this.setQuestionContentTitle();
            this.questionFieldVisibility = this.setQuestionFieldVisibility();

            if (this.questionBuildForm) {
                if (this.selectedQuestionType.value === QuestionTypes.Statement.name) {
                    this.questionBuildForm.get("description").setValue("");
                }
                this.setValidatorsForDescription();
            }

            if (changes.selectedQuestionType && !changes.selectedQuestionType.firstChange && this.selectedQuestionType.value === QuestionTypes.MultiChoice.name) {
                this.question.options = [];
                this.onAddOptionClick();
            }
        }

        this.questionChange.emit(this.question);
        this.changeDetectorRef.detectChanges();
    }

    setQuestionContentTitle(): void {
        const showTitle = this.selectedQuestionType.value === QuestionTypes.Statement.name || this.selectedQuestionType.value === QuestionTypes.Incident.name;
        this.questionContentTitle = this.translatePipe.transform(showTitle ? "Title" : "Question");
    }

    handleDescriptionChange(event: any) {
        this.questionBuildForm.get("description").setValue(event);
    }

    setValidatorsForDescription(): void {
        if (this.selectedQuestionType.value === QuestionTypes.Statement.name) {
            this.questionBuildForm.get("description").setValidators([Validators.required]);
        } else {
            this.questionBuildForm.get("description").clearValidators();
        }
        this.questionBuildForm.get("description").updateValueAndValidity();
    }

    setOption(option: string, index: number = 0): void {
        if (!this.question.options[index].id) {
            this.question.options[index].id = this.templateAction.generateOptionUUID();
        }
        this.question.options[index].option = option;
        this.question.options[index].sequence = index + 1;
        this.question.options = [...this.question.options];
        this.templateBuilderModalHelperService.updateQuestion(this.question);
    }

    handleOptionAttributesInputChange(hint: string, prop: string, index: number = 0): void {
        this.question.options[index].attributeJson[prop] = trim(hint);
        this.question.options[index].Hint = trim(hint);
        this.templateBuilderModalHelperService.updateQuestion(this.question);
    }

    onRemoveOptionClick(index: number): void {
        if (this.question.options.length < 2) {
            this.question.options[0] = this.questionBuilderService.getEmptyOption();
        } else {
            this.question.options.splice(index, 1);
        }
    }

    onAddOptionClick(index: number = 0, event?: MouseEvent): void {
        if (!this.question.options) {
            this.question.options = [];
        }
        if (this.question.options.length) {
            this.question.options.splice(index + 1, 0, this.questionBuilderService.getEmptyOption());
        } else {
            this.question.options.push(this.questionBuilderService.getEmptyOption());
        }
        if (event) {
            this.scrollChange.emit(event);
        }
    }

    trackByOptionId(option: IQuestionOptionDetails): string {
        return option.id;
    }

    private setQuestionFieldVisibility(): IStringMap<boolean> {
        return {
            description: this.setDescriptionVisibility(),
            friendlyName: this.setFriendlyNameVisibility(),
            mcOptions: this.setMcOptionsVisibility(),
            questionHint: this.setQuestionHintVisibility(),
        };
    }

    private setQuestionHintVisibility(): boolean {
        return this.question.attributeJson.isQuestionHintEnabled
            && this.selectedQuestionType.value !== QuestionTypes.Statement.name
            && this.selectedQuestionType.value !== QuestionTypes.Incident.name;
    }

    private setFriendlyNameVisibility(): boolean {
        return this.selectedQuestionType.value !== QuestionTypes.Statement.name;
    }

    private setDescriptionVisibility(): boolean {
        return this.selectedQuestionType.value === QuestionTypes.Statement.name;
    }

    private setMcOptionsVisibility(): boolean {
        return this.selectedQuestionType.value === QuestionTypes.MultiChoice.name;
    }

    private initForm(): void {
        this.questionFieldVisibility = this.setQuestionFieldVisibility();
        // To do Validations
        this.questionBuildForm = this.formBuilder.group({
            question: [this.question.content, [Validators.required]],
            questionHint: [this.question.hint],
            friendlyName: [this.question.friendlyName, [Validators.maxLength(150)]],
            description: [this.question.description],
        });
        if (this.selectedQuestionType.value === QuestionTypes.Statement.name) {
            this.setValidatorsForDescription();
        }
        this.questionBuildForm.valueChanges.pipe(
            distinctUntilChanged(null, (value) => JSON.stringify(value)),
        ).subscribe((question) => {
            this.updateQuestionBuild();
        });
    }

    private updateQuestionBuild(): void {
        const question: IQuestionDetails = {
            ...this.question,
            content: this.questionBuildForm.controls.question.value,
            description: this.questionBuildForm.controls.description.value,
            friendlyName: trim(this.questionBuildForm.controls.friendlyName.value),
            hint: trim(this.questionBuildForm.controls.questionHint.value),
        };
        assign(this.question, question);
    }
}
