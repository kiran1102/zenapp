// External library
import {
    Component,
    Inject,
    Input,
    Output,
    EventEmitter,
    OnInit,
    OnChanges,
    SimpleChanges,
} from "@angular/core";
import {
    some,
    filter,
    find,
} from "lodash";
import { TranslatePipe } from "pipes/translate.pipe";

// Constants
import {
    QuestionTypes,
    QuestionTypeNames,
} from "constants/question-types.constant";

// Enums
import { QuestionAnswerTypes } from "enums/question-types.enum";
import { TemplateModes } from "enums/template-modes.enum";

// Redux
import {
    getTemplateState,
    getQuestionById,
    getSectionByQuestionId,
} from "oneRedux/reducers/template.reducer";
import { StoreToken } from "tokens/redux-store.token";
import { getModalData } from "oneRedux/reducers/modal.reducer";

// Interfaces
import {
    IQuestionDetails,
    IQuestionAttributes,
    IAssessmentTemplateAttribute,
    IQuestionBuilderModalData,
} from "interfaces/assessment-template-question.interface";
import { IAction } from "interfaces/redux.interface";
import { IQuestionType } from "interfaces/question-type-attribute.interface";
import { IStringMap } from "interfaces/generic.interface";
import { IQuestionTypes } from "modules/template/interfaces/template.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";
import { IInventorySchemaV2 } from "interfaces/inventory.interface";
import { IAssessmentTemplateOptionSettingsDetails } from "interfaces/assessment-template-detail.interface";
import { IStore } from "interfaces/redux.interface";

// services
import TemplateAction from "templateServices/actions/template-action.service";
import { QuestionBuilderService } from "templateServices/question-builder.service";
import { TemplateBuilderService } from "templateServices/template-builder.service";
import { Permissions } from "modules/shared/services/helper/permissions.service";
import { InventorySchemaIds } from "constants/inventory-config.constant";

@Component({
    selector: "question-configuration",
    templateUrl: "question-configuration.component.html",
})

export class QuestionConfigurationComponent implements OnInit, OnChanges {
    @Input() question: IQuestionDetails;
    @Input() selectedQuestionType: IQuestionType;
    @Input() templateEditMode: TemplateModes;
    @Input() isInventoryQuestion: boolean;
    @Input() parentQuestionId: string;
    @Input() questionIdExists: boolean;
    @Output() handleQuestionConfigurationToggleState = new EventEmitter<IAction>();
    @Output() selectedInventory = new EventEmitter<IInventorySchemaV2>();
    @Output() handleAttributeQuestionChange = new EventEmitter();

    attributePlaceholderText: string;
    inventoryAttributes: IQuestionAttributes[] = [];
    inventoryQuestions: IQuestionDetails[] = [];
    inventoryTypes: IInventorySchemaV2[];
    isAttributeDropdownDisabled = true;
    isInventoryTypesFetched = false;
    isQuestionConfigurationExpanded = true;
    isQuestionConfigurationDisabled = false;
    linkAssessmentToInventoryDetails: IAssessmentTemplateOptionSettingsDetails;
    prePopulateResponseDetails: IAssessmentTemplateOptionSettingsDetails;
    questionAnswerTypes = QuestionAnswerTypes;
    questionTypes: IStringMap<IQuestionTypes> = QuestionTypes;
    selectedAttributeType: IQuestionAttributes;
    selectedInventoryQuestion: IQuestionDetails;
    selectedInventoryType: IInventorySchemaV2;
    showAttributeDeleteMessage = false;
    templateId: string;
    templateModes = TemplateModes;
    private hasDependentAttributeQuestion = false;
    private oldInventorySelectedType: string;
    private modalData: IQuestionBuilderModalData;

    constructor(
        @Inject("TemplateAction") private templateAction: TemplateAction,
        @Inject(StoreToken) private store: IStore,
        @Inject("TemplateBuilderService") private TemplateBuilder: TemplateBuilderService,
        private questionBuilderService: QuestionBuilderService,
        private translatePipe: TranslatePipe,
        private permissions: Permissions,
    ) { }

    ngOnInit(): void {
        this.isQuestionConfigurationDisabled = this.templateEditMode === TemplateModes.Edit;
        this.setInventoryConfiguration();
        this.modalData = getModalData(this.store.getState());
        this.inventoryQuestions = this.questionIdExists ? this.TemplateBuilder.fillInventoryQuestionsDropdown(this.modalData.sectionId, this.modalData.question.id) :
            this.TemplateBuilder.fillInventoryQuestionsDropdown(this.modalData.sectionId);
        this.templateId = getModalData(this.store.getState()).templateId;
        if (this.questionIdExists && this.selectedQuestionType.value === this.questionTypes.Attribute.name) {
            this.isAttributeDropdownDisabled = false;
            this.configureAttributeTypeQuestion();
        }
    }

    ngOnChanges(changes: SimpleChanges): void {
        if (changes.selectedQuestionType
            && !changes.selectedQuestionType.firstChange
            && this.selectedQuestionType.value === this.questionTypes.Attribute.name) {
            this.isAttributeDropdownDisabled = true;
            this.inventoryQuestions = this.questionIdExists ? this.TemplateBuilder.fillInventoryQuestionsDropdown(this.modalData.sectionId, this.modalData.question.id) :
            this.TemplateBuilder.fillInventoryQuestionsDropdown(this.modalData.sectionId);
            this.selectedAttributeType = this.selectedInventoryQuestion = null;
        }
        this.attributePlaceholderText = this.translatePipe.transform("SelectAttribute");
    }

    onToggle(event: boolean, configurationType: string): void {
        this.handleQuestionConfigurationToggleState.emit({ type: configurationType, payload: event });
    }

    fetchInventorySchema(): void {
        this.templateAction.getInventorySchema().then((response: IProtocolResponse<IInventorySchemaV2[]>): void => {
            if (response.result) {
                this.inventoryTypes = response.data.filter((type): boolean => {
                    if (type.id !== InventorySchemaIds.Entities) {
                        return this.questionBuilderService.acceptedInventoryTypes(type);
                    } else {
                        return this.questionBuilderService.acceptedInventoryTypes(type) && this.permissions.canShow("DataMappingEntitiesInventory");
                    }
                });
                this.inventoryTypes.forEach((type: IInventorySchemaV2) => {
                    type.name = this.translatePipe.transform(type.nameKey);
                });
                this.isInventoryTypesFetched = true;

                // Pre-select Inventory Type when editing already saved question
                if (this.question && this.question.id
                    && this.selectedQuestionType.value === this.questionTypes.Inventory.name) {
                    this.selectedInventoryType = this.questionBuilderService.preSelectInventoryType(this.inventoryTypes, this.question.attributeJson.inventoryType);
                    this.oldInventorySelectedType = this.selectedInventoryType.id;
                }
            } else {
                this.isInventoryTypesFetched = false;
            }
        });
    }

    handleInventoryTypeChange(selection: IInventorySchemaV2): void {
        this.question.attributeJson["link"] = selection.uri;
        this.question.attributeJson["inventoryType"] = selection.id;
        this.question.attributeJson["displayAnswerType"] = this.questionAnswerTypes.Dropdown;  // Inventory Questions are ALWAYS dropdowns
        this.selectedInventoryType = selection;
        this.showAttributeDeleteMessage = this.oldInventorySelectedType !== selection.id && this.hasDependentAttributeQuestion;
        this.selectedInventory.emit(selection);
    }

    inventoryPrepopulateToggleStateChanged(isInventoryPrepopulated: boolean) {
        this.question.prePopulateResponse = isInventoryPrepopulated;
    }

    linkAssessmentToInventoryToggleStateChanged(): void {
        this.question.linkAssessmentToInventory = !this.question.linkAssessmentToInventory;
    }

    private onInventoryQuestionChange(questionSelection: IQuestionDetails): void {
        this.selectedInventoryQuestion = questionSelection;
        this.parentQuestionId = questionSelection.id;
        this.isAttributeDropdownDisabled = true;
        this.inventoryAttributes = this.selectedAttributeType = this.question.attributeJson.attributeId = null;
        if (questionSelection) {
            this.attributePlaceholderText = this.translatePipe.transform("Loading");
            this.getAttributeTypes(questionSelection.id);
        }
    }

    private onAttributeChange(attributeSelection: IQuestionAttributes): void {
        this.selectedAttributeType = attributeSelection;
        this.handleAttributeQuestionChange.emit({ attributeSelection, parentQuestionId: this.parentQuestionId });
    }

    private setInventoryConfiguration(): void {
        if (this.selectedQuestionType.value === QuestionTypes.Inventory.name) {
            this.fetchInventorySchema();
            this.hasDependentAttributeQuestion = some(getTemplateState(this.store.getState()).builder.questionById, { questionType: QuestionTypeNames.Attribute, parentQuestionId: this.question.id });
        }

        // Inventory Prepopulate details
        this.prePopulateResponseDetails = {
            title: this.translatePipe.transform("PrepopulateInventoryTitle"),
            description: this.translatePipe.transform("PrepopulateInventoryDescription"),
        };

        this.linkAssessmentToInventoryDetails = {
            title: this.translatePipe.transform("LinkSelectionToAssessment"),
            description: this.translatePipe.transform("LinkAssessmentToInventoryDescription"),
        };
    }

    private configureAttributeTypeQuestion() {
        this.setParentFollowUpQuestion();
        this.setAttributeForFollowUpQuestion();
    }

    private setParentFollowUpQuestion(): void {
        const parentQuestion: any = getQuestionById(this.parentQuestionId)(this.store.getState());
        const parentSectionSequence: number = (getSectionByQuestionId(this.parentQuestionId)(this.store.getState())).sequence;
        parentQuestion.content = `${parentSectionSequence}.${parentQuestion.sequence}: `
            + (parentQuestion.friendlyName || parentQuestion.content);
        this.selectedInventoryQuestion = parentQuestion;
    }

    private setAttributeForFollowUpQuestion(): void {
        this.selectedAttributeType = this.question.attributeJson;
        this.getAttributeTypes(this.parentQuestionId);
    }

    private getAttributeTypes(inventoryQuestionId: string): void {
        this.templateAction.getAttributesByInventory(this.templateId, inventoryQuestionId)
            .then((response: IAssessmentTemplateAttribute[]): void => {
                const { isAttributeDropdownDisabled, attributePlaceholderText } = this.TemplateBuilder.handleInventoryQuestionChangeAction(response);
                this.inventoryAttributes = response;
                this.isAttributeDropdownDisabled = isAttributeDropdownDisabled;
                this.attributePlaceholderText = attributePlaceholderText;
            });
    }

}
