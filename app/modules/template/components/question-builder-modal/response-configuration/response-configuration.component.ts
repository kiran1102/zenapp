// External library
import {
    OnInit,
    Component,
    Inject,
    Input,
    Output,
    EventEmitter,
    SimpleChanges,
} from "@angular/core";
import { find } from "lodash";

// Constants
import {
    AttributeType,
    AttributeQuestionResponseType,
    QuestionTypes,
} from "constants/question-types.constant";

// Enums
import { QuestionAnswerTypes } from "enums/question-types.enum";
import { TemplateModes } from "enums/template-modes.enum";

// Interfaces
import { ILabelValue } from "interfaces/generic.interface";
import {
    IQuestionAttributes,
    IAdditionalAttribute,
} from "interfaces/assessment-template-question.interface";
import {
    IQuestionTypeList,
    IQuestionType,
} from "interfaces/question-type-attribute.interface";
import { IStringMap } from "interfaces/generic.interface";
import { IQuestionTypes } from "modules/template/interfaces/template.interface";
import { IAction } from "interfaces/redux.interface";

// Services
import { QuestionBuilderService } from "templateServices/question-builder.service";
import { Permissions } from "modules/shared/services/helper/permissions.service";

@Component({
    selector: "response-configuration",
    templateUrl: "response-configuration.component.html",
})

export class ResponseConfigurationComponent implements OnInit {

    @Input() selectedQuestionType: IQuestionType;
    @Input() questionAttributes: IQuestionAttributes;
    @Input() templateEditMode: TemplateModes;
    @Input() sourceInventoryQuestion: boolean;
    @Input() questionIdExists: boolean;
    @Output() public handleResponseConfigurationToggleState = new EventEmitter<IAction>();
    @Output() handleDisplayAnswerAsChange = new EventEmitter();

    additionalAttributes: IAdditionalAttribute[] = [];
    displayAnswerAsOptions: Array<ILabelValue<string>>;
    isDisplayAnswerTypeEnabled: boolean;
    isInventoryQuestion: boolean;
    isResponseConfigurationExpanded = true;
    isResponseConfigurationDisabled = false;
    isSourceInventoryQuestion: boolean;
    questionTypeList: IQuestionTypeList;
    questionTypes: IStringMap<IQuestionTypes> = QuestionTypes;
    selectedDisplayAnswerAsType: ILabelValue<string>;
    templateModes = TemplateModes;

    constructor(
        private questionBuilderService: QuestionBuilderService,
        private readonly permissions: Permissions,
    ) { }

    ngOnInit(): void {
        this.questionTypes = QuestionTypes;
        this.displayAnswerAsOptions = this.questionBuilderService.getDisplayAnwerTypes();
        this.additionalAttributes = this.questionBuilderService.getAdditionalAttributes();
        this.setDisplayAnswerAsType();
        this.isInventoryQuestion = this.selectedQuestionType.value === QuestionTypes.Inventory.name;
        this.isResponseConfigurationDisabled = this.templateEditMode === TemplateModes.Edit;
    }

    ngOnChanges(changes: SimpleChanges): void {
        if (changes.selectedQuestionType && !changes.selectedQuestionType.firstChange) {
            this.selectedDisplayAnswerAsType = this.displayAnswerAsOptions[0];
        }
        if (!this.selectedQuestionType.attributes && this.questionAttributes.responseType) {
            this.selectedQuestionType.attributes = this.questionBuilderService.getEnabledAttributesForAttributeQuestion(this.questionAttributes.responseType);
        }

        this.isDisplayAnswerTypeEnabled = this.selectedQuestionType.value === this.questionTypes.MultiChoice.name
            || (this.selectedQuestionType.value === this.questionTypes.Attribute.name
                && this.questionAttributes.responseType !== AttributeQuestionResponseType.TEXT
                && this.questionAttributes.responseType !== AttributeQuestionResponseType.DATE
                && this.notUserOrLocationAttributeType());
    }

    handleDisplayAnswerAs(displayAnswerAsOption: ILabelValue<string>): void {
        this.selectedDisplayAnswerAsType = displayAnswerAsOption;
        this.handleDisplayAnswerAsChange.emit({ displayAnswerAsOption });
    }

    handleToggleStateChange(event: boolean, attributeName: string): void {
        this.handleResponseConfigurationToggleState.emit({ type: attributeName, payload: event });
    }

    isAdditionalAttributesDisabled(attributeValue: string): boolean {
        return (this.templateEditMode === this.templateModes.Edit
            || (this.selectedQuestionType.value !== this.questionTypes.MultiChoice.name
                && this.isSourceInventoryQuestionEnabled()
                && attributeValue === "isMultiSelect"));
    }

    trackByAttributeValue(attributeValue: IAdditionalAttribute): string {
        return attributeValue.value;
    }

    private notUserOrLocationAttributeType(): boolean {
        return !(this.questionAttributes.attributeType === AttributeType.USERS ||
            this.questionAttributes.attributeType === AttributeType.LOCATIONS);
    }

    private setDisplayAnswerAsType(): void {
        if (!this.questionIdExists) {
            this.selectedDisplayAnswerAsType = this.displayAnswerAsOptions[0];
        } else {
            let displayAnswerType: string = this.questionAttributes.displayAnswerType;
            if (!this.questionAttributes.displayAnswerType || this.questionAttributes.displayAnswerType === QuestionAnswerTypes.Button) {
                displayAnswerType = QuestionAnswerTypes.Button;
            }
            this.selectedDisplayAnswerAsType = find(this.displayAnswerAsOptions, { value: displayAnswerType });
        }
    }

    private isSourceInventoryQuestionEnabled(): boolean {
        if (!this.permissions.canShow("InventoryLinkingEnabled")) {
            return this.isInventoryQuestion && this.sourceInventoryQuestion;
        }
        return false;
    }

}
