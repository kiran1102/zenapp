// 3rd Party
import {
    Component,
    OnInit,
    Inject,
    ViewChild,
    ElementRef,
    Renderer2,
} from "@angular/core";
import {
    forEach,
    some,
} from "lodash";

// Enums / Constants
import {
    QuestionConfigurations,
    QuestionTypeNames,
} from "constants/question-types.constant";
import { QuestionTypes } from "constants/question-types.constant";
import { TemplateModes } from "enums/template-modes.enum";
import { QuestionAnswerTypes } from "enums/question-types.enum";

// Redux
import { getModalData } from "oneRedux/reducers/modal.reducer";
import { StoreToken } from "tokens/redux-store.token";
import {
    getCurrentTemplate,
    getTemplateState,
    getQuestionById,
} from "oneRedux/reducers/template.reducer";

// Interfaces
import {
    IQuestionDetails,
    IQuestionOptionDetails,
    IAssessmentTemplateAttribute,
} from "interfaces/assessment-template-question.interface";
import {
    IQuestionTypeList,
    IQuestionType,
} from "interfaces/question-type-attribute.interface";
import { IStore } from "interfaces/redux.interface";
import { ILabelValue } from "interfaces/generic.interface";
import { IIncidentSchema } from "modules/template/interfaces/incident-schema.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";

// Services
import { ModalService } from "sharedServices/modal.service";
import { QuestionBuilderService } from "templateServices/question-builder.service";
import TemplateAction from "templateServices/actions/template-action.service";
import { TemplateBuilderService } from "templateServices/template-builder.service";
import { TemplateBuilderModalHelperService } from "modules/template/services/template-builder-modal-helper.service";

// Templates
import { QuestionBuildComponent } from "modules/template/components/question-builder-modal/question-build/question-build.component";
import { QuestionConfigurationComponent } from "modules/template/components/question-builder-modal/question-configuration/question-configuration.component";

@Component({
    selector: "template-question-builder-modal",
    templateUrl: "./template-question-builder-modal.component.html",
})

export class TemplateQuestionBuilderModalComponent implements OnInit {
    @ViewChild(QuestionBuildComponent)
    questionBuildComponent: QuestionBuildComponent;
    @ViewChild(QuestionConfigurationComponent)
    questionConfigurationComponent: QuestionConfigurationComponent;

    hasQuestionConfiguration = true;
    hasResponseConfiguration = true;
    incidentSchema: IIncidentSchema;
    isQuestionBuildEnabled = true;
    isQuestionPreviewEnabled: boolean;
    question: IQuestionDetails | null;
    questionIdExists: boolean;
    questionTypeList: IQuestionTypeList;
    questionTypeOptions: IQuestionType[] = [];
    saveInProgress = false;
    saveAndAddInProgress = false;
    selectedQuestionType: IQuestionType;
    templateEditMode: TemplateModes;
    selectedSaveAndAddNew = false;

    private nextQuestionId: string | null;
    private sectionId: string;
    @ViewChild("scrollContainer") private scrollContainer: ElementRef;

    constructor(
        @Inject(StoreToken) private store: IStore,
        @Inject("TemplateAction") private templateAction: TemplateAction,
        @Inject("TemplateBuilderService") private templateBuilderService: TemplateBuilderService,
        private readonly modalService: ModalService,
        private renderer: Renderer2,
        private questionBuilderService: QuestionBuilderService,
        private templateBuilderModalHelperService: TemplateBuilderModalHelperService) { }

    ngOnInit(): void {
        this.initialize();
    }

    ngAfterViewInit() {
        this.handleScrollChange();
    }

    initialize(): void {
        const questionsById = getTemplateState(this.store.getState()).builder.questionById;
        const hasInventoryQuestion: boolean = some(questionsById, { questionType: QuestionTypeNames.Inventory });
        const hasIncidentQuestion: boolean = some(questionsById, { questionType: QuestionTypeNames.Incident });

        this.question = this.getQuestion(this.selectedSaveAndAddNew);
        this.hasQuestionConfiguration = this.setQuestionConfiguration(this.question.questionType);
        this.hasResponseConfiguration = this.setResponseConfiguration(this.question.questionType);
        this.questionTypeList = this.questionBuilderService.initializeQuestionTypeList(this.question.attributeJson.responseType, hasInventoryQuestion, hasIncidentQuestion);
        this.questionTypeOptions = this.getQuestionTypeOptions(hasIncidentQuestion);
        this.setRequiredJustificationForAttribute();
        this.selectedQuestionType = this.questionTypeList[this.question.questionType];

        if (this.question.required && this.selectedQuestionType.attributes.allowJustification) {
            this.selectedQuestionType.attributes.requireJustification = true;
        }

        if (this.selectedQuestionType && this.selectedQuestionType.value === QuestionTypeNames.Incident) {
            this.fetchIncidentSchemas();
        }
    }

    closeModal(): void {
        this.modalService.closeModal();
        this.modalService.clearModalData();
    }

    handleScrollChange() {
        this.renderer.listen(this.scrollContainer.nativeElement, "click", () => {
            this.renderer.setProperty(this.scrollContainer.nativeElement, "scrollTop", this.scrollContainer.nativeElement.scrollHeight);
        });
    }

    handleQuestionTypeChanges(questionType: IQuestionType) {
        this.selectedQuestionType = questionType;
        this.hasQuestionConfiguration = this.setQuestionConfiguration(questionType.value);
        this.hasResponseConfiguration = this.setResponseConfiguration(questionType.value);

        if (questionType.value !== this.question.questionType) {
            this.question = this.resetQuestionAttributesOnTypeChange(questionType.value);
        }

        if (questionType.value === QuestionTypeNames.Inventory) {
            this.question.linkAssessmentToInventory = true;
            this.question.prePopulateResponse = false;
            if (!this.questionConfigurationComponent.isInventoryTypesFetched) {
                this.questionConfigurationComponent.fetchInventorySchema();
            }
        }

        if (questionType.value === QuestionTypeNames.Incident) {
            this.fetchIncidentSchemas();
        }
    }

    fetchIncidentSchemas() {
        if (!this.incidentSchema) {
            this.templateAction.fetchIncidentSchemas().then((res: IProtocolResponse<IIncidentSchema[]>) => {
                if (res.result) {
                    this.incidentSchema = res.data[0];
                    this.question.attributeJson.link = this.incidentSchema.uri;
                }
            });
        } else {
            this.question.attributeJson.link = this.incidentSchema.uri;
        }
    }

    onQuestionConfigurationToggle(event: { type: string, payload: boolean }) {
        switch (event.type) {
            case QuestionConfigurations.IS_ANSWER_REQUIRED:
                this.question.required = event.payload;
                this.question = {
                    ...this.question,
                    required: event.payload,
                };
                this.setRequiredJustificationForAttribute();
                break;
            case QuestionConfigurations.IS_QUESTION_HINT_ENABLED:
                if (!event.payload) {
                    this.questionBuildComponent.questionBuildForm.get("questionHint").setValue("");
                }
                this.question = {
                    ...this.question,
                    attributeJson: {
                        ...this.question.attributeJson,
                        isQuestionHintEnabled: event.payload,
                    },
                };
                break;
        }
    }

    onResponseConfigurationToggle(event: { type: string, payload: boolean }) {
        this.question.attributeJson[event.type] = event.payload;
        if (event.type === "optionHintEnabled") {
            forEach(this.question.options, (option: IQuestionOptionDetails): void => {
                option.Hint = "";
                option.attributeJson = { hint: "" };
            });
            this.templateBuilderModalHelperService.updateQuestion(this.question);
        }
        this.setRequiredJustificationForAttribute();
    }

    onDisplayAnswerAsChange(displayAnswerAsOption: { displayAnswerAsOption: ILabelValue<string> }) {
        if (displayAnswerAsOption.displayAnswerAsOption) {
            this.question.attributeJson.displayAnswerType = displayAnswerAsOption.displayAnswerAsOption.value as QuestionAnswerTypes;
        }
    }

    onAttributeQuestionChange(event: { attributeSelection: IAssessmentTemplateAttribute, parentQuestionId: string }) {
        this.question = {
            ...this.question,
            content: event.attributeSelection.name,
            parentQuestionId: event.parentQuestionId,
            attributeJson: {
                ...this.question.attributeJson,
                allowJustification: false,
                allowNotApplicable: false,
                allowNotSure: false,
                attributeId: event.attributeSelection.id,
                attributeType: event.attributeSelection.attributeType,
                displayAnswerType: this.templateBuilderService.initializeDisplayAsDropdown(event.attributeSelection),
                fieldName: event.attributeSelection.fieldName,
                name: event.attributeSelection.name,
                nameKey: event.attributeSelection.nameKey,
                requireJustification: false,
                responseType: event.attributeSelection.responseType,
            },
        };

        if (this.questionBuildComponent.questionBuildForm) {
            this.questionBuildComponent.questionBuildForm.get("question").setValue(this.question.content);
        }
        this.hasResponseConfiguration = this.setResponseConfiguration(this.question.questionType);
    }

    saveQuestion(): void {
        this.saveInProgress = true;
        if (this.question.id) {
            return this.updateQuestion(this.question);
        }
        this.templateAction.createQuestion(this.nextQuestionId, this.sectionId, getCurrentTemplate(this.store.getState()).id, this.question)
            .then((response: boolean): void => {
                this.saveInProgress = false;
                if (response) {
                    this.closeModal();
                }
            });
    }

    saveAndAddNewQuestion(): void {
        this.saveAndAddInProgress = true;
        this.selectedSaveAndAddNew = true;
        if (this.question.id) {
            return this.updateQuestion(this.question);
        }
        this.templateAction.createQuestion(this.nextQuestionId, this.sectionId, getCurrentTemplate(this.store.getState()).id, this.question)
            .then((response: boolean): void => {
                this.saveAndAddInProgress = false;
                if (response) {
                    this.questionBuildComponent.questionBuildForm.reset();
                    this.initialize();
                    this.questionBuildComponent.questionBuildForm.get("description").setValue("");
                    this.selectedSaveAndAddNew = false;
                }
            });
    }

    isSaveQuestionsDisabled(): boolean {
        if (!this.questionBuildComponent) return true;
        return this.questionBuildComponent.questionBuildForm.invalid
            || !this.questionBuildComponent.questionBuildForm.value.question.trim()
            || this.saveInProgress
            || (this.selectedQuestionType.value === QuestionTypeNames.Inventory && !this.question.attributeJson.inventoryType)
            || this.isAttributeTypeNotSelected()
            || this.isMultichoiceOptionsEmpty();
    }

    onBuildPreviewtoggle(): void {
        this.isQuestionBuildEnabled = !this.isQuestionBuildEnabled;
        this.isQuestionPreviewEnabled = !this.isQuestionPreviewEnabled;
    }

    changeQuestion(changedQuestion: IQuestionDetails): void {
        if (changedQuestion.options) {
            this.question.options = [...changedQuestion.options];
        }
    }

    isQuestionTypeDisabled(): boolean {
        return this.templateEditMode === TemplateModes.Edit;
    }

    private setRequiredJustificationForAttribute(): void {
        if (!this.questionTypeList[this.question.questionType] || !this.questionTypeList[this.question.questionType].attributes) return;

        // Enabling justification required option
        this.questionTypeList[this.question.questionType].attributes.requireJustification = this.question.attributeJson.allowJustification
            && this.question.required;

        // Setting requireJustification to false if both, allow justification and required are not set to true.
        if (!this.question.attributeJson.allowJustification || !this.question.required) {
            this.question.attributeJson.requireJustification = false;
        }
    }

    private getQuestionTypeOptions(hasIncidentQuestion: boolean): IQuestionType[] {
        return Object.keys(this.questionTypeList)
            .filter((type) => {
                if (type !== QuestionTypeNames.Incident) return true;
                return !hasIncidentQuestion;
            })
            .map((type) => this.questionTypeList[type]);
    }

    private getQuestion(selectedAddAndNew: boolean): IQuestionDetails {
        const modalData = getModalData(this.store.getState());
        this.sectionId = modalData.sectionId;
        this.nextQuestionId = modalData.nextQuestionId;
        let question: IQuestionDetails = modalData.question;
        if (question && question.id) {
            this.questionIdExists = true;
            // Options
            const options = question.options;
            question.options = options && options.length ? this.questionBuilderService.extractQuestionOptions(question.attributeJson.optionHintEnabled, options) : undefined;
            // In 'Edit' mode restrict disable Type and Option changes
            this.templateEditMode = getTemplateState(this.store.getState()).templateMode;
        } else {
            // setting the question with default values
            question = {
                attributeJson: {
                    isQuestionHintEnabled: false,
                },
                content: null,
                questionType: QuestionTypeNames.TextBox,
                linkAssessmentToInventory: question && question.questionType === QuestionTypeNames.Inventory,
                ...question,
            };

            if (selectedAddAndNew) {
                question.questionType = QuestionTypeNames.TextBox;
            }
        }
        return question;
    }

    private updateQuestion(question: IQuestionDetails): void {
        this.templateAction.updateQuestion(getCurrentTemplate(this.store.getState()).id, this.sectionId, question)
            .then((response: boolean): void => {
                if (response) {
                    this.saveInProgress = false;
                    this.closeModal();
                }
            });
    }

    private resetQuestionAttributesOnTypeChange(selectedQuestionType: string): IQuestionDetails {
        const isStatementType = (this.question.questionType === QuestionTypeNames.Statement || selectedQuestionType === QuestionTypeNames.Statement);
        const question: IQuestionDetails = {
            ...this.question,
            attributeJson: {
                isQuestionHintEnabled: this.selectedQuestionType.value === QuestionTypeNames.Statement ? false : this.question.attributeJson.isQuestionHintEnabled,
            },
            destinationInventoryQuestion: this.question.destinationInventoryQuestion,
            sourceInventoryQuestion: this.question.sourceInventoryQuestion,
            parentQuestionId: selectedQuestionType === QuestionTypeNames.Attribute && this.question.parentQuestionId ? this.question.parentQuestionId : null,
            id: this.question.id || null,
            options: [],
            questionType: selectedQuestionType,
            required: false,
        };
        if (isStatementType) {
            this.questionBuildComponent.questionBuildForm.patchValue({ question: "", friendlyName: "", questionHint: "", description: "" });
        }
        return question;
    }

    private isMultichoiceOptionsEmpty(): boolean {
        let isOptionEmpty: boolean;
        if (this.selectedQuestionType.value === QuestionTypeNames.MultiChoice && this.question.options) {
            forEach(this.question.options, (option: IQuestionOptionDetails): void => {
                if (!option.option || !option.option.trim().length) {
                    isOptionEmpty = true;
                    return;
                }
            });
        }
        return isOptionEmpty;
    }

    private isAttributeTypeNotSelected(): boolean {
        return this.selectedQuestionType.value === QuestionTypeNames.Attribute
            && !(this.question.parentQuestionId && this.question.attributeJson.attributeId);
    }

    private setQuestionConfiguration(type: string): boolean {
        return !(type === QuestionTypeNames.Incident || type === QuestionTypeNames.Statement);
    }

    private setResponseConfiguration(type: string): boolean {
        return (type !== QuestionTypeNames.Statement)
            && (type !== QuestionTypeNames.PersonalData)
            && (type !== QuestionTypeNames.Incident)
            && this.isAttributeTypeAdded(type);
    }

    private isAttributeTypeAdded(type: string): boolean {
        if (type !== QuestionTypeNames.Attribute) return true;
        return this.isIncidentAttribute(this.question.parentQuestionId) ? false : Boolean(this.question.attributeJson.attributeId);
    }

    private isIncidentAttribute(parentId: string): boolean {
        const parent = getQuestionById(parentId)(this.store.getState());
        return parent.questionType === QuestionTypeNames.Incident;
    }

}
