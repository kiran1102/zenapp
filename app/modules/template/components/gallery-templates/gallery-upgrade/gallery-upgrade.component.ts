// Angular
import {
    Component,
    Input,
} from "@angular/core";

// Services
import { McmModalService } from "modules/mcm/mcm-modal.service";

@Component({
    selector: "gallery-upgrade",
    templateUrl: "./gallery-upgrade.component.html",
})

export class GalleryUpgradeComponent {

    @Input() description: string;

    constructor(
        private mcmModalService: McmModalService,
    ) { }

    public upgradeModule(): void {
        this.mcmModalService.openMcmModal("Upgrade");
    }
}
