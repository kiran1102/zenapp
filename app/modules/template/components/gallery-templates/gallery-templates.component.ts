// Angular
import {
    Component,
    Inject,
    OnInit,
} from "@angular/core";
import { StateService } from "@uirouter/core";

// Interfaces
import {
    IGalleryTemplate,
    IGalleryTemplateResponse,
    IGalleryTemplateCardAttributes,
} from "interfaces/gallery-templates.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";
import { IStringMap } from "interfaces/generic.interface";
import { ITemplateCreateModalData } from "interfaces/template-create-modal-data.interface";
import { IStore } from "interfaces/redux.interface";

// Enums
import { TemplateStates } from "enums/template-states.enum";

// Constants
import { TemplateTypes } from "constants/template-types.constant";

// Services
import TemplateAction from "templateServices/actions/template-action.service";
import { TemplateAPI } from "templateServices/api/template-api.service";
import { TemplateService } from "templateServices/templates.service";
import { TemplateCreateHelperService } from "modules/template/services/template-create-helper.service";

// Redux
import { StoreToken } from "tokens/redux-store.token";

// Rxjs
import { Subject } from "rxjs";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

@Component({
    selector: "gallery-templates",
    templateUrl: "./gallery-templates.component.html",
})

export class GalleryTemplatesComponent implements OnInit {

    galleryTemplateDetail: IGalleryTemplateResponse;
    isListLoaded = false;
    loadingGalleryTemplate = false;
    loadingTemplate = false;
    templateStates = TemplateStates;
    templates: IGalleryTemplate[];
    translations: IStringMap<string>;
    selectedTemplate: IGalleryTemplate;
    templateCards: IGalleryTemplateCardAttributes[] = [];
    allTemplates: IGalleryTemplateCardAttributes[];
    resetSearch$ = new Subject();

    private routes: IStringMap<string> = {
        list: "zen.app.pia.module.templatesv2.list",
    };

    private vendorRoutes: IStringMap<string> = {
        galleryStateCheck: "vendor.gallery",
        templatesList: "zen.app.pia.module.vendor.templates",
        gallery: "zen.app.pia.module.vendor.gallery",
    };

    private templateId: string;

    constructor(
        @Inject(StoreToken) private store: IStore,
        @Inject("TemplateAction") private templateAction: TemplateAction,
        @Inject("TemplateAPI") private templateAPI: TemplateAPI,
        private templateService: TemplateService,
        private translatePipe: TranslatePipe,
        private stateService: StateService,
        private templateCreateHelperService: TemplateCreateHelperService,
    ) { }

    ngOnInit(): void {
        this.getGallery();
        this.setConfigurationByTemplateType();
    }

    isRenderedFromVendorModule(): boolean {
        return this.stateService.current.name.search(this.vendorRoutes.galleryStateCheck) > 0;
    }

    collapseGallery(template: IGalleryTemplateCardAttributes): void {
        this.selectedTemplate = {
            id: template.id,
            name: template.header.text,
            icon: template.image.icon,
            status: template.status,
            description: template.content,
        };
        this.getGalleryTemplateByTemplateId(template.id);
    }

    expandGallery(): void {
        this.selectedTemplate = null;
    }

    openCreateTemplateModal(): void {
        const templateData: ITemplateCreateModalData = {
            name: this.selectedTemplate.name,
            description: this.selectedTemplate.description,
            icon: this.selectedTemplate.icon,
            id: this.selectedTemplate.id,
        };
        this.templateCreateHelperService.openCloneGalleryTemplateModal(templateData);
    }

    navigateToTemplateList(path: string): void {
        if (path === this.translations.templateNavUrl) {
            this.stateService.go(this.translations.templateNavUrl);
        }
    }

    trackByTemplateId(index, template) {
        return template.id;
    }

    filterTemplates(searchText: string) {
        this.templateCards = this.allTemplates.filter((template) => {
            return template.name.toLowerCase().includes(searchText.toLowerCase());
        });
    }

    clearResults() {
        this.resetSearch$.next();
        this.filterTemplates("");
    }

    private getTemplateType(): string {
        return this.isRenderedFromVendorModule() ? TemplateTypes.VENDOR : TemplateTypes.PIA;
    }

    private setConfigurationByTemplateType(): void {
        switch (this.getTemplateType()) {
            case TemplateTypes.VENDOR:
                this.translations = {
                    templateHeader: this.translatePipe.transform("VendorTemplates"),
                    selectTemplateToPreview: this.translatePipe.transform("SelectVendorTemplateToPreview"),
                    templateNavUrl: this.vendorRoutes.templatesList,
                };
                break;
            case TemplateTypes.PIA:
            default:
                this.translations = {
                    templateHeader: this.translatePipe.transform("QuestionnaireTemplates"),
                    selectTemplateToPreview: this.translatePipe.transform("SelectQuestionnaireTemplateToPreview"),
                    templateNavUrl: this.routes.list,
                };
                break;
        }
    }

    private getGallery(): void {
        this.templateService.gallery(this.getTemplateType()).then((response: IProtocolResponse<IGalleryTemplate[]>): void => {
            this.templates = ((response.result && Array.isArray(response.data)) ? response.data : []);
            this.templates.forEach((template: IGalleryTemplate) => {
                this.templateCards.push({
                    id: template.id,
                    header: {
                        text: template.name,
                    },
                    image: {
                        icon: template.icon,
                    },
                    locked: template.status === this.templateStates.Locked,
                    button: {
                        text: "Preview",
                    },
                    content: template.description,
                    status: template.status,
                    name: template.name,
                });
            });
            this.allTemplates = [ ...this.templateCards ];
            this.isListLoaded = true;
        });
    }

    private getGalleryTemplateByTemplateId(templateId: string): void {
        if (this.templateId !== templateId) {
            this.templateId = templateId;
            this.loadingGalleryTemplate = true;
            this.templateAPI.getGalleryTemplateByTemplateId(templateId)
                .then((response: IProtocolResponse<IGalleryTemplateResponse>): void => {
                    if (response.result) {
                        this.loadingGalleryTemplate = false;
                        this.galleryTemplateDetail = response.data;
                    }
                });
        }
    }
}
