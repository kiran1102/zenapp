// Angular
import {
    Component,
    Input,
    Inject,
    OnInit,
} from "@angular/core";

// 3rd party
import { forEach } from "lodash";

// Interfaces
import {
    IGalleryTemplateQuestion,
    IQuestionOptionDetails,
    IGalleryTemplateResponse,
} from "interfaces/gallery-templates.interface";
import { IStringMap } from "interfaces/generic.interface";

// Services
import TemplateAction from "templateServices/actions/template-action.service";
import { QuestionTypeService } from "modules/template/services/question-type.service";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

@Component({
    selector: "gallery-template-preview",
    templateUrl: "./gallery-template-preview.component.html",
    styles: ["./gallery-template-preview.component.scss"],
})

export class GalleryTemplatePreviewComponent implements OnInit {

    @Input() galleryTemplateDetail: IGalleryTemplateResponse;

    translations: IStringMap<string>;
    isWelcomeMessageExpanded = true;

    constructor(
        @Inject("TemplateAction") private templateAction: TemplateAction,
        private translatePipe: TranslatePipe,
        private readonly QuestionType: QuestionTypeService,
    ) { }

    ngOnInit(): void {
        this.translations = { templateWelcomMessage: this.translatePipe.transform("TemplateWelcomMessage") };
    }

    trackBySectionId(index, section): number {
        return section.id;
    }

    getQuestionTypeIcon(type: string): string {
        return this.QuestionType.getQuestionTypeIcon(type);
    }

    showQuestionPreview(question: IGalleryTemplateQuestion): void {
        question.isPreviewShown = !question.isPreviewShown;
        question.attributeJson = this.templateAction.parseAttributes(question.attributes);
        if (question.options && question.options.length) {
            forEach(question.options, (option: IQuestionOptionDetails): void => {
                if (question.attributeJson.optionHintEnabled) {
                    option.attributeJson = this.templateAction.parseAttributes(option.attributes);
                }
            });
        }
    }

}
