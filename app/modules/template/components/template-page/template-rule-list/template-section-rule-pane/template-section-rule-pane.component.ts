// Angular
import {
    Component,
    OnInit,
    Inject,
} from "@angular/core";

// Redux
import { StoreToken } from "tokens/redux-store.token";
import { getBuilderView } from "oneRedux/reducers/template.reducer";

// Services
import { QuestionTypeService } from "modules/template/services/question-type.service";

// Interfaces
import { IStore } from "interfaces/redux.interface";
import { IAssessmentTemplateSection } from "interfaces/assessment-template-section.interface";
import { IAssessmentTemplateQuestion } from "interfaces/assessment-template-question.interface";
import { IStringMap } from "interfaces/generic.interface";

@Component({
    selector: "template-section-rule-pane",
    templateUrl: "template-section-rule-pane.component.html",
})
export class TemplateSectionRulePaneComponent implements OnInit {
    questionByIds: IStringMap<IAssessmentTemplateQuestion>;
    sectionQuestionIdsMap: IStringMap<string[]>;
    sectionById: IStringMap<IAssessmentTemplateSection>;
    sectionAllId: string[];
    questionTypeIcon: string;
    sectionExpanded: IStringMap<any>;

    constructor(
        @Inject(StoreToken) private store: IStore,
        private questionTypeService: QuestionTypeService,
    ) { }

    ngOnInit(): void {
        const builderState = getBuilderView(this.store.getState());
        this.questionByIds = builderState.questionById;
        this.sectionQuestionIdsMap = builderState.sectionIdQuestionIdsMap;
        this.sectionById = builderState.sectionById;
        this.sectionAllId = builderState.sectionAllIds;
        this.sectionExpanded = Object.assign({}, ...this.sectionAllId.map((sectionId: string) => ({ [sectionId]: true })));
    }

    trackBySectionId(index: number, sectionId: string): string {
        return sectionId;
    }

    trackByQuestionId(index: number, questionId: string): string {
        return questionId;
    }

    getQuestionTypeIcon(type: string): string {
        return this.questionTypeService.getQuestionTypeIcon(type);
    }
}
