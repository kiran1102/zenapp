//#region IMPORTS
// Angular
import {
    Component,
    OnInit,
    Input,
    Inject,
    OnDestroy,
    Output,
    EventEmitter,
} from "@angular/core";

// Rxjs
import { Subject } from "rxjs";
import {
    startWith,
    first,
    takeUntil,
} from "rxjs/operators";

// External Libraries
import {
    map,
    isEmpty,
} from "lodash";

// Redux
import {
    IStore,
    IStoreState,
} from "interfaces/redux.interface";
import { StoreToken } from "tokens/redux-store.token";
import {
    TemplateActions,
    getTempNavigationById,
    getNavigationById,
    getTemplateActionRuleTempModel,
    getTemplateActionRuleById,
    getCurrentTemplate,
} from "oneRedux/reducers/template.reducer";
import {
    getCurrentOrgId,
    getOrgById,
} from "oneRedux/reducers/org.reducer";
import observableFromStore from "oneRedux/utils/observable-from-store";

// Services
import { TemplateRuleService } from "./template-rule.service";
import { OrgGroupApiService } from "oneServices/api/org-group-api.service";
import { Permissions } from "modules/shared/services/helper/permissions.service";

// Interfaces
import {
    IAssessmentNavigation,
    IAssessmentNavigationCondition,
} from "interfaces/assessment/assessment-navigation.interface";
import { IAssessmentTemplateSection } from "interfaces/assessment-template-section.interface";
import {
    IAssessmentTemplateQuestion,
    IQuestionOptionDetails,
} from "interfaces/assessment-template-question.interface";
import {
    IStringMap,
    ILabelValue,
} from "interfaces/generic.interface";
import {
    ITemplateRule,
    ITemplateRuleConditionGroup,
    IQuestionParameter,
} from "./template-rule.interface";
import { IOrganization } from "interfaces/organization.interface";
import { IOrgUserAdapted } from "interfaces/org-user.interface";
import {
    IAddApproverParameters,
} from "modules/template/components/template-page/template-rule-list/template-rule/template-rule.interface";
import { IAssessmentTemplateDetail } from "interfaces/assessment-template-detail.interface";

// Enums
import {
    TemplateRuleActions,
    TemplateRuleModes,
} from "modules/template/enums/template-rule.enums";
import {
    LogicalOperators,
    ComparisonOperators,
} from "enums/operators.enum";
import { OrgUserTraversal } from "enums/org-user-dropdown.enum";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

//#endregion IMPORTS

@Component({
    selector: "template-rule",
    templateUrl: "template-rule.component.html",
})

export class TemplateRuleComponent implements OnInit, OnDestroy {

    @Input() allowedActions: TemplateRuleActions[];
    @Input() actionId: string;
    @Input("mode") mode: TemplateRuleModes;
    @Input("editAction") showEditAction = false;
    @Input("deleteAction") showDeleteAction = false;

    @Output() conditionDeleted = new EventEmitter<string>();
    @Output() conditionEditAction = new EventEmitter<string>();

    actionPickList: Array<ILabelValue<string>>;
    actionPickListDisabled: boolean;
    actionRule: ITemplateRule;
    actionSelected: ILabelValue<string>;
    actionTypes: IStringMap<ILabelValue<string>>;
    approverList: IOrgUserAdapted[] = [];
    isApproversLoading = false;
    canAddCondition: boolean;
    comparisonOperatorPickList: Array<ILabelValue<ComparisonOperators>>;
    comparisonOperatorSelected: Array<ILabelValue<ComparisonOperators>>;
    conditions: ITemplateRuleConditionGroup[] | IAssessmentNavigationCondition[];
    isPreviewMode: boolean;
    isTemplateActive: boolean;
    isTemplateLocked: boolean;
    logicalOperatorPickList: Array<ILabelValue<LogicalOperators>>;
    logicalOperatorSelected: ILabelValue<LogicalOperators>;
    nextQuestionsPickList: IAssessmentTemplateQuestion[];
    options: IQuestionOptionDetails[][];
    optionSelected: IQuestionOptionDetails[];
    selectedOrg: IOrganization;
    selectedQuestion: IAssessmentTemplateQuestion;
    selectedSection: IAssessmentTemplateSection;
    skipCondition: IAssessmentNavigation;
    typePickList: Array<ILabelValue<string>> | IQuestionParameter[];
    typePickListDisabled: boolean;
    typeSelected: Array<ILabelValue<string>> | IQuestionParameter[];
    allApprovers: IOrgUserAdapted[];
    approversForOrgQuestionPickList = [{ label: this.translatePipe.transform("Organization"), value: this.translatePipe.transform("Organization").toLowerCase() }];

    private destroy$ = new Subject();

    constructor(
        @Inject(StoreToken) private store: IStore,
        private translatePipe: TranslatePipe,
        private templateRule: TemplateRuleService,
        private permissions: Permissions,
        private readonly OrgGroups: OrgGroupApiService,
    ) { }

    ngOnInit(): void {
        this.isPreviewMode = (this.mode === TemplateRuleModes.PREVIEW);
        this.actionPickListDisabled = (this.mode !== TemplateRuleModes.CREATE || this.allowedActions.length === 1);
        this.actionTypes = this.templateRule.action;
        if (this.actionId) {
            this.actionRule = getTemplateActionRuleById(this.actionId)(this.store.getState());
        }

        this.logicalOperatorPickList = [this.templateRule.logicalOperator.or, this.templateRule.logicalOperator.and];
        this.selectedOrg = {} as IOrganization;

        if (this.actionRule && this.actionRule.action) {
            this.onActionSelected(this.actionRule.action);
        } else {
            this.onActionSelected(this.allowedActions[0]);
        }

        switch (this.actionSelected.value) {
            case TemplateRuleActions.ASSESSMENT_CREATION_ON_SUBMIT:
            case TemplateRuleActions.ASSESSMENT_CREATION_ON_APPROVAL:
            case TemplateRuleActions.RISK_CREATION:
            case TemplateRuleActions.ADD_APPROVER_ON_SUBMISSION:
            case TemplateRuleActions.ADD_APPROVERS_FOR_ORG_ON_LAUNCH:
                this.actionPickList = [
                    this.templateRule.action.riskCreation,
                    this.templateRule.action.sendAssessmentOnSubmit,
                    this.templateRule.action.sendAssessmentOnApprove,
                    this.templateRule.action.addApproverOnSubmit,
                    this.templateRule.action.addApproverByOrgOnLaunch,
                ];
                break;
            case TemplateRuleActions.SKIP:
                this.actionPickList = [this.templateRule.action.skip];
                break;
        }

        observableFromStore(this.store).pipe(
            startWith(this.store.getState()),
            takeUntil(this.destroy$),
        ).subscribe((state: IStoreState) =>
            this.isPreviewMode
                ? this.componentWillReceiveStatePreview(state)
                : this.componentWillReceiveStateEdit(state));
    }

    ngOnDestroy(): void {
        this.destroy$.next();
        this.destroy$.complete();
    }

    initialize(): void {
        this.conditions = this.getConditions();
        this.initializeLogicalOperator();
        this.initializeTypeSelector();
        this.initializeComparisonOperator();
        this.initializeOptions();
    }

    onActionSelected(action: string | ILabelValue<TemplateRuleActions>, create?: boolean): void {
        if (this.shouldPreventAction(action)) return;

        if (create) {
            this.templateRule.createTempActionRule((action as ILabelValue<TemplateRuleActions>).value);
        }
        this.canAddCondition = true;
        switch ((action as ILabelValue<TemplateRuleActions>).value || action) {
            case TemplateRuleActions.SKIP:
                this.initializeSkipCondition();
                break;
            case TemplateRuleActions.ASSESSMENT_CREATION_ON_SUBMIT:
                this.initializeActionRule();
                break;
            case TemplateRuleActions.ASSESSMENT_CREATION_ON_APPROVAL:
                this.initializeSendAssessmentUponApproveRule();
                break;
            case TemplateRuleActions.RISK_CREATION:
                this.initializeRiskCreation();
                break;
            case TemplateRuleActions.ADD_APPROVER_ON_SUBMISSION:
                this.initializeAddApproverOnSubmission();
                break;
            case TemplateRuleActions.ADD_APPROVERS_FOR_ORG_ON_LAUNCH:
                this.initializeAddApproverByOrgOnLaunch();
                break;
        }
        this.initialize();
    }

    addCondition(index: number): void {
        switch (this.actionSelected.value) {
            case TemplateRuleActions.SKIP:
                (this.typeSelected as any[]).splice(index + 1, 0, this.typePickList[0]);
                this.options.splice(index + 1, 0, this.options[index]);
                this.templateRule.addSkipCondition(index, this.logicalOperatorSelected.value, this.actionId);
                break;
            case TemplateRuleActions.ASSESSMENT_CREATION_ON_SUBMIT:
            case TemplateRuleActions.ASSESSMENT_CREATION_ON_APPROVAL:
            case TemplateRuleActions.ADD_APPROVER_ON_SUBMISSION:
            case TemplateRuleActions.ADD_APPROVERS_FOR_ORG_ON_LAUNCH:
                (this.typeSelected as any[]).splice(index + 1, 0, null);
                this.options.splice(index + 1, 0, null);
                this.templateRule.addRuleCondition(index, this.logicalOperatorSelected.value);
                break;
            case TemplateRuleActions.RISK_CREATION:
                (this.typeSelected as any[]).splice(index + 1, 0, this.typeSelected[0]);
                this.options.splice(index + 1, 0, null);
                this.templateRule.addRuleCondition(index, this.logicalOperatorSelected.value);
                this.typePickListDisabled = this.typeSelected.length > 1;
                this.onQuestionSelected(this.typeSelected[0] as any, index + 1);
                break;
        }
        this.comparisonOperatorSelected.splice(index + 1, 0, this.comparisonOperatorPickList[0]);
        this.optionSelected.splice(index + 1, 0, null);
    }

    removeCondition(index: number): void {
        this.comparisonOperatorSelected.splice(index, 1);
        this.typeSelected.splice(index, 1);
        this.optionSelected.splice(index, 1);
        this.options.splice(index, 1);
        switch (this.actionSelected.value) {
            case TemplateRuleActions.SKIP:
                this.templateRule.removeSkipCondition(index, this.actionId);
                break;
            case TemplateRuleActions.ASSESSMENT_CREATION_ON_SUBMIT:
            case TemplateRuleActions.ASSESSMENT_CREATION_ON_APPROVAL:
            case TemplateRuleActions.ADD_APPROVER_ON_SUBMISSION:
                this.templateRule.removeRuleCondition(index);
                break;
            case TemplateRuleActions.RISK_CREATION:
                this.templateRule.removeRuleCondition(index);
                this.typePickListDisabled = this.typeSelected.length > 1;
                break;
        }
    }

    onEditCondition(): void {
        this.conditionEditAction.emit();
    }

    onConditionDeleted(): void {
        this.conditionDeleted.emit(this.actionId);
    }

    onComparisonOperatorSelected(operator: ILabelValue<ComparisonOperators>, index: number): void {
        this.comparisonOperatorSelected[index] = operator;
        switch (this.actionSelected.value) {
            case TemplateRuleActions.SKIP:
                this.templateRule.updateComparisonOperatorSkipAction(operator, index, this.actionId);
                break;
            case TemplateRuleActions.ASSESSMENT_CREATION_ON_SUBMIT:
            case TemplateRuleActions.ASSESSMENT_CREATION_ON_APPROVAL:
            case TemplateRuleActions.ADD_APPROVER_ON_SUBMISSION:
            case TemplateRuleActions.RISK_CREATION:
                this.templateRule.updateComparisonOperatorRuleAction(operator, index);
                break;
        }
    }

    onLogicalOperatorSelected(operator: ILabelValue<LogicalOperators>): void {
        switch (this.actionSelected.value) {
            case TemplateRuleActions.SKIP:
                this.templateRule.updateLogicalOperatorSkipAction(operator, this.actionId);
                break;
            case TemplateRuleActions.ASSESSMENT_CREATION_ON_SUBMIT:
            case TemplateRuleActions.ASSESSMENT_CREATION_ON_APPROVAL:
            case TemplateRuleActions.ADD_APPROVER_ON_SUBMISSION:
            case TemplateRuleActions.RISK_CREATION:
                this.templateRule.updateLogicalOperatorRuleAction(operator);
                break;
        }
        this.logicalOperatorSelected = operator;
    }

    onOptionSelected(option: IQuestionOptionDetails, index: number): void {
        switch (this.actionSelected.value) {
            case TemplateRuleActions.SKIP:
                this.templateRule.updateSkipConditionOption(option, index, this.actionId);
                break;
            case TemplateRuleActions.ASSESSMENT_CREATION_ON_SUBMIT:
            case TemplateRuleActions.ASSESSMENT_CREATION_ON_APPROVAL:
            case TemplateRuleActions.ADD_APPROVER_ON_SUBMISSION:
            case TemplateRuleActions.RISK_CREATION:
                this.templateRule.updateRuleConditionOption(option, index);
                break;
        }
        this.optionSelected[index] = option;
    }

    onOrgOptionSelected(orgId: string, index: number): void {
        this.approverList = [];
        this.actionRule.actionParameters = null;
        this.selectedOrg.id = orgId;
        this.selectedOrg.name = this.selectedOrg.id ? getOrgById(orgId)(this.store.getState()).Name : "";

        const option: IQuestionOptionDetails = {
            orgGroupId: this.selectedOrg.id,
            option: null,
            optionType: null,
        };

        switch (this.actionSelected.value) {
            case TemplateRuleActions.ADD_APPROVERS_FOR_ORG_ON_LAUNCH:
                this.templateRule.updateRuleConditionOption(option, index);
                break;
        }
        this.optionSelected[index] = option;
    }

    onQuestionSelected(question: IQuestionParameter, index: number): void {
        this.typeSelected[index] = question;
        this.optionSelected.splice(index, 1);
        this.templateRule.getOptions(question.value, question.sectionId).pipe(
            first(),
        ).subscribe((options: IQuestionOptionDetails[]) => this.options[index] = options);
        this.templateRule.updateQuestionActionRule(question, index);
        this.canAddCondition = true;
    }

    getConditions(): IAssessmentNavigationCondition[] | ITemplateRuleConditionGroup[] {
        switch (this.actionSelected.value) {
            case TemplateRuleActions.SKIP:
                return this.skipCondition.conditions;
            case TemplateRuleActions.ASSESSMENT_CREATION_ON_SUBMIT:
            case TemplateRuleActions.ASSESSMENT_CREATION_ON_APPROVAL:
            case TemplateRuleActions.RISK_CREATION:
            case TemplateRuleActions.ADD_APPROVER_ON_SUBMISSION:
            case TemplateRuleActions.ADD_APPROVERS_FOR_ORG_ON_LAUNCH:
                return this.actionRule.conditionGroups;
        }
    }

    shouldPreventAction(action: string | ILabelValue<TemplateRuleActions>): boolean {
        if (!this.actionSelected) return false;
        if ((typeof action === "string") && (this.actionSelected.value === action)) return true;
        if ((typeof action === "object") && (this.actionSelected.value === (action as ILabelValue<TemplateRuleActions>).value)) return true;
        return false;
    }

    onApproverSelected(approver: IOrgUserAdapted[]): void {
        this.templateRule.updateRuleApproverList(map(approver, "Id"));
    }

    private componentWillReceiveStateEdit(state: IStoreState): void {
        switch (this.actionSelected.value) {
            case TemplateRuleActions.SKIP:
                this.skipCondition = getTempNavigationById(this.actionId)(state);
                this.conditions = this.getConditions();
                break;
            case TemplateRuleActions.ASSESSMENT_CREATION_ON_SUBMIT:
            case TemplateRuleActions.ASSESSMENT_CREATION_ON_APPROVAL:
            case TemplateRuleActions.ADD_APPROVER_ON_SUBMISSION:
            case TemplateRuleActions.RISK_CREATION:
            case TemplateRuleActions.ADD_APPROVERS_FOR_ORG_ON_LAUNCH:
                this.actionRule = getTemplateActionRuleTempModel(state);
                this.conditions = this.getConditions();
                break;
        }

        this.updateTemplateMetaData(state);
    }

    private componentWillReceiveStatePreview(state: IStoreState): void {
        switch (this.actionSelected.value) {
            case TemplateRuleActions.ASSESSMENT_CREATION_ON_SUBMIT:
            case TemplateRuleActions.ASSESSMENT_CREATION_ON_APPROVAL:
            case TemplateRuleActions.ADD_APPROVER_ON_SUBMISSION:
            case TemplateRuleActions.RISK_CREATION:
            case TemplateRuleActions.ADD_APPROVERS_FOR_ORG_ON_LAUNCH:
                this.actionRule = getTemplateActionRuleById(this.actionId)(state);
                if (!this.actionRule || !this.actionRule.id) {
                    return;
                }
                this.conditions = this.getConditions();
                this.resetActionRuleSelections();
                break;
        }

        this.updateTemplateMetaData(state);
    }

    private updateTemplateMetaData(state: IStoreState) {
        const template: IAssessmentTemplateDetail = getCurrentTemplate(state);
        this.isTemplateLocked = template.isLocked;
        this.isTemplateActive = template.isActive;
    }

    private initializeSkipCondition(): void {
        this.skipCondition = getNavigationById(this.actionId)(this.store.getState());
        if (isEmpty(this.skipCondition.id)) {
            this.skipCondition = getTempNavigationById(this.actionId)(this.store.getState());
        }
        this.store.dispatch({ type: TemplateActions.UPDATE_TEMP_NAVIGATION, navigation: { ...this.skipCondition } });
        this.actionSelected = this.templateRule.action.skip;
        this.typePickListDisabled = true;
    }

    private initializeActionRule(): void {
        this.actionRule = this.isPreviewMode
            ? getTemplateActionRuleById(this.actionId)(this.store.getState())
            : getTemplateActionRuleTempModel(this.store.getState());
        this.actionSelected = this.templateRule.action.sendAssessmentOnSubmit;
    }

    private initializeSendAssessmentUponApproveRule(): void {
        this.actionRule = this.isPreviewMode
            ? getTemplateActionRuleById(this.actionId)(this.store.getState())
            : getTemplateActionRuleTempModel(this.store.getState());
        this.actionSelected = this.templateRule.action.sendAssessmentOnApprove;
    }

    private initializeRiskCreation(): void {
        this.actionSelected = this.templateRule.action.riskCreation;
        this.canAddCondition = this.mode === TemplateRuleModes.EDIT;
    }

    private initializeAddApproverOnSubmission(): void {
        this.actionSelected = this.templateRule.action.addApproverOnSubmit;
        this.canAddCondition = this.mode === TemplateRuleModes.EDIT;
    }

    private initializeAddApproverByOrgOnLaunch() {
        this.actionSelected = this.templateRule.action.addApproverByOrgOnLaunch;
        this.canAddCondition = false;
        if (this.actionRule.conditionGroups && this.actionRule.conditionGroups.length) {
            this.selectedOrg.id = this.actionRule.conditionGroups[0].conditions[0].orgGroupId || null;
            this.selectedOrg.name = this.selectedOrg.id ? getOrgById(this.selectedOrg.id)(this.store.getState()).Name : "";
        }

        this.isApproversLoading = true;
        this.OrgGroups.getOrgUsersWithPermission(
            getCurrentOrgId(this.store.getState()),
            OrgUserTraversal.All,
            null,
            ["AssessmentCanBeApprover"],
            [],
        ).then((response) => {
            if (response.result) {
                this.allApprovers = response.data;
                this.approverList = this.getApproverList(this.actionRule);
                this.isApproversLoading = false;
            }
        });

    }

    private initializeComparisonOperator(): void {
        this.comparisonOperatorPickList = [this.templateRule.comparisonOperator.equal, this.templateRule.comparisonOperator.notEqual];
        this.comparisonOperatorSelected = [];
        switch (this.actionSelected.value) {
            case TemplateRuleActions.SKIP:
                this.skipCondition.conditions.forEach((condition: IAssessmentNavigationCondition, index: number) => {
                    switch (condition.comparisonOperator) {
                        case ComparisonOperators.EQUAL_TO:
                            this.comparisonOperatorSelected[index] = this.templateRule.comparisonOperator.equal;
                            break;
                        case ComparisonOperators.NOT_EQUAL_TO:
                            this.comparisonOperatorSelected[index] = this.templateRule.comparisonOperator.notEqual;
                    }
                });
                break;
            case TemplateRuleActions.ASSESSMENT_CREATION_ON_SUBMIT:
            case TemplateRuleActions.ASSESSMENT_CREATION_ON_APPROVAL:
            case TemplateRuleActions.ADD_APPROVERS_FOR_ORG_ON_LAUNCH:
            case TemplateRuleActions.ADD_APPROVER_ON_SUBMISSION:
            case TemplateRuleActions.RISK_CREATION:
                this.actionRule.conditionGroups.forEach((condition: ITemplateRuleConditionGroup, index: number) => {
                    switch (condition.conditions[0].comparisonOperator) {
                        case ComparisonOperators.EQUAL_TO:
                            this.comparisonOperatorSelected[index] = this.templateRule.comparisonOperator.equal;
                            break;
                        case ComparisonOperators.NOT_EQUAL_TO:
                            this.comparisonOperatorSelected[index] = this.templateRule.comparisonOperator.notEqual;
                    }
                });
                break;
        }
    }

    private initializeLogicalOperator(): void {
        if (this.conditions && this.conditions.length) {
            switch (this.conditions[0].logicalOperator) {
                case LogicalOperators.OR:
                    this.logicalOperatorSelected = this.templateRule.logicalOperator.or;
                    break;
                case LogicalOperators.AND:
                    this.logicalOperatorSelected = this.templateRule.logicalOperator.and;
                    break;
                default:
                    this.logicalOperatorSelected = this.templateRule.logicalOperator.or;
            }
        } else {
            this.logicalOperatorSelected = this.templateRule.logicalOperator.or;
        }
    }

    private initializeOptions(): void {
        this.optionSelected = [];
        this.options = [];
        switch (this.actionSelected.value) {
            case TemplateRuleActions.SKIP:
                this.templateRule.getOptions(this.skipCondition.sourceQuestionId, this.skipCondition.sourceSectionId).pipe(
                    first(),
                ).subscribe((options: IQuestionOptionDetails[]) => {
                    this.skipCondition.conditions.forEach((condition: IAssessmentNavigationCondition, index: number) => {
                        this.optionSelected[index] = options.find((option: IQuestionOptionDetails) => option.id === condition.optionId);
                        this.options[index] = options;
                    });
                });
                break;
            case TemplateRuleActions.ASSESSMENT_CREATION_ON_SUBMIT:
            case TemplateRuleActions.ASSESSMENT_CREATION_ON_APPROVAL:
            case TemplateRuleActions.ADD_APPROVER_ON_SUBMISSION:
            case TemplateRuleActions.RISK_CREATION:
                this.actionRule.conditionGroups.forEach((condition: ITemplateRuleConditionGroup, index: number) => {
                    if (condition.questionId) {
                        this.templateRule.getOptions(condition.questionId, condition.sectionId).pipe(
                            first(),
                        ).subscribe((options: IQuestionOptionDetails[]) => {
                            this.optionSelected[index] = options.find((option: IQuestionOptionDetails) => option.id === condition.conditions[0].optionId);
                            this.options[index] = options;
                        });
                    }
                });
                break;
            case TemplateRuleActions.ADD_APPROVERS_FOR_ORG_ON_LAUNCH:
                this.approverList = this.getApproverList(this.actionRule);
                break;
        }
    }

    private getApproverList(actionRule: ITemplateRule) {
        if (actionRule && actionRule.actionParameters) {
            const approverIds = (actionRule.actionParameters as IAddApproverParameters).approverIds;
            const approverList = [];
            if (this.allApprovers && approverIds) {
                this.allApprovers.forEach((approver) => {
                    approverIds.forEach((approverId) => {
                        if (approver.Id === approverId) {
                            approverList.push(approver);
                        }
                    });
                });
            }
            return [...approverList];
        }
    }

    private initializeTypeSelector(): void {
        this.typeSelected = [];
        this.typePickListDisabled = false;
        switch (this.actionSelected.value) {
            case TemplateRuleActions.SKIP:
                this.typePickList = [this.templateRule.type.response];
                this.typePickListDisabled = true;
                this.skipCondition.conditions.forEach((condition: IAssessmentNavigationCondition, index: number) => {
                    this.typeSelected[index] = this.templateRule.type.response;
                });
                break;
            case TemplateRuleActions.ASSESSMENT_CREATION_ON_SUBMIT:
            case TemplateRuleActions.ASSESSMENT_CREATION_ON_APPROVAL:
            case TemplateRuleActions.ADD_APPROVER_ON_SUBMISSION:
                this.typePickList = this.templateRule.getQuestionList(true);
                this.actionRule.conditionGroups.forEach((condition: ITemplateRuleConditionGroup, index: number) => {
                    this.typeSelected[index] = (this.typePickList as IQuestionParameter[]).find((question: IQuestionParameter) => question.value === condition.questionId);
                });
                break;
            case TemplateRuleActions.ADD_APPROVERS_FOR_ORG_ON_LAUNCH:
                this.typePickList = this.approversForOrgQuestionPickList;
                this.typePickListDisabled = true;
                this.actionRule.conditionGroups.forEach((condition: ITemplateRuleConditionGroup, index: number) => {
                    this.typeSelected[index] = this.approversForOrgQuestionPickList[index];
                });
                break;
            case TemplateRuleActions.RISK_CREATION:
                this.typePickList = this.templateRule.getQuestionList(true);
                this.actionRule.conditionGroups.forEach((condition: ITemplateRuleConditionGroup, index: number) => {
                    this.typeSelected[index] = (this.typePickList as IQuestionParameter[]).find((question: IQuestionParameter) => question.value === condition.questionId);
                });
                this.typePickListDisabled = this.typeSelected.length > 1;
                break;
        }
    }

    private resetActionRuleSelections(): void {
        this.actionRule.conditionGroups.forEach((group: ITemplateRuleConditionGroup, index: number) => {
            this.selectedOrg.id = group.conditions[0].orgGroupId;
            this.selectedOrg.name = this.selectedOrg.id ? getOrgById(this.selectedOrg.id)(this.store.getState()).Name : "";

            switch (this.actionSelected.value) {
                case TemplateRuleActions.ADD_APPROVERS_FOR_ORG_ON_LAUNCH:
                    group["orgGroupId"] = group.conditions[0].orgGroupId || "";
                    this.approverList = this.getApproverList(this.actionRule);
                    this.actionRule.conditionGroups.forEach((condition: ITemplateRuleConditionGroup) => {
                        this.typeSelected[index] = this.approversForOrgQuestionPickList[index];
                    });
                    break;
                default:
                    this.typeSelected[index] = (this.typePickList as IQuestionParameter[]).find((question: IQuestionParameter) => question.value === group.questionId);
                    break;
            }
            if (this.actionSelected.value !== TemplateRuleActions.ADD_APPROVERS_FOR_ORG_ON_LAUNCH) {
                this.templateRule.getOptions(group.questionId, group.sectionId).pipe(
                    first(),
                ).subscribe((options: IQuestionOptionDetails[]) => {
                    this.options[index] = options;
                    this.optionSelected[index] = this.options[index].find((option: IQuestionOptionDetails) => option.id === group.conditions[0].optionId);
                });
            }
            if (group.conditions && group.conditions.length) {
                switch (group.conditions[0].comparisonOperator) {
                    case ComparisonOperators.EQUAL_TO:
                        this.comparisonOperatorSelected[index] = this.templateRule.comparisonOperator.equal;
                        break;
                    case ComparisonOperators.NOT_EQUAL_TO:
                        this.comparisonOperatorSelected[index] = this.templateRule.comparisonOperator.notEqual;
                }
            }
        });
        if (this.actionRule.conditionGroups.length && this.actionRule.conditionGroups[0].logicalOperator) {
            switch (this.actionRule.conditionGroups[0].logicalOperator) {
                case LogicalOperators.OR:
                    this.logicalOperatorSelected = this.templateRule.logicalOperator.or;
                    break;
                case LogicalOperators.AND:
                    this.logicalOperatorSelected = this.templateRule.logicalOperator.and;
                    break;
                default:
                    this.logicalOperatorSelected = this.templateRule.logicalOperator.or;
            }
        }
    }
}
