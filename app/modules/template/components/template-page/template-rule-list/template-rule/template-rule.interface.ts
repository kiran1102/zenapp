// Interfaces
import { IAssessmentTemplateSection } from "interfaces/assessment-template-section.interface";
import { IAssessmentTemplateQuestion } from "interfaces/assessment-template-question.interface";
import { IOrgUserAdapted } from "interfaces/org-user.interface";

// Enums
import {
    TemplateRuleApproverOptions,
    TemplateRuleActions,
    TemplateRuleRespondentOptions,
} from "modules/template/enums/template-rule.enums";

export interface ITemplateRule {
    id: string;
    conditionGroups: ITemplateRuleConditionGroup[];
    sequence: number;
    action: TemplateRuleActions;
    actionParameters: ICreateAssessmentParameters | ICreateRiskParameters | IAddApproverParameters | IAddApproverByOrgParameters;
    // UI fields
    invalid?: boolean;
    errorMessage?: string;
}

export interface ITemplateRuleConditionGroup {
    sectionId?: string;
    questionId?: string;
    conditions?: ITemplateRuleCondition[] | ITemplateRuleApproverByOrgCondition[];
    orgGroupId?: string;
    logicalOperator?: "OR" | "AND";
}

export interface ITemplateRuleApproverByOrgConditionGroup {
    orgGroupId: string;
    conditions: ITemplateRuleCondition[];
    logicalOperator: "OR" | "AND";
}

export interface ITemplateRuleApproverByOrgCondition {
    logicalOperator: "OR" | "AND";
    comparisonOperator: "EQUAL_TO" | "NOT_EQUAL_TO";
    optionId: string | null;
    optionType: string | null;
    orgGroupId: string;
}

export interface ITemplateRuleCondition {
    logicalOperator: "OR" | "AND";
    comparisonOperator: "EQUAL_TO" | "NOT_EQUAL_TO";
    optionId: string | null;
    optionType: string | null;
    orgGroupId: string;
}

export interface ISkipConditionParameters {
    nextSections?: IAssessmentTemplateSection[];
    nextQuestions?: IAssessmentTemplateQuestion[];
    selectedSection?: IAssessmentTemplateSection;
    selectedQuestion?: IAssessmentTemplateQuestion;
}

export interface ICreateAssessmentParameters {
    templateRootVersionId: string;
    approverSource: TemplateRuleApproverOptions;
    respondentSource: TemplateRuleRespondentOptions;
    keepOpen?: boolean;
    needsConfirmation?: boolean;
}

export interface IAddApproverParameters {
    templateRootVersionId: string;
    approverIds: string[];
}

export interface IAddApproverByOrgParameters {
    orgId: string;
    approversByOrg: IOrgUserAdapted[];
}

export interface ICreateRiskParameters {
    level: number;
    description: string;
    recommendation: string;
    impactLevel: string;
    impactLevelId: number;
    probabilityLevel: string;
    probabilityLevelId: number;
    riskScore: number;
}

export interface IQuestionParameter {
    value: string;
    label: string;
    sectionId: string;

}
