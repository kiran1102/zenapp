//#region IMPORTS
// Rxjs
import { Observable, of, NEVER, from } from "rxjs";
import {
    map,
    switchMap,
} from "rxjs/operators";

// Angular
import {
    Injectable,
    Inject,
} from "@angular/core";
import { IPromise } from "angular";

// 3rd Party
import {
    isNil,
    omitBy,
    omit,
} from "lodash";

// External Libraries
import {
    isEmpty,
    isArray,
    isBoolean,
    uniq,
    each,
    intersection,
} from "lodash";

// Redux
import observableFromStore from "oneRedux/utils/observable-from-store";
import { StoreToken } from "tokens/redux-store.token";
import {
    getTempNavigationById,
    getBuilderView,
    getSections,
    getQuestionById,
    getSectionById,
    TemplateActions,
    getCurrentTemplate,
    getTemplateActionRuleTempModel,
    getPublishedTemplates,
    isFetchingPublishedTemplates,
    getTemplateRuleNextSequence,
    getTemplateActionRuleById,
    getTemplateRuleCombinations,
    getSkipConditionCombinations,
    getTempNavigationsByQuestion,
} from "oneRedux/reducers/template.reducer";

// Interfaces
import {
    IStore,
    IStoreState,
} from "interfaces/redux.interface";
import {
    IAssessmentNavigation,
    IAssessmentNavigationCondition,
} from "interfaces/assessment/assessment-navigation.interface";
import {
    IStringMap,
    ILabelValue,
} from "interfaces/generic.interface";
import { IAssessmentTemplateSection } from "interfaces/assessment-template-section.interface";
import {
    IAssessmentTemplateQuestion,
    IQuestionOptionDetails,
} from "interfaces/assessment-template-question.interface";
import {
    ISkipConditionParameters,
    IQuestionParameter,
    ITemplateRule,
    ITemplateRuleConditionGroup,
    ITemplateRuleCondition,
    ICreateAssessmentParameters,
    ICreateRiskParameters,
    IAddApproverParameters,
} from "./template-rule.interface";
import { ITemplateNameValue } from "modules/template/interfaces/template.interface";
import { ITemplateSkipConditionCombination } from "interfaces/template-skip-condition-combination.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";
import { ICustomTemplateDetails } from "modules/template/interfaces/custom-templates.interface";
import { IAddApproverByOrgParameters } from "./template-rule.interface";

// Constants
import {
    QuestionTypes,
    QuestionResponseType,
    AttributeType,
} from "constants/question-types.constant";
import { TemplateTypes } from "constants/template-types.constant";
import { AttributeResponseTypes } from "constants/inventory.constant";
import { TemplateCriteria } from "constants/template-states.constant";

// Enums
import {
    TemplateRuleApproverOptions,
    TemplateRuleActions,
    TemplateRuleRespondentOptions,
} from "modules/template/enums/template-rule.enums";
import {
    ComparisonOperators,
    LogicalOperators,
} from "enums/operators.enum";
import { TemplateStates } from "enums/template-states.enum";

// Services
import { TemplateAPI } from "templateServices/api/template-api.service";
import TemplateAction from "templateServices/actions/template-action.service";
import { AATemplateApiService } from "modules/assessment/services/api/aa-template-api.service";
import { OrgGroupApiService } from "oneServices/api/org-group-api.service";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

//#endregion IMPORTS

@Injectable()
export class TemplateRuleService {

    get comparisonOperator() {
        return {
            equal:    { value: ComparisonOperators.EQUAL_TO, label: this.translatePipe.transform("Is").toLowerCase() },
            notEqual: { value: ComparisonOperators.NOT_EQUAL_TO, label: this.translatePipe.transform("IsNot").toLowerCase() },
        };
    }
    get logicalOperator() {
        return {
            and: { value: LogicalOperators.AND, label: this.translatePipe.transform("All").toLowerCase() },
            or:  { value: LogicalOperators.OR, label: this.translatePipe.transform("any") },
        };
    }
    get type() {
        return {
            response: { value: "response", label: this.translatePipe.transform("Response").toLowerCase() },
        };
    }
    get action(): IStringMap<ILabelValue<TemplateRuleActions>> {
        return {
            skip:                       { value: TemplateRuleActions.SKIP, label: this.translatePipe.transform("SkipTo") },
            sendAssessmentOnSubmit:     { value: TemplateRuleActions.ASSESSMENT_CREATION_ON_SUBMIT, label: this.translatePipe.transform("SendAssessmentUponSubmission") },
            sendAssessmentOnApprove:    { value: TemplateRuleActions.ASSESSMENT_CREATION_ON_APPROVAL, label: this.translatePipe.transform("SendAssessmentUponApproval") },
            riskCreation:               { value: TemplateRuleActions.RISK_CREATION, label: this.translatePipe.transform("CreateRisk") },
            addApproverOnSubmit:        { value: TemplateRuleActions.ADD_APPROVER_ON_SUBMISSION, label: this.translatePipe.transform("AddApproversUponSubmission") },
            addApproverByOrgOnLaunch:   { value: TemplateRuleActions.ADD_APPROVERS_FOR_ORG_ON_LAUNCH, label: this.translatePipe.transform("AddApproversBasedOnAssessmentOrg") },
        };
    }

    constructor(
        @Inject(StoreToken) private store: IStore,
        @Inject("TemplateAPI") private templateAPI: TemplateAPI,
        @Inject("TemplateAction") private readonly templateAction: TemplateAction,
        private translatePipe: TranslatePipe,
        private OrgGroups: OrgGroupApiService,
        private AATemplateApi: AATemplateApiService,
    ) { }

    public getQuestionList(sequencePrepended: boolean): IQuestionParameter[] {
        const questionList: IQuestionParameter[] = [];
        const builderState = getBuilderView(this.store.getState());
        const sectionList = builderState.sectionAllIds;
        sectionList.forEach((sectionId: string) => {
            const section = builderState.sectionById[sectionId];
            const sectionQuestionList = builderState.sectionIdQuestionIdsMap[sectionId] || [];
            sectionQuestionList.forEach((questionId: string) => {
                const question = builderState.questionById[questionId];
                if (question.questionType === QuestionTypes.MultiChoice.name ||
                    question.questionType === QuestionTypes.YesNo.name) {
                    this.addQuestion(questionList, question, section, sequencePrepended);

                } else if (question.questionType === QuestionTypes.Attribute.name
                    && question.attributeJson && question.attributeJson.attributeType !== AttributeType.USERS
                    && (question.attributeJson.responseType === AttributeResponseTypes.SingleSelect ||
                        question.attributeJson.responseType === AttributeResponseTypes.MultiSelect)) {
                    const parentInventoryQuestion = builderState.questionById[question.parentQuestionId];
                    if (parentInventoryQuestion && !parentInventoryQuestion.attributeJson.isMultiSelect) {
                        this.addQuestion(questionList, question, section, sequencePrepended);
                    }
                }
            });
        });

        return questionList;
    }

    public getSkipParameters(skipConditionId: string): ISkipConditionParameters {
        const navigation = getTempNavigationById(skipConditionId)(this.store.getState());
        const nextSections = this.getNextSections(navigation.sourceSectionId);
        let nextQuestions = [];
        if (navigation.destinationSectionId) {
            nextQuestions = this.getNextQuestions(navigation.sourceSectionId, navigation.sourceQuestionId, navigation.destinationSectionId);
        }
        let selectedSection: IAssessmentTemplateSection;
        if (navigation.destinationSectionId) {
            selectedSection = getSectionById(this.store.getState(), navigation.destinationSectionId);
        }
        let selectedQuestion: IAssessmentTemplateQuestion;
        if (navigation.destinationQuestionId) {
            selectedQuestion = getQuestionById(navigation.destinationQuestionId)(this.store.getState());
        }
        return {
            nextSections,
            nextQuestions,
            selectedSection,
            selectedQuestion,
        };
    }

    public createTempActionRule(action: TemplateRuleActions): void {
        const templateRootVersionId = getCurrentTemplate(this.store.getState()).rootVersionId;
        const sequence = getTemplateRuleNextSequence(this.store.getState());
        let actionParameters: ICreateAssessmentParameters | ICreateRiskParameters | IAddApproverParameters | IAddApproverByOrgParameters;
        if (action === TemplateRuleActions.ASSESSMENT_CREATION_ON_SUBMIT) {
            actionParameters = {
                approverSource: null,
                templateRootVersionId: null,
                keepOpen: false,
            } as ICreateAssessmentParameters;
        } else if (action === TemplateRuleActions.ASSESSMENT_CREATION_ON_APPROVAL) {
            actionParameters = {
                approverSource: null,
                templateRootVersionId: null,
                needsConfirmation: false,
            } as ICreateAssessmentParameters;
        } else if (action === TemplateRuleActions.RISK_CREATION) {
            actionParameters = {
                description: null,
                level: null,
                recommendation: null,
            } as ICreateRiskParameters;
        } else if (action === TemplateRuleActions.ADD_APPROVER_ON_SUBMISSION) {
            actionParameters = {
                approverIds: null,
                templateRootVersionId: null,
            } as IAddApproverParameters;
        }
        const actionRule: ITemplateRule = {
            id: null,
            sequence,
            action,
            actionParameters,
            conditionGroups: [{
                logicalOperator: LogicalOperators.OR,
                questionId: null,
                sectionId: null,
                conditions: [{
                    comparisonOperator: ComparisonOperators.EQUAL_TO,
                    logicalOperator: LogicalOperators.OR,
                    optionId: null,
                    optionType: null,
                    orgGroupId: null,
                }],
            }],
            invalid: true,
        };
        this.store.dispatch({ type: TemplateActions.UPDATE_ACTION_TEMP_MODEL, actionRule });
        this.store.dispatch({ type: TemplateActions.RESET_SELECTED_TEMPLATE_RULE_COMBINATION });
    }

    public setTempActionRule(ruleId: string): void {
        const actionRule = getTemplateActionRuleById(ruleId)(this.store.getState());

        this.generateTemplateRuleCombination(actionRule);
        this.store.dispatch({ type: TemplateActions.UPDATE_ACTION_TEMP_MODEL, actionRule });
    }

    public getOptions(questionId: string, sectionId: string): Observable<IQuestionOptionDetails[]> {
        const question = getQuestionById(questionId)(this.store.getState());
        const options = [];
        if (question.questionType === QuestionTypes.MultiChoice.name || question.questionType === QuestionTypes.YesNo.name) {
            if (isArray(question.options) && question.options.length > 0) {
                question.options.forEach((option: IQuestionOptionDetails) => {
                    let optionLabel: string;
                    if (question.questionType === QuestionTypes.YesNo.name) {
                        optionLabel = this.translatePipe.transform(option.option);
                    } else {
                        switch (option.optionType) {
                            case QuestionResponseType.Default:
                                optionLabel = option.option;
                                break;
                            case QuestionResponseType.NotApplicable:
                                optionLabel = this.translatePipe.transform("NotApplicable");
                                break;
                            case QuestionResponseType.NotSure:
                                optionLabel = this.translatePipe.transform("NotSure");
                                break;
                            case QuestionResponseType.Others:
                                optionLabel = this.translatePipe.transform("Other");
                                break;
                        }
                    }
                    options.push({ ...option, option: optionLabel });
                });
            }
            return of(options);
        } else if (question.questionType === QuestionTypes.Attribute.name) {
            return this.templateAction
                .getQuestionOptions(questionId, sectionId, getCurrentTemplate(this.store.getState()).id).pipe(
                    map((list: IQuestionOptionDetails[]) => {
                        let questionOptionDetails: IQuestionOptionDetails[] = [...list];
                        if (questionOptionDetails.length > 0) {
                            questionOptionDetails = questionOptionDetails.map((option: IQuestionOptionDetails) => {
                                const newOption = { ...option };
                                switch (option.optionType) {
                                    case QuestionResponseType.Default:
                                        break;
                                    case QuestionResponseType.NotApplicable:
                                        newOption.option = this.translatePipe.transform("NotApplicable");
                                        break;
                                    case QuestionResponseType.NotSure:
                                        newOption.option = this.translatePipe.transform("NotSure");
                                        break;
                                    case QuestionResponseType.Others:
                                        newOption.option = this.translatePipe.transform("Other");
                                        break;
                                }
                                return newOption;
                            });
                            return questionOptionDetails;
                        }
                    }));
        }
    }

    public addSkipCondition(index: number, logicalOperator: LogicalOperators, conditionId: string): void {
        const navigation = getTempNavigationById(conditionId)(this.store.getState());
        const newConditions = [...navigation.conditions];
        newConditions.splice(index + 1, 0, {
            comparisonOperator: ComparisonOperators.EQUAL_TO,
            logicalOperator: logicalOperator as any,
            optionId: null,
            optionType: null,
        });
        navigation.conditions = newConditions;
        this.validateAndUpdateSkipCondition(navigation);
    }

    public addRuleCondition(index: number, logicalOperator: LogicalOperators): void {
        const templateRule = getTemplateActionRuleTempModel(this.store.getState());
        const newConditionGroups = [...templateRule.conditionGroups];
        newConditionGroups.splice(index + 1, 0, {
            logicalOperator,
            questionId: null,
            sectionId: null,
            conditions: [{
                comparisonOperator: ComparisonOperators.EQUAL_TO,
                logicalOperator,
                optionId: null,
                optionType: null,
                orgGroupId: null,
            }],
        });
        templateRule.conditionGroups = newConditionGroups;
        this.validateAndUpdateRule(templateRule);
    }

    public removeSkipCondition(index: number, conditionId: string): void {
        const navigation = getTempNavigationById(conditionId)(this.store.getState());
        const newConditions = [...navigation.conditions];
        newConditions.splice(index, 1);
        navigation.conditions = newConditions;
        this.validateAndUpdateSkipCondition(navigation);
    }

    public removeRuleCondition(index: number): void {
        const actionRule = getTemplateActionRuleTempModel(this.store.getState());
        const newConditionGroups = [...actionRule.conditionGroups];
        newConditionGroups.splice(index, 1);
        actionRule.conditionGroups = newConditionGroups;
        this.store.dispatch({ type: TemplateActions.REMOVE_SELECTED_TEMPLATE_RULE_COMBINATION, index });
        this.validateAndUpdateRule(actionRule);
    }

    public updateSkipQuestion(question: IAssessmentTemplateQuestion, conditionId: string): void {
        const navigation = getTempNavigationById(conditionId)(this.store.getState());
        navigation.destinationQuestionId = question.id;
        this.validateAndUpdateSkipCondition(navigation);
    }

    public updateSkipSection(section: IAssessmentTemplateSection, conditionId: string): ISkipConditionParameters {
        const navigation = getTempNavigationById(conditionId)(this.store.getState());
        const nextQuestions = this.getNextQuestions(navigation.sourceSectionId, navigation.sourceQuestionId, section.id);
        navigation.destinationSectionId = section.id;
        navigation.destinationQuestionId = null;
        this.validateAndUpdateSkipCondition(navigation);
        return {
            nextQuestions,
        };
    }

    public updateComparisonOperatorSkipAction(operator: ILabelValue<ComparisonOperators>, index: number, conditionId: string): void {
        const navigation = getTempNavigationById(conditionId)(this.store.getState());
        const newConditions = [...navigation.conditions];
        newConditions[index] = {
            ...newConditions[index],
            comparisonOperator: operator.value,
        };
        navigation.conditions = newConditions;
        this.validateAndUpdateSkipCondition(navigation);
    }

    public updateComparisonOperatorRuleAction(operator: ILabelValue<ComparisonOperators>, index: number): void {
        const actionRule = getTemplateActionRuleTempModel(this.store.getState());
        const newConditionGroups = [...actionRule.conditionGroups];
        newConditionGroups[index] = {
            ...newConditionGroups[index],
            conditions: [{
                ...newConditionGroups[index].conditions[0],
                comparisonOperator: operator.value,
            }],
        };
        actionRule.conditionGroups = newConditionGroups;
        this.updateSelectedTemplateRuleCombination(actionRule, index);
        this.validateAndUpdateRule(actionRule);
    }

    public updateLogicalOperatorSkipAction(operator: ILabelValue<LogicalOperators>, conditionId: string): void {
        const navigation = getTempNavigationById(conditionId)(this.store.getState());
        const newConditions = navigation.conditions.map((condition: IAssessmentNavigationCondition): IAssessmentNavigationCondition => {
            return { ...condition, logicalOperator: operator.value };
        });
        navigation.conditions = newConditions;
        this.validateAndUpdateSkipCondition(navigation);
    }

    public updateLogicalOperatorRuleAction(operator: ILabelValue<LogicalOperators>): void {
        const actionRule = getTemplateActionRuleTempModel(this.store.getState());
        const newConditionGroups = actionRule.conditionGroups.map((group: ITemplateRuleConditionGroup): ITemplateRuleConditionGroup => {
            return {
                ...group,
                logicalOperator: operator.value,
                conditions: [{
                    ...group.conditions[0],
                    logicalOperator: operator.value,
                }],
            };
        });
        actionRule.conditionGroups = newConditionGroups;
        this.validateAndUpdateRule(actionRule);
    }

    public updateQuestionActionRule(question: IQuestionParameter, index: number): void {
        const actionRule = getTemplateActionRuleTempModel(this.store.getState());
        const newConditionGroups = [...actionRule.conditionGroups];
        newConditionGroups[index] = {
            ...newConditionGroups[index],
            questionId: question.value,
            sectionId: question.sectionId,
        };
        actionRule.conditionGroups = newConditionGroups;
        this.updateSelectedTemplateRuleCombination(actionRule, index);
        this.validateAndUpdateRule(actionRule);
    }

    public updateSkipConditionOption(option: IQuestionOptionDetails, index: number, conditionId: string): void {
        const navigation = getTempNavigationById(conditionId)(this.store.getState());
        const newConditions = [...navigation.conditions];
        newConditions[index] = {
            ...newConditions[index],
            optionId: option.id,
            optionType: option.optionType,
        };
        navigation.conditions = newConditions;
        this.validateAndUpdateSkipCondition(navigation);
    }

    public updateRiskParameters(params: ICreateRiskParameters): void {
        const actionRule = getTemplateActionRuleTempModel(this.store.getState());
        actionRule.actionParameters = {
            ...actionRule.actionParameters,
            level: params.level,
            description: params.description,
            recommendation: params.recommendation,
            impactLevel: params.impactLevel,
            impactLevelId: params.impactLevelId,
            probabilityLevel: params.probabilityLevel,
            probabilityLevelId: params.probabilityLevelId,
            riskScore: params.riskScore,
        };
        this.validateAndUpdateRule(actionRule);
    }

    public updateRuleConditionOption(option: IQuestionOptionDetails, index: number): void {
        const actionRule = getTemplateActionRuleTempModel(this.store.getState());
        let newConditionGroups: ITemplateRuleConditionGroup[] = [...actionRule.conditionGroups];
        newConditionGroups = newConditionGroups.map((conditionGroup): ITemplateRuleConditionGroup => {
            conditionGroup.orgGroupId = option.orgGroupId;
            return omitBy(conditionGroup, isNil);
        });
        let conditions = [];
        switch (actionRule.action) {
            case TemplateRuleActions.ADD_APPROVERS_FOR_ORG_ON_LAUNCH:
                conditions = [{
                    ...newConditionGroups[index].conditions[0],
                    orgGroupId: option.orgGroupId,
                }];
                break;
            default:
                conditions = [{
                    ...newConditionGroups[index].conditions[0],
                    optionId: option.id,
                    optionType: option.optionType,
                }];
                break;
        }
        omit(newConditionGroups[index], ["questionId", "sectionId"]);

        newConditionGroups[index] = {
            ...newConditionGroups[index],
            orgGroupId: option.orgGroupId,
            conditions,
        };
        actionRule.conditionGroups = newConditionGroups;
        this.updateSelectedTemplateRuleCombination(actionRule, index);
        this.validateAndUpdateRule(actionRule);
    }

    public updateRuleApproverSource(approverSource: TemplateRuleApproverOptions): void {
        const actionRule = getTemplateActionRuleTempModel(this.store.getState());
        actionRule.actionParameters = {
            ...actionRule.actionParameters,
            approverSource,
        };
        this.validateAndUpdateRule(actionRule);
    }

    public updateRuleRespondentSource(respondentSource: TemplateRuleRespondentOptions): void {
        const actionRule = getTemplateActionRuleTempModel(this.store.getState());
        actionRule.actionParameters = {
            ...actionRule.actionParameters,
            respondentSource,
        };
        this.validateAndUpdateRule(actionRule);
    }

    public updateRuleApproverList(approverIds: string[]): void {
        const actionRule = getTemplateActionRuleTempModel(this.store.getState());
        actionRule.actionParameters = {
            ...actionRule.actionParameters,
            approverIds,
        };
        this.validateAndUpdateRule(actionRule);
    }

    public updateRuleTemplate(templateRootVersionId: string): void {
        const actionRule = getTemplateActionRuleTempModel(this.store.getState());
        actionRule.actionParameters = {
            ...actionRule.actionParameters,
            templateRootVersionId,
        };
        this.validateAndUpdateRule(actionRule);
    }

    public updateRuleToggleForAutoSubmission(keepOpen: boolean): void {
        const actionRule = getTemplateActionRuleTempModel(this.store.getState());
        actionRule.actionParameters = {
            ...actionRule.actionParameters,
            keepOpen,
        };
        this.validateAndUpdateRule(actionRule);
    }

    public validateCondition(condition: IAssessmentNavigation): boolean | string {
        if (isEmpty(condition.destinationSectionId)) {
            return false;
        }
        if (isEmpty(condition.destinationQuestionId)) {
            return false;
        }

        return this.validationConditionExpressions(condition.conditions);
    }

    public fecthPublishedTemplates(): Observable<ITemplateNameValue[] | boolean> {
        const state: IStoreState = this.store.getState();
        const templates: ITemplateNameValue[] = getPublishedTemplates(state);
        const isFetchingTemplate: boolean = isFetchingPublishedTemplates(state);
        if (templates.length) {
            return of(templates);
        } else {
            if (!isFetchingTemplate) {
                this.store.dispatch({ type: TemplateActions.FETCH_PUBLISHED_TEMPLATE_LIST });
                return from(this.AATemplateApi.getAssessmentTemplates(`${TemplateTypes.PIA},${TemplateTypes.VENDOR}`, TemplateStates.Published, TemplateCriteria.Active)
                    .then((response: IProtocolResponse<ICustomTemplateDetails[]>): ITemplateNameValue[] => {
                        if (response.result) {
                            const templateNameValuePairs: ITemplateNameValue[] = response.data.map((customTemplateDetails: ICustomTemplateDetails): ITemplateNameValue => {
                                return {
                                    id: customTemplateDetails.rootVersionId,
                                    name: customTemplateDetails.name,
                                };
                            });
                            this.store.dispatch({ type: TemplateActions.SET_PUBLISHED_TEMPLATE_LIST, templates: templateNameValuePairs });
                            return templateNameValuePairs;
                        }
                }));
            } else {
                return observableFromStore(this.store).pipe(
                    switchMap((storeState: IStoreState) => isFetchingPublishedTemplates(storeState) ? NEVER : of(getPublishedTemplates(storeState))),
                );
            }
        }
    }

    public saveRule(): Observable<boolean> {
        const storeState = this.store.getState();
        const actionRule = getTemplateActionRuleTempModel(storeState);
        const templateId = getCurrentTemplate(storeState).id;
        return this.templateAPI.saveTemplateRule(templateId, [actionRule]).pipe(
            map((actionRules: ITemplateRule[]): boolean => {
                if (actionRules.length > 0) {
                    this.store.dispatch({ type: TemplateActions.UPDATE_TEMPLATE_ACTION_RULE, actionRules });
                    return true;
                }
                return false;
            }),
        );
    }

    public deleteTemplateActionRule(templateId: string, conditionId: string): IPromise<boolean> {
        return this.templateAPI.deleteTemplateActionRule(templateId, conditionId)
            .then((result: boolean): boolean => {
                this.store.dispatch({ type: TemplateActions.DELETE_TEMPLATE_ACTION_RULE, actionId: conditionId });
                return result;
            });
    }

    public generateTemplateRuleCombination(actionRule: ITemplateRule): void {
        const templateConditionIds: string[] = [];
        each(actionRule.conditionGroups, (group: ITemplateRuleConditionGroup): void => {
            const condition = group.conditions[0];
            templateConditionIds.push(`${group.questionId}-${condition.comparisonOperator}-${condition.optionId}`);
        });
        this.store.dispatch({ type: TemplateActions.ADD_TEMPLATE_RULE_COMBINATION, combinations: templateConditionIds });
    }

    public generateSkipConditionCombinations(navigations: IAssessmentNavigation[]): void {
        const combinationList: IStringMap<ITemplateSkipConditionCombination> = {};
        each(navigations, (navigation: IAssessmentNavigation) => {
            const conditionCombination: ITemplateSkipConditionCombination = {
                combinations: [],
                defaultValue: false,
                logicalOperator: navigation.conditions[0].logicalOperator,
            };
            navigation.conditions.forEach((condition: IAssessmentNavigationCondition) => {
                conditionCombination.combinations.push(`${condition.comparisonOperator}-${condition.optionId}`);
            });
            combinationList[navigation.id] = conditionCombination;
        });
        this.store.dispatch({ type: TemplateActions.ADD_SKIP_CONDITION_COMBINATION, combinations: combinationList });
    }

    public addSelectedSkipConditionCombination(navigation: IAssessmentNavigation): void {
        const selectedConditionCombination: ITemplateSkipConditionCombination = {
            combinations: [],
            defaultValue: true,
            logicalOperator: navigation.conditions[0].logicalOperator,
        };
        navigation.conditions.forEach((condition: IAssessmentNavigationCondition) => {
            selectedConditionCombination.combinations.push(`${condition.comparisonOperator}-${condition.optionId}`);
        });
        this.store.dispatch({ type: TemplateActions.UPDATE_SELECTED_SKIP_CONDITION_COMBINATION, conditionId: navigation.id, condition: selectedConditionCombination });
    }

    public removeSelectedSkipConditionCombination(questionId: string, conditionId: string): void {
        this.store.dispatch({ type: TemplateActions.REMOVE_SELECTED_SKIP_CONDITION_COMBINATION, conditionId });
        const conditions = getTempNavigationsByQuestion(questionId)(this.store.getState());
        each(conditions, (condition: IAssessmentNavigation) => {
            this.validateAndUpdateSkipCondition(condition);
        });
    }

    public updateRuleToggleUponApproval(needsConfirmation: boolean): void {
        const actionRule = getTemplateActionRuleTempModel(this.store.getState());
        actionRule.actionParameters = {
            ...actionRule.actionParameters,
            needsConfirmation,
        };
        this.validateAndUpdateRule(actionRule);
    }

    private getNextSections(sectionId: string): IAssessmentTemplateSection[] {
        const sectionSequence = [...getBuilderView(this.store.getState()).sectionAllIds];
        const sectionIndex = sectionSequence.indexOf(sectionId);
        return sectionSequence
            .splice(sectionIndex)
            .map((currentSectionId: string) => getSections(this.store.getState())(currentSectionId));
    }

    private getNextQuestions(sectionId: string, questionId: string, selectedSectionId: string): IAssessmentTemplateQuestion[] {
        // if selected section is same as current section id, select the questions ahead of current question
        let nextQuestionIds = [];
        if (sectionId === selectedSectionId) {
            const questionSequence = [...(getBuilderView(this.store.getState()).sectionIdQuestionIdsMap[sectionId] || [])];
            const questionIndex = questionSequence.indexOf(questionId);
            nextQuestionIds = questionSequence.splice(questionIndex + 1);
        } else {
            nextQuestionIds = [...(getBuilderView(this.store.getState()).sectionIdQuestionIdsMap[selectedSectionId] || [])];
        }

        const navigationQuestion = getQuestionById(questionId)(this.store.getState());

        return nextQuestionIds.reduce((filtered: IAssessmentTemplateQuestion[], currentQuestionId: string) => {
            const question = getQuestionById(currentQuestionId)(this.store.getState());
            // select only non-attribute questions or questions that belong to the same inventory question
            if (!question.parentQuestionId || question.parentQuestionId === navigationQuestion.parentQuestionId) {
                filtered.push(question);
            }
            return filtered;
        }, []);
    }

    private updateSelectedTemplateRuleCombination(actionRule: ITemplateRule, index: number): void {
        const condition = actionRule.conditionGroups[index].conditions[0];
        const id = `${actionRule.conditionGroups[index].questionId}-${condition.comparisonOperator}-${condition.optionId}`;
        this.store.dispatch({ type: TemplateActions.UPDATE_SELECTED_TEMPLATE_RULE_COMBINATION, id, index });
    }

    private updateSelectedSkipConditionCombination(navigation: IAssessmentNavigation): boolean | string {
        const selectedSkipCondition: ITemplateSkipConditionCombination = {
            combinations: [],
            defaultValue: false,
            logicalOperator: navigation.conditions[0].logicalOperator,
        };
        navigation.conditions.forEach((condition: IAssessmentNavigationCondition) => {
            if (condition.optionId) {
                selectedSkipCondition.combinations.push(`${condition.comparisonOperator}-${condition.optionId}`);
            } else {
                navigation.invalid = true;
                navigation.errorMessage = "";
            }
        });

        const skipConditioncombinations = getSkipConditionCombinations(this.store.getState());
        const conditions = getTempNavigationsByQuestion(navigation.destinationQuestionId)(this.store.getState());

        let duplicates: string[] = [];
        each(skipConditioncombinations, (condition: ITemplateSkipConditionCombination, id: string): void => {
            if (condition.defaultValue) {
                return;
            }
            if (selectedSkipCondition.logicalOperator === condition.logicalOperator && navigation.id !== id) {
                const duplicateIds = intersection(condition.combinations, selectedSkipCondition.combinations);
                duplicates = duplicates.concat(duplicateIds);
            }
        });

        this.store.dispatch({ type: TemplateActions.UPDATE_SELECTED_SKIP_CONDITION_COMBINATION, conditionId: navigation.id, condition: selectedSkipCondition });
        if (duplicates.length > 0 || (uniq(selectedSkipCondition.combinations).length !== selectedSkipCondition.combinations.length)) {
            return this.translatePipe.transform("DuplicateConditionsAdded");
        }
        return false;
    }

    private validateAndUpdateRule(actionRule: ITemplateRule): void {
        const valid = this.validateActionRule(actionRule);
        const combinations = getTemplateRuleCombinations(this.store.getState()) || [];
        const validCombination = uniq(combinations).length !== combinations.length ? this.translatePipe.transform("DuplicateConditionsAdded") : false;

        if (isBoolean(valid) && isBoolean(validCombination)) {
            actionRule.errorMessage = "";
            actionRule.invalid = !valid;
        } else {
            switch (actionRule.action) {
                case this.action.skip.value:
                    actionRule.errorMessage = valid as string;
                    break;
                case this.action.sendAssessmentOnSubmit.value:
                case this.action.sendAssessmentOnApprove.value:
                case this.action.riskCreation.value:
                case this.action.addApproverOnSubmit.value:
                case this.action.addApproverByOrgOnLaunch.value:
                    actionRule.errorMessage = validCombination as string;
                    break;
            }
            actionRule.invalid = true;
        }
        this.store.dispatch({ type: TemplateActions.UPDATE_ACTION_TEMP_MODEL, actionRule });
    }

    private validateActionRule(actionRule: ITemplateRule): string | boolean {
        switch (actionRule.action) {
            case TemplateRuleActions.ASSESSMENT_CREATION_ON_SUBMIT:
            case TemplateRuleActions.ASSESSMENT_CREATION_ON_APPROVAL:
                if (isEmpty((actionRule.actionParameters as ICreateAssessmentParameters).approverSource)
                    || isEmpty((actionRule.actionParameters as ICreateAssessmentParameters).templateRootVersionId)) {
                    return false;
                }
                break;
            case TemplateRuleActions.RISK_CREATION:
                if (!(actionRule.actionParameters as ICreateRiskParameters).level) {
                    return false;
                }
                break;
            case TemplateRuleActions.ADD_APPROVER_ON_SUBMISSION:
            case TemplateRuleActions.ADD_APPROVERS_FOR_ORG_ON_LAUNCH:
                if (actionRule.actionParameters) {
                    const approverList = (actionRule.actionParameters as IAddApproverParameters).approverIds;
                    if (!approverList || !approverList.length) {
                        return false;
                    }
                } else {
                    return false;
                }
        }
        return actionRule.conditionGroups.every((group: ITemplateRuleConditionGroup) => {
            switch (actionRule.action) {
                case TemplateRuleActions.ADD_APPROVERS_FOR_ORG_ON_LAUNCH:
                    const actionParameters = actionRule.actionParameters as IAddApproverParameters;
                    return !isEmpty(group.logicalOperator)
                        && Boolean(actionParameters)
                        && Boolean(actionRule.conditionGroups[0].orgGroupId);
                default:
                    if (isEmpty(group.questionId)
                        || isEmpty(group.logicalOperator)
                        || isEmpty(group.sectionId)) {
                        return false;
                    }

                    return group.conditions.every((condition: ITemplateRuleCondition) => {
                        if (isEmpty(condition.optionId)
                            || isEmpty(condition.comparisonOperator)) {
                            return false;
                        }
                        return true;
                    });
            }
        });

    }

    private validateAndUpdateSkipCondition(navigation: IAssessmentNavigation): void {
        const valid = this.validateCondition(navigation);
        const isValidCombinations = this.updateSelectedSkipConditionCombination(navigation);

        if (isBoolean(valid) && isBoolean(isValidCombinations)) {
            navigation.errorMessage = "";
            navigation.invalid = !valid;
        } else {
            navigation.invalid = true;
            if (!isBoolean(isValidCombinations)) {
                navigation.errorMessage = isValidCombinations as string;
            } else {
                navigation.errorMessage = valid as string;
            }
        }
        this.store.dispatch({ type: TemplateActions.UPDATE_TEMP_NAVIGATION, navigation });
    }

    private validationConditionExpressions(conditions: IAssessmentNavigationCondition[]): boolean | string {

        let isNotComparisonUsed = false;
        let errorMessage: string;

        const allConditionsValid = conditions.every((condition: IAssessmentNavigationCondition, index: number) => {

            isNotComparisonUsed = isNotComparisonUsed || condition.comparisonOperator === this.comparisonOperator.notEqual.value;

            // when 'is not' is used, we can have only one condition.
            if (isNotComparisonUsed && index !== 0) {
                errorMessage = this.translatePipe.transform("OnlyOneConditionAllowedForIsNot");
                return false;
            }

            // check condition type
            if (isEmpty(condition.logicalOperator)) {
                return false;
            }
            if (isEmpty(condition.optionId)) {
                return false;
            }

            return true;
        });

        return errorMessage || allConditionsValid;
    }

    private addQuestion(questionList: IQuestionParameter[], question: IAssessmentTemplateQuestion, section: IAssessmentTemplateSection, sequencePrepended: boolean): void {
        questionList.push({
            value: question.id,
            sectionId: section.id,
            label: sequencePrepended
                ? `${section.sequence}.${question.sequence} ${question.friendlyName || question.content}`
                : `${question.friendlyName || question.content}`,
        });
    }

}
