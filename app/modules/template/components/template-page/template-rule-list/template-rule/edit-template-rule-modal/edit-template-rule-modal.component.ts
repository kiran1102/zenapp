// Angular
import {
    Component,
    OnInit,
    Inject,
    OnDestroy,
} from "@angular/core";

// Rxjs
import { Subject } from "rxjs";
import {
    startWith,
    takeUntil,
} from "rxjs/operators";

// Interfaces
import {
    IStore,
    IStoreState,
} from "interfaces/redux.interface";

// Services
import { ModalService } from "sharedServices/modal.service";
import { TemplateRuleService } from "../template-rule.service";
import { Permissions } from "modules/shared/services/helper/permissions.service";

// Redux
import { StoreToken } from "tokens/redux-store.token";
import {
    TemplateActions,
    getTemplateActionRuleTempModel,
} from "oneRedux/reducers/template.reducer";
import observableFromStore from "oneRedux/utils/observable-from-store";

// Enums
import {
    TemplateRuleActions,
    TemplateRuleModes,
} from "modules/template/enums/template-rule.enums";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

@Component({
    selector: "edit-template-tule-modal",
    templateUrl: "edit-template-rule-modal.component.html",
})

export class EditTemplateRuleModalComponent implements OnInit, OnDestroy {

    actionId: string;
    allowedActions: TemplateRuleActions[] = [
        TemplateRuleActions.ASSESSMENT_CREATION_ON_SUBMIT,
        TemplateRuleActions.ASSESSMENT_CREATION_ON_APPROVAL,
    ];
    destroy$ = new Subject();
    invalidRule: boolean;
    isSaving = false;
    mode: TemplateRuleModes.EDIT | TemplateRuleModes.CREATE;
    modalTitle = this.translatePipe.transform("Rules");
    templateActionRule = getTemplateActionRuleTempModel(this.store.getState());

    constructor(
        @Inject(StoreToken) private store: IStore,
        private translatePipe: TranslatePipe,
        private templateRule: TemplateRuleService,
        private modalService: ModalService,
        private permissionsLegacyService: Permissions,
    ) {
        this.allowedActions.push(TemplateRuleActions.RISK_CREATION);
    }

    ngOnInit(): void {
        this.mode = this.templateActionRule.id ? TemplateRuleModes.EDIT : TemplateRuleModes.CREATE;

        observableFromStore(this.store).pipe(
            startWith(this.store.getState()),
            takeUntil(this.destroy$),
        ).subscribe((state: IStoreState) => this.componentWillReceiveState(state));
    }

    ngOnDestroy(): void {
        this.destroy$.next();
        this.destroy$.complete();
        this.store.dispatch({ type: TemplateActions.RESET_ACTION_TEMP_MODEL });
    }

    closeModal = (): void => {
        this.modalService.closeModal();
        this.modalService.clearModalData();
    }

    saveRule(): void {
        this.isSaving = true;
        this.templateRule.saveRule().subscribe((success: boolean): void => {
            if (success) {
                this.closeModal();
            }
            this.isSaving = false;
        });
    }

    private componentWillReceiveState(state: IStoreState): void {
        this.invalidRule = getTemplateActionRuleTempModel(state).invalid;
    }
}
