// Angular
import {
    Component,
    Inject,
    Input,
    OnInit,
    OnDestroy,
} from "@angular/core";

// External
import { Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";

// Lodash
import {
    map,
    filter,
    forEach,
} from "lodash";

// Services
import { TemplateRuleService } from "../../../template-rule/template-rule.service";
import { OrgGroupApiService } from "oneServices/api/org-group-api.service";

// Redux
import { StoreToken } from "tokens/redux-store.token";
import {
    getTemplateActionRuleTempModel,
    getTemplateActionRuleById,
} from "oneRedux/reducers/template.reducer";
import observableFromStore from "oneRedux/utils/observable-from-store";
import { getCurrentOrgId } from "oneRedux/reducers/org.reducer";

// Enums
import { OrgUserTraversal } from "enums/org-user-dropdown.enum";

// Interfaces
import { IStore } from "interfaces/redux.interface";
import { IOrgUserAdapted } from "interfaces/org-user.interface";
import {
    ITemplateRule,
    IAddApproverParameters,
} from "modules/template/components/template-page/template-rule-list/template-rule/template-rule.interface";

@Component({
    selector: "template-rule-add-approvers-parameters",
    templateUrl: "template-rule-add-approvers-parameters.component.html",
})

export class TemplateRuleAddApproversParametersComponent implements OnInit, OnDestroy {

    @Input() actionRuleId: string;
    @Input("previewMode") isPreviewMode: boolean;
    @Input() TemplateRuleService: string;

    approverList: IOrgUserAdapted[] = [];
    isLoading = true;

    private actionRule: ITemplateRule;
    private approver: IOrgUserAdapted = {
        FullName: "",
        Id: "",
    };
    private allApprovers: IOrgUserAdapted[];
    private destroy$ = new Subject();

    constructor(
        @Inject(StoreToken) private store: IStore,
        private templateRule: TemplateRuleService,
        private OrgGroups: OrgGroupApiService,
    ) { }

    ngOnInit(): void {
        this.actionRule = this.isPreviewMode
            ? getTemplateActionRuleById(this.actionRuleId)(this.store.getState())
            : getTemplateActionRuleTempModel(this.store.getState());

        if (this.actionRuleId) {
            this.getAllApprovers();
        } else {
            this.approverList.push(this.approver);
            this.isLoading = false;
        }

        if (this.isPreviewMode) {
            observableFromStore(this.store).pipe(
                takeUntil(this.destroy$),
            ).subscribe((state) => this.componentWillReceiveState(state));
        }
    }

    ngOnDestroy(): void {
        this.destroy$.next();
        this.destroy$.complete();
    }

    onApproverSelected(approver: IOrgUserAdapted[]): void {
        this.templateRule.updateRuleApproverList(map(approver, "Id"));
    }

    initializeState() {
        if (!this.actionRule.actionParameters) return;

        const approverIds = (this.actionRule.actionParameters as IAddApproverParameters).approverIds;
        const approverList = [];

        filter(this.allApprovers, (approver) => {
            forEach(approverIds, (approverId) => {
                if (approver.Id === approverId) {
                    approverList.push(approver);
                }
            });
        });
        this.approverList = approverList;
    }

    componentWillReceiveState(state) {
        this.actionRule = getTemplateActionRuleById(this.actionRuleId)(state);
        this.initializeState();
    }

    private getAllApprovers() {
        this.OrgGroups.getOrgUsersWithPermission(
            getCurrentOrgId(this.store.getState()),
            OrgUserTraversal.All,
            null,
            ["AssessmentCanBeApprover"],
            [],
        ).then((response) => {
            if (response.result) {
                this.allApprovers = response.data;
                this.initializeState();
                this.isLoading = false;
            }
        });
    }
}
