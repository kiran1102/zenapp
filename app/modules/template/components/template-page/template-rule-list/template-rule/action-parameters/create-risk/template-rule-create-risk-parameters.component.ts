// Angular
import {
    Component,
    Inject,
    Input,
    OnInit,
} from "@angular/core";

// Rxjs
import { Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";

// External
import { debounce } from "lodash";

// Enum
import {
    RiskCharacterLimit,
} from "enums/risk.enum";

// Redux
import { StoreToken } from "tokens/redux-store.token";
import {
    IStore,
    IStoreState,
} from "interfaces/redux.interface";
import {
    getTemplateActionRuleById,
} from "oneRedux/reducers/template.reducer";
import observableFromStore from "oneRedux/utils/observable-from-store";

// Services
import { TemplateRuleService } from "../../../template-rule/template-rule.service";

// Interfaces
import { IRiskHeatmap } from "interfaces/risks-settings.interface";
import {
    IStringMap,
    ILabelValue,
    IKeyValue,
} from "interfaces/generic.interface";
import {
    ITemplateRule,
    ICreateRiskParameters,
} from "../../template-rule.interface";
import { RiskV2Levels } from "enums/riskV2.enum";

@Component({
    selector: "template-rule-create-risk-parameters",
    templateUrl: "template-rule-create-risk-parameters.component.html",
})

export class TemplateRuleCreateRiskParametersComponent implements OnInit {
    @Input() actionRuleId: string;
    @Input("previewMode") isPreviewMode: boolean;

    actionRule: ITemplateRule;
    description: string;
    recommendation: string;
    riskCharLimit: number = RiskCharacterLimit.Limit;
    impactLevel: string;
    impactLevelId: number;
    probabilityLevel: string;
    probabilityLevelId: number;
    score: number;
    levelId: number;

    private destroy$ = new Subject();
    private debouncedUpdateParameters = debounce(() => this.templateRule.updateRiskParameters({
        level: this.levelId,
        description: this.description,
        recommendation: this.recommendation,
        impactLevel: this.impactLevel,
        impactLevelId: this.impactLevelId,
        probabilityLevel: this.probabilityLevel,
        probabilityLevelId: this.probabilityLevelId,
        riskScore: this.score,
        }), 300);

    constructor(
        @Inject(StoreToken) private store: IStore,
        private templateRule: TemplateRuleService,
    ) { }

    ngOnInit(): void {
        if (this.actionRuleId) {
            this.initializeRiskParameters();
        }

        if (this.isPreviewMode) {
            observableFromStore(this.store).pipe(
                takeUntil(this.destroy$),
            ).subscribe((state: IStoreState) => this.componentWillReceiveState(state));
        }
    }

    ngOnDestroy(): void {
        this.destroy$.next();
        this.destroy$.complete();
    }

    onParameterChanged(param: string, value: string): void {
        this[param] = value;
        this.debouncedUpdateParameters();
    }

    onRiskChange(heatmapValue: IRiskHeatmap): void {
        this.impactLevel = heatmapValue.impactLevel;
        this.impactLevelId = heatmapValue.impactLevelId;
        this.probabilityLevel = heatmapValue.probabilityLevel;
        this.probabilityLevelId = heatmapValue.probabilityLevelId;
        this.score = heatmapValue.riskScore;
        this.levelId = heatmapValue.levelId;
        this.debouncedUpdateParameters();
    }

    private componentWillReceiveState(state: IStoreState): void {
        this.initializeRiskParameters();
    }

    private initializeRiskParameters(): void {
        const actionParameters = getTemplateActionRuleById(this.actionRuleId)(this.store.getState()).actionParameters as ICreateRiskParameters;
        if (!actionParameters) return;

        this.levelId = actionParameters.level as unknown as number;
        this.description = actionParameters.description;
        this.recommendation = actionParameters.recommendation;
        this.impactLevel = actionParameters.impactLevel;
        this.impactLevelId = actionParameters.impactLevelId;
        this.probabilityLevel = actionParameters.probabilityLevel;
        this.probabilityLevelId = actionParameters.probabilityLevelId;
        this.score = actionParameters.riskScore;
    }
}
