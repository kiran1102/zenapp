// Angular
import {
    Component,
    Inject,
    Input,
    OnInit,
} from "@angular/core";

// Rxjs
import { Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";

// interfaces
import { ITemplateNameValue } from "modules/template/interfaces/template.interface";
import { ITemplateRule, ICreateAssessmentParameters } from "../../template-rule.interface";
import { ILabelValue } from "interfaces/generic.interface";

// Services
import { TemplateRuleService } from "../../../template-rule/template-rule.service";

// Redux
import { StoreToken } from "tokens/redux-store.token";
import {
    IStore,
    IStoreState,
} from "interfaces/redux.interface";
import {
    getTemplateActionRuleTempModel,
    getTemplateActionRuleById,
} from "oneRedux/reducers/template.reducer";
import observableFromStore from "oneRedux/utils/observable-from-store";

// Enums
import {
    TemplateRuleApproverOptions,
    TemplateRuleRespondentOptions,
} from "modules/template/enums/template-rule.enums";
import { TemplateRuleActions } from "modules/template/enums/template-rule.enums";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

@Component({
    selector: "template-rule-create-assessment-parameters",
    templateUrl: "template-rule-create-assessment-parameters.component.html",
})
export class TemplateRuleCreateAssessmentParametersComponent implements OnInit {

    @Input() actionRuleId: string;
    @Input("previewMode") isPreviewMode: boolean;
    @Input() actionSelected: string;

    templateRuleActions = TemplateRuleActions;
    approver: ILabelValue<string>;
    respondent: ILabelValue<string>;
    approverOptions: Array<ILabelValue<string>>;
    respondentOptions: Array<ILabelValue<string>>;
    actionRule: ITemplateRule;
    selectedTemplate: ITemplateNameValue;
    templateList: ITemplateNameValue[] = [];
    keepOpen: boolean;
    needsConfirmation: boolean;

    private destroy$ = new Subject();

    constructor(
        @Inject(StoreToken) private store: IStore,
        private translatePipe: TranslatePipe,
        private templateRule: TemplateRuleService,
    ) {}

    ngOnInit(): void {
        this.actionRule = this.isPreviewMode
            ? getTemplateActionRuleById(this.actionRuleId)(this.store.getState())
            : getTemplateActionRuleTempModel(this.store.getState());
        if (this.actionRuleId) {
            this.setAutoSubmissionOrConfirmUponApprovalRule(this.store.getState());
        }

        this.approverOptions = [
            { value: TemplateRuleApproverOptions.FROM_CURRENT_ASSESSMENT, label: this.translatePipe.transform("CurrentAssessmentsApprover") },
            { value: TemplateRuleApproverOptions.LEAVE_BLANK, label: this.translatePipe.transform("Unassigned") },
        ];

        this.respondentOptions = [
            { value: TemplateRuleRespondentOptions.FROM_CURRENT_ASSESSMENT_RESPONDENT, label: this.translatePipe.transform("CurrentAssessmentsRespondent") },
            { value: TemplateRuleRespondentOptions.FROM_CURRENT_ASSESSMENT_APPROVER, label: this.translatePipe.transform("CurrentAssessmentsApprover") },
        ];

        this.initializeState();

        // need to subscribe to the store to fetch the latest state if edited in the edit modal.
        if (this.isPreviewMode) {
            observableFromStore(this.store).pipe(
                takeUntil(this.destroy$),
            ).subscribe((state: IStoreState) => this.componentWillReceiveState(state));
        } else if (!this.actionRuleId) {
            this.keepOpen = false;
        }
    }

    ngOnDestroy(): void {
        this.destroy$.next();
        this.destroy$.complete();
    }

    onSelectTemplate(selectedTemplate: ITemplateNameValue): void {
        this.selectedTemplate = selectedTemplate;
        this.templateRule.updateRuleTemplate(selectedTemplate.id);
    }

    onApproverSelected(approver: ILabelValue<string>): void {
        this.approver = approver;
        this.templateRule.updateRuleApproverSource(TemplateRuleApproverOptions[approver.value]);
    }

    onRespondentSelected(respondent: ILabelValue<string>): void {
        this.respondent = respondent;
        this.templateRule.updateRuleRespondentSource(TemplateRuleRespondentOptions[respondent.value]);
    }

    toggleStateChange(keepOpen: boolean): void {
        this.templateRule.updateRuleToggleForAutoSubmission(keepOpen);
    }

    confirmUponApproval(needsConfirmation: boolean): void {
        this.templateRule.updateRuleToggleUponApproval(needsConfirmation);
    }

    private initializeState(): void {
        this.templateRule.fecthPublishedTemplates().subscribe((list: ITemplateNameValue[]) => {
            this.templateList = list;
            this.selectedTemplate = list.find((template: ITemplateNameValue) => {
                if (this.actionRule.actionParameters) {
                    return template.id === (this.actionRule.actionParameters as ICreateAssessmentParameters).templateRootVersionId;
                }
            });
        });

        switch ((this.actionRule.actionParameters as ICreateAssessmentParameters).approverSource) {
            case TemplateRuleApproverOptions.LEAVE_BLANK:
                this.approver = this.approverOptions[1];
                break;
            case TemplateRuleApproverOptions.FROM_CURRENT_ASSESSMENT:
                this.approver = this.approverOptions[0];
                break;
        }

        switch ((this.actionRule.actionParameters as ICreateAssessmentParameters).respondentSource) {
            case TemplateRuleRespondentOptions.FROM_CURRENT_ASSESSMENT_APPROVER:
                this.respondent = this.respondentOptions[1];
                break;
            case TemplateRuleRespondentOptions.FROM_CURRENT_ASSESSMENT_RESPONDENT:
                this.respondent = this.respondentOptions[0];
                break;
            default:
                this.respondent = this.respondentOptions[0];
                break;
        }
    }

    private componentWillReceiveState(state: IStoreState): void {
        this.actionRule = getTemplateActionRuleById(this.actionRuleId)(state);
        this.setAutoSubmissionOrConfirmUponApprovalRule(state);
        if (!this.actionRule || !this.actionRule.id) {
            return;
        }
        this.initializeState();
    }

    private setAutoSubmissionOrConfirmUponApprovalRule(state: IStoreState): void {
        const actionParameters: ICreateAssessmentParameters = this.actionRule.actionParameters as ICreateAssessmentParameters;
        if (actionParameters) {
            switch (this.actionSelected) {
                case TemplateRuleActions.ASSESSMENT_CREATION_ON_SUBMIT:
                    this.keepOpen = actionParameters.keepOpen;
                    break;
                case TemplateRuleActions.ASSESSMENT_CREATION_ON_APPROVAL:
                    this.needsConfirmation = actionParameters.needsConfirmation;
                    break;
            }
        }
    }
}
