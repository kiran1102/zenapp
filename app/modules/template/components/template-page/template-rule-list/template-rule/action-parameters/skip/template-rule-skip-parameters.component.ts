// Angular
import {
    Component,
    Input,
} from "@angular/core";

// Interfaces
import { IAssessmentTemplateSection } from "interfaces/assessment-template-section.interface";
import { IAssessmentTemplateQuestion } from "interfaces/assessment-template-question.interface";

// Services
import { TemplateRuleService } from "../../template-rule.service";

@Component({
    selector: "template-rule-skip-parameters",
    templateUrl: "template-rule-skip-parameters.component.html",
})
export class TemplateRuleSkipParameteresComponent {

    @Input() skipConditionId: string;
    @Input("previewMode") isPreviewMode = true;

    nextSections: IAssessmentTemplateSection[];
    nextQuestions: IAssessmentTemplateQuestion[];
    selectedSection: IAssessmentTemplateSection;
    selectedQuestion: IAssessmentTemplateQuestion;

    constructor(
        private templateRule: TemplateRuleService,
    ) { }

    ngOnInit(): void {
        const initialParameters = this.templateRule.getSkipParameters(this.skipConditionId);
        this.nextSections = initialParameters.nextSections;
        this.nextQuestions = initialParameters.nextQuestions;
        this.selectedSection = initialParameters.selectedSection;
        this.selectedQuestion = initialParameters.selectedQuestion;

    }

    onSectionSelected(section: IAssessmentTemplateSection): void {
        const updatedParameters = this.templateRule.updateSkipSection(section, this.skipConditionId);
        this.nextQuestions = updatedParameters.nextQuestions;
        this.selectedSection = section;
        this.selectedQuestion = null;
    }

    onQuestionSelected(question: IAssessmentTemplateQuestion): void {
        const updatedParameters = this.templateRule.updateSkipQuestion(question, this.skipConditionId);
        this.selectedQuestion = question;
    }
}
