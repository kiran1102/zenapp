//#region IMPORTS
// Angular
import {
    Component,
    OnInit,
    Inject,
} from "@angular/core";

// Rxjs
import { Subject } from "rxjs";
import {
    startWith,
    takeUntil,
} from "rxjs/operators";

// Redux
import observableFromStore from "oneRedux/utils/observable-from-store";
import { StoreToken } from "tokens/redux-store.token";
import {
    IStore,
    IStoreState,
} from "interfaces/redux.interface";
import {
    getCurrentTemplate,
    getBuilderView,
} from "oneRedux/reducers/template.reducer";

// Services
import { ModalService } from "sharedServices/modal.service";
import { TemplateRuleService } from "modules/template/components/template-page/template-rule-list/template-rule/template-rule.service";

// Interfaces
import { IDeleteConfirmationModalResolve } from "interfaces/delete-confirmation-modal-resolve.interface";
import { IAssessmentTemplateDetail } from "interfaces/assessment-template-detail.interface";

// Enums / Constants
import { TemplateStates } from "constants/template-states.constant";
import {
    TemplateRuleActions,
    TemplateRuleModes,
} from "modules/template/enums/template-rule.enums";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

//#endregion IMPORTS

@Component({
    selector: "template-rule-list",
    templateUrl: "template-rule-list.component.html",
})
export class TemplateRuleListComponent implements OnInit {

    allowedActions = [
        TemplateRuleActions.ASSESSMENT_CREATION_ON_SUBMIT,
        TemplateRuleActions.ASSESSMENT_CREATION_ON_APPROVAL,
        TemplateRuleActions.RISK_CREATION,
        TemplateRuleActions.ADD_APPROVER_ON_SUBMISSION,
    ];
    isTemplatePublished: boolean;
    ruleAllIds: string[];
    template: IAssessmentTemplateDetail;
    templateRuleMode = TemplateRuleModes.PREVIEW;

    private destroy$ = new Subject();

    constructor(
        @Inject(StoreToken) private store: IStore,
        private translatePipe: TranslatePipe,
        private modalService: ModalService,
        private templateRule: TemplateRuleService,
    ) { }

    ngOnInit(): void {
        observableFromStore(this.store).pipe(
            startWith(this.store.getState()),
            takeUntil(this.destroy$),
        ).subscribe((state: IStoreState) => this.componentWillReceiveState(state));
    }

    ngOnDestroy(): void {
        this.destroy$.next();
        this.destroy$.complete();
    }

    addRule(): void {
        this.templateRule.createTempActionRule(TemplateRuleActions.ASSESSMENT_CREATION_ON_SUBMIT);
        this.modalService.openModal("downgradeEditTemplateRuleModal");
    }

    onEditRule(ruleId: string): void {
        this.templateRule.setTempActionRule(ruleId);
        this.modalService.openModal("downgradeEditTemplateRuleModal");
    }

    onConditionDeleted(conditionId: string): void {
        const modalData: IDeleteConfirmationModalResolve = {
            modalTitle: this.translatePipe.transform("DeleteRule"),
            confirmationText: this.translatePipe.transform("DeleteTemplateRuleWarning"),
            cancelButtonText: this.translatePipe.transform("Cancel"),
            submitButtonText: this.translatePipe.transform("Confirm"),
            promiseToResolve: (): ng.IPromise<boolean> => this.templateRule.deleteTemplateActionRule(this.template.id, conditionId),
        };
        this.modalService.setModalData(modalData);
        this.modalService.openModal("deleteConfirmationModal");
    }

    private componentWillReceiveState(state: IStoreState): void {
        this.ruleAllIds = getBuilderView(state).actionAllIds;
        this.template = getCurrentTemplate(this.store.getState());
        this.isTemplatePublished = this.template.status === TemplateStates.PUBLISHED.toUpperCase();
    }
}
