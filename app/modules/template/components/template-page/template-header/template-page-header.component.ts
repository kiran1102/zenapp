// Angular
import {
    Component,
    OnInit,
    Inject,
    OnDestroy,
} from "@angular/core";
import { StateService } from "@uirouter/core";

// Rxjs
import {
    startWith,
    takeUntil,
} from "rxjs/operators";
import {
    Subscription,
    Subject,
} from "rxjs";

// External Libraries
import { isArray } from "lodash";

import { TransitionService } from "@uirouter/core";

// Constants
import { TemplateHeaderActionButtons } from "constants/template-header-action-buttons.constant";
import { TemplateStates } from "constants/template-states.constant";
import { TemplateTypes } from "constants/template-types.constant";
import { AssessmentTemplateErrorCodes } from "constants/assessment-template-error-codes.constant";

// Enums
import { TemplateModes } from "enums/template-modes.enum";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

// Services
import { OtModalService } from "@onetrust/vitreus";
import TemplateAction from "templateServices/actions/template-action.service";
import { TemplateAPI } from "templateServices/api/template-api.service";
import { Permissions } from "modules/shared/services/helper/permissions.service";
import { NotificationService } from "modules/shared/services/provider/notification.service";
import { TemplateCreateHelperService } from "modules/template/services/template-create-helper.service";
import { TemplateDetailsService } from "modules/template/services/template-details.service";
import { TemplateDetailsHelperService } from "modules/template/services/template-details-helper.service";

// Interfaces
import {
    IAssessmentTemplateDetail,
    ICreateTemplateVersionResponse,
    IPublishTemplatePayload,
    IPublishTemplateModalData,
} from "interfaces/assessment-template-detail.interface";
import { IInventoryRelationshipContract } from "modules/template/interfaces/template-related-inventory.interface";
import {
    IStore,
    IStoreState,
} from "interfaces/redux.interface";
import { IStringMap } from "interfaces/generic.interface";
import { ITemplateHeaderButtonConfig } from "interfaces/template-header-button-config.interface";
import { ITemplateBuilderState } from "interfaces/template-reducer.interface";
import { IBreadcrumb } from "interfaces/breadcrumb.interface";
import { IHeaderOption } from "interfaces/assessment.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";
import {
    ConfirmModalType,
    ContextMenuType,
} from "@onetrust/vitreus";

// Redux
import observableFromStore from "oneRedux/utils/observable-from-store";
import {
    getBuilderView,
    getCurrentTemplate,
    getTemplateState,
    getTemplateVersionHistoryDrawerState,
    TemplateActions,
    getTemplateTempModel,
    isTemplateModelValid,
} from "oneRedux/reducers/template.reducer";
import { StoreToken } from "tokens/redux-store.token";

// Component
import { AaTemplateWithAssessmentPublishModal } from "modules/template-shared/components/templates-with-assessment-publish-modal/template-with-assessment-publish-modal.component";

@Component({
    selector: "template-page-header",
    templateUrl: "./template-page-header.component.html",
})

export class TemplatePageHeaderComponent implements OnInit, OnDestroy {
    isTemplateActive: boolean;
    isTemplateLocked: boolean;
    TemplateStates: IStringMap<string> = TemplateStates;
    version: number;
    name: string;
    templateId: string;
    templateType: string;
    breadCrumb: IBreadcrumb;
    isPublishable: boolean;
    mode: TemplateModes;
    permissions: IStringMap<boolean>;
    primaryButtonObj: ITemplateHeaderButtonConfig;
    rootVersionId: string;
    secondaryButtonObj: ITemplateHeaderButtonConfig;
    status: string;
    storeSubscription: Subscription;
    transitionDeregisterHook: any;
    isDrawerOpen = false;
    templateHasExistingDraft = false;
    headerOptions: IHeaderOption[] = [];
    headerDisabled: boolean;
    canShowContextMenuOptions = false;
    loadingAssessmentCount = false;
    openAssessmentPayload: IPublishTemplatePayload = {
        pushTemplateChanges: false,
        notifyRespondents: false,
    };
    translations: IStringMap<string>;
    isArchived: boolean;
    disablePrimaryButton: boolean;
    contextMenuType = ContextMenuType;

    private isEditMode: boolean;
    private destroy$ = new Subject();

    constructor(
        @Inject(StoreToken) private store: IStore,
        @Inject("TemplateAction") private templateAction: TemplateAction,
        @Inject("TemplateAPI") private templateAPI: TemplateAPI,
        private stateService: StateService,
        private permissionsService: Permissions,
        private translatePipe: TranslatePipe,
        private $transitions: TransitionService,
        private notificationService: NotificationService,
        private otModalService: OtModalService,
        private templateCreateHelperService: TemplateCreateHelperService,
        private templateDetailsService: TemplateDetailsService,
        private templateDetailsHelperService: TemplateDetailsHelperService,
    ) {
    }

    ngOnInit(): void {
        this.translations = {
            cancelButtonText: this.translatePipe.transform("Cancel"),
            confirmButtonText: this.translatePipe.transform("Confirm"),
            discardChanges: this.translatePipe.transform("DiscardChanges"),
            discardDraft: this.translatePipe.transform("DiscardDraft"),
            discardTemplate: this.translatePipe.transform("DiscardTemplate"),
            createNewVersion: this.translatePipe.transform("CreateNewVersion"),
            editThisVersion: this.translatePipe.transform("EditThisVersion"),
            invalidTemplate: this.translatePipe.transform("InvalidTemplate"),
            publish: this.translatePipe.transform("Publish"),
            publishTemplate: this.translatePipe.transform("PublishTemplate"),
            publishTemplateModalDesc: this.translatePipe.transform("PublishTemplateModalDesc"),
            showVersions: this.translatePipe.transform("ShowVersions"),
            hideVersions: this.translatePipe.transform("HideVersions"),
            copyTemplate: this.translatePipe.transform("CopyTemplate"),
            newVersionCannotBeCreated: this.translatePipe.transform("NewVersionCannotBeCreated"),
            duplicateTemplateName: this.translatePipe.transform("DuplicateTemplateName"),
            doYouWantToContinue: this.translatePipe.transform("DoYouWantToContinue"),
            TemplateIsInactive: this.translatePipe.transform("TemplateIsInactive"),
            ErrorUpdatingTemplate: this.translatePipe.transform("ErrorUpdatingTemplate"),
        };

        this.permissions = {
            TemplatesCreateAndPublish: this.permissionsService.canShow("TemplatesCreateAndPublish"),
            TemplatesDiscardDraft: this.permissionsService.canShow("TemplatesDiscardDraft"),
            TemplatesEditPublished: this.permissionsService.canShow("TemplatesEditPublished"),
            TemplatesCopy: this.permissionsService.canShow("TemplatesCopy"),
            TemplateLock: this.permissionsService.canShow("TemplatesLock"),
        };

        this.storeSubscription = observableFromStore(this.store).pipe(
            startWith(this.store.getState()),
        ).subscribe((state: IStoreState): void => this.componentWillReceiveState(state));

        this.setHeaderOptions();
        this.setTransitionAwayHook();
    }

    ngOnDestroy(): void {
        if (this.transitionDeregisterHook) {
            this.transitionDeregisterHook();
        }
        if (this.storeSubscription) {
            this.storeSubscription.unsubscribe();
        }
        this.destroy$.next();
        this.destroy$.complete();
    }

    goToRoute(route: { params: any, path: string }): void {
        this.stateService.go(route.path);
    }

    onPrimaryActionClick(buttonId: string): void {
        switch (buttonId) {
            case TemplateHeaderActionButtons.SaveUpdates:
                this.savePublishedTemplateDetails();
                break;
            case TemplateHeaderActionButtons.Publish:
                this.onPublishTemplate();
                break;
            case TemplateHeaderActionButtons.EditThisVersion:
                this.setTemplateEditMode(TemplateModes.Edit);
                break;
        }
    }

    onSecondaryActionClick(buttonId: string): void {
        switch (buttonId) {
            case TemplateHeaderActionButtons.Cancel:
                this.resetTemplateMode();
                break;
            case TemplateHeaderActionButtons.DiscardDraft:
                this.discardDraftedTemplate();
                break;
            case TemplateHeaderActionButtons.CreateNewVersion:
                this.primaryButtonObj.isDisabled = true;
                this.secondaryButtonObj.isDisabled = true;
                this.createNewTemplateVersion();
                break;
        }
    }

    createOtAutoId(buttonObjText: string): string {
        return "Template".concat(buttonObjText.split(" ").join(""));
    }

    toggleContextMenu(value: boolean): void {
        this.canShowContextMenuOptions = value;
    }

    unLockTemplate(): void {
        this.primaryButtonObj.isLoading = true;
        this.primaryButtonObj.isDisabled = true;
        this.lockTemplate(false);
        this.store.dispatch({ type: TemplateActions.DISABLE_HEADER_ACTIONS, disableHeaderActions: true });
        this.store.dispatch({ type: TemplateActions.UPDATE_TEMPLATE_MODE, templateMode: TemplateModes.Edit });
    }

    private setTransitionAwayHook(): void {
        this.transitionDeregisterHook = this.$transitions.onBefore({ exiting: "zen.app.pia.module.templatesv2.studio.manage" }, (): any => {
            if (this.mode === TemplateModes.Edit) {
                const transitionPromise = new Promise<boolean>((resolve, reject) => {
                    this.templateDetailsHelperService.openSaveUnSavedChangesModal(this.templateId, this.rootVersionId, this.status)
                        .subscribe((saveChanges: boolean | undefined) => {
                            if (saveChanges === true) {
                                resolve(true);
                            } else if (saveChanges === false) {
                                this.resetTemplateMode();
                                resolve(true);
                            } else {
                                resolve(false);
                            }
                        });
                });

                return transitionPromise;
            }
            return true;
        });
    }

    private componentWillReceiveState(state: IStoreState): void {
        const builderView: ITemplateBuilderState = getBuilderView(state);
        const sections: string[] = builderView.sectionAllIds;

        if (isArray(sections) && sections.length > 0 && Object.keys(builderView.questionById).length > 0) {
            this.isPublishable = true;
        } else {
            this.isPublishable = false;
        }

        const template: IAssessmentTemplateDetail = getCurrentTemplate(state);
        [
            this.name,
            this.version,
            this.status,
            this.templateId,
            this.templateType,
            this.mode,
            this.rootVersionId,
            this.templateHasExistingDraft,
        ] = [
                template.name,
                template.templateVersion,
                template.status,
                template.id,
                template.templateType,
                getTemplateState(state).templateMode,
                template.rootVersionId,
                template.isDraft,
            ];
        this.isTemplateLocked = template.isLocked;
        this.isTemplateActive = template.isActive;
        this.updateActionButtonsConfig();
        this.configureBreadCrumb();
        this.isDrawerOpen = getTemplateVersionHistoryDrawerState(state);
        this.headerDisabled = getTemplateState(state).disableHeaderActions;
        this.setHeaderOptions();
        this.isArchived = template.status === TemplateStates.ARCHIVE;
        this.isEditMode = getTemplateState(state).templateMode === TemplateModes.Edit;
        this.disablePrimaryButton = !isTemplateModelValid(this.store.getState()) && this.primaryButtonObj.buttonId !== TemplateHeaderActionButtons.EditThisVersion || (this.primaryButtonObj.buttonId === TemplateHeaderActionButtons.Publish && this.isEditMode);
    }

    private discardDraftedTemplate(): void {
        this.otModalService
            .confirm({
                type: ConfirmModalType.DELETE,
                translations: {
                    title: this.translations.discardTemplate,
                    desc: this.translatePipe.transform("SureToDiscardTemplateDraft", { TemplateDraftName: this.name }),
                    confirm: "",
                    submit: this.translations.confirmButtonText,
                    cancel: this.translations.cancelButtonText,
                },
            }).pipe(
                takeUntil(this.destroy$),
            ).subscribe((data: boolean) => {
                if (data) {
                    this.templateAction.discardDraftedTemplate(this.templateId, this.rootVersionId)
                        .then((result: boolean): boolean => {
                            if (result) {
                                this.navigateToTemplatesPage();
                            }
                            return result;
                        });
                }
            });
    }

    private createNewTemplateVersion(): void {
        this.templateAPI.createNewTemplateVersion(this.rootVersionId)
            .then((response: IProtocolResponse<ICreateTemplateVersionResponse>): void => {
                if (response.result) {
                    this.stateService.transitionTo("zen.app.pia.module.templatesv2.studio.manage", { templateId: response.data.rootVersionId, version: response.data.templateVersion });
                } else {
                    if (response.errorCode === AssessmentTemplateErrorCodes.TEMPLATE_INACTIVE) {
                        this.notificationService.alertError(this.translatePipe.transform("Error"), this.translations.TemplateIsInactive);
                        this.navigateToTemplatesPage();
                    } else {
                        this.notificationService.alertError(this.translatePipe.transform("Error"), this.translations.ErrorUpdatingTemplate);
                    }
                }
            });
    }

    private setTemplateEditMode(mode: TemplateModes): void {
        this.store.dispatch({ type: TemplateActions.UPDATE_TEMPLATE_MODE, templateMode: mode });
    }

    private onPublishTemplate() {
        this.loadingAssessmentCount = true;
        this.templateAPI.getOpenAssessmentCount(this.rootVersionId)
            .then((openAssessmentCount: number) => {
                this.loadingAssessmentCount = false;
                if (openAssessmentCount) {
                    this.onPublishTemplateWithOpenAssessments(openAssessmentCount);
                    return;
                }
                this.onPublishTemplateWithoutOpenAssessments();
            });
    }

    private onPublishTemplateWithoutOpenAssessments(): void {
        this.otModalService
            .confirm({
                type: ConfirmModalType.SUCCESS,
                translations: {
                    title: this.translations.publishTemplate,
                    desc: this.translations.publishTemplateModalDesc,
                    confirm: this.translations.doYouWantToContinue,
                    submit: this.translations.confirmButtonText,
                    cancel: this.translations.cancelButtonText,
                },
            }).pipe(
                takeUntil(this.destroy$),
            ).subscribe((data: boolean) => {
                if (data) {
                    this.publishTemplate();
                }
            });
    }

    private publishTemplate(): ng.IPromise<boolean> {
        const payload: IPublishTemplatePayload = this.openAssessmentPayload.pushTemplateChanges ? this.openAssessmentPayload : undefined;
        return this.templateAPI.publishTemplate(this.templateId, payload)
            .then((response: IProtocolResponse<void>): boolean => {
                if (response.result) {
                    this.navigateToTemplatesPage();
                } else {
                    if (response.errorCode === AssessmentTemplateErrorCodes.TEMPLATE_INVALID) {
                        this.notificationService.alertError(this.translatePipe.transform("Error"), this.translations.invalidTemplate);
                        this.templateAction.setTemplateBuilderState(this.templateId);
                    }
                }
                return true;
            }).finally(() => {
                this.openAssessmentPayload = {
                    pushTemplateChanges: false,
                    notifyRespondents: false,
                };
            });
    }

    private onPublishTemplateWithOpenAssessments(openAssessmentCount: number) {
        this.otModalService
            .create(
                AaTemplateWithAssessmentPublishModal,
                {
                    isComponent: true,
                },
                {
                    ...this.openAssessmentPayload,
                    count: openAssessmentCount,
                },
            ).subscribe((data: IPublishTemplateModalData) => {
                if (data) {
                    this.openAssessmentPayload.pushTemplateChanges = data.pushTemplateChanges;
                    this.openAssessmentPayload.notifyRespondents = data.notifyRespondents;
                    this.publishTemplate();
                }
            });
    }

    private resetTemplateMode(): void {
        const inventoryRelations = getTemplateTempModel(this.store.getState()).inventoryRelations;
        const questionIds: string[] = inventoryRelations.map((relation: IInventoryRelationshipContract) => relation.destinationInventoryQuestionId);

        this.store.dispatch({ type: TemplateActions.RESET_TEMP_MODEL_WITH_CURRENT, questionIds });
    }

    private setDraftTemplateButtons = (): void => {
        if (this.isPublishable) {
            this.primaryButtonObj = {
                buttonId: TemplateHeaderActionButtons.Publish,
                text: this.translations.publish,
                isVisible: this.permissions.TemplatesCreateAndPublish,
            };
        } else {
            this.primaryButtonObj = {
                buttonId: null,
                text: null,
                isVisible: false,
            };
        }

        this.secondaryButtonObj = {
            buttonId: TemplateHeaderActionButtons.DiscardDraft,
            text: this.translations.discardDraft,
            isVisible: this.permissions.TemplatesDiscardDraft,
        };
    }

    private setPublishedStateButtons(): void {
        this.primaryButtonObj = {
            buttonId: TemplateHeaderActionButtons.EditThisVersion,
            text: this.translations.editThisVersion,
            isVisible: this.permissions.TemplatesEditPublished,
        };
        this.secondaryButtonObj = {
            buttonId: TemplateHeaderActionButtons.CreateNewVersion,
            text: this.translations.createNewVersion,
            isVisible: this.status === TemplateStates.PUBLISHED.toUpperCase(),
            isDisabled: this.templateHasExistingDraft,
            toolTip: this.templateHasExistingDraft ? this.translations.newVersionCannotBeCreated : "",
        };
    }

    private setSaveChangesButtons(): void {
        this.primaryButtonObj = this.primaryButtonObj = {
            buttonId: TemplateHeaderActionButtons.SaveUpdates,
            text: this.translatePipe.transform("SaveUpdates"),
            isVisible: this.permissions.TemplatesEditPublished,
            isDisabled: this.disablePrimaryButton,
        };
        this.secondaryButtonObj = {
            buttonId: TemplateHeaderActionButtons.Cancel,
            text: this.translations.discardChanges,
            isVisible: true,
        };
    }

    private updateActionButtonsConfig(): void {
        switch (this.status) {
            case TemplateStates.DRAFT.toUpperCase():
                this.setDraftTemplateButtons();
                break;
            case TemplateStates.PUBLISHED.toUpperCase():
                if (this.mode === TemplateModes.Edit) {
                    this.setSaveChangesButtons();
                } else {
                    this.setPublishedStateButtons();
                }
                break;
        }
    }

    private onToggleVersionsClick(): void {
        this.store.dispatch({ type: TemplateActions.TEMPLATE_VERSION_HISTORY_PANE_TOGGLE, isTemplateVersionHistoryPaneOpen: this.isDrawerOpen });
    }

    private configureBreadCrumb(): void {
        this.breadCrumb = { stateName: "", text: "" };
        switch (this.templateType.toUpperCase()) {
            case TemplateTypes.VENDOR:
                this.breadCrumb.stateName = "zen.app.pia.module.vendor.templates";
                this.breadCrumb.text = this.translatePipe.transform("Vendor");
                this.breadCrumb.identifier = "VendorLandingPageNavigation";
                break;
            case TemplateTypes.PIA:
            default:
                this.breadCrumb.stateName = "zen.app.pia.module.templatesv2.list";
                this.breadCrumb.text = this.translatePipe.transform("Questionnaire");
                this.breadCrumb.identifier = "TemplateLandingPageNavigation";
                break;
        }
    }

    private copyTemplateModal(): void {
        if (this.mode === TemplateModes.Edit) {
            const currentTemplate: IAssessmentTemplateDetail = getCurrentTemplate(this.store.getState());
            this.templateDetailsHelperService.openSaveUnSavedChangesModal(currentTemplate.id, currentTemplate.rootVersionId, currentTemplate.status)
                .pipe(takeUntil(this.destroy$))
                .subscribe((data: boolean | undefined) => {
                    if (typeof data === "boolean") {
                        this.templateCreateHelperService.openCopyTemplateModal();
                        return;
                    }
                });
        } else {
            this.templateCreateHelperService.openCopyTemplateModal();
        }
    }

    private navigateToTemplatesPage() {
        switch (this.templateType.toUpperCase()) {
            case TemplateTypes.VENDOR:
                this.stateService.go("zen.app.pia.module.vendor.templates");
                break;
            case TemplateTypes.PIA:
            default:
                this.stateService.go("zen.app.pia.module.templatesv2.list");
                break;
        }
    }

    private setHeaderOptions(): void {
        this.headerOptions = [
            {
                text: this.translatePipe.transform("ShowHideVersions"),
                action: (): void => {
                    this.onToggleVersionsClick();
                },
            },
        ];

        if (this.permissions.TemplatesCopy && this.isTemplateActive) {
            this.headerOptions.push(
                {
                    text: this.translatePipe.transform("CopyTemplate"),
                    action: (): void => {
                        this.copyTemplateModal();
                    },
                },
            );
        }

        if (this.permissions.TemplateLock) {
            if (!this.isTemplateLocked) {
                this.headerOptions.push(
                    {
                        id: "lock",
                        text: this.translatePipe.transform("LockTemplate"),
                        action: (): void => {
                            this.lockTemplate(true);
                        },
                    },
                );
            }
        }
    }

    private lockTemplate(locked: boolean): void {
        this.templateDetailsService.lockTemplate(locked, true)
            .subscribe((data: boolean) => {
                if (typeof data === "boolean") {
                    this.primaryButtonObj.isDisabled = false;
                    this.primaryButtonObj.isLoading = false;
                    this.store.dispatch({ type: TemplateActions.LOCK_TEMPLATE, isLocked: locked });
                }
                this.store.dispatch({ type: TemplateActions.DISABLE_HEADER_ACTIONS, disableHeaderActions: false });
            });
    }

    private savePublishedTemplateDetails(): void {
        this.templateDetailsHelperService.openSavePublishedTemplateConfirmModal()
            .pipe(
                takeUntil(this.destroy$),
            )
            .subscribe((data: boolean) => {
                if (data) {
                    this.templateDetailsService.updatePublishedTemplate(this.templateId)
                        .then((result) => {
                            this.name = getCurrentTemplate(this.store.getState()).name;
                            this.setPublishedStateButtons();
                            this.resetTemplateMode();

                        });
                }
            });
    }
}
