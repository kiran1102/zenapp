// Angular
import {
    Component,
    Input,
    Inject,
    OnChanges,
    SimpleChanges,
    TemplateRef,
    OnDestroy,
} from "@angular/core";
import { StateService } from "@uirouter/core";

// 3rd party
import {
    sortBy,
} from "lodash";

// Rxjs
import { Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";

// enums
import { TemplateStates } from "enums/template-states.enum";

// redux
import { StoreToken } from "tokens/redux-store.token";
import { getUserById } from "oneRedux/reducers/user.reducer";
import {
    TemplateActions,
} from "oneRedux/reducers/template.reducer";

// interfaces
import { IStore } from "interfaces/redux.interface";
import {
    ITemplateVersion,
    IVersionHistoryModel,
} from "modules/template/interfaces/template.interface";
import { ConfirmModalType } from "@onetrust/vitreus";

// service
import TemplateAction from "templateServices/actions/template-action.service";
import { OtModalService } from "@onetrust/vitreus";

// pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

@Component({
    selector: "template-version-pane",
    templateUrl: "./template-version-pane.component.html",
})

export class TemplateVersionPaneComponent implements OnChanges, OnDestroy {
    @Input() isDrawerOpen;
    @Input() rootVersionId;
    @Input() currentVersion;
    @Input() isTemplateLocked;
    @Input() isTemplateActive: boolean;

    templateStates = TemplateStates;
    versionGroups: IVersionHistoryModel[];
    versionString = this.translatePipe.transform("Version") + " ";

    private destroy$ = new Subject();

    constructor(
        private stateService: StateService,
        @Inject("TemplateAction") private templateAction: TemplateAction,
        @Inject(StoreToken) private store: IStore,
        private translatePipe: TranslatePipe,
        private otModalService: OtModalService,
    ) { }

    ngOnChanges(changes: SimpleChanges): void {
        this.getVersionHistory();
    }

    ngOnDestroy(): void {
        this.destroy$.next();
        this.destroy$.complete();
    }

    getOneTagLabelClass(status: string): string {
        const labelClass = (status === this.templateStates.Published) ? "success" : "";
        return labelClass;
    }

    goToVersion(templateVersion: number, rootVersionId: string): void {
        this.stateService.go("zen.app.pia.module.templatesv2.studio.manage", { templateId: rootVersionId, version: templateVersion });
    }

    deleteDraft(event: Event, versionListItem: ITemplateVersion): void {
        event.stopPropagation();
        this.otModalService
            .confirm({
                type: ConfirmModalType.DELETE,
                translations: {
                    title: this.translatePipe.transform("DiscardTemplate"),
                    desc: this.translatePipe.transform("SureToDiscardTemplateDraft", { TemplateDraftName: versionListItem.name }),
                    confirm: null,
                    submit: this.translatePipe.transform("Confirm"),
                    cancel: this.translatePipe.transform("Cancel"),
                },
            }).pipe(
                takeUntil(this.destroy$),
            ).subscribe((data: boolean) => {
                if (data) {
                    this.templateAction.discardDraftedTemplate(versionListItem.id, versionListItem.rootVersionId)
                    .then((result: boolean): boolean => {
                        if (result) {
                            if (versionListItem.templateVersion === this.currentVersion) {
                                this.goToRoute(versionListItem.templateVersion, versionListItem.rootVersionId);
                            } else {
                                this.reloadVersionHistoryPane();
                            }
                        }
                        return result;
                    });
                }
            });
    }

    getUser(userId: string): string {
        const user = getUserById(userId)(this.store.getState());
        return user ? user.FullName : "";
    }

    getOneTagLabel(status) {
        return status === this.templateStates.Draft ? this.translatePipe.transform("Draft") : this.translatePipe.transform("Published");
    }

    trackByVersionListId(index, versionList) {
        return versionList.id;
    }

    closeVersionPane(): void {
        this.store.dispatch({ type: TemplateActions.TEMPLATE_VERSION_HISTORY_PANE_TOGGLE, isTemplateVersionHistoryPaneOpen: this.isDrawerOpen });
     }

    private getVersionHistory(): void {
        if (this.isDrawerOpen && this.rootVersionId && (!this.versionGroups)) {
            this.templateAction.getTemplateVersionList(this.rootVersionId)
                .then((response: ITemplateVersion[]) => {
                    this.versionGroups = this.splitVersionGroups(response);
                });
        }
    }

    private reloadVersionHistoryPane(): void {
        this.versionGroups = null;
        this.getVersionHistory();
    }

    private splitVersionGroups(list: ITemplateVersion[]): IVersionHistoryModel[] {
        const sortedList: ITemplateVersion[] = sortBy(list, "templateVersion").reverse();
        let splitter = 1;
        if (sortedList[0].status === this.templateStates.Draft) {
            splitter = 2;
        }

        const versionGroups: IVersionHistoryModel[] = [
            { title: this.translatePipe.transform("CurrentVersions"), versionList: sortedList.slice(0, splitter) },
            { title: this.translatePipe.transform("PreviousVersions"), versionList: sortedList.slice(splitter) },
        ];
        return versionGroups;
    }

    private goToRoute(templateVersion: number, rootVersionId: string): void {
        if (templateVersion > 1) {
            this.stateService.go("zen.app.pia.module.templatesv2.studio.manage", { templateId: rootVersionId, version: templateVersion - 1 });
        } else {
            this.stateService.go("zen.app.pia.module.templatesv2.list");
        }
    }
}
