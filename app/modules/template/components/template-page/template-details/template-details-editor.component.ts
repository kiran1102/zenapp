// Angular
import {
    Component,
    OnInit,
    Inject,
    Input,
    OnDestroy,
} from "@angular/core";

// Rxjs
import {
    Subject,
    BehaviorSubject,
    Observable,
} from "rxjs";
import {
    startWith,
    debounceTime,
    takeUntil,
} from "rxjs/operators";

// Interfaces
import {
    IAssessmentTemplateDetail,
    IAssessmentTemplateDetailUpdateParams,
} from "interfaces/assessment-template-detail.interface";
import {
    IStore,
    IStoreState,
} from "interfaces/redux.interface";
import { ITemplateSettings } from "modules/template/interfaces/template.interface";
import {
    ITemplateUpdateSettings,
    ITemplateUpdateRequest,
    ITemplateUpdateMetadata,
} from "modules/template/interfaces/template-update-request.interface";
import { IInventoryRelationshipContract } from "modules/template/interfaces/template-related-inventory.interface";

// Services
import OTIconService from "templateServices/icon.service";
import { Permissions } from "modules/shared/services/helper/permissions.service";
import { TemplateDetailsService } from "modules/template/services/template-details.service";

// Enums
import { TemplateModes } from "enums/template-modes.enum";

// Constants
import { TemplateStates } from "constants/template-states.constant";

// Redux
import observableFromStore from "oneRedux/utils/observable-from-store";
import {
    TemplateActions,
    getCurrentTemplate,
    getTemplateState,
    getTemplateTempModel,
    isTemplateModelValid,
} from "oneRedux/reducers/template.reducer";
import { TranslatePipe } from "modules/pipes/translate.pipe";
import { StoreToken } from "tokens/redux-store.token";
import {
    getCurrentOrgId,
    getOrgById,
} from "oneRedux/reducers/org.reducer";

@Component({
    selector: "template-detail-editor",
    templateUrl: "./template-detail-editor.component.html",
})

export class TemplateDetailsEditorComponent implements OnInit, OnDestroy {
    @Input() templateId: string;
    @Input() templateSettings: ITemplateSettings;

    isTemplateActive: boolean;
    isIconDisabled: boolean;
    selectedOrgName: string;
    showFooter: boolean;
    iconList: any[] = [];
    isDetailsExpanded = true;
    isOptionsExpanded = true;
    isEditMode = false;
    isSaveInProgress$: Observable<boolean>;
    originalTemplate: IAssessmentTemplateDetail;
    isReadonly: boolean;
    isTemplateValid: boolean;

    translations = {
        description: this.translatePipe.transform("Description"),
        details: this.translatePipe.transform("Details"),
        enterDescription: this.translatePipe.transform("EnterDescription"),
        enterFriendlyName: this.translatePipe.transform("EnterFriendlyName"),
        enterName: this.translatePipe.transform("EnterName"),
        friendlyName: this.translatePipe.transform("FriendlyName"),
        name: this.translatePipe.transform("Name"),
        templateFriendlyNameTooltip: this.translatePipe.transform("TemplateFriendlyNameTooltip"),
        templateIcon: this.translatePipe.transform("TemplateIcon"),
    };

    permissions = {
        TemplatesEditPublished: this.permissionsService.canShow("TemplatesEditPublished"),
    };

    private template: ITemplateUpdateMetadata;
    private isSaveInProgress = new BehaviorSubject(false);
    private modelSource = new Subject<ITemplateUpdateMetadata>();
    private modelObservable = this.modelSource.asObservable();
    private currentOrgId = getCurrentOrgId(this.store.getState());
    private settings: ITemplateUpdateSettings = {
        prepopulateAttributeQuestions: false,
    };

    private destroy$ = new Subject();

    constructor(
        @Inject(StoreToken) private store: IStore,
        @Inject("OTIconService") private otIconService: OTIconService,
        private translatePipe: TranslatePipe,
        private permissionsService: Permissions,
        private templateDetailsService: TemplateDetailsService,
    ) {
    }

    ngOnInit(): void {
        this.isSaveInProgress$ = this.isSaveInProgress.asObservable();
        this.iconList = this.otIconService.getIconList();

        const storeState = this.store.getState();
        this.originalTemplate = getCurrentTemplate(storeState);
        this.isReadonly = getTemplateState(storeState).templateMode === TemplateModes.Readonly;
        if (this.isReadonly) {
            this.template = {
                name: this.originalTemplate.name,
                friendlyName: this.originalTemplate.friendlyName,
                description: this.originalTemplate.description,
                icon: this.originalTemplate.icon,
                orgGroupId: this.originalTemplate.orgGroupId || getOrgById(this.currentOrgId)(this.store.getState()).Id,
            };
            this.settings.prepopulateAttributeQuestions = this.originalTemplate.prepopulateAttributeQuestions;
        } else {
            const templateTempModel: ITemplateUpdateRequest = getTemplateTempModel(storeState);
            this.template = templateTempModel.metadata;
            this.settings = templateTempModel.settings;
        }

        if (this.template.orgGroupId) {
            this.selectedOrgName = getOrgById(this.template.orgGroupId)(storeState).Name;
        }

        observableFromStore(this.store).pipe(
            startWith(this.store.getState()),
            takeUntil(this.destroy$),
        ).subscribe((state: IStoreState): void => this.componentWillReceiveState(state));

        this.modelObservable
            .pipe(
                debounceTime(400),
                takeUntil(this.destroy$),
            ).subscribe((model: ITemplateUpdateMetadata) => {
                this.template = model;
                const template = {
                    metadata: {
                        ...this.template,
                    },
                    settings: {
                        prepopulateAttributeQuestions: this.settings.prepopulateAttributeQuestions,
                    },
                    welcomeText: {
                        welcomeText: this.originalTemplate.welcomeText,
                    },
                };
                this.store.dispatch({ type: TemplateActions.UPDATE_TEMP_MODEL_METADATA_AND_SETTINGS, template });
                this.store.dispatch({ type: TemplateActions.UPDATE_TEMPLATE_MODE, templateMode: TemplateModes.Edit });

            });
    }

    ngOnDestroy() {
        this.destroy$.next();
        this.destroy$.complete();
    }

    setEditMode() {
        if (this.isTemplateActive && !this.originalTemplate.isLocked) {
            this.store.dispatch({ type: TemplateActions.UPDATE_TEMPLATE_MODE, templateMode: TemplateModes.Edit });
        }
    }

    iconChanged(icon: string, prop: string): void {
        if (this.isIconDisabled) return;
        this.template.icon = icon;
        this.setEditMode();
        this.saveModelChange(prop, icon);
    }

    selectOrg(prop: string, orgGroupId: string): void {
        this.template.orgGroupId = orgGroupId || "";
        this.saveModelChange(prop, this.template.orgGroupId);
        if (!this.template.orgGroupId) return;
        this.selectedOrgName = getOrgById(this.template.orgGroupId)(this.store.getState()).Name;
    }

    saveModelChange(prop: string, payload: string): void {
        const request: ITemplateUpdateMetadata = {
            ...this.template,
            [prop]: payload,
        };

        this.modelSource.next(request);
    }

    saveTemplateDetails(): void {
        if (this.template.name) {
            this.isSaveInProgress.next(true);
            const templatePayload: IAssessmentTemplateDetailUpdateParams = {
                metadata: {
                    name: this.template.name,
                    orgGroupId: this.template.orgGroupId,
                    friendlyName: this.template.friendlyName,
                    description: this.template.description,
                    icon: this.template.icon,
                },
                settings: {
                    prepopulateAttributeQuestions: this.settings.prepopulateAttributeQuestions,
                },
            };

            if (this.originalTemplate.status === TemplateStates.DRAFT.toUpperCase()) {
                this.templateDetailsService.updateDraftTemplateDetails(templatePayload)
                    .pipe(
                        debounceTime(400),
                        takeUntil(this.destroy$),
                    ).subscribe((result): void => {
                        if (result) {
                            this.store.dispatch({ type: TemplateActions.UPDATE_TEMPLATE_SETTINGS, settings: this.settings });
                        }
                        this.isSaveInProgress.next(false);
                    });
            }
        }
    }

    updateAttributeQuestionSettings(prepopulateAttrQuestions: boolean): void {
        this.settings.prepopulateAttributeQuestions = prepopulateAttrQuestions;
        this.modelSource.next(this.template);
    }

    discardChanges(): void {
        const inventoryRelations = getTemplateTempModel(this.store.getState()).inventoryRelations;
        const questionIds: string[] = inventoryRelations.map((relation: IInventoryRelationshipContract) => relation.destinationInventoryQuestionId);

        this.store.dispatch({ type: TemplateActions.RESET_TEMP_MODEL_WITH_CURRENT, questionIds });
    }

    private componentWillReceiveState(state: IStoreState): void {
        const tempModel = getTemplateTempModel(state);
        this.originalTemplate = getCurrentTemplate(state);
        this.template = {
            name: tempModel.metadata.name,
            friendlyName: tempModel.metadata.friendlyName,
            description: tempModel.metadata.description,
            icon: tempModel.metadata.icon,
            orgGroupId: tempModel.metadata.orgGroupId || getOrgById(this.currentOrgId)(this.store.getState()).Id,
        };
        this.settings.prepopulateAttributeQuestions = tempModel.settings.prepopulateAttributeQuestions;
        this.isReadonly = getTemplateState(state).templateMode === TemplateModes.Readonly;
        this.isEditMode = getTemplateState(state).templateMode === TemplateModes.Edit;
        this.isTemplateValid = isTemplateModelValid(state);
        this.isTemplateActive = this.originalTemplate.isActive;
        this.showFooter = this.isEditMode && this.isTemplateActive && !this.originalTemplate.isLocked && !this.isReadonly && (this.originalTemplate.status === TemplateStates.DRAFT.toLocaleUpperCase());
        this.isIconDisabled = !this.isTemplateActive || this.originalTemplate.isLocked;
    }
}
