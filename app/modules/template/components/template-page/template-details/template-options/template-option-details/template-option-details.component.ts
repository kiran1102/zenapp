// External Libraries
import {
    Component,
    EventEmitter,
    Inject,
    Input,
    Output,
    OnInit,
} from "@angular/core";
import { forEach } from "lodash";

// Services
import { TemplateBuilderService } from "templateServices/template-builder.service";

// Interfaces
import { IInventoryQuestionOption } from "interfaces/assessment-template-question.interface";
import { IAssessmentTemplateOptionSettingsDetails } from "interfaces/assessment-template-detail.interface";
import { IStore } from "interfaces/redux.interface";

// Redux
import { StoreToken } from "tokens/redux-store.token";

@Component({
    selector: "template-option-details",
    templateUrl: "./template-option-details.component.html",
})

export class TemplateOptionDetailsComponent implements OnInit {

    @Input() identifier: string;
    @Input() isOptionDisabled: boolean;
    @Input() inventoryType: string;
    @Input() option: IAssessmentTemplateOptionSettingsDetails;
    @Input() optionValue: boolean | string;

    @Output() public inventoryQuestionChange = new EventEmitter();
    @Output() public toggleStateChanged = new EventEmitter();

    inventoryQuestionOption: IInventoryQuestionOption;
    inventoryQuestionOptions: IInventoryQuestionOption[];

    constructor(
        @Inject("TemplateBuilderService") private TemplateBuilder: TemplateBuilderService,
        @Inject(StoreToken) private store: IStore) { }

    public ngOnInit(): void {
        this.inventoryQuestionOptions = this.TemplateBuilder.getAllInventoryQuestions(this.inventoryType);
        if (this.optionValue && typeof this.optionValue !== "boolean") {
            forEach(this.inventoryQuestionOptions, (inventoryQuestionOption: IInventoryQuestionOption): void => {
                if (inventoryQuestionOption.id === this.optionValue) {
                    this.inventoryQuestionOption = inventoryQuestionOption;
                    return;
                }
            });
        }
    }

    public toggleStateChangeCallback(settingValue: boolean) {
        this.toggleStateChanged.emit(settingValue);
    }
}
