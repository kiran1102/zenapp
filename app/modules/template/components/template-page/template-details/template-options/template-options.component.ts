// Angular
import {
    Component,
    OnInit,
    OnDestroy,
    Inject,
    Input,
    Output,
    EventEmitter,
} from "@angular/core";

// Rxjs
import { Subject } from "rxjs";
import {
    startWith,
    takeUntil,
} from "rxjs/operators";

// Enums
import { TemplateStates } from "enums/template-states.enum";

// Constants
import { InventorySchemaIds } from "constants/inventory-config.constant";

// Interfaces
import {
    IAssessmentTemplateDetail,
    IAssessmentTemplateOptionSettingsInitParams,
} from "interfaces/assessment-template-detail.interface";
import {
    IStore,
    IStoreState,
} from "interfaces/redux.interface";
import { ITemplateUpdateSettings } from "modules/template/interfaces/template-update-request.interface";

// Redux
import {
    getCurrentTemplate,
    getTemplateTempModel,
} from "oneRedux/reducers/template.reducer";
import observableFromStore from "oneRedux/utils/observable-from-store";
import { StoreToken } from "tokens/redux-store.token";
import { TranslatePipe } from "modules/pipes/translate.pipe";

@Component({
    selector: "template-options",
    templateUrl: "./template-options.component.html",
})

export class TemplateOptionsComponent implements OnInit, OnDestroy {

    @Input() isReadonly: boolean;

    @Output() updateAttributeQuestionSettings: EventEmitter<boolean> = new EventEmitter<boolean>();

    inventorySchemaIds = InventorySchemaIds;
    isTemplateActive: boolean;
    isTemplateLocked: boolean;
    template: IAssessmentTemplateDetail;
    templateStates = TemplateStates;
    options: IAssessmentTemplateOptionSettingsInitParams;
    settings: ITemplateUpdateSettings;

    private destroy$ = new Subject();

    constructor(
        @Inject(StoreToken) private store: IStore,
        private translatePipe: TranslatePipe,
    ) { }

    ngOnInit(): void {
        this.options = this.getOptions();
        observableFromStore(this.store).pipe(
            startWith(this.store.getState()),
            takeUntil(this.destroy$),
        ).subscribe((state: IStoreState): void => this.receiveState(state));
    }

    ngOnDestroy(): void {
        this.destroy$.next();
        this.destroy$.complete();
    }

    toggleStateChanged(optionValue: boolean): void {
        this.updateAttributeQuestionSettings.emit(optionValue);
    }

    private getOptions = (): IAssessmentTemplateOptionSettingsInitParams => {
        return {
            prepopulateAttributeQuestions: {
                title: this.translatePipe.transform("PrepopulateAttributeQuestions"),
                description: this.translatePipe.transform("PrepopulateAttributeQuestionsDescription"),
            },
        };
    }

    private receiveState = (state: IStoreState): void => {
        this.settings = getTemplateTempModel(state).settings;
        this.template = getCurrentTemplate(state);
        this.isTemplateLocked = this.template.isLocked;
        this.isTemplateActive = this.template.isActive;
    }
}
