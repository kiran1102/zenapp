// Angular
import {
    Component,
    OnInit,
    Inject,
} from "@angular/core";
import { StateService } from "@uirouter/core";

// 3rd party
import { Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";

// Constants // Enums
import { TranslationModules } from "constants/translation.constant";
import { StatusCodes } from "enums/status-codes.enum";
import { TemplateTabOptions } from "constants/template-tab-options.constant";
import { TemplateModes } from "enums/template-modes.enum";
import { TemplateStates } from "constants/template-states.constant";

// Interfaces
import { IStringMap } from "interfaces/generic.interface";
import { ITabsNav } from "interfaces/one-tabs-nav.interface";
import { ITemplateSettings } from "modules/template/interfaces/template.interface";
import { ITemplateState } from "interfaces/template-reducer.interface";
import { IStore } from "interfaces/redux.interface";
import { IAssessmentTemplateDetail } from "interfaces/assessment-template-detail.interface";

// Services
import TemplateAction from "templateServices/actions/template-action.service";
import { Permissions } from "modules/shared/services/helper/permissions.service";
import { TenantTranslationService } from "sharedServices/tenant-translation.service";
import { TemplateCreateService } from "modules/template/services/template-create.service";
import { TemplateDetailsHelperService } from "modules/template/services/template-details-helper.service";

// Redux
import { StoreToken } from "tokens/redux-store.token";
import {
    TemplateActions,
    getTemplateState,
} from "oneRedux/reducers/template.reducer";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

@Component({
    selector: "template-page",
    templateUrl: "./template-page.component.html",
})

export class TemplatePageComponent implements OnInit {

    isTemplateLoaded = false;
    currentTab = "";
    permissions: IStringMap<boolean>;
    tabOptions: ITabsNav[] = [];
    templateId: string;
    templateTabOptions: typeof TemplateTabOptions;
    templateSettings: ITemplateSettings;

    private version: number;
    private destroy$ = new Subject();

    constructor(
        @Inject(StoreToken) private store: IStore,
        @Inject("TemplateAction") private templateAction: TemplateAction,
        @Inject("TenantTranslationService") private tenantTranslationService: TenantTranslationService,
        private stateService: StateService,
        private translatePipe: TranslatePipe,
        private permissionsService: Permissions,
        private templateCreateService: TemplateCreateService,
        private templateDetailsHelperService: TemplateDetailsHelperService,
    ) { }

    ngOnInit(): void {
        this.tenantTranslationService.addModuleTranslations(TranslationModules.Inventory);
        this.templateTabOptions = TemplateTabOptions;
        this.tabOptions = this.getTabOptions();
        this.currentTab = this.tabOptions[1].id;
        this.templateId = this.stateService.params.templateId;
        this.version = parseInt(this.stateService.params.version, 10) || 1;

        this.templateAction
            .getDetailByVersion(this.templateId, this.version)
            .subscribe((response): void => {
                if (response.data) {
                    this.isTemplateLoaded = true;
                    this.templateId = response.data.id; // Update with the current templateId
                    // also get the builder data, to fetch section-question details
                    this.templateAction.setTemplateBuilderState(this.templateId);
                } else {
                    if (response.status === StatusCodes.Forbidden) {
                        this.permissionsService.goToFallback();
                    }
                }
            });

        this.permissions = {
            TemplatesView: this.permissionsService.canShow("TemplatesView"),
        };

        this.templateCreateService.getTemplateSettings()
            .pipe(
                takeUntil(this.destroy$),
            )
            .subscribe((response) => {
                if (response.result) {
                    this.templateSettings = {
                        allowAccessControl: response.data.allowAccessControl,
                        allowedAssessmentCreationOrgHierarchy: response.data.allowedAssessmentCreationOrgHierarchy,
                    };
                }
            });
    }

    private getTabOptions = (): ITabsNav[] => {
        return [{
            id: TemplateTabOptions.Details,
            text: this.translatePipe.transform("Details"),
            action: (option: ITabsNav): void => this.tabChanged(option),
        }, {
            id: TemplateTabOptions.Builder,
            text: this.translatePipe.transform("AssessmentTemplateBuilder"),
            action: (option: ITabsNav): void => this.tabChanged(option),
        }, {
            id: TemplateTabOptions.Rules,
            text: this.translatePipe.transform("Rules"),
            action: (option: ITabsNav): void => this.tabChanged(option),
        }];
    }

    private tabChanged = (option: ITabsNav): void => {
        if (this.currentTab !== option.id) {
            if (this.currentTab === TemplateTabOptions.Details) {
                this.showSaveChangesModal(option);
            } else {
                this.currentTab = option.id;
            }
        }
    }

    private showSaveChangesModal(option: ITabsNav) {
        const templateState: ITemplateState = getTemplateState(this.store.getState());
        if (templateState.templateMode === TemplateModes.Edit && templateState.current.status === TemplateStates.DRAFT.toUpperCase()) {
            const currentTemplate: IAssessmentTemplateDetail = templateState.current;
            this.templateDetailsHelperService.openSaveUnSavedChangesModal(currentTemplate.id, currentTemplate.rootVersionId, currentTemplate.status)
                .subscribe((data: boolean | undefined) => {
                    if (typeof data === "boolean") {
                        this.currentTab = option.id;
                        return;
                    }
                });
        } else {
            this.currentTab = option.id;
        }
    }
}
