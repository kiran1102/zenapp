// Rxjs
import { startWith } from "rxjs/operators";

// Angular
import {
    Component,
    OnInit,
    Inject,
} from "@angular/core";

// 3rd Party
import { getTemplateState } from "oneRedux/reducers/template.reducer";
import { Subscription } from "rxjs";
import observableFromStore from "oneRedux/utils/observable-from-store";
import {
    remove,
    some,
} from "lodash";

// Redux
import { StoreToken } from "tokens/redux-store.token";

// Services
import { Permissions } from "modules/shared/services/helper/permissions.service";

// Interfaces
import { IQuestionTypes } from "modules/template/interfaces/template.interface";
import {
    IStore,
    IStoreState,
} from "interfaces/redux.interface";

// Enums & Constants
import {
    QuestionTypes,
    QuestionTypeNames,
} from "constants/question-types.constant";
import { BuilderDraggableTypes } from "constants/template-builder.constant";

@Component({
    selector: "question-types-selection",
    templateUrl: "question-types-selection.component.html",
})

export class QuestionTypesSelection implements OnInit {

    questionTypes: IQuestionTypes[];
    storeSubscription: Subscription;
    builderDraggableTypes = BuilderDraggableTypes;

    constructor(
        @Inject(StoreToken) private store: IStore,
        private permissions: Permissions,
    ) {}

    ngOnInit(): void {
        this.questionTypes = [
            QuestionTypes.Date,
            QuestionTypes.MultiChoice,
            QuestionTypes.Statement,
            QuestionTypes.TextBox,
            QuestionTypes.YesNo,
        ];

        if (this.permissions.canShow("TemplateInventoryQuestion")) {
            this.questionTypes.push(QuestionTypes.Inventory);
        }

        if (this.permissions.canShow("TemplatePersonalDataQuestion")) {
            this.questionTypes.push(QuestionTypes.PersonalData);
        }

        this.storeSubscription = observableFromStore(this.store).pipe(
            startWith(this.store.getState()),
        ).subscribe((state: IStoreState): void => this.componentWillReceiveState(state));
    }

    trackByIndex(index): number {
        return index;
    }

    private componentWillReceiveState(state: IStoreState) {
        this.setIncidentQuestionVisibility(state);
        this.setAttributeQuestionVisibility(state);
    }

    private setIncidentQuestionVisibility(state: IStoreState) {
        if (this.permissions.canShow("IncidentBreachManagement")) {
            const templateHasIncidentQuestions = some(getTemplateState(state).builder.questionById, { questionType: QuestionTypeNames.Incident });
            if (templateHasIncidentQuestions) {
                this.questionTypes = this.questionTypes.filter((type: IQuestionTypes) => type.name !== QuestionTypeNames.Incident);
            } else if (!some(this.questionTypes, { name: QuestionTypeNames.Incident })) {
                this.questionTypes.push(QuestionTypes.Incident);
            }
        }
    }

    private setAttributeQuestionVisibility(state: IStoreState): void {
        if (this.permissions.canShow("TemplateAttributeQuestion")) {
            const questionsById = getTemplateState(state).builder.questionById;
            const templateHasInventoryQuestions = some(questionsById, { questionType: QuestionTypeNames.Inventory });
            const templateHasIncidentQuestions = some(questionsById, { questionType: QuestionTypeNames.Incident });

            if (templateHasInventoryQuestions || templateHasIncidentQuestions) {
                if (this.questionTypes && !some(this.questionTypes, { name: QuestionTypeNames.Attribute })) {
                    this.questionTypes.push(QuestionTypes.Attribute);
                }
            } else {
                remove(this.questionTypes, {name: QuestionTypeNames.Attribute});
            }
        }
    }
}
