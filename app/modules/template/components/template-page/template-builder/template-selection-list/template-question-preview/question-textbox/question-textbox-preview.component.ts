// Angular
import {
    Component,
    Inject,
    Input,
} from "@angular/core";

import { isUndefined } from "lodash";

// Interfaces
import { IAssessmentQuestionAction } from "interfaces/question.interface";
import { IQuestionDetails } from "interfaces/assessment-template-question.interface";

// Constants
import {
    TEXTBOX_UPDATED,
    NOT_SURE_SELECTED,
    NOT_APPLICABLE_SELECTED,
} from "generalcomponent/Assessment/assessment-actions.constants";

@Component({
    selector: "question-textbox-preview",
    templateUrl: "question-textbox-preview.component.html",
})

export class QuestionTextboxPreviewComponent {
    @Input() question: IQuestionDetails;

    isDisabled: false;
    answer: string;
    notApplicable: boolean;
    notSure: boolean;

    toggleNotSure(): void {
        if (!this.notSure) {
            this.answer = null;
            this.notApplicable = false;
        }
        this.notSure = !this.notSure;
    }

    toggleNotApplicable(): void {
        if (!this.notApplicable) {
            this.answer = null;
            this.notSure = false;
        }
        this.notApplicable = !this.notApplicable;
    }

    setTextbox(value: string): void {
        this.answer = value;
        this.notSure = false;
        this.notApplicable = false;
    }

    sendAction(type: string, value?: string): void {
        const actionType: IAssessmentQuestionAction = { type };
        if (type === TEXTBOX_UPDATED) {
            actionType.value = isUndefined(value) ? "" : value;
        }
        switch (type) {
            case TEXTBOX_UPDATED:
                this.setTextbox(value);
                break;
            case NOT_SURE_SELECTED:
                this.toggleNotSure();
                break;
            case NOT_APPLICABLE_SELECTED:
                this.toggleNotApplicable();
                break;
        }
    }
}
