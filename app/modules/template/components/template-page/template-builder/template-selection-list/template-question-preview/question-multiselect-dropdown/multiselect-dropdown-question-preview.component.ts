// Angular
import {
    Component,
    Input,
    OnChanges,
    SimpleChanges,
    AfterViewInit,
    OnInit,
} from "@angular/core";

// 3rd Party
import {
    findIndex,
    find,
    some,
    sortBy,
} from "lodash";
import { Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";

// Interfaces
import {
    IQuestionDetails,
    IQuestionOptionDetails,
    IMultipleSelectPayload,
} from "interfaces/assessment-template-question.interface";
import { IKeyValue } from "interfaces/generic.interface";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

// Services
import { TemplateBuilderModalHelperService } from "modules/template/services/template-builder-modal-helper.service";

@Component({
    selector: "multiselect-dropdown-question-preview",
    templateUrl: "./multiselect-dropdown-question-preview.component.html",
})

export class MultiselectDropdownQuestionComponent implements OnInit, OnChanges, AfterViewInit {
    @Input() question: IQuestionDetails;

    isDisabled: false;
    notApplicable: boolean;
    notSure: boolean;
    options: IQuestionOptionDetails[];
    selectedOptions: IQuestionOptionDetails[];
    multiSelectEnabled = true;
    autoCloseEnabled = true;
    clearInputOnModelChange = true;
    inputValue: string;
    prependText = this.translatePipe.transform("AddOption");

    private destroy$ = new Subject();

    constructor(
        private translatePipe: TranslatePipe,
        private templateBuilderModalHelperService: TemplateBuilderModalHelperService,
    ) {}

    ngOnInit(): void {
        this.templateBuilderModalHelperService.$questionBuilderModalListener.pipe(
            takeUntil(this.destroy$),
        ).subscribe((question: IQuestionDetails) => {
            this.options = question.options;
            this.selectedOptions = [];
        });
    }

    ngOnDestroy() {
        this.destroy$.next();
        this.destroy$.complete();
    }

    ngOnChanges(changes: SimpleChanges) {
        this.initialize();
    }

    ngAfterViewInit(): void {
        this.options = [...this.question.options];
    }

    initialize(): void {
        this.selectedOptions = [];
    }

    onInputChange({ key, value }: IKeyValue<string>): void {
        if (key === "Enter") return;
        this.inputValue = value;
    }

    isOptionInList(questionOptions: IQuestionOptionDetails[], stringSearch: string): boolean {
        return Boolean(find(questionOptions, (option: IQuestionOptionDetails): boolean => option.option === stringSearch));
    }

    handleFirstOption(value: string): void {
        if (!this.question.attributeJson.allowOther) return;
        if (value && !this.isOptionInList(this.options, value)) {
            const selectedOp = { option: value, id: "", optionType: "OTHERS" };
            this.selectedOptions = this.selectedOptions.concat(selectedOp);
            this.notApplicable = false;
            this.notSure = false;
        }
        this.inputValue = null;
    }

    onModelChange({ currentValue, change, previousValue }: IMultipleSelectPayload): void {
        if (!currentValue) {
            this.inputValue = "";
        }
        this.selectedOptions = currentValue;
        if (!previousValue || currentValue.length > previousValue.length) {
            const optionIndex: number = findIndex(this.options, (opt: IQuestionOptionDetails): boolean => {
                return change.option === opt.option;
            });
            this.options.splice(optionIndex, 1);
            this.options = [...this.options];
        } else if (change.id
            && !some(this.options, {id: change.id})
            && some(this.question.options, {id: change.id})) {
            this.options.push(change);
        }
        this.options = sortBy(this.options, ["sequence"]);
        this.notSure = false;
        this.notApplicable = false;
    }

    toggleNotApplicableAnswer(): void {
        this.notApplicable = !this.notApplicable;
        if (this.notApplicable) {
            this.notSure = false;
            this.selectedOptions = [];
            this.options = [...this.question.options];
        }
    }

    toggleNotSureAnswer(): void {
        this.notSure = !this.notSure;
        if (this.notSure) {
            this.notApplicable = false;
            this.selectedOptions = [];
            this.options = [...this.question.options];
        }
    }
}
