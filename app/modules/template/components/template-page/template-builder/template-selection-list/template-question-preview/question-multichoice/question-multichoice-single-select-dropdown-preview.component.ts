// Angular
import {
    Component,
    Inject,
    Input,
    SimpleChanges,
    OnChanges,
} from "@angular/core";

// 3rd Party
import {
    findIndex,
    filter,
    map,
    find,
} from "lodash";

// Interfaces
import {
    IQuestionDetails,
    IQuestionOptionDetails,
    ISingleSelectPayload,
} from "interfaces/assessment-template-question.interface";
import {
    IKeyValue,
} from "interfaces/generic.interface";

@Component({
    selector: "question-multichoice-single-select-dropdown-preview",
    templateUrl: "question-multichoice-single-select-dropdown-preview.component.html",
})

export class QuestionMultichoiceSingleSelectDropDownPreviewComponent implements OnChanges {
    @Input() question: IQuestionDetails;
    @Input() allowOtherPrependText = "AddOption";

    isDisabled: false;
    notApplicable: boolean;
    notSure: boolean;
    options: IQuestionOptionDetails[];
    originalOptions: IQuestionOptionDetails[];
    selectedOption: IQuestionOptionDetails;
    defaultOption =  true;
    multiSelectEnabled = false;
    autoCloseEnabled = true;
    clearInputOnModelChange = false;
    applySingleSelectInputStyle = false;
    applySingleSelectOtherInputStyle = false;
    inputValue: string;

    ngOnChanges(changes: SimpleChanges): void {
        this.initialize();
    }

    initialize(): void {
        this.selectedOption = null;
    }

    toggleNotApplicableAnswer(): void {
        this.notApplicable = !this.notApplicable;
        if (this.notApplicable) {
            this.resetOptions();
            this.notApplicable = true;
            this.initialize();
        }
    }

    toggleNotSureAnswer(): void {
        this.notSure = !this.notSure;
        if (this.notSure) {
            this.resetOptions();
            this.notSure = true;
            this.initialize();
        }
    }

    onInputChange(event: IKeyValue<string>): void {
        const { key, value } = event;
        this.inputValue = value;
        if (key === "Enter")  return;
        this.defaultOption = false;
        this.filterList(this.options, value);
    }

    filterList(questionOptions: IQuestionOptionDetails[], stringSearch: string): void {
        this.originalOptions = filter(questionOptions, (opt: IQuestionOptionDetails): boolean => (opt.option.search(stringSearch) !== -1));
    }

    onModelChange({ currentValue, change }: ISingleSelectPayload): void {
        this.selectedOption = currentValue;
        if(!this.selectedOption) {
            this.resetOptions();
        } else {
            this.resetOptions();
            this.applySingleSelectInputStyle = true;
        }
        this.originalOptions = this.options;
    }

    addOtherOption(value: string) {
        if(!value) {
            this.resetOptions();
        } else {
            this.inputValue = value;
            const selectedOption = {id: "", optionType: "OTHERS", option: value };
            this.selectedOption = selectedOption;
            this.resetOptions();
            this.applySingleSelectOtherInputStyle = true;
        }
        this.originalOptions = this.options;
    }

    resetOptions() {
        this.notSure = false;
        this.notApplicable = false;
        this.applySingleSelectInputStyle = false;
        this.applySingleSelectOtherInputStyle = false;
    }
}