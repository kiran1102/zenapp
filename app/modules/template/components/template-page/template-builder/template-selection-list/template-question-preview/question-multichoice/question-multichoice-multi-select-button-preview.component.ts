import {
    Component,
    Input,
    Inject,
} from "@angular/core";

// 3rd party
import {
    find,
    remove,
    includes,
} from "lodash";

// interfaces
import { IQuestionDetails } from "interfaces/assessment-template-question.interface";
import { ITemplateQuestionMultichoiceOptions } from "modules/template/interfaces/template-questions.interfaces";

// constants
import {
    MULTISELECT_OPTION_SELECTED,
    NOT_SURE_SELECTED,
    NOT_APPLICABLE_SELECTED,
    OTHER_OPTION_CREATED,
    OTHER_OPTION_UPDATED,
} from "generalcomponent/Assessment/assessment-actions.constants";
import { AAOtherOptionConfig } from "constants/question-types.constant";

@Component({
    selector: "question-multichoice-multi-select-button-preview",
    templateUrl: "question-multichoice-multi-select-button-preview.component.html",
})
export class QuestionMultichoiceMultiSelectButtonPreviewComponent {

    @Input() question: IQuestionDetails;

    answers: ITemplateQuestionMultichoiceOptions[] = [];
    notSure: boolean;
    notApplicable: boolean;
    otherAnswers: string[] = [];
    newOtherModel = "";
    otherConfig = AAOtherOptionConfig;
    newOtherConfig = { ...this.otherConfig, clearOnSelect: true };

    sendAction(type: string, value?: any, index?: number) {
        switch (type) {
            case MULTISELECT_OPTION_SELECTED:
                this.setMultiSelect(value);
                break;
            case NOT_SURE_SELECTED:
                this.toggleNotSure();
                break;
            case NOT_APPLICABLE_SELECTED:
                this.toggleNotApplicable();
                break;
            case OTHER_OPTION_CREATED:
                this.setOther(value);
                break;
            case OTHER_OPTION_UPDATED:
                this.updateOther(value, index);
                break;
        }
    }

    isSelected(option: ITemplateQuestionMultichoiceOptions): boolean {
        return Boolean(find(this.answers, (answer: ITemplateQuestionMultichoiceOptions): boolean => answer === option));
    }

    private setMultiSelect(value: ITemplateQuestionMultichoiceOptions) {
        if (!this.isSelected(value)) {
            this.answers.push(value);
        } else {
            remove(this.answers, (answer: ITemplateQuestionMultichoiceOptions): boolean => answer === value);
        }
        this.notApplicable = false;
        this.notSure = false;
    }

    private setOther(value: string) {
        if (!value || includes(this.otherAnswers, value)) return;
        this.otherAnswers.push(value);
        this.notSure = false;
        this.notApplicable = false;
    }

    private updateOther(value: string, index: number) {
        if (!value || includes(this.otherAnswers, value)) {
            this.otherAnswers.splice(index, 1);
        }
    }

    private toggleNotSure() {
        if (!this.notSure) {
            this.answers = [];
            this.otherAnswers = [];
            this.notApplicable = false;
        }
        this.notSure = !this.notSure;
    }

    private toggleNotApplicable() {
        if (!this.notApplicable) {
            this.answers = [];
            this.otherAnswers = [];
            this.notSure = false;
        }
        this.notApplicable = !this.notApplicable;
    }
}
