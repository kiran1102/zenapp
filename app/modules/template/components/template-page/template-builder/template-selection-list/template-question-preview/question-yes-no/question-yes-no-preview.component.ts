import {
    Component,
    Input,
    Inject,
} from "@angular/core";

// interfaces
import { IQuestionDetails } from "interfaces/assessment-template-question.interface";

// constants
import {
    YES_SELECTED,
    NO_SELECTED,
    NOT_SURE_SELECTED,
    NOT_APPLICABLE_SELECTED,
} from "generalcomponent/Assessment/assessment-actions.constants";
import { IStringMap } from "interfaces/generic.interface";

@Component({
    selector: "question-yes-no-preview",
    templateUrl: "./question-yes-no-preview.component.html",
})
export class QuestionYesNoPreviewComponent {
    @Input() question: IQuestionDetails;

    answer: boolean;
    justification: string;
    notApplicable: boolean;
    notSure: boolean;
    translations: IStringMap<string>;

    setYesNo(value: boolean): void {
        this.answer = (value === this.answer) ? null : value;
        this.notSure = false;
        this.notApplicable = false;
    }

    toggleNotSure(): void {
        if (!this.notSure) {
            this.answer = null;
            this.notApplicable = false;
        }
        this.notSure = !this.notSure;
    }

    toggleNotApplicable(): void {
        if (!this.notApplicable) {
            this.answer = null;
            this.notSure = false;
        }
        this.notApplicable = !this.notApplicable;
    }

    sendAction(type: string, value?: boolean): void {
        switch (type) {
            case YES_SELECTED:
            case NO_SELECTED:
                this.setYesNo(value);
                break;
            case NOT_SURE_SELECTED:
                this.toggleNotSure();
                break;
            case NOT_APPLICABLE_SELECTED:
                this.toggleNotApplicable();
                break;
        }
    }
}
