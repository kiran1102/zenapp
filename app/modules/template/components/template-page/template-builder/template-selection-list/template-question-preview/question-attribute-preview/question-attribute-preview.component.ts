// External Libraries
import {
    Component,
    Input,
} from "@angular/core";

// Constants
import { AttributeQuestionResponseType } from "constants/question-types.constant";

// Interfaces
import { IQuestionDetails } from "interfaces/assessment-template-question.interface";

@Component({
    selector: "question-attribute-preview",
    templateUrl: "./question-attribute-preview.component.html",
})

export class AttributeQuestionPreviewComponent {
    @Input() question: IQuestionDetails;

    attributeQuestionResponseType = AttributeQuestionResponseType;
}
