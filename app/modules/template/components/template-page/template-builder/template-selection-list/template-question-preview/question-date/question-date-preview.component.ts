// Angular
import {
    Component,
    Inject,
    Input,
} from "@angular/core";

// 3rd party
import { IMyDateModel } from "mydatepicker";

// Interfaces
import { IQuestionDetails } from "interfaces/assessment-template-question.interface";

// Services
import { TimeStamp } from "oneServices/time-stamp.service";

// Constants
import {
    DATE_SELECTED,
    NOT_SURE_SELECTED,
    NOT_APPLICABLE_SELECTED,
} from "generalcomponent/Assessment/assessment-actions.constants";

@Component({
    selector: "question-date-preview",
    templateUrl: "./question-date-preview.component.html",
})

export class QuestionDatePreviewComponent {
    @Input() question: IQuestionDetails;

    answer: string;
    format = this.timeStamp.getDatePickerDateFormat();
    isDisabled: boolean;
    notApplicable: boolean;
    notSure: boolean;

    constructor(
        private timeStamp: TimeStamp,
    ) { }

    toggleNotSure(): void {
        if (!this.notSure) {
            this.answer = null;
            this.notApplicable = false;
        }
        this.notSure = !this.notSure;
    }

    toggleNotApplicable(): void {
        if (!this.notApplicable) {
            this.answer = null;
            this.notSure = false;
        }
        this.notApplicable = !this.notApplicable;
    }

    setDate(value: IMyDateModel): void {
        this.answer = value.formatted;
        if (this.answer) {
        this.notSure = false;
        this.notApplicable = false;
        }
    }

    sendAction(type: string, value?: IMyDateModel): void {
        switch (type) {
            case DATE_SELECTED:
                this.setDate(value);
                break;
            case NOT_SURE_SELECTED:
                this.toggleNotSure();
                break;
            case NOT_APPLICABLE_SELECTED:
                this.toggleNotApplicable();
                break;
        }
    }
}
