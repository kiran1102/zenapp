// angular
import {
    Component,
    Input,
    OnInit,
} from "@angular/core";

// interfaces
import { IQuestionDetails } from "interfaces/assessment-template-question.interface";
import { ITemplateQuestionMultichoiceOptions } from "modules/template/interfaces/template-questions.interfaces";

// constants
import {
    MULTICHOICE_OPTION_SELECTED,
    NOT_SURE_SELECTED,
    NOT_APPLICABLE_SELECTED,
    OTHER_OPTION_UPDATED,
} from "generalcomponent/Assessment/assessment-actions.constants";
import { AAOtherOptionConfig } from "constants/question-types.constant";
import { IStringMap } from "interfaces/generic.interface";

// pipes
import { TranslatePipe } from "pipes/translate.pipe";

@Component({
    selector: "question-multichoice-single-select-button-preview",
    templateUrl: "question-multichoice-single-select-button-preview.component.html",
})
export class QuestionMultichoiceSingleSelectButtonPreviewComponent implements OnInit {
    @Input() question: IQuestionDetails;

    answer: ITemplateQuestionMultichoiceOptions;
    otherAnswer: string;
    notSure: boolean;
    notApplicable: boolean;
    justification: string;
    otherConfig = AAOtherOptionConfig;
    translations: IStringMap<string>;

    constructor(
        private translatePipe: TranslatePipe,
    ) {}

    ngOnInit(): void {
        this.translations = {
            notSure: this.translatePipe.transform("NotSure"),
            notApplicable: this.translatePipe.transform("NotApplicable"),
            justifyYourAnswerBelow: this.translatePipe.transform("JustifyYourAnswerBelow", { Justify: this.translatePipe.transform("Justify") }),
            pleaseProvideJustification: this.translatePipe.transform("PleaseProvideJustification"),
        };
    }

    setMultichoice(value: ITemplateQuestionMultichoiceOptions): void {
        this.answer = (value === this.answer) ? null : value;
        this.otherAnswer = null;
        this.notSure = false;
        this.notApplicable = false;
    }

    setOther(value: string): void {
        this.otherAnswer = value;
        this.answer = null;
        this.notSure = false;
        this.notApplicable = false;
    }

    toggleNotSure(): void {
        if (!this.notSure) {
            this.answer = null;
            this.otherAnswer = null;
            this.notApplicable = false;
        }
        this.notSure = !this.notSure;
    }

    toggleNotApplicable(): void {
        if (!this.notApplicable) {
            this.answer = null;
            this.otherAnswer = null;
            this.notSure = false;
        }
        this.notApplicable = !this.notApplicable;
    }

    sendAction(type: string, value?: any): void {
        switch (type) {
            case MULTICHOICE_OPTION_SELECTED:
                this.setMultichoice(value);
                break;
            case NOT_SURE_SELECTED:
                this.toggleNotSure();
                break;
            case NOT_APPLICABLE_SELECTED:
                this.toggleNotApplicable();
                break;
            case OTHER_OPTION_UPDATED:
                this.setOther(value);
                break;
        }
    }
}
