// Angular
import {
    Component,
    Input,
} from "@angular/core";

// Constants
import { QuestionTypes } from "constants/question-types.constant";

// Enums
import { QuestionAnswerTypes } from "enums/question-types.enum";

// Interfaces
import { IQuestionDetails } from "interfaces/assessment-template-question.interface";
import { IStringMap } from "interfaces/generic.interface";
import { IQuestionTypes } from "modules/template/interfaces/template.interface";

@Component({
    selector: "template-question-preview",
    templateUrl: "template-question-preview.component.html",
})

export class TemplateQuestionPreviewComponent {

    @Input() question: IQuestionDetails | null;
    @Input() removeBorder: boolean;

    questionTypes: IStringMap<IQuestionTypes> = QuestionTypes;
    questionAnswerTypes = QuestionAnswerTypes;

    constructor() {}
}
