// Rxjs
import { SubscriptionLike as ISubscription } from "rxjs";
import { startWith } from "rxjs/operators";

// Angular
import {
    Component,
    OnInit,
    Inject,
    Input,
    OnDestroy,
} from "@angular/core";

// External Libraries
import { forEach } from "lodash";

// Interfaces
import {
    IAssessmentTemplateQuestion,
    IQuestionDetails,
    IQuestionOptionDetails,
    IQuestionAttributes,
    IQuestionBuilderModalData,
    IBuilderQuestionDragItem,
} from "interfaces/assessment-template-question.interface";
import { IStringMap } from "interfaces/generic.interface";
import {
    IStore,
    IStoreState,
} from "interfaces/redux.interface";
import { IDeleteConfirmationModalResolve } from "interfaces/delete-confirmation-modal-resolve.interface";
import { ITemplateState } from "interfaces/template-reducer.interface";
import { IAssessmentConditionsModalResolve } from "interfaces/assessment/assessment-conditions-modal-resolve.interface";

// Redux
import {
    getBuilderView,
    getTemplateState,
    hasQuestionNavigation,
    getCurrentTemplate,
    getQuestionById,
    getSectionByQuestionId,
} from "oneRedux/reducers/template.reducer";
import observableFromStore from "oneRedux/utils/observable-from-store";
import {
    QuestionTypeNames,
    AttributeQuestionResponseType,
} from "constants/question-types.constant";
import { StoreToken } from "tokens/redux-store.token";

// Services
import TemplateAction from "templateServices/actions/template-action.service";
import { ModalService } from "sharedServices/modal.service";
import { QuestionTypeService } from "modules/template/services/question-type.service";
import { TemplateBuilderService } from "templateServices/template-builder.service";
import { Permissions } from "modules/shared/services/helper/permissions.service";

// Enums / Constants
import { TemplateModes } from "enums/template-modes.enum";
import { InventorySchemaIds } from "constants/inventory-config.constant";
import { BuilderDraggableTypes } from "constants/template-builder.constant";
import { TemplateStates } from "enums/template-states.enum";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

interface IParentQuestionDetails {
    sequence: string;
    tooltip: string;
}

@Component({
    selector: "builder-question",
    templateUrl: "builder-question.component.html",
})

export class BuilderQuestion implements OnInit, OnDestroy {

    @Input() isReadonly: boolean;
    @Input() parentQuestionDetails: IParentQuestionDetails;
    @Input() question: IAssessmentTemplateQuestion;
    @Input() questionDetails: IQuestionDetails;
    @Input() questionId: string;
    @Input() sectionId: string;
    @Input() sectionSequence: number;
    @Input() sequence: number;
    @Input() templateId: string;

    dragDisabled = false;
    hasNavigation: boolean;
    editQuestionEnabled = false;
    hoverEnabled = false;
    isAddConditionShown = false;
    isRelatedInventoryIconShown = false;
    isCreateMode: boolean;
    isPreviewShown = false;
    isTemplateActive: boolean;
    isTemplateLocked: boolean;
    isQuestionPreviewLoaded = true;
    questionHoverClass = "cursor-default";
    questionIsInvalid = false;
    questionTypeIcon: string;
    storeSubscription: ISubscription;
    builderDraggableTypes = BuilderDraggableTypes;
    questionDragData: IBuilderQuestionDragItem = null;

    translations: IStringMap<string> = {
        questionPreview: this.translatePipe.transform("QuestionPreview"),
        deleteQuestion: this.translatePipe.transform("DeleteQuestion"),
        cancelButtonText: this.translatePipe.transform("Cancel"),
        confirmButtonText: this.translatePipe.transform("Confirm"),
        sureToDeleteThisQuestion: this.translatePipe.transform("SureToDeleteThisQuestion"),
    };

    permissions: IStringMap<boolean> = {
        TemplatesCreateAndPublish: this.permissionsService.canShow("TemplatesCreateAndPublish"),
    };

    constructor(
        @Inject("TemplateAction") private templateAction: TemplateAction,
        @Inject(StoreToken) private store: IStore,
        @Inject("TemplateBuilderService") private templateBuilder: TemplateBuilderService,
        private translatePipe: TranslatePipe,
        private readonly modalService: ModalService,
        private readonly permissionsService: Permissions,
        private questionType: QuestionTypeService,
    ) { }

    ngOnInit() {
        this.storeSubscription = observableFromStore(this.store).pipe(
            startWith(this.store.getState()),
        ).subscribe((state: IStoreState): void =>
            this.componentWillReceiveState(state),
        );
    }

    openConditionsModal(): void {
        const modalData: IAssessmentConditionsModalResolve = {
            questionId: this.questionId,
            sectionId: this.sectionId,
            previewMode: !this.isCreateMode,
        };
        this.modalService.setModalData(modalData);
        this.modalService.openModal("downgradeTemplateConditionsModal");
    }

    openRelatedInventoryModal(): void {
        const modalData: IAssessmentConditionsModalResolve = {
            questionId: this.questionId,
            sectionId: this.sectionId,
            previewMode: !this.isCreateMode,
        };
        this.modalService.setModalData(modalData);
        this.permissionsService.canShow("InventoryLinkingEnabled")
            ? this.modalService.openModal("downgradeConfigureRelationshipModal")
            : this.modalService.openModal("downgradeTemplateRelatedInventoryModal");
    }

    ngOnDestroy(): void {
        if (this.storeSubscription) {
            this.storeSubscription.unsubscribe();
        }
    }

    onPreviewQuestionClick = (questionId: string): void => {
        this.isPreviewShown = !this.isPreviewShown;
        if (this.isPreviewShown) {
            this.isQuestionPreviewLoaded = true;
            this.templateAction.getQuestion(getCurrentTemplate(this.store.getState()).id, this.sectionId, questionId)
                .then((question: IQuestionDetails): void => {
                    this.isQuestionPreviewLoaded = false;
                    if (question) {
                        if (question.options && question.options.length) {
                            forEach(question.options, (option: IQuestionOptionDetails): void => {
                                if (question.attributeJson.optionHintEnabled) {
                                    option.attributeJson = this.templateAction.parseAttributes(option.attributes);
                                    option.Hint = (option.attributeJson) ? option.attributeJson.hint : option.Hint;
                                }
                            });
                        }
                    }
                    this.questionDetails = question;
                });
        }
    }

    onQuestionDelete = (question: IAssessmentTemplateQuestion): void => {
        const modalData: IDeleteConfirmationModalResolve = {
            modalTitle: this.translations.deleteQuestion,
            confirmationText: this.translations.sureToDeleteThisQuestion,
            cancelButtonText: this.translations.cancelButtonText,
            submitButtonText: this.translations.confirmButtonText,
            promiseToResolve:
                (): ng.IPromise<boolean> => this.templateAction.deleteTemplateQuestion(getCurrentTemplate(this.store.getState()).id, this.sectionId, question.id)
                    .then((result: boolean): boolean => {
                        return result;
                    }),
        };
        this.modalService.setModalData(modalData);
        this.modalService.openModal("otDeleteConfirmationModal");
    }

    onEditQuestionClick = (): void => {
        this.editQuestionEnabled = true;
        this.templateAction.getQuestion(getCurrentTemplate(this.store.getState()).id, this.sectionId, this.questionId)
            .then((question: IQuestionDetails): void => {
                this.templateAction.openTemplateEditorModal({
                    templateId: this.templateId,
                    sectionId: this.sectionId,
                    question,
                });
                this.isPreviewShown = false;
                this.editQuestionEnabled = false;
            });
    }

    getRelatedInventoryIconTooltip(): string {
        if (this.permissionsService.canShow("InventoryLinkingEnabled")) {
            return this.translatePipe.transform("ConfigureInventoryRelationship");
        } else {
            if (this.question.attributeJson.inventoryType === InventorySchemaIds.Assets) return this.translatePipe.transform("RelateAssetToProcessingActivity");
            if (this.question.attributeJson.inventoryType === InventorySchemaIds.Processes) return this.translatePipe.transform("RelateProcessingActivityToAsset");
            if (this.question.questionType === QuestionTypeNames.PersonalData) return this.translatePipe.transform("RelatePersonalDataToProcessingActivity");
        }
    }

    onBuilderQuestionHover(): void {
        if (!this.isQuestionInvalid(this.question)) {
            this.hoverEnabled = true;
        }
    }

    onQuestionPanelClick(): void {
        if (!this.questionIsInvalid || this.isTemplateLocked || !this.isTemplateActive) return;
        const questionSequence = getBuilderView(this.store.getState()).sectionIdQuestionIdsMap[this.sectionId] || [];
        const questionBuilderModalData: IQuestionBuilderModalData = {
            templateId: this.templateId,
            sectionId: this.sectionId,
            question: this.question,
            nextQuestionId: questionSequence[this.question.sequence],
        };

        this.templateAction.openResolveInvalidQuestionModal(questionBuilderModalData);
    }

    private componentWillReceiveState(state: IStoreState): void {
        this.question = getBuilderView(state).questionById[this.questionId];
        // update from temp model if present
        this.questionDragData = {
            questionId: this.questionId,
            sectionId: this.sectionId,
            index: (this.question) ? this.question.sequence : null,
        };
        const template: ITemplateState = getTemplateState(state);
        this.isTemplateLocked = template.current.isLocked;
        this.isTemplateActive = template.current.isActive;
        this.dragDisabled = template.current.status === TemplateStates.Published || template.current.isLocked || !template.current.isActive;
        const questionDragging: boolean = template.dragQuestion;
        this.isReadonly = template.templateMode === TemplateModes.Readonly;
        this.isCreateMode = template.templateMode === TemplateModes.Create;
        this.isPreviewShown = this.isPreviewShown ? !questionDragging : false;

        if (this.question) {
            this.questionTypeIcon = this.questionType.getQuestionTypeIcon(this.question.questionType);
            this.question.attributeJson = this.question.attributes ? JSON.parse(this.question.attributes) : {};
            this.isAddConditionShown = this.canHaveNavigationRule();
            this.isRelatedInventoryIconShown = this.canShowRelatedInventoryIcon();

            // Attribute Questions display the Parent Question's Sequence
            this.parentQuestionDetails = this.question.questionType === QuestionTypeNames.Attribute && this.question.parentQuestionId
                ? this.getParentQuestionDetails(this.question.parentQuestionId)
                : null;

            this.questionHoverClass = this.getQuestionHoverClass(this.question);
            this.questionIsInvalid = this.isQuestionInvalid(this.question);
        }

        this.hasNavigation = hasQuestionNavigation(this.questionId)(state);
    }

    private getQuestionHoverClass(question: IAssessmentTemplateQuestion): string {
        if (this.isQuestionInvalid(question)) {
            return "cursor-pointer";
        } else if (this.isCreateMode) {
            return "cursor-move";
        } else {
            return "cursor-default";
        }
    }

    private isQuestionInvalid(question: IAssessmentTemplateQuestion): boolean {
        return this.templateBuilder.isQuestionInvalid(this.sectionId, question ? question.id : "");
    }

    private canHaveNavigationRule(): boolean {
        if (this.question.questionType === QuestionTypeNames.MultiChoice || this.question.questionType === QuestionTypeNames.YesNo) return true;

        if (this.question.questionType === QuestionTypeNames.Attribute
            && (this.question.attributeJson.responseType === AttributeQuestionResponseType.SINGLE_SELECT
                || this.question.attributeJson.responseType === AttributeQuestionResponseType.MULTI_SELECT)
        ) {
            const parentInventoryQuestion = getQuestionById(this.question.parentQuestionId)(this.store.getState());
            if (parentInventoryQuestion.attributes && (JSON.parse(parentInventoryQuestion.attributes) as IQuestionAttributes).isMultiSelect) {
                return false;
            }
            return true;
        }

        return false;
    }

    private canShowRelatedInventoryIcon(): boolean {
        if (this.permissionsService.canShow("InventoryLinkingEnabled")) {
            return this.question.questionType === QuestionTypeNames.Inventory || this.question.questionType === QuestionTypeNames.PersonalData;
        } else {
            return this.question.questionType === QuestionTypeNames.PersonalData ||
                (this.question.questionType === QuestionTypeNames.Inventory
                    && this.question.attributeJson.inventoryType !== InventorySchemaIds.Elements
                    && this.question.attributeJson.inventoryType !== InventorySchemaIds.Vendors);
        }
    }

    private getQuestionTypeIcon(type: string): string {
        return this.questionType.getQuestionTypeIcon(type);
    }

    private getParentQuestionDetails(parentQuestionId: string): IParentQuestionDetails {
        const parentQuestion: IAssessmentTemplateQuestion = getQuestionById(this.question.parentQuestionId)(this.store.getState());
        const parentQuestionSequence = `${getSectionByQuestionId(parentQuestion.id)(this.store.getState()).sequence}.${parentQuestion.sequence}`;
        return {
            sequence: parentQuestionSequence,
            tooltip: `${parentQuestionSequence}) ${parentQuestion.content}`,
        };
    }

    private translateQuestionContent(): string {
        if (this.question.attributeJson && this.question.attributeJson.nameKey) {
            return this.translatePipe.transform(this.question.attributeJson.nameKey);
        }
        return this.question.content;
    }
}
