// Rxjs
import { Subscription } from "rxjs/";
import { startWith } from "rxjs/operators";

// Angular
import {
    Component,
    OnInit,
    Inject,
    Input,
    OnDestroy,
} from "@angular/core";

// External Modules
import {
    remove,
    isEqual,
} from "lodash";

// Interfaces
import {
    IStore,
    IStoreState,
} from "interfaces/redux.interface";
import { ITemplateState } from "interfaces/template-reducer.interface";
import {
    IAssessmentTemplateQuestion,
    IQuestionBuilderModalData,
} from "interfaces/assessment-template-question.interface";
import { IStringMap } from "interfaces/generic.interface";

// Redux
import {
    getBuilderView,
    getTemplateState,
    getCurrentTemplate,
} from "oneRedux/reducers/template.reducer";
import { StoreToken } from "tokens/redux-store.token";
import observableFromStore from "oneRedux/utils/observable-from-store";

// Services
import TemplateAction from "templateServices/actions/template-action.service";
import { TemplateBuilderService } from "templateServices/template-builder.service";
import { Permissions } from "modules/shared/services/helper/permissions.service";

// Enums / Constants
import { TemplateModes } from "enums/template-modes.enum";
import { BuilderDraggableTypes } from "constants/template-builder.constant";
import { QuestionTypeNames } from "constants/question-types.constant";

@Component({
    selector: "builder-question-list",
    templateUrl: "builder-question-list.component.html",
})

export class BuilderQuestionList implements OnInit, OnDestroy {

    @Input() sectionId: string;
    @Input() sectionSequence: number;
    @Input() templateId: string;

    builderDraggableTypes: IStringMap<string> = BuilderDraggableTypes;
    isCreateMode: boolean;
    isQuestionSectionDragging = false;
    isTemplateActive: boolean;
    isTemplateLocked: boolean;
    permissions: IStringMap<boolean>;
    questionModels: IStringMap<IAssessmentTemplateQuestion>;
    questionSequence: string[];
    storeSubscription: Subscription;

    constructor(
        @Inject("TemplateAction") private templateAction: TemplateAction,
        @Inject(StoreToken) private store: IStore,
        @Inject("TemplateBuilderService") private templateBuilder: TemplateBuilderService,
        private permissionsService: Permissions,
    ) { }

    onDragOver = (event): void => {
        event.preventDefault();
        event.stopPropagation();
    }

    onDragLeave = (event): void => {
        event.preventDefault();
        event.stopPropagation();
    }

    dndInserted = (event): void | boolean => {
        // ngx-grag-drop-lists does not give an option in the documentation, to override dndDrop,
        // and adds the dropped item to the list. Remove it here once the operation is complete
        remove(this.questionSequence, (sequence: any): boolean =>
            (sequence.name && sequence.name === event.item.name) || (sequence.questionId && sequence.questionId === event.item.questionId),
        );
    }

    dndDropHandler = (event): void => {
        if (event.type === BuilderDraggableTypes.BuilderQuestionIcon) {
            const questionBuilderModalData: IQuestionBuilderModalData = {
                templateId: this.templateId,
                sectionId: this.sectionId,
                question: {
                    questionType: (event.item.name as string),
                },
                nextQuestionId: this.questionSequence[event.index],
            };
            if (event.item.name === QuestionTypeNames.Attribute) {
                this.templateAction.openTemplateAttributeQuestionModal(questionBuilderModalData);
            } else {
                this.templateAction.openTemplateEditorModal(questionBuilderModalData);
            }
        } else if (event.type === BuilderDraggableTypes.BuilderQuestion) {
            // if the element is dropped in the same section,
            // also check if the location is same
            if (this.sectionId === event.item.sectionId) {
                // there is a quirk of angular-drag-drop-list that returns
                // index (zero index + 1) if the drag is happening down and
                // if we drop the item at its position, the index won't be same
                const oldSequence: string[] = [...this.questionSequence];
                const newSequence: string[] = [...this.questionSequence];
                // add the element first at the drop location, and remove it from earlier location
                // and then match
                newSequence.splice(event.index, 0, event.item.questionId);
                newSequence.splice(event.item.index - 1, 1);

                if (isEqual(oldSequence, newSequence)) {
                    return;
                }
            }

            this.templateAction.reorderQuestion(getCurrentTemplate(this.store.getState()).id, {
                questionId: event.item.questionId,
                sectionId: event.item.sectionId,
                nextQuestionId: this.questionSequence[event.index],
                nextSectionId: this.sectionId === event.item.sectionId ? null : this.sectionId,
            });
        }
    }

    ngOnInit() {
        this.permissions = {
            TemplatesCreateAndPublish: this.permissionsService.canShow("TemplatesCreateAndPublish"),
        };

        this.storeSubscription = observableFromStore(this.store).pipe(
            startWith(this.store.getState()),
        ).subscribe((state: IStoreState): void => this.componentWillReceiveState(state));
    }

    ngOnDestroy(): void {
        if (this.storeSubscription) {
            this.storeSubscription.unsubscribe();
        }
    }

    trackByQuestionId(index: number, questionId: string): string {
        return questionId;
    }

    onAddQuestionClick = (): void => {
        this.templateAction.openTemplateEditorModal({
            templateId: this.templateId,
            sectionId: this.sectionId,
        });
    }

    private componentWillReceiveState(state: IStoreState): void {
        const builder = getBuilderView(state);
        this.questionSequence = builder.sectionIdQuestionIdsMap[this.sectionId] || [];
        this.questionModels = builder.questionById;
        const template: ITemplateState = getTemplateState(state);
        this.isTemplateLocked = template.current.isLocked;
        this.isTemplateActive = template.current.isActive;
        this.isQuestionSectionDragging = template.dragQuestion || template.dragSection;
        this.isCreateMode = template.templateMode === TemplateModes.Create;
    }
}
