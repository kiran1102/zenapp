// Rxjs
import { SubscriptionLike as ISubscription } from "rxjs";
import { startWith } from "rxjs/operators";

// Angular
import {
    Component,
    OnInit,
    Inject,
    Input,
    OnDestroy,
} from "@angular/core";

// Interfaces
import {
    IAssessmentTemplateSection,
    IEditTemplateSectionResolve,
} from "interfaces/assessment-template-section.interface";
import { IStringMap } from "interfaces/generic.interface";
import {
    IStore,
    IStoreState,
} from "interfaces/redux.interface";
import { IDeleteConfirmationModalResolve } from "interfaces/delete-confirmation-modal-resolve.interface";

// Services
import TemplateAction from "templateServices/actions/template-action.service";
import { Permissions } from "modules/shared/services/helper/permissions.service";

// Redux
import {
    getSectionById,
    getTemplateState,
    getTempSectionNameById,
    TemplateActions,
    getCurrentTemplate,
} from "oneRedux/reducers/template.reducer";
import { StoreToken } from "tokens/redux-store.token";
import observableFromStore from "oneRedux/utils/observable-from-store";
import { ModalService } from "sharedServices/modal.service";

// Constants
import { BuilderDraggableTypes } from "constants/template-builder.constant";
import { ITemplateState } from "interfaces/template-reducer.interface";
import { TemplateModes } from "enums/template-modes.enum";
import { TemplateStates } from "enums/template-states.enum";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

@Component({
    selector: "template-section",
    templateUrl: "template-section.component.html",
})

export class TemplateSectionComponent implements OnInit, OnDestroy {

    @Input() sectionName: string;
    @Input() sectionId: string;
    @Input() sequence: number;
    @Input() templateId: string;
    @Input() sectionSequence: string;

    builderDraggableTypes = BuilderDraggableTypes;
    hoverEnabled: boolean;
    isCreateMode: boolean;
    isSectionExpanded = false;
    isTemplateActive: boolean;
    isTemplateLocked: boolean;
    isReadonly: boolean;
    permissions: IStringMap<boolean>;
    storeSubscription: ISubscription;
    section: IAssessmentTemplateSection;
    sectionDragging: boolean;
    dragDisabled = false;

    constructor(
        @Inject("TemplateAction") private templateAction: TemplateAction,
        @Inject(StoreToken) private store: IStore,
        private translatePipe: TranslatePipe,
        private permissionsService: Permissions,
        private modalService: ModalService,
    ) { }

    ngOnInit() {
        this.storeSubscription = observableFromStore(this.store).pipe(
            startWith(this.store.getState()),
        ).subscribe((state: IStoreState): void => this.componentWillReceiveState(state));

        this.permissions = {
            TemplatesCreateAndPublish: this.permissionsService.canShow("TemplatesCreateAndPublish"),
        };

        this.isSectionExpanded = this.section && Boolean(this.section.invalidQuestionCount);
    }

    ngOnDestroy(): void {
        if (this.storeSubscription) {
            this.storeSubscription.unsubscribe();
        }
    }

    onDragOver = (event): void => {
        if (!this.isSectionExpanded && !this.sectionDragging) {
            this.isSectionExpanded = true;
        }
    }

    onDragStart = (): void => {
        setTimeout(() => {
            this.store.dispatch({ type: TemplateActions.DRAG_SECTION_START });
        }, 10);
    }

    onDragEnd = (): void => {
        this.store.dispatch({ type: TemplateActions.DRAG_SECTION_END });
    }

    launchEditSectionModal(): void {
        const modalData: IEditTemplateSectionResolve = {
            modalTitle: this.translatePipe.transform("SectionName"),
            templateId: getCurrentTemplate(this.store.getState()).id,
            sectionId: this.sectionId,
            sectionName: this.section.name,
        };
        this.modalService.setModalData(modalData);
        this.modalService.openModal("downgradeEditTemplateSectionModalComponent");
    }

    onSectionDelete = (section: IAssessmentTemplateSection): void => {
        const modalData: IDeleteConfirmationModalResolve = {
            modalTitle: this.translatePipe.transform("DeleteSection"),
            confirmationText: this.translatePipe.transform("DeleteSectionConfirmation"),
            cancelButtonText: this.translatePipe.transform("Cancel"),
            submitButtonText: this.translatePipe.transform("Confirm"),
            promiseToResolve: (): ng.IPromise<boolean> => this.templateAction.deleteTemplateSection(getCurrentTemplate(this.store.getState()).id, section.id),
        };

        this.modalService.setModalData(modalData);
        this.modalService.openModal("otDeleteConfirmationModal");
    }

    onAddQuestionClick = (): void => {
        this.templateAction.openTemplateEditorModal({
            templateId: this.templateId,
            sectionId: this.sectionId,
        });
    }

    private componentWillReceiveState(state: IStoreState): void {
        this.section = getSectionById(state, this.sectionId);
        if (this.section) {
            this.sectionName = getTempSectionNameById(state, this.sectionId) || this.section.name;
        }
        const template: ITemplateState = getTemplateState(state);
        this.isTemplateLocked = template.current.isLocked;
        this.isTemplateActive = template.current.isActive;
        this.dragDisabled = template.current.status === TemplateStates.Published || template.current.isLocked || !template.current.isActive;
        this.sectionDragging = template.dragSection;
        this.isReadonly = template.templateMode === TemplateModes.Readonly;
        this.isCreateMode = (template.templateMode === TemplateModes.Create) || (template.templateMode === TemplateModes.Edit && getCurrentTemplate(state).status === TemplateStates.Draft.toUpperCase());
        this.isSectionExpanded = this.isSectionExpanded ? !this.sectionDragging : this.isSectionExpanded;
    }
}
