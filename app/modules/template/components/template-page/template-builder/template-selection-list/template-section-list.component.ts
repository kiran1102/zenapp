// Rxjs
import { Subscription } from "rxjs";
import { startWith } from "rxjs/operators";

// Angular
import {
    Component,
    OnInit,
    Inject,
    Input,
    OnDestroy,
} from "@angular/core";

// External Modules
import {
    remove,
    isEqual,
} from "lodash";

// Interfaces
import {
    IStore,
    IStoreState,
} from "interfaces/redux.interface";
import observableFromStore from "oneRedux/utils/observable-from-store";
import { IEditTemplateSectionResolve } from "interfaces/assessment-template-section.interface";
import { IStringMap } from "interfaces/generic.interface";
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";

// Enums
import { TemplateModes } from "enums/template-modes.enum";
import { BuilderDraggableTypes } from "constants/template-builder.constant";
import { TemplateStates } from "enums/template-states.enum";

// Services
import TemplateAction from "templateServices/actions/template-action.service";
import {
    getBuilderView,
    getTemplateState,
    getCurrentTemplate,
    TemplateActions,
} from "oneRedux/reducers/template.reducer";
import { ModalService } from "sharedServices/modal.service";
import { Permissions } from "modules/shared/services/helper/permissions.service";

// Redux
import { StoreToken } from "tokens/redux-store.token";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

@Component({
    selector: "template-section-list",
    templateUrl: "template-section-list.component.html",
})

export class TemplateSectionListComponent implements OnInit, OnDestroy {

    @Input() templateId: string;
    @Input() sectionSequence: string[];

    isBuilderLoading = true;
    storeSubscription: Subscription;
    isCreateMode: boolean;
    isTemplateActive: boolean;
    isTemplateLocked: boolean;
    permissions: IStringMap<boolean>;
    builderDraggableTypes: IStringMap<string> = BuilderDraggableTypes;

    constructor(
        @Inject("TemplateAction") private templateAction: TemplateAction,
        @Inject(StoreToken) private store: IStore,
        private permissionsService: Permissions,
        private modalService: ModalService,
        private translatePipe: TranslatePipe,
    ) { }

    ngOnInit() {
        this.storeSubscription = observableFromStore(this.store).pipe(
            startWith(this.store.getState()),
        ).subscribe((state: IStoreState): void => this.componentWillReceiveState(state));

        this.permissions = {
            TemplatesCreateAndPublish: this.permissionsService.canShow("TemplatesCreateAndPublish"),
        };
    }

    ngOnDestroy(): void {
        if (this.storeSubscription) {
            this.storeSubscription.unsubscribe();
        }
    }

    onDragOver = (event): void => {
        event.preventDefault();
        event.stopPropagation();
    }

    onDragLeave = (event): void => {
        event.preventDefault();
        event.stopPropagation();
    }

    dndInserted = (event): void | boolean => {
        // ngx-grag-drop-lists does not give an option in the documentation, to override dndDrop,
        // and adds the dropped item to the list. Remove it here once the operation is complete
        remove(this.sectionSequence, (sectionId: string, index: number): boolean =>
            (sectionId === event.item) && (index !== event.index),
        );
    }

    dndDropHandler = (event): void => {
        const newSequence = [...this.sectionSequence];
        const currentSequence = [...this.sectionSequence];
        const currentIndex = newSequence.indexOf(event.item);
        // add the element first at the drop location, and remove it from earlier location
        // and then match
        newSequence.splice(event.index, 0, event.item);
        newSequence.splice(currentIndex, 1);

        if (isEqual(this.sectionSequence, newSequence)) {
            return;
        }

        const nextSequenceId: string = this.sectionSequence[event.index];
        this.templateAction.reorderSection(event.item, nextSequenceId, getCurrentTemplate(this.store.getState()).id)
            .then((success: boolean): void => {
                if (!success) {
                    this.sectionSequence = [...currentSequence];
                    this.store.dispatch({ type: TemplateActions.UPDATE_SECTION_ALL_IDS, sectionAllIds: currentSequence });
                }
            });
    }

    trackBySectionId(index: number, sectionId: string): string {
        return sectionId;
    }

    launchEditSectionModal() {
        const modalData: IEditTemplateSectionResolve = {
            modalTitle: this.translatePipe.transform("SectionName"),
            templateId: getCurrentTemplate(this.store.getState()).id,
        };
        this.modalService.setModalData(modalData);
        this.modalService.openModal("downgradeEditTemplateSectionModalComponent");
    }

    private componentWillReceiveState(state: IStoreState): undefined | void {
        this.sectionSequence = getBuilderView(state).sectionAllIds;
        const template = getTemplateState(state);
        this.isTemplateLocked = template.current.isLocked;
        this.isTemplateActive = template.current.isActive;
        this.isBuilderLoading = template.isFetchingBuilder;
        this.isCreateMode = (template.templateMode === TemplateModes.Create) || (template.templateMode === TemplateModes.Edit && getCurrentTemplate(state).status === TemplateStates.Draft.toUpperCase());
    }
}
