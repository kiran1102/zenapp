// Rxjs
import { SubscriptionLike as ISubscription } from "rxjs";
import { startWith } from "rxjs/operators";

// Angular
import {
    Component,
    OnInit,
    Inject,
    Input,
    OnDestroy,
} from "@angular/core";

// Interfaces
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import { IStringMap } from "interfaces/generic.interface";
import {
    IStore,
    IStoreState,
} from "interfaces/redux.interface";

// Services
import TemplateAction from "templateServices/actions/template-action.service";
import { Permissions } from "modules/shared/services/helper/permissions.service";

// Enums
import { TemplateModes } from "enums/template-modes.enum";

// Redux
import {
    getTemplateState,
    getTemplateTempModelWelcomeMessage,
} from "oneRedux/reducers/template.reducer";
import observableFromStore from "oneRedux/utils/observable-from-store";
import { StoreToken } from "tokens/redux-store.token";

@Component({
    selector: "template-welcome-message",
    templateUrl: "template-welcome-message.component.html",
})

export class TemplateWelcomeMessageComponent implements OnInit, OnDestroy {

    @Input() templateId: string;

    isEditable: boolean;
    isWelcomeMessageExpanded = false;
    welcomeMessage: string;
    private permissions: IStringMap<boolean>;
    private subscription: ISubscription;

    constructor(
        @Inject("TemplateAction") private templateAction: TemplateAction,
        @Inject(StoreToken) private store: IStore,
        private permissionsService: Permissions,
    ) { }

    ngOnInit(): void {
        this.permissions = {
            TemplatesCreateAndPublish: this.permissionsService.canShow("TemplatesCreateAndPublish"),
        };

        this.subscription = observableFromStore(this.store).pipe(
            startWith(this.store.getState()),
        ).subscribe((state: IStoreState): void => this.componentWillReceiveState(state));
    }

    ngOnDestroy(): void {
        if (this.subscription) {
            this.subscription.unsubscribe();
        }
    }

    openWelcomeMessageEditModal(): void {
        if (this.isEditable) {
            this.templateAction.openTemplateWelcomeMessageModal(this.templateId);
        }
    }

    private componentWillReceiveState(state: IStoreState): void {
        this.welcomeMessage = getTemplateTempModelWelcomeMessage(state).welcomeText;
        const template = getTemplateState(state);
        this.isEditable = !template.current.isLocked
            && template.current.isActive
            && this.permissions.TemplatesCreateAndPublish
            && (template.templateMode === TemplateModes.Create
            || template.templateMode === TemplateModes.Edit);
    }

}
