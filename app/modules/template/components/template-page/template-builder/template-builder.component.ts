// Rxjs
import { Subscription } from "rxjs";
import { startWith } from "rxjs/operators";

// Angular
import {
    Component,
    OnInit,
    Inject,
    Input,
    OnDestroy,
} from "@angular/core";

// Enums
import { TemplateStates } from "enums/template-states.enum";

// External Modules
import { isEmpty } from "lodash";

// Interfaces
import { IStringMap } from "interfaces/generic.interface";
import { IStore, IStoreState } from "interfaces/redux.interface";

// Redux
import observableFromStore from "oneRedux/utils/observable-from-store";
import { getTemplateState } from "oneRedux/reducers/template.reducer";
import { StoreToken } from "tokens/redux-store.token";

// service
import { Permissions } from "modules/shared/services/helper/permissions.service";

@Component({
    selector: "template-builder",
    templateUrl: "./template-builder.component.html",
})

export class TemplateBuilder implements OnInit, OnDestroy {
    @Input() templateId: string;
    templateStates = TemplateStates;
    loaderText: string;
    permissions: IStringMap<boolean>;
    showLoader = false;
    storeSubscription: Subscription;
    templateState: string;
    isTemplateLocked: boolean;
    isTemplateActive: boolean;

    constructor(
        @Inject(StoreToken) private store: IStore,
        private permissionsService: Permissions,
    ) { }

    ngOnInit() {
        this.storeSubscription = observableFromStore(this.store).pipe(
            startWith(this.store.getState()),
        ).subscribe((state: IStoreState): void => this.componentWillReceiveState(state));

        this.permissions = {
            TemplatesView: this.permissionsService.canShow("TemplatesView"),
        };
    }

    ngOnDestroy(): void {
        if (this.storeSubscription) {
            this.storeSubscription.unsubscribe();
        }
    }

    private componentWillReceiveState(state: IStoreState): void {
        const templateState = getTemplateState(state);
        this.isTemplateLocked = templateState.current.isLocked;
        this.isTemplateActive = templateState.current.isActive;
        this.loaderText = templateState.loaderText;
        this.showLoader = !isEmpty(this.loaderText) || templateState.isFetchingBuilder;
        this.templateState = templateState.current.status;
    }
}
