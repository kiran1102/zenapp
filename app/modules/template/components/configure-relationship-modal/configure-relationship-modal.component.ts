// External Libraries
import {
    Component,
    Inject,
    OnInit,
} from "@angular/core";
import {
    forEach,
    find,
    includes,
} from "lodash";

// Enums
import { TemplateModes } from "enums/template-modes.enum";
import { TemplateStates } from "enums/template-states.enum";
import { RelationshipLinkType } from "enums/inventory.enum";

// Constants
import { InventorySchemaIds } from "constants/inventory-config.constant";
import { QuestionTypeNames } from "constants/question-types.constant";

// Redux
import {
    getQuestionById,
    getCurrentTemplate,
    getTemplateState,
    getSectionByQuestionId,
    getTemplateTempModel,
} from "oneRedux/reducers/template.reducer";
import { getModalData } from "oneRedux/reducers/modal.reducer";
import { StoreToken } from "tokens/redux-store.token";

// Interfaces
import { IStore } from "interfaces/redux.interface";
import {
    IQuestionDetails,
    IAssessmentTemplateQuestion,
} from "interfaces/assessment-template-question.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";
import {
    IRelationshipTypeParams,
    IAssetRelationshipTypeCheckbox,
} from "interfaces/inventory.interface";
import {
    IInventoryRelationshipContract,
    IRelationshipConfiguration,
    IGetInventoryRelationshipPayload,
} from "modules/template/interfaces/template-related-inventory.interface";

// Services
import { ModalService } from "sharedServices/modal.service";
import TemplateAction from "templateServices/actions/template-action.service";
import { TemplateBuilderService } from "templateServices/template-builder.service";
import { InventoryService } from "inventorySharedModule/shared/services/inventory-api.service";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

@Component({
    selector: "configure-relationship-modal",
    templateUrl: "./configure-relationship-modal.component.html",
})

export class ConfigureRelationshipModalComponent implements OnInit {
    hasExistingRelationship: boolean;
    inventoryQuestions: IQuestionDetails[];
    isEditingPublished: boolean;
    isModalDataReady = false;
    isModalDisabled: boolean;
    isRelationshipTypesFetched = true;
    isSaving = false;
    questionId: string;
    questionModel: IQuestionDetails;
    relationshipTypeOptions: IRelationshipTypeParams[] = [];
    sectionId: string;
    selectedQuestion: IQuestionDetails;
    templateId: string;

    constructor(
        @Inject(StoreToken) private store: IStore,
        @Inject("TemplateBuilderService") private TemplateBuilder: TemplateBuilderService,
        @Inject("TemplateAction") private templateAction: TemplateAction,
        private modalService: ModalService,
        private inventoryService: InventoryService,
        private translatePipe: TranslatePipe,
    ) { }

    ngOnInit() {
        const modalData = getModalData(this.store.getState());
        this.questionId = modalData.questionId;
        this.questionModel = getQuestionById(this.questionId)(this.store.getState());
        this.inventoryQuestions = this.TemplateBuilder.fillRelatedInventoryAndPersonalQuestionsDropdown(modalData.questionId, this.questionModel.questionType);
        this.inventoryQuestions = this.filterRelationshipType(this.inventoryQuestions, this.questionModel);
        this.hasExistingRelationship = this.questionModel.destinationInventoryQuestion;
        this.sectionId = getSectionByQuestionId(this.questionId)(this.store.getState()).id;
        this.templateId = getCurrentTemplate(this.store.getState()).id;
        this.setModalStateVariables();
        if (this.hasExistingRelationship) {
            this.getSelectedQuestion();
        } else {
            this.isModalDataReady = true;
        }
    }

    onQuestionSelect(selectedQuestion: IQuestionDetails): void {
        this.isRelationshipTypesFetched = false;
        this.selectedQuestion = selectedQuestion;
        this.inventoryService.getDynamicAssociationTypes(this.fetchContextOrRelatedInventoryTpe(this.questionModel), this.fetchContextOrRelatedInventoryTpe(selectedQuestion))
            .then((response: IProtocolResponse<IRelationshipTypeParams[]>): void => {
                if (response.result) {
                    this.isRelationshipTypesFetched = true;
                    this.relationshipTypeOptions = response.data;
                }
            });
    }

    updateRelationship(isRemoving: boolean = false): void {
        this.isSaving = true;
        const contract: IInventoryRelationshipContract = isRemoving ? this.configureRemovePayload() : this.configureAddPayload();

        if (this.isEditingPublished) {
            this.templateAction.updateInventoryRelationshipPublished(this.configurePublishedPayload(contract));
            this.updateQuestionRelationshipConfig(!isRemoving);
        } else {
            this.templateAction.updateInventoryRelationshipDraft({ templateId: this.templateId, contract }, this.getPayloadMessages(isRemoving))
                .then((response: boolean) => {
                    if (response) {
                        this.updateQuestionRelationshipConfig(!isRemoving);
                    } else {
                        this.isSaving = false;
                    }
                });
        }
    }

    onCheckboxToggle(selection: IRelationshipTypeParams, isSelected: boolean): void {
        this.relationshipTypeOptions = this.relationshipTypeOptions.map((relationshipTypeOption: IRelationshipTypeParams) => {
            if (relationshipTypeOption.nameKey === selection.nameKey) relationshipTypeOption.selected = isSelected;
            return relationshipTypeOption;
        });
    }

    closeModal(): void {
        this.modalService.closeModal();
        this.modalService.handleModalCallback();
        this.modalService.clearModalData();
    }

    cancel(): void {
        this.closeModal();
    }

    canSave(): boolean {
        return Boolean(this.isRelationshipTypesFetched && this.selectedQuestion) && this.hasRelationships();
    }

    private filterRelationshipType(questions: IQuestionDetails[], currentQuestion: IQuestionDetails): IQuestionDetails[] {
        if (currentQuestion.questionType === QuestionTypeNames.Inventory && currentQuestion.attributeJson.inventoryType === InventorySchemaIds.Entities) {
            return questions.filter((question: IQuestionDetails) => question.questionType !== QuestionTypeNames.PersonalData && question.attributeJson.inventoryType !== InventorySchemaIds.Vendors);
        } else if (currentQuestion.questionType === QuestionTypeNames.Inventory && currentQuestion.attributeJson.inventoryType === InventorySchemaIds.Vendors) {
            return questions.filter((question: IQuestionDetails) => question.attributeJson.inventoryType !== InventorySchemaIds.Entities);
        } else if (currentQuestion.questionType === QuestionTypeNames.PersonalData) {
            return questions.filter((question: IQuestionDetails) => question.attributeJson.inventoryType !== InventorySchemaIds.Entities);
        }
        return questions;
    }

    private setModalStateVariables(): void {
        const { templateMode } = getTemplateState(this.store.getState());
        const isPublished: boolean = getCurrentTemplate(this.store.getState()).status === TemplateStates.Published;
        this.isModalDisabled = isPublished && templateMode === TemplateModes.Readonly;
        this.isEditingPublished = isPublished && templateMode === TemplateModes.Edit;
    }

    private configureRemovePayload(): IInventoryRelationshipContract {
        return {
            destinationInventorySectionId: this.sectionId,
            destinationInventoryQuestionId: this.questionId,
            sourceRelationshipConfiguration: [],
        };
    }

    private configureAddPayload(): IInventoryRelationshipContract {
        const sourceRelationshipConfiguration: IRelationshipConfiguration[] = this.getSourceRelationshipConfiguration();

        if (sourceRelationshipConfiguration) {
            return {
                destinationInventorySectionId: this.sectionId,
                destinationInventoryQuestionId: this.questionId,
                sourceRelationshipConfiguration,
            };
        }
        return;
    }

    private getSourceRelationshipConfiguration(): IRelationshipConfiguration[] {
        const sourceInventorySectionId: string = getSectionByQuestionId(this.selectedQuestion.id)(this.store.getState()).id;
        if (this.relationshipTypeOptions && this.relationshipTypeOptions.length) {
            return this.relationshipTypeOptions
                .filter((relationshipTypeOption: IRelationshipTypeParams) => relationshipTypeOption.selected)
                .map((relationshipTypeOption: IRelationshipTypeParams) => {
                    return {
                        sourceInventorySectionId,
                        sourceInventoryQuestionId: this.selectedQuestion.id,
                        relationshipType: relationshipTypeOption.nameKey as RelationshipLinkType,
                    };
                });
        }
        return [{
            sourceInventorySectionId,
            sourceInventoryQuestionId: this.selectedQuestion.id,
            relationshipType: RelationshipLinkType.Related,
        }];
    }

    private getPayloadMessages(isRemoving: boolean = false): { success: string, error: string } {
        return isRemoving
            ? { success: "Success.RemoveInventoryRelationship", error: "Error.RemoveInventoryRelationship" }
            : { success: "Success.AddInventoryRelationship", error: "Error.AddInventoryRelationship" };
    }

    private updateQuestionRelationshipConfig(isDestinationInventoryQuestion: boolean): void {
        this.templateAction.updateQuestionRelationshipConfig(this.questionId, isDestinationInventoryQuestion);
        this.closeModal();
    }

    private getSelectedQuestion(): void {
        const tempRelationships: IInventoryRelationshipContract[] = getTemplateTempModel(this.store.getState()).inventoryRelations || [];
        const tempRelationshipQuestionIds: string[] = tempRelationships.map((tempRelationship: IInventoryRelationshipContract) => tempRelationship.destinationInventoryQuestionId);
        if (!tempRelationships) {
            this.isModalDataReady = true;
            // To do. Given a temporary fix as discard changes are not happening
        }

        if (this.isEditingPublished && tempRelationships.length && includes(tempRelationshipQuestionIds, this.questionId)) {
            const relationship: IInventoryRelationshipContract = find(tempRelationships, (tempRelationship) => tempRelationship.destinationInventoryQuestionId === this.questionId);
            this.selectedQuestion = this.getSelectedQuestionPublished(relationship);
            this.buildTempRelationshipTypes(relationship);
        } else {
            this.templateAction.getInventoryRelationship(this.templateId, this.sectionId, this.questionId).then((res: IProtocolResponse<IGetInventoryRelationshipPayload[]>) => {
                if (res.result && res.data.length) {
                    const sourceQuestion: IQuestionDetails = getQuestionById(res.data[0].sourceInventoryQuestionId)(this.store.getState());
                    const sectionSequence: number = getSectionByQuestionId(res.data[0].sourceInventoryQuestionId)(this.store.getState()).sequence;
                    this.selectedQuestion = this.TemplateBuilder.applyInventoryQuestionFormatting(sourceQuestion as IAssessmentTemplateQuestion, sectionSequence);
                    this.getRelationshipLinkCheckboxes(sourceQuestion, res.data);
                }
            });
        }
    }

    private getRelationshipLinkCheckboxes(sourceQuestion: IQuestionDetails, selectedRelationshipTypes: IGetInventoryRelationshipPayload[]): void {
        this.inventoryService.getDynamicAssociationTypes(this.fetchContextOrRelatedInventoryTpe(this.questionModel), this.fetchContextOrRelatedInventoryTpe(sourceQuestion))
            .then((response: IProtocolResponse<IRelationshipTypeParams[]>): void => {
                if (response.result) {
                    this.relationshipTypeOptions = response.data.map((relationshipTypeOption: IRelationshipTypeParams) => {
                        forEach(selectedRelationshipTypes, (selectedRelationshipType: IGetInventoryRelationshipPayload) => {
                            if (relationshipTypeOption.nameKey === selectedRelationshipType.relationshipType) relationshipTypeOption.selected = true;
                        });
                        return relationshipTypeOption;
                    });
                }
                this.isModalDataReady = true;
            });
    }

    private hasRelationships(): boolean {
        if (!(this.relationshipTypeOptions && this.relationshipTypeOptions.length)) return true;
        return Boolean(find(this.relationshipTypeOptions, (relationshipTypeOption: IRelationshipTypeParams) => relationshipTypeOption.selected === true));
    }

    private fetchContextOrRelatedInventoryTpe(question: IQuestionDetails): string {
        let inventoryType: string;
        if (question && question.attributeJson) {
            inventoryType = question.attributeJson.inventoryType;
        }
        return question.questionType === QuestionTypeNames.Inventory ? inventoryType : InventorySchemaIds.Elements;
    }

    private configurePublishedPayload(contract: IInventoryRelationshipContract): IInventoryRelationshipContract[] {
        const tempRelationships: IInventoryRelationshipContract[] = getTemplateTempModel(this.store.getState()).inventoryRelations || [];
        return tempRelationships.length && !!find(tempRelationships, (tempRelationship: IInventoryRelationshipContract) => tempRelationship.destinationInventoryQuestionId === this.questionId)
            ? tempRelationships.map((tempRelationship: IInventoryRelationshipContract) => (tempRelationship.destinationInventoryQuestionId === this.questionId) ? contract : tempRelationship)
            : tempRelationships.concat(contract);
    }

    private getSelectedQuestionPublished(relationship: IInventoryRelationshipContract = null): IAssessmentTemplateQuestion {
        return relationship
            ? this.TemplateBuilder.applyInventoryQuestionFormatting(
                getQuestionById(relationship.sourceRelationshipConfiguration[0].sourceInventoryQuestionId)(this.store.getState()),
                getSectionByQuestionId(relationship.sourceRelationshipConfiguration[0].sourceInventoryQuestionId)(this.store.getState()).sequence,
            ) : null;
    }

    private buildTempRelationshipTypes(relationship: IInventoryRelationshipContract): void {
        const selectedRelationshipTypes: IGetInventoryRelationshipPayload[] = [];
        forEach(relationship.sourceRelationshipConfiguration, (sourceRelationshipConfiguration: IRelationshipConfiguration) => {
            selectedRelationshipTypes.push({
                relationshipType: sourceRelationshipConfiguration.relationshipType,
                sourceInventorySectionId: sourceRelationshipConfiguration.sourceInventorySectionId,
                sourceInventoryQuestionId: sourceRelationshipConfiguration.sourceInventoryQuestionId,
                destinationInventorySectionId: relationship.destinationInventorySectionId,
                destinationInventoryQuestionId: relationship.destinationInventoryQuestionId,
            });
        });
        this.getRelationshipLinkCheckboxes(this.selectedQuestion, selectedRelationshipTypes);
    }

}
