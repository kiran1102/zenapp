// Angular
import {
    Component,
    Inject,
    OnInit,
} from "@angular/core";

// RXJS
import { Subject } from "rxjs/internal/Subject";

// vitreus
import { IOtModalContent } from "@onetrust/vitreus";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

@Component({
    selector: "archive-template-modal",
    templateUrl: "./archive-template-modal.component.html",
})

export class ArchiveTemplateModalComponent implements OnInit, IOtModalContent {
    otModalCloseEvent: Subject<boolean>;
    otModalData: { title: string, archiveTemplate: boolean, templateName: string } | null;
    archiveModalMessage: string;
    unarchiveModalMessage: string;

    constructor(private translatePipe: TranslatePipe) { }

    ngOnInit() {
        this.archiveModalMessage = this.translatePipe.transform("AreYouSureToArchiveTemplateName", { TemplateName: this.otModalData.templateName });
        this.unarchiveModalMessage = this.translatePipe.transform("AreYouSureToUnarchiveTemplateName", { TemplateName: this.otModalData.templateName });
    }

    closeModal() {
        this.otModalCloseEvent.next(false);
    }

    archiveTemplate() {
        this.otModalCloseEvent.next(true);
    }

}
