// Angular
import {
    Component,
    OnInit,
    Inject,
    ViewChild,
} from "@angular/core";
import { NgForm } from "@angular/forms";

// Enums
import { KeyCodes } from "enums/key-codes.enum";

// Interfaces
import { IAssessmentTemplateSectionCreateParam } from "interfaces/assessment-template-section.interface";
import { IEditTemplateSectionResolve } from "interfaces/assessment-template-section.interface";
import { IProtocolPacket } from "interfaces/protocol.interface";
import { IStore } from "interfaces/redux.interface";

// Services
import { ModalService } from "sharedServices/modal.service";
import TemplateAction from "templateServices/actions/template-action.service";
import { TemplateAPI } from "templateServices/api/template-api.service";

// Reducers
import { TemplateActions } from "oneRedux/reducers/template.reducer";
import { getModalData } from "oneRedux/reducers/modal.reducer";
import { StoreToken } from "tokens/redux-store.token";

@Component({
    selector: "edit-template-section-modal",
    templateUrl: "edit-template-section-modal.component.html",
})

export class EditTemplateSectionModalComponent implements OnInit {
    modalData: IEditTemplateSectionResolve;
    saveInProgress = false;
    savaAndAddInProgress = false;
    templateStatus: string;

    @ViewChild("editSectionForm") private editSectionForm: NgForm;

    constructor(
        @Inject("TemplateAPI") private TemplateAPI: TemplateAPI,
        @Inject("TemplateAction") private TemplateAction: TemplateAction,
        @Inject(StoreToken) private store: IStore,
        private ModalService: ModalService,
    ) { }

    ngOnInit(): void {
        this.modalData = getModalData(this.store.getState());
    }

    closeModal(): void {
        this.ModalService.closeModal();
        this.ModalService.clearModalData();
    }

    onKeyPress($event: KeyboardEvent): void {
        if ($event.keyCode === KeyCodes.Enter)
            $event.preventDefault();
    }

    onSaveSection(): void {
        if (this.modalData.sectionId) {
            this.updateSection();
        } else {
            this.addSection();
        }
    }

    valuechange(newValue: string): void {
        this.modalData.sectionName = newValue;
    }

    addSection(isSaveAndAddSectionButtonClicked: boolean = false): void {
        if (isSaveAndAddSectionButtonClicked) {
            this.savaAndAddInProgress = true;
            this.saveInProgress = false;
        } else {
            this.saveInProgress = true;
            this.savaAndAddInProgress = false;
        }
        const sectionData: IAssessmentTemplateSectionCreateParam = {
            nextSectionId: null,
            section: {
                name: this.modalData.sectionName,
                description: null,
            },
        };
        this.TemplateAPI.createTemplateSection(this.modalData.templateId, sectionData)
            .then((response: IProtocolPacket): void => {
                if (!response.result) return;
                this.store.dispatch({ type: TemplateActions.ADD_SECTION, section: response.data });
                if (!isSaveAndAddSectionButtonClicked) {
                    this.closeModal();
                } else {
                    this.editSectionForm.form.reset();
                    this.saveInProgress = false;
                    this.savaAndAddInProgress = false;
                }
            });
    }

    private updateSection(): void {
        this.saveInProgress = true;
        this.TemplateAction.updateSection(this.modalData.templateId, this.modalData.sectionId, { name: this.modalData.sectionName })
            .then((result: boolean): void => {
                if (!result) return;
                this.closeModal();
            }).finally(() => {
                this.saveInProgress = false;
            });
    }
}
