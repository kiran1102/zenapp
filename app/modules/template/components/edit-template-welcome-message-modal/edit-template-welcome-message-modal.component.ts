// Rxjs
import {
    BehaviorSubject,
    Observable,
    Subscription,
} from "rxjs";

// Angular
import {
    Component,
    OnInit,
    Inject,
} from "@angular/core";
import { trim } from "lodash";

// Constant
import { WelcomeText } from "constants/welcome-text.constant";
import { Regex } from "constants/regex.constant";

// Interfaces
import { IStore } from "interfaces/redux.interface";

// Services
import { ModalService } from "sharedServices/modal.service";
import TemplateAction from "templateServices/actions/template-action.service";

// Reducers
import { getModalData } from "oneRedux/reducers/modal.reducer";
import {
    getTemplateTempModelWelcomeMessage,
    getCurrentTemplate,
} from "oneRedux/reducers/template.reducer";
import { StoreToken } from "tokens/redux-store.token";

@Component({
    selector: "edit-template-welcome-message-modal",
    templateUrl: "edit-template-welcome-message-modal.component.html",
})

export class EditTemplateWelcomeMessage implements OnInit {
    content: string;
    contentSource = new BehaviorSubject(this.content);
    // contentObservable is to be used for pulling values only and never for pushing
    contentObservable: Observable<string> = this.contentSource.asObservable();
    saveInProgress = false;
    subscription: Subscription;
    templateId: string;
    welcomeMessage = "";

    constructor(
        @Inject("TemplateAction") private templateAction: TemplateAction,
        @Inject(StoreToken) private store: IStore,
        private readonly modalService: ModalService,
    ) {}

    ngOnInit() {
        this.templateId = getModalData(this.store.getState()).templateId;
        this.welcomeMessage = getTemplateTempModelWelcomeMessage(this.store.getState()).welcomeText;
        this.welcomeMessage = this.welcomeMessage || "";
        this.contentSource.next(this.welcomeMessage);
    }

    closeModal(): void {
        this.modalService.closeModal();
        this.modalService.clearModalData();
    }

    onResetToDefaultMessageClick(): void {
        this.welcomeMessage = WelcomeText.WELCOME_TEXT;
        this.contentSource.next(this.welcomeMessage);
    }

    onSaveMessageClick(): void {
        this.saveInProgress = true;
        this.templateAction.saveWelcomeMessage(getCurrentTemplate(this.store.getState()).id, { welcomeText: this.welcomeMessage })
            .then((response: boolean): void => {
                if ( !response ) return;
                this.closeModal();
            }).finally(() => {
                this.saveInProgress = false;
            });
    }

    onWelcomeMessageChange = (message: string): void => {
        this.welcomeMessage = trim(message.replace(Regex.REMOVE_HTML_TAGS, "").replace(Regex.REMOVE_NON_BREAKING_SPACES, "")) ? message : "";
    }
}
