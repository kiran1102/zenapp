// Angular
import {
    Component,
    Input,
    Output,
    EventEmitter,
} from "@angular/core";

@Component({
    selector: "template-header",
    templateUrl: "./template-header.component.html",
})

export class TemplateHeaderComponent {
    @Input() navigationTitle: string;
    @Input() navigationUrl: string;
    @Input() breadcrumbTitle: string;
    @Input() anchorClass: string;

    @Output() private breadcrumbClickCallback: EventEmitter<string> = new EventEmitter<string>();

    navigate(path: string): void {
        this.breadcrumbClickCallback.emit(path);
    }
}
