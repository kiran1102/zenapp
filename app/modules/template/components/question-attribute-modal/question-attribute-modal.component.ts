// Angular
import {
    Component,
    Inject,
    OnInit,
} from "@angular/core";
import { TranslatePipe } from "pipes/translate.pipe";

// 3rd Party
import { find } from "lodash";

// Tokens
import { StoreToken } from "tokens/redux-store.token";

// Redux
import { getModalData } from "oneRedux/reducers/modal.reducer";
import { getCurrentTemplate } from "oneRedux/reducers/template.reducer";

// Interfaces
import { IAssessmentTemplateQuestion,
    IAssessmentTemplateAttribute,
    IQuestionDetails,
    IQuestionBuilderModalData,
} from "interfaces/assessment-template-question.interface";
import { IStringMap } from "interfaces/generic.interface";
import { IStore } from "interfaces/redux.interface";

// Services
import { ModalService } from "sharedServices/modal.service";
import TemplateAction from "templateServices/actions/template-action.service";
import { TemplateBuilderService } from "templateServices/template-builder.service";

@Component({
    selector: "question-attribute-modal",
    templateUrl: "./question-attribute-modal.component.html",
})

export class QuestionAttributeModalComponent implements OnInit {
    attributePlaceholderText: string;
    cancelButtonText: string;
    content: string;
    inventoryType: IStringMap<string>;
    inventoryQuestions: IAssessmentTemplateQuestion[];
    inventoryAttributes: IAssessmentTemplateAttribute[];
    isAttributeDropdownDisabled = true;
    isSubmitButtonDisabled = true;
    modalData: IQuestionBuilderModalData;
    modalTitle: string;
    question: IQuestionDetails = null;
    saveAndAddInProgress = false;
    saveInProgress = false;
    selectedInventoryQuestion: IAssessmentTemplateQuestion;
    selectedAttributeType: IAssessmentTemplateAttribute;
    submitButtonText: string;
    templateId: string;

    constructor(
        @Inject(StoreToken) public store: IStore,
        @Inject("TemplateBuilderService") private TemplateBuilder: TemplateBuilderService,
        @Inject("TemplateAction") private templateAction: TemplateAction,
        public modalService: ModalService,
        private translatePipe: TranslatePipe,
    ) { }

    ngOnInit() {
        this.modalData = getModalData(this.store.getState());
        this.modalTitle = this.modalData.modalTitle || this.translatePipe.transform("SelectAttribute");
        this.submitButtonText = this.modalData.submitButtonText || this.translatePipe.transform("Update");
        this.cancelButtonText = this.modalData.cancelButtonText || this.translatePipe.transform("Cancel");
        this.inventoryQuestions = this.TemplateBuilder.fillInventoryQuestionsDropdown(this.modalData.sectionId, this.modalData.nextQuestionId);
        this.templateId = getModalData(this.store.getState()).templateId;
        this.attributePlaceholderText = "SelectAttribute";
    }

    onInventoryQuestionChange = (questionSelection: IAssessmentTemplateQuestion): void => {
        this.selectedInventoryQuestion = questionSelection;
        this.selectedAttributeType = null;
        this.isAttributeDropdownDisabled = true;
        this.inventoryAttributes = null;
        if (questionSelection) {
            this.attributePlaceholderText = "Loading";
            this.templateAction.getAttributesByInventory(this.templateId, questionSelection.id )
                .then((res: IAssessmentTemplateAttribute[]): void => {
                    const { isAttributeDropdownDisabled, attributePlaceholderText } = this.TemplateBuilder.handleInventoryQuestionChangeAction(res);
                    this.inventoryAttributes = res;
                    this.isAttributeDropdownDisabled = isAttributeDropdownDisabled;
                    this.attributePlaceholderText = attributePlaceholderText;
                });
        }
    }

    onAttributeChange = (attributeSelection: IAssessmentTemplateAttribute): void => {
        this.selectedAttributeType = attributeSelection;
        this.isSubmitButtonDisabled = !attributeSelection;
        const attribute = find(this.inventoryAttributes, { id: attributeSelection.id });
        this.question = this.TemplateBuilder.buildQuestionForSelectedAttribute(attribute, this.selectedInventoryQuestion.id);
    }

    onSaveAttributeQuestion(isSaveAndAddAttributeButtonClicked: boolean): void {
        this.saveAndAddInProgress = isSaveAndAddAttributeButtonClicked;
        this.saveInProgress = !isSaveAndAddAttributeButtonClicked;
        this.isSubmitButtonDisabled = true;
        this.templateAction.createQuestion(this.modalData.nextQuestionId, this.modalData.sectionId, getCurrentTemplate(this.store.getState()).id, this.question)
            .then((response: boolean): void => {
                if (response && isSaveAndAddAttributeButtonClicked) {
                    this.onInventoryQuestionChange(this.selectedInventoryQuestion);
                    this.saveAndAddInProgress = false;
                } else if (response && !isSaveAndAddAttributeButtonClicked) {
                    this.closeModal();
                } else {
                    this.isSubmitButtonDisabled = this.saveInProgress = this.saveAndAddInProgress = false;
                }
            });
    }

    closeModal(): void {
        this.modalService.closeModal();
        this.modalService.clearModalData();
    }
}
