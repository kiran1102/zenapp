// Angular
import {
    Component,
    Input,
    OnInit,
    Inject,
} from "@angular/core";

// 3rd Party
import {
    findIndex,
    find,
} from "lodash";
import { Subject } from "rxjs";

// Interfaces
import {
    IQuestionDetails,
    IMultichoiceDropdownSelection,
} from "interfaces/assessment-template-question.interface";
import { IKeyValue } from "interfaces/generic.interface";
import { IStore } from "interfaces/redux.interface";
import { IQuestionModelOption } from "interfaces/assessment/question-model.interface";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

// Services
import { StoreToken } from "tokens/redux-store.token";
import { AssessmentDetailApiService } from "modules/assessment/services/api/assessment-detail-api.service";

// redux
import { getCurrentOrgId } from "oneRedux/reducers/org.reducer";

// Enum
import { QuestionResponseType } from "enums/assessment/assessment-question.enum";

@Component({
    selector: "question-inventory-preview",
    templateUrl: "./question-inventory-preview.component.html",
})

export class InventoryQuestionPreviewComponent implements OnInit {
    @Input() question: IQuestionDetails;

    isDisabled: false;
    notApplicable: boolean;
    notSure: boolean;
    options: IQuestionModelOption[];
    selectedOptions: IQuestionModelOption[];
    multiSelectEnabled = true;
    autoCloseEnabled = true;
    clearInputOnModelChange = true;
    inputValue: string;
    prependText = this.translatePipe.transform("AddOption");
    optionDetails: IQuestionModelOption[];

    private destroy$ = new Subject();

    constructor(
        @Inject(StoreToken) private store: IStore,
        private translatePipe: TranslatePipe,
        private AssessmentDetailApi: AssessmentDetailApiService,
    ) { }

    ngOnInit(): void {
        this.selectedOptions = [];
    }

    ngOnDestroy() {
        this.destroy$.next();
        this.destroy$.complete();
    }

    onInputChange({ key, value }: IKeyValue<string>): void {
        if (key === "Enter") return;
        this.inputValue = value;
    }

    isOptionInList(questionOptions: IQuestionModelOption[], stringSearch: string): boolean {
        return Boolean(find(questionOptions, (option: IQuestionModelOption): boolean => option.option === stringSearch));
    }

    handleFirstOption(value: string): void {
        if (!this.question.attributeJson.allowOther) return;
        if (value && !this.isOptionInList(this.optionDetails, value)) {
            const selectedOp: IQuestionModelOption = { option: value, id: "", optionType: QuestionResponseType.Others };
            this.selectedOptions = this.selectedOptions.concat(selectedOp);
            this.notApplicable = false;
            this.notSure = false;
        }
        this.inputValue = null;
    }

    onModelChange({ currentValue, change, previousValue }: IMultichoiceDropdownSelection): void {
        if (!currentValue) {
            this.inputValue = "";
        }
        this.selectedOptions = currentValue;
        if (!previousValue || currentValue.length > previousValue.length) {
            const optionIndex: number = findIndex(this.optionDetails, (opt: IQuestionModelOption): boolean => {
                return change.option === opt.option;
            });
            this.optionDetails.splice(optionIndex, 1);
            this.optionDetails = [...this.optionDetails];
        } else {
            this.optionDetails.push(change);
        }
        this.notSure = false;
        this.notApplicable = false;
    }

    toggleNotApplicableAnswer(): void {
        this.notApplicable = !this.notApplicable;
        if (this.notApplicable) {
            this.notSure = false;
            this.selectedOptions = [];
            this.options = this.optionDetails;
        }
    }

    toggleNotSureAnswer(): void {
        this.notSure = !this.notSure;
        if (this.notSure) {
            this.notApplicable = false;
            this.selectedOptions = [];
            this.options = this.optionDetails;
        }
    }
}
