import { IInventoryRelationshipContract } from "modules/template/interfaces/template-related-inventory.interface";

export interface ITemplateUpdateWelcomeText {
    welcomeText: string;
}

export interface ITemplateUpdateMetadata {
    description?: string;
    friendlyName?: string;
    icon: string;
    name: string;
    orgGroupId: string;
}

export interface ITemplateUpdateSettings {
    isLocked?: boolean;
    prepopulateAttributeQuestions?: boolean;
}

export interface ITemplateUpdateQuestion {
    questionId: string;
    content: string;
    friendlyName: string;
    hint: string;
    description: string;
    prePopulateResponse: boolean;
}

export interface ITemplateUpdateSection {
    sectionId: string;
    name: string;
    questions: ITemplateUpdateQuestion[];
}

export interface ITemplateUpdateRequest {
    welcomeText?: ITemplateUpdateWelcomeText;
    metadata?: ITemplateUpdateMetadata;
    settings?: ITemplateUpdateSettings;
    sections?: ITemplateUpdateSection[];
    inventoryRelations?: IInventoryRelationshipContract[];
}
