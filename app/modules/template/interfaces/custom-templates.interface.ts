export interface ICustomTemplate {
    activeState?: string;
    childTemplate?: ICustomTemplate;
    draft?: ICustomTemplateDetails;
    published?: ICustomTemplateDetails;
    rootVersionId: string;
    totalProjectCount: number;
}

export interface ICustomTemplateDetails {
    description?: string;
    hasParents?: boolean;
    icon: string;
    id: string;
    isLocked: boolean;
    isActive?: boolean;
    name: string;
    nextTemplateId: string;
    rootVersionId: string;
    status: string;
    totalProjectCount: number;
    version: number;
    versionProjectCount: number;
}
