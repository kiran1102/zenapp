import { IQuestionAttributes } from "interfaces/assessment-template-question.interface";
import { IDMAssessmentQuestion } from "interfaces/question.interface";
import {
    IStringMap,
    INumberMap,
} from "interfaces/generic.interface";

export interface IAssessmentTemplateSection {
    id: string;
    sequence: number;
    name: string;
    description?: any;
}

export interface IAssessmentTemplateQuestion {
    id: string;
    content: string;
    sequence: number;
    friendlyName: string;
    attributes: IQuestionAttributes;
    parentQuestionId: string;
}

export interface IAssessmentTemplate {
    id: string;
    rootVersionId: string;
    name: string;
    friendlyName?: any;
    sectionAllIds: string[];
    sectionById: IStringMap<IAssessmentTemplateSection>;
    sectionIdQuestionIdsMap: IStringMap<string[]>;
    questionById: IStringMap<IAssessmentTemplateQuestion>;
}

export interface IAssessmentNeedsConfirmation {
    needsConfirmation: boolean;
}

export interface IApproveModalWithRulesSet {
    isForceOverride: boolean;
    excludeRules: boolean;
}

export interface IAssessmentCustomTemplate {
    name: string;
    description: string;
    welcomeText: string;
    icon: string;
    templateType: string;
    orgGroupId: string;
}

export interface IAssessmentGalleryTemplate {
    name: string;
    description: string;
    icon: string;
    templateId: string;
    orgGroupId: string;
}

export interface IQuestionTypes {
    name: string;
    icon: string;
    label: string;
}

export interface ITemplateSettings {
    allowAccessControl: boolean;
    allowedAssessmentCreationOrgHierarchy: string;
}

export interface ITemplate {
    Id: string;
    Name: string;
    Description: string;
    Icon: string;
    Version: string;
    WelcomeText: string;
    NextTemplateId?: string;
    IsLocked?: boolean;
    HasParents?: boolean;
}

export interface ITemplateNameValue {
    id: string;
    name: string;
}

export interface IGalleryType {
    name: any;
}

export interface ITemplateAPIParams {
    id: string;
    version?: string;
}

export interface ITemplateTypesParams {
    types: string[];
}

export interface ITemplateReorderData {
    SectionId: string;
    NextSectionId: string;
    Version: string;
}

export interface ITemplateParams {
    Id: string;
    Published: ITemplate;
}

export interface ITemplateVersion {
    id: string;
    name: string;
    templateVersion: number;
    templateType: string;
    createdDate: Date;
    publishDate?: Date;
    status: string;
    rootVersionId: string;
    friendlyName?: any;
    createdBy: string;
    publishedBy: string;
}

export interface IDMAssessmentTemplate {
    canEdit: boolean;
    createdBy: IStringMap<string>;
    createdDate: string;
    description: string;
    disabledDraggableTypes: INumberMap<boolean>;
    icon: string;
    id: string;
    isLatestPublishedVersion: boolean;
    isRootOrg: boolean;
    name: string;
    nameKey: string;
    parentId: string;
    publishedDate: string;
    publishedBy: IStringMap<string>;
    rooterVersionId: string;
    sections: IDMAssessmentTemplateSection[];
    statusId: number;
    templateType: number;
    templateVersion: number;
    updatedBy: IStringMap<string>;
    updatedDate: string;
    versionList: IDMAssessmentQuestion[];
}

export interface IDMAssessmentTemplateSection {
    description: string;
    disabled: boolean;
    id: number;
    name: string;
    number: number;
    questions: IDMAssessmentQuestion[];
    templateSectionUniqueId: string;
}

export interface ITemplateDropdownItem {
    Id: string;
    Name: string;
}

export interface IProjectDropdownItem extends ITemplateDropdownItem {
    OrgGroup: string;
    OgrGroupId: string;
}

export interface IVersionHistoryModel {
    title: string;
    versionList: ITemplateVersion[];
}
