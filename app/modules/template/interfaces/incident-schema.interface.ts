export interface IIncidentSchema {
    id: string;
    name: string;
    uri: string;
}
