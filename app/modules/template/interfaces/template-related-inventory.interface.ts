import { RelationshipLinkType } from "enums/inventory.enum";

export interface IInventoryRelationshipPayload {
    templateId: string;
    contract: IInventoryRelationshipContract;
}

export interface IInventoryRelationshipContract {
    destinationInventorySectionId: string;
    destinationInventoryQuestionId: string;
    sourceRelationshipConfiguration: IRelationshipConfiguration[];
}

export interface IRelationshipConfiguration {
    sourceInventorySectionId: string;
    sourceInventoryQuestionId: string;
    relationshipType: RelationshipLinkType;
}

export interface IGetInventoryRelationshipPayload {
    id?: string;
    relationshipType: RelationshipLinkType;
    sourceInventorySectionId: string;
    sourceInventoryQuestionId: string;
    destinationInventorySectionId: string;
    destinationInventoryQuestionId: string;
}
