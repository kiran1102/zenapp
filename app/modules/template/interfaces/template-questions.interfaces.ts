export interface ITemplateQuestionMultichoiceOptions {
    attributes: string;
    id: string;
    option: string;
    optionType: string;
    sequence: number;
}
