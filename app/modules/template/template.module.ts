import { NgModule } from "@angular/core";

// Modules
import { SharedModule } from "sharedModules/shared.module";
import { UpgradedLegacyModule } from "sharedModules/upgrade/upgraded-legacy.module";
import { CommonModule } from "@angular/common";
import { HttpClientModule } from "@angular/common/http";
import { FormsModule } from "@angular/forms";
import { ReactiveFormsModule } from "@angular/forms";
import { VitreusModule } from "@onetrust/vitreus";
import { DndListModule } from "ngx-drag-and-drop-lists";
import { OTPipesModule, PIPES } from "modules/pipes/pipes.module";
import { VirtualScrollModule } from "od-virtualscroll";
import { OneVideoModule } from "modules/video/video.module";
import { TemplateSharedModule } from "modules/template-shared/template-shared.module";
import { TmplContextMenu } from "modules/tmpl-context-menu/tmpl-context-menu.module";

// Components
import { ArchiveTemplateModalComponent } from "modules/template/components/archive-template-modal/archive-template-modal.component";
import { TemplateVersionPaneComponent } from "./components/template-page/template-header/template-version-pane/template-version-pane.component";
import { TemplateRuleListComponent } from "./components/template-page/template-rule-list/template-rule-list.component";
import { TemplateOptionDetailsComponent } from "./components/template-page/template-details/template-options/template-option-details/template-option-details.component";
import { TemplateOptionsComponent } from "./components/template-page/template-details/template-options/template-options.component";
import { TemplateRelatedInventoryModalComponent } from "./components/template-related-inventory-modal/template-related-inventory-modal.component";
import { TemplateQuestionBuilderModalComponent } from "./components/question-builder-modal/template-question-builder-modal.component";
import { QuestionConfigurationComponent } from "./components/question-builder-modal/question-configuration/question-configuration.component";
import { ResponseConfigurationComponent } from "./components/question-builder-modal/response-configuration/response-configuration.component";
import { QuestionBuildComponent } from "./components/question-builder-modal/question-build/question-build.component";
import { TemplateWelcomeMessageComponent } from "./components/template-page/template-builder/template-welcome-message/template-welcome-message.component";
import { EditTemplateSectionModalComponent } from "./components/edit-template-section-modal/edit-template-section-modal.component";
import { EditTemplateWelcomeMessage } from "./components/edit-template-welcome-message-modal/edit-template-welcome-message-modal.component";
import { QuestionTypesSelection } from "./components/template-page/template-builder/question-types-selection/question-types-selection.component";
import { TemplateConditionsModalComponent } from "./components/template-conditions-modal/template-conditions-modal.component";
import { QuestionAttributeModalComponent } from "./components/question-attribute-modal/question-attribute-modal.component";
import { CustomTemplatesComponent } from "./components/custom-templates/custom-templates.component";
import { TemplateHeaderComponent } from "./components/template-header/template-header.component";
import { GalleryTemplatesComponent } from "./components/gallery-templates/gallery-templates.component";
import { GalleryUpgradeComponent } from "./components/gallery-templates/gallery-upgrade/gallery-upgrade.component";
import { InventoryQuestionPreviewComponent } from "./components/question-inventory-preview/question-inventory-preview.component";
import { TemplateCreateFormComponent } from "./components/template-create-form/template-create-form.component";
import { TemplateIconListComponent } from "./components/template-icon-list/template-icon-list.component";
import { TemplateSaveChangesModalComponent } from "modules/template/components/template-save-changes-modal/template-save-changes-modal.component";
import { GalleryTemplatePreviewComponent } from "modules/template/components/gallery-templates/gallery-template-preview/gallery-template-preview.component";

// Services
import { QuestionTypeService } from "./services/question-type.service";
import * as TemplateRule from "./components/template-page/template-rule-list/template-rule";
import { QuestionBuilderService } from "templateServices/question-builder.service";
import { TemplateBuilderModalHelperService } from "modules/template/services/template-builder-modal-helper.service";
import {
    getTemplateAPI,
    getTemplateAction,
    getTemplateBuilder,
    getOtIconService,
} from "modules/template/template.provider";
import { TemplatePageComponent } from "modules/template/components/template-page/template-page.component";
import { TemplateSectionRulePaneComponent } from "modules/template/components/template-page/template-rule-list/template-section-rule-pane/template-section-rule-pane.component";
import { BuilderQuestion } from "modules/template/components/template-page/template-builder/template-selection-list/template-section/builder-question-list/builder-question/builder-question.component";
import { BuilderQuestionList } from "modules/template/components/template-page/template-builder/template-selection-list/template-section/builder-question-list/builder-question-list.component";
import { TemplateSectionComponent } from "modules/template/components/template-page/template-builder/template-selection-list/template-section/template-section.component";
import { TemplateBuilder } from "modules/template/components/template-page/template-builder/template-builder.component";
import { TemplateQuestionPreviewComponent } from "modules/template/components/template-page/template-builder/template-selection-list/template-question-preview/template-question-preview.component";
import { QuestionTextboxPreviewComponent } from "modules/template/components/template-page/template-builder/template-selection-list/template-question-preview/question-textbox/question-textbox-preview.component";
import { QuestionMultichoiceSingleSelectButtonPreviewComponent } from "modules/template/components/template-page/template-builder/template-selection-list/template-question-preview/question-multichoice/question-multichoice-single-select-button-preview.component";
import { QuestionMultichoiceSingleSelectDropDownPreviewComponent } from "modules/template/components/template-page/template-builder/template-selection-list/template-question-preview/question-multichoice/question-multichoice-single-select-dropdown-preview.component";
import { QuestionDatePreviewComponent } from "modules/template/components/template-page/template-builder/template-selection-list/template-question-preview/question-date/question-date-preview.component";
import { MultiselectDropdownQuestionComponent } from "modules/template/components/template-page/template-builder/template-selection-list/template-question-preview/question-multiselect-dropdown/multiselect-dropdown-question-preview.component";
import { QuestionYesNoPreviewComponent } from "modules/template/components/template-page/template-builder/template-selection-list/template-question-preview/question-yes-no/question-yes-no-preview.component";
import { QuestionMultichoiceMultiSelectButtonPreviewComponent } from "modules/template/components/template-page/template-builder/template-selection-list/template-question-preview/question-multichoice/question-multichoice-multi-select-button-preview.component";
import { TemplateSectionListComponent } from "modules/template/components/template-page/template-builder/template-selection-list/template-section-list.component";
import { TemplateDetailsEditorComponent } from "modules/template/components/template-page/template-details/template-details-editor.component";
import { TemplateService } from "templateServices/templates.service";
import { TemplatePageHeaderComponent } from "modules/template/components/template-page/template-header/template-page-header.component";
import { AttributeQuestionPreviewComponent } from "modules/template/components/template-page/template-builder/template-selection-list/template-question-preview/question-attribute-preview/question-attribute-preview.component";
import { ConfigureRelationshipModalComponent } from "modules/template/components/configure-relationship-modal/configure-relationship-modal.component";
import { IncidentSchemaApiService } from "modules/template/services/incident-schema-api.service";
import { TemplateCreateHelperService } from "modules/template/services/template-create-helper.service";
import { TemplateCreateService } from "modules/template/services/template-create.service";
import { TemplateCreateApiService } from "modules/template/services/template-create-api.service";
import { TemplateDetailsService } from "modules/template/services/template-details.service";
import { TemplateDetailsHelperService } from "modules/template/services/template-details-helper.service";
import { TemplateDetailsAPIService } from "modules/template/services/template-details-api.service";

export function getTemplateService(injector) {
    return injector.get("TemplateService");
}

const MODULES_TO_IMPORT = [
    SharedModule,
    UpgradedLegacyModule,
    CommonModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    VitreusModule,
    DndListModule,
    OTPipesModule,
    VirtualScrollModule,
    OneVideoModule,
    TemplateSharedModule,
    TmplContextMenu,
];

const NG_COMPONENTS_TO_BE_DOWNGRADED = [
    TemplateRule.EditTemplateRuleModalComponent,
    TemplateRuleListComponent,
    TemplateOptionsComponent,
    TemplateOptionDetailsComponent,
    TemplateVersionPaneComponent,
    TemplateRelatedInventoryModalComponent,
    TemplateQuestionBuilderModalComponent,
    EditTemplateSectionModalComponent,
    EditTemplateWelcomeMessage,
    TemplateBuilder,
    TemplateConditionsModalComponent,
    QuestionAttributeModalComponent,
    TemplatePageComponent,
    CustomTemplatesComponent,
    GalleryTemplatesComponent,
    ConfigureRelationshipModalComponent,
    ArchiveTemplateModalComponent,
    TemplateCreateFormComponent,
];

const NG_COMPONENTS_TO_BE_DECLARED = [
    TemplateOptionDetailsComponent,
    TemplateRule.EditTemplateRuleModalComponent,
    TemplateRule.TemplateRuleComponent,
    TemplateRule.TemplateRuleCreateAssessmentParametersComponent,
    TemplateRule.TemplateRuleSkipParameteresComponent,
    TemplateRule.TemplateRuleCreateRiskParametersComponent,
    TemplateRuleListComponent,
    TemplateOptionsComponent,
    TemplateVersionPaneComponent,
    TemplateSectionRulePaneComponent,
    TemplateRelatedInventoryModalComponent,
    TemplateWelcomeMessageComponent,
    TemplateQuestionBuilderModalComponent,
    EditTemplateSectionModalComponent,
    EditTemplateWelcomeMessage,
    QuestionTypesSelection,
    BuilderQuestion,
    QuestionBuildComponent,
    QuestionConfigurationComponent,
    ResponseConfigurationComponent,
    BuilderQuestionList,
    TemplateSectionComponent,
    TemplateSectionListComponent,
    TemplateBuilder,
    TemplateConditionsModalComponent,
    QuestionAttributeModalComponent,
    CustomTemplatesComponent,
    TemplateHeaderComponent,
    GalleryTemplatesComponent,
    GalleryUpgradeComponent,
    TemplateQuestionPreviewComponent,
    QuestionMultichoiceSingleSelectButtonPreviewComponent,
    QuestionMultichoiceSingleSelectDropDownPreviewComponent,
    QuestionDatePreviewComponent,
    MultiselectDropdownQuestionComponent,
    QuestionYesNoPreviewComponent,
    QuestionMultichoiceMultiSelectButtonPreviewComponent,
    QuestionAttributeModalComponent,
    InventoryQuestionPreviewComponent,
    TemplatePageComponent,
    TemplateDetailsEditorComponent,
    QuestionTextboxPreviewComponent,
    TemplatePageHeaderComponent,
    AttributeQuestionPreviewComponent,
    ConfigureRelationshipModalComponent,
    TemplateRule.TemplateRuleAddApproversParametersComponent,
    ArchiveTemplateModalComponent,
    TemplateCreateFormComponent,
    TemplateIconListComponent,
    TemplateSaveChangesModalComponent,
    GalleryTemplatePreviewComponent,
];

@NgModule({
    imports: MODULES_TO_IMPORT,
    entryComponents: [
        ...NG_COMPONENTS_TO_BE_DOWNGRADED,
        TemplateSaveChangesModalComponent,
    ],
    exports: [
        GalleryTemplatePreviewComponent,
        TemplateQuestionPreviewComponent,
        TemplateWelcomeMessageComponent,
        TemplateSectionListComponent,
        QuestionTextboxPreviewComponent,
        QuestionYesNoPreviewComponent,
        QuestionMultichoiceSingleSelectButtonPreviewComponent,
        QuestionMultichoiceMultiSelectButtonPreviewComponent,
        MultiselectDropdownQuestionComponent,
        QuestionMultichoiceSingleSelectDropDownPreviewComponent,
    ],
    declarations: NG_COMPONENTS_TO_BE_DECLARED,
    providers: [
        { provide: "TemplateAPI", deps: ["$injector"], useFactory: getTemplateAPI },
        { provide: "TemplateAction", deps: ["$injector"], useFactory: getTemplateAction },
        { provide: "TemplateBuilderService", deps: ["$injector"], useFactory: getTemplateBuilder },
        { provide: "OTIconService", deps: ["$injector"], useFactory: getOtIconService },
        { provide: TemplateService, deps: ["$injector"], useFactory: getTemplateService },
        TemplateRule.TemplateRuleService,
        QuestionTypeService,
        QuestionBuilderService,
        TemplateBuilderModalHelperService,
        IncidentSchemaApiService,
        TemplateCreateHelperService,
        TemplateCreateService,
        TemplateCreateApiService,
        TemplateDetailsService,
        TemplateDetailsHelperService,
        TemplateDetailsAPIService,
    ],
})
export class TemplateModule { }
