export enum RelatedInventoryDestination {
    PersonalData = "PersonalData",
    Asset = "Asset",
    ProcessingActivity = "ProcessingActivity",
}
