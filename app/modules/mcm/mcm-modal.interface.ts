export interface IMcmModalTemplate {
    id: string;
    name: string;
    cssFile: string;
    templateFile: string;
}

export interface IMcmModalButton {
    text: string;
    url?: string;
    priority: number;
    functionName: string;
}

export interface IMcmModalResponse {
    title: string;
    name: string;
    viewId?: string;
    subTitle: string;
    mediaUrl?: string;
    contentId?: string;
    mediaTitle?: string;
    mediaSubText?: string;
    titleDescription?: string;
    descriptionList?: string[];
    buttons?: IMcmModalButton[];
    template?: IMcmModalTemplate;
}
