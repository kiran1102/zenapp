// Angular
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { HttpClientModule } from "@angular/common/http";

// Components
import { McmModalComponent } from "./mcm-modal/mcm-modal.component";

// Modules
import { OneVideoModule } from "./../video/video.module";

// Pipes
import { OTPipesModule } from "modules/pipes/pipes.module";

// Vitreus
import {
    OtButtonModule,
    OtModalModule,
} from "@onetrust/vitreus";

// Services
import { McmModalService } from "./mcm-modal.service";
import { SkuManagementModalService } from "modules/sku/shared/sku-management-modal.service";

// Assets
import "images/mcm-modal/data-mapping-feature-image.png";
import "images/mcm-modal/ConsentRecords.gif";
import "images/mcm-modal/default-image.png";
import "images/mcm-modal/SelfServicePortal.gif";
import "images/mcm-modal/UserAndGroups.gif";
import "images/mcm-modal/Integrations.png";
import "images/mcm-modal/BuyNow-Cookie-Pro.gif";

@NgModule({
    imports: [
        CommonModule,
        HttpClientModule,
        OTPipesModule,
        OtButtonModule,
        OtModalModule,
        OneVideoModule,
    ],
    providers: [
        McmModalService,
        SkuManagementModalService,
    ],
    declarations: [
        McmModalComponent,
    ],
    entryComponents: [
        McmModalComponent,
    ],
})
export class McmModalModule { }
