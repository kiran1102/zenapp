// Angular
import { Component, OnInit } from "@angular/core";

// Constants
import { McmModalMediaConstants } from "../mcm-modal.constants";

// Interfaces
import { IMcmModalResponse, IMcmModalButton } from "../mcm-modal.interface";

// rxjs
import { Subject } from "rxjs";

// Vitreus
import { IOtModalContent } from "@onetrust/vitreus";

@Component({
    selector: "mcm-modal",
    templateUrl: "./mcm-modal.component.html",
})
export class McmModalComponent implements IOtModalContent, OnInit {
    modalTitle: string;
    otModalData: IMcmModalResponse;
    otModalCloseEvent: Subject<boolean>;
    mediaFilePath: string;
    mediaKey: string;
    mediaType: string;

    ngOnInit() {
        this.modalTitle = this.otModalData.title;
        this.mediaKey = this.otModalData.mediaUrl;
        this.mediaType = this.mediaKey.split("_")[0];
        this.mediaFilePath = McmModalMediaConstants[this.otModalData.mediaUrl];
        this.otModalData.buttons.sort((btn1, btn2) => {
            return btn1.priority - btn2.priority;
        });
    }

    cancel() {
        this.otModalCloseEvent.next();
    }

    openUpgradeModel() {
        this.otModalCloseEvent.next(true);
    }

    redirect(functionName: string) {
        window.open(this.otModalData.buttons.find((button: IMcmModalButton) => button.functionName === functionName).url);
        this.otModalCloseEvent.next();
    }

}
