export const McmModalMediaConstants = {
    VIDEO_MODULE_INCIDENT_INCIDENT_MEDIA: "https://onetrustprivacy.wistia.com/medias/dx2p7akpy0",
    IMAGE_FEATURE_DATAMAPPING_ASSETMAP_MEDIA: "images/mcm-modal/data-mapping-feature-image.png",
    IMAGE_DEFAULT_MEDIA_CONSTANT: "images/mcm-modal/default-image.png",
    VIDEO_CONSENT_MEDIA_CONSTANT: "https://onetrustprivacy.wistia.com/medias/wfz74lirj0",
    SELF_SERVICE_PORTAL_MEDIA_CONSTANT: "images/mcm-modal/SelfServicePortal.gif",
    GIF_USERS_AND_GROUPS_MEDIA_CONSTANT: "images/mcm-modal/UserAndGroups.gif",
    IMAGE_INTEGRATION_MARKETPLACE_MEDIA_CONSTANT: "images/mcm-modal/Integrations.png",
    IMAGE_LIMIT_DSAR_DATA_SUBJECT_MEDIA: "images/mcm-modal/default-image.png",
    IMAGE_MODULE_COOKIES_COOKIES_MEDIA: "images/mcm-modal/BuyNow-Cookie-Pro.gif",
};
