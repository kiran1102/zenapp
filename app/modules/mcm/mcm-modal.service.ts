// Angular
import {
    Injectable,
    Inject,
} from "@angular/core";

// Components
import { McmModalComponent } from "./mcm-modal/mcm-modal.component";

// Interfaces
import { IMcmModalResponse } from "./mcm-modal.interface";
import { IUser } from "interfaces/user.interface";
import {
    IProtocolConfig,
    IProtocolResponse,
} from "interfaces/protocol.interface";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

// rxjs
import { from, Observable } from "rxjs";

// Reducer
import { getCurrentUser } from "oneRedux/reducers/user.reducer";

// Services
import { ProtocolService } from "modules/core/services/protocol.service";
import { SkuManagementModalService } from "modules/sku/shared/sku-management-modal.service";

// Tokens
import { IStore } from "interfaces/redux.interface";
import { StoreToken } from "tokens/redux-store.token";

// Vitreus
import {
    OtModalService,
    ModalSize,
} from "@onetrust/vitreus";

@Injectable({
    providedIn: "root",
})
export class McmModalService {
    currentUser: IUser;
    constructor(
        @Inject(StoreToken) private store: IStore,
        private translatePipe: TranslatePipe,
        private protocolService: ProtocolService,
        private otModalService: OtModalService,
        private skuManagementModalService: SkuManagementModalService,
    ) { }

    openMcmModal(modalLevel: string) {
        this.currentUser = getCurrentUser(this.store.getState());
        this.getModalContent(modalLevel).subscribe(
            (response: IProtocolResponse<IMcmModalResponse>) => {
                const modalContent: IMcmModalResponse  = response.data;
                this.otModalService
                    .create(
                        McmModalComponent,
                        {
                            isComponent: true,
                            size: ModalSize.MEDIUM,
                        },
                        modalContent,
                    )
                    .subscribe((openUpgradeModel) => {
                        if (openUpgradeModel) {
                            this.skuManagementModalService.openSkuManagementModal(this.currentUser);
                        }
                    });
            },
        );
    }

    getModalContent(modalLevel: string): Observable<IProtocolResponse<IMcmModalResponse>> {
        const url = `/api/content/v1/reference/${modalLevel}`;
        const config: IProtocolConfig = this.protocolService.customConfig("GET", url);
        return from(this.protocolService.http(config));
    }

}
