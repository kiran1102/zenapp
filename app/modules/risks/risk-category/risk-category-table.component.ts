// Angular
import {
    Component,
    OnInit,
    OnDestroy,
} from "@angular/core";

// RxJs
import { Subject } from "rxjs";
import { takeUntil, filter } from "rxjs/operators";

// Const/Enums
import { TableColumnTypes } from "enums/data-table.enum";
import {
    RiskCategoryStatusColorMap,
    RiskCategoryContextMenuOptions,
} from "./risk-category.constants";

// Interface
import {
    IGridColumn,
    IGridSort,
    IGridPaginationParams,
} from "interfaces/grid.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";
import { IRiskCategoryDetail } from "interfaces/risk.interface";
import { IDropdownOption } from "interfaces/one-dropdown.interface";
import {
    IPageableMeta,
    IPageableOf,
} from "interfaces/pagination.interface";

// Service
import { RiskAPIService } from "oneServices/api/risk-api.service";
import { Permissions } from "sharedModules/services/helper/permissions.service";
import { TranslateService } from "@ngx-translate/core";
import { NotificationService } from "sharedModules/services/provider/notification.service";

// Component
import { RiskCategoryAddModalComponent } from "./risk-category-add-modal/risk-category-add-modal.component";
import { IRiskAddCategoryModalData } from "./risk-category-add-modal/risk-category-add-modal.interface";

// Vitreus
import {
    IConfirmModalConfig,
    OtModalService,
    ConfirmModalType,
} from "@onetrust/vitreus";

@Component({
    selector: "risk-category-table",
    templateUrl: "./risk-category-table.component.html",
})

export class RiskCategoryTableComponent implements OnInit, OnDestroy {

    riskCategoryStatusColorMap = RiskCategoryStatusColorMap;
    tableColumnTypes = TableColumnTypes;
    riskCategoryContextMenuOptions = RiskCategoryContextMenuOptions;
    sortData: IGridSort;
    isPageloading = true;
    tableData: IRiskCategoryDetail[] = [];
    menuOptions: IDropdownOption[] = [];
    emptyValue: "----";
    translations: {};
    translateKeys = [
        "Name",
        "Description",
        "Status",
        "EditCategory",
        "Risk.ArchiveCategory",
        "Risk.UnarchiveCategory",
        "Confirm",
        "Cancel",
        "Success",
        "ArchiveCategoryConfirmationMessage",
        "UnarchiveCategoryConfirmationMessage",
        "DoYouWantToContinue",
        "RiskCategoryArchivedSuccessfully",
        "RiskCategoryUnarchivedSuccessfully",
    ];
    columns: IGridColumn[] = null;
    defaultPagination: IGridPaginationParams = { page: 0, size: 20 };
    paginationParams = this.defaultPagination;
    paginationDetail: IPageableMeta;

    private destroy$ = new Subject();

    constructor(
        private translate: TranslateService,
        private riskAPI: RiskAPIService,
        private permissions: Permissions,
        private otModalService: OtModalService,
        private notificationService: NotificationService,
    ) { }

    ngOnInit() {
        this.translate.stream(this.translateKeys).subscribe((t) => {
            this.translations = t;
        });
        this.columns = [
            { name: "CategoryName", sortKey: "CategoryName", type: this.tableColumnTypes.Text, nameKey: this.translations["Name"], valueKey: "nameKey" },
            { name: "Description", sortKey: "Description", type: this.tableColumnTypes.Text, nameKey: this.translations["Description"], valueKey: "descriptionKey" },
            { name: "Status", sortKey: "Status", type: this.tableColumnTypes.Status, nameKey: this.translations["Status"], valueKey: "archived" },
            { name: "", type: this.tableColumnTypes.Action, valueKey: "" },
        ];
        this.getRiskCategories();
    }

    ngOnDestroy() {
        this.destroy$.next();
        this.destroy$.complete();
    }

    addCategory(isEditMode: boolean = false) {
        const modalData: IRiskAddCategoryModalData = {
            isEditMode,
            categoryInformation: { name: "", description: "" },
        };
        this.otModalService.create(
            RiskCategoryAddModalComponent,
            {
                isComponent: true,
                hideClose: true,
            },
            modalData,
        ).pipe(takeUntil(this.destroy$))
            .subscribe((reloadCategoriesList: boolean) => {
                if (reloadCategoriesList) {
                    this.getRiskCategories();
                }
            });
    }

    handleMenuItemClicks(row: IRiskCategoryDetail, optionId: string) {
        const CategoryModalData: IConfirmModalConfig = {
            icon: "",
            translations: {
                cancel: this.translations["Cancel"],
                confirm: this.translations["DoYouWantToContinue"],
                desc:  "",
                submit: this.translations["Confirm"],
                title: "",
            },
            type: null,
        };
        switch (optionId) {
            case this.riskCategoryContextMenuOptions.Edit:
                const modalData: IRiskAddCategoryModalData = {
                    isEditMode: true,
                    categoryId: row.id,
                    categoryInformation: { name: row.name, description: row.description },
                };
                this.otModalService.create(
                    RiskCategoryAddModalComponent,
                    {
                        isComponent: true,
                        hideClose: true,
                    },
                    modalData,
                ).pipe(takeUntil(this.destroy$))
                    .subscribe((reloadCategoriesList: boolean) => {
                        if (reloadCategoriesList) {
                            this.getRiskCategories();
                        }
                    });
                break;
            case this.riskCategoryContextMenuOptions.Archive:
                CategoryModalData.icon = "ot ot-archive";
                const archiveMessage = this.translate.instant(["ArchiveCategoryConfirmationMessage"], { categoryName: row.name });
                CategoryModalData.translations.desc = archiveMessage["ArchiveCategoryConfirmationMessage"];
                CategoryModalData.translations.title = this.translations["Risk.ArchiveCategory"];
                CategoryModalData.type = ConfirmModalType.WARNING;
                this.otModalService
                    .confirm(
                        CategoryModalData,
                    ).pipe(
                        takeUntil(this.destroy$),
                        filter((data: boolean) => data))
                    .subscribe((data: boolean) => {
                        this.riskAPI.archiveRiskCategory(row.id).subscribe((response: IProtocolResponse<void>) => {
                            if (response) {
                                this.notificationService.alertSuccess(this.translations["Success"], this.translations["RiskCategoryArchivedSuccessfully"]);
                                this.getRiskCategories();
                            }
                        });
                    });
                break;
            case this.riskCategoryContextMenuOptions.Unarchive:
                CategoryModalData.icon = "ot ot-undo-action";
                const unarchiveMessage = this.translate.instant(["UnarchiveCategoryConfirmationMessage"], {categoryName: row.name});
                CategoryModalData.translations.desc = unarchiveMessage["UnarchiveCategoryConfirmationMessage"];
                CategoryModalData.translations.title = this.translations["Risk.UnarchiveCategory"];
                CategoryModalData.type = ConfirmModalType.SUCCESS;
                this.otModalService
                    .confirm(
                        CategoryModalData,
                    ).pipe(
                        takeUntil(this.destroy$),
                        filter((data: boolean) => data))
                    .subscribe(() => {
                        this.riskAPI.unarchiveRiskCategory(row.id).subscribe((response: IProtocolResponse<void>) => {
                            if (response) {
                                this.notificationService.alertSuccess(this.translations["Success"], this.translations["RiskCategoryUnarchivedSuccessfully"]);
                                this.getRiskCategories();
                            }
                        });
                    });
                break;
        }
    }

    onToggle(event: boolean, row: IRiskCategoryDetail) {
        if (event) {
            this.menuOptions = [];
            if (!row.archived && !row.seeded) {
                this.menuOptions.push({
                    text: this.translations["EditCategory"],
                    otAutoId: "EditCategory",
                    value: this.riskCategoryContextMenuOptions.Edit,
                });
            }
            if (!row.archived) {
                this.menuOptions.push({
                    text: this.translations["Risk.ArchiveCategory"],
                    otAutoId: "ArchiveCategory",
                    value: this.riskCategoryContextMenuOptions.Archive,
                });
            }
            if (row.archived) {
                this.menuOptions.push({
                    text: this.translations["Risk.UnarchiveCategory"],
                    otAutoId: "UnArchiveCategory",
                    value: this.riskCategoryContextMenuOptions.Unarchive,
                });
            }
        }
    }

    getRiskCategories() {
        this.isPageloading = true;
        this.tableData = [];
        this.riskAPI.fetchRiskCategory(this.paginationParams, null, null)
            .subscribe((response: IProtocolResponse<IPageableOf<IRiskCategoryDetail>>) => {
                if (response.result) {
                    this.tableData = response.data.content;
                    this.paginationDetail = {
                        first: response.data.first,
                        last: response.data.last,
                        number: response.data.number,
                        numberOfElements: response.data.numberOfElements,
                        size: response.data.size,
                        sort: response.data.sort,
                        totalElements: response.data.totalElements,
                        totalPages: response.data.totalPages,
                    };
                }
                this.isPageloading = false;
            });
    }

    pageChanged(page: number) {
        this.fetchPage(page);
    }

    fetchPage(page: number) {
        if (this.paginationDetail.number !== page) {
            this.paginationParams.page = page;
            this.getRiskCategories();
        }
    }
}
