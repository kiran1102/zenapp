export interface IRiskNewCategoryModel {
    name: string;
    description: string;
}

export interface IRiskAddCategoryModalData {
    isEditMode: boolean;
    categoryId?: string;
    categoryInformation?: IRiskNewCategoryModel;
}
