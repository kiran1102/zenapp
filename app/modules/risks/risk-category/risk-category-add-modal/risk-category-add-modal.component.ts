// Angular
import {
    Component,
    OnInit,
} from "@angular/core";
import {
    FormGroup,
    FormBuilder,
    Validators,
} from "@angular/forms";

// RxJs
import { Subject } from "rxjs";

// Services
import { RiskAPIService } from "oneServices/api/risk-api.service";
import { NotificationService } from "sharedModules/services/provider/notification.service";
import { TranslateService } from "@ngx-translate/core";

// Interfaces
import { IOtModalContent } from "@onetrust/vitreus";
import {
    IRiskAddCategoryModalData,
    IRiskNewCategoryModel,
} from "./risk-category-add-modal.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";

@Component({
    selector: "risk-category-add-modal",
    templateUrl: "risk-category-add-modal.component.html",
})

export class RiskCategoryAddModalComponent implements OnInit, IOtModalContent {

    otModalData: IRiskAddCategoryModalData;
    otModalCloseEvent: Subject<boolean>;
    isSaving: boolean;
    isDisabled: boolean;
    isEditMode: boolean;
    isSavingAndAddingNewCategory: boolean;
    riskCategoryAddModel: IRiskNewCategoryModel = null;
    categoryForm: FormGroup = new FormGroup({});
    translations: {};
    translateKeys = [
        "Success",
        "Error",
        "RiskCategoryUpdatedSuccessfully",
        "NewRiskCategoryAddedSuccessfully",
        "PleaseEnterTheRequiredFields",
    ];
    addAnotherSuccess: boolean;

    constructor(
        private riskAPI: RiskAPIService,
        private formBuilder: FormBuilder,
        private notificationService: NotificationService,
        private translate: TranslateService,
    ) {}

    ngOnInit() {
        this.translate.stream(this.translateKeys).subscribe((t) => {
            this.translations = t;
        });
        this.isDisabled = true;
        this.isEditMode = this.otModalData.isEditMode;
        this.initializeForm(this.otModalData.categoryInformation);
    }

    initializeForm(categoryInformation: IRiskNewCategoryModel = { name: "", description: ""}) {
        this.categoryForm = this.formBuilder.group({
           name: [categoryInformation.name, [Validators.required, Validators.maxLength(100)]],
           description: [categoryInformation.description, Validators.maxLength(1000)],
        });
    }

    canSave(): boolean {
        return this.categoryForm.status !== "VALID";
    }

    onSave(addAnotherCategory: boolean) {
        if (this.categoryForm.status !== "INVALID") {
            this.addAnotherSuccess = addAnotherCategory;
            this.isSaving = !addAnotherCategory;
            this.isSavingAndAddingNewCategory = addAnotherCategory;
            this.riskCategoryAddModel = {
                name: this.categoryForm.get("name").value,
                description: this.categoryForm.get("description").value,
            };
            if (this.isEditMode) {
                const categoryId: string = this.otModalData.categoryId;
                this.riskAPI.updateRiskCategory(categoryId, this.riskCategoryAddModel).subscribe((response: IProtocolResponse<void>) => {
                    if (response.result) {
                        this.notificationService.alertSuccess(
                            this.translations["Success"],
                            this.translations["RiskCategoryUpdatedSuccessfully"],
                        );
                        addAnotherCategory ? this.initializeForm() : this.completeModalEvent(true);
                    }
                    this.isSaving = false;
                    this.isSavingAndAddingNewCategory = false;
                });
            } else {
                this.riskAPI.addNewCategory(this.riskCategoryAddModel).subscribe((response: IProtocolResponse<void>) => {
                    if (response.result) {
                        this.notificationService.alertSuccess(
                            this.translations["Success"],
                            this.translations["NewRiskCategoryAddedSuccessfully"],
                        );
                        addAnotherCategory ? this.initializeForm() : this.completeModalEvent(true);
                    }
                    this.isSaving = false;
                    this.isSavingAndAddingNewCategory = false;
                });
            }
        } else {
            this.notificationService.alertError(
                this.translations["Error"],
                this.translations["PleaseEnterTheRequiredFields"],
            );
        }
    }

    completeModalEvent(reloadCategories?: boolean) {
        if (this.addAnotherSuccess && !reloadCategories) {
            this.otModalCloseEvent.next(true);
        } else {
            this.otModalCloseEvent.next(reloadCategories);
        }
        this.otModalCloseEvent.complete();
    }
}
