import { RiskCategoryFilterOperator } from "./risk-category.constants";

export interface IRiskCategoryFilter {
    field: string;
    operator: RiskCategoryFilterOperator;
    value: string[];
}
