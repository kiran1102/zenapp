export const RiskCategoryStatusColorMap = {
    Success: "success",
    Disabled: "disabled",
};

export const RiskCategoryContextMenuOptions = {
    Edit: "Edit",
    Archive: "Archive",
    Unarchive: "Unarchive",
};

export enum RiskCategoryStatus {
    UNARCHIVED  = "UNARCHIVED",
    ARCHIVED = "ARCHIVED",
}

export enum RiskCategoryFilterOperator {
    NOT_EQUAL_TO = "NOT_EQUAL_TO",
    EQUAL_TO = "EQUAL_TO",
}
