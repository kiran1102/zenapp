// Angular
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import {
    FormsModule,
    ReactiveFormsModule,
} from "@angular/forms";

// Modules
import { SharedModule } from "sharedModules/shared.module";
import { VitreusModule } from "@onetrust/vitreus";
import { OTPipesModule } from "modules/pipes/pipes.module";
import { TranslateModule } from "@ngx-translate/core";

// Components
import { RiskCategoryTableComponent } from "./risk-category-table.component";
import { RiskCategoryAddModalComponent } from "./risk-category-add-modal/risk-category-add-modal.component";

@NgModule({
    imports: [
        SharedModule,
        CommonModule,
        VitreusModule,
        OTPipesModule,
        FormsModule,
        ReactiveFormsModule,
        TranslateModule,
    ],
    declarations: [
        RiskCategoryTableComponent,
        RiskCategoryAddModalComponent,
    ],
    exports: [
        RiskCategoryTableComponent,
        RiskCategoryAddModalComponent,
    ],
    entryComponents: [
        RiskCategoryTableComponent,
        RiskCategoryAddModalComponent,
    ],
})
export class RiskCategoryTableModule {}
