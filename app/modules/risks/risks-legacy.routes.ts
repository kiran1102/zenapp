import { Permissions } from "modules/shared/services/helper/permissions.service";

const permissionResolve = (permission: string, fallback = "") => {
    return {
        RiskRepositoryPermission: [
            "Permissions",
            (
                permissions: Permissions,
            ): boolean => {
                if (permissions.canShow(permission)) {
                    return true;
                } else {
                    permissions.goToFallback(fallback);
                }
            },
        ],
    };
};

export default function routes($stateProvider: ng.ui.IStateProvider) {
    $stateProvider
        .state("zen.app.pia.module.risks.views", {
            abstract: true,
            url: "",
            resolve: permissionResolve("NewRiskRepositoryView"),
        })
        .state("zen.app.pia.module.risks", {
            abstract: true,
            url: "risks",
            views: {
                module: {
                    template: "<ui-view></ui-view>",
                },
            },
        })
        .state("zen.app.pia.module.risks.views.riskrepo", {
            url: "/risk-repo",
            template: "<downgrade-risks-wrapper><downgrade-risk-repo></downgrade-risk-repo></downgrade-risks-wrapper>",
        })
        .state("zen.app.pia.module.risks.views.risk-categories", {
            url: "/categories",
            template: "<downgrade-risks-wrapper><downgrade-risk-category-table></downgrade-risk-category-table></downgrade-risks-wrapper>",
            resolve: {
                RiskRepositoryPermission: [
                    "Permissions",
                    (
                        permissions: Permissions,
                    ): boolean => {
                        if (permissions.canShow("RiskCategoryConfig")) {
                            return true;
                        } else {
                            permissions.goToFallback();
                        }
                        return false;
                    },
                ],
            },
        })
        .state("zen.app.pia.module.risks.views.details", {
            url: "/details/:riskId/:tabId",
            params: {
                time: null,
            },
            template: "<downgrade-risks-wrapper><downgrade-risk-register-risk-details></downgrade-risk-register-risk-details></downgrade-risks-wrapper>",
        })
        .state("zen.app.pia.module.risks.views.settings", {
            url: "/settings",
            template: "<downgrade-risks-wrapper><risk-settings-page></risk-settings-page></downgrade-risks-wrapper>",
            resolve: {
                RiskRepositoryPermission: [
                    "Permissions",
                    (
                        permissions: Permissions,
                    ): boolean => {
                        if (permissions.canShow("RiskV2Setting")) {
                            return true;
                        } else {
                            permissions.goToFallback();
                        }
                        return false;
                    },
                ],
            },
        });
}

routes.$inject = ["$stateProvider"];
