import * as angular from "angular";
import routes from "./risks-legacy.routes";

export const risksLegacyModule = angular
    .module("zen.risks", ["ui.router"])
    .config(routes)
    ;
