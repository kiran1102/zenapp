// Angular
import {
    Component,
    OnInit,
    ViewChild,
} from "@angular/core";

// Constants/Enums
import {
    InventoryRiskControlsTabColumnNames,
} from "enums/inventory.enum";
import { TableColumnTypes } from "enums/data-table.enum";
import { GridViewStorageNames } from "sharedModules/enums/grid-view-storage.enum";
import { RiskV2SourceTypes } from "enums/riskV2.enum";

// Interface
import { ITabsNav } from "interfaces/one-tabs-nav.interface";
import { IStringMap } from "interfaces/generic.interface";
import { IRiskDetails } from "interfaces/risk.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";
import { IInventoryControlsTableRow } from "interfaces/inventory.interface";
import {
    IGridPaginationParams,
    IGridSort,
} from "interfaces/grid.interface";
import {
    IPageableMeta,
    IPageableOf,
} from "interfaces/pagination.interface";
// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

// Service
import { StateService } from "@uirouter/angularjs";
import { RiskAPIService } from "oneServices/api/risk-api.service";

// Components
import { RiskControlsListComponent } from "modules/risks/risk-controls-list/risk-controls-list.component";
import { RiskControlsFilterService } from "../risk-controls-list/risk-controls-filter/risk-controls-filter.service";

export const RiskDetailsTabs = {
    Details: "details",
    Controls: "controls",
    Activity: "activity",
};

@Component({
    selector: "risk-register-risk-details",
    templateUrl: "./risk-register-risk-details.component.html",
})

export class RiskRegisterRiskDetailsComponent implements OnInit {

    @ViewChild(RiskControlsListComponent) riskControlsListComponent: RiskControlsListComponent;

    currentTab: string;
    isLoadingDetails: boolean;
    riskDetails: IRiskDetails;
    riskId: string;
    searchString: string;
    pageUrl: string;
    tabs: ITabsNav[];
    tabIds: IStringMap<string> = RiskDetailsTabs;
    isFilterOpen: boolean;
    tableColumnTypes = TableColumnTypes;
    storageKey = GridViewStorageNames.INVENTORY_RISK_CONTROLS_FILTER_LIST;
    currentFilters = [];
    query = [];
    searchText: string;
    sortData: IGridSort;
    tableData: IInventoryControlsTableRow[] = [];
    isPageloading = true;
    defaultPagination: IGridPaginationParams = { page: 0, size: 20 };
    paginationParams = this.defaultPagination;
    paginationDetail: IPageableMeta;
    filterOptions = [
        { sortKey: InventoryRiskControlsTabColumnNames.FRAMEWORK, type: this.tableColumnTypes.Text, name: this.translatePipe.transform("Framework"), cellValueKey: "frameworkName", filterKey: "frameworkId"},
        { sortKey: InventoryRiskControlsTabColumnNames.CATEGORY, type: this.tableColumnTypes.Text, name: this.translatePipe.transform("Category"), cellValueKey: "categoryName", filterKey: "categoryId"},
        { sortKey: InventoryRiskControlsTabColumnNames.STATUS, type: this.tableColumnTypes.Text, name: this.translatePipe.transform("Status"), cellValueKey: "status", filterKey: "status"},
    ];

    private recordId: string;

    constructor(
        private riskAPI: RiskAPIService,
        readonly stateService: StateService,
        private riskControlsFilterService: RiskControlsFilterService,
        readonly translatePipe: TranslatePipe,
    ) { }

    ngOnInit(): void {
        this.currentTab = this.stateService.params.tabId || this.tabIds.Details;
        this.riskId = this.stateService.params.riskId;
        this.pageUrl = "zen.app.pia.module.risks.views.details";
        this.fetchRiskDetailsById();
        this.currentFilters = this.riskControlsFilterService.loadFilters(this.storageKey);
        this.isFilterOpen = false;
    }

    fetchRiskDetailsById() {
        this.isLoadingDetails = true;
        this.riskAPI.fetchRiskV2ById(this.riskId)
            .then((res: IProtocolResponse<IRiskDetails>) => {
                if (res.result) {
                    this.riskDetails = res.data;
                    if (this.riskDetails.sourceType === RiskV2SourceTypes[RiskV2SourceTypes.PIA]) {
                        if (this.currentTab === this.tabIds.Controls) {
                            this.getInventoryRiskControls();
                        } else {
                            this.currentTab = this.tabIds.Details;
                        }
                        this.tabs = this.getTabs(true);
                    } else {
                        this.currentTab = this.tabIds.Details;
                        this.tabs = this.getTabs();
                    }
                }
                this.isLoadingDetails = false;
            });
    }

    goToListView(route: { path: string, params: any }) {
        this.stateService.go("zen.app.pia.module.risks.views.riskrepo");
    }

    toggleFilterDrawer(eventData?: boolean) {
        this.isFilterOpen = !this.isFilterOpen;
    }

    filterApplied(filterData) {
        this.query = this.riskControlsFilterService.buildQuery(filterData);
        this.getInventoryRiskControls();
    }

    handleSortChange(sortData: IGridSort) {
        this.sortData = sortData;
        this.paginationParams.sort = `${sortData.sortedKey},${sortData.sortOrder}`;
        this.getInventoryRiskControls();
    }

    handleSearch(searchText: string) {
        this.searchText = searchText;
        this.getInventoryRiskControls();
    }

    pageChanged(page: number) {
        this.fetchPage(page - 1);
    }

    fetchPage(page: number) {
        if (this.paginationDetail.number !== page) {
            this.paginationParams.page = page;
            this.getInventoryRiskControls();
        }
    }

    getInventoryRiskControls() {
        this.isPageloading = true;
        this.tableData = [];
        this.paginationDetail = null;
        this.riskAPI.fetchInventoryRiskControl(this.riskId, this.paginationParams, this.searchText, this.query)
            .subscribe((response: IProtocolResponse<IPageableOf<IInventoryControlsTableRow>>) => {
                if (response.result) {
                    this.currentTab = this.tabIds.Controls;
                    this.tableData = response.data.content;
                    this.paginationDetail = {
                        first: response.data.first,
                        last: response.data.last,
                        number: response.data.number,
                        numberOfElements: response.data.numberOfElements,
                        size: response.data.size,
                        sort: response.data.sort,
                        totalElements: response.data.totalElements,
                        totalPages: response.data.totalPages,
                    };
                }
                this.isPageloading = false;
            });
    }

    private getTabs(addControlsTab = false): ITabsNav[] {
        const tabs = [{
                id: this.tabIds.Details,
                text: this.translatePipe.transform("Details"),
                action: (option: ITabsNav): void => { this.goToTab(option.id); },
            }];

        if (addControlsTab) {
            tabs.push({
                id: this.tabIds.Controls,
                text: this.translatePipe.transform("Controls"),
                action: (option: ITabsNav): void => { this.goToTab(option.id); },
            });
        }

        tabs.push({
                id: this.tabIds.Activity,
                text: this.translatePipe.transform("Activity"),
                action: (option: ITabsNav): void => { this.goToTab(option.id); },
        });

        return tabs;
    }

    private goToTab(tabId: string): void {
        this.currentTab = tabId;
        if (tabId === this.tabIds.Controls) {
            this.getInventoryRiskControls();
        }
    }
}
