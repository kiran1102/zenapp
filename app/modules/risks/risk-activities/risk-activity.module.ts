// Angular
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

// Modules
import { SharedModule } from "sharedModules/shared.module";
import { VitreusModule } from "@onetrust/vitreus";
import { TranslateModule } from "@onetrust/common";

// Components
import { RiskActivityComponent } from "./components/risk-activity.component";

// Services
import { RiskActivityHelperService } from "./services/risk-activity-helper.service";
import { RiskActivityApiService } from "./services/risk-activity-api.service";

@NgModule({
    imports: [
        SharedModule,
        CommonModule,
        VitreusModule,
        TranslateModule,
    ],
    declarations: [RiskActivityComponent],
    exports: [RiskActivityComponent],
    providers: [
        RiskActivityHelperService,
        RiskActivityApiService,
    ],
})
export class RiskActivityModule {}
