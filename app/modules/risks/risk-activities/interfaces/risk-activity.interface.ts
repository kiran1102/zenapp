// Interfaces
import { IAuditHistoryItem } from "interfaces/audit-history.interface";
import { IPageable } from "interfaces/pagination.interface";

export interface IRiskActivityState {
    riskActivity: IAuditHistoryItem[];
    riskActivityMetaData: IPageable;
    loadingRiskActivityData: boolean;
    loadingViewMoreRiskActivityData: boolean;
}

export interface IRiskFieldLabel {
    oldFieldLabel: string;
    newFieldLabel: string;
}

export interface IRiskActivitiesFieldLabels {
    RISK_LEVEL_CHANGED: IRiskFieldLabel;
    RISK_SCORE_CHANGED: IRiskFieldLabel;
    RISK_IMPACT_LEVEL_CHANGED: IRiskFieldLabel;
    RISK_PROBABILITY_LEVEL_CHANGED: IRiskFieldLabel;
    RISK_OWNER_CHANGED: IRiskFieldLabel;
    RISK_DEADLINE_CHANGED: IRiskFieldLabel;
    RISK_DESCRIPTION_CHANGED: IRiskFieldLabel;
    RISK_RECOMMENDATION_CHANGED: IRiskFieldLabel;
    RISK_REMEDIATION_CHANGED: IRiskFieldLabel;
    RISK_EXCEPTION: IRiskFieldLabel;
    RISK_ORGANIZATION_CHANGED: IRiskFieldLabel;
    RISK_MITIGATED_DATE_CHANGED: IRiskFieldLabel;
    RISK_STATE_CHANGED: IRiskFieldLabel;
    RISK_CONTROL_ADDED: IRiskFieldLabel;
    RISK_CONTROL_STATUS: IRiskFieldLabel;
}
