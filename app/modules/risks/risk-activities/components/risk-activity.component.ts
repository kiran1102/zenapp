// Angular
import {
    Component,
    Input,
    Inject,
} from "@angular/core";

// Rxjs
import { Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

// Constants
import { RiskActivities } from "../constants/risk-activity.constant";
import { Color } from "constants/color.constant";
import { RiskLevelTags } from "constants/risk-level.constant";
import { RiskV2StateTranslationKeys } from "constants/risk-v2.constants";

// Enums
import { RiskControlStatusNames } from "modules/assessment/enums/aa-risk-control.enum";

// Interfaces
import { IPageable } from "interfaces/pagination.interface";
import { IStringMap } from "@onetrust/dsar-components/lib/_shared/interfaces/generic.interface";
import {
    IAuditHistoryItem,
    IFormattedActivity,
} from "interfaces/audit-history.interface";
import {
    IRiskActivityState,
    IRiskActivitiesFieldLabels,
} from "../interfaces/risk-activity.interface";
import { IAuditChange } from "modules/dsar/interfaces/audit-history.interface";

// Services
import { TimeStamp } from "oneServices/time-stamp.service";
import { RiskActivityHelperService } from "../services/risk-activity-helper.service";
import { TranslationsService } from "@onetrust/common";

@Component({
    selector: "risk-activity",
    templateUrl: "./risk-activity.component.html",
})

export class RiskActivityComponent {

    @Input() riskId: string;

    activitiesAscending = true;
    allItemsExpanded = true;
    expandedItemMap: IStringMap<boolean> = {};
    formattedActivities: IFormattedActivity[] = [];
    hasMoreActivities: boolean;
    orderIcon = "ot ot-sort-amount-asc";

    private riskActivityList: IAuditHistoryItem[];
    private destroy$: Subject<void> = new Subject();
    private riskActivityMetaData: IPageable;
    private riskOldNewLabelTranslations: IRiskActivitiesFieldLabels;

    constructor(
        public riskActivityService: RiskActivityHelperService,
        private translationsService: TranslationsService,
        private timeStampService: TimeStamp,
        private translatePipe: TranslatePipe) {
    }

    ngOnInit(): void {
        this.riskOldNewLabelTranslations = this.riskActivityService.getRiskOldNewLabelTranslations();
        this.riskActivityService.setDefaultState();
        this.riskActivityService.getRiskActivity(this.riskId);
        this.riskActivityService.riskActivityState$
            .pipe(takeUntil(this.destroy$))
            .subscribe((response: IRiskActivityState) => {
                this.riskActivityList = response.riskActivity;
                this.riskActivityMetaData = response.riskActivityMetaData;
                this.hasMoreActivities = this.riskActivityMetaData ? !this.riskActivityMetaData.last : false;
                if (this.riskActivityList && this.riskActivityList.length) {
                    this.formatData();
                    this.setExpandedItemMap();
                }
                if (!this.activitiesAscending) {
                    this.reverseActivityOrder();
                }
            });
    }

    toggleActivity(itemId: string): void {
        this.expandedItemMap[itemId] = !this.expandedItemMap[itemId];
    }

    loadMoreRiskActivities(): void {
        if (this.hasMoreActivities) {
            this.riskActivityService.getRiskActivity(this.riskId, this.riskActivityMetaData.number + 1, false);
        }
    }

    expandAll(): void {
        Object.keys(this.expandedItemMap).forEach((key) => this.expandedItemMap[key] = true);
        this.allItemsExpanded = true;
    }

    collapseAll(): void {
        Object.keys(this.expandedItemMap).forEach((key) => this.expandedItemMap[key] = false);
        this.allItemsExpanded = false;
    }

    toggleExpand(): void {
        this.allItemsExpanded ? this.collapseAll() : this.expandAll();
    }

    toggleOrder(): void {
        this.activitiesAscending = !this.activitiesAscending;
        this.orderIcon = this.activitiesAscending ? "ot ot-sort-amount-asc" : "ot ot-sort-amount-desc";
        this.reverseActivityOrder();
    }

    private reverseActivityOrder(): void {
        this.formattedActivities = this.formattedActivities.reverse();
    }

    private setExpandedItemMap(): void {
        this.formattedActivities.forEach((activity: IFormattedActivity) => {
            if (this.expandedItemMap[activity.id] === undefined) {
                this.expandedItemMap[activity.id] = true;
            }
        });
    }

    private formatData(): void {
        this.formattedActivities = [];
        this.riskActivityList.forEach((activity: IAuditHistoryItem, index: number = 0): void => {
            if (activity) {
                this.formattedActivities.push(this.setActivityMetaData(activity, index));
            }
        });
    }

    private setActivityMetaData(activity: IAuditHistoryItem, index: number): IFormattedActivity {
        let riskActivityMetaData: IFormattedActivity;
        const activityUserName: string = activity.userDetail && activity.userDetail.fullName ? activity.userDetail.fullName : this.translatePipe.transform("UserNotFound");

        switch (activity.description) {
            case RiskActivities.RISK_CREATED:
                riskActivityMetaData = {
                    id: index.toString(),
                    title: activity.source ? this.translatePipe.transform("System") : this.translatePipe.transform("UserCreatedRisk", { userName: activityUserName }),
                    fieldLabel: this.translatePipe.transform("FieldName"),
                    description: activity.description,
                    timeStamp: this.timeStampService.formatDate(activity.changeDateTime),
                    icon: "ot ot-plus",
                    iconColor: Color.White,
                    iconBackgroundColor: Color.Grey10,
                    newFieldLabel: this.translatePipe.transform("RiskID"),
                    changes: activity.changes,
                };
                break;
            case RiskActivities.RISK_CONTROL_ADDED:
                riskActivityMetaData = {
                    id: index.toString(),
                    title: activity.source ? this.translatePipe.transform("System") : this.translatePipe.transform("UserAddedControl", { userName: activityUserName }),
                    fieldLabel: this.translatePipe.transform("FieldName"),
                    description: activity.description,
                    timeStamp: this.timeStampService.formatDate(activity.changeDateTime),
                    icon: "ot ot-plus",
                    iconColor: Color.White,
                    iconBackgroundColor: Color.Grey10,
                    showOldNewValueField: true,
                    oldFieldLabel: this.riskOldNewLabelTranslations[activity.description].oldFieldLabel,
                    newFieldLabel: this.riskOldNewLabelTranslations[activity.description].newFieldLabel,
                    changes: activity.changes,
                };
                break;
            case RiskActivities.RISK_CONTROL_REMOVED:
                riskActivityMetaData = {
                    id: index.toString(),
                    title: activity.source ? this.translatePipe.transform("System") : this.translatePipe.transform("UserRemovedControl", { userName: activityUserName }),
                    fieldLabel: this.translatePipe.transform("FieldName"),
                    description: activity.description,
                    timeStamp: this.timeStampService.formatDate(activity.changeDateTime),
                    icon: "ot ot-trash",
                    iconColor: Color.White,
                    iconBackgroundColor: Color.LightRed,
                    newFieldLabel: this.translatePipe.transform("ControlName"),
                    changes: activity.changes,
                };
                break;
            case RiskActivities.RISK_DEADLINE_CHANGED:
            case RiskActivities.RISK_MITIGATED_DATE_CHANGED:
                riskActivityMetaData = {
                    id: index.toString(),
                    title: activity.source ? this.translatePipe.transform("System") : this.translatePipe.transform("UserMadeChanges", { userName: activityUserName }),
                    fieldLabel: this.translatePipe.transform("FieldName"),
                    description: activity.description,
                    timeStamp: this.timeStampService.formatDate(activity.changeDateTime),
                    icon: "ot ot-pencil-square-o",
                    iconColor: Color.White,
                    iconBackgroundColor: Color.Grey10,
                    showOldNewValueField: true,
                    oldFieldLabel: this.riskOldNewLabelTranslations[activity.description].oldFieldLabel,
                    newFieldLabel: this.riskOldNewLabelTranslations[activity.description].newFieldLabel,
                    changes: this.getDateTimeFormat(activity.changes),
                };
                break;
            case RiskActivities.RISK_LEVEL_CHANGED:
            case RiskActivities.RISK_IMPACT_LEVEL_CHANGED:
            case RiskActivities.RISK_PROBABILITY_LEVEL_CHANGED:
            case RiskActivities.RISK_STATE_CHANGED:
            case RiskActivities.RISK_CONTROL_STATUS:
                riskActivityMetaData = {
                    id: index.toString(),
                    title: activity.source ? this.translatePipe.transform("System") : this.translatePipe.transform("UserMadeChanges", { userName: activityUserName }),
                    fieldLabel: this.translatePipe.transform("FieldName"),
                    description: activity.description,
                    timeStamp: this.timeStampService.formatDate(activity.changeDateTime),
                    icon: "ot ot-pencil-square-o",
                    iconColor: Color.White,
                    iconBackgroundColor: Color.Grey10,
                    showOldNewValueField: true,
                    oldFieldLabel: this.riskOldNewLabelTranslations[activity.description].oldFieldLabel,
                    newFieldLabel: this.riskOldNewLabelTranslations[activity.description].newFieldLabel,
                    changes: this.getTranslatedData(activity.changes, activity.description),
                };
                break;
            default:
                riskActivityMetaData = {
                    id: index.toString(),
                    title: activity.source ? this.translatePipe.transform("System") : this.translatePipe.transform("UserMadeChanges", { userName: activityUserName }),
                    fieldLabel: this.translatePipe.transform("FieldName"),
                    description: activity.description,
                    timeStamp: this.timeStampService.formatDate(activity.changeDateTime),
                    icon: "ot ot-pencil-square-o",
                    iconColor: Color.White,
                    iconBackgroundColor: Color.Grey10,
                    showOldNewValueField: true,
                    oldFieldLabel: this.riskOldNewLabelTranslations[activity.description].oldFieldLabel,
                    newFieldLabel: this.riskOldNewLabelTranslations[activity.description].newFieldLabel,
                    changes: activity.changes,
                };
                break;
        }
        return riskActivityMetaData;
    }

    private getTranslatedData(activityChanges: IAuditChange[], activityType: string): IAuditChange[] {
        let oldValue: string;
        let newValue: string;
        switch (activityType) {
            case RiskActivities.RISK_LEVEL_CHANGED:
                oldValue = RiskLevelTags[activityChanges[0].oldValue.name];
                newValue = RiskLevelTags[activityChanges[0].newValue.name];
                break;
            case RiskActivities.RISK_STATE_CHANGED:
                oldValue = RiskV2StateTranslationKeys[activityChanges[0].oldValue.name];
                newValue = RiskV2StateTranslationKeys[activityChanges[0].newValue.name];
                break;
            case RiskActivities.RISK_CONTROL_STATUS:
                oldValue = RiskControlStatusNames[activityChanges[0].oldValue.name];
                newValue = RiskControlStatusNames[activityChanges[0].newValue.name];
                break;
        }
        return [{
            ...activityChanges[0],
            oldValue: {
                name: oldValue ? this.translationsService.translate.instant(oldValue) :
                    this.translationsService.translate.instant(activityChanges[0].oldValue.name),
            },
            newValue: {
                name: newValue ? this.translationsService.translate.instant(newValue) :
                this.translationsService.translate.instant(activityChanges[0].newValue.name),
            },
        }];
    }

    private getDateTimeFormat(activityChanges: IAuditChange[]): IAuditChange[] {
        return [{
            ...activityChanges[0],
            oldValue: {
                name: activityChanges[0].oldValue.name ? this.timeStampService.formatDate(activityChanges[0].oldValue.name) : null,
            },
            newValue: {
                name: activityChanges[0].newValue.name ? this.timeStampService.formatDate(activityChanges[0].newValue.name) : null,
            },
        }];
    }
}
