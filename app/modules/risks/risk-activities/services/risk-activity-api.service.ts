// angular
import { Injectable } from "@angular/core";

// Rxjs
import {
    Observable,
    from,
} from "rxjs";
import { of } from "rxjs";

// Interfaces
import { IPageable } from "interfaces/pagination.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";
import {
    IProtocolConfig,
    IProtocolMessages,
} from "interfaces/protocol.interface";

// services
import { ProtocolService } from "modules/core/services/protocol.service";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

@Injectable()
export class RiskActivityApiService {

    constructor(
        private oneProtocol: ProtocolService,
        private translatePipe: TranslatePipe) {
    }

    getRiskActivity(riskId: string, page: number, size: number): Observable<IPageable> {
        const config: IProtocolConfig = this.oneProtocol.customConfig("GET", `/api/audit/v1/audit/${riskId}/history`, { page, size });
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("CouldNotGetRiskActivities") } };
        return from(this.oneProtocol.http(config, messages)
            .then((response: IProtocolResponse<IPageable>): IPageable | null => {
                return response.result ? response.data : null;
            }),
        );
    }
}
