// Angular
import { Injectable } from "@angular/core";

// Rxjs
import {
    BehaviorSubject,
    Observable,
    Subject,
} from "rxjs";
import { takeUntil } from "rxjs/operators";

// Pipe
import { TranslatePipe } from "pipes/translate.pipe";

// Interfaces
import { IPageable } from "interfaces/pagination.interface";
import {
    IRiskActivityState,
    IRiskActivitiesFieldLabels,
} from "../interfaces/risk-activity.interface";

// Services
import { RiskActivityApiService } from "./risk-activity-api.service";

@Injectable()
export class RiskActivityHelperService {

    riskActivityState$: Observable<IRiskActivityState>;
    private destroy$: Subject<void> = new Subject();
    private defaultState: IRiskActivityState = {
        riskActivity: [],
        riskActivityMetaData: {
            content: [],
            first: true,
            last: true,
            number: 0,
            numberOfElements: 0,
            size: 0,
            totalElements: 0,
            totalPages: 0,
        },
        loadingRiskActivityData: false,
        loadingViewMoreRiskActivityData: false,
    };
    private riskActivityState: BehaviorSubject<IRiskActivityState> = new BehaviorSubject(this.defaultState);

    constructor(
        private riskActivityApiService: RiskActivityApiService,
        private translatePipe: TranslatePipe) {
        this.riskActivityState$ = this.riskActivityState.asObservable();
    }

    getRiskActivity(riskId: string, page: number = 0, viewLessItems: boolean = true, size: number = 8) {
        this.setLoader(viewLessItems, true);
        this.getActivity(riskId, page, size, viewLessItems);
    }

    setDefaultState(): void {
        this.updateState(this.defaultState);
    }

    getRiskOldNewLabelTranslations(): IRiskActivitiesFieldLabels {
        return {
            RISK_LEVEL_CHANGED: {
                oldFieldLabel: this.translatePipe.transform("OldRiskLevel"),
                newFieldLabel: this.translatePipe.transform("NewRiskLevel"),
            },
            RISK_SCORE_CHANGED: {
                oldFieldLabel: this.translatePipe.transform("OldRiskScore"),
                newFieldLabel: this.translatePipe.transform("NewRiskScore"),
            },
            RISK_IMPACT_LEVEL_CHANGED: {
                oldFieldLabel: this.translatePipe.transform("OldImpactLevel"),
                newFieldLabel: this.translatePipe.transform("NewImpactLevel"),
            },
            RISK_PROBABILITY_LEVEL_CHANGED: {
                oldFieldLabel: this.translatePipe.transform("OldProbabilityLevel"),
                newFieldLabel: this.translatePipe.transform("NewProbabilityLevel"),
            },
            RISK_DEADLINE_CHANGED: {
                oldFieldLabel: this.translatePipe.transform("OldDeadline"),
                newFieldLabel: this.translatePipe.transform("NewDeadline"),
            },
            RISK_OWNER_CHANGED: {
                oldFieldLabel: this.translatePipe.transform("OldRiskOwner"),
                newFieldLabel: this.translatePipe.transform("NewRiskOwner"),
            },
            RISK_RECOMMENDATION_CHANGED: {
                oldFieldLabel: this.translatePipe.transform("OldRecommendation"),
                newFieldLabel: this.translatePipe.transform("NewRecommendation"),
            },
            RISK_REMEDIATION_CHANGED: {
                oldFieldLabel: this.translatePipe.transform("OldRemediation"),
                newFieldLabel: this.translatePipe.transform("NewRemediation"),
            },
            RISK_ORGANIZATION_CHANGED: {
                oldFieldLabel: this.translatePipe.transform("OldOrganization"),
                newFieldLabel: this.translatePipe.transform("NewOrganization"),
            },
            RISK_DESCRIPTION_CHANGED: {
                oldFieldLabel: this.translatePipe.transform("OldDescription"),
                newFieldLabel: this.translatePipe.transform("NewDescription"),
            },
            RISK_MITIGATED_DATE_CHANGED: {
                oldFieldLabel: this.translatePipe.transform("OldMitigatedDate"),
                newFieldLabel: this.translatePipe.transform("NewMitigatedDate"),
            },
            RISK_EXCEPTION: {
                oldFieldLabel: this.translatePipe.transform("OldException"),
                newFieldLabel: this.translatePipe.transform("NewException"),
            },
            RISK_STATE_CHANGED: {
                oldFieldLabel: this.translatePipe.transform("OldState"),
                newFieldLabel: this.translatePipe.transform("NewState"),
            },
            RISK_CONTROL_ADDED: {
                oldFieldLabel: this.translatePipe.transform("OldControl"),
                newFieldLabel: this.translatePipe.transform("NewControl"),
            },
            RISK_CONTROL_STATUS: {
                oldFieldLabel: this.translatePipe.transform("OldStatus"),
                newFieldLabel: this.translatePipe.transform("CurrentStatus"),
            },
        };
    }

    private getActivity(riskId: string, page: number, size: number, viewLessItems: boolean): void {
        this.riskActivityApiService.getRiskActivity(riskId, page, size)
            .pipe(takeUntil(this.destroy$))
            .subscribe((response: IPageable): void => {
                if (response) {
                    const newValue = response.content;
                    const riskActivityMetaData = response;
                    delete riskActivityMetaData.content;
                    this.updateState({
                        riskActivity: [...this.getCurrentValue().riskActivity, ...newValue],
                        riskActivityMetaData,
                    });
                    this.setLoader(viewLessItems, false);
                }
            });
    }

    private setLoader(viewLessItems: boolean, isLoading: boolean): void {
        viewLessItems ? this.setLoadingState(isLoading) : this.setLoadingMoreItemsState(isLoading);
    }

    private setLoadingState(isLoading: boolean): void {
        this.updateState({
            ...this.getCurrentValue(),
            loadingRiskActivityData: isLoading,
        });
    }

    private setLoadingMoreItemsState(isLoading: boolean): void {
        this.updateState({
            ...this.getCurrentValue(),
            loadingViewMoreRiskActivityData: isLoading,
        });
    }

    private updateState(state: {}): void {
        this.riskActivityState.next({
            ...this.getCurrentValue(),
            ...state,
        });
    }

    private getCurrentValue(): IRiskActivityState {
        return this.riskActivityState.getValue();
    }
}
