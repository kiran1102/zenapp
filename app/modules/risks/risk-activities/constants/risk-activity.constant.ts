export const RiskActivities = {
    RISK_CREATED: "RISK_CREATED",
    RISK_LEVEL_CHANGED: "RISK_LEVEL_CHANGED",
    RISK_SCORE_CHANGED: "RISK_SCORE_CHANGED",
    RISK_IMPACT_LEVEL_CHANGED: "RISK_IMPACT_LEVEL_CHANGED",
    RISK_PROBABILITY_LEVEL_CHANGED: "RISK_PROBABILITY_LEVEL_CHANGED",
    RISK_OWNER_CHANGED: "RISK_OWNER_CHANGED",
    RISK_DEADLINE_CHANGED: "RISK_DEADLINE_CHANGED",
    RISK_DESCRIPTION_CHANGED: "RISK_DESCRIPTION_CHANGED",
    RISK_RECOMMENDATION_CHANGED: "RISK_RECOMMENDATION_CHANGED",
    RISK_REMEDIATION_CHANGED: "RISK_REMEDIATION_CHANGED",
    RISK_EXCEPTION: "RISK_EXCEPTION",
    RISK_ORGANIZATION_CHANGED: "RISK_ORGANIZATION_CHANGED",
    RISK_MITIGATED_DATE_CHANGED: "RISK_MITIGATED_DATE_CHANGED",
    RISK_STATE_CHANGED: "RISK_STATE_CHANGED",
    RISK_CONTROL_ADDED: "RISK_CONTROL_ADDED",
    RISK_CONTROL_STATUS: "RISK_CONTROL_STATUS",
    RISK_CONTROL_REMOVED: "RISK_CONTROL_REMOVED",
};
