// Angular
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";

// Modules
import { SharedModule } from "sharedModules/shared.module";
import { VitreusModule } from "@onetrust/vitreus";
import { OTPipesModule } from "modules/pipes/pipes.module";
import { FilterModule } from "modules/filter/filter.module";

// Components
import { RiskDetailsFormComponent } from "./risk-details-form.component";

@NgModule({
    imports: [
        SharedModule,
        CommonModule,
        FormsModule,
        VitreusModule,
        OTPipesModule,
        FilterModule,
    ],
    declarations: [
        RiskDetailsFormComponent,
    ],
    exports: [
        RiskDetailsFormComponent,
    ],
})
export class RiskDetailsFormModule {}
