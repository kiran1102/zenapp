// Angular
import {
    Component,
    Input,
    OnInit,
    TemplateRef,
    ViewChild,
} from "@angular/core";
import {
    StateService,
    TransitionService,
    Transition,
} from "@uirouter/core";

// Vitreus
import {
    OtModalService,
} from "@onetrust/vitreus";

// Rxjs
import { Subject } from "rxjs";
import {
    first,
    takeUntil,
} from "rxjs/operators";

// Interface
import {
    IRiskDetails,
    IRiskCategoryBasicInfo,
    IRiskUpdateRequest,
} from "interfaces/risk.interface";
import { IRiskHeatmap } from "interfaces/risks-settings.interface";
import {
    RiskV2States,
    RiskV2ReferenceTypes,
} from "enums/riskV2.enum";
import { IProtocolResponse } from "interfaces/protocol.interface";
import { IOtOrgUserOutput } from "sharedModules/components/ot-org-user/ot-org-user.interface";

// Types
import { RiskV2UpdateActions } from "modules/assessment/types/risk-v2.types";

// Services
import { RiskAPIService } from "oneServices/api/risk-api.service";
import { TimeStamp } from "oneServices/time-stamp.service";
import { Permissions } from "sharedModules/services/helper/permissions.service";
import { LookupService } from "sharedModules/services/helper/lookup.service.ts";

// Constants/Enums
import { RiskV2SourceTypes } from "enums/riskV2.enum";
import {
    InventoryDetailRoutes,
    InventoryDetailsTabs,
} from "constants/inventory.constant";
import {
    InventoryTableIds,
} from "enums/inventory.enum";
import { RiskV2StateTranslationKeys } from "constants/risk-v2.constants";
import { RiskCategoryStatus } from "../risk-category/risk-category.constants";

// Pipes
import { IDateTimeOutputModel } from "@onetrust/vitreus";

@Component({
    selector: "risk-details-form",
    templateUrl: "./risk-details-form.component.html",
})
export class RiskDetailsFormComponent implements OnInit {

    @Input() riskDetails: IRiskDetails;
    @Input() pageUrl: string;

    disableUntilDate = this.timeStampService.getDisableUntilDate();
    dateFormat = this.timeStampService.getDatePickerDateFormat();
    is24Hour: boolean;

    selectedRiskOwner: string;
    defaultRiskDetails: IRiskDetails;
    isLoadingDetails: boolean;
    savingRecord: boolean;
    isEditing: boolean;
    isRiskDetailsEditable: boolean;
    isRecommendationEditable: boolean;
    inventoryId: InventoryTableIds;
    riskV2StateTranslationKeys = RiskV2StateTranslationKeys;
    selectedCategories: IRiskCategoryBasicInfo[];
    categoryList: IRiskCategoryBasicInfo[];
    filteredCategoryOptions: IRiskCategoryBasicInfo[];
    categoryIds: string[] = [];
    riskDetailsUpdateModel: IRiskUpdateRequest;

    @ViewChild("navigationConfirmModal") navigationModalContentRef: TemplateRef<unknown>;

    private destroy$ = new Subject();
    private navigationModalClose$: Subject<boolean>;

    constructor(
        private stateService: StateService,
        private riskAPI: RiskAPIService,
        private timeStampService: TimeStamp,
        private permission: Permissions,
        private $transitions: TransitionService,
        private lookupService: LookupService,
        private otModalService: OtModalService,
    ) {}

    ngOnInit() {
        this.is24Hour = this.timeStampService.getIs24Hour();
        this.defaultRiskDetails = Object.assign({}, this.riskDetails);
        this.selectedRiskOwner = this.riskDetails.riskOwnerId;
        this.isEditing = false;
        this.inventoryId = +this.stateService.params.inventoryId;
        this.setEditPermission();
        this.setTransitionHook();
        this.riskDetailsUpdateModel = null;
        if (this.permission.canShow("RiskViewCategory")) {
            this.initializeCategories();
        }
    }

    setEditPermission() {
        this.isRiskDetailsEditable = (this.riskDetails.sourceType === RiskV2SourceTypes[RiskV2SourceTypes.INVENTORY]);
        const currentState = this.riskDetails.state as any as string;
        this.isRecommendationEditable = (
            (currentState === RiskV2States.IDENTIFIED) ||
            (currentState === RiskV2States.RECOMMENDATION_ADDED));
    }

    selectRiskLevel(value: IRiskHeatmap) {
        this.riskDetails.probabilityLevel = value.probabilityLevel;
        this.riskDetails.probabilityLevelId = value.probabilityLevelId;
        this.riskDetails.impactLevel = value.impactLevel;
        this.riskDetails.impactLevelId = value.impactLevelId;
        this.riskDetails.riskScore = value.riskScore;
        this.riskDetails.levelId = value.levelId;
    }

    onDeadlineChange(event: IDateTimeOutputModel) {
        this.riskDetails.deadline = event.jsdate;
    }

    setRiskDetailsRecommendation(event: string) {
        this.riskDetails.recommendation = event;
    }

    riskDescriptionChange(event: string) {
        this.riskDetails.description = event;
    }

    handleRiskOwnerChange(event: IOtOrgUserOutput) {
        if (event.valid) {
            if (event.selection) {
                this.selectedRiskOwner = event.selection.Id;
                this.riskDetails.riskOwner = event.selection.FullName;
                this.riskDetails.riskOwnerId = (event.selection.Id === event.selection.FullName) ? null : event.selection.Id;
            } else {
                this.selectedRiskOwner = null;
                this.riskDetails.riskOwner = null;
                this.riskDetails.riskOwnerId = null;
            }
        }
    }

    handleSourceClick(event) {
        event.stopPropagation();
        const sourceType = this.riskDetails.sourceType;
        const inventoryIdType = this.riskDetails.source.additionalAttributes.inventoryType;
        const sourceId = this.riskDetails.source.id;
        if (sourceType === RiskV2SourceTypes[RiskV2SourceTypes.PIA]) {
            this.stateService.go("zen.app.pia.module.assessment.detail", {
                assessmentId: sourceId,
            });
        } else {
            this.stateService.go(InventoryDetailRoutes[inventoryIdType], {
                recordId: sourceId,
                inventoryId: inventoryIdType,
                tabId: InventoryDetailsTabs.Details,
            }, { reload: true });
        }
    }

    saveCancelled() {
        this.isEditing = false;
        this.resetToDefaultRiskDetails();
    }

    saveRisk() {
        this.isEditing = false;
        this.isLoadingDetails = true;
        this.savingRecord = true;
        this.setRequestAction();
        this.massageRiskDetailsForUpdateCall();
        this.riskDetailsUpdateModel = this.convertRiskDetailsToUpdateModel(this.riskDetails);
        this.riskAPI.updateRiskDetailsV2New(this.riskDetailsUpdateModel).subscribe((response: IProtocolResponse<void>) => {
            if (this.navigationModalClose$ && !this.navigationModalClose$.closed) {
                this.navigationModalClose$.next(response.result);
            } else if (response.result) {
                this.riskAPI.fetchRiskV2ById(this.riskDetails.id)
                    .then((res: IProtocolResponse<IRiskDetails>) => {
                        if (res.result) this.riskDetails = res.data;
                        this.isLoadingDetails = false;
                        this.savingRecord = false;
                        this.isEditing = false;
                        this.defaultRiskDetails = Object.assign({}, this.riskDetails);
                    });
            } else {
                this.isLoadingDetails = false;
                this.savingRecord = false;
                this.isEditing = false;
            }
        });
    }

    setRequestAction() {
        if ( this.riskDetails.recommendation === this.defaultRiskDetails.recommendation ) {
            this.riskDetails.action = null;
        } else {
            if ( this.riskDetails.recommendation ) {
                this.riskDetails.action = RiskV2States.RECOMMENDATION_ADDED as RiskV2UpdateActions;
            } else {
                this.riskDetails.action = RiskV2States.RECOMMENDATION_REMOVED as RiskV2UpdateActions;
            }
        }
    }

    setTransitionHook() {
        const transitionDeregisterHook = this.$transitions.onExit({ exiting: this.pageUrl }, (transition: Transition) => {
            if (this.isEditing) {
                this.navigationModalClose$ = this.otModalService.create(
                    this.navigationModalContentRef, {});
                return this.navigationModalClose$.pipe(first()).toPromise();
            }
            return true;
        });

        this.destroy$.subscribe(() => {
            transitionDeregisterHook();
        });
    }

    massageRiskDetailsForUpdateCall() {
        this.riskDetails.orgGroupId = this.riskDetails.orgGroup.id;
        this.riskDetails.references = [];
        this.riskDetails.references.push({
            id: this.riskDetails.source.id,
            version: null,
            type: this.riskDetails.source.type,
            name: this.riskDetails.source.name,
            additionalAttributes: { inventoryType: this.riskDetails.source.additionalAttributes.inventoryType },
        });
    }

    toggleOnEditMode(toggle: boolean) {
        if (toggle) this.isEditing = true;
    }

    resetToDefaultRiskDetails() {
        this.riskDetails = Object.assign({}, this.defaultRiskDetails);
        this.selectedRiskOwner = this.riskDetails.riskOwnerId;
    }

    onCancelNavigation() {
        this.navigationModalClose$.next(false);
    }

    onDiscardChanges() {
        this.isEditing = false;
        this.navigationModalClose$.next(true);
    }

    onModelValueChange({currentValue}): void {
        this.selectedCategories = currentValue;
        this.filteredCategoryOptions = this.lookupService.filterOptionsBySelections(this.selectedCategories, this.categoryList, "id");
        this.riskDetails.categories = this.selectedCategories;
    }

    onCategoryLookupInputChange({ key, value }) {
        if (key === "Enter") {
            this.selectedCategories.push(this.filteredCategoryOptions[0]);
            this.onModelValueChange({ currentValue: this.selectedCategories });
            return;
        }
        this.filteredCategoryOptions = this.lookupService.filterOptionsByInput(this.selectedCategories, this.categoryList, value, "id", "name");
    }

    private initializeCategories() {
        this.riskAPI.fetchRiskCategoryListByStatus(RiskCategoryStatus.UNARCHIVED)
            .pipe(takeUntil(this.destroy$))
            .subscribe((response: IProtocolResponse<IRiskCategoryBasicInfo[]>) => {
                if (response.result) {
                    this.categoryList = response.data;
                    this.filteredCategoryOptions = this.lookupService.filterOptionsBySelections(this.riskDetails.categories, this.categoryList, "id");
                }
            });
    }

    private convertRiskDetailsToUpdateModel(riskDetails: IRiskDetails): IRiskUpdateRequest {
        return {
            action: riskDetails.action,
            deadline: riskDetails.deadline,
            reminderDays: riskDetails.reminderDays,
            description: riskDetails.description,
            id: riskDetails.id,
            impactLevelId: riskDetails.impactLevelId,
            levelId: riskDetails.levelId,
            mitigation: riskDetails.mitigation,
            orgGroupId: riskDetails.orgGroup.id,
            probabilityLevelId: riskDetails.probabilityLevelId,
            recommendation: riskDetails.recommendation,
            references: [{
                type: RiskV2ReferenceTypes[RiskV2ReferenceTypes.ASSESSMENT],
                id: riskDetails.source.id,
                name: riskDetails.source.name,
                version: riskDetails.source.version,
            }],
            requestedException: riskDetails.requestedException,
            riskApproversId: riskDetails.riskApproversId,
            riskOwner: riskDetails.riskOwner,
            riskOwnerId: riskDetails.riskOwnerId,
            riskScore: riskDetails.riskScore,
            state: riskDetails.state as string,
            sourceType: riskDetails.sourceType,
            categoryIds: riskDetails.categories && riskDetails.categories.length ? riskDetails.categories.map((category) => category.id) : null,
        };
    }
}
