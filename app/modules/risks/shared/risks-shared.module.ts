// Angular
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";

// Modules
import { SharedModule } from "sharedModules/shared.module";
import { VitreusModule } from "@onetrust/vitreus";
import { OTPipesModule } from "modules/pipes/pipes.module";

// Components
import { RiskModalComponent } from "./risk-modal/risk-modal.component";
import { VendorSharedModule } from "modules/vendor/shared/vendor-shared.module";

@NgModule({
    imports: [
        SharedModule,
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        VitreusModule,
        OTPipesModule,
        VendorSharedModule,
    ],
    declarations: [
        RiskModalComponent,
    ],
    entryComponents: [
        RiskModalComponent,
    ],
})
export class RisksSharedModule {}
