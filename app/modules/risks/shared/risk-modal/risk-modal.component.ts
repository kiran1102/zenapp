// Angular
import {
    Component,
    OnInit,
    OnDestroy,
} from "@angular/core";

// Rxjs
import { Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";

// Enums and Constants
import { RiskV2SourceTypes } from "enums/riskV2.enum";
import { OrgUserTraversal } from "enums/org-user-dropdown.enum";
import { RiskCategoryStatus } from "modules/risks/risk-category/risk-category.constants";

// Interfaces
import {
    IRiskDetails,
    IRiskCategoryBasicInfo,
    IRiskCreateRequest,
} from "interfaces/risk.interface";
import { IRiskHeatmap } from "interfaces/risks-settings.interface";
import { IOtOrgUserOutput } from "sharedModules/components/ot-org-user/ot-org-user.interface";
import { IOtModalContent } from "@onetrust/vitreus";
import { IProtocolResponse } from "interfaces/protocol.interface";

// Services
import { RiskActionService } from "oneServices/actions/risk-action.service";
import { TimeStamp } from "oneServices/time-stamp.service";
import { RiskAPIService } from "oneServices/api/risk-api.service";
import { Permissions } from "sharedModules/services/helper/permissions.service";
import { LookupService } from "sharedModules/services/helper/lookup.service.ts";

@Component({
    selector: "risk-modal",
    templateUrl: "./risk-modal.component.html",
})

export class RiskModalComponent implements OnInit, IOtModalContent, OnDestroy {

    riskModel: IRiskCreateRequest;
    format: string;
    deadline: string = null;
    disabledUntilDate = this.timeStamp.getDisableUntilDate();
    isSubmitting = false;
    orgUserTraversal = OrgUserTraversal;
    otModalCloseEvent: Subject<IRiskDetails>;
    otModalData: { riskModel: IRiskDetails };
    riskV2SourceTypes = RiskV2SourceTypes;
    selectedRiskOwner: string;
    categoryList: IRiskCategoryBasicInfo[];
    filteredCategoryOptions: IRiskCategoryBasicInfo[] = [];
    selectedCategories: IRiskCategoryBasicInfo[] = [];

    destroy$ = new Subject();

    constructor(
        private riskAction: RiskActionService,
        private timeStamp: TimeStamp,
        private permissions: Permissions,
        private riskAPI: RiskAPIService,
        private lookupService: LookupService,
    ) { }

    ngOnInit(): void {
        this.riskModel = this.otModalData.riskModel;
        this.selectedRiskOwner = this.riskModel.riskOwnerId;
        this.format = this.timeStamp.getDatePickerDateFormat();
        if (this.permissions.canShow("RiskViewCategory")) {
            this.initializeCategories();
        }
    }

    ngOnDestroy(): void {
        this.destroy$.next();
        this.destroy$.complete();
    }

    handleRiskOwnerChange(event: IOtOrgUserOutput): void {
        if (event.valid) {
            if (event.selection) {
                this.selectedRiskOwner = event.selection.Id;
                this.riskModel.riskOwner = event.selection.FullName;
                this.riskModel.riskOwnerId = (event.selection.Id === event.selection.FullName) ? null : event.selection.Id;
            } else {
                this.selectedRiskOwner = null;
                this.riskModel.riskOwner = null;
                this.riskModel.riskOwnerId = null;
            }
        }
    }

    onCategoryLookupInputChange({ key, value }) {
        if (key === "Enter") {
            this.selectedCategories.push(this.filteredCategoryOptions[0]);
            this.onModelValueChange({ currentValue: this.selectedCategories });
            return;
        }
        this.filteredCategoryOptions = this.lookupService.filterOptionsByInput(this.selectedCategories, this.categoryList, value, "id", "name");
    }

    handleInputChange(payload: any, prop: string): void {
        if (prop === "deadline") {
            this.deadline = payload.formatted;
            this.riskModel.deadline = payload.jsdate ? payload.jsdate.toISOString() : null;
        } else {
            this.riskModel[prop] = payload;
        }
    }

    closeModal(risk?: IRiskDetails): void {
        this.otModalCloseEvent.next(risk);
        this.otModalCloseEvent.complete();
    }

    onModelValueChange({currentValue}): void {
        this.selectedCategories = currentValue;
        this.filteredCategoryOptions = this.lookupService.filterOptionsBySelections(this.selectedCategories, this.categoryList, "id");
        this.riskModel.categoryIds = this.selectedCategories && this.selectedCategories.length ? this.selectedCategories.map((category) => category.id) : null;
    }

    onRiskHeatmapSelected(riskSelected: IRiskHeatmap): void {
        this.riskModel = {
            ...this.riskModel,
            levelId: riskSelected.levelId,
            impactLevel: riskSelected.impactLevel,
            impactLevelId: riskSelected.impactLevelId,
            probabilityLevel: riskSelected.probabilityLevel,
            probabilityLevelId: riskSelected.probabilityLevelId,
            riskScore: riskSelected.riskScore,
        };
    }

    submitModal(): void {
        this.isSubmitting = true;
        this.riskAction.saveRiskV2(this.riskModel).then((newRisk: IRiskDetails): void => {
            if (newRisk) {
                this.closeModal(newRisk);
            }
            this.isSubmitting = false;
        });
    }

    isSubmitDisabled(): boolean {
        return this.isSubmitting || !this.riskModel.levelId;
    }

    private initializeCategories() {
        this.riskAPI.fetchRiskCategoryListByStatus(RiskCategoryStatus.UNARCHIVED)
            .pipe(takeUntil(this.destroy$))
            .subscribe((response: IProtocolResponse<IRiskCategoryBasicInfo[]>) => {
                if (response.result) {
                    this.categoryList = response.data;
                    this.filteredCategoryOptions = this.lookupService.filterOptionsBySelections(this.selectedCategories, this.categoryList, "id");
                }
            });
    }
}
