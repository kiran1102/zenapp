// Angular
import {
    Component,
    EventEmitter,
    Input,
    OnInit,
    OnDestroy,
    Output,
} from "@angular/core";

// RxJs
import { Subject } from "rxjs";
import {
    takeUntil,
} from "rxjs/operators";

// Interface
import { ILabelValue } from "interfaces/generic.interface";
import {
    IControlLibraryTableColumn,
    IFrameworkListInformation,
} from "controls/shared/interfaces/controls-library.interface";
import { ICategoryInformation } from "modules/controls/shared/interfaces/controls.interface";
import { IFilterOutput, IFilterV2 } from "modules/filter/interfaces/filter.interface";

// Enum
import {
    InventoryRiskControlStatusNames,
    InventoryRiskControlsFilterOperator,
} from "enums/inventory.enum";

// Services
import { GridViewStorageService } from "sharedModules/services/helper/grid-view-storage.service";
import { ControlsLibraryService } from "modules/controls/shared/services/controls-library.service";

// Pipe
import { TranslatePipe } from "pipes/translate.pipe";

@Component({
    selector: "risk-controls-filter",
    templateUrl: "./risk-controls-filter.component.html",
    styles: [`
        :host {
            flex: 1 1 auto;
        }
    `],
})

export class RiskControlsFilterComponent implements OnInit, OnDestroy {
    @Input() fields;
    @Input() currentFilters;
    @Input() storage;

    @Output() filterApplied = new EventEmitter();
    @Output() filterCanceled = new EventEmitter();

    selectedFilters = [];
    inventoryRiskControlStatusNames = InventoryRiskControlStatusNames;
    filterOperators = [
        {
            label: this.translatePipe.transform("EqualTo"),
            value: InventoryRiskControlsFilterOperator.EQUAL_TO,
            labelKey: "EqualTo",
        },
        {
            label: this.translatePipe.transform("NotEqualTo"),
            value: InventoryRiskControlsFilterOperator.NOT_EQUAL_TO,
            labelKey: "NotEqualTo",
        },
    ];
    currentOperator = this.filterOperators[0];
    currentFilterValues = [];
    currentFilterValueType: string;
    query = [];

    // Filter Values
    frameworks = [];
    categories = [];
    statuses = [ {
        value: this.inventoryRiskControlStatusNames.Pending,
        label: this.translatePipe.transform(this.inventoryRiskControlStatusNames.Pending),
        labelKey: this.inventoryRiskControlStatusNames.Pending,
        },
        {
            value: this.inventoryRiskControlStatusNames.Implemented,
            label: this.translatePipe.transform(this.inventoryRiskControlStatusNames.Implemented),
            labelKey: this.inventoryRiskControlStatusNames.Implemented,
        },
        {
            value: this.inventoryRiskControlStatusNames.NotDoing,
            label: this.translatePipe.transform(this.inventoryRiskControlStatusNames.NotDoing),
            labelKey: this.inventoryRiskControlStatusNames.NotDoing,
        }];

    // Toggle popup
    closeFilter$ = new Subject<boolean>();

    private destroy$ = new Subject();

    constructor(
        private translatePipe: TranslatePipe,
        private controlsLibraryService: ControlsLibraryService,
        private gridViewStorageService: GridViewStorageService,
    ) { }

    ngOnInit() {
        if (this.currentFilters) this.selectedFilters = this.currentFilters;
        this.fetchFrameworks();
        this.fetchCategories();
    }

    ngOnDestroy(): void {
        this.destroy$.next();
        this.destroy$.complete();
    }

    setFilterValues(selectedField: ILabelValue<string>) {
        if (!selectedField) return;
        const selectedColumn = this.fields.find((column) => column.cellValueKey === selectedField.value);
        this.currentFilterValueType = selectedColumn.type.charAt(0).toUpperCase() + selectedColumn.type.slice(1);

        switch (selectedColumn.sortKey) {
            case "frameworkName":
                this.currentFilterValues = this.frameworks;
                break;
            case "controlCategoryName":
            case "categoryName":
                this.currentFilterValues = this.categories;
                break;
            case "status":
                this.currentFilterValues = this.statuses;
                break;
        }
    }

    selectOperator(operator?) {
        this.currentOperator = operator.value || this.currentOperator;
    }

    applyFilters(filters) {
        if (!filters) return;
        this.closeFilter$.next(false);
        this.selectedFilters = this.deserializeFilters(filters);
        this.gridViewStorageService.saveLocalStorageFilters(this.storage, this.selectedFilters);
        this.filterApplied.emit(this.selectedFilters);
    }

    deserializeFilters(filters: IFilterOutput[]): Array<IFilterV2<IControlLibraryTableColumn, ILabelValue<string>, ILabelValue<string>>> {
        return filters.map((item) => {
            const matchingColumn = this.fields.find((column): boolean => column.cellValueKey === item.field.value);
            return ({
                field: matchingColumn,
                operator: item.operator || this.filterOperators[0],
                values: item.values,
                type: item.type,
            } as any);
        });
    }

    removeFilter(filters) {
        this.selectedFilters = this.deserializeFilters(filters.currentList);
        this.selectedFilters.splice(filters.index, 1);
    }

    addFilter(filters) {
        filters.currentList.push(filters.filter);
        this.selectedFilters = this.deserializeFilters(filters.currentList);
    }

    removeAllFilters() {
        this.selectedFilters = [];
    }

    cancel() {
        this.selectedFilters = this.currentFilters;
        this.closeFilter$.next(false);
        this.filterCanceled.emit(this.currentFilters);
    }

    getFieldLabelValue(column): ILabelValue<string> {
        return { label: column.name, value: column.sortKey };
    }

    getOperatorLabelValue(operator: ILabelValue<string>): ILabelValue<string> {
        return operator;
    }

    // Fetch filter values
    fetchFrameworks() {
        this.controlsLibraryService.retrieveAllFrameworks();
        this.controlsLibraryService.controlsFrameworks$.pipe(
            takeUntil(this.destroy$),
        ).subscribe((vendorFrameworks: IFrameworkListInformation[]) => {
            if (vendorFrameworks) {
                this.frameworks = vendorFrameworks.map((item) => ({ value: item.frameworkId, label: item.frameworkName }));
                this.frameworks.push({
                    value: "blank",
                    label: this.translatePipe.transform("Blanks"),
                    labelKey: "Blanks",
                });
            }
        });
    }

    fetchCategories() {
        this.controlsLibraryService.retrieveAllCategories();
        this.controlsLibraryService.controlsAllCategories$.pipe(
            takeUntil(this.destroy$),
        ).subscribe((categoriesResponse: ICategoryInformation[]) => {
            if (categoriesResponse) {
                this.categories = categoriesResponse.map((item) => ({ value: item.id, label: item.name }));
            }
            this.categories.push({
                value: "blank",
                label: this.translatePipe.transform("Blanks"),
                labelKey: "Blanks",
            });
        });
    }
}
