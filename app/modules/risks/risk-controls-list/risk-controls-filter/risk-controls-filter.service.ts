import { Injectable } from "@angular/core";

// Services
import { GridViewStorageService } from "sharedModules/services/helper/grid-view-storage.service";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

@Injectable({
    providedIn: "root",
})
export class RiskControlsFilterService {

    constructor(
        private browserStorageListService: GridViewStorageService,
        private translatePipe: TranslatePipe,
    ) { }

    buildQuery(filters) {
        const query = [];
        if (filters) {
            filters.forEach((filter) => {
                const operator = filter.operator.value;
                const values = [];
                const field = filter.field.filterKey || filter.field.sortKey;

                filter.values.forEach((item) => {
                    values.push(item.value !== "blank" ? item.value : null);
                });

                query.push({
                    field,
                    operator,
                    value: values,
                });
            });
        }
        return query;
    }

    loadFilters(storageKey) {
        const storage = this.browserStorageListService.getBrowserStorageList(storageKey);
        if (storage && storage.filters) {
            storage.filters.forEach((part, index) => {
                const filter = storage.filters[index];
                filter.field.name = filter.field.columnKey ? this.translatePipe.transform(filter.field.columnKey) : filter.field.name;
                filter.operator.label = filter.operator.labelKey ? this.translatePipe.transform(filter.operator.labelKey) : filter.operator.label;
                const valueIndex = filter.values.findIndex((item) => item.value === "blank");
                if (valueIndex >= 0) {
                    filter.values[valueIndex].label = filter.values[valueIndex].labelKey ? this.translatePipe.transform(filter.values[valueIndex].labelKey) : filter.values[valueIndex].label;
                }
            });
            return storage.filters;
        }
        return [];
    }
}
