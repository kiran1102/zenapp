// Angular
import {
    Component,
    OnInit,
    Input,
    Output,
    EventEmitter,
} from "@angular/core";

// Interface
import {
    IGridColumn,
    IGridSort,
} from "interfaces/grid.interface";
import {
    IInventoryControlsTableRow,
} from "interfaces/inventory.interface";

// Const/Enums
import { TableColumnTypes } from "enums/data-table.enum";
import { RiskControlsStatusColorMap } from "constants/inventory.constant";
import { InventoryRiskControlsTabColumnNames } from "enums/inventory.enum";
import {
    InventoryDetailsTabs,
    InventoryDetailRoutes,
} from "constants/inventory.constant";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

// Services
import { StateService } from "@uirouter/core";

@Component({
    selector: "risk-controls-list",
    templateUrl: "./risk-controls-list.component.html",
})

export class RiskControlsListComponent implements OnInit {

    @Input() controlsTableData: IInventoryControlsTableRow[];
    @Input() recordId: string;
    @Input() sortData: IGridSort;

    @Output() handleTableSort: EventEmitter<IGridSort> = new EventEmitter<IGridSort>();

    tableColumnTypes = TableColumnTypes;
    riskControlsStatusColorMap = RiskControlsStatusColorMap;
    inventoryId: number;

    columns: IGridColumn[] = [
        { name: "ControlID", sortKey: "controlIdentifier", type: this.tableColumnTypes.Text, nameKey: this.translatePipe.transform("ID"), valueKey: InventoryRiskControlsTabColumnNames.CONTROL_ID, style: { width: "10rem", padding: "1rem" } },
        { name: "Name", sortKey: "controlName", type: this.tableColumnTypes.Text, nameKey: this.translatePipe.transform("Name"), valueKey: InventoryRiskControlsTabColumnNames.NAME, style: { width: "25rem" } },
        { name: "Framework", sortKey: "frameworkName", type: this.tableColumnTypes.Text, nameKey: this.translatePipe.transform("Framework"), valueKey: InventoryRiskControlsTabColumnNames.FRAMEWORK, style: { width: "8rem" } },
        { name: "Category", sortKey: "categoryName", type: this.tableColumnTypes.Text, nameKey: this.translatePipe.transform("Category"), valueKey: InventoryRiskControlsTabColumnNames.CATEGORY, style: { width: "15rem" } },
        { name: "Status", sortKey: "status", type: this.tableColumnTypes.Text, nameKey: this.translatePipe.transform("Status"), valueKey: InventoryRiskControlsTabColumnNames.STATUS, style: { width: "15rem" } },
    ];

    constructor(
        private translatePipe: TranslatePipe,
        private stateService: StateService,
    ) { }

    ngOnInit(): void {
        this.inventoryId = Number.parseInt(this.stateService.params.inventoryId, 10);
    }

    handleSortChange(sortData: IGridSort) {
        this.handleTableSort.emit(sortData);
    }
}
