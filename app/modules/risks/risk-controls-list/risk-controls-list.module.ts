// Angular
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

// Modules
import { SharedModule } from "sharedModules/shared.module";
import { VitreusModule } from "@onetrust/vitreus";
import { OTPipesModule } from "modules/pipes/pipes.module";
import { FilterModule } from "modules/filter/filter.module";

// Components
import { RiskControlsListComponent } from "./risk-controls-list.component";
import { RiskControlsFilterComponent } from "./risk-controls-filter/risk-controls-filter.component";

// Service
import { RiskControlsFilterService } from "./risk-controls-filter/risk-controls-filter.service";

// Services

@NgModule({
    imports: [
        SharedModule,
        CommonModule,
        VitreusModule,
        OTPipesModule,
        FilterModule,
    ],
    declarations: [
        RiskControlsListComponent,
        RiskControlsFilterComponent,
    ],
    exports: [
        RiskControlsListComponent,
        RiskControlsFilterComponent,
    ],
    providers: [
        RiskControlsFilterService,
    ],
})
export class RiskControlsListModule {}
