// Angular
import {
    Component,
    OnInit,
} from "@angular/core";

// Services
import { StateService } from "@uirouter/core";
import { Permissions } from "modules/shared/services/helper/permissions.service";
import { GlobalSidebarService } from "modules/global-sidebar/services/one-global-sidebar.service";
import { Wootric } from "sharedModules/services/provider/wootric.service";
import { McmModalService } from "modules/mcm/mcm-modal.service";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

// Interfaces
import { INavMenuItem } from "modules/global-sidebar/interfaces/nav-menu-item.interface";

// Constants
import { WootricModuleMap } from "modules/shared/constants/wootric.constants";

@Component({
    selector: "risks-wrapper",
    templateUrl: "./risks-wrapper.component.html",
})
export class RisksWrapperComponent implements OnInit {
    constructor(
        private permissions: Permissions,
        private translatePipe: TranslatePipe,
        private globalSidebarService: GlobalSidebarService,
        private wootric: Wootric,
        private mcmModalService: McmModalService,
    ) {}

    ngOnInit() {
        this.globalSidebarService.set(this.getMenuRoutes(), this.translatePipe.transform("Risks"));
        this.wootric.run(WootricModuleMap.RiskRegister);
    }

    private getMenuRoutes(): INavMenuItem[] {
        const menuGroup: INavMenuItem[] = [];
        menuGroup.push({
            id: "RisksRepoSidebarItem",
            title: this.translatePipe.transform("RiskRegister"),
            route: "zen.app.pia.module.risks.views.riskrepo",
            activeRoute: "zen.app.pia.module.risks.views.riskrepo",
            icon: "ot ot-flag",
        });

        if (this.permissions.canShow("RiskCategoryConfig")) {
            menuGroup.push({
                id: "RiskCategorySidebarItem",
                title: this.translatePipe.transform("Risk.RiskCategories"),
                route: "zen.app.pia.module.risks.views.risk-categories",
                activeRoute: "zen.app.pia.module.risks.views.risk-categories",
                icon: "ot ot-list",
            });
        }

        if (this.permissions.canShow("ControlsLibrary")) {
            menuGroup.push({
                id: "SettingsControlsLibraryMenuItem",
                title: this.translatePipe.transform("ControlsLibrary"),
                route: "zen.app.pia.module.risks.views.controls.library",
                activeRouteFn: (stateService: StateService) =>
                    stateService.includes("zen.app.pia.module.risks.views.controls.library")
                    || stateService.includes("zen.app.pia.module.risks.views.controls.control"),
                icon: "ot ot-file-text-o",
            });
        } else if (this.permissions.canShow("ControlsLibraryLock")) {
            menuGroup.push({
                id: "SettingsControlsLockibraryMenuItem",
                title: this.translatePipe.transform("ControlsLibrary"),
                upgrade: true,
                action: (): void => this.mcmModalService.openMcmModal("ControlsLibraryLock"),
            });
        }

        if (this.permissions.canShow("RiskV2Setting")) {
            menuGroup.push({
                id: "SettingsRisksSidebarItem",
                title: this.translatePipe.transform("Settings"),
                route: "zen.app.pia.module.settings.risks",
                activeRoute: "zen.app.pia.module.settings.risks",
                icon: "ot ot-settings",
            });
        }

        return menuGroup;
    }
}
