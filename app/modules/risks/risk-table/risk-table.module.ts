// Angular
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

// Modules
import { VitreusModule } from "@onetrust/vitreus";
import { SharedModule } from "sharedModules/shared.module";
import { FilterModule } from "modules/filter/filter.module";

// Components
import { RisksTableComponent } from "./risks-table/risks-table.component";
import { RiskFilterComponent } from "./risks-table/risk-filter/risk-filter.component";

// Services
import { RisksFilterService } from "./risks-table/risk-filter/risks-filter.service";

// Pipes
import { OTPipesModule } from "modules/pipes/pipes.module";

@NgModule({
    imports: [
        VitreusModule,
        OTPipesModule,
        CommonModule,
        SharedModule,
        FilterModule,
    ],
    declarations: [
        RisksTableComponent,
        RiskFilterComponent,
    ],
    entryComponents: [
        RisksTableComponent,
    ],
    exports: [
        RisksTableComponent,
        RiskFilterComponent,
    ],

    providers: [
        RisksFilterService,
    ],
})
export class RiskTableModule {}
