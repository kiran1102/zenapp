// Angular
import {
    Component,
    Input,
    Output,
    EventEmitter,
    OnChanges,
    SimpleChanges,
} from "@angular/core";

// Enums / Constants
import { TableColumnTypes } from "enums/data-table.enum";
import { RiskLevels } from "enums/risk.enum";
import {
    InventoryDetailRoutes,
    InventoryDetailsTabs,
    InventoryRiskDetailsRoutes,
} from "constants/inventory.constant";
import { RiskV2SourceTypes } from "enums/riskV2.enum";
import { RiskV2StateTranslationKeys } from "constants/risk-v2.constants";
import { ContextMenuType } from "@onetrust/vitreus";
import { RiskDetailsTabs } from "modules/inventory/inventory-risk-details/inventory-risk-details.component";

// Interfaces
import {
    IGridColumn,
    IGridSort,
} from "interfaces/grid.interface";
import { IInventoryRiskTableRow } from "interfaces/inventory.interface";
import { IRiskDetails } from "interfaces/risk.interface";

// Services
import { Permissions } from "modules/shared/services/helper/permissions.service";
import { StateService } from "@uirouter/core";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

interface IMenuOptions {
    text: string;
    otAutoId: string;
    value: number;
}
@Component({
    selector: "risks-table",
    templateUrl: "risks-table.component.html",
})
export class RisksTableComponent implements OnChanges {

    @Input() columns: IGridColumn;
    @Input() tableData: IInventoryRiskTableRow[];
    @Input() sortData: IGridSort;
    @Input() inventoryRecordName: string;
    @Input() inventoryId: string;
    @Input() recordId: string;

    @Output() handleTableSort: EventEmitter<IGridSort> = new EventEmitter<IGridSort>();
    @Output() onDeleteRisk: EventEmitter<IRiskDetails> = new EventEmitter<IRiskDetails>();
    @Output() onUnlinkRisk: EventEmitter<IRiskDetails> = new EventEmitter<IRiskDetails>();

    riskLevel = RiskLevels;
    tableColumnTypes = TableColumnTypes;
    riskV2SourceTypes = RiskV2SourceTypes;
    riskV2StateTranslationKeys = RiskV2StateTranslationKeys;
    riskDetailsTabs = RiskDetailsTabs;
    menuOptionsInventoryRisk = [
        {
            text: this.translatePipe.transform("Delete"),
            otAutoId: "Delete",
            value: 1,
        },
    ];

    menuOptionsAssessmentRisk = [
        {
            text: this.translatePipe.transform("UnLink"),
            otAutoId: "Unlink",
            value: 2,
        },
    ];
    menuOptions: IMenuOptions[];
    menuType = ContextMenuType.Small;
    emptyValue = "- - - -";

    constructor(
        private stateService: StateService,
        private translatePipe: TranslatePipe,
        readonly permissions: Permissions,
    ) {}

    ngOnChanges(changes: SimpleChanges) {
        if (changes.tableData && changes.tableData.currentValue) {
            this.tableData = this.tableData.map((data: IInventoryRiskTableRow): IInventoryRiskTableRow => {
                return {
                    ...data,
                    categoriesCsv: data.categories ? data.categories.map((category) => category.nameKey ? this.translatePipe.transform(category.nameKey) : category.name).join(", ") : "",
                };
            });
        }

    }

    goToRiskDetails(risk: IInventoryRiskTableRow, tabId = RiskDetailsTabs.Details) {
        const inventoryId: number = Number.parseInt(this.inventoryId, 10);
        this.stateService.go(InventoryRiskDetailsRoutes[inventoryId], {
            inventoryId,
            recordId: this.recordId,
            riskId: risk.id,
            inventoryName: this.inventoryRecordName,
            tabId,
        });
    }

    goToSourceDetails(row: IInventoryRiskTableRow) {
        const sourceType = row.sourceType;
        const inventoryIdType = row.source.additionalAttributes.inventoryType;
        const sourceId = row.source.id;
        if (sourceType === RiskV2SourceTypes[RiskV2SourceTypes.PIA]) {
            this.stateService.go("zen.app.pia.module.assessment.detail", {
                assessmentId: sourceId,
            });
        } else {
            this.stateService.go(InventoryDetailRoutes[inventoryIdType], {
                recordId: sourceId,
                inventoryId: inventoryIdType,
                tabId: InventoryDetailsTabs.Details,
            }, { reload: true });
        }
    }

    handleSortChange(sortData: IGridSort) {
        this.handleTableSort.emit(sortData);
    }

    onToggle(event: boolean, row) {
        if (event) {
            if (row.sourceType === RiskV2SourceTypes[RiskV2SourceTypes.INVENTORY] && !row.viewOnly) {
                this.menuOptions = this.menuOptionsInventoryRisk;
            } else if (row.sourceType === RiskV2SourceTypes[RiskV2SourceTypes.PIA]) {
                this.menuOptions = this.menuOptionsAssessmentRisk;
            }
        }
    }

    handleMenuItemClicks(row: IRiskDetails) {
        if (row.sourceType === RiskV2SourceTypes[RiskV2SourceTypes.INVENTORY]) {
            this.onDeleteRisk.emit(row);
        } else if (row.sourceType === RiskV2SourceTypes[RiskV2SourceTypes.PIA]) {
            this.onUnlinkRisk.emit(row);
        }
    }
}
