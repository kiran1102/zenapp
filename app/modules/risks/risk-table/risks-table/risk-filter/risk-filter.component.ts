// Angular
import {
    Component,
    OnInit,
    Input,
    Output,
    EventEmitter,
} from "@angular/core";

// RxJs
import { Subject } from "rxjs";

// Interfaces
import { IAssessmentFilterParams } from "interfaces/assessment/assessment.interface";
import {
    IFilterColumnOption,
    IFilterValueOption,
    IFilter,
} from "interfaces/filter.interface";
import {
    IFilterOutput,
    IFilterV2,
} from "modules/filter/interfaces/filter.interface";
import { ILabelValue } from "interfaces/generic.interface";

// Services
import { RisksFilterService } from "./risks-filter.service";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

@Component({
    selector: "risk-filter",
    template: `
        <one-filter
            class="flex-full-height"
            [selectedFilters]="selectedFilters"
            [fields]="columnOptions"
            [getFieldLabelValue]="getFieldLabelValue"
            [currentValues]="currentFilterValues"
            [getValueLabelValue]="getColumnOptionLabelValue"
            [currentType]="currentFilterValueType"
            [togglePopup$]="closeFilter$"
            (selectField)="setFilterValues($event)"
            (applyFilters)="apply($event)"
            (cancel)="cancel.emit()"
            >
        </one-filter>
    `,
})
export class RiskFilterComponent implements OnInit {

    @Input() set filtersOpen(isOpen: boolean) {
        if (!isOpen) this.closeFilter$.next(isOpen);
    }

    @Output() applyFilters = new EventEmitter<IAssessmentFilterParams[]>();
    @Output() cancel = new EventEmitter<null>();

    currentFilterValueType: number;
    selectedFilters: Array<IFilterV2<IFilter, ILabelValue<string>, IFilterValueOption | string | number>>;
    columnOptions: IFilterColumnOption[] = [];
    currentFilterValues: IFilterValueOption[] = [];
    closeFilter$ = new Subject<boolean>();

    constructor(
        private risksFilterService: RisksFilterService,
        private translatePipe: TranslatePipe,
    ) {}

    ngOnInit() {
        this.risksFilterService
            .getFilterValues()
            .subscribe((options) => {
                this.columnOptions = options;
                if (this.columnOptions.length) {
                    this.preFetchFilters();
                }
            });
    }

    getFieldLabelValue = (column: IFilterColumnOption): ILabelValue<string> => {
        return {
            label: column.translationKey ? this.translatePipe.transform(column.translationKey) : (column && column.name ? column.name : ""),
            value:  column && column.id ? column.id : "",
        };
    }

    getColumnOptionLabelValue = (option: IFilterValueOption): ILabelValue<string | number> => {
        return {
            label: option.translationKey ? this.translatePipe.transform(option.translationKey) : option.value as string,
            value: option.key,
        };
    }

    setFilterValues(selectedField: ILabelValue<string>) {
        [this.currentFilterValueType, this.currentFilterValues] = this.risksFilterService.setFilterValues(selectedField, this.columnOptions);
    }

    apply(filters: IFilterOutput[]) {
        const inventoryRiskFilters: IAssessmentFilterParams[] = this.risksFilterService.applyFilters(filters && filters.length ? filters : null);
        this.risksFilterService.setActiveInventoryFilters(inventoryRiskFilters, filters, this.columnOptions);
        this.applyFilters.emit(inventoryRiskFilters);
    }

    preFetchFilters() {
        this.selectedFilters = this.risksFilterService.getActiveInventoryFilters(this.columnOptions);
    }
}
