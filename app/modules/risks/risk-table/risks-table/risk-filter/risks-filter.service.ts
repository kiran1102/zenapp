// Angular
import {
    Injectable,
    Inject,
} from "@angular/core";

// RxJs
import {
    of,
    from,
    combineLatest,
    Observable,
} from "rxjs";
import {
    filter as filterRx,
    map,
} from "rxjs/operators";

// Interfaces
import {
    IFilterV2,
    IFilterOutput,
} from "modules/filter/interfaces/filter.interface";
import {
    IFilterColumnOption,
    IFilter,
    IFilterValueOption,
} from "interfaces/filter.interface";
import {
    IKeyValue,
    ILabelValue,
} from "interfaces/generic.interface";
import { IStore } from "interfaces/redux.interface";
import { IOrganization } from "interfaces/org.interface";
import { IOrgUserAdapted } from "interfaces/org-user.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";
import { IRiskLevel } from "interfaces/risks-settings.interface";
import { IAssessmentFilterParams } from "interfaces/assessment/assessment.interface";
import { IRiskCategoryBasicInfo } from "interfaces/risk.interface";

// Constants/Enums
import { OrgUserTraversal } from "enums/org-user-dropdown.enum";
import { InventoryRiskType } from "enums/inventory.enum";
import {
    InventoryRiskFilterTypes,
    RiskV2States,
} from "enums/riskV2.enum";
import { RiskTranslations } from "enums/risk.enum";
import { InventoryRiskFilterValueTypes } from "constants/risk-v2.constants";
import { RiskCategoryStatus } from "modules/risks/risk-category/risk-category.constants";

// Redux
import { StoreToken } from "tokens/redux-store.token";
import {
    getOrgList,
    getCurrentOrgId,
} from "oneRedux/reducers/org.reducer";

// Services
import { OrgGroupApiService } from "oneServices/api/org-group-api.service.ts";
import { RiskAPIService } from "oneServices/api/risk-api.service";
import { Permissions } from 'sharedModules/services/helper/permissions.service';

@Injectable()
export class RisksFilterService {

    constructor(
        @Inject(StoreToken) private store: IStore,
        private riskAPI: RiskAPIService,
        private orgGroupApi: OrgGroupApiService,
        private permissions: Permissions,
    ) {}

    getFilterValues(): Observable<IFilterColumnOption[]> {
        const isRiskCategoriesAllowed = this.permissions.canShow("RiskViewCategory");
        return combineLatest(
            this.getRiskOwnersList(),
            this.getAllOrganisations(),
            this.riskAPI.getRiskLevel(false),
            isRiskCategoriesAllowed ? this.getRiskCategories() : of([]),
        )
        .pipe(
            filterRx(([riskOwners, orgList, riskLevels, categories]: [ Array<IKeyValue<string>>, Array<IKeyValue<string>>, IRiskLevel[], Array<IKeyValue<string>>]) => Boolean(riskOwners && orgList && riskLevels && categories)),
            map(([riskOwners, orgList, riskLevels, categories]) => {
                const options = [
                    {
                        name: "RiskLevel",
                        id: "riskLevel",
                        translationKey: "RiskLevel",
                        type: InventoryRiskFilterTypes.RiskLevel,
                        valueOptions: riskLevels.map((riskLevel: IRiskLevel) => ({ translationKey: RiskTranslations[riskLevel.name], key: riskLevel.name, value: riskLevel.id })),
                    },
                    {
                        name: "RiskStatus",
                        id: "riskStatus",
                        translationKey: "RiskStatus",
                        type: InventoryRiskFilterTypes.RiskState,
                        valueOptions: [
                            { translationKey: "Identified", key: RiskV2States.IDENTIFIED, value: RiskV2States.IDENTIFIED },
                            { translationKey: "RecommendationAdded", key: RiskV2States.RECOMMENDATION_ADDED, value: RiskV2States.RECOMMENDATION_ADDED },
                            { translationKey: "RecommendationSent", key: RiskV2States.RECOMMENDATION_SENT, value: RiskV2States.RECOMMENDATION_SENT },
                            { translationKey: "RemediationProposed", key: RiskV2States.REMEDIATION_PROPOSED, value: RiskV2States.REMEDIATION_PROPOSED },
                            { translationKey: "ExceptionRequested", key: RiskV2States.EXCEPTION_REQUESTED, value: RiskV2States.EXCEPTION_REQUESTED },
                            { translationKey: "Reduced", key: RiskV2States.REDUCED, value: RiskV2States.REDUCED },
                            { translationKey: "Retained", key: RiskV2States.RETAINED, value: RiskV2States.RETAINED },
                        ],
                    },
                    {
                        name: "Type",
                        id: "type",
                        translationKey: "Type",
                        type: InventoryRiskFilterTypes.Type,
                        valueOptions: [
                            { translationKey: "PIA", key: InventoryRiskType.Question, value: InventoryRiskType.Question },
                            { translationKey: "Inventory", key: InventoryRiskType.Inventory, value: InventoryRiskType.Inventory },
                        ],
                    },
                    {
                        name: "Organization",
                        id: "organization",
                        translationKey: "Organization",
                        type: InventoryRiskFilterTypes.Organization,
                        valueOptions: orgList,
                    },
                    {
                        name: "RiskOwner",
                        id: "riskOwner",
                        translationKey: "RiskOwner",
                        type: InventoryRiskFilterTypes.RiskOwner,
                        valueOptions: riskOwners,
                    },     
            ];
            if (isRiskCategoriesAllowed) {
                options.push({
                    name: "Category",
                    id: "category",
                    translationKey: "Category",
                    type: InventoryRiskFilterTypes.Category,
                    valueOptions: categories,
                },)
            }
            return options;
        }));
    }

    applyFilters(filters: IFilterOutput[]): IAssessmentFilterParams[] {
        if (!(filters && filters.length)) return [];

        return filters.map((filter: IFilterOutput): IAssessmentFilterParams => {
            return {
                field: filter.field.value as string,
                operation: filter && filter.operator ? filter.operator.value as string : "=",
                value: filter.values.map((item) => item.value as string),
            };
        });
    }

    setFilterValues(selectedField: ILabelValue<string>, columnOptions: IFilterColumnOption[]): [number, IFilterValueOption[]] {
        if (!selectedField) return;
        const filterIndex = columnOptions.findIndex((column: IFilterColumnOption) => column.id === selectedField.value);
        const selectedColumn = columnOptions[filterIndex];
        const currentFilterValueType = InventoryRiskFilterValueTypes[selectedColumn.type];
        const currentFilterValues = selectedColumn ? selectedColumn.valueOptions : [];
        return [currentFilterValueType, currentFilterValues];
    }

    setActiveInventoryFilters(inventoryRiskFilters: IAssessmentFilterParams[], filters: IFilterOutput[], columnOptions: IFilterColumnOption[]) {
        const sessionData = JSON.stringify({
            preselectedInventoryRiskFilters: inventoryRiskFilters,
            activeInventoryRiskFilters: this.deserializeFilters(filters, columnOptions),
        });
        sessionStorage.setItem("ActiveInventoryRiskFilterList", sessionData);
    }

    getActiveInventoryFilters(columnOptions: IFilterColumnOption[]): Array<IFilterV2<IFilter, ILabelValue<string>, IFilterValueOption | string | number>>  {
        if (sessionStorage.getItem("ActiveInventoryRiskFilterList")) {
            const activeInventoryRiskFilterList = JSON.parse(sessionStorage.getItem("ActiveInventoryRiskFilterList"));
            if (activeInventoryRiskFilterList.activeInventoryRiskFilters && activeInventoryRiskFilterList.activeInventoryRiskFilters.length) {
                return this.serializeFilters(activeInventoryRiskFilterList.activeInventoryRiskFilters, columnOptions);
            }
        }
    }

    private deserializeFilters(filters: IFilterOutput[], columnOptions: IFilterColumnOption[]): IFilter[] {
        if (!filters) return [];
        return (filters).map((selectedFilter: IFilterOutput): IFilter => {
            const matchingColumn: IFilterColumnOption = columnOptions.find((column: IFilterColumnOption): boolean => column.id === selectedFilter.field.value);
            return {
                columnId: matchingColumn.id,
                attributeKey: matchingColumn.id,
                entityType: matchingColumn.entityType,
                dataType: matchingColumn.type,
                operator: "EQ",
                values: this.deserializeFilterValues(selectedFilter.values, matchingColumn.type),
            };
        });
    }

    private deserializeFilterValues(values: Array<ILabelValue<string|number>>, filterType: number): Array<string|number> {
        if (!(values && values.length)) return [];
        switch (filterType) {
            case InventoryRiskFilterTypes.RiskLevel:
            case InventoryRiskFilterTypes.RiskState:
            case InventoryRiskFilterTypes.Type:
            case InventoryRiskFilterTypes.Category:
                return values.map((value: ILabelValue<string|number>): string | number => value.value || value.label);
            case InventoryRiskFilterTypes.Organization:
            case InventoryRiskFilterTypes.RiskOwner:
                return values.map((value: ILabelValue<string|number>): string | number => value.label || value.value);
        }
    }

    private serializeFilters(filters: IFilter[], columnOptions: IFilterColumnOption[]): Array<IFilterV2<IFilter, ILabelValue<string>, IFilterValueOption | string | number>> {
        return filters.map((selectedFilter: IFilter): IFilterV2<IFilter, ILabelValue<string>, IFilterValueOption | string | number> => {
            const matchingColumn: IFilterColumnOption = columnOptions.find((column: IFilterColumnOption): boolean => column.id === selectedFilter.columnId);
            return {
                field: matchingColumn,
                operator: null,
                values: selectedFilter.values && selectedFilter.values.length ? this.formatFilterSelections(matchingColumn, selectedFilter.values as string[]) : selectedFilter.values,
                type: InventoryRiskFilterValueTypes[matchingColumn.type],
            };
        });
    }

    private formatFilterSelections(column: IFilterColumnOption, selections: string[]): IFilterValueOption[] {
        switch (column.type) {
            case InventoryRiskFilterTypes.RiskLevel:
            case InventoryRiskFilterTypes.RiskState:
            case InventoryRiskFilterTypes.Type:
            case InventoryRiskFilterTypes.Category:
                return column && column.valueOptions && column.valueOptions.length ? selections.map((value: string) => column.valueOptions.find((option: IFilterValueOption) => option.key === value)) : [];
            case InventoryRiskFilterTypes.Organization:
            case InventoryRiskFilterTypes.RiskOwner:
                return selections.map((value: string) => column.valueOptions.find((option: IFilterValueOption) => option.value === value));
        }
    }

    private getAllOrganisations() {
        const state = this.store.getState();
        return of(getOrgList(getCurrentOrgId(state))(state).map((org: IOrganization) => {
            return { key: org.Id, value: org.Name };
        }));
    }

    private getRiskOwnersList() {
        return from(this.orgGroupApi.getOrgUsersWithPermission(getCurrentOrgId(this.store.getState()), OrgUserTraversal.Branch)
            .then((response: IProtocolResponse<IOrgUserAdapted[]>): Array<IKeyValue<string>> => {
                return response.data.map((user: IOrgUserAdapted): IKeyValue<string> => {
                    return {
                        key: user.Id,
                        value: user.FullName,
                    };
                });
            }));
    }

    private getRiskCategories(): Observable<Array<IKeyValue<string>>> {
        return this.riskAPI.fetchRiskCategoryListByStatus(RiskCategoryStatus.UNARCHIVED)
            .pipe(map((response: IProtocolResponse<IRiskCategoryBasicInfo[]>): Array<IKeyValue<string>> => {
                return response.data.map((category: IRiskCategoryBasicInfo): IKeyValue<string> => {
                        return { key: category.id, value: category.name };
                    });
            }));
    }
}
