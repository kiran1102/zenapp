// Angular
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

// Modules
import { SharedModule } from "sharedModules/shared.module";
import { SettingsRisksModule } from "modules/settings/settings-risks/settings-risks.module";
import { RiskRepoModule } from "./risk-repo/risk-repo.module";
import { ControlsLibraryModule } from "modules/controls/controls-library/controls-library.module";
import { RisksSharedModule } from "./shared/risks-shared.module";
import { RiskRegisterRiskDetailsModule } from "./risk-register-risk-details/risk-register-risk-details.module";
import { RiskCategoryTableModule } from "modules/risks/risk-category/risk-category-table.module";

// Components
import { RisksWrapperComponent } from "./risks-wrapper/risks-wrapper.component";

@NgModule({
    imports: [
        SharedModule,
        CommonModule,
        RiskRepoModule,
        SettingsRisksModule,
        ControlsLibraryModule,
        RisksSharedModule,
        RiskRegisterRiskDetailsModule,
        RiskCategoryTableModule,
    ],
    declarations: [
        RisksWrapperComponent,
    ],
    entryComponents: [
        RisksWrapperComponent,
    ],
})
export class RisksModule {}
