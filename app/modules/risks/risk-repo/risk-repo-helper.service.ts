// Angular
import { Injectable } from "@angular/core";
import { IRiskRepoTable } from "interfaces/risk.interface";

const RIKS_REPO_SELECTED_COLUMNS_STORE_KEY = "riskRegisterSelectedColumns";

@Injectable()
export class RiskRepoHelperService {

    setSelectedColumns(columns: IRiskRepoTable[]) {
        localStorage.setItem(RIKS_REPO_SELECTED_COLUMNS_STORE_KEY, JSON.stringify( columns ? columns.map((column) => column.name)  : []));
    }

    getActiveInactiveColumnsMap(defaultActiveColumns: IRiskRepoTable[], defaultInactiveColumns: IRiskRepoTable[]) {
        const selectedColumns: string[] = JSON.parse(localStorage.getItem(RIKS_REPO_SELECTED_COLUMNS_STORE_KEY));
        if (selectedColumns && selectedColumns.length) {
            const activeColumns = [];
            const allColumns = [...defaultActiveColumns, ...defaultInactiveColumns];
            selectedColumns.forEach((selectedColumnName: string) => {
                const selectedColumnIndex = allColumns.findIndex((column: IRiskRepoTable) => column.name === selectedColumnName);
                if (selectedColumnIndex > -1) {
                    activeColumns.push(allColumns[selectedColumnIndex]);
                    allColumns.splice(selectedColumnIndex, 1);
                }
            });
            const inactiveColumns: IRiskRepoTable[] = [];
            allColumns.forEach((column: IRiskRepoTable) => {
                column.isMandatory ? activeColumns.push(column) : inactiveColumns.push(column);
            });
            return { activeColumns, inactiveColumns };
        }
        return { activeColumns: defaultActiveColumns, inactiveColumns: defaultInactiveColumns };
    }
}
