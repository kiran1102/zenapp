// External Libraries
import {
    Component,
    Inject,
    Input,
    Output,
    EventEmitter,
    OnInit,
    SimpleChanges,
} from "@angular/core";

// Redux
import { getCurrentOrgId } from "oneRedux/reducers/org.reducer";
import { StoreToken } from "tokens/redux-store.token";
import { IStore } from "interfaces/redux.interface";

// Interfaces
import {
    IRiskDetailsRegisterView,
    IRiskDetails,
} from "interfaces/risk.interface";
import { IGridSort } from "interfaces/grid.interface";
import { IRiskRepoTable } from "interfaces/risk.interface";

// Constants
import { RiskV2StateTranslationKeys } from "constants/risk-v2.constants";

// Enums
import {
    RiskV2ColumnTypes,
    RiskV2SourceTypes,
} from "enums/riskV2.enum";

// Service
import { RiskHelperService } from "sharedModules/services/helper/risk-helper.service";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

@Component({
    selector: "risk-repo-table",
    templateUrl: "./risk-repo-table.component.html",
})
export class RiskRepoTableComponent implements OnInit {

    @Input() tableData: IRiskDetails[];
    @Input() columns: IRiskRepoTable[];
    @Input() sortData;
    @Output() sortRisks: EventEmitter<IGridSort> = new EventEmitter<IGridSort>();
    @Output() goToAssessment: EventEmitter<IRiskDetails> = new EventEmitter<IRiskDetails>();
    @Output() goToRiskDetails: EventEmitter<IRiskDetails> = new EventEmitter<IRiskDetails>();
    @Output() goToRiskControls: EventEmitter<IRiskDetails> = new EventEmitter<IRiskDetails>();

    isSideBarOpen = true;
    orgGroupId: string;
    riskColumnTypes = RiskV2ColumnTypes;
    riskV2StateTranslationKeys = RiskV2StateTranslationKeys;
    riskV2SourceTypes = RiskV2SourceTypes;
    tableViewData: IRiskDetailsRegisterView[];
    emptyValue = "- - - -";

    constructor(
        @Inject(StoreToken) public store: IStore,
        private helper: RiskHelperService,
        private translatePipe: TranslatePipe,
    ) { }

    ngOnInit(): void {
        this.orgGroupId = getCurrentOrgId(this.store.getState());
    }

    ngOnChanges(changes: SimpleChanges) {
        if (changes.tableData && changes.tableData.currentValue) {
            this.tableViewData = this.tableData.map((data: IRiskDetails): IRiskDetailsRegisterView => {
                return {
                    ...data,
                    impactLevel: data.impactLevel ? this.helper.getLabelValue(data.impactLevel) : data.impactLevel,
                    probabilityLevel: data.probabilityLevel ? this.helper.getLabelValue(data.probabilityLevel) : data.probabilityLevel,
                    controlsCount: data.controlsIdentifier ? data.controlsIdentifier.length : 0,
                    categoriesCsv: data.categories ? data.categories.map((category) => category.nameKey ? this.translatePipe.transform(category.nameKey) : category.name).join(", ") : "",
                };
            });
        }
    }

    handleIconColor(riskLevel): string {
        switch (riskLevel) {
            case "LOW":
                return "risk-low--bg";
            case "MEDIUM":
                return "risk-medium--bg";
            case "HIGH":
                return "risk-high--bg";
            case "VERY_HIGH":
                return "risk-very-high--bg";
            default:
                return "";
        }
    }

    handleSortChange(event: IGridSort): any {
        this.sortRisks.emit(event);
    }
}
