// Angular
import {
    Component,
    EventEmitter,
    Input,
    Output,
    Inject,
    OnDestroy,
} from "@angular/core";

// 3rd Party
import { forEach } from "lodash";

// RxJs
import { Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";

// Constants and Enums
import {
    RiskV2States,
    RiskV2SourceTypes,
} from "enums/riskV2.enum";
import { OrgUserDropdownTypes } from "enums/org-user-dropdown.enum";
import { RiskV2StateTranslationKeys } from "constants/risk-v2.constants";
import { RiskFilterTypes } from "./risk-repo-filter.enum";
import { OrgUserTraversal } from "enums/org-user-dropdown.enum";
import { RiskCategoryStatus } from "modules/risks/risk-category/risk-category.constants";

// Redux
import {
    getOrgList,
    getCurrentOrgId,
} from "oneRedux/reducers/org.reducer";
import { StoreToken } from "tokens/redux-store.token";
import { IStore } from "interfaces/redux.interface";

// Interfaces
import { IRiskRepoFilter } from "./risk-repo-filter.interface";
import { ILabelValue } from "interfaces/generic.interface";
import { IOrgUserAdapted } from "interfaces/org-user.interface";
import { IOrganization } from "interfaces/org.interface";
import { IRiskLevel } from "interfaces/risks-settings.interface";
import { IRiskCategoryBasicInfo } from "interfaces/risk.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

// Services
import { RiskAPIService } from "oneServices/api/risk-api.service";
import { Permissions } from "sharedModules/services/helper/permissions.service";

@Component({
    selector: "risk-repo-filter",
    templateUrl: "risk-repo-filter.component.html",
})
export class RiskRepoFilterComponent implements OnDestroy {

    @Input() initialFilter: IRiskRepoFilter[];
    @Input() isOpen: boolean;

    @Output() apply = new EventEmitter<IRiskRepoFilter[]>();
    @Output() close = new EventEmitter<void>();

    fieldValueOptions: Array<ILabelValue<string>> | Array<ILabelValue<number>>;
    fieldValueSelected: Array<ILabelValue<string>> | ILabelValue<string>;
    filterOptions: Array<ILabelValue<RiskFilterTypes>> = [
        { value: RiskFilterTypes.RISK_LEVEL, label: this.translatePipe.transform("RiskLevel") },
        { value: RiskFilterTypes.RISK_OWNER, label: this.translatePipe.transform("RiskOwner") },
        { value: RiskFilterTypes.RISK_STATE, label: this.translatePipe.transform("RiskStatus") },
        { value: RiskFilterTypes.SOURCE_TYPE, label: this.translatePipe.transform("Type") },
        { value: RiskFilterTypes.ORG_GROUP_ID, label: this.translatePipe.transform("Organization") },
    ];
    categoryFilterOption = {
        value: RiskFilterTypes.CATEGORY, label: this.translatePipe.transform("Category"),
    };
    filters: IRiskRepoFilter[] = [];
    filterSelected: ILabelValue<RiskFilterTypes>;
    isAddButtonDisabled = true;
    isFieldValueMultiSelect = false;
    notRiskOwnerOptionSelected = true;
    riskOwnerDropdownType = OrgUserDropdownTypes.RiskOwner;
    riskOwnerList: Array<ILabelValue<string>> = [];
    riskOwnerSelected: string;
    orgUserTraversal = OrgUserTraversal;

    riskLevelOptions: Array<ILabelValue<number>> = [];
    readonly sourceTypeOptions: Array<ILabelValue<string>> = [
        { value: RiskV2SourceTypes[RiskV2SourceTypes.PIA], label: this.translatePipe.transform("PIA") },
        { value: RiskV2SourceTypes[RiskV2SourceTypes.INVENTORY], label: this.translatePipe.transform("Inventory") },
    ];
    readonly riskStateOptions: Array<ILabelValue<string>> = [
        { value: RiskV2States.IDENTIFIED, label: this.translatePipe.transform(RiskV2StateTranslationKeys.IDENTIFIED) },
        { value: RiskV2States.RECOMMENDATION_ADDED, label: this.translatePipe.transform(RiskV2StateTranslationKeys.RECOMMENDATION_ADDED, { Recommendation: "Recommendation" }) },
        { value: RiskV2States.RECOMMENDATION_SENT, label: this.translatePipe.transform(RiskV2StateTranslationKeys.RECOMMENDATION_SENT, { Recommendation: "Recommendation" }) },
        { value: RiskV2States.REMEDIATION_PROPOSED, label: this.translatePipe.transform(RiskV2StateTranslationKeys.REMEDIATION_PROPOSED) },
        { value: RiskV2States.EXCEPTION_REQUESTED, label: this.translatePipe.transform(RiskV2StateTranslationKeys.EXCEPTION_REQUESTED) },
        { value: RiskV2States.REDUCED, label: this.translatePipe.transform(RiskV2StateTranslationKeys.REDUCED) },
        { value: RiskV2States.RETAINED, label: this.translatePipe.transform(RiskV2StateTranslationKeys.RETAINED) },
    ];
    orgList: Array<ILabelValue<string>> = [];
    allOrgList: IOrganization[] = this.getAllOrganisations();
    riskLevelFilters: Array<ILabelValue<number>> = [];
    riskCategoryOptions: Array<ILabelValue<string>> = [];

    private destroy$ = new Subject();

    constructor(
        @Inject(StoreToken) public store: IStore,
        private riskAPI: RiskAPIService,
        private permissions: Permissions,
        private translatePipe: TranslatePipe,
    ) { }

    ngOnInit() {
        forEach(this.initialFilter, (filter: IRiskRepoFilter) => {
            this.filters.push(filter);
            const filterIndex = this.filterOptions.findIndex((option) => option.value === filter.field.value);
            this.filterOptions.splice(filterIndex, 1);
        });
        this.OrgList();
        // permission to check "RiskCategory"
        if (this.permissions.canShow("RiskViewCategory")) {
            this.filterOptions.splice(5, 0, this.categoryFilterOption);
            this.fetchRiskCategoryList();
        }

        this.riskAPI.getRiskLevel()
            .pipe(
                takeUntil(this.destroy$),
            )
            .subscribe((riskLevels: IRiskLevel[]) => {
                if (riskLevels) {
                    riskLevels.forEach((riskLevel: IRiskLevel) => {
                        this.riskLevelOptions = [...this.riskLevelOptions, { value: riskLevel.id, label: riskLevel.displayName }];
                    });
                }
            });
    }

    ngOnDestroy() {
        this.destroy$.next();
        this.destroy$.complete();
    }

    closePane() {
        this.close.emit();
    }

    onFilterSelection(selection: ILabelValue<RiskFilterTypes>) {
        this.filterSelected = selection;
        this.fieldValueSelected = null;
        this.isAddButtonDisabled = true;
        this.isFieldValueMultiSelect = false;
        this.notRiskOwnerOptionSelected = true;
        switch (selection.value) {
            case RiskFilterTypes.SOURCE_TYPE:
                this.fieldValueOptions = this.sourceTypeOptions;
                break;
            case RiskFilterTypes.RISK_LEVEL:
                this.fieldValueOptions = this.riskLevelOptions;
                this.fieldValueSelected = [];
                this.isFieldValueMultiSelect = true;
                break;
            case RiskFilterTypes.RISK_STATE:
                this.fieldValueOptions = this.riskStateOptions;
                this.fieldValueSelected = [];
                this.isFieldValueMultiSelect = true;
                break;
            case RiskFilterTypes.RISK_OWNER:
                this.fieldValueOptions = this.riskOwnerList;
                this.fieldValueSelected = [];
                this.isFieldValueMultiSelect = true;
                this.notRiskOwnerOptionSelected = false;
                break;
            case RiskFilterTypes.CATEGORY:
                this.fieldValueOptions = this.riskCategoryOptions;
                this.fieldValueSelected = [];
                this.isFieldValueMultiSelect = true;
                break;
            case RiskFilterTypes.ORG_GROUP_ID:
                this.fieldValueOptions = this.orgList;
                break;
            default:
                this.fieldValueOptions = [];
        }
    }

    onFieldValueSelection(selection: ILabelValue<string> | { currentValue: Array<ILabelValue<string>> }) {
        if ((selection as ILabelValue<string>).value ) {
            this.fieldValueSelected = selection as ILabelValue<string>;
        } else {
            this.fieldValueSelected = (selection as { currentValue: Array<ILabelValue<string>>}).currentValue;
        }
        this.enableAddButton();
    }

    onRiskOwnerSelection(args: { selection: IOrgUserAdapted | string, valid: boolean }, filter) {
        if (args.valid) {
            const selectedUser = args.selection as IOrgUserAdapted;
            this.fieldValueSelected = { value: selectedUser.Id, label: selectedUser.FullName };
        } else {
            this.fieldValueSelected = null;
        }
        this.enableAddButton();
    }

    addFilter() {
        this.filters.push({ field: this.filterSelected, selection: this.fieldValueSelected });
        const filterIndex = this.filterOptions.findIndex((filter: ILabelValue<string>) => filter.value === this.filterSelected.value);
        this.filterOptions.splice(filterIndex, 1);
        this.filterSelected = null;
        this.fieldValueSelected = null;
        this.isAddButtonDisabled = true;
        this.notRiskOwnerOptionSelected = true;
    }

    clearFilters() {
        this.filters.forEach((filter) => this.filterOptions.push(filter.field));
        this.filters = [];
    }

    removeFilter(index: number) {
        const removed = this.filters.splice(index, 1)[0];
        this.filterOptions.push(removed.field);
    }

    applyFilter() {
        this.apply.emit(this.filters);
    }

    getFieldDisplayValue(filter: IRiskRepoFilter): string {
        if (filter.field.value === RiskFilterTypes.RISK_OWNER
            || filter.field.value === RiskFilterTypes.SOURCE_TYPE
            || filter.field.value === RiskFilterTypes.ORG_GROUP_ID) {
            return (filter.selection as ILabelValue<string>).label;
        } else {
            const orTranslated = this.translatePipe.transform("Or").toLowerCase();
            return (filter.selection as Array<ILabelValue<string>>).reduce((displayValue: string, currentValue: ILabelValue<string>, currentIndex: number) => {
                return currentIndex === 0 ? currentValue.label : `${displayValue} ${orTranslated} ${currentValue.label}`;
            }, "");
        }
    }

    private enableAddButton() {
        this.isAddButtonDisabled = !this.filterSelected || !this.fieldValueSelected;
    }

    private fetchRiskCategoryList() {
        this.riskAPI.fetchRiskCategoryListByStatus(RiskCategoryStatus.UNARCHIVED)
            .pipe(takeUntil(this.destroy$))
            .subscribe((response: IProtocolResponse<IRiskCategoryBasicInfo[]>) => {
                if (response.result) {
                    this.riskCategoryOptions = response.data.map((categoryInfo: IRiskCategoryBasicInfo): ILabelValue<string> => {
                        const info: ILabelValue<string> = {
                            label: categoryInfo.name,
                            value: categoryInfo.id,
                        };
                        return info;
                    });
                }
            });
    }

    private getAllOrganisations(): IOrganization[] {
        const selectedOrgGroup = getCurrentOrgId(this.store.getState());
        return getOrgList(selectedOrgGroup)(this.store.getState());
    }

    private OrgList(): void {
        forEach(this.allOrgList, (org: IOrganization) => {
            this.orgList = [...this.orgList, { value: org.Id, label: org.Name }];
        });
    }
}
