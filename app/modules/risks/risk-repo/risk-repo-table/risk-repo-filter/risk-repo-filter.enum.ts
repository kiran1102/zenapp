export enum RiskFilterTypes {
    SOURCE_TYPE = "sourceType",
    RISK_LEVEL = "riskLevelId",
    RISK_STATE = "riskState",
    RISK_OWNER = "riskOwnerId",
    ORG_GROUP_ID = "orgGroupId",
    CATEGORY = "categoryId",
}
