import { ILabelValue } from "interfaces/generic.interface";
import { RiskFilterTypes } from "../risk-repo-filter/risk-repo-filter.enum";

export interface IRiskRepoFilter {
    selection: ILabelValue<string> | Array<ILabelValue<string>>;
    field: ILabelValue<RiskFilterTypes>;
}
