// Angular
import {
    Component,
    OnInit,
    OnDestroy,
} from "@angular/core";
import { StateService } from "@uirouter/core";

// 3rd Party
import { isArray } from "lodash";

// Interfaces
import {
    IGridSort,
    IGridPaginationParams,
} from "interfaces/grid.interface";
import { IPageableMeta } from "interfaces/pagination.interface";
import { IRiskDetails } from "interfaces/risk.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";
import { IPageableOf } from "interfaces/pagination.interface";
import { IRiskRepoFilter } from "./risk-repo-table/risk-repo-filter/risk-repo-filter.interface";
import { IStringMap, ILabelValue } from "interfaces/generic.interface";
import { IRiskRepoTable } from "interfaces/risk.interface";
import { IListState } from "@onetrust/vitreus";
import { IColumnSelectorModalResolve } from "interfaces/column-selector-modal.interface";
import { ITaskConfirmationModalResolve } from "interfaces/task-confirmation-modal-resolve.interface";

// Services
import { RiskAPIService } from "oneServices/api/risk-api.service";
import { ModalService } from "sharedServices/modal.service";
import { TaskPollingService } from "sharedServices/task-polling.service";
import { RiskRepoHelperService } from "./risk-repo-helper.service";
import { Permissions } from "sharedModules/services/helper/permissions.service";

// Constants & Enums
import {
    RiskV2SourceTypes,
    RiskV2ColumnTypes,
} from "enums/riskV2.enum";
import {
    InventoryDetailRoutes,
    InventoryDetailsTabs,
} from "constants/inventory.constant";
import { RiskDetailsTabs } from "modules/inventory/inventory-risk-details/inventory-risk-details.component";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";
import { Subject } from "rxjs";
import {
  debounceTime,
  distinctUntilChanged,
  takeUntil,
} from "rxjs/operators";

@Component({
    selector: "risk-repo",
    templateUrl: "./risk-repo.component.html",
})
export class RiskRepoComponent implements OnInit, OnDestroy {
    activeColumnList: IRiskRepoTable[] = [];
    disabledColumnList: IRiskRepoTable[] = [];
    currentSearchTerm = "";
    filters: IRiskRepoFilter[];
    paginationInfo: IPageableMeta;
    paginationParams: IGridPaginationParams = { page: 0, size: 20, sort: "number,desc" || [] };
    ready = false;
    risksExist = true;
    showFilterPane = false;
    sortData: IGridSort;
    tableData: IRiskDetails[];

    defaultActiveColumns: IRiskRepoTable[] = [
        { name: "id", columnName: this.translatePipe.transform("ID"), columnType: RiskV2ColumnTypes.Link, cellValueKey: "number", sortKey: "number", columnNameTranslationKey: "ID", isMandatory: true},
        { name: "description", columnName: this.translatePipe.transform("Description"), columnType: RiskV2ColumnTypes.Link, cellValueKey: "description", sortKey: "description", style: { width: "25rem" }, columnNameTranslationKey: "Description", isMandatory: true},
        { name: "riskLevel", columnName: this.translatePipe.transform("RiskLevel"), columnType: RiskV2ColumnTypes.Icon, cellValueKey: "riskLevel", sortKey: "riskLevelId", columnNameTranslationKey: "RiskLevel"},
        { name: "organization", columnName: this.translatePipe.transform("Organization"), columnType: RiskV2ColumnTypes.Org, cellValueKey: "orgGroup", sortKey: "orgGroupId", columnNameTranslationKey: "Organization"},
        { name: "source", columnName: this.translatePipe.transform("Source"), columnType: RiskV2ColumnTypes.Link, cellValueKey: "source", sortKey: "sourceName", columnNameTranslationKey: "Source"},
        { name: "recommendation", columnName: this.translatePipe.transform("Recommendation"), columnType: RiskV2ColumnTypes.TextBox, cellValueKey: "recommendation", sortKey: "recommendation", style: { width: "25rem" }, columnNameTranslationKey: "Recommendation"},
        { name: "riskOwner", columnName: this.translatePipe.transform("RiskOwner"), columnType: null, cellValueKey: "riskOwner", sortKey: "riskOwnerId", style: { width: "15rem" }, columnNameTranslationKey: "RiskOwner"},
        { name: "type", columnName: this.translatePipe.transform("Type"), columnType: null, cellValueKey: "sourceType", sortKey: "sourceType", columnNameTranslationKey: "Type"},
        { name: "status", columnName: this.translatePipe.transform("Status"), columnType: RiskV2ColumnTypes.Status, sortKey: "riskState", cellValueKey: "state", style: { width: "15rem" }, columnNameTranslationKey: "Status"},
    ];

    defaultDisabledColumns: IRiskRepoTable[] = [
        { name: "impact", columnName: this.translatePipe.transform("Impact"), columnType: null, cellValueKey: "impactLevel", sortKey: "riskImpactLevel.name", columnNameTranslationKey: "Impact"},
        { name: "probability", columnName: this.translatePipe.transform("Probability"), columnType: null, cellValueKey: "probabilityLevel", sortKey: "riskProbabilityLevel.name", columnNameTranslationKey: "Probability"},
        { name: "score", columnName: this.translatePipe.transform("Score"), columnType: null, cellValueKey: "riskScore", sortKey: "riskScore", columnNameTranslationKey: "Score"},
        { name: "deadline", columnName: this.translatePipe.transform("Deadline"), columnType: RiskV2ColumnTypes.Date, cellValueKey: "deadline", sortKey: "deadline", columnNameTranslationKey: "Deadline"},
        { name: "mitigatedDate", columnName: this.translatePipe.transform("MitigatedDate"), columnType: RiskV2ColumnTypes.Date, cellValueKey: "mitigatedDate", sortKey: "mitigationDate", columnNameTranslationKey: "MitigatedDate"},
        { name: "justification", columnName: this.translatePipe.transform("Justification"), columnType: RiskV2ColumnTypes.TextBox, sortKey: "exceptionRequest", cellValueKey: "justification", style: { width: "25rem" }, columnNameTranslationKey: "Justification"},
        { name: "createdDate", columnName: this.translatePipe.transform("CreatedDate"), columnType: RiskV2ColumnTypes.DateTime, sortKey: "createdDate", cellValueKey: "createdUTCDateTime", columnNameTranslationKey: "CreatedDate"},
    ];

    riskControlsColumn = { name: "controls", columnName: this.translatePipe.transform("Controls"), columnType: RiskV2ColumnTypes.Link, cellValueKey: "controlsCount", style: { width: "15rem" }, columnNameTranslationKey: "Controls" };
    riskCategoryColumn = { name: "category", columnName: this.translatePipe.transform("Category"), columnType: RiskV2ColumnTypes.TextBox, cellValueKey: "categoriesCsv", columnNameTranslationKey: "Category", style: { width: "7rem" }};

    private searchText$ = new Subject<string>();
    private destroy$ = new Subject();

    constructor(
        private stateService: StateService,
        private permission: Permissions,
        private helper: RiskRepoHelperService,
        private RiskAPI: RiskAPIService,
        private modalService: ModalService,
        private taskPollingService: TaskPollingService,
        private translatePipe: TranslatePipe,
    ) {}

    ngOnInit(): void {
        // permission check for the 'RiskControl' feature flag
        if (this.permission.canShow("RiskControl")) {
            this.defaultActiveColumns.splice(6, 0, this.riskControlsColumn);
        }
        // permission check for the 'RiskCategory' feature flag
        if (this.permission.canShow("RiskViewCategory")) {
            this.defaultDisabledColumns.splice(1, 0, this.riskCategoryColumn);
        }
        this.fetchRiskSummary();
        this.searchText$.pipe(
            debounceTime(300),
            distinctUntilChanged(),
            takeUntil(this.destroy$),
        ).subscribe((searchText) => {
            this.currentSearchTerm = searchText;
            this.paginationParams.page = 0;
            this.fetchRiskSummary();
        });
    }

    ngOnDestroy() {
        this.destroy$.next();
        this.destroy$.complete();
    }

    handleExportClick(): void {
        this.RiskAPI.exportRisks(null, this.paginationParams, this.getformattedFilterForRequest(), this.currentSearchTerm).then((): ng.IPromise<null> => {
            const exportModalData: ITaskConfirmationModalResolve = {
                modalTitle: this.translatePipe.transform("ReportDownload"),
                confirmationText: this.translatePipe.transform("ReportIsGeneratedCheckTasksBar"),
            };
            this.modalService.setModalData(exportModalData);
            this.modalService.openModal("oneNotificationModalComponent");
            this.taskPollingService.startPolling();
            return;
        });
    }

    handleSearch(event): void {
        this.searchText$.next(event);
    }

    handleSort(event: IGridSort): void {
        this.sortData = event;

        this.paginationParams.sort = `${event.sortedKey},${event.sortOrder}`;

        if (event.sortedKey === "exceptionRequest") {
            const sortArray = [];
            sortArray.push(this.paginationParams.sort);
            sortArray.push(`mitigation,${event.sortOrder}`);
            this.paginationParams.sort = sortArray;
        }

        this.fetchRiskSummary();
    }

    updateColumns() {
        const columnsMap = this.helper.getActiveInactiveColumnsMap([...this.defaultActiveColumns], [...this.defaultDisabledColumns]);
        this.activeColumnList = columnsMap.activeColumns;
        this.disabledColumnList = columnsMap.inactiveColumns;
    }

    updateColumnNames(columnList: IRiskRepoTable[]): IRiskRepoTable[] {
        const updatedColumnList = columnList.map((column: IRiskRepoTable): IRiskRepoTable => {
            return {
                ...column,
                columnName: this.translatePipe.transform(column.columnNameTranslationKey),
            };
        });
        return updatedColumnList;
    }

    openColumnPickerModal() {
        const columnPickerModalData: IColumnSelectorModalResolve = {
            modalTitle: "ColumnSelector",
            leftList: [...this.disabledColumnList],
            rightList: [...this.activeColumnList],
            labelKey: "columnName",
        };
        columnPickerModalData.promiseToResolve = (columnSelectionMap: IListState): Promise<boolean> => {
            this.helper.setSelectedColumns(columnSelectionMap.right as IRiskRepoTable[]);
            this.updateColumns();
            return Promise.resolve(true);
        };
        this.modalService.setModalData(columnPickerModalData);
        this.modalService.openModal("columnSelectorModal");
    }

    goToAssessment(assessment: IRiskDetails): void {
        const sourceReference = assessment.source;
        if (sourceReference.type === RiskV2SourceTypes[RiskV2SourceTypes.PIA]) {
            this.stateService.go("zen.app.pia.module.assessment.detail", {
                assessmentId: sourceReference.id,
            });
        } else if (sourceReference.type === RiskV2SourceTypes[RiskV2SourceTypes.INVENTORY]) {
            this.stateService.go(InventoryDetailRoutes[sourceReference.additionalAttributes.inventoryType], {
                recordId: sourceReference.id,
                inventoryId: sourceReference.additionalAttributes ? sourceReference.additionalAttributes.inventoryType : "",
                tabId: InventoryDetailsTabs.Risks,
            });
        }
    }

    goToRiskDetails(risk: IRiskDetails): void {
        this.stateService.go("zen.app.pia.module.risks.views.details", {
            riskId: risk.id,
            tabId: RiskDetailsTabs.Details,
        });
    }

    goToRiskControls(risk: IRiskDetails): void {
        this.stateService.go("zen.app.pia.module.risks.views.details", {
            riskId: risk.id,
            tabId: RiskDetailsTabs.Controls,
        });
    }

    toggleFilterPane(): void {
        this.showFilterPane = !this.showFilterPane;
    }

    applyFilter(filters: IRiskRepoFilter[]) {
        this.filters = filters;
        this.fetchRiskSummary();
    }

    fetchPage(event: number) {
        if (this.paginationInfo.number !== event) {
            this.paginationParams.page = event;
            this.fetchRiskSummary();
        }
    }

    private fetchRiskSummary(): void {
        this.ready = false;

        this.RiskAPI.fetchRiskV2Summary(null, this.paginationParams, this.getformattedFilterForRequest(), this.currentSearchTerm).then((response: IProtocolResponse<IPageableOf<IRiskDetails>>): void => {
            if (!response) return;
            this.ready = true;
            this.tableData = response.data.content;
            this.risksExist = this.tableData && this.tableData.length > 0;
            this.paginationInfo = {
                first: response.data.first,
                last: response.data.last,
                number: response.data.number,
                numberOfElements: response.data.numberOfElements,
                size: response.data.size,
                sort: response.data.sort,
                totalElements: response.data.totalElements,
                totalPages: response.data.totalPages,
            };
            this.updateColumns();
        });
    }

    private getformattedFilterForRequest(): IStringMap<string> {
        let filterRequest: IStringMap<string>;
        if (this.filters) {
            filterRequest = this.filters.reduce((filter, value) => {
                filter[value.field.value] = isArray(value.selection)
                    ? (value.selection as Array<ILabelValue<string>>).map((selectedValue) => selectedValue.value)
                    : (value.selection as ILabelValue<string>).value;
                return filter;
            }, {});
        }
        return filterRequest;
    }
}
