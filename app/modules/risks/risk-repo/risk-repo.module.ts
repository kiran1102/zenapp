// 3rd party
import { NgModule } from "@angular/core";

// Modules
import { CommonModule } from "@angular/common";
import { SharedModule } from "sharedModules/shared.module";
import { UpgradedLegacyModule } from "sharedModules/upgrade/upgraded-legacy.module";
import { OTPipesModule } from "modules/pipes/pipes.module";
import { VitreusModule } from "@onetrust/vitreus";

// Components
import { RiskRepoTableComponent } from "./risk-repo-table/risk-repo-table.component";
import { RiskRepoComponent } from "./risk-repo.component";
import { RiskRepoFilterComponent } from "./risk-repo-table/risk-repo-filter/risk-repo-filter.component";

// Services
import { RiskRepoHelperService } from "./risk-repo-helper.service";

@NgModule({
    imports: [
        SharedModule,
        UpgradedLegacyModule,
        CommonModule,
        VitreusModule,
        OTPipesModule,
    ],
    declarations: [
        RiskRepoTableComponent,
        RiskRepoComponent,
        RiskRepoFilterComponent,
    ],
    exports: [
        RiskRepoComponent,
    ],
    entryComponents: [
        RiskRepoComponent,
    ],
    providers: [
        RiskRepoHelperService,
    ],
})
export class RiskRepoModule { }
