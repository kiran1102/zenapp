// Angular
import { Component } from "@angular/core";

// Vitreus
import {
    OtModalService,
    ModalSize,
    IOtModalContent,
} from "@onetrust/vitreus";

// RxJS
import { Subject } from "rxjs";
import { take, filter } from "rxjs/operators";

// Components
import { SkuManagementContactSalesModalComponent } from "../sku-management-contact-sales-modal/sku-management-contact-sales-modal.component";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

// Interfaces
import { ISkuManagementModalInfo } from "interfaces/sku-management-modal-info.interface";

@Component({
    selector: "sku-management-trial-failure-modal",
    templateUrl: "sku-management-trial-failure-modal.component.html",
})

export class SkuManagementTrialFailureModalComponent implements IOtModalContent {

    otModalData: ISkuManagementModalInfo;
    otModalCloseEvent: Subject<boolean>;
    skuManagementModalInfo: ISkuManagementModalInfo = {};

    constructor(
        private otModalService: OtModalService,
        private translatePipe: TranslatePipe,
    ) { }

    purchaseNow() {
        const modalData: ISkuManagementModalInfo = {
            pricingLink: this.otModalData.pricingLink,
            currentUser: this.otModalData.currentUser,
            orgid: this.otModalData.orgid,
            salesforceUrl: this.otModalData.salesforceUrl,
        };
        this.otModalService.create(
            SkuManagementContactSalesModalComponent,
            {
                isComponent: true,
                size: ModalSize.MEDIUM,
            },
            modalData,
        ).pipe(
            take(1),
            filter((data) => data),
        ).subscribe(() => {
            this.otModalCloseEvent.next();
        });
    }

    closeModal() {
        this.otModalCloseEvent.next(true);
    }
}
