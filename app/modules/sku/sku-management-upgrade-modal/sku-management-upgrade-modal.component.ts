// Angular
import {
    Component,
} from "@angular/core";

// RxJS
import { Subject } from "rxjs";
import { filter, take } from "rxjs/operators";

// Vitreus
import {
    OtModalService,
    ModalSize,
    IOtModalContent,
} from "@onetrust/vitreus";

// Components
import { SkuManagementContactSalesModalComponent } from "../sku-management-contact-sales-modal/sku-management-contact-sales-modal.component";

// Interfaces
import { ISkuManagementModalInfo } from "interfaces/sku-management-modal-info.interface";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

@Component({
    selector: "sku-management-upgrade-modal",
    templateUrl: "sku-management-upgrade-modal.component.html",
})

export class SkuManagementUpgradeModalComponent implements IOtModalContent {

    otModalData: ISkuManagementModalInfo;
    otModalCloseEvent: Subject<boolean>;
    disabled = false;
    description: string;

    constructor(
        private otModalService: OtModalService,
        private translatePipe: TranslatePipe,
    ) { }

    purchaseNow() {
        const modalData: ISkuManagementModalInfo = {
            pricingLink: this.otModalData.pricingLink,
            currentUser: this.otModalData.currentUser,
            orgid: this.otModalData.orgid,
            salesforceUrl: this.otModalData.salesforceUrl,
        };

        this.otModalService.create(
            SkuManagementContactSalesModalComponent,
            {
                isComponent: true,
                size: ModalSize.MEDIUM,
            },
            modalData,
        ).pipe(
            take(1),
            filter((data) => data),
        ).subscribe(() => {
            this.otModalCloseEvent.next();
        });
    }

    startTrial() {
        this.otModalCloseEvent.next(true);
    }

    closeModal() {
        this.otModalCloseEvent.next();
    }
}
