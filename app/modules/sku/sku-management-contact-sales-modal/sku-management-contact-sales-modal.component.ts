// Angular
import {
    Component,
    OnInit,
} from "@angular/core";
import {
    FormGroup,
    FormBuilder,
    Validators,
    FormControl,
} from "@angular/forms";

// RxJS
import { Subject } from "rxjs";
import {
    take,
    first,
    finalize,
    filter,
} from "rxjs/operators";

// Vitreus
import {
    IOtModalContent,
    OtModalService,
    ModalSize,
 } from "@onetrust/vitreus";

// Component
import { SkuManagementSuccessModalComponent } from "./../sku-management-sucess-modal/sku-management-success-modal.component";

// Services
import { SkuManagementModalApiService } from "../shared/sku-management-modal-api.service";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

// Constants
import { NO_BLANK_SPACE } from "constants/regex.constant";

// Interfaces
import { ISkuManagementModalInfo } from "interfaces/sku-management-modal-info.interface";
import { ISkuManagementContactSalesInfo } from "interfaces/sku-management-modal-info.interface";

@Component({
    selector: "sku-management-contact-sales-modal",
    templateUrl: "sku-management-contact-sales-modal.component.html",
})

export class SkuManagementContactSalesModalComponent implements IOtModalContent, OnInit {

    otModalCloseEvent: Subject<boolean>;
    otModalData: ISkuManagementModalInfo;
    skuManagementContactSalesForm: FormGroup;
    emailErrorMessage: string;
    firstNameErrorMessage: string;
    lastNameErrorMessage: string;
    disabled = false;
    description: string;
    subscriptionOptionsText: string;

    constructor(
        private translatePipe: TranslatePipe,
        private formBuilder: FormBuilder,
        private skuManagementModalApiService: SkuManagementModalApiService,
        private otModalService: OtModalService,
    ) { }

    ngOnInit() {
        this.initForm();
    }

    cancelPurchase() {
        this.otModalCloseEvent.next(true);
    }

    getError(field: string): boolean {
        if (field === "firstName") {
            const firstName = this.skuManagementContactSalesForm.get(field);
            if (!firstName || !firstName.errors) {
                return false;
            } else if (firstName.dirty && firstName.touched && (firstName.errors.required || firstName.errors.pattern)) {
                this.firstNameErrorMessage = this.translatePipe.transform(
                    "PleaseEnterYourFirstName",
                );
                return true;
            }
        } else if (field === "lastName") {
            const lastName = this.skuManagementContactSalesForm.get(field);
            if (!lastName || !lastName.errors) {
            return false;
            } else if (lastName.dirty && lastName.touched && (lastName.errors.required || lastName.errors.pattern)) {
                this.lastNameErrorMessage = this.translatePipe.transform(
                    "PleaseEnterYourLastName",
                );
                return true;
            }
        } else if (field === "emailAddress") {
            const emailAddress = this.skuManagementContactSalesForm.get(field);
            if (!emailAddress || !emailAddress.errors) {
                return false;
            } else if (emailAddress.dirty && emailAddress.touched &&
                (emailAddress.errors.required || emailAddress.errors.pattern || emailAddress.errors.email)) {
                this.emailErrorMessage = this.translatePipe.transform(
                    "ErrorEnterValidEmailAddress",
                );
                return true;
            }
        }
        return false;
    }

    contactSalesSubmit() {
        this.disabled = true;
        const upgradeData: ISkuManagementContactSalesInfo = {
            email: this.skuManagementContactSalesForm.get("emailAddress").value,
            firstName: this.skuManagementContactSalesForm.get("firstName").value,
            lastName: this.skuManagementContactSalesForm.get("lastName").value,
            contactNumber: this.skuManagementContactSalesForm.get("phoneNumber").value,
            interestedDesc: this.skuManagementContactSalesForm.get("interestDesc").value,
            orgid: this.otModalData.orgid,
            salesforceUrl: this.otModalData.salesforceUrl,
            tenantId: this.otModalData.currentUser.CurrentTenant,
            company: this.otModalData.currentUser.OrgGroupName,
        };

        this.skuManagementModalApiService.contactSales(upgradeData).pipe(
            first(),
            finalize(() => {
                this.otModalCloseEvent.next(true);
                this.disabled = false;
                this.openFormSubmitSuccessModal();
            }),
            ).subscribe(
                // next
                () => {},
                // error
                () => {},
        );
    }

    ngOnDestroy() {
        this.otModalCloseEvent.next();
        this.otModalCloseEvent.unsubscribe();
    }

    private openFormSubmitSuccessModal() {
        this.otModalService.create(
            SkuManagementSuccessModalComponent,
            {
                isComponent: true,
                size: ModalSize.MEDIUM,
            },
            { title: this.translatePipe.transform("ContactSalesSuccessModalTitle"), successType: "ContactSalesForm" },
        ).pipe(
            take(1),
            filter((data) => data),
        ).subscribe(() => {
            this.otModalCloseEvent.next();
        });
    }

    private initForm() {
        this.skuManagementContactSalesForm = this.formBuilder.group({
            firstName: new FormControl(this.otModalData.currentUser.FirstName, [
                Validators.required,
                Validators.pattern(NO_BLANK_SPACE),
            ]),
            lastName: new FormControl(this.otModalData.currentUser.LastName, [
                Validators.required,
                Validators.pattern(NO_BLANK_SPACE),
            ]),
            emailAddress: new FormControl(this.otModalData.currentUser.Email, [
                Validators.email,
                Validators.required,
                Validators.pattern(NO_BLANK_SPACE),
            ]),
            phoneNumber: new FormControl("", []),
            interestDesc: new FormControl("", []),
        });
    }
}
