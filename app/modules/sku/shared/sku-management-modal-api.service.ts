// Angular
import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";

// Rxjs
import { Observable, from } from "rxjs";

// Common
import { InterceptorHttpParams } from "@onetrust/common";

// Services
import { Content } from "sharedModules/services/provider/content.service";
import { ProtocolService } from "modules/core/services/protocol.service";

// Interfaces
import { IStringMap } from "interfaces/generic.interface";
import {
    IProtocolConfig,
    IProtocolMessages,
    IProtocolResponse,
} from "interfaces/protocol.interface";
import { ISkuManagementContactSalesInfo } from "interfaces/sku-management-modal-info.interface";

@Injectable()
export class SkuManagementModalApiService {

    constructor(
        private content: Content,
        private oneProtocol: ProtocolService,
        private httpClient: HttpClient,
    ) { }

    getBuyNowInfo(): Observable<IStringMap<string>> {
        return from(this.content.GetContent());
    }

    checkTrialApproved(): Observable<IProtocolResponse<unknown>> {
        const url = `/api/access/v1/tenants/current/subscriptions`;
        const config: IProtocolConfig = this.oneProtocol.customConfig("GET", url, { trial: true },
        );
        const messages: IProtocolMessages = {};
        return from(this.oneProtocol.http(config, messages));
    }

    requestTrial(reqTrialData: ISkuManagementContactSalesInfo): Observable<{}> {
        const url = reqTrialData.salesforceUrl;
        const description = `Please upgrade this tenant to a trial ${reqTrialData.tenantId} / ${reqTrialData.company} / ${reqTrialData.fullName} / ${reqTrialData.email}.`;
        const subject = `Create Trial ${reqTrialData.company} ${reqTrialData.tenantId}`;

        const requestTrialData = {
            "orgid": reqTrialData.orgid,
            "retURL": "http://www.onetrust.com",
            "name": reqTrialData.fullName,
            "Tenant_GUID__c": reqTrialData.tenantId,
            "company": reqTrialData.company,
            "email": reqTrialData.email,
            "subject": subject,
            "description": description,
            "00N3600000TOsWF": "Customer",
            "00N3600000TOsWK": "App Trial Request",
            "type": "Task",
            "priority": "High",
            "status": "New",
            "recordType": "01236000000oCpg",
            "submit": "Submit",
        };

        return this.formPost(url, requestTrialData);
    }

    contactSales(upgradeData: ISkuManagementContactSalesInfo): Observable<{}> {
        const url = upgradeData.salesforceUrl;
        const name = `${upgradeData.firstName} ${upgradeData.lastName}`;
        const contactNumberString = upgradeData.contactNumber ? `/ ${upgradeData.contactNumber}` : "";
        const description = `This free edition customer at ${upgradeData.company} submitted a request to contact sales. Please reach out to: ${name} / ${upgradeData.email} ${contactNumberString}.`;
        const subject = "Free Edition Contact Us Request";

        const contactSalesData = {
            "orgid": upgradeData.orgid,
            "retURL": "http://www.onetrust.com",
            "name": name,
            "Tenant_GUID__c": upgradeData.tenantId,
            "company": upgradeData.company,
            "email": upgradeData.email,
            "subject": subject,
            "description": description,
            "Interested_In__c": upgradeData.interestedDesc,
            "00N3600000TOsWF": "Customer",
            "00N3600000TOsWK": "App Contact Request",
            "type": "Task",
            "priority": "High",
            "status": "New",
            "recordType": "01236000000oCpg",
            "submit": "Submit",
        };

        return this.formPost(url, contactSalesData);
    }

    private formPost(url: string, data) {
        const formData = new FormData();
        for (const key in data) {
            if (data.hasOwnProperty(key)) {
                formData.append(key, data[key]);
            }
        }
        return this.httpClient.post(url, formData, { params: new InterceptorHttpParams({ tokenNotRequired: true }) });
    }
}
