// Angular
import { Injectable } from "@angular/core";

// Rxjs
import {
    first,
    filter,
    take,
    finalize,
} from "rxjs/operators";

// Vitreus
import { OtModalService, ModalSize } from "@onetrust/vitreus";

// Components
import { SkuManagementUpgradeModalComponent } from "../sku-management-upgrade-modal/sku-management-upgrade-modal.component";
import { SkuManagementSuccessModalComponent } from "../sku-management-sucess-modal/sku-management-success-modal.component";
import { SkuManagementTrialFailureModalComponent } from "../sku-management-trial-failure-modal/sku-management-trial-failure-modal.component";

// Services
import { NotificationService } from "sharedModules/services/provider/notification.service";
import { SkuManagementModalApiService } from "./sku-management-modal-api.service";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

// Interfaces
import { IStringMap } from "interfaces/generic.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";
import { IUser } from "interfaces/user.interface";
import { ISkuManagementContactSalesInfo, ISkuManagementModalInfo } from "interfaces/sku-management-modal-info.interface";

@Injectable()
export class SkuManagementModalService {

    constructor(
        private notificationService: NotificationService,
        private skuManagementModalApiService: SkuManagementModalApiService,
        private otModalService: OtModalService,
        private translatePipe: TranslatePipe,
    ) {}

    openSkuManagementModal(currentUser: IUser) {
        this.skuManagementModalApiService.getBuyNowInfo()
            .pipe(
                first(),
                filter((response) => response !== null),
            ).subscribe((response: IStringMap<string>) => {
                const pricingLink = response.BuyNowLink;
                const orgid = response.SalesforceOrgid;
                const salesforceUrl = response.SalesforceWebToCaseUrl;
                const modalData: ISkuManagementModalInfo = {
                    pricingLink,
                    currentUser,
                    salesforceUrl,
                    orgid,
                };
                this.otModalService.create(
                        SkuManagementUpgradeModalComponent,
                        {
                            isComponent: true,
                            size: ModalSize.MEDIUM,
                        },
                        modalData,
                    ).pipe(
                        take(1),
                    ).subscribe((data) => {
                        if (!data) return;
                        this.skuManagementModalApiService.checkTrialApproved().pipe(
                            first(),
                            filter((trialResponse) => trialResponse.result),
                        ).subscribe((trialResponse: IProtocolResponse<any>) => {
                            if (!trialResponse.data.length) {
                                const reqTrialData: ISkuManagementContactSalesInfo = {
                                    tenantId: currentUser.CurrentTenant,
                                    company: currentUser.OrgGroupName,
                                    fullName: currentUser.FullName,
                                    email: currentUser.Email,
                                    orgid,
                                    salesforceUrl,
                                };
                                this.skuManagementModalApiService.requestTrial(reqTrialData).pipe(
                                    first(),
                                    finalize(() => {
                                        this.openSuccessModal("TrialSubmit");
                                    }),
                                ).subscribe(
                                    // next
                                    () => {},
                                    // error
                                    () => {},
                                );
                            } else {
                                this.openTrialFailureModal(modalData);
                            }
                        });
                    });
            });
    }

    private openSuccessModal(successType: string) {
        this.otModalService.create(
            SkuManagementSuccessModalComponent,
            {
                isComponent: true,
            },
            { title: this.translatePipe.transform("RequestSubmitted"), successType },
        );
    }

    private showTrialFailureToast() {
        this.notificationService.alertError(
            this.translatePipe.transform("Error"),
            this.translatePipe.transform("UpgradeErrorMessage"),
        );
    }

    private openTrialFailureModal(modalData: ISkuManagementModalInfo) {
        this.otModalService.create(
            SkuManagementTrialFailureModalComponent,
            {
                isComponent: true,
                size: ModalSize.SMALL,
            },
            modalData,
        );
    }
}
