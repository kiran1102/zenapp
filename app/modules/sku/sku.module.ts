// Angular
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { HttpClientModule } from "@angular/common/http";
import { ReactiveFormsModule } from "@angular/forms";

// Vitreus
import {
    OtButtonModule,
    OtInputModule,
    OtFormModule,
    OtModalModule,
} from "@onetrust/vitreus";

import { OTPipesModule } from "modules/pipes/pipes.module";

// Components
import { SkuManagementUpgradeModalComponent } from "./sku-management-upgrade-modal/sku-management-upgrade-modal.component";
import { SkuManagementSuccessModalComponent } from "./sku-management-sucess-modal/sku-management-success-modal.component";
import { SkuManagementTrialFailureModalComponent } from "./sku-management-trial-failure-modal/sku-management-trial-failure-modal.component";
import { SkuManagementContactSalesModalComponent } from "./sku-management-contact-sales-modal/sku-management-contact-sales-modal.component";

// Services
import { SkuManagementModalService } from "./shared/sku-management-modal.service";
import { SkuManagementModalApiService } from "./shared/sku-management-modal-api.service";

@NgModule({
    imports: [
        CommonModule,
        HttpClientModule,
        ReactiveFormsModule,
        OTPipesModule,
        OtButtonModule,
        OtInputModule,
        OtFormModule,
        OtModalModule,
    ],
    providers: [
        SkuManagementModalService,
        SkuManagementModalApiService,
    ],
    declarations: [
        SkuManagementContactSalesModalComponent,
        SkuManagementTrialFailureModalComponent,
        SkuManagementSuccessModalComponent,
        SkuManagementUpgradeModalComponent,
    ],
    entryComponents: [
        SkuManagementContactSalesModalComponent,
        SkuManagementTrialFailureModalComponent,
        SkuManagementSuccessModalComponent,
        SkuManagementUpgradeModalComponent,
    ],
})
export class SkuModule { }
