// Angular
import { Component, OnInit } from "@angular/core";

// Rxjs
import { Subject } from "rxjs";

// Vitreus
import { IOtModalContent } from "@onetrust/vitreus";

// Interfaces
import { ISkuManagementModalInfo } from "interfaces/sku-management-modal-info.interface";

@Component({
    selector: "sku-management--success-modal",
    templateUrl: "sku-management-success-modal.component.html",
})
export class SkuManagementSuccessModalComponent
    implements IOtModalContent, OnInit {
    otModalCloseEvent: Subject<boolean>;
    successType: string;
    otModalData: ISkuManagementModalInfo;

    ngOnInit() {
        this.successType = this.otModalData.successType;
    }
    closeSuccessModal() {
        this.otModalCloseEvent.next(true);
    }
}
