import angular from "angular";

import { routes } from "./controls.routes";

import { ControlsWrapperComponent } from "./controls-wrapper.component";

export const controlsLegacyModule = angular
    .module("zen.controls", [
        "ui.router",
    ])
    .config(routes)
    .component("controlsWrapper", ControlsWrapperComponent)
    ;
