import { NgModule } from "@angular/core";

// Modules
import { SharedModule } from "sharedModules/shared.module";
import { CommonModule } from "@angular/common";
import { HttpClientModule } from "@angular/common/http";
import { FormsModule } from "@angular/forms";
import { ReactiveFormsModule } from "@angular/forms";
import { VitreusModule } from "@onetrust/vitreus";
import { DndListModule } from "ngx-drag-and-drop-lists";
import { OTPipesModule, PIPES } from "modules/pipes/pipes.module";
import { VirtualScrollModule } from "od-virtualscroll";
import { OneVideoModule } from "modules/video/video.module";
import { ControlsLibraryModule } from "controls/controls-library/controls-library.module";
import { ControlPageModule } from "controls/control-page/control-page.module";
import { ControlsSharedModule } from "controls/shared/controls-shared.module";
import { VendorSharedModule } from "modules/vendor/shared/vendor-shared.module";

@NgModule({
    imports: [
        SharedModule,
        CommonModule,
        HttpClientModule,
        FormsModule,
        ReactiveFormsModule,
        VitreusModule,
        DndListModule,
        OTPipesModule,
        VirtualScrollModule,
        OneVideoModule,
        ControlsLibraryModule,
        ControlPageModule,
        ControlsSharedModule,
        VendorSharedModule,
    ],
    exports: [
        ControlsSharedModule,
    ],
    providers: [
        PIPES, // TODO: Remove from providers when ot-common is implemented. Only necessary if you are using pipe in TS file, else just module is fine
    ],
})
export class ControlsModule { }
