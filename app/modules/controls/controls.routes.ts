import { IGeneralData } from "app/scripts/PIA/pia.routes";
import { Permissions } from "modules/shared/services/helper/permissions.service";
import { InventoryDetailsTabs } from "constants/inventory.constant";
import { InventoryTableIds } from "enums/inventory.enum";

export function routes($stateProvider: ng.ui.IStateProvider) {
    $stateProvider
        .state("zen.app.pia.module.risks.views.controls", {
            abstract: true,
            url: "",
            resolve: {
                ControlsPermission: [
                    "GeneralData",
                    "$state",
                    "Permissions",
                    (
                        GeneralData: IGeneralData,
                        $state: ng.ui.IStateService,
                        permissions: Permissions,
                    ): boolean => {
                        if (permissions.canShow("ControlsLibrary")) {
                            return true;
                        } else {
                            permissions.goToFallback();
                        }
                        return false;
                    },
                ],
            },
            template: "<ui-view></ui-view>",
        })
        .state("zen.app.pia.module.risks.views.controls.library", {
            url: "/controls-library",
            template: "<downgrade-risks-wrapper><downgrade-controls-library></downgrade-controls-library></downgrade-risks-wrapper>",
        })
        .state("zen.app.pia.module.risks.views.controls.control", {
            url: "/controls-library/:controlId/:editMode",
            template: "<downgrade-risks-wrapper><downgrade-control-page></downgrade-control-page></downgrade-risks-wrapper>",
            params: {
                controlId: null,
            },
        })
    ;
}

routes.$inject = ["$stateProvider"];
