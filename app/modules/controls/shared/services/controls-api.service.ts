// Core
import { Injectable } from "@angular/core";

// Services
import { ProtocolService } from "modules/core/services/protocol.service";
import { TaskPollingService } from "sharedServices/task-polling.service";

// Interfaces
import { IStringMap } from "interfaces/generic.interface";
import {
    IProtocolConfig,
    IProtocolMessages,
    IProtocolResponse,
} from "interfaces/protocol.interface";
import {
    IPageableOf,
    IPaginationFilter,
    IPaginationParams,
} from "interfaces/pagination.interface";
import {
    IControlsLibraryResponse,
    IFrameworkCatalogueResponse,
    IFrameworkListInformation,
} from "controls/shared/interfaces/controls-library.interface";
import { ICategoryInformation } from "controls/shared/interfaces/controls.interface";
import { IFrameworkControlInformation } from "modules/vendor/shared/interfaces/inventory-controls.interface";
import { ISamlSettingsResponse } from "interfaces/setting.interface";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

// Rxjs
import {
    from,
    Observable,
} from "rxjs";

@Injectable()
export class ControlsApiService {

    constructor(
        private protocolService: ProtocolService,
        private translatePipe: TranslatePipe,
        private taskPollingService: TaskPollingService,
    ) { }

    searchControlsLibrary(fullText: string, filters: IPaginationFilter[], paginationParams: IPaginationParams<any>, payload?): Observable<IProtocolResponse<IPageableOf<IControlsLibraryResponse>>> {
        const sort = paginationParams && paginationParams.sort && paginationParams.sort.sortKey && paginationParams.sort.sortOrder ? `${paginationParams.sort.sortKey},${paginationParams.sort.sortOrder}` : "";
        const config: IProtocolConfig = this.protocolService.customConfig("POST", `/api/control/v1/libraries/controls/pages`,
            { filters, ...paginationParams, sort }, { fullText: fullText ? fullText : "", filters: payload ? payload : null });
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("Vendor.Error.RetrievingControls") } };
        return from(this.protocolService.http(config, messages));
    }

    fetchControlsStatusOptions(): Observable<IProtocolResponse<string[]>> {
        const config: IProtocolConfig = this.protocolService.customConfig("GET", `/api/control/v1/libraries/controls/statuses`);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("Vendor.Error.RetrievingStatusOptions") } };
        return from(this.protocolService.http(config, messages));
    }

    fetchControlsCategories(): Observable<IProtocolResponse<ICategoryInformation[]>> {
        const config: IProtocolConfig = this.protocolService.customConfig("GET", `/api/control/v1/libraries/controls/categories`);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("Vendor.Error.RetrievingCategories") } };
        return from(this.protocolService.http(config, messages));
    }

    importControlsByFrameworkId(payload: string[]): Observable<IProtocolResponse<string>> {
        const config: IProtocolConfig = this.protocolService.customConfig("POST", `/api/control/v1/libraries/controls/catalogue/frameworks/controls/imports`, null, payload);
        const messages: IProtocolMessages = {
            Error: { custom: this.translatePipe.transform("Vendor.Error.AddFramework") },
            Success: { custom: this.translatePipe.transform("Vendor.Success.AddFramework") },
        };
        return from(this.protocolService.http(config, messages));
    }

    addControl(payload: IStringMap<string>): Observable<IProtocolResponse<any>> {
        const config: IProtocolConfig = this.protocolService.customConfig("POST", `/api/control/v1/libraries/controls`, null, payload);
        const messages: IProtocolMessages = {
            Error: { Hide: true },
            Success: { custom: this.translatePipe.transform("ControlSuccessfullyAdded") },
        };
        return from(this.protocolService.http(config, messages));
    }

    updateControl(controlId: string, payload: IStringMap<string>): Observable<IProtocolResponse<any>> {
        const config: IProtocolConfig = this.protocolService.customConfig("PUT", `/api/control/v1/libraries/controls/${controlId}/controls`, null, payload);
        const messages: IProtocolMessages = {
            Error: { Hide: true },
            Success: { custom: this.translatePipe.transform("ControlSuccessfullyUpdated") },
        };
        return from(this.protocolService.http(config, messages));
    }

    changeControlStatus(controlIds: string[], status: string): Observable<IProtocolResponse<ISamlSettingsResponse>> {
        const config: IProtocolConfig = this.protocolService.customConfig("PUT", `/api/control/v1/libraries/controls/statuses/controls`, null, { controlIds, status});
        const messages: IProtocolMessages = {
            Error: { custom: this.translatePipe.transform("ErrorWhileArchivingControls") },
            Success: { custom: this.translatePipe.transform("ControlsSuccessfullyUpdated") },
        };
        return from(this.protocolService.http(config, messages));
    }

    fetchFrameworksFromControlLibrary(): Observable<IProtocolResponse<IFrameworkListInformation[]>> {
        const config: IProtocolConfig = this.protocolService.customConfig("GET", `/api/control/v1/libraries/controls/frameworks`);
        const messages: IProtocolMessages = { Error: { Hide: true } };
        return from(this.protocolService.http(config, messages));
    }

    fetchControlsLibraryStatus(): Observable<IProtocolResponse<string[]>> {
        const config: IProtocolConfig = this.protocolService.customConfig("GET", `/api/control/v1/libraries/controls/statuses`);
        const messages: IProtocolMessages = { Error: { Hide: true } };
        return from(this.protocolService.http(config, messages));
    }

    fetchControlById(controlId: string): Observable<IProtocolResponse<IControlsLibraryResponse>> {
        const config: IProtocolConfig = this.protocolService.customConfig("GET", `/api/control/v1/libraries/controls/${controlId}/controls`);
        const messages: IProtocolMessages = { Error: { Hide: true } };
        return from(this.protocolService.http(config, messages));
    }

    fetchAllFrameworks(): Observable<IProtocolResponse<IFrameworkListInformation[]>> {
        const config: IProtocolConfig = this.protocolService.customConfig("GET", `/api/control/v1/libraries/controls/frameworks`);
        const messages: IProtocolMessages = { Error: { Hide: true } };
        return from(this.protocolService.http(config, messages));
    }

    fetchAllCatalogueFrameworks(): Observable<IProtocolResponse<IFrameworkCatalogueResponse[]>> {
        const config: IProtocolConfig = this.protocolService.customConfig("GET", `/api/control/v1/libraries/controls/catalogue/frameworks`);
        const messages: IProtocolMessages = { Error: { Hide: true } };
        return from(this.protocolService.http(config, messages));
    }

    fetchControlsByFrameworkId(payload: string[]): Observable<IFrameworkControlInformation> {
        const config: IProtocolConfig = this.protocolService.customConfig("POST", `/api/control/v1/libraries/controls/frameworks/identifiers/frameworks/controls`, null, payload);
        const messages: IProtocolMessages = { Error: { Hide: true } };
        return from(this.protocolService.http(config, messages)
            .then((res: IProtocolResponse<IFrameworkControlInformation>) => res.result ? res.data[0] : null),
        );
    }

    archiveBulkControls(contractIds: string[]): Observable<IProtocolResponse<ISamlSettingsResponse>> {
        const payload = {
            controlIds: contractIds,
            status: "Archived",
        };
        const config: IProtocolConfig = this.protocolService.customConfig("PUT", `/api/control/v1/libraries/controls/statuses/controls`, null, payload);
        const messages: IProtocolMessages = {
            Error: { custom: this.translatePipe.transform("ErrorWhileArchivingControls") },
            Success: { custom: this.translatePipe.transform("ControlsSuccessfullyArchived") },
        };
        return from(this.protocolService.http(config, messages));
    }

    exportControls(filterPayload, searchText: string, sort: string): Observable<IProtocolResponse<any>> {
        this.taskPollingService.startPolling(5000);
        const config: IProtocolConfig = this.protocolService.customConfig("POST", `/api/control/v1/libraries/controls/exports`, { sort }, { filters: filterPayload, fullText: searchText });
        const messages: IProtocolMessages = { Error: { Hide: true } };
        return from(this.protocolService.http(config, messages));
    }
}
