// TODO: Move to Controls Module once created.
import { Injectable, Inject } from "@angular/core";

// Rxjs
import {
    BehaviorSubject,
    Observable,
    Subject,
} from "rxjs";
import {
    map,
    distinctUntilChanged,
    takeUntil,
} from "rxjs/operators";

// Services
import { NotificationService } from "sharedModules/services/provider/notification.service";
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import { ControlsApiService } from "controls/shared/services/controls-api.service";
import { StateService } from "@uirouter/core";
import { GridViewStorageService } from "sharedModules/services/helper/grid-view-storage.service";

// Interfaces
import { IProtocolResponse } from "interfaces/protocol.interface";
import {
    IControlsLibraryResponse,
    IFrameworkListInformation,
    IFrameworkCatalogueResponse,
} from "controls/shared/interfaces/controls-library.interface";
import {
    IPageableOf,
    IPageableMeta,
    IPaginationParams,
} from "interfaces/pagination.interface";
import { ISamlSettingsResponse } from "interfaces/setting.interface";
import { ICategoryInformation } from "controls/shared/interfaces/controls.interface";
import { IFrameworkControlInformation } from "modules/vendor/shared/interfaces/inventory-controls.interface";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

// Contants
import { DefaultPagination } from "../constants/controls.constants";
import { GridViewStorageNames } from "sharedModules/enums/grid-view-storage.enum";

@Injectable()
export class ControlsLibraryService {

    controlsLibrary$ = new BehaviorSubject<IControlsLibraryResponse[]>(null);
    controlsLibraryMeta$ = new BehaviorSubject<IPageableMeta>(null);
    controlsLoading$ = new BehaviorSubject<boolean>(false);
    controlsFrameworks$ = new BehaviorSubject<IFrameworkListInformation[]>(null);
    controlsAllCategories$ = new BehaviorSubject<ICategoryInformation[]>(null);
    controlsAllStatuses$ = new BehaviorSubject<string[]>(null);
    controlsCatalogueFrameworks$ = new BehaviorSubject<IFrameworkCatalogueResponse[]>(null);
    controlsSearchName$ = new BehaviorSubject<string>(null);
    paginationParams$ = new BehaviorSubject<{ page: number, size: number, sort: { sortKey: string, sortOrder: string } }>({ ...DefaultPagination });
    control$ = new BehaviorSubject<IControlsLibraryResponse>(null);
    controlLibraryStatus$ = new BehaviorSubject<string[]>(null);
    storageKey = GridViewStorageNames.CONTROLS_LIBRARY_LIST;
    destroy$ = new Subject();

    constructor(
        @Inject("$rootScope") private rootScope: IExtendedRootScopeService,
        private browserStorageListService: GridViewStorageService,
        private translatePipe: TranslatePipe,
        private controlsApiService: ControlsApiService,
        private notificationService: NotificationService,
        private stateService: StateService,
    ) {
        this.clearVendorCache();
        this.paginationParams$.pipe(
            distinctUntilChanged(),
            takeUntil(this.destroy$),
        ).subscribe((paginationParam: { page: number, size: number, sort: { sortKey: string, sortOrder: string } }) => {
            if (JSON.stringify(this.browserStorageListService.getBrowserStorageList(this.storageKey).pagination) !== JSON.stringify(paginationParam)) {
                this.browserStorageListService.saveSessionStoragePagination(this.storageKey, paginationParam);
            }
        });
    }

    ngOnDestroy() {
        this.destroy$.next();
        this.destroy$.complete();
    }

    reset() {
        this.controlsLibrary$.next(null);
        this.controlsLibraryMeta$.next(null);
        this.controlsFrameworks$.next(null);
        this.controlsCatalogueFrameworks$.next(null);
        this.control$.next(null);
    }

    retrieveControlsLibrary(payload?, pagination?: IPaginationParams<any>) {
        pagination = pagination ? pagination : this.paginationParams$.getValue();
        this.controlsLoading$.next(true);
        this.controlsApiService.searchControlsLibrary(
            this.controlsSearchName$.getValue(),
            null,
            pagination,
            payload,
        ).subscribe((res: IProtocolResponse<IPageableOf<IControlsLibraryResponse>>) => {
            if (res.result) {
                const controlsLibraryMeta = { ...res.data };
                delete controlsLibraryMeta.content;
                this.controlsLibraryMeta$.next(controlsLibraryMeta);
                this.controlsLibrary$.next(res.data.content);
                if (!controlsLibraryMeta.first && controlsLibraryMeta.numberOfElements === 0 && controlsLibraryMeta.number > 0) {
                    pagination.page--;
                    this.retrieveControlsLibrary(payload, pagination);
                } else {
                    this.controlsLoading$.next(false);
                }
                if (controlsLibraryMeta.size === DefaultPagination.size)
                    this.paginationParams$.next({
                        page: controlsLibraryMeta.number,
                        size: controlsLibraryMeta.size,
                        sort: {
                            sortKey: controlsLibraryMeta.sort[0].property,
                            sortOrder: (controlsLibraryMeta.sort[0].direction as string).toLowerCase(),
                        },
                    });
            }
        });
    }

    retrieveControlById(controlId: string) {
        this.controlsApiService.fetchControlById(controlId)
            .subscribe((res: IProtocolResponse<IControlsLibraryResponse>) => {
                this.control$.next(res.result ? res.data : null);
            });
    }

    getFrameworksFromControlLibrary() {
        this.controlsApiService.fetchFrameworksFromControlLibrary().subscribe((res: IProtocolResponse<IFrameworkListInformation[]>) => {
            if (res.result) {
                this.controlsFrameworks$.next(res.data);
            } else {
                this.notificationService.alertError(this.translatePipe.transform("Error"), this.translatePipe.transform("Vendor.Error.RetrievingFrameworks"));
            }
        });
    }

    retrieveAllFrameworks() {
        this.controlsApiService.fetchAllFrameworks().subscribe((res: IProtocolResponse<IFrameworkListInformation[]>) => {
            if (res.result) {
                this.controlsFrameworks$.next(res.data);
            } else {
                this.notificationService.alertError(this.translatePipe.transform("Error"), this.translatePipe.transform("Vendor.Error.RetrievingFrameworks"));
            }
        });
    }

    retrieveAllCatalogueFrameworks() {
        this.controlsApiService.fetchAllCatalogueFrameworks().subscribe((res: IProtocolResponse<IFrameworkCatalogueResponse[]>) => {
            if (res.result) {
                this.controlsCatalogueFrameworks$.next(res.data);
            } else {
                this.notificationService.alertError(this.translatePipe.transform("Error"), this.translatePipe.transform("Vendor.Error.RetrievingFrameworks"));
            }
        });
    }

    retrieveControlsLibraryStatus() {
        this.controlsApiService.fetchControlsLibraryStatus()
            .subscribe((res: IProtocolResponse<string[]>) => {
                if (res.result) {
                    this.controlLibraryStatus$.next(res.data);
                }
            });
    }

    importControlsByFrameworkId(payload: string[]): Observable<IProtocolResponse<string>> {
        return this.controlsApiService.importControlsByFrameworkId(payload);
    }

    retrieveAllCategories() {
        this.controlsApiService.fetchControlsCategories()
            .subscribe((res: IProtocolResponse<ICategoryInformation[]>) => {
                if (res.result) {
                    this.controlsAllCategories$.next(res.data);
                } else {
                    this.notificationService.alertError(
                        this.translatePipe.transform("Error"),
                        this.translatePipe.transform("Vendor.Error.RetrievingCategories"),
                    );
                }
            });
    }

    retrieveAllStatuses() {
        this.controlsApiService.fetchControlsStatusOptions()
            .subscribe((res: IProtocolResponse<string[]>) => {
                if (res.result) {
                    this.controlsAllStatuses$.next(res.data);
                } else {
                    this.notificationService.alertError(this.translatePipe.transform("Error"), this.translatePipe.transform("ErrorRetrievingStatuses"));
                }
            });
    }

    clearVendorCache() {
        this.rootScope.$on("event:auth-logout", () => {
            this.controlsSearchName$.next(null);
            this.paginationParams$.next({ ...DefaultPagination });
        });
    }

    fetchControlsByFrameworkId(payload: string[]): Observable<IFrameworkControlInformation> {
        return this.controlsApiService.fetchControlsByFrameworkId(payload);
    }

}
