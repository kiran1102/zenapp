export enum ControlsFormControlNames {
    CONTROLID = "controlID",
    NAME = "controlName",
    FRAMEWORK = "controlFramework",
    CATEGORY = "controlCategory",
    DESCRIPTION = "controlDescription",
    STATUS = "controlStatus",
}

export enum ControlDetailsTabOptions {
    Details = "DETAILS",
}
