export enum ControlsLibraryTableColumnNames {
    CONTROLID = "identifier",
    NAME = "name",
    ORGANIZATION = "orgGroupName",
    DESCRIPTION = "description",
    FRAMEWORK = "frameworkName",
    CATEGORY = "categoryName",
    STATUS = "status",
}

export enum ControlsLibraryFilterKeys {
    FRAMEWORK = "frameworkName",
    CONTROL_CATEGORY = "controlCategoryName",
    CATEGORY = "categoryName",
    CONTROL_STATUS = "status",
    MATURITY = "maturityNameKey",
    PRIMARY_VENDOR = "vendorName",
    VENDOR = "vendor",
    CONTRACT_STATUS = "statusKey",
}
