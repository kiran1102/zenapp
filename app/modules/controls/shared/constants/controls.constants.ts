export const DefaultPagination = {
    page: 0,
    size: 20,
    sort: { sortKey: "", sortOrder: "" },
};

export const ControlsStatusDescTranslationKeys = {
    Archived: "InventoryArchivedStatusDescription",
    Active: "ControlsActiveStatusDescription",
    Pending: "ControlsPendingStatusDescription",
};
