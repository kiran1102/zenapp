export interface IControlInformation {
    categoryId: string;
    categoryName: string;
    categoryNameKey: string;
    description: string;
    frameworkId: string;
    frameworkName?: string;
    id: string;
    identifier: string;
    name: string;
    orgGroupId: string;
}

export interface ICategoryInformation {
    id: string;
    name: string;
    nameKey: string;
}
