import { ITableColumnConfig } from "interfaces/table-config.interface";
import { IPageableMeta } from "interfaces/pagination.interface";

export interface IControlsLibraryTable {
    rows: IControlsLibraryResponse[];
    columns?: IControlLibraryTableColumn[];
    metaData?: IPageableMeta;
}

export interface IControlLibraryTableColumn extends ITableColumnConfig {
    filterKey?: string;
}

export interface IControlsLibraryResponse {
    id: string;
    identifier: string;
    name: string;
    description: string;
    recommendation: string;
    orgGroupId: string;
    orgGroupName: string;
    seedControlId: string;
    frameworkId: string;
    seedFrameworkId: string;
    frameworkName: string;
    frameworkNameKey: string;
    categoryId?: string;
    categoryName?: string;
    categoryNameKey?: string;
    status?: string;
    viewOnly: boolean;
}

export interface IControlLibraryQuery {
    query: IControlLibraryFilter[];
}

export interface IControlLibraryFilter {
    field: string;
    operator: string;
    value: string[];
}

export interface IFrameworkListInformation {
    frameworkId: string;
    frameworkName: string;
    frameworkNameKey: string;
}

export interface IFrameworkCatalogueResponse {
    id: string;
    name: string;
    nameKey: string;
    availableInControlsLibrary: boolean;
}
