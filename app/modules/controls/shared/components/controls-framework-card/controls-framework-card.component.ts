// Core
import {
    Component,
    Input,
    OnInit,
} from "@angular/core";

// Interfaces
import { IFrameworkCatalogueResponse } from "controls/shared/interfaces/controls-library.interface";

// Constants
import { GetLogoNameForCert } from "modules/vendor/certificates/shared/constants/certificates.constants";

@Component({
    selector: "controls-framework-card",
    templateUrl: "./controls-framework-card.component.html",
})
export class ControlsFrameworkCardComponent implements OnInit {

    @Input() controlsFramework: IFrameworkCatalogueResponse;
    imgSrc: string;

    ngOnInit() {
        this.imgSrc = `images/vendor/certificates/${GetLogoNameForCert(this.controlsFramework.name)}.png`;
    }
}
