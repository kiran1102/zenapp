// Rxjs
import { Subject } from "rxjs";
import {
    filter,
    takeUntil,
    take,
} from "rxjs/operators";

// Core
import {
    Component,
    EventEmitter,
    Input,
    OnInit,
    Output,
    Inject,
} from "@angular/core";
import { ILabelValue } from "interfaces/generic.interface";
import {
    IControlLibraryTableColumn,
    IFrameworkListInformation,
} from "controls/shared/interfaces/controls-library.interface";
import { ICategoryInformation } from "modules/controls/shared/interfaces/controls.interface";
import {
    IFilterOutput,
    IFilterV2,
} from "modules/filter/interfaces/filter.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";
import { IStore } from "interfaces/redux.interface";
import { IRelatedListOption } from "modules/vendor/shared/interfaces/vendor-upload-document.interface";

// Reducers
import { getCurrentOrgId } from "oneRedux/reducers/org.reducer";
import { StoreToken } from "tokens/redux-store.token";

// Enums
import { InventoryTableIds } from "enums/inventory.enum";
import { ControlsLibraryFilterKeys } from "controls/shared/enums/controls-library.enum";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

// Services
import { ControlsLibraryService } from "modules/controls/shared/services/controls-library.service";
import { GridViewStorageService } from "sharedModules/services/helper/grid-view-storage.service";
import { InventoryControlsService } from "modules/vendor/shared/services/inventory-controls.service";
import { InventoryService } from "modules/inventory/inventory-shared/shared/services/inventory-api.service";
import { ContractService } from "modules/vendor/contracts/shared/services/contract.service";

@Component({
    selector: "controls-filter",
    templateUrl: "./controls-filter.component.html",
    styles: [`
       :host {
           flex: 1 1 auto;
       }
    `],
})

export class ControlsFilterComponent implements OnInit {
    @Input() fields;
    @Input() currentFilters;
    @Input() storage;
    @Output() filterApplied = new EventEmitter();
    @Output() filterCanceled = new EventEmitter();

    selectedFilters = [];
    filterOperators = [
        {
            label: this.translatePipe.transform("EqualTo"),
            value: "EQUAL_TO",
            labelKey: "EqualTo",
        },
        {
            label: this.translatePipe.transform("NotEqualTo"),
            value: "NOT_EQUAL_TO",
            labelKey: "NotEqualTo",
        },
    ];
    currentOperator = this.filterOperators[0];
    currentFilterValues = [];
    currentFilterValueType: string;
    query = [];

    // Filter Values
    frameworks = [];
    categories = [];
    statuses = [];
    maturities = [];
    inventoryList: Array<ILabelValue<string>> = [];
    contractStatuses: Array<ILabelValue<string>> = [];

    // Toggle popup
    closeFilter$ = new Subject<boolean>();

    private destroy$ = new Subject();

    constructor(
        @Inject(StoreToken) private store: IStore,
        private inventoryService: InventoryService,
        private translatePipe: TranslatePipe,
        private controlsLibraryService: ControlsLibraryService,
        private inventoryControlsService: InventoryControlsService,
        private browserStorageListService: GridViewStorageService,
        private contractService: ContractService,
    ) { }

    ngOnInit() {
        if (this.currentFilters) this.selectedFilters = this.currentFilters;
        this.fetchAllFilterValues();
    }

    ngOnDestroy(): void {
        this.destroy$.next();
        this.destroy$.complete();
    }

    setFilterValues(selectedField: ILabelValue<string>) {
        if (!selectedField) return;
        const selectedColumn = this.fields.find((column) => column.cellValueKey === selectedField.value);
        this.currentFilterValueType = selectedColumn.type.charAt(0).toUpperCase() + selectedColumn.type.slice(1);

        switch (selectedColumn.sortKey) {
            case ControlsLibraryFilterKeys.FRAMEWORK:
                this.currentFilterValues = this.frameworks;
                break;
            case ControlsLibraryFilterKeys.CONTROL_CATEGORY:
            case ControlsLibraryFilterKeys.CATEGORY:
                this.currentFilterValues = this.categories;
                break;
            case ControlsLibraryFilterKeys.CONTROL_STATUS:
                this.currentFilterValues = this.statuses;
                break;
            case ControlsLibraryFilterKeys.MATURITY:
                this.currentFilterValues = this.maturities;
                break;
            case ControlsLibraryFilterKeys.PRIMARY_VENDOR:
            case ControlsLibraryFilterKeys.VENDOR:
                this.currentFilterValues = this.inventoryList;
                break;
            case ControlsLibraryFilterKeys.CONTRACT_STATUS:
                this.currentFilterValues = this.contractStatuses;
                break;
        }
    }

    selectOperator(operator?) {
        this.currentOperator = operator.value || this.currentOperator;
    }

    applyFilters(filters) {
        if (!filters) return;
        this.closeFilter$.next(false);
        this.selectedFilters = this.deserializeFilters(filters);
        this.browserStorageListService.saveLocalStorageFilters(this.storage, this.selectedFilters);
        this.filterApplied.emit(this.selectedFilters);
    }

    deserializeFilters(filters: IFilterOutput[]): Array<IFilterV2<IControlLibraryTableColumn, ILabelValue<string>, ILabelValue<string>>> {
        return filters.map((item) => {
            const matchingColumn = this.fields.find((column): boolean => column.cellValueKey === item.field.value);
            return ({
                field: matchingColumn,
                operator: item.operator || this.filterOperators[0],
                values: item.values,
                type: item.type,
            } as any);
        });
    }

    removeFilter(filters) {
        this.selectedFilters = this.deserializeFilters(filters.currentList);
        this.selectedFilters.splice(filters.index, 1);
    }

    addFilter(filters) {
        filters.currentList.push(filters.filter);
        this.selectedFilters = this.deserializeFilters(filters.currentList);
    }

    removeAllFilters() {
        this.selectedFilters = [];
    }

    cancel() {
        this.selectedFilters = this.currentFilters;
        this.closeFilter$.next(false);
        this.filterCanceled.emit(this.currentFilters);
    }

    getFieldLabelValue(column): ILabelValue<string> {
        return { label: column.name, value: column.sortKey };
    }

    getOperatorLabelValue(operator: ILabelValue<string>): ILabelValue<string> {
        return operator;
    }

    // Fetch filter values
    fetchAllFilterValues() {
        this.fields.forEach((element) => {
            switch (element.sortKey) {
                case ControlsLibraryFilterKeys.FRAMEWORK:
                    this.fetchFrameworks();
                    break;
                case ControlsLibraryFilterKeys.CONTROL_CATEGORY:
                case ControlsLibraryFilterKeys.CATEGORY:
                    this.fetchCategories();
                    break;
                case ControlsLibraryFilterKeys.CONTROL_STATUS:
                    this.fetchStatuses();
                    break;
                case ControlsLibraryFilterKeys.MATURITY:
                    this.fetchMaturities();
                    break;
                case ControlsLibraryFilterKeys.PRIMARY_VENDOR:
                case ControlsLibraryFilterKeys.VENDOR:
                    this.fetchInventoryList();
                    break;
                case ControlsLibraryFilterKeys.CONTRACT_STATUS:
                    this.fetchContractStatuses();
                    break;
            }
        });
    }

    fetchFrameworks() {
        this.controlsLibraryService.retrieveAllFrameworks();
        this.controlsLibraryService.controlsFrameworks$.pipe(
            takeUntil(this.destroy$),
        ).subscribe((vendorFrameworks: IFrameworkListInformation[]) => {
            if (vendorFrameworks) {
                this.frameworks = vendorFrameworks.map((item) => ({ value: item.frameworkId, label: item.frameworkName }));
                this.frameworks.push({
                    value: "blank",
                    label: this.translatePipe.transform("Blanks"),
                    labelKey: "Blanks",
                });
            }
        });
    }

    fetchCategories() {
        this.controlsLibraryService.retrieveAllCategories();
        this.controlsLibraryService.controlsAllCategories$.pipe(
            takeUntil(this.destroy$),
        ).subscribe((categoriesResponse: ICategoryInformation[]) => {
            if (categoriesResponse) {
                this.categories = categoriesResponse.map((item) => ({ value: item.id, label: item.name }));
            }
            this.categories.push({
                value: "blank",
                label: this.translatePipe.transform("Blanks"),
                labelKey: "Blanks",
            });
        });
    }

    fetchStatuses() {
        this.controlsLibraryService.retrieveAllStatuses();
        this.controlsLibraryService.controlsAllStatuses$.pipe(
            takeUntil(this.destroy$),
            filter((statusResponse) => Boolean(statusResponse)),
        ).subscribe((statusResponse) => {
            this.statuses = statusResponse.map((item) => {
                return {
                    value: item,
                    label: this.translatePipe.transform(item),
                    labelKey: item,
                };
            });
        });
    }

    fetchMaturities() {
        this.inventoryControlsService.retrieveAllMaturities();
        this.inventoryControlsService.controlAllMaturities$.pipe(
            takeUntil(this.destroy$),
            filter(Boolean),
        ).subscribe((controlMaturities) => {
            this.maturities = controlMaturities.map((item) => {
                return {
                    value: item.id,
                    label: this.translatePipe.transform(item.nameKey),
                    labelKey: item.nameKey,
                };
            });
            this.maturities.push({
                value: "blank",
                label: this.translatePipe.transform("Blanks"),
                labelKey: "Blanks",
            });
        });
    }

    fetchContractStatuses() {
        this.contractService.retrieveContractStatuses();
        this.contractService.contractStatuses$.pipe(
            takeUntil(this.destroy$),
            filter(Boolean),
        ).subscribe((contractStatuses) => {
            this.contractStatuses = contractStatuses.map((item) => {
                return {
                    value: item.value,
                    label: this.translatePipe.transform(item.label),
                    labelKey: item.label,
                };
            });
        });
    }

    fetchInventoryList() {
        const orgId = getCurrentOrgId(this.store.getState());
        this.inventoryService.getSearchedInventoryList(InventoryTableIds.Vendors, orgId).then((response: IProtocolResponse<IRelatedListOption[]>) => {
            if (response.result) {
                this.inventoryList = this.formatInventoryList(response.data);
            } else {
                this.inventoryList = [];
            }
        }).catch(() => {
            this.inventoryList = [];
        });
    }

    private formatInventoryList(options: IRelatedListOption[]): Array<ILabelValue<string>> {
        return options.map((option: IRelatedListOption): ILabelValue<string> => {
            let label = `${option.name} - ${option.organization.value}`;
            if (option.location) {
                label = `${option.name} - ${option.organization.value} - ${option.location.value}`;
            } else if (option.type) {
                label = `${option.name} - ${option.organization.value} - ${option.type.value}`;
            }
            return {
                label,
                value: option.id,
            };
        });
    }

}
