import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from "@angular/core";

// Modules
import { SharedModule } from "sharedModules/shared.module";
import { UpgradedLegacyModule } from "sharedModules/upgrade/upgraded-legacy.module";
import { CommonModule } from "@angular/common";
import { HttpClientModule } from "@angular/common/http";
import { FormsModule } from "@angular/forms";
import { ReactiveFormsModule } from "@angular/forms";
import { VitreusModule } from "@onetrust/vitreus";
import { DndListModule } from "ngx-drag-and-drop-lists";
import { OTPipesModule, PIPES } from "modules/pipes/pipes.module";
import { VirtualScrollModule } from "od-virtualscroll";
import { OneVideoModule } from "modules/video/video.module";
import { FilterModule } from "modules/filter/filter.module";

// Services
import { ControlsApiService } from "controls/shared/services/controls-api.service";
import { ControlsLibraryService } from "controls/shared/services/controls-library.service";

// Components
import { ControlsFrameworkCardComponent } from "controls/shared/components/controls-framework-card/controls-framework-card.component";
import { ControlsFilterComponent } from "controls/shared/components/controls-filter/controls-filter.component";
import { ControlsFilterService } from "controls/shared/components/controls-filter/controls-filter.service";

@NgModule({
    imports: [
        SharedModule,
        UpgradedLegacyModule,
        CommonModule,
        HttpClientModule,
        FormsModule,
        ReactiveFormsModule,
        VitreusModule,
        DndListModule,
        OTPipesModule,
        VirtualScrollModule,
        OneVideoModule,
        FilterModule,
    ],
    declarations: [
        ControlsFrameworkCardComponent,
        ControlsFilterComponent,
    ],
    exports: [
        ControlsFrameworkCardComponent,
        ControlsFilterComponent,
    ],
    entryComponents: [],
    providers: [
        ControlsApiService,
        ControlsLibraryService,
        ControlsFilterService,
        PIPES, // TODO: Remove from providers when ot-common is implemented. Only necessary if you are using pipe in TS file, else just module is fine
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class ControlsSharedModule {}
