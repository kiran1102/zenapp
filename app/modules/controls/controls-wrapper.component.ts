// Services
import { StateService } from "@uirouter/core";
import { Permissions } from "modules/shared/services/helper/permissions.service";
import { GlobalSidebarService } from "modules/global-sidebar/services/one-global-sidebar.service";
import { TenantTranslationService } from "sharedServices/tenant-translation.service";
import { Wootric } from "sharedModules/services/provider/wootric.service";

// Interfaces
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import { INavMenuItem } from "modules/global-sidebar/interfaces/nav-menu-item.interface";

// Constants / Enums
import { TranslationModules } from "constants/translation.constant";

// Constants
import { WootricModuleMap } from "modules/shared/constants/wootric.constants";

class ControlsWrapperController implements ng.IComponentController {
    static $inject: string[] = [
        "$rootScope",
        "Permissions",
        "GlobalSidebarService",
        "TenantTranslationService",
        "Wootric",
    ];

    constructor(
        private $rootScope: IExtendedRootScopeService,
        private permissions: Permissions,
        private globalSidebarService: GlobalSidebarService,
        private tenantTranslationService: TenantTranslationService,
        private wootric: Wootric,
    ) {}

    $onInit() {
        this.wootric.run(WootricModuleMap.ControlsLibrary);
    }
}

export const ControlsWrapperComponent: ng.IComponentOptions = {
    controller: ControlsWrapperController,
    template: `
        <div class="flex-full-height height-100 background-grey text-color-default">
            <div class="row-nowrap width-100-percent height-100 overflow-hidden">
                <section ui-view class="width-100-percent padding-all-0 background-grey min-width-1"></section>
            </div>
        </div>
    `,
};
