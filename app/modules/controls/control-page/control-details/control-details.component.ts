// Angular
import {
    Component,
    Input,
    Output,
    EventEmitter,
    ElementRef,
    OnInit,
    OnDestroy,
} from "@angular/core";
import {
    FormBuilder,
    FormGroup,
    Validators,
    AbstractControl,
} from "@angular/forms";

// 3rd Party
import {
    Transition,
    TransitionService,
    StateService,
    HookResult,
} from "@uirouter/core";

// Enums
import { ControlsLibraryTableColumnNames } from "controls/shared/enums/controls-library.enum";
import { ControlsFormControlNames } from "controls/shared/enums/controls.enum";
import { StatusCodes } from "enums/status-codes.enum";

// Constants
import { FormDataTypes } from "constants/form-types.constant";
import { ControlsExportErrorCodes } from "modules/controls/shared/constants/controls-export-error-codes.constants";

// Rxjs
import {
    BehaviorSubject,
    Subject,
    combineLatest,
} from "rxjs";
import {
    takeUntil,
    debounceTime,
    map,
} from "rxjs/operators";

// Pipes
import { ControlsLibraryService } from "controls/shared/services/controls-library.service";
import { FilterPipe } from "modules/pipes/filter.pipe";
import { TranslatePipe } from "modules/pipes/translate.pipe";

// Interfaces
import { IControlsLibraryResponse } from "controls/shared/interfaces/controls-library.interface";
import { ICreateData } from "modules/vendor/shared/interfaces/vendor-upload-document.interface";
import { IOTLookupSelection } from "sharedModules/components/ot-org-user/ot-org-user.interface";
import { IKeyValue } from "interfaces/generic.interface";
import { ICategoryInformation } from "controls/shared/interfaces/controls.interface";
import { IStringMap } from "@onetrust/dsar-components/lib/_shared/interfaces/generic.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";

// Services
import { ControlsApiService } from "controls/shared/services/controls-api.service";
import { NotificationService } from "sharedModules/services/provider/notification.service";

// Vitreus
import { OtModalService, ModalSize } from "@onetrust/vitreus";

// Modal Component
import { UnsavedChangesConfirmationModal } from "modules/shared/components/unsaved-changes-confirmation-modal/unsaved-changes-confirmation-modal.component";

@Component({
    selector: "control-details",
    templateUrl: "./control-details.component.html",
})
export class ControlDetailsComponent implements OnInit, OnDestroy {

    @Input() controlId: string;
    @Input() editMode: boolean;
    @Input() isEditable: boolean;
    @Output() toggleEdit = new EventEmitter<boolean>();

    controlForm: FormGroup;
    defaultLookupModel = { id: null, name: null, nameKey: null };
    frameworks: ICategoryInformation[];
    categories: ICategoryInformation[];
    filterredFrameworks: ICategoryInformation[];
    filterredCategories: ICategoryInformation[];
    statusOptions: ICategoryInformation[];
    categoryInputChange$: Subject<ICategoryInformation> = new Subject();
    frameworkInputChange$: Subject<ICategoryInformation> = new Subject();
    isSaving = false;
    control: IControlsLibraryResponse;
    formDataTypes = FormDataTypes;
    formData: ICreateData[] = [];
    hasEdits = false;
    emptyData = "- - - -";

    private transitionDeregisterHook: () => any;
    private destroy$ = new Subject();

    constructor(
        private element: ElementRef,
        private fb: FormBuilder,
        private controlsLibraryService: ControlsLibraryService,
        private filterPipe: FilterPipe,
        private stateService: StateService,
        private $transitions: TransitionService,
        private controlsApiService: ControlsApiService,
        private otModalService: OtModalService,
        private translatePipe: TranslatePipe,
        private notificationService: NotificationService,
    ) { }

    ngOnInit() {
        this.controlsLibraryService.retrieveAllFrameworks();
        combineLatest(
            this.controlsLibraryService.control$,
            this.controlsLibraryService.controlsAllCategories$,
            this.controlsLibraryService.controlsFrameworks$,
            this.controlsLibraryService.controlLibraryStatus$,
        ).pipe(
            takeUntil(this.destroy$),
        ).subscribe(([control, controlCategories, controlFrameworks, controlStatus]) => {
            if (control) {
                this.control = control;
                this.hasEdits = false;
                this.generateControlFormGroup();
                this.onFormChanges();
            }
            if (controlCategories) {
                this.categories = controlCategories.map((item) => {
                    return { ...item, name: item.nameKey ? this.translatePipe.transform(item.nameKey) : item.name };
                });
                this.filterredCategories = [...this.categories];
            }
            if (controlFrameworks) {
                this.frameworks = controlFrameworks.map((item) => {
                    return { id: item.frameworkId, name: item.frameworkNameKey ? this.translatePipe.transform(item.frameworkNameKey) : item.frameworkName, nameKey: item.frameworkNameKey };
                });
                this.filterredFrameworks = [...this.frameworks];
            }
            if (controlStatus) {
                this.statusOptions = controlStatus.map((status) => {
                    if (status) return { nameKey: status, name: this.translatePipe.transform(status), id: null };
                });
            }
            this.generateFormData();
        });
        this.configureInputListener();
        this.setTransitionAwayHook();
    }

    ngOnDestroy() {
        this.destroy$.next();
        this.destroy$.complete();
        if (this.transitionDeregisterHook) {
            this.transitionDeregisterHook();
        }
    }

    // Reactive form related Functions.
    generateFormData() {
        this.formData = [
            {
                name: ControlsLibraryTableColumnNames.CONTROLID,
                label: "ControlID",
                type: FormDataTypes.INPUT,
                isRequired: true,
                requiredErrorMessage: "EnterControlNumber",
                formControlName: "controlId",
                placeholder: "EnterControlNumber",
                id: "ControlsControlDetailsNumberTextInput",
            },
            {
                name: ControlsLibraryTableColumnNames.NAME,
                label: "Name",
                type: FormDataTypes.TEXTAREA,
                isRequired: true,
                requiredErrorMessage: "EnterControlName",
                formControlName: "controlName",
                placeholder: "EnterControlName",
                rows: "2",
                id: "ControlsControlDetailsNameTextarea",
            },
            {
                labelKey: "name",
                valueKey: "nameKey",
                name: ControlsLibraryTableColumnNames.STATUS,
                label: "Status",
                type: FormDataTypes.PICKLIST,
                isRequired: true,
                formControlName: ControlsFormControlNames.STATUS,
                options: this.statusOptions,
                id: "ControlsControlDetailsStatusPicklist",
            },
            {
                name: ControlsLibraryTableColumnNames.DESCRIPTION,
                label: "Description",
                type: FormDataTypes.TEXTAREA,
                isRequired: false,
                formControlName: "controlDescription",
                placeholder: "EnterControlDescription",
                rows: "2",
                id: "ControlsControlDetailsDescriptionTextarea",
            },
            {
                labelKey: "name",
                labelTranslationKey: "nameKey",
                valueKey: "id",
                name: ControlsLibraryTableColumnNames.FRAMEWORK,
                label: "Framework",
                type: FormDataTypes.LOOKUP,
                isRequired: false,
                formControlName: ControlsFormControlNames.FRAMEWORK,
                options: this.filterredFrameworks,
                id: "ControlsControlDetailsFrameworkLookup",
                inputValue: this.defaultLookupModel,
            },
            {
                labelKey: "name",
                labelTranslationKey: "nameKey",
                valueKey: "id",
                name: ControlsLibraryTableColumnNames.CATEGORY,
                label: "Category",
                type: FormDataTypes.LOOKUP,
                isRequired: false,
                formControlName: ControlsFormControlNames.CATEGORY,
                options: this.filterredCategories,
                id: "ControlsControlDetailsCategoryLookup",
                inputValue: this.defaultLookupModel,
            },
        ];
    }

    onFormChanges(): void {
        this.controlForm.valueChanges.subscribe((val) => {
            this.hasEdits = true;
        });
    }

    get controlCategory() { return this.controlForm.get(ControlsFormControlNames.CATEGORY); }
    get controlFramework() { return this.controlForm.get(ControlsFormControlNames.FRAMEWORK); }
    get controlStatus() { return this.controlForm.get(ControlsFormControlNames.STATUS); }

    // Handling lookup events
    onLookupModelChange(formControlName: string, event: IOTLookupSelection<ICategoryInformation>) {
        this.hasEdits = true;
        if (!event.change) return;
        const currentFormData: ICreateData = this.formData[this.formData.findIndex((data) => data.formControlName === ControlsFormControlNames.CATEGORY)];
        if (formControlName === ControlsFormControlNames.CATEGORY) {
            this.controlCategory.setValue(event.currentValue);
            this.controlCategory.markAsDirty();
            this.categoryInputChange$.next(null);
        }
        if (formControlName === ControlsFormControlNames.FRAMEWORK) {
            this.controlFramework.setValue(event.currentValue);
            this.controlFramework.markAsDirty();
            this.frameworkInputChange$.next(null);
        }
        currentFormData.inputValue = this.defaultLookupModel;
    }

    onDropdownChange(formControlName: string, event: ICategoryInformation) {
        this.hasEdits = true;
        const currentFormData: ICreateData = this.formData[this.formData.findIndex((data) => data.formControlName === ControlsFormControlNames.STATUS)];
        if (formControlName === ControlsFormControlNames.STATUS) {
            this.controlStatus.setValue(event);
            currentFormData.inputValue = event.nameKey;
        }
    }

    onLookupInputChange(formControlName: string, event: IKeyValue<string>) {
        this.controlForm.get(formControlName).markAsTouched();
        if (event.key === "Enter") {
            return;
        }
        const currentFormData: ICreateData = this.formData[this.formData.findIndex((data) => data.formControlName === formControlName)];
        if (currentFormData.inputValue) {
            if (event.key === "Backspace" && !event.value) {
                currentFormData.inputValue = { ...this.defaultLookupModel };
                formControlName === ControlsFormControlNames.CATEGORY ? this.categoryInputChange$.next(currentFormData.inputValue) : this.frameworkInputChange$.next(currentFormData.inputValue);
                return;
            }
            if (event.value) {
                currentFormData.inputValue = { ...this.defaultLookupModel, name: event.value };
                formControlName === ControlsFormControlNames.CATEGORY ? this.categoryInputChange$.next(currentFormData.inputValue) : this.frameworkInputChange$.next(currentFormData.inputValue);
                this.categoryInputChange$.next(currentFormData.inputValue);
            }
        }
    }

    onLookupAddNew(formControlName: string) {
        const currentFormData: ICreateData = this.formData[this.formData.findIndex((data) => data.formControlName === formControlName)];
        if (currentFormData.inputValue) {
            let error = null;
            if ((currentFormData.inputValue as ICategoryInformation).name.length > 100) {
                error = {
                    maxlength: { requiredLength: 100, actualLength: (currentFormData.inputValue as ICategoryInformation).name.length },
                };
            }
            if (formControlName === ControlsFormControlNames.CATEGORY) {
                this.controlCategory.setValue(currentFormData.inputValue);
                this.controlCategory.setErrors(error);

            }
            if (formControlName === ControlsFormControlNames.FRAMEWORK) {
                this.controlFramework.setValue(currentFormData.inputValue);
                this.controlFramework.setErrors(error);
            }
        }
        currentFormData.inputValue = this.defaultLookupModel;
    }

    resetInputValue(formControlName: string) {
        if (formControlName === ControlsFormControlNames.CATEGORY && !this.controlCategory.value) {
            this.categoryInputChange$.next(this.defaultLookupModel);
        }
        if (formControlName === ControlsFormControlNames.FRAMEWORK && !this.controlFramework.value) {
            this.frameworkInputChange$.next(this.defaultLookupModel);
        }
    }

    onToggleEdit() {
        if (this.isEditable) {
            this.editMode = true;
            this.hasEdits = false;
            this.toggleEdit.emit(this.editMode);
            this.focus();
        }
    }

    onSave(callback?: () => void) {
        if (this.controlForm.valid && this.hasEdits && !this.isSaving) {
            this.isSaving = true;
            const values = this.controlForm.value;
            const updateControlPayload: IStringMap<string> = {
                identifier: values.controlId ? values.controlId.trim() : null,
                name: values.controlName ? values.controlName.trim() : null,
                description: values.controlDescription ? values.controlDescription.trim() : null,
                recommendation: null,
                orgGroupId: this.control.orgGroupId,
                categoryId: values.controlCategory ? values.controlCategory.id : null,
                categoryName: values.controlCategory ? values.controlCategory.name.trim() : null,
                categoryNameKey: values.controlCategory ? values.controlCategory.nameKey : null,
                frameworkId: values.controlFramework ? values.controlFramework.id : null,
                frameworkName: values.controlFramework ? values.controlFramework.name.trim() : null,
                frameworkNameKey: values.controlFramework ? values.controlFramework.nameKey : null,
                status: values.controlStatus ? values.controlStatus.nameKey : null,
            };
            this.controlsApiService.updateControl(this.control.id, updateControlPayload)
                .pipe(takeUntil(this.destroy$))
                .subscribe((response: IProtocolResponse<IControlsLibraryResponse>) => {
                    if (response.result) {
                        this.controlsLibraryService.control$.next(response.result ? response.data : null);
                        if (callback) {
                            callback();
                        }
                        this.editMode = false;
                        this.hasEdits = false;
                    } else {
                        if (response.status === StatusCodes.BadRequest || response.status === StatusCodes.NotFound) {
                            let errorMsg = `${this.translatePipe.transform("Controls.Error.UpdateControlDetails")}`;
                            if (response.errorCode === ControlsExportErrorCodes.DUPLICATE_CONTROL) {
                                errorMsg = `${this.translatePipe.transform("ControlAlreadyExists")}`;
                            }
                            this.notificationService.alertError(this.translatePipe.transform("Error"), errorMsg);
                        }
                    }
                    this.isSaving = false;
                });
        }
    }

    cancelAction() {
        if (!this.isSaving) {
            this.editMode = false;
            this.hasEdits = false;
            this.generateControlFormGroup();
            this.onFormChanges();
            this.toggleEdit.emit(this.editMode);
        }
    }

    // Form builder
    private generateControlFormGroup() {
        this.controlForm = this.fb.group({
            controlId: [this.control.identifier || null, [Validators.required, Validators.minLength(1), Validators.maxLength(50), this.validateControlNumber]],
            controlName: [this.control.name, [Validators.required, Validators.minLength(1), Validators.maxLength(300), this.validateControlName]],
            controlDescription: [this.control.description, [Validators.maxLength(500)]],
            controlFramework: [this.control.frameworkId ? {
                id: this.control.frameworkId,
                name: this.control.frameworkNameKey ? this.translatePipe.transform(this.control.frameworkNameKey) : this.control.frameworkName,
                nameKey: this.control.frameworkNameKey,
            } : null, Validators.maxLength(100)],
            controlCategory: [this.control.categoryId ? {
                id: this.control.categoryId,
                name: this.control.categoryNameKey ? this.translatePipe.transform(this.control.categoryNameKey) : this.control.categoryName,
                nameKey: this.control.categoryNameKey,
            } : null, Validators.maxLength(100)],
            controlStatus: [this.control.status ? {
                id: this.control.status,
                name: this.translatePipe.transform(this.control.status),
                nameKey: this.control.status,
            } : null],
        });
    }

    private focus() {
        setTimeout(() => {
            this.setupInputContents();
        }, 0);
    }

    private setupInputContents() {
        const inputEl: HTMLElement = this.getInputElement(this.element.nativeElement);
        if (inputEl) {
            inputEl.focus();
            inputEl.click();
        }
    }

    private getInputElement(elem: HTMLElement): HTMLElement {
        return (elem && elem[0]) ? elem[0].querySelector("input") : null;
    }

    private validateControlNumber(control: AbstractControl) {
        if ((control.value || "").trim().length === 0) {
            return { controlNumber: { value: "" } };
        }
    }

    private validateControlName(control: AbstractControl) {
        if ((control.value || "").trim().length === 0) {
            return { controlName: { value: "" } };
        }
    }

    private handleStateChange(toState: IStringMap<any>, toParams: IStringMap<any>, options: IStringMap<any>): boolean {
        if (!this.hasEdits) return true;
        const callback = () => {
            this.stateService.go(toState, toParams, options);
        };
        this.launchSaveChangesModal(callback);
        return false;
    }

    private launchSaveChangesModal(callback: () => void): void {
        this.otModalService
            .create(
                UnsavedChangesConfirmationModal,
                {
                    isComponent: true,
                    size: ModalSize.SMALL,
                    hideClose: true,
                },
                {
                    promiseToResolve: () => {
                        this.onSave(callback);
                    },
                    discardCallback: () => {
                        this.cancelAction();
                        callback();
                    },
                },
            ).pipe(
                takeUntil(this.destroy$),
            ).subscribe();
    }

    private setTransitionAwayHook(): void {
        this.transitionDeregisterHook = this.$transitions.onExit({ from: "zen.app.pia.module.risks.views.controls.control" }, (transition: Transition): HookResult => {
            return this.handleStateChange(transition.to(), transition.params("to"), transition.options);
        }) as any;
    }

    private configureInputListener() {
        this.categoryInputChange$.pipe(
            debounceTime(300),
            map((category: ICategoryInformation) => this.filterPipe.transform(this.categories, ["name"], category != null ? category.name : "")),
            takeUntil(this.destroy$),
        ).subscribe((categoryList: ICategoryInformation[]) => {
            this.formData[this.formData.findIndex((data) => data.formControlName === ControlsFormControlNames.CATEGORY)].options = categoryList;
        });
        this.frameworkInputChange$.pipe(
            debounceTime(300),
            map((framework: ICategoryInformation) => this.filterPipe.transform(this.frameworks, ["name"], framework != null ? framework.name : "")),
            takeUntil(this.destroy$),
        ).subscribe((frameworkList: ICategoryInformation[]) => {
            this.formData[this.formData.findIndex((data) => data.formControlName === ControlsFormControlNames.FRAMEWORK)].options = frameworkList;
        });
    }
}
