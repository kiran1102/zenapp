// Angular
import {
    Component,
    OnInit,
    OnDestroy,
    ViewChild,
} from "@angular/core";

// Interfaces
import { IControlsLibraryResponse } from "controls/shared/interfaces/controls-library.interface";

// Enums
import { ControlDetailsTabOptions } from "controls/shared/enums/controls.enum";
import { GridViewStorageNames } from "sharedModules/enums/grid-view-storage.enum";

// Services
import { StateService } from "@uirouter/core";
import { ControlsLibraryService } from "controls/shared/services/controls-library.service";
import { ControlsFilterService } from "../shared/components/controls-filter/controls-filter.service";
import { GridViewStorageService } from "sharedModules/services/helper/grid-view-storage.service";

// Interfaces
import { ITabsNav } from "interfaces/one-tabs-nav.interface";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

// Rxjs
import { takeUntil } from "rxjs/operators";
import { Subject } from "rxjs";

// Components
import { ControlDetailsComponent } from "./control-details/control-details.component";

@Component({
    selector: "control-page",
    templateUrl: "./control-page.component.html",
})
export class ControlPageComponent implements OnInit, OnDestroy {

    @ViewChild(ControlDetailsComponent) controlDetailsComponent: ControlDetailsComponent;
    controlId: string;
    control: IControlsLibraryResponse;
    isLoading = true;
    selectedTab = ControlDetailsTabOptions.Details;
    ControlDetailsTabOptions = ControlDetailsTabOptions;
    tabOptions: ITabsNav[] = [];
    editMode: boolean;
    storageKey = GridViewStorageNames.CONTROLS_LIBRARY_LIST;
    currentFilters = [];

    private destroy$ = new Subject();

    constructor(
        private stateService: StateService,
        private translatePipe: TranslatePipe,
        private controlsLibraryService: ControlsLibraryService,
        private controlsFilterService: ControlsFilterService,
    ) { }

    ngOnInit() {
        this.setTabOptions();
        this.controlId = this.stateService.params.controlId || null;
        this.editMode = this.stateService.params.editMode === "true" || false;
        this.controlsLibraryService.retrieveAllCategories();
        this.controlsLibraryService.retrieveControlsLibraryStatus();
        this.controlsLibraryService.retrieveControlById(this.controlId);
        this.controlsLibraryService.control$
            .pipe(takeUntil(this.destroy$))
            .subscribe((control: IControlsLibraryResponse) => {
                this.control = control;
                this.isLoading = false;
            });
        this.currentFilters = this.controlsFilterService.loadFilters(this.storageKey);
    }

    ngOnDestroy() {
        this.destroy$.next();
        this.destroy$.complete();
    }

    goToRoute(route: { path: string, params: any }) {
        this.stateService.go(route.path, route.params);
    }

    onTabClick(option: ITabsNav) {
        this.selectedTab = option.id as ControlDetailsTabOptions;
    }

    setTabOptions() {
        this.tabOptions.push({
            text: this.translatePipe.transform("Details"),
            id: ControlDetailsTabOptions.Details,
            otAutoId: "ControlDetailTabDetails",
        });
    }

    setEditMode(editMode: boolean) {
        this.editMode = editMode;
    }
}
