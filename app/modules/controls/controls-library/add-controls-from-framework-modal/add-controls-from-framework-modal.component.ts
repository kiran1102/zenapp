import { Component } from "@angular/core";
import { IOtModalContent } from "@onetrust/vitreus";

// Rxjs
import { Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";

// Services
import { ControlsLibraryService } from "controls/shared/services/controls-library.service";

// Interface
import {
    IControlLibraryQuery,
    IFrameworkCatalogueResponse,
} from "controls/shared/interfaces/controls-library.interface";

@Component({
    selector: "add-controls-from-framework-modal",
    templateUrl: "./add-controls-from-framework-modal.component.html",
})
export class AddControlsFromFrameworkModalComponent implements IOtModalContent {

    otModalCloseEvent: Subject<string>;
    frameworks: IFrameworkCatalogueResponse[] = [];
    frameworksData: IFrameworkCatalogueResponse[] = [];
    isLoading = false;
    selectedFrameworks: IFrameworkCatalogueResponse[] = [];
    searchFilter = "";
    isReady = true;
    otModalData: IControlLibraryQuery;

    private destroy$ = new Subject();

    constructor(
        private controlsLibraryService: ControlsLibraryService,
    ) { }

    ngOnInit(): void {
        this.isReady = false;
        this.controlsLibraryService.retrieveAllCatalogueFrameworks();
        this.controlsLibraryService.controlsCatalogueFrameworks$.pipe(
            takeUntil(this.destroy$),
        ).subscribe((vendorFrameworks: IFrameworkCatalogueResponse[]) => {
            if (vendorFrameworks) {
                this.frameworks = vendorFrameworks;
                this.frameworksData = vendorFrameworks;
                this.isReady = true;
            }
        });
    }

    ngOnDestroy() {
        this.destroy$.next();
        this.destroy$.complete();
    }

    onAdd(): void {
        this.isLoading = true;
        const selectedIds = this.selectedFrameworks.map((s) => s.id);
        this.controlsLibraryService.importControlsByFrameworkId(selectedIds).subscribe((res: any) => {
            this.controlsLibraryService.retrieveControlsLibrary(this.otModalData.query);
            this.controlsLibraryService.retrieveAllFrameworks();
            this.isLoading = false;
            this.closeModal();
        });
    }

    onSelectFrameworks(framework: IFrameworkCatalogueResponse): void {
        if (!framework.availableInControlsLibrary) {
            const idx = this.selectedFrameworks.findIndex((f) => f.id === framework.id);
            if (idx !== -1) {
                this.selectedFrameworks.splice(idx, 1);
            } else {
                this.selectedFrameworks.push(framework);
            }
        }
    }

    onSearchFilter(event): void {
        this.isReady = false;
        this.frameworks = this.frameworksData.filter((f) => f.name.toLowerCase().includes(event.toLowerCase()));
        this.isReady = true;
    }

    closeModal(): void {
        this.otModalCloseEvent.next();
    }
}
