// Core
import {
    Component,
    ViewChild,
} from "@angular/core";
import { StateService } from "@uirouter/core";

// Vitreus
import {
    OtModalService,
    ModalSize,
    ContextMenuType,
    ConfirmModalType,
} from "@onetrust/vitreus";

// Rxjs
import {
    Subject,
    combineLatest,
} from "rxjs";
import { takeUntil } from "rxjs/operators";

// Interfaces
import {
    IControlsLibraryTable,
    IControlsLibraryResponse,
} from "controls/shared/interfaces/controls-library.interface";
import { IHeaderOption } from "interfaces/assessment.interface";
import { ISamlSettingsResponse } from "interfaces/setting.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";

// Services
import { ControlsLibraryService } from "controls/shared/services/controls-library.service";
import { Permissions } from "sharedModules/services/helper/permissions.service";
import { ControlsFilterService } from "controls/shared/components/controls-filter/controls-filter.service";
import { GridViewStorageService } from "sharedModules/services/helper/grid-view-storage.service";
import { ControlsApiService } from "controls/shared/services/controls-api.service";
import { NotificationService } from "sharedModules/services/provider/notification.service";

// Enums
import { TableColumnTypes } from "enums/data-table.enum";
import { ControlsLibraryTableColumnNames } from "controls/shared/enums/controls-library.enum";
import { GridViewStorageNames } from "sharedModules/enums/grid-view-storage.enum";
import { BulkImportCards } from "modules/admin/enums/bulk-import.enums";

// Constants
import { DefaultPagination } from "../shared/constants/controls.constants";
import { ControlsExportErrorCodes } from "../shared/constants/controls-export-error-codes.constants";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";
import { IPageableMeta } from "interfaces/pagination.interface";

// Modals
import { AddControlModalComponent } from "controls/controls-library/add-control-modal/add-control-modal.component";
import { AddControlsFromFrameworkModalComponent } from "controls/controls-library/add-controls-from-framework-modal/add-controls-from-framework-modal.component";

// Components
import { ControlsLibraryTableComponent } from "controls/controls-library/controls-library-table/controls-library-table.component";
import { OTNotificationModalComponent } from "sharedModules/components/ot-notification-modal/ot-notification-modal.component";
import { ChangeControlsLibraryStatusModalComponent } from "./change-controls-library-status-modal/change-controls-library-status-modal.component";

// Enums
import { StatusCodes } from "enums/status-codes.enum";

@Component({
    selector: "controls-library",
    templateUrl: "./controls-library.component.html",
})
export class ControlsLibraryComponent {

    @ViewChild(ControlsLibraryTableComponent) controlsLibraryTableComponent: ControlsLibraryTableComponent;

    contextMenuOptions: IHeaderOption[] = [];
    contextMenuType = ContextMenuType;
    controlIds = [];
    currentFilters = [];
    defaultPagination = DefaultPagination;
    disableAddFilter = false;
    isFiltering = false;
    isPageloading = true;
    isSearching = false;
    paginationParams = { ...DefaultPagination };
    query = [];
    searchName = "";
    selectedCount = null;
    showHeader = false;
    storageKey = GridViewStorageNames.CONTROLS_LIBRARY_LIST;
    table: IControlsLibraryTable;
    tableMetaData: IPageableMeta;
    ControlsExportErrorCodes = ControlsExportErrorCodes;

    private destroy$ = new Subject();

    constructor(
        private controlsLibraryService: ControlsLibraryService,
        private translatePipe: TranslatePipe,
        private otModalService: OtModalService,
        private controlsFilterService: ControlsFilterService,
        private browserStorageListService: GridViewStorageService,
        private permissions: Permissions,
        private stateService: StateService,
        private controlsApiService: ControlsApiService,
        private notificationService: NotificationService,
    ) { }

    ngOnInit(): void {
        const storage = this.browserStorageListService.getBrowserStorageList(this.storageKey);
        this.searchName = storage.searchText;
        this.controlsLibraryService.controlsSearchName$.next(this.searchName);
        this.contextMenuOptions = this.initializeContextMenuItems();
        this.currentFilters = this.controlsFilterService.loadFilters(this.storageKey);
        this.query = this.controlsFilterService.buildQuery(this.currentFilters);
        this.controlsLibraryService.retrieveControlsLibrary(this.query);
        combineLatest(
            this.controlsLibraryService.controlsLibrary$,
            this.controlsLibraryService.paginationParams$,
        ).pipe(
            takeUntil(this.destroy$),
        ).subscribe(([controlsLibrary, paginationParams]) => {
            if (controlsLibrary) {
                this.table = this.configureTable(controlsLibrary);
                this.tableMetaData = this.controlsLibraryService.controlsLibraryMeta$.getValue();
                this.isPageloading = false;
            }
            if (paginationParams) {
                this.paginationParams = paginationParams;
            }
        });
    }

    ngOnDestroy(): void {
        this.controlsLibraryService.reset();
        this.destroy$.next();
        this.destroy$.complete();
    }

    displayControlsHeaderTab(controlIds: string[]) {
        this.controlIds = controlIds;
        this.selectedCount = controlIds.length;
        this.showHeader = this.selectedCount > 0;
    }

    closeControlsHeaderTab() {
        this.showHeader = false;
        this.controlsLibraryTableComponent.selectedControlIds.map((item) => {
            this.controlsLibraryTableComponent.selectedRowMap[item] = false;
        });
        this.controlsLibraryTableComponent.allRowSelected = false;
        this.controlsLibraryTableComponent.selectedControlIds = [];
    }

    onPaginationChange(event: number = 0): void {
        this.isSearching = true;
        this.isFiltering = false;
        this.showHeader = false;
        this.paginationParams.page = event;
        this.controlsLibraryService.paginationParams$.next(this.paginationParams);
        this.controlsLibraryService.retrieveControlsLibrary(this.query);
    }

    addNewControl() {
        this.otModalService
            .create(
                AddControlModalComponent,
                {
                    isComponent: true,
                },
                {
                    query: this.query,
                },
            );
    }

    toggleFilterDrawer() {
        this.isFiltering = !this.isFiltering;
    }

    filterApplied(filters) {
        this.isFiltering = false;
        this.showHeader = false;
        this.query = this.controlsFilterService.buildQuery(filters);
        this.currentFilters = filters;
        this.paginationParams.page = 0;
        this.controlsLibraryService.paginationParams$.next({ ...this.paginationParams });
        this.controlsLibraryService.retrieveControlsLibrary(this.query);
    }

    filterCanceled() {
        this.isFiltering = false;
    }

    onSearchName(searchName: string) {
        this.searchName = searchName;
        this.showHeader = false;
        this.controlsLibraryService.controlsSearchName$.next(this.searchName);
        this.browserStorageListService.saveSessionStorageSearch(this.storageKey, searchName);
        this.paginationParams.page = 0;
        this.controlsLibraryService.paginationParams$.next({ ...this.paginationParams });
        this.controlsLibraryService.retrieveControlsLibrary(this.query);
    }

    onSortChange({ sortKey, sortOrder }) {
        this.paginationParams.sort.sortKey = sortKey;
        this.paginationParams.sort.sortOrder = sortOrder;
        this.controlsLibraryService.paginationParams$.next(this.paginationParams);
        this.controlsLibraryService.retrieveControlsLibrary(this.query);
    }

    handleBulkChangeStatus() {
        this.otModalService
            .create(
                ChangeControlsLibraryStatusModalComponent,
                {
                    isComponent: true,
                },
                {
                    controlIds: this.controlIds,
                    status: "Active",
                    payload: this.query,
                    controlIdentifier: `${this.selectedCount} ${this.translatePipe.transform("Controls")}`,
                    bulkEdit: true,
                },
            ).pipe(
                takeUntil(this.destroy$),
            ).subscribe(() => {
                this.closeControlsHeaderTab();
            });
    }

    getFilterableColumns(column): boolean {
        return Boolean(column.canFilter);
    }

    contextMenuItemSelected(item: IHeaderOption) {
        item.action();
    }

    addFrameworks(): void {
        this.otModalService
            .create(
                AddControlsFromFrameworkModalComponent,
                {
                    isComponent: true,
                    size: ModalSize.MEDIUM,
                },
                {
                    query: this.query,
                },
            ).pipe(
                takeUntil(this.destroy$),
            );
    }

    private configureTable(records: IControlsLibraryResponse[]): IControlsLibraryTable {
        return {
            rows: records,
            columns: [
                {
                    name: "",
                    sortKey: null,
                    type: TableColumnTypes.Checkbox,
                    cellValueKey: null,
                    canFilter: false,
                },
                {
                    name: this.translatePipe.transform("ControlID"),
                    sortKey: ControlsLibraryTableColumnNames.CONTROLID,
                    type: TableColumnTypes.Link,
                    cellValueKey: ControlsLibraryTableColumnNames.CONTROLID,
                    sortable: true,
                },
                {
                    name: this.translatePipe.transform("Name"),
                    sortKey: ControlsLibraryTableColumnNames.NAME,
                    type: TableColumnTypes.Text,
                    cellValueKey: ControlsLibraryTableColumnNames.NAME,
                    sortable: true,
                },
                {
                    name: this.translatePipe.transform("Organization"),
                    sortKey: ControlsLibraryTableColumnNames.ORGANIZATION,
                    type: TableColumnTypes.Text,
                    cellValueKey: ControlsLibraryTableColumnNames.ORGANIZATION,
                },
                {
                    name: this.translatePipe.transform("Description"),
                    sortKey: ControlsLibraryTableColumnNames.DESCRIPTION,
                    type: TableColumnTypes.Text,
                    cellValueKey: ControlsLibraryTableColumnNames.DESCRIPTION,
                },
                {
                    name: this.translatePipe.transform("Framework"),
                    columnKey: "Framework",
                    sortKey: ControlsLibraryTableColumnNames.FRAMEWORK,
                    type: TableColumnTypes.Text,
                    cellValueKey: ControlsLibraryTableColumnNames.FRAMEWORK,
                    canFilter: true,
                    sortable: true,
                    filterKey: "frameworkId",
                },
                {
                    name: this.translatePipe.transform("Category"),
                    columnKey: "Category",
                    sortKey: ControlsLibraryTableColumnNames.CATEGORY,
                    type: TableColumnTypes.Text,
                    cellValueKey: ControlsLibraryTableColumnNames.CATEGORY,
                    canFilter: true,
                    filterKey: "categoryId",
                },
                {
                    name: this.translatePipe.transform("Status"),
                    columnKey: "Status",
                    sortKey: ControlsLibraryTableColumnNames.STATUS,
                    type: TableColumnTypes.Stage,
                    cellValueKey: ControlsLibraryTableColumnNames.STATUS,
                    canFilter: true,
                },
                {
                    name: "",
                    type: TableColumnTypes.Action,
                    cellValueKey: "",
                },
            ],
        };
    }

    private exportControls() {
        this.controlsApiService.exportControls(this.query, this.searchName, `${this.paginationParams.sort.sortKey},${this.paginationParams.sort.sortOrder}`).subscribe((res: IProtocolResponse<any>) => {
            if (res.result) {
                const taskNotificationModal = this.otModalService
                    .create(
                        OTNotificationModalComponent,
                        { isComponent: true },
                        {
                            confirmationText: this.translatePipe.transform("ReportIsGeneratedCheckTasksBar"),
                            modalTitle: "ReportDownload",
                        },
                    )
                    .subscribe(() => {
                        taskNotificationModal.unsubscribe();
                    });
            } else if (res.status === StatusCodes.BadRequest || res.status === StatusCodes.NotFound) {
                if (res.errorCode === ControlsExportErrorCodes.NO_CONTROLS_TO_EXPORT) {
                    this.notificationService.alertError(this.translatePipe.transform("Error"), this.translatePipe.transform("NoControlsToExport"));
                } else if (res.errorCode === ControlsExportErrorCodes.EXPORT_LIMIT_EXCEEDED) {
                    this.notificationService.alertError(this.translatePipe.transform("Error"), this.translatePipe.transform("MaxSizeLimitForExportExceeded"));
                } else if (res.data) {
                    this.notificationService.alertError(this.translatePipe.transform("Error"), res.data);
                } else {
                    this.notificationService.alertError(this.translatePipe.transform("Error"), this.translatePipe.transform("ExportFailed"));
                }
            }
        });
    }

    private initializeContextMenuItems(): IHeaderOption[] {
        const menuOptions = [];
        if (this.permissions.canShow("ControlsLibraryExportControls")) {
            menuOptions.push({
                id: "ControlsExport",
                text: this.translatePipe.transform("Export"),
                action: (): void => {
                    this.exportControls();
                },
            });
        }
        if (this.permissions.canShow("ControlsLibraryBulkImportControls")) {
            menuOptions.push({
                id: "BulkImportAddControlsToInventory",
                text: this.translatePipe.transform("BulkAddControls"),
                action: (): void => {
                    this.stateService.go("zen.app.pia.module.admin.import.templates", { activeCard: BulkImportCards.ADD_CONTROLS_TO_LIBRARY });
                },
            });
        }
        return menuOptions;
    }
}
