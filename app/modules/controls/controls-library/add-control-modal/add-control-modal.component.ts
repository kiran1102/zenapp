import {
    Component,
    OnInit,
    Inject,
    OnDestroy,
    ElementRef,
} from "@angular/core";

// Forms
import {
    FormGroup,
    FormBuilder,
    Validators,
    AbstractControl,
} from "@angular/forms";

// Vitreus
import {
    IOtModalContent,
} from "@onetrust/vitreus";

// Rxjs
import {
    Subject,
    combineLatest,
} from "rxjs";
// Rxjs
import {
    debounceTime,
    takeUntil,
    map,
} from "rxjs/operators";

// Services
import { NotificationService } from "sharedModules/services/provider/notification.service";
import { ControlsLibraryService } from "controls/shared/services/controls-library.service";
import { ControlsApiService } from "controls/shared/services/controls-api.service";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";
import { FilterPipe } from "modules/pipes/filter.pipe";

// Enums
import { StatusCodes } from "enums/status-codes.enum";
import { ControlsFormControlNames } from "controls/shared/enums/controls.enum";

// Interfaces
import {
    IStringMap,
    IKeyValue,
} from "interfaces/generic.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";
import { ICategoryInformation } from "controls/shared/interfaces/controls.interface";
import { IOTLookupSelection } from "sharedModules/components/ot-org-user/ot-org-user.interface";
import { IPageableMeta } from "interfaces/pagination.interface";

// Redux
import { getCurrentOrgId } from "oneRedux/reducers/org.reducer";
import { IStore } from "interfaces/redux.interface";
import { StoreToken } from "tokens/redux-store.token";

// Constants
import { ControlsExportErrorCodes } from "modules/controls/shared/constants/controls-export-error-codes.constants";

@Component({
    selector: "add-control-modal",
    templateUrl: "./add-control-modal.component.html",
})
export class AddControlModalComponent implements IOtModalContent, OnInit, OnDestroy {
    destroy$ = new Subject();
    otModalCloseEvent: Subject<boolean>;
    controlForm: FormGroup;
    isSaving = false;
    isSaveAndAddNewTriggered = false;
    orgGroupId = getCurrentOrgId(this.store.getState());
    defaultLookupModel = { id: null, name: null, nameKey: null };
    categoryOptions: ICategoryInformation[];
    filterredCategories: ICategoryInformation[];
    frameworkOptions: ICategoryInformation[];
    filterredFrameworks: ICategoryInformation[];
    inputChange$: Subject<ICategoryInformation> = new Subject();
    inputValue: ICategoryInformation;
    frameworkInputChange$: Subject<ICategoryInformation> = new Subject();
    frameworkInputValue: ICategoryInformation;
    tableMetaData: IPageableMeta;
    statusOptions: ICategoryInformation[];
    otModalData;

    get controlNumber() { return this.controlForm.get("controlNumber"); }
    get controlName() { return this.controlForm.get("controlName"); }
    get controlDescription() { return this.controlForm.get("controlDescription"); }
    get category() { return this.controlForm.get("category"); }
    get framework() { return this.controlForm.get("framework"); }
    get controlStatus() { return this.controlForm.get("controlStatus"); }

    constructor(
        @Inject(StoreToken) private store: IStore,
        private fb: FormBuilder,
        private translatePipe: TranslatePipe,
        private controlsApiService: ControlsApiService,
        private notificationService: NotificationService,
        private controlsLibraryService: ControlsLibraryService,
        private filterPipe: FilterPipe,
        private elementRef: ElementRef,
    ) { }

    ngOnInit() {
        this.configureInputListener();
        this.controlsLibraryService.retrieveAllCategories();
        this.controlsLibraryService.retrieveControlsLibraryStatus();
        combineLatest(
            this.controlsLibraryService.controlsAllCategories$,
            this.controlsLibraryService.controlsFrameworks$,
            this.controlsLibraryService.controlLibraryStatus$,
        ).pipe(
            takeUntil(this.destroy$),
        ).subscribe(([categoriesResponse, frameworkResponse, controlStatus]) => {
            if (categoriesResponse) {
                this.categoryOptions = categoriesResponse.map((item) => {
                    return { ...item, name: item.nameKey ? this.translatePipe.transform(item.nameKey) : item.name };
                });
                this.filterredCategories = [...this.categoryOptions];
                this.tableMetaData = this.controlsLibraryService.controlsLibraryMeta$.getValue();
            }
            if (frameworkResponse) {
                this.frameworkOptions = frameworkResponse.map((item) => {
                    return { id: item.frameworkId, name: item.frameworkNameKey ? this.translatePipe.transform(item.frameworkNameKey) : item.frameworkName, nameKey: item.frameworkNameKey };
                });
                this.filterredFrameworks = [...this.frameworkOptions];

            }
            if (controlStatus) {
                this.statusOptions = controlStatus.map((status) => {
                    if (status) return { nameKey: status, name: this.translatePipe.transform(status), id: null };
                });
            }
            this.generateControlFormGroup();
        });
    }

    ngOnDestroy() {
        this.inputChange$.unsubscribe();
        this.destroy$.next(true);
        this.destroy$.complete();
    }

    closeModal(success: boolean) {
        this.generateControlFormGroup();
        this.otModalCloseEvent.next(success);
    }

    // Lookup input handlers
    updateCategory(category: IOTLookupSelection<ICategoryInformation>) {
        if (!category.change) return;
        this.category.setValue(category.currentValue);
        this.category.markAsDirty();
        this.inputChange$.next(null);
        this.inputValue = this.defaultLookupModel;
    }

    updateFramework(framework: IOTLookupSelection<ICategoryInformation>) {
        if (!framework.change) return;
        this.framework.setValue(framework.currentValue);
        this.framework.markAsDirty();
        this.frameworkInputChange$.next(null);
        this.frameworkInputValue = this.defaultLookupModel;
    }
    onCategoryInputChange(category: IKeyValue<string>) {
        this.category.markAsTouched();
        if (category.key === "Enter") {
            return;
        }
        if (category.key === "Backspace" && !category.value) {
            this.inputValue = { ...this.defaultLookupModel };
            this.inputChange$.next(this.inputValue);
            return;
        }
        if (category.value) {
            this.inputValue = { ...this.defaultLookupModel, name: category.value };
            this.inputChange$.next(this.inputValue);
        }
    }

    onFrameworkInputChange(framework: IKeyValue<string>) {
        this.category.markAsTouched();
        if (framework.key === "Enter") {
            return;
        }
        if (framework.key === "Backspace" && !framework.value) {
            this.frameworkInputValue = { ...this.defaultLookupModel };
            this.frameworkInputChange$.next(this.inputValue);
            return;
        }
        if (framework.value) {
            this.frameworkInputValue = { ...this.defaultLookupModel, name: framework.value };
            this.frameworkInputChange$.next(this.inputValue);
        }
    }

    onStatusChange(event: ICategoryInformation) {
        this.controlStatus.setValue(event);
    }

    addNewCategory() {
        if (this.inputValue) {
            this.category.setValue(this.inputValue);
            if (this.inputValue.name.length > 100) {
                this.category.setErrors({
                    maxlength: { requiredLength: 100, actualLength: this.inputValue.name.length },
                });
            }
            this.inputValue = this.defaultLookupModel;
        }
    }

    addNewFramework() {
        if (this.frameworkInputValue) {
            this.framework.setValue(this.frameworkInputValue);
            if (this.frameworkInputValue.name.length > 100) {
                this.framework.setErrors({
                    maxlength: { requiredLength: 100, actualLength: this.frameworkInputValue.name.length },
                });
            }
            this.frameworkInputValue = this.defaultLookupModel;
        }
    }

    onSave(saveAndNew = false) {
        if (this.controlForm.invalid || this.isSaveAndAddNewTriggered || this.isSaving) return;
        this.isSaveAndAddNewTriggered = saveAndNew;
        this.isSaving = !saveAndNew;
        const values = this.controlForm.value;
        const addControlPayload: IStringMap<string> = {
            identifier: values.controlNumber ? values.controlNumber.trim() : null,
            name: values.controlName ? values.controlName.trim() : null,
            description: values.controlDescription ? values.controlDescription.trim() : null,
            recommendation: null,
            orgGroupId: this.orgGroupId,
            categoryId: values.category ? values.category.id : null,
            categoryName: values.category ? values.category.name.trim() : null,
            categoryNameKey: values.category ? values.category.nameKey : null,
            frameworkId: values.framework ? values.framework.id : null,
            frameworkName: values.framework ? values.framework.name.trim() : null,
            frameworkNameKey: values.framework ? values.framework.nameKey : null,
            status: values.controlStatus ? values.controlStatus.nameKey : null,
        };
        this.controlsApiService.addControl(addControlPayload).subscribe((response: IProtocolResponse<ICategoryInformation[]>) => {
            if (response.status === StatusCodes.BadRequest || response.status === StatusCodes.NotFound) {
                let errorMsg = `${this.translatePipe.transform("Vendor.Error.AddControl")}`;
                if (response.errorCode === ControlsExportErrorCodes.DUPLICATE_CONTROL) {
                    errorMsg = `${this.translatePipe.transform("ControlAlreadyExists")}`;
                }
                this.notificationService.alertError(this.translatePipe.transform("Error"), errorMsg);
                this.controlsLibraryService.controlsLoading$.next(false);
            } else if (response.result) {
                if (!saveAndNew) {
                    this.closeModal(true);
                } else {
                    this.generateControlFormGroup();
                    this.elementRef.nativeElement.querySelector("form").scrollIntoView(true);
                }
                this.controlsLibraryService.controlsLoading$.next(true);
                this.controlsLibraryService.retrieveControlsLibrary(this.otModalData.query);
            }
            this.isSaving = false;
            this.isSaveAndAddNewTriggered = false;
        });
    }

    resetInputValue(formControlName: string) {
        if (formControlName === ControlsFormControlNames.CATEGORY && !this.category.value) {
            this.inputChange$.next(this.defaultLookupModel);
        }
        if (formControlName === ControlsFormControlNames.CATEGORY && !this.framework.value) {
            this.frameworkInputChange$.next(this.defaultLookupModel);
        }
    }

    private generateControlFormGroup() {
        this.controlForm = this.fb.group({
            controlNumber: [null, [Validators.required, Validators.minLength(1), Validators.maxLength(50), this.validateControlNumber]],
            controlStatus: [this.statusOptions ? this.statusOptions[0] : null, [Validators.required]],
            controlName: [null, [Validators.required, Validators.minLength(1), Validators.maxLength(300), this.validateControlName]],
            controlDescription: [null, [Validators.maxLength(500)]],
            framework: [null, Validators.maxLength(100)],
            category: [null, Validators.maxLength(100)],
        });
    }

    private validateControlNumber(control: AbstractControl) {
        if ((control.value || "").trim().length === 0) {
            return { controlNumber: { value: "" } };
        }
    }

    private validateControlName(control: AbstractControl) {
        if ((control.value || "").trim().length === 0) {
            return { controlName: { value: "" } };
        }
    }

    private configureInputListener() {
        this.inputChange$.pipe(
            takeUntil(this.destroy$),
            debounceTime(300),
            map((category: ICategoryInformation) => this.filterPipe.transform(this.categoryOptions, ["name"], category != null ? category.name : "")),
        ).subscribe((categoryList: ICategoryInformation[]) => {
            this.filterredCategories = categoryList;
        });
    }

}
