
// Core
import {
    Component,
    OnInit,
    OnDestroy,
} from "@angular/core";
import {
    FormGroup,
    FormBuilder,
    Validators,
} from "@angular/forms";

// Services
import { ControlsApiService } from "modules/controls/shared/services/controls-api.service";
import { ControlsLibraryService } from "modules/controls/shared/services/controls-library.service";

// Rxjs
import { Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";

// Interface
import { ILabelValue } from "interfaces/generic.interface";
import { IOtModalContent } from "@onetrust/vitreus";
import { ISamlSettingsResponse } from "interfaces/setting.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";

// Enums & Constants
import { ControlsStatusDescTranslationKeys } from "modules/controls/shared/constants/controls.constants";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

@Component({
    selector: "change-controls-library-status-modal",
    templateUrl: "./change-controls-library-status-modal.component.html",
})
export class ChangeControlsLibraryStatusModalComponent implements OnInit, OnDestroy, IOtModalContent {

    otModalData: { controlIds: string[], status: string, controlIdentifier: string, payload: any, bulkEdit?: boolean };
    otModalCloseEvent: Subject<void>;
    isReady = false;
    isSaving = false;
    hasEdits = false;
    statusOptions: Array<ILabelValue<string>>;
    controlIdentifier: string;
    controlsStatusDescTranslationKeys = ControlsStatusDescTranslationKeys;
    changeControlStatusForm: FormGroup;

    private destroy$ = new Subject();

    get status() { return this.changeControlStatusForm.get("status"); }

    constructor(
        private translatePipe: TranslatePipe,
        private controlsLibraryService: ControlsLibraryService,
        private controlsApiService: ControlsApiService,
        private formBuilder: FormBuilder,
    ) { }

    ngOnInit() {
        this.controlsLibraryService.retrieveControlsLibraryStatus();
        this.controlsLibraryService.controlLibraryStatus$.pipe(
            takeUntil(this.destroy$),
        ).subscribe((controlStatus: string[]) => {
            if (controlStatus) {
                this.statusOptions = controlStatus.map((status) => {
                    if (status) return { value: status, label: this.translatePipe.transform(status) };
                });
                this.isReady = true;
            }
        });
        this.changeControlStatusForm = this.formBuilder.group({
            status: [this.otModalData.status ? { value: this.otModalData.status, label: this.translatePipe.transform(this.otModalData.status) } : null, Validators.required],
        });
        this.onFormChanges();
        this.controlIdentifier = this.otModalData.controlIdentifier;
        if (this.otModalData.bulkEdit) this.hasEdits = true;
    }

    ngOnDestroy(): void {
        this.destroy$.next();
        this.destroy$.complete();
    }

    onFormChanges() {
        this.changeControlStatusForm.valueChanges.pipe(
            takeUntil(this.destroy$),
        ).subscribe(() => {
            this.hasEdits = true;
        });
    }

    selectStatus(event: ILabelValue<string>) {
        this.status.setValue(event);
    }

    closeModal() {
        this.otModalCloseEvent.next();
        this.otModalCloseEvent.complete();
    }

    updateStatus() {
        if (this.changeControlStatusForm.invalid || this.isSaving) return;
        this.isSaving = true;
        this.controlsApiService.changeControlStatus(this.otModalData.controlIds, this.changeControlStatusForm.value.status.value)
            .subscribe((response: IProtocolResponse<ISamlSettingsResponse>) => {
                if (response.result) {
                    this.closeModal();
                    this.controlsLibraryService.controlsLoading$.next(true);
                    this.controlsLibraryService.retrieveControlsLibrary(this.otModalData.payload);
                }
                this.isSaving = false;
            });
    }

}
