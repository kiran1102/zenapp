// Core
import {
    Component,
    Input,
    Output,
    OnInit,
    OnDestroy,
    EventEmitter,
} from "@angular/core";

// Vitreus
import {
    ContextMenuType,
    OtModalService,
} from "@onetrust/vitreus";

// Interfaces
import { IDropdownOption } from "interfaces/one-dropdown.interface";
import { IDataTableColumn } from "interfaces/tables.interface";
import {
    IControlsLibraryTable,
    IControlsLibraryResponse,
} from "controls/shared/interfaces/controls-library.interface";

// Services
import { Permissions } from "modules/shared/services/helper/permissions.service";
import { StateService } from "@uirouter/core";
import { ControlsLibraryService } from "controls/shared/services/controls-library.service";

// Rxjs
import { Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";

// Enums
import { TableColumnTypes } from "enums/data-table.enum";
import { DefaultPagination } from "modules/vendor/shared/constants/vendor-documents.constants";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

// Constants
import { VPControlsStatusColorMap } from "modules/vendor/shared/constants/vendor-controls.constants";

// Components
import { ChangeControlsLibraryStatusModalComponent } from "controls/controls-library/change-controls-library-status-modal/change-controls-library-status-modal.component";

@Component({
    selector: "controls-library-table",
    templateUrl: "./controls-library-table.component.html",
})
export class ControlsLibraryTableComponent implements OnInit, OnDestroy {

    @Input() table: IControlsLibraryTable;
    @Input() currentQuery;

    @Output() controlIds = new EventEmitter<string[]>();
    @Output() handleTableActions = new EventEmitter<{ sortKey: string, sortOrder: string }>();

    menuClass: string;
    resizableCol = false;
    responsiveTable = true;
    rowBordered = true;
    rowNoHover = false;
    rowStriped = false;
    selectedRowMap = {};
    sortable = true;
    tableColumnTypes = TableColumnTypes;
    truncateCellContent = true;
    wrapCellContent = false;
    menuOptions: IDropdownOption[] = [];
    menuType = ContextMenuType.Small;
    emptyData = "- - - -";
    allRowSelected = false;
    selectedControlIds = [];
    paginationParams = { ...DefaultPagination };
    vPControlsStatusColorMap = VPControlsStatusColorMap;
    private destroy$ = new Subject();

    constructor(
        private stateService: StateService,
        private translatePipe: TranslatePipe,
        private permissions: Permissions,
        private otModalService: OtModalService,
        private controlsLibraryService: ControlsLibraryService) { }

    isRowDisabled = (row: IControlsLibraryResponse): boolean => row.viewOnly;

    ngOnInit() {
        this.generateMenuOptions();
        this.controlsLibraryService.paginationParams$.pipe(
            takeUntil(this.destroy$),
        ).subscribe((paginationParams) => {
            this.paginationParams = paginationParams;
        });
    }

    ngOnDestroy() {
        this.destroy$.next();
        this.destroy$.complete();
    }

    hasEditableRows() {
        let count = 0;
        this.table.rows.forEach((item: IControlsLibraryResponse) => { if (item.viewOnly) count++; });
        return this.table.rows.length === count;
    }

    columnTrackBy(index: number, column: IDataTableColumn): string {
        return column.id;
    }

    handleSortChange({ sortKey, sortOrder }) {
        this.handleTableActions.emit({ sortKey, sortOrder });
    }

    goToRecord(row: IControlsLibraryResponse, editMode = false) {
        this.stateService.go("zen.app.pia.module.risks.views.controls.control", { controlId: row.id, editMode });
    }

    handleTableSelectClick(actionType: string, rowId: string) {
        switch (actionType) {
            case "header":
                this.handleAllRowSelection();
                break;
            case "row":
                this.handleEachRowSelection(rowId);
                break;
        }
    }

    handleAllRowSelection() {
        this.allRowSelected = !this.allRowSelected;
        if (this.allRowSelected) {
            this.table.rows.map((item) => {
                if (!item.viewOnly) {
                    this.selectedRowMap[item.id] = true;
                    if (this.selectedControlIds.indexOf(item.id) === -1) {
                        this.selectedControlIds.push(item.id);
                    }
                }
            });
            this.controlIds.emit(this.selectedControlIds);
        } else {
            this.table.rows.map((item) => {
                this.selectedRowMap[item.id] = false;
            });
            this.selectedControlIds = [];
            this.controlIds.emit(this.selectedControlIds);
        }
    }

    handleEachRowSelection(rowId: string) {
        let activeRow = 0;
        this.selectedRowMap[rowId] = !this.selectedRowMap[rowId];
        this.table.rows.forEach((item: IControlsLibraryResponse) => { if (!item.viewOnly) activeRow++; });
        this.allRowSelected = Object.values(this.selectedRowMap).filter(Boolean).length === activeRow;
        if (this.selectedRowMap[rowId]) {
            this.selectedControlIds.push(rowId);
        } else if (this.selectedControlIds.indexOf(rowId) > -1) {
            this.selectedControlIds.splice(this.selectedControlIds.indexOf(rowId), 1);
        }
        this.controlIds.emit(this.selectedControlIds);
    }

    private generateMenuOptions() {
        if (this.permissions.canShow("ControlsLibraryEditControl")) {
            this.menuOptions.push({
                text: this.translatePipe.transform("Edit"),
                action: (row: IControlsLibraryResponse) => {
                    this.goToRecord(row, true);
                },
            });
        }
        if (this.permissions.canShow("ControlsLibraryRemoveControl")) {
            this.menuOptions.push({
                text: this.translatePipe.transform("ChangeStatus"),
                action: (row: IControlsLibraryResponse) => {
                    this.openChangeControlStatusModal(row);
                },
            });
        }
    }

    private openChangeControlStatusModal(control: IControlsLibraryResponse) {
        this.otModalService
            .create(
                ChangeControlsLibraryStatusModalComponent,
                {
                    isComponent: true,
                },
                {
                    controlIds: [control.id],
                    status: control.status,
                    payload: this.currentQuery,
                    controlIdentifier: control.identifier,
                },
            ).pipe(
                takeUntil(this.destroy$),
            ).subscribe();
    }
}
