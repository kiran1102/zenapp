import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from "@angular/core";

// Modules
import { SharedModule } from "sharedModules/shared.module";
import { UpgradedLegacyModule } from "sharedModules/upgrade/upgraded-legacy.module";
import { CommonModule } from "@angular/common";
import { HttpClientModule } from "@angular/common/http";
import { FormsModule } from "@angular/forms";
import { ReactiveFormsModule } from "@angular/forms";
import { VitreusModule } from "@onetrust/vitreus";
import { DndListModule } from "ngx-drag-and-drop-lists";
import { OTPipesModule, PIPES } from "modules/pipes/pipes.module";
import { VirtualScrollModule } from "od-virtualscroll";
import { OneVideoModule } from "modules/video/video.module";
import { ControlsSharedModule } from "controls/shared/controls-shared.module";

// Components
import { ControlsLibraryComponent } from "controls/controls-library/controls-library.component";
import { ControlsLibraryTableComponent } from "controls/controls-library/controls-library-table/controls-library-table.component";
import { AddControlModalComponent } from "controls/controls-library/add-control-modal/add-control-modal.component";
import { AddControlsFromFrameworkModalComponent } from "controls/controls-library/add-controls-from-framework-modal/add-controls-from-framework-modal.component";
import { ChangeControlsLibraryStatusModalComponent } from "./change-controls-library-status-modal/change-controls-library-status-modal.component";

@NgModule({
    imports: [
        SharedModule,
        UpgradedLegacyModule,
        CommonModule,
        HttpClientModule,
        FormsModule,
        ReactiveFormsModule,
        VitreusModule,
        DndListModule,
        OTPipesModule,
        VirtualScrollModule,
        OneVideoModule,
        ControlsSharedModule,
    ],
    declarations: [
        ControlsLibraryComponent,
        ControlsLibraryTableComponent,
        AddControlModalComponent,
        AddControlsFromFrameworkModalComponent,
        ChangeControlsLibraryStatusModalComponent,
    ],
    exports: [
        ControlsLibraryComponent,
        ControlsLibraryTableComponent,
        AddControlModalComponent,
        AddControlsFromFrameworkModalComponent,
    ],
    entryComponents: [
        ControlsLibraryComponent,
        AddControlModalComponent,
        AddControlsFromFrameworkModalComponent,
        ChangeControlsLibraryStatusModalComponent,
    ],
    providers: [
        PIPES, // TODO: Remove from providers when ot-common is implemented. Only necessary if you are using pipe in TS file, else just module is fine
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class ControlsLibraryModule {}
