// Angular
import { NgModule } from "@angular/core";
import { HTTP_INTERCEPTORS } from "@angular/common/http";

// Interceptors
import { MockPermissionsInterceptor } from "./_helpers/mock-permissions.interceptor";

@NgModule({
    providers: [{ provide: HTTP_INTERCEPTORS, useClass: MockPermissionsInterceptor, multi: true }],
})
export class MockModule {}
