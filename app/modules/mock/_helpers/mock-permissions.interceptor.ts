// Angular
import { Injectable } from "@angular/core";
import {
    HttpEvent,
    HttpHandler,
    HttpInterceptor,
    HttpRequest,
    HttpResponse,
} from "@angular/common/http";

// rxjs
import { Observable } from "rxjs";
import { map } from "rxjs/operators";

// Mocks
import { mockPermissions } from "../_data/permissions.mock";

@Injectable()
export class MockPermissionsInterceptor implements HttpInterceptor {

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        if (req.url.endsWith("/api/v1/private/permission/get")) {
            return next.handle(req).pipe(
                map((response: HttpResponse<any>) => {
                    if (response instanceof HttpResponse) {
                        response = response.clone({
                            body: {
                                ...response.body,
                                ...mockPermissions,
                            },
                        });
                    }
                    return response;
                }),
            );
        }
        return next.handle(req);
    }

}
