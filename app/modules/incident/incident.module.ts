import { NgModule } from "@angular/core";

// Modules
import { IncidentDetailModule } from "incidentModule/incident-detail/incident-detail.module";
import { IncidentRegisterModule } from "incidentModule/incident-register/incident-register.module";
import { IncidentDataBreachPediaModule } from "incidentModule/incident-databreachpedia/incident-databreachpedia.module";
import { IncidentWorkflowModule } from "incidentModule/incident-workflow/incident-workflow.module";
import { IncidentTypesModule } from "incidentModule/incident-types/incident-types.module";

@NgModule({
    imports: [
        IncidentDetailModule,
        IncidentRegisterModule,
        IncidentDataBreachPediaModule,
        IncidentWorkflowModule,
        IncidentTypesModule,
    ],
})
export class IncidentModule { }
