// Services
import { Permissions } from "modules/shared/services/helper/permissions.service";
import { GlobalSidebarService } from "modules/global-sidebar/services/one-global-sidebar.service";
import { Wootric } from "sharedModules/services/provider/wootric.service";
import { StateService } from "@uirouter/core";
import { TenantTranslationService } from "sharedServices/tenant-translation.service";

// Interfaces
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import { INavMenuItem } from "modules/global-sidebar/interfaces/nav-menu-item.interface";

// Constants
import { WootricModuleMap } from "modules/shared/constants/wootric.constants";
import { TranslationModules } from "constants/translation.constant";

class IncidentWrapperController implements ng.IComponentController {

    static $inject: string[] = [
        "$rootScope",
        "Permissions",
        "GlobalSidebarService",
        "TenantTranslationService",
        "Wootric",
    ];

    constructor(
        private $rootScope: IExtendedRootScopeService,
        private permissions: Permissions,
        private globalSidebarService: GlobalSidebarService,
        private tenantTranslationService: TenantTranslationService,
        private wootric: Wootric,
    ) {}

    $onInit() {
        this.globalSidebarService.set(this.getMenuRoutes(), this.$rootScope.t("IncidentResponse"));
        this.getIncidentTranslations();
        this.wootric.run(WootricModuleMap.IncidentResponse);
    }

    private getIncidentTranslations = (): ng.IPromise<boolean> => {
        return this.tenantTranslationService.addModuleTranslations(TranslationModules.Incident);
    }

    private getMenuRoutes(): INavMenuItem[] {
        const menuGroup: INavMenuItem[] = [];
        menuGroup.push({
            id: "IncidentRegisterSidebarItem",
            title: this.$rootScope.t("IncidentRegister"),
            route: "zen.app.pia.module.incident.views.incident-register",
            activeRouteFn: (stateService: StateService) => stateService.includes("zen.app.pia.module.incident.views.incident-register")
                || stateService.includes("zen.app.pia.module.incident.views.incident-detail"),
            icon: "ot ot-exclamation-circle",
        });

        if (this.permissions.canShow("IncidentBreachManagement")) {
            menuGroup.push({
                id: "IncidentDataBreachSidebarItem",
                title: this.$rootScope.t("DataBreachPedia"),
                route: "zen.app.pia.module.incident.views.incident-databreachpedia",
                activeRouteFn: (stateService: StateService) => stateService.includes("zen.app.pia.module.incident.views.incident-databreachpedia")
                || stateService.includes("zen.app.pia.module.incident.views.incident-databreachpedia-jurisdiction-view")
                || stateService.includes("zen.app.pia.module.incident.views.incident-databreachpedia-law-view"),
                icon: "ot ot-databreachpedia",
            });
        }

        const setupChildren: INavMenuItem[] = [];
        if (this.permissions.canShow("IncidentManageSettings")) {
            setupChildren.push({
                id: "IncidentWorkflowSidebarItem",
                title: this.$rootScope.t("IncidentWorkflow"),
                route: "zen.app.pia.module.incident.views.incident-workflow",
                activeRoute: "zen.app.pia.module.incident.views.incident-workflow",
            });
            setupChildren.push({
                id: "IncidentTypesSidebarItem",
                title: this.$rootScope.t("IncidentTypes"),
                route: "zen.app.pia.module.incident.views.incident-types",
                activeRoute: "zen.app.pia.module.incident.views.incident-types",
            });
        }

        if (setupChildren.length) {
            menuGroup.push({
                id: "IncidentSetupSidebarItem",
                title: this.$rootScope.t("Setup"),
                children: setupChildren,
                open: true,
                icon: "ot ot-wrench",
            });
        }
        return menuGroup;
    }
}

export const IncidentWrapperComponent: ng.IComponentOptions = {
    controller: IncidentWrapperController,
    template: `
        <div class="ib flex-full-height height-100 background-grey text-color-default">
            <div class="ib-content row-nowrap width-100-percent height-100 overflow-hidden">
                <section ui-view="incident_body" class="ib-body width-100-percent padding-all-0 background-grey stretch-horizontal"></section>
            </div>
        </div>
    `,
};
