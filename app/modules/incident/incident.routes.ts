import { IGeneralData } from "app/scripts/PIA/pia.routes";
import { Permissions } from "modules/shared/services/helper/permissions.service";

export function routes($stateProvider: ng.ui.IStateProvider) {
    $stateProvider
        .state("zen.app.pia.module.incident.views", {
            abstract: true,
            url: "",
            views: {
                incident_body: {
                    template: "<ui-view></ui-view>",
                },
            },
        })
        .state("zen.app.pia.module.incident", {
            abstract: true,
            url: "incident",
            resolve: {
                IncidentPermission: ["GeneralData", "Permissions",
                    (GeneralData: IGeneralData, permissions: Permissions): boolean => {
                        if (permissions.canShow("IncidentBreachManagement")) {
                            return true;
                        } else {
                            permissions.goToFallback();
                        }
                        return false;
                    },
                ],
            },
            views: {
                module: {
                    template: "<incident-wrapper></incident-wrapper>",
                },
            },
        })
        .state("zen.app.pia.module.incident.views.incident-register", {
            url: "/incident-register",
            params: { time: null },
            template: "<downgrade-incident-register></downgrade-incident-register>",
            resolve: {
                IncidentListPermissions: ["IncidentPermission", "Permissions",
                    (IncidentPermission: boolean, permissions: Permissions): boolean => {
                        return IncidentPermission;
                    },
                ],
            },
        })
        .state("zen.app.pia.module.incident.views.incident-databreachpedia", {
            url: "/incident-databreachpedia/list",
            params: { time: null },
            template: "<downgrade-incident-databreachpedia></downgrade-incident-databreachpedia>",
            resolve: {
                IncidentListPermissions: ["IncidentPermission", "Permissions",
                    (IncidentPermission: boolean, permissions: Permissions): boolean => {
                        if (permissions.canShow("IncidentBreachManagement")) {
                            return IncidentPermission;
                        } else {
                            permissions.goToFallback();
                            return !IncidentPermission;
                        }
                    },
                ],
            },
        })
        .state("zen.app.pia.module.incident.views.incident-databreachpedia-jurisdiction-view", {
            url: "/incident-databreachpedia/view/jurisdiction/:jurisdictionId",
            template: "<downgrade-incident-databreachpedia-view></downgrade-incident-databreachpedia-view>",
            resolve: {
                IncidentListPermissions: ["IncidentPermission", "Permissions",
                    (IncidentPermission: boolean, permissions: Permissions): boolean => {
                        if (permissions.canShow("IncidentBreachManagement")) {
                            return IncidentPermission;
                        } else {
                            permissions.goToFallback();
                            return !IncidentPermission;
                        }
                    },
                ],
            },
        })
        .state("zen.app.pia.module.incident.views.incident-databreachpedia-law-view", {
            url: "/incident-databreachpedia/view/law/:lawId",
            template: "<downgrade-incident-databreachpedia-view></downgrade-incident-databreachpedia-view>",
            resolve: {
                IncidentListPermissions: ["IncidentPermission", "Permissions",
                    (IncidentPermission: boolean, permissions: Permissions): boolean => {
                        if (permissions.canShow("IncidentBreachManagement")) {
                            return IncidentPermission;
                        } else {
                            permissions.goToFallback();
                            return !IncidentPermission;
                        }
                    },
                ],
            },
        })
        .state("zen.app.pia.module.incident.views.incident-detail", {
            url: "/detail/:id",
            params: { time: null },
            template: "<downgrade-incident-detail></downgrade-incident-detail>",
            resolve: {
                IncidentListPermissions: ["IncidentPermission",
                    (IncidentPermission: boolean): boolean => {
                        return IncidentPermission;
                    },
                ],
            },
        })
        .state("zen.app.pia.module.incident.views.incident-launch-assessment", {
            url: "/launch/:inventoryTypeId",
            params: {
                time: null,
                detailsId: null,
            },
            template: "<downgrade-aa-bulk-launch></downgrade-aa-bulk-launch>",
        })
        .state("zen.app.pia.module.incident.views.incident-workflow", {
            url: "/incident-workflow",
            template: "<downgrade-incident-workflow></downgrade-incident-workflow>",
            resolve: {
                IncidentListPermissions: ["IncidentPermission", "Permissions",
                    (IncidentPermission: boolean, permissions: Permissions): boolean => {
                        if (permissions.canShow("IncidentManageSettings")) {
                            return IncidentPermission;
                        } else {
                            permissions.goToFallback();
                            return !IncidentPermission;
                        }
                    },
                ],
            },
        })
        .state("zen.app.pia.module.incident.views.incident-types", {
            url: "/incident-types",
            template: "<downgrade-incident-types></downgrade-incident-types>",
            resolve: {
                IncidentListPermissions: ["IncidentPermission", "Permissions",
                    (IncidentPermission: boolean, permissions: Permissions): boolean => {
                        if (permissions.canShow("IncidentManageSettings")) {
                            return IncidentPermission;
                        } else {
                            permissions.goToFallback();
                            return !IncidentPermission;
                        }
                    },
                ],
            },
        })
    ;
}

routes.$inject = ["$stateProvider"];
