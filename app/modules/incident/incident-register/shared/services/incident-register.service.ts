import { Injectable } from "@angular/core";

// Rxjs
import {
    Subject,
    BehaviorSubject,
} from "rxjs";

// Services
import { IncidentApiService } from "incidentModule/shared/services/incident-api.service";

// Interfaces
import {
    IPaginationFilter,
    IPageableMeta,
} from "interfaces/pagination.interface";
import {
    IIncidentRegister,
    IIncidentRegisterPageable,
} from "incidentModule/shared/interfaces/incident-register.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";

@Injectable()
export class IncidentRegisterService {
    public incidentRegister$ = new Subject<IIncidentRegister[]> ();
    public incidentRegisterFilterSettings$ = new BehaviorSubject<any> ({
        searchText: null,
        filters: [],
        paginationParams: null,
    });
    public incidentRegisterMetaData$ = new BehaviorSubject<IPageableMeta> (
        {
            first: true,
            last: true,
            number: 0,
            numberOfElements: 0,
            size: 0,
            totalElements: 0,
            totalPages: 0,
        },
    );

    constructor(
        private incidentApi: IncidentApiService,
    ) { }

    public retrieveIncidentRegister(searchText = "", filters: IPaginationFilter[] = [], paginationParams = { page: 0, size: 20, sort: "" }) {
        this.incidentRegisterFilterSettings$.next({
            searchText, filters, paginationParams,
        });
        this.incidentApi.fetchIncidentRegister(searchText, filters, paginationParams).then((res: IProtocolResponse<IIncidentRegisterPageable>): void => {
            if (res.result) {
                this.incidentRegister$.next(res.data.content);
                this.incidentRegisterMetaData$.next(
                    {
                        first: res.data.first,
                        last: res.data.last,
                        number: res.data.number,
                        numberOfElements: res.data.numberOfElements,
                        size: res.data.size,
                        sort: [...res.data.sort],
                        totalElements: res.data.totalElements,
                        totalPages: res.data.totalPages,
                    },
                    );
                }
        });
    }

    public removeIncidentRegister(incidentId, tableMetaData: IPageableMeta) {
        this.incidentApi.removeIncidentRegister(incidentId).then((res: IProtocolResponse<string>): void => {
            const config = this.incidentRegisterFilterSettings$.getValue();
            if (tableMetaData.last && tableMetaData.numberOfElements <= 1) {
                --config.paginationParams.page;
            }
            this.retrieveIncidentRegister(config.searchText, config.filters, config.paginationParams);
        });
    }
}
