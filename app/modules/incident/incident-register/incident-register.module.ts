import { NgModule } from "@angular/core";

// Modules
import { SharedModule } from "sharedModules/shared.module";
import { UpgradedLegacyModule } from "sharedModules/upgrade/upgraded-legacy.module";
import { CommonModule } from "@angular/common";
import { HttpClientModule } from "@angular/common/http";
import { FormsModule } from "@angular/forms";
import { ReactiveFormsModule } from "@angular/forms";
import { VitreusModule } from "@onetrust/vitreus";
import { DndListModule } from "ngx-drag-and-drop-lists";
import { OTPipesModule, PIPES } from "modules/pipes/pipes.module";
import { VirtualScrollModule } from "od-virtualscroll";
import { OneVideoModule } from "modules/video/video.module";
import { IncidentSharedModule } from "incidentModule/shared/incident-shared.module";

// Components
import { IncidentRegisterTableComponent } from "incidentModule/incident-register/incident-register-table/incident-register-table.component";
import { IncidentRegisterComponent } from "incidentModule/incident-register/incident-register.component";

// Services
import { IncidentRegisterService } from "incidentModule/incident-register/shared/services/incident-register.service";

@NgModule({
    imports: [
        SharedModule,
        UpgradedLegacyModule,
        CommonModule,
        HttpClientModule,
        FormsModule,
        ReactiveFormsModule,
        VitreusModule,
        DndListModule,
        OTPipesModule,
        VirtualScrollModule,
        OneVideoModule,
        IncidentSharedModule,
    ],
    declarations: [
        IncidentRegisterTableComponent,
        IncidentRegisterComponent,
    ],
    exports: [
        IncidentRegisterComponent,
    ],
    entryComponents: [
        IncidentRegisterComponent,
    ],
    providers: [
        IncidentRegisterService,
        PIPES, // TODO: Remove from providers when ot-common is implemented. Only necessary if you are using pipe in TS file, else just module is fine
    ],
})
export class IncidentRegisterModule {}
