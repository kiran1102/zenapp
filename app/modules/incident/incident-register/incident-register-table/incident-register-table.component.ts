// Core
import {
    Component,
    EventEmitter,
    Input,
    Inject,
    Output,
} from "@angular/core";
import { StateService } from "@uirouter/core";
import { OtModalService, ConfirmModalType } from "@onetrust/vitreus";

// 3rd Party
import { Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";

// Services
import { Permissions } from "modules/shared/services/helper/permissions.service";
import { TimeStamp } from "oneServices/time-stamp.service";
import { AssessmentBulkLaunchActionService } from "sharedModules/services/actions/bulk-launch-assessment-action.service";
import { IncidentRegisterService } from "incidentModule/incident-register/shared/services/incident-register.service";

// Interfaces
import { IAction } from "interfaces/redux.interface";
import { IIncidentRegister } from "incidentModule/shared/interfaces/incident-register.interface";
import { ITableColumnConfig } from "interfaces/table-config.interface";
import { IDropdownOption } from "interfaces/one-dropdown.interface";
import { IPageableMeta } from "interfaces/pagination.interface";

// Enums / Constants
import { TableColumnTypes } from "enums/data-table.enum";
import { IncidentTableActions } from "incidentModule/shared/enums/incident-actions.enums";
import { findIndex } from "lodash";
import { InventoryTableIds } from "enums/inventory.enum";
import { IncidentStageBadgeColorMap } from "incidentModule/shared/constants/incident-badge-color.constants";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";
import { IRecordAssignment } from "interfaces/inventory.interface";
import { IAssessmentCreationDetails } from "interfaces/assessment/assessment-creation-details.interface";

@Component({
    selector: "incident-register-table",
    templateUrl: "./incident-register-table.component.html",
})
export class IncidentRegisterTableComponent {

    @Input() rows = [];
    @Input() columns: ITableColumnConfig[];
    @Input() allRowsSelected = false;
    @Input() sortOrder: string;
    @Input() sortKey: string | number;
    @Input() tableMetaData: IPageableMeta;
    @Output() handleTableActions = new EventEmitter<IAction>();
    @Output() addNewRecord = new EventEmitter();

    incidentBadgeColorMap = IncidentStageBadgeColorMap;
    colBordered = false;
    disabledRowMap = {};
    editColIndex: number;
    editRowIndex: number;
    menuClass: string;
    resizableCol = false;
    responsiveTable = true;
    rowBordered = true;
    rowNoHover = false;
    rowStriped = false;
    selectedRowMap = {};
    sortable = true;
    tableColumnTypes = TableColumnTypes;
    truncateCellContent = true;
    wrapCellContent = false;
    showDeadline = this.timeStamp.displayDate() && this.timeStamp.displayTime();

    private destroy$ = new Subject();

    constructor(
        private stateService: StateService,
        public permissions: Permissions,
        public translatePipe: TranslatePipe,
        private readonly timeStamp: TimeStamp,
        private incidentRegisterService: IncidentRegisterService,
        private otModalService: OtModalService,
        private assessmentBulkLaunchActionService: AssessmentBulkLaunchActionService,
    ) {}

    ngOnInit(): void {
    }

    ngOnDestroy() {
        this.destroy$.next();
        this.destroy$.complete();
    }

    checkIfRowIsDisabled = (row: IIncidentRegister): boolean => !!this.disabledRowMap[row.incidentId];
    checkIfRowIsSelected = (row: IIncidentRegister): boolean => !!this.selectedRowMap[row.incidentId];
    columnTrackBy = (i: number): number => i;

    handleSortChange({ sortKey, sortOrder }): void {
        this.sortKey = sortKey;
        this.sortOrder = sortOrder;
        this.handleTableActions.emit({ type: IncidentTableActions.SORT_QUEUE, payload: { sortKey: this.sortKey, sortOrder: this.sortOrder } });
    }

    checkIfAllSelected(): boolean {
        const keys = Object.keys(this.selectedRowMap);

        if (keys.length === this.rows.length) {
            for (let i = 0; i < keys.length; i++) {
                if (!this.selectedRowMap[keys[i]]) {
                    return false;
                }
            }
        } else {
            return false;
        }
        return true;
    }

    handleTableSelectClick(actionType: string, rowId: string): void {
        switch (actionType) {
            case "header":
                this.allRowsSelected = !this.allRowsSelected;

                if (this.allRowsSelected) {
                    this.rows.forEach((elem: IIncidentRegister) => {
                        this.selectedRowMap[elem.incidentId] = elem.incidentId;
                    });
                } else {
                    this.selectedRowMap = {};
                }
                break;
            case "row":
                this.selectedRowMap[rowId] = !this.selectedRowMap[rowId];
                this.allRowsSelected = this.checkIfAllSelected();
        }
    }

    goToRoute(row: IIncidentRegister): void {
        this.stateService.go("zen.app.pia.module.incident.views.incident-detail", { id: row.incidentId });
    }

    launchAssessment(record: IIncidentRegister): void {
        const bulkInventoryList: IRecordAssignment[] = [{
            InventoryId: record.incidentId,
            Name: record.name,
            OrgGroupId: record.orgGroupId,
            defaultRespondent: record.assigneeId,
        }];
        const bulkAssessmentDetails: IAssessmentCreationDetails[] = this.assessmentBulkLaunchActionService.getAssessmentCreationItems(bulkInventoryList, InventoryTableIds.Incidents);
        this.assessmentBulkLaunchActionService.setBulkLaunchAssessments(bulkAssessmentDetails);
        this.stateService.go("zen.app.pia.module.incident.views.incident-launch-assessment", { inventoryTypeId: InventoryTableIds.Incidents });
    }

    deleteIncident(record: IIncidentRegister): void {
        this.otModalService
            .confirm({
                type: ConfirmModalType.DELETE,
                translations: {
                    title: this.translatePipe.transform("DeleteRecord"),
                    desc: this.translatePipe.transform("DeleteIncidentRecordDescription"),
                    confirm: this.translatePipe.transform("DeleteIncidentRecordConfirmation"),
                    submit: this.translatePipe.transform("Delete"),
                    cancel: this.translatePipe.transform("Cancel"),
                },
                hideClose: false,
            }).pipe(
                takeUntil(this.destroy$),
            ).subscribe(
                (res) => res && this.incidentRegisterService.removeIncidentRegister(record.incidentId, this.tableMetaData),
            );
    }

    onAddNewIncidentClicked(): void {
        this.addNewRecord.emit();
    }

    getActions(row: IIncidentRegister): () => IDropdownOption[] {
        return (): IDropdownOption[] => {
            this.setMenuClass(row);
            const menuOptions: IDropdownOption[] = [];

            menuOptions.push({
                text: this.translatePipe.transform("LaunchAssessment"),
                action: (): void => this.launchAssessment(row),
            });

            if (this.permissions.canShow("IncidentDelete")) {
                menuOptions.push({
                    text: this.translatePipe.transform("DeleteIncident"),
                    action: (): void => this.deleteIncident(row),
                });
            }

            return menuOptions;
        };
    }

    private setMenuClass(row: IIncidentRegister) {
        this.menuClass = "actions-table__context-list";
        if (this.rows.length <= 10) return;
        const halfwayPoint: number = (this.rows.length - 1) / 2;
        const rowIndex: number = findIndex(this.rows, (item: IIncidentRegister): boolean => row.incidentId === item.incidentId);
        if (rowIndex > halfwayPoint) this.menuClass += " actions-table__context-list--above";
    }
}
