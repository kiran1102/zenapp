// Core
import {
    OnInit,
    Component,
    Inject,
    OnDestroy,
} from "@angular/core";
import { IncidentAddEditModalComponent } from "incidentModule/shared/components/incident-add-edit-modal/incident-add-edit-modal.component";

// Rxjs
import {
    Subscription,
    Subject,
    BehaviorSubject,
} from "rxjs";
import { takeUntil } from "rxjs/operators";

// Redux
import { getCurrentOrgId } from "oneRedux/reducers/org.reducer";
import { StoreToken } from "tokens/redux-store.token";

// Services
import { OtModalService } from "@onetrust/vitreus";
import { Permissions } from "modules/shared/services/helper/permissions.service";
import { IncidentRegisterService } from "incidentModule/incident-register/shared/services/incident-register.service";
import { IncidentDetailService } from "incidentModule/incident-detail/shared/services/incident-detail.service";

// Interfaces
import { IAction } from "interfaces/redux.interface";
import { IStore } from "interfaces/redux.interface";
import { IIncidentRegister } from "incidentModule/shared/interfaces/incident-register.interface";
import { IIncidentAddEditModalData } from "incidentModule/shared/interfaces/incident-add-edit-modal.interface";
import { IIncidentRegisterTable } from "incidentModule/shared/interfaces/incident-register.interface";

// Enums / Constants
import {
    IncidentTableActions,
    IncidentTableColumnNames,
} from "incidentModule/shared/enums/incident-actions.enums";
import { TableColumnTypes } from "enums/data-table.enum";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";
import { IPageableMeta } from "interfaces/pagination.interface";

@Component({
    selector: "incident-register",
    templateUrl: "./incident-register.component.html",
})

export class IncidentRegisterComponent implements OnInit, OnDestroy {
    loadingIncidentRegister$ = new BehaviorSubject<boolean> (true);
    incidentSearchText = "";
    sortKey: string | number = IncidentTableColumnNames.CREATED_DT_COLUMN_NAME;
    sortOrder = "desc";
    defaultPagination = { page: 0, size: 20, sort: `${this.sortKey},${this.sortOrder}` };
    paginationParams = this.defaultPagination;
    searchText = "";
    resetSearch$ = new Subject();
    table: IIncidentRegisterTable;
    tableMetaData: IPageableMeta;

    private subscription: Subscription;
    private destroy$ = new Subject();

    constructor(
        @Inject(StoreToken) private store: IStore,
        private incidentRegisterService: IncidentRegisterService,
        private incidentDetailService: IncidentDetailService,
        private otModalService: OtModalService,
        private translatePipe: TranslatePipe,
        public permissions: Permissions,
    ) {}

    ngOnInit(): void {
        this.incidentRegisterService.incidentRegister$.pipe(
            takeUntil(this.destroy$),
        ).subscribe((records: IIncidentRegister[]) => {
            this.table = this.configureTable(records);
            this.loadingIncidentRegister$.next(false);
        });

        this.incidentRegisterService.incidentRegisterMetaData$.pipe(
            takeUntil(this.destroy$),
        ).subscribe((metaData: IPageableMeta) => {
            this.tableMetaData = metaData;
        });

        this.incidentRegisterService.retrieveIncidentRegister();
        this.incidentDetailService.retrieveIncidentTypes();
    }

    ngOnDestroy(): void {
        this.destroy$.next();
        this.destroy$.complete();
    }

    configureTable(records: IIncidentRegister[]): IIncidentRegisterTable {
        return {
            rows: records,
            columns: [
                {
                    name: this.translatePipe.transform("Name"),
                    sortKey: IncidentTableColumnNames.INCIDENT_NAME_COLUMN,
                    type: TableColumnTypes.Link,
                    cellValueKey: IncidentTableColumnNames.INCIDENT_NAME_COLUMN,
                },
                {
                    name: this.translatePipe.transform("Type"),
                    sortKey: IncidentTableColumnNames.INCIDENT_TYPE_NAME_COLUMN,
                    type: TableColumnTypes.Type,
                    cellValueKey: IncidentTableColumnNames.INCIDENT_TYPE_NAME_COLUMN,
                },
                {
                    name: this.translatePipe.transform("Organization"),
                    sortKey: IncidentTableColumnNames.INCIDENT_ORG_GROUP_NAME_COLUMN,
                    type: TableColumnTypes.Text,
                    cellValueKey: IncidentTableColumnNames.INCIDENT_ORG_GROUP_NAME_COLUMN,
                },
                {
                    name: this.translatePipe.transform("Reporter"),
                    sortKey: IncidentTableColumnNames.CREATOR_COLUMN_NAME,
                    type: TableColumnTypes.Text,
                    cellValueKey: IncidentTableColumnNames.CREATOR_COLUMN_NAME,
                },
                {
                    name: this.translatePipe.transform("Stage"),
                    sortKey: IncidentTableColumnNames.INCIDENT_STAGE_NAME,
                    type: TableColumnTypes.Stage,
                    cellValueKey: IncidentTableColumnNames.INCIDENT_STAGE_NAME,
                },
                {
                    name: this.translatePipe.transform("Assignee"),
                    sortKey: IncidentTableColumnNames.INCIDENT_ASSIGNEE_NAME_COLUMN,
                    type: TableColumnTypes.Assignee,
                    cellValueKey: IncidentTableColumnNames.INCIDENT_ASSIGNEE_NAME_COLUMN,
                },
                {
                    name: this.translatePipe.transform("DateReported"),
                    sortKey: IncidentTableColumnNames.CREATED_DT_COLUMN_NAME,
                    type: TableColumnTypes.Date,
                    cellValueKey: IncidentTableColumnNames.CREATED_DT_COLUMN_NAME,
                },
                {
                    name: this.translatePipe.transform("Deadline"),
                    sortKey: IncidentTableColumnNames.INCIDENT_DEADLINE_COLUMN,
                    type: TableColumnTypes.Deadline,
                    cellValueKey: IncidentTableColumnNames.INCIDENT_DEADLINE_COLUMN,
                },
                {
                    name: "",
                    type: TableColumnTypes.Action,
                    cellValueKey: "",
                },
            ],
        };
    }

    handleTableAction(action: IAction): void {
        this.handleAction(action.type, action.payload);
    }

    handleAction(type: string, payload: any) {
        if (type === IncidentTableActions.SORT_QUEUE) this.onSortChange(payload.sortKey, payload.sortOrder);
    }

    onSortChange(sortKey: number = 1, sortOrder: string = ""): void {
        this.sortKey = sortKey;
        this.sortOrder = sortOrder;
        this.defaultPagination = { ...this.defaultPagination, sort: `${sortKey},${sortOrder}`};
        this.paginationParams = { ...this.paginationParams, sort: `${sortKey},${sortOrder}`};
        this.incidentRegisterService.retrieveIncidentRegister(this.searchText, [], this.paginationParams);
        this.loadingIncidentRegister$.next(true);
    }

    onSearchChange(searchText: string = ""): void {
        this.searchText = searchText;
        this.paginationParams = this.defaultPagination;
        this.incidentRegisterService.retrieveIncidentRegister(this.searchText, [], this.defaultPagination);
        this.loadingIncidentRegister$.next(true);
    }

    onPaginationChange(event: number = 0): void {
        this.paginationParams = { ...this.paginationParams, page: event - 1 };
        this.incidentRegisterService.retrieveIncidentRegister(this.searchText, [], this.paginationParams);
        this.loadingIncidentRegister$.next(true);
    }

    onAddNewIncidentClicked(): void {
        const modalData: IIncidentAddEditModalData = {
            parentId: getCurrentOrgId(this.store.getState()),
            incidentRegisterLength: this.tableMetaData.totalElements,
            incidentDetail: null,
            actionButtonTitle: "Add",
            cancelButtonTitle: "Cancel",
            modalTitle: "AddNewIncident",
            callback: () => {
                this.onSearchChange("");
                this.resetSearch$.next();
            },
        };
        this.otModalService
            .create(
                IncidentAddEditModalComponent,
                {
                    isComponent: true,
                },
                modalData,
            ).pipe(
                takeUntil(this.destroy$),
            );
    }
}
