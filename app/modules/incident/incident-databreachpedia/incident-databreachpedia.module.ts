import { NgModule } from "@angular/core";

// Modules
import { SharedModule } from "sharedModules/shared.module";
import { CommonModule } from "@angular/common";
import { HttpClientModule } from "@angular/common/http";
import { FormsModule } from "@angular/forms";
import { ReactiveFormsModule } from "@angular/forms";
import { VitreusModule } from "@onetrust/vitreus";
import { DndListModule } from "ngx-drag-and-drop-lists";
import { OTPipesModule, PIPES } from "modules/pipes/pipes.module";
import { VirtualScrollModule } from "od-virtualscroll";
import { OneVideoModule } from "modules/video/video.module";
import { IncidentSharedModule } from "incidentModule/shared/incident-shared.module";

// Components
import { IncidentDataBreachPediaComponent } from "incidentModule/incident-databreachpedia/incident-databreachpedia.component";
import { IncidentDatabreachpediaListComponent } from "incidentModule/incident-databreachpedia/incident-databreachpedia-list/incident-databreachpedia-list.component";
import { IncidentDatabreachpediaViewComponent } from "incidentModule/incident-databreachpedia/incident-databreachpedia-view/incident-databreachpedia-view.component";

// Services
import { IncidentDataBreachPediaService } from "incidentModule/incident-databreachpedia/shared/services/incident-databreachpedia.service";
import { IncidentDataBreachPediaApiService } from "incidentModule/incident-databreachpedia/shared/services/incident-databreachpedia-api.service";

@NgModule({
    imports: [
        SharedModule,
        CommonModule,
        HttpClientModule,
        FormsModule,
        ReactiveFormsModule,
        VitreusModule,
        DndListModule,
        OTPipesModule,
        VirtualScrollModule,
        OneVideoModule,
        IncidentSharedModule,
    ],
    declarations: [
        IncidentDataBreachPediaComponent,
        IncidentDatabreachpediaListComponent,
        IncidentDatabreachpediaViewComponent,
    ],
    exports: [
        IncidentDataBreachPediaComponent,
        IncidentDatabreachpediaViewComponent,
    ],
    entryComponents: [
        IncidentDataBreachPediaComponent,
        IncidentDatabreachpediaViewComponent,
    ],
    providers: [
        IncidentDataBreachPediaService,
        IncidentDataBreachPediaApiService,
        PIPES, // TODO: Remove from providers when ot-common is implemented. Only necessary if you are using pipe in TS file, else just module is fine
    ],
})
export class IncidentDataBreachPediaModule {}
