// Core
import { Component } from "@angular/core";
import { StateService } from "@uirouter/core";

// Service
import { IncidentDataBreachPediaService } from "incidentModule/incident-databreachpedia/shared/services/incident-databreachpedia.service";

// interface
import {
    IIncidentDBPLawMeta,
    IIncidentDBPLawContent,
} from "incidentModule/shared/interfaces/incident-databreachpedia-view.interface";

// 3rd Party
import { Subject } from "rxjs";

// Enums
import { IncidentDBPViewContentSections } from "incidentModule/shared/enums/incident-databreachpedia-view.enums";

@Component({
    selector: "incident-databreachpedia-view",
    templateUrl: "./incident-databreachpedia-view.component.html",
})
export class IncidentDatabreachpediaViewComponent {
    jurisdictionId = "";
    jurisdictionName = "";
    lawId = "";
    incidentDBPViewContentSections = IncidentDBPViewContentSections;
    jurisdictionLaws: IIncidentDBPLawMeta[];
    incidentDBPContent: IIncidentDBPLawContent;
    private destroy$ = new Subject();

    constructor(
        private incidentDataBreachPediaService: IncidentDataBreachPediaService,
        private stateService: StateService,
        ) {}

    ngOnInit() {
        this.jurisdictionId = this.stateService.params.jurisdictionId;
        this.lawId = this.stateService.params.lawId;
        if (this.jurisdictionId) {
            this.getDbpLaws(this.jurisdictionId);
        } else if (this.lawId) {
            this.getDbpLaw(this.lawId);
        }
    }

    ngOnDestroy() {
        this.destroy$.next();
        this.destroy$.complete();
    }

    goToDBPListView() {
        this.jurisdictionId = null;
        this.lawId = null;
        this.stateService.go("zen.app.pia.module.incident.views.incident-databreachpedia");
    }

    getDbpLaws(jurisdictionId: string) {
        this.incidentDataBreachPediaService.getJurisdictionLaws(jurisdictionId).then((jurisdictionLawsMeta) => {
            if (jurisdictionLawsMeta.data) {
                this.jurisdictionLaws = jurisdictionLawsMeta.data;
                if (this.lawId) {
                    this.openLawSection();
                    setTimeout(() => {
                        this.registerSections(this.jurisdictionLaws);
                    });
                } else {
                    this.openLaw(this.jurisdictionLaws[0].law.id);
                }
            }
        });
    }

    getDbpLaw(lawId: string) {
        this.incidentDataBreachPediaService.getDatabreachLaw(lawId).then((databreachLaw) => {
            if (databreachLaw.data) {
                this.incidentDBPContent = databreachLaw.data;
                this.jurisdictionName = this.incidentDBPContent.jurisdictions[0].name;
                this.jurisdictionId = this.incidentDBPContent.jurisdictions[0].id;
                this.getDbpLaws(this.incidentDBPContent.jurisdictions[0].id);
            }
        });
    }

    openLaw(lawId: string) {
        this.incidentDataBreachPediaService.getDatabreachLaw(lawId).then((databreachLaw) => {
            if (databreachLaw.data) {
                this.incidentDBPContent = databreachLaw.data;
                this.jurisdictionName = this.incidentDBPContent.jurisdictions[0].name;
                this.openLawSection();
                setTimeout(() => {
                    this.registerSections(this.jurisdictionLaws);
                });
            }
        });
    }

    lawTrackBy(index: number): number {
        return index;
    }

    goToLawSection(sectionName: string) {
        this.incidentDataBreachPediaService.scrollToElement(sectionName);
    }

    openLawSection() {
        this.jurisdictionLaws.some((law) => law.isActive = false);
        this.jurisdictionLaws[0].isActive = true;
    }

    registerSections(jurisdictionLaws: IIncidentDBPLawMeta[]) {
        jurisdictionLaws[0].lawSections.forEach((section) => {
            const scrollingContainer = document.getElementById(section);
            if (scrollingContainer) {
                this.incidentDataBreachPediaService.registerDOMElement(section, scrollingContainer);
            }
        });
    }
}
