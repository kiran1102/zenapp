import { Injectable } from "@angular/core";

// Rxjs
import { BehaviorSubject, Observable } from "rxjs";

// Interfaces
import { IDatabreachLawRegions,
    IDatabreachLawRegion,
    IIncidentDBPLawMeta,
    IIncidentDBPLawContent,
    IIncidentDBPLaw,
} from "incidentModule/shared/interfaces/incident-databreachpedia-view.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";

// Service
import { IncidentDataBreachPediaApiService } from "incidentModule/incident-databreachpedia/shared/services/incident-databreachpedia-api.service";

@Injectable()
export class IncidentDataBreachPediaService {
    databreachLaws$ = new BehaviorSubject<IDatabreachLawRegion[]> (null);
    private domElements = {};

    constructor(
        private incidentDataBreachPediaApiService: IncidentDataBreachPediaApiService,
        ) { }

    reset() {
        this.databreachLaws$.next(null);
    }

    getDatabreachLaws() {
        this.incidentDataBreachPediaApiService.getDatabreachLaws().then((breachLaws: IProtocolResponse<IDatabreachLawRegions>) => {
            if (breachLaws && breachLaws.data && breachLaws.data.regions) {
                breachLaws.data.regions.forEach((region) => {
                    region.laws.sort((prev, next) => prev.displayName < next.displayName ? -1 : 1);
                });
                this.databreachLaws$.next(breachLaws.data.regions);
            }
        });
    }

    getJurisdictionLaws(jurisdictionId: string): ng.IPromise<IProtocolResponse<IIncidentDBPLawMeta[]>> {
        return this.incidentDataBreachPediaApiService.getJurisdictionLaws(jurisdictionId);
    }

    getDatabreachLaw(lawId: string): ng.IPromise<IProtocolResponse<IIncidentDBPLawContent>> {
        return this.incidentDataBreachPediaApiService.getDatabreachLaw(lawId);
    }

    registerDOMElement(domId: string, element: HTMLElement): void {
        this.domElements[domId] = element;
    }

    scrollToElement(domId: string) {
        if (domId && this.domElements[domId]) {
            this.domElements[domId].scrollIntoView({ behavior: "smooth" });
        }
    }

    getListOfDatabreachLaws(jurisdictionIds: string[]): Observable<IIncidentDBPLaw[]> {
        return this.incidentDataBreachPediaApiService.getListOfDatabreachLaws(jurisdictionIds);
    }
}
