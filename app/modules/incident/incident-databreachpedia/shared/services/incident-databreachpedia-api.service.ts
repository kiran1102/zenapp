// Core
import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";

// Services
import { ProtocolService } from "modules/core/services/protocol.service";

// Interfaces
import {
    IProtocolConfig,
    IProtocolMessages,
    IProtocolResponse,
} from "interfaces/protocol.interface";
import {
    IDatabreachLawRegions,
    IIncidentDBPLawMeta,
    IIncidentDBPLawContent,
    IIncidentDBPLaw,
} from "incidentModule/shared/interfaces/incident-databreachpedia-view.interface";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

// RxJs
import { Observable } from "rxjs";

@Injectable()
export class IncidentDataBreachPediaApiService {
    constructor(
        private translatePipe: TranslatePipe,
        private protocolService: ProtocolService,
        private httpClient: HttpClient,
    ) { }

    public getDatabreachLaws(): ng.IPromise<IProtocolResponse<IDatabreachLawRegions>> {
        const config: IProtocolConfig = this.protocolService.customConfig("GET", `/api/gallery/v1/gallery/jurisdictions/laws`, null);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("ErrorGettingDatabreachLaws") } };
        return this.protocolService.http(config, messages);
    }

    public getJurisdictionLaws(jurisdictionId: string): ng.IPromise<IProtocolResponse<IIncidentDBPLawMeta[]>> {
        const config: IProtocolConfig = this.protocolService.customConfig("GET", `/api/gallery/v1/gallery/jurisdictions/${jurisdictionId}/laws`, null);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("ErrorGettingJurisdictionLaws") } };
        return this.protocolService.http(config, messages);
    }

    public getDatabreachLaw(lawId: string): ng.IPromise<IProtocolResponse<IIncidentDBPLawContent>> {
        const config: IProtocolConfig = this.protocolService.customConfig("GET", `/api/gallery/v1/gallery/jurisdictions/laws/${lawId}/content`, null);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("ErrorGettingDatabreachLaw") } };
        return this.protocolService.http(config, messages);
    }

    public getListOfDatabreachLaws(jurisdictionIds: string[]): Observable<IIncidentDBPLaw[]> {
        const url = `/api/gallery/v1/gallery/jurisdictions/laws`;
        return this.httpClient.post<IIncidentDBPLaw[]>(url, jurisdictionIds);
    }
}
