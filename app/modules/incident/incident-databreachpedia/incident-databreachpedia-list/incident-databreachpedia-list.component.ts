// Core
import { Component } from "@angular/core";
import { StateService } from "@uirouter/core";

// Service
import { IncidentJurisdictionService } from "incidentModule/incident-detail/shared/services/incident-jurisdiction.service";
import { IncidentDataBreachPediaService } from "incidentModule/incident-databreachpedia/shared/services/incident-databreachpedia.service";

// interface
import { IDatabreachLawMap } from "incidentModule/shared/interfaces/incident-databreachpedia-view.interface";

@Component({
    selector: "incident-databreachpedia-list",
    templateUrl: "./incident-databreachpedia-list.component.html",
})
export class IncidentDatabreachpediaListComponent {
    jurisdictionRegionClosed: { [key: string]: boolean } = {};
    lawClosed: { [key: string]: boolean } = {};
    jurisdictionRegions$ = this.incidentJurisdictionService.jurisdictionOptions$;
    databreachLaws$ = this.incidentDataBreachPediaService.databreachLaws$;
    children = "stateProvinces";
    value = "jurisdictionId";
    label = "name";
    model = null;

    constructor(
        private incidentJurisdictionService: IncidentJurisdictionService,
        private incidentDataBreachPediaService: IncidentDataBreachPediaService,
        private stateService: StateService,
        ) {}

    ngOnInit() {
        this.incidentJurisdictionService.retrieveJurisdictionOptions();
        this.incidentDataBreachPediaService.getDatabreachLaws();
    }

    viewJurisdiction(id: string) {
        if (id) {
            this.stateService.go("zen.app.pia.module.incident.views.incident-databreachpedia-jurisdiction-view", { jurisdictionId: id });
        }
    }

    viewLaw(law: IDatabreachLawMap) {
        this.stateService.go("zen.app.pia.module.incident.views.incident-databreachpedia-law-view", { lawId: law.id });
    }

    toggledJurisdictionRegion(toggled: boolean, id: string) {
        if (!toggled) {
            this.jurisdictionRegionClosed[id] = true;
        } else {
            delete this.jurisdictionRegionClosed[id];
        }
    }

    toggledLaw(toggled: boolean, id: string) {
        if (!toggled) {
            this.lawClosed[id] = true;
        } else {
            delete this.lawClosed[id];
        }
    }
}
