import angular from "angular";
import { routes } from "./incident.routes";
import { IncidentWrapperComponent } from "incidentModule/incident.component";

export const incidentLegacyModule = angular
    .module("zen.incident", [
        "ui.router",
    ])
    .config(routes)
    .component("incidentWrapper", IncidentWrapperComponent)
    ;
