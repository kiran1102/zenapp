// Core
import {
    Component,
    Input,
    Inject,
    Output,
    EventEmitter,
} from "@angular/core";
import { StateService } from "@uirouter/core";

// Rxjs
import { Subject } from "rxjs";

// 3rd Party
import { findIndex } from "lodash";

// Services
import { IncidentDetailService } from "incidentModule/incident-detail/shared/services/incident-detail.service";
import { TimeStamp } from "oneServices/time-stamp.service";
import { Permissions } from "modules/shared/services/helper/permissions.service";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

// Interfaces
import { IDropdownOption } from "interfaces/one-dropdown.interface";
import { IIncidentAssessmentRecord } from "incidentModule/shared/interfaces/incident-assessment-state.interface";
import { ITableColumnConfig } from "interfaces/table-config.interface";
import { IDataTableColumn } from "interfaces/tables.interface";
import { IPageableOf } from "interfaces/pagination.interface";

// Enums / Constants
import { TableColumnTypes } from "enums/data-table.enum";
import {
    RiskLabelBgClasses,
    RiskLevels,
} from "enums/risk.enum";
import { IncidentTranslationKeys } from "incidentModule/shared/enums/incident-translation-keys.enums";
import { AssessmentStatus } from "enums/assessment/assessment-status.enum";
import { AssessmentStateKeys } from "constants/assessment.constants";
import { IncidentAssessmentStatusBadgeColorMap } from "incidentModule/shared/constants/incident-badge-color.constants";

@Component({
    selector: "incident-assessment-table",
    templateUrl: "./incident-assessment-table.component.html",
})
export class IncidentAssessmentTableComponent {
    @Input() table;
    @Input() rows = [];
    @Input() columns: ITableColumnConfig[];
    @Input() metaData: IPageableOf<IIncidentAssessmentRecord>;
    @Input() allRowsSelected = false;
    @Output() reloadIncidentLinkedAssessments: EventEmitter<any> = new EventEmitter<any>();
    @Output() onLinkAssessment: EventEmitter<any> = new EventEmitter<any>();
    colBordered = false;
    disabledRowMap = {};
    editColIndex: number;
    editRowIndex: number;
    incidentId = this.stateService.params.id;
    menuClass: string;
    resizableCol = false;
    responsiveTable = true;
    rowBordered = true;
    rowNoHover = false;
    rowStriped = false;
    selectedRowMap = {};
    sortKey = "";
    sortOrder = "";
    sortable = false;
    tableColumnTypes = TableColumnTypes;
    truncateCellContent = true;
    wrapCellContent = false;
    showDeadline = this.timeStamp.displayDate() && this.timeStamp.displayTime();
    riskLevelBgClass: typeof RiskLabelBgClasses = RiskLabelBgClasses;
    riskLevel: typeof RiskLevels = RiskLevels;
    incidentBadgeColorMap = IncidentAssessmentStatusBadgeColorMap;
    incidentTranslationKeys = IncidentTranslationKeys;

    private destroy$ = new Subject();

    constructor(
        private stateService: StateService,
        private timeStamp: TimeStamp,
        private incidentDetailService: IncidentDetailService,
        private translatePipe: TranslatePipe,
        public permissions: Permissions,
    ) {}

    ngOnInit(): void {
    }

    ngOnDestroy(): void {
        this.destroy$.next();
        this.destroy$.complete();
    }

    columnTrackBy(index: number, column: IDataTableColumn): string {
        return column.id;
    }

    checkIfRowIsDisabled = (row: IIncidentAssessmentRecord): boolean => !!this.disabledRowMap[row.assessmentId];
    checkIfRowIsSelected = (row: IIncidentAssessmentRecord): boolean => !!this.selectedRowMap[row.assessmentId];

    handleSortChange({ sortKey, sortOrder }): void {
        this.sortKey = sortKey;
        this.sortOrder = sortOrder;
        this.rows.reverse();
    }

    checkIfAllSelected(): boolean {
        const keys = Object.keys(this.selectedRowMap);

        if (keys.length === this.rows.length) {
            for (let i = 0; i < keys.length; i++) {
                if (!this.selectedRowMap[keys[i]]) {
                    return false;
                }
            }
        } else {
            return false;
        }
        return true;
    }

    handleTableSelectClick(actionType: string, rowId: string): void {
        switch (actionType) {
            case "header":
                this.allRowsSelected = !this.allRowsSelected;

                if (this.allRowsSelected) {
                    this.rows.forEach((elem: IIncidentAssessmentRecord) => {
                        this.selectedRowMap[elem.assessmentId] = elem.assessmentId;
                    });
                } else {
                    this.selectedRowMap = {};
                }
                break;
            case "row":
                this.selectedRowMap[rowId] = !this.selectedRowMap[rowId];
                this.allRowsSelected = this.checkIfAllSelected();
        }
    }

    getActions(row: IIncidentAssessmentRecord): () => IDropdownOption[] {
        return (): IDropdownOption[] => {
            this.setMenuClass(row);
            const menuOptions: IDropdownOption[] = [];

            menuOptions.push({
                text: this.translatePipe.transform("RemoveLink"),
                isDisabled: row.primaryLink,
                toolTip: row.primaryLink ? this.translatePipe.transform(IncidentTranslationKeys.removeLinkIncidentToolTip) : "",
                action: (): void => this.removeAssessmentLink(row.assessmentId),
            });

            return menuOptions;
        };
    }

    goToRoute(row: IIncidentAssessmentRecord): void {
        this.stateService.go("zen.app.pia.module.assessment.detail", { assessmentId: row.assessmentId });
    }

    assessmentStatusName(name: string): string {
        switch (name) {
            case AssessmentStatus.Not_Started:
                return this.translatePipe.transform(AssessmentStateKeys.NotStarted);
            case AssessmentStatus.In_Progress:
                return this.translatePipe.transform(AssessmentStateKeys.InProgress);
            case AssessmentStatus.Under_Review:
                return this.translatePipe.transform(AssessmentStateKeys.UnderReview);
            case AssessmentStatus.Completed:
                return this.translatePipe.transform(AssessmentStateKeys.Completed);
            default:
                return name;
        }
    }

    private removeAssessmentLink(assessmentId: string): void {
        this.incidentDetailService.removeIncidentAssessmentLink(this.incidentId, assessmentId, this.metaData);
    }

    private setMenuClass(row: IIncidentAssessmentRecord) {
        this.menuClass = "actions-table__context-list";
        if (this.table.rows.length <= 10) return;
        const halfwayPoint: number = (this.table.rows.length - 1) / 2;
        const rowIndex: number = findIndex(this.table.rows, (item: IIncidentAssessmentRecord): boolean => row.assessmentId === item.assessmentId);
        if (rowIndex > halfwayPoint) this.menuClass += " actions-table__context-list--above";
    }
}
