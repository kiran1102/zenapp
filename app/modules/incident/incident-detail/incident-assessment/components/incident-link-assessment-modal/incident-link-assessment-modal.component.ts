// Angular
import { Component, Inject } from "@angular/core";
import {
    FormGroup,
    FormBuilder,
    Validators,
} from "@angular/forms";
import { IOtModalContent } from "@onetrust/vitreus";

// Rxjs
import {
    Subscription,
    timer,
    Subject,
} from "rxjs";

// Third Party
import {
    differenceBy,
    map,
    filter,
} from "lodash";

// Services
import { IncidentDetailService } from "incidentModule/incident-detail/shared/services/incident-detail.service";

// Tokens
import { StoreToken } from "tokens/redux-store.token";

// Interfaces
import { IStore } from "interfaces/redux.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";
import { INameId } from "interfaces/generic.interface";
import { IIncidentAssessmentLink } from "incidentModule/shared/interfaces/incident-create-state.interface";

// Pipes && Enums
import { TranslatePipe } from "pipes/translate.pipe";
import { IncidentTranslationKeys } from "incidentModule/shared/enums/incident-translation-keys.enums";
import { IIncidentDetail } from "incidentModule/shared/interfaces/incident-detail-state.interface";

@Component({
    selector: "incident-link-assessment-modal",
    templateUrl: "./incident-link-assessment-modal.component.html",
})

export class IncidentLinkAssessmentModalComponent implements IOtModalContent {

    otModalCloseEvent: Subject<string>;
    assessmentOptions: Array<INameId<string>>;
    assessmentDropdownPlaceholder = this.translatePipe.transform("SelectATemplate");
    postSelectionAssessmentOptions: Array<INameId<string>> = [];
    postFilterAssessmentOptions: Array<INameId<string>> = [];
    inputAssessmentValue = "";
    labelKey = "name";
    valueKey = "id";
    isLoading = true;
    isLoadingAssessments = false;
    isSaving: boolean;
    linkAssessmentForm: FormGroup;
    otModalData: IIncidentDetail;
    incidentTranslationKeys = IncidentTranslationKeys;
    noAssessmentOptions: boolean;
    selectedAssessments: Array<INameId<string>>;
    selectedTemplateType: string;
    subscription: Subscription;

    constructor(
        @Inject(StoreToken) private store: IStore,
        private incidentDetailService: IncidentDetailService,
        private formBuilder: FormBuilder,
        private translatePipe: TranslatePipe,
    ) { }

    ngOnInit(): void {
        this.reset();
        this.subscription = timer(500)
            .subscribe(() => {
                this.isLoading = false;
            });
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

    cancelButtonClicked(): void {
        this.closeModal();
    }

    linkButtonClicked(): void {
        this.linkAssessments();
    }

    reset(): void {
        this.linkAssessmentForm = this.formBuilder.group({
            templateType: ["", Validators.required],
            assessments: [[], Validators.required],
        });
    }

    closeModal(): void {
        this.reset();
        this.otModalCloseEvent.next();
    }

    linkAssessments(): void {
        const assessmentIdsArray: string[] = map(this.selectedAssessments, (assessment: INameId<string>): string => {
            return assessment.id;
        });
        const payload: IIncidentAssessmentLink = {
            incidentId: this.otModalData.id,
            incidentName: this.otModalData.name,
            assessmentIds: assessmentIdsArray,
        };
        this.isSaving = true;
        this.incidentDetailService.createIncidentAssessmentsLink(payload)
            .then((res: IProtocolResponse<string>): void => {
                this.isSaving = false;
                if (res.result) {
                    this.closeModal();
                }
            });
    }

    selectTemplateType(selection: INameId<string>) {
        this.selectedTemplateType = selection.id;

        if (!selection) {
            this.linkAssessmentForm.get("templateType").setValue(null);
            this.selectedAssessments = [];
            return;
        }

        if (selection && selection.id !== this.linkAssessmentForm.get("templateType").value) {
            this.getIncidentLinkAssessmentDropdownList(selection.id);
        }

        this.linkAssessmentForm.get("templateType").setValue(selection.id);
    }

    getIncidentLinkAssessmentDropdownList(templateId: string): void {
        this.isLoadingAssessments = true;
        this.incidentDetailService.getIncidentLinkAssessmentDropdownList(this.otModalData.id, templateId)
            .then((res: IProtocolResponse<Array<INameId<string>>>): void => {
                if (!res.result) return;
                if (res.data.length === 0) {
                    this.noAssessmentOptions = true;
                    this.assessmentDropdownPlaceholder = this.translatePipe.transform("NoOptionsAvailable");
                } else {
                    this.noAssessmentOptions = false;
                    this.assessmentDropdownPlaceholder = this.translatePipe.transform("Assessments");
                }
                this.assessmentOptions = res.data;
                this.postSelectionAssessmentOptions = res.data;
                this.postFilterAssessmentOptions = res.data;
                this.selectedAssessments = [];
                this.isLoadingAssessments = false;
            });
    }

    onAssessmentModelChange(event: { currentValue: Array<INameId<string>>}) {
        this.postSelectionAssessmentOptions = differenceBy(this.assessmentOptions, event.currentValue, "id");
        this.postFilterAssessmentOptions = this.postSelectionAssessmentOptions;
        this.selectedAssessments = event.currentValue;
        this.linkAssessmentForm.get("assessments").setValue(event.currentValue);
    }

    onAssessmentInputChange({ key, value}) {
        if (key === "Enter") return;
        this.filterList(this.postSelectionAssessmentOptions, value);
    }

    private filterList(options: Array<INameId<string>>, stringSearch: string) {
        this.postFilterAssessmentOptions = filter(options, (option: INameId<string>): boolean => (option.name.search(new RegExp(stringSearch, "i")) !== -1));
    }
}
