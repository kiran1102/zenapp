// Core
import {
    Component,
    Inject,
    Input,
} from "@angular/core";

// Components
import { IncidentLinkAssessmentModalComponent } from "incidentModule/incident-detail/incident-assessment/components/incident-link-assessment-modal/incident-link-assessment-modal.component";

// Redux
import { StoreToken } from "tokens/redux-store.token";
import { getCurrentUser } from "oneRedux/reducers/user.reducer";

// Rxjs
import {
    Subject,
    BehaviorSubject,
    combineLatest,
} from "rxjs";
import { takeUntil } from "rxjs/operators";

// Services
import { Permissions } from "modules/shared/services/helper/permissions.service";
import { AssessmentBulkLaunchActionService } from "sharedModules/services/actions/bulk-launch-assessment-action.service";
import { IncidentDetailService } from "incidentModule/incident-detail/shared/services/incident-detail.service";
import { RiskAPIService } from "oneServices/api/risk-api.service";
import { OtModalService, ModalSize } from "@onetrust/vitreus";
import { StateService } from "@uirouter/core";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

// Interfaces
import { IStore } from "interfaces/redux.interface";
import { IIncidentDetail } from "incidentModule/shared/interfaces/incident-detail-state.interface";
import { IRecordAssignment } from "interfaces/inventory.interface";
import { IAssessmentCreationDetails } from "interfaces/assessment/assessment-creation-details.interface";
import { IIncidentAssessmentsRecordTable } from "incidentModule/shared/interfaces/incident-assessment-state.interface";

// Constants / Enums
import { InventoryTableIds } from "enums/inventory.enum";
import { TableColumnTypes } from "enums/data-table.enum";

@Component({
    selector: "incident-assessment",
    templateUrl: "./incident-assessment.component.html",
})
export class IncidentAssessmentComponent {
    @Input() model: IIncidentDetail;
    incidentId = this.stateService.params.id;
    defaultPagination = { page: 0, size: 10};
    paginationParams = this.defaultPagination;
    showLinkAssessment = false;
    showMore = false;
    heatmapEnabled = false;
    table: IIncidentAssessmentsRecordTable;
    loadingIncidentLinkedAssessmentRecords$ = new BehaviorSubject<boolean>(true);
    private destroy$ = new Subject();

    constructor(
        @Inject(StoreToken) private store: IStore,
        private stateService: StateService,
        private riskApi: RiskAPIService,
        public permissions: Permissions,
        private translatePipe: TranslatePipe,
        private incidentDetailService: IncidentDetailService,
        private assessmentBulkLaunchActionService: AssessmentBulkLaunchActionService,
        private otModalService: OtModalService,
    ) { }

    ngOnInit(): void {
        combineLatest(
            this.incidentDetailService.incidentLinkedAssessments$,
            this.riskApi.getRiskSetting(),
            this.incidentDetailService.incidentLinkedAssessmentsMetaData$,
        ).pipe(
            takeUntil(this.destroy$),
        ).subscribe(([linkedAssessments, riskSettings, linkedAssessmentsMetaData]) => {
            if (linkedAssessments && riskSettings && linkedAssessmentsMetaData) {
                this.heatmapEnabled = riskSettings.enableRiskHeatMap;
                this.table = this.configureTable(linkedAssessments);
                this.table.metaData = linkedAssessmentsMetaData;
                this.loadingIncidentLinkedAssessmentRecords$.next(false);
            }
        });

        this.getIncidentLinkedAssessments();
        this.showLinkAssessment = this.canLinkAssessment(this.model.assigneeId);
    }

    ngOnDestroy(): void {
        this.destroy$.next();
        this.destroy$.complete();
    }

    onLinkAssessment(): void {
        this.otModalService.create(
                IncidentLinkAssessmentModalComponent,
                {
                    isComponent: true,
                    size: ModalSize.SMALL,
                },
                this.model,
            ).pipe(
                takeUntil(this.destroy$),
            ).subscribe(
                () => this.getIncidentLinkedAssessments(),
            );
    }

    onLaunchAssessment(): void {
        const bulkInventoryList: IRecordAssignment[] = [{
            InventoryId: this.model.id,
            Name: this.model.name,
            OrgGroupId: this.model.orgGroupId,
            defaultRespondent: this.model.assigneeId,
        }];
        const bulkAssessmentDetails: IAssessmentCreationDetails[] = this.assessmentBulkLaunchActionService.getAssessmentCreationItems(bulkInventoryList, InventoryTableIds.Incidents);
        this.assessmentBulkLaunchActionService.setBulkLaunchAssessments(bulkAssessmentDetails);
        this.stateService.go(
            "zen.app.pia.module.incident.views.incident-launch-assessment",
            { inventoryTypeId: InventoryTableIds.Incidents, detailsId: this.incidentId },
        );
    }

    getIncidentLinkedAssessments(): void {
        this.incidentDetailService.getIncidentLinkedAssessments(this.incidentId, this.paginationParams);
    }

    canLinkAssessment(assigneeId: string): boolean {
        return this.permissions.canShow("IncidentRecordsView")
            || getCurrentUser(this.store.getState()).Id === assigneeId;
    }

    configureTable(linkedAssessments): IIncidentAssessmentsRecordTable {
        return {
            rows: linkedAssessments,
            columns: [
                { name: this.translatePipe.transform("Assessment"), sortKey: 1, type: TableColumnTypes.Link, cellValueKey: "assessment" },
                { name: this.translatePipe.transform("Organization"), sortKey: 2, type: TableColumnTypes.Text, cellValueKey: "organization" },
                { name: this.translatePipe.transform("Template"), sortKey: 3, type: TableColumnTypes.Text, cellValueKey: "template" },
                { name: this.translatePipe.transform("Stage"), sortKey: 4, type: TableColumnTypes.Status, cellValueKey: "status" },
                { name: this.translatePipe.transform("RiskLevel"), sortKey: 5, type: TableColumnTypes.Risk, cellValueKey: "assessmentRiskScore" },
                { name: this.translatePipe.transform("Deadline"), sortKey: 6, type: TableColumnTypes.Deadline, cellValueKey: "deadline" },
                { name: this.translatePipe.transform("Respondent"), sortKey: 7, type: TableColumnTypes.Text, cellValueKey: "respondent" },
                { name: "", sortKey: 7, type: TableColumnTypes.Action, cellValueKey: ""},
            ],
        };
    }

    onPaginationChange(event: number = 0): void {
        this.paginationParams = { ...this.paginationParams, page: event };
        this.getIncidentLinkedAssessments();
        this.loadingIncidentLinkedAssessmentRecords$.next(true);
    }
}
