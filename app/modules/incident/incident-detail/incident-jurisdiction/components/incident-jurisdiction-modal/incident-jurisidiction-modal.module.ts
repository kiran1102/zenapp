import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

// Modules
import { VitreusModule } from "@onetrust/vitreus";
import { IncidentCheckboxModule } from "incidentModule/shared/components/incident-checkbox/incident-checkbox.module";
import { OTPipesModule } from "modules/pipes/pipes.module";

// Components
import { IncidentJurisdictionModalComponent } from "incidentModule/incident-detail/incident-jurisdiction/components/incident-jurisdiction-modal/incident-jurisdiction-modal.component";

// Services
import { IncidentDetailService } from "incidentModule/incident-detail/shared/services/incident-detail.service";
import { IncidentJurisdictionService } from "incidentModule/incident-detail/shared/services/incident-jurisdiction.service";
import { IncidentJurisdictionApiService } from "incidentModule/incident-detail/shared/services/incident-jurisdiction-api.service";

@NgModule({
    imports: [
        CommonModule,
        OTPipesModule,
        VitreusModule,
        IncidentCheckboxModule,
    ],
    declarations: [
        IncidentJurisdictionModalComponent,
    ],
    entryComponents: [
        IncidentJurisdictionModalComponent,
    ],
    providers: [
        IncidentJurisdictionService,
        IncidentJurisdictionApiService,
        IncidentDetailService,
    ],
})
export class IncidentJurisdictionModalModule {}
