import { Component, OnInit, OnDestroy, Input } from "@angular/core";
import { IOtModalContent } from "@onetrust/vitreus";

// Interfaces
import { IIncidentJurisdiction, IJurisdiction, IJurisdictionModalData } from "incidentModule/shared/interfaces/incident-jurisdiction.interface";

// Services
import { IncidentJurisdictionService } from "incidentModule/incident-detail/shared/services/incident-jurisdiction.service";

// 3rd Party
import { Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";

interface ISelectedIncidentJurisdictions {
    [key: string]: IIncidentJurisdiction;
}

interface ISelectedRegion {
    id: number;
    name: string;
    countries: any[];
    jurisdictionId: null;
}

@Component({
    selector: "incident-jurisdiction-modal",
    templateUrl: "./incident-jurisdiction-modal.component.html",
})
export class IncidentJurisdictionModalComponent implements IOtModalContent, OnInit, OnDestroy {
    otModalData: IJurisdictionModalData;
    otModalCloseEvent: Subject<boolean>;
    loading = true;
    isSaving = false;
    searchModel = "";
    regions: IJurisdiction[];
    allSelected = false;
    atleastOneSelected = false;
    selectedRegion: ISelectedRegion;
    selectedJurisdictions: ISelectedIncidentJurisdictions = {};

    private destroy$ = new Subject();

    constructor(private incidentJurisdictionService: IncidentJurisdictionService) {}

    ngOnInit() {
        this.selectedJurisdictions = this.buildSelectedJurisdictions(this.otModalData.currentJurisdictions);
        this.incidentJurisdictionService.jurisdictionOptions$.pipe(
            takeUntil(this.destroy$),
        ).subscribe((jurisdictionOptions) => {
            if (jurisdictionOptions) {
                this.regions = jurisdictionOptions;
                this.selectRegion("all");
                this.loading = false;
            }
        });

        this.incidentJurisdictionService.retrieveJurisdictionOptions();
    }

    ngOnDestroy() {
        this.destroy$.next();
        this.destroy$.complete();
    }

    async onSave() {
        this.isSaving = true;
        const completeJurisdictions = this.buildCompleteJurisdicitons();
        if (await this.otModalData.saveJurisdictionCB(completeJurisdictions)) {
            this.isSaving = false;
            this.closeModal();
        } else {
            this.isSaving = false;
        }
    }

    closeModal() {
        this.otModalCloseEvent.next();
    }

    buildSelectedJurisdictions(incidentJurisdictions: IIncidentJurisdiction[]): ISelectedIncidentJurisdictions {
        const tempSelected = {};
        incidentJurisdictions.map((incidentJurisdiction) => {
            if (incidentJurisdiction) {
                tempSelected[incidentJurisdiction.jurisdictionId] = { ...incidentJurisdiction };
            }
        });

        return tempSelected;
    }

    buildCompleteJurisdicitons(): IIncidentJurisdiction[] {
        return Object.keys(this.selectedJurisdictions).map((key) => {
            return this.selectedJurisdictions[key];
        });
    }

    onSearch(value) {
        this.searchModel = value.trim().toLowerCase();

        if (this.selectedRegion && this.selectedRegion.id) {
            const currentRegion = this.regions.find((region) => region.id === this.selectedRegion.id);
            this.selectRegion(currentRegion);
        } else {
            this.selectRegion("all");
        }
    }

    filterSearch(region): any[] {
        const filteredRegion = region.countries.filter((country) => !this.searchModel
                || country.name.toLowerCase().includes(this.searchModel)
                || country.stateProvinces && country.stateProvinces.some((state) => state.name.toLowerCase().includes(this.searchModel)))
                .map((country) => {
                    const filteredCountry = { jurisdictionId: country.jurisdictionId, regionId: region.id, regionName: region.name, countryId: country.id, countryName: country.name, countryCode: country.code, stateProvinces: null, allStateProvinces: null };
                    if (country.stateProvinces && country.stateProvinces.length) {
                        const filteredProvinces = [];
                        const allProvinces = [];
                        country.stateProvinces.map((stateProvince) => {
                            if (!this.searchModel || stateProvince.name.toLowerCase().includes(this.searchModel)) {
                                filteredProvinces.push({ jurisdictionId: stateProvince.jurisdictionId, regionId: region.id, regionName: region.name, countryId: country.id, countryName: country.name, countryCode: country.code, stateProvinceId: stateProvince.id, stateProvinceName: stateProvince.name, stateProvinceCode: stateProvince.code });
                            }
                            allProvinces.push({ jurisdictionId: stateProvince.jurisdictionId, regionId: region.id, regionName: region.name, countryId: country.id, countryName: country.name, countryCode: country.code, stateProvinceId: stateProvince.id, stateProvinceName: stateProvince.name, stateProvinceCode: stateProvince.code });
                        });
                        filteredCountry.stateProvinces = filteredProvinces.length ? filteredProvinces : null;
                        filteredCountry.allStateProvinces = allProvinces.length ? allProvinces : null;
                    }
                    return filteredCountry;
                });
        return filteredRegion;
    }

    selectRegion(region) {
        if (region === "all" || !region) {
            this.selectedRegion = {
                id: 392029012,
                name: "all",
                countries: this.regions.map((currentRegion) => this.filterSearch(currentRegion))
                    .reduce((prev, next) => prev.concat(next), [] ),
                jurisdictionId: null,
            };
        } else {
            this.selectedRegion = {
                ...region,
                countries: this.filterSearch(region),
            };
        }

        this.buildSelectedRegionSelections();
    }

    extractSubLevel(element, onlyFilteredSubs = false): any[] {
        const extractFromRegion = (region) => {
            const subLevels = [];

            if (region.countries && region.countries.length) {
                region.countries.map((country) => {
                    if (onlyFilteredSubs && country.allStateProvinces && country.allStateProvinces.length) {
                        country.allStateProvinces.map((state) => {
                            if (state.jurisdictionId) {
                                subLevels.push(state);
                            }
                        });
                    } else if (!onlyFilteredSubs && country.stateProvinces && country.stateProvinces.length) {
                        country.stateProvinces.map((state) => {
                            if (state.jurisdictionId) {
                                subLevels.push(state);
                            }
                        });
                    } else {
                        if (country.jurisdictionId) {
                            subLevels.push(country);
                        }
                    }
                });
            }

            return subLevels;
        };

        const extractFromCountry = (countries) => {
            if (onlyFilteredSubs) {
                return countries.stateProvinces.map((state) => state);
            } else {
                return countries.allStateProvinces.map((state) => state);
            }
        };

        if (element.hasOwnProperty("countries")) {
            if (!element.countries || !element.countries.length) {
                return [element];
            }
            return extractFromRegion(element);
        } else if (element.hasOwnProperty("allStateProvinces")) {
            if (!element.allStateProvinces || !element.allStateProvinces.length && !element.allStateProvinces || !element.allStateProvinces.length) {
                return [element];
            }
            return extractFromCountry(element);
        } else {
            return [element];
        }
    }

    buildSelectedRegionSelections() {
        this.allSelected = true;
        this.atleastOneSelected = false;
        this.selectedRegion.countries.map((country) => {
            if (country.stateProvinces && country.stateProvinces.length) {
                const subLevels = this.extractSubLevel(country, true);
                const isAllSelected = subLevels.every((value) => !!this.selectedJurisdictions[value.jurisdictionId]);
                if (this.allSelected) {
                    this.allSelected = isAllSelected;
                }

                if (!this.atleastOneSelected) {
                    this.atleastOneSelected = subLevels.some((value) => !!this.selectedJurisdictions[value.jurisdictionId]);
                }
            } else {
                if (this.allSelected) {
                    this.allSelected = !!this.selectedJurisdictions[country.jurisdictionId];
                }

                if (!this.atleastOneSelected) {
                    this.atleastOneSelected = !!this.selectedJurisdictions[country.jurisdictionId];
                }
            }
        });
    }

    toggleAll() {
        const elements = this.extractSubLevel(this.selectedRegion);

        if (elements.every((value) => !!this.selectedJurisdictions[value.jurisdictionId])) {
            elements.map((el) => {
                delete this.selectedJurisdictions[el.jurisdictionId];
            });
            this.allSelected = false;
        } else {
            elements.map((el) => {
                if (!this.selectedJurisdictions[el.jurisdictionId]) {
                    this.selectedJurisdictions[el.jurisdictionId] = el;
                }
            });
            this.allSelected = true;
        }

        this.buildSelectedRegionSelections();
    }

    toggleElement(element, hasChildElements) {
        const elements = this.extractSubLevel(element);

        if (!hasChildElements) {
            elements.map((el) => {
                if (!!this.selectedJurisdictions[el.jurisdictionId]) {
                    delete this.selectedJurisdictions[el.jurisdictionId];
                } else {
                    this.selectedJurisdictions[el.jurisdictionId] = el;
                }
            });
        } else if (elements.every((value) => !!this.selectedJurisdictions[value.jurisdictionId])) {
            elements.map((el) => {
                delete this.selectedJurisdictions[el.jurisdictionId];
            });
        } else {
            elements.map((el) => {
                if (!this.selectedJurisdictions[el.jurisdictionId]) {
                    this.selectedJurisdictions[el.jurisdictionId] = el;
                }
            });
        }

        this.buildSelectedRegionSelections();
    }

    countryIsFilled(country): boolean {
        return country.allStateProvinces.every((stateProvince) =>
            !!this.selectedJurisdictions[stateProvince.jurisdictionId] );
    }

    countryIsPartialFilled(country): boolean {
        if (!country.stateProvinces) return false;

        return country.stateProvinces.some((stateProvince) =>
            !!this.selectedJurisdictions[stateProvince.jurisdictionId] );
    }
}
