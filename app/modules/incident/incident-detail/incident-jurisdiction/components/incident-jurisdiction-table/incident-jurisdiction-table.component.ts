// Core
import {
    Component,
    Input,
    Output,
    Inject,
    EventEmitter,
} from "@angular/core";

// Rxjs
import { Subject } from "rxjs";

// Services
import { Permissions } from "modules/shared/services/helper/permissions.service";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

// Interfaces
import { IDropdownOption } from "interfaces/one-dropdown.interface";
import { IDataTableColumn } from "interfaces/tables.interface";
import { IIncidentJurisdictionTable, IIncidentJurisdiction } from "incidentModule/shared/interfaces/incident-jurisdiction.interface";

// Enums / Constants
import { TableColumnTypes } from "enums/data-table.enum";

@Component({
    selector: "incident-jurisdiction-table",
    templateUrl: "./incident-jurisdiction-table.component.html",
})
export class IncidentJurisdictionTableComponent {
    @Input() table: IIncidentJurisdictionTable;
    @Input() loadingJurisdiction: boolean;
    @Output() callManageJurisdiction: EventEmitter<void> = new EventEmitter<void>();
    @Output() removeJurisdiction: EventEmitter<IIncidentJurisdiction> = new EventEmitter<IIncidentJurisdiction>();

    menuClass: string;
    resizableCol = false;
    responsiveTable = true;
    rowBordered = true;
    rowNoHover = false;
    rowStriped = false;
    selectedRowMap = {};
    tableColumnTypes = TableColumnTypes;
    truncateCellContent = true;
    wrapCellContent = false;

    private destroy$ = new Subject();

    constructor(
        @Inject("Export") readonly Export: any,
        private translatePipe: TranslatePipe,
        public permissions: Permissions,
    ) {}

    ngOnDestroy() {
        this.destroy$.next();
        this.destroy$.complete();
    }

    columnTrackBy(index: number, column: IDataTableColumn): string {
        return column.id;
    }

    getActions(jurisdiction): () => IDropdownOption[] {
        return () => [
            {
                text: this.translatePipe.transform("RemoveJurisdiction"),
                action: () => this.removeJurisdiction.emit(jurisdiction),
            },
        ];
    }
}
