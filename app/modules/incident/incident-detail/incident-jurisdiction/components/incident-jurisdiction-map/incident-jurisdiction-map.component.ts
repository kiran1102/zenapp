// Core
import {
    Component,
    AfterViewInit,
    Input,
} from "@angular/core";

// Rxjs
import { Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";

// Interfaces
import { IIncidentMapData } from "incidentModule/shared/interfaces/incident-jurisdiction-map.interface";
import { IIncidentJurisdiction } from "incidentModule/shared/interfaces/incident-jurisdiction.interface";

// Enums/Const
import { IncidentMapFillKey } from "incidentModule/shared/enums/incident-jurisdiction.enums";

// Services
import { Permissions } from "modules/shared/services/helper/permissions.service";
import { IncidentJurisdictionMapService } from "incidentModule/incident-detail/shared/services/incident-jurisdiction-map.service";
import { IncidentDetailService } from "incidentModule/incident-detail/shared/services/incident-detail.service";

@Component({
    selector: "incident-jurisdiction-map",
    templateUrl: "./incident-jurisdiction-map.component.html",
})
export class IncidentJurisdictionMapComponent implements AfterViewInit {
    mapId = "incident-jurisdiction-map";
    mapData: IIncidentMapData;

    private destroy$ = new Subject();

    constructor(
        public permissions: Permissions,
        private incidentDetailService: IncidentDetailService,
        private incidentJurisdictionMapService: IncidentJurisdictionMapService,
    ) {}

    ngAfterViewInit() {
        this.incidentDetailService.incidentDetailModel$.pipe(
            takeUntil(this.destroy$),
        ).subscribe((incidentDetail) => {
            if (incidentDetail) {
                this.mapData = this.massageMapData(incidentDetail.jurisdictions);
                this.incidentJurisdictionMapService.updateMapHighlights(this.mapData);
            }
        });

        this.drawMap();
        window.addEventListener("resize", this.handleMapResizeEvent);
    }

    ngOnDestroy() {
        this.destroy$.next();
        this.destroy$.complete();
        window.removeEventListener("resize", this.handleMapResizeEvent);
        this.incidentJurisdictionMapService.destroyMap();
    }

    massageMapData = (jurisdictions: IIncidentJurisdiction[]): IIncidentMapData => {
        const massaged: IIncidentMapData = {};
        jurisdictions.map((jurisdiction: IIncidentJurisdiction) => {
            if (jurisdiction.countryCode) {
                if (massaged[jurisdiction.countryCode]) {
                    massaged[jurisdiction.countryCode].states++;
                } else {
                    massaged[jurisdiction.countryCode] = { states: 1, fillKey: IncidentMapFillKey.SELECTED };
                }
            }
        });

        return massaged;
    }

    handleMapResizeEvent = () => {
        this.incidentJurisdictionMapService.resizeMap();
    }

    private drawMap() {
        this.incidentJurisdictionMapService.initMap(this.mapId, this.mapData);
    }
}
