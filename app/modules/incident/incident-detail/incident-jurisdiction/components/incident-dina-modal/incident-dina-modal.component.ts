// Core / Angular
import {
    Component,
    OnInit,
    OnDestroy,
} from "@angular/core";
import { IOtModalContent } from "@onetrust/vitreus";
import {
    FormControl,
    FormGroup,
    FormBuilder,
    Validators,
} from "@angular/forms";

// Services
import { IncidentJurisdictionService } from "incidentModule/incident-detail/shared/services/incident-jurisdiction.service";
import { IncidentDataBreachPediaService } from "incidentModule/incident-databreachpedia/shared/services/incident-databreachpedia.service";
import { IncidentDetailService } from "incidentModule/incident-detail/shared/services/incident-detail.service";

// 3rd Party
import { Subject } from "rxjs";
import { takeUntil, filter } from "rxjs/operators";

// Interfaces
import { IIncidentJurisdiction, IDINATemplateDetail } from "incidentModule/shared/interfaces/incident-jurisdiction.interface";
import { IIncidentDBPLaw } from "incidentModule/shared/interfaces/incident-databreachpedia-view.interface";
import { IDINAAssessmentRequest } from "incidentModule/shared/interfaces/incident-jurisdiction.interface";
import { IIncidentDetail } from "incidentModule/shared/interfaces/incident-detail-state.interface";
import { IIncidentAssessmentLink } from "incidentModule/shared/interfaces/incident-create-state.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";

// Enums / Constants
import { NO_BLANK_SPACE } from "constants/regex.constant";

@Component({
    selector: "incident-dina-modal",
    templateUrl: "./incident-dina-modal.component.html",
})
export class IncidentDINAModalComponent implements IOtModalContent, OnInit, OnDestroy {
    otModalData: IIncidentDetail;
    otModalCloseEvent: Subject<boolean>;
    incidentCreateDINAForm: FormGroup = this.formBuilder.group({
        dinaName: new FormControl("", [
            Validators.required,
            Validators.maxLength(250),
            Validators.pattern(NO_BLANK_SPACE),
        ]),
    });
    isLoading = false;
    template: IDINATemplateDetail;
    breachLaws: string[] = [];
    private destroy$ = new Subject();

    constructor(
        private incidentJurisdictionService: IncidentJurisdictionService,
        private incidentDataBreachPediaService: IncidentDataBreachPediaService,
        private incidentDetailService: IncidentDetailService,
        private formBuilder: FormBuilder,
    ) { }

    ngOnInit() {
        this.getLaws();
        this.fetchDinaTemplate();
    }

    ngOnDestroy() {
        this.destroy$.next();
        this.destroy$.complete();
    }

    closeModal() {
        this.otModalCloseEvent.next();
    }

    fetchDinaTemplate() {
        this.incidentJurisdictionService.getDINATemplate()
            .pipe(
                takeUntil(this.destroy$),
                filter((response: IDINATemplateDetail[]) => response != null),
            ).subscribe((response: IDINATemplateDetail[]) => {
                this.template = response[0];
            });
    }

    getLaws() {
        const jurisdictionIds = this.otModalData.jurisdictions.map((jurisdiction: IIncidentJurisdiction) => jurisdiction.jurisdictionId);
        this.incidentDataBreachPediaService.getListOfDatabreachLaws(jurisdictionIds)
            .pipe(
                takeUntil(this.destroy$),
                filter((response: IIncidentDBPLaw[]) => Boolean(response.length)),
            ).subscribe((response: IIncidentDBPLaw[]) => {
                this.breachLaws = response.map((law: IIncidentDBPLaw) => law.name);
            });
    }

    onLaunch(event?: KeyboardEvent) {
        if (event) event.preventDefault();
        this.isLoading = true;
        const assessmentDetails: IDINAAssessmentRequest = {
            name: this.incidentCreateDINAForm.get("dinaName").value,
            laws: this.breachLaws,
            orgGroupId: this.otModalData.orgGroupId,
            orgGroupName: this.otModalData.orgGroupName,
            templateId: this.template.id,
            templateType: this.template.templateType,
        };
        this.incidentJurisdictionService.createDINA(assessmentDetails)
            .pipe(
                takeUntil(this.destroy$),
                filter((response: IProtocolResponse<string>) => response != null),
            ).subscribe((response: IProtocolResponse<string>) => {
                const assessmentId = response.data;
                if (assessmentId) {
                    this.linkAssessmentToIncident(assessmentId);
                }
                this.isLoading = false;
                this.closeModal();
            });
    }

    linkAssessmentToIncident(assessmentId: string) {
        const linkAssessmentRequest: IIncidentAssessmentLink = {
            assessmentIds: [assessmentId],
            incidentId: this.otModalData.id,
            incidentName: this.otModalData.name,
        };
        this.incidentDetailService.createIncidentAssessmentsLink(linkAssessmentRequest);
    }
}
