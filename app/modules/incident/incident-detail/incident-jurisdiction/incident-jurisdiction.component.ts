import {
    Component,
    ViewChild,
    TemplateRef,
} from "@angular/core";
import {
    OtModalService,
    ModalSize,
    ConfirmModalType,
} from "@onetrust/vitreus";
import { IncidentJurisdictionModalComponent } from "incidentModule/incident-detail/incident-jurisdiction/components/incident-jurisdiction-modal/incident-jurisdiction-modal.component";
import { StateService } from "@uirouter/core";

// Interfaces
import { IIncidentJurisdictionTable, IIncidentJurisdiction } from "incidentModule/shared/interfaces/incident-jurisdiction.interface";

// Services
import { IncidentDetailService } from "incidentModule/incident-detail/shared/services/incident-detail.service";
import { IncidentJurisdictionService } from "incidentModule/incident-detail/shared/services/incident-jurisdiction.service";

// 3rd Party
import { Subject } from "rxjs";
import { takeUntil, filter } from "rxjs/operators";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

// Enums
import { TableColumnTypes } from "enums/data-table.enum";
import { IncidentJurisdictionTabOptions, IncidentJurisdictionTableColumnNames } from "incidentModule/shared/enums/incident-jurisdiction.enums";
import { IIncidentDetail } from "incidentModule/shared/interfaces/incident-detail-state.interface";
import { IncidentDINAModalComponent } from "./components/incident-dina-modal/incident-dina-modal.component";

@Component({
    selector: "incident-jurisdiction",
    templateUrl: "./incident-jurisdiction.component.html",
})
export class IncidentJurisdictionComponent {
    @ViewChild("IncidentJurisdictionModal") fileModalRef: TemplateRef<any>;
    defaultPagination = { page: 0, size: 10 };
    paginationParams = this.defaultPagination;
    currentJurisdictions: IIncidentJurisdiction[];
    currentTab = IncidentJurisdictionTabOptions.MapView;
    incidentDetail: IIncidentDetail;
    incidentJurisdictionTabOptions = IncidentJurisdictionTabOptions;
    loadingJurisdiction = true;
    table: IIncidentJurisdictionTable;

    private destroy$ = new Subject();

    constructor(
        private stateService: StateService,
        private incidentDetailService: IncidentDetailService,
        private incidentJurisdictionService: IncidentJurisdictionService,
        private translatePipe: TranslatePipe,
        private otModalService: OtModalService,
    ) { }

    ngOnInit() {
        this.incidentDetailService.incidentDetailModel$.pipe(
            takeUntil(this.destroy$),
        ).subscribe((incidentDetail: IIncidentDetail) => {
            this.currentJurisdictions = incidentDetail.jurisdictions;
            this.incidentDetail = incidentDetail;
            this.loadingJurisdiction = true;
            if (this.currentJurisdictions) {
                this.buildTableItems();
            }
        });
    }

    ngOnDestroy() {
        this.incidentJurisdictionService.reset();
        this.destroy$.next();
        this.destroy$.complete();
    }

    changeTab(tab: IncidentJurisdictionTabOptions.TableView) {
        this.currentTab = tab;
    }

    onManage() {
        this.otModalService.create(
            IncidentJurisdictionModalComponent,
            {
                isComponent: true,
                size: ModalSize.MEDIUM,
            },
            { saveJurisdictionCB: this.saveJurisdictions.bind(this), currentJurisdictions: this.currentJurisdictions },
        ).pipe(
            takeUntil(this.destroy$),
        );
    }

    saveJurisdictions(jurisdictions): Promise<boolean> {
        return new Promise((resolve) => {
            this.incidentJurisdictionService.updateJurisdictions(this.stateService.params.id, jurisdictions)
                .then(() => {
                    resolve(true);
                }).catch(() => {
                    resolve(false);
                });
        });
    }

    buildTableItems() {
        // Hanlding pagination locally here since we're in a scenario where we need all jurisdictions, but we don't need them all rendered on the table at once
        const indices = [(((this.paginationParams.page + 1) * this.paginationParams.size) - this.paginationParams.size), ((this.paginationParams.page + 1) * this.paginationParams.size)];
        const paginatedJurisdictions = [];
        this.currentJurisdictions.map((currentJurisdiction, index) => {
            if (index >= indices[0] && index < indices[1]) {
                paginatedJurisdictions.push(currentJurisdiction);
            }
        });

        this.table = this.configureTable(paginatedJurisdictions);
        this.table.metaData = {
            first: this.paginationParams.page === 0,
            last: this.paginationParams.page !== 0 && paginatedJurisdictions.length < this.paginationParams.size,
            number: this.paginationParams.page,
            numberOfElements: paginatedJurisdictions.length,
            size: this.paginationParams.size,
            totalElements: this.currentJurisdictions.length,
            totalPages: Math.ceil(this.currentJurisdictions.length / this.paginationParams.size),
        };

        // Fallback to decrease the page count if there are not elements on it
        if (this.table.metaData.numberOfElements === 0 && this.paginationParams.page > 0) {
            this.paginationParams.page--;
            this.buildTableItems();
        } else {
            this.loadingJurisdiction = false;
        }
    }

    configureTable(records: any[]): IIncidentJurisdictionTable {
        return {
            rows: records,
            columns: [
                {
                    name: this.translatePipe.transform("Region"),
                    sortKey: IncidentJurisdictionTableColumnNames.REGION,
                    type: TableColumnTypes.Text,
                    cellValueKey: IncidentJurisdictionTableColumnNames.REGION,
                },
                {
                    name: this.translatePipe.transform("Country"),
                    sortKey: IncidentJurisdictionTableColumnNames.COUNTRY,
                    type: TableColumnTypes.Text,
                    cellValueKey: IncidentJurisdictionTableColumnNames.COUNTRY,
                },
                {
                    name: this.translatePipe.transform("StateProvince"),
                    sortKey: IncidentJurisdictionTableColumnNames.STATE,
                    type: TableColumnTypes.Text,
                    cellValueKey: IncidentJurisdictionTableColumnNames.STATE,
                },
                {
                    name: this.translatePipe.transform("DateAdded"),
                    sortKey: IncidentJurisdictionTableColumnNames.DATE_ADDED,
                    type: TableColumnTypes.Date,
                    cellValueKey: IncidentJurisdictionTableColumnNames.DATE_ADDED,
                },
                {
                    name: "",
                    type: TableColumnTypes.Action,
                    cellValueKey: "",
                },
            ],
        };
    }

    removeJurisdiction(jurisdiction: IIncidentJurisdiction) {
        this.otModalService
            .confirm({
                type: ConfirmModalType.DELETE,
                translations: {
                    title: this.translatePipe.transform("RemoveJurisdiction"),
                    desc: this.translatePipe.transform("AreYouSureRemoveJurisdiction"),
                    confirm: this.translatePipe.transform("DoYouWantToContinue"),
                    submit: this.translatePipe.transform("Remove"),
                    cancel: this.translatePipe.transform("Cancel"),
                },
                hideClose: false,
            }).pipe(
                takeUntil(this.destroy$),
            ).subscribe((res) => {
                if (res) {
                    this.loadingJurisdiction = true;
                    this.incidentJurisdictionService.removeJurisdiction(this.stateService.params.id, jurisdiction);
                }
            });
    }

    onPaginationChange(event: number = 0) {
        this.loadingJurisdiction = true;
        this.paginationParams = { ...this.paginationParams, page: event };
        this.buildTableItems();
    }

    onLaunchDINA() {
        this.otModalService.create(
            IncidentDINAModalComponent,
            {
                isComponent: true,
            },
            this.incidentDetail,
        );
    }
}
