import { NgModule } from "@angular/core";

// Modules
import { SharedModule } from "sharedModules/shared.module";
import { UpgradedLegacyModule } from "sharedModules/upgrade/upgraded-legacy.module";
import { CommonModule } from "@angular/common";
import { HttpClientModule } from "@angular/common/http";
import { FormsModule } from "@angular/forms";
import { ReactiveFormsModule } from "@angular/forms";
import { VitreusModule } from "@onetrust/vitreus";
import { DndListModule } from "ngx-drag-and-drop-lists";
import { OTPipesModule, PIPES } from "modules/pipes/pipes.module";
import { VirtualScrollModule } from "od-virtualscroll";
import { OneVideoModule } from "modules/video/video.module";
import { IncidentSharedModule } from "incidentModule/shared/incident-shared.module";
import { IncidentJurisdictionModalModule } from "incidentModule/incident-detail/incident-jurisdiction/components/incident-jurisdiction-modal/incident-jurisidiction-modal.module";
import { TranslateModule } from "@onetrust/common";

// Components
import { IncidentDetailComponent } from "incidentModule/incident-detail/incident-detail.component";
import { IncidentDetailsComponent } from "incidentModule/incident-detail/incident-details/incident-details.component";
import { IncidentAssessmentComponent } from "incidentModule/incident-detail/incident-assessment/incident-assessment.component";
import { IncidentAssessmentTableComponent } from "incidentModule/incident-detail/incident-assessment/components/incident-assessment-table/incident-assessment-table.component";
import { IncidentActivityComponent } from "incidentModule/incident-detail/incident-activity/incident-activity.component";
import { IncidentSubtasksComponent } from "incidentModule/incident-detail/incident-subtasks/incident-subtasks.component";
import { IncidentSubtasksTableComponent } from "incidentModule/incident-detail/incident-subtasks/components/incident-subtasks-table/incident-subtasks-table.component";
import { IncidentPathComponent } from "incidentModule/incident-detail/incident-path/incident-path.component";
import { IncidentAttachmentsComponent } from "incidentModule/incident-detail/incident-attachments/incident-attachments.component";
import { IncidentAttachmentsTableComponent } from "incidentModule/incident-detail/incident-attachments/components/incident-attachments-table/incident-attachments-table.component";
import { IncidentLinkAssessmentModalComponent } from "incidentModule/incident-detail/incident-assessment/components/incident-link-assessment-modal/incident-link-assessment-modal.component";
import { IncidentAddEditSubTasksModalComponent } from "incidentModule/incident-detail/incident-subtasks/components/incident-add-edit-subtasks-modal/incident-add-edit-subtasks-modal.component";
import { IncidentAttachmentsFileModalComponent } from "incidentModule/incident-detail/incident-attachments/components/incident-attachments-file-modal/incident-attachments-file-modal.component";
import { IncidentAttachmentsLinkModalComponent } from "incidentModule/incident-detail/incident-attachments/components/incident-attachments-link-modal/incident-attachments-link-modal.component";
import { IncidentCardsComponent } from "incidentModule/incident-detail/incident-cards/incident-cards.component";
import { IncidentJurisdictionComponent } from "incidentModule/incident-detail/incident-jurisdiction/incident-jurisdiction.component";
import { IncidentJurisdictionTableComponent } from "incidentModule/incident-detail/incident-jurisdiction/components/incident-jurisdiction-table/incident-jurisdiction-table.component";
import { IncidentJurisdictionMapComponent } from "incidentModule/incident-detail/incident-jurisdiction/components/incident-jurisdiction-map/incident-jurisdiction-map.component";
import { IncidentDINAModalComponent } from "incidentModule/incident-detail/incident-jurisdiction/components/incident-dina-modal/incident-dina-modal.component";

// Services
import { IncidentSubTasksService } from "incidentModule/incident-detail/shared/services/incident-subtasks.service";
import { IncidentSubtasksApiService } from "incidentModule/incident-detail/shared/services/incident-subtasks-api.service";
import { IncidentDetailService } from "incidentModule/incident-detail/shared/services/incident-detail.service";
import { IncidentAttachmentService } from "incidentModule/incident-detail/shared/services/incident-attachment.service";
import { IncidentAttachmentsApiService } from "incidentModule/incident-detail/shared/services/incident-attachment-api.service";
import { IncidentJurisdictionService } from "incidentModule/incident-detail/shared/services/incident-jurisdiction.service";
import { IncidentJurisdictionApiService } from "incidentModule/incident-detail/shared/services/incident-jurisdiction-api.service";
import { IncidentJurisdictionMapService } from "incidentModule/incident-detail/shared/services/incident-jurisdiction-map.service";

@NgModule({
    imports: [
        SharedModule,
        UpgradedLegacyModule,
        CommonModule,
        HttpClientModule,
        TranslateModule,
        FormsModule,
        ReactiveFormsModule,
        VitreusModule,
        DndListModule,
        OTPipesModule,
        VirtualScrollModule,
        OneVideoModule,
        IncidentSharedModule,
        IncidentJurisdictionModalModule,
    ],
    declarations: [
        IncidentAssessmentTableComponent,
        IncidentDINAModalComponent,
        IncidentDetailsComponent,
        IncidentSubtasksTableComponent,
        IncidentAttachmentsTableComponent,
        IncidentDetailComponent,
        IncidentLinkAssessmentModalComponent,
        IncidentAttachmentsFileModalComponent,
        IncidentAddEditSubTasksModalComponent,
        IncidentAssessmentComponent,
        IncidentActivityComponent,
        IncidentSubtasksComponent,
        IncidentPathComponent,
        IncidentAttachmentsComponent,
        IncidentAttachmentsLinkModalComponent,
        IncidentCardsComponent,
        IncidentJurisdictionComponent,
        IncidentJurisdictionTableComponent,
        IncidentJurisdictionMapComponent,
    ],
    exports: [
        IncidentDetailComponent,
    ],
    entryComponents: [
        IncidentDetailComponent,
        IncidentLinkAssessmentModalComponent,
        IncidentDINAModalComponent,
        IncidentAttachmentsFileModalComponent,
        IncidentAddEditSubTasksModalComponent,
        IncidentAttachmentsLinkModalComponent,
    ],
    providers: [
        IncidentAttachmentService,
        IncidentAttachmentsApiService,
        IncidentDetailService,
        IncidentSubTasksService,
        IncidentSubtasksApiService,
        IncidentJurisdictionService,
        IncidentJurisdictionApiService,
        IncidentJurisdictionMapService,
        PIPES, // TODO: Remove from providers when ot-common is implemented. Only necessary if you are using pipe in TS file, else just module is fine
    ],
})
export class IncidentDetailModule {}
