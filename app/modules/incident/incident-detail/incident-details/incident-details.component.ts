// Core
import {
    Component, Input,
} from "@angular/core";

// Interfaces
import { IIncidentDetail } from "incidentModule/shared/interfaces/incident-detail-state.interface";

// Constants / Enums
import { IncidentSourceTypes } from "incidentModule/shared/enums/incident-details.enums";

@Component({
    selector: "incident-details",
    templateUrl: "./incident-details.component.html",
})
export class IncidentDetailsComponent {

    @Input() model: IIncidentDetail;
    sourceTypes = IncidentSourceTypes;
}
