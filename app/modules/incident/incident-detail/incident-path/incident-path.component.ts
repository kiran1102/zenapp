// Core
import {
    Component,
    Input,
    TemplateRef,
    ViewChild,
} from "@angular/core";

// Vitreus
import { OtModalService } from "@onetrust/vitreus";

// Rxjs
import {
    BehaviorSubject,
    Subject,
} from "rxjs";
import { takeUntil } from "rxjs/operators";

// Services
import { IncidentDetailService } from "incidentModule/incident-detail/shared/services/incident-detail.service";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

// Interfaces / Enums
import { IIncidentPath, IStagePathItem } from "incidentModule/shared/interfaces/incident-path.interface";

@Component({
    selector: "incident-path",
    templateUrl: "./incident-path.component.html",
})
export class IncidentPathComponent {
    @Input() incidentId: string;
    @ViewChild("IncidentPathConfirmModal") contentRef: TemplateRef<any>;
    modalCloseEvent: Subject<string> = new Subject();
    loadingIncidentPath$ = new BehaviorSubject<boolean> (true);
    pathItems: IStagePathItem[];
    activeIndex: number;
    triggerRules: boolean;
    confirmIcon = "warning";
    private destroy$ = new Subject();
    private selectedStage: IStagePathItem;

    constructor(
        private incidentDetailService: IncidentDetailService,
        private translatePipe: TranslatePipe,
        private otModalService: OtModalService,
    ) { }

    ngOnInit(): void {
        this.incidentDetailService.incidentStagePath$
        .pipe(takeUntil(this.destroy$))
        .subscribe((stagePath: IIncidentPath) => {
            this.pathItems = stagePath.pathItems.map((pathItem) => {
                return {
                    ...pathItem,
                    label: this.translatePipe.transform(pathItem.label),
                };
            });
            this.activeIndex = stagePath.activeIndex;
            this.loadingIncidentPath$.next(false);
        });
    }

    ngOnDestroy(): void {
        this.closeModal();
        this.modalCloseEvent.unsubscribe();
        this.destroy$.next();
        this.destroy$.complete();
    }

    generateCustomConfirmModal() {
        this.modalCloseEvent = this.otModalService
            .create(
                this.contentRef,
                null,
            );
    }

    setTriggerRules(trigger): void {
        this.triggerRules = trigger;
    }

    onStagePathChange(event: number): void {
        this.selectedStage = this.pathItems[event];
        if (event !== this.activeIndex) {
            if (this.incidentDetailService.hasVisitedStage(this.selectedStage.id)) {
                this.triggerRules = null;
                this.generateCustomConfirmModal();
            } else {
                this.changeStage(event);
            }
        }
    }

    closeModal(): void {
        this.selectedStage = null;
        this.triggerRules = null;
        this.modalCloseEvent.next();
    }

    updateStage() {
        this.incidentDetailService.updateStagePath(this.incidentId, this.selectedStage.id, this.triggerRules);
        this.closeModal();
    }

    changeStage(event: number) {
        this.activeIndex = event;
        this.incidentDetailService.updateStagePath(this.incidentId, this.selectedStage.id);
    }
}
