// Core
import {
    Component,
    Inject,
} from "@angular/core";
import { StateService } from "@uirouter/core";

// Components
import { IncidentAddEditModalComponent } from "incidentModule/shared/components/incident-add-edit-modal/incident-add-edit-modal.component";

// Rxjs
import {
    Subject,
    BehaviorSubject,
    combineLatest,
} from "rxjs";

import { takeUntil } from "rxjs/operators";

// Redux
import { getCurrentOrgId } from "oneRedux/reducers/org.reducer";
import { StoreToken } from "tokens/redux-store.token";
import { getCurrentUser } from "oneRedux/reducers/user.reducer";

// Services
import { IncidentDetailService } from "incidentModule/incident-detail/shared/services/incident-detail.service";
import { OtModalService } from "@onetrust/vitreus";
import { Permissions } from "modules/shared/services/helper/permissions.service";
import { UserActionService } from "oneServices/actions/user-action.service";

// Interfaces
import { IStore } from "interfaces/redux.interface";
import { IIncidentDetail } from "incidentModule/shared/interfaces/incident-detail-state.interface";
import { IIncidentAddEditModalData } from "incidentModule/shared/interfaces/incident-add-edit-modal.interface";
import { IIncidentTypeRecord } from "incidentModule/shared/interfaces/incident-type-state.interface";

// Constants / Enums
import { IncidentTabOptions } from "incidentModule/shared/enums/incident-details.enums";
import { IncidentStageBadgeColorMap } from "incidentModule/shared/constants/incident-badge-color.constants";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

@Component({
    selector: "incident-detail",
    templateUrl: "./incident-detail.component.html",
})
export class IncidentDetailComponent {
    loadingIncidentDetails$ = new BehaviorSubject<boolean> (true);
    currentTab = IncidentTabOptions.Details;
    incidentId = this.stateService.params.id;
    incidentTabOptions = IncidentTabOptions;
    model: IIncidentDetail;
    showEditIncident = false;
    showMore = false;
    tabOptions = [];
    overflowText = "More";
    activeIndex = 0;
    buttonText: string;
    buttonDisabled: boolean;
    buttonLoading: boolean;
    stageTypes: IIncidentTypeRecord[];
    incidentBadgeColorMap = IncidentStageBadgeColorMap;

    private destroy$ = new Subject();

    constructor(
        @Inject(StoreToken) private store: IStore,
        private stateService: StateService,
        private incidentDetailService: IncidentDetailService,
        private translatePipe: TranslatePipe,
        private otModalService: OtModalService,
        public permissions: Permissions,
        private userAction: UserActionService,
    ) { }

    ngOnInit(): void {
        this.userAction.fetchUsers();

        combineLatest(this.incidentDetailService.incidentDetailModel$, this.incidentDetailService.stageNameTypes$).pipe(
            takeUntil(this.destroy$),
        ).subscribe(([model, stageTypes]) => {
            if (model && stageTypes) {
                this.model = model;
                this.showEditIncident = this.canEditIncident(model.assigneeId);
                this.stageTypes = stageTypes;
                this.incidentDetailService.setStagePath();
                if (this.currentTab === IncidentTabOptions.Activity) {
                    this.incidentDetailService.retrieveIncidentActivity(this.incidentId);
                }
                this.loadingIncidentDetails$.next(false);
            }
        });

        this.incidentDetailService.retrieveIncidentById(this.incidentId);
        this.incidentDetailService.retrieveIncidentTypes();
        this.incidentDetailService.retrieveStageNameTypes();
        this.setTabOptions();
    }

    ngOnDestroy(): void {
        this.destroy$.next();
        this.destroy$.complete();
        this.incidentDetailService.reset();
    }

    refreshIncidentDetail(): void {
        this.incidentDetailService.retrieveIncidentById(this.incidentId);
    }

    changeTab(option: IncidentTabOptions): void {
        this.currentTab = option;
    }

    goToRoute(route: { path: string, params: any }): void {
        this.stateService.go(route.path);
    }

    onEditIncident(): void {
        const modalData: IIncidentAddEditModalData = {
            parentId: getCurrentOrgId(this.store.getState()),
            incidentDetail: this.model,
            incidentRegisterLength: null,
            actionButtonTitle: "Save",
            cancelButtonTitle: "Cancel",
            modalTitle: "EditIncident",
            callback: () => {
                this.refreshIncidentDetail();
            },
        };
        this.otModalService
            .create(
                IncidentAddEditModalComponent,
                {
                    isComponent: true,
                },
                modalData,
            ).pipe(
                takeUntil(this.destroy$),
            );
    }

    setTabOptions(): void {
        this.tabOptions.push({
            text: this.translatePipe.transform("Details"),
            id: IncidentTabOptions.Details,
            otAutoId: "IncidentDetailTabDetails",
        });

        this.tabOptions.push({
            text: this.translatePipe.transform("Activity"),
            id: IncidentTabOptions.Activity,
            otAutoId: "IncidentDetailTabActivity",
        });

        this.tabOptions.push({
            text: this.translatePipe.transform("SubTasks"),
            id: IncidentTabOptions.SubTasks,
            otAutoId: "IncidentDetailTabSubTasks",
        });

        this.tabOptions.push({
            text: this.translatePipe.transform("Jurisdictions"),
            id: IncidentTabOptions.Jurisdiction,
            otAutoId: "IncidentDetailTabJurisdiction",
        });

        this.tabOptions.push({
            text: this.translatePipe.transform("Assessments"),
            id: IncidentTabOptions.Assessments,
            otAutoId: "IncidentDetailTabAssessment",
        });

        this.tabOptions.push({
            text: this.translatePipe.transform("Attachments"),
            id: IncidentTabOptions.Attachments,
            otAutoId: "IncidentDetailTabAttachments",
        });

    }

    canEditIncident(assigneeId: string): boolean {
        return this.permissions.canShow("IncidentRecordsView")
            || getCurrentUser(this.store.getState()).Id === assigneeId;
    }
}
