import {
    Component,
    Input,
} from "@angular/core";

// Components
import { IncidentAddEditSubTasksModalComponent } from "incidentModule/incident-detail/incident-subtasks/components/incident-add-edit-subtasks-modal/incident-add-edit-subtasks-modal.component";

// Interfaces
import { IIncidentDetail } from "incidentModule/shared/interfaces/incident-detail-state.interface";
import { IIncidentSubTasksRecord, IIncidentSubTasksRecordTable } from "incidentModule/shared/interfaces/incident-subtasks.interface";
import { IIncidentAddEditSubTasksModalData } from "incidentModule/shared/interfaces/incident-modal.interface";
import { IPageableMeta } from "interfaces/pagination.interface";

// Services
import { OtModalService } from "@onetrust/vitreus";
import { IncidentSubTasksService } from "incidentModule/incident-detail/shared/services/incident-subtasks.service";
import { IncidentDetailService } from "incidentModule/incident-detail/shared/services/incident-detail.service";

// 3rd Party
import { Subject, BehaviorSubject } from "rxjs";
import { takeUntil } from "rxjs/operators";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

// Enums
import { TableColumnTypes } from "enums/data-table.enum";
import { IncidentTableColumnNames } from "incidentModule/shared/enums/incident-actions.enums";

@Component({
    selector: "incident-subtasks",
    templateUrl: "./incident-subtasks.component.html",
})
export class IncidentSubtasksComponent {
    @Input() model: IIncidentDetail;
    incidentSubTasksRecords: IIncidentSubTasksRecord[];
    loadingIncidentSubTasksRecords$ = new BehaviorSubject<boolean>(true);
    showAddSubtask = true;
    defaultPagination = { page: 0, size: 10};
    paginationParams = this.defaultPagination;
    searchText = "";
    table: IIncidentSubTasksRecordTable;
    private destroy$ = new Subject();

    constructor(
        private incidentSubTasksService: IncidentSubTasksService,
        private incidentDetailService: IncidentDetailService,
        private otModalService: OtModalService,
        private translatePipe: TranslatePipe,
    ) { }

    ngOnInit(): void {
        this.incidentSubTasksService.incidentSubTasksList$.pipe(
            takeUntil(this.destroy$),
        ).subscribe((records: IIncidentSubTasksRecord[]) => {
            if (records) {
                this.table = this.configureTable(records);
                this.loadingIncidentSubTasksRecords$.next(false);
            }
        });

        this.incidentSubTasksService.incidentSubTasksRecordMetaData$.pipe(
            takeUntil(this.destroy$),
        ).subscribe((metaData: IPageableMeta) => {
            if (metaData) {
                this.table.metaData = metaData;
            }
        });

        this.incidentDetailService.incidentDetailModel$.pipe(
            takeUntil(this.destroy$),
        ).subscribe(() => {
            this.loadingIncidentSubTasksRecords$.next(true);
            this.getIncidentSubTasks();
        });
        this.getIncidentSubTasks();
    }

    ngOnDestroy(): void {
        this.destroy$.next();
        this.destroy$.complete();
        this.incidentSubTasksService.reset();
    }

    getIncidentSubTasks(): void {
        this.incidentSubTasksService.getSubTasksByIncidentId(this.model.id, this.paginationParams);
    }

    onAddSubTask(): void {
        const modalData: IIncidentAddEditSubTasksModalData = {
            incidentSubTasksModel: null,
            incidentId: this.model.id,
            orgGroupId: this.model.orgGroupId,
            modalTitle: "AddSubTask",
            actionButtonTitle: "Add",
            cancelButtonTitle: "Cancel",
            editSubTaskFlag: true,
            callback: (() => {
                this.getIncidentSubTasks();
            }),
        };
        this.otModalService
            .create(
                IncidentAddEditSubTasksModalComponent,
                {
                    isComponent: true,
                },
                modalData,
            ).pipe(
                takeUntil(this.destroy$),
            );
    }

    configureTable(records: IIncidentSubTasksRecord[]): IIncidentSubTasksRecordTable {
        return {
            rows: records,
            columns: [
                {
                    name: this.translatePipe.transform("Name"),
                    sortKey: IncidentTableColumnNames.INCIDENT_NAME_COLUMN,
                    type: TableColumnTypes.Link,
                    cellValueKey: IncidentTableColumnNames.INCIDENT_NAME_COLUMN,
                },
                {
                    name: this.translatePipe.transform("Status"),
                    sortKey: IncidentTableColumnNames.INCIDENT_STATUS_NAME,
                    type: TableColumnTypes.Status,
                    cellValueKey: IncidentTableColumnNames.INCIDENT_STATUS_NAME,
                },
                {
                    name: this.translatePipe.transform("Deadline"),
                    sortKey: IncidentTableColumnNames.INCIDENT_DEADLINE_COLUMN,
                    type: TableColumnTypes.Deadline,
                    cellValueKey: IncidentTableColumnNames.INCIDENT_DEADLINE_COLUMN,
                },
                {
                    name: this.translatePipe.transform("Assignee"),
                    sortKey: IncidentTableColumnNames.INCIDENT_ASSIGNEE_NAME_COLUMN,
                    type: TableColumnTypes.AssigneeName,
                    cellValueKey: IncidentTableColumnNames.INCIDENT_ASSIGNEE_NAME_COLUMN,
                },
                {
                    name: "",
                    sortKey: IncidentTableColumnNames.INCIDENT_SUBTASK_ACTION,
                    type: TableColumnTypes.Action,
                    cellValueKey: IncidentTableColumnNames.INCIDENT_SUBTASK_ACTION,
                },
            ],
        };
    }

    onPaginationChange(event: number = 0): void {
        this.paginationParams = { ...this.paginationParams, page: event };
        this.incidentSubTasksService.getSubTasksByIncidentId(this.model.id, this.paginationParams);
        this.loadingIncidentSubTasksRecords$.next(true);
    }
}
