// Core
import {
    Component,
    Inject,
    OnInit,
} from "@angular/core";
import {
    FormGroup,
    FormBuilder,
} from "@angular/forms";
import { IOtModalContent } from "@onetrust/vitreus";

// 3rd Party
import { BehaviorSubject, Subject } from "rxjs";

// Services
import { TimeStamp } from "oneServices/time-stamp.service";
import { IncidentSubTasksService } from "incidentModule/incident-detail/shared/services/incident-subtasks.service";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

// Tokens
import { StoreToken } from "tokens/redux-store.token";

// Interfaces
import { IProtocolResponse } from "interfaces/protocol.interface";
import { IStore } from "interfaces/redux.interface";
import { IOrgUserAdapted } from "interfaces/org-user.interface";
import { IOtOrgUserOutput } from "sharedModules/components/ot-org-user/ot-org-user.interface";
import { IIncidentSubTasksRecord } from "incidentModule/shared/interfaces/incident-subtasks.interface";
import {
    IIncidentAddEditSubTasksModalData,
    IIncidentAddEditSubTasksCreateRequest,
    IIncidentAddEditSubTasksUpdateRequest,
} from "incidentModule/shared/interfaces/incident-modal.interface";
import { IDateTimeOutputModel } from "@onetrust/vitreus";
import { ILabelValue } from "@onetrust/vitreus";

// Enums
import { OrgUserTraversal } from "enums/org-user-dropdown.enum";
import { IncidentSubTasksStateKeys } from "incidentModule/shared/enums/incident-translation-keys.enums";

@Component({
    selector: "incident-add-edit-subtasks-modal",
    templateUrl: "./incident-add-edit-subtasks-modal.component.html",
})

export class IncidentAddEditSubTasksModalComponent implements IOtModalContent {
    otModalCloseEvent: Subject<string>;
    isLoading$ = new BehaviorSubject<boolean>(true);
    actionButtonTitle: string;
    assignee: IOrgUserAdapted;
    cancelButtonTitle: string;
    isDateOccurredInvalid: boolean;
    isDateDiscoveredInvalid: boolean;
    isDeadlineInvalid: boolean;
    deadlineTime: string;
    deadlineValue: string;
    disableUntilDate = this.timeStamp.getDisableUntilDate();
    format = this.timeStamp.getDatePickerDateFormat();
    incidentSubTasksRecord: IIncidentSubTasksRecord;
    incidentAddEditSubTasksForm: FormGroup;
    is24Hour: boolean;
    isSaving: boolean;
    editSubTaskFlag: boolean;
    otModalData: IIncidentAddEditSubTasksModalData;
    modalTitle: string;
    orgUserError: boolean;
    orgUserTraversal = OrgUserTraversal;
    subTasksStatusStates: Array<ILabelValue<string>>;
    subTasksStatusState: ILabelValue<string, string>;
    subTasksStatusStateValue: IncidentSubTasksStateKeys;
    customModules = [
        [
            "bold",
            "italic",
            "underline",
            "strike",
            "blockquote",
            "code-block",
            "link",
            "image",
        ],
        [
            {
                list: "ordered",
            },
            {
                list: "bullet",
            },
        ],
        [
            {
                indent: "-1",
            },
            {
                indent: "+1",
            },
        ],
    ];

    constructor(
        @Inject(StoreToken) private store: IStore,
        private incidentSubTasksService: IncidentSubTasksService,
        private readonly formBuilder: FormBuilder,
        private readonly timeStamp: TimeStamp,
        private translatePipe: TranslatePipe,
    ) { }

    ngOnInit(): void {
        this.is24Hour = this.timeStamp.getIs24Hour();
        this.modalTitle = this.otModalData.modalTitle;
        this.actionButtonTitle = this.otModalData.actionButtonTitle;
        this.cancelButtonTitle = this.otModalData.cancelButtonTitle;
        this.incidentSubTasksRecord = this.otModalData.incidentSubTasksModel;
        this.editSubTaskFlag = this.otModalData.editSubTaskFlag;
        this.reset();

        this.subTasksStatusStates = this.setSubTasksStatusTypes(this.subTasksStatusStates);

        if (this.incidentSubTasksRecord) {
            this.preSelectIncidentSubTaskName();
            this.preSelectIncidentSubTaskDeadline();
            this.preSelectIncidentSubTaskDescription();
            this.preSelectIncidentSubTaskAssignee();
            this.preSelectIncidentSubTasksStatusType();
        }
        this.isLoading$.next(false);
    }

    reset(): void {
        this.incidentAddEditSubTasksForm = this.formBuilder.group({
            name: [""],
            description: [""],
            deadline: [""],
            assignee: [""],
        });
    }

    closeModal(): void {
        this.reset();
        this.otModalCloseEvent.next();
    }

    cancelButtonClicked(): void {
        this.closeModal();
    }

    setSubTasksStatusTypes(subTasksStatusStates: Array<ILabelValue<string>>): Array<ILabelValue<string>> {
        subTasksStatusStates = [
            { label: this.translatePipe.transform("Open").toUpperCase(), value: "Open" },
            { label: this.translatePipe.transform("Closed").toUpperCase(), value: "Closed" },
        ];
        return subTasksStatusStates;
    }

    selectSubTasksStatusType(event): void {
        this.subTasksStatusState = event;
        this.subTasksStatusStateValue = event.value;
    }

    actionButtonClicked(): void {
        if (this.otModalData.incidentSubTasksModel) {
            this.updateSubTask();
        } else {
            this.createSubTask();
        }
    }

    selectDeadline(dateTimeValue: IDateTimeOutputModel): void {
        this.isDeadlineInvalid = dateTimeValue.hasError;
        this.incidentAddEditSubTasksForm.get("deadline").setValue(dateTimeValue.dateTime);
        this.deadlineValue = dateTimeValue.dateTime ? this.timeStamp.incrementISODateMillisecond(dateTimeValue.dateTime) : "";
    }

    selectAssignee(selectedOption: IOtOrgUserOutput) {
        if (!selectedOption.valid) {
            this.orgUserError = true;
        } else {
            this.orgUserError = false;
            selectedOption.selection
                ? this.incidentAddEditSubTasksForm.get("assignee").setValue(selectedOption.selection.Id)
                : this.incidentAddEditSubTasksForm.get("assignee").setValue(null);
        }
    }

    isFormDataInvalid(): boolean {
        return !this.incidentAddEditSubTasksForm.valid
            || this.isDeadlineInvalid
            || !this.incidentAddEditSubTasksForm.get("description").value.trim()
            || !this.incidentAddEditSubTasksForm.get("name").value.trim();
    }

    preSelectIncidentSubTaskName(): void {
        if (this.otModalData.incidentSubTasksModel.name) {
            this.incidentAddEditSubTasksForm.get("name").setValue(this.otModalData.incidentSubTasksModel.name);
        }
    }

    preSelectIncidentSubTasksStatusType(): void {
        if (this.otModalData.incidentSubTasksModel.status) {
            this.subTasksStatusStates.map((status) => {
                if (status.value === this.otModalData.incidentSubTasksModel.status) {
                    this.subTasksStatusState = status;
                }
            });
        }
    }

    preSelectIncidentSubTaskDeadline(): void {
        if (this.otModalData.incidentSubTasksModel.deadline) {
            this.deadlineValue = this.timeStamp.incrementISODateMillisecond(this.otModalData.incidentSubTasksModel.deadline);
            this.incidentAddEditSubTasksForm.get("deadline").setValue(this.otModalData.incidentSubTasksModel.deadline);
        }
    }

    preSelectIncidentSubTaskDescription(): void {
        if (this.otModalData.incidentSubTasksModel.description) {
            this.incidentAddEditSubTasksForm.get("description").setValue(this.otModalData.incidentSubTasksModel.description);
        }
    }

    preSelectIncidentSubTaskAssignee(): void {
        if (this.otModalData.incidentSubTasksModel.assigneeId) {
            this.incidentAddEditSubTasksForm.get("assignee").setValue(this.otModalData.incidentSubTasksModel.assigneeId);
        }
    }

    createSubTask(): void {
        const newSubTask: IIncidentAddEditSubTasksCreateRequest = {
            name: this.incidentAddEditSubTasksForm.get("name").value ? this.incidentAddEditSubTasksForm.get("name").value : null,
            assigneeId: this.incidentAddEditSubTasksForm.get("assignee").value ? this.incidentAddEditSubTasksForm.get("assignee").value : null,
            deadline: this.deadlineValue,
            description: this.incidentAddEditSubTasksForm.get("description").value,
            incidentId: this.otModalData.incidentId,
        };
        this.isSaving = true;
        this.incidentSubTasksService.createSubTask(newSubTask).then((res: IProtocolResponse<IIncidentSubTasksRecord>): void => {
            if (res.result) {
                this.closeModal();
                this.otModalData.callback();
            } else {
                this.isSaving = false;
            }
        });
    }

    updateSubTask(): void {
        const updateIncident: IIncidentAddEditSubTasksUpdateRequest = {
            name: this.incidentAddEditSubTasksForm.get("name").value ? this.incidentAddEditSubTasksForm.get("name").value : null,
            assigneeId: this.incidentAddEditSubTasksForm.get("assignee").value ? this.incidentAddEditSubTasksForm.get("assignee").value : null,
            deadline: this.deadlineValue,
            description: this.incidentAddEditSubTasksForm.get("description").value,
            incidentId: this.otModalData.incidentId,
            status: this.subTasksStatusStateValue ? this.subTasksStatusStateValue : this.otModalData.incidentSubTasksModel.status,
        };
        this.isSaving = true;
        this.incidentSubTasksService.updateSubTask(this.otModalData.incidentSubTasksModel.id, updateIncident).then((res: IProtocolResponse<IIncidentSubTasksRecord>): void => {
            if (res.result) {
                this.closeModal();
                this.otModalData.callback();
            } else {
                this.isSaving = false;
            }
        });
    }
}
