import {
    Component,
    Input,
    Output,
    EventEmitter,
} from "@angular/core";

// Components
import { IncidentAddEditSubTasksModalComponent } from "incidentModule/incident-detail/incident-subtasks/components/incident-add-edit-subtasks-modal/incident-add-edit-subtasks-modal.component";

// 3rd Party
import { BehaviorSubject, Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";

// Services
import { Permissions } from "modules/shared/services/helper/permissions.service";
import { TimeStamp } from "oneServices/time-stamp.service";
import { OtModalService } from "@onetrust/vitreus";
import { IncidentSubTasksService } from "incidentModule/incident-detail/shared/services/incident-subtasks.service";

// Interfaces
import { IDataTableColumn } from "interfaces/tables.interface";
import { ITableColumnConfig } from "interfaces/table-config.interface";
import { IIncidentAddEditSubTasksModalData, IIncidentAddEditSubTasksUpdateRequest } from "incidentModule/shared/interfaces/incident-modal.interface";
import { IIncidentDetail } from "incidentModule/shared/interfaces/incident-detail-state.interface";
import { IIncidentSubTasksRecord } from "incidentModule/shared/interfaces/incident-subtasks.interface";
import { IDropdownOption } from "interfaces/one-dropdown.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";
import { IPageableMeta } from "interfaces/pagination.interface";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

// Enums
import { TableColumnTypes } from "enums/data-table.enum";
import { IncidentSubTasksStateKeys } from "incidentModule/shared/enums/incident-translation-keys.enums";

// Constants
import { IncidentSubTasksStatusColorMap } from "incidentModule/shared/constants/incident-badge-color.constants";

@Component({
    selector: "incident-subtasks-table",
    templateUrl: "./incident-subtasks-table.component.html",
})
export class IncidentSubtasksTableComponent {
    @Input() rows = [];
    @Input() columns: ITableColumnConfig[];
    @Input() metaData: IPageableMeta;
    @Input() model: IIncidentDetail;
    @Output() callOnAddSubTask: EventEmitter<any> = new EventEmitter<any>();
    @Output() callOnEditSubTask: EventEmitter<any> = new EventEmitter<any>();

    colBordered = false;
    resizableCol = false;
    responsiveTable = true;
    rowBordered = true;
    rowNoHover = false;
    rowStriped = false;
    sortKey = "";
    sortOrder = "";
    menuClass: string;
    sortable = false;
    truncateCellContent = true;
    wrapCellContent = false;
    disabledRowMap = {};
    selectedRowMap = {};
    tableColumnTypes = TableColumnTypes;
    showDeadline = this.timeStamp.displayDate() && this.timeStamp.displayTime();
    incidentSubTasksStatusColorMap = IncidentSubTasksStatusColorMap;
    incidentSubTasksStateKeys = IncidentSubTasksStateKeys;
    loadingIncidentSubTasksRecords$ = new BehaviorSubject<boolean>(true);
    private destroy$ = new Subject();

    constructor(
        public permissions: Permissions,
        private incidentSubTasksService: IncidentSubTasksService,
        private translatePipe: TranslatePipe,
        private readonly timeStamp: TimeStamp,
        private otModalService: OtModalService,
    ) { }

    ngOnInit(): void {
        this.loadingIncidentSubTasksRecords$.next(false);
    }

    columnTrackBy(index: number, column: IDataTableColumn): string {
        return column.id;
    }

    checkIfRowIsDisabled = (row: IIncidentSubTasksRecord): boolean => !!this.disabledRowMap[row.id];
    checkIfRowIsSelected = (row: IIncidentSubTasksRecord): boolean => !!this.selectedRowMap[row.id];

    callAddSubTask() {
        this.callOnAddSubTask.emit();
    }

    callEditSubTask() {
        this.callOnEditSubTask.emit();
    }

    editIncident(row: IIncidentSubTasksRecord): void {
        const modalData: IIncidentAddEditSubTasksModalData = {
            incidentSubTasksModel: row,
            incidentId: this.model.id,
            orgGroupId: this.model.orgGroupId,
            modalTitle: "EditSubTask",
            actionButtonTitle: "Save",
            cancelButtonTitle: "Cancel",
            editSubTaskFlag: false,
            callback: (() => {
                this.callEditSubTask();
            }),
        };
        this.otModalService
            .create(
                IncidentAddEditSubTasksModalComponent,
                {
                    isComponent: true,
                },
                modalData,
            ).pipe(
                takeUntil(this.destroy$),
            );
    }

    subTasksStatusName(name: string): string {
        switch (name) {
            case IncidentSubTasksStateKeys.Open:
                return this.translatePipe.transform(IncidentSubTasksStateKeys.Open).toUpperCase();
            case IncidentSubTasksStateKeys.Closed:
                return this.translatePipe.transform(IncidentSubTasksStateKeys.Closed).toUpperCase();
            default:
                return name;
        }
    }

    getActions(row: IIncidentSubTasksRecord): () => IDropdownOption[] {
        return (): IDropdownOption[] => {
            const menuOptions: IDropdownOption[] = [];
            menuOptions.push({
                text: row.status === this.incidentSubTasksStateKeys.Open ? this.translatePipe.transform("MarkAsCompleted") : this.translatePipe.transform("Reopen"),
                action: (): void => this.changeSubTaskStatus(row),
            });

            menuOptions.push({
                text: this.translatePipe.transform("Delete"),
                action: (): void => this.incidentSubTasksService.deleteSubTaskModel(row, this.model.id),
            });

            return menuOptions;
        };
    }

    changeSubTaskStatus(row: IIncidentSubTasksRecord): void {
        const updateIncident: IIncidentAddEditSubTasksUpdateRequest = {
            name: row.name ? row.name : null,
            assigneeId: row.assigneeId ? row.assigneeId : null,
            deadline: row.deadline ? this.timeStamp.incrementISODateMillisecond(row.deadline) : null,
            description: row.description ? row.description : null,
            incidentId: this.model.id,
            status: row.status === this.incidentSubTasksStateKeys.Open ? IncidentSubTasksStateKeys.Closed : IncidentSubTasksStateKeys.Open,
        };
        this.loadingIncidentSubTasksRecords$.next(true);
        this.incidentSubTasksService.updateSubTask(row.id, updateIncident).then((res: IProtocolResponse<IIncidentSubTasksRecord>): void => {
            if (res.result) {
                this.callEditSubTask();
                this.loadingIncidentSubTasksRecords$.next(false);
            }
        });
    }
}
