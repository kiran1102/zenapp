import {
    Component,
    Inject,
    ViewChild,
    TemplateRef,
} from "@angular/core";
import { StateService } from "@uirouter/core";
import { OtModalService } from "@onetrust/vitreus";
import { IncidentAttachmentsFileModalComponent } from "incidentModule/incident-detail/incident-attachments/components/incident-attachments-file-modal/incident-attachments-file-modal.component";
import { IncidentAttachmentsLinkModalComponent } from "incidentModule/incident-detail/incident-attachments/components/incident-attachments-link-modal/incident-attachments-link-modal.component";

// Interfaces
import { IIncidentAttachmentsTable, IIncidentAttachmentsModel } from "incidentModule/shared/interfaces/incident-attachments.interface";
import { IPageableMeta } from "interfaces/pagination.interface";

// Services
import { IncidentDetailService } from "incidentModule/incident-detail/shared/services/incident-detail.service";
import { IncidentAttachmentService } from "incidentModule/incident-detail/shared/services/incident-attachment.service";

// 3rd Party
import { Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

// Enums
import { TableColumnTypes } from "enums/data-table.enum";
import { IncidentAttachmentTableColumnNames } from "incidentModule/shared/enums/incident-attachments.enums";

@Component({
    selector: "incident-attachments",
    templateUrl: "./incident-attachments.component.html",
})
export class IncidentAttachmentsComponent {
    @ViewChild("IncidentAttachmentFileModal") fileModalRef: TemplateRef<any>;
    loadingAttachments = true;
    defaultPagination = { page: 0, size: 10};
    paginationParams = this.defaultPagination;
    table: IIncidentAttachmentsTable;
    tableMetaData: IPageableMeta;

    private destroy$ = new Subject();

    constructor(
        private stateService: StateService,
        private incidentDetailService: IncidentDetailService,
        private incidentAttachmentService: IncidentAttachmentService,
        private translatePipe: TranslatePipe,
        private otModalService: OtModalService,
    ) { }

    ngOnInit(): void {
        this.incidentDetailService.incidentDetailModel$.pipe(
            takeUntil(this.destroy$),
        ).subscribe(() => {
            this.loadingAttachments = true;
            this.incidentAttachmentService.retrieveIncidentAttachments(this.stateService.params.id, this.defaultPagination);
        });

        this.incidentAttachmentService.incidentAttachments$.pipe(
            takeUntil(this.destroy$),
        ).subscribe((attachmentList: IIncidentAttachmentsModel[]) => {
            if (attachmentList) {
                this.table = this.configureTable(attachmentList);
                this.tableMetaData = this.incidentAttachmentService.incidentAttachmentMeta$.getValue();
                this.loadingAttachments = false;
            }
        });
    }

    ngOnDestroy(): void {
        this.incidentAttachmentService.reset();
        this.destroy$.next();
        this.destroy$.complete();
    }

    onAddFile(): void {
        this.otModalService
            .create(
                IncidentAttachmentsFileModalComponent,
                {
                    isComponent: true,
                },
            ).pipe(
                takeUntil(this.destroy$),
            );
    }

    onAddLink(): void {
        this.otModalService
            .create(
                IncidentAttachmentsLinkModalComponent,
                {
                    isComponent: true,
                },
            ).pipe(
                takeUntil(this.destroy$),
            );
    }

    configureTable(records: IIncidentAttachmentsModel[]): IIncidentAttachmentsTable {
        return {
            rows: records,
            columns: [
                {
                    name: this.translatePipe.transform("Name"),
                    sortKey: IncidentAttachmentTableColumnNames.NAME,
                    type: TableColumnTypes.Link,
                    cellValueKey: IncidentAttachmentTableColumnNames.NAME,
                },
                {
                    name: this.translatePipe.transform("Description"),
                    sortKey: IncidentAttachmentTableColumnNames.DESCRIPTION,
                    type: TableColumnTypes.Text,
                    cellValueKey: IncidentAttachmentTableColumnNames.DESCRIPTION,
                },
                {
                    name: this.translatePipe.transform("Type"),
                    sortKey: IncidentAttachmentTableColumnNames.TYPE,
                    type: TableColumnTypes.Type,
                    cellValueKey: IncidentAttachmentTableColumnNames.TYPE,
                },
                {
                    name: this.translatePipe.transform("DateAdded"),
                    sortKey: IncidentAttachmentTableColumnNames.DATE_ADDED,
                    type: TableColumnTypes.Date,
                    cellValueKey: IncidentAttachmentTableColumnNames.DATE_ADDED,
                },
                {
                    name: this.translatePipe.transform("AddedBy"),
                    sortKey: IncidentAttachmentTableColumnNames.ADDED_BY,
                    type: TableColumnTypes.Assignee,
                    cellValueKey: IncidentAttachmentTableColumnNames.ADDED_BY,
                },
                {
                    name: "",
                    type: TableColumnTypes.Action,
                    cellValueKey: "",
                },
            ],
        };
    }

    onPaginationChange(event: number = 0): void {
        this.paginationParams = { ...this.paginationParams, page: event };
        this.loadingAttachments = true;
        this.incidentAttachmentService.retrieveIncidentAttachments(this.stateService.params.id, this.paginationParams);
    }

}
