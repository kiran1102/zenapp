// Core
import {
    Component,
    Input,
    Inject,
} from "@angular/core";

// Rxjs
import { Subject } from "rxjs";

// Services
import { Permissions } from "modules/shared/services/helper/permissions.service";
import { IncidentAttachmentService } from "incidentModule/incident-detail/shared/services/incident-attachment.service";
import { StateService } from "@uirouter/core";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

// Interfaces
import { IDropdownOption } from "interfaces/one-dropdown.interface";
import { IDataTableColumn } from "interfaces/tables.interface";
import { IIncidentAttachmentsTable, IIncidentAttachmentsModel } from "incidentModule/shared/interfaces/incident-attachments.interface";

// Enums / Constants
import { TableColumnTypes } from "enums/data-table.enum";
import { IncidentAttachmentType } from "incidentModule/shared/enums/incident-attachments.enums";

@Component({
    selector: "incident-attachments-table",
    templateUrl: "./incident-attachments-table.component.html",
})
export class IncidentAttachmentsTableComponent {
    @Input() table: IIncidentAttachmentsTable;
    @Input() loadingAttachments: boolean;

    menuClass: string;
    resizableCol = false;
    responsiveTable = true;
    rowBordered = true;
    rowNoHover = false;
    rowStriped = false;
    selectedRowMap = {};
    sortKey = "";
    sortOrder = "";
    sortable = false;
    tableColumnTypes = TableColumnTypes;
    truncateCellContent = true;
    wrapCellContent = false;
    incidentAttachmentType = IncidentAttachmentType;

    private destroy$ = new Subject();

    constructor(
        private stateService: StateService,
        private incidentAttachmentService: IncidentAttachmentService,
        private translatePipe: TranslatePipe,
        public permissions: Permissions,
    ) {}

    ngOnDestroy(): void {
        this.destroy$.next();
        this.destroy$.complete();
    }

    columnTrackBy(index: number, column: IDataTableColumn): string {
        return column.id;
    }

    attachmentActions(row: IIncidentAttachmentsModel) {
       this.incidentAttachmentService.attachmentActions(row);
    }

    handleSortChange({ sortKey, sortOrder }): void {
        this.sortKey = sortKey;
        this.sortOrder = sortOrder;
        this.table.rows.reverse();
    }

    getActions(row): () => IDropdownOption[] {
        return (): IDropdownOption[] => {
            this.setMenuClass(row);
            const menuOptions: IDropdownOption[] = [];

            menuOptions.push({
                text: this.translatePipe.transform("DeleteAttachment"),
                action: (): void => this.incidentAttachmentService.removeAttachmentModel(row, this.stateService.params.id),
            });

            return menuOptions;
        };
    }

    private setMenuClass(row) {
        this.menuClass = "actions-table__context-list";
        if (this.table.rows.length <= 10) return;
        const halfwayPoint: number = (this.table.rows.length - 1) / 2;
        const rowIndex: number = this.table.rows.findIndex((item) => row.Id === item.attachmentId);
        if (rowIndex > halfwayPoint) this.menuClass += " actions-table__context-list--above";
    }
}
