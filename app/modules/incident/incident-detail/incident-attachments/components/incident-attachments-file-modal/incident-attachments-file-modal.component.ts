import {
    Component,
    Inject,
    ViewChild,
} from "@angular/core";
import { StateService } from "@uirouter/core";
import { IOtModalContent, ToastService } from "@onetrust/vitreus";

// 3rd party
import { Subject } from "rxjs";

// Services
import { IncidentAttachmentService } from "incidentModule/incident-detail/shared/services/incident-attachment.service";

// Constants
import { FileConstraints } from "constants/file.constant.ts";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

// Enums
import { SaveType } from "incidentModule/shared/enums/incident-attachments.enums";

@Component({
    selector: "incident-attachments-file-modal",
    templateUrl: "./incident-attachments-file-modal.component.html",
})
export class IncidentAttachmentsFileModalComponent implements IOtModalContent {
    @ViewChild("fileSelect") fileSelectRef;
    otModalCloseEvent: Subject<string>;
    allowedExtensions = ["csv", "doc", "docx", "jpg", "jpeg", "pdf", "png", "ppt", "pptx", "txt", "xls", "xlsx"];
    isHovering: boolean;
    files: File[] = [];
    fileName = "";
    fileDescription = "";
    hasError = false;
    fileSizeError = false;
    isSavingFile: SaveType;
    saveType = SaveType;

    constructor(
        private stateService: StateService,
        private incidentAttachmentService: IncidentAttachmentService,
        private readonly toastService: ToastService,
        private readonly translatePipe: TranslatePipe,
    ) {}

    reset(): void {
        this.files = [];
        this.fileName = "";
        this.fileDescription = "";
        this.hasError = false;
        this.isSavingFile = null;
    }

    onSave(type: SaveType): void {
        this.fileName = this.fileName.trim();
        this.fileDescription = this.fileDescription.trim();
        if (this.files.length && this.fileName) {
            this.saveFile(type, { file: this.files[0], name: this.fileName, description: this.fileDescription })
                .then(() => type === SaveType.SaveAddNew ? this.reset() : this.closeModal())
                .catch(() => null);
        } else {
            this.hasError = true;
        }
    }

    saveFile(type, fileConfig): Promise<boolean> {
        return new Promise((resolve, reject) => {
            this.isSavingFile = type;
            this.incidentAttachmentService.saveFile(this.stateService.params.id, fileConfig)
                .then(() => {
                    this.isSavingFile = null;
                    resolve(true);
                }).catch(() => {
                    this.isSavingFile = null;
                    reject(false);
                });
        });
    }

    closeModal(): void {
        this.reset();
        this.otModalCloseEvent.next();
    }

    triggerFileSelect(): void {
        this.fileSelectRef.nativeElement.click();
    }

    handleFiles(files: File[]): void {
        this.files = [];
        this.fileSizeError = false;
        if (!files.length) {
            return;
        }

        if (files[0].size === 0 || files[0].size > FileConstraints.FILE_SIZE_LIMIT) {
            this.fileSizeError = true;
            this.toastService.error(this.translatePipe.transform("InvalidFile"), this.translatePipe.transform("FileSizeLimitViolation"));
        } else if (files[0].name.length > FileConstraints.FILE_NAME_MAX_LENGTH) {
            this.toastService.error(this.translatePipe.transform("InvalidFile"), this.translatePipe.transform("FileNameExceedsLimitOf100Characters"));
        } else {
            this.files = files;
            this.fileName = files[0].name;
        }
    }

    handleInvalidFiles(files: File[]): void {
        if (!files.length) {
            return;
        }

        if (files[0].size === 0 || files[0].size > FileConstraints.FILE_SIZE_LIMIT) {
            this.fileSizeError = true;
            this.toastService.error(this.translatePipe.transform("InvalidFile"), this.translatePipe.transform("FileSizeLimitViolation"));
        } else {
            this.toastService.error(this.translatePipe.transform("ThisFileTypeIsNotAllowed"), this.translatePipe.transform("AllowedFileFormats", { formats: this.allowedExtensions.join(", ") }));
        }
    }

    handleFilesHover(isHovering: boolean): void {
        this.isHovering = isHovering;
    }

    removeFile(index: number): void {
        this.reset();
    }

    disableSaveButton(): boolean {
        return !(this.files.length && this.fileName.trim() && !this.isSavingFile);
    }
}
