import { Component, Inject } from "@angular/core";
import { StateService } from "@uirouter/core";
import { IOtModalContent } from "@onetrust/vitreus";

// Services
import { IncidentAttachmentService } from "incidentModule/incident-detail/shared/services/incident-attachment.service";

// 3rd party
import { Subject } from "rxjs";

// interface
import { IIncidentAttachmentsModel } from "incidentModule/shared/interfaces/incident-attachments.interface";

// Enums
import { IncidentAttachmentType } from "incidentModule/shared/enums/incident-attachments.enums";
import { SaveType } from "incidentModule/shared/enums/incident-attachments.enums";

@Component({
    selector: "incident-attachments-link-modal",
    templateUrl: "./incident-attachments-link-modal.component.html",
})
export class IncidentAttachmentsLinkModalComponent implements IOtModalContent {
    otModalCloseEvent: Subject<string>;
    incidentAttachmentsModel: IIncidentAttachmentsModel;
    isSavingLink: SaveType;
    saveType = SaveType;

    constructor(
        private stateService: StateService,
        private incidentAttachmentService: IncidentAttachmentService,
    ) {}

    ngOnInit(): void {
        this.reset();
    }

    reset(): void {
        this.incidentAttachmentsModel = {
            attachmentId: null,
            link: "",
            name: "",
            description: "",
            incidentId: this.stateService.params.id,
            type: IncidentAttachmentType.URL,
        };
        this.isSavingLink = null;
    }

    closeModal(): void {
        this.reset();
        this.otModalCloseEvent.next();
    }

    validate(): boolean {
        return !(this.incidentAttachmentsModel.link.trim() && this.incidentAttachmentsModel.name.trim() && !this.isSavingLink);
    }

    onSave(type: SaveType): void {
        this.saveLink(type)
            .then(() => type === SaveType.SaveAddNew ? this.reset() : this.closeModal())
            .catch(() => null);
    }

    saveLink(type: SaveType): Promise<boolean> {
        return new Promise((resolve, reject) => {
            this.isSavingLink = type;
            this.incidentAttachmentService.saveLink(this.stateService.params.id, this.incidentAttachmentsModel)
                .then(() => {
                    this.isSavingLink = null;
                    resolve(true);
                }).catch(() => {
                    this.isSavingLink = null;
                    reject(false);
                });
        });
    }
}
