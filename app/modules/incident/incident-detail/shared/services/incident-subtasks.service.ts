import { Injectable } from "@angular/core";

// Services
import { IncidentSubtasksApiService } from "incidentModule/incident-detail/shared/services/incident-subtasks-api.service";
import { OtModalService, ConfirmModalType } from "@onetrust/vitreus";

// Interfaces
import { IIncidentSubTasksRecord, IIncidentSubtaskCardFilter } from "incidentModule/shared/interfaces/incident-subtasks.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";
import {
    IIncidentAddEditSubTasksCreateRequest,
    IIncidentAddEditSubTasksUpdateRequest,
} from "incidentModule/shared/interfaces/incident-modal.interface";
import { IPageableMeta,
    IPaginationParams,
    IPageableOf,
} from "interfaces/pagination.interface";
import { IIncidentCardFilters } from "incidentModule/shared/interfaces/incident-cards.interface";

// Enums
import { IncidentSubTasksStateKeys } from "incidentModule/shared/enums/incident-translation-keys.enums";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

// 3rd Party
import { BehaviorSubject, Subject } from "rxjs";
import { takeUntil, filter } from "rxjs/operators";

@Injectable()
export class IncidentSubTasksService {

    public incidentSubTasksList$ = new BehaviorSubject<IIncidentSubTasksRecord[]>(null);
    public incidentSubTasksCardList$ = new BehaviorSubject<IIncidentSubTasksRecord[]>(null);
    public incidentSubTasksRecordMetaData$ = new BehaviorSubject<IPageableMeta>(null);
    defaultPageDetails = { page: 0, size: 10};
    defaultCardPageDetails = { page: 0, size: 4};
    private destroy$ = new Subject();

    constructor(
        private incidentSubtasksApiService: IncidentSubtasksApiService,
        private otModalService: OtModalService,
        private translatePipe: TranslatePipe,
    ) { }

    reset(): void {
        this.incidentSubTasksList$.next(null);
        this.incidentSubTasksRecordMetaData$.next(null);
    }

    getSubTasksByIncidentId(incidentId: string, paginationParams: IPaginationParams<any> ): void {
        this.incidentSubtasksApiService.fetchSubTasksByIncidentId(incidentId, paginationParams).then((res: IProtocolResponse<IPageableOf<IIncidentSubTasksRecord>>): void => {
            if (res.data) {
                const subtasksListMeta = { ...res.data };
                this.incidentSubTasksList$.next(subtasksListMeta.content);
                this.incidentSubTasksRecordMetaData$.next(
                    {
                        first: res.data.first,
                        last: res.data.last,
                        number: res.data.number,
                        numberOfElements: res.data.numberOfElements,
                        size: res.data.size,
                        sort: [...res.data.sort],
                        totalElements: res.data.totalElements,
                        totalPages: res.data.totalPages,
                    },
                );
            }
            this.getSubTasksForIncidentCard(incidentId);
        });
    }

    getSubTasksForIncidentCard(subtaskIncidentId: string): void {
        const criteria: IIncidentSubtaskCardFilter = {
            incidentId: subtaskIncidentId,
            subTaskStatus: IncidentSubTasksStateKeys.Open,
        };
        const subtaskCardFilter: IIncidentCardFilters = {
            fullText: "",
            filters: criteria,
        };
        this.incidentSubtasksApiService.getSubTasksForIncidentCard(subtaskCardFilter, this.defaultCardPageDetails).then((res: IProtocolResponse<IPageableOf<IIncidentSubTasksRecord>>): void => {
            if (res.data) {
                const subtasksListMeta = { ...res.data };
                this.incidentSubTasksCardList$.next(subtasksListMeta.content);
            }
        });
    }

    deleteSubTaskModel(modelData: IIncidentSubTasksRecord, incidentId: string, pageMetaData?: IPageableMeta): void {
        this.otModalService
            .confirm({
                type: ConfirmModalType.DELETE,
                translations: {
                    title: this.translatePipe.transform("Delete"),
                    desc: this.translatePipe.transform("DeleteIncidentRecordDescription"),
                    confirm: this.translatePipe.transform("AreYouSureDeleteSubTask"),
                    submit: this.translatePipe.transform("Yes"),
                    cancel: this.translatePipe.transform("No"),
                },
            }).pipe(
                takeUntil(this.destroy$),
                filter((result: boolean) => result),
            ).subscribe(() => {
                    this.deleteSubTask(modelData.id)
                    .then((response: IProtocolResponse<void>): boolean => {
                        if (response.result) {
                            this.getSubTasksByIncidentId(incidentId, this.getSubtaskPageMetaData());
                        }
                        return response.result;
                    });
                },
            );
    }

    getSubtaskPageMetaData(): IPaginationParams<any> {
        const meta = this.incidentSubTasksRecordMetaData$.getValue();
        if (meta && meta.last && meta.numberOfElements <= 1) {
            --meta.number;
        }
        this.defaultPageDetails = { page: meta && meta.number ? meta.number : 0, size: 10 };
        return this.defaultPageDetails;
    }

    createSubTask(newSubTask: IIncidentAddEditSubTasksCreateRequest): ng.IPromise<IProtocolResponse<IIncidentSubTasksRecord>> {
        return this.incidentSubtasksApiService.createSubTask(newSubTask);
    }

    updateSubTask(subTaskId: string, subTask: IIncidentAddEditSubTasksUpdateRequest): ng.IPromise<IProtocolResponse<IIncidentSubTasksRecord>> {
        return this.incidentSubtasksApiService.updateSubTask(subTaskId, subTask);
    }

    deleteSubTask(subTaskId: string): ng.IPromise<IProtocolResponse<void>>  {
       return this.incidentSubtasksApiService.deleteSubTask(subTaskId);
    }
}
