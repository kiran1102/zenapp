import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";

// Services
import { ProtocolService } from "modules/core/services/protocol.service";

// Interfaces
import {
    IProtocolConfig,
    IProtocolMessages,
    IProtocolResponse,
} from "interfaces/protocol.interface";
import {
    IDINAAssessmentRequest,
    IDINATemplateDetail,
    IJurisdictionRegions,
} from "incidentModule/shared/interfaces/incident-jurisdiction.interface";

// RxJs
import { Observable, from } from "rxjs";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

@Injectable()
export class IncidentJurisdictionApiService {

    constructor(
        private translatePipe: TranslatePipe,
        private protocolService: ProtocolService,
        private httpClient: HttpClient,
    ) { }

    getJurisdictionOptions(): ng.IPromise<IProtocolResponse<IJurisdictionRegions>> {
        const config: IProtocolConfig = this.protocolService.customConfig("GET", `/api/gallery/v1/gallery/jurisdictions`, null);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("ErrorGettingJurisdictions") } };
        return this.protocolService.http(config, messages);
    }

    getDINATemplate(templateTypes: string, state: string, activeCriteria?: string, assessmentCreationAllowed?: boolean): Observable<IDINATemplateDetail[]> {
        const url = `/api/template/v1/templates/readiness`;
        return this.httpClient.get<IDINATemplateDetail[]>(url, { params: {templateTypes, state, activeCriteria} });
    }

    createDINA(assessmentDetails: IDINAAssessmentRequest): Observable<IProtocolResponse<string>> {
        const config: IProtocolConfig = this.protocolService.customConfig("POST", `/assessment-v2/v2/assessments/readiness`, null, assessmentDetails);
        const messages: IProtocolMessages = {
            Error: { custom: this.translatePipe.transform("ErrorLaunchingNewAssessment") },
        };
        return from(this.protocolService.http(config, messages));
    }
}
