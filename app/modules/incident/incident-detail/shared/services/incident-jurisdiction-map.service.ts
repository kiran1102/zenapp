import { Injectable } from "@angular/core";

// 3rd party
import * as d3 from "d3";
import Datamap from "datamaps";

// Interfaces
import { IIncidentMapData } from "incidentModule/shared/interfaces/incident-jurisdiction-map.interface";

// Enums/Constants
import { IncidentMapColor } from "incidentModule/shared/constants/incident-jurisdiction-map.constants";
import { IncidentMapFillKey } from "incidentModule/shared/enums/incident-jurisdiction.enums";

@Injectable()
export class IncidentJurisdictionMapService {
    private map: Datamap;

    initMap(containerId: string, mapData: IIncidentMapData) {
        this.map = new Datamap({
            element: document.getElementById(containerId),
            scope: "world",
            projection: "mercator",
            responsive: true,
            geographyConfig: {
                dataUrl: null,
                borderWidth: 1.0,
                borderColor: IncidentMapColor.defaultBorderColor,
                popupOnHover: false,
                highlightOnHover: false,
                popupTemplate: ((geo, data) => data && data.states && `<div class="hoverinfo"><strong>${geo.properties.name}<br/>States: ${data.states}</strong></div>`),
                highlightFillColor: IncidentMapColor.highlightFillColor,
                highlightBorderColor: IncidentMapColor.highlightBorderColor,
                highlightBorderWidth: 1,
            },
            fills: {
                defaultFill: IncidentMapColor.defaultFillColor,
                [IncidentMapFillKey.SELECTED]: IncidentMapColor.selectedFillColor,
            },
            data: mapData,
        });
        this.setMapZoom();
    }

    updateMapHighlights(mapData: IIncidentMapData) {
        if (this.map) {
            this.map.updateChoropleth(mapData, { reset: true });
        }
    }

    resizeMap(): void {
        if (this.map) {
            this.map.resize();
        }
    }

    destroyMap(): void {
        this.map = null;
    }

    private setMapZoom(): void {
        this.map.svg.call(d3.behavior.zoom()
            .scaleExtent([1, 4])
            .on("zoom", (): void => {
                this.map.svg.selectAll("g").attr("transform", "translate(" + d3.event.translate + ")scale(" + d3.event.scale + ")");
                d3.event.sourceEvent.stopPropagation();
            }));
    }
}
