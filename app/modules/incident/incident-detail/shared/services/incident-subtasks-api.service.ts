import { Injectable } from "@angular/core";

// Services
import { ProtocolService } from "modules/core/services/protocol.service";

// Interfaces
import {
    IProtocolConfig,
    IProtocolMessages,
    IProtocolResponse,
} from "interfaces/protocol.interface";
import { IPaginationParams, IPageableOf } from "interfaces/pagination.interface";
import { IIncidentSubTasksRecord } from "incidentModule/shared/interfaces/incident-subtasks.interface";
import { IIncidentCardFilters } from "incidentModule/shared/interfaces/incident-cards.interface";
import {
    IIncidentAddEditSubTasksCreateRequest,
    IIncidentAddEditSubTasksUpdateRequest,
} from "incidentModule/shared/interfaces/incident-modal.interface";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

@Injectable()
export class IncidentSubtasksApiService {

    constructor(
        private protocolService: ProtocolService,
        private translatePipe: TranslatePipe,
    ) { }

    fetchSubTasksByIncidentId(incidentId: string, paginationParams: IPaginationParams<any>): ng.IPromise<IProtocolResponse<IPageableOf<IIncidentSubTasksRecord>>> {
        const config: IProtocolConfig = this.protocolService.customConfig("GET", `/api/incident/v1/incidents/subtasks/incidents/${incidentId}/subtasks`, paginationParams);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("Tasks.NoTasksFound") } };
        return this.protocolService.http(config, messages);
    }

    getSubTasksForIncidentCard(subtaskCardFilter: IIncidentCardFilters, paginationParams: IPaginationParams<any>): ng.IPromise<IProtocolResponse<IPageableOf<IIncidentSubTasksRecord>>> {
        const config: IProtocolConfig = this.protocolService.customConfig("POST", "/api/incident/v1/incidents/subtasks/search", { ...paginationParams } , subtaskCardFilter);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("Incident.Error.RetrievingIncidents") } };
        return this.protocolService.http(config, messages);
    }

    createSubTask(newSubTask: IIncidentAddEditSubTasksCreateRequest): ng.IPromise<IProtocolResponse<IIncidentSubTasksRecord>> {
        const config: IProtocolConfig = this.protocolService.customConfig("POST", "/api/incident/v1/incidents/subtasks", null, newSubTask);
        const messages: IProtocolMessages = {
            Error: { custom: this.translatePipe.transform("ErrorAddingSubTask") },
            Success: { custom: this.translatePipe.transform("SubTaskAdded") },
        };
        return this.protocolService.http(config, messages);
    }

    updateSubTask(subtaskId: string, subTask: IIncidentAddEditSubTasksUpdateRequest): ng.IPromise<IProtocolResponse<IIncidentSubTasksRecord>> {
        const config: IProtocolConfig = this.protocolService.customConfig("PUT", `/api/incident/v1/incidents/subtasks/${subtaskId}/subtasks`, null, subTask);
        const messages: IProtocolMessages = {
            Error: { custom: this.translatePipe.transform("ErrorUpdatingSubTask") },
            Success: { custom: this.translatePipe.transform("Incident.Success.UpdatingSubTask") },
        };
        return this.protocolService.http(config, messages);
    }

    deleteSubTask(subtaskId: string): ng.IPromise<IProtocolResponse<void>> {
        const config: IProtocolConfig = this.protocolService.customConfig("DELETE", `/api/incident/v1/incidents/subtasks/${subtaskId}`, null);
        const messages: IProtocolMessages = {
            Error: { custom: this.translatePipe.transform("ErrorDeletingSubTask") },
            Success: { custom: this.translatePipe.transform("SuccessDeletingSubTask") },
        };
        return this.protocolService.http(config, messages);
    }
}
