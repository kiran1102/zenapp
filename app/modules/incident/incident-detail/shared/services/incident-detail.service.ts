import {
    Injectable,
    Inject,
    EventEmitter,
} from "@angular/core";
import { StateService } from "@uirouter/core";

// Rxjs
import {
    Subject,
    BehaviorSubject,
} from "rxjs";

// Services
import { IncidentApiService } from "incidentModule/shared/services/incident-api.service";
import { NotificationService } from "sharedModules/services/provider/notification.service";
import { IncidentAssessmentLinkApiService } from "incidentModule/shared/services/incident-assessment-linking-api.service";
import { RiskAPIService } from "oneServices/api/risk-api.service";
import { TimeStamp } from "oneServices/time-stamp.service";
import { IncidentSubTasksService } from "./incident-subtasks.service";

// Interfaces
import { IProtocolResponse, IProtocolMessages } from "interfaces/protocol.interface";
import { IIncidentTypeRecord } from "incidentModule/shared/interfaces/incident-type-state.interface";
import { IIncidentDetail } from "incidentModule/shared/interfaces/incident-detail-state.interface";
import {
    IIncidentUpdateRequest,
    IIncidentCreateRequest,
    IIncidentAssessmentLink,
} from "incidentModule/shared/interfaces/incident-create-state.interface";
import { IIncidentAssessmentRecord } from "incidentModule/shared/interfaces/incident-assessment-state.interface";
import { IIncidentPath } from "incidentModule/shared/interfaces/incident-path.interface";
import {
    IIncidentActivityPageable,
    IIncidentActivity,
    IIncidentActivityMeta,
} from "incidentModule/shared/interfaces/incident-activity-state.interface";
import { ILinkOption } from "interfaces/inventory.interface";
import { IPaginationParams, IPageableOf } from "interfaces/pagination.interface";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

// Enums
import { StatusCodes } from "enums/status-codes.enum";

@Injectable()
export class IncidentDetailService {
    incidentDetailModel$ = new BehaviorSubject<IIncidentDetail> (null);
    incidentTypes$ = new BehaviorSubject<IIncidentTypeRecord[]> (null);
    stageNameTypes$ = new BehaviorSubject<IIncidentTypeRecord[]> (null);
    incidentStagePath$ = new BehaviorSubject<IIncidentPath> (null);
    incidentLinkedAssessments$ = new Subject<IIncidentAssessmentRecord[]> ();
    incidentLinkedAssessmentsCard$ = new Subject<IIncidentAssessmentRecord[]> ();
    incidentLinkedAssessmentsMetaData$ = new BehaviorSubject<IPageableOf<IIncidentAssessmentRecord>> (null);
    incidentActivityList$ = new BehaviorSubject<IIncidentActivity[]> (null);
    incidentActivityListMetaData$ = new BehaviorSubject<IIncidentActivityMeta> (null);
    defaultPageDetails = { page: 0, size: 10};
    defaultCardPageDetails = { page: 0, size: 4};

    constructor(
        private stateService: StateService,
        private incidentApi: IncidentApiService,
        private incidentLinkAssessmentApi: IncidentAssessmentLinkApiService,
        private incidentSubTasksService: IncidentSubTasksService,
        private translatePipe: TranslatePipe,
        private notificationService: NotificationService,
        private timeStamp: TimeStamp,
        private riskApi: RiskAPIService,
    ) { }

    reset(): void {
        this.incidentDetailModel$.next(null);
        this.incidentTypes$.next(null);
        this.stageNameTypes$.next(null);
        this.incidentStagePath$.next(null);
        this.incidentLinkedAssessments$.next(null);
        this.incidentLinkedAssessmentsMetaData$.next(null);
        this.incidentActivityList$.next(null);
        this.incidentActivityListMetaData$.next(null);
    }

    retrieveIncidentById(incidentId: string): void {
        this.incidentApi.fetchIncidentById(incidentId).then((res: IProtocolResponse<IIncidentDetail>): void => {
            if (res.result) {
                this.incidentDetailModel$.next(res.data);
            } else if (res.status === StatusCodes.Forbidden) {
                this.stateService.go("zen.app.pia.module.incident.views.incident-register");
            } else {
                this.notificationService.alertError(this.translatePipe.transform("Error"), this.translatePipe.transform("Incident.Error.RetrievingIncident"));
            }
        });
    }

    retrieveIncidentTypes(): void {
        this.incidentApi.fetchIncidentTypes()
            .then((res: IProtocolResponse<IIncidentTypeRecord[]>): void => {
                if (!res.result) {
                    this.incidentTypes$.next([]);
                }

                this.incidentTypes$.next(res.data);
            });
    }

    retrieveStageNameTypes(): void {
        this.incidentApi.fetchIncidentStageTypes().then((res: IProtocolResponse<IIncidentTypeRecord[]>): void => {
            if (!res.result) {
                this.stageNameTypes$.next([]);
                return;
            }

            this.stageNameTypes$.next(res.data);
        });
    }

    updateStagePath(incidentId: string, incidentStageId: string, triggerRules = true): void {
        this.incidentApi.updateStage(incidentId, incidentStageId, triggerRules).then((res: IProtocolResponse<IIncidentDetail>): void => {
            if (res.result) {
                this.incidentDetailModel$.next(res.data);
                this.getIncidentLinkedAssessmentsCard(incidentId);
                this.incidentSubTasksService.getSubTasksForIncidentCard(incidentId);
            }
        });
    }

    hasVisitedStage(stageId: string): boolean {
        return this.incidentDetailModel$.getValue() && this.incidentDetailModel$.getValue().incidentStageRulesId.includes(stageId);
    }

    getIncidentLinkedAssessments(incidentId: string, paginationParams: IPaginationParams<any>): void {
        this.incidentLinkAssessmentApi.getIncidentLinkedAssessments(incidentId, paginationParams).then((res: IProtocolResponse<IPageableOf<IIncidentAssessmentRecord>>): void => {
            if (res.result) {
                res.data.content.forEach((record) => {
                    if (record.respondent) {
                        record.respondent = record.respondent.split(",").join(", ");
                    }
                });
                this.incidentLinkedAssessments$.next(res.data.content);
                this.incidentLinkedAssessmentsMetaData$.next(res.data);
                this.getIncidentLinkedAssessmentsCard(incidentId);
            }
        });
    }

    getIncidentLinkedAssessmentsCard(incidentId: string): void {
        this.incidentLinkAssessmentApi.getIncidentLinkedAssessments(incidentId, this.defaultCardPageDetails).then((res: IProtocolResponse<IPageableOf<IIncidentAssessmentRecord>>): void => {
            if (res.result) {
                res.data.content.forEach((record) => {
                    if (record.respondent) {
                        record.respondent = record.respondent.split(",").join(", ");
                    }
                });
                this.incidentLinkedAssessmentsCard$.next(res.data.content);
            }
        });
    }

    removeIncidentAssessmentLink(incidentId: string, assessmentId: string, paginationParams?: IPaginationParams<any>): void {
        this.incidentLinkAssessmentApi.removeIncidentAssessmentLink(incidentId, assessmentId)
        .then(() => {
            this.getIncidentLinkedAssessments(incidentId, this.getAssessmentsPageMetaData());
        });
    }

    getAssessmentsPageMetaData(): IPaginationParams<any> {
        const meta = this.incidentLinkedAssessmentsMetaData$.getValue();
        if (meta.last && meta.numberOfElements <= 1) {
            --meta.number;
        }
        this.defaultPageDetails = { page: meta.number, size: 10 };
        return this.defaultPageDetails;
    }

    prepareIncidentDetail(incident: IIncidentUpdateRequest): IIncidentDetail {
        const incidentDetailBody = { ...this.incidentDetailModel$.getValue(), ...incident };
        if (incidentDetailBody.dateOccurred) {
            incidentDetailBody.dateOccurred = this.timeStamp.incrementISODateMillisecond(incidentDetailBody.dateOccurred);
        }

        if (incidentDetailBody.dateDiscovered) {
            incidentDetailBody.dateDiscovered = this.timeStamp.incrementISODateMillisecond(incidentDetailBody.dateDiscovered);
        }

        if (incidentDetailBody.deadline) {
            incidentDetailBody.deadline = this.timeStamp.incrementISODateMillisecond(incidentDetailBody.deadline);
        }

        return incidentDetailBody;
    }

    updateIncident(incidentId: string, incident: IIncidentUpdateRequest, customMessages?: IProtocolMessages): Promise<IIncidentDetail> {
        return new Promise((resolve, reject) => {
            const incidentDetailBody = this.prepareIncidentDetail(incident);
            return this.incidentApi.updateIncident(incidentId, incidentDetailBody, customMessages)
                .then((res: IProtocolResponse<IIncidentDetail>) => {
                    if (res.result) {
                        this.incidentDetailModel$.next(res.data);
                        resolve(res.data);
                    } else {
                        reject();
                    }
                }).catch(reject);
        });
    }

    createNewIncident(newIncident: IIncidentCreateRequest): ng.IPromise<IProtocolResponse<string>> {
        return this.incidentApi.createIncident(newIncident);
    }

    retrieveIncidentActivity(incidentId: string, page: number = 0, size: number = 8): void {
        this.incidentApi.fetchIncidentActivity(incidentId, page, size).then((res: IProtocolResponse<IIncidentActivityPageable>): void => {
            if (res.result) {
                const activityListMeta = {...res.data};
                activityListMeta.incidentId = incidentId;
                delete activityListMeta.content;
                this.incidentActivityListMetaData$.next(activityListMeta);
                this.incidentActivityList$.next(res.data.content);
            }
        });
    }

    retrieveMoreIncidentActivities(incidentId: string, page: number = 0, size: number = 8): void {
        this.incidentApi.fetchIncidentActivity(incidentId, page, size).then((res: IProtocolResponse<IIncidentActivityPageable>): void => {
            if (res.result) {
                const activityListMeta = {...res.data};
                activityListMeta.incidentId = incidentId;
                delete activityListMeta.content;
                this.incidentActivityListMetaData$.next(activityListMeta);
                this.incidentActivityList$.next([...this.incidentActivityList$.getValue(), ...res.data.content]);
            }
        });
    }

    setStagePath() {
        const stageTypes = this.stageNameTypes$.getValue();
        const incidentStageId = this.incidentDetailModel$.getValue().incidentStageId;
        const activeIndex = stageTypes.findIndex((value) => incidentStageId === value.id);
        const pathItems = stageTypes.map((stageType, index) => {
            return {
                id: stageType.id,
                label: stageType.name,
                isActive: index <= activeIndex,
            };
        });

        this.incidentStagePath$.next({ pathItems, activeIndex });
    }

    createIncidentAssessmentsLink(payload: IIncidentAssessmentLink): ng.IPromise<IProtocolResponse<string>> {
        return this.incidentLinkAssessmentApi.createIncidentAssessmentLink(payload);
    }

    getIncidentLinkAssessmentDropdownList(incidentId: string, rootTemplateId: string): ng.IPromise<IProtocolResponse<ILinkOption[]>> {
        return this.incidentLinkAssessmentApi.getIncidentLinkAssessmentDropdownList(incidentId, rootTemplateId);
    }
}
