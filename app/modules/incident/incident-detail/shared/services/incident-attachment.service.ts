import { Injectable, Inject } from "@angular/core";

// Rxjs
import { BehaviorSubject } from "rxjs";

// Services
import { AttachmentService } from "oneServices/attachment.service";
import { IncidentAttachmentsApiService } from "incidentModule/incident-detail/shared/services/incident-attachment-api.service";
import { OtModalService, ConfirmModalType } from "@onetrust/vitreus";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

// Interfaces
import { IProtocolResponse } from "interfaces/protocol.interface";
import { IPageableMeta, IPageableOf } from "interfaces/pagination.interface";
import { IAttachmentFile, IAttachmentFileResponse } from "interfaces/attachment-file.interface";
import { IStringMap } from "interfaces/generic.interface";
import { IIncidentAttachmentsModel } from "incidentModule/shared/interfaces/incident-attachments.interface";

// Enums
import { IncidentAttachmentType } from "incidentModule/shared/enums/incident-attachments.enums";

@Injectable()
export class IncidentAttachmentService {
    incidentAttachments$ = new BehaviorSubject<IIncidentAttachmentsModel[]> (null);
    incidentAttachmentsCard$ = new BehaviorSubject<IIncidentAttachmentsModel[]> (null);
    incidentAttachmentMeta$ = new BehaviorSubject<IPageableMeta> (null);
    defaultCardPageDetails = { page: 0, size: 4};

    constructor(
        @Inject("Export") readonly Export: any,
        private otModalService: OtModalService,
        private translatePipe: TranslatePipe,
        private attachmentService: AttachmentService,
        private incidentAttachmentsApiService: IncidentAttachmentsApiService,
    ) { }

    reset(): void {
        this.incidentAttachments$.next(null);
        this.incidentAttachmentMeta$.next(null);
    }

    retrieveIncidentAttachments(incidentId: string, paginationParams: IStringMap<number>): void {
        this.incidentAttachmentsApiService.getPageableAttachmentList(incidentId, paginationParams).then((res: IProtocolResponse<IPageableOf<IIncidentAttachmentsModel>>): void => {
            if (res.result) {
                const attachmentMeta = { ...res.data };
                delete attachmentMeta.content;
                this.incidentAttachmentMeta$.next(attachmentMeta);
                this.incidentAttachments$.next(res.data.content);
                this.retrieveIncidentAttachmentsCard(incidentId);
            }
        });
    }

    retrieveIncidentAttachmentsCard(incidentId: string): void {
        this.incidentAttachmentsApiService.getPageableAttachmentList(incidentId, this.defaultCardPageDetails).then((res: IProtocolResponse<IPageableOf<IIncidentAttachmentsModel>>): void => {
            if (res.result) {
                const attachmentMeta = { ...res.data };
                delete attachmentMeta.content;
                this.incidentAttachmentsCard$.next(res.data.content);
            }
        });
    }

    refreshIncidentAttachments(incidentId: string): void {
        let page = 0;
        let size = 10;
        const meta = this.incidentAttachmentMeta$.getValue();
        if (meta && typeof meta.number === "number") {
            page = meta.number;
            size = meta.size;
        }
        this.retrieveIncidentAttachments(incidentId, { page, size });
    }

    saveFile(incidentId, { name, description, file }): Promise<boolean> {
        return new Promise((resolve, reject) => {
            const fileData: IAttachmentFile = {
                Blob: file,
                FileName: file.name,
                Name: name,
                Type: 70,
                RequestId: incidentId,
                RefIds: [incidentId],
                Comments: description,
                IsInternal: true,
                Extension: file.name.substr(file.name.lastIndexOf(".") + 1),
            };
            this.attachmentService.addAttachment(fileData).then((response: IProtocolResponse<IAttachmentFile>): void => {
                if (!response || !response.result) {
                    reject(false);
                }
                const incidentAttachmentsFileOrLinkModel: IIncidentAttachmentsModel = {
                    attachmentId: response.data.Id,
                    name: response.data.Name,
                    link: response.data.Location,
                    description: response.data.Comments,
                    incidentId: response.data.RefId,
                    type: IncidentAttachmentType.FILE,
                };
                this.incidentAttachmentsApiService.addAttachmentToIncident(incidentAttachmentsFileOrLinkModel).then((res: IProtocolResponse<IIncidentAttachmentsModel>): void => {
                    if (!res || !res.result) {
                        reject(false);
                    }
                    this.refreshIncidentAttachments(incidentId);
                    resolve(true);
                });
                resolve(true);
            });
        });
    }

    saveLink(incidentId, incidentAttachmentsModel: IIncidentAttachmentsModel): Promise<boolean> {
        return new Promise((resolve, reject) => {
            this.incidentAttachmentsApiService.addAttachmentToIncident(incidentAttachmentsModel).then((response: IProtocolResponse<IIncidentAttachmentsModel>): void => {
                if (!response || !response.result) {
                    reject(false);
                }
                this.refreshIncidentAttachments(incidentId);
                resolve(true);
            });
        });
    }

    removeAttachment(incidentId: string, incidentAttachmentsModel: IIncidentAttachmentsModel): void {
        this.incidentAttachmentsApiService.deleteAttachment(incidentId, incidentAttachmentsModel)
            .then((res) => {
                const meta = this.incidentAttachmentMeta$.getValue();
                if (meta && meta.last && meta.numberOfElements <= 1) {
                    --meta.number;
                    this.retrieveIncidentAttachments(incidentId, { page: meta.number, size: 10 });
                } else {
                    this.refreshIncidentAttachments(incidentId);
                }
            })
            .catch((err) => null);
    }

    attachmentActions(row: IIncidentAttachmentsModel) {
        switch (row.type) {
            case IncidentAttachmentType.FILE:
                this.downloadAttachment(row);
                break;
            case IncidentAttachmentType.URL:
                this.openLink(row.link);
                break;
        }
    }

    downloadAttachment(attachment: IIncidentAttachmentsModel) {
        this.attachmentService.downloadAttachment(attachment.attachmentId).then(
            (res: IProtocolResponse<any>): void => {
                if (res.result) {
                    this.Export.basicFileDownload(
                        res.data,
                        attachment.name,
                    );
                }
            },
        );
    }

    openLink(link: string): void {
        if (link) window.open(link, "_blank");
    }

    public removeAttachmentModel(attachment: IIncidentAttachmentsModel, incidentId: string): void {
        this.otModalService
            .confirm({
                type: ConfirmModalType.DELETE,
                translations: {
                    title: this.translatePipe.transform("DeleteAttachment"),
                    desc: this.translatePipe.transform("DeleteIncidentRecordDescription"),
                    confirm: this.translatePipe.transform("DoYouWantToContinue"),
                    submit: this.translatePipe.transform("Delete"),
                    cancel: this.translatePipe.transform("Cancel"),
                },
                hideClose: false,
            }).subscribe((res) => {
                if (res) {
                    this.removeAttachment(incidentId, attachment);
                }
            });
    }

}
