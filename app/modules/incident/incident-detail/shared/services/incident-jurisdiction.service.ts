import { Injectable } from "@angular/core";

// Rxjs
import {
    BehaviorSubject,
    Observable,
    of,
} from "rxjs";
import { catchError } from "rxjs/operators";

// Services
import { IncidentJurisdictionApiService } from "incidentModule/incident-detail/shared/services/incident-jurisdiction-api.service";
import { IncidentDetailService } from "incidentModule/incident-detail/shared/services/incident-detail.service";
import { NotificationService } from "sharedModules/services/provider/notification.service";
import { TranslateService } from "@ngx-translate/core";

// Interfaces
import {
    IJurisdiction,
    IIncidentJurisdiction,
    IDINATemplateDetail,
} from "incidentModule/shared/interfaces/incident-jurisdiction.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";
import { IDINAAssessmentRequest } from "incidentModule/shared/interfaces/incident-jurisdiction.interface";

// Constants / Enums
import { TemplateTypes } from "constants/template-types.constant";
import { TemplateCriteria } from "constants/template-states.constant";
import { TemplateStates } from "enums/template-states.enum";

@Injectable()
export class IncidentJurisdictionService {
    jurisdictionOptions$ = new BehaviorSubject<IJurisdiction[]>(null);

    constructor(
        private incidentJurisdictionApiService: IncidentJurisdictionApiService,
        private incidentDetailService: IncidentDetailService,
        private translateService: TranslateService,
        private notificationService: NotificationService,
    ) { }

    reset(): void {
        this.jurisdictionOptions$.next(null);
    }

    getInicidentDetailJurisdictions(): any {
        return this.incidentDetailService.incidentDetailModel$.getValue().jurisdictions;
    }

    retrieveJurisdictionOptions(): void {
        this.incidentJurisdictionApiService.getJurisdictionOptions()
            .then((res: IProtocolResponse<{ regions: IJurisdiction[] }>) => {
                if (res.data && res.data.regions) {
                    this.jurisdictionOptions$.next(res.data.regions);
                }
            });
    }

    updateJurisdictions(incidentId: string, jurisdictions: IIncidentJurisdiction[]): Promise<void> {
        return new Promise((resolve, reject) => {
            const currentIncidentDetail = this.incidentDetailService.incidentDetailModel$.getValue();
            currentIncidentDetail.jurisdictions = jurisdictions;
            const updateIncidentDetailBody = {
                name: currentIncidentDetail.name,
                assigneeId: currentIncidentDetail.assigneeId,
                dateOccurred: currentIncidentDetail.dateOccurred,
                dateDiscovered: currentIncidentDetail.dateDiscovered,
                deadline: currentIncidentDetail.deadline,
                description: currentIncidentDetail.description,
                incidentTypeId: currentIncidentDetail.incidentTypeId,
                orgGroupId: currentIncidentDetail.orgGroupId,
                rootCause: currentIncidentDetail.rootCause,
                jurisdictions,
            };

            const customMessages = {
                Error: { custom: this.translateService.instant("Incident.Error.UpdatingJurisdictions") },
                Success: { custom: this.translateService.instant("Incident.Success.UpdatingJurisdictions") },
            };

            this.incidentDetailService.updateIncident(incidentId, updateIncidentDetailBody, customMessages)
                .then(() => resolve())
                .catch(() => reject());
        });
    }

    removeJurisdiction(incidentId: string, jurisdiction: IIncidentJurisdiction): void {
        const currentIncidentDetailJurisdictions = this.incidentDetailService.incidentDetailModel$.getValue().jurisdictions;
        const newJurisdictions: IIncidentJurisdiction[] = [];
        currentIncidentDetailJurisdictions.map((currentJurisdiction) => {
            if (currentJurisdiction.jurisdictionId !== jurisdiction.jurisdictionId) {
                newJurisdictions.push(currentJurisdiction);
            }
        });

        this.updateJurisdictions(incidentId, newJurisdictions);
    }

    getDINATemplate(): Observable<IDINATemplateDetail[] | void> {
        const onError = () => of(this.onApiError("ErrorRetreivingTemplateDetails"));
        return this.incidentJurisdictionApiService.getDINATemplate(`${TemplateTypes.DINA}`, TemplateStates.Published, TemplateCriteria.Both)
            .pipe(
                catchError(onError),
            );
    }

    createDINA(assessmentDetails: IDINAAssessmentRequest): Observable<IProtocolResponse<string>> {
        return this.incidentJurisdictionApiService.createDINA(assessmentDetails);
    }

    protected onApiError(message: string) {
        const translated = this.translateService.instant(message);
        const title = this.translateService.instant("Error");
        this.notificationService.alertError(title, translated);
    }
}
