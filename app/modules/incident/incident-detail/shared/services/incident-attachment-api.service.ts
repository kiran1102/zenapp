import { Injectable } from "@angular/core";

// Services
import { ProtocolService } from "modules/core/services/protocol.service";

// Interfaces
import {
    IProtocolConfig,
    IProtocolMessages,
    IProtocolResponse,
} from "interfaces/protocol.interface";
import { IIncidentAttachmentsModel } from "incidentModule/shared/interfaces/incident-attachments.interface";
import { IPaginationParams, IPageableOf } from "interfaces/pagination.interface";
import { ISamlSettingsResponse } from "interfaces/setting.interface";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

@Injectable()
export class IncidentAttachmentsApiService {

    constructor(
        private translatePipe: TranslatePipe,
        private protocolService: ProtocolService,
    ) { }

    public addAttachmentToIncident(incidentAttachmentsModel: IIncidentAttachmentsModel): ng.IPromise<IProtocolResponse<IIncidentAttachmentsModel>> {
        const config: IProtocolConfig = this.protocolService.customConfig("POST", `/api/incident/v1/incidents/${incidentAttachmentsModel.incidentId}/attachments`, null, incidentAttachmentsModel);
        const messages: IProtocolMessages = {
            Error: { custom: this.translatePipe.transform("ErrorUploadingLinks") },
            Success: { custom: this.translatePipe.transform("AttachmentSaved") },
        };
        return this.protocolService.http(config, messages);
    }

    public getPageableAttachmentList(incidentId: string, paginationParams: IPaginationParams<any>): ng.IPromise<IProtocolResponse<IPageableOf<IIncidentAttachmentsModel>>> {
        const config: IProtocolConfig = this.protocolService.customConfig("GET", `/api/incident/v1/incidents/${incidentId}/attachments`, paginationParams);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("ErrorRetrievingResponses") } };
        return this.protocolService.http(config, messages);
    }

    public deleteAttachment(incidentId: string, incidentAttachmentsModel: IIncidentAttachmentsModel): ng.IPromise<IProtocolResponse<ISamlSettingsResponse>> {
        const config: IProtocolConfig = this.protocolService.customConfig("DELETE", `/api/incident/v1/incidents/${incidentId}/attachments/${incidentAttachmentsModel.attachmentId}/${incidentAttachmentsModel.type}`);
        const messages: IProtocolMessages = {
            Error: { custom: this.translatePipe.transform("ErrorDeletingFile") },
            Success: { custom: this.translatePipe.transform("AttachmentDeleted") },
        };
        return this.protocolService.http(config, messages);
    }

}
