// Core
import {
    Component,
    Inject,
} from "@angular/core";
import { StateService } from "@uirouter/core";

// Rxjs
import {
    Subject,
    BehaviorSubject,
} from "rxjs";
import {
    takeUntil,
    skip,
} from "rxjs/operators";

// 3rd party
import {
    reverse,
    toString,
    isUndefined,
    cloneDeep,
} from "lodash";

// Redux
import { getUserById } from "oneRedux/reducers/user.reducer";
import { StoreToken } from "tokens/redux-store.token";

// Services
import { IncidentDetailService } from "incidentModule/incident-detail/shared/services/incident-detail.service";
import { TimeStamp } from "oneServices/time-stamp.service";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

// Enums/Constants
import {
    IncidentActivityTypes,
    IncidentActivityFieldTypes,
} from "incidentModule/shared/enums/incident-activity.enums";
import { Color } from "constants/color.constant";
import { Regex } from "constants/regex.constant";
import {
    ActivityColor,
    ActivityIcon,
} from "constants/activity-stream.constant";

// Interfaces
import { IStringMap } from "interfaces/generic.interface";
import { IStore } from "interfaces/redux.interface";
import {
    IFormattedActivity,
    IAuditChange,
} from "interfaces/audit-history.interface";
import {
    IIncidentActivityMeta,
    IIncidentActivity,
} from "incidentModule/shared/interfaces/incident-activity-state.interface";

@Component({
    selector: "incident-activity",
    templateUrl: "./incident-activity.component.html",
})
export class IncidentActivityComponent {
    incidentId = this.stateService.params.id;
    expandedItemMap: IStringMap<boolean> = {};
    allItemsExpanded = true;
    activitiesAscending = true;
    hasMoreActivities: boolean;
    orderIcon = ActivityIcon.OrderAscend;
    formattedActivities: IFormattedActivity[] = [];
    isLoadingMoreActivities$ = new BehaviorSubject<boolean> (true);
    private activityList: IIncidentActivity[];
    private activityListMetaInfo: IIncidentActivityMeta;
    private destroy$ = new Subject();

    constructor(
        @Inject(StoreToken) private store: IStore,
        private stateService: StateService,
        private incidentDetailService: IncidentDetailService,
        private timeStampService: TimeStamp,
        private translatePipe: TranslatePipe,
    ) { }

    ngOnInit(): void {
        this.incidentDetailService.incidentActivityList$.pipe(
            takeUntil(this.destroy$),
            skip(1),
        ).subscribe((incidentActivityList) => {
            if (incidentActivityList) {
                this.activityList = cloneDeep(incidentActivityList);
                this.activityListMetaInfo = this.incidentDetailService.incidentActivityListMetaData$.getValue();
                this.hasMoreActivities = this.activityListMetaInfo ? !this.activityListMetaInfo.last : false;
                this.formatData();
                this.setExpandedItemMap();
                if (!this.activitiesAscending) {
                    this.reverseActivityOrder();
                }
                this.isLoadingMoreActivities$.next(false);
            }
        });

        this.incidentDetailService.retrieveIncidentActivity(this.incidentId);
    }

    ngOnDestroy(): void {
        this.destroy$.next();
        this.destroy$.complete();
    }

    public addActivitiesToView(): void {
        if (this.hasMoreActivities) {
            this.isLoadingMoreActivities$.next(true);
            this.incidentDetailService.retrieveMoreIncidentActivities(this.activityListMetaInfo.incidentId, this.activityListMetaInfo.number + 1);
        }
    }

    public toggleActivity(itemId: string): void {
        this.expandedItemMap[itemId] = !this.expandedItemMap[itemId];
        if (Object.values(this.expandedItemMap).every((item) => item === false) && this.allItemsExpanded) {
            this.setAllItemsExpanded();
        } else if (Object.values(this.expandedItemMap).every((item) => item === true) && !this.allItemsExpanded) {
            this.setAllItemsExpanded();
        }
    }

    public setExpandedItemMap(): void {
        this.formattedActivities.forEach((activity: IFormattedActivity): void => {
            if (isUndefined(this.expandedItemMap[activity.id])) {
                this.expandedItemMap[activity.id] = this.allItemsExpanded;
            }
        });
    }

    public setAllItemsExpanded(): void {
        this.allItemsExpanded = !this.allItemsExpanded;
    }

    public expandAll(): void {
        Object.keys(this.expandedItemMap).forEach((key) => this.expandedItemMap[key] = true);
    }

    public collapseAll(): void {
        Object.keys(this.expandedItemMap).forEach((key) => this.expandedItemMap[key] = false);
    }

    public toggleExpand(): void {
        if (this.allItemsExpanded) {
            this.collapseAll();
        } else {
            this.expandAll();
        }
        this.setAllItemsExpanded();
    }

    public toggleOrder(): void {
        this.activitiesAscending = !this.activitiesAscending;
        this.orderIcon = this.activitiesAscending ? ActivityIcon.OrderAscend : ActivityIcon.OrderDescend;
        this.reverseActivityOrder();
    }

    private reverseActivityOrder(): void {
        this.formattedActivities = reverse(this.formattedActivities);
    }

    private formatData(): void {
        this.formattedActivities = [];
        this.activityList.map((activity: IIncidentActivity, index: number): void => {
            if (activity && activity.changes && activity.changes.length) {
                activity.userName = getUserById(activity.userId)(this.store.getState()) ? getUserById(activity.userId)(this.store.getState()).FullName : this.translatePipe.transform("UserNotFound");
                const formattedActivity: IFormattedActivity = {
                    id: toString(index),
                    title: this.translatePipe.transform("UserMadeIncidentChanges", { userName: activity.userName }),
                    fieldLabel: this.translatePipe.transform("FieldName"),
                    description: activity.description,
                    timeStamp: this.timeStampService.formatDate(activity.changeDateTime),
                    icon: ActivityIcon.Edit,
                    iconColor: Color.White,
                    iconBackgroundColor: ActivityColor.Edit,
                    changes: activity.changes,
                    showOldNewValueField: true,
                };

                switch (activity.description) {
                    case IncidentActivityTypes.CREATED_ASSET:
                        formattedActivity.title = this.translatePipe.transform("UserCreatedIncident", { userName: activity.userName });
                        formattedActivity.icon = ActivityIcon.Created;
                        formattedActivity.iconColor = Color.White;
                        formattedActivity.iconBackgroundColor = ActivityColor.Created;
                        formattedActivity.fieldLabel = this.translatePipe.transform("IncidentName");
                        formattedActivity.showOldNewValueField = false;
                        break;
                    case IncidentActivityTypes.SUB_TASK_CREATED:
                        formattedActivity.title = this.translatePipe.transform("UserCreatedSubTask", { userName: activity.userName });
                        formattedActivity.icon = ActivityIcon.Created;
                        formattedActivity.iconColor = Color.White;
                        formattedActivity.iconBackgroundColor = ActivityColor.Linked;
                        formattedActivity.fieldLabel = this.translatePipe.transform("SubTaskName");
                        formattedActivity.showOldNewValueField = false;
                        break;
                    case IncidentActivityTypes.SUB_TASK_CHANGED:
                        formattedActivity.title = this.translatePipe.transform("UserMadeSubTaskChanges", { userName: activity.userName });
                        break;
                    case IncidentActivityTypes.STAGE_CHANGED:
                        formattedActivity.title = this.translatePipe.transform("UserMadeStageChanges", { userName: activity.userName });
                        formattedActivity.icon = ActivityIcon.Status;
                        formattedActivity.iconColor = Color.White;
                        formattedActivity.iconBackgroundColor = ActivityColor.Status;
                        break;
                    case IncidentActivityTypes.SUB_TASK_DELETED:
                        formattedActivity.title = this.translatePipe.transform("UserDeletedSubTask", { userName: activity.userName });
                        formattedActivity.icon = ActivityIcon.Delete;
                        formattedActivity.iconBackgroundColor = ActivityColor.Delete;
                        formattedActivity.fieldLabel = this.translatePipe.transform("SubTaskName");
                        formattedActivity.showOldNewValueField = false;
                        break;
                    case IncidentActivityTypes.ASSESSMENT_LINKED:
                        formattedActivity.title = this.translatePipe.transform("UserLinkedAssessment", { userName: activity.userName });
                        formattedActivity.icon = ActivityIcon.Linked;
                        formattedActivity.iconBackgroundColor = ActivityColor.Linked;
                        formattedActivity.fieldLabel = this.translatePipe.transform("LinkedAssessment");
                        formattedActivity.showOldNewValueField = false;
                        break;
                    case IncidentActivityTypes.ASSESSMENT_UNLINKED:
                        formattedActivity.title = this.translatePipe.transform("UserUnlinkedAssessment", { userName: activity.userName });
                        formattedActivity.icon = ActivityIcon.Unlinked;
                        formattedActivity.iconBackgroundColor = ActivityColor.Unlinked;
                        formattedActivity.fieldLabel = this.translatePipe.transform("UnlinkedAssessment");
                        formattedActivity.showOldNewValueField = false;
                        break;
                    case IncidentActivityTypes.JURISDICTIONS:
                        formattedActivity.title = this.translatePipe.transform("UserUpdatedJurisdictions", { userName: activity.userName });
                        formattedActivity.icon = ActivityIcon.Edit;
                        formattedActivity.iconBackgroundColor = ActivityColor.Edit;
                        break;
                }

                formattedActivity.changes = this.massageChanges(formattedActivity);
                this.formattedActivities.push({ ...formattedActivity });
            }
        });
    }

    private convertActivityDateField(change) {
        if (change.newValue && change.newValue.name && Regex.ISO_OR_UTC_DATE.test(change.newValue.name)) {
            change.newValue.name = this.timeStampService.formatJavaDate(change.newValue.name);
        }

        if (change.oldValue && change.oldValue.name && Regex.ISO_OR_UTC_DATE.test(change.oldValue.name)) {
            change.oldValue.name = this.timeStampService.formatJavaDate(change.oldValue.name);
        }

        return change;
    }

    private convertValueTranslations(change) {
        if (change.newValue && change.newValue.name) {
            change.newValue.name = this.translatePipe.transform(change.newValue.name);
        }

        if (change.oldValue && change.oldValue.name) {
            change.oldValue.name = this.translatePipe.transform(change.oldValue.name);
        }
    }

    private massageChanges(formattedActivity: IFormattedActivity): IAuditChange[] {
        const changes = formattedActivity.changes.map((change) => {
            switch (change.fieldName) {
                case IncidentActivityTypes.INCIDENT_NAME:
                    change.fieldLabel = this.translatePipe.transform("IncidentName");
                    change.fieldName = change.newValue.name;
                    break;
                case IncidentActivityTypes.METHOD_OF_REPORTING:
                    change.fieldLabel = this.translatePipe.transform("MethodOfReporting");
                    change.fieldName = change.newValue.name;
                    break;
                case IncidentActivityFieldTypes.INCIDENT_TYPE_ID:
                    change.fieldLabel = formattedActivity.fieldLabel;
                    change.fieldName = this.translatePipe.transform("IncidentType");
                    this.convertValueTranslations(change);
                    break;
                case IncidentActivityFieldTypes.NAME:
                    change.fieldLabel = formattedActivity.fieldLabel;
                    change.fieldName = this.translatePipe.transform("Name");
                    break;
                case IncidentActivityFieldTypes.INCIDENT_ORG_GROUP_ID:
                    change.fieldLabel = formattedActivity.fieldLabel;
                    change.fieldName = this.translatePipe.transform("Organization");
                    break;
                case IncidentActivityFieldTypes.INCIDENT_STAGE_ID:
                    change.fieldLabel = formattedActivity.fieldLabel;
                    change.fieldName = this.translatePipe.transform("Stage");
                    this.convertValueTranslations(change);
                    break;
                case IncidentActivityFieldTypes.ASSIGNEE_ID:
                    change.fieldLabel = formattedActivity.fieldLabel;
                    change.fieldName = this.translatePipe.transform("Assignee");
                    break;
                case IncidentActivityFieldTypes.DESCRIPTION:
                    change.fieldLabel = formattedActivity.fieldLabel;
                    change.fieldName = this.translatePipe.transform("Description");
                    break;
                case IncidentActivityFieldTypes.ROOT_CAUSE:
                    change.fieldLabel = formattedActivity.fieldLabel;
                    change.fieldName = this.translatePipe.transform("RootCause");
                    break;
                case IncidentActivityFieldTypes.DATE_DISCOVERED:
                    change.fieldLabel = formattedActivity.fieldLabel;
                    change.fieldName = this.translatePipe.transform("DateDiscovered");
                    change = this.convertActivityDateField(change);
                    break;
                case IncidentActivityFieldTypes.DATE_OCCURRED:
                    change.fieldLabel = formattedActivity.fieldLabel;
                    change.fieldName = this.translatePipe.transform("DateOccurred");
                    change = this.convertActivityDateField(change);
                    break;
                case IncidentActivityFieldTypes.DEADLINE:
                    change.fieldLabel = formattedActivity.fieldLabel;
                    change.fieldName = this.translatePipe.transform("Deadline");
                    change = this.convertActivityDateField(change);
                    break;
                case IncidentActivityFieldTypes.SUB_TASK_NAME:
                    change.fieldLabel = formattedActivity.fieldLabel;
                    change.fieldName = formattedActivity.showOldNewValueField
                                        ? this.translatePipe.transform("Name")
                                        : change.newValue.name;
                    break;
                case IncidentActivityTypes.SUB_TASK_DELETED:
                    change.fieldLabel = formattedActivity.fieldLabel;
                    change.fieldName = change.oldValue.name;
                    break;
                case IncidentActivityFieldTypes.SUB_TASK_STATUS_ID:
                    change.fieldLabel = formattedActivity.fieldLabel;
                    change.fieldName = this.translatePipe.transform("Status");
                    this.convertValueTranslations(change);
                    break;
                case IncidentActivityTypes.ASSESSMENT_LINKED:
                case IncidentActivityFieldTypes.LINKED_ASSESSMENT:
                    change.fieldLabel = formattedActivity.fieldLabel;
                    if (change.newValue && change.newValue.name) {
                        change.fieldName = change.newValue.name;
                    }
                    break;
                case IncidentActivityTypes.ASSESSMENT_UNLINKED:
                case IncidentActivityFieldTypes.UNLINKED_ASSESSMENT:
                    change.fieldLabel = formattedActivity.fieldLabel;
                    if (change.oldValue && change.oldValue.name) {
                        change.fieldName = change.oldValue.name;
                    }
                    break;
                case IncidentActivityFieldTypes.JURISDICTIONS:
                    change.fieldLabel = formattedActivity.fieldLabel;
                    change.fieldName = this.translatePipe.transform("Jurisdictions");
                    break;
                default:
                    change.fieldLabel = formattedActivity.fieldLabel;
                    if (change.newValue && change.newValue.name) {
                        change.fieldName = change.newValue.name;
                    }
            }
            return change;
        });

        return changes;
    }
}
