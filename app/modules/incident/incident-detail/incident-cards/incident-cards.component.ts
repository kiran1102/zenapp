// Core
import {
    Component,
    Input,
    Output,
    EventEmitter,
} from "@angular/core";

// Components
import { IncidentAddEditSubTasksModalComponent } from "incidentModule/incident-detail/incident-subtasks/components/incident-add-edit-subtasks-modal/incident-add-edit-subtasks-modal.component";

// 3rd Party
import { Subject, combineLatest } from "rxjs";
import { takeUntil } from "rxjs/operators";

// Interfaces
import { IIncidentDetail } from "incidentModule/shared/interfaces/incident-detail-state.interface";
import { IIncidentCard } from "incidentModule/shared/interfaces/incident-cards.interface";
import { IIncidentAddEditSubTasksModalData, IIncidentAddEditSubTasksUpdateRequest } from "incidentModule/shared/interfaces/incident-modal.interface";
import { IIncidentAssessmentRecord } from "incidentModule/shared/interfaces/incident-assessment-state.interface";
import { IIncidentAttachmentsModel } from "incidentModule/shared/interfaces/incident-attachments.interface";
import { IIncidentSubTasksRecord } from "incidentModule/shared/interfaces/incident-subtasks.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";

// Services
import { IncidentSubTasksService } from "incidentModule/incident-detail/shared/services/incident-subtasks.service";
import { IncidentAttachmentService } from "incidentModule/incident-detail/shared/services/incident-attachment.service";
import { IncidentDetailService } from "incidentModule/incident-detail/shared/services/incident-detail.service";
import { OtModalService } from "@onetrust/vitreus";
import { TimeStamp } from "oneServices/time-stamp.service";
import { StateService } from "@uirouter/core";
import { Permissions } from "modules/shared/services/helper/permissions.service";

// Enums
import { IncidentCardActions, IncidentCardDisplayFields } from "incidentModule/shared/enums/incident-cards.enums";
import { IncidentSubTasksStateKeys } from "incidentModule/shared/enums/incident-translation-keys.enums";
import { IncidentTabOptions } from "incidentModule/shared/enums/incident-details.enums";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

// vitreus
import { ContextMenuType } from "@onetrust/vitreus";

@Component({
    selector: "incident-cards",
    templateUrl: "./incident-cards.component.html",
})
export class IncidentCardsComponent {
    @Input() model: IIncidentDetail;
    @Input() activeTab: IncidentTabOptions;
    @Output() changeCurrentTab: EventEmitter<IncidentTabOptions> = new EventEmitter<IncidentTabOptions>();
    contextMenuType = ContextMenuType;
    incidentSubTasksStateKeys = IncidentSubTasksStateKeys;
    cards: IIncidentCard[];
    subtasksCard: IIncidentCard;
    alignment = "center";
    defaultPageDetails = { page: 0, size: 10};
    width = 40;
    loadingCards = false;
    private destroy$ = new Subject();

    constructor(
        private timeStamp: TimeStamp,
        private incidentSubTasksService: IncidentSubTasksService,
        private incidentDetailService: IncidentDetailService,
        private incidentAttachmentService: IncidentAttachmentService,
        private stateService: StateService,
        private otModalService: OtModalService,
        private translatePipe: TranslatePipe,
        private permissions: Permissions,
    ) { }

    ngOnInit(): void {
        this.loadingCards = true;
        this.getIncidentCardsData();
        this.configureIncidentCards();
    }

    ngOnDestroy(): void {
        this.destroy$.next();
        this.destroy$.complete();
    }

    configureIncidentCards() {
        combineLatest(
            this.incidentSubTasksService.incidentSubTasksCardList$,
            this.incidentAttachmentService.incidentAttachmentsCard$,
            this.incidentDetailService.incidentLinkedAssessmentsCard$,
        ).pipe(
            takeUntil(this.destroy$),
        ).subscribe(([subtasks, attachments, assessments]) => {
            if (subtasks && attachments && assessments) {
                const subtaskCard: IIncidentCard = {
                    title: {
                        text: this.translatePipe.transform("OpenSubtasks"),
                        truncate: false,
                    },
                    body: subtasks,
                    displayField: IncidentCardDisplayFields.Subtasks,
                    footer: this.translatePipe.transform("ViewAllSubtasks"),
                    shiftTab: IncidentTabOptions.SubTasks,
                    editModal: IncidentTabOptions.SubTasks,
                    menuOptions: [{
                        text: this.translatePipe.transform("Delete"),
                        value: IncidentCardActions.DeleteSubTask,
                    },
                    {
                        text: this.translatePipe.transform("MarkAsCompleted"),
                        value: IncidentCardActions.MarkSubTaskAsCompleted,
                    }],
                };
                const attachmentCard: IIncidentCard = {
                    title: {
                        text: this.translatePipe.transform("Attachments"),
                        truncate: false,
                    },
                    body: attachments,
                    displayField: IncidentCardDisplayFields.Attachments,
                    footer: this.translatePipe.transform("ViewAllAttachments"),
                    shiftTab: IncidentTabOptions.Attachments,
                    editModal: IncidentTabOptions.Attachments,
                    menuOptions: this.permissions.canShow("FileDelete") ? [{
                        text: this.translatePipe.transform("DeleteAttachment"),
                        value: IncidentCardActions.DeleteAttachment,
                    }] : [],
                };
                const assessmentCard: IIncidentCard = {
                    title: {
                        text: this.translatePipe.transform("Assessments"),
                        truncate: false,
                    },
                    body: assessments,
                    displayField: IncidentCardDisplayFields.Assessments,
                    disableField: IncidentCardDisplayFields.PrimaryLink,
                    footer: this.translatePipe.transform("ViewAllAssessments"),
                    shiftTab: IncidentTabOptions.Assessments,
                    editModal: IncidentTabOptions.Assessments,
                    menuOptions: [{
                        text: this.translatePipe.transform("RemoveLink"),
                        value: IncidentCardActions.RemoveLink,
                    }],
                };
                this.cards = [];
                this.cards.push(subtaskCard);
                this.cards.push(assessmentCard);
                this.cards.push(attachmentCard);
                this.loadingCards = false;
            }
        });
    }

    viewEditModal(type: IncidentTabOptions, model: any) {
        switch (type) {
            case IncidentTabOptions.SubTasks:
                this.viewEditSubtask(model);
                break;
            case IncidentTabOptions.Assessments:
                this.viewEditAssessment(model);
                break;
            case IncidentTabOptions.Attachments:
                this.viewDownloadAttachment(model);
                break;
        }
    }

    viewEditSubtask(subtask: IIncidentSubTasksRecord) {
        const modalData: IIncidentAddEditSubTasksModalData = {
            incidentSubTasksModel: subtask,
            incidentId: this.model.id,
            orgGroupId: this.model.orgGroupId,
            modalTitle: "EditSubTask",
            actionButtonTitle: "Save",
            cancelButtonTitle: "Cancel",
            editSubTaskFlag: false,
            callback: (() => {
                this.getIncidentSubTasks();
                this.incidentSubTasksService.getSubTasksByIncidentId(this.model.id, this.incidentSubTasksService.getSubtaskPageMetaData());
            }),
        };
        this.otModalService
            .create(
                IncidentAddEditSubTasksModalComponent,
                {
                    isComponent: true,
                },
                modalData,
            ).pipe(
                takeUntil(this.destroy$),
            );
    }

    viewEditAssessment(assessment: IIncidentAssessmentRecord) {
        this.stateService.go("zen.app.pia.module.assessment.detail", { assessmentId: assessment.assessmentId });
    }

    viewDownloadAttachment(attachment: IIncidentAttachmentsModel) {
        this.incidentAttachmentService.attachmentActions(attachment);
    }

    getIncidentCardsData() {
        this.getIncidentSubTasks();
        this.getIncidentAttachments();
        this.getIncidentAssessments();
    }

    getIncidentSubTasks() {
        this.incidentSubTasksService.getSubTasksForIncidentCard(this.model.id);
    }

    getIncidentAttachments() {
        this.incidentAttachmentService.retrieveIncidentAttachmentsCard(this.model.id);
    }
    getIncidentAssessments() {
        this.incidentDetailService.getIncidentLinkedAssessmentsCard(this.model.id);
    }

    shiftTab(tab: IncidentTabOptions) {
        this.changeCurrentTab.emit(tab);
    }

    handleAction(type: IncidentCardActions, model: any) {
        switch (type) {
            case IncidentCardActions.MarkSubTaskAsCompleted:
                this.markSubTaskAsComplete(model);
                break;
            case IncidentCardActions.DeleteSubTask:
                this.incidentSubTasksService.deleteSubTaskModel(model, this.model.id);
                break;
            case IncidentCardActions.DeleteAttachment:
                this.incidentAttachmentService.removeAttachmentModel(model, this.model.id);
                break;
            case IncidentCardActions.RemoveLink:
                this.incidentDetailService.removeIncidentAssessmentLink(this.model.id, model.assessmentId);
                break;
        }
    }

    markSubTaskAsComplete(model: IIncidentSubTasksRecord) {
        const updateIncident: IIncidentAddEditSubTasksUpdateRequest = {
            name: model.name ? model.name : null,
            assigneeId: model.assigneeId ? model.assigneeId : null,
            deadline: model.deadline ? this.timeStamp.incrementISODateMillisecond(model.deadline) : null,
            description: model.description ? model.description : null,
            incidentId: this.model.id,
            status: model.status === this.incidentSubTasksStateKeys.Open ? IncidentSubTasksStateKeys.Closed : IncidentSubTasksStateKeys.Open,
        };
        this.incidentSubTasksService.updateSubTask(model.id, updateIncident).then((res: IProtocolResponse<IIncidentSubTasksRecord>): void => {
            if (res.result) {
                this.getIncidentSubTasks();
                this.incidentSubTasksService.getSubTasksByIncidentId(this.model.id, this.incidentSubTasksService.getSubtaskPageMetaData());
            }
        });
    }

}
