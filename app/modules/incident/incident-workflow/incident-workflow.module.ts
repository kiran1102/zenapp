import { NgModule } from "@angular/core";

// Modules
import { SharedModule } from "sharedModules/shared.module";
import { CommonModule } from "@angular/common";
import { HttpClientModule } from "@angular/common/http";
import { FormsModule } from "@angular/forms";
import { ReactiveFormsModule } from "@angular/forms";
import { VitreusModule } from "@onetrust/vitreus";
import { DndListModule } from "ngx-drag-and-drop-lists";
import { OTPipesModule, PIPES } from "modules/pipes/pipes.module";
import { VirtualScrollModule } from "od-virtualscroll";
import { OneVideoModule } from "modules/video/video.module";
import { IncidentSharedModule } from "incidentModule/shared/incident-shared.module";

// Components
import { IncidentWorkflowComponent } from "incidentModule/incident-workflow/incident-workflow.component";
import { IncidentWorkflowPathComponent } from "incidentModule/incident-workflow/components/incident-workflow-path/incident-workflow-path.component";
import { IncidentWorkflowRulesComponent } from "incidentModule/incident-workflow/components/incident-workflow-rules/incident-workflow-rules.component";

// Services
import { IncidentWorkflowService } from "incidentModule/incident-workflow/shared/services/incident-workflow.service";
import { IncidentWorkflowApiService } from "incidentModule/incident-workflow/shared/services/incident-workflow-api.service";

@NgModule({
    imports: [
        SharedModule,
        CommonModule,
        HttpClientModule,
        FormsModule,
        ReactiveFormsModule,
        VitreusModule,
        DndListModule,
        OTPipesModule,
        VirtualScrollModule,
        OneVideoModule,
        IncidentSharedModule,
    ],
    declarations: [
        IncidentWorkflowPathComponent,
        IncidentWorkflowRulesComponent,
        IncidentWorkflowComponent,
    ],
    exports: [
        IncidentWorkflowComponent,
    ],
    entryComponents: [
        IncidentWorkflowComponent,
    ],
    providers: [
        IncidentWorkflowService,
        IncidentWorkflowApiService,
        PIPES, // TODO: Remove from providers when ot-common is implemented. Only necessary if you are using pipe in TS file, else just module is fine
    ],
})
export class IncidentWorkflowModule {}
