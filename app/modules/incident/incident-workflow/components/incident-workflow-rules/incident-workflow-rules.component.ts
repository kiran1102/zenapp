// Core
import {
    Component,
    Inject,
} from "@angular/core";

// 3rd Party
import { cloneDeep } from "lodash";

// Rxjs
import {
    Subject,
    BehaviorSubject,
    combineLatest,
} from "rxjs";
import { takeUntil, skip } from "rxjs/operators";

// Redux
import { StoreToken } from "tokens/redux-store.token";
import { getOrgList, getCurrentOrgId } from "oneRedux/reducers/org.reducer";

// Services
import { IncidentWorkflowService } from "incidentModule/incident-workflow/shared/services/incident-workflow.service";
import { IncidentJurisdictionApiService } from "incidentModule/incident-detail/shared/services/incident-jurisdiction-api.service";

// Interfaces
import { IStore } from "interfaces/redux.interface";
import {
    IRule,
    IRuleAction,
    IRulesConfig,
    IRuleDeleteConfirmationModalResolve,
    IRuleActionTemplates,
} from "incidentModule/shared/incident-rules/incident-rules.interface";
import { IOrganization } from "interfaces/org.interface";
import { IIncidentTypeRecord } from "incidentModule/shared/interfaces/incident-type-state.interface";
import { ICustomTemplateDetails } from "modules/template/interfaces/custom-templates.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";
import { IJurisdiction, IJurisdictionCountry, IJurisdictionStateProvince } from "incidentModule/shared/interfaces/incident-jurisdiction.interface";

// Enums / Constants
import { IncidentWorkflowRuleConditionKeys,
    IncidentWorkflowRuleTaskActionKeys,
    IncidentWorkflowRuleTranslationKeys,
    IncidentWorkflowRuleActionApiKeys,
} from "incidentModule/shared/enums/incident-workflow-rules.enums";
import { IncidentSourceTypes } from "incidentModule/shared/enums/incident-details.enums";
import { AssessmentPermissions } from "constants/assessment.constants";
import { RuleActionType,
    RuleOperator,
    RuleEventType,
    RuleConditionType,
} from "incidentModule/shared/incident-rules/incident-rules.enum";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

@Component({
    selector: "incident-workflow-rules",
    templateUrl: "./incident-workflow-rules.component.html",
})
export class IncidentWorkflowRulesComponent {
    incidentWorkflowId = "";
    rules: IRule[] = [];
    stage: IIncidentTypeRecord;
    incidentTypes: IIncidentTypeRecord[];
    templates: ICustomTemplateDetails[];
    config: IRulesConfig;
    ruleEditMode = false;
    deleteConfig: IRuleDeleteConfirmationModalResolve = {
        modalTitle: this.translatePipe.transform("RemoveRule"),
        confirmationText: this.translatePipe.transform("RemoveRuleConfirmationText"),
        submitButtonText: this.translatePipe.transform("Remove"),
        cancelButtonText: this.translatePipe.transform("Cancel"),
        promiseToResolve: null,
        callbackPromise: (): Promise<boolean> => {
            return new Promise((resolve) => {
                resolve(true);
            });
        },
    };

    isLoading$ = new BehaviorSubject(true);
    private destroy$ = new Subject();

    constructor(
        @Inject(StoreToken) private store: IStore,
        private incidentWorkflowService: IncidentWorkflowService,
        private incidentJurisdictionApiService: IncidentJurisdictionApiService,
        private readonly translatePipe: TranslatePipe,
    ) {}

    ngOnInit() {
        this.incidentWorkflowService.retrieveIncidentTypes();
        this.incidentWorkflowService.getAssessmentTemplates();

        combineLatest(this.incidentWorkflowService.incidentTypes$, this.incidentWorkflowService.incidentActiveStage$, this.incidentWorkflowService.templates$)
            .pipe(takeUntil(this.destroy$))
            .subscribe(([incidentTypes, activeStage, templateList]) => {
                this.isLoading$.next(true);
                if (incidentTypes && activeStage && templateList) {
                    this.incidentTypes = cloneDeep(incidentTypes);
                    this.stage = cloneDeep(activeStage);
                    this.templates = cloneDeep(templateList);
                    const filter = {
                        ruleKey1: this.stage.id,
                        ruleKey2: "",
                        ruleKey3: "",
                        ruleEventType: RuleEventType.STAGE_ENTER,
                    };

                    this.incidentWorkflowService.retrieveAllRules(filter);
                }
            });

        this.incidentWorkflowService.workflowRules$
            .pipe(skip(1), takeUntil(this.destroy$))
            .subscribe((workflowRules) => {
               const modifiedWorkflowRules = cloneDeep(workflowRules).map((workflowRule: IRule): IRule => {
                   switch (workflowRule.taskType.label) {
                        case IncidentWorkflowRuleActionApiKeys.createSubtask:
                            workflowRule.taskType.label = this.translatePipe.transform(IncidentWorkflowRuleTranslationKeys.createSubtask);
                            break;
                        case IncidentWorkflowRuleActionApiKeys.launchAssessment:
                            workflowRule.taskType.label = this.translatePipe.transform(IncidentWorkflowRuleTranslationKeys.launchAssessment);
                            workflowRule.actionParameters = this.incidentWorkflowService.handleRespondentsDropDownValues(workflowRule.actionParameters, IncidentWorkflowRuleActionApiKeys.launchAssessment);
                            workflowRule.actionParameters.respondents = workflowRule.actionParameters.respondents ? cloneDeep(workflowRule.actionParameters.respondents) : [];
                            break;
                        case IncidentWorkflowRuleActionApiKeys.sendNotification:
                            workflowRule.taskType.label = this.translatePipe.transform(IncidentWorkflowRuleTranslationKeys.sendNotification);
                            workflowRule.actionParameters = this.incidentWorkflowService.handleRespondentsDropDownValues(workflowRule.actionParameters, IncidentWorkflowRuleActionApiKeys.sendNotification);
                            workflowRule.actionParameters.recipients = workflowRule.actionParameters.recipients ? cloneDeep(workflowRule.actionParameters.recipients) : [];
                            break;
                   }
                   return workflowRule;
                });
               this.init(modifiedWorkflowRules);
            });
    }

    ngOnDestroy(): void {
        this.destroy$.next();
        this.destroy$.complete();
    }

    async init(workflowRules) {
        this.incidentWorkflowId = "";
        this.rules = workflowRules;
        this.config = await this.setupConfig();
        this.ruleEditMode = false;
        this.isLoading$.next(false);
    }

    setupConfig(): Promise<IRulesConfig> {
        return new Promise(async (resolve) => {
            const selectedOrgGroup = getCurrentOrgId(this.store.getState());
            const orgList = getOrgList(selectedOrgGroup)(this.store.getState())
                .map((org: IOrganization) => {
                    return { value: org.Id, key: org.Name};
                });

            const jurisdictionList = await this.createJurisdictionOptions();

            const templateList = this.templates.map((template: ICustomTemplateDetails): IRuleActionTemplates => {
                const templateName: string = template.isActive ? template.name : `${template.name} (${this.translatePipe.transform("Archived")})`;
                return { templateRootVersionId: template.rootVersionId, templateName};
            });
            resolve({
                entityTypeId: this.stage.id,
                task: {
                    options: [
                        {
                            label: this.translatePipe.transform(IncidentWorkflowRuleTranslationKeys.createSubtask),
                            value: IncidentWorkflowRuleActionApiKeys.createSubtask,
                            required: true,
                        },
                        {
                            label: this.translatePipe.transform(IncidentWorkflowRuleTranslationKeys.launchAssessment),
                            value: IncidentWorkflowRuleActionApiKeys.launchAssessment,
                            required: true,
                        },
                        {
                            label: this.translatePipe.transform(IncidentWorkflowRuleTranslationKeys.sendNotification),
                            value: IncidentWorkflowRuleActionApiKeys.sendNotification,
                            required: true,
                        },
                    ],
                    subOptions: {
                        createSubtask: [
                            {
                                field: IncidentWorkflowRuleTaskActionKeys.NAME,
                                fieldDisplay: this.translatePipe.transform(IncidentWorkflowRuleTranslationKeys.name),
                                placeholder: this.translatePipe.transform(IncidentWorkflowRuleTranslationKeys.name),
                                type: RuleActionType.InputString,
                                defaultValue: "",
                                required: true,
                            },
                            {
                                field: IncidentWorkflowRuleTaskActionKeys.DESCRIPTION,
                                fieldDisplay: this.translatePipe.transform(IncidentWorkflowRuleTranslationKeys.description),
                                placeholder: this.translatePipe.transform(IncidentWorkflowRuleTranslationKeys.description),
                                type: RuleActionType.RichText,
                                richTextOptions: [
                                    [
                                        "bold",
                                        "italic",
                                        "underline",
                                        "strike",
                                        "clean",
                                        "blockquote",
                                        "code-block",
                                        "link",
                                    ],
                                    [
                                        {
                                            list: "ordered",
                                        },
                                        {
                                            list: "bullet",
                                        },
                                    ],
                                    [
                                        {
                                            indent: "-1",
                                        },
                                        {
                                            indent: "+1",
                                        },
                                    ],
                                ],
                                defaultValue: "",
                                required: true,
                            },
                        ],
                        launchAssessment: [
                            {
                                field: IncidentWorkflowRuleTaskActionKeys.NAME,
                                fieldDisplay: this.translatePipe.transform(IncidentWorkflowRuleTranslationKeys.name),
                                type: RuleActionType.InputString,
                                placeholder: this.translatePipe.transform(IncidentWorkflowRuleTranslationKeys.name),
                                defaultValue: "",
                                required: true,
                            },
                            {
                                field: IncidentWorkflowRuleTaskActionKeys.TEMPLATE_ROOT_VERSION_ID,
                                fieldDisplay: this.translatePipe.transform(IncidentWorkflowRuleTranslationKeys.template),
                                type: RuleActionType.Picklist,
                                picklistPlaceholder: this.translatePipe.transform(IncidentWorkflowRuleTranslationKeys.template),
                                picklistOptions: [...templateList],
                                picklistLabelKey: "templateName",
                                picklistValueKey: "templateRootVersionId",
                                defaultValue: "",
                                required: true,
                            },
                            {
                                field: IncidentWorkflowRuleTaskActionKeys.RESPONDENT,
                                fieldDisplay: this.translatePipe.transform(IncidentWorkflowRuleTranslationKeys.respondent),
                                type: RuleActionType.OrgUserMultiSelect,
                                defaultValue: "",
                                fetchOptionsOnClick: false,
                                returnEmptyEntriesOnSelect: true,
                                permission: AssessmentPermissions.AssessmentCanBeRespondent,
                                allowEmail: true,
                                required: true,
                            },
                        ],
                        sendNotification: [
                            {
                                field: IncidentWorkflowRuleTaskActionKeys.RECIPIENTS,
                                fieldDisplay: this.translatePipe.transform(IncidentWorkflowRuleTranslationKeys.recipients),
                                type: RuleActionType.OrgUserMultiSelect,
                                defaultValue: "",
                                fetchOptionsOnClick: false,
                                returnEmptyEntriesOnSelect: true,
                                permission: "",
                                allowEmail: true,
                                required: true,
                            },
                            {
                                field: IncidentWorkflowRuleTaskActionKeys.SUBJECT,
                                fieldDisplay: this.translatePipe.transform(IncidentWorkflowRuleTranslationKeys.subject),
                                type: RuleActionType.InputString,
                                placeholder: this.translatePipe.transform(IncidentWorkflowRuleTranslationKeys.subject),
                                defaultValue: "",
                                required: true,
                            },
                            {
                                field: IncidentWorkflowRuleTaskActionKeys.BODY,
                                fieldDisplay: this.translatePipe.transform(IncidentWorkflowRuleTranslationKeys.body),
                                placeholder: this.translatePipe.transform(IncidentWorkflowRuleTranslationKeys.body),
                                type: RuleActionType.RichText,
                                richTextOptions: [
                                    [
                                        "bold",
                                        "italic",
                                        "underline",
                                        "strike",
                                        "clean",
                                        "blockquote",
                                        "code-block",
                                        "link",
                                    ],
                                    [
                                        {
                                            list: "ordered",
                                        },
                                        {
                                            list: "bullet",
                                        },
                                    ],
                                    [
                                        {
                                            indent: "-1",
                                        },
                                        {
                                            indent: "+1",
                                        },
                                    ],
                                ],
                                defaultValue: "",
                                required: true,
                            },
                        ],
                    },
                    valueKey: "value",
                    labelKey: "label",
                },
                ruleOperators: {
                    placeholder: this.translatePipe.transform("All").toLowerCase(),
                    options: [
                        {
                            label: this.translatePipe.transform("All").toLowerCase(),
                            value: "all",
                        },
                        {
                            label: this.translatePipe.transform("any"),
                            value: "any",
                        },
                    ],
                    valueKey: "value",
                    labelKey: "label",
                },
                conditionMeta: [
                    {
                        field: IncidentWorkflowRuleConditionKeys.INCIDENT_TYPE,
                        fieldDisplay: this.translatePipe.transform("IncidentType"),
                        options: this.createIncidentTypeOptions(),
                        type: RuleConditionType.Picklist,
                        operators: [
                            { operator: RuleOperator.EQUAL_TO, operatorDisplay: this.translatePipe.transform("EqualTo") },
                            { operator: RuleOperator.NOT_EQUAL_TO, operatorDisplay: this.translatePipe.transform("NotEqualTo") },
                        ],
                        labelKey: "key",
                        valueKey: "value",
                    },
                    {
                        field: IncidentWorkflowRuleConditionKeys.METHOD_OF_REPORTING,
                        fieldDisplay: this.translatePipe.transform("MethodOfReporting"),
                        options: this.createMethodOfReportingOptions(),
                        type: RuleConditionType.Picklist,
                        operators: [
                            { operator: RuleOperator.EQUAL_TO, operatorDisplay: this.translatePipe.transform("EqualTo") },
                            { operator: RuleOperator.NOT_EQUAL_TO, operatorDisplay: this.translatePipe.transform("NotEqualTo") },
                        ],
                        labelKey: "key",
                        valueKey: "value",
                    },
                    {
                        field: IncidentWorkflowRuleConditionKeys.ORGANIZATION,
                        fieldDisplay: this.translatePipe.transform("Organization"),
                        options: [...orgList],
                        type: RuleConditionType.OrgSelect,
                        operators: [
                            { operator: RuleOperator.EQUAL_TO, operatorDisplay: this.translatePipe.transform("EqualTo") },
                        ],
                        labelKey: "key",
                        valueKey: "value",
                    },
                    {
                        field: IncidentWorkflowRuleConditionKeys.JURISICICTION,
                        fieldDisplay: this.translatePipe.transform("Jurisdictions"),
                        options: [...jurisdictionList],
                        type: RuleConditionType.Picklist,
                        operators: [
                            { operator: RuleOperator.EQUAL_TO, operatorDisplay: this.translatePipe.transform("EqualTo") },
                        ],
                        labelKey: "key",
                        valueKey: "value",
                    },
                ],
            });
        });
    }

    createIncidentTypeOptions(): IRuleAction[] {
        return this.incidentTypes.map((type: IIncidentTypeRecord) => {
            return {
                key: this.translatePipe.transform(type.nameKey),
                value: type.id,
            };
        });
    }

    createMethodOfReportingOptions(): IRuleAction[] {
        return [
            {
                key: this.translatePipe.transform("Manual"),
                value: IncidentSourceTypes.MANUAL.toUpperCase(),
            },
            {
                key: this.translatePipe.transform("Assessment"),
                value: IncidentSourceTypes.ASSESSMENT.toUpperCase(),
            },
        ];
    }

    createJurisdictionOptions(): Promise<IRuleAction[]> {
        return new Promise((resolve) => {
            this.incidentJurisdictionApiService.getJurisdictionOptions()
                .then((res: IProtocolResponse<{ regions: IJurisdiction[] }>) => {
                    if (res.data && res.data.regions) {
                        const regions = [];

                        res.data.regions.map((region: IJurisdiction) => {
                            if (region.countries && region.countries.length) {
                                region.countries.map((country: IJurisdictionCountry) => {
                                    if (country.stateProvinces && country.stateProvinces.length) {
                                        country.stateProvinces.map((state: IJurisdictionStateProvince) => {
                                            regions.push({ key: `${country.code}-${state.name}`, value: state.jurisdictionId });
                                        });
                                    } else {
                                        regions.push({ key: country.name, value: country.jurisdictionId });
                                    }
                                });
                            }
                        });

                        resolve(regions);
                    } else {
                        throw Error("No regions");
                    }
                }).catch((err) => {
                    resolve([]);
                });
        });
    }

    editModeChanged(mode: boolean) {
        this.ruleEditMode = mode;
    }

    resetRules() {
        this.isLoading$.next(true);
        const filter = {
            ruleKey1: this.stage.id,
            ruleKey2: "",
            ruleKey3: "",
            ruleEventType: RuleEventType.STAGE_ENTER,
        };

        this.incidentWorkflowService.retrieveAllRules(filter);
    }

    rulesSaved(rules: IRule[]) {
        this.incidentWorkflowService.saveRules([ ...rules ], this.stage.id);
    }

    getRuleById(ruleId: string) {}
}
