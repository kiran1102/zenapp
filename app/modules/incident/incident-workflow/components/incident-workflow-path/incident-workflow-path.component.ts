// Core
import {
    Component,
    Input,
} from "@angular/core";

// rxjs
import { BehaviorSubject, Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";

// Interfaces
import { PathItem } from "@onetrust/vitreus";
import { IIncidentPath } from "incidentModule/shared/interfaces/incident-path.interface";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

// Services
import { IncidentWorkflowService } from "incidentModule/incident-workflow/shared/services/incident-workflow.service";

@Component({
    selector: "incident-workflow-path",
    templateUrl: "./incident-workflow-path.component.html",
})
export class IncidentWorkflowPathComponent {
    @Input() incidentId: string;

    loadingIncidentPath$ = new BehaviorSubject<boolean>(true);
    pathItems: PathItem[];
    activeIndex: number;

    private destroy$ = new Subject();

    constructor(
        private incidentWorkflowService: IncidentWorkflowService,
        private translatePipe: TranslatePipe,
    ) { }

    ngOnInit(): void {
        this.incidentWorkflowService.incidentStagePath$
        .pipe(takeUntil(this.destroy$))
        .subscribe((stagePath: IIncidentPath) => {
            if (stagePath) {
                this.pathItems = stagePath.pathItems.map((pathItem) => {
                    return { ...pathItem, label: this.translatePipe.transform(pathItem.label) };
                });
                this.activeIndex = stagePath.activeIndex;
                this.loadingIncidentPath$.next(false);
            }
        });

        this.incidentWorkflowService.retrieveStageNameTypes();
    }

    ngOnDestroy(): void {
        this.destroy$.next();
        this.destroy$.complete();
    }

    onStagePathChange(event: number) {
        this.activeIndex = event;
        this.incidentWorkflowService.changeActiveStagePath(event);
        for (let i = 0; i < this.pathItems.length; i++) {
            this.pathItems[i].isActive = (i === this.activeIndex);
        }
    }

}
