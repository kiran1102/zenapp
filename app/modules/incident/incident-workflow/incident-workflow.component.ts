// Core
import { Component } from "@angular/core";
import { StateService } from "@uirouter/core";

// Services
import { Permissions } from "modules/shared/services/helper/permissions.service";
import { IncidentWorkflowService } from "incidentModule/incident-workflow/shared/services/incident-workflow.service";

@Component({
    selector: "incident-workflow",
    templateUrl: "./incident-workflow.component.html",
})

export class IncidentWorkflowComponent {
    constructor(
        public permissions: Permissions,
        public incidentWorkflowService: IncidentWorkflowService,
        private stateService: StateService,
    ) {}

    ngOnDestroy(): void {
        this.incidentWorkflowService.reset();
    }
}
