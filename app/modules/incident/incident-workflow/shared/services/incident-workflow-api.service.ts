import { Injectable, Inject } from "@angular/core";

// Services
import { ProtocolService } from "modules/core/services/protocol.service";

// Interfaces
import {
    IProtocolConfig,
    IProtocolMessages,
    IProtocolResponse,
} from "interfaces/protocol.interface";
import { IIncidentTypeRecord } from "incidentModule/shared/interfaces/incident-type-state.interface";
import { ICustomTemplateDetails } from "modules/template/interfaces/custom-templates.interface";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import { IRuleMeta, IRule } from "incidentModule/shared/incident-rules/incident-rules.interface";
import ng from "angular";

@Injectable()
export class IncidentWorkflowApiService {

    constructor(
        @Inject("$rootScope") private $rootScope: IExtendedRootScopeService,
        private protocolService: ProtocolService,
        private translatePipe: TranslatePipe,
    ) { }

    geIncidentRuleMeta(): ng.IPromise<IProtocolResponse<any>> {
        const config: IProtocolConfig = this.protocolService.customConfig("GET", `/api/incident/v1/incidents/rule/metadata/`);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("Error.GetIncidentWorkflowRules") } };
        return this.protocolService.http(config, messages);
    }

    getAllWorkflowRules(filter): ng.IPromise<IProtocolResponse<IRuleMeta[]>> {
        const config: IProtocolConfig = this.protocolService.customConfig("POST", `/api/incident/v1/incidents/rules/search`, null, filter);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("Error.GetAllIncidentWorkflowRules") } };
        return this.protocolService.http(config, messages);
    }

    getRuleById(ruleId: string): ng.IPromise<IProtocolResponse<IRule>> {
        const config: IProtocolConfig = this.protocolService.customConfig("GET", `/api/incident/v1/incidents/rule/${ruleId}`);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("Error.GetIncidentWorkflowRulesById") } };
        return this.protocolService.http(config, messages);
    }

    reorderRules(webformId: string, rules: IRule[]): ng.IPromise<IProtocolResponse<IRuleMeta[]>> {
        const lang = this.$rootScope.tenantLanguage;
        const config: IProtocolConfig = this.protocolService.customConfig("POST", `/api/incident/v1/incidents/rule/reorder`, null, { rules });
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("Error.ReorderIncidentWorkflowRules") } };
        return this.protocolService.http(config, messages);
    }

    saveRules(rules: IRule[], isDelete?: boolean): ng.IPromise<IProtocolResponse<IRuleMeta[]>> {
        const successMessageKey = isDelete ? "RulesDeletedSuccessfully" : "RulesSavedSuccessfully";
        const failureMessageKey = isDelete ? "ErrorDeletingRules" : "Rule.ErrorSaving";
        const config: IProtocolConfig = this.protocolService.customConfig("POST", `/api/incident/v1/incidents/rules`, null, { rules });
        const messages: IProtocolMessages = {
            Success: { custom: this.translatePipe.transform(successMessageKey) },
            Error: { custom: this.translatePipe.transform(failureMessageKey) },
        };
        return this.protocolService.http(config, messages);
    }

    saveAllRules(rules: IRule[], stageId: string): ng.IPromise<IProtocolResponse<IRuleMeta[]>> {
        const config: IProtocolConfig = this.protocolService.customConfig("POST", `/api/incident/v1/incidents/rules/save`, { stageId }, { rules });
        const messages: IProtocolMessages = {
            Success: { custom: this.translatePipe.transform("RulesSavedSuccessfully") },
            Error: { custom: this.translatePipe.transform("Rule.ErrorSaving") },
        };
        return this.protocolService.http(config, messages);
    }

    fetchIncidentStageTypes(): ng.IPromise<IProtocolResponse<IIncidentTypeRecord[]>> {
        const config: IProtocolConfig = this.protocolService.customConfig("GET", "/api/incident/v1/incidents/stages/all", null, null);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("Error.RetrievingIncidentStagesTypes") } };
        return this.protocolService.http(config, messages);
    }

    getAssessmentTemplates = (types: string, state: string, activeCriteria?: string, assessmentCreationAllowed?: boolean): ng.IPromise<IProtocolResponse<ICustomTemplateDetails[]>> => {
        const config: IProtocolConfig = this.protocolService.customConfig("GET", "/api/template/v1/templates", { types, state, activeCriteria, assessmentCreationAllowed });
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("Error.RetrievingIncidentStagesTypes") } };
        return this.protocolService.http(config, messages);
    }
}
