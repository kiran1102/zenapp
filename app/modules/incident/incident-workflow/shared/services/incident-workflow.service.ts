import { Injectable } from "@angular/core";

// Rxjs
import {
    BehaviorSubject,
    from,
    Subscription,
} from "rxjs";

// Services
import { IncidentWorkflowApiService } from "incidentModule/incident-workflow/shared/services/incident-workflow-api.service";
import { IncidentApiService } from "incidentModule/shared/services/incident-api.service";

// Constants/Enums
import { LogicalOperator } from "incidentModule/shared/incident-rules/incident-rules.enum";
import { TemplateTypes } from "constants/template-types.constant";
import { TemplateCriteria } from "constants/template-states.constant";
import { TemplateStates } from "enums/template-states.enum";
import { IncidentWorkflowRuleActionApiKeys } from "incidentModule/shared/enums/incident-workflow-rules.enums";
import { Regex } from "constants/regex.constant";

// Interfaces
import { IIncidentPath } from "incidentModule/shared/interfaces/incident-path.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";
import { IIncidentTypeRecord } from "incidentModule/shared/interfaces/incident-type-state.interface";
import {
    IRule,
    IRuleActionRespondents,
    IRuleActionRecipients,
} from "incidentModule/shared/incident-rules/incident-rules.interface";
import { IIncidentRuleGetBody } from "incidentModule/shared/incident-rules/inicident-rules-api.interface";
import { ICustomTemplateDetails } from "modules/template/interfaces/custom-templates.interface";

// 3rd Party
import { cloneDeep, without } from "lodash";

@Injectable()
export class IncidentWorkflowService {
    workflowRules$ = new BehaviorSubject<IRule[]>(null);
    ruleFetcher: Subscription;
    incidentTypes$ = new BehaviorSubject<IIncidentTypeRecord[]> (null);
    stageNameTypes$ = new BehaviorSubject<IIncidentTypeRecord[]>(null);
    incidentStagePath$ = new BehaviorSubject<IIncidentPath>(null);
    incidentActiveStage$ = new BehaviorSubject<IIncidentTypeRecord>(null);
    templates$ = new BehaviorSubject<ICustomTemplateDetails[]>(null);

    constructor(
        private incidentWorkflowApi: IncidentWorkflowApiService,
        private incidentApi: IncidentApiService,
    ) { }

    reset(): void {
        this.workflowRules$.next(null);
        this.incidentTypes$.next(null);
        this.stageNameTypes$.next(null);
        this.incidentStagePath$.next(null);
        this.incidentActiveStage$.next(null);
        this.templates$.next(null);
    }

    retrieveIncidentTypes(): void {
        this.incidentApi.fetchIncidentTypes()
            .then((res: IProtocolResponse<IIncidentTypeRecord[]>): void => {
                if (!res.result) {
                    this.incidentTypes$.next([]);
                }

                this.incidentTypes$.next(res.data);
            });
    }

    retrieveStageNameTypes(): void {
        this.incidentWorkflowApi.fetchIncidentStageTypes().then((res: IProtocolResponse<IIncidentTypeRecord[]>): void => {
            if (!res.result) {
                this.stageNameTypes$.next([]);
                return;
            }

            this.stageNameTypes$.next(res.data);
            this.setStagePath();
        });
    }

    setStagePath(): void {
        const stageTypes = this.stageNameTypes$.getValue();
        const activeIndex = 0;
        const pathItems = stageTypes.map((stageType, index) => {
            return {
                id: stageType.id,
                label: stageType.name,
                isActive: index <= activeIndex,
            };
        });

        this.incidentActiveStage$.next(stageTypes[activeIndex]);
        this.incidentStagePath$.next({ pathItems, activeIndex });
    }

    changeActiveStagePath(activeIndex): void {
        this.incidentActiveStage$.next(this.stageNameTypes$.getValue()[activeIndex]);
    }

    retrieveAllRules(filter: IIncidentRuleGetBody): void {
        if (this.ruleFetcher) {
            this.ruleFetcher.unsubscribe();
        }

        this.ruleFetcher = from(this.incidentWorkflowApi.getAllWorkflowRules(filter))
            .subscribe((res) => {
                if (!res.result) {
                    this.workflowRules$.next([]);
                    return;
                }
                this.workflowRules$.next(this.parseRules(res.data));
            });
    }

    saveRules(rules: IRule[], stageId: string): void {
        const parsedRules = cloneDeep(rules).map((rule, index) => {
            if ((!rule.conditionGroups[0].conditions[0].field
                || rule.conditionGroups[0].conditions[0].field === ""
                || rule.conditionGroups[0].conditions[0].value === "")
                && rule.conditionGroups.length > 1) {
                rule.conditionGroups.splice(0, 1);
            }
            rule = this.convertMultiOrgSelectRespondents(rule);
            rule.actionParameters = JSON.stringify(rule.actionParameters);
            rule.sequence = index;
            return rule;
        });
        this.incidentWorkflowApi.saveAllRules(parsedRules, stageId)
            .then((res: IProtocolResponse<IRule[]>) => {
                if (!res.result) {
                    return;
                }
                this.workflowRules$.next(this.parseRules(res.data));
            });
    }

    parseRules(rules: IRule[]): IRule[] {
        return rules.map((rule) => {
            rule.actionParameters = JSON.parse(rule.actionParameters);
            rule.operator = rule.conditionGroups[0].logicalOperator === LogicalOperator.OR
                ? { label: "any", value: "any" }
                : { label: "all", value: "all" };
            return rule;
        });
    }

    getAssessmentTemplates(): void {
        this.incidentWorkflowApi.getAssessmentTemplates(`${TemplateTypes.PIA},${TemplateTypes.VENDOR}`, TemplateStates.Published, TemplateCriteria.Both)
            .then((res: IProtocolResponse<ICustomTemplateDetails[]>) => {
                if (res.result) {
                    this.templates$.next(res.data);
                }
        });
    }

    handleRespondentsDropDownValues(actionParameters: any, actionType: string): any {
        switch (actionType) {
            case IncidentWorkflowRuleActionApiKeys.launchAssessment:
                actionParameters.templateRootVersionId = {
                    label: actionParameters.templateRootVersionId,
                    value: actionParameters.templateRootVersionId,
                };
                const respondents = actionParameters.respondents.map((element: IRuleActionRespondents) => {
                    if (element && element.respondentId !== undefined) {
                        return { FullName: element.respondentName, Id: element.respondentId };
                    }
                });
                actionParameters.respondents = respondents;
                return actionParameters;
            case IncidentWorkflowRuleActionApiKeys.sendNotification:
                const recipients = actionParameters.recipients.map((element: IRuleActionRecipients) => {
                    if (element && element.recipientId !== undefined) {
                        return { FullName: element.recipientName, Id: element.recipientId };
                    }
                });
                actionParameters.recipients = recipients;
                return actionParameters;
        }
    }

    convertMultiOrgSelectRespondents(rule: IRule): IRule {
        if (rule.taskType.value === IncidentWorkflowRuleActionApiKeys.launchAssessment) {
            if (rule.actionParameters.respondents) {
                if (rule.actionParameters.templateRootVersionId instanceof Object) {
                    rule.actionParameters.templateRootVersionId = rule.actionParameters.templateRootVersionId.value;
                }
                const respondents = rule.actionParameters.respondents.map((element: any) => {
                    if (element && element.Id !== undefined) {
                        return { respondentId: this.validateUserId(element.Id), respondentName: element.FullName };
                    } else if (element && element.respondentId !== undefined) {
                        return { respondentId: this.validateUserId(element.respondentId), respondentName: element.respondentName };
                    } else {
                        return element;
                    }
                });
                rule.actionParameters.respondents = without(respondents, undefined);
            }
        } else if (rule.taskType.value === IncidentWorkflowRuleActionApiKeys.sendNotification) {
            if (rule.actionParameters.recipients) {
                const recipients = rule.actionParameters.recipients.map((element: any) => {
                    if (element && element.respondentId !== undefined) {
                        return { recipientId: this.validateUserId(element.respondentId), recipientName: element.respondentName };
                    } else if (element && element.Id !== undefined) {
                        return { recipientId: this.validateUserId(element.Id), recipientName: element.FullName };
                    } else {
                        return element;
                    }
                });
                rule.actionParameters.recipients = without(recipients, undefined);
            }
        }
        return rule;
    }

    validateUserId(userId: string): string {
        return userId && userId.match(Regex.GUID) ? userId : null;
    }
}
