// Core
import {
    Component,
    OnInit,
    OnDestroy,
} from "@angular/core";
import { IncidentTypesAddModalComponent } from "incidentModule/incident-types/incident-types-add-modal/incident-types-add-modal.component";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

// Services
import { IncidentDetailService } from "incidentModule/incident-detail/shared/services/incident-detail.service";
import { OtModalService, ModalSize } from "@onetrust/vitreus";

// Enums / Constants
import { IncidentTypeStatus, IncidentTypesTableColumnNames } from "incidentModule/shared/enums/incident-type.enums";
import { TableColumnTypes } from "enums/data-table.enum";

// Interfaces
import {
    IIncidentTypeRecord,
    IIncidentTypesTable,
    IIncidentTypeAddModal,
} from "incidentModule/shared/interfaces/incident-type-state.interface";

// rxjs
import { Subject } from "rxjs";
import { takeUntil, filter } from "rxjs/operators";

@Component({
    selector: "incident-types",
    templateUrl: "./incident-types.component.html",
})
export class IncidentTypesComponent implements OnInit, OnDestroy {
    loading = true;
    incidentTypeStatus = IncidentTypeStatus;
    incidentTypesActiveTable: IIncidentTypesTable;
    incidentTypesInActiveTable: IIncidentTypesTable;

    private destroy$ = new Subject();

    constructor(
        private incidentDetailService: IncidentDetailService,
        private translatePipe: TranslatePipe,
        private otModalService: OtModalService,
    ) {}

    ngOnInit() {
        this.fetchIncidentTypes();
    }

    ngOnDestroy() {
        this.destroy$.next();
        this.destroy$.complete();
    }

    fetchIncidentTypes() {
        this.incidentDetailService.retrieveIncidentTypes();
        this.incidentDetailService.incidentTypes$.pipe(
            takeUntil(this.destroy$),
            filter((incidentTypes) => Boolean(incidentTypes)),
        ).subscribe((incidentTypes: IIncidentTypeRecord[]) => {
            const activeIncidentTypes: IIncidentTypeRecord[] = [];
            const inActiveIncidentTypes: IIncidentTypeRecord[] = [];
            incidentTypes.map((incidentType) => {
                const incidentTypeRecord: IIncidentTypeRecord = {
                    id: incidentType.id,
                    name: incidentType.name,
                    nameKey: incidentType.nameKey,
                    description: incidentType.description,
                    active: incidentType.active,
                } ;
                if (incidentType.active) {
                    activeIncidentTypes.push(incidentTypeRecord);
                } else if (!incidentType.active) {
                    inActiveIncidentTypes.push(incidentTypeRecord);
                }
            });
            this.incidentTypesActiveTable = this.configureTable(activeIncidentTypes);
            this.incidentTypesInActiveTable = this.configureTable(inActiveIncidentTypes);
        });
        this.loading = false;
    }

    configureTable(records: IIncidentTypeRecord[]): IIncidentTypesTable {
        return {
            rows: [...records],
            columns: [
                {
                    name: this.translatePipe.transform("Key"),
                    sortKey: IncidentTypesTableColumnNames.NAME,
                    type: TableColumnTypes.Text,
                    cellValueKey: IncidentTypesTableColumnNames.NAME,
                },
                {
                    name: this.translatePipe.transform("Name"),
                    sortKey: IncidentTypesTableColumnNames.KEY,
                    type: TableColumnTypes.Text,
                    cellValueKey: IncidentTypesTableColumnNames.KEY,
                },
                {
                    name: "",
                    sortKey: IncidentTypesTableColumnNames.ACTION,
                    type: TableColumnTypes.Action,
                    cellValueKey: IncidentTypesTableColumnNames.ACTION,
                },
            ],
        };
    }

    addIncidentType() {
        const incidentTypeAddModal: IIncidentTypeAddModal = {
            modalTitle: this.translatePipe.transform("AddOptions"),
            attributeNameKey: this.translatePipe.transform("IncidentTypes"),
            currentActiveOptions: this.incidentTypesActiveTable.rows,
            currentInactiveOptions: this.incidentTypesInActiveTable.rows,
        };
        this.otModalService.create(
            IncidentTypesAddModalComponent,
            {
                isComponent: true,
                size: ModalSize.MEDIUM,
            },
            incidentTypeAddModal,
        ).pipe(
            takeUntil(this.destroy$),
        );
    }
}
