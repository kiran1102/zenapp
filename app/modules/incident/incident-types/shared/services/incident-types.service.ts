import { Injectable } from "@angular/core";

// Services
import { IncidentTypesApiService } from "./incident-types-api.service";

// RXJS
import { Observable } from "rxjs";

// Interfaces
import { IIncidentTypeRecordUpdate, IIncidentTypeRecord } from "incidentModule/shared/interfaces/incident-type-state.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";

@Injectable()
export class IncidentTypesService {
    constructor(
        private incidentTypesApiService: IncidentTypesApiService,
    ) {}

    updateIncidentType(incidentTypeRecord: IIncidentTypeRecordUpdate): Observable<IProtocolResponse<string>> {
        return this.incidentTypesApiService.updateIncidentType(incidentTypeRecord);
    }

    createIncidentTypes(incidentTypeRecords: IIncidentTypeRecord[]): Observable<IProtocolResponse<string[]>> {
        return this.incidentTypesApiService.createIncidentTypes(incidentTypeRecords);
    }
}
