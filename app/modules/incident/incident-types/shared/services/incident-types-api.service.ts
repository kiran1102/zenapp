import { Injectable } from "@angular/core";

// Services
import { ProtocolService } from "modules/core/services/protocol.service";

// RXJS
import {
    from,
    Observable,
} from "rxjs";

// Interfaces
import { IIncidentTypeRecordUpdate, IIncidentTypeRecord } from "incidentModule/shared/interfaces/incident-type-state.interface";
import {
    IProtocolConfig,
    IProtocolMessages,
    IProtocolResponse,
} from "interfaces/protocol.interface";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

@Injectable()
export class IncidentTypesApiService {

    constructor(
        private protocolService: ProtocolService,
        private translatePipe: TranslatePipe,
    ) {}

    updateIncidentType(incidentTypeRecord: IIncidentTypeRecordUpdate): Observable<IProtocolResponse<string>> {
        const config: IProtocolConfig = this.protocolService.customConfig("PUT", `/api/incident/v1/incidents/types/${incidentTypeRecord.id}/types`, null, incidentTypeRecord);
        const messages: IProtocolMessages = {
            Error: { custom: this.translatePipe.transform("ErrorEditingAttribute") },
            Success: { custom: this.translatePipe.transform("AttributeSuccessfullyUpdated") },
        };
        return from(this.protocolService.http(config, messages));
    }

    createIncidentTypes(incidentTypeRecords: IIncidentTypeRecord[]): Observable<IProtocolResponse<string[]>> {
        const config: IProtocolConfig = this.protocolService.customConfig("POST", `/api/incident/v1/incidents/types/bulk`, null, incidentTypeRecords);
        const messages: IProtocolMessages = {
            Error: { custom: this.translatePipe.transform("ErrorAddingAttribute") },
            Success: { custom: this.translatePipe.transform("AttributeSuccessfullyAdded") },
        };
        return from(this.protocolService.http(config, messages));
    }
}
