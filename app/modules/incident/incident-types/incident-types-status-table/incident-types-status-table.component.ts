// Core
import {
    Component,
    Input,
    OnDestroy,
    OnChanges,
    OnInit,
    Inject,
} from "@angular/core";

// Interfaces
import { IDataTableColumn } from "interfaces/tables.interface";
import { IIncidentTypesTable, IIncidentTypeRecord, IIncidentTypeRecordUpdate } from "incidentModule/shared/interfaces/incident-type-state.interface";
import { IDropdownOption } from "interfaces/one-dropdown.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";

// Rxjs
import { Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";

// Services
import { IncidentTypesService } from "incidentModule/incident-types/shared/services/incident-types.service";
import { IncidentDetailService } from "incidentModule/incident-detail/shared/services/incident-detail.service";
import { TenantTranslationService } from "sharedServices/tenant-translation.service";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

// Enums / Constants
import { TableColumnTypes } from "enums/data-table.enum";
import { IncidentTypeStatus } from "incidentModule/shared/enums/incident-type.enums";
import { ContextMenuType } from "@onetrust/vitreus";
import { StatusCodes } from "enums/status-codes.enum";
import { TranslationModules } from "constants/translation.constant";

@Component({
    selector: "incident-types-status-table",
    templateUrl: "./incident-types-status-table.component.html",
})
export class IncidentTypesStatusTableComponent implements OnInit, OnDestroy, OnChanges {
    @Input() table: IIncidentTypesTable;
    @Input() loadingIncidentTypes: boolean;
    @Input() tableType: IncidentTypeStatus;
    @Input() emptyState: boolean;

    colBordered = false;
    resizableCol = false;
    responsiveTable = true;
    rowBordered = true;
    rowNoHover = false;
    rowStriped = false;
    sortKey = "";
    sortOrder = "";
    sortable = false;
    truncateCellContent = true;
    wrapCellContent = false;
    tableColumnTypes = TableColumnTypes;
    menuType = ContextMenuType.Small;
    menuOptions: IDropdownOption[];
    hideContextMenuOptions = false;
    activeOptionsHeader = this.translatePipe.transform("ActiveOptions");
    inActiveOptionsHeader = this.translatePipe.transform("InactiveOptions");

    private destroy$ = new Subject();

    constructor(
        @Inject("TenantTranslationService") private tenantTranslationService: TenantTranslationService,
        private translatePipe: TranslatePipe,
        private incidentTypesService: IncidentTypesService,
        private incidentDetailService: IncidentDetailService,
    ) {}

    ngOnInit() {
        this.generateMenuOptions();
        this.tenantTranslationService.addModuleTranslations(TranslationModules.Incident);
    }

    ngOnDestroy() {
        this.destroy$.next();
        this.destroy$.complete();
    }

    ngOnChanges() {
        this.emptyState = this.table && this.table.rows.length === 0;
        this.hideContextMenuOptions = this.table && this.table.rows.some((incidentType: IIncidentTypeRecord) => incidentType.active === true) && this.table.rows.length === 1;
    }

    columnTrackBy(index: number, column: IDataTableColumn): string {
        return column.id;
    }

    generateMenuOptions() {
        this.menuOptions = [];
        this.menuOptions.push({
            text: this.tableType === 0 ? this.translatePipe.transform("Deactivate") : this.translatePipe.transform("Activate"),
            action: (row: IIncidentTypeRecord): void => this.updateIncidentType(row),
        });
    }

    updateIncidentType(row: IIncidentTypeRecord) {
        const incidentType: IIncidentTypeRecordUpdate = {
            id: row.id,
            description: row.description,
            name: row.name,
            nameKey: row.nameKey,
            isActive: !row.active,
        };
        this.incidentTypesService.updateIncidentType(incidentType)
            .pipe(takeUntil(this.destroy$))
            .subscribe((res: IProtocolResponse<string>): void => {
                if (res.status === StatusCodes.Success) {
                    this.incidentDetailService.retrieveIncidentTypes();
                }
            });
    }
}
