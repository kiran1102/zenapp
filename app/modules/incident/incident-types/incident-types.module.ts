import { NgModule } from "@angular/core";

// Modules
import { SharedModule } from "sharedModules/shared.module";
import { CommonModule } from "@angular/common";
import { HttpClientModule } from "@angular/common/http";
import { FormsModule } from "@angular/forms";
import { ReactiveFormsModule } from "@angular/forms";
import { VitreusModule } from "@onetrust/vitreus";
import { DndListModule } from "ngx-drag-and-drop-lists";
import { OTPipesModule, PIPES } from "modules/pipes/pipes.module";
import { VirtualScrollModule } from "od-virtualscroll";
import { OneVideoModule } from "modules/video/video.module";
import { IncidentSharedModule } from "incidentModule/shared/incident-shared.module";

// Components
import { IncidentTypesComponent } from "incidentModule/incident-types/incident-types.component";
import { IncidentTypesStatusTableComponent } from "./incident-types-status-table/incident-types-status-table.component";
import { IncidentTypesAddModalComponent } from "incidentModule/incident-types/incident-types-add-modal/incident-types-add-modal.component";

// Services
import { IncidentTypesService } from "incidentModule/incident-types/shared/services/incident-types.service";
import { IncidentTypesApiService } from "incidentModule/incident-types/shared/services/incident-types-api.service";

@NgModule({
    imports: [
        SharedModule,
        CommonModule,
        HttpClientModule,
        FormsModule,
        ReactiveFormsModule,
        VitreusModule,
        DndListModule,
        OTPipesModule,
        VirtualScrollModule,
        OneVideoModule,
        IncidentSharedModule,
    ],
    declarations: [
        IncidentTypesComponent,
        IncidentTypesStatusTableComponent,
        IncidentTypesAddModalComponent,
    ],
    exports: [
        IncidentTypesComponent,
    ],
    entryComponents: [
        IncidentTypesComponent,
        IncidentTypesStatusTableComponent,
        IncidentTypesAddModalComponent,
    ],
    providers: [
        IncidentTypesService,
        IncidentTypesApiService,
        PIPES, // TODO: Remove from providers when ot-common is implemented. Only necessary if you are using pipe in TS file, else just module is fine
    ],
})
export class IncidentTypesModule {}
