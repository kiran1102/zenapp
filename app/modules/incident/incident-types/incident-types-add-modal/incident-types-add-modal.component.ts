// Core
import {
    Component,
    OnDestroy,
    OnInit,
    Inject,
} from "@angular/core";
import { IOtModalContent } from "@onetrust/vitreus";
import {
    FormControl,
    FormGroup,
    FormArray,
    FormBuilder,
    Validators,
} from "@angular/forms";

// Interfaces
import { IIncidentTypeAddModal, IIncidentTypeRecord } from "incidentModule/shared/interfaces/incident-type-state.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";

// Rxjs
import { Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";

// Services
import { IncidentTypesService } from "incidentModule/incident-types/shared/services/incident-types.service";
import { IncidentDetailService } from "incidentModule/incident-detail/shared/services/incident-detail.service";
import { TenantTranslationService } from "sharedServices/tenant-translation.service";

// Enums / Constants
import { StatusCodes } from "enums/status-codes.enum";
import { NO_BLANK_SPACE } from "constants/regex.constant";
import { TranslationModules } from "constants/translation.constant";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

@Component({
    selector: "incident-types-add-modal",
    templateUrl: "./incident-types-add-modal.component.html",
})
export class IncidentTypesAddModalComponent implements IOtModalContent, OnInit, OnDestroy {
    otModalCloseEvent: Subject<string>;
    otModalData: IIncidentTypeAddModal;
    optionsForm: FormGroup;
    isDuplicate: boolean;
    value = "value";
    isLoading = true;
    maxLength = 10;

    private destroy$ = new Subject();

    constructor(
        @Inject("TenantTranslationService") private tenantTranslationService: TenantTranslationService,
        private formBuilder: FormBuilder,
        private incidentTypesService: IncidentTypesService,
        private incidentDetailService: IncidentDetailService,
        private translatePipe: TranslatePipe,
    ) {}

    ngOnInit() {
        this.optionsForm = this.formBuilder.group({
            newOptionInput: new FormControl("", [
                Validators.required,
                Validators.maxLength(150),
                Validators.pattern(NO_BLANK_SPACE),
            ]),
            options: new FormArray([]),
        });
        this.isLoading = false;
    }

    ngOnDestroy() {
        this.destroy$.next();
        this.destroy$.complete();
    }

    closeModal(): void {
        this.otModalCloseEvent.next();
    }

    addOption(event?: KeyboardEvent): void {
        if (event) event.preventDefault();
        const newValue: string = this.optionsForm.get("newOptionInput").value;
        const activeOptionExists: boolean = this.otModalData.currentActiveOptions.some((activeIncidentType: IIncidentTypeRecord) => Boolean(activeIncidentType.name.toLowerCase() === newValue.toLowerCase() || this.translatePipe.transform(activeIncidentType.nameKey) === newValue));
        const inActiveOptionExists: boolean = this.otModalData.currentInactiveOptions.some((inActiveIncidentType: IIncidentTypeRecord) => Boolean(inActiveIncidentType.name.toLowerCase() === newValue.toLowerCase() || this.translatePipe.transform(inActiveIncidentType.nameKey) === newValue));
        this.isDuplicate = false;
        if (activeOptionExists || inActiveOptionExists) {
            this.isDuplicate = true;
        }
        if (newValue && newValue.trim() && this.optionsForm.get("newOptionInput").valid && !this.isDuplicate) {
            const newOptionInput = this.optionsForm.get("newOptionInput") as FormControl;
            const options = this.optionsForm.get("options") as FormArray;
            options.push(this.createOption(newOptionInput.value));
            newOptionInput.reset();
        }
    }

    removeOption(optionIndex: number): void {
        const options = this.optionsForm.get("options") as FormArray;
        options.removeAt(optionIndex);
    }

    createOption(optionText: string): FormGroup {
        return new FormGroup({
            value: new FormControl(optionText, [Validators.required,
                Validators.maxLength(150),
                Validators.minLength(1),
                Validators.pattern(NO_BLANK_SPACE)]),
        });
    }

    onSave() {
        this.isLoading = true;
        const options: string[] = this.optionsForm.get("options").value;
        const incidentTypes: IIncidentTypeRecord[] = [];
        options.map((option) => {
            const incidentTypeCreateRequest: IIncidentTypeRecord = {
                name: option["value"],
                languageCode: this.tenantTranslationService.getCurrentLanguage(),
            };
            incidentTypes.push(incidentTypeCreateRequest);
        });
        this.incidentTypesService.createIncidentTypes(incidentTypes)
            .pipe(takeUntil(this.destroy$))
            .subscribe((res: IProtocolResponse<string[]>): void => {
            if (res.status === StatusCodes.Created) {
                this.tenantTranslationService.addModuleTranslations(TranslationModules.Incident).then(() => {
                    this.incidentDetailService.retrieveIncidentTypes();
                });
            }
            this.isLoading = false;
            this.closeModal();
        });
    }
}
