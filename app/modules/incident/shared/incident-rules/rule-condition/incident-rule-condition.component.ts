// Angular
import {
    Component,
    Input,
    OnInit,
} from "@angular/core";

// 3rd Party
import {
    find,
    cloneDeep,
} from "lodash";

// Interfaces
import {
    IRuleCondition,
    IRuleConditionMeta,
    IConditionOperator,
    IRuleAction,
} from "incidentModule/shared/incident-rules/incident-rules.interface";
import { IStringMap } from "interfaces/generic.interface";

// Enums
import { RuleConditionType } from "incidentModule/shared/incident-rules/incident-rules.enum";

// Pipes
import { FilterPipe } from "pipes/filter.pipe";

// Service
import { IncidentRulesHelperService } from "incidentModule/shared/incident-rules/incident-rules-helper.service";

@Component({
    selector: "incident-rule-condition",
    templateUrl: "./incident-rule-condition.component.html",
})
export class IncidentRuleConditionComponent implements OnInit {
    @Input() readonly first: boolean;
    @Input() readonly last: boolean;
    @Input() readonly ruleIndex: number;
    @Input() readonly subRuleIndex: number;
    @Input() readonly conditionIndex: number;
    @Input() readonly translations: IStringMap<string>;
    @Input() readonly editMode: boolean;
    @Input() readonly conditionMeta: IRuleConditionMeta[];
    @Input() set condition(condition: IRuleCondition) {
        if (condition.field) {
            this.fieldModel = find(this.conditionMeta, (x) => x.field === condition.field);
            this.operatorModel = find(this.fieldModel.operators, (x) => x.operator === condition.operator);
            this.valueModel = find(this.fieldModel.options, (x) => x[this.fieldModel.valueKey] === condition.value);
        } else {
            this.fieldModel = {
                field: "",
                fieldDisplay: "",
                type: RuleConditionType.Picklist,
                operators: [],
                options: [],
                labelKey: "label",
                valueKey: "value",
            };
        }
    }

    readonly ruleConditionType = RuleConditionType;
    fieldModel: IRuleConditionMeta;
    fieldModelFiltered: IRuleConditionMeta;
    conditionMetaFiltered: IRuleConditionMeta[];
    operatorModel: IConditionOperator;
    valueModel: IRuleAction;
    errors = {
        field: false,
        operator: false,
        value: false,
        orgValue: false,
    };
    saveValidated: boolean;

    constructor(
        private readonly incidentRulesHelperService: IncidentRulesHelperService,
        private readonly filterPipe: FilterPipe,
    ) { }

    ngOnInit() {
        this.fieldModelFiltered = cloneDeep(this.fieldModel);
        this.incidentRulesHelperService.contentSource.asObservable().subscribe((res: {saveValidated: boolean}) => {
            this.saveValidated = res.saveValidated;
            if (this.saveValidated) this.validateConditions();
        });
    }

    validateConditions() {
        if (((this.fieldModel && this.fieldModel.field)
                && (this.operatorModel && this.operatorModel.operator)
                && (this.valueModel && this.valueModel.value))
            || ((!this.fieldModel || !this.fieldModel.field)
                && (!this.operatorModel || !this.operatorModel.operator)
                && (!this.valueModel || !this.valueModel.value))
        ) {
            this.errors = {
                field: false,
                operator: false,
                value: false,
                orgValue: false,
            };
            return;
        }

        if (!this.fieldModel || !this.fieldModel.field) {
            this.errors = {
                ...this.errors,
                field: true,
            };
        }

        if (!this.operatorModel || !this.operatorModel.operator) {
            this.errors = {
                ...this.errors,
                operator: true,
            };
        }

        if (!this.valueModel || !this.valueModel.value) {
            this.errors = {
                ...this.errors,
                value: true,
            };
        }

        if (!this.valueModel || !this.valueModel.value && this.fieldModel.field === this.ruleConditionType.OrgSelect) {
            this.errors = {
                ...this.errors,
                orgValue: true,
            };
        }
    }

    addCondition() {
        this.incidentRulesHelperService.addCondition(
            this.ruleIndex,
            this.subRuleIndex,
        );
    }

    removeCondition() {
        this.incidentRulesHelperService.removeCondition(
            this.ruleIndex,
            this.subRuleIndex,
            this.conditionIndex,
        );
    }

    filterInput({ key, value }, fieldName: string) {
        if (key === "Enter") { return; }
        if (!value) {
            value = "";
        }
        value = value.toUpperCase();
        if (fieldName === "field") {
            this.conditionMetaFiltered = this.filterPipe.transform(this.conditionMeta, ["fieldDisplay"], value);
        } else if (fieldName === "operator") {
            if (this.fieldModel.operators.length) {
                this.fieldModelFiltered.operators = this.filterPipe.transform(this.fieldModel.operators, ["operatorDisplay"], value);
            }
        } else if (fieldName === "options") {
            if (this.fieldModel.options.length) {
                this.fieldModelFiltered.options = this.filterPipe.transform(this.fieldModel.options, ["key"], value);
            }
        }
    }

    fieldSelect(field: IRuleConditionMeta) {
        this.errors.field = false;
        this.fieldModel = field;
        this.operatorSelect(null);
        this.valueSelect(null);
        if (field) {
            this.fieldModelFiltered = cloneDeep(this.fieldModel);
        } else {
            this.fieldModelFiltered = null;
        }
        this.incidentRulesHelperService.setConditionField(
            this.ruleIndex,
            this.subRuleIndex,
            this.conditionIndex,
            field && field.field ? field.field : null,
        );
    }

    operatorSelect(operator: IConditionOperator) {
        this.errors.operator = false;
        this.operatorModel = operator;
        this.incidentRulesHelperService.setConditionOperator(
            this.ruleIndex,
            this.subRuleIndex,
            this.conditionIndex,
            operator ? operator.operator : null,
        );
    }

    valueSelect(value: any, type?: RuleConditionType) {
        this.errors.value = false;
        this.valueModel = value;
        if (type === RuleConditionType.OrgSelect) {
            this.errors.orgValue = false;
            this.incidentRulesHelperService.setConditionValue(
                this.ruleIndex,
                this.subRuleIndex,
                this.conditionIndex,
                value ? value : null,
            );
        } else {
            this.incidentRulesHelperService.setConditionValue(
                this.ruleIndex,
                this.subRuleIndex,
                this.conditionIndex,
                value ? value[this.fieldModel.valueKey] : null,
            );
        }
    }
}
