// Angular
import {
    Component,
    Input,
    Output,
    EventEmitter,
    OnInit,
} from "@angular/core";

// Interfaces
import {
    IRule,
    IRulesConfig,
    IRuleValidate,
    IRuleDeleteConfirmationModalResolve,
} from "incidentModule/shared/incident-rules/incident-rules.interface";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

// Service
import { IncidentRulesHelperService } from "incidentModule/shared/incident-rules/incident-rules-helper.service";

// 3rd party
import { cloneDeep } from "lodash";
@Component({
    selector: "incident-rules",
    templateUrl: "./incident-rules.component.html",
    host: {
        "class": "flex-full-height",
        "[class.background-white]": "!editMode",
    },
})
export class IncidentRulesComponent implements OnInit {
    @Input() config: IRulesConfig;
    @Input("rules") set _rules(rules: IRule[]) {
        this.rules = rules;
        this.incidentRulesHelperService.setUpdate(() => {
            this.rules = this.incidentRulesHelperService.getRules();
            this.enableReorderRules = this.rules.length > 1;
        });
        this.incidentRulesHelperService.setRules(rules);
    }
    @Input() editMode: boolean;
    @Input() deleteConfig: IRuleDeleteConfirmationModalResolve;
    @Output() editModeChanged = new EventEmitter<boolean>();
    @Output() rulesSaved = new EventEmitter<IRule[]>();
    @Output() ruleOpened = new EventEmitter<string>();
    @Output() ruleCancelled = new EventEmitter<string>();
    enableReorderRules: boolean;
    rules: IRule[];
    validationRules: boolean[];
    disableConditionBlock: boolean;

    readonly translations = {
        AND: this.translatePipe.transform("DSARWebformRules.And"),
        EditRules: this.translatePipe.transform("EditRules"),
        AddRule: this.translatePipe.transform("AddRule"),
        AddCondition: this.translatePipe.transform("AddCondition"),
        Conditions: this.translatePipe.transform("Conditions"),
        Condition: this.translatePipe.transform("Condition"),
        Cancel: this.translatePipe.transform("Cancel"),
        OR: this.translatePipe.transform("Or").toUpperCase(),
        RuleName: this.translatePipe.transform("RuleName"),
        AddConditionBlock: this.translatePipe.transform("AddConditionBlock"),
        Save: this.translatePipe.transform("Save"),
        Field: this.translatePipe.transform("DSARWebformRules.Field"),
        Operator: this.translatePipe.transform("DSARWebformRules.Operator"),
        Value: this.translatePipe.transform("DSARWebformRules.Value"),
        ThenPerformTheFollowingTasks: this.translatePipe.transform("ThenPerformTheFollowingTasks"),
        Actions: this.translatePipe.transform("Actions"),
        Action: this.translatePipe.transform("Action"),
    };

    constructor(
        private readonly incidentRulesHelperService: IncidentRulesHelperService,
        private readonly translatePipe: TranslatePipe,
    ) {}

    ngOnInit() {
        this.incidentRulesHelperService.resetSaveValidated();
        this.incidentRulesHelperService.setActionParamterValidations(cloneDeep(this.config.task.subOptions));
    }

    editRules() {
        this.resetValidation();
        this.incidentRulesHelperService.resetSaveValidated();
        this.editMode = true;
        this.disableConditionBlock = true;
        this.editModeChanged.emit(true);
        this.incidentRulesHelperService.toggleAllRules(true);
    }

    addRule() {
        this.resetValidation();
        this.incidentRulesHelperService.addRule(this.config.entityTypeId);
        this.disableConditionBlock = true;
        this.editMode = true;
        this.editModeChanged.emit(true);
    }

    cleanUpRules() {
        return this.rules.map((rule) => {
            if (rule.conditionGroups) {
                const cleanedConditionGroups = [];
                rule.conditionGroups.map((conditionGroup, index) => {
                    if (index && conditionGroup.conditions) {
                        if (conditionGroup.conditions[0].field) {
                            cleanedConditionGroups.push(conditionGroup);
                        }
                    } else {
                        cleanedConditionGroups.push(conditionGroup);
                    }
                });
                rule.conditionGroups = cleanedConditionGroups;
            }

            return rule;
        });
    }

    saveRules() {
        const validate: IRuleValidate = this.incidentRulesHelperService.saveChanges();
        this.validationRules = validate.validationRules.length ? [...validate.validationRules] : this.validationRules;
        if (validate.isValid) {
            this.editMode = false;
            this.rulesSaved.emit(this.cleanUpRules());
        }
    }

    cancelRules() {
        this.resetValidation();
        this.ruleCancelled.emit("");
        this.editMode = false;
    }

    resetValidation() {
        this.validationRules = null;
    }

    trackByFn(index: number, rule: IRule): string {
        return rule.id;
    }
}
