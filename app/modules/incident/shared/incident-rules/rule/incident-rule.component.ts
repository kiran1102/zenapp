// Angular
import {
    Component,
    Input,
    Output,
    EventEmitter,
} from "@angular/core";

// Interfaces
import {
    IRule,
    IRulesConfig,
    ISubRule,
    IRuleDeleteConfirmationModalResolve,
    IRuleOperatorOption,
} from "incidentModule/shared/incident-rules/incident-rules.interface";
import { IStringMap } from "interfaces/generic.interface";

// Service
import { IncidentRulesHelperService } from "incidentModule/shared/incident-rules/incident-rules-helper.service";

@Component({
    selector: "incident-rule",
    templateUrl: "./incident-rule.component.html",
})
export class IncidentRuleComponent {
    @Input() readonly ruleIndex: number;
    @Input() readonly translations: IStringMap<string>;
    @Input() readonly editMode: boolean;
    @Input() readonly enableReorderRules: boolean;
    @Input() readonly disableConditionBlock: boolean;
    @Input() readonly rule: IRule;
    @Input() readonly config: IRulesConfig;
    @Input() hasError: boolean;
    @Input() deleteConfig: IRuleDeleteConfirmationModalResolve;
    @Output() resetValidation = new EventEmitter<null>();
    conditionBlockFlag: boolean;
    actionsLengthBadgeValue: string;
    conditionsLengthBadgeValue: string;

    constructor(
        private readonly incidentRulesHelperService: IncidentRulesHelperService,
    ) {}

    validateActionParameter(configItem) {
        if (!this.hasError) {
            return false;
        } else if (!!this.rule.actionParameters &&
             !!this.rule.actionParameters[configItem.field]) {
                if (Array.isArray(this.rule.actionParameters[configItem.field]) && this.rule.actionParameters[configItem.field].some((value) => value)) {
                    return false;
                } else if (!Array.isArray(this.rule.actionParameters[configItem.field]) && this.rule.actionParameters[configItem.field] instanceof Object && this.rule.actionParameters[configItem.field] !== null) {
                    return false;
                } else if (!Array.isArray(this.rule.actionParameters[configItem.field]) && !!this.rule.actionParameters[configItem.field].trim()) {
                    return false;
                }
            }
        return true;
    }

    ngOnInit() {
        this.getConditionGroupLength();
        this.getActionsLength();
        this.manageConditionsBlock();
    }

    ngOnChanges() {
        if (this.editMode) {
            this.manageConditionsBlock();
        } else {
            this.conditionBlockFlag = this.editMode;
        }
    }

    setRuleName(name: string) {
        this.incidentRulesHelperService.setRuleName(this.ruleIndex, name);
        this.resetValidation.emit();
    }

    setRuleOperator(type: IRuleOperatorOption) {
        this.incidentRulesHelperService.setRuleOperator(this.ruleIndex, type);
        this.resetValidation.emit();
    }

    setRuleTaskType(task: any) {
        if (this.rule.taskType === null || this.rule.taskType.label !== task.label) {
            this.incidentRulesHelperService.setRuleTaskType(this.ruleIndex, task);
            this.resetValidation.emit();
        }
    }

    deleteRule() {
        this.incidentRulesHelperService.deleteRule(this.ruleIndex, this.deleteConfig);
        this.resetValidation.emit();
    }

    shiftRuleUp() {
        this.incidentRulesHelperService.ruleShiftUp(this.ruleIndex);
        this.resetValidation.emit();
    }

    shiftRuleDown() {
        this.incidentRulesHelperService.ruleShiftDown(this.ruleIndex);
        this.resetValidation.emit();
    }

    addSubRule() {
        this.incidentRulesHelperService.addConditionBlock(this.ruleIndex);
        this.resetValidation.emit();
    }

    toggleRuleContent(ruleContent: boolean) {
        this.rule.isOpen = ruleContent;
    }

    trackByFn(index: number, subRule: ISubRule): string {
        return subRule.id;
    }

    disableConditionsBlock(event: boolean) {
        if (this.rule.conditionGroups.length <= 1) {
            this.conditionBlockFlag = event;
        }
    }

    manageConditionsBlock() {
        if (this.rule.conditionGroups[0].conditions[0].field === "" && this.editMode) {
            this.conditionBlockFlag = this.disableConditionBlock;
        }
    }

    getConditionGroupLength() {
        const conditionsLength = this.rule.conditionGroups[0].conditions[0].field === "" ? 0 : this.rule.conditionGroups.length;
        this.conditionsLengthBadgeValue = conditionsLength === 1 ? conditionsLength + " " + this.translations.Condition : conditionsLength + " " + this.translations.Conditions;
    }

    getActionsLength() {
        const actionsLength = 1; // Currently we support only one action.
        this.actionsLengthBadgeValue = actionsLength === 1 ? actionsLength + " " + this.translations.Action : actionsLength + " " + this.translations.Actions;
    }
}
