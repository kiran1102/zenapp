// Rxjs
import { BehaviorSubject } from "rxjs";

// Angular
import { Injectable, EventEmitter } from "@angular/core";

// Services
import { ModalService } from "sharedServices/modal.service";

// Interfaces
import {
    IRule,
    IRuleOperatorOption,
    IConditionGroup,
    ICondition,
    IRuleValidate,
    IRuleDeleteConfirmationModalResolve,
} from "incidentModule/shared/incident-rules/incident-rules.interface";

// Constants & Enums
import {
    RuleStatus,
    LogicalOperator,
    RuleEventType,
} from "incidentModule/shared/incident-rules/incident-rules.enum";
import { Regex } from "constants/regex.constant";

@Injectable()
export class IncidentRulesHelperService {

    contentSource = new BehaviorSubject({saveValidated: false});
    private rules: IRule[];
    private update: () => void;
    private updateOrgUserOrg: (org: string) => void;
    private internalId = 100000000000;
    private actionParameterValidations: any;

    constructor(
        readonly modalService: ModalService,
    ) {}

    getRules(): IRule[] {
        return this.rules;
    }

    setRules(rules: IRule[]) {
        this.rules = rules;
        this.update();
    }

    setUpdate(update: () => void) {
        this.update = update;
    }

    updateRules() {
        this.update();
    }

    saveChanges(): IRuleValidate {
        this.contentSource.next({saveValidated: true});
        const validate: IRuleValidate = this.areChangesValid();
        if (validate.isValid) {
            this.update();
            return validate;
        }
        return {
            validationRules: validate.validationRules,
            isValid: false,
        };
    }

    resetSaveValidated() {
        this.contentSource.next({saveValidated: false});
    }

    setActionParamterValidations(subOptions: any) {
        this.actionParameterValidations = subOptions;
    }

    areChangesValid(): IRuleValidate {
        const validationRules: boolean[] = [];
        let isValid = true;
        this.rules.forEach((rule: IRule, index: number) => {
            rule.name = rule.name.trim();
            validationRules[index] = !!rule.name && this.validateActions(rule) && this.validateConditionGroups(rule.conditionGroups);
            if (isValid) {
                isValid = validationRules[index];
            }
        });
        return { validationRules, isValid };
    }

    validateActions(rule: IRule): boolean {
        const task = rule.taskType && rule.taskType.value;
        if (!task) {
            return false;
        }
        if (!this.actionParameterValidations || !Object.keys(this.actionParameterValidations).length) {
            return true;
        } else {
            let isValid = true;
            Object.keys(this.actionParameterValidations).map((key) => {
                if (task === key) {
                    const action = this.actionParameterValidations[key];
                    action.map((actionParameter) => {
                        if (isValid && actionParameter.required) {
                            if (Array.isArray(rule.actionParameters[actionParameter.field])) {
                                isValid = !!rule.actionParameters[actionParameter.field] && rule.actionParameters[actionParameter.field].some((value) => value);
                            } else if (rule.actionParameters[actionParameter.field] instanceof Object) {
                                isValid = !!rule.actionParameters[actionParameter.field] && rule.actionParameters[actionParameter.field] !== null;
                            } else if (!Array.isArray(rule.actionParameters[actionParameter.field])) {
                                isValid = !!rule.actionParameters[actionParameter.field] && !!this.validateActionParameterValues(rule.actionParameters[actionParameter.field]).trim();
                            }
                        }
                    });
                }
            });
            return isValid;
        }
    }

    validateActionParameterValues(value: any): string {
        if (value && value.match(Regex.REMOVE_HTML_TAGS)) {
            const validateValue = value;
            return validateValue.replace(Regex.REMOVE_HTML_TAGS, "").trim() === "" ? "" : value;
        }
        return value;
    }

    validateConditionGroups(conditionGroups: IConditionGroup[]): boolean {
        return conditionGroups.every((conditionGroup) => {
            return this.validateConditions(conditionGroup.conditions);
        });
    }

    validateConditions(conditions: ICondition[]): boolean {
        return conditions.every((condition) => {
            const { field, operator, value } = condition;
            if ((field && operator && value)
                || (!field && !operator && !value)
            ) {
                return true;
            }

            return false;
        });
    }

    addRule(ruleEntityId) {
        this.contentSource.next({saveValidated: false});
        this.rules.push({
            id: null,
            ruleEntityId,
            name: "",
            status: RuleStatus.ACTIVE,
            isNew: true,
            isOpen: true,
            eventType : RuleEventType.STAGE_ENTER,
            conditionGroups: [
                {
                    conditions: [
                        {
                            id: this.generateUniqueId(),
                            field: "",
                            operator: null,
                            value: "",
                            logicalOperator: LogicalOperator.OR,
                        },
                    ],
                    logicalOperator: LogicalOperator.AND,
                },
            ],
            taskType: null,
            actionParameters: {},
        });
        this.update();
    }

    deleteRule(ruleIndex: number, deleteConfig?: IRuleDeleteConfirmationModalResolve) {
        deleteConfig.promiseToResolve = (): Promise<boolean> => {
            return deleteConfig.callbackPromise()
                .then(() => {
                    this.rules.splice(ruleIndex, 1);
                    this.update();
                    return true;
                });
        };

        this.modalService.setModalData(deleteConfig);
        this.modalService.openModal("otDeleteConfirmationModal");
    }

    toggleAllRules(expand) {
        this.rules.forEach((rule) => {
            rule.isOpen = expand;
        });
    }

    ruleShiftUp(ruleIndex: number) {
        if (ruleIndex !== 0) {
            const tempRule = this.rules[ruleIndex - 1];
            this.rules[ruleIndex - 1] = this.rules[ruleIndex];
            this.rules[ruleIndex] = tempRule;
            this.update();
        }
    }

    ruleShiftDown(ruleIndex: number) {
        if (ruleIndex !== this.rules.length - 1) {
            const tempRule = this.rules[ruleIndex + 1];
            this.rules[ruleIndex + 1] = this.rules[ruleIndex];
            this.rules[ruleIndex] = tempRule;
            this.update();
        }
    }

    setRuleName(ruleIndex: number, name: string) {
        this.rules[ruleIndex].name = name;
    }

    setRuleOperator(ruleIndex: number, type: IRuleOperatorOption) {
        this.rules[ruleIndex].operator = type;
        const operator = type.value === "any" ? LogicalOperator.OR : LogicalOperator.AND;
        this.rules[ruleIndex].conditionGroups.forEach((conditionGroup: IConditionGroup) => {
            conditionGroup.logicalOperator = operator;
        });
    }

    setRuleTaskType(ruleIndex: number, task: any) {
        this.rules[ruleIndex].taskType = { label: task.label, value: task.value };
        this.rules[ruleIndex].actionParameters = {};
    }

    addConditionBlock(ruleIndex: number) {
        this.rules[ruleIndex].conditionGroups.push(
            {
                conditions: [
                    {
                        id: this.generateUniqueId(),
                        field: "",
                        operator: null,
                        value: "",
                        logicalOperator: LogicalOperator.OR,
                    },
                ],
                logicalOperator: LogicalOperator.AND,
            },
        );
        this.update();
    }

    removeConditionBlock(ruleIndex: number, subRuleIndex: number, conditionBlockFlag: EventEmitter<boolean>) {
        this.rules[ruleIndex].conditionGroups.splice(subRuleIndex, 1);
        if (this.rules[ruleIndex].conditionGroups.length === 0 && subRuleIndex === 0) {
            conditionBlockFlag.emit(true);
            this.addConditionBlock(ruleIndex);
        }
    }

    addCondition(ruleIndex: number, conditionBlockIndex: number) {
        this.rules[ruleIndex].conditionGroups[conditionBlockIndex].conditions.push(
            {
                id: this.generateUniqueId(),
                field: "",
                operator: null,
                value: "",
                logicalOperator: LogicalOperator.OR,
            },
        );
    }

    removeCondition(ruleIndex: number, subRuleIndex: number, conditionIndex: number) {
        this.rules[ruleIndex].conditionGroups[subRuleIndex].conditions.splice(conditionIndex, 1);
        if (!this.rules[ruleIndex].conditionGroups[subRuleIndex].conditions.length) {
            this.addCondition(ruleIndex, subRuleIndex);
        }
    }

    setConditionField(ruleIndex: number, subRuleIndex: number, conditionIndex: number, field: string) {
        this.rules[ruleIndex].conditionGroups[subRuleIndex].conditions[conditionIndex].field = field;
    }

    setConditionOperator(ruleIndex: number, subRuleIndex: number, conditionIndex: number, operator: string) {
        this.rules[ruleIndex].conditionGroups[subRuleIndex].conditions[conditionIndex].operator = operator;
    }

    setConditionValue(ruleIndex: number, subRuleIndex: number, conditionIndex: number, value: string) {
        this.rules[ruleIndex].conditionGroups[subRuleIndex].conditions[conditionIndex].value = value;
    }

    setActionValue(ruleIndex: number, field: string, value: any, actionIndex?: number) {
        this.rules[ruleIndex].actionParameters[field] = value;
    }

    setOrgUserCallback(updateOrgUserOrg: (org: string) => void) {
        this.updateOrgUserOrg = updateOrgUserOrg;
    }

    updateOrg(org: string) {
        this.updateOrgUserOrg(org);
    }

    private generateUniqueId() {
        return `00000000-0000-0000-0000-${this.internalId++}`;
    }
}
