import { RuleEventType } from "incidentModule/shared/incident-rules/incident-rules.enum";

export interface IIncidentRuleGetBody {
    ruleKey1: string;
    ruleKey2: string;
    ruleKey3: string;
    ruleEventType: RuleEventType;
}
