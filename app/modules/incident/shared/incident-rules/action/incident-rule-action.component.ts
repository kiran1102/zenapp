// Angular
import {
    Component,
    Input,
    Inject,
} from "@angular/core";

// 3rd Party
import { find } from "lodash";

// Interfaces
import { IRuleActionConfig } from "incidentModule/shared/incident-rules/incident-rules.interface";
import { IStore } from "interfaces/redux.interface";
import { IOtOrgUserOutput } from "sharedModules/components/ot-org-user/ot-org-user.interface";
import { IUser } from "interfaces/user.interface";
import { IOrgUserAdapted } from "interfaces/org-user.interface";

// Constants & Enums
import { RuleActionType } from "incidentModule/shared/incident-rules/incident-rules.enum";

// Service
import { IncidentRulesHelperService } from "incidentModule/shared/incident-rules/incident-rules-helper.service";

// Store & Reducer
import { StoreToken } from "tokens/redux-store.token";
import { getUserById } from "oneRedux/reducers/user.reducer";

@Component({
    selector: "incident-rule-action",
    templateUrl: "./incident-rule-action.component.html",
})
export class IncidentRuleActionComponent {
    @Input() readonly ruleIndex: number;
    @Input() readonly actionIndex: number;
    @Input() readonly editMode: boolean;
    @Input() readonly hasError: boolean;
    @Input() readonly config: IRuleActionConfig;
    @Input() set actions(actions: any) {
        const action = actions[this.config.field];
        if (this.config.type === RuleActionType.Picklist) {
            this.valueModel = action ? find(this.config.picklistOptions, [this.config.picklistValueKey, action.value]) : this.config.defaultValue;
        } else if (this.config.orgSelectChoosesUserOrg && this.config.type === RuleActionType.OrgUser) {
            this.incidentRulesHelperService.setOrgUserCallback((org: string) => {
                this.orgUserOrg = org;
            });
            this.orgUserOrg = this.config.orgSelectDefaultValue;
            const user: IUser | string = action ? getUserById(action.value)(this.store.getState()) : "";
            this.valueModel = user ? (user as IUser).Id : "";
        } else if (this.config.type === RuleActionType.OrgUserMultiSelect) {
            this.mutiOrgUsers = action ? action : [];
        } else {
            this.valueModel = action ? action : this.config.defaultValue;
        }
    }

    readonly ruleActionType = RuleActionType;
    valueModel: any;
    orgUserOrg: string;
    mutiOrgUsers: IOrgUserAdapted[] = [];

    constructor(
        private readonly incidentRulesHelperService: IncidentRulesHelperService,
        @Inject(StoreToken) private store: IStore,
    ) { }

    valueSet(value: any) {
        this.incidentRulesHelperService.setActionValue(this.ruleIndex, this.config.field, value, this.actionIndex);
    }

    numberValueSet(event: any) {
        let value = parseInt(event.target.value, 0);
        if ((value > this.config.inputNumberMax) || (value < this.config.inputNumberMin) || !value) {
            value = this.config.inputNumberMin;
        }
        this.valueModel = value;
        this.incidentRulesHelperService.setActionValue(this.ruleIndex, this.config.field, value);
    }

    picklistValueSet(value: any) {
        this.valueModel = value;
        this.valueSet(value[this.config.picklistValueKey]);
    }

    orgSelectValueSet(value: any) {
        this.valueModel = value;
        if (this.config.orgSelectChoosesUserOrg) {
            this.incidentRulesHelperService.updateOrg(value);
        }
        this.valueSet(value);
    }

    orgUserValueSet(value: IOtOrgUserOutput) {
        const id = value && value.selection ? value.selection.Id : null;
        this.valueModel = id;
        this.valueSet(id);
    }

    multiOrgUserValueSet(value: IOrgUserAdapted[]) {
        if (value) {
            const multiOrgUserValue = value.map((user: IOrgUserAdapted) => {
                if (user) {
                    return { respondentId: user.Id, respondentName: user.FullName };
                }
            });
            this.valueSet(multiOrgUserValue);
        }
    }
}
