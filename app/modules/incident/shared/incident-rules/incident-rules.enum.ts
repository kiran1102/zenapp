export enum RuleActionType {
    Toggle,
    Checkbox,
    InputNumber,
    InputString,
    Picklist,
    OrgUserMultiSelect,
    OrgSelect,
    OrgUser,
    TextArea,
    RichText,
}

export enum RuleTaskType {
    Picklist,
}

export enum LogicalOperator {
    NONE = "NONE",
    AND = "AND",
    OR = "OR",
}

export enum RuleOperator {
    EQUAL_TO = "EQUAL_TO",
    NOT_EQUAL_TO = "NOT_EQUAL_TO",
    IN = "IN",
    NOT_IN = "NOT_IN",
    GREATER_THAN = "GREATER_THAN",
    GREATER_THAN_EQUAL_TO = "GREATER_THAN_EQUAL_TO",
    LESS_THAN_EQUAL_TO = "LESS_THAN_EQUAL_TO",
    LESS_THAN = "LESS_THAN",
}

export enum RuleConditionType {
    OrgSelect = "OrgSelect",
    Picklist = "Picklist",
}

export enum RuleEventType {
    STAGE_ENTER = "STAGE_ENTER",
}

export enum RuleStatus {
    ACTIVE = 0,
    INACTIVE = 1,
}
