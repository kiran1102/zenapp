// Angular
import {
    Component,
    Input,
    Output,
    EventEmitter,
} from "@angular/core";

// Interfaces
import {
    ISubRule,
    IRuleConditionMeta,
    IRuleCondition,
} from "incidentModule/shared/incident-rules/incident-rules.interface";
import { IStringMap } from "interfaces/generic.interface";

// Service
import { IncidentRulesHelperService } from "incidentModule/shared/incident-rules/incident-rules-helper.service";

@Component({
    selector: "incident-sub-rule",
    templateUrl: "./incident-sub-rule.component.html",
})
export class IncidentSubRuleComponent {
    @Input() readonly editMode: boolean;
    @Input() readonly ruleIndex: number;
    @Input() readonly subRuleIndex: number;
    @Input() readonly translations: IStringMap<string>;
    @Input() readonly subRule: ISubRule;
    @Input() readonly conditionMeta: IRuleConditionMeta[];
    @Output() conditionBlockFlag = new EventEmitter<boolean>();

    constructor(
        private readonly incidentRulesHelperService: IncidentRulesHelperService,
        ) { }

    removeSubRule() {
        this.incidentRulesHelperService.removeConditionBlock(
            this.ruleIndex,
            this.subRuleIndex,
            this.conditionBlockFlag,
        );
    }

    trackByFn(index: number, condition: IRuleCondition): string {
        return condition.id;
    }
}
