import {
    RuleActionType,
    RuleOperator,
    LogicalOperator,
} from "incidentModule/shared/incident-rules/incident-rules.enum";
import { OrgUserTraversal } from "enums/org-user-dropdown.enum";
import { IOrganization } from "interfaces/org.interface";

export interface IRuleMeta {
    id?: string;
    name: string;
    status?: number;
    operator?: any;
    taskType?: any;
    isNew?: boolean;
    delete?: boolean;
    oldId?: string;
    isOpen?: boolean;
    eventType?: string;
    ruleEntityId?: string;
    sequence?: number;
}

export interface IRule extends IRuleMeta {
    conditionGroups?: IConditionGroup[];
    actions?: any;
    taskActions?: IRuleAction[];
    actionParameters?: any;
}

export interface IConditionGroup {
    conditions: ICondition[];
    logicalOperator: LogicalOperator;
}

export interface ICondition {
    id: string;
    field: string;
    operator: string;
    value: string;
    logicalOperator: LogicalOperator;
}

export interface ISubRule {
    id: string;
    conditions: IRuleCondition[];
}

export interface IRuleCondition {
    id: string;
    field: string;
    operator: string;
    value: string;
}

export interface IRuleAction {
    value: any;
    field?: string;
    key?: string;
    searchValue?: string;
}

export interface IRuleOperator {
    options: IRuleOperatorOption[];
    placeholder?: string;
    valueKey: string;
    labelKey: string;
}

export interface IRuleOperatorOption {
    label: string;
    value: string;
}

export interface IRuleActionOption {
    label: string;
    value: string;
    required?: boolean;
}

export interface IRulesConfig {
    entityTypeId?: string;
    conditionMeta: IRuleConditionMeta[];
    actionMeta?: IRuleActionMeta;
    actions?: IRuleActionConfig[];
    task?: any;
    ruleOperators?: IRuleOperator;
}

export interface IRuleActionMeta {
    actionTypes: IRuleActionOption[];
    actionOptions: any;
}

export interface IRuleActionConfig {
    field: string;
    fieldDisplay: string;
    defaultValue: any;
    type: RuleActionType;
    required: boolean;
    inputNumberMin?: number;
    inputNumberMax?: number;
    picklistOptions?: Array<{}>;
    picklistLabelKey?: string;
    picklistValueKey?: string;
    orgSelectOptions?: IOrganization[];
    orgSelectChoosesUserOrg?: boolean;
    orgSelectDefaultValue?: string;
    orgUserOrgId?: string;
    orgUserTraversal?: OrgUserTraversal;
}

export interface IRuleConditionMeta {
    field: string;
    fieldDisplay: string;
    operators: IConditionOperator[];
    options: any[];
    type: string;
    labelKey: string;
    valueKey: string;
}

export interface IConditionOperator {
    operator: RuleOperator;
    operatorDisplay: string;
}

export interface IRuleDeleteConfirmationModalResolve {
    promiseToResolve?: () => Promise<any>;
    modalTitle: string;
    confirmationText: string;
    cancelButtonText?: string;
    submitButtonText?: string;
    customConfirmationText?: string;
    customKeyText?: string;
    cancelIdentifier?: string;
    deleteIdentifier?: string;
    modalIcon?: string;
    modalIconTextClass?: string;
    callback?: () => void;
    callbackPromise: () => Promise<boolean>;
}

export interface IRuleValidate {
    validationRules: boolean[];
    isValid: boolean;
}

export interface IRuleActionRespondents {
    respondentId: string;
    respondentName: string;
}

export interface IRuleActionRecipients {
    recipientId: string;
    recipientName: string;
}

export interface IRuleActionTemplates {
    templateRootVersionId: string;
    templateName: string;
}
