import { IAuditChange } from "interfaces/audit-history.interface";

export interface IIncidentActivityState {
    isLoadingIncidentActivity: boolean;
    activityList: IIncidentActivity[];
    activityListMeta: IIncidentActivityMeta;
}

export interface IIncidentActivityPageable {
    content: IIncidentActivity[];
    first: boolean;
    last: boolean;
    number: number;
    numberOfElements: number;
    size: number;
    sort: any;
    totalElements: boolean;
    totalPages: boolean;
    incidentId?: string;
}

export interface IIncidentActivity {
    changeDateTime: string;
    changes: IAuditChange[];
    description: string;
    objectDetail: string;
    objectId: string;
    recordedDateTime: string;
    userId: string;
    userName: string;
}

export interface IIncidentActivityChangeValue {
    associatedObjectId: string;
    name: string;
}

export interface IIncidentActivityMeta {
    first: boolean;
    last: boolean;
    number: number;
    numberOfElements: number;
    size: number;
    sort: any;
    totalElements: boolean;
    totalPages: boolean;
    incidentId?: string;
}
