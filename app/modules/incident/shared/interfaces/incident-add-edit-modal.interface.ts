import { IIncidentDetail } from "incidentModule/shared/interfaces/incident-detail-state.interface";

export interface IIncidentAddEditModalData {
    parentId: string;
    incidentRegisterLength: number;
    modalTitle: string;
    actionButtonTitle: string;
    cancelButtonTitle: string;
    incidentDetail: IIncidentDetail;
    callback: () => void;
}
