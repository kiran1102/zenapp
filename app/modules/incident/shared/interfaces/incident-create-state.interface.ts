import { IIncidentJurisdiction } from "incidentModule/shared/interfaces/incident-jurisdiction.interface";

export interface IIncidentUpdateRequest {
    name: string;
    assigneeId: string;
    dateOccurred: string;
    dateDiscovered: string;
    deadline: string;
    description: string;
    incidentTypeId: string;
    orgGroupId: string;
    rootCause: string;
    jurisdictions?: IIncidentJurisdiction[];
}

export interface IIncidentCreateRequest extends IIncidentUpdateRequest {
    sourceType: string;
}

export interface IIncidentAssessmentLink {
    incidentId: string;
    incidentName: string;
    assessmentIds: string[];
}
