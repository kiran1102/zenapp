import { PathItem } from "@onetrust/vitreus";

export interface IStagePathItem extends PathItem {
    id: string;
}

export interface IIncidentPath {
    pathItems: IStagePathItem[];
    activeIndex: number;
}
