import { IncidentType } from "incidentModule/shared/enums/incident-type.enums";

import { IIncidentAssessmentLink } from "incidentModule/shared/interfaces/incident-create-state.interface";
import { IIncidentJurisdiction } from "incidentModule/shared/interfaces/incident-jurisdiction.interface";

export interface IIncidentDetailState {
    isLoadingIncidentDetail: boolean;
    data: IIncidentDetail;
}

export interface IIncidentDetail {
    affectedSubjectRangeFrom: number;
    affectedSubjectRangeTo: number;
    assessmentLinks: IIncidentAssessmentLink[];
    assigneeId: string;
    assigneeName: string;
    createdBy: string;
    createdDT: string;
    creator: string;
    dateOccurred: string;
    dateDiscovered: string;
    deadline: string;
    description: string;
    id: string;
    incidentStageId: string;
    incidentStageName: IncidentType;
    incidentStageNameKey: IncidentType;
    incidentStageRulesId?: string[];
    incidentTypeId: string;
    incidentTypeName: IncidentType;
    incidentTypeNameKey: IncidentType;
    jurisdictions: IIncidentJurisdiction[];
    name: string;
    number: number;
    orgGroupId: string;
    orgGroupName: string;
    rootCause: string;
    sourceType: string;
}
