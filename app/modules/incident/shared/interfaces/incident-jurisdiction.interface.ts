import { ITableColumnConfig } from "interfaces/table-config.interface";
import { IPageableMeta } from "interfaces/pagination.interface";

export interface IIncidentJurisdictionTable {
    rows: IIncidentJurisdiction[];
    columns?: ITableColumnConfig[];
    metaData?: IPageableMeta;
}
export interface IIncidentJurisdiction {
    id?: string;
    jurisdictionId: string;
    regionId: number;
    regionName: string;
    countryId: number;
    countryName: string;
    countryCode: string;
    stateProvinceId?: number;
    stateProvinceName?: string;
    stateProvinceCode?: string;
}

export interface IJurisdiction {
    id: number;
    name: string;
    countries: IJurisdictionCountry[];
    jurisdictionId?: string;
}

export interface IJurisdictionRegions {
    regions: IJurisdiction[];
}

export interface IJurisdictionCountry {
    id: number;
    name: string;
    code: string;
    jurisdictionId: string;
    regionId: number;
    stateProvinces?: IJurisdictionStateProvince[];
}

export interface IJurisdictionStateProvince {
    id: number;
    name: string;
    code: string;
    jurisdictionId: string;
}

export interface IJurisdictionModalData {
    saveJurisdictionCB: (jurisdictions: IIncidentJurisdiction[], extraInfo?: { [key: string]: any }) => Promise<boolean>;
    currentJurisdictions: IIncidentJurisdiction[];
}

export interface IDINAAssessmentRequest {
    name: string;
    templateType: string;
    templateId: string;
    laws: string[];
    orgGroupId: string;
    orgGroupName: string;
}

export interface IDINATemplateDetail {
    id: string;
    name: string;
    description: string;
    templateType: string;
}
