import { ITableColumnConfig } from "interfaces/table-config.interface";
import { IPageableMeta } from "interfaces/pagination.interface";

export interface IIncidentAttachmentsTable {
    rows: IIncidentAttachmentsModel[];
    columns?: ITableColumnConfig[];
    metaData?: IPageableMeta;
}

export interface IIncidentAttachmentsModel {
    attachmentId?: string;
    incidentId?: string;
    link: string;
    name: string;
    description?: string;
    type: string;
    creator?: {
        id: string,
        name: string,
    };
}
