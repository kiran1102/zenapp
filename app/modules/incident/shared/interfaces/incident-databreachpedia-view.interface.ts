export interface IIncidentDBPLaw {
    id: string;
    name: string;
    displayName: string;
    description: string;
}

export interface IIncidentDBPLawMeta {
    law: IIncidentDBPLaw;
    lawSections: string[];
    isActive?: boolean;
}

export interface IIncidentDBPLawContent {
    law: IIncidentDBPLaw;
    jurisdictions: IIncidentDBPLawJurisdictions[];
    lawContents: Array<{[ key: string ]: string}>;
}

export interface IIncidentDBPLawJurisdictions {
    id: string;
    name: string;
}

export interface IDatabreachLawRegions {
    regions: IDatabreachLawRegion[];
}

export interface IDatabreachLawRegion {
    id: number;
    name: string;
    lawMap: IDatabreachLawMap[];
    laws: IDatabreachLawMap[];
}

export interface IDatabreachLawMap {
    id: number;
    description: string;
    displayName: string;
    name: string;
}
