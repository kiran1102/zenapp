import { ICardTitle } from "@onetrust/vitreus";
import { IDropdownOption } from "interfaces/one-dropdown.interface";
import { IncidentTabOptions } from "incidentModule/shared/enums/incident-details.enums";
import { IIncidentSubtaskCardFilter } from "incidentModule/shared/interfaces/incident-subtasks.interface";

export interface IIncidentCard {
    title: ICardTitle;
    body: any[];
    displayField: string;
    disableField?: string;
    footer: string;
    menuOptions: IDropdownOption[];
    shiftTab: IncidentTabOptions;
    editModal: IncidentTabOptions;
}

export interface IIncidentCardFilters {
    fullText: string;
    filters: IIncidentSubtaskCardFilter;
}
