import { IncidentSubTasksStateKeys } from "incidentModule/shared/enums/incident-translation-keys.enums";
import { ITableColumnConfig } from "interfaces/table-config.interface";
import { IPageableMeta } from "interfaces/pagination.interface";

export interface IIncidentSubTasksRecord {
    id: string;
    name: string;
    status: IncidentSubTasksStateKeys;
    deadline: string;
    assigneeId: string;
    description: string;
    assigneeName: string;
    incidentId: string;
}

export interface IIncidentSubTasksRecordTable {
    rows: IIncidentSubTasksRecord[];
    columns?: ITableColumnConfig[];
    metaData?: IPageableMeta;
}

export interface IIncidentSubtaskCardFilter {
    id?: string;
    name?: string;
    incidentId?: string;
    subTaskStatus?: IncidentSubTasksStateKeys;
    deadline?: string;
    assigneeId?: string;
    description?: string;
    assigneeName?: string;
}
