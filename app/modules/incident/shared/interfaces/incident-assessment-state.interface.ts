import { IPageableOf } from "interfaces/pagination.interface";
import { ITableColumnConfig } from "interfaces/table-config.interface";

export interface IIncidentListState {
    isLoadingIncidentList: boolean;
    tableData: IPageableOf<IIncidentRegister>;
}

export interface IIncidentRegister {
    assigneeId: string;
    assigneeName: string;
    createdBy: string;
    createdDate: string;
    creator: string;
    deadline: string;
    description: string;
    incidentId: string;
    incidentTypeId: string;
    incidentTypeName: string;
    isExtended: boolean;
    name: string;
    number: number;
    orgGroupId: string;
    orgGroupName: string;
    totalAssessment: number;
}

export interface IIncidentAssessmentsRecordTable {
    rows: IIncidentAssessmentRecord[];
    columns?: ITableColumnConfig[];
    metaData?: IPageableOf<IIncidentAssessmentRecord>;
}

export interface IIncidentAssessmentRecord {
    approver: string;
    assessment: string;
    assessmentId: string;
    deadline: Date;
    maxRiskLevelId: string;
    primaryLink?: boolean;
    organization: string;
    respondent: string;
    status: string;
    template: string;
}
