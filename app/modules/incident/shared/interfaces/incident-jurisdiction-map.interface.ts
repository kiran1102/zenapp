import { IncidentMapFillKey } from "incidentModule/shared/enums/incident-jurisdiction.enums";

export interface IIncidentMapData {
    [key: string]: {
        states: number,
        fillKey: IncidentMapFillKey,
    };
}
