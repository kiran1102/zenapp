import { IIncidentDetail } from "incidentModule/shared/interfaces/incident-detail-state.interface";
import { IIncidentSubTasksRecord } from "incidentModule/shared/interfaces/incident-subtasks.interface";
import { IncidentSubTasksStateKeys } from "incidentModule/shared/enums/incident-translation-keys.enums";

export interface IIncidentAddNewModalData {
    parentId: string;
    modalTitle: string;
    callback: () => void;
}

export interface IIncidentLinkAssessmentModalData {
    incidentModel: IIncidentDetail;
    modalBody: string;
    modalTitle: string;
    callback: () => void;
}

export interface IIncidentAddEditSubTasksModalData {
    incidentSubTasksModel: IIncidentSubTasksRecord;
    incidentId: string;
    orgGroupId: string;
    modalTitle: string;
    editSubTaskFlag: boolean;
    actionButtonTitle: string;
    cancelButtonTitle: string;
    callback: () => void;
}

export interface IIncidentAddEditSubTasksCreateRequest {
    name: string;
    description: string;
    incidentId: string;
    deadline: string;
    assigneeId: string;
}

export interface IIncidentAddEditSubTasksUpdateRequest extends IIncidentAddEditSubTasksCreateRequest {
    status: IncidentSubTasksStateKeys;
}
