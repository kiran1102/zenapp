import { ITableColumnConfig } from "interfaces/table-config.interface";

export interface IIncidentTypeRecord {
    id?: string;
    name: string;
    nameKey?: string;
    description?: string;
    active?: boolean;
    languageCode?: string;
}

export interface IIncidentTypesTable {
    rows: IIncidentTypeRecord[];
    columns?: ITableColumnConfig[];
}

export interface IIncidentTypeRecordUpdate extends IIncidentTypeRecord {
    isActive: boolean;
}

export interface IIncidentTypeAddModal {
    modalTitle: string;
    attributeNameKey: string;
    currentActiveOptions: IIncidentTypeRecord[];
    currentInactiveOptions: IIncidentTypeRecord[];
}
