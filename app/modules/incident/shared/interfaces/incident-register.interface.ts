import { ITableColumnConfig } from "interfaces/table-config.interface";
import { IPageableMeta } from "interfaces/pagination.interface";
import { IIncidentJurisdiction } from "./incident-jurisdiction.interface";

export interface IIncidentRegisterTable {
    rows: IIncidentRegister[];
    columns?: ITableColumnConfig[];
    metaData?: IPageableMeta;
}
export interface IIncidentRegister {
    assigneeId: string;
    assigneeName: string;
    createdBy: string;
    createdDate: string;
    creator: string;
    dateDiscovered: string;
    dateOccurred: string;
    deadline: string;
    description: string;
    incidentId: string;
    incidentStageId: string;
    incidentStageName: string;
    incidentStageNameKey: string;
    incidentTypeId: string;
    incidentTypeName: string;
    incidentTypeNameKey: string;
    jurisdictions: IIncidentJurisdiction[];
    isExtended: boolean;
    name: string;
    number: number;
    openAssessment: number;
    orgGroupId: string;
    orgGroupName: string;
    totalAssessment: number;
}

export interface IIncidentRegisterPageable {
    content: IIncidentRegister[];
    first: boolean;
    last: boolean;
    number: number;
    numberOfElements: number;
    size: number;
    sort: any[];
    totalElements: number;
    totalPages: number;
}
