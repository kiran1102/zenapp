export const IncidentMapColor = {
    defaultFillColor: "#C1C1C1",
    defaultBorderColor: "white",
    selectedFillColor: "#5BC0DE",
    highlightFillColor: "#FACAC1",
    highlightBorderColor: "#FC4F45",
};
