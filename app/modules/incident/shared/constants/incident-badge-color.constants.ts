// Temporary until there's a new Vitreus update
export enum BadgeColorTypes {
    DEFAULT = "default",
    PRIMARY = "primary",
    SUCCESS = "success",
    DESTRUCTIVE = "destructive",
    WARNING = "warning",
    HOLD = "hold",
    DISABLED = "disabled",
    ADD = "add",
    EDIT = "edit",
}

export const IncidentStageBadgeColorMap = {
    New: BadgeColorTypes.DEFAULT,
    Investigating: BadgeColorTypes.PRIMARY,
    Remediating: BadgeColorTypes.PRIMARY,
    Notifying: BadgeColorTypes.WARNING,
    Complete: BadgeColorTypes.SUCCESS,
};

export const IncidentAssessmentStatusBadgeColorMap = {
    NOT_STARTED: BadgeColorTypes.DEFAULT,
    IN_PROGRESS: BadgeColorTypes.PRIMARY,
    UNDER_REVIEW: BadgeColorTypes.WARNING,
    COMPLETED: BadgeColorTypes.SUCCESS,
};

export const IncidentSubTasksStatusColorMap = {
    OPEN: BadgeColorTypes.DEFAULT,
    CLOSED: BadgeColorTypes.SUCCESS,
};
