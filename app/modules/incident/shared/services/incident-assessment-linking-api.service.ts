import { Injectable } from "@angular/core";

// Services
import { ProtocolService } from "modules/core/services/protocol.service";

// Interfaces
import { ILinkOption } from "interfaces/inventory.interface";
import {
    IProtocolConfig,
    IProtocolMessages,
    IProtocolResponse,
} from "interfaces/protocol.interface";
import { IPaginationParams, IPageableOf } from "interfaces/pagination.interface";
import { IIncidentAssessmentRecord } from "incidentModule/shared/interfaces/incident-assessment-state.interface";
import { IIncidentAssessmentLink } from "incidentModule/shared/interfaces/incident-create-state.interface";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

@Injectable()
export class IncidentAssessmentLinkApiService {

    constructor(
        private OneProtocol: ProtocolService,
        private translatePipe: TranslatePipe,
    ) { }

    public createIncidentAssessmentLink(payload: IIncidentAssessmentLink): ng.IPromise<IProtocolResponse<string>> {
        const config: IProtocolConfig = this.OneProtocol.config("POST", `/assessmentv2/assessments/incidents/link`, null, payload);
        const messages: IProtocolMessages = {
            Error: { custom: this.translatePipe.transform("ErrorLinkingAssessment") },
            Success: { custom: this.translatePipe.transform("SuccessfullyLinkedAssessments") },
        };
        return this.OneProtocol.http(config, messages);
    }

    public getIncidentLinkAssessmentDropdownList = (incidentId: string, rootTemplateId: string): ng.IPromise<IProtocolResponse<ILinkOption[]>> => {
        const config: IProtocolConfig = this.OneProtocol.config("GET", `/assessmentv2/assessments/incidents/filter/${incidentId}/templates/${rootTemplateId}/assessments`);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("CouldNotRetrieveAssessments") } };
        return this.OneProtocol.http(config, messages);
    }

    public getIncidentLinkedAssessments = (incidentId: string,  paginationParams: IPaginationParams<any>): ng.IPromise<IProtocolResponse<IPageableOf<IIncidentAssessmentRecord>>> => {
        const config: IProtocolConfig = this.OneProtocol.config("GET", `/assessmentv2/assessments/incidents/${incidentId}/assessments`, paginationParams);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("CouldNotRetrieveAssessments") } };
        return this.OneProtocol.http(config, messages);
    }

    public removeIncidentAssessmentLink(incidentId: string, assessmentId: string): ng.IPromise<IProtocolResponse<void>> {
        const config: IProtocolConfig = this.OneProtocol.config("DELETE", `/assessmentv2/assessments/incidents/${incidentId}/assessments/${assessmentId}`);
        const messages: IProtocolMessages = {
            Error: { custom: this.translatePipe.transform("ErrorRemovingAssessmentLink") },
            Success: { custom: this.translatePipe.transform("SuccessRemovingAssessmentLink") },
        };
        return this.OneProtocol.http(config, messages);
    }
}
