import { Injectable } from "@angular/core";

// Services
import { ProtocolService } from "modules/core/services/protocol.service";

// Interfaces
import {
    IProtocolConfig,
    IProtocolMessages,
    IProtocolResponse,
} from "interfaces/protocol.interface";
import {
    IPageableOf,
    IPaginationFilter,
} from "interfaces/pagination.interface";
import { IIncidentRegister } from "incidentModule/shared/interfaces/incident-register.interface";
import {
    IIncidentCreateRequest,
    IIncidentUpdateRequest,
} from "incidentModule/shared/interfaces/incident-create-state.interface";
import { IIncidentTypeRecord } from "incidentModule/shared/interfaces/incident-type-state.interface";
import { IIncidentDetail } from "incidentModule/shared/interfaces/incident-detail-state.interface";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

@Injectable()
export class IncidentApiService {

    constructor(
        private protocolService: ProtocolService,
        private translatePipe: TranslatePipe,
    ) {}

    createIncident(newIncident: IIncidentCreateRequest): ng.IPromise<IProtocolResponse<string>> {
        const config: IProtocolConfig = this.protocolService.customConfig("POST", "/api/incident/v1/incidents", null, newIncident);
        const messages: IProtocolMessages = {
            Error: { custom: this.translatePipe.transform("Incident.Error.CreatingIncident") },
            Success: { custom: this.translatePipe.transform("Incident.Success.CreatingIncident") },
        };
        return this.protocolService.http(config, messages);
    }

    fetchIncidentRegister(fullText: string, filters: IPaginationFilter[], paginationParams): ng.IPromise<IProtocolResponse<IPageableOf<IIncidentRegister>>> {
        const filter: string = filters && filters.length > 0 ? encodeURI(JSON.stringify(filters)) : "";
        const config: IProtocolConfig = this.protocolService.customConfig("POST", "/api/incident/v1/incidents/search", { text: "", filter, ...paginationParams }, { fullText });
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("Incident.Error.RetrievingIncidents") } };
        return this.protocolService.http(config, messages);
    }

    removeIncidentRegister(incidentId: string): ng.IPromise<IProtocolResponse<string>> {
        const config: IProtocolConfig = this.protocolService.customConfig("DELETE", `/api/incident/v1/incidents/${incidentId}`);
        const messages: IProtocolMessages = {
            Error: { custom: this.translatePipe.transform("Incident.Error.DeletingIncident") },
            Success: { custom: this.translatePipe.transform("Incident.Success.DeletingIncident") },
        };
        return this.protocolService.http(config, messages);
    }

    fetchIncidentById(incidentId: string): ng.IPromise<IProtocolResponse<IIncidentDetail>> {
        const config: IProtocolConfig = this.protocolService.customConfig("GET", `/api/incident/v1/incidents/${incidentId}/incidents`);
        const messages: IProtocolMessages = { Error: { Hide: true } };
        return this.protocolService.http(config, messages);
    }

    fetchIncidentActivity(incidentId: string, page: number = 0, size: number = 8): ng.IPromise<IProtocolResponse<any>> {
        const config: IProtocolConfig = this.protocolService.customConfig("GET", `/api/audit/v1/audit/${incidentId}/history/`, {page, size});
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("Incident.Error.RetrievingIncidentActivity") } };
        return this.protocolService.http(config, messages);
    }

    fetchIncidentTypes(): ng.IPromise<IProtocolResponse<IIncidentTypeRecord[]>> {
        const config: IProtocolConfig = this.protocolService.customConfig("GET", "/api/incident/v1/incidents/types/all", null, null);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("Error.RetrievingIncidentTypes") } };
        return this.protocolService.http(config, messages);
    }

    fetchIncidentStageTypes(): ng.IPromise<IProtocolResponse<IIncidentTypeRecord[]>> {
        const config: IProtocolConfig = this.protocolService.customConfig("GET", "/api/incident/v1/incidents/stages/all", null, null);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("Error.RetrievingIncidentStagesTypes") } };
        return this.protocolService.http(config, messages);
    }

    updateIncident(incidentId: string, incident: IIncidentUpdateRequest, customMessages?: IProtocolMessages): ng.IPromise<IProtocolResponse<IIncidentDetail>> {
        const config: IProtocolConfig = this.protocolService.customConfig("PUT", `/api/incident/v1/incidents/${incidentId}/incidents`, null, incident);
        const messages: IProtocolMessages = customMessages || {
            Error: { custom: this.translatePipe.transform("Incident.Error.UpdatingIncident") },
            Success: { custom: this.translatePipe.transform("Incident.Success.UpdatingIncident") },
        };
        return this.protocolService.http(config, messages);
    }

    updateStage(incidentId: string, incidentStageId: string, rulesEnabled: boolean): ng.IPromise<IProtocolResponse<IIncidentDetail>> {
        const config: IProtocolConfig = this.protocolService.customConfig("POST", `/api/incident/v1/incidents/${incidentId}/stage`, null, {incidentStageId, rulesEnabled});
        const messages: IProtocolMessages = {
            Error: { custom: this.translatePipe.transform("Incident.Error.UpdatingIncidentStage") },
            Success: { custom: this.translatePipe.transform("Incident.Success.UpdatingIncidentStage") },
        };
        return this.protocolService.http(config, messages);
    }
}
