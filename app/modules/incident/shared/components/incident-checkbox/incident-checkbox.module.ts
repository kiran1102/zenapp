import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

// Components
import { IncidentCheckboxComponent } from "incidentModule/shared/components/incident-checkbox/incident-checkbox.component";

@NgModule({
    imports: [
        CommonModule,
    ],
    declarations: [
        IncidentCheckboxComponent,
    ],
    exports: [
        IncidentCheckboxComponent,
    ],
})
export class IncidentCheckboxModule {}
