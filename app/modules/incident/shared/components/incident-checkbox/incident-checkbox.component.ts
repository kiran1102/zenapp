import {
    Component,
    Input,
    EventEmitter,
    Output,
} from "@angular/core";

@Component({
    selector: "incident-checkbox",
    templateUrl: "./incident-checkbox.component.html",
})
export class IncidentCheckboxComponent {
    @Input() checked = false;
    @Input() partialCheck = false;
    @Input() describedBy: string;
    @Input() disabled: boolean;
    @Input() id = ("incident-checkbox" + Math.random());
    @Input() name = "default-incident-checkbox-name";

    @Output() toggle = new EventEmitter<boolean>();

    checkboxToggled() {
        this.toggle.emit(!this.checked);
    }
}
