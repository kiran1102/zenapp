// Core
import {
    Component,
    Inject,
} from "@angular/core";
import {
    FormGroup,
    FormBuilder,
    Validators,
} from "@angular/forms";
import { IOtModalContent } from "@onetrust/vitreus";

// 3rd party
import { find } from "lodash";

// Services
import { IncidentDetailService } from "incidentModule/incident-detail/shared/services/incident-detail.service";
import { TimeStamp } from "oneServices/time-stamp.service";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

// Tokens
import { StoreToken } from "tokens/redux-store.token";

// Interfaces
import { IIncidentAddEditModalData } from "incidentModule/shared/interfaces/incident-add-edit-modal.interface";
import { IIncidentTypeRecord } from "incidentModule/shared/interfaces/incident-type-state.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";
import { IStore } from "interfaces/redux.interface";
import {
    IIncidentCreateRequest,
    IIncidentUpdateRequest,
} from "incidentModule/shared/interfaces/incident-create-state.interface";
import { IIncidentDetail } from "incidentModule/shared/interfaces/incident-detail-state.interface";
import { IOrgUserAdapted } from "interfaces/org-user.interface";
import { IOtOrgUserOutput } from "sharedModules/components/ot-org-user/ot-org-user.interface";

// Enums
import { OrgUserTraversal } from "enums/org-user-dropdown.enum";
import { IDateTimeOutputModel } from "@onetrust/vitreus";
import { BehaviorSubject, Subject } from "rxjs";

@Component({
    selector: "incident-add-edit-modal",
    templateUrl: "./incident-add-edit-modal.component.html",
})
export class IncidentAddEditModalComponent implements IOtModalContent {
    otModalCloseEvent: Subject<string>;
    isLoading$ = new BehaviorSubject<boolean> (true);
    actionButtonTitle: string;
    assignee: IOrgUserAdapted;
    cancelButtonTitle: string;
    isDateOccurredInvalid: boolean;
    isDateDiscoveredInvalid: boolean;
    isDeadlineInvalid: boolean;
    dateOccurredValue: string;
    dateDiscoveredValue: string;
    deadlineTime: string;
    deadlineValue: string;
    disableUntilDate = this.timeStamp.getDisableUntilDate();
    format = this.timeStamp.getDatePickerDateFormat();
    incidentDetail: IIncidentDetail;
    incidentForm: FormGroup;
    incidentTypes: IIncidentTypeRecord[];
    is24Hour: boolean;
    isSaving: boolean;
    otModalData: IIncidentAddEditModalData;
    modalTitle: string;
    orgUserError: boolean;
    orgUserTraversal = OrgUserTraversal;
    selectedIncidentType: IIncidentTypeRecord;
    incidentRegisterLength: number;
    incidentNamePlaceholder: string;
    labelKey = "nameKey";
    valueKey = "id";

    constructor(
        @Inject(StoreToken) private store: IStore,
        private incidentDetailService: IncidentDetailService,
        private readonly formBuilder: FormBuilder,
        private readonly timeStamp: TimeStamp,
        private translatePipe: TranslatePipe,
    ) { }

    ngOnInit(): void {
        this.is24Hour = this.timeStamp.getIs24Hour();
        this.modalTitle = this.otModalData.modalTitle;
        this.actionButtonTitle = this.otModalData.actionButtonTitle;
        this.cancelButtonTitle = this.otModalData.cancelButtonTitle;
        this.incidentDetail = this.otModalData.incidentDetail;
        this.incidentRegisterLength = this.otModalData.incidentRegisterLength + 1;
        this.prePopulateIncidentName();
        this.reset();
        this.fetchIncidentTypes();

        if (this.incidentDetail) {
            this.preSelectIncidentName();
            this.preSelectIncidentDateDiscovered();
            this.preSelectIncidentDateOccurred();
            this.preSelectIncidentDeadline();
            this.preSelectIncidentDescription();
            this.preSelectIncidentOrganization();
            this.preSelectIncidentAssignee();
            this.preSelectIncidentRootCause();
        }
    }

    reset(): void {
        this.incidentForm = this.formBuilder.group({
            name: ["", !this.incidentNamePlaceholder ? Validators.required : null],
            incidentTypeId: ["", Validators.required],
            description: [""],
            dateOccurred: [""],
            dateDiscovered: [""],
            deadline: [""],
            selectedOrg: ["", Validators.required],
            assignee: [this.assignee],
            rootCause: [""],
        });
    }

    closeModal(): void {
        this.reset();
        this.otModalCloseEvent.next();
    }

    cancelButtonClicked(): void {
        this.closeModal();
    }

    actionButtonClicked(): void {
        if (this.otModalData.incidentDetail) {
            this.updateIncident();
        } else {
            this.createNewIncident();
        }
    }

    selectOrg(event: string): void {
        if (!event || this.orgSelectionChanged(event)) {
            this.incidentForm.get("assignee").setValue(null);
        }
        this.incidentForm.get("selectedOrg").setValue(event);
    }

    selectDateOccurred(dateTimeValue: IDateTimeOutputModel): void {
        this.isDateOccurredInvalid = dateTimeValue.hasError;
        this.incidentForm.get("dateOccurred").setValue(dateTimeValue.jsdate);
        this.dateOccurredValue = dateTimeValue.dateTime ? this.timeStamp.incrementISODateMillisecond(dateTimeValue.dateTime) : "";
    }

    selectDateDiscovered(dateTimeValue: IDateTimeOutputModel): void {
        this.isDateDiscoveredInvalid = dateTimeValue.hasError;
        this.incidentForm.get("dateDiscovered").setValue(dateTimeValue.jsdate);
        this.dateDiscoveredValue = dateTimeValue.dateTime ? this.timeStamp.incrementISODateMillisecond(dateTimeValue.dateTime) : "";
    }

    selectDeadline(dateTimeValue: IDateTimeOutputModel): void {
        this.isDeadlineInvalid = dateTimeValue.hasError;
        this.incidentForm.get("deadline").setValue(dateTimeValue.jsdate);
        this.deadlineValue = dateTimeValue.dateTime ? this.timeStamp.incrementISODateMillisecond(dateTimeValue.dateTime) : "";
    }

    selectIncidentType(selectedOption: IIncidentTypeRecord): void {
        this.incidentForm.get("incidentTypeId").setValue(selectedOption.id);
        this.selectedIncidentType = selectedOption;
    }

    selectAssignee(selectedOption: IOtOrgUserOutput) {
        if (!selectedOption.valid) {
            this.orgUserError = true;
        } else {
            this.orgUserError = false;
            selectedOption.selection
                ? this.incidentForm.get("assignee").setValue(selectedOption.selection.Id)
                : this.incidentForm.get("assignee").setValue(null);
        }
    }

    isFormDataInvalid(): boolean {
        return !this.incidentForm.valid
            || this.isDeadlineInvalid
            || this.isDateOccurredInvalid
            || this.isDateDiscoveredInvalid
            || this.orgUserError
            || !this.incidentNamePlaceholder ? (this.incidentForm.get("name").value.trim() !== "" ? !this.incidentForm.valid : true) : false;
    }

    private fetchIncidentTypes(): void {
        this.incidentTypes = this.translateIncidentTypeRecord(this.incidentDetailService.incidentTypes$.getValue())
                                    .filter((incidentType: IIncidentTypeRecord) => {
                                        if (this.otModalData.incidentDetail && incidentType.id === this.otModalData.incidentDetail.incidentTypeId) {
                                            return incidentType;
                                        }
                                        return incidentType && incidentType.active === true;
                                    });
        this.preSelectIncidentType();
        this.isLoading$.next(false);
    }

    private translateIncidentTypeRecord(incidentTypes: IIncidentTypeRecord[]): IIncidentTypeRecord[] {
        return incidentTypes.map((incidentType) => {
            return {
                id: incidentType.id,
                name: incidentType.name,
                nameKey: this.translatePipe.transform(incidentType.nameKey),
                description: incidentType.description,
                active: incidentType.active,
            };
        });
    }

    private prePopulateIncidentName(): void {
        if (!this.otModalData.incidentDetail) {
            this.incidentNamePlaceholder = this.translatePipe.transform("Name");
        } else {
            this.incidentNamePlaceholder = null;
        }
    }

    private preSelectIncidentName(): void {
        if (this.otModalData.incidentDetail.name) {
            this.incidentForm.get("name").setValue(this.otModalData.incidentDetail.name);
        }
    }

    private preSelectIncidentType(): void {
        if (this.otModalData.incidentDetail && this.otModalData.incidentDetail.incidentTypeId) {
            this.selectIncidentType(find(this.incidentTypes, (incidentType: IIncidentTypeRecord): boolean => {
                return incidentType.id === this.otModalData.incidentDetail.incidentTypeId;
            }));
        }
    }

    private preSelectIncidentDateOccurred(): void {
        if (this.otModalData.incidentDetail.dateOccurred) {
            this.dateOccurredValue = this.timeStamp.incrementISODateMillisecond(this.otModalData.incidentDetail.dateOccurred);
            this.incidentForm.get("dateOccurred").setValue(new Date(this.otModalData.incidentDetail.dateOccurred));
        }
    }

    private preSelectIncidentDateDiscovered(): void {
        if (this.otModalData.incidentDetail.dateDiscovered) {
            this.dateDiscoveredValue = this.timeStamp.incrementISODateMillisecond(this.otModalData.incidentDetail.dateDiscovered);
            this.incidentForm.get("dateDiscovered").setValue(new Date(this.otModalData.incidentDetail.dateDiscovered));
        }
    }

    private preSelectIncidentDeadline(): void {
        if (this.otModalData.incidentDetail.deadline) {
            this.deadlineValue = this.timeStamp.incrementISODateMillisecond(this.otModalData.incidentDetail.deadline);
            this.incidentForm.get("deadline").setValue(new Date(this.otModalData.incidentDetail.deadline));
        }
    }

    private preSelectIncidentDescription(): void {
        if (this.otModalData.incidentDetail.description) {
            this.incidentForm.get("description").setValue(this.otModalData.incidentDetail.description);
        }
    }

    private preSelectIncidentOrganization(): void {
        if (this.otModalData.incidentDetail.orgGroupId) {
            this.incidentForm.get("selectedOrg").setValue(this.otModalData.incidentDetail.orgGroupId);
        }
    }

    private preSelectIncidentAssignee(): void {
        if (this.otModalData.incidentDetail.assigneeId) {
            this.incidentForm.get("assignee").setValue(this.otModalData.incidentDetail.assigneeId);
        }
    }

    private preSelectIncidentRootCause(): void {
        if (this.otModalData.incidentDetail.rootCause) {
            this.incidentForm.get("rootCause").setValue(this.otModalData.incidentDetail.rootCause);
        }
    }

    private orgSelectionChanged(event: string): boolean {
        return this.incidentForm.get("selectedOrg").value !== event;
    }

    private createNewIncident() {
        const newIncident: IIncidentCreateRequest = {
            name: this.incidentForm.get("name").value ? this.incidentForm.get("name").value.trim() : null,
            assigneeId: this.incidentForm.get("assignee").value ? this.incidentForm.get("assignee").value : null,
            dateOccurred: this.dateOccurredValue,
            dateDiscovered: this.dateDiscoveredValue,
            deadline: this.deadlineValue,
            description: this.incidentForm.get("description").value ? this.incidentForm.get("description").value.trim() : null,
            incidentTypeId: this.incidentForm.get("incidentTypeId").value,
            orgGroupId: this.incidentForm.get("selectedOrg").value,
            rootCause: this.incidentForm.get("rootCause").value ? this.incidentForm.get("rootCause").value.trim() : null,
            sourceType: "MANUAL",
        };
        this.isSaving = true;
        this.incidentDetailService.createNewIncident( newIncident ).then(( res: IProtocolResponse<string>): void => {
            if (res.result) {
                this.otModalData.callback();
                this.closeModal();
            }  else {
                this.isSaving = false;
            }
        });
    }

    private updateIncident() {
        const updateIncidentBody: IIncidentUpdateRequest = {
            name: this.incidentForm.get("name").value ? this.incidentForm.get("name").value.trim() : null,
            assigneeId: this.incidentForm.get("assignee").value ? this.incidentForm.get("assignee").value : null,
            dateOccurred: this.dateOccurredValue,
            dateDiscovered: this.dateDiscoveredValue,
            deadline: this.deadlineValue,
            description: this.incidentForm.get("description").value ? this.incidentForm.get("description").value.trim() : null,
            incidentTypeId: this.incidentForm.get("incidentTypeId").value,
            orgGroupId: this.incidentForm.get("selectedOrg").value,
            rootCause: this.incidentForm.get("rootCause").value ? this.incidentForm.get("rootCause").value.trim() : null,
        };
        this.isSaving = true;
        this.incidentDetailService.updateIncident(this.otModalData.incidentDetail.id, updateIncidentBody)
            .then((incidentDetail: IIncidentDetail): void => {
                this.closeModal();
            }).catch((err) => {
                this.isSaving = false;
            });
    }
}
