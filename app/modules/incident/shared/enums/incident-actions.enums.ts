export enum IncidentListActions {
    FETCHING_INCIDENT_LIST = "FETCHING_INCIDENT_LIST",
    RESET_INCIDENT_LIST = "RESET_INCIDENT_LIST",
    UPDATE_INCIDENT_LIST = "UPDATE_INCIDENT_LIST",
}

export enum IncidentDetailActions {
    FETCHING_INCIDENT_DETAIL = "FETCHING_INCIDENT_DETAIL",
    RESET_INCIDENT_DETAIL = "RESET_INCIDENT_DETAIL",
    UPDATE_INCIDENT_DETAIL = "UPDATE_INCIDENT_DETAIL",
}

export enum IncidentAssessmentListActions {
    FETCHING_INCIDENT_ASSESSMENT_LIST = "FETCHING_INCIDENT_ASSESSMENT_LIST",
    RESET_INCIDENT_ASSESSMENT_LIST = "RESET_INCIDENT_ASSESSMENT_LIST",
    UPDATE_INCIDENT_ASSESSMENT_LIST = "UPDATE_INCIDENT_ASSESSMENT_LIST",
}

export enum IncidentActivityActions {
    FETCHING_INCIDENT_ACTIVITY_LIST = "FETCHING_INCIDENT_ACTIVITY_LIST",
    RESET_INCIDENT_ACTIVITY_LIST = "RESET_INCIDENT_ACTIVITY_LIST",
    UPDATE_INCIDENT_ACTIVITY_LIST = "UPDATE_INCIDENT_ACTIVITY_LIST",
}

export enum IncidentTableActions {
    SORT_QUEUE = "SORT_QUEUE",
}

export enum IncidentTableColumnNames {
    INCIDENT_NUMBER_COLUMN = "number",
    INCIDENT_NAME_COLUMN = "name",
    INCIDENT_DESCRIPTION_COLUMN = "description",
    INCIDENT_ROOT_CAUSE_COLUMN = "rootCause",
    INCIDENT_ORG_GROUP_ID_COLUMN = "orgGroupId",
    INCIDENT_ORG_GROUP_NAME_COLUMN = "orgGroupName",
    INCIDENT_ASSIGNEE_ID_COLUMN = "assigneeId",
    INCIDENT_ASSIGNEE_NAME_COLUMN = "assigneeName",
    CREATED_DT_COLUMN_NAME = "createdDate",
    CREATED_BY_COLUMN_NAME = "createdBy",
    CREATOR_COLUMN_NAME = "creator",
    INCIDENT_ID_COLUMN = "incidentId",
    INCIDENT_TYPE_NAME_COLUMN = "incidentTypeNameKey",
    INCIDENT_TYPE_ID_COLUMN = "incidentTypeId",
    INCIDENT_IS_EXTENDED_COLUMN = "isExtended",
    INCIDENT_AFFECTED_SUBJECT_RANGE_FROM_COLUMN = "affectedSubjectRangeFrom",
    INCIDENT_AFFECTED_SUBJECT_RANGE_TO_COLUMN = "affectedSubjectRangeTo",
    INCIDENT_DATEOCCURRED_COLUMN = "dateOccurred",
    INCIDENT_DATEDISCOVERED_COLUMN = "dateDiscovered",
    INCIDENT_DEADLINE_COLUMN = "deadline",
    INCIDENT_SOURCE_TYPE = "sourceType",
    INCIDENT_STAGE_NAME = "incidentStageNameKey",
    INCIDENT_STATUS_NAME = "status",
    INCIDENT_SUBTASK_ACTION = "action",
}
