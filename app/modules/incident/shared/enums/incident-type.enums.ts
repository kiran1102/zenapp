export enum IncidentType {
    LOSS_THEFT = "Loss/Theft",
    SOCIAL_ENGINEERING = "Social Engineering (e.g. Phishing)",
    UNAUTHORIZED_DISCLOSURE = "Unauthorized Disclosure of Information",
    HOAX = "Hoax",
    INTRUSION = "Intrusion",
    DOS = "Denial of Service Attack",
    VIRUS_MALICIOUS_CODE = "Virus/Malicious Code",
    SYSTEM_MISUSE = "System Misuse",
    TECHNICAL_VULNERABILITY = "Technical Vulnerability",
    ROOT_COMPROMISE = "Root Compromise",
    WEBSITE_DEFACEMENT = "Website Defacement",
    USER_ACCOUNT_COMPROMISE = "User Account Compromise",
    NETWORK_SCANNING_PROBING = "Network Scanning/Probing",
    MISDIRECTED_EMAIL = "Misdirected Email",
    UNAUTHORIZED_ALTERATION = "Unauthorized Alteration",
}

export enum IncidentTypeStatus {
    ACTIVE,
    IN_ACTIVE,
}

export enum IncidentTypesTableColumnNames {
    KEY = "nameKey",
    NAME = "name",
    ACTION = "action",
}
