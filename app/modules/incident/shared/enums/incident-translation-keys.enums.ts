export enum IncidentTranslationKeys {
    linkAssessmentBodyText = "Incident.LinkAssessmentBodyText",
    removeLinkIncidentToolTip = "Incident.RemoveLinkToolTip",
}

export enum IncidentSubTasksStateKeys {
    Open = "Open",
    Closed = "Closed",
}
