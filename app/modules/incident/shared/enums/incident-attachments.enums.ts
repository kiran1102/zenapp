export enum IncidentAttachmentTableColumnNames {
    NAME = "name",
    DESCRIPTION = "description",
    TYPE = "type",
    DATE_ADDED = "createdDate",
    ADDED_BY = "creator",
}

export enum IncidentAttachmentType {
    URL = "URL",
    FILE = "FILE",
}

export enum SaveType {
    Save = 1,
    SaveAddNew,
}
