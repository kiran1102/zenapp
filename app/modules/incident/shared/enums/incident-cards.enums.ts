export enum IncidentCardActions {
    DeleteSubTask = "DELETE_SUBTASK",
    MarkSubTaskAsCompleted = "MARK_SUBTASK_AS_COMPLETED",
    DeleteAttachment = "DELETE_ATTACHMENT",
    RemoveLink = "REMOVE_LINK",
}

export enum IncidentCardDisplayFields {
    Subtasks = "name",
    Attachments = "name",
    Assessments = "assessment",
    PrimaryLink = "primaryLink",
}
