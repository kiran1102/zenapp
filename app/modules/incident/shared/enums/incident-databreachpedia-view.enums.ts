export enum IncidentDBPViewContentSections {
    OVERVIEW = "Overview",
    NOTIFYREGULATOR = "Notify Regulator",
    NOTIFYINDIVIDUALS = "Notify Individuals",
    DEADLINE = "Deadline",
    REGULATOR = "Regulator",
    NOTIFICATIONFORMAT = "Notification Format",
}
