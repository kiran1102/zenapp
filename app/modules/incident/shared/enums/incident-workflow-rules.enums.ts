export const IncidentWorkflowRuleActionKeys = {
    CREATESUBTASK: "CREATESUBTASK",
    SUBTASK_NAME: "SUBTASK_NAME",
    SUBTASK_DESCRIPTION: "SUBTASK_DESCRIPTION",
    SENDASSESSMENT: "SENDASSESSMENT",
};

export const IncidentWorkflowRuleConditionKeys = {
    INCIDENT_TYPE: "incidentTypeId",
    METHOD_OF_REPORTING: "sourceType",
    ORGANIZATION: "orgGroupId",
    JURISICICTION: "jurisdictionId",
};

export const IncidentWorkflowRuleTaskActionKeys = {
    NAME: "name",
    DESCRIPTION: "description",
    ASSIGNEE_ID: "assigneeId",
    INCIDENT_ID: "incidentId",
    TEMPLATE_ROOT_VERSION_ID: "templateRootVersionId",
    RESPONDENT: "respondents",
    RECIPIENTS: "recipients",
    SUBJECT: "subject",
    BODY: "messageBody",
};

export const IncidentWorkflowRuleTranslationKeys = {
    createSubtask: "CreateSubtask",
    launchAssessment: "LaunchAssessment",
    sendNotification: "SendNotification",
    name: "Name",
    template: "Template",
    respondent: "Respondent",
    description: "Description",
    recipients: "Recipients",
    subject: "Subject",
    body: "Body",
};

export const IncidentWorkflowRuleActionApiKeys = {
    createSubtask: "createSubtask",
    launchAssessment: "launchAssessment",
    sendNotification: "sendNotification",
};
