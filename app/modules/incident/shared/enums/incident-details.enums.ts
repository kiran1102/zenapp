export enum IncidentSourceTypes {
    MANUAL = "Manual",
    ASSESSMENT = "Assessment",
}

export enum IncidentTabOptions {
    Activity = "Activity",
    Details = "Details",
    SubTasks = "Subtasks",
    Assessments = "Assessments",
    Attachments = "Attachments",
    Jurisdiction = "Jurisdiction",
}
