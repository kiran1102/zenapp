export enum IncidentJurisdictionTableColumnNames {
    REGION = "regionName",
    COUNTRY = "countryName",
    STATE = "stateProvinceName",
    DATE_ADDED = "createdDT",
}

export enum IncidentJurisdictionTabOptions {
    MapView,
    TableView,
    MapTableView,
}

export enum IncidentMapFillKey {
    SELECTED = "selected",
}
