import { NgModule } from "@angular/core";

// Modules
import { SharedModule } from "sharedModules/shared.module";
import { CommonModule } from "@angular/common";
import { HttpClientModule } from "@angular/common/http";
import { FormsModule } from "@angular/forms";
import { ReactiveFormsModule } from "@angular/forms";
import { VitreusModule } from "@onetrust/vitreus";
import { DndListModule } from "ngx-drag-and-drop-lists";
import { OTPipesModule, PIPES } from "modules/pipes/pipes.module";
import { VirtualScrollModule } from "od-virtualscroll";
import { OneVideoModule } from "modules/video/video.module";

// Components
import { IncidentRuleComponent } from "incidentModule/shared/incident-rules/rule/incident-rule.component";
import { IncidentRuleActionComponent } from "incidentModule/shared/incident-rules/action/incident-rule-action.component";
import { IncidentRuleConditionComponent } from "incidentModule/shared/incident-rules/rule-condition/incident-rule-condition.component";
import { IncidentRulesComponent } from "incidentModule/shared/incident-rules/rules/incident-rules.component";
import { IncidentSubRuleComponent } from "incidentModule/shared/incident-rules/sub-rule/incident-sub-rule.component";
import { IncidentAddEditModalComponent } from "incidentModule/shared/components/incident-add-edit-modal/incident-add-edit-modal.component";

// Services
import { IncidentRulesHelperService } from "incidentModule/shared/incident-rules/incident-rules-helper.service";
import { IncidentApiService } from "incidentModule/shared/services/incident-api.service";
import { IncidentAssessmentLinkApiService } from "incidentModule/shared/services/incident-assessment-linking-api.service";

@NgModule({
    imports: [
        SharedModule,
        CommonModule,
        HttpClientModule,
        FormsModule,
        ReactiveFormsModule,
        VitreusModule,
        DndListModule,
        OTPipesModule,
        VirtualScrollModule,
        OneVideoModule,
    ],
    declarations: [
        IncidentRuleComponent,
        IncidentRuleActionComponent,
        IncidentRuleConditionComponent,
        IncidentRulesComponent,
        IncidentSubRuleComponent,
        IncidentAddEditModalComponent,
    ],
    exports: [
        IncidentRuleComponent,
        IncidentRuleActionComponent,
        IncidentRuleConditionComponent,
        IncidentRulesComponent,
        IncidentSubRuleComponent,
        IncidentAddEditModalComponent,
    ],
    entryComponents: [
        IncidentAddEditModalComponent,
    ],
    providers: [
        IncidentAssessmentLinkApiService,
        IncidentApiService,
        IncidentRulesHelperService,
        PIPES, // TODO: Remove from providers when ot-common is implemented. Only necessary if you are using pipe in TS file, else just module is fine
    ],
})
export class IncidentSharedModule {}
