declare var angular: angular.IAngularStatic;

import { routes } from "./intg.routes";

import { IntgWrapperComponent } from "./intg-wrapper.component";

export const intgLegacyModule = angular
    .module("zen.intg", ["ui.router"])
    .config(routes)

    .component("intgWrapper", IntgWrapperComponent)
    ;
