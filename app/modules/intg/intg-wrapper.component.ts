// Services
import { Permissions } from "modules/shared/services/helper/permissions.service";
import { GlobalSidebarService } from "modules/global-sidebar/services/one-global-sidebar.service";
import { Wootric } from "sharedModules/services/provider/wootric.service";

// Interfaces
import { INavMenuItem } from "modules/global-sidebar/interfaces/nav-menu-item.interface";
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";

// Constants
import { WootricModuleMap } from "modules/shared/constants/wootric.constants";
import { HideTab } from "modules/intg/constants/integration.constant";

class IntgWrapperCtrl implements ng.IComponentController {
    static $inject: string[] = [
        "Permissions",
        "GlobalSidebarService",
        "Wootric",
        "$rootScope",
    ];

    constructor(
        private permissions: Permissions,
        private globalSidebarService: GlobalSidebarService,
        private wootric: Wootric,
        private readonly $rootScope: IExtendedRootScopeService,
    ) {}

    $onInit() {
        this.globalSidebarService.set(this.getMenuRoutes());
        this.wootric.run(WootricModuleMap.Integrations);
    }

    private getMenuRoutes(): INavMenuItem[] {
        const menuGroup: INavMenuItem[] = [];
        const integrationsMenuGroup: INavMenuItem = {
            id: "IntegrationSidebarMenu",
            title: this.$rootScope.t("Integrations"),
            children: [],
            open: false,
            icon: "ot ot-code",
        };

        if (this.permissions.canShow("IntegrationMarketplaceView")) {
            menuGroup.push({
                id: "IntgMarketplaceSidebarItem",
                title: this.$rootScope.t("Marketplace"),
                route: "zen.app.pia.module.intg.marketplace.list",
                activeRoute: "zen.app.pia.module.intg.marketplace",
                icon: "ot ot-shopping-cart",
            });
        }

        if (this.permissions.canShow("IntegrationsCustomValues")) {
            integrationsMenuGroup.children.push({
                id: "IntgValuesSidebarItem",
                title: this.$rootScope.t("Values"),
                route: "zen.app.pia.module.intg.values.list",
                activeRoute: "zen.app.pia.module.intg.values",
                icon: "ot ot-shopping-cart",
            });
        }

        if (this.permissions.canShow("IntegrationConnectionListView")) {
            integrationsMenuGroup.children.push({
                id: "IntgConnectionsSidebarItem",
                title: this.$rootScope.t("Connections"),
                route: "zen.app.pia.module.intg.connections.list",
                activeRoute: "zen.app.pia.module.intg.connections",
                icon: "ot ot-shopping-cart",
            });
        }

        if (this.permissions.canShow("IntegrationWorkflowListView")) {
            integrationsMenuGroup.children.push({
                id: "IntgWorkflowSidebarItem",
                title: this.$rootScope.t("Workflows"),
                route: "zen.app.pia.module.intg.workflows.list",
                activeRoute: "zen.app.pia.module.intg.workflows",
                icon: "ot ot-shopping-cart",
            });
        }

        if (this.permissions.canShow("IntegrationCredentialListView")) {
            integrationsMenuGroup.children.push({
                id: "IntgCredentialsSidebarItem",
                title: this.$rootScope.t("Credentials"),
                route: "zen.app.pia.module.intg.credentials.list",
                activeRoute: "zen.app.pia.module.intg.credentials",
                icon: "ot ot-shopping-cart",
            });
        }

        if (!HideTab.hideFeature && this.permissions.canShow("IntegrationMarketplaceView")) {
            integrationsMenuGroup.children.push({
                id: "IntgTemplatesSidebarItem",
                title: this.$rootScope.t("Templates"),
                icon: "ot ot-shopping-cart",
            });
        }

        if (this.permissions.canShow("IntegrationMarketplaceView") && this.permissions.canShow("IntegrationConnector")) {
            integrationsMenuGroup.children.push({
                id: "IntgSystemsSidebarItem",
                title: this.$rootScope.t("Systems"),
                icon: "ot ot-shopping-cart",
                route: "zen.app.pia.module.intg.systems.list",
                activeRoute: "zen.app.pia.module.intg.systems",
            });
        }

        if (integrationsMenuGroup.children.length) {
            menuGroup.push(integrationsMenuGroup);
        }

        if (this.permissions.canShow("IntegrationConnectionListView")) {
            menuGroup.push({
                id: "IntgAPIKeysSidebarItem",
                title: this.$rootScope.t("API_Keys"),
                route: "zen.app.pia.module.intg.apikey.list",
                activeRoute: "zen.app.pia.module.intg.apikey",
                icon: "ot ot-API-Keys text-huge",
            });
        }
        return menuGroup;
    }
}

export const IntgWrapperComponent: ng.IComponentOptions = {
    controller: IntgWrapperCtrl,
    template: `
        <section class="is-module row-nowrap full-size background-eggshell text-color-default">
            <div class="stretch-horizontal full-size overflow-hidden column-nowrap">
                <ui-view class="stretch-vertical full-size"></ui-view>
            </div>
        </section>
    `,
};
