export type MethodType = "GET" | "PUT" | "PATCH" | "POST";
export type AuthType = "NO_AUTH" | "BASIC" | "BEARER_TOKEN";
