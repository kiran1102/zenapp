export const SystemConstants = {
    GENERIC: "1d709ba3-32fd-4052-a1a5-1ac5b9c27e7e",
    ONETRUST: "6eb78f1a-56ac-4a89-a655-8f5a870fa054",
    JIRA: "a4b4442e-0a5f-49c8-99df-8722d03c89f2",
    SLACK: "157bca65-593b-4438-9217-e2d9b7bac2a2",
    SMTP: "baf242ce-1efa-41ef-a4d1-cdf474134d29",
    SALESFORCE: "d599db8b-3b54-4108-a99f-75f4e678f8ce",
    RSA: "ca4833f5-8258-48cf-a213-5fdfbe4444ed",
    OKTA: "8f65ec5f-7f28-4709-9706-fb153d0e61c6",
    SERVICENOW: "db92ca40-b841-4aaf-b3fb-898bfbec38f6",
    ELOQUA: "5984fbdb-7f18-44f4-b29e-05b5bd2dc913",
    IBM: "817d3288-bb07-494f-bf4f-7a80a99c1ad0",
    IMPORT: "4af960f3-b7c5-4952-905a-69c3f0729479",
    OUTLOOK: "954b71fe-7168-4916-a156-645cf94d73fc",
    PING: "be082f4b-355d-4fda-ba6c-7b859fdc4404",
    POWERBI: "c0dd29cd-5ae1-4b82-832e-cfff98395d3c",
    PARDOT: "b8ad551a-3f4f-4482-adc8-50e89295de95",
    SILVERPOP: "dbc8e7c8-76c1-478e-acd7-e78daa317cad",
    SYMANTEC: "e4b17c63-6924-4dfd-b084-7bba66b29336",
    TABLEAU: "e5e97a57-bc93-432b-98c4-75fe68cc6883",
    BMC: "282e658c-685c-4da6-8542-8dea98d2686b",
    GSUITE: "5dc90484-fb2b-491f-b3ec-15e4b01e772a",
    ADFS: "04c9762b-ea4a-4410-af34-8cb0be5c359e",
    ONELOGIN: "91aff299-2d01-4abf-9bee-a80dad73ef3e",
    DRUPAL: "5665b37a-c806-4e40-b856-85913f3f9287",
    WORDPRESS: "f21e5e68-52df-4146-879b-c062b863fc47",
    TEALIUM: "eb32d89b-92c9-4cc7-8c60-9c608fb302fe",
    JOOMLA: "83c9270a-45d1-4531-b23a-89f9a41dcc35",
    SQUARESPACE: "de5c02bd-2804-4aab-abfa-785ec7109502",
    COLLIBRA: "3877d083-0d3b-4d82-bd0f-e9047a8e02ad",
    ATM: "18e2226c-f232-4f98-8cc3-451dd127c59e",
    GTM: "7c85c97d-5395-4e72-927f-e575cd8f131b",
    AZURE: "1a02b613-5ce1-434c-b828-f0588fff3c4e",
    MARKETO: "87983b99-0cd4-441d-9aa9-d7e402245eae",
    CMS: "40d6b839-5767-47c2-8cda-6024d4240b85",
    AMP: "021a3098-4bed-4785-81a3-8ed300d97b65",
    AEM: "7e69b8b1-9237-4045-a60f-b29f25d47e03",
    ADOBELAUNCH: "debfbf52-6823-4d25-b0ce-2b9b92da5584",
    ENSIGHTEN: "bcce4846-7a31-4a3a-b269-eb8d4aa599c4",
    MPARTICLE: "3427f2d0-5b0d-4ec5-82c8-fffdd53d754b",
    BIGID: "5bbabb47-8af7-42a9-a292-b890b4b941ed",
    DATAGUISE: "9e170412-07e7-45d6-b867-7b2e01af4d43",
    VARONIS: "3d7a3e36-9c37-4729-904d-18e1970455c5",
    IBMSECURITY: "3d85d111-b1a9-497a-b48d-909813d29dcd",
    MAILCHIMP: "59504a62-e8c7-11e8-9f32-f2801f1b9fd1",
};

export const SystemListFilters = {
    MARKETPLACE: "showInMarketplace",
    CONNECTABLE: "isConnectable",
    COMING_SOON: "isComingSoon",
    ADD_CONNECTION: "isAddingConnections",
};

export const ModuleFilters = {
    DataMapping: "DataMapping",
    Cookies: "Cookies",
    All: "All",
    SingleSignOn: "SingleSignOn",
    Notification: "Notification",
    AssessmentAutomation: "AssessmentAutomation",
    Risks: "Risks",
    UniversalConsent: "UniversalConsent",
    UserManagement: "UserManagement",
    Calendar: "Calendar",
    E_Discovery: "E_Discovery",
    Ticket_Sync: "TicketSync",
    TargetedDataDiscovery: "TargetedDataDiscovery",
};

export const IntgActionTabs = {
    SYSTEM: "SYSTEM",
    ONETRUST: "ONETRUST",
    REQUEST: "REQUEST",
    RESPONSE: "RESPONSE",
    SAMPLE: "SAMPLE",
    LOGIC: "LOGIC",
};

export const IntgTriggerEvents = {
    ASSESSMENTS: "ASSESSMENTS",
    CONSENT: "CONSENT",
    DATAMAPPING: "DATAMAPPING",
    DSAR: "DSAR",
    BUILTIN: "BUILTIN",
    VENDOR: "VENDOR",
};

export const IntgTestResponseTabs = {
    RESPONSE: "RESPONSE",
    LOGS: "LOGS",
};

export const ConnectionManageTabs = {
    OVERVIEW: "OVERVIEW",
    API_CONNECTION: "API_CONNECTION",
    CREDENTIALS: "CREDENTIALS",
    ACTIONS: "ACTIONS",
};

export const IntgActionMethodTypes = {
    GET: "GET",
    PATCH: "PATCH",
    POST: "POST",
    PUT: "PUT",
    DELETE: "DELETE",
};

export const IntgAuthTypeKeys = {
    NO_AUTH: "NoAuth",
    BASIC: "Basic",
    BEARER_TOKEN: "Bearer",
    OAUTH2_TOKEN: "OAuth2",
};

export const IntgImportTypeKeys = {
    WorkflowCreate: "WorkflowCreate",
    CreateSystem: "CreateSystem",
    CreateSystemFromJSON: "CreateSystemFromJSON",
    UpdateSystemFromJSON: "UpdateSystemFromJSON",
};

export const IntgAuthType = {
    NO_AUTH: "NO_AUTH",
    BASIC: "BASIC",
    BEARER_TOKEN: "BEARER_TOKEN",
    OAUTH2_TOKEN: "OAUTH2_TOKEN",
};

export const IntgGrantTypeKeys = {
    Authorization_Code: "AuthorizationCode",
    Password_Credentials: "PasswordCredentials",
    Refresh: "Refresh",
    Client_Credentials: "ClientCredentials",
};

export const ClientAuthenticationType = {
    SEND_IN_BODY: "SEND_IN_BODY",
    SEND_IN_REQUESTPARAMS: "SEND_IN_REQUESTPARAMS",
    SEND_IN_URL_ENCODED: "SEND_IN_URL_ENCODED",
};

export const ClientAuthenticationTypeKeys = {
    SEND_IN_BODY: "SendInBody",
    SEND_IN_REQUESTPARAMS: "SendInRequestParams",
    SEND_IN_URL_ENCODED: "SendInFormUrlEncoded",
};

export const IntgGrantType = {
    Authorization_Code: "Authorization_Code",
    Password_Credentials: "Password_Credentials",
    Refresh: "Refresh",
    Client_Credentials: "Client_Credentials",
};

export const IntgGrantTypeQueryParam = {
    Password: "password",
    Refresh_Token: "refresh_token",
    Client_Credentials: "client_credentials",
};

export const ProcessType = {
    HTTP: "HTTP",
    TRANSFORM: "TRANSFORM",
    MESSAGE_LOG: "MESSAGELOG",
    APPLY_EACH: "APPLYEACH",
};

export const IpWhiteListType = {
    IP_ADDRESS: "IP_ADDRESS",
    CIDR_ADDRESS: "CIDR_ADDRESS",
};

export const ApiConnectionActions = {
    CONNECTION_EDIT: "CONNECTION_EDIT",
    CONNECTION_ADD: "CONNECTION_ADD",
    CONNECTION_DELETE: "CONNECTION_DELETE",
};

export const IntgActionHeaderTypes = {
    Content_Type: "Content-Type",
    Authorization: "Authorization",
    Accept: "Accept",
};

export const ColumnComponent = {
    ACTIVE_TAG: "active-tag-table-cell",
    ORG_NAME: "org-name-table-cell",
};

export const LookUpValueOptionType = {
    OTHERS : "OTHERS",
};

export const KeyCode = {
    ENTER: "Enter",
};

export const NextSteps = {
    CREDENTIAL_STEP: "CREDENTIAL_STEP",
    REQUEST_RESPONSE_STEP: "REQUEST_RESPONSE_STEP",
};

export const FrequencyType = {
    DAILY: 1,
    WEEKLY: 2,
    MONTHLY: 3,
};

export const SchedulerTerms = {
    SCHEDULER: "Scheduler",
    WEBHOOK: "Webhook",
};

export const TriggerType = {
    SCHEDULED: "SCHEDULED",
    WEBHOOK: "WEBHOOK",
    EVENT: "EVENT",
};

export const EventCode = {
    SCHEDULER: 9000,
    WEBHOOK: 9010,
    EVENT: 3000,
};

export const JsonFilename = {
    OTWORKFLOW : "OTWorkflow.json",
    OTSYSTEM : "OTIntegrationSystem.json",
};

export const DelayType = {
    WAIT: "Wait",
    HOLD_UNTIL: "Hold_Until",
};

export const DelayTypeKeys = {
    WAIT: "Wait",
    HOLD_UNTIL: "HoldUntil",
};

export const DelayUnit = {
    MONTH: "Month",
    WEEK: "Week",
    DAY: "Day",
    HOUR: "Hour",
    MINUTE: "Minute",
};

export const MarketPlaceView = {
    MARKET_PLACE_LIST_VIEW: "MarketPlaceListView",
    SYSTEM_LIST_VIEW: "SystemListView",
};

export const ConnectionListView = {
    API_KEYS_LIST_VIEW: "APIKeysListView",
    CONNECTION_LIST_VIEW: "ConnectionListView",
};

export const MarketPlaceDetailsView = {
    MARKET_PLACE_DETAILS_VIEW: "MarketPlaceDetailsView",
    SYSTEM_DETAILS_VIEW: "SystemDetailsView",
};

export const HideTab = {
    hideFeature: true,
};

export const TargetDiscoverySystem = {
    MARKETO: "Marketo",
    MAIL_CHIMP: "MailChimp",
    HUBSPOT: "HubSpot",
    OKTA: "Okta",
    SERVICENOW: "ServiceNow",
    OFFICE365: "Office365",
    SALESFORCE: "Salesforce",
};

export const TargetDiscoveryModuleIcon = {
    CONSENT: "images/launcher/CM.svg",
    DATAMAPPING: "images/launcher/DM.svg",
    DSAR: "images/launcher/DSAR.svg",
};

export const TargetDiscoveryModule = {
    CONSENT: "CONSENT",
    DATAMAPPING: "DATAMAPPING",
    DSAR: "DSAR",
};

export const SourceType = {
    SEED: "SEED",
    USER_DEFINED: "USER_DEFINED",
    INSTANCE: "INSTANCE",
};

export const MapMergeActions = {
    ADDNEW: "ADDNEW",
    IGNORE: "IGNORE",
    MAP: "MAP",
    PENDING: "PENDING",
    REJECT: "REJECT",
};

export const MapMergeParam = {
    MODAL_LAUNCH_PARAM: "dataDiscoveryModalActive=true",
};

export const CustomValueTableColumnKey = {
    Name: "name",
    ParamaterType: "parameterType",
    IntegrationName: "integrationName",
    WorkflowName: "workflowName",
    Value: "value",
};

export const CustomValueTypes = {
    STRING: "STRING",
    BOOLEAN: "BOOLEAN",
    DOUBLE: "DOUBLE",
    DATE: "DATETIME",
    OBJECT: "OBJECT",
};

export const VarTypes = {
    TEXT: "text",
    NUMBER: "number",
};
