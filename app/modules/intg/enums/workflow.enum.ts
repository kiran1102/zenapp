export enum WorkflowType {
    CUSTOM = "CUSTOM",
    NATIVE = "NATIVE",
    COMPOSITE= "COMPOSITE",
}
