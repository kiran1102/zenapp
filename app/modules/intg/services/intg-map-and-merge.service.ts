// Angular
import { Injectable } from "@angular/core";

// Services
import { ProtocolService } from "modules/core/services/protocol.service";

// Interfaces
import {
    IProtocolConfig,
    IProtocolMessages,
    IProtocolResponse,
} from "interfaces/protocol.interface";
import { IPageableOf } from "interfaces/pagination.interface";
import { IDataDiscoveryItem, IDataDiscoveryResponseItem } from "modules/intg/interfaces/integration.interface";

// Constants
import { MapMergeActions } from "modules/intg/constants/integration.constant";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

// RxJS
import { Observable, from } from "rxjs";

@Injectable()
export class IntgMapAndMergeService {

    constructor(
        private oneProtocol: ProtocolService,
        private translatePipe: TranslatePipe,
    ) {}

    getAssets(status: string = MapMergeActions.PENDING, page: number = 0, size: number = 100): Observable<IProtocolResponse<IPageableOf<IDataDiscoveryItem>>> {
        const config: IProtocolConfig = this.oneProtocol.customConfig("GET", `/api/assetdiscovery/v1/preferences/resolve`, { status, page, size });
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("CannotRetrieveAssets") } };
        return from(this.oneProtocol.http(config, messages));
    }

    saveAssets(assetPayload: IDataDiscoveryItem[]): Observable<IProtocolResponse<IDataDiscoveryResponseItem[]>> {
        const config: IProtocolConfig = this.oneProtocol.customConfig("PUT", `/api/assetdiscovery/v1/preferences`, null, assetPayload);
        const messages: IProtocolMessages = {
            Success: { custom: this.translatePipe.transform("AssetsUpdated") },
            Error: { custom: this.translatePipe.transform("CannotSaveAssets") },
        };
        return from(this.oneProtocol.http(config, messages));
    }
}
