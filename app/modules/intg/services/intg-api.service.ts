import {
    Injectable,
    Inject,
 } from "@angular/core";

// lodash
import {
    first,
    map,
    capitalize,
} from "lodash";

// Service
import { ProtocolService } from "modules/core/services/protocol.service";
import OrgGroupStoreNew from "oneServices/org-group.store";
import { IntgConfigService } from "intsystemservice/configuration/intg-configuration.service";

// Interface
import {
    IProtocolConfig,
    IProtocolMessages,
    IProtocolResponse,
} from "interfaces/protocol.interface";
import { IStringMap } from "interfaces/generic.interface";
import {
    IISystem,
    IIAPIKey,
    IIConnection,
    IIRoutingKeys,
    IITestConnectionResponse,
    IIPage,
    IIEvents,
} from "modules/intg/interfaces/integration.interface";

// Pipe
import { TranslatePipe } from "modules/pipes/translate.pipe";

@Injectable()
export class IntgApiService {

    constructor(
        @Inject("OrgGroupStoreNewService") private readonly orgGroupStoreNew: OrgGroupStoreNew,
        private readonly IntgConfig: IntgConfigService,
        private readonly OneProtocol: ProtocolService,
        private translatePipe: TranslatePipe,
    ) {}

    public getConnection(instanceId?: string, systemId?: string, type?: string, page?: number, size?: number): ng.IPromise<IProtocolResponse<IIPage<IIConnection>>> {
        const config: IProtocolConfig = this.OneProtocol.config("GET", `/Integration/GetConnections`, ({ instanceId, systemId, type, page, size }));
        const messages: IProtocolMessages = { Error: { Hide: true } };
        return this.OneProtocol.http(config, messages).then((response: IProtocolResponse<IIPage<IIConnection>>): IProtocolResponse<IIPage<IIConnection>> => {
            if (response.result && response.data && response.data.content) {
                response.data.content = this.handleConnections(response.data.content);
            }
            return response;
        });
    }

    public getConnectionById(systemInstanceId: string, integrationSystemId: string): ng.IPromise<IProtocolResponse<IIConnection>> {
        let responseData: IIConnection;
        return this.getConnection(systemInstanceId, integrationSystemId).then((response: IProtocolResponse<IIPage<IIConnection>>): IProtocolResponse<IIConnection> => {
            if (response.result && response.data && response.data.content) {
                responseData = first(response.data.content);
            }
            return { ...response, data: responseData };
        });
    }

    public createConnection(request: IIConnection): ng.IPromise<IProtocolResponse<IIConnection>> {
        const config: IProtocolConfig = this.OneProtocol.config("POST", `/Integration/CreateConnection`, {}, request);
        const messages: IProtocolMessages = { Error: { object: this.translatePipe.transform("IntegrationSystemConnection"), action: this.translatePipe.transform("saving") } };
        return this.OneProtocol.http(config, messages);
    }

    public createSlackConnection(name: string): ng.IPromise<IProtocolResponse<IIConnection>> {
        const config: IProtocolConfig = this.OneProtocol.config("GET", `/Integration/SlackLogin`, { name });
        const messages: IProtocolMessages = { Error: { object: this.translatePipe.transform("IntegrationSystemConnection"), action: this.translatePipe.transform("saving") } };
        return this.OneProtocol.http(config, messages);
    }

    public updateConnection(request: IIConnection, id: string): ng.IPromise<IProtocolResponse<IIConnection>> {
        const config: IProtocolConfig = this.OneProtocol.config("PUT", `/Integration/UpdateConnection`, { id }, request);
        const messages: IProtocolMessages = { Error: { object: this.translatePipe.transform("IntegrationSystemConnection"), action: this.translatePipe.transform("updating") } };
        return this.OneProtocol.http(config, messages);
    }

    public deleteConnection(id: string): ng.IPromise<IProtocolResponse<void>> {
        const config: IProtocolConfig = this.OneProtocol.config("DELETE", `/Integration/DeleteInstance`, { id });
        const messages: IProtocolMessages = { Error: { object: this.translatePipe.transform("IntegrationSystemConnection"), action: this.translatePipe.transform("deleting") } };
        return this.OneProtocol.http(config, messages);
    }

    public getIntegrationSystemEvents(integrationSystemId: string, page: number = 0, size: number = 20): ng.IPromise<IProtocolResponse<IIEvents[]>> {
        const config: IProtocolConfig = this.OneProtocol.config("GET", `/Integration/GetUIEvents`, { integrationSystemId, page, size });
        const messages: IProtocolMessages = { Error: { Hide: true } };
        let responseData: IIEvents[];
        return this.OneProtocol.http(config, messages).then((response: IProtocolResponse<IIPage<IIEvents>>): IProtocolResponse<IIEvents[]> => {
            if (response.result && response.data) {
                responseData = response.data.content;
            }
            return { ...response, data: responseData };
        });
    }

    public getIntegrationSystemRoutingKeys(instanceId: string, systemId: string): ng.IPromise<IProtocolResponse<IIEvents[]>> {
        const config: IProtocolConfig = this.OneProtocol.config("GET", `/Integration/GetRoutingKeys`, { systemId, instanceId });
        const messages: IProtocolMessages = { Error: { object: this.translatePipe.transform("IntegrationSystemEvents"), action: this.translatePipe.transform("getting") } };
        let responseData: IIEvents[];
        return this.OneProtocol.http(config, messages).then((response: IProtocolResponse<IIPage<IIEvents>>): IProtocolResponse<IIEvents[]> => {
            if (response.result && response.data) {
                responseData = response.data.content;
            }
            return { ...response, data: responseData };
        });
    }

    public createRoutingKey(request: IIRoutingKeys): ng.IPromise<IProtocolResponse<IIRoutingKeys>> {
        const config: IProtocolConfig = this.OneProtocol.config("POST", `/Integration/CreateRoutingKey`, {}, request);
        const messages: IProtocolMessages = { Error: { object: this.translatePipe.transform("IntegrationSystemEvent"), action: this.translatePipe.transform("enabling") } };
        return this.OneProtocol.http(config, messages);
    }

    public deleteRoutingKey(id: string): ng.IPromise<IProtocolResponse<void>> {
        const config: IProtocolConfig = this.OneProtocol.config("DELETE", `/Integration/DeleteRoutingKey`, { id });
        const messages: IProtocolMessages = { Error: { object: this.translatePipe.transform("IntegrationSystemEvent"), action: this.translatePipe.transform("disabling") } };
        return this.OneProtocol.http(config, messages);
    }

    public getApiKeys(systemInstanceId: string, integrationSystemId: string): ng.IPromise<IProtocolResponse<IIEvents>> {
        const config: IProtocolConfig = this.OneProtocol.config("GET", `/Integration/GetApiKeys`, { systemInstanceId, integrationSystemId });
        const messages: IProtocolMessages = { Error: { Hide: true } };
        let responseData: IIEvents;
        return this.OneProtocol.http(config, messages).then((response: IProtocolResponse<IIPage<IIEvents>>): IProtocolResponse<IIEvents> => {
            if (response.result) {
                responseData = first(response.data.content);
            }
            return { ...response, data: responseData };
        });
    }

    public createApiKey(request: IIAPIKey): ng.IPromise<IProtocolResponse<IIAPIKey>> {
        const config: IProtocolConfig = this.OneProtocol.config("POST", `/Integration/CreateApiKey`, null, request);
        const messages: IProtocolMessages = { Error: { Hide: true } };
        return this.OneProtocol.http(config, messages);
    }

    public patchApiKey(request: IIAPIKey): ng.IPromise<IProtocolResponse<IIAPIKey>> {
        const config: IProtocolConfig = this.OneProtocol.config("PATCH", `/Integration/UpdateApiKeys`, null, request);
        const messages: IProtocolMessages = {
            Error: { Hide: true },
        };
        return this.OneProtocol.http(config, messages);
    }

    public testConnection(request: IIConnection): ng.IPromise<IProtocolResponse<IITestConnectionResponse>> {
        const config: IProtocolConfig = this.OneProtocol.config("POST", `/Integration/TestConnection`, {}, request);
        const messages: IProtocolMessages = { Error: { Hide: true } };
        return this.OneProtocol.http(config, messages);
    }

    public importSystemCreate(request: unknown, name: string, templateId: string, type = "openapi"): ng.IPromise<IProtocolResponse<void>> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("POST", `/integrationmanager/api/v1/integrations/import`, { type, name, templateId }, request);
        const messages: IProtocolMessages = {
            Error: { custom: this.translatePipe.transform("UnableToImportSystem") },
            Success: { custom: this.translatePipe.transform("SystemImportedSuccessfully") },
        };
        return this.OneProtocol.http(config, messages);
    }

    public importSystemUpdate(request: unknown, integrationId: string, templateId: string, type = "openapi"): ng.IPromise<IProtocolResponse<void>> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("PUT", `/integrationmanager/api/v1/integrations/${integrationId}/import`, { type, templateId }, request);
        const messages: IProtocolMessages = {
            Error: { custom: this.translatePipe.transform("UnableToImportSystem") },
            Success: { custom: this.translatePipe.transform("SystemImportedSuccessfully") },
        };
        return this.OneProtocol.http(config, messages);
    }

    private handleConnections(connections: IIConnection[]): IIConnection[] {
        if (!connections.length || (connections.length === 1 && !connections[0].id)) {
            return [];
        }
        const tenantOrgTable: IStringMap<string> = this.orgGroupStoreNew.tenantOrgTable;
        connections = map(connections, (connection: IIConnection): IIConnection => {
            return this.handleConnection(connection, tenantOrgTable);
        });
        return connections;
    }

    private handleConnection(connection: IIConnection, tenantOrgTable: IStringMap<string> = this.orgGroupStoreNew.tenantOrgTable): IIConnection {
        connection.orgGroupId = tenantOrgTable.rootId;
        connection.status = Boolean(connection.systemInstanceId);
        const system: IISystem = this.IntgConfig.getSystemById(connection.integrationSystemId, "meta") as IISystem;
        if (system) {
            connection.system = system.displayName || (system.nameKey ? this.translatePipe.transform(system.nameKey) : capitalize(system.name));
        }
        return connection;
    }
}
