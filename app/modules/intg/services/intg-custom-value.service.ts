// Angular
import { Injectable } from "@angular/core";

// interfaces
import {
    IProtocolResponse,
    IProtocolConfig,
    IProtocolMessages,
 } from "interfaces/protocol.interface";
import { IPageableOf } from "interfaces/pagination.interface";
import {
    ICustomValuesResponse,
    ICustomValueLookupFieldList,
} from "modules/intg/interfaces/integration.interface";

// services
import { ProtocolService } from "modules/core/services/protocol.service";

// pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

// Rxjs
import {
    Observable,
    from,
} from "rxjs";

@Injectable()
export class IntgCustomValueService {
    constructor(
        private readonly oneProtocol: ProtocolService,
        private translatePipe: TranslatePipe,
    ) { }

    getAllCustomValues(page: number = 0, size: number = 1000, sort = "name,asc"): Observable<IProtocolResponse<IPageableOf<ICustomValuesResponse>>> {
        const config: IProtocolConfig = this.oneProtocol.customConfig("GET", `/integrationmanager/api/v1/params`, { page, size, sort });
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("CannotRetrieveCustomValues") } };
        return from(this.oneProtocol.http(config, messages));
    }

    getCustomValuesByWorkflow(workflowId: string): Observable<IProtocolResponse<ICustomValueLookupFieldList>> {
        const config: IProtocolConfig = this.oneProtocol.customConfig("GET", `/integrationmanager/api/v1/workflows/${workflowId}/params`);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("CannotRetrieveCustomValues") } };
        return from(this.oneProtocol.http(config, messages));
    }

    createCustomValues(customValues: ICustomValuesResponse, id?: string): Observable<IProtocolResponse<ICustomValuesResponse>> {
        const httpMethod = id ? "PUT" : "POST";
        const config: IProtocolConfig = this.oneProtocol.customConfig(httpMethod, `/integrationmanager/api/v1/params`, null, customValues);
        const messages: IProtocolMessages = {
            Error: { custom: this.translatePipe.transform("CannotSaveCustomValues") },
            Success: { custom: this.translatePipe.transform("CustomValuesSavedSuccessfully") },
        };
        return from(this.oneProtocol.http(config, messages));
    }

    deleteCustomValue(id: string): Observable<IProtocolResponse<void>> {
        const config: IProtocolConfig = this.oneProtocol.customConfig("DELETE", `/integrationmanager/api/v1/params/${id}`);
        const messages: IProtocolMessages = {
            Error: { custom: this.translatePipe.transform("CannotDeleteCustomValues") },
            Success: { custom: this.translatePipe.transform("CustomValuesDeletedSuccessfully") },
        };
        return from(this.oneProtocol.http(config, messages));
    }

    getCustomValuesDetails(valueId: string) {
        const config: IProtocolConfig = this.oneProtocol.customConfig("GET", `/integrationmanager/api/v1/params/${valueId}`);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("CannotCreateValues") } };
        return from(this.oneProtocol.http(config, messages));
    }
}
