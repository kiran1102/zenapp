// Angular
import { Injectable } from "@angular/core";

// Services
import { ProtocolService } from "modules/core/services/protocol.service";

// Interfaces
import {
    IProtocolConfig,
    IProtocolMessages,
    IProtocolResponse,
} from "interfaces/protocol.interface";
import {
    IIEvent,
    IISeededEvent,
} from "modules/intg/interfaces/integration.interface";
import { IPageableOf } from "interfaces/pagination.interface";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

@Injectable()
export class IntgEventTypeService {

    constructor(
        private OneProtocol: ProtocolService,
        private translatePipe: TranslatePipe,
    ) {}

    getEventTypes(page: number = 0, size: number = 100, sort = "name,asc"): ng.IPromise<IProtocolResponse<IPageableOf<IISeededEvent>>> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("GET", `/integrationmanager/api/v1/eventtypes`, { page, size, sort });
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("CannotRetrieveEventTypes") } };
        return this.OneProtocol.http(config, messages);
    }

    getEventTypeById(id: string): ng.IPromise<IProtocolResponse<IIEvent>> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("GET", `/integrationmanager/api/v1/eventtypes/${id}`);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("CannotRetrieveEventTypeById") } };
        return this.OneProtocol.http(config, messages);
    }

    createEventType(customEventType: IIEvent): ng.IPromise<IProtocolResponse<IIEvent>> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("POST", `/integrationmanager/api/v1/eventtypes`, null, customEventType);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("CannotCreateEventType") } };
        return this.OneProtocol.http(config, messages);
    }

    addEventTypeBatch(customEventType: IIEvent[]): ng.IPromise<IProtocolResponse<IIEvent>> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("POST", `/integrationmanager/api/v1/eventtypes/batch`, null, customEventType);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("CannotCreateEventType") } };
        return this.OneProtocol.http(config, messages);
    }

    updateEventType(updatedEventType: IISeededEvent): ng.IPromise<IProtocolResponse<IIEvent>> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("PUT", `/integrationmanager/api/v1/eventtypes`, null, updatedEventType);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("CannotUpdateEventType") } };
        return this.OneProtocol.http(config, messages);
    }

    deleteEventType(templateToDelete: IISeededEvent): ng.IPromise<IProtocolResponse<IIEvent>> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("DELETE", `/integrationmanager/api/v1/eventtypes`, null, templateToDelete);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("CannotDeleteEventType") } };
        return this.OneProtocol.http(config, messages);
    }
}
