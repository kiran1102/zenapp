
import {
    Inject,
    Injectable,
} from "@angular/core";

// External
import {
    map,
    filter,
    find,
} from "lodash";

// Interfaces
import { ConsentPurposeService } from "consentModule/shared/services/cr-purpose.service";
import {
    IProtocolConfig,
    IProtocolMessages,
    IProtocolResponse,
} from "interfaces/protocol.interface";
import {
    IIConnectionProperty,
    IIConnectionMappingElement,
    IContactMappingOptions,
    IISubjectProfileMapping,
    IIPreferencesId,
    IIPreferenceCdoPurposeMap,
    IIModuleFilters,
} from "modules/intg/interfaces/integration.interface";
import {
    IIContactMappingElement,
} from "modules/intg/interfaces/integration.interface";
import { INameId, IStringMap } from "interfaces/generic.interface";

// Models
import { EntityItem } from "crmodel/entity-item";

// Services
import { ProtocolService } from "modules/core/services/protocol.service";

// Enums
import { Endpoints } from "consentModule/enums/endpoints.enum";

@Injectable()
export class IntgEloquaService {

    constructor(
        private consentPurposeService: ConsentPurposeService,
        private OneProtocol: ProtocolService,
    ) { }

    public generateEloquaMappings(properties: IIConnectionProperty[], propertyNamePrefix: string): IIConnectionMappingElement[] {
        const mappingProperties: IIConnectionProperty[] = filter(properties,
            (value: IIConnectionProperty): boolean => value.pname.indexOf(propertyNamePrefix) === 0);

        return map(mappingProperties, (property: IIConnectionProperty) => {
            return {
                InputId: property.pname.replace(propertyNamePrefix, ""),
                OutputId: property.pvalue,
            };
        });
    }

    public generateEloquaContactMappings(properties: IIConnectionProperty[], propertyNamePrefix: string): IIContactMappingElement[] {
        const contactMappings: IIContactMappingElement[] = [];
        const dsElementProperty: IIConnectionProperty = find(properties,
            (value: IIConnectionProperty): boolean => value.pname.indexOf(propertyNamePrefix) === 0);
        if (!dsElementProperty) return [];

        const dsElementJson = JSON.parse(dsElementProperty.pvalue);

        for (const key in dsElementJson) {
            if (dsElementJson.hasOwnProperty(key)) {
                contactMappings.push({
                    dsElementId: key,
                    contactId: dsElementJson[key],
                });
            }
        }
        return contactMappings;
    }

    public generateEloquaDataPreferenceMap(properties: IIConnectionProperty[], propertyNamePrefix: string): IIPreferencesId[] {
        const dataPreferenceMappings: IIPreferencesId[] = [];
        const dsElementPreferenceMapping: IIConnectionProperty[] = filter(properties, (property: IIConnectionProperty) => {
            return property.pname.startsWith(propertyNamePrefix);
        });
        if (!dsElementPreferenceMapping) return [];
        dsElementPreferenceMapping.map((dataElements: IIConnectionProperty): void => {
            const dataPreferencePropJson: IStringMap<string> = dataElements.pvalue ? JSON.parse(dataElements.pvalue) : "";
            const tempDataElements: string[] = dataElements.pname.split(":");
            const purposeId: string = tempDataElements[2];
            const cdoId: string = tempDataElements[4];
            const topicAttribute: IIPreferenceCdoPurposeMap[] = [];
            for (const key in dataPreferencePropJson) {
                if (dataPreferencePropJson.hasOwnProperty(key)) {
                    topicAttribute.push({
                        purposeTopicId: key,
                        cdoAttributesId: dataPreferencePropJson[key],
                    });
                }
            }
            dataPreferenceMappings.push({
                purposeId,
                cdoId,
                topicAttributes: topicAttribute,
            });
        });
        return dataPreferenceMappings;
    }

    public generateEloquaDataProfMappings(properties: IIConnectionProperty[], propertyNamePrefix: string): IISubjectProfileMapping[] {
        const dataContactMappings: IISubjectProfileMapping[] = [];
        const dataProfProperty: IIConnectionProperty[] = filter(properties,
            (value: IIConnectionProperty): boolean => value.pname.indexOf(propertyNamePrefix) === 0);
        if (!dataProfProperty) return [];

        dataProfProperty.map((dataSubjectPropertMapping: IIConnectionProperty): void => {
            const dataSubjectProfPropJson = JSON.parse(dataSubjectPropertMapping.pvalue);
            const purposeId = dataSubjectPropertMapping.pname.replace(propertyNamePrefix, "");
            const contactMappings: IIContactMappingElement[] = [];

            for (const key in dataSubjectProfPropJson) {
                if (dataSubjectProfPropJson.hasOwnProperty(key)) {
                    contactMappings.push({
                        dsElementId: key,
                        contactId: dataSubjectProfPropJson[key],
                    });
                }
            }

            dataContactMappings.push({
                purposeId,
                contactMappings,
            });
        });
        return dataContactMappings;
    }

    public getEloquaMappingOptions(systemInstanceId: string): Promise<EntityItem[][]> {
        const purposePromise: ng.IPromise<EntityItem[]> = this.consentPurposeService.getActivePurposeFilters();
        const cdoPromise: ng.IPromise<EntityItem[]> = this.getEloquaCdoFilters(systemInstanceId);
        return Promise.all([purposePromise, cdoPromise]);
    }

    public getEloquaCdoFilters(systemInstanceId: string): ng.IPromise<EntityItem[]> {
        const config: IProtocolConfig = this.OneProtocol.config("GET", `/Integration/CustomDataObjects/${systemInstanceId}`);
        const messages: IProtocolMessages = { Error: { Hide: true } };
        return this.OneProtocol.http(config, messages).then((response: IProtocolResponse<Array<INameId<string>>>): EntityItem[] =>
            response.result ? this.cdosToEntityItems(response.data) : null);
    }

    public getEloquaContactList(systemInstanceId: string): ng.IPromise<IContactMappingOptions[]> {
        const config: IProtocolConfig = this.OneProtocol.config("GET", `/Integration/ContactFields/${systemInstanceId}`);
        const messages: IProtocolMessages = { Error: { Hide: true } };
        return this.OneProtocol.http(config, messages)
            .then((response: IProtocolResponse<IContactMappingOptions[]>): IContactMappingOptions[] => response.result ? response.data : []);
    }

    public getOtfList(systemInstanceId: string): ng.IPromise<IContactMappingOptions[]> {
        const params = { type: systemInstanceId };
        const config: IProtocolConfig = this.OneProtocol.customConfig("GET", `${Endpoints.DataSubjects}/fields`, params);
        const messages: IProtocolMessages = { Error: { Hide: true } };
        return this.OneProtocol.http(config, messages)
            .then((response: IProtocolResponse<IContactMappingOptions[]>): IContactMappingOptions[] => response.result ? response.data : []);

    }

    public getEloquaSelectedCdoList(systemInstanceId: string, Id: string): ng.IPromise<IIModuleFilters[]> {
        const config: IProtocolConfig = this.OneProtocol.config("GET", `/integration/${systemInstanceId}/CustomDataObjects/${Id}/attributes`);
        const messages: IProtocolMessages = { Error: { Hide: true } };
        return this.OneProtocol.http(config, messages)
            .then((response: IProtocolResponse<IIModuleFilters[]>): IIModuleFilters[] => response.result ? response.data : []);
    }

    private cdosToEntityItems(cdos: Array<INameId<string>>): EntityItem[] {
        return map(cdos, (cdo: INameId<string>): EntityItem => {
            return {
                Id: cdo.id,
                Label: cdo.name,
            };
        });
    }
}
