import { Injectable } from "@angular/core";

// Interface
import { IIGenericTableConfig } from "interfaces/table-config.interface";

// Pipe
import { TranslatePipe } from "modules/pipes/translate.pipe";

@Injectable()
export class IntgConnectionsHelperService {

    constructor(private translatePipe: TranslatePipe) {}

    public getConnectionsListConfig(): IIGenericTableConfig[] {
        return [
            {
                headerText: this.translatePipe.transform("ConnectionName"),
                sortable: false,
                sortKey: "name",
                modelPropKey: "name",
                multiLine: false,
                component: "link-table-cell",
            },
            {
                headerText: this.translatePipe.transform("Type"),
                sortable: false,
                sortKey: "system",
                modelPropKey: "system",
                multiLine: false,
            },
            {
                headerText: this.translatePipe.transform("Status"),
                sortable: false,
                sortKey: "status",
                modelPropKey: "status",
                multiLine: false,
                component: "active-tag-table-cell",
            },
            {
                headerText: this.translatePipe.transform("Organization"),
                sortable: false,
                sortKey: "orgGroupId",
                modelPropKey: "orgGroupId",
                multiLine: false,
                component: "org-name-table-cell",
            },
        ];
    }

    public getMarketplaceConnectionsConfig(): IIGenericTableConfig[] {
        return [
            {
                headerText: this.translatePipe.transform("ConnectionName"),
                sortable: false,
                sortKey: "name",
                modelPropKey: "name",
                multiLine: false,
                component: "link-table-cell",
            },
            {
                headerText: this.translatePipe.transform("Status"),
                sortable: false,
                sortKey: "status",
                modelPropKey: "status",
                multiLine: false,
                component: "active-tag-table-cell",
            },
            {
                headerText: this.translatePipe.transform("Organization"),
                sortable: false,
                sortKey: "orgGroupId",
                modelPropKey: "orgGroupId",
                multiLine: false,
                component: "org-name-table-cell",
            },
        ];
    }

    public getApiKeyListConfig(): IIGenericTableConfig[] {
        return [
            {
                headerText: this.translatePipe.transform("APIKey"),
                sortable: true,
                sortKey: "apiKey",
                modelPropKey: "apiKey",
                multiLine: true,
            },
            {
                headerText: this.translatePipe.transform("Organization"),
                sortable: false,
                sortKey: "orgGroupId",
                modelPropKey: "orgGroupId",
                multiLine: false,
                component: "org-name-table-cell",
            },
        ];
    }

    public getConnectionEventsConfig(): IIGenericTableConfig[] {
        return [
            {
                id: "EventId",
                headerText: this.translatePipe.transform("EventID"),
                sortable: false,
                sortKey: "eventId",
                modelPropKey: "eventId",
                multiLine: false,
            },
            {
                id: "EventName",
                headerText: this.translatePipe.transform("Event"),
                sortable: false,
                sortKey: "name",
                modelPropKey: "name",
                multiLine: false,
            },
            {
                id: "Status",
                headerText: this.translatePipe.transform("Status"),
                sortable: false,
                sortKey: "isEnabled",
                modelPropKey: "isEnabled",
                multiLine: false,
                component: "active-tag-table-cell",
            },
        ];
    }
}
