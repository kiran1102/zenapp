// Angular
import { Injectable } from "@angular/core";

// Services
import { ProtocolService } from "modules/core/services/protocol.service";

// Interfaces
import {
    IProtocolConfig,
    IProtocolMessages,
    IProtocolResponse,
} from "interfaces/protocol.interface";
import {
    IICredential,
    IICreateCredential,
    IIOAuth2TokenResponse,
} from "modules/intg/interfaces/integration.interface";
import { IPageableOf } from "interfaces/pagination.interface";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

// RxJS
import { Observable, from } from "rxjs";

@Injectable()
export class IntgCredentialsService {

    constructor(
        private OneProtocol: ProtocolService,
        private translatePipe: TranslatePipe,
    ) { }

    getCredentials(page: number = 0, size: number = 100, sort = "name,asc"): ng.IPromise<IProtocolResponse<IPageableOf<IICredential>>> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("GET", `/integrationmanager/api/v1/credentials`, { page, size, sort });
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("CannotRetrieveCredentials") } };
        return this.OneProtocol.http(config, messages);
    }

    getCredentialById(id: string): ng.IPromise<IProtocolResponse<IICredential>> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("GET", `/integrationmanager/api/v1/credentials/${id}`);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("CannotRetrieveCredentialById") } };
        return this.OneProtocol.http(config, messages);
    }

    getOAuthToken(oAuthBody: IIOAuth2TokenResponse, requestBody?: string): Observable<IProtocolResponse<IIOAuth2TokenResponse>> {
        const request = requestBody ?  { body: requestBody, requestUri: oAuthBody.requestUri, headers: oAuthBody.headers} : oAuthBody;
        const config: IProtocolConfig = this.OneProtocol.customConfig("POST", "/integrationmanager/api/v1/tokens/request", null, request);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("CannotGenerateAccessToken") } };
        return from(this.OneProtocol.http(config, messages));
    }

    saveCredential(newCredential: IICreateCredential): ng.IPromise<IProtocolResponse<IICreateCredential>> {
        const httpVerb =  newCredential.id ?  "PUT" : "POST";
        const config: IProtocolConfig = this.OneProtocol.customConfig(httpVerb, `/integrationmanager/api/v1/credentials`, null, newCredential);
        const messages: IProtocolMessages = {
            Error: { custom: this.translatePipe.transform(newCredential.id ? "CannotUpdateCredential" : "CannotCreateCredential") },
            Success: { custom: this.translatePipe.transform(newCredential.id ? "CredentialUpdatedSuccessfully" : "CredentialCreatedSuccessfully") },
        };
        return this.OneProtocol.http(config, messages);
    }

    deleteCredential(id: string): ng.IPromise<IProtocolResponse<IICredential>> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("DELETE", `/integrationmanager/api/v1/credentials/${id}`);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("CannotDeleteCredential") } };
        return this.OneProtocol.http(config, messages);
    }
}
