// Angular
import { Injectable } from "@angular/core";

// 3rd  party
import {
    filter,
    map,
    values,
    find,
    merge,
    cloneDeep,
    remove,
} from "lodash";

// Interfaces
import { IStringMap } from "interfaces/generic.interface";
import {
    IISystemConfigData,
    IISystem,
    IIModuleFilters,
} from "modules/intg/interfaces/integration.interface";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

// Constants
import { SystemConstants, ModuleFilters, SystemListFilters } from "modules/intg/constants/integration.constant";
import { FormDataTypes, FormInputTypes } from "constants/form-types.constant";

// Services
import { Content } from "sharedModules/services/provider/content.service";
import { Permissions } from "modules/shared/services/helper/permissions.service";

@Injectable()
export class IntgConfigService {

    private systems: IISystemConfigData;

    constructor(
        private readonly content: Content,
        private readonly permissions: Permissions,
        private readonly translatePipe: TranslatePipe,
    ) {
        const contentSystems: string = this.content.GetKey("intg_systems");
        this.systems = contentSystems ? merge([], this.data, JSON.parse(contentSystems)) : cloneDeep(this.data);
    }

    public showFilteredTiles(id?: string): IISystem[] {
        return remove(this.getSystemsList(SystemListFilters.MARKETPLACE), (system: IISystem): string => {
            return find(system.moduleList, (module: string): boolean => module === id);
        });
    }

    public getSystemsList(filterBy?: string): IISystem[] {
        const systems: IISystem[] = filter(map(values(this.systems), "meta"), (systemMeta: IISystem): boolean => {
            return this.canShowSystem(systemMeta);
        }) as IISystem[];
        return filterBy ? filter(systems, filterBy) : systems;
    }

    public getSystemById(id: string, mapBy?: string): IISystemConfigData | IISystem {
        const system: IISystemConfigData = find(
            values(this.systems),
            (systemConfig: IISystemConfigData): boolean => {
                return systemConfig.meta.id === id && this.canShowSystem(systemConfig.meta);
            },
        ) as IISystemConfigData;
        return system && mapBy ? system[mapBy] : system;
    }

    public getFiltersList(): IIModuleFilters[] {
        const moduleList = [
            {
                id: ModuleFilters.All,
                name: this.translatePipe.transform("All"),
            },
            {
                id: ModuleFilters.DataMapping,
                name: this.translatePipe.transform("DataMapping"),
            },
            {
                id: ModuleFilters.Cookies,
                name: this.translatePipe.transform("Cookies"),
            },
            {
                id: ModuleFilters.SingleSignOn,
                name: this.translatePipe.transform("SingleSignOn"),
            },
            {
                id: ModuleFilters.Notification,
                name: this.translatePipe.transform("Notification"),
            },
            {
                id: ModuleFilters.AssessmentAutomation,
                name: this.translatePipe.transform("AssessmentAutomation"),
            },
            {
                id: ModuleFilters.Risks,
                name: this.translatePipe.transform("Risks"),
            },
            {
                id: ModuleFilters.UserManagement,
                name: this.translatePipe.transform("UserManagement"),
            },
            {
                id: ModuleFilters.Calendar,
                name: this.translatePipe.transform("Calendar"),
            },
            {
                id: ModuleFilters.UniversalConsent,
                name: this.translatePipe.transform("UniversalConsent"),
            },
            {
                id: ModuleFilters.E_Discovery,
                name: this.translatePipe.transform("E_Discovery"),
            },
            {
                id: ModuleFilters.Ticket_Sync,
                name: this.translatePipe.transform("Ticket_Sync"),
            },
        ];

        if (this.permissions.canShow("IntegrationsDataDiscovery")) {
            moduleList.push({
                id: ModuleFilters.TargetedDataDiscovery,
                name: this.translatePipe.transform("TargetedDataDiscovery"),
            });
        }

        return moduleList;
    }

    private canShowSystem(systemMeta: IISystem): boolean {
        return (systemMeta.permission && this.permissions.canShow(systemMeta.permission)) || systemMeta.isComingSoon;
    }

    // tslint:disable-next-line:member-ordering
    private data: IStringMap<IISystemConfigData> = {
        generic: {
            meta: {
                id: SystemConstants.GENERIC,
                permission: "ISSystemGeneric",
                name: "API Key",
                nameKey: "APIKey",
                displayName: "APIKey",
                descriptionKeys: [
                    "OneTrustSystemDescriptionV2",
                ],
                typeKey: "SystemTypeProductivity",
                publishedByKey: "IntegrationSystemDocumentation",
                publisherLink: "https://support.onetrust.com/hc/en-us/articles/360000048888",
                publishedDt: "",
                showInMarketplace: true,
                isConnectable: true,
                isComingSoon: false,
                ShowAdvancedView: true,
                isContactOneTrust: false,
                isAddingConnections: true,
                moduleList: [
                    ModuleFilters.AssessmentAutomation,
                ],
                logo: {
                    url: "images/intg-systems/icon-api.svg",
                    style: { "background-color": "#6CC04A" },
                },
            },
            form: {
                showInbound: true,
                showOutbound: false,
                outboundFormKeys: [],
            },
        },
        onetrust: {
            meta: {
                id: SystemConstants.ONETRUST,
                permission: "ISSystemGeneric",
                name: "onetrust",
                nameKey: "",
                displayName: "OneTrust",
                descriptionKeys: [
                    "OneTrustSystemDescriptionV2",
                ],
                typeKey: "SystemTypeProductivity",
                publishedByKey: "IntegrationSystemDocumentation",
                publisherLink: "https://support.onetrust.com/hc/en-us/articles/360000048888",
                publishedDt: "",
                showInMarketplace: false,
                isConnectable: false,
                isComingSoon: false,
                ShowAdvancedView: false,
                isContactOneTrust: false,
                logo: {
                    url: "images/intg-systems/onetrust-tile-logo.png",
                    style: { "background-color": "#6CC04A" },
                },
            },
            form: {
                showInbound: false,
                showOutbound: false,
                outboundFormKeys: [],
            },
        },
        jira: {
            meta: {
                id: SystemConstants.JIRA,
                permission: "ISSystemJira",
                name: "jira",
                nameKey: "",
                displayName: "Jira",
                descriptionKeys: [
                    "JiraDescriptionV2",
                    "JiraDescriptionV2.para1",
                    "JiraDescriptionV2.para2",
                ],
                typeKey: "SystemTypeProductivity",
                publishedByKey: "IntegrationSystemDocumentation",
                publisherLink: "https://support.onetrust.com/hc/en-us/articles/360000019547",
                publishedDt: "",
                showInMarketplace: true,
                isConnectable: true,
                isComingSoon: false,
                ShowAdvancedView: true,
                isContactOneTrust: false,
                isAddingConnections: true,
                moduleList: [
                    ModuleFilters.AssessmentAutomation,
                ],
                logo: {
                    url: "images/intg-systems/jira-white.svg",
                    style: { "background-color": "#0052cc" },
                },
                illustration: {
                    url: "images/intg-systems/jira-illustration.svg",
                },
            },
            form: {
                showInbound: true,
                showOutbound: true,
                showTemplatesList: true,
                outboundFormKeys: [
                    "OutboundConnection",
                    "EnterLoginCredentials",
                ],
                templatesList: [
                    "DefaultTemplates",
                    "ListOfTemplatesConfig",
                ],
                outboundFields: [
                    {
                        dataType: FormDataTypes.TEXT,
                        inputType: FormInputTypes.URL,
                        fieldKey: "url",
                        fieldNameKey: "CustomURL",
                        validationKey: "EnterValidCustomUrl",
                        required: true,
                        placeholderKey: "EnterCustomURL",
                    },
                    {
                        dataType: FormDataTypes.TEXT,
                        inputType: FormInputTypes.TEXT,
                        fieldKey: "username",
                        fieldNameKey: "Username",
                        validationKey: "EnterValidUsername",
                        required: true,
                        placeholderKey: "EnterUsername",
                    },
                    {
                        dataType: FormDataTypes.TEXT,
                        inputType: FormInputTypes.PASSWORD,
                        fieldKey: "password",
                        fieldNameKey: "Password",
                        validationKey: "EnterValidPassword",
                        required: true,
                        placeholderKey: "EnterPassword",
                    },
                    {
                        dataType: FormDataTypes.BUTTON,
                        inputType: FormInputTypes.TEST_CONNECTION,
                        fieldClass: "row-flex-end",
                        btnType: "primary",
                        textKey: "TestConnection",
                        requireFields: ["url", "username", "password"],
                    },
                ],
            },
        },
        slack: {
            meta: {
                id: SystemConstants.SLACK,
                permission: "ISSystemSlack",
                name: "slack",
                nameKey: "",
                displayName: "Slack",
                descriptionKeys: [
                    "slackDescriptionV2",
                ],
                typeKey: "SystemTypeProductivity",
                publishedByKey: "IntegrationSystemDocumentation",
                publisherLink: "https://support.onetrust.com/hc/en-us/articles/360000046028",
                publishedDt: "",
                showInMarketplace: true,
                isConnectable: true,
                isComingSoon: false,
                ShowAdvancedView: false,
                isContactOneTrust: false,
                isAddingConnections: true,
                moduleList: [
                      ModuleFilters.Notification,
                ],
                logo: {
                    url: "images/intg-systems/slack.svg",
                    style: { "background-color": "#ffffff" },
                },
                illustration: {
                    url: "images/intg-systems/slack-illustration.svg",
                },
            },
            form: {
                nameFormKeys: [
                    {
                        headerText: "EnterSlackLogin",
                        subHeaderText: [
                            {
                                name: "SlackAdminsOnly",
                                link: "https://get.slack.help/hc/en-us/articles/222386767-Manage-apps-for-your-workspace",
                                linkText: "ClickHere",
                            },
                        ],
                    },
                ],
                showInbound: false,
                showOutbound: false,
                outboundFormKeys: [],
                outboundFields: [
                    {
                        dataType: FormDataTypes.TEXT,
                        inputType: FormInputTypes.TEXT,
                        pValueKey: "slack.team.name",
                        fieldNameKey: "TeamName",
                        descriptionKeys: "",
                        validationKey: "",
                        required: false,
                        placeholderKey: "",
                    },
                ],
            },
        },
        smtp: {
            meta: {
                id: SystemConstants.SMTP,
                permission: "ISSystemSMTP",
                name: "smtp",
                nameKey: "",
                displayName: "SMTP",
                descriptionKeys: [
                    "SmtpDescriptionV2",
                ],
                typeKey: "SystemTypeConnectivity",
                publishedByKey: "IntegrationSystemDocumentation",
                publisherLink: "https://support.onetrust.com/hc/en-us/articles/115015859148",
                publishedDt: "",
                showInMarketplace: true,
                isConnectable: true,
                isComingSoon: false,
                ShowAdvancedView: false,
                isContactOneTrust: false,
                isAddingConnections: false,
                moduleList: [
                    ModuleFilters.Notification,
                ],
                logo: {
                    url: "images/intg-systems/smtp.svg",
                    style: { "background-color": "#6CC04A" },
                },
            },
            form: {
                showInbound: false,
                showOutbound: false,
            },
        },
        salesforce: {
            meta: {
                id: SystemConstants.SALESFORCE,
                permission: "ISSystemSalesForce",
                name: "salesforce",
                nameKey: "",
                displayName: "Salesforce",
                descriptionKeys: [
                    "SalesForceOverview",
                ],
                typeKey: "SystemTypeProductivity",
                publishedByKey: "IntegrationSystemDocumentation",
                publisherLink: "https://support.onetrust.com/hc/en-us/articles/360005273134-OneTrust-Salesforce-Integration",
                publishedDt: "",
                showInMarketplace: false,
                isConnectable: true,
                isComingSoon: false,
                ShowAdvancedView: false,
                isContactOneTrust: false,
                upgradeModuleName: "SalesForce Integration",
                isAddingConnections: false,
                moduleList: [
                    ModuleFilters.UniversalConsent,
                ],
                logo: {
                    url: "images/intg-systems/salesforce.png",
                    style: { "background-color": "#06a1df" },
                },
                illustration: {
                    url: "images/intg-systems/salesforce-illustration.svg",
                },
            },
            form: {
                showInbound: false,
                showOutbound: false,
            },
        },
        rsa: {
            meta: {
                id: SystemConstants.RSA,
                permission: "ISSystemRSA",
                name: "rsa Archer",
                nameKey: "",
                displayName: "RSA Archer",
                descriptionKeys: [
                    "BulkImportModuleDescription",
                ],
                typeKey: "SystemTypeProductivity",
                publishedByKey: "IntegrationSystemDocumentation",
                publisherLink: "https://support.onetrust.com/hc/en-us/articles/360000136188",
                publishedDt: "",
                showInMarketplace: true,
                isConnectable: true,
                isComingSoon: false,
                ShowAdvancedView: false,
                isContactOneTrust: false,
                isAddingConnections: false,
                moduleList: [
                    ModuleFilters.DataMapping,
                ],
                logo: {
                    url: "images/intg-systems/rsa.svg",
                    style: { "background-color": "#be3734" },
                },
                illustration: {
                    url: "images/intg-systems/import-export-illustration.svg",
                },
            },
            form: {
                showInbound: false,
                showOutbound: false,
            },
        },
        okta: {
            meta: {
                id: SystemConstants.OKTA,
                name: "okta",
                permission: "ISSystemOkta",
                nameKey: "",
                displayName: "Okta",
                descriptionKeys: [
                    "oktaDescription",
                    "SystemDescription.para1",
                    "SystemDescription.para2",
                ],
                typeKey: "SystemTypeProductivity",
                publishedByKey: "IntegrationSystemDocumentation",
                publisherLink: "https://support.onetrust.com/hc/en-us/articles/115008174827",
                publishedDt: "",
                showInMarketplace: false,
                isConnectable: true,
                isComingSoon: false,
                ShowAdvancedView: false,
                isContactOneTrust: false,
                isAddingConnections: false,
                moduleList: [
                    ModuleFilters.SingleSignOn,
                ],
                logo: {
                    url: "images/intg-systems/okta.svg",
                    style: { "background-color": "#ffffff" },
                },
                illustration: {
                    url: "images/intg-systems/okta-illustration.svg",
                },
            },
            form: {
                showInbound: false,
                showOutbound: false,
            },
        },
        servicenow: {
            meta: {
                id: SystemConstants.SERVICENOW,
                permission: "ISSystemServiceNow",
                name: "servicenow",
                nameKey: "",
                displayName: "Servicenow",
                descriptionKeys: [
                    "BulkImportModuleDescription",
                ],
                typeKey: "SystemTypeProductivity",
                publishedByKey: "IntegrationSystemDocumentation",
                publisherLink: "https://support.onetrust.com/hc/en-us/articles/360000136188",
                publishedDt: "",
                showInMarketplace: false,
                isConnectable: true,
                isComingSoon: false,
                ShowAdvancedView: false,
                isContactOneTrust: false,
                isAddingConnections: false,
                moduleList: [
                    ModuleFilters.DataMapping,
                ],
                logo: {
                    url: "images/intg-systems/servicenow.svg",
                    style: { "background-color": "#070707" },
                },
                illustration: {
                    url: "images/intg-systems/import-export-illustration.svg",
                },
            },
            form: {
                showInbound: false,
                showOutbound: false,
            },
        },
        eloquaconsent: {
            meta: {
                id: SystemConstants.ELOQUA,
                permission: "ISSystemEloquaConsent",
                name: "eloqua",
                nameKey: "",
                displayName: "Eloqua",
                descriptionKeys: [
                    "UniversalConsentModuleDescription",
                ],
                typeKey: "SystemTypeProductivity",
                publishedByKey: "IntegrationSystemDocumentation",
                publisherLink: "https://support.onetrust.com/hc/en-us/articles/360005334113-OneTrust-Eloqua-Integration",
                publishedDt: "",
                showInMarketplace: true,
                isConnectable: true,
                isComingSoon: false,
                ShowAdvancedView: true,
                isAddingConnections: true,
                isContactOneTrust: false,
                moduleList: [
                    ModuleFilters.UniversalConsent,
                ],
                logo: {
                    url: "images/intg-systems/eloqua.svg",
                    style: { "background-color": "#ffffff" },
                },
                illustration: {
                    url: "images/intg-systems/eloqua-illustration.svg",
                },
            },
            form: {
                showInbound: false,
                showOutbound: true,
                showIntegrationMapping: true,
                outboundFormKeys: [
                    "OutboundConnection",
                    "EnterLoginCredentials",
                ],
                outboundFields: [
                    {
                        dataType: FormDataTypes.TEXT,
                        inputType: FormInputTypes.URL,
                        fieldKey: "url",
                        fieldNameKey: "CustomURL",
                        validationKey: "EnterValidCustomUrl",
                        required: true,
                        placeholderKey: "EnterCustomURL",
                    },
                    {
                        dataType: FormDataTypes.TEXT,
                        inputType: FormInputTypes.TEXT,
                        fieldKey: "username",
                        fieldNameKey: "Username",
                        validationKey: "EnterValidUsername",
                        required: true,
                        placeholderKey: "EnterUsername",
                    },
                    {
                        dataType: FormDataTypes.TEXT,
                        inputType: FormInputTypes.PASSWORD,
                        fieldKey: "password",
                        fieldNameKey: "Password",
                        validationKey: "EnterValidPassword",
                        required: true,
                        placeholderKey: "EnterPassword",
                    },
                    {
                        dataType: FormDataTypes.BUTTON,
                        inputType: FormInputTypes.TEST_CONNECTION,
                        fieldClass: "row-flex-end",
                        btnType: "primary",
                        textKey: "TestConnection",
                        requireFields: ["url", "username", "password"],
                    },
                    {
                        dataType: FormDataTypes.INTEGRATION_MAPPINGS,
                        fieldKey: "mappings",
                    },
                ],
            },
        },
        eloqua: {
            meta: {
                id: SystemConstants.ELOQUA,
                permission: "ISSystemEloqua",
                name: "eloqua",
                nameKey: "",
                displayName: "Eloqua",
                descriptionKeys: [
                    "UniversalConsentModuleDescription",
                ],
                typeKey: "SystemTypeProductivity",
                publishedByKey: "",
                publisherLink: "",
                publishedDt: "",
                showInMarketplace: true,
                isConnectable: true,
                isComingSoon: false,
                ShowAdvancedView: false,
                isAddingConnections: false,
                isContactOneTrust: true,
                upgradeModuleName: "Eloqua Integration",
                moduleList: [
                    ModuleFilters.UniversalConsent,
                ],
                logo: {
                    url: "images/intg-systems/eloqua.svg",
                    style: { "background-color": "#ffffff" },
                },
            },
            form: {
                showInbound: false,
                showOutbound: false,
            },
        },
        ibm: {
            meta: {
                id: SystemConstants.IBM,
                permission: "ISSystemIBMNotes",
                name: "ibm notes",
                nameKey: "",
                displayName: "IBM Notes",
                descriptionKeys: [
                    "CalendarModuleDescription",
                ],
                typeKey: "SystemTypeProductivity",
                publishedByKey: "IntegrationSystemDocumentation",
                publisherLink: "https://support.onetrust.com/hc/en-us/articles/115013735107",
                publishedDt: "",
                showInMarketplace: true,
                isConnectable: true,
                isComingSoon: false,
                ShowAdvancedView: false,
                isAddingConnections: false,
                isContactOneTrust: true,
                upgradeModuleName: "IBM Notes Integration",
                moduleList: [
                    ModuleFilters.Calendar,
                ],
                logo: {
                    url: "images/intg-systems/ibm-notes.svg",
                    style: { "background-color": "#ffffff" },
                },
                illustration: {
                    url: "images/intg-systems/reminder-illustration.svg",
                },
            },
            form: {
                showInbound: false,
                showOutbound: false,
            },
        },
        import: {
            meta: {
                id: SystemConstants.IMPORT,
                permission: "ISSystemDataImportExport",
                name: "Data Import/Export",
                nameKey: "",
                displayName: "Data Import/Export",
                descriptionKeys: [
                    "ImportModuleDescription",
                ],
                typeKey: "SystemTypeProductivity",
                publishedByKey: "IntegrationSystemDocumentation",
                publisherLink: "https://support.onetrust.com/hc/en-us/articles/115015848387",
                publishedDt: "",
                showInMarketplace: true,
                isConnectable: true,
                isComingSoon: false,
                ShowAdvancedView: false,
                isContactOneTrust: false,
                isAddingConnections: false,
                moduleList: [
                    ModuleFilters.UserManagement,
                    ModuleFilters.DataMapping,
                ],
                logo: {
                    url: "images/intg-systems/import.svg",
                    style: { "background-color": "#B8BFC3" },
                },
                illustration: {
                    url: "images/intg-systems/import-export-illustration.svg",
                },
            },
            form: {
                showInbound: false,
                showOutbound: false,
            },
        },
        outlook: {
            meta: {
                id: SystemConstants.OUTLOOK,
                permission: "ISSystemOutlook",
                name: "outlook",
                nameKey: "",
                displayName: "Outlook",
                descriptionKeys: [
                    "CalendarModuleDescription",
                ],
                typeKey: "SystemTypeProductivity",
                publishedByKey: "IntegrationSystemDocumentation",
                publisherLink: "	https://support.onetrust.com/hc/en-us/articles/115013735107",
                publishedDt: "",
                showInMarketplace: true,
                isConnectable: true,
                isComingSoon: false,
                ShowAdvancedView: false,
                isContactOneTrust: true,
                upgradeModuleName: "Outlook Integration",
                isAddingConnections: false,
                moduleList: [
                    ModuleFilters.Calendar,
                ],
                logo: {
                    url: "images/intg-systems/outlook.svg",
                    style: { "background-color": "#0072C6" },
                },
                illustration: {
                    url: "images/intg-systems/reminder-illustration.svg",
                },
            },
            form: {
                showInbound: false,
                showOutbound: false,
            },
        },
        ping: {
            meta: {
                id: SystemConstants.PING,
                permission: "ISSystemPing",
                name: "Ping Identity",
                nameKey: "",
                displayName: "Ping Identity",
                descriptionKeys: [
                    "SystemDescription",
                    "SystemDescription.para1",
                    "SystemDescription.para2",
                ],
                typeKey: "SystemTypeProductivity",
                publishedByKey: "IntegrationSystemDocumentation",
                publisherLink: "https://support.onetrust.com/hc/en-us/articles/115004345627",
                publishedDt: "",
                showInMarketplace: true,
                isConnectable: true,
                isComingSoon: false,
                ShowAdvancedView: false,
                isContactOneTrust: false,
                isAddingConnections: false,
                moduleList: [
                    ModuleFilters.SingleSignOn,
                ],
                logo: {
                    url: "images/intg-systems/ping-identity.svg",
                    style: { "background-color": "#ffffff" },
                },
                illustration: {
                    url: "images/intg-systems/ping-illustration.svg",
                },
            },
            form: {
                showInbound: false,
                showOutbound: false,
            },
        },
        powerbi: {
            meta: {
                id: SystemConstants.POWERBI,
                permission: "ISSystemPowerBI",
                name: "power bi",
                nameKey: "",
                displayName: "Power BI",
                descriptionKeys: [
                    "RiskModuleDescription",
                ],
                typeKey: "SystemTypeProductivity",
                publishedByKey: "IntegrationSystemDocumentation",
                publisherLink: "https://support.onetrust.com/hc/en-us/articles/115012473867",
                publishedDt: "",
                showInMarketplace: true,
                isConnectable: true,
                isComingSoon: false,
                ShowAdvancedView: false,
                isContactOneTrust: true,
                upgradeModuleName: "Power BI Integration",
                isAddingConnections: false,
                moduleList: [
                    ModuleFilters.DataMapping,
                    ModuleFilters.Risks,
                ],
                logo: {
                    url: "images/intg-systems/power-bi.svg",
                    style: { "background-color": "#F2C811" },
                },
                illustration: {
                    url: "images/intg-systems/data-visualization-illustration.svg",
                },
            },
            form: {
                showInbound: false,
                showOutbound: false,
            },
        },
        pardot: {
            meta: {
                id: SystemConstants.PARDOT,
                permission: "ISSystemPardot",
                name: "pardot",
                nameKey: "",
                displayName: "Pardot",
                descriptionKeys: [
                    "PardotDescription",
                ],
                typeKey: "SystemTypeProductivity",
                publishedByKey: "IntegrationSystemDocumentation",
                publisherLink: "https://support.onetrust.com/hc/en-us/articles/360005333853-OneTrust-Pardot-Integration",
                publishedDt: "",
                showInMarketplace: true,
                isConnectable: true,
                isComingSoon: false,
                ShowAdvancedView: false,
                isAddingConnections: false,
                isContactOneTrust: true,
                upgradeModuleName: "Pardot Integration",
                moduleList: [
                    ModuleFilters.UniversalConsent,
                ],
                logo: {
                    url: "images/intg-systems/salesforce-pardot.svg",
                    style: { "background-color": "#ffffff" },
                },
                illustration: {
                    url: "images/intg-systems/pardot-illustration.svg",
                },
            },
            form: {
                showInbound: false,
                showOutbound: false,
            },
        },
        silverpop: {
            meta: {
                id: SystemConstants.SILVERPOP,
                permission: "ISSystemSilverpop",
                name: "silverpop",
                nameKey: "",
                displayName: "SilverPop",
                descriptionKeys: [
                    "UniversalConsentModuleDescription",
                ],
                typeKey: "SystemTypeProductivity",
                publishedByKey: "",
                publisherLink: "",
                publishedDt: "",
                showInMarketplace: true,
                isConnectable: true,
                isComingSoon: false,
                ShowAdvancedView: false,
                isContactOneTrust: true,
                upgradeModuleName: "SilverPop Integration",
                isAddingConnections: false,
                moduleList: [
                    ModuleFilters.UniversalConsent,
                ],
                logo: {
                    url: "images/intg-systems/silverpop.svg",
                    style: { "background-color": "#ffffff" },
                },
            },
            form: {
                showInbound: false,
                showOutbound: false,
            },
        },
        symantec: {
            meta: {
                id: SystemConstants.SYMANTEC,
                permission: "ISSystemSymantec",
                name: "symantec",
                nameKey: "",
                displayName: "Symantec",
                descriptionKeys: [
                    "BulkImportModuleDescription",
                ],
                typeKey: "SystemTypeProductivity",
                publishedByKey: "IntegrationSystemDocumentation",
                publisherLink: "https://support.onetrust.com/hc/en-us/articles/360000136188",
                publishedDt: "",
                showInMarketplace: true,
                isConnectable: true,
                isComingSoon: false,
                ShowAdvancedView: false,
                isContactOneTrust: false,
                isAddingConnections: false,
                moduleList: [
                    ModuleFilters.DataMapping,
                ],
                logo: {
                    url: "images/intg-systems/symantec.svg",
                    style: { "background-color": "#070707" },
                },
                illustration: {
                    url: "images/intg-systems/import-export-illustration.svg",
                },
            },
            form: {
                showInbound: false,
                showOutbound: false,
            },
        },
        tableau: {
            meta: {
                id: SystemConstants.TABLEAU,
                permission: "ISSystemTableau",
                name: "tableau",
                nameKey: "",
                displayName: "Tableau",
                descriptionKeys: [
                    "RiskModuleDescription",
                ],
                typeKey: "SystemTypeProductivity",
                publishedByKey: "IntegrationSystemDocumentation",
                publisherLink: "https://support.onetrust.com/hc/en-us/articles/115012473867",
                publishedDt: "",
                showInMarketplace: true,
                isConnectable: true,
                isComingSoon: false,
                ShowAdvancedView: false,
                isAddingConnections: false,
                isContactOneTrust: true,
                upgradeModuleName: "Tableau Integration",
                moduleList: [
                    ModuleFilters.DataMapping,
                    ModuleFilters.Risks,
                ],
                logo: {
                    url: "images/intg-systems/tableau.svg",
                    style: { "background-color": "#ffffff" },
                },
                illustration: {
                    url: "images/intg-systems/data-visualization-illustration.svg",
                },
            },
            form: {
                showInbound: false,
                showOutbound: false,
            },
        },
        bmc: {
            meta: {
                id: SystemConstants.BMC,
                permission: "ISSystemBMC",
                name: "BMC Software",
                nameKey: "",
                displayName: "BMC Software",
                descriptionKeys: [
                    "BulkImportModuleDescription",
                ],
                typeKey: "SystemTypeProductivity",
                publishedByKey: "IntegrationSystemDocumentation",
                publisherLink: "https://support.onetrust.com/hc/en-us/articles/360000136188",
                publishedDt: "",
                showInMarketplace: true,
                isConnectable: true,
                isComingSoon: false,
                ShowAdvancedView: false,
                isAddingConnections: false,
                isContactOneTrust: false,
                moduleList: [
                    ModuleFilters.DataMapping,
                ],
                logo: {
                    url: "images/intg-systems/bmc.svg",
                    style: { "background-color": "#ffffff" },
                },
                illustration: {
                    url: "images/intg-systems/import-export-illustration.svg",
                },
            },
            form: {
                showInbound: false,
                showOutbound: false,
            },
        },
        adfs: {
            meta: {
                id: SystemConstants.ADFS,
                permission: "ISSystemADFS",
                name: "microsoft adfs",
                nameKey: "",
                displayName: "Microsoft ADFS",
                descriptionKeys: [
                    "SystemDescription",
                    "SystemDescription.para1",
                    "SystemDescription.para2",
                ],
                typeKey: "SystemTypeProductivity",
                publishedByKey: "IntegrationSystemDocumentation",
                publisherLink: "https://support.onetrust.com/hc/en-us/articles/115005125307",
                publishedDt: "",
                showInMarketplace: true,
                isConnectable: true,
                isComingSoon: false,
                ShowAdvancedView: false,
                isContactOneTrust: false,
                isAddingConnections: false,
                moduleList: [
                    ModuleFilters.SingleSignOn,
                ],
                logo: {
                    url: "images/intg-systems/adfs.svg",
                    style: { "background-color": "#ffffff" },
                },
                illustration: {
                    url: "images/intg-systems/adfs-illustration.svg",
                },
            },
            form: {
                showInbound: false,
                showOutbound: false,
            },
        },
        onelogin: {
            meta: {
                id: SystemConstants.ONELOGIN,
                permission: "ISSystemOnelogin",
                name: "OneLogin",
                nameKey: "",
                displayName: "Onelogin",
                descriptionKeys: [
                    "SystemDescription",
                    "SystemDescription.para1",
                    "SystemDescription.para2",
                ],
                typeKey: "SystemTypeProductivity",
                publishedByKey: "IntegrationSystemDocumentation",
                publisherLink: "https://support.onetrust.com/hc/en-us/articles/115004345627",
                publishedDt: "",
                showInMarketplace: true,
                isConnectable: true,
                isComingSoon: false,
                ShowAdvancedView: false,
                isContactOneTrust: false,
                isAddingConnections: false,
                moduleList: [
                    ModuleFilters.SingleSignOn,
                ],
                logo: {
                    url: "images/intg-systems/onelogin.svg",
                    style: { "background-color": "#1C1F2A" },
                },
                illustration: {
                    url: "images/intg-systems/onelogin-illustration.svg",
                },
            },
            form: {
                showInbound: false,
                showOutbound: false,
            },
        },
        gsuite: {
            meta: {
                id: SystemConstants.GSUITE,
                permission: "ISSystemGSuite",
                name: "g suite",
                nameKey: "",
                displayName: "G-Suite",
                descriptionKeys: [
                    "SystemDescription",
                    "SystemDescription.para1",
                    "GsuiteDescription.para2",
                ],
                typeKey: "SystemTypeProductivity",
                publishedByKey: "IntegrationSystemDocumentation",
                publisherLink: "https://support.onetrust.com/hc/en-us/articles/115008172047",
                publishedDt: "",
                showInMarketplace: true,
                isConnectable: true,
                isComingSoon: false,
                ShowAdvancedView: false,
                isContactOneTrust: false,
                isAddingConnections: false,
                moduleList: [
                    ModuleFilters.SingleSignOn,
                    ModuleFilters.Calendar,
                ],
                logo: {
                    url: "images/intg-systems/g-suite.svg",
                    style: { "background-color": "#ffffff" },
                },
            },
            form: {
                showInbound: false,
                showOutbound: false,
            },
        },
        drupal: {
            meta: {
                id: SystemConstants.DRUPAL,
                permission: "ISSystemDrupal",
                name: "drupal",
                nameKey: "",
                displayName: "Drupal",
                descriptionKeys: [
                    "CookieConsentModuleDescription",
                ],
                typeKey: "SystemTypeProductivity",
                publishedByKey: "IntegrationSystemDocumentation",
                publisherLink: "https://support.onetrust.com/hc/en-us/articles/115013749467",
                publishedDt: "",
                showInMarketplace: true,
                isConnectable: true,
                isComingSoon: false,
                ShowAdvancedView: false,
                isAddingConnections: false,
                isContactOneTrust: true,
                upgradeModuleName: "Drupal Integration",
                moduleList: [
                    ModuleFilters.Cookies,
                ],
                logo: {
                    url: "images/intg-systems/drupal.svg",
                    style: { "background-color": "#ffffff" },
                },
                illustration: {
                    url: "images/intg-systems/cms-illustration.svg",
                },
            },
            form: {
                showInbound: false,
                showOutbound: false,
            },
        },
        wordpress: {
            meta: {
                id: SystemConstants.WORDPRESS,
                permission: "ISSystemWordpress",
                name: "wordpress",
                nameKey: "",
                displayName: "Wordpress",
                descriptionKeys: [
                    "CookieConsentModuleDescription",
                ],
                typeKey: "SystemTypeProductivity",
                publishedByKey: "IntegrationSystemDocumentation",
                publisherLink: "https://support.onetrust.com/hc/en-us/articles/115013749467",
                publishedDt: "",
                showInMarketplace: true,
                isConnectable: true,
                isComingSoon: false,
                ShowAdvancedView: false,
                isContactOneTrust: true,
                upgradeModuleName: "Wordpress Integration",
                isAddingConnections: false,
                moduleList: [
                    ModuleFilters.Cookies,
                ],
                logo: {
                    url: "images/intg-systems/wordpress.svg",
                    style: { "background-color": "#21759B" },
                },
                illustration: {
                    url: "images/intg-systems/cms-illustration.svg",
                },
            },
            form: {
                showInbound: false,
                showOutbound: false,
            },
        },
        tealium: {
            meta: {
                id: SystemConstants.TEALIUM,
                permission: "ISSystemTealiem",
                name: "tealium",
                nameKey: "",
                displayName: "Tealium",
                descriptionKeys: [
                    "CookieConsentModuleDescription",
                ],
                typeKey: "SystemTypeProductivity",
                publishedByKey: "IntegrationSystemDocumentation",
                publisherLink: "https://support.onetrust.com/hc/en-us/articles/115013749467",
                publishedDt: "",
                showInMarketplace: true,
                isConnectable: true,
                isComingSoon: false,
                ShowAdvancedView: false,
                isAddingConnections: false,
                isContactOneTrust: true,
                upgradeModuleName: "Tealium Integration",
                moduleList: [
                    ModuleFilters.Cookies,
                ],
                logo: {
                    url: "images/intg-systems/tealium.svg",
                    style: { "background-color": "#007CC2" },
                },
                illustration: {
                    url: "images/intg-systems/tealium-illustration.svg",
                },
            },
            form: {
                showInbound: false,
                showOutbound: false,
            },
        },
        joomla: {
            meta: {
                id: SystemConstants.JOOMLA,
                permission: "ISSystemJoomla",
                name: "joomla",
                nameKey: "",
                displayName: "Joomla",
                descriptionKeys: [
                    "CookieConsentModuleDescription",
                ],
                typeKey: "SystemTypeProductivity",
                publishedByKey: "IntegrationSystemDocumentation",
                publisherLink: "https://support.onetrust.com/hc/en-us/articles/115013749467",
                publishedDt: "",
                showInMarketplace: true,
                isConnectable: true,
                isComingSoon: false,
                ShowAdvancedView: false,
                isAddingConnections: false,
                isContactOneTrust: true,
                upgradeModuleName: "Joomla Integration",
                moduleList: [
                    ModuleFilters.Cookies,
                ],
                logo: {
                    url: "images/intg-systems/joomla.svg",
                    style: { "background-color": "#25304F" },
                },
                illustration: {
                    url: "images/intg-systems/cms-illustration.svg",
                },
            },
            form: {
                showInbound: false,
                showOutbound: false,
            },
        },
        squarespace: {
            meta: {
                id: SystemConstants.SQUARESPACE,
                permission: "ISSystemSquarespace",
                name: "squarespace",
                nameKey: "",
                displayName: "Squarespace",
                descriptionKeys: [
                    "CookieConsentModuleDescription",
                ],
                typeKey: "SystemTypeProductivity",
                publishedByKey: "IntegrationSystemDocumentation",
                publisherLink: "https://support.onetrust.com/hc/en-us/articles/115013749467",
                publishedDt: "",
                showInMarketplace: true,
                isConnectable: true,
                isComingSoon: false,
                ShowAdvancedView: false,
                isAddingConnections: false,
                isContactOneTrust: true,
                upgradeModuleName: "Squarespace Integration",
                moduleList: [
                    ModuleFilters.Cookies,
                ],
                logo: {
                    url: "images/intg-systems/squarespace.svg",
                    style: { "background-color": "#070707" },
                },
                illustration: {
                    url: "images/intg-systems/cms-illustration.svg",
                },
            },
            form: {
                showInbound: false,
                showOutbound: false,
            },
        },
        collibra: {
            meta: {
                id: SystemConstants.COLLIBRA,
                permission: "ISSystemCollibra",
                name: "collibra",
                nameKey: "",
                displayName: "Collibra",
                descriptionKeys: [
                    "BulkImportModuleDescription",
                ],
                typeKey: "SystemTypeProductivity",
                publishedByKey: "IntegrationSystemDocumentation",
                publisherLink: "https://support.onetrust.com/hc/en-us/articles/360000136188",
                publishedDt: "",
                showInMarketplace: true,
                isConnectable: true,
                isComingSoon: false,
                ShowAdvancedView: false,
                isContactOneTrust: false,
                isAddingConnections: false,
                moduleList: [
                    ModuleFilters.DataMapping,
                ],
                logo: {
                    url: "images/intg-systems/collibra.svg",
                    style: { "background-color": "#6CC04A" },
                },
                illustration: {
                    url: "images/intg-systems/import-export-illustration.svg",
                },
            },
            form: {
                showInbound: false,
                showOutbound: false,
            },
        },
        atm: {
            meta: {
                id: SystemConstants.ATM,
                permission: "ISSystemATMCMS",
                name: "Adobe Tag Manager",
                nameKey: "",
                displayName: "Adobe Tag Manager",
                descriptionKeys: [
                    "CookieConsentModuleDescriptionV2",
                ],
                typeKey: "SystemTypeProductivity",
                publishedByKey: "IntegrationSystemDocumentation",
                publisherLink: "https://support.onetrust.com/hc/en-us/articles/115013749467",
                publishedDt: "",
                showInMarketplace: true,
                isConnectable: true,
                isComingSoon: false,
                ShowAdvancedView: false,
                isAddingConnections: false,
                isContactOneTrust: true,
                upgradeModuleName: "Adobe Tag Manager Integration",
                moduleList: [
                    ModuleFilters.Cookies,
                ],
                logo: {
                    url: "images/intg-systems/atm.svg",
                    style: { "background-color": "#ffffff" },
                },
                illustration: {
                    url: "images/intg-systems/adobe-illustration.svg",
                },
            },
            form: {
                showInbound: false,
                showOutbound: false,
            },
        },
        gtm: {
            meta: {
                id: SystemConstants.GTM,
                permission: "ISSystemGTM",
                name: "Google Tag Manager",
                nameKey: "",
                displayName: "Google Tag Manager",
                descriptionKeys: [
                    "CookieConsentModuleDescriptionV2",
                ],
                typeKey: "SystemTypeProductivity",
                publishedByKey: "IntegrationSystemDocumentation",
                publisherLink: "https://support.onetrust.com/hc/en-us/articles/115003274887",
                publishedDt: "",
                showInMarketplace: true,
                isConnectable: true,
                isComingSoon: false,
                ShowAdvancedView: false,
                isContactOneTrust: true,
                upgradeModuleName: "Google Tag Manager Integration",
                moduleList: [
                    ModuleFilters.Cookies,
                ],
                logo: {
                    url: "images/intg-systems/gtm.svg",
                    style: { "background-color": "#ffffff" },
                },
                illustration: {
                    url: "images/intg-systems/gtm-illustration.svg",
                },
            },
            form: {
                showInbound: false,
                showOutbound: false,
            },
        },
        azure: {
            meta: {
                id: SystemConstants.AZURE,
                permission: "ISSystemAzureAD",
                name: "Azure AD",
                nameKey: "",
                displayName: "Azure AD",
                descriptionKeys: [
                    "SystemDescription",
                    "SystemDescription.para1",
                    "SystemDescription.para2",
                ],
                typeKey: "SystemTypeProductivity",
                publishedByKey: "IntegrationSystemDocumentation",
                publisherLink: "https://support.onetrust.com/hc/en-us/articles/115004345627",
                publishedDt: "",
                showInMarketplace: true,
                isConnectable: true,
                isComingSoon: false,
                ShowAdvancedView: false,
                isContactOneTrust: false,
                moduleList: [
                    ModuleFilters.SingleSignOn,
                ],
                logo: {
                    url: "images/intg-systems/azure.svg",
                    style: { "background-color": "#ffffff" },
                },
                illustration: {
                    url: "images/intg-systems/azure-illustration.svg",
                },
            },
            form: {
                showInbound: false,
                showOutbound: false,
            },
        },
        marketo: {
            meta: {
                id: SystemConstants.MARKETO,
                permission: "ISSystemMarketo",
                name: "Marketo",
                nameKey: "",
                displayName: "Marketo",
                descriptionKeys: [
                    "UniversalConsentModuleDescription",
                ],
                typeKey: "SystemTypeProductivity",
                publishedByKey: "IntegrationSystemDocumentation",
                publisherLink: "https://support.onetrust.com/hc/en-us/articles/360005333993-OneTrust-Marketo-Integration",
                publishedDt: "",
                showInMarketplace: false,
                isConnectable: true,
                isComingSoon: false,
                ShowAdvancedView: false,
                isContactOneTrust: true,
                upgradeModuleName: "Marketo Integration",
                moduleList: [
                    ModuleFilters.UniversalConsent,
                ],
                logo: {
                    url: "images/intg-systems/marketo.svg",
                    style: { "background-color": "#5A54A4" },
                },
                illustration: {
                    url: "images/intg-systems/marketo-illustration.svg",
                },
            },
            form: {
                showInbound: false,
                showOutbound: false,
            },
        },
        amp: {
            meta: {
                id: SystemConstants.AMP,
                permission: "ISSystemAMP",
                name: "AMP",
                nameKey: "",
                displayName: "AMP",
                descriptionKeys: [
                    "AMPDescriptionCC",
                ],
                typeKey: "SystemTypeProductivity",
                publishedByKey: "IntegrationSystemDocumentation",
                publisherLink: "https://support.onetrust.com/hc/en-us/articles/115013749467",
                publishedDt: "",
                showInMarketplace: true,
                isConnectable: true,
                isComingSoon: false,
                ShowAdvancedView: false,
                isContactOneTrust: true,
                upgradeModuleName: "AMP Integration",
                moduleList: [
                    ModuleFilters.Cookies,
                ],
                logo: {
                    url: "images/intg-systems/amp.svg",
                    style: { "background-color": "#ffffff" },
                },
            },
            form: {
                showInbound: false,
                showOutbound: false,
            },
        },
        cms: {
            meta: {
                id: SystemConstants.CMS,
                permission: "ISSystemCustomCMS",
                name: "Custom CMS",
                nameKey: "",
                displayName: "Custom CMS",
                descriptionKeys: [
                    "CustomCmsDescription",
                ],
                typeKey: "SystemTypeProductivity",
                publishedByKey: "IntegrationSystemDocumentation",
                publisherLink: "https://support.onetrust.com/hc/en-us/articles/115013749467",
                publishedDt: "",
                showInMarketplace: true,
                isConnectable: true,
                isComingSoon: false,
                ShowAdvancedView: false,
                isContactOneTrust: true,
                upgradeModuleName: "Custom CMS Integration",
                moduleList: [
                    ModuleFilters.Cookies,
                ],
                logo: {
                    url: "images/intg-systems/cms.svg",
                    style: { "background-color": "#B8BFC3" },
                },
                illustration: {
                    url: "images/intg-systems/cms-illustration.svg",
                },
            },
            form: {
                showInbound: false,
                showOutbound: false,
            },
        },
        aem: {
            meta: {
                id: SystemConstants.AEM,
                permission: "ISSystemAdobeEM",
                name: "Adobe Experience Manager",
                nameKey: "",
                displayName: "Adobe Experience Mgr",
                descriptionKeys: [
                    "AdobeExperienceMgrDescription",
                ],
                typeKey: "SystemTypeProductivity",
                publishedByKey: "IntegrationSystemDocumentation",
                publisherLink: "https://support.onetrust.com/hc/en-us/articles/115013749467",
                publishedDt: "",
                showInMarketplace: true,
                isConnectable: true,
                isComingSoon: false,
                ShowAdvancedView: false,
                isContactOneTrust: true,
                upgradeModuleName: "Adobe Experience Manager Integration",
                moduleList: [
                    ModuleFilters.Cookies,
                ],
                logo: {
                    url: "images/intg-systems/aem.svg",
                    style: { "background-color": "#200F00" },
                },
                illustration: {
                    url: "images/intg-systems/cms-illustration.svg",
                },
            },
            form: {
                showInbound: false,
                showOutbound: false,
            },
        },
        adobe: {
            meta: {
                id: SystemConstants.ADOBELAUNCH,
                permission: "ISSystemAdobeLB",
                name: "Launch By Adobe",
                nameKey: "",
                displayName: "Launch By Adobe",
                descriptionKeys: [
                    "LaunchByAdobeDescription",
                ],
                typeKey: "SystemTypeProductivity",
                publishedByKey: "IntegrationSystemDocumentation",
                publisherLink: "https://support.onetrust.com/hc/en-us/articles/115013749467",
                publishedDt: "",
                showInMarketplace: true,
                isConnectable: true,
                isComingSoon: false,
                ShowAdvancedView: false,
                isContactOneTrust: true,
                upgradeModuleName: "Adobe Launch Integration",
                moduleList: [
                    ModuleFilters.Cookies,
                ],
                logo: {
                    url: "images/intg-systems/adobe_logo.svg",
                    style: { "background-color": "#ED1C24" },
                },
                illustration: {
                    url: "images/intg-systems/adobe-illustration.svg",
                },
            },
            form: {
                showInbound: false,
                showOutbound: false,
            },
        },
        ensighten: {
            meta: {
                id: SystemConstants.ENSIGHTEN,
                permission: "ISSystemEnsighten",
                name: "Ensighten",
                nameKey: "",
                displayName: "Ensighten",
                descriptionKeys: [
                    "EnsightenDescriptionV2",
                ],
                typeKey: "SystemTypeProductivity",
                publishedByKey: "IntegrationSystemDocumentation",
                publisherLink: "https://support.onetrust.com/hc/en-us/articles/360000534478",
                publishedDt: "",
                showInMarketplace: true,
                isConnectable: true,
                isComingSoon: false,
                ShowAdvancedView: false,
                isContactOneTrust: true,
                moduleList: [
                    ModuleFilters.Cookies,
                ],
                logo: {
                    url: "images/intg-systems/ensighten.png",
                    style: { "background-color": "#ffffff" },
                },
                illustration: {
                    url: "images/intg-systems/ensighten-illustration.png",
                },
            },
            form: {
                showInbound: false,
                showOutbound: false,
            },
        },
        mParticle: {
            meta: {
                id: SystemConstants.MPARTICLE,
                permission: "ISSystemmParticle",
                name: "mParticle",
                nameKey: "",
                displayName: "mParticle",
                descriptionKeys: [
                    "mParticleDescriptionV2",
                ],
                typeKey: "SystemTypeProductivity",
                publishedByKey: "IntegrationSystemDocumentation",
                publisherLink: "https://support.onetrust.com/hc/en-us/articles/360000637998",
                publishedDt: "",
                showInMarketplace: true,
                isConnectable: true,
                isComingSoon: false,
                ShowAdvancedView: false,
                isContactOneTrust: true,
                moduleList: [
                    ModuleFilters.Cookies,
                    ModuleFilters.UniversalConsent,
                ],
                logo: {
                    url: "images/intg-systems/mp-logo.svg",
                    style: { "background-color": "#121212" },
                },
            },
            form: {
                showInbound: false,
                showOutbound: false,
            },
        },
        bigId: {
            meta: {
                id: SystemConstants.BIGID,
                permission: "ISSystemmParticle",
                name: "BigID",
                nameKey: "",
                displayName: "BigID",
                descriptionKeys: [
                    "BigIdDescriptionV2",
                ],
                typeKey: "SystemTypeProductivity",
                publishedByKey: "IntegrationSystemDocumentation",
                publisherLink: "https://support.onetrust.com/hc/en-us/articles/360000870677",
                publishedDt: "",
                showInMarketplace: true,
                isConnectable: true,
                isComingSoon: false,
                ShowAdvancedView: false,
                isContactOneTrust: false,
                moduleList: [
                    ModuleFilters.E_Discovery,
                ],
                logo: {
                    url: "images/intg-systems/BigId.png",
                    style: { "background-color": "#121212" },
                },
            },
            form: {
                showInbound: false,
                showOutbound: false,
            },
        },
        dataGuise: {
            meta: {
                id: SystemConstants.DATAGUISE,
                permission: "ISSystemmParticle",
                name: "DataGuise",
                nameKey: "",
                displayName: "DataGuise",
                descriptionKeys: [
                    "DataGuiseDescriptionV2",
                ],
                typeKey: "SystemTypeProductivity",
                publishedByKey: "IntegrationSystemDocumentation",
                publisherLink: " https://support.onetrust.com/hc/en-us/articles/360000870697",
                publishedDt: "",
                showInMarketplace: true,
                isConnectable: true,
                isComingSoon: false,
                ShowAdvancedView: false,
                isContactOneTrust: false,
                moduleList: [
                    ModuleFilters.E_Discovery,
                ],
                logo: {
                    url: "images/intg-systems/dataguise-logo.png",
                    style: { "background-color": "#ffffff" },
                },
            },
            form: {
                showInbound: false,
                showOutbound: false,
            },
        },
        Varonis: {
            meta: {
                id: SystemConstants.VARONIS,
                permission: "ISSystemmParticle",
                name: "Varonis",
                nameKey: "",
                displayName: "Varonis",
                descriptionKeys: [
                    "VaronisDescriptionV2",
                ],
                typeKey: "SystemTypeProductivity",
                publishedByKey: "IntegrationSystemDocumentation",
                publisherLink: "https://support.onetrust.com/hc/en-us/articles/360000870737",
                publishedDt: "",
                showInMarketplace: true,
                isConnectable: true,
                isComingSoon: false,
                ShowAdvancedView: false,
                isContactOneTrust: false,
                moduleList: [
                    ModuleFilters.E_Discovery,
                ],
                logo: {
                    url: "images/intg-systems/varonis_Logo.svg",
                    style: { "background-color": "#121212" },
                },
            },
            form: {
                showInbound: false,
                showOutbound: false,
            },
        },
        MailChimp: {
            meta: {
                id: SystemConstants.MAILCHIMP,
                permission: "ISSystemmParticle",
                name: "MailChimp",
                nameKey: "",
                displayName: "MailChimp",
                descriptionKeys: [
                    "MailChimpDescriptionV2",
                ],
                typeKey: "SystemTypeProductivity",
                publishedByKey: "IntegrationSystemDocumentation",
                publisherLink: "https://support.onetrust.com/hc/en-us/articles/360000870737",
                publishedDt: "",
                showInMarketplace: true,
                isConnectable: true,
                isComingSoon: false,
                ShowAdvancedView: false,
                isContactOneTrust: false,
                moduleList: [
                    ModuleFilters.UniversalConsent,
                ],
                logo: {
                    url: "images/intg-systems/mailchimp-logo.svg",
                    style: { "background-color": "#ffe01b" },
                },
            },
            form: {
                showInbound: false,
                showOutbound: false,
            },
        },
        ibmSecurity: {
            meta: {
                id: SystemConstants.IBMSECURITY,
                permission: "ISSystemmParticle",
                name: "IBM Security",
                nameKey: "",
                displayName: "IBM Security",
                descriptionKeys: [
                    "IbmSecurityDescription",
                ],
                typeKey: "SystemTypeProductivity",
                publishedByKey: "IntegrationSystemDocumentation",
                publisherLink: "https://support.onetrust.com/hc/en-us/articles/360000870717",
                publishedDt: "",
                showInMarketplace: true,
                isConnectable: true,
                isComingSoon: false,
                ShowAdvancedView: false,
                isContactOneTrust: false,
                moduleList: [
                    ModuleFilters.E_Discovery,
                ],
                logo: {
                    url: "images/intg-systems/ibm-security-logo.svg",
                    style: { "background-color": "#ffffff" },
                },
            },
            form: {
                showInbound: false,
                showOutbound: false,
            },
        },
    };
}
