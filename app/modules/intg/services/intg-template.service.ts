// Angular
import { Injectable } from "@angular/core";

// Services
import { ProtocolService } from "modules/core/services/protocol.service";

// Interfaces
import {
    IProtocolConfig,
    IProtocolMessages,
    IProtocolResponse,
} from "interfaces/protocol.interface";
import {
    IITemplate,
    IICreateCustomTemplate,
    IIUpdateCustomTemplate,
} from "modules/intg/interfaces/integration.interface";
import { IPageableOf } from "interfaces/pagination.interface";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

// Rxjs
import { from, Observable } from "rxjs";

// Enums
import { StatusCodes } from "enums/status-codes.enum";

@Injectable()
export class IntgTemplateService {

    constructor(
        private OneProtocol: ProtocolService,
        private translatePipe: TranslatePipe,
    ) { }

    getTemplates(page: number = 0, size: number = 100, sort = "name,asc"): Observable<IProtocolResponse<IPageableOf<IITemplate>>> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("GET", `/integrationmanager/api/v1/templates`, { page, size, sort });
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("CannotRetrieveConnectors") } };
        return from(this.OneProtocol.http(config, messages));
    }

    getTemplateById(id: string): ng.IPromise<IProtocolResponse<IITemplate>> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("GET", `/integrationmanager/api/v1/templates/${id}`);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("CannotRetrieveConnectorById") } };
        return this.OneProtocol.http(config, messages);
    }

    createTemplate(customTemplate: IICreateCustomTemplate, isImport = false): ng.IPromise<IProtocolResponse<IITemplate>> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("POST", `/integrationmanager/api/v1/templates`, null, customTemplate);
        const messages: IProtocolMessages = {
            Error: {
                custom: isImport ? this.translatePipe.transform("CannotCreateSystem") : this.translatePipe.transform("CannotCreateConnector"),
            } };
        return this.OneProtocol.http(config, messages);
    }

    updateTemplate(updatedCustomTemplate: IIUpdateCustomTemplate): ng.IPromise<IProtocolResponse<IITemplate>> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("PUT", `/integrationmanager/api/v1/templates`, null, updatedCustomTemplate);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("CannotUpdateConnector") } };
        return this.OneProtocol.http(config, messages);
    }

    deleteTemplate(id: string, integrationId: string): ng.IPromise<IProtocolResponse<IITemplate>> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("DELETE", `/integrationmanager/api/v1/templates/${id}`, { integrationId });
        const messages: IProtocolMessages = StatusCodes.BadRequest ? { Error: { custom: this.translatePipe.transform("UnableToDeleteSystemContainingWorkflows") } }
            : { Error: { custom: this.translatePipe.transform("CannotDeleteConnector") } };
        return this.OneProtocol.http(config, messages);
    }
}
