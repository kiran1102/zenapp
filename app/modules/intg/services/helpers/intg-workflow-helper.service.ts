// Angular
import { Injectable } from "@angular/core";
import { FormControl, ValidationErrors } from "@angular/forms";

// 3rd party
import { forEach } from "lodash";

// Rxjs
import { Subject, Observable } from "rxjs";

// Interfaces
import {
    IIField,
    IIWorkflowNode,
    IISelectedAction,
    IRequestSchema,
    IITrigger,
    IISeededEvent,
    ICustomValuesResponse,
    IAvailableEvent,
} from "modules/intg/interfaces/integration.interface";
import {
    ProcessType,
    IntgActionHeaderTypes,
    IntgActionMethodTypes,
    IntgActionTabs,
    EventCode,
} from "modules/intg/constants/integration.constant";
import {
    IKeyValue,
    IStringMap,
} from "interfaces/generic.interface";
import { ITabsNav } from "interfaces/one-tabs-nav.interface";

// Vitreus
import { OtModalService } from "@onetrust/vitreus";

// Components
import { IntgWorkflowParseSchemaModal } from "intsystemcomponent/workflows/builder/intg-workflow-parse-schema-modal.component";

// Pipes and constants.
import { TranslatePipe } from "pipes/translate.pipe";
import { LINK } from "constants/regex.constant";

@Injectable()
export class IntgWorkflowHelper {

    private workflowActionUpdated = new Subject();
    private saveWorkflow = new Subject();
    private trigger = new Subject();
    private unsavedChanges = new Subject();

    // tslint:disable-next-line:member-ordering
    $workflowActionUpdated = this.workflowActionUpdated.asObservable();
    // tslint:disable-next-line:member-ordering
    saveWorkflow$ = this.saveWorkflow.asObservable();
    // tslint:disable-next-line:member-ordering
    trigger$ = this.trigger.asObservable();
    // tslint:disable-next-line:member-ordering
    unsavedChanges$ = this.unsavedChanges.asObservable();

    constructor(
        private otModalService: OtModalService,
        private translatePipe: TranslatePipe,
    ) {}

    updateWorkflowAction(workflowActions: IISelectedAction[]) {
        this.workflowActionUpdated.next(workflowActions);
    }

    setUnsavedChanges(hasUnsavedChanges: boolean) {
        this.unsavedChanges.next(hasUnsavedChanges);
    }

    triggerSaveWorkflow() {
        this.saveWorkflow.next();
    }

    setTriggerForWebHook(webhookTrigger: IITrigger) {
        this.trigger.next(webhookTrigger);
    }

    insertAtCursorPosition(responseBody: HTMLTextAreaElement, responseText: string) {
        if (responseBody.selectionStart || responseBody.selectionStart === 0) {
            responseBody.value = responseBody.value.substring(0, responseBody.selectionStart)
                + responseText
                + responseBody.value.substring(responseBody.selectionEnd, responseBody.value.length);
        } else {
            responseBody.value += responseText;
        }
    }

    setEventFieldsLabel(fields: IIField[], nodeLabel: string): IIField[] {
        forEach(fields, (field: IIField) => {
            field.name = this.replaceNodelLabel(field.name, nodeLabel);
            field.body = this.replaceNodelLabel(field.body, nodeLabel);
        });
        return fields;
    }

    createMessageLogNode(workflowId: string): IIWorkflowNode {
        return  {
            credentialsId: null,
            nodeLabel: "messagelog",
            processType: ProcessType.MESSAGE_LOG,
            workflowId,
            nodeDescription: "Terminating Node : messagelog",
            processContext: {
                MessageLog: {
                    type: ProcessType.MESSAGE_LOG,
                },
            },
        };
    }

    getActionName(name: string): string {
        return name.split("[")[0].trim();
    }

    launchParseSchemaModal(): Observable<string> {
        return this.otModalService.create(
            IntgWorkflowParseSchemaModal,
            { isComponent: true },
        ).asObservable();
    }

    getResponseSchema(node: IIWorkflowNode, schema: string): Partial<IRequestSchema> {
        const responseSchema = node.responseSchema || (schema ? { name: node.nodeDescription } as IRequestSchema : null);
        if (responseSchema) {
            responseSchema.schema = schema;
        }
        return responseSchema;
    }

    calculateEventLookup(
        workflowActions: IISelectedAction[],
        currentEvent: IISeededEvent,
        trigger: IITrigger,
        currentIndex: number,
        customFields: ICustomValuesResponse[],
    ): IAvailableEvent[] {
        let  availableEvent: IAvailableEvent[];
        let customFieldArray = [];
        if (customFields) {
            customFieldArray = customFields.map((value) => {
                return {
                    name: value.name,
                    key: value.name,
                    body: value.lookupExpression,
                    type: value.parameterType,
                };
            });
            availableEvent = [{
                category: this.translatePipe.transform("CustomValues"),
                isOtEvent: true,
                fields: [...customFieldArray],
                isCustom: true,
            }];
        }
        if (!availableEvent) {
            availableEvent = [];
        }
        if (currentEvent && currentEvent.eventFields && currentEvent.eventFields.fields && currentEvent.eventFields.fields.length) {
            if (currentEvent.code !== EventCode.SCHEDULER.toString()) {
                availableEvent.push({
                    category: currentEvent.name,
                    isOtEvent: true,
                    fields: currentEvent.eventFields ? [...currentEvent.eventFields.fields] : [],
                });
            }
        } else if (trigger && trigger.lookupFields && trigger.lookupFields.fields && trigger.lookupFields.fields.length) {
            availableEvent.push({
                category: currentEvent.name,
                isOtEvent: true,
                fields: trigger.lookupFields ? [...trigger.lookupFields.fields] : [],
            });
        }

        // Calculate event fields dynamically if atleast one action is selected.
        if (currentIndex) {
            // Picking actions only till current action.
            const previousActions =  workflowActions.slice(0, currentIndex);
            let previousSequence = -1;
            let applyEachIcon: boolean;
            previousActions.forEach((addedAction: IISelectedAction, index) => {
                const selectedNode = addedAction.httpNode ? addedAction.httpNode : addedAction.applyEachNode;
                if (addedAction.applyEachNode) {
                    applyEachIcon = previousActions[index - 1].isOtAction;
                }
                if (selectedNode && selectedNode.responseSchema) {
                    const fields = selectedNode.responseSchema && selectedNode.responseSchema.lookupFields ? selectedNode.responseSchema.lookupFields.fields : [];
                    const nodeDescription = addedAction.applyEachNode ? addedAction.applyEachNode.nodeLabel : selectedNode.nodeDescription;
                    if (nodeDescription)
                        availableEvent.push({
                            category: nodeDescription,
                            isOtEvent: addedAction.applyEachNode ? applyEachIcon : addedAction.isOtAction,
                            fields: this.setEventFieldsLabel([...fields], selectedNode.nodeLabel ? selectedNode.nodeLabel : `http_${previousSequence + 1}`),
                        });

                    if (selectedNode.nodeLabel) {
                        previousSequence = Number(selectedNode.nodeLabel.substring(5, selectedNode.nodeLabel.length));
                    } else {
                        previousSequence++;
                    }
                }
            });
        }
        return availableEvent;
    }

    getHeaderTypes(): Array<IKeyValue<string>> {
        return [
            {
                key: IntgActionHeaderTypes.Content_Type,
                value: IntgActionHeaderTypes.Content_Type,
            },
            {
                key: IntgActionHeaderTypes.Authorization,
                value: IntgActionHeaderTypes.Authorization,
            },
            {
                key: IntgActionHeaderTypes.Accept,
                value: IntgActionHeaderTypes.Accept,
            },
        ];
    }

    getMethodTypes(): Array<IKeyValue<string>> {
        return [
            {
                key: this.translatePipe.transform(`Method.${IntgActionMethodTypes.GET}`),
                value: IntgActionMethodTypes.GET,
            },
            {
                key: this.translatePipe.transform(`Method.${IntgActionMethodTypes.PUT}`),
                value: IntgActionMethodTypes.PUT,
            },
            {
                key: this.translatePipe.transform(`Method.${IntgActionMethodTypes.PATCH}`),
                value: IntgActionMethodTypes.PATCH,
            },
            {
                key: this.translatePipe.transform(`Method.${IntgActionMethodTypes.POST}`),
                value: IntgActionMethodTypes.POST,
            },
            {
                key: this.translatePipe.transform(`Method.${IntgActionMethodTypes.DELETE}`),
                value: IntgActionMethodTypes.DELETE,
            },
        ];
    }

    getAllowedTabs(): IStringMap<ITabsNav> {
         return {
            SYSTEM: {
                id: IntgActionTabs.SYSTEM,
                text: this.translatePipe.transform("System"),
            },
            ONETRUST: {
                id: IntgActionTabs.ONETRUST,
                text: this.translatePipe.transform("OneTrust"),
            },
            LOGIC: {
                id: IntgActionTabs.LOGIC,
                text: this.translatePipe.transform("Logic"),
            },
            REQUEST: {
                id: IntgActionTabs.REQUEST,
                text: this.translatePipe.transform("Request"),
            },
            RESPONSE: {
                id: IntgActionTabs.RESPONSE,
                text: this.translatePipe.transform("ResponseSchema"),
            },
            SAMPLE: {
                id: IntgActionTabs.SAMPLE,
                text: this.translatePipe.transform("ResponseSample"),
            },
        };
    }

    refernceUrlValidator = (option: FormControl): ValidationErrors | null => {
        const valid = (option.value as string).startsWith("${") || LINK.test(option.value);
        return valid ? null : { invalidUrl: true } ;
    }

    private replaceNodelLabel(inputValue: string, nodeLabel: string): string {
        // Regular expression for selecting all occurence of text between 'step.' and '.' contaning 'step.' as well.
        const regex = new RegExp(/(step\.).*?(?=\.)/, "g");

        return inputValue.replace(regex, `step.${nodeLabel}`);
    }
}
