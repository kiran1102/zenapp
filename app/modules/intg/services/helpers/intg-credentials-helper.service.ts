// 3rd party
import {
    forEach,
    map,
    keys,
} from "lodash";

// Angular
import {
    FormGroup,
    FormArray,
    FormBuilder,
} from "@angular/forms";

// Interfaces
import {
    IStringMap,
    IKeyValue,
} from "interfaces/generic.interface";
import {
    IICreateCredential,
    IICredential,
    IIOAuth2RequestBody,
} from "modules/intg/interfaces/integration.interface";

// Constants
import {
    IntgGrantType,
    IntgAuthType,
    ClientAuthenticationType,
    IntgGrantTypeQueryParam,
} from "modules/intg/constants/integration.constant";

export class IntgCredentialsHelper {

    public convertFormDataToModel(credentialForm: FormGroup, credentialId: string): IICreateCredential {
        const credentialFormValue = credentialForm.value;
        const credential: IICreateCredential = {
            name: credentialFormValue.name,
            description: credentialFormValue.description,
            authType: credentialFormValue.authType.value,
            authContext: {},
            id: credentialId,
        };

        const headers = this.getHeaders(credentialForm);
        const type = credentialFormValue.authType.value;
        const hostName = credentialFormValue.hostname ? credentialFormValue.hostname.trim() : "";

        switch (credential.authType) {
            case IntgAuthType.BASIC:
                credential.authContext.BasicAuth = {
                    userName: credentialFormValue.username,
                    password: credentialFormValue.password,
                    headers,
                    type,
                    hostName,
                };
                break;
            case IntgAuthType.BEARER_TOKEN:
                credential.authContext.BearerTokenAuth = {
                    token: credentialFormValue.token,
                    headers,
                    type,
                    hostName,
                };
                break;
            case IntgAuthType.NO_AUTH:
                credential.authContext.NoAuth = {
                    headers,
                    type,
                    hostName,
                };
                break;
            case IntgAuthType.OAUTH2_TOKEN:
                const oAuth = credentialFormValue.oAuth;
                const grantType = oAuth.grantType.value;
                credential.authContext.OAuth2TokenAuth = {
                    headers,
                    type,
                    grantType,
                    tokenUrl: oAuth.accessTokenUrl,
                    clientId: oAuth.clientId,
                    accessToken: credentialFormValue.oAuthResponse.accessToken,
                    authParameterType: credentialFormValue.oAuth.authParameterType.value,
                    refreshToken: credentialFormValue.oAuthResponse.refreshToken,
                    hostName,
                };
                if (grantType === IntgGrantType.Password_Credentials) {
                    credential.authContext.OAuth2TokenAuth.username = oAuth.oAuthUsername;
                    credential.authContext.OAuth2TokenAuth.password = oAuth.oAuthPassword;
                    credential.authContext.OAuth2TokenAuth.clientSecret = oAuth.clientSecret;
                } else if (grantType === IntgGrantType.Refresh) {
                    credential.authContext.OAuth2TokenAuth.clientSecret = oAuth.clientSecret;
                } else if (grantType === IntgGrantType.Client_Credentials) {
                    credential.authContext.OAuth2TokenAuth.clientSecret = oAuth.clientSecret;
                    credential.authContext.OAuth2TokenAuth.scope = oAuth.scope;
                }
                break;
        }
        return credential;
    }

    public removeControls(formGroup: FormGroup, controlNames: string[]) {
        forEach(controlNames, (name: string) => {
            if (formGroup.contains(name)) {
                formGroup.removeControl(name);
            }
        });
    }

    public getHeaders(credentialForm: FormGroup): IStringMap< string> {
        const headers = {};
        const headerArray = credentialForm.get("defaultHeader").value ;
        forEach(headerArray, (header: IKeyValue<string>) => {
            if (header.key) {
                headers[header.key] = header.value;
            }
        });
        return headers;
    }

    public getRequestBody(credentialForm: FormGroup, type: string): string | IIOAuth2RequestBody {
        const oAuth = credentialForm.value.oAuth;
        const oAuthResponse = credentialForm.value.oAuthResponse;
        if (type === ClientAuthenticationType.SEND_IN_URL_ENCODED) {
            return this.setRequestBodyForURLEncoded(oAuth, oAuthResponse);
        } else if (type === ClientAuthenticationType.SEND_IN_BODY)
            return this.setRequestBody(oAuth, oAuthResponse);
    }

    public getRequestUri(credentialForm: FormGroup): string {
        const oAuth = credentialForm.value.oAuth;
        const oAuthResponse = credentialForm.value.oAuthResponse;

        let requestUri = "";
        switch (oAuth.grantType.value) {
            case IntgGrantType.Password_Credentials:
                requestUri = `${oAuth.accessTokenUrl}?grant_type=password&client_id=${oAuth.clientId}&client_secret=${oAuth.clientSecret}&username=${oAuth.oAuthUsername}&password=${oAuth.oAuthPassword}&response_type=token`;
                break;
            case IntgGrantType.Refresh:
                requestUri = `${oAuth.accessTokenUrl}?grant_type=refresh_token&client_id=${oAuth.clientId}&client_secret=${oAuth.clientSecret}&refresh_token=${oAuthResponse.refreshToken}`;
                break;
            case IntgGrantType.Client_Credentials:
                requestUri = `${oAuth.accessTokenUrl}?grant_type=client_credentials&client_id=${oAuth.clientId}&client_secret=${oAuth.clientSecret}&scope=${oAuth.scope}`;
                break;
         }
        return requestUri;
    }

    public getHostname(credential: IICreateCredential | IICredential): string {
        let hostname: string;
        if (credential && credential.authContext) {
            switch (credential.authType) {
                case IntgAuthType.BASIC:
                    hostname = credential.authContext.BasicAuth.hostName;
                    break;
                case IntgAuthType.BEARER_TOKEN:
                    hostname = credential.authContext.BearerTokenAuth.hostName;
                    break;
                case IntgAuthType.NO_AUTH:
                    hostname = credential.authContext.NoAuth.hostName;
                    break;
                case IntgAuthType.OAUTH2_TOKEN:
                    hostname = credential.authContext.OAuth2TokenAuth.hostName;
                    break;
            }
        }
        return hostname;
    }

    public getDefaultHeaderFormArray(isEdit: boolean, credential: IICreateCredential, formBuilder: FormBuilder): FormArray {
        let headers: IStringMap<string>;
        if (isEdit) {
            switch (credential.authType) {
                case IntgAuthType.BASIC:
                    headers = credential.authContext.BasicAuth.headers;
                    break;
                case IntgAuthType.BEARER_TOKEN:
                    headers = credential.authContext.BearerTokenAuth.headers;
                    break;
                case IntgAuthType.NO_AUTH:
                    headers = credential.authContext.NoAuth.headers;
                    break;
                case IntgAuthType.OAUTH2_TOKEN:
                    headers = credential.authContext.OAuth2TokenAuth.headers;
                    break;
            }
        }
        if (keys(headers).length) {
            return formBuilder.array(map(headers, (value: string, key: string) => {
                return formBuilder.group({key, value});
            }));

        } else {
            return formBuilder.array([formBuilder.group({ key: "", value: ""})]);
        }
    }

    private setRequestBodyForURLEncoded(oAuth, oAuthResponse): string {
        let constructedRequest: string;
        switch (oAuth.grantType.value) {
            case IntgGrantType.Password_Credentials:
                constructedRequest = `grant_type=password&client_id=${oAuth.clientId}&client_secret=${oAuth.clientSecret}&username=${oAuth.oAuthUsername}&password=${oAuth.oAuthPassword}&response_type=token`;
                break;
            case IntgGrantType.Refresh:
                constructedRequest = `grant_type=refresh_token&client_id=${oAuth.clientId}&client_secret=${oAuth.clientSecret}&refresh_token=${oAuthResponse.refreshToken}`;
                break;
            case IntgGrantType.Client_Credentials:
                constructedRequest = `grant_type=client_credentials&client_id=${oAuth.clientId}&client_secret=${oAuth.clientSecret}&scope=${oAuth.scope}`;
                break;
         }
        return constructedRequest;
    }

    private setRequestBody(oAuth, oAuthResponse): IIOAuth2RequestBody {
        const requestBody = {} as IIOAuth2RequestBody;
        switch (oAuth.grantType.value) {
            case IntgGrantType.Password_Credentials:
                requestBody.grant_type = IntgGrantTypeQueryParam.Password;
                requestBody.client_id = oAuth.clientId;
                requestBody.client_secret = oAuth.clientSecret;
                requestBody.username = oAuth.oAuthUsername;
                requestBody.password = oAuth.oAuthPassword;
                requestBody.response_type = "token";
                break;
            case IntgGrantType.Refresh:
                requestBody.grant_type = IntgGrantTypeQueryParam.Refresh_Token;
                requestBody.client_id = oAuth.clientId;
                requestBody.client_secret = oAuth.clientSecret;
                requestBody.refresh_token = oAuthResponse.refreshToken;
                break;
            case IntgGrantType.Client_Credentials:
                requestBody.grant_type = IntgGrantTypeQueryParam.Client_Credentials;
                requestBody.client_id = oAuth.clientId;
                requestBody.client_secret = oAuth.clientSecret;
                requestBody.scope = oAuth.scope;
                break;
         }
        return requestBody;
    }
}
