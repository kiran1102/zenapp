// Angular
import { Injectable } from "@angular/core";

// 3rd Party
import { keyBy, map } from "lodash";

// Services
import { ProtocolService } from "modules/core/services/protocol.service";
import { IntgTemplateService } from "modules/intg/services/intg-template.service";

// Interfaces
import {
    IProtocolConfig,
    IProtocolMessages,
    IProtocolResponse,
} from "interfaces/protocol.interface";
import {
    IIIntegration,
    IICreateCustomTemplate,
    IITemplate,
    IISystem,
    IIIntegrationDetails,
    IICredential,
    IIWorkflow,
} from "modules/intg/interfaces/integration.interface";
import { IPageableOf } from "interfaces/pagination.interface";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

// Enums
import { WorkflowType } from "modules/intg/enums/workflow.enum";

// Rxjs
import { from, Observable } from "rxjs";

@Injectable()
export class IntgIntegrationsService {

    constructor(
        private OneProtocol: ProtocolService,
        private translatePipe: TranslatePipe,
        private intgTemplateService: IntgTemplateService,
    ) { }

    getIntegrations(isTemplate?: boolean, page: number = 0, size: number = 1000, sort = "name,asc", workflowType: string = WorkflowType.CUSTOM): Observable<IProtocolResponse<IPageableOf<IIIntegration>>> {
        const params = { page, size, sort, isTemplate, workflowType };
        const config: IProtocolConfig = this.OneProtocol.customConfig("GET", `/integrationmanager/api/v1/integrations`, params);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("CannotRetrieveIntegrations") } };
        return from(this.OneProtocol.http(config, messages));
    }

    getSeededIntegrations(isTemplate: boolean, page: number = 0, size: number = 1000, sort = "name,asc"): ng.IPromise<IProtocolResponse<IPageableOf<IISystem>>> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("GET", `/integrationmanager/api/v1/integrations`, { isTemplate, page, size, sort });
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("CannotRetrieveIntegrations") } };
        return this.OneProtocol.http(config, messages);
    }

    getSeededIntegrationsAsObservable(isTemplate: boolean, page: number = 0, size: number = 1000, sort = "name,asc"): Observable<IProtocolResponse<IPageableOf<IIIntegration>>> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("GET", `/integrationmanager/api/v1/integrations`, { isTemplate, page, size, sort });
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("CannotRetrieveIntegrations") } };
        return from(this.OneProtocol.http(config, messages));
    }

    getIntegrationById(id: string): ng.IPromise<IProtocolResponse<IIIntegration>> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("GET", `/integrationmanager/api/v1/integrations/${id}`);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("CannotRetrieveIntegrationsById") } };
        return this.OneProtocol.http(config, messages);
    }

    createIntegration(customConnector: IICreateCustomTemplate, isSystem: boolean): ng.IPromise<IProtocolResponse<IIIntegration>> {
        return this.intgTemplateService.createTemplate(customConnector).then((response) => {
            if (!response.result) return response;
            const customIntg = {
                name: response.data.name,
                templateId: response.data.id,
                template: isSystem,
            };
            const config: IProtocolConfig = this.OneProtocol.customConfig("POST", `/integrationmanager/api/v1/integrations`, null, customIntg);
            const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("CannotCreateIntegrations") } };
            return this.OneProtocol.http(config, messages);
        });
    }

    updateIntegration(updatedIntegration: IIIntegration): ng.IPromise<IProtocolResponse<IIIntegration>> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("PUT", `/integrationmanager/api/v1/integrations`, null, updatedIntegration);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("CannotUpdateIntegrations") } };
        return this.OneProtocol.http(config, messages);
    }

    deleteIntegration(id: string): ng.IPromise<IProtocolResponse<IIIntegration>> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("DELETE", `/integrationmanager/api/v1/integrations/${id}`);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("CannotDeleteIntegration") } };
        return this.OneProtocol.http(config, messages);
    }

    getDetails(id: string, errorMessage = "CannotRetrieveIntegrationsById"): ng.IPromise<IProtocolResponse<IIIntegrationDetails>> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("GET", `/integrationmanager/api/v1/integrations/${id}/details`);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform(errorMessage) } };
        return this.OneProtocol.http(config, messages);
    }

    getSystem(id: string): ng.IPromise<IISystem> {
        return this.getDetails(id).then((response: IProtocolResponse<IIIntegrationDetails>) => {
            if (!response.result) return null;
            return this.intgTemplateService.getTemplateById(response.data.templateId).then((templateResponse: IProtocolResponse<IITemplate>) => {
                const templateDetails: IITemplate = templateResponse.data;
                return this.mapIntegrationTemplate(response.data, templateDetails);
            });
        });
    }

    getCredentials(id: string): ng.IPromise<IProtocolResponse<IICredential[]>> {
        return this.getDetails(id, "CannotRetrieveCredentials").then((response) => {
            const data = response.result && response.data ? response.data.credentials : [];
            return { ...response, data };
        });
    }

    getWorkflows(id: string): ng.IPromise<IProtocolResponse<IIWorkflow[]>> {
        return this.getDetails(id, "CannotRetrieveWorkflows").then((response) => {
            const data = response.result && response.data ? response.data.workflows : [];
            return { ...response, data };
        });
    }

    exportSystem(integrationId: string): Observable<IProtocolResponse<string>> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("GET", `/integrationmanager/api/v1/integrations/${integrationId}/export`);
        const messages: IProtocolMessages = {
            Success: { custom: this.translatePipe.transform("SystemExportedSuccessfully")},
            Error: { custom: this.translatePipe.transform("UnableToExportSystem") },
        };
        return from(this.OneProtocol.http(config, messages));
    }

    getOtIntegrations(page: number = 0, size: number = 100, sort = "name,asc"): ng.IPromise<IProtocolResponse<IPageableOf<IIWorkflow>>> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("GET", `/integrationmanager/api/v1/workflows/otactions`, { page, size, sort });
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("CannotRetrieveWorkflows") } };
        return this.OneProtocol.http(config, messages);
    }

    mapIntegrationTemplatesList(integrations: IIIntegration[], templates: IITemplate[]): IISystem[] {
        const templateMap = keyBy(templates, "id");
        if (!integrations || !integrations.length) return [];
        return map(integrations, (integration): IISystem => {
            const template = templateMap[integration.templateId];
            return this.mapIntegrationTemplate(integration, template);
        });
    }

    private mapIntegrationTemplate(integration: IIIntegration|IIIntegrationDetails, template: IITemplate): IISystem {
        return {
            id: integration.id,
            name: integration.name,
            displayName: integration.name,
            description: template.description,
            seed: template.seed,
            ShowAdvancedView: true,
            isCustomConnector: integration.template as boolean,
            showInMarketplace: true,
            integratesWith: template.integratesWith,
            sourceType: integration.sourceType,
            logo: {
                style: { "background-color": template.bgcolor },
                url: template.icon || "/app/images/intg-systems/icon-connector.png",
            },
        };
    }
}
