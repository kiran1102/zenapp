// Angular
import {
    Injectable,
    Inject,
} from "@angular/core";

// Services
import { ProtocolService } from "modules/core/services/protocol.service";

// Interfaces
import {
    IProtocolConfig,
    IProtocolMessages,
    IProtocolResponse,
} from "interfaces/protocol.interface";
import {
    IIWorkflow,
    IICreateWorkflow,
    IIWorkflowDetail,
    IIWorkflowNode,
    IICreateActions,
    IIAddWorkflowEvent,
    IIIntegration,
    IICreateIntegration,
    IIFreeMarkerTemplate,
    IIWorkflowTestResponse,
    ISeededWorkflowTemplate,
} from "modules/intg/interfaces/integration.interface";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";
import { IPageableOf } from "interfaces/pagination.interface";

// enums
import { WorkflowType } from "modules/intg/enums/workflow.enum";

// Rxjs
import { from, Observable } from "rxjs";

@Injectable()
export class IntgWorkflowService {

    constructor(
        @Inject("Export") readonly Export: any,
        private oneProtocol: ProtocolService,
        private translatePipe: TranslatePipe,
    ) { }

    getWorkflows(page: number = 0, size: number = 100, sort = "name,asc", type = WorkflowType.COMPOSITE): ng.IPromise<IProtocolResponse<IPageableOf<IIWorkflow>>> {
        const config: IProtocolConfig = this.oneProtocol.customConfig("GET", `/integrationmanager/api/v1/workflows`, { page, size, sort, type});
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("CannotRetrieveWorkflows") } };
        return this.oneProtocol.http(config, messages);
    }

    getWorkflowById(id: string, details = false): ng.IPromise<IProtocolResponse<IIWorkflow | IIWorkflowDetail>> {
        const config: IProtocolConfig = this.oneProtocol.customConfig("GET", `/integrationmanager/api/v1/workflows/${id}${details ? "/details" : ""}`);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("CannotRetrieveWorkflowsById") } };
        return this.oneProtocol.http(config, messages);
    }

    getWorkflowActions(id: string): ng.IPromise<IIWorkflowNode[]> {
        const config: IProtocolConfig = this.oneProtocol.customConfig("GET", `/integrationmanager/api/v1/workflows/${id}/details`);
        const messages: IProtocolMessages = { Error: { Hide: true } };
        return this.oneProtocol.http(config, messages).then((response: IProtocolResponse<IIWorkflowDetail>) => {
            const workflow = response.result ? response.data : null;
            return workflow ? workflow.workflowNodes : [];
        });
    }

    saveWorkflow(newWorkflow: IICreateWorkflow): ng.IPromise<IProtocolResponse<IIWorkflow>> {
        const httpVerb =  newWorkflow.id ?  "PUT" : "POST";
        const config: IProtocolConfig = this.oneProtocol.customConfig(httpVerb, `/integrationmanager/api/v1/workflows`, null, newWorkflow);
        const messages: IProtocolMessages = {
            Error: { custom: this.translatePipe.transform(newWorkflow.id ? "CannotUpdateActions" : "CannotCreateActions") },
        };
        return this.oneProtocol.http(config, messages);
    }

    createWorkflow(newWorkflow: IICreateWorkflow): ng.IPromise<IProtocolResponse<IIWorkflowDetail>> {
        const config: IProtocolConfig = this.oneProtocol.customConfig("POST", `/integrationmanager/api/v1/workflows/${newWorkflow.id}/actions`, null, newWorkflow);
        const messages: IProtocolMessages = {
            Error: { custom: this.translatePipe.transform("CannotCreateActions") },
        };
        return this.oneProtocol.http(config, messages);
    }

    createIntegration(newIntegration: IICreateIntegration): ng.IPromise<IProtocolResponse<IIIntegration>> {
            const config: IProtocolConfig = this.oneProtocol.customConfig("POST", `/integrationmanager/api/v1/integrations`, null, newIntegration);
            const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("CannotCreateIntegrations") } };
            return this.oneProtocol.http(config, messages);
    }

    createActions(id: string, actionList: IICreateActions): ng.IPromise<IProtocolResponse<IIWorkflowDetail>> {
        const config: IProtocolConfig = this.oneProtocol.customConfig("POST", `/integrationmanager/api/v1/workflows/${id}/actions`, null, actionList);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("CannotCreateActions") } };
        return this.oneProtocol.http(config, messages);
    }

    saveWorkflowEvent(workflowId: string, workflowEvent: IIAddWorkflowEvent): ng.IPromise<IProtocolResponse<IIWorkflowDetail>> {
        const config: IProtocolConfig = this.oneProtocol.customConfig("POST", `/integrationmanager/api/v1/workflows/${workflowId}/events`, null, workflowEvent);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("CannotCreateActions") } };
        return this.oneProtocol.http(config, messages);
    }

    deleteWorkflowEvent(workflowId: string): ng.IPromise<IProtocolResponse<IIWorkflowDetail>> {
        const config: IProtocolConfig = this.oneProtocol.customConfig("DELETE", `/integrationmanager/api/v1/workflows/${workflowId}/events`);
        const messages: IProtocolMessages = { Error: { Hide: true } };
        return this.oneProtocol.http(config, messages);
    }

    deleteWorkflow(workflowId: string): ng.IPromise<IProtocolResponse<void>> {
        const config: IProtocolConfig = this.oneProtocol.customConfig("DELETE", `/integrationmanager/api/v1/workflows/${workflowId}`);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("CannotDeleteAction") } };
        return this.oneProtocol.http(config, messages);
    }

    testWorkflow(workflowId: string, payLoad: string, eventCode: number): ng.IPromise<IProtocolResponse<IIWorkflowTestResponse>> {
        const config: IProtocolConfig = this.oneProtocol.customConfig("PUT", `/integrationmanager/api/v1/workflows/${workflowId}/run`, { eventCode }, payLoad);
        const messages: IProtocolMessages = {
            Error: { Hide: true },
        };
        return this.oneProtocol.http(config, messages);
    }

    validateFreemarker(payLoad: IIFreeMarkerTemplate): Observable<IProtocolResponse<any>> {
        const config: IProtocolConfig = this.oneProtocol.customConfig("POST", `/integrationmanager/api/v1/workflows/validate`, null, payLoad);
        const messages: IProtocolMessages = {
            Error: { Hide: true },
        };
        return from(this.oneProtocol.http(config, messages));
    }

    exportWorkflow(workflowId: string, fileName: string): ng.IPromise<void> {
        const config: IProtocolConfig = this.oneProtocol.customConfig("GET", `/integrationmanager/api/v1/workflows/${workflowId}/export`);
        const messages: IProtocolMessages = {
            Success: { custom: this.translatePipe.transform("WorkflowExportedSuccessfully")},
            Error: { custom: this.translatePipe.transform("UnableToExportWorkflow") },
        };
        return this.oneProtocol.http(config, messages)
            .then((response: IProtocolResponse<string>): void => {
                if (response.result) {
                    this.Export.basicFileDownload(JSON.stringify(response.data, undefined, 2), fileName);
                }
            });
    }

    public importWorkflow(request: IICreateWorkflow, name: string): Observable<IProtocolResponse<IIWorkflowDetail>> {
        const config: IProtocolConfig = this.oneProtocol.customConfig("POST", `/integrationmanager/api/v1/workflows/import`, {name}, request);
        const messages: IProtocolMessages = {
            Error: { custom: this.translatePipe.transform("UnableToImportWorkflow") },
            Success: { custom: this.translatePipe.transform("WorkflowImportedSuccessfully") },
        };
        return from(this.oneProtocol.http(config, messages));
    }

    public getSeededWorkflowTemplate(system: string): Observable<IProtocolResponse<IPageableOf<ISeededWorkflowTemplate>>> {
        const config: IProtocolConfig = this.oneProtocol.customConfig("GET", `/integrationmanager/api/v1/workflows/seeded`, {system});
        const messages: IProtocolMessages = {
            Error: { Hide: true },
        };
        return from(this.oneProtocol.http(config, messages));
    }

    public createWorkflowFromTemplate(templateId: string, name?: string, credentialsId?: string): Observable<IProtocolResponse<IIWorkflowDetail>> {
        const config: IProtocolConfig = this.oneProtocol.customConfig("POST", `/integrationmanager/api/v1/workflows/${templateId}/import`, { name, credentialsId});
        const messages: IProtocolMessages = {
            Error: { custom: this.translatePipe.transform("CannotCreateWorkflow") },
        };
        return from(this.oneProtocol.http(config, messages));
    }

    public extractWorkflowSchema(responsePayload: string): Observable<IProtocolResponse<unknown>> {
        const config: IProtocolConfig = this.oneProtocol.customConfig("POST", `/integrationmanager/api/v1/workflows/extract`, null, responsePayload);
        const messages: IProtocolMessages = {
            Error: { Hide: true },
        };
        return from(this.oneProtocol.http(config, messages));
    }
}
