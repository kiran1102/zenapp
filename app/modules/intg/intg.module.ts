// Modules
import { NgModule } from "@angular/core";
import { SharedModule } from "sharedModules/shared.module";
import { CommonModule } from "@angular/common";
import { HttpClientModule } from "@angular/common/http";
import { FormsModule } from "@angular/forms";
import { ReactiveFormsModule } from "@angular/forms";
import { VitreusModule } from "@onetrust/vitreus";
import { DndListModule } from "ngx-drag-and-drop-lists";
import { OTPipesModule, PIPES } from "modules/pipes/pipes.module";
import { VirtualScrollModule } from "od-virtualscroll";
import { OneVideoModule } from "modules/video/video.module";
import { TranslateModule } from "@ngx-translate/core";

// Components
import { IsConnectionsTableComponent } from "intsystemcomponent/Connections/List/intg-connections-table.component";
import { IntgMarketplaceModulesPicklistComponent } from "intsystemcomponent/Marketplace/List/intg-marketplace-modules-picklist.component";
import { IntgContactMappingComponent } from "intsystemcomponent/intg-contact-mapping/intg-contact-mapping.component";
import { IntgDataSubjectProfileComponent } from "intsystemcomponent/intg-data-subject-profile/intg-data-subject-profile.components";
import { IntgPreferencesComponent } from "intsystemcomponent/intg-preferences/intg-preferences.component";
import { IntgPreferencesModalComponent } from "intsystemcomponent/intg-preferences/preference-modal/intg-preference-modal.component";
import { IntgPreferencesDeleteModalComponent } from "intsystemcomponent/intg-preferences/preference-delete-modal/intg-preference-delete-modal.component";

import { IntgCustomSystemAddModal } from "modules/intg/components/intg-add-custom-connector-modal/intg-add-custom-connector-modal.component";
import { IntgTable } from "intsystemcomponent/intg-table/intg-table.component";
import { IntgCredentialsComponent } from "intsystemcomponent/intg-credentials/intg-credentials.component";
import { IntgCredentialsAddModal } from "intsystemcomponent/intg-credentials-add-modal/intg-credentials-add-modal.component";
import { IntgSystemActionsComponent } from "intsystemcomponent/intg-system-actions/intg-system-actions.component";
import { IntgSystemActionsAddModal } from "intsystemcomponent/intg-system-actions-add-modal/intg-system-actions-add-modal.component";
import { IntgApiKeyTableComponent } from "intsystemcomponent/Connections/Manage/intg-api-key-table/intg-api-key-table.component";
import { IntgConnectionFormComponent } from "intsystemcomponent/Connections/Manage/intg-connection-form/intg-connection-form.component";
import { IntgConnectionEventsComponent } from "intsystemcomponent/Connections/Manage/intg-connection-events/intg-connection-events.component";
import { IntgConnectionManageFooterComponent } from "intsystemcomponent/Connections/Manage/intg-connection-manage-footer/intg-connection-manage-footer.component";
import { IntgConnectionDetailsAdvancedComponent } from "intsystemcomponent/Connections/Manage/intg-connection-details-advanced/intg-connection-details-advanced.component";
import { IntgConnectionManageSelectComponent } from "intsystemcomponent/Connections/Manage/intg-connection-manage-select-system/intg-connection-manage-select-system.component";
import { IntgConnectionDetailsComponent } from "intsystemcomponent/Connections/Manage/intg-connection-details/intg-connection-details.component";
import { IntgConnectionsListComponent } from "intsystemcomponent/Connections/List/intg-connections-list.component";
import { IntgConnectionManageBodyComponent } from "intsystemcomponent/Connections/Manage/intg-connection-manage-body/intg-connection-manage-body.component";
import { IntgConnectionManageComponent } from "intsystemcomponent/Connections/Manage/intg-connection-manage/intg-connection-manage.component";

import { IntgOneCornerRibbonComponent } from "intsystemcomponent/intg-one-corner-ribbon/intg-one-corner-ribbon.component";
import { IntgIsLogoComponent } from "intsystemcomponent/intg-logo/intg-logo.component";
import { IntgCardComponent } from "intsystemcomponent/intg-card/intg-card.component";
import { IntgSystemLayoutComponent } from "intsystemcomponent/intg-system-layout/intg-system-layout.component";
import { IntgMarketplaceConnectionsListComponent } from "intsystemcomponent/Marketplace/Details/intg-marketplace-connections-list.component";
import { IntgWorkflowBuilderComponent } from "intsystemcomponent/workflows/builder/intg-workflow-builder.component";
import { IntgWorkflowBuilderTriggersComponent } from "intsystemcomponent/workflows/builder/intg-workflow-builder-triggers.component";
import { IntgWorkflowBuilderActionItemComponent } from "intsystemcomponent/workflows/builder/intg-workflow-builder-action-item.component";
import { IntgWorkflowBuilderScheduleItemComponent } from "intsystemcomponent/workflows/builder/intg-workflow-builder-schedule-item.component";
import { IntgWorkflowBuilderWebhookItemComponent } from "intsystemcomponent/workflows/builder/intg-workflow-builder-webhook-item.component";
import { IntgMarketplaceBodyComponent } from "intsystemcomponent/Marketplace/List/intg-marketplace-body.component";
import { IntgWorkflowsListComponent } from "intsystemcomponent/workflows/list/intg-workflows-list.component";
import { IntgMarketplaceDetailsComponent } from "intsystemcomponent/Marketplace/Details/intg-marketplace-details.component";
import { IntgMarketplaceDetailsBodyComponent } from "intsystemcomponent/Marketplace/Details/intg-marketplace-details-body.component";
import { IntgMarketplaceComponent } from "intsystemcomponent/Marketplace/List/intg-marketplace/intg-marketplace.component";
import { IntgWorkFlowCreateModal } from "intsystemcomponent/workflows/addWorkflow/intg-workflow-create-modal.component";
import { IntgWorkflowBuiderTestModal } from "intsystemcomponent/workflows/builder/intg-workflow-builder-test-modal.component";
import { IntgImportModal } from "intsystemcomponent/workflows/import/intg-import-modal.component";
import { IntgWorkflowBuilderLogicActionDelayComponent } from "intsystemcomponent/workflows/builder/logic/intg-workflow-builder-logic-action-delay.component";
import { IntgFreemarkerValidatorComponent } from "intsystemcomponent/workflows/builder/freemarkerValidation/intg-freemarker-validator-modal.component";
import { IntgWorkflowBuilderLogicApplyEachComponent } from "intsystemcomponent/workflows/builder/logic/intg-workflow-builder-logic-apply-each.component";
import { IntgWorkflowParseSchemaModal } from "intsystemcomponent/workflows/builder/intg-workflow-parse-schema-modal.component";
import { IntgDataDiscoveryModalComponent } from "modules/intg/components/workflows/map-merge/intg-map-and-merge-modal.component";
import { IntgSeedWorkFlowCreateModal } from "intsystemcomponent/workflows/seedWorkflow/intg-seedWorkflow-create-modal.component";
import { IntgWorkflowBuilderAPIActionItemComponent } from "intsystemcomponent/workflows/builder/intg-workflow-builder-api-action-item.component";
import { IntgCustomValueListComponent } from "intsystemcomponent/custom-values/list/intg-custom-value-list.component";
import { IntgCustomValuesCreateModalComponent } from "intsystemcomponent/custom-values/intg-customValues-create-modal/intg-custom-values-create-modal.component";
import { IntgCustomValueTableComponent } from "intsystemcomponent/custom-values/list/intg-custom-value-table.component";

// Services
import { IntgEloquaService } from "./services/intg-eloqua.service"; // Eloqua service AngularJS -> ng4 upgrading the service
import { IntgConnectionsHelperService } from "intsystemservice/intg-connections-helper.service";

import { IntgCredentialsService } from "modules/intg/services/intg-credentials.service";
import { IntgEventTypeService } from "modules/intg/services/intg-event-type.service";
import { IntgIntegrationsService } from "modules/intg/services/intg-integrations.service";
import { IntgTemplateService } from "modules/intg/services/intg-template.service";
import { IntgWorkflowService } from "modules/intg/services/intg-workflow.service";
import { IntgConfigService } from "modules/intg/services/configuration/intg-configuration.service";
import { IntgMapAndMergeService } from "modules/intg/services/intg-map-and-merge.service";
import { IntgApiService } from "intsystemservice/intg-api.service";
import { IntgCredentialsHelper } from "intsystemservice/helpers/intg-credentials-helper.service";
import { IntgWorkflowHelper } from "intsystemservice/helpers/intg-workflow-helper.service";
import { IntgCustomValueService } from "intsystemservice/intg-custom-value.service";

// Assets
import "images/intg-systems/jira-white.svg";
import "images/intg-systems/slack.svg";
import "images/intg-systems/salesforce.png";
import "images/intg-systems/rsa.svg";
import "images/intg-systems/okta.png";
import "images/intg-systems/servicenow.svg";
import "images/intg-systems/smtp.svg";
import "images/intg-systems/eloqua.svg";
import "images/intg-systems/ibm-notes.svg";
import "images/intg-systems/icon-api.svg";
import "images/intg-systems/import.svg";
import "images/intg-systems/outlook.svg";
import "images/intg-systems/ping-identity.svg";
import "images/intg-systems/power-bi.svg";
import "images/intg-systems/salesforce-pardot.svg";
import "images/intg-systems/silverpop.svg";
import "images/intg-systems/symantec.svg";
import "images/intg-systems/tableau.svg";
import "images/intg-systems/bmc.svg";
import "images/intg-systems/adfs.svg";
import "images/intg-systems/g-suite.svg";
import "images/intg-systems/onelogin.svg";
import "images/intg-systems/collibra.svg";
import "images/intg-systems/drupal.svg";
import "images/intg-systems/atm.svg";
import "images/intg-systems/joomla.svg";
import "images/intg-systems/squarespace.svg";
import "images/intg-systems/tealium.svg";
import "images/intg-systems/gtm.svg";
import "images/intg-systems/azure.svg";
import "images/intg-systems/wordpress.svg";
import "images/intg-systems/marketo.svg";
import "images/intg-systems/cms.svg";
import "images/intg-systems/amp.svg";
import "images/intg-systems/jira-illustration.svg";
import "images/intg-systems/slack-illustration.svg";
import "images/intg-systems/tealium-illustration.svg";
import "images/intg-systems/reminder-illustration.svg";
import "images/intg-systems/ping-illustration.svg";
import "images/intg-systems/onelogin-illustration.svg";
import "images/intg-systems/okta-illustration.svg";
import "images/intg-systems/import-export-illustration.svg";
import "images/intg-systems/gtm-illustration.svg";
import "images/intg-systems/data-visualization-illustration.svg";
import "images/intg-systems/cms-illustration.svg";
import "images/intg-systems/azure-illustration.svg";
import "images/intg-systems/adfs-illustration.svg";
import "images/intg-systems/adobe-illustration.svg";
import "images/intg-systems/adobe_logo.svg";
import "images/intg-systems/aem.svg";
import "images/intg-systems/salesforce-illustration.svg";
import "images/intg-systems/pardot-illustration.svg";
import "images/intg-systems/marketo-illustration.svg";
import "images/intg-systems/eloqua-illustration.svg";
import "images/intg-systems/icon-connector.png";
import "images/intg-systems/ensighten.png";
import "images/intg-systems/ensighten-illustration.png";
import "images/intg-systems/mp-logo.svg";
import "images/intg-systems/logic-icon.svg";
import "images/intg-systems/schedule-icon.svg";
import "images/intg-systems/BigId.png";
import "images/intg-systems/varonis_Logo.svg";
import "images/intg-systems/ibm-security-logo.svg";
import "images/intg-systems/mailchimp-logo.svg";
import "images/intg-systems/hubspot-logo.svg";
import "images/intg-systems/dataguise-logo.png";

export function getIsPurposeFilterService(injector) {
    return injector.get("ConsentPurposeService");
}

export function getTemplates(injector) {
    return injector.get("Templates");
}

const MODULES = [
    TranslateModule,
    SharedModule,
    CommonModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    VitreusModule,
    DndListModule,
    OTPipesModule,
    VirtualScrollModule,
    OneVideoModule,
];

const PROVIDERS = [
    {
        provide: "ConsentPurposeService",
        deps: ["$injector"],
        useFactory: getIsPurposeFilterService,
    },
    {
        provide: "Templates",
        deps: ["$injector"],
        useFactory: getTemplates,
    },
];

const SERVICES = [
    IntgCredentialsService,
    IntgEventTypeService,
    IntgIntegrationsService,
    IntgTemplateService,
    IntgWorkflowService,
    IntgEloquaService,
    IntgConnectionsHelperService,
    IntgConfigService,
    IntgApiService,
    IntgCredentialsHelper,
    IntgWorkflowHelper,
    IntgMapAndMergeService,
    IntgCustomValueService,
];

const COMPONENTS_TO_BE_DOWNGRADED = [
    IsConnectionsTableComponent,
    IntgMarketplaceModulesPicklistComponent,
    IntgPreferencesModalComponent,
    IntgPreferencesDeleteModalComponent,
    IntgCustomSystemAddModal,
    IntgCredentialsComponent,
    IntgCredentialsAddModal,
    IntgSystemActionsComponent,
    IntgSystemActionsAddModal,
    IntgOneCornerRibbonComponent,
    IntgIsLogoComponent,
    IntgCardComponent,
    IntgSystemLayoutComponent,
    IntgMarketplaceConnectionsListComponent,
    IntgConnectionsListComponent,
    IntgWorkflowsListComponent,
    IntgCustomValueListComponent,
];

const COMPONENTS = [
    IntgTable,
    IntgApiKeyTableComponent,
    IntgConnectionFormComponent,
    IntgConnectionEventsComponent,
    IntgConnectionManageFooterComponent,
    IntgConnectionDetailsAdvancedComponent,
    IntgConnectionManageSelectComponent,
    IntgConnectionDetailsComponent,
    IntgConnectionManageBodyComponent,
    IntgConnectionManageComponent,
    IntgMarketplaceBodyComponent,
    IntgMarketplaceDetailsComponent,
    IntgMarketplaceDetailsBodyComponent,
    IntgContactMappingComponent,
    IntgDataSubjectProfileComponent,
    IntgPreferencesComponent,
    IntgWorkflowBuilderComponent,
    IntgWorkflowBuilderTriggersComponent,
    IntgWorkflowBuilderActionItemComponent,
    IntgWorkflowBuilderScheduleItemComponent,
    IntgWorkflowBuilderWebhookItemComponent,
    IntgMarketplaceComponent,
    IntgWorkFlowCreateModal,
    IntgWorkflowBuiderTestModal,
    IntgImportModal,
    IntgWorkflowBuilderLogicActionDelayComponent,
    IntgFreemarkerValidatorComponent,
    IntgWorkflowBuilderLogicApplyEachComponent,
    IntgWorkflowParseSchemaModal,
    IntgDataDiscoveryModalComponent,
    IntgSeedWorkFlowCreateModal,
    IntgWorkflowBuilderAPIActionItemComponent,
    IntgCustomValueTableComponent,
    IntgCustomValuesCreateModalComponent,
];

@NgModule({
    imports: MODULES,
    providers: [
        ...PROVIDERS,
        ...SERVICES,
    ],
    declarations: [
        ...COMPONENTS,
        ...COMPONENTS_TO_BE_DOWNGRADED,
    ],
    entryComponents: [
        ...COMPONENTS,
        ...COMPONENTS_TO_BE_DOWNGRADED,
    ],
})
export class IntgModule {}
