export interface ITableAction {
    show: boolean;
    text: string;
    autoId: string;
    action: () => void;
}

export interface ITableViewActions {
    add?: ITableAction;
    addFromTable?: ITableAction;
    bulkActions?: ITableAction;
}
