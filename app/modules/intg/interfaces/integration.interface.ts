import {
    IStringMap,
    INameId,
    IKeyValue,
} from "interfaces/generic.interface";
import { IFormField } from "interfaces/form-field.interface";
import { EntityItem } from "crmodel/entity-item";
import {
    MethodType,
    AuthType,
} from "modules/intg/types/intg.type";

import { WorkflowType } from "modules/intg/enums/workflow.enum";
import { IPageableMeta } from "interfaces/pagination.interface";
import { ParameterScope } from "modules/intg/enums/intg-enum";

export interface IISystemConfigData {
    meta: IISystem;
    form: IStringMap<any>;
}

export interface IISystem {
    id: string;
    name: string;
    nameKey?: string;
    displayName?: string;
    descriptionKeys?: string[];
    typeKey?: string;
    publishedByKey?: string;
    publisherLink?: string;
    publishedDt?: string;
    logo?: IISystemLogo;
    templateId?: string;
    showInMarketplace?: boolean;
    isConnectable?: boolean;
    isComingSoon?: boolean;
    permission?: string;
    illustration?: IISystemLogo;
    moduleList?: string[];
    ShowAdvancedView?: boolean;
    isContactOneTrust?: boolean;
    isAddingConnections?: boolean;
    upgradeModuleName?: string;
    description?: string;
    isCustomConnector?: boolean;
    integratesWith?: string;
    seed?: boolean;
    sourceType?: string;
}

export interface IISystemLogo {
    url: string;
    style?: IStringMap<string>;
    logoStyle?: IStringMap<string>;
}

export interface IISystemConfigForm {
    showInbound: boolean;
    showOutbound: boolean;
    outboundFields?: IFormField[];
    nameFormKeys: IINameFormKeys[];
}

export interface IIConnection {
    id?: string;
    integrationSystemId?: string;
    systemInstanceId?: string;
    name?: string;
    orgGroupId?: string;
    createdBy?: string;
    createdDate?: string;
    properties?: IIConnectionProperty[];
    status?: boolean;
    system?: string;
}

export interface IIConnectionProperty {
    pname: string;
    pvalue?: string;
}

export interface IIAPIKey {
    apiKey?: string;
    systemInstanceId?: string;
    id?: string;
    integrationSystemId?: string;
    onPrem?: string | boolean;
    ipWhiteList?: string;
    disabled?: boolean | string;
    endDate?: string;
    orgGroupId?: string;
    orgGroup?: string;
}

export interface IIRoutingKeys {
    sourceIntegrationSystemId: string;
    destinationIntegrationSystemId: string;
    eventId?: string;
    id?: string;
    systemInstanceId?: string;
    routingKey?: string;
    disabled?: boolean | string;
}

export interface IIEvents {
    eventId?: string;
    id?: string;
    name?: string;
    routingKeyId?: string;
    isEnabled?: boolean;
}

export interface IIPermissions {
    jira: boolean;
    slack: boolean;
    generic: boolean;
}

export interface IINameFormKeys {
    subHeaderText: IISubHeaderText[];
    headerText: string;
}

export interface IISubHeaderText {
    name: string;
    link: string;
    linkText: string;
}

export interface IIConnectionTableRow extends IIConnection {
    statusText?: string;
    orgName?: string;
    modifierClass?: string;
}

export interface IIModuleFilters {
    id: string;
    name: string;
}

export interface IInstallApiKeyModalResolve {
    modalTitle?: string;
    cancelButtonText?: string;
    apiKey: string;
    connectionName: string;
}

export interface IIConnectionMappingElement {
    InputId: string;
    OutputId: string;
}

export interface IIContactMappingElement {
    contactId: string;
    dsElementId: string;
}

export interface IICdoMappingElement {
    purposeTopicId: string;
    cdoId: string;
    cdoAttributesId: string;
}

export interface IContactMappingOptions {
    FieldId: string;
    FieldName: string;
}

export interface IContactMappingDisplay {
    contactField?: INameId<string>;
    dsElementField?: IContactMappingOptions;
}

export interface IICdoMappingDisplay {
    purposeField?: IIPurposeTopicsSelected;
    cdoAttributesField?: IIModuleFilters;
    cdoField?: EntityItem;
}

export interface IISubjectProfileMappingDisplay {
    purpose?: EntityItem;
    contactMappings?: IContactMappingDisplay[];
}

export interface IIPreferenceMappingDisplay {
    purposeId?: EntityItem;
    cdoId?: EntityItem;
    topicAttributes: IIPreferenceCdoPurposeMap[];
}

export interface IIPreferenceCdoPurposeMapDisplay {
    purposeTopicId: IIModuleFilters;
    cdoAttributesId: IIPurposeTopicsSelected;
}

export interface IISubjectProfileMappingElement {
    purposeId: string;
    contactId: string;
    dsElementId: string;
}

export interface IISubjectProfileMapping {
    purposeId: string;
    contactMappings: IIContactMappingElement[];
}

export interface IIDeleteIndex {
    purposeIndex: number;
    contactIndex: number;
}

export interface IIPreferenceDelete {
    purposeIndex: number;
    cdoIndex: number;
}

export interface IIPurposeTopicsSelected {
    Id: string;
    Name: string;
    IntegrationKey: string;
    CanDelete: boolean;
}

export interface IIPreferenceCdoPurposeMap {
    purposeTopicId: string;
    cdoAttributesId: string;
}

export interface IIPreferencesubArrayValues {
    purposeTopicId: IIPurposeTopicsSelected;
    cdoAttributesId: IIModuleFilters;
}

export interface IIPreferencesId {
    purposeId: string;
    cdoId: string;
    topicAttributes?: IIPreferenceCdoPurposeMap[];
}

export interface IIntgPreferenecePurpose {
    purposeId: EntityItem;
    cdoId: EntityItem;
}

export interface IIntgCreateModal {
    editModalValue: boolean;
    handler: (IIPreferencesId) => void;
    editMappingValues?: IIPreferencesId[];
}

export interface IIntgEditModal {
    editMappingValues?: IIPreferencesId[];
    editModalValue: boolean;
    selectedRowValue?: IIntgPreferenecePurpose;
    editHandler?: (IIPreferencesId) => void;
    handler?: (IIPreferencesId) => void;
    deleteHandler?: () => void;
}

export interface IPreferenceMapDataValue {
    sortKey: number;
    columnType: string;
    columnName: string;
    columnWidth: number;
    cellValueKey: string;
    labelKey?: string;
}

export interface IIntgSystemAction {
    id: string;
    name: string;
    description: string;
    endpointPath: string;
    optionalHeader?: Array<IKeyValue<string>>;
    credential: string;
    event: string;
    method: MethodType;
    body: string;
}

export interface IIntgCustomSystemModal {
    module: string;
    callback?: (result: boolean) => void;
}

export interface IIAuthContext {
    BasicAuth: IIBasicAuthContext;
    BearerTokenAuth: IIBearerAuthContext;
    NoAuth: IIBaseAuthContext;
    OAuth2TokenAuth: IIOAuth2TokenContext;
}

export interface IIBaseAuthContext {
    headers?: IStringMap<string>;
    type: AuthType;
    hostName?: string;
}

export interface IIBasicAuthContext extends IIBaseAuthContext {
    userName?: string;
    password?: string;
}

export interface IIBearerAuthContext extends IIBaseAuthContext {
    token: string;
}

export interface IIOAuth2TokenContext extends IIBaseAuthContext {
    grantType: string;
    authUrl?: string;
    tokenUrl: string;
    clientId: string;
    clientSecret?: string;
    username?: string;
    password?: string;
    accessToken: string;
    refreshToken?: string;
    scope?: string;
    authParameterType?: string;
}

export interface IIOAuth2PasswordCredentialBody {
    grant_type: string;
    username: string;
    password: string;
    client_id: string;
    client_secret: string;
}

export interface IIOAuth2ResponseBody {
    access_token: string;
    refresh_token?: string;
    id?: string;
    instance_url?: string;
    issued_at?: string;
    signature?: string;
    token_type?: string;
}

export interface IIOAuth2TokenResponse {
    body?: string;
    requestUri: string;
    headers?: IStringMap<string>;
    httpStatus?: number;
}

export interface IIOAuth2RequestBody {
    grant_type: string;
    client_id: string;
    client_secret: string;
    username?: string;
    password?: string;
    refresh_token?: string;
    scope?: string;
    response_type?: string;
}

// INTG Templates
export interface IITemplate {
    name: string;
    description: string;
    icon: string;
    id: string;
    template: boolean;
    bgcolor?: string;
    integratesWith?: string;
    seed?: boolean;
}

export interface IICreateCustomTemplate {
    name: string;
    description: string;
    icon?: string;
}

export interface IIUpdateCustomTemplate {
    id: string;
    name: string;
    description: string;
    icon?: string;
    isSeed?: boolean;
}

// INTG Credentials
export interface IICredential {
    id: string;
    integrationId: string;
    name: string;
    authType: string;
    authContext: Partial<IIAuthContext>;
}

export interface IICreateCredential {
    id?: string;
    integrationId?: string;
    name: string;
    description?: string;
    authType: AuthType;
    authContext: Partial<IIAuthContext>;
}

export interface IIDeleteCredential {
    id: string;
}

// INTG Integrations
export interface IIIntegration {
    id: string;
    name: string;
    templateId: string;
    template: boolean | IITemplate;
    sourceType?: string;
}

export interface IIIntegrationDetails {
    id: string;
    name: string;
    templateId: string;
    template?: IITemplate;
    credentials?: IICredential[];
    workflows?: IIWorkflow[];
    referenceIntegrationId?: string;
    sourceType?: string;
}

export interface IICreateIntegration {
    name: string;
    templateId: string;
    template: boolean;
    referenceIntegrationId?: string;
    referenceIntegrationName?: string;
}

// INTG Workflows
export interface IIWorkflow {
    id: string;
    integrationId: string;
    name: string;
    description: string;
    enabled: boolean;
    halted?: boolean;
    workflowType: WorkflowType;
    rawDsl?: string;
    integrationName?: string;
    referenceIntegrationId?: string;
    createdDate?: number;
    route?: IStringMap<string>;
    index?: number;
}

export interface IICreateWorkflow {
    id?: string;
    integrationId: string;
    name: string;
    description: string;
    enabled: boolean;
    workflowType: WorkflowType;
    action?: IICreateActions[];
    trigger?: IITrigger;
}

export interface IIWorkflowDetail {
    id: string;
    integrationId: string;
    name: string;
    description: string;
    enabled: boolean;
    halted?: boolean;
    workflowType: WorkflowType;
    rawDsl: string;
    integration: IIIntegration;
    workflowNodes: IIWorkflowNode[];
    workflowEventMappings: IIWorkflowEventMapping[];
    workflowEdges: IIWorkflowEdge[];
    trigger?: IITrigger;
    sourceType?: string;
}

export interface IIWorkflowNode {
    id?: string;
    credentialsId?: string;
    workflowId: string;
    processType: string;
    processContext: Partial<IIProcessContext>;
    nodeLabel: string;
    requestSchema?: Partial<IRequestSchema>;
    responseSchema?: Partial<IRequestSchema>;
    nodeDescription?: string;
}

export interface IRequestSchema {
    sample: string;
    lookupFields?: IIEventField;
    id?: string;
    description?: string;
    integrationId?: string;
    name?: string;
    schema?: string;
}

export interface IIWorkflowEventMapping {
    id: string;
    eventId: string;
    workflowId: string;
}

export interface IIWorkflowEdge {
    id: string;
    workflowId: string;
    endNodeId: string;
    startNodeId: string;
    edgeLabel: string;
}

export interface IIWorkflowTestResponseBody {
    headers: IStringMap<string>;
    body: string;
    statusCode: string;
    statusCodeValue: number;
}
export interface IIWorkflowTestResponse {
    httpStatus: number;
    headers?: IStringMap<string>;
    body: IIWorkflowTestResponseBody;
    traceLogs: string;
}

// INTG Events
export interface IIEvent {
    name: string;
    code: string;
    module?: string;
    eventSample: string;
    eventSchema: string;
    eventFields?: IIEventField;
}

export interface IIEventField {
    fields: IIField[];
}

export interface IIField {
    name: string;
    formattedKey?: string;
    key: string;
    body: string;
    example?: string;
    type?: string;
    jsonPath?: string;
}

export interface IISeededEvent extends IIEvent {
    id: string;
}

// INTG Actions
export interface IICreateActions extends IIWorkflow {
    id: string;
    actions: IIWorkflowNode[];
    directions: IIDirection[];
    trigger?: IITrigger;
}

export interface IIDirection {
    startNode: string;
    endNode: string;
}

// INTG Workflow Event Mapping
export interface IIAddWorkflowEvent {
    eventId: string;
    id?: string;
}
export interface IIProcessContext {
    DataTransformer: IIDataTransformer;
    HttpConnector: IIHttpConnector;
    MessageLog: IMessageLog;
    ApplyEach: IApplyEach;
}

export interface IIDataTransformer {
    type: string;
    template: string;
}

export interface IMessageLog {
    type: string;
}

export interface IApplyEach {
    type: string;
    jsonPath: string;
    listKey?: string;
}

export interface IIHttpConnector {
    type: string;
    headers: IStringMap<string>;
    requestURI: string;
    httpMethod: string;
    isInternal?: boolean;
}

export interface IIConfigIpCidr {
    address: string;
    type: string;
    inbound: boolean;
}
export interface IITestConnectionResponse {
    response: string;
    // TODO : Remove tid once backend is synced to return transactionId always.
    tid?: string;
    transactionId?: string;
}

export interface IIPage<T> {
    content: T[];
    page: IPageableMeta;
}

export interface IIWorkflowModalInputData {
    integrationId: string;
    actionData: IIWorkflow;
    workflowData: IIWorkflowNode[];
}

export interface IISelectedAction {
    isActionSave?: boolean;
    credentialId?: string;
    requestURI?: string;
    requestBody?: string;
    actionMethod?: string;
    headers?: IStringMap<string>;
    eventId?: string;
    httpNode?: IIWorkflowNode;
    applyEachNode?: IIWorkflowNode;
    transformNode?: IIWorkflowNode;
    isOtAction?: boolean;
    addingNewAction?: boolean;
    savedHttpNodeLabel?: string;
    savedTransformNodeLabel?: string;
    isNewNode?: boolean;
}

export interface ITestWorkflow {
    workflowId: string;
    payload: string;
    event: IISeededEvent;
    trigger: IITrigger;
}

export interface IITrigger {
    workflowId: string;
    name: string;
    description: string;
    eventCode: string;
    triggerType: string;
    triggerContext: Partial<IITriggerContext>;
    lookupFields?: IIEventField;
    sample?: string;
    id?: string;
    schema?: string;
}

export interface IITriggerContext {
    EventTrigger: {
        eventCode: string,
        type: string,
    };
    WebHookTrigger: {
        eventCode: string,
        type: string,
        referenceUrl: string,
    };
    ScheduledTrigger: {
        cronExpression: string;
        dateTime: IIScheduleFrontendSchema;
        type: string,
    };
}

export interface IIWebhookStatus {
    fieldsIncomplete: boolean;
    htmlHidden: boolean;
}
export interface IIScheduleFrontendSchema {
    seconds: string;
    minutes: string;
    hours: IIScheduleItemObject;
    dayOfMonth: string;
    month: string;
    dayOfWeek: IIScheduleItemDayObject;
    year: string;
    frequency: IIScheduleItemObject;
    frequencyIntervalTracker: string;
}

export interface IIScheduleItemObject {
    label: string;
    value: string;
    cronExp?: string;
}

export interface IIScheduleItemDayObject {
    label: string;
    value: string;
    cronExp: string;
}

export interface IIFreeMarkerTemplate {
    event?: string;
    template?: string;
    step?: any;
}

export interface IIFreemarkerInputModal {
    event: IISeededEvent;
    addedActions: IISelectedAction[];
    currentActionIndex: number;
    template: string;
}

export interface ITargetDataDiscoveryObject {
    titleKey: string;
    textKey: string;
    url: string;
    templateId: string;
    style?: IStringMap<string>;
}

export interface IDataDiscoveryItem {
    assetName: string;
    externalId: string;
    id: string;
    status: string;
    workflowId: string;
    workflowName: string;
    action?: string;
    onetrustId?: string;
    overRideName?: string;
    value?: string;
}

export interface ISeededWorkflowTemplate {
    description: string;
    eventCode: string;
    id: string;
    integrationName: string;
    name: string;
    otModule: string;
    workflowTriggerName: string;
}

// Map & Merge Modal Interfaces
export interface ICustomDataDiscoveryInput {
    data: string;
}

export interface ICustomDataDiscoveryOutput {
    data: string;
}

export interface IDataDiscoveryOption {
    assetName?: string;
    label?: string;
    value?: string;
}

export interface IAssetItem {
    assetList?: Array<IDataDiscoveryItem|IDataDiscoveryOption[]>;
    assetName?: string;
    dropdown?: boolean;
}

export interface IDataDiscoveryResponseItem {
    success?: boolean;
    preferenceId?: string;
    message?: string;
}

export interface ICustomValuesResponse {
    description?: string;
    id?: string;
    integrationId: string;
    integrationName?: string;
    name: string;
    parameterScope: ParameterScope | string;
    parameterType: string;
    value: any;
    workflowId: string;
    workflowName?: string;
    lookupExpression?: string;
}

export interface ICustomValueLookupFieldList {
    paramNames: ICustomValueLookupField[];
}

export interface ICustomValueLookupField {
    name: string;
    parameterScope: string;
}

export interface IAvailableEvent {
    category: string;
    isOtEvent: boolean;
    fields: IIField[];
    isCustom?: boolean;
}
