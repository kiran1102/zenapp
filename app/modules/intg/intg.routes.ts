import { Permissions } from "modules/shared/services/helper/permissions.service";

export function routes($stateProvider: ng.ui.IStateProvider) {
    $stateProvider
        .state("zen.app.pia.module.intg.marketplace", {
            abstract: true,
            url: "/marketplace",
            template: `<ui-view></ui-view>`,
            resolve: {
                ISMarketplacePermission: [
                    "ISPermission",
                    (ISPermission: boolean): boolean => {
                        return ISPermission;
                    },
                ],
            },
        })
        .state("zen.app.pia.module.intg.marketplace.list", {
            url: "",
            params: { time: null },
            template: `<intg-marketplace  [list-to-show]="'MarketPlaceListView'"></intg-marketplace>`,
            resolve: {
                ISMarketplaceListPermission: [
                    "ISMarketplacePermission",
                    (ISMarketplacePermission: boolean): boolean => {
                        return ISMarketplacePermission;
                    },
                ],
            },
        })
        .state("zen.app.pia.module.intg.systems", {
            abstract: true,
            url: "/systems",
            template: `<ui-view></ui-view>`,
            resolve: {
                ISSeededSystemPermission: [
                    "ISPermission",
                    (ISPermission: boolean): boolean => {
                        return ISPermission;
                    },
                ],
            },
        })
        .state("zen.app.pia.module.intg.systems.list", {
            url: "",
            params: { component: null },
            template: `<intg-marketplace  [list-to-show]="'SystemListView'"></intg-marketplace>`,
            resolve: {
                ISSeededSystemListPermission: [
                    "ISSeededSystemPermission",
                    (ISSeededSystemPermission: boolean): boolean => {
                        return ISSeededSystemPermission;
                    },
                ],
            },
        })
        .state("zen.app.pia.module.intg.apikey", {
            abstract: true,
            url: "/apikey",
            template: `<ui-view></ui-view>`,
            resolve: {
                ISAPIKeyPermission: [
                    "ISPermission",
                    (ISPermission: boolean): boolean => {
                        return ISPermission;
                    },
                ],
            },
        })
        .state("zen.app.pia.module.intg.apikey.list", {
            url: "",
            params: { time: null },
            template: `<downgrade-intg-connections-list  [list-to-show]="'APIKeysListView'"></downgrade-intg-connections-list>`,
            resolve: {
                ISAPIKeysListPermission: [
                    "ISAPIKeyPermission",
                    (ISAPIKeyPermission: boolean): boolean => {
                        return ISAPIKeyPermission;
                    },
                ],
            },
        })
        .state("zen.app.pia.module.intg.marketplace.details", {
            url: "/:system",
            params: { time: null },
            template: `<intg-marketplace-details [page-to-show]="'MarketPlaceDetailsView'"></intg-marketplace-details>`,
            resolve: {
                ISMarketplaceSystemPermission: [
                    "ISMarketplacePermission",
                    (ISMarketplacePermission: boolean): boolean => {
                        return ISMarketplacePermission;
                    },
                ],
            },
        })
        .state("zen.app.pia.module.intg.systems.details", {
            url: "/:system",
            params: { time: null },
            template: `<intg-marketplace-details [page-to-show]="'SystemDetailsView'"></intg-marketplace-details>`,
            resolve: {
                ISMarketplaceSystemPermission: [
                    "ISSeededSystemPermission",
                    (ISSeededSystemPermission: boolean): boolean => {
                        return ISSeededSystemPermission;
                    },
                ],
            },
        })
        .state("zen.app.pia.module.intg.connections", {
            abstract: true,
            url: "/connections",
            template: `<ui-view></ui-view>`,
            resolve: {
                ISConnectionsPermission: [
                    "ISPermission",
                    (ISPermission: boolean): boolean => {
                        return ISPermission;
                    },
                ],
            },
        })
        .state("zen.app.pia.module.intg.connections.list", {
            url: "",
            params: { time: null },
            template: `<downgrade-intg-connections-list [list-to-show]="'ConnectionListView'"></downgrade-intg-connections-list>`,
            resolve: {
                ISConnectionsListPermission: [
                    "ISConnectionsPermission",
                    (ISConnectionsPermission: boolean): boolean => {
                        return ISConnectionsPermission;
                    },
                ],
            },
        })
        .state("zen.app.pia.module.intg.workflows", {
            abstract: true,
            url: "/workflows",
            template: `<ui-view></ui-view>`,
            resolve: {
                ISConnectionsPermission: [
                    "ISPermission",
                    (ISPermission: boolean): boolean => {
                        return ISPermission;
                    },
                ],
            },
        })
        .state("zen.app.pia.module.intg.workflows.list", {
            url: "",
            params: { time: null },
            template: `<intg-workflows-list><intg-workflows-list>`,
            resolve: {
                ISConnectionsListPermission: [
                    "ISConnectionsPermission", "Permissions",
                    (ISConnectionsPermission: boolean,  permissions: Permissions): boolean => {
                        if (ISConnectionsPermission && permissions.canShow("IntegrationWorkflowListView")) {
                            return true;
                        }
                        permissions.goToFallback();
                    },
                ],
            },
        })
        .state("zen.app.pia.module.intg.values", {
            abstract: true,
            url: "/values",
            template: `<ui-view></ui-view>`,
            resolve: {
                ISConnectionsPermission: [
                    "ISPermission",
                    (ISPermission: boolean): boolean => {
                        return ISPermission;
                    },
                ],
            },
        })
        .state("zen.app.pia.module.intg.values.list", {
            url: "",
            params: { time: null },
            template: `<intg-custom-values-list></intg-custom-values-list>`,
            resolve: {
                ISConnectionsListPermission: [
                    "ISConnectionsPermission", "Permissions",
                    (ISConnectionsPermission: boolean,  permissions: Permissions): boolean => {
                        if (ISConnectionsPermission && permissions.canShow("IntegrationsCustomValues")) {
                            return true;
                        }
                        permissions.goToFallback();
                    },
                ],
            },
        })
        .state("zen.app.pia.module.intg.credentials", {
            abstract: true,
            url: "/credentials",
            template: `<ui-view></ui-view>`,
            resolve: {
                ISCredentialsPermission: [
                    "ISPermission",
                    (ISPermission: boolean): boolean => {
                        return ISPermission;
                    },
                ],
            },
        })
        .state("zen.app.pia.module.intg.credentials.list", {
            url: "",
            params: { time: null },
            template: `<intg-credentials><intg-credentials>`,
            resolve: {
                ISConnectionsListPermission: [
                    "ISCredentialsPermission", "Permissions",
                    (ISCredentialsPermission: boolean,  permissions: Permissions): boolean => {
                        if (ISCredentialsPermission && permissions.canShow("IntegrationCredentialListView")) {
                            return true;
                        }
                        permissions.goToFallback();
                    },
                ],
            },
        })
        .state("zen.app.pia.module.intg.workflows.builder", {
            url: "/:id",
            params: {
                time: null,
                id: null,
            },
            template: `<intg-workflow-builder><intg-workflow-builder>`,
            resolve: {
                ISConnectionsListPermission: [
                    "ISConnectionsPermission", "Permissions",
                    (ISConnectionsPermission: boolean,  permissions: Permissions): boolean => {
                        if (ISConnectionsPermission && permissions.canShow("IntegrationWorkflowListView")) {
                            return true;
                        }
                        permissions.goToFallback();
                    },
                ],
            },
        })
        .state("zen.app.pia.module.intg.connections.manage", {
            url: "/manage?:id&:system",
            params: { time: null },
            template: `<intg-connection-manage></intg-connection-manage>`,
            resolve: {
                ISConnectionsManagePermission: [
                    "ISConnectionsPermission",
                    (ISConnectionsPermission: boolean): boolean => {
                        return ISConnectionsPermission;
                    },
                ],
            },
        })
    ;
}

routes.$inject = ["$stateProvider"];
