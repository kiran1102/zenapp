import { Component, Input } from "@angular/core";

@Component({
    selector: "intg-logo",
    templateUrl: "./intg-logo.component.html",
})

export class IntgIsLogoComponent {
    @Input() logoUrl: string;
    @Input() logoStyle?: string;
    @Input() ribbonText?: string;
    @Input() ribbonPosition?: string;
    @Input() logoSize?: string;
}
