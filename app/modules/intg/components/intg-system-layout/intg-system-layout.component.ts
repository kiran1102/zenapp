import {
    Component,
    Input,
} from "@angular/core";

// Constants
import { ModuleFilters } from "modules/intg/constants/integration.constant";

@Component({
    selector: "intg-system-layout",
    templateUrl: "./intg-system-layout.component.html",
})

export class IntgSystemLayoutComponent {
    @Input() system;
    @Input() hideBanner: boolean;
    ModuleFilters = ModuleFilters;
}
