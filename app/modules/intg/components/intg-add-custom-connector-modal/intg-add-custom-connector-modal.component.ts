import {
    Component,
    OnInit,
    Inject,
} from "@angular/core";

// Services
import { ModalService } from "sharedServices/modal.service";
import { IntgTemplateService } from "modules/intg/services/intg-template.service";
import { IntgIntegrationsService } from "modules/intg/services/intg-integrations.service";

// Interfaces
import { IStore } from "interfaces/redux.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";
import {
    IIIntegration,
    IICreateCustomTemplate,
    IIntgCustomSystemModal,
} from "modules/intg/interfaces/integration.interface";

// Redux
import { StoreToken } from "tokens/redux-store.token";
import { getModalData } from "oneRedux/reducers/modal.reducer";

@Component({
    selector: "intg-custom-system-add-modal",
    templateUrl: "./intg-add-custom-connector-modal.component.html",
})

export class IntgCustomSystemAddModal implements OnInit {

    public customConnectorModel: IICreateCustomTemplate;
    public currentIconUrl: string;
    public isLoading = true;
    public isImageConverting = false;
    public canAdd: boolean;
    public saving: boolean;
    public maxFileUploadSizeInBytes = 3145728; // 3 MB limit
    public isCurrentFileSizeAccepted: boolean;
    public filesFromInput: File[] = [];
    public filesFromTooBig: File[] = [];
    public invalidFilesFromDrag: File[] = [];
    private modalData: IIntgCustomSystemModal;

    constructor(
        @Inject(StoreToken) private readonly store: IStore,
        private readonly modalService: ModalService,
        private readonly intgIntegrationsService: IntgIntegrationsService,
    ) { }

    ngOnInit() {
        this.isCurrentFileSizeAccepted = true;
        this.customConnectorModel = {} as IICreateCustomTemplate;
        this.modalData = getModalData(this.store.getState());
    }

    clearFiles() {
        this.invalidFilesFromDrag = [];
        this.filesFromInput = [];
        this.filesFromTooBig = [];
        this.canAdd = Boolean(this.customConnectorModel.name && this.customConnectorModel.description);
    }

    public closeModal(result = false) {
        if (this.modalData.callback) {
            this.modalData.callback(result);
        }
        this.modalService.closeModal();
        this.modalService.clearModalData();
    }

    public handleFile(files: File[]) {
        if (Array.isArray(files)) {
            this.clearFiles();
            this.generateBase64UrlFromUpload(files[0]);
        }
    }

    public setProperty(prop: keyof IICreateCustomTemplate, propValue: string) {
        this.customConnectorModel[prop] = propValue;
        this.isConnectorReadyToAdd();
    }

    public addCustomConnector() {
        this.canAdd = false;
        this.saving = true;
        const customConnector: IICreateCustomTemplate = {
            name: this.customConnectorModel.name,
            description: this.customConnectorModel.description,
            icon: this.currentIconUrl,
        };
        this.intgIntegrationsService.createIntegration(customConnector, false)
            .then((integration: IProtocolResponse<IIIntegration>) => {
                if (integration.result) {
                    this.closeModal(true);
                } else {
                    this.isConnectorReadyToAdd();
                }
                this.saving = false;
            });
    }

    private isValidFileType(imgFile: any): boolean {
        const currentFileExtension = imgFile.name.split(".").pop();
        const exceptedFileExtensions = ["png", "jpeg", "jpg"];
        return exceptedFileExtensions.indexOf(currentFileExtension.toLowerCase()) > -1;
    }

    private isConnectorReadyToAdd() {
        this.canAdd = this.customConnectorModel.name && this.customConnectorModel.description && this.invalidFilesFromDrag.length === 0 && this.isCurrentFileSizeAccepted && !this.isImageConverting;
    }

    private generateBase64UrlFromUpload(imgFile: any) {
        this.isCurrentFileSizeAccepted = true;
        if (imgFile) {
            this.isImageConverting = true;
            this.canAdd = false;
            if (this.isValidFileType(imgFile)) {
                const reader: FileReader = new FileReader();
                reader.onloadend = (e: any) => {
                    if (imgFile.size > this.maxFileUploadSizeInBytes) {
                        this.isCurrentFileSizeAccepted = false;
                        this.filesFromTooBig.push(imgFile);
                    } else {
                        this.filesFromInput.push(imgFile);
                        this.currentIconUrl = e.target.result;
                    }
                    this.isImageConverting = false;
                    this.isConnectorReadyToAdd();
                };
                reader.readAsDataURL(imgFile);
            } else {
                this.invalidFilesFromDrag.push(imgFile);
                this.isImageConverting = false;
                this.isConnectorReadyToAdd();
            }
        }
    }

}
