// Angular
import {
    Component,
    Inject,
    OnInit,
} from "@angular/core";
import { StateService } from "@uirouter/core";
import { IPromise } from "angular";

// 3rd party
import { sortBy } from "lodash";

// Services
import { Permissions } from "modules/shared/services/helper/permissions.service";
import { ModalService } from "sharedServices/modal.service";
import { IntgIntegrationsService } from "modules/intg/services/intg-integrations.service";
import { IntgCredentialsService } from "intsystemservice/intg-credentials.service";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

// Interface
import { IProtocolResponse } from "interfaces/protocol.interface";
import { IPageableOf } from "interfaces/pagination.interface";
import { ITableColumnConfig } from "interfaces/table-config.interface";
import { IDropdownOption } from "interfaces/one-dropdown.interface";
import { ITableViewActions } from "modules/intg/interfaces/intg-table.interface";
import { IICredential } from "modules/intg/interfaces/integration.interface";
import { IDeleteConfirmationModalResolve } from "interfaces/delete-confirmation-modal-resolve.interface";

// Enums
import { TableColumnTypes } from "enums/data-table.enum";

@Component({
    selector: "intg-credentials",
    templateUrl: "./intg-credentials.component.html",
})
export class IntgCredentialsComponent implements OnInit {
    public options: IDropdownOption[];
    ready = false;
    page: IPageableOf<IICredential> = null;
    systemId = this.stateService.params.system;
    readonly columns: ITableColumnConfig[] = [
        {
            name: this.translatePipe.transform("CredentialName"),
            sortKey: 1,
            type: TableColumnTypes.Text,
            cellValueKey: "name",
        },
        {
            name: this.translatePipe.transform("AuthenticationType"),
            sortKey: 2,
            type: TableColumnTypes.Text,
            cellValueKey: "authType",
        },
        {
            name: "",
            type: TableColumnTypes.Action,
            cellValueKey: "",
        },
    ];
    readonly viewPermissions = {
        view: this.permissions.canShow("IntegrationCredentials"),
        add: this.permissions.canShow("IntegrationCredentialsAdd"),
        edit: this.permissions.canShow("IntegrationsCredentialsEdit"),
        delete: this.permissions.canShow("IntegrationsCredentialsDelete"),
        addFromTable: false,
    };
    readonly viewActions: ITableViewActions = {
        add: this.systemId ? {
            show: this.viewPermissions.add,
            text: "AddCredentials",
            action: () => this.addCredentials(),
            autoId: "IntegrationCredentialsAddBtn",
        } : undefined,
        addFromTable: {
            show: this.viewPermissions.add && this.viewPermissions.addFromTable,
            text: "AddCredentials",
            action: () => this.addCredentials(),
            autoId: "IntegrationCredentialsAddFromTableBtn",
        },
    };

    constructor(
        private stateService: StateService,
        private permissions: Permissions,
        private modalService: ModalService,
        private translatePipe: TranslatePipe,
        private intgCredentialsService: IntgCredentialsService,
    ) {}

    ngOnInit() {
        this.getCredentialsPage();
        this.options = this.getActions();
    }

    handlePageChanged(page: number) {
        if (this.page.totalPages) {
            this.getCredentialsPage(page, 20);
        }
    }

    getCredentialsPage(page = 0, size = 20) {
        this.ready = false;
        return this.intgCredentialsService.getCredentials(page, size).then(
            (response: IProtocolResponse<IPageableOf<IICredential>>) => {
                const content = response.result ? response.data.content.sort((a, b) => a.name.localeCompare(b.name)) : [];
                this.page = {
                    first: true,
                    last: false,
                    number: response.data.number,
                    numberOfElements: response.data.numberOfElements,
                    size: response.data.size,
                    totalElements: response.data.totalElements,
                    totalPages: response.data.totalPages,
                    content,
                };
                this.ready = true;
            },
        );
    }

    getActions = () => {
            const actions: IDropdownOption[] = [];
            if (this.viewPermissions.edit) {
                actions.push({
                    text: this.translatePipe.transform("Edit"),
                    action: (row): void => {
                        this.editCredential(row);
                    },
                });
            }
            if (this.viewPermissions.delete) {
                actions.push({
                    text: this.translatePipe.transform("Delete"),
                    action: (row): void => {
                        this.deleteCredential(row);
                    },
                });
            }
            if (!actions.length) {
                actions.push({
                    text: this.translatePipe.transform("NoActionsAvailable"),
                    action: (): void => {}, // Keep Empty
                });
            }
            return actions;
    }

    addCredentials() {
        const modalData = {
            integrationId: this.systemId,
            callback: (result: boolean) => {
                if (result) {
                    this.getCredentialsPage();
                }
            },
        };
        this.modalService.setModalData(modalData);
        this.modalService.openModal("intgCredentialsAddModal");
    }

    editCredential(credential) {
        const modalData = {
            integrationId: this.systemId,
            credential,
            callback: (result: boolean) => {
                if (result) {
                    this.getCredentialsPage();
                }
            },
        };
        this.modalService.setModalData(modalData);
        this.modalService.openModal("intgCredentialsAddModal");
    }
    deleteCredential(credential) {
        const deleteModalData: IDeleteConfirmationModalResolve = {
            promiseToResolve: (): IPromise<void> => {
                return this.intgCredentialsService.deleteCredential(credential.id)
                    .then((response: IProtocolResponse<IICredential>) => {
                        if (response.result) {
                            this.getCredentialsPage();
                            this.modalService.closeModal();
                            this.modalService.clearModalData();
                        }
                    });
            },
            modalTitle: this.translatePipe.transform("DeleteCredential"),
            confirmationText: this.translatePipe.transform("AreYouSureDeleteCredential"),
            submitButtonText: this.translatePipe.transform("Delete"),
            cancelButtonText: this.translatePipe.transform("Cancel"),
        };
        this.modalService.setModalData(deleteModalData);
        this.modalService.openModal("otDeleteConfirmationModal");
    }
}
