import { Component, Input } from "@angular/core";

@Component({
    selector: "intg-one-corner-ribbon",
    templateUrl: "./intg-one-corner-ribbon.component.html",
})

export class IntgOneCornerRibbonComponent {
    @Input() ribbonText: string;
    @Input() ribbonClass?: string;
    @Input() ribbonPosition?: string;
}
