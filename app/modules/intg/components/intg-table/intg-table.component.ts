// Angular
import {
    Component,
    Input,
    Output,
    EventEmitter,
} from "@angular/core";
import { StateService } from "@uirouter/core";

// Interface
import { IPageableOf } from "interfaces/pagination.interface";
import { ITableColumnConfig } from "interfaces/table-config.interface";
import { IDropdownOption } from "interfaces/one-dropdown.interface";
import { ITableViewActions } from "modules/intg/interfaces/intg-table.interface";

// Enums
import { TableColumnTypes } from "enums/data-table.enum";
import { TableRowType } from "modules/intg/enums/intg-table.enum";

@Component({
    selector: "intg-table",
    templateUrl: "./intg-table.component.html",
    host: { class: "stretch-vertical column-nowrap" },
})
export class IntgTable {
    @Input() ready = false;
    @Input("page") set page(page) {
        if (page && page.content) {
            this.pageData = [];
            page.content.forEach((pageContent, index: number) => {
                this.pageData.push({...pageContent, index});
            });
        }
    }
    @Input() emptyStateMessage;
    @Input() columns: ITableColumnConfig[] = [];
    @Input() viewActions: ITableViewActions;
    @Input() options: IDropdownOption[];
    @Input() sortable = false;
    @Input() sortKey = "";
    @Input() sortOrder = "";

    @Output() onSort = new EventEmitter();
    @Output() onPageChange = new EventEmitter();
    @Output() clickAction = new EventEmitter();
    pageData: any = [];

    allRowsSelected = false;

    readonly colBordered = false;
    readonly resizableCol = false;
    readonly responsiveTable = true;
    readonly rowBordered = true;
    readonly rowNoHover = false;
    readonly rowStriped = false;
    readonly tableColumnTypes = TableColumnTypes;
    readonly tableRowType = TableRowType;
    readonly truncateCellContent = true;
    readonly wrapCellContent = false;

    private disabledRowMap = {};
    private selectedRowMap = {};

    constructor(
        private stateService: StateService,
    ) { }

    checkIfRowIsDisabled = (row): boolean => this.disabledRowMap[row.id];
    checkIfRowIsSelected = (row): boolean => this.selectedRowMap[row.id];

    columnTrackBy = (i: number): number => i;

    checkIfAllSelected(): boolean {
        const keys = Object.keys(this.selectedRowMap);
        if (keys.length !== this.pageData.length) return false;
        for (let i = 0; i < keys.length; i++) {
            if (!this.selectedRowMap[keys[i]]) {
                return false;
            }
        }
        return true;
    }

    goToRoute(cell) {
        this.stateService.go(cell.routePath, { id: cell.id });
    }

    handleTableSelectClick(actionType: TableRowType, rowId?: string) {
        switch (actionType) {
            case TableRowType.Header:
                this.allRowsSelected = !this.allRowsSelected;

                if (this.allRowsSelected) {
                    this.pageData.forEach((elem) => {
                        this.selectedRowMap[elem.id] = true;
                    });
                } else {
                    this.selectedRowMap = {};
                }
                break;
            case TableRowType.Row:
                this.selectedRowMap[rowId] = !this.selectedRowMap[rowId];
                this.allRowsSelected = this.checkIfAllSelected();
        }
    }

    handleSortChange(sort: { sortKey: string, sortOrder: string }) {
        if (this.onSort.observers.length) {
            this.handleSortChange(sort);
        }
    }

    handlePageChanged(page: number) {
        if (this.onPageChange.observers.length) {
            this.onPageChange.emit(page);
        }
    }

    onToggleMenu(row) {
        this.clickAction.emit(row);
    }
}
