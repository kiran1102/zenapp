import {
    Component,
    Input,
    Inject,
    OnInit,
} from "@angular/core";
import { StateService } from "@uirouter/core";

import { IISystem } from "modules/intg/interfaces/integration.interface";

// constants
import { MarketPlaceView } from "modules/intg/constants/integration.constant";

@Component({
    selector: "intg-card",
    templateUrl: "./intg-card.component.html",
})

export class IntgCardComponent implements OnInit {
    @Input() system: IISystem;
    @Input() componentToNavigate: string;

    private navigationPath: string;

    constructor(
        private stateService: StateService,
    ) {}

    ngOnInit() {
        const isMarketPlaceView = this.componentToNavigate === MarketPlaceView.MARKET_PLACE_LIST_VIEW;
        this.navigationPath = isMarketPlaceView ? "zen.app.pia.module.intg.marketplace.details" : "zen.app.pia.module.intg.systems.details";
    }

    navigateToConnector(systemId: string) {
        this.stateService.go(this.navigationPath, {system: systemId});
    }
}
