// Rxjs
import { from, zip } from "rxjs";

// Angular
import {
    Component,
    Inject,
    EventEmitter,
    OnInit,
    Input,
    Output,
    SimpleChanges,
    OnChanges,
} from "@angular/core";
import { StateService } from "@uirouter/core";

import { IStore } from "interfaces/redux.interface";

// Services
import { IntgEloquaService } from "intsystemservice/intg-eloqua.service";

// Redux
import { StoreToken } from "tokens/redux-store.token";

// Interfaces
import {
    IContactMappingOptions,
    IIContactMappingElement,
    IContactMappingDisplay,
} from "modules/intg/interfaces/integration.interface";
import { INameId } from "interfaces/generic.interface";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

@Component({
    selector: "intg-contact-mapping",
    templateUrl: "./intg-contact-mapping.component.html",
})
export class IntgContactMappingComponent implements OnInit, OnChanges {
    @Input() autoCloseEnabled = true;
    @Input() contactMappings: IIContactMappingElement[];
    @Output() contactMappingOutput = new EventEmitter<IIContactMappingElement>();
    @Output() deleteContactMapping = new EventEmitter<number>();

    connectionID: string;
    contactList: Array<INameId<string>> = [];
    contactMappingList: IContactMappingDisplay[] = [];
    selectedOtfOptionValue: string;
    type = "elements";
    typeOptions: IContactMappingOptions[] = [];
    loading = true;

    selectedOtfOption: IContactMappingOptions;
    selectedContactOption: INameId<string>;

    constructor(
        private stateService: StateService,
        @Inject(StoreToken) private store: IStore,
        private intgEloquaService: IntgEloquaService,
        private translatePipe: TranslatePipe,
    ) {
    }

    ngOnInit(): void {
        this.connectionID = this.stateService.params.id;

        const contactListObservable = from(this.intgEloquaService.getEloquaContactList(this.connectionID)
            .then((response: any): any => {
                this.contactList = response;
            }));
        const oneTrustFieldListObservable = from(this.intgEloquaService.getOtfList(this.type)
            .then((response: any): any => {
                this.typeOptions = response;
            }));

        zip(contactListObservable, oneTrustFieldListObservable)
            .subscribe(() => {
                this.resetMappingList(this.contactMappings);
                this.loading = false;
            });
    }

    public ngOnChanges(changes: SimpleChanges): void {
        if (changes.contactMappings && changes.contactMappings.currentValue) {
            this.resetMappingList(changes.contactMappings.currentValue);
        }
    }

    public validateForm = (): boolean => {
        if (!this.selectedOtfOption || !this.selectedContactOption) {
            return false;
        }

        return this.selectedOtfOption.FieldId && !!this.selectedContactOption.id;
    }

    public selectOtfOption(selectedOtfValueObject: IContactMappingOptions): void {
        this.selectedOtfOption = selectedOtfValueObject;
        this.selectedOtfOptionValue = this.selectedOtfOption.FieldName;
    }

    selectContactOption(selectedContactValueObject: INameId<string>): void {
        this.selectedContactOption = selectedContactValueObject;
    }

    addToList(): void {
        const contactMapping: IIContactMappingElement = {
            dsElementId: this.selectedOtfOption.FieldId,
            contactId: this.selectedContactOption.id,
        };

        this.contactMappingOutput.emit(contactMapping);

        // reset dropdown values
        this.selectedContactOption = null;
        this.selectedOtfOption = null;
    }

    private resetMappingList(contacts: IIContactMappingElement[]): void {
        if (this.contactList.length === 0 || this.typeOptions.length === 0) return;

        this.contactMappingList = contacts.map((mappedContact: IIContactMappingElement): IContactMappingDisplay => {
            const displayContactMapping: IContactMappingDisplay = {};
            displayContactMapping.contactField = this.contactList.find((contact: INameId<string>) => contact.id === mappedContact.contactId);
            displayContactMapping.dsElementField = this.typeOptions.find((option: IContactMappingOptions) => option.FieldId === mappedContact.dsElementId);
            return displayContactMapping;
        });
    }

    private removeContactMapping(index: number): void {
        this.deleteContactMapping.emit(index);
    }

    private trackContactMappingList(index: number): number {
        return index;
    }
}
