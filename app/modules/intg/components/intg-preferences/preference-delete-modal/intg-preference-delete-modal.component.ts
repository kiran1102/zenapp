// Angular
import {
    Component,
    Inject,
    OnInit,
} from "@angular/core";

// Interface
import { IIntgEditModal } from "modules/intg/interfaces/integration.interface";

// Redux
import { getModalData } from "oneRedux/reducers/modal.reducer";
import { StoreToken } from "tokens/redux-store.token";

// services
import { ModalService } from "sharedServices/modal.service";

// Model
import { IStore } from "interfaces/redux.interface";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

@Component({
    selector: "intg-preference-delete-modal",
    templateUrl: "./intg-preference-delete-modal.component.html",
})

export class IntgPreferencesDeleteModalComponent implements OnInit {
    autoCloseEnabled = true;
    modaldata: IIntgEditModal;
    deleteMessageConfirmation: string;
    confirmString: string;
    modalTitle: string;
    constructor(
        @Inject(StoreToken) private store: IStore,
        private translatePipe: TranslatePipe,
        private readonly modalService: ModalService,
    ) { }

    ngOnInit(): void {
        this.modalTitle = this.translatePipe.transform("DeletePurpose");
        this.deleteMessageConfirmation = this.translatePipe.transform("PreferenceDspDeleteConfirmationText");
        this.confirmString = this.translatePipe.transform("DoYouWantToContinue");
        this.modaldata = getModalData(this.store.getState());
    }

    closeModal(): void {
        this.modalService.closeModal();
    }

    onDeletePreferenceValue(): void {
        this.modaldata.deleteHandler();
        this.modalService.closeModal();
    }
}
