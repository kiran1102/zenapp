// Rxjs
import { Observable, from, zip } from "rxjs";

// Angular
import {
    Component,
    Inject,
    OnInit,
} from "@angular/core";
import { StateService } from "@uirouter/core";

// External
import {
    find,
    forEach,
    every,
} from "lodash";

// Interface
import {
    IIPurposeTopicsSelected,
    IIModuleFilters,
    IIPreferencesId,
    IIPreferenceCdoPurposeMap,
    IIPreferencesubArrayValues,
    IIntgEditModal,
} from "modules/intg/interfaces/integration.interface";

// Redux
import { getModalData } from "oneRedux/reducers/modal.reducer";
import { StoreToken } from "tokens/redux-store.token";

// services
import { ModalService } from "sharedServices/modal.service";
import { ConsentPurposeService } from "consentModule/shared/services/cr-purpose.service";
import { IntgEloquaService } from "intsystemservice/intg-eloqua.service";

// Model
import { EntityItem } from "crmodel/entity-item";
import { IStore } from "interfaces/redux.interface";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

@Component({
    selector: "intg-preference-modal",
    templateUrl: "./intg-preference-modal.component.html",
})

export class IntgPreferencesModalComponent implements OnInit {
    autoCloseEnabled = true;
    connectionID: string;
    purposeList: EntityItem[];
    selectedPurposeOption: EntityItem;
    cdoList: EntityItem[];
    selectedCdoOption: EntityItem;
    cdoOptionsSelected: IIModuleFilters[];
    purposeOptionsSelected: IIPurposeTopicsSelected[] = [];
    selectedPurposeTopics: IIPurposeTopicsSelected[] = [];
    selectedCdoAttributes: IIModuleFilters[] = [];
    modaldata: IIntgEditModal;
    purposeTopics: IIPreferencesubArrayValues[] = [];
    canShowTopicMapping = false;
    onEditDisable: boolean;
    purposeListObservable: Observable<void>;
    canSaveEditTitle: string;
    modalTitle: string;
    editValuesLoader: boolean;

    constructor(
        private stateService: StateService,
        @Inject(StoreToken) private store: IStore,
        private consentPurposeService: ConsentPurposeService,
        private translatePipe: TranslatePipe,
        private intgEloquaService: IntgEloquaService,
        private readonly modalService: ModalService,
    ) { }

    ngOnInit(): void {
        this.connectionID = this.stateService.params.id;
        this.modalTitle = this.translatePipe.transform("AddPurpose");
        this.modaldata = getModalData(this.store.getState());
        const purposeListObservable = from(this.consentPurposeService.getActivePurposeFilters()
            .then((response: EntityItem[]): void => {
                this.purposeList = response;
                this.modaldata.editMappingValues.forEach((id: IIPreferencesId) => {
                    const itemIndex = this.purposeList.findIndex((values: EntityItem) => values.Id === id.purposeId);
                    this.purposeList.splice(itemIndex, 1);
                });
            }));

        const cdoListObservable = from(this.intgEloquaService.getEloquaCdoFilters(this.connectionID)
            .then((response: EntityItem[]): void => {
                this.cdoList = response;
                this.modaldata.editMappingValues.forEach((id: IIPreferencesId) => {
                    const itemIndex = this.cdoList.findIndex((values: EntityItem) => values.Id === id.cdoId);
                    this.cdoList.splice(itemIndex, 1);
                });
            }));

        if (this.modaldata.editModalValue === true) {
            this.editValuesLoader = true;
            this.selectedPurposeOption = this.modaldata.selectedRowValue.purposeId;
            this.selectPurposeOption(this.selectedPurposeOption);
            this.selectedCdoOption = this.modaldata.selectedRowValue.cdoId;
            this.selectCdoOption(this.selectedCdoOption);
            this.onEditDisable = this.modaldata.editModalValue;
            this.canSaveEditTitle = this.translatePipe.transform("Save");
        } else {
            this.pushTopicsAttribute();
            this.canSaveEditTitle = this.translatePipe.transform("Create");
        }
    }

    selectPurposeOption(selectedPurposeObject: EntityItem): void {
        this.selectedPurposeOption = selectedPurposeObject;
        const purposeSelectedObservable = from(this.consentPurposeService.getPurposeDetails(this.selectedPurposeOption.Id, this.selectedPurposeOption.Version)
            .then((response: any): void => {
                this.purposeOptionsSelected = response.Topics;
            }));
        if (this.modaldata.editModalValue) {
            this.purposeListObservable = purposeSelectedObservable;
        } else {
            this.purposeTopics = [];
            this.purposeTopics.push({
                purposeTopicId: null,
                cdoAttributesId: null,
            });
        }
    }

    selectCdoOption(selectedCdoObject: EntityItem): void {
        this.selectedCdoOption = selectedCdoObject;
        const cdoSelectedObservable = from(this.intgEloquaService.getEloquaSelectedCdoList(this.connectionID, this.selectedCdoOption.Id)
            .then((response: IIModuleFilters[]): void => {
                this.cdoOptionsSelected = response;
            }));
        if (this.modaldata.editModalValue) {
            zip(cdoSelectedObservable, this.purposeListObservable).subscribe(() => {
                this.displayTopicAttributes();
                this.editValuesLoader = false;
                this.canShowTopicMapping = this.onEditDisable;
            });
        } else {
            this.purposeTopics = [];
            this.purposeTopics.push({
                purposeTopicId: null,
                cdoAttributesId: null,
            });
        }
    }

    displayTopicAttributes(): IIPreferencesubArrayValues[] {
        const selectedTopicAttributes: IIPreferencesId = find(this.modaldata.editMappingValues, (items: IIPreferencesId): boolean => {
            return this.selectedPurposeOption.Id === items.purposeId;
        });
        const displayTopicAttributes = selectedTopicAttributes.topicAttributes.map((displayValues: IIPreferenceCdoPurposeMap) => {
            const purposeTopicId = find(this.purposeOptionsSelected, (items: IIPurposeTopicsSelected): boolean => {
                return items.Id === displayValues.purposeTopicId;
            });
            const cdoAttributesId = find(this.cdoOptionsSelected, (items: IIModuleFilters): boolean => {
                return items.id === displayValues.cdoAttributesId;
            });
            return {
                purposeTopicId,
                cdoAttributesId,
            };
        });
        this.selectedPurposeTopics = [];
        this.selectedCdoAttributes = [];
        if (!displayTopicAttributes.length) {
            this.pushTopicsAttribute(null, null);
        } else {
            forEach(displayTopicAttributes, (val: IIPreferencesubArrayValues) => {
                this.pushTopicsAttribute(val.purposeTopicId, val.cdoAttributesId);
            });
        }
        return displayTopicAttributes;
    }

    selectPurposetopics(selectedtopics: IIPurposeTopicsSelected, index: number): void {
        this.purposeTopics[index].purposeTopicId = selectedtopics;
    }

    selectCdoAttributes(selectedCdoObject: IIModuleFilters, index: number): void {
        this.purposeTopics[index].cdoAttributesId = selectedCdoObject;
    }

    canEditSave(): void {
        const eloquaPreferenceMap: IIPreferencesId = {
            purposeId: this.selectedPurposeOption.Id,
            cdoId: this.selectedCdoOption.Id,
        };
        const selectedtopicAttribute: IIPreferenceCdoPurposeMap[] = [];
        this.purposeTopics.forEach((obj: IIPreferencesubArrayValues) => {
            if (obj.cdoAttributesId && obj.purposeTopicId) {
                selectedtopicAttribute.push({
                    purposeTopicId: obj.purposeTopicId.Id,
                    cdoAttributesId: obj.cdoAttributesId.id,
                });
            }
        });
        if (selectedtopicAttribute.length) {
            eloquaPreferenceMap["topicAttributes"] = selectedtopicAttribute;
        }
        if (this.modaldata.editModalValue) {
            this.canEdit(eloquaPreferenceMap);
        } else {
            this.canSavePreferences(eloquaPreferenceMap);
        }
    }

    canSavePreferences(eloquaPreferenceMap: IIPreferencesId): void {
        this.modaldata.handler(eloquaPreferenceMap);
    }

    showAddTopics(): void {
        this.canShowTopicMapping = !this.canShowTopicMapping;
    }

    closeModal(): void {
        this.modalService.closeModal();
    }

    pushTopicsAttribute(purposeTopicId?: IIPurposeTopicsSelected, cdoAttributesId?: IIModuleFilters): void {
        if (purposeTopicId && cdoAttributesId) {
            this.purposeTopics.push({
                purposeTopicId,
                cdoAttributesId,
            });
        } else {
            this.purposeTopics.push({
                purposeTopicId: null,
                cdoAttributesId: null,
            });
        }
    }

    popTopicsAttribute(index: number): void {
        if (index === 0 && this.purposeTopics.length === 1) {
            this.canShowTopicMapping = false;
            this.purposeTopics = [{ purposeTopicId: null, cdoAttributesId: null }];
        } else {
            this.purposeTopics.splice(index, 1);
        }
    }

    canEdit(eloquaPreferenceMap: IIPreferencesId): void {
        this.modaldata.editHandler(eloquaPreferenceMap);
    }

    public validateForm = (): boolean => {
        let disabled = true;
        if (this.canShowTopicMapping) {
            disabled = !every(this.purposeTopics, (topic) => topic.cdoAttributesId && topic.purposeTopicId);
        } else if (this.selectedPurposeOption && this.selectedCdoOption) {
            disabled = false;
        }
        return disabled;
    }
}
