// Rxjs
import { from, zip } from "rxjs";

// Angular
import {
    Component,
    OnInit,
    Input,
    Output,
    EventEmitter,
} from "@angular/core";
import { StateService } from "@uirouter/core";

// Services
import { ModalService } from "sharedServices/modal.service";
import { ConsentPurposeService } from "consentModule/shared/services/cr-purpose.service";
import { IntgEloquaService } from "intsystemservice/intg-eloqua.service";

// Interface
import {
    IIPreferencesId,
    IIntgPreferenecePurpose,
    IIntgCreateModal,
    IIntgEditModal,
    IPreferenceMapDataValue,
} from "modules/intg/interfaces/integration.interface";
import { IDropdownOption } from "interfaces/one-dropdown.interface";

// Model
import { EntityItem } from "crmodel/entity-item";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

@Component({
    selector: "intg-preferences",
    templateUrl: "./intg-preferences.component.html",
})

export class IntgPreferencesComponent implements OnInit {
    @Input() autoCloseEnabled = true;
    @Input() editDeletePreference = false;
    @Input() intgPreferenceDataList: IIPreferencesId[];
    @Output() preferenceModalDataEmit = new EventEmitter<IIPreferencesId>();
    @Output() editPreferenceModalEmit = new EventEmitter<IIPreferencesId>();
    @Output() deletePreferenceModalEmit = new EventEmitter<any>();

    purposeList: EntityItem[];
    cdoList: EntityItem[];
    connectionID: string;
    dataMappingGroup: IIntgPreferenecePurpose[];
    rowBordered = true;
    sortKey = "";
    resizableCol = false;
    responsiveTable = true;
    editRowIndex: number;
    editColIndex: number;
    truncateCellContent = true;
    wrapCellContent = false;
    displayPreferenceData: IIntgPreferenecePurpose[];
    loading = true;
    menuOptions: IDropdownOption[];

    preferenceMapDataValues: IPreferenceMapDataValue[] = [
        { sortKey: 1, columnType: null, columnName: this.translatePipe.transform("OneTrustPurpose"), columnWidth: null, cellValueKey: "purposeId", labelKey: "Label" },
        { sortKey: 2, columnType: null, columnName: this.translatePipe.transform("EloquaObject"), columnWidth: null, cellValueKey: "cdoId", labelKey: "Label" },
        { sortKey: 3, columnType: "action", columnName: "", columnWidth: null, cellValueKey: "" },
    ];

    constructor(
        private stateService: StateService,
        private consentPurposeService: ConsentPurposeService,
        private intgEloquaService: IntgEloquaService,
        private translatePipe: TranslatePipe,
        private modalService: ModalService,
    ) {
    }

    ngOnInit() {
        this.connectionID = this.stateService.params.id;

        const purposeListObservable = from(this.consentPurposeService.getActivePurposeFilters()
            .then((response: EntityItem[]): void => {
                this.purposeList = response;
            }));

        const cdoListObservable = from(this.intgEloquaService.getEloquaCdoFilters(this.connectionID)
            .then((response: EntityItem[]): void => {
                this.cdoList = response;
            }));

        zip(purposeListObservable, cdoListObservable).subscribe(() => {
            this.displayPopulatedValues(this.intgPreferenceDataList);
            this.displayPreferenceData = this.dataMappingGroup;
            this.loading = false;
        });
    }

    openPrefAdd(): void {
        const modalData: IIntgCreateModal = {
            editModalValue: false,
            editMappingValues: this.intgPreferenceDataList,
            handler: (response: IIPreferencesId): void => {
                this.preferenceModalDataEmit.emit(response);
            },
        };
        this.modalService.setModalData(modalData);
        this.modalService.openModal("intgPreferencesModalComponent");
    }

    displayPopulatedValues(intgPreferenceDataList: IIPreferencesId[]): IIntgPreferenecePurpose[] {
        this.dataMappingGroup = intgPreferenceDataList.map((populateValues: IIPreferencesId): IIntgPreferenecePurpose => {
            const purposeId = this.purposeList.find((purpose: EntityItem) => purpose.Id === populateValues.purposeId);
            const cdoId = this.cdoList.find((cdo: EntityItem) => cdo.Id === populateValues.cdoId);
            return {
                purposeId,
                cdoId,
            };
        });
        return this.dataMappingGroup;
    }

    getContextActions() {
        this.menuOptions = [
            {
                text: this.translatePipe.transform("Edit"),
                action: (row): void => {
                    this.editPreferenceModal(row);
                },
            },
            {
                text: this.translatePipe.transform("Delete"),
                action: (row): void => {
                    this.deletePreferenceModal(row);
                },
            },
        ];
    }

    toggleContextMenu(isOpen: boolean): boolean {
        return this.editDeletePreference = isOpen;
    }

    editPreferenceModal(selectedRowValues: IIntgPreferenecePurpose): void {
        const editModalData: IIntgEditModal = {
            editMappingValues: this.intgPreferenceDataList,
            selectedRowValue: selectedRowValues,
            editModalValue: true,
            editHandler: (createModalData: IIPreferencesId) => {
                this.editPreferenceModalEmit.emit(createModalData);
            },
        };
        this.modalService.setModalData(editModalData);
        this.modalService.openModal("intgPreferencesModalComponent");
    }

    deletePreferenceModal(selectedRowValues: IIntgPreferenecePurpose): void {
        const deleteModalData = {
            deleteHandler: () => this.deletePreferenceModalEmit.emit(selectedRowValues),
        };
        this.modalService.setModalData(deleteModalData);
        this.modalService.openModal("IntgPreferencesDeleteModalComponent");
    }

    trackPreferenceMapDataValues(index: number): number {
        return index;
    }
}
