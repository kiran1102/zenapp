// Angular
import { Component, Inject, OnInit } from "@angular/core";
import {
    FormBuilder,
    FormGroup,
    Validators,
    FormArray,
} from "@angular/forms";

// 3rd party
import {
     find,
     keys,
     map,
} from "lodash";

// Redux / Tokens
import { IStore } from "interfaces/redux.interface";
import { StoreToken } from "tokens/redux-store.token";
import { getModalData } from "oneRedux/reducers/modal.reducer";

// Services
import { ModalService } from "sharedServices/modal.service";
import { IntgCredentialsService } from "intsystemservice/intg-credentials.service";
import { IntgCredentialsHelper } from "intsystemservice/helpers/intg-credentials-helper.service";
import { NotificationService } from "sharedModules/services/provider/notification.service";

// Pipe, Constant and Types
import { TranslatePipe } from "modules/pipes/translate.pipe";
import {
    IntgAuthTypeKeys,
    IntgAuthType,
    IntgGrantTypeKeys,
    IntgGrantType,
    IntgActionHeaderTypes,
    LookUpValueOptionType,
    ClientAuthenticationTypeKeys,
    ClientAuthenticationType,
} from "modules/intg/constants/integration.constant";
import { AuthType } from "modules/intg/types/intg.type";
import { NO_BLANK_SPACE } from "constants/regex.constant";

// Interfaces
import {
    IICreateCredential,
    IIOAuth2ResponseBody,
    IIOAuth2TokenResponse,
} from "modules/intg/interfaces/integration.interface";
import {
    IKeyValue,
    IStringMap,
} from "interfaces/generic.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";

// Enums
import { StatusCodes } from "enums/status-codes.enum";

@Component({
    selector: "intg-credentials-add-modal",
    templateUrl: "./intg-credentials-add-modal.component.html",
})
export class IntgCredentialsAddModal implements OnInit {
    ready = false;
    modalTitle = "AddCredential";
    modalData = getModalData(this.store.getState());
    credential: IICreateCredential;
    isEdit: boolean;
    selectedOption = [];
    inputValue: string;
    credentialForm: FormGroup;
    intgAuthType = IntgAuthType;
    intgGrantType = IntgGrantType;
    saveInProgress = false;
    generateTokenInProgress = false;
    allowOtherPrependText = "AddOption";
    selectedGrantType: string;
    authTypes = [
        {
            key: this.translatePipe.transform(IntgAuthTypeKeys.BASIC),
            value: IntgAuthType.BASIC,
        },
        {
            key: this.translatePipe.transform(IntgAuthTypeKeys.BEARER_TOKEN),
            value: IntgAuthType.BEARER_TOKEN,
        },
        {
            key: this.translatePipe.transform(IntgAuthTypeKeys.OAUTH2_TOKEN),
            value: IntgAuthType.OAUTH2_TOKEN,
        },
    ];

    grantTypes = [
        {
            key: this.translatePipe.transform(IntgGrantTypeKeys.Password_Credentials),
            value: IntgGrantType.Password_Credentials,
        },
        {
            key: this.translatePipe.transform(IntgGrantTypeKeys.Refresh),
            value: IntgGrantType.Refresh,
        },
        {
            key: this.translatePipe.transform(IntgGrantTypeKeys.Client_Credentials),
            value: IntgGrantType.Client_Credentials,
        },
    ];

    clientAuthentication: Array<IKeyValue<string>> = [
        {
            key: this.translatePipe.transform(ClientAuthenticationTypeKeys.SEND_IN_REQUESTPARAMS),
            value: ClientAuthenticationType.SEND_IN_REQUESTPARAMS,
        },
        {
            key: this.translatePipe.transform(ClientAuthenticationTypeKeys.SEND_IN_BODY),
            value: ClientAuthenticationType.SEND_IN_BODY,
        },
        {
            key: this.translatePipe.transform(ClientAuthenticationTypeKeys.SEND_IN_URL_ENCODED),
            value: ClientAuthenticationType.SEND_IN_URL_ENCODED,
        },
    ];

    headerTypes: Array<IKeyValue<string>> = [
        {
            key: IntgActionHeaderTypes.Content_Type,
            value: IntgActionHeaderTypes.Content_Type,
        },
        {
            key: IntgActionHeaderTypes.Authorization,
            value: IntgActionHeaderTypes.Authorization,
        },
        {
            key: IntgActionHeaderTypes.Accept,
            value: IntgActionHeaderTypes.Accept,
        },
    ];

    constructor(
        @Inject(StoreToken) private store: IStore,
        private modalService: ModalService,
        private readonly formBuilder: FormBuilder,
        private readonly translatePipe: TranslatePipe,
        private readonly intgCredentialsApi: IntgCredentialsService,
        private readonly intgCredentialsHelper: IntgCredentialsHelper,
        private readonly notificationService: NotificationService,
    ) {}

    ngOnInit() {
        this.ready = true;
        this.isEdit = Boolean(this.modalData.credential);
        if (this.isEdit) {
            this.modalTitle = "EditCredential";
            this.credential = this.modalData.credential as IICreateCredential;
        } else {
            this.modalTitle = "AddCredential";
            this.credential = {
                authContext: {
                    BasicAuth: {
                        type: "BASIC",
                        headers: {},
                    },
                },
            } as IICreateCredential;
        }
        this.credentialForm = this.initializeForm(this.credential);
        this.applyConditionalControls(this.isEdit ? this.credential.authType : this.authTypes[1].value as AuthType);
        this.formControlValueChanged();
    }

    onModelChange({ currentValue }, index) {
        this.selectedOption[index] = currentValue;
        const control = this.credentialForm.controls["defaultHeader"] as FormArray;
        const selectedHeader = control.at(index);
        selectedHeader.patchValue({ key: currentValue ? currentValue.value : null});
        if (!currentValue) {
            if (selectedHeader.get("value").value) {
                selectedHeader.get("key").setErrors({ incorrect: true });
            } else {
                selectedHeader.get("key").setErrors(null);
            }
        } else {
            if (!selectedHeader.get("value").value) {
                selectedHeader.get("value").setErrors({ incorrect: true });
            } else {
                selectedHeader.get("key").setErrors(null);
            }
        }
    }

    onInputChange({ key, value }: IKeyValue<string>) {
        if (key === "Enter") return;
        this.inputValue = value;
    }

    onClickAddCredential() {
        this.saveInProgress = true;
        const credential:  IICreateCredential = this.intgCredentialsHelper.convertFormDataToModel(this.credentialForm, this.credential.id);
        this.intgCredentialsApi.saveCredential(credential).then((response: IProtocolResponse<IICreateCredential>) => {
            if (response.result) {
                this.closeModal(true, response.data);
            }
            this.saveInProgress = false;
        });
    }

    onRemoveOptionClick(index: number) {
        const control = this.credentialForm.controls["defaultHeader"] as FormArray;
        control.removeAt(index);
        this.selectedOption.splice(index, 1);
    }

    handleFirstOption(value: string, index: number) {
        const selectedOp = { value, key: value, optionType: LookUpValueOptionType.OTHERS };
        this.selectedOption[index] = selectedOp;
        const control = this.credentialForm.controls["defaultHeader"] as FormArray;
        const selectedHeader = control.at(index);
        if (selectedHeader) {
            selectedHeader.patchValue({ key: value });
        }
    }

    onAddOptionClick() {
        const control = this.credentialForm.controls["defaultHeader"] as FormArray;
        control.push(this.formBuilder.group({
            key: ["", Validators.pattern(NO_BLANK_SPACE)],
            value: ["", Validators.pattern(NO_BLANK_SPACE)],
        }));
    }

    onValueChange(value: string, index: number) {
        const control = this.credentialForm.controls["defaultHeader"] as FormArray;
        const selectedHeader = control.at(index);
        if (!value.trim()) {
            if (selectedHeader.get("key").value) {
                selectedHeader.get("value").setErrors({ incorrect: true });
            } else {
                selectedHeader.get("key").setErrors(null);
            }
        } else {
            if (!selectedHeader.get("key").value) {
                selectedHeader.get("key").setErrors({ incorrect: true });
            } else {
                selectedHeader.get("key").setErrors(null);
            }
        }
    }

    closeModal(result = false, response?) {
        if (this.modalData.callback && response) {
            this.modalData.callback(result, response);
        }
        this.modalService.closeModal();
        this.modalService.clearModalData();
    }

    generateToken() {
        let requestUri;
        this.generateTokenInProgress = true;
        if (this.credentialForm.get("oAuth").get("authParameterType").value.value === ClientAuthenticationType.SEND_IN_REQUESTPARAMS) {
            requestUri = this.intgCredentialsHelper.getRequestUri(this.credentialForm);
            this.getAccessToken(requestUri);
        } else if (this.credentialForm.get("oAuth").get("authParameterType").value.value === ClientAuthenticationType.SEND_IN_URL_ENCODED) {
            const requestBody = this.intgCredentialsHelper.getRequestBody(this.credentialForm, ClientAuthenticationType.SEND_IN_URL_ENCODED);
            requestUri = this.credentialForm.get("oAuth").get("accessTokenUrl").value;
            this.getAccessToken(requestUri, requestBody as string, { "Content-Type": "application/x-www-form-urlencoded" });
        } else if (this.credentialForm.get("oAuth").get("authParameterType").value.value === ClientAuthenticationType.SEND_IN_BODY) {
            const requestBody = this.intgCredentialsHelper.getRequestBody(this.credentialForm, ClientAuthenticationType.SEND_IN_BODY);
            requestUri = this.credentialForm.get("oAuth").get("accessTokenUrl").value;
            this.getAccessToken(requestUri, JSON.stringify(requestBody));
        }
    }

    private getAccessToken(requestUri: string, requestBody?: string, headers?: IStringMap<string>) {
        const oAuthBody: IIOAuth2TokenResponse = headers ? {requestUri, headers} : {requestUri};
        this.intgCredentialsApi.getOAuthToken(oAuthBody, requestBody).subscribe((response: IProtocolResponse<IIOAuth2TokenResponse>) => {
            if (response.result) {
                if (response.data.httpStatus === StatusCodes.Success) {
                    const responseControl = this.credentialForm.controls["oAuthResponse"] as FormGroup;
                    const responseBody: IIOAuth2ResponseBody = JSON.parse(response.data.body);
                    if (responseBody.access_token) {
                        responseControl.controls["accessToken"].setValue(responseBody.access_token);
                    }
                    if (responseBody.refresh_token) {
                        responseControl.controls["refreshToken"].setValue(responseBody.refresh_token);
                    }
                } else {
                    const errorMsg = this.translatePipe.transform("CannotGenerateAccessToken");
                    this.notificationService.alertError(this.translatePipe.transform("Error"), errorMsg);
                }
            }
            this.generateTokenInProgress = false;
        });
    }

    private getDefaultHeaderFormArray(): FormArray {
        let headers: IStringMap<string>;
        if (this.isEdit) {
            switch (this.credential.authType) {
                case IntgAuthType.BASIC:
                    headers = this.credential.authContext.BasicAuth.headers;
                    break;
                case IntgAuthType.BEARER_TOKEN:
                    headers = this.credential.authContext.BearerTokenAuth.headers;
                    break;
                case IntgAuthType.NO_AUTH:
                    headers = this.credential.authContext.NoAuth.headers;
                    break;
                case IntgAuthType.OAUTH2_TOKEN:
                    headers = this.credential.authContext.OAuth2TokenAuth.headers;
                    break;
            }
        }
        let index = 0;
        if (keys(headers).length) {
            return this.formBuilder.array(map(headers, (value: string, key: string) => {
                this.selectedOption[index++] = { key, value: key };
                return this.formBuilder.group({key, value});
            }));

        } else {
            return this.formBuilder.array([this.formBuilder.group({ key: "", value: ""})]);
        }
    }

    private initializeForm(credential: IICreateCredential): FormGroup {
        return this.formBuilder.group({
            name: [credential.name, [Validators.required, Validators.pattern(NO_BLANK_SPACE)]],
            description: [credential.description],
            hostname: [this.intgCredentialsHelper.getHostname(credential)],
            defaultHeader: this.getDefaultHeaderFormArray(),
            authType: [this.getSelectedAuth(this.isEdit ? credential.authType : this.authTypes[1].value), Validators.required],
        });
    }

    private getSelectedAuth(value: string) {
        return find(this.authTypes, (authType) => authType.value === value);
    }

    private getSelectedGrantType(value: string) {
        return find(this.grantTypes, (grantType: IKeyValue<string>) => grantType.value === value);
    }

    private getSelectedAuthParamType(value: string): IKeyValue<string> {
        return this.clientAuthentication.find((paramType: IKeyValue<string>) => paramType.value === value);
    }

    private formControlValueChanged(): void {
        this.credentialForm.get("authType").valueChanges.subscribe((authType: IKeyValue<AuthType>) => {
            this.applyConditionalControls(authType.value);
        });
    }

    private applyConditionalControls(authValue: AuthType): void  {
        switch (authValue) {
            case IntgAuthType.BASIC:
                const username = this.isEdit && this.credential.authContext.BasicAuth ? this.credential.authContext.BasicAuth.userName : "";
                const password = this.isEdit && this.credential.authContext.BasicAuth ? this.credential.authContext.BasicAuth.password : "";
                this.intgCredentialsHelper.removeControls(this.credentialForm, ["token", "oAuth", "oAuthResponse"]);
                this.credentialForm.addControl("username", this.formBuilder.control(username, Validators.required));
                this.credentialForm.addControl("password", this.formBuilder.control(password, Validators.required));
                break;
            case IntgAuthType.BEARER_TOKEN:
                const token = this.isEdit && this.credential.authContext.BearerTokenAuth ? this.credential.authContext.BearerTokenAuth.token : "";
                this.intgCredentialsHelper.removeControls(this.credentialForm, ["username", "password", "oAuth", "oAuthResponse"]);
                this.credentialForm.addControl("token", this.formBuilder.control(token, Validators.required));
                break;
            case IntgAuthType.OAUTH2_TOKEN:
                this.intgCredentialsHelper.removeControls(this.credentialForm, ["username", "password", "token"]);
                const OAuth2TokenAuth = this.credential.authContext.OAuth2TokenAuth;
                const grantType = this.getSelectedGrantType(this.isEdit && OAuth2TokenAuth ? OAuth2TokenAuth.grantType : this.grantTypes[0].value);
                const accessTokenUrl = this.isEdit && OAuth2TokenAuth ? OAuth2TokenAuth.tokenUrl : "";
                const clientId = this.isEdit && OAuth2TokenAuth ? OAuth2TokenAuth.clientId : "";
                const accessToken = this.isEdit && OAuth2TokenAuth ? OAuth2TokenAuth.accessToken : "";
                const refreshToken = this.isEdit && OAuth2TokenAuth ? OAuth2TokenAuth.refreshToken : "";
                const authParameterType = this.getSelectedAuthParamType(this.isEdit && OAuth2TokenAuth &&  OAuth2TokenAuth.authParameterType ? OAuth2TokenAuth.authParameterType : this.clientAuthentication[0].value);
                this.credentialForm.addControl("oAuth", this.formBuilder.group({
                    grantType: [grantType, [Validators.required]],
                    accessTokenUrl: [accessTokenUrl, [Validators.required]],
                    clientId: [clientId, [Validators.required]],
                    authParameterType: [authParameterType, [Validators.required]],
                }));

                this.credentialForm.addControl("oAuthResponse", this.formBuilder.group({
                    accessToken: [accessToken, [Validators.required]],
                    refreshToken: [refreshToken],
                }));

                this.credentialForm.get("oAuth").get("grantType").valueChanges.subscribe((selectedGrantType: IKeyValue<AuthType>) => {
                    this.onGrantTypeChange(selectedGrantType.value);
                });
                this.onGrantTypeChange(grantType.value);
                break;
        }
    }

    private onGrantTypeChange(grantType: string) {
        this.selectedGrantType = grantType;
        const OAuth2TokenAuth = this.credential.authContext.OAuth2TokenAuth;
        const oAuthControl = this.credentialForm.get("oAuth") as FormGroup;
        const clientSecret = this.isEdit && OAuth2TokenAuth ? OAuth2TokenAuth.clientSecret : "";

        switch (grantType) {
            case IntgGrantType.Password_Credentials:
                const oAuthUsername = this.isEdit && OAuth2TokenAuth ? OAuth2TokenAuth.username : "";
                const oAuthPassword = this.isEdit && OAuth2TokenAuth ? OAuth2TokenAuth.password : "";
                if (oAuthControl.contains("scope")) {
                    this.intgCredentialsHelper.removeControls(oAuthControl, ["scope"]);
                }
                oAuthControl.addControl("oAuthUsername", this.formBuilder.control(oAuthUsername, Validators.required));
                oAuthControl.addControl("oAuthPassword", this.formBuilder.control(oAuthPassword, Validators.required));
                if (!oAuthControl.contains("clientSecret")) {
                    oAuthControl.addControl("clientSecret", this.formBuilder.control(clientSecret, Validators.required));
                }
                break;
            case IntgGrantType.Refresh:
                if (oAuthControl.contains("oAuthUsername")) {
                    this.intgCredentialsHelper.removeControls(oAuthControl, ["oAuthUsername", "oAuthPassword"]);
                }
                if (oAuthControl.contains("scope")) {
                    this.intgCredentialsHelper.removeControls(oAuthControl, ["scope"]);
                }
                if (!oAuthControl.contains("clientSecret")) {
                    oAuthControl.addControl("clientSecret", this.formBuilder.control(clientSecret, Validators.required));
                }
                break;
            case IntgGrantType.Client_Credentials:
                const scope = this.isEdit && OAuth2TokenAuth ? OAuth2TokenAuth.scope : "";
                if (oAuthControl.contains("oAuthUsername")) {
                    this.intgCredentialsHelper.removeControls(oAuthControl, ["oAuthUsername", "oAuthPassword"]);
                }
                if (!oAuthControl.contains("clientSecret")) {
                    oAuthControl.addControl("clientSecret", this.formBuilder.control(clientSecret, Validators.required));
                }
                oAuthControl.addControl("scope", this.formBuilder.control(scope));
                break;
        }
    }
}
