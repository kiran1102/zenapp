// Rxjs
import { from, zip } from "rxjs";

// Angular
import {
    Component,
    Inject,
    OnInit,
    Input,
    Output,
    EventEmitter,
    SimpleChanges,
} from "@angular/core";
import { StateService } from "@uirouter/core";

// External
import {
    forEach,
    find,
} from "lodash";

// Services
import { IntgEloquaService } from "intsystemservice/intg-eloqua.service";
import { ConsentPurposeService } from "consentModule/shared/services/cr-purpose.service";

// Interface
import {
    IContactMappingOptions,
    IISubjectProfileMappingElement,
    IISubjectProfileMapping,
    IISubjectProfileMappingDisplay,
    IIContactMappingElement,
    IContactMappingDisplay,
    IIDeleteIndex,
    IIPreferencesId,
} from "modules/intg/interfaces/integration.interface";
import { INameId } from "interfaces/generic.interface";

// Model
import { EntityItem } from "crmodel/entity-item";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

@Component({
    selector: "intg-data-subject-profile",
    templateUrl: "./intg-data-subject-profile.component.html",
})
export class IntgDataSubjectProfileComponent implements OnInit {
    @Input() autoCloseEnabled = true;
    @Input() dataSubjProfMapped: IISubjectProfileMapping[];
    @Input() intgAdvPurposeList: IIPreferencesId[];
    @Output() addDataSubjectProfile = new EventEmitter<IISubjectProfileMappingElement>();
    @Output() removeDataSubjProfMapping = new EventEmitter<IIDeleteIndex>();

    connectionID: string;
    type = "profile";
    otfOptions: IContactMappingOptions[] = [];
    purposeList: EntityItem[];
    contactList: Array<INameId<string>> = [];
    selectedPurposeOption: EntityItem;
    selectedOtfOption: IContactMappingOptions;
    selectedContactOption: INameId<string>;
    dataMappingPurposeGroup: IISubjectProfileMappingDisplay[];
    loading = true;

    constructor(
        private stateService: StateService,
        private intgEloquaService: IntgEloquaService,
        private consentPurposeService: ConsentPurposeService,
        private translatePipe: TranslatePipe,
    ) {}

    ngOnInit(): void {
        this.connectionID = this.stateService.params.id;

        const contactListObservable = from(this.intgEloquaService.getEloquaContactList(this.connectionID)
            .then((response: any): void => {
                this.contactList = response;
            }));

        const otfListObservable = from(this.intgEloquaService.getOtfList(this.type)
            .then((response: any): void => {
                this.otfOptions = response;
            }));

        const purposeFilterObservable = from(this.consentPurposeService.getActivePurposeFilters()
            .then((response: EntityItem[]): void => {
                const purposeList: EntityItem[] = [];
                forEach(this.intgAdvPurposeList, (intgAdvPurpose: IIPreferencesId): void => {
                    const entityItem = find(response, (purpose: EntityItem): boolean => intgAdvPurpose.purposeId === purpose.Id);
                    if (entityItem) {
                        purposeList.push(entityItem);
                    }
                });
                this.purposeList = purposeList;
            }));

        zip(contactListObservable, otfListObservable, purposeFilterObservable)
            .subscribe(() => {
                this.resetDataSubMapList(this.dataSubjProfMapped);
                this.loading = false;
            });
    }

    public ngOnChanges(changes: SimpleChanges): void {
        if (changes.dataSubjProfMapped && changes.dataSubjProfMapped.currentValue) {
            this.resetDataSubMapList(changes.dataSubjProfMapped.currentValue);
        }
    }

    selectPurposeOption(selectedPurposeObject: EntityItem): void {
        this.selectedPurposeOption = selectedPurposeObject;
    }

    selectOtfOption(selectedOtfOptionObject: IContactMappingOptions): void {
        this.selectedOtfOption = selectedOtfOptionObject;
    }

    selectContactOption(selectedContactOptions: INameId<string>): void {
        this.selectedContactOption = selectedContactOptions;
    }

    addProfile(): void {
        const dataSubjProfMap: IISubjectProfileMappingElement = {
            contactId: this.selectedContactOption.id,
            purposeId: this.selectedPurposeOption.Id,
            dsElementId: this.selectedOtfOption.FieldId,
        };
        this.addDataSubjectProfile.emit(dataSubjProfMap);

        this.selectedContactOption = null;
        this.selectedOtfOption = null;
        this.selectedPurposeOption = null;
    }

    public validateForm = (): boolean => {
        if (!this.selectedOtfOption || !this.selectedContactOption || !this.selectedPurposeOption) {
            return false;
        }
        return this.selectedOtfOption.FieldId && !!this.selectedContactOption.id && !!this.selectedPurposeOption.Id;
    }

    private resetDataSubMapList(mappings: IISubjectProfileMapping[]): void {

        if (this.contactList.length === 0
            || this.otfOptions.length === 0
            || this.purposeList.length === 0
            || !mappings) {
            return;
        }

        this.dataMappingPurposeGroup = mappings.map((purposeMapping: IISubjectProfileMapping): IISubjectProfileMappingDisplay => {
            const purposeMappingDisplay: IISubjectProfileMappingDisplay = {};
            purposeMappingDisplay.purpose = this.purposeList.find((purpose: EntityItem) => purpose.Id === purposeMapping.purposeId);
            purposeMappingDisplay.contactMappings = purposeMapping.contactMappings.map((contactMapping: IIContactMappingElement) => {
                const contactDisplay: IContactMappingDisplay = {};
                contactDisplay.contactField = this.contactList.find((contact: INameId<string>) => contact.id === contactMapping.contactId);
                contactDisplay.dsElementField = this.otfOptions.find((otfOption: IContactMappingOptions) => otfOption.FieldId === contactMapping.dsElementId);
                return contactDisplay;
            });
            return purposeMappingDisplay;
        });
    }

    private deleteDataSubjProfMap(purposeIndex: number, contactIndex: number): void {

        const indexValues: IIDeleteIndex = {
            purposeIndex,
            contactIndex,
        };
        this.removeDataSubjProfMapping.emit(indexValues);
    }
}
