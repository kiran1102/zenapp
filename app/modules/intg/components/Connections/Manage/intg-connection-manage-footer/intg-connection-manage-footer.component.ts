import {
    Component,
    Inject,
    Output,
    EventEmitter,
    Input,
    OnChanges,
} from "@angular/core";
import { StateService } from "@uirouter/core";

// Interface
import {
    IIConnection,
    IISystemConfigData,
} from "modules/intg/interfaces/integration.interface";
import { IFormFieldInput } from "interfaces/form-field.interface";
import { ITabsNav } from "interfaces/one-tabs-nav.interface";
import { IStringMap } from "interfaces/generic.interface";

// lodash
import {
    isNil,
    some,
} from "lodash";

// Constant
import { SystemConstants } from "modules/intg/constants/integration.constant";

// Enum
import { ConnectionManageTabs } from "enums/integration.enum";

@Component({
    selector: "intg-connection-manage-footer",
    templateUrl: "./intg-connection-manage-footer.component.html",
})

export class IntgConnectionManageFooterComponent implements OnChanges {

    @Input() isSavingConnection: boolean;
    @Input() connection: IIConnection = {};
    @Input() system: IISystemConfigData;
    @Input() outboundFields: IFormFieldInput[];
    @Input() allowedTabs: IStringMap<ITabsNav>;
    @Input() saveActionText: string;
    @Input() selectedTab: string;
    @Output() saveConnection = new EventEmitter();

    canShowFooter: boolean;
    private isOutboundFormValid = false;

    constructor(
        private stateService: StateService,
    ) { }

    ngOnChanges(): void {
        this.canShowFooter = (this.selectedTab === ConnectionManageTabs.ADVANCED)
            || (this.selectedTab === ConnectionManageTabs.AUTHENTICATION)
            || (this.selectedTab === ConnectionManageTabs.DATA_SUBJECT_ELEMENTS)
            || (this.selectedTab === ConnectionManageTabs.DATA_SUBJECT_PROFILE);
    }

    goToRoute() {
        this.stateService.go("zen.app.pia.module.intg.connections.list");
    }

    callback() {
        this.saveConnection.emit();
    }

    canSave(): boolean {
        this.isOutboundFormValid = some(this.system.form.outboundFields, (field: IFormFieldInput): boolean => {
            return field.required && isNil(field.inputValue || null);
        });
        return (this.connection && this.connection.name)
            && (!this.isOutboundFormValid)
            && (this.system.meta.id === SystemConstants.SLACK ? !this.connection.systemInstanceId : true);
    }
}
