// Angular.
import {
    Component,
    Input,
    Output,
    EventEmitter,
    Inject,
} from "@angular/core";

// Interfaces.
import {
    IISystemConfigData,
    IIConnection,
    IITestConnectionResponse,
} from "modules/intg/interfaces/integration.interface";
import {
    IFormFieldBtn,
    IFormFieldInput,
} from "interfaces/form-field.interface";
import { IStringMap } from "interfaces/generic.interface";
import {
    each,
    find,
} from "lodash";
import { IProtocolResponse } from "interfaces/protocol.interface";

// Services
import { IntgApiService } from "intsystemservice/intg-api.service";
import { NotificationService } from "sharedModules/services/provider/notification.service";

// Enums and Constants
import { ConnectionManageMode } from "enums/integration.enum";
import { FormInputTypes } from "constants/form-types.constant";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

@Component({
    selector: "intg-connection-details",
    templateUrl: "./intg-connection-details.component.html",
})
export class IntgConnectionDetailsComponent {

    @Input() system: IISystemConfigData;
    @Input() manageMode: number;
    @Input() connection: IIConnection;
    @Input() formIsDisabled: boolean;

    @Output() handleAction = new EventEmitter();

    modes = ConnectionManageMode;

    constructor(
        public readonly IntgApiService: IntgApiService,
        private translatePipe: TranslatePipe,
        private notificationService: NotificationService,
    ) {

    }

    trackByIndex(index: number): number {
        return index;
    }

    handleFormAction(field: IFormFieldBtn): void | undefined {
        if (!field) return;
        switch (field.inputType) {
            case FormInputTypes.TEST_CONNECTION:
                this.formTestConnection(field);
                break;
        }
    }

    private formTestConnection(field: IFormFieldBtn): void {
        field.isLoading = true;
        const request: IStringMap<string> = this.buildRequestFromFields(
            {
                systemName: this.system.meta.name,
            },
            field.requireFields,
        );
        this.IntgApiService
            .testConnection(request)
            .then((response: IProtocolResponse<IITestConnectionResponse>): void => {
                if (response.result) {
                    this.notificationService.alertSuccess(this.translatePipe.transform("Success"), this.translatePipe.transform("TestConnectionSuccessful"));
                } else {
                    this.notificationService.alertError(this.translatePipe.transform("Error"), this.translatePipe.transform("TestConnectionFailed"));
                }
                field.isLoading = false;
            });
    }

    private buildRequestFromFields(
        request: IStringMap<string>,
        fieldKeys: string[],
    ): IStringMap<string> {
        each(fieldKeys, (requiredField: string): void => {
            const field: IFormFieldInput = this.getFormFieldByKey(requiredField);
            if (field && field.inputValue) {
                request[requiredField] = field.inputValue;
            }
        });
        if (this.connection.systemInstanceId) {
            request.systemInstanceId = this.connection.systemInstanceId;
        }
        return request;
    }

    private getFormFieldByKey(key: string): IFormFieldInput {
        return find(this.system.form.outboundFields, (field: IFormFieldInput): boolean => field.fieldKey === key);
    }
};
