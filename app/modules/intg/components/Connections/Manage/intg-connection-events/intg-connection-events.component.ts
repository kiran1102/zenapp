import {
     Component,
     Input,
     Output,
     EventEmitter,
} from "@angular/core";
import { StateService } from "@uirouter/core";

// Interface
import { IProtocolResponse } from "interfaces/protocol.interface";
import {
    IIEvents,
    IIRoutingKeys,
    IISystemConfigData,
    IIConnection,
    IIPage,
 } from "modules/intg/interfaces/integration.interface";

// Services
import { IntgApiService } from "intsystemservice/intg-api.service";
import { NotificationService } from "sharedModules/services/provider/notification.service";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

// Constants
import { SystemConstants } from "modules/intg/constants/integration.constant";

// lodash
import {
    find,
    map,
} from "lodash";
@Component({
    selector: "intg-connection-events",
    templateUrl: "./intg-connection-events.component.html",
})

export class IntgConnectionEventsComponent {

    @Input() events: IIEvents[];
    @Input() system: IISystemConfigData;
    @Input() connection: IIConnection = {};
    @Output() sendEvents: EventEmitter<IIEvents[]> = new EventEmitter<IIEvents[]>();

    public isUpdatingRoutingKeys = false;
    private connectionId: string = this.stateService.params.id;
    private systemId: string = this.stateService.params.system;
    private routingKeys: IIRoutingKeys[];

    constructor(
        private stateService: StateService,
        public intgApiService: IntgApiService,
        private notificationService: NotificationService,
        private translatePipe: TranslatePipe,
    ) { }

    updateRoutingKeys(eventSelected: boolean, eventId: number, index: number ): void {
        this.isUpdatingRoutingKeys = true;
        let savePromise: ng.IPromise<IProtocolResponse<IIRoutingKeys | void>>;
        const event: IIEvents = find(this.events, { eventId }) as IIEvents;
        if (!eventSelected) {
            savePromise = this.intgApiService.deleteRoutingKey(event.routingKeyId);
        } else {
            const request: IIRoutingKeys = {
                eventId: event.id,
                sourceIntegrationSystemId: SystemConstants.ONETRUST,
                destinationIntegrationSystemId: this.connection.integrationSystemId,
                routingKey: `onetrust.${this.system.meta.name}`,
                systemInstanceId: this.connection.systemInstanceId,
                disabled: false,
            };
            savePromise = this.intgApiService.createRoutingKey(request);
        }
        savePromise.then((response: IProtocolResponse<IIRoutingKeys | void>): void => {
            if (response.result) {
                this.events[index].isEnabled = eventSelected;
                this.getEventsAndRoutingKeys(this.connectionId, this.systemId).then(() => {
                    this.isUpdatingRoutingKeys = false;
                    this.notificationService.alertSuccess(
                        this.translatePipe.transform("Success"),
                        this.translatePipe.transform("EventToggled"),
                    );
                });
            } else {
                this.isUpdatingRoutingKeys = false;
                this.notificationService.alertError(
                    this.translatePipe.transform("Error"),
                    this.translatePipe.transform("EventToggleFailed"),
                );
            }
        });
    }

    private getEventsAndRoutingKeys(connectionId: string, systemId: string): Promise<IProtocolResponse<IIEvents[][]>> {
        const promises: Array<ng.IPromise<IProtocolResponse<IIEvents[] | IIRoutingKeys[]>>> = [
            this.getRoutingKeys(connectionId, systemId),
            this.getEvents(systemId),
        ];
        return Promise
            .all(promises)
            .then((responses: Array<IProtocolResponse<IIEvents[]>>): IProtocolResponse<IIEvents[][]> => {
                if (this.events && this.events.length) {
                    this.formatEvents();
                }
                return {
                    result: Boolean(
                        find(
                            responses,
                            (response: IProtocolResponse<IIEvents[]>): boolean =>
                                !response.result,
                        ),
                    ),
                    data: map(responses, "data"),
                };
            });
    }

    private getEvents(systemId: string): ng.IPromise<IProtocolResponse<IIEvents[]>> {
        return this.intgApiService
            .getIntegrationSystemEvents(systemId)
            .then((response: IProtocolResponse<IIEvents[]>): IProtocolResponse<IIEvents[]> => {
                if (response.result) {
                    this.events = response.data;
                }
                return response;
            });
    }

    private formatEvents(): void {
        this.events = map(this.events, (event: IIEvents): IIEvents => {
            const routingKey: IIRoutingKeys = find(this.routingKeys, {
                eventId: event.id,
            });
            return {
                ...event,
                routingKeyId: routingKey ? routingKey.id : null,
                isEnabled: Boolean(routingKey),
            };
        });
        this.sendEvents.emit(this.events);
    }

    private getRoutingKeys(
        connectionId: string,
        systemId: string,
    ): ng.IPromise<IProtocolResponse<IIRoutingKeys[]>> {
        return this.intgApiService
            .getIntegrationSystemRoutingKeys(connectionId, systemId)
            .then((response: IProtocolResponse<IIRoutingKeys[]>): IProtocolResponse<IIRoutingKeys[]> => {
                if (response.result) {
                    this.routingKeys = response.data;
                }
                return response;
            });
    }
}
