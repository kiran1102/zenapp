// Angular
import {
    Component,
    Input,
    Output,
    EventEmitter,
    OnInit,
    Inject,
    OnChanges,
} from "@angular/core";

// Interfaces
import {
    IIAPIKey,
    IIConnection,
} from "modules/intg/interfaces/integration.interface";
import { ITableColumnConfig } from "interfaces/table-config.interface";
import { IStore } from "interfaces/redux.interface";
import { IDropdownOption } from "interfaces/one-dropdown.interface";
import { IPageableOf } from "interfaces/pagination.interface";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

// services
import { Permissions } from "sharedModules/services/helper/permissions.service";

// Enums
import { TableColumnTypes } from "enums/data-table.enum";

// redux
import { StoreToken } from "tokens/redux-store.token";
import { getOrgNameById } from "oneRedux/reducers/org.reducer";

@Component({
    selector: "intg-api-key-table",
    templateUrl: "./intg-api-key-table.component.html",
})

export class IntgApiKeyTableComponent implements OnChanges {
    @Input()  key: IIAPIKey;
    @Input()  connection: IIConnection;
    @Output()  callback: EventEmitter<{ key: IIAPIKey, action: string }> = new EventEmitter<{ key: IIAPIKey, action: string }>();
    public ready = false;
    public page: IPageableOf<IIConnection>;
    public options: IDropdownOption[];
    readonly columns: ITableColumnConfig[] = [
        {
            name: this.translatePipe.transform("APIKey"),
            sortKey: 1,
            type: TableColumnTypes.Text,
            cellValueKey: "apiKey",
        },
        {
            name: this.translatePipe.transform("Organization"),
            sortKey: 2,
            type: TableColumnTypes.Text,
            cellValueKey: "orgGroup",
        },
        {
            name: "",
            type: TableColumnTypes.Action,
            cellValueKey: "",
        },
    ];

    constructor(
        @Inject(StoreToken) private store: IStore,
        private translatePipe: TranslatePipe,
        private permissions: Permissions,
    ) { }

    public ngOnChanges() {
        if (this.key && this.connection.orgGroupId) {
            this.key.orgGroupId = this.connection.orgGroupId;
            this.key.orgGroup = getOrgNameById(this.key.orgGroupId)(this.store.getState());
            const content = [this.key];
            const size = 20;
            this.page = {
                first: true,
                last: true,
                number: 0,
                numberOfElements: 1,
                size,
                totalElements: 1,
                totalPages: 1,
                content,
            };
            this.options = this.getActions();
            this.ready = true;
        }
    }

    public handleRowAction(key: IIAPIKey, action: string): void {
        this.callback.emit({ key, action });
    }

    public getActions(): IDropdownOption[] {
        const actions: IDropdownOption[] = [];
        actions.push({
            text: this.translatePipe.transform("Copy"),
            action: (): void => {
                this.handleRowAction(this.key, "COPY_CONNECTION");
            },
        });

        if (this.permissions.canShow("IntegrationPageEdit")) {
            actions.push({
                text: this.translatePipe.transform("Delete"),
                action: (): void => {
                    this.handleRowAction(this.key, "DELETE_CONNECTION");
                },
            });
        }

        if (!actions.length) {
            actions.push({
                text: this.translatePipe.transform("NoActionsAvailable"),
                action: (): void => { }, // Keep Empty
            });
        }
        return actions;
    }
}
