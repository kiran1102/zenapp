import {
    Component,
    Inject,
    Input,
    OnInit,
    Output,
    EventEmitter,
} from "@angular/core";
import { StateService } from "@uirouter/core";

// Interfaces
import {
    IIAPIKey,
    IISystemConfigData,
    IIConnection,
    IIConnectionMappingElement,
    IIConfigIpCidr,
} from "modules/intg/interfaces/integration.interface";
import { IStringMap } from "interfaces/generic.interface";
import { IDeleteConfirmationModalResolve } from "interfaces/delete-confirmation-modal-resolve.interface";
import { ITemplate } from "modules/template/interfaces/template.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";
import { ITabsNav } from "interfaces/one-tabs-nav.interface";

// Services
import { IntgApiService } from "intsystemservice/intg-api.service";
import TemplateService from "oneServices/templates.service";
import { ModalService } from "sharedServices/modal.service";
import { IntgConfigService } from "intsystemservice/configuration/intg-configuration.service";
import { NotificationService } from "sharedModules/services/provider/notification.service";
import { IntgEloquaService } from "intsystemservice/intg-eloqua.service";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

// Constants and Utilities
import { SystemConstants, IpWhiteListType } from "modules/intg/constants/integration.constant";
import Utilities from "Utilities";

// Enums
import {
    ConnectionManageTabs,
    ConnectionManageMode,
} from "enums/integration.enum";

// lodash
import {
    cloneDeep,
    filter,
    find,
} from "lodash";

// Models
import { EntityItem } from "crmodel/entity-item";
import { TemplateTypes } from "modules/intg/enums/intg-enum";

@Component({
   selector: "intg-connection-details-advanced",
   templateUrl: "./intg-connection-details-advanced.component.html",
})

export class IntgConnectionDetailsAdvancedComponent implements OnInit {
    @Input() allowedTabs: IStringMap<ITabsNav>;
    @Input() regex: IStringMap<RegExp>;
    @Input() selectedTemplateId: string;

    @Output() toggleIpAddress: EventEmitter<boolean> = new EventEmitter<boolean>();
    @Output() selectTemplateCallback = new EventEmitter<ITemplate>();
    @Output() callback: EventEmitter<IIConfigIpCidr> = new EventEmitter<IIConfigIpCidr>();

    public templatesResponse: ITemplate;
    public inbound: boolean;
    public apiKeysInfo: IIAPIKey;
    public isIpAddress = true;
    public address: string;
    public cidrAddress: string;
    public system: IISystemConfigData;
    public connection: IIConnection = {};
    public isTemplatesLoading = false;
    public isApiKeysInfo = false;
    public templates: ITemplate[];
    public custom: string;
    public inputMappingOptions: EntityItem[];
    public outputMappingOptions: EntityItem[];
    public selectedTemplate: ITemplate;
    public isMappingLoading = false;
    public selectedInputMapping: string;
    public selectedOutputMapping: string;
    public integrationMappings: IIConnectionMappingElement[];

    private connectionId: string = this.stateService.params.id;
    private systemId: string = this.stateService.params.system;
    private manageMode: number;
    private breadcrumbText: string;
    private tabs: ITabsNav[] = [];
    private ready = false;

    constructor(
        private stateService: StateService,
        @Inject("Templates") public Templates: TemplateService,
        public intgApiService: IntgApiService,
        private IntgConfig: IntgConfigService,
        private intgEloquaService: IntgEloquaService,
        private translatePipe: TranslatePipe,
        private modalService: ModalService,
        private notificationService: NotificationService,
    ) { }

   ngOnInit() {
       this.custom = TemplateTypes.Custom.toString();
       this.setupModeEdit();
   }

    public mappingLabel(id: string, isInput: boolean): string {
        const item: EntityItem = find(
            isInput ? this.inputMappingOptions : this.outputMappingOptions,
            (mappingOption: EntityItem): boolean => mappingOption.Id === id);

        return item ? item.Label : "";
    }

    public selectTemplate(selection: ITemplate) {
        this.selectedTemplate = selection;
        this.selectTemplateCallback.emit(selection);

    }

    public selectMapping(selection: string, isInput: boolean) {
        if (isInput) {
            this.selectedInputMapping = selection;
        } else {
            this.selectedOutputMapping = selection;
        }
    }

    public getIpAddress(address: string) {
        if (!this.inbound) return "";
        if (this.isIpAddress) {
            this.cidrAddress = null;
            this.address = address;
            const ipconfig = {
                address: this.address,
                type: IpWhiteListType.IP_ADDRESS,
                inbound: this.inbound,
            };
            this.callback.emit(ipconfig);
        } else {
            this.address = null;
            this.cidrAddress = address;
            const cidrconfig = {
                address: this.cidrAddress,
                type: IpWhiteListType.CIDR_ADDRESS,
                inbound: this.inbound,
            };
            this.callback.emit(cidrconfig);
        }
    }

    public addMapping() {
        if (!this.integrationMappings) {
            this.integrationMappings = [];
        }

        const hasMapping = find(this.integrationMappings, (intgMapping: IIConnectionMappingElement) =>
            mapping.InputId === this.selectedInputMapping && intgMapping.OutputId === this.selectedOutputMapping);

        if (hasMapping) {
            this.notificationService.alertError(this.translatePipe.transform("IntegrationMappingExistsTitle"), this.translatePipe.transform("IntegrationMappingExistsText"));
            return;
        }

        const mapping: IIConnectionMappingElement = {
            InputId: this.selectedInputMapping,
            OutputId: this.selectedOutputMapping,
        };
        this.integrationMappings.push(mapping);
        this.selectedInputMapping = null;
        this.selectedOutputMapping = null;
    }

    public toggleIpAddressType(toggle: boolean) {
        this.isIpAddress = toggle;
        this.toggleIpAddress.emit(this.isIpAddress);
    }

    public toggleConnectionInbound() {
        this.inbound = !this.inbound;
    }

    public handleApiKey(event: { key: IIAPIKey, action: string }) {
        if (event.action === "COPY_CONNECTION") {
            this.copyApiKey();
        } else if (event.action === "DELETE_CONNECTION") {
            this.deleteApiKey();
        }
    }

    trackByIndex(index: number): number {
        return index;
    }

    private setupModeEdit() {
        this.ready = false;
        this.manageMode = ConnectionManageMode.EDIT;
        this.tabs.push(this.allowedTabs[ConnectionManageTabs.AUTHENTICATION]);
        this.system = this.getSystemById(this.systemId);
        if (this.system && SystemConstants.JIRA) {
               this.getTemplateList(this.custom.toString());
        }
        if (this.system.meta.ShowAdvancedView) {
            if (this.systemId === SystemConstants.ELOQUA) {
                this.tabs.push(this.allowedTabs[ConnectionManageTabs.ADVANCED_PREFERENCE]);
            } else {
                this.tabs.push(this.allowedTabs[ConnectionManageTabs.ADVANCED]);
            }
        }

        if (this.systemId === SystemConstants.ELOQUA) {
             this.getEloquaMappings();
             this.tabs.push(this.allowedTabs[ConnectionManageTabs.DATA_SUBJECT_ELEMENTS]);
             this.tabs.push(this.allowedTabs[ConnectionManageTabs.DATA_SUBJECT_PROFILE]);
        }

        const promises: Array<ng.IPromise<IProtocolResponse<IIConnection | IIAPIKey>>> = [
            this.getConnection(this.connectionId, this.systemId),
            this.getApiKeys(this.connectionId, this.systemId),
        ];
        return Promise
            .all(promises)
            .then(() => {
                if (this.connection && this.system) {
                    this.ready = true;
                }
            });
    }

    private getApiKeys(connectionId: string, systemId: string): ng.IPromise<IProtocolResponse<IIAPIKey>> {
        return this.intgApiService
            .getApiKeys(this.connectionId, this.systemId)
            .then((response: IProtocolResponse<IIAPIKey>): IProtocolResponse<IIAPIKey> => {
                if (response.result) {
                    this.apiKeysInfo = response.data || null;
                    this.setInboundAddresses(this.apiKeysInfo);
                }
                this.isApiKeysInfo = true;
                return response;
            });
    }

    private setInboundAddresses(apiKey: IIAPIKey) {
        if (!apiKey) return;
        this.inbound = Boolean(apiKey.onPrem);
        this.isIpAddress = !Utilities.matchRegex(
            apiKey.ipWhiteList,
            this.regex.CIDR_MULTI,
        );
        if (this.isIpAddress) {
            this.address = apiKey.ipWhiteList;
        } else {
            this.cidrAddress = apiKey.ipWhiteList;
        }
    }

    private getSystemById(systemId: string): IISystemConfigData {
        return cloneDeep(this.IntgConfig.getSystemById(
            systemId,
        ) as IISystemConfigData);
    }

    private getConnection(connectionId: string, systemId: string): ng.IPromise<IProtocolResponse<IIConnection>> {
        return this.intgApiService
            .getConnectionById(this.connectionId, this.systemId)
            .then((response: IProtocolResponse<IIConnection>): IProtocolResponse<IIConnection> => {
                if (response.result) {
                    this.connection = response.data;
                    this.systemId = this.connection.integrationSystemId;
                    this.system = this.getSystemById(this.systemId);
                    this.breadcrumbText =
                        this.system.meta.displayName ||
                        (this.system.meta.nameKey
                            ? this.translatePipe.transform(this.system.meta.nameKey)
                            : this.system.meta.name);
                } else {
                    this.connection = null;
                    this.system = null;
                }
                return response;
            });
    }

    private copyApiKey() {
        this.apiKeysInfo.disabled = false;
        Utilities.copyToClipboard(this.apiKeysInfo.apiKey);
        this.notificationService.alertSuccess(
            this.translatePipe.transform("Success"),
            this.translatePipe.transform("APIKeyCopied"),
        );

    }

    private deleteApiKey() {
        const apiKeyFields: IIAPIKey = {
            id: this.apiKeysInfo.id,
            disabled: "true",
        };
        const deleteApiKeyModal: IDeleteConfirmationModalResolve = {
            modalTitle: this.translatePipe.transform("Disable"),
            submitButtonText: this.translatePipe.transform("Disable"),
            confirmationText: this.translatePipe.transform("DisableApiKey"),
            cancelButtonText: this.translatePipe.transform("Cancel"),
            promiseToResolve: (): ng.IPromise<boolean> =>
                this.intgApiService.patchApiKey(apiKeyFields).then((response: IProtocolResponse<IIAPIKey>): boolean => {
                    if (response.result) {
                        this.modalService.closeModal();
                        this.modalService.clearModalData();
                        this.notificationService.alertSuccess(
                            this.translatePipe.transform("Success"),
                            this.translatePipe.transform("ClearAllKeys"),
                        );
                        this.reloadView();
                    }
                    return response.result;
                }),
        };
        this.modalService.setModalData(deleteApiKeyModal);
        this.modalService.openModal("deleteConfirmationModal");
    }

    private reloadView() {
        this.stateService.go(
            "zen.app.pia.module.intg.connections.manage",
            {
                id: this.connectionId,
                system: this.systemId,
                time: new Date().getTime(),
            },
            { location: "replace" },
        );
    }

    private getTemplateList(templateType: string): ng.IPromise<void> | undefined {
        if (!this.system.form.showTemplatesList) return;
        this.isTemplatesLoading = true;
        return this.Templates.publishedList(templateType).then((response: IProtocolResponse<any>): void => {
            if (response.result) {
                this.templates = filter(response.data, (template: ITemplate): boolean => {
                    return template && !template.HasParents;
                });
                this.selectedTemplate = find(this.templates, (template: ITemplate): boolean => template.Id === this.selectedTemplateId) || null;
            }
            this.isTemplatesLoading = false;
        });
    }

    private getEloquaMappings(): void {
        if (!this.system.form.showIntegrationMapping) {
            return;
        }
        this.isMappingLoading = true;
        this.intgEloquaService.getEloquaMappingOptions(this.connectionId).then((options: [EntityItem[], EntityItem[]]): void => {
            this.inputMappingOptions = options[0];
            this.outputMappingOptions = options[1];
            this.isMappingLoading = false;
        });
    }
}
