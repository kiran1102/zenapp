// Angular.
import {
    Component,
    Input,
    Output,
    EventEmitter,
} from "@angular/core";

// Interfaces
import { IFormFieldInput } from "interfaces/form-field.interface";

// Constants
import {
    FormDataTypes,
} from "constants/form-types.constant";

@Component({
    selector: "intg-connection-form",
    templateUrl: "./intg-connection-form.component.html",
})
export class IntgConnectionFormComponent {
    @Input() formName: string;
    @Input() formInputFields: IFormFieldInput[];
    @Input() formIsDisabled: boolean;

    @Output() handleAction =  new EventEmitter();

    public formDataTypes = FormDataTypes;

    handleFormAction(field: IFormFieldInput) {
        this.handleAction.emit(field);
    }
};
