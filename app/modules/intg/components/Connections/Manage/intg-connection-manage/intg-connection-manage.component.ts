import {
    Component,
    Inject,
    OnInit,
} from "@angular/core";
import { StateService } from "@uirouter/core";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

// lodash
import {
    cloneDeep,
} from "lodash";

// Services
import { NotificationService } from "sharedModules/services/provider/notification.service";
import { ModalService } from "sharedServices/modal.service";
import { IntgApiService } from "intsystemservice/intg-api.service";

// Enums
import { ConnectionManageMode } from "enums/integration.enum";

// Interfaces
import {
    IISystemConfigData,
    IIConnection,
} from "modules/intg/interfaces/integration.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";
import { IDeleteConfirmationModalResolve } from "interfaces/delete-confirmation-modal-resolve.interface";
import { IntgConfigService } from "intsystemservice/configuration/intg-configuration.service";

@Component({
    selector: "intg-connection-manage",
    templateUrl: "./intg-connection-manage.component.html",
 })

 export class IntgConnectionManageComponent implements OnInit {
    MODES = ConnectionManageMode;
    breadcrumbHeader: string;
    breadcrumbText: string;
    ready: boolean;
    manageMode: number;
    system: IISystemConfigData;
    connection: IIConnection = {};

    private systemId: string = this.stateService.params.system;
    private connectionId: string = this.stateService.params.id;

    constructor(
        private stateService: StateService,
        private intgApiService: IntgApiService,
        private intgConfigService: IntgConfigService,
        private translatePipe: TranslatePipe,
        private notificationService: NotificationService,
        private modalService: ModalService,
    ) { }

    ngOnInit() {
        if (this.connectionId) {
            this.setupModeEdit();
        } else if (this.systemId) {
            this.setupModeCreate();
        } else {
            this.setupModeSystems();
        }
    }

     goToRoute(route: { path: string, params: any }) {
        this.stateService.go(route.path);
    }

    deleteConnection(): void {
        const modalData: IDeleteConfirmationModalResolve = {
            modalTitle: this.translatePipe.transform("DeleteConnection"),
            submitButtonText: this.translatePipe.transform("Delete"),
            confirmationText: this.translatePipe.transform("DeleteConnectionMessage"),
            cancelButtonText: this.translatePipe.transform("Cancel"),
            promiseToResolve: (): ng.IPromise<boolean> =>
                this.intgApiService.deleteConnection(this.connectionId).then(
                    (response: IProtocolResponse<void>): boolean => {
                        if (response.result) {
                            this.notificationService.alertSuccess(
                                this.translatePipe.transform("Success"),
                                this.translatePipe.transform("SuccessfullyDeletedtheConnection"),
                            );
                            this.stateService.go("zen.app.pia.module.intg.connections.list");
                        }
                        return response.result;
                    },
                ),
        };
        this.modalService.setModalData(modalData);
        this.modalService.openModal("otDeleteConfirmationModal");
    }

     private setupModeEdit() {
        this.ready = false;
        this.manageMode = ConnectionManageMode.EDIT;
        this.breadcrumbHeader = this.translatePipe.transform("ConnectionDetails");
        this.system = this.getSystemById(this.systemId);
        this.getConnection(this.connectionId, this.systemId);

    }

    private setupModeSystems(): void {
        this.ready = false;
        this.manageMode = ConnectionManageMode.SYSTEMS;
        this.breadcrumbHeader = this.translatePipe.transform("Connections");
        this.breadcrumbText = this.translatePipe.transform("AddNew");
        this.ready = true;
    }

    private getSystemById(systemId: string): IISystemConfigData {
        return cloneDeep(this.intgConfigService.getSystemById(
            systemId,
        ) as IISystemConfigData);
    }

    private getConnection(connectionId: string, systemId: string): ng.IPromise<IProtocolResponse<IIConnection>> {
        return this.intgApiService
            .getConnectionById(this.connectionId, this.systemId)
            .then((response: IProtocolResponse<IIConnection>): IProtocolResponse<IIConnection> => {
                if (response.result) {
                    this.connection = response.data;
                    this.systemId = this.connection.integrationSystemId;
                    this.system = this.getSystemById(this.systemId);
                    this.ready = true;
                    this.breadcrumbText =
                        this.system.meta.displayName ||
                        (this.system.meta.nameKey
                            ? this.translatePipe.transform(this.system.meta.nameKey)
                            : this.system.meta.name);
                } else {
                    this.connection = null;
                    this.system = null;
                }
                return response;
            });
    }

    private setupModeCreate() {
        this.ready = false;
        this.manageMode = ConnectionManageMode.CREATE;
        this.breadcrumbHeader = this.translatePipe.transform("ConnectionDetails");
        this.system = this.getSystemById(this.systemId);
        this.breadcrumbText = this.system.meta.nameKey
            ? this.translatePipe.transform(this.system.meta.nameKey)
            : this.system.meta.name;
        this.ready = true;
    }
}
