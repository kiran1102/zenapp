import {
    Component,
    OnInit,
    Inject,
    Input,
} from "@angular/core";
import { StateService } from "@uirouter/core";

// Interface
import {
    IISystem,
    IIConnection,
    IIPage,
} from "modules/intg/interfaces/integration.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";
import { IKeyValue } from "interfaces/generic.interface";

// Service
import { IntgConfigService } from "intsystemservice/configuration/intg-configuration.service";
import { IntgApiService } from "intsystemservice/intg-api.service";

// lodash
import {
    startCase,
    remove,
    map,
} from "lodash";

// Constant
import {
    SystemListFilters,
    SystemConstants,
} from "modules/intg/constants/integration.constant";

// Pipe
import { TranslatePipe } from "modules/pipes/translate.pipe";
import { FilterPipe } from "modules/pipes/filter.pipe";

@Component({
    selector: "intg-connection-manage-select-system",
    templateUrl: "./intg-connection-manage-select-system.component.html",
})

export class IntgConnectionManageSelectComponent implements OnInit {

    @Input() system: IISystem;
    selectedSystem: IISystem;
    systems: IISystem[];
    options: IISystem[];
    loading: boolean;

    constructor(
        private stateService: StateService,
        public readonly intgApiService: IntgApiService,
        private readonly IntgConfig: IntgConfigService,
        private readonly translatePipe: TranslatePipe,
        private readonly filterPipe: FilterPipe,
    ) { }

    ngOnInit() {
        this.loading = true;
        this.systems = map(this.IntgConfig.getSystemsList(SystemListFilters.ADD_CONNECTION), (system: IISystem): IISystem => {
            system.name = startCase(system.nameKey ? this.translatePipe.transform(system.nameKey) : system.name);
            return system;
        });
        this.getSystemConnections(SystemConstants.SLACK).then((response: IIConnection[]): void => {
            if (response.length) {
                this.systems = remove(this.systems, (system: IISystem): boolean => {
                    return (system.id !== SystemConstants.SLACK && system.isConnectable);
                });
            }
            this.options = [...this.systems];
            this.loading = false;
        });
    }

    createConnection(): void {
        this.stateService.go(".", {
            system: this.selectedSystem.id,
            id: null,
        });
    }

    selectConnectionType({ currentValue }): void {
        this.selectedSystem = currentValue;
    }

    filterOptions(filter: IKeyValue<string>) {
        this.options = this.filterPipe.transform(this.systems, ["name"], filter.value);
    }

    goToRoute() {
        this.stateService.go("zen.app.pia.module.intg.connections.list");
    }

    private getSystemConnections(systemId: string): ng.IPromise<IIConnection[]> {
        return this.intgApiService.getConnection("", systemId).then((response: IProtocolResponse<IIPage<IIConnection>>): IIConnection[] => {
            return response.result && response.data && response.data.content ? response.data.content : [];
        });
    }
}
