import {
    Component,
    OnInit,
} from "@angular/core";
import { StateService } from "@uirouter/core";

// Interfaces
import {
    IISystemConfigData,
    IIAPIKey,
    IIConnection,
    IIEvents,
    IIRoutingKeys,
    IIConnectionProperty,
    IIContactMappingElement,
    IISubjectProfileMapping,
    IIPreferencesId,
    IIConfigIpCidr,
    IIPreferenceCdoPurposeMap,
    IInstallApiKeyModalResolve,
    IISubjectProfileMappingElement,
    IIntgPreferenecePurpose,
    IIDeleteIndex,
 } from "modules/intg/interfaces/integration.interface";
import { ITabsNav } from "interfaces/one-tabs-nav.interface";
import { IStringMap } from "interfaces/generic.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";
import { IFormFieldInput } from "interfaces/form-field.interface";
import { ITemplate } from "modules/template/interfaces/template.interface";

// Services
import { IntgEloquaService } from "intsystemservice/intg-eloqua.service";
import { ModalService } from "sharedServices/modal.service";
import { NotificationService } from "sharedModules/services/provider/notification.service";
import { IntgConfigService } from "intsystemservice/configuration/intg-configuration.service";
import { IntgApiService } from "intsystemservice/intg-api.service";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

// Constants and Utilities
import Utilities from "Utilities";
import { FormDataTypes } from "constants/form-types.constant";
import { Regex } from "constants/regex.constant";
import {
    SystemConstants,
    IpWhiteListType,
} from "modules/intg/constants/integration.constant";

// Enums
import {
    ConnectionManageTabs,
    ConnectionManageMode,
    ConnectionMappingPrefixes,
} from "enums/integration.enum";

// lodash
import {
    cloneDeep,
    map,
    find,
    isArray,
    each,
    isNil,
    findIndex,
    uniqBy,
} from "lodash";

@Component({
   selector: "intg-connection-manage-body",
   templateUrl: "./intg-connection-manage-body.component.html",
})

export class IntgConnectionManageBodyComponent implements OnInit {
    overflowText: string;
    templatesResponse: ITemplate;
    isSavingConnection = false;
    apiKeysInfo: IIAPIKey;
    tabs: ITabsNav[] = [];
    selectedTab: string = ConnectionManageTabs.AUTHENTICATION;
    system: IISystemConfigData;
    connection: IIConnection = {};
    manageMode: number;
    formIsDisabled = false;
    ready = false;
    events: IIEvents[];
    saveActionText: string;
    regex: IStringMap<RegExp>;
    integrationContactMappings: IIContactMappingElement[];
    integrationDataSubProfileMap: IISubjectProfileMapping[];
    integrationPreferenceData: IIPreferencesId[];
    TABS = ConnectionManageTabs;
    allowedTabs: IStringMap<ITabsNav> = {
        AUTHENTICATION: {
            id: this.TABS.AUTHENTICATION,
            text: this.translatePipe.transform("General"),
            action: (option: ITabsNav): void => {
                this.selectedTab = option.id;
            },
        },
        API_DEFINITION: {
            id: this.TABS.API_DEFINITION,
            text: this.translatePipe.transform("Events"),
            action: (option: ITabsNav): void => {
                this.selectedTab = option.id;
            },
        },
        ADVANCED: {
            id: this.TABS.ADVANCED,
            text: this.translatePipe.transform("Advanced"),
            action: (option: ITabsNav): void => {
                this.selectedTab = option.id;
            },
        },
        DATA_SUBJECT_ELEMENTS: {
            id: this.TABS.DATA_SUBJECT_ELEMENTS,
            text: this.translatePipe.transform("DataSubjectElements"),
            action: (option: ITabsNav): void => {
                this.selectedTab = option.id;
            },
        },
        DATA_SUBJECT_PROFILE: {
            id: this.TABS.DATA_SUBJECT_PROFILE,
            text: this.translatePipe.transform("DataSubjectProfile"),
            action: (option: ITabsNav): void => {
                this.selectedTab = option.id;
            },
        },
        ADVANCED_PREFERENCE: {
            id: this.TABS.ADVANCED_PREFERENCE,
            text: this.translatePipe.transform("Preferences"),
            action: (option: ITabsNav): void => {
                this.selectedTab = option.id;
            },
        },
    };

    private systemId: string = this.stateService.params.system;
    private connectionId: string = this.stateService.params.id;
    private routingKeys: IIRoutingKeys[];
    private selectedInputContactMapping: string;
    private selectedOutputContactMapping: string;
    private inbound: boolean;
    private isIpAddress = true;
    private address: string;
    private cidrAddress: string;
    private selectedTemplate: string;
    private MODES = ConnectionManageMode;
    private selectedInputMapping: string;

    constructor(
        private stateService: StateService,
        private intgApiService: IntgApiService,
        private IntgConfig: IntgConfigService,
        private intgEloquaService: IntgEloquaService,
        private translatePipe: TranslatePipe,
        private notificationService: NotificationService,
        private modalService: ModalService,
    ) {
        this.regex = Regex;
    }

    ngOnInit() {
        this.overflowText = "More";
        if (this.connectionId) {
            this.setupModeEdit();
        } else if (this.systemId) {
            this.setupModeCreate();
        } else {
           this.setupModeSystems();
        }
        this.formIsDisabled = this.systemId === SystemConstants.SLACK && this.manageMode === this.MODES.EDIT;
    }

    onTabClick(value: ITabsNav) {
        this.selectedTab = value.id;
    }

    getAdressAndCidr(valueObj: IIConfigIpCidr) {
        this.inbound = valueObj.inbound;
        if (valueObj.type === IpWhiteListType.IP_ADDRESS) {
            this.cidrAddress = null;
            this.address = valueObj.address;
        } else if (valueObj.type === IpWhiteListType.CIDR_ADDRESS) {
            this.address = null;
            this.cidrAddress = valueObj.address;
        }
    }

    toggleIpAddressType(toggle: boolean) {
        this.isIpAddress = toggle;
    }

    contactMappingOutValues(contactMappingValues: IIContactMappingElement) {
        this.selectedInputContactMapping = contactMappingValues.dsElementId;
        this.selectedOutputContactMapping = contactMappingValues.contactId;
        this.addContactMapping();
    }

    removeContactMapping(index: number) {
        this.integrationContactMappings.splice(index, 1);
        this.integrationContactMappings = [...this.integrationContactMappings];
    }

    setEvents(event: IIEvents[]) {
        this.events = event;
    }

    saveConnection() {
        this.isSavingConnection = true;
        const request: IIConnection = this.buildRequestFromFields(
            {
                name: this.connection.name,
                integrationSystemId: this.systemId,
                host: "",
                port: "",
            },
            this.getAllFormFieldKeys(),
        );
        let savePromise: ng.IPromise<IProtocolResponse<IIConnection>>;
        let apiKeyPromise: ng.IPromise<IProtocolResponse<IIAPIKey>>;
        if (this.systemId === SystemConstants.JIRA && !isNil(this.templatesResponse)) {
            request.properties = [
                {
                    pname: "onetrust.default.template",
                    pvalue: this.templatesResponse.Id,
                },
            ];
        }

        if (this.systemId === SystemConstants.ELOQUA) {
            request.properties = [];

            if (!isNil(this.integrationPreferenceData)) {
                const integrationPreferenceMappingProperties = this.integrationPreferenceData
                    .map((mapping: IIPreferencesId): IIConnectionProperty => {
                        return {
                            pname: `${ConnectionMappingPrefixes.ELOQUA_PREFERENCE_PURPOSE}${mapping.purposeId}${ConnectionMappingPrefixes.ELOQUA_PREFERENCE_CDO}${mapping.cdoId}`,
                            pvalue: mapping.topicAttributes ? JSON.stringify(mapping.topicAttributes.reduce((items: IStringMap<string>, subArray: IIPreferenceCdoPurposeMap): IStringMap<string> => {
                                return {
                                    ...items,
                                    [subArray.purposeTopicId]: String(subArray.cdoAttributesId),
                                };
                            }, {})) : "",
                        };
                    });
                request.properties = request.properties.concat(integrationPreferenceMappingProperties);
            }

            if (!isNil(this.integrationContactMappings)) {
                const integrationContactMappingProperties = this.integrationContactMappings
                    .reduce((mapping: IStringMap<string>, contactMapping: IIContactMappingElement): IStringMap<string> => {
                        return {
                            ...mapping,
                            [contactMapping.dsElementId]: String(contactMapping.contactId),
                        };
                    }, {});

                request.properties = request.properties.concat({
                    pname: ConnectionMappingPrefixes.ELOQUA_CONTACT_MAPPING,
                    pvalue: JSON.stringify(integrationContactMappingProperties),
                });
            }

            if (!isNil(this.integrationDataSubProfileMap)) {
                const integrationMappingProperties = this.integrationDataSubProfileMap
                    .map((mapping: IISubjectProfileMapping): IIConnectionProperty => {
                        return {
                            pname: `${ConnectionMappingPrefixes.ELOQUA_DATASUBJECT_PROFILE}${mapping.purposeId}`,
                            pvalue: JSON.stringify(mapping.contactMappings.reduce((items: IStringMap<string>, contactsPurposeValue: IISubjectProfileMappingElement): IStringMap<string> => {
                                return {
                                    ...items,
                                    [contactsPurposeValue.dsElementId]: String(contactsPurposeValue.contactId),
                                };
                            }, {})),
                        };
                    });

                request.properties = request.properties.concat(integrationMappingProperties);
            }
        }

        const saveMessage: string = this.connectionId
            ? this.translatePipe.transform("UpdatedConnection")
            : this.translatePipe.transform("CreatedConnection");
        if (this.connectionId) {
            request.systemInstanceId = this.connectionId;
            request.integrationSystemId = this.systemId;
            request.createdBy = this.connection.createdBy;
            request.createdDate = this.connection.createdDate;
            savePromise = this.intgApiService.updateConnection(
                request,
                this.connection.id,
            );
        } else if (this.systemId === SystemConstants.SLACK) {
            return this.createSlackConnection();
        } else {
            savePromise = this.intgApiService.createConnection(request);
        }
        savePromise.then((response: IProtocolResponse<IIConnection>): void => {
            if (response.result) {
                this.connectionId = response.data
                    ? response.data.systemInstanceId
                    : this.connectionId;
                apiKeyPromise = this.upsertApiKey();
                if (apiKeyPromise) {
                    apiKeyPromise.then((apiKeyResponse: IProtocolResponse<IIAPIKey>): void => {
                        this.notificationService.alertSuccess(
                            this.translatePipe.transform("Success"),
                            saveMessage,
                        );
                        this.reloadView();
                        if (apiKeyResponse.data) {
                            this.installConnection(apiKeyResponse.data.apiKey);
                        }
                        this.isSavingConnection = false;
                    });
                } else {
                    this.notificationService.alertSuccess(
                        this.translatePipe.transform("Success"),
                        saveMessage,
                    );
                    this.reloadView();
                    this.isSavingConnection = false;
                }
            } else {
                this.isSavingConnection = false;
            }
        });
    }

    addDataSubjectProfile(dataSubjProfMap: IISubjectProfileMappingElement) {
        if (!this.integrationDataSubProfileMap) {
            this.integrationDataSubProfileMap = [];
        }
        const purposeMap = find(this.integrationDataSubProfileMap,
            (dataSubjProfileMap: IISubjectProfileMapping) => dataSubjProfileMap.purposeId === dataSubjProfMap.purposeId);

        if (purposeMap) {
            purposeMap.contactMappings = purposeMap.contactMappings || [];
            const contactMapping = find(purposeMap.contactMappings,
                (contMapping: IIContactMappingElement) => contMapping.contactId === dataSubjProfMap.contactId && contMapping.dsElementId === dataSubjProfMap.dsElementId);

            if (contactMapping) {
                this.notificationService.alertError(this.translatePipe.transform("IntegrationMappingExistsTitle"), this.translatePipe.transform("IntegrationMappingExistsText"));
                return;
            }
            purposeMap.contactMappings.push({
                contactId: dataSubjProfMap.contactId,
                dsElementId: dataSubjProfMap.dsElementId,
            });
        } else {
            this.integrationDataSubProfileMap.push({
                purposeId: dataSubjProfMap.purposeId,
                contactMappings: [{
                    contactId: dataSubjProfMap.contactId,
                    dsElementId: dataSubjProfMap.dsElementId,
                }],
            });
        }

        this.integrationDataSubProfileMap = [...this.integrationDataSubProfileMap];
    }

    removeDataSubjProfMapping(index: IIDeleteIndex) {

        this.integrationDataSubProfileMap[index.purposeIndex].contactMappings.splice(index.contactIndex, 1);
        if (this.integrationDataSubProfileMap[index.purposeIndex].contactMappings.length === 0) {
            this.integrationDataSubProfileMap.splice(index.purposeIndex, 1);
        }
        this.integrationDataSubProfileMap = [...this.integrationDataSubProfileMap];
    }

    deleteSelectedPreferenceRow(selectedValues: IIntgPreferenecePurpose) {
        const preferencesValueIndex = findIndex(this.integrationPreferenceData, (values: IIPreferencesId): boolean => {
            return values.purposeId === selectedValues.purposeId.Id;
        });
        const intgDataSubjProfileIndex = findIndex(this.integrationDataSubProfileMap, (values: IISubjectProfileMapping) => {
            return values.purposeId === selectedValues.purposeId.Id;
        });
        this.integrationPreferenceData.splice(preferencesValueIndex, 1);
        if (intgDataSubjProfileIndex >= 0) {
            this.integrationDataSubProfileMap.splice(intgDataSubjProfileIndex, 1);
        }
        this.saveConnection();
    }

    intgPreferenceEditMapping(editPreference: IIPreferencesId) {

        if (editPreference.topicAttributes) {
            const topics = uniqBy(editPreference.topicAttributes, (purposeTopics: IIPreferenceCdoPurposeMap) => {
                return purposeTopics.purposeTopicId;
            });
            const attributes = uniqBy(editPreference.topicAttributes, (values: IIPreferenceCdoPurposeMap) => {
                return values.cdoAttributesId;
            });
            if (topics.length !== editPreference.topicAttributes.length) {
                this.notificationService.alertError(this.translatePipe.transform("IntegrationTopicsMappingExists"), this.translatePipe.transform("IntegrationMappingExistsText"));
                return;
            }
            if (attributes.length !== editPreference.topicAttributes.length) {
                this.notificationService.alertError(this.translatePipe.transform("IntegrationAttributesMappingExists"), this.translatePipe.transform("IntegrationMappingExistsText"));
                return;
            }
            const intgPreferenceEditValueIndex = findIndex(this.integrationPreferenceData, (items: IIPreferencesId) => {
                return items.purposeId === editPreference.purposeId;
            });
            if (intgPreferenceEditValueIndex < 0) return;
            this.integrationPreferenceData[intgPreferenceEditValueIndex] = editPreference;
        } else {
            const intgPreferenceEditValueIndex = findIndex(this.integrationPreferenceData, (items: IIPreferencesId) => {
                return items.purposeId === editPreference.purposeId;
            });
            this.integrationPreferenceData[intgPreferenceEditValueIndex] = editPreference;
        }
        this.saveConnection();
        this.modalService.closeModal();
    }

    intgPreferenceMapping(intgPreferenceIds: IIPreferencesId) {
        if (!this.integrationPreferenceData) {
            this.integrationPreferenceData = [];
        }
        this.integrationPreferenceData.push({
            purposeId: intgPreferenceIds.purposeId,
            cdoId: intgPreferenceIds.cdoId,
        });
        if (intgPreferenceIds.topicAttributes) {
            const topics = uniqBy(intgPreferenceIds.topicAttributes, (values: IIPreferenceCdoPurposeMap) => {
                return values.purposeTopicId;
            });
            const attributes = uniqBy(intgPreferenceIds.topicAttributes, (values: IIPreferenceCdoPurposeMap) => {
                return values.cdoAttributesId;
            });
            if (topics.length !== intgPreferenceIds.topicAttributes.length) {
                this.notificationService.alertError(this.translatePipe.transform("IntegrationTopicsMappingExists"), this.translatePipe.transform("IntegrationMappingExistsText"));
                return;
            }
            if (attributes.length !== intgPreferenceIds.topicAttributes.length) {
                this.notificationService.alertError(this.translatePipe.transform("IntegrationAttributesMappingExists"), this.translatePipe.transform("IntegrationMappingExistsText"));
                return;
            }
            const topicAttributes = intgPreferenceIds.topicAttributes.map((values: IIPreferenceCdoPurposeMap) => values);

            this.integrationPreferenceData.forEach((obj: IIPreferencesId) => {
                if (obj.purposeId === intgPreferenceIds.purposeId) {
                    return obj.topicAttributes = topicAttributes;
                }
            });
        }
        this.saveConnection();
        this.modalService.closeModal();
    }

    private setupModeEdit(): Promise<void> {
        this.ready = false;
        this.manageMode = ConnectionManageMode.EDIT;
        this.tabs.push(this.allowedTabs[ConnectionManageTabs.AUTHENTICATION]);
        this.system = this.getSystemById(this.systemId);
        if (this.system.meta.ShowAdvancedView) {
            if (this.systemId === SystemConstants.ELOQUA) {
                this.tabs.push(this.allowedTabs[ConnectionManageTabs.ADVANCED_PREFERENCE]);
            } else {
                this.tabs.push(this.allowedTabs[ConnectionManageTabs.ADVANCED]);
            }
        }

        if (this.systemId === SystemConstants.ELOQUA) {
            this.tabs.push(this.allowedTabs[ConnectionManageTabs.DATA_SUBJECT_ELEMENTS]);
            this.tabs.push(this.allowedTabs[ConnectionManageTabs.DATA_SUBJECT_PROFILE]);
        }

        const promises: any = [
            this.getConnection(this.connectionId, this.systemId),
            this.getApiKeys(this.connectionId, this.systemId),
            this.getEventsAndRoutingKeys(this.connectionId, this.systemId),
        ];

        return Promise
            .all(promises)
            .then((): void => {
                if (this.events && this.events.length) {
                    this.formatEvents();
                    this.tabs.push(
                        this.allowedTabs[ConnectionManageTabs.API_DEFINITION],
                    );
                }
                if (this.connection && this.system) {
                    this.populateFields();
                }
                this.saveActionText =
                    this.connectionId && this.apiKeysInfo
                        ? this.translatePipe.transform("Save")
                        : this.translatePipe.transform("Install");
                this.ready = true;
            });
    }

    private setupModeCreate() {
        this.ready = false;
        this.saveActionText = this.translatePipe.transform("Install");
        this.manageMode = ConnectionManageMode.CREATE;
        this.system = this.getSystemById(this.systemId);
        this.tabs.push(this.allowedTabs[ConnectionManageTabs.AUTHENTICATION]);
        if (this.system.meta.ShowAdvancedView) {
            this.tabs.push(this.allowedTabs[ConnectionManageTabs.ADVANCED]);
        }
        this.ready = true;
    }

    private setupModeSystems() {
        this.ready = false;
        this.manageMode = ConnectionManageMode.SYSTEMS;
        this.ready = true;
    }

    private getSystemById(systemId: string): IISystemConfigData {
        return cloneDeep(this.IntgConfig.getSystemById(
            systemId,
        ) as IISystemConfigData);
    }

    private getConnection(connectionId: string, systemId: string): ng.IPromise<IProtocolResponse<IIConnection>> {
        return this.intgApiService
            .getConnectionById(this.connectionId, this.systemId)
            .then((response: IProtocolResponse<IIConnection>): IProtocolResponse<IIConnection> => {
                if (response.result) {
                    this.connection = response.data;
                    this.systemId = this.connection.integrationSystemId;
                    this.system = this.getSystemById(this.systemId);
                } else {
                    this.connection = null;
                    this.system = null;
                }
                return response;
            });
    }

    private getApiKeys(connectionId: string, systemId: string): ng.IPromise<IProtocolResponse<IIAPIKey>> {
        return this.intgApiService
            .getApiKeys(this.connectionId, this.systemId)
            .then((response: IProtocolResponse<IIAPIKey>): IProtocolResponse<IIAPIKey> => {
                if (response.result) {
                    this.apiKeysInfo = response.data || null;
                    this.setInboundAddresses(this.apiKeysInfo);
                }
                return response;
            });
    }

    private setInboundAddresses(apiKey: IIAPIKey) {
        if (!apiKey) return;
        this.inbound = Boolean(apiKey.onPrem);
        this.isIpAddress = !Utilities.matchRegex(
            apiKey.ipWhiteList,
            this.regex.CIDR_MULTI,
        );
        if (this.isIpAddress) {
            this.address = apiKey.ipWhiteList;
        } else {
            this.cidrAddress = apiKey.ipWhiteList;
        }
    }

    private getEventsAndRoutingKeys(connectionId: string, systemId: string): Promise<IProtocolResponse<Array<IIRoutingKeys[] | IIEvents[]>>> {
        const promises: Array<ng.IPromise<IProtocolResponse<IIRoutingKeys[] | IIEvents[]>>> = [
            this.getRoutingKeys(connectionId, systemId),
            this.getEvents(systemId),
        ];
        return Promise
            .all(promises)
            .then((responses: Array<IProtocolResponse<IIRoutingKeys[] | IIEvents[]>>): IProtocolResponse<Array<IIRoutingKeys[] | IIEvents[]>> => {
                if (this.events && this.events.length) {
                    this.formatEvents();
                }
                return {
                    result: Boolean(
                        find(responses, (response: IProtocolResponse<IIRoutingKeys[] | IIEvents[]>) => !response.result),
                    ),
                    data: map(responses, "data"),
                };
            });
    }

    private getRoutingKeys(connectionId: string, systemId: string): ng.IPromise<IProtocolResponse<IIRoutingKeys[]>> {
        return this.intgApiService
            .getIntegrationSystemRoutingKeys(this.connectionId, this.systemId)
            .then((response: IProtocolResponse<IIRoutingKeys[]>): IProtocolResponse<IIRoutingKeys[]> => {
                if (response.result) {
                    this.routingKeys = response.data;
                }
                return response;
            });
    }

    private getEvents(systemId: string): ng.IPromise<IProtocolResponse<IIEvents[]>> {
        return this.intgApiService
            .getIntegrationSystemEvents(systemId)
            .then((response: IProtocolResponse<IIEvents[]>): IProtocolResponse<IIEvents[]> => {
                if (response.result) {
                    this.events = response.data;
                }
                return response;
            });
    }

    private formatEvents() {
        this.events = map(this.events, (event: IIEvents): IIEvents => {
            const routingKey: IIRoutingKeys = find(this.routingKeys, {
                eventId: event.id,
            });
            return {
                ...event,
                routingKeyId: routingKey ? routingKey.id : null,
                isEnabled: Boolean(routingKey),
            };
        });
    }

    private addContactMapping() {
        if (!this.integrationContactMappings) {
            this.integrationContactMappings = [];
        }
        const hasMapping = find(this.integrationContactMappings, (intgContactMapping: IIContactMappingElement) =>
            intgContactMapping.dsElementId === this.selectedInputContactMapping && intgContactMapping.contactId === this.selectedOutputContactMapping);
        if (hasMapping) {
            this.notificationService.alertError(this.translatePipe.transform("IntegrationMappingExistsTitle"), this.translatePipe.transform("IntegrationMappingExistsText"));
            return;
        }
        const contactMapping: IIContactMappingElement = {
            dsElementId: this.selectedInputContactMapping,
            contactId: this.selectedOutputContactMapping,
        };

        this.integrationContactMappings = [...this.integrationContactMappings, contactMapping];
    }

    private selectTemplate(selection: ITemplate) {
        this.selectedTemplate = selection.Id;
        this.templatesResponse = selection;
    }

    private populateFields() {
        if (this.system.form && isArray(this.system.form.outboundFields)) {
            each(
                this.system.form.outboundFields,
                (field: IFormFieldInput): void => {
                    if (field.fieldKey && field.dataType === FormDataTypes.TEXT) {
                        field.inputValue = this.connection[field.fieldKey];
                    } else if (field.pValueKey && field.dataType === FormDataTypes.TEXT) {
                        const property: IIConnectionProperty = find(
                            this.connection.properties,
                            (value: IIConnectionProperty): boolean => {
                                return (value.pname === field.pValueKey);
                            },
                        );
                        if (property) {
                            field.inputValue = property.pvalue;
                            if (this.systemId === SystemConstants.SLACK) {
                                this.system.form.showOutbound = true;
                            }
                        }
                    } else if (field.dataType === FormDataTypes.INTEGRATION_MAPPINGS) {

                        this.integrationContactMappings = this.intgEloquaService.generateEloquaContactMappings(
                            this.connection.properties,
                            ConnectionMappingPrefixes.ELOQUA_CONTACT_MAPPING,
                        );
                        this.integrationDataSubProfileMap = this.intgEloquaService.generateEloquaDataProfMappings(
                            this.connection.properties,
                            ConnectionMappingPrefixes.ELOQUA_DATASUBJECT_PROFILE,
                        );

                        this.integrationPreferenceData = this.intgEloquaService.generateEloquaDataPreferenceMap(
                            this.connection.properties,
                            ConnectionMappingPrefixes.ELOQUA_PREFERENCE_PURPOSE,
                        );

                    } else if (this.connection.properties) {
                        const property: IIConnectionProperty = find(this.connection.properties, (value: IIConnectionProperty): boolean => Boolean(value.pvalue));
                        this.selectedTemplate = this.systemId === SystemConstants.JIRA ? property.pvalue : "";
                    }
                },
            );
        }
    }

    private buildRequestFromFields(request: IStringMap<string>, fieldKeys: string[]): IStringMap<string> {
        each(fieldKeys, (requiredField: string): void => {
            const field: IFormFieldInput = this.getFormFieldByKey(
                requiredField,
            );
            if (field && field.inputValue) {
                request[requiredField] = field.inputValue;
            }
        });
        if (this.connection.systemInstanceId) {
            request.systemInstanceId = this.connection.systemInstanceId;
        }
        return request;
    }

    private getFormFieldByKey(key: string): IFormFieldInput {
        return find(
            this.system.form.outboundFields,
            (field: IFormFieldInput): boolean =>
                field.fieldKey === key,
        );
    }

    private reloadView() {
        this.stateService.go(
            "zen.app.pia.module.intg.connections.manage",
            {
                id: this.connectionId,
                system: this.systemId,
                time: new Date().getTime(),
            },
            { location: "replace" },
        );
    }

    private installConnection(apiKey: string) {
        const modalData: IInstallApiKeyModalResolve = {
            apiKey,
            connectionName: this.connection.name,
        };
        this.modalService.setModalData(modalData);
        this.modalService.openModal("installAPIKeyModal");
    }

    private createSlackConnection() {
        this.intgApiService.createSlackConnection(this.connection.name).then((response: IProtocolResponse<any>): void => {
            const location: string = response.result && response.data && response.data.url ? response.data.url : "";
            if (location) {
                window.location.href = location;
            }
        });
    }

    private getAllFormFieldKeys(): string[] {
        if (this.system.form && this.system.form.outboundFields) {
            return map(
                this.system.form.outboundFields,
                (field: IFormFieldInput): string => {
                    return field.fieldKey;
                },
            );
        }
        return [];
    }

    private upsertApiKey(): ng.IPromise<IProtocolResponse<IIAPIKey>> {
        let apiKeyPromise: ng.IPromise<IProtocolResponse<IIAPIKey>>;
        const apiKeyFields: IIAPIKey = {
            integrationSystemId: this.systemId,
            systemInstanceId: this.connectionId,
            onPrem: Boolean(this.inbound).toString(),
            ipWhiteList: this.getIpAddress(),
        };
        if (this.apiKeysInfo && this.apiKeysInfo.id) {
            apiKeyFields.id = this.apiKeysInfo.id;
            apiKeyPromise = this.intgApiService.patchApiKey(apiKeyFields);
        } else {
            apiKeyFields.disabled = false;
            apiKeyPromise = this.intgApiService.createApiKey(apiKeyFields);
        }
        return apiKeyPromise;
    }

    private getIpAddress(): string {
        if (!this.inbound) return "";
        if (this.isIpAddress && this.address) {
            this.cidrAddress = null;
            return this.address;
        } else if (!this.isIpAddress && this.cidrAddress) {
            this.address = null;
            return this.cidrAddress;
        }
    }
}
