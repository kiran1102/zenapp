// Angular
import {
    Component,
    Inject,
    OnInit,
    Input,
} from "@angular/core";
import { StateService } from "@uirouter/core";

// Interfaces
import { IProtocolResponse } from "interfaces/protocol.interface";
import {
    IIConnection,
    IIConnectionTableRow,
    IIPage,
} from "modules/intg/interfaces/integration.interface";
import {
    IAction,
    IStore,
} from "interfaces/redux.interface";
import { IDeleteConfirmationModalResolve } from "interfaces/delete-confirmation-modal-resolve.interface";
import { IBasicTableCell } from "interfaces/tables.interface";
import { IIGenericTableConfig } from "interfaces/table-config.interface";
import { IPageableMeta } from "interfaces/pagination.interface";

// 3rd Party
import {
    map,
    each,
} from "lodash";

// Redux
import { getOrgById } from "oneRedux/reducers/org.reducer";

// Services
import { IntgApiService } from "intsystemservice/intg-api.service";
import { IntgConnectionsHelperService } from "intsystemservice/intg-connections-helper.service";
import { ModalService } from "sharedServices/modal.service";
import { NotificationService } from "sharedModules/services/provider/notification.service";

// Pipes and Constants
import {
    ApiConnectionActions,
    ColumnComponent,
    ConnectionListView,
    SystemConstants,
} from "modules/intg/constants/integration.constant";
import { StoreToken } from "tokens/redux-store.token";
import { TranslatePipe } from "modules/pipes/translate.pipe";

@Component({
    selector: "intg-connections-list",
    templateUrl: "./intg-connections-list.component.html",
})

export class IntgConnectionsListComponent implements OnInit {

    @Input() listToShow: string;

    ready = false;
    table: { rows: IIConnection[], columns: IIGenericTableConfig[] };
    page: IPageableMeta;
    connections: IIConnection[];
    title: string;
    pageNumber = 1;
    noOfElementsOnCurrentPage: number;

    private columnConfig: IIGenericTableConfig[] = this.intgConnectionsHelperService.getConnectionsListConfig();

    constructor(
        private stateService: StateService,
        @Inject(StoreToken) public store: IStore,
        private intgApiService: IntgApiService,
        private modalService: ModalService,
        private notificationService: NotificationService,
        private intgConnectionsHelperService: IntgConnectionsHelperService,
        private translatePipe: TranslatePipe,
    ) {}

    public ngOnInit(): void {
        if (this.listToShow === ConnectionListView.CONNECTION_LIST_VIEW) {
            this.title = "API_Connections";
            this.loadConnections();
        } else if (this.listToShow === ConnectionListView.API_KEYS_LIST_VIEW) {
            this.title = "API_Keys";
            this.loadConnections(SystemConstants.GENERIC);
        }
    }

    loadConnections(systemId?: string, pageNumber = 0, size = 20) {
        this.ready = false;
        this.intgApiService.getConnection(null, systemId, null, pageNumber, size).then((response: IProtocolResponse<IIPage<IIConnection>>): void => {
            this.connections = response.result && response.data && response.data.content ? response.data.content : [];
            if (response.result && response.data && response.data.page) {
                this.page = {
                    ...response.data.page,
                    first: response.data.page.number === 0,
                    last: (response.data.page.number + 1) === response.data.page.totalPages,
                };
                this.noOfElementsOnCurrentPage = response.data.content.length;
            }

            this.ready = true;
            this.table = {
                rows: this.toBasicTableRows(this.connections),
                columns: this.columnConfig,
            };
        });
    }

    goToManage() {
        this.listToShow === ConnectionListView.CONNECTION_LIST_VIEW ? this.stateService.go("zen.app.pia.module.intg.connections.manage") : this.stateService.go("zen.app.pia.module.intg.connections.manage", { id: null, system: SystemConstants.GENERIC });
    }

    handleActionClicks(action: IAction): void {
        switch (action.type) {
            case ApiConnectionActions.CONNECTION_EDIT:
                this.stateService.go("zen.app.pia.module.intg.connections.manage", {
                    id: action.payload.systemInstanceId,
                    system: action.payload.integrationSystemId,
                });
                break;
            case ApiConnectionActions.CONNECTION_ADD:
                this.stateService.go("zen.app.pia.module.intg.connections.manage", {
                    system: action.payload.integrationSystemId,
                });
                break;
            case ApiConnectionActions.CONNECTION_DELETE:
                const systemInstanceId: string = action.payload.systemInstanceId;
                const modalData: IDeleteConfirmationModalResolve = {
                    modalTitle: this.translatePipe.transform("DeleteConnection"),
                    submitButtonText: this.translatePipe.transform("Delete"),
                    confirmationText: this.translatePipe.transform("DeleteConnectionMessage"),
                    cancelButtonText: this.translatePipe.transform("Cancel"),
                    promiseToResolve: (): ng.IPromise<boolean> =>
                        this.intgApiService.deleteConnection(systemInstanceId).then(
                            (response: IProtocolResponse<void>): boolean => {
                                if (response.result) {
                                    this.notificationService.alertSuccess(
                                        this.translatePipe.transform("Success"),
                                        this.translatePipe.transform("SuccessfullyDeletedtheConnection"),
                                    );
                                    const systemId: string = this.listToShow === ConnectionListView.API_KEYS_LIST_VIEW ? SystemConstants.GENERIC :  null;
                                    this.pageNumber = this.noOfElementsOnCurrentPage === 1 ? this.pageNumber - 1 : this.pageNumber;
                                    this.loadConnections(systemId, this.pageNumber - 1);
                                }
                                return response.result;
                            },
                        ),
                };
                this.modalService.setModalData(modalData);
                this.modalService.openModal("otDeleteConfirmationModal");
                break;
        }
    }

    goToRoute(cell: IBasicTableCell): void {
        this.stateService.go(cell.route, cell.params);
    }

    pageChanged(pageNumber: number) {
        this.pageNumber =  pageNumber;
        const systemId = this.listToShow === ConnectionListView.API_KEYS_LIST_VIEW ? SystemConstants.GENERIC : null;
        this.loadConnections(systemId, pageNumber - 1);
    }

    private toBasicTableRows(rows: IIConnection[]): IIConnection[] {
        return map(rows, (row: IIConnection): IBasicTableCell => {
            const cells: IBasicTableCell[] = [];
            each(this.columnConfig, (column: IIGenericTableConfig): void => {
                this.getStatusAndOrgName(column, row);
                cells.push({
                    key: column.modelPropKey,
                    value: row[column.modelPropKey],
                    displayValue: row[column.modelPropKey],
                    route: "zen.app.pia.module.intg.connections.manage",
                    routeParams: {
                        id: row.systemInstanceId,
                        system: row.integrationSystemId,
                    },
                });
            });
            return { ...row, cells };
        });
    }

    private getStatusAndOrgName(column?: IIGenericTableConfig, row?: IIConnectionTableRow): void {
        switch (column.component) {
            case ColumnComponent.ACTIVE_TAG:
                row.statusText = row[column.modelPropKey]
                    ? this.translatePipe.transform("Active")
                    : this.translatePipe.transform("Inactive");
                row.modifierClass = row[column.modelPropKey] ? "primary" : "destructive";
                break;
            case ColumnComponent.ORG_NAME:
                row.orgName = getOrgById(row[column.modelPropKey])(this.store.getState()).Name;
                break;
        }
    }
}
