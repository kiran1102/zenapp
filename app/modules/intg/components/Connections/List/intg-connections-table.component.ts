import {
    Component,
    EventEmitter,
    Input,
    Output,
} from "@angular/core";

// Interfaces
import { IDropdownOption } from "interfaces/one-dropdown.interface";
import { IBasicTableCell } from "interfaces/tables.interface";
import { IIConnection } from "modules/intg/interfaces/integration.interface";
import { IIGenericTableConfig } from "interfaces/table-config.interface";
import { IAction } from "interfaces/redux.interface";

// services
import { Permissions } from "modules/shared/services/helper/permissions.service";

// Constants and Pipes
import {
    SystemConstants,
    ApiConnectionActions,
} from "modules/intg/constants/integration.constant";
import { TranslatePipe } from "modules/pipes/translate.pipe";

// 3rd Party
import { findIndex } from "lodash";

@Component({
    selector: "intg-connections-table",
    templateUrl: "./intg-connections-table.component.html",
})

export class IsConnectionsTableComponent {
    @Input() public rows;
    @Input() public columns;
    @Input() public isUpgradeDropdown;

    @Output() public goToRoute: EventEmitter<IBasicTableCell> = new EventEmitter<IBasicTableCell>();
    @Output() public handleActionClicks: EventEmitter<IAction> = new EventEmitter<IAction>();
    public rowBordered = true;
    public responsive = true;
    public menuOptions: IDropdownOption[];

    private menuClass: string;

    constructor(
        private translatePipe: TranslatePipe,
        private permissions: Permissions,
    ) {}

    public columnTrackBy(column: IIGenericTableConfig): string {
        return column.id;
    }

    public cellTrackBy(cell: IBasicTableCell): string {
        return cell.key;
    }

    public getActions(row: IIConnection) {
        this.menuOptions = [];
        if (this.permissions.canShow("IntegrationPageEdit") &&  row.integrationSystemId !== SystemConstants.SLACK) {
            this.menuOptions.push(
                {
                  text: this.translatePipe.transform("AddConnection"),
                  action: (): void => { this.handleActionClicks.emit({payload: row, type: ApiConnectionActions.CONNECTION_ADD}); },
                },
                {
                    text: this.translatePipe.transform("EditConnection"),
                    action: (): void => { this.handleActionClicks.emit({payload: row, type: ApiConnectionActions.CONNECTION_EDIT}); },
                },
            );
        }
        if (this.permissions.canShow("IntegrationPageDelete") && row.integrationSystemId) {
            this.menuOptions.push(
                {
                    text: this.translatePipe.transform("DeleteConnection"),
                    action: (): void => { this.handleActionClicks.emit({payload: row, type: ApiConnectionActions.CONNECTION_DELETE}); },
                },
            );
        }
    }

    private setMenuClass(row: IIConnection): void | undefined {
        this.menuClass = "actions-table__context-list";
        if (this.rows.length <= 10) return;
        const halfwayPoint: number = (this.rows.length - 1) / 2;
        const rowIndex: number = findIndex(this.rows, (item: IIConnection): boolean => row.id === item.id);
        if (rowIndex > halfwayPoint) this.menuClass += " actions-table__context-list--above";
    }

}
