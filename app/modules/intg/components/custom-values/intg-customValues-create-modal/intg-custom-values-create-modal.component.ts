// Angular
import { Component } from "@angular/core";
import {
    FormGroup,
    FormBuilder,
    Validators,
} from "@angular/forms";

// Rxjs
import { Subject } from "rxjs";

// Services
import { IntgIntegrationsService } from "intsystemservice/intg-integrations.service";
import { IntgWorkflowService } from "intsystemservice/intg-workflow.service";
import { TimeStamp } from "oneServices/time-stamp.service";
import { IntgCustomValueService } from "intsystemservice/intg-custom-value.service";

// Interfaces
import { IProtocolResponse } from "interfaces/protocol.interface";
import { IPageableOf } from "interfaces/pagination.interface";
import {
    IISystem,
    IIWorkflow,
    ICustomValuesResponse,
} from "modules/intg/interfaces/integration.interface";
import { IKeyValue } from "interfaces/generic.interface";
import { IDateTimeOutputModel } from "@onetrust/vitreus";

// Enum and Constants
import {
    CustomValueTypes,
    VarTypes,
} from "modules/intg/constants/integration.constant";
import { ParameterScope } from "modules/intg/enums/intg-enum";

// Pipe
import { TranslatePipe } from "pipes/translate.pipe";

@Component({
    selector: "intg-custom-values-create-modal",
    templateUrl: "./intg-custom-values-create-modal.component.html",
})

export class IntgCustomValuesCreateModalComponent {
    systems: IISystem[] = [
        {
            name: this.translatePipe.transform("All"),
            id: null,
        },
    ];
    workflows: IIWorkflow[] = [
        {
            id: null,
            integrationId: null,
            name: this.translatePipe.transform("All"),
            description: null,
            enabled: null,
            workflowType: null,
        },
    ];
    filteredWorkflow: IIWorkflow[];
    workflowId: string;
    otModalCloseEvent: Subject<boolean>;
    customValueForm: FormGroup;
    isLoading: boolean;
    isWorkflowsLoading: boolean;
    isSystemsLoading: boolean;
    expirationDateValue: string;
    varType: string;
    modalTitle: string;
    dateTimeValue: string;
    is24Hour: boolean;
    expirationDate: Date;
    valueType = CustomValueTypes;
    page: number;
    isCustomValue = false;
    otModalData: {
        page: number,
        customValuesDetails: ICustomValuesResponse,
    };
    customValueDetails: ICustomValuesResponse;
    integrationId: string;
    ready = true;
    isEdit: boolean;
    systemId: string;
    format = this.timeStamp.getDatePickerDateFormat();

    valueTypes: Array<IKeyValue<string>> = [
        {
            key: CustomValueTypes.STRING,
            value: CustomValueTypes.STRING,
        },
        {
            key: CustomValueTypes.DOUBLE,
            value: CustomValueTypes.DOUBLE,
        },
        {
            key: CustomValueTypes.BOOLEAN,
            value: CustomValueTypes.BOOLEAN,
        },
        {
            key: CustomValueTypes.DATE,
            value: CustomValueTypes.DATE,
        },
        {
            key: CustomValueTypes.OBJECT,
            value: CustomValueTypes.OBJECT,
        },
    ];

    constructor(
        private formBuilder: FormBuilder,
        private intgIntegrationsService: IntgIntegrationsService,
        private intgWorkflowService: IntgWorkflowService,
        private timeStamp: TimeStamp,
        private translatePipe: TranslatePipe,
        private intgCustomValuesService: IntgCustomValueService,
    ) {}

    ngOnInit() {
        this.is24Hour = this.timeStamp.getIs24Hour();
        this.customValueForm = this.initializeForm();
        this.isEdit = Boolean(this.otModalData.customValuesDetails);
        if (this.isEdit) {
            this.modalTitle = "EditValue";
            this.getCustomValuesDetails(this.otModalData.customValuesDetails.id);
        } else {
            this.modalTitle = "CreateValue";
        }
        this.page = this.otModalData.page;
        this.getCustomSystems();
        this.getWorkflow();
    }

    closeModal(reloadPage?: boolean) {
        this.otModalCloseEvent.next(reloadPage);
    }

    setType(value: IKeyValue<string>): void {
        this.customValueForm.get("type").setValue(value);
        this.customValueForm.get("value").setValue("");
        if (this.customValueForm.get("type").value.value === this.valueType.BOOLEAN) {
            this.customValueForm.get("value").setValue(false);
        }
        if (this.customValueForm.get("type").value.value === this.valueType.STRING) {
            this.varType = VarTypes.TEXT;
        } else {
            this.varType = VarTypes.NUMBER;
        }
    }

    setSystem(value: IISystem): void {
        this.customValueForm.get("selectedSystem").setValue(value);
        this.filteredWorkflow = this.getFilteredWorkflowList();
        this.customValueForm.get("selectedWorkflow").setValue(this.workflows[0]);
        if (!value.id) {
           this.filteredWorkflow =  this.workflows;
        }
    }

    setWorkflow(value: IIWorkflow): void {
        this.customValueForm.get("selectedWorkflow").setValue(value);
    }

    getFilteredWorkflowList(): IIWorkflow[] {
        const filteredWorkflow = this.workflows.filter((data) => data.id === null || (data.referenceIntegrationId === this.customValueForm.get("selectedSystem").value.id));
        return filteredWorkflow;
    }

    selectDateTime(dateTimeValue: IDateTimeOutputModel): void {
        this.customValueForm.get("value").setValue(dateTimeValue ? dateTimeValue.jsdate : "");
    }

    createCustomValues(): void {
        this.isLoading = true;
        const customValues: ICustomValuesResponse = {
            id: this.isEdit ? this.customValueDetails.id : null,
            integrationId: this.customValueForm.get("selectedSystem").value.id,
            name: this.customValueForm.get("name").value,
            parameterScope: this.getParameterScope(),
            parameterType: this.customValueForm.get("type").value.value,
            value: this.customValueForm.get("value").value,
            workflowId: this.customValueForm.get("selectedWorkflow").value.id,
        };
        this.intgCustomValuesService.createCustomValues(customValues, this.customValueDetails ? this.customValueDetails.id : null).subscribe((response) => {
            if (response.result) {
                this.closeModal(true);
            }
            this.isLoading = false;
        });
    }

    getParameterScope(): string {
        let parameterScope;
        if (this.customValueForm.get("selectedWorkflow").value.id) {
            parameterScope = ParameterScope.WORKFLOW;
        } else if (this.customValueForm.get("selectedSystem").value.id) {
            parameterScope = ParameterScope.SYSTEM;
        } else {
            parameterScope = ParameterScope.ALL;
        }
        return parameterScope;
    }

    onToggle(value: boolean): void {
        this.isCustomValue = value;
        this.customValueForm.get("value").setValue(this.isCustomValue);
    }

    getCustomValuesDetails(valueId: string): void {
        this.ready = false;
        this.intgCustomValuesService.getCustomValuesDetails(valueId).subscribe(
            (response: IProtocolResponse<ICustomValuesResponse>) => {
                if (response.result) {
                    this.customValueDetails = response.data;
                    this.workflowId = response.data.workflowId;
                    this.integrationId = response.data.integrationId;
                    this.setFormValues(this.customValueDetails);
                }
                this.ready = true;
            },
        );
    }

    setFormValues(customValueDetails: ICustomValuesResponse): void {
        this.customValueForm.patchValue({
            selectedSystem: this.getSystemById(customValueDetails.integrationId) || this.systems[0],
            name: customValueDetails.name,
            type: {
                key: customValueDetails.parameterType,
                value: customValueDetails.parameterType,
            },
            value: this.getValue(customValueDetails),
            selectedWorkflow: this.getWorkflowsById(customValueDetails.workflowId) || this.workflows[0],
        });
    }

    getValue(customValueDetails: ICustomValuesResponse): string | Date {
        if (customValueDetails.parameterType === this.valueType.DATE) {
            return new Date(customValueDetails.value);
        } else if (customValueDetails.parameterType === this.valueType.BOOLEAN) {
            this.isCustomValue = customValueDetails.value;
            return customValueDetails.value;
        } else if (customValueDetails.parameterType === this.valueType.DOUBLE) {
            this.varType = VarTypes.NUMBER;
            return customValueDetails.value;
        } else {
            return customValueDetails.value;
        }
    }

    getSystemById(systemId: string): IISystem {
        const selectedSystem = this.systems.find((data) => data.id === systemId);
        return selectedSystem;
    }

    getWorkflowsById(workflowId: string): IIWorkflow {
        const selectedWorkflow = this.workflows.find((data) => data.id === workflowId);
        return selectedWorkflow;
    }

    onPaste(event: ClipboardEvent) {
        const clipboardData = event.clipboardData;
        const pastedText = clipboardData.getData("text");
        return !(pastedText.includes("e"));
    }

    private initializeForm(): FormGroup {
        return this.formBuilder.group({
            name: ["", Validators.required],
            value: [""],
            systemIntegrationWorkflow: [""],
            type: [this.valueTypes[0], Validators.required],
            selectedSystem: [this.systems[0]],
            selectedWorkflow: [this.workflows[0]],
        });
    }

    private getCustomSystems(): ng.IPromise<void> {
        this.isSystemsLoading = true;
        return this.intgIntegrationsService.getSeededIntegrations(true).then((systems: IProtocolResponse<IPageableOf<IISystem>>) => {
            this.systems.push(...systems.data.content.filter((data) => {
                return !data.name.startsWith("OneTrust-");
            }));
            this.isSystemsLoading = false;
            this.customValueForm.get("selectedSystem").setValue(this.getSystemById(this.integrationId) || this.systems[0]);
        });
    }

    private getWorkflow(page = this.page, size = 1000): ng.IPromise<void> {
        this.isWorkflowsLoading = true;
        return this.intgWorkflowService.getWorkflows(page, size).then(
            (response: IProtocolResponse<IPageableOf<IIWorkflow>>) => {
                this.workflows =  [this.workflows[0]];
                this.workflows.push(...response.data.content);
                this.filteredWorkflow = this.workflows;
                this.customValueForm.get("selectedWorkflow").setValue(this.getWorkflowsById(this.workflowId) || this.workflows[0]);
                this.isWorkflowsLoading = false;

            });
    }
}
