// Angular
import {
    Component,
    Input,
 } from "@angular/core";

 // interfaces
import { IDropdownOption } from "interfaces/one-dropdown.interface";
import { IPageableOf } from "interfaces/pagination.interface";
import { ICustomValuesResponse } from "modules/intg/interfaces/integration.interface";
import { ITableColumnConfig } from "interfaces/table-config.interface";

// enums and const
import { TableColumnTypes } from "enums/data-table.enum";
import { CustomValueTableColumnKey } from "modules/intg/constants/integration.constant";

@Component({
    selector: "intg-custom-table",
    templateUrl: "./intg-custom-value-table.component.html",
    host: {
        class: "flex-full-height column-nowrap full-size",
    },
})
export class IntgCustomValueTableComponent {

    @Input() ready = false;
    @Input() page: IPageableOf<ICustomValuesResponse>;
    @Input() emptyStateMessage;
    @Input() columns: ITableColumnConfig[];
    @Input() options: IDropdownOption[];

    tableColumnTypes = TableColumnTypes;
    resizableCol = false;
    responsiveTable = true;
    truncateCellContent = true;
    wrapCellContent = false;
    rowBordered = true;
    rowNoHover = false;
    rowStriped = false;
    colBordered = false;
    isOpenPopOver = false;
    selectedIndex: number;
    columnKey = CustomValueTableColumnKey;

    isObject(val) { return typeof val === "object"; }

    columnTrackBy(column: number): number {
        return column;
    }
}
