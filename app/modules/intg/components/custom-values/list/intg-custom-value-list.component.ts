// Angular
import {
    Component,
    OnInit,
 } from "@angular/core";

 // interfaces
import { ITableColumnConfig } from "interfaces/table-config.interface";
import { IDropdownOption } from "interfaces/one-dropdown.interface";
import { ICustomValuesResponse } from "modules/intg/interfaces/integration.interface";
import { IPageableOf } from "interfaces/pagination.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";
import { IDeleteConfirmationModalResolve } from "interfaces/delete-confirmation-modal-resolve.interface";

// pipes
import { TranslatePipe } from "pipes/translate.pipe";

// enums and constants
import { TableColumnTypes } from "enums/data-table.enum";
import {
    CustomValueTableColumnKey,
    CustomValueTypes,
} from "modules/intg/constants/integration.constant";

// services
import { IntgCustomValueService } from "intsystemservice/intg-custom-value.service";
import { OtModalService, ConfirmModalType } from "@onetrust/vitreus";
import { ModalService } from "sharedServices/modal.service";

// Component
import { IntgCustomValuesCreateModalComponent } from "intsystemcomponent/custom-values/intg-customValues-create-modal/intg-custom-values-create-modal.component";

// Rxjs
import { filter, takeUntil } from "rxjs/operators";
import { Subject } from "rxjs";

@Component({
    selector: "intg-custom-values",
    templateUrl: "./intg-custom-value-list.component.html",
})

export class IntgCustomValueListComponent implements OnInit {

    ready = false;
    options: IDropdownOption[];
    columnConfig: ITableColumnConfig[] =
     [
        {
            name: this.translatePipe.transform("Name"),
            type: TableColumnTypes.Text,
            cellValueKey: CustomValueTableColumnKey.Name,
        },
        {
            name: this.translatePipe.transform("Type"),
            type: TableColumnTypes.Text,
            cellValueKey: CustomValueTableColumnKey.ParamaterType,
        },
        {
            name: this.translatePipe.transform("System"),
            type: TableColumnTypes.Text,
            cellValueKey: CustomValueTableColumnKey.IntegrationName,
        },
        {
            name: this.translatePipe.transform("Workflow"),
            type: TableColumnTypes.Text,
            cellValueKey:  CustomValueTableColumnKey.WorkflowName,
        },
        {
            name: this.translatePipe.transform("Value"),
            type: TableColumnTypes.Text,
            cellValueKey:  CustomValueTableColumnKey.Value,
        },
        {
            name: "",
            type: TableColumnTypes.Action,
            cellValueKey: "",
        },
    ];
    pagevalues: IPageableOf<ICustomValuesResponse>;
    private destroy$ = new Subject();

    constructor(
        private translatePipe: TranslatePipe,
        private intgCustomValueService: IntgCustomValueService,
        private otModalService: OtModalService,
        private modalService: ModalService,
    ) {}

    ngOnInit() {
        this.options = this.getActions();
        this.getCustomValues(0, 20);
    }

    handlePageChanged(currentPage: number) {
        if (this.pagevalues.totalPages) {
            this.ready = false;
            this.getCustomValues(currentPage, 20);
        }
    }

    openCustomModal() {
        this.otModalService
            .create(
                IntgCustomValuesCreateModalComponent,
                {
                    isComponent: true,
                }, {
                    page: this.pagevalues.number,
                },
        ).pipe(
            filter((data) => Boolean(data)),
        ).subscribe(() => {
            this.ready = false;
            this.getCustomValues(0, 20);
        });
    }

    editCustomValues(customValues: ICustomValuesResponse) {
        this.otModalService
            .create(
                IntgCustomValuesCreateModalComponent,
                {
                    isComponent: true,
                }, {
                    page: this.pagevalues.number,
                    customValuesDetails: customValues,
                },
        ).pipe(
            filter((data) => Boolean(data)),
        ).subscribe(() => {
            this.ready = false;
            this.getCustomValues(0, 20);
        });
    }

    ngOnDestroy() {
        this.destroy$.next();
        this.destroy$.complete();
    }

    private getCustomValues(pageNumber: number, size: number) {
        this.pagevalues = {} as IPageableOf<ICustomValuesResponse>;
        this.intgCustomValueService.getAllCustomValues(pageNumber, size).
            subscribe((response: IProtocolResponse<IPageableOf<ICustomValuesResponse>>) => {
                if (response.result) {
                    this.pagevalues = response.data;
                    this.pagevalues.content = this.pagevalues.content.map((res: ICustomValuesResponse) => {
                        if (res.parameterType === CustomValueTypes.DATE && res.value) {
                                res.value = new Date(res.value).toLocaleString();
                        }
                        return res;
                    });
                }
                this.ready = true;
        });
    }

    private deleteCustomValueConfirmation(id: string) {
        this.otModalService
            .confirm({
                type: ConfirmModalType.WARNING,
                translations: {
                    title: this.translatePipe.transform("DeleteCustomValue"),
                    desc: this.translatePipe.transform("AreYouSureDeleteCustomValueWarning"),
                    confirm: this.translatePipe.transform("AreYouSureContinueCustomValue"),
                    submit: this.translatePipe.transform("Ok"),
                    cancel: this.translatePipe.transform("Cancel"),
                },
            }).pipe(
                takeUntil(this.destroy$),
            ).subscribe(
                (data) => {
                    if (data && id) {
                        this.intgCustomValueService.deleteCustomValue(id).
                            subscribe((response: IProtocolResponse<void>) => {
                                if (response.result) {
                                    this.getCustomValues(0, 100);
                                }
                        });
                    }
                },
            );
    }

    private getActions(): IDropdownOption[] {
        const actions: IDropdownOption[] = [];
        actions.push({
            text:  this.translatePipe.transform("Edit"),
            action: (row): void => {
                this.editCustomValues(row);
            },
        });
        actions.push({
            text: this.translatePipe.transform("Delete"),
            action: (row): void => {
                this.deleteCustomValueConfirmation(row.id);
            },
        });
        if (!actions.length) {
            actions.push({
                text: this.translatePipe.transform("NoActionsAvailable"),
                action: (): void => {},
            });
        }
        return actions;
    }
}
