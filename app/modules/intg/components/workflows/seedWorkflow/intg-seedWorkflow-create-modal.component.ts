// Angular
import {
    Component,
    OnInit,
} from "@angular/core";
import {
    FormGroup,
    FormBuilder,
    Validators,
} from "@angular/forms";

// interfaces
import { IOtModalContent } from "@onetrust/vitreus";
import { IProtocolResponse } from "interfaces/protocol.interface";
import {
    IIWorkflowDetail,
    IICredential,
} from "modules/intg/interfaces/integration.interface";
import { IPageableOf } from "interfaces/pagination.interface";

// services
import { IntgWorkflowService } from "intsystemservice/intg-workflow.service";
import { IntgCredentialsService } from "intsystemservice/intg-credentials.service";
import { StateService } from "@uirouter/core";

// pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

// constants
import { NO_BLANK_SPACE } from "constants/regex.constant";

@Component({
    selector: "intg-seed-workflow-create-modal",
    templateUrl: "./intg-seedWorkflow-create-modal.component.html",
})

export class IntgSeedWorkFlowCreateModal implements IOtModalContent, OnInit {
    otModalData?: { templateId: string};
    otModalCloseEvent;
    saveInProgress: boolean;
    seedWorkflowForm: FormGroup;
    selectedCredential: IICredential;
    credentials: IICredential[] = [
        {
            name: this.translatePipe.transform("None"),
            id: null,
            authContext: null,
            authType: null,
            integrationId: null,
        },
    ];

    constructor(
        private intgWorkflow: IntgWorkflowService,
        private translatePipe: TranslatePipe,
        private stateService: StateService,
        private formBuilder: FormBuilder,
        private intgCredentialsService: IntgCredentialsService,
    ) {}

    ngOnInit() {
        this.getCredentials();
        this.seedWorkflowForm = this.initializeForm();
    }

    onClickAddWorkflowName() {
        this.saveInProgress = true;
        this.intgWorkflow.createWorkflowFromTemplate(this.otModalData.templateId, this.seedWorkflowForm.get("workflowName").value, this.selectedCredential.id)
        .subscribe((response: IProtocolResponse<IIWorkflowDetail>) => {
            if (response.result) {
                this.stateService.go("zen.app.pia.module.intg.workflows.builder", {
                    id: response.data.id,
                });
                this.otModalCloseEvent.next(false);
            }
            this.saveInProgress = false;
        });
    }

    setSelectedCredential(credential: IICredential) {
        this.selectedCredential = credential;
    }

    closeModal() {
        this.otModalCloseEvent.next(false);
        this.otModalCloseEvent.complete();
    }

    private initializeForm(): FormGroup {
        return this.formBuilder.group({
            workflowName: ["", [Validators.required, Validators.pattern(NO_BLANK_SPACE)]],
        });
    }

    private getCredentials() {
        this.intgCredentialsService.getCredentials().then((credentials: IProtocolResponse<IPageableOf<IICredential>>) => {
            if (credentials.result)
                this.credentials.push(...credentials.data.content as IICredential[]);
        });
    }

}
