// Angular
import {
    Component,
    Input,
    Output,
    EventEmitter,
    SimpleChanges,
    OnInit,
    OnChanges,
    OnDestroy,
} from "@angular/core";

// 3rd party
import {
    find,
} from "lodash";

// Rxjs
import { Subject } from "rxjs";

// Services
import { IntgWorkflowService } from "intsystemservice/intg-workflow.service";
import { IntgWorkflowHelper } from "intsystemservice/helpers/intg-workflow-helper.service";

// Interfaces
import { IStringMap } from "interfaces/generic.interface";
import { ITabsNav } from "interfaces/one-tabs-nav.interface";
import {
    IICredential,
    IISeededEvent,
    IIIntegrationDetails,
    IIWorkflow,
    IIWorkflowDetail,
    IIWorkflowNode,
    IISelectedAction,
    IAvailableEvent,
    ICustomValuesResponse,
} from "modules/intg/interfaces/integration.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";

// Enums & Constants
import { WorkflowBuilderSteps } from "enums/integration.enum";
import {
    IntgActionTabs,
    ProcessType,
    NextSteps,
    TriggerType,
} from "modules/intg/constants/integration.constant";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

@Component({
    selector: "intg-workflow-builder-action-item",
    templateUrl: "./intg-workflow-builder-action-item.component.html",
})

export class IntgWorkflowBuilderActionItemComponent implements OnInit, OnChanges, OnDestroy {

    @Input() integration: IIIntegrationDetails;
    @Input() otActionIcon: string;
    @Input() currentEvent: IISeededEvent;
    @Input() workflow: IIWorkflowDetail;
    @Input() seededIntegration: IIIntegrationDetails;
    @Input() otActions: IIWorkflow[];
    @Input() currentIndex: number;
    @Input() addedActionBlocks: IISelectedAction[];
    @Input() selectedEventCode: number;
    @Input() credentials: IICredential[];
    @Input() isFinishButtonReady: boolean;
    @Input() customFields: ICustomValuesResponse[];

    @Output() deleteThisAction: EventEmitter<string> = new EventEmitter();
    @Output() updateCredentialList: EventEmitter<IICredential[]> = new EventEmitter();

    eventEditActive = true;
    editMode = false;
    workflowDetail: IIWorkflowNode;
    currentNodeDescription: string;
    selectedOption = [];
    availableEvent: IAvailableEvent[] = [];
    currentHoveredLiIndex: number;
    currentStep: number = WorkflowBuilderSteps.TRIGGER_STEP;
    isLogicAction: boolean;
    logicIcon: string;
    logicText: string;
    tabs: ITabsNav[] = [];
    selectedTab: string = IntgActionTabs.SYSTEM;
    systemActionFilter: string;
    TABS = IntgActionTabs;
    WORKFLOW_STEPS = WorkflowBuilderSteps;
    isOtActionSelected = false;
    currentActionBlock: IISelectedAction;
    allowedTabs: IStringMap<ITabsNav> = this.intgWorkflowHelper.getAllowedTabs();

    private destroy$ = new Subject();

    constructor(
        public translatePipe: TranslatePipe,
        private intgWorkflowApi: IntgWorkflowService,
        private intgWorkflowHelper: IntgWorkflowHelper,
    ) {}

    ngOnInit() {
        this.tabs.push(this.allowedTabs.SYSTEM);
        this.tabs.push(this.allowedTabs.ONETRUST);
        this.tabs.push(this.allowedTabs.LOGIC);
        this.currentActionBlock = this.addedActionBlocks[this.currentIndex];
        if (this.currentActionBlock.applyEachNode) {
            this.eventEditActive = false;
            this.onLogicClick(WorkflowBuilderSteps[WorkflowBuilderSteps.APPLY_EACH]);
        } else if (this.currentActionBlock.httpNode) {
            this.isOtActionSelected = this.currentActionBlock.isOtAction;
            this.eventEditActive = false;
            this.nextStep(NextSteps.CREDENTIAL_STEP, null, this.isOtActionSelected);
            this.nextStep(NextSteps.REQUEST_RESPONSE_STEP);
        }
        this.intgWorkflowHelper.$workflowActionUpdated.subscribe((workflowActions: IISelectedAction[]) => {
            this.availableEvent = this.intgWorkflowHelper.calculateEventLookup(workflowActions, this.currentEvent, this.workflow.trigger, this.currentIndex, this.customFields);
        });
    }

    ngOnChanges(changes: SimpleChanges) {
        if (changes.workflow) {
            if (changes.workflow.currentValue.trigger.triggerType === TriggerType.WEBHOOK) {
                this.availableEvent = this.intgWorkflowHelper.calculateEventLookup(this.addedActionBlocks, this.currentEvent, this.workflow.trigger, this.currentIndex, this.customFields);
            }
        }
        if (changes.currentIndex || changes.currentEvent || changes.customFields) {
            this.availableEvent = this.intgWorkflowHelper.calculateEventLookup(this.addedActionBlocks, this.currentEvent, this.workflow.trigger, this.currentIndex, this.customFields);
        }
    }

    nextStep(nextStep: string, action?: IIWorkflow, isOtAction = false) {
        if (nextStep === WorkflowBuilderSteps[WorkflowBuilderSteps.CREDENTIAL_STEP]) {
            this.isOtActionSelected = isOtAction;
            this.workflowDetail = this.currentActionBlock.httpNode;
            this.currentStep = isOtAction ? WorkflowBuilderSteps.REQUEST_RESPONSE_STEP : WorkflowBuilderSteps[nextStep];
            if (this.workflowDetail) {
                this.currentNodeDescription = this.intgWorkflowHelper.getActionName(this.workflowDetail.nodeDescription);
            }
        }
        if (action) {
            this.currentNodeDescription = action.name;
            this.intgWorkflowApi.getWorkflowById(action.id, true).then((details: IProtocolResponse<IIWorkflowDetail>) => {
                const httpWorkflowNode = find(details.data.workflowNodes, (node: IIWorkflowNode) => node.processType === ProcessType.HTTP);
                const transformNode = find(details.data.workflowNodes, (node: IIWorkflowNode) => node.processType === ProcessType.TRANSFORM);
                const httpNodeLabel = this.currentActionBlock.savedHttpNodeLabel;
                const transformNodeLabel = this.currentActionBlock.savedTransformNodeLabel;
                if (httpWorkflowNode) {
                    this.currentActionBlock.httpNode =  { ...httpWorkflowNode, nodeDescription: details.data.name, nodeLabel: httpNodeLabel, id: null };
                    this.currentActionBlock.isOtAction = isOtAction;
                }
                if (transformNode) {
                    this.currentActionBlock.transformNode =  { ...transformNode, nodeDescription: details.data.name, nodeLabel: transformNodeLabel };
                } else {
                    this.currentActionBlock.transformNode = null;
                }
                this.currentActionBlock.isActionSave =  false;
                this.currentActionBlock.addingNewAction =  true;
                this.intgWorkflowHelper.updateWorkflowAction(this.addedActionBlocks);
                this.workflowDetail = this.currentActionBlock.httpNode;
            });
        }
    }

    ngOnDestroy() {
        this.destroy$.next();
        this.destroy$.complete();
    }

    trackByIndex(index: number): number {
        return index;
    }

    filterSystemActionListChanged(filterText: string): void {
        this.systemActionFilter = filterText;
    }

    finishAction(isLogic = false) {
        this.eventEditActive = false;
        this.currentActionBlock.isActionSave = true;
        if (isLogic) {
            switch (this.currentStep) {
                case WorkflowBuilderSteps.APPLY_EACH :
                this.intgWorkflowHelper.triggerSaveWorkflow();
                break;
            }

        }
    }

    setBackgroundColor(index: number) {
        this.currentHoveredLiIndex = index;
    }

    removeBackgroundColor() {
        this.currentHoveredLiIndex = null;
    }

    onTabClick(value: ITabsNav) {
        this.selectedTab = value.id;
    }

    backStep(previousStep: string) {
        if (this.currentStep > 1) {
            this.currentStep = WorkflowBuilderSteps[previousStep];
        }
        if (previousStep === WorkflowBuilderSteps[WorkflowBuilderSteps.TRIGGER_STEP]) {
            this.currentNodeDescription = null;
            this.currentActionBlock.httpNode = null;
            this.currentActionBlock.transformNode = null;
            this.intgWorkflowHelper.updateWorkflowAction(this.addedActionBlocks);
        }
        this.isLogicAction = false;
    }

    onEdit() {
        this.eventEditActive = !this.eventEditActive;
        this.currentActionBlock.isActionSave = false;
        this.editMode = true;
    }

    onLogicClick(logicStep: string) {
        this.currentStep = WorkflowBuilderSteps[logicStep];
        this.isLogicAction = true;
        switch (this.currentStep) {
            case WorkflowBuilderSteps.APPLY_EACH :
                this.logicIcon = "images/intg-systems/logic-icon.svg";
                this.logicText = "ApplyToEach";
                break;
            case WorkflowBuilderSteps.DELAY_LOGIC :
                this.logicIcon = "images/intg-systems/schedule-icon.svg";
                this.logicText = "Delay";
                break;
        }
    }

    onDelete() {
        this.deleteThisAction.emit();
    }
}
