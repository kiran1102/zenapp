// Angular
import {
    Component,
    Input,
    Output,
    EventEmitter,
    OnInit,
} from "@angular/core";

// Interface
import {
    IISeededEvent,
    IIIntegrationDetails,
    IIWorkflowDetail,
    IISelectedAction,
    IIWorkflow,
    IICredential,
    IIWebhookStatus,
    IITrigger,
    ICustomValueLookupFieldList,
    ICustomValueLookupField,
 } from "modules/intg/interfaces/integration.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";
import { IPageableOf } from "interfaces/pagination.interface";
import { ITabsNav } from "interfaces/one-tabs-nav.interface";
import { IStringMap } from "interfaces/generic.interface";

 // lodash
import { find } from "lodash";

// services
import { IntgCredentialsService } from "intsystemservice/intg-credentials.service";
import { IntgWorkflowHelper } from "intsystemservice/helpers/intg-workflow-helper.service";
import { IntgCustomValueService } from "intsystemservice/intg-custom-value.service";

// pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

// constants
import {
    SchedulerTerms,
    IntgTriggerEvents,
    EventCode,
} from "modules/intg/constants/integration.constant";

// 3rd Party
import {
    Subject,
    Observable,
    Subscription,
} from "rxjs";
import { takeUntil, take} from "rxjs/operators";

@Component({
    selector: "intg-workflow-builder-triggers",
    templateUrl: "./intg-workflow-builder-triggers.component.html",
})

export class IntgWorkflowBuilderTriggersComponent implements OnInit {
    @Input() events: IISeededEvent[];
    @Input() eventObservable: Observable<void>;
    @Input() integration: IIIntegrationDetails;
    @Input() workflow: IIWorkflowDetail;
    @Input() seededIntegration: IIIntegrationDetails;
    @Input() otActions: IIWorkflow[];
    @Input() selectedEventCode: string;
    @Input() isFinishButtonReady: boolean;
    @Input() addedActionBlocks: IISelectedAction[];
    @Output() passUpSchedule: EventEmitter<IITrigger> = new EventEmitter();
    @Output() passUpWebhook: EventEmitter<IITrigger> = new EventEmitter();
    @Output() passUpWebhookStatus: EventEmitter<IIWebhookStatus> = new EventEmitter();
    @Output() selectedEvent: EventEmitter<IISeededEvent> = new EventEmitter();
    @Output() isSchedulerInEditMode: EventEmitter<boolean> = new EventEmitter();

    triggerEditActive = true;
    schedulerEdit = false;
    currentEvent: IISeededEvent;
    assessmentTrigger: IISeededEvent[] = [];
    consentTrigger: IISeededEvent[] = [];
    dmTrigger: IISeededEvent[] = [];
    dsarTrigger: IISeededEvent[] = [];
    builtinTrigger: IISeededEvent[] = [];
    vendorTrigger: IISeededEvent[] = [];
    currentHoveredLiIndex: number;
    selectedActionList: IIWorkflowDetail[] = [];
    otActionIcon = "images/favicon.png";
    otCheckered = "images/checkered-blend.png";
    ready = false;
    systemActionFilter: string;
    scheduleActive = false;
    TABS = IntgTriggerEvents;
    tabs: ITabsNav[] = [];
    selectedTab: string = IntgTriggerEvents.ASSESSMENTS;
    webhookActive = false;
    customFields: ICustomValueLookupField[];
    SCHEDULE_TERMS = SchedulerTerms;
    credentials: IICredential[] = [
        {
            name: this.translatePipe.transform("None"),
            id: null,
            authContext: null,
            authType: null,
            integrationId: null,
        },
    ];
    eventObservableSubscription: any;
    completeWebhookObservable = new Subject<any>();
    allowedTabs: IStringMap<ITabsNav> = {
        ASSESSMENTS: {
            id: this.TABS.ASSESSMENTS,
            text: this.translatePipe.transform("Assessments"),
            otAutoId: "IntgTriggerAssessmentEventTabId",
            action: (option: ITabsNav): void => {
                this.selectedTab = option.id;
            },
        },
        CONSENT: {
            id: this.TABS.CONSENT,
            text: this.translatePipe.transform("Consent"),
            otAutoId: "IntgTriggerConsentEventTabId",
            action: (option: ITabsNav): void => {
                this.selectedTab = option.id;
            },
        },
        DATA_MAPPING: {
            id: this.TABS.DATAMAPPING,
            text: this.translatePipe.transform("WelcomeDMTitle"),
            otAutoId: "IntgTriggerDataMappingtEventTabId",
            action: (option: ITabsNav): void => {
                this.selectedTab = option.id;
            },
        },
        DATA_SUBJECT_REQUESTS: {
            id: this.TABS.DSAR,
            text: this.translatePipe.transform("DataSubjectRequests"),
            otAutoId: "IntgTriggerDSAREventTabId",
            action: (option: ITabsNav): void => {
                this.selectedTab = option.id;
            },
        },
        VENDOR: {
            id: this.TABS.VENDOR,
            text: this.translatePipe.transform("Vendor"),
            otAutoId: "IntgTriggerVendorEventTabId",
            action: (option: ITabsNav): void => {
                this.selectedTab = option.id;
            },
        },
        BUILT_IN: {
            id: this.TABS.BUILTIN,
            text: this.translatePipe.transform("Builtin"),
            otAutoId: "IntgTriggerBuiltinEventTabId",
            action: (option: ITabsNav): void => {
                this.selectedTab = option.id;
            },
        },
    };

    private destroy$ = new Subject();

    constructor(
        private intgCredentialsService: IntgCredentialsService,
        private translatePipe: TranslatePipe,
        private intgWorkflowHelper: IntgWorkflowHelper,
        private intgCustomValueService: IntgCustomValueService,
    ) {}

    ngOnInit() {
        this.tabs.push(this.allowedTabs.ASSESSMENTS);
        this.tabs.push(this.allowedTabs.CONSENT);
        this.tabs.push(this.allowedTabs.DATA_MAPPING);
        this.tabs.push(this.allowedTabs.DATA_SUBJECT_REQUESTS);
        this.tabs.push(this.allowedTabs.BUILT_IN);
        this.tabs.push(this.allowedTabs.VENDOR);
        this.getCredentials();
        this.getCustomParams();
        const selectedEvent = find(this.events, (event: IISeededEvent) => {
            return this.selectedEventCode === event.code;
        });
        if (selectedEvent) {
            this.setCurrentEvent(selectedEvent, true);
        }
        this.eventObservable.pipe(
            takeUntil(this.destroy$),
        ).subscribe(() => {
            this.completeWebhookObservable.next();
        });
        this.events.forEach((event: IISeededEvent) => {
            switch (event.module) {
                case IntgTriggerEvents.ASSESSMENTS:
                    this.assessmentTrigger.push(event);
                    break;
                case IntgTriggerEvents.CONSENT:
                    this.consentTrigger.push(event);
                    break;
                case IntgTriggerEvents.DATAMAPPING:
                    this.dmTrigger.push(event);
                    break;
                case IntgTriggerEvents.DSAR:
                    this.dsarTrigger.push(event);
                    break;
                case IntgTriggerEvents.BUILTIN:
                    this.builtinTrigger.push(event);
                    break;
                case IntgTriggerEvents.VENDOR:
                    this.vendorTrigger.push(event);
                    break;
            }
        });
    }

    ngOnDestroy() {
        this.destroy$.next();
        this.destroy$.complete();
    }

    getCredentials() {
        this.intgCredentialsService.getCredentials().then((credentials: IProtocolResponse<IPageableOf<IICredential>>) => {
            if (credentials.result)
                this.credentials.push(...credentials.data.content as  IICredential[]);
        });
    }

    onTabClick(value: ITabsNav) {
        this.selectedTab = value.id;
    }

    setCurrentEvent(event: IISeededEvent, onLoad: boolean) {
        this.currentEvent = event;
        const eventType =  parseInt(event.code, 10);
        const isEventTrigger =  eventType !== EventCode.SCHEDULER && eventType !== EventCode.WEBHOOK;
        if (isEventTrigger) this.selectedEvent.emit(event);
        this.triggerEditActive = false;
        this.scheduleActive = false;
        this.webhookActive = false;
        if (event.name === (SchedulerTerms.SCHEDULER) && !onLoad) {
            this.scheduleActive = true;
            this.schedulerEdit = this.triggerEditActive;
        }
        if (event.name === (SchedulerTerms.WEBHOOK) && !onLoad) {
            this.webhookActive = true;
        }
    }

    filterSystemActionListChanged(filterText: string) {
        this.systemActionFilter = filterText;
    }

    setEditMode(event: boolean) {
        this.isSchedulerInEditMode.emit(event);
    }

    addActionItem() {
        this.addedActionBlocks.push({
            httpNode: null,
            isNewNode: true,
        });
    }

    onPassUpSchedule($event: IITrigger) {
        this.passUpSchedule.emit($event);
        this.scheduleActive = false;
        this.deactivateTriggerVariables();
    }

    onPassUpWebhook($event: IITrigger) {
        this.passUpWebhook.emit($event);
        this.webhookActive = false;
        this.deactivateTriggerVariables();
    }

    onPassUpWebhookStatus($event: IIWebhookStatus) {
        this.passUpWebhookStatus.emit($event);
    }

    deactivateTriggerVariables() {
        this.triggerEditActive = false;
    }

    onEdit() {
        this.currentEvent = null;
        this.selectedEvent.emit(null);
        this.triggerEditActive ? this.triggerEditActive = false : this.triggerEditActive = true;
    }

    onDelete(index: number) {
        if (index >= 0) {
            if (!this.addedActionBlocks[index].isNewNode) {
                this.intgWorkflowHelper.setUnsavedChanges(true);
            }
            this.addedActionBlocks.splice(index, 1);
        } else {
            this.currentEvent = undefined;
            this.selectedEvent.emit(null);
            this.scheduleActive = false;
            this.triggerEditActive = true;
        }
    }

    updateCredentialList(credentialList: IICredential[]) {
        const cred = this.credentials[0];
        this.credentials = [];
        this.credentials.push(cred);
        this.credentials.push(...credentialList);
    }

    getCustomParams() {
        this.intgCustomValueService.getCustomValuesByWorkflow(this.workflow.id).
            pipe(
                take(1),
            )
            .subscribe((response: IProtocolResponse<ICustomValueLookupFieldList>) => {
                if (response.result) {
                    this.customFields = response.data.paramNames;
                }
        });
    }

}
