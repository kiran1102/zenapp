// Angular
import { Component } from "@angular/core";

// Vitreus
import { IOtModalContent } from "@onetrust/vitreus";

// Rxjs
import { Subject } from "rxjs";

// Services
import { IntgWorkflowService } from "intsystemservice/intg-workflow.service";

// Interfaces
import { IProtocolResponse } from "interfaces/protocol.interface";

@Component({
    selector: "intg-workflow-parse-schema-modal",
    templateUrl: "intg-workflow-parse-schema-modal.component.html",
})
export class IntgWorkflowParseSchemaModal implements IOtModalContent {

    otModalCloseEvent: Subject<string>;
    responsePayload: string;
    isLoading = false;
    invalidResponse = false;

    constructor(
        private intgWorkflowApi: IntgWorkflowService,
    ) {
    }

    parseSchema(): void {
        this.isLoading = true;
        this.intgWorkflowApi.extractWorkflowSchema(this.responsePayload).subscribe((schema: IProtocolResponse<unknown>) => {
            this.isLoading = false;
            if (schema.result) {
                this.otModalCloseEvent.next(JSON.stringify(schema.data, null, "\t"));
                this.otModalCloseEvent.complete();
            }
            this.invalidResponse = !schema.result;
        });
    }

    closeModal(): void {
        this.otModalCloseEvent.next();
        this.otModalCloseEvent.complete();
    }
}
