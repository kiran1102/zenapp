// Angular
import {
    OnInit,
    Component,
    Input,
    Output,
    EventEmitter,
} from "@angular/core";

// Interface
import {
    IISelectedAction,
    IIField,
    IISeededEvent,
    IITrigger,
} from "modules/intg/interfaces/integration.interface";
import { IKeyValue } from "interfaces/generic.interface";

// Constants and pipe.
import {
    ProcessType,
    SchedulerTerms,
 } from "modules/intg/constants/integration.constant";

@Component({
    selector: "intg-workflow-builder-logic-apply-each",
    templateUrl: "./intg-workflow-builder-logic-apply-each.component.html",
})
export class IntgWorkflowBuilderLogicApplyEachComponent implements OnInit {

    @Input() currentActionBlock: IISelectedAction;
    @Input() previousActionBlock: IISelectedAction;
    @Input() isFinishButtonReady: boolean;
    @Input() trigger: IITrigger;
    @Input() currentEvent: IISeededEvent;
    @Output() finishAction = new EventEmitter();

    allowOtherPrependText = "AddJsonPath";
    lookUpFields: IIField[];
    jsonPathList: string[];
    selectedJsonPath: IIField;
    inputValue: string;

    ngOnInit() {
        if (this.currentActionBlock && this.currentActionBlock.applyEachNode) {
            const applyEachContext = this.currentActionBlock.applyEachNode.processContext.ApplyEach;
            this.selectedJsonPath = {
                jsonPath: applyEachContext.jsonPath,
                formattedKey: this.formatLookUpKey(applyEachContext.listKey),
                key: applyEachContext.listKey,
            } as IIField;
        }
        const lookupValues: IIField[] = this.currentEvent.name === SchedulerTerms.WEBHOOK ? this.trigger.lookupFields.fields : this.currentEvent.eventFields.fields;
        this.previousActionBlock ? this.extractLookupField(this.previousActionBlock) : this.extractLookupFieldForEvent(lookupValues);
    }

    finishApplyEachAction() {
        if (this.currentActionBlock && !this.currentActionBlock.applyEachNode) {
            const previousResponseSchema = this.previousActionBlock && this.previousActionBlock.httpNode ? this.previousActionBlock.httpNode.responseSchema : null;
            this.currentActionBlock.applyEachNode =  {
                nodeLabel: null,
                processType: ProcessType.APPLY_EACH,
                workflowId: null,
                id: null,
                responseSchema: previousResponseSchema ? { ...previousResponseSchema, id: null, sample: null, lookupFields: null } :
                this.currentEvent.name === SchedulerTerms.WEBHOOK ? { schema: this.trigger.schema, name: "Webhook Trigger", id: null, sample: null, lookupFields: null } :
                { schema: this.currentEvent.eventSchema, name: this.currentEvent.name, id: null, sample: null, lookupFields: null},
                processContext: {
                    ApplyEach: {
                        type: ProcessType.APPLY_EACH,
                        jsonPath: this.selectedJsonPath.jsonPath,
                        listKey: this.selectedJsonPath.key,
                    },
                },
            };
        } else {
            this.currentActionBlock.applyEachNode.processContext.ApplyEach.jsonPath = this.selectedJsonPath.jsonPath;
            this.currentActionBlock.applyEachNode.processContext.ApplyEach.listKey = this.selectedJsonPath.key;
        }
        this.finishAction.emit();
    }

    selectJsonPath({ currentValue }) {
        this.selectedJsonPath = currentValue;
    }

    onInputChange({ key, value }: IKeyValue<string>) {
        if (key === "Enter") return;
        this.inputValue = value;
    }

    handleCustomOption(value: string) {
        const selectedOp: IIField = { name,  body : "", key: value, formattedKey: value, jsonPath: this.inputValue };
        this.selectedJsonPath = selectedOp;
    }

    private extractLookupField(actionBlock: IISelectedAction) {
        if (actionBlock.httpNode.responseSchema) {
            const fields =  actionBlock.httpNode.responseSchema.lookupFields ? actionBlock.httpNode.responseSchema.lookupFields.fields : [];
            this.lookUpFields = fields.filter((field: IIField) => field.type === "ARRAY");
            this.lookUpFields.forEach((lookUp: IIField) => {
                lookUp.formattedKey = this.formatLookUpKey(lookUp.key);
           });
        }
    }

    private extractLookupFieldForEvent(lookupValues: IIField[]) {
        this.lookUpFields = lookupValues.filter((field: IIField) => field.type === "ARRAY");
        this.lookUpFields.forEach((lookUp: IIField) => {
            lookUp.formattedKey = this.formatLookUpKey(lookUp.key);
        });
    }

    private  formatLookUpKey(key: string): string {
        // Regular expression to match string containing everything between 'step.' and '.' inclusive.
        const regex = new RegExp(/(step\.).*?(\.)/, "g");
        return key.replace(regex, "");
    }
}
