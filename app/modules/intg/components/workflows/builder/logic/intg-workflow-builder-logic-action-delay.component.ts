// Angular
import {
    OnInit,
    Component,
} from "@angular/core";

// Interface
import { IKeyValue } from "interfaces/generic.interface";

// Constants and pipe.
import {
    DelayUnit,
    DelayType,
    DelayTypeKeys,
 } from "modules/intg/constants/integration.constant";
import { TranslatePipe } from "modules/pipes/translate.pipe";

@Component({
    selector: "intg-workflow-builder-logic-action-delay",
    templateUrl: "./intg-workflow-builder-logic-action-delay.component.html",
})
export class IntgWorkflowBuilderLogicActionDelayComponent implements OnInit {

    selectedDelayType: IKeyValue<string>;
    selectedDelayUnit: IKeyValue<string>;
    delayCount: number;

    delayTypeOptions: Array<IKeyValue<string>> = [
        {
            key: this.translatePipe.transform(DelayTypeKeys.WAIT),
            value: DelayType.WAIT,
        },
        {
            key: this.translatePipe.transform(DelayTypeKeys.HOLD_UNTIL),
            value: DelayType.HOLD_UNTIL,
        },
    ];

    delayUnitOptions: Array<IKeyValue<string>> = [
        {
            key: this.translatePipe.transform(DelayUnit.MONTH),
            value: DelayUnit.MONTH,
        },
        {
            key: this.translatePipe.transform(DelayUnit.WEEK),
            value: DelayUnit.WEEK,
        },
        {
            key: this.translatePipe.transform(DelayUnit.DAY),
            value: DelayUnit.DAY,
        },
        {
            key: this.translatePipe.transform(DelayUnit.HOUR),
            value: DelayUnit.HOUR,
        },
        {
            key: this.translatePipe.transform(DelayUnit.MINUTE),
            value: DelayUnit.MINUTE,
        },
    ];

    constructor(
        private translatePipe: TranslatePipe,
    ) {}

    ngOnInit() {
        this.selectedDelayType = this.delayTypeOptions[0];
        this.selectedDelayUnit = this.delayUnitOptions[0];
    }

    selectType(type: IKeyValue<string>) {
        this.selectedDelayType = type;
    }

    selectUnit(unit: IKeyValue<string>) {
        this.selectedDelayUnit = unit;
    }
}
