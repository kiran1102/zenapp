// Angular
import {
    Component,
    Input,
    Output,
    EventEmitter,
} from "@angular/core";

// Interface
import {
    IISeededEvent,
    IIWorkflowDetail,
    IIScheduleItemObject,
    IIScheduleItemDayObject,
    IITrigger,
 } from "modules/intg/interfaces/integration.interface";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

// Constants
import {
    FrequencyType,
    TriggerType,
    EventCode,
} from "modules/intg/constants/integration.constant";

@Component({
    selector: "intg-workflow-builder-schedule-item",
    templateUrl: "./intg-workflow-builder-schedule-item.component.html",
})

export class IntgWorkflowBuilderScheduleItemComponent {

    @Input() savedSchedule?: IITrigger;
    @Input() selectedEventCode: string;
    @Input() workflow: IIWorkflowDetail;
    @Input() currentEvent: IISeededEvent;
    @Input() editMode: boolean;

    @Output() passUpSchedule: EventEmitter<IITrigger> = new EventEmitter();
    @Output() deleteThisSchedule: EventEmitter<IISeededEvent> = new EventEmitter();
    @Output() isSchedulerInEditMode: EventEmitter<boolean> = new EventEmitter();

    dayOptionsModel: string;
    selectedFrequencyOption;
    selectedTimeOfDayOption;
    selectedDayOfWeekOption;
    scheduleItemInactive = false;
    schedulePayload: IITrigger;

    frequencyOptions: IIScheduleItemObject[] = [
        {
            label: this.translatePipe.transform("Daily"), value: "1", cronExp: "1/1",
        },
        {
            label: this.translatePipe.transform("Weekly"), value: "2", cronExp: "?",
        },
    ];

    dayOptions: any[] = [
        { label: "S", value: "1", cronExp: "SUN" },
        { label: "M", value: "2", cronExp: "MON" },
        { label: "T", value: "3", cronExp: "TUE" },
        { label: "W", value: "4", cronExp: "WED" },
        { label: "T", value: "5", cronExp: "THU" },
        { label: "F", value: "6", cronExp: "FRI" },
        { label: "S", value: "7", cronExp: "SAT" },
    ];

    timeOfDayOptions: any[] = [
        { label: "0:00", value: 0 },
        { label: "1:00", value: 1 },
        { label: "2:00", value: 2 },
        { label: "3:00", value: 3 },
        { label: "4:00", value: 4 },
        { label: "5:00", value: 5 },
        { label: "6:00", value: 6 },
        { label: "7:00", value: 7 },
        { label: "8:00", value: 8 },
        { label: "9:00", value: 9 },
        { label: "10:00", value: 10 },
        { label: "11:00", value: 11 },
        { label: "12:00", value: 12 },
        { label: "13:00", value: 13 },
        { label: "14:00", value: 14 },
        { label: "15:00", value: 15 },
        { label: "16:00", value: 16 },
        { label: "17:00", value: 17 },
        { label: "18:00", value: 18 },
        { label: "19:00", value: 19 },
        { label: "20:00", value: 20 },
        { label: "21:00", value: 21 },
        { label: "22:00", value: 22 },
        { label: "23:00", value: 23 },
    ];

    constructor(
        private translatePipe: TranslatePipe,
    ) {}

    ngOnInit() {
        if (this.savedSchedule && this.savedSchedule.triggerType === TriggerType.SCHEDULED) {
            this.isSchedulerInEditMode.emit(this.editMode);
            this.scheduleItemInactive = !this.editMode;
            this.schedulePayload = this.savedSchedule;
        } else {
            this.isSchedulerInEditMode.emit(true);
            this.scheduleItemInactive = false;
            this.schedulePayload = {
                id: this.savedSchedule ? this.savedSchedule.id : null,
                workflowId: this.workflow.id,
                name : this.workflow.name,
                description : this.workflow.description,
                eventCode : EventCode.SCHEDULER.toString(),
                triggerType: TriggerType.SCHEDULED,
                triggerContext: {
                    ScheduledTrigger: {
                        cronExpression: "0 0 1 ? * THU *",
                        dateTime: {
                            seconds: "0",
                            minutes: "0",
                            hours: { label: "0:00", value: "0" },
                            dayOfMonth: null,
                            month: null,
                            dayOfWeek: { label: "M", value: "2", cronExp: "MON" },
                            year: null,
                            frequency: { label: this.translatePipe.transform("Daily"), value: "1", cronExp: "1/1" },
                            frequencyIntervalTracker: "?",
                        },
                        type: TriggerType.SCHEDULED,
                    },
                },
            };
        }
        this.selectedFrequencyOption = this.schedulePayload.triggerContext.ScheduledTrigger.dateTime.frequency;
        this.selectedTimeOfDayOption = this.schedulePayload.triggerContext.ScheduledTrigger.dateTime.hours;
        if (this.schedulePayload.triggerContext.ScheduledTrigger.dateTime.dayOfWeek) {
            this.selectedDayOfWeekOption = this.schedulePayload.triggerContext.ScheduledTrigger.dateTime.dayOfWeek.value;
        }
    }

    clearSelection() {
        this.dayOptionsModel = "";
    }

    frequencySelectOption(value: IIScheduleItemObject) {
        this.selectedFrequencyOption = value;
        this.schedulePayload.triggerContext.ScheduledTrigger.dateTime.frequency = value;
        this.updateCronPayload();
    }

    timeOfDaySelectOption(value: IIScheduleItemObject) {
        this.selectedTimeOfDayOption = value;
        this.schedulePayload.triggerContext.ScheduledTrigger.dateTime.hours = value;
        this.updateCronPayload();
    }

    dayOfWeekSelectOption(selection: IIScheduleItemDayObject) {
        this.selectedDayOfWeekOption = selection.value;
        this.schedulePayload.triggerContext.ScheduledTrigger.dateTime.dayOfWeek = selection;
        this.updateCronPayload();
    }

    finishSchedule() {
        this.passUpSchedule.emit(this.schedulePayload);
        this.scheduleItemInactive = true;
        this.isSchedulerInEditMode.emit(false);
    }

    onEdit() {
        this.passUpSchedule.emit(null);
        this.savedSchedule = null;
        this.isSchedulerInEditMode.emit(true);
        this.scheduleItemInactive = false;
    }

    onDelete() {
        this.passUpSchedule.emit(null);
        this.savedSchedule = null;
        this.isSchedulerInEditMode.emit(true);
        this.deleteThisSchedule.emit();
    }

    updateCronPayload() {
        if (this.schedulePayload.triggerContext.ScheduledTrigger.dateTime.frequency.value === FrequencyType.DAILY.toString()) {
            this.schedulePayload.triggerContext.ScheduledTrigger.dateTime.frequencyIntervalTracker = "?";
        } else if (this.schedulePayload.triggerContext.ScheduledTrigger.dateTime.frequency.value === FrequencyType.WEEKLY.toString()) {
            this.schedulePayload.triggerContext.ScheduledTrigger.dateTime.frequencyIntervalTracker = this.schedulePayload.triggerContext.ScheduledTrigger.dateTime.dayOfWeek.cronExp;
        }
        this.schedulePayload.triggerContext.ScheduledTrigger.cronExpression = `0 0 ${this.schedulePayload.triggerContext.ScheduledTrigger.dateTime.hours.value} ${this.schedulePayload.triggerContext.ScheduledTrigger.dateTime.frequency.cronExp} * ${this.schedulePayload.triggerContext.ScheduledTrigger.dateTime.frequencyIntervalTracker} *`;
    }
}
