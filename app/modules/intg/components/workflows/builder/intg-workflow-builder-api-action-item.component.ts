// Angular
import {
    Component,
    Input,
    Output,
    EventEmitter,
    SimpleChanges,
    OnInit,
    OnChanges,
    OnDestroy,
} from "@angular/core";
import {
    FormGroup,
    FormBuilder,
    Validators,
    FormArray,
} from "@angular/forms";

// components
import { IntgFreemarkerValidatorComponent } from "intsystemcomponent/workflows/builder/freemarkerValidation/intg-freemarker-validator-modal.component";

// 3rd party
import {
    forEach,
    find,
    map,
    keys,
} from "lodash";

// Rxjs
import {
    filter,
    takeUntil,
} from "rxjs/operators";
import { Subject } from "rxjs";

// Services
import { IntgCredentialsService } from "intsystemservice/intg-credentials.service";
import { ModalService } from "sharedServices/modal.service";
import { IntgWorkflowHelper } from "intsystemservice/helpers/intg-workflow-helper.service";
import { IntgCredentialsHelper } from "intsystemservice/helpers/intg-credentials-helper.service";
import {
    OtModalService,
    ModalSize,
} from "@onetrust/vitreus";

// Interfaces
import {
    IStringMap,
    IKeyValue,
} from "interfaces/generic.interface";
import { ITabsNav } from "interfaces/one-tabs-nav.interface";
import {
    IICredential,
    IISeededEvent,
    IIIntegrationDetails,
    IIWorkflowDetail,
    IIField,
    IIWorkflowNode,
    IISelectedAction,
    IIFreemarkerInputModal,
} from "modules/intg/interfaces/integration.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";
import { IPageableOf } from "interfaces/pagination.interface";

// Enums & Constants
import { WorkflowBuilderSteps } from "enums/integration.enum";
import {
    IntgActionTabs,
    KeyCode,
    LookUpValueOptionType,
    NextSteps,
    HideTab,
} from "modules/intg/constants/integration.constant";
import {
    NO_BLANK_SPACE,
    HOST_NAME,
} from "constants/regex.constant";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

@Component({
    selector: "intg-workflow-builder-api-action-item",
    templateUrl: "./intg-workflow-builder-api-action-item.component.html",
})

export class IntgWorkflowBuilderAPIActionItemComponent implements OnInit, OnChanges, OnDestroy {

    @Input() integration: IIIntegrationDetails;
    @Input() seededIntegration: IIIntegrationDetails;
    @Input() otActionIcon: string;
    @Input() currentEvent: IISeededEvent;
    @Input() workflow: IIWorkflowDetail;
    @Input() currentIndex: number;
    @Input() addedActionBlocks: IISelectedAction[];
    @Input() selectedEventCode: number;
    @Input() credentials: IICredential[];
    @Input() workflowNodeDetail: IIWorkflowNode;
    @Input() isOtActionSelected: boolean;
    @Input() currentNodeDescription: string;
    @Input() editMode: boolean;
    @Input() availableEvent: Array<{category: string, isOtEvent: boolean, fields: IIField[]}>;

    @Output() deleteThisAction: EventEmitter<string> = new EventEmitter();
    @Output() backToAddAction: EventEmitter<string> = new EventEmitter();
    @Output() updateCredentialList: EventEmitter<IICredential[]> = new EventEmitter();
    @Output() finishAction = new EventEmitter();

    hideValidatorIcon = HideTab;
    selectedOption = [];
    requestForm: FormGroup;
    currentCredential: IICredential;
    currentStep: number = WorkflowBuilderSteps.CREDENTIAL_STEP;
    isLookUpOpen = false;
    tabsJson: ITabsNav[] = [];
    selectedTabJson: string = IntgActionTabs.REQUEST;
    lookupFieldsFilter: string;
    open = true;
    inputValue: string;
    TABS = IntgActionTabs;
    WORKFLOW_STEPS = WorkflowBuilderSteps;
    allowOtherPrependText = "AddOption";
    currentActionBlock: IISelectedAction;
    responseSchema: string;
    responseSample: string;
    allowedTabs: IStringMap<ITabsNav> = this.intgWorkflowHelper.getAllowedTabs();
    methodTypes: Array<IKeyValue<string>> = this.intgWorkflowHelper.getMethodTypes();
    headerTypes: Array<IKeyValue<string>> = this.intgWorkflowHelper.getHeaderTypes();

    private destroy$ = new Subject();

    constructor(
        public translatePipe: TranslatePipe,
        private readonly modalService: ModalService,
        private readonly formBuilder: FormBuilder,
        private intgCredentialsService: IntgCredentialsService,
        private intgWorkflowHelper: IntgWorkflowHelper,
        private intgCredentialsHelper: IntgCredentialsHelper,
        private otModalService: OtModalService,
    ) {}

    ngOnInit() {
        this.tabsJson.push(this.allowedTabs.REQUEST);
        this.tabsJson.push(this.allowedTabs.RESPONSE);
        this.tabsJson.push(this.allowedTabs.SAMPLE);
        this.currentActionBlock = this.addedActionBlocks[this.currentIndex];
        this.nextStep(NextSteps.CREDENTIAL_STEP);
        if (this.currentActionBlock.httpNode) {
            this.setEditedFormValues();
        }
        if (this.currentActionBlock.httpNode.id || this.editMode) {
            this.nextStep(NextSteps.REQUEST_RESPONSE_STEP);
        }
    }

    ngOnChanges(changes: SimpleChanges) {
        if (changes.workflow) {
            if (this.currentActionBlock && this.currentActionBlock.httpNode) {
                this.workflowNodeDetail = this.currentActionBlock.httpNode;
                if (this.workflowNodeDetail && this.workflowNodeDetail.responseSchema) {
                    this.responseSchema = JSON.stringify(JSON.parse(this.workflowNodeDetail.responseSchema.schema), undefined, 3);
                    this.responseSample = JSON.stringify(JSON.parse(this.workflowNodeDetail.responseSchema.sample), undefined, 3);
                }
            }
        }
    }

    openParseSchemaModal() {
        this.intgWorkflowHelper.launchParseSchemaModal().pipe(
            filter(Boolean),
            takeUntil(this.destroy$),
        ).subscribe((schema: string) => {
            this.responseSchema = schema;
        });
    }

    nextStep(nextStep: string) {
        if (nextStep === WorkflowBuilderSteps[WorkflowBuilderSteps.CREDENTIAL_STEP]) {
            this.requestForm = this.initializeForm();
            this.workflowNodeDetail = this.currentActionBlock.httpNode;
            if (this.workflowNodeDetail) {
                this.currentNodeDescription = this.intgWorkflowHelper.getActionName(this.workflowNodeDetail.nodeDescription);
            }
        }
        if (this.isOtActionSelected) {
            this.responseSchema = this.workflowNodeDetail.responseSchema ? JSON.stringify(JSON.parse(this.workflowNodeDetail.responseSchema.schema), undefined, 3) : null;
            this.responseSample = this.workflowNodeDetail.responseSchema ? JSON.stringify(JSON.parse(this.workflowNodeDetail.responseSchema.sample), undefined, 3) : null;
            this.setCredentialNextStep(this.credentials[0]);
        } else {
            this.currentStep = WorkflowBuilderSteps[nextStep];
        }
        if (this.workflowNodeDetail && this.workflowNodeDetail.responseSchema) {
            this.responseSchema = JSON.stringify(JSON.parse(this.workflowNodeDetail.responseSchema.schema), undefined, 3);
            this.responseSample = JSON.stringify(JSON.parse(this.workflowNodeDetail.responseSchema.sample), undefined, 3);
        }
    }

    ngOnDestroy() {
        this.destroy$.next();
        this.destroy$.complete();
    }

    setCredentialNextStep(credential: IICredential) {
        this.currentCredential = credential;
        this.currentActionBlock.credentialId = this.currentCredential.id;
        const hostname = this.intgCredentialsHelper.getHostname(credential);
        if (hostname) {
            const httpWorkflowNode = this.currentActionBlock.httpNode;
            this.requestForm.patchValue({
                requestUrl: httpWorkflowNode.processContext.HttpConnector.requestURI.replace(HOST_NAME, `://${hostname}`),
            });
        }
        if (this.isOtActionSelected) {
            this.currentStep = this.WORKFLOW_STEPS.REQUEST_RESPONSE_STEP;
        }
    }

    initializeForm(): FormGroup {
        let actionBody = "";
        const transformNode = this.currentActionBlock.transformNode;
        const httpNode = this.currentActionBlock.httpNode;
        if (transformNode && transformNode.processContext.DataTransformer.template) {
            actionBody = transformNode.processContext.DataTransformer.template;
        } else if (httpNode.requestSchema && httpNode.requestSchema.sample ) {
            actionBody = JSON.stringify(JSON.parse(httpNode.requestSchema.sample), undefined, 2);
        }
        const headerArray = this.getOptionalHeaderFormArray(httpNode && httpNode.processContext.HttpConnector.headers);
        return this.formBuilder.group({
            defaultHeader: headerArray,
            method: [this.getSelectedMethodType(httpNode.processContext.HttpConnector.httpMethod), Validators.required],
            requestUrl: [ httpNode.processContext.HttpConnector.requestURI, [Validators.required, this.intgWorkflowHelper.refernceUrlValidator]],
            requestTextArea: [actionBody],
        });
    }

    onInputChange({ key, value }: IKeyValue<string>) {
        if (key === KeyCode.ENTER) return;
        this.inputValue = value;
    }

    openFreemarkerValidatorModal() {
        const freemarkerInputTemplate: IIFreemarkerInputModal = {
            event: this.currentEvent,
            addedActions: this.addedActionBlocks,
            currentActionIndex: this.currentIndex,
            template: this.requestForm.value.requestTextArea,
        };

        this.otModalService
        .create(
            IntgFreemarkerValidatorComponent,
            {
                isComponent: true,
                size: ModalSize.LARGE,
            },
            freemarkerInputTemplate,
        );
    }

    onModelChange({ currentValue }, index) {
        this.selectedOption[index] = currentValue;
        const control = this.requestForm.controls["defaultHeader"] as FormArray;
        const selectedHeader = control.at(index);
        selectedHeader.patchValue({ key: currentValue ? currentValue.value : null});
        if (currentValue) {
            if (selectedHeader.get("value").value) {
                selectedHeader.get("key").setErrors(null);
            } else {
                selectedHeader.get("value").setErrors({ incorrect: true });
            }
        } else {
            if (selectedHeader.get("value").value) {
                selectedHeader.get("key").setErrors({ incorrect: true });
            } else {
                selectedHeader.get("key").setErrors(null);
            }
        }
    }

    onLookUpClick(valueBody: string) {
        this.intgWorkflowHelper.insertAtCursorPosition(document.getElementsByName("IntgWorkflowLookupText" + this.currentIndex)[0] as HTMLTextAreaElement, valueBody);
        this.requestForm.patchValue({ requestTextArea: (document.getElementsByName("IntgWorkflowLookupText" + this.currentIndex)[0] as HTMLTextAreaElement).value});
    }

    addCredential() {
        const modalData = {
            callback: (result: boolean, response: IICredential) => {
                if (result) {
                    this.setCredentialNextStep(response);
                    this.nextStep(NextSteps.REQUEST_RESPONSE_STEP);
                    this.refreshCredentialList();
                }
            },
        };
        this.modalService.setModalData(modalData);
        this.modalService.openModal("intgCredentialsAddModal");
    }

    onRemoveOptionClick(index: number) {
        const control = this.requestForm.controls["defaultHeader"] as FormArray;
        control.removeAt(index);
        this.selectedOption.splice(index, 1);
    }

    handleFirstOption(value: string, index: number) {
        const selectedOp = { value, key: value, optionType: LookUpValueOptionType.OTHERS };
        this.selectedOption[index] = selectedOp;
        const control = this.requestForm.controls["defaultHeader"] as FormArray;
        const selectedHeader = control.at(index);
        if (selectedHeader) {
            selectedHeader.patchValue({ key: value });
        }
    }

    onAddOptionClick() {
        const control = this.requestForm.controls["defaultHeader"] as FormArray;
        control.push(this.formBuilder.group({
            key: ["", Validators.pattern(NO_BLANK_SPACE)],
            value: ["", Validators.pattern(NO_BLANK_SPACE)],
        }));
    }

    onValueChange(value: string, index: number) {
        const control = this.requestForm.controls["defaultHeader"] as FormArray;
        const selectedHeader = control.at(index);
        if (value.trim()) {
            if (selectedHeader.get("key").value) {
                selectedHeader.get("key").setErrors(null);
            } else {
                selectedHeader.get("key").setErrors({ incorrect: true });
            }
        } else {
            if (selectedHeader.get("key").value) {
                selectedHeader.get("value").setErrors({ incorrect: true });
            } else {
                selectedHeader.get("key").setErrors(null);
            }
        }
    }

    refreshCredentialList() {
        this.intgCredentialsService.getCredentials().then(
            (response: IProtocolResponse<IPageableOf<IICredential>>) => {
                if (response.result)
                    this.updateCredentialList.emit(response.data.content);
            });
    }

    filterLookupFields(filterText: string): void {
        this.lookupFieldsFilter = filterText;
        this.open = true;
    }

    onTabJsonClick(value: ITabsNav) {
        this.selectedTabJson = value.id;
    }

    onLookUpButtonToggle(isOpen: boolean) {
        this.isLookUpOpen = isOpen;
    }

    onLookUpButtonClick() {
        this.isLookUpOpen = !this.isLookUpOpen;
    }

    backStep(previousStep: string) {
        this.currentStep = WorkflowBuilderSteps[previousStep];
        if (previousStep === WorkflowBuilderSteps[WorkflowBuilderSteps.TRIGGER_STEP]) {
            this.backToAddAction.emit();
        }
    }

    finishApiAction() {
        this.currentActionBlock.isActionSave = true;
        this.currentActionBlock.requestURI = this.requestForm.value.requestUrl;
        this.currentActionBlock.requestBody = this.requestForm.value.requestTextArea;
        this.currentActionBlock.headers = this.getHeaders();
        this.currentActionBlock.actionMethod = this.requestForm.value.method.value;
        const transformNode = this.currentActionBlock.transformNode;
        const httpNode = this.currentActionBlock.httpNode;
        if (transformNode) {
            transformNode.responseSchema = this.intgWorkflowHelper.getResponseSchema(transformNode, this.responseSchema);
        }
        httpNode.responseSchema = this.intgWorkflowHelper.getResponseSchema(httpNode, this.responseSchema);
        this.finishAction.emit();
    }

    onDelete() {
        this.deleteThisAction.emit();
    }

    private getHeaders(): IStringMap<string> {
        const headers = {};
        const headerArray = this.requestForm.get("defaultHeader").value;
        forEach(headerArray, (header: IKeyValue<string>) => {
            if (header.key) {
                headers[header.key] = header.value;
            }
        });
        return headers;
    }

    private getOptionalHeaderFormArray(headers?: IStringMap<string>): FormArray {
        let index = 0;
        if (keys(headers).length) {
            return this.formBuilder.array(map(headers, (value: string, key: string) => {
                this.selectedOption[index++] = { key, value: key };
                return this.formBuilder.group({
                    key: [key, Validators.pattern(NO_BLANK_SPACE)],
                    value: [value, Validators.pattern(NO_BLANK_SPACE)],
                });
            }));
        }
        this.selectedOption = [];
        return this.formBuilder.array([this.formBuilder.group({
            key: ["", Validators.pattern(NO_BLANK_SPACE)],
            value: ["", Validators.pattern(NO_BLANK_SPACE)],
        })]);
    }

    private getSelectedMethodType(methodType: string) {
        return find(this.methodTypes, (type) => type.value === methodType);
    }

    private setEditedFormValues() {
        this.currentCredential = find(this.credentials, (credential ) => {
            return this.currentActionBlock.credentialId === credential.id;
        });
        if (this.requestForm  && this.currentActionBlock.requestURI) {
            this.requestForm.patchValue({
                requestUrl: this.currentActionBlock.requestURI,
                requestTextArea: this.currentActionBlock.requestBody,
                method: this.getSelectedMethodType(this.currentActionBlock.actionMethod),
            });
            this.setHeaderfromHeaderArray();
        }
    }

    private setHeaderfromHeaderArray() {
        const headersArray = this.getOptionalHeaderFormArray(this.currentActionBlock.headers);
        this.requestForm.setControl("defaultHeader", headersArray);
    }
}
