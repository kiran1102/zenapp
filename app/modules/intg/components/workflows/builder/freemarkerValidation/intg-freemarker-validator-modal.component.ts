// Angular
import {
    Component,
    OnInit,
} from "@angular/core";

// services
import { IOtModalContent } from "@onetrust/vitreus";
import { IntgWorkflowService } from "intsystemservice/intg-workflow.service";

// interfaces
import { IISeededEvent,
        IIFreeMarkerTemplate,
        IISelectedAction,
        IIFreemarkerInputModal,
} from "modules/intg/interfaces/integration.interface";

// 3rd Party
import { Subject } from "rxjs";

@Component({
    selector: "intg-freemarker-validator",
    templateUrl: "./intg-freemarker-validator-modal.component.html",
})

export class IntgFreemarkerValidatorComponent implements IOtModalContent, OnInit {

    otModalCloseEvent: Subject<boolean>;
    otModalData: IIFreemarkerInputModal;
    selectedEvent: IISeededEvent;
    freemarkerPayload: IIFreeMarkerTemplate = {};
    currentActionIndex: number;
    addedActions: IISelectedAction[];
    actionResponseSchema: string[];
    responseData: any;
    isActionSaved = false;
    isValidationComplete = true;
    isCorrectJson = true;

    constructor(private intgWorkflowService: IntgWorkflowService) {}

    ngOnInit() {
        this.selectedEvent = this.otModalData.event;
        this.freemarkerPayload.event = JSON.parse(this.selectedEvent.eventSample);
        this.currentActionIndex = this.otModalData.currentActionIndex;
        this.addedActions = this.otModalData.addedActions;
        this.setActionsToShow();
    }

    closeModal() {
        this.otModalCloseEvent.next(false);
        this.otModalCloseEvent.complete();
    }

    transformPayload() {
        this.isValidationComplete = false;
        this.intgWorkflowService.validateFreemarker(this.freemarkerPayload).subscribe(
            (response) => {
                this.responseData = JSON.stringify(response.data, undefined, 2);
                this.isCorrectJson = this.isJson(this.responseData);
                this.isValidationComplete = true;
            });
    }

    private setActionsToShow() {
        const transformNode = this.addedActions[this.currentActionIndex].transformNode ? this.addedActions[this.currentActionIndex].transformNode.processContext.DataTransformer : null;
        this.isActionSaved = transformNode ? true : false;
        this.freemarkerPayload.template = this.otModalData.template ? this.otModalData.template : transformNode ? transformNode.template : "";
        const responseSchema = this.addedActions.map((action) => {
            return action.httpNode.responseSchema ? action.httpNode.responseSchema.sample : null;
        });
        this.actionResponseSchema = responseSchema.slice(0, this.currentActionIndex);

        this.constructActionSteps();
    }

    private constructActionSteps() {
        this.freemarkerPayload.step = {};
        if (this.isActionSaved) {
            this.actionResponseSchema.forEach((schema, index) => {
                this.freemarkerPayload.step[this.addedActions[index].httpNode.nodeLabel] = JSON.parse(schema);
            });
        } else {
            const requiredActions = this.addedActions.slice(0, this.currentActionIndex);
            requiredActions.forEach((action: IISelectedAction, index) => {
                this.freemarkerPayload.step["http_" + index] = action.httpNode.responseSchema ? JSON.parse(action.httpNode.responseSchema.sample) : null;
            });
        }
    }

    private isJson(payload: string): boolean {
        if (payload) {
            try {
                payload = JSON.parse(payload);
            } catch (e) {
                return false;
            }
            return (typeof payload === "object" && payload !== null);
        } else {
            return true;
        }
    }
}
