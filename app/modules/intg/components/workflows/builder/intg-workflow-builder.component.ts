// Angular
import {
    Component,
} from "@angular/core";
import {
    StateService,
    TransitionService,
    Transition,
    HookResult,
    StateOrName,
    RawParams,
    TransitionOptions,
} from "@uirouter/core";

// interfaces
import { IProtocolResponse } from "interfaces/protocol.interface";
import { IPageableOf } from "interfaces/pagination.interface";
import {
    IISeededEvent,
    IIWorkflowDetail,
    IIWorkflow,
    IICreateActions,
    IICreateWorkflow,
    IIWorkflowNode,
    IISelectedAction,
    IIDirection,
    IIWebhookStatus,
    IRequestSchema,
    IITrigger,
 } from "modules/intg/interfaces/integration.interface";
import { IIIntegrationDetails } from "modules/intg/interfaces/integration.interface";
import { IDropdownOption } from "interfaces/one-dropdown.interface";

// services
import { IntgEventTypeService } from "intsystemservice/intg-event-type.service";
import { IntgIntegrationsService } from "modules/intg/services/intg-integrations.service";
import { IntgWorkflowService } from "intsystemservice/intg-workflow.service";
import { Permissions } from "modules/shared/services/helper/permissions.service";
import { NotificationService } from "sharedModules/services/provider/notification.service";
import { IntgWorkflowHelper } from "intsystemservice/helpers/intg-workflow-helper.service";

// lodash
import {
    forEach,
    find,
    filter,
} from "lodash";

// constant
import {
    ProcessType,
    JsonFilename,
    SchedulerTerms,
    TriggerType,
    EventCode,
} from "modules/intg/constants/integration.constant";

// pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

// 3rd Party
import {
    Subject,
    Subscription,
} from "rxjs";
import { takeUntil } from "rxjs/operators";

import { IntgWorkflowBuiderTestModal } from "intsystemcomponent/workflows/builder/intg-workflow-builder-test-modal.component";
import {
    OtModalService,
    ModalSize,
    ConfirmModalType,
    ToastService,
} from "@onetrust/vitreus";

// Components
import { UnsavedChangesConfirmationModal } from "sharedModules/components/unsaved-changes-confirmation-modal/unsaved-changes-confirmation-modal.component";

@Component({
    selector: "intg-workflow-builder",
    templateUrl: "./intg-workflow-builder.component.html",
})

export class IntgWorkflowBuilderComponent {

    events: IISeededEvent[];
    event: IISeededEvent;
    trigger: IITrigger;
    selectedEventCode: string;
    workflow = {} as IIWorkflowDetail;
    integration: IIIntegrationDetails;
    seededIntegration: IIIntegrationDetails;
    addedActionBlocks: IISelectedAction[] = [];
    addedActionsNode: IIWorkflowNode[];
    publishHasPermission = this.permissions.canShow("IntgWorkflowBuilderPublish");
    testHasPermission = this.permissions.canShow("IntgWorkflowBuilderTest");
    isSaveWorkflowComplete = true;
    canTestWorkflow = false;
    publishingWorkflow = false;
    ready = false;
    otActions: IIWorkflow[];
    menuOptions: IDropdownOption[];
    messageLogNode: IIWorkflowNode;
    editMode: boolean;
    isFinishButtonReady: boolean;
    hasUnsavedChanges = false;
    savedTriggerId: string;
    saveWorkflowSubscription: Subscription;
    triggerWebhookSubscription: Subscription;
    unsavedChangesSubscription: Subscription;
    redirectSubscription: Subscription;
    webhookStatus: IIWebhookStatus = {
        fieldsIncomplete: false,
        htmlHidden: true,
    };

    private saveWebhookObservable = new Subject<any>();
    private redirectObservable = new Subject<any>();

    private destroy$ = new Subject();
    private transitionDeregisterHook = this.$transitions.onExit({ from: "zen.app.pia.module.intg.workflows.builder" }, (transition: Transition): HookResult => {
        return this.handleStateChange(transition.to(), transition.params("to"), transition.options());
    });

    constructor(
        private stateService: StateService,
        private intgEventTypeApi: IntgEventTypeService,
        private intgIntegrationsApi: IntgIntegrationsService,
        private intgWorkflowApi: IntgWorkflowService,
        private permissions: Permissions,
        private translatePipe: TranslatePipe,
        private notificationService: NotificationService,
        private otModalService: OtModalService,
        private intgWorkflowHelper: IntgWorkflowHelper,
        private toastService: ToastService,
        private $transitions: TransitionService,
    ) {}

    ngOnInit() {
        const promises = [];
        promises.push(this.intgEventTypeApi.getEventTypes());
        Promise.all(promises).then(
            (responses: Array<IProtocolResponse<IPageableOf<IISeededEvent>>>) => {
                this.events = responses[0].result && responses[0].data ? (responses[0].data as IPageableOf<IISeededEvent>).content : [];
            },
        );
        this.getWorkflowDetails(this.stateService.params.id);
        this.isFinishButtonReady = true;
        this.saveWorkflowSubscription = this.intgWorkflowHelper.saveWorkflow$.subscribe(() => {
            this.saveEvents();
        });
        this.triggerWebhookSubscription = this.intgWorkflowHelper.trigger$.subscribe((triggerPayload: IITrigger) => {
            this.trigger = triggerPayload;
        });
        this.unsavedChangesSubscription = this.intgWorkflowHelper.unsavedChanges$.subscribe((hasUnsavedChanges: boolean) => {
            this.hasUnsavedChanges = hasUnsavedChanges;
        });

    }

    ngOnDestroy() {
        this.saveWorkflowSubscription.unsubscribe();
        this.triggerWebhookSubscription.unsubscribe();
        this.unsavedChangesSubscription.unsubscribe();
        if (this.transitionDeregisterHook) {
            this.transitionDeregisterHook();
        }
        this.destroy$.next();
        this.destroy$.complete();
    }

    goToRoute(route: { path: string, params: any }) {
        this.stateService.go(route.path);
    }

    getWorkflowDetails(workflowId: string) {
        this.intgWorkflowApi.getWorkflowById(workflowId, true).then(
            (response: IProtocolResponse<IIWorkflowDetail>) => {
                if (response.result) {
                    this.workflow = response.data;
                    this.canTestWorkflow = this.workflow.workflowNodes.length > 0;
                    if (this.workflow.trigger) {
                        this.selectedEventCode = this.workflow.trigger.eventCode;
                        this.trigger = this.workflow.trigger;
                        this.savedTriggerId =  this.trigger.id;
                    }
                    this.getIntegration(this.workflow.integrationId);
                }
            },
        );
    }

    getIntegration(systemId: string) {
        this.intgIntegrationsApi.getDetails(systemId).then(
            (response: IProtocolResponse<IIIntegrationDetails>) => {
                if (response.result) {
                    this.integration = response.data;
                    this.getotActions();
                }
            },
        );
    }

    getSeededIntegrationWorkflows(systemId: string) {
        this.intgIntegrationsApi.getDetails(systemId).then(
            (response: IProtocolResponse<IIIntegrationDetails>) => {
                if (response.result) {
                    this.seededIntegration = response.data;
                    this.seededIntegration.workflows.forEach((workflowAction: IIWorkflow) => {
                        workflowAction.name = this.intgWorkflowHelper.getActionName(workflowAction.name);
                    });
                    this.ready = true;
                    const nodes = this.workflow.workflowNodes.filter((node: IIWorkflowNode) => node.processType === ProcessType.HTTP || node.processType === ProcessType.APPLY_EACH);
                    const transformNodes = filter(this.workflow.workflowNodes, (node: IIWorkflowNode) => node.processType === ProcessType.TRANSFORM);
                    forEach(nodes, (node: IIWorkflowNode) => {
                        if (node.processType === ProcessType.HTTP) {
                            const nodeNumber = Number(node.nodeLabel.substring(5, node.nodeLabel.length));
                            const transform = transformNodes.find((transformNode: IIWorkflowNode) => transformNode.nodeLabel === `transform_${nodeNumber}`);
                            this.addedActionBlocks.push({
                                credentialId: node.credentialsId,
                                actionMethod: node.processContext.HttpConnector.httpMethod,
                                headers: node.processContext.HttpConnector.headers,
                                isActionSave: true,
                                requestBody: transform ? transform.processContext.DataTransformer.template : "",
                                requestURI: node.processContext.HttpConnector.requestURI,
                                httpNode: node,
                                transformNode: transform,
                                isOtAction: node.processContext.HttpConnector.isInternal,
                                savedHttpNodeLabel: node.nodeLabel,
                                savedTransformNodeLabel: transform ? transform.nodeLabel : null,
                            });
                        } else if (node.processType === ProcessType.APPLY_EACH) {
                            this.addedActionBlocks.push({
                                isActionSave: true,
                                applyEachNode: node,
                            });
                        }
                    });
                    if (this.workflow.workflowNodes && this.workflow.workflowNodes.length) {
                        this.messageLogNode = this.workflow.workflowNodes.find((node: IIWorkflowNode) => {
                            return node.processType === ProcessType.MESSAGE_LOG;
                        });
                    }
                }
            },
        );
    }

    getotActions() {
        this.intgIntegrationsApi.getOtIntegrations().then(
            (response: IProtocolResponse<IPageableOf<IIWorkflow>>) => {
                if (response.result) {
                    this.otActions = response.data.content;
                    this.otActions.forEach((otActions: IIWorkflow) => {
                        otActions.name = this.intgWorkflowHelper.getActionName(otActions.name);
                    });
                    this.getSeededIntegrationWorkflows(this.integration.referenceIntegrationId);
                }
            },
        );
    }

    checkAllActionSaved() {
        const isActionsInEditMode = this.addedActionBlocks.some((node: IISelectedAction) => {
            return !node.isActionSave;
        });

        const canDisableSave = !this.ready || !this.trigger ||
                                // Scheduler is in edit mode.
                                (this.trigger.eventCode === EventCode.SCHEDULER.toString() && this.editMode) ||
                                // Webhook is in edit mode
                                (this.saveWebhookDisabled() || !this.webhookStatus.htmlHidden) ||
                                // Some of the actions are in edit mode.
                                isActionsInEditMode ||
                                // Saving is in progress.
                                !this.isSaveWorkflowComplete;
        if (canDisableSave) {
            this.hasUnsavedChanges = true;
        }
        return canDisableSave;
    }

    setSchedulerEditMode(event) {
        this.editMode = event;
    }

    saveWebhookDisabled(): boolean {
        if (this.trigger.eventCode === EventCode.WEBHOOK.toString() && (
            !this.workflow.trigger ||
            this.webhookStatus.fieldsIncomplete)) {
                return true;
        }
        return false;
    }

    activateOrDeactivateWorkflow(isEnabled: boolean) {
        const workflow: IICreateWorkflow = {
            id: this.workflow.id,
            description: this.workflow.description,
            enabled: isEnabled,
            integrationId: this.workflow.integrationId,
            name: this.workflow.name,
            workflowType:  this.workflow.workflowType,
        };

        this.publishingWorkflow = true;

        this.intgWorkflowApi.saveWorkflow(workflow).then((response: IProtocolResponse<IIWorkflow>) => {
            if (response.result) {
                this.workflow.enabled = isEnabled;
                this.workflow.halted = response.data.halted;
            }
            this.publishingWorkflow = false;
        });
    }

    saveEvents() {
        this.saveEventWorkFlowMapping();
    }

    saveWorkflow(redirect: boolean = false) {
        const workflowDetail: IICreateActions = {
            id: this.workflow.id,
            name: this.workflow.name,
            description: this.workflow.description,
            integrationId: this.workflow.integrationId,
            enabled: false,
            workflowType: this.workflow.workflowType,
            actions: this.getActions(this.workflow.id),
            directions: [],
            trigger: this.trigger,
        };
        workflowDetail.directions = this.getDirectionEdges(workflowDetail);
        this.isFinishButtonReady = false;
        this.intgWorkflowApi.createWorkflow(workflowDetail).then((response: IProtocolResponse<IIWorkflowDetail>) => {
            if (response.result) {
                this.workflow = response.data;
                this.trigger = this.workflow.trigger;
                this.savedTriggerId =  this.trigger.id;
                this.canTestWorkflow = this.workflow.workflowNodes.length > 0;
                this.setActionResponse(response.data);
                const successMsg = this.translatePipe.transform("WorkflowCreatedSuccessfully");
                this.notificationService.alertSuccess(this.translatePipe.transform("Success"), successMsg);
                this.intgWorkflowHelper.updateWorkflowAction(this.addedActionBlocks);
                this.hasUnsavedChanges = false;
                if (redirect) {
                    this.redirectObservable.next();
                    this.redirectSubscription.unsubscribe();
                }
                if (this.workflow.enabled && this.workflow.workflowNodes.length === 0) {
                    this.activateOrDeactivateWorkflow(false);
                }
            }
            this.isFinishButtonReady = true;
            this.isSaveWorkflowComplete = true;
            this.saveWebhookObservable.next();
        });
    }

    selectedEvent(event: IISeededEvent) {
        this.event = event;
        if (event) {
            const triggerId = this.savedTriggerId || null;
            this.trigger = {
                id: triggerId,
                description: event.name,
                triggerType: TriggerType.EVENT,
                eventCode: event.code,
                schema: event.eventSchema,
                name: event.name,
                workflowId: this.workflow.id,
                triggerContext: {
                    EventTrigger: {
                        type: TriggerType.EVENT,
                        eventCode: event.code,
                    },
                },
            };
        } else {
            this.trigger = null;
        }
        this.workflow.trigger = this.trigger;
    }

    openTestModal() {
        this.otModalService
            .create(
                IntgWorkflowBuiderTestModal,
                {
                    isComponent: true,
                    size: ModalSize.MEDIUM,
                }, {
                    workflowId: this.workflow.id,
                    event: this.events.find((event) => event.code === this.trigger.eventCode),
                    trigger: this.workflow.trigger,
                },
            );
    }

    openActivateWarningModal() {
        this.otModalService
        .confirm({
            type: ConfirmModalType.WARNING,
            translations: {
                title: this.translatePipe.transform("Confirm"),
                desc: this.translatePipe.transform("ActivateWarningModalText"),
                confirm: this.translatePipe.transform("AreYouSureContinue"),
                submit: this.translatePipe.transform("Ok"),
                cancel: this.translatePipe.transform("Cancel"),
            },
        }).pipe(
            takeUntil(this.destroy$),
        ).subscribe((data: boolean) => {
            if (data) {
                this.activateOrDeactivateWorkflow(true);
            }
        });
    }

    getContextActions() {
        this.menuOptions = [
            {
                text: this.translatePipe.transform("TestWorkflow"),
                action: (): void => {
                    if (this.canTestWorkflow) {
                        this.openTestModal();
                    }
                },
                isDisabled: !this.canTestWorkflow,
            },
            {
                text: this.translatePipe.transform("ExportWorkflow"),
                action: (): void => {
                    this.intgWorkflowApi.exportWorkflow(this.workflow.id, JsonFilename.OTWORKFLOW);
                },
            },
        ];
    }

    onPassUpSchedule(event: IITrigger) {
        this.trigger = event;
        this.workflow.trigger = this.trigger;
    }

    onPassUpWebhook(event: IITrigger) {
        this.trigger = event;
        this.saveEvents();
    }

    onPassUpWebhookStatus(event: IIWebhookStatus) {
        this.webhookStatus = event;
    }

    isActivateFeasible() {
        if (this.workflow.workflowNodes && this.workflow.workflowNodes.length === 0 || this.checkAllActionSaved() || this.publishingWorkflow ) {
            this.toastService.show(this.translatePipe.transform("Error"), this.translatePipe.transform("CannotActivateWorkflow"), "error");
        } else {
            this.openActivateWarningModal();
        }
    }

    private openUnsavedChangesModal() {
        this.otModalService
            .create(
                UnsavedChangesConfirmationModal,
                {
                    isComponent: true,
                    size: ModalSize.SMALL,
                    hideClose: true,
                },
                {
                    promiseToResolve: () => this.saveEventWorkFlowMapping(true),
                    discardCallback: () => {
                        this.redirectObservable.next();
                        this.redirectSubscription.unsubscribe();
                    },
                },
            );
    }

    private handleStateChange(toState: StateOrName, toParams: RawParams, options: TransitionOptions): boolean {
        if (!this.hasUnsavedChanges || this.checkAllActionSaved()) return true;
        this.redirectSubscription = this.redirectObservable.subscribe(() => {
            this.hasUnsavedChanges = false;
            this.stateService.go(toState, toParams, options);
        });
        this.openUnsavedChangesModal();
        return false;
    }

    private getActions(workflowId: string) {
        const nodes: IIWorkflowNode[] = [];
        let previousSequence = -1;
        let previousApplyEachSequence = -1;
        const actionsNotSaved = this.addedActionBlocks.filter((addedAction: IISelectedAction) => !(addedAction.isNewNode && !addedAction.isActionSave));
        forEach(actionsNotSaved, (action: IISelectedAction, index) => {
            if (action.applyEachNode) {
                if (!action.applyEachNode.nodeLabel) {
                    action.applyEachNode.nodeLabel = `applyEach_${previousApplyEachSequence + 1}`;
                }
                previousApplyEachSequence = Number(action.applyEachNode.nodeLabel.substring(10, action.applyEachNode.nodeLabel.length));
                nodes.push(action.applyEachNode);
            } else {
                let requestSchema: Partial<IRequestSchema> = null;
                let responseSchema: Partial<IRequestSchema> = null;
                let transformRequestSchema: Partial<IRequestSchema> = null;
                let transformResponseSchema: Partial<IRequestSchema> = null;
                if (action.addingNewAction) {
                    requestSchema = action.httpNode.requestSchema ? { ...action.httpNode.requestSchema, id: null } : null;
                    responseSchema = action.httpNode.responseSchema ? { ...action.httpNode.responseSchema, id: null } : null;
                    if (action.transformNode) {
                        transformRequestSchema = action.transformNode.requestSchema ? { ...action.transformNode.requestSchema, id: null } : null;
                        transformResponseSchema = action.transformNode.responseSchema ? { ...action.transformNode.responseSchema, id: null } : null;
                    } else {
                        transformRequestSchema = requestSchema;
                        transformResponseSchema = responseSchema;
                    }
                } else {
                    requestSchema = action.httpNode.requestSchema;
                    responseSchema = action.httpNode.responseSchema;
                    transformRequestSchema = action.transformNode.requestSchema;
                    transformResponseSchema = action.transformNode.responseSchema;
                }

                requestSchema = this.modifySchemaForSave(requestSchema);
                transformRequestSchema = this.modifySchemaForSave(transformRequestSchema);
                responseSchema = this.modifySchemaForSave(responseSchema);
                transformResponseSchema = this.modifySchemaForSave(transformResponseSchema);

                const httpAction: IIWorkflowNode = {
                    credentialsId: action.credentialId,
                    nodeLabel: action.httpNode && action.httpNode.nodeLabel ? action.httpNode.nodeLabel : `http_${previousSequence + 1}`,
                    processType: ProcessType.HTTP,
                    workflowId,
                    id: action.httpNode ? action.httpNode.id : null,
                    requestSchema,
                    responseSchema,
                    nodeDescription: this.intgWorkflowHelper.getActionName(action.httpNode.nodeDescription),
                    processContext: {
                        HttpConnector: {
                            type: ProcessType.HTTP,
                            headers: action.headers,
                            requestURI: action.requestURI,
                            httpMethod: action.actionMethod,
                            isInternal: action.isOtAction,
                        },
                    },
                };
                nodes.push(httpAction);
                action.httpNode = httpAction;
                let transformLabel: string = null;
                if (action.transformNode && action.transformNode.nodeLabel) {
                    transformLabel = action.transformNode.nodeLabel;
                } else if (action.savedTransformNodeLabel) {
                    transformLabel = action.savedTransformNodeLabel;
                }
                const transformAction: IIWorkflowNode = {
                    nodeLabel: transformLabel || `transform_${previousSequence + 1}`,
                    processType: ProcessType.TRANSFORM,
                    workflowId,
                    id: action.transformNode ? action.transformNode.id : null,
                    requestSchema: transformRequestSchema,
                    responseSchema: transformResponseSchema,
                    nodeDescription: action.transformNode ? this.intgWorkflowHelper.getActionName(action.transformNode.nodeDescription) : action.httpNode.nodeDescription,
                    processContext: {
                        DataTransformer: {
                            type: ProcessType.TRANSFORM,
                            template: action.requestBody,
                        },
                    },
                };
                nodes.push(transformAction);
                action.transformNode = transformAction;
                previousSequence = Number(httpAction.nodeLabel.substring(5, httpAction.nodeLabel.length));
            }
        });

        if (nodes.length) {
            if (!this.messageLogNode) {
                this.messageLogNode = this.intgWorkflowHelper.createMessageLogNode(workflowId);
            }
            nodes.push(this.messageLogNode);
        }
        return nodes;
    }

    private saveEventWorkFlowMapping(redirect?: boolean) {
        this.isSaveWorkflowComplete = false;
        this.saveWorkflow(redirect);
    }

    private modifySchemaForSave(schema: Partial<IRequestSchema>): Partial<IRequestSchema> {
        if (schema) {
            schema = { ...schema, lookupFields: null, sample: null };
        }
        return schema;
    }

    private setActionResponse(savedWorkflow: IIWorkflowDetail) {
        forEach(this.addedActionBlocks, (actionBlock: IISelectedAction) => {

            if (actionBlock.httpNode) {
                const savedHttpNode = find(savedWorkflow.workflowNodes, (node: IIWorkflowNode) => node.nodeLabel === actionBlock.httpNode.nodeLabel);
                const savedTransformNode = find(savedWorkflow.workflowNodes, (node: IIWorkflowNode) => node.nodeLabel === actionBlock.transformNode.nodeLabel);
                actionBlock.httpNode = savedHttpNode;
                actionBlock.transformNode = savedTransformNode;
                actionBlock.savedHttpNodeLabel = savedHttpNode.nodeLabel;
                actionBlock.savedTransformNodeLabel = savedTransformNode.nodeLabel;
                actionBlock.isNewNode = false;
            } else if (actionBlock.applyEachNode) {
                const savedApplyEachNode = savedWorkflow.workflowNodes.find((node: IIWorkflowNode) => node.nodeLabel === actionBlock.applyEachNode.nodeLabel);
                actionBlock.applyEachNode = savedApplyEachNode;
            }
            actionBlock.addingNewAction = false;
            this.messageLogNode = savedWorkflow.workflowNodes.find((node: IIWorkflowNode) =>  node.processType === ProcessType.MESSAGE_LOG);
        });
    }

    private getDirectionEdges(workflow: IICreateActions): IIDirection[] {
        const nodes: IIWorkflowNode[] = workflow.actions.filter((node: IIWorkflowNode) => node.processType === ProcessType.TRANSFORM || node.processType === ProcessType.APPLY_EACH);
        let httpNode: IIWorkflowNode[] = workflow.actions.filter((node: IIWorkflowNode) => node.processType === ProcessType.HTTP);
        let transformNode: IIWorkflowNode[] = workflow.actions.filter((node: IIWorkflowNode) => node.processType === ProcessType.TRANSFORM);
        const logNode: IIWorkflowNode = workflow.actions.find((node: IIWorkflowNode) => node.processType === ProcessType.MESSAGE_LOG);
        httpNode = httpNode.sort((a, b) => a.nodeLabel.localeCompare(b.nodeLabel));
        transformNode = transformNode.sort((a, b) => a.nodeLabel.localeCompare(b.nodeLabel));
        const directions: IIDirection[] = [];
        forEach(nodes, (action: IIWorkflowNode, index) => {
            const nextNode: IIWorkflowNode = nodes[index + 1];

            if (action.processType === ProcessType.TRANSFORM) {
                const sequence = action.nodeLabel.substring(10, action.nodeLabel.length);
                directions.push({
                    startNode: action.nodeLabel,
                    endNode: `http_${sequence}`,
                });
                if (nextNode) {
                    directions.push({
                        startNode: `http_${sequence}`,
                        endNode: nextNode.nodeLabel,
                    });
                }
            } else if (nextNode) {
                directions.push({
                    startNode: action.nodeLabel,
                    endNode: nextNode.nodeLabel,
                });
            }
        });
        let lastEdge: IIDirection = null;
        if (directions.length) {
            lastEdge = directions[directions.length - 1];
            directions.push({
                startNode: lastEdge.endNode,
                endNode: logNode.nodeLabel,
            });
        }
        return directions;
    }
}
