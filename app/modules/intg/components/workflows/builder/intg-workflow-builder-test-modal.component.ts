import {
    Component,
    OnInit,
} from "@angular/core";

// 3rd Party
import { Subject } from "rxjs";

// Interfaces
import { IOtModalContent } from "@onetrust/vitreus";
import {
    ITestWorkflow,
    IISeededEvent,
    IITrigger,
} from "modules/intg/interfaces/integration.interface";
import { IProtocolError } from "interfaces/protocol.interface";
import { IStringMap } from "interfaces/generic.interface";
import { ITabsNav } from "interfaces/one-tabs-nav.interface";

// Service
import { IntgWorkflowService } from "intsystemservice/intg-workflow.service";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

// Enums and Constants
import {
    IntgTestResponseTabs,
    TriggerType,
    EventCode,
} from "modules/intg/constants/integration.constant";
import { StatusCodes } from "enums/status-codes.enum";

@Component({
    selector: "intg-workflow-builder-test-modal",
    templateUrl: "intg-workflow-builder-test-modal.component.html",
})
export class IntgWorkflowBuiderTestModal implements IOtModalContent, OnInit {
    otModalCloseEvent: Subject<boolean>;
    workflowId: string;
    eventId: string;
    otModalData: ITestWorkflow;
    payload: string;
    ready = true;
    TABS = IntgTestResponseTabs;
    selectedTab: string = IntgTestResponseTabs.RESPONSE;
    tabs: ITabsNav[] = [];
    responseData: string | IProtocolError = "";
    responseLogs: string | IProtocolError = "";
    responseStatusText: string;
    statusFailure: boolean;
    event: IISeededEvent;
    eventCode: number;
    trigger: IITrigger;
    isCorrectJson = true;
    allowedTabs: IStringMap<ITabsNav> = {
        RESPONSE: {
            id: this.TABS.RESPONSE,
            text: this.translatePipe.transform("Response"),
            action: (option: ITabsNav): void => {
                this.selectedTab = option.id;
            },
        },
        LOGS: {
            id: this.TABS.LOGS,
            text: this.translatePipe.transform("Logs"),
            action: (option: ITabsNav): void => {
                this.selectedTab = option.id;
            },
        },
    };

    constructor(
        private intgWorkflowService: IntgWorkflowService,
        private translatePipe: TranslatePipe,
    ) {}

    ngOnInit() {
        this.tabs.push(this.allowedTabs.RESPONSE);
        this.tabs.push(this.allowedTabs.LOGS);
        this.workflowId = this.otModalData.workflowId;
        this.event = this.otModalData.event;
        this.trigger = this.otModalData.trigger;
        if (this.trigger && this.trigger.sample) {
            this.payload = JSON.stringify(JSON.parse(this.trigger.sample), undefined, 2);
        } else if (this.event) {
            this.payload = JSON.stringify(JSON.parse(this.event.eventSample), undefined, 2);
        }
        if (this.trigger.triggerType === TriggerType.SCHEDULED) {
            this.eventCode = EventCode.SCHEDULER;
        } else if (this.trigger.triggerType === TriggerType.WEBHOOK) {
            this.eventCode = EventCode.WEBHOOK;
        } else {
            this.eventCode = EventCode.EVENT;
        }
     }

    closeModal() {
        this.otModalCloseEvent.next(false);
    }

    saveTestWorkflow() {
        if (this.isJson(this.payload)) {
            this.isCorrectJson = true;
            this.ready = false;
            let responseText;
            this.intgWorkflowService.testWorkflow(this.workflowId, this.payload, this.eventCode).then((response) => {
                this.ready = true;
                if (response.result) {
                    this.responseData = response.data ? JSON.stringify(response.data.body, undefined, 2) : "";
                    this.responseLogs = response.data ? JSON.stringify(response.data.traceLogs, undefined, 2) : "";
                    responseText =  response.data ? `${response.data.body.statusCodeValue}  ${response.data.body.statusCode ? response.data.body.statusCode : ""}` : "";
                    this.statusFailure = response.data.httpStatus >= StatusCodes.BadRequest;

                } else {
                    this.responseData = response ? JSON.stringify(response, undefined, 2) : "";
                    responseText =  `${response.status}  ${response.data ? response.data : ""}`;
                    this.statusFailure = true;
                }
                this.responseStatusText = `${this.translatePipe.transform("Status")}: ${responseText}`;
            });
        } else {
            this.isCorrectJson = false;
        }
    }

    onTabClick(value: ITabsNav) {
        this.selectedTab = value.id;
    }

    private isJson(bodyValue: string): boolean {
        if (bodyValue) {
            try {
                bodyValue = JSON.parse(bodyValue);
            } catch (e) {
                return false;
            }
            return (typeof bodyValue === "object" && bodyValue !== null);
        } else {
            return true;
        }
    }
}
