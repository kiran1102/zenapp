// Angular
import {
    Component,
    Input,
    Output,
    EventEmitter,
} from "@angular/core";

// Interface
import {
    IISeededEvent,
    IIWorkflowDetail,
    IIWebhookStatus,
    IITrigger,
 } from "modules/intg/interfaces/integration.interface";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

// Constants
import {
        TriggerType,
        EventCode,
} from "modules/intg/constants/integration.constant";

// Rxjs
import {
    Observable,
    Subject,
} from "rxjs";
import {
    filter,
    takeUntil,
} from "rxjs/operators";

// Vitreous
import { OtModalService } from "@onetrust/vitreus";

// Utilities
import Utilities from "Utilities";
import { NotificationService } from "modules/shared/services/provider/notification.service";

// services
import { IntgWorkflowHelper } from "intsystemservice/helpers/intg-workflow-helper.service";

@Component({
    selector: "intg-workflow-builder-webhook-item",
    templateUrl: "./intg-workflow-builder-webhook-item.component.html",
})

export class IntgWorkflowBuilderWebhookItemComponent {

    @Input() savedWebhook?: IITrigger;
    @Input() workflow: IIWorkflowDetail;
    @Input() isFinishButtonReady: boolean;
    @Input() webhookChildObservable: Observable<void>;
    @Output() passUpWebhook = new EventEmitter<IITrigger>();
    @Output() passUpWebhookStatus = new EventEmitter<IIWebhookStatus>();
    @Output() deleteThisWebhook = new EventEmitter<IISeededEvent>();

    webhookItemInactive = false;
    webhookIsLoading = false;
    webhookPayload: IITrigger;
    webhookUrl: string;
    webhookJson: string;
    webhookSubscription: any;
    webhookCurrentlyActive: boolean;
    isValidJson: boolean;

    private destroy$ = new Subject();

    constructor(
        private translatePipe: TranslatePipe,
        private notificationService: NotificationService,
        private intgWorkflowHelper: IntgWorkflowHelper,
    ) {}

    ngOnInit() {
        this.webhookSubscription = this.webhookChildObservable.subscribe(() => {
            this.webhookItemInactive = true;
            this.webhookIsLoading = false;
            this.webhookActive();
        });
        if (this.savedWebhook && this.savedWebhook.triggerType === TriggerType.WEBHOOK) {
            this.webhookPayload = this.savedWebhook;
            this.webhookJson = JSON.stringify(JSON.parse(this.webhookPayload.schema), null, "\t");
            this.webhookItemInactive = true;
            this.webhookIsLoading = false;
            this.webhookActive("true");
        } else {
            this.webhookPayload = {
                id: this.savedWebhook ? this.savedWebhook.id : null,
                workflowId: this.workflow.id,
                name: this.workflow.name,
                description: this.workflow.description,
                eventCode: EventCode.WEBHOOK.toString(),
                triggerType: TriggerType.WEBHOOK,
                triggerContext: {
                    WebHookTrigger: {
                        type: TriggerType.WEBHOOK,
                        eventCode: EventCode.WEBHOOK.toString(),
                        referenceUrl: `${window.location.origin}/integrationmanager/api/v1/webhook/${this.workflow.id}`,
                    },
                },
                lookupFields: null,
                sample: null,
                schema: null,
            };
        }
        this.webhookUrl = this.webhookPayload.triggerContext && this.webhookPayload.triggerContext.WebHookTrigger
                         ? this.webhookPayload.triggerContext.WebHookTrigger.referenceUrl
                         : `${window.location.origin}/integrationmanager/api/v1/webhook/${this.workflow.id}`;
        if (this.webhookJson === "null") {
            this.webhookJson = "";
        }
    }

    ngOnDestroy() {
        this.webhookSubscription.unsubscribe();
        this.destroy$.next();
        this.destroy$.complete();
    }

    webhookActive(activeStatus?: string): boolean {
        this.webhookCurrentlyActive = (!this.webhookUrl || !this.webhookJson);
        if (activeStatus) {
            this.webhookCurrentlyActive = false;
        }
        this.passUpWebhookStatus.emit({
            fieldsIncomplete: this.webhookCurrentlyActive,
            htmlHidden: this.webhookItemInactive,
        });
        return this.webhookCurrentlyActive;
    }

    copyWebhookUrl() {
        Utilities.copyToClipboard(this.webhookUrl);
        this.notificationService.alertSuccess(this.translatePipe.transform("WebhookCopied"), this.translatePipe.transform("WebhookCopiedSuccessfully"));
    }

    finishWebhook() {
        this.webhookPayload.schema = this.webhookJson;
        this.passUpWebhook.emit(this.webhookPayload);
        this.webhookIsLoading = true;
    }

    openParseSchemaModal() {
        this.intgWorkflowHelper.launchParseSchemaModal().pipe(
            filter(Boolean),
            takeUntil(this.destroy$),
        ).subscribe((schema: string) => {
            this.webhookJson = schema;
            this.isValidJson = this.isJson(this.webhookJson);
        });
    }

    onEdit() {
        this.webhookItemInactive = false;
        this.webhookActive("true");
        this.isValidJson = this.isJson(this.webhookJson);
    }

    onDelete() {
        this.deleteThisWebhook.emit();
        this.passUpWebhookStatus.emit({
            fieldsIncomplete: false,
            htmlHidden: true,
        });
    }

    saveModel(event: any) {
        this.webhookJson = event.target.value;
        this.isValidJson = this.isJson(this.webhookJson);
    }

    ngOnChanges(): void {
        this.webhookCurrentlyActive = (!this.webhookUrl || !this.webhookJson);
    }

    private isJson(payload: string): boolean {
        if (payload) {
            try {
                payload = JSON.parse(payload);
            } catch (e) {
                return false;
            }
            return (typeof payload === "object" && payload !== null);
        } else {
            return true;
        }
    }
}
