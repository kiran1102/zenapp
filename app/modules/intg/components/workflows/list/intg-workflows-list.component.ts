import {
    Component,
    OnInit,
} from "@angular/core";

// services
import { IntgWorkflowService } from "intsystemservice/intg-workflow.service";
import { ModalService } from "sharedServices/modal.service";
import { Permissions } from "modules/shared/services/helper/permissions.service";
import { NotificationService } from "sharedModules/services/provider/notification.service";
import { IntgIntegrationsService } from "intsystemservice/intg-integrations.service";

// pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

// enums and constants
import { TableColumnTypes } from "enums/data-table.enum";
import { JsonFilename } from "modules/intg/constants/integration.constant";

// lodash
import { forEach } from "lodash";

// interfaces
import {
    IIWorkflow,
    IIntgCustomSystemModal,
    IIIntegration,
    IIWorkflowDetail,
} from "modules/intg/interfaces/integration.interface";
import { ITableColumnConfig } from "interfaces/table-config.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";
import { IPageableOf } from "interfaces/pagination.interface";
import { IDropdownOption } from "interfaces/one-dropdown.interface";
import { IDeleteConfirmationModalResolve } from "interfaces/delete-confirmation-modal-resolve.interface";

// vitreus
import {
    OtModalService,
    ConfirmModalType,
    ToastService,
} from "@onetrust/vitreus";

// component
import { IntgImportModal } from "intsystemcomponent/workflows/import/intg-import-modal.component";

// rxjs
import { takeUntil } from "rxjs/operators";
import { Subject } from "rxjs";

@Component({
    selector: "intg-workflows-list",
    templateUrl: "./intg-workflows-list.component.html",
})
export class IntgWorkflowsListComponent implements OnInit {
    workFlowList: IIWorkflow[] = [];
    options: IDropdownOption[];
    ready = false;
    page: IPageableOf<IIWorkflow> = null;
    currentPage: number;
    readonly columns: ITableColumnConfig[] = [
        {
            name: this.translatePipe.transform("WorkflowName"),
            sortKey: 1,
            type: TableColumnTypes.Link,
            cellValueKey: "name",
        },
        {
            name: this.translatePipe.transform("Status"),
            sortKey: 2,
            type: TableColumnTypes.Status,
            cellValueKey: "enabled",
            component: "active-tag-table-cell",
        },
        {
            name: this.translatePipe.transform("System"),
            sortKey: 3,
            type: TableColumnTypes.Text,
            cellValueKey: "referenceIntegrationName",
        },
        {
            name: this.translatePipe.transform("DateCreated"),
            sortKey: 4,
            type: TableColumnTypes.Date,
            cellValueKey: "createdDate",
        },
        {
            name: "",
            type: TableColumnTypes.Action,
            cellValueKey: "",
        },
    ];

    readonly viewPermissions = {
        activateOrDeactivate: this.permissions.canShow("IntegrationsSystemActionsEdit"),
        delete: this.permissions.canShow("IntegrationsSystemActionsDelete"),
        export: this.permissions.canShow("IntegrationSystemActions"),
    };

    private destroy$ = new Subject();
    private workflow: IIWorkflowDetail;

    constructor(
        private intgWorkflowService: IntgWorkflowService,
        private translatePipe: TranslatePipe,
        private permissions: Permissions,
        private modalService: ModalService,
        private notificationService: NotificationService,
        private intgIntegrationService: IntgIntegrationsService,
        private otModalService: OtModalService,
        private toastService: ToastService,
    ) { }

    ngOnInit() {
        this.getWorkflowPage();
    }

    ngOnDestroy() {
        this.destroy$.next();
        this.destroy$.complete();
    }

    getWorkflowPage(page = this.currentPage, size = 20) {
        this.ready = false;
        return this.intgWorkflowService.getWorkflows(page, size).then(
            (workflowsResponse: IProtocolResponse<IPageableOf<IIWorkflow>>) => {
                this.workFlowList = workflowsResponse.data.content || [];
                const content = this.workFlowList.sort((a, b) => a.name.localeCompare(b.name));
                forEach(content, (workflow: IIWorkflow) => {
                    workflow.route = {
                        routePath: "zen.app.pia.module.intg.workflows.builder",
                        id: workflow.id,
                    };
                });
                this.page = {
                    first: workflowsResponse.data.first,
                    last: workflowsResponse.data.last,
                    number: workflowsResponse.data.number,
                    numberOfElements: workflowsResponse.data.numberOfElements,
                    size: workflowsResponse.data.size,
                    totalElements: workflowsResponse.data.totalElements,
                    totalPages: workflowsResponse.data.totalPages,
                    content,
                };
                this.ready = true;
            },
        );
    }

    getActions = (row) => {
        const actions: IDropdownOption[] = [];
        if (this.viewPermissions.activateOrDeactivate) {
            actions.push({
                text: row.enabled ? this.translatePipe.transform("Deactivate") : this.translatePipe.transform("Activate"),
                action: (): void => {
                    if (row.enabled) {
                        row.enabled = !row.enabled;
                        this.saveWorkflow(row);
                    } else {
                        this.getWorkflowDetails(row);
                    }
                },
            });
        }
        if (this.viewPermissions.delete) {
            actions.push({
                text: this.translatePipe.transform("Delete"),
                action: (): void => {
                    this.deleteWorkflow(row);
                },
            });
        }
        if (!actions.length) {
            actions.push({
                text: this.translatePipe.transform("NoActionsAvailable"),
                action: (): void => {},
            });
        }
        if (this.viewPermissions.export) {
            actions.push({
                text: this.translatePipe.transform("Export"),
                action: (): void => {
                    this.intgWorkflowService.exportWorkflow(row.id, JsonFilename.OTWORKFLOW);
                },
            });
        }
        if (row["halted"]) {
            actions.push({
                text: this.translatePipe.transform("Resume"),
                action: (): void => {
                    row.halted = !row.halted;
                    this.saveWorkflow(row);
                },
            });
        }
        return actions;
    }

    openAddWorkflowModal() {
        const moduleData: IIntgCustomSystemModal = {
            module: "intgWorkFlowCreateModal",
            callback: (result: boolean) => {
                if (result) {
                    this.getWorkflowPage();
                }
            },
        };
        this.modalService.setModalData(moduleData);
        this.modalService.openModal("intgWorkFlowCreateModal");
    }

    openActivateWarningModal(row: IIWorkflow) {
        this.otModalService
        .confirm({
            type: ConfirmModalType.WARNING,
            translations: {
                title: this.translatePipe.transform("Confirm"),
                desc: this.translatePipe.transform("ActivateWarningModalText"),
                confirm: this.translatePipe.transform("AreYouSureContinue"),
                submit: this.translatePipe.transform("Ok"),
                cancel: this.translatePipe.transform("Cancel"),
            },
        }).pipe(
            takeUntil(this.destroy$),
        ).subscribe((data: boolean) => {
            if (data) {
                row.enabled = !row.enabled;
                this.saveWorkflow(row);
            }
        });
    }

    openImportModal() {
        this.otModalService
            .create(
                IntgImportModal,
                {
                    isComponent: true,
                },
                {
                    addFromSystem: false,
                },
            );
    }

    handlePageChanged(page: number) {
        if (this.page.totalPages) {
            this.currentPage = page;
            this.getWorkflowPage(page, 20);
        }
    }

    getContextMenuActions(row: IIWorkflow) {
        this.options = this.getActions(row);
    }

    private deleteWorkflow(row: IIWorkflow) {
        const deleteModalData: IDeleteConfirmationModalResolve = {
            promiseToResolve: (): ng.IPromise<void> => {
                return this.intgWorkflowService.deleteWorkflow(row.id)
                    .then((response: IProtocolResponse<void>) => {
                        if (response.result) {
                            this.deleteIntegration(row.integrationId);
                        }
                    });
            },
            modalTitle: this.translatePipe.transform("DeleteWorkflow"),
            confirmationText: this.translatePipe.transform("AreYouSureDeleteWorkflow"),
            submitButtonText: this.translatePipe.transform("Delete"),
            cancelButtonText: this.translatePipe.transform("Cancel"),
        };
        this.modalService.setModalData(deleteModalData);
        this.modalService.openModal("otDeleteConfirmationModal");
    }

    private deleteIntegration(integrationId: string) {
        this.intgIntegrationService.deleteIntegration(integrationId)
            .then((response: IProtocolResponse<IIIntegration>) => {
                if (response.result) {
                    if (this.page.last && !this.page.first && this.page.numberOfElements === 1) {
                        this.currentPage = this.currentPage - 1;
                    }
                    this.getWorkflowPage();
                    this.modalService.closeModal();
                    this.modalService.clearModalData();
                }
            });
    }

    private getWorkflowDetails(workflow: IIWorkflow) {
        this.intgWorkflowService.getWorkflowById(workflow.id, true).then(
            (response: IProtocolResponse<IIWorkflowDetail>) => {
                if (response.result) {
                    this.workflow = response.data;
                    this.workflow.workflowNodes.length === 0 ? this.toastService.show(this.translatePipe.transform("Error"), this.translatePipe.transform("CannotActivateWorkflow"), "error") : this.openActivateWarningModal(workflow);
                }
            },
        );
    }

    private saveWorkflow(row: IIWorkflow) {
        this.ready = false;
        delete row.route;
        delete row.index;
        this.intgWorkflowService.saveWorkflow(row).then((response: IProtocolResponse<IIWorkflow>) => {
            if (response.result) {
                this.ready = true;
                const successMsg = response.data.enabled ? this.translatePipe.transform("WorkflowEnabled") : this.translatePipe.transform("WorkflowDisabled");
                this.notificationService.alertSuccess(this.translatePipe.transform("Success"), successMsg);
            }
            this.getWorkflowPage();
        });
    }
}
