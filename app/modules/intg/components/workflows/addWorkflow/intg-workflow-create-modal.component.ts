import {
    Component,
    OnInit,
    Inject,
} from "@angular/core";
import { StateService } from "@uirouter/core";

// services
import { ModalService } from "sharedServices/modal.service";
import { IntgWorkflowService } from "intsystemservice/intg-workflow.service";
import { IntgIntegrationsService } from "intsystemservice/intg-integrations.service";

// interfaces
import {
    IISystem,
    IICreateWorkflow,
    IIWorkflow,
    IIntgCustomSystemModal,
    IIIntegration,
    IICreateIntegration,
} from "modules/intg/interfaces/integration.interface";
import { IStore } from "interfaces/redux.interface";
import { IPageableOf } from "interfaces/pagination.interface";

// lodash
import { filter } from "lodash";

// enum
import { WorkflowType } from "modules/intg/enums/workflow.enum";

// Stores and reducers
import { IProtocolResponse } from "interfaces/protocol.interface";
import { StoreToken } from "tokens/redux-store.token";
import { getModalData } from "oneRedux/reducers/modal.reducer";

@Component({
    selector: "intg-workflow-create-modal",
    templateUrl: "./intg-workflow-create-modal.component.html",
})
export class IntgWorkFlowCreateModal implements OnInit {
    systems: IISystem[] = [];
    isLoading = true;
    selectedSystem: IISystem;
    integrationSystem = {} as IICreateIntegration;
    saveInProgress = false;
    isSystem = false;
    workflowModal = {} as IICreateWorkflow;
    private modalData: IIntgCustomSystemModal;

    constructor(
        @Inject(StoreToken) private store: IStore,
        private stateService: StateService,
        private modalService: ModalService,
        private intgIntegrationsService: IntgIntegrationsService,
        private intgWorkflowService: IntgWorkflowService,

    ) { }

    ngOnInit() {
        this.getCustomSystems();
        this.modalData = getModalData(this.store.getState());
    }

    closeModal(result = false) {
        if (this.modalData.callback) {
            this.modalData.callback(result);
        }
        this.modalService.closeModal();
        this.modalService.clearModalData();
    }

    setSystem(value: IISystem) {
        this.selectedSystem = value;
        this.isSystem = true;
        this.integrationSystem.templateId = value.templateId;
        this.integrationSystem.referenceIntegrationId = value.id;
        this.workflowModal.description = value.description;
        this.integrationSystem.referenceIntegrationName = value.name;
    }

    onClickAddWorkflow() {
        this.saveInProgress = true;
        this.workflowModal.workflowType = WorkflowType.COMPOSITE;
        this.workflowModal.enabled = false;
        this.intgWorkflowService.createIntegration(this.integrationSystem).then((newIntegrationDetails: IProtocolResponse<IIIntegration>) => {
            if (newIntegrationDetails.result) {
                this.workflowModal.integrationId = newIntegrationDetails.data.id;
                this.intgWorkflowService.saveWorkflow(this.workflowModal).then((response: IProtocolResponse<IIWorkflow>) => {
                    if (response.result) {
                        this.closeModal(true);
                        this.stateService.go("zen.app.pia.module.intg.workflows.builder", {
                            id: response.data.id,
                        });
                    }
                });
            }
            this.saveInProgress = false;
        });
    }

    setWorkflowName(value: string) {
        this.workflowModal.name = value;
        this.integrationSystem.name = value;
    }

    private getCustomSystems(): ng.IPromise<void> {
        return this.intgIntegrationsService.getSeededIntegrations(true).then((systems: IProtocolResponse<IPageableOf<IISystem>>) => {
            this.systems = systems.data.content.filter((data) => {
                return !data.name.startsWith("OneTrust-");
            });
            this.isLoading = false;
        });
    }

}
