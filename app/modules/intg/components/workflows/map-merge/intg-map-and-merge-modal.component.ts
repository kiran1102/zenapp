// Angular
import { Component } from "@angular/core";
import { IOtModalContent } from "@onetrust/vitreus/src/app/vitreus/components/modal/modal";
import {
    FormBuilder,
    FormGroup,
    FormControl,
    Validators,
} from "@angular/forms";
import { ToastService } from "@onetrust/vitreus";

// Redux
import { Subject } from "rxjs";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

// Constants
import { MapMergeActions } from "modules/intg/constants/integration.constant";

// Interfaces
import {
    ICustomDataDiscoveryInput,
    ICustomDataDiscoveryOutput,
    IDataDiscoveryItem,
    IDataDiscoveryOption,
    IAssetItem,
    IDataDiscoveryResponseItem,
} from "modules/intg/interfaces/integration.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";
import { IPaginatedResponse, IPageableOf } from "interfaces/pagination.interface";
import { IInventoryRecordV2 } from "interfaces/inventory.interface";
import { IStringMap } from "interfaces/generic.interface";

// Services
import { IntgMapAndMergeService } from "modules/intg/services/intg-map-and-merge.service";
import { InventoryService } from "inventorySharedModule/shared/services/inventory-api.service";

@Component({
    selector: "intg-map-and-merge-modal",
    templateUrl: "./intg-map-and-merge-modal.component.html",
})

export class IntgDataDiscoveryModalComponent implements IOtModalContent {

    otModalData: ICustomDataDiscoveryInput;
    otModalCloseEvent: Subject<ICustomDataDiscoveryOutput>;
    selectedOption: IDataDiscoveryOption;
    selectedApplyAllOption: IDataDiscoveryOption;
    mappedAssets: IStringMap<IAssetItem> = {};
    mappedAssetsArray: any[] = [];
    dataDiscoveryPayload: IDataDiscoveryItem[] = [];
    mapToOptions: IDataDiscoveryOption[] = [];
    responseArray: IDataDiscoveryItem[] = [];
    modalLoading = true;
    dataDiscoveryForm: FormGroup;
    MAP_MERGE_ACTIONS = MapMergeActions;

    applyAllOptions = [
        {
            label: this.translatePipe.transform("Ignore"), value: this.MAP_MERGE_ACTIONS.IGNORE,
        },
        {
            label: this.translatePipe.transform("Reject"), value: this.MAP_MERGE_ACTIONS.REJECT,
        },
    ];

    options = [
        {
            label: this.translatePipe.transform("Ignore"), value: this.MAP_MERGE_ACTIONS.IGNORE,
        },
        {
            label: this.translatePipe.transform("Reject"), value: this.MAP_MERGE_ACTIONS.REJECT,
        },
        {
            label: this.translatePipe.transform("CreateNew"), value: this.MAP_MERGE_ACTIONS.ADDNEW,
        },
    ];

    constructor(
        private intgMapAndMergeService: IntgMapAndMergeService,
        private inventoryService: InventoryService,
        private formBuilder: FormBuilder,
        private toastService: ToastService,
        private readonly translatePipe: TranslatePipe,
    ) {}

    ngOnInit() {
        this.setupInventoryMapping();
        this.dataDiscoveryForm = this.formBuilder.group({});
    }

    setupInventoryMapping() {
        this.inventoryService.getInventoryList(20, { page: 0, size: 100, filter: { name: "" }}).then(
            (response: IProtocolResponse<IPaginatedResponse<IInventoryRecordV2>>) => {
                let firstOption;
                if (response.result) {
                    response.data.data.forEach((item, index) => {
                        const currentOption = { label: item.name.toString(), value: item.id };
                        if (index === 0) {
                            firstOption = currentOption;
                        }
                        this.mapToOptions.push(currentOption);
                    });
                    this.getAssets(firstOption);
                }
            },
        );
    }

    getAssets(defaultMapOption: IDataDiscoveryOption) {
        this.intgMapAndMergeService.getAssets().subscribe(
            (response: IProtocolResponse<IPageableOf<IDataDiscoveryItem>>) => {
                if (response.result) {
                    const correctedResponseObject = [...response.data.content];
                    this.mapAssetResponse(correctedResponseObject, defaultMapOption);
                }
            },
        );
    }

    mapAssetResponse(response: IDataDiscoveryItem[], defaultAddNewOption: IDataDiscoveryOption) {
        let oneTrustIdAdded = false;
        response.forEach((asset) => {
            if (!this.mappedAssets[asset.workflowName]) {
                this.mappedAssets[asset.workflowName] = {};
                this.mappedAssets[asset.workflowName].assetList = [];
                this.mappedAssets[asset.workflowName].dropdown = true;
            }

            if (defaultAddNewOption) {
                asset.onetrustId = defaultAddNewOption.value;
                oneTrustIdAdded = true;
            }
            this.mappedAssets[asset.workflowName].assetName = asset.assetName;
            this.mappedAssets[asset.workflowName].assetList.push([asset, {label: this.translatePipe.transform("Ignore"), value: this.MAP_MERGE_ACTIONS.IGNORE}, defaultAddNewOption]);
        });
        if (oneTrustIdAdded) {
            this.options.push({ label: this.translatePipe.transform("MapTo"), value: this.MAP_MERGE_ACTIONS.MAP });
        }
        const assetProps = Object.keys(this.mappedAssets);
        assetProps.forEach((prop) => {
            this.mappedAssetsArray.push([[prop], this.mappedAssets[prop]]);
        });
        this.modalLoading = false;
    }

    applyAllOptionSelected(optionItem: IDataDiscoveryOption) {
        this.selectedApplyAllOption = optionItem;
    }

    applyAllOptionButton() {
        this.mappedAssetsArray.forEach((system) => {
            const assetItem = this.mappedAssets[system[0]].assetList;
            assetItem.forEach((asset, index) => {
                assetItem[index][0].action = this.selectedApplyAllOption.value;
                assetItem[index][1] = this.selectedApplyAllOption;
            });
        });
    }

    generatePayloadAndSave() {
        this.mappedAssetsArray.forEach((system) => {
            this.mappedAssets[system[0]].assetList.forEach((asset, index) => {
                const currentStatus = (this.mappedAssets[system[0]].assetList[index][0].action ? this.mappedAssets[system[0]].assetList[index][0].action : this.MAP_MERGE_ACTIONS.IGNORE);
                const currentAsset = this.mappedAssets[system[0]].assetList[index][0];
                if (currentStatus === this.MAP_MERGE_ACTIONS.ADDNEW || currentStatus === this.MAP_MERGE_ACTIONS.MAP || currentStatus === this.MAP_MERGE_ACTIONS.REJECT) {
                    if (currentStatus === this.MAP_MERGE_ACTIONS.REJECT && currentAsset.onetrustId) {
                        delete currentAsset.onetrustId;
                    }
                    this.dataDiscoveryPayload.push(currentAsset);
                }
            });
        });
        this.intgMapAndMergeService.saveAssets(this.dataDiscoveryPayload).subscribe(
            (response: IProtocolResponse<IDataDiscoveryResponseItem[]>) => {
                if (response.result) {
                    let duplicateAssetNamesFound = false;
                    const title = this.translatePipe.transform("ErrorCreatingAssetsHeader");
                    const message = `${this.translatePipe.transform("ErrorCreatingAssetsBody")}: `;
                    response.data.forEach((asset) => {
                        if (!asset.success) {
                            duplicateAssetNamesFound = true;
                        }
                    });
                    if (duplicateAssetNamesFound) {
                        this.toastService.updateConfig({
                            positionClass: "bottom-right",
                            maxOpened: 1,
                            closeOldestOnMax: true,
                            closeOnNavigation: true,
                            timeout: 5000,
                            closeLabel: "close",
                        });
                        this.toastService.show(title, message, "error");
                    }
                    this.closeModal();
                }
            },
        );
    }

    saveAssetNameInputModel(event: any, item: string, index: number) {
        this.mappedAssets[item].assetList[index][0].overRideName = event.target.value;
    }

    trackByAssetIndex(index: number): number {
        return index;
    }

    toggleExpand(item: string) {
        this.mappedAssets[item].dropdown = !this.mappedAssets[item].dropdown;
    }

    innerAssetOptionSelected($event: Event, item: string, index: number, formName: string) {
        this.mappedAssets[item[0]].assetList[index][1] = $event;
        const currentOptionValue = this.mappedAssets[item[0]].assetList[index][1].value;
        switch (currentOptionValue) {
            case(MapMergeActions.ADDNEW):
                this.mappedAssets[item[0]].assetList[index][0].action = MapMergeActions.ADDNEW;
                this.updateValidators(formName);
                break;
            case(MapMergeActions.MAP):
                this.mappedAssets[item[0]].assetList[index][0].action = MapMergeActions.MAP;
                this.clearValidators(formName);
                break;
            case(MapMergeActions.REJECT):
                this.mappedAssets[item[0]].assetList[index][0].action = MapMergeActions.REJECT;
                this.clearValidators(formName);
                break;
            default:
                this.mappedAssets[item[0]].assetList[index][0].action = MapMergeActions.IGNORE;
                this.clearValidators(formName);
                break;
        }

    }

    selectMapOption($event: IDataDiscoveryOption, assetName: string, index: number) {
        this.mappedAssets[assetName[0]].assetList[index][0].onetrustId = $event.value;
        this.mappedAssets[assetName[0]].assetList[index][2] = $event;
    }

    updateValidators(formName: string) {
        this.dataDiscoveryForm.addControl(formName, new FormControl("", [Validators.required, Validators.minLength(1)]));
    }

    clearValidators(formName: string) {
        this.dataDiscoveryForm.removeControl(formName);
    }

    closeModal() {
        this.otModalCloseEvent.next();
    }
}
