// Angular
import {
    Component,
} from "@angular/core";
import {
    FormBuilder,
    FormGroup,
    Validators,
} from "@angular/forms";
import { StateService } from "@uirouter/core";

// 3rd party.
import { Subject } from "rxjs";

// Interfaces
import { IOtModalContent } from "@onetrust/vitreus";
import { IKeyValue } from "interfaces/generic.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";
import { IPageableOf } from "interfaces/pagination.interface";
import {
    IISystem,
    IICreateCustomTemplate,
    IITemplate,
    IIWorkflowDetail,
    IICreateWorkflow,
    IIUpdateCustomTemplate,
    IIIntegration,
} from "modules/intg/interfaces/integration.interface";

// Constants & pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";
import { IntgImportTypeKeys } from "modules/intg/constants/integration.constant";
import { NO_BLANK_SPACE } from "constants/regex.constant";

// Services
import { IntgIntegrationsService } from "intsystemservice/intg-integrations.service";
import { IntgTemplateService } from "intsystemservice/intg-template.service";
import { IntgApiService } from "intsystemservice/intg-api.service";
import { IntgWorkflowService } from "intsystemservice/intg-workflow.service";

@Component({
    selector: "intg-import-modal",
    templateUrl: "./intg-import-modal.component.html",
})

export class IntgImportModal implements IOtModalContent<void, boolean> {
    otModalCloseEvent: Subject<boolean>;
    otModalData: any;
    submitButtonText = "Add";
    importForm: FormGroup;
    systems: IISystem[];
    selectedImportType: string = IntgImportTypeKeys.CreateSystem;
    importType = IntgImportTypeKeys;
    allowedExtensions = ["json"];
    currentIconUrl: string;
    isImageConverting = false;
    isJsonLoading = false;
    swaggerBody: IICreateWorkflow;
    isInvalidJsonFile = false;
    maxIconFileUploadSizeInBytes = 3145728; // 3 MB limit
    maxSwaggerFileUploadSizeInBytes = 67108864; // 64 MB limit
    isCurrentFileSizeAccepted: boolean;
    iconFilesFromInput: File[] = [];
    iconFilesFromTooBig: File[] = [];
    iconInvalidFilesFromDrag: File[] = [];
    swaggerFilesFromInput: File[] = [];
    swaggerInvalidFilesFromDrag: File[] = [];
    swaggerFilesFromTooBig: File[] = [];
    isCurrentSwaggerFileSizeAccepted: boolean;
    isSubmitting = false;
    title: string;
    isFileUplod: boolean;
    importTypes: Array<IKeyValue<string>>;
    importTypesForWorkflow = [
        {
            key: this.translatePipe.transform("WorkflowCreateLabel"),
            value: IntgImportTypeKeys.WorkflowCreate,
        },
    ];

    importTypesForSystem = [
        {
            key: this.translatePipe.transform("CreateSystem"),
            value: IntgImportTypeKeys.CreateSystem,
        },
        {
            key: this.translatePipe.transform("CreateSystemFromJSON"),
            value: IntgImportTypeKeys.CreateSystemFromJSON,
        },
        {
            key: this.translatePipe.transform("UpdateSystemFromJSON"),
            value: IntgImportTypeKeys.UpdateSystemFromJSON,
        },
    ];

    constructor(
        private translatePipe: TranslatePipe,
        private formBuilder: FormBuilder,
        private intgIntegrationsService: IntgIntegrationsService,
        private intgTemplateService: IntgTemplateService,
        private intgApiService: IntgApiService,
        private intgWorkflowService: IntgWorkflowService,
        private stateService: StateService,
        ) { }

    ngOnInit() {
        if (this.otModalData.addFromSystem) {
            this.importTypes = this.importTypesForSystem;
            this.title = this.translatePipe.transform("CreateorUpdateSystem");
        } else {
            this.importTypes = this.importTypesForWorkflow;
            this.title = this.translatePipe.transform("Import");
        }

        this.importForm = this.initializeForm();
        this.onImportTypeChange(this.importTypes[0].value);
        this.importForm.get("importType").valueChanges.subscribe((selectedImportType: IKeyValue<string>) => {
            this.onImportTypeChange(selectedImportType.value);
        });
        this.getCustomSystems();
    }

    closeModal(reloadPage?: boolean) {
        this.otModalCloseEvent.next(reloadPage);
    }

    import() {
        switch (this.selectedImportType) {
            case IntgImportTypeKeys.CreateSystem:
                this.addCustomSystem();
                break;
            case IntgImportTypeKeys.CreateSystemFromJSON:
                this.createTemplateAndImportSystem();
                break;
            case IntgImportTypeKeys.UpdateSystemFromJSON:
                this.importSystemUpdate();
                break;
            case IntgImportTypeKeys.WorkflowCreate:
                this.importWorkflow();
                break;
        }
    }

    handleFile(files: File[], isIcon: boolean) {
        if (Array.isArray(files)) {
            this.clearFiles(isIcon);
            if (isIcon) {
                this.generateBase64UrlFromUpload(files[0]);
            } else {
                this.generateSwaggerJsonFromUpload(files[0]);
            }
        }
    }

    clearFiles(isIcon: boolean = false) {
        if (isIcon) {
            this.iconInvalidFilesFromDrag = [];
            this.iconFilesFromInput = [];
            this.iconFilesFromTooBig = [];
        } else {
            this.swaggerInvalidFilesFromDrag = [];
            this.swaggerFilesFromInput = [];
            this.isInvalidJsonFile = false;
        }
    }

    canDisableImport() {
        return this.isImageConverting || this.isJsonLoading || (!(this.selectedImportType === IntgImportTypeKeys.CreateSystem) && !this.swaggerFilesFromInput.length)
                || this.isInvalidJsonFile || this.isSubmitting || this.iconFilesFromTooBig.length
                || this.swaggerFilesFromTooBig.length;
    }

    addCustomSystem() {
        this.isSubmitting = true;
        const customConnector: IICreateCustomTemplate = {
            name: this.importForm.get("name").value,
            description: this.importForm.get("description").value,
            icon: this.currentIconUrl,
        };
        this.intgIntegrationsService.createIntegration(customConnector, true)
            .then((integration: IProtocolResponse<IIIntegration>) => {
                if (integration.result) {
                    this.closeModal(true);
                }
                this.isSubmitting = false;
            });
    }

    private initializeForm(): FormGroup {
        return this.formBuilder.group({
            importType: [this.importTypes[0], Validators.required],
            system: [""],
            systemName: ["", [Validators.required, Validators.pattern(NO_BLANK_SPACE)]],
            workflowName: ["", [Validators.required, Validators.pattern(NO_BLANK_SPACE)]],
            name: ["", [Validators.required, Validators.pattern(NO_BLANK_SPACE)]],
            description: ["", [Validators.required]],
        });
    }

    private onImportTypeChange(importType: string) {
        this.selectedImportType = importType;
        const workflowNameControl =  this.importForm.get("workflowName");
        this.isFileUplod = (this.selectedImportType === this.importType.CreateSystemFromJSON) || (this.selectedImportType === this.importType.WorkflowCreate) || (this.selectedImportType === this.importType.UpdateSystemFromJSON);
        const systemControl =  this.importForm.get("system");
        const systemNameControl =  this.importForm.get("systemName");
        const connectorNameControl =  this.importForm.get("name");
        const systemDescriptionControl = this.importForm.get("description");
        switch (importType) {
            case IntgImportTypeKeys.CreateSystem:
                this.submitButtonText = "Add";
                workflowNameControl.disable();
                systemControl.disable();
                systemNameControl.disable();
                connectorNameControl.enable();
                systemDescriptionControl.enable();
                break;
            case IntgImportTypeKeys.CreateSystemFromJSON:
                this.submitButtonText = "Add";
                workflowNameControl.disable();
                systemControl.disable();
                systemNameControl.enable();
                connectorNameControl.disable();
                systemDescriptionControl.disable();
                break;
            case IntgImportTypeKeys.UpdateSystemFromJSON:
                this.submitButtonText = "Submit";
                workflowNameControl.disable();
                systemControl.enable();
                connectorNameControl.disable();
                systemNameControl.disable();
                systemDescriptionControl.disable();
                break;
            case IntgImportTypeKeys.WorkflowCreate:
                workflowNameControl.enable();
                systemControl.disable();
                systemNameControl.disable();
                connectorNameControl.disable();
                systemDescriptionControl.disable();
                break;
        }
    }

    private getCustomSystems(): ng.IPromise<void> {
        return this.intgIntegrationsService.getSeededIntegrations(true).then((systems: IProtocolResponse<IPageableOf<IISystem>>) => {
            this.systems = systems.data.content.filter((data) => {
                return !data.name.startsWith("OneTrust-");
            });
            this.importForm.patchValue({system: this.systems[0]});
        });
    }

    private isValidFileType(imgFile: File, isIcon = true): boolean {
        const currentFileExtension = imgFile.name.split(".").pop();
        const exceptedFileExtensions = isIcon ? ["png", "jpeg", "jpg"] : ["json"];
        return exceptedFileExtensions.indexOf(currentFileExtension.toLowerCase()) > -1;
    }

    private generateBase64UrlFromUpload(imgFile: File) {
        this.isCurrentFileSizeAccepted = true;
        if (imgFile) {
            this.isImageConverting = true;
            if (this.isValidFileType(imgFile)) {
                const reader: FileReader = new FileReader();
                reader.onloadend = () => {
                    if (imgFile.size > this.maxIconFileUploadSizeInBytes) {
                        this.isCurrentFileSizeAccepted = false;
                        this.iconFilesFromTooBig.push(imgFile);
                    } else {
                        this.iconFilesFromInput.push(imgFile);
                        this.currentIconUrl = reader.result as string;
                    }
                    this.isImageConverting = false;
                };
                reader.readAsDataURL(imgFile);
            } else {
                this.iconInvalidFilesFromDrag.push(imgFile);
                this.isImageConverting = false;
            }
        }
    }

    private generateSwaggerJsonFromUpload(jsonFile: File) {
        if (jsonFile) {
            this.isJsonLoading = true;
            if (this.isValidFileType(jsonFile, false)) {
                const reader: FileReader = new FileReader();
                reader.onload = () => {
                    if (jsonFile.size > this.maxSwaggerFileUploadSizeInBytes) {
                        this.isCurrentSwaggerFileSizeAccepted = false;
                        this.swaggerFilesFromTooBig.push(jsonFile);
                    } else {
                        this.swaggerFilesFromInput.push(jsonFile);
                        try {
                            this.swaggerBody = JSON.parse(reader.result as string);
                            this.isInvalidJsonFile = false;
                        } catch (e) {
                            this.isInvalidJsonFile = true;
                        }
                        this.isJsonLoading = false;
                    }
                };
                reader.readAsText(jsonFile);
            } else {
                this.swaggerInvalidFilesFromDrag.push(jsonFile);
                this.isJsonLoading = false;
            }
        }
    }

    private createTemplateAndImportSystem() {
        this.isSubmitting = true;
        const customTemplate: IICreateCustomTemplate = {
            name: this.importForm.value.systemName,
            description: this.importForm.value.systemName,
            icon: this.currentIconUrl,
        };
        this.intgTemplateService.createTemplate(customTemplate, true).then((response: IProtocolResponse<IITemplate>) => {
            if (response.result) {
                this.intgApiService.importSystemCreate(this.swaggerBody, this.importForm.value.systemName, response.data.id).then(() => {
                    this.isSubmitting  = false;
                    this.closeModal(true);
                });
            } else {
                this.isSubmitting  = false;
            }
        });
    }

    private importSystemUpdate() {
        this.isSubmitting = true;
        const selectedSystem: IISystem = this.importForm.value.system as IISystem;

        this.intgTemplateService.getTemplateById(selectedSystem.templateId).then((response: IProtocolResponse<IITemplate>) => {
            if (response.result) {
                const template =  response.data;
                const customTemplate: IIUpdateCustomTemplate = {
                    name: template.name,
                    description: template.description,
                    id: selectedSystem.templateId,
                    icon: this.currentIconUrl || template.icon,
                    isSeed: true,
                };
                Promise.all([
                    this.intgTemplateService.updateTemplate(customTemplate),
                    this.intgApiService.importSystemUpdate(this.swaggerBody, selectedSystem.id, selectedSystem.templateId),
                ]).then((responses: [IProtocolResponse<IITemplate>, IProtocolResponse<void>]) => {
                    this.isSubmitting = false;
                    if (responses[0].result && responses[1].result) {
                        this.closeModal(true);
                    }
                });
            } else {
                this.isSubmitting  = false;
            }
        });
    }

    private importWorkflow() {
        this.isSubmitting = true;
        const name = this.importForm.value.workflowName;
        this.intgWorkflowService.importWorkflow(this.swaggerBody, name).subscribe((response: IProtocolResponse<IIWorkflowDetail>) => {
            this.isSubmitting = false;
            if (response.result) {
                this.stateService.go("zen.app.pia.module.intg.workflows.builder", { id: response.data.id });
                this.closeModal(false);
            }
        });
    }
}
