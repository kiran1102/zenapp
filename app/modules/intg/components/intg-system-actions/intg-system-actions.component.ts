// Angular
import {
    Component,
    OnInit,
    OnDestroy,
} from "@angular/core";
import { StateService } from "@uirouter/core";
import { IPromise } from "angular";

// Services
import { Permissions } from "modules/shared/services/helper/permissions.service";
import { ModalService } from "sharedServices/modal.service";
import { IntgIntegrationsService } from "modules/intg/services/intg-integrations.service";
import { IntgWorkflowService } from "intsystemservice/intg-workflow.service";
import { NotificationService } from "sharedModules/services/provider/notification.service";
import {
    OtModalService,
    ModalSize,
} from "@onetrust/vitreus";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

// Interface
import { IPageableOf } from "interfaces/pagination.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";
import { ITableColumnConfig } from "interfaces/table-config.interface";
import { IDropdownOption } from "interfaces/one-dropdown.interface";
import { ITableViewActions } from "modules/intg/interfaces/intg-table.interface";
import { IIWorkflow } from "modules/intg/interfaces/integration.interface";
import { IDeleteConfirmationModalResolve } from "interfaces/delete-confirmation-modal-resolve.interface";

// Enums and constants
import { TableColumnTypes } from "enums/data-table.enum";
import { SourceType } from "modules/intg/constants/integration.constant";

// component
import { IntgSystemActionsAddModal } from "intsystemcomponent/intg-system-actions-add-modal/intg-system-actions-add-modal.component";

// RxJs
import { Subject } from "rxjs";
import {
    takeUntil,
    filter,
 } from "rxjs/operators";

@Component({
    selector: "intg-system-actions",
    templateUrl: "./intg-system-actions.component.html",
    host: { class: "stretch-vertical column-nowrap" },
})
export class IntgSystemActionsComponent implements OnInit, OnDestroy {
    public options: IDropdownOption[];
    ready = false;
    page: IPageableOf<IIWorkflow> = null;
    readonly columns: ITableColumnConfig[] = [
        {
            name: this.translatePipe.transform("ActionName"),
            sortKey: 1,
            type: TableColumnTypes.Text,
            cellValueKey: "name",
        },
        {
            name: this.translatePipe.transform("ActionDescription"),
            sortKey: 2,
            type: TableColumnTypes.Text,
            cellValueKey: "description",
        },
        {
            name: "",
            type: TableColumnTypes.Action,
            cellValueKey: "",
        },
    ];
    readonly viewPermissions = {
        view: this.permissions.canShow("IntegrationSystemActions"),
        add: this.permissions.canShow("IntegrationSystemActionsAdd"),
        edit: this.permissions.canShow("IntegrationsSystemActionsEdit"),
        delete: this.permissions.canShow("IntegrationsSystemActionsDelete"),
        addFromTable: false,
    };
    readonly viewActions: ITableViewActions = {
        add: {
            show: this.viewPermissions.add,
            text: "AddAction",
            action: () => this.addAction(),
            autoId: "IntegrationActionsAddBtn",
        },
        addFromTable: {
            show: this.viewPermissions.add && this.viewPermissions.addFromTable,
            text: "AddAction",
            action: () => this.addAction(),
            autoId: "IntegrationActionsAddFromTableBtn",
        },
    };

    private readonly systemId = this.stateService.params.system;
    private workflows: IIWorkflow[];
    private destroy$ = new Subject();

    constructor(
        private stateService: StateService,
        private notificationService: NotificationService,
        private permissions: Permissions,
        private modalService: ModalService,
        private otModalService: OtModalService,
        private translatePipe: TranslatePipe,
        private intgIntegrationsService: IntgIntegrationsService,
        private workflowService: IntgWorkflowService,
    ) {}

    ngOnInit() {
        this.getActionsPage();
    }

    getActionsPage(page = 0, size = 20) {
        this.ready = false;
        return this.intgIntegrationsService.getWorkflows(this.systemId).then(
            (workflowsResponse: IProtocolResponse<IIWorkflow[]>) => {
                this.workflows = workflowsResponse.data || [];
                const content = this.workflows.sort((a, b) => a.name.localeCompare(b.name));
                this.page = {
                    first: true,
                    last: true,
                    number: page,
                    numberOfElements: content.length,
                    size,
                    totalElements: content.length,
                    totalPages: page + 1,
                    content,
                };
                this.ready = true;
            },
        );
    }

    getActions = (row) => {
            const actions: IDropdownOption[] = [];
            if (this.viewPermissions.edit) {
                actions.push({
                    text: this.translatePipe.transform(row.sourceType === SourceType.SEED ? "View" : "Edit"),
                    action: (): void => {
                        this.editAction(row);
                    },
                });
            }
            if (this.viewPermissions.delete && row.sourceType !== SourceType.SEED) {
                actions.push({
                    text: this.translatePipe.transform("Delete"),
                    action: (): void => {
                        this.deleteAction(row);
                    },
                });
            }
            if (!actions.length) {
                actions.push({
                    text: this.translatePipe.transform("NoActionsAvailable"),
                    action: (): void => {}, // Keep Empty
                });
            }
            return actions;
        }

    addAction() {
        this.otModalService.create(
            IntgSystemActionsAddModal,
            {
                isComponent: true,
            },
            {integrationId: this.systemId},
        ).pipe(
            takeUntil(this.destroy$),
            filter(Boolean),
        ).subscribe(() => this.getActionsPage());
    }

    editAction(systemAction: IIWorkflow) {
        const modalData = {
            integrationId: this.systemId,
            workflow: systemAction,
        };
        this.otModalService.create(
            IntgSystemActionsAddModal,
            {
                isComponent: true,
                size: ModalSize.SMALL,
            },
            modalData,
        ).pipe(
            takeUntil(this.destroy$),
            filter(Boolean),
        ).subscribe(() => this.getActionsPage());
    }

    deleteAction(systemAction: IIWorkflow) {
        const deleteModalData: IDeleteConfirmationModalResolve = {
            promiseToResolve: (): IPromise<void> => {
                return this.workflowService.deleteWorkflow(systemAction.id)
                    .then((response: IProtocolResponse<void>) => {
                        if (response.result) {
                            this.getActionsPage();
                            this.modalService.closeModal();
                            this.modalService.clearModalData();
                        }
                    });
            },
            modalTitle: this.translatePipe.transform("DeleteAction"),
            confirmationText: this.translatePipe.transform("AreYouSureDeleteAction"),
            submitButtonText: this.translatePipe.transform("Delete"),
            cancelButtonText: this.translatePipe.transform("Cancel"),
        };
        this.modalService.setModalData(deleteModalData);
        this.modalService.openModal("otDeleteConfirmationModal");
    }

    ngOnDestroy() {
        this.destroy$.next();
    }

    getContextMenuActions(row: IIWorkflow) {
        this.options = this.getActions(row);
    }

    private saveWorkflow(row: IIWorkflow) {
        delete row.index;
        this.ready = false;
        this.workflowService.saveWorkflow(row).then((response: IProtocolResponse<IIWorkflow>) => {
            if (response.result) {
                this.ready = true;
                const successMsg = response.data.enabled ? this.translatePipe.transform("ActionActivated") : this.translatePipe.transform("ActionDeactivated");
                this.notificationService.alertSuccess(this.translatePipe.transform("Success"), successMsg);
            }
        });
    }
}
