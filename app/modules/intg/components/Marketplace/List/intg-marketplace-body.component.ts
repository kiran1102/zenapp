import {
    Component,
    Input,
} from "@angular/core";

import { IISystem } from "modules/intg/interfaces/integration.interface";

@Component({
    selector: "intg-marketplace-body",
    templateUrl: "./intg-marketplace-body.component.html",
})

export class IntgMarketplaceBodyComponent {
    @Input() systems: IISystem;
    @Input() componentToNavigate: string;

    trackById(index, item): number {
        return item.Id;
    }
}
