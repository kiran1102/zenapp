import { Component, Input, Output, EventEmitter, Inject } from "@angular/core";
import { IIModuleFilters } from "modules/intg/interfaces/integration.interface";

// pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

@Component({
    selector: "intg-marketplace-modules-picklist",
    templateUrl: "./intg-marketplace-modules-picklist.component.html",
    styles: ["[otFormElement] { width: 25rem }"],
})

export class IntgMarketplaceModulesPicklistComponent {
    @Input() public autoCloseEnabled = true;
    @Input() public filterOptions;
    @Output() public filterByModule: EventEmitter<IIModuleFilters> = new EventEmitter<IIModuleFilters>();

    selectedOption: IIModuleFilters;
    placeholder: string = this.translatePipe.transform("All");

    constructor(
        private translatePipe: TranslatePipe,
    ) {}

    selectOption(value: IIModuleFilters): void {
        this.selectedOption = value;
        this.filterByModule.emit(this.selectedOption);
    }
}
