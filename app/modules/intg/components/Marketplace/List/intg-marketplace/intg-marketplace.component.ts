import {
    Component,
    OnInit,
    Input,
    OnDestroy,
} from "@angular/core";
import { StateService } from "@uirouter/core";

// 3rd Party
import { orderBy } from "lodash";

// Services
import { Permissions } from "modules/shared/services/helper/permissions.service";
import { ModalService } from "sharedServices/modal.service";
import { IntgConfigService } from "intsystemservice/configuration/intg-configuration.service";
import { IntgIntegrationsService } from "modules/intg/services/intg-integrations.service";
import { IntgTemplateService } from "intsystemservice/intg-template.service";
import { OtModalService } from "@onetrust/vitreus";

// Interfaces
import {
    IISystem,
    IIModuleFilters,
    IIntgCustomSystemModal,
} from "modules/intg/interfaces/integration.interface";
import { ISupportUpgrade } from "interfaces/support.interface";
import { IPermissions } from "modules/shared/interfaces/permissions.interface";

// Constants
import {
    SystemListFilters,
    ModuleFilters,
    MarketPlaceView,
} from "modules/intg/constants/integration.constant";

// Rxjs
import {
    combineLatest,
    Subscription,
} from "rxjs";
import { filter } from "rxjs/operators";

// pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

// component
import { IntgImportModal } from "intsystemcomponent/workflows/import/intg-import-modal.component";

@Component({
    selector: "intg-marketplace",
    templateUrl: "./intg-marketplace.component.html",
})

export class IntgMarketplaceComponent implements OnInit, OnDestroy {

    @Input() listToShow: string;

    viewPermissions: IPermissions = {
        getConnectors: this.permissions.canShow("IntegrationConnector"),
        addConnector: this.permissions.canShow("IntegrationConnectorAdd"),
    };
    ready = false;
    filters: IIModuleFilters[];
    systems: IISystem[];
    title: string;
    customConnectors: IISystem[];
    filteredSystem: IISystem[];
    filteredCustomSystem: IISystem[];
    marketPlaceView = MarketPlaceView;
    private marketPlaceSubscription: Subscription;
    private systemSubscription: Subscription;
    private selectedSystem: IIModuleFilters;

    constructor(
        private stateService: StateService,
        private IntgConfig: IntgConfigService,
        private modalService: ModalService,
        private permissions: Permissions,
        private intgIntegrationsService: IntgIntegrationsService,
        private intgTemplateService: IntgTemplateService,
        private otModalService: OtModalService,
        private translatePipe: TranslatePipe,
    ) {}

    ngOnInit() {
        if (this.listToShow === MarketPlaceView.MARKET_PLACE_LIST_VIEW) {
            this.title = "IntegrationMarketplace";
            this.setFilter();
            this.generateAllSystems();
        } else if ( this.listToShow === MarketPlaceView.SYSTEM_LIST_VIEW) {
            this.title = "IntegrationSystems";
            this.getSeededSystems();
        }
    }

    ngOnDestroy() {
        if (this.listToShow === MarketPlaceView.MARKET_PLACE_LIST_VIEW) {
            this.marketPlaceSubscription.unsubscribe();
        } else if (this.listToShow === MarketPlaceView.SYSTEM_LIST_VIEW) {
            this.systemSubscription.unsubscribe();
        }
    }

    select(selected: IIModuleFilters = null): void {
        this.selectedSystem = selected;
        this.systems = this.getSystems();
    }

    goToRoute() {
        if (this.listToShow === MarketPlaceView.MARKET_PLACE_LIST_VIEW) {
            this.stateService.go("zen.app.pia.module.intg.connections.manage");
        } else if (this.listToShow === MarketPlaceView.SYSTEM_LIST_VIEW) {
            this.otModalService
            .create(
                IntgImportModal,
                { isComponent: true },
                { addFromSystem: true },
            ).pipe(
                filter((data) => Boolean(data)),
            ).subscribe(() => {
                this.ready = false;
                this.getSeededSystems();
            });
        }
    }

    private setFilter(): void {
        this.filters = orderBy(
            this.IntgConfig.getFiltersList(),
            ["name"],
            ["asc"],
        );
    }

    private generateAllSystems(): void {
        this.systems = this.getSystems();
        if (this.viewPermissions.getConnectors) {
            this.getCustomSystems();
        } else {
            this.ready = true;
        }
    }

    private getSystems(): IISystem[] {
        return !this.selectedSystem ||
            this.selectedSystem.id === ModuleFilters.All
                ? this.filterSystems(SystemListFilters.MARKETPLACE)
                : this.filterSystems(this.selectedSystem.id);
    }

    private filterSystems(id: string) {
        this.filteredCustomSystem = this.customConnectors;
        if (id === SystemListFilters.MARKETPLACE) {
            this.filteredSystem = this.IntgConfig.getSystemsList(id);
        } else {
            this.filteredSystem = this.IntgConfig.showFilteredTiles(id);
            this.filteredCustomSystem = this.customConnectors.filter((system: IISystem) => {
                const integrateswithArr = system.integratesWith ? system.integratesWith.split(",") : "";
                return integrateswithArr.includes(id);
            });
        }
        return this.filteredCustomSystem && this.filteredCustomSystem.length ?  [...this.filteredSystem, ...this.filteredCustomSystem] :  [...this.filteredSystem];
    }

    private getCustomSystems() {
        this.ready = false;
        const templates$ = this.intgTemplateService.getTemplates(0, 1000);
        const integrations$ = this.intgIntegrationsService.getIntegrations();
        this.marketPlaceSubscription = combineLatest(templates$, integrations$).subscribe(([templates, integrations]) => {
            this.customConnectors = this.intgIntegrationsService.mapIntegrationTemplatesList(integrations.data.content, templates.data.content);
            this.customConnectors = this.customConnectors.filter((data) => {
                return !data.name.startsWith("OneTrust-");
            });
            this.systems.push(...this.customConnectors);
            this.ready = true;
        });
    }

    private openContactModal(): void {
        const moduleName: ISupportUpgrade = {
            module: "Marketplace",
        };
        this.modalService.setModalData(moduleName);
        this.modalService.openModal("contactOnetrustModal");
    }

    private openAddCustomModal(): void {
        const moduleData: IIntgCustomSystemModal = {
            module: "IntgCustomSystemAddModal",
            callback: (result: boolean) => {
                if (result) {
                    this.generateAllSystems();
                }
            },
        };
        this.modalService.setModalData(moduleData);
        this.modalService.openModal("IntgCustomSystemAddModal");
    }

    private getSeededSystems() {
        const templates$ = this.intgTemplateService.getTemplates(0, 1000);
        const integrations$ = this.intgIntegrationsService.getSeededIntegrationsAsObservable(true);
        this.systemSubscription = combineLatest(templates$, integrations$).subscribe(([templates, integrations]) => {
            const systems = this.intgIntegrationsService.mapIntegrationTemplatesList(integrations.data.content, templates.data.content) || [];
            this.systems = systems.filter((data) => {
                return !data.name.startsWith("OneTrust-");
            });
            this.ready = true;
        });
    }

 }
