import {
    Component,
    Inject,
    Input,
} from "@angular/core";
import { StateService } from "@uirouter/core";

import { IPromise } from "angular";

// Services
import { IntgApiService } from "intsystemservice/intg-api.service";
import { IntgConfigService } from "intsystemservice/configuration/intg-configuration.service";
import { ModalService } from "sharedServices/modal.service";
import { Permissions } from "modules/shared/services/helper/permissions.service";
import { IntgTemplateService } from "modules/intg/services/intg-template.service";
import { IntgIntegrationsService } from "modules/intg/services/intg-integrations.service";

// Interfaces
import {
    IISystem,
    IIConnection,
    IIIntegration,
    IITemplate,
    IIPage,
} from "modules/intg/interfaces/integration.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";
import { ITabsNav } from "interfaces/one-tabs-nav.interface";
import { IStringMap } from "interfaces/generic.interface";
import { ISupportUpgrade } from "interfaces/support.interface";
import { IDeleteConfirmationModalResolve } from "interfaces/delete-confirmation-modal-resolve.interface";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

// Constants
import {
    ModuleFilters,
    SystemConstants,
    ConnectionManageTabs,
    MarketPlaceDetailsView,
    TargetDiscoverySystem,
    JsonFilename,
    SourceType,
} from "modules/intg/constants/integration.constant";

@Component({
    selector: "intg-marketplace-details",
    templateUrl: "./intg-marketplace-details.component.html",
})
export class IntgMarketplaceDetailsComponent {
    @Input() pageToShow: string;

    intgName: string;
    ready = false;
    systemId: string = this.stateService.params.system;
    isCustomSystem = false;
    isLoading: boolean;
    system: IISystem;
    connections: IIConnection[];
    ModuleFilters = ModuleFilters;
    SystemConstants = SystemConstants;
    systemIsDisabled = false;
    tabs: ITabsNav[] = [];
    TABS = ConnectionManageTabs;
    title: string;
    headerName: string;
    isMarketPlaceView: boolean;
    link: string;
    selectedTab: string = ConnectionManageTabs.OVERVIEW;
    isEnableContact = this.systemId === this.SystemConstants.PARDOT || this.systemId === this.SystemConstants.MARKETO;
    canShowButton = false;
    disableDeleteButton = false;
    allowedTabs: IStringMap<ITabsNav> = {
        OVERVIEW: {
            id: this.TABS.OVERVIEW,
            text: this.translatePipe.transform("Overview"),
            action: (option: ITabsNav): void => {
                this.selectedTab = option.id;
            },
        },
        API_CONNECTION: {
            id: this.TABS.API_CONNECTION,
            text: this.translatePipe.transform("API_Connections"),
            action: (option: ITabsNav): void => {
                this.selectedTab = option.id;
            },
        },
        ACTIONS: {
            id: this.TABS.ACTIONS,
            text: this.translatePipe.transform("Actions"),
            action: (option: ITabsNav): void => {
                this.selectedTab = option.id;
            },
        },
    };

    targetDiscoverySystem = [
        TargetDiscoverySystem.MAIL_CHIMP,
        TargetDiscoverySystem.HUBSPOT,
        TargetDiscoverySystem.OKTA,
        TargetDiscoverySystem.SERVICENOW,
        TargetDiscoverySystem.OFFICE365,
        TargetDiscoverySystem.MARKETO,
        TargetDiscoverySystem.SALESFORCE,
    ];
    isTargetDiscoverySystem = false;
    seededSystem: IISystem;
    systemName: string;

    constructor(
        @Inject("$q") private $q: ng.IQService,
        @Inject("Export") private Export: any,
        private stateService: StateService,
        @Inject(Permissions) public permissions: Permissions,
        private readonly intgApiService: IntgApiService,
        private readonly IntgConfig: IntgConfigService,
        private readonly translatePipe: TranslatePipe,
        private readonly intgIntegrationsService: IntgIntegrationsService,
        private readonly intgTemplateService: IntgTemplateService,
        private readonly modalService: ModalService,
    ) {}

    ngOnInit(): void {
        this.isMarketPlaceView = this.pageToShow === MarketPlaceDetailsView.MARKET_PLACE_DETAILS_VIEW;
        this.title = this.isMarketPlaceView ? "Marketplace" : "Systems";
        this.headerName = this.isMarketPlaceView ? "IntegrationDetails" : "SystemDetails";
        this.link = this.isMarketPlaceView ? "zen.app.pia.module.intg.marketplace.list" : "zen.app.pia.module.intg.systems.list";
        this.getSystem(this.systemId);
    }

    trackByIndex(index, item): number {
        return index;
    }

    deleteCustomConnector(connector: IISystem): void {
        const deleteModalData: IDeleteConfirmationModalResolve = {
            promiseToResolve: (): IPromise<void> => {
                return this.intgIntegrationsService.getIntegrationById(connector.id)
                .then((response: IProtocolResponse<IIIntegration>) => {
                    if (response.result) {
                        return this.removeAssociatedTemplate(response.data.templateId, connector.id);
                    }
                });
            },
            modalTitle: this.translatePipe.transform("DeleteConnection"),
            confirmationText: this.translatePipe.transform("AreYouSureDeleteConnection"),
            submitButtonText: this.translatePipe.transform("Delete"),
            cancelButtonText: this.translatePipe.transform("Cancel"),
        };
        this.modalService.setModalData(deleteModalData);
        this.modalService.openModal("otDeleteConfirmationModal");
    }

    getCustomSystem(id) {
        return this.intgIntegrationsService.getSystem(id).then((system: IISystem) => {
            this.system = system;
            this.disableDeleteButton = system.seed && system.sourceType !== SourceType.USER_DEFINED;
            this.systemName = this.targetDiscoverySystem.find((name: string) => name.toLowerCase() === this.system.name.toLowerCase());
            this.isTargetDiscoverySystem = !!this.systemName;
            if (this.system.isCustomConnector && this.isTargetDiscoverySystem) {
                const seededSystemId = this.getSeededSystemIdByName(this.systemName);
                this.seededSystem = this.IntgConfig.getSystemById(seededSystemId, "meta") as IISystem;
            }
        });
    }

    onRouteClick(route: { path: string, params: any }) {
        this.stateService.go(route.path);
    }

    openContactModal(system: IISystem): void {
        const moduleName: ISupportUpgrade = {
            module: system.upgradeModuleName,
        };
        this.modalService.setModalData(moduleName);
        this.modalService.openModal("contactOnetrustModal");
    }

    exportSystem(): void {
        this.isLoading = true;
        this.intgIntegrationsService.exportSystem(this.systemId)
            .subscribe((response: IProtocolResponse<string>) => {
                if (response.result) {
                    this.Export.basicFileDownload(JSON.stringify(response.data, undefined, 2), JsonFilename.OTSYSTEM);
                }
                this.isLoading = false;
            });
    }

    goToRoute(systemId: string): void {
        switch (systemId) {
            case this.SystemConstants.OKTA:
            case this.SystemConstants.PING:
            case this.SystemConstants.ONELOGIN:
            case this.SystemConstants.ADFS:
            case this.SystemConstants.GSUITE:
            case this.SystemConstants.AZURE:
                this.stateService.go("zen.app.pia.module.settings.sso");
                break;
            case this.SystemConstants.SMTP:
                this.stateService.go("zen.app.pia.module.settings.smtp");
                break;
            case this.SystemConstants.SERVICENOW:
            case this.SystemConstants.BMC:
            case this.SystemConstants.IMPORT:
            case this.SystemConstants.RSA:
            case this.SystemConstants.COLLIBRA:
            case this.SystemConstants.SYMANTEC:
            case this.SystemConstants.BIGID:
            case this.SystemConstants.DATAGUISE:
            case this.SystemConstants.VARONIS:
            case this.SystemConstants.IBMSECURITY:
                this.stateService.go("zen.app.pia.module.admin.import.templates");
                break;
            case this.SystemConstants.JIRA:
            case this.SystemConstants.SLACK:
            case this.SystemConstants.ELOQUA:
            case this.SystemConstants.SALESFORCE:
            case this.SystemConstants.MAILCHIMP:
            case this.SystemConstants.GENERIC:
                this.stateService.go("zen.app.pia.module.intg.connections.manage", { id: null, system: systemId } );
                break;
        }
    }

    private removeAssociatedTemplate(templateId: string, integrationId: string): IPromise<void> {
      return this.intgTemplateService.deleteTemplate(templateId, integrationId)
                .then((response: IProtocolResponse<IITemplate>) => {
                    if (response.result) {
                        this.modalService.closeModal();
                        this.modalService.clearModalData();
                        this.stateService.go("zen.app.pia.module.intg.marketplace.list");
                    }
                });
    }

    private getSystem(id: string) {
        this.ready = false;
        this.tabs = [ this.allowedTabs[ConnectionManageTabs.OVERVIEW] ];
        const promises = [];
        this.system = (this.IntgConfig.getSystemById(id, "meta") as IISystem);
        if (this.system) {
            this.seededSystem = this.system;
            const connectionsPromise = this.intgApiService.getConnection("", this.systemId).then((response: IProtocolResponse<IIPage<IIConnection>>): void => {
                this.connections = response.result && response.data.content ? response.data.content : [];
                if (this.connections.length) {
                    this.tabs.push(this.allowedTabs[ConnectionManageTabs.API_CONNECTION]);
                }
                this.systemIsDisabled = this.systemId === this.SystemConstants.SLACK && this.connections.length >= 1;
                this.ready = true;
            });
            promises.push(connectionsPromise);
        } else {
            this.isCustomSystem = true;
            const customSystemPromise = this.getCustomSystem(id).then(() => {
                if (this.permissions.canShow("IntegrationSystemActions")) {
                    this.tabs.push(this.allowedTabs[ConnectionManageTabs.ACTIONS]);
                }
            });
            promises.push(customSystemPromise);
        }
        return this.$q.all(promises).then(() => {
            this.ready = true;
            this.canShowButton =  this.seededSystem && this.seededSystem.isConnectable && !this.isEnableContact && !this.systemIsDisabled;
        });
    }

    private getSeededSystemIdByName(name: string): string {
        let systemId = "";
        switch (name) {
            case TargetDiscoverySystem.MARKETO:
                systemId = SystemConstants.MARKETO;
                break;
            case TargetDiscoverySystem.OKTA:
                systemId = SystemConstants.OKTA;
                break;
            case TargetDiscoverySystem.SERVICENOW:
                systemId = SystemConstants.SERVICENOW;
                break;
            case TargetDiscoverySystem.SALESFORCE:
                systemId = SystemConstants.SALESFORCE;
                break;
            case TargetDiscoverySystem.MAIL_CHIMP:
                systemId = SystemConstants.MAILCHIMP;
                break;
        }
        return systemId;
    }
}
