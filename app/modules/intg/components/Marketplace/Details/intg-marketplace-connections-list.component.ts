import {
    Component,
    Inject,
    Input,
} from "@angular/core";
import { StateService } from "@uirouter/core";

import { IntgConnectionsHelperService } from "intsystemservice/intg-connections-helper.service";
import { IIGenericTableConfig } from "interfaces/table-config.interface";
import { IBasicTableCell } from "interfaces/tables.interface";
import { IIConnection, IIConnectionTableRow } from "modules/intg/interfaces/integration.interface";
import { IStore } from "interfaces/redux.interface";
import { StoreToken } from "tokens/redux-store.token";
import { map, each } from "lodash";
import { getOrgById } from "oneRedux/reducers/org.reducer";

import { TranslatePipe } from "pipes/translate.pipe";

@Component({
    selector: "intg-marketplace-connections-list",
    templateUrl: "./intg-marketplace-connections-list.component.html",
})

export class IntgMarketplaceConnectionsListComponent {

    @Input() connections: IIConnection[] = [];

    public rows: IIConnection[];
    public columnConfig: IIGenericTableConfig[];

    constructor(
        private stateService: StateService,
        @Inject(StoreToken) readonly store: IStore,
        public translatePipe: TranslatePipe,
        private readonly intgConnectionsHelperService: IntgConnectionsHelperService,
    ) {}

    ngOnInit(): void {
        this.columnConfig = this.intgConnectionsHelperService.getMarketplaceConnectionsConfig();
        this.rows = this.toBasicTableRows(this.connections);
    }

    public toBasicTableRows(rows: IIConnection[]): IIConnection[] {
        return map(rows, (row: IIConnection): IBasicTableCell => {
            const cells: IBasicTableCell[] = [];
            each(this.columnConfig, (column: IIGenericTableConfig): void => {
                this.getStatusAndOrgName(column, row);
                cells.push({
                    key: column.modelPropKey,
                    value: row[column.modelPropKey],
                    displayValue: row[column.modelPropKey],
                    route: "zen.app.pia.module.intg.connections.manage",
                    routeParams: {
                        id: row.systemInstanceId,
                        system: row.integrationSystemId,
                    },
                });
            });
            return { ...row, cells };
        });
    }

    public getStatusAndOrgName(column?: IIGenericTableConfig, row?: IIConnectionTableRow): void {
        switch (column.component) {
            case "active-tag-table-cell":
                row.statusText = row[column.modelPropKey] ? this.translatePipe.transform("Active") : this.translatePipe.transform("Inactive");
                row.modifierClass = row[column.modelPropKey] ? "primary" : "destructive";
                break;
            case "org-name-table-cell":
                row.orgName = getOrgById(row[column.modelPropKey])(this.store.getState()).Name;
                break;
        }
    }

    public goToRoute(cell: IBasicTableCell): void {
        this.stateService.go(cell.route, cell.params);
    }

}
