// Angular
import {
    Component,
    Input,
    Output,
    EventEmitter,
} from "@angular/core";

// Interfaces
import {
    IISystem,
    IIConnection,
    ITargetDataDiscoveryObject,
    ISeededWorkflowTemplate,
    IIPage,
} from "modules/intg/interfaces/integration.interface";
import { ITabsNav } from "interfaces/one-tabs-nav.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";
import { IPageableOf } from "interfaces/pagination.interface";

// Constants
import {
    ConnectionManageTabs,
    TargetDiscoveryModuleIcon,
    TargetDiscoveryModule,
} from "modules/intg/constants/integration.constant";

// Services
import { IntgWorkflowService } from "intsystemservice/intg-workflow.service";
import {
    OtModalService,
    ModalSize,
} from "@onetrust/vitreus";
import { Permissions } from "sharedModules/services/helper/permissions.service";
import { IntgApiService } from "intsystemservice/intg-api.service";

// components
import { IntgSeedWorkFlowCreateModal } from "intsystemcomponent/workflows/seedWorkflow/intg-seedWorkflow-create-modal.component";
import { TranslatePipe } from "modules/pipes/translate.pipe";

@Component({
    selector: "intg-marketplace-details-body",
    templateUrl: "./intg-marketplace-details-body.component.html",
})

export class IntgMarketplaceDetailsBodyComponent {
    @Input() system: IISystem;
    @Input() selectedTab: string;
    @Input() tabs: ITabsNav[];
    @Input() connections: IIConnection[];
    @Input() seededSystem: IISystem;
    @Input() isTargetDiscoverySystem: boolean;
    @Input() systemName: string;
    @Input() canShowButton: boolean;

    @Output()  goToRoute: EventEmitter<string> = new EventEmitter<string>();
    @Output()  openContactModal: EventEmitter<IISystem> = new EventEmitter<IISystem>();

    public TABS = ConnectionManageTabs;

    targetedDataDiscoveryList: ITargetDataDiscoveryObject[] = [];
    loadingTemplate = false;
    loadingConnections = false;

    constructor(
        private intgWorkflow: IntgWorkflowService,
        private intgApiService: IntgApiService,
        private otModalService: OtModalService,
        private permissions: Permissions,
        private translatePipe: TranslatePipe,
    ) {
    }

    ngOnInit() {
        if (this.permissions.canShow("IntegrationsDataDiscovery") && this.isTargetDiscoverySystem) {
            this.loadingTemplate = true;
            if (this.seededSystem && !this.connections) {
                this.loadingConnections = true;
                this.intgApiService.getConnection("", this.seededSystem.id).then((response: IProtocolResponse<IIPage<IIConnection>>): void => {
                    this.connections = response.result && response.data.content ? response.data.content : [];
                    this.loadingConnections = false;
                });
            }
            this.intgWorkflow.getSeededWorkflowTemplate(this.systemName).subscribe((response: IProtocolResponse<IPageableOf<ISeededWorkflowTemplate>>) => {
                if (response.result) {
                    response.data.content.forEach((workflowTemplate: ISeededWorkflowTemplate) => {
                        const moduleDetails = this.getModuleIconByModuleName(workflowTemplate.otModule);
                        this.targetedDataDiscoveryList.push({
                            titleKey: `${moduleDetails.moduleTitle} : ${workflowTemplate.name}`,
                            textKey: workflowTemplate.description,
                            url: moduleDetails.moduleIcon,
                            templateId: workflowTemplate.id,
                        });
                    });
                }
                this.loadingTemplate = false;
            });
        }
    }

    onTabClick(value: ITabsNav) {
        this.selectedTab = value.id;
    }

    trackByIndex(index, item): number {
        return index;
    }

    getModuleIconByModuleName(moduleName: string): {moduleIcon: string, moduleTitle: string} {
        const moduleDetails = {moduleIcon: "", moduleTitle: ""};
        switch (moduleName) {
            case TargetDiscoveryModule.CONSENT:
                moduleDetails.moduleIcon = TargetDiscoveryModuleIcon.CONSENT;
                moduleDetails.moduleTitle = this.translatePipe.transform("Consent");
                break;
            case TargetDiscoveryModule.DSAR:
                moduleDetails.moduleIcon = TargetDiscoveryModuleIcon.DSAR;
                moduleDetails.moduleTitle = this.translatePipe.transform("DSAR");
                break;
            case TargetDiscoveryModule.DATAMAPPING:
                moduleDetails.moduleIcon = TargetDiscoveryModuleIcon.DATAMAPPING;
                moduleDetails.moduleTitle = this.translatePipe.transform("DataMapping");
                break;
        }
        return moduleDetails;
    }

    openModal(templateId: string) {
        this.otModalService
        .create(
            IntgSeedWorkFlowCreateModal,
            {
                isComponent: true,
                size: ModalSize.SMALL,
            },
            {
                templateId,
            },
        );
    }
}
