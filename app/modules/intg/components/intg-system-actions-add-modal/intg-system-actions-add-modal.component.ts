// Angular
import {
    Component,
    Inject,
    OnInit,
} from "@angular/core";
import {
    FormBuilder,
    FormGroup,
    Validators,
    FormArray,
    AbstractControl,
    FormControl,
    ValidationErrors,
} from "@angular/forms";

// 3rd party
import {
     map,
     forEach,
     find,
     keys,
     trim,
} from "lodash";

// Redux / Tokens
import { IStore } from "interfaces/redux.interface";
import { StoreToken } from "tokens/redux-store.token";

// Services
import { IntgWorkflowService } from "intsystemservice/intg-workflow.service";

// Pipes and constants.
import { TranslatePipe } from "pipes/translate.pipe";
import {
    IntgActionMethodTypes,
    ProcessType,
    IntgActionHeaderTypes,
    SourceType,
} from "modules/intg/constants/integration.constant";
import { WorkflowType } from "modules/intg/enums/workflow.enum";
import {
    NO_BLANK_SPACE,
    LINK,
} from "constants/regex.constant";

// Interfaces
import {

    IIWorkflowDetail,
    IIWorkflowNode,
    IICreateWorkflow,
    IIWorkflow,
    IICreateActions,
    IIField,
} from "modules/intg/interfaces/integration.interface";
import {
    IKeyValue,
    IStringMap,
} from "interfaces/generic.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";
import { IOtModalContent } from "@onetrust/vitreus";

// RxJs
import {
    Subject,
    forkJoin,
    Observable,
 } from "rxjs";

@Component({
    selector: "intg-system-actions-add-modal",
    templateUrl: "./intg-system-actions-add-modal.component.html",
})
export class IntgSystemActionsAddModal implements IOtModalContent, OnInit {
    otModalData: { integrationId: string, workflow?: IIWorkflow };
    otModalCloseEvent: Subject<boolean>;
    ready = false;
    selectedOption = [];
    modalTitle = "AddAction";
    actionForm: FormGroup;
    isEdit: boolean;
    inputValue: string;
    saveInProgress = false;
    workflowDetail: IIWorkflowDetail;
    credentialId: string;
    lookUpValues: IIField[];
    isLookUpOpen = false;
    allowOtherPrependText = "AddOption";
    transformedRequestSchema: any;
    transformedResponseSchema: any;
    sourceType = SourceType;

    methodTypes: Array<IKeyValue<string>> = [
        {
            key: this.translatePipe.transform(`Method.${IntgActionMethodTypes.GET}`),
            value: IntgActionMethodTypes.GET,
        },
        {
            key: this.translatePipe.transform(`Method.${IntgActionMethodTypes.PUT}`),
            value: IntgActionMethodTypes.PUT,
        },
        {
            key: this.translatePipe.transform(`Method.${IntgActionMethodTypes.PATCH}`),
            value: IntgActionMethodTypes.PATCH,
        },
        {
            key: this.translatePipe.transform(`Method.${IntgActionMethodTypes.POST}`),
            value: IntgActionMethodTypes.POST,
        },
        {
            key: this.translatePipe.transform(`Method.${IntgActionMethodTypes.DELETE}`),
            value: IntgActionMethodTypes.DELETE,
        },
    ];

    headerTypes: Array<IKeyValue<string>> = [
        {
            key: IntgActionHeaderTypes.Content_Type,
            value: IntgActionHeaderTypes.Content_Type,
        },
        {
            key: IntgActionHeaderTypes.Authorization,
            value: IntgActionHeaderTypes.Authorization,
        },
        {
            key: IntgActionHeaderTypes.Accept,
            value: IntgActionHeaderTypes.Accept,
        },
    ];

    constructor(
        @Inject(StoreToken) private store: IStore,
        private readonly formBuilder: FormBuilder,
        private readonly translatePipe: TranslatePipe,
        private intgWorkflowApi: IntgWorkflowService,
    ) {}

    ngOnInit() {
        this.isEdit = Boolean(this.otModalData.workflow);
        if (this.isEdit) {
            this.modalTitle = "EditAction";
            this.getWorkflowDetails(this.otModalData.workflow.id);
        } else {
            this.modalTitle = "AddAction";
            this.ready = true;
        }
        this.actionForm = this.initializeForm();
        }

    onModelChange({ currentValue }, index) {
        this.selectedOption[index] = currentValue;
        const control = this.actionForm.controls["optionalHeader"] as FormArray;
        const selectedHeader = control.at(index);
        if (selectedHeader) {
            selectedHeader.patchValue({ key: currentValue ? currentValue.value : null});
        }
    }

    onClickAddActions() {
        this.saveWorkflow();
    }

    onRemoveOptionClick(index: number) {
        const control = this.actionForm.controls["optionalHeader"] as FormArray;
        control.removeAt(index);
        this.selectedOption.splice(index, 1);
    }

    onAddOptionClick() {
        const control = this.actionForm.controls["optionalHeader"] as FormArray;
        control.push(this.formBuilder.group({
            key: ["", Validators.pattern(NO_BLANK_SPACE)],
            value: ["", Validators.pattern(NO_BLANK_SPACE)],
        }));
    }

    closeModal(flag?: boolean) {
        this.otModalCloseEvent.next(flag ? flag : false);
        this.otModalCloseEvent.complete();
    }

    trackByKey(index: number, field: IIField) {
        return field.key;
    }

    handleFirstOption(value: string, index: number) {
        const selectedOp = { value, key: value, optionType: "OTHERS" };
        this.selectedOption[index] = selectedOp;
        const control = this.actionForm.controls["optionalHeader"] as FormArray;
        const selectedHeader = control.at(index);
        if (selectedHeader) {
            selectedHeader.patchValue({ key: value });
        }
    }

    onInputChange({ key, value }: IKeyValue<string>) {
        if (key === "Enter") return;
        this.inputValue = value;
    }

    onValueChange(value: string, index: number) {
        const control = this.actionForm.controls["optionalHeader"] as FormArray;
        const selectedHeader = control.at(index);
        if (!trim(value) || !selectedHeader.get("key").value) {
            selectedHeader.get("key").setErrors({ incorrect: true });
        }
    }

    private initializeForm(): FormGroup {
        return this.formBuilder.group({
            name: ["", [Validators.required, Validators.pattern(NO_BLANK_SPACE)]],
            description: ["", [Validators.required, this.emptyCheck]],
            endpointPath: ["", [Validators.required, this.refernceUrlValidator]],
            optionalHeader: this.getOptionalHeaderFormArray(),
            method: ["", Validators.required],
            requestBody: ["", this.validateJson.bind(this)],
            responseBody: ["", this.validateJson.bind(this)],
        });
    }

    private setFormValues(workflowDetail: IIWorkflowDetail) {
        const httpWorkflowNode = find(workflowDetail.workflowNodes, (node: IIWorkflowNode) => node.processType === ProcessType.HTTP);
        const headers = this.getOptionalHeaderFormArray(httpWorkflowNode);
        let requestBody = null;
        let responseBody = null;

        if (workflowDetail.workflowNodes[0].requestSchema && workflowDetail.workflowNodes[0].requestSchema.schema) {
            requestBody = workflowDetail.workflowNodes[0].requestSchema.sample;
        }
        if (workflowDetail.workflowNodes[0].responseSchema && workflowDetail.workflowNodes[0].responseSchema.schema) {
            responseBody = workflowDetail.workflowNodes[0].responseSchema.sample;
        }

        try {
            // Converting the string to pretty format if it is a valid Json.
            requestBody = requestBody ? JSON.stringify(JSON.parse(requestBody), undefined, 2) : "";
            responseBody = responseBody ? JSON.stringify(JSON.parse(responseBody), undefined, 2) : "";

        } catch (e) {
            // Kept the catch block empty so that if action body is not a valid json we keep showing body in normal format.
        }

        this.actionForm.patchValue({
            name: workflowDetail.name,
            description: workflowDetail.description,
            endpointPath: httpWorkflowNode ? httpWorkflowNode.processContext.HttpConnector.requestURI : "",
            method: httpWorkflowNode ? this.getSelectedMethodType(httpWorkflowNode.processContext.HttpConnector.httpMethod) : "",
            requestBody,
            responseBody,
        }, {
                emitEvent: false,
            });
        this.actionForm.setControl("optionalHeader", headers);
    }

    private getOptionalHeaderFormArray(node?: IIWorkflowNode): FormArray {
        const headers = node ? node.processContext.HttpConnector.headers : {};
        let index = 0;
        if (keys(headers).length) {
            return this.formBuilder.array(map(headers, (value: string, key: string) => {
                this.selectedOption[index++] = { key, value: key };
                return this.formBuilder.group({
                    key: [key, Validators.pattern(NO_BLANK_SPACE)],
                    value: [value, Validators.pattern(NO_BLANK_SPACE)],
                });
            }));

        }
        return this.formBuilder.array([this.formBuilder.group({
            key: ["", Validators.pattern(NO_BLANK_SPACE)],
            value: ["", Validators.pattern(NO_BLANK_SPACE)],
        })]);
    }

    private getSelectedMethodType(methodType: string) {
        return find(this.methodTypes, (type) => type.value === methodType);
    }

    private refernceUrlValidator = (option: FormControl): ValidationErrors | null => {
        const valid = (option.value as string).startsWith("${") || LINK.test(option.value);
        return valid ? null : { invalidUrl: true } ;
    }

    private emptyCheck(option: FormControl): ValidationErrors | null {
        return option.value && option.value.trim() ? null : { invalidInput: true } ;
    }

    private saveWorkflow() {
        this.saveInProgress = true;
        const workflow: IICreateWorkflow = {
            enabled: true,
            name: this.actionForm.get("name").value,
            description: this.actionForm.get("description").value,
            integrationId: this.isEdit ? this.workflowDetail.integrationId : this.otModalData.integrationId,
            workflowType: WorkflowType.CUSTOM,
        };

        if (this.isEdit) {
            workflow.id = this.workflowDetail.id;
        }
        this.intgWorkflowApi.saveWorkflow(workflow).then((response: IProtocolResponse<IIWorkflow>) => {
            if (response.result) {
                this.saveAction(response.data);
            } else {
                this.saveInProgress = false;
            }
        });
    }

    private saveAction(workflow: IIWorkflow) {
        const parseSchemaObservables: Array<Observable<IProtocolResponse<any>>> = [];
        if (this.actionForm.value.requestBody && this.actionForm.value.responseBody) {
            parseSchemaObservables.push(this.intgWorkflowApi.extractWorkflowSchema(this.actionForm.value.requestBody));
            parseSchemaObservables.push(this.intgWorkflowApi.extractWorkflowSchema(this.actionForm.value.responseBody));
            forkJoin(parseSchemaObservables).subscribe(([parsedRequestSchema, parsedResponseSchema]: Array<IProtocolResponse<any>>) => {
                if (parsedRequestSchema.result && parsedResponseSchema.result) {
                    this.transformedRequestSchema = JSON.stringify(parsedRequestSchema.data);
                    this.transformedResponseSchema = JSON.stringify(parsedResponseSchema.data);
                    this.createAction(workflow);
                }
            });
        } else if (this.actionForm.value.requestBody) {
            this.intgWorkflowApi.extractWorkflowSchema(this.actionForm.value.requestBody).
                subscribe((requestSchema: IProtocolResponse<any>) => {
                    if (requestSchema.result) {
                        this.transformedRequestSchema = JSON.stringify(requestSchema.data);
                        this.transformedResponseSchema = null;
                        this.createAction(workflow);
                    }
                });
        } else if (this.actionForm.value.responseBody) {
            this.intgWorkflowApi.extractWorkflowSchema(this.actionForm.value.responseBody).
                subscribe((responseSchema: IProtocolResponse<any>) => {
                    if (responseSchema.result) {
                        this.transformedResponseSchema = JSON.stringify(responseSchema.data);
                        this.transformedRequestSchema = null;
                        this.createAction(workflow);
                    }
            });
        } else {
            this.createAction(workflow);
        }
    }

    private createAction(workflow: IIWorkflow) {
        this.intgWorkflowApi.createActions(workflow.id, this.setSaveActionPayload(workflow)).then((response) => {
            if (response.result) {
                this.closeModal(true);
            }
            this.saveInProgress = false;
        });
    }
    private setSaveActionPayload(workflow: IIWorkflow): IICreateActions {
        const workflowDetail: IICreateActions = {
            id: workflow.id,
            name: workflow.name,
            description: workflow.description,
            integrationId: workflow.integrationId,
            enabled: workflow.enabled,
            workflowType: workflow.workflowType,
            rawDsl: workflow.rawDsl,
            actions: [this.getHttpAction(workflow.id)],
            directions: [],
            trigger: null,
        };
        return workflowDetail;
    }

    private getHttpAction(workflowId: string): IIWorkflowNode {
        const formValues = this.actionForm.value;
        const savedRequestSchema = this.workflowDetail ? this.workflowDetail.workflowNodes[0].requestSchema : null;
        const savedResponseSchema = this.workflowDetail ? this.workflowDetail.workflowNodes[0].requestSchema : null;

        const httpAction: IIWorkflowNode = {
            nodeLabel: "Poster",
            processType: ProcessType.HTTP,
            workflowId,
            processContext: {
                HttpConnector: {
                    type: ProcessType.HTTP,
                    headers: this.getHeaders(),
                    requestURI: formValues.endpointPath,
                    httpMethod: formValues.method.value,
                },
            },
            requestSchema:  this.transformedRequestSchema ? savedRequestSchema ?
            { id: savedRequestSchema.id, name: formValues.name, schema: this.transformedRequestSchema, lookupFields: null, sample: null } :
            { id: null, name: formValues.name, schema: this.transformedRequestSchema, lookupFields: null, sample: null } : null,

            responseSchema:  this.transformedResponseSchema ? savedResponseSchema ?
            { id: savedResponseSchema.id, name: formValues.name, schema: this.transformedResponseSchema, lookupFields: null, sample: null } :
            { id: null, name: formValues.name, schema: this.transformedResponseSchema, lookupFields: null, sample: null } : null,
        };

        if (this.workflowDetail && this.workflowDetail.workflowNodes && this.workflowDetail.workflowNodes.length) {
            const httpWorkflowNode = find(this.workflowDetail.workflowNodes, (node: IIWorkflowNode) => node.processType === ProcessType.HTTP);
            httpAction.id = httpWorkflowNode.id;
        }
        return httpAction;
    }

    private getHeaders(): IStringMap<string> {
        const headers = {};
        const headerArray = this.actionForm.get("optionalHeader").value;
        forEach(headerArray, (header: IKeyValue<string>) => {
            if (header.key) {
                headers[header.key] = header.value;
            }
        });
        return headers;
    }
    private getWorkflowDetails(workflowId: string) {
        this.intgWorkflowApi.getWorkflowById(workflowId, true).then(
            (response: IProtocolResponse<IIWorkflowDetail>) => {
                if (response.result) {
                    this.workflowDetail = response.data;
                    this.ready = true;
                    this.setFormValues(this.workflowDetail);
                }
            },
        );
    }
    private validateJson(control: AbstractControl) {
        if (control.value  && !this.isJson(control.value)) {
            return  {validJson : true};
        }
        return null;
    }

    private isJson(payload: string): boolean {
        if (payload) {
            try {
                payload = JSON.parse(payload);
            } catch (e) {
                return false;
            }
            return (typeof payload === "object" && payload !== null);
        } else {
            return true;
        }
    }
}
