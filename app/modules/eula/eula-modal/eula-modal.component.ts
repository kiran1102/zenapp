// Angular
import {
    Component,
    Inject,
    OnInit,
} from "@angular/core";

// 3rd party
declare var PDFJS: any;
import { each } from "lodash";

// Services
import { ModalService } from "sharedServices/modal.service";
import { EulaService } from "../shared/eula.service";
import { EulaPdfLoader } from "../shared/eula.pdfloader.service";
import { AuthHandlerService } from "modules/shared/services/helper/auth-handler.service";

// Tokens
import { StoreToken } from "tokens/redux-store.token";

// Redux
import { getModalData } from "oneRedux/reducers/modal.reducer";

// Rxjs
import { filter } from "rxjs/operators";

// Interfaces
import { IStore } from "interfaces/redux.interface";
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";

@Component({
    selector: "eula-modal",
    templateUrl: "./eula-modal.component.html",
})
export class EulaModal implements OnInit {

    disableAcceptBtn: boolean;
    private agreementTermId: string;
    private callback: () => void ;

    constructor(
        @Inject(StoreToken) public store: IStore,
        @Inject("$rootScope") public $rootScope: IExtendedRootScopeService,
        private modalService: ModalService,
        private eulaService: EulaService,
        private eulaPdfLoader: EulaPdfLoader,
        private authHandlerService: AuthHandlerService,
    ) {}

    ngOnInit() {
        this.$rootScope.eulaModalOpen = true;
        this.disableAcceptBtn = true;
        this.callback = getModalData(this.store.getState()).callback;
        this.eulaService.getlatestEulaTerm()
        .pipe(
            filter((response) => response !== null),
        )
        .subscribe((response: any) => {
            this.agreementTermId = response.data.agreementTermId;
            this.eulaPdfLoader.loadEulaAgreementFromPdf(response.data.url).then(() => {
                this.eulaPdfLoader.detectEulaDomChanges((mutations: MutationRecord[]) => {
                    each(mutations, (mutation: MutationRecord) => {
                        if (mutation.removedNodes.length) {
                            this.declineEula();
                        }
                    });
                });
                this.disableAcceptBtn = false;
            });
        });
    }

    acceptEula() {
        this.eulaService.saveEulaAcceptance(this.agreementTermId).subscribe(() => {
            this.closeModal();
            if (this.callback) {
                this.callback();
            }
        });
    }

    declineEula() {
        this.closeModal();
        this.authHandlerService.logout();
    }

    closeModal(): void {
        this.$rootScope.eulaModalOpen = false;
        this.eulaPdfLoader.stopDetectingEulaDomChanges();
        this.modalService.closeModal();
        this.modalService.clearModalData();
    }
}
