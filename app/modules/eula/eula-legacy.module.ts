import angular from "angular";

import { EulaPdfLoader } from "./shared/eula.pdfloader.service";

export const eulaLegacyModule = angular
    .module("zen.eula", [
    ])
    .service("EulaPdfLoader", EulaPdfLoader)
    ;
