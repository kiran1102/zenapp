import { NgModule } from "@angular/core";

// Modules
import { SharedModule } from "sharedModules/shared.module";
import { OtButtonModule } from "@onetrust/vitreus";
import { OTPipesModule, PIPES } from "modules/pipes/pipes.module";

// Components
import { EulaModal } from "./eula-modal/eula-modal.component";

// Services
import { EulaService } from "./shared/eula.service";
import { EulaPdfLoader } from "./shared/eula.pdfloader.service";

// Legacy Provider Injections
export function getLegacyEulaPdfLoader(injector) {
    return injector.get("EulaPdfLoader");
}

@NgModule({
    imports: [
        SharedModule,
        OTPipesModule,
        OtButtonModule,
    ],
    declarations: [
        EulaModal,
    ],
    entryComponents: [
        EulaModal,
    ],
    providers: [
        EulaService,
        {
            provide: EulaPdfLoader,
            deps: ["$injector"],
            useFactory: getLegacyEulaPdfLoader,
        },
        PIPES, // TODO: Remove from providers when ot-common is implemented. Only necessary if you are using pipe in TS file, else just module is fine
    ],
})
export class EulaModule { }
