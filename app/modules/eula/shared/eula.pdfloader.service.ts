
// tslint:disable-next-line: interface-name
interface System {
    import<T>(request: string): Promise<T>;
}

declare var System: System;

export class EulaPdfLoader {
    static $inject: string[] = ["$q"];

    private pdfDoc: any = null;
    private scale = 3;
    private pageCount = 0;
    private pageNumber = 0;
    private observer: MutationObserver;

    constructor(private $q: ng.IQService) { }

    public loadEulaAgreementFromPdf(url: string): angular.IPromise<{}> {

        const deferred: angular.IDeferred<{}> = this.$q.defer();

        // tslint:disable-next-line:only-arrow-functions
        System.import(/* webpackChunkName: "pdfjs-dist" */ "pdfjs-dist").then((m: any) => {
            m.PDFJS.workerSrc = "./scripts/pdf.worker.min.js";
            m.PDFJS.getDocument(url)
                .then((pdfDoc_: any) => {
                    this.pdfDoc = pdfDoc_;
                    this.pageCount = this.pdfDoc.numPages;
                    if (this.pageNumber > 0) {
                        this.cleanEulaContainer();
                    }
                    this.pageNumber = 1;
                    this.renderPage(deferred);
                });

        });

        return deferred.promise;
    }

    // Handle EULA modal being called multiple times on login.
    public cleanEulaContainer(): void {
        this.stopDetectingEulaDomChanges();
        const wrapper: Element = document.querySelector(".eula-modal__agreement");
        if (wrapper) {
            while (wrapper.firstChild) {
                wrapper.removeChild(wrapper.firstChild);
            }
        }
    }

    public detectEulaDomChanges(callback: MutationCallback): void {
        const container: Element = document.querySelector(".modal");
        this.observer = new MutationObserver(callback);
        this.observer.observe(container, {
            attributes: true,
            childList: true,
            characterData: true,
            subtree: true,
        });
    }

    public stopDetectingEulaDomChanges(): void {
        if (this.observer) {
            this.observer.disconnect();
        }
    }

    private renderPage(deferred: any): void {

        this.pdfDoc.getPage(this.pageNumber).then((page: any): void => {
            const canvasId: string = "pdf-viewer-" + this.pageNumber;
            const wrapper = document.querySelector(".eula-modal__agreement") as any;
            const canvas = document.createElement("canvas") as any;
            wrapper.appendChild(canvas);
            canvas.setAttribute("id", canvasId);

            const viewport = page.getViewport(this.scale);
            canvas.width = viewport.width;
            canvas.height = viewport.height;
            canvas.style.width = "100%";
            canvas.style.height = "100%";

            const ctx = canvas.getContext("2d");
            const renderContext = {
                canvasContext: ctx,
                viewport,
            };
            const renderTask = page.render(renderContext);

            renderTask.promise.then((): void => {
                this.pageNumber++;
                if (this.pageNumber <= this.pageCount) {
                    this.renderPage(deferred);
                } else {
                    deferred.resolve();
                }
            });
        });
    }

}
