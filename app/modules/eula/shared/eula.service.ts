// Angular
import { Inject, Injectable } from "@angular/core";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

// Interfaces
import { IProtocolConfig, IProtocolMessages } from "interfaces/protocol.interface";

// Services
import { ModalService } from "sharedServices/modal.service";
import { AuthHandlerService } from "modules/shared/services/helper/auth-handler.service";
import { ProtocolService } from "modules/core/services/protocol.service";
import { StateService } from "@uirouter/core";

// Reducer
import { isNotProjectViewer, getCurrentUser } from "oneRedux/reducers/user.reducer";

// Redux
import { StoreToken } from "tokens/redux-store.token";

// rxjs
import { Observable, from } from "rxjs";

// Interfaces
import { IStore } from "interfaces/redux.interface";
import { IUser } from "interfaces/user.interface";

@Injectable()
export class EulaService {
    readonly EulaAgreementTypeId = 10;

    constructor(
        @Inject(StoreToken) private store: IStore,
        private translatePipe: TranslatePipe,
        private stateService: StateService,
        private modalService: ModalService,
        private authHandlerService: AuthHandlerService,
        private protocolService: ProtocolService,
    ) {}

    public displayEula(callback?: () => void) {
        this.getEulaAcceptance().subscribe((response) => {
            if (response && !response.data.accepted) {
                    this.resetLandingPage();
                    this.modalService.setModalData({callback});
                    this.modalService.openModal("eulaModal");
            } else if (callback) {
                callback();
            }
        },
            () => {
                this.authHandlerService.logout();
        });
    }

    public getEulaAcceptance(): Observable<any> {
        const config: IProtocolConfig = this.protocolService.customConfig("GET", `/api/agreement/v1/acceptances/${this.EulaAgreementTypeId}?version=@latest`);
        const messages: IProtocolMessages = {
            Error: { custom: this.translatePipe.transform("ErrorGettingEulaAcceptance") },
        };
        return from (this.protocolService.http(config, messages));
    }

    public getlatestEulaTerm(): Observable<any> {
        const config: IProtocolConfig = this.protocolService.customConfig("GET", `/api/agreement/v1/agreements/${this.EulaAgreementTypeId}/latest?fallbackLanguage=en-us`);
        const messages: IProtocolMessages = {
            Error: { custom: this.translatePipe.transform("ErrorGettingLatestEula") },
        };
        return from (this.protocolService.http(config, messages));
    }

    public saveEulaAcceptance(eulaTermId: string): Observable<any> {
        const config: IProtocolConfig = this.protocolService.customConfig("POST", `/api/agreement/v1/acceptances/${this.EulaAgreementTypeId}/terms/${eulaTermId}`, {});
        const messages: IProtocolMessages = {
            Error: { custom: this.translatePipe.transform("ErrorSavingEulaAcceptance") },
        };
        return from (this.protocolService.http(config, messages));
    }

    private resetLandingPage() {
        const currentUser: IUser = getCurrentUser(this.store.getState());
        if (isNotProjectViewer(currentUser)) {
            this.stateService.go("module", { path: "welcome" });
        }
    }
}
