// Angular
import {
    Component,
    OnInit,
    Inject,
} from "@angular/core";

// Service
import { ModalService } from "sharedServices/modal.service";

// Redux
import { getModalData } from "oneRedux/reducers/modal.reducer";
import { StoreToken } from "tokens/redux-store.token";

// Interface
import { IStore } from "interfaces/redux.interface";

@Component({
    selector: "aa-unapproval-modal",
    template: `
        <one-modal-base
            [modalTitle]="modalTitle"
            (close)="closeModal()"
            >
            <div class="padding-left-right-3 margin-top-2 text-bold text-center"
                [innerText]="unapproveConfirmationText"
                >
            </div>
            <footer class="row-flex-end padding-all-2">
                <button otButton neutral
                    [innerText]="'Cancel' | otTranslate"
                    (click)="closeModal()"
                ></button>
                <button otButton brand
                    [innerText]="'Confirm' | otTranslate"
                    [loading]="isSubmitting"
                    [disabled]="isSubmitting"
                    (click)="handleUnpprove()"
                ></button>
            </footer>
        </one-modal-base>
    `,
})
export class AAUnapprovalModal implements OnInit {
    modalData: any;
    modalTitle: string;
    unapproveConfirmationText: string;
    isSubmitting: boolean;

    private promiseToResolve: () => ng.IPromise<boolean>;

    constructor(
        @Inject(StoreToken) private readonly store: IStore,
        private modalService: ModalService,
    ) {}

    ngOnInit(): void {
        this.modalData = getModalData(this.store.getState());
        this.modalTitle = this.modalData.modalTitle;
        this.unapproveConfirmationText = this.modalData.unapproveConfirmationText;
        this.promiseToResolve = this.modalData.promiseToResolve;
    }

    handleUnpprove() {
        if (typeof this.promiseToResolve === "function") {
            this.isSubmitting = true;
            this.promiseToResolve().then((success: boolean) => {
                if (success) {
                    this.modalService.handleModalCallback();
                    this.modalService.closeModal();
                    this.modalService.clearModalData();
                }
                this.isSubmitting = false;
            });
        }
    }

    closeModal(): void {
        this.modalService.closeModal();
        this.modalService.clearModalData();
    }
}
