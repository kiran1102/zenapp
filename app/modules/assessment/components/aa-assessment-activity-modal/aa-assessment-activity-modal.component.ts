// Angular
import {
    Component,
    Inject,
} from "@angular/core";

// 3rd Party
import { Subject } from "rxjs";
import { isUndefined } from "lodash";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

// Services
import { TimeStamp } from "oneServices/time-stamp.service";
import { AAActivityActionService } from "modules/assessment/services/action/aa-assessment-activity-action.service";

// Interfaces
import { IStringMap } from "interfaces/generic.interface";
import { IAssessmentTableRow } from "interfaces/assessment/assessment.interface";
import {
    IActivityHistoryItem,
    IFormattedActivity,
    IActivityMetaData,
    IAssessmentActivityParams,
    IActivityChange,
} from "interfaces/assessment/aa-assessment-activity.interface";
import { IPageable } from "interfaces/pagination.interface";
import { ILabelValue } from "interfaces/generic.interface";
import { IStore } from "interfaces/redux.interface";

// Redux
import { StoreToken } from "tokens/redux-store.token";

// Constants
import { Color } from "constants/color.constant";
import { AssessmentActivityKeys } from "constants/assessment.constants";

// Enums
import { AssessmentActivity } from "enums/assessment/assessment-status.enum";

@Component({
    selector: "assessment-activity",
    templateUrl: "./aa-assessment-activity-modal.component.html",
})
export class AAActivityModalComponent {
    otModalData: IAssessmentTableRow;
    otModalCloseEvent: Subject<any>;
    expandedItemMap: IStringMap<boolean> = {};
    allItemsExpanded = true;
    activitiesDescending = true;
    isLoadingMoreActivities = false;
    loading = true;
    hasMoreActivities: boolean;
    activity: IFormattedActivity;
    activityType: string;
    formattedActivities: IFormattedActivity[] = [];
    selectedOption: ILabelValue<string> = {
        label: "All",
        value: "ALL",
    };
    assessmentFilterStates: Array<ILabelValue<string>> = this.setFilterStates();

    private activityFilter: IAssessmentActivityParams = {
        filter: "ALL",
        size: 8,
        page: 0,
        text: "",
        sort: "createdDate,desc",
    };

    private activityList: IActivityHistoryItem[];
    private activityListMeta: IPageable;
    private destroy$ = new Subject();
    private activityMetaData: IActivityMetaData;

    constructor(
        @Inject(StoreToken) public store: IStore,
        private AssessmentActivityAction: AAActivityActionService,
        private timeStampService: TimeStamp,
        private translatePipe: TranslatePipe,
    ) { }

    ngOnInit(): void {
        this.assessmentFilterStates = this.setFilterStates();
        this.getFormattedActivities();
    }

    ngOnDestroy(): void {
        this.destroy$.next();
        this.destroy$.complete();
    }

    selectOption(selectedOption: ILabelValue<string>): void {
        this.selectedOption = selectedOption;
        this.refreshActivities();
    }

    toggleActivity(itemId: string): void {
        this.expandedItemMap[itemId] = !this.expandedItemMap[itemId];
    }

    toggleExpand(): void {
        this.allItemsExpanded ? this.collapseAll() : this.expandAll();
        this.allItemsExpanded = !this.allItemsExpanded;
    }

    toggleOrder(): void {
        this.activitiesDescending = !this.activitiesDescending;
        this.refreshActivities();
    }

    addActivitiesToView(): void {
        this.isLoadingMoreActivities = true;
        if (this.hasMoreActivities) {
            this.activityFilter.page += 1;
            this.getActivities()
                .then(() => this.isLoadingMoreActivities = false);
        }
    }

    trackByActivity(index: number, activity: IFormattedActivity): string {
        return activity.id;
    }

    closeModal(): void {
        this.otModalCloseEvent.next();
    }

    private setFilterStates(): Array<ILabelValue<string>> {
        return [
            { label: this.translatePipe.transform(AssessmentActivityKeys.ALL), value: AssessmentActivity.ALL },
            { label: this.translatePipe.transform(AssessmentActivityKeys.STAGE_CHANGED), value: AssessmentActivity.STATE_CHANGED },
            { label: this.translatePipe.transform(AssessmentActivityKeys.NAME_CHANGED), value: AssessmentActivity.NAME_CHANGED },
            { label: this.translatePipe.transform(AssessmentActivityKeys.DEADLINE_CHANGED), value: AssessmentActivity.DEADLINE_CHANGED },
            { label: this.translatePipe.transform(AssessmentActivityKeys.APPROVER_CHANGED), value: AssessmentActivity.APPROVER_CHANGED },
            { label: this.translatePipe.transform(AssessmentActivityKeys.RESPONDENT_CHANGED), value: AssessmentActivity.RESPONDENT_CHANGED },
            { label: this.translatePipe.transform(AssessmentActivityKeys.ASSESSMENT_SHARED), value: AssessmentActivity.VIEWER_CHANGED },
            { label: this.translatePipe.transform(AssessmentActivityKeys.TEMPLATE_CHANGED), value: AssessmentActivity.TEMPLATE_CHANGED },
        ];
    }

    private getFormattedActivities(clearData = false): void {
        this.loading = true;
        this.getActivities(clearData)
            .then(() => {
                this.loading = false;
            });
    }

    private getActivities(clearData = false): ng.IPromise<void> {
        return this.AssessmentActivityAction.getRecordActivity(this.otModalData.assessmentId, this.activityFilter)
            .then((response: IPageable): void => {
                if (response) {
                    if (clearData) {
                        this.formattedActivities = [];
                        this.activityList = [];
                    }
                    this.activityList = response.content;
                    this.activityListMeta = response;
                    this.hasMoreActivities = !this.activityListMeta.last;
                    if (this.activityList && this.activityList.length) {
                        this.formatData();
                        this.expandedItemMap = this.setExpandedItemMap(this.formattedActivities, this.expandedItemMap);
                    }
                }
            });
    }

    private formatData(): void {
        this.activityList.forEach((activity: IActivityHistoryItem, index: number): void => {
            if (activity) {
                this.activityMetaData = this.setActivityMetaData(activity);
                if (this.activityMetaData) {
                    this.activityType = activity.activities[0].activityType;
                    activity.activities[0].fieldName = this.translatePipe.transform(activity.activities[0].fieldName);

                    if (this.activityType === "ASSESSMENT_CREATED") {
                        activity.activities[0].fieldName = activity.activities[0].assessmentName;
                    } else if (this.activityType === "INCIDENT_LINK_ADDED" || this.activityType === "INVENTORY_LINK_ADDED") {
                        activity.activities[0].fieldName = activity.activities[0].newValue ? activity.activities[0].newValue
                            : activity.activities[0].fieldName;
                    } else if (this.activityType === "INVENTORY_LINK_REMOVED" || this.activityType === "INCIDENT_LINK_REMOVED") {
                        activity.activities[0].fieldName = activity.activities[0].oldValue ? activity.activities[0].oldValue
                            : activity.activities[0].fieldName;
                    }

                    this.formattedActivities.push({
                        id: activity.id,
                        title: this.translatePipe.transform(this.activityMetaData.iconHeader, {
                            userName: activity.userName,
                            result: activity.activities[0].newValue,
                        }),
                        timeStamp: this.timeStampService.formatJavaDate(activity.timeStamp),
                        icon: this.activityMetaData.icon,
                        iconColor: this.activityMetaData.iconColor,
                        iconBackgroundColor: this.activityMetaData.iconBackgroundColor,
                        activities: this.formatActivities(activity.activities),
                        isOldNewLabelEmpty: this.activityMetaData.isOldNewLabelEmpty,
                        fieldLabel: this.activityMetaData.fieldLabel,
                        oldFieldLabel: this.activityMetaData.oldFieldLabel,
                        newFieldLabel: this.activityMetaData.newFieldLabel,
                    });
                }
            }
        });
    }

    private formatActivities(activity: IActivityChange[]): IActivityChange[] {
        const { newValue, oldValue } = activity[0];
        activity[0].oldValue = oldValue ? oldValue.split(",").join(", ") : "";
        activity[0].newValue = newValue ? newValue.split(",").join(", ") : "";
        return activity;
    }

    private setActivityMetaData(activity: IActivityHistoryItem): IActivityMetaData {
        switch (activity.activities[0].activityType) {
            case "ORG_GROUP_CHANGED":
                this.activityMetaData = {
                    ...this.activityMetaData,
                    iconHeader: "UserChangedOrganization",
                    isOldNewLabelEmpty: false,
                    fieldLabel: this.translatePipe.transform("FieldName"),
                    icon: "ot ot-check-square-o",
                    iconColor: Color.White,
                    iconBackgroundColor: Color.Grey10,
                    oldFieldLabel: this.translatePipe.transform("OldOrganization"),
                    newFieldLabel: this.translatePipe.transform("NewOrganization"),
                };
                break;
            case "APPROVAL_REVOKE":
                this.activityMetaData = {
                    ...this.activityMetaData,
                    iconHeader: "UserClearedResult",
                    isOldNewLabelEmpty: false,
                    fieldLabel: this.translatePipe.transform("FieldName"),
                    icon: "ot ot-check-square-o",
                    iconColor: Color.White,
                    iconBackgroundColor: Color.EditBlue,
                    oldFieldLabel: this.translatePipe.transform("OldResult"),
                    newFieldLabel: this.translatePipe.transform("NewResult"),
                };
                break;
            case "APPROVAL_VOTE":
                this.activityMetaData = {
                    ...this.activityMetaData,
                    iconHeader: "UserApprovedVote",
                    isOldNewLabelEmpty: false,
                    fieldLabel: this.translatePipe.transform("FieldName"),
                    icon: "ot ot-check-square-o",
                    iconColor: Color.White,
                    iconBackgroundColor: Color.EditBlue,
                    oldFieldLabel: this.translatePipe.transform("OldResult"),
                    newFieldLabel: this.translatePipe.transform("NewResult"),
                };
                break;
            case "ASSESSMENT_CREATED":
                this.activityMetaData = {
                    ...this.activityMetaData,
                    iconHeader: "UserCreatedAssessment",
                    isOldNewLabelEmpty: true,
                    fieldLabel: this.translatePipe.transform("AssessmentName"),
                    icon: "ot ot-plus",
                    iconColor: Color.White,
                    iconBackgroundColor: Color.Grey10,
                    oldFieldLabel: "",
                    newFieldLabel: "",
                };
                break;
            case "STATE_CHANGED":
                this.activityMetaData = {
                    ...this.activityMetaData,
                    iconHeader: "UserMadeChanges",
                    isOldNewLabelEmpty: false,
                    fieldLabel: this.translatePipe.transform("FieldName"),
                    icon: "ot ot-check-square-o",
                    iconColor: Color.White,
                    iconBackgroundColor: Color.EditBlue,
                    oldFieldLabel: this.translatePipe.transform("OldStage"),
                    newFieldLabel: this.translatePipe.transform("NewStage"),
                };
                break;
            case "NAME_CHANGED":
                this.activityMetaData = {
                    ...this.activityMetaData,
                    iconHeader: "UserMadeChanges",
                    isOldNewLabelEmpty: false,
                    fieldLabel: this.translatePipe.transform("FieldName"),
                    icon: "ot ot-pencil-square-o",
                    iconColor: Color.White,
                    iconBackgroundColor: Color.Grey10,
                    oldFieldLabel: this.translatePipe.transform("OldAssessmentName"),
                    newFieldLabel: this.translatePipe.transform("NewAssessmentName"),
                };
                break;
            case "APPROVER_CHANGED":
                this.activityMetaData = {
                    ...this.activityMetaData,
                    iconHeader: "UserMadeChanges",
                    isOldNewLabelEmpty: false,
                    fieldLabel: this.translatePipe.transform("FieldName"),
                    icon: "ot ot-pencil-square-o",
                    iconColor: Color.White,
                    iconBackgroundColor: Color.Grey10,
                    oldFieldLabel: this.translatePipe.transform("OldApprover"),
                    newFieldLabel: this.translatePipe.transform("NewApprover"),
                };
                break;
            case "RESPONDENT_CHANGED":
                this.activityMetaData = {
                    ...this.activityMetaData,
                    iconHeader: "UserMadeChanges",
                    isOldNewLabelEmpty: false,
                    fieldLabel: this.translatePipe.transform("FieldName"),
                    icon: "ot ot-pencil-square-o",
                    iconColor: Color.White,
                    iconBackgroundColor: Color.Grey10,
                    oldFieldLabel: this.translatePipe.transform("OldRespondent"),
                    newFieldLabel: this.translatePipe.transform("NewRespondent"),
                };
                break;
            case "DEADLINE_CHANGED":
                this.activityMetaData = {
                    ...this.activityMetaData,
                    iconHeader: "UserMadeChanges",
                    isOldNewLabelEmpty: false,
                    fieldLabel: this.translatePipe.transform("FieldName"),
                    icon: activity.activities[0].newValue ? "ot ot-pencil-square-o" : "ot ot-trash",
                    iconColor: Color.White,
                    iconBackgroundColor: activity.activities[0].newValue ? Color.Grey10 : Color.DestructRed,
                    oldFieldLabel: this.translatePipe.transform("OldDeadline"),
                    newFieldLabel: this.translatePipe.transform("NewDeadline"),
                };
                break;
            case "INVENTORY_LINK_ADDED":
                this.activityMetaData = {
                    ...this.activityMetaData,
                    iconHeader: "UserLinkedInventory",
                    isOldNewLabelEmpty: true,
                    fieldLabel: this.translatePipe.transform("LinkedInventory"),
                    icon: "ot ot-list-ul",
                    iconColor: Color.White,
                    iconBackgroundColor: Color.Green1,
                    oldFieldLabel: "",
                    newFieldLabel: "",
                };
                break;
            case "INVENTORY_LINK_REMOVED":
                this.activityMetaData = {
                    ...this.activityMetaData,
                    iconHeader: "UserUnlinkedInventory",
                    isOldNewLabelEmpty: true,
                    fieldLabel: this.translatePipe.transform("UnlinkedInventory"),
                    icon: "ot ot-list-ul",
                    iconColor: Color.White,
                    iconBackgroundColor: Color.DestructRed,
                    oldFieldLabel: "",
                    newFieldLabel: "",
                };
                break;
            case "INCIDENT_LINK_ADDED":
                this.activityMetaData = {
                    ...this.activityMetaData,
                    iconHeader: "UserLinkedIncident",
                    isOldNewLabelEmpty: true,
                    fieldLabel: this.translatePipe.transform("LinkedIncident"),
                    icon: "ot ot-list-ul",
                    iconColor: Color.White,
                    iconBackgroundColor: Color.Green1,
                    oldFieldLabel: "",
                    newFieldLabel: "",
                };
                break;
            case "INCIDENT_LINK_REMOVED":
                this.activityMetaData = {
                    ...this.activityMetaData,
                    iconHeader: "UserUnlinkedIncident",
                    isOldNewLabelEmpty: true,
                    fieldLabel: this.translatePipe.transform("UnlinkedIncident"),
                    icon: "ot ot-list-ul",
                    iconColor: Color.White,
                    iconBackgroundColor: Color.DestructRed,
                    oldFieldLabel: "",
                    newFieldLabel: "",
                };
                break;
            case "VIEWER_CHANGED":
                this.activityMetaData = {
                    ...this.activityMetaData,
                    iconHeader: "SharedAssessment",
                    isOldNewLabelEmpty: false,
                    fieldLabel: this.translatePipe.transform("SharedWith"),
                    icon: activity.activities[0].newValue ? "ot ot-share" : "ot ot-trash",
                    iconColor: Color.White,
                    iconBackgroundColor: activity.activities[0].newValue ? Color.Grey10 : Color.DestructRed,
                    oldFieldLabel: this.translatePipe.transform("OldValue"),
                    newFieldLabel: this.translatePipe.transform("NewValue"),
                };
                break;
            case "TEMPLATE_CHANGED":
                this.activityMetaData = {
                    ...this.activityMetaData,
                    iconHeader: "TemplateVersionChanged",
                    isOldNewLabelEmpty: false,
                    fieldLabel: this.translatePipe.transform("FieldName"),
                    icon: "ot ot-edit",
                    iconColor: Color.White,
                    iconBackgroundColor: Color.Grey10,
                    oldFieldLabel: this.translatePipe.transform("OldVersion"),
                    newFieldLabel: this.translatePipe.transform("NewVersion"),
                };
                break;
            case "ASSESSMENT_RESULT":
                this.activityMetaData = {
                    ...this.activityMetaData,
                    iconHeader: "AssessmentResultChanged",
                    isOldNewLabelEmpty: false,
                    fieldLabel: this.translatePipe.transform("FieldName"),
                    icon: "ot ot-check-square-o",
                    iconColor: Color.White,
                    iconBackgroundColor: Color.EditBlue,
                    oldFieldLabel: this.translatePipe.transform("OldResult"),
                    newFieldLabel: this.translatePipe.transform("NewResult"),
                };
                break;
            default:
                this.activityMetaData = null;
                break;
        }
        return this.activityMetaData;
    }

    private setExpandedItemMap(formattedActivities: IFormattedActivity[], expandedItemMap: IStringMap<boolean>): IStringMap<boolean> {
        formattedActivities.forEach((activity: IFormattedActivity): void => {
            if (isUndefined(expandedItemMap[activity.id])) {
                expandedItemMap[activity.id] = true;
            }
        });
        return expandedItemMap;
    }

    private setDefaultConfig(filter: ILabelValue<string>): void {
        this.activityFilter = {
            ...this.activityFilter,
            page: 0,
            sort: "createdDate," + (this.activitiesDescending ? "desc" : "asc"),
            filter: filter.value,
        };
    }

    private refreshActivities() {
        this.setDefaultConfig(this.selectedOption);
        this.getFormattedActivities(true);
    }

    private expandAll(): void {
        Object.keys(this.expandedItemMap).forEach((key: string): boolean => this.expandedItemMap[key] = true);
    }

    private collapseAll(): void {
        Object.keys(this.expandedItemMap).forEach((key: string): boolean => this.expandedItemMap[key] = false);
    }
}
