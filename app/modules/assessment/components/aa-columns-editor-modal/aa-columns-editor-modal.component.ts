// Angular
import {
    Component,
    ViewChild,
    OnInit,
} from "@angular/core";

// External
import { IOtModalContent } from "@onetrust/vitreus";
import { Subject } from "rxjs";
import { forEach, filter } from "lodash";

// Interfaces
import { IAssessmentColumns } from "interfaces/assessment/assessment.interface";
import { IGridColumn } from "interfaces/grid.interface";
import { OtDuelingPicklistComponent } from "interfaces/ot-dueling-picklist.interface";
import { IListState } from "@onetrust/vitreus";

@Component({
    selector: "aa-columns-editor-modal",
    templateUrl: "./aa-columns-editor-modal.component.html",
})

export class AAColumnsEditorModalComponent implements IOtModalContent, OnInit {
    otModalCloseEvent: Subject<IListState | boolean>;
    otModalData: IAssessmentColumns;
    tempDisabledColumns: IGridColumn[];
    activeSearchTerm: string;
    searchActive: boolean;
    // TODO: Yet to use these variables on logic
    currentSelectionIndex = 0;
    totalSearchChar = 0;

    @ViewChild("duelingPicklist")
    private duelingListComponent: OtDuelingPicklistComponent;

    ngOnInit(): void {
        this.tempDisabledColumns = this.otModalData.disabledColumns;
    }

    applyColumnSelecetions(): void {
        this.closeModal(this.duelingListComponent.getListState());
    }

    closeModal(data?: IListState): void {
        if (data && this.searchActive) {
            data.left = this.tempDisabledColumns;
        }
        this.otModalCloseEvent.next(data);
    }

    setInactiveSearchTerm(inactiveSearchTerm: string): void {
        const search: string = inactiveSearchTerm.trim().toLowerCase();
        this.searchActive = Boolean(search);
        if (search) {
            this.otModalData.disabledColumns = filter(this.tempDisabledColumns, (disabledColumn: IGridColumn): boolean => {
                return disabledColumn.name.toLowerCase().includes(search);
            });
        } else {
            this.otModalData.disabledColumns = this.tempDisabledColumns;
        }
    }

    setActiveSearchTerm(activeSearchTerm: string): void {
        this.activeSearchTerm = activeSearchTerm;
    }

    removeDisabledColumn(event: IGridColumn) {
        if (this.searchActive) {
            this.tempDisabledColumns.splice(this.tempDisabledColumns.indexOf(event), 1);
        }
    }

    addDisabledColumn(event: IGridColumn) {
        if (this.searchActive) {
            this.tempDisabledColumns.push(event);
        }
    }

    incrementSelection(): void {
        // To do
    }

    decrementSelection(): void {
        // To do
    }
}
