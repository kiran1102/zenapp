// Angular
import {
    Component,
    OnInit,
    Inject,
} from "@angular/core";

// Interfaces
import { IQuestionCommentCreateRequest } from "interfaces/aa-question-comment.interface";
import { IStore } from "interfaces/redux.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

// Services
import { AACommentApiService } from "modules/assessment/services/api/aa-comment-api.service";
import { ModalService } from "sharedServices/modal.service";
import { NotificationService } from "modules/shared/services/provider/notification.service";
import { AaCollaborationPaneCountsService } from "modules/assessment/services/messaging/aa-section-detail-header-helper.service";

// Redux
import { StoreToken } from "tokens/redux-store.token";
import { getModalData } from "oneRedux/reducers/modal.reducer";

// Enums
import { AACollaborationCountsTypes } from "modules/assessment/enums/aa-detail-header.enum";

@Component({
    selector: "aa-comments-modal",
    templateUrl: "aa-comments-modal.component.html",
})

export class AACommentsModalComponent implements OnInit {

    currentComment: IQuestionCommentCreateRequest;
    isAdding = false;

    constructor(
        @Inject(StoreToken) private readonly store: IStore,
        private aaCollaborationPaneCountsService: AaCollaborationPaneCountsService,
        private translatePipe: TranslatePipe,
        private modalService: ModalService,
        private AAComment: AACommentApiService,
        private notificationService: NotificationService,
    ) { }

    closeModal = (): void => {
        this.modalService.closeModal();
        this.modalService.clearModalData();
    }

    ngOnInit(): void {
        this.currentComment = getModalData(this.store.getState()) as IQuestionCommentCreateRequest;
    }

    saveComments(): void {
        this.isAdding = true;
        if (this.currentComment.comment.length) {
            this.AAComment.addcomment(this.currentComment).then((response: IProtocolResponse<string>): boolean => {
                if (response.result) {
                    this.notificationService.alertSuccess(
                        this.translatePipe.transform("Success"),
                        this.translatePipe.transform("CommentSuccessMessage"),
                    );
                    this.modalService.handleModalCallback(1);
                    this.aaCollaborationPaneCountsService.getCollaborationPaneCounts(this.currentComment.assessmentId, AACollaborationCountsTypes.Comments);
                    this.isAdding = false;
                    this.closeModal();
                }
                return response.result;
            });
        }
    }

    handleInputChange(commentText: string): void {
        this.currentComment.comment = commentText;
    }
}
