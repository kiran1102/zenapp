import {
    Component,
    Inject,
    Input,
    OnInit,
    EventEmitter,
    Output,
    OnDestroy,
} from "@angular/core";
import { OtModalService, ModalSize } from "@onetrust/vitreus";

// 3rd Party
import { Subject } from "rxjs";
import {
    distinctUntilChanged,
    debounceTime,
    takeUntil,
} from "rxjs/operators";

// Redux
import { StoreToken } from "tokens/redux-store.token";

// Interfaces
import { IQuestionModel } from "interfaces/assessment/question-model.interface";
import { IStore } from "interfaces/redux.interface";
import { IIncidentJurisdiction } from "incidentModule/shared/interfaces/incident-jurisdiction.interface";

// Components
import { IncidentJurisdictionModalComponent } from "incidentModule/incident-detail/incident-jurisdiction/components/incident-jurisdiction-modal/incident-jurisdiction-modal.component";

@Component({
    selector: "aa-jurisdiction",
    template: `
        <div
            otFormElement
            [label]="label"
            [required]="isRequired"
        >
            <div class="row-space-between-center">
                <span class="text-medium text-bold">{{ 'Jurisdictions' | otTranslate }}</span>
                <button otButton neutral
                    [attr.ot-auto-id]="otAutoId"
                    [disabled]="isDisabled"
                    (click)="manageJurisdiction()"
                >
                {{ 'ManageJurisdiction' | otTranslate }}
                </button>
            </div>
            <hr />
            <div *ngIf="displayJurisdictionKeys?.length">
                <div
                    *ngFor="let region of displayJurisdictionKeys"
                    class="padding-all-2 margin-top-bottom-2 border border-radius background-grey"
                    >
                    <div class="text-bold margin-bottom-1-half">{{ region }}</div>
                    <div>
                        <ot-pill *ngFor="let child of displayJurisdictionValues[region]"
                            class="margin-bottom-half margin-right-half"
                            [text]="child"
                            [hideDelete]="true"
                            >
                        </ot-pill>
                    </div>
                </div>
            </div>
        </div>
    `,
})
export class AAJurisdictionComponent implements OnInit, OnDestroy {
    @Input() isDisabled: boolean;
    @Input() isJustification: boolean;
    @Input() isRequired: boolean;
    @Input() label: string;
    @Input() question: IQuestionModel;
    @Input() questionIndex: string;
    @Input() subQuestionId: string;

    @Output() jurisdictionUpdated = new EventEmitter();

    debouncer$: Subject<IIncidentJurisdiction[]> = new Subject<IIncidentJurisdiction[]>();
    otAutoId: string;
    responseId = "";

    displayJurisdictionKeys = [];
    displayJurisdictionValues = {};
    selectedJurisdictions: IIncidentJurisdiction[] = [];

    private destroy$ = new Subject();

    constructor(
        @Inject(StoreToken) private store: IStore,
        private otModalService: OtModalService,
    ) {}

    ngOnInit(): void {
        const responses = this.question.subQuestions.byId[this.subQuestionId].responses.byId;
        Object.keys(responses).map((responseKey: any) => {
            const value = responses[responseKey];
            const jurisdiction = JSON.parse(value.response);
            this.selectedJurisdictions.push(jurisdiction);
        });
        this.setDisplayJurisdictions(this.selectedJurisdictions);

        this.debouncer$.pipe(
            debounceTime(750),
            distinctUntilChanged(),
            takeUntil(this.destroy$),
        ).subscribe((jurisdictions: IIncidentJurisdiction[]) => {
            this.setDisplayJurisdictions(jurisdictions);
            if (this.jurisdictionUpdated) {
                this.jurisdictionUpdated.emit(jurisdictions);
            }
        });
        this.otAutoId = this.questionIndex
            ? `AssessmentQuestionDetail_${this.questionIndex}_Textbox`
            : `AssessmentQuestionDetail_Textbox`;
    }

    ngOnDestroy(): void {
        this.destroy$.next();
        this.destroy$.complete();
    }

    manageJurisdiction() {
        this.otModalService
            .create(
                IncidentJurisdictionModalComponent,
                {
                    isComponent: true,
                    size: ModalSize.MEDIUM,
                },
                { saveJurisdictionCB: this.submitJurisdictions.bind(this), currentJurisdictions: this.selectedJurisdictions },
            ).pipe(
                takeUntil(this.destroy$),
            );
    }

    private setDisplayJurisdictions(jurisdictions: IIncidentJurisdiction[]) {
        this.displayJurisdictionKeys = [];
        this.displayJurisdictionValues = {};
        jurisdictions.map((jurisdiction: IIncidentJurisdiction) => {
            const name = jurisdiction.stateProvinceName ? `${jurisdiction.countryCode} - ${jurisdiction.stateProvinceName}` : jurisdiction.countryName;
            const regionName = jurisdiction.regionName;
            if (this.displayJurisdictionKeys.some((key) => key === regionName)) {
                this.displayJurisdictionValues[regionName].push(name);
            } else {
                this.displayJurisdictionKeys.push(regionName);
                this.displayJurisdictionValues[regionName] = [name];
            }
        });
    }

    private submitJurisdictions(jurisdictions: IIncidentJurisdiction[]): Promise<boolean> {
        return new Promise((resolve) => {
            this.selectedJurisdictions = jurisdictions;
            this.debouncer$.next(jurisdictions);
            resolve(true);
        });
    }
}
