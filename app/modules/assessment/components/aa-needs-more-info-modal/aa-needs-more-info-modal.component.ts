// Core
import {
    OnInit,
    Component,
    Inject,
} from "@angular/core";
import { trim } from "lodash";

// Interfaces
import { IStringMap } from "interfaces/generic.interface";
import { IStore } from "interfaces/redux.interface";

// Constant
import { Regex } from "constants/regex.constant";

// Redux
import { getModalData } from "oneRedux/reducers/modal.reducer";
import { StoreToken } from "tokens/redux-store.token";
import { getQuestionModels } from "modules/assessment/reducers/assessment-detail.reducer";

// Services
import { ModalService } from "sharedServices/modal.service";
import { AANeedsMoreInfoActionService } from "modules/assessment/services/action/aa-needs-more-info-action.service";
import { AaCollaborationPaneCountsService } from "modules/assessment/services/messaging/aa-section-detail-header-helper.service";

// Enum
import { AACollaborationCountsTypes } from "modules/assessment/enums/aa-detail-header.enum";

@Component({
    selector: "aa-needs-more-info-modal",
    templateUrl: "aa-needs-more-info-modal.component.html",
})

export class AANeedsMoreInfoModalComponent implements OnInit {

    public translations: IStringMap<string>;
    public isSaving: boolean;
    public isSendButtonDisabled = true;
    public isLoading = false;
    public requestText: string;
    public questionEditEnabled = false;
    public showAllowQuestionEditingToggle = false;
    public showQuestionEditMessage = false;

    constructor(
        @Inject(StoreToken) private store: IStore,
        private aaNeedsMoreInfoActionService: AANeedsMoreInfoActionService,
        private aaCollaborationPaneCountsService: AaCollaborationPaneCountsService,
        private modalService: ModalService,
    ) { }

    ngOnInit(): void {
        this.showQuestionEditMessage = true;
        this.showAllowQuestionEditingToggle = getQuestionModels(this.store.getState())[getModalData(this.store.getState()).modalData.questionId].canReopenWithAllowEditOption;
    }

    public handleInputChange(richTextContent: string): void {
        this.isSendButtonDisabled = !(trim(richTextContent.replace(Regex.REMOVE_HTML_TAGS, "")));
        this.requestText = richTextContent;
    }

    public sendRequest(): void {
        this.isSendButtonDisabled = this.isLoading = true;
        const needsMoreInfoCreationData = getModalData(this.store.getState());
        this.aaNeedsMoreInfoActionService.addNeedsMoreInfoRequest(
            needsMoreInfoCreationData.modalData.assessmentId,
            needsMoreInfoCreationData.modalData.sectionId,
            needsMoreInfoCreationData.modalData.questionId,
            this.requestText,
            this.questionEditEnabled).then((response: boolean): void => {
                if (response) {
                    this.aaNeedsMoreInfoActionService.getAllAssessmentRequestDetails(needsMoreInfoCreationData.modalData.assessmentId);
                    this.aaCollaborationPaneCountsService.getCollaborationPaneCounts(needsMoreInfoCreationData.modalData.assessmentId, AACollaborationCountsTypes.Nmi);
                    this.closeModal();
                }
                this.isSendButtonDisabled = this.isLoading = false;
                this.resetQuestionEditOptions();
            });
    }

    public onToggle(questionEditEnabled: boolean) {
        this.questionEditEnabled = questionEditEnabled;
    }

    public closeModal() {
        this.resetQuestionEditOptions();
        this.modalService.closeModal();
        this.modalService.clearModalData();
    }

    public resetQuestionEditOptions() {
        this.showQuestionEditMessage = false;
        this.showAllowQuestionEditingToggle = false;
        this.questionEditEnabled = false;
    }
}
