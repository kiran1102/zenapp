import {
    OnInit,
    OnDestroy,
    Inject,
    Component,
    Input,
    SimpleChanges,
} from "@angular/core";
import { StateService } from "@uirouter/core";

// 3rd Party
import {
    uniqBy,
    cloneDeep,
} from "lodash";
import { Subject } from "rxjs";
import { StoreToken } from "tokens/redux-store.token";

// Interfaces
import { IStore } from "interfaces/redux.interface";
import {
    IAssessmentCreationDetails,
} from "interfaces/assessment/assessment-creation-details.interface";
import { ICustomTemplateDetails } from "modules/template/interfaces/custom-templates.interface";

// Reducers
import { getBulkAssessmentDetailsList } from "modules/assessment/reducers/assessment-detail.reducer";

// Enum/Constants
import { OrgUserDropdownTypes } from "enums/org-user-dropdown.enum";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

// Services
import { AssessmentBulkLaunchActionService } from "sharedModules/services/actions/bulk-launch-assessment-action.service";
import { IOrgUserAdapted } from "interfaces/org-user.interface";
import { TimeStamp } from "oneServices/time-stamp.service";
import { AssessmentBulkLaunchApiService } from "sharedModules/services/api/bulk-launch-assessment-api.service";

@Component({
    selector: "assessment-bulk-launch-controls",
    templateUrl: "./assessment-bulk-launch-controls.component.html",
})

export class AssessmentBulkLaunchControlsComponent implements OnInit, OnDestroy {

    @Input() templateOptions: ICustomTemplateDetails[];
    @Input() isTemplateReady = false;
    @Input() approverUserList: IOrgUserAdapted[];
    @Input() respondentUserList: IOrgUserAdapted[];
    @Input() orgIdList: string[];

    public minDate: Date = new Date();
    public controlsOrgId: string;
    public isApproverDisabled = false;
    public isRespondentDisabled = false;
    public approverDisabledTooltip: string;
    public respondentDisabledTooltip: string;
    public dateModel: any;
    public approverDropdownType: OrgUserDropdownTypes = OrgUserDropdownTypes.ApproverPia;
    public respondentDropdownType: OrgUserDropdownTypes = OrgUserDropdownTypes.RespondentPia;
    public bulkAssessmentDetails: IAssessmentCreationDetails[];
    public bulkTemplateName: string;
    public bulkRespondentInvitedUser: string;
    public selectedTemplate: ICustomTemplateDetails;
    public validReminder = true;
    public loadUserOptionsOnClick: boolean;
    public loadExternalUserOptions: boolean;

    public bulkDeadline: string;
    public bulkReminder: number;
    public disableUntilDate = this.timeStamp.getDisableUntilDate();
    public templateAccordianOpen: boolean;
    public approversList: IOrgUserAdapted[] = [];
    public respondentsList: IOrgUserAdapted[] = [];
    public format = this.timeStamp.getDatePickerDateFormat();
    public disabledApproverTooltipMessage: string;
    public disabledRespondentTooltipMessage: string;

    private destroy$ = new Subject();
    private bulkTemplateId: string;
    private bulkApproverId: string;
    private bulkApproverName: string;
    private selectedRespondentsList: IOrgUserAdapted[] = [];
    private selectedApproversList: IOrgUserAdapted[] = [];
    private respondentChangedOnce = false;

    constructor(
        @Inject(StoreToken) private store: IStore,
        private translatePipe: TranslatePipe,
        private assessmentBulkLaunchActionService: AssessmentBulkLaunchActionService,
        private assessmentBulkLaunchApiService: AssessmentBulkLaunchApiService,
        private timeStamp: TimeStamp,
    ) {}

    ngOnInit(): void {
        this.initializeBulkControls();
    }

    ngOnChanges(changes: SimpleChanges): void {
        this.templateOptions = changes.templateOptions ? changes.templateOptions.currentValue : this.templateOptions;
        this.isTemplateReady = changes.isTemplateReady ? changes.isTemplateReady.currentValue === true : this.isTemplateReady;
        this.initializeBulkControls();
    }

    ngOnDestroy(): void {
        this.destroy$.next();
        this.destroy$.complete();
    }

    onApplyToAll(): void {
        this.bulkAssessmentDetails = getBulkAssessmentDetailsList(this.store.getState());

        if (this.selectedTemplate) {
            const inventoryIds = this.bulkAssessmentDetails.map((bulkDetails: IAssessmentCreationDetails): string => {
                return bulkDetails.inventoryDetails.inventoryId;
            });

            const params = {
                templateRootVersionId: this.selectedTemplate.rootVersionId,
                inventoryIds,
            };

            this.assessmentBulkLaunchApiService.getDuplicateAssessmentWarnings(params)
                .subscribe((res) => {
                    if (res.data.length > 0) {
                        res.data.forEach((inventoryId: string) => {
                            this.bulkAssessmentDetails.find((bulkDetails: IAssessmentCreationDetails): any => {
                                if (bulkDetails.inventoryDetails.inventoryId === inventoryId) {
                                    bulkDetails.showDuplicateWarning = true;
                                }
                            });
                        });
                    } else {
                        this.bulkAssessmentDetails.forEach((bulkDetail: IAssessmentCreationDetails): void => {
                            bulkDetail.showDuplicateWarning = false;
                        });
                    }
                });
        }
        const uniqueOrgIds = uniqBy(this.bulkAssessmentDetails, "orgGroupId").map((item: IAssessmentCreationDetails) => item.orgGroupId);
        this.bulkAssessmentDetails.forEach((assessmentDetails: IAssessmentCreationDetails) => {
            // Apply all approver & respondents only if the inventory org is the same.
            if (uniqueOrgIds && uniqueOrgIds.length === 1) {
                assessmentDetails.approverId = this.bulkApproverId || "";
                assessmentDetails.approverName = this.bulkApproverName || "";
                assessmentDetails.approversOrgUserList = cloneDeep(this.selectedApproversList);
                if (this.selectedRespondentsList && this.selectedRespondentsList.length) {
                    if (this.selectedRespondentsList.some(Boolean) || this.respondentChangedOnce) {
                        assessmentDetails.respondentsOrgUserList = cloneDeep(this.selectedRespondentsList);
                        this.respondentChangedOnce = true;
                    }
                }
                assessmentDetails.respondentModel = "";
            }
            assessmentDetails.templateId = this.bulkTemplateId || "";
            assessmentDetails.templateName = this.bulkTemplateName || "";
            assessmentDetails.deadline = this.bulkDeadline || "";
            assessmentDetails.reminder = this.bulkReminder && this.validReminder ? this.bulkReminder : null;
            assessmentDetails.respondentInvitedUser = this.bulkRespondentInvitedUser || "";
            assessmentDetails.isValidReminder = true;
        });
        this.assessmentBulkLaunchActionService.setBulkLaunchAssessments(this.bulkAssessmentDetails);
    }

    templateSelected(selectedTemplate: any): void {
        this.selectedTemplate = selectedTemplate;
        if (this.selectedTemplate) {
            this.bulkTemplateId = selectedTemplate.id;
            this.bulkTemplateName = selectedTemplate.name;
        } else {
            this.bulkTemplateId = this.bulkTemplateName = "";
        }
    }

    deadlineSelected(selectedDeadline: any): void {
        if (!selectedDeadline.dateTimeUtc) {
            this.bulkReminder = selectedDeadline.dateTimeUtc;
            this.validReminder = true;

        }
        this.bulkDeadline = (selectedDeadline)
            ? selectedDeadline.dateTime
            : "";
        this.reminderDaysSelected(this.bulkReminder, selectedDeadline.dateTimeUtc);
    }

    approverSelected(selectedApprover: any): void {
        if (selectedApprover && selectedApprover.isValid) {
            this.bulkApproverId = selectedApprover.selection.Id;
            this.bulkApproverName = selectedApprover.selection.FullName;
        }
    }

    respondentsSelected = (selectedRespondents: IOrgUserAdapted[]) => {
        this.selectedRespondentsList = selectedRespondents || null;
    }

    approversSelected = (selectedApprovers: IOrgUserAdapted[]) => {
        this.selectedApproversList = selectedApprovers || null;
    }

    reminderDaysSelected(selectedReminder: number, deadline: string): void {
        const today = this.timeStamp.currentDate.format();
        const reminderDate = this.timeStamp.getReminderDate(selectedReminder, deadline);

        this.validReminder = selectedReminder > 0 && reminderDate > today;
        this.bulkReminder = selectedReminder;

        if ((!selectedReminder && selectedReminder !== 0) || this.validReminder) {
            this.validReminder = true;
        } else {
            this.validReminder = false;
        }
    }

    private initializeBulkControls(): void {
        // Load approvers & respondents based on selected inventory orgId
        this.bulkAssessmentDetails = getBulkAssessmentDetailsList(this.store.getState());
        if (this.orgIdList.length > 0 || (this.approverUserList || this.respondentUserList)) {
            this.loadUserOptionsOnClick = false;
            this.loadExternalUserOptions = true;
        } else {
            this.loadUserOptionsOnClick = true;
            this.loadExternalUserOptions = false;
        }
        const uniqueOrgIds = uniqBy(this.bulkAssessmentDetails, "orgGroupId").map((item: IAssessmentCreationDetails) => item.orgGroupId);
        if (uniqueOrgIds && uniqueOrgIds.length === 1) {
            this.controlsOrgId = uniqueOrgIds[0];
            this.approverDisabledTooltip = this.respondentDisabledTooltip = "";
        } else {
            this.isApproverDisabled = this.isRespondentDisabled = true;
            this.controlsOrgId = "";
            this.approverDisabledTooltip = this.translatePipe.transform("BulkApproverUnavailable");
            this.respondentDisabledTooltip = this.translatePipe.transform("BulkRespondentUnavailable");
        }
        if (this.isApproverDisabled) {
            this.disabledApproverTooltipMessage = this.translatePipe.transform("BulkControlApproverDisabled");
        }
        if (this.isRespondentDisabled) {
            this.disabledRespondentTooltipMessage = this.translatePipe.transform("BulkControlRespondentDisabled");
        }
        this.templateAccordianOpen = true;
    }
}
