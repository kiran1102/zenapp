// Angular
import {
    Component,
    Inject,
    OnInit,
} from "@angular/core";
import { StateService } from "@uirouter/core";

// Lodash
import {
    map,
    forEach,
} from "lodash";

// Services
import { AssessmentDetailApiService } from "modules/assessment/services/api/assessment-detail-api.service";

// Interfaces
import {
    IAssessmentModel,
    IAssessmentCopyPayload,
    IAssessmentApprover,
    IAsessmentCopyContent,
} from "interfaces/assessment/assessment-model.interface";
import { IOrgUserAdapted } from "interfaces/org-user.interface";
import {
    IAssessmentApproverDetails,
    IAssessmentRespondentDetails,
} from "interfaces/assessment/assessment-creation-details.interface";
import { INameId } from "interfaces/generic.interface";

@Component({
    selector: "copy-assessment",
    templateUrl: "./aa-copy-assessment.component.html",
})

export class AACopyAssessmentComponent implements OnInit {

    assessments: IAssessmentModel[] = [];
    assessmentsPayload: IAssessmentCopyPayload[] = [];
    canSaveAssessment = true;
    isLoading = true;
    saveInProgress = false;
    copyContent: IAsessmentCopyContent = {
        copyResponses: true,
        copyComments: true,
        copyAttachments: false,
        copyNotes: false,
    };

    bulkCopyContent: IAsessmentCopyContent = {
        copyResponses: true,
        copyComments: true,
        copyAttachments: false,
        copyNotes: false,
    };

    constructor(
        private stateService: StateService,
        private assessmentDetailService: AssessmentDetailApiService,
    ) { }

    ngOnInit(): void {
        this.assessmentDetailService.fetchAssessmentWelcomeSection(this.stateService.params.assessmentId)
            .then((assessment: IAssessmentModel) => {
                this.assessments.push(assessment);
                this.isLoading = false;
            });
    }

    enableSaveButton(isAssessmentValid: boolean): void {
        this.canSaveAssessment = isAssessmentValid;
    }

    saveAssessments(): void {
        if (!this.canSaveAssessment) return;
        this.saveInProgress = true;
        this.assessmentDetailService.saveAssessmentCopy({ ...this.assessmentsPayload[0], ...this.copyContent })
            .then(() => {
                this.goToListingPage();
            });
    }

    goToListingPage(): void {
        this.stateService.go("zen.app.pia.module.assessment.list");
    }

    constructAssessmentCopyModel(payload: IAssessmentCopyPayload) {
        this.assessmentsPayload = [];
        this.assessmentsPayload.push({
            ...payload,
            approvers: this.constructApproverPayload(payload.approvers as IOrgUserAdapted[]),
            respondents: this.constructRespondentPayload(payload.respondents as IOrgUserAdapted[], payload.comment),
        });
    }

    applyBulkChanges(modelToApply: IAssessmentCopyPayload) {
        this.assessmentsPayload = map(this.assessmentsPayload, (assessment): IAssessmentCopyPayload => {
            forEach(modelToApply, (value, key) => {
                assessment[key] = value;
            });
            assessment.approvers = this.constructApproverPayload(modelToApply.approvers as IOrgUserAdapted[]);
            assessment.respondents = this.constructRespondentPayload(modelToApply.respondents as IOrgUserAdapted[], null);
            return { ...assessment };
        });

        this.copyContent.copyComments = this.bulkCopyContent.copyComments;
        this.copyContent.copyResponses = this.bulkCopyContent.copyResponses;

        this.assessments = map(this.assessments, (assessment): IAssessmentModel => {
            assessment.name = this.assessmentsPayload[0].name;
            assessment.orgGroup.id = modelToApply.orgGroupId;
            assessment.orgGroup.name = modelToApply.orgGroupName;
            assessment.approvers = this.constructOrgUsersModel(modelToApply.approvers as IOrgUserAdapted[]);
            assessment.respondents = this.constructOrgUsersModel(modelToApply.respondents as IOrgUserAdapted[]);
            assessment.deadline = modelToApply.deadline;
            assessment.reminder = modelToApply.reminder;
            assessment.latestTemplateVersion = modelToApply.latestTemplateVersion;
            return { ...assessment };
        });
    }

    private constructApproverPayload(approvers: IOrgUserAdapted[]): IAssessmentApproverDetails[] {
        return map(approvers, (approver: IOrgUserAdapted): IAssessmentApproverDetails => {
            return {
                approverId: approver.Id,
                approverName: approver.FullName,
            };
        });
    }

    private constructRespondentPayload(respondents: IOrgUserAdapted[], comment: string): IAssessmentRespondentDetails[] {
        return map(respondents, (respondent: IOrgUserAdapted): IAssessmentRespondentDetails => {
            return {
                respondentId: respondent.Id === respondent.FullName ? null : respondent.Id,
                respondentName: respondent.FullName,
                comment,
            };
        });
    }

    private constructOrgUsersModel(users: IOrgUserAdapted[]): Array<INameId<string>> {
        return map(users, (user: IOrgUserAdapted): IAssessmentApprover => {
            return {
                id: user.Id,
                name: user.FullName,
            };
        });
    }
}
