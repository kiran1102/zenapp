// Angular
import {
    Component,
    Input,
    Inject,
    Output,
    EventEmitter,
    OnChanges,
    SimpleChanges,
    OnInit,
} from "@angular/core";

// Lodash
import { map } from "lodash";

// Services
import { TimeStamp } from "oneServices/time-stamp.service";

// Redux
import { getOrgById } from "oneRedux/reducers/org.reducer";
import { StoreToken } from "tokens/redux-store.token";
import { getCurrentUser } from "oneRedux/reducers/user.reducer";

// Interfaces
import { IOrgUserAdapted } from "interfaces/org-user.interface";
import { IStore } from "interfaces/redux.interface";
import { IMyDate } from "mydaterangepicker";
import {
    IAssessmentApprover,
    IAssessmentCopyPayload,
    IAssessmentModel,
    IAsessmentCopyContent,
} from "interfaces/assessment/assessment-model.interface";
import { INameId } from "interfaces/generic.interface";
import { IOrganization } from "interfaces/org.interface";
import { IUser } from "interfaces/user.interface";

// Enums
import { AssessmentCreationActions } from "constants/assessment-creation-actions.constant";
@Component({
    selector: "copy-assessment-card",
    templateUrl: "./aa-copy-assessment-card.component.html",
})

export class AACopyAssessmentCard implements OnInit, OnChanges {

    @Input() assessment: IAssessmentModel;
    @Input() copyContent: IAsessmentCopyContent;

    @Output() enableSaveButton: EventEmitter<boolean> = new EventEmitter<boolean>();
    @Output() onChange: EventEmitter<IAssessmentCopyPayload> = new EventEmitter<IAssessmentCopyPayload>();

    selectedOrgId: string;
    approverList: IOrgUserAdapted[] = [];
    respondentList: IOrgUserAdapted[] = [];
    assessmentActions = AssessmentCreationActions;
    deadline: string = null;
    disableUntil: IMyDate = this.timeStamp.getDisableUntilDate();
    contentCheckboxData: Array<{ label: string, model: string }> = [
        {
            label: "Responses",
            model: "copyResponses",
        },
        {
            label: "Comment",
            model: "copyComments",
        },
        // {
        //     label: "Attachments",
        //     checked: copyAttachments,
        // },
        // {
        //     label: "Notes",
        //     checked: copyNotes,
        // }, TODO: Uncomment this lines once backend is ready
    ];
    assessmentCopy: IAssessmentCopyPayload;
    currentOrg = {} as IOrganization;
    currentUser = {} as IUser;
    dateFormat = this.timeStamp.getDatePickerDateFormat();

    constructor(
        @Inject(StoreToken) private store: IStore,
        private timeStamp: TimeStamp,
    ) { }

    ngOnInit(): void {
        this.currentUser = getCurrentUser(this.store.getState());
        this.currentOrg.Id = this.currentUser.OrgGroupId;
        this.currentOrg.Name = this.currentUser.OrgGroupName;
    }

    ngOnChanges(changes: SimpleChanges): void {
        this.getAssessmentCopyModel();
        this.setAssessmentCard();
    }

    handleAction = (action: string, payload?: any): void => {
        switch (action) {
            case AssessmentCreationActions.NAME_CHANGED:
                this.assessmentCopy.name = payload;
                break;
            case AssessmentCreationActions.COMMENT_CHANGED:
                this.assessmentCopy.comment = payload;
                break;
            case AssessmentCreationActions.DEADLINE_CHANGED:
                this.deadlineSelected(payload);
                break;
            case AssessmentCreationActions.DESCRIPTION_CHANGED:
                this.assessmentCopy.description = payload;
                break;
            case AssessmentCreationActions.ORG_SELECTED:
                this.orgSelected(payload);
                break;
            case AssessmentCreationActions.SELECT_TEMPLATE_VERSION:
                this.assessmentCopy.latestTemplateVersion = payload;
                break;
            case AssessmentCreationActions.EXPLICIT_CHANGE_REQUIRED:
                this.assessmentCopy.reviewChangedResponses = payload;
                break;
        }

        this.isAssessmentCopyValid();
    }

    selectedApprovers(approverList: IOrgUserAdapted[]): void {
        this.assessmentCopy.approvers = approverList;
        this.updatePayload();
    }

    selectedRespondents(respondentList: IOrgUserAdapted[]): void {
        this.assessmentCopy.respondents = respondentList;
        this.isAssessmentCopyValid();
    }

    reminderDaysSelected(selectedReminder?: number): void {
        if (!selectedReminder) {
            selectedReminder = this.assessmentCopy.reminder;
            return;
        }
        const today = this.timeStamp.currentDate.format();
        const reminderDate = this.timeStamp.getReminderDate(selectedReminder, this.deadline);

        this.assessment.isReminderValid = (selectedReminder >= 0 && reminderDate > today);
        this.assessmentCopy.reminder = selectedReminder;
        this.isAssessmentCopyValid();
    }

    copyParameters(attribute: string) {
        this.copyContent[attribute] = !this.copyContent[attribute];
    }

    private setAssessmentCard(): void {
        this.disableUntil = this.timeStamp.getDisableUntilDate();
        this.assessment.isReminderValid = true;
        this.deadline = this.assessmentCopy.deadline ? this.timeStamp.formatDateWithoutTime(this.assessmentCopy.deadline) : null;
        this.approverList = [...this.assessmentCopy.approvers  as IOrgUserAdapted[]];
        this.respondentList = [...this.assessmentCopy.respondents as IOrgUserAdapted[]];
        this.updatePayload();
        this.isAssessmentCopyValid();
    }

    private getAssessmentCopyModel() {
        this.assessmentCopy = {
            name: this.assessment.name,
            description: this.assessment.description,
            orgGroupId: this.assessment.orgGroup.id,
            orgGroupName: this.assessment.orgGroup.name,
            deadline: this.assessment.deadline,
            reminder: this.assessment.reminder,
            approvers: this.getUserMultiSelectData(this.assessment.approvers),
            reviewChangedResponses: true,
            respondents: this.getUserMultiSelectData(this.assessment.respondents),
            sourceAssessmentId: this.assessment.assessmentId,
            latestTemplateVersion: this.assessment.latestTemplateVersion ? this.assessment.latestTemplateVersion : false,
        };
    }

    private orgSelected(orgId: string): void {
        if (orgId) {
            this.assessmentCopy.orgGroupId = orgId;
            this.assessmentCopy.orgGroupName = getOrgById(orgId)(this.store.getState()).Name;
        } else {
            this.assessmentCopy.orgGroupId = this.currentOrg.Id;
            this.assessmentCopy.orgGroupName = this.currentOrg.Name;
        }
        this.assessmentCopy.approvers = [];
        this.approverList = [];
        this.assessmentCopy.respondents = [];
        this.respondentList = [];
    }

    private deadlineSelected(deadline: any): void {
        this.deadline = deadline.date;
        this.assessmentCopy.deadline = deadline.jsdate ? deadline.jsdate.toISOString() : null;
        this.assessment.isReminderValid = true;
        this.reminderDaysSelected(this.assessmentCopy.reminder);
    }

    private getUserMultiSelectData(users: IAssessmentApprover[] | Array<INameId<string>>): IOrgUserAdapted[] {
        return map(users, (user) => {
            return {
                Id: user["id"],
                FullName: user["name"],
            };
        });
    }

    private updatePayload() {
        this.onChange.emit(this.assessmentCopy);
    }

    private isAssessmentCopyValid() {
        let isAssessmentValid = true;
        if (!this.assessmentCopy.name.length || !this.assessmentCopy.respondents.length ||
            !this.assessmentCopy.orgGroupId || !this.assessment.isReminderValid) {
            isAssessmentValid = false;
        }
        if (isAssessmentValid) {
            this.updatePayload();
        }
        this.enableSaveButton.emit(isAssessmentValid);
    }
}
