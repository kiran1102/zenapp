// Angular
import {
    Component,
    Inject,
    OnInit,
} from "@angular/core";
import { StateService } from "@uirouter/core";

// Rxjs
import { Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";

// Enum
import { SendBackType } from "enums/assessment/assessment-status.enum";

// Interfaces
import { IProtocolResponse } from "interfaces/protocol.interface";
import { IStore } from "interfaces/redux.interface";
import { IStringMap } from "interfaces/generic.interface";
import { IOtModalContent } from "@onetrust/vitreus";
import { ISspTemplatesRestrictView } from "interfaces/self-service-portal/ssp.interface";

// Redux
import {
    getAssessmentDetailApprovers,
    getAssessmentDetailSelectedSectionId,
    getAssessmentDetailSelectedSectionRespondents,
    getAssessmentDetailModel,
} from "modules/assessment/reducers/assessment-detail.reducer";
import { StoreToken } from "tokens/redux-store.token";

// Service
import { AATemplateApiService } from "modules/assessment/services/api/aa-template-api.service";
import { SettingsAAActionService } from "settingsServices/actions/settings-assessment-action.service";

// Pipe
import { TranslatePipe } from "modules/pipes/translate.pipe";

@Component({
    selector: "aa-send-back-modal",
    templateUrl: "./aa-send-back-modal.component.html",
})

export class AASendBackModalComponent implements OnInit, IOtModalContent {

    assessmentId: string;
    sendBackEmailComment: string;
    isSendingBackAssessment = false;
    modalTitle: string;
    respondentName: string;
    translations: IStringMap<string>;
    sendBackModalDesc: string;
    otModalCloseEvent: Subject<boolean>;
    otModalData: {
        sendBackType: string;
        title: string;
        approverSendBackFlow,
        openInfoRequestMsg,
    } | null;
    sendBackModalSettings = {
        sendBulkInfoRequestEmail: true,
        editAllResponses: true,
    };
    sendBackTypes = SendBackType;

    sendBackType: string;
    bulkSendEmailSettings: boolean;
    loading = true;

    private destroy$ = new Subject();

    constructor(
        private stateService: StateService,
        @Inject(StoreToken) private store: IStore,
        private readonly AssessmentTemplateAPI: AATemplateApiService,
        private readonly translatePipe: TranslatePipe,
        private settingsAssessmentAction: SettingsAAActionService,
    ) { }

    ngOnInit(): void {
        this.settingsAssessmentAction.getAssessmentSettings()
            .pipe(takeUntil(this.destroy$))
            .subscribe((response: IProtocolResponse<ISspTemplatesRestrictView>) => {
                if (response.result) {
                    this.bulkSendEmailSettings = response.data.infoRequestBulkEmailAllowed;
                    this.sendBackModalSettings.sendBulkInfoRequestEmail = this.bulkSendEmailSettings ? this.otModalData.approverSendBackFlow : false;
                }
                this.loading = false;
            });

        const state = this.store.getState();
        const approvers = getAssessmentDetailApprovers(state);
        const respondentName = getAssessmentDetailSelectedSectionRespondents(getAssessmentDetailSelectedSectionId(state))(state)
            .map((respondent) => respondent.name).join(", ");
        const { assessmentId } = getAssessmentDetailModel(state);

        [this.assessmentId, this.respondentName, this.sendBackType] = [assessmentId, respondentName, this.otModalData.sendBackType];
        if (this.sendBackType === SendBackType.SendBackToInProgress) {
            this.sendBackModalDesc = this.translatePipe.transform("AssessmentResetToInProgress", { RespondentName: this.respondentName });
        } else {
            this.sendBackModalDesc = (approvers && approvers.length > 1)
                ? this.translatePipe.transform("AssessmentResetToUnderReviewForMultipleApprovers")
                : this.translatePipe.transform("AssessmentResetToUnderReview");
        }
    }

    ngOnDestroy(): void {
        this.destroy$.next();
        this.destroy$.complete();
    }

    sendBackAssessment(): void {
        this.isSendingBackAssessment = true;
        this.AssessmentTemplateAPI.sendBackAssessment(this.assessmentId, this.sendBackEmailComment, this.sendBackType, this.sendBackModalSettings.editAllResponses, this.sendBackModalSettings.sendBulkInfoRequestEmail)
            .then((response: IProtocolResponse<void>): void => {
                if (response.result) {
                    this.closeSendBackModal();
                    this.stateService.go("zen.app.pia.module.assessment.list");
                }
                this.isSendingBackAssessment = false;
            });
    }

    setSendBackEmailComment(sendBackEmailComment: string): void {
        this.sendBackEmailComment = sendBackEmailComment;
    }

    toggleSendInfoRequests(sendInfoRequests: boolean): void {
        this.sendBackModalSettings.sendBulkInfoRequestEmail = sendInfoRequests;
    }

    toggleEditAllResponses(editAllResponses: boolean): void {
        this.sendBackModalSettings.editAllResponses = editAllResponses;
    }

    closeSendBackModal(): void {
        this.otModalCloseEvent.next(false);
    }
}
