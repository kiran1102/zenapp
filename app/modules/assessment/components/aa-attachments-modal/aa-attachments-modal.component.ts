// angular
import {
    Component,
    Inject,
    OnInit,
} from "@angular/core";

// 3rd party
import {
    forEach,
    some,
} from "lodash";

// interfaces
import { IStore } from "interfaces/redux.interface";
import {
    IAttachmentFile,
    IAttachmentFileType,
} from "interfaces/attachment-file.interface";
import {
    IAAAttachmentDetails,
    IAAAttachmentModalData,
    IAAAttachmentModalResponse,
    IUploadFile,
} from "interfaces/assessment/assessment-attachment-modal.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";
import { ISspTemplatesRestrictView } from "interfaces/self-service-portal/ssp.interface";

// constants
import { UserRoles } from "constants/user-roles.constant";
import { AssessmentStatus } from "enums/assessment/assessment-status.enum";

// services
import { ModalService } from "sharedServices/modal.service";
import { AttachmentService } from "oneServices/attachment.service";
import { AAAttachmentApiService } from "modules/assessment/services/api/aa-attachment-api.service";
import { Permissions } from "modules/shared/services/helper/permissions.service";
import { AssessmentDetailService } from "modules/assessment/services/view-logic/assessment-detail.service";
import { AssessmentListApiService } from "modules/assessment/services/api/assessment-list-api.service";

// redux
import { getModalData } from "oneRedux/reducers/modal.reducer";
import { StoreToken } from "tokens/redux-store.token";
import { getCurrentUser } from "oneRedux/reducers/user.reducer";
import { getAssessmentDetailStatus } from "modules/assessment/reducers/assessment-detail.reducer";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

@Component({
    selector: "aa-attachments-modal",
    templateUrl: "./aa-attachments-modal.component.html",
})
export class AAAttachmentsModalComponent implements OnInit {

    attachment: any;
    assessmentData: IAAAttachmentModalData;
    attachmentList: IAAAttachmentModalResponse[] = [];
    description = "";
    attachmentErrorList: string[] = [];
    attachmentSizeLimit = 67108864; // 64MB
    isHovering: boolean;
    isLoading: boolean;
    isDisabled: boolean;
    placeholder = this.translatePipe.transform("EnterDescription");
    extensionRegex = /(?:\.([^.]+))?$/;
    acceptedFileTypes = ["csv", "doc", "docx", "jpg", "jpeg", "mpp", "msg", "pdf", "png", "ppt", "pptx", "txt", "vsd", "vsdx", "xls", "xlsx", "xlsm"];
    isReadOnly: boolean;
    isUserInvited = false;
    assessmentStatus: string;
    restrictInvitedUser = false;

    constructor(
        @Inject(StoreToken) private readonly store: IStore,
        @Inject("Export") readonly Export: any,
        private translatePipe: TranslatePipe,
        private permissions: Permissions,
        private modalService: ModalService,
        private attachmentService: AttachmentService,
        private aAAttachmentApiService: AAAttachmentApiService,
        private assessmentDetailLogic: AssessmentDetailService,
        private assessmentListApiService: AssessmentListApiService,
    ) { }

    ngOnInit(): void {
        this.isLoading = true;
        this.isDisabled = false;
        const state = this.store.getState();
        this.assessmentData = getModalData(state);

        this.isUserInvited = getCurrentUser(this.store.getState()).RoleName === UserRoles.Invited;
        this.assessmentStatus = getAssessmentDetailStatus(state);

        const promises: Array<ng.IPromise<IProtocolResponse<IAAAttachmentModalResponse[]>
            | IProtocolResponse<IAttachmentFileType[]>
            | IProtocolResponse<ISspTemplatesRestrictView>>> = [
                this.aAAttachmentApiService.getQuestionAttachments(this.assessmentData.assessmentId,
                    this.assessmentData.sectionId, this.assessmentData.questionId),
            ];

        promises.push(this.attachmentService.getAttachmentFileTypes());
        if (this.assessmentStatus === AssessmentStatus.Completed && this.isUserInvited) {
            promises.push(this.assessmentListApiService.getSspRestrictViewSetting());
        }

        Promise.all(promises)
            .then((responses): void => {

                if (responses[0]) {
                    const list = responses[0];
                    if (list.status === 423) {
                        this.isDisabled = true;
                        return;
                    }
                    forEach(list.data, (element: IAAAttachmentModalResponse): void => {
                        this.attachmentList.push(element);
                    });
                }

                if (responses[1] && responses[1].data) {
                    const fileTypes: IAttachmentFileType[] = responses[1].data as IAttachmentFileType[];
                    this.acceptedFileTypes = fileTypes.map((file: IAttachmentFileType) => file.FileTypeExtension);
                }

                if (responses[2]) {
                    this.restrictInvitedUser = responses[1].data &&
                        (responses[1].data as ISspTemplatesRestrictView).restrictInvitedUserAttachmentAction;
                }
                this.isLoading = false;
            });

        this.assessmentDetailLogic.fetchAssessmentReadOnlyState().subscribe((readonly: boolean) => {
            this.isReadOnly = readonly;
        });
    }

    handleFilesDropped(files: IUploadFile[]): void {
        if (files && files.length > 0 && this.isValidFile(files[0])) {
            this.attachment = files[0];
        }
    }

    handleFileSelected(files: IUploadFile[]): void {
        if (files && files.length > 0 && this.isValidFile(files[0])) {
            this.attachment = files[0];
        }
    }

    isValidFile(file: IUploadFile): boolean {
        this.attachmentErrorList.splice(0);
        let isValid = true;
        const fileExt = this.extensionRegex.exec(file.name)[0].replace(".", "").toLowerCase();

        if (this.acceptedFileTypes.indexOf(fileExt) < 0) {
            this.attachmentErrorList.push(this.translatePipe.transform("FileTypeExtensionNotValid"));
            isValid = false;
        }
        if (file.size > this.attachmentSizeLimit || file.size === 0) {
            this.attachmentErrorList.push(this.translatePipe.transform("FileSizeLimitViolation"));
            isValid = false;
        }
        if (file.name.length > 100) {
            this.attachmentErrorList.push(this.translatePipe.transform("FileNameExceedsLimitOf100Characters"));
            isValid = false;
        }
        return isValid;
    }

    handleFilesHovered(isHovering: boolean): void {
        this.isHovering = isHovering;
    }

    addAttachment(): void {
        this.isLoading = true;
        this.isDisabled = true;
        const fileData: IAttachmentFile = {
            Blob: this.attachment,
            FileName: this.attachment.name,
            Name: this.attachment.name,
            Type: 60,
            RequestId: this.assessmentData.questionId,
            RefIds: [this.assessmentData.assessmentId, this.assessmentData.questionId],
            Comments: this.description,
            IsInternal: true,
            Extension: this.attachment.name.substr(this.attachment.name.lastIndexOf(".") + 1), // .txt
        };
        this.attachmentService.addAttachment(fileData).then((response: IProtocolResponse<IAttachmentFile>): void => {
            if (!response || !response.result) return;

            const assessmentAttachmentDetails: IAAAttachmentDetails = {
                assessmentId: this.assessmentData.assessmentId,
                sectionId: this.assessmentData.sectionId,
                questionId: this.assessmentData.questionId,
                attachmentId: response.data.Id,
                fileName: this.attachment.name,
                fileDescription: this.description,
            };
            this.aAAttachmentApiService.addAttachmentToQuestion(assessmentAttachmentDetails)
                .then((resp: IProtocolResponse<null>): void => {
                    if (resp.result) {
                        this.cancelAttachment();
                        this.aAAttachmentApiService.getQuestionAttachments(this.assessmentData.assessmentId, this.assessmentData.sectionId, this.assessmentData.questionId).then((list: IProtocolResponse<IAAAttachmentModalResponse[]>): void => {
                            this.attachmentList = list.data;
                            this.description = "";
                        });
                    }
                }).finally(() => {
                    this.isLoading = false;
                    this.isDisabled = false;
                });
        });
    }

    deleteAttachment(attachmentId: string): void {
        this.isLoading = true;
        this.aAAttachmentApiService.deleteAttachment(this.assessmentData, attachmentId).then((response: IProtocolResponse<null>): void => {
            if (!response) return;
            this.aAAttachmentApiService.getQuestionAttachments(this.assessmentData.assessmentId, this.assessmentData.sectionId, this.assessmentData.questionId).then((list: IProtocolResponse<IAAAttachmentModalResponse[]>): void => {
                this.attachmentList = list.data;
                this.isLoading = false;
            });
        });
    }

    canUserDeleteAttachment(attachmentId: string): boolean {
        const currentUser = getCurrentUser(this.store.getState());
        const isCurrentUserAttachmentCreator = some(this.attachmentList, (attachment: IAAAttachmentModalResponse): boolean => {
            if (attachment.creator && attachment.attachmentId === attachmentId) {
                return attachment.creator.id === currentUser.Id;
            }
        });
        return this.permissions.canShow("AssessmentAttachmentDelete") ||
            isCurrentUserAttachmentCreator;
    }

    downloadAttachment(attachment: IAAAttachmentModalResponse): void {
        this.attachmentService.downloadAttachment(attachment.attachmentId).then((response) => {
            if (!response) return;
            this.Export.basicFileDownload(response.data, attachment.fileName);
        });
    }

    getDescription(value: string): void {
        this.description = value;
    }

    cancelAttachment(): void {
        this.attachment = null;
    }

    columnTrackBy(column: number): number {
        return column;
    }

    closeModal(): void {
        this.modalService.handleModalCallback(this.attachmentList.length);
        this.modalService.closeModal();
        this.modalService.clearModalData();
    }
}
