// Rxjs
import { Subject } from "rxjs";
import {
    startWith,
    takeUntil,
} from "rxjs/operators";

// Angular
import {
    OnInit,
    OnDestroy,
    Inject,
    Component,
    Input,
    SimpleChanges,
} from "@angular/core";

// 3rd Party
import { StoreToken } from "tokens/redux-store.token";
import {
    findIndex,
    remove,
    cloneDeep,
} from "lodash";

// Redux
import observableFromStore from "oneRedux/utils/observable-from-store";
import { getBulkAssessmentDetailsList } from "modules/assessment/reducers/assessment-detail.reducer";

// Services
import { AssessmentBulkLaunchActionService } from "sharedModules/services/actions/bulk-launch-assessment-action.service";
import { AssessmentBulkLaunchApiService } from "sharedModules/services/api/bulk-launch-assessment-api.service";
import { TimeStamp } from "oneServices/time-stamp.service";

// interface
import {
    IStore,
    IStoreState,
} from "interfaces/redux.interface";
import { IAssessmentCreationDetails } from "interfaces/assessment/assessment-creation-details.interface";
import { ICustomTemplateDetails } from "modules/template/interfaces/custom-templates.interface";
import { IOrgUserAdapted } from "interfaces/org-user.interface";

// Enum
import { OrgUserDropdownTypes } from "enums/org-user-dropdown.enum";
import { InventoryTableIds } from "enums/inventory.enum";

@Component({
    selector: "assessment-bulk-launch-cards",
    templateUrl: "./assessment-bulk-launch-cards.component.html",
})

export class AssessmentBulkLaunchCardsComponent implements OnInit, OnDestroy {

    @Input() inventoryTypeId: InventoryTableIds;
    @Input() templateOptions: ICustomTemplateDetails[] = [];
    @Input() isTemplateReady = false;
    @Input() approverUserList: IOrgUserAdapted[];
    @Input() respondentUserList: IOrgUserAdapted[];
    @Input() orgIdList: string[];

    public bulkAssessmentDetails: IAssessmentCreationDetails[];
    public duplicateTemplateIds: string[] = [];
    public approverDropdownType: OrgUserDropdownTypes = OrgUserDropdownTypes.ApproverPia;
    public respondentDropdownType: OrgUserDropdownTypes = OrgUserDropdownTypes.RespondentPia;
    public validReminder = true;
    public disableUntilDate = this.timeStamp.getDisableUntilDate();
    public loadUserOptionsOnClick: boolean;
    public loadExternalUserOptions: boolean;
    public format = this.timeStamp.getDatePickerDateFormat();
    private destroy$ = new Subject();

    constructor(
        @Inject(StoreToken) private store: IStore,
        private assessmentBulkLaunchActionService: AssessmentBulkLaunchActionService,
        private assessmentBulkLaunchApiService: AssessmentBulkLaunchApiService,
        private timeStamp: TimeStamp,
    ) {}

    ngOnInit(): void {
        observableFromStore(this.store).pipe(
            startWith(this.store.getState()),
            takeUntil(this.destroy$),
        ).subscribe((state: IStoreState) => this.componentWillReceiveState(state));
    }

    ngOnChanges(changes: SimpleChanges): void {
        this.templateOptions = changes.templateOptions ? changes.templateOptions.currentValue : this.templateOptions;
        this.isTemplateReady = changes.isTemplateReady ? changes.isTemplateReady.currentValue === true : this.isTemplateReady;
        this.setBulkLaunchCards();
    }

    ngOnDestroy(): void {
        this.destroy$.next();
        this.destroy$.complete();
    }

    setBulkLaunchCards(): void {
        if (this.orgIdList.length <= 1) {
            this.loadUserOptionsOnClick = false;
            this.loadExternalUserOptions = true;
        } else {
            this.loadUserOptionsOnClick = true;
            this.loadExternalUserOptions = false;
        }
        this.bulkAssessmentDetails = getBulkAssessmentDetailsList(this.store.getState());
        this.bulkAssessmentDetails.forEach((assessmentDetails: IAssessmentCreationDetails) => {
            assessmentDetails.respondentsOrgUserList = assessmentDetails.respondentsOrgUserList ? cloneDeep(assessmentDetails.respondentsOrgUserList) : [];
            assessmentDetails.approversOrgUserList = assessmentDetails.approversOrgUserList ? cloneDeep(assessmentDetails.approversOrgUserList) : [];
        });
    }

    accessName(assessmentDetails: IAssessmentCreationDetails) {
        return this.inventoryTypeId === InventoryTableIds.Incidents
            ? assessmentDetails.incidentDetails.incidentName
            : assessmentDetails.inventoryDetails.inventoryName;
    }

    approversSelected(selectedApprovers: IOrgUserAdapted[], assessmentDetails: IAssessmentCreationDetails) {
        assessmentDetails.approversOrgUserList = selectedApprovers;
        this.updateStoreWithAssesmentDetails(assessmentDetails);
    }

    respondentsSelected(selectedRespondents: IOrgUserAdapted[], assessmentDetails: IAssessmentCreationDetails) {
        assessmentDetails.respondentsOrgUserList = selectedRespondents;
        this.updateStoreWithAssesmentDetails(assessmentDetails);
    }

    getTemplateSelectedModel(assessmentDetails: IAssessmentCreationDetails): ICustomTemplateDetails {
        const selectedTemplate: ICustomTemplateDetails = {
            icon: "",
            id: assessmentDetails.templateId,
            isLocked: false,
            name: assessmentDetails.templateName,
            nextTemplateId: "",
            rootVersionId: "",
            status: "",
            totalProjectCount: 0,
            version: 0,
            versionProjectCount: 0,
        };
        return selectedTemplate;
    }

    templateSelected(selectedTemplate: any, assessmentDetails: IAssessmentCreationDetails): void {
        if (selectedTemplate && selectedTemplate.rootVersionId) {
            const rootVersionId = selectedTemplate.rootVersionId;
            const inventoryIds = assessmentDetails.incidentDetails ? assessmentDetails.incidentDetails.incidentId : assessmentDetails.inventoryDetails.inventoryId;
            const params = {
                templateRootVersionId: rootVersionId,
                inventoryIds: Array(inventoryIds),
            };

            this.assessmentBulkLaunchApiService.getDuplicateAssessmentWarnings(params)
                .subscribe((res) => {
                    if (res) {
                        assessmentDetails.showDuplicateWarning = res.data.includes(assessmentDetails.incidentDetails ? assessmentDetails.incidentDetails.incidentId : assessmentDetails.inventoryDetails.inventoryId);
                    }
                });
        }
        if (selectedTemplate) {
            assessmentDetails.templateId = selectedTemplate.id;
            assessmentDetails.templateName = selectedTemplate.name;
        } else {
            assessmentDetails.templateId = assessmentDetails.templateName = "";
        }
        this.updateStoreWithAssesmentDetails(assessmentDetails);
    }

    saveModel(attributeValue: string, attributeKey: string, assessmentDetails: IAssessmentCreationDetails): void {
        attributeValue = attributeValue.trim();
        if (attributeValue) {
            assessmentDetails[attributeKey] = attributeValue;
        } else {
            assessmentDetails[attributeKey] = "";
        }
        this.updateStoreWithAssesmentDetails(assessmentDetails);
    }

    dateSelected(selectedDate: any, assessmentDetails: IAssessmentCreationDetails): void {
        if (selectedDate.dateTime) {
            assessmentDetails.deadline = selectedDate.dateTime;
        } else {
            assessmentDetails.deadline = "";
            assessmentDetails.reminder = selectedDate.dateTimeUtc;
            assessmentDetails.isValidReminder = true;
        }
        this.updateStoreWithAssesmentDetails(assessmentDetails);
        this.reminderSelected(assessmentDetails.reminder, assessmentDetails);
    }

    reminderSelected(days: number, assessmentDetails: IAssessmentCreationDetails): void {
        const today = this.timeStamp.currentDate.format();
        const reminderDate = this.timeStamp.getReminderDate(days, assessmentDetails.deadline);
        const validReminder = days > 0 && reminderDate > today;

        assessmentDetails.reminder = days;
        assessmentDetails.isValidReminder = (!days && days !== 0) || validReminder;
        this.updateStoreWithAssesmentDetails(assessmentDetails);
    }

    deleteAssessment(assessmentDetails: IAssessmentCreationDetails): void {
        remove(this.bulkAssessmentDetails, this.whichDetailsToReference(this.inventoryTypeId, assessmentDetails));
        this.updateStore(this.bulkAssessmentDetails);
    }

    toggleDetails(assessmentDetails: IAssessmentCreationDetails): void {
        assessmentDetails.showDetails = !assessmentDetails.showDetails;
    }

    closeWarning(assessmentDetails: IAssessmentCreationDetails): void {
        assessmentDetails.showDuplicateWarning = false;
    }

    private updateStoreWithAssesmentDetails(assessmentDetails: IAssessmentCreationDetails): void {
        const index = findIndex(this.bulkAssessmentDetails, this.whichDetailsToReference(this.inventoryTypeId, assessmentDetails));
        this.bulkAssessmentDetails.splice(index, 1, assessmentDetails);
        this.updateStore(this.bulkAssessmentDetails);
    }

    private updateStore(bulkAssessmentDetails: IAssessmentCreationDetails[]): void {
        this.assessmentBulkLaunchActionService.setBulkLaunchAssessments(bulkAssessmentDetails);
    }

    private componentWillReceiveState(state: IStoreState): void {
        this.setBulkLaunchCards();
    }

    private whichDetailsToReference(inventoryId: InventoryTableIds, assessmentDetails: IAssessmentCreationDetails) {
        if (inventoryId === InventoryTableIds.Incidents) {
            return { incidentDetails: { incidentId: assessmentDetails.incidentDetails.incidentId } };
        } else {
            return { inventoryDetails: { inventoryId: assessmentDetails.inventoryDetails.inventoryId } };
        }
    }

}
