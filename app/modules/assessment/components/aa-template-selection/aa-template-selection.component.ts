// Core
import { Component, OnInit, Inject } from "@angular/core";
import { StateService } from "@uirouter/core";

// interfaces
import { ICustomTemplateDetails } from "modules/template/interfaces/custom-templates.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";

// constants
import { TemplateTypes } from "constants/template-types.constant";
import { TemplateCriteria } from "constants/template-states.constant";

// enums
import { TemplateStates } from "enums/template-states.enum";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

// services
import { AATemplateApiService } from "modules/assessment/services/api/aa-template-api.service";
import { IBreadcrumb } from "interfaces/breadcrumb.interface";
import { IGalleryTemplateCardAttributes } from "interfaces/gallery-templates.interface";

@Component({
    selector: "aa-template-selection",
    templateUrl: "./aa-template-selection.component.html",
})

export class AATemplateSelectionComponent implements OnInit {

    breadCrumb: IBreadcrumb;
    loadingTemplates = true;
    templateCards: IGalleryTemplateCardAttributes[] = [];

    private createQuestionnaireUrl: string;

    constructor(
        private stateService: StateService,
        private translatePipe: TranslatePipe,
        private AATemplateApi: AATemplateApiService,
    ) { }

    ngOnInit(): void {
        this.getAssessmentTemplates();
        this.configureBreadCrumb();
    }

    trackByFn(index: number, card: IGalleryTemplateCardAttributes): string {
        return card.id;
    }

    goToRoute(route: {params: any, path: string}): void {
        this.stateService.go(route.path);
    }

    routeToQuestionnaires(): void {
        this.stateService.go(this.createQuestionnaireUrl);
    }

    getAssessmentTemplates = (): void => {
        const getAssessmentTemplatesFilter: string = this.stateService.params.launchAssessmentDetails
            ? `${TemplateTypes.PIA},${TemplateTypes.VENDOR}`
            : TemplateTypes.PIA;

        this.AATemplateApi.getAssessmentTemplates(getAssessmentTemplatesFilter, TemplateStates.Published, TemplateCriteria.Active, true)
            .then((response: IProtocolResponse<ICustomTemplateDetails[]>): void => {
                if (response.result) {
                    this.templateCards = [];
                    for (const key in response.data) {
                        if (response.data[key]) {
                            const template = response.data[key];
                            this.templateCards.push({
                                id: template.id,
                                header: {
                                    text: template.name,
                                },
                                image: {
                                    icon: template.icon,
                                },
                                version: template.version,
                            });
                        }
                    }
                }
                this.loadingTemplates = false;
            });
    }

    onTemplateClick = (card: IGalleryTemplateCardAttributes): void => {
        this.stateService.go("zen.app.pia.module.assessment.wizard.assessment_creation", {
            templateId: card.id,
            version: card.version,
            launchAssessmentDetails: this.stateService.params.launchAssessmentDetails || null,
        });
    }

    configureBreadCrumb(): void {
        this.breadCrumb = { stateName: "", text: "" };
        if (this.stateService.params.launchAssessmentDetails) {
            const inventoryTypeId: number = this.stateService.params.launchAssessmentDetails.inventoryTypeId;
            this.breadCrumb.stateName = `zen.app.pia.module.vendor.inventory.list({Id: ${inventoryTypeId}, page: null, size: null})`;
            this.breadCrumb.text = this.translatePipe.transform("VendorInventory");
            this.createQuestionnaireUrl = "zen.app.pia.module.vendor.templates";
        } else {
            this.breadCrumb.stateName = "zen.app.pia.module.assessment.list";
            this.breadCrumb.text = this.translatePipe.transform("Assessments");
            this.createQuestionnaireUrl = "zen.app.pia.module.templatesv2.list";
        }
    }
}
