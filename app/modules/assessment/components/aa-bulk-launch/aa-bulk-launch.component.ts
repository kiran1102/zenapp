// Core
import { Inject, Component, OnInit } from "@angular/core";
import { StateService } from "@uirouter/core";

// 3rd Party
import {
    toInteger,
    uniq,
    keys,
    keyBy,
} from "lodash";

// Reducers
import { getBulkAssessmentDetailsList } from "modules/assessment/reducers/assessment-detail.reducer";

// Interfaces
import { IProtocolResponse } from "interfaces/protocol.interface";
import { ICustomTemplateDetails } from "modules/template/interfaces/custom-templates.interface";
import { IStore } from "interfaces/redux.interface";
import { StoreToken } from "tokens/redux-store.token";
import { IStringMap } from "interfaces/generic.interface";
import {
    IUserDropdownConfig,
    IOrgUserAdapted,
} from "interfaces/org-user.interface";

// Service
import { AATemplateApiService } from "modules/assessment/services/api/aa-template-api.service";
import {
    AABulkLaunchAdapterService,
    IAABulkLaunchParams,
} from "modules/assessment/services/adapter/aa-bulk-launch-adapter.service";
import { OrgUserDropdownService } from "generalcomponent/org-user-dropdown/org-user-dropdown.service";
import { OrgGroupApiService } from "oneServices/api/org-group-api.service.ts";

// Enum/Constants
import { TemplateStates } from "enums/template-states.enum";
import { TemplateTypes } from "constants/template-types.constant";
import { TemplateCriteria } from "constants/template-states.constant";
import { InventoryTableIds } from "enums/inventory.enum";
import { OrgUserDropdownTypes } from "enums/org-user-dropdown.enum";

@Component({
    selector: "aa-bulk-launch",
    templateUrl: "./aa-bulk-launch.component.html",
})

export class AABulkLaunchComponent implements OnInit {

    approverDropdownOptions: IOrgUserAdapted[];
    approverUserList: IStringMap<IOrgUserAdapted[]> = {};
    breadcrumb: IAABulkLaunchParams;
    bulkAssessmentDetails = getBulkAssessmentDetailsList(this.store.getState());
    isBulkControlsReady = false;
    isLoadingAssessments = true;
    orgIdList: string[];
    respondentDropdownOptions: IOrgUserAdapted[];
    respondentUserList: IStringMap<IOrgUserAdapted[]> = {};
    templateDetails: ICustomTemplateDetails[] = [];

    private inventoryTypeId = toInteger(this.stateService.params.inventoryTypeId) as InventoryTableIds;

    constructor(
        private stateService: StateService,
        @Inject(StoreToken) private store: IStore,
        private AssessmentTemplateAPI: AATemplateApiService,
        readonly OrgGroups: OrgGroupApiService,
        readonly orgUserDropdownService: OrgUserDropdownService,
        readonly aABulkLaunchAdapterService: AABulkLaunchAdapterService,
    ) {}

    ngOnInit(): void {
        this.breadcrumb = this.aABulkLaunchAdapterService.getBreadcrumb(this.stateService.params);

        if (this.bulkAssessmentDetails && this.bulkAssessmentDetails.length < 1) {
            this.stateService.go(this.breadcrumb.path, this.breadcrumb.params);
        } else {
            const promises = [];
            this.orgIdList = this.setOrgIdList();
            promises.push(this.AssessmentTemplateAPI.getAssessmentTemplates(`${TemplateTypes.PIA},${TemplateTypes.VENDOR}`, TemplateStates.Published, TemplateCriteria.Active, true));
            promises.push(this.getOrgGroupUserList(OrgUserDropdownTypes.ApproverPia));
            promises.push(this.getOrgGroupUserList(OrgUserDropdownTypes.RespondentPia));

            Promise.all(promises).then(([templates, approvers, respondents]) => {
                if (templates && approvers && respondents) {
                    this.templateDetails = templates ? templates.data : null;
                    this.approverDropdownOptions = approvers ? approvers.data : null;
                    this.respondentDropdownOptions = respondents ? respondents.data : null;

                    if (this.respondentDropdownOptions) {
                        this.bulkAssessmentDetails = this.prePopulateRespondents();
                    }

                    if (templates) {
                        this.isBulkControlsReady = true;
                        this.isLoadingAssessments = false;
                    }
                }
            });
        }

    }

    goToRoute(route: { path: string, params: any }) {
        this.stateService.go(route.path, route.params);
    }

    private setOrgIdList(): string[] {
        if (this.inventoryTypeId === InventoryTableIds.Incidents) {
            return [this.stateService.params.orgId];
        } else {
            return uniq(keys(keyBy(this.bulkAssessmentDetails, "orgGroupId")));
        }
    }

    private prePopulateRespondents() {
        return this.bulkAssessmentDetails.map((details) => {
            if (details.respondentModel) {
                details.respondentsOrgUserList = [];
                const user = this.respondentDropdownOptions.find((option: IOrgUserAdapted) => option.Id === details.respondentModel || option.FullName === details.respondentModel);
                if (user) {
                    details.respondentsOrgUserList.push(user);
                }
            }
            return details;
        });
    }

    private getOrgGroupUserList(type: OrgUserDropdownTypes): ng.IPromise<IProtocolResponse<IOrgUserAdapted[]>> {
        const preConfig: IUserDropdownConfig = {
            filterCurrentUser: false,
            orgId: "",
            permissionKeys: undefined,
            type,
        };
        const config = this.orgUserDropdownService.getOrgUserDropdownConfig(preConfig);
        return this.OrgGroups.getOrgUsersWithPermission(
            config.orgId,
            config.traversal,
            config.filterCurrentUser,
            config.permissionKeys,
        );
    }
}
