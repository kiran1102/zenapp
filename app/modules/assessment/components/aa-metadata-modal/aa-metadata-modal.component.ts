// Angular
import {
    OnInit,
    Inject,
    Component,
} from "@angular/core";

// 3rd party
import {
    findIndex,
    find,
    some,
    sortBy,
    filter,
    uniqBy,
} from "lodash";
import { IMyDateModel } from "mydatepicker";

// Rxjs
import {
    startWith,
    takeUntil,
} from "rxjs/operators";
import { Subject } from "rxjs";

// Services
import { ModalService } from "sharedServices/modal.service";
import { TimeStamp } from "oneServices/time-stamp.service";
import { AaDetailActionService } from "modules/assessment/services/action/aa-detail-action.service";
import { AssessmentDetailApiService } from "modules/assessment/services/api/assessment-detail-api.service";

// Reducer
import { getModalData } from "oneRedux/reducers/modal.reducer";
import {
    getAssessmentDetailTempModel,
    getAssessmentDetailStatus,
} from "modules/assessment/reducers/assessment-detail.reducer";
import { StoreToken } from "tokens/redux-store.token";
import {
    IStore,
    IStoreState,
} from "interfaces/redux.interface";
import { ISectionHeaderModel } from "interfaces/assessment/section-model.interface";
import observableFromStore from "oneRedux/utils/observable-from-store";
import { getOrgById } from "oneRedux/reducers/org.reducer";

// Interfaces
import { IOrgUserAdapted } from "interfaces/org-user.interface";
import {
    ISectionRespondentMetaDataModel,
    ISectionApproverMetaDataModel,
} from "interfaces/assessment/section-model.interface";
import { IAssessmentModel } from "interfaces/assessment/assessment-model.interface";
import { IKeyValue } from "interfaces/generic.interface";
import { IQuestionModelOption } from "interfaces/assessment/question-model.interface";
import { IMultichoiceDropdownSelection } from "interfaces/assessment-template-question.interface";
import { IOrganization } from "interfaces/organization.interface";
import { IAssessmentValidApproverRespondentResolve } from "interfaces/assessment/aa-metadata-modal.interfaces";
import { IProtocolResponse } from "interfaces/protocol.interface";

// Enums
import { AssessmentStatus } from "enums/assessment/assessment-status.enum";

@Component({
    selector: "question-build",
    templateUrl: "./aa-metadata-modal.component.html",
})

export class AAMetadataModalComponent implements OnInit {

    isToggleAssessmentDetailsOpen = true;
    assessmentName: string;
    assessmentId: string;
    assessmentDeadlineModel: string;
    updatedAssessmentDeadline: IMyDateModel;
    validReminder = true;
    assessmentReminder: number;
    assessmentMetaDataDescription: string;
    isToggleApproverListOpen = true;
    approverList: IOrgUserAdapted[] = [];
    approverComment: string;
    isToggleRespondentListOpen = true;
    respondentList: IOrgUserAdapted[] = [];
    respondentComment: string;
    selectedApproverList: IOrgUserAdapted[] = [];
    selectedRespondentList: IOrgUserAdapted[] = [];
    modalData: ISectionHeaderModel;
    orgId: string;
    updatedAssessment: IAssessmentModel;
    isSavingMetaData = false;
    format = this.timeStamp.getDatePickerDateFormat();
    disableUntilDate = this.timeStamp.getDisableUntilDate();
    inputValue: string;
    originalOptions: IQuestionModelOption[] = [];
    dropdownOptions: IQuestionModelOption[] = [];
    selectedOptions: IQuestionModelOption[] = [];
    allowOtherPrependText = "AddOption";
    savedAssessmentDeadLine: string;
    selectedOrg: IOrganization;
    invalidRespondent: IOrgUserAdapted[] = [];
    invalidApprover: IOrgUserAdapted[] = [];
    originalApprovers: IOrgUserAdapted[] = [];
    originalRespondents: IOrgUserAdapted[] = [];
    templateName: string;
    isLoading = false;
    isAssessmentInCompleteState = false;
    isOrgChanged = false;

    private destroy$ = new Subject();

    constructor(
        @Inject(StoreToken) private store: IStore,
        private modalService: ModalService,
        private timeStamp: TimeStamp,
        private assessmentDetailAction: AaDetailActionService,
        private assessmentDetailApi: AssessmentDetailApiService,
    ) {}

    ngOnInit(): void {
        observableFromStore(this.store).pipe(
            startWith(this.store.getState()),
            takeUntil(this.destroy$),
        ).subscribe((state: IStoreState) => this.componentWillReceiveState(state));
    }

    initializeModal(modalData) {
        const currentOrg = getOrgById(modalData.org.id)(this.store.getState());
        this.assessmentName = modalData.name;
        this.assessmentMetaDataDescription = modalData.description ? modalData.description : "";
        this.savedAssessmentDeadLine = modalData.deadline ? modalData.deadline : null;
        this.assessmentDeadlineModel = modalData.deadline ? this.timeStamp.formatDateWithoutTime(modalData.deadline) : "";
        this.assessmentReminder = modalData.reminder ? modalData.reminder : null;
        this.selectedOrg = {
            id: modalData.org.id,
            name: currentOrg.Name,
            parentId: currentOrg.ParentId,
            canDelete: currentOrg.CanDelete,
            privacyOfficerId: currentOrg.PrivacyOfficerId,
            privacyOfficerName: currentOrg.PrivacyOfficerName,
        };
        if (modalData.selectedTags && modalData.selectedTags.length) {
            modalData.selectedTags.forEach((assessmentTag: IQuestionModelOption): void => {
                this.selectedOptions.push({
                    id: assessmentTag.id,
                    option: assessmentTag.option,
                });
            });
            this.selectedOptions = uniqBy(this.selectedOptions, "option");
        }

        this.assessmentDetailAction.getAssessmentTags().then((assessmentTags: IQuestionModelOption[]) => {
            if (assessmentTags && assessmentTags.length) {
                assessmentTags.forEach((assessmentTag: IQuestionModelOption): void => {
                    this.originalOptions.push({
                        id: assessmentTag.id,
                        option: assessmentTag.option,
                    });
                });
                this.dropdownOptions = sortBy(this.applyExcludeOptions(this.selectedOptions, this.originalOptions), ["option"]);
            }
        });

        if (modalData.respondents.length) {
            modalData.respondents.forEach((respondent: ISectionRespondentMetaDataModel): void => {
                this.respondentList.push({
                    Id: respondent.id,
                    FullName: respondent.name,
                });
            });
            this.respondentList = uniqBy(this.respondentList, "Id");
        }

        if (modalData.approvers.length) {
            modalData.approvers.forEach((approver: ISectionApproverMetaDataModel): void => {
                this.approverList.push({
                    Id: approver.id,
                    FullName: approver.name,
                });
            });
            this.approverList = uniqBy(this.approverList, "Id");
        }
        this.originalApprovers = [...this.approverList];
        this.selectedApproverList = [...this.approverList];
        this.originalRespondents = [...this.respondentList];
        this.selectedRespondentList = [...this.respondentList];
    }

    applyExcludeOptions(selectedOptions: IQuestionModelOption[], originalOptions: IQuestionModelOption[]): IQuestionModelOption[] {
        if (selectedOptions
            && selectedOptions.length
            && selectedOptions.filter((option) => option != null).length > 0) {
            return (originalOptions.filter((option) => !(findIndex(selectedOptions, {id: option.id}) > -1))).filter((option) => !(findIndex(selectedOptions, {option: option.option}) > -1));
        } else {
            return originalOptions;
        }
    }

    toggleAssessmentDetails(isToggleAssessmentDetailsOpen: boolean) {
        this.isToggleAssessmentDetailsOpen = isToggleAssessmentDetailsOpen;
    }

    toggleApproverList(isToggleApproverListOpen: boolean) {
        this.isToggleApproverListOpen = isToggleApproverListOpen;
    }

    toggleRespondentList(isToggleRespondentListOpen: boolean) {
        this.isToggleRespondentListOpen = isToggleRespondentListOpen;
    }

    setAssessmentName(assessmentName: string): void {
        assessmentName.trim() ? this.assessmentName = assessmentName : this.assessmentName = "";
    }

    setOrganization(orgId: string) {
        this.isOrgChanged = true;
        if (orgId !== this.orgId) {
            this.invalidApprover = [];
            this.invalidRespondent = [];
            this.selectedApproverList = [...this.originalApprovers].filter((user: IOrgUserAdapted): boolean => {
                return this.selectedApproverList.includes(user);
            });
            this.approverList = [...this.selectedApproverList];
            this.selectedRespondentList = [...this.originalRespondents].filter((user: IOrgUserAdapted): boolean => {
                return this.selectedRespondentList.includes(user);
            });
            this.respondentList = [...this.selectedRespondentList];
        }

        if (!orgId) {
            this.orgId = orgId;
            this.selectedOrg = {
                id: null,
                name: null,
                parentId: null,
                canDelete: null,
                privacyOfficerId: null,
                privacyOfficerName: null,
            };
            return;
        }
        const currentOrg = getOrgById(orgId)(this.store.getState());
        this.isLoading = true;
        this.assessmentDetailApi.validateEntities(this.assessmentId, orgId).subscribe((response: IProtocolResponse<IAssessmentValidApproverRespondentResolve>) => {
            if (!response) return;
            const validApprovers = response.data.validApproverIds;
            const validRespondents = response.data.validRespondentIds;

            if (this.selectedApproverList.length > 0 && this.selectedApproverList[0] !== null) {
                this.selectedApproverList.forEach((approver: IOrgUserAdapted) => {
                    approver.errorMessage = !validApprovers.includes(approver.Id) ? "ApproverNotValid" : null;
                    if (approver.errorMessage) {
                        this.invalidApprover.push(approver);
                    }
                });
                this.invalidApprover = this.invalidApprover.filter((approver, index) => {
                    return this.invalidApprover.indexOf(approver) >= index;
                });
            }

            if (this.selectedRespondentList.length > 0) {
                this.selectedRespondentList.forEach((respondent: IOrgUserAdapted) => {
                    respondent.errorMessage = !validRespondents.includes(respondent.Id) ? "RespondentNotValid" : null;
                    if (respondent.errorMessage) {
                        this.invalidRespondent.push(respondent);
                    }
                });
                this.invalidRespondent = this.invalidRespondent.filter((respondent, index) => {
                    return this.invalidRespondent.indexOf(respondent) >= index;
                });
            }
            this.orgId = orgId;
            this.selectedOrg = {
                id: orgId,
                name: currentOrg.Name,
                parentId: currentOrg.ParentId,
                canDelete: currentOrg.CanDelete,
                privacyOfficerId: currentOrg.PrivacyOfficerId,
                privacyOfficerName: currentOrg.PrivacyOfficerName,
            };
            this.isLoading = false;
        });
    }

    setAssessmentDeadline(assessmentDeadline: IMyDateModel): void {
        this.savedAssessmentDeadLine = null;
        this.updatedAssessmentDeadline = assessmentDeadline ? assessmentDeadline : null;
        this.assessmentDeadlineModel = assessmentDeadline.formatted ? assessmentDeadline.formatted : "";
        this.assessmentReminder && this.assessmentDeadlineModel ? this.setReminderDaysSelected(this.assessmentReminder) : this.assessmentReminder = null;
    }

    setReminderDaysSelected(selectedReminder: number): void {
        const today = this.timeStamp.currentDate.format();
        const deadline: string = this.updatedAssessmentDeadline && this.updatedAssessmentDeadline.jsdate ? this.updatedAssessmentDeadline.jsdate.toISOString() : null;
        const reminderDate = this.timeStamp.getReminderDate(selectedReminder, deadline);

        this.validReminder = selectedReminder > 0 && reminderDate > today;
        this.assessmentReminder = selectedReminder;

        if ((!selectedReminder && selectedReminder !== 0) || this.validReminder) {
            this.validReminder = true;
        } else {
            this.validReminder = false;
        }
    }

    setAssessmentMetaDataDescription(assessmentMetaDataDescription: string): void {
        this.assessmentMetaDataDescription = assessmentMetaDataDescription;
    }

    setApproverComment(approverComment: string): void {
        this.approverComment = approverComment;
    }

    setRespondentComment(respondentComment: string): void {
        this.respondentComment = respondentComment;
    }

    saveAssessmentMetaData(): void {
        this.isSavingMetaData = true;
        this.updatedAssessment = getAssessmentDetailTempModel(this.store.getState());

        this.updatedAssessment.approvers = [];
        this.updatedAssessment.respondents = [];

        if (this.selectedApproverList.length) {
            this.selectedApproverList.forEach((approver: IOrgUserAdapted): void => {
                if (approver) {
                    this.updatedAssessment.approvers.push({
                        id: approver.Id,
                        name: approver.FullName,
                        comment: this.approverComment,
                        approvedOn: find(this.modalData.approvers, { id: approver.Id }) ? find(this.modalData.approvers, { id: approver.Id }).approvedOn : null,
                        approvalState: find(this.modalData.approvers, { id: approver.Id }) ? find(this.modalData.approvers, { id: approver.Id }).approvalState : null,
                    });
                }
            });
        }

        if (this.selectedRespondentList.length) {
            this.selectedRespondentList.forEach((respondent: IOrgUserAdapted): void => {
                if (respondent) {
                    const respondentId = respondent.Id === respondent.FullName ? null : respondent.Id;
                    this.updatedAssessment.respondents.push({
                        id: respondentId,
                        name: respondent.FullName,
                        comment: this.respondentComment,
                    });
                }
            });
        }

        this.updatedAssessment = {
            ...this.updatedAssessment,
            name: this.assessmentName,
            description: this.assessmentMetaDataDescription,
            deadline: this.savedAssessmentDeadLine ? this.savedAssessmentDeadLine :
                this.updatedAssessmentDeadline && this.updatedAssessmentDeadline.jsdate ? this.updatedAssessmentDeadline.jsdate.toISOString() : "",
            orgGroup: {
                id: this.selectedOrg.id,
                name: this.selectedOrg.name,
            },
            reminder: this.assessmentReminder,
            tags: this.selectedOptions,
        };

        this.assessmentDetailAction.saveTempAssessment(this.updatedAssessment)
            .then(() => {
                this.closeModal();
                if (this.modalData.closeCallback) {
                    this.modalData.closeCallback();
                }
                this.isSavingMetaData = false;
                this.isOrgChanged = false;
            });
    }

    isSaveButtonDisabled() {
        if (this.assessmentName.length && this.validReminder && this.orgId && (this.invalidRespondent.length === 0) && (this.invalidApprover.length === 0) && (this.selectedRespondentList.length > 0)) {
            return this.hasNoRespondentsSelected();
        } else {
            return true;
        }
    }

    onSelectedApproversList(approverList: IOrgUserAdapted[]) {
        this.selectedApproverList = approverList;
        if (this.invalidApprover.length > 0) {
            this.invalidApprover.forEach((user, index) => {
                if (!this.selectedApproverList.includes(user)) {
                    this.invalidApprover.splice(index, 1);
                }
            });
        }
    }

    selectedRespondentsList(respondentList: IOrgUserAdapted[]) {
        this.selectedRespondentList = respondentList;
        if (this.invalidRespondent.length > 0) {
            this.invalidRespondent.forEach((user, index) => {
                if (!this.selectedRespondentList.includes(user)) {
                    this.invalidRespondent.splice(index, 1);
                }
            });
        }
    }

    onModelChange({ currentValue, change, previousValue }: IMultichoiceDropdownSelection): void {
        if (!currentValue) {
            this.inputValue = "";
        }
        this.selectedOptions = currentValue;
        if (!previousValue || currentValue.length > previousValue.length) {
            const optionIndex: number = findIndex(this.dropdownOptions, (opt: IQuestionModelOption): boolean => {
                return change.option === opt.option;
            });
            this.dropdownOptions.splice(optionIndex, 1);
            this.dropdownOptions = [...this.dropdownOptions];
        } else if (change.id
            && !some(this.dropdownOptions, {id: change.id})
            && !some(this.selectedOptions, {id: change.id})
            && some(this.originalOptions, {id: change.id})) {
                this.dropdownOptions.push({
                    id: change.id,
                    option: change.option,
                });
        }
        this.dropdownOptions = sortBy(this.dropdownOptions, ["option"]);
    }

    onInputChange({ key, value }: IKeyValue<string>): void {
        if (key === "Enter") return;
        this.inputValue = value;
        this.filterList(this.originalOptions, value);
    }

    isOptionInList(originalOptions: IQuestionModelOption[], selectedOptions: IQuestionModelOption[], stringSearch: string): boolean {
        return Boolean(find(originalOptions, (option: IQuestionModelOption): boolean => option.option.toLowerCase() === stringSearch.toLowerCase()) || find(selectedOptions, (option: IQuestionModelOption): boolean => option.option.toLowerCase() === stringSearch.toLowerCase()));
    }

    handleFirstOption(value) {
        if (value && !this.isOptionInList(this.originalOptions, this.selectedOptions, value)) {
            this.selectedOptions.push({
                id: null,
                option: value,
                optionType: null,
            });
        }
        this.selectedOptions = [...this.selectedOptions];
        this.inputValue = "";
        this.dropdownOptions = sortBy(this.applyExcludeOptions(this.selectedOptions, this.originalOptions), ["option"]);
    }

    filterList(originalOptions: IQuestionModelOption[], stringSearch: string): void {
        this.dropdownOptions = sortBy(this.applyExcludeOptions(this.selectedOptions, originalOptions), ["option"]);
        this.dropdownOptions = filter(this.dropdownOptions, (opt: IQuestionModelOption): boolean => (opt.option.toLowerCase().search(stringSearch.toLowerCase()) !== -1));
    }

    closeModal(): void {
        this.destroy$.next();
        this.destroy$.complete();
        this.modalService.closeModal();
        this.modalService.clearModalData();
    }

    hasNoRespondentsSelected(): boolean {
        return !((this.selectedRespondentList.length && this.selectedRespondentList[0]) || (this.respondentList.length && this.respondentList[0]));
    }

    private componentWillReceiveState(state: IStoreState): void {
        if (!this.isSavingMetaData && !this.isOrgChanged) {
            this.modalData = getModalData(this.store.getState());
            this.isAssessmentInCompleteState = getAssessmentDetailStatus(this.store.getState()) === AssessmentStatus.Completed;
            if (!this.modalData) return;
            this.initializeModal(this.modalData);
            this.orgId = this.modalData.org.id;
            this.templateName = this.modalData.template.name;
            this.assessmentId = getAssessmentDetailTempModel(state).assessmentId;
        }
    }
}
