// Angular
import {
    Component,
    OnInit,
    Inject,
} from "@angular/core";
import { StateService } from "@uirouter/core";

// Lodash
import {
    sortBy,
} from "lodash";

// Redux
import { getCurrentUser } from "oneRedux/reducers/user.reducer";
import { getOrgById } from "oneRedux/reducers/org.reducer";
import { StoreToken } from "tokens/redux-store.token";
// services
import { AATemplateApiService } from "assessmentServices/api/aa-template-api.service";
import { Permissions } from "modules/shared/services/helper/permissions.service";
import { TimeStamp } from "oneServices/time-stamp.service";
import { NotificationService } from "modules/shared/services/provider/notification.service";
import { AaDetailActionService } from "modules/assessment/services/action/aa-detail-action.service";

// interfaces
import { IAssessmentCreationDetails } from "interfaces/assessment/assessment-creation-details.interface";
import { IOrganization } from "interfaces/organization.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";
import { IStringMap } from "interfaces/generic.interface";
import { IUser } from "interfaces/user.interface";
import { IBreadcrumb } from "interfaces/breadcrumb.interface";
import { IStore } from "interfaces/redux.interface";
import { IOrgUserAdapted } from "interfaces/org-user.interface";
import { IKeyValue } from "interfaces/generic.interface";
import { IQuestionModelOption } from "interfaces/assessment/question-model.interface";
import { IMultichoiceDropdownSelection } from "interfaces/assessment-template-question.interface";

// constants
import { AssessmentCreationActions } from "constants/assessment-creation-actions.constant";
import { AAErrorMessage } from "constants/aa-error-message.constant";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

@Component({
    selector: "aa-create-form",
    templateUrl: "aa-create-form.component.html",
})

export class AACreateFormComponent implements OnInit {
    assessment: IAssessmentCreationDetails;
    assessmentActions: typeof AssessmentCreationActions;
    comment = "";
    currentUser: IUser;
    orgList: IOrganization[];
    createPermissions: IStringMap<boolean>;
    respondentUserId: string;
    saveInProgress = false;
    selectedOrg: IOrganization;
    showMoreDetails: boolean;
    translations: IStringMap<string>;
    breadCrumb: IBreadcrumb;
    format = this.timeStamp.getDatePickerDateFormat();
    options = [];
    selectedOrgId: string;
    deadline: string;
    validReminder = true;
    reminder: number;
    approverList: IOrgUserAdapted[] = [];
    respondentList: IOrgUserAdapted[] = [];
    selectedApproverList: IOrgUserAdapted[] = [];
    selectedRespondentList: IOrgUserAdapted[] = [];
    disableUntilDate = this.timeStamp.getDisableUntilDate();
    inputValue: string;
    selectedOptions: IQuestionModelOption[] = [];
    dropdownOptions: IQuestionModelOption[] = [];
    originalOptions: IQuestionModelOption[] = [];
    allowOtherPrependText = "AddOption";

    constructor(
        private stateService: StateService,
        @Inject(StoreToken) private store: IStore,
        public translatePipe: TranslatePipe,
        private AssessmentTemplateAPI: AATemplateApiService,
        private permissions: Permissions,
        private timeStamp: TimeStamp,
        private notificationService: NotificationService,
        private aaDetailActionService: AaDetailActionService,
    ) { }

    ngOnInit() {
        this.createPermissions = {
            AssessmentCanBeApprover: this.permissions.canShow("AssessmentCanBeApprover"),
        };
        this.assessmentActions = AssessmentCreationActions;
        this.currentUser = getCurrentUser(this.store.getState());
        this.assessment = this.initializeAssessment();
        this.selectedOrg = {} as IOrganization;
        this.selectedOrg.name = getOrgById(this.currentUser.OrgGroupId)(this.store.getState()).Name;
        this.selectedOrg.id = this.currentUser.OrgGroupId;
        this.configureBreadCrumb();
        this.getTags();
    }

    onInputChange({ key, value }: IKeyValue<string>) {
        if (key === "Enter") return;
        this.inputValue = value;
        this.filterList(this.originalOptions, value);
    }

    onModelChange({ currentValue, change, previousValue }: IMultichoiceDropdownSelection) {
        if (!currentValue) {
            this.inputValue = "";
        }
        this.selectedOptions = currentValue;
        if (!previousValue || currentValue.length > previousValue.length) {
            const optionIndex: number = this.dropdownOptions.findIndex((opt: IQuestionModelOption): boolean => {
                return change.option === opt.option;
            });
            this.dropdownOptions.splice(optionIndex, 1);
            this.dropdownOptions = [...this.dropdownOptions];
        } else if (change.id
            && !this.dropdownOptions.some((dropOps) => dropOps.id === change.id)
            && !this.selectedOptions.some((selectedOps) => selectedOps.id === change.id)
            && this.originalOptions.some((originalOps) => originalOps.id === change.id)) {
                this.dropdownOptions.push({
                    id: change.id,
                    option: change.option,
                });
        }
        this.dropdownOptions = sortBy(this.dropdownOptions, ["option"]);
    }

    handleFirstOption(value: any) {
        if (value && !this.isOptionInList(this.originalOptions, this.selectedOptions, value)) {
            this.selectedOptions.push({
                id: null,
                option: value,
                optionType: null,
            });
        }
        this.selectedOptions = [...this.selectedOptions];
        this.inputValue = "";
        this.dropdownOptions = sortBy(this.applyExcludeOptions(this.selectedOptions, this.originalOptions), ["option"]);
    }

    filterList(originalOptions: IQuestionModelOption[], stringSearch: string) {
        this.dropdownOptions = sortBy(this.applyExcludeOptions(this.selectedOptions, originalOptions), ["option"]);
        this.dropdownOptions = this.dropdownOptions.filter((option: IQuestionModelOption): boolean => (option.option.toLowerCase().search(stringSearch.toLowerCase()) !== -1));
    }

    applyExcludeOptions(selectedOptions: IQuestionModelOption[], originalOptions: IQuestionModelOption[]): IQuestionModelOption[] {
        if (selectedOptions
            && selectedOptions.length
            && selectedOptions.filter((option) => option != null).length > 0) {
            return (originalOptions.filter((option) => !(selectedOptions.findIndex((selectedOps) => selectedOps.id === option.id) > -1)).filter((option) => !(selectedOptions.findIndex((selectedOps) => selectedOps.id === option.id) > -1)));
        } else {
            return originalOptions;
        }
    }

    getTags() {
        this.aaDetailActionService.getAssessmentTags().then((res: IQuestionModelOption[]) => {
            if (res && res.length) {
                res.forEach((assessmentTag: IQuestionModelOption): void => {
                    this.originalOptions.push({
                        id: assessmentTag.id,
                        option: assessmentTag.option,
                    });
                });
                this.dropdownOptions = sortBy(this.applyExcludeOptions(this.selectedOptions, this.originalOptions), ["option"]);
            }
        });
    }

    goToRoute(route: { params: any, path: string }): void {
        this.stateService.go(route.path);
    }

    valueChange(newValue: string) {
        this.assessment.name = newValue;
    }

    handleAction = (action: string, payload?: any): void => {
        switch (action) {
            case AssessmentCreationActions.COMMENT_CHANGED:
                this.comment = payload;
                break;
            case AssessmentCreationActions.DEADLINE_CHANGED:
                if (payload) {
                    this.assessment.deadline = payload.jsdate ? payload.jsdate.toISOString() : null;
                    this.deadline = payload.formatted;
                } else {
                    this.assessment.deadline = null;
                }
                this.assessment.reminder && this.deadline ? this.reminderDaysSelected(this.assessment.reminder) : this.assessment.reminder = null;
                break;
            case AssessmentCreationActions.DESCRIPTION_CHANGED:
                this.assessment.description = payload;
                break;
            case AssessmentCreationActions.ORG_SELECTED:
                this.selectedOrg.id = payload;
                this.selectedOrg.name = getOrgById(payload)(this.store.getState()).Name;
                this.resetApproverRespondentLists();
                this.assessment.respondents = [];
                break;
            case AssessmentCreationActions.TOGGLE_SHOW_MORE_DETAILS:
                this.showMoreDetails = !this.showMoreDetails;
                break;
        }
    }

    handleInputChange(payload: string, action: string): void {
        this.handleAction(action, payload);
    }

    resetApproverRespondentLists() {
        this.respondentList = [];
        this.selectedRespondentList = [];
        this.approverList = [];
        this.selectedApproverList = [];
    }

    initializeAssessment = (): IAssessmentCreationDetails => {
        this.selectedOrg = null;
        this.resetApproverRespondentLists();
        return {
            approverId: "",
            approverName: "",
            approvers: [],
            deadline: "",
            description: "",
            orgGroupId: "",
            orgGroupName: "",
            name: "",
            respondents: [],
            respondentModel: "",
            respondentInvitedUser: "",
            templateId: this.stateService.params.templateId,
            inventoryDetails: null,
            showDetails: false,
        };
    }

    onCreateAssessmentClick = (): void => {
        this.saveInProgress = true;
        this.assessment.orgGroupId = this.selectedOrg.id;
        this.assessment.orgGroupName = this.selectedOrg.name;
        this.assessment.tags = this.selectedOptions.map((opt: IQuestionModelOption) => opt.option);
        this.selectedApproverList.forEach((approver: IOrgUserAdapted): void => {
            this.assessment.approvers.push({
                approverId: approver.Id,
                approverName: approver.FullName,
                comment: null,
            });
        });
        this.selectedRespondentList.forEach((respondent: IOrgUserAdapted): void => {
            const respondentId = respondent.Id === respondent.FullName ? null : respondent.Id;
            this.assessment.respondents.push({
                respondentId,
                respondentName: respondent.FullName,
                comment: this.comment,
            });
        });
        this.assessment.inventoryDetails = this.stateService.params.launchAssessmentDetails || null;
        this.AssessmentTemplateAPI.createAssessment(this.assessment).then((response: IProtocolResponse<string>): void => {
            if (response.result) {
                const isCurrentUserRespondent = this.selectedRespondentList.some((respondent: IOrgUserAdapted) => this.currentUser.Id === respondent.Id);

                if (isCurrentUserRespondent) {
                    this.stateService.go("zen.app.pia.module.assessment.detail", { assessmentId: response.data, launchAssessmentDetails: this.assessment.inventoryDetails });
                } else {
                    this.stateService.go("zen.app.pia.module.assessment.list");
                }
            } else if (response.errors.code) {
                this.notificationService.alertError(this.translatePipe.transform("Error"), this.translatePipe.transform(AAErrorMessage[response.errors.code]));
                this.saveInProgress = false;
            } else {
                this.saveInProgress = false;
            }
        });
    }

    onCancelAssessmentClick = (): void => {
        this.assessment = this.initializeAssessment();
        if (this.breadCrumb.text === this.translatePipe.transform("VendorInventory")) {
            const inventoryTypeId: number = this.stateService.params.launchAssessmentDetails.inventoryTypeId;
            this.stateService.go("zen.app.pia.module.vendor.inventory.list", { Id: inventoryTypeId });
        } else {
            this.stateService.go("zen.app.pia.module.assessment.list");
        }
    }

    configureBreadCrumb(): void {
        this.breadCrumb = { stateName: "", text: "" };
        if (this.stateService.params.launchAssessmentDetails) {
            const inventoryTypeId: number = this.stateService.params.launchAssessmentDetails.inventoryTypeId;
            this.breadCrumb.stateName = `zen.app.pia.module.vendor.inventory.list({Id: ${inventoryTypeId}, page: null, size: null})`;
            this.breadCrumb.text = this.translatePipe.transform("VendorInventory");
        } else {
            this.breadCrumb.stateName = "zen.app.pia.module.assessment.list";
            this.breadCrumb.text = this.translatePipe.transform("Assessment");
        }
    }

    reminderDaysSelected(selectedReminder: number): void {
        const today = this.timeStamp.currentDate.format();
        const reminderDate = this.timeStamp.getReminderDate(selectedReminder, this.assessment.deadline);

        this.validReminder = selectedReminder > 0 && reminderDate > today;
        this.assessment.reminder = selectedReminder;

        if ((!selectedReminder && selectedReminder !== 0) || this.validReminder) {
            this.validReminder = true;
        } else {
            this.validReminder = false;
        }
    }

    selectedApprovers(approverList: IOrgUserAdapted[]): void {
        this.selectedApproverList = approverList;
    }

    selectedRespondents(respondentList: IOrgUserAdapted[]): void {
        this.selectedRespondentList = respondentList;
    }

    private isOptionInList(originalOptions: IQuestionModelOption[], selectedOptions: IQuestionModelOption[], stringSearch: string): boolean {
        return Boolean(originalOptions.find((option: IQuestionModelOption): boolean => option.option.toLowerCase() === stringSearch.toLowerCase()) || selectedOptions.find((option: IQuestionModelOption): boolean => option.option.toLowerCase() === stringSearch.toLowerCase()));
    }
}
