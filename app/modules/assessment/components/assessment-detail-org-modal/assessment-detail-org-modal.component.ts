import { buildDefaultModalTemplate } from "modals/modal-template/modal-template";

// redux
import { getModalData } from "oneRedux/reducers/modal.reducer";

// interfaces
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import { IStore } from "interfaces/redux.interface";
import { IStringMap } from "interfaces/generic.interface";
import { IOrganization } from "interfaces/organization.interface";
import { IOrgUserAdapted } from "interfaces/org-user.interface";
import {
    IAssessmentUpdateApproverRequest,
    IAssessmentUpdateRespondentRequest,
} from "interfaces/assessment/assessment-api.interface";

// enums
import { OrgUserDropdownTypes } from "enums/org-user-dropdown.enum";

// service
import OrgGroupStoreNew from "oneServices/org-group.store";
import { ModalService } from "sharedServices/modal.service";
import { AaDetailActionService } from "modules/assessment/services/action/aa-detail-action.service";

class AssessmentDetailOrgModalComponentController implements ng.IComponentController {

    static $inject: string[] = [
        "$rootScope",
        "store",
        "ModalService",
        "OrgGroupStoreNew",
        "AaServiceDispatcherService",
    ];

    approverDropdownType: OrgUserDropdownTypes;
    approverPayload: IAssessmentUpdateApproverRequest;
    currentOrg: { id: string, name: string };
    isSubmitting = false;
    modalTitle: string;
    orgList: IOrganization[];
    promiseToResolve: (...args: any[]) => any;
    removeCurrentUser: boolean;
    respondentDropdownType: OrgUserDropdownTypes;
    respondentPayload: IAssessmentUpdateRespondentRequest;
    selectedOrg: IOrganization;
    translate: (key: string, data?: any) => string;

    constructor(
        private readonly $rootScope: IExtendedRootScopeService,
        private readonly store: IStore,
        private readonly modalService: ModalService,
        readonly orgGroupStoreNew: OrgGroupStoreNew,
        private assessmentDetailAction: AaDetailActionService,
    ) {
        this.translate = $rootScope.t;
    }

    $onInit(): void {
        const appState = this.store.getState();
        this.currentOrg = getModalData(appState).currentOrg;
        this.modalTitle = getModalData(appState).modalTitle;
        this.promiseToResolve = getModalData(appState).promiseToResolve;
        this.orgList = this.orgGroupStoreNew.orgList;
        this.approverDropdownType = OrgUserDropdownTypes.ApproverPia;
        this.respondentDropdownType = OrgUserDropdownTypes.RespondentPia;
        this.approverPayload = {
            approverId: "",
            approverName: "",
            comment: "",
        };
        this.respondentPayload = {
            respondentId: "",
            respondentName: "",
            comment: "",
            sectionId: "",
        };
    }

    closeModal(): void {
        this.resetOrgSelection();
        this.modalService.closeModal();
        this.modalService.clearModalData();
    }

    handleAction(action: string, payload: any): void {
        if (this.currentOrg.id !== payload.org.id) {
            this.selectedOrg = payload.org;
            this.removeCurrentUser = true;
            this.assessmentDetailAction.updateTempAssessment({ orgGroup: { id: payload.org.id, name: payload.org.name } });
        } else {
            this.resetOrgSelection();
        }
    }

    resetOrgSelection(): void {
        this.selectedOrg = null;
        this.assessmentDetailAction.updateTempAssessment({ orgGroup: { id: this.currentOrg.id, name: this.currentOrg.name } });
    }

    handleInputChange(value: string, data: any): void {
        data.comment = value;
    }

    approverSelected(approver: IOrgUserAdapted, isValid: boolean): void {
        if (isValid) {
            this.approverPayload.approverId = approver.Id;
            this.approverPayload.approverName = approver.FullName;
        }
    }

    respondentSelected(respondent: IOrgUserAdapted, isValid: boolean): void {
        if (isValid) {
            this.respondentPayload = {
                respondentId: respondent.Id,
                respondentName: respondent.FullName,
                sectionId: "",
            };
        }
    }

    submitModal(): void {
        this.isSubmitting = true;
        this.promiseToResolve(this.approverPayload, this.respondentPayload)
            .then((success: boolean): ng.IPromise<void> | boolean => {
                if (success) {
                    this.modalService.closeModal();
                    this.modalService.clearModalData();
                    return true;
                }
                this.isSubmitting = false;
                return false;
            });
    }
}

const assessmentDetailOrgModalTemplate: IStringMap<string> = {
    template: `
        <section class="flex flex-full-height padding-all-2">
            <one-label-content
                body-class="full-width"
                is-required="true"
                name="{{::$ctrl.translate('Organization')}}"
                wrapper-class="margin-bottom-2 form-group"
            >
                <one-org-input
                    action-callback="$ctrl.handleAction(action, payload)"
                    org-list="$ctrl.orgList"
                    placeholder="{{::$ctrl.translate('SelectOrganizationGroup')}}"
                    selected-org="$ctrl.selectedOrg"
                >
                </one-org-input>
            </one-label-content>

            <!-- Uncomment when ready to reassign Approvers/Respondents based on changing Org  -->
            <!--
            <org-user-dropdown
                ng-if="$ctrl.selectedOrg"
                is-required="true"
                label-style="{'flex-basis':'20%', 'align-self':'flex-start'}"
                on-select="$ctrl.approverSelected(selection, isValid)"
                org-id="$ctrl.selectedOrg.id"
                remove-current-user="$ctrl.removeCurrentUser"
                type="$ctrl.approverDropdownType"
                wrapper-class="form-group"
            >
            </org-user-dropdown>

            <one-label-content
                body-class="full-width"
                name="{{::$ctrl.translate('Comments')}}"
                ng-if="$ctrl.selectedOrg"
                wrapper-class="margin-bottom-2 form-group"
            >
                <one-textarea
                    input-changed="$ctrl.handleInputChange(value, $ctrl.approverPayload)"
                    input-text="{{$ctrl.comment}}"
                    placeholder="{{::$ctrl.translate('ApproverCommentPlaceholder')}}"
                >
                </one-textarea>
            </one-label-content>


            <org-user-dropdown
                ng-if="$ctrl.selectedOrg"
                is-required="true"
                label-style="{'flex-basis':'20%', 'align-self':'flex-start'}"
                on-select="$ctrl.respondentSelected(selection, isValid)"
                org-id="$ctrl.selectedOrg.id"
                remove-current-user="$ctrl.removeCurrentUser"
                type="$ctrl.respondentDropdownType"
                wrapper-class="form-group"
            >
            </org-user-dropdown>

            <one-label-content
                body-class="full-width"
                name="{{::$ctrl.translate('Comments')}}"
                ng-if="$ctrl.selectedOrg"
                wrapper-class="margin-bottom-2 form-group"
            >
                <one-textarea
                    input-changed="$ctrl.handleInputChange(value, $ctrl.respondentPayload)"
                    input-text="{{$ctrl.comment}}"
                    placeholder="{{::$ctrl.translate('RespondentCommentPlaceholder')}}"
                >
                </one-textarea>
            </one-label-content>
            -->

            <footer class="row-horizontal-center margin-top-2">
                <one-button
                    button-click="$ctrl.submitModal()"
                    enable-loading="true"
                    identifier="orgReAssignSubmit"
                    is-loading="$ctrl.isSubmitting"
                    is-disabled="!$ctrl.selectedOrg || \
                                $ctrl.isSubmitting"
                    text="{{::$ctrl.translate('ReAssign')}}"
                    type="primary"
                ></one-button>
            </footer>
        </section>
    `,
};

export const assessmentDetailOrgModalComponent: ng.IComponentOptions = {
    controller: AssessmentDetailOrgModalComponentController,
    template: buildDefaultModalTemplate(assessmentDetailOrgModalTemplate.template),
};
