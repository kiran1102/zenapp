// Angular
import {
    Component,
    EventEmitter,
    Input,
    Output,
    Inject,
    OnInit,
} from "@angular/core";
import { StateService } from "@uirouter/core";

// Lodash
import { findIndex } from "lodash";

// Rxjs
import {
    takeUntil,
    take,
} from "rxjs/operators";
import { Subject } from "rxjs";

// Interfaces
import {
    IAssessmentTableRow,
    IAssessmentExportModel,
    IShareAssessmentModalData,
} from "interfaces/assessment/assessment.interface";
import {
    IGridColumn,
    IGridSort,
} from "interfaces/grid.interface";
import { IPageableMeta } from "interfaces/pagination.interface";
import { IStringMap } from "interfaces/generic.interface";
import { IDropdownOption } from "interfaces/one-dropdown.interface";
import {
    IUser,
    ISharedAssessmentUsersContractById,
} from "interfaces/user.interface";
import { IStore } from "interfaces/redux.interface";
import { IViewRelatedAssessmentModalData } from "sharedModules/interfaces/view-related-assessment-modal.interface";
import { IReportLayout } from "reportsModule/reports-shared/interfaces/report.interface";

// Enums
import {
    RiskLabelBgClasses,
    RiskLevels,
} from "enums/risk.enum";
import { AssessmentStatus } from "enums/assessment/assessment-status.enum";
import { ModuleTypes } from "enums/module-types.enum";
import { ReportTypeLowercase } from "reportsModule/reports-shared/enums/reports.enum";

// Services
import { AATemplateApiService } from "modules/assessment/services/api/aa-template-api.service";
import { ModalService } from "sharedServices/modal.service";
import { TaskPollingService } from "sharedServices/task-polling.service";
import { NotificationService } from "modules/shared/services/provider/notification.service";
import { Permissions } from "modules/shared/services/helper/permissions.service";
import { ShareAssessmentService } from "modules/assessment/services/api/share-assessment-api.service";
import { ViewRelatedAssessmentModalApiService } from "sharedModules/services/api/view-related-assessment-modal-api.service";
import { ReportTemplateExportService } from "reportsModule/reports-shared/services/report-template-export.service";

// Components
import { ViewRelatedAssessmentModalComponent } from "sharedModules/components/view-related-assessment-modal/view-related-assessment-modal.component";
import { OTNotificationModalComponent } from "sharedModules/components/ot-notification-modal/ot-notification-modal.component";
import { AAActivityModalComponent } from "modules/assessment/components/aa-assessment-activity-modal/aa-assessment-activity-modal.component";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";
import { IProtocolResponse } from "interfaces/protocol.interface";

// Redux
import { getCurrentUser } from "oneRedux/reducers/user.reducer";
import { StoreToken } from "tokens/redux-store.token";

// Vitreus
import {
    OtModalService,
    ModalSize,
} from "@onetrust/vitreus";

@Component({
    selector: "assessment-grid",
    templateUrl: "./assessment-grid.component.html",
})
export class AssessmentGridComponent implements OnInit {

    @Input() assessments: IAssessmentTableRow[];
    @Input() pagination: IPageableMeta;
    @Input() permissions: IStringMap<boolean>;
    @Input() sortData;
    @Input() columns: IGridColumn[];

    @Output() goToAssessmentDetail: EventEmitter<string> = new EventEmitter<string>();
    @Output() deleteAssessment: EventEmitter<string> = new EventEmitter<string>();
    @Output() sortAssessments: EventEmitter<IGridSort> = new EventEmitter<IGridSort>();
    @Output() editAssessment: EventEmitter<string> = new EventEmitter<string>();

    defaultExportLayout: IReportLayout;
    riskLevelBgClass: typeof RiskLabelBgClasses = RiskLabelBgClasses;
    riskLevel: typeof RiskLevels = RiskLevels;
    menuClass: string;
    currentUser: IUser;
    size: ModalSize = ModalSize.MEDIUM;
    modalData: IViewRelatedAssessmentModalData[];

    private destroy$ = new Subject();

    constructor(
        @Inject(StoreToken) public store: IStore,
        private stateService: StateService,
        private readonly AssessmentTemplateAPI: AATemplateApiService,
        private modalService: ModalService,
        private taskPollingService: TaskPollingService,
        private translatePipe: TranslatePipe,
        private Permission: Permissions,
        private notificationService: NotificationService,
        private shareAssessmentService: ShareAssessmentService,
        private otModalService: OtModalService,
        private viewRelatedAssessmentModalApiService: ViewRelatedAssessmentModalApiService,
        private reportTemplateExportService: ReportTemplateExportService,
    ) { }

    ngOnInit(): void {
        this.currentUser = getCurrentUser(this.store.getState());
        if (this.Permission.canShow("SetDefaultAssessmentPDF")) {
            this.reportTemplateExportService.getDefaultLayout(ReportTypeLowercase.PDF, ModuleTypes.PIA)
                .then((response: IProtocolResponse<IReportLayout>) => {
                    this.defaultExportLayout = response.data;
                });
        }
    }

    ngOnDestroy(): void {
        this.destroy$.next();
        this.destroy$.complete();
    }

    handleSortChange(sortData) {
        this.sortAssessments.emit(sortData);
    }

    getActions(assessment: IAssessmentTableRow): () => IDropdownOption[] {
        return (): IDropdownOption[] => {
            this.setMenuClass(assessment);
            const actions: any[] = [];
            if (this.Permission.canShow("ProjectsViewHistory")) {
                actions.push(this.getAssessmentActivityAction(assessment));
            }
            actions.push(this.getExportPdfAction(assessment));
            if (this.Permission.canShow("AssessmentViewRelatedAssessments")) {
                actions.push(this.getViewRelatedAction(assessment));
            }
            if ((assessment.status !== AssessmentStatus.Completed) && this.Permission.canShow("ResendLinkToAssessment") && (assessment.respondentIds.toUpperCase() !== this.currentUser.Id.toUpperCase())) {
                actions.push(this.getResendLinkToAssessmentAction(assessment));
            }
            if (this.Permission.canShow("AssessmentShare")) {
                actions.push(this.getShareAssessmentAction(assessment.assessmentId));
            }
            if (this.Permission.canShow("AssessmentEditDetails")) {
                actions.push(this.getEditAssessmentAction(assessment));
            }
            if (this.Permission.canShow("AssessmentCopy")) {
                actions.push(this.getCopyAssessmentAction(assessment));
            }
            if (this.permissions.assessmentDelete) {
                actions.push(this.getDeleteAction(assessment));
            }
            return actions;
        };
    }

    setMenuClass(assessment: IAssessmentTableRow): void | undefined {
        this.menuClass = "actions-table__context-list";
        const rowIndex: number = findIndex(this.assessments, (item: IAssessmentTableRow): boolean => assessment.assessmentId === item.assessmentId);
        if (rowIndex > (this.assessments.length + 1) / 2) this.menuClass += " actions-table__context-list--above";
    }

    getViewRelatedAction(assessment: IAssessmentTableRow): IDropdownOption {
        return {
            text: this.translatePipe.transform("ViewRelatedAssessments"),
            action: (): void => {
                this.otModalService
                    .create(
                        ViewRelatedAssessmentModalComponent,
                        {
                            isComponent: true,
                            size: this.size,
                        },
                        assessment.assessmentId,
                    ).pipe(
                        takeUntil(this.destroy$),
                    );
            },
        };
    }

    getDeleteAction(assessment: IAssessmentTableRow): IDropdownOption {
        return {
            text: this.translatePipe.transform("Delete"),
            action: (): void => {
                this.deleteAssessment.emit(assessment.assessmentId);
            },
            identifier: "AssessmentListDeleteAssessmentAction",
        };
    }

    getExportPdfAction(assessment: IAssessmentTableRow): IDropdownOption {
        return {
            text: this.translatePipe.transform("ExportAsPDF"),
            action: (): void => {
                this.otModalService.create(
                    OTNotificationModalComponent,
                    { isComponent: true },
                    { modalTitle: "ReportDownload", confirmationText: this.translatePipe.transform("ReportIsGeneratedCheckTasksBar") },
                );
                if (this.Permission.canShow("SetDefaultAssessmentPDF")) {
                    this.reportTemplateExportService.assessmentsViewExport(assessment, this.defaultExportLayout);
                } else {
                    const exportBody: IAssessmentExportModel = {
                        name: assessment.name,
                        entityId: assessment.assessmentId,
                        reportExportFormat: "Pdf",
                        reportEntityType: 10,
                        reportExportLayoutInfo: {},
                    };
                    this.AssessmentTemplateAPI.exportAsPdf(exportBody);
                }
            },
            identifier: "AssessmentListExportAssessmentAction",
        };
    }

    getAssessmentActivityAction(assessment: IAssessmentTableRow): IDropdownOption {
        return {
            text: this.translatePipe.transform("ViewActivity"),
            action: (): void => {
                this.otModalService
                    .create(
                        AAActivityModalComponent,
                        {
                            isComponent: true,
                            size: this.size,
                        },
                        assessment,
                    ).pipe(
                        take(1),
                    ).subscribe(() => {
                        this.taskPollingService.startPolling();
                    });
            },
            identifier: "AssessmentListAssessmentActivityAction",
        };
    }

    getResendLinkToAssessmentAction(assessment: IAssessmentTableRow): IDropdownOption {
        return {
            text: this.translatePipe.transform("ResendLink"),
            action: (): void => {
                this.AssessmentTemplateAPI.getResendLinkToAssessment(assessment.assessmentId)
                    .then((response: IProtocolResponse<boolean>): void => {
                        if (response.result) {
                            this.notificationService.alertSuccess(this.translatePipe.transform("Success"), this.translatePipe.transform("EmailSent"));
                        } else if (response.status === 423 && response.errors.code === "ERROR_ASSESSMENT-V2_ASSESSMENT_LOCKED") {
                            this.notificationService.alertError(this.translatePipe.transform("Error"), this.translatePipe.transform("AssessmentLockedForPushingTemplate"));
                        }
                    });
            },
            identifier: "AssessmentResendLinkToAssessmentAction",
        };
    }

    getCopyAssessmentAction(row: IAssessmentTableRow): IDropdownOption {
        return {
            text: this.translatePipe.transform("Copy"),
            action: (): void => {
                this.stateService.go("zen.app.pia.module.assessment.copy-assessment", { assessmentId: row.assessmentId });
            },
            identifier: "AssessmentCopyAssessmentAction",
        };
    }

    getEditAssessmentAction(row: IAssessmentTableRow): IDropdownOption {
        return {
            text: this.translatePipe.transform("EditDetails"),
            action: (): void => {
                this.editAssessment.emit(row.assessmentId);
            },
            identifier: "AssessmentEditAssessmentAction",
        };
    }

    private getShareAssessmentAction(assessmentId: string): IDropdownOption {
        return {
            text: this.translatePipe.transform("Share"),
            action: (): void => {
                this.shareAssessmentService.getAllSharedAssessmentUsers(assessmentId)
                    .then((response: IProtocolResponse<ISharedAssessmentUsersContractById>): void => {
                        if (response.result) {
                            const usersToWhichAssessmentsShared: ISharedAssessmentUsersContractById = response.data;
                            const modalData: IShareAssessmentModalData = {
                                assessmentId,
                                usersToWhichAssessmentsShared,
                            };
                            this.modalService.setModalData(modalData);
                            this.modalService.openModal("downgradeShareAssessmentModal");
                        }
                    });
            },
            identifier: "AssessementShareAssessmentDropdownAction",
        };
    }
}
