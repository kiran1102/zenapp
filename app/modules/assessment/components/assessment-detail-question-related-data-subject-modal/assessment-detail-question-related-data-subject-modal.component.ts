// Rxjs
import { startWith } from "rxjs/operators";
// 3rd Party
import {
    forEach,
    uniqBy,
    isUndefined,
    some,
    map as lodashMap,
    find,
    remove,
} from "lodash";
import {
    Component,
    Inject,
    OnInit,
} from "@angular/core";

// Tokens
import { StoreToken } from "tokens/redux-store.token";

// Redux
import observableFromStore from "oneRedux/utils/observable-from-store";
import { getModalData } from "oneRedux/reducers/modal.reducer";

// Interfaces
import {
    IStore,
    IStoreState,
} from "interfaces/redux.interface";
import {
    IDataCategory,
} from "interfaces/inventory.interface";
import {
    IStringMap,
    INameId,
} from "interfaces/generic.interface";
import {
    IPersonalDataUpdateResponseMap,
    IPersonalDataElement,
} from "interfaces/assessment/question-response-v2.interface";
import { ISaveSectionResponsesContract } from "interfaces/assessment/assessment-api.interface";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

// Services
import { ModalService } from "sharedServices/modal.service";
import { InventoryRelatedService } from "modules/inventory/services/adapter/inventory-related.service";

// Const and Enums
import {
    getQuestionPersonalDataUpdateResponseMap,
    getAssessmentDetailSelectedSectionId,
    getAssessmentDetailAssessmentId,
} from "modules/assessment/reducers/assessment-detail.reducer";
import { AaDetailActionService } from "modules/assessment/services/action/aa-detail-action.service";

export interface IRelatedDataSubjectModalData {
    currentOrg: INameId<string>;
    dataSubjectCategoryId: string;
    dataSubjectId: string;
    isEditMode: boolean;
    modalTitle: string;
    questionId: string;
    callback?: () => void;
}

@Component({
    selector: "assessment-detail-question-relate-data-subject-modal",
    templateUrl: "./assessment-detail-question-related-data-subject-modal.component.html",
})
export class AssessmentDetailRelatedDataSubjectModalComponent implements OnInit {
    modalTitle: string;
    subjectTitle: string;
    isEditMode = false;
    submitButtonText: string;
    cancelButtonText: string;
    selectedSubjectType: IPersonalDataElement;
    selectedCategory: IDataCategory;
    currentlySelectedElements: IPersonalDataUpdateResponseMap[];
    elementList: IPersonalDataElement[] = [];
    currentElements: IPersonalDataElement[];
    elementMap: IStringMap<boolean> = {};
    isSaving = false;
    allSelected = false;
    dataSubjectTypeList: IPersonalDataElement[] = [];
    dataCategoryList: IDataCategory[] = [];
    recordInventoryType: number;
    orgGroupId: string;

    private modalData: IRelatedDataSubjectModalData;

    constructor(
        @Inject(StoreToken) public store: IStore,
        private translatePipe: TranslatePipe,
        public modalService: ModalService,
        private inventoryRelatedService: InventoryRelatedService,
        private assessmentDetailAction: AaDetailActionService,
    ) { }

    ngOnInit(): void {
        this.defaultCategory();
        observableFromStore(this.store).pipe(
            startWith(this.store.getState()),
        ).subscribe((state: IStoreState) => this.componentWillReceiveState(state));
        this.modalTitle = this.modalData.modalTitle || this.translatePipe.transform("ManageDataElements");
        this.submitButtonText = this.translatePipe.transform("Update");
        this.cancelButtonText = this.translatePipe.transform("Cancel");
        this.orgGroupId = this.modalData.currentOrg.id;
        this.isEditMode = this.modalData.isEditMode || false;
        this.getSelectedResponses();
        this.getDataSubjectTypeList();
    }

    getSelectedResponses(): void {
        if (this.modalData && this.modalData.questionId) {
            this.currentlySelectedElements = getQuestionPersonalDataUpdateResponseMap(this.modalData.questionId)(this.store.getState());
        }
    }

    getDataSubjectTypeList(): void {
        this.inventoryRelatedService.getDataSubjectList(this.orgGroupId).then((response: IPersonalDataElement[]) => {
            if (response && !response.length) return;
            this.dataSubjectTypeList = this.setTranslatedValues(response);
            const selectedDataSubject: IPersonalDataElement = find(this.dataSubjectTypeList, (cell: IPersonalDataElement) => cell.valueId === this.modalData.dataSubjectId)
                || this.dataSubjectTypeList[0]
                || null;

            if (selectedDataSubject) {
                this.selectedSubjectType = selectedDataSubject;
                this.subjectTitle = this.selectedSubjectType.displayValue;
                this.getDataCategoryList(this.selectedSubjectType.valueId);
            }
        });
    }

    getDataCategoryList(subjectId: string): void {
        this.inventoryRelatedService.getDataCategoryList(subjectId, this.orgGroupId).then((response: IDataCategory[]): void => {
            if (response && !response.length) return;

            this.dataCategoryList = this.setDataCategoryTranslatedValues(response);
            this.setElementMapForCategory(this.dataCategoryList);
            this.updateResponses();
            const selectedCategory: IDataCategory = find(this.dataCategoryList,
                (cell: IDataCategory) => cell.categoryId === this.modalData.dataSubjectCategoryId)
                || this.dataCategoryList[0]
                || null;

            if (selectedCategory) {
                this.selectCategory(selectedCategory);
            }
        });
    }

    updateResponses(): void {
        const removeList: string[] = [];
        this.currentlySelectedElements = lodashMap(this.currentlySelectedElements, (response: IPersonalDataUpdateResponseMap) => {
            let updatedElement: IPersonalDataUpdateResponseMap = null;
            let elementExists = false;
            forEach(this.dataCategoryList, (items: IDataCategory) => {
                forEach(items.elements, (element: IPersonalDataElement) => {
                    if (response.DATA_ELEMENTS && response.DATA_ELEMENTS.id === element.valueId) {
                        updatedElement = {
                            DATA_SUBJECTS: { id: this.selectedSubjectType.valueId, name: this.selectedSubjectType.valueKey },
                            DATA_CATEGORIES: { id: items.categoryId, name: items.categoryKey },
                            DATA_ELEMENTS: { id: element.valueId, name: element.value },
                        };
                        elementExists = true;
                    }
                    return element;
                });
                return items;
            });

            if (elementExists) {
                response = updatedElement;
            } else if (response.DATA_ELEMENTS && response.DATA_SUBJECTS.id === this.selectedSubjectType.valueId) {
                removeList.push(response.DATA_ELEMENTS.id);
            }
            return response;
        });
        this.removeElements(removeList);
    }

    removeElements(removeList: string[]) {
        forEach(removeList, (elementId: string): void => {
            remove(this.currentlySelectedElements, (map: IPersonalDataUpdateResponseMap) =>
                map.DATA_ELEMENTS && map.DATA_ELEMENTS.id === elementId);
        });
    }

    selectCategory(category: IDataCategory): void {
        this.selectedCategory = category;
        this.currentElements = category.elements;
        this.allElementsSelected();
    }

    setElementMapForCategory(categoryList: IDataCategory[]): void {
        forEach(categoryList, (category: IDataCategory): void => {
            this.elementList = uniqBy([...this.elementList, ...category.elements], "valueId");
            forEach(category.elements, (element: IPersonalDataElement): void => {
                if (isUndefined(this.elementMap[element.valueId])) {
                    this.elementMap[element.valueId] = some(this.currentlySelectedElements,
                        (selection: IPersonalDataUpdateResponseMap) => selection.DATA_ELEMENTS && (selection.DATA_ELEMENTS.id === element.valueId));
                    }
                });
            });
    }

    selectSubjectType(selection: IPersonalDataElement): void {
        this.defaultCategory();
        this.selectedSubjectType = selection;
        this.subjectTitle = this.selectedSubjectType.displayValue;
        this.getDataCategoryList(this.selectedSubjectType.valueId);
    }

    allElementsSelected(): void {
        let allCurrentElementsSelected = true;
        for (let i = 0; i < this.currentElements.length; i++) {
            if (!this.elementMap[this.currentElements[i].valueId]) {
                allCurrentElementsSelected = false;
                break;
            }
        }
        this.allSelected = allCurrentElementsSelected;
    }

    selectAll(): void {
        const checkStatus: boolean = !this.allSelected;
        forEach(this.currentElements, (element: IPersonalDataElement): void => {
            this.elementMap[element.valueId] = checkStatus;
            this.updateSelectedResponses(element.valueId, checkStatus);
        });
        this.allSelected = checkStatus;
    }

    defaultCategory(): void {
        this.dataCategoryList = [];
        this.selectedCategory = {
            categoryId: "",
            categoryValue: "",
            displayCategoryValue: "",
            elements: [],
        };
    }

    toggleElement(elementId: string): void {
        this.elementMap[elementId] = !this.elementMap[elementId];
        this.updateSelectedResponses(elementId, this.elementMap[elementId]);
        this.allElementsSelected();
    }

    updateSelectedResponses(elementId: string, addToSelectedList: boolean): void {
        const existingSelection = some(this.currentlySelectedElements, (selection: IPersonalDataUpdateResponseMap) =>
            selection.DATA_ELEMENTS && (selection.DATA_ELEMENTS.id === elementId));
        if (addToSelectedList && !existingSelection) {
            this.removeEmptySubjectEntry(this.selectedSubjectType.valueId);
            this.currentlySelectedElements.push({
                DATA_SUBJECTS: { id: this.selectedSubjectType.valueId, name: this.selectedSubjectType.valueKey },
                DATA_CATEGORIES: { id: this.selectedCategory.categoryId, name: this.selectedCategory.categoryKey },
                DATA_ELEMENTS: { id: elementId, name: find(this.selectedCategory.elements, { valueId: elementId }).value },
            });
        } else if (!addToSelectedList) {
            remove(this.currentlySelectedElements, (map: IPersonalDataUpdateResponseMap) =>
                map.DATA_ELEMENTS && map.DATA_ELEMENTS.id === elementId);
        }
    }

    removeEmptySubjectEntry(subjectId: string): void {
        remove(this.currentlySelectedElements, (response) =>
            response.DATA_SUBJECTS.id === subjectId &&
            response.DATA_CATEGORIES === null &&
            response.DATA_ELEMENTS === null,
        );
    }

    updateSelectedSubject(): void {
        const existingSubjectSelection = some(this.currentlySelectedElements, (selection: IPersonalDataUpdateResponseMap) =>
            selection.DATA_SUBJECTS.id === this.selectedSubjectType.valueId,
        );
        if (!existingSubjectSelection) {
            this.currentlySelectedElements.push({
                DATA_SUBJECTS: { id: this.selectedSubjectType.valueId, name: this.selectedSubjectType.valueKey },
                DATA_CATEGORIES: null,
                DATA_ELEMENTS: null,
            });
        }
    }

    closeModal(): void {
        this.modalService.closeModal();
        this.modalService.clearModalData();
    }

    saveDSE(): void {
        this.isSaving = true;
        const assessmentId = getAssessmentDetailAssessmentId(this.store.getState());
        const sectionId = getAssessmentDetailSelectedSectionId(this.store.getState());
        this.updateSelectedSubject();
        const responseForSave: ISaveSectionResponsesContract[] = [{
            assessmentId,
            parentAssessmentDetailId: null,
            sectionId,
            questionId: this.modalData.questionId,
            responses: this.assessmentDetailAction.formatQuestionResponses(this.currentlySelectedElements),
        }];
        this.assessmentDetailAction.saveDSEResponses(responseForSave).then((response: boolean): void => {
            if (response) {
                if (this.modalData.callback) {
                    this.modalData.callback();
                }
                this.closeModal();
            } else {
                this.isSaving = false;
            }
        });
    }

    private setTranslatedValues(list: IPersonalDataElement[]): IPersonalDataElement[] {
        list.map((item) => {
            item.displayValue = item.valueKey ? this.translatePipe.transform(item.valueKey) : item.value;
        });
        return list;
    }

    private setDataCategoryTranslatedValues(list: IDataCategory[]): IDataCategory[] {
        list.map((item) => {
            item.displayCategoryValue = item.categoryKey ? this.translatePipe.transform(item.categoryKey) : item.categoryValue;
        });
        return list;
    }

    private componentWillReceiveState(state: IStoreState): void {
        this.modalData = getModalData(state);
    }
}
