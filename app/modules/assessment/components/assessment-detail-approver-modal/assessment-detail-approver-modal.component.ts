import Utilities from "Utilities";
import { buildDefaultModalTemplate } from "modals/modal-template/modal-template";

// redux
import { getCurrentUser } from "oneRedux/reducers/user.reducer";
import { getModalData } from "oneRedux/reducers/modal.reducer";
import {
    getAssessmentDetailModel,
    getAssessmentDetailName,
} from "modules/assessment/reducers/assessment-detail.reducer";

// service
import { ModalService } from "sharedServices/modal.service";

// interface
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import { IAssessmentUpdateApproverRequest } from "interfaces/assessment/assessment-api.interface";
import { IOrgUserAdapted } from "interfaces/org-user.interface";
import { IStore } from "interfaces/redux.interface";
import { IStringMap } from "interfaces/generic.interface";
import { IUser } from "interfaces/user.interface";

// constant
import { AssessmentPermissions } from "constants/assessment.constants";

class AssessmentDetailApproverModalComponentController implements ng.IComponentController {
    private resolve: any;
    private close: any;
    private dismiss: any;

    approver: { id: string, name: string };
    approverPermissions: string[] = [AssessmentPermissions.AssessmentCanBeApprover];
    assessmentName: string;
    comment: string;
    currentUser: IUser;
    isSubmitting = false;
    modalTitle: string;
    orgGroupId: string;
    promiseToResolve: (...args: any[]) => any;
    selectedApprover: IOrgUserAdapted;
    translate: Function;

    static $inject: string[] = [
        "$rootScope",
        "store",
        "ModalService",
    ];

    constructor(
        private readonly $rootScope: IExtendedRootScopeService,
        private readonly store: IStore,
        private readonly ModalService: ModalService,
    ) {
        this.translate = $rootScope.t;
    }

    $onInit(): void {
        const appState = this.store.getState();
        this.assessmentName = getAssessmentDetailName(appState);
        this.currentUser = getCurrentUser(appState);
        this.modalTitle = getModalData(appState).modalTitle;
        this.orgGroupId = getModalData(appState).orgGroupId;
        this.promiseToResolve = getModalData(appState).promiseToResolve;
        this.approver = getAssessmentDetailModel(appState).approvers[0];
    }

    approverSelected(selection, isValid): void {
        if (isValid) {
            this.selectedApprover = selection;
        } else {
            this.selectedApprover = null;
        }
    }

    closeModal(): void {
        this.ModalService.closeModal();
        this.ModalService.clearModalData();
    }

    handleInputChange(value: string): void {
        this.comment = value;
    }

    submitModal(): void {
        this.isSubmitting = true;
        const payload: IAssessmentUpdateApproverRequest = {
            approverId: this.selectedApprover.Id,
            approverName: this.selectedApprover.FullName,
            comment: this.comment,
        };
        this.promiseToResolve(payload)
            .then((success: boolean): ng.IPromise<void> | boolean => {
                if (success) {
                    this.ModalService.closeModal();
                    this.ModalService.clearModalData();
                    return true;
                }
                this.isSubmitting = false;
                return false;
            });
    }
}

const assessmentDetailApproverModalTemplate: IStringMap<string> = {
    template: `
            <section class="flex flex-full-height padding-all-2">
                <div class="text-center" ng-if="$ctrl.approver">
                    <span ng-if="$ctrl.approver.id !== $ctrl.currentUser.Id">
                        {{::$ctrl.translate("ProjectIsAssignedToAssigneeName", {ProjectName : $ctrl.assessmentName, AssigneeName: $ctrl.approver.name}) }}
                    </span>
                    <span ng-if="$ctrl.approver.id === $ctrl.currentUser.Id">
                        {{::$ctrl.translate("ProjectIsAssignedToAssigneeNameMe", {ProjectName : $ctrl.assessmentName, AssigneeName: $ctrl.approver.name}) }}
                    </span>
                </div>

                <div class="text-center" ng-if="!$ctrl.approver">
                    {{::$ctrl.translate("ProjectIsNotCurrentlyAssignedToAnyone", {ProjectName : $ctrl.assessmentName})}}
                </div>

                <!-- re-assign approver dropdown -->
                <org-user-dropdown
                    label="{{::$ctrl.translate('ReAssignTo')}}"
                    on-select="$ctrl.approverSelected(selection, isValid)"
                    org-id="$ctrl.orgGroupId"
                    permission-keys="$ctrl.approverPermissions"
                    traversal="'UP'"
                    wrapper-class="margin-top-bottom-2 form-group"
                >
                </org-user-dropdown>

                <!--comments to send email to the new approver -->
                <one-label-content
                    body-class="full-width"
                    name="{{::$ctrl.translate('Comments')}}"
                    wrapper-class="margin-bottom-2 form-group"
                >
                    <one-textarea
                        identifier="AssessmentApproverModalCommentTextArea"
                        input-changed="$ctrl.handleInputChange(value)"
                        input-text="{{$ctrl.comment}}"
                        placeholder="{{::$ctrl.translate('ApproverCommentPlaceholder')}}"
                    >
                    </one-textarea>
                </one-label-content>

                <!-- Footer -->
                <footer class="row-horizontal-center margin-top-2">
                    <one-button
                        identifier="AssessmentApproverModalReassignButton"
                        button-click="$ctrl.submitModal()"
                        enable-loading="true"
                        is-loading="$ctrl.isSubmitting"
                        is-disabled="!$ctrl.selectedApprover || $ctrl.isSubmitting"
                        text="{{::$ctrl.translate('ReAssign')}}"
                        type="primary"
                    ></one-button>
                </footer>
            </section>
        `,
};

export const assessmentDetailApproverModalComponent: ng.IComponentOptions = {
    controller: AssessmentDetailApproverModalComponentController,
    bindings: {
        resolve: "<",
        close: "&",
        dismiss: "&",
    },
    template: buildDefaultModalTemplate(assessmentDetailApproverModalTemplate.template),
};
