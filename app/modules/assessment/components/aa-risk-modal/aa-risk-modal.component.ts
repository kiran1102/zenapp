// Angular
import {
    Component,
    Inject,
    OnInit,
} from "@angular/core";

// External libraries
import {
    find,
    isNull,
} from "lodash";
import {
    Observable,
    Subject,
    Subscription,
} from "rxjs";
import {
    filter,
    first,
    map,
    startWith,
    takeUntil,
    withLatestFrom,
} from "rxjs/operators";

// Interfaces
import { IAssessmentModel } from "interfaces/assessment/assessment-model.interface";
import { IOrgUserAdapted } from "interfaces/org-user.interface";
import {
    IRiskDetails,
    IRisk,
    IRiskCreateRequestModel,
    IRiskMetadata,
    IRiskReference,
    IRiskCategoryBasicInfo,
    IRiskCreateRequest,
} from "interfaces/risk.interface";
import {
    IStore,
    IStoreState,
} from "interfaces/redux.interface";
import { IStringMap } from "interfaces/generic.interface";
import { IUser } from "interfaces/user.interface";
import {
    IRiskSetting,
    IRiskHeatmap,
    IRiskLevel,
} from "interfaces/risks-settings.interface";
import { IOtOrgUserOutput } from "sharedModules/components/ot-org-user/ot-org-user.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";

// Redux
import { getModalData } from "oneRedux/reducers/modal.reducer";
import { getCurrentUser } from "oneRedux/reducers/user.reducer";
import { getTempRiskById } from "oneRedux/reducers/risk.reducer";
import observableFromStore from "oneRedux/utils/observable-from-store";
import {
    AssessmentDetailAction,
    getAssessmentDetailSelectedSectionId,
    getAssessmentDetailModel,
    getAssessmentDetailAssessmentId,
} from "modules/assessment/reducers/assessment-detail.reducer";

// Enums and Constants
import {
    RiskReferenceTypes,
    RiskCharacterLimit,
} from "enums/risk.enum";
import { RiskV2ReferenceTypes } from "enums/riskV2.enum";
import { AACollaborationCountsTypes } from "modules/assessment/enums/aa-detail-header.enum";
import { RiskCategoryStatus } from "modules/risks/risk-category/risk-category.constants";

// Redux
import {
    areAllPropsDefined,
    isAnyPropChanged,
} from "oneRedux/utils/utils";
import { StoreToken } from "tokens/redux-store.token";

// Service
import { ModalService } from "sharedServices/modal.service";
import { AssessmentRiskActionService } from "modules/assessment/services/action/assessment-risk-action.service";
import { AssessmentRiskViewLogicService } from "modules/assessment/services/view-logic/assessment-risk-view-logic.service";
import { TimeStamp } from "oneServices/time-stamp.service";
import { RiskAPIService } from "oneServices/api/risk-api.service";
import { AaRiskControlActionService } from "modules/assessment/services/action/aa-risk-control-link-controls-action.service";
import { NotificationService } from "modules/shared/services/provider/notification.service";
import { AaDetailActionService } from "modules/assessment/services/action/aa-detail-action.service";
import { AssessmentListApiService } from "modules/assessment/services/api/assessment-list-api.service";
import { AaCollaborationPaneCountsService } from "modules/assessment/services/messaging/aa-section-detail-header-helper.service";
import { Permissions } from "sharedModules/services/helper/permissions.service";
import { LookupService } from "sharedModules/services/helper/lookup.service.ts";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

@Component({
    selector: "aa-risk-modal",
    templateUrl: "./aa-risk-modal.component.html",
})

export class AARiskModalComponent implements OnInit {

    assessmentId: string;
    canSubmit = false;
    currentUser: IUser;
    dataLoaded = false;
    disableUntilDate = this.timeStamp.getDisableUntilDate();
    isSubmitting: boolean;
    questionId: string;
    risk: IRiskCreateRequest;
    riskSub: Subscription;
    sectionId: string;
    userList: IOrgUserAdapted[];
    viewState: IStringMap<boolean>;
    riskCharLimit: number;
    riskOwner: string;
    heatmapEnabled: boolean;
    format = this.timeStamp.getDatePickerDateFormat();
    deadline = null;
    riskLevels: IRiskLevel[];
    categoryList: IRiskCategoryBasicInfo[];
    filteredCategoryOptions: IRiskCategoryBasicInfo[];
    selectedCategories: IRiskCategoryBasicInfo[] = [];

    private restrictAssessmentCompletionWithOpenRisk: boolean;
    private destroy$ = new Subject();

    constructor(
        @Inject(StoreToken) private store: IStore,
        private readonly modalService: ModalService,
        private readonly AssessmentRiskViewLogic: AssessmentRiskViewLogicService,
        private readonly AssessmentRiskAction: AssessmentRiskActionService,
        private readonly timeStamp: TimeStamp,
        private readonly riskApi: RiskAPIService,
        private readonly aaRiskControlActionService: AaRiskControlActionService,
        private aaCollaborationPaneCountsService: AaCollaborationPaneCountsService,
        private notificationService: NotificationService,
        private translatePipe: TranslatePipe,
        private assessmentListAPI: AssessmentListApiService,
        private assessmentDetailAction: AaDetailActionService,
        private lookupService: LookupService,
        private permissions: Permissions,
    ) {
    }

    ngOnInit(): void {
        const appState = this.store.getState();
        const riskId = getModalData(appState).riskId;
        this.risk = getTempRiskById(riskId)(appState) as unknown as IRiskDetails;
        this.risk.orgGroupId = getAssessmentDetailModel(this.store.getState()).orgGroup.id;
        this.risk.deadline = this.risk.deadline ? new Date(this.timeStamp.getLocalDate(this.risk.deadline.toString())) : null;
        this.currentUser = getCurrentUser(appState);
        this.assessmentId = find(this.risk.references, (reference: IRiskReference) => reference.typeId === RiskReferenceTypes.Assessment || reference.type === RiskV2ReferenceTypes[RiskV2ReferenceTypes.ASSESSMENT]).id;
        const sectionReference = find(this.risk.references, (reference: IRiskReference) => reference.typeId === RiskReferenceTypes.Section);
        this.sectionId = sectionReference ? sectionReference.id : getAssessmentDetailSelectedSectionId(appState);
        this.questionId = find(this.risk.references, (reference: IRiskReference) => reference.typeId === RiskReferenceTypes.Question || reference.type === RiskV2ReferenceTypes[RiskV2ReferenceTypes.QUESTION]).id;
        const assessmentModel: IAssessmentModel = getModalData(appState).assessmentModel;
        const respondentId: string = assessmentModel.respondents[0].id;
        this.riskCharLimit = RiskCharacterLimit.Limit;

        this.riskApi.getRiskLevel()
            .pipe(
                takeUntil(this.destroy$),
            )
            .subscribe((riskLevels: IRiskLevel[]) => {
                if (riskLevels) {
                    this.riskLevels = riskLevels;
                }
            });

        this.riskApi.getRiskSetting()
            .pipe(
                takeUntil(this.destroy$),
            )
            .subscribe((settings: IRiskSetting) => {
                if (settings) {
                    this.heatmapEnabled = settings.enableRiskHeatMap;
                }
            });

        this.AssessmentRiskAction.getRiskOwners(this.risk)
            .then((users: IOrgUserAdapted[]) => {
                this.userList = users;

                // Setting respondent as a default risk owner.
                if (!this.risk.riskOwnerId ) {
                    const userModel: IOrgUserAdapted = find(this.userList, (user: IOrgUserAdapted) => user.Id === respondentId);
                    if (userModel) {
                        this.riskOwner = userModel.Id;
                        this.AssessmentRiskAction.updateTempRisk(this.risk.id, { riskOwnerId: respondentId });
                    }
                }

                const store$: Observable<IStoreState> = observableFromStore(this.store).pipe(
                    startWith(this.store.getState()),
                );

                const riskModel$: Observable<IRisk | IRiskDetails> = store$
                    .pipe(
                        map(getTempRiskById(riskId)),
                        filter((data: IRiskDetails): boolean => Boolean(data)),
                    );

                const origRiskModel$: Observable<IRisk | IRiskDetails> = riskModel$.pipe(first());

                this.riskSub = riskModel$
                    .pipe(
                        withLatestFrom(origRiskModel$),
                        takeUntil(this.destroy$),
                    )
                    .subscribe(([newModel, oldModel]): void => this.componentWillReceiveState(newModel as unknown as IRiskDetails, oldModel as unknown as IRiskDetails));
                this.dataLoaded = true;
            });
        this.checkForOpenRiskRestriction();
        if (this.permissions.canShow("RiskViewCategory")) {
            this.initializeCategories();
        }
    }

    handleInputChange(payload: any, prop: string): void {
        if (prop === "deadline") {
            this.deadline = payload.formatted;
            this.risk.deadline = payload.jsdate ? payload.jsdate.toISOString() : null;
            this.AssessmentRiskAction.updateTempRisk(this.risk.id, { [prop]: this.risk.deadline });
        } else {
            this.AssessmentRiskAction.updateTempRisk(this.risk.id, { [prop]: payload });
        }
    }

    handleRiskOwnerChange(option: IOtOrgUserOutput): void {
        // we have to send null to backend if we are removing a risk owner.
        let riskOwnerId;
        if (!isNull(option.selection) && option.selection.Id) {
            // Since the component is returning Id and FullName same for External/Invited user Id
            if (option.selection.Id === option.selection.FullName) {
                option.selection.Id = null;
            }
            this.risk.riskOwner = option.selection.FullName;
            this.riskOwner = option.selection.Id ? option.selection.Id : option.selection.FullName;
            this.risk.riskOwnerId = option.selection.Id;
            riskOwnerId = option.selection.Id;
        } else {
            this.riskOwner = null;
            this.risk.riskOwner = null;
            riskOwnerId = null;
        }
        this.AssessmentRiskAction.updateTempRisk(this.risk.id, { riskOwnerId });
    }

    $onDestroy(): void {
        this.destroy$.next();
        this.destroy$.complete();
    }

    closeModal = (): void => {
        this.AssessmentRiskAction.deleteTempRisk(this.risk.id);
        this.modalService.closeModal();
        this.modalService.clearModalData();
    }

    onRiskHeatmapSelected(riskSelected: IRiskHeatmap): void {
        this.AssessmentRiskAction.updateTempRisk(this.risk.id, {
            levelId: riskSelected.levelId,
            impactLevel: riskSelected.impactLevel,
            impactLevelId: riskSelected.impactLevelId,
            probabilityLevel: riskSelected.probabilityLevel,
            probabilityLevelId: riskSelected.probabilityLevelId,
            riskScore: riskSelected.riskScore,
        });
    }

    openLinkControlsModal(riskId: string) {
        this.aaRiskControlActionService.openAddRiskControlsModal(riskId)
            .pipe(takeUntil(this.destroy$))
            .subscribe((added: boolean) => {
                if (added) {
                    this.notificationService.alertSuccess(
                        this.translatePipe.transform("Success"),
                        this.translatePipe.transform("RiskControlAddedSuccessfully"));
                }
            });
    }

    onCategoryLookupInputChange({ key, value }) {
        if (key === "Enter") {
            this.selectedCategories.push(this.filteredCategoryOptions[0]);
            this.onModelValueChange({ currentValue: this.selectedCategories });
            return;
        }
        this.filteredCategoryOptions = this.lookupService.filterOptionsByInput(this.selectedCategories, this.categoryList, value, "id", "name");
    }

    onModelValueChange({currentValue}): void {
        this.selectedCategories = currentValue;
        this.filteredCategoryOptions = this.lookupService.filterOptionsBySelections(this.selectedCategories, this.categoryList, "id");
        this.risk.categoryIds = this.selectedCategories && this.selectedCategories.length ? this.selectedCategories.map((category) => category.id) : null;
    }

    private componentWillReceiveState(newModel: IRiskDetails, oldModel: IRiskDetails): void {
        const appState = this.store.getState();
        const currentUser = getCurrentUser(appState);
        const assessment = getModalData(appState).assessmentModel;
        this.viewState = this.AssessmentRiskViewLogic.calculateRiskStatePermissions(currentUser, assessment, oldModel);
        this.canSubmit = this.validateRiskModal(oldModel, newModel);
        this.risk = newModel;
    }

    private validateRiskModal(oldRiskModel: IRiskDetails, newRiskModel: IRiskDetails): boolean {

        const propsThatMustBeDefined: string[] = ["levelId"];
        const propsThatCanChange: string[] = [...propsThatMustBeDefined, "probabilityLevelId", "impactLevelId", "deadline", "riskOwnerId", "description", "recommendation"];

        return areAllPropsDefined(propsThatMustBeDefined)(oldRiskModel, newRiskModel)
            && isAnyPropChanged(propsThatCanChange)(oldRiskModel, newRiskModel);
    }

    private submitModal(isAddControls = false): void {
        this.isSubmitting = true;
        const riskCreateModel: IRiskCreateRequestModel = {
            riskCreateRequest: this.risk,
            sectionId: this.sectionId,
            questionId: this.questionId,
        };

        this.AssessmentRiskAction.createRisk(riskCreateModel, this.assessmentId).then((newRisk: IRiskMetadata): ng.IPromise<void> | ng.IPromise<any> | boolean => {
            if (newRisk && newRisk.riskId) {
                return this.AssessmentRiskAction.reloadRiskModel(newRisk.riskId).then((res: IRiskDetails): void => {
                    this.modalService.handleModalCallback(newRisk, this.risk, this.sectionId);
                    this.clearModel(this.risk.id);
                    if (isAddControls)this.openLinkControlsModal(newRisk.riskId);
                    this.updateSection();
                    this.aaCollaborationPaneCountsService.getCollaborationPaneCounts(this.assessmentId, AACollaborationCountsTypes.Risks);
                });
            }
            this.isSubmitting = false;
            return false;
        });
    }

    private checkForOpenRiskRestriction(): any {
        this.assessmentListAPI.getSspRestrictViewSetting()
            .then((response) => {
                if (response) {
                    this.restrictAssessmentCompletionWithOpenRisk = response.data && response.data.restrictAssessmentCompletionWithOpenRisk;
                }
            });
    }

    private updateSection() {
        if (this.restrictAssessmentCompletionWithOpenRisk) {
            const assessmentId = getAssessmentDetailAssessmentId(this.store.getState());
            this.assessmentDetailAction.fetchCurrentSection(assessmentId).then((assessmentModel: IAssessmentModel): void => {
                if (assessmentModel) {
                    this.store.dispatch({ type: AssessmentDetailAction.UPDATE_ASSESSMENT_ALL, assessmentModel });
                }
            });
        }
    }

    private initializeCategories() {
        this.riskApi.fetchRiskCategoryListByStatus(RiskCategoryStatus.UNARCHIVED)
            .pipe(takeUntil(this.destroy$))
            .subscribe((response: IProtocolResponse<IRiskCategoryBasicInfo[]>) => {
                if (response.result) {
                    this.categoryList = response.data;
                    this.filteredCategoryOptions = response.data;
                }
            });
    }

    private clearModel = (riskId: string): void => {
        this.AssessmentRiskAction.deleteTempRisk(riskId);
        this.modalService.closeModal();
        this.modalService.clearModalData();
    }
}
