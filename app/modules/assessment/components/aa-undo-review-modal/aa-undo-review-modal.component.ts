// Angular
import {
    Component,
    Inject,
    OnInit,
} from "@angular/core";

// Rxjs
import { Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";

// Interfaces
import { IStore } from "interfaces/redux.interface";
import { AssessmentFinishReviewActions } from "constants/assessment.constants";

// Redux
import { getAssessmentDetailModel } from "modules/assessment/reducers/assessment-detail.reducer";
import { getCurrentUser } from "oneRedux/reducers/user.reducer";
import { StoreToken } from "tokens/redux-store.token";

// Service
import { AaDetailActionService } from "modules/assessment/services/action/aa-detail-action.service";

@Component({
    selector: "aa-undo-review-modal",
    templateUrl: "./aa-undo-review-modal.component.html",
})

export class AAUndoReviewModalComponent implements OnInit {

    otModalCloseEvent: Subject<boolean>;
    userAssessmentResult: string;
    isUndoReviewing = false;
    assessmentFinishReviewActions = AssessmentFinishReviewActions;

    private destroy$ = new Subject();

    constructor(
        @Inject(StoreToken) private store: IStore,
        private assessmentDetailAction: AaDetailActionService,
    ) {}

    ngOnInit(): void {
        const state = this.store.getState();
        const assessmentModel = getAssessmentDetailModel(state);
        this.userAssessmentResult = assessmentModel.approvers.find((user) => user.id === getCurrentUser(state).Id).approvalState;
        this.assessmentDetailAction.undoReviewResult$.pipe(
            takeUntil(this.destroy$),
            ).subscribe((res: boolean) => {
                this.isUndoReviewing = false;
                if (res) {
                    this.closeUndoReviewModal();
                }
            });
    }

    ngOnDestroy(): void {
        this.destroy$.next();
        this.destroy$.complete();
        this.assessmentDetailAction.reset();
    }

    undoReviewAssessment(): void {
        this.isUndoReviewing = true;
        this.assessmentDetailAction.undoReviewAssessment();
    }

    closeUndoReviewModal(): void {
        this.otModalCloseEvent.next(false);
    }
}
