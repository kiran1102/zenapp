// Angular
import {
    Component,
    OnInit,
} from "@angular/core";

// Services
import { AAActivityStreamApiService } from "modules/assessment/services/api/aa-activity-stream-api.service";
import { AAQuestionActivityAdapterService } from "modules/assessment/services/adapter/aa-question-activity-adapter.service";

// Interfaces
import { IStringMap } from "interfaces/generic.interface";
import { IPageableOf } from "interfaces/pagination.interface";
import {
    IQuestionActivityContent,
    IQuestionActivityFormatted,
    IQuestionActivityStreamActivity,
    IQuestionActivityParams,
} from "interfaces/assessment/assessment-question-activity.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";
import { IQuestionActivityModalData } from "interfaces/assessment/assessment-question-activity.interface";

// 3rd Party
import { Subject } from "rxjs";

// Constants
import { AssessmentQuestionButtonKeys } from "constants/assessment.constants";

// Pipes
import { OtDatePipe } from "pipes/ot-date.pipe";
import { TranslatePipe } from "pipes/translate.pipe";

@Component({
    selector: "aa-question-activity-stream",
    templateUrl: "aa-question-activity-stream.component.html",
})
export class AAQuestionActivityStreamModalComponent implements OnInit {
    otModalData: IQuestionActivityModalData;
    otModalCloseEvent: Subject<null>;
    allItemsExpanded = true;
    expandedActivityMap: IStringMap<boolean> = {};
    formattedActivities: IQuestionActivityFormatted[] = [];
    hasMoreActivities = false;
    isLoading = true;
    sortIcon = "ot ot-sort-amount-desc";
    sortDirection = "desc";
    private assessmentId: string;
    private questionId: string;
    private questionActivityParams: IQuestionActivityParams;
    private activitiesCount: number;

    constructor(
        private aAActivityStreamApiService: AAActivityStreamApiService,
        private DatePipe: OtDatePipe,
        private translatePipe: TranslatePipe,
        private questionActivityAdapterService: AAQuestionActivityAdapterService,
    ) {
        this.questionActivityParams = {
            page: 0,
            size: 8,
            filter: "",
            sort: "createdDate,desc",
            text: "",
        };
    }

    ngOnInit() {
        this.assessmentId = this.otModalData.assessmentId;
        this.questionId = this.otModalData.questionId;
        this.aAActivityStreamApiService.getQuestionActivityStream(this.assessmentId, this.questionId, this.questionActivityParams).then((response: IProtocolResponse<IPageableOf<IQuestionActivityContent>>) => {
            if (!response) return;
            this.hasMoreActivities = !response.data.last;
            this.activitiesCount = response.data.numberOfElements;
            this.formattedActivities = this.formatActivities(response.data.content);
            this.isLoading = false;
        });
    }

    getMoreQuestionActivities() {
        this.questionActivityParams.page += 1;
        this.aAActivityStreamApiService.getQuestionActivityStream(this.assessmentId, this.questionId, this.questionActivityParams).then((response: IProtocolResponse<IPageableOf<IQuestionActivityContent>>) => {
            if (!response) return;
            this.hasMoreActivities = !response.data.last;
            this.activitiesCount += response.data.numberOfElements;
            this.formattedActivities = this.formatActivities(response.data.content, this.formattedActivities);
        });
    }

    reverseActivities() {
        this.isLoading = true;
        this.sortIcon = this.sortIcon.includes("desc") ? "ot ot-sort-amount-asc" : "ot ot-sort-amount-desc";
        this.sortDirection = this.sortIcon.includes("desc") ? "desc" : "asc";
        this.questionActivityParams = {
            ...this.questionActivityParams,
            size: this.activitiesCount,
            page: 0,
        };

        let activitySort = this.questionActivityParams.sort.split(",");
        activitySort.pop();
        activitySort = activitySort.concat(this.sortDirection);
        this.questionActivityParams.sort = activitySort.join();

        this.aAActivityStreamApiService.getQuestionActivityStream(this.assessmentId, this.questionId, this.questionActivityParams)
            .then((response: IProtocolResponse<IPageableOf<IQuestionActivityContent>>) => {
                if (!response) return;
                this.hasMoreActivities = !response.data.last;
                this.formattedActivities = this.formatActivities(response.data.content);
                this.isLoading = false;
            });
    }

    toggleActivity(activityId: string) {
        this.expandedActivityMap[activityId] = !this.expandedActivityMap[activityId];
        this.allItemsExpanded = this.checkIfAllExpanded() ? true : false;
    }

    expandAllToggle() {
        Object.keys(this.expandedActivityMap).forEach((key) => this.expandedActivityMap[key] = true);
        this.allItemsExpanded = true;
    }

    collapseAllToggle() {
        Object.keys(this.expandedActivityMap).forEach((key) => this.expandedActivityMap[key] = false);
        this.allItemsExpanded = false;
    }

    trackByFn(index: number): number {
        return index;
    }

    closeModal() {
        this.otModalCloseEvent.next();
    }

    private formatActivities(unformattedActivities: IQuestionActivityContent[], formattedActivities: IQuestionActivityFormatted[] = []): IQuestionActivityFormatted[] {
        unformattedActivities.map((activity: IQuestionActivityContent) => {
            formattedActivities.push({
                id: formattedActivities.length,
                title: this.translatePipe.transform("UserMadeChanges", { userName: activity.userName }),
                timeStamp: this.DatePipe.transform(activity.timeStamp, "dateTime"),
                icon: `ot ${this.questionActivityAdapterService.getIcon(activity.activities[0])}`,
                iconColor: "#FFFFFF",
                iconBackgroundColor: this.questionActivityAdapterService.getIconBackgroundColor(activity.activities[0]),
                changes: this.translateChanges(activity.activities),
            });
            this.expandedActivityMap[formattedActivities.length - 1] = true;
        });
        return formattedActivities;
    }

    private translateChanges(changes: IQuestionActivityStreamActivity[]): IQuestionActivityStreamActivity[] {
        changes.forEach((change: IQuestionActivityStreamActivity) => {
            if (change.oldValue === AssessmentQuestionButtonKeys.NotApplicable) {
                change.oldValue = this.translatePipe.transform("NotApplicable");
            }
            if (change.newValue === AssessmentQuestionButtonKeys.NotApplicable) {
                change.newValue = this.translatePipe.transform("NotApplicable");
            }
            if (change.oldValue === AssessmentQuestionButtonKeys.NotSure) {
                change.oldValue = this.translatePipe.transform("NotSure");
            }
            if (change.newValue === AssessmentQuestionButtonKeys.NotSure) {
                change.newValue = this.translatePipe.transform("NotSure");
            }
            if (change.oldValue === AssessmentQuestionButtonKeys.Yes) {
                change.oldValue = this.translatePipe.transform("Yes");
            }
            if (change.newValue === AssessmentQuestionButtonKeys.Yes) {
                change.newValue = this.translatePipe.transform("Yes");
            }
            if (change.oldValue === AssessmentQuestionButtonKeys.No) {
                change.oldValue = this.translatePipe.transform("No");
            }
            if (change.newValue === AssessmentQuestionButtonKeys.No) {
                change.newValue = this.translatePipe.transform("No");
            }
            if (change.newValue === "null") {
                change.newValue = "";
            }
            if (change.oldValue === "null") {
                change.oldValue = "";
            }
        });
        return changes;
    }

    private checkIfAllExpanded(): boolean {
        const keys = Object.keys(this.expandedActivityMap);

        if (keys.length === this.formattedActivities.length) {
            for (let i = 0; i < keys.length; i++) {
                if (!this.expandedActivityMap[keys[i]]) {
                    return false;
                }
            }
        } else {
            return false;
        }
        return true;
    }
}
