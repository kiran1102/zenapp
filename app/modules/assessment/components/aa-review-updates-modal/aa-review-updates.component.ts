// Angular
import {
    Component,
    OnInit,
} from "@angular/core";

// 3rd party
import { Subject } from "rxjs";

// Services
import { AssessmentDetailApiService } from "modules/assessment/services/api/assessment-detail-api.service";

// Interfaces
import { IOtModalContent } from "@onetrust/vitreus";
import {
    IUpdatedTemplateQuestions,
    IUpdatedTemplateQuestionsResponse,
} from "interfaces/assessment/assessment-model.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";

// Constants
import { AssessmentReviewUpdatesStatus } from "constants/assessment.constants";

@Component({
    selector: "aa-review-updates",
    templateUrl: "./aa-review-updates.component.html",
})

export class AaReviewTemplateUpdatesComponent implements OnInit, IOtModalContent {
    otModalCloseEvent: Subject<boolean | IUpdatedTemplateQuestions>;
    otModalData: {
        newTemplateId: string;
        oldTemplateId: string;
        disabledSections: string[];
    } | null;
    reviewUpdatesStatus = AssessmentReviewUpdatesStatus;
    loadingQuestions = true;

    questionsChanged: IUpdatedTemplateQuestions[];

    constructor(private assessmentDetailApi: AssessmentDetailApiService) { }

    ngOnInit(): void {
        this.assessmentDetailApi.getTemplateUpdates(this.otModalData.oldTemplateId, this.otModalData.newTemplateId)
            .subscribe((response: IProtocolResponse<IUpdatedTemplateQuestionsResponse>) => {
                if (response.result) {
                    this.questionsChanged = this.formatChangedQuestions(response.data.questionsChanged);
                }
                this.loadingQuestions = false;
            });
    }

    closeModal(): void {
        this.otModalCloseEvent.next();
    }

    closeModalAndGoToQuestion(question: IUpdatedTemplateQuestions): void {
        this.otModalCloseEvent.next(question);
    }

    private formatChangedQuestions(questionsChanged: IUpdatedTemplateQuestions[]): IUpdatedTemplateQuestions[] {
        questionsChanged.forEach((question: IUpdatedTemplateQuestions) => {
            question.deleted = question.changeTypes.includes("QUESTION_DELETED") || question.changeTypes.includes("SECTION_DELETED");
            question.disabled = this.otModalData.disabledSections.includes(question.sectionId);
        });

        return [...questionsChanged];
    }
}
