// Angular
import {
    Injectable,
    Inject,
} from "@angular/core";

// 3rd Party
import {
    forEach,
    keyBy,
} from "lodash";

// Enums
import {
    FilterOperatorSymbol,
    FilterOperators,
} from "modules/filter/enums/filter.enum";
import { LogicalOperators } from "enums/operators.enum";
import { RiskPrettyNames } from "enums/risk.enum";

// Constants
import {
    AssessmentExportColumnMetaData,
    AssessmentColumnData,
    AssessmentResultKeysToExport,
    AssessmentStatusKeysToExport,
} from "constants/assessment.constants";

// Redux
import { StoreToken } from "tokens/redux-store.token";
import { getCurrentUser } from "oneRedux/reducers/user.reducer";

// Interfaces
import { IGridColumn } from "interfaces/grid.interface";
import { IStringMap } from "interfaces/generic.interface";
import {
    IFilterColumnOption,
    IFilterColumn,
    IFilter,
} from "interfaces/filter.interface";
import { IStore } from "interfaces/redux.interface";
import {
    IAAListFilter,
    IAssessmentFilterParams,
} from "interfaces/assessment/assessment.interface";

// Services
import { Permissions } from "modules/shared/services/helper/permissions.service";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

@Injectable()
export class AAListHelperService {

    constructor(
        @Inject(StoreToken) private store: IStore,
        private permissions: Permissions,
        private translatePipe: TranslatePipe,
    ) {}

    getVisibleColumnsToExport(assessmentColumnMetaDataList: IFilterColumnOption[]): IFilterColumn[] {
        const columnData = JSON.parse(localStorage.getItem("AssessmentColumnData"));
        const visibleColumns: IFilterColumn[] = [];
        forEach(columnData.activeColumns, (activeColumn: IGridColumn) => {
            forEach(assessmentColumnMetaDataList, (assessmentMetaData: IFilterColumnOption) => {
                if (AssessmentExportColumnMetaData[activeColumn.valueKey] === assessmentMetaData.id
                    || (AssessmentExportColumnMetaData[activeColumn.valueKey] === AssessmentExportColumnMetaData[AssessmentColumnData.RiskLevel]
                        && assessmentMetaData.id === AssessmentExportColumnMetaData.RiskScore)) {
                    visibleColumns.push(this.getVisibleColumns(assessmentMetaData));
                }
            });
        });
        return visibleColumns;
    }

    getFilterCriteria(assessmentColumnMetaDataList: IFilterColumnOption[], applyDashboardFilters: boolean): IStringMap<IStringMap<IFilter[]> | any[]> {
        if (applyDashboardFilters) {
            return;
        }
        let activeAssessmentFilterList: IFilter[] = [];
        let preSelectedFilters: IAssessmentFilterParams[] = [];
        const assessmentFilter: IAAListFilter = JSON.parse(localStorage.getItem("ActiveAssessmentFilterList"));
        preSelectedFilters = assessmentFilter && assessmentFilter.preSelectedFilters ? assessmentFilter.preSelectedFilters : null;
        activeAssessmentFilterList = assessmentFilter && assessmentFilter.activeAssessmentFilters ? assessmentFilter.activeAssessmentFilters : null;
        let filterCriteria: IStringMap<IFilter[]> | any[] = [];
        if (!this.permissions.canShow("AssessmentViewAllOrgAssessments")) {
            // Mandatory filter criteria to export
            // skip if AssessmentViewAllOrgAssessments permission is available (SiteOwner & ProjectOwner).
            filterCriteria.push(this.getMandatoryFilterCriteria(assessmentColumnMetaDataList));
        }
        // Assessment or dashboard filter criteria to export
        filterCriteria = this.modifiedFilterCriteria(activeAssessmentFilterList, assessmentColumnMetaDataList, filterCriteria, preSelectedFilters);
        if (!(filterCriteria && filterCriteria.length)) return;
        return {
            [LogicalOperators.AND]: filterCriteria,
        };
    }

    private getMandatoryFilterCriteria(assessmentColumnMetaDataList: IFilterColumnOption[]): IStringMap<IFilter[]> {
        const assessmentMetaDataKeyValuePair: IStringMap<IFilterColumnOption> = keyBy(assessmentColumnMetaDataList, "id");
        const currentUserFullName: string = getCurrentUser(this.store.getState()).FullName;
        const mandatoryFilterCriteria: IFilter[] = [
            {
                columnId: assessmentMetaDataKeyValuePair[AssessmentExportColumnMetaData[AssessmentColumnData.Approver]].id,
                attributeKey: assessmentMetaDataKeyValuePair[AssessmentExportColumnMetaData[AssessmentColumnData.Approver]].id,
                dataType: assessmentMetaDataKeyValuePair[AssessmentExportColumnMetaData[AssessmentColumnData.Approver]].type,
                entityType: assessmentMetaDataKeyValuePair[AssessmentExportColumnMetaData[AssessmentColumnData.Approver]].entityType,
                operator: FilterOperators.Equals,
                values: [currentUserFullName],
            },
            {
                columnId: assessmentMetaDataKeyValuePair[AssessmentExportColumnMetaData[AssessmentColumnData.Respondent]].id,
                attributeKey: assessmentMetaDataKeyValuePair[AssessmentExportColumnMetaData[AssessmentColumnData.Respondent]].id,
                dataType: assessmentMetaDataKeyValuePair[AssessmentExportColumnMetaData[AssessmentColumnData.Respondent]].type,
                entityType: assessmentMetaDataKeyValuePair[AssessmentExportColumnMetaData[AssessmentColumnData.Respondent]].entityType,
                operator: FilterOperators.Equals,
                values: [currentUserFullName],
            },
        ];
        return {
            [LogicalOperators.OR]: mandatoryFilterCriteria,
        };
    }

    private getVisibleColumns(assessmentMetaData: IFilterColumnOption): IFilterColumn {
        return {
            columnId: assessmentMetaData.id,
            entityType: assessmentMetaData.entityType,
            columnName: assessmentMetaData.name,
            translationKey: assessmentMetaData.translationKey,
            dataType: assessmentMetaData.type,
        };
    }

    private getActiveFiltersToExport(assessmentMetaData: IFilterColumnOption, activeAssessmentFilter: IFilter, showValues: boolean, operator: string, preSelectedFilters?: IAssessmentFilterParams[]): IFilter {
        return {
            columnId: assessmentMetaData.id,
            attributeKey: assessmentMetaData.id,
            dataType: assessmentMetaData.type,
            operator,
            entityType: assessmentMetaData.entityType,
            values: this.getValuesToExport(assessmentMetaData, activeAssessmentFilter, showValues, preSelectedFilters),
        };
    }

    private mapAssessmentFilterOperatorsToExport(operators: string): string {
        switch (operators) {
            case FilterOperatorSymbol.EqualTo:
                return FilterOperators.Equals;
            case FilterOperatorSymbol.LessThan:
                return FilterOperators.LessThan;
            case FilterOperatorSymbol.GreaterThan:
                return FilterOperators.GreaterThan;
        }
    }

    private modifiedFilterCriteria(activeAssessmentFilterList: IFilter[], assessmentColumnMetaDataList: IFilterColumnOption[], filterCriteria: IStringMap<IFilter[]> | any, preSelectedFilters?: IAssessmentFilterParams[]): IStringMap<IFilter[]> | any[] {
        forEach(activeAssessmentFilterList, (activeAssessmentFilter: IFilter) => {
            forEach(assessmentColumnMetaDataList, (assessmentMetaData: IFilterColumnOption) => {
                if (activeAssessmentFilter.attributeKey === assessmentMetaData.id) {
                    switch (activeAssessmentFilter.attributeKey) {
                        case AssessmentExportColumnMetaData[AssessmentColumnData.Approver]:
                        case AssessmentExportColumnMetaData[AssessmentColumnData.Respondent]:
                        case AssessmentExportColumnMetaData[AssessmentColumnData.Creator]:
                        case AssessmentExportColumnMetaData[AssessmentColumnData.Organization]:
                        case AssessmentExportColumnMetaData[AssessmentColumnData.Template]:
                        case AssessmentExportColumnMetaData[AssessmentColumnData.Tags]:
                            filterCriteria.push(this.getActiveFiltersToExport(assessmentMetaData, activeAssessmentFilter, false, FilterOperators.Equals));
                            break;
                        case AssessmentExportColumnMetaData[AssessmentColumnData.DateCreated]:
                        case AssessmentExportColumnMetaData[AssessmentColumnData.Deadline]:
                            filterCriteria.push(this.getActiveFiltersToExport(assessmentMetaData, activeAssessmentFilter, true, FilterOperators.Between, preSelectedFilters));
                            break;
                        case AssessmentExportColumnMetaData[AssessmentColumnData.OpenInfoRequest]:
                        case AssessmentExportColumnMetaData[AssessmentColumnData.OpenRiskCount]:
                            const operator: string = this.mapAssessmentFilterOperatorsToExport(activeAssessmentFilter.operator);
                            filterCriteria.push(this.getActiveFiltersToExport(assessmentMetaData, activeAssessmentFilter, true, operator));
                            break;
                        default:
                            filterCriteria.push(this.getActiveFiltersToExport(assessmentMetaData, activeAssessmentFilter, true, FilterOperators.Equals));
                    }
                }
            });
        });
        return filterCriteria;
    }

    private getValuesToExport(assessmentMetaData: IFilterColumnOption, activeAssessmentFilter: IFilter, showValues: boolean, preSelectedFilters?: IAssessmentFilterParams[]): Array<string | number> {
        switch (assessmentMetaData.id) {
            case AssessmentExportColumnMetaData[AssessmentColumnData.State]:
                return activeAssessmentFilter.values.map((value: string | number): string => {
                    return AssessmentStatusKeysToExport[value];
                });
            case AssessmentExportColumnMetaData[AssessmentColumnData.Result]:
                return activeAssessmentFilter.values.map((value: string | number): string => {
                    return AssessmentResultKeysToExport[value];
                });
            case AssessmentExportColumnMetaData[AssessmentColumnData.RiskLevel]:
                return activeAssessmentFilter.values.map((value: string | number): string => {
                    return RiskPrettyNames[value];
                });
            case AssessmentExportColumnMetaData[AssessmentColumnData.DateCreated]:
            case AssessmentExportColumnMetaData[AssessmentColumnData.Deadline]:
                return this.getPreSelectedDates(preSelectedFilters, activeAssessmentFilter);
            case AssessmentExportColumnMetaData[AssessmentColumnData.Template]:
                return activeAssessmentFilter.labels.map((value: string): string => {
                    return value.replace(`(${this.translatePipe.transform("Archived")})`, "").trim();
                });
            default:
                return showValues ? activeAssessmentFilter.values : activeAssessmentFilter.labels;

        }
    }

    private getPreSelectedDates(preSelectedFilters: IAssessmentFilterParams[], activeAssessmentFilter: IFilter): string[] {
        const filteredDates: string[] = [];
        forEach(preSelectedFilters, (preSelectedFilter: IAssessmentFilterParams) => {
            if (preSelectedFilter.field === activeAssessmentFilter.columnId) {
                filteredDates.push(preSelectedFilter.value as string);
                filteredDates.push(preSelectedFilter.toValue as string);
            }
        });
        return filteredDates;
    }
}
