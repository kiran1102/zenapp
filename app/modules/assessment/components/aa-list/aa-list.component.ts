// Angular
import {
    Component,
    Inject,
    OnInit,
    OnDestroy,
} from "@angular/core";
import { StateService } from "@uirouter/core";

// 3rd Party
import {
    remove,
    forEach,
} from "lodash";
import {
    OtModalService,
    ModalSize,
    ConfirmModalType,
} from "@onetrust/vitreus";
import { takeUntil } from "rxjs/operators";
import { Subject } from "rxjs";

// Redux
import { getCurrentUser } from "oneRedux/reducers/user.reducer";
import { UserRoles } from "constants/user-roles.constant";
import { StoreToken } from "tokens/redux-store.token";
import { IStore } from "interfaces/redux.interface";

// Interfaces
import {
    IAssessmentTableRow,
    IAssessmentTable,
    IAssessmentFilterModel,
    IAssessmentFilterParams,
    IAssessmentColumns,
} from "interfaces/assessment/assessment.interface";
import {
    IGridColumn,
    IGridSort,
    IGridPaginationParams,
} from "interfaces/grid.interface";
import { IPageableMeta } from "interfaces/pagination.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";
import {
    IStringMap,
    ILabelValue,
} from "interfaces/generic.interface";
import {
    IFilterData,
    IAddedFilter,
    IFilterTranslatedLabels,
} from "@onetrust/vitreus";
import { ISectionHeaderModel } from "interfaces/assessment/section-model.interface";
import {
    IAAListFilter,
    IAssessmentExportCriteria,
    IAssessmentSort,
} from "interfaces/assessment/assessment.interface";
import { AAFilterId } from "modules/assessment/enums/aa-filter.enum";
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import { ITaskConfirmationModalResolve } from "interfaces/task-confirmation-modal-resolve.interface";
import {
    IFilterColumnOption,
    IFilterColumn,
    IFilter,
} from "interfaces/filter.interface";

// Constants
import {
    AssessmentPermissions,
    AssessmentExportColumnMetaData,
    AssessmentColumnData,
} from "constants/assessment.constants";
import { AssessmentStateKeys } from "constants/assessment.constants";
import { WootricModuleMap } from "modules/shared/constants/wootric.constants";
import { AAErrorMessage } from "constants/aa-error-message.constant";

// Enums
import { AssessmentStatus } from "enums/assessment/assessment-status.enum";
import { AAErrorCode } from "enums/aa-error-code.enum";
import { ModuleTypes } from "enums/module-types.enum";

// Services
import { Permissions } from "modules/shared/services/helper/permissions.service";
import { AATemplateApiService } from "modules/assessment/services/api/aa-template-api.service";
import { AssessmentListApiService } from "modules/assessment/services/api/assessment-list-api.service";
import { ModalService } from "sharedServices/modal.service";
import { AaDetailActionService } from "modules/assessment/services/action/aa-detail-action.service";
import { Wootric } from "sharedModules/services/provider/wootric.service";
import { NotificationService } from "modules/shared/services/provider/notification.service";
import { AAListHelperService } from "./aa-list-helper.service";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

// Interfaces
import { IListState } from "@onetrust/vitreus";

// Components
import { AAColumnsEditorModalComponent } from "modules/assessment/components/aa-columns-editor-modal/aa-columns-editor-modal.component";

@Component({
    selector: "aa-list",
    templateUrl: "aa-list.component.html",
})

export class AAListComponent implements OnInit, OnDestroy {

    assessmentExportEnabled: boolean;
    inputFilterData: IFilterData[];
    addedFilter: IAddedFilter;
    translatedLabels: IFilterTranslatedLabels;
    preSelectedFilter: IAddedFilter[] = [];
    projectStates: Array<ILabelValue<string>>;
    assessmentList: IAssessmentTableRow[];
    currentFilter: IAssessmentFilterModel = {
        statusFilter: null,
        templateFilter: null,
    };
    paginationDetail: IPageableMeta;
    permissions: IStringMap<boolean>;
    sortData: IGridSort;
    exportSortInfo: IAssessmentSort = {
        columnName: AssessmentExportColumnMetaData[AssessmentColumnData.DateCreated],
        ascending: false,
    };
    translations: IStringMap<string>;
    viewReady = false;
    isProjectOwner: boolean;
    allowCreateAssessment: boolean;
    currentPage: number;
    numPages: number;
    isFilterOpen = false;
    activeColumns: IGridColumn[];
    disabledColumns: IGridColumn[];
    isColumnsEditorDisabled = true;
    filtersApplied: boolean;
    defaultViews; // TODO: Add type when API is established

    defaultActiveColumns: IGridColumn[] = [
        { nameKey: "ID", name: this.translatePipe.transform("ID"), valueKey: AssessmentColumnData.Id, sortKey: AssessmentColumnData.Id, style: { "width": "2rem", "line-height": "2rem" } },
        { nameKey: "Name", name: this.translatePipe.transform("Name"), valueKey: AssessmentColumnData.Name, sortKey: AssessmentColumnData.Name, style: { "width": "7rem", "line-height": "2rem" }, isMandatory: true },
        { nameKey: "Stage", name: this.translatePipe.transform("Stage"), valueKey: AssessmentColumnData.State, sortKey: AssessmentColumnData.State, style: { "width": "3rem", "line-height": "2rem" } },
        { nameKey: "Result", name: this.translatePipe.transform("Result"), valueKey: AssessmentColumnData.Result, sortKey: AssessmentColumnData.Result, style: { "width": "3rem", "line-height": "2rem", "text-align": "center" } },
        { nameKey: "RiskLevel", name: this.translatePipe.transform("RiskLevel"), valueKey: AssessmentColumnData.RiskLevel, sortKey: AssessmentColumnData.RiskLevel, style: { "width": "3rem", "line-height": "2rem" } },
        { nameKey: "RiskScore", name: this.translatePipe.transform("RiskScore"), valueKey: AssessmentColumnData.RiskScore, sortKey: AssessmentColumnData.RiskScore, style: { "width": "3rem", "line-height": "2rem" } },
        { nameKey: "Organization", name: this.translatePipe.transform("Organization"), valueKey: AssessmentColumnData.Organization, sortKey: AssessmentColumnData.Organization, style: { "width": "3rem", "line-height": "2rem" } },
        { nameKey: "Respondent", name: this.translatePipe.transform("Respondent"), valueKey: AssessmentColumnData.Respondent, sortKey: AssessmentColumnData.Respondent, style: { "width": "3rem", "line-height": "2rem" } },
        { nameKey: "Approver", name: this.translatePipe.transform("Approver"), valueKey: AssessmentColumnData.Approver, sortKey: AssessmentColumnData.Approver, style: { "width": "3rem", "line-height": "2rem" } },
        { nameKey: "Deadline", name: this.translatePipe.transform("Deadline"), valueKey: AssessmentColumnData.Deadline, sortKey: AssessmentColumnData.Deadline, style: { "width": "3.5rem", "line-height": "2rem" } },
        { nameKey: "OpenInfoRequestCount", name: this.translatePipe.transform("OpenInfoRequest"), valueKey: AssessmentColumnData.OpenInfoRequest, sortKey: AssessmentColumnData.OpenInfoRequest, style: { "width": "3.5rem", "line-height": "2rem" } },
        { nameKey: "OpenRiskCount", name: this.translatePipe.transform("OpenRiskCount"), valueKey: AssessmentColumnData.OpenRiskCount, sortKey: AssessmentColumnData.OpenRiskCount, style: { "width": "3.5rem", "line-height": "2rem" } },
        { nameKey: "Tags", name: this.translatePipe.transform("Tags"), valueKey: AssessmentColumnData.Tags, sortKey: AssessmentColumnData.Tags, style: { "width": "3.5rem", "line-height": "2rem" } },
    ];
    defaultDisabledColumns: IGridColumn[] = [
        { nameKey: "Template", name: this.translatePipe.transform("Template"), valueKey: AssessmentColumnData.Template, sortKey: AssessmentColumnData.Template, style: { "width": "3rem", "line-height": "2rem" } },
        { nameKey: "Creator", name: this.translatePipe.transform("Creator"), valueKey: AssessmentColumnData.Creator, sortKey: AssessmentColumnData.Creator, style: { "width": "3rem", "line-height": "2rem" } },
        { nameKey: "DateCreated", name: this.translatePipe.transform("DateCreated"), valueKey: AssessmentColumnData.DateCreated, sortKey: AssessmentColumnData.DateCreated, style: { "width": "3.5rem", "line-height": "2rem" } },
    ];
    viewTranslations = {
        UpdateView: this.translatePipe.transform("UpdateView"),
        CloneView: this.translatePipe.transform("CloneView"),
        CreateView: this.translatePipe.transform("CreateView"),
        ManageViews: this.translatePipe.transform("ManageViews"),
        SearchPlaceHolder: `${this.translatePipe.transform("Search")}...`,
        NoViewsFound: this.translatePipe.transform("NoViewsFound"),
    };
    applyDashboardFilters: boolean;
    showListDropdown = false;
    currentView = 1;
    headerTitle = "View All Records"; // TODO: Add translation when title confirmed
    showButton = false;

    private allAssessmentsLabel = this.translatePipe.transform("AllProjectsDuplicate", { ProjectTerm: this.translatePipe.transform("Assessments") });
    private appliedFilters: IAssessmentFilterParams[] = [];
    private paginationParams: IGridPaginationParams = { page: 0, size: 20, sort: "createDT,desc" };
    private searchText = "";
    private allTemplatesLabel = this.translatePipe.transform("AllTemplates");
    private assessmentColumnMetaData: IFilterColumnOption[];
    private destroy$ = new Subject();

    constructor(
        @Inject(StoreToken) private store: IStore,
        @Inject("$rootScope") private $rootScope: IExtendedRootScopeService,
        private stateService: StateService,
        public Permission: Permissions,
        public AssessmentTemplateAPI: AATemplateApiService,
        public AssessmentListAPI: AssessmentListApiService,
        public modalService: ModalService,
        private translatePipe: TranslatePipe,
        private assessmentDetailAction: AaDetailActionService,
        private wootric: Wootric,
        private otModalService: OtModalService,
        private notificationService: NotificationService,
        private aAListHelperService: AAListHelperService,
    ) { }

    ngOnInit(): void {
        this.translations = {
            exportCSV: this.translatePipe.transform("ExportCSV"),
            noAssessmentsMatchYourAppliedFilters: this.translatePipe.transform("NoAssessmentsMatchYourAppliedFilters"),
            DeleteAssessment: this.translatePipe.transform("DeleteAssessment"),
            cancelButtonText: this.translatePipe.transform("Cancel"),
            confirmButtonText: this.translatePipe.transform("Confirm"),
        };

        this.projectStates = [
            { label: this.translatePipe.transform("AllProjectsDuplicate", { ProjectTerm: this.translatePipe.transform("Assessments") }), value: null },
            { label: this.translatePipe.transform(AssessmentStateKeys.NotStarted), value: AssessmentStatus.Not_Started },
            { label: this.translatePipe.transform(AssessmentStateKeys.InProgress), value: AssessmentStatus.In_Progress },
            { label: this.translatePipe.transform(AssessmentStateKeys.UnderReview), value: AssessmentStatus.Under_Review },
            { label: this.translatePipe.transform(AssessmentStateKeys.Completed), value: AssessmentStatus.Completed },
        ];

        this.translatedLabels = {
            closeLabel: this.translatePipe.transform("Close"),
            addFilterLabel: this.translatePipe.transform("AddFilter"),
            removeFilterLabel: this.translatePipe.transform("RemoveFilters"),
            newFilterLabel: this.translatePipe.transform("NewFilter"),
            applyLabel: this.translatePipe.transform("Apply"),
            cancelLabel: this.translatePipe.transform("Cancel"),
            addLabel: this.translatePipe.transform("Add"),
            saveLabel: this.translatePipe.transform("Save"),
            fieldLabel: this.translatePipe.transform("Field"),
            operatorLabel: this.translatePipe.transform("Operator"),
            filterValueLabel: this.translatePipe.transform("FilterValue"),
            orLabel: this.translatePipe.transform("Or"),
        };

        // Currently orgGroupId is being passed from AA V2 dashboard page
        if (this.stateService.params.orgGroupId) {
            this.appliedFilters.push({
                field: "orgGroupId",
                operation: "=",
                value: this.stateService.params.orgGroupId,
            });
        }
        if (this.stateService.params.statusFilter) {
            this.applyDashboardFilters = true;
            this.appliedFilters.push({
                field: "status",
                operation: "=",
                value: this.stateService.params.statusFilter,
            });
            this.currentFilter.statusFilter = this.stateService.params.statusFilter;
        }

        if (this.stateService.params.templateId) {
            this.appliedFilters.push({
                field: "templateRootVersionId",
                operation: "=",
                value: this.stateService.params.templateId,
            });
        }

        if (!this.applyDashboardFilters && localStorage.getItem("ActiveAssessmentFilterList")) {
            const activeAssessmentFilterList: IAAListFilter = JSON.parse(localStorage.getItem("ActiveAssessmentFilterList"));
            if (activeAssessmentFilterList.preSelectedFilters && activeAssessmentFilterList.preSelectedFilters.length) {
                this.appliedFilters = activeAssessmentFilterList.preSelectedFilters || [];
                this.appliedFilters = this.setCustomFilterOptions(this.appliedFilters);
                this.paginationParams.page = 0;
                this.filtersApplied = true;
            }
        }

        this.getAssessments()
            .then(() => {
                this.getRestrictViewSetting();
            });

        this.permissions = {
            assessmentCreate: this.Permission.canShow(AssessmentPermissions.AssessmentCreate),
            assessmentDelete: this.Permission.canShow(AssessmentPermissions.AssessmentDelete),
            assessmentSavedViews: this.Permission.canShow(AssessmentPermissions.AssessmentSavedViews),
        };
        this.assessmentExportEnabled = this.Permission.canShow(AssessmentPermissions.FileRead)
            && this.Permission.canShow(AssessmentPermissions.ReportsExportNew)
            && this.Permission.canShow(AssessmentPermissions.AssessmentExports);
        this.isProjectOwner = getCurrentUser(this.store.getState()).RoleName === UserRoles.ProjectOwner;
        this.wootric.run(WootricModuleMap.AssessmentAutomation);
        this.defaultViews = this.AssessmentListAPI.retrieveViews("id"); // TODO: Implement actual parameters once API is established

    }

    ngOnDestroy() {
        this.destroy$.next();
        this.destroy$.complete();
    }

    public fetchPage = (page: number): void => {
        if (this.paginationDetail.number !== page) {
            this.paginationParams.page = page;
            this.getAssessments();
        }
    }

    setCustomFilterOptions(filters: IAssessmentFilterParams[]): IAssessmentFilterParams[] {
        if (filters && filters.length) {
            filters = filters.map((filter: IAssessmentFilterParams) => {
                if ((filter.field === AAFilterId.Deadline || filter.field === AAFilterId.DateCreated)
                    && Array.isArray(filter.value)) {
                    filter.value = filter.value[0];
                }
                return filter;
            });
        }
        return filters;
    }

    getRestrictViewSetting(): void {
        this.AssessmentListAPI.getSspRestrictViewSetting()
            .then((response): void => {
                if (response) {
                    const restrictViewSetting = response.data && response.data.restrictProjectOwnerView;
                    if (this.permissions.assessmentCreate) {
                        this.allowCreateAssessment = this.Permission.canShow("BypassSelfServicePortal") || !restrictViewSetting;
                    } else {
                        this.allowCreateAssessment = false;
                    }
                }
            });
    }

    public search = (searchText: string): void => {
        this.searchText = searchText ? searchText : "";
        this.paginationParams.page = 0;
        this.getAssessments();
    }

    public onAddAssessmentClick = (): void => {
        this.stateService.go("zen.app.pia.module.assessment.wizard.assessment_type");
    }

    public onAssessmentDetailClick = (assessmentId: string): void => {
        this.stateService.go("zen.app.pia.module.assessment.detail", {
            assessmentId,
        });
    }

    public sortAssessments = (sortData: IGridSort): void => {
        this.sortData = sortData;
        this.buildSortInfoOnAssessmentExport(sortData);
        this.paginationParams.sort = sortData.sortedKey + "," + sortData.sortOrder;
        this.getAssessments();
    }

    public deleteAssessment = (assessmentId: string): void => {
        const selectedAssessmentName: string = this.assessmentList.find((assessment) => assessment.assessmentId === assessmentId).name;
        this.otModalService
            .confirm({
                type: ConfirmModalType.DELETE,
                translations: {
                    title: this.translations.DeleteAssessment,
                    desc:  this.translatePipe.transform("AssessmentDeleteConfirmation", { ASSESSMENT_NAME: selectedAssessmentName }),
                    confirm: "",
                    submit: this.translations.confirmButtonText,
                    cancel: this.translations.cancelButtonText,
                },
            }).pipe(takeUntil(this.destroy$))
            .subscribe((data: boolean) => {
                if (data) {
                    this.AssessmentTemplateAPI.deleteAssessment(assessmentId)
                        .then((response: IProtocolResponse<boolean>): boolean => {
                            if (response.result) {
                                remove(this.assessmentList, (row: IAssessmentTableRow): boolean => row.assessmentId === assessmentId);
                                if (!this.assessmentList.length) {
                                    --this.paginationParams.page;
                                    this.getAssessments();
                                }
                            } else if (response.status === 423 && response.errors.code === "ERROR_ASSESSMENT-V2_ASSESSMENT_LOCKED") {
                                this.notificationService.alertError(this.translatePipe.transform("Error"), this.translatePipe.transform("AssessmentLockedForPushingTemplate"));
                            }
                            return response.result;
                        });
                }
            });
    }

    pageChanged(page: number): void {
        this.fetchPage(page - 1);
    }

    toggleFilterPane(): void {
        this.isFilterOpen = !this.isFilterOpen;
    }

    applyFilters(aaFilters: IAssessmentFilterParams[]) {
        this.applyDashboardFilters = false;
        this.appliedFilters = [];
        this.appliedFilters = aaFilters;
        this.filtersApplied = Boolean(aaFilters && aaFilters.length);
        this.paginationParams.page = 0;
        this.getAssessments();
    }

    applyFilter(addedFilterValues: IAddedFilter[]): void {
        this.preSelectedFilter = addedFilterValues;
        this.appliedFilters = [];

        forEach(addedFilterValues, (addedFilterValue) => {
            if (addedFilterValue.option.value === "Status" && addedFilterValue.allSelectedOptions !== this.allAssessmentsLabel) {
                this.appliedFilters.push({ field: "status", operation: "=", value: addedFilterValue.values.map((item) => item.value) });
            } else if (addedFilterValue.option.value === "Template" && addedFilterValue.allSelectedOptions !== this.allTemplatesLabel) {
                this.appliedFilters.push({ field: "templateRootVersionId", operation: "=", value: addedFilterValue.values.map((item) => item.value) });
            }
        });

        if (!addedFilterValues.length) {
            this.appliedFilters = [];
        }

        localStorage.setItem("ActiveAssessmentFilterList", JSON.stringify({
            preSelectedFilters: this.preSelectedFilter,
            activeAssessmentFilters: this.appliedFilters,
        }));

        this.paginationParams.page = 0;
        this.getAssessments();
    }

    openAssessmentDetailsModel(assessmentId: string): void {
        this.assessmentDetailAction.openEditAssessmentModel(assessmentId)
            .then((): void => {
                const sectionHeaderModel: ISectionHeaderModel = this.assessmentDetailAction.getAssessmentDetailSectionHeaderModel(this.store.getState());
                sectionHeaderModel.closeCallback = () => {
                    this.getAssessments();
                };
                this.modalService.setModalData(sectionHeaderModel);
            });
    }

    columnsEditorButton(): void {
        this.otModalService.create(
            AAColumnsEditorModalComponent,
            {
                isComponent: true,
                size: ModalSize.MEDIUM,
            },
            this.getColumnsForPicker(),
        ).pipe(takeUntil(this.destroy$))
            .subscribe((data: IListState): void => {
                if (data && (data.left || data.right)) {
                    localStorage.setItem("AssessmentColumnData", JSON.stringify({
                        activeColumns: data.right,
                        disabledColumns: data.left,
                    }));
                    this.updateColumns();
                }
            });
    }

    onExportAssessmentClick(): void {
        this.AssessmentListAPI.getAssessmentsColumnMetaData().subscribe((response: IProtocolResponse<IFilterColumnOption[]>): void => {
            if (response.result) {
                this.assessmentColumnMetaData = response.data;
                const visibleColumns: IFilterColumn[] = this.aAListHelperService.getVisibleColumnsToExport(this.assessmentColumnMetaData);
                const filterCriteria: IStringMap<IStringMap<IFilter[]> | any[]> = this.aAListHelperService.getFilterCriteria(this.assessmentColumnMetaData, this.applyDashboardFilters);
                const sortInfo: IAssessmentSort = this.exportSortInfo;
                const exportCriteria: IAssessmentExportCriteria = {
                    reportExportFormat: "EXCEL",
                    savedReportView: {
                        name: "assessment-export",
                        type: ModuleTypes.PIA,
                        viewType: "COLUMN",
                        filterCriteria,
                        sortInfo,
                        visibleColumns,
                    },
                };
                this.exportAssessmentColumnMetaData(exportCriteria);
            }
        });
    }

    updateView(currentView: any) {
        // TODO: When a view is clicked, retrieve the filters/columns and apply them & reload page
    }

    cloneView(currentView: any) {
        // TODO: Clone Filters and Columsn, then add to a new view with a generated name...
     }

    createView() {
        // TODO: Open the create view modal
     }

    manageView(views: any[]) {
        // TODO: Open manage view modal
    }

    updateListDropdown(showListDropdown: boolean) {
        this.showListDropdown = showListDropdown;
    }

    onModelChange(model: number) {
        this.currentView = model;
    }

    handleViewSelectorOutsideClick() {
        this.showListDropdown = false;
    }

    private getAssessments = (): ng.IPromise<void> => {
        this.viewReady = false;
        this.assessmentList = [];
        this.paginationDetail = null;
        const getAssessmentsFunction = this.applyDashboardFilters ? this.AssessmentListAPI.getAssessmentsDashboard : this.AssessmentListAPI.getAssessments;
        return getAssessmentsFunction(this.searchText, this.appliedFilters, this.paginationParams)
            .then((response: IProtocolResponse<IAssessmentTable>): void => {
                if (response.result) {
                    this.assessmentList = response.data.content;
                    this.currentPage = response.data.number + 1;
                    this.numPages = response.data.totalPages;
                    this.paginationDetail = {
                        first: response.data.first,
                        last: response.data.last,
                        number: response.data.number,
                        numberOfElements: response.data.numberOfElements,
                        size: response.data.size,
                        sort: response.data.sort,
                        totalElements: response.data.totalElements,
                        totalPages: response.data.totalPages,
                    };
                    this.isColumnsEditorDisabled = false;
                    this.updateColumns();
                } else if (response.errors.code === AAErrorCode.INVALID_SEARCH_CRITERIA) {
                    this.notificationService.alertError(this.translatePipe.transform("Error"), this.translatePipe.transform(AAErrorMessage[response.errors.code]));
                } else {
                    this.notificationService.alertError(this.translatePipe.transform("Error"), this.translatePipe.transform("ErrorRetrievingProject", { Project: this.$rootScope.customTerms.Projects }));
                }
            }).finally((): void => {
                this.viewReady = true;
            });
    }

    private updateColumns(): void {
        const columnData = JSON.parse(localStorage.getItem("AssessmentColumnData"));
        if (columnData) {
            this.activeColumns = columnData.activeColumns;
            this.disabledColumns = columnData.disabledColumns;
        } else {
            this.activeColumns = [...this.defaultActiveColumns];
            this.disabledColumns = [...this.defaultDisabledColumns];
            localStorage.setItem("AssessmentColumnData", JSON.stringify({
                activeColumns: this.activeColumns,
                disabledColumns: this.disabledColumns,
            }));
        }
    }

    private getColumnsForPicker(): IAssessmentColumns {
        this.activeColumns = this.updateColumnTranslations(this.activeColumns);
        this.disabledColumns = this.updateColumnTranslations(this.disabledColumns);
        return {
            activeColumns: [...this.activeColumns],
            disabledColumns: [...this.disabledColumns],
        };
    }

    private updateColumnTranslations(columns: IGridColumn[]): IGridColumn[] {
        Object.keys(columns).forEach((key) => {
            columns[key].name = this.translatePipe.transform(columns[key].nameKey);
        });
        return columns;
    }

    private exportAssessmentColumnMetaData(assessmentExportCriteria: IAssessmentExportCriteria): void {
        this.AssessmentListAPI.export(assessmentExportCriteria).subscribe((response: IProtocolResponse<void>) => {
            if (response.result) {
                const exportModalData: ITaskConfirmationModalResolve = {
                    modalTitle: this.translatePipe.transform("ReportDownload"),
                    confirmationText: this.translatePipe.transform("ReportIsGeneratedCheckTasksBar"),
                };
                this.modalService.setModalData(exportModalData);
                this.modalService.openModal("oneNotificationModalComponent");
            }
        });
    }

    private buildSortInfoOnAssessmentExport(sortData: IGridSort): void {
        this.exportSortInfo = {
            columnName: AssessmentExportColumnMetaData[sortData.sortedKey],
            ascending: sortData.sortOrder === "asc" ? true : false,
        };
    }
}
