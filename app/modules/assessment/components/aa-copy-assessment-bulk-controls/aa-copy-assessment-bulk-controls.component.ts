// Angular
import {
    Component,
    Inject,
    OnInit,
    Output,
    Input,
    EventEmitter,
} from "@angular/core";

// Redux
import { StoreToken } from "tokens/redux-store.token";
import { getCurrentUser } from "oneRedux/reducers/user.reducer";
import { getOrgById } from "oneRedux/reducers/org.reducer";
import { IStore } from "interfaces/redux.interface";

// Interfaces
import { IUser } from "interfaces/user.interface";
import { IOrganization } from "interfaces/org.interface";
import { IOrgUserAdapted } from "interfaces/org-user.interface";
import {
    IAssessmentCopyPayload,
    IAsessmentCopyContent,
} from "interfaces/assessment/assessment-model.interface";
import { IMyDate } from "mydaterangepicker";

// Constants
import { AssessmentCreationActions } from "constants/assessment-creation-actions.constant";

// Service
import { TimeStamp } from "oneServices/time-stamp.service";
@Component({
    selector: "aa-copy-assessment-bulk-controls",
    templateUrl: "aa-copy-assessment-bulk-controls.component.html",
})

export class AACopyAssessmentBulkControlsComponent implements OnInit {

    @Input() bulkCopyContent: IAsessmentCopyContent;

    @Output() onApplyChanges: EventEmitter<IAssessmentCopyPayload> = new EventEmitter<IAssessmentCopyPayload>();

    assessmentActions: typeof AssessmentCreationActions;
    currentUser: IUser;
    orgList: IOrganization[];
    respondentUserId: string;
    selectedOrg: IOrganization;
    deadline: string;
    reminder: number;
    validReminder = true;
    approverList: IOrgUserAdapted[] = [];
    respondentList: IOrgUserAdapted[] = [];
    selectedApproverList: IOrgUserAdapted[] = [];
    selectedRespondentList: IOrgUserAdapted[] = [];
    selectedOrgId: string;
    copyAssessment: IAssessmentCopyPayload;
    disableUntil: IMyDate = this.timeStamp.getDisableUntilDate();
    dateFormat = this.timeStamp.getDatePickerDateFormat();

    constructor(
        @Inject(StoreToken) private store: IStore,
        private timeStamp: TimeStamp,
    ) { }

    ngOnInit() {
        this.assessmentActions = AssessmentCreationActions;
        this.currentUser = getCurrentUser(this.store.getState());
        this.selectedOrg = {} as IOrganization;
        this.selectedOrg.Name = getOrgById(this.currentUser.OrgGroupId)(this.store.getState()).Name;
        this.selectedOrg.Id = this.selectedOrgId = this.currentUser.OrgGroupId;
        this.initialisingBulkControls();
    }

    initialisingBulkControls(): void {
        this.copyAssessment = {
            orgGroupId: this.selectedOrg.Id,
            orgGroupName: this.selectedOrg.Name,
            approvers: [] as IOrgUserAdapted[],
            respondents: [] as IOrgUserAdapted[],
            deadline: "",
            reminder: null,
            latestTemplateVersion: false,
            reviewChangedResponses: true,
        };
    }

    resetRespondentList(): void {
        this.respondentList = [];
        this.selectedRespondentList = [];
        this.copyAssessment.respondents = [];
    }

    handleAction = (action: string, payload?: any): void => {
        switch (action) {
            case AssessmentCreationActions.DEADLINE_CHANGED:
                this.copyAssessment.deadline = payload.jsdate ? payload.jsdate.toISOString() : null;
                this.deadline = payload.date;
                this.validReminder = true;
                this.reminderDaysSelected(this.copyAssessment.reminder, this.deadline);
                break;
            case AssessmentCreationActions.ORG_SELECTED:
                payload = payload ? payload : this.currentUser.OrgGroupId;
                this.selectedOrgId = payload;
                this.copyAssessment.approvers = [];
                this.approverList = [];
                this.resetRespondentList();
                this.copyAssessment.orgGroupId = payload;
                this.copyAssessment.orgGroupName = getOrgById(payload)(this.store.getState()).Name;
                break;
        }
    }

    handleInputChange(payload: string, action: string): void {
        this.handleAction(action, payload);
    }

    onApplyToAllClick(): void {
        this.onApplyChanges.emit(this.copyAssessment);
    }

    selectedApprovers(approverList: IOrgUserAdapted[]): void {
        this.copyAssessment.approvers = [...approverList];
    }

    selectedRespondents(respondentList: IOrgUserAdapted[]): void {
        this.selectedRespondentList = respondentList;
        this.copyAssessment.respondents = [...respondentList];

    }

    reminderDaysSelected(selectedReminder: number, deadline: string): void {
        const today = this.timeStamp.currentDate.format();
        const reminderDate = this.timeStamp.getReminderDate(selectedReminder, deadline);

        this.validReminder = selectedReminder > 0 && reminderDate > today;
        this.copyAssessment.reminder = selectedReminder;
        this.validReminder = (!selectedReminder && selectedReminder !== 0) || this.validReminder;
    }

    onToggle(isResponse: boolean): void {
        if (isResponse) {
            this.bulkCopyContent.copyResponses = !this.bulkCopyContent.copyResponses;
        } else {
            this.bulkCopyContent.copyComments = !this.bulkCopyContent.copyComments;
        }
    }
}
