// Rxjs
import { Subject } from "rxjs";
import {
    startWith,
    takeUntil,
} from "rxjs/operators";

// Angular
import {
    Component,
    OnInit,
    OnDestroy,
    Inject,
} from "@angular/core";
import { StateService } from "@uirouter/core";

// 3rd Party
import { forEach } from "lodash";
import observableFromStore from "oneRedux/utils/observable-from-store";
import { StoreToken } from "tokens/redux-store.token";

// Redux
import { getBulkAssessmentDetailsList } from "modules/assessment/reducers/assessment-detail.reducer";
import { getCurrentUser } from "oneRedux/reducers/user.reducer";

// Interfaces
import { IStore } from "interfaces/redux.interface";
import {
    IAssessmentCreationDetails,
    IAssessmentRespondentDetails,
    IAssessmentApproverDetails,
} from "interfaces/assessment/assessment-creation-details.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";
import { IOrgUserAdapted } from "interfaces/org-user.interface";
import { IUser } from "interfaces/user.interface";

// Services
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import { AATemplateApiService } from "modules/assessment/services/api/aa-template-api.service";
import { AABulkLaunchAdapterService } from "modules/assessment/services/adapter/aa-bulk-launch-adapter.service";

@Component({
    selector: "assessment-bulk-launch-footer",
    templateUrl: "./assessment-bulk-launch-footer.component.html",
})

export class AssessmentBulkLaunchFooterComponent implements OnInit, OnDestroy {

    public bulkAssessmentDetails: IAssessmentCreationDetails[];
    public destroy$ = new Subject();
    public isCreateButtonDisabled = true;
    public saveInProgress = false;
    currentUser: IUser;

    constructor(
        private stateService: StateService,
        @Inject(StoreToken) private store: IStore,
        private AssessmentTemplateAPI: AATemplateApiService,
        readonly aABulkLaunchAdapterService: AABulkLaunchAdapterService,
    ) {}

    ngOnInit(): void {
        observableFromStore(this.store).pipe(
            startWith(this.store.getState()),
            takeUntil(this.destroy$),
        ).subscribe(() => this.componentWillReceiveState());
        this.currentUser = getCurrentUser(this.store.getState());
    }

    ngOnDestroy(): void {
        this.destroy$.next();
        this.destroy$.complete();
    }

    public onCreateAndSend(): void {
        this.saveInProgress = this.isCreateButtonDisabled = true;
        this.bulkAssessmentDetails = this.fillUserDetails(this.bulkAssessmentDetails);
        if (this.bulkAssessmentDetails && this.bulkAssessmentDetails.length > 1) {
            this.AssessmentTemplateAPI.createAssessmentsBulk(this.bulkAssessmentDetails).then((response: IProtocolResponse<string>): void => {
                if (response.result) {
                    const breadcrumb = this.aABulkLaunchAdapterService.getBreadcrumb(this.stateService.params);
                    this.stateService.go(breadcrumb.path, breadcrumb.params);
                } else {
                    this.saveInProgress = this.isCreateButtonDisabled = false;
                }
            });
        } else {
            this.AssessmentTemplateAPI.createAssessment(this.bulkAssessmentDetails[0]).then((response: IProtocolResponse<string>): void => {
                if (response.result) {
                    const isCurrentUserRespondent = this.bulkAssessmentDetails[0].respondents.some((respondent: IAssessmentRespondentDetails) => this.currentUser.Id === respondent.respondentId);

                    if (isCurrentUserRespondent) {
                        this.stateService.go("zen.app.pia.module.assessment.detail", { assessmentId: response.data, launchAssessmentDetails: this.bulkAssessmentDetails[0].inventoryDetails });
                    } else {
                        const breadcrumb = this.aABulkLaunchAdapterService.getBreadcrumb(this.stateService.params);
                        this.stateService.go(breadcrumb.path, breadcrumb.params);
                    }
                } else {
                    this.saveInProgress = this.isCreateButtonDisabled = false;
                }
            });
        }
    }

    fillUserDetails(bulkAssessmentDetails: IAssessmentCreationDetails[]): IAssessmentCreationDetails[] {
        // Fill comments, approvers & respondents from OrgUser list.
        return bulkAssessmentDetails.map((assessmentDetail: IAssessmentCreationDetails): IAssessmentCreationDetails => {
            if (assessmentDetail.respondentsOrgUserList) {
                assessmentDetail.respondents = assessmentDetail.respondentsOrgUserList
                    .filter((respondent: IOrgUserAdapted) => respondent != null)
                    .map((respondent: IOrgUserAdapted): IAssessmentRespondentDetails => {
                    return {
                        respondentId: respondent.Id === respondent.FullName ? null : respondent.Id,
                        respondentName: respondent.FullName,
                        comment: assessmentDetail.comments,
                    };
                });
            }
            if (assessmentDetail.approversOrgUserList) {
                assessmentDetail.approvers = assessmentDetail.approversOrgUserList
                    .filter((approver: IOrgUserAdapted) => approver != null)
                    .map((approver: IOrgUserAdapted): IAssessmentApproverDetails => {
                    return {
                        approverId: approver.Id,
                        approverName: approver.FullName,
                        comment: assessmentDetail.comments,
                    };
                });
                return assessmentDetail;
            }
        });
    }

    public onCancel() {
        const breadcrumb = this.aABulkLaunchAdapterService.getBreadcrumb(this.stateService.params);
        this.stateService.go(breadcrumb.path, breadcrumb.params);
    }

    private componentWillReceiveState(): void {
        this.bulkAssessmentDetails = getBulkAssessmentDetailsList(this.store.getState());
        this.toggleCreateButton();
    }

    private toggleCreateButton(): void {
        if (!this.bulkAssessmentDetails.length) {
            this.isCreateButtonDisabled = true;
            return;
        }
        this.isCreateButtonDisabled = false;
        forEach(this.bulkAssessmentDetails, (bulkAssessmentDetail: IAssessmentCreationDetails): boolean => {
            // if any required field is missing from any of the assessment models, then disable the "Create" button.
            if (!bulkAssessmentDetail.name
                || !bulkAssessmentDetail.templateName
                || !bulkAssessmentDetail.isValidReminder
                || !bulkAssessmentDetail.respondentsOrgUserList
                || bulkAssessmentDetail.respondentsOrgUserList.filter((respondent) => respondent !== null).length === 0) {
                this.isCreateButtonDisabled = true;
                return false;
            }
        });
    }
}
