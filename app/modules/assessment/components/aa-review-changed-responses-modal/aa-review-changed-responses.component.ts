// Angular
import {
    Component,
    OnInit,
} from "@angular/core";

// 3rd party
import { Subject } from "rxjs";

// Services
import { AaChangedResponsesService } from "modules/assessment/services/api/aa-changed-responses-service";

// Interfaces
import { IOtModalContent } from "@onetrust/vitreus";
import {
    IChangedQuestion,
    IChangedQuestionResponse,
    IChangedResponse,
} from "interfaces/assessment/assessment-model.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

// Constants
import { AssessmentQuestionButtonKeys } from "constants/assessment.constants";

@Component({
    selector: "aa-review-changed-responses",
    templateUrl: "./aa-review-changed-responses.component.html",
    host: {
        "[class.flex-full-height]": "true",
        "[class.aa-changed-responses-modal]": "true",
    },
})

export class AaReviewChangedResponseComponent implements OnInit, IOtModalContent {

    otModalCloseEvent: Subject<boolean | IChangedQuestion>;
    otModalData: { readOnly: true } | null;
    changesConfirmed = false;
    totalQuestionCount: number;
    changedQuestions: IChangedQuestion[];
    selectedQuestion: IChangedQuestion;
    loadingResponseDiff = true;
    questionsResponseChanged: string;
    responseDiff: IChangedResponse;

    constructor(
        private aaChangedResponses: AaChangedResponsesService,
        private translate: TranslatePipe,
    ) { }

    ngOnInit(): void {
        this.aaChangedResponses.getChangedQuestions()
            .then((response: IProtocolResponse<IChangedQuestionResponse>) => {
                if (response.result) {
                    this.totalQuestionCount = response.data.totalQuestionCount;
                    this.changedQuestions = response.data.questionsChanged;
                    this.selectedQuestion = this.changedQuestions[0];
                    this.getResponseDiff();
                    this.questionsResponseChanged = this.translate.transform("QuestionsResponseChanged", {
                        NUMBER_OF_CHANGED_QUESTIONS: this.changedQuestions.length,
                        NUMBER_OF_TOTAL_QUESTIONS: this.totalQuestionCount,
                    });
                }
            });
    }

    confirmChanges(confirmChanges: boolean): void {
        this.changesConfirmed = confirmChanges;
    }

    showResponseDiff(question: IChangedQuestion): void {
        this.selectedQuestion = question;
        this.getResponseDiff();
    }

    closeModal(): void {
        this.otModalCloseEvent.next();
        this.otModalCloseEvent.complete();
    }

    submitAssessment(): void {
        if (this.changesConfirmed) this.otModalCloseEvent.next(true);
    }

    closeModalAndGoToQuestion(): void {
        this.otModalCloseEvent.next(this.selectedQuestion);
        this.otModalCloseEvent.complete();
    }

    private getResponseDiff(): void {
        if (!this.changedQuestions.length) {
            this.changesConfirmed = true;
            return;
        }
        this.loadingResponseDiff = true;
        this.aaChangedResponses.getResponseDiff(this.selectedQuestion.questionId).
            then((response: IProtocolResponse<IChangedResponse>) => {
                if (response.result) {
                    this.responseDiff = this.transformResponse(response.data);
                }
                this.loadingResponseDiff = false;
            });
    }

    private transformResponse(response: IChangedResponse) {
        if (AssessmentQuestionButtonKeys[response.currentResponse]) {
            response.currentResponse = this.translate.transform(response.currentResponse);
        }
        if (AssessmentQuestionButtonKeys[response.originalResponse]) {
            response.originalResponse = this.translate.transform(response.originalResponse);
        }

        return { ...response };
    }

}
