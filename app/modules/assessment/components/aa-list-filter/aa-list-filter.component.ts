// 3rd Party
import {
    Component,
    OnInit,
    EventEmitter,
    Output,
    Input,
    Inject,
} from "@angular/core";

// Services
import { AAListFilterService } from "modules/assessment/services/filter/aa-list-filter.service";
import { AATemplateApiService } from "modules/assessment/services/api/aa-template-api.service";
import { AssessmentDetailApiService } from "modules/assessment/services/api/assessment-detail-api.service";
import { TimeStamp } from "oneServices/time-stamp.service";
import { OrgGroupApiService } from "oneServices/api/org-group-api.service";

// Interfaces
import {
    IFilterColumnOption,
    IFilterValueOption,
    IFilter,
} from "interfaces/filter.interface";
import {
    IFilterV2,
    IFilterOutput,
} from "modules/filter/interfaces/filter.interface";
import { ICustomTemplateDetails } from "modules/template/interfaces/custom-templates.interface";
import { ILabelValue } from "interfaces/generic.interface";
import { IAssessmentFilterParams } from "interfaces/assessment/assessment.interface";
import { IQuestionModelOption } from "interfaces/assessment/question-model.interface";
import { IStore } from "interfaces/redux.interface";
import { IOrgUserAdapted } from "interfaces/org-user.interface";

// Enum + Constants
import { ExportTypes } from "enums/export-types.enum";
import {
    AAFilterId,
    AAFilterTypes,
} from "modules/assessment/enums/aa-filter.enum";
import { TemplateStates } from "enums/template-states.enum";
import { OrgUserTraversal } from "enums/org-user-dropdown.enum";
import { AAColumnFilterValueTypes } from "constants/assessment.constants";
import { TemplateTypes } from "constants/template-types.constant";
import { TemplateCriteria } from "constants/template-states.constant";
import { IOrganization } from "interfaces/org.interface";
import { FilterOperators } from "modules/filter/enums/filter.enum";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

// Rxjs
import {
    Subject,
    combineLatest,
} from "rxjs";
import { takeUntil } from "rxjs/operators";

// Redux
import {
    getOrgList,
    getCurrentOrgId,
} from "oneRedux/reducers/org.reducer";
import { StoreToken } from "tokens/redux-store.token";

@Component({
    selector: "aa-list-filter",
    template: `
        <one-filter
            class="flex-full-height"
            [selectedFilters]="selectedFilters"
            [fields]="columnOptions"
            [getFieldLabelValue]="getFieldLabelValue"
            [currentValues]="currentFilterValues"
            [getValueLabelValue]="getColumnOptionLabelValue"
            [getOperatorLabelValue]="getOperatorLabelValue"
            [currentType]="currentFilterValueType"
            [togglePopup$]="closeFilter$"
            [loadingFilter]="loadingFilter"
            [loadingSavedFilter]="loadingFilter"
            [currentOperators]="currentOperators"
            (selectField)="setFilterValues($event)"
            (applyFilters)="apply($event)"
            (cancel)="cancel.emit()"
            >
        </one-filter>
    `,
})
export class AAListFilterComponent implements OnInit {

    @Input() set filtersOpen(isOpen: boolean) {
        if (!isOpen) {
            this.closeFilter$.next(isOpen);
        } else {
            this.loadFilterOptions();
        }
    }

    @Output() applyFilters = new EventEmitter<IAssessmentFilterParams[]>();
    @Output() cancel = new EventEmitter<null>();

    exportTypes = ExportTypes;
    showingExportOptions = false;
    loadingFilter = false;
    currentFilterValues: IFilterValueOption[] = [];
    templatesList: IFilterValueOption[] = [];
    tagsList: IFilterValueOption[] = [];
    userList: IFilterValueOption[] = [];
    orgList: IFilterValueOption[] = [];
    closeFilter$ = new Subject<boolean>();
    destroy$ = new Subject();

    currentFilterValueType: number;
    selectedFilters: Array<IFilterV2<IFilter, ILabelValue<string>, IFilterValueOption | string | number>>;
    columnOptions: IFilterColumnOption[];
    currentOperators: IFilterValueOption[];

    constructor(
        @Inject(StoreToken) private store: IStore,
        private translatePipe: TranslatePipe,
        private aAListFilterService: AAListFilterService,
        private timeStampService: TimeStamp,
        private assessmentTemplateAPI: AATemplateApiService,
        private readonly orgGroupApi: OrgGroupApiService,
        private assessmentDetailApi: AssessmentDetailApiService,
    ) {}

    ngOnInit() {
        this.columnOptions = this.aAListFilterService.getAaListFilters();
    }

    loadFilterOptions() {
        this.loadingFilter = true;
        const templateTypes = `${TemplateTypes.PIA},${TemplateTypes.VENDOR}`;
        const orgList = getOrgList(getCurrentOrgId(this.store.getState()))(this.store.getState());
        this.orgList = orgList && orgList.length ? orgList.map((org: IOrganization): IFilterValueOption => {
            return { value: org.Name, key: org.Id, translationKey: null };
        }) : [];

        combineLatest(
            this.assessmentTemplateAPI.getAssessmentTemplates(templateTypes, TemplateStates.Published, TemplateCriteria.Both),
            this.assessmentDetailApi.getAssessmentTags(),
            this.orgGroupApi.getOrgUsersWithPermission(getCurrentOrgId(this.store.getState()), OrgUserTraversal.All),
        ).pipe(
            takeUntil(this.destroy$),
        ).subscribe(([templates, tags, orgUsers]) => {
            if (templates && tags && orgUsers && orgList) {
                this.templatesList = templates.result && templates.data && templates.data.length ?
                templates.data.map((template: ICustomTemplateDetails): IFilterValueOption => {
                    const templateName: string = template.isActive ? template.name : `${template.name} (${this.translatePipe.transform("Archived")})`;
                    return {
                        value: templateName,
                        key: template.rootVersionId,
                        translationKey: null,
                    };
                }) : [];

                this.tagsList = tags.data && tags.data.length ? tags.data.map((tag: IQuestionModelOption): IFilterValueOption => {
                    return { value: tag.option, key: tag.id, translationKey: null };
                }) : [];

                this.userList = orgUsers.data && orgUsers.data.length ? orgUsers.data.map((user: IOrgUserAdapted): IFilterValueOption => {
                    return { value: user.FullName, key: user.Id, translationKey: null };
                }) : [];

                this.initializeFilterData();
            }
        });
    }

    ngOnDestroy(): void {
        this.destroy$.next();
        this.destroy$.complete();
    }

    initializeFilterData() {
        if (this.templatesList && this.tagsList && this.userList) {
            this.updateColumnOptions(AAFilterId.Template, this.templatesList);
            this.updateColumnOptions(AAFilterId.Tags, this.tagsList);
            this.updateColumnOptions(AAFilterId.Approver, this.userList);
            this.updateColumnOptions(AAFilterId.Respondent, this.userList);
            this.updateColumnOptions(AAFilterId.Creator, this.userList);
            this.updateColumnOptions(AAFilterId.Organization, this.orgList);
            this.updateAAFilters();
            this.loadingFilter = false;
        }
    }

    updateColumnOptions(aAFilterId: AAFilterId, list: IFilterValueOption[]): void {
        const filterIndex = this.columnOptions.findIndex((column: IFilterColumnOption) => column.id === aAFilterId);
        this.columnOptions[filterIndex].valueOptions = list;
    }

    updateAAFilters() {
        if (localStorage.getItem("ActiveAssessmentFilterList")) {
            const activeAssessmentFilterList = JSON.parse(localStorage.getItem("ActiveAssessmentFilterList"));
            if (activeAssessmentFilterList.activeAssessmentFilters && activeAssessmentFilterList.activeAssessmentFilters.length) {
                this.selectedFilters = this.serializeFilters(activeAssessmentFilterList.activeAssessmentFilters);
            }
        }
    }

    apply(filters: IFilterOutput[]) {
        const aaFilters: IAssessmentFilterParams[] = this.aAListFilterService.applyFilters(filters && filters.length ? filters : null);
        const sessionData = JSON.stringify({
            preSelectedFilters: aaFilters,
            activeAssessmentFilters: this.deserializeFilters(filters),
        });
        localStorage.setItem("ActiveAssessmentFilterList", sessionData);
        this.applyFilters.emit(aaFilters);
    }

    getFieldLabelValue = (column: IFilterColumnOption): ILabelValue<string> => {
        return {
            label: column.translationKey ? this.translatePipe.transform(column.translationKey) : (column && column.name ? column.name : ""),
            value:  column && column.id ? column.id : "",
        };
    }

    setFilterValues(selectedField: ILabelValue<string>) {
        if (!selectedField) return;
        const filterIndex = this.columnOptions.findIndex((column: IFilterColumnOption) => column.id === selectedField.value);
        const selectedColumn = this.columnOptions[filterIndex];
        this.currentFilterValueType = AAColumnFilterValueTypes[selectedColumn.type];
        this.currentFilterValues = selectedColumn ? selectedColumn.valueOptions : [];
        this.currentOperators = (selectedField.value === AAFilterId.OpenInfoRequest || selectedField.value === AAFilterId.OpenRiskCount) ?
                this.aAListFilterService.getCurrentOperators() : [];
    }

    getColumnOptionLabelValue = (option: IFilterValueOption): ILabelValue<string | number> => {
        return {
            label: option.translationKey ? this.translatePipe.transform(option.translationKey) : option.value as string,
            value: option.key,
        };
    }

    getOperatorLabelValue = (operator: IFilterValueOption): ILabelValue<string | number> => {
        return {
            label: this.translatePipe.transform(operator.translationKey),
            value: operator.value,
        };
    }

    private formatFilterSelections(column: IFilterColumnOption, selections: string[]): IFilterValueOption[] {
        switch (column.type) {
            case AAFilterTypes.Approver:
            case AAFilterTypes.Respondent:
            case AAFilterTypes.Creator:
            case AAFilterTypes.Template:
            case AAFilterTypes.Result:
            case AAFilterTypes.Tags:
                return column && column.valueOptions && column.valueOptions.length ? selections.map((value: string) => column.valueOptions.find((option: IFilterValueOption) => option.key === value)) : [];
            case AAFilterTypes.Deadline:
            case AAFilterTypes.DateCreated:
                return selections.map((value: string) => ({value: this.timeStampService.formatDateWithoutTime(value), key: value}));
            case AAFilterTypes.OpenInfoRequest:
            case AAFilterTypes.OpenRiskCount:
                return [{key: selections[0], value: selections[0]}];
            case AAFilterTypes.State:
            case AAFilterTypes.RiskLevel:
                return selections.map((value: string) => column.valueOptions.find((option: IFilterValueOption) => option.key === value));

            default:
                return selections.map((value: string) => column.valueOptions.find((option: IFilterValueOption) => option.value === value));
        }
    }

    private serializeFilters(filters: IFilter[]): Array<IFilterV2<IFilter, ILabelValue<string>, IFilterValueOption | string | number>> {
        return filters.map((selectedFilter: IFilter): IFilterV2<IFilter, ILabelValue<string>, IFilterValueOption | string | number> => {
            const matchingColumn: IFilterColumnOption = this.columnOptions.find((column: IFilterColumnOption): boolean => column.id === selectedFilter.columnId);
            return {
                field: matchingColumn,
                operator: selectedFilter.operator === FilterOperators.Equals ? null : this.aAListFilterService.getSelectedOperatorLabelValuePair(selectedFilter.operator),
                values: selectedFilter.values && selectedFilter.values.length ? this.formatFilterSelections(matchingColumn, selectedFilter.values as string[]) : selectedFilter.values,
                type: AAColumnFilterValueTypes[matchingColumn.type],
            };
        });
    }

    private deserializeFilters(filters: IFilterOutput[]): IFilter[] {
        if (!filters) return [];
        return (filters).map((selectedFilter: IFilterOutput): IFilter => {
            const matchingColumn: IFilterColumnOption = this.columnOptions.find((column: IFilterColumnOption): boolean => column.id === selectedFilter.field.value);
            const deserializedFilterValues = this.deserializeFilterValues(selectedFilter.values, matchingColumn.type);
            return {
                columnId: matchingColumn.id,
                attributeKey: matchingColumn.name,
                entityType: matchingColumn.entityType,
                dataType: matchingColumn.type,
                operator: selectedFilter.operator ? selectedFilter.operator.value.toString() : FilterOperators.Equals,
                labels: deserializedFilterValues.labels,
                values: deserializedFilterValues.values,
            };
        });
    }

    private deserializeFilterValues(values: Array<ILabelValue<string|number>>, filterType: number): { labels: Array<string | number>, values: Array<string | number> } | any {
        if (!(values && values.length)) return [];
        switch (filterType) {
            case AAFilterTypes.Approver:
            case AAFilterTypes.Respondent:
            case AAFilterTypes.Creator:
            case AAFilterTypes.Template:
            case AAFilterTypes.Deadline:
            case AAFilterTypes.DateCreated:
            case AAFilterTypes.State:
            case AAFilterTypes.RiskLevel:
            case AAFilterTypes.Result:
            case AAFilterTypes.Tags:
                return {
                    labels: values.map((value: ILabelValue<string | number>): string | number => value.label),
                    values: values.map((value: ILabelValue<string | number>): string | number => value.value || value.label),
                };
            default:
                return {
                    labels: values.map((value: ILabelValue<string | number>): string | number => value.label),
                    values: values.map((value: ILabelValue<string | number>): string | number => value.label || value.value),
                };
        }
    }
}
