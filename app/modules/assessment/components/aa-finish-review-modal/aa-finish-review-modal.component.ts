// Angular
import {
    Component,
    OnInit,
    Inject,
} from "@angular/core";

// 3rd party
import { Subject } from "rxjs";

// Services
import { AaDetailActionService } from "modules/assessment/services/action/aa-detail-action.service";
import { StoreToken } from "tokens/redux-store.token";

// Interfaces
import { IOtModalContent } from "@onetrust/vitreus";
import {
    IAssessmentNeedsConfirmation,
    IApproveModalWithRulesSet,
} from "modules/template/interfaces/template.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";
import { IStore } from "interfaces/redux.interface";
import { AssessmentDetailApiService } from "modules/assessment/services/api/assessment-detail-api.service";
import { IApproverTextConfig } from "interfaces/assessment/aa-approval-modal.interface";
import { ILabelValue } from "interfaces/generic.interface";

// Redux
import { isAssessmentReady } from "modules/assessment/reducers/assessment-detail.reducer";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

// Constants
import { AssessmentFinishReviewActions } from "constants/assessment.constants";

@Component({
    selector: "aa-finish-review-modal",
    templateUrl: "./aa-finish-review-modal.component.html",
})

export class AaFinishReviewModalComponent implements OnInit, IOtModalContent {

    otModalCloseEvent: Subject<boolean | IApproveModalWithRulesSet>;
    otModalData: { assessmentId: string };
    forceOverride = false;
    excludeRules = false;
    isSubmitting: boolean;
    approveModalData: IApproverTextConfig;
    initializing = true;
    finishReviewActionItems: Array<ILabelValue<number>> = [
        {
            label: this.translate.transform(AssessmentFinishReviewActions.APPROVED),
            value: 1,
        },
        {
            label: this.translate.transform(AssessmentFinishReviewActions.REJECTED),
            value: 2,
        },
    ];
    selectedReviewAction: ILabelValue<number>;
    emailComment = "";

    constructor(
        @Inject(StoreToken) private store: IStore,
        private assessmentDetailApi: AssessmentDetailApiService,
        private assessmentDetailAction: AaDetailActionService,
        private translate: TranslatePipe,
    ) { }

    ngOnInit(): void {
        if (isAssessmentReady(this.store.getState())) {
            this.preApprovalConfirmationCheck();
        }
    }

    toggleForceOverride(toggleState: boolean): void {
        this.forceOverride = toggleState;
        if (!this.selectedReviewAction || this.selectedReviewAction.value !== 2) {
            this.preApprovalConfirmationCheck();
        }
    }

    selectReviewAction(selectReviewAction: ILabelValue<number>): void {
        this.selectedReviewAction = selectReviewAction;
    }

    updateComment(comment: string): void {
        this.emailComment = comment;
    }

    confirmAssessmentApproval(): void {
        this.isSubmitting = true;

        switch (this.selectedReviewAction.value) {
            case 1:
                this.approveAssessment();
                break;
            case 2:
                this.rejectAssessment();
                break;
        }
    }

    sendFollowUpAssessment(): void {
        this.excludeRules = !this.excludeRules;
    }

    closeModal(): void {
        this.otModalCloseEvent.next(false);
    }

    private approveAssessment(): void {
        this.assessmentDetailAction.approveAssessmentPromiseToResolve(this.otModalData.assessmentId, this.forceOverride, this.excludeRules, this.emailComment)
            .then((success: boolean): void => {
                if (success) {
                    this.otModalCloseEvent.next(false);
                }
                this.isSubmitting = false;
            });
    }

    private rejectAssessment(): void {
        this.assessmentDetailAction.rejectAssessment(this.otModalData.assessmentId, this.forceOverride, this.emailComment)
            .subscribe((response: IProtocolResponse<null>): void => {
                if (response.result) {
                    this.otModalCloseEvent.next(false);
                }
                this.isSubmitting = false;
            });
    }

    private preApprovalConfirmationCheck() {
        return this.assessmentDetailApi.isSendAssessmentUponApprovalRuleSet(this.otModalData.assessmentId, this.forceOverride).then((res: IProtocolResponse<IAssessmentNeedsConfirmation>): void => {
            if (res.result) {
                this.approveModalData = this.assessmentDetailAction.getNewApproveAssessmentConfirmationText(res.data.needsConfirmation);
                if (this.approveModalData.needsConfirmation) {
                    this.excludeRules = true;
                }
            }
        }).finally(() => {
            this.initializing = false;
        });
    }
}
