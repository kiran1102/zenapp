import { buildDefaultModalTemplate } from "modals/modal-template/modal-template";

// redux
import { getCurrentUser } from "oneRedux/reducers/user.reducer";
import {
    getAssessmentDetailSelectedSectionRespondent,
    getAssessmentDetailName,
    getAssessmentDetailSelectedSectionId,
} from "modules/assessment/reducers/assessment-detail.reducer";
import { getModalData } from "oneRedux/reducers/modal.reducer";

// interface
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import { IAssessmentUpdateRespondentRequest } from "interfaces/assessment/assessment-api.interface";
import { IOrgUserAdapted } from "interfaces/org-user.interface";
import { IStore } from "interfaces/redux.interface";
import { IStringMap } from "interfaces/generic.interface";
import { IUser } from "interfaces/user.interface";

// constants
import { AssessmentPermissions } from "constants/assessment.constants";
import { OrgUserDropdownTypes } from "enums/org-user-dropdown.enum";

// service
import { ModalService } from "sharedServices/modal.service";

class AssessmentDetailRespondentModalComponentController implements ng.IComponentController {

    static $inject: string[] = [
        "$rootScope",
        "store",
        "ModalService",
    ];

    assessmentName: string;
    comment: string;
    currentUser: IUser;
    isSubmitting = false;
    modalTitle: string;
    onRespondentSelect: () => any;
    orgGroupId: string;
    promiseToResolve: (...args: any[]) => any;
    respondentDropdownType: OrgUserDropdownTypes;
    respondentPermissions: string[] = [];
    sectionRespondent: { id: string, name: string };
    selectedRespondent: any;
    translate: (key: string) => string;

    private resolve: any;
    private close: any;
    private dismiss: any;

    constructor(
        private readonly $rootScope: IExtendedRootScopeService,
        private readonly store: IStore,
        private readonly modalService: ModalService,
    ) {
        this.translate = $rootScope.t;
    }

    $onInit(): void {
        const appState = this.store.getState();
        this.assessmentName = getAssessmentDetailName(appState);
        this.currentUser = getCurrentUser(appState);
        this.modalTitle = getModalData(appState).modalTitle;
        this.onRespondentSelect = getModalData(appState).onRespondentSelect;
        this.orgGroupId = getModalData(appState).orgGroupId;
        this.promiseToResolve = getModalData(appState).promiseToResolve;
        this.respondentPermissions = [AssessmentPermissions.AssessmentCanBeRespondent];
        const sectionId = getAssessmentDetailSelectedSectionId(appState);
        this.sectionRespondent = getAssessmentDetailSelectedSectionRespondent(sectionId)(appState);
        this.respondentDropdownType = OrgUserDropdownTypes.RespondentPia;
    }

    respondentSelected(selection: any, isValid: boolean): void {
        if (isValid) {
            this.selectedRespondent = selection;
        } else {
            this.selectedRespondent = null;
        }
    }

    closeModal(): void {
        this.modalService.closeModal();
        this.modalService.clearModalData();
    }

    handleInputChange(value: string): void {
        this.comment = value;
    }

    submitModal(): void {
        this.isSubmitting = true;
        const payload: IAssessmentUpdateRespondentRequest = {
            respondentId: this.selectedRespondent.Id ? this.selectedRespondent.Id : null,
            respondentName: this.selectedRespondent.FullName ? this.selectedRespondent.FullName : this.selectedRespondent,
            comment: this.comment,
            sectionId: "",
        };
        this.promiseToResolve(payload)
            .then((success: boolean): ng.IPromise<void> | boolean => {
                if (success) {
                    this.modalService.closeModal();
                    this.modalService.clearModalData();
                    return true;
                }
                this.isSubmitting = false;
                return false;
            });
    }
}

const assessmentDetailRespondentModalTemplate: IStringMap<string> = {
    template: `
            <section class="flex flex-full-height padding-all-2">
                <div
                    class="text-center margin-bottom-2"
                    ng-if="$ctrl.sectionRespondent"
                >
                    <span ng-if="$ctrl.sectionRespondent.id !== $ctrl.currentUser.Id">
                        {{::$ctrl.translate("ProjectIsAssignedToAssigneeName", {ProjectName : $ctrl.assessmentName, AssigneeName: $ctrl.sectionRespondent.name}) }}
                    </span>
                    <span ng-if="$ctrl.sectionRespondent.id === $ctrl.currentUser.Id">
                        {{::$ctrl.translate("ProjectIsAssignedToAssigneeNameMe", {ProjectName : $ctrl.assessmentName, AssigneeName: $ctrl.sectionRespondent.name}) }}
                    </span>
                </div>
                <div class="text-center margin-bottom-2" ng-if="!$ctrl.sectionRespondent">
                    <span>{{::$ctrl.translate("ProjectIsNotCurrentlyAssignedToAnyone", {ProjectName : $ctrl.assessmentName})}}</span>
                </div>

                <!-- Respondent dropdown -->
                <org-user-dropdown
                    allow-other="true"
                    text-error="{{::$ctrl.translate('EnterAValidEmailAddress')}}"
                    do-validate="true"
                    is-required="true"
                    label="{{::$ctrl.translate('AssignRespondent')}}"
                    other-option-prepend-text="{{::$ctrl.translate('ReAssign')}}"
                    on-select="$ctrl.respondentSelected(selection, isValid)"
                    org-id="$ctrl.orgGroupId"
                    permission-keys="$ctrl.respondentPermissions"
                    traversal="'BRANCH'"
                    wrapper-class="margin-bottom-2 form-group"
                    type="$ctrl.respondentDropdownType"
                >
                </org-user-dropdown>

                <!--comments to send email to the new respondent -->
                <one-label-content
                    body-class="full-width"
                    name="{{::$ctrl.translate('Comments')}}"
                    wrapper-class="margin-bottom-2 form-group"
                >
                    <one-textarea
                        identifier="AssessmentRespondentModalCommentTextBox"
                        input-changed="$ctrl.handleInputChange(value)"
                        input-text="{{$ctrl.comment}}"
                        placeholder="{{::$ctrl.translate('RespondentCommentPlaceholder')}}"
                    >
                    </one-textarea>
                </one-label-content>

                <!-- Footer -->
                <footer class="row-horizontal-center margin-top-2">
                    <one-button
                        identifier="AssessmentRespondentModalReAssignButton"
                        button-click="$ctrl.submitModal()"
                        enable-loading="true"
                        is-loading="$ctrl.isSubmitting"
                        is-disabled="!$ctrl.selectedRespondent || $ctrl.isSubmitting"
                        text="{{::$ctrl.translate('ReAssign')}}"
                        type="primary"
                    ></one-button>
                </footer>
            </section>
        `,
};

export const assessmentDetailRespondentModalComponent: ng.IComponentOptions = {
    controller: AssessmentDetailRespondentModalComponentController,
    bindings: {
        resolve: "<",
        close: "&",
        dismiss: "&",
    },
    template: buildDefaultModalTemplate(assessmentDetailRespondentModalTemplate.template),
};
