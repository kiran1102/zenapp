export enum AACollaborationCountsTypes {
    Notes = "NOTES",
    Comments = "COMMENTS",
    Nmi = "NMI",
    Risks = "RISKS",
    All = "ALL",
}
