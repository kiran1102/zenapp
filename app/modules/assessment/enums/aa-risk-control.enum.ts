export enum RiskControlStatusNames {
    PENDING = "Pending",
    IMPLEMENTED =  "Implemented",
    NOT_DOING = "NotDoing",
}

export enum RiskControlActionNames {
    UPDATE_STATUS = "UPDATE_STATUS",
    REMOVE_CONTROL = "REMOVE_CONTROL",
}
