export enum AATranslationKeys {
    asssessmentNewIncidentNotification = "Asssessment.NewIncidentNotification",
    asssessmentExistingIncidentNotification = "Assessment.ExistingIncidentNotification",
}
