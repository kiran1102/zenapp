export enum AAFilterTypes {
    State = 10,
    Template = 20,
    RiskLevel = 30,
    Organization = 40,
    Creator = 50,
    Respondent = 51,
    Approver = 52,
    DateCreated = 60,
    Deadline = 61,
    Tags = 70,
    OpenInfoRequest = 80,
    OpenRiskCount = 81,
    Result = 90,
}

export enum AAFilterId {
    Approver = "approverId",
    Creator = "createdBy",
    DateCreated = "createDT",
    Deadline = "deadline",
    OpenInfoRequest = "openInfoRequestCount",
    OpenRiskCount = "openRiskCount",
    Organization = "orgGroupId",
    Respondent = "respondentId",
    RiskLevel = "assessmentRiskLevelId",
    State = "status",
    Tags = "tagId",
    Template = "templateRootVersionId",
    Result = "result",
}

export enum AASectionFilterTypes {
    AllQuestions = "AllQuestions",
    RequiredQuestions = "Required",
    UnansweredQuestions = "Unanswered",
    RequiredAndUnansweredQuestions = "RequiredAndUnanswered",
}
