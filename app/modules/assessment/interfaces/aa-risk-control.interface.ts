// Interface
import { IControlInformation } from "modules/controls/shared/interfaces/controls.interface";
import { ILabelValue } from "interfaces/generic.interface";

// Enums
import { RiskControlStatusNames } from "../enums/aa-risk-control.enum";

export interface IRiskControlInformation {
    id: string;
    status: RiskControlStatusNames;
    control: IControlInformation;
}

export interface IRiskControlUpdateModalData {
    riskControl: IRiskControlInformation;
    riskControlStatusList: Array<ILabelValue<string>>;
}
