export interface INeedsMoreInfoButtonConfig {
    name: string;
    identifier: string;
    isDisabled: boolean;
    isLoading: boolean;
    id: string;
}
