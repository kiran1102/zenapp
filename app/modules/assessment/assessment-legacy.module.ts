declare const angular: any;
import { assessmentLegacyRoutes } from "./assessment-legacy.routes";

import { AssessmentDetailApiService } from "modules/assessment/services/api/assessment-detail-api.service";
import { AssessmentDetailAdapterService } from "modules/assessment/services/adapter/assessment-detail-adapter.service";
import { AssessmentDetailViewScrollerService } from "modules/assessment/services/view-scroller/assessment-detail-view-scroller.service";

export const assessmentLegacyModule = angular
    .module("assessment-module", [
        "ui.router",
        "ngSanitize",
    ])
    .config(assessmentLegacyRoutes)

    .service("AssessmentDetailApi", AssessmentDetailApiService)
    .service("AssessmentDetailAdapter", AssessmentDetailAdapterService)
    .service("AssessmentDetailViewScroller", AssessmentDetailViewScrollerService)
;
