import { NgModule } from "@angular/core";
import { SharedModule } from "sharedModules/shared.module";
import { UpgradedLegacyModule } from "sharedModules/upgrade/upgraded-legacy.module";
import { CommonModule } from "@angular/common";
import { HttpClientModule } from "@angular/common/http";
import { FormsModule } from "@angular/forms";
import { ReactiveFormsModule } from "@angular/forms";
import { VitreusModule } from "@onetrust/vitreus";
import { DndListModule } from "ngx-drag-and-drop-lists";
import { OTPipesModule, PIPES } from "modules/pipes/pipes.module";
import { VirtualScrollModule } from "od-virtualscroll";
import { OneVideoModule } from "modules/video/video.module";
import { TemplateSharedModule } from "modules/template-shared/template-shared.module";
import { AAPipesModule, PIPES as AAPipes } from "modules/assessment/pipes/aa-pipes.module";
import { FilterModule } from "modules/filter/filter.module";
import { AADetailModule } from "modules/aa-detail/aa-detail.module";
import { AASectionFooterModule } from "modules/aa-section-footer/aa-section-footer.module";

// Components
import { AAMetadataModalComponent } from "modules/assessment/components/aa-metadata-modal/aa-metadata-modal.component";
import { AAActivityModalComponent } from "modules/assessment/components/aa-assessment-activity-modal/aa-assessment-activity-modal.component";
import { AAAttachmentsModalComponent } from "modules/assessment/components/aa-attachments-modal/aa-attachments-modal.component";
import { AAUnapprovalModal } from "modules/assessment/components/aa-unapproval-modal/aa-unapproval-modal.component";
import { AAUndoReviewModalComponent } from "modules/assessment/components/aa-undo-review-modal/aa-undo-review-modal.component";
import { AABulkLaunchComponent } from "modules/assessment/components/aa-bulk-launch/aa-bulk-launch.component";
import { AACommentsModalComponent } from "modules/assessment/components/aa-comments-modal/aa-comments-modal.component";
import { AAListComponent } from "./components/aa-list/aa-list.component";
import { AANeedsMoreInfoModalComponent } from "./components/aa-needs-more-info-modal/aa-needs-more-info-modal.component";
import { AAQuestionActivityStreamModalComponent } from "modules/assessment/components/aa-question-activity-stream/aa-question-activity-stream.component";
import { AATemplateSelectionComponent } from "modules/assessment/components/aa-template-selection/aa-template-selection.component";
import { AssessmentBulkLaunchCardsComponent } from "modules/assessment/components/assessment-bulk-launch-cards/assessment-bulk-launch-cards.component";
import { AssessmentBulkLaunchControlsComponent } from "modules/assessment/components/assessment-bulk-launch-controls/assessment-bulk-launch-controls.component";
import { AssessmentBulkLaunchFooterComponent } from "modules/assessment/components/assessment-bulk-launch-footer/assessment-bulk-launch-footer.component";
import { AssessmentDetailRelatedDataSubjectModalComponent } from "./components/assessment-detail-question-related-data-subject-modal/assessment-detail-question-related-data-subject-modal.component";
import { AssessmentGridComponent } from "./components/assessment-grid/assessment-grid.component";
import { AACreateFormComponent } from "modules/assessment/components/aa-create-form/aa-create-form.component";
import { AASendBackModalComponent } from "modules/assessment/components/aa-send-back-modal/aa-send-back-modal.component";
import { AACopyAssessmentCard } from "modules/assessment/components/aa-copy-assessment/aa-copy-assessment-card/aa-copy-assessment-card.component";
import { AACopyAssessmentComponent } from "modules/assessment/components/aa-copy-assessment/aa-copy-assessment.component";
import { AACopyAssessmentBulkControlsComponent } from "modules/assessment/components/aa-copy-assessment-bulk-controls/aa-copy-assessment-bulk-controls.component";
import { AARiskModalComponent } from "modules/assessment/components/aa-risk-modal/aa-risk-modal.component";
import { AaReviewChangedResponseComponent } from "modules/assessment/components/aa-review-changed-responses-modal/aa-review-changed-responses.component";
import { AaReviewTemplateUpdatesComponent } from "modules/assessment/components/aa-review-updates-modal/aa-review-updates.component";
import { AAListFilterComponent } from "./components/aa-list-filter/aa-list-filter.component";
import { AAColumnsEditorModalComponent } from "./components/aa-columns-editor-modal/aa-columns-editor-modal.component";
import { AaFinishReviewModalComponent } from "./components/aa-finish-review-modal/aa-finish-review-modal.component";

// Services
import { AaProtocolService } from "assessmentServices/api/aa-protocol.service";
import { AAActivityActionService } from "modules/assessment/services/action/aa-assessment-activity-action.service";
import { AAActivityStreamApiService } from "modules/assessment/services/api/aa-activity-stream-api.service";
import { AAAttachmentApiService } from "modules/assessment/services/api/aa-attachment-api.service";
import { AABulkLaunchAdapterService } from "modules/assessment/services/adapter/aa-bulk-launch-adapter.service";
import { AACommentApiService } from "modules/assessment/services/api/aa-comment-api.service";
import { AAIncidentAdapterService } from "modules/assessment/services/adapter/aa-incident-adapter.service";
import { AANeedsMoreInfoActionService } from "modules/assessment/services/action/aa-needs-more-info-action.service";
import { AANeedsMoreInfoApiService } from "modules/assessment/services/api/aa-needs-more-info-api.service";
import { AAQuestionCommentViewLogicService } from "modules/assessment/services/view-logic/aa-question-comment-view-logic.service";
import { AATemplateApiService } from "modules/assessment/services/api/aa-template-api.service";
import { AssessmentListApiService } from "modules/assessment/services/api/assessment-list-api.service";
import { AssessmentRiskV2PermissionService } from "modules/assessment/services/permission/assessment-risk-v2-permission.service";
import { AssessmentSectionDetailHeaderPermissionService } from "modules/assessment/services/permission/assessment-detail-header-permission.service";
import { AssessmentSectionDetailHeaderViewLogicService } from "modules/assessment/services/view-logic/assessment-detail-header-view-logic.service";
import { RiskTrackingActionConfig } from "modules/assessment/services/configuration/risk-tracking-action-config.service";
import { AAQuestionActivityAdapterService } from "modules/assessment/services/adapter/aa-question-activity-adapter.service";
import { AaNotesActionService } from "modules/assessment/services/action/aa-notes-action.service";
import { ShareAssessmentService } from "modules/assessment/services/api/share-assessment-api.service";
import { AssessmentDetailService } from "modules/assessment/services/view-logic/assessment-detail.service";
import { AaDetailActionService } from "modules/assessment/services/action/aa-detail-action.service";
import { AaChangedResponsesService } from "modules/assessment/services/api/aa-changed-responses-service";
import { AssessmentDetailApiService } from "modules/assessment/services/api/assessment-detail-api.service";
import { AaDetailSchedulerService } from "modules/assessment/services/scheduler/aa-detail-scheduler.service";
import { AAListFilterService } from "./services/filter/aa-list-filter.service";
import { AssessmentSectionQuestionViewLogicService } from "modules/assessment/services/view-logic/assessment-section-question-view-logic.service";
import { AssessmentRiskViewLogicService } from "modules/assessment/services/view-logic/assessment-risk-view-logic.service";
import { AaSectionQuestionPermissionService } from "modules/assessment/services/permission/aa-section-question-permission.service";
import { AAListHelperService } from "./components/aa-list/aa-list-helper.service";
import { AARiskControlApiService } from "./services/api/aa-risk-control-api.service";
import { AaRiskControlActionService } from "./services/action/aa-risk-control-link-controls-action.service";
import { AaSectionNavMessagingService } from "./services/messaging/aa-section-messaging.service";
import { AASectionApiService } from "./services/api/aa-section-api.service";
import { AaCollaborationPaneCountsService } from "modules/assessment/services/messaging/aa-section-detail-header-helper.service";
import { AssessmentDetailHeaderApiService } from "modules/assessment/services/api/aa-assessment-detail-header-api.service";

// Legacy Services
import { AssessmentDetailViewScrollerService } from "modules/assessment/services/view-scroller/assessment-detail-view-scroller.service";
import { AssessmentMultichoiceDropdownViewLogicService } from "modules/assessment/services/view-logic/assessment-multichoice-dropdown-view-logic.service";
import { AssessmentRiskActionService } from "modules/assessment/services/action/assessment-risk-action.service";
import { AssessmentRiskApiService } from "modules/assessment/services/api/assessment-risk-api.service";
import { OrgGroupApiService } from "oneServices/api/org-group-api.service";
import { OrgUserDropdownService } from "generalcomponent/org-user-dropdown/org-user-dropdown.service";
import { AssessmentDetailAdapterService } from "modules/assessment/services/adapter/assessment-detail-adapter.service";

export function getLegacyAssessmentDetailAdapterService(injector) {
    return injector.get("AssessmentDetailAdapter");
}

export function getLegacyViewScrollService(injector) {
    return injector.get("AssessmentDetailViewScroller");
}

export function getLegacyAssessmentRiskViewLogic(injector) {
    return injector.get("AssessmentRiskViewLogic");
}

export function getLegacyOrgGroupApiService(injector) {
    return injector.get("OrgGroups");
}

export function getLegacyOrgUserDropdownService(injector) {
    return injector.get("OrgUserDropdownService");
}

export function getLegacyExportService(injector) {
    return injector.get("Export");
}

@NgModule({
    imports: [
        SharedModule,
        UpgradedLegacyModule,
        CommonModule,
        HttpClientModule,
        FormsModule,
        ReactiveFormsModule,
        VitreusModule,
        DndListModule,
        OTPipesModule,
        VirtualScrollModule,
        OneVideoModule,
        TemplateSharedModule,
        AAPipesModule,
        FilterModule,
        AADetailModule,
        AASectionFooterModule,
    ],
    declarations: [
        AAMetadataModalComponent,
        AAActivityModalComponent,
        AAAttachmentsModalComponent,
        AAUnapprovalModal,
        AAUndoReviewModalComponent,
        AABulkLaunchComponent,
        AACommentsModalComponent,
        AAListComponent,
        AANeedsMoreInfoModalComponent,
        AAQuestionActivityStreamModalComponent,
        AATemplateSelectionComponent,
        AssessmentBulkLaunchCardsComponent,
        AssessmentBulkLaunchControlsComponent,
        AssessmentBulkLaunchFooterComponent,
        AssessmentDetailRelatedDataSubjectModalComponent,
        AssessmentGridComponent,
        AASendBackModalComponent,
        AACreateFormComponent,
        AACopyAssessmentComponent,
        AACopyAssessmentCard,
        AACopyAssessmentBulkControlsComponent,
        AARiskModalComponent,
        AaReviewChangedResponseComponent,
        AAListFilterComponent,
        AaReviewTemplateUpdatesComponent,
        AAColumnsEditorModalComponent,
        AaFinishReviewModalComponent,
    ],
    // TODO: Clean up which are not needed here
    entryComponents: [
        AAMetadataModalComponent,
        AAActivityModalComponent,
        AAAttachmentsModalComponent,
        AAUnapprovalModal,
        AAUndoReviewModalComponent,
        AABulkLaunchComponent,
        AACommentsModalComponent,
        AAListComponent,
        AANeedsMoreInfoModalComponent,
        AAQuestionActivityStreamModalComponent,
        AATemplateSelectionComponent,
        AssessmentDetailRelatedDataSubjectModalComponent,
        AACreateFormComponent,
        AASendBackModalComponent,
        AACopyAssessmentComponent,
        AARiskModalComponent,
        AaReviewChangedResponseComponent,
        AaReviewTemplateUpdatesComponent,
        AAColumnsEditorModalComponent,
        AaFinishReviewModalComponent,
    ],
    providers: [
        { provide: AssessmentDetailViewScrollerService, deps: ["$injector"], useFactory: getLegacyViewScrollService },
        { provide: AssessmentRiskViewLogicService, deps: ["$injector"], useFactory: getLegacyAssessmentRiskViewLogic },
        { provide: OrgGroupApiService, deps: ["$injector"], useFactory: getLegacyOrgGroupApiService },
        { provide: OrgUserDropdownService, deps: ["$injector"], useFactory: getLegacyOrgUserDropdownService },
        { provide: "Export", deps: ["$injector"], useFactory: getLegacyExportService },
        { provide: AssessmentDetailAdapterService, deps: ["$injector"], useFactory: getLegacyAssessmentDetailAdapterService },
        AaProtocolService,
        AaSectionNavMessagingService,
        AASectionApiService,
        AaCollaborationPaneCountsService,
        AssessmentMultichoiceDropdownViewLogicService,
        AAQuestionCommentViewLogicService,
        AssessmentDetailApiService,
        AssessmentDetailHeaderApiService,
        AssessmentRiskViewLogicService,
        AssessmentSectionDetailHeaderViewLogicService,
        AssessmentSectionDetailHeaderPermissionService,
        AANeedsMoreInfoActionService,
        AANeedsMoreInfoApiService,
        AAAttachmentApiService,
        RiskTrackingActionConfig,
        AssessmentRiskV2PermissionService,
        AATemplateApiService,
        AssessmentListApiService,
        AACommentApiService,
        AAActivityStreamApiService,
        AAActivityActionService,
        AABulkLaunchAdapterService,
        AAIncidentAdapterService,
        AAQuestionActivityAdapterService,
        AaNotesActionService,
        ShareAssessmentService,
        AssessmentDetailService,
        AaDetailActionService,
        AaChangedResponsesService,
        AaDetailSchedulerService,
        AAListFilterService,
        AssessmentSectionQuestionViewLogicService,
        AssessmentRiskActionService,
        AssessmentRiskApiService,
        AaSectionQuestionPermissionService,
        AAListHelperService,
        AARiskControlApiService,
        AaRiskControlActionService,
        PIPES, // TODO: Remove from providers when ot-common is implemented. Only necessary if you are using pipe in TS file, else just module is fine
        ...AAPipes,
    ],
})
export class AssessmentModule { }
