export type RiskV2UpdateActions = "RECOMMENDATION_SEND" | "EXCEPTION_GRANTED" | "EXCEPTION_REJECTED" | "REMEDIATION_APPROVED" | "REMEDIATION_REJECTED" | "REMEDIATION_PROPOSED" | "EXCEPTION_REQUESTED";
export type RiskV2Fields = "mitigation" | "recommendation" | "requestedException";
