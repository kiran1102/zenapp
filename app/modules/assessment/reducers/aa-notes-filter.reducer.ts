import { AnyAction } from "redux";
import { Selector } from "reselect";

// Enum
import { AANotesTypes } from "enums/assessment/assessment-comment-types.enum";

// Interfaces
import { INoteState } from "interfaces/note.interface.ts";
import { IStoreState } from "interfaces/redux.interface";
import { ILabelValue } from "interfaces/generic.interface";

export const AANoteActions = {
    SET_NOTES_FILTER: "SET_NOTES_FILTER",
};

export const initialNoteState: INoteState = {
    filter: null,
};

export function getNotesFilter(state: INoteState = initialNoteState, action: AnyAction): INoteState {
    switch (action.type) {
        case AANoteActions.SET_NOTES_FILTER:
            return {
                ...state,
                filter: action.filterState,
            };
        default:
            return state;
    }
}

export const getNotesFilterState: Selector<IStoreState, ILabelValue<AANotesTypes>> = (state: IStoreState): ILabelValue<AANotesTypes> => state.assessment.notes.filter;
