// lodash
import {
    merge,
    map,
    filter,
} from "lodash";
import { Selector } from "reselect";

// interfaces
import { AnyAction } from "redux";
import { IStoreState } from "interfaces/redux.interface";
import { IAssessmentCommentState } from "interfaces/assessment/assessment-comment-reducer.interface";
import { IAssessmentCommentModel } from "interfaces/assessment/assessment-comment-model.interface";

export const AssessmentCommentAction = {
    SET_COMMENT: "SET_COMMENT",
    RESET_COMMENT: "RESET_COMMENT",
    REMOVE_COMMENT: "REMOVE_COMMENT",
    UPDATE_COMMENT: "UPDATE_COMMENT",
    ADD_COMMENT: "ADD_COMMENT",
};

export const initialAssessmentCommentState: IAssessmentCommentState = {
    commentModel: null,
};

export function assessmentComment(state: IAssessmentCommentState = initialAssessmentCommentState, action: AnyAction): IAssessmentCommentState {
    switch (action.type) {
        case AssessmentCommentAction.SET_COMMENT:
            return {
                ...state,
                commentModel: action.commentModel,
            };
        case AssessmentCommentAction.ADD_COMMENT:
            const comments = [...state.commentModel];
            comments.unshift(action.comment);
            return {
                ...state,
                commentModel: comments,
            };
        case AssessmentCommentAction.UPDATE_COMMENT:
            const commentList = map(state.commentModel, (comment: IAssessmentCommentModel): IAssessmentCommentModel => {
                if (comment.id === action.comment.id) {
                    return { ...action.comment };
                }
                return comment;
            });
            return {
                ...state,
                commentModel: commentList,
            };
        case AssessmentCommentAction.REMOVE_COMMENT:
            return {
                ...state,
                commentModel: filter(state.commentModel, (comment: IAssessmentCommentModel) => comment.id !== action.commentId),
            };
        case AssessmentCommentAction.RESET_COMMENT:
            return {
                ...state,
                commentModel: [],
            };
        default:
            return state;
    }
}

export const getAssessmentCommentState: Selector<IStoreState, IAssessmentCommentState> = (state: IStoreState): IAssessmentCommentState => state.assessment.assessmentComment;
