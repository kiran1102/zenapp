
import { AnyAction } from "redux";
import {
    Selector,
} from "reselect";

// Interfaces
import { INeedsMoreInfoState } from "interfaces/assessment/needs-more-info.interface";
import {
    IStoreState,
} from "interfaces/redux.interface";
import { ILabelValue } from "interfaces/generic.interface";

export const AANeedsMoreInfoReducerAction = {
    SET_NEEDS_MORE_INFO: "SET_NEEDS_MORE_INFO",
    SET_FILTER_STATE: "SET_FILTER_STATE",
};

export const initialNeedsMoreInfoState: INeedsMoreInfoState = {
    needsMoreInfoModel: null,
    filterState: null,
};

export function aaNeedsMoreInfo(state: INeedsMoreInfoState = initialNeedsMoreInfoState, action: AnyAction): INeedsMoreInfoState {
    switch (action.type) {
        case AANeedsMoreInfoReducerAction.SET_NEEDS_MORE_INFO:
        return {
            ...state,
            needsMoreInfoModel: action.needsMoreInfoModel,
        };
        case AANeedsMoreInfoReducerAction.SET_FILTER_STATE:
        return {
            ...state,
            filterState: action.filterState,
        };
        default:
            return state;
    }
}

export const getNeedsMoreInfoState: Selector<IStoreState, INeedsMoreInfoState> = (state: IStoreState): INeedsMoreInfoState => state.assessment.aaNeedsMoreInfoDetail;
export const getNeedsMoreInfoFilterState: Selector<IStoreState, ILabelValue<string>> = (state: IStoreState): ILabelValue<string> => state.assessment.aaNeedsMoreInfoDetail.filterState;
