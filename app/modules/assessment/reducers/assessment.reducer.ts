import { AnyAction } from "redux";
import {
    createSelector,
    Selector,
} from "reselect";

// interfaces
import { IAssessmentState } from "interfaces/assessment/assessment-reducer.interface";
import { IStoreState } from "interfaces/redux.interface";

import {
    assessmentDetail,
    AssessmentDetailAction,
    initialAssessmentDetailState,
} from "modules/assessment/reducers/assessment-detail.reducer";
import {
    AssessmentCommentAction,
    assessmentComment,
    initialAssessmentCommentState,
} from "modules/assessment/reducers/assessment-comment.reducer";
import {
    AANeedsMoreInfoReducerAction,
    aaNeedsMoreInfo,
    initialNeedsMoreInfoState,
} from "modules/assessment/reducers/aa-needs-more-info.reducer";
import {
    initialNoteState,
    AANoteActions,
    getNotesFilter,
 } from "modules/assessment/reducers/aa-notes-filter.reducer";

export const AssessmentAction = {
    SET_PRESELECTED_RISKID: "SET_PRESELECTED_RISKID",
    SET_PRESELECTED_ROOTREQUESTID: "SET_PRESELECTED_ROOTREQUESTID",
    SET_PRESELECTED_COMMENT_QUESTION_ID: "SET_PRESELECTED_COMMENT_QUESTION_ID",
};

const initialState: IAssessmentState = {
    allIds: [],
    byId: {},
    selectedAssessmentIds: {},
    aaNeedsMoreInfoDetail: initialNeedsMoreInfoState,
    assessmentDetail: initialAssessmentDetailState,
    assessmentComment: initialAssessmentCommentState,
    preSelectedRiskId: null,
    preSelectedRootRequestId: null,
    preSelectedCommentQuestionId: null,
    notes : initialNoteState,
};

export function assessment(state = initialState, action: AnyAction): IAssessmentState {
    if (AssessmentDetailAction[action.type]) {
        return {
            ...state,
            assessmentDetail: assessmentDetail(state.assessmentDetail, action),
        };
    }

    if (AssessmentCommentAction[action.type]) {
        return {
            ...state,
            assessmentComment: assessmentComment(state.assessmentComment, action),
        };
    }

    if (AANeedsMoreInfoReducerAction[action.type]) {
        return {
            ...state,
            aaNeedsMoreInfoDetail: aaNeedsMoreInfo(state.aaNeedsMoreInfoDetail, action),
        };
    }

    if (AANoteActions[action.type]) {
        return {
            ...state,
            notes: getNotesFilter(state.notes, action),
        };
    }

    switch (action.type) {
        case AssessmentAction.SET_PRESELECTED_RISKID:
            return {
                ...state,
                preSelectedRiskId: action.preSelectedRiskId,
                preSelectedRootRequestId: "",
                preSelectedCommentQuestionId: "",
            };
        case AssessmentAction.SET_PRESELECTED_ROOTREQUESTID:
            return {
                ...state,
                preSelectedRiskId: "",
                preSelectedRootRequestId: action.preSelectedRootRequestId,
                preSelectedCommentQuestionId: "",
            };
        case AssessmentAction.SET_PRESELECTED_COMMENT_QUESTION_ID:
            return {
                ...state,
                preSelectedRiskId: "",
                preSelectedRootRequestId: "",
                preSelectedCommentQuestionId: action.preSelectedCommentQuestionId,
            };
        default:
            return state;
    }
}

export const assessmentState: Selector<IStoreState, IAssessmentState> = (state: IStoreState): IAssessmentState => state.assessment;
export const getAssessmentModelById = (id: string) => createSelector(assessmentState, (state: IAssessmentState) => state.byId[id]);
export const getPreSelectedRiskId = createSelector(assessmentState, (state: IAssessmentState) => state.preSelectedRiskId);
export const getPreSelectedRootRequestId = createSelector(assessmentState, (state: IAssessmentState) => state.preSelectedRootRequestId);
export const getPreSelectedCommentQuestionId = createSelector(assessmentState, (state: IAssessmentState) => state.preSelectedCommentQuestionId);
