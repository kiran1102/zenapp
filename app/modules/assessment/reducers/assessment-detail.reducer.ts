import { AnyAction } from "redux";
import {
    createSelector,
    Selector,
} from "reselect";
import {
    merge,
    get,
    set,
    omit,
    map,
    filter,
    isNull,
    includes,
    assign,
    uniq,
    uniqueId,
    forEach,
    forIn,
    omitBy,
    keys,
    some,
    remove,
} from "lodash";

// Interfaces
import {
    IStoreState,
    IFlattenedState,
} from "interfaces/redux.interface";
import { IAssessmentDetailState } from "interfaces/assessment/assessment-detail-state.interface";
import {
    IAssessmentModel,
    IAssessmentTemplate,
    IAssessmentApprover,
 } from "interfaces/assessment/assessment-model.interface";
import {
    IQuestionModel,
    IQuestionModelOption,
    IQuestionModelSubQuestion,
    IQuestionResponsesState,
} from "interfaces/assessment/question-model.interface";
import { ISectionModel } from "interfaces/assessment/section-model.interface";
import {
    IQuestionResponseV2,
    IQuestionModelResponse,
    IAssessementDataElementResponse,
    IPersonalDataQuestionResponse,
    IPersonalDataCategory,
    IPersonalDataUpdateResponseMap,
} from "interfaces/assessment/question-response-v2.interface";
import {
    IStringMap,
    INameId,
} from "interfaces/generic.interface";
import { ISectionHeaderInfo } from "interfaces/assessment/section-header-info.interface";
import { IRiskMetadata } from "interfaces/risk.interface";
import { IAssessmentCreationDetails } from "interfaces/assessment/assessment-creation-details.interface";
import { ISectionHeaderReducerState } from "interfaces/assessment/section-header-reducer-state.interface";
import { IOrgUserAdapted } from "interfaces/org-user.interface";

// Constants
import { EmptyGuid } from "constants/empty-guid.constant";
import { QuestionResponseType } from "constants/question-types.constant";
import { AssessmentStatus } from "enums/assessment/assessment-status.enum";
import { IAssessmentTemplateQuestion, IAssessmentTemplateSection } from "modules/template/interfaces/template.interface";
import { TemplateTypes, TemplateType } from "constants/template-types.constant";

export const AssessmentDetailAction = {
    LOADING_ASSESSMENT_PAGE: "LOADING_ASSESSMENT_PAGE",
    LOADING_ASSESSMENT_SECTION: "LOADING_ASSESSMENT_SECTION",
    FINISH_LOADING_ASSESSMENT_SECTION: "FINISH_LOADING_ASSESSMENT_SECTION",
    UPDATE_IS_LOADING_MESSAGE: "UPDATE_IS_LOADING_MESSAGE",

    UPDATE_ASSESSMENT_ALL: "UPDATE_ASSESSMENT_ALL",
    UPDATE_ASSESSMENT_PARTIAL: "UPDATE_ASSESSMENT_PARTIAL",
    SET_CURRENT_SECTION: "SET_CURRENT_SECTION",
    UPDATE_TEMP_ASSESSMENT: "UPDATE_TEMP_ASSESSMENT",
    ACTIVATE_SCHEDULER: "ACTIVATE_SCHEDULER",
    RESET_ASSESSMENT_DETAIL_STATE: "RESET_ASSESSMENT_DETAIL_STATE",
    TOGGLE_SECTION_ACCORDION: "TOGGLE_SECTION_ACCORDION",
    CLOSE_SECTION_ACCORDIONS: "CLOSE_SECTION_ACCORDIONS",
    SUBMITTING_ASSESSMENT: "SUBMITTING_ASSESSMENT",
    SUBMITTING_ASSESSMENT_COMPLETE: "SUBMITTING_ASSESSMENT_COMPLETE",
    SET_SAVE_IN_PROGRESS: "SET_SAVE_IN_PROGRESS",
    SET_BULK_LAUNCH_ASSESSMENTS: "SET_BULK_LAUNCH_ASSESSMENTS",

    ADD_QUESTION_RESPONSE: "ADD_QUESTION_RESPONSE",
    ADD_MULTISELECT_DROPDOWN_RESPONSES: "ADD_MULTISELECT_DROPDOWN_RESPONSES",
    REMOVE_QUESTION_RESPONSE: "REMOVE_QUESTION_RESPONSE",
    REMOVE_QUESTION_RESPONSES: "REMOVE_QUESTION_RESPONSES",
    REMOVE_QUESTION_INVALID_RESPONSES: "REMOVE_QUESTION_INVALID_RESPONSES",
    UPDATE_SUB_QUESTION_RESPONSES: "UPDATE_SUB_QUESTION_RESPONSES",
    UPDATE_JUSTIFICATION: "UPDATE_JUSTIFICATION",
    UPDATE_QUESTION_HAS_NAVIGATION_RULES: "UPDATE_QUESTION_HAS_NAVIGATION_RULES",
    UPDATE_QUESTION_HAS_OTHER_RESPONSES: "UPDATE_QUESTION_HAS_OTHER_RESPONSES",
    REMOVE_PERSONAL_DATA_QUESTION_INVALID_RESPONSES: "REMOVE_PERSONAL_DATA_QUESTION_INVALID_RESPONSES",

    UPDATE_QUESTION_IDS_TO_SAVE: "UPDATE_QUESTION_IDS_TO_SAVE",
    RESTORE_QUESTION_IDS_TO_SAVE: "RESTORE_QUESTION_IDS_TO_SAVE",
    UPDATE_QUESTION_IDS_BEING_SAVED: "UPDATE_QUESTION_IDS_BEING_SAVED",
    REMOVE_QUESTION_IDS_BEING_SAVED: "REMOVE_QUESTION_IDS_BEING_SAVED",

    ADD_SUB_QUESTION_RESPONSE: "ADD_SUB_QUESTION_RESPONSE",
    ADD_SUB_MULTISELECT_DROPDOWN_RESPONSES: "ADD_SUB_MULTISELECT_DROPDOWN_RESPONSES",
    REMOVE_SUB_QUESTION_RESPONSE: "REMOVE_SUB_QUESTION_RESPONSE",
    REMOVE_SUB_QUESTION_RESPONSES: "REMOVE_SUB_QUESTION_RESPONSES",
    REMOVE_SUB_QUESTION_INVALID_RESPONSES: "REMOVE_SUB_QUESTION_INVALID_RESPONSES",
    UPDATE_SUB_JUSTIFICATION: "UPDATE_SUB_JUSTIFICATION",

    UPDATE_SUB_QUESTION_IDS_TO_SAVE: "UPDATE_SUB_QUESTION_IDS_TO_SAVE",
    RESTORE_SUB_QUESTION_IDS_TO_SAVE: "RESTORE_SUB_QUESTION_IDS_TO_SAVE",
    UPDATE_SUB_QUESTION_IDS_BEING_SAVED: "UPDATE_SUB_QUESTION_IDS_BEING_SAVED",
    REMOVE_SUB_QUESTION_IDS_BEING_SAVED: "REMOVE_SUB_QUESTION_IDS_BEING_SAVED",

    UPDATE_ASSESSMENT_RISK: "UPDATE_ASSESSMENT_RISK",
    DELETE_ASSESSMENT_RISKS: "DELETE_ASSESSMENT_RISKS",
    UPDATE_QUESTION_INVENTORY_OPTIONS: "UPDATE_QUESTION_INVENTORY_OPTIONS",
    UPDATE_QUESTION_INVENTORY_ATTRIBUTE_USER_OPTIONS: "UPDATE_QUESTION_INVENTORY_ATTRIBUTE_USER_OPTIONS",
    UPDATE_RISK_STATISTICS: "UPDATE_RISK_STATISTICS",

    SET_TEMPLATE_METADATA: "SET_TEMPLATE_METADATA",

    ADD_INVALID_RISK_RESPONSE: "ADD_INVALID_RISK_RESPONSE",
    REMOVE_INVALID_RISK_RESPONSE: "REMOVE_INVALID_RISK_RESPONSE",
    UPDATE_QUESTION_COMMENT_COUNT: "UPDATE_QUESTION_COMMENT_COUNT",
};

export const initialAssessmentDetailState: IAssessmentDetailState = {
    assessmentModel: null,
    expandedSectionAccordion: {},
    inventoryOptions: null,
    inventoryUserOptions: null,
    isLoadingAssessment: false,
    isLoadingMessage: "LoadingSection",
    isLoadingSection: false,
    isSaveInProgress: false,
    isSubmittingAssessment: false,
    questionIdsBeingSaved: [],
    questionIdsToSave: [],
    selectedSectionId: null,
    subQuestionIdsBeingSaved: [],
    subQuestionIdsToSave: [],
    tempAssessmentModel: null,
    bulkAssessmentDetailsList: [],
    templateMetadata: null,
    invalidQuestionRisksIdMap: {},
};

// the cyclomatic-complexity of this function is increased due to many switch cases and other control flows in between
// tslint:disable-next-line:cyclomatic-complexity
export function assessmentDetail(state: IAssessmentDetailState = initialAssessmentDetailState, action: AnyAction): IAssessmentDetailState {
    switch (action.type) {
        case AssessmentDetailAction.LOADING_ASSESSMENT_PAGE:
            return { ...state, isLoadingAssessment: true };

        case AssessmentDetailAction.LOADING_ASSESSMENT_SECTION:
            return {
                ...state,
                isLoadingSection: true,
                isLoadingMessage: "LoadingSection",
                invalidQuestionRisksIdMap: {},
            };
        case AssessmentDetailAction.UPDATE_IS_LOADING_MESSAGE:
            const { message } = action;
            return {
                ...state,
                isLoadingMessage: message,
            };
        case AssessmentDetailAction.FINISH_LOADING_ASSESSMENT_SECTION:
            return {
                ...state,
                isLoadingSection: false,
                isLoadingMessage: "LoadingSection",
            };
        case AssessmentDetailAction.UPDATE_ASSESSMENT_ALL:
            const { assessmentModel } = action;
            return {
                ...state,
                isLoadingAssessment: false,
                assessmentModel: checkIfSaveIsPending(state.assessmentModel, assessmentModel, !!state.questionIdsToSave.length),
                selectedSectionId: get(assessmentModel, `section.sectionId`, EmptyGuid),
                tempAssessmentModel: merge({}, assessmentModel),
            };
        case AssessmentDetailAction.UPDATE_ASSESSMENT_PARTIAL:
            const partialModel = action.model;
            return {
                ...state,
                assessmentModel: {
                    ...state.assessmentModel,
                    sectionHeaders: {
                        ...state.assessmentModel.sectionHeaders,
                        allIds: partialModel.sectionHeaders.allIds,
                        byIds: partialModel.sectionHeaders.byIds,
                    },
                    canApproveAssessment: partialModel.canApproveAssessment,
                    canSubmitAssessment: partialModel.canSubmitAssessment,
                    firstSection: partialModel.firstSection,
                    lastSection: partialModel.lastSection,
                    status: partialModel.status,
                },
            };
        case AssessmentDetailAction.SET_CURRENT_SECTION:
            const { selectedSectionId } = action;
            return {
                ...state,
                selectedSectionId,
            };
        case AssessmentDetailAction.UPDATE_TEMP_ASSESSMENT:
            const { keyValueToMerge } = action;
            return {
                ...state,
                tempAssessmentModel: assign(state.tempAssessmentModel, keyValueToMerge),
            };
        case AssessmentDetailAction.RESET_ASSESSMENT_DETAIL_STATE: {
            return initialAssessmentDetailState;
        }
        case AssessmentDetailAction.TOGGLE_SECTION_ACCORDION: {
            const { sectionIdToToggle } = action;
            const toggleMap = {
                [sectionIdToToggle]: !state.expandedSectionAccordion[sectionIdToToggle],
            };

            return {
                ...state,
                expandedSectionAccordion: toggleMap,
            };
        }
        case AssessmentDetailAction.CLOSE_SECTION_ACCORDIONS: {
            return {
                ...state,
                expandedSectionAccordion: {},
            };
        }
        case AssessmentDetailAction.SUBMITTING_ASSESSMENT: {
            return {
                ...state,
                isSubmittingAssessment: true,
            };
        }
        case AssessmentDetailAction.SUBMITTING_ASSESSMENT_COMPLETE: {
            return {
                ...state,
                isSubmittingAssessment: false,
            };
        }
        case AssessmentDetailAction.ADD_QUESTION_RESPONSE: {
            const { questionId, response } = action;
            const { aaModel, questions, questionsById } = getStateVariables(state);
            const responses = questionsById[questionId].responses;
            const responseIds: string[] = responses ? questionsById[questionId].responses.allIds : [];
            const responsesById: Partial<Map<string, IQuestionResponseV2>> = responses ? questionsById[questionId].responses.byId : {};

            return {
                ...state,
                isSaveInProgress: true,
                assessmentModel: {
                    ...aaModel,
                    questions: {
                        ...questions,
                        byId: {
                            ...questionsById,
                            [questionId]: {
                                ...questionsById[questionId],
                                responses: {
                                    allIds: includes(responseIds, response.responseId) ? responseIds : [...responseIds, response.responseId],
                                    byId: {
                                        ...responsesById,
                                        [response.responseId]: {...response},
                                    },
                                },
                            },
                        },
                    },
                },
            };
        }
        case AssessmentDetailAction.ADD_SUB_QUESTION_RESPONSE: {
            const { questionId, subQuestionId, response } = action;
            const { aaModel, questions, questionsById } = getStateVariables(state);
            const subQuestions = questionsById[questionId].subQuestions;
            const subQuestionsById: Partial<Map<string, IQuestionModelSubQuestion>> = subQuestions ? subQuestions.byId : null;
            const subQuestionResponses = subQuestions ? subQuestionsById[subQuestionId].responses : null;
            const subQuestionResponsesAllIds: string[] = subQuestionResponses ? subQuestionResponses.allIds : [];
            const subQuestionResponsesById: Partial<Map<string, IQuestionResponseV2>> = subQuestionResponses ? subQuestionResponses.byId : {};

            return {
                ...state,
                isSaveInProgress: true,
                assessmentModel: {
                    ...aaModel,
                    questions: {
                        ...questions,
                        byId: {
                            ...questionsById,
                            [questionId]: {
                                ...questionsById[questionId],
                                subQuestions: {
                                    ...subQuestions,
                                    byId: {
                                        ...subQuestionsById,
                                        [subQuestionId]: {
                                            ...subQuestionsById[subQuestionId],
                                            responses: {
                                                allIds: includes(subQuestionResponsesAllIds, response.responseId) ? subQuestionResponsesAllIds : [...subQuestionResponsesAllIds, response.responseId],
                                                byId: {
                                                    ...subQuestionResponsesById,
                                                    [response.responseId]: {...response},
                                                },
                                            },
                                        },
                                    },
                                },
                            },
                        },
                    },
                },
            };
        }
        case AssessmentDetailAction.ADD_MULTISELECT_DROPDOWN_RESPONSES: {
            const { questionId, responsesAllIds, responsesById } = action;
            const { aaModel, questions, questionsById } = getStateVariables(state);

            return {
                ...state,
                isSaveInProgress: true,
                assessmentModel: {
                    ...aaModel,
                    questions: {
                        ...questions,
                        byId: {
                            ...questionsById,
                            [questionId]: {
                                ...questionsById[questionId],
                                responses: {
                                    allIds: responsesAllIds,
                                    byId: responsesById,
                                },
                            },
                        },
                    },
                },
            };
        }
        case AssessmentDetailAction.ADD_SUB_MULTISELECT_DROPDOWN_RESPONSES: {
            const { questionId, subQuestionId, responsesAllIds, responsesById } = action;
            const { aaModel, questions, questionsById } = getStateVariables(state);
            const subQuestions = questionsById[questionId].subQuestions;
            const subQuestionsById: Partial<Map<string, IQuestionModelSubQuestion>> = subQuestions ? subQuestions.byId : null;

            return {
                ...state,
                isSaveInProgress: true,
                assessmentModel: {
                    ...aaModel,
                    questions: {
                        ...questions,
                        byId: {
                            ...questionsById,
                            [questionId]: {
                                ...questionsById[questionId],
                                subQuestions: {
                                    ...subQuestions,
                                    byId: {
                                        ...subQuestionsById,
                                        [subQuestionId]: {
                                            ...subQuestionsById[subQuestionId],
                                            responses: {
                                                allIds: responsesAllIds,
                                                byId: responsesById,
                                            },
                                        },
                                    },
                                },
                            },
                        },
                    },
                },
            };
        }
        case AssessmentDetailAction.REMOVE_QUESTION_RESPONSE: {
            const { questionId, responseId } = action;
            const { aaModel, questions, questionsById } = getStateVariables(state);
            const responseIds: string[] = questionsById[questionId].responses.allIds;
            const responsesById: Partial<Map<string, IQuestionResponseV2>> = questionsById[questionId].responses.byId;

            return {
                ...state,
                isSaveInProgress: true,
                assessmentModel: {
                    ...aaModel,
                    questions: {
                        ...questions,
                        byId: {
                            ...questionsById,
                            [questionId]: {
                                ...questionsById[questionId],
                                responses: {
                                    allIds: responseIds.filter((id) => id !== responseId),
                                    byId: omit(responsesById, [responseId]),
                                },
                            },
                        },
                    },
                },
            };
        }
        case AssessmentDetailAction.REMOVE_SUB_QUESTION_RESPONSE: {
            const { questionId, subQuestionId, responseId } = action;
            const { aaModel, questions, questionsById } = getStateVariables(state);
            const subQuestions = questionsById[questionId].subQuestions;
            const subQuestionsById: Partial<Map<string, IQuestionModelSubQuestion>> = subQuestions ? subQuestions.byId : null;
            const subQuestionResponses = subQuestions ? subQuestionsById[subQuestionId].responses : null;
            const subQuestionResponsesAllIds: string[] = subQuestionResponses ? subQuestionResponses.allIds : [];
            const subQuestionResponsesById: Partial<Map<string, IQuestionResponseV2>> = subQuestionResponses ? subQuestionResponses.byId : {};

            return {
                ...state,
                isSaveInProgress: true,
                assessmentModel: {
                    ...aaModel,
                    questions: {
                        ...questions,
                        byId: {
                            ...questionsById,
                            [questionId]: {
                                ...questionsById[questionId],
                                subQuestions: {
                                    ...subQuestions,
                                    byId: {
                                        ...subQuestionsById,
                                        [subQuestionId]: {
                                            ...subQuestionsById[subQuestionId],
                                            responses: {
                                                allIds: subQuestionResponsesAllIds.filter((id) => id !== responseId),
                                                byId: omit(subQuestionResponsesById, [responseId]),
                                            },
                                        },
                                    },
                                },
                            },
                        },
                    },
                },
            };
        }
        case AssessmentDetailAction.REMOVE_QUESTION_RESPONSES: {
            const { questionId } = action;
            const { aaModel, questions, questionsById } = getStateVariables(state);
            const responses = state.assessmentModel.questions.byId[questionId].responses;
            const updatedResponses = { byId: {}, allIds: [] };

            return {
                ...state,
                isSaveInProgress: true,
                assessmentModel: {
                    ...aaModel,
                    questions: {
                        ...questions,
                        byId: {
                            ...questionsById,
                            [questionId]: {
                                ...questionsById[questionId],
                                responses: updatedResponses,
                            },
                        },
                    },
                },
            };
        }
        case AssessmentDetailAction.UPDATE_SUB_QUESTION_RESPONSES: {
            const { questionId, subQuestionId, updatedResponses } = action;
            const { aaModel, questions, questionsById } = getStateVariables(state);
            const subQuestions = questionsById[questionId].subQuestions;
            const subQuestionsById = subQuestions ? questionsById[questionId].subQuestions.byId : {};

            return {
                ...state,
                isSaveInProgress: true,
                assessmentModel: {
                    ...aaModel,
                    questions: {
                        ...questions,
                        byId: {
                            ...questionsById,
                            [questionId]: {
                                ...questionsById[questionId],
                                subQuestions: {
                                    ...subQuestions,
                                    byId: {
                                        ...subQuestionsById,
                                        [subQuestionId]: {
                                            ...subQuestionsById[subQuestionId],
                                            responses: updatedResponses,
                                        },
                                    },
                                },
                            },
                        },
                    },
                },
            };
        }
        case AssessmentDetailAction.REMOVE_QUESTION_INVALID_RESPONSES: {
            const { questionId } = action;
            const { aaModel, questions, questionsById } = getStateVariables(state);
            const question: IQuestionModel = questionsById[questionId];
            const validResponseIds: string[] = filter(question.responses.allIds, (id: string) => !includes(question.responses.invalidQuestionIds, id));
            const omitInvalidResponses = (response: IQuestionResponseV2): boolean => response.valid === false;
            const validResponses = omitBy(question.responses.byId, omitInvalidResponses);

            return {
                ...state,
                assessmentModel: {
                    ...aaModel,
                    questions: {
                        ...questions,
                        byId: {
                            ...questionsById,
                            [questionId]: {
                                ...questionsById[questionId],
                                responses: {
                                    allIds: validResponseIds,
                                    byId: validResponses,
                                },
                            },
                        },
                    },
                },
            };
        }
        case AssessmentDetailAction.REMOVE_SUB_QUESTION_INVALID_RESPONSES: {
            const { questionId, subQuestionId } = action;
            const { aaModel, questions, questionsById } = getStateVariables(state);
            const subQuestions: IFlattenedState<IQuestionModelSubQuestion> = questionsById[questionId].subQuestions;
            const subQuestionsById: Partial<Map<string, IQuestionModelSubQuestion>> = subQuestions ? subQuestions.byId : {};
            const subQuestion: IQuestionModelSubQuestion = subQuestionsById[subQuestionId];
            const validResponseIds: string[] = filter(subQuestion.responses.allIds, (id: string) => !includes(subQuestion.responses.invalidQuestionIds, id));
            const omitInvalidResponses = (response: IQuestionModelResponse): boolean => response.valid === false;
            const validResponses = omitBy(subQuestion.responses.byId, omitInvalidResponses);

            return {
                ...state,
                assessmentModel: {
                    ...aaModel,
                    questions: {
                        ...questions,
                        byId: {
                            ...questionsById,
                            [questionId]: {
                                ...questionsById[questionId],
                                subQuestions: {
                                    ...subQuestions,
                                    byId: {
                                        ...subQuestionsById,
                                        [subQuestionId]: {
                                            ...subQuestionsById[subQuestionId],
                                            responses: {
                                                allIds: validResponseIds,
                                                byId: validResponses,
                                            },
                                        },
                                    },
                                },
                            },
                        },
                    },
                },
            };
        }
        case AssessmentDetailAction.REMOVE_SUB_QUESTION_RESPONSES: {
            const { questionId, subQuestionId } = action;
            const { aaModel, questions, questionsById } = getStateVariables(state);
            const subQuestions = questionsById[questionId].subQuestions;
            const subQuestionsById = subQuestions ? questionsById[questionId].subQuestions.byId : {};
            const updatedResponses = { byId: {}, allIds: [], invalidQuestionIds: [] };

            return {
                ...state,
                isSaveInProgress: true,
                assessmentModel: {
                    ...aaModel,
                    questions: {
                        ...questions,
                        byId: {
                            ...questionsById,
                            [questionId]: {
                                ...questionsById[questionId],
                                subQuestions: {
                                    ...subQuestions,
                                    byId: {
                                        ...subQuestionsById,
                                        [subQuestionId]: {
                                            ...subQuestionsById[subQuestionId],
                                            responses: updatedResponses,
                                        },
                                    },
                                },
                            },
                        },
                    },
                },
            };
        }
        case AssessmentDetailAction.REMOVE_PERSONAL_DATA_QUESTION_INVALID_RESPONSES: {
            const { questionId } = action;
            const { aaModel, questions, questionsById } = getStateVariables(state);
            const question: IQuestionModel = questionsById[questionId];
            const responseById = Object.keys(question.responses.byId)[0];
            const validResponses = getValidPersonalDataResponses(question);
            const sectionId = aaModel.section.sectionId;

            return {
                ...state,
                assessmentModel: {
                    ...aaModel,
                    questions: {
                        ...questions,
                        byId: {
                            ...questionsById,
                            [questionId]: {
                                ...questionsById[questionId],
                                responses: {
                                    ...questionsById[questionId].responses,
                                    byId: {
                                        [responseById]: {
                                            ...questionsById[questionId].responses.byId[responseById],
                                            responseMap: { ...validResponses },
                                        },
                                    },
                                    invalidResponsesMap: {},
                                },
                            },
                        },
                    },
                    sectionHeaders : {
                        ...state.assessmentModel.sectionHeaders,
                        byIds : {
                            ...state.assessmentModel.sectionHeaders.byIds,
                            [sectionId] : {
                                ...state.assessmentModel.sectionHeaders.byIds[sectionId],
                                invalidQuestionCount: 0,
                            },
                        },
                    },
                },
            };
        }
        case AssessmentDetailAction.UPDATE_JUSTIFICATION: {
            const { questionId, response } = action;
            const { aaModel, questions, questionsById } = getStateVariables(state);

            return {
                ...state,
                isSaveInProgress: true,
                assessmentModel: {
                    ...aaModel,
                    questions: {
                        ...questions,
                        byId: {
                            ...questionsById,
                            [questionId]: {
                                ...questionsById[questionId],
                                justification: response,
                            },
                        },
                    },
                },
            };
        }
        case AssessmentDetailAction.UPDATE_SUB_JUSTIFICATION: {
            const { questionId, subQuestionId, response } = action;
            const { aaModel, questions, questionsById } = getStateVariables(state);
            const subQuestions = questionsById[questionId].subQuestions;
            const subQuestionsById: Partial<Map<string, IQuestionModelSubQuestion>> = subQuestions ? subQuestions.byId : null;

            return {
                ...state,
                isSaveInProgress: true,
                assessmentModel: {
                    ...aaModel,
                    questions: {
                        ...questions,
                        byId: {
                            ...questionsById,
                            [questionId]: {
                                ...questionsById[questionId],
                                subQuestions: {
                                    ...subQuestions,
                                    byId: {
                                        ...subQuestionsById,
                                        [subQuestionId]: {
                                            ...subQuestionsById[subQuestionId],
                                            justification: response,
                                        },
                                    },
                                },
                            },
                        },
                    },
                },
            };
        }
        case AssessmentDetailAction.UPDATE_QUESTION_IDS_TO_SAVE: {
            return { ...state, questionIdsToSave: action.questionIds };
        }
        case AssessmentDetailAction.UPDATE_SUB_QUESTION_IDS_TO_SAVE: {
            return {
                ...state,
                questionIdsToSave: action.questionIds,
                subQuestionIdsToSave: action.subQuestionIds,
            };
        }
        case AssessmentDetailAction.UPDATE_ASSESSMENT_RISK: {
            const { riskToAdd } = action;
            const section = state.assessmentModel.section;
            const byQuestionId = section.risks.byQuestionId[riskToAdd.questionId] || [];
            let riskAdded = false;
            const byQuestionIdToMerge = byQuestionId.map((risk: IRiskMetadata) => {
                if (riskToAdd.riskId === risk.riskId) {
                    riskAdded = true;
                    return riskToAdd;
                }
                return risk;
            });
            if (!riskAdded) {
                byQuestionIdToMerge.push(riskToAdd);
            }

            const riskToMerge = {
                allIds: section.risks.byId[riskToAdd.riskId]
                    ? section.risks.allIds
                    : section.risks.allIds.concat(riskToAdd.riskId),
                byId: {
                    ...section.risks.byId,
                    [riskToAdd.riskId]: riskToAdd,
                },
                byQuestionId: {
                    ...section.risks.byQuestionId,
                    [riskToAdd.questionId]: byQuestionIdToMerge,
                },
            };

            return {
                ...state,
                assessmentModel: {
                    ...state.assessmentModel,
                    section: {
                        ...state.assessmentModel.section,
                        risks: riskToMerge,
                    },
                },
            };
        }
        case AssessmentDetailAction.DELETE_ASSESSMENT_RISKS: {
            const { riskIds, questionId } = action;
            const riskState = state.assessmentModel.section.risks;
            const byQuestionIdToMerge = filter(riskState.byQuestionId[questionId], (risk: IRiskMetadata) => !includes(riskIds, risk.riskId));
            const allIdsToMerge = filter(riskState.allIds, (id: string) => !includes(riskIds, id));
            const byIdToMerge = omit(riskState.byId, riskIds);
            const riskToMerge = {
                allIds: allIdsToMerge,
                byId: byIdToMerge,
                byQuestionId: {
                    ...riskState.byQuestionId,
                    [questionId]: byQuestionIdToMerge,
                },
            };

            return {
                ...state,
                assessmentModel: {
                    ...state.assessmentModel,
                    section: {
                        ...state.assessmentModel.section,
                        risks: riskToMerge,
                    },
                },
            };
        }
        case AssessmentDetailAction.UPDATE_RISK_STATISTICS: {
            const { sectionId, riskStatisticsToUpdate } = action;
            const sectionHeaders = state.assessmentModel.sectionHeaders;

            const sectionHeaderToMerge = {
                riskStatisticsBySectionIds: {
                    ...sectionHeaders.riskStatisticsBySectionIds,
                    [sectionId]: riskStatisticsToUpdate,
                },
            };

            const stateToMerge = set({}, "assessmentModel.sectionHeaders", sectionHeaderToMerge);
            return merge({}, state, stateToMerge);
        }
        case AssessmentDetailAction.UPDATE_QUESTION_INVENTORY_OPTIONS: {
            const { options } = action;

            return {
                ...state,
                inventoryOptions: options,
            };
        }
        case AssessmentDetailAction.UPDATE_QUESTION_INVENTORY_ATTRIBUTE_USER_OPTIONS: {
            const { inventoryAttributeUserList } = action;

            return {
                ...state,
                inventoryUserOptions: inventoryAttributeUserList,
            };
        }
        case AssessmentDetailAction.UPDATE_QUESTION_IDS_BEING_SAVED: {
            return {
                ...state,
                questionIdsToSave: [],
                questionIdsBeingSaved: state.questionIdsToSave.concat(),
            };
        }
        case AssessmentDetailAction.REMOVE_QUESTION_IDS_BEING_SAVED: {
            return {
                ...state,
                isSaveInProgress: false,
                questionIdsBeingSaved: [],
            };
        }
        case AssessmentDetailAction.RESTORE_QUESTION_IDS_TO_SAVE: {
            return {
                ...state,
                isSaveInProgress: false,
                questionIdsToSave: uniq(state.questionIdsToSave.concat(state.questionIdsBeingSaved)),
                questionIdsBeingSaved: [],
            };
        }
        case AssessmentDetailAction.UPDATE_SUB_QUESTION_IDS_BEING_SAVED: {
            return {
                ...state,
                questionIdsToSave: [],
                questionIdsBeingSaved: state.questionIdsToSave.concat(),
                subQuestionIdsToSave: [],
                subQuestionIdsBeingSaved: state.subQuestionIdsToSave.concat(),
            };
        }
        case AssessmentDetailAction.REMOVE_SUB_QUESTION_IDS_BEING_SAVED: {
            return {
                ...state,
                isSaveInProgress: false,
                questionIdsBeingSaved: [],
                subQuestionIdsBeingSaved: [],
            };
        }
        case AssessmentDetailAction.RESTORE_SUB_QUESTION_IDS_TO_SAVE: {
            return {
                ...state,
                isSaveInProgress: false,
                questionIdsToSave: uniq(state.questionIdsToSave.concat(state.questionIdsBeingSaved)),
                questionIdsBeingSaved: [],
                subQuestionIdsToSave: uniq(state.subQuestionIdsToSave.concat(state.subQuestionIdsBeingSaved)),
                subQuestionIdsBeingSaved: [],
            };
        }
        case AssessmentDetailAction.SET_BULK_LAUNCH_ASSESSMENTS: {
            return {
                ...state,
                bulkAssessmentDetailsList: action.bulkAssessmentDetailsList,
            };
        }
        case AssessmentDetailAction.SET_TEMPLATE_METADATA: {
            return {
                ...state,
                templateMetadata: action.templateMetadata,
            };
        }
        case AssessmentDetailAction.ADD_INVALID_RISK_RESPONSE: {
            const questionIds = keys(action.invalidQuestionRisksIdMap);
            const invalidQuestionRisksIdMapToMerge: IStringMap<string[]> = {};
            forEach(questionIds, (id) => {
                invalidQuestionRisksIdMapToMerge[id] = action.invalidQuestionRisksIdMap[id];
            });

            return {
                ...state,
                invalidQuestionRisksIdMap: {
                    ...state.invalidQuestionRisksIdMap,
                    ...invalidQuestionRisksIdMapToMerge,
                },
            };
        }
        case AssessmentDetailAction.REMOVE_INVALID_RISK_RESPONSE: {
            return {
                ...state,
                invalidQuestionRisksIdMap: {
                    ...state.invalidQuestionRisksIdMap,
                    [action.questionId]: [],
                },
            };
        }
        case AssessmentDetailAction.UPDATE_QUESTION_COMMENT_COUNT: {
            const { questionId, totalComments } = action;
            const { aaModel, questions, questionsById } = getStateVariables(state);
            return {
                ...state,
                assessmentModel: {
                    ...aaModel,
                    questions: {
                        ...questions,
                        byId: {
                            ...questionsById,
                            [questionId]: {
                                ...questionsById[questionId],
                                totalComments,
                            },
                        },
                    },
                },
            };
        }
        default:
            return state;
    }
}

export const assessmentDetailState: Selector<IStoreState, IAssessmentDetailState> = (state: IStoreState): IAssessmentDetailState => state.assessment.assessmentDetail;
export const isLoadingAssessment = createSelector(assessmentDetailState, (state: IAssessmentDetailState): boolean => state.isLoadingAssessment);
export const isLoadingSection = createSelector(assessmentDetailState, (state: IAssessmentDetailState): boolean => state.isLoadingSection);
export const getIsLoadingMessage = createSelector(assessmentDetailState, (state: IAssessmentDetailState): string => state.isLoadingMessage);
export const hasQuestionIdsToSave = createSelector(assessmentDetailState, (state: IAssessmentDetailState): boolean => !!state.questionIdsToSave.length);
export const isSubmittingAssessment = createSelector(assessmentDetailState, (state: IAssessmentDetailState): boolean => state.isSubmittingAssessment);
export const isAssessmentDetailFirstSection = createSelector(assessmentDetailState, (state: IAssessmentDetailState): boolean => state.assessmentModel.firstSection);
export const isAssessmentDetailLastSection = createSelector(assessmentDetailState, (state: IAssessmentDetailState): boolean => state.assessmentModel.lastSection);
export const isAssessmentDetailWelcomeSection = createSelector(assessmentDetailState, (state: IAssessmentDetailState): boolean => state.assessmentModel.welcomeSection);
export const getExpandedSectionAccordion = createSelector(assessmentDetailState, (state: IAssessmentDetailState): {} | Map<string, boolean> => state.expandedSectionAccordion);
export const getQuestionIdsToSave = createSelector(assessmentDetailState, (state: IAssessmentDetailState): string[] => state.questionIdsToSave || []);
export const getQuestionIdsBeingSaved = createSelector(assessmentDetailState, (state: IAssessmentDetailState): string[] => state.questionIdsBeingSaved || []);
export const getSubQuestionIdsToSave = createSelector(assessmentDetailState, (state: IAssessmentDetailState): string[] => state.subQuestionIdsToSave || []);
export const getSubQuestionIdsBeingSaved = createSelector(assessmentDetailState, (state: IAssessmentDetailState): string[] => state.subQuestionIdsBeingSaved || []);
export const getAssessmentDetailAssessmentId = createSelector(assessmentDetailState, (state: IAssessmentDetailState): string => state.assessmentModel.assessmentId);
export const getAssessmentDetailCanSubmit = createSelector(assessmentDetailState, (state: IAssessmentDetailState): boolean => state.assessmentModel.canSubmitAssessment);
export const getAssessmentDetailCanApprove = createSelector(assessmentDetailState, (state: IAssessmentDetailState): boolean => state.assessmentModel.canApproveAssessment);
export const getAssessmentDetailTempModel = createSelector(assessmentDetailState, (state: IAssessmentDetailState): IAssessmentModel => state.tempAssessmentModel);
export const getAssessmentDetailSelectedSectionId = createSelector(assessmentDetailState, (state: IAssessmentDetailState): string => state.selectedSectionId);
export const getAssessmentDetailModel = createSelector(assessmentDetailState, (state: IAssessmentDetailState): IAssessmentModel => state.assessmentModel);
export const isSaveInProgress = createSelector(assessmentDetailState, (state: IAssessmentDetailState): boolean => state.isSaveInProgress);
export const isAssessmentReady = createSelector(assessmentDetailState, (state: IAssessmentDetailState): boolean => {
    return !state.isSaveInProgress
        && !state.isSubmittingAssessment
        && state.questionIdsToSave.length === 0
        && state.questionIdsBeingSaved.length === 0
        && state.subQuestionIdsToSave.length === 0
        && state.subQuestionIdsBeingSaved.length === 0;
});

export const getAssessmentDetailSectionModel = createSelector(getAssessmentDetailModel, (state: IAssessmentModel): ISectionModel => state ? state.section : null);
export const getAssessmentDetailName = createSelector(getAssessmentDetailModel, (state: IAssessmentModel): string => state ? state.name : null);
export const getAssessmentDetailStatus = createSelector(getAssessmentDetailModel, (state: IAssessmentModel): AssessmentStatus => state ? state.status : null);
export const getAssessmentDetailOrg = createSelector(getAssessmentDetailModel, (state: IAssessmentModel): { id: string, name: string } => state ? state.orgGroup : null);
export const getAssessmentDetailCreatorName = createSelector(getAssessmentDetailModel, (state: IAssessmentModel): string => get(state, "createdBy.name", null));
export const getAssessmentDetailDeadline = createSelector(getAssessmentDetailModel, (state: IAssessmentModel): string => state ? state.deadline : null);
export const getAssessmentDetailReminder = createSelector(getAssessmentDetailModel, (state: IAssessmentModel): number => state ? state.reminder : null);
export const getAssessmentDetailTags = createSelector(getAssessmentDetailModel, (state: IAssessmentModel): IQuestionModelOption[] => state && state.tags ? state.tags : null);
export const getAssessmentDetailApproverNames = createSelector(getAssessmentDetailModel, (state: IAssessmentModel): string[] => (state && !isNull(state.approvers[0])) ? state.approvers.map((approver) => approver.name) : []);
export const getAssessmentDetailRespondentNames = createSelector(getAssessmentDetailModel, (state: IAssessmentModel): string[] => (state && !isNull(state.respondents[0])) ? state.respondents.map((respondent) => respondent.name) : []);
export const getAssessmentDetailApprovers = createSelector(getAssessmentDetailModel, (state: IAssessmentModel): Array<{id: string, name: string}> => (state && !isNull(state.approvers[0])) ? state.approvers.map((approver) => approver) : []);
export const getAssessmentDetailDescription = createSelector(getAssessmentDetailModel, (state: IAssessmentModel): string => state ? state.description : null);
export const getAssessmentDetailTemplate = createSelector(getAssessmentDetailModel, (state: IAssessmentModel): IAssessmentTemplate => state ? state.template : null);
export const getAssessmentDetailInventoryOptions = createSelector(assessmentDetailState, (state: IAssessmentDetailState): IStringMap<IFlattenedState<IQuestionModelOption>> => state && state.inventoryOptions ? state.inventoryOptions : null);
export const getAssessmentDetailInventoryAttributeUserOptions = createSelector(assessmentDetailState, (state: IAssessmentDetailState): IOrgUserAdapted[] => state && state.inventoryUserOptions ? state.inventoryUserOptions : null);
export const getBulkAssessmentDetailsList = createSelector(assessmentDetailState, (inventoryState: IAssessmentDetailState): IAssessmentCreationDetails[] => inventoryState.bulkAssessmentDetailsList);
export const isReadinessTemplate = (readinessTemplateType?: TemplateType) =>
    createSelector(getAssessmentDetailModel, (state: IAssessmentModel): boolean => {
        if (readinessTemplateType) {
            return state && state.template.templateType === readinessTemplateType;
        }
        return state && (state.template.templateType === TemplateTypes.GRA || state.template.templateType === TemplateTypes.RA);
    });
export const isDINATemplate = (dinaTemplateType?: TemplateType) =>
    createSelector(getAssessmentDetailModel, (state: IAssessmentModel): boolean => {
        if (dinaTemplateType) {
            return state && state.template.templateType === dinaTemplateType;
        }
        return state && (state.template.templateType === TemplateTypes.DINA);
    });

export const getAssessmentDetailSelectedSectionRespondent = (selectedSectionId: string) => createSelector(getAssessmentDetailModel, (state: IAssessmentModel): { id: string, name: string } => {
    return state ? state.respondent : null;
});
export const getAssessmentDetailSelectedSectionRespondents = (selectedSectionId: string) => createSelector(getAssessmentDetailModel, (state: IAssessmentModel): Array<INameId<string>> => {
    return state ? state.respondents : null;
});
export const getAssessmentDetailSelectedSectionApprovers = (selectedSectionId: string) => createSelector(getAssessmentDetailModel, (state: IAssessmentModel): IAssessmentApprover[] => {
    return state ? state.approvers : null;
});
export const getAssessmentRisksByQuestionId = (questionId: string) => createSelector(getAssessmentDetailSectionModel, (section: ISectionModel): IRiskMetadata[] => section.risks.byQuestionId[questionId]);
export const getAssessmentDetailSectionNumber = (sectionId: string) => createSelector(getAssessmentDetailModel, (state: IAssessmentModel): number => state.sectionHeaders.allIds.indexOf(sectionId));

export const getQuestionIds = createSelector(getAssessmentDetailModel, (state: IAssessmentModel): string[] => state.questions.allIds);
export const getQuestionModels = createSelector(getAssessmentDetailModel, (state: IAssessmentModel): Partial<Map<string, IQuestionModel>> => state.questions.byId);
export const getQuestionsInOrder = createSelector(getQuestionIds, getQuestionModels, (allIds: string[], byId: Partial<Map<string, IQuestionModel>>): IQuestionModel[] => allIds.map((id: string): IQuestionModel => byId[id]));
export const getQuestionResponseIds = (questionId: string) => createSelector(getQuestionModels, (questions: Partial<Map<string, IQuestionModel>>): string[] => questions[questionId] && questions[questionId].responses ? questions[questionId].responses.allIds : []);
export const getQuestionResponseModels = (questionId: string) => createSelector(getQuestionModels, (questions: Partial<Map<string, IQuestionModel>>): Partial<Map<string, IQuestionResponseV2>> => questions[questionId] && questions[questionId].responses ? questions[questionId].responses.byId : {});
export const getQuestionResponsesArray = (questionId: string) => createSelector(getQuestionResponseIds(questionId), getQuestionResponseModels(questionId), (allIds: string[], byId: Partial<Map<string, IQuestionModelResponse>>): IQuestionModelResponse[] => allIds.length ? map(allIds, ((id: string): IQuestionModelResponse => byId[id])) : []);
export const getInvalidQuestionResponsesArray = (questionId: string) => createSelector(getQuestionResponseIds(questionId), getQuestionResponseModels(questionId), (allIds: string[], byId: Partial<Map<string, IQuestionModelResponse>>): IQuestionModelResponse[] => {
    if (!allIds.length) return [];
    return map(allIds, ((id: string): IQuestionModelResponse => {
        if (byId[id] && !byId[id].valid) {
            return byId[id];
        }
    }));
});
export const getQuestionPersonalDataResponseMap = (questionId: string) => createSelector(getQuestionResponsesArray(questionId), (responseArray: IQuestionModelResponse[]): IStringMap<IStringMap<IAssessementDataElementResponse[]>> | null => (responseArray.length && responseArray[0].responseMap) ? responseArray[0].responseMap : null);
export const getQuestionPersonalDataUpdateResponseMap = (questionId: string) => createSelector(getQuestionResponsesArray(questionId), getQuestionPersonalDataResponseMap(questionId), (questionResponseArray: IQuestionModelResponse[], responseMapSubjectsList: IStringMap<IStringMap<IAssessementDataElementResponse[]>>): IPersonalDataUpdateResponseMap[] => formatPersonalDataUpdateResponseMap(questionResponseArray, responseMapSubjectsList));

export const getSubQuestionIds = (questionId: string) => createSelector(getQuestionModels, (questions: Partial<Map<string, IQuestionModel>>): string[] => questions[questionId] && questions[questionId].subQuestions ? questions[questionId].subQuestions.allIds : []);
export const getSubQuestionModels = (questionId: string) => createSelector(getQuestionModels, (questions: Partial<Map<string, IQuestionModel>>): Partial<Map<string, IQuestionModelSubQuestion>> => questions[questionId] && questions[questionId].subQuestions ? questions[questionId].subQuestions.byId : {});
export const getSubQuestionsInOrder = (questionId: string) => createSelector(getSubQuestionIds(questionId), getSubQuestionModels(questionId), (allIds: string[], byId: Partial<Map<string, IQuestionModelSubQuestion>>): IQuestionModelSubQuestion[] => allIds.map((id: string): IQuestionModelSubQuestion => byId[id]));
export const getSubQuestionResponseIds = (questionId: string, subQuestionId: string) => createSelector(getSubQuestionModels(questionId), (subQuestions: Partial<Map<string, IQuestionModelSubQuestion>>): string[] => subQuestions[subQuestionId] && subQuestions[subQuestionId].responses ? subQuestions[subQuestionId].responses.allIds : []);
export const getSubQuestionResponseModels = (questionId: string, subQuestionId: string) => createSelector(getSubQuestionModels(questionId), (subQuestions: Partial<Map<string, IQuestionModelSubQuestion>>): Partial<Map<string, IQuestionResponseV2>> => subQuestions[subQuestionId] && subQuestions[subQuestionId].responses ? subQuestions[subQuestionId].responses.byId : {});
export const getSubQuestionResponsesArray = (questionId: string, subQuestionId: string) => createSelector(getSubQuestionResponseIds(questionId, subQuestionId), getSubQuestionResponseModels(questionId, subQuestionId), (allIds: string[], byId: Partial<Map<string, IQuestionResponseV2>>): IQuestionResponseV2[] => allIds.length ? map(allIds, ((id: string): IQuestionResponseV2 => byId[id])) : []);
export const getInvalidSubQuestionResponsesArray = (questionId: string, subQuestionId: string) => createSelector(getSubQuestionResponseIds(questionId, subQuestionId), getSubQuestionResponseModels(questionId, subQuestionId), (allIds: string[], byId: Partial<Map<string, IQuestionResponseV2>>): IQuestionResponseV2[] => {
    const arr = [];
    for (const id of allIds) {
        if (byId[id] && byId[id].valid === false) {
            arr.push(byId[id]);
        }
    }
    return arr;
});

export const getPersonalDataQuestionResponses = (questionId: string) => createSelector(getQuestionResponseIds(questionId), getQuestionResponseModels(questionId), (allIds: string[], byId: Partial<Map<string, IQuestionModelResponse>>): IPersonalDataQuestionResponse[] => allIds.length ? formatPersonalDataResponses(byId[allIds[0]]) : []);

export const getInventoryOptionIds = (inventoryType: string) => createSelector(getAssessmentDetailInventoryOptions, (options: IStringMap<IFlattenedState<IQuestionModelOption>>): string[] => options && options[inventoryType] ? options[inventoryType].allIds : []);
export const getInventoryOptionModels = (inventoryType: string) => createSelector(getAssessmentDetailInventoryOptions, (options: IStringMap<IFlattenedState<IQuestionModelOption>>): Partial<Map<string, IQuestionModelOption>> => options && options[inventoryType] ? options[inventoryType].byId : {});
export const getInventoryOptionsArray = (inventoryType: string) => createSelector(getInventoryOptionIds(inventoryType), getInventoryOptionModels(inventoryType), (allIds: string[], byId: IStringMap<IQuestionModelOption>): IQuestionModelOption[] => allIds.length ? map(allIds, ((id: string): IQuestionModelOption => byId[id])) : []);
export const hasInventoryOptionModels = (inventoryType: string) => createSelector(getAssessmentDetailInventoryOptions, (options: IStringMap<IFlattenedState<IQuestionModelOption>>): boolean => options && options[inventoryType] ? options[inventoryType].allIds.length > 0 : false);
export const getInvalidQuestionRiskIds = createSelector(assessmentDetailState, (state: IAssessmentDetailState): IStringMap<string[]> => state.invalidQuestionRisksIdMap );

export const getAssessmentQuestionTitle = (questionId: string) => createSelector(assessmentDetailState, (state: IAssessmentDetailState): string => {
    const question: IAssessmentTemplateQuestion = state.templateMetadata.questionById[questionId];
    return question.friendlyName || question.content;
});
export const getAssessmentQuestionSequence = (questionId: string, withSectionSequence: boolean = true, separator: string = ".") => createSelector(assessmentDetailState, (state: IAssessmentDetailState): string => {
    const metadata = state.templateMetadata;
    let questionSequence: string = metadata.questionById[questionId].sequence as any as string;
    if (withSectionSequence) {
        // find the section question belongs to
        const sectionMap = metadata.sectionIdQuestionIdsMap;
        for (const sectionId in sectionMap) {
            if (sectionMap.hasOwnProperty(sectionId)) {
                const questionIds = sectionMap[sectionId];
                if (questionIds.findIndex((id: string) => id === questionId) > -1) {
                    // section found, append the index
                    const sectionSequence = metadata.sectionById[sectionId].sequence;
                    questionSequence = `${sectionSequence}${separator}${questionSequence}`;
                }
            }
        }
    }

    return questionSequence;
});

export const getAssessmentSectionByQuestionId = (questionId: string) => createSelector(assessmentDetailState, (state: IAssessmentDetailState): IAssessmentTemplateSection => {
    const sectionMap = state.templateMetadata.sectionIdQuestionIdsMap;
    for (const sectionId in sectionMap) {
        if (sectionMap.hasOwnProperty(sectionId)) {
            const questionIds = sectionMap[sectionId];
            if (questionIds.findIndex((id: string) => id === questionId) > -1) {
                return state.templateMetadata.sectionById[sectionId];
            }
        }
    }
});

export const getQuestionCommentCountByQuestionId = (questionId: string) => createSelector(assessmentDetailState, (state: IAssessmentDetailState): number => {
    return (state.assessmentModel.questions
        && state.assessmentModel.questions.byId
        && state.assessmentModel.questions.byId[questionId]) ?
        state.assessmentModel.questions.byId[questionId].totalComments : 0;
});

export const assessmentHasInvalidQuestions = createSelector(assessmentDetailState, (state: IAssessmentDetailState): boolean => {
    return some(state.assessmentModel.sectionHeaders.byIds, (section: ISectionHeaderInfo) => section.invalidQuestionIds && section.invalidQuestionIds.length > 0);
});

export const geInventoryQuestionByAttributeQuestionId = (questionId: string) => createSelector(assessmentDetailState, (state: IAssessmentDetailState): IAssessmentTemplateQuestion => {
    const question = state.templateMetadata.questionById[questionId];
    return state.templateMetadata.questionById[question.parentQuestionId];
});

export const getDisbaledSectionIds = createSelector(assessmentDetailState, (state: IAssessmentDetailState): string[] => getDisabledSectionsFromAssessment(state.assessmentModel.sectionHeaders));

// REDUCER HELPERS
function formatPersonalDataUpdateResponseMap(questionResponseArray: IQuestionModelResponse[], responseMapSubjectsList: IStringMap<IStringMap<IAssessementDataElementResponse[]>>): IPersonalDataUpdateResponseMap[] {
    const responseMap: IPersonalDataUpdateResponseMap[] = [];
    forEach(responseMapSubjectsList, (categoriesList: IStringMap<IAssessementDataElementResponse[]>, subjectIndex: string): void => {
        const subjectId = subjectIndex;
        if (!categoriesList) {
            responseMap.push({
                DATA_SUBJECTS: { id: subjectId, name: questionResponseArray[0].dataSubjectById[subjectId] },
                DATA_CATEGORIES: null,
                DATA_ELEMENTS: null,
            });
        } else {
            forEach(categoriesList, (elementsList: IAssessementDataElementResponse[], categoryIndex: string): void => {
                const categoryId = categoryIndex;
                forEach(elementsList, (element: IAssessementDataElementResponse): void => {
                    responseMap.push({
                        DATA_SUBJECTS: { id: subjectId, name: questionResponseArray[0].dataSubjectById[subjectId] },
                        DATA_CATEGORIES: { id: categoryId, name: questionResponseArray[0].categoryById[categoryId]},
                        DATA_ELEMENTS: { id: element.id, name: element.name},
                    });
                });
            });
        }
    });
    return responseMap;
}

function checkIfSaveIsPending(
    localAssessmentModel: IAssessmentModel,
    incomingAssessmentModel: IAssessmentModel,
    anyQuestionIdsToSave: boolean,
): IAssessmentModel {
    if (anyQuestionIdsToSave) {
        return {
            ...incomingAssessmentModel,
            questions: localAssessmentModel.questions,
        };
    }

    return incomingAssessmentModel;
}

function getStateVariables(state: IAssessmentDetailState) {
    return {
        aaModel: state.assessmentModel,
        questions: state.assessmentModel.questions,
        questionsAllIds: state.assessmentModel.questions.allIds,
        questionsById: state.assessmentModel.questions.byId,
    };
}

function formatPersonalDataResponses(response: IQuestionModelResponse): IPersonalDataQuestionResponse[] {
    const responses = [];
    forIn(response.responseMap, (dataCategory: IStringMap<IAssessementDataElementResponse[]>, subjectId: string) => {
        responses.push({
            name: response.dataSubjectById[subjectId],
            id: subjectId,
            categories: formatPersonalDataCategories(response, subjectId),
        });
    });
    return responses;
}

function formatPersonalDataCategories(response: IQuestionModelResponse, subjectId: string): IPersonalDataCategory[] {
    const categories = [];
    forIn(response.responseMap[subjectId], (elementResponses: IAssessementDataElementResponse[], categoryId: string) => {
        categories.push({
            categoryValue: response.categoryById[categoryId],
            categoryId,
            elementResponses,
        });
    });
    return categories;
}

function getValidPersonalDataResponses(question: IQuestionModel): IStringMap<IStringMap<IAssessementDataElementResponse[]>> {
    const responses: IQuestionResponsesState<IQuestionModelResponse> = question.responses;
    const validResponseMap: IStringMap<IStringMap<IAssessementDataElementResponse[]>> = { ...responses.byId[Object.keys(responses.byId)[0]].responseMap };
    forIn(responses.invalidResponsesMap, (dataSubject, dataSubjectId) => {
        forIn(dataSubject.categories, (category, categoryId) => {
            forEach(category.dataElements, (dataElement) => {
                remove(validResponseMap[dataSubjectId][categoryId], (element) => element.id === dataElement.id);
            });
        });
    });

    return validResponseMap;
}

function getDisabledSectionsFromAssessment(sectionHeaders: ISectionHeaderReducerState ): string[] {
    const disabledSections: string[] = [];

    Object.keys(sectionHeaders.byIds).forEach((sectionId) => {
        if (sectionHeaders.byIds[sectionId].hidden) {
            disabledSections.push(sectionId);
        }
    });

    return [...disabledSections];
}
