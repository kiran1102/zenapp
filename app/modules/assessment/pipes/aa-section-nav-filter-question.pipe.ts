// Angular
import {
    Pipe,
    PipeTransform,
} from "@angular/core";

// Interfaces
import { ILabelValue } from "@onetrust/vitreus/src/app/interface/label-value.interface";
import { IQuestionModel } from "interfaces/assessment/question-model.interface";
import { ISectionHeaderInfo } from "interfaces/assessment/section-header-info.interface";

// Enums
import { AASectionFilterTypes } from "modules/assessment/enums/aa-filter.enum";

// Constants
import { QuestionTypeNames } from "constants/question-types.constant";

@Pipe({
    name: "sectionNavFilterShowQuestion",
})

export class SectionNavFilterShowQuestionPipe implements PipeTransform {
    transform(question: IQuestionModel, selectedFilter: ILabelValue<string>, sectionHeader: ISectionHeaderInfo): boolean {
        if (question.hidden) {
            return false;
        }
        if (selectedFilter) {
            switch (selectedFilter.value) {
                case AASectionFilterTypes.RequiredQuestions:
                    return this.questionExists(sectionHeader.requiredQuestionIds, question, selectedFilter.value);
                case AASectionFilterTypes.UnansweredQuestions:
                    return this.questionExists(sectionHeader.unansweredQuestionIds, question, selectedFilter.value);
                case AASectionFilterTypes.RequiredAndUnansweredQuestions:
                    return this.questionExists(sectionHeader.requiredUnansweredQuestionIds, question, selectedFilter.value);
                default:
                    return true;
            }
        }
        return true;
    }

    private questionExists(questionIds: string[], question: IQuestionModel, filter: string): boolean {
        if (question.questionType === QuestionTypeNames.Date) {
            return this.handleDateQuestion(question, filter);
        }
        return questionIds ? questionIds.indexOf(question.id) >= 0 : true;
    }

    private handleDateQuestion(question: IQuestionModel, filter: string): boolean {
        const noDateResponse = question.responses === null || (question.responses && question.responses.allIds
            && question.responses.allIds.length && question.responses.byId[question.responses.allIds[0]].response === null);
        if (filter === AASectionFilterTypes.RequiredQuestions) {
            return question.required;
        } else if (filter === AASectionFilterTypes.UnansweredQuestions) {
            return noDateResponse;
        } else if (filter === AASectionFilterTypes.RequiredAndUnansweredQuestions) {
            return noDateResponse && question.required;
        }
    }
}
