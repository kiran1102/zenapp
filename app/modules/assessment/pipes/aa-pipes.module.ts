import { NgModule } from "@angular/core";

// Pipes
import { DisplayInventoryLabelPipe  } from "modules/assessment/pipes/aa-display-inv-label.pipe";
import { DisplayQuestionNotificationPipe  } from "modules/assessment/pipes/aa-display-question-notification.pipe";
import { DisplayBadgePipe  } from "modules/assessment/pipes/aa-display-badge.pipe";
import { SectionNavFilterShowQuestionPipe } from "modules/assessment/pipes/aa-section-nav-filter-question.pipe";
import { SectionNavFilterShowSectionPipe } from "modules/assessment/pipes/aa-section-nav-filter-section.pipe";

export const PIPES = [
    DisplayInventoryLabelPipe,
    DisplayQuestionNotificationPipe,
    DisplayBadgePipe,
    SectionNavFilterShowQuestionPipe,
    SectionNavFilterShowSectionPipe,
];

@NgModule({
    imports: [],
    providers: [],
    exports: [PIPES],
    declarations: [PIPES],
})

export class AAPipesModule { }
