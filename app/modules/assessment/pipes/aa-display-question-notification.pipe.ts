import {
    Pipe,
    PipeTransform,
} from "@angular/core";
import { AAQuestionNotification } from "enums/assessment/assessment-question.enum";
import { includes } from "lodash";

@Pipe({
    name: "displayQuestionNotification",
})

export class DisplayQuestionNotificationPipe implements PipeTransform {
    transform(questionNotificationLabel: string[]): string {
        if (includes(questionNotificationLabel, "MULTI_SELECT_NOT_ALLOWED")) {
            return AAQuestionNotification.MULTI_SELECT_NOT_ALLOWED;
        } else if (includes(questionNotificationLabel, "OPTION_REMOVED")) {
            return AAQuestionNotification.OPTION_REMOVED;
        } else if (includes(questionNotificationLabel, "NEW_QUESTION")) {
            return AAQuestionNotification.NEW_QUESTION;
        } else if (includes(questionNotificationLabel, "OTHER_RESPONSE_NOT_ALLOWED")) {
            return AAQuestionNotification.OTHER_RESPONSE_NOT_ALLOWED;
        } else if (includes(questionNotificationLabel, "NOT_SURE_NOT_ALLOWED")) {
            return AAQuestionNotification.NOT_SURE_NOT_ALLOWED;
        } else if (includes(questionNotificationLabel, "NOT_APPLICABLE_NOT_ALLOWED")) {
            return AAQuestionNotification.NOT_APPLICABLE_NOT_ALLOWED;
        } else {
            return "QuestionsResponseChanged";
        }
    }
}
