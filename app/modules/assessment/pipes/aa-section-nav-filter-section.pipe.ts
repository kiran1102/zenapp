// Angular
import {
    Pipe,
    PipeTransform,
} from "@angular/core";

// Interfaces
import { ILabelValue } from "@onetrust/vitreus/src/app/interface/label-value.interface";
import { ISectionHeaderInfo } from "interfaces/assessment/section-header-info.interface";

// Enums
import { AASectionFilterTypes } from "modules/assessment/enums/aa-filter.enum";

// Constants
import { EmptyGuid } from "constants/empty-guid.constant";

@Pipe({
    name: "sectionNavFilterShowSection",
})

export class SectionNavFilterShowSectionPipe implements PipeTransform {
    transform(selectedFilter: ILabelValue<string>, sectionHeader: ISectionHeaderInfo): boolean {
        if (sectionHeader.sectionId === EmptyGuid) {
            return true;
        }
        if (selectedFilter) {
            switch (selectedFilter.value) {
                case AASectionFilterTypes.RequiredQuestions:
                    return !!(sectionHeader.requiredQuestionIds && sectionHeader.requiredQuestionIds.length);
                case AASectionFilterTypes.UnansweredQuestions:
                    return !!(sectionHeader.unansweredQuestionIds && sectionHeader.unansweredQuestionIds.length);
                case AASectionFilterTypes.RequiredAndUnansweredQuestions:
                    return !!(sectionHeader.requiredUnansweredQuestionIds && sectionHeader.requiredUnansweredQuestionIds.length);
                default:
                    return true;
            }
        } else {
            return true;
        }
    }
}
