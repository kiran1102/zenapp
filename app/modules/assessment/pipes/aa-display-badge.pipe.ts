import {
    Pipe,
    PipeTransform,
} from "@angular/core";

@Pipe({
    name: "displayBadge",
})

export class DisplayBadgePipe implements PipeTransform {
    transform(assessmentStatus: string): string {
        switch (assessmentStatus) {
            case "NotStarted":
                return "";
            case  "InProgress":
                return "primary";
            case "UnderReview":
                return "warning";
            case "Completed":
                return "success";
            default:
                return "";
        }
    }
}
