import {
    Pipe,
    PipeTransform,
} from "@angular/core";

@Pipe({
    name: "displayInventoryLabel",
})

export class DisplayInventoryLabelPipe implements PipeTransform {
    transform(subQuestionDisplayLabel: string, subQuestionsLength: number): string | null {
        return subQuestionsLength > 1 ? subQuestionDisplayLabel : null;
    }
}
