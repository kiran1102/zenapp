// Services
import { Permissions } from "modules/shared/services/helper/permissions.service";
import { GlobalSidebarService } from "modules/global-sidebar/services/one-global-sidebar.service";
import { AssessmentDetailApiService } from "modules/assessment/services/api/assessment-detail-api.service";
import { AssessmentListApiService } from "modules/assessment/services/api/assessment-list-api.service";

// Interfaces
import { IStringMap } from "interfaces/generic.interface";

export function assessmentLegacyRoutes($stateProvider: ng.ui.IStateProvider) {
    $stateProvider
        .state("zen.app.pia.module.assessment", {
            abstract: true,
            url: "assessment",
            resolve: {
                AssessmentsPermission: [
                    "GeneralData", "Permissions",
                    (GeneralData: IStringMap<any>, permissions: Permissions): boolean => {
                        if (permissions.canShow("Assessments")
                            || permissions.canShow("VendorRiskManagement")
                            || permissions.canShow("AssessmentDetailsView")) {
                            return true;
                        }
                        permissions.goToFallback();
                        return false;
                    },
                ],
            },
            views: {
                module: {
                    template: "<ui-view></ui-view>",
                    controller: [
                        "GlobalSidebarService",
                        (globalSidebarService: GlobalSidebarService) => {
                            globalSidebarService.setAAMenu();
                        },
                    ],
                },
            },
        })
        .state("zen.app.pia.module.assessment.list", {
            url: "",
            template: "<downgrade-aa-list></downgrade-aa-list>",
            params: {
                statusFilter: null,
                orgGroupId: null,
                templateId: null,
            },
        })
        .state("zen.app.pia.module.assessment.wizard", {
            abstract: true,
            url: "/wizard",
            resolve: {
                AssessmentCreatePermission: [
                    "Permissions",
                    "AssessmentListApi",
                    "$state",
                    (permissions: Permissions, AssessmentListApi: AssessmentListApiService, $state): ng.IPromise<boolean> => {
                    return AssessmentListApi.getSspRestrictViewSetting()
                        .then((response): boolean => {
                            if (response) {
                                const restrictViewSetting = response.data && response.data.restrictProjectOwnerView;
                                if (permissions.canShow("AssessmentCreate")) {
                                    const allowCreateAssessment = permissions.canShow("BypassSelfServicePortal") || !restrictViewSetting;
                                    if (allowCreateAssessment) {
                                        return true;
                                    }
                                    $state.go("zen.app.pia.module.ssp.list");
                                    return false;
                                }
                                permissions.goToFallback();
                                return false;
                            }
                        });
                    },
                ],
            },
            template: "<ui-view></ui-view>",
        })
        .state("zen.app.pia.module.assessment.wizard.assessment_type", {
            url: "/select",
            template: "<downgrade-aa-template-selection></downgrade-aa-template-selection>",
            params: { launchAssessmentDetails: null },
        })
        .state("zen.app.pia.module.assessment.wizard.assessment_creation", {
            url: "/info/:templateId/:version",
            template: "<downgrade-aa-create-form></downgrade-aa-create-form>",
            params: { launchAssessmentDetails: null },
        })
        .state("zen.app.pia.module.assessment.bulk-launch-inventory-assessment", {
            url: "/launchassessment/:inventoryTypeId",
            template: "<downgrade-aa-bulk-launch></downgrade-aa-bulk-launch>",
            params: {
                recordId: null,
                tabId: null,
            },
            resolve: {
                AssessmentCreatePermission: [
                    "Permissions",
                    "AssessmentListApi",
                    "$state",
                    (permissions: Permissions, AssessmentListApi: AssessmentListApiService, $state): ng.IPromise<boolean> => {
                    return AssessmentListApi.getSspRestrictViewSetting()
                        .then((response): boolean => {
                            if (response) {
                                const restrictViewSetting = response.data && response.data.restrictProjectOwnerView;
                                if (permissions.canShow("AssessmentCreate")) {
                                    const allowCreateAssessment = permissions.canShow("BypassSelfServicePortal") || !restrictViewSetting;
                                    if (allowCreateAssessment) {
                                        return true;
                                    }
                                    $state.go("zen.app.pia.module.ssp.list");
                                    return false;
                                }
                                permissions.goToFallback();
                                return false;
                            }
                        });
                    },
                ],
            },
        })
        .state({
            name: "zen.app.pia.module.assessment.detail",
            url: "/detail/:assessmentId?openPanel&filterByStatus",
            template: "<downgrade-assessment-detail></downgrade-assessment-detail>",
            params: {
                backToModule: null,
                launchAssessmentDetails: null,
            },
            resolve: {
                canLoadAssessmentDetail: [
                    "Permissions",
                    "AssessmentDetailApi",
                    "$stateParams",
                    (permissions: Permissions, AssessmentDetailApi: AssessmentDetailApiService, $stateParams: ng.ui.IStateParamsService): ng.IPromise<boolean> => {
                        return AssessmentDetailApi.validateAssessment($stateParams.assessmentId)
                            .then((response: boolean): boolean => {
                                if (response) {
                                    return true;
                                } else {
                                    permissions.goToFallback();
                                    return false;
                                }
                            });
                    },
                ],
            },
        })
        .state("zen.app.pia.module.assessment.copy-assessment", {
            url: "/copyassessment",
            template: "<downgrade-copy-assessment></downgrade-copy-assessment>",
            params: {
                assessmentId: null,
            },
            resolve: {
                canLoadAssessmentCard: [
                    "$state",
                    "$stateParams",
                    ($state, $stateParams): boolean => {
                        if (!$stateParams.assessmentId) {
                            $state.go("zen.app.pia.module.assessment.list");
                            return false;
                        }
                        return true;
                    },
                ],
            },
        })
        ;
}

assessmentLegacyRoutes.$inject = ["$stateProvider"];
