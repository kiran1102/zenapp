import { Injectable } from "@angular/core";

// Services
import { AssessmentDetailHeaderApiService } from "modules/assessment/services/api/aa-assessment-detail-header-api.service";

// Rxjs
import { BehaviorSubject } from "rxjs";
import { AACollaborationCountsTypes } from "../../enums/aa-detail-header.enum";

@Injectable()
export class AaCollaborationPaneCountsService {
    private totalCommentSubject$ = new BehaviorSubject(0);
    private totalNotesSubject$ = new BehaviorSubject(0);
    private totalNMISubject$ = new BehaviorSubject(0);
    private totalRisksSubject$ = new BehaviorSubject(0);

    constructor(
        private assessmentDetailHeaderApi: AssessmentDetailHeaderApiService,
    ) { }

    getCollaborationPaneCounts(assessmentId: string, type: AACollaborationCountsTypes) {
        this.assessmentDetailHeaderApi.getCollabCounts(assessmentId, type).subscribe((response) => {
            if (response && response.data) {
                if (type === AACollaborationCountsTypes.All || type === AACollaborationCountsTypes.Notes) {
                    const notes = response.data.find((data) => {
                        return data.type === AACollaborationCountsTypes.Notes;
                    });
                    this.totalNotesSubject$.next(notes ? notes.count : 0);
                }
                if (type === AACollaborationCountsTypes.All || type === AACollaborationCountsTypes.Nmi) {
                    const NMIs = response.data.find((data) => {
                        return data.type === AACollaborationCountsTypes.Nmi;
                    });
                    this.totalNMISubject$.next(NMIs ? NMIs.count : 0);
                }
                if (type === AACollaborationCountsTypes.All || type === AACollaborationCountsTypes.Risks) {
                    const risks = response.data.find((data) => {
                        return data.type === AACollaborationCountsTypes.Risks;
                    });
                    this.totalRisksSubject$.next(risks ? risks.count : 0);
                }
                if (type === AACollaborationCountsTypes.All || type === AACollaborationCountsTypes.Comments) {
                    const comments = response.data.find((data) => {
                        return data.type === AACollaborationCountsTypes.Comments;
                    });
                    this.totalCommentSubject$.next(comments ? comments.count : 0);
                }
            }
        });
    }
}
