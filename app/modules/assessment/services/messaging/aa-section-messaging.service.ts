// Angular
import { Injectable } from "@angular/core";

// Interfaces
import { ILabelValue } from "interfaces/generic.interface";

// Rxjs
import { BehaviorSubject } from "rxjs";

@Injectable()
export class AaSectionNavMessagingService {

    defaultSectionFilterData: ILabelValue<string>;
    sectionFilterBehaviorSubject = new BehaviorSubject(this.defaultSectionFilterData);
    sectionFilterDataListener = this.sectionFilterBehaviorSubject.asObservable();
    sectionFilter: ILabelValue<string>;

    aaInvalidQuestionCountData = new BehaviorSubject(0);
    sectionInvalidQuestionDataListener = this.aaInvalidQuestionCountData.asObservable();

    publishSectionFilterData(sectionFilterData: ILabelValue<string>) {
        this.sectionFilter = sectionFilterData;
        this.sectionFilterBehaviorSubject.next(sectionFilterData);
    }

    publishInvalidQuestionCountData(invalidQuestionCount: number) {
        this.aaInvalidQuestionCountData.next(invalidQuestionCount);
    }
}
