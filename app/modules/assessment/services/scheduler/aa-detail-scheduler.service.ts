// Angular
import {
    Injectable,
    Inject,
} from "@angular/core";

// Rxjs
import {
    Subject,
    BehaviorSubject,
    NEVER,
    of,
    merge,
} from "rxjs";
import {
    map as rxMap,
    filter,
    switchMap,
    tap,
} from "rxjs/operators";

// 3rd Party
import { IPromise } from "angular";
import {
    map,
    keyBy,
    forIn,
    includes,
    keys,
    forEach,
} from "lodash";
import {
    TransitionService,
    Transition,
} from "@uirouter/core";

// Redux
import { AnyAction } from "redux";
import {
    IStore,
    IStoreState,
    IFlattenedState,
} from "interfaces/redux.interface";
import {
    AssessmentDetailAction,
    getAssessmentDetailModel,
    hasQuestionIdsToSave,
    getQuestionIdsToSave,
    getQuestionResponseModels,
    getQuestionResponseIds,
    getQuestionModels,
    isSaveInProgress,
    getAssessmentDetailInventoryOptions,
    getAssessmentDetailOrg,
    getSubQuestionIdsToSave,
    getSubQuestionModels,
    getSubQuestionResponsesArray,
    getInventoryOptionModels,

} from "modules/assessment/reducers/assessment-detail.reducer";
import { StoreToken } from "tokens/redux-store.token";

// Service
import { AssessmentDetailApiService } from "modules/assessment/services/api/assessment-detail-api.service";
import { AssessmentDetailAdapterService } from "modules/assessment/services/adapter/assessment-detail-adapter.service";
import { AaSectionNavMessagingService } from "../messaging/aa-section-messaging.service";
import { OrgGroupApiService } from "oneServices/api/org-group-api.service";
import Utilities from "Utilities";

// Interface
import {
    IAssessmentModel,
    IAssessmentInventoryLink,
} from "interfaces/assessment/assessment-model.interface";
import { ISaveSectionResponsesContract } from "interfaces/assessment/assessment-api.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";
import { IStringMap } from "interfaces/generic.interface";
import {
    IQuestionResponseV2,
    IQuestionPersonalDataResponse,
    IPersonalDataQuestionResponseV2,
} from "interfaces/assessment/question-response-v2.interface";
import {
    IQuestionModel,
    IQuestionModelOption,
    IQuestionModelSubQuestion,
} from "interfaces/assessment/question-model.interface";
import { IOrgUserAdapted } from "interfaces/org-user.interface";

// Enums
import { QuestionResponseType } from "enums/assessment/assessment-question.enum";
import { OrgUserTraversal } from "enums/org-user-dropdown.enum";

// Constants
import { Regex } from "constants/regex.constant";

@Injectable()
export class AaDetailSchedulerService {

    schedulerPause$ = new BehaviorSubject(true);
    assessmentDetailMiddleware$ = new Subject();
    sectionChange$ = new Subject();

    constructor(
        @Inject("$q") private $q: ng.IQService,
        @Inject(StoreToken) private store: IStore,
        private $transitions: TransitionService,
        private AssessmentDetailApi: AssessmentDetailApiService,
        private aaSectionNavMessagingService: AaSectionNavMessagingService,
        private AssessmentDetailAdapter: AssessmentDetailAdapterService,
        private orgGroups: OrgGroupApiService,
    ) {

        const filterForSectionChange = (action: AnyAction): boolean => action.type === "FETCH_NEW_SECTION";
        const filterForRouteChange = (action: AnyAction): boolean => action.type === "NAVIGATE_FROM_ASSESSMENT_DETAIL";
        const filterForSubmitAssessment = (action: AnyAction): boolean => action.type === "SUBMIT_ASSESSMENT_DETAIL_SECTION";
        const filterForQuestionSave = (action: AnyAction): boolean => action.type === "SAVE_QUESTION";

        const determineWhichSectionToFetchAndReturnNewAssessmentModel = (action: AnyAction) => {
            this.store.dispatch({ type: "LOADING_ASSESSMENT_SECTION" });
            const { payload: { assessmentId, sectionId, direction } } = action;

            switch (direction) {
                case "WELCOME":
                    return this.AssessmentDetailApi.fetchAssessmentWelcomeSection(assessmentId)
                        .then((assessmentModel: IAssessmentModel) => ({ assessmentModel, sectionId, action }));
                case "FIRST":
                    return this.AssessmentDetailApi.fetchFirstSection(assessmentId)
                        .then((assessmentModel: IAssessmentModel) => ({ assessmentModel, sectionId: assessmentModel.section.sectionId, action }));
                case "PREVIOUS":
                    return this.AssessmentDetailApi.fetchPreviousSection(assessmentId, sectionId)
                        .then((assessmentModel: IAssessmentModel) => ({ assessmentModel, sectionId: assessmentModel.section.sectionId, action }));
                case "NEXT":
                    return this.AssessmentDetailApi.fetchNextSection(assessmentId, sectionId)
                        .then((assessmentModel: IAssessmentModel) => ({ assessmentModel, sectionId: assessmentModel.section.sectionId, action }));
                default:
                    return this.AssessmentDetailApi.fetchCurrentSection(assessmentId, sectionId)
                        .then((assessmentModel: IAssessmentModel) => ({ assessmentModel, sectionId, action }));
            }
        };

        const fetchInventoryOptionsPromises = (inventoryLinks: IAssessmentInventoryLink[]): Array<IPromise<IStringMap<IFlattenedState<IQuestionModelOption>>>> => {
            return inventoryLinks.map((inventoryLink: IAssessmentInventoryLink) => {
                const assessmentOrgId = getAssessmentDetailOrg(this.store.getState()).id;
                return this.AssessmentDetailApi.fetchInventoryQuestionOptions(inventoryLink.link, inventoryLink.type, assessmentOrgId)
                    .then((response: IQuestionModelOption[]): IStringMap<IFlattenedState<IQuestionModelOption>> => {
                        return {
                            [inventoryLink.type]: {
                                allIds: map(response, ((opt: IQuestionModelOption): string => opt.id)),
                                byId: keyBy(response, ((opt: IQuestionModelOption): string => opt.id)),
                            },
                        };
                    });
            });
        };

        const fetchInventoryAttibuteUserList = (): IPromise<IOrgUserAdapted[]> => {
            return this.orgGroups.getOrgUsersWithPermission(getAssessmentDetailOrg(this.store.getState()).id,
                OrgUserTraversal.Branch, false, [], [])
                .then((response) => {
                    return response.data;
            });
        };

        const fetchInventoryOptions = (inventoryLinks: IAssessmentInventoryLink[]): Promise<IStringMap<IFlattenedState<IQuestionModelOption>> | boolean> => {
            this.store.dispatch({ type: AssessmentDetailAction.UPDATE_IS_LOADING_MESSAGE, message: "LoadingInventoryRecords" });
            const inventoryOptionResults = fetchInventoryOptionsPromises(inventoryLinks);

            return Promise.all(inventoryOptionResults).then((responses: Array<IStringMap<IFlattenedState<IQuestionModelOption>>>) => {
                const inventoryOptions = getAssessmentDetailInventoryOptions(store.getState()) || {};

                responses.forEach((response: IStringMap<IFlattenedState<IQuestionModelOption>>): void => {
                    forIn(response, (value: IFlattenedState<IQuestionModelOption>, key: string): void => {
                        inventoryOptions[key] = value;
                    });
                });

                return inventoryOptions;
            });
        };

        const getInventoryLinksToFetch = (inventoryLinks: IAssessmentInventoryLink[]): IAssessmentInventoryLink[] => {
            const cachedInventoryOptions = getAssessmentDetailInventoryOptions(store.getState());
            if (!cachedInventoryOptions) return inventoryLinks;

            // Fetch inventories that haven't been cached in Store
            const cachedInventoryKeys = Object.keys(cachedInventoryOptions);
            return inventoryLinks.filter((inventoryLink: IAssessmentInventoryLink): boolean => !includes(cachedInventoryKeys, inventoryLink.type));
        };

        const shouldFetchInventoryOptions = (assessmentModel: IAssessmentModel): boolean => {
            return assessmentModel
                && assessmentModel.inventoryLinks
                && Boolean(assessmentModel.inventoryLinks.length);
        };

        const handleSectionUpdatesToStore = (assessmentModel: IAssessmentModel): void => {
            if (!assessmentModel) return;  // TODO: better error handling?

            switch (assessmentModel.displayState) {
                case "REFRESH_NONE":
                    break;
                case "REFRESH_PARTIAL": {
                    this.store.dispatch({ type: AssessmentDetailAction.UPDATE_ASSESSMENT_PARTIAL, model: assessmentModel });
                    this.store.dispatch({ type: AssessmentDetailAction.FINISH_LOADING_ASSESSMENT_SECTION });
                    break;
                }
                case "REFRESH_ALL": {
                    // Inventory questions fetch their options from Inventory microservice
                    const inventoryLinksToFetch = shouldFetchInventoryOptions(assessmentModel) ? getInventoryLinksToFetch(assessmentModel.inventoryLinks) : [];
                    if (inventoryLinksToFetch.length) {
                        Promise.all([fetchInventoryAttibuteUserList(), fetchInventoryOptions(inventoryLinksToFetch)]).then((response) => {
                            const inventoryAttributeUserList: IOrgUserAdapted[] = response[0];
                            const options: IStringMap<IFlattenedState<IQuestionModelOption>> | boolean = response[1];
                            if (options) {
                                this.store.dispatch({ type: AssessmentDetailAction.UPDATE_QUESTION_INVENTORY_OPTIONS, options });
                            }
                            if (inventoryAttributeUserList) {
                                this.store.dispatch({ type: AssessmentDetailAction.UPDATE_QUESTION_INVENTORY_ATTRIBUTE_USER_OPTIONS, inventoryAttributeUserList });
                            }
                            this.finishLoadAssesmmentAndUpdateStore(assessmentModel);
                        });

                    } else {
                        this.finishLoadAssesmmentAndUpdateStore(assessmentModel);
                    }
                    break;
                }
            }
            this.aaSectionNavMessagingService.publishSectionFilterData(this.aaSectionNavMessagingService.sectionFilter);
        };

        const checkIfSupposedToTriggerTheSave = (state: IStoreState): boolean => hasQuestionIdsToSave(state) && !isSaveInProgress(state);

        // Runs when FETCH_NEW_SECTION action hits the store
        const sectionChange$ = this.assessmentDetailMiddleware$.pipe(
            filter(filterForSectionChange),
            switchMap((action: AnyAction) => checkIfSupposedToTriggerTheSave(store.getState()) ? this.saveSectionResponses().then(() => action) : of(action)),
            switchMap(determineWhichSectionToFetchAndReturnNewAssessmentModel),
            tap(({ assessmentModel, sectionId, action }) => {
                this.store.dispatch({ type: "SET_CURRENT_SECTION", selectedSectionId: sectionId });
                handleSectionUpdatesToStore(assessmentModel);
            }),
        );

        // Runs when NAVIGATE_FROM_ASSESSMENT_DETAIL action hits the store
        const routeChange$ = this.assessmentDetailMiddleware$.pipe(
            filter(filterForRouteChange),
            tap((action: AnyAction) => checkIfSupposedToTriggerTheSave(store.getState()) ? this.saveSectionResponses() : of(action)),
            tap((action: AnyAction) => {
                const { payload: deferred } = action;
                deferred.resolve(true);  // transition proceeds when promise resolves
            }),
        );

        // Runs when SUBMIT_ASSESSMENT_DETAIL_SECTION action hits the store
        const submitAssessment$ = this.assessmentDetailMiddleware$.pipe(
            filter(filterForSubmitAssessment),
            switchMap((action: any) => checkIfSupposedToTriggerTheSave(store.getState()) ? this.saveSectionResponses() : of(action)),
        );

        // Runs when SAVE_QUESTION action hits the store
        const saveQuestion$ = this.assessmentDetailMiddleware$.pipe(
            filter(filterForQuestionSave),
            rxMap(() => of(checkIfSupposedToTriggerTheSave(store.getState()))),
            switchMap((triggerSave) => !!triggerSave ? this.saveSectionResponses() : NEVER),
            rxMap((res: IProtocolResponse<IAssessmentModel>) => res && res.result ? this.AssessmentDetailAdapter.convertAssessmentStructure(res.data) : false),
            tap((assessmentModel: any) => assessmentModel && handleSectionUpdatesToStore(assessmentModel)),
        );

        // intercept transition
        $transitions.onBefore({ exiting: "zen.app.pia.module.assessment.detail" }, (transition: Transition) => {
            const deferred = this.$q.defer();
            this.assessmentDetailMiddleware$.next({ type: "NAVIGATE_FROM_ASSESSMENT_DETAIL", payload: deferred });
            return deferred.promise as any;
        });

        merge(sectionChange$, routeChange$, submitAssessment$, saveQuestion$).subscribe();
    }

    finishLoadAssesmmentAndUpdateStore(assessmentModel: IAssessmentModel) {
        this.store.dispatch({ type: AssessmentDetailAction.FINISH_LOADING_ASSESSMENT_SECTION });
        this.store.dispatch({ type: AssessmentDetailAction.UPDATE_ASSESSMENT_ALL, assessmentModel });
    }

    formatQuestionResponses(questionId: string, inventoryType?: string): IQuestionResponseV2[] {
        const responseIds: string[] = getQuestionResponseIds(questionId)(this.store.getState());
        const responseModels = getQuestionResponseModels(questionId)(this.store.getState());
        const inventoryOptionModels = getInventoryOptionModels(inventoryType)(this.store.getState());

        return map(responseIds, (id: string) => {
            const response: IQuestionResponseV2 = responseModels[id];
            const inventoryOption: IQuestionModelOption = inventoryOptionModels[id];

            if (inventoryOption) {
                return {
                    response: inventoryOption.optionDisplay,
                    responseId: inventoryOption.id,
                    type: inventoryOption.optionType,
                };
            } else if (response.type === QuestionResponseType.Others && !Utilities.matchRegex(response.responseId, Regex.GUID)) {
                return { ...response, responseId: null };
            } else {
                return response;
            }
        });
    }

    saveSectionResponses(): ng.IPromise<IProtocolResponse<IAssessmentModel> | null> {
        const assessmentModel = getAssessmentDetailModel(this.store.getState());
        if (!assessmentModel) {
            return this.$q.when(null);
        }

        const payload: ISaveSectionResponsesContract[] = [];
        const questionIdToSave = getQuestionIdsToSave(this.store.getState())[0];
        const subQuestionIdToSave = getSubQuestionIdsToSave(this.store.getState())[0];

        if (questionIdToSave && !subQuestionIdToSave) {
            return this.updateQuestionResponses(questionIdToSave);
        } else if (questionIdToSave && subQuestionIdToSave) {
            return this.updateSubQuestionResponses(questionIdToSave, subQuestionIdToSave);
        } else {
            return this.$q.when(null);
        }
    }

    updateQuestionResponses(questionIdToSave: string): ng.IPromise<IProtocolResponse<IAssessmentModel> | null> {
        const assessmentModel = getAssessmentDetailModel(this.store.getState());
        const payload: ISaveSectionResponsesContract[] = this.createQuestionResponsePayload(questionIdToSave);
        const isPersonalDataQuestion = assessmentModel.questions.byId[payload[0].questionId].questionType === "PERSONAL_DATA";

        if (payload && payload.length) {
            this.store.dispatch({ type: AssessmentDetailAction.UPDATE_QUESTION_IDS_BEING_SAVED });

            if (isPersonalDataQuestion) {
                payload[0].responses = this.formatPersonalDataQuestionsPayload(assessmentModel, payload);
            }

            return this.AssessmentDetailApi.saveSectionResponses(assessmentModel.assessmentId, payload).then((res: IProtocolResponse<IAssessmentModel>) => {
                if (!res.result) {
                    this.store.dispatch({ type: AssessmentDetailAction.RESTORE_QUESTION_IDS_TO_SAVE });
                } else {
                    this.handleInvalidRiskResponse(res.data.invalidQuestionToRiskIdMap, payload[0].questionId);
                    this.store.dispatch({ type: AssessmentDetailAction.REMOVE_QUESTION_IDS_BEING_SAVED });
                }
                return res;
            });
        } else {
            return this.$q.when(null);
        }
    }

    handleInvalidRiskResponse(invalidQuestionRisksIdMap: IStringMap<string[]>, questionId: string) {
        if (invalidQuestionRisksIdMap && keys(invalidQuestionRisksIdMap).length) {
            this.store.dispatch({ type: AssessmentDetailAction.ADD_INVALID_RISK_RESPONSE, invalidQuestionRisksIdMap });
        } else {
            this.store.dispatch({ type: AssessmentDetailAction.REMOVE_INVALID_RISK_RESPONSE, questionId });
        }
    }

    createQuestionResponsePayload(questionIdToSave: string): ISaveSectionResponsesContract[] {
        const assessmentModel = getAssessmentDetailModel(this.store.getState());

        return map([questionIdToSave], (questionId: string) => {
            const question: IQuestionModel = getQuestionModels(this.store.getState())[questionId];
            let inventoryType: string;
            if (question.attributes) {
                inventoryType = question.attributes.inventoryType;
            }
            const result = {
                assessmentId: assessmentModel.assessmentId,
                parentAssessmentDetailId: null,
                sectionId: assessmentModel.section.sectionId,
                questionId,
                responses: this.formatQuestionResponses(question.id, inventoryType),
            };

            if (question && question.justification) {
                result.responses.push({
                    response: question.justification,
                    responseId: null,
                    type: QuestionResponseType.Justification,
                });
            }

            return result;
        });
    }

    updateSubQuestionResponses(questionId: string, subQuestionId: string): ng.IPromise<IProtocolResponse<IAssessmentModel> | null> {
        const assessmentModel = getAssessmentDetailModel(this.store.getState());
        const payload: ISaveSectionResponsesContract[] = this.createSubQuestionResponsePayload(questionId, subQuestionId);

        if (payload && payload.length) {
            this.store.dispatch({ type: AssessmentDetailAction.UPDATE_SUB_QUESTION_IDS_BEING_SAVED });

            return this.AssessmentDetailApi.saveSectionResponses(assessmentModel.assessmentId, payload).then((res: IProtocolResponse<IAssessmentModel>) => {
                if (!res.result) {
                    this.store.dispatch({ type: AssessmentDetailAction.RESTORE_SUB_QUESTION_IDS_TO_SAVE });
                } else {
                    this.handleInvalidRiskResponse(res.data.invalidQuestionToRiskIdMap, payload[0].questionId);
                    this.store.dispatch({ type: AssessmentDetailAction.REMOVE_SUB_QUESTION_IDS_BEING_SAVED });
                }
                return res;
            });
        } else {
            return this.$q.when(null);
        }
    }

    createSubQuestionResponsePayload(questionIdToSave: string, subQuestionIdToSave: string): ISaveSectionResponsesContract[] {
        const assessmentModel = getAssessmentDetailModel(this.store.getState());

        return map([questionIdToSave], (questionId: string) => {
            const subQuestion: IQuestionModelSubQuestion = getSubQuestionModels(questionId)(this.store.getState())[subQuestionIdToSave];

            const result = {
                assessmentId: assessmentModel.assessmentId,
                parentAssessmentDetailId: subQuestionIdToSave,
                sectionId: assessmentModel.section.sectionId,
                questionId,
                responses: this.formatSubQuestionResponses(questionId, subQuestionIdToSave),
            };

            if (subQuestion && subQuestion.justification) {
                result.responses.push({
                    response: subQuestion.justification,
                    responseId: null,
                    type: QuestionResponseType.Justification,
                });
            }

            return result;
        });
    }

    formatSubQuestionResponses(questionId: string, subQuestionId: string): IQuestionResponseV2[] {
        const subQuestionResponses = getSubQuestionResponsesArray(questionId, subQuestionId)(this.store.getState());
        const responses = [];
        if (subQuestionResponses && subQuestionResponses.length) {
            subQuestionResponses.forEach((response: IQuestionResponseV2) => {
                if (!response) return;
                if (response.type === QuestionResponseType.Others) {
                    responses.push({ ...response, responseId: null });
                } else {
                    responses.push(response);
                }
            });
        }
        return responses;
    }

    formatPersonalDataQuestionsPayload(assessmentDetail: IAssessmentModel, payload: ISaveSectionResponsesContract[]): IQuestionPersonalDataResponse[] {
        const currentPayload: ISaveSectionResponsesContract = payload[0];
        const responseById = assessmentDetail.questions.byId[currentPayload.questionId].responses.byId[currentPayload.responses[0].responseId];
        const formattedPayload: IQuestionPersonalDataResponse[] = [];

        forEach((currentPayload.responses[0] as IPersonalDataQuestionResponseV2).responseMap, (dataSubject, dataSubjectId) => {
            forEach(dataSubject, (category, categoryId) => {
                forEach(category, (dataElement) => {
                    formattedPayload.push({
                        response: null,
                        responseId: null,
                        type: null,
                        responseMap: {
                            DATA_SUBJECTS: { id: dataSubjectId, name: responseById.dataSubjectById[dataSubjectId] },
                            DATA_CATEGORIES: { id: categoryId, name: responseById.categoryById[categoryId] },
                            DATA_ELEMENTS: { id: dataElement.id, name: dataElement.name },
                        },
                    });
                });
            });
        });

        return formattedPayload;
    }
}
