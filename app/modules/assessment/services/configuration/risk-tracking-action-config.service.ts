import {
    Injectable,
    Inject,
} from "@angular/core";

// Interfaces
import { IRiskActionButtonConfig } from "interfaces/assessment-risk-action-button-config.interface";
import {
    IStringMap,
    INameId,
} from "interfaces/generic.interface";
import { IStore } from "interfaces/redux.interface";
import { StoreToken } from "tokens/redux-store.token";
import { IRiskDetails } from "interfaces/risk.interface";
import { IAssessmentModel } from "interfaces/assessment/assessment-model.interface";

// Redux
import { getCurrentUser } from "oneRedux/reducers/user.reducer";

// Enums
import { RiskV2States } from "enums/riskV2.enum";

// Constants
import { RiskV2Actions } from "constants/assessment-riskV2-action.constant";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

// Services
import { AssessmentRiskV2PermissionService } from "modules/assessment/services/permission/assessment-risk-v2-permission.service";

@Injectable()
export class RiskTrackingActionConfig {
    constructor(
        @Inject(StoreToken) private store: IStore,
        private AssessmentRiskV2Permission: AssessmentRiskV2PermissionService,
        private translatePipe: TranslatePipe,
    ) { }

    getEditRiskDetailActions(): IStringMap<IRiskActionButtonConfig> {
        const primaryButtonObj = {
            name: this.translatePipe.transform("Save"),
            identifier: "AssessmentRiskPaneSaveButton",
            isDisabled: false,
            isLoading: false,
            id: RiskV2Actions.SaveRisk,
        };
        const secondaryButtonObj = {
            name: this.translatePipe.transform("Cancel"),
            identifier: "AssessmentRiskPaneCancelButton",
            isDisabled: false,
            isLoading: false,
            id: RiskV2Actions.CancelEdit,
        };
        return {
            primaryButtonObj,
            secondaryButtonObj,
        };
    }

    getRiskIdentifiedStateActions(assessment: IAssessmentModel, risk: IRiskDetails): IStringMap<IRiskActionButtonConfig> {
        const hasPermission = this.AssessmentRiskV2Permission.hasRiskPermission("canAddRecommendation", getCurrentUser(this.store.getState()), assessment, risk);
        let primaryButtonObj = null;
        if (hasPermission) {
            primaryButtonObj = {
                name: this.translatePipe.transform("AddRecommendation"),
                identifier: "AssessmentRiskPaneAddRecommendationButton",
                isDisabled: false,
                isLoading: false,
                id: RiskV2Actions.AddRecommendation,
            };
        }
        return {
            primaryButtonObj,
            secondaryButtonObj: null,
        };
    }

    getAddRecommendationStateActions(): IStringMap<IRiskActionButtonConfig> {
        const primaryButtonObj = {
            name: this.translatePipe.transform("Save"),
            identifier: "AssessmentRiskPaneSaveRecommendationButton",
            isDisabled: false,
            isLoading: false,
            id: RiskV2Actions.SaveRecommendation,
        };
        const secondaryButtonObj = {
            name: this.translatePipe.transform("Cancel"),
            identifier: "AssessmentRiskPaneCancelRecommendationSaveButton",
            isDisabled: false,
            isLoading: false,
            id: RiskV2Actions.CancelRecommendation,
        };
        return {
            primaryButtonObj,
            secondaryButtonObj,
        };
    }

    getRecommendationAddedStateActions(assessment: IAssessmentModel, risk: IRiskDetails): IStringMap<IRiskActionButtonConfig> {
        const hasPermission = this.AssessmentRiskV2Permission.hasRiskPermission("canSendRecommendation", getCurrentUser(this.store.getState()), assessment, risk);
        let primaryButtonObj = null;
        if (hasPermission) {
            primaryButtonObj = {
                name: this.translatePipe.transform("SendRecommendation"),
                identifier: "AssessmentRiskPaneSendRecommendationButton",
                isDisabled: false,
                isLoading: false,
                id: RiskV2Actions.SendRecommendation,
            };
        }
        return {
            primaryButtonObj,
            secondaryButtonObj: null,
        };
    }

    getRecommendationSentStateActions(assessment: IAssessmentModel, risk: IRiskDetails): IStringMap<IRiskActionButtonConfig> {
        const canAddRemediation = this.AssessmentRiskV2Permission.hasRiskPermission("canAddRemediation", getCurrentUser(this.store.getState()), assessment, risk);
        const canAddException = this.AssessmentRiskV2Permission.hasRiskPermission("canAddException", getCurrentUser(this.store.getState()), assessment, risk);
        let primaryButtonObj = null;
        let secondaryButtonObj = null;
        if (canAddRemediation) {
            primaryButtonObj = {
                name: this.translatePipe.transform("ProposeRemediation"),
                identifier: "AssessmentRiskPaneProposeRemediationButton",
                isDisabled: false,
                isLoading: false,
                id: RiskV2Actions.ProposeRemediation,
            };
        }
        if (canAddException) {
            secondaryButtonObj = {
                name: this.translatePipe.transform("RequestException"),
                identifier: "AssessmentRiskPaneRequestExceptionButton",
                isDisabled: false,
                isLoading: false,
                id: RiskV2Actions.RequestException,
            };
        }
        return {
            primaryButtonObj,
            secondaryButtonObj,
        };
    }

    public getAddRemediaitonStateActions(): IStringMap<IRiskActionButtonConfig> {
        const primaryButtonObj = {
            name: this.translatePipe.transform("SendRemediation"),
            identifier: "AssessmentRiskPaneSendRemediationButton",
            isDisabled: false,
            isLoading: false,
            id: RiskV2Actions.SendRemediation,
        };
        const secondaryButtonObj = {
            name: this.translatePipe.transform("Cancel"),
            identifier: "AssessmentRiskPaneCancelRemediationButton",
            isDisabled: false,
            isLoading: false,
            id: RiskV2Actions.CancelRemediation,
        };
        return {
            primaryButtonObj,
            secondaryButtonObj,
        };
    }

    public getRemediationAddedStateActions(assessment: IAssessmentModel, risk: IRiskDetails): IStringMap<IRiskActionButtonConfig> {
        const canApproveRemediation = this.AssessmentRiskV2Permission.hasRiskPermission("canApproveRemediation", getCurrentUser(this.store.getState()), assessment, risk);
        const canRejectRemediation = this.AssessmentRiskV2Permission.hasRiskPermission("canRejectRemediation", getCurrentUser(this.store.getState()), assessment, risk);
        let primaryButtonObj = null;
        let secondaryButtonObj = null;
        if (canApproveRemediation) {
            primaryButtonObj = {
                name: this.translatePipe.transform("ApproveRemediation"),
                identifier: "AssessmentRiskPaneApproveRemediationButton",
                isDisabled: false,
                isLoading: false,
                id: RiskV2Actions.ApproveRemediation,
            };
        }
        if (canRejectRemediation) {
            secondaryButtonObj = {
                name: this.translatePipe.transform("RejectRemediation"),
                identifier: "AssessmentRiskPaneRejectRemediationButton",
                isDisabled: false,
                isLoading: false,
                id: RiskV2Actions.RejectRemediation,
            };
        }
        return {
            primaryButtonObj,
            secondaryButtonObj,
        };
    }

    getAddExceptionStateActions(): IStringMap<IRiskActionButtonConfig> {
        const primaryButtonObj = {
            name: this.translatePipe.transform("SendException"),
            identifier: "AssessmentRiskPaneSendExceptionButton",
            isDisabled: false,
            isLoading: false,
            id: RiskV2Actions.SendException,
        };
        const secondaryButtonObj = {
            name: this.translatePipe.transform("Cancel"),
            identifier: "AssessmentRiskPaneCancelExceptionButton",
            isDisabled: false,
            isLoading: false,
            id: RiskV2Actions.CancelException,
        };
        return {
            primaryButtonObj,
            secondaryButtonObj,
        };
    }

    getExceptionAddedActions(assessment: IAssessmentModel, risk: IRiskDetails): IStringMap<IRiskActionButtonConfig> {
        const canGrantException = this.AssessmentRiskV2Permission.hasRiskPermission("canGrantException", getCurrentUser(this.store.getState()), assessment, risk);
        const canRejectException = this.AssessmentRiskV2Permission.hasRiskPermission("canRejectException", getCurrentUser(this.store.getState()), assessment, risk);
        let primaryButtonObj = null;
        let secondaryButtonObj = null;
        if (canGrantException) {
            primaryButtonObj = {
                name: this.translatePipe.transform("GrantException"),
                identifier: "AssessmentRiskPaneGrantExceptionButton",
                isDisabled: false,
                isLoading: false,
                id: RiskV2Actions.GrantException,
            };
        }
        if (canRejectException) {
            secondaryButtonObj = {
                name: this.translatePipe.transform("RejectException"),
                identifier: "AssessmentRiskPaneRejectExceptionButton",
                isDisabled: false,
                isLoading: false,
                id: RiskV2Actions.RejectException,
            };
        }
        return {
            primaryButtonObj,
            secondaryButtonObj,
        };
    }

    getContextMenuOptions(assessment: IAssessmentModel, risk: IRiskDetails): Array<INameId<string>> {
        const options: Array<INameId<string>> = [];
        const canEditRiskDetails = this.hasEditRiskPermission(assessment, risk);
        const canEditRiskOwner = this.hasEditRiskOwnerPermission(assessment, risk);
        const canDeleteRisk = this.AssessmentRiskV2Permission.hasRiskPermission("canDeleteRisk", getCurrentUser(this.store.getState()), assessment, risk);
        if (canEditRiskDetails || canEditRiskOwner) {
            options.push({
                name: this.translatePipe.transform("Edit"),
                id: RiskV2Actions.EditRisk,
            });
        }
        if (canDeleteRisk) {
            options.push({
                name: this.translatePipe.transform("Delete"),
                id: RiskV2Actions.DeleteRisk,
            });
        }
        switch (risk.state) {
            case RiskV2States.RECOMMENDATION_ADDED:
            case RiskV2States.RECOMMENDATION_SENT:
                const canClearRecommenation = this.AssessmentRiskV2Permission.hasRiskPermission("canClearRecommenation", getCurrentUser(this.store.getState()), assessment, risk);
                if (canClearRecommenation) {
                    options.push({
                        name: this.translatePipe.transform("ClearRecommendation"),
                        id: RiskV2Actions.ClearRecommendation,
                    });
                }
                break;
            case RiskV2States.REMEDIATION_PROPOSED:
                const canClearRemediation = this.AssessmentRiskV2Permission.hasRiskPermission("canClearRemediation", getCurrentUser(this.store.getState()), assessment, risk);
                if (canClearRemediation) {
                    options.push({
                        name: this.translatePipe.transform("ClearRemediation"),
                        id: RiskV2Actions.ClearRemediation,
                    });
                }
                break;
            case RiskV2States.EXCEPTION_REQUESTED:
                const canClearException = this.AssessmentRiskV2Permission.hasRiskPermission("canClearException", getCurrentUser(this.store.getState()), assessment, risk);
                if (canClearException) {
                    options.push({
                        name: this.translatePipe.transform("ClearException"),
                        id: RiskV2Actions.ClearException,
                    });
                }
                break;
            default:
                break;
        }
        return options;
    }

    hasEditRiskPermission(assessment: IAssessmentModel, risk: IRiskDetails): boolean {
        return this.AssessmentRiskV2Permission.hasRiskPermission("canEditRisk", getCurrentUser(this.store.getState()), assessment, risk);
    }

    hadEditCategoryPermission(assessment: IAssessmentModel, risk: IRiskDetails): boolean {
        return this.AssessmentRiskV2Permission.hasRiskPermission("canEditRiskCategory", getCurrentUser(this.store.getState()), assessment, risk);
    }

    hasEditRiskOwnerPermission(assessment: IAssessmentModel, risk: IRiskDetails): boolean {
        return this.AssessmentRiskV2Permission.hasRiskPermission("canEditRiskOwner", getCurrentUser(this.store.getState()), assessment, risk);
    }

    hasAddRecommendationPermission(assessment: IAssessmentModel, risk: IRiskDetails): boolean {
        return this.AssessmentRiskV2Permission.hasRiskPermission("canAddRecommendation", getCurrentUser(this.store.getState()), assessment, risk);
    }

    hasEditRecommendationPermission(assessment: IAssessmentModel, risk: IRiskDetails): boolean {
        return this.AssessmentRiskV2Permission.hasRiskPermission("canEditRecommendation", getCurrentUser(this.store.getState()), assessment, risk);
    }

    hasEditRemediationPermission(assessment: IAssessmentModel, risk: IRiskDetails): boolean {
        return this.AssessmentRiskV2Permission.hasRiskPermission("canEditRemediation", getCurrentUser(this.store.getState()), assessment, risk);
    }

    hasEditExceptionPermission(assessment: IAssessmentModel, risk: IRiskDetails): boolean {
        return this.AssessmentRiskV2Permission.hasRiskPermission("canEditException", getCurrentUser(this.store.getState()), assessment, risk);
    }
}
