// Core
import { Injectable } from "@angular/core";

// 3rd Party
import { includes } from "lodash";

// Enums
import { AssessmentStatus } from "enums/assessment/assessment-status.enum";

// Services
import { Permissions } from "modules/shared/services/helper/permissions.service";

// Constants
import { AssessmentPermissions } from "constants/assessment.constants";
import { AssignmentTypes } from "constants/assignment-types.constant";
import { UserRoles } from "constants/user-roles.constant";
import { EmptyGuid } from "constants/empty-guid.constant";

// Interfaces
import { IAssessmentModel } from "interfaces/assessment/assessment-model.interface";
import { ISectionModel } from "interfaces/assessment/section-model.interface";

@Injectable()
export class AssessmentSectionDetailHeaderPermissionService {

    private assessmentSectionDetailHeaderPermissions;

    constructor(
        private permissions: Permissions,
    ) {
        const { Not_Started, In_Progress, Under_Review, Completed } = AssessmentStatus;
        const {
            AssessmentDetailsView,
            AssessmentOrgView,
            AssessmentApproverReassign,
            AssessmentRespondentReassign,
            AssessmentEditDetails,
            AssessmentChangeAssessmentOrg,
        } = AssessmentPermissions;
        const { approver, respondent } = AssignmentTypes;

        this.assessmentSectionDetailHeaderPermissions = {
            canViewAssessmentName: {
                allowedPermission: AssessmentDetailsView,
                allowedAssessmentStates: [Not_Started, In_Progress, Under_Review, Completed],
                allowedSections: ["welcome"],
            },
            canEditAssessmentName: {
                allowedPermission: AssessmentEditDetails,
                allowedAssessmentStates: [Not_Started, In_Progress, Under_Review],
                disallowedRole: [UserRoles.Invited],
            },
            canViewAssessmentCreator: {
                allowedPermission: AssessmentDetailsView,
                allowedAssessmentStates: [Not_Started, In_Progress, Under_Review, Completed],
                allowedSections: ["welcome"],
            },
            canViewAssessmentOrg: {
                allowedPermission: AssessmentDetailsView,
                allowedAssessmentStates: [Not_Started, In_Progress, Under_Review, Completed],
                allowedSections: ["welcome"],
            },
            canEditAssessmentOrg: {
                allowedPermission: AssessmentChangeAssessmentOrg,
                allowedAssessmentStates: [Not_Started, In_Progress, Under_Review, Completed],
            },
            canViewAssessmentDeadline: {
                allowedPermission: AssessmentDetailsView,
                allowedAssessmentStates: [Not_Started, In_Progress, Under_Review, Completed],
                allowedSections: ["welcome"],
            },
            canEditAssessmentDeadline: {
                allowedPermission: AssessmentEditDetails,
                allowedAssessmentStates: [Not_Started, In_Progress, Under_Review],
                disallowedRole: [UserRoles.Invited],
            },
            canViewAssessmentDescription: {
                allowedPermission: AssessmentDetailsView,
                allowedAssessmentStates: [Not_Started, In_Progress, Under_Review, Completed],
                allowedSections: ["welcome"],
            },
            canEditAssessmentDescription: {
                allowedPermission: AssessmentEditDetails,
                allowedAssessmentStates: [Not_Started, In_Progress, Under_Review],
                disallowedRole: [UserRoles.Invited],
            },
            canEditAssessmentReminder: {
                allowedPermission: AssessmentEditDetails,
                allowedAssessmentStates: [Not_Started, In_Progress, Under_Review],
                disallowedRole: [UserRoles.Invited],
            },
            canViewAssessmentApprover: {
                allowedPermission: AssessmentDetailsView,
                allowedAssessmentStates: [Not_Started, In_Progress, Under_Review, Completed],
                allowedSections: ["all"],
            },
            canEditAssessmentApprover: {
                allowedPermission: AssessmentApproverReassign,
                allowedAssessmentStates: [Not_Started, In_Progress, Under_Review],
                disallowedRole: [UserRoles.Invited],
            },
            canViewAssessmentRespondent: {
                allowedPermission: AssessmentDetailsView,
                allowedAssessmentStates: [Not_Started, In_Progress, Under_Review, Completed],
                allowedSections: ["all"],
            },
            canEditAssessmentRespondent: {
                allowedPermission: AssessmentRespondentReassign,
                allowedAssessmentStates: [Not_Started, In_Progress, Under_Review],
                disallowedRole: [UserRoles.Invited],
            },
            canShowSectionStatus: {
                allowedPermission: AssessmentDetailsView,
                allowedAssessmentStates: [Not_Started, In_Progress, Under_Review, Completed],
                allowedSections: ["not welcome"],
            },
        };
    }

    public hasViewPermission(permissionToCheck: string, assessment: IAssessmentModel, currentSection: ISectionModel): boolean {
        const config = this.assessmentSectionDetailHeaderPermissions[permissionToCheck];

        return this.hasPermission(config.allowedPermission)
            && this.isAllowedState(config.allowedAssessmentStates, assessment.status)
            && this.isAllowedSection(config.allowedSections, currentSection);
    }

    public hasEditPermission(permissionToCheck: string, assessment: IAssessmentModel, currentSection: ISectionModel): boolean {
        const config = this.assessmentSectionDetailHeaderPermissions[permissionToCheck];

        return this.hasPermission(config.allowedPermission)
            && this.isAllowedState(config.allowedAssessmentStates, assessment.status);
    }

    private hasPermission(permissionToCheck: string): boolean {
        return this.permissions.canShow(permissionToCheck) || this.permissions.canShow("ProjectsFullAccess");
    }

    private isAllowedState(stateArray: Array<string | number>, state: string | number): boolean {
        return includes(stateArray, state);
    }

    private isAllowedSection(allowedSection: string[], currentSection: ISectionModel): boolean {
        if (allowedSection[0] === "all") return true;
        if (allowedSection[0] === "welcome") return !currentSection ? true : false;
        if (allowedSection[0] === "not welcome") return currentSection ? currentSection.sectionId !== EmptyGuid : false;
    }
}
