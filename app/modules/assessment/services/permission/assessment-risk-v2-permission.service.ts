import {
    Injectable,
} from "@angular/core";

// External libraries.
import invariant from "invariant";
import {
    includes,
    isNil,
    some,
} from "lodash";

// Interfaces
import { IAssessmentModel } from "interfaces/assessment/assessment-model.interface";
import { IRiskDetails } from "interfaces/risk.interface";
import {
    IStringMap,
    INameId,
} from "interfaces/generic.interface";
import { IUser } from "interfaces/user.interface";

// Enums
import { AssessmentStatus } from "enums/assessment/assessment-status.enum";
import { RiskV2States } from "enums/riskV2.enum";

// Constants
import { AssignmentTypes } from "constants/assignment-types.constant";
import { RiskV2Permissions } from "constants/risk-v2.constants";

// Services
import { Permissions } from "modules/shared/services/helper/permissions.service";

@Injectable()
export class AssessmentRiskV2PermissionService {
    private riskPermissions;

    constructor(
        private permissions: Permissions,
    ) {
        const { NONE, IDENTIFIED, RECOMMENDATION_ADDED, RECOMMENDATION_SENT, REMEDIATION_PROPOSED, EXCEPTION_REQUESTED, REDUCED, RETAINED, ARCHIVED_IN_VERSION } = RiskV2States;
        const { Under_Review, Completed } = AssessmentStatus;
        const { approver, riskOwner, respondent } = AssignmentTypes;

        this.riskPermissions = {
            canCreateRisk: {
                allowedPermission: RiskV2Permissions.RiskCreate,
                allowedAssignments: [approver],
                allowedProjectStates: [Under_Review, Completed],
                allowedRiskStates: [NONE],
            },
            canViewRisk: {
                allowedPermission: RiskV2Permissions.RiskView,
                allowedAssignments: [approver, respondent, riskOwner],
                allowedProjectStates: [Under_Review, Completed],
                allowedRiskStates: [NONE, IDENTIFIED, RECOMMENDATION_ADDED, RECOMMENDATION_SENT, REMEDIATION_PROPOSED, EXCEPTION_REQUESTED, REDUCED, RETAINED, ARCHIVED_IN_VERSION],
            },
            canEditRisk: {
                allowedPermission: RiskV2Permissions.RiskEdit,
                allowedAssignments: [approver],
                allowedProjectStates: [Under_Review, Completed],
                allowedRiskStates: [IDENTIFIED, RECOMMENDATION_ADDED, RECOMMENDATION_SENT, REMEDIATION_PROPOSED, EXCEPTION_REQUESTED],
            },
            canEditRiskOwner: {
                allowedPermission: RiskV2Permissions.RiskEdit,
                allowedAssignments: [approver, riskOwner],
                allowedProjectStates: [Under_Review, Completed],
                allowedRiskStates: [IDENTIFIED, RECOMMENDATION_ADDED, RECOMMENDATION_SENT, REMEDIATION_PROPOSED, EXCEPTION_REQUESTED],
            },
            canAddRecommendation: {
                allowedPermission: RiskV2Permissions.RiskRecommendation,
                allowedAssignments: [approver],
                allowedProjectStates: [Under_Review, Completed],
                allowedRiskStates: [IDENTIFIED],
            },
            canEditRecommendation: {
                allowedPermission: RiskV2Permissions.RiskRecommendation,
                allowedAssignments: [approver],
                allowedProjectStates: [Under_Review, Completed],
                allowedRiskStates: [RECOMMENDATION_ADDED],
            },
            canClearRecommenation: {
                allowedPermission: RiskV2Permissions.RiskRecommendation,
                allowedAssignments: [approver],
                allowedProjectStates: [Under_Review, Completed],
                allowedRiskStates: [RECOMMENDATION_ADDED, RECOMMENDATION_SENT],
            },
            canSendRecommendation: {
                allowedPermission: RiskV2Permissions.RiskRecommendation,
                allowedAssignments: [approver],
                allowedProjectStates: [Under_Review, Completed],
                allowedRiskStates: [RECOMMENDATION_ADDED],
            },
            canAddRemediation: {
                allowedPermission: RiskV2Permissions.RiskRemediation,
                allowedAssignments: [approver, riskOwner],
                allowedProjectStates: [Under_Review, Completed],
                allowedRiskStates: [RECOMMENDATION_SENT],
            },
            canEditRemediation: {
                allowedPermission: RiskV2Permissions.RiskRemediation,
                allowedAssignments: [approver, riskOwner],
                allowedProjectStates: [Under_Review, Completed],
                allowedRiskStates: [REMEDIATION_PROPOSED],
            },
            canClearRemediation: {
                allowedPermission: RiskV2Permissions.RiskRemediation,
                allowedAssignments: [approver, riskOwner],
                allowedProjectStates: [Under_Review, Completed],
                allowedRiskStates: [REMEDIATION_PROPOSED],
            },
            canApproveRemediation: {
                allowedPermission: RiskV2Permissions.RiskRemediation,
                allowedAssignments: [approver],
                allowedProjectStates: [Under_Review, Completed],
                allowedRiskStates: [REMEDIATION_PROPOSED],
            },
            canRejectRemediation: {
                allowedPermission: RiskV2Permissions.RiskRemediation,
                allowedAssignments: [approver],
                allowedProjectStates: [Under_Review, Completed],
                allowedRiskStates: [REMEDIATION_PROPOSED],
            },
            canAddException: {
                allowedPermission: RiskV2Permissions.RiskException,
                allowedAssignments: [approver, riskOwner],
                allowedProjectStates: [Under_Review, Completed],
                allowedRiskStates: [RECOMMENDATION_SENT],
            },
            canEditException: {
                allowedPermission: RiskV2Permissions.RiskException,
                allowedAssignments: [approver, riskOwner],
                allowedProjectStates: [Under_Review, Completed],
                allowedRiskStates: [EXCEPTION_REQUESTED],
            },
            canClearException: {
                allowedPermission: RiskV2Permissions.RiskException,
                allowedAssignments: [approver, riskOwner],
                allowedProjectStates: [Under_Review, Completed],
                allowedRiskStates: [EXCEPTION_REQUESTED],
            },
            canGrantException: {
                allowedPermission: RiskV2Permissions.RiskException,
                allowedAssignments: [approver],
                allowedProjectStates: [Under_Review, Completed],
                allowedRiskStates: [EXCEPTION_REQUESTED],
            },
            canRejectException: {
                allowedPermission: RiskV2Permissions.RiskException,
                allowedAssignments: [approver],
                allowedProjectStates: [Under_Review, Completed],
                allowedRiskStates: [EXCEPTION_REQUESTED],
            },
            canDeleteRisk: {
                allowedPermission: RiskV2Permissions.AssessmentRiskDelete,
                allowedAssignments: [approver],
                allowedProjectStates: [Under_Review, Completed],
                allowedRiskStates: [IDENTIFIED, RECOMMENDATION_ADDED, RECOMMENDATION_SENT, REMEDIATION_PROPOSED, EXCEPTION_REQUESTED, REDUCED, RETAINED],
            },
            canEditRiskCategory: {
                allowedPermission: RiskV2Permissions.RiskEditCategory,
                allowedAssignments: [ approver, riskOwner ],
                allowedProjectStates: [Under_Review, Completed],
                allowedRiskStates: [IDENTIFIED, RECOMMENDATION_ADDED, RECOMMENDATION_SENT, REMEDIATION_PROPOSED, EXCEPTION_REQUESTED ],
            }
        };
    }

    public hasRiskPermission(permissionToCheck: string, currentUser: IUser, assessmentModel: IAssessmentModel, riskModel: IRiskDetails): boolean {
        if (assessmentModel) {
            const config = this.riskPermissions[permissionToCheck];
            invariant(Boolean(config), `Permission: ${permissionToCheck || "undefined"}, is not available.`);
            return (this.permissions.canShow(config.allowedPermission)
                || this.isAllowedUser(config.allowedAssignments, currentUser, assessmentModel, riskModel))
                && includes(config.allowedProjectStates, assessmentModel.status)
                && this.isAllowedRiskState(config.allowedRiskStates, riskModel);
        }
        return false;
    }

    private isAllowedRiskState(permissionArray: string[], riskModel: IRiskDetails | undefined): boolean {
        if (isNil(riskModel) && includes(permissionArray, RiskV2States.NONE)) {
            return true;
        }
        return !isNil(riskModel) && (includes(permissionArray, riskModel.state) || includes(permissionArray, RiskV2States[riskModel.state]));
    }

    private isAllowedUser(permissionArray: string[], user: IUser, assessmentModel: IAssessmentModel, riskModel: IRiskDetails): boolean {
        return permissionArray.some((permission: string): boolean => {
            if (permission === AssignmentTypes.approver) {
                return some(assessmentModel.approvers, (approver: IStringMap<string>) => approver.id === user.Id);
            } else if (permission === AssignmentTypes.respondent) {
                return some(assessmentModel.respondents, (respondent: INameId<string>) => respondent.id === user.Id);
            } else if (riskModel) {
                return riskModel.riskOwnerId === user.Id;
            } else {
                return false;
            }
        });
    }
}
