// Angular
import {
    Injectable,
} from "@angular/core";

// Constants
import { AssessmentPermissions } from "constants/assessment.constants";
import { AssignmentTypes } from "constants/assignment-types.constant";
import { TemplateTypes } from "constants/template-types.constant";

// Enums
import { AssessmentStatus } from "enums/assessment/assessment-status.enum";

// Interfaces
import { IAssessmentQuestionPermission } from "interfaces/assessment.interface";
import {
    IAssessmentModel,
    IAssessmentApprover,
} from "interfaces/assessment/assessment-model.interface";
import { IQuestionModel } from "interfaces/assessment/question-model.interface";
import { INameId } from "interfaces/generic.interface";

// Pipes
import { PermitPipe } from "pipes/permit.pipe";

@Injectable()
export class AaSectionQuestionPermissionService {

    private assessmentQuestionPermissions = {
        canViewQuestion: {
            allowedPermission: AssessmentPermissions.AssessmentQuestionView,
            allowedRoles: [AssignmentTypes.approver, AssignmentTypes.respondent],
            allowedAssessmentStates: ["all"],
        },
        canEditQuestion: {
            allowedPermission: AssessmentPermissions.AssessmentEditResponses,
            allowedRoles: [AssignmentTypes.approver, AssignmentTypes.respondent],
            allowedAssessmentStates: [AssessmentStatus.In_Progress, AssessmentStatus.Under_Review, AssessmentStatus.Not_Started],
        },
    };

    constructor(private readonly permitPipe: PermitPipe) { }

    hasQuestionEditPermission(permissionToCheck: string, currentUserId: string, assessmentModel: IAssessmentModel, questionModel: IQuestionModel): boolean {
        const config: IAssessmentQuestionPermission = this.assessmentQuestionPermissions[permissionToCheck];

        return ((this.permitPipe.transform(config.allowedPermission)
            || this.isAllowedUserEdit(config.allowedRoles, currentUserId, assessmentModel, questionModel))
            && this.isAllowedState(config.allowedAssessmentStates, assessmentModel.status))
            || assessmentModel.template.templateType === TemplateTypes.RA
            || assessmentModel.template.templateType === TemplateTypes.GRA;
    }

    private isAllowedState(stateArray: Array<string | number>, state: string | number): boolean {
        return stateArray[0] === "all" || stateArray.includes(state);
    }

    private isAllowedUserEdit(permissionArray: string[], currentUserId: string, assessmentModel: IAssessmentModel, questionModel: IQuestionModel): boolean {

        let isApprover = false;
        let isRespondent = false;

        permissionArray.forEach((permission: string): void => {
            if (permission === AssignmentTypes.approver) {
                isApprover = assessmentModel.approvers.some((approver: IAssessmentApprover) => approver.id === currentUserId);
            } else if (permission === AssignmentTypes.respondent) {
                isRespondent = assessmentModel.respondents.some((respondent: INameId<string>) => respondent.id === currentUserId);
            }
        });

        if (assessmentModel.status === AssessmentStatus.Under_Review) {
            if (isRespondent) {
                return questionModel.responseEditableWhileUnderReview;
            } else {
                return isApprover;
            }
        } else if (assessmentModel.status === AssessmentStatus.In_Progress) {
            return isApprover || (isRespondent && (assessmentModel.editAllResponsesWhenInProgress || questionModel.responseEditableWhileUnderReview));
        } else {
            return isApprover || isRespondent;
        }
    }
}
