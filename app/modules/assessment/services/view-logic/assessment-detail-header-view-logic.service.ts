// Core
import { Injectable } from "@angular/core";

// Interfaces
import { IAssessmentModel } from "interfaces/assessment/assessment-model.interface";
import { IStringMap } from "interfaces/generic.interface";
import { ISectionModel } from "interfaces/assessment/section-model.interface";

// Services
import { AssessmentSectionDetailHeaderPermissionService } from "modules/assessment/services/permission/assessment-detail-header-permission.service";

@Injectable()
export class AssessmentSectionDetailHeaderViewLogicService {
    constructor(
        private AssessmentSectionDetailHeaderPermission: AssessmentSectionDetailHeaderPermissionService,
    ) { }

    calculateNewViewState(assessment: IAssessmentModel, currentSection: ISectionModel): IStringMap<boolean> {
        return {
            // Show/Hide
            canShowDescription: this.hasViewPermission("canViewAssessmentDescription", assessment, currentSection),
            canShowName: this.hasViewPermission("canViewAssessmentName", assessment, currentSection),
            canShowOrg: this.hasViewPermission("canViewAssessmentOrg", assessment, currentSection),
            canShowDeadline: this.hasViewPermission("canViewAssessmentDeadline", assessment, currentSection),
            canShowApprover: this.hasViewPermission("canViewAssessmentApprover", assessment, currentSection),
            canShowRespondent: this.hasViewPermission("canViewAssessmentRespondent", assessment, currentSection),
            canShowCreator: this.hasViewPermission("canViewAssessmentCreator", assessment, currentSection),
            // Edit
            canEditDescription: this.hasEditPermission("canEditAssessmentDescription", assessment, currentSection),
            canEditName: this.hasEditPermission("canEditAssessmentName", assessment, currentSection),
            canEditOrg: this.hasEditPermission("canEditAssessmentOrg", assessment, currentSection),
            canEditDeadline: this.hasEditPermission("canEditAssessmentDeadline", assessment, currentSection),
            canEditReminder: this.hasEditPermission("canEditAssessmentReminder", assessment, currentSection),
            canEditApprover: this.hasEditPermission("canEditAssessmentApprover", assessment, currentSection),
            canEditRespondent: this.hasEditPermission("canEditAssessmentRespondent", assessment, currentSection),
        };
    }

    private hasViewPermission(itemPermissionsKey: string, assessment: IAssessmentModel, currentSection: ISectionModel): boolean {
        return this.AssessmentSectionDetailHeaderPermission.hasViewPermission(itemPermissionsKey, assessment, currentSection);
    }

    private hasEditPermission(itemPermissionsKey: string, assessment: IAssessmentModel, currentSection: ISectionModel): boolean {
        return this.AssessmentSectionDetailHeaderPermission.hasEditPermission(itemPermissionsKey, assessment, currentSection);
    }
}
