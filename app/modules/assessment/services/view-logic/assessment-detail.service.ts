// Angular
import {
    Injectable,
    Inject,
} from "@angular/core";

// External
import { some } from "lodash";
import { Observable } from "rxjs";
import {
    startWith,
    map,
    filter,
} from "rxjs/operators";

// Enum
import { AssessmentStatus } from "enums/assessment/assessment-status.enum";

// Redux
import { getCurrentUser } from "oneRedux/reducers/user.reducer";
import { getAssessmentDetailModel } from "modules/assessment/reducers/assessment-detail.reducer";
import { StoreToken } from "tokens/redux-store.token";
import observableFromStore from "oneRedux/utils/observable-from-store";

// Interfaces
import { IStore } from "interfaces/redux.interface";
import { IUser } from "interfaces/user.interface";
import {
    IAssessmentApprover,
    IAssessmentModel,
} from "interfaces/assessment/assessment-model.interface";
import { INameId } from "interfaces/generic.interface";

// Services
import { Permissions } from "modules/shared/services/helper/permissions.service";

@Injectable()
export class AssessmentDetailService {
    constructor(
        @Inject(StoreToken) private readonly store: IStore,
        private permission: Permissions,
    ) { }

    fetchAssessmentReadOnlyState(): Observable<boolean> {
        const currentUser: IUser = getCurrentUser(this.store.getState());
        return observableFromStore(this.store)
            .pipe(
                startWith(this.store.getState()),
                map((state) => getAssessmentDetailModel(state)),
                filter((assessmentModel: IAssessmentModel) => Boolean(assessmentModel)),
                map((assessmentModel: IAssessmentModel): boolean => {
                    return this.isReadOnly(assessmentModel, currentUser.Id);
                }),
        );
    }

    private isReadOnly(assessmentModel: IAssessmentModel, currentUserId: string): boolean {
        let isApprover = false;
        let isRespondent = false;
        isApprover = some(assessmentModel.approvers, (approver: IAssessmentApprover) => approver.id === currentUserId);
        isRespondent = some(assessmentModel.respondents, (respondent: INameId<string>) => respondent.id === currentUserId);
        if ((assessmentModel.status === AssessmentStatus.Under_Review
            || assessmentModel.status === AssessmentStatus.Completed)
            && this.readOnlyConstraint(isApprover, assessmentModel)
        )
            return true;
        else if (assessmentModel.status !== AssessmentStatus.Completed
            && assessmentModel.status !== AssessmentStatus.Under_Review
            && !isRespondent
            && this.readOnlyConstraint(isApprover, assessmentModel)
        )
            return true;
        return false;
    }

    private readOnlyConstraint(isApprover: boolean, assessmentModel: IAssessmentModel): boolean {
        return !this.permission.canShow("AssessmentEditResponse")
            && !isApprover
            && !this.permission.canShow("AssessmentViewAllOrgAssessments")
            && assessmentModel.viewer;
    }
}
