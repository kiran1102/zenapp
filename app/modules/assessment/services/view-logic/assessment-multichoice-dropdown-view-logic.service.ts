// Angular
import {
    Injectable,
    Inject,
} from "@angular/core";

// Tokens
import { StoreToken } from "tokens/redux-store.token";

// 3rd Party
import { filter, map, includes, find } from "lodash";

// Redux
import {
    getQuestionResponseIds,
    getInventoryOptionModels,
    getQuestionResponseModels,
    getAssessmentDetailStatus,
    getInventoryOptionsArray,
    getSubQuestionResponseModels,
    getSubQuestionResponseIds,
} from "modules/assessment/reducers/assessment-detail.reducer";

// Interfaces
import { IStore } from "interfaces/redux.interface";
import {
    IQuestionModelOption,
    IQuestionModel,
} from "interfaces/assessment/question-model.interface";
import { IQuestionResponseV2 } from "interfaces/assessment/question-response-v2.interface";

// enums/constants
import { QuestionResponseType, QuestionTypes } from "constants/question-types.constant";
import { AssessmentStatus } from "enums/assessment/assessment-status.enum";

export interface IMultichoiceDropdownSelection {
    previousValue: IQuestionModelOption | IQuestionModelOption[];
    currentValue: IQuestionModelOption | IQuestionModelOption[];
    change: IQuestionModelOption;
}

export interface IMutlichoiceDropdownResponses {
    selectedOption: IQuestionModelOption;
    selectedOptions: IQuestionModelOption[];
    invalidOptions: IQuestionModelOption[];
}

@Injectable()
export class AssessmentMultichoiceDropdownViewLogicService {

    constructor(
        @Inject(StoreToken) public store: IStore,
    ) {}

    canAllowOther(question: IQuestionModel): boolean {
        if (question && question.attributes && question.attributes.allowOther) {
            const assessmentStatus: AssessmentStatus = getAssessmentDetailStatus(this.store.getState());
            if (question.questionType === QuestionTypes.Inventory.name) {
                return assessmentStatus !== AssessmentStatus.Under_Review;
            }
            return true;
        }
        return false;
    }

    canAllowHint(question: IQuestionModel): boolean {
        return question && question.attributes && question.attributes.optionHintEnabled;
    }

    formatResponse(option: IQuestionModelOption): IQuestionResponseV2 {
        return {
            response: option.option,
            responseId: option.id,
            type: option.optionType,
        };
    }

    isResponseBeingAdded(previous: IQuestionModelOption[], current: IQuestionModelOption[]): boolean {
        return !previous || previous.length < current.length;
    }

    filterDropdownOptions(options: IQuestionModelOption[] = [], responses: IQuestionModelOption[] = []): IQuestionModelOption[] {
        const validDropdownOptions = options.reduce((validOptions: IQuestionModelOption[], option: IQuestionModelOption) => {
            if (this.isValidDropdownOption(option)) {
                validOptions.push(this.mapQuestionModelOption(option));
            }
            return validOptions;
        }, []);

        if (!responses.length) {
            return validDropdownOptions;
        } else {
            const responseIds = map(responses, (response: IQuestionModelOption) => response.id);
            return filter(validDropdownOptions, (option: IQuestionModelOption) => !includes(responseIds, option.id));
        }
    }

    filterDropdownOption(options: IQuestionModelOption[], response: IQuestionModelOption = null): IQuestionModelOption[] {
        const validDropdownOptions = options.reduce((validOptions: IQuestionModelOption[], option: IQuestionModelOption) => {
            if (this.isValidDropdownOption(option)) {
                validOptions.push(this.mapQuestionModelOption(option));
            }
            return validOptions;
        }, []);

        if (!response) {
            return validDropdownOptions;
        } else {
            return filter(validDropdownOptions, (option: IQuestionModelOption) => option.id !== response.id);
        }
    }

    filterOptionsByInputValue(inputVal: string, labelKey: string, options: IQuestionModelOption[]): IQuestionModelOption[] {
        const regex = new RegExp(inputVal, "gi") || null;
        return options.filter((option: IQuestionModelOption): boolean => {
            return regex ? option[labelKey].match(regex) : false;
        });
    }

    checkForDuplicateOptions(inputVal: string, options: IQuestionModelOption[] | IQuestionResponseV2[], labelKey: string): boolean {
        let duplicateOptionExists = false;
        const modelLength: number = options.length;

        for (let i = 0; i < modelLength; i++) {
            if (!options[i].hasOwnProperty(labelKey)) {
                throw new Error("The labelKey provided does not exist on model.");
            }
            if (inputVal && options[i] && options[i][labelKey] && options[i][labelKey].toLowerCase() === inputVal.toLowerCase()) {
                duplicateOptionExists = true;
                break;
            }
        }

        return duplicateOptionExists;
    }

    checkForDuplicateInventoryOptions(inputVal: string, inventoryType: string, labelKey: string): boolean {
        const inventoryOptions = getInventoryOptionsArray(inventoryType)(this.store.getState());
        const duplicateOption = find(inventoryOptions, (opt) => opt[labelKey] === inputVal);
        return !!duplicateOption;
    }

    formatMultiSelectResponses(questionId: string, subQuestionId: string = null): IMutlichoiceDropdownResponses {
        const { responseIds, responseModels } = this.getResponses(questionId, subQuestionId);
        const selectedOptions: IQuestionModelOption[] = [];
        const invalidOptions: IQuestionModelOption[] = [];

        for (const id of responseIds) {
            const response: IQuestionResponseV2 = responseModels[id];
            const isValidDropdownResponse: boolean = this.isValidDropdownResponse(response);

            if (isValidDropdownResponse && response.valid === false) {
                selectedOptions.push(this.formatOption(response, false));
                invalidOptions.push(this.formatOption(response, false));
            } else if (isValidDropdownResponse) {
                selectedOptions.push(this.formatOption(response));
            }
        }

        return { selectedOption: null, selectedOptions, invalidOptions };
    }

    formatSingleSelectResponse(questionId: string, subQuestionId: string = null): IMutlichoiceDropdownResponses {
        const { responseIds, responseModels } = this.getResponses(questionId, subQuestionId);
        let selectedOption: IQuestionModelOption = null;
        const invalidOptions: IQuestionModelOption[] = [];

        for (const id of responseIds) {
            const response: IQuestionResponseV2 = responseModels[id];
            const isValidDropdownResponse: boolean = this.isValidDropdownResponse(response);

            if (isValidDropdownResponse && response.valid === false) {
                selectedOption = this.formatOption(response, false);
                invalidOptions.push(this.formatOption(response, false));
            } else if (isValidDropdownResponse) {
                selectedOption = this.formatOption(response);
            }
        }

        return { selectedOption, selectedOptions: null, invalidOptions };
    }

    formatMultiSelectInventoryResponses(questionId: string, inventoryType: string): IMutlichoiceDropdownResponses {
        const { responseIds, responseModels } = this.getResponses(questionId);
        const inventoryOptionModels = getInventoryOptionModels(inventoryType)(this.store.getState());
        const selectedOptions: IQuestionModelOption[] = [];
        const invalidOptions: IQuestionModelOption[] = [];

        for (const id of responseIds) {
            const response = responseModels[id];
            const inventoryHasResponse: boolean = !!inventoryOptionModels[id];
            const isValidDropdownResponse: boolean = this.isValidDropdownResponse(response);

            // response is consistent with current Inventory Schema
            if (inventoryHasResponse) {
                selectedOptions.push(this.formatInventoryOption(response));
            } else if (isValidDropdownResponse && response.valid === false) {
                selectedOptions.push(this.formatInventoryOption(response, false));
                invalidOptions.push(this.formatInventoryOption(response));
            } else if (isValidDropdownResponse) {
                selectedOptions.push(this.formatInventoryOption(response));
            }
        }

        return { selectedOption: null, selectedOptions, invalidOptions };
    }

    formatSingleSelectInventoryResponse(questionId: string, inventoryType: string): IMutlichoiceDropdownResponses {
        const { responseIds, responseModels } = this.getResponses(questionId);
        const inventoryOptionModels = getInventoryOptionModels(inventoryType)(this.store.getState());
        const responseId = responseIds[0];
        const response = responseModels[responseId];
        const inventoryHasResponse: boolean = !!inventoryOptionModels[responseId];
        const isValidDropdownResponse: boolean = this.isValidDropdownResponse(response);
        let selectedOption: IQuestionModelOption = null;
        const invalidOptions: IQuestionModelOption[] = [];

        // response is consistent with current Inventory Schema
        if (inventoryHasResponse) {
            selectedOption = this.formatInventoryOption(response);
        } else if (isValidDropdownResponse && response.valid === false) {
            selectedOption = this.formatInventoryOption(response, false);
            invalidOptions.push(this.formatInventoryOption(response));
        } else if (isValidDropdownResponse) {
            selectedOption = this.formatInventoryOption(response);
        }

        return { selectedOption, selectedOptions: null, invalidOptions };
    }

    private getResponses(questionId: string, subQuestionId: string = null): { responseIds: string[], responseModels: Partial<Map<string, IQuestionResponseV2>> } {
        const responseIds = subQuestionId
            ? getSubQuestionResponseIds(questionId, subQuestionId)(this.store.getState())
            : getQuestionResponseIds(questionId)(this.store.getState());

        const responseModels = subQuestionId
            ? getSubQuestionResponseModels(questionId, subQuestionId)(this.store.getState())
            : getQuestionResponseModels(questionId)(this.store.getState());

        return { responseIds, responseModels };
    }

    private isValidDropdownOption(option: IQuestionModelOption): boolean {
        return option
            && option.optionType !== QuestionResponseType.NotApplicable
            && option.optionType !== QuestionResponseType.NotSure
            && option.option !== "Other";
    }

    private formatOption(response: IQuestionResponseV2, valid = true): IQuestionModelOption {
        return {
            id: response.responseId,
            option: response.response,
            optionType: response.type,
            valid,
        };
    }

    private formatInventoryOption(response: IQuestionResponseV2, valid = true): IQuestionModelOption {
        return {
            id: response.responseId,
            option: response.response,
            optionDisplay: response.response,
            optionType: response.type,
            valid,
        };
    }

    private isValidDropdownResponse(response: IQuestionResponseV2): boolean {
        return response
            && response.response
            && (response.type === QuestionResponseType.Default || response.type === QuestionResponseType.Others);
    }

    private mapQuestionModelOption(option: IQuestionModelOption): IQuestionModelOption {
        if (option.attributes && option.attributes.hint) {
            option.hint = option.attributes.hint;
        }
        return option;
    }
}
