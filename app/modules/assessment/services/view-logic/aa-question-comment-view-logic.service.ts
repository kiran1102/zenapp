// Core
import { Injectable, Inject } from "@angular/core";

// Redux
import { StoreToken } from "tokens/redux-store.token";
import { getAssessmentDetailModel, isReadinessTemplate } from "modules/assessment/reducers/assessment-detail.reducer";
import { getCurrentUser } from "oneRedux/reducers/user.reducer";

// Interface
import { IStore } from "interfaces/redux.interface";

// Services
import { Permissions } from "modules/shared/services/helper/permissions.service";

// 3rd party
import { some } from "lodash";

@Injectable()
export class AAQuestionCommentViewLogicService {
    constructor(
        @Inject(StoreToken) private store: IStore,
        private permissions: Permissions,
    ) {}

    checkForRespondent(): boolean {
        const state = this.store.getState();
        const currentUser = getCurrentUser(state).Id;
        if (isReadinessTemplate()(state)) {
            return false;
        } else {
            const respondents = getAssessmentDetailModel(state).respondents;
            return some(respondents, (respondentId: string) => respondentId === currentUser);
        }
    }

    permissionCheckForComment(): boolean {
        return this.checkForRespondent() || this.permissions.canShow("CreateQuestionComment") || this.permissions.canShow("ViewQuestionComment");
    }
}
