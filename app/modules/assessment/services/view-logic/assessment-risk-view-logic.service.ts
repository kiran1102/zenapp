// Angular
import { Injectable } from "@angular/core";

// Interfaces
import { IAssessmentModel } from "interfaces/assessment/assessment-model.interface";
import { IRiskDetails } from "interfaces/risk.interface";
import { IStringMap } from "interfaces/generic.interface";
import { IUser } from "interfaces/user.interface";

// Services
import { AssessmentRiskV2PermissionService } from "modules/assessment/services/permission/assessment-risk-v2-permission.service";

@Injectable()
export class AssessmentRiskViewLogicService {

    constructor(
        private readonly assessmentRiskV2Permission: AssessmentRiskV2PermissionService) {
    }

    public calculateRiskStatePermissions(currentUser: IUser, assessmentModel: IAssessmentModel, riskModel: IRiskDetails): IStringMap<boolean> {
        return {
            canViewRisk: this.assessmentRiskV2Permission.hasRiskPermission("canViewRisk", currentUser, assessmentModel, riskModel),
            canEditRisk: this.assessmentRiskV2Permission.hasRiskPermission("canEditRisk", currentUser, assessmentModel, riskModel),
            canEditRiskOwner: this.assessmentRiskV2Permission.hasRiskPermission("canEditRiskOwner", currentUser, assessmentModel, riskModel),
            canAddRecommendation: this.assessmentRiskV2Permission.hasRiskPermission("canAddRecommendation", currentUser, assessmentModel, riskModel),
            canEditRecommendation: this.assessmentRiskV2Permission.hasRiskPermission("canEditRecommendation", currentUser, assessmentModel, riskModel),
        };
    }

    public hasCreateRiskPermission(currentUser: IUser, assessmentModel: IAssessmentModel, riskModel: IRiskDetails): boolean {
        return this.assessmentRiskV2Permission.hasRiskPermission("canCreateRisk", currentUser, assessmentModel, riskModel);
    }
}
