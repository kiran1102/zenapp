// Angular
import {
    Injectable,
    Inject,
} from "@angular/core";

// reducers
import { getCurrentUser } from "oneRedux/reducers/user.reducer";
import { StoreToken } from "tokens/redux-store.token";

// services
import { AaSectionQuestionPermissionService } from "modules/assessment/services/permission//aa-section-question-permission.service";

// interfaces
import { IStore } from "interfaces/redux.interface";
import { IAssessmentModel } from "interfaces/assessment/assessment-model.interface";
import { IQuestionModel } from "interfaces/assessment/question-model.interface";
import { IAssessmentQuestionViewState } from "interfaces/assessment/assessment-question-viewstate.interface";

@Injectable()
export class AssessmentSectionQuestionViewLogicService {
    constructor(
        @Inject(StoreToken) private readonly store: IStore,
        private readonly aaSectionQuestionPermission: AaSectionQuestionPermissionService,
    ) { }

    calculateNewViewState(assessmentModel: IAssessmentModel, questionModel: IQuestionModel): IAssessmentQuestionViewState {
        const currentUserId = getCurrentUser(this.store.getState()).Id;
        return {
            canViewQuestion: this.hasQuestionViewPermission(currentUserId, assessmentModel, questionModel),
            isQuestionDisabled: !this.hasQuestionEditPermission(currentUserId, assessmentModel, questionModel) || !questionModel.valid,
        };
    }

    private hasQuestionEditPermission(currentUserId: string, assessmentModel: IAssessmentModel, questionModel: IQuestionModel): boolean {
        return this.aaSectionQuestionPermission.hasQuestionEditPermission("canEditQuestion", currentUserId, assessmentModel, questionModel);
    }

    private hasQuestionViewPermission(currentUserId: string, assessmentModel: IAssessmentModel, questionModel: IQuestionModel): boolean {
        return this.aaSectionQuestionPermission.hasQuestionEditPermission("canViewQuestion", currentUserId, assessmentModel, questionModel);
    }
}
