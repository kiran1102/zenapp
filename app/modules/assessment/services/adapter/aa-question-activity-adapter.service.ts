// Angular
import {
    Injectable,
} from "@angular/core";

// Interface
import {
    IQuestionActivityStreamActivity,
} from "interfaces/assessment/assessment-question-activity.interface";

// Constants
import { ActivityColor } from "constants/activity-stream.constant";

@Injectable()
export class AAQuestionActivityAdapterService {

    getIconBackgroundColor(activity: IQuestionActivityStreamActivity): string {
        const { oldValue, newValue } = activity;
        let backgroundColor: string;
        if (!oldValue && newValue) {
            backgroundColor = ActivityColor.Add;
        } else if (oldValue && newValue) {
            backgroundColor = ActivityColor.Edit;
        } else if (oldValue && !newValue) {
            backgroundColor = ActivityColor.Delete;
        }
        return backgroundColor;
    }

    getIcon(activity: IQuestionActivityStreamActivity): string {
        const { oldValue, newValue } = activity;
        let icon: string;
        if (!oldValue && newValue) {
            icon = "ot-plus";
        } else if (oldValue && newValue) {
            icon = "ot-pencil-square-o";
        } else if (oldValue && !newValue) {
            icon = "ot-trash";
        }
        return icon;
    }
}
