// Core
import { Injectable } from "@angular/core";

// Constants & Enums
import { InventoryTableIds } from "enums/inventory.enum";
import {
    InventoryDetailRoutes,
    InventoryListRoutes,
} from "constants/inventory.constant";
import {
    InventoryTypeTranslations,
    InventorySchemaIds,
} from "constants/inventory-config.constant";

export interface IAABulkLaunchParams {
    label: string;
    path: string;
    params: { [key: string]: any };
}

const _breadcrumb = {
    [InventoryTableIds.Incidents]: {
        detailsLabel: "IncidentDetails",
        detailsPath: "zen.app.pia.module.incident.views.incident-detail",
        detailsParams: { id: null },
        listLabel: "IncidentRecords",
        listPath: "zen.app.pia.module.incident.views.incident-register",
        listParams: {},
    },
    [InventoryTableIds.Assets]: {
        detailsLabel: InventoryTypeTranslations[InventoryTableIds.Assets],
        detailsPath: InventoryDetailRoutes[InventoryTableIds.Assets],
        detailsParams: { inventoryId: InventoryTableIds.Assets, recordId: null, tabId: null },
        listLabel: InventoryTypeTranslations[InventoryTableIds.Assets],
        listPath: InventoryListRoutes[InventoryTableIds.Assets],
        listParams: { Id: InventorySchemaIds.Assets, page: null, size: null },
    },
    [InventoryTableIds.Processes]: {
        detailsLabel: InventoryTypeTranslations[InventoryTableIds.Processes],
        detailsPath: InventoryDetailRoutes[InventoryTableIds.Processes],
        detailsParams: { inventoryId: InventoryTableIds.Processes, recordId: null, tabId: null },
        listLabel: InventoryTypeTranslations[InventoryTableIds.Processes],
        listPath: InventoryListRoutes[InventoryTableIds.Processes],
        listParams: { Id: InventorySchemaIds.Processes, page: null, size: null },
    },
    [InventoryTableIds.Vendors]: {
        detailsLabel: InventoryTypeTranslations[InventoryTableIds.Vendors],
        detailsPath: InventoryDetailRoutes[InventoryTableIds.Vendors],
        detailsParams: { inventoryId: InventoryTableIds.Vendors, recordId: null, tabId: null },
        listLabel: InventoryTypeTranslations[InventoryTableIds.Vendors],
        listPath: InventoryListRoutes[InventoryTableIds.Vendors],
        listParams: { Id: InventoryTableIds.Vendors, page: null, size: null },
    },
    [InventoryTableIds.Entities]: {
        detailsLabel: InventoryTypeTranslations[InventoryTableIds.Entities],
        detailsPath: InventoryDetailRoutes[InventoryTableIds.Entities],
        detailsParams: { inventoryId: InventoryTableIds.Entities, recordId: null, tabId: null },
        listLabel: InventoryTypeTranslations[InventoryTableIds.Entities],
        listPath: InventoryListRoutes[InventoryTableIds.Entities],
        listParams: { Id: InventorySchemaIds.Entities, page: null, size: null },
    },
};

@Injectable()
export class AABulkLaunchAdapterService {

    public getBreadcrumb(params: { [key: string]: any }): IAABulkLaunchParams {
        // Incident Details
        const inventoryId = params.inventoryTypeId;
        if (params.detailsId) {
            return {
                label: _breadcrumb[inventoryId].detailsLabel,
                path: _breadcrumb[inventoryId].detailsPath,
                params: {
                    ..._breadcrumb[inventoryId].detailsParams,
                    id: params.detailsId,
                },
            };
        // Inventory Details
        } else if (params.recordId && params.tabId) {
            return {
                label: _breadcrumb[inventoryId].detailsLabel,
                path: _breadcrumb[inventoryId].detailsPath,
                params: {
                    ..._breadcrumb[inventoryId].detailsParams,
                    recordId: params.recordId,
                    tabId: params.tabId,
                },
            };
        // Incident & Inventory List pages
        } else {
            return {
                label: _breadcrumb[inventoryId].listLabel,
                path: _breadcrumb[inventoryId].listPath,
                params: _breadcrumb[inventoryId].listParams,
            };
        }
    }

}
