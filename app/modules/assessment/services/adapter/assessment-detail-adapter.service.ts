// External Libraries
import {
    forEach,
    assign,
    isArray,
    map,
} from "lodash";

// Interfaces
import { IAssessmentModel } from "interfaces/assessment/assessment-model.interface";
import { IFlattenedState } from "interfaces/redux.interface";
import {
    IQuestionModel,
    IQuestionModelOption,
} from "interfaces/assessment/question-model.interface";
import { ISectionHeaderInfo } from "interfaces/assessment/section-header-info.interface";
import { IRiskReducerState } from "interfaces/assessment/risk-reducer-state.interface";
import { ISectionHeaderReducerState } from "interfaces/assessment/section-header-reducer-state.interface";
import {
    IAssetsInventoryOption,
    IProcessingActivityInventoryOption,
    IVendorInventoryOption,
    IDataElementsInventoryOption,
    InventoryOptionsResponse,
    IEntitiesInventoryOption,
} from "interfaces/assessment/assessment-api.interface";
import { IRiskStatistics, IRiskMetadata } from "interfaces/risk.interface";

// Constants
import { QuestionResponseType } from "enums/assessment/assessment-question.enum";
import { InventorySchemaIds } from "constants/inventory-config.constant";

export class AssessmentDetailAdapterService {

    convertAssessmentStructure(assessmentModel: IAssessmentModel): IAssessmentModel {
        return this.convertArraysToMap(assessmentModel);
    }

    // Inventory Question Adapter
    mapInventoryOptions(type: string, data: InventoryOptionsResponse[]) {
        switch (type) {
            case InventorySchemaIds.Assets:
                return map(data, this.mapAssetInventoryOptions);
            case InventorySchemaIds.Processes:
                return map(data, this.mapProcessInventoryOptions);
            case InventorySchemaIds.Vendors:
                return map(data, this.mapVendorInventoryOptions);
            case InventorySchemaIds.Elements:
                return map(data, this.mapDataElementInventoryOptions);
            case InventorySchemaIds.Entities:
                return map(data, this.mapEntityInventoryOptions);
        }
    }

    // returns a new assessment model with questions and responses data structure converted to redux standard structure
    private convertArraysToMap(assessmentModel: IAssessmentModel): IAssessmentModel {
        return assign(
            assessmentModel,
            {
                ...assessmentModel,
                sectionHeaders: this.flattenSectionHeaders(assessmentModel.sectionHeaders as any as ISectionHeaderInfo[]),
                section: assessmentModel.section ? {
                    ...assessmentModel.section,
                    risks: this.flattenRisks(assessmentModel.questions),
                } : null,
            },
        );
    }

    private flattenSectionHeaders(sectionHeaders: ISectionHeaderInfo[]): ISectionHeaderReducerState {
        if (isArray(sectionHeaders) && sectionHeaders.length > 0) {
            const flattenedSectionHeaders = {
                allIds: [],
                byIds: {} as Map<string, ISectionHeaderInfo>,
                riskStatisticsBySectionIds: {} as Map<string, IRiskStatistics>,
            };
            sectionHeaders.forEach((sectionHeader: ISectionHeaderInfo) => {
                flattenedSectionHeaders.allIds.push(sectionHeader.sectionId);
                flattenedSectionHeaders.byIds[sectionHeader.sectionId] = sectionHeader;
                flattenedSectionHeaders.riskStatisticsBySectionIds[sectionHeader.sectionId] = sectionHeader.riskStatistics;
            });
            return flattenedSectionHeaders;
        }
        return {
            allIds: [],
            byIds: {} as Map<string, ISectionHeaderInfo>,
            riskStatisticsBySectionIds: {} as Map<string, IRiskStatistics>,
        };
    }

    private flattenRisks(questions: IFlattenedState<IQuestionModel>): {} | IRiskReducerState {
        const riskResult = {
            allIds: [],
            byId: {},
            byQuestionId: {},
        };

        if (!questions) {
            return riskResult;
        }

        const questionAllIds = questions.allIds;
        const questionsById = questions.byId;

        forEach(questionAllIds, (id: string) => {
            const question = questionsById[id];
            if (question.risks) {
                riskResult.byQuestionId[id] = [];
                question.risks.forEach((risk: IRiskMetadata) => {
                    riskResult.allIds.push(risk.riskId);
                    riskResult.byId[risk.riskId] = risk;
                    riskResult.byQuestionId[risk.questionId].push(risk);
                });
            }
        });

        return riskResult;
    }

    private mapAssetInventoryOptions(inventory: IAssetsInventoryOption): IQuestionModelOption {
        const { id, name, organization, location } = inventory;
        return {
            id,
            option: name,
            optionDisplay: `${name} | ${organization.value} | ${location.value}`,
            optionType: QuestionResponseType.Default,
        };
    }

    private mapEntityInventoryOptions(inventory: IEntitiesInventoryOption): IQuestionModelOption {
        const { id, name, organization, primaryOperatingLocation } = inventory;
        return {
            id,
            option: name,
            optionDisplay: `${name} | ${organization.value} | ${primaryOperatingLocation.value}`,
            optionType: QuestionResponseType.Default,
        };
    }

    private mapProcessInventoryOptions(inventory: IProcessingActivityInventoryOption): IQuestionModelOption {
        const { id, name, organization } = inventory;
        return {
            id,
            option: name,
            optionDisplay: `${name} | ${organization.value}`,
            optionType: QuestionResponseType.Default,
        };
    }

    private mapVendorInventoryOptions(inventory: IVendorInventoryOption): IQuestionModelOption {
        const { id, name, organization, type } = inventory;
        return {
            id,
            option: name,
            optionDisplay: `${name} | ${organization.value} | ${type.value}`,
            optionType: QuestionResponseType.Default,
        };
    }

    private mapDataElementInventoryOptions(inventory: IDataElementsInventoryOption): IQuestionModelOption {
        const { id, name } = inventory;
        return {
            id,
            option: name,
            optionDisplay: name,
            optionType: QuestionResponseType.Default,
        };
    }
}
