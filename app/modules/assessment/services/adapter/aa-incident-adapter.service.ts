// Core
import { Injectable } from "@angular/core";

// Interfaces
import {
    IQuestionModel,
    IQuestionModelSubQuestion,
} from "interfaces/assessment/question-model.interface";
import { IQuestionModelResponse } from "interfaces/assessment/question-response-v2.interface";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

// Enums
import { AATranslationKeys } from "modules/assessment/enums/aa-translation-keys.enums";
import { AAParentQuestionType } from "modules/assessment/enums/aa-question.enums";

@Injectable()
export class AAIncidentAdapterService {

    private currentIncident: string;
    private existingIncidentMessage = AATranslationKeys.asssessmentExistingIncidentNotification as string;
    private newIncidentMessage = AATranslationKeys.asssessmentNewIncidentNotification as string;
    private defaultIncidentBannerText = "New Incident";

    constructor(
        private translatePipe: TranslatePipe,
    ) {}

    getCurrentIncident(): string {
        return this.currentIncident;
    }

    isIncidentAttribute(question: IQuestionModel): boolean {
        return this.getSourceQuestionType(this.getSubQuestion(question)) === AAParentQuestionType.Incident;
    }

    setIncidentBannerMessage(question: IQuestionModel): string {
        return this.setBannerMessage(this.getResponse(question));
    }

    setIncidentAttributeMessage(question: IQuestionModel): string {
        return this.setAttributeMessage(this.getSubQuestion(question));
    }

    private setCurrentIncident(name: string) {
        this.currentIncident = name;
    }

    private getSubQuestion(question: IQuestionModel): IQuestionModelSubQuestion {
        return question.subQuestions.byId[question.subQuestions.allIds[0]];
    }

    private getSourceQuestionType(subQuestion: IQuestionModelSubQuestion): AAParentQuestionType {
        return subQuestion.parentQuestionType;
    }

    private getResponse(question: IQuestionModel): IQuestionModelResponse {
        return question.responses.byId[question.responses.allIds[0]];
    }

    private setBannerMessage(response: IQuestionModelResponse = null): string {
        if (response && response.response !== this.defaultIncidentBannerText) {
            this.setCurrentIncident(response.response);
            return this.translatePipe.transform(this.existingIncidentMessage, { IncidentName: response.response });
        }
        return this.translatePipe.transform(this.newIncidentMessage);
    }

    private setAttributeMessage(subQuestion: IQuestionModelSubQuestion = null): string {
        if (subQuestion && subQuestion.displayLabel !== this.translatePipe.transform("NewIncident") && subQuestion.displayLabel !== this.defaultIncidentBannerText) {
            return `${this.translatePipe.transform("Incident")}: ${this.getCurrentIncident()}`;
        }
        return this.translatePipe.transform("NewIncident");
    }
}
