// Core
import { Injectable } from "@angular/core";

// Services
import { AAActivityStreamApiService } from "modules/assessment/services/api/aa-activity-stream-api.service";

// Interfaces
import { IProtocolResponse } from "interfaces/protocol.interface";
import { IAssessmentActivityParams } from "interfaces/assessment/aa-assessment-activity.interface";
import { IPageable } from "interfaces/pagination.interface";

@Injectable()
export class AAActivityActionService {
    constructor(private AAActivityStreamApi: AAActivityStreamApiService) {}

    getRecordActivity = (recordId: string, defaultConfig: IAssessmentActivityParams): ng.IPromise<IPageable> => {
        return this.AAActivityStreamApi.getActivityData(recordId, defaultConfig).then((response: IProtocolResponse<IPageable>): IPageable => {
            if (response.result) {
                return response.data;
            }
        });
    }
}
