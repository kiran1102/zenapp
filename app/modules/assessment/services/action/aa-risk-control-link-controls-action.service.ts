// Angular
import { Injectable } from "@angular/core";

// Vitreus
import {
    OtModalService,
    ModalSize,
} from "@onetrust/vitreus";

// RxJs
import {
    Subject,
    Observable,
} from "rxjs";
import {
    switchMap,
    map,
} from "rxjs/operators";

// Services
import { AARiskControlApiService } from "../api/aa-risk-control-api.service";

// Interfaces
import { ILinkControlsModalData } from "modules/link-controls/link-controls-api/interfaces/link-controls.interface";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

// Component
import { LinkControlsModalComponent } from "modules/link-controls/link-controls-modal/components/link-controls-modal/link-controls-modal.component";

@Injectable()
export class AaRiskControlActionService {

    constructor(
        private otModalService: OtModalService,
        private aaRiskControlApiService: AARiskControlApiService,
        private translatePipe: TranslatePipe,
    ) { }

    openAddRiskControlsModal(riskId: string): Observable<boolean> {
        const linkControlSource = new Subject<string[]>();
        const modalData: ILinkControlsModalData = {
            linkControlSource,
            linkControl$: linkControlSource.pipe(
                switchMap((controlIds) => {
                    return this.aaRiskControlApiService.addRiskControl(riskId, controlIds).pipe( map((response) => response.result));
                }),
            ),
        };

        return this.otModalService.create(
            LinkControlsModalComponent,
            {
                isComponent: true,
                size: ModalSize.MEDIUM,
            },
            { ...modalData },
        );
    }
}
