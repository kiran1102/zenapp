// External libraries.
import {
    uniqueId,
    find,
} from "lodash";

// Core
import {
    Injectable,
    Inject,
} from "@angular/core";

// Enums / Constants
import {
    RiskTypes,
    RiskSourceTypes,
    RiskReferenceTypes,
} from "enums/risk.enum";
import {
    RiskV2States,
} from "enums/riskV2.enum";
import { OrgUserTraversal } from "enums/org-user-dropdown.enum";
import { AssessmentPermissions } from "constants/assessment.constants";

// Redux
import {
    AssessmentDetailAction,
    getAssessmentDetailModel,
    getAssessmentDetailSelectedSectionId,
} from "modules/assessment/reducers/assessment-detail.reducer";
import {
    IStore,
    IStoreState,
    IStringToBooleanMap,
} from "interfaces/redux.interface";
import {
    RiskActions,
} from "oneRedux/reducers/risk.reducer";
import {
    getUserById,
    getFilteredUsers,
    isNotProjectViewer,
} from "oneRedux/reducers/user.reducer";
import { getCurrentRelatedOrgIdsMap } from "oneRedux/reducers/org.reducer";
import { multiForkJoin, areAllEqualTo } from "oneRedux/utils/utils";
import { StoreToken } from "tokens/redux-store.token";

// Types
import { RiskV2UpdateActions } from "modules/assessment/types/risk-v2.types";

// Services
import { ModalService } from "sharedServices/modal.service";
import { AssessmentRiskApiService } from "modules/assessment/services/api/assessment-risk-api.service";
import { RiskAPIService } from "oneServices/api/risk-api.service";
import { AssessmentDetailApiService } from "modules/assessment/services/api/assessment-detail-api.service";
import { OrgGroupApiService } from "oneServices/api/org-group-api.service";

// Interfaces
import { IProtocolResponse } from "interfaces/protocol.interface";
import {
    IRiskMetadata,
    IRiskCreateRequestModel,
    IRiskReference,
    IRiskStatistics,
    IRiskDetails,
    IRiskUpdateRequest,
    IRiskCreateRequest,
} from "interfaces/risk.interface";
import { IAssessmentModel } from "interfaces/assessment/assessment-model.interface";
import { IOrgUserAdapted } from "interfaces/org-user.interface";
import { IUser } from "interfaces/user.interface";

@Injectable()
export class AssessmentRiskActionService {

    constructor(
        @Inject(StoreToken) private store: IStore,
        private modalService: ModalService,
        private AssessmentRiskApi: AssessmentRiskApiService,
        private readonly RiskAPI: RiskAPIService,
        private readonly AssessmentDetailApi: AssessmentDetailApiService,
        private OrgGroups: OrgGroupApiService,
    ) {
    }

    public openRiskModal = (risk: IRiskMetadata, assessmentId: string, sectionId: string, questionId: string): void => {
        let riskId: string;
        const assessmentModel: IAssessmentModel = getAssessmentDetailModel(this.store.getState());
        riskId = this.addBlankRiskModel(assessmentId, sectionId, questionId);
        this.openModal(riskId, assessmentModel, this.updateRiskCallback);
    }

    public clearRecommendation = (riskId: string, assessmentId: string): ng.IPromise<IRiskMetadata> => {
        return this.AssessmentRiskApi.clearRecommendation(riskId, assessmentId)
            .then((response: IProtocolResponse<IRiskMetadata>): IRiskMetadata => {
                return response.data;
            });
    }

    public createRisk = (risk: IRiskCreateRequestModel, assessmentId: string): ng.IPromise<IRiskMetadata> => {
        return this.AssessmentRiskApi.createRisk(risk, assessmentId)
            .then((response: IProtocolResponse<IRiskMetadata>): IRiskMetadata => {
                return response.data;
            });
    }

    public updateRisk = (risk: IRiskUpdateRequest, riskId: string, assessmentId: string): ng.IPromise<IRiskMetadata> => {
        return this.AssessmentRiskApi.updateRisk(risk, riskId, assessmentId)
            .then((response: IProtocolResponse<IRiskMetadata>): IRiskMetadata => {
                this.store.dispatch({ type: RiskActions.RISK_RELOADED, riskModelToReload: response.data.riskResponse, identifier: "id" });
                return response.data;
            });
    }

    public performRiskAction = (riskId: string, action: RiskV2UpdateActions): ng.IPromise<IRiskDetails> => {
        return this.AssessmentRiskApi.performRiskAction(riskId, action)
            .then((response: IProtocolResponse<IRiskDetails>): IRiskDetails => {
                return response.data;
            });
    }

    public updateRiskMetadataInAssessment = (riskMetadata: IRiskMetadata): void => {
        this.store.dispatch({ type: AssessmentDetailAction.UPDATE_ASSESSMENT_RISK, riskToAdd: riskMetadata });
    }

    public updateRiskCallback = (riskMetadata: IRiskMetadata, risk: IRiskDetails, sectionId?: string): void => {
        const assessmentId: string = riskMetadata.riskResponse.source.id;
        if (!sectionId) {
            const sectionReference = find(risk.references, (reference: IRiskReference) => reference.typeId === RiskReferenceTypes.Section);
            sectionId = sectionReference ? sectionReference.id : getAssessmentDetailSelectedSectionId(this.store.getState());
        }
        this.updateRiskMetadataInAssessment(riskMetadata);
        this.updateRiskStatistics(assessmentId, sectionId);
    }

    public deleteRiskCallback(riskIds: string[], questionId: string, sectionId: string, assessmentId: string): void {
        this.store.dispatch({ type: AssessmentDetailAction.DELETE_ASSESSMENT_RISKS, riskIds, questionId });
        this.store.dispatch({ type: AssessmentDetailAction.REMOVE_INVALID_RISK_RESPONSE, questionId });
        this.updateRiskStatistics(assessmentId, sectionId);
    }

    public updateRiskStatistics(assessmentId: string, sectionId: string): void {
        this.AssessmentRiskApi
            .fetchRiskStatistics(assessmentId, sectionId)
            .then((riskStatistics: IProtocolResponse<IRiskStatistics>): void => {
                this.store.dispatch({ type: AssessmentDetailAction.UPDATE_RISK_STATISTICS, sectionId, riskStatisticsToUpdate: riskStatistics.data });
            });
    }

    public fetchRiskById = (riskId: string): ng.IPromise<IRiskDetails> => {
        return this.RiskAPI.fetchRiskV2ById(riskId)
            .then((response: IProtocolResponse<IRiskDetails>): IRiskDetails => {
                const risk: IRiskDetails = response.data;
                this.store.dispatch({ type: RiskActions.ADD_RISK, riskToAdd: risk, idKey: "id" });
                this.store.dispatch({ type: RiskActions.ADD_TEMP_RISK, tempRiskToAdd: risk, idKey: "id" });
                return response.data;
            });
    }

    public deleteTempRisk(riskId: string): void {
        if (!riskId) throw new Error("Risk Id cannot be undefined");
        this.store.dispatch({ type: RiskActions.DELETE_TEMP_RISK, tempRiskToDelete: riskId });
    }

    public updateTempRisk(riskIdTemp: string, keyValueToMergeTemp: any): void {
        this.store.dispatch({ type: RiskActions.UPDATE_TEMP_RISK, riskIdTemp, keyValueToMergeTemp: { ...keyValueToMergeTemp } });
    }

    public reloadRiskModel(riskId: string): ng.IPromise<any> {
        return this.RiskAPI.fetchRiskV2ById(riskId).then((res: IProtocolResponse<IRiskDetails>): IRiskDetails => {
            if (res.result) {
                const risk: IRiskDetails = res.data;
                this.store.dispatch({ type: RiskActions.RISK_RELOADED, riskModelToReload: risk, identifier: "id" });
                return res.data;
            }
            return res.data;
        });
    }

    public openRiskModalFromSummary = (riskModel: IRiskDetails, callback: () => void): void => {
        if (riskModel && riskModel.id) {
            const assessmentId: string = find(riskModel.references, (reference: IRiskReference): boolean => reference.typeId === RiskReferenceTypes.Assessment).id;
            const sectionId: string = find(riskModel.references, (reference: IRiskReference): boolean => reference.typeId === RiskReferenceTypes.Section).id;
            this.store.dispatch({ type: RiskActions.ADD_TEMP_RISK, tempRiskToAdd: riskModel, idKey: "id" });
            this.AssessmentDetailApi.fetchCurrentSection(assessmentId, sectionId)
                .then((assessmentModel: IAssessmentModel) => {
                    this.openModal(riskModel.id, assessmentModel, callback);
                });
        }
    }

    getRiskOwners(risk: IRiskDetails): ng.IPromise<IUser[]> {
        const storeState: IStoreState = this.store.getState();
        const assessmentModel = getAssessmentDetailModel(storeState);
        const respondentId: string = assessmentModel.respondents[0].id;

        return this.OrgGroups.getOrgUsersWithPermission(
            assessmentModel.orgGroup.id,
            OrgUserTraversal.Branch,
            false,
            [AssessmentPermissions.CanBeRiskOwner])
            .then((res: IProtocolResponse<IOrgUserAdapted[]>): IUser[] => {
                let userList: IUser[];
                if (!res.result) {
                    userList = this.findAndSetUsersRelatedToCurrentOrg(risk, respondentId);
                } else {
                    const noInvited: IUser[] = res.data
                        .map((user: IUser) => getUserById(user.Id)(storeState))
                        .filter((user: IUser) => user && user.RoleName !== "Invited");

                    const respondent: IUser = getUserById(respondentId)(storeState);
                    if (respondent && respondent.RoleName === "Invited") {
                        userList = [...noInvited, respondent];
                    } else {
                        userList = noInvited;
                    }
                }

                return userList;
            });
    }

    private addBlankRiskModel(assessmentId: string, sectionId: string, questionId: string): string {
        const tempId = `temp-risk-${uniqueId()}`;
        const blankRiskModel: IRiskCreateRequest = {
            level: "",
            conditionGroupUuid: null,
            deadline: null,
            description: "",
            id: tempId,
            levelId: null,
            mitigatedDate: null,
            mitigation: null,
            recommendation: "",
            reminderDays: null,
            impactLevel: null,
            riskOwnerId: null,
            probabilityLevel: null,
            previousState: RiskV2States.IDENTIFIED,
            state: RiskV2States.IDENTIFIED,
            typeId: RiskTypes.Question,
            sourceTypeId: RiskSourceTypes.PIA,
            requestedException: null,
            orgGroup: null,
            references: [
                { id: assessmentId, typeId: RiskReferenceTypes.Assessment, version: 1 },
                { id: sectionId, typeId: RiskReferenceTypes.Section, version: 1 },
                { id: questionId, typeId: RiskReferenceTypes.Question, version: 1 },
            ],
            categoryIds: null,
        };
        this.store.dispatch({ type: RiskActions.ADD_BLANK_RISK, tempRiskToAdd: blankRiskModel, idKey: "id" });
        return tempId;
    }

    private openModal = (riskId: string, assessmentModel: IAssessmentModel, callback?: (riskMetadata: IRiskMetadata, risk: IRiskDetails, sectionId?: string) => void): void => {
        this.modalService.setModalData({ riskId, assessmentModel, callback });
        this.modalService.openModal("downgradeAaRiskModalComponent");
    }

    private findAndSetUsersRelatedToCurrentOrg(riskModel: IRiskDetails, respondentId: string): IUser[] {
        const relatedOrgIds: IStringToBooleanMap = getCurrentRelatedOrgIdsMap(this.store.getState());
        const userExistsInOrgIdMap = (user: IUser): boolean => Boolean(relatedOrgIds[user.OrgGroupId]);
        const removeDisabledUser = (user: IUser): boolean => Boolean(user.IsActive);
        const removeInvitedUsersWhoAreNotRespondentOrRiskOwner = (invitedRespondentId: string) => (user: IUser): boolean => Boolean(user.RoleName !== "Invited" || user.Id === invitedRespondentId || user.Id === riskModel.riskOwnerId);
        const isUserNotInSiblingOrgsAndNotProjectViewer = multiForkJoin(areAllEqualTo(true), isNotProjectViewer, userExistsInOrgIdMap, removeInvitedUsersWhoAreNotRespondentOrRiskOwner(respondentId), removeDisabledUser);
        const getUsersThatAreNotInSiblingOrgsAndNotProjectViewer = getFilteredUsers(isUserNotInSiblingOrgsAndNotProjectViewer);

        return getUsersThatAreNotInSiblingOrgsAndNotProjectViewer(this.store.getState());
    }
}
