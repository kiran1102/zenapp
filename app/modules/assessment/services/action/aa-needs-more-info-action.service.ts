// Angular
import {
    Injectable,
    Inject,
} from "@angular/core";

// Redux
import { StoreToken } from "tokens/redux-store.token";

// Services
import { ModalService } from "sharedServices/modal.service";
import { AANeedsMoreInfoApiService } from "modules/assessment/services/api/aa-needs-more-info-api.service";

// Interfaces
import { IStore } from "interfaces/redux.interface";
import {
    INeedsMoreInfoCreationDetails,
    INeedsMoreInfoApiResponse,
    INeedsMoreInfoResponse,
    INeedsMoreInfoModel,
} from "interfaces/assessment/needs-more-info.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";
import { AANeedsMoreInfoReducerAction } from "modules/assessment/reducers/aa-needs-more-info.reducer";

@Injectable()
export class AANeedsMoreInfoActionService {
    constructor(
        @Inject(StoreToken) private store: IStore,
        private modalService: ModalService,
        private NeedsMoreInfoApi: AANeedsMoreInfoApiService,
    ) { }

    openNeedsMoreInfoModal = (assessmentId: string, sectionId: string, questionId: string): void => {
        const modalData: INeedsMoreInfoCreationDetails = {
            assessmentId,
            sectionId,
            questionId,
            response: "",
        };
        this.openModal(modalData);
    }

    openModal = (modalData: INeedsMoreInfoCreationDetails): void => {
        this.modalService.setModalData({ modalData });
        this.modalService.openModal("downgradeAaNeedsMoreInfoModal");
    }

    getAllAssessmentRequestDetails = (assessmentId: string): ng.IPromise<INeedsMoreInfoModel> => {
        return this.NeedsMoreInfoApi.getAllAssessmentRequestDetails(assessmentId)
            .then((response: IProtocolResponse<INeedsMoreInfoModel>): INeedsMoreInfoModel => {
                if (response.result) {
                    this.store.dispatch({ type: AANeedsMoreInfoReducerAction.SET_NEEDS_MORE_INFO, needsMoreInfoModel: response.data });
                    return response.data;
                }
            });
    }

    getAllAssessmentRequestResponses = (assessmentId: string, rootRequestId: string): ng.IPromise<INeedsMoreInfoResponse[]> => {
        return this.NeedsMoreInfoApi.getAllAssessmentRequestResponses(assessmentId, rootRequestId)
            .then((response: IProtocolResponse<INeedsMoreInfoResponse[]>): INeedsMoreInfoResponse[] => {
                if (response.result) {
                    return response.data;
                }
            });
    }

    addNeedsMoreInfoRequest = (assessmentId: string, sectionId: string, questionId: string, requestText: string, isResponseEditable: boolean): ng.IPromise<boolean> => {
        const needsMoreInfoCreationDetails: INeedsMoreInfoCreationDetails = {
            assessmentId,
            sectionId,
            questionId,
            response: requestText,
            isResponseEditable,
        };
        return this.NeedsMoreInfoApi.addAssessmentRequest(needsMoreInfoCreationDetails)
            .then((response: IProtocolResponse<INeedsMoreInfoApiResponse>): boolean => {
                return response.result;
            });
    }

    addNeedsMoreInfoResponse = (assessmentId: string, sectionId: string, questionId: string, rootRequestId: string, responseText: string, isResponseEditable: boolean = false): ng.IPromise<INeedsMoreInfoModel> => {
        // isResponseEditable is being set in the BackEnd. So assigning a default value
        const needsMoreInfoCreationDetails: INeedsMoreInfoCreationDetails = {
            assessmentId,
            sectionId,
            questionId,
            response: responseText,
            isResponseEditable,
        };
        return this.NeedsMoreInfoApi.addAssessmentResponse(needsMoreInfoCreationDetails, rootRequestId)
            .then((response: IProtocolResponse<INeedsMoreInfoApiResponse>): ng.IPromise<INeedsMoreInfoModel> => {
                if (response.result) {
                    return this.getAllAssessmentRequestDetails(assessmentId);
                }
            });
    }

    reOpenNeedsMoreInfoRequest = (assessmentId: string, sectionId: string, questionId: string, rootRequestId: string, comment: string, isResponseEditable: boolean): ng.IPromise<INeedsMoreInfoModel> => {
        const needsMoreInfoCreationDetails: INeedsMoreInfoCreationDetails = {
            assessmentId,
            sectionId,
            questionId,
            response: comment,
            isResponseEditable,
        };
        return this.NeedsMoreInfoApi.reOpenNeedsMoreInfoRequest(needsMoreInfoCreationDetails, rootRequestId)
            .then((response: IProtocolResponse<INeedsMoreInfoApiResponse>): ng.IPromise<INeedsMoreInfoModel> => {
                if (response.result) {
                    return this.getAllAssessmentRequestDetails(assessmentId);
                }
            });
    }

    closeNeedsMoreInfoRequest = (assessmentId: string, rootRequestId: string): ng.IPromise<INeedsMoreInfoModel> => {
        return this.NeedsMoreInfoApi.closeNeedsMoreInfoRequest(assessmentId, rootRequestId)
            .then((response: IProtocolResponse<INeedsMoreInfoApiResponse>): ng.IPromise<INeedsMoreInfoModel> => {
                if (response.result) {
                    return this.getAllAssessmentRequestDetails(assessmentId);
                }
            });
    }
}
