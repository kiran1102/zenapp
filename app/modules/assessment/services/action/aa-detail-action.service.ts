// Angular
import {
    Injectable,
    Inject,
} from "@angular/core";
import { StateService } from "@uirouter/core";

// 3rd Party
import {
    map as lodashMap,
    includes,
    uniqueId,
} from "lodash";
import { Observable } from "rxjs";
import {
    tap,
    first,
} from "rxjs/operators";

// Services
import { ModalService } from "sharedServices/modal.service";
import { NotificationService } from "modules/shared/services/provider/notification.service";
import { AssessmentDetailApiService } from "modules/assessment/services/api/assessment-detail-api.service";
import { AaDetailSchedulerService } from "modules/assessment/services/scheduler/aa-detail-scheduler.service";
import { TimeStamp } from "oneServices/time-stamp.service";
import { AssessmentSectionDetailHeaderViewLogicService } from "modules/assessment/services/view-logic/assessment-detail-header-view-logic.service";
import { Permissions } from "modules/shared/services/helper/permissions.service";
import { AaSectionNavMessagingService } from "../messaging/aa-section-messaging.service";
import {
    ConfirmModalType,
    OtModalService,
    ModalSize,
} from "@onetrust/vitreus";

// Interfaces
import {
    IAssessmentModel,
    IAssessmentApprover,
} from "interfaces/assessment/assessment-model.interface";
import {
    IAssessmentMetadataRequest,
    ISaveSectionResponsesContract,
} from "interfaces/assessment/assessment-api.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";
import {
    IStore,
    IStoreState,
} from "interfaces/redux.interface";
import {
    IStringMap,
    INameId,
    ICommentNameId,
} from "interfaces/generic.interface";
import {
    IQuestionResponseV2,
    IPersonalDataUpdateResponseMap,
    IQuestionPersonalDataResponse,
} from "interfaces/assessment/question-response-v2.interface";
import { IConfirmationModalResolve } from "interfaces/confirmation-modal-resolve.interface";
import {
    IQuestionModelOption,
    IQuestionModel,
} from "interfaces/assessment/question-model.interface";
import { IAssessmentTemplate } from "modules/template/interfaces/template.interface";
import { ISectionHeaderModel } from "interfaces/assessment/section-model.interface";
import {
    IApproverTextConfig,
    IUnapproveModalResolve,
} from "interfaces/assessment/aa-approval-modal.interface";

// Enums / Constants
import { EmptyGuid } from "constants/empty-guid.constant";
import { QuestionResponseType } from "enums/assessment/assessment-question.enum";
import { InventoryDetailsTabs } from "constants/inventory.constant";
import { QuestionTypes } from "constants/question-types.constant";
import { InventoryTableIds } from "enums/inventory.enum";
import { IRelatedDataSubjectModalData } from "modules/assessment/components/assessment-detail-question-related-data-subject-modal/assessment-detail-question-related-data-subject-modal.component";
import { NeedsMoreInfoFilterState } from "enums/aa-needs-more-info.enum";
import { StatusCodes } from "enums/status-codes.enum";
import { AAErrorMessage } from "constants/aa-error-message.constant";
import { SendBackType } from "enums/assessment/assessment-status.enum";
import { AAErrorCode } from "enums/aa-error-code.enum";

// Rxjs
import {
    filter,
    take,
} from "rxjs/operators";
import { BehaviorSubject } from "rxjs";

// Redux
import { getNeedsMoreInfoState } from "modules/assessment/reducers/aa-needs-more-info.reducer";
import { getCurrentUser } from "oneRedux/reducers/user.reducer";
import {
    AssessmentDetailAction,
    getAssessmentDetailAssessmentId,
    getAssessmentDetailModel,
    getAssessmentDetailSelectedSectionId,
    getQuestionIdsToSave,
    getQuestionModels,
    getQuestionResponseIds,
    getQuestionResponseModels,
    getQuestionResponsesArray,
    getSubQuestionIdsToSave,
    getSubQuestionResponseIds,
    getSubQuestionResponseModels,
    getAssessmentDetailSelectedSectionRespondents,
    isAssessmentDetailWelcomeSection,
    getAssessmentDetailApproverNames,
    getAssessmentDetailName,
    getAssessmentDetailCreatorName,
    getAssessmentDetailDeadline,
    getAssessmentDetailDescription,
    getAssessmentDetailOrg,
    getAssessmentDetailSelectedSectionRespondent,
    getAssessmentDetailSectionModel,
    getAssessmentDetailSelectedSectionApprovers,
    getAssessmentDetailReminder,
    getAssessmentDetailTemplate,
    getAssessmentDetailTags,
} from "modules/assessment/reducers/assessment-detail.reducer";
import { StoreToken } from "tokens/redux-store.token";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

// Components
import { AASendBackModalComponent } from "modules/assessment/components/aa-send-back-modal/aa-send-back-modal.component";
import { IIncidentJurisdiction } from "incidentModule/shared/interfaces/incident-jurisdiction.interface";

@Injectable()
export class AaDetailActionService {
    public undoReviewResult$ = new BehaviorSubject<boolean>(null);

    constructor(
        private stateService: StateService,
        @Inject(StoreToken) private store: IStore,
        private translatePipe: TranslatePipe,
        private modalService: ModalService,
        private assessmentDetailApi: AssessmentDetailApiService,
        private assessmentDetailScheduler: AaDetailSchedulerService,
        private permissions: Permissions,
        private notificationService: NotificationService,
        private timeStampService: TimeStamp,
        private aaSectionNavMessagingService: AaSectionNavMessagingService,
        private assessmentSectionDetailHeaderViewLogic: AssessmentSectionDetailHeaderViewLogicService,
        private otModalService: OtModalService,
    ) { }

    reset(): void {
        this.undoReviewResult$.next(null);
    }

    createAssessmentMetdataUpdatepayload(assessment: IAssessmentModel): IAssessmentMetadataRequest {
        const tempUpdateRespondentRequests = [];
        const tempUpdateApproverRequests = [];
        const tempUpdatedAssessmentTags = [];

        assessment.respondents.forEach((respondent: ICommentNameId<string>): void => {
            tempUpdateRespondentRequests.push({
                respondentId: respondent.id,
                respondentName: respondent.name,
                comment: respondent.comment,
            });
        });

        assessment.approvers.forEach((approver: IAssessmentApprover): void => {
            tempUpdateApproverRequests.push({
                approverId: approver.id,
                approverName: approver.name,
                comment: approver.comment,
            });
        });

        assessment.tags.forEach((tags: IQuestionModelOption): void => {
            tempUpdatedAssessmentTags.push(tags.option);
        });

        const assessmentMetaDataRequest = {
            name: assessment.name,
            description: assessment.description,
            orgGroupId: assessment.orgGroup.id,
            orgGroupName: assessment.orgGroup.name,
            deadline: assessment.deadline,
            reminder: assessment.reminder,
            updateRespondentRequests: tempUpdateRespondentRequests,
            updateApproverRequests: tempUpdateApproverRequests,
            tags: tempUpdatedAssessmentTags,
        };
        return assessmentMetaDataRequest;
    }

    // welcome section is a special one because it doesn't have its own section id. we call a different endpoint
    fetchAssessmentWelcomeSection(assessmentId: string): void {
        if (getAssessmentDetailModel(this.store.getState())) throw new Error();  // when assessmentModel exists, use goToWelcomeSection()
        this.store.dispatch({ type: AssessmentDetailAction.LOADING_ASSESSMENT_PAGE });
        this.assessmentDetailApi.fetchAssessmentWelcomeSection(assessmentId)
            .then((response: IAssessmentModel | null) => {
                this.store.dispatch({ type: AssessmentDetailAction.UPDATE_ASSESSMENT_ALL, assessmentModel: response });
                this.store.dispatch({ type: AssessmentDetailAction.FINISH_LOADING_ASSESSMENT_SECTION });
            });
    }

    fetchAssessmentTemplateMetadata(assessmentId: string): void {
        this.assessmentDetailApi.fetchAssessmentTemplateMetadata(assessmentId)
            .then((response: IAssessmentTemplate) => {
                this.store.dispatch({ type: AssessmentDetailAction.SET_TEMPLATE_METADATA, templateMetadata: response });
            });
    }

    // sets selectedSectionId in the store which is used by section accordions to highlight the color
    selectSection(sectionId: string): void {
        this.store.dispatch({ type: "SET_CURRENT_SECTION", selectedSectionId: sectionId });
    }

    getIdsForSectionChange(): { assessmentId: string, sectionId: string } {
        const assessmentId = getAssessmentDetailAssessmentId(this.store.getState());
        const sectionId = getAssessmentDetailSelectedSectionId(this.store.getState());
        return { assessmentId, sectionId };
    }

    // convenience method
    goToSectionHelper(_sectionId: string = null): void {
        const assessmentId = getAssessmentDetailAssessmentId(this.store.getState());
        const sectionId = _sectionId || getAssessmentDetailSelectedSectionId(this.store.getState());

        if (sectionId === EmptyGuid) {
            this.goToWelcomeSection();
        } else {
            this.goToSection(assessmentId, sectionId);
        }
    }

    goToPreviousSection(): void {
        const { assessmentId, sectionId } = this.getIdsForSectionChange();
        this.closeAllExpandedSection();
        this.assessmentDetailScheduler.assessmentDetailMiddleware$.next({ type: "FETCH_NEW_SECTION", payload: { assessmentId, sectionId, direction: "PREVIOUS" } });
    }

    goToNextSection(): void {
        const { assessmentId, sectionId } = this.getIdsForSectionChange();
        this.closeAllExpandedSection();
        this.assessmentDetailScheduler.assessmentDetailMiddleware$.next({ type: "FETCH_NEW_SECTION", payload: { assessmentId, sectionId, direction: "NEXT" } });
    }

    // first section means the section right after welcome section
    goToFirstSection(): void {
        const { assessmentId, sectionId } = this.getIdsForSectionChange();
        this.assessmentDetailScheduler.assessmentDetailMiddleware$.next({ type: "FETCH_NEW_SECTION", payload: { assessmentId, sectionId, direction: "FIRST" } });
    }

    updateTempAssessment(keyValueToMerge: IStringMap<any>): void {
        this.store.dispatch({ type: AssessmentDetailAction.UPDATE_TEMP_ASSESSMENT, keyValueToMerge });
    }

    openEditAssessmentDetailsModal(assessmentId: string, sectionHeaderModal: ISectionHeaderModel): void {
        this.modalService.setModalData(sectionHeaderModal);
        this.modalService.openModal("downgradeAAMetadataModal");
    }

    getAssessmentTags(): ng.IPromise<IQuestionModelOption[]> {
        return this.assessmentDetailApi.getAssessmentTags().then((res: IProtocolResponse<IQuestionModelOption[]>) => {
            return res.data;
        });
    }

    // pulls temp assessment model in the store and makes a PUT request
    saveTempAssessment(tempUpdatedAssessment: IAssessmentModel): ng.IPromise<boolean> {
        this.updateTempAssessment(tempUpdatedAssessment);
        return this.assessmentDetailApi.updateAssessmentMetadata(tempUpdatedAssessment.assessmentId, this.createAssessmentMetdataUpdatepayload(tempUpdatedAssessment))
            .then((response: IProtocolResponse<null>): boolean => {
                if (response.result) {
                    this.fetchCurrentSection(tempUpdatedAssessment.assessmentId).then((assessmentModel: IAssessmentModel): void => {
                        this.store.dispatch({ type: AssessmentDetailAction.UPDATE_ASSESSMENT_ALL, assessmentModel });
                        this.store.dispatch({ type: AssessmentDetailAction.FINISH_LOADING_ASSESSMENT_SECTION });
                    });
                } else if (response.status === 423 && response.errors.code === "ERROR_ASSESSMENT-V2_ASSESSMENT_LOCKED") {
                    this.notificationService.alertError(this.translatePipe.transform("Error"), this.translatePipe.transform("AssessmentLockedForPushingTemplate"));
                } else if (response.errors && response.errors.code === AAErrorCode.INVALID_APPROVER_RESPONDENT_CHANGE_AFTER_DEADLINE) {
                    this.notificationService.alertError(this.translatePipe.transform("Error"), this.translatePipe.transform(AAErrorMessage[response.errors.code]));
                } else {
                    this.notificationService.alertError(this.translatePipe.transform("Error"), this.translatePipe.transform("AssessmentDetailsUpdateFailed"));
                }
                return response.result;
            });
    }

    openAssessmentDataSubjectElementModal(modalData: IRelatedDataSubjectModalData): void {
        this.modalService.setModalData(modalData);
        this.modalService.openModal("downgradeAssessmentDetailRelatedDataSubjectModal");
    }

    openAssessmentActivityModal(modalData: IRelatedDataSubjectModalData): void {
        this.modalService.openModal("downgradeAssessmentActivityModal");
    }

    resetAssessmentDetailState(): void {
        this.store.dispatch({ type: AssessmentDetailAction.RESET_ASSESSMENT_DETAIL_STATE });
    }

    // adds a section id to store which is then used to expand a section accordion
    toggleExpandSection(sectionIdToToggle: string): void {
        this.store.dispatch({ type: AssessmentDetailAction.TOGGLE_SECTION_ACCORDION, sectionIdToToggle });
    }

    // close all section accordions
    closeAllExpandedSection(): void {
        this.store.dispatch({ type: AssessmentDetailAction.CLOSE_SECTION_ACCORDIONS });
    }

    submitAssessment(): void {
        this.otModalService.confirm({
            type: ConfirmModalType.SUCCESS,
            translations: {
                title: this.translatePipe.transform("SubmitAssessmentPrompt"),
                desc: "",
                confirm: this.translatePipe.transform("AreYouSureYouWantToSubmitThisAssessment"),
                cancel: this.translatePipe.transform("Cancel"),
                submit: this.translatePipe.transform("Confirm"),
            },
        }).pipe(
            take(1),
            filter((result: boolean) => result),
        ).subscribe(() => {
            this.store.dispatch({ type: AssessmentDetailAction.SUBMITTING_ASSESSMENT });
            this.saveAssessment();
        });
    }

    saveAssessment(): ng.IPromise<boolean> {
        const { assessmentId } = getAssessmentDetailModel(this.store.getState());
        return this.assessmentDetailApi.submitAssessment(assessmentId).then((res: IProtocolResponse<null>): ng.IPromise<boolean> | boolean => {
            if (res.result) {
                this.routeToListOrThankYouPage(assessmentId);
                this.notificationService.alertSuccess(this.translatePipe.transform("Success"), this.translatePipe.transform("AssessmentSubmitted"));
                return true;
            } else if (res.status === StatusCodes.Conflict) {
                return this.handleInvalidResponse(assessmentId);
            } else if (res.status === StatusCodes.Forbidden && res.data === "INVENTORY_ACTION_UNAUTHORIZED") {
                this.notificationService.alertWarning(this.translatePipe.transform("Warning"), this.translatePipe.transform("SubmitPermissionDenied"));
                this.store.dispatch({ type: AssessmentDetailAction.SUBMITTING_ASSESSMENT_COMPLETE });
                return false;
            } else if (res.status === 423 && res.errors.code === "ERROR_ASSESSMENT-V2_ASSESSMENT_LOCKED") {
                this.notificationService.alertError(this.translatePipe.transform("Error"), this.translatePipe.transform("AssessmentLockedForPushingTemplate"));
                this.store.dispatch({ type: AssessmentDetailAction.SUBMITTING_ASSESSMENT_COMPLETE });
                return false;
            } else {
                this.notificationService.alertError(this.translatePipe.transform("Error"), this.translatePipe.transform("CouldNotSubmitAssessment"));
                this.store.dispatch({ type: AssessmentDetailAction.SUBMITTING_ASSESSMENT_COMPLETE });
                return false;
            }
        });
    }

    routeToListOrThankYouPage(assessmentId: string, saveAndExit = false): void {
        if (!this.permissions.canShow("Login")) {
            this.stateService.go("zen.app.pia.module.thanks", {
                projectId: assessmentId,
                templateType: "PIA",
                projectVersion: 1,
                saveAndExit,
            });
        } else if (this.stateService.params.launchAssessmentDetails) {
            this.routeToInventoryAssessments();
        } else {
            this.stateService.go("zen.app.pia.module.assessment.list");
        }
    }

    sendBack(sendBackStage: string, approverSendBackFlow: boolean, openInfoRequestMsg: string): void {
        this.otModalService
            .create(
                AASendBackModalComponent,
                {
                    isComponent: true,
                    size: ModalSize.SMALL,
                },
                {
                    title: sendBackStage === SendBackType.SendBackToInProgress ? this.translatePipe.transform("SendBackToInProgress")
                        : this.translatePipe.transform("SendBackToUnderReview"),
                    sendBackType: sendBackStage,
                    approverSendBackFlow,
                    openInfoRequestMsg,
                },
            );
    }

    undoReviewAssessment(): void {
        const { assessmentId } = getAssessmentDetailModel(this.store.getState());
        this.assessmentDetailApi.undoReviewAssessment(assessmentId).pipe(
            first(),
        ).subscribe((res: IProtocolResponse<null>): void => {
            if (res.result) {
                if (this.stateService.params.launchAssessmentDetails) {
                    this.routeToInventoryAssessments();
                } else {
                    this.stateService.go("zen.app.pia.module.assessment.list");
                }
                this.notificationService.alertSuccess(this.translatePipe.transform("Success"), this.translatePipe.transform("ResultCleared"));
                this.undoReviewResult$.next(true);
            } else if (res.status === 423 && res.errors.code === "ERROR_ASSESSMENT-V2_ASSESSMENT_LOCKED") {
                this.notificationService.alertError(this.translatePipe.transform("Error"), this.translatePipe.transform("AssessmentLockedForPushingTemplate"));
                this.undoReviewResult$.next(false);
            } else {
                this.notificationService.alertError(this.translatePipe.transform("Error"), this.translatePipe.transform("CouldNotUnapproveAssessment"));
                this.undoReviewResult$.next(false);
            }
        });
    }

    getApproveAssessmentConfirmationText(): string {
        const hasOpenInfoRequest: boolean = getNeedsMoreInfoState(this.store.getState()).needsMoreInfoModel.rootRequestInformationResponses.some((needsMoreInfo) => needsMoreInfo.responseState === NeedsMoreInfoFilterState.Open);
        return hasOpenInfoRequest ? this.translatePipe.transform("ApproveAssessmentWithOpenInfoRequestConfirmation")
            : this.translatePipe.transform("AreYouSureYouWantToApproveThisAssessment");
    }

    getNewApproveAssessmentConfirmationText(needsConfirmation?: boolean): IApproverTextConfig {
        const currentUser = getCurrentUser(this.store.getState());
        const assessmentApprovers = getAssessmentDetailApproverNames(this.store.getState());
        const isValidApprover: boolean = includes(assessmentApprovers, currentUser.FullName);
        const hasOpenInfoRequest: boolean = getNeedsMoreInfoState(this.store.getState()).needsMoreInfoModel.rootRequestInformationResponses.some((needsMoreInfo) => needsMoreInfo.responseState === NeedsMoreInfoFilterState.Open);

        let approvalTextConfig: IApproverTextConfig = {
            forceApprove: false,
            bypassOtherApproversEnabled: false,
            approverNMI: hasOpenInfoRequest,
            isMultiApprover: assessmentApprovers.length > 1,
            hasApprover: Boolean(assessmentApprovers && assessmentApprovers.length),
            needsConfirmation,
        };
        if (isValidApprover && this.permissions.canShow("AssessmentApprove") && approvalTextConfig.isMultiApprover) {
            approvalTextConfig = {
                ...approvalTextConfig,
                bypassOtherApproversEnabled: true,
            };
        } else if (!isValidApprover && this.permissions.canShow("AssessmentApprove") && approvalTextConfig.hasApprover) {
            approvalTextConfig = {
                ...approvalTextConfig,
                forceApprove: true,
            };
        }
        return approvalTextConfig;
    }

    handleInvalidResponse(assessmentId: string): ng.IPromise<boolean> {
        this.notificationService.alertWarning(this.translatePipe.transform("Warning"), this.translatePipe.transform("InvalidResponse"));
        return this.fetchCurrentSection(assessmentId).then((assessmentModel: IAssessmentModel): boolean => {
            this.store.dispatch({ type: AssessmentDetailAction.UPDATE_ASSESSMENT_ALL, assessmentModel });
            this.store.dispatch({ type: AssessmentDetailAction.SUBMITTING_ASSESSMENT_COMPLETE });
            return true;
        });
    }

    routeToInventoryAssessments(): void {
        const launchAssessmentParams = {
            recordId: this.stateService.params.launchAssessmentDetails.inventoryId,
            inventoryId: this.stateService.params.launchAssessmentDetails.inventoryTypeId,
            tabId: InventoryDetailsTabs.Assessments,
        };
        if (launchAssessmentParams.inventoryId) {
            switch (launchAssessmentParams.inventoryId) {
                case InventoryTableIds.Vendors:
                    this.stateService.go("zen.app.pia.module.vendor.inventory.details", launchAssessmentParams);
                    break;
                default:
                    this.stateService.go("zen.app.pia.module.datamap.views.inventory.details", launchAssessmentParams);
            }
        } else {
            this.stateService.go("zen.app.pia.module.vendor.inventory.assessments", launchAssessmentParams);
        }
    }

    questionResponseAdapter(option: IQuestionModelOption): IQuestionResponseV2 {
        return {
            responseId: option.id,
            response: option.option,
            type: option.optionType,
        };
    }

    handleNotSureNotApplicable(question: IQuestionModel, option: Partial<IQuestionModelOption>, subQuestionId: string = null, forceSelect = false): void {
        const isResponseSelected = this.isResponseSelected(option.id, question.id, subQuestionId);
        this.removeQuestionResponses(question.id, subQuestionId);

        if (!isResponseSelected || forceSelect) {
            this.addQuestionOptionResponse(question, option, subQuestionId);
        }

        this.markQuestionToBeSaved(question.id, subQuestionId);
    }

    handleJurisdiction(question: IQuestionModel, jurisdictions, subQuestionId: string = null): void {
        this.removeQuestionResponses(question.id, subQuestionId);

        jurisdictions.map((jurisdiction: IIncidentJurisdiction) => {
            const response = { responseId: jurisdiction.jurisdictionId, response: JSON.stringify(jurisdiction), type: QuestionResponseType.Default };
            subQuestionId
                ? this.store.dispatch({ type: AssessmentDetailAction.ADD_SUB_QUESTION_RESPONSE, questionId: question.id, subQuestionId, response })
                : this.store.dispatch({ type: AssessmentDetailAction.ADD_QUESTION_RESPONSE, questionId: question.id, response });
        });

        this.markQuestionToBeSaved(question.id, subQuestionId);
    }

    handleOther(questionId: string, responseText: string, responseId: string, isDuplicate: boolean, subQuestionId: string = null): void {
        if (isDuplicate) {
            this.notificationService.alertWarning(this.translatePipe.transform("Error"), this.translatePipe.transform("DuplicateOptionsNotAllowed"));
            this.removeQuestionResponse(questionId, subQuestionId, responseId);
        } else {
            const question: IQuestionModel = getQuestionModels(this.store.getState())[questionId];
            const { attributes: { isMultiSelect } } = question;

            if (responseId) {
                if (responseText) {
                    const response = { responseId, response: responseText, type: QuestionResponseType.Others };
                    subQuestionId
                        ? this.store.dispatch({ type: AssessmentDetailAction.ADD_SUB_QUESTION_RESPONSE, questionId, subQuestionId, response })
                        : this.store.dispatch({ type: AssessmentDetailAction.ADD_QUESTION_RESPONSE, questionId, response });
                } else {
                    if (!isMultiSelect || this.hasNotSureNotApplicableResponses(questionId)) {
                        this.removeQuestionResponses(questionId, subQuestionId);
                    } else {
                        this.removeQuestionResponse(questionId, subQuestionId, responseId);
                    }
                }
            } else {
                if (!isMultiSelect || this.hasNotSureNotApplicableResponses(questionId, subQuestionId)) {
                    this.removeQuestionResponses(questionId, subQuestionId);
                }
                const response = { responseId: uniqueId(), response: responseText, type: QuestionResponseType.Others };
                subQuestionId
                    ? this.store.dispatch({ type: AssessmentDetailAction.ADD_SUB_QUESTION_RESPONSE, questionId, subQuestionId, response })
                    : this.store.dispatch({ type: AssessmentDetailAction.ADD_QUESTION_RESPONSE, questionId, response });
            }
        }

        this.markQuestionToBeSaved(questionId, subQuestionId);
    }

    handleDate(questionId: string, date: string, responseId: string): void {
        const responseArray = getQuestionResponsesArray(questionId)(this.store.getState());
        if (responseArray && !date && (responseArray[0].type === "NOT_SURE" || responseArray[0].type === "NOT_APPLICABLE")) {
            return;
        }

        const dateString = date ? JSON.parse(JSON.stringify(date)).replace(".000Z", "Z") : null;
        this.removeQuestionResponses(questionId);

        if (responseId) {
            if (dateString) {
                const response: IQuestionResponseV2 = { responseId, response: dateString, type: QuestionResponseType.Default };
                this.store.dispatch({ type: AssessmentDetailAction.ADD_QUESTION_RESPONSE, questionId, response });
            }
        } else {
            const response: IQuestionResponseV2 = { responseId: null, response: dateString, type: QuestionResponseType.Default };
            this.store.dispatch({ type: AssessmentDetailAction.ADD_QUESTION_RESPONSE, questionId, response });
        }

        this.markQuestionToBeSaved(questionId);
    }

    handleTextbox(responseText: string, responseId: string = null, questionId: string, subQuestionId: string = null): void {
        this.removeQuestionResponses(questionId, subQuestionId);

        if (responseText) {
            const response: IQuestionResponseV2 = { responseId, response: responseText, type: QuestionResponseType.Default };
            subQuestionId
                ? this.store.dispatch({ type: AssessmentDetailAction.ADD_SUB_QUESTION_RESPONSE, questionId, subQuestionId, response })
                : this.store.dispatch({ type: AssessmentDetailAction.ADD_QUESTION_RESPONSE, questionId, response });
        }

        this.markQuestionToBeSaved(questionId, subQuestionId);
    }

    handleDatetime(responseText: string, responseId: string = null, questionId: string, subQuestionId: string = null): void {
        this.removeQuestionResponses(questionId, subQuestionId);

        if (responseText) {
            const response: IQuestionResponseV2 = { responseId, response: responseText, type: QuestionResponseType.Default };
            subQuestionId
                ? this.store.dispatch({ type: AssessmentDetailAction.ADD_SUB_QUESTION_RESPONSE, questionId, subQuestionId, response })
                : this.store.dispatch({ type: AssessmentDetailAction.ADD_QUESTION_RESPONSE, questionId, response });
        }

        this.markQuestionToBeSaved(questionId, subQuestionId);
    }

    handleYesNo(question: IQuestionModel, option: Partial<IQuestionModelOption>): void {
        if (this.isResponseSelected(option.id, question.id)) {
            this.removeQuestionResponses(question.id);
        } else {
            this.removeQuestionResponses(question.id);
            this.addQuestionOptionResponse(question, option);
        }

        this.markQuestionToBeSaved(question.id);
    }

    handleMultichoice(question: IQuestionModel, option: Partial<IQuestionModelOption>, subQuestionId: string = null): void {
        const { attributes: { isMultiSelect }, id: questionId } = question;

        if (this.isResponseSelected(option.id, questionId, subQuestionId)) {
            if (isMultiSelect) {
                this.removeQuestionResponse(questionId, subQuestionId, option.id);
            } else {
                this.removeQuestionResponses(questionId, subQuestionId);
            }
        } else {
            if (!isMultiSelect || this.hasNotSureNotApplicableResponses(questionId, subQuestionId)) {
                this.removeQuestionResponses(questionId, subQuestionId);
            }
            this.addQuestionOptionResponse(question, option, subQuestionId);
        }

        this.markQuestionToBeSaved(questionId, subQuestionId);
    }

    handleMultiselectDropdown(question: IQuestionModel, options: Array<Partial<IQuestionModelOption>>, subQuestionId: string = null): void {
        this.removeQuestionResponses(question.id, subQuestionId);
        this.addMultiSelectDropdownResponses(options, question, subQuestionId);
        this.markQuestionToBeSaved(question.id, subQuestionId);
    }

    handleMultichoiceReset(questionId: string, subQuestionId: string = null): void {
        this.removeQuestionResponses(questionId, subQuestionId);
        this.markQuestionToBeSaved(questionId, subQuestionId);
    }

    handleJustification(response: string, questionId: string, subQuestionId: string = null): void {
        subQuestionId
            ? this.store.dispatch({ type: AssessmentDetailAction.UPDATE_SUB_JUSTIFICATION, response, questionId, subQuestionId })
            : this.store.dispatch({ type: AssessmentDetailAction.UPDATE_JUSTIFICATION, response, questionId });

        this.markQuestionToBeSaved(questionId, subQuestionId);
    }

    addQuestionOptionResponse(question: IQuestionModel, option: Partial<IQuestionModelOption>, subQuestionId: string = null): void {
        const questionId = question.id;
        const response = (question.questionType === QuestionTypes.Inventory.name) ?
            this.normalizeInventoryQuestionModelOption(option)
            : this.normalizeQuestionModelOption(option);

        subQuestionId
            ? this.store.dispatch({ type: AssessmentDetailAction.ADD_SUB_QUESTION_RESPONSE, questionId, subQuestionId, response })
            : this.store.dispatch({ type: AssessmentDetailAction.ADD_QUESTION_RESPONSE, questionId, response });
    }

    addMultiSelectDropdownResponses(options: Array<Partial<IQuestionModelOption>>, question: IQuestionModel, subQuestionId: string = null): void {
        const questionId = question.id;
        const responses = (question.questionType === QuestionTypes.Inventory.name) ?
            lodashMap(options, this.normalizeInventoryQuestionModelOption)
            : lodashMap(options, this.normalizeQuestionModelOption);
        const responsesAllIds = lodashMap(responses, (response: IQuestionResponseV2) => response.responseId);
        const responsesById = responses.reduce((map: Partial<Map<string, IQuestionResponseV2>>, obj: IQuestionResponseV2) => {
            map[obj.responseId] = obj;
            return map;
        }, {});

        subQuestionId
            ? this.store.dispatch({ type: AssessmentDetailAction.ADD_SUB_MULTISELECT_DROPDOWN_RESPONSES, questionId, subQuestionId, responsesAllIds, responsesById })
            : this.store.dispatch({ type: AssessmentDetailAction.ADD_MULTISELECT_DROPDOWN_RESPONSES, questionId, responsesAllIds, responsesById });
    }

    isResponseSelected(optionId: string, questionId: string, subQuestionId: string = null): boolean {
        const responseIds = subQuestionId
            ? getSubQuestionResponseIds(questionId, subQuestionId)(this.store.getState())
            : getQuestionResponseIds(questionId)(this.store.getState());

        return includes(responseIds, optionId);
    }

    normalizeQuestionModelOption(option: Partial<IQuestionModelOption>): IQuestionResponseV2 {
        return { responseId: option.id, response: option.option, type: option.optionType };
    }

    normalizeInventoryQuestionModelOption(option: Partial<IQuestionModelOption>): IQuestionResponseV2 {
        return {
            responseId: option.id,
            response: (option.optionDisplay) ? option.optionDisplay : option.option,
            type: option.optionType,
        };
    }

    removeQuestionResponse(questionId: string, subQuestionId: string = null, responseId: string): void {
        subQuestionId
            ? this.store.dispatch({ type: AssessmentDetailAction.REMOVE_SUB_QUESTION_RESPONSE, questionId, subQuestionId, responseId })
            : this.store.dispatch({ type: AssessmentDetailAction.REMOVE_QUESTION_RESPONSE, questionId, responseId });
    }

    removeQuestionResponses(questionId: string, subQuestionId: string = null): void {
        subQuestionId
            ? this.store.dispatch({ type: AssessmentDetailAction.REMOVE_SUB_QUESTION_RESPONSES, questionId, subQuestionId })
            : this.store.dispatch({ type: AssessmentDetailAction.REMOVE_QUESTION_RESPONSES, questionId });
    }

    markQuestionToBeSaved(questionId: string, subQuestionId: string = null): void {
        const ids = getQuestionIdsToSave(this.store.getState());
        const questionIds = includes(ids, questionId) ? ids : ids.concat(questionId);
        const subQuestionIds = subQuestionId ? getSubQuestionIdsToSave(this.store.getState()) : null;

        subQuestionId
            ? this.store.dispatch({
                type: AssessmentDetailAction.UPDATE_SUB_QUESTION_IDS_TO_SAVE,
                questionIds,
                subQuestionIds: includes(subQuestionIds, subQuestionId) ? ids : ids.concat(subQuestionId),
            })
            : this.store.dispatch({
                type: AssessmentDetailAction.UPDATE_QUESTION_IDS_TO_SAVE,
                questionIds,
            });

        this.assessmentDetailScheduler.assessmentDetailMiddleware$.next({ type: "SAVE_QUESTION" });
    }

    clearInvalidResponses(questionId: string, subQuestionId: string = null): void {
        const question = getQuestionModels(this.store.getState())[questionId];
        const { attributes: { isMultiSelect } } = question;

        if (isMultiSelect) {
            subQuestionId
                ? this.store.dispatch({ type: "REMOVE_SUB_QUESTION_INVALID_RESPONSES", questionId, subQuestionId })
                : this.store.dispatch({ type: "REMOVE_QUESTION_INVALID_RESPONSES", questionId });
        } else if (question.questionType === "PERSONAL_DATA") {
            this.store.dispatch({ type: "REMOVE_PERSONAL_DATA_QUESTION_INVALID_RESPONSES", questionId });
        } else {
            this.removeQuestionResponses(questionId, subQuestionId);
        }

        this.markQuestionToBeSaved(questionId, subQuestionId);
    }

    formatQuestionResponses(currentlySelectedElements: IPersonalDataUpdateResponseMap[]): IQuestionPersonalDataResponse[] {
        return lodashMap(currentlySelectedElements, (response: IPersonalDataUpdateResponseMap): IQuestionPersonalDataResponse => {
            return {
                response: null,
                responseId: null,
                type: QuestionResponseType.Default,
                responseMap: response,
            };
        });
    }

    saveDSEResponses(response: ISaveSectionResponsesContract[]): ng.IPromise<boolean> {
        const assessmentId = getAssessmentDetailAssessmentId(this.store.getState());
        const sectionId = getAssessmentDetailSelectedSectionId(this.store.getState());

        return this.assessmentDetailApi.saveSectionResponses(assessmentId, response).then((res: IProtocolResponse<IAssessmentModel>): ng.IPromise<boolean> | boolean => {
            if (res.result) {
                return this.assessmentDetailApi.fetchCurrentSection(assessmentId, sectionId).then((assessmentModel: IAssessmentModel): boolean => {
                    if (assessmentModel) {
                        this.store.dispatch({ type: AssessmentDetailAction.UPDATE_ASSESSMENT_ALL, assessmentModel });
                        this.aaSectionNavMessagingService.publishSectionFilterData(this.aaSectionNavMessagingService.sectionFilter);
                    }
                    return !!assessmentModel;
                });
            } else {
                return res.result;
            }
        });
    }

    openEditAssessmentModel(assessmentId: string): ng.IPromise<boolean> {
        this.modalService.openModal("downgradeAAMetadataModal");
        return this.assessmentDetailApi.fetchAssessmentWelcomeSection(assessmentId)
            .then((response: IAssessmentModel | boolean): boolean => {
                if (response) {
                    this.store.dispatch({ type: AssessmentDetailAction.UPDATE_ASSESSMENT_ALL, assessmentModel: response });
                    this.store.dispatch({ type: AssessmentDetailAction.FINISH_LOADING_ASSESSMENT_SECTION });
                    return true;
                }
                return false;
            });
    }

    getAssessmentDetailSectionHeaderModel(state: IStoreState): ISectionHeaderModel {
        const assessment = getAssessmentDetailModel(state);
        const selectedSectionId = getAssessmentDetailSelectedSectionId(state);

        return {
            name: getAssessmentDetailName(state),
            org: getAssessmentDetailOrg(state),
            creatorName: getAssessmentDetailCreatorName(state),
            respondent: getAssessmentDetailSelectedSectionRespondent(selectedSectionId)(state),
            respondents: getAssessmentDetailSelectedSectionRespondents(selectedSectionId)(state),
            approvers: getAssessmentDetailSelectedSectionApprovers(selectedSectionId)(state),
            deadline: getAssessmentDetailDeadline(state),
            approverNames: getAssessmentDetailApproverNames(state).join(", "),
            description: getAssessmentDetailDescription(state),
            respondentNames: this.getRespondentNames(getAssessmentDetailSelectedSectionRespondents(selectedSectionId)(state)),
            reminder: getAssessmentDetailReminder(state),
            template: getAssessmentDetailTemplate(state),
            formattedDisplayDate: getAssessmentDetailDeadline(state) ? this.timeStampService.formatDateWithoutTime(getAssessmentDetailDeadline(state)) : "",
            viewState: this.assessmentSectionDetailHeaderViewLogic.calculateNewViewState(assessment, getAssessmentDetailSectionModel(state)),
            selectedTags: getAssessmentDetailTags(state),
        };
    }

    approveAssessmentPromiseToResolve(assessmentId: string, forceOverride: boolean = false, excludeRules: boolean = false, emailReviewComment: string): ng.IPromise<boolean> {
        this.store.dispatch({ type: AssessmentDetailAction.SUBMITTING_ASSESSMENT });

        return this.assessmentDetailApi.approveAssessment(assessmentId, forceOverride, excludeRules, emailReviewComment).then((res: IProtocolResponse<null>): ng.IPromise<boolean> | boolean => {
            if (res.result) {
                if (this.stateService.params.launchAssessmentDetails) {
                    this.routeToInventoryAssessments();
                } else {
                    this.stateService.go("zen.app.pia.module.assessment.list");
                }
                this.notificationService.alertSuccess(this.translatePipe.transform("Success"), this.translatePipe.transform("AssessmentApproved"));
                return true;
            } else if (res.status === StatusCodes.Conflict) {
                return this.handleInvalidResponse(assessmentId);
            } else if (res.status === StatusCodes.Forbidden && res.errors.code === AAErrorCode.UNAUTHORIZED_INVENTORY_ACTION) {
                this.notificationService.alertCustomizedNote(this.translatePipe.transform("Warning"), this.translatePipe.transform(AAErrorMessage[res.errors.code]), "warning", 10000);
                this.store.dispatch({ type: AssessmentDetailAction.SUBMITTING_ASSESSMENT_COMPLETE });
                return false;
            } else if (res.status === 423 && res.errors.code === "ERROR_ASSESSMENT-V2_ASSESSMENT_LOCKED") {
                this.notificationService.alertError(this.translatePipe.transform("Error"), this.translatePipe.transform("AssessmentLockedForPushingTemplate"));
                this.store.dispatch({ type: AssessmentDetailAction.SUBMITTING_ASSESSMENT_COMPLETE });
                return false;
            } else {
                this.notificationService.alertError(this.translatePipe.transform("Error"), this.translatePipe.transform("CouldNotReviewAnAssessment"));
                this.store.dispatch({ type: AssessmentDetailAction.SUBMITTING_ASSESSMENT_COMPLETE });
                return false;
            }
        });
    }

    rejectAssessment(assessmentId: string, forceOverride: boolean = false, emailComment: string = ""): Observable<IProtocolResponse<null>> {
        return this.assessmentDetailApi.rejectAssessment(assessmentId, forceOverride, emailComment)
            .pipe(tap((response: IProtocolResponse<null>) => {
                if (response.result) {
                    this.stateService.go("zen.app.pia.module.assessment.list");
                    this.notificationService.alertSuccess(this.translatePipe.transform("Success"), this.translatePipe.transform("AssessmentRejected"));
                }
            }));
    }

    fetchCurrentSection(assessmentId: string): ng.IPromise<IAssessmentModel | boolean> {
        if (isAssessmentDetailWelcomeSection(this.store.getState())) {
            return this.assessmentDetailApi.fetchAssessmentWelcomeSection(assessmentId);
        } else {
            return this.assessmentDetailApi.fetchCurrentSection(assessmentId, getAssessmentDetailSelectedSectionId(this.store.getState()));
        }
    }

    private getRespondentNames(respondents: Array<INameId<string>>): string {
        return respondents ? respondents.map((respondent: INameId<string>) => respondent.name).join(",") : null;
    }

    private goToSection(assessmentId: string, sectionId: string): void {
        this.selectSection(sectionId);
        this.assessmentDetailScheduler.assessmentDetailMiddleware$.next({ type: "FETCH_NEW_SECTION", payload: { assessmentId, sectionId } });
    }

    private goToWelcomeSection(): void {
        const { assessmentId } = this.getIdsForSectionChange();
        this.closeAllExpandedSection();
        this.selectSection(EmptyGuid);
        this.assessmentDetailScheduler.assessmentDetailMiddleware$.next({ type: "FETCH_NEW_SECTION", payload: { assessmentId, sectionId: EmptyGuid, direction: "WELCOME" } });
    }

    private hasNotSureNotApplicableResponses(id: string, subQuestionId?: string): boolean {
        let responseIds: string[];
        let responses: Partial<Map<string, IQuestionResponseV2>>;
        if (subQuestionId) {
            responseIds = getSubQuestionResponseIds(id, subQuestionId)(this.store.getState());
            responses = getSubQuestionResponseModels(id, subQuestionId)(this.store.getState());
        } else {
            responseIds = getQuestionResponseIds(id)(this.store.getState());
            responses = getQuestionResponseModels(id)(this.store.getState());
        }
        for (const responseId of responseIds) {
            if (responses[responseId] && this.isNotSureOrNotApplicableResponse(responses[responseId])) {
                return true;
            }
        }
        return false;
    }

    private isNotSureOrNotApplicableResponse(response: IQuestionResponseV2): boolean {
        return (response.type === QuestionResponseType.NotSure || response.type === QuestionResponseType.NotApplicable);
    }
}
