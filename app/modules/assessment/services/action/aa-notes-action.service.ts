// Angular
import {
    Injectable,
    Inject,
} from "@angular/core";

// Services
import { ModalService } from "sharedServices/modal.service";
import { AssessmentDetailApiService } from "modules/assessment/services/api/assessment-detail-api.service";
import { AaCollaborationPaneCountsService } from "modules/assessment/services/messaging/aa-section-detail-header-helper.service";

// Interfaces
import {
    IProtocolResponse,
} from "interfaces/protocol.interface";
import { IStore } from "interfaces/redux.interface";
import { NotificationService } from "modules/shared/services/provider/notification.service";
import {
    IAssessmentCommentCreationReq,
    IAssessmentCommentModel,
    IAssessmentCommentModaldata,
} from "interfaces/assessment/assessment-comment-model.interface";

// Reducer
import { AssessmentCommentAction } from "modules/assessment/reducers/assessment-comment.reducer";
import { StoreToken } from "tokens/redux-store.token";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

// Enums
import { AACollaborationCountsTypes } from "modules/assessment/enums/aa-detail-header.enum";

@Injectable()
export class AaNotesActionService {

    constructor(
        @Inject(StoreToken) private store: IStore,
        private modalService: ModalService,
        private assessmentDetailApi: AssessmentDetailApiService,
        private aaCollaborationPaneCountsService: AaCollaborationPaneCountsService,
        private notificationService: NotificationService,
        private translatePipe: TranslatePipe,
    ) { }

    updateNote(assessmentId: string, payload: IAssessmentCommentModaldata): ng.IPromise<IAssessmentCommentModel> {
        if (payload.id) {
            return this.assessmentDetailApi.updateNote(assessmentId, payload)
                .then((response: IProtocolResponse<IAssessmentCommentModel>): IAssessmentCommentModel => {
                    if (response.result) {
                        const comment: IAssessmentCommentModel = {
                            commenter: response.data.commenter,
                            commenterId: response.data.commenterId,
                            createdDT: response.data.createdDT,
                            editable: true,
                            id: payload.id,
                            refId: assessmentId,
                            type: payload.type,
                            value: payload.value,
                        };
                        this.store.dispatch({ type: AssessmentCommentAction.UPDATE_COMMENT, comment });
                        return response.data;
                    }
                    return null;
                });
        } else {
            const data: IAssessmentCommentCreationReq = {
                type: payload.type,
                value: payload.value,
            };
            return this.assessmentDetailApi.addNote(assessmentId, data)
                .then((response: IProtocolResponse<IAssessmentCommentModel>): IAssessmentCommentModel => {
                    if (response.result) {
                        const comment: IAssessmentCommentModel = {
                            ...response.data,
                            editable: true,
                        };
                        this.store.dispatch({ type: AssessmentCommentAction.ADD_COMMENT, comment });
                        this.aaCollaborationPaneCountsService.getCollaborationPaneCounts(assessmentId, AACollaborationCountsTypes.Notes);
                        return response.data;
                    } else if (response.status === 423 && response.errors.code === "ERROR_ASSESSMENT-V2_ASSESSMENT_LOCKED") {
                        this.notificationService.alertError(this.translatePipe.transform("Error"), this.translatePipe.transform("AssessmentLockedForPushingTemplate"));
                    }
                    return null;
                });
        }
    }

    openNotesModal(assessmentId: string, modalData: IAssessmentCommentModaldata, modalTitle: string): void {
        this.modalService.setModalData({
            assessmentId,
            modalData,
            modalTitle,
            promiseToResolve: (payload: IAssessmentCommentModaldata): ng.IPromise<IAssessmentCommentModel> => this.updateNote(assessmentId, payload),
        });
        this.modalService.openModal("assessmentDetailNotesModal");
    }

    deleteNotesModal(assessmentId: string, comment: IAssessmentCommentModel): void {
        this.modalService.setModalData({
            modalTitle: this.translatePipe.transform("DeleteNote"),
            confirmationText: this.translatePipe.transform("DeleteNoteWarning"),
            cancelButtonText: this.translatePipe.transform("Cancel"),
            submitButtonText: this.translatePipe.transform("Delete"),
            promiseToResolve: (): ng.IPromise<boolean> =>
                this.assessmentDetailApi.deleteNote(assessmentId, comment.id)
                    .then((response: boolean): boolean => {
                        if (response) {
                            this.notificationService.alertSuccess(
                                this.translatePipe.transform("Success"),
                                this.translatePipe.transform("NoteDeleted"),
                            );
                            this.aaCollaborationPaneCountsService.getCollaborationPaneCounts(assessmentId, AACollaborationCountsTypes.Notes);
                            this.store.dispatch({ type: AssessmentCommentAction.REMOVE_COMMENT, commentId: comment.id });
                        }
                        return response;
                    }),
        });
        this.modalService.openModal("deleteConfirmationModal");
    }

    fetchNotes(assessmentId: string): ng.IPromise<IAssessmentCommentModel[]> {
        return this.assessmentDetailApi.getNotes(assessmentId)
            .then((response: IAssessmentCommentModel[]): IAssessmentCommentModel[] => {
                this.store.dispatch({ type: AssessmentCommentAction.SET_COMMENT, commentModel: response });
                return response;
            });
    }
}
