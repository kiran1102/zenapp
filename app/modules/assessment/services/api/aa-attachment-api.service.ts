
// 3rd party
import { Injectable } from "@angular/core";

// Interfaces
import {
    IProtocolConfig,
    IProtocolResponse,
    IProtocolMessages,
} from "interfaces/protocol.interface";
import {
    IAAAttachmentDetails,
    IAAAttachmentModalResponse,
    IAAAttachmentModalData,
} from "interfaces/assessment/assessment-attachment-modal.interface";

// Services
import { AaProtocolService } from "assessmentServices/api/aa-protocol.service";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

@Injectable()
export class AAAttachmentApiService {
    constructor(
        private translatePipe: TranslatePipe,
        private aaProtocolService: AaProtocolService,
    ) { }

    public addAttachmentToQuestion(assessmentAttachmentDetails: IAAAttachmentDetails): ng.IPromise<IProtocolResponse<null>> {
        const config: IProtocolConfig = this.aaProtocolService.aaConfig("POST", `/assessmentv2/assessments/${assessmentAttachmentDetails.assessmentId}/sections/${assessmentAttachmentDetails.sectionId}/questions/${assessmentAttachmentDetails.questionId}/attachments`, null, assessmentAttachmentDetails);
        const messages: IProtocolMessages = {
            Success: { custom: this.translatePipe.transform("SuccessfullyAddedResponse") },
            Error: { custom: this.translatePipe.transform("CouldNotAddResponse") },
        };
        return this.aaProtocolService.aaRequestHandler(config, messages);
    }

    public getQuestionAttachments(assessmentId: string, sectionId: string, questionId: string): ng.IPromise<IProtocolResponse<IAAAttachmentModalResponse[]>> {
        const config: IProtocolConfig = this.aaProtocolService.aaConfig("GET", `/assessmentv2/assessments/${assessmentId}/sections/${sectionId}/questions/${questionId}/attachments`, null);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("ErrorGettingAttachments") } };
        return this.aaProtocolService.aaRequestHandler(config, messages);
    }

    public deleteAttachment(assessmentData: IAAAttachmentModalData, attachmentId: string): ng.IPromise<IProtocolResponse<null>> {
        const { assessmentId, questionId, sectionId } = assessmentData;
        const config: IProtocolConfig = this.aaProtocolService.aaConfig("DELETE", `/assessmentv2/assessments/${assessmentId}/sections/${sectionId}/questions/${questionId}/attachments/${attachmentId}`);
        const messages: IProtocolMessages = {
            Success: { custom: this.translatePipe.transform("SuccessfullyDeletedAttachment") },
            Error: { custom: this.translatePipe.transform("ErrorDeletingAttachment") },
        };
        return this.aaProtocolService.aaRequestHandler(config, messages);
    }
}
