// Angular
import {
    Injectable,
    Inject,
} from "@angular/core";

// Services
import { AaProtocolService } from "assessmentServices/api/aa-protocol.service";

// Redux
import { getAssessmentDetailModel } from "modules/assessment/reducers/assessment-detail.reducer";
import { IStore } from "interfaces/redux.interface";
import { StoreToken } from "tokens/redux-store.token";

// Interfaces
import {
    IProtocolConfig,
    IProtocolMessages,
    IProtocolResponse,
} from "interfaces/protocol.interface";
import { IPromise } from "angular";
import {
    IChangedQuestionResponse,
    IChangedResponse,
} from "interfaces/assessment/assessment-model.interface";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

@Injectable()
export class AaChangedResponsesService {

    constructor(
        @Inject(StoreToken) private store: IStore,
        private translatePipe: TranslatePipe,
        private aaProtocolService: AaProtocolService,
    ) { }

    getChangedQuestions(): IPromise<IProtocolResponse<IChangedQuestionResponse>> {
        const assessmentId = getAssessmentDetailModel(this.store.getState()).assessmentId;
        const config: IProtocolConfig = this.aaProtocolService.aaCustomConfig("GET", `/assessment-v2/v2/assessments/${assessmentId}/changed-responses`);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("ErrorUpdatingAssessment") } };
        return this.aaProtocolService.aaRequestHandler(config, messages);
    }

    getResponseDiff(questionId: string): IPromise<IProtocolResponse<IChangedResponse>> {
        const assessmentId = getAssessmentDetailModel(this.store.getState()).assessmentId;
        const config: IProtocolConfig = this.aaProtocolService.aaCustomConfig("GET", `/assessment-v2/v2/assessments/${assessmentId}/changed-responses/${questionId}`);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("ErrorUpdatingAssessment") } };
        return this.aaProtocolService.aaRequestHandler(config, messages);
    }
}
