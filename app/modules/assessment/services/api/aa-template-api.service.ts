// Core
import { Injectable, Inject } from "@angular/core";

// Services
import { AaProtocolService } from "assessmentServices/api/aa-protocol.service";

// Interfaces
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import { IAssessmentCreationDetails } from "interfaces/assessment/assessment-creation-details.interface";
import { ICustomTemplateDetails } from "modules/template/interfaces/custom-templates.interface";
import { IStringMap } from "interfaces/generic.interface";
import {
    IProtocolConfig,
    IProtocolMessages,
    IProtocolResponse,
} from "interfaces/protocol.interface";
import { IAssessmentExportModel } from "interfaces/assessment/assessment.interface";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

@Injectable()
export class AATemplateApiService {

    private projectCustomTerm: any;
    private translations: IStringMap<string>;

    constructor(
        @Inject("$rootScope") private $rootScope: IExtendedRootScopeService,
        private translatePipe: TranslatePipe,
        private aaProtocolService: AaProtocolService,
    ) {
        this.projectCustomTerm = { Project: $rootScope.customTerms.Projects };
        this.translations = {
            errorRetrievingProject: translatePipe.transform("ErrorRetrievingProject", this.projectCustomTerm),
        };
    }

    createAssessment(assessment: IAssessmentCreationDetails): ng.IPromise<IProtocolResponse<string>> {
        const config: IProtocolConfig = this.aaProtocolService.aaConfig("POST", "/assessmentv2/createassessments", null, assessment);
        const messages: IProtocolMessages = { Error: { Hide: true } };
        return this.aaProtocolService.aaRequestHandler(config, messages);
    }

    createAssessmentsBulk(assessmentList: IAssessmentCreationDetails[]): ng.IPromise<IProtocolResponse<string>> {
        const config: IProtocolConfig = this.aaProtocolService.aaConfig("POST", "/assessmentv2/createassessments/bulk", null, assessmentList);
        const messages: IProtocolMessages = {
            Success: { custom: this.translatePipe.transform("SuccessfullyCreatedAssessments") },
            Error: { custom: this.translatePipe.transform("ErrorCreatingAssessments") },
        };
        return this.aaProtocolService.aaRequestHandler(config, messages);
    }

    getAssessmentTemplates = (types: string, state: string, activeCriteria?: string, assessmentCreationAllowed?: boolean): ng.IPromise<IProtocolResponse<ICustomTemplateDetails[]>> => {
        const config: IProtocolConfig = this.aaProtocolService.aaCustomConfig("GET", "/api/template/v1/templates", { types, state, activeCriteria, assessmentCreationAllowed });
        const messages: IProtocolMessages = { Error: { custom: this.translations.errorRetrievingProject } };
        return this.aaProtocolService.aaRequestHandler(config, messages);
    }

    sendBackAssessment(assessmentId: string, comments: string, sendBackType: string, editAllResponses: boolean, sendBulkInfoRequestEmail: boolean): ng.IPromise<IProtocolResponse<void>> {
        const config: IProtocolConfig = this.aaProtocolService.aaConfig("POST", `/assessmentv2/SendBackAssessment`, { assessmentId }, {
            comment: comments,
            sendBackType,
            editAllResponses,
            sendBulkInfoRequestEmail,
        });
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("CouldNotSendBackAssessment") } };
        return this.aaProtocolService.aaRequestHandler(config, messages);
    }

    deleteAssessment(assessmentId: string): ng.IPromise<IProtocolResponse<boolean>> {
        const config: IProtocolConfig = this.aaProtocolService.aaConfig("DELETE", `/assessmentv2/assessments/${assessmentId}`);
        const messages: IProtocolMessages = { Error: { Hide: true } };
        return this.aaProtocolService.aaRequestHandler(config, messages);
    }

    exportAsPdf(requestBody: IAssessmentExportModel): ng.IPromise<IProtocolResponse<any>> {
        const config: IProtocolConfig = this.aaProtocolService.aaCustomConfig("PUT", `/reporting/async/export/entity`, [], requestBody, null, null, null, "text");
        const messages: IProtocolMessages = { Error: { Hide: true } };
        return this.aaProtocolService.aaRequestHandler(config, messages);
    }

    getResendLinkToAssessment(assessmentId: string): ng.IPromise<IProtocolResponse<boolean>> {
        const config: IProtocolConfig = this.aaProtocolService.aaConfig("POST", `/assessmentv2/assessments/${assessmentId}/reminder/send`);
        const messages: IProtocolMessages = { Error: { Hide: true } };
        return this.aaProtocolService.aaRequestHandler(config, messages);
    }
}
