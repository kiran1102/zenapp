// Angular
import {
    Injectable,
    Inject,
} from "@angular/core";

// Interfaces
import {
    IProtocolConfig,
    IProtocolResponse,
    IProtocolMessages,
} from "interfaces/protocol.interface";
import {
    IRiskDetails,
    IRiskMetadata,
    IRiskStatistics,
    IRiskUpdateRequest,
    IRiskCreateRequestModel,
} from "interfaces/risk.interface";
import { IQuestionHavingRisk } from "interfaces/assessment/question-model.interface";

// Redux
import { IStore } from "interfaces/redux.interface";
import { StoreToken } from "tokens/redux-store.token";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

// Services
import { AaProtocolService } from "assessmentServices/api/aa-protocol.service";

// Types
import { RiskV2UpdateActions } from "modules/assessment/types/risk-v2.types";

@Injectable()
export class AssessmentRiskApiService {

    constructor(
        @Inject(StoreToken) private store: IStore,
        private translatePipe: TranslatePipe,
        private aaProtocolService: AaProtocolService,
    ) { }

    public fetchRiskStatistics(assessmentId: string, sectionId: string): ng.IPromise<IProtocolResponse<IRiskStatistics>> {
        const config: IProtocolConfig = this.aaProtocolService.aaConfig("GET", `/assessmentv2/risks/assessments/${assessmentId}/sections/${sectionId}/statistics`);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("CouldNotRetrieveRiskStatistics") } };
        return this.aaProtocolService.aaRequestHandler(config, messages);
    }

    public clearRecommendation(riskId: string, assessmentId: string): ng.IPromise<IProtocolResponse<IRiskMetadata>> {
        const config: IProtocolConfig = this.aaProtocolService.aaConfig("POST", `/assessmentv2/risks/assessments/${assessmentId}/risks/${riskId}/recommendation/remove`, null);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("CouldNotUpdateRisk") } };
        return this.aaProtocolService.aaRequestHandler(config, messages);
    }

    public createRisk(risk: IRiskCreateRequestModel, assessmentId: string): ng.IPromise<IProtocolResponse<IRiskMetadata>> {
        const config: IProtocolConfig = this.aaProtocolService.aaConfig("POST", "/assessmentv2/createassessmentrisk", { assessmentId }, risk);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("CouldNotUpdateRisk") } };
        return this.aaProtocolService.aaRequestHandler(config, messages);
    }

    public updateRisk(risk: IRiskUpdateRequest, riskId: string, assessmentId: string): ng.IPromise<IProtocolResponse<IRiskMetadata>> {
        const config: IProtocolConfig = this.aaProtocolService.aaConfig("PUT", `/assessmentv2/assessments/${assessmentId}/risks/${riskId}`, null, risk);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("CouldNotUpdateRisk") } };
        return this.aaProtocolService.aaRequestHandler(config, messages);
    }

    public performRiskAction(riskId: string, action: RiskV2UpdateActions): ng.IPromise<IProtocolResponse<IRiskDetails>> {
        const config: IProtocolConfig = this.aaProtocolService.aaCustomConfig("PUT", `/assessment-v2/v2/risks/${riskId}/perform/${action}`);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("CouldNotUpdateRisk") } };
        return this.aaProtocolService.aaRequestHandler(config, messages);
    }

    public deleteAssessmentRisk = (riskId: string, assessmentId: string): ng.IPromise<IProtocolResponse<void>> => {
        const config: IProtocolConfig = this.aaProtocolService.aaConfig("DELETE", "/assessmentv2/deleteassessmentrisk", { assessmentId, riskId });
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("CouldNotDeleteRisk") } };
        return this.aaProtocolService.aaRequestHandler(config, messages);
    }

    public deleteAssessmentRisks = (riskIds: string[], assessmentId: string): ng.IPromise<IProtocolResponse<void>> => {
        const config: IProtocolConfig = this.aaProtocolService.aaCustomConfig("DELETE", `/assessment-v2/v2/risks/assessments/${assessmentId}/risks`, "", riskIds);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("CouldNotDeleteRisk") } };
        return this.aaProtocolService.aaRequestHandler(config, messages);
    }

    public getAssessmentRiskQuestion(riskId: string, assessmentId: string): ng.IPromise<IProtocolResponse<IQuestionHavingRisk>> {
        const config: IProtocolConfig = this.aaProtocolService.aaConfig("GET", `/assessmentv2/risks/assessments/${assessmentId}/risks/${riskId}/question`);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("CouldNotRetrieveQuestion") } };
        return this.aaProtocolService.aaRequestHandler(config, messages);
    }
}
