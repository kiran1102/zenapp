// 3rd party
import { Injectable } from "@angular/core";

// Interfaces
import {
    IProtocolConfig,
    IProtocolResponse,
    IProtocolMessages,
} from "interfaces/protocol.interface";
import { IQuestionActivityContent } from "interfaces/assessment/assessment-question-activity.interface";
import {
    IPageable,
    IPageableOf,
} from "interfaces/pagination.interface";
import { IQuestionActivityParams } from "interfaces/assessment/assessment-question-activity.interface";
import { IAssessmentActivityParams } from "interfaces/assessment/aa-assessment-activity.interface";

// Services
import { AaProtocolService } from "assessmentServices/api/aa-protocol.service";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

@Injectable()
export class AAActivityStreamApiService {
    constructor(
        private translatePipe: TranslatePipe,
        private aaProtocolService: AaProtocolService,
    ) {}

    getActivityData = (recordId: string, defaultConfig: IAssessmentActivityParams): ng.IPromise<IProtocolResponse<IPageable>> => {
        const config: IProtocolConfig = this.aaProtocolService.aaCustomConfig("GET", `/assessment-v2/v2/assessments/${recordId}/activities`, defaultConfig);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("ErrorRetrievingActivity") } };
        return this.aaProtocolService.aaRequestHandler(config, messages);
    }

    getQuestionActivityStream(assessmentId: string, questionId: string, params: IQuestionActivityParams): ng.IPromise<IProtocolResponse<IPageableOf<IQuestionActivityContent>>> {
        const config: IProtocolConfig = this.aaProtocolService.aaConfig("GET", `/assessmentv2/assessments/${assessmentId}/questions/${questionId}/activities`, params);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("CouldNotGetActivityStream") } };
        return this.aaProtocolService.aaRequestHandler(config, messages);
    }
}
