// Angular
import { Injectable } from "@angular/core";

// Interfaces
import {
    IQuestionCommentCreateRequest,
    ICommentCollaborationPaneResponse,
} from "interfaces/aa-question-comment.interface";
import {
    IProtocolConfig,
    IProtocolMessages,
    IProtocolResponse,
} from "interfaces/protocol.interface";
import { IStringMap } from "interfaces/generic.interface";

// Service
import { AaProtocolService } from "assessmentServices/api/aa-protocol.service";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

@Injectable()
export class AACommentApiService {

    constructor(
        private translatePipe: TranslatePipe,
        private aaProtocolService: AaProtocolService,
    ) { }

    getCommentsByQuestionId(assessmentId: string, sectionId: string, questionId: string): any {
        const requestData: IStringMap<string> = {
            assessmentId,
            sectionId,
            questionId,
        };
        const config: IProtocolConfig = this.aaProtocolService.aaCustomConfig("POST", `/assessment-v2/v2/comments/search`, null, requestData);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("CouldNotGetComment") } };
        return this.aaProtocolService.aaRequestHandler(config, messages);
    }

    addcomment(commentRequest: IQuestionCommentCreateRequest): ng.IPromise<IProtocolResponse<string>> {
        const config: IProtocolConfig = this.aaProtocolService.aaCustomConfig("POST", `/assessment-v2/v2/comments`, null, commentRequest);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("CouldNotCommentOnQuestion") } };
        return this.aaProtocolService.aaRequestHandler(config, messages);
    }

    getAllAssessmentComments(assessmentId: string, params: IStringMap<number>): ng.IPromise<IProtocolResponse<ICommentCollaborationPaneResponse>> {
        const config: IProtocolConfig = this.aaProtocolService.aaCustomConfig("GET", `/assessment-v2/v2/comments/assessment/${assessmentId}`, params);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("CouldNotGetComment") } };
        return this.aaProtocolService.aaRequestHandler(config, messages);
    }
}
