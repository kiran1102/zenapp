// 3rd party
import { Injectable } from "@angular/core";

// Interfaces
import {
    IProtocolConfig,
    IProtocolResponse,
    IProtocolMessages,
} from "interfaces/protocol.interface";
import {
    IAAAttachmentDetails,
    IAAAttachmentModalResponse,
    IAAAttachmentModalData,
} from "interfaces/assessment/assessment-attachment-modal.interface";

// Services
import { AaProtocolService } from "assessmentServices/api/aa-protocol.service";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";
import { Observable } from "rxjs/Observable";
import { from } from "rxjs/internal/observable/from";
import { AaSectionNavMessagingService } from "../messaging/aa-section-messaging.service";

@Injectable()
export class AASectionApiService {
    constructor(
        private aaProtocolService: AaProtocolService,
        private aaSectionNavMessagingService: AaSectionNavMessagingService,
    ) { }

    // TODO - as part of SectionNav epic
}
