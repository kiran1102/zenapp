// External library
import { Injectable } from "@angular/core";
import { TranslatePipe } from "modules/pipes/translate.pipe";

// Services
import { AaProtocolService } from "assessmentServices/api/aa-protocol.service";

// Interfaces
import {
    IProtocolResponse,
    IProtocolConfig,
    IProtocolMessages,
} from "interfaces/protocol.interface";
import {
    IUserRequestParams,
    ISharedAssessmentUsersContractById,
} from "interfaces/user.interface";

@Injectable()
export class ShareAssessmentService {
    constructor(
        private translatePipe: TranslatePipe,
        private aaProtocolService: AaProtocolService,
    ) { }

    shareAssessmentToUsers(assessmentId: string, userRequestParams: IUserRequestParams[]): ng.IPromise<IProtocolResponse<null>> {
        const config: IProtocolConfig = this.aaProtocolService.aaCustomConfig("PUT", `/assessment-v2/v2/assessments/${assessmentId}/viewers`, null, userRequestParams);
        const messages: IProtocolMessages = {
            Error: { custom: this.translatePipe.transform("CouldNotShareAnAssessment") },
            Success: { custom: this.translatePipe.transform("ShareAssessmentSuccessful") },
        };
        return this.aaProtocolService.aaRequestHandler(config, messages);
    }

    getAllSharedAssessmentUsers(assessmentId: string): ng.IPromise<IProtocolResponse<ISharedAssessmentUsersContractById>> {
        const config: IProtocolConfig = this.aaProtocolService.aaCustomConfig("GET", `/assessment-v2/v2/assessments/${assessmentId}/viewers`);
        const messages: IProtocolMessages = {
            Error: { custom: this.translatePipe.transform("CouldNotRetrieveUsers") },
        };
        return this.aaProtocolService.aaRequestHandler(config, messages);
    }
}
