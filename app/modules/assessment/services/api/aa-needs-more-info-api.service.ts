
// 3rd party
import { Injectable } from "@angular/core";

// Interfaces
import {
    INeedsMoreInfoModel,
    INeedsMoreInfoCreationDetails,
    INeedsMoreInfoApiResponse,
    INeedsMoreInfoResponse,
} from "interfaces/assessment/needs-more-info.interface";
import {
    IProtocolConfig,
    IProtocolResponse,
    IProtocolMessages,
} from "interfaces/protocol.interface";

// Services
import { AaProtocolService } from "assessmentServices/api/aa-protocol.service";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

@Injectable()
export class AANeedsMoreInfoApiService {
    constructor(
        private translatePipe: TranslatePipe,
        private aaProtocolService: AaProtocolService,
    ) { }

    public getAllAssessmentRequestDetails(assessmentId: string): ng.IPromise<IProtocolResponse<INeedsMoreInfoModel>> {
        const config: IProtocolConfig = this.aaProtocolService.aaCustomConfig("GET", `/assessment-v2/v2/assessments/${assessmentId}/inforequests/details`);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("CouldNotRetrieveInfoRequest") } };
        return this.aaProtocolService.aaRequestHandler(config, messages);
    }

    public getAllAssessmentRequestResponses(assessmentId: string, rootRequestId: string): ng.IPromise<IProtocolResponse<INeedsMoreInfoResponse[]>> {
        const config: IProtocolConfig = this.aaProtocolService.aaConfig("GET", `/assessmentv2/assessments/${assessmentId}/inforequests/${rootRequestId}/responses`);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("CouldNotRetrieveInfoResponses") } };
        return this.aaProtocolService.aaRequestHandler(config, messages);
    }

    public addAssessmentRequest(needsMoreInfoCreationDetails: INeedsMoreInfoCreationDetails): ng.IPromise<IProtocolResponse<INeedsMoreInfoApiResponse>> {
        const config: IProtocolConfig = this.aaProtocolService.aaConfig("POST", `/assessmentv2/assessments/${needsMoreInfoCreationDetails.assessmentId}/inforequests`,
            null, needsMoreInfoCreationDetails);
        const messages: IProtocolMessages = {
            Success: { custom: this.translatePipe.transform("SuccessfullyAddedInfoRequest") },
            Error: { custom: this.translatePipe.transform("CouldNotAddInfoRequest") },
        };
        return this.aaProtocolService.aaRequestHandler(config, messages);
    }

    public addAssessmentResponse(needsMoreInfoCreationDetails: INeedsMoreInfoCreationDetails, rootRequestId: string): ng.IPromise<IProtocolResponse<INeedsMoreInfoApiResponse>> {
        const config: IProtocolConfig = this.aaProtocolService.aaConfig("POST", `/assessmentv2/assessments/${needsMoreInfoCreationDetails.assessmentId}/infoRequests/${rootRequestId}/responses`,
            null, needsMoreInfoCreationDetails);
        const messages: IProtocolMessages = {
            Success: { custom: this.translatePipe.transform("SuccessfullyAddedResponse") },
            Error: { custom: this.translatePipe.transform("CouldNotAddResponse") },
        };
        return this.aaProtocolService.aaRequestHandler(config, messages);
    }

    public reOpenNeedsMoreInfoRequest(needsMoreInfoCreationDetails: INeedsMoreInfoCreationDetails, rootRequestId: string): ng.IPromise<IProtocolResponse<INeedsMoreInfoApiResponse>> {
        const config: IProtocolConfig = this.aaProtocolService.aaConfig("PUT", `/assessmentv2/assessments/${needsMoreInfoCreationDetails.assessmentId}/infoRequests/${rootRequestId}/reopen`,
            null, needsMoreInfoCreationDetails);
        const messages: IProtocolMessages = {
            Success: { custom: this.translatePipe.transform("SuccessfullyReOpenedRequest") },
            Error: { custom: this.translatePipe.transform("CouldNotReOpenRequest") },
        };
        return this.aaProtocolService.aaRequestHandler(config, messages);
    }

    public closeNeedsMoreInfoRequest(assessmentId: string, rootRequestId: string): ng.IPromise<IProtocolResponse<INeedsMoreInfoApiResponse>> {
        const config: IProtocolConfig = this.aaProtocolService.aaConfig("PUT", `/assessmentv2/assessments/${assessmentId}/infoRequests/${rootRequestId}/close`,
            null);
        const messages: IProtocolMessages = {
            Success: { custom: this.translatePipe.transform("SuccessfullyClosedRequest") },
            Error: { custom: this.translatePipe.transform("CouldNotCloseRequest") },
        };
        return this.aaProtocolService.aaRequestHandler(config, messages);
    }

    public deleteNeedsMoreInfoRequest(assessmentId: string, rootRequestId: string): ng.IPromise<IProtocolResponse<void>> {
        const config: IProtocolConfig = this.aaProtocolService.aaConfig("DELETE", `/assessmentv2/assessments/${assessmentId}/infoRequests/${rootRequestId}`);
        const messages: IProtocolMessages = {
            Success: { custom: this.translatePipe.transform("RequestSuccessfullyDeleted") },
            Error: { custom: this.translatePipe.transform("CouldNotDeleteNeedsMoreInfo") },
        };
        return this.aaProtocolService.aaRequestHandler(config, messages);
    }
}
