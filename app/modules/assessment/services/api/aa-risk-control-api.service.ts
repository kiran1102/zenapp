// Angular
import { Injectable } from "@angular/core";

// RxJs
import {
    Observable,
    from,
} from "rxjs";

// Interfaces
import {
    IProtocolConfig,
    IProtocolMessages,
    IProtocolResponse,
} from "interfaces/protocol.interface";
import { IRiskControlInformation } from "modules/assessment/interfaces/aa-risk-control.interface";
import { IGridPaginationParams } from "interfaces/grid.interface";
import { IPageableOf } from "interfaces/pagination.interface";

// Enums
import { RiskControlStatusNames } from "modules/assessment/enums/aa-risk-control.enum";

// Service
import { AaProtocolService } from "assessmentServices/api/aa-protocol.service";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

@Injectable()
export class AARiskControlApiService {

    constructor(
        private translatePipe: TranslatePipe,
        private aaProtocolService: AaProtocolService,
    ) { }

    addRiskControl(riskId: string, controlIds: string[]): Observable<IProtocolResponse<void>> {
        const config: IProtocolConfig = this.aaProtocolService.aaCustomConfig("POST", `/api/risk-v2/v2/risks/${riskId}/controls`, null, { controlIds });
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("CouldNotAddRiskControl") }};
        return from(this.aaProtocolService.aaRequestHandler(config, messages));
    }

    removeRiskControl(riskControlId: string): Observable<boolean> {
        const config: IProtocolConfig = this.aaProtocolService.aaCustomConfig("DELETE", `/api/risk-v2/v2/risks/controls/${riskControlId}`);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("CouldNotRemoveRiskControl") }};
        return from(this.aaProtocolService.aaRequestHandler(config, messages)
            .then((response: IProtocolResponse<void>) => response.result));
    }

    updateRiskControlStatus(riskControlId: string, state: RiskControlStatusNames): Observable<boolean> {
        const config: IProtocolConfig = this.aaProtocolService.aaCustomConfig("PUT", `/api/risk-v2/v2/risks/controls/${riskControlId}/status`, null, `"${state}"`, { "Content-Type": "application/json"});
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("CouldNotUpdateRiskControl") }};
        return from(this.aaProtocolService.aaRequestHandler(config, messages)
            .then((response: IProtocolResponse<void>) => response.result));
    }

    fetchRiskControlByRiskId(riskId: string, param: IGridPaginationParams): Observable<IProtocolResponse<IPageableOf<IRiskControlInformation>>> {
        const requestBody = { fullText: "" };
        const config: IProtocolConfig = this.aaProtocolService.aaCustomConfig("POST", `/api/risk-v2/v2/risks/${riskId}/controls/pages/`, param, requestBody);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("CouldNotFetchRiskControl") }};
        return from(this.aaProtocolService.aaRequestHandler(config, messages));
    }
}
