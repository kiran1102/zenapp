// Angular
import { Injectable } from "@angular/core";

// Rxjs
import {
    Observable,
    from,
    Subject,
} from "rxjs";
import { takeUntil } from "rxjs/operators";

// constants
import { AssessmentSectionNavFilters } from "constants/assessment.constants";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

// Services
import { AssessmentDetailAdapterService } from "modules/assessment/services/adapter/assessment-detail-adapter.service";
import { AaProtocolService } from "assessmentServices/api/aa-protocol.service";
import { AaSectionNavMessagingService } from "assessmentServices/messaging/aa-section-messaging.service";

// Interfaces
import {
    IAssessmentModel,
    IAssessmentCopyPayload,
    IAsessmentCopyContent,
    IUpdatedTemplateQuestionsResponse,
} from "interfaces/assessment/assessment-model.interface";
import {
    InventoryOptionsResponse,
    IAssessmentMetadataRequest,
    ISaveSectionResponsesContract,
} from "interfaces/assessment/assessment-api.interface";
import {
    IProtocolConfig,
    IProtocolMessages,
    IProtocolResponse,
} from "interfaces/protocol.interface";
import { IAssessmentTableRow } from "interfaces/assessment/assessment.interface";
import { IQuestionModelOption } from "interfaces/assessment/question-model.interface";
import {
    IAssessmentCommentModel,
    IAssessmentCommentCreationReq,
    IAssessmentCommentModaldata,
} from "interfaces/assessment/assessment-comment-model.interface";
import {
    IAssessmentTemplate,
    IAssessmentNeedsConfirmation,
} from "modules/template/interfaces/template.interface";
import { IAssessmentValidApproverRespondentResolve } from "interfaces/assessment/aa-metadata-modal.interfaces";
import { ILabelValue } from "interfaces/generic.interface";

// Enums
import { AASectionFilterTypes } from "modules/assessment/enums/aa-filter.enum";

@Injectable()
export class AssessmentDetailApiService {
    constructor(
        private assessmentDetailAdapter: AssessmentDetailAdapterService,
        private translate: TranslatePipe,
        private aaProtocolService: AaProtocolService,
        private aaSectionNavMessagingService: AaSectionNavMessagingService,
    ) { }

    fetchCurrentSection(assessmentId: string, sectionId: string, filter: string = AssessmentSectionNavFilters.AllQuestions): ng.IPromise<IAssessmentModel | boolean> {
        filter = this.aaSectionNavMessagingService.sectionFilter ? this.convertAppliedFilterValue(this.aaSectionNavMessagingService.sectionFilter) : filter;
        const config: IProtocolConfig = this.aaProtocolService.aaConfig("GET", "/assessmentv2/currentsection", { assessmentId, sectionId, filter });
        const messages: IProtocolMessages = { Error: { custom: this.translate.transform("CouldNotRetrieveSection") } };
        return this.aaProtocolService.aaRequestHandler(config, messages).then((res: IProtocolResponse<IAssessmentModel>): IAssessmentModel | boolean => {
            if (res.result) {
                return this.assessmentDetailAdapter.convertAssessmentStructure(res.data);
            }
            return false;
        });
    }

    fetchFirstSection = (assessmentId: string, filter: string = AssessmentSectionNavFilters.AllQuestions): ng.IPromise<IAssessmentModel | boolean> => {
        filter = (Object.values(AssessmentSectionNavFilters).includes(filter)) ? filter : AssessmentSectionNavFilters.AllQuestions;
        filter = this.aaSectionNavMessagingService.sectionFilter ? this.convertAppliedFilterValue(this.aaSectionNavMessagingService.sectionFilter) : filter;
        const config: IProtocolConfig = this.aaProtocolService.aaConfig("GET", "/assessmentv2/firstsection", { assessmentId, filter });
        const messages: IProtocolMessages = { Error: { custom: this.translate.transform("CouldNotRetrieveSection") } };
        return this.aaProtocolService.aaRequestHandler(config, messages).then((res: IProtocolResponse<IAssessmentModel>): IAssessmentModel | boolean => {
            if (res.result) {
                return this.assessmentDetailAdapter.convertAssessmentStructure(res.data);
            }
            return false;
        });
    }

    fetchNextSection(assessmentId: string, sectionId: string, filter: string = AssessmentSectionNavFilters.AllQuestions): ng.IPromise<IAssessmentModel | boolean> {
        filter = this.aaSectionNavMessagingService.sectionFilter ? this.convertAppliedFilterValue(this.aaSectionNavMessagingService.sectionFilter) : filter;
        const config: IProtocolConfig = this.aaProtocolService.aaConfig("GET", "/assessmentv2/nextsection", { assessmentId, sectionId, filter });
        const messages: IProtocolMessages = { Error: { custom: this.translate.transform("CouldNotRetrieveSection") } };
        return this.aaProtocolService.aaRequestHandler(config, messages).then((res: IProtocolResponse<IAssessmentModel>): IAssessmentModel | boolean => {
            if (res.result) {
                return this.assessmentDetailAdapter.convertAssessmentStructure(res.data);
            }
            return false;
        });
    }

    fetchPreviousSection(assessmentId: string, sectionId: string, filter: string = AssessmentSectionNavFilters.AllQuestions): ng.IPromise<IAssessmentModel | boolean> {
        filter = this.aaSectionNavMessagingService.sectionFilter ? this.convertAppliedFilterValue(this.aaSectionNavMessagingService.sectionFilter) : filter;
        const config: IProtocolConfig = this.aaProtocolService.aaConfig("GET", "/assessmentv2/previoussection", { assessmentId, sectionId, filter });
        const messages: IProtocolMessages = { Error: { custom: this.translate.transform("CouldNotRetrieveSection") } };
        return this.aaProtocolService.aaRequestHandler(config, messages).then((res: IProtocolResponse<IAssessmentModel>): IAssessmentModel | boolean => {
            if (res.result) {
                return this.assessmentDetailAdapter.convertAssessmentStructure(res.data);
            }
            return false;
        });
    }

    fetchAssessmentWelcomeSection(assessmentId: string, filter: string = AssessmentSectionNavFilters.AllQuestions): ng.IPromise<IAssessmentModel | boolean> {
        filter = this.aaSectionNavMessagingService.sectionFilter ? this.convertAppliedFilterValue(this.aaSectionNavMessagingService.sectionFilter) : filter;
        const config: IProtocolConfig = this.aaProtocolService.aaConfig("GET", "/assessmentv2/welcomesectionforassessment", { assessmentId, filter });
        const messages: IProtocolMessages = { Error: { custom: this.translate.transform("CouldNotRetrieveSection") } };
        return this.aaProtocolService.aaRequestHandler(config, messages).then((res: IProtocolResponse<IAssessmentModel>): IAssessmentModel | boolean => {
            if (res.result) {
                return this.assessmentDetailAdapter.convertAssessmentStructure(res.data);
            }
            return false;
        });
    }

    fetchAssessmentTemplateMetadata(assessmentId: string): ng.IPromise<IAssessmentTemplate> {
        const config: IProtocolConfig = this.aaProtocolService.aaCustomConfig("GET", `/assessment-v2/v2/assessments/${assessmentId}/template`);
        const messages: IProtocolMessages = { Error: { custom: this.translate.transform("CouldNotRetrieveSection") } };
        return this.aaProtocolService.aaRequestHandler(config, messages).then((res: IProtocolResponse<IAssessmentTemplate>): IAssessmentTemplate => {
            return res.result ? res.data : null;
        });
    }

    isSendAssessmentUponApprovalRuleSet(assessmentId: string, forceOverride: boolean): ng.IPromise<IProtocolResponse<IAssessmentNeedsConfirmation>> {
        const config: IProtocolConfig = this.aaProtocolService.aaCustomConfig("GET", `/assessment-v2/v2/assessments/${assessmentId}/preApprovalConfirmationCheck`, { forceOverride });
        const messages: IProtocolMessages = { Error: { Hide: true } };
        return this.aaProtocolService.aaRequestHandler(config, messages);
    }

    updateAssessmentMetadata(assessmentId: string, metadata: IAssessmentMetadataRequest): ng.IPromise<IProtocolResponse<null>> {
        const config: IProtocolConfig = this.aaProtocolService.aaConfig("PUT", "/assessmentv2/updateassessmentmetadata", { assessmentId }, metadata);
        const messages: IProtocolMessages = { Error: { Hide: true } };
        return this.aaProtocolService.aaRequestHandler(config, messages);
    }

    saveSectionResponses(assessmentId: string, responses: ISaveSectionResponsesContract[], filter: string = AssessmentSectionNavFilters.AllQuestions): ng.IPromise<IProtocolResponse<IAssessmentModel>> {
        filter = this.aaSectionNavMessagingService.sectionFilter ? this.convertAppliedFilterValue(this.aaSectionNavMessagingService.sectionFilter) : filter;
        const config: IProtocolConfig = this.aaProtocolService.aaConfig("POST", "/assessmentv2/SubmitAnswer", { assessmentId, filter }, responses);
        const messages: IProtocolMessages = { Error: { custom: this.translate.transform("ErrorUpdatingAssessment") } };
        return this.aaProtocolService.aaRequestHandler(config, messages);
    }

    submitAssessment(assessmentId: string): ng.IPromise<IProtocolResponse<null>> {
        const config: IProtocolConfig = this.aaProtocolService.aaConfig("POST", "/assessmentv2/SubmitAssessment", { assessmentId });
        const messages: IProtocolMessages = { Error: { Hide: true } };
        return this.aaProtocolService.aaRequestHandler(config, messages);
    }
    // return should be object with 2 string arrays
    validateEntities(assessmentId: string, orgGroupId: string): Observable<IProtocolResponse<IAssessmentValidApproverRespondentResolve>> {
        const config: IProtocolConfig = this.aaProtocolService.aaCustomConfig("GET", `/assessment-v2/v2/assessments/${assessmentId}/orgGroup/${orgGroupId}/validateEntitiesForOrgChange`); // could be GET
        const messages: IProtocolMessages = { Error: { Hide: true } };
        return from(this.aaProtocolService.aaRequestHandler(config, messages));
    }

    approveAssessment(assessmentId: string, forceOverride?: boolean, excludeRules?: boolean, emailReviewComment: string = ""): ng.IPromise<IProtocolResponse<null>> {
        const payload = {
            assessmentId,
            forceOverride: forceOverride || null,
            excludeRules,
            emailReviewComment,
        };
        const config: IProtocolConfig = this.aaProtocolService.aaConfig("POST", "/assessmentv2/ApproveAssessment", payload);
        const messages: IProtocolMessages = { Error: { Hide: true } };
        return this.aaProtocolService.aaRequestHandler(config, messages);
    }

    undoReviewAssessment(assessmentId: string): Observable<IProtocolResponse<null>> {
        const config: IProtocolConfig = this.aaProtocolService.aaCustomConfig("POST", `/assessment-v2/v2/assessments/${assessmentId}/withdraw-vote`);
        const messages: IProtocolMessages = { Error: { Hide: true } };
        return from(this.aaProtocolService.aaRequestHandler(config, messages));
    }

    fetchInventoryQuestionOptions(url: string, type: string, assessmentOrgId?: string, search: string = ""): ng.IPromise<IQuestionModelOption[]> {
        const config: IProtocolConfig = this.aaProtocolService.aaCustomConfig("GET", url, { orgGroupId: assessmentOrgId, search });
        const messages: IProtocolMessages = { Error: { custom: this.translate.transform("Error.GetInventoryOptions") } };
        return this.aaProtocolService.aaRequestHandler(config, messages).then((res: IProtocolResponse<InventoryOptionsResponse[]>) => {
            if (res.result) {
                return this.assessmentDetailAdapter.mapInventoryOptions(type, res.data);
            }
        });
    }

    validateAssessment(assessmentId: string): ng.IPromise<boolean> {
        const config: IProtocolConfig = this.aaProtocolService.aaConfig("GET", "/assessmentv2/validateaccess", { assessmentId });
        const messages: IProtocolMessages = { Error: { Hide: true } };
        return this.aaProtocolService.aaRequestHandler(config, messages)
            .then((res: IProtocolResponse<IAssessmentTableRow>): boolean => {
                return res.result;
            });
    }

    addNote(assessmentId: string, payload: IAssessmentCommentCreationReq): ng.IPromise<IProtocolResponse<IAssessmentCommentModel>> {
        const config: IProtocolConfig = this.aaProtocolService.aaCustomConfig("POST", `/assessment-v2/v2/assessments/${assessmentId}/notes`, null, payload);
        const messages: IProtocolMessages = { Error: { Hide: true } };
        return this.aaProtocolService.aaRequestHandler(config, messages);
    }

    getNotes(assessmentId: string): ng.IPromise<IAssessmentCommentModel[]> {
        const config: IProtocolConfig = this.aaProtocolService.aaCustomConfig("GET", `/assessment-v2/v2/assessments/${assessmentId}/notes`);
        const messages: IProtocolMessages = { Error: { custom: this.translate.transform("CouldNotRetrieveNotes") } };
        return this.aaProtocolService.aaRequestHandler(config, messages)
            .then((res: IProtocolResponse<IAssessmentCommentModel[]>): IAssessmentCommentModel[] => {
                return res.result ? res.data : [];
            });
    }

    updateNote(assessmentId: string, payload: IAssessmentCommentModaldata): ng.IPromise<IProtocolResponse<IAssessmentCommentModel>> {
        const config: IProtocolConfig = this.aaProtocolService.aaCustomConfig("PUT", `/assessment-v2/v2/assessments/${assessmentId}/notes/${payload.id}`, null, { type: payload.type, value: payload.value });
        const messages: IProtocolMessages = { Error: { custom: this.translate.transform("ErrorUpdatingNote") } };
        return this.aaProtocolService.aaRequestHandler(config, messages);
    }

    deleteNote(assessmentId: string, noteId: string): ng.IPromise<boolean> {
        const config: IProtocolConfig = this.aaProtocolService.aaCustomConfig("DELETE", `/assessment-v2/v2/assessments/${assessmentId}/notes/${noteId}`);
        const messages: IProtocolMessages = { Error: { custom: this.translate.transform("CouldNotDeleteNote") } };
        return this.aaProtocolService.aaRequestHandler(config, messages)
            .then((res: IProtocolResponse<void>): boolean => {
                return res.result;
            });
    }

    saveAssessmentCopy(payload: IAssessmentCopyPayload & IAsessmentCopyContent): ng.IPromise<boolean> {
        const config: IProtocolConfig = this.aaProtocolService.aaCustomConfig("POST", `/assessment-v2/v2/assessments/${payload.sourceAssessmentId}/copy`, null, payload);
        const messages: IProtocolMessages = { Error: { custom: this.translate.transform("ErrorUpdatingAssessment") } };
        return this.aaProtocolService.aaRequestHandler(config, messages)
            .then((res: IProtocolResponse<void>): boolean => {
                return res.result;
            });
    }

    getAssessmentTags(): ng.IPromise<IProtocolResponse<IQuestionModelOption[]>> {
        const config: IProtocolConfig = this.aaProtocolService.aaCustomConfig("GET", `/assessment-v2/v2/assessments/tags`);
        const messages: IProtocolMessages = { Error: { custom: this.translate.transform("CouldNotGetAssessmentTags") } };
        return this.aaProtocolService.aaRequestHandler(config, messages);
    }

    getTemplateUpdates(firstTemplateId: string, templateId: string): Observable<IProtocolResponse<IUpdatedTemplateQuestionsResponse>> {
        const config: IProtocolConfig = this.aaProtocolService.aaCustomConfig("GET", `/api/template/v1/templates/compare`, { newTemplateId: templateId, oldTemplateId: firstTemplateId });
        const messages: IProtocolMessages = { Error: { custom: this.translate.transform("CouldNotGetAssessmentTags") } };
        return from(this.aaProtocolService.aaRequestHandler(config, messages));
    }

    rejectAssessment(assessmentId: string, forceOverride: boolean, emailComment: string): Observable<IProtocolResponse<null>> {
        const config: IProtocolConfig = this.aaProtocolService.aaCustomConfig("POST", `/assessment-v2/v2/assessments/${assessmentId}/reject`, { emailComment, forceOverride });
        const messages: IProtocolMessages = { Error: { custom: this.translate.transform("ErrorRejectingAssessment") } };
        return from(this.aaProtocolService.aaRequestHandler(config, messages));
    }

    private convertAppliedFilterValue(appliedFilter: ILabelValue<string>): string {
        if (appliedFilter && appliedFilter.value) {
            switch (appliedFilter.value) {
                case AASectionFilterTypes.AllQuestions:
                    return AssessmentSectionNavFilters.AllQuestions;
                    break;
                case AASectionFilterTypes.RequiredAndUnansweredQuestions:
                    return AssessmentSectionNavFilters.RequiredAndUnansweredQuestions;
                    break;
                case AASectionFilterTypes.RequiredQuestions:
                    return AssessmentSectionNavFilters.RequiredQuestions;
                    break;
                case AASectionFilterTypes.UnansweredQuestions:
                    return AssessmentSectionNavFilters.UnansweredQuestions;
                    break;
                default:
                    return AssessmentSectionNavFilters.AllQuestions;
            }
        } else {
            return AssessmentSectionNavFilters.AllQuestions;
        }
    }
}
