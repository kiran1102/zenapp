
// Angular
import { Injectable } from "@angular/core";

// Rxjs
import {
    Observable,
    from,
} from "rxjs";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

// Services
import { AaProtocolService } from "assessmentServices/api/aa-protocol.service";

// Interfaces
import {
    IProtocolConfig,
    IProtocolMessages,
    IProtocolResponse,
} from "interfaces/protocol.interface";
import { ICollaborationPaneCounts } from "interfaces/assessment/aa-detail-header.interface";

// Enums
import { AACollaborationCountsTypes } from "../../enums/aa-detail-header.enum";

@Injectable()
export class AssessmentDetailHeaderApiService {
    constructor(
        private translate: TranslatePipe,
        private aaProtocolService: AaProtocolService,
    ) { }

    getCollabCounts(assessmentId: string, type: AACollaborationCountsTypes): Observable<IProtocolResponse<ICollaborationPaneCounts[]>> {
        const config: IProtocolConfig = this.aaProtocolService.aaCustomConfig("GET", `/assessment-v2/v2/assessments/${assessmentId}/referenceEntityCounts`, {type});
        const messages: IProtocolMessages = { Error: { custom: this.translate.transform("CouldNotRetrieveCounts") } };
        return from(this.aaProtocolService.aaRequestHandler(config, messages)
            .then((res: IProtocolResponse<ICollaborationPaneCounts[]>): IProtocolResponse<ICollaborationPaneCounts[]> => {
                return res.result ? res : null;
        }));
    }
}
