// Angular
import { Injectable } from "@angular/core";
// Interfaces
import { IStringMap } from "interfaces/generic.interface";
import { ILinkOption } from "interfaces/inventory.interface";
import { ICustomTemplateDetails } from "modules/template/interfaces/custom-templates.interface";
import {
    IProtocolConfig,
    IProtocolMessages,
    IProtocolResponse,
} from "interfaces/protocol.interface";

// Services
import { AaProtocolService } from "assessmentServices/api/aa-protocol.service";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

@Injectable()
export class AssessmentLinkingAPIService {

    private translations: IStringMap<string> = {
        errorRetrievingTemplates: this.translatePipe.transform("ErrorRetreivingTemplateDetails"),
        errorLinkingAssessment: this.translatePipe.transform("ErrorLinkingAssessment"),
        errorRemovingAssessmentLink: this.translatePipe.transform("ErrorRemovingAssessmentLink"),
        successfullyLinkedAssessments: this.translatePipe.transform("SuccessfullyLinkedAssessments"),
        couldNotRetrieveAssessments: this.translatePipe.transform("CouldNotRetrieveAssessments"),
    };

    constructor(
        private translatePipe: TranslatePipe,
        private aaProtocolService: AaProtocolService,
    ) { }

    public getAssessmentTemplates = (types: string, state: string, activeCriteria?: string, assessmentCreationAllowed?: boolean): ng.IPromise<IProtocolResponse<ICustomTemplateDetails[]>> => {
        const config: IProtocolConfig = this.aaProtocolService.aaCustomConfig("GET", "/api/template/v1/templates", { types, state, activeCriteria, assessmentCreationAllowed });
        const messages: IProtocolMessages = { Error: { custom: this.translations.errorRetrievingProject } };
        return this.aaProtocolService.aaRequestHandler(config, messages);
    }

    public getAssessmentDropdownList = (templateId: string, recordId: string): ng.IPromise<IProtocolResponse<ILinkOption[]>> => {
        const config: IProtocolConfig = this.aaProtocolService.aaConfig("GET", `/assessmentv2/assessments/inventory/filter/${recordId}/templates/${templateId}/assessments`);
        const messages: IProtocolMessages = { Error: { custom: this.translations.couldNotRetrieveAssessments } };
        return this.aaProtocolService.aaRequestHandler(config, messages);
    }

    public linkAssessments = (recordId: string, assessmentsIds: string[], typeId: number, name: string): ng.IPromise<IProtocolResponse<null>> => {
        const config: IProtocolConfig = this.aaProtocolService.aaConfig("POST", `/assessmentv2/assessments/inventory`, null, {
            assessmentIds: assessmentsIds,
            inventoryId: recordId,
            inventoryTypeId: typeId,
            inventoryName: name,
        });
        const messages: IProtocolMessages = {
            Error: { custom: this.translations.ErrorLinkingAssessment },
            Success: { custom: this.translations.successfullyLinkedAssessments },
        };
        return this.aaProtocolService.aaRequestHandler(config, messages);
    }

    public removeAssessmentLink(recordId: string, assessmentId: string): ng.IPromise<boolean> {
        const config: IProtocolConfig = this.aaProtocolService.aaConfig("DELETE", `/assessmentv2/assessments/inventory/${assessmentId}/${recordId}`);
        const messages: IProtocolMessages = { Error: { custom: this.translations.errorRemovingAssessmentLink } };
        return this.aaProtocolService.aaRequestHandler(config, messages).then((res: IProtocolResponse<null>): boolean => {
            return res.result;
        });
    }
}
