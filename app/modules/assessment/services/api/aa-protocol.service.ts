
// Angular
import { Injectable } from "@angular/core";

// 3rd party
import { debounce } from "lodash";

// Interfaces
import {
    IProtocolConfig,
    IProtocolResponse,
    IProtocolMessages,
    IProtocolError,
} from "interfaces/protocol.interface";

// Services
import { ProtocolService } from "modules/core/services/protocol.service";
import { NotificationService } from "modules/shared/services/provider/notification.service";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

@Injectable()
export class AaProtocolService {

    aaCustomConfig = this.oneProtocol.customConfig.bind(this.oneProtocol);
    aaConfig = this.oneProtocol.config.bind(this.oneProtocol);

    private handle423Responses = debounce(() => this.notificationService.alertError(this.translatePipe.transform("Error"), this.translatePipe.transform("AssessmentLockedForPushingTemplate")), 5000, {
        leading: true,
        trailing: false,
    });

    constructor(
        private oneProtocol: ProtocolService,
        private notificationService: NotificationService,
        private translatePipe: TranslatePipe,
    ) { }

    aaRequestHandler(config: IProtocolConfig, messages: IProtocolMessages): ng.IPromise<IProtocolResponse<any>> {
        const customErrorMessage: string = messages && messages.Error ? messages.Error.custom : "";
        const hideMessage = messages.Error.Hide;
        messages = { Error: { Hide: true } };
        return this.oneProtocol.http(config, messages)
            .then((response: IProtocolResponse<any>) => {
                if (response.result || hideMessage) return response;

                if (response.status === 423 && response.errors.code === "ERROR_ASSESSMENT-V2_ASSESSMENT_LOCKED") {
                    this.handle423Responses();
                } else if (customErrorMessage) {
                    this.notificationService.alertError(this.translatePipe.transform("Error"), customErrorMessage);
                } else if (response.data) {
                    this.notificationService.alertWarning(this.translatePipe.transform("Error"), response.data);
                } else if (response.data && response.data.length) {
                    (response.data as IProtocolError[]).forEach((errorMessage: IProtocolError) => {
                        this.notificationService.alertWarning(errorMessage.title, errorMessage.detail);
                    });
                }

                return response;
            });
    }
}
