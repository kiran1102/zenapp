// Core
import {
    Injectable,
    Inject,
 } from "@angular/core";

 // Constants
import { APIResponseTypes } from "constants/api-response-types.constant";

// Interfaces
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import {
    IAssessmentFilterParams,
    IAssessmentTable,
    IAssessmentExportCriteria,
 } from "interfaces/assessment/assessment.interface";
import {
    IProtocolConfig,
    IProtocolMessages,
    IProtocolResponse,
} from "interfaces/protocol.interface";
import { IGridPaginationParams } from "interfaces/grid.interface";
import { IFilterColumnOption } from "interfaces/filter.interface";

// Rxjs
import {
    Observable,
    from,
} from "rxjs";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

// Services
import { AaProtocolService } from "assessmentServices/api/aa-protocol.service";
// TODO - Create a local AA interface when SSP is moved out of Zenapp
import { ISspTemplatesRestrictView } from "interfaces/self-service-portal/ssp.interface";

@Injectable()
export class AssessmentListApiService {

    constructor(
        @Inject("$rootScope") private $rootScope: IExtendedRootScopeService,
        private translatePipe: TranslatePipe,
        private aaProtocolService: AaProtocolService,
    ) {}

    public getAssessmentsDashboard = (text: string, filters: IAssessmentFilterParams[], paginationParams: IGridPaginationParams): ng.IPromise<IProtocolResponse<IAssessmentTable>> => {
        const config: IProtocolConfig = this.aaProtocolService.aaCustomConfig("POST", "/assessment-v2/v2/assessments/lists/dashboard", { text, ...paginationParams}, filters);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("ErrorRetrievingProject", { Project: this.$rootScope.customTerms.Projects }) }};
        return this.aaProtocolService.aaRequestHandler(config, messages);
    }

    public getAssessments = (text: string, filters: IAssessmentFilterParams[], paginationParams: IGridPaginationParams): ng.IPromise<IProtocolResponse<IAssessmentTable>> => {
        const config: IProtocolConfig = this.aaProtocolService.aaCustomConfig("POST", "/assessment-v2/v2/assessments/lists", { text, ...paginationParams}, filters);
        const messages: IProtocolMessages = { Error: { Hide: true } };
        return this.aaProtocolService.aaRequestHandler(config, messages);
    }

    public getAssignedAssessments = (text: string, filters: IAssessmentFilterParams[], paginationParams: IGridPaginationParams): ng.IPromise<IProtocolResponse<IAssessmentTable>> => {
        const filter: string = filters && filters.length > 0 ? encodeURI(JSON.stringify(filters)) : "";
        const config: IProtocolConfig = this.aaProtocolService.aaConfig("GET", "/assessmentv2/assessments/assigned/assessments", { text, filter, ...paginationParams });
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("ErrorRetrievingProject", { Project: this.$rootScope.customTerms.Projects }) }};
        return this.aaProtocolService.aaRequestHandler(config, messages);
    }

    public getAssessmentsColumnMetaData(): Observable<IProtocolResponse<IFilterColumnOption[]>> {
        const config: IProtocolConfig = this.aaProtocolService.aaCustomConfig("GET", "/assessment-v2/v2/assessments/reports/metadata", null);
        const messages: IProtocolMessages = { Error: { Hide: true } };
        return from(this.aaProtocolService.aaRequestHandler(config, messages));
    }

    public export(exportCriteria: IAssessmentExportCriteria): Observable<IProtocolResponse<void>> {
        const config: IProtocolConfig = this.aaProtocolService.aaCustomConfig("PUT", "/reporting/async/export/entities", null, exportCriteria, null, null, null, APIResponseTypes.TEXT);
        const messages: IProtocolMessages = { Error: { Hide: true } };
        return from(this.aaProtocolService.aaRequestHandler(config, messages));
    }

    public getSspRestrictViewSetting(): ng.IPromise<IProtocolResponse<ISspTemplatesRestrictView>> {
        const config: IProtocolConfig = this.aaProtocolService.aaCustomConfig("GET", `/assessment-v2/v2/assessments/setting`);
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("CouldNotRetrieveSetting") } };
        return this.aaProtocolService.aaRequestHandler(config, messages);
    }
    public retrieveViews(id: string) { // TODO: Add return type when API is implemented
        return [
            {
                label: "View All Records",
                value: 1,
                isDefaultView: true,
            },
            {
                label: "Seeded View Name",
                value: 2,
                isDefaultView: true,
            },
            {
                label: "View 2",
                value: 3,
            },
            {
                label: "View 3",
                value: 4,
            },
            {
                label: "View 4",
                value: 5,
            },
            {
                label: "View 5",
                value: 6,
            },
            {
                label: "View 6",
                value: 7,
            },
        ];
    }
}
