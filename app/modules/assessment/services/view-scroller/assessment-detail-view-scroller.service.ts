export class AssessmentDetailViewScrollerService {
    private scrollingContainer: HTMLElement;
    private questionElements = {};
    private cycleIndex = 0;

    registerScrollingContainer(element: HTMLElement): void {
        this.scrollingContainer = element;
    }

    registerQuestion(questionId: string, element: HTMLElement): void {
        this.questionElements[questionId] = element;
    }

    cycleThroughQuestions(questionIds: string[] = []) {
        if (questionIds.length) {
            const questionId = questionIds[this.cycleIndex];

            if (!this.questionElements[questionId]) return;

            this.questionElements[questionId].scrollIntoView(true);
            this.cycleIndex++;

            if (this.cycleIndex === questionIds.length) {
                this.cycleIndex = 0;
            }
        }
    }

    scrollToQuestion(questionId: string) {
        if (questionId && this.questionElements[questionId]) {
            this.questionElements[questionId].scrollIntoView(true);
        }
    }

    resetCycle() {
        this.cycleIndex = 0;
    }
}
