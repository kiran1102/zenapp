// 3rd Party
import { Injectable } from "@angular/core";

// Enum
import {
    AAFilterId,
    AAFilterTypes,
} from "modules/assessment/enums/aa-filter.enum";
import { AssessmentStatus } from "enums/assessment/assessment-status.enum";
import { FilterOperatorSymbol } from "modules/filter/enums/filter.enum";

// Constants
import {
    AAColumnFilterOperators,
    AssessmentResultKeys,
    AssessmentExportColumnMetaData,
    AssessmentColumnData,
} from "constants/assessment.constants";

// Interfaces
import {
    IFilterColumnOption,
    IFilterValueOption,
} from "interfaces/filter.interface";
import { IFilterOutput } from "modules/filter/interfaces/filter.interface";
import { IAssessmentFilterParams } from "interfaces/assessment/assessment.interface";
import { IStringMap } from "interfaces/generic.interface";

// Services
import { TimeStamp } from "oneServices/time-stamp.service";

@Injectable()
export class AAListFilterService {
    private aAColumnFilterOperators: IStringMap<IFilterValueOption> = AAColumnFilterOperators;

    constructor(
        private timeStamp: TimeStamp,
    ) { }

    applyFilters(filters: IFilterOutput[]): IAssessmentFilterParams[] {
        const appliedFilters: IAssessmentFilterParams[] = [];
        if (filters && filters.length) {
            filters.forEach((filter: IFilterOutput): void => {
                let value: string | string[];
                let toValue: string | string[];
                switch (filter.field.value) {
                    case AAFilterId.Deadline:
                    case AAFilterId.DateCreated:
                        value = filter.values && filter.values.length && filter.values[0] ? filter.values[0].value as string : null;
                        toValue = filter.values && filter.values.length && filter.values[1] ? this.timeStamp.getEndOfDayISO(filter.values[1].value as string) : null;
                        break;
                    case AAFilterId.OpenInfoRequest:
                    case AAFilterId.OpenRiskCount:
                        value = filter.values && filter.values.length ? filter.values[0].value as string : null;
                        toValue = null;
                        break;
                    default:
                        value = filter.values.map((item) => item.value as string);
                        toValue = null;
                }
                appliedFilters.push({
                    field: filter.field.value as string,
                    operation: filter && filter.operator ? filter.operator.value as string : FilterOperatorSymbol.EqualTo,
                    value,
                    toValue,
                });
            });
        }
        return appliedFilters;
    }

    getFilterValues(filter: IFilterOutput): string | string[] {
        if ((filter.field.value === AAFilterId.Deadline
            || filter.field.value === AAFilterId.DateCreated
            || filter.field.value === AAFilterId.OpenInfoRequest
            || filter.field.value === AAFilterId.OpenRiskCount)
            && Array.isArray(filter.values)) {
            return filter.values[0].value as string;
        }
        return filter.values.map((item) => item.value as string);
    }

    getAaListFilters(): IFilterColumnOption[] {
        // TODO: Filter options will be retrieved from api in a later phase.
        return [
            {
                type: AAFilterTypes.Approver,
                name: AssessmentExportColumnMetaData[AssessmentColumnData.Approver],
                id: AAFilterId.Approver,
                sortable: true,
                translationKey: "Approver",
                valueOptions: [],
                groupLabel: "",
                groupName: "Approver",
            },
            {
                type: AAFilterTypes.Creator,
                name: AssessmentExportColumnMetaData[AssessmentColumnData.Creator],
                id: AAFilterId.Creator,
                sortable: true,
                translationKey: "Creator",
                valueOptions: [],
                groupLabel: "",
                groupName: "Creator",
            },
            {
                type: AAFilterTypes.DateCreated,
                name: AssessmentExportColumnMetaData[AssessmentColumnData.DateCreated],
                id: AAFilterId.DateCreated,
                sortable: true,
                translationKey: "DateCreated",
                valueOptions: [],
                groupLabel: "",
                groupName: "Date Created",
            },
            {
                type: AAFilterTypes.Deadline,
                name: AssessmentExportColumnMetaData[AssessmentColumnData.Deadline],
                id: AAFilterId.Deadline,
                sortable: true,
                translationKey: "Deadline",
                valueOptions: [],
                groupLabel: "",
                groupName: "Deadline",
            },
            {
                type: AAFilterTypes.OpenInfoRequest,
                name: AssessmentExportColumnMetaData[AssessmentColumnData.OpenInfoRequest],
                id: AAFilterId.OpenInfoRequest,
                sortable: true,
                translationKey: "OpenInfoRequest",
                valueOptions: [],
                groupLabel: "",
                groupName: "Open Info Request",
            },
            {
                type: AAFilterTypes.OpenRiskCount,
                name: AssessmentExportColumnMetaData[AssessmentColumnData.OpenRiskCount],
                id: AAFilterId.OpenRiskCount,
                sortable: true,
                translationKey: "OpenRiskCount",
                valueOptions: [],
                groupLabel: "",
                groupName: "OpenRiskCount",
            },
            {
                type: AAFilterTypes.Organization,
                name: AssessmentExportColumnMetaData[AssessmentColumnData.Organization],
                id: AAFilterId.Organization,
                sortable: true,
                translationKey: "Organization",
                valueOptions: [],
                groupLabel: "",
                groupName: "Organization",
            },
            {
                type: AAFilterTypes.Respondent,
                name: AssessmentExportColumnMetaData[AssessmentColumnData.Respondent],
                id: AAFilterId.Respondent,
                sortable: false,
                translationKey: "Respondent",
                valueOptions: [],
                groupLabel: "",
                groupName: "Respondent",
            },
            {
                type: AAFilterTypes.Result,
                name: AssessmentExportColumnMetaData[AssessmentColumnData.Result],
                id: AAFilterId.Result,
                sortable: true,
                translationKey: "Result",
                valueOptions: [
                    {
                        value: "Approved",
                        key: AssessmentResultKeys.Approved,
                        translationKey: AssessmentResultKeys.Approved,
                    },
                    {
                        value: "Rejected",
                        key: AssessmentResultKeys.Rejected,
                        translationKey: AssessmentResultKeys.Rejected,
                    },
                    {
                        value: "Auto Closed",
                        key: AssessmentResultKeys.AutoClosed,
                        translationKey: AssessmentResultKeys.AutoClosed,
                    },
                ],
                groupLabel: "",
                groupName: "Result",
            },
            {
                type: AAFilterTypes.RiskLevel,
                name: AssessmentExportColumnMetaData[AssessmentColumnData.RiskLevel],
                id: AAFilterId.RiskLevel,
                sortable: true,
                translationKey: "RiskLevel",
                valueOptions: [
                    {
                        value: "Low",
                        key: 1,
                        translationKey: "Low",
                    },
                    {
                        value: "Medium",
                        key: 2,
                        translationKey: "Medium",
                    },
                    {
                        value: "High",
                        key: 3,
                        translationKey: "High",
                    },
                    {
                        value: "Very High",
                        key: 4,
                        translationKey: "VeryHigh",
                    },
                ],
                groupLabel: "",
                groupName: "Risk Level",
            },
            {
                type: AAFilterTypes.State,
                name: AssessmentExportColumnMetaData[AssessmentColumnData.State],
                id: AAFilterId.State,
                sortable: true,
                translationKey: "Stage",
                valueOptions: [
                    {
                        value: "In Progress",
                        key: AssessmentStatus.In_Progress,
                        translationKey: "InProgress",
                    },
                    {
                        value: "Not Started",
                        key: AssessmentStatus.Not_Started,
                        translationKey: "NotStarted",
                    },
                    {
                        value: "Under Review",
                        key: AssessmentStatus.Under_Review,
                        translationKey: "UnderReview",
                    },
                    {
                        value: "Completed",
                        key: AssessmentStatus.Completed,
                        translationKey: "Completed",
                    },
                ],
                groupLabel: "",
                groupName: "State",
            },
            {
                type: AAFilterTypes.Tags,
                name: AssessmentExportColumnMetaData.Tags,
                id: AAFilterId.Tags,
                sortable: true,
                translationKey: "Tags",
                valueOptions: [],
                groupLabel: "",
                groupName: "Tags",
            },
            {
                type: AAFilterTypes.Template,
                name: AssessmentExportColumnMetaData[AssessmentColumnData.Template],
                id: AAFilterId.Template,
                sortable: true,
                translationKey: "Template",
                valueOptions: [],
                groupLabel: "",
                groupName: "Template",
            },
        ];
    }

    public getCurrentOperators(): IFilterValueOption[] {
        return Object.values(this.aAColumnFilterOperators);
    }

    public getSelectedOperatorLabelValuePair(operator: string): any {
        return this.aAColumnFilterOperators[operator];
    }
}
