// Angular
import { Component, OnInit } from "@angular/core";

// Services
import { LandingService } from "../../core/services/landing.service";

@Component({
    template: `
        <ot-loading otPageLoading class="app-loading flex-full-height"></ot-loading>
    `,
})
export class LandingComponent implements OnInit {

    constructor(private landingService: LandingService) {}

    ngOnInit() {
        this.landingService.execute();
    }
}
