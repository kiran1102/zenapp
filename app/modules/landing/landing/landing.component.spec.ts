import { async, ComponentFixture, TestBed } from "@angular/core/testing";
import { By } from "@angular/platform-browser";

import { LandingComponent } from "./landing.component";

describe("LandingComponent", () => {
    let component: LandingComponent;
    let fixture: ComponentFixture<LandingComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [LandingComponent],
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(LandingComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it("should create", () => {
        expect(component).toBeTruthy();
    });

    it("should display a loading component", () => {
        const loadingDebugEl = fixture.debugElement.query(By.css("ot-loading"));
        expect(loadingDebugEl).toBeTruthy();
        const loadingEl: HTMLElement = loadingDebugEl.nativeElement;
        expect(loadingEl).toBeTruthy();
        expect(loadingEl.attributes.getNamedItem("ot-page-loading")).toBeTruthy();
    });
});
