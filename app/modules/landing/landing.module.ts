import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

// Modules
import { OtLoadingModule } from "@onetrust/vitreus";

// Services
import { LandingService } from "modules/core/services/landing.service";

// Components
import { LandingComponent } from "./landing/landing.component";

// Routing
import { routing } from "./landing.routing";

@NgModule({
    imports: [
        CommonModule,
        OtLoadingModule,
        routing,
    ],
    providers: [LandingService],
    declarations: [LandingComponent],
})
export class LandingModule {}
