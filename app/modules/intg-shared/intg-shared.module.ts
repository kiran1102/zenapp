// Angular
import { NgModule } from "@angular/core";
import { SharedModule } from "sharedModules/shared.module";
import { CommonModule } from "@angular/common";
import { HttpClientModule } from "@angular/common/http";
import { FormsModule } from "@angular/forms";
import { ReactiveFormsModule } from "@angular/forms";
import { VitreusModule } from "@onetrust/vitreus";
import { DndListModule } from "ngx-drag-and-drop-lists";
import { OTPipesModule, PIPES } from "modules/pipes/pipes.module";
import { VirtualScrollModule } from "od-virtualscroll";
import { OneVideoModule } from "modules/video/video.module";

// Components
import { DataDiscoveryComponent } from "modules/intg-shared/components/intg-data-discovery.component";

// Assets
import "images/intg-systems/discovery_icon.svg";
import "images/intg-systems/service-now.png";
import "images/intg-systems/office_365.png";

@NgModule({
    imports: [
        SharedModule,
        CommonModule,
        HttpClientModule,
        FormsModule,
        ReactiveFormsModule,
        VitreusModule,
        DndListModule,
        OTPipesModule,
        VirtualScrollModule,
        OneVideoModule,
    ],
    declarations: [DataDiscoveryComponent],
    entryComponents: [DataDiscoveryComponent],
    exports: [DataDiscoveryComponent],
    providers: [
        PIPES, // TODO: Remove from providers when ot-common is implemented. Only necessary if you are using pipe in TS file, else just module is fine
    ],
})

export class IntgSharedModule { }
