export interface IIDataDiscoverySystems {
    id: string;
    name: string;
    icon: string;
    description: string;
    backgroundColor?: string;
}
