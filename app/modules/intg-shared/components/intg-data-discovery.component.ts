// Angular
import {Component} from "@angular/core";
import { StateService } from "@uirouter/core";

// pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

// interface
import { IIDataDiscoverySystems } from "modules/intg-shared/interfaces/intg-data-discovery.interface";

// constant
import { SystemConstants } from "modules/intg/constants/integration.constant";

@Component({
    selector: "data-discovery",
    templateUrl: "./intg-data-discovery.component.html",
})
export class DataDiscoveryComponent {
    discoveryIcon = "images/intg-systems/discovery_icon.svg";
    oktaIcon = "images/intg-systems/okta.svg";
    targetedDiscovery: IIDataDiscoverySystems[] = [
        {
            id: "a7921023-4d00-4e25-aff3-9fd5e60d7a51",
            name: "okta",
            icon: "images/intg-systems/okta.png",
            description: this.translatePipe.transform("OktaDesc"),
            backgroundColor: "white",
        },
        {
            id: "7686e769-89e1-40e9-afcb-1badb6816622",
            name: "servicenow",
            icon: "images/intg-systems/service-now.png",
            description: this.translatePipe.transform("ServicenowDescription"),
            backgroundColor: "#263F40",
        },
        {
            id: "bd6d14bc-0c8d-4e9b-a805-a6a2a2c24fad",
            name: "Office365",
            icon: "images/intg-systems/office_365.png",
            description: this.translatePipe.transform("TargetDiscoveryDeployedDescription", {systemName: "Office365"}),
            backgroundColor: "white",
        },
    ];
    eDiscoveryList: IIDataDiscoverySystems[] = [
        {
            id: SystemConstants.BIGID,
            name: "bigid",
            icon: "images/intg-systems/BigId.png",
            description: this.translatePipe.transform("BigIdDescriptionV2"),
            backgroundColor: "black",
        },
        {
            id: SystemConstants.DATAGUISE,
            name: "dataguise",
            icon: "images/intg-systems/dataguise-logo.png",
            description: this.translatePipe.transform("DataGuiseDescriptionV2"),
            backgroundColor: "white",
        },
        {
            id: SystemConstants.VARONIS,
            name: "varonis",
            icon: "images/intg-systems/varonis_Logo.svg",
            description: this.translatePipe.transform("VaronisDescriptionV2"),
            backgroundColor: "black",
        },
        {
            id: SystemConstants.IBMSECURITY,
            name: "ibmsecurity",
            icon: "images/intg-systems/ibm-security-logo.svg",
            description: this.translatePipe.transform("IbmSecurityDescription"),
            backgroundColor: "white",
        },
    ];

    constructor(
         private translatePipe: TranslatePipe,
         private stateService: StateService,
    ) { }

    goToRoute(integrationId: string) {
        this.stateService.go("zen.app.pia.module.intg.marketplace.details", {system: integrationId});
    }
}
