import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
name: "sort",
})

export class SortPipe implements PipeTransform {
    transform(value: any[], sortByValue: any): any[] {
        if (!value || !sortByValue) return [];
        return value.sort(
            (a, b) => (a[sortByValue].toLocaleUpperCase() > b[sortByValue].toLocaleUpperCase()) ? 1 :
            ((b[sortByValue].toLocaleUpperCase() > a[sortByValue].toLocaleUpperCase()) ? -1 : 0));
    }
}
