import {
    Pipe,
    PipeTransform,
    Inject,
} from "@angular/core";
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import { ILabelValue } from "@onetrust/vitreus/src/app/interface/label-value.interface";

@Pipe({
    name: "otTranslateOption",
})

export class TranslateOptionPipe implements PipeTransform {
    constructor(
        @Inject("$rootScope") private $rootScope: IExtendedRootScopeService,
    ) {}

    transform(option: ILabelValue<string>): ILabelValue<string> {
        if (!option || !option.label) return;
        return {...option, label: this.$rootScope.t(option.label)};
    }
}

@Pipe({
    name: "otTranslateOptions",
})

export class TranslateOptionsPipe implements PipeTransform {
    constructor(
        @Inject("$rootScope") private $rootScope: IExtendedRootScopeService,
    ) {}

    transform(options: Array<ILabelValue<string>>, upperCase: boolean = false): Array<ILabelValue<string>> {
        if (!options || !options.length || !options[0].label) return;
        return options.map((opt) => ({...opt, label: upperCase ? this.$rootScope.t(opt.label).toUpperCase() : this.$rootScope.t(opt.label)}));
    }
}
