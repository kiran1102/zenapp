// 3rd Party
import {
    Pipe,
    PipeTransform,
} from "@angular/core";

// Services
import { Permissions } from "modules/shared/services/helper/permissions.service";

@Pipe({
    name: "otPermit",
})
export class PermitPipe implements PipeTransform {
    constructor(
        private permissions: Permissions,
    ) {}

    transform(permissionKeys: string | string[], concatLogic?: "OR" | "AND"): boolean {
        if (typeof permissionKeys === "string") return this.permissions.canShow(permissionKeys);
        if (!(permissionKeys && permissionKeys.length)) return false;
        if (concatLogic === "OR") {
            for (let i = 0; i < permissionKeys.length; i++) {
                if (this.permissions.canShow(permissionKeys[i])) return true;
            }
            return false;
        } else {
            for (let i = 0; i < permissionKeys.length; i++) {
                if (!this.permissions.canShow(permissionKeys[i])) return false;
            }
            return true;
        }
    }
}
