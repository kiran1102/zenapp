import {
    Pipe,
    PipeTransform,
} from "@angular/core";

@Pipe({
    name: "highlight",
})

export class HighlightPipe implements PipeTransform {

    transform(value: string, searchTerm: string): string {
        if (!value || !searchTerm) return value;
        const startIndex: number = value.toLowerCase().indexOf(searchTerm.toLowerCase());
        if (startIndex === -1) return value;
        const endIndex: number = startIndex + searchTerm.length;
        const textBeforeHighlight: string = value.substring(0, startIndex);
        const highlightedText: string = value.substring(startIndex, endIndex);
        const textAfterHighlight: string = value.substring(endIndex);
        return `${textBeforeHighlight}<span class="highlight">${highlightedText}</span>${textAfterHighlight}`;
    }
}
