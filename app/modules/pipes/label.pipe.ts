import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
    name: "getLabelBy",
})

export class LabelPipe implements PipeTransform {
    transform(value: any, mapToStringFn: (item: any) => string): string {
        return mapToStringFn(value);
    }
}
