import { Pipe, PipeTransform } from "@angular/core";
import { each } from "lodash";

@Pipe({
    name: "filter",
})

export class FilterPipe implements PipeTransform {
    transform(value: any[], filterProperty: string[], filterValue: string): any[] {
        if (!filterValue) {
            return value;
        }
        const filteredArray = [];
        filterValue = filterValue.toLocaleLowerCase();
        value.filter((row: any): void => {
            each(filterProperty, (prop: string): void => {
                if (row[prop].toLocaleLowerCase().indexOf(filterValue) !== -1) {
                    filteredArray.push(row);
                }
            });
        });
        return filteredArray;
    }
}
