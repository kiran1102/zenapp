// Angular
import {
    Pipe,
    PipeTransform,
} from "@angular/core";

// Constants
import { AssessmentStatusKeys } from "constants/assessment.constants";

// Enum
import { AssessmentStatus } from "enums/assessment/assessment-status.enum";

@Pipe({
    name: "badgeLabel",
})
export class AssessmentBadgeLabelPipe implements PipeTransform {
    transform(status: string): string {
        switch (status) {
            case AssessmentStatus.In_Progress:
                return AssessmentStatusKeys.IN_PROGRESS;
            case AssessmentStatus.Under_Review:
                return AssessmentStatusKeys.UNDER_REVIEW;
            case AssessmentStatus.Completed:
                return AssessmentStatusKeys.COMPLETED;
            case AssessmentStatus.Not_Started:
                return AssessmentStatusKeys.NOT_STARTED;
        }
    }
}
