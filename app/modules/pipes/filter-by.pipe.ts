import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
    name: "filterBy",
})

export class FilterByPipe implements PipeTransform {
    transform(values: any[], filterByFn: (item: any) => boolean): any[] {
        return values.filter(filterByFn);
    }
}
