import { Pipe, PipeTransform } from "@angular/core";
import { IOptionsType } from "sharedModules/enums/options-type.enum";

@Pipe({
    name: "convertToOptions",
})

export class ConvertToOptionsPipe implements PipeTransform {
    transform(options: any[], type: number): any[] {
       if (type === IOptionsType.ILabelValue) {
            return options.map((option: string, index: number) => {
                return {
                    value: index,
                    label: option,
                };
            });
       }
    }
}
