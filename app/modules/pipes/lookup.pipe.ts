// 3rd Party
import {
    Pipe,
    PipeTransform,
} from "@angular/core";

// Services
import { LookupService } from "sharedModules/services/helper/lookup.service";

@Pipe({
    name: "lookup",
})

export class LookupPipe implements PipeTransform {
    constructor(
        private lookupService: LookupService,
    ) {}

    transform(values: any[], selections: any[], searchTerm: string, idPropertyName?: string, namePropertyName?: string): any[] {
        return this.lookupService.filterOptionsByInput(selections, values, searchTerm, idPropertyName, namePropertyName);
    }
}
