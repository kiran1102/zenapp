import { Pipe, PipeTransform } from "@angular/core";
import { IKeyValue, IStringMap } from "interfaces/generic.interface";

@Pipe({
    name: "groupBy",
})

export class GroupByPipe implements PipeTransform {
    transform<T>(items: T[], groupKey: string): Array<IKeyValue<T[]>> {
        if (!items || !items.length) return [];
        const groupMap = items.reduce((prev: IStringMap<T[]>, curr: T) => {
            if (!prev[curr[groupKey]]) {
                prev[curr[groupKey]] = [curr];
            } else {
                prev[curr[groupKey]].push(curr);
            }
            return prev;
        }, {});
        return Object.keys(groupMap).map((key) => ({ key, value: groupMap[key]}));
    }
}
