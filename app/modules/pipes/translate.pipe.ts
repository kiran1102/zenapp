import {
    Pipe,
    PipeTransform,
    Inject,
} from "@angular/core";
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";

@Pipe({
    name: "otTranslate",
})

export class TranslatePipe implements PipeTransform {
    constructor(
        @Inject("$rootScope") private $rootScope: IExtendedRootScopeService,
    ) {}

    transform(translationKey: string = null, customTerms: {} = null): string {
        if (!translationKey) return "";
        return this.$rootScope.t(translationKey, customTerms);
    }
}
