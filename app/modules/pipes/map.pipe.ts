import { Pipe, PipeTransform } from "@angular/core";

// Usage: This is a generic mapping pipe that can be used to map anything
// Params:
// 1) An array of anything
// 2) A function that takes a single item of the same type as the items in the first param and maps that item to any other type
@Pipe({
    name: "map",
})

export class MapPipe implements PipeTransform {
    transform(values: any[], mapToFn: (item: any) => any): any[] {
        if (!(mapToFn && values && values.length)) return values;
        return values.map(mapToFn);
    }
}
