import {
    Pipe,
    PipeTransform,
} from "@angular/core";
import { RiskHelperService } from "sharedModules/services/helper/risk-helper.service";
import { RiskV2Levels } from "enums/riskV2.enum";

@Pipe({
    name: "otRiskColor",
})
export class RiskColorPipe implements PipeTransform {

    constructor(private readonly riskHelper: RiskHelperService) { }

    transform(level: RiskV2Levels): string {
        return this.riskHelper.getRiskColorByLevel(level);
    }
}
