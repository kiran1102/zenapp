// Angular
import {
    Pipe,
    PipeTransform,
} from "@angular/core";

// Pipes
import { OtDatePipe } from "pipes/ot-date.pipe";

@Pipe({
    name: "aaDate",
})
export class AaDatePipe implements PipeTransform {
    constructor(private readonly otDatePipe: OtDatePipe) {}

    transform(dateToFormat: string, type: "date" | "dateTime" = "date"): string {
        if ((dateToFormat === "Not Answered") || (dateToFormat === "NotApplicable") || (dateToFormat === "NotSure")) {
            return dateToFormat;
        } else {
            return this.otDatePipe.transform(dateToFormat, type);
        }
    }
}
