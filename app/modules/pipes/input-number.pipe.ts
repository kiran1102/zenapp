import {
    Pipe,
    PipeTransform,
} from "@angular/core";

@Pipe({
    name: "inputNumber",
})

export class InputNumberPipe implements PipeTransform {
    transform(input: string | number, minValue: number, maxValue: number): number {
        let value = parseInt(input as string, 0);
        if ((value > maxValue) || (value < minValue) || !value) {
            value = minValue;
        }
        return value;
    }
}
