import { Pipe, PipeTransform } from "@angular/core";

// Usage: This is a generic transform pipe that can be take any value and return any value based on a transform function
// Params:
// 1) Any value. The pipe will rerun only when this value is changed
// 2) A function that takes the value from param 1 as well as any additional parameters and return some desired value
// 3) An array of additional params needed to calculate the final value. These will not be passed to the transform function
//    as an array, but rather individual arguments, so the transform function you use should accept a matching number of params
@Pipe({
    name: "transform",
})

export class TransformPipe implements PipeTransform {
    transform(value: any, transformFn: (...args: any[]) => any, additionalParams: any[] = []): any[] {
        if (!transformFn) return value;
        return transformFn(value, ...additionalParams);
    }
}
