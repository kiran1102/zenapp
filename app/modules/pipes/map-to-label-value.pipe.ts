import { Pipe, PipeTransform } from "@angular/core";
import { ILabelValue } from "interfaces/generic.interface";

// Usage: This pipe is used to map an array of anything to an array of ILabelValue
// Params:
// 1) An array of anything
// 2) A function that takes a single item of the same type as the items in the first param and returns an ILabelValue object
@Pipe({
    name: "mapToLabelValue",
})

export class MapToLabelValuePipe implements PipeTransform {
    transform(values: any[], mapToLabelValueFn: (item: any) => ILabelValue<string | number>): Array<ILabelValue<string | number>> {
        if (!(mapToLabelValueFn && values && values.length)) return values;
        return values.map(mapToLabelValueFn);
    }
}
