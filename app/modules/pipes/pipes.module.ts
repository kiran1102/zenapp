import { NgModule } from "@angular/core";

// Pipes
import { FilterPipe } from "./filter.pipe";
import { OtDatePipe } from "./ot-date.pipe";
import { OtPageMetaPipe } from "./ot-page-meta.pipe";
import { SortPipe } from "./sort.pipe";
import { TranslatePipe } from "./translate.pipe";
import { TranslateOptionPipe, TranslateOptionsPipe } from "./translate-option.pipe";
import { RiskColorPipe } from "./risk-color.pipe";
import { InputNumberPipe } from "./input-number.pipe";
import { BadgeColorPipe } from "./badge-color.pipe";
import { OtUserInitialsPipe } from "./ot-user-initials";
import { PermitPipe } from "./permit.pipe";
import { ConvertToOptionsPipe } from "./convert-to-options.pipe";
import { OtUserPipe } from "./user.pipe";
import { AaDatePipe } from "./aa-date.pipe";
import { LabelPipe } from "./label.pipe";
import { FilterByPipe } from "./filter-by.pipe";
import { HighlightPipe } from "./highlight.pipe";
import { GroupByPipe } from "./group-by.pipe";
import { MapToLabelValuePipe } from "./map-to-label-value.pipe";
import { LookupPipe } from "./lookup.pipe";
import { AssessmentBadgeLabelPipe } from "./assessment-badge-label.pipe";
import { MapPipe } from "./map.pipe";
import { TransformPipe } from "./transform.pipe";

// TODO stop using pipes as services
export const PIPES = [
    FilterPipe,
    OtDatePipe,
    OtPageMetaPipe,
    SortPipe,
    TranslatePipe,
    TranslateOptionPipe,
    TranslateOptionsPipe,
    RiskColorPipe,
    InputNumberPipe,
    BadgeColorPipe,
    OtUserInitialsPipe,
    PermitPipe,
    ConvertToOptionsPipe,
    OtUserPipe,
    AaDatePipe,
    LabelPipe,
    FilterByPipe,
    HighlightPipe,
    GroupByPipe,
    MapToLabelValuePipe,
    LookupPipe,
    AssessmentBadgeLabelPipe,
    MapPipe,
    TransformPipe,
];

@NgModule({
    imports: [],
    providers: [],
    exports: [
        FilterPipe,
        OtDatePipe,
        OtPageMetaPipe,
        SortPipe,
        TranslatePipe,
        TranslateOptionPipe,
        TranslateOptionsPipe,
        RiskColorPipe,
        InputNumberPipe,
        BadgeColorPipe,
        OtUserInitialsPipe,
        PermitPipe,
        ConvertToOptionsPipe,
        OtUserPipe,
        AaDatePipe,
        LabelPipe,
        FilterByPipe,
        HighlightPipe,
        GroupByPipe,
        MapToLabelValuePipe,
        LookupPipe,
        AssessmentBadgeLabelPipe,
        MapPipe,
        TransformPipe,
    ],
    declarations: [
        FilterPipe,
        OtDatePipe,
        OtPageMetaPipe,
        SortPipe,
        TranslatePipe,
        TranslateOptionPipe,
        TranslateOptionsPipe,
        RiskColorPipe,
        InputNumberPipe,
        BadgeColorPipe,
        OtUserInitialsPipe,
        PermitPipe,
        ConvertToOptionsPipe,
        OtUserPipe,
        AaDatePipe,
        LabelPipe,
        FilterByPipe,
        HighlightPipe,
        GroupByPipe,
        MapToLabelValuePipe,
        LookupPipe,
        AssessmentBadgeLabelPipe,
        MapPipe,
        TransformPipe,
    ],
})

export class OTPipesModule { }
