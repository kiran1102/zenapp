import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
    name: "badgeColor",
})

export class BadgeColorPipe implements PipeTransform {
    transform(value: string, list?): string {
        if (!list || !list[value]) {
            return "";
        }

        return list[value];
    }
}
