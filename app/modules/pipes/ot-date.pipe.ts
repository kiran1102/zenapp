import {
    Pipe,
    PipeTransform,
} from "@angular/core";
import moment from "moment";
import "moment-timezone";

// Services
import { TimeStamp } from "oneServices/time-stamp.service";

@Pipe({
    name: "otDate",
})

export class OtDatePipe implements PipeTransform {

    localTimeZone = moment().tz(moment.tz.guess()).format("z");
    formattedText = "";

    constructor(private readonly timeStamp: TimeStamp) {}

    transform(dateToFormat: string, type: "date" | "dateLocal" | "datePicker" | "dateTime" | "datetime" = "dateTime", useLocalTimeZoneText = false, defaultText: string = ""): string {
        if (!dateToFormat) return defaultText;

        const dateObj = moment.utc(dateToFormat);

        if (useLocalTimeZoneText && (this.timeStamp.currentDateFormatting !== " " || this.timeStamp.currentTimeFormatting !== " ")) {
            this.formattedText = ` ${this.localTimeZone}`;
        }

        switch (type) {
            case "date":
                return `${dateObj.format(this.timeStamp.currentDateFormatting)}`;
            case "dateLocal":
                return `${dateObj.local().format(this.timeStamp.currentDateFormatting)}`;
            case "datePicker":
                return `${this.timeStamp.formatDateWithoutTime(dateObj.toISOString())}`;
            case "dateTime":
            case "datetime":
                return `${this.timeStamp.formatDate(dateToFormat)}${this.formattedText}`;
        }
    }
}
