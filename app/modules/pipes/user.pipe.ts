import {
    Pipe,
    PipeTransform,
    Inject,
} from "@angular/core";

// Tokens
import { StoreToken } from "tokens/redux-store.token";

// Interfaces
import { IStore } from "interfaces/redux.interface";
import { IUser } from "interfaces/user.interface";

// Redux
import { getUserById} from "oneRedux/reducers/user.reducer";

@Pipe({
    name: "otUser",
})
export class OtUserPipe implements PipeTransform {

    constructor(
        @Inject(StoreToken) readonly store: IStore,
    ) {}

    transform(userString: string): string {
        const systemUser: IUser = getUserById(userString)(this.store.getState());
        return systemUser ? systemUser.FullName || systemUser.Email : userString || "";
    }
}
