// Angular
import {
    Pipe,
    PipeTransform,
} from "@angular/core";

// Interfaces
import { IPageableMeta } from "interfaces/pagination.interface";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

@Pipe({
    name: "otPageMeta",
})

export class OtPageMetaPipe implements PipeTransform {
    constructor(
        private readonly translatePipe: TranslatePipe,
    ) { }

    transform(listData: IPageableMeta, format: string): string {
        if (!listData || !listData.totalElements) return "0";
        const firstNum = listData.first ? 1 : listData.number * listData.size + 1;
        const secondNum = listData.last ? listData.totalElements : firstNum + listData.size - 1;
        const pageRange = `${firstNum} - ${secondNum}`;

        switch (format) {
            case "nocount":
                return this.translatePipe.transform("ShowingPageRange", { PageRange: pageRange });

            default:
                return this.translatePipe.transform("ShowingPageRangeOfTotalItemCount", { PageRange: pageRange, TotalItemCount: listData.totalElements });
        }
    }
}
