// 3rd Party
import {
    Pipe,
    PipeTransform,
} from "@angular/core";
import {
    capitalize,
    head,
    join,
    map,
    split,
} from "lodash";

@Pipe({
    name: "otUserInitials",
})

export class OtUserInitialsPipe implements PipeTransform {

    transform(fullName: string): string {
        let initials = "";
        if (fullName) {
            const nameArray = split(fullName, " ");
            initials = join(map(nameArray, (name, index) => {
                if (index === 0 || index === nameArray.length - 1) {
                    return capitalize(head(name));
                }
            }), "");
        }
        return initials;
    }
}
