// Services
import { Permissions } from "modules/shared/services/helper/permissions.service";
import { GlobalSidebarService } from "modules/global-sidebar/services/one-global-sidebar.service";

export default function routes($stateProvider: ng.ui.IStateProvider) {
    $stateProvider
        .state("zen.app.pia.module.globalreadiness", {
            abstract: true,
            url: "global-readiness",
            resolve: {
                GlobalReadinessPermission: ["Permissions",
                    (permissions: Permissions): boolean => {
                        if (permissions.canShow("ReadinessAssessmentV2")) return true;
                        permissions.goToFallback();
                        return false;
                    },
                ],
            },
            views: {
                module: {
                    template: "<global-readiness-wrapper></global-readiness-wrapper>",
                    controller: [
                        "GlobalSidebarService",
                        (globalSidebarService: GlobalSidebarService) => {
                            globalSidebarService.set();
                        },
                    ],
                },
            },
        })
        .state("zen.app.pia.module.globalreadiness.views", {
            abstract: true,
            url: "",
            views: {
                global_readiness_body: {
                    template: "<ui-view></ui-view>",
                },
            },
        })
        .state("zen.app.pia.module.globalreadiness.views.welcome", {
            url: "/welcome",
            params: { time: null },
            template: "<downgrade-global-readiness-welcome></downgrade-global-readiness-welcome>",
        })
        .state("zen.app.pia.module.globalreadiness.views.start", {
            url: "/start",
            params: {
                time: null,
                graTemplateId: null,
            },
            template: "<downgrade-global-readiness-launch></downgrade-global-readiness-launch>",
        })
        .state("zen.app.pia.module.globalreadiness.views.dashboard", {
            url: "/dashboard",
            params: { time: null },
            template: "<downgrade-global-readiness-dashboard></downgrade-global-readiness-dashboard>",
        })
        .state("zen.app.pia.module.globalreadiness.views.assessmentreport", {
            url: "/assessment/report/:assessmentId",
            params: {
                time: null,
                assessmentId: null,
            },
            template: "<downgrade-global-readiness-assessment-report></downgrade-global-readiness-assessment-report>",
        })
        .state("zen.app.pia.module.globalreadiness.views.assessments", {
            url: "/assessments",
            params: { time: null },
            template: "<downgrade-global-readiness-assessments></downgrade-global-readiness-assessments>",
        });
}

routes.$inject = ["$stateProvider"];
