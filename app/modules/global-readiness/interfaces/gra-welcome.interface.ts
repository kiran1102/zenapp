import { IQuestionAttributes } from "interfaces/assessment-template-question.interface";

export interface ISeededGRATemplateDetail {
    id: string;
    name: string;
    description: string;
    templateType: string;
}

export interface IFilteredGRATemplateDetail {
    title: {
        text: string,
    };
    id: string;
    content: string;
    templateType?: string;
}

export interface IGRATemplateCardsResponse {
    actions?: any[];
    active?: any;
    businessKeyReference?: any;
    description: string;
    friendlyName: string;
    icon: string;
    id: string;
    inventoryRelationshipsByQuestionId?: string;
    name: string;
    navigations?: any[];
    nextTemplateId?: string;
    orgGroupId?: string;
    parentChildQuestionIds?: any;
    previousTemplateId?: string;
    rootVersionId?: string;
    sections: IGRATemplatePreviewSection[];
    settings?: any;
    status: string;
    templateType: string;
    templateVersion: number;
    valid?: boolean;
    welcomeText: string;
}

export interface IGRATemplatePreviewSection {
    id: string;
    invalidQuestionCount: number;
    isSectionExpanded?: boolean;
    name: string;
    questions: IGRATemplatePreviewQuestion[];
    rootVersionId: string;
    sequence: number;
}

export interface IGRATemplatePreviewQuestion {
    attributes: any;
    attributeJson: IQuestionAttributes;
    content: string;
    description: string;
    friendlyName: string;
    hint: string;
    id: string;
    isPreviewShown?: boolean;
    linkAssessmentToInventory?: boolean;
    options?: IGRATemplatePreviewQuestionOption[];
    prePopulateResponse: boolean;
    questionType: string;
    required: boolean;
    rootVersionId: string;
    sequence: number;
    valid: boolean;
}

export interface IGRATemplatePreviewQuestionOption {
    attributes?: string;
    attributeJson?: {
        hint?: string;
    };
    id: string;
    option: string;
    optionType: string;
    sequence: number;
}
