import { IHorseshoeGraphData } from "interfaces/one-horseshoe-graph.interface";
import { IDropdownOption } from "interfaces/one-dropdown.interface";

export interface IGlobalReadinessDashboardResponse {
    id: string;
    name: string;
    readinessScore: string;
    completionScore: string;
    answeredQuestionsCount: number;
    unAnsweredQuestionsCount: number;
    readyQuestionsCount: number;
    gapsIdentified: number;
}

export interface IGlobalReadinessDashboardGraph {
    assessment: IGlobalReadinessDashboardResponse;
    title: string;
    graphData: IHorseshoeGraphData;
    countText?: string;
    dropDownMenu?: IDropdownOption[];
}
