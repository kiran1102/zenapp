import { GRAQuestionStatus } from "modules/global-readiness/enums/gra.enum";

export interface IGRAOverallReadinessSummary {
    assessmentName: string;
    templateType: string;
    statisticsSummary: IGRAResponseSummaryDetail;
    lawStatisticsDetails?: IGRAApplicableLawReadiness[];
}

export interface IGRAApplicableLawReadiness {
    lawName: string;
    lawStatisticsSummary: IGRAResponseSummaryDetail;
}

export interface IGRAResponseSummaryDetail {
    readinessScore: number;
    gapsIdentified: number;
    unAnsweredQuestionsCount: number;
    readyQuestionsCount: number;
}

export interface IGRASectionSummaryResponse {
    sectionSummaryDtos: IGRASectionSummary[];
    templateType: string;
}

export interface IGRASectionSummary {
    sectionId: string;
    sectionName: string;
    ready: number;
    gapIdentified: number;
    unanswered: number;
    questionDtos: IGRAQuestionSummary[];
}

export interface IGRAQuestionSummary {
    questionId: string;
    questionName: string;
    status: GRAQuestionStatus;
    laws: string;
    response: string;
    recommendation: string;
}
