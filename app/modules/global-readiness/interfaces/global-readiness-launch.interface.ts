// Enums
import { progressIndicatorState } from "modules/global-readiness/enums/gra-progress-indicator-state.enums";

export interface IRegionDetail {
    name: string;
    isChecked: boolean;
    regionId: number;
    zones: IZoneDetail[];
    countries: ICountryDetail[];
}

export interface ICountryDetail {
    code: string;
    name: string;
    isChecked: boolean;
    countryId: number;
    zoneIds: number[];
    regionId: number;
    lawIds: number[];
}

export interface IZoneDetail {
    name: string;
    isChecked: boolean;
    zoneId: number;
    regionId: number;
    countryIds: number[];
}

export interface IRegionCardEventEmitDetail {
    areaType: string;
    isChecked: boolean;
    countryId?: number;
    zoneId?: number;
    regionId?: number;
}

export interface IFrameworkLawDetail {
    lawId: number;
    isChecked: boolean;
    lawName: string;
    lawDescription: string;
    countryIds: number[];
    displayName: string;
}

export interface IGRAStartAssessmentModalData {
    modalTitle: string;
    startAssessmentRequest: IGRAStartAssessmentRequest;
    callback?: (res) => void;
}

export interface IGRAStartAssessmentRequest {
    name: string;
    templateType: string;
    templateId: string;
    laws: string[];
    orgGroupId: string;
    orgGroupName: string;
}

export interface IProgressIndicatorState {
    text: string;
    state?: progressIndicatorState;
}
