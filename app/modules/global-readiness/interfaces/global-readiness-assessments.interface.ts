import { IPageableMeta } from "interfaces/pagination.interface";
import { IDropdownOption } from "interfaces/one-dropdown.interface";

export interface IAssessmentTableRow extends IPageableMeta {
    content: IGlobalReadinessAssessmentRecord[];
}

export interface IGlobalReadinessAssessmentRecord {
    assessmentId: string;
    name: string;
    readinessScore: number;
    completionScore: number;
    createDT: string;
    updateDT: string;
    templateName: string;
    displayOnReadinessDashboard?: boolean;
    dropDownMenu?: IDropdownOption[];
}

export interface IGRAAssessmentReadinessLawGuidance {
    questionId: string;
    topic: string;
    laws: string[];
    guidance: IAssessmentLawGuidanceDetail[];
}

export interface IAssessmentLawGuidanceDetail {
    law: string;
    detail: string;
}

export interface IGRAShowGuidanceModalData {
    modalTitle: string;
    questionId: string;
}