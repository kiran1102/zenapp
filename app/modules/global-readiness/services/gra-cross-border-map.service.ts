// 3rd party
import { Injectable } from "@angular/core";
import * as d3 from "d3";

// Interfaces
import { IStringMap } from "interfaces/generic.interface";

// Constants
import { GRAMapColor } from "modules/global-readiness/constants/gra.constants";

@Injectable()
export class GRACrossBorderService {

    private map: any;
    private crossBorderMap: any;

    constructor() { }

    initMap(containerId: string, mapData: IStringMap<string>) {
        import(/* webpackChunkName: "datamaps" */ "datamaps").then((m) => {
            this.crossBorderMap = m.default;
            this.map = this.drawMap(containerId, mapData);
            this.setMapZoom(this.map);
        });
    }

    updateMapHighlights(mapData: IStringMap<string>) {
        if (this.map) {
            this.map.updateChoropleth(mapData);
        }
    }

    resizeMap(): void {
        if (this.map) {
            this.map.resize();
        }
    }

    destroyMap(): void {
        this.map = null;
    }

    private drawMap(containerId: string, mapData: IStringMap<string>): any {
        const mapConfig: any = this.getMapConfig(containerId, mapData);
        return new this.crossBorderMap(mapConfig);
    }

    private getMapConfig(containerId: string, mapData: IStringMap<string>): any {
        return {
            element: document.getElementById(containerId),
            projection: "mercator",
            geographyConfig: {
                dataUrl: null,
                popupOnHover: false,
                highlightOnHover: false,
                borderColor: GRAMapColor.defaultBorderColor,
                borderWidth: 1.0,
                highlightFillColor: GRAMapColor.highlightFillColor,
                highlightBorderColor: GRAMapColor.highlightBorderColor,
                highlightBorderWidth: 1,
            },
            responsive: true,
            fills: {
                defaultFill: GRAMapColor.defaultFillColor,
            },
            data: mapData,
        };
    }

    private setMapZoom(map: any): void {
        map.svg.call(d3.behavior.zoom()
            .scaleExtent([1, 5])
            .on("zoom", (): void => {
                map.svg.selectAll("g").attr("transform", "translate(" + d3.event.translate + ")scale(" + d3.event.scale + ")");
                d3.event.sourceEvent.stopPropagation();
            }));
    }
}
