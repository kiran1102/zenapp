import { Injectable } from "@angular/core";

// Services
import { GlobalReadinessLaunchApiService } from "modules/global-readiness/services/api/global-readiness-launch-api.service";

// Interfaces
import { IProtocolResponse } from "interfaces/protocol.interface";
import { IGRAStartAssessmentRequest } from "modules/global-readiness/interfaces/global-readiness-launch.interface";

@Injectable()
export class GlobalReadinessLaunchActionService {

    constructor(
        private globalReadinessLaunchApiService: GlobalReadinessLaunchApiService,
    ) { }

    getRegionLawData(): ng.IPromise<any> {
        return this.globalReadinessLaunchApiService.getRegionLawData().then((res: IProtocolResponse<any>): any => {
            if (res.result) {
                return res.data;
            }
            return null;
        });
    }

    startAssessment(assessmentData: IGRAStartAssessmentRequest): ng.IPromise<IProtocolResponse<any>> {
        return this.globalReadinessLaunchApiService.startAssessment(assessmentData);
    }
}
