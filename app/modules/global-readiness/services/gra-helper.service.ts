// Angular
import { Injectable } from "@angular/core";

// 3rd Party
import { round } from "lodash";

// Interfaces
import {
    IRegionDetail,
    ICountryDetail,
    IZoneDetail,
    IFrameworkLawDetail,
    IProgressIndicatorState,
} from "modules/global-readiness/interfaces/global-readiness-launch.interface";
import { ProgressIndicatorLabels } from "@onetrust/vitreus";
import {
    IGlobalReadinessDashboardResponse,
    IGlobalReadinessDashboardGraph,
} from "modules/global-readiness/interfaces/global-readiness-dashboard.interface";
import { IStringMap } from "interfaces/generic.interface";
import { IHorseshoeGraphData } from "interfaces/one-horseshoe-graph.interface";
import { IGRAResponseSummaryDetail } from "modules/global-readiness/interfaces/gra-report.interface";

// Rxjs
import { BehaviorSubject } from "rxjs";

// Enums
import { progressIndicatorState } from "modules/global-readiness/enums/gra-progress-indicator-state.enums";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

// Constants
import { GRARegionLawResponseKeys } from "modules/global-readiness/constants/gra.constants";

@Injectable()
export class GRAHelperService {

    createAssessmentSteps: IStringMap<string> = {
        step1: this.translatePipe.transform("CountriesAndRegions"),
        step2: this.translatePipe.transform("FrameworksAndLaws"),
        step3: this.translatePipe.transform("StartYourAssessment"),
    };
    ProgressIndicatorLabels: ProgressIndicatorLabels = {
        completed: "Completed",
        active: "Active",
        progress: "Progress",
        error: "ERROR",
    };
    progressState$ = new BehaviorSubject<IProgressIndicatorState[]>([
        { text: this.createAssessmentSteps.step1, state: progressIndicatorState.ACTIVE },
        { text: this.createAssessmentSteps.step2 },
        { text: this.createAssessmentSteps.step3 },
    ]);

    private regionDataList: IRegionDetail[];
    private frameworkLawDataList: IFrameworkLawDetail[];

    constructor(
        private readonly translatePipe: TranslatePipe,
    ) { }

    massageRegionLawResponse(regionLawResponse: any): [IRegionDetail[], IFrameworkLawDetail[]] {
        this.regionDataList = [];
        this.frameworkLawDataList = [];
        if (regionLawResponse) {
            // CONTRY REGION DATA
            regionLawResponse[GRARegionLawResponseKeys.regionAllIds].forEach((currentRegionId: number) => {
                const region: IRegionDetail = {
                    regionId: currentRegionId,
                    name: regionLawResponse[GRARegionLawResponseKeys.regionById][currentRegionId].name,
                    isChecked: false,
                    zones: [],
                    countries: [],
                };
                // create and add countries to a region
                if (regionLawResponse[GRARegionLawResponseKeys.regionIdCountryIdMap][currentRegionId]) {
                    regionLawResponse[GRARegionLawResponseKeys.regionIdCountryIdMap][currentRegionId].forEach((currentCountryId: number) => {
                        const country: ICountryDetail = {
                            code: regionLawResponse[GRARegionLawResponseKeys.countryById][currentCountryId].code,
                            name: regionLawResponse[GRARegionLawResponseKeys.countryById][currentCountryId].name,
                            isChecked: false,
                            countryId: currentCountryId,
                            zoneIds: this.getZonesForGivenCountry(currentCountryId, regionLawResponse),
                            regionId: regionLawResponse[GRARegionLawResponseKeys.countryById][currentCountryId].regionId,
                            lawIds: regionLawResponse[GRARegionLawResponseKeys.countryIdLawIdMap][currentCountryId]
                                ? regionLawResponse[GRARegionLawResponseKeys.countryIdLawIdMap][currentCountryId]
                                : [],
                        };
                        region.countries.push(country);
                    });
                }
                // Create and add zones to a region
                if (regionLawResponse[GRARegionLawResponseKeys.regionIdZoneIdMap][currentRegionId]) {
                    regionLawResponse[GRARegionLawResponseKeys.regionIdZoneIdMap][currentRegionId].forEach((currentZoneId: number) => {
                        const zone: IZoneDetail = {
                            name: regionLawResponse[GRARegionLawResponseKeys.zoneById][currentZoneId].name,
                            isChecked: false,
                            zoneId: currentZoneId,
                            regionId: currentRegionId,
                            countryIds: regionLawResponse[GRARegionLawResponseKeys.zoneIdCountryIdMap][currentZoneId],
                        };
                        region.zones.push(zone);
                    });
                }
                this.regionDataList.push(region);
            });

            // FRAMEWORK LAW DATA
            regionLawResponse[GRARegionLawResponseKeys.lawAllIds].forEach((lawId: number) => {
                const law: IFrameworkLawDetail = {
                    lawId: regionLawResponse[GRARegionLawResponseKeys.lawById][lawId].id,
                    isChecked: false,
                    lawName: regionLawResponse[GRARegionLawResponseKeys.lawById][lawId].name,
                    lawDescription: regionLawResponse[GRARegionLawResponseKeys.lawById][lawId].description,
                    countryIds: this.getCountriesForGivenLaw(lawId, regionLawResponse),
                    displayName: regionLawResponse[GRARegionLawResponseKeys.lawById][lawId].displayName,
                };
                this.frameworkLawDataList.push(law);
            });
        }
        return [this.regionDataList, this.frameworkLawDataList];
    }

    getCountriesForGivenLaw(lawId: number, regionLawResponse: any): number[] {
        const countryIds: number[] = [];
        regionLawResponse[GRARegionLawResponseKeys.countryAllIds].forEach((currentCountryId: number) => {
            if (regionLawResponse[GRARegionLawResponseKeys.countryIdLawIdMap][currentCountryId] &&
                regionLawResponse[GRARegionLawResponseKeys.countryIdLawIdMap][currentCountryId].includes(lawId)) countryIds.push(currentCountryId);
        });
        return countryIds;
    }

    getZonesForGivenCountry(countryId: number, regionLawResponse: any): number[] {
        const zoneIds: number[] = [];
        regionLawResponse[GRARegionLawResponseKeys.zoneAllIds].forEach((currentZoneId: number) => {
            if (regionLawResponse[GRARegionLawResponseKeys.zoneIdCountryIdMap][currentZoneId] &&
                regionLawResponse[GRARegionLawResponseKeys.zoneIdCountryIdMap][currentZoneId].includes(countryId)) zoneIds.push(currentZoneId);
        });
        return zoneIds;
    }

    generateDashboardGraphData(assessments: IGlobalReadinessDashboardResponse[]): IGlobalReadinessDashboardGraph[] {
        return assessments.map((assessment): IGlobalReadinessDashboardGraph => {
            if (Object.values(assessment).includes(null)) {
                return {
                    assessment,
                    title: "0%",
                    graphData: {
                        series: [
                            {
                                graphClass: "stroke-grey-1",
                                label: this.translatePipe.transform("Evaluating"),
                                value: 1,
                            },
                        ],
                    },
                    countText: this.translatePipe.transform("Evaluating"),
                };
            }
            let totalQuestions = assessment.readyQuestionsCount + assessment.unAnsweredQuestionsCount + assessment.gapsIdentified;
            if (totalQuestions === 0) totalQuestions = 1;
            const readyValue = assessment.readyQuestionsCount / totalQuestions;
            const gapsValue = assessment.gapsIdentified / totalQuestions;
            const unansweredValue = assessment.unAnsweredQuestionsCount / totalQuestions;
            return {
                assessment,
                title: `${+assessment.readinessScore}%`,
                graphData: {
                    series: [
                        {
                            graphClass: "stroke-green",
                            label: `${round(readyValue, 4) * 100}% ` + this.translatePipe.transform("Ready"),
                            value: readyValue,
                        },
                        {
                            graphClass: "stroke-orange",
                            label: `${round(gapsValue, 4) * 100}% ` + this.translatePipe.transform("GapsIdentified"),
                            value: gapsValue,
                        },
                        {
                            graphClass: "stroke-grey-1",
                            label: `${round(unansweredValue, 4) * 100}% ` + this.translatePipe.transform("Unanswered"),
                            value: unansweredValue,
                        },
                    ],
                },
                countText: `${assessment.completionScore} ` + this.translatePipe.transform("Answered"),
            };
        });
    }

    generateAssessmentSummaryGraphData(summaryDetail: IGRAResponseSummaryDetail): IHorseshoeGraphData {
        let totalQuestions = summaryDetail.readyQuestionsCount + summaryDetail.unAnsweredQuestionsCount + summaryDetail.gapsIdentified;
        if (totalQuestions === 0) totalQuestions = 1;
        const readyValue = summaryDetail.readyQuestionsCount / totalQuestions;
        const gapsValue = summaryDetail.gapsIdentified / totalQuestions;
        const unansweredValue = summaryDetail.unAnsweredQuestionsCount / totalQuestions;
        return {
            series: [
                {
                    graphClass: "stroke-green",
                    label: `${round(readyValue, 4) * 100}% ` + this.translatePipe.transform("Ready"),
                    value: readyValue,
                },
                {
                    graphClass: "stroke-orange",
                    label: `${round(gapsValue, 4) * 100}% ` + this.translatePipe.transform("GapsIdentified"),
                    value: gapsValue,
                },
                {
                    graphClass: "stroke-grey-1",
                    label: `${round(unansweredValue, 4) * 100}% ` + this.translatePipe.transform("Unanswered"),
                    value: unansweredValue,
                },
            ],
        };
    }

    // progress bar
    onStateChange(tabState: string): void {
        switch (tabState) {
            case this.createAssessmentSteps.step1:
                this.progressState$.next([
                    { text: this.createAssessmentSteps.step1, state: progressIndicatorState.ACTIVE },
                    { text: this.createAssessmentSteps.step2 },
                    { text: this.createAssessmentSteps.step3 },
                ]);
                break;
            case this.createAssessmentSteps.step2:
                this.progressState$.next([
                    { text: this.createAssessmentSteps.step1, state: progressIndicatorState.COMPLETED },
                    { text: this.createAssessmentSteps.step2, state: progressIndicatorState.ACTIVE },
                    { text: this.createAssessmentSteps.step3 },
                ]);
                break;
            case this.createAssessmentSteps.step3:
                this.progressState$.next([
                    { text: this.createAssessmentSteps.step1, state: progressIndicatorState.COMPLETED },
                    { text: this.createAssessmentSteps.step2, state: progressIndicatorState.COMPLETED },
                    { text: this.createAssessmentSteps.step3, state: progressIndicatorState.ACTIVE },
                ]);
                break;
        }
    }
}
