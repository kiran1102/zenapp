import { Injectable } from "@angular/core";

// Services
import { ProtocolService } from "modules/core/services/protocol.service";

// Interfaces
import {
    IProtocolConfig,
    IProtocolMessages,
    IProtocolResponse,
} from "interfaces/protocol.interface";
import {
    IAssessmentTableRow,
    IGRAAssessmentReadinessLawGuidance,
} from "modules/global-readiness/interfaces/global-readiness-assessments.interface";
import { IGridPaginationParams } from "interfaces/grid.interface";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

@Injectable()
export class GRAAssessmentApiService {

    constructor(
        private OneProtocol: ProtocolService,
        private translatePipe: TranslatePipe,
    ) { }

    getReadinessAssessments(text: string, filter: string, requestParam: IGridPaginationParams): ng.IPromise<IProtocolResponse<IAssessmentTableRow>> {
        const requestBody = {
            text,
            filter,
            ...requestParam,
        };
        const config: IProtocolConfig = this.OneProtocol.customConfig("GET", `/assessment-v2/v2/assessments/readiness/assessments`, requestBody);
        const messages: IProtocolMessages = {
            Error: { custom: this.translatePipe.transform("ErrorFetchingReadinessAssesments") },
        };
        return this.OneProtocol.http(config, messages);
    }

    getReadinessAssessmentLawGuidance(questionId: string, laws: string[]): ng.IPromise<IProtocolResponse<IGRAAssessmentReadinessLawGuidance>> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("POST", `/api/template/v1/templates/readiness/${questionId}/lawguidance`, null, laws);
        const messages: IProtocolMessages = {
            Error: { custom: this.translatePipe.transform("ErrorFetchingReadinessAssesmentLawGuidance") },
        };
        return this.OneProtocol.http(config, messages);
    }
}
