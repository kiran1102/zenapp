// Angular
import { Injectable } from "@angular/core";

import {
    Observable,
    from,
} from "rxjs";

// Services
import { ProtocolService } from "modules/core/services/protocol.service";

// Interfaces
import {
    IProtocolConfig,
    IProtocolMessages,
    IProtocolResponse,
} from "interfaces/protocol.interface";
import { IGlobalReadinessDashboardResponse } from "modules/global-readiness/interfaces/global-readiness-dashboard.interface";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

@Injectable()
export class GlobalReadinessDashboardApiService {

    constructor(
        private OneProtocol: ProtocolService,
        private translatePipe: TranslatePipe,
    ) { }

    getDashboardData(): Observable<IGlobalReadinessDashboardResponse[]> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("GET", `/assessment-v2/v2/assessments/readiness/dashboard`);
        const messages: IProtocolMessages = {
            Error: { custom: this.translatePipe.transform("ErrorFetchingDashboardData") },
        };
        return from(this.OneProtocol.http(config, messages).then((response: IProtocolResponse<IGlobalReadinessDashboardResponse[]>) => response.result ? response.data : []));
    }

    showHideAssessmentsFromDashboard(assessmentId: string, payload: { displayOnReadinessDashboard: boolean }): Observable<boolean> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("POST", `/assessment-v2/v2/assessments/${assessmentId}/settings`, null, payload);
        const messages: IProtocolMessages = {
            Error: { custom: this.translatePipe.transform("ErrorUpdatingShowHideOnAssessment") },
        };
        return from(this.OneProtocol.http(config, messages).then((response: IProtocolResponse<boolean>) => response.result));
    }
}
