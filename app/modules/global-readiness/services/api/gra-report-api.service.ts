// Angular
import { Injectable } from "@angular/core";

// External
import {
    Observable,
    from,
} from "rxjs";

// Services
import { ProtocolService } from "modules/core/services/protocol.service";

// Interfaces
import {
    IGRASectionSummaryResponse,
    IGRAOverallReadinessSummary,
} from "modules/global-readiness/interfaces/gra-report.interface";
import {
    IProtocolConfig,
    IProtocolMessages,
    IProtocolResponse,
} from "interfaces/protocol.interface";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

@Injectable()
export class GRAReportApiService {

    constructor(
        private readonly OneProtocol: ProtocolService,
        private readonly translatePipe: TranslatePipe,
    ) { }

    getAssessmentReportSummary(assessmentId: string): Observable<IGRASectionSummaryResponse> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("GET", `/assessment-v2/v2/assessments/readiness/${assessmentId}/summary`);
        const messages: IProtocolMessages = {
            Error: { custom: this.translatePipe.transform("ErrorFetchGRAReportSummary") },
        };
        return from(this.OneProtocol.http(config, messages)
            .then((response: IProtocolResponse<IGRASectionSummaryResponse>) => response.result ? response.data : null));
    }

    getAssessmentOverallReadiness(assessmentId: string): Observable<IGRAOverallReadinessSummary> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("POST", `/assessment-v2/v2/assessments/readiness/${assessmentId}/statistics`, null, null);
        const messages: IProtocolMessages = {
            Error: { custom: this.translatePipe.transform("ErrorFetchingGRAOverallSummary") },
        };
        return from(this.OneProtocol.http(config, messages)
            .then((response: IProtocolResponse<IGRAOverallReadinessSummary>) => response.result ? response.data : null));
    }
}
