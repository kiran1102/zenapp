// TODO: Needs to be updated when the backend services are up.
import { Injectable } from "@angular/core";

// Services
import { ProtocolService } from "modules/core/services/protocol.service";

// Interfaces
import {
    IProtocolConfig,
    IProtocolMessages,
    IProtocolResponse,
} from "interfaces/protocol.interface";
import { IGRAStartAssessmentRequest } from "modules/global-readiness/interfaces/global-readiness-launch.interface";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

@Injectable()
export class GlobalReadinessLaunchApiService {

    constructor(
        private OneProtocol: ProtocolService,
        private translatePipe: TranslatePipe,
    ) { }

    getRegionLawData(): ng.IPromise<IProtocolResponse<any>> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("GET", `/api/gallery/v1/gallery/regionlaw`);
        const messages: IProtocolMessages = {
            Error: { custom: this.translatePipe.transform("ErrorFetchingRegionLawData") },
        };
        return this.OneProtocol.http(config, messages);
    }

    startAssessment(assessmentDetails: IGRAStartAssessmentRequest): ng.IPromise<IProtocolResponse<any>> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("POST", `/assessment-v2/v2/assessments/readiness`, null, assessmentDetails);
        const messages: IProtocolMessages = {
            Error: { custom: this.translatePipe.transform("ErrorLaunchingNewAssessment") },
        };
        return this.OneProtocol.http(config, messages);
    }
}
