import { Injectable } from "@angular/core";

// External Libraries
import { Observable, from } from "rxjs";
import { map } from "rxjs/operators";

// Services
import { ProtocolService } from "modules/core/services/protocol.service";

// Interfaces
import {
    IProtocolConfig,
    IProtocolMessages,
    IProtocolResponse,
} from "interfaces/protocol.interface";
import { ISeededGRATemplateDetail } from "modules/global-readiness/interfaces/gra-welcome.interface";

// Constants
import { TemplateTypes } from "constants/template-types.constant";
import { ALPHA_NUMERIC } from "constants/regex.constant";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

@Injectable()
export class GRAWelcomeApiService {

    constructor(
        private OneProtocol: ProtocolService,
        private translatePipe: TranslatePipe,
    ) { }

    getSeededTemplateDetails(): Observable<ISeededGRATemplateDetail[]> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("GET", `/api/template/v1/templates/readiness`);
        const messages: IProtocolMessages = {
            Error: { custom: this.translatePipe.transform("ErrorFetchingReadinessAssesments") },
        };
        return from(this.OneProtocol.http(config, messages)).pipe(
            map((response: IProtocolResponse<ISeededGRATemplateDetail[]>) => response.result ? response.data : []),
            map((templates) => this.updateRATranslations(templates)));
    }

    private updateRATranslations(templates: ISeededGRATemplateDetail[]): ISeededGRATemplateDetail[] {
        return templates.map((template) => {
            if (template.templateType === TemplateTypes.RA) {
                const translationPrefix = template.name.replace(ALPHA_NUMERIC, "");
                template.name = this.translatePipe.transform(`${translationPrefix}-Name`);
                template.description = this.translatePipe.transform(`${translationPrefix}-Description`);
            }
            return template;
        });
    }
}
