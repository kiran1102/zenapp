export const GRAMapColor = {
    defaultFillColor: "#F9F5ED",
    defaultBorderColor: "#BBBFC1",
    highlightFillColor: "#FACAC1",
    highlightBorderColor: "#FC4F45",
};

export const GRARegionLawResponseKeys = {
    regionAllIds: "regionAllIds",
    regionById: "regionById",
    zoneAllIds: "zoneAllIds",
    zoneById: "zoneById",
    countryAllIds: "countryAllIds",
    countryById: "countryById",
    lawAllIds: "lawAllIds",
    lawById: "lawById",
    regionIdCountryIdMap: "regionIdCountryIdMap",
    regionIdZoneIdMap: "regionIdZoneIdMap",
    zoneIdCountryIdMap: "zoneIdCountryIdMap",
    countryIdLawIdMap: "countryIdLawIdMap",
};
