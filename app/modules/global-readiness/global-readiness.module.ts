import {
    NgModule,
} from "@angular/core";

// Modules
import { SharedModule } from "sharedModules/shared.module";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";
import {
    OtBreadcrumbModule,
    OtButtonModule,
    OtCardModule,
    OtCheckboxModule,
    OtContextMenuModule,
    OtDataTableModule,
    OtDrawerModule,
    OtEmptyStateModule,
    OtFormModule,
    OtInputModule,
    OtLoadingModule,
    OtPageFooterModule,
    OtPageHeaderModule,
    OtPaginationModule,
    OtProgressIndicatorModule,
    OtSearchModule,
    OtTooltipModule,
} from "@onetrust/vitreus";
import { OTPipesModule } from "modules/pipes/pipes.module";
import { TemplateModule } from "modules/template/template.module";
import { TranslateModule } from "@ngx-translate/core";

// Components
import * as c from "./components";
import { GlobalReadinessWelcomePreviewComponent } from "modules/global-readiness/components/welcome/global-readiness-welcome-preview/global-readiness-welcome-preview.component";
import { GlobalReadinessWelcomeQuestionPreviewComponent } from "modules/global-readiness/components/welcome/global-readiness-welcome-question-preview/global-readiness-welcome-question-preview.component";
import { GlobalReadinessWelcomeStartPaneComponent } from "modules/global-readiness/components/welcome/global-readiness-welcome-start-pane/global-readiness-welcome-start-pane.component";

// Services
import * as services from "./services";

const COMPONENTS_TO_BE_DOWNGRADED = [
    c.GlobalReadinessAssessmentsListComponent,
    c.GlobalReadinessDashboardComponent,
    c.GlobalReadinessWelcomeComponent,
    c.GlobalReadinessAssessmentsTableComponent,
    c.GlobalReadinessLaunchComponent,
    c.GRASeededAssessmentCard,
    c.GRAStartAssessmentModalComponent,
    c.GlobalReadinessAssessmentReport,
    c.GRAShowGuidanceModalComponent,

];

const COMPONENTS_TO_BE_UPGRADED = [
];

const COMPONENTS = [
    c.GRACrossBorderCard,
    c.GRACrossBorderMap,
    c.RegionCardComponent,
    c.FrameworkLawCardComponent,
    c.GRAResponseSummaryCardComponent,
    c.GRAReportSectionSummaryComponent,
    c.GRAReportQuestionSummaryComponent,
    c.GRAApplicableLawReadinessComponent,
    GlobalReadinessWelcomePreviewComponent,
    GlobalReadinessWelcomeQuestionPreviewComponent,
    GlobalReadinessWelcomeStartPaneComponent,
];

const SERVICES = [
    services.GlobalReadinessDashboardApiService,
    services.GlobalReadinessLaunchActionService,
    services.GlobalReadinessLaunchApiService,
    services.GRACrossBorderService,
    services.GRAHelperService,
    services.GRAReportApiService,
    services.GRAWelcomeApiService,
    services.GRAAssessmentApiService,
];

const LEGACY_SERVICES = [
];

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        OTPipesModule,
        OtBreadcrumbModule,
        OtButtonModule,
        OtCardModule,
        OtCheckboxModule,
        OtContextMenuModule,
        OtDataTableModule,
        OtDrawerModule,
        OtEmptyStateModule,
        OtFormModule,
        OtInputModule,
        OtLoadingModule,
        OtPageFooterModule,
        OtPageHeaderModule,
        OtPaginationModule,
        OtProgressIndicatorModule,
        OtSearchModule,
        OtTooltipModule,
        SharedModule,
        TemplateModule,
        TranslateModule,
    ],
    declarations: [
        ...COMPONENTS,
        ...COMPONENTS_TO_BE_DOWNGRADED,
        ...COMPONENTS_TO_BE_UPGRADED,
    ],
    entryComponents: [
        ...COMPONENTS_TO_BE_DOWNGRADED,
    ],
    providers: [
        ...SERVICES,
        ...LEGACY_SERVICES,
    ],
})
export class GlobalReadinessModule { }
