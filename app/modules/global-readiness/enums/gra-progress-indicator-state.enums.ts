export enum progressIndicatorState {
    COMPLETED = 0,
    ACTIVE = 1,
    ERROR = 2,
}
