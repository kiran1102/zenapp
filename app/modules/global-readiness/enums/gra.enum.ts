export enum GRAQuestionStatus {
    UNANSWERED = "UNANSWERED",
    ANSWERED = "ANSWERED",
    RISK_FOUND = "RISK_FOUND",
}

export enum GRADashboardMenuActions {
    HIDE = 10,
    SHOW = 20,
    DELETE = 30,
}
