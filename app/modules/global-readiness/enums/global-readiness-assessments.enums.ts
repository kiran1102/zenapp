export enum GlobalReadinessAssessmentsTableColumns {
    NAME = "name",
    CREATED_DATE = "createDT",
    UPDATE_DATE = "updateDT",
    TEMPLATE = "templateName",
}
