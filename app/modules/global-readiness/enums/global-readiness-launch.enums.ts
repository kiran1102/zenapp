export enum GlobalReadinessAreaTypes {
    REGION = "REGION",
    ZONE = "ZONE",
    COUNTRY = "COUNTRY",
}
