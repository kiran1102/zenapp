declare var angular: any;

import routes from "./global-readiness-legacy.routes";

import * as c from "./components/index";

export const globalReadinessLegacyModule = angular.module("zen.global-readiness", [
    "ui.router",
])
    .config(routes)
    .component("globalReadinessWrapper", c.GlobalReadinessComponent)
    ;
