import {
    Component,
    Input,
    AfterViewInit,
    OnDestroy,
} from "@angular/core";

// Interfaces
import { IStringMap } from "interfaces/generic.interface";

// Services
import { GRACrossBorderService } from "modules/global-readiness/services/gra-cross-border-map.service";

@Component({
    selector: "gra-cross-border-map",
    template: `<div class="full-size-block" [ngStyle]="{'pointer-events': 'none'}" [attr.id]="mapId"></div>`,
})

export class GRACrossBorderMap implements AfterViewInit, OnDestroy {

    @Input() mapId: string;
    @Input() mapData: IStringMap<string>;

    constructor(
        readonly crossBorderService: GRACrossBorderService,
    ) {}

    ngAfterViewInit() {
        this.drawMap();
        window.addEventListener("resize", this.handleMapResizeEvent);
    }

    ngOnDestroy() {
        window.removeEventListener("resize", this.handleMapResizeEvent);
        this.crossBorderService.destroyMap();
    }

    handleMapResizeEvent = () => {
        this.crossBorderService.resizeMap();
    }

    private drawMap(): void {
        this.crossBorderService.initMap(this.mapId, this.mapData);
    }
}
