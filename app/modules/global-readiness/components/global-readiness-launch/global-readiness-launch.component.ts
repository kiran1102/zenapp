// Angular
import {
    Component,
    OnInit,
    Inject,
} from "@angular/core";
import { StateService } from "@uirouter/core";

// 3rd party
import {
    union,
    forEach,
} from "lodash";

// Redux
import {
    getCurrentOrgId,
    getOrgById,
} from "oneRedux/reducers/org.reducer";
import { StoreToken } from "tokens/redux-store.token";
import { IStore } from "interfaces/redux.interface";

// Interface
import {
    IRegionDetail,
    IZoneDetail,
    ICountryDetail,
    IRegionCardEventEmitDetail,
    IFrameworkLawDetail,
    IGRAStartAssessmentModalData,
} from "modules/global-readiness/interfaces/global-readiness-launch.interface";
import {
    IStringMap,
    INumberMap,
} from "interfaces/generic.interface";

// Service
import { ModalService } from "sharedServices/modal.service";
import { GRACrossBorderService } from "modules/global-readiness/services/gra-cross-border-map.service";
import { GlobalReadinessLaunchActionService } from "modules/global-readiness/services/actions/global-readiness-launch-action.service";
import { GRAHelperService } from "modules/global-readiness/services/gra-helper.service";

// Enums
import { GlobalReadinessAreaTypes } from "modules/global-readiness/enums/global-readiness-launch.enums";
import { ModuleTypes } from "enums/module-types.enum";

// Constants
import { TemplateTypes } from "constants/template-types.constant";
import { GRAMapColor } from "modules/global-readiness/constants/gra.constants";

@Component({
    selector: "global-readiness-launch",
    templateUrl: "./global-readiness-launch.component.html",
})

export class GlobalReadinessLaunchComponent implements OnInit {

    onCountriesAndRegionsPage = true;
    isCountriesAndRegionsEnabled = true;
    allRegionsChecked = false;
    allFrameworkLawsChecked = false;
    regionDataList: IRegionDetail[];
    frameworkLawDataList: IFrameworkLawDetail[];
    isCountriesAndRegionsLoading: boolean;
    progressState$ = this.graHelperService.progressState$;
    progressSteps = this.graHelperService.createAssessmentSteps;
    labels = this.graHelperService.ProgressIndicatorLabels;
    graTemplateId: string;
    enableStartAssessmentButton: boolean;

    constructor(
        @Inject(StoreToken) private store: IStore,
        private stateService: StateService,
        private modalService: ModalService,
        private crossBorderService: GRACrossBorderService,
        private globalReadinessLaunchActionService: GlobalReadinessLaunchActionService,
        private graHelperService: GRAHelperService,
    ) { }

    ngOnInit() {
        this.isCountriesAndRegionsLoading = true;
        this.enableStartAssessmentButton = false;
        this.graTemplateId = this.stateService.params.graTemplateId;
        if (this.graTemplateId === null) {
            this.stateService.go("zen.app.pia.module.globalreadiness.views.welcome");
            return;
        }
        this.regionDataList = [];
        this.frameworkLawDataList = [];
        this.globalReadinessLaunchActionService.getRegionLawData().then((regionLawResponse: any) => {
            [ this.regionDataList , this.frameworkLawDataList ] = this.graHelperService.massageRegionLawResponse(regionLawResponse);
            this.isCountriesAndRegionsLoading = false;
        });
    }

    onStepChange(tabState: string): void {
        this.graHelperService.onStateChange(tabState);
    }

    onCountryFrameworkToggle(): void {
        this.isCountriesAndRegionsEnabled = !this.isCountriesAndRegionsEnabled;
        const state = this.isCountriesAndRegionsEnabled ? this.progressSteps.step1 : this.progressSteps.step2;
        this.onStepChange(state);
    }

    // COUNTRY CARDS CODE

    trackByRegionId(index: number, item: IRegionDetail): number {
        return item.regionId;
    }

    onAllRegionsToggle(event: boolean): void {
        const newFills: IStringMap<string> = {};
        this.allRegionsChecked = event;
        forEach(this.regionDataList, (region: IRegionDetail) => {
            region.isChecked = event;
            forEach(region.zones, (zone: IZoneDetail) => {
                zone.isChecked = event;
            });
            forEach(region.countries, (country: ICountryDetail) => {
                country.isChecked = event;
                newFills[country.code] = event ? GRAMapColor.highlightFillColor : GRAMapColor.defaultFillColor;
            });
        });
        this.crossBorderService.updateMapHighlights(newFills);
        this.syncFrameworkLawSelectionOnCountrySelection();
    }

    regionCardsSelectionChanged(event: IRegionCardEventEmitDetail): void {
        if (!event.isChecked) this.allRegionsChecked = event.isChecked;
        switch (event.areaType) {
            case GlobalReadinessAreaTypes.REGION:
                this.onRegionToggle(event);
                break;
            case GlobalReadinessAreaTypes.ZONE:
                this.onZoneToggle(event);
                break;
            case GlobalReadinessAreaTypes.COUNTRY:
                this.onCountryToggle(event);
                break;
        }
        this.syncFrameworkLawSelectionOnCountrySelection();
        this.syncRegionZoneCountrySelections();
    }

    onRegionToggle(event: IRegionCardEventEmitDetail): void {
        const newFills: IStringMap<string> = {};
        forEach(this.regionDataList, (region: IRegionDetail) => {
            if (region.regionId === event.regionId) {
                region.isChecked = event.isChecked;
                forEach(region.zones, (zone: IZoneDetail) => {
                    zone.isChecked = event.isChecked;
                });
                forEach(region.countries, (country: ICountryDetail) => {
                    country.isChecked = event.isChecked;
                    newFills[country.code] = event.isChecked ? GRAMapColor.highlightFillColor : GRAMapColor.defaultFillColor;
                });
                this.crossBorderService.updateMapHighlights(newFills);
                return;
            }
        });
    }

    onZoneToggle(event: IRegionCardEventEmitDetail): void {
        const currentRegion: IRegionDetail = this.regionDataList.find((region: IRegionDetail) => region.regionId === event.regionId);
        if (!event.isChecked) currentRegion.isChecked = event.isChecked;
        const currentZone: IZoneDetail = currentRegion.zones.find((zone: IZoneDetail) => zone.zoneId === event.zoneId);
        currentZone.isChecked = event.isChecked;
        const newFills: IStringMap<string> = {};
        forEach(currentZone.countryIds, (countryId: number) => {
            forEach(currentRegion.countries, (currentCountry: ICountryDetail) => {
                if (countryId === currentCountry.countryId) {
                    currentCountry.isChecked = event.isChecked;
                    newFills[currentCountry.code] = event.isChecked ? GRAMapColor.highlightFillColor : GRAMapColor.defaultFillColor;
                }
            });
        });
        this.crossBorderService.updateMapHighlights(newFills);
    }

    onCountryToggle(event: IRegionCardEventEmitDetail): void {
        const currentRegion: IRegionDetail = this.regionDataList.find((region: IRegionDetail) => region.regionId === event.regionId);
        if (!event.isChecked) currentRegion.isChecked = event.isChecked;
        const newFills: IStringMap<string> = {};
        const currentCountry: ICountryDetail = currentRegion.countries.find((country: ICountryDetail) => country.countryId === event.countryId);
        currentCountry.isChecked = event.isChecked;
        if (!event.isChecked) {
            forEach(currentCountry.zoneIds, (currentCountryZoneId) => {
                const regionZoneIndex = currentRegion.zones.findIndex((currentRegionZone) => currentRegionZone.zoneId === currentCountryZoneId);
                if (regionZoneIndex !== -1) currentRegion.zones[regionZoneIndex].isChecked = event.isChecked;
            });
        }
        newFills[currentCountry.code] = event.isChecked ? GRAMapColor.highlightFillColor : GRAMapColor.defaultFillColor;
        this.crossBorderService.updateMapHighlights(newFills);
    }

    syncFrameworkLawSelectionOnCountrySelection(): void {
        this.allFrameworkLawsChecked = false;
        let lawSelectionList: number[] = [];
        forEach(this.regionDataList, (currentRegion: IRegionDetail) => {
            forEach(currentRegion.countries, (currentCountry: ICountryDetail) => {
                if (currentCountry.isChecked) lawSelectionList = union(lawSelectionList, currentCountry.lawIds);
            });
        });
        forEach(this.frameworkLawDataList, (frameworkLawData: IFrameworkLawDetail) => {
            frameworkLawData.isChecked = lawSelectionList.includes(frameworkLawData.lawId);
        });
        this.checkFrameworkLawSelection();
    }

    syncRegionZoneCountrySelections(): void {
        // while iterating through countries and creating a map, check if at least one country is unselected.
        let isAtLeastOneRegionUnselected = false;
        this.regionDataList.forEach((region: IRegionDetail) => {
            let isAtLeastOneCountryUnselected = false;
            const countrySelectionMap: INumberMap<boolean> = {}; // hold country selections for current region

            region.countries.forEach((country: ICountryDetail) => {
                if (!country.isChecked) isAtLeastOneCountryUnselected = true;
                countrySelectionMap[country.countryId] = country.isChecked;
            });
            region.isChecked = !isAtLeastOneCountryUnselected;
            if (!region.isChecked) isAtLeastOneRegionUnselected = true;

            // for each of the zones, if al teast one zone is unselected, mark it as unselected.
            region.zones.forEach((zone: IZoneDetail) => {
                const isUnselectedCountryFound = zone.countryIds.some((countryId) => countrySelectionMap[countryId] === false);
                zone.isChecked = !isUnselectedCountryFound;
            });
        });
        // Finally, if at least one region is unselected, mark the select all unchecked.
        this.allRegionsChecked = !isAtLeastOneRegionUnselected;
    }

    // FRAMEWORK LAW CARDS
    trackByFrameworkLawId(index: number, item: IFrameworkLawDetail): number {
        return item.lawId;
    }

    onAllFrameworkLawsToggle(event: boolean): void {
        this.allFrameworkLawsChecked = event;
        this.enableStartAssessmentButton = event;
        forEach(this.frameworkLawDataList, (frameworkLaw: IFrameworkLawDetail) => {
            frameworkLaw.isChecked = event;
        });
        this.syncCountrySelectionOnLawSelection();
        this.syncRegionZoneCountrySelections();
    }

    frameworkLawCardsSelectionChanged(frameworkLawDetail: IFrameworkLawDetail): void {
        if (!frameworkLawDetail.isChecked) this.allFrameworkLawsChecked = frameworkLawDetail.isChecked;
        this.checkFrameworkLawSelection();
        this.syncCountrySelectionOnLawSelection();
        this.syncRegionZoneCountrySelections();
    }

    syncCountrySelectionOnLawSelection(): void {
        this.allRegionsChecked = false;
        let countrySelectionList: number[] = [];
        const newFills: IStringMap<string> = {};
        this.frameworkLawDataList.forEach((currentFrameworkLaw: IFrameworkLawDetail) => {
            if (currentFrameworkLaw.isChecked) countrySelectionList = union(countrySelectionList, currentFrameworkLaw.countryIds);
        });
        this.regionDataList.forEach((currentRegion: IRegionDetail) => {
            currentRegion.isChecked = false;
            currentRegion.zones.forEach((currentZone: IZoneDetail) => {
                currentZone.isChecked = false;
            });
            currentRegion.countries.forEach((currentCountry: ICountryDetail) => {
                currentCountry.isChecked = countrySelectionList.includes(currentCountry.countryId);
                newFills[currentCountry.code] = currentCountry.isChecked ? GRAMapColor.highlightFillColor : GRAMapColor.defaultFillColor;
            });
        });
        this.crossBorderService.updateMapHighlights(newFills);
    }

    checkFrameworkLawSelection(): void {
        this.enableStartAssessmentButton = this.frameworkLawDataList.some((frameworkLaw: IFrameworkLawDetail) => frameworkLaw.isChecked);
        this.allFrameworkLawsChecked = !this.frameworkLawDataList.some((frameworkLaw: IFrameworkLawDetail) => frameworkLaw.isChecked === false);
    }

    // FOOTER FUNCTIONS
    onContactButtonClick(): void { }

    onCancelButtonClick(): void {
        this.stateService.go("zen.app.pia.module.globalreadiness.views.welcome");
    }

    onPreviousButtonClick(): void {
        this.isCountriesAndRegionsEnabled = true;
        this.onCountriesAndRegionsPage = true;
        this.onStepChange(this.progressSteps.step1);
    }

    onNextButtonClick(): void {
        this.isCountriesAndRegionsEnabled = false;
        this.onCountriesAndRegionsPage = false;
        this.onStepChange(this.progressSteps.step2);
    }

    onStartAssessmentButtonClick(): void {
        const selectedLaws = [];
        forEach(this.frameworkLawDataList, (frameworkLawData: IFrameworkLawDetail) => {
            if (frameworkLawData.isChecked) selectedLaws.push(frameworkLawData.lawName);
        });
        const currentOrgId = getCurrentOrgId(this.store.getState());
        const modalData: IGRAStartAssessmentModalData = {
            modalTitle: "StartAssessment",
            startAssessmentRequest: {
                name: "",
                templateType: TemplateTypes.GRA,
                templateId: this.graTemplateId,
                laws: selectedLaws,
                orgGroupId: currentOrgId,
                orgGroupName: getOrgById(currentOrgId)(this.store.getState()).Name,
            },
            callback: (res) => {
                if (res.result) {
                    this.stateService.go("zen.app.pia.module.assessment.detail", { assessmentId: res.data, backToModule: ModuleTypes.GRA });
                }
            },
        };
        this.modalService.setModalData(modalData);
        this.modalService.openModal("downgradeStartAssessmentModal");
        this.onStepChange(this.progressSteps.step3);
    }
}
