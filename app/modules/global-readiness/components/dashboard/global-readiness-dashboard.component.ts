// Angular
import {
    OnInit,
    Component,
} from "@angular/core";
import { StateService } from "@uirouter/core";

// External
import { takeUntil } from "rxjs/operators";
import { Subject } from "rxjs";

// Services
import { GRAHelperService } from "modules/global-readiness/services/gra-helper.service";
import { GlobalReadinessDashboardApiService } from "modules/global-readiness/services/api/global-readiness-dashboard-api.service";
import { TranslateService } from "@ngx-translate/core";

// Interface
import { IGlobalReadinessDashboardGraph } from "modules/global-readiness/interfaces/global-readiness-dashboard.interface";

// Enums/Constants
import { ModuleTypes } from "enums/module-types.enum";
import { GRADashboardMenuActions } from "modules/global-readiness/enums/gra.enum";

@Component({
    selector: "global-readiness-dashboard",
    templateUrl: "./global-readiness-dashboard.component.html",
})

export class GlobalReadinessDashboardComponent implements OnInit {

    isLoaded = false;
    assessmentGraphs: IGlobalReadinessDashboardGraph[];
    translations = {};
    translationKeys = [
        "HideFromDashboard",
    ];

    readonly menuType = 1;
    readonly menuPosition = "bottom-right";
    readonly legendEntries = [
        {
            labelKey: "Ready",
            colorClass: "background-green",
        },
        {
            labelKey: "GapIdentified",
            colorClass: "background-orange",
        },
        {
            labelKey: "Unanswered",
            colorClass: "background-grey-1",
        },
    ];

    private destroy$ = new Subject();

    constructor(
        private readonly helper: GRAHelperService,
        private readonly apiService: GlobalReadinessDashboardApiService,
        private readonly translate: TranslateService,
        private stateService: StateService,
    ) {}

    ngOnInit(): void {
        this.translate.stream(this.translationKeys).subscribe((t) => {
            this.translations = t;
        });

        this.fetchDashboardDetails();
    }

    fetchDashboardDetails(): void {
        this.isLoaded = false;
        this.assessmentGraphs = [];
        this.apiService.getDashboardData()
            .pipe(takeUntil(this.destroy$))
            .subscribe((data) => {
                this.isLoaded = true;
                this.assessmentGraphs = this.helper.generateDashboardGraphData(data);
                this.generateMenuOptions();
            });
    }

    ngOnDestroy(): void {
        this.destroy$.next();
        this.destroy$.complete();
    }

    onContinue(assessmentId: string): void {
        this.stateService.go("zen.app.pia.module.assessment.detail", { assessmentId, backToModule: ModuleTypes.GRA });
    }

    handleMenuItemClicks(assessmentId: string, value: number): void {
        if (value === GRADashboardMenuActions.HIDE) {
            this.apiService.showHideAssessmentsFromDashboard(assessmentId, { displayOnReadinessDashboard: false })
                .pipe(takeUntil(this.destroy$))
                .subscribe((data) => {
                    if (data) this.fetchDashboardDetails();
                });
        }
    }

    generateMenuOptions(): void {
        this.assessmentGraphs.forEach((assessmentGraph): void => {
            assessmentGraph.dropDownMenu = [{ text: this.translations["HideFromDashboard"], value: GRADashboardMenuActions.HIDE }];
        });
    }
}
