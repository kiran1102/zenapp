// Services
import { GlobalSidebarService } from "modules/global-sidebar/services/one-global-sidebar.service";
import { StateService } from "@uirouter/core";
import { Wootric } from "sharedModules/services/provider/wootric.service";

// Interfaces
import { INavMenuItem } from "modules/global-sidebar/interfaces/nav-menu-item.interface";
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";

// Constants
import { WootricModuleMap } from "modules/shared/constants/wootric.constants";

class GlobalReadinessController implements ng.IComponentController {

    static $inject: string[] = [
        "$rootScope",
        "GlobalSidebarService",
        "Wootric",
    ];

    constructor(
        private $rootScope: IExtendedRootScopeService,
        private globalSidebarService: GlobalSidebarService,
        private wootric: Wootric,
    ) { }

    public $onInit(): void {
        this.globalSidebarService.set(this.getMenuRoutes(), this.$rootScope.t("MaturityBenchmarking"));
        this.wootric.run(WootricModuleMap.GlobalReadinessAssessment);
    }

    private getMenuRoutes(): INavMenuItem[] {
        const menuGroup: INavMenuItem[] = [];

        menuGroup.push({
            id: "GRAWelcomeSidebarItem",
            title: this.$rootScope.t("Welcome"),
            route: "zen.app.pia.module.globalreadiness.views.welcome",
            activeRouteFn: (stateService: StateService) =>
                stateService.includes("zen.app.pia.module.globalreadiness.views.welcome")
                || stateService.includes("zen.app.pia.module.globalreadiness.views.start"),
            icon: "fa fa-globe",
        });
        menuGroup.push({
            id: "GRADashboardSidebarItem",
            title: this.$rootScope.t("Dashboard"),
            route: "zen.app.pia.module.globalreadiness.views.dashboard",
            activeRoute: "zen.app.pia.module.globalreadiness.views.dashboard",
            icon: "fa fa-dashboard",
        });
        menuGroup.push({
            id: "GRAAssessmentsSidebarItem",
            title: this.$rootScope.t("Assessments"),
            route: "zen.app.pia.module.globalreadiness.views.assessments",
            activeRouteFn: (stateService: StateService) =>
                stateService.includes("zen.app.pia.module.globalreadiness.views.assessments")
                || stateService.includes("zen.app.pia.module.globalreadiness.views.assessmentreport"),
            icon: "fa fa-file-text-o",
        });

        return menuGroup;
    }
}

export const GlobalReadinessComponent: ng.IComponentOptions = {
    controller: GlobalReadinessController,
    template: `
        <div class="height-100 flex-full-width flex-full-height background-grey text-color-default">
            <div class="row-nowrap width-100-percent height-100 overflow-hidden">
                <section ui-view='global_readiness_body' class="width-100-percent height-100 stretch-horizontal"></section>
            </div>
        </div>
    `,
};
