// Angular
import {
    Component,
    OnInit,
    Inject,
} from "@angular/core";

// Interfaces
import { IGRAStartAssessmentModalData } from "modules/global-readiness/interfaces/global-readiness-launch.interface";
import { IStore } from "interfaces/redux.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";

// Reducers
import { getModalData } from "oneRedux/reducers/modal.reducer";
import { StoreToken } from "tokens/redux-store.token";

// Services
import { ModalService } from "sharedServices/modal.service";
import { GlobalReadinessLaunchActionService } from "modules/global-readiness/services/actions/global-readiness-launch-action.service";
import { GRAHelperService } from "modules/global-readiness/services/gra-helper.service";

@Component({
    selector: "gra-start-assessment-modal",
    templateUrl: "./gra-start-assessment-modal.component.html",
})

export class GRAStartAssessmentModalComponent implements OnInit {

    isStartingAssessment: boolean;
    modalData: IGRAStartAssessmentModalData;
    isStartButtonDisabled: boolean;

    constructor(
        @Inject(StoreToken) private store: IStore,
        private readonly modalService: ModalService,
        private readonly globalReadinessLaunchActionService: GlobalReadinessLaunchActionService,
        private readonly graHelperService: GRAHelperService,
    ) { }

    ngOnInit(): void {
        this.isStartButtonDisabled = true;
        this.isStartingAssessment = false;
        this.modalData = getModalData(this.store.getState()) as IGRAStartAssessmentModalData;
    }

    closeModal(): void {
        this.graHelperService.onStateChange(this.graHelperService.createAssessmentSteps.step2);
        this.modalService.closeModal();
        this.modalService.clearModalData();
    }
    saveModel(eventValue): void {
        this.modalData.startAssessmentRequest.name = eventValue.target.value;
        this.isStartButtonDisabled = this.modalData.startAssessmentRequest.name ? false : true;
    }

    onStart(): void {
        this.isStartingAssessment = true;
        this.globalReadinessLaunchActionService.startAssessment(this.modalData.startAssessmentRequest).then((res: IProtocolResponse<any>) => {
            if (res.result) {
                this.modalService.handleModalCallback({ result: true, data: res.data });
                this.closeModal();
            } else {
                this.isStartingAssessment = false;
            }
        });
    }
}
