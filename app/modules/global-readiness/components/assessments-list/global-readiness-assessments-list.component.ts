// Core
import {
    OnInit,
    Component,
    Inject,
    OnDestroy,
} from "@angular/core";

// 3rd Party
import { remove } from "lodash";

// Rxjs
import {
    Subscription,
    Subject,
} from "rxjs";

// Redux
import { StoreToken } from "tokens/redux-store.token";

// Services
import { GRAAssessmentApiService } from "modules/global-readiness/services/api/gra-assessment-api.service";

// Interfaces
import {
    IStoreState,
    IStore,
} from "interfaces/redux.interface";
import {
    IGlobalReadinessAssessmentRecord,
    IAssessmentTableRow,
} from "modules/global-readiness/interfaces/global-readiness-assessments.interface";
import { IPageableMeta } from "interfaces/pagination.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";
import {
    IGridPaginationParams,
    IGridColumn,
    IGridSort,
} from "interfaces/grid.interface";

// Enums / Constants
import { TableColumnTypes } from "enums/data-table.enum";
import { GlobalReadinessAssessmentsTableColumns } from "modules/global-readiness/enums/global-readiness-assessments.enums";

interface IGlobalReadinessAssessmentsTable {
    rows: IGlobalReadinessAssessmentRecord[];
    columns?: IGridColumn[];
    metaData?: IPageableMeta;
}

@Component({
    selector: "global-readiness-assessments",
    templateUrl: "./global-readiness-assessments-list.component.html",
})

export class GlobalReadinessAssessmentsListComponent implements OnInit, OnDestroy {

    isLoading = false;
    readinessAssessmentSearchText = "";
    sortData: IGridSort;
    defaultPagination: IGridPaginationParams = { page: 0, size: 20, sort: `createDT,desc` };
    filter = "";
    paginationParams = this.defaultPagination;
    resetSearch$ = new Subject();
    table: IGlobalReadinessAssessmentsTable;

    private subscription: Subscription;

    constructor(
        @Inject(StoreToken) private store: IStore,
        private readonly GRAAssessmentAPI: GRAAssessmentApiService,
    ) { }

    ngOnInit(): void {
        this.table = {
            rows: [],
            columns: [
                {
                    name: "AssessmentName",
                    sortKey: "name",
                    valueKey: GlobalReadinessAssessmentsTableColumns.NAME,
                    type: TableColumnTypes.Link,
                },
                {
                    name: "DateCreated",
                    sortKey: "createDT",
                    valueKey: GlobalReadinessAssessmentsTableColumns.CREATED_DATE,
                    type: TableColumnTypes.Date,
                },
                {
                    name: "DateUpdated",
                    sortKey: "updateDT",
                    valueKey: GlobalReadinessAssessmentsTableColumns.UPDATE_DATE,
                    type: TableColumnTypes.Date,
                },
                {
                    name: "Template",
                    sortKey: "templateName",
                    valueKey: GlobalReadinessAssessmentsTableColumns.TEMPLATE,
                    type: TableColumnTypes.Text,
                },
                {
                    name: "",
                    type: TableColumnTypes.Action,
                    valueKey: "",
                },
            ],
            metaData: {
                first: true,
                last: false,
                number: 0,
                numberOfElements: 0,
                size: 20,
                sort: null,
                totalElements: 0,
                totalPages: 0,
            },
        };
        this.getAssessments();
    }

    ngOnDestroy(): void {
        // TODO
    }

    componentWillReceiveState(state: IStoreState): void {
        // TODO
    }

    configureTable(state): IGlobalReadinessAssessmentsTable {
        // TODO
        return null;
    }

    getAssessments(): void {
        this.isLoading = true;
        this.GRAAssessmentAPI.getReadinessAssessments(this.readinessAssessmentSearchText, this.filter, this.paginationParams).then((response: IProtocolResponse<IAssessmentTableRow>): void => {
            if (response.result) {
                this.table.rows = response.data.content;
                this.table.metaData = {
                    first: response.data.first,
                    last: response.data.last,
                    number: response.data.number,
                    numberOfElements: response.data.numberOfElements,
                    size: response.data.size,
                    sort: response.data.sort,
                    totalElements: response.data.totalElements,
                    totalPages: response.data.totalPages,
                };
            }
            this.isLoading = false;
        });
    }

    onSortChange(sortData: IGridSort): void {
        this.sortData = sortData;
        this.paginationParams.sort = `${sortData.sortedKey},${sortData.sortOrder}`;
        this.getAssessments();
    }

    onSearchChange(searchText: string = ""): void {
        this.readinessAssessmentSearchText = searchText;
        this.paginationParams = this.defaultPagination;
        this.isLoading = true;
        this.getAssessments();
    }

    onPaginationChange(event: number = 0): void {
        this.paginationParams = { ...this.paginationParams, page: event - 1 };
        this.isLoading = true;
        this.getAssessments();
    }

    onDeleteAssessment(assessmentId: string): void {
        remove(this.table.rows, (row: IGlobalReadinessAssessmentRecord) => row.assessmentId === assessmentId );
    }

    // TODO: implement when more info is available
    startNewReadinessAssessmentClicked(): void {}
}
