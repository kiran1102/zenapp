// Angular
import {
    Component,
    Input,
    Output,
    EventEmitter,
} from "@angular/core";

// Interface
import {
    IGRATemplateCardsResponse,
    IGRATemplatePreviewSection,
} from "modules/global-readiness/interfaces/gra-welcome.interface";

@Component({
    selector: "global-readiness-welcome-preview",
    templateUrl: "./global-readiness-welcome-preview.component.html",
})
export class GlobalReadinessWelcomePreviewComponent {

    @Input() currentTemplate: IGRATemplateCardsResponse;
    @Input() isLoading = false;

    @Output() toggleDrawer = new EventEmitter();
    @Output() showQuestionPreview = new EventEmitter();
    @Output() startAssessment = new EventEmitter<string>();
    @Output() goToNextTemplate = new EventEmitter<string>();
    @Output() goToPreviousTemplate = new EventEmitter<string>();

    isWelcomeMessageExpanded = true;
    templateChosen = false;

    expandSection(section: IGRATemplatePreviewSection) {
        section.isSectionExpanded = !section.isSectionExpanded;
    }

    toggleTemplateChosen() {
        this.templateChosen = !this.templateChosen;
    }
}
