// Angular
import {
    Component,
    Input,
} from "@angular/core";

// Constants
import { QuestionTypes } from "constants/question-types.constant";

// Enums
import { QuestionAnswerTypes } from "enums/question-types.enum";

// Interfaces
import { IGalleryTemplateQuestion } from "interfaces/gallery-templates.interface";
import { IStringMap } from "interfaces/generic.interface";
import { IQuestionTypes } from "modules/template/interfaces/template.interface";

@Component({
    selector: "global-readiness-welcome-question-preview",
    templateUrl: "./global-readiness-welcome-question-preview.component.html",
})

export class GlobalReadinessWelcomeQuestionPreviewComponent {

    @Input() question: IGalleryTemplateQuestion;

    questionTypes: IStringMap<IQuestionTypes> = QuestionTypes;
    questionAnswerTypes = QuestionAnswerTypes;
}
