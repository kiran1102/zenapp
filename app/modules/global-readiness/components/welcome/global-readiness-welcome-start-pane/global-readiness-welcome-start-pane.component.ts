// Angular
import {
    Component,
    Input,
    Output,
    EventEmitter,
} from "@angular/core";

// Interface
import { IGRATemplateCardsResponse } from "modules/global-readiness/interfaces/gra-welcome.interface";

@Component({
    selector: "global-readiness-welcome-start-pane",
    templateUrl: "./global-readiness-welcome-start-pane.component.html",
})

export class GlobalReadinessWelcomeStartPaneComponent {

    @Input() currentTemplate: IGRATemplateCardsResponse;

    @Output() startAssessment = new EventEmitter<string>();
    @Output() toggleDrawer = new EventEmitter();
    @Output() toggleTemplateChosen = new EventEmitter();

    assessmentName: string;

    setAssessmentName(event: string) {
        this.assessmentName = event;
    }
}
