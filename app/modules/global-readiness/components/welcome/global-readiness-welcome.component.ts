// Angular
import {
    Component,
    Inject,
} from "@angular/core";
import { StateService } from "@uirouter/core";

// Service
import { GRAWelcomeApiService } from "modules/global-readiness/services/api/gra-welcome-api.service";
import TemplateAction from "templateServices/actions/template-action.service";
import { GlobalReadinessLaunchActionService } from "modules/global-readiness/services/actions/global-readiness-launch-action.service";
import { Principal } from "modules/shared/services/helper/principal.service";

// Interface
import {
    ISeededGRATemplateDetail,
    IFilteredGRATemplateDetail,
    IGRATemplateCardsResponse,
    IGRATemplatePreviewQuestion,
    IGRATemplatePreviewQuestionOption,
} from "modules/global-readiness/interfaces/gra-welcome.interface";

// Constants
import { TemplateTypes } from "constants/template-types.constant";
import { ModuleTypes } from "enums/module-types.enum";

// Enums
import { TemplateStates } from "enums/template-states.enum";

// Redux
import { getCurrentUser } from "oneRedux/reducers/user.reducer";
import { IStore } from "interfaces/redux.interface";
import { StoreToken } from "tokens/redux-store.token";

@Component({
    selector: "global-readiness-welcome",
    templateUrl: "./global-readiness-welcome.component.html",
})

export class GlobalReadinessWelcomeComponent {

    globalReadinessTemplate: IFilteredGRATemplateDetail = null;
    seededTemplates: IFilteredGRATemplateDetail[] = [];
    isLoadingReadinessAssessment = false;
    filteredTemplates: IFilteredGRATemplateDetail[] = [];
    searchTerm: string;
    currentTemplate: IGRATemplateCardsResponse;
    templateId: string;
    isPreviewOpen = false;
    isTemplateLoading = false;

    constructor(
        @Inject("TemplateAction") private templateAction: TemplateAction,
        @Inject(StoreToken) private store: IStore,
        private stateService: StateService,
        private GRAWelcomeApi: GRAWelcomeApiService,
        private principal: Principal,
        private readonly globalReadinessLaunchAction: GlobalReadinessLaunchActionService,
    ) {}

    ngOnInit() {
        this.getReadinessAssessments();
    }

    showQuestionPreview(question: IGRATemplatePreviewQuestion) {
        question.isPreviewShown = !question.isPreviewShown;
        question.attributeJson = this.templateAction.parseAttributes(question.attributes);
        if (question.options && question.options.length) {
            question.options.forEach((option: IGRATemplatePreviewQuestionOption) => {
                if (question.attributeJson.optionHintEnabled) {
                    option.attributeJson = this.templateAction.parseAttributes(option.attributes);
                }
            });
        }
    }

    openPreviewView(templateId: string, nextOrPrevious = null) {
        if (templateId) {
            this.templateId = templateId;
            this.isTemplateLoading = true;
            this.templateAction.getTemplateMetadata(this.templateId).then((response: IGRATemplateCardsResponse) => {
                if (response) {
                    this.currentTemplate = {
                        description: response.description,
                        friendlyName: response.friendlyName,
                        icon: response.icon,
                        id: response.id,
                        name: response.name,
                        nextTemplateId: this.calculateNextTemplate(templateId),
                        orgGroupId: response.orgGroupId,
                        previousTemplateId: this.calculatePreviousTemplate(templateId),
                        sections: response.sections,
                        status: TemplateStates.Draft,
                        templateType: response.templateType,
                        templateVersion: response.templateVersion,
                        welcomeText: response.welcomeText,
                    };

                    if (!nextOrPrevious) {
                        this.isPreviewOpen = !this.isPreviewOpen;
                    }
                    this.isTemplateLoading = false;

                }
            });
        }
    }

    calculatePreviousTemplate(templateId: string): string | null {
        const currentTemplateIndex = this.filteredTemplates.findIndex((template: IFilteredGRATemplateDetail): boolean => {
            return templateId === template.id;
        });

        if (!currentTemplateIndex) {
            return null;
        } else {
            return this.filteredTemplates[currentTemplateIndex - 1].id;
        }
    }

    calculateNextTemplate(templateId: string): string | null {
        const currentTemplateIndex = this.filteredTemplates.findIndex((template: IFilteredGRATemplateDetail): boolean => {
            return templateId === template.id;
        });

        if (currentTemplateIndex === this.filteredTemplates.length - 1) {
            return null;
        } else {
            return this.filteredTemplates[currentTemplateIndex + 1].id;
        }

    }

    startAssessment(name: string) {
        const state = this.store.getState();
        const params = {
            name,
            templateType: TemplateTypes.RA,
            templateId: this.currentTemplate.id,
            laws: [],
            orgGroupId: this.principal.getOrggroup(),
            orgGroupName: getCurrentUser(state).OrgGroupName,
        };

        this.globalReadinessLaunchAction.startAssessment(params).then((response) => {
            this.stateService.go("zen.app.pia.module.assessment.detail", { assessmentId: response.data, backToModule: ModuleTypes.GRA });
        });
    }

    onCustomizeGlobalReadinessClick() {
        this.stateService.go("zen.app.pia.module.globalreadiness.views.start", { graTemplateId: this.globalReadinessTemplate.id });
    }

    searchTemplate(event: string) {
        this.searchTerm = event;
        this.filteredTemplates = this.seededTemplates.filter((template: IFilteredGRATemplateDetail) => {
            if (template.title.text.toLowerCase().includes(this.searchTerm.toLowerCase())) {
                return template;
            }
        });
    }

    closeOnClick() {
        this.isPreviewOpen = !this.isPreviewOpen;
    }

    clearSearchField() {
        this.searchTerm = "";
        this.filteredTemplates = this.seededTemplates;
    }

    getReadinessAssessments() {
        this.isLoadingReadinessAssessment = true;
        this.globalReadinessTemplate = null;
        this.GRAWelcomeApi.getSeededTemplateDetails().subscribe((templates: ISeededGRATemplateDetail[]) => {
            if (templates.length) {
                templates.forEach((template: ISeededGRATemplateDetail) => {
                    this.seededTemplates.push({
                        title: {
                            text: template.name,
                        },
                        id: template.id,
                        content: template.description,
                        templateType: template.templateType,
                    });
                });
                this.filteredTemplates = this.seededTemplates;
                const indexOfGRATemplate = this.seededTemplates.findIndex((assessment) => assessment.templateType === TemplateTypes.GRA);
                if (indexOfGRATemplate !== -1) {
                    this.globalReadinessTemplate = this.seededTemplates[indexOfGRATemplate];
                    this.seededTemplates.splice(indexOfGRATemplate, 1);
                }
            }
            this.isLoadingReadinessAssessment = false;
        });
    }
}
