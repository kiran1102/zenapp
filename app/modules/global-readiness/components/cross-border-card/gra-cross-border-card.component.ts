import {
    Component,
    OnInit,
} from "@angular/core";

// Services
import { GRACrossBorderService } from "modules/global-readiness/services/gra-cross-border-map.service";

// Interfaces
import { IStringMap } from "interfaces/generic.interface";

@Component({
    selector: "gra-cross-border-card",
    template: `
        <section class="height-100 position-relative background-map-water-blue">
            <ot-loading
                *ngIf="!ready"
                class="absolute-center"
            ></ot-loading>
            <gra-cross-border-map
                *ngIf="ready"
                [mapId]="'gra-cross-border-map'"
                [mapData]="mapData"
            ></gra-cross-border-map>
        </section>`,
})
export class GRACrossBorderCard implements OnInit {

    ready = true;
    mapData: IStringMap<string> = null;

    constructor(
        readonly CrossBorderService: GRACrossBorderService,
    ) { }

    ngOnInit() {}
}
