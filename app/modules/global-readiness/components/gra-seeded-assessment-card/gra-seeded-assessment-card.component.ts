// Angular
import {
    Component,
    Input,
    Inject,
} from "@angular/core";
import { StateService } from "@uirouter/core";

// Interface
import { ISeededGRATemplateDetail } from "modules/global-readiness/interfaces/gra-welcome.interface";
import { IGRAStartAssessmentModalData } from "modules/global-readiness/interfaces/global-readiness-launch.interface";

// Service
import { GlobalReadinessLaunchActionService } from "modules/global-readiness/services";
import { ModalService } from "sharedServices/modal.service";

// Enums / Constant
import { TemplateTypes } from "constants/template-types.constant";
import { ModuleTypes } from "enums/module-types.enum";

// Redux
import { IStore } from "interfaces/redux.interface";
import { StoreToken } from "tokens/redux-store.token";
import {
    getCurrentOrgId,
    getOrgById,
} from "oneRedux/reducers/org.reducer";

@Component({
    selector: "gra-seeded-assessment-card",
    templateUrl: "./gra-seeded-assessment-card.component.html",
})

export class GRASeededAssessmentCard {

    @Input() seededTemplateDetails: ISeededGRATemplateDetail;

    constructor(
        @Inject(StoreToken) private store: IStore,
        private stateService: StateService,
        private readonly modalService: ModalService,
        private readonly globalReadinessLaunchActionService: GlobalReadinessLaunchActionService,
    ) { }

    onStartClick(): void {
        const state = this.store.getState();
        const orgGroupId = getCurrentOrgId(state);

        const modalData: IGRAStartAssessmentModalData = {
            modalTitle: "StartAssessment",
            startAssessmentRequest: {
                name: "",
                templateType: TemplateTypes.RA,
                templateId: this.seededTemplateDetails.id,
                laws: [],
                orgGroupId,
                orgGroupName: getOrgById(orgGroupId)(state).Name,
            },
            callback: (res) => {
                if (res.result) {
                    this.stateService.go("zen.app.pia.module.assessment.detail", { assessmentId: res.data, backToModule: ModuleTypes.GRA });
                }
            },
        };
        this.modalService.setModalData(modalData);
        this.modalService.openModal("downgradeStartAssessmentModal");
    }
}
