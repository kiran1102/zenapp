// Core
import {
    Component,
    EventEmitter,
    Input,
    Inject,
    Output,
} from "@angular/core";
import { StateService } from "@uirouter/core";

// External
import { Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";

// Services
import { Permissions } from "modules/shared/services/helper/permissions.service";
import { AATemplateApiService } from "modules/assessment/services/api/aa-template-api.service";
import { ModalService } from "sharedServices/modal.service";
import { GlobalReadinessDashboardApiService } from "modules/global-readiness/services/api/global-readiness-dashboard-api.service";
import { TranslateService } from "@ngx-translate/core";

// Interfaces
import { IGlobalReadinessAssessmentRecord } from "modules/global-readiness/interfaces/global-readiness-assessments.interface";
import { IDropdownOption } from "interfaces/one-dropdown.interface";
import {
    IGridColumn,
    IGridSort,
} from "interfaces/grid.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";
import { IDeleteConfirmationModalResolve } from "interfaces/delete-confirmation-modal-resolve.interface";

// Enums / Constants
import { TableColumnTypes } from "enums/data-table.enum";
import { findIndex } from "lodash";
import { ModuleTypes } from "enums/module-types.enum";
import { GRADashboardMenuActions } from "modules/global-readiness/enums/gra.enum";
import { AssessmentPermissions } from "constants/assessment.constants";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

@Component({
    selector: "global-readiness-assessments-table",
    templateUrl: "./global-readiness-assessments-table.component.html",
})
export class GlobalReadinessAssessmentsTableComponent {

    @Input() rows: IGlobalReadinessAssessmentRecord[] = [];
    @Input() columns: IGridColumn[];
    @Input() allRowsSelected = true;
    @Input() sortData: IGridSort;

    @Output() emptyTableAction = new EventEmitter();
    @Output() handleTableSort: EventEmitter<IGridSort> = new EventEmitter<IGridSort>();
    @Output() onDeleteAssessment = new EventEmitter();
    @Output() handleTableReload = new EventEmitter();

    colBordered = false;
    disabledRowMap = {};
    editColIndex: number;
    editRowIndex: number;
    resizableCol = false;
    responsiveTable = true;
    rowBordered = true;
    rowNoHover = false;
    rowStriped = false;
    selectedRowMap = {};
    sortable = true;
    tableColumnTypes = TableColumnTypes;
    translations = {};
    translationKeys = [
        "HideFromDashboard",
        "ShowOnDashboard",
        "Delete",
        "DeleteAssessment",
        "CanWeDeleteAssessmentRecord",
        "Cancel",
        "Confirm",
    ];
    truncateCellContent = true;
    wrapCellContent = false;

    readonly menuType = 1;

    private destroy$ = new Subject();

    constructor(
        private stateService: StateService,
        public readonly AssessmentTemplateAPI: AATemplateApiService,
        public readonly permissions: Permissions,
        public readonly modalService: ModalService,
        public translate: TranslateService,
        private apiService: GlobalReadinessDashboardApiService,
    ) { }

    ngOnInit(): void {
        this.translate.stream(this.translationKeys).subscribe((t) => {
            this.translations = t;
        });

        this.generateMenuOptions();
    }

    ngOnDestroy(): void {
    }

    checkIfRowIsDisabled = (row: IGlobalReadinessAssessmentRecord): boolean => !!this.disabledRowMap[row.assessmentId];
    checkIfRowIsSelected = (row: IGlobalReadinessAssessmentRecord): boolean => !!this.selectedRowMap[row.assessmentId];
    columnTrackBy = (i: number): number => i;

    handleSortChange(sortData: IGridSort): void {
        this.handleTableSort.emit(sortData);
    }

    checkIfAllSelected(): boolean {
        const keys = Object.keys(this.selectedRowMap);

        if (keys.length === this.rows.length) {
            for (let i = 0; i < keys.length; i++) {
                if (!this.selectedRowMap[keys[i]]) {
                    return false;
                }
            }
        } else {
            return false;
        }
        return true;
    }

    handleTableSelectClick(actionType: string, rowId: string): void {
        switch (actionType) {
            case "header":
                this.allRowsSelected = !this.allRowsSelected;

                if (this.allRowsSelected) {
                    this.rows.forEach((elem: IGlobalReadinessAssessmentRecord) => {
                        this.selectedRowMap[elem.assessmentId] = elem.assessmentId;
                    });
                } else {
                    this.selectedRowMap = {};
                }
                break;
            case "row":
                this.selectedRowMap[rowId] = !this.selectedRowMap[rowId];
                this.allRowsSelected = this.checkIfAllSelected();
        }
    }

    goToRoute(row: IGlobalReadinessAssessmentRecord): void {
        this.stateService.go("zen.app.pia.module.assessment.detail", { assessmentId: row.assessmentId, backToModule: ModuleTypes.GRA });
    }

    generateMenuOptions(): void {
        this.rows.forEach((assessment: IGlobalReadinessAssessmentRecord) => {
            const menuOptions: IDropdownOption[] = [];
            if (assessment.displayOnReadinessDashboard === null || assessment.displayOnReadinessDashboard) {
                menuOptions.push({
                    text: this.translations["HideFromDashboard"],
                    value: GRADashboardMenuActions.HIDE,
                });
            } else {
                menuOptions.push({
                    text: this.translations["ShowOnDashboard"],
                    value: GRADashboardMenuActions.SHOW,
                });
            }
            if (this.permissions.canShow(AssessmentPermissions.AssessmentDelete)) {
                menuOptions.push({
                    text: this.translations["Delete"],
                    value: GRADashboardMenuActions.DELETE,
                });
            }
            assessment.dropDownMenu = menuOptions;
        });
    }

    // TODO: implement below empty methods when more info is available.
    showTile(row: IGlobalReadinessAssessmentRecord): void {}

    deleteAssessment(row: IGlobalReadinessAssessmentRecord): void {
        const assessmentId = row.assessmentId;
        const modalData: IDeleteConfirmationModalResolve = {
            modalTitle: this.translations["DeleteAssessment"],
            confirmationText: this.translations["CanWeDeleteAssessmentRecord"],
            cancelButtonText: this.translations["Cancel"],
            cancelIdentifier: "GRAAssessmentDeleteModalCancelButton",
            submitButtonText: this.translations["Confirm"],
            deleteIdentifier: "GRAAssessmentDeleteModalDeleteButton",
            promiseToResolve:
                (): ng.IPromise<boolean> => this.AssessmentTemplateAPI.deleteAssessment(assessmentId)
                    .then((response: IProtocolResponse<boolean>): boolean => {
                        if (response.result) {
                            this.onDeleteAssessment.emit(assessmentId);
                        }
                        return response.result;
                    }),
        };
        this.modalService.setModalData(modalData);
        this.modalService.openModal("deleteConfirmationModal");
    }

    handleMenuItemClicks(row: IGlobalReadinessAssessmentRecord, value: number): void {
        switch (value) {
            case GRADashboardMenuActions.HIDE:
                this.apiService.showHideAssessmentsFromDashboard(row.assessmentId, { displayOnReadinessDashboard: false })
                    .pipe(takeUntil(this.destroy$))
                    .subscribe((data) => {
                        if (data) this.handleTableReload.emit();
                    });
                break;
            case GRADashboardMenuActions.SHOW:
                this.apiService.showHideAssessmentsFromDashboard(row.assessmentId, { displayOnReadinessDashboard: true })
                    .pipe(takeUntil(this.destroy$))
                    .subscribe((data) => {
                        if (data) this.handleTableReload.emit();
                    });
                break;
            case GRADashboardMenuActions.DELETE:
                this.deleteAssessment(row);
                break;
        }
    }
    // TODO: implement when more info is available
    // onStartNewReadinessAssessmentClicked(): void {
    //     this.emptyTableAction.emit();
    // }

    private setMenuPosition(row: IGlobalReadinessAssessmentRecord): string {
        if (this.rows.length <= 10) return "bottom-right";
        const halfwayPoint: number = (this.rows.length - 1) / 2;
        const rowIndex: number = findIndex(this.rows, (item: IGlobalReadinessAssessmentRecord): boolean => row.assessmentId === item.assessmentId);
        if (rowIndex > halfwayPoint) {
            return "top-right";
        } else {
            return "bottom-right";
        }
    }
}
