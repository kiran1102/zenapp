// Angular
import {
    Component,
    Input,
    Output,
    EventEmitter,
} from "@angular/core";

// Interface
import {
    IRegionDetail,
    IZoneDetail,
    ICountryDetail,
    IRegionCardEventEmitDetail,
} from "modules/global-readiness/interfaces/global-readiness-launch.interface";

// Enums
import { GlobalReadinessAreaTypes } from "modules/global-readiness/enums/global-readiness-launch.enums";

@Component({
    selector: "region-card",
    templateUrl: "./region-card.component.html",
})

export class RegionCardComponent {

    @Input() regionData: IRegionDetail;

    @Output() onChange = new EventEmitter<IRegionCardEventEmitDetail>();

    trackByZoneId(index: number, zone: IZoneDetail): number {
        return zone.zoneId;
    }

    trackByCountryId(index: number, country: ICountryDetail): number {
        return country.countryId;
    }

    onToggleRegion(event: boolean, region: number): void {
        this.onChange.emit({
            areaType: GlobalReadinessAreaTypes.REGION,
            isChecked: event,
            regionId: region,
        });
    }

    onToggleZone(event: boolean, zone: number, region: number): void {
        this.onChange.emit({
            areaType: GlobalReadinessAreaTypes.ZONE,
            isChecked: event,
            zoneId: zone,
            regionId: region,
        });
    }

    onToggleCountry(event: boolean, country: number, region: number): void {
        this.onChange.emit({
            areaType: GlobalReadinessAreaTypes.COUNTRY,
            isChecked: event,
            countryId: country,
            regionId: region,
        });
    }
}
