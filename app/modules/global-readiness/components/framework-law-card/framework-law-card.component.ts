// Angular
import {
    Component,
    Input,
    Output,
    EventEmitter,
} from "@angular/core";

// Interface
import { IFrameworkLawDetail } from "modules/global-readiness/interfaces/global-readiness-launch.interface";

@Component({
    selector: "framework-law-card",
    templateUrl: "./framework-law-card.component.html",
})

export class FrameworkLawCardComponent {

    @Input() frameworkLawData: IFrameworkLawDetail;

    @Output() onChange = new EventEmitter<IFrameworkLawDetail>();

    onToggleFrameworkLaw(event: boolean, frameworkLawDetail: IFrameworkLawDetail): void {
        frameworkLawDetail.isChecked = event;
        this.onChange.emit(frameworkLawDetail);
    }
}
