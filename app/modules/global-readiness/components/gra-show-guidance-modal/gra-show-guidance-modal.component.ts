// Angular
import {
    Component,
    OnInit,
    Inject,
} from "@angular/core";

// Services
import { ModalService } from "sharedServices/modal.service";
import { GRAAssessmentApiService } from "modules/global-readiness/services";

// Reducers
import { getModalData } from "oneRedux/reducers/modal.reducer";
import { StoreToken } from "tokens/redux-store.token";
import { IStore } from "interfaces/redux.interface";
import { getAssessmentDetailModel } from "modules/assessment/reducers/assessment-detail.reducer";

// Interface
import {
    IGRAShowGuidanceModalData,
    IGRAAssessmentReadinessLawGuidance,
    IAssessmentLawGuidanceDetail,
} from "modules/global-readiness/interfaces/global-readiness-assessments.interface";
import { ILabelValue } from "interfaces/generic.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";
import { forEach } from "lodash";

@Component({
    selector: "gra-show-guidance-modal",
    templateUrl: "./gra-show-guidance-modal.component.html",
})

export class GRAShowGuidanceModalComponent implements OnInit {

    public lawGuidanceData: IGRAAssessmentReadinessLawGuidance;
    public lawGuidanceList: any;
    public lawList: Array<ILabelValue<string>> = [];
    public modalData: IGRAShowGuidanceModalData;
    public questionId: string;
    public selectedLaw: ILabelValue<string>;
    public lawDetails: string;
    public isLoadingGuidance: boolean;

    constructor(
        @Inject(StoreToken) private store: IStore,
        private readonly modalService: ModalService,
        private readonly GRAAssessmentAPI: GRAAssessmentApiService,
    ) { }

    ngOnInit(): void {
        this.isLoadingGuidance = true;
        this.modalData = getModalData(this.store.getState()) as IGRAShowGuidanceModalData;
        this.questionId = this.modalData.questionId;
        const assessmentLaws = getAssessmentDetailModel(this.store.getState()).laws;
        this.GRAAssessmentAPI.getReadinessAssessmentLawGuidance(this.questionId, assessmentLaws).then((response: IProtocolResponse<IGRAAssessmentReadinessLawGuidance>): void => {
            if (response.result) {
                this.lawGuidanceData = response.data;
                forEach(this.lawGuidanceData.guidance, (guidance: IAssessmentLawGuidanceDetail): void => {
                    this.lawList.push({
                        label: guidance.law,
                        value: guidance.law,
                    });
                });
                this.selectedLaw = this.lawList[0];
                this.selectOption(this.selectedLaw);
            }
            this.isLoadingGuidance = false;
        });
    }

    closeModal(): void {
        this.modalService.closeModal();
        this.modalService.clearModalData();
    }

    selectOption(selection: ILabelValue<string>): void {
        this.selectedLaw = selection;
        this.lawDetails = this.lawGuidanceData.guidance.find((x) => x.law === selection.value).detail;
    }
}
