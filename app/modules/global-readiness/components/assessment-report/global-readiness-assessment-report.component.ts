// Angular
import {
    Component,
    OnInit,
    Inject,
    OnDestroy,
} from "@angular/core";
import { StateService } from "@uirouter/core";

// External
import { Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";

// Services
import { GRAReportApiService } from "modules/global-readiness/services/api/gra-report-api.service";
import { GRAHelperService } from "modules/global-readiness/services/gra-helper.service";

// Interfaces
import {
    IGRAOverallReadinessSummary,
    IGRASectionSummaryResponse,
    IGRASectionSummary,
    IGRAApplicableLawReadiness,
} from "modules/global-readiness/interfaces/gra-report.interface";
import { IHorseshoeGraphData } from "interfaces/one-horseshoe-graph.interface";

// Constants/Enums
import { TemplateTypes } from "constants/template-types.constant";

@Component({
    selector: "global-readiness-assessment-report",
    templateUrl: "./global-readiness-assessment-report.component.html",
})
export class GlobalReadinessAssessmentReport implements OnInit, OnDestroy {

    assessmentId: string;
    overallReadinessSummary: IGRAOverallReadinessSummary;
    sectionSummary: IGRASectionSummaryResponse;
    overallReadinessGraphData: IHorseshoeGraphData;
    overallReadinessForRATemplate: IGRAApplicableLawReadiness;

    isLoadingOverallReadinessSummary: boolean;
    isLoadingSectionSummary: boolean;
    isGRATemplate: boolean;

    private destroy$ = new Subject();

    constructor(
        private stateService: StateService,
        private reportApi: GRAReportApiService,
        private helper: GRAHelperService,
    ) { }

    ngOnInit(): void {
        this.isLoadingOverallReadinessSummary = true;
        this.isLoadingSectionSummary = true;
        this.assessmentId = this.stateService.params.assessmentId;

        this.reportApi.getAssessmentOverallReadiness(this.assessmentId)
            .pipe(takeUntil(this.destroy$))
            .subscribe((readinessReport: IGRAOverallReadinessSummary): void => {
                this.overallReadinessSummary = readinessReport;
                if (this.overallReadinessSummary.templateType === TemplateTypes.GRA) {
                    this.isGRATemplate = true;
                    this.overallReadinessGraphData = this.helper.generateAssessmentSummaryGraphData(this.overallReadinessSummary.statisticsSummary);
                } else {
                    this.isGRATemplate = false;
                    this.overallReadinessForRATemplate = {
                        lawName: this.overallReadinessSummary.assessmentName,
                        lawStatisticsSummary: this.overallReadinessSummary.statisticsSummary,
                    };
                }
                this.isLoadingOverallReadinessSummary = false;
            });

        this.reportApi.getAssessmentReportSummary(this.assessmentId)
            .pipe(takeUntil(this.destroy$))
            .subscribe((summary: IGRASectionSummaryResponse): void => {
                this.sectionSummary = summary;
                this.isLoadingSectionSummary = false;
            });
    }

    ngOnDestroy() {
        this.destroy$.next();
        this.destroy$.complete();
    }

    trackByLawId(index: number, law: IGRAApplicableLawReadiness): number {
        return index;
    }

    trackBySectionId(index: number, section: IGRASectionSummary): number {
        return index;
    }

    goToRoute(event): void {
        if (event.path === "/assessments") this.stateService.go("zen.app.pia.module.globalreadiness.views.assessments");
    }

    // TODO: Once BE Services are up.
    exportFullReport(): void { }

    onGoToAssessmentButtonClick(): void {
        this.stateService.go("zen.app.pia.module.assessment.detail", { assessmentId: this.assessmentId });
    }
}
