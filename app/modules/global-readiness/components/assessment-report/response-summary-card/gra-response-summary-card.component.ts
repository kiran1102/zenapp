// Angular
import {
    Component,
    Input,
} from "@angular/core";

// Interface
import { IGRAResponseSummaryDetail} from "modules/global-readiness/interfaces/gra-report.interface";

@Component({
    selector: "gra-response-summary-card",
    templateUrl: "./gra-response-summary-card.component.html",
})

export class GRAResponseSummaryCardComponent {

    @Input() responseSummaryDetail: IGRAResponseSummaryDetail;
}
