// Angular
import {
    Component,
    Input,
} from "@angular/core";

// Interfaces
import { IGRASectionSummary } from "modules/global-readiness/interfaces/gra-report.interface";

@Component({
    selector: "gra-report-section-summary",
    templateUrl: "./gra-report-section-summary.component.html",
})

export class GRAReportSectionSummaryComponent {
    @Input() sectionSummary: IGRASectionSummary;
    @Input() templateType: string;

    showQuestionSummary = false;

    toggleQuestionSummary(): void {
        this.showQuestionSummary = !this.showQuestionSummary;
    }
}
