// Angular
import {
    Component,
    Input,
    OnInit,
} from "@angular/core";

// Interfaces
import { IGRAQuestionSummary } from "modules/global-readiness/interfaces/gra-report.interface";

// Constants/Enums
import { TemplateTypes } from "constants/template-types.constant";

@Component({
    selector: "gra-report-question-summary",
    templateUrl: "gra-report-question-summary.component.html",
})

export class GRAReportQuestionSummaryComponent implements OnInit {
    @Input() questionSummary: IGRAQuestionSummary[];
    @Input() templateType: string;

    showApplicableLaws: boolean;

    ngOnInit(): void {
        this.showApplicableLaws = (this.templateType === TemplateTypes.GRA);
    }

    trackByQuestionId(index: number, question: any): number {
        return question.id;
    }
}
