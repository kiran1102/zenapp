// Angular
import {
    Component,
    Input,
    OnInit,
} from "@angular/core";

// Interfaces
import { IGRAApplicableLawReadiness} from "modules/global-readiness/interfaces/gra-report.interface";

@Component({
    selector: "gra-applicable-law-readiness",
    templateUrl: "./gra-applicable-law-readiness.component.html",
})
export class GRAApplicableLawReadinessComponent implements OnInit {

    @Input() applicableLawReadiness: IGRAApplicableLawReadiness;

    greenBarValue: number;
    orangeBarValue: number;

    ngOnInit(): void {
        let totalQuestions =
            this.applicableLawReadiness.lawStatisticsSummary.readyQuestionsCount
            + this.applicableLawReadiness.lawStatisticsSummary.unAnsweredQuestionsCount
            + this.applicableLawReadiness.lawStatisticsSummary.gapsIdentified;
        if (totalQuestions === 0) totalQuestions = 1;
        this.greenBarValue = this.applicableLawReadiness.lawStatisticsSummary.readyQuestionsCount / totalQuestions * 100;
        this.orangeBarValue = this.applicableLawReadiness.lawStatisticsSummary.gapsIdentified / totalQuestions * 100;
    }
}
