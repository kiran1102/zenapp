import angular from "angular";

import { routes } from "./consentreceipt.routes";
import crConstants from "consentModule/shared/cr-constants";

export const consentReceiptLegacyModule = angular
    .module("zen.consentreceipt", [
        // Angular modules
        "ui.bootstrap",
        "ui.router",
        "ngMaterial",
        "ngSanitize",
        "ngStorage",
        "ngAnimate",
        // Custom modules
        "zen.identity",
    ])
    .config(routes)
    .constant("crConstants", crConstants)
    ;
