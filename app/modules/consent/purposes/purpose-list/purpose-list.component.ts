import { Component } from "@angular/core";
import { StateService } from "@uirouter/core";

// Constants
import crConstants from "consentModule/shared/cr-constants";

// Interfaces
import { IDropdownOption } from "interfaces/one-dropdown.interface";

// Models
import { PurposeFilters } from "crmodel/purpose-filters";
import { ColumnDefinition } from "crmodel/column-definition";
import { PurposeDetails } from "crmodel/purpose-details";
import {
    PurposeList,
    PurposeListItem,
} from "crmodel/purpose-list";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

// Rxjs
import {
    filter,
    take,
} from "rxjs/operators";

// Services
import { ConsentPurposeService } from "consentModule/shared/services/cr-purpose.service";
import {
    OtModalService,
    ConfirmModalType,
} from "@onetrust/vitreus";
import { Permissions } from "modules/shared/services/helper/permissions.service";

@Component({
    selector: "cr-purpose-list",
    templateUrl: "./purpose-list.component.html",
})
export class CrPurposeListComponent {

    crConstants = crConstants;
    columns: ColumnDefinition[];
    pageSize = 20;
    searchInProgress = true;
    tableData: PurposeList;
    filters: PurposeFilters = {};
    canDeletePurposes: boolean;

    constructor(
        private stateService: StateService,
        private consentPurposeService: ConsentPurposeService,
        private otModalService: OtModalService,
        private permissions: Permissions,
        private translatePipe: TranslatePipe,
    ) {
        this.canDeletePurposes = this.permissions.canShow("ConsentDeletePurposes") && !this.permissions.canShow("ConsentPurposeVersion");
        this.columns = [
            {
                columnName: this.translatePipe.transform("ProcessingPurpose"),
                cellValueKey: "Label",
                cellClass: "max-width-50",
                columnType: "idLink",
                route: "zen.app.pia.module.consentreceipt.views.purposes.details",
                routeVersionField: "Version",
            },
            {
                columnName: this.translatePipe.transform("Status"),
                cellValueKey: "Status",
                columnType: "statusTag",
                rowToText: (row: PurposeDetails): string => this.translateConst(row.Status, crConstants.CRPurposeStatus),
            },
            {
                columnName: this.translatePipe.transform("CurrentVersion"),
                cellValueKey: "Version",
                columnType: "versionTag",
                rowToText: (row: PurposeDetails): string => this.consentPurposeService.getVersionLabel(row.Version),
                rowToLabel: (): string => "text-bold one-tag--override one-tag--primary-white",
            },
            {
                columnName: this.translatePipe.transform("CollectionPoints"),
                cellValueKey: "CollectionPoints",
                columnType: "number",
            },
            {
                columnName: this.translatePipe.transform("ActiveDataSubjects"),
                cellValueKey: "ActiveDataSubjects",
                columnType: "number",
            },
            {
                columnName: this.translatePipe.transform("PendingDataSubjects"),
                cellValueKey: "PendingDataSubjects",
                columnType: "number",
            },
            {
                columnName: this.translatePipe.transform("WithdrawnDataSubjects"),
                cellValueKey: "WithdrawnDataSubjects",
                columnType: "number",
            },
        ];

        if (this.canDeletePurposes) {
            this.columns.push(
                {
                    columnType: "actions",
                    getOptions: (row: PurposeListItem): IDropdownOption[] => this.purposeOptions(row),
                },
            );
        }
    }

    ngOnInit() {
        this.filters.name = sessionStorage.getItem(`ConsentPurposeNameFilter`);
        this.searchPurposes(this.filters);
    }

    loadPage(page: number) {
        this.searchPurposes(this.filters, page);
    }

    goToRoute(cell: any) {
        this.stateService.go(cell.route, cell.params);
    }

    translateConst(value: string, constType: any): string {
        return this.consentPurposeService.translateConstValue(value, constType);
    }

    transactionStatusStyle(status: string): string {
        return crConstants.CRTransactionStatusStyles[status];
    }

    searchPurposes(filters: PurposeFilters = {}, page: number = 0) {
        this.searchInProgress = true;
        this.filters = filters;
        this.consentPurposeService.getPurposes(filters, page, this.pageSize)
            .then((items: PurposeList) => {
                this.tableData = items;
                this.searchInProgress = false;
            });
    }

    createPurpose() {
        this.stateService.go("zen.app.pia.module.consentreceipt.views.purposes.create");
    }

    private purposeOptions(purpose: PurposeListItem): IDropdownOption[] {
        return [{
            id: "delete",
            text: this.translatePipe.transform("DeletePurpose"),
            action: (option: IDropdownOption) => this.openConfirmationModal(purpose.Id, purpose.Version),
        }];
    }

    private openConfirmationModal(purposeId: string, purposeVersion: number) {
        this.otModalService.confirm({
            type: ConfirmModalType.DELETE,
            translations: {
                title: this.translatePipe.transform("DiscardPurposeDraft"),
                desc: this.translatePipe.transform("DeleteDraftWarning"),
                confirm: this.translatePipe.transform("ConfirmPurposeDeletion"),
                submit: this.translatePipe.transform("Confirm"),
                cancel: this.translatePipe.transform("Cancel"),
            },
        }).pipe(
            take(1),
            filter((result: boolean) => result),
        ).subscribe(() => {
            this.deletePurpose(purposeId, purposeVersion);
        });
    }

    private deletePurpose(purposeId: string, purposeVersion: number): ng.IPromise<boolean> {
        return this.consentPurposeService.deletePurpose(purposeId, purposeVersion).then((success: boolean): boolean => {
            if (success) {
                this.loadPage(0);
            }
            return success;
        });
    }
}
