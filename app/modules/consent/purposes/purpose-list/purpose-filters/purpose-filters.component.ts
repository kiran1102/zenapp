// Third Party
import {
    Component,
    Input,
} from "@angular/core";

// Components
import { CrFiltersBase } from "modules/consent/shared/cr-filters-base/cr-filters-base.component";

// Constants
import crConstants from "consentModule/shared/cr-constants";

// Interfaces
import { PurposeFilters } from "crmodel/purpose-filters";

@Component({
    selector: "cr-purpose-filters",
    templateUrl: "./purpose-filters.component.html",
})
export class CrPurposeFiltersComponent extends CrFiltersBase<PurposeFilters> {
    @Input() filters: PurposeFilters;
    crConstants = crConstants;

    ngOnChanges() {
        this.filters.name = sessionStorage.getItem(`ConsentPurposeNameFilter`) || "";
    }

    handleApply() {
        if (this.filters.name) { sessionStorage.setItem(`ConsentPurposeNameFilter`, this.filters.name); }
        super.handleApply();
    }

    handleClear() {
        super.handleClear();
        this.filters.name = "";
        sessionStorage.removeItem(`ConsentPurposeNameFilter`);
    }

    handleSearch(model: string = "", clear: boolean = false) {
        this.filters.name = model;
        if (!clear) {
            this.handleApply();
        }
    }

    protected populateFilters() {
        // placeholder for filter setup
    }
}
