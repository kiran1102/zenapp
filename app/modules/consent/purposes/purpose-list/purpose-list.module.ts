// Angular
import { NgModule } from "@angular/core";

// Components
import { CrPurposeListComponent } from "consentModule/purposes/purpose-list/purpose-list.component";
import { CrPurposeFiltersComponent } from "consentModule/purposes/purpose-list/purpose-filters/purpose-filters.component";

// Modules
import { PurposesSharedModule } from "consentModule/purposes/shared/purposes-shared.module";

@NgModule({
    imports: [
        PurposesSharedModule,
    ],
    declarations: [
        CrPurposeListComponent,
        CrPurposeFiltersComponent,
    ],
    entryComponents: [
        CrPurposeListComponent,
    ],
})
export class PurposeListModule { }
