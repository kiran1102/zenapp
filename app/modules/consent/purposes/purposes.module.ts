// Angular
import { NgModule } from "@angular/core";

// Modules
import { PurposesSharedModule } from "consentModule/purposes/shared/purposes-shared.module";
import { PurposeCreateModule } from "./purpose-create/purpose-create.module";
import { PurposeDetailsModule } from "./purpose-details/purpose-details.module";
import { PurposeListModule } from "./purpose-list/purpose-list.module";

@NgModule({
    imports: [
        PurposesSharedModule,
        PurposeCreateModule,
        PurposeDetailsModule,
        PurposeListModule,
    ],
})
export class PurposesModule { }
