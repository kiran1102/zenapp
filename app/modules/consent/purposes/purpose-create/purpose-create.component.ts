// Third Party
import {
    Component,
    Inject,
} from "@angular/core";
import { StateService } from "@uirouter/core";

// Constants
import crConstants from "consentModule/shared/cr-constants";

// Interfaces
import { IGetAllLanguageResponse } from "interfaces/dsar/dsar-edit-language.interface";

// Models
import { PurposeCreate } from "crmodel/purpose-create";

// Services
import { ConsentPurposeService } from "consentModule/shared/services/cr-purpose.service";
import {
    IConsentLength,
    IDropDownElement,
} from "consentModule/shared/services/consent-base.service";

@Component({
    selector: "cr-purpose-create",
    templateUrl: "./purpose-create.component.html",
})
export class CrPurposeCreateComponent {
    languageList: IGetAllLanguageResponse[];
    selectedLanguage: IGetAllLanguageResponse;
    isLoaded: boolean;
    purpose: PurposeCreate;
    createInProgress = false;
    crConsentLengthUnitTypes: IDropDownElement[];
    consentLengthDropdownElement: IDropDownElement;
    consentLength: IConsentLength;

    crConstants = crConstants;

    constructor(
        private stateService: StateService,
        private readonly consentPurposeService: ConsentPurposeService,
    ) {}

    ngOnInit() {
        this.crConsentLengthUnitTypes = this.consentPurposeService.getDropDownOptions(this.crConstants.CRLengthUnitTypes);

        this.purpose = new PurposeCreate();
        this.consentLength = {
            unit: this.crConstants.CRLengthUnitTypes.Days,
            number: 7,
            untilWithdrawn: true,
        };
        this.consentLengthDropdownElement = this.crConsentLengthUnitTypes[0];
        this.isLoaded = false;
        this.consentPurposeService.getAllLanguages().then((languages: IGetAllLanguageResponse[]) => {
            this.languageList = languages;
            this.isLoaded = true;
        });
    }

    createPurpose(): ng.IPromise<boolean> {
        this.createInProgress = true;
        this.purpose.DefaultLanguage = this.selectedLanguage.Code;
        this.purpose.ConsentLifeSpan = this.consentPurposeService.calculateConsentLength(this.consentLength);
        return this.consentPurposeService.createPurpose(this.purpose).then((result: string): boolean => {
            this.createInProgress = false;
            if (result) {
                this.stateService.go("zen.app.pia.module.consentreceipt.views.purposes.list");
            }
            return Boolean(result);
        });
    }

    toggleUntilWithdrawn() {
        this.consentLength.untilWithdrawn = !this.consentLength.untilWithdrawn;
    }

    handleChanged(payload: string, prop: keyof PurposeCreate) {
        this.purpose[prop] = payload;
    }

    selectLanguage(language: IGetAllLanguageResponse): void {
        this.selectedLanguage = language;
    }

    consentLengthUnit(selection: IDropDownElement) {
        this.consentLengthDropdownElement = selection;
        this.consentLength.unit = selection.value;
        this.calcLifeSpan();
    }

    consentLengthNumber(value: number) {
        this.consentLength.number = value;
        this.calcLifeSpan();
    }

    calcLifeSpan() {
        this.purpose.ConsentLifeSpan = this.consentPurposeService.calculateConsentLength(this.consentLength);
    }

    returnToPurposeList() {
        this.stateService.go("zen.app.pia.module.consentreceipt.views.purposes.list");
    }

    get emptyField(): boolean {
        if (this.purpose.Name && this.purpose.Description) {
            return !(
                this.purpose.Name.trim()
                && this.purpose.Description.trim()
            );
        } else {
            return true;
        }
    }
}
