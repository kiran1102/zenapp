// Angular
import { NgModule } from "@angular/core";

// Components
import { CrPurposeCreateComponent } from "./purpose-create.component";

// Modules
import { PurposesSharedModule } from "consentModule/purposes/shared/purposes-shared.module";

@NgModule({
    imports: [
        PurposesSharedModule,
    ],
    declarations: [
        CrPurposeCreateComponent,
    ],
    entryComponents: [
        CrPurposeCreateComponent,
    ],
})
export class PurposeCreateModule { }
