import {
    Component,
    Inject,
    OnInit,
} from "@angular/core";
import { StateService } from "@uirouter/core";

// Components
import { CrPurposeBaseComponent } from "../purpose-base/purpose-base.component";

// Constants
import crConstants from "consentModule/shared/cr-constants";

// Interfaces
import { IHorseshoeGraphData } from "interfaces/one-horseshoe-graph.interface";

// Models
import { PurposeDetails } from "crmodel/purpose-details";

// Redux
import { StoreToken } from "tokens/redux-store.token";
import {
    IStore,
    IStoreState,
} from "interfaces/redux.interface";

// Services
import { ConsentPurposeService } from "consentModule/shared/services/cr-purpose.service";
import {
    IConsentLength,
    IDropDownElement,
} from "consentModule/shared/services/consent-base.service";
import { PurposeActionService } from "consentModule/purposes/purpose-details/purpose-action.service";
import { getPurposeDraft } from "oneRedux/reducers/purpose.reducer";

@Component({
    selector: "cr-purpose-form",
    templateUrl: "./purpose-form.component.html",
})
export class CrPurposeFormComponent extends CrPurposeBaseComponent implements OnInit {
    purposeId: string;
    version: number;
    dataSubjectCounters: IHorseshoeGraphData;
    showPreferences: boolean;
    isInitialized: boolean;
    consentLength: IConsentLength;
    consentLengthDropdownElement: IDropDownElement;
    crConsentLengthUnitTypes: IDropDownElement[];

    constructor(
        @Inject(StoreToken) private store: IStore,
        private stateService: StateService,
        private consentPurposeService: ConsentPurposeService,
        private purposeActionService: PurposeActionService,
    ) {
        super(store, consentPurposeService);
    }

    ngOnInit(): void {
        this.crConsentLengthUnitTypes = this.consentPurposeService.getDropDownOptions(crConstants.CRLengthUnitTypes);
        super.ngOnInit();
        this.purposeId = this.stateService.params.Id;
        this.version = Number(this.stateService.params.version || 1);
    }

    toggleUntilWithdrawn() {
        this.consentLength.untilWithdrawn = !this.consentLength.untilWithdrawn;
        this.calcLifeSpan();
    }

    consentLengthUnit(selection: IDropDownElement) {
        this.consentLengthDropdownElement = selection;
        this.consentLength.unit = selection.value;
        this.calcLifeSpan();
    }

    consentLengthNumber(value: number) {
        this.consentLength.number = value;
        this.calcLifeSpan();
    }

    calcLifeSpan() {
        this.purpose.LifeSpan = this.consentPurposeService.calculateConsentLength(this.consentLength);
        this.purposeActionService.updatePurposeDraft(this.purpose);
    }

    onModelChange(payload: string, prop: keyof PurposeDetails): void {
        this.purposeActionService.onPurposeModelChange(payload, prop);
    }

    parseConsentLength() {
        this.consentLength = this.consentPurposeService.parseConsentLength(this.purpose.LifeSpan);
        if (this.consentLength.untilWithdrawn) {
            this.consentLength.unit = this.crConsentLengthUnitTypes[0].value;
            this.consentLength.number = 7;
            this.consentLengthDropdownElement = this.crConsentLengthUnitTypes[0];
        } else {
            this.consentLengthDropdownElement = this.crConsentLengthUnitTypes.find((unit: IDropDownElement): boolean => {
                return unit.value === this.consentLength.unit;
            });
        }
    }

    get formattedPurposeLifeSpan(): string {
        return this.purpose ? this.consentPurposeService.formatDuration(this.purpose.LifeSpan) : "";
    }

    protected onStateUpdate(state: IStoreState) {
        super.onStateUpdate(state);
        this.purpose = getPurposeDraft(state);
        if (!this.isInitialized) {
            this.parseConsentLength();
            this.isInitialized = true;
        }
    }
}
