// Third Party
import {
    Component,
    Inject,
    OnInit,
} from "@angular/core";
import { StateService } from "@uirouter/core";
import {
    isEmpty,
    orderBy,
} from "lodash";

// Components
import { CrPurposeBaseComponent } from "./purpose-base/purpose-base.component";
import { LegacyPurposeLanguageSelectionModalComponent } from "./legacy-purpose-language-selection-modal/legacy-purpose-language-selection-modal.component";

// Constants
import crConstants from "consentModule/shared/cr-constants";

// Interfaces
import { ITabsNav } from "interfaces/one-tabs-nav.interface";
import { ConsentPurposeService } from "consentModule/shared/services/cr-purpose.service";
import { IDropdownOption } from "interfaces/one-dropdown.interface";
import { IVersionListItem } from "interfaces/consent/cr-versions.interface";

// Models
import { PurposeDetails } from "crmodel/purpose-details";

// Redux
import { StoreToken } from "tokens/redux-store.token";
import {
    IStore,
    IStoreState,
} from "interfaces/redux.interface";
import { getPurposeValidStatus } from "oneRedux/reducers/purpose.reducer";

// Rxjs
import {
    filter,
    take,
} from "rxjs/operators";

// Services
import { Permissions } from "modules/shared/services/helper/permissions.service";
import { PurposeActionService } from "consentModule/purposes/purpose-details/purpose-action.service";
import {
    OtModalService,
    ConfirmModalType,
} from "@onetrust/vitreus";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

@Component({
    selector: "cr-purpose-details",
    templateUrl: "./purpose-details.component.html",
})

export class CrPurposeDetailsComponent extends CrPurposeBaseComponent implements OnInit {
    purposeId: string;
    tabName: string;
    tabs: ITabsNav[];
    purposeVersions: IVersionListItem[];
    ellipsisOptions: IDropdownOption[];

    canDisplayLanguages = this.permissions.canShow("ConsentPurposeLanguageDisplay");
    canPublishPurpose = this.permissions.canShow("ConsentPurposePublish");
    createNewVersionToggle = this.permissions.canShow("ConsentPurposeVersion");
    canAddTopics = this.permissions.canShow("ConsentEditTopics");
    redirectAfterPublish = this.permissions.canShow("ConsentPurposeVersionRedirect");
    canDeletePurpose = this.permissions.canShow("ConsentDeletePurposes");
    isLoading: boolean;
    isDrawerOpen: boolean;
    canSavePurpose: boolean;
    isInitialized: boolean;
    selectedVersion: number;
    crConstants = crConstants;

    private readonly canAddCustomPreferences = this.permissions.canShow("ConsentEditCustomPreferences");

    constructor(
        @Inject(StoreToken) private store: IStore,
        private stateService: StateService,
        private consentPurposeService: ConsentPurposeService,
        private purposeActionService: PurposeActionService,
        private otModalService: OtModalService,
        private permissions: Permissions,
        private translatePipe: TranslatePipe,
    ) {
        super(store, consentPurposeService);
        this.purposeId = stateService.params.Id;
        this.tabName = stateService.params.tab;
        this.selectedVersion = Number(stateService.params.version || 1);
    }

    ngOnInit() {
        this.purposeActionService.fetchPurpose(this.purposeId, this.selectedVersion).then(() => {
            if (!this.purpose.Languages[0].Language) {
                this.legacyLanguageSelect();
            }
        });
        super.ngOnInit();
    }

    getTabs() {
        this.tabs = [
            {
                id: "details",
                text: this.translatePipe.transform("Details"),
                action: (option: ITabsNav) => this.tabChanged(option),
            },
        ];

        const isCookieType = this.purpose.Type === crConstants.CRPurposeTypes.Cookie;

        if (this.purpose && !isCookieType) {
            if (this.canDisplayLanguages) {
                this.tabs.push({
                    id: "languages",
                    text: this.translatePipe.transform("SupportedLanguages"),
                    action: (option: ITabsNav) => this.onLanguageTabSelect(option),
                });
            }

            if (this.canAddCustomPreferences) {
                this.tabs.push({
                    id: this.crConstants.CRPurposeTabs.CustomPreferences,
                    text: this.translatePipe.transform("CustomPreferences"),
                    action: (option: ITabsNav) => this.tabChanged(option),
                });
            }

            if (this.canAddTopics) {
                this.tabs.push({
                    id: "topics",
                    text: this.translatePipe.transform("Topics"),
                    action: (option: ITabsNav) => this.tabChanged(option),
                });
            }
        }

        this.tabs.push({
            id: "report",
            text: this.translatePipe.transform("Report"),
            action: (option: ITabsNav) => this.tabChanged(option),
        });
    }

    getEllipsisOptions() {
        this.ellipsisOptions = [
            {
                text: this.translatePipe.transform("ViewVersions"),
                action: () => { this.toggleDrawer(); },
            },
        ];
    }

    getPurposeVersions() {
        this.consentPurposeService.getSelectedPurposeVersions(this.purpose.Id).then((result: IVersionListItem[]) => {
            this.purposeVersions = orderBy(result, "Version", "desc");
        });
    }

    toggleDrawer() {
        this.isDrawerOpen = !this.isDrawerOpen;
    }

    publishConfirmationModal() {
        this.otModalService.confirm({
            type: ConfirmModalType.WARNING,
            translations: {
                title: this.translatePipe.transform("ConfirmPurposePublish"),
                desc: this.translatePipe.transform("PurposePublishDescription"),
                confirm: this.translatePipe.transform("AreYouSureContinue"),
                submit: this.translatePipe.transform("Publish"),
                cancel: this.translatePipe.transform("Cancel"),
            },
        }).pipe(
            take(1),
            filter((result: boolean) => result),
        ).subscribe(() => {
            this.publishPurpose();
        });
    }

    publishPurpose(): ng.IPromise<boolean> {
        this.isLoading = true;
        return this.purposeActionService.publishPurpose()
            .then((response: boolean): boolean => {
                this.isLoading = false;
                if (response) {
                    if (this.redirectAfterPublish && this.purpose.Version > 1) {
                        this.stateService.go("zen.app.pia.module.consentreceipt.views.collections.updates", { purposeId: this.purposeId, purposeVersion: this.purpose.Version });
                    } else {
                        this.getPurposeVersions();
                        this.ngOnInit();
                    }
                }
                return response;
            });
    }

    savePurpose() {
        this.purposeActionService.savePurpose();
    }

    createNewVersion(): ng.IPromise<PurposeDetails> {
        this.isLoading = true;
        return this.consentPurposeService.createNewVersion(this.purpose.Id).then((result: PurposeDetails): PurposeDetails => {
            if (result) {
                this.stateService.go("zen.app.pia.module.consentreceipt.views.purposes.details", { Id: this.purpose.Id, version: result.Version });
            }
            this.isLoading = false;
            return result;
        });
    }

    goToVersion(purposeVersion: number) {
        this.stateService.go("zen.app.pia.module.consentreceipt.views.purposes.details", { Id: this.purpose.Id, version: purposeVersion });
    }

    goToPreviousVersion(currentVersion: number) {
        if (currentVersion > 1) {
            this.stateService.go("zen.app.pia.module.consentreceipt.views.purposes.details", { Id: this.purpose.Id, version: currentVersion - 1 }, { reload: true });
        } else {
            this.stateService.go("zen.app.pia.module.consentreceipt.views.purposes.list");
        }
    }

    onHeaderRouteClick(route: { path: string, params: any }) {
        this.stateService.go(route.path);
    }

    deleteDraftConfirmation(purposeVersion: number) {
        this.otModalService.confirm({
            type: ConfirmModalType.DELETE,
            translations: {
                title: this.translatePipe.transform("DiscardPurposeDraft"),
                desc: this.translatePipe.transform("DeleteDraftWarning"),
                confirm: this.translatePipe.transform("ConfirmPurposeDeletion"),
                submit: this.translatePipe.transform("Confirm"),
                cancel: this.translatePipe.transform("Cancel"),
            },
        }).pipe(
            take(1),
            filter((result: boolean) => result),
        ).subscribe(() => {
            this.consentPurposeService.deletePurpose(this.purpose.Id, purposeVersion)
                .then((result: boolean): boolean => {
                    if (result) {
                        this.goToPreviousVersion(purposeVersion);
                    }
                    return result;
                });
        });
    }

    legacyLanguageSelect() {
        this.otModalService.create(
            LegacyPurposeLanguageSelectionModalComponent,
            {
                isComponent: true,
            },
        ).pipe(
            take(1),
            filter((result: string) => result.trim() !== null),
        ).subscribe((result: string) => {
            this.purpose.Languages[0].Language = result;
            this.savePurpose();
        });
    }

    onLanguageTabSelect(option: ITabsNav) {
        if (this.purpose.Languages[0].Language) {
            this.tabChanged(option);
        } else {
            this.legacyLanguageSelect();
        }
    }

    protected onStateUpdate(state: IStoreState) {
        super.onStateUpdate(state);
        this.canSavePurpose = getPurposeValidStatus(state);
        if (this.purpose && !this.isInitialized) {
            this.isInitialized = true;
            this.getTabs();
            this.getEllipsisOptions();
            this.getPurposeVersions();
            this.isInitialized = true;
        }
    }

    private tabChanged = (option: ITabsNav) => {
        this.tabName = option.id;
        this.stateService.go(".", { Id: this.purposeId, tab: option.id });
    }

    get displayStatusClass(): string {
        return `text-bold one-tag--no-bg-rounded one-tag--override ${crConstants.CRPurposeRoundedStatusStyles[this.purpose.Status]}`;
    }

    get isPurposeEmpty(): boolean {
        return isEmpty(this.purpose);
    }
}
