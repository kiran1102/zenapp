// Third Party
import { Component, EventEmitter, Input, Output } from "@angular/core";
import { StateService } from "@uirouter/core";

// Models
import crConstants from "consentModule/shared/cr-constants";

// Interfaces
import { PurposeDetails } from "crmodel/purpose-details";
import { PurposeCustomPreference } from "crmodel/custom-preference-list";
import { ColumnDefinition } from "crmodel/column-definition";
import { IPageableOf } from "interfaces/pagination.interface";
import { IBasicTableCell } from "interfaces/tables.interface";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

interface IEllipsisActionOption {
    text: string;
    action: () => void;
}

@Component({
    selector: "cr-purpose-custom-preferences-list",
    templateUrl: "./purpose-custom-preferences-list.component.html",
})
export class PurposeCustomPreferencesListComponent {
    @Input() set purpose(purpose: PurposeDetails) {
        this._purpose = purpose;
        this.setupTableModel();
    }

    get purpose(): PurposeDetails {
        return this._purpose;
    }

    @Output() onRemoveRequested = new EventEmitter<PurposeCustomPreference>();

    ellipsisOptions: Array<{ text: string; action: () => void }>;
    columns: ColumnDefinition[];
    rows: Partial<IPageableOf<PurposeCustomPreference>>;

    readonly crConstants = crConstants;

    private _purpose: PurposeDetails;

    constructor(
        private translatePipe: TranslatePipe,
        private stateService: StateService,
    ) {
    }

    goToRoute(cell: IBasicTableCell) {
        this.stateService.go(cell.route, cell.params);
    }

    private setupTableModel() {
        this.columns = this.getColumns();
        this.rows = this.getRows();
    }

    private getColumns(): ColumnDefinition[] {
        const actions = this.getColumnsActions();

        return [
            {
                columnName: this.translatePipe.transform("Name"),
                cellValueKey: "Name",
                cellClass: "max-width-50",
                columnType: "idLink",
                route: "zen.app.pia.module.consentreceipt.views.custompreferences.details",
            },
            {
                columnName: this.translatePipe.transform("ID"),
                cellValueKey: "PurposeCustomPreferenceId",
                columnType: "number",
            },
            {
                columnName: this.translatePipe.transform("Options"),
                rowToText(row: PurposeCustomPreference) {
                    return row.NumberOfOptions.toString();
                },
                columnType: "number",
            },
            {
                columnName: this.translatePipe.transform("Languages"),
                rowToText(row: PurposeCustomPreference) {
                    return row.NumberOfLanguages.toString();
                },
                columnType: "number",
            },
            {
                columnName: this.translatePipe.transform("CreatedDate"),
                cellValueKey: "CreatedDate",
                columnType: "timestamp",
            },
            {
                columnName: this.translatePipe.transform("UpdatedDate"),
                cellValueKey: "UpdatedDate",
                columnType: "timestamp",
            },
            actions,
        ];
    }

    private getRows(): Partial<IPageableOf<PurposeCustomPreference>> {
        const preferences = this.purpose.CustomPreferences;
        return {
            content: preferences,
            totalElements: preferences.length,
            number: 0,
        };
    }

    private getColumnsActions(): ColumnDefinition | {} {
        if (this.isDraft()) {
            return {
                columnType: "actions",
                getOptions: (row: PurposeCustomPreference) => {
                    return this.getEllipsisOptions(row);
                },
            };
        }

        return {};
    }

    private getEllipsisOptions(row: PurposeCustomPreference): IEllipsisActionOption[] {
        const removePreferenceOption = this.getRemoveCustomPreferenceOption(row);
        return [removePreferenceOption];
    }

    private isDraft(): boolean {
        return this.purpose.Status === crConstants.CRPurposeStatus.Draft;
    }

    private getRemoveCustomPreferenceOption(row: PurposeCustomPreference): IEllipsisActionOption {
        const REMOVE_CUSTOM_PREF_KEY = "RemoveCustomPreference";
        const text = this.translatePipe.transform(REMOVE_CUSTOM_PREF_KEY);
        const action = () => this.onRemoveRequested.emit(row);

        return { text, action };
    }
}
