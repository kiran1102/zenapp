// Third Party
import {
    Inject,
    Component,
    OnInit,
} from "@angular/core";

// Components
import { CrPurposeBaseComponent } from "../purpose-base/purpose-base.component";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

// Redux
import { StoreToken } from "tokens/redux-store.token";
import { IStore } from "interfaces/redux.interface";

// rx
import { filter, mergeMap, take } from "rxjs/operators";

// Services
import { PurposeActionService } from "consentModule/purposes/purpose-details/purpose-action.service";
import { ConsentPurposeService } from "crservice/cr-purpose.service";

// Modal
import { ConfirmModalType, IConfirmModalConfig, OtModalService } from "@onetrust/vitreus";
import { CrAddCustomPreferenceModalComponent } from "consentModule/purposes/purpose-details/purpose-custom-preferences/add-custom-preference-modal/add-custom-preference-modal.component";

// Interfaces
import { PurposeDetails } from "crmodel/purpose-details";
import { PurposeCustomPreference } from "crmodel/custom-preference-list";
import { ISelectTopicModalResolve } from "interfaces/consent/select-topic-modal-resolve.interface";
import { Observable } from "rxjs";

@Component({
    selector: "cr-purpose-custom-preferences",
    templateUrl: "./purpose-custom-preferences.component.html",
})
export class CrPurposeCustomPreferencesComponent extends CrPurposeBaseComponent implements OnInit {
    public canAddCustomPreferences: boolean;
    public shouldShowCustomPreferencesTable: boolean;

    constructor(
        @Inject(StoreToken) private store: IStore,
        private consentPurposeService: ConsentPurposeService,
        private purposeActionService: PurposeActionService,
        private translatePipe: TranslatePipe,
        private otModalService: OtModalService,
    ) {
        super(store, consentPurposeService);
    }

    ngOnInit() {
        super.ngOnInit();
        this.setupProperties();
    }

    openAddCustomPreferenceModal() {
        const modalData: ISelectTopicModalResolve = {
            purpose: this.purpose,
        };

        const config = {
            isComponent: true,
        };

        this.otModalService
            .create(CrAddCustomPreferenceModalComponent, config, modalData)
            .pipe(
                take(1),
                filter(Boolean),
            )
            .subscribe(() => {
                this.refreshPurposeDetails();
            });
    }

    openRemoveCustomPreferenceModal(customPreference: PurposeCustomPreference) {
        const translations = {
            title: this.translatePipe.transform("RemoveCustomPreference"),
            desc: "",
            confirm: this.translatePipe.transform("ConfirmPurposeCustomPreferenceRemoval"),
            submit: this.translatePipe.transform("Remove"),
            cancel: this.translatePipe.transform("Cancel"),
        };

        const config: IConfirmModalConfig = {
            type: ConfirmModalType.DELETE,
            translations,
        };

        this.otModalService
            .confirm(config)
            .pipe(
                take(1),
                filter(Boolean),
                mergeMap(() => {
                    const id = customPreference.PurposeCustomPreferenceId;
                    return this.removeCustomPreference(id);
                }),
            )
            .subscribe(() => {
                this.refreshPurposeDetails();
            });
    }

    private setupProperties() {
        this.canAddCustomPreferences = this.isDraft();
        this.shouldShowCustomPreferencesTable = this.hasCustomPreferences();
    }

    private removeCustomPreference(customPreferenceId: string): Observable<boolean> {
        const { Id, Version } = this.purpose;
        return this.consentPurposeService.removeCustomPreference(Id, Version, customPreferenceId);
    }

    private refreshPurposeDetails() {
        const { Id, Version } = this.purpose;

        this.consentPurposeService
            .getPurposeDetails(Id, Version)
            .then((purpose: PurposeDetails) => {
                this.purpose = purpose;
                this.purposeActionService.updatePurposeDraft(this.purpose);
                this.setupProperties();
            });
    }

    private isDraft(): boolean {
        return this.purpose.Status === this.crConstants.CRPurposeStatus.Draft;
    }

    private hasCustomPreferences(): boolean {
        return this.purpose.CustomPreferences && this.purpose.CustomPreferences.length > 0;
    }
}
