// Third Party
import {
    Component,
    OnInit,
} from "@angular/core";

// Constants
import { crConstants } from "consentModule/shared/cr-constants";

// Models
import { CustomPreferenceListItem } from "crmodel/custom-preference-list";

// Rxjs
import { BehaviorSubject, combineLatest, Observable, Subject } from "rxjs";
import { tap, map, mergeMap } from "rxjs/operators";

// Services
import { ISelectTopicModalResolve } from "interfaces/consent/select-topic-modal-resolve.interface";
import { ConsentCustomPreferenceService } from "consentModule/custom-preferences/shared/custom-preference.service";
import { LookupService } from "sharedModules/services/helper/lookup.service.ts";
import { ConsentPurposeService } from "crservice/cr-purpose.service";

@Component({
    selector: "cr-add-custom-preference-modal",
    templateUrl: "./add-custom-preference-modal.component.html",
})
export class CrAddCustomPreferenceModalComponent implements OnInit {
    customPreferences$: Observable<CustomPreferenceListItem[]>;
    selectedCustomPreferences$ = new BehaviorSubject<CustomPreferenceListItem[]>([]);
    isAddButtonDisabled$: Observable<boolean>;
    isSaving = false;

    readonly crConstants = crConstants;
    readonly otModalData: ISelectTopicModalResolve;
    readonly otModalCloseEvent: Subject<boolean>;

    private readonly onInputChange$ = new BehaviorSubject<string>("");

    constructor(
        private consentCustomPreferenceService: ConsentCustomPreferenceService,
        private consentPurposeService: ConsentPurposeService,
        private lookupService: LookupService,
    ) {
    }

    ngOnInit() {
        this.customPreferences$ = combineLatest(
            this.getCustomPreferences(),
            this.onInputChange$.asObservable(),
        ).pipe(
            mergeMap(([preferences, input]: [CustomPreferenceListItem[], string]) => {
                return this.getFilteredPreferencesList(preferences, input);
            }),
        );

        this.isAddButtonDisabled$ = this.selectedCustomPreferences$.pipe(
            map((preferences) => preferences.length === 0),
        );
    }

    onModelChanged<T extends { currentValue: CustomPreferenceListItem[] }>(changes: T) {
        this.selectedCustomPreferences$.next(changes.currentValue);
        this.onInputChange$.next("");
    }

    closeModal(payload = false) {
        this.otModalCloseEvent.next(payload);
    }

    saveSelectedCustomPreferences() {
        const { Id, Version } = this.otModalData.purpose;
        const preferences = this.selectedCustomPreferences$.value;
        const ids = preferences.map((preference) => preference.Id);

        this.submitSavePreferencesRequest(Id, Version, ids);
    }

    onInputChange({ value }: { value: string }) {
        this.onInputChange$.next(value);
    }

    private getFilteredPreferencesList(
        preferences: CustomPreferenceListItem[],
        input: string,
    ): Observable<CustomPreferenceListItem[]> {
        return this.selectedCustomPreferences$.pipe(
            map((selected: CustomPreferenceListItem[]) => {
                const sortedPreferencesList = this.getSortedPreferencesList(preferences);
                return this.filterCustomPreferencesList(selected, sortedPreferencesList, input);
            }),
        );
    }

    private getSortedPreferencesList(preferences: CustomPreferenceListItem[]): CustomPreferenceListItem[] {
        return [...preferences].sort((prev: CustomPreferenceListItem, current: CustomPreferenceListItem) => {
            const prevPreferenceName = prev.Name.toUpperCase();
            const currentPreferenceName = current.Name.toUpperCase();

            if (prevPreferenceName < currentPreferenceName) {
                return -1;
            }

            if (prevPreferenceName > currentPreferenceName) {
                return 1;
            }

            return 0;
        });
    }

    private filterCustomPreferencesList(
        selections: CustomPreferenceListItem[],
        preferences: CustomPreferenceListItem[],
        input: string,
    ): CustomPreferenceListItem[] {
        const ID_KEY: keyof CustomPreferenceListItem = "Id";
        const NAME_KEY: keyof CustomPreferenceListItem = "Name";
        const selected = [...this.otModalData.purpose.CustomPreferences, ...selections];

        return this.lookupService.filterOptionsByInput(selected, preferences, input, ID_KEY, NAME_KEY);
    }

    private submitSavePreferencesRequest(id: string, version: number, ids: string[]) {
        const onRequestSuccessful = (result: boolean) => {
            this.isSaving = false;
            this.closeModal(result);
        };

        this.consentPurposeService
            .addCustomPreferences(id, version, ids)
            .pipe(
                tap(() => this.isSaving = true),
            )
            .subscribe(onRequestSuccessful);
    }

    private getCustomPreferences(): Observable<CustomPreferenceListItem[]> {
        return this.consentCustomPreferenceService.getAllCustomPreferences();
    }
}
