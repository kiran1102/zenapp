// 3rd Party
import {
    map,
    some,
    filter,
} from "lodash";
import {
    Component,
    Inject,
} from "@angular/core";

// Interface
import { ISelectTopicModalResolve } from "interfaces/consent/select-topic-modal-resolve.interface";

// Models
import { EntityItem } from "crmodel/entity-item";
import { TopicListItem } from "crmodel/topic-list";

// Redux
import { IStore } from "interfaces/redux.interface";
import { StoreToken } from "tokens/redux-store.token";

// Rxjs
import { Subject } from "rxjs";

// Services
import { TopicService } from "consentModule/shared/services/cr-topic.service";
import { ConsentPurposeService } from "consentModule/shared/services/cr-purpose.service";
import { LookupService } from "sharedModules/services/helper/lookup.service.ts";

@Component({
    selector: "cr-purpose-select-topics-modal",
    templateUrl: "./purpose-select-topics-modal.component.html",
})
export class CrPurposeSelectTopicsModalComponent {
    fullTopicList: EntityItem[];
    selectedTopics: EntityItem[] = [];
    topicOptions: EntityItem[];
    isSaving: boolean;
    inputValue = "";

    otModalData: ISelectTopicModalResolve;
    otModalCloseEvent: Subject<boolean>;

    constructor(
        @Inject(StoreToken) private store: IStore,
        private topicService: TopicService,
        private consentPurposeService: ConsentPurposeService,
        private lookupService: LookupService,
    ) { }

    ngOnInit() {
        this.topicService.getTopicFilters().then((topics: EntityItem[]) => {
            this.fullTopicList = topics;
            this.topicOptions = this.lookupService.filterOptionsBySelections(this.otModalData.purpose.Topics, this.fullTopicList, "Id");
        });
    }

    filterTopics(allTopics: EntityItem[], existingTopics: TopicListItem[]): EntityItem[] {
        const filtered = filter(allTopics, (topic: EntityItem): boolean =>
            !some(existingTopics, (existingTopic: TopicListItem): boolean => existingTopic.Name === topic.Label));
        return filtered;
    }

    closeModal() {
        this.otModalCloseEvent.next(false);
    }

    onInputChange({ key, value }) {
        if (key === "Enter") {
            this.selectOption({ currentValue: this.selectedTopics.push(this.topicOptions[0]) });
            return;
        }
        this.topicOptions = this.lookupService.filterOptionsByInput(this.selectedTopics, this.fullTopicList, value, "Id", "Label");
    }

    selectOption({ currentValue }) {
        this.selectedTopics = currentValue;
        this.topicOptions = this.lookupService.filterOptionsBySelections(this.selectedTopics, this.fullTopicList, "Id");
    }

    addSelectedTopics(selectedTopics: EntityItem[]) {
        const topicIds = map(selectedTopics, (topic: EntityItem): string => topic.Id);
        this.isSaving = true;
        this.consentPurposeService.addPurposeTopics(this.otModalData.purpose.Id, this.otModalData.purpose.Version, topicIds)
            .then((result: boolean) => {
                this.isSaving = false;
                this.otModalCloseEvent.next(result);
            });
    }
}
