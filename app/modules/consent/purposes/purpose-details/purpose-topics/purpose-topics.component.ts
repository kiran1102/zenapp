// Third Party
import {
    Inject,
    Component,
    OnInit,
} from "@angular/core";

// Components
import { CrPurposeBaseComponent } from "../purpose-base/purpose-base.component";
import { CrPurposeSelectTopicsModalComponent } from "./purpose-select-topics-modal/purpose-select-topics-modal.component";
import { CrPurposeUpdateTopicModalComponent } from "./purpose-update-topic-modal/purpose-update-topic-modal.component";

// Interfaces
import { IDropdownOption } from "interfaces/one-dropdown.interface";
import { IUpdateTopicModalResolve } from "./purpose-update-topic-modal/update-topic-modal-resolve.interface";
import { ISelectTopicModalResolve } from "interfaces/consent/select-topic-modal-resolve.interface";

// Models
import { TopicListItem } from "crmodel/topic-list";
import { PurposeDetails } from "crmodel/purpose-details";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

// Redux
import { StoreToken } from "tokens/redux-store.token";
import {
    IStore,
    IStoreState,
} from "interfaces/redux.interface";
import {
    getPurposeDraft,
    getPurposeSavingStatus,
} from "oneRedux/reducers/purpose.reducer";

// Rxjs
import {
    filter,
    take,
} from "rxjs/operators";

// Services
import { ConsentPurposeService } from "consentModule/shared/services/cr-purpose.service";
import { PurposeActionService } from "consentModule/purposes/purpose-details/purpose-action.service";
import {
    OtModalService,
    ConfirmModalType,
} from "@onetrust/vitreus";

@Component({
    selector: "cr-purpose-topics",
    templateUrl: "./purpose-topics.component.html",
})
export class CrPurposeTopicsComponent extends CrPurposeBaseComponent implements OnInit {
    isSaving: boolean;
    isInitialized: boolean;
    ellipsisOptions: IDropdownOption[] = [];
    pageSize = 100;

    constructor(
        @Inject(StoreToken) private readonly store: IStore,
        private readonly otModalService: OtModalService,
        private readonly consentPurposeService: ConsentPurposeService,
        readonly purposeActionService: PurposeActionService,
        private readonly translatePipe: TranslatePipe,
    ) {
        super(store, consentPurposeService);
    }

    ngOnInit() {
        super.ngOnInit();
    }

    addTopics() {
        const modalData: ISelectTopicModalResolve = {
            purpose: this.purpose,
        };
        this.otModalService.create(
            CrPurposeSelectTopicsModalComponent,
            {
                isComponent: true,
            },
            modalData,
        ).pipe(
            take(1),
            filter((result: boolean) => result),
        ).subscribe((result: boolean) => {
            if (result) {
                this.consentPurposeService.getPurposeDetails(this.purpose.Id, this.purpose.Version)
                    .then((purpose: PurposeDetails) => {
                        this.purpose = purpose;
                        this.purposeActionService.updatePurposeDraft(this.purpose);
                    });
            }
        });
    }

    getEllipsisOptions(row: TopicListItem) {
        this.ellipsisOptions = [];
        if (this.purpose.Status === this.crConstants.CRPurposeStatus.Draft) {
            this.ellipsisOptions.push({
                text: this.translatePipe.transform("UpdateTopic"),
                action: (): void => this.openEditModal(row),
            });
            if (row.CanDelete) {
                this.ellipsisOptions.push({
                    text: this.translatePipe.transform("RemoveTopic"),
                    action: (): void => this.openConfirmationModal(row.Id),
                });
            }
        }
    }

    protected onStateUpdate(state: IStoreState) {
        super.onStateUpdate(state);
        this.isSaving = getPurposeSavingStatus(state);
        if (!this.isSaving) { this.purpose = getPurposeDraft(state); }
    }

    private openEditModal(topic: TopicListItem) {
        const updateTopicModalData: IUpdateTopicModalResolve = {
            purposeId: this.purpose.Id,
            purposeVersion: this.purpose.Version,
            topicId: topic.Id,
            integrationKey: topic.IntegrationKey,
        };
        this.otModalService.create(
            CrPurposeUpdateTopicModalComponent,
            {
                isComponent: true,
            },
            updateTopicModalData,
        ).pipe(
            take(1),
            filter((result: string) => Boolean(result.trim())),
        ).subscribe((integrationKey: string): void => {
            if (integrationKey) {
                topic.IntegrationKey = integrationKey;
            }
        });
    }

    private openConfirmationModal(topicId: string): void {
        this.otModalService.confirm({
            type: ConfirmModalType.DELETE,
            translations: {
                title: this.translatePipe.transform("RemoveTopic"),
                desc: "",
                confirm: this.translatePipe.transform("ConfirmTopicRemoval"),
                submit: this.translatePipe.transform("Remove"),
                cancel: this.translatePipe.transform("Cancel"),
            },
        }).pipe(
            take(1),
            filter((result: boolean) => result),
        ).subscribe(() => {
            this.removeTopic(topicId);
        });
    }

    private removeTopic(topicId: string): ng.IPromise<boolean> {
        return this.purposeActionService.removeTopic(topicId);
    }
}
