export interface IUpdateTopicModalResolve {
    purposeId: string;
    purposeVersion: number;
    topicId: string;
    integrationKey: string;
}
