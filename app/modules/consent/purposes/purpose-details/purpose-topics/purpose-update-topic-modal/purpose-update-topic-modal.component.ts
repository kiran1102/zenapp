// 3rd Party
import { Component } from "@angular/core";

// Interfaces
import { IUpdateTopicModalResolve } from "./update-topic-modal-resolve.interface";

// Rxjs
import { Subject } from "rxjs";

// Services
import { ConsentPurposeService } from "consentModule/shared/services/cr-purpose.service";

@Component({
    selector: "cr-purpose-update-topic-modal",
    templateUrl: "./purpose-update-topic-modal.component.html",
})
export class CrPurposeUpdateTopicModalComponent {
    integrationKey: string;
    updateInProgress: boolean;

    otModalData: IUpdateTopicModalResolve;
    otModalCloseEvent: Subject<string>;

    constructor(
        private readonly consentPurposeService: ConsentPurposeService,
    ) { }

    ngOnInit() {
        this.integrationKey = this.otModalData.integrationKey;
    }

    closeModal() {
        this.otModalCloseEvent.next();
    }

    handleChanged(key: string) {
        this.integrationKey = key;
    }

    updateTopic() {
        this.updateInProgress = true;
        this.consentPurposeService.updateTopic(this.otModalData.purposeId, this.otModalData.purposeVersion, this.otModalData.topicId, this.integrationKey)
            .then((result: boolean) => {
                if (result) {
                    this.otModalCloseEvent.next(this.integrationKey);
                }
                this.updateInProgress = false;
            });
    }
}
