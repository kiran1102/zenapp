// Angular
import {
    Component,
    OnInit,
} from "@angular/core";

// Interfaces
import { IGetAllLanguageResponse } from "interfaces/dsar/dsar-edit-language.interface";

// Rxjs
import { Subject } from "rxjs";

// Services
import { ConsentPurposeService } from "consentModule/shared/services/cr-purpose.service";
import { LookupService } from "sharedModules/services/helper/lookup.service.ts";

@Component({
    selector: "legacy-purpose-language-select",
    templateUrl: "./legacy-purpose-language-selection-modal.component.html",
})
export class LegacyPurposeLanguageSelectionModalComponent implements OnInit {

    allLanguages: IGetAllLanguageResponse[];
    languageOptions: IGetAllLanguageResponse[];
    selectedLanguage: IGetAllLanguageResponse;
    loading: boolean;

    otModalCloseEvent: Subject<string>;

    constructor(
        private consentPurposeService: ConsentPurposeService,
        private lookupService: LookupService,
    ) {}

    ngOnInit(): void {
        this.consentPurposeService.getAllLanguages()
            .then((languages: IGetAllLanguageResponse[]) => {
                this.allLanguages = languages;
                this.languageOptions = languages;
            }).finally(() => {
                this.loading = false;
            });
    }

    closeModal(): void {
        this.otModalCloseEvent.next();
    }

    onLanguageInputChange({ key, value }): void {
        if (key === "Enter") {
            this.selectLanguage({ currentValue: this.languageOptions[0] });
            return;
        }
        this.languageOptions = this.lookupService.filterOptionsByInput([], this.allLanguages, value, "Code", "Name");
    }

    selectLanguage({ currentValue }): void {
        this.selectedLanguage = currentValue;
        this.languageOptions = this.allLanguages;
    }

    savePurpose() {
        this.otModalCloseEvent.next(this.selectedLanguage.Code);
    }
}
