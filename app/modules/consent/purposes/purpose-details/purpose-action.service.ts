import { Injectable, Inject } from "@angular/core";
import { IPromise } from "angular";
import {
    isEmpty,
    remove,
} from "lodash";

// Redux
import {
    PurposeActions,
    getPurposeDraft,
} from "oneRedux/reducers/purpose.reducer";
import { StoreToken } from "tokens/redux-store.token";

// Interfaces
import { IStore } from "interfaces/redux.interface";

// Services
import { ConsentPurposeService } from "consentModule/shared/services/cr-purpose.service";

// Models
import { PurposeDetails } from "crmodel/purpose-details";
import { PurposeLanguage } from "crmodel/purpose-list";
import { TopicListItem } from "crmodel/topic-list";

@Injectable()
export class PurposeActionService {

    constructor(
        @Inject(StoreToken) private store: IStore,
        private readonly ConsentPurpose: ConsentPurposeService,
    ) { }

    onPurposeModelChange(payload: string | PurposeLanguage[] | TopicListItem[], prop: keyof PurposeDetails) {
        const purpose = getPurposeDraft(this.store.getState());
        purpose[prop] = payload;
        this.setPurposeValidStatus(this.validatePurposeDetails(purpose));
        this.updatePurposeDraft(purpose);
    }

    updatePurposeDraft(updatedPurposeDraft: PurposeDetails, isModified: boolean = true): void {
        this.store.dispatch({
            type: PurposeActions.UPDATE_PURPOSE_DRAFT,
            updatedPurposeDraft,
            isModified,
        });
    }

    fetchPurpose(id: string, version: number): Promise<boolean> {
        this.store.dispatch({ type: PurposeActions.FETCHING_PURPOSE });
        const fetchPromise = this.ConsentPurpose.getPurposeDetails(id, version)
            .then((purpose: PurposeDetails): boolean => {
                this.store.dispatch({ type: PurposeActions.UPDATE_PURPOSE, updatedPurpose: { ...purpose } });
                this.updatePurposeDraft(purpose, false);
                this.store.dispatch({ type: PurposeActions.FETCHED_PURPOSE });
                this.setPurposeValidStatus(this.validatePurposeDetails(purpose));
                return Boolean(purpose);
            });
        return Promise.resolve(fetchPromise);
    }

    savePurpose() {
        const purpose = getPurposeDraft(this.store.getState());
        this.ConsentPurpose.updatePurposeTranslations(purpose)
            .then((result: boolean): void => {
                if (result) {
                    this.store.dispatch({ type: PurposeActions.SAVING_PURPOSE });
                    this.ConsentPurpose.savePurpose(purpose)
                        .then((saveResult: boolean): boolean => {
                            if (saveResult) {
                                this.updatePurposeDraft(purpose, false);
                                this.store.dispatch({ type: PurposeActions.SAVED_PURPOSE });
                            } else {
                                this.store.dispatch({ type: PurposeActions.PURPOSE_ERROR });
                            }
                            return saveResult;
                        });
                } else {
                    this.store.dispatch({ type: PurposeActions.PURPOSE_ERROR });
                    return;
                }
            });
    }

    publishPurpose(): ng.IPromise<boolean> {
        const purpose = getPurposeDraft(this.store.getState());
        return this.ConsentPurpose.updatePurposeTranslations(purpose)
            .then((result: boolean): ng.IPromise<boolean> => {
                if (result) {
                    this.store.dispatch({ type: PurposeActions.SAVING_PURPOSE });
                    return this.ConsentPurpose.publishPurpose(purpose)
                        .then((publishResult: boolean): boolean => {
                            if (publishResult) {
                                this.store.dispatch({ type: PurposeActions.SAVED_PURPOSE });
                            } else {
                                this.store.dispatch({ type: PurposeActions.PURPOSE_ERROR });
                                return;
                            }
                            return publishResult;
                        });
                } else {
                    this.store.dispatch({ type: PurposeActions.PURPOSE_ERROR });
                    return;
                }
            });
    }

    removeTopic(topicId: string): IPromise<boolean> {
        const purpose = getPurposeDraft(this.store.getState());
        return this.ConsentPurpose.removeTopic(purpose.Id, purpose.Version, topicId).then((success: boolean): boolean => {
            if (success) {
                remove(purpose.Topics, (topic: TopicListItem): boolean => topic.Id === topicId);
                this.updatePurposeDraft(purpose);
            }
            return success;
        });
    }

    private validatePurposeDetails(purpose: PurposeDetails, allowNoDefaultLanguage = false) {
        if (purpose && purpose.Label && purpose.Description) {
            return purpose.Label.trim()
                && purpose.Description.trim()
                && this.validatePurposeTranslations(purpose.Languages, allowNoDefaultLanguage);
        } else {
            return false;
        }
    }

    private validatePurposeTranslations(translations: PurposeLanguage[], allowNoDefaultLanguage: boolean): boolean {
        return translations.every((translation: PurposeLanguage) => this.validateTranslation(translation, allowNoDefaultLanguage));
    }

    private validateTranslation(translation: PurposeLanguage, allowNoDefaultLanguage: boolean): boolean {
        return !isEmpty(translation.Name.trim()) && !isEmpty(translation.Description.trim()) &&
            (allowNoDefaultLanguage || !isEmpty(translation.Language));
    }

    private setPurposeValidStatus(isValid: boolean) {
        if (isValid) {
            this.store.dispatch({ type: PurposeActions.PURPOSE_VALID });
        } else {
            this.store.dispatch({ type: PurposeActions.PURPOSE_INVALID });
        }
    }
}
