// Angular
import { NgModule } from "@angular/core";

// Components
import { CrPurposeDetailsComponent } from "consentModule/purposes/purpose-details/purpose-details.component";
import { CrPurposeFormComponent } from "consentModule/purposes/purpose-details/purpose-form/purpose-form.component";
import { CrPurposeHeaderComponent } from "consentModule/purposes/purpose-details/purpose-header/purpose-header.component";
import { CrPurposeLanguagesComponent } from "consentModule/purposes/purpose-details/purpose-languages/purpose-languages.component";
import { CrPurposeReportComponent } from "consentModule/purposes/purpose-details/purpose-report/purpose-report.component";
import { CrPurposeTopicsComponent } from "consentModule/purposes/purpose-details/purpose-topics/purpose-topics.component";
import { CrPurposeSelectTopicsModalComponent } from "consentModule/purposes/purpose-details/purpose-topics/purpose-select-topics-modal/purpose-select-topics-modal.component";
import { CrPurposeUpdateTopicModalComponent } from "consentModule/purposes/purpose-details/purpose-topics/purpose-update-topic-modal/purpose-update-topic-modal.component";
import { LegacyPurposeLanguageSelectionModalComponent } from "consentModule/purposes/purpose-details/legacy-purpose-language-selection-modal/legacy-purpose-language-selection-modal.component";
import { CrAddCustomPreferenceModalComponent } from "consentModule/purposes/purpose-details/purpose-custom-preferences/add-custom-preference-modal/add-custom-preference-modal.component";

// Modules
import { PurposesSharedModule } from "consentModule/purposes/shared/purposes-shared.module";

// Services
import { PurposeActionService } from "./purpose-action.service";
import { CrPurposeCustomPreferencesComponent } from "consentModule/purposes/purpose-details/purpose-custom-preferences/purpose-custom-preferences.component";
import { PurposeCustomPreferencesListComponent } from "consentModule/purposes/purpose-details/purpose-custom-preferences/purpose-custom-preferences-list/purpose-custom-preferences-list.component";

@NgModule({
    imports: [
        PurposesSharedModule,
    ],
    declarations: [
        CrPurposeDetailsComponent,
        CrPurposeFormComponent,
        CrPurposeHeaderComponent,
        CrPurposeLanguagesComponent,
        CrPurposeReportComponent,
        CrPurposeTopicsComponent,
        CrPurposeSelectTopicsModalComponent,
        CrPurposeUpdateTopicModalComponent,
        LegacyPurposeLanguageSelectionModalComponent,
        CrAddCustomPreferenceModalComponent,
        CrPurposeCustomPreferencesComponent,
        PurposeCustomPreferencesListComponent,
    ],
    entryComponents: [
        CrPurposeDetailsComponent,
        CrPurposeSelectTopicsModalComponent,
        CrPurposeUpdateTopicModalComponent,
        LegacyPurposeLanguageSelectionModalComponent,
        CrAddCustomPreferenceModalComponent,
    ],
    providers: [
        PurposeActionService,
    ],
})
export class PurposeDetailsModule { }
