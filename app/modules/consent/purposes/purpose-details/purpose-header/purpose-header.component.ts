// Third Party
import {
    Inject,
    Component,
    Input,
} from "@angular/core";

// Redux
import { StoreToken } from "tokens/redux-store.token";
import { IStore } from "interfaces/redux.interface";

// Services
import { ConsentPurposeService } from "consentModule/shared/services/cr-purpose.service";

// Components and Constants
import { CrPurposeBaseComponent } from "../purpose-base/purpose-base.component";
import crConstants from "consentModule/shared/cr-constants";

@Component({
    selector: "cr-purpose-header",
    templateUrl: "./purpose-header.component.html",
})
export class CrPurposeHeaderComponent extends CrPurposeBaseComponent {
    @Input() title: string;
    crConstants = crConstants;

    constructor(
        @Inject(StoreToken) private store: IStore,
        private consentPurposeService: ConsentPurposeService,
    ) {
        super(store, consentPurposeService);
    }
}
