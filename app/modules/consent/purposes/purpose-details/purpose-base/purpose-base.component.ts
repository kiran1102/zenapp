// Redux
import { ReduxBaseComponent } from "sharedModules/components/redux-base/redux-base.component";
import {
    IStore,
    IStoreState,
} from "interfaces/redux.interface";
import {
    getPurposeDraft,
    getPurposeLoadingStatus,
} from "oneRedux/reducers/purpose.reducer";

// Constants
import crConstants from "consentModule/shared/cr-constants";

// Models
import { PurposeDetails } from "crmodel/purpose-details";

// Services
import { ConsentPurposeService } from "consentModule/shared/services/cr-purpose.service";
import { IConsentLength } from "consentModule/shared/services/consent-base.service";

export abstract class CrPurposeBaseComponent extends ReduxBaseComponent {
    crConstants = crConstants;
    purpose: PurposeDetails;
    consentLength: IConsentLength;
    isLoading: boolean;
    isSaving: boolean;

    constructor(
        private commonStore: IStore,
        private readonly baseConsentPurposeService: ConsentPurposeService,
    ) {
        super(commonStore);
    }

    onModelChange(payload: string, prop: keyof PurposeDetails): void {
        this.purpose[prop] = payload;
    }

    protected onStateUpdate(state: IStoreState): void {
        this.purpose = getPurposeDraft(state);
        this.isLoading = getPurposeLoadingStatus(state);
    }
}
