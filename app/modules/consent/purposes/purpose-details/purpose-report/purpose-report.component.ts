import {
    Component,
    Inject,
    OnInit,
} from "@angular/core";
import { StateService } from "@uirouter/core";

// Components
import { CrPurposeBaseComponent } from "../purpose-base/purpose-base.component";

// Constants
import crConstants from "consentModule/shared/cr-constants";

// Interfaces
import { IHorseshoeGraphData } from "interfaces/one-horseshoe-graph.interface";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

// Redux
import { StoreToken } from "tokens/redux-store.token";
import { IStore } from "interfaces/redux.interface";

// Services
import { ConsentPurposeService } from "consentModule/shared/services/cr-purpose.service";
import { PurposeActionService } from "consentModule/purposes/purpose-details/purpose-action.service";

@Component({
    selector: "cr-purpose-report",
    templateUrl: "./purpose-report.component.html",
})
export class CrPurposeReportComponent extends CrPurposeBaseComponent {
    purposeId: string;
    dataSubjectTotal: number;
    dataSubjectCounters: IHorseshoeGraphData;

    constructor(
        private stateService: StateService,
        @Inject(StoreToken) private readonly store: IStore,
        private readonly consentPurposeService: ConsentPurposeService,
        readonly purposeActionService: PurposeActionService,
        private readonly translatePipe: TranslatePipe,
    ) {
        super(store, consentPurposeService);
    }

    ngOnInit() {
        super.ngOnInit();
        this.purposeId = this.stateService.params.Id;
        this.dataSubjectTotal = this.getTotalDataSubjects();
        this.generateCounters();
    }

    getTotalDataSubjects(): number {
        return !this.purpose ? 0 : (
            this.purpose.ActiveDataSubjects
            + this.purpose.ExpiredDataSubjects
            + this.purpose.NoConsentDataSubjects
            + this.purpose.PendingDataSubjects
            + this.purpose.WithdrawnDataSubjects
            + this.purpose.OptedOutDataSubjects
        );
    }

    goToDataSubjects() {
        this.stateService.go("zen.app.pia.module.consentreceipt.views.datasubjects.list", { purposeId: this.purposeId });
    }

    goToCollectionPoints() {
        this.stateService.go("zen.app.pia.module.consentreceipt.views.collections.list", { purposeId: this.purpose.Id, purposeVersion: this.purpose.Version });
    }

    generateCounters() {
        if (this.dataSubjectTotal === 0) {
            this.dataSubjectCounters = {
                series: [
                    {
                        graphClass: "empty",
                        label: "",
                        value: 1,
                    },
                ],
            };
            return;
        }

        const getCounterValue = (count: number): number => {
            if (count <= 0) {
                return 0;
            }
            return count / this.dataSubjectTotal;
        };

        this.dataSubjectCounters = {
            series: [
                {
                    graphClass: "stroke-green",
                    label: "ActiveDataSubjects",
                    value: getCounterValue(this.purpose.ActiveDataSubjects),
                },
                {
                    graphClass: "stroke-lagoon",
                    label: "ExpiredDataSubjects",
                    value: getCounterValue(this.purpose.ExpiredDataSubjects),
                },
                {
                    graphClass: "stroke-frost",
                    label: "NoConsentDataSubjects",
                    value: getCounterValue(this.purpose.NoConsentDataSubjects),
                },
                {
                    graphClass: "stroke-blue",
                    label: "PendingDataSubjects",
                    value: getCounterValue(this.purpose.PendingDataSubjects),
                },
                {
                    graphClass: "stroke-light-red",
                    label: "WithdrawnDataSubjects",
                    value: getCounterValue(this.purpose.WithdrawnDataSubjects),
                },
                {
                    graphClass: "stroke-orange",
                    label: "OptedOutDataSubjects",
                    value: getCounterValue(this.purpose.OptedOutDataSubjects),
                },
            ],
            legend: [
                {
                    label: this.translatePipe.transform("Active"),
                    class: "background-green",
                },
                {
                    label: this.translatePipe.transform("Expired"),
                    class: "background-lagoon",
                },
                {
                    label: this.translatePipe.transform("NoConsentGiven"),
                    class: "background-frost",
                }, {
                    label: this.translatePipe.transform("Pending"),
                    class: "background-blue",
                }, {
                    label: this.translatePipe.transform("Withdrawn"),
                    class: "background-light-red",
                }, {
                    label: this.translatePipe.transform("OptedOut"),
                    class: "background-orange",
                },
            ],
        };
    }

    get purposeTypeCookie(): boolean {
        return this.purpose.Type === crConstants.CRPurposeTypes.Cookie;
    }
}
