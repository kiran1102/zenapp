// Third Party
import {
    Inject,
    Component,
    OnInit,
} from "@angular/core";
import { isEmpty } from "lodash";

// Components
import { CrPurposeBaseComponent } from "../purpose-base/purpose-base.component";

// Interfaces
import { IStringMap } from "interfaces/generic.interface";

// Models
import { PurposeDetails } from "crmodel/purpose-details";
import { PurposeLanguage } from "crmodel/purpose-list";
import { ColumnDefinition } from "crmodel/column-definition";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

// Redux
import { StoreToken } from "tokens/redux-store.token";
import {
    IStore,
    IStoreState,
} from "interfaces/redux.interface";
import { getPurposeDraft } from "oneRedux/reducers/purpose.reducer";

// Services
import { ConsentPurposeService } from "consentModule/shared/services/cr-purpose.service";
import { Permissions } from "modules/shared/services/helper/permissions.service";
import { IEntityTranslation } from "interfaces/consent/entity-translation";
import { PurposeActionService } from "consentModule/purposes/purpose-details/purpose-action.service";

@Component({
    selector: "cr-purpose-languages",
    templateUrl: "./purpose-languages.component.html",
})
export class CrPurposeLanguagesComponent extends CrPurposeBaseComponent implements OnInit {
    canEditLanguages: boolean;
    isInitialized: boolean;
    columns: ColumnDefinition[];
    defaultLanguage: string;
    languageList: string[];
    previousVersion: PurposeDetails;
    previousTranslationMap: IStringMap<IEntityTranslation> = {};

    constructor(
        @Inject(StoreToken) private readonly store: IStore,
        private readonly permissions: Permissions,
        private readonly consentPurposeService: ConsentPurposeService,
        private readonly purposeActionService: PurposeActionService,
        private readonly translatePipe: TranslatePipe,
    ) {
        super(store, consentPurposeService);
    }

    ngOnInit() {
        super.ngOnInit();
        this.canEditLanguages = this.permissions.canShow("ConsentPurposeLanguageEdit");
        this.columns = [
            { columnType: "language", columnName: this.translatePipe.transform("Language"), cellValueKey: "Language", required: true },
            { columnType: "previous", columnName: this.translatePipe.transform("PreviousVersion") },
            { columnType: "current", columnName: this.translatePipe.transform("SelectedVersion") },
        ];
    }

    onTranslationChange(translations: IEntityTranslation[]) {
        const formattedTranslations = translations.map((translation: IEntityTranslation): PurposeLanguage => {
            return {
                Language: translation.Language,
                Name: translation.Name,
                Description: translation.Description,
                Default: translation.Default,
            };
        });
        this.purpose.Languages = formattedTranslations;
        this.purposeActionService.onPurposeModelChange(formattedTranslations, "Languages");
    }

    getPreviousVersion() {
        if (this.purpose.Version > 1) {
            this.consentPurposeService.getPurposeDetails(this.purpose.Id, this.purpose.Version - 1)
                .then((result: PurposeDetails): void => {
                    this.previousVersion = result;
                    this.getPreviousTranslationMap();
                });
        }
    }

    getLanguageList() {
        this.languageList = this.purpose.Languages.map((language: PurposeLanguage): string => {
            return language.Language;
        });
    }

    setDefaultLanguage() {
        const purposeLanguage = this.purpose.Languages.find((language: PurposeLanguage): boolean => language.Default);
        if (purposeLanguage) {
            this.defaultLanguage = purposeLanguage.Language;
        }
    }

    getPreviousTranslationMap() {
        if (this.previousVersion.Languages) {
            this.previousVersion.Languages.forEach((language: PurposeLanguage) => {
                this.previousTranslationMap[language.Language] = language;
            });
        }
    }

    validateTranslations(translations: IEntityTranslation[]): boolean {
        return translations.every((translation: IEntityTranslation) => this.validateTranslation(translation));
    }

    validateTranslation(translation: IEntityTranslation): boolean {
        return !isEmpty(translation.Name) && !isEmpty(translation.Description);
    }

    savePurpose() {
        this.purposeActionService.savePurpose();
    }

    protected onStateUpdate(state: IStoreState) {
        super.onStateUpdate(state);
        this.purpose = getPurposeDraft(state);
        if (!this.isInitialized) {
            this.getPreviousVersion();
            this.getLanguageList();
            this.setDefaultLanguage();
            this.isInitialized = true;
        }
    }
}
