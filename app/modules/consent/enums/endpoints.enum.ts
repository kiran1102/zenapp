export enum Endpoints {
    Purposes = "/api/consentmanager/v1/purposes",
    CollectionPoints = "/api/consentmanager/v1/collectionpoints",
    DataElements = "/api/consentmanager/v1/dataelements",
    Transactions = "/api/consentmanager/v1/transactions",
    DataSubjects = "/api/consentmanager/v1/datasubjects",
    PreferenceCenters = "/api/consentmanager/v1/preferencecenters",
}
