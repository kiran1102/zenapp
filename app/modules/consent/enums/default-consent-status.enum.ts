export enum DefaultConsentStatus {
    Active = "ACTIVE",
    OptOut = "OPT OUT",
}
