// Angular
import { NgModule } from "@angular/core";

// Modules
import { CollectionPointsSharedModule } from "consentModule/collection-points/shared/collection-points-shared.module";
import { CollectionPointCreateModule } from "./collection-point-create/collection-point-create.module";
import { CollectionPointDetailsModule } from "./collection-point-details/collection-point-details.module";
import { CollectionPointListModule } from "./collection-point-list/collection-point-list.module";
import { CollectionPointTypeModule } from "./collection-point-type/collection-point-type.module";
import { CollectionPointUpdatesModule } from "./collection-point-updates/collection-point-updates.module";

@NgModule({
    imports: [
        CollectionPointsSharedModule,
        CollectionPointCreateModule,
        CollectionPointDetailsModule,
        CollectionPointListModule,
        CollectionPointTypeModule,
        CollectionPointUpdatesModule,
    ],
})
export class CollectionPointsModule { }
