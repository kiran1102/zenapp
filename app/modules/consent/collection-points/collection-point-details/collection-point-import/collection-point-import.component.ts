// Third Party
import { Component } from "@angular/core";
import { StateService } from "@uirouter/core";

// Services
import { CollectionPointService } from "consentModule/shared/services/collection-point.service";

// Components
import { CrCollectionPointCommon } from "consentModule/collection-points/shared/collection-point-common.component";

@Component({
    selector: "cr-collection-point-import",
    templateUrl: "./collection-point-import.component.html",
})
export class CrCollectionPointImportComponent extends CrCollectionPointCommon {
    constructor(
        protected stateService: StateService,
        protected collectionPointService: CollectionPointService,
    ) {
        super(stateService, collectionPointService);
    }

    goToBulkImport() {
        this.stateService.go("zen.app.pia.module.admin.bulkimport.status");
    }
}
