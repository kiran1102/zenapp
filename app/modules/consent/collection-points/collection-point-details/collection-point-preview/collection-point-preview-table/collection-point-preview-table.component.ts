// Angular
import {
    Component,
    Input,
    Output,
    EventEmitter,
} from "@angular/core";

// Third Party
import { StateService } from "@uirouter/core";

// Services
import {
    IDropDownElement,
    IUpdateCustomPreference,
    IUpdatePurposeItem,
    IUpdateTopic,
} from "consentModule/shared/services/consent-base.service";
import { CollectionPointService } from "consentModule/shared/services/collection-point.service";

// Models
import { PreviewPurpose } from "crmodel/purpose-list";

// Components
import { CrCollectionPointCommon } from "consentModule/collection-points/shared/collection-point-common.component";

// Interfaces
import { IStringMap } from "interfaces/generic.interface";

@Component({
    selector: "cr-collection-point-preview-table",
    templateUrl: "./collection-point-preview-table.component.html",
})
export class CrCollectionPointPreviewTableComponent extends CrCollectionPointCommon {
    @Input() purposes: IDropDownElement[];
    @Output() onUpdate = new EventEmitter<IUpdatePurposeItem[]>();

    purposeStates: IStringMap<PreviewPurpose> = {};

    constructor(
        protected stateService: StateService,
        protected collectionPointService: CollectionPointService,
    ) {
        super(stateService, collectionPointService);
    }

    setTopic(purposeId: string, purposeVersion: number, topicId: string) {
        this.findOrCreatePurpose(purposeId, purposeVersion);

        this.purposeStates[purposeId].Topics[topicId] = !this.purposeStates[purposeId].Topics[topicId];
        if (this.purposeStates[purposeId].Topics[topicId]) {
            this.purposeStates[purposeId].IsChecked = true;
        } else {
            delete this.purposeStates[purposeId].Topics[topicId];
            if (Object.keys(this.purposeStates[purposeId].Topics).length === 0) {
                delete this.purposeStates[purposeId];
            }
        }
        this.updatePayload();
    }

    setCustomPreferenceOption(purposeId: string, purposeVersion: number, customPreferenceId: string, option: string) {
        const customPreference = this.findOrCreateCustomPreference(purposeId, purposeVersion, customPreferenceId);

        customPreference.Options[option] = !customPreference.Options[option];

        if (customPreference.Options[option]) {
            this.purposeStates[purposeId].IsChecked = true;
        } else {
            delete customPreference.Options[option];

            if (Object.keys(customPreference.Options).filter(Boolean).length === 0) {
                delete this.purposeStates[purposeId];
            }
        }

        this.updatePayload();
    }

    setPurpose(purposeId: string, purposeVersion: number) {
        this.findOrCreatePurpose(purposeId, purposeVersion);

        this.purposeStates[purposeId].IsChecked = !this.purposeStates[purposeId].IsChecked;
        if (!this.purposeStates[purposeId].IsChecked) {
            this.purposeStates[purposeId].Topics = {};
        }
        this.updatePayload();
    }

    updatePayload() {
        const purposePayload: IUpdatePurposeItem[] = [];

        Object.keys(this.purposeStates).forEach((purposeKey: string) => {
            const purposeValue = this.purposeStates[purposeKey];
            const topicList: IUpdateTopic[] = [];
            const customPreferencesList: IUpdateCustomPreference[] = [];

            const customPreferences = this.purposeStates[purposeKey].CustomPreferences;
            const topics = this.purposeStates[purposeKey].Topics;

            Object.keys(topics).forEach((topicKey: string) => {
                const topicValue = topics[topicKey];
                const topic: IUpdateTopic = { TopicId: "" };

                if (topicValue) {
                    topic.TopicId = topicKey;
                    topicList.push(topic);
                }
            });

            Object.keys(customPreferences).forEach((customPreferenceKey: string) => {
                const customPreferenceValue = customPreferences[customPreferenceKey];
                const options = customPreferenceValue.Options;
                const hasOptions = Boolean(
                    Object
                        .values(options)
                        .filter(Boolean).length,
                );

                if (hasOptions) {
                    const consentedOptions = Object
                        .keys(options)
                        .reduce((acc: string[], id: string) => {
                            return options[id] ? [...acc, id] : acc;
                        }, []);

                    customPreferencesList.push({
                        Id: customPreferenceKey,
                        Options: consentedOptions,
                    });
                }
            });

            if (purposeValue.IsChecked) {
                purposePayload.push({
                    Id: purposeKey,
                    Version: purposeValue.Version,
                    Preferences: topicList.length ? topicList : undefined,
                    CustomPreferences: customPreferencesList.length ? customPreferencesList : undefined,
                });
            }
        });

        this.onUpdate.emit(purposePayload);
    }

    isPurposeChecked(purposeId: string): boolean {
        if (this.purposeStates[purposeId]) {
            return (this.purposeStates[purposeId].IsChecked || Object.keys(this.purposeStates[purposeId].Topics).length > 0);
        } else {
            return false;
        }
    }

    isCustomPreferenceOptionChecked(purposeId: string, customPreferenceId: string, optionId: string) {
        const purpose = this.purposeStates[purposeId];
        const customPreference = purpose && purpose.CustomPreferences[customPreferenceId];
        return customPreference && customPreference.Options[optionId];
    }

    isTopicChecked(purposeId: string, topicId: string): boolean {
        return this.purposeStates[purposeId] && this.purposeStates[purposeId].Topics[topicId];
    }

    private findOrCreatePurpose(purposeId: string, purposeVersion: number): PreviewPurpose {
        if (!this.purposeStates[purposeId]) {
            this.purposeStates[purposeId] = {
                IsChecked: false,
                CustomPreferences: {},
                Topics: {},
                Version: purposeVersion,
            };
        }

        return this.purposeStates[purposeId];
    }

    private findOrCreateCustomPreference(purposeId: string, purposeVersion: number, customPreference: string) {
        const purpose = this.findOrCreatePurpose(purposeId, purposeVersion);

        if (!purpose.CustomPreferences[customPreference]) {
            purpose.CustomPreferences[customPreference] = {
                Options: {},
            };
        }

        return purpose.CustomPreferences[customPreference];
    }
}
