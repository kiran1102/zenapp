// Third Party
import {
    Component,
    Input,
    OnDestroy,
    OnInit,
} from "@angular/core";
import { StateService } from "@uirouter/core";
import { map } from "lodash";
import { Base64 } from "js-base64";

// Services
import { CollectionPointService } from "consentModule/shared/services/collection-point.service";
import { ConsentSettingsService } from "consentModule/shared/services/cr-settings.service";
import {
    IUpdatePurposeItem,
    IUpdateTopic,
} from "consentModule/shared/services/consent-base.service";
import { Permissions } from "modules/shared/services/helper/permissions.service";

// Models
import { CollectionPointDetails } from "crmodel/collection-point-details";
import { SdkSettings } from "crmodel/sdk-settings";
import { EntityItem } from "crmodel/entity-item";
import { TopicListItem } from "crmodel/topic-list";

// Components
import { CrCollectionPointCommon } from "consentModule/collection-points/shared/collection-point-common.component";

// Constants
import crConstants from "consentModule/shared/cr-constants";
import { IConsentSettings } from "interfaces/consent-settings.interface";

declare var OneTrust: {
    Receipt: {
        TriggerReceiptAction(downloadRawReceipt?: boolean, payload?: IUpdatePurposeItem[]): Promise<string | null>;
    },
};

@Component({
    selector: "cr-collection-point-preview",
    templateUrl: "./collection-point-preview.component.html",
})
export class CrCollectionPointPreviewComponent extends CrCollectionPointCommon implements OnInit, OnDestroy {
    @Input() collectionPoint: CollectionPointDetails;
    @Input() elements: EntityItem[];

    crConstants = crConstants;
    sdkSettings: SdkSettings;
    collectionPointToken: string;
    testTransactionTriggered: boolean;
    testTransactionId: string;
    testIdentifier: string;
    updateInProgress: boolean;
    payload: IUpdatePurposeItem[];
    showNotice = this.permissions.canShow("ConsentReceiptPreviewNotice");
    noConsentToggle = this.permissions.canShow("ConsentSettingNoConsentTransaction");
    consentSettings: IConsentSettings;

    triggerId = "trigger";
    identifierId = "identifier";

    constructor(
        protected stateService: StateService,
        protected collectionPointService: CollectionPointService,
        private consentSettingsService: ConsentSettingsService,
        private permissions: Permissions,
    ) {
        super(stateService, collectionPointService);
    }

    ngOnInit() {
        this.updateInProgress = true;

        const settingsPromise = this.collectionPointService.getCollectionPointSettings(this.collectionPointId, this.collectionPointVersion)
            .then((settings: SdkSettings) => { this.sdkSettings = settings; });
        const tokenPromise = this.collectionPointService.getCollectionPointToken(this.collectionPointId, this.collectionPointVersion)
            .then((token: string) => { this.collectionPointToken = token; });
        const tenantSettingsPromise = this.consentSettingsService.getConsent().then((settings: IConsentSettings) => {
            this.consentSettings = settings;
        });

        Promise.all([settingsPromise, tokenPromise, tenantSettingsPromise]).then(() => {
            this.insertConsentScript();
            this.updateInProgress = false;
        });
        this.payload = this.purposesToPayload(null);
    }

    ngOnDestroy() {
        this.removeConsentScript();
    }

    updatePayload(purposes: IUpdatePurposeItem[]) {
        this.payload = this.purposesToPayload(purposes);
    }

    goBackClicked() {
        this.testTransactionId = null;
        this.testTransactionTriggered = false;
        this.payload = [];
    }

    get encodedSettings(): string {
        if (!this.sdkSettings || !this.sdkSettings.integrationSettings) {
            return "";
        }
        const settingsJson: string = JSON.stringify(this.sdkSettings.integrationSettings);
        return Base64.encode(settingsJson);
    }

    get previewLabel(): string {
        return this.collectionPoint.SubjectIdentifier;
    }

    get collectionPointConsentType(): string {
        return this.collectionPoint ? this.translateConst(this.collectionPoint.ConsentType, crConstants.CRConsentTypes) : "";
    }

    onIdentifierChange(identifier: string) {
        this.testIdentifier = identifier;
    }

    previewTriggerClicked() {
        if (this.canSendConsent) {
            this.createPreviewTransaction();
        } else {
            this.testTransactionId = null;
            this.testTransactionTriggered = true;
        }
    }

    goToTransactionList() {
        this.stateService.go("zen.app.pia.module.consentreceipt.views.transactions.list", { receiptId: this.testTransactionId });
    }

    private insertConsentScript() {
        this.sdkSettings.integrationSettings.testMode = true;
        this.sdkSettings.integrationSettings.manualTrigger = true;
        const conditionalId: string = this.isExplicitConsent ? crConstants.CRConditionalTriggerId : "";
        const scriptTag: HTMLScriptElement = document.createElement("script");
        scriptTag.charset = "utf-8";
        scriptTag.src = this.sdkSettings.scriptSourceLocation;
        scriptTag.id = crConstants.CRConsentScriptId;
        scriptTag.innerText = `
            triggerId="${this.triggerId}";
            identifierId="${this.identifierId}";
            conditionalConsentTriggerId="${conditionalId}";
            confirmationId="confirmation";
            token="${this.collectionPointToken}";
            settings="${this.encodedSettings}";
        `;
        document.getElementsByTagName("head")[0].appendChild(scriptTag);
    }

    private removeConsentScript() {
        const scriptElement = document.getElementById(crConstants.CRConsentScriptId);
        if (!scriptElement) {
            return;
        }
        scriptElement.remove();
    }

    private get canSendConsent(): boolean {
        return (this.payload && this.payload.length > 0) ||
            (this.noConsentToggle && this.consentSettings && this.consentSettings.noConsentTransactions
                && this.collectionPoint && this.collectionPoint.NoConsentTransactions);
    }

    private createPreviewTransaction() {
        this.updateInProgress = true;
        OneTrust.Receipt.TriggerReceiptAction(false, this.payload).then((transactionId: string) => {
            this.testTransactionId = transactionId;
            this.testTransactionTriggered = true;
            this.updateInProgress = false;
        });
    }

    private purposesToPayload(purposes: IUpdatePurposeItem[]): IUpdatePurposeItem[] {
        if (!this.isExplicitConsent && this.collectionPoint.Purposes && this.collectionPoint.Purposes.length === 1) {
            const TopicIds: IUpdateTopic[] = map(this.collectionPoint.Purposes[0].Topics, (topic: TopicListItem): IUpdateTopic => {
                return { TopicId: topic.Id };
            });

            return [{
                Id: this.collectionPoint.Purposes[0].Id,
                Version: this.collectionPoint.Purposes[0].Version,
                Preferences: TopicIds,
            }];
        }

        return purposes || [];
    }
}
