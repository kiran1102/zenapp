// Angular
import {
    Component,
    EventEmitter,
    Input,
    Output,
} from "@angular/core";
import { StateService } from "@uirouter/core";

// Third Party
import { sortBy } from "lodash";

// Components
import { CrCollectionPointCommon } from "consentModule/collection-points/shared/collection-point-common.component";

// Interfaces
import { IConsentSettings } from "interfaces/consent-settings.interface";

// Models
import { CollectionPointDetails } from "crmodel/collection-point-details";
import { CollectionPointSettingsPayload } from "crmodel/collection-point-details";
import { UpdateCollectionPoint } from "crmodel/create-collection-point";
import { PreferenceCenter } from "crmodel/preference-center-list";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

// Services
import { Permissions } from "modules/shared/services/helper/permissions.service";
import { CollectionPointService } from "consentModule/shared/services/collection-point.service";
import { ConsentPreferenceService } from "consentModule/shared/services/cr-preference.service";
import { ConsentSettingsService } from "consentModule/shared/services/cr-settings.service";
import { LookupService } from "sharedModules/services/helper/lookup.service.ts";

@Component({
    selector: "cr-collection-point-settings",
    templateUrl: "./collection-point-settings.component.html",
})
export class CrCollectionPointSettingsComponent extends CrCollectionPointCommon {
    @Input() collectionPoint: CollectionPointDetails;
    @Output() onRedirectUrlChange = new EventEmitter<CollectionPointSettingsPayload>();
    @Output() onPreferenceCenterChange = new EventEmitter<PreferenceCenter>();
    @Output() onUpdate = new EventEmitter<boolean>();

    externalSettingsToggle = this.permissions.canShow("ConsentSDKExternalSettings");
    showNoConsentTransactionToggle = this.permissions.canShow("ConsentSettingNoConsentTransaction");
    doubleOptInEnabled = this.permissions.canShow("ConsentDoubleOptIn");
    consentAcknowledgementEmailEnabled = this.permissions.canShow("ConsentAcknowledgementEmail");

    consentSettings: IConsentSettings;
    preferenceCenterList: PreferenceCenter[];
    preferenceCenterOptions: PreferenceCenter[];
    selectedPreferenceCenter: PreferenceCenter;
    settingsPayload: CollectionPointSettingsPayload;
    loading: boolean;
    updateInProgress: boolean;
    redirectUrlValid: boolean;
    showUpdateSdkWarning = false;

    constructor(
        protected stateService: StateService,
        protected collectionPointService: CollectionPointService,
        private consentPreferenceService: ConsentPreferenceService,
        private consentSettingsService: ConsentSettingsService,
        private permissions: Permissions,
        private lookupService: LookupService,
        private translatePipe: TranslatePipe,
    ) {
        super(stateService, collectionPointService);
    }

    ngOnInit() {
        this.loading = true;
        this.redirectUrlValid = this.getRedirectUrlValid();
        Promise.all([this.getConsentSettings(), this.getPreferenceCenters()]).then(() => { this.loading = false; });
    }

    getConsentSettings(): Promise<void> {
        return Promise.resolve(this.consentSettingsService.getConsent().then((settings: IConsentSettings) => {
            this.consentSettings = settings;
        }));
    }

    getPreferenceCenters(): Promise<void> {
        return Promise.resolve(this.consentPreferenceService.getAllPreferenceCenters().then((response: PreferenceCenter[]) => {
            this.preferenceCenterList = sortBy(response, (preferenceCenter: PreferenceCenter): string => {
                return preferenceCenter.Name.toLowerCase();
            });
            this.preferenceCenterOptions = this.preferenceCenterList;
            if (this.collectionPoint && this.collectionPoint.PreferenceCenter) {
                this.selectedPreferenceCenter = this.preferenceCenterList.find((preferenceCenter: PreferenceCenter): boolean => {
                    return preferenceCenter.Id === this.collectionPoint.PreferenceCenter.Id;
                });
            }
        }));
    }

    toggleDoubleOptIn() {
        this.collectionPoint.DoubleOptIn = !this.collectionPoint.DoubleOptIn;
        if (this.collectionPoint.DoubleOptIn) {
            this.collectionPoint.SubjectIdentifier = this.translatePipe.transform("EmailAddress");
        } else {
            this.collectionPoint.RedirectUrl = "";
        }
        this.updateCollectionPoint();
    }

    handleRedirectUrlChange(payload: string, prop: keyof CollectionPointDetails) {
        this.handleChanged(payload, prop);
        this.redirectUrlValid = this.getRedirectUrlValid();
        this.onRedirectUrlChange.emit({ redirectUrl: payload, redirectUrlValid: this.redirectUrlValid });
    }

    onInputChange({ key, value }) {
        if (key === "Enter") {
            this.selectOption({ currentValue: this.preferenceCenterOptions[0] });
            return;
        }
        this.preferenceCenterOptions = this.lookupService.filterOptionsByInput([], this.preferenceCenterList, value, "Id", "Name");
    }

    selectOption({ currentValue }) {
        this.selectedPreferenceCenter = currentValue;
        this.onPreferenceCenterChange.emit(currentValue);
        this.preferenceCenterOptions = this.preferenceCenterList;
    }

    toggleNoConsentTransactions() {
        this.showUpdateSdkWarning = true;
        this.collectionPoint.NoConsentTransactions = !this.collectionPoint.NoConsentTransactions;
        this.updateCollectionPoint();
    }

    toggleExternalSdkSettings() {
        this.collectionPoint.HostedSDK = !this.collectionPoint.HostedSDK;
        this.updateCollectionPoint();
    }

    toggleConsentAcknowledgementEmail() {
        this.collectionPoint.SendConsentEmail = !this.collectionPoint.SendConsentEmail;
        if (this.collectionPoint.SendConsentEmail) {
            this.collectionPoint.SubjectIdentifier = this.translatePipe.transform("EmailAddress");
        } else {
            this.selectedPreferenceCenter = undefined;
        }
        this.updateCollectionPoint();
    }

    getRedirectUrlValid(): boolean {
        return this.collectionPoint && (!this.collectionPoint.RedirectUrl || this.collectionPointService.validateLink(this.collectionPoint.RedirectUrl));
    }

    get consentAcknowledgementEmailDisabled(): boolean {
        if (this.collectionPoint.Status === this.crConstants.CRCollectionPointStatus.Retired) {
            return true;
        } else if (this.collectionPoint.Status === this.crConstants.CRCollectionPointStatus.Draft) {
            return false;
        } else {
            return !(this.collectionPoint.SubjectIdentifier === this.translatePipe.transform("Email")
                || this.collectionPoint.SubjectIdentifier === this.translatePipe.transform("EmailAddress"));
        }
    }

    private updateCollectionPoint(showToaster: boolean = true) {
        this.updateInProgress = true;
        this.onUpdate.emit(this.updateInProgress);
        if (this.collectionPoint.Purposes) {
            this.collectionPoint.PurposeId = this.collectionPointService.idsToString(this.collectionPoint.Purposes);
        }
        const updateModel = UpdateCollectionPoint.fromDetails(this.collectionPoint);
        this.collectionPointService.updateCollectionPoint(this.collectionPoint.Id, this.collectionPoint.Version, updateModel, showToaster)
            .then(() => {
                this.updateInProgress = false;
                this.onUpdate.emit(this.updateInProgress);
            });
    }
}
