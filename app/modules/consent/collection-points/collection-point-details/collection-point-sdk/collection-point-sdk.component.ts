// Third Party
import {
    Component,
    ViewChild,
    ElementRef,
} from "@angular/core";
import { StateService } from "@uirouter/core";

// Interfaces
import { IConsentSettings } from "interfaces/consent-settings.interface";

// Models
import { SdkSettings } from "crmodel/sdk-settings";
import { UpdateCollectionPoint } from "crmodel/create-collection-point";

// Components
import { CrCollectionPointCommon } from "consentModule/collection-points/shared/collection-point-common.component";

// Services
import { Permissions } from "modules/shared/services/helper/permissions.service";
import { ToastService } from "@onetrust/vitreus";
import { ConsentSettingsService } from "consentModule/shared/services/cr-settings.service";
import { CollectionPointService } from "consentModule/shared/services/collection-point.service";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

// Utilities
import Utilities from "Utilities";

@Component({
    selector: "cr-collection-point-sdk",
    templateUrl: "./collection-point-sdk.component.html",
})
export class CrCollectionPointSdkComponent extends CrCollectionPointCommon {
    showDecodedSettings = this.permissions.canShow("ConsentSDKShowDecodedSettings");
    showNoConsentTransactionToggle = this.permissions.canShow("ConsentSettingNoConsentTransaction");
    externalSettingsToggle = this.permissions.canShow("ConsentSDKExternalSettings");
    sdkSettings: SdkSettings;
    collectionPointToken: string;
    updateInProgress = false;
    showUpdateSdkWarning = false;
    consentSettings: IConsentSettings;
    @ViewChild("sdkCode") sdkCodeElement: ElementRef;

    constructor(
        protected stateService: StateService,
        protected collectionPointService: CollectionPointService,
        private consentSettingsService: ConsentSettingsService,
        private permissions: Permissions,
        private toastService: ToastService,
        private translatePipe: TranslatePipe,
    ) {
        super(stateService, collectionPointService);
    }

    ngOnInit() {
        this.collectionPointService.getCollectionPointSettings(this.collectionPointId, this.collectionPointVersion)
            .then((settings: SdkSettings) => { this.sdkSettings = settings ? settings : new SdkSettings(); });
        this.collectionPointService.getCollectionPointToken(this.collectionPointId, this.collectionPointVersion)
            .then((token: string) => { this.collectionPointToken = token; });
        this.consentSettingsService.getConsent().then((settings: IConsentSettings) => {
            this.consentSettings = settings;
        });
    }

    get encodedSettings(): string {
        if (!this.sdkSettings || !this.sdkSettings.integrationSettings) {
            return "";
        }
        const settingsJson = JSON.stringify(this.isSinglePurposeFormSubmit || this.showDecodedSettings ?
            Object.assign({}, this.sdkSettings.integrationSettings, { preferences: undefined }) :
            this.sdkSettings.integrationSettings);
        return window.btoa(settingsJson);
    }

    get settingsUrl() {
        return this.sdkSettings ? this.sdkSettings.scriptSettingsLocation : "";
    }

    get isLoading(): boolean {
        return !(this.collectionPoint && this.sdkSettings && this.collectionPointToken && this.consentSettings);
    }

    get isNoConsent(): boolean {
        return this.showNoConsentTransactionToggle && this.consentSettings && this.collectionPoint
            && this.consentSettings.noConsentTransactions && this.collectionPoint.NoConsentTransactions;
    }

    trackByFn(index: number): number {
        return index;
    }

    toggleNoConsentTransactions() {
        this.showUpdateSdkWarning = true;
        this.collectionPoint.NoConsentTransactions = !this.collectionPoint.NoConsentTransactions;
        this.updateCollectionPoint();
    }

    toggleExternalSdkSettings() {
        this.collectionPoint.HostedSDK = !this.collectionPoint.HostedSDK;
        this.updateCollectionPoint();
    }

    closeSdkUpdateWarning() {
        this.showUpdateSdkWarning = false;
    }

    copySdk() {
        if (!this.sdkCodeElement) {
            return;
        }
        const sdk: string = this.sdkCodeElement.nativeElement.textContent || this.sdkCodeElement.nativeElement.innerText;
        Utilities.copyToClipboard(sdk.trim());
        this.collectionPoint.LastSdkIntegrationDate = new Date();
        this.updateCollectionPoint(false);
        this.toastService.success(this.translatePipe.transform("SdkCopied"), this.translatePipe.transform("SdkCopiedSuccessfully"));
    }

    private updateCollectionPoint(showToaster: boolean = true) {
        this.updateInProgress = true;
        if (this.collectionPoint.Purposes) {
            this.collectionPoint.PurposeId = this.collectionPointService.idsToString(this.collectionPoint.Purposes);
        }
        const updateModel = UpdateCollectionPoint.fromDetails(this.collectionPoint);
        this.collectionPointService.updateCollectionPoint(this.collectionPoint.Id, this.collectionPoint.Version, updateModel, showToaster)
            .then(() => {
                this.updateInProgress = false;
            });
    }
}
