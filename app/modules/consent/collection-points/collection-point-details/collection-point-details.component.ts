// Third Party
import {
    Inject,
    Component,
    OnInit,
} from "@angular/core";
import { StateService } from "@uirouter/core";
import { orderBy } from "lodash";

// Components and Constants
import crConstants from "consentModule/shared/cr-constants";
import { CrCollectionPointCommon } from "consentModule/collection-points/shared/collection-point-common.component";
import { LanguageSelectionModal } from "sharedModules/components/language-selection-modal/language-selection-modal.component";

// Interfaces
import { IStore } from "interfaces/redux.interface";
import { ITabsNav } from "interfaces/one-tabs-nav.interface";
import { IVersionListItem } from "interfaces/consent/cr-versions.interface";
import { IGetAllLanguageResponse } from "interfaces/dsar/dsar-edit-language.interface";
import { IDropdownOption } from "interfaces/one-dropdown.interface";
import { ILanguageSelectionModalResolve } from "interfaces/language-selection-modal-resolve.interface";

// Redux
import { StoreToken } from "tokens/redux-store.token";

// Rxjs
import {
    filter,
    take,
} from "rxjs/operators";

// Services
import { CollectionPointService } from "consentModule/shared/services/collection-point.service";
import { ConsentPurposeService } from "consentModule/shared/services/cr-purpose.service";
import { Permissions } from "modules/shared/services/helper/permissions.service";
import {
    ConfirmModalType,
    OtModalService,
} from "@onetrust/vitreus";

// Models
import { CollectionPointDetails } from "crmodel/collection-point-details";
import { CollectionPointFormPayload } from "crmodel/collection-point-details";
import { CollectionPointSettingsPayload } from "crmodel/collection-point-details";
import { EntityItem } from "crmodel/entity-item";
import { UpdateCollectionPoint } from "crmodel/create-collection-point";
import { PreferenceCenter } from "crmodel/preference-center-list";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

@Component({
    selector: "cr-collection-point-details",
    templateUrl: "./collection-point-details.component.html",
})
export class CrCollectionPointDetailsComponent extends CrCollectionPointCommon implements OnInit {
    isDrawerOpen: boolean;
    loading: boolean;
    saving: boolean;
    privacyPolicyValid: boolean;
    redirectUrlValid: boolean;
    collectionPointValid: boolean;
    collectionPointVersions: IVersionListItem[];
    dataElements: EntityItem[];
    allLanguages: IGetAllLanguageResponse[];
    defaultLanguage: string;
    ellipsisOptions: IDropdownOption[];
    crConstants = crConstants;

    multiplePreferenceToggle = this.permissions.canShow("ConsentMultiplePreferenceCenters");
    readonlyPreferenceCenter = this.permissions.canShow("ConsentReadonlyPreferenceCenter");
    createNewVersionToggle = this.permissions.canShow("ConsentNewCollectionPointVersion");
    canSeeBulkImport = this.permissions.canShow("ImportOverview");
    collectionPointLanguagesToggle = this.permissions.canShow("ConsentCollectionPointLanguages");
    dataElementsEnabled = this.permissions.canShow("ConsentDataElements");

    tabs: ITabsNav[];
    tabName: string;

    constructor(
        @Inject(StoreToken) private store: IStore,
        protected stateService: StateService,
        protected collectionPointService: CollectionPointService,
        private consentPurposeService: ConsentPurposeService,
        private permissions: Permissions,
        private otModalService: OtModalService,
        private translatePipe: TranslatePipe,
    ) {
        super(stateService, collectionPointService);
        this.tabName = stateService.params.tab;
    }

    ngOnInit() {
        this.loadCollectionPoint();
        this.collectionPointService.getDataElements().then((dataElements: EntityItem[]) => {
            this.dataElements = dataElements;
        });
        this.consentPurposeService.getAllLanguages().then((languages: IGetAllLanguageResponse[]) => {
            this.allLanguages = languages;
        });
    }

    loadCollectionPoint() {
        this.loading = true;
        this.collectionPointService.getCollectionPointDetails(this.collectionPointId, this.collectionPointVersion)
            .then((collectionPoint: CollectionPointDetails) => {
                this.collectionPoint = collectionPoint;
                this.collectionPoint.PreferenceCenterGuid = this.collectionPoint.PreferenceCenter.Id;
                if (this.collectionPoint) {
                    if (!this.collectionPoint.Languages) { this.collectionPoint.Languages = []; }
                    if (this.collectionPoint.CollectionPointType === this.crConstants.CRTypes.PreferenceCenter) {
                        if (this.multiplePreferenceToggle) {
                            this.stateService.go("zen.app.pia.module.consentreceipt.views.preferencecenters.details", { Id: this.collectionPointId });
                        } else {
                            this.stateService.go("zen.app.pia.module.consentreceipt.views.preferencecenters.list");
                        }
                    }
                    this.getEllipsisOptions();
                    this.getCollectionPointVersions();
                    this.configureTabs();
                    this.checkLinks();
                    this.getCollectionPointValid();
                }
                this.loading = false;
            });
    }

    handleCollectionPointChange(payload: CollectionPointFormPayload) {
        if (payload) {
            this.collectionPoint = payload.details;
            this.privacyPolicyValid = payload.privacyPolicyValid;
        }
        this.getCollectionPointValid();
    }

    handleRedirectUrlChange(payload: CollectionPointSettingsPayload) {
        if (payload) {
            this.collectionPoint.RedirectUrl = payload.redirectUrl;
            this.redirectUrlValid = payload.redirectUrlValid;
        }
        this.getCollectionPointValid();
    }

    handlePreferenceCenterChange(payload: PreferenceCenter) {
        this.collectionPoint.PreferenceCenter = payload;
        this.collectionPoint.PreferenceCenterGuid = payload ? payload.Id : "";
    }

    handleResponsibleUserChange(payload: string) {
        if (payload) { this.collectionPoint.ResponsibleUserId = payload; }
        this.getCollectionPointValid();
    }

    handleUpdate(payload: boolean) {
        this.saving = payload;
    }

    getEllipsisOptions() {
        const shouldShowEllipsis = this.collectionPointType &&
            this.collectionPointType !== this.crConstants.CRTypes.NotificationOptOut;

        this.ellipsisOptions = shouldShowEllipsis ? [
            {
                text: this.translatePipe.transform("ManageLanguages"),
                action: () => {
                    this.showEditLanguagesModal();
                },
            },
            {
                text: this.translatePipe.transform("ViewVersions"),
                action: () => {
                    this.toggleDrawer();
                },
            },
            {
                text: this.translatePipe.transform("Transactions"),
                action: () => {
                    this.goToTransactions();
                },
            },
        ] : [];
    }

    getCollectionPointVersions() {
        this.collectionPointService.getSelectedCollectionPointVersions(this.collectionPoint.Id).then((result: IVersionListItem[]) => {
            this.collectionPointVersions = orderBy(result, "Version", "desc");
        });
    }

    showEditLanguagesModal() {
        const languageSelectionModalData: ILanguageSelectionModalResolve = {
            defaultLanguage: this.collectionPoint.Language,
            selectedLanguages: this.collectionPoint.Languages,
            allLanguages: this.allLanguages,
        };
        this.otModalService.create(
            LanguageSelectionModal,
            {
                isComponent: true,
            },
            languageSelectionModalData,
        ).pipe(
            take(1),
            filter((response: { languages: string[], defaultLanguage: string }): boolean => {
                return response && response.languages.length && ( response.defaultLanguage.trim() !== null);
            }),
        ).subscribe(({ languages, defaultLanguage }) => {
            this.selectLanguages(languages, defaultLanguage);
        });
    }

    selectLanguages(selection: string[], defaultLanguage: string) {
        this.collectionPoint.Language = defaultLanguage;
        this.collectionPoint.Languages = selection;
        this.updateCollectionPoint();
    }

    publishCollectionPoint() {
        this.otModalService.confirm({
            type: ConfirmModalType.WARNING,
            translations: {
                title: this.translatePipe.transform("Publish"),
                desc: this.translatePipe.transform("ActivateCollectionPointConfirmation"),
                confirm: this.translatePipe.transform("AreYouSureContinue"),
                submit: this.translatePipe.transform("Publish"),
                cancel: this.translatePipe.transform("Cancel"),
            },
        }).pipe(
            take(1),
            filter((result: boolean) => result),
        ).subscribe(() => {
            this.loading = true;
            this.collectionPoint.Status = crConstants.CRCollectionPointStatus.Active;
            this.updateCollectionPoint().then((result: boolean) => {
                if (result) {
                            this.stateService.go("zen.app.pia.module.consentreceipt.views.collections.details",
                    { Id: this.collectionPointId, version: this.collectionPointVersion }, { reload: true });
                } else {
                    this.loading = false;
                }
            });
        });
    }

    updateCollectionPoint(): ng.IPromise<boolean> {
        this.saving = true;
        this.collectionPoint.PurposeId = this.collectionPointService.idsToString(this.collectionPoint.Purposes);
        const updateModel = UpdateCollectionPoint.fromDetails(this.collectionPoint);
        return this.collectionPointService.updateCollectionPoint(this.collectionPoint.Id, this.collectionPoint.Version, updateModel)
            .then((result: boolean): boolean => {
                if (this.collectionPoint.CollectionPointType === crConstants.CRTypes.Mobile) {
                    this.loadCollectionPoint();
                }
                this.saving = false;
                return result;
            });
    }

    toggleDrawer() {
        this.isDrawerOpen = !this.isDrawerOpen;
    }

    tabChanged = (option: ITabsNav) => {
        this.tabName = option.id;
        this.stateService.go(".", { Id: this.collectionPointId, tab: option.id });
    }

    configureTabs() {
        if (this.tabs) {
            return;
        }

        this.tabs = [
            {
                id: "details",
                text: this.translatePipe.transform("Details"),
                action: (option: ITabsNav) => this.tabChanged(option),
            },
        ];

        if (this.shouldShowSettingsAndReportTabs) {
            this.tabs.push({
                id: "settings",
                text: this.translatePipe.transform("Settings"),
                action: (option: ITabsNav) => this.tabChanged(option),
            });
        }

        if (this.collectionPointType === this.crConstants.CRTypes.WebForm) {
            this.tabs = this.tabs.concat([
                {
                    id: "sdk",
                    text: this.translatePipe.transform("SDK"),
                    action: (option: ITabsNav) => this.tabChanged(option),
                },
                {
                    id: "api",
                    text: this.translatePipe.transform("CustomAPI"),
                    action: (option: ITabsNav) => this.tabChanged(option),
                },
                {
                    id: "preview",
                    text: this.translatePipe.transform("Preview"),
                    action: (option: ITabsNav) => this.tabChanged(option),
                },
            ]);
        } else if (this.collectionPointType === this.crConstants.CRTypes.API) {
            this.tabs = this.tabs.concat({
                id: "api",
                text: this.translatePipe.transform("CustomAPI"),
                action: (option: ITabsNav) => this.tabChanged(option),
            });
        } else if (this.collectionPointType === this.crConstants.CRTypes.Mobile) {
            this.tabs = this.tabs.concat({
                id: "integrations",
                text: this.translatePipe.transform("Integrations"),
                action: (option: ITabsNav) => this.tabChanged(option),
            });
        }

        if (this.shouldShowSettingsAndReportTabs) {
            this.tabs.push({
                id: "report",
                text: this.translatePipe.transform("Report"),
                action: (option: ITabsNav) => this.tabChanged(option),
            });
        }

        if (this.canSeeBulkImport && this.shouldShowBulkImportTab) {
            this.tabs = this.tabs.concat({
                id: "import",
                text: this.translatePipe.transform("BulkImport"),
                action: (option: ITabsNav) => this.tabChanged(option),
            });
        }
    }

    checkLinks() {
        this.redirectUrlValid = this.collectionPoint && (!this.collectionPoint.RedirectUrl || this.collectionPointService.validateLink(this.collectionPoint.RedirectUrl));
        this.privacyPolicyValid = this.collectionPoint && (!this.collectionPoint.PrivacyPolicyUrl || this.collectionPointService.validateLink(this.collectionPoint.PrivacyPolicyUrl));
    }

    goToRoute(collectionPointVersion: number) {
        if (collectionPointVersion > 1) {
            this.stateService.go("zen.app.pia.module.consentreceipt.views.collections.details", { Id: this.collectionPoint.Id, version: collectionPointVersion - 1 });
        } else {
            this.stateService.go("zen.app.pia.module.consentreceipt.views.collections");
        }
    }

    goToTransactions() {
        this.stateService.go("zen.app.pia.module.consentreceipt.views.transactions.list",
            { collectionPointId: this.collectionPoint.Id });
    }

    goToVersion(version: number) {
        this.stateService.go("zen.app.pia.module.consentreceipt.views.collections.details", { Id: this.collectionPoint.Id, version });
    }

    onHeaderRouteClick(route: { path: string, params: any }) {
        this.stateService.go(route.path);
    }

    get canEdit(): boolean {
        return this.collectionPoint && this.collectionPoint.Status === this.crConstants.CRCollectionPointStatus.Draft;
    }

    get cookieBannerConsent(): boolean {
        return this.collectionPoint && this.collectionPoint.ConsentType === this.crConstants.CRConsentTypes.CookieBannerFormSubmit;
    }

    private get shouldShowSettingsAndReportTabs(): boolean {
        const EXCLUSION_TYPES = [
            this.crConstants.CRTypes.Cookie,
            this.crConstants.CRTypes.Mobile,
            this.crConstants.CRTypes.NotificationOptOut,
        ];

        return this.collectionPointType &&
            EXCLUSION_TYPES.includes(this.collectionPointType) === false;
    }

    private get shouldShowBulkImportTab(): boolean {
        const EXCLUSION_TYPES = [
            this.crConstants.CRTypes.Cookie,
            this.crConstants.CRTypes.Mobile,
            this.crConstants.CRTypes.NotificationOptOut,
            this.crConstants.CRTypes.API,
        ];

        return this.collectionPointType &&
            EXCLUSION_TYPES.includes(this.collectionPointType) === false;
    }

    get canCreateNewVersion(): boolean {
        return this.createNewVersionToggle && this.collectionPoint && this.collectionPoint.Status === this.crConstants.CRCollectionPointStatus.Active &&
            this.collectionPoint.CanCreateNewVersion;
    }

    getCollectionPointValid() {
        this.collectionPointValid = this.redirectUrlValid
            && Boolean(this.collectionPoint.Name.trim())
            && Boolean(this.collectionPoint.Description.trim())
            && Boolean(this.collectionPoint.Purposes.length)
            && Boolean(this.collectionPoint.OrganizationId)
            && this.collectionPoint.PrivacyPolicyUrl
            && this.privacyPolicyValid;
        }

    createNewVersion() {
        this.collectionPointService.createNewVersion(this.collectionPoint.Id).then((result: CollectionPointDetails): CollectionPointDetails => {
            if (result) {
                this.stateService.go("zen.app.pia.module.consentreceipt.views.collections.details", { Id: this.collectionPoint.Id, version: result.Version });
            }
            return result;
        });
    }
}
