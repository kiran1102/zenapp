// Third Party
import {
    Component,
    OnInit,
} from "@angular/core";
import { StateService } from "@uirouter/core";

// Models
import { SdkSettings } from "crmodel/sdk-settings";

// Services
import { CollectionPointService } from "consentModule/shared/services/collection-point.service";

// Components
import { CrCollectionPointCommon } from "consentModule/collection-points/shared/collection-point-common.component";

@Component({
    selector: "cr-collection-point-api",
    templateUrl: "./collection-point-api.component.html",
})
export class CrCollectionPointApiComponent extends CrCollectionPointCommon implements OnInit {
    collectionPointToken: string;
    sdkSettings: SdkSettings;

    constructor(
        protected stateService: StateService,
        protected collectionPointService: CollectionPointService,
    ) {
        super(stateService, collectionPointService);
    }

    ngOnInit() {
        this.collectionPointService.getCollectionPointSettings(this.collectionPointId, this.collectionPointVersion)
            .then((settings: SdkSettings) => { this.sdkSettings = settings ? settings : new SdkSettings(); });
        this.collectionPointService.getCollectionPointToken(this.collectionPointId, this.collectionPointVersion)
            .then((token: string) => { this.collectionPointToken = token; });
    }
}
