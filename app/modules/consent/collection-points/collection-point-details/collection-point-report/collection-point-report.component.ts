// Third Party
import {
    Component,
    OnInit,
} from "@angular/core";
import { StateService } from "@uirouter/core";
import {
    sumBy,
    values,
    sum,
    isEmpty,
} from "lodash";

// Services
import { CollectionPointService } from "consentModule/shared/services/collection-point.service";

// Components and Constants
import { CrCollectionPointCommon } from "consentModule/collection-points/shared/collection-point-common.component";
import crConstants from "consentModule/shared/cr-constants";

// Interfaces
import { IHorseshoeGraphData } from "interfaces/one-horseshoe-graph.interface";
import { IStringMap } from "interfaces/generic.interface";
import { IPieGraphData } from "interfaces/pie-graph-data.interface";

// Models
import {
    CollectionPointReport,
    CollectionPointPurposeReport,
} from "crmodel/collection-point-report";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

@Component({
    selector: "cr-collection-point-report",
    templateUrl: "./collection-point-report.component.html",
})
export class CrCollectionPointReportComponent extends CrCollectionPointCommon implements OnInit {
    crConstants = crConstants;
    report: CollectionPointReport;
    statusGraph: IPieGraphData[];
    purposeGraph: IPieGraphData[];
    reportTimedOut: boolean;
    colors: string[] = [
        "#ED5565",
        "#FC6E51",
        "#FFCE54",
        "#A0D468",
        "#4ACCB9",
        "#4FC1E9",
        "#5D9CEC",
        "#967ADC",
        "#CCD1D9",
        "#656D78",
    ];

    constructor(
        protected stateService: StateService,
        protected collectionPointService: CollectionPointService,
        private translatePipe: TranslatePipe,
    ) {
        super(stateService, collectionPointService);
    }

    ngOnInit() {
        this.collectionPointService.getCollectionPointReport(this.collectionPointId).then((report: CollectionPointReport) => {
            if (report) {
                this.report = report;
                this.purposeGraph = this.generateTransactionsByPurpose();
                this.statusGraph = this.generateTransactionsByStatus();
            } else {
                this.reportTimedOut = true;
            }
        });
    }

    getNoConsentGraph() {
        const totalCount = this.collectionPoint.DoubleOptIn ?
            Math.max(this.report.Transactions.PendingTransactionCount + this.report.Transactions.NoConsentTransactionCount, this.report.Transactions.ActiveTransactionCount) :
            this.report.Transactions.ActiveTransactionCount + this.report.Transactions.NoConsentTransactionCount;

        if (!this.report || totalCount === 0) {
            return this.collectionPointService.emptyGraph();
        }

        const graph: IHorseshoeGraphData = {
            series: [
                {
                    graphClass: "stroke-green",
                    value: (this.report.Transactions.ActiveTransactionCount / totalCount),
                    label: this.translatePipe.transform("Consent"),
                },
                {
                    graphClass: "stroke-white",
                    value: 0.005,
                    label: "",
                },
                {
                    graphClass: "stroke-grey-1",
                    value: ((totalCount - this.report.Transactions.ActiveTransactionCount) / totalCount),
                    label: this.translatePipe.transform("NoConsent"),
                },
            ],
            legend: [
                {
                    label: this.translatePipe.transform("Consent"),
                    class: "background-green",
                },
                {
                    label: this.translatePipe.transform("NoConsent"),
                    class: "background-grey-1",
                },
            ],
        };
        return graph;
    }

    get consentRate(): string {
        const totalCount = this.collectionPoint.DoubleOptIn ?
            this.report.Transactions.PendingTransactionCount + this.report.Transactions.NoConsentTransactionCount :
            this.report.Transactions.ActiveTransactionCount + this.report.Transactions.NoConsentTransactionCount;

        if (!this.report || this.report.TransactionCount === 0 || totalCount === 0) {
            return "0%";
        }

        const consentRate = 100 * Math.min(this.report.Transactions.ActiveTransactionCount / totalCount, 1);
        return `${consentRate.toFixed(0)}%`;
    }

    purposeConsentRate(purpose: CollectionPointPurposeReport): string {
        if (!purpose || purpose.TransactionCount === 0) {
            return "0%";
        }
        const consentRate = 100 * Math.min(purpose.TransactionCount / (purpose.NoConsentTransactionCount + purpose.TransactionCount), 1);
        return `${consentRate.toFixed(0)}%`;
    }

    purposeConsentRateClass(purpose: CollectionPointPurposeReport): IStringMap<string> {
        const rate = this.purposeConsentRate(purpose);
        return rate ? { width: rate } : null;
    }

    get transactionCount(): string {
        return this.report ? this.collectionPointService.shortenLargeNumber(this.report.TransactionCount) : "";
    }

    get receiptCount(): string {
        return this.report ? this.collectionPointService.shortenLargeNumber(this.report.ReceiptCount) : "";
    }

    get hasPurposes(): boolean {
        return this.report && !isEmpty(this.report.Purposes);
    }

    private generateTransactionsByStatus(): IPieGraphData[] {
        if (!this.report) {
            return null;
        }

        const totalCount: number = sum(values(this.report.Transactions));
        const graph: IPieGraphData[] = [
            {
                color: "#656D78",
                legendLabel: this.translatePipe.transform("Expired"),
                magnitude: this.report.Transactions.ExpiredTransactionCount / totalCount,
                label: this.ratioToPercentage(this.report.Transactions.ExpiredTransactionCount / totalCount) + "%",
                show: true,
            },
            {
                color: "#1F96DB",
                legendLabel: this.translatePipe.transform("Pending"),
                magnitude: this.report.Transactions.PendingTransactionCount / totalCount,
                label: this.ratioToPercentage(this.report.Transactions.PendingTransactionCount / totalCount) + "%",
                show: true,
            },
            {
                color: "#F5A623",
                legendLabel: this.translatePipe.transform("NoConsent"),
                magnitude: this.report.Transactions.NoConsentTransactionCount / totalCount,
                label: this.ratioToPercentage(this.report.Transactions.NoConsentTransactionCount / totalCount) + "%",
                show: true,
            },
            {
                color: "#6CC04A",
                legendLabel: this.translatePipe.transform("Active"),
                magnitude: this.report.Transactions.ActiveTransactionCount / totalCount,
                label: this.ratioToPercentage(this.report.Transactions.ActiveTransactionCount / totalCount) + "%",
                show: true,
            },
            {
                color: "#FC6E51",
                legendLabel: this.translatePipe.transform("Withdrawn"),
                magnitude: this.report.Transactions.WithdrawTransactionCount / totalCount,
                label: this.ratioToPercentage(this.report.Transactions.WithdrawTransactionCount / totalCount) + "%",
                show: true,
            },
        ];
        return graph;
    }

    private generateTransactionsByPurpose(): IPieGraphData[] {
        if (!this.report) {
            return null;
        }
        const graph: IPieGraphData[] = [];
        const totalCount = sumBy(this.report.Purposes, (item: CollectionPointPurposeReport) => item.TransactionCount + item.NoConsentTransactionCount);
        for (let i = 0; i < this.report.Purposes.length; i++) {
            const element = this.report.Purposes[i];
            const colorIndex = i % this.colors.length;
            graph.push({
                color: this.colors[colorIndex],
                legendLabel: element.Label,
                magnitude: (element.TransactionCount + element.NoConsentTransactionCount) / totalCount,
                label: this.ratioToPercentage((element.TransactionCount + element.NoConsentTransactionCount) / totalCount) + "%",
                show: true,
            });
        }
        return graph;
    }

    private ratioToPercentage(ratio: number): string {
        return (100 * Math.min(ratio, 1)).toFixed(0);
    }

}
