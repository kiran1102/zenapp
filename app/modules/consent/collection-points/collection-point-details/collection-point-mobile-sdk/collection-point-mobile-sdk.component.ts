import {
    Component,
    Input,
    OnInit,
} from "@angular/core";
import { StateService } from "@uirouter/core";
import Utilities from "Utilities";

// Models
import { CollectionPointDetails } from "crmodel/collection-point-details";
import { SdkSettings } from "crmodel/sdk-settings";
import { PreferenceCenter } from "crmodel/preference-center-list";
import { PreferenceCenterList } from "crmodel/preference-center-list";
import { PreferenceCenterFilters } from "crmodel/preference-center-filters";

// Services
import { CollectionPointService } from "consentModule/shared/services/collection-point.service";
import { ConsentPreferenceService } from "consentModule/shared/services/cr-preference.service";
import { NotificationService } from "modules/shared/services/provider/notification.service";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

// Constants
import crConstants from "consentModule/shared/cr-constants";

@Component({
    selector: "cr-collection-point-mobile-sdk",
    templateUrl: "./collection-point-mobile-sdk.component.html",
})

export class CrCollectionPointMobileSdkComponent implements OnInit {
    @Input() collectionPoint: CollectionPointDetails;

    purposeId: string;
    apiIsExpanded: boolean;
    sdkIsExpanded: boolean;
    sdksetting = {} as SdkSettings;
    preferenceCenter: PreferenceCenter;
    preferenceCenterList: PreferenceCenterList;
    preferenceCenterId: string;
    organizationId: string;
    iabVendorId: number;
    isDownloading = false;
    filename: string;
    sdkFileNames = crConstants.SdkFileNames;

    private collectionPointId = this.stateService.params.Id;
    private collectionPointVersion = Number(this.stateService.params.version || 1);

    constructor(
        private stateService: StateService,
        private collectionPointService: CollectionPointService,
        private consentPreferenceService: ConsentPreferenceService,
        private translatePipe: TranslatePipe,
        private notificationService: NotificationService,
    ) { }

    ngOnInit() {
        this.apiIsExpanded = true;
        this.sdkIsExpanded = true;
        this.collectionPointService.getCollectionPointDetails(this.collectionPointId, this.collectionPointVersion)
            .then((collectionPoint: CollectionPointDetails) => {
                this.collectionPoint = collectionPoint;
                this.organizationId = collectionPoint.OrganizationId;
                this.iabVendorId = this.collectionPoint.IabVendorId || null;
                this.getPreferenceCenter({}, this.collectionPoint.Name);
            });
        this.collectionPointService.getCollectionPointMobileSdkSetting(this.collectionPointId)
            .then((settings: SdkSettings) => {
                this.sdksetting = settings ? settings : new SdkSettings();
            });
    }

    public onToggleApi = () => {
        this.apiIsExpanded = !this.apiIsExpanded;
    }

    public onToggleSdk = () => {
        this.sdkIsExpanded = !this.sdkIsExpanded;
    }

    public downloadSdk(fileName: string) {
        this.filename = fileName;
        this.isDownloading = true;
        this.collectionPointService.getCollectionPointDownloadSetting(fileName)
            .then(() => this.isDownloading = false);
    }

    private copyItem(item: string, msg: string) {
        Utilities.copyToClipboard(item);
        this.notificationService.alertSuccess(
            this.translatePipe.transform("Success"),
            this.translatePipe.transform(msg),
        );
    }

    private getPreferenceCenter(filters: PreferenceCenterFilters = {}, collectionPointName: string) {
        this.consentPreferenceService.getPreferenceCenters(filters, 0, 1000)
            .then((items: PreferenceCenterList) => {
                const filteredPreferenceCenterId = items.content.filter((item) => item.Name === collectionPointName);
                this.preferenceCenterId = filteredPreferenceCenterId[0].Id;
            });
    }
}
