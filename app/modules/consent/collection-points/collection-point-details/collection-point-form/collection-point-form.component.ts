// Third Party
import {
    Component,
    OnInit,
    Inject,
    Output,
    EventEmitter,
    Input,
} from "@angular/core";
import { StateService } from "@uirouter/core";
import { IPromise } from "angular";
import {
    matches,
    sortBy,
} from "lodash";

// Interfaces
import { IOrganization } from "interfaces/org.interface";
import { IUser } from "interfaces/user.interface";
import { IOrgUserAdapted } from "interfaces/org-user.interface";
import { IVendorMobile } from "interfaces/cookies/cc-iab-purpose.interface";
import { IPageableOf } from "interfaces/pagination.interface";
import { IStore } from "interfaces/redux.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";

// Models
import { CollectionPointDetails } from "crmodel/collection-point-details";
import { CollectionPointFormPayload } from "crmodel/collection-point-details";
import { EntityItem } from "crmodel/entity-item";
import { PurposeDetails } from "crmodel/purpose-details";
import { PurposeFilters } from "crmodel/purpose-filters";
import { PurposeList } from "crmodel/purpose-list";
import { PreferenceCenter } from "crmodel/preference-center-list";

// Redux
import { StoreToken } from "tokens/redux-store.token";
import { getCurrentOrgList } from "oneRedux/reducers/org.reducer";
import { getCurrentUser } from "oneRedux/reducers/user.reducer";

// Services
import { CollectionPointService } from "consentModule/shared/services/collection-point.service";
import { ConsentPurposeService } from "consentModule/shared/services/cr-purpose.service";
import { IDropDownElement } from "consentModule/shared/services/consent-base.service";
import { CookieIABIntegrationService } from "cookiesService/cookie-iab-integration.service";
import { Permissions } from "modules/shared/services/helper/permissions.service";
import { LookupService } from "sharedModules/services/helper/lookup.service.ts";

// Constants and Utilities
import crConstants from "consentModule/shared/cr-constants";

// Components
import { CrCollectionPointCommon } from "consentModule/collection-points/shared/collection-point-common.component";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

@Component({
    selector: "cr-collection-point-form",
    templateUrl: "./collection-point-form.component.html",
})
export class CrCollectionPointFormComponent extends CrCollectionPointCommon implements OnInit {
    @Input() elements: EntityItem[];
    @Output() onFormChange = new EventEmitter<CollectionPointFormPayload>();
    @Output() onUpdate = new EventEmitter();

    canEditNoticeFields = this.permissions.canShow("ConsentEditNoticeFields");
    doubleOptInEnabled = this.permissions.canShow("ConsentDoubleOptIn");
    dataElementsEnabled = this.permissions.canShow("ConsentDataElements");
    consentIABCollectionPoint = this.permissions.canShow("ConsentIABCollectionPoint");
    crConstants = crConstants;

    collectionPoint: CollectionPointDetails;
    updateInProgress = false;

    crConsentTypes: IDropDownElement[];
    selectedConsentType: IDropDownElement;

    orgList: IOrganization[];
    selectedOrg: IOrganization;

    userList: IOrgUserAdapted[];
    userListOptions: IOrgUserAdapted[];
    responsibleUser: IOrgUserAdapted;
    currentUser: IUser;

    fullPurposeList: PurposeDetails[];
    purposeOptions: PurposeDetails[];
    selectedPurposes: PurposeDetails[] = [];
    selectedDataElements: EntityItem[] = [];
    dataElementOptions: EntityItem[];
    dataElementInput: string;

    iabConsentToggled = false;
    vendorsList: IPageableOf<IVendorMobile>;
    purposeListIAB: PurposeDetails[];
    selectedPurposesIAB: PurposeDetails[] = [];
    selectedList: number;
    preferenceCenter: PreferenceCenter;

    overviewOpen = true;
    configurationOpen = true;

    constructor(
        @Inject(StoreToken) public store: IStore,
        private permissions: Permissions,
        private consentPurposeService: ConsentPurposeService,
        private cookieIABIntegrationService: CookieIABIntegrationService,
        private lookupService: LookupService,
        private translatePipe: TranslatePipe,
        protected stateService: StateService,
        protected collectionPointService: CollectionPointService,
    ) {
        super(stateService, collectionPointService);
    }

    ngOnInit() {
        this.crConsentTypes = this.collectionPointService.getDropDownOptions(crConstants.CRConsentTypesNoCookie);
        if (this.collectionPoint.DataElements) {
            for (const dataElement of this.collectionPoint.DataElements) {
                this.selectedDataElements.push(this.elements.find(matches({ Label: dataElement })));
            }
        }
        if (this.collectionPoint) {
            this.selectedPurposes = this.collectionPoint.Purposes;
            this.crConsentTypes = this.collectionPointService.updateConsentTypeList(this.collectionPoint.Purposes, this.crConsentTypes);
            this.selectedConsentType = this.crConsentTypes.find((c: IDropDownElement) => c.value === this.collectionPoint.ConsentType);
        }

        this.consentPurposeService.getActivePurposeFilters().then((purposes: PurposeDetails[]) => {
            this.fullPurposeList = purposes;
            this.purposeOptions = this.lookupService.filterOptionsBySelections(this.selectedPurposes, this.fullPurposeList, "Id");
        });
        this.dataElementOptions = this.lookupService.filterOptionsBySelections(this.selectedDataElements, this.elements, "Label");

        this.orgList = getCurrentOrgList(this.store.getState());
        const org: IOrganization = this.orgList.find((o: IOrganization): boolean => o.Id === this.collectionPoint.OrganizationId);
        if (org) {
            this.getOrgUsers(org).then(() => this.findResponsibleUser());
        } else {
            this.stateService.go("zen.app.pia.module.consentreceipt.views.collections.list");
        }
        this.currentUser = getCurrentUser(this.store.getState());

        if (this.collectionPoint.IabVendorId) {
            this.selectedList = this.collectionPoint.IabVendorId;
            this.iabConsentToggled = true;
        }

        if (this.collectionPoint.CollectionPointType === crConstants.CRTypes.Mobile) {
            if (this.consentIABCollectionPoint) {
                this.searchIabPurposes();
                this.getVendors();
            }
        }
    }

    getOrgUsers(org: IOrganization): IPromise<void> {
        this.selectedOrg = org;
        return this.collectionPointService.getSelectedOrgUsers(org.Id).then((response: IProtocolResponse<IOrgUserAdapted[]>) => {
            if (response.result) {
                this.userList = this.sortUsers(response.data);
                this.userListOptions = this.userList;
                this.responsibleUser = undefined;
            }
        });
    }

    updateSelectedOrg(org: IOrganization) {
        this.selectedOrg = org;
        this.collectionPoint.OrganizationId = org.Id;
        this.collectionPoint.ResponsibleUserId = "";
        this.getOrgUsers(this.selectedOrg);
        this.onFormChange.emit({ details: this.collectionPoint, privacyPolicyValid: this.isPrivacyPolicyValid });
    }

    findResponsibleUser() {
        this.responsibleUser = this.userList.find((user: IOrgUserAdapted): boolean => {
            return user.Id === this.collectionPoint.ResponsibleUserId;
        });
    }

    updateConsentType(type: IDropDownElement) {
        this.selectedConsentType = type;
        this.collectionPoint.ConsentType = type.value;
        this.onFormChange.emit({ details: this.collectionPoint, privacyPolicyValid: this.isPrivacyPolicyValid });
    }

    handleChanged(payload: string, prop: keyof CollectionPointDetails) {
        this.collectionPoint[prop] = payload;
        this.onFormChange.emit({ details: this.collectionPoint, privacyPolicyValid: this.isPrivacyPolicyValid });
    }

    handleMultiSelect(purposeArray: PurposeDetails[]) {
        this.selectedPurposes = purposeArray;
        this.collectionPoint.Purposes = this.selectedPurposes;
        this.purposeOptions = this.lookupService.filterOptionsBySelections(this.selectedPurposes, this.fullPurposeList, "Id");
        this.crConsentTypes = this.collectionPointService.updateConsentTypeList(this.selectedPurposes, this.crConsentTypes);
        const updatedConsentType = this.collectionPointService.modifyConsentType(this.selectedPurposes, this.crConsentTypes, this.collectionPoint.ConsentType);
        this.updateConsentType(updatedConsentType);
        this.onFormChange.emit({ details: this.collectionPoint, privacyPolicyValid: this.isPrivacyPolicyValid });
    }

    handleMultiSelectIAB(purposeArray: PurposeDetails[]) {
        this.selectedPurposesIAB = purposeArray;
        this.collectionPoint.Purposes = this.selectedPurposesIAB;
        this.crConsentTypes = this.collectionPointService.updateConsentTypeList(this.selectedPurposesIAB, this.crConsentTypes);
        const updatedConsentType = this.collectionPointService.modifyConsentType(this.selectedPurposesIAB, this.crConsentTypes, this.collectionPoint.ConsentType);
        this.updateConsentType(updatedConsentType);
        this.onFormChange.emit({ details: this.collectionPoint, privacyPolicyValid: this.isPrivacyPolicyValid });
    }

    toggleIabConsent() {
        this.iabConsentToggled = !this.iabConsentToggled;
        this.selectedPurposes = [];
        this.collectionPoint.Purposes = [];
        this.onFormChange.emit({ details: this.collectionPoint, privacyPolicyValid: this.isPrivacyPolicyValid });
    }

    toggleIabVendor(selection) {
        this.collectionPoint.IabVendorId = selection;
        this.onFormChange.emit({ details: this.collectionPoint, privacyPolicyValid: this.isPrivacyPolicyValid });
    }

    toggleHeader(sectionToggle: boolean) {
        sectionToggle = !sectionToggle;
    }

    onInputChange({ key, value }) {
        if (key === "Enter") {
            this.selectOption({ currentValue: this.selectedPurposes.push(this.purposeOptions[0]) });
            return;
        }
        this.purposeOptions = this.lookupService.filterOptionsByInput(this.selectedPurposes, this.fullPurposeList, value, "Id", "Label");
    }

    onResponsibleUserInputChange({ key, value }) {
        if (key === "Enter") {
            this.selectResponsibleUser({ currentValue: this.userList[0] });
            return;
        }
        this.userListOptions = this.lookupService.filterOptionsByInput([], this.userList, value, "Id", "FullName");
    }

    onDataElementInputChange({ key, value }) {
        if (key === "Enter") {
            this.selectDataElement({ currentValue: this.selectedDataElements.push(this.dataElementOptions[0]) });
            this.dataElementInput = "";
            return;
        }
        this.dataElementInput = value;
        this.dataElementOptions = this.lookupService.filterOptionsByInput(this.selectedDataElements, this.elements, value, "Id", "Label");
    }

    selectOption({ currentValue }) {
        this.selectedPurposes = currentValue;
        this.collectionPoint.Purposes = this.selectedPurposes;
        this.purposeOptions = this.lookupService.filterOptionsBySelections(this.selectedPurposes, this.fullPurposeList, "Id");
        this.crConsentTypes = this.collectionPointService.updateConsentTypeList(this.selectedPurposes, this.crConsentTypes);
        const updatedConsentType = this.collectionPointService.modifyConsentType(this.selectedPurposes, this.crConsentTypes, this.collectionPoint.ConsentType);
        this.updateConsentType(updatedConsentType);
        this.onFormChange.emit({ details: this.collectionPoint, privacyPolicyValid: this.isPrivacyPolicyValid });
    }

    selectDataElement({ currentValue }) {
        this.selectedDataElements = currentValue;
        this.dataElementOptions = this.lookupService.filterOptionsBySelections(this.selectedDataElements, this.elements, "Label");
        this.collectionPoint.DataElements = this.selectedDataElements.map((item: EntityItem): string => item.Label);
        this.onFormChange.emit({ details: this.collectionPoint, privacyPolicyValid: this.isPrivacyPolicyValid });
    }

    selectOtherDataElement() {
        const newOption: EntityItem = {
            Id: this.dataElementInput,
            Label: this.dataElementInput,
        };
        this.dataElementInput = "";
        this.elements.push(newOption);
        this.selectedDataElements = this.selectedDataElements.concat(newOption);
        this.dataElementOptions = this.lookupService.filterOptionsBySelections(this.selectedDataElements, this.elements, "Label");
        this.collectionPoint.DataElements = this.selectedDataElements.map((item: EntityItem): string => item.Label);
        this.onFormChange.emit({ details: this.collectionPoint, privacyPolicyValid: this.isPrivacyPolicyValid });
    }

    selectResponsibleUser({ currentValue }) {
        this.responsibleUser = currentValue;
        this.collectionPoint.ResponsibleUserId = this.responsibleUser ? this.responsibleUser.Id : "";
        this.userListOptions = this.userList;
        this.onFormChange.emit({ details: this.collectionPoint, privacyPolicyValid: this.isPrivacyPolicyValid });
    }

    sortUsers(users: IOrgUserAdapted[]): IOrgUserAdapted[] {
        users = sortBy(users, (user: IOrgUserAdapted): string => {
            return user.FullName.toLowerCase();
        });
        const currentUserIndex = users.findIndex((user: IOrgUserAdapted): boolean => {
            return user.Id === this.currentUser.Id;
        });
        if (currentUserIndex) {
            users.splice(currentUserIndex, 1);
            users = [{ Id: this.currentUser.Id, FullName: this.currentUser.FullName }, ...users];
        }
        return users;
    }

    get isPrivacyPolicyValid(): boolean {
        return this.collectionPoint && (!this.collectionPoint.PrivacyPolicyUrl || this.collectionPointService.validateLink(this.collectionPoint.PrivacyPolicyUrl));
    }

    private searchIabPurposes(filters: PurposeFilters = {}) {
        this.consentPurposeService.getPurposes(filters, 0, 1000)
            .then((items: PurposeList) => {
                this.purposeListIAB = items.content.filter((item) => item.Type === "IAB" && item.Status === "ACTIVE");
            });
    }

    private getVendors = (num: number = 0, size: number = 2000) => {
        this.cookieIABIntegrationService.getVendorsForMobile(num, size).then(
            (response: IPageableOf<IVendorMobile>) => {
                this.vendorsList = response;
            });
    }
}
