// Third Party
import {
    Component,
    EventEmitter,
    Inject,
    Input,
    OnInit,
    Output,
} from "@angular/core";
import { StateService } from "@uirouter/core";
import { sortBy } from "lodash";

// Interfaces
import { IOrganization } from "interfaces/org.interface";
import { IOrgUserAdapted } from "interfaces/org-user.interface";
import { IStore } from "interfaces/redux.interface";
import { IUser } from "interfaces/user.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";
import { getCurrentUser } from "oneRedux/reducers/user.reducer";

// Redux
import { getCurrentOrgList } from "oneRedux/reducers/org.reducer";
import { StoreToken } from "tokens/redux-store.token";

// Services
import { CollectionPointService } from "consentModule/shared/services/collection-point.service";
import { Permissions } from "modules/shared/services/helper/permissions.service";
import { LookupService } from "sharedModules/services/helper/lookup.service.ts";

// Models
import { EntityItem } from "crmodel/entity-item";

// Components and Constants
import { CrCollectionPointCommon } from "consentModule/collection-points/shared/collection-point-common.component";
import crConstants from "consentModule/shared/cr-constants";

@Component({
    selector: "cr-collection-point-info",
    templateUrl: "./collection-point-info.component.html",
})
export class CrCollectionPointInfoComponent extends CrCollectionPointCommon implements OnInit {
    @Input() elements: EntityItem[];
    @Output() onResponsibleUserChange = new EventEmitter<string>();

    canEditNoticeFields = this.permissions.canShow("ConsentEditNoticeFields");

    crConstants = crConstants;
    loading: boolean;
    updating: boolean;

    organizations: IOrganization[];
    selectedOrg: IOrganization;

    userList: IOrgUserAdapted[];
    userListOptions: IOrgUserAdapted[];
    responsibleUser: IOrgUserAdapted;
    currentUser: IUser;

    constructor(
        @Inject(StoreToken) readonly store: IStore,
        private lookupService: LookupService,
        private permissions: Permissions,
        protected stateService: StateService,
        protected collectionPointService: CollectionPointService,
    ) {
        super(stateService, collectionPointService);
    }

    ngOnInit() {
        this.loading = true;
        this.organizations = getCurrentOrgList(this.store.getState());
        this.currentUser = getCurrentUser(this.store.getState());
        this.findOrg();
        this.findResponsibleUser();
    }

    findOrg() {
        this.selectedOrg = this.organizations.find((organization: IOrganization): boolean => {
            return organization.Id === this.collectionPoint.OrganizationId;
        });
    }

    findResponsibleUser() {
        if (!this.selectedOrg) {
            this.stateService.go("zen.app.pia.module.consentreceipt.views.collections.list");
        } else {
            this.collectionPointService.getSelectedOrgUsers(this.selectedOrg.Id).then((response: IProtocolResponse<IOrgUserAdapted[]>) => {
                if (response.result) {
                    this.userList = this.sortUsers(response.data);
                    this.userListOptions = this.userList;
                    this.responsibleUser = this.userList.find((user: IOrgUserAdapted): boolean => {
                        return user.Id === this.collectionPoint.ResponsibleUserId;
                    });
                }
                this.loading = false;
            });
        }
    }

    onResponsibleUserInputChange({ key, value }) {
        if (key === "Enter") {
            this.selectResponsibleUser({ currentValue: this.userList[0] });
            return;
        }
        this.userListOptions = this.lookupService.filterOptionsByInput([], this.userList, value, "Id", "FullName");
    }

    selectResponsibleUser({ currentValue }) {
        this.responsibleUser = currentValue;
        this.userListOptions = this.userList;
        this.collectionPoint.ResponsibleUserId = this.responsibleUser ? this.responsibleUser.Id : "";
        this.onResponsibleUserChange.emit(this.collectionPoint.ResponsibleUserId);
    }

    sortUsers(users: IOrgUserAdapted[]): IOrgUserAdapted[] {
        users = sortBy(users, (user: IOrgUserAdapted): string => {
            return user.FullName.toLowerCase();
        });
        const currentUserIndex = users.findIndex((user: IOrgUserAdapted): boolean => {
            return user.Id === this.currentUser.Id;
        });
        if (currentUserIndex) {
            users.splice(currentUserIndex, 1);
            users = [{ Id: this.currentUser.Id, FullName: this.currentUser.FullName }, ...users];
        }
        return users;
    }

    typeCookieBanner(): boolean {
        return this.collectionPoint.ConsentType === this.crConstants.CRConsentTypes.CookieBannerFormSubmit;
    }
}
