// Angular
import { NgModule } from "@angular/core";

// Components
import { CrCollectionPointDetailsComponent } from "consentModule/collection-points/collection-point-details/collection-point-details.component";
import { CrCollectionPointApiComponent } from "consentModule/collection-points/collection-point-details/collection-point-api/collection-point-api.component";
import { CrCollectionPointFormComponent } from "consentModule/collection-points/collection-point-details/collection-point-form/collection-point-form.component";
import { CrCollectionPointImportComponent } from "consentModule/collection-points/collection-point-details/collection-point-import/collection-point-import.component";
import { CrCollectionPointInfoComponent } from "consentModule/collection-points/collection-point-details/collection-point-info/collection-point-info.component";
import { CrCollectionPointMobileSdkComponent } from "consentModule/collection-points/collection-point-details/collection-point-mobile-sdk/collection-point-mobile-sdk.component";
import { CrCollectionPointPreviewComponent } from "consentModule/collection-points/collection-point-details/collection-point-preview/collection-point-preview.component";
import { CrCollectionPointPreviewTableComponent } from "consentModule/collection-points/collection-point-details/collection-point-preview/collection-point-preview-table/collection-point-preview-table.component";
import { CrCollectionPointReportComponent } from "consentModule/collection-points/collection-point-details/collection-point-report/collection-point-report.component";
import { CrCollectionPointSdkComponent } from "consentModule/collection-points/collection-point-details/collection-point-sdk/collection-point-sdk.component";
import { CrCollectionPointSettingsComponent } from "consentModule/collection-points/collection-point-details/collection-point-settings/collection-point-settings.component";

// Modules
import { CollectionPointsSharedModule } from "consentModule/collection-points/shared/collection-points-shared.module";

@NgModule({
    imports: [
        CollectionPointsSharedModule,
    ],
    declarations: [
        CrCollectionPointDetailsComponent,
        CrCollectionPointApiComponent,
        CrCollectionPointFormComponent,
        CrCollectionPointImportComponent,
        CrCollectionPointInfoComponent,
        CrCollectionPointMobileSdkComponent,
        CrCollectionPointPreviewComponent,
        CrCollectionPointPreviewTableComponent,
        CrCollectionPointReportComponent,
        CrCollectionPointSdkComponent,
        CrCollectionPointSettingsComponent,
    ],
    entryComponents: [
        CrCollectionPointDetailsComponent,
    ],
})
export class CollectionPointDetailsModule { }
