// Angular
import { NgModule } from "@angular/core";

// Components
import { CrCollectionPointUpdatesComponent } from "consentModule/collection-points/collection-point-updates/collection-point-updates.component";

// Modules
import { CollectionPointsSharedModule } from "consentModule/collection-points/shared/collection-points-shared.module";

@NgModule({
    imports: [
        CollectionPointsSharedModule,
    ],
    declarations: [
        CrCollectionPointUpdatesComponent,
    ],
    entryComponents: [
        CrCollectionPointUpdatesComponent,
    ],
})
export class CollectionPointUpdatesModule { }
