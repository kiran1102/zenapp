import {
    Component,
    Inject,
} from "@angular/core";
import { StateService } from "@uirouter/core";

// Interfaces
import { IDropdownOption } from "interfaces/one-dropdown.interface";

// Models
import { CollectionPointList } from "crmodel/collection-point-list";
import { CollectionPointListItem } from "crmodel/collection-point-list";
import { ColumnDefinition } from "crmodel/column-definition";

// Services
import { CollectionPointService } from "consentModule/shared/services/collection-point.service";
import { ToastService } from "@onetrust/vitreus";
import { Permissions } from "modules/shared/services/helper/permissions.service";

// Components and Utilities
import { CrCollectionPointCommon } from "consentModule/collection-points/shared/collection-point-common.component";
import Utilities from "Utilities";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

// Constants
import crConstants from "consentModule/shared/cr-constants";

@Component({
    selector: "cr-collection-point-updates",
    templateUrl: "./collection-point-updates.component.html",
})
export class CrCollectionPointUpdatesComponent extends CrCollectionPointCommon {
    tableData: CollectionPointList;
    pageSize = 20;
    searchInProgress = false;
    exportInProgress = false;
    columns: ColumnDefinition[];
    purposeId: string;
    purposeVersion: number;
    selectedRows: CollectionPointListItem[] = [];
    isUpdating = false;
    jwtTokens: boolean;
    ellipsisOptions: IDropdownOption[];

    constructor(
        protected stateService: StateService,
        protected collectionPointService: CollectionPointService,
        private toastService: ToastService,
        private translatePipe: TranslatePipe,
        private permissions: Permissions,
    ) {
        super(stateService, collectionPointService);
        this.purposeId = this.stateService.params.purposeId;
        this.purposeVersion = Number(this.stateService.params.purposeVersion);
    }

    ngOnInit() {
        this.jwtTokens = this.permissions.canShow("ConsentCollectionPointUpdateJwts");
        this.columns = [
            {
                columnName: this.translatePipe.transform("Select"),
                cellValueKey: "Id",
                columnType: "selectRow",
            },
            {
                columnName: this.translatePipe.transform("Name"),
                cellValueKey: "Name",
                cellClass: "max-width-50",
                columnType: "idLink",
                route: "zen.app.pia.module.consentreceipt.views.collections.details",
                routeVersionField: "Version",
            },
            {
                columnName: this.translatePipe.transform("Version"),
                cellValueKey: "Version",
                columnType: "versionTag",
                rowToText: (row: CollectionPointListItem): string => `V${row.Version}`,
            },
        ];
        if (this.jwtTokens) {
            this.getEllipsisOptions();
            this.columns.push(
                {
                    columnName: this.translatePipe.transform("Token"),
                    cellValueKey: "Token",
                    cellClass: "max-width-30",
                },
                {
                    columnType: "actions",
                    getOptions: (row: CollectionPointListItem): IDropdownOption[] => this.collectionPointOptions(row),
                },
            );
        }
        this.searchCollectionPoints();
    }

    loadPage(page: number) {
        this.searchCollectionPoints(page);
    }

    getEllipsisOptions() {
        this.ellipsisOptions = [
            {
                text: this.translatePipe.transform("ExportAllTokens"),
                action: () => { this.exportAllTokens(); },
            },
        ];
        if (this.selectedRows.length > 0) {
            this.ellipsisOptions.push(
                {
                    text: this.translatePipe.transform("ExportSelectedTokens"),
                    action: () => { this.exportSelectedTokens(); },
                },
            );
        }
    }

    searchCollectionPoints(page: number = 0) {
        this.searchInProgress = true;
        this.collectionPointService.getCollectionPointsForUpdate(this.purposeId, this.purposeVersion - 1, this.jwtTokens, page, this.pageSize)
            .then((collectionPoints: CollectionPointList) => {
                this.tableData = collectionPoints;
                if (this.tableData.content.length === 0) {
                    this.stateService.go("zen.app.pia.module.consentreceipt.views.purposes.details", { Id: this.purposeId, version: this.purposeVersion });
                }
                this.searchInProgress = false;
            });
    }

    get isUpdateCollectionPointsDisabledCheck(): boolean {
        return this.selectedRows === undefined || this.selectedRows.length === 0;
    }

    updateCollectionPoints() {
        this.isUpdating = true;
        if (this.selectedRows.length > 0) {
            this.collectionPointService.bulkUpdateCollectionPoints(this.selectedRows, this.purposeId, this.purposeVersion)
                .then((result: boolean) => {
                    if (result) {
                        this.isUpdating = false;
                        this.stateService.go("zen.app.pia.module.consentreceipt.views.purposes.details", { Id: this.purposeId, version: this.purposeVersion });
                    }
                });
        }
    }

    collectionPointOptions(collectionPoint: CollectionPointListItem): IDropdownOption[] {
        return [
            {
                id: "copy",
                text: this.translatePipe.transform("CopyToken"),
                action: () => this.copyToken(collectionPoint.Token),
            },
        ];
    }

    goToRoute(cell: any) {
        this.stateService.go(cell.route, cell.params);
    }

    copyToken(token: string) {
        Utilities.copyToClipboard(token);
        this.toastService.success(this.translatePipe.transform("TokenCopied"), this.translatePipe.transform("TokenCopiedSuccessfully"));
    }

    exportSelectedTokens() {
        const selection = this.tableData.content.filter((row: CollectionPointListItem) => this.selectedRows.indexOf(row) !== -1);
        if (selection.length > 0) {
            this.exportTokens(selection);
        }
    }

    onToggleMenu() {
        this.getEllipsisOptions();
    }

    exportAllTokens() {
        this.exportInProgress = true;
        this.collectionPointService.getCollectionPointsForUpdate(this.purposeId, this.purposeVersion, this.jwtTokens, 0, 10000)
            .then((collectionPoints: CollectionPointList) => {
                if (collectionPoints && collectionPoints.content) {
                    this.exportTokens(collectionPoints.content);
                }
                this.exportInProgress = false;
            });
    }

    handleSelectionChange(selection: string[]) {
        selection.forEach((collectionPointGuid: string) => {
            if (!this.selectedRows.find((collectionPoint: CollectionPointListItem): boolean => {
                return collectionPoint.Id === collectionPointGuid;
            })) {
                this.selectedRows.push(this.tableData.content.find((row: CollectionPointListItem): boolean => {
                    return row.Id === collectionPointGuid;
                }));
            }
        });
        this.selectedRows.forEach((collectionPoint: CollectionPointListItem) => {
            if (!selection.find((collectionPointGuid: string): boolean => {
                return collectionPoint.Id === collectionPointGuid;
            })) {
                const indexToRemove = this.selectedRows.findIndex((item: CollectionPointListItem): boolean => {
                    return collectionPoint.Id === item.Id;
                });
                this.selectedRows.splice(indexToRemove, 1);
            }
        });
    }

    private exportTokens(collectionPoints: CollectionPointListItem[]) {
        const tokensHeader = `${this.translatePipe.transform("CollectionPointId")}${crConstants.CRClipboardSeparator}` +
            `${this.translatePipe.transform("Token")}${crConstants.CRClipboardNewLine}`;

        const tokens = collectionPoints.map((row) => `${row.Id}${crConstants.CRClipboardSeparator}${row.Token}`)
            .join(crConstants.CRClipboardNewLine);

        const exportFile = `${this.translatePipe.transform("TokenExportFileNamePrefix")}-${this.purposeId}-V${this.purposeVersion}.csv`;
        this.collectionPointService.downloadContent(exportFile, tokensHeader + tokens);
    }
}
