// Angular
import { NgModule } from "@angular/core";

// Components
import { CrCollectionPointTypeComponent } from "consentModule/collection-points/collection-point-type/collection-point-type.component";

// Modules
import { CollectionPointsSharedModule } from "consentModule/collection-points/shared/collection-points-shared.module";

@NgModule({
    imports: [
        CollectionPointsSharedModule,
    ],
    declarations: [
        CrCollectionPointTypeComponent,
    ],
    entryComponents: [
        CrCollectionPointTypeComponent,
    ],
})
export class CollectionPointTypeModule { }
