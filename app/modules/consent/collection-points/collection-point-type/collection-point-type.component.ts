// Third Party
import { Component } from "@angular/core";
import { StateService } from "@uirouter/core";

// Services
import { CollectionPointService } from "consentModule/shared/services/collection-point.service";
import { Permissions } from "modules/shared/services/helper/permissions.service";

// Components and Constants
import { CrCollectionPointCommon } from "consentModule/collection-points/shared/collection-point-common.component";
import crConstants from "consentModule/shared/cr-constants";

@Component({
    selector: "cr-collection-point-type",
    templateUrl: "./collection-point-type.component.html",
})
export class CrCollectionPointTypeComponent extends CrCollectionPointCommon {
    crConstants = crConstants;
    canSeeCookieConsent = this.permissions.canShow("Cookie");
    consentMobileSDK = this.permissions.canShow("ConsentMobileSDK");
    canSeeBulkImport = this.permissions.canShow("ImportOverview");

    constructor(
        protected stateService: StateService,
        protected collectionPointService: CollectionPointService,
        private permissions: Permissions,
    ) {
        super(stateService, collectionPointService);
    }

    selectCollectionPointType(collectionPointType: string) {
        this.stateService.go("zen.app.pia.module.consentreceipt.views.collections.create", { type: collectionPointType });
    }

    selectCookieCollectionPointType() {
        this.stateService.go("zen.app.pia.module.cookiecompliance.views.websites");
    }
}
