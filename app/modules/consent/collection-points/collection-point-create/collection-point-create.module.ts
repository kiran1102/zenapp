// Angular
import { NgModule } from "@angular/core";

// Components
import { CrCollectionPointCreateComponent } from "./collection-point-create.component";

// Modules
import { CollectionPointsSharedModule } from "consentModule/collection-points/shared/collection-points-shared.module";

@NgModule({
    imports: [
        CollectionPointsSharedModule,
    ],
    declarations: [
        CrCollectionPointCreateComponent,
    ],
    entryComponents: [
        CrCollectionPointCreateComponent,
    ],
})
export class CollectionPointCreateModule { }
