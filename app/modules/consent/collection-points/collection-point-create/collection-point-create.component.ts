// OT
declare var OneAlert: any;

// Third Party
import {
    Component,
    OnInit,
    Inject,
} from "@angular/core";
import { StateService } from "@uirouter/core";
import { IPromise } from "angular";
import {
    isEmpty,
    find,
    sortBy,
} from "lodash";

// Interfaces
import { IOrganization } from "interfaces/org.interface";
import { IUser } from "interfaces/user.interface";
import { IOrgUserAdapted } from "interfaces/org-user.interface";
import { IVendorMobile } from "interfaces/cookies/cc-iab-purpose.interface";
import { IPageableOf } from "interfaces/pagination.interface";
import { IStore } from "interfaces/redux.interface";
import { IGetAllLanguageResponse } from "interfaces/dsar/dsar-edit-language.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";

// Models
import { CollectionPointDetails } from "crmodel/collection-point-details";
import { EntityItem } from "crmodel/entity-item";
import { PurposeListItem } from "crmodel/purpose-list";
import { PurposeDetails } from "crmodel/purpose-details";
import { PurposeFilters } from "crmodel/purpose-filters";
import { PurposeList } from "crmodel/purpose-list";
import {
    NewPreferenceCenter,
    PreferenceCenter,
} from "crmodel/preference-center-list";
import { CreateCollectionPoint } from "crmodel/create-collection-point";

// Redux
import { StoreToken } from "tokens/redux-store.token";
import { getCurrentOrgList } from "oneRedux/reducers/org.reducer";
import { getCurrentUser } from "oneRedux/reducers/user.reducer";

// Services
import { CollectionPointService } from "consentModule/shared/services/collection-point.service";
import { ConsentPurposeService } from "consentModule/shared/services/cr-purpose.service";
import { ConsentPreferenceService } from "consentModule/shared/services/cr-preference.service";
import { IDropDownElement } from "consentModule/shared/services/consent-base.service";
import { CookieIABIntegrationService } from "cookiesService/cookie-iab-integration.service";
import { Permissions } from "modules/shared/services/helper/permissions.service";
import { LookupService } from "sharedModules/services/helper/lookup.service.ts";

// Constants
import crConstants from "consentModule/shared/cr-constants";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";
import { NgForm } from "@angular/forms";

@Component({
    selector: "cr-collection-point-create",
    templateUrl: "./collection-point-create.component.html",
})
export class CrCollectionPointCreateComponent implements OnInit {
    canEditNoticeFields = this.permissions.canShow("ConsentEditNoticeFields");
    doubleOptInEnabled = this.permissions.canShow("ConsentDoubleOptIn");
    dataElementsEnabled = this.permissions.canShow("ConsentDataElements");
    consentIABCollectionPoint = this.permissions.canShow("ConsentIABCollectionPoint");
    crConstants = crConstants;

    collectionPoint: CreateCollectionPoint;
    createInProgress = false;

    crConsentTypes: IDropDownElement[];
    selectedConsentType: IDropDownElement;

    orgList: IOrganization[];
    selectedOrg: IOrganization;

    userList: IOrgUserAdapted[];
    userListOptions: IOrgUserAdapted[];
    responsibleUser: IOrgUserAdapted;
    currentUser: IUser;

    fullPurposeList: PurposeDetails[];
    purposeOptions: PurposeDetails[];
    selectedPurposes: PurposeDetails[] = [];

    dataElements: EntityItem[];
    dataElementOptions: EntityItem[];
    selectedDataElements: EntityItem[] = [];
    dataElementInput: string;

    languageList: IGetAllLanguageResponse[];
    selectedLanguage: IGetAllLanguageResponse;

    iabConsentToggled = false;
    vendorsList: IPageableOf<IVendorMobile>;
    selectedVendor: IVendorMobile;

    purposeListIAB: EntityItem[];
    purposeListMobile: EntityItem[];
    selectedPurposesIAB: EntityItem[] = [];

    collectionPointType: string;
    collectionPointForm: NgForm;

    overviewOpen = true;
    configurationOpen = true;

    constructor(
        @Inject(StoreToken) public store: IStore,
        private stateService: StateService,
        private collectionPointService: CollectionPointService,
        private permissions: Permissions,
        private consentPurposeService: ConsentPurposeService,
        private cookieIABIntegrationService: CookieIABIntegrationService,
        private consentPreferenceService: ConsentPreferenceService,
        private lookupService: LookupService,
        private translatePipe: TranslatePipe,
    ) {
        this.collectionPointType = stateService.params.type || crConstants.CRTypes.WebForm;
    }

    ngOnInit() {
        this.crConsentTypes = this.collectionPointService.getDropDownOptions(this.crConstants.CRConsentTypesNoCookie);

        this.collectionPointService.getDataElements().then((dataElements: EntityItem[]): void => {
            this.dataElements = dataElements;
            this.dataElementOptions = this.lookupService.filterOptionsBySelections(this.selectedDataElements, this.dataElements, "Label");
        });
        this.consentPurposeService.getActivePurposeFilters().then((purposes: PurposeDetails[]): void => {
            this.fullPurposeList = purposes;
            this.purposeOptions = this.lookupService.filterOptionsBySelections(this.selectedPurposes, this.fullPurposeList, "Id");
        });
        this.consentPurposeService.getAllLanguages().then((languages: IGetAllLanguageResponse[]): void => {
            this.languageList = languages;
        });

        this.collectionPoint = this.stateService.params.collectionPoint;
        if (!this.collectionPoint) {
            this.collectionPoint = new CreateCollectionPoint(this.collectionPointType);
            if (this.collectionPoint.CollectionPointType === this.crConstants.CRTypes.API) {
                this.collectionPoint.ConsentType = this.crConstants.CRConsentTypes.CustomConditionalTrigger;
            } else {
                this.collectionPoint.ConsentType = this.crConstants.CRConsentTypes.FormSubmitOnly;
            }
            this.selectedConsentType = find(this.crConsentTypes, (c: IDropDownElement) => c.value === this.collectionPoint.ConsentType);
            this.collectionPoint.RightToWithdraw = this.translatePipe.transform("DefaultRightToWithdraw");
        }

        this.orgList = getCurrentOrgList(this.store.getState());
        this.collectionPoint.OrganizationId = this.orgList[0].Id;
        this.getOrgUsers(this.orgList[0]);
        this.currentUser = getCurrentUser(this.store.getState());
        this.searchIabPurposes();
        if (this.consentIABCollectionPoint) {
            this.getVendors();
        }
    }

    getOrgUsers(org: IOrganization): IPromise<void> {
        this.selectedOrg = org;
        return this.collectionPointService.getSelectedOrgUsers(org.Id).then((response: IProtocolResponse<IOrgUserAdapted[]>) => {
            if (response.result) {
                this.userList = this.sortUsers(response.data);
                this.userListOptions = this.userList;
                this.responsibleUser = undefined;
            }
        });
    }

    updateSelectedOrg(org: IOrganization) {
        this.selectedOrg = org;
        this.collectionPoint.OrganizationId = org.Id;
        this.collectionPoint.ResponsibleUserId = "";
        this.getOrgUsers(this.selectedOrg);
    }

    updateSelectedLanguage(language: IGetAllLanguageResponse) {
        this.selectedLanguage = language;
    }

    updateSelectedVendor(vendor: IVendorMobile) {
        this.collectionPoint.IabVendorId = vendor.ListId;
        this.selectedVendor = vendor;
    }

    updateConsentType(type: IDropDownElement) {
        this.selectedConsentType = type;
        this.collectionPoint.ConsentType = type.value;
    }

    handleChanged(payload: string, prop: keyof CollectionPointDetails) {
        this.collectionPoint[prop] = payload;
    }

    handleMultiSelect(purposeArray: PurposeListItem[]) {
        this.selectedPurposes = purposeArray;
        this.crConsentTypes = this.collectionPointService.updateConsentTypeList(this.selectedPurposes, this.crConsentTypes);
        const updatedConsentType = this.collectionPointService.modifyConsentType(this.selectedPurposes, this.crConsentTypes, this.collectionPoint.ConsentType);
        this.updateConsentType(updatedConsentType);
    }

    handleMultiSelectIAB(purposeArray: EntityItem[]) {
        this.selectedPurposesIAB = purposeArray;
        this.crConsentTypes = this.collectionPointService.updateConsentTypeList(this.selectedPurposesIAB, this.crConsentTypes);
        const updatedConsentType = this.collectionPointService.modifyConsentType(this.selectedPurposesIAB, this.crConsentTypes, this.collectionPoint.ConsentType);
        this.updateConsentType(updatedConsentType);
    }

    toggleHeader(sectionToggle: boolean) {
        sectionToggle = !sectionToggle;
    }

    onInputChange({ key, value }) {
        if (key === "Enter") {
            this.selectOption({ currentValue: this.selectedPurposes.push(this.purposeOptions[0]) });
            return;
        }
        this.purposeOptions = this.lookupService.filterOptionsByInput(this.selectedPurposes, this.fullPurposeList, value, "Id", "Label");
    }

    onResponsibleUserInputChange({ key, value }) {
        if (key === "Enter") {
            this.selectResponsibleUser({ currentValue: this.userList[0] });
            return;
        }
        this.userListOptions = this.lookupService.filterOptionsByInput([], this.userList, value, "Id", "FullName");
    }

    onDataElementInputChange({ key, value }) {
        if (key === "Enter") {
            this.selectOption({ currentValue: this.selectedDataElements.push(this.dataElementOptions[0]) });
            this.dataElementInput = "";
            return;
        }
        this.dataElementInput = value;
        this.dataElementOptions = this.lookupService.filterOptionsByInput(this.selectedDataElements, this.dataElements, value, "Id", "Label");
    }

    selectOption({ currentValue }) {
        this.selectedPurposes = currentValue;
        this.purposeOptions = this.lookupService.filterOptionsBySelections(this.selectedPurposes, this.fullPurposeList, "Id");
        this.crConsentTypes = this.collectionPointService.updateConsentTypeList(this.selectedPurposes, this.crConsentTypes);
        const updatedConsentType = this.collectionPointService.modifyConsentType(this.selectedPurposes, this.crConsentTypes, this.collectionPoint.ConsentType);
        this.updateConsentType(updatedConsentType);
    }

    selectDataElement({ currentValue }) {
        this.selectedDataElements = currentValue;
        this.dataElementOptions = this.lookupService.filterOptionsBySelections(this.selectedDataElements, this.dataElements, "Label");
    }

    selectOtherDataElement() {
        const newOption: EntityItem = {
            Id: this.dataElementInput,
            Label: this.dataElementInput,
        };
        this.dataElementInput = "";
        this.dataElements.push(newOption);
        this.selectedDataElements = this.selectedDataElements.concat(newOption);
        this.dataElementOptions = this.lookupService.filterOptionsBySelections(this.selectedDataElements, this.dataElements, "Label");
    }

    selectResponsibleUser({ currentValue }) {
        this.responsibleUser = currentValue;
        this.userListOptions = this.userList;
        this.collectionPoint.ResponsibleUserId = this.responsibleUser ? this.responsibleUser.Id : "";
    }

    sortUsers(users: IOrgUserAdapted[]): IOrgUserAdapted[] {
        users = sortBy(users, (user: IOrgUserAdapted): string => {
            return user.FullName.toLowerCase();
        });
        const currentUserIndex = users.findIndex((user: IOrgUserAdapted): boolean => {
            return user.Id === this.currentUser.Id;
        });
        if (currentUserIndex) {
            users.splice(currentUserIndex, 1);
            users = [{ Id: this.currentUser.Id, FullName: this.currentUser.FullName }, ...users];
        }
        return users;
    }

    get isPurposeValid(): boolean {
        if (this.collectionPointType === this.crConstants.CRTypes.Mobile) {
            if (this.iabConsentToggled && isEmpty(this.selectedPurposesIAB)) {
                return false;
            }
        } else if (isEmpty(this.selectedPurposes)) {
            return false;
        }
        return true;
    }

    get emptyField(): boolean {
        if (this.collectionPoint.Name && this.collectionPoint.Description && this.collectionPoint.SubjectIdentifier) {
            return !(
                this.collectionPoint.Name.trim()
                && this.collectionPoint.Description.trim()
                && this.collectionPoint.SubjectIdentifier.trim()
            );
        } else {
            return true;
        }
    }

    toggleIabConsent() {
        this.iabConsentToggled = !this.iabConsentToggled;
        this.selectedPurposes = [];
    }

    createCollectionPoint() {
        if (this.collectionPoint.PrivacyPolicyUrl && !this.collectionPointService.validateLink(this.collectionPoint.PrivacyPolicyUrl)) {
            OneAlert("", this.translatePipe.transform("MissingPrivacyPolicyLink"));
            return;
        }
        this.createInProgress = true;

        if (this.collectionPointType === this.crConstants.CRTypes.Mobile && this.iabConsentToggled) {
            this.collectionPoint.PurposeId = this.collectionPointService.idsToString(this.selectedPurposesIAB);
        } else {
            this.collectionPoint.PurposeId = this.collectionPointService.idsToString(this.selectedPurposes);
        }

        if (this.dataElementsEnabled) {
            this.collectionPoint.DataElements = this.selectedDataElements.map((item: EntityItem): string => item.Label);
        }

        if (this.collectionPointType === this.crConstants.CRTypes.Mobile) {
            const preferenceCenterPayload: NewPreferenceCenter = {
                Name: this.collectionPoint.Name,
                Description: this.collectionPoint.Description,
                OrganizationId: this.collectionPoint.OrganizationId,
                Language: this.selectedLanguage.Code,
                PurposeId: this.collectionPoint.PurposeId,
            };
            this.createPreferenceCenter(preferenceCenterPayload);
        } else {
            this.continueCollectionPointProcess();
        }
    }

    goToCollectionPoints() {
        this.stateService.go("zen.app.pia.module.consentreceipt.views.collections.list");
    }

    get isPrivacyPolicyValid(): boolean {
        return this.collectionPoint && (!this.collectionPoint.PrivacyPolicyUrl || this.collectionPointService.validateLink(this.collectionPoint.PrivacyPolicyUrl));
    }

    private getVendors = (num: number = 0, size: number = 2000) => {
        this.cookieIABIntegrationService.getVendorsForMobile(num, size).then(
            (response: IPageableOf<IVendorMobile>) => {
                this.vendorsList = response;
            });
    }

    private createPreferenceCenter(prefPayload: NewPreferenceCenter) {
        this.consentPreferenceService.createPreferenceCenter(prefPayload).then((result: PreferenceCenter) => {
            if (result) {
                this.continueCollectionPointProcess(result.Id);
            } else {
                this.createInProgress = false;
            }
        });
    }

    private continueCollectionPointProcess(preferenceCenterId?: string) {
        if (this.collectionPointType === this.crConstants.CRTypes.Mobile) {
            this.collectionPoint.PurposeId = this.collectionPointService.idsToString(this.purposeListMobile);
        }
        this.collectionPointService.createCollectionPoint(this.collectionPoint).then((result: string) => {
            this.createInProgress = false;
            if (result && this.collectionPointType === this.crConstants.CRTypes.Mobile) {
                this.stateService.go("zen.app.pia.module.consentreceipt.views.preferencecenters.details", { Id: preferenceCenterId });
            } else if (result) {
                this.stateService.go("zen.app.pia.module.consentreceipt.views.collections.list");
            }
        });
    }

    private searchIabPurposes(filters: PurposeFilters = {}) {
        this.consentPurposeService.getPurposes(filters, 0, 20, "Id,desc", crConstants.CRPurposeTypes.Iab)
            .then((items: PurposeList) => {
                if (items.content) {
                    this.purposeListIAB = items.content.filter((item) => item.Type === crConstants.CRPurposeTypes.Iab && item.Status === crConstants.CRPurposeStatus.Active);
                    this.searchMobilePurposes();
                }
            });
    }

    private searchMobilePurposes(filters: PurposeFilters = {}) {
        this.consentPurposeService.getPurposes(filters, 0, 5, "Id,desc", crConstants.CRPurposeTypes.Mobile)
            .then((items: PurposeList) => {
                if (items.content) {
                    this.purposeListMobile = items.content.filter((item) => item.Type === crConstants.CRPurposeTypes.Mobile);
                }
            });
    }
}
