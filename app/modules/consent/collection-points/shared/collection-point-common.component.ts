// Third Party
import { Input } from "@angular/core";
import { StateService } from "@uirouter/core";

// Constants
import crConstants from "consentModule/shared/cr-constants";

// Interfaces
import { IStringMap } from "interfaces/generic.interface";

// Models
import { CollectionPointDetails } from "crmodel/collection-point-details";
import { PurposeDetails } from "crmodel/purpose-details";
import { CollectionPointPurposeReport } from "crmodel/collection-point-report";

// Services
import { CollectionPointService } from "consentModule/shared/services/collection-point.service";

export abstract class CrCollectionPointCommon implements ng.IComponentController {
    crConstants = crConstants;
    collectionPointId: string;
    collectionPointVersion: number;
    @Input() collectionPoint: CollectionPointDetails;

    constructor(
        protected stateService: StateService,
        protected collectionPointService: CollectionPointService,
    ) {
        this.collectionPointId = this.stateService.params.Id;
        this.collectionPointVersion = Number(stateService.params.version || 1);
    }

    translateConst(value: string, constType: IStringMap<string>): string {
        return this.collectionPointService.translateConstValue(value, constType);
    }

    getDoubleOptInStatusString(status: boolean): string {
        return status ? crConstants.CRDoubleOptInStatus.Active : crConstants.CRDoubleOptInStatus.Inactive;
    }

    transactionStatusStyle(status: string): string {
        return crConstants.CRTransactionStatusStyles[status];
    }

    doubleOptInStatusStyle(status: string): string {
        return crConstants.CRDoubleOptInStatusStyles[status];
    }

    handleChanged(payload: string, prop: keyof CollectionPointDetails) {
        this.collectionPoint[prop] = payload;
    }

    get isExplicitConsent(): boolean {
        return this.collectionPoint &&
            (
                this.collectionPoint.ConsentType === crConstants.CRConsentTypes.OptInCheckboxFormSubmit ||
                this.collectionPoint.ConsentType === crConstants.CRConsentTypes.OptOutCheckboxSubmit ||
                this.collectionPoint.ConsentType === crConstants.CRConsentTypes.CustomConditionalTrigger
            );
    }

    get isSinglePurposeFormSubmit(): boolean {
        return this.collectionPoint && this.collectionPoint.ConsentType === crConstants.CRConsentTypes.FormSubmitOnly &&
            this.collectionPoint.Purposes && this.collectionPoint.Purposes.length === 1;
    }

    get collectionPointType(): string {
        return this.collectionPoint ? this.collectionPoint.CollectionPointType : null;
    }

    get isMobileType(): boolean {
        return this.collectionPoint && this.collectionPoint.CollectionPointType === crConstants.CRTypes.Mobile;
    }

    goToCollectionPoints() {
        this.stateService.go("zen.app.pia.module.consentreceipt.views.collections.list");
    }

    goToPurpose(purpose: PurposeDetails | CollectionPointPurposeReport) {
        this.stateService.go("zen.app.pia.module.consentreceipt.views.purposes.details", { Id: purpose.Id, version: purpose.Version });
    }
}
