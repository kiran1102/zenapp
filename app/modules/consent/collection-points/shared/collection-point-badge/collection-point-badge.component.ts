// Third Party
import {
    Component,
    Input,
} from "@angular/core";

// Constants
import crConstants from "consentModule/shared/cr-constants";

@Component({
    selector: "cr-collection-point-badge",
    templateUrl: "./collection-point-badge.component.html",
})
export class CrCollectionPointBadgeComponent {
    @Input() collectionPointType: string;
    @Input() creating: boolean;
    crConstants = crConstants;
}
