// Angular
import { NgModule } from "@angular/core";

// Components
import { CrCollectionPointBadgeComponent } from "consentModule/collection-points/shared/collection-point-badge/collection-point-badge.component";
import { CrCollectionPointHeaderComponent } from "consentModule/collection-points/shared/collection-point-header/collection-point-header.component";

// Modules
import { ConsentSharedModule } from "modules/consent/shared/consent-shared.module";

@NgModule({
    imports: [
        ConsentSharedModule,
    ],
    exports: [
        ConsentSharedModule,
        CrCollectionPointBadgeComponent,
        CrCollectionPointHeaderComponent,
    ],
    declarations: [
        CrCollectionPointBadgeComponent,
        CrCollectionPointHeaderComponent,
    ],
})
export class CollectionPointsSharedModule { }
