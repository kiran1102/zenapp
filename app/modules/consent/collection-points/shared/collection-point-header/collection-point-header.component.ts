// Third Party
import {
    Component,
    Input,
} from "@angular/core";
import { StateService } from "@uirouter/core";

// Services
import { CollectionPointService } from "consentModule/shared/services/collection-point.service";

// Components and Constants
import { CrCollectionPointCommon } from "consentModule/collection-points/shared/collection-point-common.component";
import crConstants from "consentModule/shared/cr-constants";

@Component({
    selector: "cr-collection-point-header",
    templateUrl: "./collection-point-header.component.html",
})
export class CrCollectionPointHeaderComponent extends CrCollectionPointCommon {
    @Input() title: string;
    crConstants = crConstants;

    constructor(
        protected stateService: StateService,
        protected collectionPointService: CollectionPointService,
    ) {
        super(stateService, collectionPointService);
    }
}
