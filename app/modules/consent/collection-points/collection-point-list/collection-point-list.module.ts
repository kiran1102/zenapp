// Angular
import { NgModule } from "@angular/core";

// Components
import { CrCollectionPointListComponent } from "consentModule/collection-points/collection-point-list/collection-point-list.component";
import { CrCollectionPointFiltersComponent } from "consentModule/collection-points/collection-point-list/collection-point-filters/collection-point-filters.component";

// Modules
import { CollectionPointsSharedModule } from "consentModule/collection-points/shared/collection-points-shared.module";

@NgModule({
    imports: [
        CollectionPointsSharedModule,
    ],
    declarations: [
        CrCollectionPointListComponent,
        CrCollectionPointFiltersComponent,
    ],
    entryComponents: [
        CrCollectionPointListComponent,
    ],
})
export class CollectionPointListModule { }
