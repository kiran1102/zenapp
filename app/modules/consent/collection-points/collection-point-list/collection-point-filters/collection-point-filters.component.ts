// Third Party
import {
    Component,
    Input,
    OnChanges,
} from "@angular/core";

import { orderBy } from "lodash";

// Interfaces
import { IVersionListItem } from "interfaces/consent/cr-versions.interface";
import { IOTLookupSingleSelection } from "sharedModules/components/ot-template-select/ot-template-select.component";

// Models and Components
import { EntityItem } from "crmodel/entity-item";
import {
    CollectionPointFilters,
    CollectionPointFiltersSelectionsOptions,
} from "crmodel/collection-point-filters";
import { CrFiltersBase } from "modules/consent/shared/cr-filters-base/cr-filters-base.component";

// Services
import { IDropDownElement } from "consentModule/shared/services/consent-base.service";
import { CollectionPointService } from "consentModule/shared/services/collection-point.service";
import { ConsentPurposeService } from "consentModule/shared/services/cr-purpose.service";

// Constants
import crConstants from "consentModule/shared/cr-constants";

@Component({
    selector: "cr-collection-point-filters",
    templateUrl: "./collection-point-filters.component.html",
})
export class CrCollectionPointFiltersComponent extends CrFiltersBase<CollectionPointFilters> implements OnChanges {
    @Input() filters: CollectionPointFilters;
    @Input() crPurposes: IDropDownElement[];

    constructor(
        private readonly collectionPointService: CollectionPointService,
        private readonly consentPurposeService: ConsentPurposeService,
    ) {
        super();
    }

    ngOnChanges() {
        this.populateFilters();
        this.filters.name = sessionStorage.getItem("ConsentCollectionPointNameFilter") || "";
        const result: CollectionPointFiltersSelectionsOptions = this.collectionPointService.getSessionStorageFilters(this.crPurposes, this.options);
        if (result) {
            this.selections = result.selections;
            this.options = result.options;
        }
    }

    handleApply() {
        this.isDrawerOpen = false;
        this.collectionPointService.setSessionStorageFilters(this.filters);
        super.handleApply();
    }

    handleClear() {
        super.handleClear();
        this.filters.name = "";
        this.filters.purposeGuid = "";
        this.collectionPointService.clearSessionStorageFilters();
    }

    selectPurpose(selection: IOTLookupSingleSelection<IDropDownElement>) {
        this.selectOption("purposeGuid", selection.currentValue);
        this.selectOption("purposeVersion", null);
        this.getFilteredPurposeVersions(selection.currentValue);
    }

    protected populateFilters() {
        this.options.purposeGuid = this.crPurposes;
        this.options.collectionPointType = this.collectionPointService.getDropDownOptions(crConstants.CRFilterTypes);
        this.options.consentType = this.collectionPointService.getDropDownOptions(crConstants.CRConsentTypesNoCookie);
        this.options.status = this.collectionPointService.getDropDownOptions(crConstants.CRCollectionPointStatus)
            .filter((option: IDropDownElement): boolean => option.value !== crConstants.CRCollectionPointStatus.Retired);
    }

    private getFilteredPurposeVersions(selectedPurpose: IDropDownElement) {
        if (selectedPurpose && selectedPurpose.value) {
            this.consentPurposeService.getFilteredPurposeVersions(selectedPurpose.value).then((result: EntityItem[]) => {
                if (result && result.length > 0) {
                    this.options.purposeVersion = orderBy(result, "Version", "desc").map((item: IVersionListItem): IDropDownElement => {
                        return {
                            label: `${item.Label} (${this.consentPurposeService.getVersionLabel(item.Version)})`,
                            value: item.Id,
                            version: item.Version,
                        };
                    });
                }
            });
        } else {
            this.options.purposeVersion = null;
        }
    }
}
