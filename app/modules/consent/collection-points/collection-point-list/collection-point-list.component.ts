import { Component } from "@angular/core";
import { StateService } from "@uirouter/core";

// Interfaces
import { IDropdownOption } from "interfaces/one-dropdown.interface";

// Models and Components
import { CollectionPointFilters } from "crmodel/collection-point-filters";
import {
    CollectionPointList,
    CollectionPointListItem,
} from "crmodel/collection-point-list";
import { PurposeDetails } from "crmodel/purpose-details";
import { ColumnDefinition } from "crmodel/column-definition";

// Services
import { IDropDownElement } from "consentModule/shared/services/consent-base.service";
import { ConsentPurposeService } from "consentModule/shared/services/cr-purpose.service";
import { CollectionPointService } from "consentModule/shared/services/collection-point.service";

// Components and Constants
import crConstants from "consentModule/shared/cr-constants";
import { CrCollectionPointCommon } from "consentModule/collection-points/shared/collection-point-common.component";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

@Component({
    selector: "cr-collection-point-list",
    templateUrl: "./collection-point-list.component.html",
})
export class CrCollectionPointListComponent extends CrCollectionPointCommon {
    tableData: CollectionPointList;
    pageSize = 20;
    searchInProgress = false;
    columns: ColumnDefinition[];
    filters: CollectionPointFilters;
    crPurposes: IDropDownElement[];

    constructor(
        protected collectionPointService: CollectionPointService,
        protected stateService: StateService,
        private consentPurposeService: ConsentPurposeService,
        private translatePipe: TranslatePipe,
    ) {
        super(stateService, collectionPointService);
    }

    ngOnInit() {
        this.filters = this.collectionPointService.handleFilters(this.stateService.params.purposeId, this.stateService.params.purposeVersion);
        this.columns = [
            {
                columnName: this.translatePipe.transform("Name"),
                cellValueKey: "Name",
                cellClass: "max-width-50",
                columnType: "idLink",
                route: "zen.app.pia.module.consentreceipt.views.collections.details",
                routeVersionField: "Version",
            },
            {
                columnName: this.translatePipe.transform("Type"),
                rowToText: (row: CollectionPointListItem): string => this.translateConst(row.CollectionPointType, crConstants.CRTypes),
            },
            {
                columnName: this.translatePipe.transform("TypeOfConsent"),
                rowToText: (row: CollectionPointListItem): string => this.translateConst(row.ConsentType, crConstants.CRConsentTypes),
            },
            {
                columnName: this.translatePipe.transform("Status"),
                cellValueKey: "Status",
                columnType: "statusTag",
                rowToText: (row: CollectionPointListItem): string => this.translateConst(row.Status, crConstants.CRCollectionPointStatus),
            },
            {
                columnName: this.translatePipe.transform("DoubleOptIn"),
                columnType: "doubleOptIn",
                rowToText: (row: CollectionPointListItem): string => this.translateConst(this.getDoubleOptInStatusString(row.DoubleOptIn), crConstants.CRDoubleOptInStatus),
            },
            {
                columnName: this.translatePipe.transform("Version"),
                cellValueKey: "Version",
                columnType: "versionTag",
                rowToText: (row: CollectionPointListItem): string => `V${row.Version}`,
            },
            {
                columnName: this.translatePipe.transform("UniqueSubjectId"),
                cellValueKey: "SubjectIdentifier",
            },
            {
                columnName: this.translatePipe.transform("CreatedOn"),
                cellValueKey: "CreateDate",
                columnType: "timestamp",
            },
            {
                columnName: this.translatePipe.transform("FirstReceiptOn"),
                cellValueKey: "FirstReceiptOn",
                columnType: "timestamp",
            },
            {
                columnName: this.translatePipe.transform("NoOfReceipts"),
                cellValueKey: "ReceiptCount",
                columnType: "number",
            },
            {
                columnType: "actions",
                getOptions: (row: CollectionPointListItem): IDropdownOption[] => this.collectionPointOptions(row),
            },
        ];
        this.consentPurposeService.getActivePurposeFilters(true).then((purposeFilters: PurposeDetails[]) => {
            if (purposeFilters && purposeFilters.length > 0) {
                this.crPurposes = purposeFilters.map((item: PurposeDetails): IDropDownElement => {
                    return { label: item.Label, value: item.Id, version: item.Version };
                });
            }
        });
        this.searchCollectionPoints(this.filters);
    }

    loadPage(page: number) {
        this.searchCollectionPoints(this.filters, page);
    }

    searchCollectionPoints(filters: CollectionPointFilters = {}, page: number = 0) {
        this.filters = filters;
        this.searchInProgress = true;
        this.collectionPointService.getCollectionPointList(filters, page, this.pageSize)
            .then((collectionPoints: CollectionPointList) => {
                this.tableData = collectionPoints;
                this.searchInProgress = false;
        });
    }

    collectionPointOptions(collectionPoint: CollectionPointListItem): IDropdownOption[] {
            const cpRoute = "zen.app.pia.module.consentreceipt.views.collections.details";
            const actionArray: IDropdownOption[] = [
                {
                    id: "details",
                    text: this.translatePipe.transform("Details"),
                    action: (): Promise<any> => this.stateService.go(cpRoute, { Id: collectionPoint.Id, tab: "details", version: collectionPoint.Version }),
                },
            ];
            if (collectionPoint.CollectionPointType === crConstants.CRTypes.WebForm) {
                actionArray.push(
                    {
                        id: "transactions",
                        text: this.translatePipe.transform("Transactions"),
                        action: (): Promise<any> => this.stateService.go("zen.app.pia.module.consentreceipt.views.transactions.list", { collectionPointId: collectionPoint.Id }),
                    },
                    {
                        id: "sdk",
                        text: this.translatePipe.transform("SDK"),
                        action: (): Promise<any> => this.stateService.go(cpRoute, { Id: collectionPoint.Id, tab: "sdk", version: collectionPoint.Version }),
                    },
                    {
                        id: "api",
                        text: this.translatePipe.transform("APIIntegration"),
                        action: (): Promise<any> => this.stateService.go(cpRoute, { Id: collectionPoint.Id, tab: "api", version: collectionPoint.Version }),
                    },
                    {
                        id: "preview",
                        text: this.translatePipe.transform("Preview"),
                        action: (): Promise<any> => this.stateService.go(cpRoute, { Id: collectionPoint.Id, tab: "preview", version: collectionPoint.Version }),
                    },
                );
            } else if (collectionPoint.CollectionPointType === crConstants.CRTypes.API) {
                actionArray.push(
                    {
                        id: "api",
                        text: this.translatePipe.transform("APIIntegration"),
                        action: (): Promise<any> => this.stateService.go(cpRoute, { Id: collectionPoint.Id, tab: "api", version: collectionPoint.Version }),
                    },
                );
            }
            return actionArray;
    }

    goToRoute(cell: any) {
        this.stateService.go(cell.route, cell.params);
    }

    createCollectionPoint() {
        this.stateService.go("zen.app.pia.module.consentreceipt.views.collections.type");
    }
}
