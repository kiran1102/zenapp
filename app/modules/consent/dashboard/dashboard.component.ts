import {
    Component,
    OnInit,
} from "@angular/core";
import { StateService } from "@uirouter/core";

// Models
import { IDashboardData } from "crmodel/dashboard-data";

// Services
import { CollectionPointService } from "consentModule/shared/services/collection-point.service";
import { ConsentDataSubjectService } from "consentModule/shared/services/cr-data-subject.service";
import { ConsentDashboardService } from "./cr-dashboard.service";

@Component({
    selector: "cr-dashboard",
    templateUrl: "./dashboard.component.html",
})
export class CrDashboardComponent implements OnInit {
    isLoading: boolean;
    dashboardData: IDashboardData;
    canClickCollectionPointDetails: boolean;

    constructor(
        private stateService: StateService,
        private consentDashboardService: ConsentDashboardService,
        private collectionPointService: CollectionPointService,
        private consentDataSubjectService: ConsentDataSubjectService,
    ) {
    }

    ngOnInit() {
        this.isLoading = true;
        this.consentDashboardService.getDashboard().then((response: IDashboardData): void => {
            this.dashboardData = response;
            this.isLoading = false;
        });
    }

    goToPurposes() {
        sessionStorage.removeItem("ConsentPurposeNameFilter");
        this.stateService.go("zen.app.pia.module.consentreceipt.views.purposes.list");
    }

    goToCollectionPoints() {
        this.collectionPointService.clearSessionStorageFilters();
        this.stateService.go("zen.app.pia.module.consentreceipt.views.collections.list");
    }

    goToDataSubjects() {
        this.consentDataSubjectService.clearSessionStorageFilters();
        this.stateService.go("zen.app.pia.module.consentreceipt.views.datasubjects.list");
    }
}
