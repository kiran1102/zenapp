// Angular
import { NgModule } from "@angular/core";

// Components
import { CrDashboardComponent } from "consentModule/dashboard/dashboard.component";

// Modules
import { ConsentSharedModule } from "consentModule/shared/consent-shared.module";

// Services
import { ConsentDashboardService } from "consentModule/dashboard/cr-dashboard.service";

@NgModule({
    imports: [
        ConsentSharedModule,
    ],
    declarations: [
        CrDashboardComponent,
    ],
    entryComponents: [
        CrDashboardComponent,
    ],
    providers: [
        ConsentDashboardService,
    ],
})
export class CrDashboardModule { }
