// Third Party
import {
    sum,
    sumBy,
    values,
} from "lodash";
import { Injectable, Inject } from "@angular/core";

// Interfaces
import { IProtocolResponse, IProtocolConfig } from "interfaces/protocol.interface";
import { IHorseshoeGraphData } from "interfaces/one-horseshoe-graph.interface";

// Models
import {
    IDashboardData,
    TransactionSummary,
    DashboardCollectionPointSummary,
    DashboardPurposeSummary,
} from "crmodel/dashboard-data";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

// Services
import { ProtocolService } from "modules/core/services/protocol.service";
import { ConsentBaseService } from "consentModule/shared/services/consent-base.service";
import { Principal } from "sharedModules/services/helper/principal.service";

// Constants
import crConstants from "consentModule/shared/cr-constants";

type DashboardResponse = TransactionSummary | DashboardCollectionPointSummary[] | DashboardPurposeSummary[];

@Injectable()
export class ConsentDashboardService extends ConsentBaseService {
    colors: string[] = [
        "green",
        "light-red",
        "blue",
        "orange",
        "lagoon",
        "yellow",
    ];

    constructor(
        protected OneProtocol: ProtocolService,
        protected translatePipe: TranslatePipe,
        protected principal: Principal,
    ) {
        super(OneProtocol, translatePipe, principal);
    }

    getDashboard(): Promise<IDashboardData> {
        const transactionSummaryPromise: ng.IPromise<TransactionSummary> = this.getTransactionSummary();
        const collectionPointSummaryPromise: ng.IPromise<DashboardCollectionPointSummary[]> = this.getCollectionPointSummary();
        const purposeSummaryPromise: ng.IPromise<DashboardPurposeSummary[]> = this.getPurposeSummary();

        return Promise.all([purposeSummaryPromise, collectionPointSummaryPromise, transactionSummaryPromise])
            .then((responses: DashboardResponse[]): IDashboardData => {
                return {
                    Purposes: this.generatePurposesGraph(responses[0] as DashboardPurposeSummary[]),
                    CollectionPoints: this.generateCollectionPointGraph(responses[1] as DashboardCollectionPointSummary[]),
                    Statuses: this.generateStatusGraph(responses[2] as TransactionSummary),
                };
            });
    }

    getTransactionSummary(): ng.IPromise<TransactionSummary> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("GET", `/api/consentmanager/v1/dashboards/statussummary`);
        return this.OneProtocol.http(config)
            .then((response: IProtocolResponse<TransactionSummary>): TransactionSummary => response.result ? response.data : null);
    }

    getCollectionPointSummary(): ng.IPromise<DashboardCollectionPointSummary[]> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("GET", `/api/consentmanager/v1/dashboards/collectionpointsummary`);
        return this.OneProtocol.http(config)
            .then((response: IProtocolResponse<DashboardCollectionPointSummary[]>): DashboardCollectionPointSummary[] => response.result ? response.data : null);
    }

    getPurposeSummary(): ng.IPromise<DashboardPurposeSummary[]> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("GET", `/api/consentmanager/v1/dashboards/purposesummary`);
        return this.OneProtocol.http(config)
            .then((response: IProtocolResponse<DashboardPurposeSummary[]>): DashboardPurposeSummary[] => response.result ? response.data : null);
    }

    private generatePurposesGraph(summary: DashboardPurposeSummary[]): IHorseshoeGraphData {
        const totalCount: number = sumBy(summary, "Count");

        if (totalCount === 0) {
            return {
                ...this.emptyGraph(),
                title: 0,
            };
        }

        const title = this.shortenLargeNumber(totalCount);
        const graph: IHorseshoeGraphData = { series: [], legend: [], title };

        for (let i = 0; i < summary.length; i++) {
            const element = summary[i];
            graph.series.push({
                graphClass: ("stroke-" + this.colors[i]),
                label: element.PurposeName,
                value: element.Count / totalCount,
            });
            graph.legend.push({
                class: ("background-" + this.colors[i]),
                label: element.PurposeName,
            });
        }

        return graph;
    }

    private generateCollectionPointGraph(summary: DashboardCollectionPointSummary[]): IHorseshoeGraphData {
        const EXCLUSION_COLLECTION_POINT_TYPES = [
            crConstants.CRTypes.PreferenceCenter,
            crConstants.CRTypes.NotificationOptOut,
        ];

        const collectionPointsFilter = (type: string) => {
            return EXCLUSION_COLLECTION_POINT_TYPES.includes(type) === false;
        };

        const collectionPoints = summary ? summary.filter((collectionPoint) => {
            return collectionPointsFilter(collectionPoint.CollectionPointType);
        }) : [];

        if (!collectionPoints || collectionPoints.length === 0) {
            return {
                ...this.emptyGraph(),
                title: 0,
            };
        }

        const totalCount: number = sumBy(collectionPoints, "Count");
        const title = this.shortenLargeNumber(totalCount);
        const graph: IHorseshoeGraphData = { series: [], legend: [], title };

        for (let i = 0; i < collectionPoints.length; i++) {
            const element = collectionPoints[i];
            const translatedLabel: string = this.translateConstValue(element.CollectionPointType, crConstants.CRTypes);
            graph.series.push({
                graphClass: ("stroke-" + this.colors[i]),
                label: translatedLabel,
                value: element.Count / totalCount,
            });
            graph.legend.push({
                label: translatedLabel,
                class: ("background-" + this.colors[i]),
            });
        }

        return graph;
    }

    private generateStatusGraph(summary: TransactionSummary): IHorseshoeGraphData {
        if (!summary || !summary.ActiveTransactionCount) {
            return {
                ...this.emptyGraph(),
                title: 0,
            };
        }

        const totalCount: number = sum(values(summary));
        const title = this.shortenLargeNumber(totalCount);

        const graph: IHorseshoeGraphData = {
            series: [
                {
                    graphClass: "stroke-blue",
                    label: this.translatePipe.transform("Pending"),
                    value: summary.PendingTransactionCount / totalCount,
                },
                {
                    graphClass: "stroke-green",
                    label: this.translatePipe.transform("ActiveConsentCount"),
                    value: summary.ActiveTransactionCount / totalCount,
                },
                {
                    graphClass: "stroke-lagoon",
                    label: this.translatePipe.transform("ExpiredConsentCount"),
                    value: summary.ExpiredTransactionCount / totalCount,
                },
                {
                    graphClass: "stroke-light-red",
                    label: this.translatePipe.transform("WithdrawnConsentCount"),
                    value: summary.WithdrawTransactionCount / totalCount,
                },
                {
                    graphClass: "stroke-grey-2",
                    label: this.translatePipe.transform("NoConsentCount"),
                    value: summary.NoConsentTransactionCount / totalCount,
                },
                {
                    graphClass: "stroke-orange",
                    label: this.translatePipe.transform("OptedOut"),
                    value: summary.OptedOutTransactionCount / totalCount,
                },
            ],
            legend: [
                {
                    label: this.translatePipe.transform("Pending"),
                    class: "background-blue",
                }, {
                    label: this.translatePipe.transform("ActiveConsentCount"),
                    class: "background-green",
                }, {
                    label: this.translatePipe.transform("ExpiredConsentCount"),
                    class: "background-lagoon",
                }, {
                    label: this.translatePipe.transform("WithdrawnConsentCount"),
                    class: "background-light-red",
                }, {
                    label: this.translatePipe.transform("NoConsentCount"),
                    class: "background-grey-2",
                },
                {
                    label: this.translatePipe.transform("OptedOut"),
                    class: "background-orange",
                },
            ],
            title,
        };

        return graph;
    }
}
