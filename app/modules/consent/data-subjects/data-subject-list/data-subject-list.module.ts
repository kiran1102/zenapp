// Angular
import { NgModule } from "@angular/core";

// Components
import { CrDataSubjectListComponent } from "consentModule/data-subjects/data-subject-list/data-subject-list.component";
import { CrDataSubjectFiltersComponent } from "consentModule/data-subjects/data-subject-list/data-subject-filters/data-subject-filters.component";

// Modules
import { DataSubjectsSharedModule } from "consentModule/data-subjects/shared/data-subjects-shared.module";

@NgModule({
    imports: [
        DataSubjectsSharedModule,
    ],
    declarations: [
        CrDataSubjectListComponent,
        CrDataSubjectFiltersComponent,
    ],
    entryComponents: [
        CrDataSubjectListComponent,
    ],
})
export class DataSubjectListModule { }
