// Third Party
import {
    Component,
    Inject,
} from "@angular/core";
import { StateService } from "@uirouter/core";

// Interfaces
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import { IStringMap } from "interfaces/generic.interface";

// Models
import { ColumnDefinition } from "crmodel/column-definition";
import { DataSubjectList } from "crmodel/data-subject-list";
import { PurposeDetails } from "crmodel/purpose-details";
import { DataSubjectFilters } from "crmodel/data-subject-filters";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

// Services
import { ConsentDataSubjectService } from "consentModule/shared/services/cr-data-subject.service";
import { ConsentPurposeService } from "consentModule/shared/services/cr-purpose.service";
import { IDropDownElement } from "consentModule/shared/services/consent-base.service";
import crConstants from "consentModule/shared/cr-constants";
import { Permissions } from "modules/shared/services/helper/permissions.service";
import { ToastService } from "@onetrust/vitreus";

@Component({
    selector: "cr-data-subject-list",
    templateUrl: "./data-subject-list.component.html",
})
export class CrDataSubjectListComponent {
    crConstants = crConstants;

    crPurposes: IDropDownElement[];
    columns: ColumnDefinition[];
    tableData: DataSubjectList;
    filters: DataSubjectFilters = {};
    searchInProgress = true;
    canExport: boolean;
    hidePageCounts: boolean;
    readonly pageSize = 20;

    constructor(
        @Inject("$rootScope") readonly $rootScope: IExtendedRootScopeService,
        private stateService: StateService,
        private readonly consentDataSubjectService: ConsentDataSubjectService,
        private readonly consentPurposeService: ConsentPurposeService,
        private readonly toastService: ToastService,
        private readonly permissions: Permissions,
        private readonly translatePipe: TranslatePipe,
    ) {}

    ngOnInit() {
        this.filters = this.consentDataSubjectService.handleFilters(this.stateService.params.purposeId);
        this.columns = [
            {
                columnName: this.translatePipe.transform("DataSubject"),
                cellValueKey: "Identifier",
                columnType: "idLink",
                route: "zen.app.pia.module.consentreceipt.views.datasubjects.details",
            },
            {
                columnName: this.translatePipe.transform("Transactions"),
                cellValueKey: "TransactionSummary.TotalTransactionCount",
                columnType: "number",
            },
            {
                columnName: this.translatePipe.transform("FirstTransactionDate"),
                cellValueKey: "TransactionSummary.FirstTransactionDate",
                columnType: "timestamp",
            },
            {
                columnName: this.translatePipe.transform("LastTransactionDate"),
                cellValueKey: "TransactionSummary.LastTransactionDate",
                columnType: "timestamp",
            },
        ];
        this.canExport = this.permissions.canShow("ConsentExportDataSubjects");
        this.hidePageCounts = this.permissions.canShow("ConsentHidePageCountsForBigTables");
        this.consentPurposeService.getActivePurposeFilters(true).then((purposeFilters: PurposeDetails[]) => {
            if (purposeFilters && purposeFilters.length > 0) {
                this.crPurposes = purposeFilters.map((item: PurposeDetails): IDropDownElement => {
                    return { label: item.Label, value: item.Id, version : item.Version };
                });
            }
            this.searchDataSubjects(this.filters);
        });
    }

    loadPage(page: number) {
        this.searchDataSubjects(this.filters, page);
    }

    goToRoute(cell: any) {
        this.stateService.go(cell.route, cell.params);
    }

    searchDataSubjects(filters: DataSubjectFilters = {}, page?: number) {
        this.filters = filters;
        this.filters.includeCounts = !this.hidePageCounts;
        this.searchInProgress = true;
        this.consentDataSubjectService.getDataSubjects(this.filters, page, this.pageSize)
            .then((items: DataSubjectList) => {
                if (!items.numberOfElements && items.number > 1) {
                    this.toastService.show(this.translatePipe.transform("NoResultsFound"), this.translatePipe.transform("EndOfList"), "warning");
                } else {
                    this.tableData = items;
                }
                this.searchInProgress = false;
            });
        }

    receiptStatusStyle(status: string): string {
        return this.crConstants.CRReceiptStatusStyles[status];
    }

    translateConst(value: string, constType: IStringMap<string>): string {
        return this.consentDataSubjectService.translateConstValue(value, constType);
    }
}
