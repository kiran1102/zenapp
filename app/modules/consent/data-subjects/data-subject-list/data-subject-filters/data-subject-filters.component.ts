// Third Party
import {
    filter,
    isEmpty,
    uniqBy,
    orderBy,
} from "lodash";
import {
    Component,
    Input,
} from "@angular/core";

// Interfaces
import { IVersionListItem } from "interfaces/consent/cr-versions.interface";

// Models and Components
import { PurposeDetails } from "crmodel/purpose-details";
import { EntityItem } from "crmodel/entity-item";
import { TopicListItem } from "crmodel/topic-list";
import {
    DataSubjectFilters,
    DataSubjectFiltersSelectionsOptions,
} from "crmodel/data-subject-filters";
import { CrFiltersBase } from "modules/consent/shared/cr-filters-base/cr-filters-base.component";

// Services
import { CollectionPointService } from "consentModule/shared/services/collection-point.service";
import { ConsentPurposeService } from "consentModule/shared/services/cr-purpose.service";
import { IDropDownElement } from "consentModule/shared/services/consent-base.service";
import { ConsentDataSubjectService } from "consentModule/shared/services/cr-data-subject.service";
import { Permissions } from "modules/shared/services/helper/permissions.service";

@Component({
    selector: "cr-data-subject-filters",
    templateUrl: "./data-subject-filters.component.html",
})
export class CrDataSubjectFiltersComponent extends CrFiltersBase<DataSubjectFilters> {
    @Input() filters: DataSubjectFilters;
    @Input() crPurposes: IDropDownElement[];

    purposeId: string;
    dataElementValue: string;
    loadingTopics = false;
    showTopicFilter = this.permissions.canShow("ConsentReceiptPreferences");
    isDrawerOpen = false;

    selectedPurpose: IDropDownElement;
    selectedPurposeVersion: IDropDownElement;
    dataElements: IDropDownElement[];
    dataElement: IDropDownElement;

    constructor(
        private consentPurposeService: ConsentPurposeService,
        private  consentDataSubjectService: ConsentDataSubjectService,
        private  collectionPointService: CollectionPointService,
        private  permissions: Permissions,
    ) {
        super();
    }

    ngOnInit() {
        this.populateFilters();
    }

    ngOnChanges() {
        this.filters.identifier = sessionStorage.getItem("ConsentDataSubjectIdentifierFilter") || "";
        const result: DataSubjectFiltersSelectionsOptions = this.consentDataSubjectService.getSessionStorageFilters(this.crPurposes, this.options);
        if (result) {
            this.selections = result.selections;
            this.options = result.options;
        }
        this.parseSessionStorageDataElementFilters();
    }

    parseSessionStorageDataElementFilters() {
        const dataElementFilter = JSON.parse(sessionStorage.getItem("ConsentDataSubjectDataElementsFilter"));
        if (dataElementFilter && this.options.dataElements) {
            this.dataElement = this.options.dataElements.find((option: IDropDownElement): boolean => {
                return option.label === dataElementFilter[0].name;
            });
            this.dataElementValue = dataElementFilter[0].value;
        }
    }

    getDataElementFilters() {
        this.collectionPointService.getDataElements()
            .then((dataElements: EntityItem[]) => {
                this.options.dataElements = dataElements.map((dataElement: EntityItem) => {
                    return {
                        label: dataElement.Label,
                        value: dataElement.Id,
                    };
                });
            });
    }

    handleApply() {
        this.isDrawerOpen = false;
        this.consentDataSubjectService.setSessionStorageFilters(this.filters);
        super.handleApply();
    }

    handleClear() {
        this.filters = {};
        this.selections = {};
        this.options.purposeVersion = null;
        this.dataElement = { label: "", value: "" };
        this.dataElementValue = "";
        this.consentDataSubjectService.clearSessionStorageFilters();
        this.updateTopicFilters();
    }

    handleSearch(model: string = "", clear: boolean = false) {
        this.filters.identifier = clear ? "" : model;
        if (!clear) {
            this.handleApply();
        }
    }

    toggleDrawer() {
        this.isDrawerOpen = !this.isDrawerOpen;
    }

    setFilterValue(prop: keyof DataSubjectFilters, value: string) {
        this.filters[prop] = value;
    }

    selectOption(prop: keyof DataSubjectFilters, selection: IDropDownElement) {
        this.selections[prop] = selection;
        this.filters[prop] = selection.value;
    }

    selectPurpose({ currentValue }) {
        this.selections.purposeGuid = currentValue || null;
        this.selections.purposeVersion = null;
        this.filters.purposeGuid = currentValue ? currentValue.value : undefined;
        this.filters.purposeVersion = undefined;
        this.getFilteredPurposeVersions(currentValue);
    }

    selectPurposeVersion(purpose: IDropDownElement) {
        this.selections.purposeVersion = purpose;
        this.filters.purposeVersion = purpose.version;
        this.loadingTopics = true;
        this.consentPurposeService.getPurposeDetails(purpose.value, purpose.version)
            .then((result: PurposeDetails) => {
                if (result) {
                    this.filters.purposeVersion = result.Version;
                    this.getPurposeTopicFilters(result);
                    this.updateTopicFilters();
                }
                this.loadingTopics = false;
            });
    }

    selectDataElementName(dataElement: IDropDownElement) {
        if (isEmpty(this.filters.dataElements)) {
            this.filters.dataElements = [{ name: "", value: "" }];
        }
        this.dataElement = dataElement;
        this.filters.dataElements = [{ name: this.dataElement.label, value: this.dataElementValue }];
    }

    setDataElementValue(value: string) {
        if (isEmpty(this.filters.dataElements)) {
            this.filters.dataElements = [{ name: "", value: "" }];
        }
        this.dataElementValue = value;
        this.filters.dataElements = [{ name: this.dataElement.label, value: this.dataElementValue }];
    }

    onInputChange({ key, value }) {
        if (key === "Enter") {
            this.selections.purposeGuid = this.options.purposeGuid[0];
            return;
        }
        if (value) {
            this.filterList(uniqBy(this.crPurposes, "label"), value);
        } else {
            this.options.purposeGuid = null;
        }
    }

    protected populateFilters() {
        this.updateTopicFilters();
        this.getDataElementFilters();
        this.options.purposeGuid = this.crPurposes;
    }

    private updateTopicFilters() {
        if (!this.showTopicFilter) {
            return;
        }
        if (!this.filters.purposeGuid) {
            this.options.topics = [];
            return;
        }
    }

    private filterList(purposeList: IDropDownElement[], searchString: string) {
        const needle = searchString.toLowerCase();
        this.options.purposeGuid = filter(purposeList, (purpose: IDropDownElement): boolean => purpose.label.toLowerCase().indexOf(needle) !== -1);
    }

    private getPurposeTopicFilters(purpose: PurposeDetails) {
        if (!purpose || !purpose.Topics) {
            this.options.topics = [];
        }
        this.options.topics = purpose.Topics.map((topic: TopicListItem): IDropDownElement => {
            return { label: topic.Name, value: topic.Id === "Any" ? undefined : topic.Id };
        });
    }

    private getPurposeName(): string {
        const testPurpose: IDropDownElement = this.crPurposes.find((purpose: IDropDownElement): boolean => purpose.value === this.filters.purposeGuid);
        return testPurpose ? testPurpose.label : "";
    }

    private getFilteredPurposeVersions(selectedPurpose: IDropDownElement) {
        this.filters.purposeGuid = selectedPurpose.value;
        this.consentPurposeService.getFilteredPurposeVersions(selectedPurpose.value).then((result: IVersionListItem[]) => {
            if (result && result.length > 0) {
                this.options.purposeVersion = orderBy(result, "Version", "desc").map((item: IVersionListItem): IDropDownElement => {
                    return {
                        label: `${item.Label} (${this.consentPurposeService.getVersionLabel(item.Version)})`,
                        value: item.Id,
                        version: item.Version,
                    };
                });
            }
        });
    }
}
