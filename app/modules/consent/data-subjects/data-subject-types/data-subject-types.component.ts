// Third Party
import { Component } from "@angular/core";

// Services
import { McmModalService } from "modules/mcm/mcm-modal.service";

@Component({
    selector: "cr-data-subject-types",
    templateUrl: "./data-subject-types.component.html",
})
export class CrDataSubjectTypesComponent {
    constructor(
        private mcmModalService: McmModalService,
    ) {}

    upgradeModule() {
        this.mcmModalService.openMcmModal("Upgrade");
    }
}
