// Angular
import { NgModule } from "@angular/core";

// Components
import { CrDataSubjectTypesComponent } from "consentModule/data-subjects/data-subject-types/data-subject-types.component";

// Modules
import { DataSubjectsSharedModule } from "consentModule/data-subjects/shared/data-subjects-shared.module";

@NgModule({
    imports: [
        DataSubjectsSharedModule,
    ],
    declarations: [
        CrDataSubjectTypesComponent,
    ],
    entryComponents: [
        CrDataSubjectTypesComponent,
    ],
})
export class DataSubjectTypesModule { }
