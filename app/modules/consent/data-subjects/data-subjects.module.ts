// Angular
import { NgModule } from "@angular/core";

// Modules
import { DataSubjectsSharedModule } from "consentModule/data-subjects/shared/data-subjects-shared.module";
import { DataSubjectDetailsModule } from "./data-subject-details/data-subject-details.module";
import { DataSubjectListModule } from "./data-subject-list/data-subject-list.module";
import { DataSubjectTypesModule } from "./data-subject-types/data-subject-types.module";

@NgModule({
    imports: [
        DataSubjectsSharedModule,
        DataSubjectDetailsModule,
        DataSubjectListModule,
        DataSubjectTypesModule,
    ],
})
export class DataSubjectsModule { }
