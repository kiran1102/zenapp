// Rxjs
import {
    filter,
    take,
} from "rxjs/operators";

// Third Party
import {
    Component,
    Inject,
} from "@angular/core";
import { StateService } from "@uirouter/core";

// Components
import { CrWithdrawConsentModalComponent } from "./withdraw-consent-modal/withdraw-consent-modal.component";

// Constants
import crConstants from "consentModule/shared/cr-constants";

// Interfaces
import { IStore } from "interfaces/redux.interface";
import { IHorseshoeGraphData } from "interfaces/one-horseshoe-graph.interface";
import { IStringMap } from "interfaces/generic.interface";
import { IWithdrawConsentModalResolve } from "interfaces/withdraw-consent-modal-resolve.interface";

// Models
import {
    DataSubjectDetails,
    DataSubjectPurposeItem,
} from "crmodel/data-subject-details";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

// Redux
import { StoreToken } from "tokens/redux-store.token";

// Services
import { ConsentDataSubjectService } from "consentModule/shared/services/cr-data-subject.service";
import {
    orderBy,
    uniqBy,
} from "lodash";
import { OtModalService } from "@onetrust/vitreus";

@Component({
    selector: "cr-data-subject-details",
    templateUrl: "./data-subject-details.component.html",
})

export class CrDataSubjectDetailsComponent {
    purposeCounters: IHorseshoeGraphData;
    showWithdrawButton: boolean;
    showPreferences: boolean;
    showDataElements: boolean;
    isLoading: boolean;
    dataSubjectId: string;
    subject: DataSubjectDetails;
    crConstants = crConstants;

    constructor(
        private stateService: StateService,
        @Inject(StoreToken) public store: IStore,
        private consentDataSubjectService: ConsentDataSubjectService,
        private otModalService: OtModalService,
        private translatePipe: TranslatePipe,
    ) {}

    trackByFn(index: number, item: DataSubjectPurposeItem) {
        return index;
    }

    ngOnInit(): void {
        this.dataSubjectId = this.stateService.params.Id;
        this.loadDataSubject();
    }

    goToPurposes(): void {
        this.stateService.go("zen.app.pia.module.consentreceipt.views.purposes.list");
    }

    goToTransactions(): void {
        this.stateService.go("zen.app.pia.module.consentreceipt.views.transactions.list", { identifier: this.subject.Identifier });
    }

    goToPurposeTransactions(purposeGuid: string): void {
        this.stateService.go("zen.app.pia.module.consentreceipt.views.transactions.list", ({ identifier: this.subject.Identifier, purposeId: purposeGuid }));
    }

    onHeaderRouteClick(route: { path: string, params: any }) {
        this.stateService.go(route.path);
    }

    translateConst(value: string, constType: IStringMap<string>): string {
        return this.consentDataSubjectService.translateConstValue(value, constType);
    }

    canWithdrawConsent(purpose: DataSubjectPurposeItem): boolean {
        return purpose.ConsentStatus === this.crConstants.CRReceiptStatus.Active;
    }

    withdrawConsent(purpose: DataSubjectPurposeItem): void | undefined {
        if (!purpose || !purpose.TransactionSummary) {
            return;
        }
        const withdrawConsentModalData: IWithdrawConsentModalResolve = {
            transactionId: purpose.TransactionSummary.LastTransactionId,
        };
        this.otModalService.create(
            CrWithdrawConsentModalComponent,
            {
                isComponent: true,
            },
            withdrawConsentModalData,
        ).pipe(
            take(1),
            filter((result: boolean) => result),
        ).subscribe(() => {
            this.stateService.go(".", { Id: this.stateService.params.Id }, { reload: true });
        });
    }

    private loadDataSubject(): void {
        this.isLoading = true;
        this.consentDataSubjectService.getDataSubjectDetails(this.dataSubjectId)
            .then((dataSubject: DataSubjectDetails): void => {
                dataSubject.Purposes = uniqBy(orderBy(dataSubject.Purposes, "PurposeVersion", "desc"), "PurposeId");
                this.subject = dataSubject;
                this.generateCounters();
            }).finally(() => {
                this.isLoading = false;
            });
    }

    private purposeCountByStatus(status: string): number {
        let count = 0;
        this.subject.Purposes.forEach((purpose: DataSubjectPurposeItem) => {
            if (purpose.ConsentStatus === status) {
                count++;
            }
        });
        return count;
    }

    private generateCounters(): void | undefined {
        const allPurposes: number = this.subject.Purposes.length;
        if (allPurposes === 0) {
            this.purposeCounters = {
                series: [
                    {
                        graphClass: "empty",
                        label: "",
                        value: 1,
                    },
                ],
                legend: [],
            };
        } else {
            this.purposeCounters = {
                series: [
                    {
                        graphClass: "stroke-blue",
                        label: "PendingPurposes",
                        value: this.purposeCountByStatus(crConstants.CRReceiptStatus.Pending) / allPurposes,
                    },
                    {
                        graphClass: "stroke-green",
                        label: "ActivePurposes",
                        value: this.purposeCountByStatus(crConstants.CRReceiptStatus.Active) / allPurposes,
                    },
                    {
                        graphClass: "stroke-lagoon",
                        label: "ExpiredPurposes",
                        value: this.purposeCountByStatus(crConstants.CRReceiptStatus.Expired) / allPurposes,
                    },
                    {
                        graphClass: "stroke-light-red",
                        label: "WithdrawnPurposes",
                        value: this.purposeCountByStatus(crConstants.CRReceiptStatus.Withdrawn) / allPurposes,
                    },
                    {
                        graphClass: "stroke-grey-2",
                        label: "NoConsentPurposes",
                        value: this.purposeCountByStatus(crConstants.CRReceiptStatus.NoConsent) / allPurposes,
                    },
                    {
                        graphClass: "stroke-orange",
                        label: "OptedOutPurposes",
                        value: this.purposeCountByStatus(crConstants.CRReceiptStatus.OptedOut) / allPurposes,
                    },
                ],
                legend: [
                    {
                        label: this.translatePipe.transform("PendingPurposes"),
                        class: "background-blue",
                    },
                    {
                        label: this.translatePipe.transform("ActivePurposes"),
                        class: "background-green",
                    },
                    {
                        label: this.translatePipe.transform("ExpiredPurposes"),
                        class: "background-lagoon",
                    },
                    {
                        label: this.translatePipe.transform("WithdrawnPurposes"),
                        class: "background-light-red",
                    },
                    {
                        label: this.translatePipe.transform("NoConsentPurposes"),
                        class: "background-grey-2",
                    },
                    {
                        label: this.translatePipe.transform("OptedOutPurposes"),
                        class: "background-orange",
                    },
                ],
            };
        }
    }
}
