import { Component, Inject, Input } from "@angular/core";
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import { ColumnDefinition } from "crmodel/column-definition";
import { DataSubjectElementItem } from "crmodel/data-subject-details";

@Component({
    selector: "cr-data-subject-elements",
    templateUrl: "./data-subject-elements.component.html",
})
export class CrDataSubjectElementsComponent {
    @Input() elements: DataSubjectElementItem[];

    columns: ColumnDefinition[];
    translate = this.$rootScope.t;

    constructor(
        @Inject("$rootScope") readonly $rootScope: IExtendedRootScopeService,
    ) {
        this.columns = [
            {
                columnName: this.translate("Name"),
                cellValueKey: "Name",
            },
            {
                columnName: this.translate("Value"),
                cellValueKey: "Value",
            },
        ];
    }
}
