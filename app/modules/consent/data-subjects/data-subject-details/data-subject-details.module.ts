// Angular
import { NgModule } from "@angular/core";

// Components
import { CrDataSubjectDetailsComponent } from "consentModule/data-subjects/data-subject-details/data-subject-details.component";
import { CrDataSubjectElementsComponent } from "consentModule/data-subjects/data-subject-details/data-subject-elements/data-subject-elements.component";
import { CrWithdrawConsentModalComponent } from "consentModule/data-subjects/data-subject-details/withdraw-consent-modal/withdraw-consent-modal.component";

// Modules
import { DataSubjectsSharedModule } from "consentModule/data-subjects/shared/data-subjects-shared.module";

@NgModule({
    imports: [
        DataSubjectsSharedModule,
    ],
    declarations: [
        CrDataSubjectDetailsComponent,
        CrDataSubjectElementsComponent,
        CrWithdrawConsentModalComponent,
    ],
    entryComponents: [
        CrDataSubjectDetailsComponent,
        CrDataSubjectElementsComponent,
        CrWithdrawConsentModalComponent,
    ],
})
export class DataSubjectDetailsModule { }
