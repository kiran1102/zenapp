// Third Party
import { Component } from "@angular/core";

// Interfaces
import { IWithdrawConsentModalResolve } from "interfaces/withdraw-consent-modal-resolve.interface";

// Models
import { TransactionWithdrawalRequest } from "crmodel/transaction-withdrawal-request";

// Rxjs
import { Subject } from "rxjs";

// Services
import { ConsentTransactionService } from "consentModule/shared/services/cr-transaction.service";

@Component({
    selector: "cr-withdraw-content-modal",
    templateUrl: "./withdraw-consent-modal.component.html",
})
export class CrWithdrawConsentModalComponent {

    modalTitle: string;
    actionInProgress: boolean;
    notes: string;

    otModalCloseEvent: Subject<boolean>;
    otModalData: IWithdrawConsentModalResolve;

    constructor(
        private consentTransactionService: ConsentTransactionService,
    ) { }

    closeModal() {
        this.otModalCloseEvent.next(false);
    }

    handleChanged(text: string) {
        this.notes = text;
    }

    withdrawConsent() {
        this.actionInProgress = true;
        const request: TransactionWithdrawalRequest = {
            Notes: this.notes,
        };
        this.consentTransactionService.withdrawTransaction(this.otModalData.transactionId, request)
            .then((result: boolean) => {
                this.actionInProgress = false;
                if (result) {
                    this.otModalData.result = true;
                    this.otModalCloseEvent.next(this.otModalData.result);
                }
            });
    }
}
