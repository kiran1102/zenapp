// 3rd Party
import { isEmpty } from "lodash";
import {
    Component,
    Inject,
    Input,
} from "@angular/core";

// Redux
import { StoreToken } from "tokens/redux-store.token";
import { IStore, IStoreState } from "interfaces/redux.interface";
import {
    getPreferenceCenterAllLanguages,
} from "oneRedux/reducers/preference-center.reducer";

// Components
import { CrPreferenceCenterBase } from "consentModule/preferences/shared/preference-center-base";
import { LanguageSelectionModal } from "sharedModules/components/language-selection-modal/language-selection-modal.component";

// Interfaces
import { IGetAllLanguageResponse } from "interfaces/dsar/dsar-edit-language.interface";
import { ILanguageSelectionModalResolve } from "interfaces/language-selection-modal-resolve.interface";

// Models
import { PreferenceCenterLabels } from "crmodel/preference-center-list";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

// Rxjs
import {
    filter,
    take,
} from "rxjs/operators";

// Services
import { PreferenceCenterActionService } from "consentModule/preferences/shared/preference-center-action.service";
import { OtModalService } from "@onetrust/vitreus";

@Component({
    selector: "cr-preference-center-ribbon",
    templateUrl: "./preference-center-ribbon.component.html",
})
export class CrPreferenceCenterRibbonComponent extends CrPreferenceCenterBase {
    isInactive: boolean;
    allLanguages: IGetAllLanguageResponse[];

    constructor(
        @Inject(StoreToken) private store: IStore,
        @Inject("crConstants") public crConstants: any,
        private preferenceCenterActionService: PreferenceCenterActionService,
        private otModalService: OtModalService,
        private translatePipe: TranslatePipe,
    ) {
        super(store);
    }

    selectActiveLanguage(selection: IGetAllLanguageResponse): void {
        this.preferenceCenterActionService.updateLanguage(selection);
        if (isEmpty(this.preferenceCenter.PreferenceCenterSettings.Languages[selection.Code])) {
            this.preferenceCenter.PreferenceCenterSettings.Languages[selection.Code] = new PreferenceCenterLabels();
        }
    }

    selectLanguages(selection: string[], defaultLanguage: string): void {
        this.preferenceCenter.Languages = selection;
        this.preferenceCenter.Language = defaultLanguage;
        this.preferenceCenterActionService.updatePreferenceCenterPreview(this.preferenceCenter);
        this.ensureCorrectDefaultLanguage();
    }

    showEditLanguagesModal(): void {
        const languageSelectionModalData: ILanguageSelectionModalResolve = {
            defaultLanguage: this.preferenceCenter.Language,
            selectedLanguages: this.preferenceCenter.Languages,
            allLanguages: this.allLanguages,
        };
        this.otModalService.create(
            LanguageSelectionModal,
            {
                isComponent: true,
            },
            languageSelectionModalData,
        ).pipe(
            take(1),
            filter((response: { languages: string[], defaultLanguage: string }): boolean => {
                return response && response.languages.length && ( response.defaultLanguage.trim() !== null);
            }),
        ).subscribe(({ languages, defaultLanguage }) => {
            this.selectLanguages(languages, defaultLanguage);
            this.preferenceCenterActionService.savePreferenceCenter();
        });
    }

    get languages(): IGetAllLanguageResponse[] {
        if (!this.preferenceCenter || !this.allLanguages) {
            return null;
        }
        const selectedLanguages = this.allLanguages.filter((language: IGetAllLanguageResponse): boolean => {
            return this.preferenceCenter.Languages.indexOf(language.Code) >= 0;
        });
        return selectedLanguages;
    }

    protected onStateUpdate(state: IStoreState): void {
        super.onStateUpdate(state);
        this.allLanguages = getPreferenceCenterAllLanguages(state);
    }

    private ensureCorrectDefaultLanguage(): void {
        const activeLanguageIndex = this.languages.findIndex((language: IGetAllLanguageResponse): boolean => language.Code === this.language.Code);
        if (activeLanguageIndex < 0) {
            const defaultLanguage = this.languages.find((language: IGetAllLanguageResponse): boolean => language.Code === this.preferenceCenter.Language);
            this.preferenceCenterActionService.updateLanguage(defaultLanguage);
        }
    }
}
