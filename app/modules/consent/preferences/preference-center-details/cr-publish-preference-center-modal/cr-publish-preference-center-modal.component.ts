// Third Party
import {
    Component,
    Inject,
} from "@angular/core";

// Components
import { IPublishPreferenceCenterModalResolve } from "consentModule/preferences/shared/publish-preference-center-modal-resolve.interface";

// Interfaces
import { IStore } from "interfaces/redux.interface";

// Redux
import { StoreToken } from "tokens/redux-store.token";

// Rxjs
import { Subject } from "rxjs";

// Services
import { PreferenceCenterActionService } from "consentModule/preferences/shared/preference-center-action.service";

@Component({
    selector: "cr-publish-preference-center-modal",
    templateUrl: "./cr-publish-preference-center-modal.component.html",
})
export class PublishPreferenceCenterModalComponent {
    actionInProgress: boolean;
    otModalData: IPublishPreferenceCenterModalResolve;
    otModalCloseEvent = new Subject();

    constructor(
        @Inject(StoreToken) public store: IStore,
        private preferenceCenterActionService: PreferenceCenterActionService,
    ) { }

    closeModal() {
        this.otModalCloseEvent.next(false);
    }

    publishPreferenceCenter() {
        this.actionInProgress = true;
        this.preferenceCenterActionService.publishPreferenceCenter().then((success: boolean) => {
            this.actionInProgress = false;
            if (success) {
                this.otModalCloseEvent.next(true);
            }
        });
    }
}
