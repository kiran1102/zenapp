declare const OneConfirm: any;
import { each } from "lodash";
import { Component, Inject } from "@angular/core";
import {
    TransitionService,
    Transition,
    StateDeclaration,
    StateService,
} from "@uirouter/core";

// Models
import { PreferenceCenter } from "crmodel/preference-center-list";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";
import { ITabsNav } from "interfaces/one-tabs-nav.interface";

// Redux
import { StoreToken } from "tokens/redux-store.token";
import { IStore, IStoreState } from "interfaces/redux.interface";
import {
    getPreferenceCenterPreview,
    getPreferenceCenterLoadingStatus,
    getPreferenceCenterActionStatus,
    getPreferenceCenterModifiedStatus,
} from "oneRedux/reducers/preference-center.reducer";
import { ReduxBaseComponent } from "sharedModules/components/redux-base/redux-base.component";

// Services
import { PreferenceCenterActionService } from "consentModule/preferences/shared/preference-center-action.service";
import { Permissions } from "modules/shared/services/helper/permissions.service";

@Component({
    selector: "cr-preference-center-details",
    templateUrl: "./preference-center-details.component.html",
})

export class CrPreferenceCenterDetailsComponent extends ReduxBaseComponent {
    preferenceCenterId: string;
    preferenceCenter: PreferenceCenter;
    loading: boolean;
    actionInProgress: boolean;
    tabs: ITabsNav[];
    currentTab: string;
    ribbonToggle: boolean;
    unRegistrationHook: any;
    hasPendingChanges: boolean;
    isInactive = false;
    isValid = true;

    constructor(
        private stateService: StateService,
        @Inject("crConstants") readonly crConstants: any,
        @Inject(StoreToken) private store: IStore,
        private readonly permissions: Permissions,
        private readonly preferenceCenterActionService: PreferenceCenterActionService,
        private $transitions: TransitionService,
        readonly translatePipe: TranslatePipe,
    ) {
        super(store);
    }

    ngOnInit(): void {
        const pcDetailsRoute = this.stateService.current.name;
        this.unRegistrationHook = this.$transitions.onExit(
            { from: pcDetailsRoute },
            (transition: Transition, state: StateDeclaration) => {
                if (state.name === pcDetailsRoute && this.hasPendingChanges) {
                    return OneConfirm("", this.translatePipe.transform("UnsavedChangesWouldYouLikeToStay"), "", "", false, this.translatePipe.transform("Stay"), this.translatePipe.transform("DiscardChanges"), "md")
                        .then((result: boolean) => {
                            if (!result) {
                                this.preferenceCenterActionService.resetState();
                            }
                            return !result;
                        });
                }
                this.preferenceCenterActionService.resetState();
                return true;
            },
        );
        super.ngOnInit();
        this.ribbonToggle = this.permissions.canShow("ConsentPreferenceCenterRibbon");
        this.preferenceCenterId = this.stateService.params.Id;
        this.configureTabs(this.stateService.params.tab);
        this.loading = true;
        this.preferenceCenterActionService.fetchPreferenceCenter(this.preferenceCenterId);
        this.preferenceCenterActionService.fetchAllLanguages();
    }

    ngOnDestroy(): void {
        super.ngOnDestroy();
        this.unRegistrationHook();
    }

    validCheck(isFormValid: boolean): void {
        this.isValid = isFormValid;
    }

    protected onStateUpdate(state: IStoreState): void {
        this.preferenceCenter = getPreferenceCenterPreview(state);
        this.loading = getPreferenceCenterLoadingStatus(state);
        this.actionInProgress = getPreferenceCenterActionStatus(state);
        this.hasPendingChanges = getPreferenceCenterModifiedStatus(state);
        if (this.preferenceCenter) {
            this.isInactive = this.preferenceCenter.Status === this.crConstants.CRPreferenceCenterStatus.Inactive;
        }
    }

    private configureTabs(currentTab: string): void {
        this.currentTab = currentTab || "builder";
        this.tabs = [
            { text: this.translatePipe.transform("Builder"), id: "builder" },
            { text: this.translatePipe.transform("Branding"), id: "branding" },
            { text: this.translatePipe.transform("Integrations"), id: "integrations" },
            { text: this.translatePipe.transform("Details"), id: "details" },
        ];

        each(this.tabs, (option: ITabsNav): void => {
            option.action = (tabOptions: ITabsNav): void => {
                this.changeTab(tabOptions);
            };
        });
    }

    private changeTab(option: ITabsNav): void {
        this.currentTab = option.id;
        this.stateService.go(".", { Id: this.preferenceCenterId, tab: option.id });
    }
}
