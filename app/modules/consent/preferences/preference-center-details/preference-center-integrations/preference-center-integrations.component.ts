import {
    Component,
    Inject,
    Input,
} from "@angular/core";

// Models
import { PreferenceCenter } from "crmodel/preference-center-list";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

// Redux
import { ReduxBaseComponent } from "sharedModules/components/redux-base/redux-base.component";
import { StoreToken } from "tokens/redux-store.token";
import {
    IStore,
    IStoreState,
} from "interfaces/redux.interface";
import {
    getPreferenceCenterPreview,
    getPreferenceCenterActionStatus,
    getPreferenceCenterInactiveStatus,
} from "oneRedux/reducers/preference-center.reducer";

// Services
import { NotificationService } from "modules/shared/services/provider/notification.service";
import { Permissions } from "modules/shared/services/helper/permissions.service";

// Utilities
import Utilities from "Utilities";

@Component({
    selector: "cr-preference-center-integrations",
    templateUrl: "./preference-center-integrations.component.html",
})

export class CrPreferenceCenterIntegrationsComponent extends ReduxBaseComponent {
    @Input() isInactive: boolean;

    preferenceCenter: PreferenceCenter;
    actionInProgress: boolean;
    canUseAzureBlobUrl = this.permissions.canShow("ConsentPreferenceCenterBlobPublish");

    constructor(
        @Inject(StoreToken) private store: IStore,
        private notificationService: NotificationService,
        private translatePipe: TranslatePipe,
        private permissions: Permissions,
    ) {
        super(store);
    }

    copyPreferenceCenterUrl(): void {
        Utilities.copyToClipboard(this.preferenceCenter.Url);
        this.notificationService.alertSuccess(this.translatePipe.transform("LinkCopied"), this.translatePipe.transform("PreferenceCenterUrlCopiedSuccessfully"));
    }

    copyPreferenceCenterMagicLinkUrl(): void {
        Utilities.copyToClipboard(this.preferenceCenter.LinkTokenUrl);
        this.notificationService.alertSuccess(this.translatePipe.transform("LinkCopied"), this.translatePipe.transform("PreferenceCenterMagicLinkUrlCopiedSuccessfully"));
    }

    copyPreferenceCenterBlobUrl(): void {
        Utilities.copyToClipboard(this.preferenceCenter.BlobUrl);
        this.notificationService.alertSuccess(this.translatePipe.transform("LinkCopied"), this.translatePipe.transform("PreferenceCenterBlobUrlCopiedSuccessfully"));
    }

    protected onStateUpdate(state: IStoreState): void {
        this.preferenceCenter = getPreferenceCenterPreview(state);
        this.actionInProgress = getPreferenceCenterActionStatus(state);
        this.isInactive = getPreferenceCenterInactiveStatus(state);
    }

}
