import { Component, Inject } from "@angular/core";

// Components
import { CrPreferenceCenterBase } from "consentModule/preferences/shared/preference-center-base";

// Models
import { PreferenceCenterLabels } from "crmodel/preference-center-list";

// Redux
import { StoreToken } from "tokens/redux-store.token";
import { IStore } from "interfaces/redux.interface";

// Services
import { PreferenceCenterActionService } from "consentModule/preferences/shared/preference-center-action.service";
import { Permissions } from "modules/shared/services/helper/permissions.service";

@Component({
    selector: "cr-preference-center-branding",
    templateUrl: "./preference-center-branding.component.html",
})

export class CrPreferenceCenterBrandingComponent extends CrPreferenceCenterBase {
    canEditTranslations = this.permissions.canShow("ConsentPreferenceCenterEditTranslations");
    canEditPreferenceCenterCssUrl = this.permissions.canShow("ConsentPreferenceCenterEditCustomCssUrl");
    // looks like HostBinding in Vitreus doesn't work as expected and false is converted into something truthy along the way
    cssUrlReadonly = this.canEditPreferenceCenterCssUrl ? undefined : true;
    formReadonly = this.canEditTranslations ? undefined : true;
    hasCssUrlError: boolean;

    constructor(
        @Inject(StoreToken) private readonly store: IStore,
        private readonly preferenceCenterActionService: PreferenceCenterActionService,
        private readonly permissions: Permissions,
    ) {
        super(store);
    }

    ngOnInit(): void {
        super.ngOnInit();
        if (this.preferenceCenter) {
            const validationResult = this.preferenceCenterActionService.validatePreferenceCenter(this.preferenceCenter);
            this.hasCssUrlError = !validationResult.CustomCssUrlValid;
        }
    }

    onLabelChange(label: string, prop: keyof PreferenceCenterLabels): void {
        this.preferenceCenterLabels[prop] = label;
        this.preferenceCenterActionService.updatePreferenceCenterPreview(this.preferenceCenter);
    }

    onCustomCssChange(url: string): void {
        this.preferenceCenter.PreferenceCenterSettings.CustomCssUrl = url;
        const validationResult = this.preferenceCenterActionService.updatePreferenceCenterPreview(this.preferenceCenter);
        this.hasCssUrlError = !validationResult.CustomCssUrlValid;
    }
}
