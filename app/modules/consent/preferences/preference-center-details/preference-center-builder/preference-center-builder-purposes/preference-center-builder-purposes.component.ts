// Angular
import {
    Component,
    EventEmitter,
    Inject,
    Input,
    OnInit,
    Output,
} from "@angular/core";
import {
    CdkDragDrop,
    moveItemInArray,
} from "@angular/cdk/drag-drop";

// Components
import { CrPreferenceCenterBase } from "consentModule/preferences/shared/preference-center-base";

// Enums
import { DefaultConsentStatus } from "consentModule/enums/default-consent-status.enum";

// Models
import { PurposeListItem } from "crmodel/purpose-list";
import { TopicListItem } from "crmodel/topic-list";
import {
    CustomPreferenceListItem,
    CustomPreferenceLanguage,
    CustomPreferenceOption,
} from "crmodel/custom-preference-list";
import { IDropDownElement } from "crservice/consent-base.service";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

// Redux
import { StoreToken } from "tokens/redux-store.token";
import { IStore } from "interfaces/redux.interface";

// Services
import { PreferenceCenterActionService } from "modules/consent/preferences/shared/preference-center-action.service";
import { ConsentPurposeService } from "crservice/cr-purpose.service";
import { ToastService } from "@onetrust/vitreus";

@Component({
    selector: "cr-preference-center-builder-purposes",
    templateUrl: "./preference-center-builder-purposes.component.html",
})
export class CrPreferenceCenterBuilderPurposesComponent extends CrPreferenceCenterBase implements OnInit {
    @Input() displayNotificationPurpose: boolean;
    @Input() purposes: PurposeListItem[];
    @Input() purposesViewSettings: { [id: string]: { removable: boolean, draggable: boolean } } = {};
    @Output() onPurposeDrop = new EventEmitter<PurposeListItem[]>();
    @Output() onNotificationPurposeToggle = new EventEmitter<boolean>();

    noNotificationPurpose: PurposeListItem[];
    notificationPurpose: PurposeListItem;
    displayPurposeList: boolean;

    constructor(
        @Inject(StoreToken) private store: IStore,
        private preferenceCenterActionService: PreferenceCenterActionService,
        private consentPurposeService: ConsentPurposeService,
        private toastService: ToastService,
        private translatePipe: TranslatePipe,
    ) {
        super(store);
    }

    ngOnInit() {
        super.ngOnInit();
        if (this.purposes && this.purposesViewSettings) {
            this.filterNotificationPurpose();
            this.notificationPurpose = this.purposes.find((purpose: PurposeListItem): boolean => {
                return !this.purposesViewSettings[purpose.Id].draggable;
            });
        }
    }

    handleDroppedPurpose(event: CdkDragDrop<IDropDownElement[] | PurposeListItem>) {
        if (event.item.data) {
            const promiseArray = [];
            event.item.data.forEach((item: IDropDownElement) => {
                if (!this.noNotificationPurpose.find((purpose: PurposeListItem): boolean => {
                    return purpose.Id === item.value;
                })) {
                    const purpose: Partial<PurposeListItem> = {
                        Label: item.label,
                        Id: item.value,
                    };

                    const index = event.currentIndex;

                    const revertOptimisticUpdate = () => {
                        this.noNotificationPurpose.splice(index, 1);
                    };

                    const addPurposeToList = () => {
                        this.noNotificationPurpose.splice(index, 0, purpose as PurposeListItem);
                    };

                    // update UI optimistically
                    addPurposeToList();

                    promiseArray.push(
                        this.consentPurposeService.getPurposeDetails(item.value, item.version)
                            .then((response: PurposeListItem) => {
                                this.purposesViewSettings[response.Id] = {
                                    removable: this.isPurposeItemRemovable(response),
                                    draggable: this.isPurposeItemDraggable(response),
                                };
                                this.noNotificationPurpose.splice(index, 1, response);
                            })
                            .catch(revertOptimisticUpdate),
                    );
                } else {
                    this.toastService.warning(
                        this.translatePipe.transform("Warning"),
                        this.translatePipe.transform("Consent.PreferenceCenter.Details.Builder.DuplicatePurpose", { Purpose: item.label }),
                    );
                }
            });
            Promise.all(promiseArray).then(() => {
                this.purposes = this.noNotificationPurpose.concat([this.notificationPurpose]);
                this.onPurposeDrop.emit(this.purposes);
            });
        } else {
            if (event.previousIndex === event.currentIndex) {
                return;
            }
            moveItemInArray(this.noNotificationPurpose, event.previousIndex, event.currentIndex);
            this.purposes = this.noNotificationPurpose.concat([this.notificationPurpose]);
            this.onPurposeDrop.emit(this.purposes);
        }
    }

    removePurpose(purposeId: string) {
        this.preferenceCenterActionService.removePurpose(purposeId);
        this.filterNotificationPurpose();
    }

    onNotificationPurposeDisplayToggle(toggled: boolean) {
        this.onNotificationPurposeToggle.emit(toggled);
    }

    collapsePurpose(index: number) {
        this.noNotificationPurpose[index].Expanded = false;
    }

    handlePurposeListDisplay(hovering: boolean) {
        this.displayPurposeList = hovering ? true : (this.noNotificationPurpose && this.noNotificationPurpose.length > 0);
    }

    trackPurposeById(index: number, purpose: PurposeListItem) {
        return purpose.Id;
    }

    trackOption(index: number, item: CustomPreferenceOption) {
        return index;
    }

    translatedPurpose = (purpose: PurposeListItem): string => {
        if (!purpose) {
            return null;
        }
        if (!purpose.Languages) {
            return purpose.Label;
        }
        const label = this.getTranslatedLabel(purpose.Languages, "Name");
        return label || purpose.Label;
    }

    translatedTopic = (topic: TopicListItem): string => {
        if (!topic) {
            return null;
        }
        if (!topic.Languages) {
            return topic.Name;
        }
        const label = this.getTranslatedLabel(topic.Languages, "Name");
        return label || topic.Name;
    }

    translatedCustomPreference = (customPreference: CustomPreferenceListItem): string => {
        if (!customPreference) {
            return null;
        }
        if (!customPreference.Languages) {
            return customPreference.Name;
        }
        const customPreferenceTranslation = customPreference.Languages
            .find((customPrefLanguage: CustomPreferenceLanguage): boolean => customPrefLanguage.Language === this.language.Code)
            || customPreference.Languages.find((customPrefLanguage: CustomPreferenceLanguage): boolean => customPrefLanguage.Default);
        return customPreferenceTranslation.Name;
    }

    translatedOption(customPreference: CustomPreferenceListItem, optionIndex: number): string {
        if (!customPreference) {
            return null;
        }
        if (!customPreference.Languages) {
            return customPreference.Options[optionIndex].Label;
        }
        const customPreferenceTranslation = customPreference.Languages
            .find((customPrefLanguage: CustomPreferenceLanguage): boolean => customPrefLanguage.Language === this.language.Code)
            || customPreference.Languages.find((customPrefLanguage: CustomPreferenceLanguage): boolean => customPrefLanguage.Default);
        return customPreferenceTranslation.Options[optionIndex].Label;
    }

    private filterNotificationPurpose() {
        this.noNotificationPurpose = this.purposes.filter((purpose: PurposeListItem): boolean => {
            return this.purposesViewSettings[purpose.Id].draggable;
        });
        this.displayPurposeList = this.noNotificationPurpose && this.noNotificationPurpose.length > 0;
    }

    private isPurposeItemDraggable(purpose: PurposeListItem) {
        return this.isDefaultConsentStatusActive(purpose) === false;
    }

    private isPurposeItemRemovable(purpose: PurposeListItem) {
        return !this.isInactive && !this.isDefaultConsentStatusActive(purpose);
    }

    private isDefaultConsentStatusActive(purpose: PurposeListItem): boolean {
        return purpose.DefaultConsentStatus === DefaultConsentStatus.Active;
    }
}
