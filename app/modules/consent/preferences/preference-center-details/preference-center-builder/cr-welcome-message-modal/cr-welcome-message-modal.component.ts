// Third Party
import {
    Component,
    OnInit,
} from "@angular/core";

// Interfaces
import { IProtocolResponse } from "interfaces/protocol.interface";
import { IWelcomeMessageModalResolve } from "./welcome-message-modal-resolve.interface";

// Rxjs
import { Subject } from "rxjs";

// Services
import { Permissions } from "sharedModules/services/helper/permissions.service";

@Component({
    selector: "cr-welcome-message-modal",
    templateUrl: "./cr-welcome-message-modal.component.html",
})
export class WelcomeMessageModalComponent implements OnInit {
    text = "";
    modalTitle: string;
    canEditTranslations: boolean;

    otModalData: IWelcomeMessageModalResolve;
    otModalCloseEvent: Subject<IProtocolResponse<string>>;

    constructor(private permissionsService: Permissions) {}

    ngOnInit() {
        this.canEditTranslations = this.getCanEditTranslations();

        if (this.otModalData && this.otModalData.text) {
            this.text = this.otModalData.text;
        }
    }

    closeModal() {
        this.otModalCloseEvent.next();
    }

    handleSave() {
        this.otModalCloseEvent.next({ result: true, data: this.text  });
    }

    handleMessageChange(message: string) {
        this.text = message;
    }

    private getCanEditTranslations(): boolean {
        const EDIT_TRANSLATIONS_PERMISSION_KEY = "ConsentPreferenceCenterEditTranslations";
        return this.permissionsService.canShow(EDIT_TRANSLATIONS_PERMISSION_KEY);
    }
}
