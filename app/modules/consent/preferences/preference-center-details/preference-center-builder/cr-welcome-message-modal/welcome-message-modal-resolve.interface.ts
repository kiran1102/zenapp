export interface IWelcomeMessageModalResolve {
    text: string;
    language: string;
    isInactive: boolean;
}
