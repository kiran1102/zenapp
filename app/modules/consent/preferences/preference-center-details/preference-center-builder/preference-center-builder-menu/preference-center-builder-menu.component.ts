import {
    Component,
    Inject,
} from "@angular/core";
import { CdkDragDrop } from "@angular/cdk/drag-drop";

// Models
import { PreferenceCenter } from "crmodel/preference-center-list";
import { PurposeDetails } from "crmodel/purpose-details";

// Redux
import { ReduxBaseComponent } from "sharedModules/components/redux-base/redux-base.component";
import { StoreToken } from "tokens/redux-store.token";
import {
    IStore,
    IStoreState,
} from "interfaces/redux.interface";
import {
    getPreferenceCenterPreview,
    getPreferenceCenterActionStatus,
    getPreferenceCenterSelectedPurposes,
} from "oneRedux/reducers/preference-center.reducer";

// Services
import { ConsentPurposeService } from "consentModule/shared/services/cr-purpose.service";
import { IDropDownElement } from "consentModule/shared/services/consent-base.service";
import { PreferenceCenterActionService } from "consentModule/preferences/shared/preference-center-action.service";

@Component({
    selector: "cr-preference-center-builder-menu",
    templateUrl: "./preference-center-builder-menu.component.html",
})

export class CrPreferenceCenterBuilderMenuComponent extends ReduxBaseComponent {
    preferenceCenter: PreferenceCenter;
    purposes: IDropDownElement[];
    selectedPurposes: IDropDownElement[] = [];
    purposeFilter = "";
    actionInProgress: boolean;
    loading: boolean;
    isInactive: boolean;

    constructor(
        @Inject(StoreToken) private readonly store: IStore,
        @Inject("crConstants") readonly crConstants: any,
        private readonly consentPurposeService: ConsentPurposeService,
        private readonly preferenceCenterActionService: PreferenceCenterActionService,
    ) {
        super(store);
    }

    ngOnInit(): void {
        super.ngOnInit();
        this.loading = true;
        this.consentPurposeService.getActivePurposeFilters().then((purposes: PurposeDetails[]): void => {
            this.preferenceCenterActionService.dragPurposes([]);
            this.purposes = purposes.map((purpose: PurposeDetails): IDropDownElement => {
                return {
                    label: purpose.Label,
                    value: purpose.Id,
                    version: purpose.Version,
                };
            });
            this.loading = false;
        });
    }

    selectPurposeForDragging(purpose: IDropDownElement, selection: boolean): void | undefined {
        const existingIndex: number = this.selectedPurposeIndex(purpose.value);
        if (selection && existingIndex === -1) {
            this.selectedPurposes.push(purpose);
            return;
        }

        if (!selection && existingIndex > -1) {
            this.selectedPurposes.splice(existingIndex, 1);
        }
    }

    purposeFilterChanged(filter: string): void {
        this.purposeFilter = filter;
    }

    selectedPurposeIndex(purposeId: string): number {
        return this.selectedPurposes.findIndex((purpose: IDropDownElement): boolean => purpose.value === purposeId);
    }

    purposesDropped(event: CdkDragDrop<IDropDownElement[]>) {
        if (event.container.element.nativeElement.id === "PreferenceCenterBuilderPurposeList") {
            this.selectedPurposes = [];
        }
    }

    protected onStateUpdate(state: IStoreState): void {
        this.preferenceCenter = getPreferenceCenterPreview(state);
        this.actionInProgress = getPreferenceCenterActionStatus(state);
        this.selectedPurposes = getPreferenceCenterSelectedPurposes(state);
        if (this.preferenceCenter) {
            this.isInactive = this.preferenceCenter.Status === this.crConstants.CRPreferenceCenterStatus.Inactive;
        }
    }
}
