// Rxjs
import { Subscription } from "rxjs";
// Angular
import {
    Component,
    Inject,
} from "@angular/core";

// Redux
import { StoreToken } from "tokens/redux-store.token";
import {
    IStore,
    IStoreState,
} from "interfaces/redux.interface";

// Components
import { CrPreferenceCenterBase } from "consentModule/preferences/shared/preference-center-base";

// Models
import { PurposeListItem } from "crmodel/purpose-list";

// Services
import { PreferenceCenterActionService } from "consentModule/preferences/shared/preference-center-action.service";

// Enums
import { DefaultConsentStatus } from "consentModule/enums/default-consent-status.enum";

@Component({
    selector: "cr-preference-center-builder",
    templateUrl: "./preference-center-builder.component.html",
})
export class CrPreferenceCenterBuilderComponent extends CrPreferenceCenterBase {
    welcomeMessageExpanded = false;
    isInactive: boolean;
    subscription: Subscription;
    purposesViewSettings: { [id: string]: { removable: boolean, draggable: boolean } } = {};

    constructor(
        @Inject(StoreToken) private store: IStore,
        private preferenceCenterActionService: PreferenceCenterActionService,
    ) {
        super(store);
    }

    ngOnInit() {
        super.ngOnInit();
    }

    ngOnDestroy() {
        if (this.subscription) {
            this.subscription.unsubscribe();
        }
    }

    trackByFn(index: number, item: string) {
        return index;
    }

    purposeDropped(purposes: PurposeListItem[]) {
        this.preferenceCenter.Purposes = purposes;
        this.preferenceCenterActionService.updatePreferenceCenterPreview(this.preferenceCenter);
    }

    notificationPurposeToggled(toggled: boolean) {
        this.preferenceCenter.DisplayNotificationPurpose = toggled;
        this.preferenceCenterActionService.updatePreferenceCenterPreview(this.preferenceCenter);
    }

    protected onStateUpdate(state: IStoreState) {
        super.onStateUpdate(state);
        this.moveDefaultConsentStatusPurposeToBottom();
        this.setViewSettings();
    }

    private isPurposeItemDraggable(purpose: PurposeListItem) {
        return this.isDefaultConsentStatusActive(purpose) === false;
    }

    private isPurposeItemRemovable(purpose: PurposeListItem) {
        return !this.isInactive && !this.isDefaultConsentStatusActive(purpose);
    }

    private setViewSettings() {
        const purposes = this.preferenceCenter.Purposes;

        return purposes && purposes.forEach((purpose) => {
            this.purposesViewSettings[purpose.Id] = {
                removable: this.isPurposeItemRemovable(purpose),
                draggable: this.isPurposeItemDraggable(purpose),
            };
        });
    }

    private isDefaultConsentStatusActive(purpose: PurposeListItem): boolean {
        return purpose.DefaultConsentStatus === DefaultConsentStatus.Active;
    }

    private moveDefaultConsentStatusPurposeToBottom() {
        const purposes = this.preferenceCenter.Purposes;

        if (!purposes) {
            return;
        }

        const consentStatusPurpose = purposes.find((item) => {
            return this.isDefaultConsentStatusActive(item);
        });

        const getConsentStatusPurposeIndex = () => {
            return purposes.indexOf(consentStatusPurpose);
        };

        if (!consentStatusPurpose) {
            return;
        }

        const TARGET_INDEX = purposes.length - 1;

        const swapDefaultConsentPurposeWithNext = () => {
            const index = getConsentStatusPurposeIndex();
            const temp = purposes[index];
            purposes[index] = purposes[index + 1];
            purposes[index + 1] = temp;
        };

        const shouldPurposeShift = () => {
            const index = getConsentStatusPurposeIndex();
            return index < TARGET_INDEX;
        };

        while (shouldPurposeShift()) {
            swapDefaultConsentPurposeWithNext();
        }
    }
}
