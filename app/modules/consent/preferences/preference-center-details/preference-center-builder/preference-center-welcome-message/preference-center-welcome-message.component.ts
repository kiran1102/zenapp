// Angular
import {
    Component,
    Inject,
    Input,
} from "@angular/core";

// Components
import { CrPreferenceCenterBase } from "consentModule/preferences/shared/preference-center-base";
import { WelcomeMessageModalComponent } from "consentModule/preferences/preference-center-details/preference-center-builder/cr-welcome-message-modal/cr-welcome-message-modal.component";

// Interfaces
import { IProtocolResponse } from "interfaces/protocol.interface";
import { IWelcomeMessageModalResolve } from "consentModule/preferences/preference-center-details/preference-center-builder/cr-welcome-message-modal/welcome-message-modal-resolve.interface";

// Redux
import { StoreToken } from "tokens/redux-store.token";
import { IStore } from "interfaces/redux.interface";

// Rxjs
import {
    filter,
    take,
} from "rxjs/operators";

// Services
import { PreferenceCenterActionService } from "modules/consent/preferences/shared/preference-center-action.service";
import { OtModalService } from "@onetrust/vitreus";

@Component({
    selector: "cr-preference-center-welcome-message",
    templateUrl: "./preference-center-welcome-message.component.html",
})
export class CrPreferenceCenterWelcomeMessageComponent extends CrPreferenceCenterBase {
    welcomeMessageExpanded = false;

    constructor(
        @Inject(StoreToken) private store: IStore,
        private otModalService: OtModalService,
        private preferenceCenterActionService: PreferenceCenterActionService,
    ) {
        super(store);
    }

    editWelcomeMessage() {
        const welcomeMessageModalData: IWelcomeMessageModalResolve = {
            text: this.preferenceCenterLabels.WelcomeMessage,
            language: this.language.Name,
            isInactive: this.isInactive,
        };
        this.otModalService.create(
            WelcomeMessageModalComponent,
            {
                isComponent: true,
            },
            welcomeMessageModalData,
        ).pipe(
            take(1),
            filter((result: IProtocolResponse<string>) => result !== null),
        ).subscribe((res: IProtocolResponse<string>) => {
            if (res && res.data) {
                this.preferenceCenterLabels.WelcomeMessage = res.data;
                this.preferenceCenterActionService.updatePreferenceCenterPreview(this.preferenceCenter);
                this.preferenceCenterActionService.savePreferenceCenter();
            }
        });
    }
}
