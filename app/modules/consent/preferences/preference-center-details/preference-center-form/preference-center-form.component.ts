import { find, matches } from "lodash";
import { Component, Inject } from "@angular/core";

// Redux
import { StoreToken } from "tokens/redux-store.token";
import { IStore } from "interfaces/redux.interface";
import { getCurrentOrgList } from "oneRedux/reducers/org.reducer";

// Interfaces
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import { ILanguage } from "interfaces/localization.interface";
import { IOrganization } from "interfaces/org.interface";

// Services
import { PreferenceCenterActionService } from "consentModule/preferences/shared/preference-center-action.service";
import { Permissions } from "modules/shared/services/helper/permissions.service";

// Models
import { PreferenceCenter } from "crmodel/preference-center-list";
import { CrPreferenceCenterBase } from "consentModule/preferences/shared/preference-center-base";

@Component({
    selector: "cr-preference-center-form",
    templateUrl: "./preference-center-form.component.html",
})

export class CrPreferenceCenterFormComponent extends CrPreferenceCenterBase {
    organizations: IOrganization[];
    selectedOrganization: IOrganization;
    selectedLanguage: ILanguage;
    actionInProgress: boolean;
    languageToggle: boolean;

    constructor(
        @Inject("$rootScope") readonly $rootScope: IExtendedRootScopeService,
        @Inject(StoreToken) private readonly store: IStore,
        @Inject("crConstants") readonly crConstants: any,
        private readonly preferenceCenterActionService: PreferenceCenterActionService,
        private readonly permissions: Permissions,
    ) {
        super(store);
    }

    ngOnInit() {
        super.ngOnInit();
        this.organizations = getCurrentOrgList(this.store.getState());
        this.languageToggle = this.permissions.canShow("ConsentPreferenceCenterLanguage");
        this.selectedOrganization = find(this.organizations, matches ({ Name: this.preferenceCenter.OrganizationName }));
        if (this.preferenceCenter.Language) {
            this.selectedLanguage = find(this.$rootScope.languages, matches({ value: this.preferenceCenter.Language }));
        }
    }

    onModelChange(payload: string, prop: keyof PreferenceCenter) {
        this.preferenceCenter[prop] = payload;
        this.preferenceCenterActionService.updatePreferenceCenterPreview(this.preferenceCenter);
    }

    onToggleChange(prop: keyof PreferenceCenter) {
        this.preferenceCenter[prop] = !this.preferenceCenter[prop];
        this.preferenceCenterActionService.updatePreferenceCenterPreview(this.preferenceCenter);
    }

    selectOrganization(organization: IOrganization) {
        this.selectedOrganization = organization;
        this.preferenceCenter.OrganizationId = this.selectedOrganization.Id;
        this.preferenceCenter.OrganizationName = this.selectedOrganization.Name;
        this.preferenceCenterActionService.updatePreferenceCenterPreview(this.preferenceCenter);
    }

    selectLanguage(language: ILanguage) {
        this.selectedLanguage = language;
        this.preferenceCenter.Language = this.selectedLanguage.Code;
        this.preferenceCenterActionService.updatePreferenceCenterPreview(this.preferenceCenter);
    }
}
