// Angular
import { NgModule } from "@angular/core";
import { DragDropModule } from "@angular/cdk/drag-drop";

// Components
import { CrPreferenceCenterDetailsComponent } from "consentModule/preferences/preference-center-details/preference-center-details.component";
import { CrPreferenceCenterBrandingComponent } from "consentModule/preferences/preference-center-details/preference-center-branding/preference-center-branding.component";
import { CrPreferenceCenterBuilderComponent } from "consentModule/preferences/preference-center-details/preference-center-builder/preference-center-builder.component";
import { CrPreferenceCenterBuilderMenuComponent } from "consentModule/preferences/preference-center-details/preference-center-builder/preference-center-builder-menu/preference-center-builder-menu.component";
import { CrPreferenceCenterBuilderPurposesComponent } from "consentModule/preferences/preference-center-details/preference-center-builder/preference-center-builder-purposes/preference-center-builder-purposes.component";
import { CrPreferenceCenterWelcomeMessageComponent } from "consentModule/preferences/preference-center-details/preference-center-builder/preference-center-welcome-message/preference-center-welcome-message.component";
import { WelcomeMessageModalComponent } from "consentModule/preferences/preference-center-details/preference-center-builder/cr-welcome-message-modal/cr-welcome-message-modal.component";
import { CrPreferenceCenterFormComponent } from "consentModule/preferences/preference-center-details/preference-center-form/preference-center-form.component";
import { CrPreferenceCenterIntegrationsComponent } from "consentModule/preferences/preference-center-details/preference-center-integrations/preference-center-integrations.component";
import { CrPreferenceCenterRibbonComponent } from "consentModule/preferences/preference-center-details/preference-center-ribbon/preference-center-ribbon.component";
import { PublishPreferenceCenterModalComponent } from "consentModule/preferences/preference-center-details/cr-publish-preference-center-modal/cr-publish-preference-center-modal.component";

// Modules
import { PreferenceCentersSharedModule } from "consentModule/preferences/shared/preference-centers-shared.module";
import { OtGridModule } from "@onetrust/vitreus";

@NgModule({
    imports: [
        PreferenceCentersSharedModule,
        DragDropModule,
        OtGridModule,
    ],
    declarations: [
        CrPreferenceCenterDetailsComponent,
        CrPreferenceCenterBrandingComponent,
        CrPreferenceCenterBuilderComponent,
        CrPreferenceCenterBuilderMenuComponent,
        CrPreferenceCenterBuilderPurposesComponent,
        CrPreferenceCenterWelcomeMessageComponent,
        WelcomeMessageModalComponent,
        CrPreferenceCenterFormComponent,
        CrPreferenceCenterIntegrationsComponent,
        CrPreferenceCenterRibbonComponent,
        PublishPreferenceCenterModalComponent,
    ],
    entryComponents: [
        CrPreferenceCenterDetailsComponent,
        WelcomeMessageModalComponent,
        PublishPreferenceCenterModalComponent,
    ],
})
export class PreferenceCenterDetailsModule { }
