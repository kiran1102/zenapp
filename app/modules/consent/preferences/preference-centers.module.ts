// Angular
import { NgModule } from "@angular/core";

// Modules
import { PreferenceCentersSharedModule } from "consentModule/preferences/shared/preference-centers-shared.module";
import { PreferenceCenterDetailsModule } from "./preference-center-details/preference-center-details.module";
import { PreferenceCenterListModule } from "./preference-center-list/preference-center-list.module";

@NgModule({
    imports: [
        PreferenceCentersSharedModule,
        PreferenceCenterDetailsModule,
        PreferenceCenterListModule,
    ],
})
export class PreferenceCentersModule { }
