// Third Party
import {
    Component,
    Inject,
} from "@angular/core";

// Interfaces
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import { ILanguage } from "interfaces/localization.interface";
import { IOrganization } from "interfaces/org.interface";
import { IStore } from "interfaces/redux.interface";

// Models
import {
    NewPreferenceCenter,
    PreferenceCenter,
} from "crmodel/preference-center-list";
import { PurposeDetails } from "crmodel/purpose-details";

// Redux
import { StoreToken } from "tokens/redux-store.token";
import { getCurrentOrgList } from "oneRedux/reducers/org.reducer";

// Rxjs
import { Subject } from "rxjs";

// Services
import { ConsentPreferenceService } from "consentModule/shared/services/cr-preference.service";
import { CollectionPointService } from "consentModule/shared/services/collection-point.service";
import { ConsentPurposeService } from "consentModule/shared/services/cr-purpose.service";
import { LookupService } from "sharedModules/services/helper/lookup.service.ts";

@Component({
    selector: "cr-create-preference-center-modal",
    templateUrl: "./cr-create-preference-center-modal.component.html",
})
export class CreatePreferenceCenterModalComponent {
    createInProgress: boolean;
    organizations: IOrganization[];
    preferenceCenter: NewPreferenceCenter = new NewPreferenceCenter();
    fullPurposeList: PurposeDetails[];
    purposeOptions: PurposeDetails[];
    selectedPurposes: PurposeDetails[] = [];
    selectedOrganization: IOrganization;
    selectedLanguage: ILanguage;
    languageToggle: boolean;
    otModalCloseEvent: Subject<boolean>;

    constructor(
        @Inject("$rootScope") public $rootScope: IExtendedRootScopeService,
        @Inject(StoreToken) public store: IStore,
        private consentPurposeService: ConsentPurposeService,
        private collectionPointService: CollectionPointService,
        private consentPreferenceService: ConsentPreferenceService,
        private lookupService: LookupService,
    ) { }

    ngOnInit() {
        this.organizations = getCurrentOrgList(this.store.getState());
        this.consentPurposeService.getActivePurposeFilters().then((purposes: PurposeDetails[]) => {
            this.fullPurposeList = purposes;
            this.lookupService.filterOptionsBySelections(this.selectedPurposes, this.fullPurposeList, "Id");
        });
        this.selectedOrganization = this.organizations[0];
        this.selectedLanguage = this.$rootScope.languages[0];
    }

    closeModal() {
        this.otModalCloseEvent.next(false);
    }

    onModelChange(payload: string, prop: keyof NewPreferenceCenter) {
        this.preferenceCenter[prop] = payload;
    }

    selectOption({ currentValue }) {
        this.selectedPurposes = currentValue;
        this.purposeOptions = this.lookupService.filterOptionsBySelections(this.selectedPurposes, this.fullPurposeList, "Id");
    }

    onInputChange({ key, value }) {
        if (key === "Enter") {
            this.selectOption({ currentValue: this.selectedPurposes.push(this.purposeOptions[0]) });
            return;
        }
        this.purposeOptions = this.lookupService.filterOptionsByInput(this.selectedPurposes, this.fullPurposeList, value, "Id", "Label");
    }

    selectOrganization(organization: IOrganization) {
        this.selectedOrganization = organization;
    }

    selectLanguage(language: ILanguage) {
        this.selectedLanguage = language;
    }

    createPreferenceCenter() {
        this.createInProgress = true;
        this.preferenceCenter.OrganizationId = this.selectedOrganization.Id;
        this.preferenceCenter.Language = this.selectedLanguage.Code;
        this.preferenceCenter.PurposeId = this.collectionPointService.idsToString(this.selectedPurposes);
        this.consentPreferenceService.createPreferenceCenter(this.preferenceCenter).then((result: PreferenceCenter): void => {
            this.createInProgress = false;
            if (result) {
                this.otModalCloseEvent.next(true);
            }
        });
    }

    get emptyField(): boolean {
        if (this.preferenceCenter.Name && this.preferenceCenter.Description) {
            return !(
                this.preferenceCenter.Name.trim()
                && this.preferenceCenter.Description.trim()
            );
        } else {
            return true;
        }
    }
}
