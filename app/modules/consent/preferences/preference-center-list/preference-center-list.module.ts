// Angular
import { NgModule } from "@angular/core";

// Components
import { CrPreferenceCenterListComponent } from "consentModule/preferences/preference-center-list/preference-center-list.component";
import { CrPreferenceCenterFiltersComponent } from "consentModule/preferences/preference-center-list/preference-center-filters/preference-center-filters.component";
import { CreatePreferenceCenterModalComponent } from "consentModule/preferences/preference-center-list/cr-create-preference-center-modal/cr-create-preference-center-modal.component";

// Modules
import { PreferenceCentersSharedModule } from "consentModule/preferences/shared/preference-centers-shared.module";

@NgModule({
    imports: [
        PreferenceCentersSharedModule,
    ],
    declarations: [
        CrPreferenceCenterListComponent,
        CrPreferenceCenterFiltersComponent,
        CreatePreferenceCenterModalComponent,
    ],
    entryComponents: [
        CrPreferenceCenterListComponent,
        CreatePreferenceCenterModalComponent,
    ],
})
export class PreferenceCenterListModule { }
