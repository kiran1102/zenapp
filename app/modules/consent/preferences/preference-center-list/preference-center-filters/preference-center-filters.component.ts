// Third Party
import {
    Component,
    EventEmitter,
    Input,
    Output,
    OnChanges,
} from "@angular/core";

// Models and Components
import { PreferenceCenterFilters } from "crmodel/preference-center-filters";
import { CrFiltersBase } from "modules/consent/shared/cr-filters-base/cr-filters-base.component";

// Interfaces
import { ConsentPreferenceService } from "consentModule/shared/services/cr-preference.service";
import { IDropDownElement } from "consentModule/shared/services/consent-base.service";

@Component({
    selector: "cr-preference-center-filters",
    templateUrl: "./preference-center-filters.component.html",
})

export class CrPreferenceCenterFiltersComponent extends CrFiltersBase<PreferenceCenterFilters> implements OnChanges {
    @Input() purposeList: IDropDownElement[];
    @Output() applyFilters: EventEmitter<PreferenceCenterFilters> = new EventEmitter<PreferenceCenterFilters>();

    loadingPurposes: boolean;
    showDefaultPurposeList = true;

    constructor(
        private readonly consentPreferenceService: ConsentPreferenceService,
    ) {
        super();
    }

    ngOnChanges() {
        this.populateFilters();
        this.filters.name = sessionStorage.getItem("ConsentPreferenceCenterNameFilter");
        this.selections = this.consentPreferenceService.getSessionStorageFilters(this.purposeList);
    }

    handleApply() {
        this.isDrawerOpen = false;
        this.consentPreferenceService.setSessionStorageFilters(this.filters);
        super.handleApply();
    }

    handleClear() {
        super.handleClear();
        this.filters.name = "";
        this.filters.purposeGuid = "";
        this.consentPreferenceService.removeSessionStorageFilters();
    }

    handleSearch(model: string = "", clear: boolean = false) {
        this.filters.name = clear ? "" : model;
    }

    protected populateFilters() {
        if (this.purposeList) {
            this.options.purposeGuid = this.purposeList;
        }
    }
}
