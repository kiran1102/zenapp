// Third Party
import { Component } from "@angular/core";
import { StateService } from "@uirouter/core";

// Components
import { CreatePreferenceCenterModalComponent } from "./cr-create-preference-center-modal/cr-create-preference-center-modal.component";

// Interfaces
import { IDropdownOption } from "interfaces/one-dropdown.interface";

// Models
import { ColumnDefinition } from "crmodel/column-definition";
import {
    PreferenceCenter,
    PreferenceCenterList,
} from "crmodel/preference-center-list";
import { PurposeDetails } from "crmodel/purpose-details";
import { PreferenceCenterFilters } from "crmodel/preference-center-filters";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

// Services
import { ConsentPreferenceService } from "consentModule/shared/services/cr-preference.service";
import { ConsentPurposeService } from "consentModule/shared/services/cr-purpose.service";
import { IDropDownElement } from "consentModule/shared/services/consent-base.service";
import {
    OtModalService,
    ConfirmModalType,
} from "@onetrust/vitreus";

// Rxjs
import {
    filter,
    take,
} from "rxjs/operators";

@Component({
    selector: "cr-preference-center-list",
    templateUrl: "./preference-center-list.component.html",
})
export class CrPreferenceCenterListComponent {
    isLoading: boolean;
    isRouting: boolean;
    preferenceCenterList: PreferenceCenterList;
    columns: ColumnDefinition[];
    purposeList: IDropDownElement[];
    isDrawerOpen: boolean;
    filters: PreferenceCenterFilters;

    constructor(
        private stateService: StateService,
        private consentPreferenceService: ConsentPreferenceService,
        private consentPurposeService: ConsentPurposeService,
        private otModalService: OtModalService,
        readonly translatePipe: TranslatePipe,
    ) { }

    ngOnInit() {
        this.filters = this.consentPreferenceService.handleFilters();
        this.columns = [
            {
                columnName: this.translatePipe.transform("Name"),
                cellValueKey: "Name",
                cellClass: "max-width-50",
                columnType: "idLink",
                route: "zen.app.pia.module.consentreceipt.views.preferencecenters.details",
            },
            {
                columnName: this.translatePipe.transform("Organization"),
                rowToText: (row: PreferenceCenter): string => row.OrganizationName,
            },
            {
                columnType: "actions",
                getOptions: (row: PreferenceCenter): IDropdownOption[] => this.getOptions(row.Id),
            },
        ];
        this.getActivePurposeFilters();
    }

    viewPreferenceCenter(preferenceCenterId: string) {
        this.stateService.go("zen.app.pia.module.consentreceipt.views.preferencecenters.details", { Id: preferenceCenterId });
    }

    getOptions(preferenceCenterId: string): IDropdownOption[] {
        return [
            {
                id: "delete",
                text: this.translatePipe.transform("Delete"),
                action: () => this.openConfirmationModal(preferenceCenterId),
            },
        ];
    }

    getActivePurposeFilters() {
        this.isLoading = true;
        this.consentPurposeService.getActivePurposeFilters(true).then((purposeFilters: PurposeDetails[]) => {
            this.isLoading = false;
            if (purposeFilters && purposeFilters.length > 0) {
                this.purposeList = purposeFilters.map((purpose: PurposeDetails): IDropDownElement => {
                    return {
                        label: purpose.Label,
                        value: purpose.Id,
                        version: purpose.Version,
                    };
                });
            }
            this.pageChange(this.filters, 0);
        });
    }

    createPreferenceCenterModal() {
        this.otModalService.create(
            CreatePreferenceCenterModalComponent,
            {
                isComponent: true,
            },
        ).pipe(
            take(1),
            filter((result: boolean) => result),
        ).subscribe(() => {
            this.pageChange(this.filters, 0);
        });
    }

    deletePreferenceCenter(preferenceCenterId: string): ng.IPromise<boolean> {
        return this.consentPreferenceService.deletePreferenceCenter(preferenceCenterId)
            .then((response: boolean): boolean => {
                if (response) {
                    this.pageChange(this.filters, 0);
                }
                return response;
            });
    }

    pageChange(filters: PreferenceCenterFilters, page: number, size?: number) {
        this.isLoading = true;
        this.filters = filters;
        this.consentPreferenceService.getPreferenceCenters(filters, page, size)
            .then((response: PreferenceCenterList) => {
                this.isLoading = false;
                this.preferenceCenterList = response;
            });
        this.isDrawerOpen = false;
    }

    private openConfirmationModal(preferenceCenterId: string) {
        this.otModalService.confirm({
            type: ConfirmModalType.DELETE,
            translations: {
                title: this.translatePipe.transform("DeletePreferenceCenter"),
                desc: "",
                confirm: this.translatePipe.transform("ConfirmPreferenceCenterDeletion"),
                submit: this.translatePipe.transform("Delete"),
                cancel: this.translatePipe.transform("Cancel"),
            },
        }).pipe(
            take(1),
            filter((result: boolean) => result),
        ).subscribe((result: boolean) => {
            this.deletePreferenceCenter(preferenceCenterId);
        });
    }
}
