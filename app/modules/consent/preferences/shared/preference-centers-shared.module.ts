// Angular
import { NgModule } from "@angular/core";

// Components
import { CrPreferenceCenterHeaderComponent } from "consentModule/preferences/shared/preference-center-header/preference-center-header.component";

// Modules
import { ConsentSharedModule } from "modules/consent/shared/consent-shared.module";

// Services
import { PreferenceCenterActionService } from "./preference-center-action.service";

@NgModule({
    imports: [
        ConsentSharedModule,
    ],
    exports: [
        ConsentSharedModule,
        CrPreferenceCenterHeaderComponent,
    ],
    declarations: [
        CrPreferenceCenterHeaderComponent,
    ],
    providers: [
        PreferenceCenterActionService,
    ],
})
export class PreferenceCentersSharedModule { }
