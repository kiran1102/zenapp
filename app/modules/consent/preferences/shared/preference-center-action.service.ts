// Rxjs
import {
    Observable,
    Subject,
} from "rxjs";

// Angular
import {
    Injectable,
    Inject,
} from "@angular/core";
import { IPromise } from "angular";

// Third Party
import {
    every,
    values,
    isEmpty,
} from "lodash";

// Redux
import {
    PreferenceCenterActions,
    getPreferenceCenterSelectedPurposes,
    getPreferenceCenterPreview,
    getPreferenceCenterLoadingLanguagesStatus,
    getPreferenceCenterAllLanguages,
    getPreferenceCenterModifiedStatus,
} from "oneRedux/reducers/preference-center.reducer";
import { StoreToken } from "tokens/redux-store.token";

// Interfaces
import { IStore } from "interfaces/redux.interface";
import { ILanguage } from "interfaces/localization.interface";
import { IDropDownElement } from "consentModule/shared/services/consent-base.service";

// Services
import { ConsentPurposeService } from "consentModule/shared/services/cr-purpose.service";
import { ConsentPreferenceService } from "consentModule/shared/services/cr-preference.service";

// Models
import { PreferenceCenter } from "crmodel/preference-center-list";
import { PreferenceCenterValidation } from "crmodel/preference-center-validation";
import { PurposeListItem } from "crmodel/purpose-list";
import { IGetAllLanguageResponse } from "interfaces/dsar/dsar-edit-language.interface";
import { Regex } from "constants/regex.constant";

@Injectable()
export class PreferenceCenterActionService {

    private orderRefresh = new Subject<any>();

    constructor(
        @Inject(StoreToken) private store: IStore,
        private readonly ConsentPreference: ConsentPreferenceService,
        private readonly ConsentPurpose: ConsentPurposeService,
    ) { }

    updateOrder() {
        this.orderRefresh.next();
    }

    orderUpdateRequest(): Observable<any> {
        return this.orderRefresh.asObservable();
    }

    updateLanguage(language: ILanguage) {
        this.store.dispatch({ type: PreferenceCenterActions.SELECT_LANGUAGE, language });
    }

    updatePreferenceCenterPreview(updatedPreferenceCenterPreview: PreferenceCenter, isModified: boolean = true): PreferenceCenterValidation {
        const validationResult = this.validatePreferenceCenter(updatedPreferenceCenterPreview);
        const isValid = every(values(validationResult));
        this.store.dispatch({
            type: PreferenceCenterActions.UPDATE_PREFERENCE_CENTER_PREVIEW,
            updatedPreferenceCenterPreview,
            isModified,
            isValid,
        });
        return validationResult;
    }

    fetchPreferenceCenter(id: string): Promise<boolean> {
        this.store.dispatch({ type: PreferenceCenterActions.FETCHING_PREFERENCE_CENTER });
        const fetchPromise = this.ConsentPreference.getPreferenceCenter(id)
            .then((preferenceCenterDetails: PreferenceCenter): boolean => {
                // make sure legacy Preference Centers with empty Languages array have a default value
                if (isEmpty(preferenceCenterDetails.Languages)) {
                    preferenceCenterDetails.Languages = [preferenceCenterDetails.Language];
                }
                this.store.dispatch({ type: PreferenceCenterActions.UPDATE_PREFERENCE_CENTER, updatedPreferenceCenter: { ...preferenceCenterDetails } });
                this.updatePreferenceCenterPreview(preferenceCenterDetails, false);
                this.store.dispatch({ type: PreferenceCenterActions.FETCHED_PREFERENCE_CENTER });
                return Boolean(preferenceCenterDetails);
            });
        return Promise.resolve(fetchPromise);
    }

    savePreferenceCenter(): Promise<boolean> {
        const preferenceCenter = getPreferenceCenterPreview(this.store.getState());
        this.store.dispatch({ type: PreferenceCenterActions.SAVING_PREFERENCE_CENTER });
        const savePromise = this.ConsentPreference.updatePreferenceCenter(preferenceCenter)
            .then((result: PreferenceCenter): boolean => {
                if (result) {
                    this.updatePreferenceCenterPreview(result, false);
                    this.updateOrder();
                    this.store.dispatch({ type: PreferenceCenterActions.SAVED_PREFERENCE_CENTER });
                } else {
                    this.store.dispatch({ type: PreferenceCenterActions.PREFERENCE_CENTER_ERROR });
                }
                return Boolean(result);
            });
        return Promise.resolve(savePromise);
    }

    previewPreferenceCenter(): Promise<boolean> {
        const preferenceCenter = getPreferenceCenterPreview(this.store.getState());
        let previewPrefCenterLocation;
        if (preferenceCenter.Url) {
            previewPrefCenterLocation = preferenceCenter.Url.split("login")[0] + preferenceCenter.Id + "/draft";
        } else {
            return Promise.resolve(false);
        }
        const newTab = window.open();
        if (getPreferenceCenterModifiedStatus(this.store.getState())) {
            return this.savePreferenceCenter().then((result: boolean): boolean => {
                newTab.location.href = previewPrefCenterLocation;
                return result;
            });
        } else {
            newTab.location.href = previewPrefCenterLocation;
            return Promise.resolve(true);
        }
    }

    publishPreferenceCenter(): Promise<boolean> {
        const preferenceCenter = getPreferenceCenterPreview(this.store.getState());
        this.store.dispatch({ type: PreferenceCenterActions.SAVING_PREFERENCE_CENTER });
        const savePromise = this.ConsentPreference.publishPreferenceCenter(preferenceCenter)
            .then((result: PreferenceCenter): boolean => {
                if (result) {
                    this.updatePreferenceCenterPreview(result, false);
                    this.updateOrder();
                    this.store.dispatch({ type: PreferenceCenterActions.SAVED_PREFERENCE_CENTER });
                } else {
                    this.store.dispatch({ type: PreferenceCenterActions.PREFERENCE_CENTER_ERROR });
                }
                return Boolean(result);
            });
        return Promise.resolve(savePromise);
    }

    dragPurposes(selectedPurposes: IDropDownElement[]) {
        this.store.dispatch({ type: PreferenceCenterActions.DRAG_PURPOSES, selectedPurposes });
    }

    dropPurposes() {
        const selectedPurposes = getPreferenceCenterSelectedPurposes(this.store.getState());
        const preferenceCenter = getPreferenceCenterPreview(this.store.getState());
        if (!selectedPurposes || !preferenceCenter) {
            return;
        }
        if (!preferenceCenter.Purposes) {
            preferenceCenter.Purposes = [];
        }
        this.store.dispatch({ type: PreferenceCenterActions.DROPPING_PURPOSES });
        const resolvePromises: Array<IPromise<void>> = [];
        selectedPurposes.forEach((purpose: IDropDownElement) => {
            if (this.preferenceCenterPurposeIndex(preferenceCenter, purpose.value) > -1) {
                return;
            }
            const resolvePromise = this.ConsentPurpose.getPurposeDetails(purpose.value, purpose.version)
                .then((purposeDetails: PurposeListItem) => {
                    preferenceCenter.Purposes.push(purposeDetails);
                });
            resolvePromises.push(resolvePromise);
        });

        return Promise.all(resolvePromises).then(() => {
            this.updatePreferenceCenterPreview(preferenceCenter);
            this.updateOrder();
            this.store.dispatch({ type: PreferenceCenterActions.DROPPED_PURPOSES });
        });
    }

    removePurpose(purposeId: string) {
        const preferenceCenter = getPreferenceCenterPreview(this.store.getState());
        if (!preferenceCenter || !preferenceCenter.Purposes) {
            return;
        }
        const index = this.preferenceCenterPurposeIndex(preferenceCenter, purposeId);
        if (index === -1) {
            return;
        }
        preferenceCenter.Purposes.splice(index, 1);
        this.updatePreferenceCenterPreview(preferenceCenter);
        this.updateOrder();
    }

    fetchAllLanguages(): Promise<boolean> {
        const isLoadingLanguages = getPreferenceCenterLoadingLanguagesStatus(this.store.getState());
        const languages = getPreferenceCenterAllLanguages(this.store.getState());
        if (!isLoadingLanguages && !languages) {
            this.store.dispatch({ type: PreferenceCenterActions.FETCHING_LANGUAGES });
            const fetchPromise = this.ConsentPurpose.getAllLanguages()
                .then((allLanguages: IGetAllLanguageResponse[]): boolean => {
                    this.store.dispatch({ type: PreferenceCenterActions.FETCHED_LANGUAGES, allLanguages });
                    return !!allLanguages;
                });
            return Promise.resolve(fetchPromise);
        }
        this.store.dispatch({ type: PreferenceCenterActions.FETCHED_PREFERENCE_CENTER });
        return Promise.resolve(true);
    }

    resetState() {
        this.store.dispatch({ type: PreferenceCenterActions.RESET_STATE });
    }

    resetChanges() {
        this.store.dispatch({ type: PreferenceCenterActions.RESET_CHANGES });
    }

    validatePreferenceCenter(preferenceCenter: PreferenceCenter): PreferenceCenterValidation {
        if (!preferenceCenter || !preferenceCenter.PreferenceCenterSettings) {
            return null;
        }
        const validation: PreferenceCenterValidation = {
            CustomCssUrlValid: this.validateCustomCssUrl(preferenceCenter.PreferenceCenterSettings.CustomCssUrl),
            NameValid: Boolean(preferenceCenter.Name),
            DescriptionValid: Boolean(preferenceCenter.Description),
        };
        return validation;
    }

    validateCustomCssUrl(url: string): boolean {
        if (!url) {
            return true;
        }
        return Regex.HTTPS_LINK.test(url) && url.toLowerCase().endsWith(".css");
    }

    private preferenceCenterPurposeIndex(preferenceCenter: PreferenceCenter, purposeId: string): number {
        return preferenceCenter.Purposes.findIndex((purpose: PurposeListItem): boolean => purpose.Id === purposeId);
    }
}
