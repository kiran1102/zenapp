declare const OneConfirm: any;
import {
    Component,
    Inject,
} from "@angular/core";

// Components
import { CrPreferenceCenterBase } from "consentModule/preferences/shared/preference-center-base";
import { PublishPreferenceCenterModalComponent } from "consentModule/preferences/preference-center-details/cr-publish-preference-center-modal/cr-publish-preference-center-modal.component";
import { IPublishPreferenceCenterModalResolve } from "consentModule/preferences/shared/publish-preference-center-modal-resolve.interface";

// Constants
import crConstants from "consentModule/shared/cr-constants";

// Interfaces
import { IDropdownOption } from "interfaces/one-dropdown.interface";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

// Redux
import { StoreToken } from "tokens/redux-store.token";
import {
    IStore,
    IStoreState,
} from "interfaces/redux.interface";
import {
    getPreferenceCenterModifiedStatus,
    getPreferenceCenterValidStatus,
} from "oneRedux/reducers/preference-center.reducer";

// Services
import { PreferenceCenterActionService } from "consentModule/preferences/shared/preference-center-action.service";
import { ConsentPreferenceService } from "consentModule/shared/services/cr-preference.service";
import { OtModalService } from "@onetrust/vitreus";

@Component({
    selector: "cr-preference-center-header",
    templateUrl: "./preference-center-header.component.html",
})

export class CrPreferenceCenterHeaderComponent extends CrPreferenceCenterBase {
    previewToggle: boolean;
    hasPendingChanges: boolean;
    isPreferenceCenterValid: boolean;
    publishToggle: boolean;
    preferenceCenterStatusLabel: string;
    ellipsisOptions: IDropdownOption[] = [];
    crConstants = crConstants;

    constructor(
        @Inject(StoreToken) private store: IStore,
        private preferenceCenterActionService: PreferenceCenterActionService,
        private consentPreferenceService: ConsentPreferenceService,
        private otModalService: OtModalService,
        private translatePipe: TranslatePipe,
    ) {
        super(store);
    }

    ngOnInit() {
        super.ngOnInit();
    }

    getEllipsisOptions() {
        if (this.isInactive) {
            return;
        }
        this.ellipsisOptions = [];
        if (this.hasPendingChanges && this.isPreferenceCenterValid) {
            this.ellipsisOptions.push(
                {
                    text: this.translatePipe.transform("Save"),
                    action: (): void => { this.savePreferences(); },
                },
            );
        }
        if (this.hasPendingChanges) {
            this.ellipsisOptions.push(
                {
                    text: this.translatePipe.transform("ResetChanges"),
                    action: (): void => { this.resetChanges(); },
                },
            );
        }
    }

    savePreferences() {
        if (this.preferenceCenter.PreferenceCenterSettings.OrderBy) {
            this.setOrderByPurposes();
        }
        this.preferenceCenterActionService.savePreferenceCenter();
    }

    publishPreferences(): Promise<boolean> {
        if (this.preferenceCenter.PreferenceCenterSettings.OrderBy) {
            this.setOrderByPurposes();
        }
        return this.preferenceCenterActionService.publishPreferenceCenter();
    }

    previewPreferences() {
        this.preferenceCenterActionService.previewPreferenceCenter();
    }

    openPublishPreferenceCenterModal() {
        const publishPreferenceCenterModalData: IPublishPreferenceCenterModalResolve = {
            preferenceCenterName: this.preferenceCenter.Name,
        };

        this.otModalService.create(
            PublishPreferenceCenterModalComponent,
            {
                isComponent: true,
            },
            publishPreferenceCenterModalData,
        );
    }

    resetChanges() {
        OneConfirm("", this.translatePipe.transform("AreYouSureResetChanges"))
            .then((result: boolean) => {
                if (result) {
                    this.preferenceCenterActionService.resetChanges();
                }
            });
    }

    get displayStatusClass(): string {
        return `text-bold one-tag--no-bg-rounded one-tag--width-auto ${crConstants.CRPreferenceCenterRoundedStatusStyles[this.displayStatus]}`;
    }

    get displayStatusLabel(): string {
        return this.consentPreferenceService.translateConstValue(this.displayStatus, crConstants.CRPreferenceCenterStatus);
    }

    protected onStateUpdate(state: IStoreState) {
        super.onStateUpdate(state);
        this.hasPendingChanges = getPreferenceCenterModifiedStatus(state);
        this.isPreferenceCenterValid = getPreferenceCenterValidStatus(state);
        this.getEllipsisOptions();
    }

    private setOrderByPurposes() {
        const orderBy = this.preferenceCenter.PreferenceCenterSettings.OrderBy;
        orderBy.Purposes = {};
        if (this.preferenceCenter.Purposes) {
            this.preferenceCenter.Purposes.forEach((purpose, index) => {
                orderBy.Purposes[purpose.Id] = {
                    Id: purpose.Id,
                    Label: purpose.Label,
                    Index: index,
                    Topics: purpose.Topics ? {} : undefined,
                };
                if (purpose.Topics) {
                    purpose.Topics.forEach((topic, i) => {
                        orderBy.Purposes[purpose.Id]["Topics"][topic.Id] = {
                            Id: topic.Id,
                            Name: topic.Name,
                            Index: i,
                        };
                    });
                }
            });
        }
    }
}
