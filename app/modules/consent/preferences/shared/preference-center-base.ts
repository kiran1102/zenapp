// 3rd Party
import { isEmpty } from "lodash";

// Constants
import crConstants from "consentModule/shared/cr-constants";

// Interfaces
import { IGetAllLanguageResponse } from "interfaces/dsar/dsar-edit-language.interface";
import { IEntityTranslation } from "interfaces/consent/entity-translation";

// Models
import {
    PreferenceCenter,
    PreferenceCenterLabels,
    PreferenceCenterSettings,
} from "crmodel/preference-center-list";

// Redux
import { ReduxBaseComponent } from "sharedModules/components/redux-base/redux-base.component";
import {
    IStore,
    IStoreState,
} from "interfaces/redux.interface";
import {
    getPreferenceCenterPreview,
    getPreferenceCenterActionStatus,
    getPreferenceCenterLanguage,
    getPreferenceCenterInactiveStatus,
} from "oneRedux/reducers/preference-center.reducer";

export abstract class CrPreferenceCenterBase extends ReduxBaseComponent {
    preferenceCenter: PreferenceCenter;
    actionInProgress: boolean;
    language: IGetAllLanguageResponse;
    isInactive: boolean;

    constructor(
        private commonStore: IStore,
    ) {
        super(commonStore);
    }

    get preferenceCenterLabels(): PreferenceCenterLabels {
        if (!this.preferenceCenter) {
            return null;
        }
        const settings = this.getSettingsWithDefaults();
        if (!this.language) {
            return settings;
        }
        if (!settings.Languages[this.language.Code]) {
            settings.Languages[this.language.Code] = new PreferenceCenterLabels();
        }
        return settings.Languages[this.language.Code];
    }

    get preferenceCenterSettings(): PreferenceCenterSettings {
        return this.preferenceCenter ? this.preferenceCenter.PreferenceCenterSettings : null;
    }

    protected onStateUpdate(state: IStoreState): void {
        this.preferenceCenter = getPreferenceCenterPreview(state);
        if (!this.preferenceCenter) {
            this.preferenceCenter = new PreferenceCenter();
        }
        if (isEmpty(this.preferenceCenter.PreferenceCenterSettings)) {
            this.preferenceCenter.PreferenceCenterSettings = new PreferenceCenterSettings();
            this.preferenceCenter.PreferenceCenterSettings.Languages = { [this.preferenceCenter.Language]: new PreferenceCenterLabels() };
        }
        this.actionInProgress = getPreferenceCenterActionStatus(state);
        this.language = getPreferenceCenterLanguage(state);
        this.isInactive = getPreferenceCenterInactiveStatus(state);
    }

    protected getTranslatedLabel(translations: IEntityTranslation[], property: keyof IEntityTranslation): string {
        if (!translations || !this.language) {
            return "";
        }
        const translation = translations
            .find((label: IEntityTranslation): boolean => label.Language === this.language.Code)
            || translations.find((label: IEntityTranslation): boolean => label.Default);
        return translation ? String(translation[property]) : "";
    }

    protected getSettingsWithDefaults(): PreferenceCenterSettings {
        this.preferenceCenter.PreferenceCenterSettings = {
            Languages: { [this.language.Code]: {} },
            ...this.preferenceCenter.PreferenceCenterSettings,
        };
        return this.preferenceCenter.PreferenceCenterSettings;
    }

    protected get displayStatus(): string {
        if (this.preferenceCenter && (this.preferenceCenter.Status === crConstants.CRPreferenceCenterStatus.Inactive ||
            !this.preferenceCenter.ContainsUnpublishedData && this.preferenceCenter.Status === crConstants.CRPreferenceCenterStatus.Active)) {
            return this.preferenceCenter.Status;
        }

        return crConstants.CRPreferenceCenterStatus.UnpublishedChanges;
    }
}
