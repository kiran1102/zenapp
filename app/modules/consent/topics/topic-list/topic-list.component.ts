// Angular
import {
    OnInit,
    Component,
} from "@angular/core";
import { StateService } from "@uirouter/core";

// Components
import { CrAddTopicModalComponent } from "./add-topic-modal/add-topic-modal.component";

// Interfaces
import { IDropdownOption } from "interfaces/one-dropdown.interface";

// Rxjs
import {
    filter,
    take,
} from "rxjs/operators";

// Services
import { TopicService } from "consentModule/shared/services/cr-topic.service";
import {
    OtModalService,
    ConfirmModalType,
} from "@onetrust/vitreus";

// Models
import { ColumnDefinition } from "crmodel/column-definition";
import {
    TopicListItem,
    TopicList,
} from "crmodel/topic-list";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

@Component({
    selector: "cr-topic-list",
    templateUrl: "./topic-list.component.html",
})
export class CrTopicListComponent implements OnInit {
    tableData: TopicList;
    pageSize = 20;
    searchInProgress = false;
    columns: ColumnDefinition[];

    constructor(
        private stateService: StateService,
        private otModalService: OtModalService,
        private topicService: TopicService,
        private translatePipe: TranslatePipe,
    ) {}

    ngOnInit() {
        this.columns = [
            {
                columnName: this.translatePipe.transform("Topic"),
                cellValueKey: "Name",
                columnType: "idLink",
                route: "zen.app.pia.module.consentreceipt.views.topics.details",
                routeIdField: "Id",
            },
            {
                columnType: "actions",
                getOptions: (row: TopicListItem): IDropdownOption[] => this.getTopicOptions(row),
            },
        ];
        this.searchTopics(0);
    }

    goToRoute(cell: any) {
        this.stateService.go(cell.route, cell.params);
    }

    searchTopics(page: number = 0) {
        this.searchInProgress = true;

        this.topicService.getTopicList(page, this.pageSize).then((topics: TopicList) => {
            this.tableData = topics;
            this.searchInProgress = false;
        });
    }

    getTopicOptions(topic: TopicListItem): IDropdownOption[] {
        const actionArray: IDropdownOption[] = [];
        actionArray.push(
            {
                id: "delete",
                text: this.translatePipe.transform("Delete"),
                isDisabled: !topic.CanDelete,
                action: () => this.openConfirmationModal(topic.Id),
            },
        );
        return actionArray;
    }

    addNewTopics() {
        this.otModalService.create(
            CrAddTopicModalComponent,
            {
                isComponent: true,
            },
        ).pipe(
            take(1),
            filter((result: boolean) => result),
        ).subscribe(() => {
            this.searchTopics(0);
        });
    }

    private openConfirmationModal(topicId: string) {
        this.otModalService.confirm({
            type: ConfirmModalType.DELETE,
            translations: {
                title: this.translatePipe.transform("DeleteTopic"),
                desc: "",
                confirm: this.translatePipe.transform("ConfirmTopicDeletion"),
                cancel: this.translatePipe.transform("Cancel"),
                submit: this.translatePipe.transform("Delete"),
            },
        }).pipe(
            take(1),
            filter((result: boolean) => result),
        ).subscribe(() => {
            this.deleteTopic(topicId);
        });
    }

    private deleteTopic(topicId: string): ng.IPromise<boolean> {
        return this.topicService.deleteTopic(topicId).then((success: boolean): boolean => {
            if (success) {
                this.searchTopics(0);
            }
            return success;
        });
    }
}
