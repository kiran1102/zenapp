// Angular
import { NgModule } from "@angular/core";

// Components
import { CrTopicListComponent } from "consentModule/topics/topic-list/topic-list.component";
import { CrAddTopicModalComponent } from "consentModule/topics/topic-list/add-topic-modal/add-topic-modal.component";

// Modules
import { TopicsSharedModule } from "consentModule/topics/shared/topics-shared.module";

@NgModule({
    imports: [
        TopicsSharedModule,
    ],
    declarations: [
        CrTopicListComponent,
        CrAddTopicModalComponent,
    ],
    entryComponents: [
        CrTopicListComponent,
        CrAddTopicModalComponent,
    ],
})
export class TopicListModule { }
