// Third Party
import { Component } from "@angular/core";

// Interfaces
import { NewTopic } from "crmodel/topic-list";
import { IGetAllLanguageResponse } from "interfaces/dsar/dsar-edit-language.interface";

// Rxjs
import { Subject } from "rxjs";

// Services
import { TopicService } from "consentModule/shared/services/cr-topic.service";
import { ConsentPurposeService } from "consentModule/shared/services/cr-purpose.service";

// Templates
@Component({
    selector: "cr-add-topic-modal",
    templateUrl: "./add-topic-modal.component.html",
})
export class CrAddTopicModalComponent {
    createInProgress: boolean;
    modalTitle: string;
    topic: NewTopic = new NewTopic();
    languageList: IGetAllLanguageResponse[];
    selectedLanguage: IGetAllLanguageResponse;

    otModalCloseEvent: Subject<boolean>;

    constructor(
        private topicService: TopicService,
        private consentPurposeService: ConsentPurposeService,
    ) {
    }

    ngOnInit() {
        this.consentPurposeService.getAllLanguages().then((languages: IGetAllLanguageResponse[]) => {
            this.languageList = languages;
        });
    }

    closeModal() {
        this.otModalCloseEvent.next(false);
    }

    addTopic() {
        this.createInProgress = true;
        this.topic.DefaultLanguage = this.selectedLanguage.Code;
        this.topicService.createTopic(this.topic).then((result: string) => {
            this.createInProgress = false;
            this.topic = new NewTopic();
            if (result) {
                this.otModalCloseEvent.next(true);
            }
        });
    }

    handleChanged(payload: string, prop: keyof NewTopic) {
        this.topic[prop] = payload;
    }

    selectLanguage(language: IGetAllLanguageResponse): void {
        this.selectedLanguage = language;
    }
}
