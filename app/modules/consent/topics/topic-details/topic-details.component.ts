import {
    Component,
    OnInit,
} from "@angular/core";
import { StateService } from "@uirouter/core";
import { remove } from "lodash";

// Components
import { LanguageSelectionModal } from "sharedModules/components/language-selection-modal/language-selection-modal.component";
import { LegacyTopicLanguageSelectionModalComponent } from "./legacy-topic-language-selection-modal/legacy-topic-language-selection-modal.component";

// Interfaces
import { IEntityTranslation } from "interfaces/consent/entity-translation";
import { IGetAllLanguageResponse } from "interfaces/dsar/dsar-edit-language.interface";
import { ILanguageSelectionModalResolve } from "interfaces/language-selection-modal-resolve.interface";

// Models
import { ColumnDefinition } from "crmodel/column-definition";
import { TopicDetails } from "crmodel/topic-details";
import { TopicLanguage } from "crmodel/topic-list";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

// Rxjs
import {
    filter,
    take,
} from "rxjs/operators";

// Services
import { ConsentPurposeService } from "consentModule/shared/services/cr-purpose.service";
import { TopicService } from "consentModule/shared/services/cr-topic.service";
import { OtModalService } from "@onetrust/vitreus";

@Component({
    selector: "cr-topic-details",
    templateUrl: "./topic-details.component.html",
})
export class CrTopicDetailsComponent implements OnInit {
    canEditLanguages: boolean;
    isLoading: boolean;
    isSaving: boolean;

    topic: TopicDetails;
    topicId: string;
    columns: ColumnDefinition[];
    defaultLanguage: string;
    allLanguages: IGetAllLanguageResponse[];
    selectedLanguageList: string[];

    constructor(
        private stateService: StateService,
        private consentPurposeService: ConsentPurposeService,
        private topicService: TopicService,
        private otModalService: OtModalService,
        private translatePipe: TranslatePipe,
    ) {
        this.topicId = this.stateService.params.Id;
        this.columns = [
            { columnType: "language", columnName: this.translatePipe.transform("Language"), cellValueKey: "Language", required: true },
            { columnType: "current", columnName: this.translatePipe.transform("Label") },
        ];
    }

    ngOnInit() {
        this.isLoading = true;
        this.topicService.getTopicDetails(this.topicId).then((topic: TopicDetails) => {
            this.topic = topic;
            if (this.topic.Languages[0].Language) {
                this.setupLanguages();
            } else {
                this.legacyLanguageSelect();
            }
        }).finally(() => {
            this.isLoading = false;
        });
    }

    getLanguageList() {
        this.selectedLanguageList = this.topic.Languages.map((language: TopicLanguage): string => {
            return language.Language;
        });
    }

    getDefaultLanguage() {
        if (this.topic && this.topic.Languages) {
            this.defaultLanguage = this.topic.Languages.find((language: TopicLanguage): boolean => {
                if (language.Default) { this.sortLanguages(language); }
                return language.Default;
            }).Language;
        }
    }

    showEditLanguagesModal() {
        const languageSelectionModalData: ILanguageSelectionModalResolve = {
            defaultLanguage: this.defaultLanguage,
            selectedLanguages: this.selectedLanguageList,
            allLanguages: this.allLanguages,
        };
        this.otModalService.create(
            LanguageSelectionModal,
            {
                isComponent: true,
            },
            languageSelectionModalData,
        ).pipe(
            take(1),
            filter((response: { languages: string[], defaultLanguage: string }): boolean => {
                return response && response.languages.length && ( response.defaultLanguage.trim() !== null);
            }),
        ).subscribe(({ languages, defaultLanguage }) => {
            this.selectLanguages(languages, defaultLanguage);
        });
    }

    selectLanguages(selection: string[], defaultLanguage: string) {
        this.defaultLanguage = defaultLanguage;
        selection.forEach((item: string) => {
            if (!this.selectedLanguageList.find((language: string): boolean => {
                return item === language;
            })) {
                this.topic.Languages.push({
                    Name: this.topic.Name,
                    Language: this.allLanguages.find((language: IGetAllLanguageResponse): boolean => {
                        return item === language.Code;
                    }).Code,
                    Default: item === this.defaultLanguage,
                });
            }
        });
        this.selectedLanguageList.forEach((item: string) => {
            if (!selection.find((language: string): boolean => {
                return item === language;
            })) {
                const rowToRemove = this.topic.Languages.find((row: IEntityTranslation): boolean => {
                    return row.Language === item;
                });
                this.removeTranslation(rowToRemove);
            }
        });
        this.selectedLanguageList = selection;
        this.setDefaultLanguage(defaultLanguage);
        this.saveTopic();
    }

    legacyLanguageSelect() {
        this.otModalService.create(
            LegacyTopicLanguageSelectionModalComponent,
            {
                isComponent: true,
            },
        ).pipe(
            take(1),
        ).subscribe((result: string) => {
            if (result && result.trim()) {
                this.topic.Languages[0].Language = result;
                this.setupLanguages().then(() => {
                    this.saveTopic();
                });
            } else {
                this.stateService.go("zen.app.pia.module.consentreceipt.views.topics.list");
            }
        });
    }

    removeTranslation(row: IEntityTranslation) {
        remove(this.topic.Languages,
            (translation: IEntityTranslation): boolean => row.Language === translation.Language);
    }

    onTranslationChange(translations: IEntityTranslation[]) {
        this.topic.Languages = translations;
    }

    saveTopic() {
        this.isSaving = true;
        this.topicService.updateTopicTranslations(this.topicId, this.topic.Languages).finally(() => {
            this.isSaving = false;
        });
    }

    private setupLanguages() {
        this.getLanguageList();
        this.getDefaultLanguage();
        return this.consentPurposeService
            .getAllLanguages()
            .then((languages: IGetAllLanguageResponse[]) => {
                this.allLanguages = languages;
            });
    }

    private setDefaultLanguage(defaultLanguage: string) {
        this.topic.Languages.forEach((item: IEntityTranslation) => {
            item.Default = item.Language === defaultLanguage;
            if (item.Language === defaultLanguage) {
                this.sortLanguages(item);
            }
        });
    }

    private sortLanguages(defaultTranslation: IEntityTranslation) {
        this.topic.Languages.sort((a: IEntityTranslation, b: IEntityTranslation): number => {
            if (a.Language > b.Language) {
                return 1;
            } else if (a.Language < b.Language) {
                return -1;
            }
            return 0;
        });
        const defaultIndex = this.topic.Languages.findIndex((item: IEntityTranslation): boolean => {
            return item === defaultTranslation;
        });
        this.topic.Languages.splice(defaultIndex, 1);
        this.topic.Languages.splice(0, 0, defaultTranslation);
    }
}
