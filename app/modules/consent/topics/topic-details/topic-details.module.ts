// Angular
import { NgModule } from "@angular/core";

// Components
import { CrTopicDetailsComponent } from "consentModule/topics/topic-details/topic-details.component";
import { LegacyTopicLanguageSelectionModalComponent } from "modules/consent/topics/topic-details/legacy-topic-language-selection-modal/legacy-topic-language-selection-modal.component";

// Modules
import { TopicsSharedModule } from "consentModule/topics/shared/topics-shared.module";

@NgModule({
    imports: [
        TopicsSharedModule,
    ],
    declarations: [
        CrTopicDetailsComponent,
        LegacyTopicLanguageSelectionModalComponent,
    ],
    entryComponents: [
        CrTopicDetailsComponent,
        LegacyTopicLanguageSelectionModalComponent,
    ],
})
export class TopicDetailsModule { }
