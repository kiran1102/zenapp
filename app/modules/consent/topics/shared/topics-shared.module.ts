// Angular
import { NgModule } from "@angular/core";

// Modules
import { ConsentSharedModule } from "modules/consent/shared/consent-shared.module";

@NgModule({
    imports: [
        ConsentSharedModule,
    ],
    exports: [
        ConsentSharedModule,
    ],
})
export class TopicsSharedModule { }
