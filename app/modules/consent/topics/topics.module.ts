// Angular
import { NgModule } from "@angular/core";

// Modules
import { TopicsSharedModule } from "consentModule/topics/shared/topics-shared.module";
import { TopicDetailsModule } from "./topic-details/topic-details.module";
import { TopicListModule } from "./topic-list/topic-list.module";

@NgModule({
    imports: [
        TopicsSharedModule,
        TopicDetailsModule,
        TopicListModule,
    ],
})
export class TopicsModule { }
