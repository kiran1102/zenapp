// Angular
import { NgModule } from "@angular/core";

// Modules
import { TransactionsSharedModule } from "consentModule/transactions/shared/transactions-shared.module";
import { TransactionDetailsModule } from "./transaction-details/transaction-details.module";
import { TransactionListModule } from "./transaction-list/transaction-list.module";

@NgModule({
    imports: [
        TransactionsSharedModule,
        TransactionDetailsModule,
        TransactionListModule,
    ],
})
export class TransactionsModule { }
