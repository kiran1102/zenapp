// Angular
import { NgModule } from "@angular/core";

// Components
import { CrTransactionDetailsComponent } from "consentModule/transactions/transaction-details/transaction-details.component";

// Modules
import { TransactionsSharedModule } from "consentModule/transactions/shared/transactions-shared.module";

@NgModule({
    imports: [
        TransactionsSharedModule,
    ],
    declarations: [
        CrTransactionDetailsComponent,
    ],
    entryComponents: [
        CrTransactionDetailsComponent,
    ],
})
export class TransactionDetailsModule { }
