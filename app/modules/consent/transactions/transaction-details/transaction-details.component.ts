import {
    Component,
    OnInit,
} from "@angular/core";
import { StateService } from "@uirouter/core";
import {
    isString,
    keys,
    map,
    get,
    join,
} from "lodash";

// Models
import { TransactionDetails } from "crmodel/transaction-details";
import { CollectionPointDetails } from "crmodel/collection-point-details";
import { ReceiptJwt } from "crmodel/receipt-jwt";

// Services
import { CollectionPointService } from "consentModule/shared/services/collection-point.service";
import { ConsentTransactionService } from "consentModule/shared/services/cr-transaction.service";
import { Permissions } from "modules/shared/services/helper/permissions.service";

// Interfaces
import { IKeyValue } from "interfaces/generic.interface";

// Constants
import crConstants from "consentModule/shared/cr-constants";

@Component({
    selector: "cr-transaction-details",
    templateUrl: "./transaction-details.component.html",
})
export class CrTransactionDetailsComponent implements OnInit {
    crConstants = crConstants;
    transaction: TransactionDetails;
    collectionPoint: CollectionPointDetails;
    showReferrer: boolean;
    showPreferences: boolean;
    receiptJwt: ReceiptJwt;
    customPayloadIsString: boolean;
    private transactionId: string;

    constructor(
        private stateService: StateService,
        private permissions: Permissions,
        private consentTransactionService: ConsentTransactionService,
        protected collectionPointService: CollectionPointService,
    ) {
        this.transactionId = this.stateService.params.Id;
    }

    ngOnInit() {
        this.showReferrer = this.permissions.canShow("ConsentReceiptReferrer");
        this.showPreferences = this.permissions.canShow("ConsentReceiptPreferences");
        this.consentTransactionService.getTransactionDetails(this.transactionId)
            .then((transaction: TransactionDetails) => {
                this.transaction = transaction;
                this.customPayloadIsString = isString(this.transaction.CustomPayload);
                this.receiptJwt = this.decodeReceiptJwt(transaction.ReceiptJwt);
                this.collectionPointService.getCollectionPointDetails(transaction.CollectionPointId, transaction.CollectionPointVersion)
                    .then((collectionPoint: CollectionPointDetails) => {
                        this.collectionPoint = collectionPoint;
                    });
            });
    }

    get dataElementsList(): string {
        if (!this.receiptJwt || !this.receiptJwt.dsDataElements) {
            return "";
        }
        return join(this.receiptJwt.dsDataElements, ", ");
    }

    transactionStatusStyle(status: string): string {
        return crConstants.CRTransactionStatusInverseStyles[status];
    }

    goToDetails(route: string, id: string, version: string) {
        this.stateService.go(`zen.app.pia.module.consentreceipt.views.${route}`, { Id: id, version });
    }

    protected getCustomProperties(): Array<IKeyValue<string>> {
        return map(keys(this.transaction.CustomPayload), (key: string): IKeyValue<string> => {
            return {
                key,
                value: get(this.transaction.CustomPayload, key),
            };
        });
    }

    private decodeReceiptJwt(jwt: string): ReceiptJwt {
        if (!jwt) {
            return null;
        }
        const parts: string[] = jwt.split(".");
        if (parts.length !== 3) {
            return null;
        }
        const payload: string = window.atob(parts[1]);
        const result: ReceiptJwt = JSON.parse(payload);
        return result;
    }
}
