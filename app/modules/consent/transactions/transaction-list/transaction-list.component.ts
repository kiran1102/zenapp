// Third Party
import {
    Component,
    Inject,
} from "@angular/core";
import { StateService } from "@uirouter/core";

// Interfaces
import { IStringMap } from "interfaces/generic.interface";
import { Permissions } from "modules/shared/services/helper/permissions.service";

// Models
import { ColumnDefinition } from "crmodel/column-definition";
import { EntityItem } from "crmodel/entity-item";
import { TransactionFilters } from "crmodel/transaction-filters";
import { PurposeDetails } from "crmodel/purpose-details";
import {
    TransactionList,
    TransactionListItem,
} from "crmodel/transaction-list";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

// Services
import { CollectionPointService } from "consentModule/shared/services/collection-point.service";
import { ConsentPurposeService } from "consentModule/shared/services/cr-purpose.service";
import { ConsentTransactionService } from "consentModule/shared/services/cr-transaction.service";
import { IDropDownElement } from "consentModule/shared/services/consent-base.service";
import crConstants from "consentModule/shared/cr-constants";
import { ToastService } from "@onetrust/vitreus";

@Component({
    selector: "cr-transaction-list",
    templateUrl: "./transaction-list.component.html",
})
export class CrTransactionListComponent {
    crConstants = crConstants;

    crCollectionPoints: IDropDownElement[];
    crPurposes: IDropDownElement[];
    columns: ColumnDefinition[];
    tableData: TransactionList;
    filters: TransactionFilters = {};
    searchInProgress = true;
    readonly pageSize = 20;
    hidePageCounts: boolean;

    constructor(
        @Inject("$rootScope") readonly $rootScope: any,
        @Inject("$scope") readonly $scope: any,
        private stateService: StateService,
        private readonly consentTransactionService: ConsentTransactionService,
        readonly collectionPointService: CollectionPointService,
        private readonly consentPurposeService: ConsentPurposeService,
        private readonly toastService: ToastService,
        private readonly translatePipe: TranslatePipe,
        private readonly permissions: Permissions,
    ) {}

    ngOnInit() {
        this.hidePageCounts = this.permissions.canShow("ConsentHidePageCountsForBigTables");
        this.filters = this.consentTransactionService.handleFilters(this.stateService.params.purposeId, this.stateService.params.collectionPointId, this.stateService.params.receiptId, this.stateService.params.identifier);
        this.columns = [
            {
                columnName: this.translatePipe.transform("IdentifierValue"),
                cellValueKey: "Identifier",
                columnType: "idLink",
                route: "zen.app.pia.module.consentreceipt.views.datasubjects.details",
                routeIdField: "DataSubjectId",
            },
            {
                columnName: this.translatePipe.transform("Purpose"),
                cellValueKey: "PurposeName",
                columnType: "idLink",
                route: "zen.app.pia.module.consentreceipt.views.purposes.details",
                routeIdField: "PurposeId",
                routeVersionField: "PurposeVersion",
            },
            {
                columnName: this.translatePipe.transform("PurposeVersion"),
                cellValueKey: "PurposeVersion",
                columnType: "versionTag",
                rowToText: (row: TransactionListItem): string => this.consentTransactionService.getVersionLabel(row.PurposeVersion),
                rowToLabel: (): string => "text-bold one-tag--override one-tag--primary-white",
            },
            {
                columnName: this.translatePipe.transform("CollectionPoint"),
                cellValueKey: "CollectionPointName",
                cellClass: "max-width-50",
                columnType: "idLink",
                route: "zen.app.pia.module.consentreceipt.views.collections.details",
                routeIdField: "CollectionPointId",
                routeVersionField: "CollectionPointVersion",
            },
            {
                columnName: this.translatePipe.transform("CollectionPointVersion"),
                cellValueKey: "CollectionPointVersion",
                columnType: "versionTag",
                rowToText: (row: TransactionListItem): string => this.consentPurposeService.getVersionLabel(row.CollectionPointVersion),
                rowToLabel: (): string => "text-bold one-tag--override one-tag--primary-white",
            },
            {
                columnName: this.translatePipe.transform("TransactionNumber"),
                cellValueKey: "Id",
                columnType: "idLink",
                route: "zen.app.pia.module.consentreceipt.views.transactions.details",
            },
            {
                columnName: this.translatePipe.transform("ReceiptId"),
                cellValueKey: "ReceiptId",
            },
            {
                columnName: this.translatePipe.transform("TransactionStatus"),
                cellValueKey: "ReceiptStatus",
                columnType: "statusTag",
                rowToText: (row: TransactionListItem): string => this.translateConst(row.ReceiptStatus, this.crConstants.CRReceiptStatus),
            },
            {
                columnName: this.translatePipe.transform("TransactionDate"),
                cellValueKey: "IssueDate",
                columnType: "timestamp",
            },
        ];
        this.collectionPointService.getCollectionPointFilters().then((collectionPointFilters: EntityItem[]) => {
                if (collectionPointFilters && collectionPointFilters.length > 0) {
                    this.crCollectionPoints = collectionPointFilters.map((item: EntityItem): IDropDownElement => {
                        return { label: item.Label, value: item.Id };
                    });
                }
            });

        this.consentPurposeService.getActivePurposeFilters(true).then((purposeFilters: PurposeDetails[]) => {
            if (purposeFilters && purposeFilters.length > 0) {
                this.crPurposes = purposeFilters.map((item: PurposeDetails): IDropDownElement => {
                    return { label: item.Label, value: item.Id, version: item.Version };
                });
            }
            this.searchTransactions(this.filters);
        });
    }

    loadPage(page: number) {
        this.searchTransactions(this.filters, page);
    }

    goToRoute(cell: any) {
        this.stateService.go(cell.route, cell.params);
    }

    searchTransactions(filters: TransactionFilters = {}, page: number = 0) {
        this.filters = filters;
        this.filters.includeCounts = !this.hidePageCounts;
        this.searchInProgress = true;
        this.consentTransactionService.getTransactions(filters, page, this.pageSize)
            .then((items: TransactionList) => {
                if (!items.numberOfElements && items.number > 1) {
                    this.toastService.show(this.translatePipe.transform("NoResultsFound"), this.translatePipe.transform("EndOfList"), "warning");
                } else {
                    this.tableData = items;
                }
                this.searchInProgress = false;
            });
    }

    private translateConst(value: string, constType: IStringMap<string>): string {
        return this.consentTransactionService.translateConstValue(value, constType);
    }
}
