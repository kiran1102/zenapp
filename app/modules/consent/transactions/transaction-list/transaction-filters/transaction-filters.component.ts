// Third Party
import { orderBy } from "lodash";
import {
    Component,
    Input,
    OnChanges,
} from "@angular/core";

// Interfaces
import { IVersionListItem } from "interfaces/consent/cr-versions.interface";
import { IOTLookupSingleSelection } from "sharedModules/components/ot-template-select/ot-template-select.component";

// Models and Components
import { EntityItem } from "crmodel/entity-item";
import {
    TransactionFilters,
    TransactionFiltersSelectionsOptions,
} from "crmodel/transaction-filters";
import { CrFiltersBase } from "modules/consent/shared/cr-filters-base/cr-filters-base.component";

// Services
import { IDropDownElement } from "consentModule/shared/services/consent-base.service";
import { ConsentPurposeService } from "consentModule/shared/services/cr-purpose.service";
import { ConsentTransactionService } from "consentModule/shared/services/cr-transaction.service";
import { CollectionPointService } from "consentModule/shared/services/collection-point.service";

@Component({
    selector: "cr-transaction-filters",
    templateUrl: "./transaction-filters.component.html",
})
export class CrTransactionFiltersComponent extends CrFiltersBase<TransactionFilters> implements OnChanges {
    @Input() filters: TransactionFilters;
    @Input() crPurposes: IDropDownElement[];
    @Input() crCollectionPoints: IDropDownElement[];

    constructor(
        private collectionPointService: CollectionPointService,
        private consentPurposeService: ConsentPurposeService,
        private consentTransactionService: ConsentTransactionService,
    ) {
        super();
    }

    ngOnChanges() {
        this.populateFilters();
        this.filters.identifier = sessionStorage.getItem("ConsentTransactionIdentifierFilter");
        this.filters.guid = sessionStorage.getItem("ConsentTransactionGuidFilter");
        this.filters.receiptGuid = sessionStorage.getItem("ConsentTransactionReceiptGuidFilter");
        const result: TransactionFiltersSelectionsOptions = this.consentTransactionService.getSessionStorageFilters(this.crPurposes, this.crCollectionPoints, this.options);
        if (result) {
            this.selections = result.selections;
            this.options = result.options;
        }
    }

    handleApply() {
        this.isDrawerOpen = false;
        this.consentTransactionService.setSessionStorageFilters(this.filters);
        super.handleApply();
    }

    handleClear() {
        super.handleClear();
        this.consentTransactionService.clearSessionStorageFilters();
        this.filters.purposeGuid = "";
        this.filters.collectionPointGuid = "";
        this.options.purposeVersion = null;
        this.options.collectionPointVersion = null;
    }

    handleSearch(model: string = "", clear: boolean = false) {
        this.filters.identifier = clear ? "" : model;
        if (!clear) {
            this.handleApply();
        }
    }

    get hasValidFilters(): boolean {
        const validFilters = this.isValidGuidFilter("guid") && this.isValidGuidFilter("receiptGuid");
        return validFilters;
    }

    selectPurpose(selection: IOTLookupSingleSelection<IDropDownElement>) {
        this.selectOption("purposeGuid", selection.currentValue);
        this.selectOption("purposeVersion", null);
        this.getFilteredPurposeVersions(selection.currentValue);
    }

    selectCollectionPoint(selection: IOTLookupSingleSelection<IDropDownElement>) {
        this.selectOption("collectionPointGuid", selection.currentValue);
        this.selectOption("collectionPointVersion", null);
        this.getFilteredCollectionPointVersions(selection.currentValue);
    }

    protected populateFilters() {
        this.options.purposeGuid = this.crPurposes;
        this.options.collectionPointGuid = this.crCollectionPoints;
    }

    private getFilteredPurposeVersions(selectedPurpose: IDropDownElement) {
        if (selectedPurpose && selectedPurpose.value) {
            this.consentPurposeService.getFilteredPurposeVersions(selectedPurpose.value).then((result: EntityItem[]) => {
                if (result && result.length > 0) {
                    this.options.purposeVersion = orderBy(result, "Version", "desc").map((item: IVersionListItem): IDropDownElement => {
                        return {
                            label: `${item.Label} (${this.consentPurposeService.getVersionLabel(item.Version)})`,
                            value: item.Id,
                            version: item.Version,
                        };
                    });
                }
            });
        } else {
            this.options.purposeVersion = null;
        }
    }

    private getFilteredCollectionPointVersions(selectedCollectionPoint: IDropDownElement) {
        if (selectedCollectionPoint && selectedCollectionPoint.value) {
            this.collectionPointService.getFilteredCollectionPointVersions(selectedCollectionPoint.value).then((result: EntityItem[]) => {
                if (result && result.length > 0) {
                    this.options.collectionPointVersion = orderBy(result, "Version", "desc").map((item: IVersionListItem): IDropDownElement => {
                        return {
                            label: `${item.Label} (${this.collectionPointService.getVersionLabel(item.Version)})`,
                            value: item.Id,
                            version: item.Version,
                        };
                    });
                }
            });
        } else {
            this.options.collectionPointVersion = null;
        }
    }
}
