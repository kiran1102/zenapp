// Angular
import { NgModule } from "@angular/core";

// Components
import { CrTransactionListComponent } from "consentModule/transactions/transaction-list/transaction-list.component";
import { CrTransactionFiltersComponent } from "consentModule/transactions/transaction-list/transaction-filters/transaction-filters.component";

// Modules
import { TransactionsSharedModule } from "consentModule/transactions/shared/transactions-shared.module";

@NgModule({
    imports: [
        TransactionsSharedModule,
    ],
    declarations: [
        CrTransactionListComponent,
        CrTransactionFiltersComponent,
    ],
    entryComponents: [
        CrTransactionListComponent,
    ],
})
export class TransactionListModule { }
