import { NgModule } from "@angular/core";

// Modules
import { CollectionPointsModule } from "consentModule/collection-points/collection-points.module";
import { CustomPreferencesModule } from "consentModule/custom-preferences/custom-preferences.module";
import { CrDashboardModule } from "consentModule/dashboard/dashboard.module";
import { DataSubjectsModule } from "consentModule/data-subjects/data-subjects.module";
import { PreferenceCentersModule } from "consentModule/preferences/preference-centers.module";
import { PurposesModule } from "consentModule/purposes/purposes.module";
import { TopicsModule } from "consentModule/topics/topics.module";
import { TransactionsModule } from "consentModule/transactions/transactions.module";

@NgModule({
    imports: [
        CollectionPointsModule,
        CustomPreferencesModule,
        CrDashboardModule,
        DataSubjectsModule,
        PreferenceCentersModule,
        PurposesModule,
        TopicsModule,
        TransactionsModule,
    ],
})
export class ConsentModule { }
