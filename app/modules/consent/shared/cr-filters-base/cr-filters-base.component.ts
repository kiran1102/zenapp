import {
    Output,
    EventEmitter,
    Input,
    OnInit,
} from "@angular/core";
import {
    isFunction,
    find,
    filter as lodashFilter,
    uniqBy,
} from "lodash";

// Constants and Utilities
import Utilities from "Utilities";
import { GUID } from "constants/regex.constant";

// Interfaces
import { IDropDownElement } from "consentModule/shared/services/consent-base.service";
import { IStringMap, IKeyValue } from "interfaces/generic.interface";
import { IOTLookupSingleSelection } from "sharedModules/components/ot-template-select/ot-template-select.component";

export abstract class CrFiltersBase<TFilters extends IStringMap<any>> implements OnInit {
    @Input() filters: TFilters;
    @Input() isDrawerOpen = false;
    @Output() refreshCallback: EventEmitter<TFilters> = new EventEmitter<TFilters>();
    @Output() applyCallback: EventEmitter<TFilters> = new EventEmitter<TFilters>();

    options: { [key in keyof TFilters]?: IDropDownElement[] } = {};
    filteredOptions: { [key in keyof TFilters]?: IDropDownElement[] } = {};
    selections: { [key in keyof TFilters]?: IDropDownElement } = {};
    inputs: { [key in keyof TFilters]?: string } = {};

    protected onSettings: () => void;
    protected onFilter: () => void;

    ngOnInit() {
        if (!this.filters) {
            this.filters = {} as TFilters;
        }
        this.populateFilters();

    }

    handleSearchKeyPress(event: any) {
        if (event && event.which === 13) {
            this.handleApply();
        }
    }

    handleRefresh() {
        this.refreshCallback.emit(this.filters);
    }

    handleApply() {
        this.isDrawerOpen = false;
        this.applyCallback.emit(this.filters);
    }

    handleFilter() {
        if (isFunction(this.onFilter)) {
            this.onFilter();
        }
    }

    handleClear() {
        this.filters = {} as TFilters;
        this.selections = {};
        this.inputs = {};
        this.filteredOptions = {};
    }

    isValidGuidFilter(prop: keyof TFilters): boolean {
        const filter = this.filters[prop];
        if (!filter) {
            return true;
        }
        return Utilities.matchRegex(filter, GUID);
    }

    setFilterValue(prop: string, value: string | number) {
        if (this.filters) {
            this.filters[prop] = value;
        }
    }

    selectOption(prop: string, selection: IDropDownElement, valueKey: keyof IDropDownElement = "value") {
        this.selections[prop] = selection;
        this.filters[prop] = selection ? selection[valueKey] : undefined;
        this.inputs[prop] = selection ? selection.label : "";
    }

    selectLookupOption(prop: string, selection: IOTLookupSingleSelection<IDropDownElement>) {
        this.selectOption(prop, selection.currentValue);
    }

    onLookupInputChange(prop: string, event: IKeyValue<string>) {
        if (event.key === "Enter") {
            this.selectOption(prop, this.filteredOptions[prop][0] || this.options[prop][0]);
            return;
        }
        if (event.value) {
            const needle = event.value.toLowerCase();
            this.filteredOptions[prop] = lodashFilter(uniqBy(this.options[prop], "label"),
                (option: IDropDownElement): boolean => option.label.toLowerCase().indexOf(needle) !== -1);
        } else {
            this.filteredOptions[prop] = null;
        }
    }

    updateSelectionInput(prop: string) {
        const selection = find(this.options[prop], (option): boolean => option.value === this.selections[prop].value);
        this.inputs[prop] = selection ? selection.label : "";
    }

    toggleDrawer() {
        this.isDrawerOpen = !this.isDrawerOpen;
    }

    handleTextSearch(prop: string, model: string, storeItem: string) {
        this.filters[prop] = model;
        if (model === "") { sessionStorage.removeItem(storeItem); }
        this.handleApply();
    }

    protected abstract populateFilters();
}
