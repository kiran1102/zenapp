// 3rd Party
import {
    Component,
    Input,
    Inject,
} from "@angular/core";
import { StateService } from "@uirouter/core";

@Component({
    selector: "cr-header",
    templateUrl: "./cr-header.component.html",
    host: {
        class: "full-width",
    },
})
export class CrHeader {
    @Input() title: string;
    @Input() parentLabel: string;
    @Input() parentRoute: string;
    @Input() breadcrumbLabel: string;

    constructor(
        private stateService: StateService,
    ) {}

    onHeaderRouteClick(route: { path: string, params: any }) {
        this.stateService.go(route.path, route.params);
    }
}
