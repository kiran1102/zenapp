// Third Party
import {
    ChangeDetectionStrategy,
    Component,
    Input,
} from "@angular/core";

// Interfaces
import { ColumnDefinition } from "crmodel/column-definition";

import {
    EntityCustomPreferencesOptionsConsented,
} from "crmodel/entity-custom-preferences";
import { PurposeCustomPreference } from "crmodel/custom-preference-list";

enum ColumnType {
    Name = "Name",
    Id = "Id",
}

@Component({
    selector: "cr-consented-custom-preferences-options",
    templateUrl: "./cr-consented-custom-preferences-options.component.html",
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CrConsentedCustomPreferencesOptionsComponent {
    @Input() customPreferencesOptionsConsented: EntityCustomPreferencesOptionsConsented[] = [];
    @Input() customPreferences: PurposeCustomPreference[] = [];

    readonly columns: ColumnDefinition[] = [
        {
            columnName: "",
            cellValueKey: ColumnType.Name,
        },
        {
            columnName: "",
            cellValueKey: ColumnType.Id,
        },
    ];

    readonly columnTypes = ColumnType;

    isOptionConsented(customPreferenceId: string, optionId: string): boolean {
        const consentedCustomPreference = this.customPreferencesOptionsConsented
            .find((customPreference) => customPreference.Id === customPreferenceId);

        return consentedCustomPreference && consentedCustomPreference.Options.includes(optionId);
    }

    trackByFn(index: number, customPreference: PurposeCustomPreference): string {
        return customPreference.PurposeCustomPreferenceId;
    }
}
