// 3rd Party
import { get, each, keys, pickBy } from "lodash";
import {
    Component,
    EventEmitter,
    Input,
    Output,
} from "@angular/core";

// Interfaces
import { IPageable } from "interfaces/pagination.interface";
import { IBasicTableCell } from "interfaces/tables.interface";
import { IDropdownOption } from "interfaces/one-dropdown.interface";

// Models
import { ColumnDefinition } from "crmodel/column-definition";

@Component({
    selector: "cr-table",
    templateUrl: "./cr-table.component.html",
})
export class CrTable {
    @Input() rows: IPageable;
    @Input() columns: ColumnDefinition[];
    @Input() loading: boolean;
    @Input() noDataKey: string;
    @Input() errorKey: string;
    @Input() resizable = true;
    @Input() responsive = true;
    @Input() noCounts = false;
    @Input() rowFlagKey: string;
    @Input() noDataRoutePermission = true;
    @Output() goToRoute: EventEmitter<IBasicTableCell> = new EventEmitter<IBasicTableCell>();
    @Output() pageChange: EventEmitter<number> = new EventEmitter<number>();
    @Output() selectionChange: EventEmitter<string[]> = new EventEmitter<string[]>();
    @Output() noDataRoute: EventEmitter<void> = new EventEmitter<void>();

    sortable = false;
    sortOrder: string;
    sortedKey: string | number;
    rowBordered = true;
    rowSelection: { [id: string]: boolean } = {};
    ellipsisOptions: IDropdownOption[] = [];

    resolveProperty(object: any, path: string): string {
        if (!object) {
            return "";
        }
        return get(object, path);
    }

    handleSortChange(event: any): void {
        // to do: implement module-wide sorting by column
    }

    getEllipsisOptions(col: any, row: any) {
        this.ellipsisOptions = col.getOptions(row);
    }

    get allRowsSelected(): boolean {
        return this.rows && this.rows.content && this.selectedRowIds.length === this.rows.content.length;
    }

    get selectedRowIds(): string[] {
        return keys(pickBy(this.rowSelection));
    }

    get showPaginationControl(): boolean {
        return (!this.noCounts && this.rows && this.rows.totalElements > this.rows.size)
            || (this.noCounts && this.rows && this.rows.totalElements >= this.rows.size || this.rows.number > 1);
    }

    get paginationFormat(): string {
        return this.noCounts ? "nocount" : "";
    }

    handleSelectAll(columnValue: string) {
        const newValue = !this.allRowsSelected;
        this.setAllSelections(columnValue, newValue);
        this.selectionChange.emit(this.selectedRowIds);
    }

    handleSelectRow(rowId: string) {
        this.rowSelection[rowId] = !this.rowSelection[rowId];
        this.selectionChange.emit(this.selectedRowIds);
    }

    generateRoute(row: any, col: ColumnDefinition) {
        if (col.routeVersionField) {
            this.goToRoute.emit({ route: col.route, params: { Id: row[col.routeIdField || "Id"], version: row[col.routeVersionField] } });
        } else {
            this.goToRoute.emit({ route: col.route, params: { Id: row[col.routeIdField || "Id"] } });
        }
    }

    private setAllSelections(columnValue: string, value: boolean) {
        if (!this.rows || !this.rows.content) {
            return;
        }
        each(this.rows.content, (row) => {
            const rowId = row[columnValue];
            this.rowSelection[rowId] = value;
        });
    }
}
