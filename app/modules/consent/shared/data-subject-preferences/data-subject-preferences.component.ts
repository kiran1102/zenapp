// Third Party
import { some } from "lodash";
import {
    Component,
    Inject,
    Input,
} from "@angular/core";

// Models
import { ColumnDefinition } from "crmodel/column-definition";
import { TopicListItem } from "crmodel/topic-list";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

// Services
import { ITopicPreference } from "consentModule/shared/services/consent-base.service";

@Component({
    selector: "cr-data-subject-preferences",
    templateUrl: "./data-subject-preferences.component.html",
})

export class CrDataSubjectPreferencesComponent {
    @Input() preferences: ITopicPreference[];
    @Input() topics: TopicListItem[];

    columns: ColumnDefinition[];

    constructor(
        @Inject("crConstants") readonly crConstants: any,
        private translatePipe: TranslatePipe,
    ) {
        this.columns = [
            {
                columnName: this.translatePipe.transform("Topic"),
                cellValueKey: "Name",
            },
            {
                columnName: "",
                cellValueKey: "Id",
            },
        ];
    }

    private isTopicActive(topicId: string): boolean {
        return some(this.preferences, (p: ITopicPreference): boolean => p.TopicId === topicId);
    }
}
