// Third Party
import {
    filter,
    orderBy,
} from "lodash";
import {
    Injectable,
    Inject,
} from "@angular/core";
import { IPromise } from "angular";

// Constants
import { Regex } from "constants/regex.constant";
import crConstants from "consentModule/shared/cr-constants";

// Interfaces
import {
    IProtocolMessage,
    IProtocolPacket,
    IProtocolResponse,
} from "interfaces/protocol.interface";
import { IVersionListItem } from "interfaces/consent/cr-versions.interface";
import { IOrgUserAdapted } from "interfaces/org-user.interface";
import { OrgUserTraversal } from "enums/org-user-dropdown.enum";

// Models
import {
    CollectionPointList,
    CollectionPointListItem,
} from "crmodel/collection-point-list";
import { CollectionPointDetails } from "crmodel/collection-point-details";
import {
    CreateCollectionPoint,
    UpdateCollectionPoint,
} from "crmodel/create-collection-point";
import {
    CollectionPointFilters,
    CollectionPointFiltersSelectionsOptions,
} from "crmodel/collection-point-filters";
import { CollectionPointReport } from "crmodel/collection-point-report";
import { BulkUpdateCollectionPoint } from "crmodel/collection-point-bulk-update";
import { EntityItem } from "crmodel/entity-item";
import {
    CollectionPointSettings,
    SdkSettings,
} from "crmodel/sdk-settings";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

// Services
import {
    ConsentBaseService,
    IDropDownElement,
} from "consentModule/shared/services/consent-base.service";
import { ConsentPurposeService } from "consentModule/shared/services/cr-purpose.service";
import { OrgGroupApiService } from "oneServices/api/org-group-api.service";
import { Principal } from "modules/shared/services/helper/principal.service";
import { ProtocolService } from "modules/core/services/protocol.service";

// Enums
import { Endpoints } from "consentModule/enums/endpoints.enum";

@Injectable()
export class CollectionPointService extends ConsentBaseService {
    constructor(
        protected OneProtocol: ProtocolService,
        protected translatePipe: TranslatePipe,
        protected principal: Principal,
        private consentPurposeService: ConsentPurposeService,
        private orgGroupApiService: OrgGroupApiService,
    ) {
        super(OneProtocol, translatePipe, principal);
    }

    getCollectionPointList(filters: CollectionPointFilters, page: number = 0, size: number = 10): ng.IPromise<CollectionPointList> {
      const method = "GET";
      const url = `${Endpoints.CollectionPoints}?page=${page}&size=${size}&sort=CreatedDate,desc`;
      const config = this.OneProtocol.customConfig(method, url, filters);
      const mapper = (response: IProtocolPacket) => response.result ? response.data : null;
      return this.customApiRequest(config);
    }

    getCollectionPointDetails(id: string, version: number = 1): ng.IPromise<CollectionPointDetails> {
      const method = "GET";
      const url = `${Endpoints.CollectionPoints}/${id}?version=${version}`;
      const config = this.OneProtocol.customConfig(method, url, null);
      const mapper = (response: IProtocolPacket) => response.result ? response.data : null;
      return this.customApiRequest(config, null, mapper);
    }

    getCollectionPointToken(id: string, version: number = 1): ng.IPromise<string> {
      const method = "GET";
      const url = `${Endpoints.CollectionPoints}/${id}/token?version=${version}`;
      const config = this.OneProtocol.customConfig(method, url, null);
      const mapper = (response: IProtocolPacket) => response.result ? response.data.token : null;
      return this.customApiRequest(config, null, mapper);
    }

    getCollectionPointSettings(id: string, version: number = 1): ng.IPromise<SdkSettings> {
      const method = "GET";
      const url = `${Endpoints.CollectionPoints}/${id}/settings?version=${version}`;
      const config = this.OneProtocol.customConfig(method, url, null);
      const mapper = (response: IProtocolPacket) => response.result ? response.data.settingsConfiguration : null;
      return this.customApiRequest(config, null, mapper);
    }

    getCollectionPointMobileSdkSetting(id: string): ng.IPromise<SdkSettings> {
      const method = "GET";
      const url = `${Endpoints.CollectionPoints}/${id}/settings`;
      const config = this.OneProtocol.customConfig(method, url, null);
      const mapper = (response: IProtocolResponse<CollectionPointSettings>) => response.result ? response.data.settingsConfiguration : null;
      return this.customApiRequest(config, null, mapper);
    }

    getCollectionPointDownloadSetting(fileName: string): ng.IPromise<void> {
      const method = "GET";
      const url = `/api/consentmanager/v1/mobilesdk/download`;
      const config = this.OneProtocol.customConfig(method, url, { fileName }, null, null, null, null, "arraybuffer");
      const mapper = (response: IProtocolResponse<string>) =>  response;
      return this.customApiRequest(config, null, mapper).then((response: IProtocolResponse<string>): void => {
                      if (response) {
                          this.downloadContent(fileName, response.data);
                      }
                  });
    }

    getCollectionPointReport(id: string): ng.IPromise<CollectionPointReport> {
      const method = "GET";
      const url = `${Endpoints.CollectionPoints}/${id}/statistics`;
      const config = this.OneProtocol.customConfig(method, url, null);
      const mapper = (response: IProtocolPacket) => response.result ? response.data : null;
      return this.customApiRequest(config, null, mapper);
    }

    getCollectionPointFilters(id: string = ""): ng.IPromise<EntityItem[]> {
      const method = "GET";
      const url = `${Endpoints.CollectionPoints}/filters?id=${id}&status=ACTIVE`;
      const config = this.OneProtocol.customConfig(method, url, null);
      const mapper = (response: IProtocolPacket) => response.result ? response.data : null;
      return this.customApiRequest(config, null, mapper);
    }

    createCollectionPoint(newCollectionPoint: CreateCollectionPoint): ng.IPromise<string> {
        const successMessage = this.translatePipe.transform("CollectionPointCreated");
        const errorMessage = this.translatePipe.transform("CollectionPointCreationError");
        const messages = this.buildMessages(successMessage, errorMessage);

        const method = "POST";
        const url = Endpoints.CollectionPoints;
        const config = this.OneProtocol.customConfig(method, url, null, newCollectionPoint);
        const mapper = (response: IProtocolPacket) => response.result ? response.data.Id : null;

        return this.customApiRequest(config, messages, mapper);
    }

    updateCollectionPoint(id: string, version: number = 1, collectionPoint: UpdateCollectionPoint, showToaster: boolean = true): ng.IPromise<boolean> {
        const successMessage = showToaster ? this.translatePipe.transform("CollectionPointUpdated") : undefined;
        const errorMessage  = showToaster ? this.translatePipe.transform("CollectionPointUpdateError")  : undefined;
        const messages = this.buildMessages(successMessage, errorMessage);

        const method = "PUT";
        const url = `${Endpoints.CollectionPoints}/${id}?version=${version}`;
        const config = this.OneProtocol.customConfig(method, url, null, collectionPoint);
        const mapper = (response: IProtocolPacket): boolean => response.result;

        return this.customApiRequest(config, messages, mapper);
    }

    updateConsentTypeList(purposeArray: EntityItem[], consentTypes: IDropDownElement[]): IDropDownElement[] {
        if (this.hasMultiplePurposes(purposeArray)) {
            consentTypes = filter(consentTypes, (crType: IDropDownElement): boolean => !this.isSinglePurposeConsentType(crType.value));
        } else {
            consentTypes = this.getDropDownOptions(crConstants.CRConsentTypesNoCookie);
        }

        return consentTypes;
    }

    modifyConsentType(purposeArray: EntityItem[], consentTypes: IDropDownElement[], currentConsentType: string): IDropDownElement {
        if (this.hasMultiplePurposes(purposeArray) && this.isSinglePurposeConsentType(currentConsentType)) {
            currentConsentType = crConstants.CRConsentTypes.OptInCheckboxFormSubmit;
        }

        const result = consentTypes.find((type) => type.value === currentConsentType);
        return result;
    }

    validateLink(link: string): boolean {
        if (!link) {
            return false;
        }
        return Regex.LINK.test(link);
    }

    getDataElements(): ng.IPromise<EntityItem[]> {
    //  return this.apiRequest("GET", `/CollectionPoint/GetDataElements`)
    //    .then((response: IProtocolPacket): EntityItem[] => response.result ? response.data : null);
      const method = "GET";
      const url = Endpoints.DataElements;
      const config = this.OneProtocol.customConfig(method, url);
      const mapper = (response: IProtocolPacket) => response.result ? response.data : null;
      return this.customApiRequest(config, null, mapper);
    }

    getCollectionPointsForUpdate(purposeGuid: string, purposeVersion: number, jwt = false, page: number = 0, size: number = 10, sort: string = "CreatedDate,desc"): ng.IPromise<CollectionPointList> {
      const method = "GET";
      const url = `${Endpoints.CollectionPoints}/purposes?purposeVersion<=${purposeVersion}`;
      const params = { purposeGuid, page, size, sort, jwt };
      const config = this.OneProtocol.customConfig(method, url, params);
      const mapper = (response: IProtocolPacket) => response.result ? response.data : null;
      return this.customApiRequest(config, null, mapper);
    }

    bulkUpdateCollectionPoints(collectionPoints: CollectionPointListItem[], purposeId: string, purposeVersion: number): ng.IPromise<boolean> {
        const successMessage = this.translatePipe.transform("CollectionPointsUpdated");
        const errorMessage = this.translatePipe.transform("CollectionPointsUpdateError");
        const messages = this.buildMessages(successMessage, errorMessage);

        const method = "PUT";
        const url = `${Endpoints.CollectionPoints}/purposes/${purposeId}/version/${purposeVersion}`;
        const payload: BulkUpdateCollectionPoint = new BulkUpdateCollectionPoint();
        payload.collectionPointsToUpdate = collectionPoints.map((collectionPoint: CollectionPointDetails) => {
            return {
                CollectionPointGuid: collectionPoint.Id,
                CollectionPointVersion: collectionPoint.Version,
            };
        });
        const config = this.OneProtocol.customConfig(method, url, null, payload);
        const mapper = (response: IProtocolResponse<boolean>): boolean => response.result;

        return this.customApiRequest(config, messages, mapper);
    }

    createNewVersion(id: string): ng.IPromise<CollectionPointDetails> {
        const successMessage = this.translatePipe.transform("NewCollectionPointVersionCreated");
        const errorMessage = this.translatePipe.transform("NewCollectionPointVersionCreationError");
        const messages = this.buildMessages(successMessage, errorMessage);

        const method = "POST";
        const url = `${Endpoints.CollectionPoints}/version/${id}`;
        const config = this.OneProtocol.customConfig(method, url, null);
        const mapper = (response: IProtocolResponse<CollectionPointDetails>) => response.data;

        return this.customApiRequest(config, messages, mapper);
    }

    getSelectedCollectionPointVersions(collectionPointId: string): ng.IPromise<IVersionListItem[]> {
      const method = "GET";
      const url = `${Endpoints.CollectionPoints}/${collectionPointId}/versions`;
      const config = this.OneProtocol.customConfig(method, url, null);
      const mapper = (response: IProtocolResponse<IVersionListItem[]>) => response.result ? response.data : null;
      return this.customApiRequest(config, null, mapper);
    }

    getFilteredCollectionPointVersions(collectionPointId: string): ng.IPromise<IVersionListItem[]> {
      const method = "GET";
      const url = `${Endpoints.CollectionPoints}/${collectionPointId}/versions?status=ACTIVE&status=RETIRED`;
      const config = this.OneProtocol.customConfig(method, url, null);
      const mapper = (response: IProtocolResponse<IVersionListItem[]>) => response.result ? response.data : null;
      return this.customApiRequest(config, null, mapper);
    }

    handleFilters(purposeId: string, purposeVersion: number): CollectionPointFilters {
        const filters: CollectionPointFilters = {
            purposeGuid:         purposeId || sessionStorage.getItem("ConsentCollectionPointPurposeGuidFilter"),
            purposeVersion:      purposeVersion || Number(sessionStorage.getItem("ConsentCollectionPointPurposeVersionFilter")) || undefined,
            name:                sessionStorage.getItem("ConsentCollectionPointNameFilter"),
            collectionPointType: sessionStorage.getItem("ConsentCollectionPointTypeFilter"),
            consentType:         sessionStorage.getItem("ConsentCollectionPointConsentTypeFilter"),
            status:              sessionStorage.getItem("ConsentCollectionPointStatusFilter"),
        };

        if (purposeId) {
            this.clearSessionStorageFilters();
            sessionStorage.setItem("ConsentCollectionPointPurposeGuidFilter", filters.purposeGuid);
            if (purposeVersion) { sessionStorage.setItem("ConsentCollectionPointPurposeVersionFilter", "" + filters.purposeVersion); }
        }

        return filters;
    }

    getSessionStorageFilters(purposeList: IDropDownElement[], options: { [key in keyof CollectionPointFilters]?: IDropDownElement[] }): CollectionPointFiltersSelectionsOptions {
        const selections: { [key in keyof CollectionPointFilters]?: IDropDownElement } = {};

        if (purposeList && sessionStorage.getItem("ConsentCollectionPointPurposeGuidFilter")) {
            selections.purposeGuid = purposeList.find((option: IDropDownElement): boolean => {
                return option.value === sessionStorage.getItem("ConsentCollectionPointPurposeGuidFilter");
            });

            this.consentPurposeService.getFilteredPurposeVersions(selections.purposeGuid.value).then((result: IVersionListItem[]) => {
                if (!result || result.length === 0) {
                    return;
                }
                options.purposeVersion = orderBy(result, "Version", "desc").map((item: IVersionListItem): IDropDownElement => {
                    return {
                        label: `${item.Label} (${this.consentPurposeService.getVersionLabel(item.Version)})`,
                        value: item.Id,
                        version: item.Version,
                    };
                });
                if (!sessionStorage.getItem("ConsentCollectionPointPurposeVersionFilter")) {
                    return;
                }
                selections.purposeVersion = options.purposeVersion.find((option: IDropDownElement): boolean => {
                    return option.version === Number(sessionStorage.getItem("ConsentCollectionPointPurposeVersionFilter"));
                });
            });
        }

        if (sessionStorage.getItem("ConsentCollectionPointTypeFilter")) {
            selections.collectionPointType = options.collectionPointType.find((option: IDropDownElement): boolean => {
                return option.value === sessionStorage.getItem("ConsentCollectionPointTypeFilter");
            });
        }

        if (sessionStorage.getItem("ConsentCollectionPointConsentTypeFilter")) {
            selections.consentType = options.consentType.find((option: IDropDownElement): boolean => {
                return option.value === sessionStorage.getItem("ConsentCollectionPointConsentTypeFilter");
            });
        }

        if (sessionStorage.getItem("ConsentCollectionPointStatusFilter")) {
            selections.status = options.status.find((option: IDropDownElement): boolean => {
                return option.value === sessionStorage.getItem("ConsentCollectionPointStatusFilter");
            });
        }

        return { selections, options };
    }

    setSessionStorageFilters(filters: CollectionPointFilters) {
        if (filters.name) {
            sessionStorage.setItem("ConsentCollectionPointNameFilter", filters.name);
        } else {
            sessionStorage.removeItem("ConsentCollectionPointNameFilter");
        }

        if (filters.purposeGuid) {
            sessionStorage.setItem("ConsentCollectionPointPurposeGuidFilter", filters.purposeGuid);
        } else {
            sessionStorage.removeItem("ConsentCollectionPointPurposeGuidFilter");
        }

        if (filters.collectionPointType) {
            sessionStorage.setItem("ConsentCollectionPointTypeFilter", filters.collectionPointType);
        } else {
            sessionStorage.removeItem("ConsentCollectionPointTypeFilter");
        }

        if (filters.consentType) {
            sessionStorage.setItem("ConsentCollectionPointConsentTypeFilter", filters.consentType);
        } else {
            sessionStorage.removeItem("ConsentCollectionPointConsentTypeFilter");
        }

        if (filters.status) {
            sessionStorage.setItem("ConsentCollectionPointStatusFilter", filters.status);
        } else {
            sessionStorage.removeItem("ConsentCollectionPointStatusFilter");
        }
    }

    clearSessionStorageFilters() {
        sessionStorage.removeItem("ConsentCollectionPointNameFilter");
        sessionStorage.removeItem("ConsentCollectionPointPurposeGuidFilter");
        sessionStorage.removeItem("ConsentCollectionPointTypeFilter");
        sessionStorage.removeItem("ConsentCollectionPointConsentTypeFilter");
        sessionStorage.removeItem("ConsentCollectionPointStatusFilter");
    }

    getSelectedOrgUsers(organizationId: string): IPromise<IProtocolResponse<IOrgUserAdapted[]>> {
        return this.orgGroupApiService.getOrgUsersWithPermission(organizationId, OrgUserTraversal.Current)
            .then((response: IProtocolResponse<IOrgUserAdapted[]>) => {
                return response;
            });
    }

    private isSinglePurposeConsentType(consentType: string): boolean {
        return consentType === crConstants.CRConsentTypes.FormSubmitOnly
            || consentType === crConstants.CRConsentTypes.CustomSingleTrigger;
    }

    private hasMultiplePurposes(selectedPurposes: EntityItem[]): boolean {
        return selectedPurposes && selectedPurposes.length > 1;
    }
}
