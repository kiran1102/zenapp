// Third Party
import { map } from "lodash";
import { Injectable, Inject } from "@angular/core";

// Interfaces
import {
    IProtocolMessage,
    IProtocolResponse,
    IProtocolConfig,
} from "interfaces/protocol.interface";
import { IEntityTranslation } from "interfaces/consent/entity-translation";

// Models
import { EntityItem } from "crmodel/entity-item";
import { TopicDetails } from "crmodel/topic-details";
import {
    TopicList,
    TopicListItem,
    NewTopic,
} from "crmodel/topic-list";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

// Services
import { ProtocolService } from "modules/core/services/protocol.service";
import { ConsentBaseService } from "consentModule/shared/services/consent-base.service";
import { Principal } from "sharedModules/services/helper/principal.service";

@Injectable()
export class TopicService extends ConsentBaseService {
    constructor(
        protected OneProtocol: ProtocolService,
        protected translatePipe: TranslatePipe,
        protected principal: Principal,
    ) {
        super(OneProtocol, translatePipe, principal);
    }

    getTopicList(page: number = 0, size: number = 10): ng.IPromise<TopicList> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("GET", `/api/consentmanager/v1/topics?page=${page}&size=${size}&sort=name,asc`);
        return this.OneProtocol.http(config)
        .then((response: IProtocolResponse<TopicList>): TopicList => response.result ? response.data : null);
    }

    getTopicDetails(id: string): ng.IPromise<TopicDetails> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("GET", `/api/consentmanager/v1/topics/${id}`);
        return this.OneProtocol.http(config)
        .then((response: IProtocolResponse<TopicDetails>): TopicDetails => response.result ? response.data : null);
    }

    getTopicFilters(): ng.IPromise<EntityItem[]> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("GET", `/api/consentmanager/v1/topics?pagedisabled`);
        return this.OneProtocol.http(config)
        .then((response: IProtocolResponse<TopicListItem[]>): EntityItem[] => {
            if (!response.result) return null;

            return map(response.data, (item: TopicListItem): EntityItem => {
                return {
                    Id: item.Id,
                    Label: item.Name,
                };
            });
        });
    }

    createTopic(newTopic: NewTopic): ng.IPromise<string> {
        const successMessage: IProtocolMessage = { custom: this.translatePipe.transform("TopicCreated") };
        const errorMessage: IProtocolMessage = { custom: this.translatePipe.transform("TopicCreationError") };
        const config: IProtocolConfig = this.OneProtocol.customConfig("POST", `/api/consentmanager/v1/topics`, null, newTopic);
        return this.OneProtocol.http(config, { Error: errorMessage, Success: successMessage} )
        .then((response: IProtocolResponse<TopicListItem>): any => response.result ? response.data.Id : null);
    }

    deleteTopic(topicId: string): ng.IPromise<boolean> {
        const successMessage: IProtocolMessage = { custom: this.translatePipe.transform("TopicDeleted") };
        const errorMessage: IProtocolMessage = { custom: this.translatePipe.transform("TopicDeleteError") };
        const config: IProtocolConfig = this.OneProtocol.customConfig("DELETE", `/api/consentmanager/v1/topics/${topicId}`);
        return this.OneProtocol.http(config, {Error: errorMessage, Success: successMessage})
        .then((response: IProtocolResponse<boolean>): boolean => response.result);
    }

    updateTopicTranslations(topicId: string, translations: IEntityTranslation[]): ng.IPromise<boolean> {
        const successMessage: IProtocolMessage = { custom: this.translatePipe.transform("TopicTranslationsUpdated") };
        const errorMessage: IProtocolMessage = { custom: this.translatePipe.transform("TopicTranslationsUpdateError") };
        const payload: any = { topicId, Languages: translations };
        const config: IProtocolConfig = this.OneProtocol.customConfig("POST", `/api/consentmanager/v1/topics/languages`, null, payload);
        return this.OneProtocol.http(config, {Error: errorMessage, Success: successMessage})
            .then((response: IProtocolResponse<void>): boolean => response.result);
    }
}
