// Third Party
import { Injectable, Inject } from "@angular/core";

// Interfaces
import {
    IProtocolResponse,
    IProtocolMessage,
} from "interfaces/protocol.interface";
import { IDropDownElement } from "consentModule/shared/services/consent-base.service";

// Models
import { EntityItem } from "crmodel/entity-item";
import { PreferenceCenterFilters } from "crmodel/preference-center-filters";
import {
    PreferenceCenter,
    NewPreferenceCenter,
    PreferenceCenterList,
    UpdatePreferenceCenter,
} from "crmodel/preference-center-list";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

// Services
import {
    ConsentBaseService,
    IPreferenceResponse,
    IUpdatePreferenceChoicesRequest,
    IPurposeItem,
} from "./consent-base.service";
import { ProtocolService } from "modules/core/services/protocol.service";
import { Principal } from "sharedModules/services/helper/principal.service";

// Enums
import { Endpoints } from "consentModule/enums/endpoints.enum";

@Injectable()
export class ConsentPreferenceService extends ConsentBaseService {
    constructor(
        protected OneProtocol: ProtocolService,
        protected translatePipe: TranslatePipe,
        protected principal: Principal,
    ) {
        super(OneProtocol, translatePipe, principal);
    }

    getPreferenceCenters(filters: PreferenceCenterFilters, page: number = 0, size: number = 20, sort: string = "Name,asc"): ng.IPromise<PreferenceCenterList> {
        const method = "GET";
        const url = `${Endpoints.PreferenceCenters}/v2?page=${page}&size=${size}&sort=${sort}`;
        const config = this.OneProtocol.customConfig(method, url, filters);
        const mapper = (response: IProtocolResponse<PreferenceCenterList>) => response.result ? response.data : null;

        return this.customApiRequest(config, null, mapper);
    }

    getAllPreferenceCenters(sort: string = "Name,asc"): ng.IPromise<PreferenceCenter[]> {
        const method = "GET";
        const url = `${Endpoints.PreferenceCenters}/v2?pagedisabled=true&sort=${sort}`;
        const config = this.OneProtocol.customConfig(method, url);
        const mapper = (response: IProtocolResponse<PreferenceCenterList>) => response.result ? response.data : null;

        return this.customApiRequest(config, null, mapper);
    }

    getPreferenceCenter(id: string): ng.IPromise<PreferenceCenter> {
        const method = "GET";
        const url = `${Endpoints.PreferenceCenters}/${id}`;
        const config = this.OneProtocol.customConfig(method, url);
        const mapper = (response: IProtocolResponse<PreferenceCenterList>) => response.result ? response.data : null;

        return this.customApiRequest(config, null, mapper);
    }

    createPreferenceCenter(newPreferenceCenter?: NewPreferenceCenter): ng.IPromise<PreferenceCenter> {
        const successMessage = this.translatePipe.transform("PrefCenterCreated");
        const errorMessage = this.translatePipe.transform("PrefCenterCreationError");
        const messages = this.buildMessages(successMessage, errorMessage);

        const method = "POST";
        const url = `${Endpoints.PreferenceCenters}/v2`;
        const config = this.OneProtocol.customConfig(method, url, null, newPreferenceCenter);
        const mapper = (response: IProtocolResponse<PreferenceCenterList>) => response.result ? response.data : null;

        return this.customApiRequest(config, messages, mapper);
    }

    updatePreferenceCenter(preferenceCenter: PreferenceCenter): ng.IPromise<PreferenceCenter> {
        const model: UpdatePreferenceCenter = this.convertPreferenceCenterModel(preferenceCenter);
        const successMessage = this.translatePipe.transform("PrefCenterUpdated");
        const errorMessage = this.translatePipe.transform("PrefCenterUpdateError");
        const messages = this.buildMessages(successMessage, errorMessage);

        const method = "PUT";
        const url = `${Endpoints.PreferenceCenters}/${model.Id}`;
        const config = this.OneProtocol.customConfig(method, url, null, model);
        const mapper = (response: IProtocolResponse<PreferenceCenterList>) => response.result ? response.data : null;

        return this.customApiRequest(config, messages, mapper);
    }

    publishPreferenceCenter(preferenceCenter: PreferenceCenter): ng.IPromise<PreferenceCenter> {
        const model: UpdatePreferenceCenter = this.convertPreferenceCenterModel(preferenceCenter);
        const successMessage = this.translatePipe.transform("PrefCenterPublished");
        const errorMessage = this.translatePipe.transform("PrefCenterPublishError");
        const messages = this.buildMessages(successMessage, errorMessage);

        const method = "PUT";
        const url = `${Endpoints.PreferenceCenters}/${model.Id}/publish`;
        const config = this.OneProtocol.customConfig(method, url, null, model);
        const mapper = (response: IProtocolResponse<PreferenceCenterList>) => response.result ? response.data : null;

        return this.customApiRequest(config, messages, mapper);
    }

    deletePreferenceCenter(preferenceCenterId: string): ng.IPromise<boolean> {
        const successMessage = this.translatePipe.transform("PreferenceCenterDeleted");
        const errorMessage = this.translatePipe.transform("PreferenceCenterDeleteError");
        const messages = this.buildMessages(successMessage, errorMessage);

        const method = "DELETE";
        const url = `${Endpoints.PreferenceCenters}/${preferenceCenterId}`;
        const config = this.OneProtocol.customConfig(method, url);
        const mapper = (response: IProtocolResponse<boolean>) => response.result;

        return this.customApiRequest(config, messages, mapper);
    }

    handleFilters(): PreferenceCenterFilters {
        const filters: PreferenceCenterFilters = {
            name: sessionStorage.getItem("ConsentPreferenceCenterNameFilter"),
            purposeGuid: sessionStorage.getItem("ConsentPreferenceCenterPurposeGuidFilter"),
        };
        return filters;
    }

    getSessionStorageFilters(purposeList: IDropDownElement[]): { [key in keyof PreferenceCenterFilters]?: IDropDownElement } {
        const selections: { [key in keyof PreferenceCenterFilters]?: IDropDownElement } = {};

        if (purposeList && sessionStorage.getItem("ConsentPreferenceCenterPurposeGuidFilter")) {
            selections.purposeGuid = purposeList.find((option: IDropDownElement): boolean => {
                return option.value === sessionStorage.getItem("ConsentPreferenceCenterPurposeGuidFilter");
            });
        }
        return selections;
    }

    setSessionStorageFilters(filters: PreferenceCenterFilters) {
        if (filters.name) {
            sessionStorage.setItem("ConsentPreferenceCenterNameFilter", filters.name);
        } else {
            sessionStorage.removeItem("ConsentPreferenceCenterNameFilter");
        }

        if (filters.purposeGuid) {
            sessionStorage.setItem("ConsentPreferenceCenterPurposeGuidFilter", filters.purposeGuid);
        } else {
            sessionStorage.removeItem("ConsentPreferenceCenterPurposeGuidFilter");
        }
    }

    removeSessionStorageFilters() {
        sessionStorage.removeItem("ConsentPreferenceCenterNameFilter");
        sessionStorage.removeItem("ConsentPreferenceCenterPurposeGuidFilter");
    }

    private convertPreferenceCenterModel(preferenceCenter: PreferenceCenter): UpdatePreferenceCenter {
        const model: UpdatePreferenceCenter = Object.assign(
            {},
            preferenceCenter,
            { PurposeId: this.idsToString(preferenceCenter.Purposes) },
            { Purposes: undefined },
        );
        return model;
    }
}
