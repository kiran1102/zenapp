// Third Party
import { Injectable, Inject } from "@angular/core";

// Interfaces
import {
    IProtocolResponse,
    IProtocolMessage,
    IProtocolConfig,
} from "interfaces/protocol.interface";
import { IConsentSettings } from "interfaces/consent-settings.interface";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

// Services
import { ProtocolService } from "modules/core/services/protocol.service";
import { ConsentBaseService } from "./consent-base.service";
import { Principal } from "sharedModules/services/helper/principal.service";

@Injectable()
export class ConsentSettingsService extends ConsentBaseService {
    constructor(
        protected OneProtocol: ProtocolService,
        protected translatePipe: TranslatePipe,
        protected principal: Principal,
    ) {
        super(OneProtocol, translatePipe, principal);
    }

    setConsent(consent: IConsentSettings): ng.IPromise<boolean> {
        const successMessage: IProtocolMessage = { custom: this.translatePipe.transform("CustomSettingsSaved") };
        const errorMessage: IProtocolMessage = { custom: this.translatePipe.transform("SorryProblemSavingCustomSettings") };
        const config: IProtocolConfig = this.OneProtocol.customConfig("POST", `/api/consentmanager/v1/settings`, null, consent);
        return this.OneProtocol.http(config, { Error: errorMessage, Success: successMessage} )
            .then((response: IProtocolResponse<IConsentSettings>): boolean => response.result);
    }

    getConsent(): ng.IPromise<IConsentSettings> {
        const errorMessage: IProtocolMessage = { custom: this.translatePipe.transform("SorryProblemGettingCustomSettings") };
        const config: IProtocolConfig = this.OneProtocol.customConfig("GET", `/api/consentmanager/v1/settings`);
        return this.OneProtocol.http(config, { Error: errorMessage} )
            .then((response: IProtocolResponse<IConsentSettings>): IConsentSettings => response.result ? response.data : null);
    }

    recreateMagicLinks(): ng.IPromise<boolean> {
        const successMessage: IProtocolMessage = { custom: this.translatePipe.transform("MagicLinksRecreated") };
        const errorMessage: IProtocolMessage = { custom: this.translatePipe.transform("SorryProblemRecreatingMagicLinks") };
        const config: IProtocolConfig = this.OneProtocol.customConfig("POST", `/api/consentmanager/v1/linktokens/recreate`, null, null);
        return this.OneProtocol.http(config, { Error: errorMessage, Success: successMessage} )
            .then((response: IProtocolResponse<IConsentSettings>): boolean => response.result);
    }

    deleteMagicLinks(): ng.IPromise<boolean> {
        const successMessage: IProtocolMessage = { custom: this.translatePipe.transform("MagicLinksDeleted") };
        const errorMessage: IProtocolMessage = { custom: this.translatePipe.transform("SorryProblemDeletingMagicLinks") };
        const config: IProtocolConfig = this.OneProtocol.customConfig("DELETE", `/api/consentmanager/v1/linktokens`, null, null);
        return this.OneProtocol.http(config, { Error: errorMessage, Success: successMessage} )
            .then((response: IProtocolResponse<IConsentSettings>): boolean => response.result);
    }
}
