// Third Party
import { Injectable } from "@angular/core";

// Constants
import crConstants from "consentModule/shared/cr-constants";

// Interfaces
import {
    IProtocolMessage,
    IProtocolResponse,
    IProtocolConfig,
} from "interfaces/protocol.interface";

import { IGetAllLanguageResponse } from "interfaces/dsar/dsar-edit-language.interface";
import { IVersionListItem } from "interfaces/consent/cr-versions.interface";

// Models
import { PurposeDetails } from "crmodel/purpose-details";
import { PurposeFilters } from "crmodel/purpose-filters";
import { PurposeCreate } from "crmodel/purpose-create";

import {
    PurposeList,
    PurposeLanguage,
    SavePurpose,
} from "crmodel/purpose-list";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

// Services
import { Principal } from "modules/shared/services/helper/principal.service";
import { ConsentBaseService } from "consentModule/shared/services/consent-base.service";
import { ProtocolService } from "modules/core/services/protocol.service";

// Enums
import { Endpoints } from "consentModule/enums/endpoints.enum";

// Rx
import { from, Observable } from "rxjs";

@Injectable()
export class ConsentPurposeService extends ConsentBaseService {
    constructor(
        protected OneProtocol: ProtocolService,
        protected translatePipe: TranslatePipe,
        protected principal: Principal,
    ) {
        super(OneProtocol, translatePipe, principal);
    }

    public getPurposes(filters: PurposeFilters, page: number = 0, size: number = 10, sort: string = "Id,desc", type: string = crConstants.CRPurposeTypes.Standard): ng.IPromise<PurposeList> {
        const method = "GET";
        const url = `${Endpoints.Purposes}/list?page=${page}&size=${size}&sort=${sort}&type=${type}`;
        const config = this.OneProtocol.customConfig(method, url, filters);

        return this.customApiRequest(config);
    }

    public getPurposeDetails(id: string, version: number = 1): ng.IPromise<PurposeDetails> {
        const method = "GET";
        const url = `${Endpoints.Purposes}/${id}?version=${version}`;
        const config = this.OneProtocol.customConfig(method, url);

        return this.customApiRequest(config);
    }

    public getActivePurposeFilters(cookiePurpose: boolean = false): ng.IPromise<PurposeDetails[]> {
        const method = "GET";
        const typeQueryParam = cookiePurpose ? "" : "&type=STANDARD";
        const url = `${Endpoints.Purposes}/filters?status=ACTIVE${typeQueryParam}`;
        const config = this.OneProtocol.customConfig(method, url);

        return this.customApiRequest(config);
    }

    public getSelectedPurposeVersions(purposeId: string): ng.IPromise<IVersionListItem[]> {
        const method = "GET";
        const url = `${Endpoints.Purposes}/filters?id=${purposeId}`;
        const config = this.OneProtocol.customConfig(method, url);

        return this.customApiRequest(config);
    }

    public getFilteredPurposeVersions(purposeId: string): ng.IPromise<IVersionListItem[]> {
        const method = "GET";
        const url = `${Endpoints.Purposes}/filters?id=${purposeId}&status=ACTIVE&status=RETIRED`;
        const config = this.OneProtocol.customConfig(method, url);

        return this.customApiRequest(config);
    }

    public addPurposeTopics(purposeId: string, version: number = 1, topicIds: string[]): ng.IPromise<boolean> {
        const successMessage = this.translatePipe.transform("PurposeTopicsUpdated");
        const errorMessage = this.translatePipe.transform("PurposeTopicAddError");
        const messages = this.buildMessages(successMessage, errorMessage);

        const method = "PUT";
        const url = `${Endpoints.Purposes}/${purposeId}/topics?version=${version}`;
        const payload = { Ids: topicIds };
        const config = this.OneProtocol.customConfig(method, url, null, payload);
        const mapper = (response: IProtocolResponse<void>) => response.result;

        return this.customApiRequest(config, messages, mapper);
    }

    public createPurpose(newPurpose: PurposeCreate): ng.IPromise<string> {
        const successMessage = this.translatePipe.transform("PurposeCreated");
        const errorMessage = this.translatePipe.transform("PurposeNameUniqueError");
        const messages = this.buildMessages(successMessage, errorMessage);

        const method = "POST";
        const url = Endpoints.Purposes;
        const config = this.OneProtocol.customConfig(method, url, null, newPurpose);
        const mapper = (response: IProtocolResponse<PurposeDetails>) => response.result ? response.data.Id : null;

        return this.customApiRequest(config, messages, mapper);
    }

    public deletePurpose(purposeId: string, version: number = 1): ng.IPromise<boolean> {
        const successMessage = this.translatePipe.transform("PurposeDeleted");
        const errorMessage = this.translatePipe.transform("PurposeDeleteError");
        const messages = this.buildMessages(successMessage, errorMessage);

        const method = "DELETE";
        const url = `${Endpoints.Purposes}/${purposeId}?version=${version}`;
        const config = this.OneProtocol.customConfig(method, url);
        const mapper = (response: IProtocolResponse<void>) => response.result;

        return this.customApiRequest(config, messages, mapper);
    }

    public removeTopic(purposeId: string, version: number = 1, topicId: string): ng.IPromise<boolean> {
        const successMessage = this.translatePipe.transform("TopicRemoved");
        const errorMessage = this.translatePipe.transform("RemoveTopicError");

        const messages = this.buildMessages(successMessage, errorMessage);
        const method = "DELETE";
        const url = `${Endpoints.Purposes}/${purposeId}/topics/${topicId}?version=${version}`;
        const config = this.OneProtocol.customConfig(method, url);
        const mapper = (response: IProtocolResponse<void>) => response.result;

        return this.customApiRequest(config, messages, mapper);
    }

    public updateTopic(purposeId: string, version: number = 1, topicId: string, integrationKey: string): ng.IPromise<boolean> {
        const successMessage = this.translatePipe.transform("TopicUpdated");
        const errorMessage = this.translatePipe.transform("TopicUpdateError");
        const messages = this.buildMessages(successMessage, errorMessage);

        const payload: { IntegrationKey: string } = { IntegrationKey: integrationKey };
        const method = "PUT";
        const url = `${Endpoints.Purposes}/${purposeId}/topics/${topicId}?version=${version}`;
        const config = this.OneProtocol.customConfig(method, url, null, payload);
        const mapper = (response: IProtocolResponse<void>) => response.result;

        return this.customApiRequest(config, messages, mapper);
    }

    public getAllLanguages(): ng.IPromise<IGetAllLanguageResponse[]> {
        const method = "GET";
        const url = `/api/globalization/v1/languages/`;
        const params = { includeUntranslated: true };
        const config = this.OneProtocol.customConfig(method, url, params);
        const messages = { Error: { custom: this.translatePipe.transform("AllLanguages") } };

        return this.OneProtocol
            .http(config, messages)
            .then(ConsentBaseService.defaultResponseMapper);
    }

    public updatePurposeTranslations(purpose: PurposeDetails): ng.IPromise<boolean> {
        const successMessage = this.translatePipe.transform("PurposeTranslationsUpdated");
        const errorMessage = this.translatePipe.transform("PurposeTranslationsUpdateError");
        const messages = this.buildMessages(successMessage, errorMessage);
        const method = "POST";
        const url = `${Endpoints.Purposes}/languages`;

        const payload = {
            purposeId: purpose.Id,
            version: purpose.Version || 1,
            Languages: purpose.Languages,
        };

        const config = this.OneProtocol.customConfig(method, url, null, payload);
        const mapper = (response: IProtocolResponse<void>) => response.result;

        return this.customApiRequest(config, messages, mapper);
    }

    public publishPurpose(purpose: PurposeDetails): ng.IPromise<boolean> {
        const successMessage = this.translatePipe.transform("PurposePublished");
        const errorMessage = this.translatePipe.transform("PurposePublishError");
        const messages = this.buildMessages(successMessage, errorMessage);

        const method = "PUT";
        const url = `${Endpoints.Purposes}/${purpose.Id}/publish`;
        const payload = this.getSavePurposePayload(purpose);
        const config = this.OneProtocol.customConfig(method, url, null, payload);
        const mapper = (response: IProtocolResponse<void>) => response.result;

        return this.customApiRequest(config, messages, mapper);
    }

    public savePurpose(purpose: PurposeDetails): ng.IPromise<boolean> {
        const successMessage = this.translatePipe.transform("PurposeSaved");
        const errorMessage = this.translatePipe.transform("PurposeSaveError");
        const messages = this.buildMessages(successMessage, errorMessage);

        const method = "PUT";
        const url = `${Endpoints.Purposes}/${purpose.Id}`;
        const payload = this.getSavePurposePayload(purpose);
        const config = this.OneProtocol.customConfig(method, url, null, payload);
        const mapper = (response: IProtocolResponse<void>) => response.result;

        return this.customApiRequest(config, messages, mapper);
    }

    public createNewVersion(purposeId: string): ng.IPromise<PurposeDetails> {
        const successMessage = this.translatePipe.transform("NewPurposeVersionCreated");
        const errorMessage = this.translatePipe.transform("NewPurposeVersionCreationError");
        const messages = this.buildMessages(successMessage, errorMessage);

        const method = "POST";
        const url = `${Endpoints.Purposes}/version/${purposeId}`;
        const config = this.OneProtocol.customConfig(method, url);
        const mapper = (response: IProtocolResponse<void>) => response.data;

        return this.customApiRequest(config, messages, mapper);
    }

    public addCustomPreferences(purposeId: string, version = 1, customPreferencesIds: string[]): Observable<boolean> {
        const successMessage = this.translatePipe.transform("PurposeCustomPreferencesAdded");
        const errorMessage = this.translatePipe.transform("PurposeCustomPreferencesError");
        const messages = this.buildMessages(successMessage, errorMessage);
        const url = `${Endpoints.Purposes}/${purposeId}/customPreferences?version=${version}`;
        const method = "PUT";

        const config = this.OneProtocol.customConfig(method, url, null, customPreferencesIds);
        const mapper = (response: IProtocolResponse<void>) => response.result;
        const request = this.customApiRequest(config, messages, mapper);

        return from(request);
    }

    public removeCustomPreference(purposeId: string, version = 1, customPreferenceId: string): Observable<boolean> {
        const successMessage = this.translatePipe.transform("PurposeCustomPreferencesRemoved");
        const errorMessage = this.translatePipe.transform("PurposeCustomPreferencesRemovedError");
        const messages = this.buildMessages(successMessage, errorMessage);

        const url = `${Endpoints.Purposes}/${purposeId}/customPreferences/${customPreferenceId}?version=${version}`;
        const method = "DELETE";
        const config = this.OneProtocol.customConfig(method, url);

        const mapper = (response: IProtocolResponse<void>) => response.result;
        const request = this.customApiRequest(config, messages, mapper);

        return from(request);
    }

    private getSavePurposePayload(purpose: PurposeDetails): SavePurpose {
        const defaultLanguage = purpose.Languages.find((language: PurposeLanguage) => {
            return language.Default;
        }).Language;

        return {
            Name: purpose.Label,
            Description: purpose.Description,
            DefaultLanguage: defaultLanguage,
            ConsentLifeSpan: purpose.LifeSpan,
            Status: purpose.Status,
            Version: purpose.Version,
        };
    }
}
