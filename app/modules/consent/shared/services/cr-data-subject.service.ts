// Third Party
import {
    Injectable,
    Inject,
} from "@angular/core";
import { orderBy } from "lodash";

// Interfaces
import { IProtocolPacket } from "interfaces/protocol.interface";
import { IVersionListItem } from "interfaces/consent/cr-versions.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";

// Models
import { DataSubjectList } from "crmodel/data-subject-list";
import { DataSubjectDetails } from "crmodel/data-subject-details";
import { PurposeDetails } from "crmodel/purpose-details";
import { TopicDetails } from "crmodel/topic-details";
import {
    DataSubjectFilters,
    DataSubjectFiltersSelectionsOptions,
} from "crmodel/data-subject-filters";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

// Services
import { ProtocolService } from "modules/core/services/protocol.service";
import {
    ConsentBaseService,
    IDropDownElement,
} from "./consent-base.service";
import { ConsentPurposeService } from "consentModule/shared/services/cr-purpose.service";
import { Principal } from "sharedModules/services/helper/principal.service";

// Enums
import { Endpoints } from "consentModule/enums/endpoints.enum";

@Injectable()
export class ConsentDataSubjectService extends ConsentBaseService {
    constructor(
        protected OneProtocol: ProtocolService,
        protected translatePipe: TranslatePipe,
        protected principal: Principal,
        private consentPurposeService: ConsentPurposeService,
    ) {
        super(OneProtocol, translatePipe, principal);
    }

    getDataSubjects(filters: DataSubjectFilters, page: number = 0, size: number = 20): ng.IPromise<DataSubjectList> {
        const method = "POST";
        const url = `${Endpoints.DataSubjects}?page=${page}&size=${size}`;
        const config = this.OneProtocol.customConfig(method, url, null, filters, { dataSubjectIdentifier : filters.identifier });
        const mapper = (response: IProtocolResponse<DataSubjectList>) => response.result ? response.data : null;

        return this.customApiRequest(config, null, mapper);
    }

    getDataSubjectDetails(id: string): ng.IPromise<DataSubjectDetails> {
        const method = "GET";
        const url = `${Endpoints.DataSubjects}/${id}`;
        const config = this.OneProtocol.customConfig(method, url);
        const mapper = (response: IProtocolResponse<DataSubjectDetails>) => response.result ? response.data : null;

        return this.customApiRequest(config, null, mapper);
    }

    handleFilters(purposeId: string): DataSubjectFilters {
        const filters: DataSubjectFilters = {
            purposeGuid:    purposeId || sessionStorage.getItem("ConsentDataSubjectPurposeGuidFilter"),
            identifier:     sessionStorage.getItem("ConsentDataSubjectIdentifierFilter"),
            purposeVersion: Number(sessionStorage.getItem("ConsentDataSubjectPurposeVersionFilter")) || undefined,
            topics:         sessionStorage.getItem("ConsentDataSubjectTopicsFilter"),
            dataElements:   JSON.parse(sessionStorage.getItem("ConsentDataSubjectDataElementsFilter")),
        };

        if (purposeId) {
            this.clearSessionStorageFilters();
            sessionStorage.setItem("ConsentDataSubjectPurposeGuidFilter", filters.purposeGuid);
        }

        return filters;
    }

    getSessionStorageFilters(purposeList: IDropDownElement[], options: { [key in keyof DataSubjectFilters]?: IDropDownElement[] }): DataSubjectFiltersSelectionsOptions {
        const selections: { [key in keyof DataSubjectFilters]?: IDropDownElement } = {};

        if (!purposeList || !sessionStorage.getItem("ConsentDataSubjectPurposeGuidFilter")) {
            return;
        }
        selections.purposeGuid = purposeList.find((option: IDropDownElement): boolean => {
            return option.value === sessionStorage.getItem("ConsentDataSubjectPurposeGuidFilter");
        });
        if (!selections.purposeGuid) {
            return;
        }
        this.consentPurposeService.getFilteredPurposeVersions(selections.purposeGuid.value).then((result: IVersionListItem[]) => {
            if (!result || result.length === 0) {
                return;
            }
            options.purposeVersion = orderBy(result, "Version", "desc").map((item: IVersionListItem): IDropDownElement => {
                return {
                    label: `${item.Label} (${this.consentPurposeService.getVersionLabel(item.Version)})`,
                    value: item.Id,
                    version: item.Version,
                };
            });
            if (!sessionStorage.getItem("ConsentDataSubjectPurposeVersionFilter")) {
                return;
            }
            selections.purposeVersion = options.purposeVersion.find((option: IDropDownElement): boolean => {
                return option.version === Number(sessionStorage.getItem("ConsentDataSubjectPurposeVersionFilter"));
            });
            this.consentPurposeService.getPurposeDetails(selections.purposeGuid.value, selections.purposeVersion.version).then((purpose: PurposeDetails) => {
                options.topics = purpose.Topics.map((topic: TopicDetails): IDropDownElement => {
                    return {
                        label: topic.Name,
                        value: topic.Id,
                    };
                });
                if (sessionStorage.getItem("ConsentDataSubjectTopicsFilter")) {
                    selections.topics = options.topics.find((option: IDropDownElement): boolean => {
                        return option.value === sessionStorage.getItem("ConsentDataSubjectTopicsFilter");
                    });
                }
            });
        });
        return { selections, options };
    }

    setSessionStorageFilters(filters: DataSubjectFilters) {
        if (filters.identifier) {
            sessionStorage.setItem("ConsentDataSubjectIdentifierFilter", filters.identifier);
        } else {
            sessionStorage.removeItem("ConsentDataSubjectIdentifierFilter");
        }

        if (filters.purposeGuid) {
            sessionStorage.setItem("ConsentDataSubjectPurposeGuidFilter", filters.purposeGuid);
        } else {
            sessionStorage.removeItem("ConsentDataSubjectPurposeGuidFilter");
        }

        if (filters.purposeVersion) {
            sessionStorage.setItem("ConsentDataSubjectPurposeVersionFilter", "" + filters.purposeVersion);
        } else {
            sessionStorage.removeItem("ConsentDataSubjectPurposeVersionFilter");
        }

        if (filters.topics) {
            sessionStorage.setItem("ConsentDataSubjectTopicsFilter", filters.topics);
        } else {
            sessionStorage.removeItem("ConsentDataSubjectTopicsFilter");
        }

        if (filters.dataElements) {
            sessionStorage.setItem("ConsentDataSubjectDataElementsFilter", JSON.stringify(filters.dataElements));
        } else {
            sessionStorage.removeItem("ConsentDataSubjectDataElementsFilter");
        }
    }

    clearSessionStorageFilters() {
        sessionStorage.removeItem("ConsentDataSubjectIdentifierFilter");
        sessionStorage.removeItem("ConsentDataSubjectPurposeGuidFilter");
        sessionStorage.removeItem("ConsentDataSubjectPurposeVersionFilter");
        sessionStorage.removeItem("ConsentDataSubjectTopicsFilter");
        sessionStorage.removeItem("ConsentDataSubjectDataElementsFilter");
    }
}
