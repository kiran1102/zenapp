// Third Party
import { Injectable, Inject } from "@angular/core";
import { orderBy } from "lodash";

// Interfaces
import {
    IProtocolPacket,
    IProtocolMessage,
    IProtocolResponse,
} from "interfaces/protocol.interface";
import { IVersionListItem } from "interfaces/consent/cr-versions.interface";

// Models
import { ProtocolService } from "modules/core/services/protocol.service";
import { TransactionList } from "crmodel/transaction-list";
import { TransactionFilters } from "crmodel/transaction-filters";
import { TransactionDetails } from "crmodel/transaction-details";
import { TransactionWithdrawalRequest } from "crmodel/transaction-withdrawal-request";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

// Services
import {
    ConsentBaseService,
    IDropDownElement,
} from "consentModule/shared/services/consent-base.service";
import { ConsentPurposeService } from "consentModule/shared/services/cr-purpose.service";
import { CollectionPointService } from "consentModule/shared/services/collection-point.service";
import { Principal } from "sharedModules/services/helper/principal.service";

// Enums
import { Endpoints } from "consentModule/enums/endpoints.enum";

@Injectable()
export class ConsentTransactionService extends ConsentBaseService {
    constructor(
        protected OneProtocol: ProtocolService,
        protected translatePipe: TranslatePipe,
        protected principal: Principal,
        private collectionPointService: CollectionPointService,
        private consentPurposeService: ConsentPurposeService,
    ) {
        super(OneProtocol, translatePipe, principal);
    }

    getTransactions(filters: TransactionFilters, page: number = 0, size: number = 10, sort: string = "Id,desc"): ng.IPromise<TransactionList> {
        const method = "GET";
        const url = `${Endpoints.Transactions}?page=${page}&size=${size}&sort=${sort}`;
        const headers = filters.identifier ? { dataSubjectIdentifier: filters.identifier } : {};
        const config = this.OneProtocol.customConfig(method, url, filters, null, headers);
        const mapper = (response: IProtocolResponse<TransactionList>) => response.result ? response.data : null;

        return this.customApiRequest(config, null, mapper);
    }

    getTransactionDetails(id: string): ng.IPromise<TransactionDetails> {
        const method = "GET";
        const url = `${Endpoints.Transactions}/${id}`;
        const config = this.OneProtocol.customConfig(method, url);
        const mapper = (response: IProtocolResponse<TransactionDetails>) => response.result ? response.data : null;

        return this.customApiRequest(config, null, mapper);
    }

    withdrawTransaction(id: string, request: TransactionWithdrawalRequest): ng.IPromise<boolean> {
        const object: string = this.translatePipe.transform("Consent").toLowerCase();
        const action: string = this.translatePipe.transform("Withdrawn").toLowerCase();
        const successMessage: IProtocolMessage = {object, action };
        const errorMessage: IProtocolMessage = { custom: this.translatePipe.transform("WithdrawConsentError") };
        const messages = { Success: successMessage, Error: errorMessage };

        const method = "PUT";
        const url = `${Endpoints.Transactions}/${id}/withdraw`;
        const config = this.OneProtocol.customConfig(method, url, null, request);
        const mapper = (response: IProtocolResponse<boolean>) => response.result;

        return this.customApiRequest(config, messages, mapper);
    }

    handleFilters(purposeId: string, collectionPointId: string, receiptId: string, identifier: string): TransactionFilters {
        const filters: TransactionFilters = {
            purposeGuid:            purposeId || sessionStorage.getItem("ConsentTransactionPurposeGuidFilter"),
            collectionPointGuid:    collectionPointId || sessionStorage.getItem("ConsentTransactionCollectionPointGuidFilter"),
            receiptGuid:            receiptId || sessionStorage.getItem("ConsentTransactionReceiptGuidFilter"),
            identifier:             identifier || sessionStorage.getItem("ConsentTransactionIdentifierFilter"),
            purposeVersion:         Number(sessionStorage.getItem("ConsentTransactionPurposeVersionFilter")) || undefined,
            collectionPointVersion: Number(sessionStorage.getItem("ConsentTransactionCollectionPointVersionFilter")) || undefined,
        };
        if (purposeId && identifier) {
            this.clearSessionStorageFilters();
            sessionStorage.setItem("ConsentTransactionPurposeGuidFilter", filters.purposeGuid);
            sessionStorage.setItem("ConsentTransactionIdentifierFilter", filters.identifier);
            return filters;
        }

        if (purposeId) {
            this.clearSessionStorageFilters();
            sessionStorage.setItem("ConsentTransactionPurposeGuidFilter", filters.purposeGuid);
            return filters;
        }

        if (collectionPointId) {
            this.clearSessionStorageFilters();
            sessionStorage.setItem("ConsentTransactionCollectionPointGuidFilter", filters.collectionPointGuid);
            return filters;
        }

        if (receiptId) {
            this.clearSessionStorageFilters();
            sessionStorage.setItem("ConsentTransactionReceiptGuidFilter", filters.receiptGuid);
            return filters;
        }

        if (identifier) {
            this.clearSessionStorageFilters();
            sessionStorage.setItem("ConsentTransactionIdentifierFilter", filters.identifier);
            return filters;
        }

        return filters;
    }

    getSessionStorageFilters(purposeList: IDropDownElement[], collectionPointList: IDropDownElement[], options: { [key in keyof TransactionFilters]?: IDropDownElement[] }) {
        const selections: { [key in keyof TransactionFilters]?: IDropDownElement } = {};

        if (purposeList && sessionStorage.getItem("ConsentTransactionPurposeGuidFilter")) {
            selections.purposeGuid = purposeList.find((option: IDropDownElement): boolean => {
                return option.value === sessionStorage.getItem("ConsentTransactionPurposeGuidFilter");
            });
            if (selections.purposeGuid) {
                this.consentPurposeService.getFilteredPurposeVersions(selections.purposeGuid.value).then((result: IVersionListItem[]) => {
                    if (result && result.length > 0) {
                        options.purposeVersion = orderBy(result, "Version", "desc").map((item: IVersionListItem): IDropDownElement => {
                            return {
                                label: `${item.Label} (${this.consentPurposeService.getVersionLabel(item.Version)})`,
                                value: item.Id,
                                version: item.Version,
                            };
                        });
                        if (sessionStorage.getItem("ConsentTransactionPurposeVersionFilter")) {
                            selections.purposeVersion = options.purposeVersion.find((option: IDropDownElement): boolean => {
                                return option.version === Number(sessionStorage.getItem("ConsentTransactionPurposeVersionFilter"));
                            });
                        }
                    }
                });
            }
        }

        if (collectionPointList && sessionStorage.getItem("ConsentTransactionCollectionPointGuidFilter")) {
            selections.collectionPointGuid = collectionPointList.find((option: IDropDownElement): boolean => {
                return option.value === sessionStorage.getItem("ConsentTransactionCollectionPointGuidFilter");
            });
            if (selections.collectionPointGuid) {
                this.collectionPointService.getCollectionPointFilters(selections.collectionPointGuid.value).then((result: IVersionListItem[]) => {
                    if (result && result.length > 0) {
                        options.collectionPointVersion = orderBy(result, "Version", "desc").map((item: IVersionListItem): IDropDownElement => {
                            return {
                                label: `${item.Label} (${this.collectionPointService.getVersionLabel(item.Version)})`,
                                value: item.Id,
                                version: item.Version,
                            };
                        });
                        if (sessionStorage.getItem("ConsentTransactionCollectionPointVersionFilter")) {
                            selections.collectionPointVersion = options.collectionPointVersion.find((option: IDropDownElement): boolean => {
                                return option.version === Number(sessionStorage.getItem("ConsentTransactionCollectionPointVersionFilter"));
                            });
                        }
                    }
                });
            }
        }

        return { selections, options };
    }

    setSessionStorageFilters(filters: TransactionFilters) {
        if (filters.identifier) {
            sessionStorage.setItem("ConsentTransactionIdentifierFilter", filters.identifier);
        } else {
            sessionStorage.removeItem("ConsentTransactionIdentifierFilter");
        }

        if (filters.purposeGuid) {
            sessionStorage.setItem("ConsentTransactionPurposeGuidFilter", filters.purposeGuid);
        } else {
            sessionStorage.removeItem("ConsentTransactionPurposeGuidFilter");
        }

        if (filters.purposeVersion) {
            sessionStorage.setItem("ConsentTransactionPurposeVersionFilter", "" + filters.purposeVersion);
        } else {
            sessionStorage.removeItem("ConsentTransactionPurposeVersionFilter");
        }

        if (filters.collectionPointGuid) {
            sessionStorage.setItem("ConsentTransactionCollectionPointGuidFilter", filters.collectionPointGuid);
        } else {
            sessionStorage.removeItem("ConsentTransactionCollectionPointGuidFilter");
        }

        if (filters.collectionPointVersion) {
            sessionStorage.setItem("ConsentTransactionFCollectionPointVersionilter", "" + filters.collectionPointVersion);
        } else {
            sessionStorage.removeItem("ConsentTransactionFCollectionPointVersionilter");
        }

        if (filters.guid) {
            sessionStorage.setItem("ConsentTransactionGuidFilter", filters.guid);
        } else {
            sessionStorage.removeItem("ConsentTransactionGuidFilter");
        }

        if (filters.receiptGuid) {
            sessionStorage.setItem("ConsentTransactionReceiptGuidFilter", filters.receiptGuid);
        } else {
            sessionStorage.removeItem("ConsentTransactionReceiptGuidFilter");
        }

    }

    clearSessionStorageFilters() {
        sessionStorage.removeItem("ConsentTransactionIdentifierFilter");
        sessionStorage.removeItem("ConsentTransactionPurposeGuidFilter");
        sessionStorage.removeItem("ConsentTransactionPurposeVersionFilter");
        sessionStorage.removeItem("ConsentTransactionCollectionPointGuidFilter");
        sessionStorage.removeItem("ConsentTransactionCollectionPointVersionFilter");
        sessionStorage.removeItem("ConsentTransactionGuidFilter");
        sessionStorage.removeItem("ConsentTransactionReceiptGuidFilter");
    }
}
