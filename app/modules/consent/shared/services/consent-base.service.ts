// Third Party
import {
    find,
    each,
    clone,
    join,
    map,
} from "lodash";
import { Injectable, Inject } from "@angular/core";

// Interfaces
import {
    IProtocolMessages,
    IProtocolMessage,
    IProtocolPacket,
    IProtocolResponse,
    IProtocolConfig,
} from "interfaces/protocol.interface";
import { IStringMap } from "interfaces/generic.interface";
import { IHorseshoeGraphData } from "interfaces/one-horseshoe-graph.interface";
import { IGetAllLanguageResponse } from "interfaces/dsar/dsar-edit-language.interface";

// Models
import { TransactionFilters } from "crmodel/transaction-filters";
import { EntityItem } from "crmodel/entity-item";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

// Services
import { ProtocolService } from "modules/core/services/protocol.service";
import { Principal } from "sharedModules/services/helper/principal.service";

// Constants
import crConstants from "consentModule/shared/cr-constants";
import { EmptyGuid } from "constants/empty-guid.constant";

export interface IUpdatePreferenceChoicesRequest {
    purposes: IUpdatePurposeItem[];
}

export interface IUpdatePurposeItem {
    Id: string;
    Version: number;
    Preferences?: IUpdateTopic[];
    CustomPreferences?: IUpdateCustomPreference[];
}

export interface IUpdateTopic {
    TopicId: string;
}

export interface IUpdateCustomPreference {
    Id: string;
    Options: string[];
}

export interface ITopicPreference {
    TopicId: string;
    ChannelIds: string[];
    FrequencyIds: string[];
}

export interface IPreferenceResponse {
    PreferenceCentreId: string;
    DataSubjectId?: string;
    Language: string;
    Purposes: IPurposeItem[];
}

export interface IPurposeItem extends EntityItem {
    Frequencies: EntityItem[];
    Channels: EntityItem[];
    Topics: EntityItem[];
    Preferences: ITopicPreference[];
}

export interface IConsentLength {
    untilWithdrawn: boolean;
    unit?: string;
    number?: number;
}

export interface IDropDownElement {
    label: string;
    value: string;
    version?: number;
}

@Injectable()
export abstract class ConsentBaseService {

    protected readonly secondsPerDay: number = 60 * 60 * 24;
    protected readonly yearMultiplier: number = this.secondsPerDay * 365;
    protected readonly monthMultiplier: number = this.secondsPerDay * 30;
    protected readonly maxConsentLength: number = this.yearMultiplier * 9;
    private readonly protectedParamKeys: string[] = ["identifier", "responseType"];

    constructor(
        protected OneProtocol: ProtocolService,
        protected translatePipe: TranslatePipe,
        protected principal: Principal,
    ) { }

    calculateConsentLength(length: IConsentLength): number {
        if (length.untilWithdrawn || length.number < 0 || isNaN(length.number)) {
            return 0;
        }

        let multiplier: number;
        switch (length.unit) {
            case crConstants.CRLengthUnitTypes.Months:
                multiplier = this.monthMultiplier;
                break;
            case crConstants.CRLengthUnitTypes.Years:
                multiplier = this.yearMultiplier;
                break;
            default:
                multiplier = this.secondsPerDay;
        }

        const consentLength: number = length.number * multiplier;
        if (consentLength > this.maxConsentLength) {
            return 0;
        }
        return consentLength;
    }

    formatDuration(durationSeconds: number): string {
        if (!durationSeconds) {
            return this.translatePipe.transform("UntilWithdrawn");
        }

        let unitNumber: number;
        let translatedUnits: string;

        if (durationSeconds % this.yearMultiplier === 0) {
            unitNumber = Math.round(durationSeconds / this.yearMultiplier);
            translatedUnits = this.translatePipe.transform(unitNumber === 1 ? "Year" : "Years");
        } else if (durationSeconds % this.monthMultiplier === 0) {
            unitNumber = Math.round(durationSeconds / this.monthMultiplier);
            translatedUnits = this.translatePipe.transform(unitNumber === 1 ? "Month" : "Months");
        } else {
            unitNumber = Math.round(durationSeconds / this.secondsPerDay);
            translatedUnits = this.translatePipe.transform(unitNumber === 1 ? "Day" : "Days");
        }

        return `${unitNumber} ${translatedUnits}`;
    }

    parseConsentLength(durationSeconds: number): IConsentLength {
        if (!durationSeconds) {
            return { untilWithdrawn: true };
        }

        const consentLength: IConsentLength = { untilWithdrawn: false };

        if (durationSeconds % this.yearMultiplier === 0) {
            consentLength.number = durationSeconds / this.yearMultiplier;
            consentLength.unit = crConstants.CRLengthUnitTypes.Years;
        } else if (durationSeconds % this.monthMultiplier === 0) {
            consentLength.number = durationSeconds / this.monthMultiplier;
            consentLength.unit = crConstants.CRLengthUnitTypes.Months;
        } else {
            consentLength.number = durationSeconds / this.secondsPerDay;
            consentLength.unit = crConstants.CRLengthUnitTypes.Days;
        }
        return consentLength;
    }

    getDropDownOptions(constType: IStringMap<string>, showEmptyOption: boolean = false, emptyLabel: string = ""): IDropDownElement[] {
        const keys: string[] = Object.keys(constType);
        const options: IDropDownElement[] = [];
        if (showEmptyOption) {
            options.push({ label: emptyLabel, value: undefined });
        }

        each(keys, (key: string): void => {
            const option: IDropDownElement = {
                label: this.translatePipe.transform(key),
                value: constType[key],
            };
            options.push(option);
        });
        return options;
    }

    translateConstValue(value: string, constType: IStringMap<string>): string {
        if (!constType || !value) {
            return "";
        }
        const keys: string[] = Object.keys(constType);
        const constKey: string = find(keys, (key: string): boolean => constType[key] === value);
        return this.translatePipe.transform(constKey);
    }

    apiRequest(
        requestMethod: string,
        requestUrl: string,
        requestParams?: any,
        requestObject?: any,
        successMessage?: IProtocolMessage,
        errorMessage?: IProtocolMessage,
        timeout?: number,
        responseType?: string,
    ): ng.IPromise<IProtocolPacket> {
        const params: any = clone(requestParams);
        const headers: any = this.extractPrivateRequestParams(params);
        const config: any = this.OneProtocol.config(requestMethod, requestUrl, params, requestObject,
            headers, undefined, undefined, responseType, timeout);
        const messages: IProtocolMessages = { Error: errorMessage, Success: successMessage };
        return this.OneProtocol.http(config, messages);
    }

    idsToString(selectedPurposes: EntityItem[]): string {
        return join(map(selectedPurposes, "Id"), ",");
    }

    getVersionLabel(versionNumber: number): string {
        return `V${versionNumber}`;
    }

    downloadContent(filename: string, content: string) {
        const blob = new Blob([content]);
        if (window.navigator.msSaveOrOpenBlob) {
            window.navigator.msSaveBlob(blob, filename);
        } else {
            const link = window.document.createElement("a");
            link.href = window.URL.createObjectURL(blob);
            link.download = filename;
            document.body.appendChild(link);
            link.click();
            document.body.removeChild(link);
        }
    }

    emptyGraph(): IHorseshoeGraphData {
        const graph: IHorseshoeGraphData = {
            series: [
                {
                    graphClass: "empty",
                    label: "",
                    value: 1,
                },
            ],
            legend: [],
        };
        return graph;
    }

    shortenLargeNumber(totalCount: number): string {
        if (totalCount >= 1000000000) {
            return ((totalCount / 1000000000).toFixed(1) + "B");
        }
        if (totalCount >= 1000000) {
            // Prevent rounding to 1000.0 and displaying 1000.0M
            if ((totalCount / 1000000).toFixed(1) === "1000.0") {
                return "999.9M";
            }
            return ((totalCount / 1000000).toFixed(1) + "M");
        }
        if (totalCount >= 1000) {
            // Prevent rounding to 1000.0 and displaying 1000.0K
            if ((totalCount / 1000).toFixed(1) === "1000.0") {
                return "999.9K";
            }
            return ((totalCount / 1000).toFixed(1) + "K");
        }
        return totalCount + "";
    }

    getAllLanguages(): ng.IPromise<IGetAllLanguageResponse[]> {
        const tenantId: string = this.principal.getTenant() || EmptyGuid;
        const config: IProtocolConfig = this.OneProtocol.customConfig("GET", `/api/globalization/v1/languages/`, { includeUntranslated: true});
        const messages: IProtocolMessages = { Error: { custom: this.translatePipe.transform("AllLanguages")} };
        return this.OneProtocol.http(config, messages)
            .then((response: IProtocolResponse<IGetAllLanguageResponse[]>): IGetAllLanguageResponse[] =>
                response.result ? response.data : null);
    }

    protected filtersToQueryString(filters: TransactionFilters): string {
        const queryParameters: string[] = [];
        const keys: string[] = Object.keys(filters);

        each(keys, (key: string): void => {
            if (filters[key]) {
                const qsParam: string = key;
                const qsKeyValue = `${qsParam}=${this.encodeQsParameter(filters[key])}`;
                queryParameters.push(qsKeyValue);
            }
        });
        return queryParameters.length > 0 ? "&" + queryParameters.join("&") : "";
    }

    protected customApiRequest<T>(
        config: IProtocolConfig,
        messages?: { Success: IProtocolMessage; Error: IProtocolMessage },
        mapper = ConsentBaseService.defaultResponseMapper,
    ) {
        return this.OneProtocol
            .http(config, messages)
            .then(mapper);
    }

    protected encodeQsParameter(input: string): string {
        return encodeURIComponent(input).replace(".", "%2E").replace("-", "%2D");
    }

    protected static defaultResponseMapper<T>(response: IProtocolResponse<T>) {
        return response.result ? response.data : null;
    }

    protected buildMessages(success: string, error: string): { Error: IProtocolMessage, Success: IProtocolMessage } {
        const protocolMessage = (msg: string) => ({ custom: msg });
        return {
            Success: protocolMessage(success),
            Error: protocolMessage(error),
        };
    }

    private extractPrivateRequestParams(params?: any): { identifier: string } | null {
        if (!params) {
            return null;
        }
        const headers: any = {};
        each(this.protectedParamKeys, (key: string): void => {
            if (params[key] || params[key] === "") {
                headers[key] = params[key];
                params[key] = undefined;
            }
        });
        return headers;
    }
}
