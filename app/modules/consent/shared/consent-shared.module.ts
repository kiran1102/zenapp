// Angular
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { HttpClientModule } from "@angular/common/http";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";

// Components
import { CrWrapperComponent } from "consentModule/shared/cr-wrapper/cr-wrapper.component";
import { CrFiltersComponent } from "consentModule/shared/cr-filters/cr-filters.component";
import { CrTranslationTable } from "consentModule/shared/cr-translation-table/cr-translation-table.component";
import { CrVersionsComponent } from "consentModule/shared/cr-versions/cr-versions.component";
import { CrHeader } from "consentModule/shared/cr-header/cr-header.component";
import { CrPieGraphComponent } from "consentModule/shared/cr-pie-graph/cr-pie-graph.component";
import { CrTable } from "consentModule/shared/cr-table/cr-table.component";
import { CrDataSubjectPreferencesComponent } from "consentModule/shared/data-subject-preferences/data-subject-preferences.component";

// Modules
import { SharedModule } from "sharedModules/shared.module";
import { UpgradedLegacyModule } from "sharedModules/upgrade/upgraded-legacy.module";
import { DndListModule } from "ngx-drag-and-drop-lists";
import { VirtualScrollModule } from "od-virtualscroll";
import { OneVideoModule } from "modules/video/video.module";
import { VitreusModule } from "@onetrust/vitreus";
import { ConsentPipesModule } from "consentModule/pipes/pipes.module";
import { OTPipesModule } from "pipes/pipes.module";

// Services
import { ConsentPreferenceService } from "consentModule/shared/services/cr-preference.service";
import { CollectionPointService } from "consentModule/shared/services/collection-point.service";
import { ConsentPurposeService } from "consentModule/shared/services/cr-purpose.service";
import { TopicService } from "consentModule/shared/services/cr-topic.service";
import { ConsentDataSubjectService } from "consentModule/shared/services/cr-data-subject.service";
import { ConsentTransactionService } from "consentModule/shared/services/cr-transaction.service";
import { ConsentSettingsService } from "consentModule/shared/services/cr-settings.service";
import { ProtocolService } from "modules/core/services/protocol.service";
import { CrConsentedCustomPreferencesOptionsComponent } from "consentModule/shared/cr-consented-custom-preferences-options/cr-consented-custom-preferences-options.component";

export function getLegacyCrConstants(injector: any): any {
    return injector.get("crConstants");
}

@NgModule({
    imports: [
        CommonModule,
        SharedModule,
        UpgradedLegacyModule,
        HttpClientModule,
        FormsModule,
        ReactiveFormsModule,
        DndListModule,
        VirtualScrollModule,
        OneVideoModule,
        VitreusModule,
        ConsentPipesModule,
        OTPipesModule,
    ],
    exports: [ // TODO Move imports into specific sub-modules
        CommonModule,
        SharedModule,
        UpgradedLegacyModule,
        VitreusModule,
        FormsModule,
        ConsentPipesModule,
        OTPipesModule,
        CrWrapperComponent,
        CrFiltersComponent,
        CrTranslationTable,
        CrVersionsComponent,
        CrHeader,
        CrPieGraphComponent,
        CrTable,
        CrDataSubjectPreferencesComponent,
        CrConsentedCustomPreferencesOptionsComponent,
    ],
    declarations: [
        CrWrapperComponent,
        CrFiltersComponent,
        CrTranslationTable,
        CrVersionsComponent,
        CrHeader,
        CrPieGraphComponent,
        CrTable,
        CrDataSubjectPreferencesComponent,
        CrConsentedCustomPreferencesOptionsComponent,
    ],
    entryComponents: [
        CrWrapperComponent,
    ],
    providers: [
        ConsentPreferenceService,
        CollectionPointService,
        ConsentPurposeService,
        TopicService,
        ConsentDataSubjectService,
        ConsentTransactionService,
        ConsentSettingsService,
        ProtocolService,
        {
            provide: "crConstants",
            deps: ["$injector"],
            useFactory: getLegacyCrConstants,
        },
    ],
})
export class ConsentSharedModule { }
