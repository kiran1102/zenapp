export class ReceiptJwt {
    public jti?: string;
    public iat?: string;
    public moc?: string;
    public sub?: string;
    public iss?: string;
    public identifier?: string;
    public processId?: string;
    public tenantId?: string;
    public consentLength?: number;
    public policy_uri?: string;
    public description?: string;
    public consentType: string;
    public purposes?: ReceiptPurposeJwt[];
    public dsDataElements?: string[];
    public customPayload?: any;
}

export class ReceiptPurposeJwt {
    Id: string;
    Preferences?: ReceiptTopicJwt[];
}

export class ReceiptTopicJwt {
    TopicId: string;
}
