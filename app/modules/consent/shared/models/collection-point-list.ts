import { PagedList } from "./paged-list";
import { PurposeListItem } from "crmodel/purpose-list";
import { PreferenceCenter } from "crmodel/preference-center-list";

export class CollectionPointList extends PagedList<CollectionPointListItem> { }

export class CollectionPointListItem {
    Id: string;
    Name: string;
    Version: number;
    Status: string;
    ConsentType: string;
    CollectionPointType: string;
    DoubleOptIn: boolean;
    RedirectUrl?: string;
    SubjectIdentifier: string;
    CreateDate: Date;
    ReceiptCount: number;
    FirstReceiptOn: Date | null;
    ActivationDate: Date | null;
    LastSdkIntegrationDate?: Date | null;
    Purposes: PurposeListItem[];
    Token?: string;
    NoConsentTransactions?: boolean;
    CanCreateNewVersion: boolean;
    Language: string;
    Languages: string[];
    HostedSDK: boolean;
    SendConsentEmail: boolean;
    PreferenceCenter: PreferenceCenter;
    PreferenceCenterGuid: string;
    ResponsibleUserId: string;
}
