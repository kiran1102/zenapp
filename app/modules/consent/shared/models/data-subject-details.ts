import { ITopicPreference } from "consentModule/shared/services/consent-base.service";
import { TopicListItem } from "crmodel/topic-list";
import { EntityCustomPreferences, EntityCustomPreferencesOptionsConsented } from "crmodel/entity-custom-preferences";

export class DataSubjectTransactionSummary {
    FirstTransactionDate: Date;
    LastTransactionDate: Date;
    LastTransactionId: string;
    TotalTransactionCount: number;
}

export class DataSubjectPurposeItem {
    PurposeId: string;
    PurposeName: string;
    PurposeVersion: number;
    ConsentDate: Date | null;
    ConsentStatus: string;
    ExpiryDate: Date | null;
    CollectionPointId?: string;
    CollectionPointName?: string;
    Preferences: ITopicPreference[];
    TransactionSummary: DataSubjectTransactionSummary;
    DoubleOptIn: boolean;
    ActivationDate: Date | null;
    Topics: TopicListItem[];
    CustomPreferences: EntityCustomPreferences[];
    CustomPreferencesOptionsConsented: EntityCustomPreferencesOptionsConsented[];
}

export class DataSubjectElementItem {
    Name: string;
    Value: string;
}

export class DataSubjectDetails {
    Id: string;
    Identifier: string;
    DataSubjectType: string;
    LastLoginDate: Date | null;
    Purposes: DataSubjectPurposeItem[];
    TransactionSummary: DataSubjectTransactionSummary;
    Elements?: DataSubjectElementItem[];
    Language?: string;
}
