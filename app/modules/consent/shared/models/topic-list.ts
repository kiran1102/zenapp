import { PagedList } from "./paged-list";
import { IEntityTranslation } from "interfaces/consent/entity-translation";

export class TopicList extends PagedList<TopicListItem> { }

export class TopicListItem {
    Id: string;
    Name: string;
    CanDelete?: boolean;
    IntegrationKey?: string;
    Languages?: TopicLanguage[];
}

export class NewTopic {
    Name: string;
    DefaultLanguage: string;
}

export class TopicIdList {
    Ids: string[];
}

export class TopicLanguage implements IEntityTranslation {
    Language: string;
    Name: string;
    Default?: boolean;
}
