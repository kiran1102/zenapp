import { TransactionSummary } from "crmodel/dashboard-data";

export class CollectionPointReport {
    TransactionCount: number;
    ReceiptCount: number;
    Purposes: CollectionPointPurposeReport[];
    Transactions: TransactionSummary;
}

export class CollectionPointPurposeReport {
    Id: string;
    Version?: number;
    Label: string;
    TransactionCount: number;
    NoConsentTransactionCount: number;
}
