import { PagedList } from "./paged-list";

export class DataElementList extends PagedList<DataElementListItem> {}

export class DataElementListItem {
    name: string;
    value: string;
}
