import { IDropdownOption } from "interfaces/one-dropdown.interface";
export class ColumnDefinition {
    columnName?: string;
    cellValueKey?: string;
    cellClass?: string;
    columnType?: string;
    route?: string;
    routeIdField?: string;
    routeVersionField?: string;
    rowToText?: (row: any) => string;
    rowToLabel?: (row: any) => string;
    getOptions?: (row: any) => IDropdownOption[];
    sortKey?: string;
    required?: boolean;
}
