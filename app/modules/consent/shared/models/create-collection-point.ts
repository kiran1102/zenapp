export class CreateCollectionPoint {
    Name: string;
    Description: string;
    CollectionPointType: string;
    DoubleOptIn: boolean;
    RedirectUrl?: string;
    WebFormUrl: string;
    PrivacyPolicyUrl: string;
    SubjectIdentifier: string;
    ConsentType: string;
    OrganizationId: string;
    PurposeId: string;
    DataControllerName: string;
    RightToWithdraw: string;
    HowToWithdraw: string;
    OtherInformation: string;
    DataElements: string[];
    IabVendorId?: number;
    NoConsentTransactions?: boolean;
    Version: number;
    Language: string;
    Languages?: string[];
    ResponsibleUserId?: string;

    constructor(collectionPointType: string = "WEB_FORM") {
        this.CollectionPointType = collectionPointType;
    }
}

export class UpdateCollectionPoint extends CreateCollectionPoint {
    Status: string;
    LastSdkIntegrationDate?: Date | null;

    public static fromDetails(details: Partial<UpdateCollectionPoint>): UpdateCollectionPoint {
        const collectionPoint = new UpdateCollectionPoint();
        return { ...collectionPoint, ...details };
    }
}
