
export class SdkSettings {
    scriptSourceLocation: string;
    scriptSettingsLocation: string;
    mobileSDK: MobileSDK;
    integrationSettings: IntegrationSettings;
}

export class CollectionPointSettings {
    settingsConfiguration: SdkSettings;
}

export class MobileSDK {
    androidSDKUrl: string;
    tvOSSDKUrl: string;
    iossdkurl: string;
    preferenceCenterUrl: string;
    dataSubjectUrl: string;
    authorizationUrl: string;
}

export class IntegrationSettings {
    manualTrigger?: boolean;
    testMode?: boolean;
    continueOnError?: boolean;
    receiptApiEndpoint: string;
    preferences?: PurposeDefinition[];
}

export abstract class ElementDefinition {
    identifierAttributes?: string;
    identifierValues: string;
}

export class DataElementDefinition extends ElementDefinition {
    noticeHeadline?: string;
    noticeReason: string;
    noticePosition?: string;
}

export class PurposeDefinition extends ElementDefinition {
    purpose: string;
    topics?: TopicDefinition[];
    labels?: DefinitionLabel[];
    customPreferences?: CustomPreferenceDefinition[];
}

export class TopicDefinition extends ElementDefinition {
    topic: string;
    labels?: DefinitionLabel[];
}

export class OptionDefinition extends ElementDefinition {
    option: string;
}

export class CustomPreferenceDefinition {
    customPreference: string;
    options: OptionDefinition[];
}

export class DefinitionLabel {
    language: string;
    label: string;
    description?: string;
}
