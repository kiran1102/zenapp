import { CollectionPointListItem } from "./collection-point-list";
import { PurposeListItem } from "crmodel/purpose-list";

export class CollectionPointDetails extends CollectionPointListItem {
    DataControllerName: string;
    DataElements: string[];
    Description: string;
    HowToWithdraw: string;
    IabVendorId: number;
    OrganizationId: string;
    OtherInformation: string;
    PrivacyPolicyUrl: string;
    PurposeId: string;
    Purposes: PurposeListItem[];
    RightToWithdraw: string;
    WebFormUrl: string;
}

export class CollectionPointFormPayload {
    details: CollectionPointDetails;
    privacyPolicyValid: boolean;
}

export class CollectionPointSettingsPayload {
    redirectUrl: string;
    redirectUrlValid: boolean;
}
