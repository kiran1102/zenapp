export class BulkUpdateCollectionPoint {
    collectionPointsToUpdate: BulkUpdateCollectionPointItem[];
}

export class BulkUpdateCollectionPointItem {
    CollectionPointGuid: string;
    CollectionPointVersion: number;
}
