import { DataElementListItem } from "crmodel/data-element-list";
import { IDropDownElement } from "consentModule/shared/services/consent-base.service";

export class DataSubjectFilters {
    identifier?: string;
    purposeGuid?: string;
    purposeVersion?: number;
    topics?: string;
    dataElements?: DataElementListItem[];
    includeCounts?: boolean;
}

export class DataSubjectFiltersSelectionsOptions {
    selections: { [key in keyof DataSubjectFilters]?: IDropDownElement };
    options: { [key in keyof DataSubjectFilters]?: IDropDownElement[] };
}
