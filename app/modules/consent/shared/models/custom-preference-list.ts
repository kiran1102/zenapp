import { PagedList } from "crmodel/paged-list";
import { EntityItem } from "consentModule/shared/models/entity-item";

export class CustomPreferenceList extends PagedList<CustomPreferenceListItem> { }

export class CustomPreferenceBase {
    Name: string;
    Description: string;
    SelectionType: string;
    DisplayAs: string;
    Required: boolean;
}

export class CustomPreferenceOption extends EntityItem {
    CanDelete: boolean;
    Selected?: boolean;
}

export class CustomPreferenceListItem extends CustomPreferenceBase {
    Id: string;
    NumberOfOptions: number;
    NumberOfLanguages: number;
    CreatedDate: Date;
    UpdatedDate: Date;
    DefaultLanguage: string;
    Languages: CustomPreferenceLanguage[];
    Options: CustomPreferenceOption[];
}

export class PurposeCustomPreference extends CustomPreferenceListItem {
    PurposeCustomPreferenceId: string;
}

export class NewCustomPreference extends CustomPreferenceBase {
    Options: string[];
    Language: string;
}

export class CustomPreferenceLanguage {
    Name: string;
    Description: string;
    Language: string;
    Options: EntityItem[];
    Default: boolean;
}
