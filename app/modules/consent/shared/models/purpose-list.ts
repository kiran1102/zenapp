import { EntityItem } from "./entity-item";
import { PagedList } from "./paged-list";
import { TopicListItem } from "crmodel/topic-list";
import { IStringMap } from "interfaces/generic.interface";
import { IEntityTranslation } from "interfaces/consent/entity-translation";
import { DefaultConsentStatus } from "consentModule/enums/default-consent-status.enum";
import { PurposeCustomPreference } from "crmodel/custom-preference-list";

export class PurposeList extends PagedList<PurposeListItem> { }

export class PurposeListItem extends EntityItem {
    Description: string;
    Status: string;
    Type: string;
    DataSubjectType: string;

    LifeSpan: number;
    Version: number;
    CollectionPoints: number;
    PreferencesCenters: number;
    ActiveDataSubjects: number;
    ExpiredDataSubjects: number;
    NoConsentDataSubjects: number;
    PendingDataSubjects: number;
    WithdrawnDataSubjects: number;
    OptedOutDataSubjects: number;

    CanCreateNewVersion: boolean;
    NewVersionAvailable: boolean;

    Topics: TopicListItem[];
    Languages?: PurposeLanguage[];
    CustomPreferences: PurposeCustomPreference[];
    Preferences?: any[];
    DefaultConsentStatus: DefaultConsentStatus;
    Index: number;

    Expanded?: boolean;
}

export class PurposeLanguage implements IEntityTranslation {
    Language: string;
    Name: string;
    Description: string;
    Default?: boolean;
}

export class PreviewPurpose {
    IsChecked: boolean;
    Topics: IStringMap<boolean>;
    CustomPreferences: {
        [key: string]: {
            Options: IStringMap<boolean>,
        };
    };
    Version: number;
}

export class SavePurpose {
    Name: string;
    Description: string;
    DefaultLanguage: string;
    ConsentLifeSpan: number;
    Status: string;
    Version: number;
}
