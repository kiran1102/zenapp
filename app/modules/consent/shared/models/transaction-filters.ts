import { IDropDownElement } from "consentModule/shared/services/consent-base.service";

export class TransactionFilters {
    collectionPointGuid?: string;
    collectionPointType?: string;
    collectionPointVersion?: number;
    consentType?: string;
    dataElement?: string;
    dataElementValue?: string;
    dateSearchType?: string;
    fromDate?: Date;
    identifier?: string;
    privacyPolicyUrl?: string;
    purposeGuid?: string;
    purposeVersion?: number;
    receiptGuid?: string;
    status?: string;
    toDate?: Date;
    topics?: string;
    guid?: string;
    includeCounts?: boolean;
}

export class TransactionFiltersSelectionsOptions {
    selections: { [key in keyof TransactionFilters]?: IDropDownElement };
    options: { [key in keyof TransactionFilters]?: IDropDownElement[] };
}
