export class EntityCustomPreferences {
    Id: string;
    Name: string;
    Options: Array<{
        Id: string;
        Label: string;
    }>;
}

export class EntityCustomPreferencesOptionsConsented {
    Id: string;
    Options: string[];
}
