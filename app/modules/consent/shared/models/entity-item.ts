export class EntityItem {
    Id: string;
    Label: string;
    Version?: number;
    Index?: number;
}
