import { TransactionListItem } from "./transaction-list";
import { PurposeDetails } from "./purpose-details";
import { EntityCustomPreferencesOptionsConsented } from "crmodel/entity-custom-preferences";

export class TransactionDetails extends TransactionListItem {
    ReceiptJwt: string;
    WithdrawnBy: string;
    Notes: string;
    Purpose?: PurposeDetails;
    CustomPayload?: any;
    DoubleOptIn: boolean;
    CustomPreferences: EntityCustomPreferencesOptionsConsented[];
}
