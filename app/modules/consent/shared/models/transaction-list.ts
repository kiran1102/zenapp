import { IUpdatePurposeItem } from "consentModule/shared/services/consent-base.service";
import { PagedList } from "./paged-list";
export class TransactionList extends PagedList<TransactionListItem> { }

export class TransactionListItem {
    Id: string;
    CollectionPointId: string;
    CollectionPointName: string;
    CollectionPointType: string;
    CollectionPointVersion: number;
    DataSubjectId: string;
    Identifier: string;
    IssueDate: Date;
    ReceiptStatus: string;
    ReceiptId: string;
    WithdrawalDate: Date | null;
    ExpiryDate: Date | null;
    Test?: boolean;
    PurposeId: string;
    PurposeName: string;
    PurposeVersion: number;
    Preferences: IUpdatePurposeItem[];
    Origin: string;
    ImportDate?: Date | null;
    ImporterId?: string;
}
