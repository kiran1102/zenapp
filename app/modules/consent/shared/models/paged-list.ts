import { IPageable } from "interfaces/pagination.interface";

export class PagedList<T>  implements IPageable {
    content: T[];
    totalPages: number;
    totalElements: number;
    size: number;
    number: number;
    first: boolean;
    last: boolean;
    sort: string|null;
    numberOfElements: number;
}
