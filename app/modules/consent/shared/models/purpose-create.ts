export class PurposeCreate {
    Name: string;
    Description: string;
    ConsentLifeSpan: number|null;
    DefaultLanguage: string;
}
