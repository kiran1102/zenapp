import { PagedList } from "./paged-list";
import { DataSubjectTransactionSummary } from "./data-subject-details";
export class DataSubjectList extends PagedList<DataSubjectListItem> { }

export class DataSubjectListItem {
    Id: string;
    Identifier: string;
    TotalPurposes: number;
    ActivePurposes: number;
    TransactionSummary: DataSubjectTransactionSummary;
}
