import { PagedList } from "./paged-list";
import { PurposeListItem } from "./purpose-list";

export class PreferenceCenterList extends PagedList<PreferenceCenter> {}

export class PreferenceCenterBase {
    Name: string;
    OrganizationId: string;
    Language?: string;
    Languages?: string[];
    Description: string;
    SendPreferenceChangeEmails?: boolean;
    SendUnsubscribeEmails?: boolean;
}

export class PreferenceCenter extends PreferenceCenterBase {
    Id: string;
    Url: string;
    LinkTokenUrl: string;
    Status: string;
    Purposes?: PurposeListItem[];
    OrganizationName: string;
    PreferenceCenterSettings?: PreferenceCenterSettings;
    ContainsUnpublishedData: boolean;
    BlobUrl?: string;
    PublishedDate?: string;
    SendUnsubscribeEmails: boolean;
    DisplayNotificationPurpose: boolean;
}

export class NewPreferenceCenter extends PreferenceCenterBase {
    PurposeId: string;
}

export class UpdatePreferenceCenter extends PreferenceCenterBase {
    Id: string;
    PurposeId: string;
}

export class PreferenceCenterLabels {
    WelcomeMessage?: string;
    Title?: string;
    UnsubscribeAllLabel?: string;
    ChangePreferencesLabel?: string;
    UpdateSuccessLabel?: string;
    UpdateErrorLabel?: string;
    UnsubscribeSuccessLabel?: string;
}

export class PreferenceCenterOrderBySetting {
    Purposes: {
        [id: string]: {
            Label: string;
            Id: string;
            Index: number;

            Topics?: {
                [id: string]: {
                    Name: string;
                    Id: string;
                    Index: number;
                };
            }
        };
    };
}

export class PreferenceCenterSettings extends PreferenceCenterLabels {
    Languages?: { [language: string]: PreferenceCenterLabels };
    CustomCssUrl?: string;
    OrderBy: PreferenceCenterOrderBySetting;
}
