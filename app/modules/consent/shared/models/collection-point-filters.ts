import { IDropDownElement } from "consentModule/shared/services/consent-base.service";

export class CollectionPointFilters {
    name?: string;
    collectionPointType?: string;
    consentType?: string;
    status?: string;
    purposeGuid?: string;
    purposeVersion?: number;
}

export class CollectionPointFiltersSelectionsOptions {
    selections: { [key in keyof CollectionPointFilters]?: IDropDownElement };
    options: { [key in keyof CollectionPointFilters]?: IDropDownElement[] };
}
