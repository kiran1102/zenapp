import { IHorseshoeGraphData } from "interfaces/one-horseshoe-graph.interface";

export interface IDashboardData {
    CollectionPoints: IHorseshoeGraphData;
    Purposes: IHorseshoeGraphData;
    Statuses: IHorseshoeGraphData;
}

export class DashboardCollectionPointSummary {
    Count: number;
    CollectionPointType: string;
}

export class DashboardPurposeSummary {
    Count: number;
    PurposeName: string;
}

export class TransactionSummary {
    PendingTransactionCount: number;
    ActiveTransactionCount: number;
    WithdrawTransactionCount: number;
    ExpiredTransactionCount: number;
    NoConsentTransactionCount: number;
    OptedOutTransactionCount: number;
}
