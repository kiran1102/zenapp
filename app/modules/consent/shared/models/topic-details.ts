import { TopicListItem, TopicLanguage } from "./topic-list";
import { IEntityTranslation } from "interfaces/consent/entity-translation";

export class TopicDetails extends TopicListItem {
    Languages?: TopicLanguage[];
}
