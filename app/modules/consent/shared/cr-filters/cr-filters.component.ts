// Third Party
import {
    Component,
    Output,
    EventEmitter,
    Input,
} from "@angular/core";

@Component({
    selector: "cr-filters",
    templateUrl: "./cr-filters.component.html",
})
export class CrFiltersComponent {
    @Input() isDrawerOpen: boolean;
    @Input() textSearch: string;
    @Input() hideTextSearch: boolean;
    @Output() onApply = new EventEmitter();
    @Output() onClear = new EventEmitter();
    @Output() onRefresh = new EventEmitter();
    @Output() onTextSearch = new EventEmitter<string>();
    @Output() onToggle = new EventEmitter();
}
