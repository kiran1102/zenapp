// Angular
import {
    Component,
    OnInit,
    OnDestroy,
} from "@angular/core";

// Services
import { Permissions } from "modules/shared/services/helper/permissions.service";
import { GlobalSidebarService } from "modules/global-sidebar/services/one-global-sidebar.service";
import { StateService } from "@uirouter/core";
import { Wootric } from "sharedModules/services/provider/wootric.service";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

// Interfaces
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import { INavMenuItem } from "modules/global-sidebar/interfaces/nav-menu-item.interface";

// Constants
import { WootricModuleMap } from "modules/shared/constants/wootric.constants";

@Component({
    selector: "cr-wrapper",
    templateUrl: "./cr-wrapper.component.html",
})
export class CrWrapperComponent implements OnInit {
    constructor(
        private permissions: Permissions,
        private translatePipe: TranslatePipe,
        private globalSidebarService: GlobalSidebarService,
        private wootric: Wootric,
    ) {}

    ngOnInit() {
        this.globalSidebarService.set(this.getMenuRoutes(), this.translatePipe.transform("UniversalConsentPrefMgmt"));
        this.wootric.run(WootricModuleMap.UniversalConsentAndPreferenceManagement);
    }

    private getMenuRoutes(): INavMenuItem[] {
        const menuGroup: INavMenuItem[] = [];
        if (this.permissions.canShow("ConsentReceiptDashboard")) {
            menuGroup.push({
                id: "ConsentDashboardSidebarItem",
                title: this.translatePipe.transform("Dashboard"),
                route: "zen.app.pia.module.consentreceipt.views.dashboard",
                activeRoute: "zen.app.pia.module.consentreceipt.views.dashboard",
                icon: "ot ot-dashboard",
            });
        }
        if (this.permissions.canShow("ConsentReceiptCollectionPoints")) {
            menuGroup.push({
                id: "ConsentCollectionPointsSidebarItem",
                title: this.translatePipe.transform("CollectionPoints"),
                route: "zen.app.pia.module.consentreceipt.views.collections.list",
                activeRoute: "zen.app.pia.module.consentreceipt.views.collections",
                icon: "ot ot-list-alt",
            });
        }

        if (this.permissions.canShow("ConsentPreferenceCenters")) {
            menuGroup.push({
                id: "ConsentPreferenceCentersSidebarItem",
                title: this.translatePipe.transform("PreferenceCenters"),
                route:
                    "zen.app.pia.module.consentreceipt.views.preferencecenters.list",
                icon: "fa fa-sliders",
                activeRouteFn: (stateService: StateService) =>
                    stateService.includes("zen.app.pia.module.consentreceipt.views.preferencecenters.list")
                    || stateService.includes("zen.app.pia.module.consentreceipt.views.preferencecenters.details"),
            });
        }

        if (this.permissions.canShow("ConsentReceiptReports")) {
            const reportsGroup: INavMenuItem = {
                id: "ConsentReportingSidebarItem",
                title: this.translatePipe.transform("Reporting"),
                children: [],
                open: true,
                icon: "fa fa-area-chart",
            };

            if (this.permissions.canShow("ConsentReceiptReportsDataSubjects")) {
                reportsGroup.children.push({
                    id: "ConsentDataSubjectsSidebarItem",
                    title: this.translatePipe.transform("DataSubjects"),
                    route:
                        "zen.app.pia.module.consentreceipt.views.datasubjects.list",
                    activeRoute:
                        "zen.app.pia.module.consentreceipt.views.datasubjects",
                });
            }

            if (this.permissions.canShow("ConsentReceiptReportsTransactions")) {
                reportsGroup.children.push({
                    id: "ConsentTransactionsSidebarItem",
                    title: this.translatePipe.transform("Transactions"),
                    route:
                        "zen.app.pia.module.consentreceipt.views.transactions.list",
                    activeRoute:
                        "zen.app.pia.module.consentreceipt.views.transactions",
                });
            }

            if (reportsGroup.children.length) {
                menuGroup.push(reportsGroup);
            }
        }

        if (this.permissions.canShow("ConsentReceiptSetUp")) {
            const setupGroup: INavMenuItem = {
                id: "ConsentSetupSidebarItem",
                title: this.translatePipe.transform("Setup"),
                children: [],
                open: true,
                icon: "ot ot-wrench",
            };

            if (this.permissions.canShow("ConsentReceiptPurposes")) {
                setupGroup.children.push({
                    id: "ConsentPurposesSidebarItem",
                    title: this.translatePipe.transform("Purposes"),
                    route: "zen.app.pia.module.consentreceipt.views.purposes.list",
                    activeRoute: "zen.app.pia.module.consentreceipt.views.purposes",
                });
            }

            if (this.permissions.canShow("ConsentEditTopics")) {
                setupGroup.children.push({
                    id: "ConsentTopicsSidebarItem",
                    title: this.translatePipe.transform("Topics"),
                    route:
                        "zen.app.pia.module.consentreceipt.views.topics.list",
                    activeRoute:
                        "zen.app.pia.module.consentreceipt.views.topics",
                });
            }

            if (this.permissions.canShow("ConsentEditCustomPreferences")) {
                setupGroup.children.push({
                    id: "ConsentCustomPreferencesSidebarItem",
                    title: this.translatePipe.transform("CustomPreferences"),
                    route:
                        "zen.app.pia.module.consentreceipt.views.custompreferences.list",
                    activeRoute:
                        "zen.app.pia.module.consentreceipt.views.custompreferences",
                });
            }

            if (this.permissions.canShow("ConsentDataSubjectTypes")) {
                setupGroup.children.push({
                    id: "ConsentDataSubjectTypesSidebarItem",
                    title: this.translatePipe.transform("DataSubjectTypes"),
                    route:
                        "zen.app.pia.module.consentreceipt.views.datasubjecttypes",
                    activeRoute:
                        "zen.app.pia.module.consentreceipt.views.datasubjecttypes",
                });
            }

            if (setupGroup.children.length) {
                menuGroup.push(setupGroup);
            }

            if (this.permissions.canShow("SettingsConsent")) {
                menuGroup.push({
                    id: "ConsentConsentSettingsSidebarItem",
                    title: this.translatePipe.transform("ConsentSettings"),
                    route: "zen.app.pia.module.settings.consent",
                    activeRoute: "zen.app.pia.module.settings.consent",
                    icon: "ot ot-cog",
                });
            }
        }

        return menuGroup;
    }
}
