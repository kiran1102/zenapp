// Third Party
declare var d3;
import {
    Component,
    Input,
    AfterViewInit,
} from "@angular/core";
import Utilities from "Utilities";

// Interfaces
import { IPieGraphData } from "interfaces/pie-graph-data.interface";

@Component({
    selector: "cr-pie-graph",
    templateUrl: "./cr-pie-graph.component.html",
})
export class CrPieGraphComponent implements AfterViewInit {
    @Input() id = Utilities.uuid();
    @Input() title: string;
    @Input() titleClass: string;
    @Input() subTitle: string;
    @Input() subTitleClass: string;
    @Input() graphData: IPieGraphData[];
    @Input() minLabelAngle = .3;    // do not display labels on slices smaller than a given angle(in radians)
    @Input() innerRadius = 75;
    @Input() outerRadius = 150;
    @Input() canvasWidth = 800;
    @Input() canvasHeight;
    @Input() sortSlices = true;

    titleOffset = -2;
    subTitleOffset = 23;
    margin = 20;
    legendTextOffset = 19;
    legendBulletOffset = 80;
    legendBulletRadius = 6;
    textVerticalSpace = 26;

    arc = d3.svg.arc().innerRadius(this.innerRadius).outerRadius(this.outerRadius * 0.9);
    pieWidthTotal = this.outerRadius * 1.1;

    ngAfterViewInit() {
        this.drawPie();
    }

    private drawPie() {
        if (!this.graphData) {
            return;
        }

        if (this.sortSlices) {
            this.graphData = this.graphData.sort((a: IPieGraphData, b: IPieGraphData): number => b.magnitude - a.magnitude);
        }

        const canvas = this.createCanvas(this.textVerticalSpace);
        const arcs = this.addSlices(canvas);
        this.addSliceLabels(arcs);
        this.addLegend(canvas);
        this.addTitle(canvas);
    }

    private addSlices(canvas) {
        const tweenPie: (b: any) => any = (b) => {
            b.innerRadius = 0;
            const i = d3.interpolate({
                startAngle: 0,
                endAngle: 0,
            }, b);
            return (t) => this.arc(i(t));
        };

        const pie = d3.layout
            .pie()
            .value((d) => d.magnitude);
        const arcs = canvas.selectAll("g.slice")
            .data(pie)
            .enter()
            .append("svg:g")
            .attr("class", "slice")
            .style("stroke", "White")
            .attr("d", this.arc);
        arcs.append("svg:path")
            .attr("fill", (d, i: number) => d.data.color)
            .attr("class", (d, i: number) => "pie-" + this.id + "-arc-index-" + i)
            .style("stroke", "White")
            .attr("d", this.arc)
            .on("mouseover", (d, i: number) => this.synchronizedMouseOver(this.id, i))
            .on("mouseout", (d, i: number) => this.synchronizedMouseOut(this.id, i))
            .transition()
            .ease("exp")
            .duration(700)
            .attrTween("d", tweenPie);

        return arcs;
    }

    private addSliceLabels(arcs: any) {
        arcs.filter((d) => d.endAngle - d.startAngle > this.minLabelAngle)
            .append("svg:text")
            .attr("dy", ".35em")
            .attr("text-anchor", "middle")
            .attr("class", (d, i: number) => `cr-pie-graph--label pie-${this.id}-arc-label-index-${i}`)
            .attr("transform", (d) => {
                d.outerRadius = this.outerRadius;
                d.innerRadius = this.innerRadius;
                const c = this.arc.centroid(d);
                return `translate(${c[0]},${c[1]})`;
            })
            .style("fill", "White")
            .text((d) => d.data.label);
    }

    private addLegend(canvas) {
        const legendVerticalOffset = this.outerRadius - this.margin;

        canvas.selectAll("circle")
            .data(this.graphData)
            .enter()
            .append("svg:circle")
            .attr("cx", this.pieWidthTotal + this.legendBulletOffset)
            .attr("cy", (d, i: number) => i * this.textVerticalSpace - legendVerticalOffset)
            .attr("stroke-width", ".5")
            .style("fill", (d, i: number) => d.color)
            .attr("r", this.legendBulletRadius)
            .attr("class", (d, i: number) => `pie-${this.id}-legendBullet-index-${i}`);

        canvas.selectAll(".legend_link")
            .data(this.graphData)
            .enter()
            .append("text")
            .attr("text-anchor", "center")
            .attr("x", this.pieWidthTotal + this.legendBulletOffset + this.legendTextOffset)
            .attr("y", (d, i: number) => i * this.textVerticalSpace - legendVerticalOffset)
            .attr("dx", 0)
            .attr("dy", "5px")
            .text((d) => d.legendLabel)
            .attr("class", (d, i: number) => `pie-${this.id}-legendText-index-${i} fill-slate text-small`);
    }

    private addTitle(canvas: any) {
        canvas.append("text")
            .attr("text-anchor", "middle")
            .attr("dx", 0)
            .attr("dy", this.titleOffset)
            .attr("class", this.titleClass)
            .text(this.title);

        canvas.append("text")
            .attr("text-anchor", "middle")
            .attr("dx", 0)
            .attr("dy", this.subTitleOffset)
            .attr("class", this.subTitleClass)
            .text(this.subTitle);
    }

    private createCanvas(textVerticalSpace: number) {
        const pieCenterX = this.outerRadius + this.margin / 2;
        const pieCenterY = this.outerRadius + this.margin / 2;
        const pieDrivenHeight = this.outerRadius * 2 + this.margin * 2;
        const legendTextDrivenHeight = (this.graphData.length * textVerticalSpace) + this.margin * 2;
        const canvasHeight = this.canvasHeight ?
            this.canvasHeight :
            pieDrivenHeight >= legendTextDrivenHeight ? pieDrivenHeight : legendTextDrivenHeight;

        return d3
            .select(`#${this.id}`)
            .append("svg:svg")
            .data([this.graphData])
            .attr("width", this.canvasWidth)
            .attr("height", canvasHeight)
            .attr("align", "center")
            .append("svg:g")
            .attr("transform", "translate(" + pieCenterX + "," + pieCenterY + ")");
    }

    private synchronizedMouseOver(id: string, i: number) {
        const arcSelector = `.pie-${id}-arc-index-${i}`;
        const selectedArc = d3.selectAll(arcSelector);
        const arcOver = d3.svg.arc()
            .innerRadius(this.innerRadius)
            .outerRadius(this.outerRadius);
        selectedArc.transition()
            .duration(500)
            .ease("elastic")
            .attr("d", arcOver);
    }

    private synchronizedMouseOut(id: string, i: number) {
        const arcSelector = `.pie-${id}-arc-index-${i}`;
        const selectedArc = d3.selectAll(arcSelector);
        const arcOver = d3.svg.arc()
            .innerRadius(this.innerRadius)
            .outerRadius(this.outerRadius * 0.9);
        selectedArc.transition()
            .duration(300)
            .ease("exp")
            .attr("d", arcOver);
    }
}
