// Third Party
import {
    Component,
    EventEmitter,
    Input,
    Output,
} from "@angular/core";

// Constants
import crConstants from "consentModule/shared/cr-constants";

// Interfaces
import {
    IVersionDetails,
    IVersionGroup,
    IVersionListItem,
} from "interfaces/consent/cr-versions.interface";

// Models and Components
import { CollectionPointDetails } from "crmodel/collection-point-details";
import { EntityItem } from "crmodel/entity-item";
import { PurposeDetails } from "crmodel/purpose-details";

@Component({
    selector: "cr-versions",
    templateUrl: "./cr-versions.component.html",
})

export class CrVersionsComponent {
    @Input() item: PurposeDetails | CollectionPointDetails;
    @Input() versionList: IVersionListItem[];
    @Input() isDrawerOpen: boolean;
    @Input() canDeleteVersion: boolean;
    @Output() toggleDrawer = new EventEmitter<void>();
    @Output() goToVersion = new EventEmitter<number>();
    @Output() deleteVersion = new EventEmitter<number>();

    crConstants = crConstants;
    isLoading: boolean;
    isInitialized: boolean;
    selectedPurpose: EntityItem;

    currentVersion: number;
    details: IVersionDetails[];

    trackByFn(index: number, item: IVersionGroup | IVersionListItem) {
        return index;
    }

    toggleItem(item: IVersionListItem) {
        item.IsExpanded = !item.IsExpanded;
    }
}
