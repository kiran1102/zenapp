// Third Party
import {
    Component,
    EventEmitter,
    Input,
    Output,
    OnInit,
} from "@angular/core";
import { remove } from "lodash";

// Components
import { LanguageSelectionModal } from "sharedModules/components/language-selection-modal/language-selection-modal.component";

// Constants
import crConstants from "consentModule/shared/cr-constants";

// Interface
import { IGetAllLanguageResponse } from "interfaces/dsar/dsar-edit-language.interface";
import { IEntityTranslation } from "interfaces/consent/entity-translation";
import { ILanguageSelectionModalResolve } from "interfaces/language-selection-modal-resolve.interface";
import { IStringMap } from "interfaces/generic.interface";

// Models
import { ColumnDefinition } from "crmodel/column-definition";
import { PurposeLanguage } from "crmodel/purpose-list";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

// Rxjs
import {
    filter,
    take,
} from "rxjs/operators";

// Services
import { ConsentPurposeService } from "consentModule/shared/services/cr-purpose.service";
import { OtModalService } from "@onetrust/vitreus";

@Component({
    selector: "cr-translation-table",
    templateUrl: "./cr-translation-table.component.html",
})
export class CrTranslationTable implements OnInit {
    @Input() status: string;
    @Input() defaultName: string;
    @Input() defaultDescription: string;
    @Input() defaultLanguage: string;
    @Input() columns: ColumnDefinition[];
    @Input() editable: boolean;
    @Input() currentTranslations: IEntityTranslation[];
    @Input() previousTranslations: IStringMap<PurposeLanguage>;
    @Input() selectedLanguages: string[];
    @Input() isTopicLanguageTable = false;
    @Output() translationChange: EventEmitter<IEntityTranslation[]> = new EventEmitter<IEntityTranslation[]>();
    @Output() languagesSelected: EventEmitter<void> = new EventEmitter<void>();

    isLoading: boolean;
    cellHeight: string;
    crConstants = crConstants;

    allLanguages: IGetAllLanguageResponse[];
    languageSelections: { [key: number]: IGetAllLanguageResponse } = {};
    languageLabels: { [key: string]: string } = {};
    draftTranslations: IEntityTranslation[];

    constructor(
        private consentPurposeService: ConsentPurposeService,
        private otModalService: OtModalService,
        private translatePipe: TranslatePipe,
    ) {
    }

    ngOnInit() {
        this.cellHeight = this.isTopicLanguageTable ? "min-height-10" : "min-height-20";
        if (this.currentTranslations) { this.draftTranslations = this.currentTranslations; }
        this.consentPurposeService.getAllLanguages().then((languages: IGetAllLanguageResponse[]) => {
            this.allLanguages = languages;
            this.setupLanguages();
        }).finally(() => {
            this.isLoading = false;
        });
    }

    trackByFn(index: number, item: ColumnDefinition) {
        return index;
    }

    get statusDraft(): boolean {
        return !this.status || this.status === crConstants.CRPurposeStatus.Draft;
    }

    validateTranslations(): boolean {
        return this.draftTranslations.every((translation: IEntityTranslation) => this.validateTranslation(translation));
    }

    validateTranslation(translation: IEntityTranslation): boolean {
        return this.columns.every((definition: ColumnDefinition): boolean =>
            !definition.required || translation[definition.cellValueKey]);
    }

    canEditRowLanguage(row: IEntityTranslation): boolean {
        return this.editable && (row.IsNew || !row.Language) && this.statusDraft;
    }

    addTranslation() {
        this.draftTranslations.push({
            Language: "",
            Name: "",
            Description: "",
            Default: false,
            IsNew: true,
        });
    }

    removeTranslation(row: IEntityTranslation) {
        remove(this.draftTranslations,
            (translation: IEntityTranslation): boolean => row.Language === translation.Language);
    }

    saveChanges() {
        this.draftTranslations.forEach((translation: IEntityTranslation) => {
            translation.IsNew = undefined;
        });
        this.translationChange.emit(this.draftTranslations);
    }

    onModelChange(payload: string, rowIndex: number, prop: keyof IEntityTranslation) {
        this.draftTranslations[rowIndex][prop] = payload;
        this.translationChange.emit(this.draftTranslations);
    }

    showEditLanguagesModal() {
        const languageSelectionModalData: ILanguageSelectionModalResolve = {
            defaultLanguage: this.defaultLanguage,
            selectedLanguages: this.selectedLanguages,
            allLanguages: this.allLanguages,
        };
        this.otModalService.create(
            LanguageSelectionModal,
            {
                isComponent: true,
            },
            languageSelectionModalData,
        ).pipe(
            take(1),
            filter((response: { languages: string[], defaultLanguage: string }): boolean => {
                return response && response.languages.length && ( response.defaultLanguage.trim() !== null);
            }),
        ).subscribe(({ languages, defaultLanguage }) => {
            this.selectLanguages(languages, defaultLanguage);
        });
    }

    selectLanguages(selection: string[], defaultLanguage: string) {
        this.defaultLanguage = defaultLanguage;
        selection.forEach((item: string) => {
            if (!this.selectedLanguages.find((language: string): boolean => {
                return item === language;
            })) {
                this.draftTranslations.push({
                    Name: this.defaultName,
                    Description: this.defaultDescription,
                    Language: this.allLanguages.find((language: IGetAllLanguageResponse): boolean => {
                        return item === language.Code;
                    }).Code,
                    Default: item === this.defaultLanguage,
                    IsNew: true,
                });
            }
        });
        this.selectedLanguages.forEach((item: string) => {
            if (!selection.find((language: string): boolean => {
                return item === language;
            })) {
                const rowToRemove = this.draftTranslations.find((row: IEntityTranslation): boolean => {
                    return row.Language === item;
                });
                this.removeTranslation(rowToRemove);
            }
        });
        this.selectedLanguages = selection;
        this.setDefaultLanguage(defaultLanguage);
        this.saveChanges();
        this.languagesSelected.emit();
    }

    private setupLanguages() {
        this.allLanguages.forEach((language: IGetAllLanguageResponse) => {
            this.languageLabels[language.Code] = language.Name;
        });
        let defaultTranslation: IEntityTranslation;
        for (let i = 0; i < this.draftTranslations.length; i++) {
            const translation: IEntityTranslation = this.draftTranslations[i];
            if (translation.Language) {
                this.languageSelections[i] = this.allLanguages.find((language: IGetAllLanguageResponse): boolean => language.Code === translation.Language);
                if (translation.Language === this.defaultLanguage) {
                    defaultTranslation = translation;
                }
            }
        }
        this.sortLanguages(defaultTranslation);
    }

    private setDefaultLanguage(defaultLanguage: string) {
        this.draftTranslations.forEach((item: IEntityTranslation) => {
            item.Default = item.Language === defaultLanguage;
            if (item.Language === defaultLanguage) {
                this.sortLanguages(item);
            }
        });
    }

    private sortLanguages(defaultTranslation: IEntityTranslation) {
        const shouldShortLanguages = this.draftTranslations.length > 1;
        if (shouldShortLanguages === false) {
            return;
        }

        this.draftTranslations.sort((a: IEntityTranslation, b: IEntityTranslation): number => {
            if (this.languageLabels[a.Language] > this.languageLabels[b.Language]) {
                return 1;
            } else if (this.languageLabels[a.Language] < this.languageLabels[b.Language]) {
                return -1;
            }
            return 0;
        });
        const defaultIndex = this.draftTranslations.findIndex((item: IEntityTranslation): boolean => {
            return item === defaultTranslation;
        });
        this.draftTranslations.splice(defaultIndex, 1);
        this.draftTranslations.splice(0, 0, defaultTranslation);
    }
}
