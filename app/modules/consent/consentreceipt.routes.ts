import { Permissions } from "modules/shared/services/helper/permissions.service";
import { IStringMap } from "interfaces/generic.interface";

const consentResolve = (permission: string, fallback: string = "") => {
    return {
        ConsentReceiptPermission: [
            "GeneralData", "Permissions",
            (GeneralData: IStringMap<any>, permissions: Permissions): boolean => {
                if (permissions.canShow(permission)) {
                    return true;
                } else {
                    permissions.goToFallback(fallback);
                }
                return false;
            },
        ],
    };
};

export function routes($stateProvider: ng.ui.IStateProvider): void {

    $stateProvider
        .state("zen.app.pia.module.consentreceipt", {
            abstract: true,
            url: "consentreceipt",
            data: {
                breadcrumb: {
                    text: "ConsentManagement",
                    stateName: "zen.app.pia.module.consentreceipt.views.dashboard",
                },
            },
            resolve: consentResolve("ConsentReceipt"),
            views: {
                module: {
                    template: "<ui-view></ui-view>",
                },
            },
        })
        .state("zen.app.pia.module.consentreceipt.views", {
            abstract: true,
            url: "",
        })
        .state("zen.app.pia.module.consentreceipt.views.dashboard", {
            url: "/dashboard",
            template: "<cr-wrapper><cr-dashboard></cr-dashboard></cr-wrapper>",
            params: { time: null },
            data: {
                breadcrumb: "Dashboard",
            },
        })
        .state("zen.app.pia.module.consentreceipt.views.purposes", {
            abstract: true,
            url: "/purposes",
            data: {
                breadcrumb: {
                    text: "Purposes",
                    stateName: "zen.app.pia.module.consentreceipt.views.purposes.list",
                },
            },
        })
        .state("zen.app.pia.module.consentreceipt.views.purposes.list", {
            url: "",
            template: "<cr-wrapper><cr-purpose-list></cr-purpose-list></cr-wrapper>",
            params: { time: null },
        })
        .state("zen.app.pia.module.consentreceipt.views.purposes.details", {
            url: "/:Id/:tab?version",
            template: "<cr-wrapper><cr-purpose-details></cr-purpose-details></cr-wrapper>",
            params: {
                time: null,
                tab: {
                    value: "details",
                    dynamic: true,
                },
            },
            data: {
                breadcrumb: "Purpose",
            },
        })
        .state("zen.app.pia.module.consentreceipt.views.purposes.create", {
            url: "/create",
            template: "<cr-wrapper><cr-purpose-create></cr-purpose-create></cr-wrapper>",
            params: { time: null },
            resolve: consentResolve("ConsentCreatePurposes"),
        })
        .state("zen.app.pia.module.consentreceipt.views.collections", {
            abstract: true,
            url: "/collections",
            data: {
                breadcrumb: {
                    text: "CollectionPoints",
                    stateName: "zen.app.pia.module.consentreceipt.views.collections.list",
                },
            },
        })
        .state("zen.app.pia.module.consentreceipt.views.collections.list", {
            url: "?purposeId&purposeVersion&consentType&collectionPointType&collectionPointStatus",
            template: "<cr-wrapper><cr-collection-point-list></cr-collection-point-list></cr-wrapper>",
            params: { time: null },
        })
        .state("zen.app.pia.module.consentreceipt.views.collections.updates", {
            url: "/updates?purposeId&purposeVersion",
            template: "<cr-wrapper><cr-collection-point-updates></cr-collection-point-updates></cr-wrapper>",
            params: { time: null },
        })
        .state("zen.app.pia.module.consentreceipt.views.collections.details", {
            url: "/:Id/:tab?version",
            template: "<cr-wrapper><cr-collection-point-details></cr-collection-point-details></cr-wrapper>",
            params: {
                time: null,
                tab: {
                    value: "details",
                    dynamic: true,
                },
            },
            data: {
                breadcrumb: "CollectionPoint",
            },
        })
        .state("zen.app.pia.module.consentreceipt.views.collections.type", {
            url: "/type",
            template: "<cr-wrapper><cr-collection-point-type></cr-collection-point-type></cr-wrapper>",
            params: { time: null },
            resolve: consentResolve("ConsentCollectionPointCreate", "zen.app.pia.module.consentreceipt.views.collections.list"),
        })
        .state("zen.app.pia.module.consentreceipt.views.collections.create", {
            url: "/create?type",
            params: { collectionPoint: null, time: null },
            template: "<cr-wrapper><cr-collection-point-create></cr-collection-point-create></cr-wrapper>",
            resolve: consentResolve("ConsentCollectionPointCreate", "zen.app.pia.module.consentreceipt.views.collections.list"),
        })
        .state("zen.app.pia.module.consentreceipt.views.preferencecenters", {
            abstract: true,
            url: "/preference-centers",
            data: {
                breadcrumb: {
                    text: "PreferenceCenters",
                    stateName: "zen.app.pia.module.consentreceipt.views.preferencecenters.list",
                },
            },
            resolve: consentResolve("ConsentPreferenceCenters"),
        })
        .state("zen.app.pia.module.consentreceipt.views.preferencecenters.list", {
            url: "/list",
            template: "<cr-wrapper><cr-preference-center-list></cr-preference-center-list></cr-wrapper>",
            params: { time: null },
            resolve: consentResolve("ConsentPreferenceCenters"),
        })
        .state("zen.app.pia.module.consentreceipt.views.preferencecenters.details", {
            url: "/:Id/:tab",
            template: "<cr-wrapper><cr-preference-center-details></cr-preference-center-details></cr-wrapper>",
            params: {
                time: null,
                tab: {
                    value: "builder",
                    dynamic: true,
                },
            },
            data: {
                breadcrumb: "PreferenceCenter",
            },
            resolve: consentResolve("ConsentPreferenceCenters"),
        })
        .state("zen.app.pia.module.consentreceipt.views.datasubjects", {
            url: "/data-subjects?purposeId",
            abstract: true,
            data: {
                breadcrumb: {
                    text: "DataSubjects",
                    stateName: "zen.app.pia.module.consentreceipt.views.datasubjects.list",
                },
            },
            resolve: consentResolve("ConsentReceiptReportsDataSubjects"),
        })
        .state("zen.app.pia.module.consentreceipt.views.datasubjects.list", {
            url: "",
            template: "<cr-wrapper><cr-data-subject-list></cr-data-subject-list></cr-wrapper>",
            params: { time: null },
            resolve: consentResolve("ConsentReceiptReportsDataSubjects"),
        })
        .state("zen.app.pia.module.consentreceipt.views.datasubjects.details", {
            url: "/:Id/details",
            template: "<cr-wrapper><cr-data-subject-details></cr-data-subject-details></cr-wrapper>",
            params: { time: null },
            data: {
                breadcrumb: {
                    text: "DataSubject",
                },
            },
            resolve: consentResolve("ConsentReceiptReportsDataSubjects"),
        })
        .state("zen.app.pia.module.consentreceipt.views.transactions", {
            abstract: true,
            url: "/transactions?identifier&collectionPointId&purposeId&receiptId",
            data: {
                breadcrumb: {
                    text: "Transactions",
                    stateName: "zen.app.pia.module.consentreceipt.views.transactions.list",
                },
            },
            resolve: consentResolve("ConsentReceiptReportsTransactions"),
        })
        .state("zen.app.pia.module.consentreceipt.views.transactions.list", {
            url: "",
            template: "<cr-wrapper><cr-transaction-list></cr-transaction-list></cr-wrapper>",
            params: { time: null },
            resolve: consentResolve("ConsentReceiptReportsTransactions"),
        })
        .state("zen.app.pia.module.consentreceipt.views.transactions.details", {
            url: "/:Id/details",
            template: "<cr-wrapper><cr-transaction-details></cr-transaction-details></cr-wrapper>",
            params: { time: null },
            resolve: consentResolve("ConsentReceiptReportsTransactions"),
        })
        .state("zen.app.pia.module.consentreceipt.views.topics", {
            abstract: true,
            url: "/topics",
            params: { time: null },
            data: {
                breadcrumb: {
                    text: "Topics",
                    stateName: "zen.app.pia.module.consentreceipt.views.topics.list",
                },
            },
            resolve: consentResolve("ConsentEditTopics"),
        })
        .state("zen.app.pia.module.consentreceipt.views.topics.list", {
            url: "",
            template: "<cr-wrapper><cr-topic-list></cr-topic-list></cr-wrapper>",
            params: { time: null },
            resolve: consentResolve("ConsentEditTopics"),
        })
        .state("zen.app.pia.module.consentreceipt.views.topics.details", {
            url: "/:Id",
            template: "<cr-wrapper><cr-topic-details></cr-topic-details></cr-wrapper>",
            params: { time: null },
            resolve: consentResolve("ConsentEditTopics"),
            data: {
                breadcrumb: "Topic",
            },
        })
        .state("zen.app.pia.module.consentreceipt.views.custompreferences", {
            abstract: true,
            url: "/custom-preferences",
            params: { time: null },
            data: {
                breadcrumb: {
                    text: "CustomPreferences",
                    stateName: "zen.app.pia.module.consentreceipt.views.custompreferences.list",
                },
            },
            resolve: consentResolve("ConsentEditCustomPreferences"),
        })
        .state("zen.app.pia.module.consentreceipt.views.custompreferences.list", {
            url: "",
            template: "<cr-wrapper><cr-custom-preference-list></cr-custom-preference-list></cr-wrapper>",
            params: { time: null },
            resolve: consentResolve("ConsentEditCustomPreferences"),
        })
        .state("zen.app.pia.module.consentreceipt.views.custompreferences.details", {
            url: "/:Id/:tab",
            template: "<cr-wrapper><cr-custom-preference-details></cr-custom-preference-details></cr-wrapper>",
            params: {
                time: null,
                tab: {
                    value: "details",
                    dynamic: true,
                },
            },
            resolve: consentResolve("ConsentEditCustomPreferences"),
            data: {
                breadcrumb: "CustomPreference",
            },
        })
        ;
}

routes.$inject = ["$stateProvider"];
