import {
    Pipe,
    PipeTransform,
} from "@angular/core";

import crConstants from "consentModule/shared/cr-constants";

@Pipe({
    name: "consentStatusColor",
})

export class ConsentStatusColorPipe implements PipeTransform {
    transform(status: string): string {
        switch (status) {
            case crConstants.CRCollectionPointStatus.Active:
            case crConstants.CRDoubleOptInStatus.Active:
            case crConstants.CRPreferenceCenterStatus.Active:
            case crConstants.CRPurposeStatus.Active:
            case crConstants.CRTransactionStatus.Active:
                return "success";

            case crConstants.CRCollectionPointStatus.Retired:
            case crConstants.CRDoubleOptInStatus.Inactive:
            case crConstants.CRPreferenceCenterStatus.Inactive:
            case crConstants.CRPurposeStatus.Retired:
            case crConstants.CRReceiptStatus.Expired:
                return "hold";

            case crConstants.CRReceiptStatus.NoConsent:
                return "disabled";

            case crConstants.CRTransactionStatus.Withdrawn:
                return "destructive";

            case crConstants.CRTransactionStatus.OptedOut:
                return "warning";

            case crConstants.CRTransactionStatus.Pending:
                return "primary";

            default:
                return "";
        }
    }
}
