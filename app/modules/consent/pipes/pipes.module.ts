import { NgModule } from "@angular/core";

// Pipes
import { ConsentStatusColorPipe } from "./consent-status-color.pipe";
import { ConsentDoubleOptInStatusColorPipe } from "modules/consent/pipes/consent-double-opt-in-status-color.pipe";
import { ConsentTranslateStatusPipe } from "./consent-translate-status.pipe";
import { ConsentVersionLabelPipe } from "./consent-version-label.pipe";

export const PIPES = [
    ConsentStatusColorPipe,
    ConsentDoubleOptInStatusColorPipe,
    ConsentTranslateStatusPipe,
    ConsentVersionLabelPipe,
];

@NgModule({
    imports: [],
    providers: [],
    exports: [PIPES],
    declarations: [PIPES],
})

export class ConsentPipesModule { }
