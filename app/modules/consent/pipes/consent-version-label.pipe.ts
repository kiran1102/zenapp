import {
    Pipe,
    PipeTransform,
} from "@angular/core";

@Pipe({
    name: "consentVersionLabel",
})

export class ConsentVersionLabelPipe implements PipeTransform {
    transform(version: number): string {
        return `V${version}`;
    }
}
