import {
    Pipe,
    PipeTransform,
} from "@angular/core";

@Pipe({
    name: "consentDoubleOptInStatusColor",
})

export class ConsentDoubleOptInStatusColorPipe implements PipeTransform {
    transform(status: boolean): string {
        switch (status) {
            case true:
                return "success";

            default:
                return "hold";
        }
    }
}
