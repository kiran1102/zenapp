// Angular
import {
    Injectable,
    Inject,
} from "@angular/core";
import {
    Observable,
    from,
} from "rxjs";

// Interfaces
import {
    IProtocolMessage,
    IProtocolResponse,
    IProtocolConfig,
} from "interfaces/protocol.interface";

// Models
import {
    NewCustomPreference,
    CustomPreferenceListItem,
    CustomPreferenceList,
} from "crmodel/custom-preference-list";

// Services
import { ConsentBaseService } from "consentModule/shared/services/consent-base.service";
import { ProtocolService } from "modules/core/services/protocol.service";
import { Principal } from "modules/shared/services/helper/principal.service";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

@Injectable()
export class ConsentCustomPreferenceService extends ConsentBaseService {

    constructor(
        protected OneProtocol: ProtocolService,
        protected translatePipe: TranslatePipe,
        protected principal: Principal,
    ) {
        super(OneProtocol, translatePipe, principal);
    }

    getCustomPreferenceList(page: number, size: number = 20): Observable<CustomPreferenceList> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("GET", `/api/consentmanager/v1/custompreferences?page=${page}&size=${size}&sort=CreatedDate,desc`);
        return from(this.OneProtocol.http(config)
            .then((response: IProtocolResponse<CustomPreferenceList>) => response.result ? response.data : null));
    }

    getAllCustomPreferences(): Observable<CustomPreferenceListItem[]> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("GET", `/api/consentmanager/v1/custompreferences/filters`);
        return from(this.OneProtocol.http(config)
            .then((response: IProtocolResponse<CustomPreferenceListItem[]>) => response.result ? response.data : null));
    }

    getCustomPreference(id: string): Observable<CustomPreferenceListItem> {
        const config: IProtocolConfig = this.OneProtocol.customConfig("GET", `/api/consentmanager/v1/custompreferences/${id}`);
        return from(this.OneProtocol.http(config)
            .then((response: IProtocolResponse<CustomPreferenceListItem>) => response.result ? response.data : null));
    }

    createCustomPreference(customPreference: NewCustomPreference) {
        const successMessage: IProtocolMessage = { custom: this.translatePipe.transform("CustomPreferenceCreated") };
        const errorMessage: IProtocolMessage = { custom: this.translatePipe.transform("CustomPreferenceCreationError") };
        const config: IProtocolConfig = this.OneProtocol.customConfig("POST", `/api/consentmanager/v1/custompreferences`, null, customPreference);
        return this.OneProtocol.http(config, { Error: errorMessage, Success: successMessage })
            .then((response: IProtocolResponse<CustomPreferenceListItem>): boolean => response.result);
    }

    saveCustomPreference(customPreference: CustomPreferenceListItem): Observable<CustomPreferenceListItem> {
        const successMessage: IProtocolMessage = { custom: this.translatePipe.transform("CustomPreferenceSaved") };
        const errorMessage: IProtocolMessage = { custom: this.translatePipe.transform("CustomPreferenceSaveError") };
        const config: IProtocolConfig = this.OneProtocol.customConfig("PUT", `/api/consentmanager/v1/custompreferences/${customPreference.Id}`, null, customPreference);
        return from(this.OneProtocol.http(config, { Error: errorMessage, Success: successMessage })
            .then((response: IProtocolResponse<CustomPreferenceListItem>) => response.result ? response.data : null));
    }
}
