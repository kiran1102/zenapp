// Angular
import { NgModule } from "@angular/core";

// Modules
import { ConsentSharedModule } from "modules/consent/shared/consent-shared.module";

// Services
import { ConsentCustomPreferenceService } from "consentModule/custom-preferences/shared/custom-preference.service";

@NgModule({
    imports: [
        ConsentSharedModule,
    ],

    exports: [
        ConsentSharedModule,
    ],
    providers: [
        ConsentCustomPreferenceService,
    ],
})
export class CustomPreferencesSharedModule { }
