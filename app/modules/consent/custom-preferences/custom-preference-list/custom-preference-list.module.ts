// Angular
import { NgModule } from "@angular/core";

// Components
import { CrCustomPreferenceListComponent } from "consentModule/custom-preferences/custom-preference-list/custom-preference-list.component";
import { CrCreateCustomPreferenceModalComponent } from "consentModule/custom-preferences/custom-preference-list/create-custom-preference-modal/create-custom-preference-modal.component";

// Modules
import { CustomPreferencesSharedModule } from "consentModule/custom-preferences/shared/custom-preferences-shared.module";

@NgModule({
    imports: [
        CustomPreferencesSharedModule,
    ],
    declarations: [
        CrCustomPreferenceListComponent,
        CrCreateCustomPreferenceModalComponent,
    ],
    entryComponents: [
        CrCustomPreferenceListComponent,
        CrCreateCustomPreferenceModalComponent,
    ],
})
export class CustomPreferenceListModule { }
