// Angular
import {
    OnInit,
    Component,
} from "@angular/core";
import { StateService } from "@uirouter/core";

// Components
import { CrCreateCustomPreferenceModalComponent } from "consentModule/custom-preferences/custom-preference-list/create-custom-preference-modal/create-custom-preference-modal.component";
import { CustomPreferenceList } from "crmodel/custom-preference-list";

// Interfaces
import { IBasicTableCell } from "interfaces/tables.interface";

// Models
import { ColumnDefinition } from "crmodel/column-definition";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

// Rxjs
import {
    filter,
    take,
} from "rxjs/operators";

// Services
import { ConsentCustomPreferenceService } from "consentModule/custom-preferences/shared/custom-preference.service";
import {
    OtModalService,
    ModalSize,
} from "@onetrust/vitreus";

@Component({
    selector: "cr-custom-preference-list",
    templateUrl: "./custom-preference-list.component.html",
})
export class CrCustomPreferenceListComponent implements OnInit {
    tableData: CustomPreferenceList;
    pageSize = 20;
    loading = false;
    columns: ColumnDefinition[];

    constructor(
        private stateService: StateService,
        private otModalService: OtModalService,
        private consentCustomPreferenceService: ConsentCustomPreferenceService,
        private translatePipe: TranslatePipe,
    ) {}

    ngOnInit() {
        this.columns = [
            {
                columnName: this.translatePipe.transform("Name"),
                cellValueKey: "Name",
                cellClass: "max-width-50",
                columnType: "idLink",
                route: "zen.app.pia.module.consentreceipt.views.custompreferences.details",
            },
            {
                columnName: this.translatePipe.transform("Options"),
                cellValueKey: "NumberOfOptions",
                columnType: "number",
            },
            {
                columnName: this.translatePipe.transform("Languages"),
                cellValueKey: "NumberOfLanguages",
                columnType: "number",
            },
            {
                columnName: this.translatePipe.transform("CreatedDate"),
                cellValueKey: "CreatedDate",
                columnType: "timestamp",
            },
            {
                columnName: this.translatePipe.transform("UpdatedDate"),
                cellValueKey: "UpdatedDate",
                columnType: "timestamp",
            },
        ];
        this.getCustomPreferenceList();
    }

    getCustomPreferenceList(page: number = 0) {
        this.loading = true;
        this.consentCustomPreferenceService.getCustomPreferenceList(page)
            .pipe(
                take(1),
            ).subscribe((response: CustomPreferenceList) => {
                this.loading = false;
                if (response) { this.tableData = response; }
            });
    }

    createCustomPreference() {
        this.otModalService.create(
            CrCreateCustomPreferenceModalComponent,
            {
                isComponent: true,
                size: ModalSize.MEDIUM,
            },
        ).pipe(
            take(1),
            filter((result: boolean) => result),
        ).subscribe(() => {
            this.getCustomPreferenceList();
        });
    }

    goToRoute(cell: IBasicTableCell) {
        this.stateService.go(cell.route, cell.params);
    }
}
