// Angular
import {
    Component,
    OnInit,
} from "@angular/core";

// Constants
import { crConstants } from "consentModule/shared/cr-constants";

// Interfaces
import { IGetAllLanguageResponse } from "interfaces/dsar/dsar-edit-language.interface";
import { IStringMap } from "interfaces/generic.interface";
import { IDropDownElement } from "consentModule/shared/services/consent-base.service";

// Models
import { NewCustomPreference } from "crmodel/custom-preference-list";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

// Rxjs
import { Subject } from "rxjs";

// Services
import { ConsentCustomPreferenceService } from "consentModule/custom-preferences/shared/custom-preference.service";
import { LookupService } from "sharedModules/services/helper/lookup.service.ts";

@Component({
    selector: "cr-create-custom-preference-modal",
    templateUrl: "./create-custom-preference-modal.component.html",
})
export class CrCreateCustomPreferenceModalComponent implements OnInit {
    selectionTypeOptions: IDropDownElement[];
    selectedSelectionType: IDropDownElement;

    displayAsOptions: IDropDownElement[];
    selectedDisplayAs: IDropDownElement;

    allLanguages: IGetAllLanguageResponse[];
    languageOptions: IGetAllLanguageResponse[];
    selectedLanguage: IGetAllLanguageResponse;

    crConstants = crConstants;

    options: IDropDownElement[];
    optionTab = this.crConstants.CustomPreferenceOptionTabs.Build;

    otModalCloseEvent: Subject<boolean>;
    createInProgress: boolean;

    newCustomPreference = new NewCustomPreference();
    newCustomPreferenceInvalid = true;

    constructor(
        private consentCustomPreferenceService: ConsentCustomPreferenceService,
        private translatePipe: TranslatePipe,
        private lookupService: LookupService,
    ) {}

    ngOnInit() {
        this.newCustomPreference.Options = [""];
        this.consentCustomPreferenceService.getAllLanguages().then((languages: IGetAllLanguageResponse[]) => {
            this.allLanguages = languages;
        });
        this.selectionTypeOptions = this.translateOptions(this.crConstants.CustomPreferenceSelectionTypes);
        this.displayAsOptions = this.translateOptions(this.crConstants.CustomPreferenceDisplayTypes);
        this.getRadioButtonOptions();
    }

    closeModal() {
        this.otModalCloseEvent.next();
    }

    selectSelectionType(selection: IDropDownElement) {
        this.selectedSelectionType = selection;
        this.newCustomPreference.SelectionType = selection.value;
        this.checkNewCustomPreferenceInvalid();
    }

    selectDisplayAs(selection: IDropDownElement) {
        this.selectedDisplayAs = selection;
        this.newCustomPreference.DisplayAs = selection.value;
        this.checkNewCustomPreferenceInvalid();
    }

    onLanguageInputChange({ key, value }) {
        if (key === "Enter") {
            this.selectLanguage({ currentValue: this.languageOptions[0] });
            return;
        }
        this.languageOptions = this.lookupService.filterOptionsByInput([], this.allLanguages, value, "Code", "Name");
    }

    selectLanguage({ currentValue }) {
        this.selectedLanguage = currentValue;
        if (currentValue) {
            this.newCustomPreference.Language = currentValue.Code;
        } else {
            this.newCustomPreference.Language = "";
        }
        this.languageOptions = this.allLanguages;
        this.checkNewCustomPreferenceInvalid();
    }

    onDescriptionChange(payload: string) {
        this.newCustomPreference.Description = payload;
        this.checkNewCustomPreferenceInvalid();
    }

    toggleCustomPreferenceRequired() {
        this.newCustomPreference.Required = !this.newCustomPreference.Required;
    }

    addCustomPreferenceOption(index: number = 0) {
        if (!this.newCustomPreference.Options) {
            this.newCustomPreference.Options = [""];
            return;
        }
        this.newCustomPreference.Options.splice(index, 0, "");
        this.checkNewCustomPreferenceInvalid();
    }

    removeCustomPreferenceOption(index: number) {
        this.newCustomPreference.Options.splice(index, 1);
        this.checkNewCustomPreferenceInvalid();
    }

    checkNewCustomPreferenceInvalid() {
        this.newCustomPreferenceInvalid = !(
            this.newCustomPreference.SelectionType
            && this.newCustomPreference.DisplayAs
            && this.textTrimCheck(this.newCustomPreference.Language)
            && this.textTrimCheck(this.newCustomPreference.Description)
            && this.textTrimCheck(this.newCustomPreference.Name)
            && !this.newCustomPreferenceOptionsInvalidCheck()
        );
    }

    radioButtonSelect(payload: IDropDownElement) {
        this.optionTab = payload.value;
    }

    createCustomPreference() {
        this.createInProgress = true;
        this.consentCustomPreferenceService.createCustomPreference(this.newCustomPreference).then((result: boolean) => {
            this.createInProgress = false;
            this.otModalCloseEvent.next(true);
        });
    }

    trackByFn(index: number, item: string) {
        return index;
    }

    private textTrimCheck(text: string): boolean {
        if (text) {
            return Boolean(text.trim());
        } else {
            return false;
        }
    }

    private newCustomPreferenceOptionsInvalidCheck(): boolean {
        if (!this.newCustomPreference.Options) {
            return true;
        } else {
            let result = false;
            this.newCustomPreference.Options.forEach((option: string) => {
                result = !Boolean(option.trim()) || result;
            });
            return result;
        }
    }

    private translateOptions(options: IStringMap<string>): IDropDownElement[] {
        const translatedKeys: IDropDownElement[] = [];
        Object.keys(options).forEach((key: string) => {
            translatedKeys.push({
                label: this.translatePipe.transform(key),
                value: options[key],
            });
        });
        return translatedKeys;
    }

    private getRadioButtonOptions() {
        this.options = [
            {
                label: this.translatePipe.transform("Build"),
                value: this.crConstants.CustomPreferenceOptionTabs.Build,
            },
            {
                label: this.translatePipe.transform("Preview"),
                value: this.crConstants.CustomPreferenceOptionTabs.Preview,
            },
        ];
    }
}
