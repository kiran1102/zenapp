// Angular
import {
    Component,
    OnInit,
} from "@angular/core";

// Constants
import { crConstants } from "consentModule/shared/cr-constants";

// Interfaces
import { IGetAllLanguageResponse } from "interfaces/dsar/dsar-edit-language.interface";
import { IStringMap } from "interfaces/generic.interface";
import { IDropDownElement } from "consentModule/shared/services/consent-base.service";

// Models
import {
    CustomPreferenceListItem,
    CustomPreferenceOption,
} from "crmodel/custom-preference-list";
import { EntityItem } from "crmodel/entity-item";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

// Rxjs
import { Subject } from "rxjs";

// Services
import { ConsentCustomPreferenceService } from "consentModule/custom-preferences/shared/custom-preference.service";

@Component({
    selector: "cr-edit-custom-preference-modal",
    templateUrl: "./edit-custom-preference-modal.component.html",
})
export class CrEditCustomPreferenceModalComponent implements OnInit {
    selectionTypeOptions: IDropDownElement[];
    selectedSelectionType: IDropDownElement;

    displayAsOptions: IDropDownElement[];
    selectedDisplayAs: IDropDownElement;

    allLanguages: IGetAllLanguageResponse[];
    languageOptions: IGetAllLanguageResponse[];
    selectedLanguage: IGetAllLanguageResponse;

    crConstants = crConstants;

    options: IDropDownElement[];
    optionTab = this.crConstants.CustomPreferenceOptionTabs.Build;

    otModalCloseEvent: Subject<CustomPreferenceListItem>;
    otModalData: CustomPreferenceListItem;

    saveInProgress: boolean;
    customPreferenceInvalid = true;

    constructor(
        private consentCustomPreferenceService: ConsentCustomPreferenceService,
        private translatePipe: TranslatePipe,
    ) {}

    ngOnInit() {
        this.consentCustomPreferenceService.getAllLanguages().then((languages: IGetAllLanguageResponse[]) => {
            this.allLanguages = languages;
        });
        this.selectionTypeOptions = this.translateOptions(this.crConstants.CustomPreferenceSelectionTypes);
        this.displayAsOptions = this.translateOptions(this.crConstants.CustomPreferenceDisplayTypes);
        this.selectedSelectionType = this.selectionTypeOptions.find((selectionType: IDropDownElement) => {
            return selectionType.value === this.otModalData.SelectionType;
        });
        this.selectedDisplayAs = this.displayAsOptions.find((displayAs: IDropDownElement) => {
            return displayAs.value === this.otModalData.DisplayAs;
        });
        this.checkCustomPreferenceInvalid();
        this.checkOptionsExist();
        this.getRadioButtonOptions();
    }

    closeModal() {
        this.otModalCloseEvent.next();
    }

    selectSelectionType(selection: IDropDownElement) {
        this.selectedSelectionType = selection;
        this.otModalData.SelectionType = selection.value;
        this.checkCustomPreferenceInvalid();
    }

    selectDisplayAs(selection: IDropDownElement) {
        this.selectedDisplayAs = selection;
        this.otModalData.DisplayAs = selection.value;
        this.checkCustomPreferenceInvalid();
    }

    onDescriptionChange(payload: string) {
        this.otModalData.Description = payload;
        this.checkCustomPreferenceInvalid();
    }

    toggleCustomPreferenceRequired() {
        this.otModalData.Required = !this.otModalData.Required;
    }

    addCustomPreferenceOption(index: number = 0) {
        const emptyOption: CustomPreferenceOption = { Id: "", Label: "", CanDelete: true };
        this.otModalData.Options.splice(index, 0, emptyOption);
        this.checkCustomPreferenceInvalid();
    }

    removeCustomPreferenceOption(index: number) {
        if (this.otModalData.Options[index].CanDelete === false) {
            return;
        }

        this.otModalData.Options.splice(index, 1);
        this.checkCustomPreferenceInvalid();
    }

    checkCustomPreferenceInvalid() {
        this.customPreferenceInvalid = !(
            this.otModalData.SelectionType
            && this.otModalData.DisplayAs
            && this.textTrimCheck(this.otModalData.Description)
            && this.textTrimCheck(this.otModalData.Name)
            && !this.customPreferenceOptionsInvalidCheck()
        );
    }

    radioButtonSelect(payload: IDropDownElement) {
        this.optionTab = payload.value;
    }

    saveCustomPreference() {
        this.saveInProgress = true;
        this.consentCustomPreferenceService.saveCustomPreference(this.otModalData)
            .subscribe((result: CustomPreferenceListItem) => {
                this.saveInProgress = false;
                this.otModalCloseEvent.next(result);
        });
    }

    trackByFn(index: number) {
        return index;
    }

    private textTrimCheck(text: string): boolean {
        if (text) {
            return Boolean(text.trim());
        } else {
            return false;
        }
    }

    private checkOptionsExist() {
        if (!this.otModalData.Options || !this.otModalData.Options.length) {
            this.otModalData.Options = [{ Id: "", Label: "", CanDelete: true }];
        }
    }

    private customPreferenceOptionsInvalidCheck(): boolean {
        if (!this.otModalData.Options) {
            return true;
        } else {
            let result = false;
            this.otModalData.Options.forEach((option: EntityItem) => {
                result = !Boolean(option.Label.trim()) || result;
            });
            return result;
        }
    }

    private translateOptions(options: IStringMap<string>): IDropDownElement[] {
        const translatedKeys: IDropDownElement[] = [];
        Object.keys(options).forEach((key: string) => {
            translatedKeys.push({
                label: this.translatePipe.transform(key),
                value: options[key],
            });
        });
        return translatedKeys;
    }

    private getRadioButtonOptions() {
        this.options = [
            {
                label: this.translatePipe.transform("Build"),
                value: this.crConstants.CustomPreferenceOptionTabs.Build,
            },
            {
                label: this.translatePipe.transform("Preview"),
                value: this.crConstants.CustomPreferenceOptionTabs.Preview,
            },
        ];
    }
}
