// Angular
import { NgModule } from "@angular/core";

// Components
import { CrCustomPreferenceDetailsComponent } from "consentModule/custom-preferences/custom-preference-details/custom-preference-details.component";
import { CrCustomPreferenceFormComponent } from "consentModule/custom-preferences/custom-preference-details/custom-preference-form/custom-preference-form.component";
import { CrCustomPreferenceOptionsComponent } from "consentModule/custom-preferences/custom-preference-details/custom-preference-options/custom-preference-options.component";
import { CrCustomPreferenceTranslationsComponent } from "consentModule/custom-preferences/custom-preference-details/custom-preference-translations/custom-preference-translations.component";
import { CrEditCustomPreferenceModalComponent } from "consentModule/custom-preferences/custom-preference-details/edit-custom-preference-modal/edit-custom-preference-modal.component";

// Modules
import { CustomPreferencesSharedModule } from "consentModule/custom-preferences/shared/custom-preferences-shared.module";

@NgModule({
    imports: [
        CustomPreferencesSharedModule,
    ],
    declarations: [
        CrCustomPreferenceDetailsComponent,
        CrCustomPreferenceFormComponent,
        CrCustomPreferenceOptionsComponent,
        CrCustomPreferenceTranslationsComponent,
        CrEditCustomPreferenceModalComponent,
    ],
    entryComponents: [
        CrCustomPreferenceDetailsComponent,
        CrEditCustomPreferenceModalComponent,
    ],
})
export class CustomPreferenceDetailsModule { }
