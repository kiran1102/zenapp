// Angular
import {
    Component,
    Input,
    OnInit,
} from "@angular/core";

// Components
import { LanguageSelectionModal } from "sharedModules/components/language-selection-modal/language-selection-modal.component";

// Interfaces
import { IGetAllLanguageResponse } from "interfaces/dsar/dsar-edit-language.interface";
import { ILanguageSelectionModalResolve } from "interfaces/language-selection-modal-resolve.interface";

// Models
import {
    CustomPreferenceListItem,
    CustomPreferenceLanguage,
} from "crmodel/custom-preference-list";
import { EntityItem } from "crmodel/entity-item";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

// Rxjs
import {
    filter,
    take,
} from "rxjs/operators";

// Services
import { OtModalService } from "@onetrust/vitreus";
import { ConsentCustomPreferenceService } from "consentModule/custom-preferences/shared/custom-preference.service";

@Component({
    selector: "cr-custom-preference-translations",
    templateUrl: "./custom-preference-translations.component.html",
    host: { class: "flex-full-height" },
})
export class CrCustomPreferenceTranslationsComponent implements OnInit {
    @Input() allLanguages: IGetAllLanguageResponse[];
    @Input() customPreference: CustomPreferenceListItem;
    @Input() loading: boolean;

    saveInProgress: boolean;

    sourceIndex: number;
    sourceDropdown: IGetAllLanguageResponse;
    sourceLanguageOptions: IGetAllLanguageResponse[] = [];

    targetIndex: number;
    targetDropdown: IGetAllLanguageResponse;
    targetLanguageOptions: IGetAllLanguageResponse[] = [];

    constructor(
        private otModalService: OtModalService,
        private consentCustomPreferenceService: ConsentCustomPreferenceService,
        private translatePipe: TranslatePipe,
    ) {}

    ngOnInit() {
        this.getLanguageDropdownOptions();
        this.sourceDropdown = this.findLanguage(this.customPreference.DefaultLanguage);
        this.sourceIndex = this.findLanguageIndex(this.customPreference.DefaultLanguage);
        this.targetDropdown = this.sourceDropdown;
        this.targetIndex = this.sourceIndex;
    }

    selectSourceLanguage(selection: IGetAllLanguageResponse) {
        this.sourceDropdown = selection;
        this.sourceIndex = this.findLanguageIndex(selection.Code);
    }

    selectTargetLanguage(selection: IGetAllLanguageResponse) {
        this.targetDropdown = selection;
        this.targetIndex = this.findLanguageIndex(selection.Code);
    }

    manageLanguagesModal() {
        const languageSelectionModalData: ILanguageSelectionModalResolve = {
            defaultLanguage: this.customPreference.Languages.find((language: CustomPreferenceLanguage) => language.Default).Language,
            selectedLanguages: this.customPreference.Languages.map((language: CustomPreferenceLanguage) => language.Language),
            allLanguages: this.allLanguages,
        };
        this.otModalService.create(
            LanguageSelectionModal,
            {
                isComponent: true,
            },
            languageSelectionModalData,
        ).pipe(
            take(1),
            filter((response: { languages: string[], defaultLanguage: string }): boolean => {
                return response && response.languages.length && ( response.defaultLanguage.trim() !== null);
            }),
        ).subscribe(({ languages, defaultLanguage }) => {
            this.updateLanguages(languages, defaultLanguage);
        });
    }

    saveCustomPreference() {
        this.saveInProgress = true;
        this.consentCustomPreferenceService.saveCustomPreference(this.customPreference)
        .pipe(
            take(1),
            filter((result: CustomPreferenceListItem) => result != null),
        ).subscribe(() => {
            this.saveInProgress = false;
        });
    }

    private updateLanguages(languages: string[], defaultLanguage: string) {
        this.removeUncheckedLanguages(languages);
        this.addCheckedLanguages(languages);
        this.setDefaultLanguage(defaultLanguage);
        this.saveCustomPreference();
        this.getLanguageDropdownOptions();
        this.updateDropdownSelections();
    }

    private removeUncheckedLanguages(languages: string[]) {
        const languagesToCheck: string[] = [...this.customPreference.Languages.map((customPrefLanguage: CustomPreferenceLanguage) => customPrefLanguage.Language)];
        languagesToCheck.forEach((languageToCheck: string) => {
            if (!languages.find((language: string): boolean => {
                return language === languageToCheck;
            })) {
                const indexToRemove = this.customPreference.Languages.findIndex((customPrefLanguage: CustomPreferenceLanguage) => {
                    return customPrefLanguage.Language === languageToCheck;
                });
                this.customPreference.Languages.splice(indexToRemove, 1);
            }
        });
    }

    private addCheckedLanguages(languages: string[]) {
        languages.forEach((language: string) => {
            if (!this.customPreference.Languages.find((customPrefLanguage: CustomPreferenceLanguage) => {
                return customPrefLanguage.Language === language;
            })) {
                this.customPreference.Languages.push({
                    Name: this.customPreference.Name,
                    Description: this.customPreference.Description,
                    Language: language,
                    Options: this.customPreference.Options.map((option: EntityItem) => JSON.parse(JSON.stringify(option))),
                    Default: false,
                });
            }
        });
    }

    private setDefaultLanguage(defaultLanguage: string) {
        this.customPreference.DefaultLanguage = defaultLanguage;
        this.customPreference.Languages.forEach((item: CustomPreferenceLanguage) => {
            item.Default = item.Language === defaultLanguage;
            if (item.Language === defaultLanguage) {
                this.sortLanguages(item);
            }
        });
    }

    private getLanguageDropdownOptions() {
        this.sourceLanguageOptions = [];
        this.customPreference.Languages.forEach((customPrefLanguage: CustomPreferenceLanguage) => {
            this.sourceLanguageOptions.push(this.allLanguages.find((language: IGetAllLanguageResponse): boolean => {
                return language.Code === customPrefLanguage.Language;
            }));
        });
        this.targetLanguageOptions = [...this.sourceLanguageOptions];
    }

    private updateDropdownSelections() {
        if (this.customPreference.Languages.find((language: CustomPreferenceLanguage): boolean => {
            return this.sourceDropdown.Code === language.Language;
        })) {
            this.selectSourceLanguage(this.sourceDropdown);
        } else {
            this.selectSourceLanguage(this.findLanguage(this.customPreference.DefaultLanguage));
        }

        if (this.customPreference.Languages.find((language: CustomPreferenceLanguage): boolean => {
            return this.targetDropdown.Code === language.Language;
        })) {
            this.selectTargetLanguage(this.targetDropdown);
        } else {
            this.selectTargetLanguage(this.findLanguage(this.customPreference.DefaultLanguage));
        }
    }

    private findLanguage(selection: string): IGetAllLanguageResponse {
        return this.allLanguages.find((language: IGetAllLanguageResponse) => {
            return language.Code === selection;
        });
    }

    private findLanguageIndex(selection: string): number {
        return this.customPreference.Languages.findIndex((language: CustomPreferenceLanguage): boolean => {
            return language.Language === selection;
        });
    }

    private sortLanguages(defaultLanguageObject: CustomPreferenceLanguage) {
        this.customPreference.Languages.sort((a, b) => a.Language.localeCompare(b.Language));
        const defaultIndex = this.customPreference.Languages.findIndex((item: CustomPreferenceLanguage): boolean => {
            return item === defaultLanguageObject;
        });
        this.customPreference.Languages.splice(defaultIndex, 1);
        this.customPreference.Languages.splice(0, 0, defaultLanguageObject);
    }
}
