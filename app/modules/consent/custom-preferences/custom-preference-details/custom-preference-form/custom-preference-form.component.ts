// Angular
import {
    Component,
    EventEmitter,
    Input,
    OnInit,
    Output,
} from "@angular/core";

// Interfaces
import { IGetAllLanguageResponse } from "interfaces/dsar/dsar-edit-language.interface";

// Models
import {
    CustomPreferenceListItem,
    CustomPreferenceLanguage,
} from "crmodel/custom-preference-list";

// Rxjs
import {
    filter,
    take,
} from "rxjs/operators";

// Services
import { ConsentCustomPreferenceService } from "consentModule/custom-preferences/shared/custom-preference.service";
import { LookupService } from "sharedModules/services/helper/lookup.service.ts";

@Component({
    selector: "cr-custom-preference-form",
    templateUrl: "./custom-preference-form.component.html",
})
export class CrCustomPreferenceFormComponent implements OnInit {
    @Input() allLanguages: IGetAllLanguageResponse[];
    @Input() customPreference: CustomPreferenceListItem;
    @Output() resetBreadcrumb = new EventEmitter<CustomPreferenceListItem>();

    customPreferenceCopy: CustomPreferenceListItem;
    customPreferenceLanguages: IGetAllLanguageResponse[] = [];
    languageOptions: IGetAllLanguageResponse[];
    selectedLanguage: IGetAllLanguageResponse;
    editable: boolean;
    descriptionExtended = false;

    constructor(
        private consentCustomPreferenceService: ConsentCustomPreferenceService,
        private lookupService: LookupService,
    ) {}

    ngOnInit() {
        const languageCode = this.customPreference.Languages.find((language: CustomPreferenceLanguage) => {
            return language.Language === this.customPreference.DefaultLanguage;
        }).Language;
        this.customPreference.Languages.forEach((customPrefLanguage: CustomPreferenceLanguage) => {
            this.customPreferenceLanguages.push(this.allLanguages.find((language: IGetAllLanguageResponse): boolean => {
                return language.Code === customPrefLanguage.Language;
            }));
        });
        this.selectedLanguage = this.allLanguages.find((language: IGetAllLanguageResponse) => language.Code === languageCode);
    }

    toggleEditMode() {
        this.editable = !this.editable;
        this.customPreferenceCopy = Object.assign({}, this.customPreference);
    }

    cancelEditMode() {
        this.editable = false;
        this.customPreference = this.customPreferenceCopy;
        this.resetBreadcrumb.emit(this.customPreference);
    }

    extendDescription() {
        this.descriptionExtended = !this.descriptionExtended;
    }

    onLanguageInputChange({ key, value }) {
        if (key === "Enter") {
            this.selectLanguage({ currentValue: this.languageOptions[0] });
            return;
        }
        this.languageOptions = this.lookupService.filterOptionsByInput([], this.customPreferenceLanguages, value, "Code", "Name");
    }

    selectLanguage({ currentValue }) {
        this.selectedLanguage = currentValue;
        if (currentValue) {
            this.setDefaultLanguage(currentValue.Code);
        }
        this.languageOptions = this.customPreferenceLanguages;
    }

    saveCustomPreference() {
        this.customPreference.DefaultLanguage = this.selectedLanguage.Code;
        this.consentCustomPreferenceService.saveCustomPreference(this.customPreference)
            .pipe(
                take(1),
                filter((result: CustomPreferenceListItem) => result !== null),
            ).subscribe(() => {
                this.toggleEditMode();
            });
    }

    private setDefaultLanguage(defaultLanguage: string) {
        this.customPreference.DefaultLanguage = defaultLanguage;
        this.customPreference.Languages.forEach((item: CustomPreferenceLanguage) => {
            item.Default = item.Language === defaultLanguage;
            if (item.Language === defaultLanguage) {
                this.sortLanguages(item);
            }
        });
    }

    private sortLanguages(defaultLanguageObject: CustomPreferenceLanguage) {
        this.customPreference.Languages.sort((a, b) => a.Language.localeCompare(b.Language));
        const defaultIndex = this.customPreference.Languages.findIndex((item: CustomPreferenceLanguage): boolean => {
            return item === defaultLanguageObject;
        });
        this.customPreference.Languages.splice(defaultIndex, 1);
        this.customPreference.Languages.splice(0, 0, defaultLanguageObject);
    }
}
