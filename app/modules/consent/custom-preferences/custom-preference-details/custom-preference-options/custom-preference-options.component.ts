// Angular
import {
    Component,
    Input,
    OnInit,
} from "@angular/core";

// Models
import { CustomPreferenceListItem } from "crmodel/custom-preference-list";
import { ColumnDefinition } from "crmodel/column-definition";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

// Services
import { OtModalService } from "@onetrust/vitreus";

@Component({
    selector: "cr-custom-preference-options",
    templateUrl: "./custom-preference-options.component.html",
})
export class CrCustomPreferenceOptionsComponent implements OnInit {
    @Input() customPreference: CustomPreferenceListItem;
    @Input() loading: boolean;

    columns: ColumnDefinition[];

    constructor(
        private otModalService: OtModalService,
        private translatePipe: TranslatePipe,
    ) {}

    ngOnInit() {
        // TODO: Fetch default language options
    }

    manageOptionsModal() {
        // TODO
        // this.otModalService.create(
        //     CrCustomPreferenceOptionsModalComponent,
        //     {
        //         isComponent: true,
        //         title: this.translatePipe.transform("ManageOptions")
        //     },
        //     this.customPreference,
        // ).pipe(
        //     take(1),
        //     filter((result: boolean) => result),
        // ).subscribe(() => {

        // });
    }
}
