// Angular
import {
    Component,
    OnInit,
} from "@angular/core";
import { StateService } from "@uirouter/core";

// Components
import { CrEditCustomPreferenceModalComponent } from "./edit-custom-preference-modal/edit-custom-preference-modal.component";

// Constants
import { crConstants } from "consentModule/shared/cr-constants";

// Interfaces
import { ITabsNav } from "interfaces/one-tabs-nav.interface";
import { IGetAllLanguageResponse } from "interfaces/dsar/dsar-edit-language.interface";

// Models
import { CustomPreferenceListItem } from "crmodel/custom-preference-list";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

// Rxjs
import {
    filter,
    take,
} from "rxjs/operators";

// Services
import { ConsentCustomPreferenceService } from "consentModule/custom-preferences/shared/custom-preference.service";
import {
    OtModalService,
    ModalSize,
    ConfirmModalType,
} from "@onetrust/vitreus";

@Component({
    selector: "cr-custom-preference-details",
    templateUrl: "./custom-preference-details.component.html",
})
export class CrCustomPreferenceDetailsComponent implements OnInit {
    loading: boolean;
    tabs: ITabsNav[];
    tabName: string;
    customPreference: CustomPreferenceListItem;
    customPreferenceId: string;
    allLanguages: IGetAllLanguageResponse[];

    crConstants = crConstants;

    constructor(
        private stateService: StateService,
        private consentCustomPreferenceService: ConsentCustomPreferenceService,
        private otModalService: OtModalService,
        private translatePipe: TranslatePipe,
    ) {
        this.customPreferenceId = this.stateService.params.Id;
        this.tabName = this.stateService.params.tab;
    }

    ngOnInit() {
        this.getTabs();
        this.loading = true;
        this.consentCustomPreferenceService.getCustomPreference(this.customPreferenceId)
            .pipe(
                take(1),
                filter(Boolean),
            ).subscribe((result: CustomPreferenceListItem) => {
                this.customPreference = result;
                this.loading = false;
            });
        this.consentCustomPreferenceService.getAllLanguages().then((languages: IGetAllLanguageResponse[]) => {
            if (languages) { this.allLanguages = languages; }
        });
    }

    editCustomPreferenceModal() {
        this.otModalService.create(
            CrEditCustomPreferenceModalComponent,
            {
                isComponent: true,
                size: ModalSize.MEDIUM,
            },
            JSON.parse(JSON.stringify(this.customPreference)),
        ).pipe(
            take(1),
            filter((result: CustomPreferenceListItem) => result != null),
        ).subscribe((result: CustomPreferenceListItem) => {
            this.customPreference = result;
        });
    }

    deleteCustomPreferenceModal() {
        this.otModalService.confirm({
            type: ConfirmModalType.DELETE,
            translations: {
                title: this.translatePipe.transform("DeleteCustomPreference"),
                desc: this.translatePipe.transform("CustomPreferenceDeletion"),
                confirm: this.translatePipe.transform("AreYouSureContinue"),
                submit: this.translatePipe.transform("Delete"),
                cancel: this.translatePipe.transform("Cancel"),
            },
        }).pipe(
            take(1),
            filter((result: boolean) => result),
        ).subscribe(() => {
            // this.consentCustomPreferenceService.deleteCustomPreference();
        });
    }

    resetBreadcrumb(customPreference: CustomPreferenceListItem) {
        this.customPreference = customPreference;
    }

    onHeaderRouteClick({ path }: { path: string }) {
        this.stateService.go(path);
    }

    private getTabs() {
        this.tabs = [
            {
                id: this.crConstants.CustomPreferenceTabs.Details,
                text: this.translatePipe.transform("Details"),
                action: (option: ITabsNav) => this.tabChanged(option),
            },
            {
                id: this.crConstants.CustomPreferenceTabs.Options,
                text: this.translatePipe.transform("Options"),
                action: (option: ITabsNav) => this.tabChanged(option),
            },
            {
                id: this.crConstants.CustomPreferenceTabs.Translations,
                text: this.translatePipe.transform("Translations"),
                action: (option: ITabsNav) => this.tabChanged(option),
            },
        ];
    }

    private tabChanged = (option: ITabsNav) => {
        this.tabName = option.id;
        this.stateService.go(".", { Id: this.customPreferenceId, tab: option.id });
    }
}
