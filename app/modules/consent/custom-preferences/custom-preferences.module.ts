// Angular
import { NgModule } from "@angular/core";

// Modules
import { CustomPreferencesSharedModule } from "consentModule/custom-preferences/shared/custom-preferences-shared.module";
import { CustomPreferenceDetailsModule } from "consentModule/custom-preferences/custom-preference-details/custom-preference-details.module";
import { CustomPreferenceListModule } from "./custom-preference-list/custom-preference-list.module";

@NgModule({
    imports: [
        CustomPreferencesSharedModule,
        CustomPreferenceListModule,
        CustomPreferenceDetailsModule,
    ],
})
export class CustomPreferencesModule { }
