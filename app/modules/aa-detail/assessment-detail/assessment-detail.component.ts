// Angular
import {
    Component,
    Inject,
    OnInit,
    OnDestroy,
} from "@angular/core";

// Rxjs
import { Subject } from "rxjs";
import {
    startWith,
    takeUntil,
    take,
    filter as rxjsFilter,
} from "rxjs/operators";

// Third Party
import $ from "jquery";
import { StateService } from "@uirouter/core";

// Vitreus
import {
    OtModalService,
    ModalSize,
} from "@onetrust/vitreus";

// Redux
import observableFromStore from "oneRedux/utils/observable-from-store";
import {
    getAssessmentDetailModel,
    isLoadingAssessment,
    isLoadingSection,
    getIsLoadingMessage,
    getQuestionsInOrder,
    isReadinessTemplate,
    getAssessmentDetailApproverNames,
    getAssessmentDetailRespondentNames,
    getAssessmentDetailSectionModel,
    getAssessmentSectionByQuestionId,
    getDisbaledSectionIds,
    getAssessmentDetailAssessmentId,
    getAssessmentDetailSelectedSectionId,
    AssessmentDetailAction,
} from "modules/assessment/reducers/assessment-detail.reducer";
import { getCurrentUser } from "oneRedux/reducers/user.reducer";
import {
    getPreSelectedRiskId,
    getPreSelectedRootRequestId,
    getPreSelectedCommentQuestionId,
} from "modules/assessment/reducers/assessment.reducer";
import {
    AANeedsMoreInfoReducerAction,
    getNeedsMoreInfoState,
} from "modules/assessment/reducers/aa-needs-more-info.reducer";
import { AANoteActions } from "modules/assessment/reducers/aa-notes-filter.reducer";
import { RiskActions } from "oneRedux/reducers/risk.reducer";
import { StoreToken } from "tokens/redux-store.token";

// Interfaces
import { IAssessmentModel } from "interfaces/assessment/assessment-model.interface";
import { IQuestionModel } from "interfaces/assessment/question-model.interface";
import {
    IStore,
    IStoreState,
} from "interfaces/redux.interface";
import {
    IStringMap,
    INameId,
} from "interfaces/generic.interface";
import { IBreadcrumb } from "interfaces/breadcrumb.interface";
import { IHeaderOption } from "interfaces/assessment.interface";
import { IHelpDialogModal } from "interfaces/help-dialog-modal.interface";
import { ISectionHeaderModel } from "interfaces/assessment/section-model.interface";
import {
    IChangedQuestion,
    IUpdatedTemplateQuestions,
} from "interfaces/assessment/assessment-model.interface";
import { ContextMenuType } from "@onetrust/vitreus";
import { INeedsMoreInfoModel } from "interfaces/assessment/needs-more-info.interface";

// Constants
import {
    AssessmentStatusKeys,
    AssessmentResultKeys,
} from "constants/assessment.constants";
import { TranslationModules } from "constants/translation.constant";

// Enums
import {
    AssessmentStatus,
    SendBackType,
} from "enums/assessment//assessment-status.enum";
import { InventoryTableIds } from "enums/inventory.enum";
import { NeedsMoreInfoFilterState } from "enums/aa-needs-more-info.enum";
import { AANotesTypes } from "enums/assessment/assessment-comment-types.enum";
import { RiskV2States } from "enums/riskV2.enum";
import { ModuleTypes } from "enums/module-types.enum";
import { AASectionFilterTypes } from "modules/assessment/enums/aa-filter.enum";
import { AACollaborationCountsTypes } from "modules/assessment/enums/aa-detail-header.enum";

// Services
import { AssessmentSectionDetailHeaderViewLogicService } from "assessmentServices/view-logic/assessment-detail-header-view-logic.service";
import { AssessmentDetailViewScrollerService } from "modules/assessment/services/view-scroller/assessment-detail-view-scroller.service";
import { AssessmentPermissions } from "constants/assessment.constants";
import { AANeedsMoreInfoActionService } from "modules/assessment/services/action/aa-needs-more-info-action.service";
import { TenantTranslationService } from "sharedServices/tenant-translation.service";
import { AssessmentDetailService } from "modules/assessment/services/view-logic/assessment-detail.service";
import { ModalService } from "sharedServices/modal.service";
import { AaDetailActionService } from "modules/assessment/services/action/aa-detail-action.service";
import { AssessmentDetailApiService } from "modules/assessment/services/api/assessment-detail-api.service";
import { Permissions } from "modules/shared/services/helper/permissions.service";
import { AaSectionNavMessagingService } from "assessmentServices/messaging/aa-section-messaging.service";
import { AssessmentListApiService } from "modules/assessment/services/api/assessment-list-api.service";
import { AaCollaborationPaneCountsService } from "modules/assessment/services/messaging/aa-section-detail-header-helper.service";

// Components
import { AaReviewChangedResponseComponent } from "modules/assessment/components/aa-review-changed-responses-modal/aa-review-changed-responses.component";
import { AaReviewTemplateUpdatesComponent } from "modules/assessment/components/aa-review-updates-modal/aa-review-updates.component";
import { ViewRelatedAssessmentModalComponent } from "sharedModules/components/view-related-assessment-modal/view-related-assessment-modal.component";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";
import { PermitPipe } from "pipes/permit.pipe";
import { NewInventorySchemaIds } from "constants/inventory-config.constant";

@Component({
    selector: "assessment-detail",
    templateUrl: "./assessment-detail.component.html",
})
export class AAAssessmentDetailComponent implements OnInit, OnDestroy {
    assessmentId: string;
    assessmentModel: IAssessmentModel;
    assessmentStatusClass: string;
    assessmentStatusKeys = AssessmentStatusKeys;
    assessmentResultKeys = AssessmentResultKeys;
    canShowRiskCollaborationPane = false;
    isRiskCollaborationDrawerOpen = false;
    isCommentsCollaborationDrawerOpen = false;
    isFetchingAssessment: boolean;
    isLoadingMessage: string;
    isLoadingSection: boolean;
    isNeedsMoreInfoCollaborationDrawerOpen = false;
    isNotesDrawerOpen = false;
    isReadinessTemplate = false;
    isRespondent = false;
    isUserInvited: boolean;
    newSection$ = new Subject();
    permissions: IStringMap<boolean>;
    questionList: IQuestionModel[] = [];
    sectionList = [];
    isReadOnly = false;
    isValidatingAssessment = false;
    isOpen = false;
    approverNames: string[];
    respondentNames: string[];
    sectionHeaderModel: ISectionHeaderModel;
    viewState: IStringMap<boolean>;
    contextMenuType = ContextMenuType;
    closeModalAndGoToQuestion = false;
    scrolledQuestionId: string;
    contextMenuOption: IHeaderOption[] = [];
    scrollToQuestion = false;
    disabledSectionIds: string[];
    hideSectionNav = false;
    invalidQuestionsCount = 0;
    infoRequestsNotSentCount = 0;
    infoRequestBannerDescription: string;
    assessmentStatus = AssessmentStatus;
    approverSendBackFlow = false;

    private translations: IStringMap<string>;
    private breadCrumb: IBreadcrumb;
    private destroy$ = new Subject();
    private restrictAssessmentCompletionWithOpenRisk: boolean;

    constructor(
        @Inject(StoreToken) public store: IStore,
        @Inject("TenantTranslationService") readonly tenantTranslationService: TenantTranslationService,
        private stateService: StateService,
        private assessmentDetailAction: AaDetailActionService,
        private aANeedsMoreInfoAction: AANeedsMoreInfoActionService,
        private assessmentDetailViewScroller: AssessmentDetailViewScrollerService,
        private permissionService: Permissions,
        private AssessmentDetailLogic: AssessmentDetailService,
        private readonly modalService: ModalService,
        private translatePipe: TranslatePipe,
        private assessmentDetailApi: AssessmentDetailApiService,
        private assessmentListAPI: AssessmentListApiService,
        private otModalService: OtModalService,
        private AssessmentSectionDetailHeaderViewLogic: AssessmentSectionDetailHeaderViewLogicService,
        private permitPipe: PermitPipe,
        private aaSectionNavMessagingService: AaSectionNavMessagingService,
        private aaCollaborationPaneCountsService: AaCollaborationPaneCountsService,
    ) { }

    ngOnInit(): void {
        this.permissions = {
            createAndEditNote: this.permitPipe.transform(AssessmentPermissions.CreateAndEditNote),
            viewNote: this.permitPipe.transform(AssessmentPermissions.ViewNote),
            deleteNote: this.permitPipe.transform(AssessmentPermissions.DeleteNote),
            assessmentCanBeApprover: this.permitPipe.transform(AssessmentPermissions.AssessmentCanBeApprover),
            createQuestionComment: this.permitPipe.transform(AssessmentPermissions.CreateQuestionComment),
            viewQuestionComment: this.permitPipe.transform(AssessmentPermissions.ViewQuestionComment),
        };

        this.translations = {
            assessments: this.translatePipe.transform("Assessments"),
            all: this.translatePipe.transform("All"),
            vendors: this.translatePipe.transform("Vendors"),
            processingActivity: this.translatePipe.transform("ProcessingActivity"),
            assets: this.translatePipe.transform("Assets"),
        };
        this.tenantTranslationService.addModuleTranslations(TranslationModules.Inventory);
        this.tenantTranslationService.addModuleTranslations(TranslationModules.Incident);
        this.validateAssessment();

        this.isUserInvited = getCurrentUser(this.store.getState()).RoleName === "Invited";
        this.AssessmentDetailLogic.fetchAssessmentReadOnlyState()
            .pipe(
                rxjsFilter((readonly: boolean): boolean => readonly),
                take(1),
            )
            .subscribe((readonly: boolean): void => {
                this.isReadOnly = readonly;
                this.showReadOnlyModal();
            });

        this.aaSectionNavMessagingService.sectionInvalidQuestionDataListener
            .pipe(
                takeUntil(this.destroy$),
            ).subscribe((count: number) => {
                this.invalidQuestionsCount = count;
            });

        this.aaSectionNavMessagingService.sectionFilter = {
            label: this.translatePipe.transform(AASectionFilterTypes.AllQuestions),
            value: AASectionFilterTypes.AllQuestions,
        };
        this.configureBreadCrumb();
        this.checkForOpenRiskRestriction();
        this.openCollaborationPanes();
    }

    validateAssessment(): void {
        this.isValidatingAssessment = true;
        this.assessmentDetailApi.validateAssessment(this.stateService.params.assessmentId)
            .then((response: boolean): void => {
                if (response) {
                    this.assessmentDetailAction.resetAssessmentDetailState();
                    this.assessmentId = this.stateService.params.assessmentId;
                    this.assessmentDetailAction.fetchAssessmentWelcomeSection(this.assessmentId);
                    this.assessmentDetailAction.fetchAssessmentTemplateMetadata(this.assessmentId);
                    this.initializePaneFilters();

                    observableFromStore(this.store).pipe(
                        startWith(this.store.getState()),
                        takeUntil(this.destroy$),
                    ).subscribe((state: IStoreState): void => this.componentWillReceiveState(state));
                    this.aaCollaborationPaneCountsService.getCollaborationPaneCounts(this.assessmentId, AACollaborationCountsTypes.All);
                } else {
                    this.permissionService.goToFallback();
                }
                this.isValidatingAssessment = false;
            });
    }

    ngOnDestroy(): void {
        this.destroy$.next();
        this.destroy$.complete();
    }

    toggleHideSection(): void {
        this.hideSectionNav = !this.hideSectionNav;
    }

    toggleCommentsCollaborationDrawer(): void {
        this.isCommentsCollaborationDrawerOpen = !this.isCommentsCollaborationDrawerOpen;
        if (this.isCommentsCollaborationDrawerOpen) {
            this.isNotesDrawerOpen = false;
            this.isRiskCollaborationDrawerOpen = false;
            this.isNeedsMoreInfoCollaborationDrawerOpen = false;
        }
    }

    toggleNotesDrawer(): void {
        this.isNotesDrawerOpen = !this.isNotesDrawerOpen;
        if (this.isNotesDrawerOpen) {
            this.isRiskCollaborationDrawerOpen = false;
            this.isNeedsMoreInfoCollaborationDrawerOpen = false;
            this.isCommentsCollaborationDrawerOpen = false;
        }
    }

    toggleRiskCollaborationDrawer(): void {
        this.updateSection();
        this.isRiskCollaborationDrawerOpen = !this.isRiskCollaborationDrawerOpen;
        if (this.isRiskCollaborationDrawerOpen) {
            this.isNotesDrawerOpen = false;
            this.isNeedsMoreInfoCollaborationDrawerOpen = false;
            this.isCommentsCollaborationDrawerOpen = false;
        }
    }

    initializePaneFilters(): void {
        this.aANeedsMoreInfoAction.getAllAssessmentRequestDetails(this.assessmentId);
        if (!this.isNeedsMoreInfoCollaborationDrawerOpen) {
            this.store.dispatch({
                type: AANeedsMoreInfoReducerAction.SET_FILTER_STATE,
                filterState: { label: this.translations.all, value: NeedsMoreInfoFilterState.All },
            });
        }
        this.store.dispatch({
            type: AANoteActions.SET_NOTES_FILTER,
            filterState: { label: this.translations.all, value: AANotesTypes.AllNote },
        });
        this.store.dispatch({
            type: RiskActions.SET_RISK_FILTER,
            riskFilter: { label: this.translations.all, value: RiskV2States.NONE },
        });
    }

    toggleNeedsMoreInfoCollaborationDrawer(openedFromBanner = false): void {
        if (openedFromBanner) {
            this.store.dispatch({
                type: AANeedsMoreInfoReducerAction.SET_FILTER_STATE,
                filterState: { label: this.translatePipe.transform("Open"), value: NeedsMoreInfoFilterState.Open },
            });
        }

        this.isNeedsMoreInfoCollaborationDrawerOpen = !this.isNeedsMoreInfoCollaborationDrawerOpen;
        if (this.isNeedsMoreInfoCollaborationDrawerOpen) {
            this.isNotesDrawerOpen = false;
            this.isRiskCollaborationDrawerOpen = false;
            this.isCommentsCollaborationDrawerOpen = false;
        }
    }

    contextMenuItemSelected(item: IHeaderOption) {
        item.action();
    }

    onToggle(toggleState: boolean): void {
        this.isOpen = toggleState;
    }

    openEditMetadataModal(): void {
        this.isOpen = !this.isOpen;
        this.sectionHeaderModel = this.assessmentDetailAction.getAssessmentDetailSectionHeaderModel(this.store.getState());
        this.assessmentDetailAction.openEditAssessmentDetailsModal(this.assessmentId, this.sectionHeaderModel);
    }

    closeDrawerAndGoToQuestion(event: { questionId: string, scrollToQuestion: boolean }): void {
        this.closeAllPanes();
        if (!(event && event.questionId)) return;

        const sectionId = getAssessmentSectionByQuestionId(event.questionId)(this.store.getState()).id;
        if (!event.scrollToQuestion || this.disabledSectionIds.includes(sectionId)) return;
        this.assessmentDetailAction.goToSectionHelper(sectionId);
        this.scrolledQuestionId = event.questionId;
        this.scrollToQuestion = true;
    }

    paneOverlayClickCallback(): void {
        this.closeAllPanes();
    }

    goToSectionList(): void {
        this.stateService.go("zen.app.pia.module.assessment.list");
    }

    showSendBackModal(): void {
        this.assessmentDetailAction.sendBack(SendBackType.SendBackToInProgress, this.approverSendBackFlow, this.infoRequestBannerDescription);
    }

    private checkForOpenRiskRestriction(): any {
        this.assessmentListAPI.getSspRestrictViewSetting()
            .then((response) => {
                if (response) {
                    this.restrictAssessmentCompletionWithOpenRisk = response.data && response.data.restrictAssessmentCompletionWithOpenRisk;
                }
            });
    }

    private updateSection() {
        if (this.restrictAssessmentCompletionWithOpenRisk) {
            const assessmentId = getAssessmentDetailAssessmentId(this.store.getState());
            const sectionId = getAssessmentDetailSelectedSectionId(this.store.getState());
            this.assessmentDetailAction.fetchCurrentSection(assessmentId).then((assessmentModel: IAssessmentModel): void => {
                if (assessmentModel) {
                    this.store.dispatch({ type: AssessmentDetailAction.UPDATE_ASSESSMENT_ALL, assessmentModel });
                }
            });
        }
    }

    private closeAllPanes(): void {
        this.updateSection();
        this.isRiskCollaborationDrawerOpen = false;
        this.isNotesDrawerOpen = false;
        this.isCommentsCollaborationDrawerOpen = false;
        this.isNeedsMoreInfoCollaborationDrawerOpen = false;
    }

    private initializeContextMenuItems(): void {
        if (this.assessmentModel.parentAssessmentId) {
            this.contextMenuOption.push({
                id: "AADetailsHeaderContextMenuItem_ReviewChangedResponses",
                text: this.translatePipe.transform("ReviewChangedResponses"),
                action: (): void => {
                    this.openChangedResponsesModal();
                },
            });
        }

        if (this.assessmentModel.firstAssignedTemplateId) {
            this.contextMenuOption.push({
                id: "AADetailsHeaderContextMenuItem_ReviewUpdatesHistory",
                text: this.translatePipe.transform("ReviewUpdatesHistory"),
                action: (): void => {
                    this.openReviewUpdatesModal();
                },
            });
        }

        if (this.permissionService.canShow("AssessmentViewRelatedAssessments")) {
            this.contextMenuOption.push({
                id: "AADetailsHeaderContextMenuItem_ViewRelatedAssessments",
                text: this.translatePipe.transform("ViewRelatedAssessments"),
                action: (): void => {
                    this.getViewRelatedAction(this.assessmentModel.assessmentId);
                },
            });
        }
    }

    private componentWillReceiveState(state: IStoreState): void {
        this.isFetchingAssessment = isLoadingAssessment(state);
        this.isLoadingSection = isLoadingSection(state);
        this.assessmentModel = getAssessmentDetailModel(state);
        const preSelectedRiskId: string = getPreSelectedRiskId(state);
        const preSelectedRootRequestId: string = getPreSelectedRootRequestId(state);
        const preSelectedCommentQuestionId: string = getPreSelectedCommentQuestionId(state);
        this.isReadinessTemplate = isReadinessTemplate()(state);

        if (this.assessmentModel) {
            this.viewState = this.AssessmentSectionDetailHeaderViewLogic.calculateNewViewState(this.assessmentModel, getAssessmentDetailSectionModel(state));
        }

        if (preSelectedRiskId) {
            this.isRiskCollaborationDrawerOpen = true;
            this.isNotesDrawerOpen = false;
            this.isNeedsMoreInfoCollaborationDrawerOpen = false;
            this.isCommentsCollaborationDrawerOpen = false;
        }

        if (preSelectedRootRequestId) {
            this.isRiskCollaborationDrawerOpen = false;
            this.isNotesDrawerOpen = false;
            this.isNeedsMoreInfoCollaborationDrawerOpen = true;
            this.isCommentsCollaborationDrawerOpen = false;
        }

        if (preSelectedCommentQuestionId) {
            this.isRiskCollaborationDrawerOpen = false;
            this.isNotesDrawerOpen = false;
            this.isNeedsMoreInfoCollaborationDrawerOpen = false;
            this.isCommentsCollaborationDrawerOpen = true;
        }

        if (this.isLoadingSection) {
            this.isLoadingMessage = getIsLoadingMessage(state);
        } else if ((this.closeModalAndGoToQuestion && this.scrolledQuestionId) || this.scrollToQuestion) {
            setTimeout((): void => {
                this.assessmentDetailViewScroller.scrollToQuestion(this.scrolledQuestionId);
            }, 800);
            this.closeModalAndGoToQuestion = false;
            this.scrollToQuestion = false;
        }

        if (this.assessmentModel && this.assessmentModel.questions) {
            this.questionList = getQuestionsInOrder(state);
        }

        if (!this.isFetchingAssessment && !this.isLoadingSection) {
            // if timeout is causing problems, the next step is to convert this component to an ng2 component so we can use ngAfterViewInit hook
            setTimeout(() => {
                const scrollingContainer = $("#scrolling-container")[0];
                if (scrollingContainer) {
                    this.assessmentDetailViewScroller.registerScrollingContainer(scrollingContainer);
                }
            }, 10);
        }

        if (this.assessmentModel && this.assessmentModel.status) {
            this.assessmentStatusClass = this.getAssessmentStatusClass(this.assessmentModel.status);
            this.canShowRiskCollaborationPane = this.assessmentModel.status === AssessmentStatus.Under_Review || this.assessmentModel.status === AssessmentStatus.Completed;
        }

        if (this.assessmentModel && this.assessmentModel.approvers && this.assessmentModel.approvers.length) {
            this.approverNames = getAssessmentDetailApproverNames(state);
        }

        if (this.assessmentModel && this.assessmentModel.respondents && this.assessmentModel.respondents.length) {
            this.respondentNames = getAssessmentDetailRespondentNames(state);
        }

        const currentUserId = getCurrentUser(state).Id;
        if (this.assessmentModel && this.assessmentModel.respondents.length && currentUserId && !this.isReadinessTemplate) {
            this.isRespondent = this.assessmentModel.respondents.some((respondent: INameId<string>) => respondent.id === currentUserId);
        }

        if (this.assessmentModel && !this.contextMenuOption.length) {
            this.initializeContextMenuItems();
        }

        if (this.assessmentModel && this.assessmentModel.sectionHeaders.byIds) {
            this.disabledSectionIds = getDisbaledSectionIds(state);
        }

        const needsMoreInfoModel: INeedsMoreInfoModel = getNeedsMoreInfoState(state).needsMoreInfoModel;

        if (this.assessmentModel && needsMoreInfoModel) {
            this.initializeOpenMNIBanner(needsMoreInfoModel);
            this.approverSendBackFlow = needsMoreInfoModel.approverSendBackFlow;
        }
    }

    private configureBreadCrumb(): void {
        this.breadCrumb = { stateName: "", text: "" };
        if (this.stateService.params.launchAssessmentDetails) {
            const inventoryTypeId: number = this.stateService.params.launchAssessmentDetails.inventoryTypeId;
            switch (inventoryTypeId) {
                case InventoryTableIds.Vendors:
                    this.breadCrumb.stateName = `zen.app.pia.module.vendor.inventory.list({Id: ${inventoryTypeId}, page: null, size: null})`;
                    this.breadCrumb.text = this.translations.vendors;
                    break;
                default:
                    this.breadCrumb.stateName = `zen.app.pia.module.datamap.views.inventory.list({Id: ${NewInventorySchemaIds[inventoryTypeId]}})`;
                    if (inventoryTypeId === InventoryTableIds.Processes) {
                        this.breadCrumb.text = this.translations.processingActivity;
                    } else {
                        this.breadCrumb.text = this.translations.assets;
                    }
                    break;
            }
        } else if (this.stateService.params.backToModule === ModuleTypes.GRA) {
            this.breadCrumb.stateName = "zen.app.pia.module.globalreadiness.views.assessments";
            this.breadCrumb.text = this.translations.assessments;
        } else {
            this.breadCrumb.stateName = "zen.app.pia.module.assessment.list";
            this.breadCrumb.text = this.translations.assessments;
        }
    }

    private getAssessmentStatusClass(assessmentModelStatus: string): string {
        switch (assessmentModelStatus) {
            case AssessmentStatus.Not_Started:
                return "one-tag--primary-white";
            case AssessmentStatus.In_Progress:
                return "one-tag--primary-blue";
            case AssessmentStatus.Under_Review:
                return "one-tag--primary-yellow";
            case AssessmentStatus.Completed:
                return "one-tag--primary-green";
        }
    }

    private showReadOnlyModal(): void {
        const modalData: IHelpDialogModal = {
            modalTitle: this.translatePipe.transform("ReadOnly"),
            content: this.translatePipe.transform("ReadOnlyMessage"),
        };
        this.modalService.setModalData(modalData);
        this.modalService.openModal("helpDialogModal");
    }

    private openChangedResponsesModal() {
        this.otModalService
            .create(
                AaReviewChangedResponseComponent,
                {
                    isComponent: true,
                    size: ModalSize.MEDIUM,
                },
                {
                    readOnly: true,
                },
            ).subscribe((question: IChangedQuestion) => {
                if (question && question.sectionId) {
                    this.assessmentDetailAction.goToSectionHelper(question.sectionId);
                    this.closeModalAndGoToQuestion = true;
                    this.scrolledQuestionId = question.questionId;
                }
            });
    }

    private openReviewUpdatesModal() {
        this.otModalService
            .create(
                AaReviewTemplateUpdatesComponent,
                {
                    isComponent: true,
                },
                {
                    oldTemplateId: this.assessmentModel.firstAssignedTemplateId,
                    newTemplateId: this.assessmentModel.template.id,
                    disabledSections: this.disabledSectionIds,
                },
            ).subscribe((question: IUpdatedTemplateQuestions) => {
                if (question && question.sectionId) {
                    this.assessmentDetailAction.goToSectionHelper(question.sectionId);
                    this.closeModalAndGoToQuestion = true;
                    this.scrolledQuestionId = question.questionId;
                }
            });
    }

    private getViewRelatedAction(assessmentId: string): void {
        this.otModalService
            .create(
                ViewRelatedAssessmentModalComponent,
                {
                    isComponent: true,
                    size: ModalSize.MEDIUM,
                },
                assessmentId,
            );
    }

    private initializeOpenMNIBanner(needsMoreInfoModel: INeedsMoreInfoModel): void {
        this.infoRequestsNotSentCount = needsMoreInfoModel.openInfoRequestCount;

        if (this.infoRequestsNotSentCount) {
            this.infoRequestBannerDescription = this.translatePipe.transform("InfoRequestBannerAlertMsg", {
                REQUESTS_NOT_SENT_COUNT: `<span class="color-ui-blue text-bold">${this.infoRequestsNotSentCount}</span>`,
            });
            return;
        }

        this.infoRequestBannerDescription = null;
    }

    private openCollaborationPanes(): void {
        if (this.stateService.params.openPanel === "inforequests") {
            this.toggleNeedsMoreInfoCollaborationDrawer(true);
        }
    }
}
