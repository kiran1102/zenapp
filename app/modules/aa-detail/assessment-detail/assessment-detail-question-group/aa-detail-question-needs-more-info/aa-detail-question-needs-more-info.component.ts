// Rxjs
import { Subject } from "rxjs";
import {
    startWith,
    takeUntil,
} from "rxjs/operators";

// External libraries
import {
    Component,
    OnInit,
    OnDestroy,
    Input,
    Inject,
    SimpleChanges,
    OnChanges,
} from "@angular/core";
import { isNil } from "lodash";
import observableFromStore from "oneRedux/utils/observable-from-store";

// Enums
import { NeedsMoreInfoFilterState } from "enums/aa-needs-more-info.enum";

// Interfaces
import { IAssessmentModel } from "interfaces/assessment/assessment-model.interface";
import { IQuestionModel } from "interfaces/assessment/question-model.interface";
import { IUser } from "interfaces/user.interface";
import {
    IStore,
    IStoreState,
} from "interfaces/redux.interface";

// Redux
import {
    getAssessmentDetailModel,
    getAssessmentDetailSelectedSectionId,
} from "modules/assessment/reducers/assessment-detail.reducer";
import { getCurrentUser } from "oneRedux/reducers/user.reducer";
import { getNeedsMoreInfoState } from "modules/assessment/reducers/aa-needs-more-info.reducer";
import { StoreToken } from "tokens/redux-store.token";

// Services
import { AANeedsMoreInfoActionService } from "modules/assessment/services/action/aa-needs-more-info-action.service";
import { Permissions } from "modules/shared/services/helper/permissions.service";

// Constants
import { QuestionTypeNames } from "constants/question-types.constant";
import { INeedsMoreInfo } from "interfaces/assessment/needs-more-info.interface";
import { AssessmentAction } from "modules/assessment/reducers/assessment.reducer";
import { AssessmentStatus } from "enums/assessment/assessment-status.enum";
import { AssessmentDetailService } from "modules/assessment/services/view-logic/assessment-detail.service";

@Component({
    selector: "aa-detail-question-needs-more-info",
    templateUrl: "./aa-detail-question-needs-more-info.component.html",
})

export class AADetailQuestionNeedsMoreInfoComponent implements OnInit, OnDestroy, OnChanges {

    @Input() question: IQuestionModel;

    assessment: IAssessmentModel;
    currentUser: IUser;
    needsMoreInfoMetadata: INeedsMoreInfo[];
    sectionId: string;
    canShowNeedsMoreInfoAddButton = false;
    isReadOnly = false;
    private destroy$ = new Subject();

    constructor(
        @Inject(StoreToken) private store: IStore,
        private permissions: Permissions,
        private aaNeedsMoreInfoActionService: AANeedsMoreInfoActionService,
        private assessmentDetailLogic: AssessmentDetailService,
    ) { }

    ngOnInit() {
        observableFromStore(this.store).pipe(
            startWith(this.store.getState()),
            takeUntil(this.destroy$),
        ).subscribe((state: IStoreState) => this.componentWillReceiveState(state));
        this.assessmentDetailLogic.fetchAssessmentReadOnlyState().subscribe((readonly: boolean) => {
            this.isReadOnly = readonly;
        });
    }

    ngOnDestroy(): void {
        this.destroy$.next();
        this.destroy$.complete();
    }

    ngOnChanges(changes: SimpleChanges): void {
        if (changes.question) {
            this.componentWillReceiveState();
        }
    }

    public needsMoreInfoOnClick = (metadata: INeedsMoreInfo = null): void => {
        if (isNil(metadata)) {
            this.store.dispatch({ type: AssessmentAction.SET_PRESELECTED_ROOTREQUESTID, preSelectedRootRequestId: "" });
            this.aaNeedsMoreInfoActionService.openNeedsMoreInfoModal(this.assessment.assessmentId, this.sectionId, this.question.id);
        } else if (metadata && metadata.rootRequestId) {
            this.store.dispatch({ type: AssessmentAction.SET_PRESELECTED_ROOTREQUESTID, preSelectedRootRequestId: metadata.rootRequestId });
        }
    }

    public trackByFn(index: number, needsMoreInfo: INeedsMoreInfo): string {
        return needsMoreInfo.rootRequestId;
    }

    private componentWillReceiveState(state: IStoreState = null): void {
        state = state || this.store.getState();
        this.currentUser = getCurrentUser(state);
        this.assessment = getAssessmentDetailModel(state);
        this.sectionId = getAssessmentDetailSelectedSectionId(state);
        this.needsMoreInfoMetadata = getNeedsMoreInfoState(state).needsMoreInfoModel.rootRequestInformationResponses.filter((needsMoreInfo: INeedsMoreInfo) => {
            if (needsMoreInfo.questionId === this.question.id && needsMoreInfo.responseState === NeedsMoreInfoFilterState.Open) {
                return true;
            }
            return false;
        });
        this.canShowNeedsMoreInfoAddButton = this.setInfoButtonVisibility();
    }

    private setInfoButtonVisibility(): boolean {
        return this.question.questionType !== QuestionTypeNames.Statement
            && this.question.questionType !== QuestionTypeNames.Incident
            && this.assessment.status === AssessmentStatus.Under_Review
            && this.permissions.canShow("AssessmentRequestInfo");
    }
}
