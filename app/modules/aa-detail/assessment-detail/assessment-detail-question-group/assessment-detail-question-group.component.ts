// Rxjs
import { Subscription } from "rxjs";
import { startWith } from "rxjs/operators";

// Angular
import {
    Component,
    Inject,
    Input,
    SimpleChanges,
    ChangeDetectionStrategy,
    ChangeDetectorRef,
    OnInit,
    OnChanges,
} from "@angular/core";

// Third Party
import {
    each,
    includes,
} from "lodash";

// Enums
import { QuestionLockReason } from "enums/assessment/assessment-question.enum";
import { QuestionAnswerTypes } from "enums/question-types.enum";

// Redux
import {
    getQuestionResponseIds,
    getAssessmentDetailSelectedSectionId,
    getAssessmentDetailSectionNumber,
    getQuestionResponsesArray,
    getSubQuestionResponsesArray,
    getInvalidQuestionRiskIds,
    geInventoryQuestionByAttributeQuestionId,
    isReadinessTemplate,
} from "modules/assessment/reducers/assessment-detail.reducer";
import { StoreToken } from "tokens/redux-store.token";
import observableFromStore from "oneRedux/utils/observable-from-store";

// Interfaces
import {
    IStore,
    IStoreState,
} from "interfaces/redux.interface";
import { IAssessmentModel } from "interfaces/assessment/assessment-model.interface";
import { IStringMap } from "interfaces/generic.interface";
import { IQuestionResponseV2 } from "interfaces/assessment/question-response-v2.interface";
import { IQuestionModel, IQuestionModelOption } from "interfaces/assessment/question-model.interface";
import { IAssessmentQuestionViewState } from "interfaces/assessment/assessment-question-viewstate.interface";
import { AssessmentStatus } from "enums/assessment/assessment-status.enum";

// Services
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import { AssessmentSectionQuestionViewLogicService } from "modules/assessment/services/view-logic//assessment-section-question-view-logic.service";
import { AssessmentMultichoiceDropdownViewLogicService } from "modules/assessment/services/view-logic/assessment-multichoice-dropdown-view-logic.service";
import { AaDetailActionService } from "modules/assessment/services/action/aa-detail-action.service";

// Constants
import { QuestionTypeNames } from "constants/question-types.constant";

@Component({
    selector: "assessment-detail-question-group",
    templateUrl: "./assessment-detail-question-group.component.html",
    changeDetection: ChangeDetectionStrategy.OnPush,
})

export class AssessmentDetailQuestionGroupComponent implements OnInit, OnChanges {

    @Input() questionList: IQuestionModel[] = [];
    @Input() assessmentModel: IAssessmentModel;

    assessmentStatus = AssessmentStatus;
    newOtherModel = "";
    questionTypes = QuestionTypeNames;
    questionLockReason = QuestionLockReason;
    sectionIndex: number;
    viewState: IStringMap<IAssessmentQuestionViewState> = {};
    invalidQuestionRiskIdsMap: IStringMap<string[]> = {};
    questionAnswerTypes = QuestionAnswerTypes;
    isReadinessTemplate = false;

    private storeSubscription: Subscription;

    constructor(
        @Inject(StoreToken) private store: IStore,
        @Inject("$rootScope") public rootScope: IExtendedRootScopeService,
        private assessmentDetailAction: AaDetailActionService,
        private AssessmentSectionQuestionViewLogic: AssessmentSectionQuestionViewLogicService,
        private AssessmentMultichoiceService: AssessmentMultichoiceDropdownViewLogicService,
        private changeDetectorRef: ChangeDetectorRef,
    ) { }

    ngOnChanges(changes: SimpleChanges): void {
        if (changes.questionList) {
            this.updateViewState();
            this.changeDetectorRef.markForCheck();
        }
    }

    ngOnInit(): void {
        this.storeSubscription = observableFromStore(this.store).pipe(
            startWith(this.store.getState()),
        ).subscribe((state: IStoreState): void => this.componentWillReceiveState(state));

        this.isReadinessTemplate = isReadinessTemplate()(this.store.getState());
    }

    updateViewState(): void {
        const sectionId = getAssessmentDetailSelectedSectionId(this.store.getState());
        this.sectionIndex = getAssessmentDetailSectionNumber(sectionId)(this.store.getState());

        each(this.questionList, (question: IQuestionModel): void => {
            this.viewState[question.id] = this.AssessmentSectionQuestionViewLogic.calculateNewViewState(this.assessmentModel, question);
        });
    }

    trackById(index: number, item: any): string {
        return item.id;
    }

    checkOtherForDuplicates(question: IQuestionModel, { response, responseId, subQuestionId = null }: { response: string, responseId: string, subQuestionId: string }): void {
        const questionResponses: IQuestionResponseV2[] = subQuestionId
            ? getSubQuestionResponsesArray(question.id, subQuestionId)(this.store.getState())
            : getQuestionResponsesArray(question.id)(this.store.getState());
        const hasDuplicateOption = this.AssessmentMultichoiceService.checkForDuplicateOptions(response, question.options, "option");
        const hasDuplicateResponse = this.AssessmentMultichoiceService.checkForDuplicateOptions(response, questionResponses, "response");

        if (hasDuplicateOption || hasDuplicateResponse) {
            this.handleOther(question.id, { response, responseId, isDuplicate: true, subQuestionId });
        } else {
            this.handleOther(question.id, { response, responseId, isDuplicate: false, subQuestionId });
        }
    }

    isResponseSelected(questionId: string, optionId: string): boolean {
        const responseIds = getQuestionResponseIds(questionId)(this.store.getState());
        return includes(responseIds, optionId);
    }

    canShowSideButtons(question: IQuestionModel): boolean {
        const invQuestions = geInventoryQuestionByAttributeQuestionId(question.id)(this.store.getState());
        const isMultiSelectInventoryAttribute = question.questionType === this.questionTypes.Attribute
            && invQuestions && invQuestions.attributes && invQuestions.attributes.isMultiSelect;
        return !isMultiSelectInventoryAttribute && question.questionType !== this.questionTypes.PersonalData;
    }

    handleDate(questionId: string, { value, responseId }: { value: string, responseId: string }): void {
        this.assessmentDetailAction.handleDate(questionId, value, "");
    }

    handleYesNo(question: IQuestionModel, option: Partial<IQuestionModelOption>): void {
        this.assessmentDetailAction.handleYesNo(question, option);
    }

    handleMultichoice(question: IQuestionModel, { option, subQuestionId = null }: { option: Partial<IQuestionModelOption>, subQuestionId: string }): void {
        this.assessmentDetailAction.handleMultichoice(question, option, subQuestionId);
    }

    handleMultichoiceReset(questionId: string, subQuestionId: string = null): void {
        this.assessmentDetailAction.handleMultichoiceReset(questionId, subQuestionId);
    }

    handleMultiselectDropdown(question: IQuestionModel, { options, subQuestionId = null }: { options: Array<Partial<IQuestionModelOption>>, subQuestionId: string }): void {
        this.assessmentDetailAction.handleMultiselectDropdown(question, options, subQuestionId);
    }

    handleNotSureNotApplicable(question: IQuestionModel, { option, subQuestionId = null, forceSelect = false }: { option: Partial<IQuestionModelOption>, subQuestionId: string, forceSelect: boolean }): void {
        this.assessmentDetailAction.handleNotSureNotApplicable(question, option, subQuestionId, forceSelect);
    }

    handleJurisdiction(question: IQuestionModel, { jurisdictions, subQuestionId }): void {
        this.assessmentDetailAction.handleJurisdiction(question, jurisdictions, subQuestionId);
    }

    handleOther(questionId: string, { response, responseId, isDuplicate = false, subQuestionId = null }: { response: string, responseId: string, isDuplicate: boolean, subQuestionId?: string }): void {
        this.assessmentDetailAction.handleOther(questionId, response, responseId, isDuplicate, subQuestionId);
    }

    handleTextbox(questionId: string, { response, responseId, subQuestionId = null }: { response: string, responseId: string, subQuestionId: string }): void {
        this.assessmentDetailAction.handleTextbox(response, "", questionId, subQuestionId);
    }

    handleDatetime({ questionId, responseId, subQuestionId = null, value }: { questionId: string, responseId: string, subQuestionId: string, value: string }): void {
        this.assessmentDetailAction.handleDatetime(value, responseId, questionId, subQuestionId);
    }

    handleJustification(question: IQuestionModel, { response, subQuestionId = null }: { response: string, subQuestionId: string }): void {
        this.assessmentDetailAction.handleJustification(response, question.id, subQuestionId);
    }

    private componentWillReceiveState(state: IStoreState): undefined | void {
        this.invalidQuestionRiskIdsMap = getInvalidQuestionRiskIds(state);
        this.changeDetectorRef.markForCheck();
    }
}
