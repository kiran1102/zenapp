// Core
import {
    Component,
    Inject,
    Input,
    OnInit,
} from "@angular/core";

// Redux
import { geInventoryQuestionByAttributeQuestionId } from "modules/assessment/reducers/assessment-detail.reducer";
import { StoreToken } from "tokens/redux-store.token";

// Interfaces
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import {
    IQuestionModel,
    IQuestionModelSubQuestion,
} from "interfaces/assessment/question-model.interface";
import { IStore } from "interfaces/redux.interface";

// Services
import { AaDetailActionService } from "modules/assessment/services/action/aa-detail-action.service";

// Enums & Constants
import { AAQuestionErrorCode } from "enums/assessment/assessment-question.enum";
import { InventorySchemaIds } from "constants/inventory-config.constant";
import { IAssessmentTemplateQuestion } from "modules/template/interfaces/template.interface";

@Component({
    selector: "aa-invalid-response",
    template: `
        <div class="aa-question__invalid-response border-radius padding-all-1 {{wrapperClass}}">
            <div>
                <i class="ot ot-exclamation-circle"></i>
                <span class="aa-question__invalid-response-text padding-left-half">
                    <b>{{errorTitle | otTranslate}}:</b>
                    {{errorDescription | otTranslate}}
                </span>
            </div>
            <div class="row-space-between">
                <div class="margin-top-1">
                    <div
                        *ngFor="let response of responses;"
                        class="padding-top-bottom-1"
                    >
                        {{response[valueKey]}}
                    </div>
                </div>
                <button otButton neutral
                    class="margin-left-half align-self-end"
                    ot-auto-id="AssessmentInvalidResponseClearButton"
                    (click)="onClear()"
                    >
                    {{ buttonText | otTranslate}}
                </button>
            </div>
        </div>
    `,
})
export class AAInvalidResponse implements OnInit {

    @Input() buttonText = "Clear";
    @Input() question: IQuestionModel;
    @Input() responses = [];
    @Input() subQuestion: IQuestionModelSubQuestion;
    @Input() valueKey: string;
    @Input() wrapperClass: string;

    errorTitle = "InvalidResponse";
    errorDescription = "InvalidResponseMessage";

    constructor(
        @Inject(StoreToken) private store: IStore,
        private assessmentDetailAction: AaDetailActionService,
    ) {}

    ngOnInit() {
        if (!this.subQuestion && this.question.errorCode === AAQuestionErrorCode.INVENTORY_NOT_EXISTS) {
            this.errorDescription = "Error.AssessmentValidationInventoryDeleted";
        } else if (this.subQuestion && this.subQuestion.errorCode === AAQuestionErrorCode.DUPLICATE_INVENTORY) {
            this.errorDescription = this.getDuplicateInventoryText();
        }
    }

    getDuplicateInventoryText(): string {
        const inventoryQuestion = geInventoryQuestionByAttributeQuestionId(this.question.id)(this.store.getState());

        if (this.isAssetHostingLocationValidation(inventoryQuestion)) {
            return "Error.AssessmentValidationAssetHostingLocation";
        } else if (this.isVendorTypeValidation(inventoryQuestion)) {
            return "Error.AssessmentValidationVendorTypeError";
        } else {
            return this.errorDescription;
        }
    }

    onClear(): void {
        const parentAssessmentDetailId = this.subQuestion ? this.subQuestion.parentAssessmentDetailId : null;
        this.assessmentDetailAction.clearInvalidResponses(this.question.id, parentAssessmentDetailId);
    }

    private isAssetHostingLocationValidation(sourceInventory: IAssessmentTemplateQuestion): boolean {
        return sourceInventory.attributes.inventoryType === InventorySchemaIds.Assets && this.question.attributes.fieldName === "location";
    }

    private isVendorTypeValidation(sourceInventory: IAssessmentTemplateQuestion): boolean {
        return sourceInventory.attributes.inventoryType === InventorySchemaIds.Vendors && this.question.attributes.fieldName === "type";
    }

}
