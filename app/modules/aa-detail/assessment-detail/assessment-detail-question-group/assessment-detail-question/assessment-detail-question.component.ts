// Rxjs
import {
    Subscription,
    Subject,
} from "rxjs";
import {
    startWith,
    takeUntil,
} from "rxjs/operators";

// 3rd Party
import {
    Component,
    ContentChild,
    Input,
    ElementRef,
    Inject,
    OnChanges,
    OnDestroy,
    OnInit,
    SimpleChanges,
    ChangeDetectorRef,
} from "@angular/core";
import { isEqual } from "lodash";

// Vitreus
import {
    OtModalService,
    ModalSize,
} from "@onetrust/vitreus";

// Directives
import { QuestionNumberContentDirective } from "./question-number-content.directive";
import { QuestionTitleContentDirective } from "./question-title-content.directive";
import { QuestionAttributeTitleContentDirective } from "./question-attribute-title-content.directive";
import { QuestionDescriptionContentDirective } from "./question-description-content.directive";

// Services
import { AAQuestionCommentViewLogicService } from "modules/assessment/services/view-logic/aa-question-comment-view-logic.service";
import { AssessmentDetailViewScrollerService } from "modules/assessment/services/view-scroller/assessment-detail-view-scroller.service";
import { ModalService } from "sharedServices/modal.service";
import { Permissions } from "modules/shared/services/helper/permissions.service";
import { AssessmentDetailService } from "modules/assessment/services/view-logic/assessment-detail.service";
import { AaSectionNavMessagingService } from "assessmentServices/messaging/aa-section-messaging.service";

// Components
import { AAQuestionActivityStreamModalComponent } from "modules/assessment/components/aa-question-activity-stream/aa-question-activity-stream.component";

// Interface
import { IQuestionModel } from "interfaces/assessment/question-model.interface";
import {
    IStore,
    IStoreState,
} from "interfaces/redux.interface";
import { ILabelValue } from "interfaces/generic.interface";
import { IQuestionCommentCreateRequest } from "interfaces/aa-question-comment.interface";
import { IAAAttachmentModalData } from "interfaces/assessment/assessment-attachment-modal.interface";
import { IQuestionActivityModalData } from "interfaces/assessment/assessment-question-activity.interface";
import { ISectionHeaderInfo } from "interfaces/assessment/section-header-info.interface";

// Constants/Enums
import {
    QuestionTypeNames,
    AttributeQuestionResponseType,
} from "constants/question-types.constant";

// Redux
import {
    getAssessmentDetailModel,
    getAssessmentDetailSelectedSectionId,
    getQuestionCommentCountByQuestionId,
    isReadinessTemplate,
} from "modules/assessment/reducers/assessment-detail.reducer";
import { StoreToken } from "tokens/redux-store.token";
import { AssessmentAction } from "modules/assessment/reducers/assessment.reducer";
import observableFromStore from "oneRedux/utils/observable-from-store";

// Pipe
import { SectionNavFilterShowQuestionPipe } from "modules/assessment/pipes/aa-section-nav-filter-question.pipe";

@Component({
    selector: "assessment-detail-question",
    templateUrl: "./assessment-detail-question.component.html",
})
export class AssessmentDetailQuestionComponent implements OnInit, OnChanges, OnDestroy {
    @Input() description;
    @Input() canShowSideButtons;
    @Input() number;
    @Input() question: IQuestionModel;
    @Input() questionIndex;
    @Input() title;
    @ContentChild(QuestionNumberContentDirective) questionNumberContent: QuestionNumberContentDirective;
    @ContentChild(QuestionTitleContentDirective) questionTitleContent: QuestionTitleContentDirective;
    @ContentChild(QuestionAttributeTitleContentDirective) questionAttributeTitleContent: QuestionAttributeTitleContentDirective;
    @ContentChild(QuestionDescriptionContentDirective) questionDescriptionContent: QuestionDescriptionContentDirective;

    questionTypeNames = QuestionTypeNames;
    canShowCommentButton: boolean;
    canShowAttachmentButton: boolean;
    canShowActivityStreamButton: boolean;
    isReadinessTemplate = false;
    isGRATemplate = false;
    isReadOnly = false;
    isDateQuestion = false;
    showCopyError = false;
    selectedFilter: ILabelValue<string>;
    applyFilterOpacity = false;
    destroy$ = new Subject();
    sectionHeaders: Map<string, ISectionHeaderInfo>;

    private storeSubscription: Subscription;

    constructor(
        @Inject(StoreToken) private readonly store: IStore,
        private el: ElementRef,
        private AssessmentDetailViewScroller: AssessmentDetailViewScrollerService,
        private modalService: ModalService,
        private readonly permissions: Permissions,
        private aaSectionNavMessagingService: AaSectionNavMessagingService,
        private sectionFilterShowQuestion: SectionNavFilterShowQuestionPipe,
        private changeDetectorRef: ChangeDetectorRef,
        private AAQuestionCommentViewLogic: AAQuestionCommentViewLogicService,
        private assessmentDetailLogic: AssessmentDetailService,
        private otModalService: OtModalService,
    ) {}

    ngOnInit(): void {
        this.storeSubscription = observableFromStore(this.store).pipe(
            startWith(this.store.getState()),
        ).subscribe((state: IStoreState): void => this.componentWillReceiveState(state));
        this.assessmentDetailLogic.fetchAssessmentReadOnlyState().subscribe((readonly: boolean) => {
            this.isReadOnly = readonly;
        });
        this.aaSectionNavMessagingService.sectionFilterDataListener.pipe (
            takeUntil(this.destroy$),
        ).subscribe((data: ILabelValue<string>) => {
            this.selectedFilter = data;
            const sectionId = getAssessmentDetailSelectedSectionId(this.store.getState());
            this.applyFilterOpacity = !this.sectionFilterShowQuestion.transform(this.question, this.selectedFilter, this.sectionHeaders[sectionId]);
            this.changeDetectorRef.markForCheck();
        });
    }

    isDateOpen(): boolean {
        if (this.isDateQuestion) {
            return this.dateSelectorExists();
        }
    }

    isDateQuestionType(question: IQuestionModel): boolean {
        return (question.questionType === QuestionTypeNames.Date ||
            (question.questionType === QuestionTypeNames.Attribute && question.attributes &&
                (question.attributes.responseType === AttributeQuestionResponseType.DATE_TIME ||
                question.attributes.responseType === AttributeQuestionResponseType.DATE)));
    }

    dateSelectorExists(): boolean {
        const dateOpen = document.getElementsByClassName("selector alignselectorright");
        return !!(dateOpen && dateOpen[0] || null);
    }

    ngOnChanges(change: SimpleChanges): void {
        this.showCopyError = this.displayCopyError(change.question.currentValue, change.question.previousValue);
        this.isDateQuestion = this.isDateQuestionType(change.question.currentValue);
        this.configureShowButtons();
    }

    ngAfterViewInit() {
        this.AssessmentDetailViewScroller.registerQuestion(this.question.id, this.el.nativeElement);
    }

    displayCopyError(questionCurrentValue: IQuestionModel, questionPreviousValue: IQuestionModel) {
        if (questionCurrentValue.copyErrors && questionCurrentValue.copyErrors.length && questionCurrentValue.questionType !== QuestionTypeNames.Attribute) {
            return (!questionPreviousValue) || (isEqual(questionPreviousValue.responses, questionCurrentValue.responses)
                    && isEqual(questionPreviousValue.justification, questionCurrentValue.justification));
        }
    }

    onAttachmentClicked(question: IQuestionModel) {
        const state = this.store.getState();
        const assessmentId = getAssessmentDetailModel(state).assessmentId;
        const sectionId = getAssessmentDetailSelectedSectionId(state);
        const questionId = question.id;
        const modalData: IAAAttachmentModalData = {
            assessmentId,
            sectionId,
            questionId,
            callback: (attachmentsCount: number) => {
                this.question.totalAttachments = attachmentsCount;
            },
        };
        this.modalService.setModalData(modalData);
        this.modalService.openModal("downgradeAAAttachmentsModal");
    }

    onCommentsClicked(question: IQuestionModel): void {
        const state = this.store.getState();
        const assessmentModel = getAssessmentDetailModel(state);
        if (!question.totalComments && (this.AAQuestionCommentViewLogic.checkForRespondent()
            || this.permissions.canShow("CreateQuestionComment"))) {
            const assessmentId = assessmentModel.assessmentId;
            const sectionId = getAssessmentDetailSelectedSectionId(state);
            const questionId = question.id;
            const modalData: IQuestionCommentCreateRequest = {
                assessmentId,
                sectionId,
                questionId,
                comment: "",
                callback: (commentsCount: number) => {
                    this.question.totalComments = commentsCount;
                },
            };
            this.modalService.setModalData(modalData);
            this.modalService.openModal("downgradeAACommentsModal");
        } else if (question.totalComments > 0 && this.AAQuestionCommentViewLogic.permissionCheckForComment()) {
            this.store.dispatch({ type: AssessmentAction.SET_PRESELECTED_COMMENT_QUESTION_ID, preSelectedCommentQuestionId: question.id });
        }
    }

    onActivityStreamClick(question: IQuestionModel): void {
        const state = this.store.getState();
        const assessmentModel = getAssessmentDetailModel(state);
        const modalData: IQuestionActivityModalData = {
            assessmentId: assessmentModel.assessmentId,
            questionId: question.id,
        };
        this.otModalService
            .create(
                AAQuestionActivityStreamModalComponent,
                {
                    isComponent: true,
                    size: ModalSize.MEDIUM,
                },
                modalData,
            );
    }

    checkForQuestionType(): boolean {
        return (this.question.questionType !== this.questionTypeNames.Statement);
    }

    ngOnDestroy(): void {
        if (this.storeSubscription) {
            this.storeSubscription.unsubscribe();
        }
        this.destroy$.next();
        this.destroy$.complete();
    }

    showGuidance(): void {
        const modalData = {
            modalTitle: "ShowGuidance",
            questionId: this.question.id,
        };
        this.modalService.setModalData(modalData);
        this.modalService.openModal("downgradeShowGuidanceModal");
    }

    private componentWillReceiveState(state: IStoreState): void {
        if (this.question) {
            this.question.totalComments = getQuestionCommentCountByQuestionId(this.question.id)(state);
        }
        this.isReadinessTemplate = isReadinessTemplate()(state);
        this.isGRATemplate = isReadinessTemplate("GRA")(state);
        this.sectionHeaders = getAssessmentDetailModel(state).sectionHeaders.byIds;
    }

    private configureShowButtons(): void {
        this.canShowCommentButton = this.canShowComment();
        this.canShowAttachmentButton = this.canShowAttachment();
        this.canShowActivityStreamButton = this.canShowActivityStream();
    }

    private canShowComment(): boolean {
        return this.AAQuestionCommentViewLogic.permissionCheckForComment()
            && this.question.questionType !== this.questionTypeNames.Incident
            && this.checkForQuestionType();
    }

    private canShowAttachment(): boolean {
        return this.question.questionType !== this.questionTypeNames.Statement
            && this.question.questionType !== this.questionTypeNames.Incident
            && (this.permissions.canShow("AssessmentAttachmentViewandDownload") || this.permissions.canShow("AssessmentAttachmentAdd"));
    }

    private canShowActivityStream(): boolean {
        return this.permissions.canShow("QuestionHistory")
            && this.question.questionType !== this.questionTypeNames.Statement
            && this.question.questionType !== this.questionTypeNames.Incident;
    }
}
