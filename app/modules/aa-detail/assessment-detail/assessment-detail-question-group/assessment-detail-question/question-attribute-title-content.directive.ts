import { Directive, TemplateRef } from "@angular/core";

@Directive({ selector: "[questionAttributeTitleContent]" })
export class QuestionAttributeTitleContentDirective {
    constructor(public templateRef: TemplateRef<any>) {
    }
}
